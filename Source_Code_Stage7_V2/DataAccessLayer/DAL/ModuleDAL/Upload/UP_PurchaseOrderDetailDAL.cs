﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using System.Data;

using Utilities;
using BusinessEntities.ModuleBE.Upload;
using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using BusinessEntities.ModuleBE.Appointment.Booking;
using BusinessEntities.ModuleBE.Discrepancy.LogDiscrepancy;


namespace DataAccessLayer.ModuleDAL.Upload
{
    public class UP_PurchaseOrderDetailDAL
    {
        public List<Up_PurchaseOrderDetailBE> GetAllPODetailsDAL(Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE)
        {
            List<Up_PurchaseOrderDetailBE> Up_PurchaseOrderDetailBEList = new List<Up_PurchaseOrderDetailBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[4];
                param[index++] = new SqlParameter("@Action", oUp_PurchaseOrderDetailBE.Action);
                param[index++] = new SqlParameter("@Purchase_order", oUp_PurchaseOrderDetailBE.Purchase_order);
                param[index++] = new SqlParameter("@PurchaseOrderID", oUp_PurchaseOrderDetailBE.PurchaseOrderID);
                param[index++] = new SqlParameter("@CountryID", oUp_PurchaseOrderDetailBE.CountryID);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spUP_PurchaseOrderDetails", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    Up_PurchaseOrderDetailBE oNewUp_PurchaseOrderDetailBEList = new Up_PurchaseOrderDetailBE();
                    oNewUp_PurchaseOrderDetailBEList.Original_due_date = dr["Original_due_date"] != DBNull.Value ? Convert.ToDateTime(dr["Original_due_date"]) : (DateTime?)null;
                    oNewUp_PurchaseOrderDetailBEList.SKUID = dr["SKUID"] != DBNull.Value ? Convert.ToInt32(dr["SKUID"]) : (int?)null;
                    oNewUp_PurchaseOrderDetailBEList.Purchase_order = dr["Purchase_order"].ToString();
                    oNewUp_PurchaseOrderDetailBEList.RevisedDueDate = dr["RevisedDueDate"] != DBNull.Value ? Convert.ToDateTime(dr["RevisedDueDate"]) : (DateTime?)null;
                    oNewUp_PurchaseOrderDetailBEList.Vendor_Code = dr["Vendor_Code"].ToString();
                    oNewUp_PurchaseOrderDetailBEList.Vendor_No = dr["Vendor_No"].ToString();
                    oNewUp_PurchaseOrderDetailBEList.Line_No = dr["Line_No"] != DBNull.Value ? Convert.ToInt32(dr["Line_No"].ToString()) : (int?)null;
                    oNewUp_PurchaseOrderDetailBEList.Site = new BusinessEntities.ModuleBE.AdminFunctions.MAS_SiteBE();
                    oNewUp_PurchaseOrderDetailBEList.Site.SiteID = Convert.ToInt32(dr["SiteID"].ToString());
                    oNewUp_PurchaseOrderDetailBEList.Site.ToleranceDueDay = dr["APP_ToleranceDueDay"] != DBNull.Value ? Convert.ToInt32(dr["APP_ToleranceDueDay"]) : (int?)null;
                    oNewUp_PurchaseOrderDetailBEList.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();
                    oNewUp_PurchaseOrderDetailBEList.Vendor.ParentVendorID = dr["ParentVendorID"] != DBNull.Value ? Convert.ToInt32(dr["ParentVendorID"]) : (int?)null;
                    oNewUp_PurchaseOrderDetailBEList.Vendor.VendorID = Convert.ToInt32(dr["VendorID"]);
                    oNewUp_PurchaseOrderDetailBEList.LastUploadedFlag = dr["LastUploadedFlag"] != DBNull.Value ? dr["LastUploadedFlag"].ToString() : "N";
                    // Sprint 1 - Point 14 - Begin - Adding [Expected Date]
                    oNewUp_PurchaseOrderDetailBEList.Expected_date = dr["Expected_Date"] != DBNull.Value ? Convert.ToDateTime(dr["Expected_Date"]) : (DateTime?)null;
                    // Sprint 1 - Point 14 - End - Adding [Expected Date]

                    Up_PurchaseOrderDetailBEList.Add(oNewUp_PurchaseOrderDetailBEList);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return Up_PurchaseOrderDetailBEList;
        }


        public List<Up_PurchaseOrderDetailBE> GetOpenPODetailsDAL(Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE)
        {
            List<Up_PurchaseOrderDetailBE> Up_PurchaseOrderDetailBEList = new List<Up_PurchaseOrderDetailBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[6];

                param[index++] = new SqlParameter("@SelectedVendorIDs", oUp_PurchaseOrderDetailBE.SelectedVendorIDs);
                param[index++] = new SqlParameter("@SelectedSiteIDs", oUp_PurchaseOrderDetailBE.SelectedSiteIDs);
                param[index++] = new SqlParameter("@SelectedInventoryMgrIDs", oUp_PurchaseOrderDetailBE.SelectedInventoryMgrIDs);
                param[index++] = new SqlParameter("@PurchaseOrder", oUp_PurchaseOrderDetailBE.PurchaseOrderIds);
                param[index++] = new SqlParameter("@Date", oUp_PurchaseOrderDetailBE.SelectedDate);
                param[index++] = new SqlParameter("@Action", oUp_PurchaseOrderDetailBE.Action);


                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spRPTBOK_OpenPOOverview", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    Up_PurchaseOrderDetailBE oNewUp_PurchaseOrderDetailBEList = new Up_PurchaseOrderDetailBE();

                    oNewUp_PurchaseOrderDetailBEList.Original_due_dateString = dr["Original_due_date"] != DBNull.Value ? dr["Original_due_date"].ToString() : string.Empty;


                    oNewUp_PurchaseOrderDetailBEList.CreatedBy = dr["CreatedBy"].ToString();
                    oNewUp_PurchaseOrderDetailBEList.StockPlannerNo = dr["StockPlannerNo"] != DBNull.Value ? dr["StockPlannerNo"].ToString() : string.Empty;                    
                    oNewUp_PurchaseOrderDetailBEList.Purchase_order = dr["Purchase_order"].ToString();

                    oNewUp_PurchaseOrderDetailBEList.OD_Code = dr["OD_Code"].ToString();
                    oNewUp_PurchaseOrderDetailBEList.Direct_code = dr["Direct_code"].ToString();
                    oNewUp_PurchaseOrderDetailBEList.Vendor_Code = dr["Vendor_code"].ToString();
                    oNewUp_PurchaseOrderDetailBEList.Product_description = dr["Product_description"].ToString();
                    oNewUp_PurchaseOrderDetailBEList.UOM = dr["UOM"].ToString();
                    oNewUp_PurchaseOrderDetailBEList.Original_quantity = dr["Original_quantity"].ToString();

                    oNewUp_PurchaseOrderDetailBEList.Outstanding_Qty = dr["Outstanding_Qty"].ToString();
                    oNewUp_PurchaseOrderDetailBEList.Qty_On_Hand = dr["Qty_On_Hand"].ToString();
                    oNewUp_PurchaseOrderDetailBEList.Vendor_Name = dr["Vendor_Name"].ToString();
                    oNewUp_PurchaseOrderDetailBEList.Vendor_No = dr["Vendor_No"].ToString();
                    oNewUp_PurchaseOrderDetailBEList.Other = dr["Other"].ToString();
                    oNewUp_PurchaseOrderDetailBEList.SiteName = dr["SiteName"].ToString();


                    oNewUp_PurchaseOrderDetailBEList.Expected_dateString = dr["Expected_Date"] != DBNull.Value ? dr["Expected_Date"].ToString() : string.Empty;
                    oNewUp_PurchaseOrderDetailBEList.ScheduleDateString = dr["ScheduleDate"] != DBNull.Value ? dr["ScheduleDate"].ToString() : string.Empty;
                    oNewUp_PurchaseOrderDetailBEList.Order_raisedString = dr["Order_raised"] != DBNull.Value ? dr["Order_raised"].ToString() : string.Empty;

                    Up_PurchaseOrderDetailBEList.Add(oNewUp_PurchaseOrderDetailBEList);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return Up_PurchaseOrderDetailBEList;
        }

        public List<Up_PurchaseOrderDetailBE> GetRemainingBookingPODetails(Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE)
        {
            List<Up_PurchaseOrderDetailBE> Up_PurchaseOrderDetailBEList = new List<Up_PurchaseOrderDetailBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[4];
                param[index++] = new SqlParameter("@Action", oUp_PurchaseOrderDetailBE.Action);
                param[index++] = new SqlParameter("@VendorID", oUp_PurchaseOrderDetailBE.Vendor.VendorID);
                param[index++] = new SqlParameter("@SiteID", oUp_PurchaseOrderDetailBE.Site.SiteID);
                param[index++] = new SqlParameter("@SelectedProductCodes", oUp_PurchaseOrderDetailBE.SelectedProductCodes);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spUP_PurchaseOrderDetails", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    Up_PurchaseOrderDetailBE oNewUp_PurchaseOrderDetailBEList = new Up_PurchaseOrderDetailBE();
                    oNewUp_PurchaseOrderDetailBEList.Original_due_date = dr["Original_due_date"] != DBNull.Value ? Convert.ToDateTime(dr["Original_due_date"]) : (DateTime?)null;
                    oNewUp_PurchaseOrderDetailBEList.Purchase_order = dr["Purchase_order"].ToString();
                    oNewUp_PurchaseOrderDetailBEList.Site = new BusinessEntities.ModuleBE.AdminFunctions.MAS_SiteBE();
                    oNewUp_PurchaseOrderDetailBEList.Site.ToleranceDueDay = dr["APP_ToleranceDueDay"] != DBNull.Value ? Convert.ToInt32(dr["APP_ToleranceDueDay"]) : (int?)null;
                    Up_PurchaseOrderDetailBEList.Add(oNewUp_PurchaseOrderDetailBEList);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return Up_PurchaseOrderDetailBEList;
        }

        public List<Up_PurchaseOrderDetailBE> GetProductOrderDetailsDAL(Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE)
        {
            List<Up_PurchaseOrderDetailBE> Up_PurchaseOrderDetailBEList = new List<Up_PurchaseOrderDetailBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[6];
                param[index++] = new SqlParameter("@Action", oUp_PurchaseOrderDetailBE.Action);
                param[index++] = new SqlParameter("@Purchase_order", oUp_PurchaseOrderDetailBE.Purchase_order);
                param[index++] = new SqlParameter("@PurchaseOrderID", oUp_PurchaseOrderDetailBE.PurchaseOrderID);
                param[index++] = new SqlParameter("@CountryID", oUp_PurchaseOrderDetailBE.CountryID);
                if (oUp_PurchaseOrderDetailBE.Vendor != null)
                    param[index++] = new SqlParameter("@VendorID", oUp_PurchaseOrderDetailBE.Vendor.VendorID);
                if (oUp_PurchaseOrderDetailBE.Site != null)
                    param[index++] = new SqlParameter("@SiteID", oUp_PurchaseOrderDetailBE.Site.SiteID);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spUP_PurchaseOrderDetails", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    Up_PurchaseOrderDetailBE oNewUp_PurchaseOrderDetailBEList = new Up_PurchaseOrderDetailBE();
                    oNewUp_PurchaseOrderDetailBEList.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();
                    oNewUp_PurchaseOrderDetailBEList.Vendor.VendorID = Convert.ToInt32(dr["VendorID"].ToString());
                    oNewUp_PurchaseOrderDetailBEList.Original_due_date = dr["Original_due_date"] != DBNull.Value ? Convert.ToDateTime(dr["Original_due_date"]) : (DateTime?)null;
                    oNewUp_PurchaseOrderDetailBEList.Vendor.VendorName = dr["VendorName"].ToString();
                    oNewUp_PurchaseOrderDetailBEList.OutstandingLines = Convert.ToInt32(dr["OutstandingLines"].ToString());
                    oNewUp_PurchaseOrderDetailBEList.SKU = new UP_SKUBE();
                    oNewUp_PurchaseOrderDetailBEList.SKU.Qty_On_Hand = dr["Qty_On_Hand"].ToString();
                    oNewUp_PurchaseOrderDetailBEList.SKU.qty_on_backorder = dr["qty_on_backorder"].ToString();
                    oNewUp_PurchaseOrderDetailBEList.Site = new BusinessEntities.ModuleBE.AdminFunctions.MAS_SiteBE();
                    oNewUp_PurchaseOrderDetailBEList.Site.SiteName = dr["SiteName"].ToString();
                    oNewUp_PurchaseOrderDetailBEList.PurchaseOrderID = Convert.ToInt32(dr["PurchaseOrderID"].ToString());

                    //oNewUp_PurchaseOrderDetailBEList.SKUID = Convert.ToInt32(dr["SKUID"]);
                    //oNewUp_PurchaseOrderDetailBEList.Purchase_order = dr["Purchase_order"].ToString();
                    //oNewUp_PurchaseOrderDetailBEList.RevisedDueDate = dr["RevisedDueDate"] != DBNull.Value ? Convert.ToDateTime(dr["RevisedDueDate"]) : (DateTime?)null;


                    //oNewUp_PurchaseOrderDetailBEList.Vendor_Code = dr["Vendor_Code"].ToString();
                    //oNewUp_PurchaseOrderDetailBEList.Vendor_No = dr["Vendor_No"].ToString();
                    //oNewUp_PurchaseOrderDetailBEList.Line_No = dr["Line_No"].ToString();
                    // Sprint 1 - Point 14 - Begin - Adding [Expected Date]
                    oNewUp_PurchaseOrderDetailBEList.Expected_date = dr["Expected_Date"] != DBNull.Value ? Convert.ToDateTime(dr["Expected_Date"]) : (DateTime?)null;
                    // Sprint 1 - Point 14 - End - Adding [Expected Date]


                    Up_PurchaseOrderDetailBEList.Add(oNewUp_PurchaseOrderDetailBEList);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return Up_PurchaseOrderDetailBEList;
        }

        public int? addEditRevisedLeadTimeDAL(Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE)
        {
            int? intResult = 0;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[4];
                param[index++] = new SqlParameter("@Action", oUp_PurchaseOrderDetailBE.Action);
                param[index++] = new SqlParameter("@RevisedDueDate", oUp_PurchaseOrderDetailBE.RevisedDueDate);
                param[index++] = new SqlParameter("@SelectedProductCodes", oUp_PurchaseOrderDetailBE.SelectedProductCodes);
                param[index++] = new SqlParameter("@Purchase_order", oUp_PurchaseOrderDetailBE.Purchase_order);

                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spUP_PurchaseOrderDetails", param);

                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
                else
                    intResult = 0;

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public List<Up_PurchaseOrderDetailBE> GetODProductCodesDAL(Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE)
        {
            List<Up_PurchaseOrderDetailBE> Up_PurchaseOrderDetailBEList = new List<Up_PurchaseOrderDetailBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[4];
                param[index++] = new SqlParameter("@Action", oUp_PurchaseOrderDetailBE.Action);
                param[index++] = new SqlParameter("@Purchase_order", oUp_PurchaseOrderDetailBE.Purchase_order);
                param[index++] = new SqlParameter("@RevisedDueDate", oUp_PurchaseOrderDetailBE.RevisedDueDate);
                param[index++] = new SqlParameter("@Order_raised", oUp_PurchaseOrderDetailBE.Order_raised);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spUP_PurchaseOrderDetails", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    Up_PurchaseOrderDetailBE oNewUp_PurchaseOrderDetailBEList = new Up_PurchaseOrderDetailBE();
                    oNewUp_PurchaseOrderDetailBEList.SKUID = Convert.ToInt32(dr["SKUID"]);
                    oNewUp_PurchaseOrderDetailBEList.Purchase_order = dr["Purchase_order"].ToString();
                    oNewUp_PurchaseOrderDetailBEList.RevisedDueDate = dr["RevisedDueDate"] != DBNull.Value ? Convert.ToDateTime(dr["RevisedDueDate"]) : (DateTime?)null;
                    oNewUp_PurchaseOrderDetailBEList.ProductDescription = dr["ProductDescription"].ToString();

                    Up_PurchaseOrderDetailBEList.Add(oNewUp_PurchaseOrderDetailBEList);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return Up_PurchaseOrderDetailBEList;
        }

        public List<Up_PurchaseOrderDetailBE> GetSKUCodesDAL(Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE)
        {
            List<Up_PurchaseOrderDetailBE> Up_PurchaseOrderDetailBEList = new List<Up_PurchaseOrderDetailBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[4];
                param[index++] = new SqlParameter("@Action", oUp_PurchaseOrderDetailBE.Action);
                param[index++] = new SqlParameter("@Purchase_order", oUp_PurchaseOrderDetailBE.Purchase_order);
                //param[index++] = new SqlParameter("@SiteID", oUp_PurchaseOrderDetailBE.Site );
                param[index++] = new SqlParameter("@Order_raised", oUp_PurchaseOrderDetailBE.Order_raised);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spUP_PurchaseOrderDetails", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    Up_PurchaseOrderDetailBE oNewUp_PurchaseOrderDetailBEList = new Up_PurchaseOrderDetailBE();
                    oNewUp_PurchaseOrderDetailBEList.SKUID = Convert.ToInt32(dr["SKUID"]);
                    oNewUp_PurchaseOrderDetailBEList.Purchase_order = dr["PurchaseNumber"].ToString();
                   // oNewUp_PurchaseOrderDetailBEList.RevisedDueDate = dr["RevisedDueDate"] != DBNull.Value ? Convert.ToDateTime(dr["RevisedDueDate"]) : (DateTime?)null;
                    oNewUp_PurchaseOrderDetailBEList.ProductDescription = dr["ProductDescription"].ToString();

                    Up_PurchaseOrderDetailBEList.Add(oNewUp_PurchaseOrderDetailBEList);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return Up_PurchaseOrderDetailBEList;
        }

        public List<Up_PurchaseOrderDetailBE> GetRevisedLeadListDAL(Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE)
        {
            List<Up_PurchaseOrderDetailBE> Up_PurchaseOrderDetailBEList = new List<Up_PurchaseOrderDetailBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[2];
                param[index++] = new SqlParameter("@Action", oUp_PurchaseOrderDetailBE.Action);
                param[index++] = new SqlParameter("@PurchaseOrderID", oUp_PurchaseOrderDetailBE.PurchaseOrderID);


                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spUP_PurchaseOrderDetails", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    Up_PurchaseOrderDetailBE oNewUp_PurchaseOrderDetailBEList = new Up_PurchaseOrderDetailBE();
                    oNewUp_PurchaseOrderDetailBEList.PurchaseOrderID = Convert.ToInt32(dr["PurchaseOrderID"]);
                    oNewUp_PurchaseOrderDetailBEList.Purchase_order = dr["Purchase_order"].ToString();
                    oNewUp_PurchaseOrderDetailBEList.RevisedDueDate = dr["RevisedDueDate"] != DBNull.Value ? Convert.ToDateTime(dr["RevisedDueDate"]) : (DateTime?)null;
                    oNewUp_PurchaseOrderDetailBEList.ProductDescription = dr["ProductDescription"].ToString();
                    oNewUp_PurchaseOrderDetailBEList.CountryName = dr["CountryName"].ToString();

                    Up_PurchaseOrderDetailBEList.Add(oNewUp_PurchaseOrderDetailBEList);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return Up_PurchaseOrderDetailBEList;
        }

        public List<Up_PurchaseOrderDetailBE> GetRevisedSKULeadListDAL(Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE)
        {
            List<Up_PurchaseOrderDetailBE> Up_PurchaseOrderDetailBEList = new List<Up_PurchaseOrderDetailBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[2];
                param[index++] = new SqlParameter("@Action", oUp_PurchaseOrderDetailBE.Action);
                param[index++] = new SqlParameter("@PurchaseOrderID", oUp_PurchaseOrderDetailBE.PurchaseOrderID);


                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spUP_PurchaseOrderDetails", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    Up_PurchaseOrderDetailBE oNewUp_PurchaseOrderDetailBEList = new Up_PurchaseOrderDetailBE();
                    oNewUp_PurchaseOrderDetailBEList.Site = new BusinessEntities.ModuleBE.AdminFunctions.MAS_SiteBE();
                    oNewUp_PurchaseOrderDetailBEList.PurchaseOrderID = Convert.ToInt32(dr["PurchaseOrderID"]);
                    oNewUp_PurchaseOrderDetailBEList.Purchase_order = dr["Purchase_order"].ToString();
                    oNewUp_PurchaseOrderDetailBEList.RevisedDueDate = dr["RevisedDueDate"] != DBNull.Value ? Convert.ToDateTime(dr["RevisedDueDate"]) : (DateTime?)null;
                    oNewUp_PurchaseOrderDetailBEList.ProductDescription = dr["ProductDescription"].ToString();
                    oNewUp_PurchaseOrderDetailBEList.CountryName = dr["CountryName"].ToString();
                    oNewUp_PurchaseOrderDetailBEList.Site.SiteName  = dr["SiteName"].ToString();

                    Up_PurchaseOrderDetailBEList.Add(oNewUp_PurchaseOrderDetailBEList);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return Up_PurchaseOrderDetailBEList;
        }

        public List<Up_PurchaseOrderDetailBE> GetAllBookedProductOrderDetailsDAL(Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE)
        {
            List<Up_PurchaseOrderDetailBE> Up_PurchaseOrderDetailBEList = new List<Up_PurchaseOrderDetailBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[3];
                param[index++] = new SqlParameter("@Action", oUp_PurchaseOrderDetailBE.Action);
                param[index++] = new SqlParameter("@BookingID", oUp_PurchaseOrderDetailBE.BookingID);
                param[index++] = new SqlParameter("@UserID", oUp_PurchaseOrderDetailBE.UserID);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spUP_PurchaseOrderDetails", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    Up_PurchaseOrderDetailBE oNewUp_PurchaseOrderDetailBEList = new Up_PurchaseOrderDetailBE();
                    oNewUp_PurchaseOrderDetailBEList.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();
                    oNewUp_PurchaseOrderDetailBEList.Vendor.VendorID = Convert.ToInt32(dr["VendorID"].ToString());
                    oNewUp_PurchaseOrderDetailBEList.Original_due_date = dr["Original_due_date"] != DBNull.Value ? Convert.ToDateTime(dr["Original_due_date"]) : (DateTime?)null;
                    oNewUp_PurchaseOrderDetailBEList.Vendor.VendorName = dr["VendorName"].ToString();
                    oNewUp_PurchaseOrderDetailBEList.OutstandingLines = Convert.ToInt32(dr["OutstandingLines"].ToString());
                    oNewUp_PurchaseOrderDetailBEList.SKU = new UP_SKUBE();
                    oNewUp_PurchaseOrderDetailBEList.SKU.Qty_On_Hand = dr["Qty_On_Hand"].ToString();
                    oNewUp_PurchaseOrderDetailBEList.SKU.qty_on_backorder = dr["qty_on_backorder"].ToString();
                    oNewUp_PurchaseOrderDetailBEList.Site = new BusinessEntities.ModuleBE.AdminFunctions.MAS_SiteBE();
                    oNewUp_PurchaseOrderDetailBEList.Site.SiteName = dr["SiteName"].ToString();
                    oNewUp_PurchaseOrderDetailBEList.Purchase_order = dr["Purchase_order"].ToString();
                    oNewUp_PurchaseOrderDetailBEList.PurchaseOrderID = Convert.ToInt32(dr["PurchaseOrderID"].ToString());
                    // Sprint 1 Changes Begin
                    oNewUp_PurchaseOrderDetailBEList.Expected_date = dr["Expected_date"] != DBNull.Value ? Convert.ToDateTime(dr["Expected_date"]) : (DateTime?)null;
                    // Sprint 1 Changes End

                    if (ds.Tables[0].Columns.Contains("IsPOManuallyAdded")) {
                        oNewUp_PurchaseOrderDetailBEList.IsPOManuallyAdded = dr["IsPOManuallyAdded"] != DBNull.Value ? Convert.ToBoolean(dr["IsPOManuallyAdded"]) : false;
                    }

                    Up_PurchaseOrderDetailBEList.Add(oNewUp_PurchaseOrderDetailBEList);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return Up_PurchaseOrderDetailBEList;
        }

        public List<Up_PurchaseOrderDetailBE> GetAllPOList(Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE)
        {
            List<Up_PurchaseOrderDetailBE> Up_PurchaseOrderDetailBEList = new List<Up_PurchaseOrderDetailBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[3];
                param[index++] = new SqlParameter("@Action", oUp_PurchaseOrderDetailBE.Action);
                param[index++] = new SqlParameter("@PurchaseOrderID", oUp_PurchaseOrderDetailBE.PurchaseOrderID);


                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spUP_PurchaseOrderDetails", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    Up_PurchaseOrderDetailBE oNewUp_PurchaseOrderDetailBEList = new Up_PurchaseOrderDetailBE();
                    oNewUp_PurchaseOrderDetailBEList.PurchaseOrderID = Convert.ToInt32(dr["PurchaseOrderID"].ToString());
                    oNewUp_PurchaseOrderDetailBEList.Purchase_order = dr["Purchase_order"].ToString();

                    Up_PurchaseOrderDetailBEList.Add(oNewUp_PurchaseOrderDetailBEList);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return Up_PurchaseOrderDetailBEList;
        }


        public List<Up_PurchaseOrderDetailBE> GetPriorityDAL(Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE)
        {
            List<Up_PurchaseOrderDetailBE> Up_PurchaseOrderDetailBEList = new List<Up_PurchaseOrderDetailBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[6];
                param[index++] = new SqlParameter("@Action", oUp_PurchaseOrderDetailBE.Action);
                param[index++] = new SqlParameter("@BookingID", oUp_PurchaseOrderDetailBE.BookingID);
                param[index++] = new SqlParameter("@SiteID", oUp_PurchaseOrderDetailBE.Site.SiteID);
                param[index++] = new SqlParameter("@Purchase_order", oUp_PurchaseOrderDetailBE.Purchase_order);
                if (oUp_PurchaseOrderDetailBE.Vendor != null)
                    param[index++] = new SqlParameter("@VendorID", oUp_PurchaseOrderDetailBE.Vendor.VendorID);
                if (oUp_PurchaseOrderDetailBE.IsPOManuallyAdded != null)
                    param[index++] = new SqlParameter("@IsPOManuallyAdded", oUp_PurchaseOrderDetailBE.IsPOManuallyAdded);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spUP_PurchaseOrderDetails", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    Up_PurchaseOrderDetailBE oNewUp_PurchaseOrderDetailBEList = new Up_PurchaseOrderDetailBE();
                    oNewUp_PurchaseOrderDetailBEList.SKU = new UP_SKUBE();

                    oNewUp_PurchaseOrderDetailBEList.SKU.Qty_On_Hand = dr["Qty_On_Hand"].ToString();
                    oNewUp_PurchaseOrderDetailBEList.SKU.qty_on_backorder = dr["qty_on_backorder"].ToString();
                    oNewUp_PurchaseOrderDetailBEList.Original_quantity = dr["Original_quantity"].ToString();
                    oNewUp_PurchaseOrderDetailBEList.Qty_receipted = dr["Qty_receipted"] != DBNull.Value ? Convert.ToInt32(dr["Qty_receipted"]) : (int?)null;
                    oNewUp_PurchaseOrderDetailBEList.LineCount = dr["LineCount"] != DBNull.Value ? Convert.ToInt32(dr["LineCount"]) : (int?)null;
                    // Sprint 1 Changes Begin
                    oNewUp_PurchaseOrderDetailBEList.Expected_date = dr["Expected_date"] != DBNull.Value ? Convert.ToDateTime(dr["Expected_date"]) : (DateTime?)null;
                    // Sprint 1 Changes End

                    Up_PurchaseOrderDetailBEList.Add(oNewUp_PurchaseOrderDetailBEList);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return Up_PurchaseOrderDetailBEList;
        }


        public void AddPurchaseOrderDetails(List<Up_PurchaseOrderDetailBE> pUp_PurchaseOrderDetailBEList)
        {
            try
            {

                DataTable dt = new DataTable();
                DataRow dr;

                dt.Columns.Add(new DataColumn("Purchase_order"));
                dt.Columns.Add(new DataColumn("Line_No", typeof(System.Int32)));
                dt.Columns.Add(new DataColumn("Warehouse"));
                dt.Columns.Add(new DataColumn("Vendor_No"));
                dt.Columns.Add(new DataColumn("subvndr"));
                dt.Columns.Add(new DataColumn("Direct_code"));
                dt.Columns.Add(new DataColumn("OD_Code"));
                dt.Columns.Add(new DataColumn("Vendor_Code"));
                dt.Columns.Add(new DataColumn("Product_description"));
                dt.Columns.Add(new DataColumn("UOM"));
                dt.Columns.Add(new DataColumn("Original_quantity", typeof(System.Int32)));
                dt.Columns.Add(new DataColumn("Outstanding_Qty", typeof(System.Int32)));
                dt.Columns.Add(new DataColumn("PO_cost", typeof(System.Decimal)));
                dt.Columns.Add(new DataColumn("Order_raised", typeof(System.DateTime)));
                dt.Columns.Add(new DataColumn("Original_due_date", typeof(System.DateTime)));
                dt.Columns.Add(new DataColumn("Expected_date", typeof(System.DateTime)));
                dt.Columns.Add(new DataColumn("Buyer_no"));
                dt.Columns.Add(new DataColumn("Item_classification"));
                dt.Columns.Add(new DataColumn("Item_type"));
                dt.Columns.Add(new DataColumn("Item_Category"));
                dt.Columns.Add(new DataColumn("Other"));
                dt.Columns.Add(new DataColumn("Country"));
                dt.Columns.Add(new DataColumn("UpdatedDate", typeof(System.DateTime)));

                foreach (Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE in pUp_PurchaseOrderDetailBEList)
                {
                    dr = dt.NewRow();
                    dr["Purchase_order"] = oUp_PurchaseOrderDetailBE.Purchase_order;

                    if (oUp_PurchaseOrderDetailBE.Line_No != null)
                        dr["Line_No"] = oUp_PurchaseOrderDetailBE.Line_No;
                    else
                        dr["Line_No"] = DBNull.Value;

                    dr["Warehouse"] = oUp_PurchaseOrderDetailBE.Warehouse;
                    dr["Vendor_No"] = oUp_PurchaseOrderDetailBE.Vendor_No;
                    dr["subvndr"] = oUp_PurchaseOrderDetailBE.SubVendor;
                    dr["Direct_code"] = oUp_PurchaseOrderDetailBE.Direct_code;
                    dr["OD_Code"] = oUp_PurchaseOrderDetailBE.OD_Code;
                    dr["Vendor_Code"] = oUp_PurchaseOrderDetailBE.Vendor_Code;
                    dr["Product_description"] = oUp_PurchaseOrderDetailBE.Product_description;
                    dr["UOM"] = oUp_PurchaseOrderDetailBE.UOM;

                    if (!string.IsNullOrWhiteSpace(oUp_PurchaseOrderDetailBE.Original_quantity))
                        dr["Original_quantity"] = oUp_PurchaseOrderDetailBE.Original_quantity;
                    else
                        dr["Original_quantity"] = DBNull.Value;

                    if (!string.IsNullOrWhiteSpace(oUp_PurchaseOrderDetailBE.Outstanding_Qty))
                        dr["Outstanding_Qty"] = oUp_PurchaseOrderDetailBE.Outstanding_Qty;
                    else
                        dr["Outstanding_Qty"] = DBNull.Value;

                    if (oUp_PurchaseOrderDetailBE.PO_cost != null)
                        dr["PO_cost"] = oUp_PurchaseOrderDetailBE.PO_cost;
                    else
                        dr["PO_cost"] = DBNull.Value;

                    if (oUp_PurchaseOrderDetailBE.Order_raised != null)
                        dr["Order_raised"] = oUp_PurchaseOrderDetailBE.Order_raised;
                    else
                        dr["Order_raised"] = DBNull.Value;

                    if (oUp_PurchaseOrderDetailBE.Original_due_date != null)
                        dr["Original_due_date"] = oUp_PurchaseOrderDetailBE.Original_due_date;
                    else
                        dr["Original_due_date"] = DBNull.Value;

                    if (oUp_PurchaseOrderDetailBE.Expected_date != null)
                        dr["Expected_date"] = oUp_PurchaseOrderDetailBE.Expected_date;
                    else
                        dr["Expected_date"] = DBNull.Value;

                    dr["Buyer_no"] = oUp_PurchaseOrderDetailBE.Buyer_no;
                    dr["Item_classification"] = oUp_PurchaseOrderDetailBE.Item_classification;
                    dr["Item_type"] = oUp_PurchaseOrderDetailBE.Item_type;
                    dr["Item_Category"] = oUp_PurchaseOrderDetailBE.Item_Category;
                    dr["Other"] = oUp_PurchaseOrderDetailBE.Other;
                    dr["Country"] = oUp_PurchaseOrderDetailBE.CountryName;
                    dr["UpdatedDate"] = oUp_PurchaseOrderDetailBE.UpdatedDate;

                    dt.Rows.Add(dr);
                }
                pUp_PurchaseOrderDetailBEList = null;

                SqlConnection sqlConn = new SqlConnection(DBConnection.Connection);
                sqlConn.Open();

                SqlBulkCopy oSqlBulkCopy = new SqlBulkCopy(sqlConn, SqlBulkCopyOptions.TableLock, null);
                oSqlBulkCopy.DestinationTableName = "UP_StagePurchaseOrderDetails";

                oSqlBulkCopy.ColumnMappings.Add("Purchase_order", "Purchase_order");
                oSqlBulkCopy.ColumnMappings.Add("Line_No", "Line_No");
                oSqlBulkCopy.ColumnMappings.Add("Warehouse", "Warehouse");
                oSqlBulkCopy.ColumnMappings.Add("Vendor_No", "Vendor_No");
                oSqlBulkCopy.ColumnMappings.Add("subvndr", "subvndr");
                oSqlBulkCopy.ColumnMappings.Add("Direct_code", "Direct_code");
                oSqlBulkCopy.ColumnMappings.Add("OD_Code", "OD_Code");
                oSqlBulkCopy.ColumnMappings.Add("Vendor_Code", "Vendor_Code");
                oSqlBulkCopy.ColumnMappings.Add("Product_description", "Product_description");
                oSqlBulkCopy.ColumnMappings.Add("UOM", "UOM");
                oSqlBulkCopy.ColumnMappings.Add("Original_quantity", "Original_quantity");
                oSqlBulkCopy.ColumnMappings.Add("Outstanding_Qty", "Outstanding_Qty");
                oSqlBulkCopy.ColumnMappings.Add("PO_cost", "PO_cost");
                oSqlBulkCopy.ColumnMappings.Add("Order_raised", "Order_raised");
                oSqlBulkCopy.ColumnMappings.Add("Original_due_date", "Original_due_date");
                oSqlBulkCopy.ColumnMappings.Add("Expected_date", "Expected_date");
                oSqlBulkCopy.ColumnMappings.Add("Buyer_no", "Buyer_no");
                oSqlBulkCopy.ColumnMappings.Add("Item_classification", "Item_classification");
                oSqlBulkCopy.ColumnMappings.Add("Item_type", "Item_type");
                oSqlBulkCopy.ColumnMappings.Add("Item_Category", "Item_Category");
                oSqlBulkCopy.ColumnMappings.Add("Other", "Other");
                oSqlBulkCopy.ColumnMappings.Add("Country", "Country");
                oSqlBulkCopy.ColumnMappings.Add("UpdatedDate", "UpdatedDate");

                oSqlBulkCopy.BulkCopyTimeout = 300;
                oSqlBulkCopy.BatchSize = dt.Rows.Count;

                oSqlBulkCopy.WriteToServer(dt);
                oSqlBulkCopy.Close();
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }

        }

        public DataTable GetPoForSchedularDAL()
        {
            DataSet dsValidation = null;
            try
            {
                SqlParameter[] sqlParam = new SqlParameter[1];
                sqlParam[0] = new SqlParameter("@Action", "GetPOForScheduler");

                dsValidation = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spUP_PurchaseOrderDetails", sqlParam);

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return dsValidation.Tables[0];
        }

        // Sprint 1 - Point 7 - Start
        public List<Up_PurchaseOrderDetailBE> GetAllPODetailsOfSiteAndVendor(Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE)
        {
            List<Up_PurchaseOrderDetailBE> Up_PurchaseOrderDetailBEList = new List<Up_PurchaseOrderDetailBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[3];
                param[index++] = new SqlParameter("@Action", oUp_PurchaseOrderDetailBE.Action);
                param[index++] = new SqlParameter("@VendorID", oUp_PurchaseOrderDetailBE.Vendor.VendorID);
                param[index++] = new SqlParameter("@SiteID", oUp_PurchaseOrderDetailBE.Site.SiteID);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spUP_PurchaseOrderDetails", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    Up_PurchaseOrderDetailBE oNewUp_PurchaseOrderDetailBEList = new Up_PurchaseOrderDetailBE();
                    oNewUp_PurchaseOrderDetailBEList.Purchase_order = dr["Purchase_order"].ToString();
                    oNewUp_PurchaseOrderDetailBEList.Original_due_date = dr["Original_due_date"] != DBNull.Value ? Convert.ToDateTime(dr["Original_due_date"]) : (DateTime?)null;
                    oNewUp_PurchaseOrderDetailBEList.Expected_date = dr["Expected_date"] != DBNull.Value ? Convert.ToDateTime(dr["Expected_date"]) : (DateTime?)null;
                    oNewUp_PurchaseOrderDetailBEList.Site = new BusinessEntities.ModuleBE.AdminFunctions.MAS_SiteBE();
                    oNewUp_PurchaseOrderDetailBEList.Site.ToleranceDueDay = dr["APP_ToleranceDueDay"] != DBNull.Value ? Convert.ToInt32(dr["APP_ToleranceDueDay"]) : (int?)null;
                    oNewUp_PurchaseOrderDetailBEList.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();
                    oNewUp_PurchaseOrderDetailBEList.Vendor.VendorName = dr["Vendor_Name"] != null ? Convert.ToString(dr["Vendor_Name"]) : string.Empty;
                    oNewUp_PurchaseOrderDetailBEList.BookingID = dr["BookingID"] != DBNull.Value ? Convert.ToInt32(dr["BookingID"]) : 0;
                    oNewUp_PurchaseOrderDetailBEList.BookingDeliveryDate = dr["ScheduleDate"] != DBNull.Value ? Convert.ToDateTime(dr["ScheduleDate"]) : (DateTime?)null;

                    Up_PurchaseOrderDetailBEList.Add(oNewUp_PurchaseOrderDetailBEList);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return Up_PurchaseOrderDetailBEList;
        }
        // Sprint 1 - Point 7 - End

        public List<UP_VendorBE> GetConsolidateVendorsDAL(UP_VendorBE oUP_VendorBE)
        {
            List<UP_VendorBE> lstVendors = new List<UP_VendorBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[3];
                param[index++] = new SqlParameter("@Action", oUP_VendorBE.Action);
                param[index++] = new SqlParameter("@VendorID", oUP_VendorBE.VendorID);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spUP_PurchaseOrderDetails", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    UP_VendorBE oUP_VendorBENew = new UP_VendorBE();
                    oUP_VendorBENew.VendorID = dr["VendorID"] != DBNull.Value ? Convert.ToInt32(dr["VendorID"]) : 0;

                    lstVendors.Add(oUP_VendorBENew);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return lstVendors;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="up_PurchaseOrderBEErrorList"></param>
        public void AddPurchaseOrderDetailErrors(List<UP_DataImportErrorBE> up_PurchaseOrderBEErrorList)
        {
            try
            {
                DataTable dt = new DataTable();
                DataRow dr;

                dt.Columns.Add(new DataColumn("FolderDownloadID", typeof(System.Int32)));
                dt.Columns.Add(new DataColumn("ManualFileUploadID", typeof(System.Int32)));
                dt.Columns.Add(new DataColumn("ErrorDescription", typeof(System.String)));
                dt.Columns.Add(new DataColumn("UpdatedDate", typeof(System.DateTime)));


                foreach (UP_DataImportErrorBE obj_DataImportErrorBE in up_PurchaseOrderBEErrorList)
                {
                    dr = dt.NewRow();

                    dr["FolderDownloadID"] = obj_DataImportErrorBE.FolderDownloadID;
                    dr["ManualFileUploadID"] = obj_DataImportErrorBE.ManualFileUploadID;
                    dr["ErrorDescription"] = obj_DataImportErrorBE.ErrorDescription;
                    dr["UpdatedDate"] = obj_DataImportErrorBE.UpdatedDate;


                    //if (oUP_SKUBE.Valuated_Stock != null)
                    //    dr["Valuated_Stock"] = oUP_SKUBE.Valuated_Stock;
                    //else
                    //    dr["Valuated_Stock"] = DBNull.Value;


                    dt.Rows.Add(dr);
                }
                up_PurchaseOrderBEErrorList = null;

                SqlConnection sqlConn = new SqlConnection(DBConnection.Connection);
                sqlConn.Open();

                SqlBulkCopy oSqlBulkCopy = new SqlBulkCopy(sqlConn, SqlBulkCopyOptions.TableLock, null);
                oSqlBulkCopy.DestinationTableName = "UP_ErrorPurchaseOrderDetails";

                oSqlBulkCopy.ColumnMappings.Add("FolderDownloadID", "FolderDownloadID");
                oSqlBulkCopy.ColumnMappings.Add("ManualFileUploadID", "ManualFileUploadID");
                oSqlBulkCopy.ColumnMappings.Add("ErrorDescription", "ErrorDescription");
                oSqlBulkCopy.ColumnMappings.Add("UpdatedDate", "UpdatedDate");


                oSqlBulkCopy.BulkCopyTimeout = 300;
                oSqlBulkCopy.BatchSize = dt.Rows.Count;

                oSqlBulkCopy.WriteToServer(dt);
                oSqlBulkCopy.Close();
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }


        }

        public List<Up_PurchaseOrderDetailBE> GetPurchaseOrderInquiryDAL(Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE)
        {
            List<Up_PurchaseOrderDetailBE> Up_PurchaseOrderDetailBEList = new List<Up_PurchaseOrderDetailBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[6];
                param[index++] = new SqlParameter("@Action", oUp_PurchaseOrderDetailBE.Action);
                param[index++] = new SqlParameter("@Purchase_order", oUp_PurchaseOrderDetailBE.Purchase_order);
                param[index++] = new SqlParameter("@SKU", oUp_PurchaseOrderDetailBE.SKU.Direct_SKU);
                param[index++] = new SqlParameter("@VendorId", oUp_PurchaseOrderDetailBE.Vendor.VendorID);
                param[index++] = new SqlParameter("@SiteId", oUp_PurchaseOrderDetailBE.Site.SiteID);
                param[index++] = new SqlParameter("@Status", oUp_PurchaseOrderDetailBE.Status);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spUP_PurchaseOrderDetails", param);
                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    Up_PurchaseOrderDetailBE oNewUp_PurchaseOrderDetailBEList = new Up_PurchaseOrderDetailBE();
                    oNewUp_PurchaseOrderDetailBEList.Purchase_order = Convert.ToString(dr["PO"]);
                    oNewUp_PurchaseOrderDetailBEList.Vendor_No = Convert.ToString(dr["Vendor"]);
                    oNewUp_PurchaseOrderDetailBEList.Vendor = new UP_VendorBE();
                    oNewUp_PurchaseOrderDetailBEList.Vendor.VendorID = dr["VendorID"] != DBNull.Value ? Convert.ToInt32(dr["VendorID"].ToString()) : 0;
                    oNewUp_PurchaseOrderDetailBEList.Status = Convert.ToString(dr["Status"]);
                    oNewUp_PurchaseOrderDetailBEList.Line_No = dr["OpenLines"] != DBNull.Value ? Convert.ToInt32(dr["OpenLines"].ToString()) : 0;
                    oNewUp_PurchaseOrderDetailBEList.Expected_date = dr["ExpectedDueDate"] != DBNull.Value ? Convert.ToDateTime(dr["ExpectedDueDate"]) : (DateTime?)null;
                    oNewUp_PurchaseOrderDetailBEList.Site = new BusinessEntities.ModuleBE.AdminFunctions.MAS_SiteBE();
                    oNewUp_PurchaseOrderDetailBEList.Site.SiteName = Convert.ToString(dr["SiteName"]);
                    oNewUp_PurchaseOrderDetailBEList.Site.SiteID = dr["SiteID"] != DBNull.Value ? Convert.ToInt32(dr["SiteID"].ToString()) : 0;
                    Up_PurchaseOrderDetailBEList.Add(oNewUp_PurchaseOrderDetailBEList);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return Up_PurchaseOrderDetailBEList;
        }

        public List<Up_PurchaseOrderDetailBE> GetPurchaseOrderInquiryMainDAL(Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE)
        {
            List<Up_PurchaseOrderDetailBE> Up_PurchaseOrderDetailBEList = new List<Up_PurchaseOrderDetailBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[6];
                param[index++] = new SqlParameter("@Action", oUp_PurchaseOrderDetailBE.Action);
                param[index++] = new SqlParameter("@Purchase_order", oUp_PurchaseOrderDetailBE.Purchase_order);
                param[index++] = new SqlParameter("@VendorId", oUp_PurchaseOrderDetailBE.Vendor.VendorID);
                param[index++] = new SqlParameter("@SiteId", oUp_PurchaseOrderDetailBE.Site.SiteID);
                param[index++] = new SqlParameter("@Status", oUp_PurchaseOrderDetailBE.Status);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spUP_PurchaseOrderDetails", param);
                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    Up_PurchaseOrderDetailBE oNewUp_PurchaseOrderDetailBEList = new Up_PurchaseOrderDetailBE();
                    oNewUp_PurchaseOrderDetailBEList.Purchase_order = Convert.ToString(dr["PO"]);
                    oNewUp_PurchaseOrderDetailBEList.Vendor = new UP_VendorBE();
                    oNewUp_PurchaseOrderDetailBEList.Vendor.VendorName = Convert.ToString(dr["Vendor"]);
                    oNewUp_PurchaseOrderDetailBEList.Site = new BusinessEntities.ModuleBE.AdminFunctions.MAS_SiteBE();
                    oNewUp_PurchaseOrderDetailBEList.Site.SiteName = Convert.ToString(dr["SiteName"]);
                    oNewUp_PurchaseOrderDetailBEList.Status = Convert.ToString(dr["Status"]);
                    oNewUp_PurchaseOrderDetailBEList.Original_due_date = dr["OriginalDueDate"] != DBNull.Value ? Convert.ToDateTime(dr["OriginalDueDate"]) : (DateTime?)null;
                    oNewUp_PurchaseOrderDetailBEList.Buyer_no = Convert.ToString(dr["Buyer"]);
                    oNewUp_PurchaseOrderDetailBEList.Order_raised = dr["OrderCreated"] != DBNull.Value ? Convert.ToDateTime(dr["OrderCreated"]) : (DateTime?)null;
                    Up_PurchaseOrderDetailBEList.Add(oNewUp_PurchaseOrderDetailBEList);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return Up_PurchaseOrderDetailBEList;
        }

        public List<Up_PurchaseOrderDetailBE> GetPurchaseOrderInquiryDetailsDAL(Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE)
        {
            List<Up_PurchaseOrderDetailBE> Up_PurchaseOrderDetailBEList = new List<Up_PurchaseOrderDetailBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[6];
                param[index++] = new SqlParameter("@Action", oUp_PurchaseOrderDetailBE.Action);
                param[index++] = new SqlParameter("@Purchase_order", oUp_PurchaseOrderDetailBE.Purchase_order);
                param[index++] = new SqlParameter("@VendorId", oUp_PurchaseOrderDetailBE.Vendor.VendorID);
                param[index++] = new SqlParameter("@SiteId", oUp_PurchaseOrderDetailBE.Site.SiteID);
                param[index++] = new SqlParameter("@Status", oUp_PurchaseOrderDetailBE.Status); 

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spUP_PurchaseOrderDetails", param);
                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    Up_PurchaseOrderDetailBE oNewUp_PurchaseOrderDetailBEList = new Up_PurchaseOrderDetailBE();
                    oNewUp_PurchaseOrderDetailBEList.Line_No = dr["LineNo"] != DBNull.Value ? Convert.ToInt32(dr["LineNo"].ToString()) : 0;
                    oNewUp_PurchaseOrderDetailBEList.OD_Code = Convert.ToString(dr["ODSku"]);
                    oNewUp_PurchaseOrderDetailBEList.Direct_code = Convert.ToString(dr["VikingSKU"]);
                    oNewUp_PurchaseOrderDetailBEList.Vendor_Code = Convert.ToString(dr["VendorCode"]);
                    oNewUp_PurchaseOrderDetailBEList.Product_description = Convert.ToString(dr["Description"]);
                    oNewUp_PurchaseOrderDetailBEList.Original_quantity = Convert.ToString(dr["OriginalQty"]);
                    oNewUp_PurchaseOrderDetailBEList.Outstanding_Qty = Convert.ToString(dr["OutstandingQty"]);
                    oNewUp_PurchaseOrderDetailBEList.Expected_date = dr["ExpectedDate"] != DBNull.Value ? Convert.ToDateTime(dr["ExpectedDate"]) : (DateTime?)null;
                    oNewUp_PurchaseOrderDetailBEList.LastReceiptDate = dr["LastReceipt"] != DBNull.Value ? Convert.ToDateTime(dr["LastReceipt"]) : (DateTime?)null;
                    Up_PurchaseOrderDetailBEList.Add(oNewUp_PurchaseOrderDetailBEList);
                 }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return Up_PurchaseOrderDetailBEList;
        }

        public List<APPBOK_BookingBE> GetPurchaseOrderInquiryBookingDetailsDAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            List<APPBOK_BookingBE> lstAPPBOK_BookingBE = new List<APPBOK_BookingBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[4];
                param[index++] = new SqlParameter("@Action", oAPPBOK_BookingBE.Action);
                param[index++] = new SqlParameter("@Purchase_order", oAPPBOK_BookingBE.PurchaseOrders);
                param[index++] = new SqlParameter("@VendorId", oAPPBOK_BookingBE.VendorID);
                param[index++] = new SqlParameter("@SiteId", oAPPBOK_BookingBE.SiteId);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spUP_PurchaseOrderDetails", param);
                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    APPBOK_BookingBE appbok_BookingBE = new APPBOK_BookingBE();
                    appbok_BookingBE.ScheduleDate = dr["Date"] != DBNull.Value ? Convert.ToDateTime(dr["Date"]) : (DateTime?)null;
                    appbok_BookingBE.SlotTime=new BusinessEntities.ModuleBE.AdminFunctions.SYS_SlotTimeBE();
                    appbok_BookingBE.SlotTime.SlotTime = Convert.ToString(dr["Time"]);
                    appbok_BookingBE.BookingStatus = Convert.ToString(dr["Status"]);
                    appbok_BookingBE.BookingRef = Convert.ToString(dr["BookingRef"]);
                    appbok_BookingBE.BookingID = dr["BookingID"] != DBNull.Value ? Convert.ToInt32(dr["BookingID"].ToString()) : 0;
                    appbok_BookingBE.NumberOfPallet = dr["Pallets"] != DBNull.Value ? Convert.ToInt32(dr["Pallets"].ToString()) : 0;
                    appbok_BookingBE.NumberOfCartons = dr["Cartons"] != DBNull.Value ? Convert.ToInt32(dr["Cartons"].ToString()) : 0;
                    appbok_BookingBE.NumberOfLines = dr["Lines"] != DBNull.Value ? Convert.ToInt32(dr["Lines"].ToString()) : 0;
                    appbok_BookingBE.Carrier = new BusinessEntities.ModuleBE.Appointment.CountrySetting.MASCNT_CarrierBE();
                    appbok_BookingBE.Carrier.CarrierName = Convert.ToString(dr["Carrier"]);
                    appbok_BookingBE.SupplierType = Convert.ToString(dr["SupplierType"]);
                    appbok_BookingBE.SiteId = dr["SiteID"] != DBNull.Value ? Convert.ToInt32(dr["SiteID"].ToString()) : 0;
                    lstAPPBOK_BookingBE.Add(appbok_BookingBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return lstAPPBOK_BookingBE;
        }

        public List<DiscrepancyBE> GetPurchaseOrderInquiryDiscrepanciesDAL(DiscrepancyBE oDiscrepancyBE)
        {
            List<DiscrepancyBE> lstDiscrepancyBE = new List<DiscrepancyBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[4];
                param[index++] = new SqlParameter("@Action", oDiscrepancyBE.Action);
                param[index++] = new SqlParameter("@Purchase_order", oDiscrepancyBE.PurchaseOrder.Purchase_order);
                param[index++] = new SqlParameter("@VendorId", oDiscrepancyBE.VendorID);
                param[index++] = new SqlParameter("@SiteId", oDiscrepancyBE.POSiteID);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spUP_PurchaseOrderDetails", param);
                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    DiscrepancyBE discrepancyBE = new DiscrepancyBE();
                    discrepancyBE.DiscrepancyStatus = Convert.ToString(dr["Status"]);
                    discrepancyBE.VDRNo = Convert.ToString(dr["VDRNo"]);
                    discrepancyBE.DiscrepancyLogID = dr["DiscrepancyID"] != DBNull.Value ? Convert.ToInt32(dr["DiscrepancyID"].ToString()) : 0;
                    discrepancyBE.DiscrepancyType = Convert.ToString(dr["VDRDescription"]);
                    discrepancyBE.Username = Convert.ToString(dr["CreatedBy"]);
                    discrepancyBE.DiscrepancyLogDate = dr["VDRDate"] != DBNull.Value ? Convert.ToDateTime(dr["VDRDate"]) : (DateTime?)null;
                    discrepancyBE.UserID = dr["UserID"] != DBNull.Value ? Convert.ToInt32(dr["UserID"].ToString()) : 0;
                    discrepancyBE.DiscrepancyTypeID = dr["DiscrepancyTypeID"] != DBNull.Value ? Convert.ToInt32(dr["DiscrepancyTypeID"].ToString()) : 0;
                    lstDiscrepancyBE.Add(discrepancyBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return lstDiscrepancyBE;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessEntities.ModuleBE.Upload;
using System.Data.SqlClient;
using System.Data;
using Utilities;

namespace DataAccessLayer.ModuleDAL.Upload
{
    public class UP_ManualFileUploadDAL
    {

        public int? addEditManualUploadDAL(UP_ManualFileUploadBE oUP_ManualFileUploadBE)
        {
            int? intResult = 0;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[9];   /// --- Added on 19 Feb 2011 ---
                param[index++] = new SqlParameter("@Action", oUP_ManualFileUploadBE.Action);
                param[index++] = new SqlParameter("@DateUploaded", oUP_ManualFileUploadBE.DateUploaded);
                param[index++] = new SqlParameter("@DownloadedFilename", oUP_ManualFileUploadBE.DownloadedFilename);
                param[index++] = new SqlParameter("@UserID", oUP_ManualFileUploadBE.UserID);
                param[index++] = new SqlParameter("@TotalPORecord", oUP_ManualFileUploadBE.TotalPORecord);
                param[index++] = new SqlParameter("@TotalReceiptInformationRecord", oUP_ManualFileUploadBE.TotalReceiptInformationRecord);
                param[index++] = new SqlParameter("@TotalSKURecord", oUP_ManualFileUploadBE.TotalSKURecord);
                param[index++] = new SqlParameter("@TotalVendorRecord", oUP_ManualFileUploadBE.TotalVendorRecord);
                param[index++] = new SqlParameter("@UploadType", "MANUAL");   /// --- Added on 19 Feb 2011 ---


                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spUP_ManualFileUpload", param);

                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
                else
                    intResult = 0;

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public List<UP_ManualFileUploadBE> GetManualUploadDAL(UP_ManualFileUploadBE oUP_ManualFileUploadBE)
        {
            List<UP_ManualFileUploadBE> UP_ManualFileUploadBEList = new List<UP_ManualFileUploadBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[4];
                param[index++] = new SqlParameter("@Action", oUP_ManualFileUploadBE.Action);


                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spUP_ManualFileUpload", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    UP_ManualFileUploadBE oNewUP_ManualFileUploadBEList = new UP_ManualFileUploadBE();
                    oNewUP_ManualFileUploadBEList.ManualFileUploadID = dr["ManualFileUploadID"] != DBNull.Value ? Convert.ToInt32(dr["ManualFileUploadID"]) : (int?)null;
                    oNewUP_ManualFileUploadBEList.DateUploaded = dr["DateUploaded"] != DBNull.Value ? Convert.ToDateTime(dr["DateUploaded"]) : (DateTime?)null;
                    oNewUP_ManualFileUploadBEList.DownloadedFilename = dr["DownloadedFilename"].ToString();
                    oNewUP_ManualFileUploadBEList.UserName = dr["UserName"].ToString();
                    oNewUP_ManualFileUploadBEList.TotalPORecord = dr["TotalPORecord"] != DBNull.Value ? Convert.ToInt32(dr["TotalPORecord"]) : (int?)null;
                    oNewUP_ManualFileUploadBEList.TotalReceiptInformationRecord = dr["TotalReceiptInformationRecord"] != DBNull.Value ? Convert.ToInt32(dr["TotalReceiptInformationRecord"]) : (int?)null;
                    oNewUP_ManualFileUploadBEList.TotalSKURecord = dr["TotalSKURecord"] != DBNull.Value ? Convert.ToInt32(dr["TotalSKURecord"]) : (int?)null;
                    oNewUP_ManualFileUploadBEList.TotalVendorRecord = dr["TotalVendorRecord"] != DBNull.Value ? Convert.ToInt32(dr["TotalVendorRecord"].ToString()) : (int?)null;
                    oNewUP_ManualFileUploadBEList.ErrorPOCount = dr["ErrorPOCount"] != DBNull.Value ? Convert.ToInt32(dr["ErrorPOCount"]) : (int?)null;
                    oNewUP_ManualFileUploadBEList.ErrorReceiptCount = dr["ErrorReceiptCount"] != DBNull.Value ? Convert.ToInt32(dr["ErrorReceiptCount"]) : (int?)null;
                    oNewUP_ManualFileUploadBEList.ErrorSKUCount = dr["ErrorSKUCount"] != DBNull.Value ? Convert.ToInt32(dr["ErrorSKUCount"]) : (int?)null;
                    oNewUP_ManualFileUploadBEList.ErrorVendorCount = dr["ErrorVendorCount"] != DBNull.Value ? Convert.ToInt32(dr["ErrorVendorCount"]) : (int?)null;
                    oNewUP_ManualFileUploadBEList.TransactionDate = dr["TransactionDate"] != DBNull.Value ? Convert.ToDateTime(dr["TransactionDate"]) : (DateTime?)null;
                    UP_ManualFileUploadBEList.Add(oNewUP_ManualFileUploadBEList);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return UP_ManualFileUploadBEList;
        }

    }
}

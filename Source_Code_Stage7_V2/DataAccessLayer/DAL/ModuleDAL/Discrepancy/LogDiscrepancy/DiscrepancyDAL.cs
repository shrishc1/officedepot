﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using System.Data;

using Utilities;
using BusinessEntities.ModuleBE.Discrepancy.LogDiscrepancy;


namespace DataAccessLayer.ModuleDAL.Discrepancy.LogDiscrepancy
{
    public class DiscrepancyDAL
    {

        /// <summary>
        /// Used for checking valid purchase order number
        /// </summary>
        /// <param name="oDiscrepancyBE"></param>
        /// <returns></returns>
        public List<DiscrepancyBE> CheckValidPurchaseOrderDAL(DiscrepancyBE oDiscrepancyBE)
        {
            List<DiscrepancyBE> DiscrepancyBEList = new List<DiscrepancyBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[3];
                param[index++] = new SqlParameter("@Action", oDiscrepancyBE.Action);
                param[index++] = new SqlParameter("@SiteID", oDiscrepancyBE.SiteID);
                param[index++] = new SqlParameter("@Purchase_order", oDiscrepancyBE.PurchaseOrderNumber);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spLogDiscrepancy", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    DiscrepancyBE oNewDiscrepancyBE = new DiscrepancyBE();

                    oNewDiscrepancyBE.PurchaseOrderNumber = dr["Purchase_order"].ToString();

                    DiscrepancyBEList.Add(oNewDiscrepancyBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return DiscrepancyBEList;
        }

        public List<DiscrepancyBE> GetPurchaseOrderDateListDAL(DiscrepancyBE oDiscrepancyBE)
        {
            List<DiscrepancyBE> DiscrepancyBEList = new List<DiscrepancyBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[3];
                param[index++] = new SqlParameter("@Action", oDiscrepancyBE.Action);
                param[index++] = new SqlParameter("@SiteID", oDiscrepancyBE.SiteID);
                param[index++] = new SqlParameter("@Purchase_order", oDiscrepancyBE.PurchaseOrderNumber);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spLogDiscrepancy", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    DiscrepancyBE oNewDiscrepancyBE = new DiscrepancyBE();
                    oNewDiscrepancyBE.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();
                    oNewDiscrepancyBE.PurchaseOrder = new BusinessEntities.ModuleBE.Upload.Up_PurchaseOrderDetailBE();
                    oNewDiscrepancyBE.PurchaseOrder_Order_raised = dr["Order_raised"].ToString();
                    oNewDiscrepancyBE.VendorNoName = dr["VendorNo"].ToString();
                    oNewDiscrepancyBE.VendorID = Convert.ToInt32(dr["VendorID"]);
                    if (dr.Table.Columns.Contains("RowNumber"))
                        oNewDiscrepancyBE.RowNumber = Convert.ToInt32(dr["RowNumber"]);
                    DiscrepancyBEList.Add(oNewDiscrepancyBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return DiscrepancyBEList;
        }

        /// <summary>
        /// Used for getting Vendor Contact details and Purcahse order details
        /// </summary>
        /// <param name="oDiscrepancyBE"></param>
        /// <returns></returns>
        public List<DiscrepancyBE> GetContactPurchaseOrderDetailsDAL(DiscrepancyBE oDiscrepancyBE)
        {
            List<DiscrepancyBE> DiscrepancyBEList = new List<DiscrepancyBE>();

            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[4];
                param[index++] = new SqlParameter("@Action", oDiscrepancyBE.Action);
                param[index++] = new SqlParameter("@Purchase_order", oDiscrepancyBE.PurchaseOrderNumber);
                param[index++] = new SqlParameter("@Order_raised", oDiscrepancyBE.PurchaseOrder.Order_raised);
                param[index++] = new SqlParameter("@SiteID", oDiscrepancyBE.SiteID);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spLogDiscrepancy", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    DiscrepancyBE oNewDiscrepancyBE = new DiscrepancyBE();
                    oNewDiscrepancyBE.PurchaseOrderID = Convert.ToInt32(dr["PurchaseOrderID"].ToString());
                    oNewDiscrepancyBE.PurchaseOrderNumber = dr["Purchase_order"] != DBNull.Value ? dr["Purchase_order"].ToString() : "";

                    //Vendor Detail
                    oNewDiscrepancyBE.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();
                    oNewDiscrepancyBE.VendorID = dr["VendorID"] != DBNull.Value ? Convert.ToInt32(dr["VendorID"]) : (int?)null;
                    oNewDiscrepancyBE.VendorNoName = dr["VendorNo"] != DBNull.Value ? dr["VendorNo"].ToString() : "";
                    oNewDiscrepancyBE.Vendor.VendorContactNumber = dr["VendorContactNumber"] != DBNull.Value ? dr["VendorContactNumber"].ToString() : "";
                    oNewDiscrepancyBE.Vendor.VendorContactFax = dr["VendorFax"] != DBNull.Value ? dr["VendorFax"].ToString() : "";
                    oNewDiscrepancyBE.Vendor.VendorContactEmail = dr["VendorEmail"] != DBNull.Value ? dr["VendorEmail"].ToString() : "";

                    //Stock Planner Detail
                    oNewDiscrepancyBE.StockPlannerID = dr["StockPlannerID"] != DBNull.Value ? Convert.ToInt32(dr["StockPlannerID"]) : (int?)null;
                    oNewDiscrepancyBE.StockPlannerNO = dr["StockPlannerNumber"] != DBNull.Value ? dr["StockPlannerNumber"].ToString() : "";
                    oNewDiscrepancyBE.StockPlannerContact = dr["StockPlannerContact"] != DBNull.Value ? dr["StockPlannerContact"].ToString() : "";

                    //Purchase Order Detail
                    oNewDiscrepancyBE.PurchaseOrder = new BusinessEntities.ModuleBE.Upload.Up_PurchaseOrderDetailBE();
                    oNewDiscrepancyBE.PurchaseOrder.PurchaseOrderID = Convert.ToInt32(dr["PurchaseOrderID"].ToString());
                    oNewDiscrepancyBE.PurchaseOrder.Line_No = dr["Line_No"] != DBNull.Value ? Convert.ToInt32(dr["Line_No"].ToString()) : (int?)null;
                    oNewDiscrepancyBE.PurchaseOrder.Direct_code = dr["Direct_code"] != DBNull.Value ? dr["Direct_code"].ToString() : "";
                    oNewDiscrepancyBE.PurchaseOrder.OD_Code = dr["OD_Code"] != DBNull.Value ? dr["OD_Code"].ToString() : "";
                    oNewDiscrepancyBE.PurchaseOrder.Vendor_Code = dr["Vendor_Code"] != DBNull.Value ? dr["Vendor_Code"].ToString() : "";
                    oNewDiscrepancyBE.PurchaseOrder.ProductDescription = dr["Product_description"] != DBNull.Value ? dr["Product_description"].ToString() : "";
                    oNewDiscrepancyBE.PurchaseOrder.UOM = dr["UOM"] != DBNull.Value ? dr["UOM"].ToString() : "";
                    oNewDiscrepancyBE.PurchaseOrder.Outstanding_Qty = dr["Outstanding_Qty"] != DBNull.Value ? dr["Outstanding_Qty"].ToString() : "";
                    oNewDiscrepancyBE.PurchaseOrder.Original_quantity = dr["Original_quantity"] != DBNull.Value ? dr["Original_quantity"].ToString() : "";

                    if (dr.Table.Columns.Contains("StockPlannerEmailID"))
                        oNewDiscrepancyBE.StockPlannerEmailID = dr["StockPlannerEmailID"] != DBNull.Value ? dr["StockPlannerEmailID"].ToString() : string.Empty;
                    else
                        oNewDiscrepancyBE.StockPlannerEmailID = string.Empty;

                    DiscrepancyBEList.Add(oNewDiscrepancyBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return DiscrepancyBEList;
        }

        public string addDiscrepancyLogDAL(DiscrepancyBE oDiscrepancyBE)
        {
            string sResult = "0";
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[32];
                param[index++] = new SqlParameter("@Action", oDiscrepancyBE.Action);
                param[index++] = new SqlParameter("@SiteID", oDiscrepancyBE.SiteID);
                param[index++] = new SqlParameter("@UserID", oDiscrepancyBE.User.UserID);
                param[index++] = new SqlParameter("@POSiteID", oDiscrepancyBE.POSiteID);
                param[index++] = new SqlParameter("@StockPlannerID", oDiscrepancyBE.StockPlannerID);
                param[index++] = new SqlParameter("@DiscrepancyTypeID", oDiscrepancyBE.DiscrepancyTypeID);
                param[index++] = new SqlParameter("@DiscrepancyLogDate", oDiscrepancyBE.DiscrepancyLogDate);
                param[index++] = new SqlParameter("@PurchaseOrderNumberID", oDiscrepancyBE.PurchaseOrderID);
                param[index++] = new SqlParameter("@Purchase_order", oDiscrepancyBE.PurchaseOrderNumber);
                param[index++] = new SqlParameter("@PurchaseOrderDate", oDiscrepancyBE.PurchaseOrderDate);
                param[index++] = new SqlParameter("@InternalComments", oDiscrepancyBE.InternalComments);
                param[index++] = new SqlParameter("@DeliveryNoteNumber", oDiscrepancyBE.DeliveryNoteNumber);
                param[index++] = new SqlParameter("@DeliveryArrivedDate", oDiscrepancyBE.DeliveryArrivedDate);
                param[index++] = new SqlParameter("@CommunicationType", oDiscrepancyBE.CommunicationType);
                param[index++] = new SqlParameter("@CommunicationTo", oDiscrepancyBE.CommunicationTo);
                param[index++] = new SqlParameter("@VendorID", oDiscrepancyBE.VendorID);
                param[index++] = new SqlParameter("@CarrierID", oDiscrepancyBE.CarrierID);

                param[index++] = new SqlParameter("@PersentationIssueComment", oDiscrepancyBE.PersentationIssueComment);
                param[index++] = new SqlParameter("@QualityIssueComment", oDiscrepancyBE.QualityIssueComment);
                param[index++] = new SqlParameter("@FailPalletSpecificationComment", oDiscrepancyBE.FailPalletSpecificationComment);
                param[index++] = new SqlParameter("@PrematureReceiptInvoiceComment", oDiscrepancyBE.PrematureReceiptInvoiceComment);
                param[index++] = new SqlParameter("@GenericDiscrepancyActionRequired", oDiscrepancyBE.GenericDiscrepancyActionRequired);
                param[index++] = new SqlParameter("@OfficeDepotContactName", oDiscrepancyBE.OfficeDepotContactName);
                param[index++] = new SqlParameter("@OfficeDepotContactPhone", oDiscrepancyBE.OfficeDepotContactPhone);
                param[index++] = new SqlParameter("@Freight_Charges", oDiscrepancyBE.Freight_Charges);
                param[index++] = new SqlParameter("@NumberOfPallets", oDiscrepancyBE.NumberOfPallets);
                param[index++] = new SqlParameter("@NoEscalation", oDiscrepancyBE.NoEscalation);
                param[index++] = new SqlParameter("@NoPaperworkComment", oDiscrepancyBE.NoPaperworkComment);
                param[index++] = new SqlParameter("@GenericDiscrepancyIssueComment", oDiscrepancyBE.GenericDiscrepancyIssueComment);
                param[index++] = new SqlParameter("@PickersName", oDiscrepancyBE.PickersName);
                
                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spLogDiscrepancy", param);

                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    sResult = Result.Tables[0].Rows[0][0].ToString();
                else
                    sResult = "0";

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return sResult;
        }

        public int? addDiscrepancyItemDAL(DiscrepancyBE oDiscrepancyBE)
        {
            int? intResult = 0;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[28];
                param[index++] = new SqlParameter("@Action", oDiscrepancyBE.Action);
                param[index++] = new SqlParameter("@DiscrepancyLogID", oDiscrepancyBE.DiscrepancyLogID);
                param[index++] = new SqlParameter("@PurchaseOrderNumberID", oDiscrepancyBE.PurchaseOrderID);
                param[index++] = new SqlParameter("@Line_No", oDiscrepancyBE.Line_no);
                param[index++] = new SqlParameter("@ODSKUCode", oDiscrepancyBE.ODSKUCode);
                param[index++] = new SqlParameter("@DirectCode", oDiscrepancyBE.DirectCode);
                param[index++] = new SqlParameter("@VendorCode", oDiscrepancyBE.VendorCode);
                param[index++] = new SqlParameter("@ProductDescription", oDiscrepancyBE.ProductDescription);
                param[index++] = new SqlParameter("@OriginalPOQuantity", oDiscrepancyBE.OriginalQuantity);
                param[index++] = new SqlParameter("@OutstandingQuantity", oDiscrepancyBE.OutstandingQuantity);
                param[index++] = new SqlParameter("@UOM", oDiscrepancyBE.UOM);
                param[index++] = new SqlParameter("@DeliveryNoteQuantity", oDiscrepancyBE.DeliveredNoteQuantity);
                param[index++] = new SqlParameter("@DeliveredQuantity", oDiscrepancyBE.DeliveredQuantity);
                param[index++] = new SqlParameter("@OversQuantity", oDiscrepancyBE.OversQuantity);
                param[index++] = new SqlParameter("@ShortageQuantity", oDiscrepancyBE.ShortageQuantity);
                param[index++] = new SqlParameter("@DamageQuantity", oDiscrepancyBE.DamageQuantity);
                param[index++] = new SqlParameter("@DamageDescription", oDiscrepancyBE.DamageDescription);
                param[index++] = new SqlParameter("@AmendedQuantity", oDiscrepancyBE.AmendedQuantity);
                param[index++] = new SqlParameter("@NoPOEscalate", oDiscrepancyBE.NoPOEscalate);
                param[index++] = new SqlParameter("@PackSizeOrdered", oDiscrepancyBE.PackSizeOrdered);
                param[index++] = new SqlParameter("@PackSizeReceived", oDiscrepancyBE.PackSizeReceived);
                param[index++] = new SqlParameter("@WrongPackReceived", oDiscrepancyBE.WrongPackReceived);
                param[index++] = new SqlParameter("@CorrectODSKUCode", oDiscrepancyBE.CorrectODSKUCode);
                param[index++] = new SqlParameter("@CorrectVendorCode", oDiscrepancyBE.CorrectVendorCode);
                param[index++] = new SqlParameter("@CorrectProductDescription", oDiscrepancyBE.CorrectProductDescription);
                param[index++] = new SqlParameter("@CorrectUOM", oDiscrepancyBE.CorrectUOM);
                param[index++] = new SqlParameter("@ShuttleType", oDiscrepancyBE.ShuttleType);
                param[index++] = new SqlParameter("@DiscrepancyQty", oDiscrepancyBE.DiscrepancyQty);

                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spLogDiscrepancy", param);

                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
                else
                    intResult = 0;

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public DataSet getLanguageIDforCommunicationDAL(DiscrepancyMailBE oDiscrepancyMailBE)
        {
            DataSet Result = null;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[3];
                param[index++] = new SqlParameter("@Action", oDiscrepancyMailBE.Action);
                param[index++] = new SqlParameter("@VendorID", oDiscrepancyMailBE.vendorID);
                param[index++] = new SqlParameter("@sentTo", oDiscrepancyMailBE.sentTo);

                Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spLogDiscrepancy", param);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return Result;
        }
        public DataSet GetDiscrepancyDetailDAL(DiscrepancyMailBE oDiscrepancyMailBE)
        {
            DataSet Result = null;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[3];
                param[index++] = new SqlParameter("@Action", oDiscrepancyMailBE.Action);
                param[index++] = new SqlParameter("@discrepancyLogID", oDiscrepancyMailBE.discrepancyLogID);
                param[index++] = new SqlParameter("@CommunicationLevel", oDiscrepancyMailBE.communicationLevel);

                Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spLogDiscrepancy", param);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return Result;
        }

        public DataSet GetDiscrepancyCommunicationDAL(DiscrepancyMailBE oDiscrepancyMailBE)
        {
            DataSet Result = null;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[2];
                param[index++] = new SqlParameter("@Action", oDiscrepancyMailBE.Action);
                param[index++] = new SqlParameter("@discrepancyCommunicationID", oDiscrepancyMailBE.discrepancyCommunicationID);

                Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spLogDiscrepancy", param);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return Result;
        }
        public DataSet GetDiscrepancyImagesDAL(DiscrepancyBE oDiscrepancyBE)
        {
            DataSet Result = null;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[2];
                param[index++] = new SqlParameter("@Action", oDiscrepancyBE.Action);
                param[index++] = new SqlParameter("@DiscrepancyLogID", oDiscrepancyBE.DiscrepancyLogID);

                Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spLogDiscrepancy", param);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return Result;
        }
        public int? saveDiscrepancyMailDAL(DiscrepancyMailBE oDiscrepancyMailBE)
        {
            int? Result = null;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[10];
                param[index++] = new SqlParameter("@Action", oDiscrepancyMailBE.Action);
                param[index++] = new SqlParameter("@discrepancyLogID", oDiscrepancyMailBE.discrepancyLogID);
                param[index++] = new SqlParameter("@sentTo", oDiscrepancyMailBE.sentTo);
                param[index++] = new SqlParameter("@mailBody", oDiscrepancyMailBE.mailBody);
                param[index++] = new SqlParameter("@mailSubject", oDiscrepancyMailBE.mailSubject);
                param[index++] = new SqlParameter("@sentDate", oDiscrepancyMailBE.sentDate);
                param[index++] = new SqlParameter("@languageID", oDiscrepancyMailBE.languageID);
                param[index++] = new SqlParameter("@communicationLevel", oDiscrepancyMailBE.communicationLevel);
                param[index++] = new SqlParameter("@MailSentInLanguage", oDiscrepancyMailBE.MailSentInLanguage);

                if (oDiscrepancyMailBE.QueryDiscrepancy != null)
                    param[index++] = new SqlParameter("@QueryDiscrepancyID", oDiscrepancyMailBE.QueryDiscrepancy.QueryDiscrepancyID);

                object oResult = SqlHelper.ExecuteScalar(DBConnection.Connection, CommandType.StoredProcedure, "spLogDiscrepancy", param);
                if (oResult != null)
                    Result = Convert.ToInt32(oResult);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return Result;
        }
        public int? saveDiscrepancyLetterDAL(DiscrepancyMailBE oDiscrepancyMailBE)
        {
            int? Result = null;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[5];
                param[index++] = new SqlParameter("@Action", oDiscrepancyMailBE.Action);
                param[index++] = new SqlParameter("@discrepancyLogID", oDiscrepancyMailBE.discrepancyLogID);
                param[index++] = new SqlParameter("@DiscrepancyLetterID", oDiscrepancyMailBE.DiscrepancyLetterID);
                param[index++] = new SqlParameter("@letterBody", oDiscrepancyMailBE.LetterBody);
                param[index++] = new SqlParameter("@loggedDate", oDiscrepancyMailBE.LoggedDate);

                object oResult = SqlHelper.ExecuteScalar(DBConnection.Connection, CommandType.StoredProcedure, "spLogDiscrepancy", param);
                if (oResult != null)
                    Result = Convert.ToInt32(oResult);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return Result;
        }
        public DataSet GetDiscrepancyLetterDAL(DiscrepancyMailBE oDiscrepancyMailBE)
        {
            DataSet Result = null;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[2];
                param[index++] = new SqlParameter("@Action", oDiscrepancyMailBE.Action);
                param[index++] = new SqlParameter("@DiscrepancyLetterID", oDiscrepancyMailBE.DiscrepancyLetterID);

                Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spLogDiscrepancy", param);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return Result;
        }
        public void errorLogDAL(DiscrepancyMailBE oDiscrepancyMailBE)
        {
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[8];
                param[index++] = new SqlParameter("@Action", oDiscrepancyMailBE.Action);
                param[index++] = new SqlParameter("@discrepancyLogID", oDiscrepancyMailBE.discrepancyLogID);
                param[index++] = new SqlParameter("@sentTo", oDiscrepancyMailBE.sentTo);
                param[index++] = new SqlParameter("@mailBody", oDiscrepancyMailBE.mailBody);
                param[index++] = new SqlParameter("@mailSubject", oDiscrepancyMailBE.mailSubject);
                param[index++] = new SqlParameter("@sentDate", oDiscrepancyMailBE.sentDate);
                param[index++] = new SqlParameter("@errorDescription", oDiscrepancyMailBE.errorDescription);
                param[index++] = new SqlParameter("@Status", oDiscrepancyMailBE.Status);

                object oResult = SqlHelper.ExecuteNonQuery(DBConnection.Connection, CommandType.StoredProcedure, "spLogDiscrepancy", param);
            }
            catch (Exception ex)
            {       
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
        }

        public List<DiscrepancyBE> GetODProductDetailsDAL(DiscrepancyBE oDiscrepancyBE)
        {
            List<DiscrepancyBE> DiscrepancyBEList = new List<DiscrepancyBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[3];
                param[index++] = new SqlParameter("@Action", oDiscrepancyBE.Action);
                param[index++] = new SqlParameter("@ODSKUCode", oDiscrepancyBE.ODSKUCode);
                param[index++] = new SqlParameter("@SiteId", oDiscrepancyBE.SiteID);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spGetODProductDetails", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    DiscrepancyBE oNewDiscrepancyBE = new DiscrepancyBE();

                    oNewDiscrepancyBE.ODSKUCode = dr["ODSKUCode"] != DBNull.Value ? dr["ODSKUCode"].ToString() : "";
                    oNewDiscrepancyBE.DirectCode = dr["Direct_code"] != DBNull.Value ? dr["Direct_code"].ToString() : "";
                    oNewDiscrepancyBE.VendorCode = dr["Vendor_Code"] != DBNull.Value ? dr["Vendor_Code"].ToString() : "";
                    oNewDiscrepancyBE.ProductDescription = dr["Product_description"] != DBNull.Value ? dr["Product_description"].ToString() : "";
                    oNewDiscrepancyBE.UOM = dr["UOM"] != DBNull.Value ? dr["UOM"].ToString() : "";
                    if (dr.Table.Columns.Contains("SiteID"))
                    oNewDiscrepancyBE.SiteID = dr["SiteID"] != DBNull.Value ? Convert.ToInt32(dr["SiteID"]) : 0;

                    DiscrepancyBEList.Add(oNewDiscrepancyBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return DiscrepancyBEList;
        }

        public List<DiscrepancyBE> GetODProductDetailsBySiteVendorDAL(DiscrepancyBE oDiscrepancyBE)
        {
            List<DiscrepancyBE> DiscrepancyBEList = new List<DiscrepancyBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[4];
                param[index++] = new SqlParameter("@Action", oDiscrepancyBE.Action);
                param[index++] = new SqlParameter("@ODSKUCode", oDiscrepancyBE.ODSKUCode);
                param[index++] = new SqlParameter("@SiteId", oDiscrepancyBE.SiteID);
                param[index++] = new SqlParameter("@VendorId", oDiscrepancyBE.VendorID);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spGetODProductDetails", param);
                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    DiscrepancyBE oNewDiscrepancyBE = new DiscrepancyBE();

                    oNewDiscrepancyBE.ODSKUCode = dr["ODSKUCode"] != DBNull.Value ? dr["ODSKUCode"].ToString() : "";
                    oNewDiscrepancyBE.DirectCode = dr["Direct_code"] != DBNull.Value ? dr["Direct_code"].ToString() : "";
                    oNewDiscrepancyBE.VendorCode = dr["Vendor_Code"] != DBNull.Value ? dr["Vendor_Code"].ToString() : "";
                    oNewDiscrepancyBE.ProductDescription = dr["Product_description"] != DBNull.Value ? dr["Product_description"].ToString() : "";
                    oNewDiscrepancyBE.UOM = dr["UOM"] != DBNull.Value ? dr["UOM"].ToString() : "";

                    DiscrepancyBEList.Add(oNewDiscrepancyBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return DiscrepancyBEList;
        }

        public List<DiscrepancyBE> GetDiscrepancyLogDetailsDAL(DiscrepancyBE oDiscrepancyBE)
        {
            List<DiscrepancyBE> DiscrepancyBEList = new List<DiscrepancyBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[20];
                param[index++] = new SqlParameter("@Action", oDiscrepancyBE.Action);
                param[index++] = new SqlParameter("@UserID", oDiscrepancyBE.UserID);
                param[index++] = new SqlParameter("@DiscrepancyLogID", oDiscrepancyBE.DiscrepancyLogID);
                param[index++] = new SqlParameter("@SCDeliveryNoteNumber", oDiscrepancyBE.SCDeliveryNoteNumber);
                param[index++] = new SqlParameter("@SCDiscrepancyDateFrom", oDiscrepancyBE.SCDiscrepancyDateFrom);
                param[index++] = new SqlParameter("@SCDiscrepancyDateTo", oDiscrepancyBE.SCDiscrepancyDateTo);
                param[index++] = new SqlParameter("@SCDiscrepancyTypeID", oDiscrepancyBE.SCDiscrepancyTypeID);
                param[index++] = new SqlParameter("@SCDiscrepancyNo", oDiscrepancyBE.SCDiscrepancyNo);
                param[index++] = new SqlParameter("@SCPurchaseOrderDate", oDiscrepancyBE.SCPurchaseOrderDate);
                param[index++] = new SqlParameter("@SCPurchaseOrderNo", oDiscrepancyBE.SCPurchaseOrderNo);
                param[index++] = new SqlParameter("@SCSelectedSiteIDs", oDiscrepancyBE.SCSelectedSiteIDs);
                param[index++] = new SqlParameter("@SCSelectedVendorIDs", oDiscrepancyBE.SCSelectedVendorIDs);
                param[index++] = new SqlParameter("@SCSPUserID", oDiscrepancyBE.SCSPUserID);
                param[index++] = new SqlParameter("@DiscrepancyStatus", oDiscrepancyBE.DiscrepancyStatus);
                param[index++] = new SqlParameter("@VendorID", oDiscrepancyBE.VendorID);
                
                if (oDiscrepancyBE.User != null)
                    param[index++] = new SqlParameter("@RoleTypeFlag", oDiscrepancyBE.User.RoleTypeFlag);

                if (oDiscrepancyBE.QueryDiscrepancy != null)
                    param[index++] = new SqlParameter("@Query", oDiscrepancyBE.QueryDiscrepancy.Query);

                if (oDiscrepancyBE.UserIdForConsVendors != null)
                    param[index++] = new SqlParameter("@UserIdForConsVendors", oDiscrepancyBE.UserIdForConsVendors);

                if ( ! string.IsNullOrEmpty(oDiscrepancyBE.EscalationLevel))
                    param[index++] = new SqlParameter("@EscalationLevel", oDiscrepancyBE.EscalationLevel);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spLogDiscrepancy", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    DiscrepancyBE oNewDiscrepancyBE = new DiscrepancyBE();

                    oNewDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(dr["DiscrepancyLogID"].ToString());
                    oNewDiscrepancyBE.VDRNo = dr["VDRNo"] != DBNull.Value ? dr["VDRNo"].ToString() : "";

                    oNewDiscrepancyBE.Site = new BusinessEntities.ModuleBE.AdminFunctions.MAS_SiteBE();
                    oNewDiscrepancyBE.Site.SiteName = dr["SiteName"] != DBNull.Value ? dr["SiteName"].ToString() : "";
                    oNewDiscrepancyBE.Site.SiteID = dr["SiteID"] != DBNull.Value ? Convert.ToInt32(dr["SiteID"].ToString()) : 0;
                    oNewDiscrepancyBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
                    oNewDiscrepancyBE.User.FirstName = dr["FirstName"] != DBNull.Value ? dr["FirstName"].ToString() : "";
                    oNewDiscrepancyBE.User.PhoneNumber = dr["PhoneNumber"] != DBNull.Value ? dr["PhoneNumber"].ToString() : "";
                    oNewDiscrepancyBE.StockPlannerNO = dr["StockPlanner"] != DBNull.Value ? dr["StockPlanner"].ToString() : "";
                    oNewDiscrepancyBE.StockPlannerContact = dr["StockContactNo"] != DBNull.Value ? dr["StockContactNo"].ToString() : "";
                    oNewDiscrepancyBE.StockPlannerName = dr["StockPlannerName"] != DBNull.Value ? dr["StockPlannerName"].ToString() : "";
                    oNewDiscrepancyBE.DiscrepancyTypeID = dr["DiscrepancyTypeID"] != DBNull.Value ? Convert.ToInt32(dr["DiscrepancyTypeID"].ToString()) : (int?)null;
                    oNewDiscrepancyBE.ProductDescription = dr["DiscrepancyDiscripton"] != DBNull.Value ? dr["DiscrepancyDiscripton"].ToString() : "";
                    oNewDiscrepancyBE.DiscrepancyLogDate = dr["DiscrepancyLogDate"] != DBNull.Value ? Convert.ToDateTime(dr["DiscrepancyLogDate"].ToString()) : (DateTime?)null;
                    oNewDiscrepancyBE.DiscrepancyStatus = dr["DiscrepancyStatus"] != DBNull.Value ? dr["DiscrepancyStatus"].ToString() : "";
                    oNewDiscrepancyBE.DiscrepancyStatusDiscreption = dr["DiscrepancyStatusDiscreption"] != DBNull.Value ? dr["DiscrepancyStatusDiscreption"].ToString() : "";
                    oNewDiscrepancyBE.PurchaseOrderNumber = dr["PurchaseOrderNumber"] != DBNull.Value ? dr["PurchaseOrderNumber"].ToString() : "";
                    oNewDiscrepancyBE.PurchaseOrderDate = dr["PurchaseOrderDate"] != DBNull.Value ? Convert.ToDateTime(dr["PurchaseOrderDate"].ToString()) : (DateTime?)null;
                    oNewDiscrepancyBE.InternalComments = dr["InternalComments"] != DBNull.Value ? dr["InternalComments"].ToString() : "";
                    oNewDiscrepancyBE.DeliveryNoteNumber = dr["DeliveryNoteNumber"] != DBNull.Value ? dr["DeliveryNoteNumber"].ToString() : "";
                    oNewDiscrepancyBE.DeliveryArrivedDate = dr["DeliveryArrivedDate"] != DBNull.Value ? Convert.ToDateTime(dr["DeliveryArrivedDate"].ToString()) : (DateTime?)null;
                    oNewDiscrepancyBE.CommunicationType = dr["CommunicationType"] != DBNull.Value ? Convert.ToChar(dr["CommunicationType"]) : (char?)null;
                    oNewDiscrepancyBE.CommunicationTo = dr["CommunicationTo"] != DBNull.Value ? dr["CommunicationTo"].ToString() : "";
                    oNewDiscrepancyBE.VendorNoName = dr["VendorNoName"] != DBNull.Value ? dr["VendorNoName"].ToString() : "";
                    oNewDiscrepancyBE.VendorID = dr["VendorID"] != DBNull.Value ? Convert.ToInt16(dr["VendorID"].ToString()) : (int?)null;
                    oNewDiscrepancyBE.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();
                    oNewDiscrepancyBE.Vendor.VendorContactNumber = dr["VendorContactNumber"] != DBNull.Value ? dr["VendorContactNumber"].ToString() : "";
                    oNewDiscrepancyBE.CarrierName = dr["CarrierName"] != DBNull.Value ? dr["CarrierName"].ToString() : "";

                    oNewDiscrepancyBE.PersentationIssueComment = dr["PersentationIssueComment"] != DBNull.Value ? dr["PersentationIssueComment"].ToString() : "";
                    oNewDiscrepancyBE.QualityIssueComment = dr["QualityIssueComment"] != DBNull.Value ? dr["QualityIssueComment"].ToString() : "";
                    oNewDiscrepancyBE.FailPalletSpecificationComment = dr["FailPalletSpecificationComment"] != DBNull.Value ? dr["FailPalletSpecificationComment"].ToString() : "";
                    oNewDiscrepancyBE.PrematureReceiptInvoiceComment = dr["PrematureReceiptInvoiceComment"] != DBNull.Value ? dr["PrematureReceiptInvoiceComment"].ToString() : "";
                    oNewDiscrepancyBE.GenericDiscrepancyActionRequired = dr["GenericDiscrepancyActionRequired"] != DBNull.Value ? dr["GenericDiscrepancyActionRequired"].ToString() : "";
                    oNewDiscrepancyBE.OfficeDepotContactName = dr["OfficeDepotContactName"] != DBNull.Value ? dr["OfficeDepotContactName"].ToString() : "";
                    oNewDiscrepancyBE.OfficeDepotContactPhone = dr["OfficeDepotContactPhone"] != DBNull.Value ? dr["OfficeDepotContactPhone"].ToString() : "";
                    oNewDiscrepancyBE.SiteID = dr["SiteID"] != DBNull.Value ? Convert.ToInt32(dr["SiteID"]) : 0;
                    oNewDiscrepancyBE.POSiteID = dr["POSiteID"] != DBNull.Value ? Convert.ToInt32(dr["POSiteID"]) : 0;
                    oNewDiscrepancyBE.POSiteName = dr["POSiteName"] != DBNull.Value ? Convert.ToString(dr["POSiteName"]) : "";
                    oNewDiscrepancyBE.Freight_Charges = dr["Freight_Charges"] != DBNull.Value ? Convert.ToDecimal(dr["Freight_Charges"].ToString()) : (decimal?)null;
                    oNewDiscrepancyBE.NumberOfPallets = dr["NumberOfPallets"] != DBNull.Value ? Convert.ToInt32(dr["NumberOfPallets"].ToString()) : (int?)null;
                    oNewDiscrepancyBE.TotalLabourCost = dr["TotalLabourCost"] != DBNull.Value ? Convert.ToDecimal(dr["TotalLabourCost"].ToString()) : (decimal?)null;
                    oNewDiscrepancyBE.StockPlannerEmailID = dr["StockPlannerEmailID"] != DBNull.Value ? Convert.ToString(dr["StockPlannerEmailID"]) : null;
                    oNewDiscrepancyBE.GenericDiscrepancyIssueComment = dr["GenericDiscrepancyIssueComment"] != DBNull.Value ? dr["GenericDiscrepancyIssueComment"].ToString() : "";
                    if (oDiscrepancyBE.User != null && oDiscrepancyBE.User.RoleTypeFlag != null && !string.Equals(oDiscrepancyBE.User.RoleTypeFlag, "A"))
                    {
                        oNewDiscrepancyBE.DiscrepancyWorkFlowID = dr["DiscrepancyWorkflowID"] != DBNull.Value ? Convert.ToInt32(dr["DiscrepancyWorkflowID"]) : (int?)null;
                    }

                    if (dr.Table.Columns.Contains("GI"))
                        oNewDiscrepancyBE.GI = dr["GI"] != DBNull.Value ? Convert.ToString(dr["GI"]) : string.Empty;

                    if (dr.Table.Columns.Contains("INV"))
                        oNewDiscrepancyBE.INV = dr["INV"] != DBNull.Value ? Convert.ToString(dr["INV"]) : string.Empty;

                    if (dr.Table.Columns.Contains("AP"))
                        oNewDiscrepancyBE.AP = dr["AP"] != DBNull.Value ? Convert.ToString(dr["AP"]) : string.Empty;

                    if (dr.Table.Columns.Contains("VEN"))
                        oNewDiscrepancyBE.VEN = dr["VEN"] != DBNull.Value ? Convert.ToString(dr["VEN"]) : string.Empty;

                    if (dr.Table.Columns.Contains("PickersName"))
                        oNewDiscrepancyBE.PickersName = dr["PickersName"] != DBNull.Value ? Convert.ToString(dr["PickersName"]) : string.Empty;

                    if (dr.Table.Columns.Contains("Query"))
                    {
                        oNewDiscrepancyBE.QueryDiscrepancy = new DISLog_QueryDiscrepancyBE();
                        oNewDiscrepancyBE.QueryDiscrepancy.QueryDiscrepancyID = dr["QueryDiscrepancyID"] != DBNull.Value ? Convert.ToInt32(dr["QueryDiscrepancyID"]) : 0;
                        oNewDiscrepancyBE.QueryDiscrepancy.Query = dr["Query"] != DBNull.Value ? Convert.ToString(dr["Query"]) : string.Empty;
                    }

                    DiscrepancyBEList.Add(oNewDiscrepancyBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return DiscrepancyBEList;
        }

        public List<DiscrepancyBE> GetDiscrepancyLogSiteVendorDetailsDAL(DiscrepancyBE oDiscrepancyBE)
        {
            List<DiscrepancyBE> DiscrepancyBEList = new List<DiscrepancyBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[3];
                param[index++] = new SqlParameter("@Action", oDiscrepancyBE.Action);
                param[index++] = new SqlParameter("@DiscrepancyLogID", oDiscrepancyBE.DiscrepancyLogID);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spLogDiscrepancy", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    DiscrepancyBE oNewDiscrepancyBE = new DiscrepancyBE();

                    //oNewDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(dr["DiscrepancyLogID"].ToString());
                    //oNewDiscrepancyBE.VDRNo = dr["VDRNo"] != DBNull.Value ? dr["VDRNo"].ToString() : "";
                    oNewDiscrepancyBE.SiteID = dr["SiteID"] != DBNull.Value ? Convert.ToInt32(dr["SiteID"]) : 0;

                    oNewDiscrepancyBE.Site = new BusinessEntities.ModuleBE.AdminFunctions.MAS_SiteBE();
                    oNewDiscrepancyBE.Site.SiteID = dr["SiteID"] != DBNull.Value ? Convert.ToInt32(dr["SiteID"].ToString()) : 0;
                    oNewDiscrepancyBE.Site.SiteName = dr["SiteName"] != DBNull.Value ? dr["SiteName"].ToString() : "";
                    oNewDiscrepancyBE.Site.SiteAddressLine1 = dr["SiteAddressLine1"] != DBNull.Value ? dr["SiteAddressLine1"].ToString() : "";
                    oNewDiscrepancyBE.Site.SiteAddressLine2 = dr["SiteAddressLine2"] != DBNull.Value ? dr["SiteAddressLine2"].ToString() : "";
                    oNewDiscrepancyBE.Site.SiteAddressLine3 = dr["SiteAddressLine3"] != DBNull.Value ? dr["SiteAddressLine3"].ToString() : "";
                    oNewDiscrepancyBE.Site.SiteAddressLine4 = dr["SiteAddressLine4"] != DBNull.Value ? dr["SiteAddressLine4"].ToString() : "";
                    oNewDiscrepancyBE.Site.SiteAddressLine5 = dr["SiteAddressLine5"] != DBNull.Value ? dr["SiteAddressLine5"].ToString() : "";
                    oNewDiscrepancyBE.Site.SiteAddressLine6 = dr["SiteAddressLine6"] != DBNull.Value ? dr["SiteAddressLine6"].ToString() : "";
                    oNewDiscrepancyBE.Site.SitePincode = dr["SitePincode"] != DBNull.Value ? dr["SitePincode"].ToString() : "";

                    oNewDiscrepancyBE.Site.APAddressLine1 = dr["APAddressLine1"] != DBNull.Value ? dr["APAddressLine1"].ToString() : "";
                    oNewDiscrepancyBE.Site.APAddressLine2 = dr["APAddressLine2"] != DBNull.Value ? dr["APAddressLine2"].ToString() : "";
                    oNewDiscrepancyBE.Site.APAddressLine3 = dr["APAddressLine3"] != DBNull.Value ? dr["APAddressLine3"].ToString() : "";
                    oNewDiscrepancyBE.Site.APAddressLine4 = dr["APAddressLine4"] != DBNull.Value ? dr["APAddressLine4"].ToString() : "";
                    oNewDiscrepancyBE.Site.APAddressLine5 = dr["APAddressLine5"] != DBNull.Value ? dr["APAddressLine5"].ToString() : "";
                    oNewDiscrepancyBE.Site.APAddressLine6 = dr["APAddressLine6"] != DBNull.Value ? dr["APAddressLine6"].ToString() : "";
                    oNewDiscrepancyBE.Site.APPincode = dr["APPincode"] != DBNull.Value ? dr["APPincode"].ToString() : "";
                    oNewDiscrepancyBE.Site.SiteMangerUserID = dr["SiteMangerUserID"] != DBNull.Value ? Convert.ToInt32(dr["SiteMangerUserID"]) : (int?)null;

                    //oNewDiscrepancyBE.VendorNoName = dr["VendorNoName"] != DBNull.Value ? dr["VendorNoName"].ToString() : "";
                    oNewDiscrepancyBE.VendorID = dr["VendorID"] != DBNull.Value ? Convert.ToInt16(dr["VendorID"].ToString()) : (int?)null;
                    oNewDiscrepancyBE.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();
                    oNewDiscrepancyBE.Vendor.Vendor_No = dr["Vendor_No"] != DBNull.Value ? dr["Vendor_No"].ToString() : "";
                    oNewDiscrepancyBE.Vendor.VendorName = dr["Vendor_Name"] != DBNull.Value ? dr["Vendor_Name"].ToString() : "";
                    oNewDiscrepancyBE.Vendor.address1 = dr["address 1"] != DBNull.Value ? dr["address 1"].ToString() : "";
                    oNewDiscrepancyBE.Vendor.address2 = dr["address 2"] != DBNull.Value ? dr["address 2"].ToString() : "";
                    oNewDiscrepancyBE.Vendor.city = dr["city"] != DBNull.Value ? dr["city"].ToString() : "";
                    oNewDiscrepancyBE.Vendor.county = dr["county"] != DBNull.Value ? dr["county"].ToString() : "";
                    oNewDiscrepancyBE.Vendor.VMPPIN = dr["VMPPIN"] != DBNull.Value ? dr["VMPPIN"].ToString() : "";
                    oNewDiscrepancyBE.Vendor.VMPPOU = dr["VMPPOU"] != DBNull.Value ? dr["VMPPOU"].ToString() : "";
                    oNewDiscrepancyBE.Vendor.Fax_country_code = dr["Fax_country_code"] != DBNull.Value ? dr["Fax_country_code"].ToString() : "";
                    oNewDiscrepancyBE.Vendor.Fax_no = dr["Fax_no"] != DBNull.Value ? dr["Fax_no"].ToString() : "";
                    oNewDiscrepancyBE.Vendor.VendorContactNumber = dr["Vendor Contact Number"] != DBNull.Value ? dr["Vendor Contact Number"].ToString() : "";

                    oNewDiscrepancyBE.DiscrepancyLogDate = dr["DiscrepancyLogDate"] != DBNull.Value ? Convert.ToDateTime(dr["DiscrepancyLogDate"].ToString()) : (DateTime?)null;
                    oNewDiscrepancyBE.VDRNo = dr["VDRNo"] != DBNull.Value ? dr["VDRNo"].ToString() : "";
                    oNewDiscrepancyBE.Vendor.ReturnAddress = dr["ReturnAddress"] != DBNull.Value ? dr["ReturnAddress"].ToString() : "";
                    oNewDiscrepancyBE.DiscrepancyType = dr["DiscrepancyType"] != DBNull.Value ? dr["DiscrepancyType"].ToString() : "";
                    oNewDiscrepancyBE.DiscrepancyTypeID = dr["DiscrepancyTypeID"] != DBNull.Value ? Convert.ToInt16(dr["DiscrepancyTypeID"].ToString()) : (int?)null;
                    oNewDiscrepancyBE.Vendor.Language = dr["VendorLanguage"] != DBNull.Value ? dr["VendorLanguage"].ToString() : "";

                    if (dr.Table.Columns.Contains("PurchaseOrderNumber"))
                    {
                        oNewDiscrepancyBE.PurchaseOrder = new BusinessEntities.ModuleBE.Upload.Up_PurchaseOrderDetailBE();
                        oNewDiscrepancyBE.PurchaseOrder.Purchase_order = dr["PurchaseOrderNumber"] != DBNull.Value ? dr["PurchaseOrderNumber"].ToString() : string.Empty;
                    }

                    DiscrepancyBEList.Add(oNewDiscrepancyBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return DiscrepancyBEList;
        }

        public List<DiscrepancyBE> GetDiscrepancyWorkListForCurrentUserDAL(DiscrepancyBE oDiscrepancyBE)
        {
            List<DiscrepancyBE> DiscrepancyBEList = new List<DiscrepancyBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[5];
                param[index++] = new SqlParameter("@Action", oDiscrepancyBE.Action);
                param[index++] = new SqlParameter("@UserID", oDiscrepancyBE.User.UserID);
                param[index++] = new SqlParameter("@Role", oDiscrepancyBE.User.RoleName);
                param[index++] = new SqlParameter("@DiscrepancyLogID", oDiscrepancyBE.DiscrepancyLogID);
                param[index++] = new SqlParameter("@GreaterThan3DaysFlag", oDiscrepancyBE.GreaterThan3DaysFlag);


                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spLogDiscrepancy", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    DiscrepancyBE oNewDiscrepancyBE = new DiscrepancyBE();

                    oNewDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(dr["DiscrepancyLogID"].ToString());
                    oNewDiscrepancyBE.VDRNo = dr["VDRNo"] != DBNull.Value ? dr["VDRNo"].ToString() : "";

                    oNewDiscrepancyBE.Site = new BusinessEntities.ModuleBE.AdminFunctions.MAS_SiteBE();
                    oNewDiscrepancyBE.Site.SiteName = dr["SiteName"] != DBNull.Value ? dr["SiteName"].ToString() : "";


                    oNewDiscrepancyBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
                    oNewDiscrepancyBE.User.FirstName = dr["FirstName"] != DBNull.Value ? dr["FirstName"].ToString() : "";
                    oNewDiscrepancyBE.StockPlannerNO = dr["StockPlanner"] != DBNull.Value ? dr["StockPlanner"].ToString() : "";
                    oNewDiscrepancyBE.StockPlannerContact = dr["StockContactNo"] != DBNull.Value ? dr["StockContactNo"].ToString() : "";
                    oNewDiscrepancyBE.StockPlannerName = dr["StockPlannerName"] != DBNull.Value ? dr["StockPlannerName"].ToString() : "";
                    oNewDiscrepancyBE.DiscrepancyTypeID = dr["DiscrepancyTypeID"] != DBNull.Value ? Convert.ToInt32(dr["DiscrepancyTypeID"].ToString()) : (int?)null;
                    oNewDiscrepancyBE.ProductDescription = dr["DiscrepancyDiscripton"] != DBNull.Value ? dr["DiscrepancyDiscripton"].ToString() : "";
                    oNewDiscrepancyBE.DiscrepancyLogDate = dr["DiscrepancyLogDate"] != DBNull.Value ? Convert.ToDateTime(dr["DiscrepancyLogDate"].ToString()) : (DateTime?)null;
                    oNewDiscrepancyBE.DiscrepancyStatus = dr["DiscrepancyStatus"] != DBNull.Value ? dr["DiscrepancyStatus"].ToString() : "";
                    oNewDiscrepancyBE.PurchaseOrderNumber = dr["PurchaseOrderNumber"] != DBNull.Value ? dr["PurchaseOrderNumber"].ToString() : "";
                    oNewDiscrepancyBE.PurchaseOrderDate = dr["PurchaseOrderDate"] != DBNull.Value ? Convert.ToDateTime(dr["PurchaseOrderDate"].ToString()) : (DateTime?)null;
                    oNewDiscrepancyBE.InternalComments = dr["InternalComments"] != DBNull.Value ? dr["InternalComments"].ToString() : "";
                    oNewDiscrepancyBE.DeliveryNoteNumber = dr["DeliveryNoteNumber"] != DBNull.Value ? dr["DeliveryNoteNumber"].ToString() : "";
                    oNewDiscrepancyBE.DeliveryArrivedDate = dr["DeliveryArrivedDate"] != DBNull.Value ? Convert.ToDateTime(dr["DeliveryArrivedDate"].ToString()) : (DateTime?)null;
                    oNewDiscrepancyBE.CommunicationType = dr["CommunicationType"] != DBNull.Value ? Convert.ToChar(dr["CommunicationType"]) : (char?)null;
                    oNewDiscrepancyBE.CommunicationTo = dr["CommunicationTo"] != DBNull.Value ? dr["CommunicationTo"].ToString() : "";
                    oNewDiscrepancyBE.VendorNoName = dr["VendorNoName"] != DBNull.Value ? dr["VendorNoName"].ToString() : "";
                    oNewDiscrepancyBE.VendorID = dr["VendorID"] != DBNull.Value ? Convert.ToInt16(dr["VendorID"].ToString()) : (int?)null;
                    oNewDiscrepancyBE.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();
                    oNewDiscrepancyBE.Vendor.VendorContactNumber = dr["VendorContactNumber"] != DBNull.Value ? dr["VendorContactNumber"].ToString() : "";
                    oNewDiscrepancyBE.CarrierName = dr["CarrierName"] != DBNull.Value ? dr["CarrierName"].ToString() : "";

                    oNewDiscrepancyBE.PersentationIssueComment = dr["PersentationIssueComment"] != DBNull.Value ? dr["PersentationIssueComment"].ToString() : "";
                    oNewDiscrepancyBE.QualityIssueComment = dr["QualityIssueComment"] != DBNull.Value ? dr["QualityIssueComment"].ToString() : "";
                    oNewDiscrepancyBE.FailPalletSpecificationComment = dr["FailPalletSpecificationComment"] != DBNull.Value ? dr["FailPalletSpecificationComment"].ToString() : "";
                    oNewDiscrepancyBE.PrematureReceiptInvoiceComment = dr["PrematureReceiptInvoiceComment"] != DBNull.Value ? dr["PrematureReceiptInvoiceComment"].ToString() : "";
                    oNewDiscrepancyBE.GenericDiscrepancyActionRequired = dr["GenericDiscrepancyActionRequired"] != DBNull.Value ? dr["GenericDiscrepancyActionRequired"].ToString() : "";
                    oNewDiscrepancyBE.OfficeDepotContactName = dr["OfficeDepotContactName"] != DBNull.Value ? dr["OfficeDepotContactName"].ToString() : "";
                    oNewDiscrepancyBE.OfficeDepotContactPhone = dr["OfficeDepotContactPhone"] != DBNull.Value ? dr["OfficeDepotContactPhone"].ToString() : "";
                    oNewDiscrepancyBE.SiteID = dr["SiteID"] != DBNull.Value ? Convert.ToInt32(dr["SiteID"]) : 0;
                    oNewDiscrepancyBE.POSiteID = dr["POSiteID"] != DBNull.Value ? Convert.ToInt32(dr["POSiteID"]) : 0;
                    oNewDiscrepancyBE.DiscrepancyWorkFlowID = dr["DiscrepancyWorkflowID"] != DBNull.Value ? Convert.ToInt32(dr["DiscrepancyWorkflowID"]) : (int?)null;
                    oNewDiscrepancyBE.DiscrepancyStatusDiscreption = dr["DiscrepancyStatusDiscreption"] != DBNull.Value ? dr["DiscrepancyStatusDiscreption"].ToString() : "";

                    if (dr.Table.Columns.Contains("GI"))
                        oNewDiscrepancyBE.GI = dr["GI"] != DBNull.Value ? Convert.ToString(dr["GI"]) : string.Empty;

                    if (dr.Table.Columns.Contains("INV"))
                        oNewDiscrepancyBE.INV = dr["INV"] != DBNull.Value ? Convert.ToString(dr["INV"]) : string.Empty;

                    if (dr.Table.Columns.Contains("AP"))
                        oNewDiscrepancyBE.AP = dr["AP"] != DBNull.Value ? Convert.ToString(dr["AP"]) : string.Empty;

                    if (dr.Table.Columns.Contains("VEN"))
                        oNewDiscrepancyBE.VEN = dr["VEN"] != DBNull.Value ? Convert.ToString(dr["VEN"]) : string.Empty;

                    DiscrepancyBEList.Add(oNewDiscrepancyBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return DiscrepancyBEList;
        }

        public List<DiscrepancyBE> GetDiscrepancyItemDetailsDAL(DiscrepancyBE oDiscrepancyBE)
        {

            List<DiscrepancyBE> DiscrepancyBEList = new List<DiscrepancyBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[3];
                param[index++] = new SqlParameter("@Action", oDiscrepancyBE.Action);
                param[index++] = new SqlParameter("@DiscrepancyLogID", oDiscrepancyBE.DiscrepancyLogID);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spLogDiscrepancy", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    DiscrepancyBE oNewDiscrepancyBE = new DiscrepancyBE();

                    oNewDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(dr["DiscrepancyLogID"].ToString());
                    oNewDiscrepancyBE.Line_no = dr["Line_No"] != DBNull.Value ? Convert.ToInt32(dr["Line_No"].ToString()) : (int?)null;
                    oNewDiscrepancyBE.ODSKUCode = dr["ODSKUCode"] != DBNull.Value ? dr["ODSKUCode"].ToString() : "";
                    oNewDiscrepancyBE.DirectCode = dr["DirectCode"] != DBNull.Value ? dr["DirectCode"].ToString() : "";
                    oNewDiscrepancyBE.VendorCode = dr["VendorCode"] != DBNull.Value ? dr["VendorCode"].ToString() : "";
                    oNewDiscrepancyBE.ProductDescription = dr["ProductDescription"] != DBNull.Value ? dr["ProductDescription"].ToString() : "";
                    oNewDiscrepancyBE.OutstandingQuantity = dr["OutstandingQuantity"] != DBNull.Value ? Convert.ToInt32(dr["OutstandingQuantity"].ToString()) : (int?)null;
                    oNewDiscrepancyBE.UOM = dr["UOM"] != DBNull.Value ? dr["UOM"].ToString() : "";
                    oNewDiscrepancyBE.DeliveredNoteQuantity = dr["DeliveryNoteQuantity"] != DBNull.Value ? Convert.ToInt32(dr["DeliveryNoteQuantity"].ToString()) : (int?)null;
                    oNewDiscrepancyBE.DeliveredQuantity = dr["DeliveredQuantity"] != DBNull.Value ? Convert.ToInt32(dr["DeliveredQuantity"].ToString()) : (int?)null;
                    oNewDiscrepancyBE.OversQuantity = dr["OversQuantity"] != DBNull.Value ? Convert.ToInt32(dr["OversQuantity"].ToString()) : (int?)null;
                    oNewDiscrepancyBE.ShortageQuantity = dr["ShortageQuantity"] != DBNull.Value ? Convert.ToInt32(dr["ShortageQuantity"].ToString()) : (int?)null;
                    oNewDiscrepancyBE.DamageQuantity = dr["DamageQuantity"] != DBNull.Value ? Convert.ToInt32(dr["DamageQuantity"].ToString()) : (int?)null;
                    oNewDiscrepancyBE.DamageDescription = dr["DamageDescription"] != DBNull.Value ? dr["DamageDescription"].ToString() : "";
                    oNewDiscrepancyBE.AmendedQuantity = dr["AmendedQuantity"] != DBNull.Value ? Convert.ToInt32(dr["AmendedQuantity"].ToString()) : (int?)null;
                    oNewDiscrepancyBE.NoPOEscalate = dr["NoPOEscalate"] != DBNull.Value ? Convert.ToBoolean(dr["NoPOEscalate"].ToString()) : (bool?)null;
                    oNewDiscrepancyBE.PackSizeOrdered = dr["PackSizeOrdered"] != DBNull.Value ? dr["PackSizeOrdered"].ToString() : "";
                    oNewDiscrepancyBE.PackSizeReceived = dr["PackSizeReceived"] != DBNull.Value ? dr["PackSizeReceived"].ToString() : "";
                    oNewDiscrepancyBE.CorrectODSKUCode = dr["CorrectODSKUCode"] != DBNull.Value ? dr["CorrectODSKUCode"].ToString() : "";
                    oNewDiscrepancyBE.CorrectProductDescription = dr["CorrectProductDescription"] != DBNull.Value ? dr["CorrectProductDescription"].ToString() : "";
                    oNewDiscrepancyBE.CorrectUOM = dr["CorrectUOM"] != DBNull.Value ? dr["CorrectUOM"].ToString() : "";
                    oNewDiscrepancyBE.CorrectVendorCode = dr["CorrectVendorCode"] != DBNull.Value ? dr["CorrectVendorCode"].ToString() : "";
                    oNewDiscrepancyBE.OriginalQuantity = dr["OriginalPOQuantity"] != DBNull.Value ? Convert.ToInt32(dr["OriginalPOQuantity"].ToString()) : (int?)null;
                    oNewDiscrepancyBE.WrongPackReceived = dr["WrongPackReceived"] != DBNull.Value ? Convert.ToInt32(dr["WrongPackReceived"].ToString()) : (int?)null;
                    oNewDiscrepancyBE.VendorID = dr["VendorID"] != DBNull.Value ? Convert.ToInt32(dr["VendorID"].ToString()) : (int?)null;
                    oNewDiscrepancyBE.PurchaseOrderID = dr["PurchaseOrderID"] != DBNull.Value ? Convert.ToInt32(dr["PurchaseOrderID"].ToString()) : (int?)null;
                    oNewDiscrepancyBE.StockPlannerID = dr["StockPlannerID"] != DBNull.Value ? Convert.ToInt32(dr["StockPlannerID"].ToString()) : (int?)null;
                    if (dr.Table.Columns.Contains("ShuttleType"))
                        oNewDiscrepancyBE.ShuttleType = dr["ShuttleType"] != DBNull.Value ? dr["ShuttleType"].ToString() : string.Empty;

                    if (dr.Table.Columns.Contains("DiscrepancyQty"))
                        oNewDiscrepancyBE.DiscrepancyQty = dr["DiscrepancyQty"] != DBNull.Value ? Convert.ToInt32(dr["DiscrepancyQty"].ToString()) : (int?)null;

                    DiscrepancyBEList.Add(oNewDiscrepancyBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return DiscrepancyBEList;
        }

        public List<DiscrepancyBE> GetDiscrepancyTypeDAL(DiscrepancyBE oDiscrepancyBE)
        {
            List<DiscrepancyBE> DiscrepancyBEList = new List<DiscrepancyBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[1];
                param[index++] = new SqlParameter("@Action", oDiscrepancyBE.Action);


                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spMAS_DiscrepancyType", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    DiscrepancyBE oNewDiscrepancyBE = new DiscrepancyBE();
                    oNewDiscrepancyBE.DiscrepancyTypeID = Convert.ToInt32(dr["DiscrepancyTypeID"].ToString());
                    oNewDiscrepancyBE.DiscrepancyType = dr["DiscrepancyType"].ToString();
                    DiscrepancyBEList.Add(oNewDiscrepancyBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return DiscrepancyBEList;
        }

        public List<DiscrepancyBE> GetStockPlannersDAL(DiscrepancyBE oDiscrepancyBE)
        {
            List<DiscrepancyBE> DiscrepancyBEList = new List<DiscrepancyBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[1];
                param[index++] = new SqlParameter("@Action", oDiscrepancyBE.Action);


                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spLogDiscrepancy", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    DiscrepancyBE oNewDiscrepancyBE = new DiscrepancyBE();

                    oNewDiscrepancyBE.UserID = Convert.ToInt32(dr["UserID"].ToString());
                    oNewDiscrepancyBE.StockPlannerNO = dr["StockPlannerNo"].ToString();
                    DiscrepancyBEList.Add(oNewDiscrepancyBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return DiscrepancyBEList;
        }

        public List<DiscrepancyBE> GetAllPurchaseOrderDateListDAL(DiscrepancyBE oDiscrepancyBE)
        {
            List<DiscrepancyBE> DiscrepancyBEList = new List<DiscrepancyBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[1];
                param[index++] = new SqlParameter("@Action", oDiscrepancyBE.Action);


                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spLogDiscrepancy", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    DiscrepancyBE oNewDiscrepancyBE = new DiscrepancyBE();

                    oNewDiscrepancyBE.PurchaseOrder_Order_raised = dr["Order_raised"] != DBNull.Value ? dr["Order_raised"].ToString() : "";

                    DiscrepancyBEList.Add(oNewDiscrepancyBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return DiscrepancyBEList;
        }

        public int? addWorkFlowHTMLsDAL(DiscrepancyBE oDiscrepancyBE)
        {
            int? intResult = 0;
            try
            {
                int index = 0;

                SqlParameter[] param = new SqlParameter[24];
                param[index++] = new SqlParameter("@Action", oDiscrepancyBE.Action);
                param[index++] = new SqlParameter("@DiscrepancyLogID", oDiscrepancyBE.DiscrepancyLogID);
                param[index++] = new SqlParameter("@LoggedDateTime", oDiscrepancyBE.LoggedDateTime);
                param[index++] = new SqlParameter("@LevelNumber", oDiscrepancyBE.LevelNumber);
                param[index++] = new SqlParameter("@GINHTML", oDiscrepancyBE.GINHTML);
                param[index++] = new SqlParameter("@GINActionRequired", oDiscrepancyBE.GINActionRequired);
                param[index++] = new SqlParameter("@GINUserControl", oDiscrepancyBE.GINUserControl);
                param[index++] = new SqlParameter("@INVHTML", oDiscrepancyBE.INVHTML);
                param[index++] = new SqlParameter("@INVActionRequired", oDiscrepancyBE.INVActionRequired);
                param[index++] = new SqlParameter("@INVUserControl", oDiscrepancyBE.INVUserControl);
                param[index++] = new SqlParameter("@APHTML", oDiscrepancyBE.APHTML);
                param[index++] = new SqlParameter("@APActionRequired", oDiscrepancyBE.APActionRequired);
                param[index++] = new SqlParameter("@APUserControl", oDiscrepancyBE.APUserControl);
                param[index++] = new SqlParameter("@VENHTML", oDiscrepancyBE.VENHTML);
                param[index++] = new SqlParameter("@VenActionRequired", oDiscrepancyBE.VenActionRequired);
                param[index++] = new SqlParameter("@VENUserControl", oDiscrepancyBE.VENUserControl);
                param[index++] = new SqlParameter("@CloseDiscrepancy", oDiscrepancyBE.CloseDiscrepancy);
                param[index++] = new SqlParameter("@NextEscalationLevel",oDiscrepancyBE.NextEscalationLevel);
                param[index++] = new SqlParameter("@VendorStatus", oDiscrepancyBE.VendorStatus);
                if (oDiscrepancyBE.QueryDiscrepancy != null)
                    param[index++] = new SqlParameter("@QueryDiscrepancyID", oDiscrepancyBE.QueryDiscrepancy.QueryDiscrepancyID);

                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spLogDiscrepancyWorkFlow", param);

                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
                else
                    intResult = 0;

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public List<DiscrepancyBE> GetWorkFlowHTMLsDAL(DiscrepancyBE oDiscrepancyBE)
        {
            List<DiscrepancyBE> DiscrepancyBEList = new List<DiscrepancyBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[3];
                param[index++] = new SqlParameter("@Action", oDiscrepancyBE.Action);
                param[index++] = new SqlParameter("@DiscrepancyLogID", oDiscrepancyBE.DiscrepancyLogID);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spLogDiscrepancyWorkFlow", param);

                DataTable dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {

                    DiscrepancyBE oNewDiscrepancyBE = new DiscrepancyBE();
                    oNewDiscrepancyBE.APHTML = dr["APHTML"] != DBNull.Value ? dr["APHTML"].ToString() : "&nbsp;";
                    oNewDiscrepancyBE.VENHTML = dr["VENHTML"] != DBNull.Value ? dr["VENHTML"].ToString() : "&nbsp;";
                    oNewDiscrepancyBE.INVHTML = dr["INVHTML"] != DBNull.Value ? dr["INVHTML"].ToString() : "&nbsp;";
                    oNewDiscrepancyBE.GINHTML = dr["GINHTML"] != DBNull.Value ? dr["GINHTML"].ToString() : "&nbsp;";
                    oNewDiscrepancyBE.DiscrepancyStatus = dr["DiscrepancyStatus"] != DBNull.Value ? dr["DiscrepancyStatus"].ToString() : "&nbsp;";
                    oNewDiscrepancyBE.ForceClosedDate = dr["ForceClosedDate"] != DBNull.Value ? dr["ForceClosedDate"].ToString() : null;
                    oNewDiscrepancyBE.Username = dr["Username"] != DBNull.Value ? dr["Username"].ToString() : null;
                    if (dr.Table.Columns.Contains("ForceClosedUserName"))
                        oNewDiscrepancyBE.ForceClosedUserName = dr["ForceClosedUserName"] != DBNull.Value ? dr["ForceClosedUserName"].ToString() : string.Empty;
                    
                    DiscrepancyBEList.Add(oNewDiscrepancyBE);
                }

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return DiscrepancyBEList;
        }

        public List<DiscrepancyBE> GetUserDetailsForWorkFlowDAL(DiscrepancyBE oDiscrepancyBE)
        {
            List<DiscrepancyBE> DiscrepancyBEList = new List<DiscrepancyBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[3];
                param[index++] = new SqlParameter("@Action", oDiscrepancyBE.Action);
                param[index++] = new SqlParameter("@DiscrepancyLogID", oDiscrepancyBE.DiscrepancyLogID);
                param[index++] = new SqlParameter("@UserID", oDiscrepancyBE.User.UserID);


                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spLogDiscrepancyWorkFlow", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {

                    DiscrepancyBE oNewDiscrepancyBE = new DiscrepancyBE();
                    oNewDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(dr["DiscrepancyLogID"].ToString());
                    oNewDiscrepancyBE.VDRNo = dr["VDRNo"] != DBNull.Value ? dr["VDRNo"].ToString() : "";
                    oNewDiscrepancyBE.DiscrepancyLogID = dr["DiscrepancyLogID"] != DBNull.Value ? Convert.ToInt32(dr["DiscrepancyLogID"].ToString()) : (int?)null;
                    oNewDiscrepancyBE.SiteID = dr["SiteID"] != DBNull.Value ? Convert.ToInt32(dr["SiteID"]) : (int?)null;
                    oNewDiscrepancyBE.StockPlannerID = dr["StockPlannerID"] != DBNull.Value ? Convert.ToInt32(dr["StockPlannerID"].ToString()) : (int?)null;
                    oNewDiscrepancyBE.DiscrepancyTypeID = dr["DiscrepancyTypeID"] != DBNull.Value ? Convert.ToInt32(dr["DiscrepancyTypeID"].ToString()) : (int?)null;
                    oNewDiscrepancyBE.PurchaseOrderNumber = dr["PurchaseOrderNumber"] != DBNull.Value ? dr["DiscrepancyTypeID"].ToString() : "";

                    DiscrepancyBEList.Add(oNewDiscrepancyBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return DiscrepancyBEList;
        }

        public DataTable GetActionRequiredDetailsForWorkFlowDAL(DiscrepancyBE oDiscrepancyBE)
        {
            DataTable dt = new DataTable();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[8];
                param[index++] = new SqlParameter("@Action", oDiscrepancyBE.Action);
                param[index++] = new SqlParameter("@UserID", oDiscrepancyBE.User.UserID);
                param[index++] = new SqlParameter("@DiscrepancyLogID", oDiscrepancyBE.DiscrepancyLogID);
                param[index++] = new SqlParameter("@GINActionRequired", oDiscrepancyBE.GINActionRequired);
                param[index++] = new SqlParameter("@INVActionRequired", oDiscrepancyBE.INVActionRequired);
                param[index++] = new SqlParameter("@APActionRequired", oDiscrepancyBE.APActionRequired);
                param[index++] = new SqlParameter("@VenActionRequired", oDiscrepancyBE.VenActionRequired);
                param[index++] = new SqlParameter("@DiscrepancyWorkFlowID", oDiscrepancyBE.DiscrepancyWorkFlowID);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spLogDiscrepancyWorkFlow", param);

                dt = ds.Tables[0];
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return dt;
        }
        public DataTable GetCheckForGIN003VisibilityDAL(DiscrepancyBE oDiscrepancyBE)
        {
            DataTable dt = new DataTable();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[3];
                param[index++] = new SqlParameter("@Action", oDiscrepancyBE.Action);
                param[index++] = new SqlParameter("@DiscrepancyLogID", oDiscrepancyBE.DiscrepancyLogID);
                param[index++] = new SqlParameter("@DiscrepancyWorkFlowID", oDiscrepancyBE.DiscrepancyWorkFlowID);
                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spLogDiscrepancyWorkFlow", param);

                dt = ds.Tables[0];
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return dt;
        }

        public int? UpdateDiscrepancyStatus(DiscrepancyBE oDiscrepancyBE)
        {
            int? intResult = 0;
            try
            {
                int index = 0;

                SqlParameter[] param = new SqlParameter[4];
                param[index++] = new SqlParameter("@Action", oDiscrepancyBE.Action);
                param[index++] = new SqlParameter("@DiscrepancyLogID", oDiscrepancyBE.DiscrepancyLogID);
                param[index++] = new SqlParameter("@DiscrepancyStatus", oDiscrepancyBE.DiscrepancyStatus);
                param[index++] = new SqlParameter("@ForceClosedUserId", oDiscrepancyBE.ForceClosedUserId);

                intResult = SqlHelper.ExecuteNonQuery(DBConnection.Connection, CommandType.StoredProcedure, "spLogDiscrepancy", param);

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public int? UpdateVendorActionElapsedTimeDAL(DiscrepancyBE oDiscrepancyBE)
        {
            int? intResult = 0;
            try
            {
                int index = 0;

                SqlParameter[] param = new SqlParameter[3];
                param[index++] = new SqlParameter("@Action", oDiscrepancyBE.Action);
                param[index++] = new SqlParameter("@DiscrepancyLogID", oDiscrepancyBE.DiscrepancyLogID);
                param[index++] = new SqlParameter("@VendorActionElapsedTime", oDiscrepancyBE.VendorActionElapsedTime);

                intResult = SqlHelper.ExecuteNonQuery(DBConnection.Connection, CommandType.StoredProcedure, "spLogDiscrepancy", param);

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public DataTable GetOpenWorkflowEntry(DiscrepancyBE oDiscrepancyBE)
        {
            DataTable dt = new DataTable();
            try
            {

                int index = 0;
                SqlParameter[] param = new SqlParameter[4];
                param[index++] = new SqlParameter("@Action", oDiscrepancyBE.Action);
                param[index++] = new SqlParameter("@UserID", oDiscrepancyBE.User.UserID);
                param[index++] = new SqlParameter("@Role", oDiscrepancyBE.User.RoleName);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spLogDiscrepancy", param);

                dt = ds.Tables[0];

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return dt;
        }



        public DataSet GetDiscrepancyLogPrintLabelDAL(DiscrepancyBE oDiscrepancyBE2)
        {
            DataSet ds = new DataSet();
            try
            {

                int index = 0;
                SqlParameter[] param = new SqlParameter[4];
                param[index++] = new SqlParameter("@Action", oDiscrepancyBE2.Action);

                param[index++] = new SqlParameter("@DiscrepancyLogID", oDiscrepancyBE2.DiscrepancyLogID);

                ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spLogDiscrepancy", param);


            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return ds;
        }
        public DataSet GetDiscrepancyLogRePrintDAL(DiscrepancyBE oDiscrepancyBE2)
        {
            DataSet ds = new DataSet();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[4];
                param[index++] = new SqlParameter("@Action", oDiscrepancyBE2.Action);
                param[index++] = new SqlParameter("@DiscrepancyLogID", oDiscrepancyBE2.DiscrepancyLogID);

                ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spLogDiscrepancy", param);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return ds;
        }

        public int? AddDiscrepancyReturnNote(DiscrepancyReturnNoteBE oDiscrepancyReturnNoteBE)
        {
            int? intResult = null;
            try
            {
                int index = 0;

                SqlParameter[] param = new SqlParameter[5];
                param[index++] = new SqlParameter("@Action", oDiscrepancyReturnNoteBE.Action);
                param[index++] = new SqlParameter("@DiscrepancyLogID", oDiscrepancyReturnNoteBE.DiscrepancyLogID);
                param[index++] = new SqlParameter("@LoggedDate", oDiscrepancyReturnNoteBE.LoggedDate);
                param[index++] = new SqlParameter("@ReturnNoteBody", oDiscrepancyReturnNoteBE.ReturnNoteBody);

                object obj = SqlHelper.ExecuteScalar(DBConnection.Connection, CommandType.StoredProcedure, "spLogDiscrepancy", param);
                if (obj != null)
                {
                    intResult = Convert.ToInt32(obj);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public List<DiscrepancyBE> GetDiscrepancyItemsForReturnNote(DiscrepancyBE oDiscrepancyBE)
        {
            List<DiscrepancyBE> DiscrepancyBEList = new List<DiscrepancyBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[3];
                param[index++] = new SqlParameter("@Action", oDiscrepancyBE.Action);
                param[index++] = new SqlParameter("@DiscrepancyLogID", oDiscrepancyBE.DiscrepancyLogID);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spLogDiscrepancy", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    DiscrepancyBE oNewDiscrepancyBE = new DiscrepancyBE();

                    oNewDiscrepancyBE.ODSKUCode = dr["ODSKUCode"] != DBNull.Value ? dr["ODSKUCode"].ToString() : "";
                    oNewDiscrepancyBE.DirectCode = dr["DirectCode"] != DBNull.Value ? dr["DirectCode"].ToString() : "";
                    oNewDiscrepancyBE.ProductDescription = dr["ProductDescription"] != DBNull.Value ? dr["ProductDescription"].ToString() : "";
                    oNewDiscrepancyBE.UOM = dr["UOM"] != DBNull.Value ? dr["UOM"].ToString() : "";
                    oNewDiscrepancyBE.DeliveredQuantity = dr["DeliveredQuantity"] != DBNull.Value ? Convert.ToInt32(dr["DeliveredQuantity"].ToString()) : (int?)null;
                    oNewDiscrepancyBE.ItemVal = dr["Item_Val"] != DBNull.Value ? dr["Item_Val"].ToString() : "";
                    oNewDiscrepancyBE.CollectionAuthNumber = dr["CollectionAuthNumber"] != DBNull.Value ? dr["CollectionAuthNumber"].ToString() : "";

                    DiscrepancyBEList.Add(oNewDiscrepancyBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return DiscrepancyBEList;

        }

        public DiscrepancyReturnNoteBE GetDiscrepancyReturnNote(DiscrepancyReturnNoteBE oDiscrepancyReturnNoteBE)
        {
            try
            {
                int index = 0;

                SqlParameter[] param = new SqlParameter[5];
                param[index++] = new SqlParameter("@Action", oDiscrepancyReturnNoteBE.Action);
                param[index++] = new SqlParameter("@ReturnNoteID", oDiscrepancyReturnNoteBE.ReturnNoteID);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spLogDiscrepancy", param);

                if (ds != null)
                {
                    DataTable dt = ds.Tables[0];
                    foreach (DataRow dr in dt.Rows)
                    {
                        oDiscrepancyReturnNoteBE.ReturnNoteBody = dr["ReturnNoteBody"] != null ? dr["ReturnNoteBody"].ToString() : "";
                    }
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oDiscrepancyReturnNoteBE;
        }

        public DiscrepancyBE GetDiscrepancyReturnNote(DiscrepancyBE oDiscrepancyBE)
        {
            try
            {
                int index = 0;

                SqlParameter[] param = new SqlParameter[5];
                param[index++] = new SqlParameter("@Action", oDiscrepancyBE.Action);
                param[index++] = new SqlParameter("@DiscrepancyLogID", oDiscrepancyBE.DiscrepancyLogID);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spLogDiscrepancy", param);

                if (ds != null)
                {
                    DataTable dt = ds.Tables[0];

                    if (dt.Rows.Count > 0)
                    {
                        DataRow dr = dt.Rows[0];

                        oDiscrepancyBE.Username = dr["Vendor_Contact_Name"] != null ? dr["Vendor_Contact_Name"].ToString() : "";

                    }
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oDiscrepancyBE;
        }

        public DataSet GetCommunicationDataToSendMailDAL(DiscrepancyMailBE oDiscrepancyMailBE) {
            DataSet Result = null;
            try {
                int index = 0;
                SqlParameter[] param = new SqlParameter[1];
                param[index++] = new SqlParameter("@Action", oDiscrepancyMailBE.Action);             

                Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spLogDiscrepancy", param);
            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return Result;
        }
        public DataSet GetDiscrepancyForVendorToCollectEscDAL(DiscrepancyMailBE oDiscrepancyMailBE)
        {
            DataSet Result = null;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[1];
                param[index++] = new SqlParameter("@Action", oDiscrepancyMailBE.Action);

                Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spLogDiscrepancy", param);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return Result;
        }

        public DataSet GetDiscrepancyDetailsDAL(DiscrepancyBE oDiscrepancyBE2) {
            DataSet ds = new DataSet();
            try {
                int index = 0;
                SqlParameter[] param = new SqlParameter[4];
                param[index++] = new SqlParameter("@Action", oDiscrepancyBE2.Action);
                param[index++] = new SqlParameter("@DiscrepancyLogID", oDiscrepancyBE2.DiscrepancyLogID);

                ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spLogDiscrepancy", param);
            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return ds;
        }

        public int? AddDiscrepancyLogCommentDAL(DiscrepancyBE oDiscrepancy)
        {
            int? intResult = 0;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[5];
                param[index++] = new SqlParameter("@Action", oDiscrepancy.Action);
                param[index++] = new SqlParameter("@DiscrepancyLogID", oDiscrepancy.DiscrepancyLogID);
                param[index++] = new SqlParameter("@UserId", oDiscrepancy.UserID);
                param[index++] = new SqlParameter("@CommentDate", oDiscrepancy.CommentDateTime);
                param[index++] = new SqlParameter("@Comment", oDiscrepancy.Comment);
                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spLogDiscrepancy", param);

                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
                else
                    intResult = 0;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public string GetVendorStatusDAL(DiscrepancyBE oDiscrepancy)
        {
            string strResult = string.Empty;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[5];
                param[index++] = new SqlParameter("@Action", oDiscrepancy.Action);
                param[index++] = new SqlParameter("@DiscrepancyLogID", oDiscrepancy.DiscrepancyLogID);
                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spVendorSubsToCharges", param);

                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    strResult = Result.Tables[0].Rows[0][1].ToString();
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return strResult;
        }

        public List<DiscrepancyBE> GetDiscrepancyLogCommentsDAL(DiscrepancyBE oDiscrepancyBE)
        {
            List<DiscrepancyBE> DiscrepancyBEList = new List<DiscrepancyBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[3];
                param[index++] = new SqlParameter("@Action", oDiscrepancyBE.Action);
                param[index++] = new SqlParameter("@DiscrepancyLogID", oDiscrepancyBE.DiscrepancyLogID);
                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spLogDiscrepancy", param);
                dt = ds.Tables[0];
                foreach (DataRow dr in dt.Rows)
                {
                    DiscrepancyBE oNewDiscrepancyBE = new DiscrepancyBE();
                    oNewDiscrepancyBE.CommentDateTime = dr["CommentDate"] != DBNull.Value ? Convert.ToDateTime( dr["CommentDate"]) : (DateTime?)null;
                    oNewDiscrepancyBE.Username = dr["UserFullName"] != DBNull.Value ? dr["UserFullName"].ToString() : string.Empty;
                    oNewDiscrepancyBE.Comment = dr["Comment"] != DBNull.Value ? dr["Comment"].ToString() : string.Empty;
                    DiscrepancyBEList.Add(oNewDiscrepancyBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return DiscrepancyBEList;
        }

        public string GetDebitCreditStatusDAL(DiscrepancyBE oDiscrepancy)
        {
            string strResult = string.Empty;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[5];
                param[index++] = new SqlParameter("@Action", oDiscrepancy.Action);
                param[index++] = new SqlParameter("@DiscrepancyLogID", oDiscrepancy.DiscrepancyLogID);
                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spLogDiscrepancy", param);

                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    strResult = Result.Tables[0].Rows[0][0].ToString();
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return strResult;
        }

        public string GetDiscrepancyCountStatusDAL(DiscrepancyBE oDiscrepancy)
        {
            string strResult = string.Empty;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[2];
                param[index++] = new SqlParameter("@date", oDiscrepancy.LoggedDateTime);
                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "sp_CountGoodsInDescrepancy", param);

                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    strResult = Result.Tables[0].Rows[0][0].ToString();
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return strResult;
        }
    }
}

﻿using System;
using System.Data.SqlClient;
using System.Data;
using System.Collections.Generic;

using Utilities;
using BusinessEntities.ModuleBE.Discrepancy.CountrySetting;

namespace DataAccessLayer.ModuleDAL.Discrepancy.CountrySetting {
    public class DISCNT_AccountPayableContactDAL: BaseDAL {

        public List<DISCnt_AccountPayableContactBE> GetAccountPayableContactDetailsDAL(DISCnt_AccountPayableContactBE oDISCnt_AccountPayableContactBE) {
            DataTable dt = new DataTable();
            List<DISCnt_AccountPayableContactBE> oDISCnt_AccountPayableContactBEList = new List<DISCnt_AccountPayableContactBE>();
            try {
                int index = 0;
                SqlParameter[] param = new SqlParameter[5];
                param[index++] = new SqlParameter("@Action", oDISCnt_AccountPayableContactBE.Action);
                param[index++] = new SqlParameter("@VendorAccountPayableID", oDISCnt_AccountPayableContactBE.VendorAccountPayableID);
                param[index++] = new SqlParameter("@VendorIDs", oDISCnt_AccountPayableContactBE.VendorIDs);
                param[index++] = new SqlParameter("@UserID", oDISCnt_AccountPayableContactBE.UserID);
                param[index++] = new SqlParameter("@VendorID", oDISCnt_AccountPayableContactBE.VendorID);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spDISCNT_AccountPayableContact", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows) {
                    DISCnt_AccountPayableContactBE oNewDISCnt_AccountPayableContactBE = new DISCnt_AccountPayableContactBE();

                    if (ds.Tables[0].Columns.Contains("VendorAccountPayableID"))
                        oNewDISCnt_AccountPayableContactBE.VendorAccountPayableID = Convert.ToInt32(dr["VendorAccountPayableID"]);

                    if (ds.Tables[0].Columns.Contains("VendorID"))
                        oNewDISCnt_AccountPayableContactBE.VendorID = Convert.ToInt32(dr["VendorID"]);

                    if (ds.Tables[0].Columns.Contains("UserID"))
                    oNewDISCnt_AccountPayableContactBE.UserID = Convert.ToInt32(dr["UserID"]);

                    if (ds.Tables[0].Columns.Contains("UserName"))
                        oNewDISCnt_AccountPayableContactBE.UserName = dr["UserName"].ToString();
                 
                    oNewDISCnt_AccountPayableContactBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
                    if (ds.Tables[0].Columns.Contains("PhoneNumber"))
                        oNewDISCnt_AccountPayableContactBE.User.PhoneNumber = dr["PhoneNumber"].ToString();

                    if (ds.Tables[0].Columns.Contains("FaxNumber"))
                        oNewDISCnt_AccountPayableContactBE.User.FaxNumber = dr["FaxNumber"].ToString();

                    if (ds.Tables[0].Columns.Contains("EmailId"))
                        oNewDISCnt_AccountPayableContactBE.User.EmailId = dr["EmailId"].ToString();

                    oNewDISCnt_AccountPayableContactBE.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();
                    if (ds.Tables[0].Columns.Contains("VendorName"))
                        oNewDISCnt_AccountPayableContactBE.Vendor.VendorName = dr["VendorName"].ToString();
                    if (ds.Tables[0].Columns.Contains("CountryID"))
                        oNewDISCnt_AccountPayableContactBE.CountryID = dr["CountryID"] != null ? Convert.ToInt32(dr["CountryID"].ToString()) : (int?)null;
                    
                    oNewDISCnt_AccountPayableContactBE.Country = new BusinessEntities.ModuleBE.AdminFunctions.MAS_CountryBE();

                    if (ds.Tables[0].Columns.Contains("CountryName"))
                        oNewDISCnt_AccountPayableContactBE.Country.CountryName = dr["CountryName"].ToString();

                    oDISCnt_AccountPayableContactBEList.Add(oNewDISCnt_AccountPayableContactBE);
                }
            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return oDISCnt_AccountPayableContactBEList;
        }

        public DataTable GetAccountPayableContactDetailsDAL(DISCnt_AccountPayableContactBE oDISCnt_AccountPayableContactBE, string Nothing = "") {
            DataTable dt = new DataTable();
            List<DISCnt_AccountPayableContactBE> oDISCnt_AccountPayableContactBEList = new List<DISCnt_AccountPayableContactBE>();
            try {
                int index = 0;
                SqlParameter[] param = new SqlParameter[3];
                param[index++] = new SqlParameter("@Action", oDISCnt_AccountPayableContactBE.Action);
                param[index++] = new SqlParameter("@VendorAccountPayableID", oDISCnt_AccountPayableContactBE.VendorAccountPayableID);
                param[index++] = new SqlParameter("@VendorIDs", oDISCnt_AccountPayableContactBE.VendorIDs);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spDISCNT_AccountPayableContact", param);

                dt = ds.Tables[0];                
            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return dt;
        }

        public DataTable GetAccountPayableVendorDetailsDAL(DISCnt_AccountPayableContactBE oDISCnt_AccountPayableContactBE, string Nothing = "")
        {
            DataTable dt = new DataTable();
            List<DISCnt_AccountPayableContactBE> oDISCnt_AccountPayableContactBEList = new List<DISCnt_AccountPayableContactBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[2];
                param[index++] = new SqlParameter("@Action", oDISCnt_AccountPayableContactBE.Action);
                param[index++] = new SqlParameter("@UserID", oDISCnt_AccountPayableContactBE.UserID);
                

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spDISCNT_AccountPayableContact", param);

                dt = ds.Tables[0];
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return dt;
        }

        public int GetUnAssignedVendorsCountDAL(DISCnt_AccountPayableContactBE oDISCnt_AccountPayableContactBE, string Nothing = "")
        {
            int vendorCount = 0;
            List<DISCnt_AccountPayableContactBE> oDISCnt_AccountPayableContactBEList = new List<DISCnt_AccountPayableContactBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[2];
                param[index++] = new SqlParameter("@Action", oDISCnt_AccountPayableContactBE.Action);
                param[index++] = new SqlParameter("@VendorAccountPayableID", oDISCnt_AccountPayableContactBE.VendorAccountPayableID);

                vendorCount = Convert.ToInt32(SqlHelper.ExecuteScalar(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spDISCNT_AccountPayableContact", param));
                 
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return vendorCount;
        }

        public DataTable GetUnAssignedVendorsDAL(DISCnt_AccountPayableContactBE oDISCnt_AccountPayableContactBE, string Nothing = "")
        {       
            DataSet Result = new DataSet();
            List<DISCnt_AccountPayableContactBE> oDISCnt_AccountPayableContactBEList = new List<DISCnt_AccountPayableContactBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[2];
                param[index++] = new SqlParameter("@Action", oDISCnt_AccountPayableContactBE.Action);
                param[index++] = new SqlParameter("@VendorAccountPayableID", oDISCnt_AccountPayableContactBE.VendorAccountPayableID);

                Result = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spDISCNT_AccountPayableContact", param);
                
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return Result.Tables[0];
        }

        public int? addEditAccountPayableContactDetailsDAL(DISCnt_AccountPayableContactBE oDISCnt_AccountPayableContactBE) {
            int? intResult = 0;
            try {
                int index = 0;
                SqlParameter[] param = new SqlParameter[6];
                param[index++] = new SqlParameter("@Action", oDISCnt_AccountPayableContactBE.Action);
                param[index++] = new SqlParameter("@UserID", oDISCnt_AccountPayableContactBE.UserID);
                param[index++] = new SqlParameter("@VendorIDs", oDISCnt_AccountPayableContactBE.VendorIDs);
                param[index++] = new SqlParameter("@IsAllVendors", oDISCnt_AccountPayableContactBE.IsAllVendors);
                param[index++] = new SqlParameter("@VendorAccountPayableID", oDISCnt_AccountPayableContactBE.VendorAccountPayableID);
                param[index++] = new SqlParameter("@MoveTo", oDISCnt_AccountPayableContactBE.MoveTo);
                
                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spDISCNT_AccountPayableContact", param);

                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
                else
                    intResult = 0;

            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public List<DISCnt_AccountPayableContactBE> GetAllAssignedVendorsByUserIDDAL(DISCnt_AccountPayableContactBE oDISCnt_AccountPayableContactBE)
        {
            DataTable dt = new DataTable();
            List<DISCnt_AccountPayableContactBE> oDISCnt_AccountPayableContactBEList = new List<DISCnt_AccountPayableContactBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[5];
                param[index++] = new SqlParameter("@Action", oDISCnt_AccountPayableContactBE.Action);
                param[index++] = new SqlParameter("@UserID", oDISCnt_AccountPayableContactBE.UserID);
                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spDISCNT_AccountPayableContact", param);
                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    DISCnt_AccountPayableContactBE oNewDISCnt_AccountPayableContactBE = new DISCnt_AccountPayableContactBE();
                    oNewDISCnt_AccountPayableContactBE.UserID = Convert.ToInt32(dr["UserID"]);
                    oNewDISCnt_AccountPayableContactBE.UserName = dr["UserName"].ToString();
                    oNewDISCnt_AccountPayableContactBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
                    oNewDISCnt_AccountPayableContactBE.User.PhoneNumber = dr["PhoneNumber"].ToString();
                    oNewDISCnt_AccountPayableContactBE.User.FaxNumber = dr["FaxNumber"].ToString();
                    oNewDISCnt_AccountPayableContactBE.User.EmailId = dr["EmailId"].ToString();
                    oNewDISCnt_AccountPayableContactBE.Country = new BusinessEntities.ModuleBE.AdminFunctions.MAS_CountryBE();
                    oNewDISCnt_AccountPayableContactBE.Country.CountryName = dr["CountryName"].ToString();
                    oNewDISCnt_AccountPayableContactBE.VendorID = Convert.ToInt32(dr["VendorID"]);
                    oNewDISCnt_AccountPayableContactBE.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();
                    oNewDISCnt_AccountPayableContactBE.Vendor.VendorName = dr["Vendor_Name"].ToString();
                    oNewDISCnt_AccountPayableContactBE.Vendor.Vendor_No = dr["Vendor_No"].ToString();
                    oNewDISCnt_AccountPayableContactBE.CountryID = dr["CountryID"] != null ? Convert.ToInt32(dr["CountryID"].ToString()) : (int?)null;
                    oDISCnt_AccountPayableContactBEList.Add(oNewDISCnt_AccountPayableContactBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return oDISCnt_AccountPayableContactBEList;
        }

        public List<DISCnt_AccountPayableContactBE> GetAllUnAssignedVendorsDAL(DISCnt_AccountPayableContactBE oDISCnt_AccountPayableContactBE)
        {
            DataTable dt = new DataTable();
            List<DISCnt_AccountPayableContactBE> oDISCnt_AccountPayableContactBEList = new List<DISCnt_AccountPayableContactBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[5];
                param[index++] = new SqlParameter("@Action", oDISCnt_AccountPayableContactBE.Action);
                param[index++] = new SqlParameter("@VendorAccountPayableID", oDISCnt_AccountPayableContactBE.VendorAccountPayableID);
                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spDISCNT_AccountPayableContact", param);
                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    DISCnt_AccountPayableContactBE oNewDISCnt_AccountPayableContactBE = new DISCnt_AccountPayableContactBE();
                    oNewDISCnt_AccountPayableContactBE.VendorID = Convert.ToInt32(dr["VendorID"]);
                    oNewDISCnt_AccountPayableContactBE.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();
                    oNewDISCnt_AccountPayableContactBE.Vendor.VendorName = Convert.ToString(dr["Vendor_Name"]);
                    oNewDISCnt_AccountPayableContactBE.Vendor.Vendor_No = Convert.ToString(dr["Vendor_No"]);
                    oNewDISCnt_AccountPayableContactBE.Vendor.CountryName = Convert.ToString(dr["Vendor_Country"]);
                    oDISCnt_AccountPayableContactBEList.Add(oNewDISCnt_AccountPayableContactBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return oDISCnt_AccountPayableContactBEList;
        }
    }
}

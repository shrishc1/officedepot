﻿using System;
using System.Data;
using System.Data.SqlClient;
using BusinessEntities.ModuleBE.Discrepancy.Report;
using Utilities;


namespace DataAccessLayer.ModuleDAL.Discrepancy.Report
{
    public class DiscrepancyReportDAL
    {
        DiscrepancyReportBE oDiscrepancyReportBE = new DiscrepancyReportBE();
        public DataSet getDiscrepancyReportDAL(DiscrepancyReportBE oDiscrepancyReportBE)
        {
            DataSet Result = null;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[11];
                param[index++] = new SqlParameter("@Action", oDiscrepancyReportBE.Action);
                param[index++] = new SqlParameter("@SelectedCountryIDs", oDiscrepancyReportBE.SelectedCountryIDs);
                param[index++] = new SqlParameter("@SelectedSiteIDs", oDiscrepancyReportBE.SelectedSiteIDs);
                param[index++] = new SqlParameter("@SelectedVendorIDs", oDiscrepancyReportBE.SelectedVendorIDs);
                param[index++] = new SqlParameter("@SelectedStockPlannerIDs", oDiscrepancyReportBE.SelectedStockPlannerIDs);
                param[index++] = new SqlParameter("@SelectedAPIDs", oDiscrepancyReportBE.SelectedAPIDs);
                param[index++] = new SqlParameter("@VendorNumber", oDiscrepancyReportBE.VendorNumber);
                param[index++] = new SqlParameter("@VendorName", oDiscrepancyReportBE.VendorName);
                param[index++] = new SqlParameter("@DateTo", oDiscrepancyReportBE.DateTo);
                param[index++] = new SqlParameter("@DateFrom", oDiscrepancyReportBE.DateFrom);
                param[index++] = new SqlParameter("@DiscrepancyTypeId", oDiscrepancyReportBE.DiscrepancyTypeId);

                Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spRPTDIS_Reports", param);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return Result;
        }

        public DataSet getAssignedVendorsReportDAL(DiscrepancyReportBE oDiscrepancyReportBE)
        {
            DataSet Result = null;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[4];
                param[index++] = new SqlParameter("@Action", oDiscrepancyReportBE.Action);
                param[index++] = new SqlParameter("@SelectedCountryIDs", oDiscrepancyReportBE.SelectedCountryIDs);
                //param[index++] = new SqlParameter("@SelectedSiteIDs", oDiscrepancyReportBE.SelectedSiteIDs);
                param[index++] = new SqlParameter("@SelectedVendorIDs", oDiscrepancyReportBE.SelectedVendorIDs);
                //param[index++] = new SqlParameter("@SelectedStockPlannerIDs", oDiscrepancyReportBE.SelectedStockPlannerIDs);
                param[index++] = new SqlParameter("@SelectedAPIDs", oDiscrepancyReportBE.SelectedAPIDs);
                //param[index++] = new SqlParameter("@VendorNumber", oDiscrepancyReportBE.VendorNumber);
                //param[index++] = new SqlParameter("@VendorName", oDiscrepancyReportBE.VendorName);
                //param[index++] = new SqlParameter("@DateTo", oDiscrepancyReportBE.DateTo);
                //param[index++] = new SqlParameter("@DateFrom", oDiscrepancyReportBE.DateFrom);
                //param[index++] = new SqlParameter("@DiscrepancyTypeId", oDiscrepancyReportBE.DiscrepancyTypeId);

                Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spRPTDIS_Reports", param);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return Result;
        }

        public DataSet GetShuttleDiscrepancyReportDAL(DiscrepancyReportBE oDiscrepancyReportBE)
        {
            DataSet Result = null;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[10];
                param[index++] = new SqlParameter("@Action", oDiscrepancyReportBE.Action);
                param[index++] = new SqlParameter("@SelectedReceivingSiteIDs", oDiscrepancyReportBE.SelectedReceivingSiteIDs);
                param[index++] = new SqlParameter("@SelectedSendingSiteIDs", oDiscrepancyReportBE.SelectedSendingSiteIDs);
                param[index++] = new SqlParameter("@SelectedDiscrepancyType", oDiscrepancyReportBE.SelectedTypes);
                param[index++] = new SqlParameter("@SelectedPO", oDiscrepancyReportBE.SelectedPO);
                param[index++] = new SqlParameter("@SelectedSKU", oDiscrepancyReportBE.SelectedSKU);
                param[index++] = new SqlParameter("@DateTo", oDiscrepancyReportBE.DateTo);
                param[index++] = new SqlParameter("@DateFrom", oDiscrepancyReportBE.DateFrom);

                Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spRPTDIS_Reports", param);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return Result;
        }

        public DataSet getDiscrepancySchedulerReportDAL()
        {
            DataSet Result = null;
            try
            {

                Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "DiscrepancyStockPlannerReportScheduler");
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return Result;
        }

        public DataSet getDiscrepancySchedulerNotificationDAL(DiscrepancyNotification discrepancyNotification)
        {
            DataSet Result = null;
            try
            {
                if (discrepancyNotification != null)
                {
                    int index = 0;
                    SqlParameter[] param = new SqlParameter[10];
                    param[index++] = new SqlParameter("@Action", discrepancyNotification.Action);
                    param[index++] = new SqlParameter("@Week", discrepancyNotification.Week);
                    param[index++] = new SqlParameter("@Year", discrepancyNotification.Year);
                    param[index++] = new SqlParameter("@EmailContent", discrepancyNotification.EmailContent);
                    param[index++] = new SqlParameter("@EditedBy", discrepancyNotification.EditedBy);
                    param[index++] = new SqlParameter("@AddedBy", discrepancyNotification.AddedBy);
                    param[index++] = new SqlParameter("@MailSent", discrepancyNotification.MailSent);
                    param[index++] = new SqlParameter("@ExcelPath", discrepancyNotification.ExcelPath);
                    param[index++] = new SqlParameter("@ImagePath", discrepancyNotification.ImagePath);
                    param[index++] = new SqlParameter("@EmailSubject", discrepancyNotification.EmailSubject);
                    Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spDiscrepancyTrackerCommunication", param);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return Result;
        }

        public DiscrepancyNotification getDiscrepancyNotificationForSendingMailDAL(DiscrepancyNotification discrepancyNotification)
        {
            DiscrepancyNotification notification = new DiscrepancyNotification();
            try
            {
                if (discrepancyNotification != null)
                {
                    int index = 0;
                    SqlParameter[] param = new SqlParameter[10];
                    param[index++] = new SqlParameter("@Action", discrepancyNotification.Action);
                    param[index++] = new SqlParameter("@Week", discrepancyNotification.Week);
                    param[index++] = new SqlParameter("@Year", discrepancyNotification.Year);

                    DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spDiscrepancyTrackerCommunication", param);

                    notification.Week = Convert.ToInt32(Result.Tables[0].Rows[0]["Week"]);
                    notification.EmailContent = Convert.ToString(Result.Tables[0].Rows[0]["EmailContent"]);
                    notification.ImagePath = Convert.ToString(Result.Tables[0].Rows[0]["ImagePath"]);
                    notification.EmailSubject = Convert.ToString(Result.Tables[0].Rows[0]["EmailSubject"]);
                    notification.ExcelPath = Convert.ToString(Result.Tables[0].Rows[0]["ExcelPath"]);
                    notification.DiscrepancyTrackerId = Convert.ToInt64(Result.Tables[0].Rows[0]["DiscrepancyTrackerId"]);

                    return notification;
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return notification;
        }

        public DataSet UpdateDiscrepancySchedulerNotificationDAL(DiscrepancyNotification discrepancyNotification)
        {
            DataSet Result = null;
            try
            {
                if (discrepancyNotification != null)
                {
                    int index = 0;
                    SqlParameter[] param = new SqlParameter[2];
                    param[index++] = new SqlParameter("@Action", discrepancyNotification.Action);
                    param[index++] = new SqlParameter("@DiscrepancyTrackerId", discrepancyNotification.DiscrepancyTrackerId);
                    
                    Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spDiscrepancyTrackerCommunication", param);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return Result;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using Utilities;
using BusinessEntities.ModuleBE.StockOverview.StockTurn;

namespace DataAccessLayer.ModuleDAL.StockOverview.StockTurn
{
    public class StockTurnReportDAL
    {
        public DataSet GetStockTurnReportDAL(StockTurnReportBE oStockTurnReportBE)
        {
            DataSet Result = null;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[12];
                param[index++] = new SqlParameter("@Action", oStockTurnReportBE.ReportType);
                param[index++] = new SqlParameter("@ActionType", oStockTurnReportBE.Action);
                param[index++] = new SqlParameter("@COUNTRYIDS", oStockTurnReportBE.SelectedCountryIDs);
                param[index++] = new SqlParameter("@SITEIDS", oStockTurnReportBE.SelectedSiteIDs);
                param[index++] = new SqlParameter("@STOCKPLANNERIDS", oStockTurnReportBE.SelectedStockPlannerIDs);
                param[index++] = new SqlParameter("@VENDORIDS", oStockTurnReportBE.SelectedVendorIDs);
                param[index++] = new SqlParameter("@ITEMCLASSIFICATION", oStockTurnReportBE.SelectedItemClassification);
                param[index++] = new SqlParameter("@OD_SKU_NO", oStockTurnReportBE.SelectedSKUCodes);
                param[index++] = new SqlParameter("@DateTo", oStockTurnReportBE.DateTo);
                param[index++] = new SqlParameter("@DateFrom", oStockTurnReportBE.DateFrom);
                param[index++] = new SqlParameter("@CATEGORYIDS", oStockTurnReportBE.SelectedRMSCategoryIDs);
                param[index++] = new SqlParameter("@IsVendorScheduler", oStockTurnReportBE.IsVendorScheduler != null ? oStockTurnReportBE.IsVendorScheduler : false);

                //StringBuilder sbMessage = new StringBuilder();
                //sbMessage.Append("DAL SP name           : sp_StockTurn_Reports");
                //sbMessage.Append("\r\n");

                //for (int i = 0; i <= param.Length - 1; i++)
                //{
                //    sbMessage.Append("DAL Param Values      : " + i.ToString() + ") " + param[i].ParameterName + " - " + Convert.ToString(param[i].Value));
                //    sbMessage.Append("\r\n");
                //}

                //LogUtility.SaveTraceLogEntry(sbMessage);

                Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "sp_Stockturn_Reports", param);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return Result;
        }    
    }
}

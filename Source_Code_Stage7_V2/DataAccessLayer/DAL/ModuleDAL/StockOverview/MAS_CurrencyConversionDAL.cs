﻿using System;
using System.Data;
using System.Data.SqlClient;
using Utilities;

using BusinessEntities.ModuleBE.StockOverview;
using System.Collections.Generic;

namespace DataAccessLayer.ModuleDAL.StockOverview
{
   public class MAS_CurrencyConversionDAL
    {

       public List<MAS_CurrencyConversionBE> GetCurrenyConversionDAL(MAS_CurrencyConversionBE MAS_CurrencyConversionBE)
       {
           List<MAS_CurrencyConversionBE> oSYS_CurrencyConversionBEList = new List<MAS_CurrencyConversionBE>();
           try
           {
               DataTable dt = new DataTable();
               int index = 0;
               SqlParameter[] param = new SqlParameter[4];
               param[index++] = new SqlParameter("@Action", MAS_CurrencyConversionBE.Action);
               param[index++] = new SqlParameter("@CurrencyConversionID", MAS_CurrencyConversionBE.CurrencyConversionID);
               param[index++] = new SqlParameter("@RateApplicableDate", MAS_CurrencyConversionBE.RateApplicableDate);
               param[index++] = new SqlParameter("@FromDate", MAS_CurrencyConversionBE.FromDate);

               DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spMAS_CurrencyConversion", param);

               dt = ds.Tables[0];

               foreach (DataRow dr in dt.Rows)
               {
                   MAS_CurrencyConversionBE oNewSYS_CurrencyConversionBE = new MAS_CurrencyConversionBE();
                   oNewSYS_CurrencyConversionBE.CurrencyConversionID = Convert.ToInt32(dr["CurrencyConversionID"]);
                   oNewSYS_CurrencyConversionBE.RateApplicableDate = dr["RateApplicableDate"]!= DBNull.Value? Convert.ToDateTime(dr["RateApplicableDate"]):(DateTime?)null;
                   oNewSYS_CurrencyConversionBE.ConversionRate = Convert.ToDecimal(dr["ConversionRate"]);

                   oSYS_CurrencyConversionBEList.Add(oNewSYS_CurrencyConversionBE);
               }
           }
           catch (Exception ex)
           {
               LogUtility.SaveErrorLogEntry(ex);
           }
           return oSYS_CurrencyConversionBEList;
       }



       public int? addEditCurrencyConversionDAL(MAS_CurrencyConversionBE MAS_CurrencyConversionBE)
       {
           int? intResult = 0;
           try
           {
               int index = 0;
               SqlParameter[] param = new SqlParameter[4];
               param[index++] = new SqlParameter("@Action", MAS_CurrencyConversionBE.Action);
               param[index++] = new SqlParameter("@CurrencyConversionID", MAS_CurrencyConversionBE.CurrencyConversionID);
               param[index++] = new SqlParameter("@RateApplicableDate", MAS_CurrencyConversionBE.RateApplicableDate);
               param[index++] = new SqlParameter("@ConversionRate", MAS_CurrencyConversionBE.ConversionRate);

               DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spMAS_CurrencyConversion", param);

               if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                   intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
               else
                   intResult = 0;

           }
           catch (Exception ex)
           {
               LogUtility.SaveErrorLogEntry(ex);
           }
           finally { }
           return intResult;
       }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using BusinessEntities.ModuleBE.Security;
using Utilities;

namespace DataAccessLayer.ModuleDAL.Security
{
    public class SCT_TemplateDAL
    {
        public List<SCT_UserRoleBE> GetUserRolesDAL(SCT_UserRoleBE oSCT_UserRoleBE)
        {
            List<SCT_UserRoleBE> lstRoles = new List<SCT_UserRoleBE>();
            DataTable dt = new DataTable();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[1];
                param[index++] = new SqlParameter("Action", oSCT_UserRoleBE.Action);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spUserRole", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    SCT_UserRoleBE oUserRole = new SCT_UserRoleBE();

                    oUserRole.UserRoleID = Convert.ToInt32(dr["UserRoleID"]);
                    oUserRole.RoleName = dr["RoleName"].ToString();

                    lstRoles.Add(oUserRole);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return lstRoles;
        }

        public void SavePermissionToDB(SCT_TemplateBE oSCT_TemplateBE)
        {
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[6];
                param[index++] = new SqlParameter("Action", oSCT_TemplateBE.Action);
                param[index++] = new SqlParameter("@ForRole", oSCT_TemplateBE.ForRole);
                param[index++] = new SqlParameter("@TemplateName", oSCT_TemplateBE.TemplateName);
                param[index++] = new SqlParameter("@SelectedScreen", oSCT_TemplateBE.SelectedScreen);                
                param[index++] = new SqlParameter("@TemplateID", oSCT_TemplateBE.TemplateID != null ? oSCT_TemplateBE.TemplateID : null);


                SqlHelper.ExecuteNonQuery(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spSCT_Template", param);
            }
            catch { }

        }

        public List<SCT_TemplateBE> GetTemplate(SCT_TemplateBE oSCT_TemplateBE)
        {
            List<SCT_TemplateBE> lstTemplate = new List<SCT_TemplateBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[2];
                param[index++] = new SqlParameter("@Action", oSCT_TemplateBE.Action);
                param[index++] = new SqlParameter("@ForRole", oSCT_TemplateBE.ForRole);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spSCT_Template", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    SCT_TemplateBE oNewSCT_TemplateBE = new SCT_TemplateBE();
                    oNewSCT_TemplateBE.TemplateName = dr["TemplateName"].ToString();
                    oNewSCT_TemplateBE.TemplateID = Convert.ToInt32(dr["TemplateID"]);
                    oNewSCT_TemplateBE.RoleName = dr["RoleName"].ToString();
                    lstTemplate.Add(oNewSCT_TemplateBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return lstTemplate;
        }

        public SCT_TemplateBE GetTemplateInfo(SCT_TemplateBE oSCT_TemplateBE)
        {
            SCT_TemplateBE oNewSCT_TemplateBE = new SCT_TemplateBE();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[2];
                param[index++] = new SqlParameter("@Action", oSCT_TemplateBE.Action);
                param[index++] = new SqlParameter("@TemplateID", oSCT_TemplateBE.TemplateID);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spSCT_Template", param);

                dt = ds.Tables[0];
                string SelectedScreen = string.Empty;                
                foreach (DataRow dr in dt.Rows)
                {
                    oNewSCT_TemplateBE.TemplateName = dr["TemplateName"].ToString();
                    oNewSCT_TemplateBE.RoleName = dr["RoleName"].ToString();
                    string ScreenID= dr["ScreenID"].ToString();                     
                    SelectedScreen += ScreenID + ",";                   
                }
                oNewSCT_TemplateBE.SelectedScreen = SelectedScreen;                
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return oNewSCT_TemplateBE;
        }

        public List<SCT_TemplateBE> GetRoleTemplateDAL(SCT_TemplateBE oSCT_TemplateBE)
        {
            List<SCT_TemplateBE> lstRoleTemplates = new List<SCT_TemplateBE>();
            DataTable dt = new DataTable();
            int? RoleID = oSCT_TemplateBE.ForRole;
            try
            {
                string query = string.Empty;
                if (RoleID != null)
                    query = string.Format("SELECT * FROM SCT_Template WHERE UserRoleID={0}", RoleID);
                else
                    query = "SELECT * FROM SCT_Template";
                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.Text, query);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    SCT_TemplateBE oRoleTemplate = new SCT_TemplateBE();

                    oRoleTemplate.TemplateID = Convert.ToInt32(dr["TemplateID"]);
                    oRoleTemplate.TemplateName = dr["TemplateName"].ToString();

                    lstRoleTemplates.Add(oRoleTemplate);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return lstRoleTemplates;
        }

        //public SCT_TemplateBE GetTemplateInfoDAL(SCT_TemplateBE oSCT_TemplateBE)
        //{
        //    SCT_TemplateBE Templates = new SCT_TemplateBE();
        //    DataTable dt = new DataTable();
        //    try
        //    {
        //        string WritableScreen = string.Empty;
        //        string ReadableScreen = string.Empty;
        //        string query = string.Empty;
        //        query = string.Format("SELECT * FROM SCT_TemplateScreen WHERE TemplateID={0}", oSCT_TemplateBE.TemplateID);

        //        DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.Text, query);

        //        dt = ds.Tables[0];

        //        foreach (DataRow dr in dt.Rows)
        //        {
        //            string screenid = dr["ScreenID"].ToString();
        //            ReadableScreen += "," + screenid;
        //            if (Convert.ToBoolean(dr["IsWriteAccessAllowed"]))
        //            {
        //                WritableScreen += "," + screenid;
        //            }
        //        }
        //        Templates.ScreenWithReadPermission = ReadableScreen.Trim(',');
        //        Templates.ScreenWithWritePermission = WritableScreen.Trim(',');
        //    }
        //    catch (Exception ex)
        //    {
        //        LogUtility.SaveLogEntry(ex);
        //    }
        //    return Templates;
        //}
    }
}

﻿using System;
using System.Data;
using System.Data.SqlClient;
using Utilities;
using BusinessEntities.ModuleBE.AdminFunctions;
using System.Collections.Generic;

namespace DataAccessLayer.ModuleDAL.AdminFunctions
{
    public class MAS_MaintainVendorPointsContactDAL
    {
        public int? AddVendorPointsContactDAL(MAS_MaintainVendorPointsContactBE vendorPointsContactBE)
        {
            int? intResult = 0;
            try
            {
                var index = 0;
                SqlParameter[] param = new SqlParameter[15];
                param[index++] = new SqlParameter("@Action", vendorPointsContactBE.Action);

                if (vendorPointsContactBE.User != null)
                    param[index++] = new SqlParameter("@UserID", vendorPointsContactBE.User.UserID);

                if (vendorPointsContactBE.Country != null)
                    param[index++] = new SqlParameter("@CountryID", vendorPointsContactBE.Country.CountryID);

                if (vendorPointsContactBE.Vendor != null)
                    param[index++] = new SqlParameter("@VendorID", vendorPointsContactBE.Vendor.VendorID);
                
                param[index++] = new SqlParameter("@FirstName", vendorPointsContactBE.FirstName);
                param[index++] = new SqlParameter("@SecondName", vendorPointsContactBE.SecondName);
                param[index++] = new SqlParameter("@RoleTitle", vendorPointsContactBE.RoleTitle);
                param[index++] = new SqlParameter("@EmailAddress", vendorPointsContactBE.EmailAddress);
                param[index++] = new SqlParameter("@PhoneNumber", vendorPointsContactBE.PhoneNumber);
                param[index++] = new SqlParameter("@InventoryPOC", vendorPointsContactBE.InventoryPOC);
                param[index++] = new SqlParameter("@ProcurementPOC", vendorPointsContactBE.ProcurementPOC);
                param[index++] = new SqlParameter("@MerchandisingPOC", vendorPointsContactBE.MerchandisingPOC);
                param[index++] = new SqlParameter("@ExecutivePOC", vendorPointsContactBE.ExecutivePOC);
                param[index++] = new SqlParameter("@VendorPointID", vendorPointsContactBE.VendorPointID);
                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spUP_Vendor", param);
                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
                else
                    intResult = 0;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public int? UpdateVendorPointsContactDAL(MAS_MaintainVendorPointsContactBE vendorPointsContactBE)
        {
            int? intResult = 0;
            try
            {
                var index = 0;
                SqlParameter[] param = new SqlParameter[14];
                param[index++] = new SqlParameter("@Action", vendorPointsContactBE.Action);
                param[index++] = new SqlParameter("@UserID", vendorPointsContactBE.User.UserID);
                param[index++] = new SqlParameter("@CountryID", vendorPointsContactBE.Country.CountryID);
                param[index++] = new SqlParameter("@VendorID", vendorPointsContactBE.Vendor.VendorID);
                param[index++] = new SqlParameter("@FirstName", vendorPointsContactBE.FirstName);
                param[index++] = new SqlParameter("@SecondName", vendorPointsContactBE.SecondName);
                param[index++] = new SqlParameter("@RoleTitle", vendorPointsContactBE.RoleTitle);
                param[index++] = new SqlParameter("@EmailAddress", vendorPointsContactBE.EmailAddress);
                param[index++] = new SqlParameter("@PhoneNumber", vendorPointsContactBE.PhoneNumber);
                param[index++] = new SqlParameter("@InventoryPOC", vendorPointsContactBE.InventoryPOC);
                param[index++] = new SqlParameter("@ProcurementPOC", vendorPointsContactBE.ProcurementPOC);
                param[index++] = new SqlParameter("@MerchandisingPOC", vendorPointsContactBE.MerchandisingPOC);
                param[index++] = new SqlParameter("@ExecutivePOC", vendorPointsContactBE.ExecutivePOC);
                param[index++] = new SqlParameter("@VendorPointID", vendorPointsContactBE.VendorPointID);
                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spUP_Vendor", param);
                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
                else
                    intResult = 0;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public int? DeleteVendorPointsContactDAL(MAS_MaintainVendorPointsContactBE vendorPointsContactBE)
        {
            int? intResult = 0;
            try
            {
                var index = 0;
                SqlParameter[] param = new SqlParameter[3];
                param[index++] = new SqlParameter("@Action", vendorPointsContactBE.Action);
                param[index++] = new SqlParameter("@IsActive", vendorPointsContactBE.IsActive);               
                param[index++] = new SqlParameter("@VendorPointID", vendorPointsContactBE.VendorPointID);
                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spUP_Vendor", param);
                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
                else
                    intResult = 0;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public List<MAS_MaintainVendorPointsContactBE> GetVendorPointsContactsDAL(MAS_MaintainVendorPointsContactBE vendorPointsContactBE)
        {
            var dt = new DataTable();
            var lstVendorPointsContacts = new List<MAS_MaintainVendorPointsContactBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[10];
                param[index++] = new SqlParameter("@Action", vendorPointsContactBE.Action);
                param[index++] = new SqlParameter("@VendorPointID", vendorPointsContactBE.VendorPointID);

                if (vendorPointsContactBE.User != null)
                    param[index++] = new SqlParameter("@UserID", vendorPointsContactBE.User.UserID);

                if (vendorPointsContactBE.Country != null)
                    param[index++] = new SqlParameter("@CountryId", vendorPointsContactBE.Country.CountryID);

                if (vendorPointsContactBE.Vendor != null)
                    param[index++] = new SqlParameter("@VendorID", vendorPointsContactBE.Vendor.VendorID);

                param[index++] = new SqlParameter("@InventoryPOC", vendorPointsContactBE.InventoryPOC);
                param[index++] = new SqlParameter("@ProcurementPOC", vendorPointsContactBE.ProcurementPOC);
                param[index++] = new SqlParameter("@MerchandisingPOC", vendorPointsContactBE.MerchandisingPOC);
                param[index++] = new SqlParameter("@ExecutivePOC", vendorPointsContactBE.ExecutivePOC);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spUP_Vendor", param);

                if (ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];

                    foreach (DataRow dr in dt.Rows)
                    {
                        var oVendorPointsContactBE = new MAS_MaintainVendorPointsContactBE();
                        oVendorPointsContactBE.VendorPointID = dr["VendorPointID"] != DBNull.Value ? Convert.ToInt32(dr["VendorPointID"]) : (int?)null;
                        oVendorPointsContactBE.VendorPointDate = dr["VendorPointDate"] != DBNull.Value ? Convert.ToDateTime(dr["VendorPointDate"]) : (DateTime?)null;
                        oVendorPointsContactBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
                        oVendorPointsContactBE.User.UserID = dr["UserID"] != DBNull.Value ? Convert.ToInt32(dr["UserID"]) : 0;
                        oVendorPointsContactBE.User.UserName = dr["UserFullName"] != DBNull.Value ? dr["UserFullName"].ToString() : String.Empty;
                        oVendorPointsContactBE.Country = new MAS_CountryBE();
                        oVendorPointsContactBE.Country.CountryID = dr["CountryID"] != DBNull.Value ? Convert.ToInt32(dr["CountryID"]) : 0;
                        oVendorPointsContactBE.Country.CountryName = dr["CountryName"] != DBNull.Value ? Convert.ToString(dr["CountryName"]) : String.Empty;
                        oVendorPointsContactBE.Vendor = new MAS_VendorBE();
                        oVendorPointsContactBE.Vendor.VendorID = dr["VendorID"] != DBNull.Value ? Convert.ToInt32(dr["VendorID"]) : (int?)null;
                        oVendorPointsContactBE.Vendor.Vendor_Country = dr["Vendor_Country"].ToString();
                        oVendorPointsContactBE.Vendor.Vendor_No = dr["Vendor_No"] != DBNull.Value ? dr["Vendor_No"].ToString() : String.Empty;
                        oVendorPointsContactBE.Vendor.Vendor_Name = dr["Vendor_Name"] != DBNull.Value ? dr["Vendor_Name"].ToString() : String.Empty;
                        oVendorPointsContactBE.FirstName = dr["FirstName"] != DBNull.Value ? dr["FirstName"].ToString() : String.Empty;
                        oVendorPointsContactBE.SecondName = dr["SecondName"] != DBNull.Value ? dr["SecondName"].ToString() : String.Empty;
                        oVendorPointsContactBE.RoleTitle = dr["RoleTitle"] != DBNull.Value ? dr["RoleTitle"].ToString() : String.Empty;
                        oVendorPointsContactBE.EmailAddress = dr["EmailAddress"] != DBNull.Value ? dr["EmailAddress"].ToString() : String.Empty;
                        oVendorPointsContactBE.PhoneNumber = dr["PhoneNumber"] != DBNull.Value ? dr["PhoneNumber"].ToString() : String.Empty;
                        oVendorPointsContactBE.InventoryPOC = dr["InventoryPOC"] != DBNull.Value ? dr["InventoryPOC"].ToString() : String.Empty;
                        oVendorPointsContactBE.ProcurementPOC = dr["ProcurementPOC"] != DBNull.Value ? dr["ProcurementPOC"].ToString() : String.Empty;
                        oVendorPointsContactBE.MerchandisingPOC = dr["MerchandisingPOC"] != DBNull.Value ? dr["MerchandisingPOC"].ToString() : String.Empty;
                        oVendorPointsContactBE.ExecutivePOC = dr["ExecutivePOC"] != DBNull.Value ? dr["ExecutivePOC"].ToString() : String.Empty;
                        oVendorPointsContactBE.IsActive = dr["IsActive"] != DBNull.Value ? Convert.ToInt32(dr["IsActive"]) : (int?)null;
                        oVendorPointsContactBE.VendorPointUpdateDate = dr["VendorPointUpdateDate"] != DBNull.Value ? Convert.ToDateTime(dr["VendorPointUpdateDate"]) : (DateTime?)null;
                        oVendorPointsContactBE.VendorPointDeleteDate = dr["VendorPointDeleteDate"] != DBNull.Value ? Convert.ToDateTime(dr["VendorPointDeleteDate"]) : (DateTime?)null;
                        lstVendorPointsContacts.Add(oVendorPointsContactBE);
                    }
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return lstVendorPointsContacts;
        }
    }
}

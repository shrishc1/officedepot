﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using BusinessEntities.ModuleBE.AdminFunctions;
using Utilities;

namespace DataAccessLayer.ModuleDAL.AdminFunctions
{
    public class ReportLibraryDAL : DataAccessLayer.BaseDAL
    {
        public List<ReportLibraryBE> GetReportLibraryDAL(ReportLibraryBE oReportLibrary)
        {
            var lstReportLibrary = new List<ReportLibraryBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[1];
                param[index++] = new SqlParameter("@Action", oReportLibrary.Action);
                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spVIP_ReportLibrary", param);
                if (ds != null && ds.Tables.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        var reportLibrary = new ReportLibraryBE();
                        reportLibrary.ReportLibraryId = dr["ReportLibraryId"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["ReportLibraryId"]);
                        reportLibrary.ModuleId = dr["ModuleId"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["ModuleId"]);
                        reportLibrary.ModuleName = dr["ModuleName"] == DBNull.Value ? null : Convert.ToString(dr["ModuleName"]);
                        reportLibrary.OrderBy = dr["OrderBy"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["OrderBy"]);
                        reportLibrary.ScreenId = dr["ScreenId"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["ScreenId"]);
                        reportLibrary.ScreenName = dr["ScreenName"] == DBNull.Value ? null : Convert.ToString(dr["ScreenName"]);
                        reportLibrary.ScreenUrl = dr["ScreenUrl"] == DBNull.Value ? null : Convert.ToString(dr["ScreenUrl"]);
                        lstReportLibrary.Add(reportLibrary);
                    }
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return lstReportLibrary;
        }
    }
}

﻿using System;
using System.Data;
using System.Data.SqlClient;
using Utilities;

using BusinessEntities.ModuleBE.AdminFunctions;
using System.Collections.Generic;
namespace DataAccessLayer.ModuleDAL.AdminFunctions
{
    public class MAS_VendorDAL : DataAccessLayer.BaseDAL
    {
        public List<MAS_VendorBE> GetVendorDAL(MAS_VendorBE oMAS_VendorBE)
        {

            List<MAS_VendorBE> oMAS_VendorBEList = new List<MAS_VendorBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[9];
                param[index++] = new SqlParameter("@Action", oMAS_VendorBE.Action);
                param[index++] = new SqlParameter("@CountryID", oMAS_VendorBE.CountryID);
                param[index++] = new SqlParameter("@RegistrationStatus", oMAS_VendorBE.RegistrationStatus);
                param[index++] = new SqlParameter("@VendorsMissingDetails", oMAS_VendorBE.VendorsMissingDetails);
                param[index++] = new SqlParameter("@SiteID", oMAS_VendorBE.SiteID);
                param[index++] = new SqlParameter("@IsActiveVendor", oMAS_VendorBE.IsActiveVendor); /// Added on 21 Feb 2013
                param[index++] = new SqlParameter("@VendorIds", oMAS_VendorBE.VendorIDs);
                param[index++] = new SqlParameter("@EuropeanorLocal", oMAS_VendorBE.EuropeanorLocal);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spMAS_Vendor", param);
                if (ds != null && ds.Tables.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        MAS_VendorBE oNewMAS_VendorBE = new MAS_VendorBE();
                        oNewMAS_VendorBE.VendorID = dr["VendorID"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["VendorID"]);
                        oNewMAS_VendorBE.Vendor_No = dr["Vendor_No"] == DBNull.Value ? null : Convert.ToString(dr["Vendor_No"]);
                        oNewMAS_VendorBE.Vendor_Name = dr["Vendor_Name"] == DBNull.Value ? null : Convert.ToString(dr["Vendor_Name"]);
                        oNewMAS_VendorBE.Vendor_Country = dr["Vendor_Country"] == DBNull.Value ? null : Convert.ToString(dr["Vendor_Country"]);
                        oNewMAS_VendorBE.NoOfSKU = dr["NoOfSKU"] == DBNull.Value ? null : Convert.ToString(dr["NoOfSKU"]);
                        oNewMAS_VendorBE.Vendor = Convert.ToString(dr["Vendor_No"]) + " - " + Convert.ToString(dr["Vendor_Name"]);
                        oNewMAS_VendorBE.RegistrationStatus = dr["RegistrationStatus"] == DBNull.Value ? null : Convert.ToString(dr["RegistrationStatus"]);
                        oNewMAS_VendorBE.ContactDefined = dr["ContactDefined"] == DBNull.Value ? null : Convert.ToString(dr["ContactDefined"]);
                        oNewMAS_VendorBE.VendorSite = dr["VendorsSite"] == DBNull.Value ? null : Convert.ToString(dr["VendorsSite"]);
                        oNewMAS_VendorBE.IsActiveVendor = dr["IsActiveVendor"] == DBNull.Value ? null : Convert.ToString(dr["IsActiveVendor"]); /// Added on 21 Feb 2013
                        //------Stage 7 Point 1------//
                        if (dr.Table.Columns.Contains("EuropeanorLocal"))
                            oNewMAS_VendorBE.EuropeanorLocal = dr["EuropeanorLocal"] == DBNull.Value ? 2 : Convert.ToInt32(dr["EuropeanorLocal"]); 
                        //-------------------------------//
                        oMAS_VendorBEList.Add(oNewMAS_VendorBE);
                    }
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return oMAS_VendorBEList;
        }



        public MAS_VendorBE UpdateVendorBAL(MAS_VendorBE oMAS_VendorBE)
        {

            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[4];
                param[index++] = new SqlParameter("@Action", oMAS_VendorBE.Action);
                param[index++] = new SqlParameter("@VendorID", oMAS_VendorBE.VendorID);
                param[index++] = new SqlParameter("@IsActiveVendor", oMAS_VendorBE.IsActiveVendor); /// Added on 21 Feb 2013
                //------Stage 7 Point 1------//                                                                                     /// 
                param[index++] = new SqlParameter("@EuropeanorLocal", oMAS_VendorBE.EuropeanorLocal);           
                //---------------------------// 

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spMAS_Vendor", param);
                oMAS_VendorBE = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return oMAS_VendorBE;
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessEntities.ModuleBE.Appointment.Booking;
using System.Data.SqlClient;
using System.Data;
using Utilities;
using BusinessEntities.ModuleBE.Languages.Languages;

namespace DataAccessLayer.ModuleDAL.Appointment.Booking
{
    public class APPBOK_CommunicationDAL
    {
        public int? AddItemDAL(APPBOK_CommunicationBE oAPPBOK_CommunicationBE)
        {
            int? intResult = 0;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[8];
                param[index++] = new SqlParameter("@Action", oAPPBOK_CommunicationBE.Action);
                param[index++] = new SqlParameter("@CommunicationType", oAPPBOK_CommunicationBE.CommunicationType);
                param[index++] = new SqlParameter("@BookingID", oAPPBOK_CommunicationBE.BookingID);
                param[index++] = new SqlParameter("@Subject", oAPPBOK_CommunicationBE.Subject);
                param[index++] = new SqlParameter("@SentFrom", oAPPBOK_CommunicationBE.SentFrom);
                param[index++] = new SqlParameter("@SentTo", oAPPBOK_CommunicationBE.SentTo);
                param[index++] = new SqlParameter("@Body", oAPPBOK_CommunicationBE.Body);
                param[index++] = new SqlParameter("@SendByID", oAPPBOK_CommunicationBE.SendByID);

                intResult = SqlHelper.ExecuteNonQuery(DBConnection.Connection, CommandType.StoredProcedure, "spMASSIT_BookingCommunication", param);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public List<APPBOK_CommunicationBE> GetItemsDAL(APPBOK_CommunicationBE oAPPBOK_CommunicationBE)
        {
            DataTable dt = new DataTable();
            List<APPBOK_CommunicationBE> oAPPBOK_CommunicationBEList = new List<APPBOK_CommunicationBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[2];
                param[index++] = new SqlParameter("@Action", oAPPBOK_CommunicationBE.Action);
                param[index++] = new SqlParameter("@BookingID", oAPPBOK_CommunicationBE.BookingID);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spMASSIT_BookingCommunication", param);

                if (ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                    foreach (DataRow dr in dt.Rows)
                    {
                        APPBOK_CommunicationBE oNewAPPBOK_CommunicationBE = new APPBOK_CommunicationBE();
                        oNewAPPBOK_CommunicationBE.CommunicationID = dr["CommunicationID"] != DBNull.Value ? Convert.ToInt32(dr["CommunicationID"]) : 0;
                        oNewAPPBOK_CommunicationBE.BookingID = dr["BookingID"] != DBNull.Value ? Convert.ToInt32(dr["BookingID"]) : 0;
                        oNewAPPBOK_CommunicationBE.CommunicationType = dr["CommunicationType"] != DBNull.Value ? Convert.ToString(dr["CommunicationType"]) : string.Empty;
                        oNewAPPBOK_CommunicationBE.SentFrom = dr["SentFrom"] != DBNull.Value ? Convert.ToString(dr["SentFrom"]) : string.Empty;
                        oNewAPPBOK_CommunicationBE.SentTo = dr["SentTo"] != DBNull.Value ? Convert.ToString(dr["SentTo"]) : string.Empty;
                        oNewAPPBOK_CommunicationBE.FullName = Convert.ToString(dr["FirstName"]) + " " + Convert.ToString(dr["Lastname"].ToString());
                        oNewAPPBOK_CommunicationBE.Subject = dr["Subject"] != DBNull.Value ? Convert.ToString(dr["Subject"]) : string.Empty;
                        oNewAPPBOK_CommunicationBE.Body = dr["Body"] != DBNull.Value ? Convert.ToString(dr["Body"]) : string.Empty;
                        oNewAPPBOK_CommunicationBE.SentDate = dr["SentDate"] != DBNull.Value ? Convert.ToDateTime(dr["SentDate"].ToString()).ToString("dd/MM/yyyy - HH:mm") : "";

                        oAPPBOK_CommunicationBEList.Add(oNewAPPBOK_CommunicationBE);
                    }
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return oAPPBOK_CommunicationBEList;
        }

        public APPBOK_CommunicationBE GetItemDAL(APPBOK_CommunicationBE oAPPBOK_CommunicationBE)
        {
            DataTable dt = new DataTable();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[2];
                param[index++] = new SqlParameter("@Action", oAPPBOK_CommunicationBE.Action);
                param[index++] = new SqlParameter("@CommunicationID", oAPPBOK_CommunicationBE.CommunicationID);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spMASSIT_BookingCommunication", param);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    DataRow dr = ds.Tables[0].Rows[0];
                    APPBOK_CommunicationBE oNewAPPBOK_CommunicationBE = new APPBOK_CommunicationBE();
                    oAPPBOK_CommunicationBE.CommunicationID = dr["CommunicationID"] != DBNull.Value ? Convert.ToInt32(dr["CommunicationID"]) : 0;
                    oAPPBOK_CommunicationBE.BookingID = dr["BookingID"] != DBNull.Value ? Convert.ToInt32(dr["BookingID"]) : 0;
                    oAPPBOK_CommunicationBE.CommunicationType = dr["CommunicationType"] != DBNull.Value ? Convert.ToString(dr["CommunicationType"]) : string.Empty;
                    oAPPBOK_CommunicationBE.SentFrom = dr["SentFrom"] != DBNull.Value ? Convert.ToString(dr["SentFrom"]) : string.Empty;
                    oAPPBOK_CommunicationBE.SentTo = dr["SentTo"] != DBNull.Value ? Convert.ToString(dr["SentTo"]) : string.Empty;
                    oAPPBOK_CommunicationBE.FullName = Convert.ToString(dr["FirstName"]) + " " + Convert.ToString(dr["Lastname"].ToString());
                    oAPPBOK_CommunicationBE.Subject = dr["Subject"] != DBNull.Value ? Convert.ToString(dr["Subject"]) : string.Empty;
                    oAPPBOK_CommunicationBE.Body = dr["Body"] != DBNull.Value ? Convert.ToString(dr["Body"]) : string.Empty;
                    oAPPBOK_CommunicationBE.SentDate = dr["SentDate"] != DBNull.Value ? Convert.ToDateTime(dr["SentDate"].ToString()).ToString("dd/MM/yyyy - HH:mm") : "";
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return oAPPBOK_CommunicationBE;
        }

        public int? AddBookingCommunicationDAL(APPBOK_CommunicationBE oAPPBOK_CommunicationBE)
        {
            int? intResult = 0;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[8];
                param[index++] = new SqlParameter("@Action", oAPPBOK_CommunicationBE.Action);
                param[index++] = new SqlParameter("@CommunicationType", oAPPBOK_CommunicationBE.CommunicationType);
                param[index++] = new SqlParameter("@BookingID", oAPPBOK_CommunicationBE.BookingID);
                param[index++] = new SqlParameter("@Subject", oAPPBOK_CommunicationBE.Subject);
                param[index++] = new SqlParameter("@SentFrom", oAPPBOK_CommunicationBE.SentFrom);
                param[index++] = new SqlParameter("@SentTo", oAPPBOK_CommunicationBE.SentTo);
                param[index++] = new SqlParameter("@Body", oAPPBOK_CommunicationBE.Body);
                param[index++] = new SqlParameter("@SendByID", oAPPBOK_CommunicationBE.SendByID);

                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spMASSIT_BookingCommunication", param);

                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
                else
                    intResult = 0;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public List<MAS_LanguageBE> GetLanguages()
        {
            DataTable dt = new DataTable();
            List<MAS_LanguageBE> oMAS_LanguageBEList = new List<MAS_LanguageBE>();
            try
            {
                int index = 0;
                //SqlParameter[] param = new SqlParameter[2];
                //param[index++] = new SqlParameter("@Action", oAPPBOK_CommunicationBE.Action);
                //param[index++] = new SqlParameter("@BookingID", oAPPBOK_CommunicationBE.BookingID);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spMAS_Language");
                if (ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                    foreach (DataRow dr in dt.Rows)
                    {
                        MAS_LanguageBE oNewMAS_LanguageBE = new MAS_LanguageBE();

                        oNewMAS_LanguageBE.LanguageID = dr["LanguageID"] != DBNull.Value ? Convert.ToInt32(dr["LanguageID"]) : 0;
                        oNewMAS_LanguageBE.Language = dr["Language"] != DBNull.Value ? Convert.ToString(dr["Language"]) : "";
                        oNewMAS_LanguageBE.ISOLanguageName = dr["ISOLanguageName"] != DBNull.Value ? Convert.ToString(dr["ISOLanguageName"]) : string.Empty;
                        oNewMAS_LanguageBE.IsEnable = dr["IsEnable"] != DBNull.Value ? Convert.ToBoolean(dr["IsEnable"]) : false;
                        oNewMAS_LanguageBE.IsActiveForRegistration = dr["IsActiveForRegistration"] != DBNull.Value ? Convert.ToBoolean(dr["IsActiveForRegistration"]) : false;
                        oNewMAS_LanguageBE.ImagePath = Convert.ToString(dr["ImagePath"]) + " " + Convert.ToString(dr["ImagePath"].ToString());

                        oMAS_LanguageBEList.Add(oNewMAS_LanguageBE);
                    }
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return oMAS_LanguageBEList;
        }

    }
}

﻿using System;
using System.Data.SqlClient;
using System.Data;

using BusinessEntities.ModuleBE.Appointment.CountrySetting;
using Utilities;
using System.Collections.Generic;
using BusinessEntities.ModuleBE.Appointment.SiteSettings;

namespace DataAccessLayer.ModuleDAL.Appointment.CountrySetting {
    public class APPCNT_CarrierDAL : DataAccessLayer.BaseDAL {

        public List<MASCNT_CarrierBE> GetCarrierDetailsDAL(MASCNT_CarrierBE oMASCNT_CarrierBE) {
            DataTable dt = new DataTable();
            List<MASCNT_CarrierBE> oMASCNT_CarrierBEList = new List<MASCNT_CarrierBE>();
            try {
                int index = 0;
                SqlParameter[] param = new SqlParameter[8];
                param[index++] = new SqlParameter("@Action", oMASCNT_CarrierBE.Action);
                param[index++] = new SqlParameter("@CarrierID", oMASCNT_CarrierBE.CarrierID);
                param[index++] = new SqlParameter("@CarrierName", oMASCNT_CarrierBE.CarrierName);
                param[index++] = new SqlParameter("@CountryID", oMASCNT_CarrierBE.CountryID);
                //oMASCNT_CarrierBE.Site = new BusinessEntities.ModuleBE.AdminFunctions.MAS_SiteBE();
                if (oMASCNT_CarrierBE.Site != null)
                param[index++] = new SqlParameter("@SiteID", oMASCNT_CarrierBE.Site.SiteID);
                //oMASCNT_CarrierBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
                if (oMASCNT_CarrierBE.User != null)
                param[index++] = new SqlParameter("@UserID", oMASCNT_CarrierBE.User.UserID); 
               if(oMASCNT_CarrierBE.Discrepancy != null)
                param[index++] = new SqlParameter("@VDRNumber", oMASCNT_CarrierBE.Discrepancy.VDRNo);
               param[index++] = new SqlParameter("@CountryIDs", oMASCNT_CarrierBE.CountryIDs);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spMASCNT_Carrier", param);

                dt =  ds.Tables[0];                

                foreach (DataRow dr in dt.Rows) {
                    MASCNT_CarrierBE oNewMASCNT_CarrierBE = new MASCNT_CarrierBE();
                    oNewMASCNT_CarrierBE.CarrierID = Convert.ToInt32(dr["CarrierID"]);

                    oNewMASCNT_CarrierBE.Country = new BusinessEntities.ModuleBE.AdminFunctions.MAS_CountryBE();
                    oNewMASCNT_CarrierBE.Country.CountryName = dr["CountryName"].ToString();
                                        
                    
                    oNewMASCNT_CarrierBE.CarrierName = dr["CarrierName"].ToString();
                    oNewMASCNT_CarrierBE.CountryID = Convert.ToInt32(dr["CountryID"]);

                    if (ds.Tables[0].Columns.Contains("IsNarrativeRequired")) 
                    oNewMASCNT_CarrierBE.IsNarrativeRequired = dr["IsNarrativeRequired"] != DBNull.Value ? Convert.ToBoolean(dr["IsNarrativeRequired"]) : false;

                    oMASCNT_CarrierBEList.Add(oNewMASCNT_CarrierBE);
                }
            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return oMASCNT_CarrierBEList;
        }


        public DataTable GetAllCarrierDetailsDAL(MASCNT_CarrierBE oMASCNT_CarrierBE) {
            DataTable dt = new DataTable();

             try {
                int index = 0;
                SqlParameter[] param = new SqlParameter[8];
                param[index++] = new SqlParameter("@Action", oMASCNT_CarrierBE.Action);
                param[index++] = new SqlParameter("@CarrierID", oMASCNT_CarrierBE.CarrierID);
                param[index++] = new SqlParameter("@CarrierName", oMASCNT_CarrierBE.CarrierName);
                param[index++] = new SqlParameter("@CountryID", oMASCNT_CarrierBE.CountryID);
                //oMASCNT_CarrierBE.Site = new BusinessEntities.ModuleBE.AdminFunctions.MAS_SiteBE();
                if (oMASCNT_CarrierBE.Site != null)
                param[index++] = new SqlParameter("@SiteID", oMASCNT_CarrierBE.Site.SiteID);
                //oMASCNT_CarrierBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
                if (oMASCNT_CarrierBE.User != null)
                param[index++] = new SqlParameter("@UserID", oMASCNT_CarrierBE.User.UserID); 
               if(oMASCNT_CarrierBE.Discrepancy != null)
                param[index++] = new SqlParameter("@VDRNumber", oMASCNT_CarrierBE.Discrepancy.VDRNo);
               param[index++] = new SqlParameter("@CountryIDs", oMASCNT_CarrierBE.CountryIDs);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spMASCNT_Carrier", param);

                dt = ds.Tables[0];
             }
             catch (Exception ex) {
                 LogUtility.SaveErrorLogEntry(ex);
             }
             return dt;
        }

        public int? addEditCarrierDetailsDAL(MASCNT_CarrierBE oMASCNT_CarrierBE) {
            int? intResult = 0;
            try {
                int index = 0;
                SqlParameter[] param = new SqlParameter[6];
                param[index++] = new SqlParameter("@Action", oMASCNT_CarrierBE.Action);
                param[index++] = new SqlParameter("@CarrierID", oMASCNT_CarrierBE.CarrierID);
                param[index++] = new SqlParameter("@CountryID", oMASCNT_CarrierBE.CountryID);
                param[index++] = new SqlParameter("@CarrierName", oMASCNT_CarrierBE.CarrierName);
                param[index++] = new SqlParameter("@IsActive", oMASCNT_CarrierBE.IsActive);
                param[index++] = new SqlParameter("@IsNarrativeRequired", oMASCNT_CarrierBE.IsNarrativeRequired);


                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spMASCNT_Carrier", param);

                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
                else
                    intResult = 0;

            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public List<MASCNT_CarrierBE> GetAllCarriersDAL(MASCNT_CarrierBE oMASCNT_CarrierBE)
        {
            DataTable dt = new DataTable();
            List<MASCNT_CarrierBE> oMASCNT_CarrierBEList = new List<MASCNT_CarrierBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[8];
                param[index++] = new SqlParameter("@Action", oMASCNT_CarrierBE.Action);
                param[index++] = new SqlParameter("@CountryID", oMASCNT_CarrierBE.CountryID);
                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), 
                    CommandType.StoredProcedure, "spMASCNT_Carrier", param);
                dt = ds.Tables[0];
                foreach (DataRow dr in dt.Rows)
                {
                    MASCNT_CarrierBE oNewMASCNT_CarrierBE = new MASCNT_CarrierBE();
                    oNewMASCNT_CarrierBE.CarrierID = Convert.ToInt32(dr["CarrierID"]);
                    oNewMASCNT_CarrierBE.CarrierName = dr["CarrierName"].ToString();
                    oNewMASCNT_CarrierBE.CountryID = Convert.ToInt32(dr["CountryID"]);
                    oMASCNT_CarrierBEList.Add(oNewMASCNT_CarrierBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return oMASCNT_CarrierBEList;
        }

        public int? addEditCarrierMiscConsDAL(MASCNT_CarrierBE oMASCNT_CarrierBE)
        {
            int? intResult = 0;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[17];
                param[index++] = new SqlParameter("@Action", oMASCNT_CarrierBE.Action);
                param[index++] = new SqlParameter("@SiteCarrierID", oMASCNT_CarrierBE.SiteCarrierID);
                param[index++] = new SqlParameter("@SiteID", oMASCNT_CarrierBE.SiteID);
                param[index++] = new SqlParameter("@CarrierID", oMASCNT_CarrierBE.CarrierID);
                param[index++] = new SqlParameter("@APP_CheckingRequired", oMASCNT_CarrierBE.APP_CheckingRequired);
                param[index++] = new SqlParameter("@APP_MaximumPallets", oMASCNT_CarrierBE.APP_MaximumPallets);
                param[index++] = new SqlParameter("@APP_MaximumLines", oMASCNT_CarrierBE.APP_MaximumLines);
                param[index++] = new SqlParameter("@APP_MaximumDeliveriesInADay", oMASCNT_CarrierBE.APP_MaximumDeliveriesInADay);
                param[index++] = new SqlParameter("@APP_CannotDeliveryOnMonday", oMASCNT_CarrierBE.APP_CannotDeliveryOnMonday);
                param[index++] = new SqlParameter("@APP_CannotDeliveryOnTuesday", oMASCNT_CarrierBE.APP_CannotDeliveryOnTuesday);
                param[index++] = new SqlParameter("@APP_CannotDeliveryOnWednessday", oMASCNT_CarrierBE.APP_CannotDeliveryOnWednessday);
                param[index++] = new SqlParameter("@APP_CannotDeliveryOnThrusday", oMASCNT_CarrierBE.APP_CannotDeliveryOnThrusday);
                param[index++] = new SqlParameter("@APP_CannotDeliveryOnFriday", oMASCNT_CarrierBE.APP_CannotDeliveryOnFriday);
                param[index++] = new SqlParameter("@APP_CannotDeliveryOnSaturday", oMASCNT_CarrierBE.APP_CannotDeliveryOnSaturday);
                param[index++] = new SqlParameter("@APP_CannotDeliveryOnSunday", oMASCNT_CarrierBE.APP_CannotDeliveryOnSunday);
                param[index++] = new SqlParameter("@AfterSlotTimeId", oMASCNT_CarrierBE.AfterSlotTimeID);
                param[index++] = new SqlParameter("@BeforeSlotTimeId", oMASCNT_CarrierBE.BeforeSlotTimeID);                
                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spMASCNT_Carrier", param);

                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
                else
                    intResult = 0;

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public int? DeleteCarrierMiscConsDAL(MASCNT_CarrierBE oMASCNT_CarrierBE)
        {
            int? intResult = 0;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[17];
                param[index++] = new SqlParameter("@Action", oMASCNT_CarrierBE.Action);
                param[index++] = new SqlParameter("@SiteCarrierID", oMASCNT_CarrierBE.SiteCarrierID);
                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spMASCNT_Carrier", param);
                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
                else
                    intResult = 0;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public int? IsCarrierMiscConsExistDAL(MASCNT_CarrierBE oMASCNT_CarrierBE)
        {
            int? intResult = 0;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[4];
                param[index++] = new SqlParameter("@Action", oMASCNT_CarrierBE.Action);
                param[index++] = new SqlParameter("@SiteID", oMASCNT_CarrierBE.SiteID);
                param[index++] = new SqlParameter("@CarrierID", oMASCNT_CarrierBE.CarrierID);
                param[index++] = new SqlParameter("@SiteCarrierID", oMASCNT_CarrierBE.SiteCarrierID);      
                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spMASCNT_Carrier", param);

                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
                else
                    intResult = 0;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public List<MASSIT_FixedSlotBE> GetAllFixedSlotByCarrierIDDAL(MASSIT_FixedSlotBE oMASSIT_FixedSlotBE)
        {
            DataTable dt = new DataTable();
            List<MASSIT_FixedSlotBE> oMASSIT_FixedSlotBEList = new List<MASSIT_FixedSlotBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[5];
                param[index++] = new SqlParameter("@Action", oMASSIT_FixedSlotBE.Action);
                param[index++] = new SqlParameter("@SiteID", oMASSIT_FixedSlotBE.SiteID);
                param[index++] = new SqlParameter("@FixedSlotID", oMASSIT_FixedSlotBE.FixedSlotID);
                param[index++] = new SqlParameter("@UserID", oMASSIT_FixedSlotBE.User.UserID);
                param[index++] = new SqlParameter("@CarrierID", oMASSIT_FixedSlotBE.CarrierID);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spMASSIT_FixedSlot", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    MASSIT_FixedSlotBE oNewMASSIT_FixedSlotBE = new MASSIT_FixedSlotBE();

                    oNewMASSIT_FixedSlotBE.FixedSlotID = Convert.ToInt32(dr["FixedSlotID"]);
                    oNewMASSIT_FixedSlotBE.SiteID = Convert.ToInt32(dr["SiteID"]);
                    oNewMASSIT_FixedSlotBE.CarrierID = dr["CarrierID"] != DBNull.Value ? Convert.ToInt32(dr["CarrierID"]) : (int?)null;
                    oNewMASSIT_FixedSlotBE.SlotTimeID = dr["SlotTimeID"] != DBNull.Value ? Convert.ToInt32(dr["SlotTimeID"]) : (int?)null;
                    oNewMASSIT_FixedSlotBE.SlotTime = new BusinessEntities.ModuleBE.AdminFunctions.SYS_SlotTimeBE();
                    oNewMASSIT_FixedSlotBE.SlotTime.SlotTime = dr["SlotTime"].ToString();
                    oNewMASSIT_FixedSlotBE.Monday = dr["Monday"] != DBNull.Value ? Convert.ToBoolean(dr["Monday"]) : (bool?)null;
                    oNewMASSIT_FixedSlotBE.Tuesday = dr["Tuesday"] != DBNull.Value ? Convert.ToBoolean(dr["Tuesday"]) : (bool?)null;
                    oNewMASSIT_FixedSlotBE.Wednesday = dr["Wednesday"] != DBNull.Value ? Convert.ToBoolean(dr["Wednesday"]) : (bool?)null;
                    oNewMASSIT_FixedSlotBE.Thursday = dr["Thursday"] != DBNull.Value ? Convert.ToBoolean(dr["Thursday"]) : (bool?)null;
                    oNewMASSIT_FixedSlotBE.Friday = dr["Friday"] != DBNull.Value ? Convert.ToBoolean(dr["Friday"]) : (bool?)null;
                    oNewMASSIT_FixedSlotBE.Saturday = dr["Saturday"] != DBNull.Value ? Convert.ToBoolean(dr["Saturday"]) : (bool?)null;
                    oNewMASSIT_FixedSlotBE.Sunday = dr["Sunday"] != DBNull.Value ? Convert.ToBoolean(dr["Sunday"]) : (bool?)null;
                    oNewMASSIT_FixedSlotBE.MaximumCatrons = dr["MaximumCatrons"] != DBNull.Value ? Convert.ToInt32(dr["MaximumCatrons"]) : (int?)null;
                    oNewMASSIT_FixedSlotBE.MaximumLines = dr["MaximumLines"] != DBNull.Value ? Convert.ToInt32(dr["MaximumLines"]) : (int?)null;
                    oNewMASSIT_FixedSlotBE.MaximumPallets = dr["MaximumPallets"] != DBNull.Value ? Convert.ToInt32(dr["MaximumPallets"]) : (int?)null;

                    oMASSIT_FixedSlotBEList.Add(oNewMASSIT_FixedSlotBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return oMASSIT_FixedSlotBEList;
        }

        public List<MASCNT_CarrierBE> GetAllCarrierMiscConsDAL(MASCNT_CarrierBE oMASCNT_CarrierBE)
        {
            DataTable dt = new DataTable();
            List<MASCNT_CarrierBE> oMASCNT_CarrierBEList = new List<MASCNT_CarrierBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[8];
                param[index++] = new SqlParameter("@Action", oMASCNT_CarrierBE.Action);
                param[index++] = new SqlParameter("@SiteCarrierID", oMASCNT_CarrierBE.SiteCarrierID);
                param[index++] = new SqlParameter("@CarrierID", oMASCNT_CarrierBE.CarrierID);
                param[index++] = new SqlParameter("@SiteID", oMASCNT_CarrierBE.SiteID);
                if (oMASCNT_CarrierBE.User != null)
                param[index++] = new SqlParameter("@UserID", oMASCNT_CarrierBE.User.UserID);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), 
                    CommandType.StoredProcedure, "spMASCNT_Carrier", param);
                dt = ds.Tables[0];
                foreach (DataRow dr in dt.Rows)
                {
                    MASCNT_CarrierBE oNewMASCNT_CarrierBE = new MASCNT_CarrierBE();
                    oNewMASCNT_CarrierBE.SiteCarrierID = dr["SiteCarrierID"] != DBNull.Value ? Convert.ToInt32(dr["SiteCarrierID"]) : 0;
                    oNewMASCNT_CarrierBE.SiteID = dr["SiteID"] != DBNull.Value ? Convert.ToInt32(dr["SiteID"]) : 0;
                    oNewMASCNT_CarrierBE.SiteName = dr["SiteName"] != DBNull.Value ? Convert.ToString(dr["SiteName"]) : String.Empty;
                    oNewMASCNT_CarrierBE.CarrierID = dr["CarrierID"] != DBNull.Value ? Convert.ToInt32(dr["CarrierID"]) : 0;
                    oNewMASCNT_CarrierBE.CarrierName = dr["CarrierName"] != DBNull.Value ? Convert.ToString(dr["CarrierName"]) : String.Empty;
                    oNewMASCNT_CarrierBE.APP_CheckingRequired = dr["APP_CheckingRequired"] != DBNull.Value ? Convert.ToBoolean(dr["APP_CheckingRequired"]) : false;
                    oNewMASCNT_CarrierBE.APP_MaximumPallets = dr["APP_MaximumPallets"] != DBNull.Value ? Convert.ToInt32(dr["APP_MaximumPallets"]) : 0;
                    oNewMASCNT_CarrierBE.APP_MaximumLines = dr["APP_MaximumLines"] != DBNull.Value ? Convert.ToInt32(dr["APP_MaximumLines"]) : 0;
                    oNewMASCNT_CarrierBE.APP_MaximumDeliveriesInADay = dr["APP_MaximumDeliveriesInADay"] != DBNull.Value ? Convert.ToInt32(dr["APP_MaximumDeliveriesInADay"]) : 0;
                    oNewMASCNT_CarrierBE.APP_CannotDeliveryOnMonday = dr["APP_CannotDeliveryOnMonday"] != DBNull.Value ? Convert.ToBoolean(dr["APP_CannotDeliveryOnMonday"]) : false;
                    oNewMASCNT_CarrierBE.APP_CannotDeliveryOnTuesday = dr["APP_CannotDeliveryOnTuesday"] != DBNull.Value ? Convert.ToBoolean(dr["APP_CannotDeliveryOnTuesday"]) : false;
                    oNewMASCNT_CarrierBE.APP_CannotDeliveryOnWednessday = dr["APP_CannotDeliveryOnWednessday"] != DBNull.Value ? Convert.ToBoolean(dr["APP_CannotDeliveryOnWednessday"]) : false;
                    oNewMASCNT_CarrierBE.APP_CannotDeliveryOnThrusday = dr["APP_CannotDeliveryOnThrusday"] != DBNull.Value ? Convert.ToBoolean(dr["APP_CannotDeliveryOnThrusday"]) : false;
                    oNewMASCNT_CarrierBE.APP_CannotDeliveryOnFriday = dr["APP_CannotDeliveryOnFriday"] != DBNull.Value ? Convert.ToBoolean(dr["APP_CannotDeliveryOnFriday"]) : false;
                    oNewMASCNT_CarrierBE.APP_CannotDeliveryOnSaturday = dr["APP_CannotDeliveryOnSaturday"] != DBNull.Value ? Convert.ToBoolean(dr["APP_CannotDeliveryOnSaturday"]) : false;
                    oNewMASCNT_CarrierBE.APP_CannotDeliveryOnSunday = dr["APP_CannotDeliveryOnSunday"] != DBNull.Value ? Convert.ToBoolean(dr["APP_CannotDeliveryOnSunday"]) : false;
                    oNewMASCNT_CarrierBE.AfterSlotTimeID = dr["AfterSlotTimeId"] != DBNull.Value ? Convert.ToInt32(dr["AfterSlotTimeId"]) : 0;
                    oNewMASCNT_CarrierBE.AfterSlotTime = dr["AfterSlotTime"] != DBNull.Value ? Convert.ToString(dr["AfterSlotTime"]) : String.Empty;
                    oNewMASCNT_CarrierBE.BeforeSlotTimeID = dr["BeforeSlotTimeId"] != DBNull.Value ? Convert.ToInt32(dr["BeforeSlotTimeId"]) : 0;
                    oNewMASCNT_CarrierBE.BeforeSlotTime = dr["BeforeSlotTime"] != DBNull.Value ? Convert.ToString(dr["BeforeSlotTime"]) : String.Empty;
                    //----Sprint 3b--------------//
                    if (dr["BeforeOrderBYId"] != null)
                        oNewMASCNT_CarrierBE.BeforeOrderBYId = dr["BeforeOrderBYId"] != DBNull.Value ? Convert.ToInt32(dr["BeforeOrderBYId"]) : 0;

                    if (dr["AfterOrderBYId"] != null)
                        oNewMASCNT_CarrierBE.AfterOrderBYId = dr["AfterOrderBYId"] != DBNull.Value ? Convert.ToInt32(dr["AfterOrderBYId"]) : 0;
                    //-------------------------//

                    //oNewMASCNT_CarrierBE.APP_CannotDeliverAfter = dr["APP_CannotDeliverAfter"] != DBNull.Value ? Convert.ToDateTime(dr["APP_CannotDeliverAfter"]) : DateTime.Now;
                    //oNewMASCNT_CarrierBE.APP_CannotDeliverBefore = dr["APP_CannotDeliverBefore"] != DBNull.Value ? Convert.ToDateTime(dr["APP_CannotDeliverBefore"]) : DateTime.Now;
                    oMASCNT_CarrierBEList.Add(oNewMASCNT_CarrierBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return oMASCNT_CarrierBEList;
        }
    }
}

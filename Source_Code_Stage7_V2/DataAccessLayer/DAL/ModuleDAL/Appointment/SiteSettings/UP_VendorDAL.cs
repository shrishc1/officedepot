﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using Utilities;
using BusinessEntities.ModuleBE.Security;
using BusinessEntities.ModuleBE.AdminFunctions;
using BusinessEntities.ModuleBE.Upload;

namespace DataAccessLayer.ModuleDAL.Appointment.SiteSettings {
    public class UP_VendorDAL : BaseDAL {
        public List<UP_VendorBE> GetVendorsDAL(UP_VendorBE oUP_VendorBE) {
            DataTable dt = new DataTable();
            List<UP_VendorBE> oUP_VendorBEList = new List<UP_VendorBE>();
            try {
                int index = 0;
                SqlParameter[] param = new SqlParameter[5];
                param[index++] = new SqlParameter("@Action", oUP_VendorBE.Action);
                param[index++] = new SqlParameter("@IsChildRequired", oUP_VendorBE.IsChildRequired);
                param[index++] = new SqlParameter("@IsParentRequired", oUP_VendorBE.IsParentRequired);
                param[index++] = new SqlParameter("@IsStandAloneRequired", oUP_VendorBE.IsStandAloneRequired);
                param[index++] = new SqlParameter("@IsGrandParentRequired", oUP_VendorBE.IsGrandParentRequired);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spUP_Vendor", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows) {
                    UP_VendorBE oNewUP_VendorBE = new UP_VendorBE();
                    oNewUP_VendorBE.VendorID = Convert.ToInt32(dr["VendorID"]);
                    oNewUP_VendorBE.CountryID = Convert.ToInt32(dr["CountryID"]);
                    oNewUP_VendorBE.CountryName = dr["Vendor_Country"].ToString();
                    oNewUP_VendorBE.Vendor_No = dr["Vendor_No"].ToString();
                    oNewUP_VendorBE.VendorName = dr["VendorName"].ToString();
                    oNewUP_VendorBE.ParentVendorID = dr["ParentVendorID"] != DBNull.Value ? Convert.ToInt32(dr["ParentVendorID"]) : -1;
                    oNewUP_VendorBE.IsActiveVendor = dr["IsActiveVendor"] == DBNull.Value ? null : Convert.ToString(dr["IsActiveVendor"]); /// Added on 21 Feb 2013
                    oNewUP_VendorBE.StockPlannerName = dr["StockPlannerName"] == DBNull.Value ? null : Convert.ToString(dr["StockPlannerName"]);
                    oNewUP_VendorBE.StockPlannerContact = dr["PhoneNumber"] == DBNull.Value ? null : Convert.ToString(dr["PhoneNumber"]);/// 
                    oUP_VendorBEList.Add(oNewUP_VendorBE);
                }
            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return oUP_VendorBEList;
        }

        public List<UP_VendorBE> GetMasterVendorsDAL(UP_VendorBE oUP_VendorBE) {
            DataTable dt = new DataTable();
            List<UP_VendorBE> oUP_VendorBEList = new List<UP_VendorBE>();
            try {
                int index = 0;
                SqlParameter[] param = new SqlParameter[1];
                param[index++] = new SqlParameter("@Action", oUP_VendorBE.Action);


                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spUP_Vendor", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows) {
                    UP_VendorBE oNewUP_VendorBE = new UP_VendorBE();
                    oNewUP_VendorBE.VendorIDs = Convert.ToString(dr["VendorIDs"]);
                    oNewUP_VendorBE.VendorName = dr["VendorName"].ToString();
                    oNewUP_VendorBE.MasterParentVendorID = dr["MasterParentVendorID"] != DBNull.Value ? Convert.ToInt32(dr["MasterParentVendorID"]) : -1;

                    oUP_VendorBEList.Add(oNewUP_VendorBE);
                }
            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return oUP_VendorBEList;
        }

        public List<UP_VendorBE> GetCarrierDetailsDAL(UP_VendorBE oUP_VendorBE) {
            DataTable dt = new DataTable();
            List<UP_VendorBE> oUP_VendorBEList = new List<UP_VendorBE>();
            try {
                int index = 0;
                SqlParameter[] param = new SqlParameter[13];
                param[index++] = new SqlParameter("@Action", oUP_VendorBE.Action);
                param[index++] = new SqlParameter("@VendorName", oUP_VendorBE.VendorName);
                param[index++] = new SqlParameter("@SiteID", oUP_VendorBE.Site.SiteID);
                param[index++] = new SqlParameter("@SiteCountryID", oUP_VendorBE.Site.SiteCountryID);
                param[index++] = new SqlParameter("@VendorFlag", oUP_VendorBE.VendorFlag);
                param[index++] = new SqlParameter("@Vendor_No", oUP_VendorBE.Vendor_No);
                param[index++] = new SqlParameter("@ConsolidatedVendorIds", oUP_VendorBE.ConsolidatedVendors);
                param[index++] = new SqlParameter("@IsChildRequired", oUP_VendorBE.IsChildRequired);
                param[index++] = new SqlParameter("@IsParentRequired", oUP_VendorBE.IsParentRequired);
                param[index++] = new SqlParameter("@IsStandAloneRequired", oUP_VendorBE.IsStandAloneRequired);
                param[index++] = new SqlParameter("@IsGrandParentRequired", oUP_VendorBE.IsGrandParentRequired);
                param[index++] = new SqlParameter("@IsCountryRequiredWithVendor", oUP_VendorBE.IsCountryRequiredWithVendor);
                param[index++] = new SqlParameter("@EuropeanorLocal", oUP_VendorBE.EuropeanorLocal);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spUP_Vendor", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows) {
                    UP_VendorBE oNewUP_VendorBE = new UP_VendorBE();
                    oNewUP_VendorBE.VendorID = Convert.ToInt32(dr["VendorID"]);
                    oNewUP_VendorBE.CountryID = dr["CountryID"] != DBNull.Value ? Convert.ToInt32(dr["CountryID"]) : -1;
                    oNewUP_VendorBE.Vendor_No = dr["Vendor_No"].ToString();
                    oNewUP_VendorBE.VendorName = dr["VendorName"].ToString();
                    oNewUP_VendorBE.ParentVendorID = dr["ParentVendorID"] != DBNull.Value ? Convert.ToInt32(dr["ParentVendorID"]) : -1;
                    oNewUP_VendorBE.IsActiveVendor = dr["IsActiveVendor"] == DBNull.Value ? null : Convert.ToString(dr["IsActiveVendor"]); /// Added on 21 Feb 2013
                    oUP_VendorBEList.Add(oNewUP_VendorBE);
                }
            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return oUP_VendorBEList;
        }

        public List<UP_VendorBE> ValidateVendorDAL(UP_VendorBE oUP_VendorBE) {
            DataTable dt = new DataTable();
            List<UP_VendorBE> oUP_VendorBEList = new List<UP_VendorBE>();

            try {
                int index = 0;

                SqlParameter[] param = new SqlParameter[9];
                param[index++] = new SqlParameter("@Action", oUP_VendorBE.Action);
                param[index++] = new SqlParameter("@Vendor_No", oUP_VendorBE.Vendor_No);
                param[index++] = new SqlParameter("@SiteID", oUP_VendorBE.Site.SiteID);
                param[index++] = new SqlParameter("@SiteCountryID", oUP_VendorBE.Site.SiteCountryID);
                param[index++] = new SqlParameter("@IsChildRequired", oUP_VendorBE.IsChildRequired);
                param[index++] = new SqlParameter("@IsParentRequired", oUP_VendorBE.IsParentRequired);
                param[index++] = new SqlParameter("@IsStandAloneRequired", oUP_VendorBE.IsStandAloneRequired);
                param[index++] = new SqlParameter("@IsGrandParentRequired", oUP_VendorBE.IsStandAloneRequired);

                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spUP_Vendor", param);

                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0) {

                    dt = Result.Tables[0];

                    foreach (DataRow dr in dt.Rows) {
                        UP_VendorBE oNewUP_VendorBE = new UP_VendorBE();
                        oNewUP_VendorBE.VendorID = Convert.ToInt32(dr["VendorID"]);
                        oNewUP_VendorBE.CountryID = Convert.ToInt32(dr["CountryID"]);
                        oNewUP_VendorBE.Vendor_No = dr["Vendor_No"].ToString();
                        oNewUP_VendorBE.VendorName = dr["VendorName"].ToString();
                        oNewUP_VendorBE.VendorDetails = new MASSIT_VendorBE();
                        oNewUP_VendorBE.VendorDetails.APP_IsBookingValidationExcluded = dr["APP_IsBookingValidationExcluded"] != DBNull.Value ? Convert.ToBoolean(dr["APP_IsBookingValidationExcluded"]) : (bool?)null;
                        oNewUP_VendorBE.VendorDetails.APP_IsNonTimeDeliveryAllowed = dr["APP_IsNonTimeDeliveryAllowed"] != DBNull.Value ? Convert.ToBoolean(dr["APP_IsNonTimeDeliveryAllowed"]) : (bool?)null;
                        oNewUP_VendorBE.VendorDetails.APP_MaximumDeliveriesInADay = dr["APP_MaximumDeliveriesInADay"] != DBNull.Value ? Convert.ToInt32(dr["APP_MaximumDeliveriesInADay"]) : (int?)null;
                        oNewUP_VendorBE.VendorDetails.APP_MaximumLines = dr["APP_MaximumLines"] != DBNull.Value ? Convert.ToInt32(dr["APP_MaximumLines"]) : (int?)null;
                        oNewUP_VendorBE.VendorDetails.APP_MaximumPallets = dr["APP_MaximumPallets"] != DBNull.Value ? Convert.ToInt32(dr["APP_MaximumPallets"]) : (int?)null;
                        oNewUP_VendorBE.VendorDetails.APP_CannotDeliverAfter = dr["APP_CannotDeliverAfter"] != DBNull.Value ? Convert.ToDateTime(dr["APP_CannotDeliverAfter"]) : (DateTime?)null;
                        oNewUP_VendorBE.VendorDetails.APP_CannotDeliverBefore = dr["APP_CannotDeliverBefore"] != DBNull.Value ? Convert.ToDateTime(dr["APP_CannotDeliverBefore"]) : (DateTime?)null;
                        oNewUP_VendorBE.VendorDetails.APP_CannotDeliveryOnFriday = dr["APP_CannotDeliveryOnFriday"] != DBNull.Value ? Convert.ToBoolean(dr["APP_CannotDeliveryOnFriday"]) : (bool?)null;
                        oNewUP_VendorBE.VendorDetails.APP_CannotDeliveryOnMonday = dr["APP_CannotDeliveryOnMonday"] != DBNull.Value ? Convert.ToBoolean(dr["APP_CannotDeliveryOnMonday"]) : (bool?)null;
                        oNewUP_VendorBE.VendorDetails.APP_CannotDeliveryOnSaturday = dr["APP_CannotDeliveryOnSaturday"] != DBNull.Value ? Convert.ToBoolean(dr["APP_CannotDeliveryOnSaturday"]) : (bool?)null;
                        oNewUP_VendorBE.VendorDetails.APP_CannotDeliveryOnSunday = dr["APP_CannotDeliveryOnSunday"] != DBNull.Value ? Convert.ToBoolean(dr["APP_CannotDeliveryOnSunday"]) : (bool?)null;
                        oNewUP_VendorBE.VendorDetails.APP_CannotDeliveryOnThrusday = dr["APP_CannotDeliveryOnThrusday"] != DBNull.Value ? Convert.ToBoolean(dr["APP_CannotDeliveryOnThrusday"]) : (bool?)null;
                        oNewUP_VendorBE.VendorDetails.APP_CannotDeliveryOnTuesday = dr["APP_CannotDeliveryOnTuesday"] != DBNull.Value ? Convert.ToBoolean(dr["APP_CannotDeliveryOnTuesday"]) : (bool?)null;
                        oNewUP_VendorBE.VendorDetails.APP_CannotDeliveryOnWednessday = dr["APP_CannotDeliveryOnWednessday"] != DBNull.Value ? Convert.ToBoolean(dr["APP_CannotDeliveryOnWednessday"]) : (bool?)null;
                        oNewUP_VendorBE.VendorDetails.BeforeSlotTimeID = dr["APP_BeforeSlotTimeID"] != DBNull.Value ? Convert.ToInt32(dr["APP_BeforeSlotTimeID"]) : 0;
                        oNewUP_VendorBE.VendorDetails.AfterSlotTimeID = dr["APP_AfterSlotTimeID"] != DBNull.Value ? Convert.ToInt32(dr["APP_AfterSlotTimeID"]) : 100;
                        oNewUP_VendorBE.VendorDetails.IsConstraintsDefined = dr["IsConstraintsDefined"] != DBNull.Value ? Convert.ToBoolean(dr["IsConstraintsDefined"]) : true;

                        //----Sprint 3b--------------//
                        if (dr["BeforeOrderBYId"] != null)
                            oNewUP_VendorBE.VendorDetails.BeforeOrderBYId = dr["BeforeOrderBYId"] != DBNull.Value ? Convert.ToInt32(dr["BeforeOrderBYId"]) : 0;

                        if (dr["AfterOrderBYId"] != null)
                            oNewUP_VendorBE.VendorDetails.AfterOrderBYId = dr["AfterOrderBYId"] != DBNull.Value ? Convert.ToInt32(dr["AfterOrderBYId"]) : 0;
                        //-------------------------//

                        oUP_VendorBEList.Add(oNewUP_VendorBE);
                    }
                }

            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oUP_VendorBEList;
        }

        /// <summary>
        /// Used for managing consolidating vendors
        /// </summary>
        /// <param name="oUP_VendorBE"></param>
        /// <returns></returns>
        public int? addEditConsolidateVendorDAL(UP_VendorBE oUP_VendorBE) {
            int? intResult = 0;
            try {
                int index = 0;
                SqlParameter[] param = new SqlParameter[7];
                param[index++] = new SqlParameter("@Action", oUP_VendorBE.Action);
                param[index++] = new SqlParameter("@VendorName", oUP_VendorBE.VendorName);
                param[index++] = new SqlParameter("@Vendor_No", oUP_VendorBE.Vendor_No);
                param[index++] = new SqlParameter("@SiteCountryID", oUP_VendorBE.CountryID);
                param[index++] = new SqlParameter("@VendorFlag", oUP_VendorBE.VendorFlag);
                param[index++] = new SqlParameter("@ConsolidatedVendorIds", oUP_VendorBE.ConsolidatedVendors);
                param[index++] = new SqlParameter("@ParentVendorID", oUP_VendorBE.ParentVendorID);

                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spUP_Vendor", param);

                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
                else
                    intResult = 0;

            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public int? DeleteConsolidateVendorDAL(UP_VendorBE oUP_VendorBE) {
            int? intResult = 0;
            try {
                int index = 0;
                SqlParameter[] param = new SqlParameter[2];
                param[index++] = new SqlParameter("@Action", oUP_VendorBE.Action);
                param[index++] = new SqlParameter("@VendorID", oUP_VendorBE.ParentVendorID);

                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spUP_Vendor", param);

                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
                else
                    intResult = 0;

            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        /// <summary>
        /// Used for listing all vendors
        /// </summary>
        /// <param name="oUP_VendorBE"></param>
        /// <returns></returns>
        public List<UP_VendorBE> GetVendorConsolidationDAL(UP_VendorBE oUP_VendorBE) {
            DataTable dt = new DataTable();
            List<UP_VendorBE> oUP_VendorBEList = new List<UP_VendorBE>();
            try {
                int index = 0;
                SqlParameter[] param = new SqlParameter[5];
                param[index++] = new SqlParameter("@Action", oUP_VendorBE.Action);
                param[index++] = new SqlParameter("@VendorID", oUP_VendorBE.VendorID);
                param[index++] = new SqlParameter("@VendorFlag", oUP_VendorBE.VendorFlag);
                param[index++] = new SqlParameter("@VendorIds", oUP_VendorBE.VendorIDs);
                param[index++] = new SqlParameter("@CountryID", oUP_VendorBE.CountryID);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spUP_Vendor", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows) {
                    UP_VendorBE oNewUP_VendorBE = new UP_VendorBE();
                    oNewUP_VendorBE.VendorID = Convert.ToInt32(dr["VendorID"]);
                    oNewUP_VendorBE.CountryID = dr["CountryID"] != DBNull.Value ? Convert.ToInt32(dr["CountryID"]) : (int?)null;
                    oNewUP_VendorBE.CountryName = dr["CountryName"].ToString();
                    oNewUP_VendorBE.Vendor_No = dr["Vendor_No"].ToString();
                    oNewUP_VendorBE.VendorName = dr["Vendor_Name"].ToString();
                    oNewUP_VendorBE.VendorFlag = dr["VendorFlag"] != DBNull.Value ? Convert.ToChar(dr["VendorFlag"]) : (char?)null;
                    oNewUP_VendorBE.ParentVendorID = dr["ConsVendorID"] != DBNull.Value ? Convert.ToInt32(dr["ConsVendorID"].ToString()) : (int?)null;
                    oNewUP_VendorBE.ConsolidatedCountryID = dr["ConsVendorCountryID"] != DBNull.Value ? Convert.ToInt32(dr["ConsVendorCountryID"].ToString()) : (int?)null;
                    oNewUP_VendorBE.ConsolidatedCountryName = dr["ConsVendorCountryName"].ToString();
                    oNewUP_VendorBE.ConsolidatedCode = dr["ConsVendorVendor_No"].ToString();
                    oNewUP_VendorBE.ConsolidatedName = dr["ConsVendorVendor_Name"].ToString();
                    if (dt.Columns["ConsVendorFlag"] != null)
                        oNewUP_VendorBE.ConsVendorFlag = dr["ConsVendorFlag"] != DBNull.Value ? Convert.ToChar(dr["ConsVendorFlag"]) : (char?)null;

                    oUP_VendorBEList.Add(oNewUP_VendorBE);
                }
            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return oUP_VendorBEList;
        }

        public List<UP_VendorBE> GetVendorByIdDAL(UP_VendorBE oUP_VendorBE) {
            DataTable dt = new DataTable();
            List<UP_VendorBE> oUP_VendorBEList = new List<UP_VendorBE>();
            try {
                int index = 0;
                SqlParameter[] param = new SqlParameter[3];
                param[index++] = new SqlParameter("@Action", oUP_VendorBE.Action);
                param[index++] = new SqlParameter("@VendorID", oUP_VendorBE.VendorID);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spUP_Vendor", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows) {
                    UP_VendorBE oNewUP_VendorBE = new UP_VendorBE();
                    oNewUP_VendorBE.VendorID = Convert.ToInt32(dr["VendorID"]);
                    oNewUP_VendorBE.Vendor_No = dr["Vendor_No"].ToString();
                    oNewUP_VendorBE.VendorName = dr["VendorName"].ToString();
                    oNewUP_VendorBE.VendorContactNumber = dr["VendorContactNumber"].ToString();
                    oNewUP_VendorBE.VendorContactFax = dr["VendorFax"].ToString();
                    oNewUP_VendorBE.VendorContactEmail = dr["VendorEmail"].ToString();
                    oNewUP_VendorBE.Language = dr["Language"].ToString();
                    oNewUP_VendorBE.CountryName = dr["CountryName"].ToString();
                    oNewUP_VendorBE.address1 = dr["address1"].ToString();
                    oNewUP_VendorBE.address2 = dr["address2"].ToString();
                    oNewUP_VendorBE.city = dr["city"].ToString();
                    oNewUP_VendorBE.county = dr["county"].ToString();
                    oNewUP_VendorBE.VMPPIN = dr["VMPPIN"].ToString();
                    oNewUP_VendorBE.VMPPOU = dr["VMPPOU"].ToString();
                    oNewUP_VendorBE.StockPlannerName = dr["StockPlannerName"].ToString();
                    oNewUP_VendorBE.StockPlannerContact = dr["StockPlannerContact"].ToString();
                    oNewUP_VendorBE.StockPlannerNumber = dr["StockPlannerNumber"].ToString();
                    oNewUP_VendorBE.StockPlannerID = dr["StockPlannerID"] != DBNull.Value ? Convert.ToInt32(dr["StockPlannerID"]) : (int?)null;

                    oUP_VendorBEList.Add(oNewUP_VendorBE);
                }
            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return oUP_VendorBEList;
        }

        /// <summary>
        /// Used for getting OTIF Country Miscellenaous Settings
        /// </summary>
        /// <param name="oUP_VendorBE"></param>
        /// <returns></returns>
        public List<UP_VendorBE> GetOTIFCountrySettingsDAL(UP_VendorBE oUP_VendorBE) {
            DataTable dt = new DataTable();
            List<UP_VendorBE> oUP_VendorBEList = new List<UP_VendorBE>();
            try {
                int index = 0;
                SqlParameter[] param = new SqlParameter[3];
                param[index++] = new SqlParameter("@Action", oUP_VendorBE.Action);
                param[index++] = new SqlParameter("@VendorID", oUP_VendorBE.VendorID);
                param[index++] = new SqlParameter("@UserID", oUP_VendorBE.User.UserID);


                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spUP_Vendor", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows) {
                    UP_VendorBE oNewUP_VendorBE = new UP_VendorBE();
                    oNewUP_VendorBE.VendorID = Convert.ToInt32(dr["VendorID"]);
                    oNewUP_VendorBE.CountryName = dr["CountryName"].ToString();
                    oNewUP_VendorBE.VendorName = dr["Vendor_Name"].ToString();
                    oNewUP_VendorBE.Vendor_No = dr["Vendor_No"].ToString();
                    oNewUP_VendorBE.OTIF_PreferedFrequency = Convert.ToString(dr["OTIF_PreferedFrequency"]);
                    oNewUP_VendorBE.OTIF_AbsolutePenaltyBelowPercentage = dr["OTIF_AbsolutePenaltyBelowPercentage"] != DBNull.Value ? Convert.ToDecimal(dr["OTIF_AbsolutePenaltyBelowPercentage"]) : (decimal?)null;
                    oNewUP_VendorBE.OTIF_AbsoluteAppliedPenaltyPercentage = dr["OTIF_AbsoluteAppliedPenaltyPercentage"] != DBNull.Value ? Convert.ToDecimal(dr["OTIF_AbsoluteAppliedPenaltyPercentage"]) : (decimal?)null;
                    oNewUP_VendorBE.OTIF_ToleratedPenaltyBelowPercentage = dr["OTIF_ToleratedPenaltyBelowPercentage"] != DBNull.Value ? Convert.ToDecimal(dr["OTIF_ToleratedPenaltyBelowPercentage"]) : (decimal?)null;
                    oNewUP_VendorBE.OTIF_ToleratedAppliedPenaltyPercentage = dr["OTIF_ToleratedAppliedPenaltyPercentage"] != DBNull.Value ? Convert.ToDecimal(dr["OTIF_ToleratedAppliedPenaltyPercentage"]) : (decimal?)null;
                    oNewUP_VendorBE.OTIF_ODMeasurePenaltyBelowPercentage = dr["OTIF_ODMeasurePenaltyBelowPercentage"] != DBNull.Value ? Convert.ToDecimal(dr["OTIF_ODMeasurePenaltyBelowPercentage"]) : (decimal?)null;
                    oNewUP_VendorBE.OTIF_ODMeasureAppliedPenaltyPercentage = dr["OTIF_ODMeasureAppliedPenaltyPercentage"] != DBNull.Value ? Convert.ToDecimal(dr["OTIF_ODMeasureAppliedPenaltyPercentage"]) : (decimal?)null;
                    oNewUP_VendorBE.OTIF_BackOrderValue = dr["OTIF_BackOrderValue"] != DBNull.Value ? Convert.ToDecimal(dr["OTIF_BackOrderValue"]) : (decimal?)null;
                    oNewUP_VendorBE.CurrencyID = dr["CurrencyID"] != DBNull.Value ? Convert.ToInt32(dr["CurrencyId"]) : (int?)null;
                    oNewUP_VendorBE.CurrencyName = dr["CurrencyName"].ToString();

                    oUP_VendorBEList.Add(oNewUP_VendorBE);
                }
            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return oUP_VendorBEList;
        }

        /// <summary>
        /// Used for managing OTIF Country Miscellenaous Settings
        /// </summary>
        /// <param name="oUP_VendorBE"></param>
        /// <returns></returns>
        public int? addEditAddEditOTIFCountrySettingsDAL(UP_VendorBE oUP_VendorBE) {
            int? intResult = 0;
            try {
                int index = 0;
                SqlParameter[] param = new SqlParameter[11];
                param[index++] = new SqlParameter("@Action", oUP_VendorBE.Action);
                param[index++] = new SqlParameter("@ConsolidatedVendorIds", oUP_VendorBE.ConsolidatedVendors);
                param[index++] = new SqlParameter("@OTIF_PreferedFrequency", oUP_VendorBE.OTIF_PreferedFrequency);
                param[index++] = new SqlParameter("@OTIF_AbsolutePenaltyBelowPercentage", oUP_VendorBE.OTIF_AbsolutePenaltyBelowPercentage);
                param[index++] = new SqlParameter("@OTIF_AbsoluteAppliedPenaltyPercentage", oUP_VendorBE.OTIF_AbsoluteAppliedPenaltyPercentage);
                param[index++] = new SqlParameter("@OTIF_ToleratedPenaltyBelowPercentage", oUP_VendorBE.OTIF_ToleratedPenaltyBelowPercentage);
                param[index++] = new SqlParameter("@OTIF_ToleratedAppliedPenaltyPercentage", oUP_VendorBE.OTIF_ToleratedAppliedPenaltyPercentage);
                param[index++] = new SqlParameter("@OTIF_ODMeasurePenaltyBelowPercentage", oUP_VendorBE.OTIF_ODMeasurePenaltyBelowPercentage);
                param[index++] = new SqlParameter("@OTIF_ODMeasureAppliedPenaltyPercentage", oUP_VendorBE.OTIF_ODMeasureAppliedPenaltyPercentage);
                param[index++] = new SqlParameter("@OTIF_BackOrderValue", oUP_VendorBE.OTIF_BackOrderValue);
                param[index++] = new SqlParameter("@CurrencyID", oUP_VendorBE.CurrencyID);

                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spUP_Vendor", param);

                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
                else
                    intResult = 0;

            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public UP_VendorBE GetVendorDetailDAL(UP_VendorBE oUP_VendorBE) {
            DataTable dt = new DataTable();
            try {
                int index = 0;
                SqlParameter[] param = new SqlParameter[3];
                param[index++] = new SqlParameter("@Action", oUP_VendorBE.Action);
                param[index++] = new SqlParameter("@VendorID", oUP_VendorBE.VendorID);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spUP_Vendor", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows) {
                    oUP_VendorBE.VendorID = Convert.ToInt32(dr["VendorID"]);
                    oUP_VendorBE.Vendor_No = dr["Vendor_No"].ToString();
                    oUP_VendorBE.VendorName = dr["VendorName"].ToString();

                    oUP_VendorBE.VendorContactNumber = dr["VendorContactNumber"].ToString();
                    oUP_VendorBE.VendorContactFax = dr["VendorFax"].ToString();
                    oUP_VendorBE.VendorContactEmail = dr["VendorEmail"].ToString();
                    oUP_VendorBE.Language = dr["Language"].ToString();
                    oUP_VendorBE.CountryName = dr["CountryName"].ToString();

                    oUP_VendorBE.address1 = dr["address1"].ToString();
                    oUP_VendorBE.address2 = dr["address2"].ToString();
                    oUP_VendorBE.city = dr["city"].ToString();
                    oUP_VendorBE.county = dr["county"].ToString();
                    oUP_VendorBE.VMPPIN = dr["VMPPIN"].ToString();
                    oUP_VendorBE.VMPPOU = dr["VMPPOU"].ToString();
                    oUP_VendorBE.StockPlannerContact = dr["StockPlannerContact"].ToString();
                    oUP_VendorBE.StockPlannerID = dr["StockPlannerID"] != DBNull.Value ? Convert.ToInt32(dr["StockPlannerID"]) : (int?)null;
                    oUP_VendorBE.StockPlannerNumber = dr["StockPlannerNumber"].ToString();
                    oUP_VendorBE.StockPlannerName = dr["StockPlannerName"].ToString();
                    oUP_VendorBE.VendorFlag = dr["VendorFlag"] != DBNull.Value ? Convert.ToChar(dr["VendorFlag"]) : (char?)null; //Convert.ToChar(dr["VendorFlag"]);
                    if (ds.Tables[0].Columns.Contains("ReturnAddress"))
                        oUP_VendorBE.ReturnAddress = Convert.ToString(dr["ReturnAddress"]);

                }
            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return oUP_VendorBE;
        }

        public List<SCT_UserBE> GetVendorContact(UP_VendorBE oUP_VendorBE) {
            DataTable dt = new DataTable();
            List<SCT_UserBE> oSCT_UserBEList = new List<SCT_UserBE>();
            try {
                int index = 0;
                SqlParameter[] param = new SqlParameter[6];
                param[index++] = new SqlParameter("@Action", oUP_VendorBE.Action);
                param[index++] = new SqlParameter("@VendorID", oUP_VendorBE.VendorID);
                param[index++] = new SqlParameter("@SchedulingContact", oUP_VendorBE.User.SchedulingContact);
                param[index++] = new SqlParameter("@DiscrepancyContact", oUP_VendorBE.User.DiscrepancyContact);
                param[index++] = new SqlParameter("@OTIFContact", oUP_VendorBE.User.OTIFContact);
                param[index++] = new SqlParameter("@ScorecardContact", oUP_VendorBE.User.ScorecardContact);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spUP_Vendor", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows) {
                    SCT_UserBE oNewSCT_UserBE = new SCT_UserBE();
                    oNewSCT_UserBE.FirstName = dr["FirstName"].ToString();
                    oNewSCT_UserBE.Lastname = dr["Lastname"].ToString();
                    oNewSCT_UserBE.UserID = Convert.ToInt32(dr["UserID"]);
                    oNewSCT_UserBE.AccountStatus = dr["AccountStatus"].ToString();
                    oNewSCT_UserBE.UserName = string.Format("{0} {1}", Convert.ToString(dr["FirstName"]), Convert.ToString(dr["Lastname"]));
                    oSCT_UserBEList.Add(oNewSCT_UserBE);
                }
            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return oSCT_UserBEList;
        }

        public void UpdateReturnAddress(UP_VendorBE oUP_VendorBE) {
            try {
                int index = 0;
                SqlParameter[] param = new SqlParameter[7];
                param[index++] = new SqlParameter("@Action", oUP_VendorBE.Action);
                param[index++] = new SqlParameter("@VendorID", oUP_VendorBE.VendorID);
                param[index++] = new SqlParameter("@ReturnAddress", oUP_VendorBE.ReturnAddress);

                SqlHelper.ExecuteNonQuery(DBConnection.Connection, CommandType.StoredProcedure, "spUP_Vendor", param);
            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }

        }

        public void UpdateVendorActiveSites(UP_VendorBE oUP_VendorBE) {
            try {
                int index = 0;
                SqlParameter[] param = new SqlParameter[7];
                param[index++] = new SqlParameter("@Action", oUP_VendorBE.Action);
                param[index++] = new SqlParameter("@VendorID", oUP_VendorBE.VendorID);
                param[index++] = new SqlParameter("@VendorActiveSiteIDs", oUP_VendorBE.VendorActiveSiteIDs);

                SqlHelper.ExecuteNonQuery(DBConnection.Connection, CommandType.StoredProcedure, "spUP_Vendor", param);
            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
        }

        public DataTable GetVendorActiveSitesDAL(UP_VendorBE oUP_VendorBE) {
            DataTable dtVendorActiveSites = null;
            try {
                int index = 0;
                SqlParameter[] param = new SqlParameter[3];
                param[index++] = new SqlParameter("@Action", oUP_VendorBE.Action);
                param[index++] = new SqlParameter("@VendorID", oUP_VendorBE.VendorID);
                param[index++] = new SqlParameter("@SiteCountryID", oUP_VendorBE.CountryID);

                DataSet dsVendorActiveSites = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spUP_Vendor", param);
                if (dsVendorActiveSites != null && dsVendorActiveSites.Tables.Count > 0)
                    dtVendorActiveSites = dsVendorActiveSites.Tables[0];
            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return dtVendorActiveSites;
        }

        public List<UP_VendorBE> GetVendorByUserIdDAL(UP_VendorBE oUP_VendorBE) {
            DataTable dt = new DataTable();
            List<UP_VendorBE> oUP_VendorBEList = new List<UP_VendorBE>();
            try {
                int index = 0;
                SqlParameter[] param = new SqlParameter[3];
                param[index++] = new SqlParameter("@Action", oUP_VendorBE.Action);
                param[index++] = new SqlParameter("@UserID", oUP_VendorBE.User.UserID);
                param[index++] = new SqlParameter("@SiteID", oUP_VendorBE.Site.SiteID);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spUP_Vendor", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows) {
                    UP_VendorBE oNewUP_VendorBE = new UP_VendorBE();
                    oNewUP_VendorBE.VendorID = dr["VendorID"] != DBNull.Value ? Convert.ToInt32(dr["VendorID"]) : 0; //Convert.ToInt32(dr["VendorID"]);
                    oNewUP_VendorBE.Vendor_No = Convert.ToString(dr["Vendor_No"]);
                    oNewUP_VendorBE.VendorName = Convert.ToString(dr["VendorName"]);
                    oNewUP_VendorBE.ParentVendorID = dr["ParentVendorID"] != DBNull.Value ? Convert.ToInt32(dr["ParentVendorID"]) : -1;
                    oNewUP_VendorBE.VendorFlag = dr["VendorFlag"] != DBNull.Value ? Convert.ToChar(dr["VendorFlag"]) : (char?)null;

                    bool IsExistCountryID = dr.Table.Columns.Contains("CountryID");
                    if (IsExistCountryID)
                        oNewUP_VendorBE.CountryID = dr["CountryID"] != DBNull.Value ? Convert.ToInt32(dr["CountryID"]) : (int?)null;

                    bool IsExistCountryName = dr.Table.Columns.Contains("CountryName");
                    if (IsExistCountryName) {
                        oNewUP_VendorBE.CountryName = dr["CountryName"] != DBNull.Value ? Convert.ToString(dr["CountryName"]) : null;
                        oNewUP_VendorBE.VendorCountryName = string.Format("{0} - {1}", Convert.ToString(dr["VendorName"]), Convert.ToString(dr["CountryName"]));
                    }

                    oUP_VendorBEList.Add(oNewUP_VendorBE);
                }
            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return oUP_VendorBEList;
        }

        public List<UP_VendorBE> GetVendorByUserCountryDAL(UP_VendorBE oUP_VendorBE) {
            DataTable dt = new DataTable();
            List<UP_VendorBE> oUP_VendorBEList = new List<UP_VendorBE>();
            try {
                int index = 0;
                SqlParameter[] param = new SqlParameter[5];
                param[index++] = new SqlParameter("@Action", oUP_VendorBE.Action);
                param[index++] = new SqlParameter("@UserID", oUP_VendorBE.User.UserID);
                param[index++] = new SqlParameter("@VendorName", oUP_VendorBE.VendorName);
                param[index++] = new SqlParameter("@CountryId", oUP_VendorBE.CountryID);
                param[index++] = new SqlParameter("@VendorFlag", oUP_VendorBE.VendorFlag);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spUP_Vendor", param);
                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows) {
                    UP_VendorBE oNewUP_VendorBE = new UP_VendorBE();
                    oNewUP_VendorBE.VendorID = Convert.ToInt32(dr["VendorID"]);
                    oNewUP_VendorBE.VendorName = dr["VendorName"].ToString();
                    oNewUP_VendorBE.Vendor_No = dr["Vendor_No"].ToString();
                    oNewUP_VendorBE.IsActiveVendor = dr["IsActiveVendor"] == DBNull.Value ? null : Convert.ToString(dr["IsActiveVendor"]); /// Added on 25 Feb 2013

                    oUP_VendorBEList.Add(oNewUP_VendorBE);
                }
            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return oUP_VendorBEList;
        }

        public List<UP_VendorBE> GetVendorForEdit(UP_VendorBE oUP_VendorBE) {
            DataTable dt = new DataTable();
            List<UP_VendorBE> lstVendor = new List<UP_VendorBE>();
            try {
                int index = 0;
                SqlParameter[] param = new SqlParameter[2];
                param[index++] = new SqlParameter("@Action", oUP_VendorBE.Action);
                param[index++] = new SqlParameter("@CountryId", oUP_VendorBE.CountryID);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spUP_Vendor", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows) {
                    UP_VendorBE oNewUP_VendorBE = new UP_VendorBE();
                    oNewUP_VendorBE.VendorID = Convert.ToInt32(dr["VendorEditID"]);
                    oNewUP_VendorBE.CountryName = dr["Vendor_Country"] != DBNull.Value ? dr["Vendor_Country"].ToString() : "";
                    oNewUP_VendorBE.VendorName = dr["Vendor_Name"] != DBNull.Value ? dr["Vendor_Name"].ToString() : "";
                    oNewUP_VendorBE.Vendor_No = dr["Vendor_No"] != DBNull.Value ? dr["Vendor_No"].ToString() : "";
                    oNewUP_VendorBE.VendorContactName = dr["Vendor_Contact_Name"] != DBNull.Value ? dr["Vendor_Contact_Name"].ToString() : "";
                    oNewUP_VendorBE.VendorContactNumber = dr["Vendor_Contact_Number"] != DBNull.Value ? dr["Vendor_Contact_Number"].ToString() : "";
                    oNewUP_VendorBE.VendorContactFax = dr["Vendor_Contact_Fax"] != DBNull.Value ? dr["Vendor_Contact_Fax"].ToString() : "";
                    oNewUP_VendorBE.VendorContactEmail = dr["Vendor_Contact_Email"] != DBNull.Value ? dr["Vendor_Contact_Email"].ToString() : "";
                    oNewUP_VendorBE.address1 = dr["address_1"] != DBNull.Value ? dr["address_1"].ToString() : "";
                    oNewUP_VendorBE.address2 = dr["address_2"] != DBNull.Value ? dr["address_2"].ToString() : "";
                    oNewUP_VendorBE.city = dr["city"] != DBNull.Value ? dr["city"].ToString() : "";
                    oNewUP_VendorBE.county = dr["county"] != DBNull.Value ? dr["county"].ToString() : "";
                    oNewUP_VendorBE.VMPPIN = dr["VMPPIN"] != DBNull.Value ? dr["VMPPIN"].ToString() : "";
                    oNewUP_VendorBE.VMPPOU = dr["VMPPOU"] != DBNull.Value ? dr["VMPPOU"].ToString() : "";
                    oNewUP_VendorBE.VMTCC1 = dr["VMTCC1"] != DBNull.Value ? dr["VMTCC1"].ToString() : "";
                    oNewUP_VendorBE.VMTEL1 = dr["VMTEL1"] != DBNull.Value ? dr["VMTEL1"].ToString() : "";
                    oNewUP_VendorBE.Fax_country_code = dr["Fax_country_code"] != DBNull.Value ? dr["Fax_country_code"].ToString() : "";
                    oNewUP_VendorBE.Fax_no = dr["Fax_no"] != DBNull.Value ? dr["Fax_no"].ToString() : "";
                    oNewUP_VendorBE.Buyer = dr["Buyer"] != DBNull.Value ? dr["Buyer"].ToString() : "";


                    lstVendor.Add(oNewUP_VendorBE);
                }
            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return lstVendor;

        }

        public void UpdateDeleteVendor(UP_VendorBE oUP_VendorBE) {
            try {
                int index = 0;
                SqlParameter[] param = new SqlParameter[2];
                param[index++] = new SqlParameter("@Action", oUP_VendorBE.Action);
                param[index++] = new SqlParameter("@VendorID", oUP_VendorBE.VendorID);

                int? result = SqlHelper.ExecuteNonQuery(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spUP_Vendor", param);
            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }

        }

        public DataTable CheckExistingSettingsForVendorDAL(UP_VendorBE oUP_VendorBE) {
            DataTable dtExistingVendor = null;
            try {
                int index = 0;
                SqlParameter[] param = new SqlParameter[2];
                param[index++] = new SqlParameter("@Action", oUP_VendorBE.Action);
                param[index++] = new SqlParameter("@ConsolidatedVendorIds", oUP_VendorBE.ConsolidatedVendors);

                DataSet dsExistingVendors = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spUP_Vendor", param);
                if (dsExistingVendors != null && dsExistingVendors.Tables.Count > 0)
                    dtExistingVendor = dsExistingVendors.Tables[0];
            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return dtExistingVendor;
        }

        public bool CheckVendorNoAlreadyExistDAL(UP_VendorBE oUP_VendorBE) {
            bool IsVendorNoExist = false;
            try {
                int index = 0;
                SqlParameter[] param = new SqlParameter[3];
                param[index++] = new SqlParameter("@Action", oUP_VendorBE.Action);
                param[index++] = new SqlParameter("@Vendor_No", oUP_VendorBE.Vendor_No);
                param[index++] = new SqlParameter("@VendorID", oUP_VendorBE.VendorID);

                DataSet dsVendorNo = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spUP_Vendor", param);
                if (dsVendorNo != null && dsVendorNo.Tables.Count > 0) {
                    DataTable dtVendorNo = dsVendorNo.Tables[0];
                    if (dtVendorNo.Rows.Count > 0)
                        IsVendorNoExist = true;
                }
            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return IsVendorNoExist;

        }

        public void AddVendors(List<UP_VendorBE> pUP_VendorBEList) {
            try {

                DataTable dt = new DataTable();
                DataRow dr;

                dt.Columns.Add(new DataColumn("Vendor_Country"));
                dt.Columns.Add(new DataColumn("Vendor_No"));
                dt.Columns.Add(new DataColumn("Vendor_Name"));
                dt.Columns.Add(new DataColumn("Vendor Contact Name"));
                dt.Columns.Add(new DataColumn("Vendor Contact Number"));
                dt.Columns.Add(new DataColumn("Vendor Contact Fax"));
                dt.Columns.Add(new DataColumn("Vendor Contact Email"));
                dt.Columns.Add(new DataColumn("address 1"));
                dt.Columns.Add(new DataColumn("address 2"));
                dt.Columns.Add(new DataColumn("city"));
                dt.Columns.Add(new DataColumn("county"));
                dt.Columns.Add(new DataColumn("VMPPIN"));
                dt.Columns.Add(new DataColumn("VMPPOU"));
                dt.Columns.Add(new DataColumn("VMTCC1"));
                dt.Columns.Add(new DataColumn("VMTEL1"));
                dt.Columns.Add(new DataColumn("Fax_country_code"));
                dt.Columns.Add(new DataColumn("Fax_no"));
                dt.Columns.Add(new DataColumn("Buyer"));
                dt.Columns.Add(new DataColumn("UpdatedDate", typeof(System.DateTime)));

                foreach (UP_VendorBE oUP_VendorBE in pUP_VendorBEList) {
                    dr = dt.NewRow();

                    dr["Vendor_Country"] = oUP_VendorBE.CountryName;
                    dr["Vendor_No"] = oUP_VendorBE.Vendor_No;
                    dr["Vendor_Name"] = oUP_VendorBE.VendorName;
                    dr["Vendor Contact Name"] = oUP_VendorBE.VendorContactName;
                    dr["Vendor Contact Number"] = oUP_VendorBE.VendorContactNumber;
                    dr["Vendor Contact Fax"] = oUP_VendorBE.VendorContactFax;
                    dr["Vendor Contact Email"] = oUP_VendorBE.VendorContactEmail;
                    dr["address 1"] = oUP_VendorBE.address1;
                    dr["address 2"] = oUP_VendorBE.address2;
                    dr["city"] = oUP_VendorBE.city;
                    dr["county"] = oUP_VendorBE.county;
                    dr["VMPPIN"] = oUP_VendorBE.VMPPIN;
                    dr["VMPPOU"] = oUP_VendorBE.VMPPOU;
                    dr["VMTCC1"] = oUP_VendorBE.VMTCC1;
                    dr["VMTEL1"] = oUP_VendorBE.VMTEL1;
                    dr["Fax_country_code"] = oUP_VendorBE.Fax_country_code;
                    dr["Fax_no"] = oUP_VendorBE.Fax_no;
                    dr["Buyer"] = oUP_VendorBE.Buyer;
                    dr["UpdatedDate"] = oUP_VendorBE.UpdatedDate;
                    dt.Rows.Add(dr);
                }
                pUP_VendorBEList = null;

                SqlConnection sqlConn = new SqlConnection(DBConnection.Connection);
                sqlConn.Open();

                SqlBulkCopy oSqlBulkCopy = new SqlBulkCopy(sqlConn, SqlBulkCopyOptions.TableLock, null);
                oSqlBulkCopy.DestinationTableName = "UP_StageVendor";

                oSqlBulkCopy.ColumnMappings.Add("Vendor_Country", "Vendor_Country");
                oSqlBulkCopy.ColumnMappings.Add("Vendor_No", "Vendor_No");
                oSqlBulkCopy.ColumnMappings.Add("Vendor_Name", "Vendor_Name");
                oSqlBulkCopy.ColumnMappings.Add("Vendor Contact Name", "Vendor Contact Name");
                oSqlBulkCopy.ColumnMappings.Add("Vendor Contact Number", "Vendor Contact Number");
                oSqlBulkCopy.ColumnMappings.Add("Vendor Contact Fax", "Vendor Contact Fax");
                oSqlBulkCopy.ColumnMappings.Add("Vendor Contact Email", "Vendor Contact Email");
                oSqlBulkCopy.ColumnMappings.Add("address 1", "address 1");
                oSqlBulkCopy.ColumnMappings.Add("address 2", "address 2");
                oSqlBulkCopy.ColumnMappings.Add("city", "city");
                oSqlBulkCopy.ColumnMappings.Add("county", "county");
                oSqlBulkCopy.ColumnMappings.Add("VMPPIN", "VMPPIN");
                oSqlBulkCopy.ColumnMappings.Add("VMPPOU", "VMPPOU");
                oSqlBulkCopy.ColumnMappings.Add("VMTCC1", "VMTCC1");
                oSqlBulkCopy.ColumnMappings.Add("VMTEL1", "VMTEL1");
                oSqlBulkCopy.ColumnMappings.Add("Fax_country_code", "Fax_country_code");
                oSqlBulkCopy.ColumnMappings.Add("Fax_no", "Fax_no");
                oSqlBulkCopy.ColumnMappings.Add("Buyer", "Buyer");
                oSqlBulkCopy.ColumnMappings.Add("UpdatedDate", "UpdatedDate");

                oSqlBulkCopy.BulkCopyTimeout = 300;
                oSqlBulkCopy.BatchSize = dt.Rows.Count;

                oSqlBulkCopy.WriteToServer(dt);
                oSqlBulkCopy.Close();
                sqlConn.Close();
            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }

        }

        public List<UP_VendorBE> GetVendorReportDAL(UP_VendorBE oUP_VendorBE) {

            DataTable dt = new DataTable();
            List<UP_VendorBE> oUP_VendorBEList = new List<UP_VendorBE>();
            try {
                int index = 0;
                SqlParameter[] param = new SqlParameter[1];
                param[index++] = new SqlParameter("@Action", oUP_VendorBE.Action);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spOTIF_Reports", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows) {
                    UP_VendorBE oNewUP_VendorBE = new UP_VendorBE();
                    oNewUP_VendorBE.VendorID = Convert.ToInt32(dr["VendorID"]);
                    oNewUP_VendorBE.VendorName = dr["Vendor_Name"].ToString();
                    oNewUP_VendorBE.User = new SCT_UserBE();
                    oNewUP_VendorBE.User.UserID = Convert.ToInt32(dr["UserID"]);
                    oNewUP_VendorBE.User.EmailId = dr["UserEmailID"].ToString();
                    oNewUP_VendorBE.Language = dr["Language"].ToString();
                    oNewUP_VendorBE.CountryName = dr["Vendor_Country"].ToString();
                    if (dr.Table.Columns.Contains("StockPlannerName"))
                        oNewUP_VendorBE.StockPlannerName = dr["StockPlannerName"] != DBNull.Value ? dr["StockPlannerName"].ToString() : "";
                    if (dr.Table.Columns.Contains("PhoneNumber"))
                        oNewUP_VendorBE.StockPlannerNumber = dr["PhoneNumber"] != DBNull.Value ? dr["PhoneNumber"].ToString() : "";
                    oUP_VendorBEList.Add(oNewUP_VendorBE);

                }
            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return oUP_VendorBEList;
        }

        public List<UP_VendorBE> GetVendorByVendorSearchDAL(UP_VendorBE oUP_VendorBE) {
            DataTable dt = new DataTable();
            List<UP_VendorBE> oUP_VendorBEList = new List<UP_VendorBE>();
            try {
                int index = 0;
                SqlParameter[] param = new SqlParameter[3];
                param[index++] = new SqlParameter("@Action", oUP_VendorBE.Action);
                param[index++] = new SqlParameter("@CountryId", oUP_VendorBE.CountryID);
                param[index++] = new SqlParameter("@VendorName", oUP_VendorBE.VendorName);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spUP_Vendor", param);
                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows) {
                    UP_VendorBE oNewUP_VendorBE = new UP_VendorBE();
                    oNewUP_VendorBE.VendorID = Convert.ToInt32(dr["VendorID"]);
                    oNewUP_VendorBE.VendorName = dr["VendorName"].ToString();
                    oNewUP_VendorBE.Vendor_No = dr["Vendor_No"].ToString();
                    oNewUP_VendorBE.IsActiveVendor = dr["IsActiveVendor"] == DBNull.Value ? null : Convert.ToString(dr["IsActiveVendor"]); /// Added on 21 Feb 2013
                    oUP_VendorBEList.Add(oNewUP_VendorBE);
                }
            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return oUP_VendorBEList;
        }

        /// <summary>
        ///  Logs Errors Related to Vendors Files in Vendors Error Table
        /// </summary>
        /// <param name="up_PurchaseOrderBEErrorList"></param>
        public void AddVendorsErrors(List<UP_DataImportErrorBE> up_VendorsBEErrorList) {
            try {
                DataTable dt = new DataTable();
                DataRow dr;

                dt.Columns.Add(new DataColumn("FolderDownloadID", typeof(System.Int32)));
                dt.Columns.Add(new DataColumn("ManualFileUploadID", typeof(System.Int32)));
                dt.Columns.Add(new DataColumn("ErrorDescription", typeof(System.String)));
                dt.Columns.Add(new DataColumn("UpdatedDate", typeof(System.DateTime)));


                foreach (UP_DataImportErrorBE obj_DataImportErrorBE in up_VendorsBEErrorList) {
                    dr = dt.NewRow();

                    dr["FolderDownloadID"] = obj_DataImportErrorBE.FolderDownloadID;
                    dr["ManualFileUploadID"] = obj_DataImportErrorBE.ManualFileUploadID;
                    dr["ErrorDescription"] = obj_DataImportErrorBE.ErrorDescription;
                    dr["UpdatedDate"] = obj_DataImportErrorBE.UpdatedDate;


                    //if (oUP_SKUBE.Valuated_Stock != null)
                    //    dr["Valuated_Stock"] = oUP_SKUBE.Valuated_Stock;
                    //else
                    //    dr["Valuated_Stock"] = DBNull.Value;


                    dt.Rows.Add(dr);
                }
                up_VendorsBEErrorList = null;

                SqlConnection sqlConn = new SqlConnection(DBConnection.Connection);
                sqlConn.Open();

                SqlBulkCopy oSqlBulkCopy = new SqlBulkCopy(sqlConn, SqlBulkCopyOptions.TableLock, null);
                oSqlBulkCopy.DestinationTableName = "UP_ErrorVendor";

                oSqlBulkCopy.ColumnMappings.Add("FolderDownloadID", "FolderDownloadID");
                oSqlBulkCopy.ColumnMappings.Add("ManualFileUploadID", "ManualFileUploadID");
                oSqlBulkCopy.ColumnMappings.Add("ErrorDescription", "ErrorDescription");
                oSqlBulkCopy.ColumnMappings.Add("UpdatedDate", "UpdatedDate");


                oSqlBulkCopy.BulkCopyTimeout = 300;
                oSqlBulkCopy.BatchSize = dt.Rows.Count;

                oSqlBulkCopy.WriteToServer(dt);
                oSqlBulkCopy.Close();
            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }


        }

        public string GetGlobalConsolidateVendorIdsDAL(UP_VendorBE oUP_VendorBE) {
            DataTable dt = new DataTable();
            List<UP_VendorBE> oUP_VendorBEList = new List<UP_VendorBE>();
            string strVendorIds = string.Empty;
            try {
                int index = 0;
                SqlParameter[] param = new SqlParameter[2];
                param[index++] = new SqlParameter("@Action", oUP_VendorBE.Action);
                param[index++] = new SqlParameter("@VendorID", oUP_VendorBE.VendorID);
                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spUP_Vendor", param);
                dt = ds.Tables[0];
                foreach (DataRow dr in dt.Rows)
                    strVendorIds = Convert.ToString(dr["VendorIds"]);
            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }

            return strVendorIds;
        }

        public List<UP_VendorBE> GetAllGrandParentAndStandAloneVendorsDAL(UP_VendorBE oMAS_VendorBE) {
            var oMAS_VendorBEList = new List<UP_VendorBE>();
            try {
                int index = 0;
                SqlParameter[] param = new SqlParameter[2];
                param[index++] = new SqlParameter("@Action", oMAS_VendorBE.Action);
                param[index++] = new SqlParameter("@VendorCountToCreateReport", oMAS_VendorBE.VendorCountToCreateReport);

                var ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure,
                    "spMAS_Vendor", param);

                if (ds != null && ds.Tables.Count > 0) {
                    foreach (DataRow dr in ds.Tables[0].Rows) {
                        var oNewMAS_VendorBE = new UP_VendorBE();
                        oNewMAS_VendorBE.VendorID = dr["VendorID"] == DBNull.Value ? 0 : Convert.ToInt32(dr["VendorID"]);
                        oNewMAS_VendorBE.Vendor_No = dr["VendorNo"] == DBNull.Value ? null : Convert.ToString(dr["VendorNo"]);
                        oNewMAS_VendorBE.VendorName = dr["VendorName"] == DBNull.Value ? null : Convert.ToString(dr["VendorName"]);
                        oNewMAS_VendorBE.IsActiveVendor = dr["IsActiveVendor"] == DBNull.Value ? null : Convert.ToString(dr["IsActiveVendor"]);
                        oMAS_VendorBEList.Add(oNewMAS_VendorBE);
                    }
                }
            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return oMAS_VendorBEList;
        }

        /* Added methods for OTIF report */
        public List<UP_VendorBE> GetOTIFVendorReportsDAL(UP_VendorBE oUP_VendorBE) {
            DataTable dt = new DataTable();
            List<UP_VendorBE> oUP_VendorBEList = new List<UP_VendorBE>();
            try {
                int index = 0;
                SqlParameter[] param = new SqlParameter[4];
                param[index++] = new SqlParameter("@Action", oUP_VendorBE.Action);
                param[index++] = new SqlParameter("@VendorCountToCreateReport", oUP_VendorBE.VendorCountToCreateReport);
                param[index++] = new SqlParameter("@otifSchedularId", oUP_VendorBE.otifSchedularId);
                param[index++] = new SqlParameter("@otifApplicationName", oUP_VendorBE.otifApplicationName);

                //param[index++] = new SqlParameter("@IsChildRequired", oUP_VendorBE.IsChildRequired);
                //param[index++] = new SqlParameter("@IsParentRequired", oUP_VendorBE.IsParentRequired);
                //param[index++] = new SqlParameter("@IsStandAloneRequired", oUP_VendorBE.IsStandAloneRequired);
                //param[index++] = new SqlParameter("@IsGrandParentRequired", oUP_VendorBE.IsGrandParentRequired);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spUP_Vendor", param);
                if (ds != null && ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                    foreach (DataRow dr in dt.Rows)
                    {
                        UP_VendorBE oNewUP_VendorBE = new UP_VendorBE();
                        oNewUP_VendorBE.VendorID = Convert.ToInt32(dr["VendorID"]);
                        oNewUP_VendorBE.CountryID = Convert.ToInt32(dr["CountryID"]);
                        oNewUP_VendorBE.CountryName = dr["Vendor_Country"].ToString();
                        oNewUP_VendorBE.Vendor_No = dr["Vendor_No"].ToString();
                        oNewUP_VendorBE.VendorName = dr["Vendor_Name"].ToString();
                        oNewUP_VendorBE.ParentVendorID = dr["ParentVendorID"] != DBNull.Value ? Convert.ToInt32(dr["ParentVendorID"]) : -1;
                        oNewUP_VendorBE.IsActiveVendor = dr["IsActiveVendor"] == DBNull.Value ? null : Convert.ToString(dr["IsActiveVendor"]); /// Added on 21 Feb 2013
                        oUP_VendorBEList.Add(oNewUP_VendorBE);
                    }
                }
            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return oUP_VendorBEList;
        }

        public List<UP_VendorBE> GetOTIFMasterVendorReportsDAL(UP_VendorBE oUP_VendorBE) {
            DataTable dt = new DataTable();
            List<UP_VendorBE> oUP_VendorBEList = new List<UP_VendorBE>();
            try {
                int index = 0;
                SqlParameter[] param = new SqlParameter[4];
                param[index++] = new SqlParameter("@Action", oUP_VendorBE.Action);
                param[index++] = new SqlParameter("@VendorCountToCreateReport", oUP_VendorBE.VendorCountToCreateReport);
                param[index++] = new SqlParameter("@otifSchedularId", oUP_VendorBE.otifSchedularId);
                param[index++] = new SqlParameter("@otifApplicationName", oUP_VendorBE.otifApplicationName);
                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spUP_Vendor", param);

                if (ds != null && ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                    foreach (DataRow dr in dt.Rows)
                    {
                        UP_VendorBE oNewUP_VendorBE = new UP_VendorBE();
                        oNewUP_VendorBE.VendorIDs = Convert.ToString(dr["VendorIDs"]);
                        oNewUP_VendorBE.VendorName = dr["VendorName"].ToString();
                        oNewUP_VendorBE.MasterParentVendorID = dr["MasterParentVendorID"] != DBNull.Value ? Convert.ToInt32(dr["MasterParentVendorID"]) : -1;

                        oUP_VendorBEList.Add(oNewUP_VendorBE);
                    }
                }
            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return oUP_VendorBEList;
        }

        public int? UpdateOTIFMasterVendorReportDAL(UP_VendorBE oUP_VendorBE) {
            int? intResult = 0;
            try {
                int index = 0;
                SqlParameter[] param = new SqlParameter[2];
                param[index++] = new SqlParameter("@Action", oUP_VendorBE.Action);
                param[index++] = new SqlParameter("@ParentVendorID", oUP_VendorBE.MasterParentVendorID);
                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spUP_Vendor", param);
                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
                else
                    intResult = 0;

            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public int? UpdateOTIFVendorReportDAL(UP_VendorBE oUP_VendorBE) {
            int? intResult = 0;
            try {
                int index = 0;
                SqlParameter[] param = new SqlParameter[2];
                param[index++] = new SqlParameter("@Action", oUP_VendorBE.Action);
                param[index++] = new SqlParameter("@VendorID", oUP_VendorBE.VendorID);
                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spUP_Vendor", param);
                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
                else
                    intResult = 0;

            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        
        }

    }
}

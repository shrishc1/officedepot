﻿using System;
using System.Data.SqlClient;
using System.Data;
using System.Collections.Generic;

using Utilities;
using BusinessEntities.ModuleBE.Appointment.SiteSettings;

namespace DataAccessLayer.ModuleDAL.Appointment.SiteSettings {
    public class APPSIT_SpecificWeekSetupDAL : BaseDAL{

        public List<MASSIT_SpecificWeekSetupBE> GetSpecificWeekSetupDetailsDAL(MASSIT_SpecificWeekSetupBE oMASSIT_SpecificWeekSetupBE) {
            DataTable dt = new DataTable();
            List<MASSIT_SpecificWeekSetupBE> oMASSIT_SpecificWeekSetupBEList = new List<MASSIT_SpecificWeekSetupBE>();
            try {
                int index = 0;
                SqlParameter[] param = new SqlParameter[8];
                param[index++] = new SqlParameter("@Action", oMASSIT_SpecificWeekSetupBE.Action);
                param[index++] = new SqlParameter("@SiteScheduleDiaryID", oMASSIT_SpecificWeekSetupBE.SiteScheduleDiaryID);
                param[index++] = new SqlParameter("@SiteID", oMASSIT_SpecificWeekSetupBE.SiteID);
                param[index++] = new SqlParameter("@EndWeekday", oMASSIT_SpecificWeekSetupBE.EndWeekday);
                param[index++] = new SqlParameter("@DayDisabledFor", oMASSIT_SpecificWeekSetupBE.DayDisabledFor);
                param[index++] = new SqlParameter("@StartWeekday", oMASSIT_SpecificWeekSetupBE.StartWeekday);
                param[index++] = new SqlParameter("@WeekSetupSpecificID", oMASSIT_SpecificWeekSetupBE.WeekSetupSpecificID);
                param[index++] = new SqlParameter("@WeekStartDate", oMASSIT_SpecificWeekSetupBE.WeekStartDate);
                    
                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spMASSIT_WeekSetupSpecific", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows) {
                    MASSIT_SpecificWeekSetupBE oNewMASSIT_SpecificWeekSetupBE = new MASSIT_SpecificWeekSetupBE();
                    oNewMASSIT_SpecificWeekSetupBE.SiteID = Convert.ToInt32(dr["SiteID"]);
                    oNewMASSIT_SpecificWeekSetupBE.SiteScheduleDiaryID = Convert.ToInt32(dr["SiteScheduleDiaryID"]);
                    oNewMASSIT_SpecificWeekSetupBE.StartWeekday = dr["StartWeekday"] != DBNull.Value ? dr["StartWeekday"].ToString() : "";
                    oNewMASSIT_SpecificWeekSetupBE.StartTime = dr["StartTime"] != DBNull.Value ? Convert.ToDateTime(dr["StartTime"]) : (DateTime?)null;
                    oNewMASSIT_SpecificWeekSetupBE.EndWeekday = dr["EndWeekday"] != DBNull.Value ? dr["EndWeekday"].ToString() : "";
                    oNewMASSIT_SpecificWeekSetupBE.EndTime = dr["EndTime"] != DBNull.Value ? Convert.ToDateTime(dr["EndTime"]) : (DateTime?)null;
                    oNewMASSIT_SpecificWeekSetupBE.MaximumLift = dr["MaximumLift"] != DBNull.Value ? Convert.ToInt32(dr["MaximumLift"].ToString()) : (int?)null;
                    oNewMASSIT_SpecificWeekSetupBE.MaximumPallet = dr["MaximumPallet"] != DBNull.Value ? Convert.ToInt32(dr["MaximumPallet"].ToString()) : (int?)null;
                    oNewMASSIT_SpecificWeekSetupBE.MaximumContainer = dr["MaximumContainer"] != DBNull.Value ? Convert.ToInt32(dr["MaximumContainer"].ToString()) : (int?)null;
                    oNewMASSIT_SpecificWeekSetupBE.MaximumLine = dr["MaximumLine"] != DBNull.Value ? Convert.ToInt32(dr["MaximumLine"].ToString()) : (int?)null;
                    oNewMASSIT_SpecificWeekSetupBE.IsDayDisabled = dr["IsDayDisabled"] != DBNull.Value ? (Convert.ToString(dr["IsDayDisabled"]).ToLower() == "true" ? true : false) : false;

                    oNewMASSIT_SpecificWeekSetupBE.Site = new BusinessEntities.ModuleBE.AdminFunctions.MAS_SiteBE();
                    oNewMASSIT_SpecificWeekSetupBE.Site.SiteName = dr["SiteName"].ToString();

                    oNewMASSIT_SpecificWeekSetupBE.WeekStartDate = dr["WeekStartDate"] != DBNull.Value ? Convert.ToDateTime(dr["WeekStartDate"]) : (DateTime?)null;
                    oNewMASSIT_SpecificWeekSetupBE.WeekSetupSpecificID =dr["WeekSetupSpecificID"] != DBNull.Value ? Convert.ToInt32(dr["WeekSetupSpecificID"]) : (int?)null;

                    oNewMASSIT_SpecificWeekSetupBE.IsBackWeekExists = dr["IsBackWeekExists"].ToString();
                    oNewMASSIT_SpecificWeekSetupBE.IsNextWeekExists = dr["IsNextWeekExists"].ToString();

                    //Stage 6 Point 19
                    if (dt.Columns.Contains("MaximumDeliveries"))
                        oNewMASSIT_SpecificWeekSetupBE.MaximumDeliveries = dr["MaximumDeliveries"] != DBNull.Value ? Convert.ToInt32(dr["MaximumDeliveries"].ToString()) : (int?)null;


                    oMASSIT_SpecificWeekSetupBEList.Add(oNewMASSIT_SpecificWeekSetupBE);
                }
            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return oMASSIT_SpecificWeekSetupBEList;
        }

        public int? addEditSpecificWeekSetupDAL(MASSIT_SpecificWeekSetupBE oMASSIT_SpecificWeekSetupBE) {
            int? intResult = 0;
            try {
                int index = 0;
                SqlParameter[] param = new SqlParameter[17];
                param[index++] = new SqlParameter("@Action", oMASSIT_SpecificWeekSetupBE.Action);
                param[index++] = new SqlParameter("@SiteID", oMASSIT_SpecificWeekSetupBE.SiteID);
                param[index++] = new SqlParameter("@SiteScheduleDiaryID", oMASSIT_SpecificWeekSetupBE.SiteScheduleDiaryID);
                param[index++] = new SqlParameter("@StartWeekday", oMASSIT_SpecificWeekSetupBE.StartWeekday);
                param[index++] = new SqlParameter("@StartTime", oMASSIT_SpecificWeekSetupBE.StartTime);
                param[index++] = new SqlParameter("@EndWeekday", oMASSIT_SpecificWeekSetupBE.EndWeekday);
                param[index++] = new SqlParameter("@EndTime", oMASSIT_SpecificWeekSetupBE.EndTime);
                param[index++] = new SqlParameter("@MaximumLift", oMASSIT_SpecificWeekSetupBE.MaximumLift);
                param[index++] = new SqlParameter("@MaximumPallet", oMASSIT_SpecificWeekSetupBE.MaximumPallet);
                param[index++] = new SqlParameter("@MaximumContainer", oMASSIT_SpecificWeekSetupBE.MaximumContainer);
                param[index++] = new SqlParameter("@MaximumLine", oMASSIT_SpecificWeekSetupBE.MaximumLine);
                param[index++] = new SqlParameter("@DayDisabledFor", oMASSIT_SpecificWeekSetupBE.DayDisabledFor);
                param[index++] = new SqlParameter("@WeekSetupSpecificID", oMASSIT_SpecificWeekSetupBE.WeekSetupSpecificID);
                param[index++] = new SqlParameter("@WeekStartDate", oMASSIT_SpecificWeekSetupBE.WeekStartDate);
                param[index++] = new SqlParameter("@StartSlotTimeID", oMASSIT_SpecificWeekSetupBE.StartSlotTimeID);
                param[index++] = new SqlParameter("@EndSlotTimeID", oMASSIT_SpecificWeekSetupBE.EndSlotTimeID);
                //Stage 6 Point 19
                param[index++] = new SqlParameter("@MaximumDeliveries", oMASSIT_SpecificWeekSetupBE.MaximumDeliveries);


                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spMASSIT_WeekSetupSpecific", param);

                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
                else
                    intResult = 0;

            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public List<MASSIT_SpecificWeekSetupBE> GetTotalsDAL(MASSIT_SpecificWeekSetupBE oMASSIT_SpecificWeekSetupBE) {
            DataTable dt = new DataTable();
            List<MASSIT_SpecificWeekSetupBE> oMASSIT_SpecificWeekSetupBEList = new List<MASSIT_SpecificWeekSetupBE>();
            try {
                int index = 0;
                SqlParameter[] param = new SqlParameter[3];
                param[index++] = new SqlParameter("@Action", "GetTotals");
                param[index++] = new SqlParameter("@SiteID", oMASSIT_SpecificWeekSetupBE.SiteID);
                param[index++] = new SqlParameter("@WeekStartDate", oMASSIT_SpecificWeekSetupBE.WeekStartDate);
                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spMASSIT_WeekSetupSpecific", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows) {
                    MASSIT_SpecificWeekSetupBE oNewMASSIT_SpecificWeekSetupBE = new MASSIT_SpecificWeekSetupBE();
                    oNewMASSIT_SpecificWeekSetupBE.TOTAL_MaximumLift = Convert.ToInt32(dr["TOTAL_MaximumLift"]);
                    oNewMASSIT_SpecificWeekSetupBE.TOTAL_MaximumPallet = Convert.ToInt32(dr["TOTAL_MaximumPallet"]);
                    oNewMASSIT_SpecificWeekSetupBE.TOTAL_MaximumContainer = Convert.ToInt32(dr["TOTAL_MaximumContainer"]);
                    oNewMASSIT_SpecificWeekSetupBE.TOTAL_MaximumLine = Convert.ToInt32(dr["TOTAL_MaximumLine"]);
                    oMASSIT_SpecificWeekSetupBEList.Add(oNewMASSIT_SpecificWeekSetupBE);
                }
            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return oMASSIT_SpecificWeekSetupBEList;
        }
    }
}

﻿using System;
using System.Data.SqlClient;
using System.Data;

using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using Utilities;
using System.Collections.Generic;

namespace DataAccessLayer.ModuleDAL.Appointment.SiteSettings {
    public class MASSIT_HolidayDAL {

        public List<MASSIT_HolidayBE> GetHolidaysDAL(MASSIT_HolidayBE oMASSIT_HolidayBE) {
            DataTable dt = new DataTable();
            List<MASSIT_HolidayBE> oMASSIT_HolidayBEList = new List<MASSIT_HolidayBE>();
            try {
                int index = 0;
                SqlParameter[] param = new SqlParameter[6];
                param[index++] = new SqlParameter("@Action", oMASSIT_HolidayBE.Action);                
                param[index++] = new SqlParameter("@SiteID", oMASSIT_HolidayBE.Site.SiteID);
                param[index++] = new SqlParameter("@SiteIDs", oMASSIT_HolidayBE.SiteIDs);
                param[index++] = new SqlParameter("@SiteHolidayID", oMASSIT_HolidayBE.SiteHolidayID);
                param[index++] = new SqlParameter("@HolidayDate", oMASSIT_HolidayBE.HolidayDate);
                param[index++] = new SqlParameter("@HolidayYear", oMASSIT_HolidayBE.HolidayYear);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spMASSIT_Holiday", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows) {
                    MASSIT_HolidayBE oNewMASSIT_HolidayBE = new MASSIT_HolidayBE();

                    oNewMASSIT_HolidayBE.Site = new BusinessEntities.ModuleBE.AdminFunctions.MAS_SiteBE();
                    oNewMASSIT_HolidayBE.Site.SiteID = dr["SiteID"] != DBNull.Value ? Convert.ToInt32(dr["SiteID"].ToString()) : 0;
                    oNewMASSIT_HolidayBE.Site.SiteName = dr["SiteName"] != DBNull.Value ? dr["SiteName"].ToString() : string.Empty;
                    oNewMASSIT_HolidayBE.HolidayDate = dr["HolidayDate"] != DBNull.Value ? Convert.ToDateTime(dr["HolidayDate"].ToString()) : (DateTime?)null;
                    oNewMASSIT_HolidayBE.Reason = dr["Reason"] != DBNull.Value ? dr["Reason"].ToString() : string.Empty;
                    oNewMASSIT_HolidayBE.SiteHolidayID = dr["SiteHolidayID"] != DBNull.Value ? Convert.ToInt32(dr["SiteHolidayID"].ToString()) : 0;
                    oMASSIT_HolidayBEList.Add(oNewMASSIT_HolidayBE);
                }
            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return oMASSIT_HolidayBEList;
        }

        public int? addEditHolidayDAL(MASSIT_HolidayBE oMASSIT_HolidayBE) {
            int? intResult = 0;
            try {
                int index = 0;
                SqlParameter[] param = new SqlParameter[5];
                param[index++] = new SqlParameter("@Action", oMASSIT_HolidayBE.Action);
                param[index++] = new SqlParameter("@SiteHolidayID", oMASSIT_HolidayBE.SiteHolidayID);
                param[index++] = new SqlParameter("@SiteID", oMASSIT_HolidayBE.Site.SiteID);
                param[index++] = new SqlParameter("@Reason", oMASSIT_HolidayBE.Reason);
                param[index++] = new SqlParameter("@HolidayDate", oMASSIT_HolidayBE.HolidayDate);

                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spMASSIT_Holiday", param);

                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
                else
                    intResult = 0;

            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

    }
}

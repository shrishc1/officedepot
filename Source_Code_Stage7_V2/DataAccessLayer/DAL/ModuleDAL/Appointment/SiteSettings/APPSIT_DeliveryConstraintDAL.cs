﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using System.Data;
using Utilities;
using System.Data.SqlClient;

namespace DataAccessLayer.ModuleDAL.Appointment.SiteSettings {
    public class APPSIT_DeliveryConstraintDAL {
        public List<MASSIT_DeliveryConstraintBE> GetOpenDoorSlotTimeDAL(MASSIT_DeliveryConstraintBE oMASSIT_DeliveryConstraintBE) {
            DataTable dt = new DataTable();
            List<MASSIT_DeliveryConstraintBE> oMASSIT_DeliveryConstraintBEList = new List<MASSIT_DeliveryConstraintBE>();
            try {
                int index = 0;
                SqlParameter[] param = new SqlParameter[6];
                param[index++] = new SqlParameter("@Action", oMASSIT_DeliveryConstraintBE.Action);
                param[index++] = new SqlParameter("@SiteID", oMASSIT_DeliveryConstraintBE.SiteID);
                param[index++] = new SqlParameter("@Weekday", oMASSIT_DeliveryConstraintBE.Weekday);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spMASSIT_DeliveryConstraint", param);

                dt = ds.Tables[0];
                foreach (DataRow dr in dt.Rows) {
                    MASSIT_DeliveryConstraintBE oNewMASSIT_DeliveryConstraintBE = new MASSIT_DeliveryConstraintBE();
                    oNewMASSIT_DeliveryConstraintBE.StartTime = Convert.ToInt32(dr["StartTime"]);

                    oMASSIT_DeliveryConstraintBEList.Add(oNewMASSIT_DeliveryConstraintBE);
                }
            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return oMASSIT_DeliveryConstraintBEList;
        }

        public List<MASSIT_DeliveryConstraintBE> GetDeliveryConstraintDAL(MASSIT_DeliveryConstraintBE oMASSIT_DeliveryConstraintBE) {
            DataTable dt = new DataTable();
            List<MASSIT_DeliveryConstraintBE> oMASSIT_DeliveryConstraintBEList = new List<MASSIT_DeliveryConstraintBE>();
            try {
                int index = 0;
                SqlParameter[] param = new SqlParameter[6];
                param[index++] = new SqlParameter("@Action", oMASSIT_DeliveryConstraintBE.Action);
                param[index++] = new SqlParameter("@SiteID", oMASSIT_DeliveryConstraintBE.SiteID);
                param[index++] = new SqlParameter("@Weekday", oMASSIT_DeliveryConstraintBE.Weekday);
                param[index++] = new SqlParameter("@StartTime", oMASSIT_DeliveryConstraintBE.StartTime);
                param[index++] = new SqlParameter("@StartWeekday", oMASSIT_DeliveryConstraintBE.StartWeekday);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spMASSIT_DeliveryConstraint", param);

                dt = ds.Tables[0];
                foreach (DataRow dr in dt.Rows) {
                    MASSIT_DeliveryConstraintBE oNewMASSIT_DeliveryConstraintBE = new MASSIT_DeliveryConstraintBE();
                    oNewMASSIT_DeliveryConstraintBE.DeliveryConstraintID = dr["DeliveryConstraintID"] != DBNull.Value ? Convert.ToInt32(dr["DeliveryConstraintID"]) : (int?)null;
                    oNewMASSIT_DeliveryConstraintBE.StartTime = Convert.ToInt32(dr["StartTime"]);
                    oNewMASSIT_DeliveryConstraintBE.RestrictDelivery = dr["RestrictDelivery"] != DBNull.Value ? Convert.ToInt32(dr["RestrictDelivery"]) : (int?)null;
                    oNewMASSIT_DeliveryConstraintBE.MaximumLine = dr["MaximumLine"] != DBNull.Value ? Convert.ToInt32(dr["MaximumLine"]) : (int?)null;
                    oNewMASSIT_DeliveryConstraintBE.MaximumLift = dr["MaximumLift"] != DBNull.Value ? Convert.ToInt32(dr["MaximumLift"]) : (int?)null;
                    oNewMASSIT_DeliveryConstraintBE.SingleDeliveryPalletRestriction = dr["SingleDeliveryPalletRestriction"] != DBNull.Value ? Convert.ToInt32(dr["SingleDeliveryPalletRestriction"]) : (int?)null;
                    oNewMASSIT_DeliveryConstraintBE.SingleDeliveryLineRestriction = dr["SingleDeliveryLineRestriction"] != DBNull.Value ? Convert.ToInt32(dr["SingleDeliveryLineRestriction"]) : (int?)null;
                    
                    oMASSIT_DeliveryConstraintBEList.Add(oNewMASSIT_DeliveryConstraintBE);
                }
            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return oMASSIT_DeliveryConstraintBEList;
        }

        public int? addEdiDeliveryConstraintDAL(MASSIT_DeliveryConstraintBE oMASSIT_DeliveryConstraintBE) {
            int? intResult = 0;
            try {
                int index = 0;
                SqlParameter[] param = new SqlParameter[10];
                param[index++] = new SqlParameter("@Action", oMASSIT_DeliveryConstraintBE.Action);
                param[index++] = new SqlParameter("@SiteID", oMASSIT_DeliveryConstraintBE.SiteID);
                param[index++] = new SqlParameter("@Weekday", oMASSIT_DeliveryConstraintBE.Weekday.ToLower());
                param[index++] = new SqlParameter("@StartTime", oMASSIT_DeliveryConstraintBE.StartTime);
                param[index++] = new SqlParameter("@StartWeekday", oMASSIT_DeliveryConstraintBE.StartWeekday.ToLower());
                param[index++] = new SqlParameter("@RestrictDelivery", oMASSIT_DeliveryConstraintBE.RestrictDelivery);
                param[index++] = new SqlParameter("@MaximumLine", oMASSIT_DeliveryConstraintBE.MaximumLine);
                param[index++] = new SqlParameter("@MaximumLift", oMASSIT_DeliveryConstraintBE.MaximumLift);
                param[index++] = new SqlParameter("@SingleDeliveryPalletRestriction", oMASSIT_DeliveryConstraintBE.SingleDeliveryPalletRestriction);
                param[index++] = new SqlParameter("@SingleDeliveryLineRestriction", oMASSIT_DeliveryConstraintBE.SingleDeliveryLineRestriction);

                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spMASSIT_DeliveryConstraint", param);

                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
                else
                    intResult = 0;

            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }
    }
}

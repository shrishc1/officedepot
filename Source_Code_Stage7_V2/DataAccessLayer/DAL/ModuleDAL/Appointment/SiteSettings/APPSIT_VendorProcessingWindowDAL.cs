﻿using System;
using System.Data.SqlClient;
using System.Data;

using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using Utilities;
using System.Collections.Generic;

namespace DataAccessLayer.ModuleDAL.Appointment.SiteSettings {
    public class APPSIT_VendorProcessingWindowDAL : DataAccessLayer.BaseDAL {
        public List<MASSIT_VendorProcessingWindowBE> GetVendorDetailsDAL(MASSIT_VendorProcessingWindowBE oMASSIT_VendorProcessingWindowBE) {
            DataTable dt = new DataTable();
            List<MASSIT_VendorProcessingWindowBE> oMASSIT_VendorProcessingWindowBEList = new List<MASSIT_VendorProcessingWindowBE>();
            try {
                int index = 0;
                SqlParameter[] param = new SqlParameter[8];
                param[index++] = new SqlParameter("@Action", oMASSIT_VendorProcessingWindowBE.Action);
                param[index++] = new SqlParameter("@SiteVendorID", oMASSIT_VendorProcessingWindowBE.SiteVendorID);
                param[index++] = new SqlParameter("@VendorID", oMASSIT_VendorProcessingWindowBE.VendorID);
                param[index++] = new SqlParameter("@SiteID", oMASSIT_VendorProcessingWindowBE.SiteID);
                param[index++] = new SqlParameter("@APP_PalletsUnloadedPerHour", oMASSIT_VendorProcessingWindowBE.APP_PalletsUnloadedPerHour);
                param[index++] = new SqlParameter("@APP_CartonsUnloadedPerHour", oMASSIT_VendorProcessingWindowBE.APP_CartonsUnloadedPerHour);
                param[index++] = new SqlParameter("@APP_ReceiptingLinesPerHour", oMASSIT_VendorProcessingWindowBE.APP_ReceiptingLinesPerHour);
                param[index++] = new SqlParameter("@UserID", oMASSIT_VendorProcessingWindowBE.User.UserID);
                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spMASSIT_VendorProcessing", param);

                dt = ds.Tables[0];
                foreach (DataRow dr in dt.Rows) {
                    MASSIT_VendorProcessingWindowBE oNewMASSIT_VendorProcessingWindowBE = new MASSIT_VendorProcessingWindowBE();
                    oNewMASSIT_VendorProcessingWindowBE.SiteVendorID = Convert.ToInt32(dr["SiteVendorID"]);
                    oNewMASSIT_VendorProcessingWindowBE.VendorID =Convert.ToInt32(dr["VendorID"]);
                    oNewMASSIT_VendorProcessingWindowBE.SiteID = Convert.ToInt32(dr["SiteID"]);

                    oNewMASSIT_VendorProcessingWindowBE.APP_PalletsUnloadedPerHour = dr["APP_PalletsUnloadedPerHour"] != DBNull.Value ? Convert.ToInt32(dr["APP_PalletsUnloadedPerHour"]) : 0;
                    oNewMASSIT_VendorProcessingWindowBE.APP_CartonsUnloadedPerHour = dr["APP_CartonsUnloadedPerHour"] != DBNull.Value ? Convert.ToInt32(dr["APP_CartonsUnloadedPerHour"]) : 0;
                    oNewMASSIT_VendorProcessingWindowBE.APP_ReceiptingLinesPerHour = dr["APP_ReceiptingLinesPerHour"] != DBNull.Value ? Convert.ToInt32(dr["APP_ReceiptingLinesPerHour"]) : 0;
                    oNewMASSIT_VendorProcessingWindowBE.Site = new BusinessEntities.ModuleBE.AdminFunctions.MAS_SiteBE();

                    oNewMASSIT_VendorProcessingWindowBE.Site.SiteName = dr["SiteDescription"].ToString();
                    oNewMASSIT_VendorProcessingWindowBE.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();
                    oNewMASSIT_VendorProcessingWindowBE.Vendor.VendorName = dr["VendorName"].ToString();

                    oMASSIT_VendorProcessingWindowBEList.Add(oNewMASSIT_VendorProcessingWindowBE);
                }
            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return oMASSIT_VendorProcessingWindowBEList;
        }
        public DataTable GetVendorDetailsDAL(MASSIT_VendorProcessingWindowBE oMASSIT_VendorProcessingWindowBE  , string Nothing = "") {
            DataTable dt = new DataTable();
            List<MASSIT_VendorProcessingWindowBE> oMASSIT_VendorProcessingWindowBEList = new List<MASSIT_VendorProcessingWindowBE>();
            try {
                int index = 0;
                SqlParameter[] param = new SqlParameter[10];
                param[index++] = new SqlParameter("@Action", oMASSIT_VendorProcessingWindowBE.Action);
                param[index++] = new SqlParameter("@SiteVendorID", oMASSIT_VendorProcessingWindowBE.SiteVendorID);
                param[index++] = new SqlParameter("@VendorID", oMASSIT_VendorProcessingWindowBE.VendorID);
                param[index++] = new SqlParameter("@SiteID", oMASSIT_VendorProcessingWindowBE.SiteID);
                param[index++] = new SqlParameter("@APP_PalletsUnloadedPerHour", oMASSIT_VendorProcessingWindowBE.APP_PalletsUnloadedPerHour);
                param[index++] = new SqlParameter("@APP_CartonsUnloadedPerHour", oMASSIT_VendorProcessingWindowBE.APP_CartonsUnloadedPerHour);
                param[index++] = new SqlParameter("@APP_ReceiptingLinesPerHour", oMASSIT_VendorProcessingWindowBE.APP_ReceiptingLinesPerHour);
                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spMASSIT_VendorProcessing", param);

                dt = ds.Tables[0];
                foreach (DataRow dr in dt.Rows) {
                    MASSIT_VendorProcessingWindowBE oNewMASSIT_VendorProcessingWindowBE = new MASSIT_VendorProcessingWindowBE();
                    oNewMASSIT_VendorProcessingWindowBE.SiteVendorID = Convert.ToInt32(dr["SiteVendorID"]);
                    oNewMASSIT_VendorProcessingWindowBE.VendorID = Convert.ToInt32(dr["VendorID"]);
                    oNewMASSIT_VendorProcessingWindowBE.SiteID = Convert.ToInt32(dr["SiteID"]);
                    oNewMASSIT_VendorProcessingWindowBE.APP_PalletsUnloadedPerHour = Convert.ToInt32(dr["APP_PalletsUnloadedPerHour"]);
                    oNewMASSIT_VendorProcessingWindowBE.APP_CartonsUnloadedPerHour = Convert.ToInt32(dr["APP_CartonsUnloadedPerHour"]);
                    oNewMASSIT_VendorProcessingWindowBE.APP_ReceiptingLinesPerHour = Convert.ToInt32(dr["APP_ReceiptingLinesPerHour"]);
                    oNewMASSIT_VendorProcessingWindowBE.Site = new BusinessEntities.ModuleBE.AdminFunctions.MAS_SiteBE();

                    oNewMASSIT_VendorProcessingWindowBE.Site.SiteName = dr["SiteDescription"].ToString();
                    oNewMASSIT_VendorProcessingWindowBE.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();
                    oNewMASSIT_VendorProcessingWindowBE.Vendor.VendorName = dr["VendorName"].ToString();

                    oMASSIT_VendorProcessingWindowBEList.Add(oNewMASSIT_VendorProcessingWindowBE);
                }
            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return dt;
        }
        public int? addEditVendorDetailsDAL(MASSIT_VendorProcessingWindowBE oMASSIT_VendorProcessingWindowBE) {
            int? intResult = 0;
            DataTable dt = new DataTable();
            List<MASSIT_VendorProcessingWindowBE> oMASSIT_VendorProcessingWindowBEList = new List<MASSIT_VendorProcessingWindowBE>();
            try {
                int index = 0;
                SqlParameter[] param = new SqlParameter[10];
                param[index++] = new SqlParameter("@Action", oMASSIT_VendorProcessingWindowBE.Action);
                param[index++] = new SqlParameter("@SiteVendorID", oMASSIT_VendorProcessingWindowBE.SiteVendorID);
                param[index++] = new SqlParameter("@VendorID", oMASSIT_VendorProcessingWindowBE.VendorID);
                param[index++] = new SqlParameter("@SiteID", oMASSIT_VendorProcessingWindowBE.SiteID);
                param[index++] = new SqlParameter("@APP_PalletsUnloadedPerHour", oMASSIT_VendorProcessingWindowBE.APP_PalletsUnloadedPerHour);
                param[index++] = new SqlParameter("@APP_CartonsUnloadedPerHour", oMASSIT_VendorProcessingWindowBE.APP_CartonsUnloadedPerHour);
                param[index++] = new SqlParameter("@APP_ReceiptingLinesPerHour", oMASSIT_VendorProcessingWindowBE.APP_ReceiptingLinesPerHour);
              

                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spMASSIT_VendorProcessing", param);

                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
                else
                    intResult = 0;

            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return intResult;

        }


    }
}

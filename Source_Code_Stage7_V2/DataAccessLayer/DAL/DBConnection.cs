﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

/*****************************************************************
 Purpose        :   To Create Connection String... its a singleton class
 *****************************************************************/

namespace DataAccessLayer
{
    public class DBConnection
    {
        private static string ConStr =System.Configuration.ConfigurationSettings.AppSettings["sConn"];

        private DBConnection() { }

        public static string Connection
        {
            get { return ConStr; }
        }
    }
}







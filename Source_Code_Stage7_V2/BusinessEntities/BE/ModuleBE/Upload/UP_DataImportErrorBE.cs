﻿// -----------------------------------------------------------------------
// <copyright file="UP_DataImportErrorBE.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace BusinessEntities.ModuleBE.Upload
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class UP_DataImportErrorBE
    {
        public int FolderDownloadID { get; set; }
        public int ManualFileUploadID { get; set; }
        public string ErrorDescription { get; set; }
        public DateTime UpdatedDate { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessEntities.ModuleBE.Upload
{
    [Serializable]
    public class OTIF_LeadTimeSKUBE : BaseBe 
    { 
        public int? PurchaseOrderID { get; set; }

        public string Purchase_order { get; set; }

        public string ProductDescription { get; set; }

        public int? SKUID { get; set; }

        public int SiteID { get; set; }

        public string UserName { get; set; }

        public string VendorName { get; set; }

        public DateTime? DateApplied { get; set; }

        public string SelectedProductCodes { get; set; }
        //public int? Line_No { get; set; }

        public string Reason { get; set; }

        public string Vendor_No { get; set; }

        public int VendorID { get; set; }

        //public string Direct_code { get; set; }

        //public string OD_Code { get; set; }

        //public string Vendor_Code { get; set; }

        //public string Product_description { get; set; }

        //public string UOM { get; set; }

        //public string Original_quantity { get; set; }

        //public string Outstanding_Qty { get; set; }

        //public decimal? PO_cost { get; set; }

        public DateTime? Order_raised { get; set; }

        //public DateTime? Original_due_date { get; set; }

        //public DateTime? Expected_date { get; set; }

        //public string Buyer_no { get; set; }

        //public string Item_classification { get; set; }

        //public string Item_type { get; set; }

        //public string Item_Category { get; set; }

        //public string Other { get; set; }

        //public DateTime? RevisedDueDate { get; set; }

        //public string LastUploadedFlag { get; set; }

        //public string SelectedProductCodes { get; set; }

        public string SelectedVendorIDs { get; set; }        

        //public string ProductDescription { get; set; }

        public string CountryName { get; set; }

        public string SiteName { get; set; }

        //public BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE Vendor { get; set; }

        //public int OutstandingLines { get; set; }

        public BusinessEntities.ModuleBE.Upload.UP_SKUBE SKU { get; set; }

        //public BusinessEntities.ModuleBE.AdminFunctions.MAS_SiteBE Site { get; set; }

        //public int BookingID { get; set; }

        //public int? Qty_receipted { get; set; }

        //public int? LineCount { get; set; }

        //public int CountryID { get; set; }
        //public string SubVendor { get; set; }

        //// Sprint 1 - Point 7
        //public DateTime? BookingDeliveryDate { get; set; }
        //public string Status { get; set; }

        //public DateTime UpdatedDate { get; set; }
        //public DateTime? LastReceiptDate { get; set; }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessEntities.ModuleBE.Discrepancy.LogDiscrepancy
{
    [Serializable]
   public class DiscrepancyBE:BaseBe
    {

        //DiscrepancyLog Columns
        public int? DiscrepancyLogID { get; set; }
        public string VDRNo { get; set; }
        public int? SiteID { get; set; }      
        public int? DiscrepancyTypeID { get; set; }
        public DateTime? DiscrepancyLogDate { get; set; }
        public int? PurchaseOrderID { get; set; }
        public string PurchaseOrderNumber { get; set; }
        public DateTime? PurchaseOrderDate { get; set; }
        public string InternalComments { get; set; }
        public string DeliveryNoteNumber { get; set; }
        public DateTime? DeliveryArrivedDate { get; set; }
        public char? CommunicationType { get; set; }
        public string CommunicationTo { get; set; }
        public int? POSiteID { get; set; }
        public int? VendorID { get; set; }
        public int? CarrierID { get; set; }
        public string CarrierName { get; set; }
     
        public string PersentationIssueComment { get; set; }
        public string QualityIssueComment { get; set; }
        public string FailPalletSpecificationComment { get; set; }
        public string PrematureReceiptInvoiceComment { get; set; }
        public string GenericDiscrepancyActionRequired { get; set; }
        public string OfficeDepotContactName { get; set; }
        public string OfficeDepotContactPhone { get; set; }
        public string POSiteName { get; set; }
        public string GenericDiscrepancyIssueComment { get; set; }
        
        //DiscrepancyItem Columns
        public int DiscrepancyItemID { get; set; }
        public int? ItemPurchaseOrderID {get;set;}
        public int? Line_no { get; set; }
        public string ODSKUCode { get; set; }
        public string DirectCode { get; set; }
        public string VendorCode { get; set; }
        public string ProductDescription { get; set; }
        public int? OutstandingQuantity { get; set; }
        public int? OriginalQuantity { get; set; }
        public string UOM { get; set; }
        public int? DeliveredNoteQuantity { get; set; }      
        public int? DeliveredQuantity { get; set; }
        public int? OversQuantity { get; set; }
        public int? ShortageQuantity { get; set; }
        public int? DamageQuantity { get; set; }
        public string DamageDescription { get; set; }
        public int? AmendedQuantity { get; set; }
        public Boolean? NoPOEscalate { get; set; }
        public string PackSizeOrdered { get; set; }
        public string PackSizeReceived { get; set; }
        public int? WrongPackReceived { get; set; }
        public int? ChaseQuantity { get; set; }



        public int? StockPlannerID { get; set; }
        public string StockPlannerNO { get; set; }
        public string StockPlannerContact { get; set; }
        public string StockPlannerName { get; set; }
        public string StockPlannerEmailID { get; set; }

        public string PurchaseOrder_Order_raised { get; set; }
        public string VendorNoName { get; set; }

        public string CorrectODSKUCode { get; set; }
        public string CorrectProductDescription { get; set; }
        public string CorrectUOM { get; set; }
        public string CorrectVendorCode { get; set; }

        public DateTime? Order_raised { get; set; }

        public BusinessEntities.ModuleBE.Upload.Up_PurchaseOrderDetailBE PurchaseOrder { get; set; }
        public BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE Vendor { get; set; }
        public BusinessEntities.ModuleBE.Security.SCT_UserBE User { get; set; }
        public BusinessEntities.ModuleBE.AdminFunctions.MAS_SiteBE Site { get; set; }
        public string DiscrepancyStatus { get; set; }

        //Discrepancy Type
       
        public string DiscrepancyType { get; set; }

        //Discrepancy Search Parameters
        public int? SCDiscrepancyTypeID { get; set; }
        public string SCDiscrepancyNo { get; set; }
        public string SCSelectedSiteIDs { get; set; }
        public int? SCSPUserID { get; set; }
        public string SCPurchaseOrderNo { get; set; }
        public DateTime? SCPurchaseOrderDate { get; set; }
        public DateTime? SCDiscrepancyDateFrom { get; set; }
        public DateTime? SCDiscrepancyDateTo { get; set; }
        public string SCSelectedVendorIDs { get; set; }
        public string SCDeliveryNoteNumber { get; set; }


        public int? UserID { get; set; }

        //RPT_DiscrepancyWorkflow table
        public int? DiscrepancyWorkFlowID { get; set; }
        public DateTime? LoggedDateTime { get; set; }
        public int? LevelNumber { get; set; }

        public string GINHTML { get; set; }
        public bool GINActionRequired { get; set; }
        public string GINUserControl { get; set; }
        public string INVHTML { get; set; }
        public bool INVActionRequired { get; set; }
        public string INVUserControl { get; set; }
        public string APHTML { get; set; }
        public bool APActionRequired { get; set; }
        public string APUserControl { get; set; }
        public string VENHTML { get; set; }
        public bool VenActionRequired { get; set; }
        public string VENUserControl { get; set; }
        public bool CloseDiscrepancy { get; set; }
        public string NextEscalationLevel { get; set; }
        public string DELHTML { get; set; }

        public decimal? Freight_Charges { get; set; }
        public int? NumberOfPallets { get; set; }
        public decimal? TotalLabourCost { get; set; }
        public string DiscrepancyStatusDiscreption { get; set; }
        public bool? NoEscalation { get; set; }
        public string NoPaperworkComment { get; set; }

        public char? GreaterThan3DaysFlag { get; set; }

        public decimal? VendorActionElapsedTime { get; set; }

        public int? ForceClosedUserId { get; set; }
        public string ForceClosedUserName { get; set; }
        public string ForceClosedDate { get; set; }
        public string Username { get; set; }


        public string ItemVal { get; set; }
        public string CollectionAuthNumber { get; set; }

        public long? DiscrepancyLogCommentID { get; set; }
        public DateTime? CommentDateTime { get; set; }
        public string Comment { get; set; }
        public int? RowNumber { get; set; }

        public string GI { get; set; }
        public string INV { get; set; }
        public string AP { get; set; }
        public string VEN { get; set; }
        public string PickersName { get; set; }
        public string ShuttleType { get; set; }
        public int? DiscrepancyQty { get; set; }
        public DISLog_QueryDiscrepancyBE QueryDiscrepancy { get; set; }
        public int? UserIdForConsVendors { get; set; }
        public string EscalationLevel { get; set; }
        public int? DeletedUserId { get; set; }
        public DateTime? DeletedUserDate { get; set; }
        public string AccountPaybleName { get; set; }
        public string AccountPayblePhoneNo { get; set; }
        public string VendorStatus { get; set; }
    }
}


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessEntities.ModuleBE.OnTimeInFull.Report
{
    [Serializable]
   public  class RMSCategoryBE:BaseBe
    {
        public string SelectedCountryIDs { get; set; }
        public string SelectedSiteIDs { get; set; }

        public string SelectedVendorIDs { get; set; }

        public string SelectedStockPlannerIDs { get; set; }
        public string VendorNumber { get; set; }

        public string VendorName { get; set; }

        public DateTime? DateTo { get; set; }

        public DateTime? DateFrom { get; set; }
        public int? PurchaseOrderDue { get; set; }

        public int? PurchaseOrderReceived { get; set; }

        public int UserId { get; set; }
        public string SelectedItemClassification { get; set; }

        public string SelectedRMSCategoryIDs { get; set; }
        public string  ReportAction { get; set; }
        public string ReportType { get; set; }
      
    }
}

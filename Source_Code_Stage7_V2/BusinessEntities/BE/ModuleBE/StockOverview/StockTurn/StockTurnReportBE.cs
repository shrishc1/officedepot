﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessEntities.ModuleBE.StockOverview.StockTurn
{
     [Serializable]
    public class StockTurnReportBE : BaseBe
    {
        public string SelectedCountryIDs { get; set; }

        public string SelectedSiteIDs { get; set; }

        public string SelectedVendorIDs { get; set; }

        public string SelectedStockPlannerIDs { get; set; }

        public string SelectedAPIDs { get; set; }

        public string SelectedSKUCodes { get; set; }

        public string SelectedPurchaseOrderNo { get; set; }

        public string SelectedItemClassification { get; set; }

        public string VendorNumber { get; set; }

        public string VendorName { get; set; }

        public DateTime? DateTo { get; set; }

        public DateTime? DateFrom { get; set; }

        public string SelectedRMSCategoryIDs { get; set; }

        public string ReportAction { get ; set;    }

        public string ReportType { get ; set    ; }

        public bool IsVendorScheduler { get; set; }
    }
}

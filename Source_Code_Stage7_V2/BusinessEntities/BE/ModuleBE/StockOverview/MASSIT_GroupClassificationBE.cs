﻿using System;


namespace BusinessEntities.ModuleBE.StockOverview
{
   [Serializable]
    public class MASSIT_GroupClassificationBE:BaseBe
    {
        public int GroupClassificationID { get; set; }
        public int? SiteID { get; set; }
        public string SKUEndingWith { get; set; }
        public char? ReviewStatus { get; set; }
    }
}

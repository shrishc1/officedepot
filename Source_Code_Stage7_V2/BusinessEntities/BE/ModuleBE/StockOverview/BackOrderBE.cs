﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using BusinessEntities.ModuleBE.Upload;
using BusinessEntities.ModuleBE.Security;

namespace BusinessEntities.ModuleBE.StockOverview
{

    [Serializable]
    public class BackOrderBE : BaseBe
    {
        public UP_VendorBE Vendor { get; set; }
        public Up_PurchaseOrderDetailBE PurchaseOrder { get; set; }
        public UP_SKUBE SKU { get; set; }
        public SCT_UserBE User { get; set; }


        //Penalty Maintenance Parameters
        public int? PenaltyID { get; set; }
        public string IsVendorSubscribeToPenalty { get; set; }
        public string PenaltyType { get; set; }
        public double? OTIFRate { get; set; }
        public decimal? OTIFLineValuePenalty { get; set; }
        public double? ValueOfBackOrder { get; set; }
        public int? PenaltyAppliedId { get; set; }
        public string PenaltyAppliedName { get; set; }
        public string LocalConslolidation { get; set; }
        public string EUConsolidation { get; set; }

        // 
        public int CurrentMonthCount { get; set; }
        public int PreviousMonthCount { get; set; }
        public int? NoofBO { get; set; }
        public decimal? QtyonBackOrder { get; set; }
        public int? NoofBOWithPenalities { get; set; }
        public decimal? PotentialPenaltyCharge { get; set; }
        public DateTime? BOIncurredDate { get; set; }
        public string Year { get; set; }
        public string PenaltyRelatingTo { get; set; }
        public int SKUCountOnDashboard { get; set; }
        public int SiteID { get; set; }

        //BackOrder Penalty Flow Parameters
        public int? BOReportingID { get; set; }
        public string InventoryReviewStatus { get; set; }
        public string VendorBOReviewStatus { get; set; }
        public string DisputeReviewStatus { get; set; }
        public string MediatorStatus { get; set; }
        public string AccountsPayableAction { get; set; }
        public string OverallStatus { get; set; }
        public int? InventoryActionTakenBy { get; set; }
        public int? VendorActionTakenBy { get; set; }
        public string VendorActionTakenName { get; set; }
        public int? DisputeReviewActionTakenBy { get; set; }
        public int? MediatorActionTakenBy { get; set; }
        public int? APActionTakenBy { get; set; }
        public DateTime? InventoryActionDate { get; set; }
        public DateTime? VendorActionDate { get; set; }
        public DateTime? DisputeReviewActionDate { get; set; }
        public DateTime? MediatorActionDate { get; set; }
        public DateTime? APActionDate { get; set; }
        public string DisputeReviewActionTakenName { get; set; }
        public string IsVendorCommuncationSent { get; set; }

        //PenaltyCharge Parameters
        public int? PenaltyChargeID { get; set; }
        public decimal? DisputedValue { get; set; }
        public decimal? RevisedPenalty { get; set; }
        public string VendorComment { get; set; }
        public string SPAction { get; set; }
        public string SPComment { get; set; }
        public decimal? ImposedPenaltyCharge { get; set; }
        public string DisputeForwaredTo { get; set; }
        public string MediatorAction { get; set; }
        public string MediatorComments { get; set; }
        public decimal? AgreedPenaltyCharge { get; set; }
        public string PenaltyChargeAgreedWith { get; set; }
        public DateTime? PenaltyChargeAgreedDatetime { get; set; }
        public DateTime? InitialCommDate { get; set; }

        //AP Adress
        public string APAddress1 { get; set; }
        public string APAddress2 { get; set; }
        public string APAddress3 { get; set; }
        public string APAddress4 { get; set; }
        public string APAddress5 { get; set; }
        public string APAddress6 { get; set; }
        public int? APCountryID { get; set; }
        public string APCountryName { get; set; }
        public string APPincode { get; set; }

        //Filter Parameters
        public string SelectedCountryIDs { get; set; }
        public string SelectedVendorIDs { get; set; }
        public string SelectedStockPlannerIDs { get; set; }
        public string SelectedSiteIDs { get; set; }
        public string SelectedODSkUCode { get; set; }
        public string SelectedBOIncurredDate { get; set; }
        public string SelectedMonth { get; set; }
        public DateTime SelectedDateTo { get; set; }
        public DateTime SelectedDateFrom { get; set; }
        public string SelectedBOReportingID { get; set; }
        public string SelectedSKUID { get; set; }
        public string SelectedBOIDPC { get; set; }

        //Grid Paging Parameters
        public int GridCurrentPageNo { get; set; }
        public int GridPageSize { get; set; }
        public int TotalRecords { get; set; }

        //
        public string Status { get; set; }
        public DateTime? DateBOIncurred { get; set; }
        public string Resolution { get; set; }
        public string QueryMonth { get; set; }
        public string APClerk { get; set; }
        public double? OTIFPenaltyCharges { get; set; }
        public string PurchaseOrderValue { get; set; }
        public int? CountofBackOrder { get; set; }
        public DateTime? BOHDate { get; set; }

        //To be deleted later
        public string Country { get; set; }
        public double? OTIFPenaltyRate { get; set; }
        public string Currency1 { get; set; }
        public string Suscribe { get; set; }

        public string Subscribe { get; set; }
        public decimal? YTDTotalNoOfBO { get; set; }
        public decimal? YTDTotalNoOfVendorBO { get; set; }
        public decimal? YTDPenaltyCharges { get; set; }

        public int? CountryID { get; set; }
        public decimal? Rate { get; set; }
        public decimal? NoOfBO { get; set; }
        public decimal? QtyOnBO { get; set; }
        public string Month { get; set; }
        public decimal? AmountDebited { get; set; }
        public string SelectedAPClerkIDs { get; set; }
        public string SiteName { get; set; }
    }
}


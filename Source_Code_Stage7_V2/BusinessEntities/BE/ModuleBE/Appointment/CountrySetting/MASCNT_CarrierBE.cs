﻿
using System;
namespace BusinessEntities.ModuleBE.Appointment.CountrySetting {
    [Serializable]
    public class MASCNT_CarrierBE : BusinessEntities.BaseBe {
        public string Action { get; set; }        
        public int CountryID { get; set; }
        //public string  CountryName { get; set; }
        public string CountryIDs { get; set; }
        public string CarrierName { get; set; }
        public bool IsActive { get; set; }
        public bool IsNarrativeRequired { get; set; }
        public BusinessEntities.ModuleBE.AdminFunctions.MAS_CountryBE Country {get; set;}
        public BusinessEntities.ModuleBE.AdminFunctions.MAS_SiteBE Site { get; set; }
        public BusinessEntities.ModuleBE.Security.SCT_UserBE User { get; set; }
        public BusinessEntities.ModuleBE.Discrepancy.LogDiscrepancy.DiscrepancyBE Discrepancy { get; set; }

        public int SiteCarrierID { get; set; }
        public int SiteID { get; set; }
        public string SiteName { get; set; }
        public int CarrierID { get; set; }
        public bool? APP_CheckingRequired { get; set; }
        public int? APP_MaximumPallets { get; set; }
        public int? APP_MaximumLines { get; set; }
        public int? APP_MaximumDeliveriesInADay { get; set; }
        public bool? APP_CannotDeliveryOnMonday { get; set; }
        public bool? APP_CannotDeliveryOnTuesday { get; set; }
        public bool? APP_CannotDeliveryOnWednessday { get; set; }
        public bool? APP_CannotDeliveryOnThrusday { get; set; }
        public bool? APP_CannotDeliveryOnFriday { get; set; }
        public bool? APP_CannotDeliveryOnSaturday { get; set; }
        public bool? APP_CannotDeliveryOnSunday { get; set; }
        public DateTime? APP_CannotDeliverAfter { get; set; }
        public DateTime? APP_CannotDeliverBefore { get; set; }
        public int? AfterSlotTimeID { get; set; }
        public string AfterSlotTime { get; set; }
        public int? BeforeSlotTimeID { get; set; }
        public string BeforeSlotTime { get; set; }

        public int? AfterOrderBYId { get; set; }
        public int? BeforeOrderBYId { get; set; }
    }
}

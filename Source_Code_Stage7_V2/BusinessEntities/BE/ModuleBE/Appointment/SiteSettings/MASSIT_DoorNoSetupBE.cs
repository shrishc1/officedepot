﻿
using System;
namespace BusinessEntities.ModuleBE.Appointment.SiteSettings {
    [Serializable]
    public class MASSIT_DoorNoSetupBE : BusinessEntities.BaseBe {
        public string Action { get; set; }
        public bool IsActive { get; set; }
        public int SiteDoorNumberID { get; set; }
        public int SiteID { get; set; }
        public string DoorNumber { get; set; }
        public int SiteDoorTypeID { get; set; }
        public BusinessEntities.ModuleBE.AdminFunctions.MAS_SiteBE Site { get; set; }
        public BusinessEntities.ModuleBE.Appointment.CountrySetting.MASCNT_DoorTypeBE DoorType { get; set; }
        public BusinessEntities.ModuleBE.Security.SCT_UserBE User { get; set; }
        public string Reference { get; set; }
    }
}

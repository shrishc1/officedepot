﻿using System;

namespace BusinessEntities.ModuleBE.Appointment.SiteSettings {

    [Serializable]
    public class UP_VendorBE : BaseBe {
        public string Buyer { get; set; }
        public int VendorID { get; set; }
        public int? CountryID { get; set; }
        public string CountryName { get; set; }
        public string Vendor_No { get; set; }
        public string VendorName { get; set; }
        public string VendorContactName { get; set; }
        public string VendorContactFax { get; set; }
        public string VendorContactEmail { get; set; }
        public string VendorContactNumber { get; set; }
        public string address1 { get; set; }
        public string address2 { get; set; }
        public string city { get; set; }
        public string county { get; set; }
        public string VMPPIN { get; set; }
        public string VMPPOU { get; set; }
        public string VMTCC1 { get; set; }
        public string VMTEL1 { get; set; }
        public string Fax_country_code { get; set; }
        public string Fax_no { get; set; }
        public char? VendorFlag { get; set; }
        public int? ParentVendorID { get; set; }
        public string ConsolidatedVendors { get; set; }
        public string ConsolidatedCode { get; set; }
        public string ConsolidatedName { get; set; }
        public int? ConsolidatedCountryID { get; set; }
        public string ConsolidatedCountryName { get; set; }
        public string OTIF_PreferedFrequency { get; set; }
        public decimal? OTIF_AbsolutePenaltyBelowPercentage { get; set; }
        public decimal? OTIF_AbsoluteAppliedPenaltyPercentage { get; set; }
        public decimal? OTIF_ToleratedPenaltyBelowPercentage { get; set; }
        public decimal? OTIF_ToleratedAppliedPenaltyPercentage { get; set; }
        public decimal? OTIF_ODMeasurePenaltyBelowPercentage { get; set; }
        public decimal? OTIF_ODMeasureAppliedPenaltyPercentage { get; set; }
        public decimal? OTIF_BackOrderValue { get; set; }
        public int? CurrencyID { get; set; }
        public string CurrencyName { get; set; }

        public string StockPlannerNumber { get; set; }
        public string StockPlannerContact { get; set; }
        public int? StockPlannerID { get; set; }

        public BusinessEntities.ModuleBE.AdminFunctions.MAS_SiteBE Site { get; set; }

        public BusinessEntities.ModuleBE.Security.SCT_UserBE User { get; set; }

        public BusinessEntities.ModuleBE.Appointment.SiteSettings.MASSIT_VendorBE VendorDetails { get; set; }

        public bool? IsChildRequired {
            get;
            set;
        }

        public bool? IsParentRequired {
            get;
            set;
        }

        public bool? IsStandAloneRequired {
            get;
            set;
        }

        public bool? IsGrandParentRequired {
            get;
            set;
        }

        public bool? IsCountryRequiredWithVendor { get; set; }
        public string Language { get; set; }
        public int LanguageID { get; set; }
        public int OverrideLanguageID { get; set; }

        public string StockPlannerName { get; set; }

        public string ReturnAddress { get; set; }
        public string VendorActiveSiteIDs { get; set; }

        public char? ConsVendorFlag { get; set; }
        public DateTime UpdatedDate { get; set; }

        public string IsActiveVendor { get; set; }
        public string VendorIDs { get; set; }
        public string SearchedVendorText { get; set; }
        public int? PageIndex { get; set; }
        public int? PageSize { get; set; }
        public string VendorCountryName { get; set; }

        public int EuropeanorLocal { get; set; }

        public int MasterParentVendorID { get; set; }
        public string VendorVATRef { get; set; }

        public bool IsSendDailyPO { get; set; }
        public bool IsSendWeeklyExpediteReminder { get; set; }

        public int VendorCountToCreateReport { get; set; }

        public int? otifSchedularId { get; set; }

        public string otifApplicationName { get; set; }
    }
}

﻿using BusinessEntities.ModuleBE.AdminFunctions;
using System;

namespace BusinessEntities.ModuleBE.Appointment.SiteSettings {
    [Serializable]
    public class MASSIT_VehicleDoorMatrixBE : BusinessEntities.BaseBe{
        public string Action { get; set; }
        public bool IsActive { get; set; }
        public int VehicleTypeDoorTypeID { get; set; }
        public int DoorTypeID { get; set; }
        public int VehicleTypeID { get; set; }
        public int Priority { get; set; }
        public int SiteID { get; set; }
        public BusinessEntities.ModuleBE.Appointment.CountrySetting.MASCNT_DoorTypeBE DoorType { get; set; }
        public BusinessEntities.ModuleBE.Appointment.SiteSettings.MASSIT_VehicleTypeBE VehicleType { get; set; }
        public BusinessEntities.ModuleBE.Security.SCT_UserBE User { get; set; }
        
    }
}

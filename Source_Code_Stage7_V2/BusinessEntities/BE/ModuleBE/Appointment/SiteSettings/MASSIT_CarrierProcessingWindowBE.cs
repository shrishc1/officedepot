﻿
using System;
namespace BusinessEntities.ModuleBE.Appointment.SiteSettings {
    [Serializable]
    public class MASSIT_CarrierProcessingWindowBE : BusinessEntities.BaseBe {
        public string Action { get; set; }
        public bool IsActive { get; set; }
        public int SiteCarrierID { get; set; }
        public int SiteID { get; set; }
        public int CarrierID { get; set; }
        public bool APP_IsNonTimeDeliveryAllowed { get; set; }
        public bool APP_IsBookingValidationExcluded { get; set; }
        public int APP_PalletsUnloadedPerHour { get; set; }
        public int APP_CartonsUnloadedPerHour { get; set; }
        public int APP_ReceiptingLinesPerHour { get; set; }
        public string APP_DeliveryTypeExclusion { get; set; }
        public BusinessEntities.ModuleBE.AdminFunctions.MAS_SiteBE Site { get; set; }
        public BusinessEntities.ModuleBE.Appointment.CountrySetting.MASCNT_CarrierBE Carrier { get; set; }
        public BusinessEntities.ModuleBE.Security.SCT_UserBE User { get; set; }

    }
}

﻿using System;

using BusinessEntities.ModuleBE.AdminFunctions;

namespace BusinessEntities.ModuleBE.Appointment.SiteSettings {
    [Serializable]
    public class MASSIT_SpecificWeekSetupBE : BaseBe{
        public int? SiteScheduleDiaryID { get; set; }
        public int? SiteID { get; set; }
        public MAS_SiteBE Site { get; set; }
        public string StartWeekday { get; set; }
        public DateTime? StartTime { get; set; }
        public string EndWeekday { get; set; }
        public DateTime? EndTime { get; set; }
        public int? MaximumLift { get; set; }
        public int? MaximumPallet { get; set; }
        public int? MaximumContainer { get; set; }
        public int? MaximumLine { get; set; }
        public bool IsDayDisabled { get; set; }
        public string DayDisabledFor { get; set; }
        public int? WeekSetupSpecificID { get; set; }
        public DateTime? WeekStartDate { get; set; }
        public string IsBackWeekExists { get; set; }
        public string IsNextWeekExists { get; set; }
        public DateTime? BackWeekStartDate { get; set; }
        public DateTime? NextWeekEndDate { get; set; }

        public int TOTAL_MaximumLift { get; set; }
        public int TOTAL_MaximumPallet { get; set; }
        public int TOTAL_MaximumContainer { get; set; }
        public int TOTAL_MaximumLine { get; set; }
        public int? StartSlotTimeID { get; set; }
        public int? EndSlotTimeID { get; set; }

        //Stage 6 Point 19
        public int? MaximumDeliveries { get; set; }
    }
}

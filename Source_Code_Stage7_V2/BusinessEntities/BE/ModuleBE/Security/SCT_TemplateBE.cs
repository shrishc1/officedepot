﻿using System;

namespace BusinessEntities.ModuleBE.Security {
    [Serializable]
    public class SCT_TemplateBE : BusinessEntities.BaseBe {
        public int? TemplateID { get; set; }
        public string TemplateName { get; set; }
        public int? ForRole { get; set; }
        //public string ScreenWithReadPermission { get; set; }
        //public string ScreenWithWritePermission { get; set; }
        public string SelectedScreen { get; set; }
        public string RoleName { get; set; }
        public int? TemplateScreenID { get; set; }
        public int? ScreenID { get; set; }
        public int? RoleId { get; set; }
        public int? UserId { get; set; }

        // For VendorTemplate
        public int? VendorTemplateID { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public string SelectedVendorIds { get; set; }

        public BusinessEntities.ModuleBE.AdminFunctions.MAS_VendorBE Vendor { get; set; }


    }
}

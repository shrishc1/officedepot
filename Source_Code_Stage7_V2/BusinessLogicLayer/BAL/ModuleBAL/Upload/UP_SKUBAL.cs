﻿using System;
using System.Collections.Generic;
using BusinessEntities.ModuleBE.Upload;
using DataAccessLayer.ModuleDAL.Upload;
using Utilities;

namespace BusinessLogicLayer.ModuleBAL.Upload
{
    public class UP_SKUBAL : BaseBAL
    {
        public List<UP_SKUBE> GetSKU(UP_SKUBE oUP_SKUBE)
        {
            List<UP_SKUBE> lstUP_SKUBE = new List<UP_SKUBE>();

            try
            {
                UP_SKUDAL oUP_SKUDAL = new UP_SKUDAL();

                lstUP_SKUBE = oUP_SKUDAL.GetSKU(oUP_SKUBE);
                oUP_SKUDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return lstUP_SKUBE;
        }


        public void AddSKUs(List<UP_SKUBE> pUP_SKUBEList) {
            try {
                UP_SKUDAL oUP_SKUDAL = new UP_SKUDAL();
                oUP_SKUDAL.AddSKUs(pUP_SKUBEList);
                pUP_SKUBEList = null;
            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
        }

        /// <summary>
        ///  Logs Errors Related to SKU Files in SKU Error Table
        /// </summary>
        /// <param name="up_SKUBEErrorList">void</param>
        public void AddSKUErrors(List<UP_DataImportErrorBE> up_SKUBEErrorList)
        {
            try
            {
                UP_SKUDAL oUP_SKUDAL = new UP_SKUDAL();
                oUP_SKUDAL.AddSKUErrors(up_SKUBEErrorList);
                up_SKUBEErrorList = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
        }
    }
}

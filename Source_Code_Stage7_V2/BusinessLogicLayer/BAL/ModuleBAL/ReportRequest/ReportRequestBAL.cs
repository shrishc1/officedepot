﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessEntities.ModuleBE.ReportRequest;
using DataAccessLayer.ModuleDAL.ReportRequest;
using Utilities;

namespace BusinessLogicLayer.ModuleBAL.ReportRequest {
    public class ReportRequestBAL {

        public int? addReportRequestBAL(ReportRequestBE oReportRequestBE) {
            int? intResult = 0;
            try {
                ReportRequestDAL oReportRequestDAL = new ReportRequestDAL();
                intResult = oReportRequestDAL.addReportRequestDAL(oReportRequestBE);
                oReportRequestDAL = null;
            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public int? UpdateReportStatusBAL(ReportRequestBE oReportRequestBE) {
            int? intResult = 0;
            try {
                ReportRequestDAL oReportRequestDAL = new ReportRequestDAL();
                intResult = oReportRequestDAL.UpdateReportStatusDAL(oReportRequestBE);
                oReportRequestDAL = null;
            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public int? DeleteReportRequestBAL(ReportRequestBE oReportRequestBE) {
            int? intResult = 0;
            try {
                ReportRequestDAL oReportRequestDAL = new ReportRequestDAL();
                intResult = oReportRequestDAL.DeleteReportRequestDAL(oReportRequestBE);
                oReportRequestDAL = null;
            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public List<ReportRequestBE> GetReportRequestBAL(ReportRequestBE oReportRequestBE) {
            ReportRequestBE ReportRequestBE = new ReportRequestBE();
            List<ReportRequestBE> oReportRequestBEList = new List<ReportRequestBE>();
            try {
                ReportRequestDAL oReportRequestDAL = new ReportRequestDAL();

                oReportRequestBEList = oReportRequestDAL.GetReportRequestDAL(oReportRequestBE);
                if (oReportRequestBEList.Count > 0)
                    ReportRequestBE = oReportRequestBEList[0];
                oReportRequestDAL = null;
            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oReportRequestBEList;
        }

        public List<ReportRequestBE> GetReportRequestByReportStatusBAL(ReportRequestBE oReportRequestBE) {
            ReportRequestBE ReportRequestBE = new ReportRequestBE();
            List<ReportRequestBE> oReportRequestBEList = new List<ReportRequestBE>();
            try {
                ReportRequestDAL oReportRequestDAL = new ReportRequestDAL();

                oReportRequestBEList = oReportRequestDAL.GetReportRequestByReportStatusDAL(oReportRequestBE);
                if (oReportRequestBEList.Count > 0)
                    ReportRequestBE = oReportRequestBEList[0];
                oReportRequestDAL = null;
            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oReportRequestBEList;
        }
        
        public List<ReportRequestBE> GetGeneratedReportCountBAL(ReportRequestBE oReportRequestBE) {
            ReportRequestBE ReportRequestBE = new ReportRequestBE();
            List<ReportRequestBE> oReportRequestBEList = new List<ReportRequestBE>();
            try {
                ReportRequestDAL oReportRequestDAL = new ReportRequestDAL();

                oReportRequestBEList = oReportRequestDAL.GetGeneratedReportCountDAL(oReportRequestBE);
                if (oReportRequestBEList.Count > 0)
                    ReportRequestBE = oReportRequestBEList[0];
                oReportRequestDAL = null;
            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oReportRequestBEList;
        }

        public List<int> GetOldReportRequestIDsBAL(ReportRequestBE oReportRequestBE) {
            var lstRequestIDs = new List<int>();
            try {
                var ReportRequestDAL = new ReportRequestDAL();
                lstRequestIDs = ReportRequestDAL.GetOldReportRequestIDsDAL(oReportRequestBE);
                ReportRequestDAL = null;
            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return lstRequestIDs;
        }
    }
}

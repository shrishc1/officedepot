﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Resources;

using Utilities;
using BusinessEntities.ModuleBE.Appointment.CountrySetting;
using DataAccessLayer.ModuleDAL.Appointment.CountrySetting;

namespace BusinessLogicLayer.ModuleBAL.Appointment.CountrySetting {
    public class APPCNT_DeliveryTypeBAL : BusinessLogicLayer.BaseBAL {
        public List<MASCNT_DeliveryTypeBE> GetDeliveryTypeDetailsBAL(MASCNT_DeliveryTypeBE oMASCNT_DeliveryTypeBE) {
            List<MASCNT_DeliveryTypeBE> oMASCNT_DeliveryTypeBEList = new List<MASCNT_DeliveryTypeBE>();

            try {
                APPCNT_DeliveryTypeDAL oAPPCNT_DeliveryTypeDAL = new APPCNT_DeliveryTypeDAL();
                oMASCNT_DeliveryTypeBEList = oAPPCNT_DeliveryTypeDAL.GetDeliveryTypeDetailsDAL(oMASCNT_DeliveryTypeBE);                
            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oMASCNT_DeliveryTypeBEList;
        }

        public MASCNT_DeliveryTypeBE GetDeliveryTypeDetailsByIdBAL(MASCNT_DeliveryTypeBE oMASCNT_DeliveryTypeBE) {
            MASCNT_DeliveryTypeBE oNewMASCNT_DeliveryTypeBE = new MASCNT_DeliveryTypeBE();
            List<MASCNT_DeliveryTypeBE> oMASCNT_DeliveryTypeBEList = new List<MASCNT_DeliveryTypeBE>();
            try {
                APPCNT_DeliveryTypeDAL oAPPCNT_DeliveryTypeDAL = new APPCNT_DeliveryTypeDAL();

                oMASCNT_DeliveryTypeBEList = oAPPCNT_DeliveryTypeDAL.GetDeliveryTypeDetailsDAL(oMASCNT_DeliveryTypeBE);
                oNewMASCNT_DeliveryTypeBE = oMASCNT_DeliveryTypeBEList[0];                
            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oNewMASCNT_DeliveryTypeBE;
        }

        public int? addEditDeliveryTypeDetailsBAL(MASCNT_DeliveryTypeBE oMASCNT_DeliveryTypeBE) {
            int? intResult = 0;
            try {
                APPCNT_DeliveryTypeDAL oAPPCNT_DeliveryTypeDAL = new APPCNT_DeliveryTypeDAL();
                intResult = oAPPCNT_DeliveryTypeDAL.addEditDeliveryTypeDetailsDAL(oMASCNT_DeliveryTypeBE);
            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }
    }

    
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Resources;

using Utilities;
using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using DataAccessLayer.ModuleDAL.Appointment.SiteSettings;

namespace BusinessLogicLayer.ModuleBAL.Appointment.SiteSettings {
    public class APPSIT_DoorOpenTimeSpecificBAL: BaseBAL {

        public List<MASSIT_DoorOpenTimeSpecificBE> GetDoorNoSetupSpecificDetailsBAL(MASSIT_DoorOpenTimeSpecificBE oMASSIT_DoorOpenTimeSpecificBE) {
            List<MASSIT_DoorOpenTimeSpecificBE> oMASSIT_DoorOpenTimeSpecificBEList = new List<MASSIT_DoorOpenTimeSpecificBE>();

            try {
                APPSIT_DoorOpenTimeSpecificDAL oAPPSIT_DoorOpenTimeSpecificDAL = new APPSIT_DoorOpenTimeSpecificDAL();

                oMASSIT_DoorOpenTimeSpecificBEList = oAPPSIT_DoorOpenTimeSpecificDAL.GetDoorNoAndTypeSpecificDAL(oMASSIT_DoorOpenTimeSpecificBE);

            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oMASSIT_DoorOpenTimeSpecificBEList;
        }

        public List<MASSIT_DoorOpenTimeSpecificBE> GetDoorConstraintsSpecificBAL(MASSIT_DoorOpenTimeSpecificBE oMASSIT_DoorOpenTimeSpecificBE) {
            List<MASSIT_DoorOpenTimeSpecificBE> oMASSIT_DoorOpenTimeSpecificBEList = new List<MASSIT_DoorOpenTimeSpecificBE>();

            try {
                APPSIT_DoorOpenTimeSpecificDAL oAPPSIT_DoorOpenTimeSpecificDAL = new APPSIT_DoorOpenTimeSpecificDAL();

                oMASSIT_DoorOpenTimeSpecificBEList = oAPPSIT_DoorOpenTimeSpecificDAL.GetDoorConstraintsSpecificDAL(oMASSIT_DoorOpenTimeSpecificBE);

            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oMASSIT_DoorOpenTimeSpecificBEList;
        }

        public int? AddEditDoorConstraintsSpecificBAL(MASSIT_DoorOpenTimeSpecificBE oMASSIT_DoorOpenTimeSpecificBE) {
            int? intResult = 0;
            try {
                APPSIT_DoorOpenTimeSpecificDAL oAPPSIT_DoorOpenTimeSpecificDAL = new APPSIT_DoorOpenTimeSpecificDAL();
                intResult = oAPPSIT_DoorOpenTimeSpecificDAL.AddEditDoorConstraintsSpecificDAL(oMASSIT_DoorOpenTimeSpecificBE);
            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }
        

    }
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Resources;

using Utilities;
using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using DataAccessLayer.ModuleDAL.Appointment.SiteSettings;

namespace BusinessLogicLayer.ModuleBAL.Appointment.SiteSettings {
    public class APPSIT_NonTimeDeliveryBAL : BaseBAL {

        public List<MASSIT_NonTimeDeliveryBE> GetNonTimeDeliveryDetailsBAL(MASSIT_NonTimeDeliveryBE oMASSIT_NonTimeDeliveryBE) {
            List<MASSIT_NonTimeDeliveryBE> oMASSIT_NonTimeDeliveryBEList = new List<MASSIT_NonTimeDeliveryBE>();

            try {
                APPSIT_NonTimeDeliveryDAL oMASSIT_NonTimeDeliveryDAL = new APPSIT_NonTimeDeliveryDAL();

                oMASSIT_NonTimeDeliveryBEList = oMASSIT_NonTimeDeliveryDAL.GetNonTimeDeliverysDAL(oMASSIT_NonTimeDeliveryBE);
            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oMASSIT_NonTimeDeliveryBEList;
        }

        public DataTable GetNonTimeDeliveryDetailsBAL(MASSIT_NonTimeDeliveryBE oMASSIT_NonTimeDeliveryBE, string Nothing = "Nothing") {
            DataTable dt = new DataTable();

            try {
                APPSIT_NonTimeDeliveryDAL oMASSIT_NonTimeDeliveryDAL = new APPSIT_NonTimeDeliveryDAL();

                dt = oMASSIT_NonTimeDeliveryDAL.GetNonTimeDeliverysDAL(oMASSIT_NonTimeDeliveryBE, "");
            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return dt;
        }

        public int? addEditNonTimeDeliveryDetailsBAL(MASSIT_NonTimeDeliveryBE oMASSIT_NonTimeDeliveryBE) {
            int? intResult = 0;
            try {
                APPSIT_NonTimeDeliveryDAL oMASSIT_NonTimeDeliveryDAL = new APPSIT_NonTimeDeliveryDAL();
                intResult = oMASSIT_NonTimeDeliveryDAL.addEditNonTimeDeliveryDetailsDAL(oMASSIT_NonTimeDeliveryBE);
            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }
    }
}

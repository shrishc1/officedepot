﻿using System;
using System.Collections.Generic;
using System.Data;

using Utilities;
using BusinessEntities.ModuleBE.Appointment.Booking;
using DataAccessLayer.ModuleDAL.Appointment.Booking;

namespace BusinessLogicLayer.ModuleBAL.Appointment.Booking {
    public class APPBOK_BookingBAL : BaseBAL {

        public List<APPBOK_BookingBE> GetGetVolumeProcessedBAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            List<APPBOK_BookingBE> oAPPBOK_BookingBEList = new List<APPBOK_BookingBE>();

            try
            {
                APPBOK_BookingDAL oMASBOK_BookingDAL = new APPBOK_BookingDAL();

                oAPPBOK_BookingBEList = oMASBOK_BookingDAL.GetGetVolumeProcessedDAL(oAPPBOK_BookingBE);

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oAPPBOK_BookingBEList;
        }

        public int? AddVendorWindowBookingBAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            int? intResult = 0;
            try
            {
                APPBOK_BookingDAL oMASBOK_BookingDAL = new APPBOK_BookingDAL();
                intResult = oMASBOK_BookingDAL.AddVendorWindowBookingDAL(oAPPBOK_BookingBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public List<APPBOK_BookingBE> GetBookingDetailsBAL(APPBOK_BookingBE oAPPBOK_BookingBE) {
            List<APPBOK_BookingBE> oAPPBOK_BookingBEList = new List<APPBOK_BookingBE>();

            try {
                APPBOK_BookingDAL oMASBOK_BookingDAL = new APPBOK_BookingDAL();


                oAPPBOK_BookingBEList = oMASBOK_BookingDAL.GetBookingDetailsDAL(oAPPBOK_BookingBE);

            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oAPPBOK_BookingBEList;
        }

        public List<APPBOK_BookingBE> GetDeliveryUnloadDetailsBAL(APPBOK_BookingBE oAPPBOK_BookingBE) {
            List<APPBOK_BookingBE> oAPPBOK_BookingBEList = new List<APPBOK_BookingBE>();

            try {
                APPBOK_BookingDAL oMASBOK_BookingDAL = new APPBOK_BookingDAL();

                oAPPBOK_BookingBEList = oMASBOK_BookingDAL.GetDeliveryUnloadDetailsDAL(oAPPBOK_BookingBE);

            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oAPPBOK_BookingBEList;
        }

        public int? DeleteBookingDetailBAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            int? intResult = 0;
            try
            {
                APPBOK_BookingDAL oAPPBOK_BookingDAL = new APPBOK_BookingDAL();
                intResult = oAPPBOK_BookingDAL.DeleteBookingDetailDAL(oAPPBOK_BookingBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public int? addEditBookingDetailsBAL(APPBOK_BookingBE oAPPBOK_BookingBE) {
            int? intResult = 0;
            try {
                APPBOK_BookingDAL oAPPBOK_BookingDAL = new APPBOK_BookingDAL();
                intResult = oAPPBOK_BookingDAL.addEditBookingDetailsDAL(oAPPBOK_BookingBE);
            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        /// <summary>
        /// User for Managing Delivery Unloaded First step
        /// </summary>
        /// <param name="oAPPBOK_BookingBE"></param>
        /// <returns></returns>
        public int? updateDeliveryUnloadedFirstStepBAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            int? intResult = 0;
            try
            {
                APPBOK_BookingDAL oAPPBOK_BookingDAL = new APPBOK_BookingDAL();
                intResult = oAPPBOK_BookingDAL.updateDeliveryUnloadedFirstStepDAL(oAPPBOK_BookingBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        /// <summary>
        /// User for Managing Delivery Unloaded Final step
        /// </summary>
        /// <param name="oAPPBOK_BookingBE"></param>
        /// <returns></returns>
        public int? updateDeliveryUnloadedFinalStepBAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            int? intResult = 0;
            try
            {
                APPBOK_BookingDAL oAPPBOK_BookingDAL = new APPBOK_BookingDAL();
                intResult = oAPPBOK_BookingDAL.updateDeliveryUnloadedFinalStepDAL(oAPPBOK_BookingBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public int? updateDeliveryUnloadedAcceptAllBAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            int? intResult = 0;
            try
            {
                APPBOK_BookingDAL oAPPBOK_BookingDAL = new APPBOK_BookingDAL();
                intResult = oAPPBOK_BookingDAL.updateDeliveryUnloadedAcceptAllDAL(oAPPBOK_BookingBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        /// <summary>
        /// Used for managing Delivery Refusal
        /// </summary>
        /// <param name="oAPPBOK_BookingBE"></param>
        /// <returns></returns>
        public int? addEditDeliveryRefusalBAL(APPBOK_BookingBE oAPPBOK_BookingBE) {
            int? intResult = 0;
            try {
                APPBOK_BookingDAL oAPPBOK_BookingDAL = new APPBOK_BookingDAL();
                intResult = oAPPBOK_BookingDAL.addEditDeliveryRefusalDAL(oAPPBOK_BookingBE);
            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        /// <summary>
        /// Used for managing unexpected delivery for Vendor
        /// </summary>
        /// <param name="oAPPBOK_BookingBE"></param>
        /// <returns></returns>
        public int? addEditUnexpectedDeliveryBAL(APPBOK_BookingBE oAPPBOK_BookingBE) {
            int? intResult = 0;
            try {
                APPBOK_BookingDAL oAPPBOK_BookingDAL = new APPBOK_BookingDAL();
                intResult = oAPPBOK_BookingDAL.addEditUnexpectedDeliveryDAL(oAPPBOK_BookingBE);
            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        /// <summary>
        /// Used for managing unexpected delivery for Carrier
        /// </summary>
        /// <param name="oAPPBOK_BookingBE"></param>
        /// <returns></returns>
        public int? addEditUnexpectedDeliveryCarrierBAL(APPBOK_BookingBE oAPPBOK_BookingBE) {
            int? intResult = 0;
            try {
                APPBOK_BookingDAL oAPPBOK_BookingDAL = new APPBOK_BookingDAL();
                intResult = oAPPBOK_BookingDAL.addEditUnexpectedDeliveryCarrierDAL(oAPPBOK_BookingBE);
            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public int? addEditQualityCheckBAL(APPBOK_BookingBE oAPPBOK_BookingBE) {
            int? intResult = 0;
            try {
                APPBOK_BookingDAL oAPPBOK_BookingDAL = new APPBOK_BookingDAL();
                intResult = oAPPBOK_BookingDAL.addEditQualityCheckDAL(oAPPBOK_BookingBE);
            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public List<APPBOK_BookingBE> GetBookingHistoryBAL(APPBOK_BookingBE oAPPBOK_BookingBE) {
            List<APPBOK_BookingBE> oAPPBOK_BookingBEList = new List<APPBOK_BookingBE>();

            try {
                APPBOK_BookingDAL oMASBOK_BookingDAL = new APPBOK_BookingDAL();

                oAPPBOK_BookingBEList = oMASBOK_BookingDAL.GetBookingHistoryDAL(oAPPBOK_BookingBE);

            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oAPPBOK_BookingBEList;
        }

        public DataTable GetVolumeDetailsDAL(APPBOK_BookingBE oAPPBOK_BookingBE) {
            DataTable oAPPBOK_BookingBEList = new DataTable();

            try {
                APPBOK_BookingDAL oMASBOK_BookingDAL = new APPBOK_BookingDAL();

                oAPPBOK_BookingBEList = oMASBOK_BookingDAL.GetVolumeDetailsDAL(oAPPBOK_BookingBE);

            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oAPPBOK_BookingBEList;
        }
        public DataTable GetAllBookingDetailBAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            DataTable oAPPBOK_BookingBEList = new DataTable();

            try
            {
                APPBOK_BookingDAL oMASBOK_BookingDAL = new APPBOK_BookingDAL();

                oAPPBOK_BookingBEList = oMASBOK_BookingDAL.GetAllBookingDetailDAL(oAPPBOK_BookingBE);

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oAPPBOK_BookingBEList;
        }

        public List<APPBOK_BookingBE> GetVendorBookingFixedSlotDetailsBAL(APPBOK_BookingBE oAPPBOK_BookingBE) {
            List<APPBOK_BookingBE> oAPPBOK_BookingBEList = new List<APPBOK_BookingBE>();

            try {
                APPBOK_BookingDAL oMASBOK_BookingDAL = new APPBOK_BookingDAL();
                oAPPBOK_BookingBEList = oMASBOK_BookingDAL.GetVendorBookingFixedSlotDetailsDAL(oAPPBOK_BookingBE);
            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oAPPBOK_BookingBEList;
        }
        public List<APPBOK_BookingBE> GetVendorBookingWindowDetailsBAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            List<APPBOK_BookingBE> oAPPBOK_BookingBEList = new List<APPBOK_BookingBE>();

            try
            {
                APPBOK_BookingDAL oMASBOK_BookingDAL = new APPBOK_BookingDAL();
                oAPPBOK_BookingBEList = oMASBOK_BookingDAL.GetVendorBookingWindowDetailsDAL(oAPPBOK_BookingBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oAPPBOK_BookingBEList;
        }

        public List<APPBOK_BookingBE> GetVendorBookingFixedSlotDoorBAL(APPBOK_BookingBE oAPPBOK_BookingBE) {
            List<APPBOK_BookingBE> oAPPBOK_BookingBEList = new List<APPBOK_BookingBE>();

            try {
                APPBOK_BookingDAL oMASBOK_BookingDAL = new APPBOK_BookingDAL();
                oAPPBOK_BookingBEList = oMASBOK_BookingDAL.GetVendorBookingFixedSlotDoorDAL(oAPPBOK_BookingBE);
            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oAPPBOK_BookingBEList;
        }

        public int? AddVendorBookingBAL(APPBOK_BookingBE oAPPBOK_BookingBE) {
            int? intResult = 0;
            try {
                APPBOK_BookingDAL oMASBOK_BookingDAL = new APPBOK_BookingDAL();
                intResult = oMASBOK_BookingDAL.AddVendorBookingDAL(oAPPBOK_BookingBE);
            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public int? AddCarrierBookingBAL(APPBOK_BookingBE oAPPBOK_BookingBE) {
            int? intResult = 0;
            try {
                APPBOK_BookingDAL oMASBOK_BookingDAL = new APPBOK_BookingDAL();
                intResult = oMASBOK_BookingDAL.AddCarrierBookingDAL(oAPPBOK_BookingBE);
            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public int? AddCarrierWindowBookingBAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            int? intResult = 0;
            try
            {
                APPBOK_BookingDAL oMASBOK_BookingDAL = new APPBOK_BookingDAL();
                intResult = oMASBOK_BookingDAL.AddCarrierWindowBookingDAL(oAPPBOK_BookingBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }


        public int? AddCarrierBookingDetailsBAL(APPBOK_BookingBE oAPPBOK_BookingBE) {
            int? intResult = 0;
            try {
                APPBOK_BookingDAL oMASBOK_BookingDAL = new APPBOK_BookingDAL();
                intResult = oMASBOK_BookingDAL.AddCarrierBookingDetailsDAL(oAPPBOK_BookingBE);
            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public List<APPBOK_BookingBE> GetBookingDetailsByIDBAL(APPBOK_BookingBE oAPPBOK_BookingBE) {
            List<APPBOK_BookingBE> oAPPBOK_BookingBEList = new List<APPBOK_BookingBE>();

            try {
                APPBOK_BookingDAL oMASBOK_BookingDAL = new APPBOK_BookingDAL();
                oAPPBOK_BookingBEList = oMASBOK_BookingDAL.GetBookingDetailsByIDDAL(oAPPBOK_BookingBE);
            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oAPPBOK_BookingBEList;
        }

        public List<APPBOK_BookingBE> GetAllBookingFixedSlotDetailsBAL(APPBOK_BookingBE oAPPBOK_BookingBE) {
            List<APPBOK_BookingBE> oAPPBOK_BookingBEList = new List<APPBOK_BookingBE>();

            try {
                APPBOK_BookingDAL oMASBOK_BookingDAL = new APPBOK_BookingDAL();
                oAPPBOK_BookingBEList = oMASBOK_BookingDAL.GetAllBookingFixedSlotDoorDAL(oAPPBOK_BookingBE);
            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oAPPBOK_BookingBEList;
        }


        public List<APPBOK_BookingBE> GetBookedVolumeBAL(APPBOK_BookingBE oAPPBOK_BookingBE) {
            List<APPBOK_BookingBE> oAPPBOK_BookingBEList = new List<APPBOK_BookingBE>();

            try {
                APPBOK_BookingDAL oMASBOK_BookingDAL = new APPBOK_BookingDAL();
                oAPPBOK_BookingBEList = oMASBOK_BookingDAL.GetBookedVolumeDAL(oAPPBOK_BookingBE);
            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oAPPBOK_BookingBEList;
        }

        public List<APPBOK_BookingBE> GetBookingVolumeDeatilsBAL(APPBOK_BookingBE oAPPBOK_BookingBE) {
            List<APPBOK_BookingBE> oAPPBOK_BookingBEList = new List<APPBOK_BookingBE>();

            try {
                APPBOK_BookingDAL oMASBOK_BookingDAL = new APPBOK_BookingDAL();
                oAPPBOK_BookingBEList = oMASBOK_BookingDAL.GetBookingVolumeDeatilsDAL(oAPPBOK_BookingBE);
            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oAPPBOK_BookingBEList;
        }

        public DataTable GetSchedulingPrintOutBAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {

            DataTable oAPPBOK_BookingDatatable = new DataTable();
            try
            {
                APPBOK_BookingDAL oMASBOK_BookingDAL = new APPBOK_BookingDAL();

                oAPPBOK_BookingDatatable = oMASBOK_BookingDAL.GetSchedulingPrintOutDAL(oAPPBOK_BookingBE);

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oAPPBOK_BookingDatatable;
        }

        public List<APPBOK_BookingBE> GetNonTimeBookingBAL(APPBOK_BookingBE oAPPBOK_BookingBE) {
            List<APPBOK_BookingBE> oAPPBOK_BookingBEList = new List<APPBOK_BookingBE>();

            try {
                APPBOK_BookingDAL oMASBOK_BookingDAL = new APPBOK_BookingDAL();
                oAPPBOK_BookingBEList = oMASBOK_BookingDAL.GetNonTimeBookingDAL(oAPPBOK_BookingBE);
            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oAPPBOK_BookingBEList;
        }


        public List<APPBOK_BookingBE> GetVendorBookingDetailsBAL(APPBOK_BookingBE oAPPBOK_BookingBE) {
            List<APPBOK_BookingBE> oAPPBOK_BookingBEList = new List<APPBOK_BookingBE>();

            try {
                APPBOK_BookingDAL oMASBOK_BookingDAL = new APPBOK_BookingDAL();

                oAPPBOK_BookingBEList = oMASBOK_BookingDAL.GetVendorBookingDetailsDAL(oAPPBOK_BookingBE);

            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oAPPBOK_BookingBEList;
        }

        public List<APPBOK_BookingBE> GetCarrierBookingDetailsBAL(APPBOK_BookingBE oAPPBOK_BookingBE) {
            List<APPBOK_BookingBE> oAPPBOK_BookingBEList = new List<APPBOK_BookingBE>();

            try {
                APPBOK_BookingDAL oMASBOK_BookingDAL = new APPBOK_BookingDAL();

                oAPPBOK_BookingBEList = oMASBOK_BookingDAL.GetCarrierBookingDetailsDAL(oAPPBOK_BookingBE);

            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oAPPBOK_BookingBEList;
        }

        public List<APPBOK_BookingBE> GetCarrierVolumeDeatilsBAL(APPBOK_BookingBE oAPPBOK_BookingBE) {
            List<APPBOK_BookingBE> oAPPBOK_BookingBEList = new List<APPBOK_BookingBE>();

            try {
                APPBOK_BookingDAL oMASBOK_BookingDAL = new APPBOK_BookingDAL();

                oAPPBOK_BookingBEList = oMASBOK_BookingDAL.GetCarrierVolumeDeatilsDAL(oAPPBOK_BookingBE);

            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oAPPBOK_BookingBEList;
        }

        public int? CheckBookingBAL(APPBOK_BookingBE oAPPBOK_BookingBE) {
            int? intResult = 0;
            try {
                APPBOK_BookingDAL oAPPBOK_BookingDAL = new APPBOK_BookingDAL();
                intResult = oAPPBOK_BookingDAL.CheckBookingDAL(oAPPBOK_BookingBE);
            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public int? CheckVendorBookingBAL(APPBOK_BookingBE oAPPBOK_BookingBE) {
            int? intResult = 0;
            try {
                APPBOK_BookingDAL oAPPBOK_BookingDAL = new APPBOK_BookingDAL();
                intResult = oAPPBOK_BookingDAL.CheckVendorBookingDAL(oAPPBOK_BookingBE);
            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        // Sprint 1 - Point 1 - Start - Checking PO is existing or not.
        public List<APPBOK_BookingBE> IsExistingPO(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            List<APPBOK_BookingBE> lstAPPBOK_BookingBE = null;
            try
            {
                APPBOK_BookingDAL oAPPBOK_BookingDAL = new APPBOK_BookingDAL();
                lstAPPBOK_BookingBE = oAPPBOK_BookingDAL.IsExistingPO(oAPPBOK_BookingBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return lstAPPBOK_BookingBE;
        }
        // Sprint 1 - Point 1 - End

        // Sprint 1 - Point 9 - Begin

        public List<APPBOK_BookingBE> GetEmailIdsForStockPlannerBAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            List<APPBOK_BookingBE> lstAPPBOK_BookingBE = null;
            try
            {
                APPBOK_BookingDAL oAPPBOK_BookingDAL = new APPBOK_BookingDAL();
                lstAPPBOK_BookingBE = oAPPBOK_BookingDAL.GetEmailIdsForStockPlannerDAL(oAPPBOK_BookingBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return lstAPPBOK_BookingBE;
        }

        public List<APPBOK_BookingBE> GetEmailDataForRefusedDeliveriesBAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            List<APPBOK_BookingBE> lstAPPBOK_BookingBE = null;
            try
            {
                APPBOK_BookingDAL oAPPBOK_BookingDAL = new APPBOK_BookingDAL();
                lstAPPBOK_BookingBE = oAPPBOK_BookingDAL.GetEmailDataForRefusedDeliveriesDAL(oAPPBOK_BookingBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return lstAPPBOK_BookingBE;
        }
        // Sprint 1 - Point 9 - End

        public int? UpdateBookingStatusByIdBAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            int? intResult = 0;
            try
            {
                APPBOK_BookingDAL oMASBOK_BookingDAL = new APPBOK_BookingDAL();
                intResult = oMASBOK_BookingDAL.UpdateBookingStatusByIdDAL(oAPPBOK_BookingBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public List<APPBOK_BookingBE> GetNoShowDeliveryLetterBAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            List<APPBOK_BookingBE> oAPPBOK_BookingBEList = new List<APPBOK_BookingBE>();
            try
            {
                APPBOK_BookingDAL oMASBOK_BookingDAL = new APPBOK_BookingDAL();
                oAPPBOK_BookingBEList = oMASBOK_BookingDAL.GetNoShowDeliveryLetterDAL(oAPPBOK_BookingBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oAPPBOK_BookingBEList;
        }      

        public List<APPBOK_BookingBE> GetMultiVendorBookingDataBAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            List<APPBOK_BookingBE> oAPPBOK_BookingBEList = new List<APPBOK_BookingBE>();
            try
            {
                APPBOK_BookingDAL oMASBOK_BookingDAL = new APPBOK_BookingDAL();
                oAPPBOK_BookingBEList = oMASBOK_BookingDAL.GetMultiVendorBookingDataDAL(oAPPBOK_BookingBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oAPPBOK_BookingBEList;
        }

        public DataTable GetBookingInqueryBAL(APPBOK_BookingBE oAPPBOK_BookingBE) {
            DataTable oAPPBOK_BookingBEList = new DataTable();

            try {
                APPBOK_BookingDAL oMASBOK_BookingDAL = new APPBOK_BookingDAL();

                oAPPBOK_BookingBEList = oMASBOK_BookingDAL.GetBookingInqueryDAL(oAPPBOK_BookingBE);

            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oAPPBOK_BookingBEList;
        }

        public List<APPBOK_BookingBE> GetWindowVolumeDataBAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            List<APPBOK_BookingBE> oAPPBOK_BookingBEList = new List<APPBOK_BookingBE>();
            try
            {
                APPBOK_BookingDAL oMASBOK_BookingDAL = new APPBOK_BookingDAL();
                oAPPBOK_BookingBEList = oMASBOK_BookingDAL.GetWindowVolumeDataDAL(oAPPBOK_BookingBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oAPPBOK_BookingBEList;
        }

        public DataTable GetProvisionalBookingBAL(APPBOK_BookingBE oAPPBOK_BookingBE) {
            DataTable oAPPBOK_BookingBEList = new DataTable();

            try {
                APPBOK_BookingDAL oMASBOK_BookingDAL = new APPBOK_BookingDAL();

                oAPPBOK_BookingBEList = oMASBOK_BookingDAL.GetProvisionalBookingDAL(oAPPBOK_BookingBE);

            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oAPPBOK_BookingBEList;
        }
    }
}

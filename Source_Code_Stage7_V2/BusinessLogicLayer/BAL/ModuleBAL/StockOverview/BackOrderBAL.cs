﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Utilities;
using DataAccessLayer.ModuleDAL.StockOverview;
using BusinessEntities.ModuleBE.StockOverview;
using BusinessEntities.ModuleBE.Languages;
using BusinessEntities.ModuleBE.Languages.Languages;


namespace BusinessLogicLayer.ModuleBAL.StockOverview
{
    public class BackOrderBAL
    {

        public List<BackOrderBE> GetVendorComm1BAL(BackOrderBE oBackOrderBE)
        {
            List<BackOrderBE> lstBackOrderDAL = new List<BackOrderBE>();

            try
            {
                BackOrderDAL oBackOrderDAL = new BackOrderDAL();
                lstBackOrderDAL = oBackOrderDAL.GetVendorComm1DAL(oBackOrderBE);
                oBackOrderDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return lstBackOrderDAL;
        }

        public bool CheckSkuStatusByVendorBAL(BackOrderBE BackOrderBE)
        {
            bool IsAllVendorProcessed = true;
            try
            {
                BackOrderDAL oBackOrderDAL = new BackOrderDAL();
                IsAllVendorProcessed = oBackOrderDAL.CheckSkuStatusByVendorDAL(BackOrderBE);
                oBackOrderDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return IsAllVendorProcessed;
        }

        public List<BackOrderBE> GetVendorContactDetailsBAL(BackOrderBE oBackOrderBE)
        {
            List<BackOrderBE> lstBackOrderDAL = new List<BackOrderBE>();

            try
            {
                BackOrderDAL oBackOrderDAL = new BackOrderDAL();
                lstBackOrderDAL = oBackOrderDAL.GetVendorContactDetailsDAL(oBackOrderBE);
                oBackOrderDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return lstBackOrderDAL;
        }

        public List<BackOrderBE> GetVendorInitialEmailListBAL(BackOrderBE oBackOrderBE)
        {
            List<BackOrderBE> lstBackOrderDAL = new List<BackOrderBE>();

            try
            {
                BackOrderDAL oBackOrderDAL = new BackOrderDAL();
                lstBackOrderDAL = oBackOrderDAL.GetVendorInitialEmailListDAL(oBackOrderBE);
                oBackOrderDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return lstBackOrderDAL;
        }

        public List<MAS_LanguageBE> GetLanguages()
        {
            BackOrderDAL oBackOrderDAL = new BackOrderDAL();
            return oBackOrderDAL.GetLanguages();
        }

        public int? AddCommunicationDetailBAL(BackOrderMailBE oBackOrderMailBE)
        {
            int? intResult = 0;
            try
            {
                BackOrderDAL oBackOrderDAL = new BackOrderDAL();
                intResult = oBackOrderDAL.AddCommunicationDetailDAL(oBackOrderMailBE);
                oBackOrderDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public int? UpdateBOIDBAL(BackOrderBE oBackOrderBE)
        {
            int? intResult = 0;
            try
            {
                BackOrderDAL oBackOrderDAL = new BackOrderDAL();
                intResult = oBackOrderDAL.UpdateBOIDDAL(oBackOrderBE);
                oBackOrderDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }
        public int? UpdateVendorIDBAL(BackOrderBE oBackOrderBE)
        {
            int? intResult = 0;
            try
            {
                BackOrderDAL oBackOrderDAL = new BackOrderDAL();
                intResult = oBackOrderDAL.UpdateVendorIDDAL(oBackOrderBE);
                oBackOrderDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }
        public List<BackOrderBE> GetBOinitialCommDetailsBAL(BackOrderBE oBackOrderBE)
        {
            List<BackOrderBE> lstBackOrderDAL = new List<BackOrderBE>();

            try
            {
                BackOrderDAL oBackOrderDAL = new BackOrderDAL();
                lstBackOrderDAL = oBackOrderDAL.GetBOinitialCommDetailsDAL(oBackOrderBE);
                oBackOrderDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return lstBackOrderDAL;
        }

        public List<BackOrderBE> GetMediatorCommDetailsBAL(BackOrderBE oBackOrderBE)
        {
            List<BackOrderBE> lstBackOrderDAL = new List<BackOrderBE>();

            try
            {
                BackOrderDAL oBackOrderDAL = new BackOrderDAL();
                lstBackOrderDAL = oBackOrderDAL.GetMediatorCommDetailsDAL(oBackOrderBE);
                oBackOrderDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return lstBackOrderDAL;
        }
    }
}

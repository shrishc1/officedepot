﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BusinessEntities.ModuleBE.StockOverview;
using DataAccessLayer.ModuleDAL.StockOverview;
using Utilities;

namespace BusinessLogicLayer.ModuleBAL.StockOverview
{
  public class MAS_CurrencyConversionBAL
    {

      public List<MAS_CurrencyConversionBE> GetCurrenyConversionBAL(MAS_CurrencyConversionBE SYS_CurrencyConversionBE)
      {
          List<MAS_CurrencyConversionBE> oSYS_CurrencyConversionBEList = new List<MAS_CurrencyConversionBE>();

          try
          {
              MAS_CurrencyConversionDAL oSYS_CurrencyConversionDAL = new MAS_CurrencyConversionDAL();

              oSYS_CurrencyConversionBEList = oSYS_CurrencyConversionDAL.GetCurrenyConversionDAL(SYS_CurrencyConversionBE);

              oSYS_CurrencyConversionDAL = null;
          }
          catch (Exception ex)
          {
              LogUtility.SaveErrorLogEntry(ex);
          }
          finally { }
          return oSYS_CurrencyConversionBEList;
      }


      public int? addEditCurrencyConversionBAL(MAS_CurrencyConversionBE SYS_CurrencyConversionBE)
      {
          int? intResult = 0;
          try
          {
              MAS_CurrencyConversionDAL oSYS_CurrencyConversionDAL = new MAS_CurrencyConversionDAL();
              intResult = oSYS_CurrencyConversionDAL.addEditCurrencyConversionDAL(SYS_CurrencyConversionBE);
              oSYS_CurrencyConversionDAL = null;
          }
          catch (Exception ex)
          {
              LogUtility.SaveErrorLogEntry(ex);
          }
          finally { }
          return intResult;
      }

    }
}

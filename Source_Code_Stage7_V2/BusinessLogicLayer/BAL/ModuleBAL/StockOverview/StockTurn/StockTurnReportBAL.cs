﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessEntities.ModuleBE.StockOverview.StockTurn;
using System.Data;
using Utilities;
using DataAccessLayer.ModuleDAL.StockOverview.StockTurn;

namespace BusinessLogicLayer.ModuleBAL.StockOverview.StockTurn
{
   public class StockTurnReportBAL
    {
      // StockTurnReportBE oStockOverviewReportBE = new StockTurnReportBE();

       public DataSet getStockTurnODViewReportBAL(StockTurnReportBE oStockTurnReportBE)
        {
            DataSet Result = null;
            try
            {
                StockTurnReportDAL oStockTurnReportDAL = new StockTurnReportDAL();
                Result = oStockTurnReportDAL.GetStockTurnReportDAL(oStockTurnReportBE);
                oStockTurnReportDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return Result;

        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Resources;

using Utilities;

using BusinessEntities.ModuleBE.OnTimeInFull.CountrySetting;
using DataAccessLayer.ModuleDAL.OnTimeInFull.CountrySetting;


namespace BusinessLogicLayer.ModuleBAL.OnTimeInFull.CountrySetting {
    public class CntOTIF_FutureDateSetupBAL : BaseBAL {
        public List<CntOTIF_FutureDateSetupBE> GetCntOTIF_FutureDateSetupDetailsBAL(CntOTIF_FutureDateSetupBE oSiteOTIF_LeadTimeRuleBE) {
            List<CntOTIF_FutureDateSetupBE> oCntOTIF_FutureDateSetupBEList = new List<CntOTIF_FutureDateSetupBE>();

            try {
                CntOTIF_FutureDateSetupDAL oCntOTIF_FutureDateSetupDAL = new CntOTIF_FutureDateSetupDAL();

                oCntOTIF_FutureDateSetupBEList = oCntOTIF_FutureDateSetupDAL.GetCntOTIF_FutureDateSetupDetailsDAL(oSiteOTIF_LeadTimeRuleBE);
                oCntOTIF_FutureDateSetupDAL = null;
            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oCntOTIF_FutureDateSetupBEList;
        }

        public int? addEditCntOTIF_FutureDateSetupDetailsBAL(CntOTIF_FutureDateSetupBE oCntOTIF_FutureDateSetupBE) {
            int? intResult = 0;
            try {
                CntOTIF_FutureDateSetupDAL oCntOTIF_FutureDateSetupDAL = new CntOTIF_FutureDateSetupDAL();
                intResult = oCntOTIF_FutureDateSetupDAL.addEditCntOTIF_FutureDateSetupDetailsDAL(oCntOTIF_FutureDateSetupBE);
                oCntOTIF_FutureDateSetupDAL = null;
            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }
    
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessEntities.ModuleBE.OnTimeInFull;
using Utilities;
using DataAccessLayer.ModuleDAL.OnTimeInFull;

namespace BusinessLogicLayer.ModuleBAL.OnTimeInFull
{
    public class OTIFRulesSettingBAL
    {
        public int? AddOTIFRulesBAL(OTIFRulesSettingBE oOTIFRulesSettingBE)
        {
            int? result=null;
            try
            {
                OTIFRulesSettingDAL oOTIFRulesSettingDAL = new OTIFRulesSettingDAL();

                result=oOTIFRulesSettingDAL.AddOTIFRulesDAL(oOTIFRulesSettingBE);
                oOTIFRulesSettingDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return result;
        }

        public OTIFRulesSettingBE GetOTIFRules(OTIFRulesSettingBE oOTIFRulesSettingBE)
        {
            OTIFRulesSettingBE oNewOTIFRulesSettingBE = new OTIFRulesSettingBE();
            try
            {
                OTIFRulesSettingDAL oOTIFRulesSettingDAL = new OTIFRulesSettingDAL();

                oNewOTIFRulesSettingBE = oOTIFRulesSettingDAL.GetOTIFRules(oOTIFRulesSettingBE);
                oOTIFRulesSettingDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oNewOTIFRulesSettingBE;
        }
    }
}

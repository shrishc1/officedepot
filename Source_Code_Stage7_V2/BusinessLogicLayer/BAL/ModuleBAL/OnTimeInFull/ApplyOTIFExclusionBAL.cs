﻿using System;
using System.Data;
using System.Data.SqlClient;
using BusinessEntities.ModuleBE.OnTimeInFull;
using Utilities;
using System.Collections.Generic;
using System.Text;
using DataAccessLayer.ModuleDAL.OnTimeInFull;

namespace BusinessLogicLayer.ModuleBAL.OnTimeInFull
{
    public class ApplyOTIFExclusionBAL : BaseBAL
    {
        public int? addEditUP_ApplyOTIFExclusionBAL(ApplyOTIFExclusionBE applyOTIFExclusionBE)
        {
            int? intResult = 0;
            try
            {
                ApplyOTIFExclusionDAL applyOTIFExclusionDAL = new ApplyOTIFExclusionDAL();
                intResult = applyOTIFExclusionDAL.addEditUP_ApplyOTIFExclusionDAL(applyOTIFExclusionBE);
                applyOTIFExclusionDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public List<ApplyOTIFExclusionBE> GetApplySKUExclusionBAL(ApplyOTIFExclusionBE oApplyOTIFExclusionBE)
        {
            List<ApplyOTIFExclusionBE> oApplyOTIFExclusionBEList = new List<ApplyOTIFExclusionBE>();
            try
            {
                ApplyOTIFExclusionDAL applyOTIFExclusionDAL = new ApplyOTIFExclusionDAL();

                oApplyOTIFExclusionBEList = applyOTIFExclusionDAL.GetApplySKUExclusionDAL(oApplyOTIFExclusionBE);
                applyOTIFExclusionDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oApplyOTIFExclusionBEList;
        }

        public List<ApplyOTIFExclusionBE> GetSelectedApplySKUExclusionBAL(ApplyOTIFExclusionBE oOTIF_LeadTimeSKUBE)
        {
            List<ApplyOTIFExclusionBE> oApplyOTIFExclusionBEList = new List<ApplyOTIFExclusionBE>();
            try
            {
                ApplyOTIFExclusionDAL applyOTIFExclusionDAL = new ApplyOTIFExclusionDAL();

                oApplyOTIFExclusionBEList = applyOTIFExclusionDAL.GetSelectedApplySKUExclusionDAL(oOTIF_LeadTimeSKUBE);
                applyOTIFExclusionDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oApplyOTIFExclusionBEList;
        }

        public List<ApplyOTIFExclusionBE> GetSelectedApplySKUCodesBAL(ApplyOTIFExclusionBE oApplyOTIFExclusionBE)
        {
            List<ApplyOTIFExclusionBE> oApplyOTIFExclusionBEList = new List<ApplyOTIFExclusionBE>();
            try
            {
                ApplyOTIFExclusionDAL applyOTIFExclusionDAL = new ApplyOTIFExclusionDAL();

                oApplyOTIFExclusionBEList = applyOTIFExclusionDAL.GetSelectedApplySKUCodesDAL(oApplyOTIFExclusionBE);
                applyOTIFExclusionDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oApplyOTIFExclusionBEList;
        }

        public bool CheckApplyExclusionExistanceBAL(ApplyOTIFExclusionBE oApplyOTIFExclusionBE)
        {
            bool blnStatus = false;
            try
            {
                ApplyOTIFExclusionDAL applyOTIFExclusionDAL = new ApplyOTIFExclusionDAL();
                blnStatus = applyOTIFExclusionDAL.CheckApplyExclusionExistanceDAL(oApplyOTIFExclusionBE);
                applyOTIFExclusionDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return blnStatus;
        }
    }
}

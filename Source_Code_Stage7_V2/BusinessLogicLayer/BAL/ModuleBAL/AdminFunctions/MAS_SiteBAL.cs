﻿using System;
using System.Collections.Generic;
using System.Data;
using BusinessEntities.ModuleBE.AdminFunctions;
using DataAccessLayer.ModuleDAL.AdminFunctions;
using Utilities;

namespace BusinessLogicLayer.ModuleBAL.AdminFunctions
{
    public class MAS_SiteBAL : BusinessLogicLayer.BaseBAL
    {

        public string GetSiteTimeSlotWindowFlagBAL(string SiteID)
        {
            string ReturnSiteTimeSlotWindowFlag = string.Empty;
            try
            {
                MAS_SiteDAL oMAS_SiteDAL = new MAS_SiteDAL();
                ReturnSiteTimeSlotWindowFlag = oMAS_SiteDAL.GetSiteTimeSlotWindowFlagDAL(SiteID);
                oMAS_SiteDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return ReturnSiteTimeSlotWindowFlag;
        }

        public List<MAS_SiteBE> GetSiteBAL(MAS_SiteBE oMAS_SiteBE)
        {
            List<MAS_SiteBE> oMAS_SiteBEList = new List<MAS_SiteBE>();
            try
            {
                MAS_SiteDAL oMAS_SiteDAL = new MAS_SiteDAL();
                oMAS_SiteBEList = oMAS_SiteDAL.GetSiteDAL(oMAS_SiteBE);
                oMAS_SiteDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oMAS_SiteBEList;
        }
        public DataSet GetSiteForCountryBAL(MAS_SiteBE oMAS_SiteBE)
        {
            DataSet ds = null;
            try
            {
                MAS_SiteDAL oMAS_SiteDAL = new MAS_SiteDAL();
                ds = oMAS_SiteDAL.GetSiteForCountryDAL(oMAS_SiteBE);
                oMAS_SiteDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return ds;
        }
        public List<MAS_SiteBE> GetSelectedSiteBAL(MAS_SiteBE oMAS_SiteBE)
        {
            List<MAS_SiteBE> oMAS_SiteBEList = new List<MAS_SiteBE>();
            try
            {
                MAS_SiteDAL oMAS_SiteDAL = new MAS_SiteDAL();
                oMAS_SiteBEList = oMAS_SiteDAL.GetSelectedSiteDAL(oMAS_SiteBE);
                oMAS_SiteDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oMAS_SiteBEList;
        }


        public List<MAS_SiteBE> GetSiteMisSettingBAL(MAS_SiteBE oMAS_SiteBE)
        {
            List<MAS_SiteBE> oMAS_SiteBEList = new List<MAS_SiteBE>();
            try
            {
                MAS_SiteDAL oMAS_SiteDAL = new MAS_SiteDAL();
                oMAS_SiteBEList = oMAS_SiteDAL.GetSiteMisSettingDAL(oMAS_SiteBE);
                oMAS_SiteDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oMAS_SiteBEList;
        }

        public List<MAS_SiteBE> GetSiteAddressForSiteIdBAL(MAS_SiteBE oMAS_SiteBE)
        {
            List<MAS_SiteBE> oMAS_SiteBEList = new List<MAS_SiteBE>();
            try
            {
                MAS_SiteDAL oMAS_SiteDAL = new MAS_SiteDAL();
                oMAS_SiteBEList = oMAS_SiteDAL.GetSiteAddressForSiteIdDAL(oMAS_SiteBE);
                oMAS_SiteDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oMAS_SiteBEList;
        }

        public int? addEditSiteSettingsBAL(MAS_SiteBE oMAS_SiteBE)
        {
            int? intResult = 0;
            try
            {
                MAS_SiteDAL oMAS_SiteDAL = new MAS_SiteDAL();
                intResult = oMAS_SiteDAL.addEditSiteSettingsDAL(oMAS_SiteBE);
                oMAS_SiteDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public int? DeleteSiteSettingsBAL(MAS_SiteBE oMAS_SiteBE)
        {
            int? intResult = 0;
            try
            {
                MAS_SiteDAL oMAS_SiteDAL = new MAS_SiteDAL();
                intResult = oMAS_SiteDAL.DeleteSiteSettingsDAL(oMAS_SiteBE);
                oMAS_SiteDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public DataTable GetCountrySite(MAS_SiteBE oMAS_SiteBE)
        {
            DataTable dt = null;

            try
            {
                MAS_SiteDAL oMAS_SiteDAL = new MAS_SiteDAL();
                dt = oMAS_SiteDAL.GetCountrySite(oMAS_SiteBE);
                oMAS_SiteDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return dt;
        }

        public List<MAS_SiteBE> GetSiteByIDBAL(MAS_SiteBE oMAS_SiteBE) {
            List<MAS_SiteBE> oMAS_SiteBEList = new List<MAS_SiteBE>();
            try {
                MAS_SiteDAL oMAS_SiteDAL = new MAS_SiteDAL();
                oMAS_SiteBEList = oMAS_SiteDAL.GetSiteByIDDAL(oMAS_SiteBE);
                oMAS_SiteDAL = null;
            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oMAS_SiteBEList;
        }

        public List<MAS_SiteBE> GetAllSitesBasedCountryBAL(MAS_SiteBE mas_SiteBE)
        {
            List<MAS_SiteBE> oMAS_SiteBEList = new List<MAS_SiteBE>();
            try
            {
                MAS_SiteDAL oMAS_SiteDAL = new MAS_SiteDAL();
                oMAS_SiteBEList = oMAS_SiteDAL.GetAllSitesBasedCountryDAL(mas_SiteBE);
                oMAS_SiteDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oMAS_SiteBEList;
        }

        public MAS_SiteBE GetSingleSiteSettingBAL(MAS_SiteBE oMAS_SiteBE)
        {
            MAS_SiteDAL oMAS_SiteDAL = null;
            MAS_SiteBE mas_SiteBE = null;
            try
            {
                oMAS_SiteDAL = new MAS_SiteDAL();
                mas_SiteBE = oMAS_SiteDAL.GetSingleSiteSettingDAL(oMAS_SiteBE);
                oMAS_SiteDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return mas_SiteBE;
        }
    }
}

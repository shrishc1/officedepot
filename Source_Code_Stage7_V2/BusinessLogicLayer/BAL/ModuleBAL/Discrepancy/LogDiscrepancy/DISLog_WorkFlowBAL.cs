﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Utilities;
using BusinessEntities.ModuleBE.Discrepancy.LogDiscrepancy;
using DataAccessLayer.ModuleDAL.Discrepancy.LogDiscrepancy;
using System.Data;

namespace BusinessLogicLayer.ModuleBAL.Discrepancy.LogDiscrepancy {
    public class DISLog_WorkFlowBAL : BaseBAL {
        public int? addWorkFlowActionsBAL(DISLog_WorkFlowBE oDISLog_WorkFlowBE) {
            int? intResult = 0;
            try {
                DISLog_WorkFlowDAL oDISLog_WorkFlowDAL = new DISLog_WorkFlowDAL();
                intResult = oDISLog_WorkFlowDAL.addWorkFlowActionsDAL(oDISLog_WorkFlowBE);
                oDISLog_WorkFlowDAL = null;
            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public List<DISLog_WorkFlowBE> GetWorkFlowActionsBAL(DISLog_WorkFlowBE oDISLog_WorkFlowBE) {
            List<DISLog_WorkFlowBE> DISLog_WorkFlowBEList = new List<DISLog_WorkFlowBE>();

            try {
                DISLog_WorkFlowDAL oDISLog_WorkFlowDAL = new DISLog_WorkFlowDAL();

                DISLog_WorkFlowBEList = oDISLog_WorkFlowDAL.GetWorkFlowActionsDAL(oDISLog_WorkFlowBE);
                oDISLog_WorkFlowDAL = null;
            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return DISLog_WorkFlowBEList;
        }

        public List<DISLog_WorkFlowBE> GetPOValueExpectedBAL(DISLog_WorkFlowBE oDISLog_WorkFlowBE) {
            List<DISLog_WorkFlowBE> DISLog_WorkFlowBEList = new List<DISLog_WorkFlowBE>();

            try {
                DISLog_WorkFlowDAL oDISLog_WorkFlowDAL = new DISLog_WorkFlowDAL();

                DISLog_WorkFlowBEList = oDISLog_WorkFlowDAL.GetPOValueExpectedDAL(oDISLog_WorkFlowBE);
                oDISLog_WorkFlowDAL = null;
            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return DISLog_WorkFlowBEList;
        }

        public List<DISLog_WorkFlowBE> GetPOCarriageCostBAL(DISLog_WorkFlowBE oDISLog_WorkFlowBE) {
            List<DISLog_WorkFlowBE> DISLog_WorkFlowBEList = new List<DISLog_WorkFlowBE>();

            try {
                DISLog_WorkFlowDAL oDISLog_WorkFlowDAL = new DISLog_WorkFlowDAL();

                DISLog_WorkFlowBEList = oDISLog_WorkFlowDAL.GetPOCarriageCostDAL(oDISLog_WorkFlowBE);
                oDISLog_WorkFlowDAL = null;
            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return DISLog_WorkFlowBEList;
        }

        public DataTable GetVendorCollectionDataBAL(DISLog_WorkFlowBE oDISLog_WorkFlowBE) {
            //List<DISLog_WorkFlowBE> DISLog_WorkFlowBEList = new List<DISLog_WorkFlowBE>();
            DataTable dt = new DataTable();
            try {
                DISLog_WorkFlowDAL oDISLog_WorkFlowDAL = new DISLog_WorkFlowDAL();

                dt = oDISLog_WorkFlowDAL.GetVendorCollectionDataDAL(oDISLog_WorkFlowBE);
                oDISLog_WorkFlowDAL = null;
            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return dt;
        }

        public DataTable GetDataForSpecificActionBAL(DISLog_WorkFlowBE oDISLog_WorkFlowBE) {
            DataTable dt = new DataTable();
            try {
                DISLog_WorkFlowDAL oDISLog_WorkFlowDAL = new DISLog_WorkFlowDAL();

                dt = oDISLog_WorkFlowDAL.GetDataForSpecificActionDAL(oDISLog_WorkFlowBE);
                oDISLog_WorkFlowDAL = null;
            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return dt;
        }

        public string GetVendorAccountPayablesEmail(DISLog_WorkFlowBE oDISLog_WorkFlowBE) {
            string VendorAccountPayablesEmail = string.Empty;
            try {
                DISLog_WorkFlowDAL oDISLog_WorkFlowDAL = new DISLog_WorkFlowDAL();

                VendorAccountPayablesEmail = oDISLog_WorkFlowDAL.GetVendorAccountPayablesEmail(oDISLog_WorkFlowBE);
                oDISLog_WorkFlowDAL = null;
            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return VendorAccountPayablesEmail;
        }
    }
}

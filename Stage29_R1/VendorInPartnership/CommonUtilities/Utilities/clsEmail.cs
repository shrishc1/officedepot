﻿using System;
using System.Configuration;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using WebUtilities;


namespace Utilities
{
    public class clsEmail
    {
        #region Mail Properties Declarations
        private string emailHeader;
        private string emailFooter;
        private string emailMessage;
        private string emailSubject;
        private string emailVariables;
        private MailMessage objMail;

        private string fromEmail;
        private string fromName;
        private bool _IsHTMLMail = false;
        // private MailAddress cc;
        // private MailAddress bcc;
        #endregion

        private SmtpClient sm = new SmtpClient();
        public clsEmail()
        {
            objMail = new MailMessage();
            this.smtpHost = ConfigurationManager.AppSettings["SMTPServer"];
            this.smtpPort = Convert.ToInt32(ConfigurationManager.AppSettings["SMTPPort"]);
        }

        #region Mail Properties
        /// <summary>
        ///  Email Sender  Property
        /// </summary>
        public string FromEmail
        {
            get
            {
                return fromEmail;
            }
            set
            {
                fromEmail = value;
            }
        }

        public string FromName
        {
            get
            {
                return fromName;
            }
            set
            {
                fromName = value;
            }
        }
        public string EmailHeaderText
        {
            get
            {
                return emailHeader;
            }
            set
            {
                emailHeader = value.ToString();
            }
        }
        public string EmailFooterText
        {
            get
            {
                return emailFooter;
            }
            set
            {
                emailFooter = value.ToString();
            }
        }

        public string EmailMessageText
        {
            get
            {
                return emailMessage;
            }
            set
            {
                emailMessage = value.ToString();
            }
        }
        public string EmailSubjectText
        {
            get
            {
                return emailSubject;
            }
            set
            {
                emailSubject = value.ToString();
            }
        }
        public string EmailVariablesText
        {
            get
            {
                return emailVariables;
            }
            set
            {
                emailVariables = value.ToString();
            }
        }
        public string LogoImage { get; set; }
        #endregion

        #region SMPT Configuration
        public string smtpHost
        {
            get
            {
                return sm.Host.ToString();
            }
            set
            {
                sm.Host = value.ToString();
            }
        }
        public int SmtpConnectionTimeout
        {
            get
            {
                return sm.Timeout;
            }
            set
            {
                sm.Timeout = value;
            }
        }
        public int smtpPort
        {
            get
            {
                return sm.Port;
            }
            set
            {
                sm.Port = value;
                // sm.SendCompleted
            }
        }
        public bool IsHTMLMail
        {
            get
            {
                return _IsHTMLMail;
            }
            set
            {
                _IsHTMLMail = value;
            }
        }

        private string _ReplyTo;
        public string ReplyTo
        {
            get
            {
                return _ReplyTo;
            }
            set
            {
                _ReplyTo = value;
            }
        }
        #endregion

        #region Function add Bcc,cc,to, From
        public void fromAddress(MailAddress mailAddress)
        {
            fromEmail = mailAddress.Address;
            fromName = mailAddress.DisplayName;
            this.objMail.From = mailAddress;
            //this.objMail.From.DisplayName
        }
        public void fromAddress(string mailAddress, string displayName)
        {
            this.objMail.From = new MailAddress(mailAddress, displayName);
            //this.objMail.From.DisplayName
        }

        public void toAdd(MailAddress mailAddress)
        {
            //this.objMail.To.Add(mailAddress);

            MailAddress m = new MailAddress(mailAddress.Address);
            this.objMail.To.Add(m);
        }
        public void toAdd(string mailAddress)
        {
            //this.objMail.To.Add(mailAddress);

            MailAddress m = new MailAddress(mailAddress);
            this.objMail.To.Add(m);

        }
        public void toAdd(MailAddress[] mailAddress)
        {
            foreach (MailAddress str in mailAddress)
            {
                //this.objMail.To.Add(str);

                MailAddress m = new MailAddress(str.Address);
                this.objMail.To.Add(m);
            }
        }
        public void toAdd(MailAddressCollection mailAddressCollection)
        {
            foreach (MailAddress mailAddress in mailAddressCollection)
            {
                //this.objMail.To.Add(mailAddress);

                MailAddress m = new MailAddress(mailAddress.Address);
                this.objMail.To.Add(m);
            }
        }
        public void bccAdd(MailAddress mailAddress)
        {
            this.objMail.Bcc.Add(mailAddress);
        }
        public void bccAdd(string mailAddress)
        {
            this.objMail.Bcc.Add(mailAddress);
        }
        public void bccAdd(MailAddress[] mailAddress)
        {
            foreach (MailAddress str in mailAddress)
            {
                this.objMail.Bcc.Add(str);
            }

        }
        public void bccAdd(MailAddressCollection mailAddressCollection)
        {
            foreach (MailAddress mailAddress in mailAddressCollection)
            {
                this.objMail.Bcc.Add(mailAddress);
            }
        }

        public void ccAdd(MailAddress mailAddress)
        {
            this.objMail.CC.Add(mailAddress);
        }

        public void ccAdd(string mailAddress)
        {
            this.objMail.CC.Add(mailAddress);
        }
        public void ccAdd(MailAddress[] mailAddress)
        {
            foreach (MailAddress str in mailAddress)
            {
                this.objMail.CC.Add(str);
            }
        }
        public void ccAdd(MailAddressCollection mailAddressCollection)
        {
            foreach (MailAddress mailAddress in mailAddressCollection)
            {
                this.objMail.CC.Add(mailAddress);
            }
        }

        public void ReplyToAdd(string mailAddress)
        {
            MailAddress m = new MailAddress(mailAddress);
            this.objMail.ReplyToList.Add(m);
        }


        #endregion

        #region Function Send Mail     

        public void sendMail(string toAddress, string mailBody, string mailSubject, string fromAddress, bool bIsHTMLMail)
        {
            try
            {
                clsEmail mail = new clsEmail();
                System.Net.Mail.MailAddressCollection mailAddress = new System.Net.Mail.MailAddressCollection();
                string[] sMailAddress = null;

                if (toAddress.Contains(","))
                    sMailAddress = toAddress.Split(',');
                else if (toAddress.Contains(";"))
                    sMailAddress = toAddress.Split(';');

                if (sMailAddress != null)
                {
                    for (int i = 0; i < sMailAddress.Length; i++)
                        if (!string.IsNullOrEmpty(sMailAddress[i].Trim()))
                            mailAddress.Add(sMailAddress[i].Trim());
                }
                else
                {
                    if (!string.IsNullOrEmpty(toAddress.Trim()))
                        mailAddress.Add(toAddress.Trim());
                }

                mail.toAdd(mailAddress);
                mail.EmailMessageText = mailBody;
                mail.EmailSubjectText = mailSubject;
                mail.FromEmail = fromAddress;
                mail.IsHTMLMail = true;

                mail.bccAdd(Convert.ToString(ConfigurationManager.AppSettings["bccEmailAddress"]));
                mail.FromName = "Office Depot - Do not reply.";

                mail.SendMailWithLogo();
            }
            catch (Exception ex)
            {
                StringBuilder sb = new StringBuilder();
                sb.Append(ex.Message.ToString());
                LogUtility.SaveTraceLogEntry(sb);
                LogUtility.SaveErrorLogEntry(ex);
            }
        }

        public void sendMailWithBcc(string toAddress, string mailBody, string mailSubject, string fromAddress, bool bIsHTMLMail, string bcc)
        {
            try
            {
                clsEmail mail = new clsEmail();
                System.Net.Mail.MailAddressCollection mailAddress = new System.Net.Mail.MailAddressCollection();
                string[] BccMail = bcc.Split(',');
                string[] sMailAddress = null;
                if (toAddress.Contains(","))
                    sMailAddress = toAddress.Split(',');
                else if (toAddress.Contains(";"))
                    sMailAddress = toAddress.Split(';');

                if (sMailAddress != null)
                {
                    for (int i = 0; i < sMailAddress.Length; i++)
                        if (!string.IsNullOrEmpty(sMailAddress[i].Trim()))
                            mailAddress.Add(sMailAddress[i].Trim());
                }
                else
                {
                    if (!string.IsNullOrEmpty(toAddress.Trim()))
                        mailAddress.Add(toAddress.Trim());
                }

                mail.toAdd(mailAddress);
                mail.EmailMessageText = mailBody;
                mail.EmailSubjectText = mailSubject;
                mail.FromEmail = fromAddress;
                mail.IsHTMLMail = bIsHTMLMail;
                foreach (var str in BccMail)
                {
                    mail.bccAdd(str);
                }
                mail.FromName = "Office Depot - Do not reply.";


                mail.SendMailWithLogo();
            }
            catch (Exception ex)
            {
                StringBuilder sb = new StringBuilder();
                sb.Append(ex.Message.ToString());
                LogUtility.SaveTraceLogEntry(sb);
                LogUtility.SaveErrorLogEntry(ex);
            }
        }

        public string getGlobalResourceValue(string key)
        {
            string ResourceValue = (String)WebCommon.getGlobalResourceValue(key);

            if (!string.IsNullOrEmpty(ResourceValue))
                return (ResourceValue).Replace('\'', '`');
            else
                return ResourceValue;
        }

        public void SendMailWithLogo()
        {
            try
            {
                string path = AppDomain.CurrentDomain.BaseDirectory.ToLower();
                path = path.Replace(@"\bin\debug", "");

                var imgPath = path + @"Images\OfficeDepotLogo.jpg";

                System.Net.Mail.AlternateView htmlView = null;

                //Add basic information.
                this.objMail.Subject = this.EmailSubjectText;
                this.objMail.Body = this.EmailHeaderText + this.EmailMessageText + this.EmailFooterText;
                this.objMail.IsBodyHtml = true;// _IsHTMLMail;
                this.objMail.Priority = MailPriority.High;

                //Create two views, one text, one HTML.
                //System.Net.Mail.AlternateView plainTextView = System.Net.Mail.AlternateView.CreateAlternateViewFromString("Welcome new User", null, "text/plain");

                string htmlBody = this.EmailMessageText;
                htmlBody = htmlBody.Replace("{logoInnerPath}", "cid:logoinner");

                htmlView = System.Net.Mail.AlternateView.CreateAlternateViewFromString(htmlBody, null, "text/html");

                // Add the alternate body to the message.
                // htmlView = AlternateView.CreateAlternateViewFromString(htmlBody, null, MediaTypeNames.Text.Html);
                string mediaType = MediaTypeNames.Image.Jpeg;
                System.Net.Mail.LinkedResource imageResourceEs = new System.Net.Mail.LinkedResource(path + @"Images\OfficeDepotLogo.jpg");
                imageResourceEs.ContentId = "logoinner";
                imageResourceEs.ContentType.MediaType = mediaType;
                imageResourceEs.TransferEncoding = System.Net.Mime.TransferEncoding.Base64;
                htmlView.LinkedResources.Add(imageResourceEs);

                objMail.AlternateViews.Add(htmlView);

                if (FromEmail != null && FromEmail.Length != 0)
                    this.fromAddress(FromEmail, FromName);

                if (ReplyTo != null && ReplyTo.Length != 0)
                    this.ReplyToAdd(ReplyTo);

                if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["UseSMTPAuthentication"]) && ConfigurationManager.AppSettings["UseSMTPAuthentication"] == "1")
                    sm.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["SMTPUserName"], ConfigurationManager.AppSettings["SMTPPassword"]);

                objMail = this.CheckDebugging(objMail);

                sm.Send(objMail);
            }
            catch (Exception ex)
            {
                StringBuilder sb = new StringBuilder();
                sb.Append(ex.Message.ToString());
                LogUtility.SaveTraceLogEntry(sb);
                LogUtility.SaveErrorLogEntry(ex);
                //  clsbExceptionLogger.HandleException(ex);
            }
        }



        public void SendMail(MailAddress[] emailList, MailAddress[] emailCC, MailAddress[] emailBCC, string subject, string message)
        {

            MailMessage objMail = new MailMessage();
            foreach (MailAddress str in emailList)
            {
                objMail.To.Add(str);
            }
            foreach (MailAddress str in emailCC)
            {
                objMail.CC.Add(str);
            }
            foreach (MailAddress str in emailBCC)
            {
                objMail.CC.Add(str);
            }

            objMail.Subject = subject.ToString();
            objMail.Body = message.ToString();

            objMail.IsBodyHtml = true;
            try
            {
                //MailAddress from = new MailAddress(FromEmail, FromName);
                if (FromEmail != null && FromEmail.Length != 0)
                    this.fromAddress(FromEmail, FromName);
                SmtpClient sm = new SmtpClient();

                if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["UseSMTPAuthentication"]) && ConfigurationManager.AppSettings["UseSMTPAuthentication"] == "1")
                {
                    sm.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["SMTPUserName"], ConfigurationManager.AppSettings["SMTPPassword"]);
                }

                objMail = this.CheckDebugging(objMail);

                sm.Send(objMail);
            }
            catch (Exception ex)
            {
                StringBuilder sb = new StringBuilder();
                sb.Append(ex.Message.ToString());
                LogUtility.SaveTraceLogEntry(sb);
                LogUtility.SaveErrorLogEntry(ex);
                //  clsbExceptionLogger.HandleException(ex);
            }
            finally
            {
                GC.SuppressFinalize(this);
            }
        }
        #endregion

        #region Distructor
        public void Dispose()
        {
            GC.SuppressFinalize(this);

        }
        #endregion

        public MailMessage CheckDebugging(MailMessage message)
        {
            bool IsEmailSend = Convert.ToBoolean(ConfigurationManager.AppSettings["IsEmailSend"]);
            string EMAIL_DEBUGGING_ADDRESS = Convert.ToString(ConfigurationManager.AppSettings["EMAIL_DEBUGGING_ADDRESS"]);

            // HardCode
            //EMAIL_DEBUGGING = true;

            if (!IsEmailSend)
            {
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.AppendLine("");
                sb.AppendLine("Email at " + DateTime.Now.ToString() + ":");
                sb.AppendLine("Subject:" + message.Subject);
                sb.AppendLine("BCC:" + message.Bcc.ToString() + "; " + "CC:" + message.CC.ToString() + "; " + "To:" + message.To.ToString() + "");
                sb.AppendLine("");
                LogUtility.SaveTraceLogEntry(sb);

                //message.Body =  message.Body.Replace("<body>", "<body>BCC:" + message.Bcc.ToString() + "<br/>" + "CC:" + message.CC.ToString() + "<br/>" + "To:" + message.To.ToString() + "<br/>");
                message.Bcc.Clear();
                message.CC.Clear();
                message.To.Clear();
                message.To.Add(new MailAddress(EMAIL_DEBUGGING_ADDRESS));
                message.IsBodyHtml = true;
            }
            return message;
        }


    }
}

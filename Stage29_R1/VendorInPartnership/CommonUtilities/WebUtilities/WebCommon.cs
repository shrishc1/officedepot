﻿using BaseControlLibrary;
using HtmlAgilityPack;
using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace WebUtilities
{
    /// <summary>
    /// Class contains all common methods and static values used in application
    /// </summary>
    public static class WebCommon
    {
        /// <summary>
        /// Method to filll dropdown list
        /// </summary>
        /// <param name="drpList">ref DropDownList, DropDownList to be bind</param>
        /// <param name="lstData">List, DataSource</param>
        /// <param name="dataText">string, DataTextField</param>
        /// <param name="dataValueFiels">string, DataValueField</param>
        /// <param name="initialText">string, Default selected text</param>
        ///

        public static string GlobalURN = string.Empty;
        public static string GlobalDonorName = string.Empty;
        public static string GlobalPledgeDate = string.Empty;

        public static string GlobalURN1 = string.Empty;
        public static string GlobalDonorName1 = string.Empty;
        public static string GlobalPledgeDate1 = string.Empty;

        public static void FillDropDown(ref DropDownList drpList, IList lstData, string dataText, string dataValueFields, string initialText)
        {
            drpList.Items.Clear();
            drpList.DataSource = lstData;
            drpList.DataTextField = dataText;
            drpList.DataValueField = dataValueFields;
            drpList.DataBind();
            drpList.Items.Insert(0, new ListItem("--" + initialText + "--", "0"));
        }

        /// <summary>
        /// Method to filll dropdown list
        /// </summary>
        /// <param name="drpList">ref DropDownList, DropDownList to be bind</param>
        /// <param name="lstData">List, DataSource</param>
        /// <param name="dataText">string, DataTextField</param>
        /// <param name="dataValueFiels">string, DataValueField</param>
        /// <param name="initialText">string, Default selected text</param>
        public static void FillDropDown(ref DropDownList drpList, IList lstData, string dataText, string dataValueFields, string initialText, bool IsIntialValueRequired)
        {
            drpList.Items.Clear();
            drpList.DataSource = lstData;
            drpList.DataTextField = dataText;
            drpList.DataValueField = dataValueFields;
            drpList.DataBind();
            if (IsIntialValueRequired)
            {
                drpList.Items.Insert(0, new ListItem("--Select--", "0"));
            }
        }

        /// <summary>
        /// Method to filll CheckBoxList
        /// </summary>
        /// <param name="drpList">ref CheckBoxList, CheckBoxList to be bind</param>
        /// <param name="lstData">List, DataSource</param>
        /// <param name="dataText">string, DataTextField</param>
        /// <param name="dataValueFiels">string, DataValueField</param>
        public static void FillCheckBoxList(ref CheckBoxList chkList, IList lstData, string dataText, string dataValueFields)
        {
            chkList.Items.Clear();
            chkList.DataSource = lstData;
            chkList.DataTextField = dataText;
            chkList.DataValueField = dataValueFields;
            chkList.DataBind();
        }

        public static void FillRadioList(ref CheckBoxList rdList, IList lstData, string dataText, string dataValueFields)
        {
            rdList.DataSource = lstData;
            rdList.DataTextField = dataText;
            rdList.DataValueField = dataValueFields;
            rdList.DataBind();
        }

        /// <summary>
        /// Method to bind gridview
        /// </summary>
        /// <param name="gvList">ref GridView to be bind</param>
        /// <param name="lstData">List, DataSource</param>
        public static void FillGrid(ref GridView gvList, IList lstData)
        {
            gvList.DataSource = lstData;
            gvList.PageSize = int.Parse(ConfigurationManager.AppSettings["PageSize"]);
            gvList.AllowPaging = true;
            gvList.DataBind();
        }

        public static void FillGrid(ref GridView gvList, IList lstData, bool AllowPaging)
        {
            gvList.DataSource = lstData;
            gvList.PageSize = int.Parse(ConfigurationManager.AppSettings["PageSize"]);
            gvList.AllowPaging = AllowPaging;
            gvList.DataBind();
        }

        /// <summary>
        /// Method to bind gridview
        /// </summary>
        /// <param name="gvList">ref GridView to be bind</param>
        /// <param name="lstData">List, DataSource</param>
        public static void FillPopupGrid(ref GridView gvList, IList lstData)
        {
            gvList.DataSource = lstData;
            gvList.PageSize = 5;
            gvList.AllowPaging = true;
            gvList.DataBind();
        }

        /// <summary>
        /// Method to bind DataList
        /// </summary>
        /// <param name="DList">ref DataList to be bind</param>
        /// <param name="lstData">List, DataSource</param>
        public static void FillDataList(ref DataList DList, IList lstData)
        {
            DList.DataSource = lstData;
            DList.DataBind();
        }

        public static void FillRepeater(ref Repeater rptDeals, IList lstData, int iPageSize)
        {
            //throw new NotImplementedException();

            PagedDataSource page = new PagedDataSource();
            page.AllowCustomPaging = true;
            page.AllowPaging = true;
            page.DataSource = lstData;
            page.PageSize = iPageSize;
            rptDeals.DataSource = page;
            rptDeals.DataBind();
        }

        public static void FillRepeater(ref Repeater rptDeals, IList lstData)
        {
            //throw new NotImplementedException();
            rptDeals.DataSource = lstData;
            rptDeals.DataBind();
        }

        public static void FillListBox(ref ListBox DList, IList lstData, string sValue, string sText)
        {
            DList.DataSource = lstData;
            DList.DataTextField = sText;
            DList.DataValueField = sValue;
            DList.DataBind();
        }

        /// <summary>
        /// Method to bind gridview
        /// </summary>
        /// <param name="gvList">ref GridView to be bind</param>
        /// <param name="lstData">List, DataSource</param>
        public static void FillGridComplaints(ref GridView gvList, IList lstData)
        {
            gvList.DataSource = lstData;
            gvList.PageSize = 1;
            gvList.AllowPaging = true;
            gvList.DataBind();
        }

        /// <summary>
        /// Method to clear controls initial values
        /// </summary>
        /// <param name="root"></param>
        /// <returns></returns>
        public static int ClearFields(Control root)
        {
            if (root is TextBox)
            {
                ((TextBox)root).Text = string.Empty;
            }
            if (root is DropDownList)
            {
                ((DropDownList)root).ClearSelection();
            }
            if (root is CheckBoxList)
            {
                ((CheckBoxList)root).ClearSelection();
            }
            if (root is CheckBox)
            {
                ((CheckBox)root).Checked = false;
            }
            if (root is HyperLink)
            {
                ((HyperLink)root).Text = string.Empty;
            }
            if (root is HiddenField)
            {
                ((HiddenField)root).Value = string.Empty;
            }

            foreach (Control child in root.Controls)
            {
                ClearFields(child);
            }
            return 0;
        }

        public static int ChangeRepeaterItemMode(RepeaterItem rptItem, Boolean isEdit)
        {
            foreach (Control child in rptItem.Controls)
            {
                if (child is HtmlTableCell)
                {
                    foreach (Control subChild in child.Controls)
                    {
                        setVisibility(subChild, isEdit);
                    }
                }
                else
                {
                    setVisibility(child, isEdit);
                }
            }
            return 0;
        }

        private static void setVisibility(Control child, Boolean isEdit)
        {
            if (child is Literal)
            {
                ((Literal)child).Visible = !isEdit;
            }
            else if (child is TextBox)
            {
                ((TextBox)child).Visible = isEdit;
            }
            else if (child is DropDownList)
            {
                ((DropDownList)child).Visible = isEdit;
            }
            else if (child is ListBox)
            {
                ((ListBox)child).Visible = isEdit;
            }
            else if (child is CheckBox)
            {
                ((CheckBox)child).Enabled = isEdit;
            }
        }

        public static void addControlCalcCharLen(TextBox txtBoxToValidate, string cntrName, int limitChars)
        {
            Label lblDisplayChars = new Label();
            lblDisplayChars.ID = cntrName;
            lblDisplayChars.Text = "<span class='bold'>" + (limitChars - txtBoxToValidate.Text.Length) + "</span>&nbsp;<span >characters&nbsp;remaining.</span> ";
            lblDisplayChars.Width = 250;
            txtBoxToValidate.Parent.Controls.Add(lblDisplayChars);

            txtBoxToValidate.Attributes.Add("onKeyUp", "calcCharLen(this.form.name, this.name, '" + lblDisplayChars.ClientID.ToString() + "', " + limitChars.ToString() + ")");
            txtBoxToValidate.Attributes.Add("onblur", "calcCharLen(this.form.name, this.name, '" + lblDisplayChars.ClientID.ToString() + "', " + limitChars.ToString() + ")");
        }

        //public static string getGlobalResourceValue(string key)
        //{
        //    string ResourceValue = (String)HttpContext.GetGlobalResourceObject("OfficeDepot", key);

        //    if (!string.IsNullOrEmpty(ResourceValue))
        //        return (ResourceValue).Replace('\'', '`');
        //    else
        //        return ResourceValue;
        //}

        public static string getGlobalResourceValue(string key, string UserLanguage = "")
        {
            return WebLocalize.Resource.getGlobalResourceValue(key, UserLanguage);
        }

        public static string getGlobalResourceValueByLangID(string key, int LanguageID)
        {
            return WebLocalize.Resource.getGlobalResourceValueByLangID(key, LanguageID);
        }

        public static void ExporttoExcel(string fileName, GridView gv, bool IsHideHidden)
        {
            try
            {
                string date = DateTime.Now.ToString("ddMMyyyy");
                string time = DateTime.Now.ToString("HH:mm");
                HttpContext context = HttpContext.Current;

                context.Response.Clear();
                context.Response.ClearContent();
                context.Response.ClearHeaders();
                context.Response.Buffer = true;
                context.Response.ContentType = "application/vnd.xls";

                context.Response.AddHeader("content-disposition", string.Format("attachment; filename={0}_{1}_{2}", fileName, date, time + ".xls"));

                context.Response.Charset = "utf-8";
                context.Response.ContentEncoding = Encoding.UTF8;

                context.Response.Write("<Table border='1' bgColor='#ffffff' " +
                  "borderColor='#000000' cellSpacing='0' cellPadding='0' " +
                  "style='font-size:10.0pt; font-family:Calibri; background:white;color:black;'> <TR>");

                DataTable table = new DataTable();

                using (System.IO.StringWriter sw = new System.IO.StringWriter())
                {
                    using (HtmlTextWriter htw = new HtmlTextWriter(sw))
                    {
                        // Create a form to contain the grid
                        Table HTMLtable = new Table();

                        // add the header row to the table
                        if (gv.HeaderRow != null)
                        {
                            //for left align
                            for (int columnIndex = 0; columnIndex < gv.HeaderRow.Cells.Count; columnIndex++)
                            {
                                if (gv.Columns[columnIndex].Visible)
                                {
                                    HTMLtable.Rows.Add(gv.HeaderRow);
                                }
                                else
                                {
                                    gv.HeaderRow.Cells[columnIndex].Attributes.Add("class", "text");
                                    gv.HeaderRow.Cells[columnIndex].Text = string.Empty;
                                }
                            }

                            PrepareControlForExport(gv.HeaderRow);

                            if (IsHideHidden)
                            {
                                gv.HeaderRow.Cells[0].Attributes.Add("class", "text");
                                gv.HeaderRow.Cells[0].Text = string.Empty;
                            }
                        }

                        // add each of the data rows to the table
                        foreach (GridViewRow row in gv.Rows)
                        {
                            PrepareControlForExport(row);

                            for (int columnIndex = 0; columnIndex < row.Cells.Count; columnIndex++)
                            {
                                if (gv.Columns[columnIndex].Visible == true)
                                {
                                    HTMLtable.Rows.Add(row);
                                }
                                else
                                {
                                    //row.Cells[columnIndex].Style.Add("display", "none");
                                    row.Cells[columnIndex].Attributes.Add("class", "text");
                                }
                            }

                            if (IsHideHidden)
                            {
                                row.Cells[0].Attributes.Add("class", "text");
                            }

                            /// -----------------------------------
                            /// --- Added on 06 Mar 2013 --- START
                            /// -----------------------------------
                            HtmlGenericControl ht = (HtmlGenericControl)row.FindControl("div1");
                            if (ht != null)
                                ht.InnerHtml = ht.InnerHtml.Replace("<br/>", ", ");
                            /// -----------------------------------
                            /// --- Added on 06 Mar 2013 --- END
                            /// -----------------------------------
                            //for (int columnIndex = 0; columnIndex < row.Cells.Count; columnIndex++) {
                            //row.Cells[columnIndex].Attributes.Add("class", "text");
                            //}
                        }

                        // add the footer row to the table
                        if (gv.FooterRow != null)
                        {
                            PrepareControlForExport(gv.FooterRow);
                            HTMLtable.Rows.Add(gv.FooterRow);
                        }

                        // render the table into the htmlwriter
                        HTMLtable.RenderControl(new HtmlTextWriter(sw));

                        HtmlDocument doc = new HtmlDocument();
                        doc.LoadHtml(sw.ToString());
                        var headers = doc.DocumentNode.SelectNodes("//tr/th");

                        foreach (HtmlNode header in headers)
                        {
                            if (!(header.Attributes[0].Name == "class" && header.Attributes[0].Value == "text"))
                                table.Columns.Add(header.InnerText.Trim().Replace("/r/n", "").Replace("&nbsp;", "")); // create columns from th
                        }

                        // select rows with td elements
                        foreach (var row in doc.DocumentNode.SelectNodes("//tr[td]"))
                            table.Rows.Add(row.SelectNodes("td").Where(r => r.Attributes.Count == 0 || (r.Attributes[0].Name != "class" && r.Attributes[0].Value != "text")).Select(td => td.InnerText.Trim().Replace("/r/n", "").Replace("&nbsp;", "")).ToArray());
                    }
                }

                int columnscount = table.Columns.Count;

                for (int j = 0; j < columnscount; j++)
                {
                    context.Response.Write("<Td style='background-color:#DDDDDD;'>");
                    context.Response.Write("<B>");
                    context.Response.Write(table.Columns[j].ColumnName);
                    context.Response.Write("</B>");
                    context.Response.Write("</Td>");
                }

                context.Response.Write("</TR>");

                foreach (DataRow row in table.Rows)
                {
                    context.Response.Write("<TR>");

                    for (int i = 0; i < table.Columns.Count; i++)
                    {
                        context.Response.Write("<Td>");
                        context.Response.Write(row[i].ToString());
                        context.Response.Write("</Td>");
                    }

                    context.Response.Write("</TR>");
                }

                context.Response.Write("</Table>");

                context.Response.Write("</font>");
                context.Response.Flush();
                context.Response.End();

                HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Old Format user before phase 17
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="gv"></param>
        ///
        public static void ExportHideHidden(string fileName, GridView gv, bool IsHideHidden = false)
        {
            int[] pageindex = new int[1];
            ExportHideHidden(fileName, gv, pageindex, IsHideHidden);
        }

        public static void ExportHideHidden(string fileName, GridView gv, int[] TextColIndex, bool IsHideHidden = false, bool IsAutogeneratedGridView = false, bool isHideSecondColumn = false)
        {
            //for left align
            string date = DateTime.Now.ToString("ddMMyyyy");
            string time = DateTime.Now.ToString("HH:mm");
            string style = @"<style> .text { mso-number-format:\@;text-align:left;border:.5pt solid black;font-size:10.0pt; font-family:Calibri;} </style> ";
            string stylenum = @"<style> .textnum { mso-number-format:General;text-align:left;border:.5pt solid black;font-size:10.0pt; font-family:Calibri;} </style> ";

            //string style = @"<style> .text { text-align:left;border:.5pt solid black;font-size:10.0pt; font-family:Calibri;} </style> ";
            System.Web.HttpContext.Current.Response.Clear();
            System.Web.HttpContext.Current.Response.AddHeader("content-disposition", string.Format("attachment; filename={0}_{1}_{2}", fileName, date, time + ".xls"));

            //System.Web.HttpContext.Current.Response.ContentType = "application/ms-excel";
            System.Web.HttpContext.Current.Response.ContentType = "application/vnd.xls";

            System.Web.HttpContext.Current.Response.Charset = "utf-8";
            System.Web.HttpContext.Current.Response.ContentEncoding = System.Text.Encoding.GetEncoding("windows-1250");

            using (System.IO.StringWriter sw = new System.IO.StringWriter())
            {
                using (HtmlTextWriter htw = new HtmlTextWriter(sw))
                {
                    // Create a form to contain the grid
                    Table table = new Table();

                    // add the header row to the table
                    if (gv.HeaderRow != null)
                    {
                        PrepareControlForExport(gv.HeaderRow, 0, isHideSecondColumn);

                        //for left align
                        for (int columnIndex = 0; columnIndex < gv.HeaderRow.Cells.Count; columnIndex++)
                        {
                            if (IsAutogeneratedGridView == false)
                            {
                                if (columnIndex == 1 && isHideSecondColumn == true)  // to hide second column
                                {
                                    gv.HeaderRow.Cells[columnIndex].Style.Add("display", "none");
                                    gv.HeaderRow.Cells[columnIndex].Text = string.Empty;
                                    table.Rows.Add(gv.HeaderRow);
                                    gv.HeaderRow.Cells[columnIndex].Visible = false;
                                }
                                else if (gv.Columns[columnIndex].Visible == true)
                                {
                                    gv.HeaderRow.Cells[columnIndex].Attributes.Add("class", "text");
                                    gv.HeaderRow.Cells[columnIndex].Style.Add("background-color", "#DBDBDB");
                                    table.Rows.Add(gv.HeaderRow);
                                }
                                else
                                {
                                    gv.HeaderRow.Cells[columnIndex].Style.Add("display", "none");
                                    gv.HeaderRow.Cells[columnIndex].Text = string.Empty;
                                    table.Rows.Add(gv.HeaderRow);
                                    gv.HeaderRow.Cells[columnIndex].Visible = false;
                                }
                            }
                            else
                            {
                                gv.HeaderRow.Cells[columnIndex].Attributes.Add("class", "text");
                                gv.HeaderRow.Cells[columnIndex].Style.Add("background-color", "#DBDBDB");
                                table.Rows.Add(gv.HeaderRow);
                            }
                        }
                    }

                    // add each of the data rows to the table
                    foreach (GridViewRow row in gv.Rows)
                    {
                        PrepareControlForExport(row, row.RowIndex + 1, isHideSecondColumn);

                        /// -----------------------------------
                        /// --- Added on 06 Mar 2013 --- START
                        /// -----------------------------------
                        HtmlGenericControl ht = (HtmlGenericControl)row.FindControl("div1");
                        if (ht != null)
                            ht.InnerHtml = ht.InnerHtml.Replace("<br/>", ", ");
                        /// -----------------------------------
                        /// --- Added on 06 Mar 2013 --- END
                        /// -----------------------------------
                        ///

                        for (int columnIndex = 0; columnIndex < row.Cells.Count; columnIndex++)
                        {
                            if (IsAutogeneratedGridView == false)
                            {
                                if (columnIndex == 1 && isHideSecondColumn == true)  // to hide second column
                                {
                                    row.Cells[columnIndex].Style.Add("display", "none");
                                    table.Rows.Add(row);
                                    row.Cells[columnIndex].Visible = false;
                                }
                                else if (gv.Columns[columnIndex].Visible == true)
                                {
                                    string st = row.Cells[columnIndex].Text.Trim().Replace("/r/n", "").Replace("&nbsp;", "");

                                    if (string.IsNullOrEmpty(st))
                                    {
                                        ControlCollection ctr = row.Cells[columnIndex].Controls;

                                        if (ctr != null && ctr.Count > 0)
                                        {
                                            for (int iCount = 0; iCount < ctr.Count; iCount++)
                                            {
                                                if (ctr[iCount].GetType() == typeof(System.Web.UI.LiteralControl))
                                                {
                                                    st = ((System.Web.UI.LiteralControl)ctr[iCount]).Text.Trim().Replace("/r/n", "").Replace("&nbsp;", "");
                                                }
                                                else if (ctr[iCount].GetType() == typeof(BaseControlLibrary.ucLiteral))
                                                {
                                                    st = ((BaseControlLibrary.ucLiteral)ctr[iCount]).Text.Trim().Replace("/r/n", "").Replace("&nbsp;", "");
                                                }
                                                else if (ctr[iCount].GetType() == typeof(System.Web.UI.WebControls.Literal))
                                                {
                                                    st = ((System.Web.UI.WebControls.Literal)ctr[iCount]).Text.Trim().Replace("/r/n", "").Replace("&nbsp;", "");
                                                }

                                                if (!string.IsNullOrEmpty(st))
                                                {
                                                    break;
                                                }
                                            }
                                        }
                                    }

                                    int intcolumnIndex = 0;
                                    if (TextColIndex != null)
                                        intcolumnIndex = Array.Find(TextColIndex, element => element.ToString() == (columnIndex + 1).ToString());

                                    if (columnIndex + 1 == intcolumnIndex)
                                    {
                                        row.Cells[columnIndex].Attributes.Add("class", "text");
                                    }
                                    else
                                    {
                                        if (string.IsNullOrEmpty(st) || IsNumeric(st))
                                        {
                                            row.Cells[columnIndex].Attributes.Add("class", "textnum");
                                        }
                                        else
                                        {
                                            row.Cells[columnIndex].Attributes.Add("class", "text");
                                        }
                                    }
                                    table.Rows.Add(row);
                                }
                                else
                                {
                                    row.Cells[columnIndex].Style.Add("display", "none");
                                    table.Rows.Add(row);
                                    row.Cells[columnIndex].Visible = false;
                                }
                            }
                            else
                            {
                                string st = row.Cells[columnIndex].Text.Trim().Replace("/r/n", "").Replace("&nbsp;", "");

                                if (string.IsNullOrEmpty(st))
                                {
                                    ControlCollection ctr = row.Cells[columnIndex].Controls;

                                    if (ctr != null && ctr.Count > 0)
                                    {
                                        for (int iCount = 0; iCount < ctr.Count; iCount++)
                                        {
                                            if (ctr[iCount].GetType() == typeof(System.Web.UI.LiteralControl))
                                            {
                                                st = ((System.Web.UI.LiteralControl)ctr[iCount]).Text.Trim().Replace("/r/n", "").Replace("&nbsp;", "");
                                            }
                                            else if (ctr[iCount].GetType() == typeof(BaseControlLibrary.ucLiteral))
                                            {
                                                st = ((BaseControlLibrary.ucLiteral)ctr[iCount]).Text.Trim().Replace("/r/n", "").Replace("&nbsp;", "");
                                            }
                                            else if (ctr[iCount].GetType() == typeof(System.Web.UI.WebControls.Literal))
                                            {
                                                st = ((System.Web.UI.WebControls.Literal)ctr[iCount]).Text.Trim().Replace("/r/n", "").Replace("&nbsp;", "");
                                            }

                                            if (!string.IsNullOrEmpty(st))
                                            {
                                                break;
                                            }
                                        }
                                    }
                                }

                                int intcolumnIndex = 0;
                                if (TextColIndex != null)
                                    intcolumnIndex = Array.Find(TextColIndex, element => element.ToString() == (columnIndex + 1).ToString());

                                if (columnIndex + 1 == intcolumnIndex)
                                {
                                    row.Cells[columnIndex].Attributes.Add("class", "text");
                                }
                                else
                                {
                                    if (string.IsNullOrEmpty(st) || IsNumeric(st))
                                    {
                                        row.Cells[columnIndex].Attributes.Add("class", "textnum");
                                    }
                                    else
                                    {
                                        row.Cells[columnIndex].Attributes.Add("class", "text");
                                    }
                                }
                                table.Rows.Add(row);
                            }
                        }
                    }

                    // add the footer row to the table
                    if (gv.FooterRow != null)
                    {
                        PrepareControlForExport(gv.FooterRow, 100, isHideSecondColumn);
                        table.Rows.Add(gv.FooterRow);
                    }

                    if (IsHideHidden)
                    {
                        for (int iRow = 0; iRow < table.Rows.Count; iRow++)
                        {
                            table.Rows[iRow].Cells[0].Visible = false;
                        }
                    }

                    // render the table into the htmlwriter
                    table.RenderControl(htw);

                    System.Web.HttpContext.Current.Response.Write(style);
                    System.Web.HttpContext.Current.Response.Write(stylenum);
                    System.Web.HttpContext.Current.Response.Write(sw.ToString()); // (sw.ToString());
                    System.Web.HttpContext.Current.Response.End();

                    HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                    HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                    HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                }
            }
        }

        public static void Export(string fileName, GridView gv)
        {
            try
            {
                string date = DateTime.Now.ToString("ddMMyyyy");
                string time = DateTime.Now.ToString("HH:mm");

                //for left align
                string style = @"<style> .text { mso-number-format:General;text-align:left;border:.5pt solid black;font-size:10.0pt; font-family:Calibri;} </style> ";
                System.Web.HttpContext.Current.Response.Clear();
                System.Web.HttpContext.Current.Response.AddHeader("content-disposition", string.Format("attachment; filename={0}_{1}_{2}", fileName, date, time + ".xls"));
                System.Web.HttpContext.Current.Response.ContentType = "application/vnd.xls";
                System.Web.HttpContext.Current.Response.Charset = "utf-8";
                System.Web.HttpContext.Current.Response.ContentEncoding = System.Text.Encoding.GetEncoding("windows-1250");

                using (System.IO.StringWriter sw = new System.IO.StringWriter())
                {
                    using (HtmlTextWriter htw = new HtmlTextWriter(sw))
                    {
                        // Create a form to contain the grid
                        Table table = new Table();

                        // add the header row to the table
                        if (gv.HeaderRow != null)
                        {
                            //for left align
                            for (int columnIndex = 0; columnIndex < gv.HeaderRow.Cells.Count; columnIndex++)
                            {
                                gv.HeaderRow.Cells[columnIndex].Attributes.Add("class", "text");
                                gv.HeaderRow.Cells[columnIndex].Style.Add("background-color", "#DBDBDB");
                            }
                            PrepareControlForExport(gv.HeaderRow);
                            table.Rows.Add(gv.HeaderRow);
                        }

                        // add each of the data rows to the table
                        foreach (GridViewRow row in gv.Rows)
                        {
                            PrepareControlForExport(row);
                            /// -----------------------------------
                            /// --- Added on 06 Mar 2013 --- START
                            /// -----------------------------------
                            HtmlGenericControl ht = (HtmlGenericControl)row.FindControl("div1");
                            if (ht != null)
                                ht.InnerHtml = ht.InnerHtml.Replace("<br/>", ", ");
                            /// -----------------------------------
                            /// --- Added on 06 Mar 2013 --- END
                            /// -----------------------------------
                            for (int columnIndex = 0; columnIndex < row.Cells.Count; columnIndex++)
                            {
                                row.Cells[columnIndex].Attributes.Add("class", "text");
                            }
                            table.Rows.Add(row);
                        }

                        // add the footer row to the table
                        if (gv.FooterRow != null)
                        {
                            PrepareControlForExport(gv.FooterRow);
                            table.Rows.Add(gv.FooterRow);
                        }

                        // render the table into the htmlwriter
                        table.RenderControl(htw);
                        System.Web.HttpContext.Current.Response.Write(style);
                        System.Web.HttpContext.Current.Response.Write(sw.ToString());

                        HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                        HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                        HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static bool IsNumeric(object Expression)
        {
            if (Expression.ToString().Length == 1 && Expression.ToString()[0] == '0')
                return true;
            else if (Expression.ToString().Length > 1)
            {
                if (!Expression.ToString().Contains('.') && Expression.ToString()[0] == '0')
                    return false;
            }
            double retNum;
            bool isNum = Double.TryParse(Convert.ToString(Expression), System.Globalization.NumberStyles.Any, System.Globalization.NumberFormatInfo.InvariantInfo, out retNum);
            return isNum;
        }

        public static void ExportOnlyUsersOverview(string fileName, GridView gv)
        {
            //for left align
            string date = DateTime.Now.ToString("ddMMyyyy");
            string time = DateTime.Now.ToString("HH:mm");
            string style = @"<style> .text { mso-number-format:General;text-align:left;border:.5pt solid black;font-size:10.0pt; font-family:Calibri;} </style> ";
            System.Web.HttpContext.Current.Response.Clear();
            //System.Web.HttpContext.Current.Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", fileName + ".xls"));
            System.Web.HttpContext.Current.Response.AddHeader("content-disposition", string.Format("attachment; filename={0}_{1}_{2}", fileName, date, time + ".xls"));

            //System.Web.HttpContext.Current.Response.ContentType = "application/ms-excel";
            System.Web.HttpContext.Current.Response.ContentType = "application/vnd.xls";

            System.Web.HttpContext.Current.Response.Charset = "utf-8";
            System.Web.HttpContext.Current.Response.ContentEncoding = System.Text.Encoding.GetEncoding("windows-1250");

            using (System.IO.StringWriter sw = new System.IO.StringWriter())
            {
                using (HtmlTextWriter htw = new HtmlTextWriter(sw))
                {
                    // Create a form to contain the grid
                    Table table = new Table();
                    TableRow tr = new TableRow();
                    // add the header row to the table
                    if (gv.HeaderRow != null)
                    {
                        //for left align
                        //for (int columnIndex = 0; columnIndex < gv.HeaderRow.Cells.Count; columnIndex++) {
                        for (int columnIndex = 0; columnIndex < gv.Columns.Count; columnIndex++)
                        {
                            if (gv.Columns[columnIndex].Visible == true)
                            {
                                //gv.HeaderRow.Cells[columnIndex].Attributes.Add("class", "text");
                                //gv.HeaderRow.Cells[columnIndex].Style.Add("background-color", "#DBDBDB");
                                var tc = new TableCell() { Text = gv.Columns[columnIndex].HeaderText, BackColor = System.Drawing.Color.LightGray, CssClass = "text" };
                                tc.Style.Add("font-weight", "bold");
                                tr.Cells.Add(tc);
                            }
                            else
                            {
                                //gv.Columns.RemoveAt(columnIndex);
                                gv.HeaderRow.Cells[columnIndex].Style.Add("display", "none");
                                gv.HeaderRow.Cells[columnIndex].Text = string.Empty;
                            }
                        }
                        PrepareVisibleControlForExport(gv.HeaderRow);
                        //table.Rows.Add(gv.HeaderRow);
                        table.Rows.Add(tr);
                    }

                    // add each of the data rows to the table
                    foreach (GridViewRow row in gv.Rows)
                    {
                        if (row.RowType == DataControlRowType.DataRow)
                        {
                            ucLabel ltVendorName = (ucLabel)row.FindControl("ltVendorName");
                            ucLabel ltVendorNo = (ucLabel)row.FindControl("ltVendorNo");
                            ucLabel ltVendorEuropeanorLocal = (ucLabel)row.FindControl("ltVendorEuropeanorLocal");
                            ucLabel ltEUConsolidationCode = (ucLabel)row.FindControl("ltEUConsolidationCode");
                            ucLabel ltLocalConsolidationCode = (ucLabel)row.FindControl("ltLocalConsolidationCode");
                            ucLabel lblCreatedDate = (ucLabel)row.FindControl("lblCreatedDate");
                            ucLabel lblExpectedDeliveryDate = (ucLabel)row.FindControl("lblExpectedDeliveryDate");
                            ucLabel ltAssignedSite = (ucLabel)row.FindControl("ltAssignedSite");
                            ucLabel ltEnabledSite = (ucLabel)row.FindControl("ltEnabledSite");
                            ucLabel ltJobTitle = (ucLabel)row.FindControl("ltJobTitle");
                            ucLabel ltInventoryPOC = (ucLabel)row.FindControl("ltInventoryPOC");
                            ucLabel ltProcurementPOC = (ucLabel)row.FindControl("ltProcurementPOC");
                            ucLabel ltMerchandisingPOC = (ucLabel)row.FindControl("ltMerchandisingPOC");
                            ucLabel ltExecutivePOC = (ucLabel)row.FindControl("ltExecutivePOC");

                            tr = new TableRow();
                            PrepareVisibleControlForExport(row);
                            for (int columnIndex = 0; columnIndex < row.Cells.Count; columnIndex++)
                            {
                                if (gv.Columns[columnIndex].Visible == true)
                                {
                                    //tr.Cells.Add(new TableCell() { Text = row.Cells[columnIndex].Text });
                                    //row.Cells[columnIndex].Attributes.Add("class", "text");

                                    #region Switch Condition ...

                                    switch (columnIndex)
                                    {
                                        case 3:
                                            if (ltVendorNo != null)
                                            {
                                                tr.Cells.Add(new TableCell() { Text = ltVendorNo.Text, CssClass = "text" });
                                            }
                                            break;

                                        case 4:
                                            if (ltVendorName != null)
                                            {
                                                tr.Cells.Add(new TableCell() { Text = ltVendorName.Text, CssClass = "text" });
                                            }
                                            break;

                                        case 5:
                                            if (ltVendorEuropeanorLocal != null)
                                            {
                                                tr.Cells.Add(new TableCell() { Text = ltVendorEuropeanorLocal.Text, CssClass = "text" });
                                            }
                                            break;

                                        case 6:
                                            if (ltLocalConsolidationCode != null)
                                            {
                                                tr.Cells.Add(new TableCell() { Text = ltLocalConsolidationCode.Text, CssClass = "text" });
                                            }
                                            break;

                                        case 7:
                                            if (ltEUConsolidationCode != null)
                                            {
                                                tr.Cells.Add(new TableCell() { Text = ltEUConsolidationCode.Text, CssClass = "text" });
                                            }
                                            break;

                                        case 16:
                                            if (lblCreatedDate != null)
                                            {
                                                tr.Cells.Add(new TableCell() { Text = lblCreatedDate.Text, CssClass = "text" });
                                            }
                                            break;

                                        case 17:
                                            if (lblExpectedDeliveryDate != null)
                                            {
                                                tr.Cells.Add(new TableCell() { Text = lblExpectedDeliveryDate.Text, CssClass = "text" });
                                            }
                                            break;

                                        case 19:
                                            if (ltAssignedSite != null)
                                            {
                                                tr.Cells.Add(new TableCell() { Text = ltAssignedSite.Text, CssClass = "text" });
                                            }
                                            break;

                                        case 20:
                                            if (ltEnabledSite != null)
                                            {
                                                tr.Cells.Add(new TableCell() { Text = ltEnabledSite.Text, CssClass = "text" });
                                            }
                                            break;

                                        case 21:
                                            if (ltJobTitle != null)
                                            {
                                                tr.Cells.Add(new TableCell() { Text = ltJobTitle.Text, CssClass = "text" });
                                            }
                                            break;

                                        case 26:
                                            if (ltInventoryPOC != null)
                                            {
                                                tr.Cells.Add(new TableCell() { Text = ltInventoryPOC.Text, CssClass = "text" });
                                            }
                                            break;

                                        case 27:
                                            if (ltProcurementPOC != null)
                                            {
                                                tr.Cells.Add(new TableCell() { Text = ltProcurementPOC.Text, CssClass = "text" });
                                            }
                                            break;

                                        case 28:
                                            if (ltMerchandisingPOC != null)
                                            {
                                                tr.Cells.Add(new TableCell() { Text = ltMerchandisingPOC.Text, CssClass = "text" });
                                            }
                                            break;

                                        case 29:
                                            if (ltExecutivePOC != null)
                                            {
                                                tr.Cells.Add(new TableCell() { Text = ltExecutivePOC.Text, CssClass = "text" });
                                            }
                                            break;

                                        default:
                                            tr.Cells.Add(new TableCell() { Text = row.Cells[columnIndex].Text, CssClass = "text" });
                                            break;
                                    }

                                    #endregion Switch Condition ...
                                }
                                else
                                {
                                    row.Cells[columnIndex].Style.Add("display", "none");
                                }
                            }
                            //table.Rows.Add(row);
                            table.Rows.Add(tr);
                        }
                    }

                    // add the footer row to the table
                    if (gv.FooterRow != null)
                    {
                        PrepareVisibleControlForExport(gv.FooterRow);
                        //table.Rows.Add(gv.FooterRow);
                    }

                    //// render the table into the htmlwriter
                    //for (int iRow = 0; iRow < table.Rows.Count; iRow++) {
                    //    table.Rows[iRow].Cells[0].Visible = false;
                    //}

                    table.RenderControl(htw);
                    System.Web.HttpContext.Current.Response.Write(style);
                    System.Web.HttpContext.Current.Response.Write(sw.ToString());
                    System.Web.HttpContext.Current.Response.End();
                }
            }
        }

        private static void PrepareControlForExport(Control control, int iRow = 0, bool isHideSecondColumn = false)
        {
            for (int i = 0; i < control.Controls.Count; i++)
            {
                Control current = control.Controls[i];

                if (current is Label)
                {
                    control.Controls.Remove(current);
                    control.Controls.AddAt(i, new LiteralControl((current as Label).Text));
                }
                if (current is LinkButton)
                {
                    control.Controls.Remove(current);
                    control.Controls.AddAt(i, new LiteralControl((current as LinkButton).Text));
                }
                else if (current is ImageButton)
                {
                    control.Controls.Remove(current);
                    control.Controls.AddAt(i, new LiteralControl((current as ImageButton).AlternateText));
                }
                else if (current is HyperLink)
                {
                    control.Controls.Remove(current);
                    control.Controls.AddAt(i, new LiteralControl((current as HyperLink).Text));
                }
                else if (current is DropDownList)
                {
                    control.Controls.Remove(current);
                    control.Controls.AddAt(i, new LiteralControl((current as DropDownList).SelectedItem.Text));
                }
                else if (current is CheckBox)
                {
                    control.Controls.Remove(current);
                    if (isHideSecondColumn == true)
                    { }
                    else
                    {
                        control.Controls.AddAt(i, new LiteralControl((current as CheckBox).Checked ? "True" : "False"));
                    }
                }
                else if (current is RadioButton)
                {
                    control.Controls.Remove(current);
                    if (isHideSecondColumn == true)
                    { }
                    else
                    {
                        control.Controls.AddAt(i, new LiteralControl((current as RadioButton).Checked ? "True" : "False"));
                    }
                }
                else if (current is HiddenField)
                {
                    control.Controls.Remove(current);
                    //control.Controls.AddAt(i, new LiteralControl((current as HiddenField).Value));
                }
                else if (current is Button)
                {
                    control.Controls.Remove(current);
                    // control.Controls.AddAt(i, new LiteralControl((current as Button).Text));
                }
                else if (current is TextBox)
                {
                    control.Controls.Remove(current);
                    control.Controls.AddAt(i, new LiteralControl((current as TextBox).Text));
                }

                if (current.HasControls())
                {
                    PrepareControlForExport(current, isHideSecondColumn: isHideSecondColumn);
                }
            }
        }

        private static void PrepareVisibleControlForExport(Control control)
        {
            for (int i = 0; i < control.Controls.Count; i++)
            {
                Control current = control.Controls[i];
                current.ID = "ltExcelControl" + i.ToString();
                if (current is Label)
                {
                    var v = (Label)current;
                    if (!string.IsNullOrEmpty(v.Text))
                    {
                        control.Controls.Remove(current);
                        control.Controls.AddAt(i, new LiteralControl((current as Label).Text));
                    }
                }
                if (current is LinkButton)
                {
                    control.Controls.Remove(current);
                    control.Controls.AddAt(i, new LiteralControl((current as LinkButton).Text));
                }
                else if (current is ImageButton)
                {
                    control.Controls.Remove(current);
                    control.Controls.AddAt(i, new LiteralControl((current as ImageButton).AlternateText));
                }
                else if (current is HyperLink)
                {
                    control.Controls.Remove(current);
                    control.Controls.AddAt(i, new LiteralControl((current as HyperLink).Text));
                }
                else if (current is DropDownList)
                {
                    control.Controls.Remove(current);
                    control.Controls.AddAt(i, new LiteralControl((current as DropDownList).SelectedItem.Text));
                }
                else if (current is CheckBox)
                {
                    control.Controls.Remove(current);
                    control.Controls.AddAt(i, new LiteralControl((current as CheckBox).Checked ? "True" : "False"));
                }
                else if (current is RadioButton)
                {
                    control.Controls.Remove(current);
                    control.Controls.AddAt(i, new LiteralControl((current as RadioButton).Checked ? "True" : "False"));
                }
                else if (current is HiddenField)
                {
                    control.Controls.Remove(current);
                    //control.Controls.AddAt(i, new LiteralControl((current as HiddenField).Value));
                }
                else if (current is Button)
                {
                    control.Controls.Remove(current);
                    // control.Controls.AddAt(i, new LiteralControl((current as Button).Text));
                }

                if (current.HasControls())
                {
                    PrepareVisibleControlForExport(current);
                }
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;

namespace WebLocalize
{
    public class Resource
    {
        //public static List<MAS_LanguageTagsBE> lstTags = new List<MAS_LanguageTagsBE>();

        //public static string getGlobalResourceValueOld(string key, string UserLanguage = "")
        //{
        //    string ResourceValues = string.Empty;

        //    try
        //    {
        //        if (System.Web.HttpRuntime.Cache["LanguageTagslist"] != null)
        //        {
        //            if (lstTags == null || lstTags.Count <= 0)
        //            lstTags =(List<MAS_LanguageTagsBE>)System.Web.HttpRuntime.Cache["LanguageTagslist"];

        //            if (UserLanguage == "")
        //                UserLanguage = getUserLanguage();

        //            if (UserLanguage == "")
        //                UserLanguage = "English";

        //            List<MAS_LanguageTagsBE> lstResult = lstTags.Where(x => x.LanguageKey.ToLower() == key.ToLower()).ToList();


        //            if (lstResult != null && lstResult.Count > 0)
        //            {
        //                //Type type = lstResult[0].GetType();
        //                //PropertyInfo prop = type.GetProperty(UserLanguage);
        //                //if (prop != null)
        //                //{
        //                //    ResourceValues = Convert.ToString(prop.GetValue(lstResult[0], null));
        //                //}

        //                switch (UserLanguage) {
        //                    case "English":
        //                    ResourceValues = lstResult[0].English;
        //                    break;
        //                    case "French":
        //                    ResourceValues = lstResult[0].French;
        //                    break;
        //                    case "German":
        //                    ResourceValues = lstResult[0].German;
        //                    break;
        //                    case "Spanish":
        //                    ResourceValues = lstResult[0].Spanish;
        //                    break;
        //                    case "Italian":
        //                    ResourceValues = lstResult[0].Italian;
        //                    break;
        //                    case "Dutch":
        //                    ResourceValues = lstResult[0].Dutch;
        //                    break;
        //                    case "Czech":
        //                    ResourceValues = lstResult[0].Czech;
        //                    break;
        //                }
        //            }

        //            lstResult = null;
        //            //lstTags = null;
        //        }
        //    }
        //    catch (Exception ex)
        //    {

        //    }
        //    return ResourceValues;
        //}

        public static Dictionary<string, string> dictionaryEnglish = new Dictionary<string, string>();
        public static Dictionary<string, string> dictionaryFrench = new Dictionary<string, string>();
        public static Dictionary<string, string> dictionaryGerman = new Dictionary<string, string>();
        public static Dictionary<string, string> dictionaryDutch = new Dictionary<string, string>();
        public static Dictionary<string, string> dictionarySpanish = new Dictionary<string, string>();
        public static Dictionary<string, string> dictionaryItalian = new Dictionary<string, string>();
        public static Dictionary<string, string> dictionaryCzech = new Dictionary<string, string>();

        public static string getGlobalResourceValue(string key, string UserLanguage = "")
        {
            string ResourceValues = string.Empty;
            key = key.ToLower();
            try
            {

                if (UserLanguage == "")
                    UserLanguage = getUserLanguage();

                if (UserLanguage == "")
                    UserLanguage = "English";



                switch (UserLanguage)
                {
                    case "English":
                        if (System.Web.HttpRuntime.Cache["LanguageTagsEnglish"] != null)
                        {
                            if (dictionaryEnglish == null || dictionaryEnglish.Keys.Count <= 0)
                                dictionaryEnglish = (Dictionary<string, string>)System.Web.HttpRuntime.Cache["LanguageTagsEnglish"];
                            if (dictionaryEnglish.ContainsKey(key))
                            {
                                ResourceValues = dictionaryEnglish[key];
                            }
                        }
                        break;
                    case "French":
                        if (System.Web.HttpRuntime.Cache["LanguageTagsFrench"] != null)
                        {
                            if (dictionaryFrench == null || dictionaryFrench.Keys.Count <= 0)
                                dictionaryFrench = (Dictionary<string, string>)System.Web.HttpRuntime.Cache["LanguageTagsFrench"];
                            if (dictionaryFrench.ContainsKey(key))
                            {
                                ResourceValues = dictionaryFrench[key];
                            }
                        }
                        break;
                    case "German":
                        if (System.Web.HttpRuntime.Cache["LanguageTagsGerman"] != null)
                        {
                            if (dictionaryGerman == null || dictionaryGerman.Keys.Count <= 0)
                                dictionaryGerman = (Dictionary<string, string>)System.Web.HttpRuntime.Cache["LanguageTagsGerman"];
                            if (dictionaryGerman.ContainsKey(key))
                            {
                                ResourceValues = dictionaryGerman[key];
                            }
                        }
                        break;
                    case "Spanish":
                        if (System.Web.HttpRuntime.Cache["LanguageTagsSpanish"] != null)
                        {
                            if (dictionarySpanish == null || dictionarySpanish.Keys.Count <= 0)
                                dictionarySpanish = (Dictionary<string, string>)System.Web.HttpRuntime.Cache["LanguageTagsSpanish"];
                            if (dictionarySpanish.ContainsKey(key))
                            {
                                ResourceValues = dictionarySpanish[key];
                            }
                        }
                        break;
                    case "Italian":
                        if (System.Web.HttpRuntime.Cache["LanguageTagsItalian"] != null)
                        {
                            if (dictionaryItalian == null || dictionaryItalian.Keys.Count <= 0)
                                dictionaryItalian = (Dictionary<string, string>)System.Web.HttpRuntime.Cache["LanguageTagsItalian"];
                            if (dictionaryItalian.ContainsKey(key))
                            {
                                ResourceValues = dictionaryItalian[key];
                            }
                        }
                        break;
                    case "Dutch":
                        if (System.Web.HttpRuntime.Cache["LanguageTagsDutch"] != null)
                        {
                            if (dictionaryDutch == null || dictionaryDutch.Keys.Count <= 0)
                                dictionaryDutch = (Dictionary<string, string>)System.Web.HttpRuntime.Cache["LanguageTagsDutch"];
                            if (dictionaryDutch.ContainsKey(key))
                            {
                                ResourceValues = dictionaryDutch[key];
                            }
                        }
                        break;
                    case "Czech":
                        if (System.Web.HttpRuntime.Cache["LanguageTagsCzech"] != null)
                        {
                            if (dictionaryCzech == null || dictionaryCzech.Keys.Count <= 0)
                                dictionaryCzech = (Dictionary<string, string>)System.Web.HttpRuntime.Cache["LanguageTagsCzech"];
                            if (dictionaryCzech.ContainsKey(key))
                            {
                                ResourceValues = dictionaryCzech[key];
                            }
                        }
                        break;
                }
                //lstResult = null;
                //lstTags = null;
            }
            catch (Exception)
            {

            }
            return ResourceValues;
        }

        //public static string getGlobalResourceValueByLangIDOLD(string key, int LanguageID) {
        //    string ResourceValues = string.Empty;
        //    string UserLanguage = string.Empty;

        //    try {
        //        if (System.Web.HttpRuntime.Cache["LanguageTagslist"] != null) {

        //            if (lstTags == null || lstTags.Count <= 0)
        //                lstTags = (List<MAS_LanguageTagsBE>)System.Web.HttpRuntime.Cache["LanguageTagslist"];


        //            UserLanguage = getUICultureLanguageById(LanguageID);

        //            if (UserLanguage == "")
        //                UserLanguage = "English";

        //            List<MAS_LanguageTagsBE> lstResult = lstTags.Where(x => x.LanguageKey.ToLower() == key.ToLower()).ToList();

        //            if (lstResult != null && lstResult.Count > 0) {
        //                //Type type = lstResult[0].GetType();
        //                //PropertyInfo prop = type.GetProperty(UserLanguage);
        //                //if (prop != null) {
        //                //    ResourceValues = Convert.ToString(prop.GetValue(lstResult[0], null));
        //                //}

        //                switch (UserLanguage) {
        //                    case "English":
        //                    ResourceValues = lstResult[0].English;
        //                    break;
        //                    case "French":
        //                    ResourceValues = lstResult[0].French;
        //                    break;
        //                    case "German":
        //                    ResourceValues = lstResult[0].German;
        //                    break;
        //                    case "Spanish":
        //                    ResourceValues = lstResult[0].Spanish;
        //                    break;
        //                    case "Italian":
        //                    ResourceValues = lstResult[0].Italian;
        //                    break;
        //                    case "Dutch":
        //                    ResourceValues = lstResult[0].Dutch;
        //                    break;
        //                    case "Czech":
        //                    ResourceValues = lstResult[0].Czech;
        //                    break;
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex) {

        //    }
        //    return ResourceValues;
        //}

        public static string getGlobalResourceValueByLangID(string key, int LanguageID)
        {
            string ResourceValues = string.Empty;
            string UserLanguage = string.Empty;

            try
            {
                UserLanguage = getUICultureLanguageById(LanguageID);

                if (UserLanguage == "")
                    UserLanguage = "English";

                ResourceValues = getGlobalResourceValue(key, UserLanguage);
            }
            catch (Exception)
            {
            }
            return ResourceValues;
        }

        public static string getUserLanguage()
        {
            string UserLanguage = string.Empty;
            try
            {
                if (!string.IsNullOrEmpty(System.Threading.Thread.CurrentThread.CurrentUICulture.Name))
                    UserLanguage = getUICultureLanguage(Convert.ToString(System.Threading.Thread.CurrentThread.CurrentUICulture.Name));
                else
                    UserLanguage = Convert.ToString(System.Web.HttpContext.Current.Session["UserLanguage"]);  //Convert.ToString(System.Web.HttpRuntime.Cache["UserLanguage"]); //
                return UserLanguage;
            }
            catch
            {
                return UserLanguage;
            }
        }


        private static string getUICultureLanguage(string cultureISO)
        {
            var culture = string.Empty;
            switch (cultureISO)
            {
                case clsConstants.EnglishISO:
                    culture = "English";
                    break;
                case clsConstants.FranceISO:
                    culture = "French";
                    break;
                case clsConstants.GermanyISO:
                    culture = "German";
                    break;
                case clsConstants.SpainISO:
                    culture = "Spanish";
                    break;
                case clsConstants.ItalyISO:
                    culture = "Italian";
                    break;
                case clsConstants.NederlandISO:
                    culture = "Dutch";
                    break;
                case clsConstants.CzechISO:
                    culture = "Czech";
                    break;

            }
            return culture;
        }

        private static string getUICultureLanguageById(int languageID)
        {

            var culture = string.Empty;
            switch (languageID)
            {
                case 1:
                    culture = "English"; // "en-US"; // English
                    break;
                case 2:
                    culture = "French"; // // France
                    break;
                case 3:
                    culture = "German"; ; //"de"; // German
                    break;
                case 4:
                    culture = "Dutch"; //"nl"; // Dutch
                    break;
                case 5:
                    culture = "Spanish"; //"es"; // Spanish
                    break;
                case 6:
                    culture = "Italian"; //"it"; // Italy
                    break;
                case 7:
                    culture = "Czech";//"cz"; // Czech
                    break;
                default:
                    culture = "English"; //"en-US"; // English
                    break;
            }
            return culture;
        }

    }
}

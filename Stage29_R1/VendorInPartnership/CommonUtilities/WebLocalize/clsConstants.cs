﻿namespace WebLocalize
{
    class clsConstants
    {
        /* ISO Language name */
        public const string EnglishISO = "en-US";
        public const string FranceISO = "fr";
        public const string GermanyISO = "de";
        public const string SpainISO = "es";
        public const string ItalyISO = "it";
        public const string NederlandISO = "nl";
        public const string CzechISO = "cs";
        public const string SwedenISO = "se";
    }
}

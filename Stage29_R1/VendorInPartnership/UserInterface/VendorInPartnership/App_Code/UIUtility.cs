﻿using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using BusinessEntities.ModuleBE.Languages;
using BusinessLogicLayer.ModuleBAL.Appointment.SiteSettings;
using BusinessLogicLayer.ModuleBAL.Languages;
using System;
using System.Collections.Generic;
using System.Web;
using WebUtilities;

public class UIUtility
{
    public const int SuperAdminUserId = 1;

    public static string GetODVendorUserName(string role, string userName, string loginID)
    {
        string strUsername = string.Empty;
        if (Convert.ToString(role).Trim().ToLower() == "vendor")
            strUsername = Convert.ToString(loginID);
        else
            strUsername = Convert.ToString(userName);

        return strUsername;
    }

    public static string GetDiscrepancyDisplayName(int? discrepancyTypeId)
    {
        string strType = string.Empty;
        if (discrepancyTypeId == null) { discrepancyTypeId = 0; }
        switch (discrepancyTypeId)
        {
            case 1:
                strType = WebCommon.getGlobalResourceValue("Overs");
                break;
            case 2:
                strType = WebCommon.getGlobalResourceValue("Shortage");
                break;
            case 3:
                strType = WebCommon.getGlobalResourceValue("GoodsReceivedDamaged");
                break;
            case 4:
                strType = WebCommon.getGlobalResourceValue("NoPurchaseorder");
                break;
            case 5:
                strType = WebCommon.getGlobalResourceValue("NoPaperwork");
                break;
            case 6:
                strType = WebCommon.getGlobalResourceValue("IncorrectProduct");
                break;
            case 7:
                strType = WebCommon.getGlobalResourceValue("PresentationIssue");
                break;
            case 8:
                strType = WebCommon.getGlobalResourceValue("IncorrectAddress");
                break;
            case 9:
                strType = WebCommon.getGlobalResourceValue("PaperworkAmended");
                break;
            case 10:
                strType = WebCommon.getGlobalResourceValue("WrongPackSize");
                break;
            case 11:
                strType = WebCommon.getGlobalResourceValue("FailPalletSpecification");
                break;
            case 12:
                strType = WebCommon.getGlobalResourceValue("QualityIssues");
                break;
            case 13:
                strType = WebCommon.getGlobalResourceValue("PrematureInvoiceReceipt");
                break;
            case 14:
                strType = WebCommon.getGlobalResourceValue("GenericDiscrepancy");
                break;
            case 15:
                strType = WebCommon.getGlobalResourceValue("ShuttleDiscrepancy");
                break;
            case 16:
                strType = WebCommon.getGlobalResourceValue("ReservationIssue");
                break;
            case 17:
                strType = WebCommon.getGlobalResourceValue("InvoiceDiscrepancy");
                break;
            case 18:
                strType = WebCommon.getGlobalResourceValue("SPInvoiceDiscrepancy");
                break;
            case 19:
                strType = WebCommon.getGlobalResourceValue("InvoiceQueryDiscrepancy");
                break;
            case 20:
                strType = WebCommon.getGlobalResourceValue("ItemNotOnPO");
                break;

        }
        return strType;
    }

    public static string GetUserPhoneNoofVendor(int vendorID, int siteId)
    {
        var userPhoneNo = string.Empty;
        var lstVendorUserContact = CommonPage.GetDiscrepancyContactDetailsOfVendor(vendorID, siteId);
        if (lstVendorUserContact != null && lstVendorUserContact.Count > 0)
        {
            var vendorUserContact = lstVendorUserContact.Find(ven => ven.PhoneNumber != string.Empty);
            if (vendorUserContact != null)
            {
                if (!string.IsNullOrEmpty(vendorUserContact.PhoneNumber))
                    userPhoneNo = vendorUserContact.PhoneNumber;
            }
        }
        return userPhoneNo;
    }

    /// <summary>
    /// Added by Gourvi on 21 Apr 2014 to check unauthorized usage of OD Pages.
    /// </summary>
    public static void PageValidateForODUser()
    {
        if (HttpContext.Current.Session["Role"].ToString().ToLower() == "vendor" || HttpContext.Current.Session["Role"].ToString().ToLower() == "carrier")
        {
            HttpContext.Current.Session.Abandon();
            HttpContext.Current.Response.Redirect("~/ModuleUI/Security/Login.aspx");
        }
    }

    public static List<MAS_LanguageTagsBE> GetAllResources()
    {
        var lstLanguageTags = new List<MAS_LanguageTagsBE>();
        try
        {
            MAS_LanguageTagsBE oMAS_LanguageTagsBE = new MAS_LanguageTagsBE();
            MAS_LanguageTagsBAL oMAS_LanguageTagsBAL = new MAS_LanguageTagsBAL();
            if (lstLanguageTags != null && lstLanguageTags.Count <= 0)
            {
                oMAS_LanguageTagsBE.Action = "GetAllLanguageTags";
                lstLanguageTags = oMAS_LanguageTagsBAL.GetAllLanguageTagsBAL(oMAS_LanguageTagsBE);
            }
        }
        catch (Exception) { }
        return lstLanguageTags;
    }

    public static string getGlobalResourceValue(string key, string UserLanguage)
    {
        string ResourceValues = string.Empty;
        try
        {

            ResourceValues = WebCommon.getGlobalResourceValue(key, UserLanguage);

            ////if (GlobalObjects.lstLanguageTags == null && GlobalObjects.lstLanguageTags.Count.Equals(0))
            //if (GlobalObjects.lstLanguageTags == null )
            //    GlobalObjects.lstLanguageTags = UIUtility.GetAllResources();

            //if (GlobalObjects.lstLanguageTags != null && GlobalObjects.lstLanguageTags.Count > 0)
            //{
            //    var lstResult = GlobalObjects.lstLanguageTags.Where(x => x.LanguageKey.ToLower() == key.ToLower()).ToList();
            //    if (lstResult != null && lstResult.Count > 0)
            //    {
            //        Type type = lstResult[0].GetType();
            //        PropertyInfo prop = type.GetProperty(UserLanguage);
            //        if (prop != null)
            //        {
            //            ResourceValues = Convert.ToString(prop.GetValue(lstResult[0], null));
            //        }
            //    }
            //}
        }

        catch (Exception)
        {

        }
        return ResourceValues;
    }

    public static string[] GetVendorEmailsWithLanguage(int VendorID, int SiteID)
    {
        string[] VendorData = new string[2];
        MASSIT_VendorBE oMASSIT_VendorBE = new MASSIT_VendorBE();
        APPSIT_VendorBAL oAPPSIT_VendorBAL = new APPSIT_VendorBAL();
        string VendorEmail = string.Empty;
        List<MASSIT_VendorBE> lstVendorDetails = null;

        oMASSIT_VendorBE.Action = "GetVendorEmailForBooking"; // from user table
        oMASSIT_VendorBE.SiteVendorID = VendorID;
        oMASSIT_VendorBE.SiteID = SiteID;

        lstVendorDetails = oAPPSIT_VendorBAL.GetVendorContactDetailsBAL(oMASSIT_VendorBE);
        if (lstVendorDetails != null && lstVendorDetails.Count > 0)
        {

            foreach (MASSIT_VendorBE item in lstVendorDetails)
            {
                if (item.Vendor.VendorContactEmail == null || item.Vendor.VendorContactEmail == "")
                {
                    VendorData = GetVendorDefaultEmailWithLanguage(VendorID);
                }
                else
                {
                    VendorData[0] += item.Vendor.VendorContactEmail.ToString() + ",";
                    VendorData[1] += item.Vendor.Language.ToString() + ",";
                }
            }
        }
        else
        {
            VendorData = GetVendorDefaultEmailWithLanguage(VendorID, SiteID);
        }
        return VendorData;
    }

    private static string[] GetVendorDefaultEmailWithLanguage(int VendorID)
    {
        string[] VendorData = new string[2];
        MASSIT_VendorBE oMASSIT_VendorBE = new MASSIT_VendorBE();
        APPSIT_VendorBAL oAPPSIT_VendorBAL = new APPSIT_VendorBAL();
        List<MASSIT_VendorBE> lstVendorDetails = null;
        string VendorEmail = string.Empty;
        oMASSIT_VendorBE.Action = "GetContactDetails"; // from Up_vendor table
        oMASSIT_VendorBE.SiteVendorID = VendorID;
        VendorData[0] = "";
        VendorData[1] = "";

        lstVendorDetails = oAPPSIT_VendorBAL.GetVendorContactDetailsBAL(oMASSIT_VendorBE);
        foreach (MASSIT_VendorBE item in lstVendorDetails)
        {
            if (!string.IsNullOrEmpty(item.Vendor.VendorContactEmail))
            {
                VendorData[0] += item.Vendor.VendorContactEmail.ToString() + ",";
                VendorData[1] += item.Vendor.Language.ToString() + ",";
            }
        }
        return VendorData;
    }

    private static string[] GetVendorDefaultEmailWithLanguageID(int VendorID)
    {
        string[] VendorData = new string[2];
        MASSIT_VendorBE oMASSIT_VendorBE = new MASSIT_VendorBE();
        APPSIT_VendorBAL oAPPSIT_VendorBAL = new APPSIT_VendorBAL();
        List<MASSIT_VendorBE> lstVendorDetails = null;
        string VendorEmail = string.Empty;
        oMASSIT_VendorBE.Action = "GetContactDetails"; // from Up_vendor table
        oMASSIT_VendorBE.SiteVendorID = VendorID;
        VendorData[0] = "";
        VendorData[1] = "";

        lstVendorDetails = oAPPSIT_VendorBAL.GetVendorContactDetailsBAL(oMASSIT_VendorBE);
        foreach (MASSIT_VendorBE item in lstVendorDetails)
        {
            if (!string.IsNullOrEmpty(item.Vendor.VendorContactEmail))
            {
                VendorData[0] += item.Vendor.VendorContactEmail.ToString() + ",";
                VendorData[1] += item.Vendor.LanguageID.ToString() + ",";
            }
        }
        if (!string.IsNullOrEmpty(VendorData[0]))
        {
            VendorData[0] = System.Configuration.ConfigurationManager.AppSettings["DefaultEmailAddress"];
            VendorData[1] = "1";
        }

        return VendorData;
    }

    private static string[] GetVendorDefaultEmailWithLanguage(int VendorID, int pSiteID)
    {
        string[] VendorData = new string[2];
        MASSIT_VendorBE oMASSIT_VendorBE = new MASSIT_VendorBE();
        APPSIT_VendorBAL oAPPSIT_VendorBAL = new APPSIT_VendorBAL();
        List<MASSIT_VendorBE> lstVendorDetails = null;
        string VendorEmail = string.Empty;
        oMASSIT_VendorBE.Action = "GetContactDetailsForBooking"; // from Up_vendor table
        oMASSIT_VendorBE.SiteVendorID = VendorID;
        oMASSIT_VendorBE.SiteID = pSiteID;
        VendorData[0] = "";
        VendorData[1] = "";

        lstVendorDetails = oAPPSIT_VendorBAL.GetVendorContactDetailsBAL(oMASSIT_VendorBE);
        foreach (MASSIT_VendorBE item in lstVendorDetails)
        {
            if (!string.IsNullOrEmpty(item.Vendor.VendorContactEmail))
            {
                VendorData[0] += item.Vendor.VendorContactEmail.ToString() + ",";
                VendorData[1] += item.Vendor.Language.ToString() + ",";
            }
        }
        if (!string.IsNullOrWhiteSpace(VendorData[0]) || VendorData[0] == "")
        {
            VendorData[0] = System.Configuration.ConfigurationManager.AppSettings["DefaultEmailAddress"];
            VendorData[1] = "English";
        }
        return VendorData;
    }

}
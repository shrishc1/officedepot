﻿using BusinessEntities.ModuleBE.Upload;
using BusinessLogicLayer.ModuleBAL.Upload;
using System;
using System.Collections.Generic;
using System.IO;

/// <summary>
/// Summary description for clsReceiptInformation
/// </summary>
public class clsReceiptInformation
{
    public clsReceiptInformation()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    public string UploadReceiptInformation(string receiptInformationFilePath, string receiptInformationFileName)
    {
        int TotalRecordCount = 0;
        int DataElementCount = 0;
        string individualColumnName = "";

        /// -------------- Added on 16 Feb 2013 ------------------------
        List<UP_DataImportErrorBE> UP_DataImportErrorBEList = new List<UP_DataImportErrorBE>();
        ImportDB importDB = new ImportDB();

        DateTime ImportDate = importDB.GetFileDate(receiptInformationFileName);
        /// ------------------------------------------------------------

        try
        {
            using (StreamReader oStreamReader = new StreamReader(receiptInformationFilePath, System.Text.Encoding.Default))
            {
                GlobalVariable.textFileLine = oStreamReader.ReadLine().ToLower();
                var columns = GlobalVariable.textFileLine.Split(GlobalVariable.delimiters);
                if (columns.Length != 10)
                {
                    return "Invalid File";
                }

                List<UP_ReceiptInformationBE> UP_ReceiptInformationBEList = new List<UP_ReceiptInformationBE>();

                while ((GlobalVariable.textFileLine = oStreamReader.ReadLine()) != null)
                {
                    DataElementCount = 0;
                    UP_ReceiptInformationBE oUP_ReceiptInformationBE = new UP_ReceiptInformationBE();

                    /// -------------- Added on 16 Feb 2013 ------------------------
                    UP_DataImportErrorBE oUP_DataImportErrorBE = new UP_DataImportErrorBE();
                    string errorMessage = string.Empty;
                    /// ------------------------------------------------------------

                    GlobalVariable.textFileFieldValues = GlobalVariable.textFileLine.Split(GlobalVariable.delimiters);
                    for (int i = 0; i <= GlobalVariable.textFileFieldValues.Length - 1; i++)
                    {

                        individualColumnName = columns[i];
                        switch (individualColumnName.ToLower())
                        {
                            case "viking_sku_no":
                                /// -------------- Added on 16 Feb 2013 ------------------------
                                GlobalVariable.textFileFieldValues[i] = GlobalVariable.textFileFieldValues[i].Replace("'", "''");
                                if (!string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[i]) && GlobalVariable.textFileFieldValues[i].Length > 15)
                                {
                                    errorMessage += importDB.GetErrorMessage(individualColumnName, GlobalVariable.textFileFieldValues[i], 15);
                                    oUP_ReceiptInformationBE.Viking_sku_no = GlobalVariable.textFileFieldValues[i].Substring(0, 15);
                                }
                                else
                                    oUP_ReceiptInformationBE.Viking_sku_no = GlobalVariable.textFileFieldValues[i];

                                /// ------------------------------------------------------------
                                DataElementCount++;
                                break;
                            case "od_sku_number":
                                /// -------------- Added on 16 Feb 2013 ------------------------
                                GlobalVariable.textFileFieldValues[i] = GlobalVariable.textFileFieldValues[i].Replace("'", "''");
                                if (!string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[i]) && GlobalVariable.textFileFieldValues[i].Length > 25)
                                {
                                    errorMessage += importDB.GetErrorMessage(individualColumnName, GlobalVariable.textFileFieldValues[i], 25);
                                    oUP_ReceiptInformationBE.OD_SKU_Number = GlobalVariable.textFileFieldValues[i].Substring(0, 25);
                                }
                                else
                                    oUP_ReceiptInformationBE.OD_SKU_Number = GlobalVariable.textFileFieldValues[i];
                                /// ------------------------------------------------------------
                                DataElementCount++;
                                break;
                            case "vendor_prod_code":
                                /// -------------- Added on 16 Feb 2013 ------------------------
                                GlobalVariable.textFileFieldValues[i] = GlobalVariable.textFileFieldValues[i].Replace("'", "''");
                                if (!string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[i]) && GlobalVariable.textFileFieldValues[i].Length > 50)
                                {
                                    errorMessage += importDB.GetErrorMessage(individualColumnName, GlobalVariable.textFileFieldValues[i], 50);
                                    oUP_ReceiptInformationBE.Vendor_prod_code = GlobalVariable.textFileFieldValues[i].Substring(0, 50);
                                }
                                else
                                    oUP_ReceiptInformationBE.Vendor_prod_code = GlobalVariable.textFileFieldValues[i];
                                /// ------------------------------------------------------------
                                DataElementCount++;
                                break;
                            case "warehouse":
                                /// -------------- Added on 16 Feb 2013 ------------------------
                                GlobalVariable.textFileFieldValues[i] = GlobalVariable.textFileFieldValues[i].Replace("'", "''");
                                if (!string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[i]) && GlobalVariable.textFileFieldValues[i].Length > 10)
                                {
                                    errorMessage += importDB.GetErrorMessage(individualColumnName, GlobalVariable.textFileFieldValues[i], 10);
                                    oUP_ReceiptInformationBE.Warehouse = GlobalVariable.textFileFieldValues[i].Substring(0, 10);
                                }
                                else
                                    oUP_ReceiptInformationBE.Warehouse = GlobalVariable.textFileFieldValues[i];
                                /// ------------------------------------------------------------
                                DataElementCount++;
                                break;
                            case "po_number":
                                /// -------------- Added on 16 Feb 2013 ------------------------
                                GlobalVariable.textFileFieldValues[i] = GlobalVariable.textFileFieldValues[i].Replace("'", "''");
                                if (!string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[i]) && GlobalVariable.textFileFieldValues[i].Length > 15)
                                {
                                    errorMessage += importDB.GetErrorMessage(individualColumnName, GlobalVariable.textFileFieldValues[i], 15);
                                    oUP_ReceiptInformationBE.PO_number = GlobalVariable.textFileFieldValues[i].Substring(0, 15);
                                }
                                else
                                    oUP_ReceiptInformationBE.PO_number = GlobalVariable.textFileFieldValues[i];
                                /// ------------------------------------------------------------
                                DataElementCount++;
                                break;
                            case "qty_receipted":
                                if (!string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[i].Replace("'", "''")))
                                    oUP_ReceiptInformationBE.Qty_receipted = Convert.ToInt32(RemoveDecimal(GlobalVariable.textFileFieldValues[i].Replace("'", "''")));
                                DataElementCount++;
                                break;
                            case "other":
                                /// -------------- Added on 16 Feb 2013 ------------------------
                                GlobalVariable.textFileFieldValues[i] = GlobalVariable.textFileFieldValues[i].Replace("'", "''");
                                if (!string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[i]) && GlobalVariable.textFileFieldValues[i].Length > 50)
                                {
                                    errorMessage += importDB.GetErrorMessage(individualColumnName, GlobalVariable.textFileFieldValues[i], 50);
                                    oUP_ReceiptInformationBE.Other = GlobalVariable.textFileFieldValues[i].Substring(0, 50);
                                }
                                else
                                    oUP_ReceiptInformationBE.Other = GlobalVariable.textFileFieldValues[i];
                                /// ------------------------------------------------------------
                                DataElementCount++;
                                break;
                            case "date":

                                if (!String.IsNullOrEmpty(GlobalVariable.textFileFieldValues[i].Replace("'", "''")))
                                {
                                    try
                                    {
                                        oUP_ReceiptInformationBE.Date = Convert.ToDateTime(RemoveDecimal(GlobalVariable.textFileFieldValues[i].Replace("'", "''")));
                                    }
                                    catch
                                    {
                                        errorMessage += individualColumnName + " has invalid date - " + GlobalVariable.textFileFieldValues[i] + " ";
                                    }
                                }

                                DataElementCount++;
                                break;

                            case "country":
                                /// -------------- Added on 16 Feb 2013 ------------------------
                                GlobalVariable.textFileFieldValues[i] = GlobalVariable.textFileFieldValues[i].Replace("'", "''");
                                if (!string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[i]) && GlobalVariable.textFileFieldValues[i].Length > 20)
                                {
                                    errorMessage += importDB.GetErrorMessage(individualColumnName, GlobalVariable.textFileFieldValues[i], 20);
                                    oUP_ReceiptInformationBE.Country = GlobalVariable.textFileFieldValues[i].Substring(0, 20);
                                }
                                else
                                    oUP_ReceiptInformationBE.Country = GlobalVariable.textFileFieldValues[i];
                                /// ------------------------------------------------------------
                                DataElementCount++;
                                break;
                            case "orgorddate":

                                if (!String.IsNullOrEmpty(GlobalVariable.textFileFieldValues[i].Replace("'", "''")))
                                {
                                    try
                                    {
                                        oUP_ReceiptInformationBE.ORGOrdDate = Convert.ToDateTime(RemoveDecimal(GlobalVariable.textFileFieldValues[i].Replace("'", "''")));
                                    }
                                    catch
                                    {
                                        errorMessage += individualColumnName + " has invalid date - " + GlobalVariable.textFileFieldValues[i] + " ";
                                    }
                                }
                                DataElementCount++;
                                break;
                            default:
                                break;
                        }
                        oUP_ReceiptInformationBE.UpdatedDate = ImportDate;
                    }

                    if (DataElementCount != 10)
                        return "Invalid File";
                    UP_ReceiptInformationBEList.Add(oUP_ReceiptInformationBE);

                    /// -------------- Added on 16 Feb 2013 ------------------------
                    if (!string.IsNullOrEmpty(errorMessage))
                    {
                        // oUP_DataImportErrorBE.ManualFileUploadID = Result;
                        oUP_DataImportErrorBE.ErrorDescription = errorMessage.Substring(0, errorMessage.Length - 1) + " in file " + receiptInformationFileName;
                        oUP_DataImportErrorBE.UpdatedDate = ImportDate;
                        UP_DataImportErrorBEList.Add(oUP_DataImportErrorBE);
                    }

                    /// ------------------------------------------------------------


                    TotalRecordCount += 1;
                }
                UP_ReceiptInformationBAL oUP_ReceiptInformationBAL = new UP_ReceiptInformationBAL();
                oUP_ReceiptInformationBAL.AddReceiptInformation(UP_ReceiptInformationBEList);
                UP_ReceiptInformationBEList = null;


            }

        }
        catch (Exception)
        {
            return "Invalid File";
        }

        try
        {
            UP_ManualFileUploadBE oUP_ManualFileUploadBE = new UP_ManualFileUploadBE();
            oUP_ManualFileUploadBE.Action = "InsertManualUpload";
            oUP_ManualFileUploadBE.DateUploaded = ImportDate;
            oUP_ManualFileUploadBE.DownloadedFilename = receiptInformationFileName;
            oUP_ManualFileUploadBE.UserID = Convert.ToInt32(System.Web.HttpContext.Current.Session["UserId"]);
            oUP_ManualFileUploadBE.TotalReceiptInformationRecord = TotalRecordCount;

            UP_ManualFileUploadBAL oUP_ManualFileUploadBAL = new UP_ManualFileUploadBAL();
            int? Result = oUP_ManualFileUploadBAL.addEditManualUploadBAL(oUP_ManualFileUploadBE);


            /// -------------- Added on 16 Feb 2013 ------------------------

            foreach (UP_DataImportErrorBE obj in UP_DataImportErrorBEList)
            {
                obj.ManualFileUploadID = Convert.ToInt32(Result);

            }
            UP_ReceiptInformationBAL oUP_ReceiptInformationBAL = new UP_ReceiptInformationBAL();
            oUP_ReceiptInformationBAL.AddReceiptInformationErrors(UP_DataImportErrorBEList);
            UP_DataImportErrorBEList = null;
            /// ------------------------------------------------------------

        }
        catch (Exception)
        {

        }

        return string.Empty;
    }

    private string RemoveDecimal(string val)
    {
        string retVal;
        retVal = RemoveSpace(val);

        if (retVal != null)
        {
            if (retVal.Contains("."))
            {
                retVal = retVal.Substring(0, retVal.IndexOf("."));
            }
            if (retVal.Contains("-"))
            {
                retVal = '-' + (retVal.Trim('-'));
            }
            if (retVal.Contains("/"))
            {
                string[] arrDate = retVal.Split('/');
                retVal = arrDate[2] + "-" + arrDate[1] + "-" + arrDate[0];
            }
        }
        return retVal;
    }

    private string RemoveSpace(string val)
    {
        string retVal;
        if (val != string.Empty)
            retVal = val.Trim(' ');
        else
            retVal = null;
        return retVal;
    }
}
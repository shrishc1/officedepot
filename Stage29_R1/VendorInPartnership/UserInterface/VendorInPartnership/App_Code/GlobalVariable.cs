﻿using System.Configuration;
using System.Data.SqlClient;

/// <summary>
/// Summary description for GlobalVariable
/// </summary>
public static class GlobalVariable
{

    public static SqlConnection sqlCon;
    public static SqlCommand sqlComm;
    public static string folderName102 = string.Empty;
    public static char[] delimiters = new char[] { '|' };
    public static string[] textFileFieldValues;
    public static string textFileLine = string.Empty;
    public static string columnValue = string.Empty;
    public static string columnDatatype = string.Empty;
    public static object queryReturn = null;

    public static int CommandTimeOutTime = 4200;

    static GlobalVariable()
    {
        string ConnStr;
        ConnStr = ConfigurationManager.AppSettings["sConn"];
        sqlCon = new SqlConnection(ConnStr);
        sqlCon.Open();

        sqlComm = new SqlCommand();
        sqlComm.Connection = sqlCon;
    }
}
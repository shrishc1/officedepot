﻿using BusinessEntities.ModuleBE.Upload;
using BusinessLogicLayer.ModuleBAL.Upload;
using System;
using System.Collections.Generic;
using System.IO;


/// <summary>
/// Summary description for clsSKUImport
/// </summary>
public class clsSKUImport
{
    public clsSKUImport()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public string UploadItem(string itemFilePath, string itemFileName, string UserID)
    {
        int TotalRecordCount = 0;
        int DataElementCount = 0;
        string individualColumnName = "";
        int intColumnCount = 35;

        /// -------------- Added on 16 Feb 2013 ------------------------
        List<UP_DataImportErrorBE> UP_DataImportErrorBEList = new List<UP_DataImportErrorBE>();
        ImportDB importDB = new ImportDB();

        DateTime ImportDate = importDB.GetFileDate(itemFileName); //new DateTime(); //
        /// ------------------------------------------------------------

        try
        {
            using (StreamReader oStreamReader = new StreamReader(itemFilePath, System.Text.Encoding.Default))
            {
                GlobalVariable.textFileLine = oStreamReader.ReadLine().ToLower();
                var columns = GlobalVariable.textFileLine.Split(GlobalVariable.delimiters);
                if (columns.Length != intColumnCount)
                {
                    return "Invalid File";
                }
                List<UP_SKUBE> UP_SKUBEList = new List<UP_SKUBE>();

                bool isImportDateset = false;

                while ((GlobalVariable.textFileLine = oStreamReader.ReadLine()) != null)
                {
                    DataElementCount = 0;
                    UP_SKUBE oUP_SKUBE = new UP_SKUBE();

                    /// -------------- Added on 16 Feb 2013 ------------------------
                    UP_DataImportErrorBE oUP_DataImportErrorBE = new UP_DataImportErrorBE();
                    string errorMessage = string.Empty;
                    /// ------------------------------------------------------------


                    GlobalVariable.textFileFieldValues = GlobalVariable.textFileLine.Split(GlobalVariable.delimiters);


                    if (isImportDateset == false)
                    {
                        for (int i = 0; i <= GlobalVariable.textFileFieldValues.Length - 1; i++)
                        {

                            individualColumnName = columns[i];


                            switch (individualColumnName.ToLower())
                            {
                                case "date1":

                                    if (!String.IsNullOrEmpty(GlobalVariable.textFileFieldValues[i].Replace("'", "''")))
                                    {
                                        try
                                        {
                                            ImportDate = GetDateDate(GlobalVariable.textFileFieldValues[i].Replace("'", "''"));
                                            isImportDateset = true;
                                        }
                                        catch
                                        {

                                        }
                                    }
                                    break;
                            }
                        }
                    }

                    //System.Text.StringBuilder sb = new System.Text.StringBuilder();
                    //sb.AppendLine("");
                    //sb.AppendLine("Diract _Sku " + GlobalVariable.textFileFieldValues[0].Replace("'", "''") + ":");

                    //Utilities.LogUtility.SaveTraceLogEntry(sb);
                    DateTime? tempDate = DateTime.Now;
                    for (int i = 0; i <= GlobalVariable.textFileFieldValues.Length - 1; i++)
                    {

                        individualColumnName = columns[i];


                        switch (individualColumnName.ToLower())
                        {
                            case "direct_sku":
                                /// -------------- Added on 16 Feb 2013 ------------------------
                                GlobalVariable.textFileFieldValues[i] = GlobalVariable.textFileFieldValues[i].Replace("'", "''");
                                if (!string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[i]) && GlobalVariable.textFileFieldValues[i].Length > 15)
                                {
                                    errorMessage += importDB.GetErrorMessage(individualColumnName, GlobalVariable.textFileFieldValues[i], 15);
                                    oUP_SKUBE.Direct_SKU = GlobalVariable.textFileFieldValues[i].Substring(0, 15);
                                }
                                else
                                    oUP_SKUBE.Direct_SKU = GlobalVariable.textFileFieldValues[i];
                                /// -----------------------------------------------------------
                                DataElementCount++;
                                break;
                            case "od_sku_no":
                                /// -------------- Added on 16 Feb 2013 ------------------------
                                GlobalVariable.textFileFieldValues[i] = GlobalVariable.textFileFieldValues[i].Replace("'", "''");
                                if (!string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[i]) && GlobalVariable.textFileFieldValues[i].Length > 25)
                                {
                                    errorMessage += importDB.GetErrorMessage(individualColumnName, GlobalVariable.textFileFieldValues[i], 25);
                                    oUP_SKUBE.OD_SKU_NO = GlobalVariable.textFileFieldValues[i].Substring(0, 25);
                                }
                                else
                                    oUP_SKUBE.OD_SKU_NO = GlobalVariable.textFileFieldValues[i];
                                /// -----------------------------------------------------------
                                DataElementCount++;
                                break;
                            case "description":
                                /// -------------- Added on 16 Feb 2013 ------------------------
                                GlobalVariable.textFileFieldValues[i] = GlobalVariable.textFileFieldValues[i].Replace("'", "''");
                                if (!string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[i]) && GlobalVariable.textFileFieldValues[i].Length > 250)
                                {
                                    errorMessage += importDB.GetErrorMessage(individualColumnName, GlobalVariable.textFileFieldValues[i], 250);
                                    oUP_SKUBE.DESCRIPTION = GlobalVariable.textFileFieldValues[i].Substring(0, 250);
                                }
                                else
                                    oUP_SKUBE.DESCRIPTION = GlobalVariable.textFileFieldValues[i];
                                /// -----------------------------------------------------------
                                DataElementCount++;
                                break;
                            case "vendor_code":
                                /// -------------- Added on 16 Feb 2013 ------------------------
                                GlobalVariable.textFileFieldValues[i] = GlobalVariable.textFileFieldValues[i].Replace("'", "''");
                                if (!string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[i]) && GlobalVariable.textFileFieldValues[i].Length > 50)
                                {
                                    errorMessage += importDB.GetErrorMessage(individualColumnName, GlobalVariable.textFileFieldValues[i], 50);
                                    oUP_SKUBE.Vendor_Code = GlobalVariable.textFileFieldValues[i].Substring(0, 50);
                                }
                                else
                                    oUP_SKUBE.Vendor_Code = GlobalVariable.textFileFieldValues[i];
                                /// ------------------------------------------------------------
                                DataElementCount++;
                                break;
                            case "warehouse":
                                /// -------------- Added on 16 Feb 2013 ------------------------
                                GlobalVariable.textFileFieldValues[i] = GlobalVariable.textFileFieldValues[i].Replace("'", "''");
                                if (!string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[i]) && GlobalVariable.textFileFieldValues[i].Length > 10)
                                {
                                    errorMessage += importDB.GetErrorMessage(individualColumnName, GlobalVariable.textFileFieldValues[i], 10);
                                    oUP_SKUBE.Warehouse = GlobalVariable.textFileFieldValues[i].Substring(0, 10);
                                }
                                else
                                    oUP_SKUBE.Warehouse = GlobalVariable.textFileFieldValues[i];
                                /// ------------------------------------------------------------
                                DataElementCount++;
                                break;
                            case "date1":
                                /// -------------- Added on 16 Feb 2013 ------------------------
                                //GlobalVariable.textFileFieldValues[i] = GlobalVariable.textFileFieldValues[i].Replace("'", "''");
                                //if (!string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[i]) && GlobalVariable.textFileFieldValues[i].Length > 25)
                                //{
                                //    errorMessage += importDB.GetErrorMessage(individualColumnName, GlobalVariable.textFileFieldValues[i], 25);
                                //    oUP_SKUBE.Date = GlobalVariable.textFileFieldValues[i].Substring(0, 25);
                                //}
                                //else
                                //    oUP_SKUBE.Date = GlobalVariable.textFileFieldValues[i];

                                if (!String.IsNullOrEmpty(GlobalVariable.textFileFieldValues[i].Replace("'", "''")))
                                {
                                    try
                                    {
                                        tempDate = Convert.ToDateTime(RemoveDecimal(GlobalVariable.textFileFieldValues[i]).Replace("'", "''"));
                                        oUP_SKUBE.Date = GlobalVariable.textFileFieldValues[i].Replace("'", "''");
                                    }
                                    catch
                                    {
                                        errorMessage += individualColumnName + " has invalid date - " + GlobalVariable.textFileFieldValues[i] + " ";
                                    }
                                }
                                /// ------------------------------------------------------------
                                DataElementCount++;
                                break;
                            case "qty_sold_yesterday":
                                oUP_SKUBE.Qty_Sold_Yesterday = RemoveSpace(GlobalVariable.textFileFieldValues[i].Replace("'", "''"));
                                DataElementCount++;
                                break;
                            case "qty_on_hand":
                                oUP_SKUBE.Qty_On_Hand = RemoveSpace(GlobalVariable.textFileFieldValues[i].Replace("'", "''"));
                                DataElementCount++;
                                break;
                            case "qty_on_backorder":
                                oUP_SKUBE.qty_on_backorder = RemoveSpace(GlobalVariable.textFileFieldValues[i].Replace("'", "''"));
                                DataElementCount++;
                                break;
                            case "balance":
                                if (!string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[i].Replace("'", "''")))
                                    oUP_SKUBE.Balance = Convert.ToInt32(RemoveDecimal(GlobalVariable.textFileFieldValues[i].Replace("'", "''")));
                                DataElementCount++;
                                break;
                            case "item_val":
                                oUP_SKUBE.Item_Val = RemoveSpace(GlobalVariable.textFileFieldValues[i].Replace("'", "''"));
                                DataElementCount++;
                                break;
                            case "item_val_gbp":
                                oUP_SKUBE.Item_Val = GlobalVariable.textFileFieldValues[i].Replace("'", "''");
                                DataElementCount++;
                                break;
                            case "valuated_stock":
                                /// -------------- Added on 16 Feb 2013 ------------------------
                                GlobalVariable.textFileFieldValues[i] = RemoveSpace(GlobalVariable.textFileFieldValues[i].Replace("'", "''"));
                                if (!string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[i]) && GlobalVariable.textFileFieldValues[i].Length > 10)
                                {
                                    errorMessage += importDB.GetErrorMessage(individualColumnName, GlobalVariable.textFileFieldValues[i], 10);
                                    oUP_SKUBE.Valuated_Stock = GlobalVariable.textFileFieldValues[i].Substring(0, 10);
                                }
                                else
                                    oUP_SKUBE.Valuated_Stock = GlobalVariable.textFileFieldValues[i];
                                /// ------------------------------------------------------------
                                DataElementCount++;
                                break;
                            case "currency":
                                /// -------------- Added on 16 Feb 2013 ------------------------
                                GlobalVariable.textFileFieldValues[i] = GlobalVariable.textFileFieldValues[i].Replace("'", "''");
                                if (!string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[i]) && GlobalVariable.textFileFieldValues[i].Length > 10)
                                {
                                    errorMessage += importDB.GetErrorMessage(individualColumnName, GlobalVariable.textFileFieldValues[i], 10);
                                    oUP_SKUBE.Currency = GlobalVariable.textFileFieldValues[i].Substring(0, 10);
                                }
                                else
                                    oUP_SKUBE.Currency = GlobalVariable.textFileFieldValues[i];
                                /// ------------------------------------------------------------
                                DataElementCount++;
                                break;
                            case "vendor_no":
                                /// -------------- Added on 16 Feb 2013 ------------------------
                                GlobalVariable.textFileFieldValues[i] = GlobalVariable.textFileFieldValues[i].Replace("'", "''");
                                if (!string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[i]) && GlobalVariable.textFileFieldValues[i].Length > 15)
                                {
                                    errorMessage += importDB.GetErrorMessage(individualColumnName, GlobalVariable.textFileFieldValues[i], 15);
                                    oUP_SKUBE.Vendor_no = GlobalVariable.textFileFieldValues[i].Substring(0, 15);
                                }
                                else
                                    oUP_SKUBE.Vendor_no = GlobalVariable.textFileFieldValues[i];
                                /// ------------------------------------------------------------
                                DataElementCount++;
                                break;
                            case "vendor_name":
                                /// -------------- Added on 16 Feb 2013 ------------------------
                                GlobalVariable.textFileFieldValues[i] = GlobalVariable.textFileFieldValues[i].Replace("'", "''");
                                if (!string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[i]) && GlobalVariable.textFileFieldValues[i].Length > 50)
                                {
                                    errorMessage += importDB.GetErrorMessage(individualColumnName, GlobalVariable.textFileFieldValues[i], 50);
                                    oUP_SKUBE.Vendor_Name = GlobalVariable.textFileFieldValues[i].Substring(0, 50);
                                }
                                else
                                    oUP_SKUBE.Vendor_Name = GlobalVariable.textFileFieldValues[i];
                                /// ------------------------------------------------------------
                                DataElementCount++;
                                break;
                            case "subvndr":
                                /// -------------- Added on 16 Feb 2013 ------------------------
                                GlobalVariable.textFileFieldValues[i] = GlobalVariable.textFileFieldValues[i].Replace("'", "''");
                                if (!string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[i]) && GlobalVariable.textFileFieldValues[i].Length > 5)
                                {
                                    errorMessage += importDB.GetErrorMessage(individualColumnName, GlobalVariable.textFileFieldValues[i], 5);
                                    oUP_SKUBE.subvndr = GlobalVariable.textFileFieldValues[i].Substring(0, 5);
                                }
                                else
                                    oUP_SKUBE.subvndr = GlobalVariable.textFileFieldValues[i];
                                /// ------------------------------------------------------------
                                DataElementCount++;
                                break;
                            case "product_lead_time":
                                oUP_SKUBE.Product_Lead_time = RemoveDecimal(GlobalVariable.textFileFieldValues[i].Replace("'", "''"));
                                DataElementCount++;
                                break;
                            case "leadtime_variance":
                                oUP_SKUBE.Leadtime_Variance = RemoveDecimal(GlobalVariable.textFileFieldValues[i].Replace("'", "''"));
                                DataElementCount++;
                                break;
                            case "uom":
                                /// -------------- Added on 16 Feb 2013 ------------------------
                                GlobalVariable.textFileFieldValues[i] = GlobalVariable.textFileFieldValues[i].Replace("'", "''");
                                if (!string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[i]) && GlobalVariable.textFileFieldValues[i].Length > 5)
                                {
                                    errorMessage += importDB.GetErrorMessage(individualColumnName, GlobalVariable.textFileFieldValues[i], 5);
                                    oUP_SKUBE.UOM = GlobalVariable.textFileFieldValues[i].Substring(0, 5);
                                }
                                else
                                    oUP_SKUBE.UOM = GlobalVariable.textFileFieldValues[i];
                                /// ------------------------------------------------------------
                                DataElementCount++;
                                break;
                            case "buyer_no":
                                /// -------------- Added on 16 Feb 2013 ------------------------
                                GlobalVariable.textFileFieldValues[i] = GlobalVariable.textFileFieldValues[i].Replace("'", "''");
                                if (!string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[i]) && GlobalVariable.textFileFieldValues[i].Length > 20)
                                {
                                    errorMessage += importDB.GetErrorMessage(individualColumnName, GlobalVariable.textFileFieldValues[i], 20);
                                    oUP_SKUBE.Buyer_no = GlobalVariable.textFileFieldValues[i].Substring(0, 20);
                                }
                                else
                                    oUP_SKUBE.Buyer_no = GlobalVariable.textFileFieldValues[i];
                                /// ------------------------------------------------------------
                                DataElementCount++;
                                break;
                            case "item_category":
                                /// -------------- Added on 16 Feb 2013 ------------------------
                                GlobalVariable.textFileFieldValues[i] = GlobalVariable.textFileFieldValues[i].Replace("'", "''");
                                if (!string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[i]) && GlobalVariable.textFileFieldValues[i].Length > 10)
                                {
                                    errorMessage += importDB.GetErrorMessage(individualColumnName, GlobalVariable.textFileFieldValues[i], 10);
                                    oUP_SKUBE.Item_Category = GlobalVariable.textFileFieldValues[i].Substring(0, 10);
                                }
                                else
                                    oUP_SKUBE.Item_Category = GlobalVariable.textFileFieldValues[i];
                                /// ------------------------------------------------------------
                                DataElementCount++;
                                break;
                            case "item_classification":
                                /// -------------- Added on 16 Feb 2013 ------------------------
                                GlobalVariable.textFileFieldValues[i] = GlobalVariable.textFileFieldValues[i].Replace("'", "''");
                                if (!string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[i]) && GlobalVariable.textFileFieldValues[i].Length > 5)
                                {
                                    errorMessage += importDB.GetErrorMessage(individualColumnName, GlobalVariable.textFileFieldValues[i], 5);
                                    oUP_SKUBE.Item_classification = GlobalVariable.textFileFieldValues[i].Substring(0, 5);
                                }
                                else
                                    oUP_SKUBE.Item_classification = GlobalVariable.textFileFieldValues[i];
                                /// ------------------------------------------------------------
                                DataElementCount++;
                                break;
                            case "new_item":
                                /// -------------- Added on 16 Feb 2013 ------------------------
                                GlobalVariable.textFileFieldValues[i] = GlobalVariable.textFileFieldValues[i].Replace("'", "''");
                                if (!string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[i]) && GlobalVariable.textFileFieldValues[i].Length > 15)
                                {
                                    errorMessage += importDB.GetErrorMessage(individualColumnName, GlobalVariable.textFileFieldValues[i], 15);
                                    oUP_SKUBE.New_Item = GlobalVariable.textFileFieldValues[i].Substring(0, 15);
                                }
                                else
                                    oUP_SKUBE.New_Item = GlobalVariable.textFileFieldValues[i];
                                /// ------------------------------------------------------------
                                DataElementCount++;
                                break;
                            case "icasun":
                                /// -------------- Added on 16 Feb 2013 ------------------------
                                GlobalVariable.textFileFieldValues[i] = GlobalVariable.textFileFieldValues[i].Replace("'", "''");
                                if (!string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[i]) && GlobalVariable.textFileFieldValues[i].Length > 15)
                                {
                                    errorMessage += importDB.GetErrorMessage(individualColumnName, GlobalVariable.textFileFieldValues[i], 15);
                                    oUP_SKUBE.ICASUN = GlobalVariable.textFileFieldValues[i].Substring(0, 15);
                                }
                                else
                                    oUP_SKUBE.ICASUN = GlobalVariable.textFileFieldValues[i];
                                /// ------------------------------------------------------------
                                DataElementCount++;
                                break;
                            case "ilayun":
                                /// -------------- Added on 16 Feb 2013 ------------------------
                                GlobalVariable.textFileFieldValues[i] = GlobalVariable.textFileFieldValues[i].Replace("'", "''");
                                if (!string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[i]) && GlobalVariable.textFileFieldValues[i].Length > 15)
                                {
                                    errorMessage += importDB.GetErrorMessage(individualColumnName, GlobalVariable.textFileFieldValues[i], 15);
                                    oUP_SKUBE.ILAYUN = GlobalVariable.textFileFieldValues[i].Substring(0, 15);
                                }
                                else
                                    oUP_SKUBE.ILAYUN = GlobalVariable.textFileFieldValues[i];
                                /// ------------------------------------------------------------
                                DataElementCount++;
                                break;
                            case "ipalun":
                                /// -------------- Added on 16 Feb 2013 ------------------------
                                GlobalVariable.textFileFieldValues[i] = GlobalVariable.textFileFieldValues[i].Replace("'", "''");
                                if (!string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[i]) && GlobalVariable.textFileFieldValues[i].Length > 15)
                                {
                                    errorMessage += importDB.GetErrorMessage(individualColumnName, GlobalVariable.textFileFieldValues[i], 15);
                                    oUP_SKUBE.IPALUN = GlobalVariable.textFileFieldValues[i].Substring(0, 15);
                                }
                                else
                                    oUP_SKUBE.IPALUN = GlobalVariable.textFileFieldValues[i];
                                /// ------------------------------------------------------------
                                DataElementCount++;
                                break;
                            case "iminqt":
                                /// -------------- Added on 16 Feb 2013 ------------------------
                                GlobalVariable.textFileFieldValues[i] = GlobalVariable.textFileFieldValues[i].Replace("'", "''");
                                if (!string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[i]) && GlobalVariable.textFileFieldValues[i].Length > 15)
                                {
                                    errorMessage += importDB.GetErrorMessage(individualColumnName, GlobalVariable.textFileFieldValues[i], 15);
                                    oUP_SKUBE.IMINQT = GlobalVariable.textFileFieldValues[i].Substring(0, 15);
                                }
                                else
                                    oUP_SKUBE.IMINQT = GlobalVariable.textFileFieldValues[i];
                                /// ------------------------------------------------------------
                                DataElementCount++;
                                break;
                            case "ibmult":
                                /// -------------- Added on 16 Feb 2013 ------------------------
                                GlobalVariable.textFileFieldValues[i] = GlobalVariable.textFileFieldValues[i].Replace("'", "''");
                                if (!string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[i]) && GlobalVariable.textFileFieldValues[i].Length > 15)
                                {
                                    errorMessage += importDB.GetErrorMessage(individualColumnName, GlobalVariable.textFileFieldValues[i], 15);
                                    oUP_SKUBE.IBMULT = GlobalVariable.textFileFieldValues[i].Substring(0, 15);
                                }
                                else
                                    oUP_SKUBE.IBMULT = GlobalVariable.textFileFieldValues[i];
                                /// ------------------------------------------------------------
                                DataElementCount++;
                                break;
                            case "category":
                                /// -------------- Added on 16 Feb 2013 ------------------------
                                GlobalVariable.textFileFieldValues[i] = GlobalVariable.textFileFieldValues[i].Replace("'", "''");
                                if (!string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[i]) && GlobalVariable.textFileFieldValues[i].Length > 15)
                                {
                                    errorMessage += importDB.GetErrorMessage(individualColumnName, GlobalVariable.textFileFieldValues[i], 15);
                                    oUP_SKUBE.Category = GlobalVariable.textFileFieldValues[i].Substring(0, 15);
                                }
                                else
                                    oUP_SKUBE.Category = GlobalVariable.textFileFieldValues[i];
                                /// ------------------------------------------------------------
                                DataElementCount++;
                                break;
                            case "back order":
                                oUP_SKUBE.BackOrder = RemoveDecimal(GlobalVariable.textFileFieldValues[i].Replace("'", "''"));
                                DataElementCount++;
                                break;
                            case "backorder":
                                oUP_SKUBE.BackOrder = GlobalVariable.textFileFieldValues[i].Replace("'", "''");
                                DataElementCount++;
                                break;
                            case "back_order":
                                oUP_SKUBE.BackOrder = GlobalVariable.textFileFieldValues[i].Replace("'", "''");
                                DataElementCount++;
                                break;
                            case "country":
                                /// -------------- Added on 16 Feb 2013 ------------------------
                                GlobalVariable.textFileFieldValues[i] = GlobalVariable.textFileFieldValues[i].Replace("'", "''");
                                if (!string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[i]) && GlobalVariable.textFileFieldValues[i].Length > 20)
                                {
                                    errorMessage += importDB.GetErrorMessage(individualColumnName, GlobalVariable.textFileFieldValues[i], 20);
                                    oUP_SKUBE.Country = GlobalVariable.textFileFieldValues[i].Substring(0, 20);
                                }
                                else
                                    oUP_SKUBE.Country = GlobalVariable.textFileFieldValues[i];
                                /// ------------------------------------------------------------
                                DataElementCount++;
                                break;
                            case "total_qty_bo":
                                oUP_SKUBE.Total_Qty_BO = RemoveSpace(GlobalVariable.textFileFieldValues[i].Replace("'", "''"));
                                DataElementCount++;
                                break;
                            case "total_count_bo":
                                oUP_SKUBE.Total_Count_BO = RemoveSpace(GlobalVariable.textFileFieldValues[i].Replace("'", "''"));
                                DataElementCount++;
                                break;

                            case "total_orders":
                                oUP_SKUBE.Total_Orders = RemoveSpace(GlobalVariable.textFileFieldValues[i].Replace("'", "''"));
                                DataElementCount++;
                                break;
                            case "total_sales":
                                oUP_SKUBE.Total_Sales = RemoveSpace(GlobalVariable.textFileFieldValues[i].Replace("'", "''"));
                                DataElementCount++;
                                break;

                            default:
                                break;
                        }

                        //ImportDate = Utilities.Common.TextToDateFormat(oUP_SKUBE.Date);
                        oUP_SKUBE.UpdatedDate = ImportDate;
                    }
                    if (DataElementCount != intColumnCount)
                        return "Invalid File";

                    UP_SKUBEList.Add(oUP_SKUBE);

                    /// -------------- Added on 16 Feb 2013 ------------------------
                    if (!string.IsNullOrEmpty(errorMessage))
                    {
                        //oUP_DataImportErrorBE.ManualFileUploadID = Result;
                        oUP_DataImportErrorBE.ErrorDescription = errorMessage.Substring(0, errorMessage.Length - 1) + " in file " + itemFileName;
                        oUP_DataImportErrorBE.UpdatedDate = ImportDate;
                        UP_DataImportErrorBEList.Add(oUP_DataImportErrorBE);
                    }

                    /// ------------------------------------------------------------

                    TotalRecordCount += 1;
                }
                UP_SKUBAL oUP_SKUBAL = new UP_SKUBAL();
                oUP_SKUBAL.AddSKUs(UP_SKUBEList);
                UP_SKUBEList = null;

            }

        }
        catch (Exception)
        {
            return "Invalid File";
        }

        try
        {
            UP_ManualFileUploadBE oUP_ManualFileUploadBE = new UP_ManualFileUploadBE();
            oUP_ManualFileUploadBE.Action = "InsertManualUpload";
            oUP_ManualFileUploadBE.DateUploaded = ImportDate;
            oUP_ManualFileUploadBE.DownloadedFilename = itemFileName;
            oUP_ManualFileUploadBE.UserID = Convert.ToInt32(UserID);
            oUP_ManualFileUploadBE.TotalSKURecord = TotalRecordCount;

            UP_ManualFileUploadBAL oUP_ManualFileUploadBAL = new UP_ManualFileUploadBAL();
            int? Result = oUP_ManualFileUploadBAL.addEditManualUploadBAL(oUP_ManualFileUploadBE);

            /// -------------- Added on 16 Feb 2013 ------------------------

            foreach (UP_DataImportErrorBE obj in UP_DataImportErrorBEList)
            {
                obj.ManualFileUploadID = Convert.ToInt32(Result);

            }

            UP_SKUBAL oUP_SKUBAL = new UP_SKUBAL();
            oUP_SKUBAL.AddSKUErrors(UP_DataImportErrorBEList);
            UP_DataImportErrorBEList = null;
            /// ------------------------------------------------------------

        }
        catch (Exception)
        {

        }

        return string.Empty;
    }


    /// <summary>
    /// Gets the Date of a data to be Uploaded.  
    /// </summary>
    /// <param name="Filename">string</param>
    /// <returns>DateTime</returns>
    public DateTime GetDateDate(string date1)
    {

        string[] strArray = date1.Split('/');

        var Date = new DateTime(
            Convert.ToInt32(strArray[2].ToString()),
            Convert.ToInt32(strArray[1].ToString()),
            Convert.ToInt32(strArray[0].ToString()),
            0, 0, 0);

        return Date;
    }

    private string RemoveDecimal(string val)
    {
        string retVal;
        retVal = RemoveSpace(val);

        if (retVal != null)
        {
            if (retVal.Contains("."))
            {
                retVal = retVal.Substring(0, retVal.IndexOf("."));
            }
            if (retVal.Contains("-"))
            {
                retVal = '-' + (retVal.Trim('-'));
            }
            if (retVal.Contains("/"))
            {
                string[] arrDate = retVal.Split('/');
                retVal = arrDate[2] + "-" + arrDate[1] + "-" + arrDate[0];
            }
        }
        return retVal;
    }

    private string RemoveSpace(string val)
    {
        string retVal;
        if (val != string.Empty)
            retVal = val.Trim(' ');
        else
            retVal = null;
        return retVal;
    }
}
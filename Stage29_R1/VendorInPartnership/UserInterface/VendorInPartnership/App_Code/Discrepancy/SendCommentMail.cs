﻿using BusinessEntities.ModuleBE.Discrepancy.LogDiscrepancy;

/// <summary>
/// Summary description for SendCommentMail
/// </summary>
public class SendCommentMail
{
    public SendCommentMail()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    public void SendCommentsMail(DiscrepancyBE discrepancyBE, BaseControlLibrary.ucTextbox txtAltEmail, BaseControlLibrary.ucTextbox ucVendorEmailList, int vendorID, string ODComment, string CommentBy)
    {
        //check for the comments mail
        sendCommunication communication = new sendCommunication();
        int? retVal = communication.SendAndSaveMailForDiscrepancyComments(discrepancyBE.DiscrepancyLogID.Value, discrepancyBE.DiscrepancyType, txtAltEmail, ucVendorEmailList, vendorID, ODComment, CommentBy);
    }
    public void SendCommentsMailWorkFlow(DiscrepancyBE discrepancyBE, string ucVendorEmailList, int vendorID, string ODComment, string CommentBy)
    {
        //check for the comments mail
        sendCommunication communication = new sendCommunication();
        int? retVal = communication.SendAndSaveMailForDiscrepancyComments(discrepancyBE.DiscrepancyLogID.Value, discrepancyBE.DiscrepancyType, ucVendorEmailList, vendorID, ODComment, CommentBy);
    }
}
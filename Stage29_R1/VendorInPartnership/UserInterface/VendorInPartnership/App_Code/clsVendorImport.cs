﻿using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using BusinessEntities.ModuleBE.Upload;
using BusinessLogicLayer.ModuleBAL.Appointment.SiteSettings;
using BusinessLogicLayer.ModuleBAL.Upload;
using System;
using System.Collections.Generic;
using System.IO;

/// <summary>
/// Summary description for clsVendorImport
/// </summary>
public class clsVendorImport
{
    public clsVendorImport()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    public string UploadVendor(string vendorFilePath, string vendorFileName)
    {
        int TotalRecordCount = 0;
        int DataElementCount = 0;
        string individualColumnName = "";

        /// -------------- Added on 16 Feb 2013 ------------------------
        List<UP_DataImportErrorBE> UP_DataImportErrorBEList = new List<UP_DataImportErrorBE>();
        ImportDB importDB = new ImportDB();

        DateTime ImportDate = importDB.GetFileDate(vendorFileName);
        /// ------------------------------------------------------------

        try
        {
            using (StreamReader oStreamReader = new StreamReader(vendorFilePath, System.Text.Encoding.Default))
            {
                GlobalVariable.textFileLine = oStreamReader.ReadLine();
                var columns = GlobalVariable.textFileLine.Split(GlobalVariable.delimiters);
                if (columns.Length != 18)
                {
                    return "Invalid File";
                }
                List<UP_VendorBE> UP_VendorBEList = new List<UP_VendorBE>();

                while ((GlobalVariable.textFileLine = oStreamReader.ReadLine()) != null)
                {
                    DataElementCount = 0;
                    UP_VendorBE oUP_VendorBE = new UP_VendorBE();

                    /// -------------- Added on 16 Feb 2013 ------------------------
                    UP_DataImportErrorBE oUP_DataImportErrorBE = new UP_DataImportErrorBE();
                    string errorMessage = string.Empty;
                    /// ------------------------------------------------------------


                    GlobalVariable.textFileFieldValues = GlobalVariable.textFileLine.Split(GlobalVariable.delimiters);

                    for (int i = 0; i <= GlobalVariable.textFileFieldValues.Length - 1; i++)
                    {

                        individualColumnName = columns[i];

                        switch (individualColumnName.ToLower())
                        {
                            case "country":
                                /// -------------- Added on 16 Feb 2013 ------------------------
                                GlobalVariable.textFileFieldValues[i] = GlobalVariable.textFileFieldValues[i].Replace("'", "''");
                                if (!string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[i]) && GlobalVariable.textFileFieldValues[i].Length > 20)
                                {
                                    errorMessage += importDB.GetErrorMessage(individualColumnName, GlobalVariable.textFileFieldValues[i], 20);
                                    oUP_VendorBE.CountryName = GlobalVariable.textFileFieldValues[i].Substring(0, 20);
                                }
                                else
                                    oUP_VendorBE.CountryName = GlobalVariable.textFileFieldValues[i];
                                /// ------------------------------------------------------------
                                DataElementCount++;
                                break;
                            case "vendor_no":
                                /// -------------- Added on 16 Feb 2013 ------------------------
                                GlobalVariable.textFileFieldValues[i] = GlobalVariable.textFileFieldValues[i].Replace("'", "''");
                                if (!string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[i]) && GlobalVariable.textFileFieldValues[i].Length > 15)
                                {
                                    errorMessage += importDB.GetErrorMessage(individualColumnName, GlobalVariable.textFileFieldValues[i], 15);
                                    oUP_VendorBE.Vendor_No = GlobalVariable.textFileFieldValues[i].Substring(0, 15);
                                }
                                else
                                    oUP_VendorBE.Vendor_No = GlobalVariable.textFileFieldValues[i];
                                /// ------------------------------------------------------------
                                DataElementCount++;
                                break;
                            case "vendor_name":
                                /// -------------- Added on 16 Feb 2013 ------------------------
                                GlobalVariable.textFileFieldValues[i] = GlobalVariable.textFileFieldValues[i].Replace("'", "''");
                                if (!string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[i]) && GlobalVariable.textFileFieldValues[i].Length > 250)
                                {
                                    errorMessage += importDB.GetErrorMessage(individualColumnName, GlobalVariable.textFileFieldValues[i], 250);
                                    oUP_VendorBE.VendorName = GlobalVariable.textFileFieldValues[i].Substring(0, 250);
                                }
                                else
                                    oUP_VendorBE.VendorName = GlobalVariable.textFileFieldValues[i];
                                /// ------------------------------------------------------------
                                DataElementCount++;
                                break;
                            case "vendor contact name":
                                /// -------------- Added on 16 Feb 2013 ------------------------
                                GlobalVariable.textFileFieldValues[i] = GlobalVariable.textFileFieldValues[i].Replace("'", "''");
                                if (!string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[i]) && GlobalVariable.textFileFieldValues[i].Length > 250)
                                {
                                    errorMessage += importDB.GetErrorMessage(individualColumnName, GlobalVariable.textFileFieldValues[i], 250);
                                    oUP_VendorBE.VendorContactName = GlobalVariable.textFileFieldValues[i].Substring(0, 250);
                                }
                                else
                                    oUP_VendorBE.VendorContactName = GlobalVariable.textFileFieldValues[i];
                                /// ------------------------------------------------------------
                                DataElementCount++;
                                break;
                            case "vendor_contact_name":
                                /// -------------- Added on 16 Feb 2013 ------------------------
                                GlobalVariable.textFileFieldValues[i] = GlobalVariable.textFileFieldValues[i].Replace("'", "''");
                                if (!string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[i]) && GlobalVariable.textFileFieldValues[i].Length > 250)
                                {
                                    errorMessage += importDB.GetErrorMessage(individualColumnName, GlobalVariable.textFileFieldValues[i], 250);
                                    oUP_VendorBE.VendorContactName = GlobalVariable.textFileFieldValues[i].Substring(0, 250);
                                }
                                else
                                    oUP_VendorBE.VendorContactName = GlobalVariable.textFileFieldValues[i];
                                /// ------------------------------------------------------------
                                DataElementCount++;
                                break;




                            case "vendor_contact_number":
                                /// -------------- Added on 16 Feb 2013 ------------------------
                                GlobalVariable.textFileFieldValues[i] = GlobalVariable.textFileFieldValues[i].Replace("'", "''");
                                if (!string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[i]) && GlobalVariable.textFileFieldValues[i].Length > 100)
                                {
                                    errorMessage += importDB.GetErrorMessage(individualColumnName, GlobalVariable.textFileFieldValues[i], 100);
                                    oUP_VendorBE.VendorContactNumber = GlobalVariable.textFileFieldValues[i].Substring(0, 100);
                                }
                                else
                                    oUP_VendorBE.VendorContactNumber = GlobalVariable.textFileFieldValues[i];
                                /// ------------------------------------------------------------
                                DataElementCount++;
                                break;
                            case "vendor contact number":
                                /// -------------- Added on 16 Feb 2013 ------------------------
                                GlobalVariable.textFileFieldValues[i] = GlobalVariable.textFileFieldValues[i].Replace("'", "''");
                                if (!string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[i]) && GlobalVariable.textFileFieldValues[i].Length > 100)
                                {
                                    errorMessage += importDB.GetErrorMessage(individualColumnName, GlobalVariable.textFileFieldValues[i], 100);
                                    oUP_VendorBE.VendorContactNumber = GlobalVariable.textFileFieldValues[i].Substring(0, 100);
                                }
                                else
                                    oUP_VendorBE.VendorContactNumber = GlobalVariable.textFileFieldValues[i];
                                /// ------------------------------------------------------------
                                DataElementCount++;
                                break;

                            case "vendor_contact_fax":
                                /// -------------- Added on 16 Feb 2013 ------------------------
                                GlobalVariable.textFileFieldValues[i] = GlobalVariable.textFileFieldValues[i].Replace("'", "''");
                                if (!string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[i]) && GlobalVariable.textFileFieldValues[i].Length > 100)
                                {
                                    errorMessage += importDB.GetErrorMessage(individualColumnName, GlobalVariable.textFileFieldValues[i], 100);
                                    oUP_VendorBE.VendorContactFax = GlobalVariable.textFileFieldValues[i].Substring(0, 100);
                                }
                                else
                                    oUP_VendorBE.VendorContactFax = GlobalVariable.textFileFieldValues[i];
                                /// ------------------------------------------------------------
                                DataElementCount++;
                                break;
                            case "vendor contact fax":
                                /// -------------- Added on 16 Feb 2013 ------------------------
                                GlobalVariable.textFileFieldValues[i] = GlobalVariable.textFileFieldValues[i].Replace("'", "''");
                                if (!string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[i]) && GlobalVariable.textFileFieldValues[i].Length > 100)
                                {
                                    errorMessage += importDB.GetErrorMessage(individualColumnName, GlobalVariable.textFileFieldValues[i], 100);
                                    oUP_VendorBE.VendorContactFax = GlobalVariable.textFileFieldValues[i].Substring(0, 100);
                                }
                                else
                                    oUP_VendorBE.VendorContactFax = GlobalVariable.textFileFieldValues[i];
                                /// ------------------------------------------------------------
                                DataElementCount++;
                                break;

                            case "vendor_contact_email":
                                /// -------------- Added on 16 Feb 2013 ------------------------
                                GlobalVariable.textFileFieldValues[i] = GlobalVariable.textFileFieldValues[i].Replace("'", "''");
                                if (!string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[i]) && GlobalVariable.textFileFieldValues[i].Length > 100)
                                {
                                    errorMessage += importDB.GetErrorMessage(individualColumnName, GlobalVariable.textFileFieldValues[i], 100);
                                    oUP_VendorBE.VendorContactEmail = GlobalVariable.textFileFieldValues[i].Substring(0, 100);
                                }
                                else
                                    oUP_VendorBE.VendorContactEmail = GlobalVariable.textFileFieldValues[i];
                                /// ------------------------------------------------------------
                                DataElementCount++;
                                break;

                            case "vendor contact email":
                                /// -------------- Added on 16 Feb 2013 ------------------------
                                GlobalVariable.textFileFieldValues[i] = GlobalVariable.textFileFieldValues[i].Replace("'", "''");
                                if (!string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[i]) && GlobalVariable.textFileFieldValues[i].Length > 100)
                                {
                                    errorMessage += importDB.GetErrorMessage(individualColumnName, GlobalVariable.textFileFieldValues[i], 100);
                                    oUP_VendorBE.VendorContactEmail = GlobalVariable.textFileFieldValues[i].Substring(0, 100);
                                }
                                else
                                    oUP_VendorBE.VendorContactEmail = GlobalVariable.textFileFieldValues[i];
                                /// ------------------------------------------------------------
                                DataElementCount++;
                                break;


                            case "address_1":
                                /// -------------- Added on 16 Feb 2013 ------------------------
                                GlobalVariable.textFileFieldValues[i] = GlobalVariable.textFileFieldValues[i].Replace("'", "''");
                                if (!string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[i]) && GlobalVariable.textFileFieldValues[i].Length > 250)
                                {
                                    errorMessage += importDB.GetErrorMessage(individualColumnName, GlobalVariable.textFileFieldValues[i], 250);
                                    oUP_VendorBE.address1 = GlobalVariable.textFileFieldValues[i].Substring(0, 250);
                                }
                                else
                                    oUP_VendorBE.address1 = GlobalVariable.textFileFieldValues[i];
                                /// ------------------------------------------------------------
                                DataElementCount++;
                                break;
                            case "address 1":
                                /// -------------- Added on 16 Feb 2013 ------------------------
                                GlobalVariable.textFileFieldValues[i] = GlobalVariable.textFileFieldValues[i].Replace("'", "''");
                                if (!string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[i]) && GlobalVariable.textFileFieldValues[i].Length > 250)
                                {
                                    errorMessage += importDB.GetErrorMessage(individualColumnName, GlobalVariable.textFileFieldValues[i], 250);
                                    oUP_VendorBE.address1 = GlobalVariable.textFileFieldValues[i].Substring(0, 250);
                                }
                                else
                                    oUP_VendorBE.address1 = GlobalVariable.textFileFieldValues[i];
                                /// ------------------------------------------------------------
                                DataElementCount++;
                                break;


                            case "address_2":
                                /// -------------- Added on 16 Feb 2013 ------------------------
                                GlobalVariable.textFileFieldValues[i] = GlobalVariable.textFileFieldValues[i].Replace("'", "''");
                                if (!string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[i]) && GlobalVariable.textFileFieldValues[i].Length > 250)
                                {
                                    errorMessage += importDB.GetErrorMessage(individualColumnName, GlobalVariable.textFileFieldValues[i], 250);
                                    oUP_VendorBE.address2 = GlobalVariable.textFileFieldValues[i].Substring(0, 250);
                                }
                                else
                                    oUP_VendorBE.address2 = GlobalVariable.textFileFieldValues[i];
                                /// ------------------------------------------------------------
                                DataElementCount++;
                                break;

                            case "address 2":
                                /// -------------- Added on 16 Feb 2013 ------------------------
                                GlobalVariable.textFileFieldValues[i] = GlobalVariable.textFileFieldValues[i].Replace("'", "''");
                                if (!string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[i]) && GlobalVariable.textFileFieldValues[i].Length > 250)
                                {
                                    errorMessage += importDB.GetErrorMessage(individualColumnName, GlobalVariable.textFileFieldValues[i], 250);
                                    oUP_VendorBE.address2 = GlobalVariable.textFileFieldValues[i].Substring(0, 250);
                                }
                                else
                                    oUP_VendorBE.address2 = GlobalVariable.textFileFieldValues[i];
                                /// ------------------------------------------------------------
                                DataElementCount++;
                                break;

                            case "city":
                                /// -------------- Added on 16 Feb 2013 ------------------------
                                GlobalVariable.textFileFieldValues[i] = GlobalVariable.textFileFieldValues[i].Replace("'", "''");
                                if (!string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[i]) && GlobalVariable.textFileFieldValues[i].Length > 250)
                                {
                                    errorMessage += importDB.GetErrorMessage(individualColumnName, GlobalVariable.textFileFieldValues[i], 250);
                                    oUP_VendorBE.city = GlobalVariable.textFileFieldValues[i].Substring(0, 250);
                                }
                                else
                                    oUP_VendorBE.city = GlobalVariable.textFileFieldValues[i].Replace("'", "''");
                                /// ------------------------------------------------------------
                                DataElementCount++;
                                break;
                            case "county":
                                /// -------------- Added on 16 Feb 2013 ------------------------
                                GlobalVariable.textFileFieldValues[i] = GlobalVariable.textFileFieldValues[i].Replace("'", "''");
                                if (!string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[i]) && GlobalVariable.textFileFieldValues[i].Length > 250)
                                {
                                    errorMessage += importDB.GetErrorMessage(individualColumnName, GlobalVariable.textFileFieldValues[i], 250);
                                    oUP_VendorBE.county = GlobalVariable.textFileFieldValues[i].Substring(0, 250);
                                }
                                else
                                    oUP_VendorBE.county = GlobalVariable.textFileFieldValues[i];
                                /// ------------------------------------------------------------
                                DataElementCount++;
                                break;
                            case "zipcode1":
                                /// -------------- Added on 16 Feb 2013 ------------------------
                                GlobalVariable.textFileFieldValues[i] = GlobalVariable.textFileFieldValues[i].Replace("'", "''");
                                if (!string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[i]) && GlobalVariable.textFileFieldValues[i].Length > 250)
                                {
                                    errorMessage += importDB.GetErrorMessage(individualColumnName, GlobalVariable.textFileFieldValues[i], 250);
                                    oUP_VendorBE.VMPPIN = GlobalVariable.textFileFieldValues[i].Substring(0, 250);
                                }
                                else
                                    oUP_VendorBE.VMPPIN = GlobalVariable.textFileFieldValues[i];
                                /// ------------------------------------------------------------
                                DataElementCount++;
                                break;
                            case "zipcode2":
                                /// -------------- Added on 16 Feb 2013 ------------------------
                                GlobalVariable.textFileFieldValues[i] = GlobalVariable.textFileFieldValues[i].Replace("'", "''");
                                if (!string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[i]) && GlobalVariable.textFileFieldValues[i].Length > 250)
                                {
                                    errorMessage += importDB.GetErrorMessage(individualColumnName, GlobalVariable.textFileFieldValues[i], 250);
                                    oUP_VendorBE.VMPPOU = GlobalVariable.textFileFieldValues[i].Substring(0, 250);
                                }
                                else
                                    oUP_VendorBE.VMPPOU = GlobalVariable.textFileFieldValues[i];
                                /// ------------------------------------------------------------
                                DataElementCount++;
                                break;
                            case "tel1":
                                /// -------------- Added on 16 Feb 2013 ------------------------
                                GlobalVariable.textFileFieldValues[i] = GlobalVariable.textFileFieldValues[i].Replace("'", "''");
                                if (!string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[i]) && GlobalVariable.textFileFieldValues[i].Length > 250)
                                {
                                    errorMessage += importDB.GetErrorMessage(individualColumnName, GlobalVariable.textFileFieldValues[i], 250);
                                    oUP_VendorBE.VMTCC1 = GlobalVariable.textFileFieldValues[i].Substring(0, 250);
                                }
                                else
                                    oUP_VendorBE.VMTCC1 = GlobalVariable.textFileFieldValues[i];
                                /// ------------------------------------------------------------
                                DataElementCount++;
                                break;
                            case "tel2":
                                /// -------------- Added on 16 Feb 2013 ------------------------
                                GlobalVariable.textFileFieldValues[i] = GlobalVariable.textFileFieldValues[i].Replace("'", "''");
                                if (!string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[i]) && GlobalVariable.textFileFieldValues[i].Length > 250)
                                {
                                    errorMessage += importDB.GetErrorMessage(individualColumnName, GlobalVariable.textFileFieldValues[i], 250);
                                    oUP_VendorBE.VMTEL1 = GlobalVariable.textFileFieldValues[i].Substring(0, 250);
                                }
                                else
                                    oUP_VendorBE.VMTEL1 = GlobalVariable.textFileFieldValues[i];
                                /// ------------------------------------------------------------
                                DataElementCount++;
                                break;
                            case "fax_country_code":
                                /// -------------- Added on 16 Feb 2013 ------------------------
                                GlobalVariable.textFileFieldValues[i] = GlobalVariable.textFileFieldValues[i].Replace("'", "''");
                                if (!string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[i]) && GlobalVariable.textFileFieldValues[i].Length > 5)
                                {
                                    errorMessage += importDB.GetErrorMessage(individualColumnName, GlobalVariable.textFileFieldValues[i], 5);
                                    oUP_VendorBE.Fax_country_code = GlobalVariable.textFileFieldValues[i].Substring(0, 5);
                                }
                                else
                                    oUP_VendorBE.Fax_country_code = GlobalVariable.textFileFieldValues[i];
                                /// ------------------------------------------------------------
                                DataElementCount++;
                                break;
                            case "fax_no":
                                /// -------------- Added on 16 Feb 2013 ------------------------
                                GlobalVariable.textFileFieldValues[i] = GlobalVariable.textFileFieldValues[i].Replace("'", "''");
                                if (!string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[i]) && GlobalVariable.textFileFieldValues[i].Length > 100)
                                {
                                    errorMessage += importDB.GetErrorMessage(individualColumnName, GlobalVariable.textFileFieldValues[i], 100);
                                    oUP_VendorBE.Fax_no = GlobalVariable.textFileFieldValues[i].Substring(0, 100);
                                }
                                else
                                    oUP_VendorBE.Fax_no = GlobalVariable.textFileFieldValues[i];
                                /// ------------------------------------------------------------
                                DataElementCount++;
                                break;
                            case "buyer":
                                /// -------------- Added on 16 Feb 2013 ------------------------
                                GlobalVariable.textFileFieldValues[i] = GlobalVariable.textFileFieldValues[i].Replace("'", "''");
                                if (!string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[i]) && GlobalVariable.textFileFieldValues[i].Length > 10)
                                {
                                    errorMessage += importDB.GetErrorMessage(individualColumnName, GlobalVariable.textFileFieldValues[i], 10);
                                    oUP_VendorBE.Buyer = GlobalVariable.textFileFieldValues[i].Substring(0, 10);
                                }
                                else
                                    oUP_VendorBE.Buyer = GlobalVariable.textFileFieldValues[i];
                                /// ------------------------------------------------------------
                                DataElementCount++;
                                break;
                            default:
                                break;
                        }

                        oUP_VendorBE.UpdatedDate = ImportDate;
                    }

                    if (DataElementCount != 18)
                        return "Invalid File";

                    UP_VendorBEList.Add(oUP_VendorBE);

                    /// -------------- Added on 16 Feb 2013 ------------------------
                    if (!string.IsNullOrEmpty(errorMessage))
                    {
                        // oUP_DataImportErrorBE.ManualFileUploadID = Result;
                        oUP_DataImportErrorBE.ErrorDescription = errorMessage.Substring(0, errorMessage.Length - 1) + " in file " + vendorFileName;
                        oUP_DataImportErrorBE.UpdatedDate = ImportDate;
                        UP_DataImportErrorBEList.Add(oUP_DataImportErrorBE);
                    }

                    /// ------------------------------------------------------------

                    TotalRecordCount += 1;
                }
                UP_VendorBAL oUP_VendorBAL = new UP_VendorBAL();
                oUP_VendorBAL.AddVendors(UP_VendorBEList);
                UP_VendorBEList = null;


            }

        }
        catch (Exception)
        {
            return "Invalid File";
        }

        try
        {
            UP_ManualFileUploadBE oUP_ManualFileUploadBE = new UP_ManualFileUploadBE();
            oUP_ManualFileUploadBE.Action = "InsertManualUpload";
            oUP_ManualFileUploadBE.DateUploaded = ImportDate;
            oUP_ManualFileUploadBE.DownloadedFilename = vendorFileName;
            oUP_ManualFileUploadBE.UserID = Convert.ToInt32(System.Web.HttpContext.Current.Session["UserId"]);
            oUP_ManualFileUploadBE.TotalVendorRecord = TotalRecordCount;

            UP_ManualFileUploadBAL oUP_ManualFileUploadBAL = new UP_ManualFileUploadBAL();
            int? Result = oUP_ManualFileUploadBAL.addEditManualUploadBAL(oUP_ManualFileUploadBE);

            /// -------------- Added on 16 Feb 2013 ------------------------
            foreach (UP_DataImportErrorBE obj in UP_DataImportErrorBEList)
            {
                obj.ManualFileUploadID = Convert.ToInt32(Result);

            }

            UP_VendorBAL oUP_VendorBAL = new UP_VendorBAL();
            oUP_VendorBAL.AddVendorsErrors(UP_DataImportErrorBEList);
            UP_DataImportErrorBEList = null;

            /// ------------------------------------------------------------
        }
        catch (Exception)
        {
        }

        return string.Empty;
    }
}
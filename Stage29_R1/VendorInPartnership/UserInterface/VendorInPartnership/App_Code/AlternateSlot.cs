﻿/// <summary>
/// Summary description for AlternateSlot
/// </summary>
public class AlternateSlot
{
    public string ArrSlotTime { get; set; }
    public string EstSlotTime { get; set; }
    public string Vendor { get; set; }
    public string Pallets { get; set; }
    public string Cartoons { get; set; }
    public string Lines { get; set; }
    public string Lifts { get; set; }
    public string Color { get; set; }
    public string DoorNo { get; set; }
    public string FixedSlotID { get; set; }
    public string SlotTimeID { get; set; }
    public string SiteDoorNumberID { get; set; }
    public string SupplierType { get; set; }
    public string AllocationType { get; set; }
    public string ArrSlotTime15min { get; set; }
    public string EstSlotTime15min { get; set; }
    public string DoorNameDisplay { get; set; }
    public string DoorNoDisplay { get; set; }
    public string VendorDisplay { get; set; }
    public string SlotStartDay { get; set; }
    public string siTimeSlotOrderBYID { get; set; }
    public string EstSlotTimeDisplay { get; set; }
    public string BookingID { get; set; }
    public string BookingStatus { get; set; }
    public string BookingStatusID { get; set; }
    public string Weekday { get; set; }
    public string Issue { get; set; }
    public string Carrier { get; set; }
}
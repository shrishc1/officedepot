﻿using System;

/// <summary>
/// Summary description for AttachedImagesList
/// </summary>
[Serializable]
public class AttachedImagesList
{
    public string ImagePath { get; set; }
    public string Guid { get; set; }
}
﻿using BusinessEntities.ModuleBE.Upload;
using BusinessLogicLayer.ModuleBAL.Upload;
using System;
using System.Collections.Generic;
using System.IO;

/// <summary>
/// Summary description for clsRMSImport
/// </summary>
public class clsRMSImport
{
    public clsRMSImport()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public string UploadRMS(string itemFilePath, string itemFileName, string UserID)
    {
        int TotalRecordCount = 0;
        int DataElementCount = 0;
        string individualColumnName = "";
        int intColumnCount = 20;

        List<UP_DataImportErrorBE> UP_DataImportErrorBEList = new List<UP_DataImportErrorBE>();
        ImportDB importDB = new ImportDB();

        DateTime ImportDate = importDB.GetFileDate(itemFileName); // importDB.GetRMSFileDate(itemFileName);


        try
        {
            using (StreamReader oStreamReader = new StreamReader(itemFilePath, System.Text.Encoding.Default))
            {
                GlobalVariable.textFileLine = oStreamReader.ReadLine().ToLower();
                var columns = GlobalVariable.textFileLine.Split(GlobalVariable.delimiters);
                if (columns.Length != intColumnCount)
                {
                    return "Invalid File";
                }
                List<UP_RMSBE> UP_RMSBEList = new List<UP_RMSBE>();

                while ((GlobalVariable.textFileLine = oStreamReader.ReadLine()) != null)
                {
                    DataElementCount = 0;
                    UP_RMSBE oUP_RMSBE = new UP_RMSBE();


                    UP_DataImportErrorBE oUP_DataImportErrorBE = new UP_DataImportErrorBE();
                    string errorMessage = string.Empty;



                    GlobalVariable.textFileFieldValues = GlobalVariable.textFileLine.Split(GlobalVariable.delimiters);


                    DateTime? tempDate = DateTime.Now;
                    for (int i = 0; i <= GlobalVariable.textFileFieldValues.Length - 1; i++)
                    {

                        individualColumnName = columns[i];


                        switch (individualColumnName.ToLower().Trim().Replace(" ", "_"))
                        {
                            case "country":

                                GlobalVariable.textFileFieldValues[i] = GlobalVariable.textFileFieldValues[i].Replace("'", "''");
                                if (!string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[i]) && GlobalVariable.textFileFieldValues[i].Trim().Length > 20)
                                {
                                    errorMessage += importDB.GetErrorMessage(individualColumnName, GlobalVariable.textFileFieldValues[i], 20);
                                    oUP_RMSBE.Country = GlobalVariable.textFileFieldValues[i].Substring(0, 20);
                                }
                                else
                                    oUP_RMSBE.Country = GlobalVariable.textFileFieldValues[i].Trim();
                                /// -----------------------------------------------------------
                                DataElementCount++;
                                break;
                            case "viking_code":

                                GlobalVariable.textFileFieldValues[i] = GlobalVariable.textFileFieldValues[i].Replace("'", "''");
                                if (!string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[i]) && GlobalVariable.textFileFieldValues[i].Trim().Length > 25)
                                {
                                    errorMessage += importDB.GetErrorMessage(individualColumnName, GlobalVariable.textFileFieldValues[i], 25);
                                    oUP_RMSBE.Viking_Code = GlobalVariable.textFileFieldValues[i].Substring(0, 25);
                                }
                                else
                                    oUP_RMSBE.Viking_Code = GlobalVariable.textFileFieldValues[i].Trim();
                                /// -----------------------------------------------------------
                                DataElementCount++;
                                break;
                            case "od_code":

                                GlobalVariable.textFileFieldValues[i] = GlobalVariable.textFileFieldValues[i].Replace("'", "''");
                                if (!string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[i]) && GlobalVariable.textFileFieldValues[i].Trim().Length > 25)
                                {
                                    errorMessage += importDB.GetErrorMessage(individualColumnName, GlobalVariable.textFileFieldValues[i], 25);
                                    oUP_RMSBE.OD_Code = GlobalVariable.textFileFieldValues[i].Substring(0, 25);
                                }
                                else
                                    oUP_RMSBE.OD_Code = GlobalVariable.textFileFieldValues[i].Trim();
                                /// -----------------------------------------------------------
                                DataElementCount++;
                                break;
                            case "sap_code":

                                GlobalVariable.textFileFieldValues[i] = GlobalVariable.textFileFieldValues[i].Replace("'", "''");
                                if (!string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[i]) && GlobalVariable.textFileFieldValues[i].Trim().Length > 25)
                                {
                                    errorMessage += importDB.GetErrorMessage(individualColumnName, GlobalVariable.textFileFieldValues[i], 25);
                                    oUP_RMSBE.Sap_Code = GlobalVariable.textFileFieldValues[i].Substring(0, 25);
                                }
                                else
                                    oUP_RMSBE.Sap_Code = GlobalVariable.textFileFieldValues[i].Trim();

                                DataElementCount++;
                                break;
                            case "rms_code":

                                GlobalVariable.textFileFieldValues[i] = GlobalVariable.textFileFieldValues[i].Replace("'", "''");
                                if (!string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[i]) && GlobalVariable.textFileFieldValues[i].Trim().Length > 25)
                                {
                                    errorMessage += importDB.GetErrorMessage(individualColumnName, GlobalVariable.textFileFieldValues[i], 25);
                                    oUP_RMSBE.RMS_Code = GlobalVariable.textFileFieldValues[i].Substring(0, 25);
                                }
                                else
                                    oUP_RMSBE.RMS_Code = GlobalVariable.textFileFieldValues[i].Trim();

                                DataElementCount++;
                                break;
                            case "brand_type":
                                GlobalVariable.textFileFieldValues[i] = GlobalVariable.textFileFieldValues[i].Replace("'", "''");
                                if (!string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[i]) && GlobalVariable.textFileFieldValues[i].Trim().Length > 25)
                                {
                                    errorMessage += importDB.GetErrorMessage(individualColumnName, GlobalVariable.textFileFieldValues[i], 25);
                                    oUP_RMSBE.Brand_Type = GlobalVariable.textFileFieldValues[i].Substring(0, 25);
                                }
                                else
                                    oUP_RMSBE.Brand_Type = GlobalVariable.textFileFieldValues[i].Trim();

                                DataElementCount++;
                                break;
                            case "brand_label_name":
                                GlobalVariable.textFileFieldValues[i] = GlobalVariable.textFileFieldValues[i].Replace("'", "''");
                                if (!string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[i]) && GlobalVariable.textFileFieldValues[i].Trim().Length > 100)
                                {
                                    errorMessage += importDB.GetErrorMessage(individualColumnName, GlobalVariable.textFileFieldValues[i], 100);
                                    oUP_RMSBE.Brand_Label_Name = GlobalVariable.textFileFieldValues[i].Substring(0, 100);
                                }
                                else
                                    oUP_RMSBE.Brand_Label_Name = GlobalVariable.textFileFieldValues[i].Trim();

                                DataElementCount++;
                                break;
                            case "master_vendor_number":
                                GlobalVariable.textFileFieldValues[i] = GlobalVariable.textFileFieldValues[i].Replace("'", "''");
                                if (!string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[i]) && GlobalVariable.textFileFieldValues[i].Trim().Length > 25)
                                {
                                    errorMessage += importDB.GetErrorMessage(individualColumnName, GlobalVariable.textFileFieldValues[i], 25);
                                    oUP_RMSBE.Master_Vendor_Number = GlobalVariable.textFileFieldValues[i].Substring(0, 25);
                                }
                                else
                                    oUP_RMSBE.Master_Vendor_Number = GlobalVariable.textFileFieldValues[i].Trim();

                                DataElementCount++;
                                break;
                            case "master_vendor_name":
                                GlobalVariable.textFileFieldValues[i] = GlobalVariable.textFileFieldValues[i].Replace("'", "''");
                                if (!string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[i]) && GlobalVariable.textFileFieldValues[i].Trim().Length > 50)
                                {
                                    errorMessage += importDB.GetErrorMessage(individualColumnName, GlobalVariable.textFileFieldValues[i], 50);
                                    oUP_RMSBE.Master_Vendor_Name = GlobalVariable.textFileFieldValues[i].Substring(0, 50);
                                }
                                else
                                    oUP_RMSBE.Master_Vendor_Name = GlobalVariable.textFileFieldValues[i].Trim();

                                DataElementCount++;
                                break;
                            case "division":

                                GlobalVariable.textFileFieldValues[i] = RemoveSpace(GlobalVariable.textFileFieldValues[i].Replace("'", "''"));
                                if (!string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[i]) && GlobalVariable.textFileFieldValues[i].Trim().Length > 10)
                                {
                                    errorMessage += importDB.GetErrorMessage(individualColumnName, GlobalVariable.textFileFieldValues[i], 10);
                                    oUP_RMSBE.Division = GlobalVariable.textFileFieldValues[i].Substring(0, 10);
                                }
                                else
                                    oUP_RMSBE.Division = GlobalVariable.textFileFieldValues[i].Trim();

                                DataElementCount++;
                                break;
                            case "div_name":

                                GlobalVariable.textFileFieldValues[i] = GlobalVariable.textFileFieldValues[i].Replace("'", "''");
                                if (!string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[i]) && GlobalVariable.textFileFieldValues[i].Trim().Length > 100)
                                {
                                    errorMessage += importDB.GetErrorMessage(individualColumnName, GlobalVariable.textFileFieldValues[i], 100);
                                    oUP_RMSBE.Div_Name = GlobalVariable.textFileFieldValues[i].Substring(0, 100);
                                }
                                else
                                    oUP_RMSBE.Div_Name = GlobalVariable.textFileFieldValues[i].Trim();

                                DataElementCount++;
                                break;
                            case "group_no":

                                GlobalVariable.textFileFieldValues[i] = GlobalVariable.textFileFieldValues[i].Replace("'", "''");
                                if (!string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[i]) && GlobalVariable.textFileFieldValues[i].Trim().Length > 15)
                                {
                                    errorMessage += importDB.GetErrorMessage(individualColumnName, GlobalVariable.textFileFieldValues[i], 15);
                                    oUP_RMSBE.Group_No = GlobalVariable.textFileFieldValues[i].Substring(0, 15);
                                }
                                else
                                    oUP_RMSBE.Group_No = GlobalVariable.textFileFieldValues[i].Trim();

                                DataElementCount++;
                                break;
                            case "group_name":

                                GlobalVariable.textFileFieldValues[i] = GlobalVariable.textFileFieldValues[i].Replace("'", "''");
                                if (!string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[i]) && GlobalVariable.textFileFieldValues[i].Trim().Length > 100)
                                {
                                    errorMessage += importDB.GetErrorMessage(individualColumnName, GlobalVariable.textFileFieldValues[i], 10);
                                    oUP_RMSBE.Group_Name = GlobalVariable.textFileFieldValues[i].Substring(0, 100);
                                }
                                else
                                    oUP_RMSBE.Group_Name = GlobalVariable.textFileFieldValues[i].Trim();

                                DataElementCount++;
                                break;
                            case "dept":

                                GlobalVariable.textFileFieldValues[i] = GlobalVariable.textFileFieldValues[i].Replace("'", "''");
                                if (!string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[i]) && GlobalVariable.textFileFieldValues[i].Trim().Length > 10)
                                {
                                    errorMessage += importDB.GetErrorMessage(individualColumnName, GlobalVariable.textFileFieldValues[i], 10);
                                    oUP_RMSBE.Dept = GlobalVariable.textFileFieldValues[i].Substring(0, 10);
                                }
                                else
                                    oUP_RMSBE.Dept = GlobalVariable.textFileFieldValues[i].Trim();

                                DataElementCount++;
                                break;
                            case "dept_name":

                                GlobalVariable.textFileFieldValues[i] = GlobalVariable.textFileFieldValues[i].Replace("'", "''");
                                if (!string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[i]) && GlobalVariable.textFileFieldValues[i].Trim().Length > 100)
                                {
                                    errorMessage += importDB.GetErrorMessage(individualColumnName, GlobalVariable.textFileFieldValues[i], 100);
                                    oUP_RMSBE.Dept_Name = GlobalVariable.textFileFieldValues[i].Substring(0, 100);
                                }
                                else
                                    oUP_RMSBE.Dept_Name = GlobalVariable.textFileFieldValues[i].Trim();

                                DataElementCount++;
                                break;
                            case "class":

                                GlobalVariable.textFileFieldValues[i] = GlobalVariable.textFileFieldValues[i].Replace("'", "''");
                                if (!string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[i]) && GlobalVariable.textFileFieldValues[i].Trim().Length > 15)
                                {
                                    errorMessage += importDB.GetErrorMessage(individualColumnName, GlobalVariable.textFileFieldValues[i], 15);
                                    oUP_RMSBE.Class = GlobalVariable.textFileFieldValues[i].Substring(0, 15);
                                }
                                else
                                    oUP_RMSBE.Class = GlobalVariable.textFileFieldValues[i].Trim();

                                DataElementCount++;
                                break;
                            case "class_name":

                                GlobalVariable.textFileFieldValues[i] = GlobalVariable.textFileFieldValues[i].Replace("'", "''");
                                if (!string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[i]) && GlobalVariable.textFileFieldValues[i].Trim().Length > 100)
                                {
                                    errorMessage += importDB.GetErrorMessage(individualColumnName, GlobalVariable.textFileFieldValues[i], 100);
                                    oUP_RMSBE.Class_Name = GlobalVariable.textFileFieldValues[i].Substring(0, 100);
                                }
                                else
                                    oUP_RMSBE.Class_Name = GlobalVariable.textFileFieldValues[i].Trim();

                                DataElementCount++;
                                break;
                            case "subclass":

                                GlobalVariable.textFileFieldValues[i] = GlobalVariable.textFileFieldValues[i].Replace("'", "''");
                                if (!string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[i]) && GlobalVariable.textFileFieldValues[i].Trim().Length > 15)
                                {
                                    errorMessage += importDB.GetErrorMessage(individualColumnName, GlobalVariable.textFileFieldValues[i], 15);
                                    oUP_RMSBE.Subclass = GlobalVariable.textFileFieldValues[i].Substring(0, 15);
                                }
                                else
                                    oUP_RMSBE.Subclass = GlobalVariable.textFileFieldValues[i].Trim();

                                DataElementCount++;
                                break;
                            case "subclass_name":

                                GlobalVariable.textFileFieldValues[i] = GlobalVariable.textFileFieldValues[i].Replace("'", "''");
                                if (!string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[i]) && GlobalVariable.textFileFieldValues[i].Trim().Length > 100)
                                {
                                    errorMessage += importDB.GetErrorMessage(individualColumnName, GlobalVariable.textFileFieldValues[i], 100);
                                    oUP_RMSBE.Subclass_Name = GlobalVariable.textFileFieldValues[i].Substring(0, 100);
                                }
                                else
                                    oUP_RMSBE.Subclass_Name = GlobalVariable.textFileFieldValues[i].Trim();

                                DataElementCount++;
                                break;
                            case "discontinued_indicator":

                                GlobalVariable.textFileFieldValues[i] = GlobalVariable.textFileFieldValues[i].Replace("'", "''");
                                if (!string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[i]) && GlobalVariable.textFileFieldValues[i].Trim().Length > 1)
                                {
                                    //errorMessage += importDB.GetErrorMessage(individualColumnName, GlobalVariable.textFileFieldValues[i], 3);
                                    oUP_RMSBE.Discontinued_Indicator = GlobalVariable.textFileFieldValues[i].Substring(0, 1);
                                }
                                else
                                    oUP_RMSBE.Discontinued_Indicator = GlobalVariable.textFileFieldValues[i].Substring(0, 1);

                                DataElementCount++;
                                break;
                            default:
                                break;
                        }
                        oUP_RMSBE.UpdatedDate = ImportDate;
                    }
                    if (DataElementCount != intColumnCount)
                        return "Invalid File";

                    UP_RMSBEList.Add(oUP_RMSBE);


                    if (!string.IsNullOrEmpty(errorMessage))
                    {
                        //oUP_DataImportErrorBE.ManualFileUploadID = Result;
                        oUP_DataImportErrorBE.ErrorDescription = errorMessage.Substring(0, errorMessage.Length - 1) + " in file " + itemFileName;
                        oUP_DataImportErrorBE.UpdatedDate = ImportDate;
                        UP_DataImportErrorBEList.Add(oUP_DataImportErrorBE);
                    }



                    TotalRecordCount += 1;
                }
                UP_RMSBAL oUP_RMSBAL = new UP_RMSBAL();
                oUP_RMSBAL.AddRMSs(UP_RMSBEList);
                UP_RMSBEList = null;

            }

        }
        catch (Exception)
        {
            return "Invalid File";
        }

        try
        {
            UP_ManualFileUploadBE oUP_ManualFileUploadBE = new UP_ManualFileUploadBE();
            oUP_ManualFileUploadBE.Action = "InsertRMSUpload";
            oUP_ManualFileUploadBE.DateUploaded = ImportDate;
            oUP_ManualFileUploadBE.DownloadedFilename = itemFileName;
            oUP_ManualFileUploadBE.UserID = Convert.ToInt32(UserID);
            oUP_ManualFileUploadBE.TotalRMSRecord = TotalRecordCount;

            UP_ManualFileUploadBAL oUP_ManualFileUploadBAL = new UP_ManualFileUploadBAL();
            int? Result = oUP_ManualFileUploadBAL.addEditManualUploadBAL(oUP_ManualFileUploadBE);



            foreach (UP_DataImportErrorBE obj in UP_DataImportErrorBEList)
            {
                obj.ManualFileUploadID = Convert.ToInt32(Result);

            }

            UP_RMSBAL oUP_RMSBAL = new UP_RMSBAL();
            oUP_RMSBAL.AddRMSErrors(UP_DataImportErrorBEList);
            UP_DataImportErrorBEList = null;


        }
        catch (Exception)
        {

        }

        return string.Empty;
    }

    private string RemoveDecimal(string val)
    {
        string retVal;
        retVal = RemoveSpace(val);

        if (retVal != null)
        {
            if (retVal.Contains("."))
            {
                retVal = retVal.Substring(0, retVal.IndexOf("."));
            }
            if (retVal.Contains("-"))
            {
                retVal = '-' + (retVal.Trim('-'));
            }
            if (retVal.Contains("/"))
            {
                string[] arrDate = retVal.Split('/');
                retVal = arrDate[2] + "-" + arrDate[1] + "-" + arrDate[0];
            }
        }
        return retVal;
    }

    private string RemoveSpace(string val)
    {
        string retVal;
        if (val != string.Empty)
            retVal = val.Trim(' ');
        else
            retVal = null;
        return retVal;
    }
}
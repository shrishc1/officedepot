using BusinessEntities.ModuleBE.Upload;
using BusinessLogicLayer.ModuleBAL.Upload;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

/// <summary>
/// Summary description for ClsPurchaseOrderImport
/// </summary>
public class ClsPurchaseOrderImport
{
    public ClsPurchaseOrderImport()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    public string UploadPurchaseOrder(string purchaseOrderFilePath, string purchaseOrderFileName)
    {
        int TotalRecordCount = 0;
        int DataElementCount = 0;
        string individualColumnName = "";

        /// -------------- Added on 16 Feb 2013 ------------------------
        List<UP_DataImportErrorBE> UP_DataImportErrorBEList = new List<UP_DataImportErrorBE>();
        ImportDB importDB = new ImportDB();

        DateTime ImportDate = importDB.GetFileDate(purchaseOrderFileName);
        /// ------------------------------------------------------------

        try
        {
            using (StreamReader oStreamReader = new StreamReader(purchaseOrderFilePath))
            {
                bool IsSnooty = false;

                GlobalVariable.textFileLine = oStreamReader.ReadLine().ToLower();

                var columns = GlobalVariable.textFileLine.Split(GlobalVariable.delimiters);

                if (!columns.Contains("snooty"))
                {
                    if (columns.Length != 22)
                    {
                        return "Invalid File 1";
                    }
                }
                else
                {
                    if (columns.Length != 23)
                    {
                        return "Invalid File 1";
                    }
                }

                if (columns.Length == 23)
                {
                    IsSnooty = true;
                }

                List<Up_PurchaseOrderDetailBE> Up_PurchaseOrderDetailBEList = new List<Up_PurchaseOrderDetailBE>();

                while ((GlobalVariable.textFileLine = oStreamReader.ReadLine()) != null)
                {
                    DataElementCount = 0;

                    Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE = new Up_PurchaseOrderDetailBE();

                    /// -------------- Added on 16 Feb 2013 ------------------------
                    UP_DataImportErrorBE oUP_DataImportErrorBE = new UP_DataImportErrorBE();
                    string errorMessage = string.Empty;
                    /// ------------------------------------------------------------

                    GlobalVariable.textFileFieldValues = GlobalVariable.textFileLine.Split(GlobalVariable.delimiters);
                    for (int i = 0; i <= GlobalVariable.textFileFieldValues.Length - 1; i++)
                    {
                        individualColumnName = columns[i];

                        switch (individualColumnName.ToLower())
                        {
                            case "purchase_order":
                                /// -------------- Added on 16 Feb 2013 ------------------------
                                GlobalVariable.textFileFieldValues[i] = GlobalVariable.textFileFieldValues[i].Replace("'", "''");
                                if (!string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[i]) && GlobalVariable.textFileFieldValues[i].Length > 15)
                                {
                                    errorMessage += importDB.GetErrorMessage(individualColumnName, GlobalVariable.textFileFieldValues[i], 15);
                                    oUp_PurchaseOrderDetailBE.Purchase_order = GlobalVariable.textFileFieldValues[i].Substring(0, 15);
                                }
                                else
                                    oUp_PurchaseOrderDetailBE.Purchase_order = GlobalVariable.textFileFieldValues[i];
                                /// ------------------------------------------------------------
                                DataElementCount++;
                                break;
                            case "line_no":
                                GlobalVariable.textFileFieldValues[i] = GlobalVariable.textFileFieldValues[i].Replace("'", "''");
                                int line_no;
                                if (!string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[i].Trim()) && Int32.TryParse(RemoveDecimal(GlobalVariable.textFileFieldValues[i].Trim()), out line_no))
                                    oUp_PurchaseOrderDetailBE.Line_No = line_no; // Convert.ToInt32(RemoveDecimal(GlobalVariable.textFileFieldValues[i].Replace("'", "''")));
                                DataElementCount++;
                                break;
                            case "warehouse":
                                /// -------------- Added on 16 Feb 2013 ------------------------
                                GlobalVariable.textFileFieldValues[i] = GlobalVariable.textFileFieldValues[i].Replace("'", "''");
                                if (!string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[i]) && GlobalVariable.textFileFieldValues[i].Length > 10)
                                {
                                    errorMessage += importDB.GetErrorMessage(individualColumnName, GlobalVariable.textFileFieldValues[i], 10);
                                    oUp_PurchaseOrderDetailBE.Warehouse = GlobalVariable.textFileFieldValues[i].Substring(0, 10);
                                }
                                else
                                    oUp_PurchaseOrderDetailBE.Warehouse = GlobalVariable.textFileFieldValues[i];
                                /// ------------------------------------------------------------
                                DataElementCount++;
                                break;
                            case "vendor_no":
                                /// -------------- Added on 16 Feb 2013 ------------------------
                                GlobalVariable.textFileFieldValues[i] = GlobalVariable.textFileFieldValues[i].Replace("'", "''");
                                if (!string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[i]) && GlobalVariable.textFileFieldValues[i].Length > 15)
                                {
                                    errorMessage += importDB.GetErrorMessage(individualColumnName, GlobalVariable.textFileFieldValues[i], 15);
                                    oUp_PurchaseOrderDetailBE.Vendor_No = GlobalVariable.textFileFieldValues[i].Substring(0, 15);
                                }
                                else
                                    oUp_PurchaseOrderDetailBE.Vendor_No = GlobalVariable.textFileFieldValues[i];
                                /// ------------------------------------------------------------
                                DataElementCount++;
                                break;
                            case "subvndr":
                                /// -------------- Added on 16 Feb 2013 ------------------------
                                GlobalVariable.textFileFieldValues[i] = GlobalVariable.textFileFieldValues[i].Replace("'", "''");
                                if (!string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[i]) && GlobalVariable.textFileFieldValues[i].Length > 10)
                                {
                                    errorMessage += importDB.GetErrorMessage(individualColumnName, GlobalVariable.textFileFieldValues[i], 10);
                                    oUp_PurchaseOrderDetailBE.SubVendor = GlobalVariable.textFileFieldValues[i].Substring(0, 10);
                                }
                                else
                                    oUp_PurchaseOrderDetailBE.SubVendor = GlobalVariable.textFileFieldValues[i];
                                /// ------------------------------------------------------------
                                DataElementCount++;
                                break;
                            case "direct_code":
                                /// -------------- Added on 16 Feb 2013 ------------------------
                                GlobalVariable.textFileFieldValues[i] = GlobalVariable.textFileFieldValues[i].Replace("'", "''");
                                if (!string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[i]) && GlobalVariable.textFileFieldValues[i].Length > 15)
                                {
                                    errorMessage += importDB.GetErrorMessage(individualColumnName, GlobalVariable.textFileFieldValues[i], 15);
                                    oUp_PurchaseOrderDetailBE.Direct_code = GlobalVariable.textFileFieldValues[i].Substring(0, 15);
                                }
                                else
                                    oUp_PurchaseOrderDetailBE.Direct_code = GlobalVariable.textFileFieldValues[i];
                                /// ------------------------------------------------------------
                                DataElementCount++;
                                break;
                            case "od_code":
                                /// -------------- Added on 16 Feb 2013 ------------------------
                                GlobalVariable.textFileFieldValues[i] = GlobalVariable.textFileFieldValues[i].Replace("'", "''");
                                if (!string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[i]) && GlobalVariable.textFileFieldValues[i].Length > 25)
                                {
                                    errorMessage += importDB.GetErrorMessage(individualColumnName, GlobalVariable.textFileFieldValues[i], 25);
                                    oUp_PurchaseOrderDetailBE.OD_Code = GlobalVariable.textFileFieldValues[i].Substring(0, 25);
                                }
                                else
                                    oUp_PurchaseOrderDetailBE.OD_Code = GlobalVariable.textFileFieldValues[i];
                                /// ------------------------------------------------------------
                                DataElementCount++;
                                break;
                            case "vendor_code":
                                /// -------------- Added on 16 Feb 2013 ------------------------
                                GlobalVariable.textFileFieldValues[i] = GlobalVariable.textFileFieldValues[i].Replace("'", "''");
                                if (!string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[i]) && GlobalVariable.textFileFieldValues[i].Length > 50)
                                {
                                    errorMessage += importDB.GetErrorMessage(individualColumnName, GlobalVariable.textFileFieldValues[i], 50);
                                    oUp_PurchaseOrderDetailBE.Vendor_Code = GlobalVariable.textFileFieldValues[i].Substring(0, 50);
                                }
                                else
                                    oUp_PurchaseOrderDetailBE.Vendor_Code = GlobalVariable.textFileFieldValues[i];
                                /// ------------------------------------------------------------
                                DataElementCount++;
                                break;
                            case "product_description":
                                /// -------------- Added on 16 Feb 2013 ------------------------
                                GlobalVariable.textFileFieldValues[i] = GlobalVariable.textFileFieldValues[i].Replace("'", "''");
                                if (!string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[i]) && GlobalVariable.textFileFieldValues[i].Length > 50)
                                {
                                    errorMessage += importDB.GetErrorMessage(individualColumnName, GlobalVariable.textFileFieldValues[i], 50);
                                    oUp_PurchaseOrderDetailBE.Product_description = GlobalVariable.textFileFieldValues[i].Substring(0, 50);
                                }
                                else
                                    oUp_PurchaseOrderDetailBE.Product_description = GlobalVariable.textFileFieldValues[i];
                                /// ------------------------------------------------------------
                                DataElementCount++;
                                break;
                            case "uom":
                                /// -------------- Added on 16 Feb 2013 ------------------------
                                GlobalVariable.textFileFieldValues[i] = GlobalVariable.textFileFieldValues[i].Replace("'", "''");
                                if (!string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[i]) && GlobalVariable.textFileFieldValues[i].Length > 5)
                                {
                                    errorMessage += importDB.GetErrorMessage(individualColumnName, GlobalVariable.textFileFieldValues[i], 5);
                                    oUp_PurchaseOrderDetailBE.UOM = GlobalVariable.textFileFieldValues[i].Substring(0, 5);
                                }
                                else
                                    oUp_PurchaseOrderDetailBE.UOM = GlobalVariable.textFileFieldValues[i];
                                /// ------------------------------------------------------------
                                DataElementCount++;
                                break;
                            case "original_quantity":

                                oUp_PurchaseOrderDetailBE.Original_quantity = RemoveDecimal(GlobalVariable.textFileFieldValues[i].Replace("'", "''"));
                                DataElementCount++;
                                break;
                            case "outstanding_qty":
                                oUp_PurchaseOrderDetailBE.Outstanding_Qty = RemoveDecimal(GlobalVariable.textFileFieldValues[i].Replace("'", "''"));
                                DataElementCount++;
                                break;
                            case "po_cost":
                                if (!String.IsNullOrEmpty(GlobalVariable.textFileFieldValues[i].Replace("'", "''")))
                                    oUp_PurchaseOrderDetailBE.PO_cost = Convert.ToDecimal(RemoveSpace(GlobalVariable.textFileFieldValues[i].Replace("'", "''")));
                                DataElementCount++;
                                break;
                            case "order_raised":
                                if (!String.IsNullOrEmpty(GlobalVariable.textFileFieldValues[i].Replace("'", "''")))
                                {
                                    try
                                    {
                                        oUp_PurchaseOrderDetailBE.Order_raised = Convert.ToDateTime(RemoveDecimal(GlobalVariable.textFileFieldValues[i].Replace("'", "''")));
                                    }
                                    catch
                                    {
                                        errorMessage += individualColumnName + " has invalid date - " + GlobalVariable.textFileFieldValues[i] + " ";
                                    }
                                }
                                DataElementCount++;
                                break;
                            case "original_due_date":
                                if (!String.IsNullOrEmpty(GlobalVariable.textFileFieldValues[i].Replace("'", "''")))
                                {
                                    try
                                    {
                                        oUp_PurchaseOrderDetailBE.Original_due_date = Convert.ToDateTime(RemoveDecimal(GlobalVariable.textFileFieldValues[i].Replace("'", "''")));
                                    }
                                    catch
                                    {
                                        errorMessage += individualColumnName + " has invalid date - " + GlobalVariable.textFileFieldValues[i] + " ";
                                    }
                                }
                                DataElementCount++;
                                break;
                            case "expected_date":
                                if (!String.IsNullOrEmpty(GlobalVariable.textFileFieldValues[i].Replace("'", "''")))
                                {
                                    try
                                    {
                                        oUp_PurchaseOrderDetailBE.Expected_date = Convert.ToDateTime(RemoveDecimal(GlobalVariable.textFileFieldValues[i].Replace("'", "''")));
                                    }
                                    catch
                                    {
                                        errorMessage += individualColumnName + " has invalid date - " + GlobalVariable.textFileFieldValues[i] + " ";
                                    }
                                }
                                DataElementCount++;
                                break;
                            case "buyer_no":
                                /// -------------- Added on 16 Feb 2013 ------------------------
                                GlobalVariable.textFileFieldValues[i] = GlobalVariable.textFileFieldValues[i].Replace("'", "''");
                                if (!string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[i]) && GlobalVariable.textFileFieldValues[i].Length > 20)
                                {
                                    errorMessage += importDB.GetErrorMessage(individualColumnName, GlobalVariable.textFileFieldValues[i], 20);
                                    oUp_PurchaseOrderDetailBE.Buyer_no = GlobalVariable.textFileFieldValues[i].Substring(0, 20);
                                }
                                else
                                    oUp_PurchaseOrderDetailBE.Buyer_no = GlobalVariable.textFileFieldValues[i];
                                /// ------------------------------------------------------------
                                DataElementCount++;
                                break;
                            case "item_classification":
                                /// -------------- Added on 16 Feb 2013 ------------------------
                                GlobalVariable.textFileFieldValues[i] = GlobalVariable.textFileFieldValues[i].Replace("'", "''");
                                if (!string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[i]) && GlobalVariable.textFileFieldValues[i].Length > 5)
                                {
                                    errorMessage += importDB.GetErrorMessage(individualColumnName, GlobalVariable.textFileFieldValues[i], 5);
                                    oUp_PurchaseOrderDetailBE.Item_classification = GlobalVariable.textFileFieldValues[i].Substring(0, 5);
                                }
                                else
                                    oUp_PurchaseOrderDetailBE.Item_classification = GlobalVariable.textFileFieldValues[i];
                                /// ------------------------------------------------------------
                                DataElementCount++;
                                break;
                            case "item_type":
                                /// -------------- Added on 16 Feb 2013 ------------------------
                                GlobalVariable.textFileFieldValues[i] = GlobalVariable.textFileFieldValues[i].Replace("'", "''");
                                if (!string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[i]) && GlobalVariable.textFileFieldValues[i].Length > 10)
                                {
                                    errorMessage += importDB.GetErrorMessage(individualColumnName, GlobalVariable.textFileFieldValues[i], 10);
                                    oUp_PurchaseOrderDetailBE.Item_type = GlobalVariable.textFileFieldValues[i].Substring(0, 10);
                                }
                                else
                                    oUp_PurchaseOrderDetailBE.Item_type = GlobalVariable.textFileFieldValues[i];
                                /// ------------------------------------------------------------
                                DataElementCount++;
                                break;
                            case "item_category":
                                /// -------------- Added on 16 Feb 2013 ------------------------
                                GlobalVariable.textFileFieldValues[i] = GlobalVariable.textFileFieldValues[i].Replace("'", "''");
                                if (!string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[i]) && GlobalVariable.textFileFieldValues[i].Length > 10)
                                {
                                    errorMessage += importDB.GetErrorMessage(individualColumnName, GlobalVariable.textFileFieldValues[i], 10);
                                    oUp_PurchaseOrderDetailBE.Item_Category = GlobalVariable.textFileFieldValues[i].Substring(0, 10);
                                }
                                else
                                    oUp_PurchaseOrderDetailBE.Item_Category = GlobalVariable.textFileFieldValues[i];
                                /// ------------------------------------------------------------
                                DataElementCount++;
                                break;
                            case "other":
                                /// -------------- Added on 16 Feb 2013 ------------------------
                                GlobalVariable.textFileFieldValues[i] = GlobalVariable.textFileFieldValues[i].Replace("'", "''");
                                if (!string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[i]) && GlobalVariable.textFileFieldValues[i].Length > 500)
                                {
                                    errorMessage += importDB.GetErrorMessage(individualColumnName, GlobalVariable.textFileFieldValues[i], 500);
                                    oUp_PurchaseOrderDetailBE.Other = GlobalVariable.textFileFieldValues[i].Substring(0, 500);
                                }
                                else
                                    oUp_PurchaseOrderDetailBE.Other = GlobalVariable.textFileFieldValues[i];
                                /// ------------------------------------------------------------
                                DataElementCount++;
                                break;
                            case "country":
                                /// -------------- Added on 16 Feb 2013 ------------------------
                                GlobalVariable.textFileFieldValues[i] = GlobalVariable.textFileFieldValues[i].Replace("'", "''");
                                if (!string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[i]) && GlobalVariable.textFileFieldValues[i].Length > 20)
                                {
                                    errorMessage += importDB.GetErrorMessage(individualColumnName, GlobalVariable.textFileFieldValues[i], 20);
                                    oUp_PurchaseOrderDetailBE.CountryName = GlobalVariable.textFileFieldValues[i].Substring(0, 20);
                                }
                                else
                                    oUp_PurchaseOrderDetailBE.CountryName = GlobalVariable.textFileFieldValues[i];
                                /// ------------------------------------------------------------
                                DataElementCount++;
                                break;
                            case "snooty":

                                GlobalVariable.textFileFieldValues[i] = GlobalVariable.textFileFieldValues[i].Replace("'", "''");
                                if (!string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[i]) && GlobalVariable.textFileFieldValues[i].Length > 500)
                                {
                                    errorMessage += importDB.GetErrorMessage(individualColumnName, GlobalVariable.textFileFieldValues[i], 500);
                                    oUp_PurchaseOrderDetailBE.Snooty = GlobalVariable.textFileFieldValues[i];
                                }
                                else
                                    oUp_PurchaseOrderDetailBE.Snooty = GlobalVariable.textFileFieldValues[i];
                                DataElementCount++;
                                break;

                            default:
                                break;
                        }
                        oUp_PurchaseOrderDetailBE.UpdatedDate = ImportDate;
                    }

                    if (DataElementCount != 22)
                    {
                        if ((!string.IsNullOrEmpty(oUp_PurchaseOrderDetailBE.Snooty) && DataElementCount != 23))
                        {
                            continue;
                        }
                        else
                        {
                            Up_PurchaseOrderDetailBEList.Add(oUp_PurchaseOrderDetailBE);
                            if (!string.IsNullOrEmpty(errorMessage))
                            {
                                oUP_DataImportErrorBE.ErrorDescription = errorMessage.Substring(0, errorMessage.Length - 1) + " in file " + purchaseOrderFileName;
                                oUP_DataImportErrorBE.UpdatedDate = ImportDate;
                                UP_DataImportErrorBEList.Add(oUP_DataImportErrorBE);
                            }
                            TotalRecordCount += 1;
                        }
                    }
                    else
                    {
                        Up_PurchaseOrderDetailBEList.Add(oUp_PurchaseOrderDetailBE);
                        /// -------------- Added on 16 Feb 2013 ------------------------
                        if (!string.IsNullOrEmpty(errorMessage))
                        {
                            oUP_DataImportErrorBE.ErrorDescription = errorMessage.Substring(0, errorMessage.Length - 1) + " in file " + purchaseOrderFileName;
                            oUP_DataImportErrorBE.UpdatedDate = ImportDate;
                            UP_DataImportErrorBEList.Add(oUP_DataImportErrorBE);
                        }
                        /// ------------------------------------------------------------
                        TotalRecordCount += 1;
                    }
                }

                UP_PurchaseOrderDetailBAL oUP_PurchaseOrderDetailBAL = new UP_PurchaseOrderDetailBAL();
                oUP_PurchaseOrderDetailBAL.AddPurchaseOrderDetails(Up_PurchaseOrderDetailBEList, IsSnooty);
                Up_PurchaseOrderDetailBEList = null;
            }
        }
        catch (Exception ex)
        {
            return TotalRecordCount.ToString() + "=====" + DataElementCount.ToString() + "    Invalid File 3" + ex.Source + "---" + ex.InnerException.Message + "---" + ex.Message;
        }

        try
        {
            UP_ManualFileUploadBE oUP_ManualFileUploadBE = new UP_ManualFileUploadBE();
            oUP_ManualFileUploadBE.Action = "InsertManualUpload";
            oUP_ManualFileUploadBE.DateUploaded = ImportDate;
            oUP_ManualFileUploadBE.DownloadedFilename = purchaseOrderFileName;
            oUP_ManualFileUploadBE.UserID = Convert.ToInt32(System.Web.HttpContext.Current.Session["UserId"]);
            oUP_ManualFileUploadBE.TotalPORecord = TotalRecordCount;

            UP_ManualFileUploadBAL oUP_ManualFileUploadBAL = new UP_ManualFileUploadBAL();
            int? Result = oUP_ManualFileUploadBAL.addEditManualUploadBAL(oUP_ManualFileUploadBE);

            /// -------------- Added on 16 Feb 2013 ------------------------

            foreach (UP_DataImportErrorBE obj in UP_DataImportErrorBEList)
            {
                obj.ManualFileUploadID = Convert.ToInt32(Result);

            }
            UP_PurchaseOrderDetailBAL oUP_PurchaseOrderDetailBAL = new UP_PurchaseOrderDetailBAL();
            oUP_PurchaseOrderDetailBAL.AddPurchaseOrderDetailErrors(UP_DataImportErrorBEList);
            UP_DataImportErrorBEList = null;

            /// ------------------------------------------------------------


        }
        catch (Exception)
        {
        }

        return string.Empty;
    }
    private string RemoveDecimal(string val)
    {
        string retVal;
        retVal = RemoveSpace(val);

        if (retVal != null)
        {
            if (retVal.Contains("."))
            {
                retVal = retVal.Substring(0, retVal.IndexOf("."));
            }
            if (retVal.Contains("-"))
            {
                retVal = '-' + (retVal.Trim('-'));
            }
            if (retVal.Contains("/"))
            {
                string[] arrDate = retVal.Split('/');
                retVal = arrDate[2] + "-" + arrDate[1] + "-" + arrDate[0];
            }
        }
        return retVal;
    }
    private string RemoveSpace(string val)
    {
        string retVal;
        if (val != string.Empty)
            retVal = val.Trim(' ');
        else
            retVal = null;
        return retVal;
    }
}
﻿using System.Web.UI.WebControls;

/// <summary>
/// Summary description for clsDiscrepancy
/// </summary>
public class clsDiscrepancy
{
    public clsDiscrepancy()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public static bool ISRadioButtonChecked(params RadioButton[] radiobutton)
    {
        bool Ischecked = false;
        for (int i = 0; i < radiobutton.Length; i++)
        {
            if (Ischecked == true)
            {
                break;
            }
            if (radiobutton[i].Checked == true)
            {
                Ischecked = true;
            }
            else
            {
                Ischecked = false;
            }

        }
        return Ischecked;

    }

    public static bool ISRadioButtonListChecked(params RadioButtonList[] radiobuttonlist)
    {
        bool Ischecked = false;
        for (int i = 0; i < radiobuttonlist.Length; i++)
        {
            if (Ischecked == true)
            {
                break;
            }
            if (!string.IsNullOrEmpty(radiobuttonlist[i].SelectedValue))
            {
                Ischecked = true;
            }
            else
            {
                Ischecked = false;
            }

        }
        return Ischecked;

    }



}
﻿using BaseControlLibrary;
using System.Collections;
using System.Web.UI.WebControls;

/// <summary>
/// Summary description for FillControls
/// </summary>
public class FillControls
{
    public FillControls()
    {       //
            // TODO: Add constructor logic here	//
    }

    /// <summary>
    /// Method to filll dropdown list
    /// </summary>
    /// <param name="drpList">ref DropDownList, DropDownList to be bind</param>
    /// <param name="lstData">List, DataSource</param>
    /// <param name="dataText">string, DataTextField</param>
    /// <param name="dataValueFiels">string, DataValueField</param>
    /// <param name="initialText">string, Default selected text</param>
    public static void FillDropDown(ref ucDropdownList drpList, IList lstData, string dataText, string dataValueFields, string initialText)
    {
        drpList.Items.Clear();
        drpList.DataSource = lstData;
        drpList.DataTextField = dataText;
        drpList.DataValueField = dataValueFields;
        drpList.DataBind();
        drpList.Items.Insert(0, new ListItem("--" + initialText + "--", "0"));
    }

    /// <summary>
    /// Method to filll dropdown list
    /// </summary>
    /// <param name="drpList">ref DropDownList, DropDownList to be bind</param>
    /// <param name="lstData">List, DataSource</param>
    /// <param name="dataText">string, DataTextField</param>    
    public static void FillDropDown(ref ucDropdownList drpList, IList lstData, string dataText, string dataValueFields)
    {
        drpList.Items.Clear();
        drpList.DataSource = lstData;
        drpList.DataTextField = dataText;
        drpList.DataValueField = dataValueFields;
        drpList.DataBind();
    }

    public static void FillDropDown(ref ucDropdownList drpList, System.Data.DataTable dt, string dataText, string dataValueFields)
    {
        drpList.Items.Clear();
        drpList.DataSource = dt;
        drpList.DataTextField = dataText;
        drpList.DataValueField = dataValueFields;
        drpList.DataBind();
    }

    public static void FillDropDown(ref ucDropdownList drpList, System.Data.DataTable dt, string dataText, string dataValueFields, string initialText)
    {
        drpList.Items.Clear();
        drpList.DataSource = dt;
        drpList.DataTextField = dataText;
        drpList.DataValueField = dataValueFields;
        drpList.DataBind();
        drpList.Items.Insert(0, new ListItem("---Select---", "0"));
    }

    /// <summary>
    /// Method to fill listbox
    /// </summary>
    /// <param name="lstList"></param>
    /// <param name="lstData"></param>
    /// <param name="dataText"></param>
    /// <param name="dataValueFields"></param>
    /// <param name="initialText"></param>
    public static void FillListBox(ref ucListBox lstList, IList lstData, string dataText, string dataValueFields, string initialText)
    {
        lstList.Items.Clear();
        lstList.DataSource = lstData;
        lstList.DataTextField = dataText;
        lstList.DataValueField = dataValueFields;
        lstList.DataBind();
        lstList.Items.Insert(0, new ListItem("--" + initialText + "--", "0"));
    }

    /// <summary>
    /// Method to fill listbox without initial value
    /// </summary>
    /// <param name="lstList"></param>
    /// <param name="lstData"></param>
    /// <param name="dataText"></param>
    /// <param name="dataValueFields"></param>
    public static void FillListBox(ref ucListBox lstList, IList lstData, string dataText, string dataValueFields)
    {
        lstList.Items.Clear();
        lstList.DataSource = lstData;
        lstList.DataTextField = dataText;
        lstList.DataValueField = dataValueFields;
        lstList.DataBind();
    }

    public static void FillListBox(ref ucListBox lstList, System.Data.DataTable dt, string dataText, string dataValueFields)
    {
        lstList.Items.Clear();
        lstList.DataSource = dt;
        lstList.DataTextField = dataText;
        lstList.DataValueField = dataValueFields;
        lstList.DataBind();
    }

    /// <summary>
    /// Move one list items to another 
    /// </summary>
    /// <param name="lstOne"></param>
    /// <param name="lstTwo"></param>
    /// <param name="moveDirection"></param>
    public static void MoveAllItems(ucListBox lstOne, ucListBox lstTwo)
    {
        for (int i = 0; i < lstOne.Items.Count; i++)
        {
            lstTwo.Items.Add(new ListItem(lstOne.Items[i].Text, lstOne.Items[i].Value));
        }
        lstOne.Items.Clear();
    }

    /// <summary>
    /// Move one list item to another
    /// </summary>
    /// <param name="lstOne"></param>
    /// <param name="lstTwo"></param>
    public static void MoveOneItem(ucListBox lstOne, ucListBox lstTwo)
    {
        string CurrentItemText = lstOne.SelectedItem.Text;
        string CurrentItemValue = lstOne.SelectedItem.Value;
        int CussrentItemIndex = lstOne.SelectedIndex;

        lstTwo.Items.Add(new ListItem(CurrentItemText, CurrentItemValue));
        lstOne.Items.RemoveAt(CussrentItemIndex);
    }

    /// <summary>
    /// Move one list item to another
    /// </summary>
    /// <param name="lstOne"></param>
    /// <param name="lstTwo"></param>
    public static void MoveItemRightToLeft(ucListBox lstOne, ucListBox lstTwo, string selectedCountry)
    {
        string CurrentItemText = lstOne.SelectedItem.Text;
        string CurrentItemValue = lstOne.SelectedItem.Value;
        int CussrentItemIndex = lstOne.SelectedIndex;
        if (CurrentItemText.IndexOf("(") > -1 && CurrentItemText.IndexOf(")") > -1 && CurrentItemText.Substring(CurrentItemText.IndexOf("(") + 1, CurrentItemText.IndexOf(")") - CurrentItemText.IndexOf("(") - 1).ToLower().Trim() == selectedCountry.ToLower().Trim())
        {
            CurrentItemText = CurrentItemText.Remove(CurrentItemText.IndexOf("("), CurrentItemText.IndexOf(")") - CurrentItemText.IndexOf("(") + 1);
            lstTwo.Items.Add(new ListItem(CurrentItemText.Trim(), CurrentItemValue));
        }
        lstOne.Items.RemoveAt(CussrentItemIndex);
    }
    public static void MoveItemLeftToRight(ucListBox lstOne, ucListBox lstTwo, string selectedCountry)
    {
        string CurrentItemText = lstOne.SelectedItem.Text;
        string CurrentItemValue = lstOne.SelectedItem.Value;
        int CussrentItemIndex = lstOne.SelectedIndex;

        lstTwo.Items.Add(new ListItem(CurrentItemText + " ( " + selectedCountry + " )", CurrentItemValue));
        lstOne.Items.RemoveAt(CussrentItemIndex);
    }

    /// <summary>
    /// Move one list items to another 
    /// </summary>
    /// <param name="lstOne"></param>
    /// <param name="lstTwo"></param>
    /// <param name="moveDirection"></param>
    public static void MoveAllItemsLeftToRight(ucListBox lstOne, ucListBox lstTwo, string selectedCountry)
    {
        for (int i = 0; i < lstOne.Items.Count; i++)
        {
            lstTwo.Items.Add(new ListItem(lstOne.Items[i].Text + " ( " + selectedCountry + " )", lstOne.Items[i].Value));
        }
        lstOne.Items.Clear();
    }
    /// <summary>
    /// Move one list items to another 
    /// </summary>
    /// <param name="lstOne"></param>
    /// <param name="lstTwo"></param>
    /// <param name="moveDirection"></param>
    public static void MoveAllItemsRightToLeft(ucListBox lstOne, ucListBox lstTwo, string selectedCountry)
    {
        for (int i = 0; i < lstOne.Items.Count; i++)
        {
            string CurrentItemText = lstOne.Items[i].Text;
            if (CurrentItemText.IndexOf("(") > -1 && CurrentItemText.IndexOf(")") > -1 && CurrentItemText.Substring(CurrentItemText.IndexOf("(") + 1, CurrentItemText.IndexOf(")") - CurrentItemText.IndexOf("(") - 1).ToLower().Trim() == selectedCountry.ToLower().Trim())
            {
                CurrentItemText = CurrentItemText.Remove(CurrentItemText.IndexOf("("), CurrentItemText.IndexOf(")") - CurrentItemText.IndexOf("(") + 1);
                lstTwo.Items.Add(new ListItem(CurrentItemText, lstOne.Items[i].Value));
            }
        }
        lstOne.Items.Clear();
    }
    /// <summary>
    /// Remove the selected item from the listbox
    /// </summary>
    /// <param name="lstBox"></param>
    public static void RemoveSelectedItem(ucListBox lstBox)
    {
        lstBox.Items.Remove(lstBox.Items.FindByValue(lstBox.SelectedItem.Value));
    }

    /// <summary>
    /// Method to filll checkboxlist list
    /// </summary>
    /// <param name="drpList">ref CheckBoxList, CheckBoxList to be bind</param>
    /// <param name="lstData">List, DataSource</param>
    /// <param name="dataText">string, DataTextField</param>    
    public static void FillCheckBoxList(ref ucCheckboxList chkList, IList lstData, string dataText, string dataValueFields)
    {
        chkList.Items.Clear();
        chkList.DataSource = lstData;
        chkList.DataTextField = dataText;
        chkList.DataValueField = dataValueFields;
        chkList.DataBind();
    }

    public static void FillCheckBoxList(ref ucCheckboxList chkList, System.Data.DataTable dt, string dataText, string dataValueFields)
    {
        chkList.Items.Clear();
        chkList.DataSource = dt;
        chkList.DataTextField = dataText;
        chkList.DataValueField = dataValueFields;
        chkList.DataBind();
    }

    public static void FillCheckBoxList(ref ucCheckboxList chkList, System.Data.DataTable dt, string dataText, string dataValueFields, string initialText)
    {
        chkList.Items.Clear();
        chkList.DataSource = dt;
        chkList.DataTextField = dataText;
        chkList.DataValueField = dataValueFields;
        chkList.DataBind();
        //chkList.Items.Insert(0, new ListItem("---Select---", "0"));
    }
}
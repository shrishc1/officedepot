﻿using BusinessEntities.ModuleBE.Languages;
using BusinessLogicLayer.ModuleBAL.Languages;
using System;
using System.Collections.Generic;
using System.Data;
using Utilities;

public class Utility
{
    public static string GetJson(DataTable dataTable)
    {
        var javaScriptSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
        var rows = new List<Dictionary<string, object>>();
        Dictionary<string, object> row = null;
        foreach (DataRow dataRow in dataTable.Rows)
        {
            row = new Dictionary<string, object>();
            foreach (DataColumn dataColumn in dataTable.Columns)
            {
                row.Add(dataColumn.ColumnName.Trim(), dataRow[dataColumn] is DateTime ? ((DateTime)dataRow[dataColumn]).ToString("dd/MM/yyyy") : dataRow[dataColumn]);
            }
            rows.Add(row);
        }
        return javaScriptSerializer.Serialize(rows);
    }

    //Add by Abhinav to fix the language cache lost problem
    //public static void LoadLanguageTagsOld() {
    //    try {
    //        //Get All LanguageTags From DB
    //        MAS_LanguageTagsBE oMAS_LanguageTagsBE = new MAS_LanguageTagsBE();
    //        MAS_LanguageTagsBAL oMAS_LanguageTagsBAL = new MAS_LanguageTagsBAL();

    //        oMAS_LanguageTagsBE.Action = "GetAllLanguageTags";
    //        List<MAS_LanguageTagsBE> lstLanguageTags = oMAS_LanguageTagsBAL.GetAllLanguageTagsBAL(oMAS_LanguageTagsBE);
    //        if (lstLanguageTags.Count > 0) {

    //            //HttpContext.Current.Cache.Insert("LanguageTagslist", lstLanguageTags); // add it to cache
    //            //HttpRuntime.Cache.Insert("LanguageTagslist", lstLanguageTags, null, DateTime.Now.AddMinutes(3), System.Web.Caching.Cache.NoSlidingExpiration);
    //            System.Web.HttpRuntime.Cache.Insert("LanguageTagslist", lstLanguageTags, null,
    //                           System.Web.Caching.Cache.NoAbsoluteExpiration,
    //                           System.Web.Caching.Cache.NoSlidingExpiration);
    //        }
    //    }
    //    catch (Exception ex) {
    //        LogUtility.SaveErrorLogEntry(ex);
    //    }
    //}

    //Add by Abhinav to fix the language cache lost problem
    public static void LoadLanguageTags()
    {
        try
        {
            //Get All LanguageTags From DB
            MAS_LanguageTagsBE oMAS_LanguageTagsBE = new MAS_LanguageTagsBE();
            MAS_LanguageTagsBAL oMAS_LanguageTagsBAL = new MAS_LanguageTagsBAL();

            oMAS_LanguageTagsBE.Action = "GetUniqueLanguageTags";
            DataTable dtLanguage = oMAS_LanguageTagsBAL.GetLanguageTagsDicBAL(oMAS_LanguageTagsBE);

            Dictionary<string, string> dictionaryEnglish = new Dictionary<string, string>();
            Dictionary<string, string> dictionaryFrench = new Dictionary<string, string>();
            Dictionary<string, string> dictionaryGerman = new Dictionary<string, string>();
            Dictionary<string, string> dictionaryDutch = new Dictionary<string, string>();
            Dictionary<string, string> dictionarySpanish = new Dictionary<string, string>();
            Dictionary<string, string> dictionaryItalian = new Dictionary<string, string>();
            Dictionary<string, string> dictionaryCzech = new Dictionary<string, string>();

            foreach (DataRow dr in dtLanguage.Rows)
            {

                dictionaryEnglish.Add(dr["LanguageKey"].ToString(), dr["English"] == DBNull.Value ? null : Convert.ToString(dr["English"]));
                dictionaryFrench.Add(dr["LanguageKey"].ToString(), dr["French"] == DBNull.Value ? null : Convert.ToString(dr["French"]));
                dictionaryGerman.Add(dr["LanguageKey"].ToString(), dr["German"] == DBNull.Value ? null : Convert.ToString(dr["German"]));
                dictionaryDutch.Add(dr["LanguageKey"].ToString(), dr["Dutch"] == DBNull.Value ? null : Convert.ToString(dr["Dutch"]));
                dictionarySpanish.Add(dr["LanguageKey"].ToString(), dr["Spanish"] == DBNull.Value ? null : Convert.ToString(dr["Spanish"]));
                dictionaryItalian.Add(dr["LanguageKey"].ToString(), dr["Italian"] == DBNull.Value ? null : Convert.ToString(dr["Italian"]));
                dictionaryCzech.Add(dr["LanguageKey"].ToString(), dr["Czech"] == DBNull.Value ? null : Convert.ToString(dr["Czech"]));

            }

            if (dtLanguage.Rows.Count > 0)
            {

                System.Web.HttpRuntime.Cache.Insert("LanguageTagsEnglish", dictionaryEnglish, null,
                               System.Web.Caching.Cache.NoAbsoluteExpiration,
                               System.Web.Caching.Cache.NoSlidingExpiration);
                System.Web.HttpRuntime.Cache.Insert("LanguageTagsFrench", dictionaryFrench, null,
                              System.Web.Caching.Cache.NoAbsoluteExpiration,
                              System.Web.Caching.Cache.NoSlidingExpiration);
                System.Web.HttpRuntime.Cache.Insert("LanguageTagsGerman", dictionaryGerman, null,
                              System.Web.Caching.Cache.NoAbsoluteExpiration,
                              System.Web.Caching.Cache.NoSlidingExpiration);
                System.Web.HttpRuntime.Cache.Insert("LanguageTagsDutch", dictionaryDutch, null,
                              System.Web.Caching.Cache.NoAbsoluteExpiration,
                              System.Web.Caching.Cache.NoSlidingExpiration);
                System.Web.HttpRuntime.Cache.Insert("LanguageTagsSpanish", dictionarySpanish, null,
                              System.Web.Caching.Cache.NoAbsoluteExpiration,
                              System.Web.Caching.Cache.NoSlidingExpiration);
                System.Web.HttpRuntime.Cache.Insert("LanguageTagsItalian", dictionaryItalian, null,
                              System.Web.Caching.Cache.NoAbsoluteExpiration,
                              System.Web.Caching.Cache.NoSlidingExpiration);
                System.Web.HttpRuntime.Cache.Insert("LanguageTagsCzech", dictionaryCzech, null,
                             System.Web.Caching.Cache.NoAbsoluteExpiration,
                             System.Web.Caching.Cache.NoSlidingExpiration);
            }
        }
        catch (Exception ex)
        {
            LogUtility.SaveErrorLogEntry(ex);
        }
    }
    public static List<T> MergeListCollections<T>(List<T> firstList, List<T> secondList)
    {
        List<T> mergedList = new List<T>();
        mergedList.InsertRange(0, firstList);
        mergedList.InsertRange(mergedList.Count, secondList);
        return mergedList;
    }
}
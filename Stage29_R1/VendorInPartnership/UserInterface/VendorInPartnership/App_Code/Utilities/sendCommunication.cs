﻿using BusinessEntities.ModuleBE.AdminFunctions;
using BusinessEntities.ModuleBE.Appointment.Booking;
using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using BusinessEntities.ModuleBE.Discrepancy.LogDiscrepancy;
using BusinessEntities.ModuleBE.Languages.Languages;
using BusinessLogicLayer.ModuleBAL.AdminFunctions;
using BusinessLogicLayer.ModuleBAL.Appointment.Booking;
using BusinessLogicLayer.ModuleBAL.Appointment.SiteSettings;
using BusinessLogicLayer.ModuleBAL.Discrepancy.LogDiscrepancy;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using Utilities;
using WebUtilities;

/// <summary>
/// Summary description for sendCommunication
/// </summary>
public class sendCommunication : System.Web.UI.Page
{

    string path = AppDomain.CurrentDomain.BaseDirectory.ToLower();
    public string templatesPath = @"emailtemplates/communication1";
    public string templatesPathComments = @"emailtemplates/DiscrepancyComments";
    public int? iVendorID = 0;
    string sPortalLink = Convert.ToString(ConfigurationManager.AppSettings["Portallink"]) + @"/ModuleUI/Security/Login.aspx";
    string sFromAddress = Convert.ToString(ConfigurationManager.AppSettings["fromAddress"]);
    string OurCode = WebCommon.getGlobalResourceValue("OurCode");
    string Description = WebCommon.getGlobalResourceValue("Description1");
    string VendorItemCode = WebCommon.getGlobalResourceValue("VendorItemCode");
    string OutstandingPOQty = WebCommon.getGlobalResourceValue("OutstandingPOQty");
    string DeliveryNoteQty = WebCommon.getGlobalResourceValue("DeliveryNoteQty");
    string DeliveredQty = WebCommon.getGlobalResourceValue("DeliveredQty");
    string QtyOverDelivered = WebCommon.getGlobalResourceValue("QtyOverDelivered");
    string ReceivedQuantity = WebCommon.getGlobalResourceValue("ReceivedQuantity1");
    string ShortageQuantity = WebCommon.getGlobalResourceValue("ShortageQuantity");
    string DamagedQuantity = WebCommon.getGlobalResourceValue("DamagedQuantity");
    string DamagedDescription = WebCommon.getGlobalResourceValue("DamagedDescription");
    string RequiredCode = WebCommon.getGlobalResourceValue("RequiredCode");
    string UOM = WebCommon.getGlobalResourceValue("UOM");
    string CodeReceived = WebCommon.getGlobalResourceValue("CodeReceived");
    string QuantityAdvisedOnDNote = WebCommon.getGlobalResourceValue("QuantityAdvisedOnDNote");
    string QuantityPaperworkAmendedto = WebCommon.getGlobalResourceValue("QuantityPaperworkAmendedto");
    string POPackSize = WebCommon.getGlobalResourceValue("POPackSize");
    string PackSizeofgooddelivered = WebCommon.getGlobalResourceValue("PackSizeofgooddelivered");
    string strLine = WebCommon.getGlobalResourceValue("Line");
    string strODCode = WebCommon.getGlobalResourceValue("ODCode");
    string strVikingCode = WebCommon.getGlobalResourceValue("VikingCode");
    string strOriginalPOQty = WebCommon.getGlobalResourceValue("OriginalPOQty");
    string strType = WebCommon.getGlobalResourceValue("Type");
    string strDiscrepancyQty = WebCommon.getGlobalResourceValue("DiscrepancyQty");
    string strHi = WebCommon.getGlobalResourceValue("Hi");
    string strShuttleDiscMessage = WebCommon.getGlobalResourceValue("ShuttleDiscMessage");
    string strShortage = WebCommon.getGlobalResourceValue("Shortage");
    string strManyThanks = WebCommon.getGlobalResourceValue("ManyThanks");
    string ChaseQuantity = WebCommon.getGlobalResourceValue("ChaseQuantity");
    string Comments = WebCommon.getGlobalResourceValue("Comments");
    string NoofPacksReceived = WebCommon.getGlobalResourceValue("NoofPacksReceived");
    string DiscrepancyComment = WebCommon.getGlobalResourceValue("DiscrepancyComment");
    public StringBuilder sSendMailLink = new StringBuilder();
    public StringBuilder sLetterLink = new StringBuilder();
    sendCommunicationCommon oSendCommunicationCommon = new sendCommunicationCommon();
    string sLetterType = null;

    public int? sendCommunicationByEMail(int iDiscrepancy, string discrepancyType, BaseControlLibrary.ucTextbox txtAltEmail,
        BaseControlLibrary.ucTextbox ucTextBox1, int? vendorID, string queryDiscDate = "", string vendorComment = "",
        string odComment = "", int queryDiscID = 0, bool IsFromVendor = false)
    {
        try
        {
            string sToAddress = string.Empty;
            if (txtAltEmail != null && !string.IsNullOrEmpty(txtAltEmail.Text))
                sToAddress = txtAltEmail.Text;
            else if (ucTextBox1 != null && !string.IsNullOrEmpty(ucTextBox1.Text))
                sToAddress = ucTextBox1.Text;

            sToAddress = sToAddress.Replace(" ", "");

            if (!string.IsNullOrEmpty(sToAddress))
            {
                iVendorID = vendorID;
                return sendAndSaveMail(iDiscrepancy, discrepancyType, sToAddress, queryDiscDate, vendorComment, odComment, queryDiscID, IsFromVendor);
            }
            else
                oSendCommunicationCommon.showErrorMessage();

            return null;
        }
        catch (Exception)
        {
            return null;
        }
    }

    public int? sendCommunicationByEMail2(int iDiscrepancy, string discrepancyType, string EmailList,
       int? vendorID, string queryDiscDate = "", string vendorComment = "", string odComment = "", int queryDiscID = 0, string LetterType = "")
    {
        try
        {
            sLetterType = LetterType;
            string sToAddress = string.Empty;
            sToAddress = EmailList;

            sToAddress = sToAddress.Replace(" ", "");

            if (!string.IsNullOrEmpty(sToAddress))
            {
                iVendorID = vendorID;
                return sendAndSaveMail(iDiscrepancy, discrepancyType, sToAddress, queryDiscDate, vendorComment, odComment, queryDiscID);
            }
            else
                oSendCommunicationCommon.showErrorMessage();

            return null;
        }
        catch (Exception)
        {
            return null;
        }
    }


    public int? sendAndSaveMail(int iDiscrepancyLogID, string discrepancyType, string toAddress,
        string queryDiscDate = "", string vendorComment = "", string odComment = "", int queryDiscID = 0, bool IsFromVendor = false)
    {
        string EmailSubjectText = string.Empty;
        string htmlBody = string.Empty;
        try
        {
            List<DiscrepancyBE> lstDisLog = oSendCommunicationCommon.getDiscrepancyLogDetail("GetDiscrepancyLog", Convert.ToInt32(iDiscrepancyLogID), discrepancyType);
            List<DiscrepancyBE> lstDetails = oSendCommunicationCommon.getDiscrepancyItemDetail("GetDiscrepancyItem", Convert.ToInt32(iDiscrepancyLogID), discrepancyType);
            List<DISLog_WorkFlowBE> lstWorkFlowActionDetails = oSendCommunicationCommon.getDiscrepancyWorkFlowActionDetail("GetWorkFlowActionDetails", Convert.ToInt32(iDiscrepancyLogID));
            List<DiscrepancyBE> lstStockPlanner = null;

            string sDiscrepancyRelatedText, sLine1, sLine2 = string.Empty;

            DataTable dt = oSendCommunicationCommon.GetUserPhoneNumber("GetuserPhoneNumber", Convert.ToInt32(Session["UserID"]));

            if (queryDiscID == 0 && discrepancyType != "deleteddiscrepancy")
            {
                if (discrepancyType.ToLower() != "presentationissue" && discrepancyType.ToLower() != "paperworkamended" && discrepancyType.ToLower() != "invoice query")
                {
                    lstStockPlanner = lstDetails[0].ActualStockPlannerID.HasValue
                        ? oSendCommunicationCommon.GetSPInitialCommDetails("GetSPInitialCommDetails", Convert.ToInt32(lstDetails[0].ActualStockPlannerID.Value))
                        : oSendCommunicationCommon.GetSPInitialCommDetails("GetSPInitialCommDetails", Convert.ToInt32(lstDetails[0].StockPlannerID.Value));
                }
            }

            if (discrepancyType.ToLower() == "reservation" || discrepancyType.ToLower() == "itemnotonpo")
            {
                int? retValue = 0;
                if (queryDiscID == 0 && discrepancyType != "deleteddiscrepancy")
                {
                    retValue = sendInitialMailToSPForReservandItemNotOnPODiscrepancy(iDiscrepancyLogID, lstDisLog, lstDetails,
                                                                    lstWorkFlowActionDetails, discrepancyType, lstStockPlanner, queryDiscID, IsFromVendor);
                }
            }
            else
            {

                if (lstStockPlanner != null && lstStockPlanner.Count > 0)
                {
                    int? retValue = null;

                    string SPLanguage = lstStockPlanner[0].User.Language;
                    if (SPLanguage != null)
                    {
                        string sMailAddress = lstStockPlanner[0].StockPlannerEmailID;

                        int? SPLanguageId = Convert.ToInt32(lstStockPlanner[0].User.LanguageID);

                        APPBOK_CommunicationBAL oAPPBOK_CommunicationBAL = new APPBOK_CommunicationBAL();
                        List<MAS_LanguageBE> oLanguages = oAPPBOK_CommunicationBAL.GetLanguages();

                        foreach (MAS_LanguageBE objLanguage in oLanguages)
                        {
                            bool MailSentInLanguage = false;

                            if (objLanguage.Language.ToLower() == SPLanguage.ToLower())
                            {
                                MailSentInLanguage = true;
                            }

                            #region get file using languageid                    

                            string templateFile = null;
                            templateFile = path + templatesPath + "/";

                            if (discrepancyType.ToLower() == "generic")
                                templateFile += "dis_InitialCommunicationToSPGeneric";
                            else if (discrepancyType.ToLower() == "shuttle")
                                templateFile += "dis_InitialCommunicationToSPShuttle";
                            else
                                templateFile += "dis_InitialCommunicationToSP";

                            templateFile += ".english.htm";

                            // Changing Resourse file according to language                            

                            switch (objLanguage.LanguageID)
                            {
                                case 1:
                                    Page.UICulture = clsConstants.EnglishISO; //"en-US"; // English
                                    break;
                                case 2:
                                    Page.UICulture = clsConstants.FranceISO; //"fr"; // France
                                    break;
                                case 3:
                                    Page.UICulture = clsConstants.GermanyISO; //"de"; // German
                                    break;
                                case 4:
                                    Page.UICulture = clsConstants.NederlandISO; //"nl"; // Dutch
                                    break;
                                case 5:
                                    Page.UICulture = clsConstants.SpainISO; //"es"; // Spanish
                                    break;
                                case 6:
                                    Page.UICulture = clsConstants.ItalyISO; //"it"; // Italy
                                    break;
                                case 7:
                                    Page.UICulture = clsConstants.CzechISO; //"cz"; // Czech
                                    break;
                                default:
                                    Page.UICulture = clsConstants.EnglishISO; // "en-US"; // English
                                    break;
                            }

                            #endregion

                            #region Logic to get the exact maltilingual text based on the vendor user language ...
                            OurCode = WebCommon.getGlobalResourceValue("OurCode");
                            Description = WebCommon.getGlobalResourceValue("Description1");
                            VendorItemCode = WebCommon.getGlobalResourceValue("VendorItemCode");
                            OutstandingPOQty = WebCommon.getGlobalResourceValue("OutstandingPOQty");
                            DeliveryNoteQty = WebCommon.getGlobalResourceValue("DeliveryNoteQty");
                            DeliveredQty = WebCommon.getGlobalResourceValue("DeliveredQty");
                            QtyOverDelivered = WebCommon.getGlobalResourceValue("QtyOverDelivered");
                            ReceivedQuantity = WebCommon.getGlobalResourceValue("ReceivedQuantity1");
                            ShortageQuantity = WebCommon.getGlobalResourceValue("ShortageQuantity");
                            DamagedQuantity = WebCommon.getGlobalResourceValue("DamagedQuantity");
                            DamagedDescription = WebCommon.getGlobalResourceValue("DamagedDescription");
                            RequiredCode = WebCommon.getGlobalResourceValue("RequiredCode");
                            UOM = WebCommon.getGlobalResourceValue("UOM");
                            CodeReceived = WebCommon.getGlobalResourceValue("CodeReceived");
                            QuantityAdvisedOnDNote = WebCommon.getGlobalResourceValue("QuantityAdvisedOnDNote");
                            QuantityPaperworkAmendedto = WebCommon.getGlobalResourceValue("QuantityPaperworkAmendedto");
                            POPackSize = WebCommon.getGlobalResourceValue("POPackSize");
                            PackSizeofgooddelivered = WebCommon.getGlobalResourceValue("PackSizeofgooddelivered");
                            strLine = WebCommon.getGlobalResourceValue("Line");
                            strODCode = WebCommon.getGlobalResourceValue("ODCode");
                            strVikingCode = WebCommon.getGlobalResourceValue("VikingCode");
                            strOriginalPOQty = WebCommon.getGlobalResourceValue("OriginalPOQty");
                            strType = WebCommon.getGlobalResourceValue("Type");
                            strDiscrepancyQty = WebCommon.getGlobalResourceValue("DiscrepancyQty");
                            strHi = WebCommon.getGlobalResourceValue("Hi");
                            strShuttleDiscMessage = WebCommon.getGlobalResourceValue("ShuttleDiscMessage");
                            strShortage = WebCommon.getGlobalResourceValue("Shortage");
                            strManyThanks = WebCommon.getGlobalResourceValue("ManyThanks");
                            ChaseQuantity = WebCommon.getGlobalResourceValue("ChaseQuantity");
                            Comments = WebCommon.getGlobalResourceValue("Comments");


                            #endregion

                            string sProductGrid = getDiscrepancyItemGrid(discrepancyType, lstDisLog, lstDetails, out sDiscrepancyRelatedText, out sLine1, out sLine2);

                            if (System.IO.File.Exists(templateFile.ToLower()))
                            {
                                EmailSubjectText = WebCommon.getGlobalResourceValue("InitialCommToSPMailSubject");

                                string siteAddress = string.Empty, accountPayable = string.Empty, mailBodyToSaveInDB = string.Empty;
                                htmlBody = oSendCommunicationCommon.getMailBody(iDiscrepancyLogID, discrepancyType, lstDisLog, templateFile, sProductGrid, sLine1,
                                    sDiscrepancyRelatedText, out siteAddress, out accountPayable);

                                htmlBody = htmlBody.Replace("{POHash}", WebCommon.getGlobalResourceValue("POHash"));
                                htmlBody = htmlBody.Replace("{InvoiceNumHash}", WebCommon.getGlobalResourceValue("InvoiceNumHash"));
                                htmlBody = htmlBody.Replace("{DiscrepancyNo}", WebCommon.getGlobalResourceValue("DiscrepancyNo"));
                                htmlBody = htmlBody.Replace("{DearSirMadam}", WebCommon.getGlobalResourceValue("DearSirMadam"));
                                htmlBody = htmlBody.Replace("{InvoiceQueryComm1Line1}", WebCommon.getGlobalResourceValue("InvoiceQueryComm1Line1"));
                                htmlBody = htmlBody.Replace("{InvoiceQueryComm1Line2}", WebCommon.getGlobalResourceValue("InvoiceQueryComm1Line2"));
                                htmlBody = htmlBody.Replace("{Yoursfaithfully}", WebCommon.getGlobalResourceValue("Yoursfaithfully"));
                                htmlBody = htmlBody.Replace("{PortalLink}", "<a href=" + sPortalLink + ">" + sPortalLink + " </a>");
                                htmlBody = htmlBody.Replace("{DearSP}", WebCommon.getGlobalResourceValue("DearSP"));
                                htmlBody = htmlBody.Replace("{InitialCommToSPText1}", WebCommon.getGlobalResourceValue("InitialCommToSPText1"));
                                htmlBody = htmlBody.Replace("{InitialCommToSPText2}", WebCommon.getGlobalResourceValue("InitialCommToSPText2"));
                                htmlBody = htmlBody.Replace("{Detailsbelow}", WebCommon.getGlobalResourceValue("Detailsbelow"));

                                List<DiscrepancyBE> lstDisLogSiteVendor = null;
                                if (discrepancyType.ToLower() == "invoice query")
                                {
                                    lstDisLogSiteVendor = getDiscrepancyLogSiteVendorDetail(iDiscrepancyLogID, discrepancyType, out siteAddress, out accountPayable, "L");
                                }
                                if (discrepancyType.ToLower() == "invoice query")
                                {
                                    htmlBody = htmlBody.Replace("{InvoiceValue}", lstDisLog[0].DeliveryNoteNumber);
                                    htmlBody = htmlBody.Replace("{DiscrepancyValue}", lstDisLog[0].VDRNo);

                                    htmlBody = htmlBody.Replace("##SITENAME##", lstDisLog[0].Site.SiteName.Split('-').ElementAtOrDefault(1));

                                    htmlBody = htmlBody.Replace("{APClerkletter}", WebCommon.getGlobalResourceValue("APClerkletter"));

                                    htmlBody = htmlBody.Replace("{GoodsINTeam}", WebCommon.getGlobalResourceValue("GoodsINTeam"));
                                    htmlBody = htmlBody.Replace("{MediatorTeam}", WebCommon.getGlobalResourceValue("MediatorTeam"));

                                    htmlBody = htmlBody.Replace("{INVAPClerkContactNo}", dt.Rows[0][0].ToString());

                                    htmlBody = htmlBody.Replace("{APClerkName}", lstDisLogSiteVendor[0].StockPlannerName);

                                    htmlBody = !string.IsNullOrWhiteSpace(lstDisLogSiteVendor[0].StockPlannerContact)
                                        ? htmlBody.Replace("{APClerkContactNo}", lstDisLogSiteVendor[0].StockPlannerContact)
                                        : htmlBody.Replace("{APClerkContactNo}", string.Empty);

                                    htmlBody = htmlBody.Replace("{POValue}", lstDisLog[0].PurchaseOrderNumber);
                                     
                                    htmlBody = htmlBody.Replace("{GridValue}", sProductGrid);

                                }

                                if (discrepancyType.ToLower() == "QualityIssue")
                                {
                                    htmlBody = htmlBody.Replace("{ProductGrid}", sProductGrid);
                                }

                                 #region Logic to set the Shuttle types ...
                                 strShortage = string.Empty;
                                for (int intIndex = 0; intIndex < lstDetails.Count; intIndex++)
                                {
                                    if (!string.IsNullOrEmpty(strShortage) && !string.IsNullOrEmpty(lstDetails[intIndex].ShuttleType))
                                        strShortage = string.Format("{0}, {1}", strShortage, lstDetails[intIndex].ShuttleType);
                                    else
                                        strShortage = lstDetails[intIndex].ShuttleType;
                                }
                                #endregion
                                if (discrepancyType.ToLower() == "invoice" && sLetterType == "lettertype1")
                                    htmlBody = htmlBody.Replace("{APCommentsText}", lstWorkFlowActionDetails[1].Comment);
                                else if (discrepancyType.ToLower() == "invoice" && (sLetterType == "lettertype2" || sLetterType == "lettertype3"))
                                {
                                    htmlBody = htmlBody.Replace("{Comments} :", "");
                                    htmlBody = htmlBody.Replace("{APCommentsText}", "");
                                }

                                htmlBody = htmlBody.Replace("{Shortage}", strShortage);
                                htmlBody = htmlBody.Replace("{ManyThanksText}", strManyThanks);
                                htmlBody = htmlBody.Replace("{Comments}", Comments);

                                mailBodyToSaveInDB = htmlBody.Replace("{logoInnerPath}", oSendCommunicationCommon.getAbsolutePath());

                                retValue = oSendCommunicationCommon.saveMail(iDiscrepancyLogID, sMailAddress, mailBodyToSaveInDB, EmailSubjectText, objLanguage.LanguageID,
                                    "communication1", MailSentInLanguage, queryDiscID);
                                if (retValue == null)
                                    oSendCommunicationCommon.errorLog(iDiscrepancyLogID, EmailSubjectText, toAddress, htmlBody, "Error in save mail to database");



                                if (objLanguage.LanguageID == SPLanguageId)
                                {
                                    clsEmail oclsEmail = new clsEmail();
                                    oclsEmail.sendMail(sMailAddress, htmlBody, EmailSubjectText, sFromAddress, true);

                                    sSendMailLink.Append("<a target='_blank' href='" + Common.EncryptQuery("DISLog_ReSendCommunication.aspx?communicationid=" + retValue + "&CommunicationLevel=Communication1") + "' onclick='window.open(this.href, this.target,\"scrollbars=1,left=100px,top=100px,width=1000px,height=800px\"); return false;'>" + sMailAddress + "</a>" + System.Environment.NewLine);
                                }

                            }
                        }

                        //setting default Resourse file
                        Page.UICulture = Convert.ToString(Session["CultureInfo"]);
                        //return retValue;
                    }
                    else
                    {
                        oSendCommunicationCommon.errorLog(iDiscrepancyLogID, EmailSubjectText, toAddress, htmlBody, "No Data for Language");
                        //return null;
                    }
                }

            }

            //vendor Email
            if (lstDisLog != null && lstDisLog.Count > 0)
            {
                int? retValue = null;

                DataSet dsLanguage = oSendCommunicationCommon.getLanguage(iVendorID, toAddress);
                if (dsLanguage != null && dsLanguage.Tables.Count > 0 && dsLanguage.Tables[0].Rows.Count > 0)
                {
                    string[] sMailAddress = toAddress.Split(',');
                    for (int i = 0; i < dsLanguage.Tables[0].Rows.Count; i++)
                    {
                        string Language = dsLanguage.Tables[0].Rows[i]["Language"].ToString();
                        int? VendorLanguageId = Convert.ToInt32(dsLanguage.Tables[0].Rows[i]["languageid"]);


                        APPBOK_CommunicationBAL oAPPBOK_CommunicationBAL = new APPBOK_CommunicationBAL();
                        List<MAS_LanguageBE> oLanguages = oAPPBOK_CommunicationBAL.GetLanguages();

                        foreach (MAS_LanguageBE objLanguage in oLanguages)
                        {
                            bool MailSentInLanguage = false;

                            if (objLanguage.Language.ToLower() == Language.ToLower())
                            {
                                MailSentInLanguage = true;
                            }

                            #region get file using languageid
                            //path = path.Replace(@"\bin\debug", "");                       

                            string templateFile = null;
                            templateFile = path + templatesPath + "/";

                            if (discrepancyType.ToLower() == "generic")
                                templateFile += "dis_initialcommunicationGeneric";
                            else if (discrepancyType.ToLower() == "shuttle")
                                templateFile += "dis_Shuttle";
                            else if (discrepancyType.ToLower() == "invoice")
                                templateFile += "dis_Invoice";
                            else if (discrepancyType.ToLower() == "querydiscaccept")
                                templateFile += "DisputedAcceptLetterGeneric";
                            else if (discrepancyType.ToLower() == "querydiscreject")
                                templateFile += "DisputedRejectLetterGeneric";
                            else if (discrepancyType.ToLower() == "deleteddiscrepancy")
                                templateFile += "DeletedDiscrepanciesLetterGenericNew";
                            else if (discrepancyType.ToLower() == "invoice query")
                                templateFile += "InvoiceQuery";
                            else if (discrepancyType.ToLower() == "itemnotonpo")
                            {
                                templateFile += "ItemNotOnPo";
                            }
                            else
                                templateFile += "dis_initialcommunication";

                            //int? languageID = (int?)(dsLanguage.Tables[0].Rows[i]["languageid"] == DBNull.Value ? 1 : dsLanguage.Tables[0].Rows[i]["languageid"]);
                            //templateFile += "." + Convert.ToString(dsLanguage.Tables[0].Rows[i]["language"] == DBNull.Value ? "english" : dsLanguage.Tables[0].Rows[i]["language"]) + ".htm";
                            templateFile += ".english.htm";

                            // Changing Resourse file according to language                            

                            switch (objLanguage.LanguageID)
                            {
                                case 1:
                                    Page.UICulture = clsConstants.EnglishISO; //"en-US"; // English
                                    break;
                                case 2:
                                    Page.UICulture = clsConstants.FranceISO; //"fr"; // France
                                    break;
                                case 3:
                                    Page.UICulture = clsConstants.GermanyISO; //"de"; // German
                                    break;
                                case 4:
                                    Page.UICulture = clsConstants.NederlandISO; //"nl"; // Dutch
                                    break;
                                case 5:
                                    Page.UICulture = clsConstants.SpainISO; //"es"; // Spanish
                                    break;
                                case 6:
                                    Page.UICulture = clsConstants.ItalyISO; //"it"; // Italy
                                    break;
                                case 7:
                                    Page.UICulture = clsConstants.CzechISO; //"cz"; // Czech
                                    break;
                                default:
                                    Page.UICulture = clsConstants.EnglishISO; // "en-US"; // English
                                    break;
                            }

                            #endregion

                            #region Logic to get the exact maltilingual text based on the vendor user language ...
                            OurCode = WebCommon.getGlobalResourceValue("OurCode");
                            Description = WebCommon.getGlobalResourceValue("Description1");
                            VendorItemCode = WebCommon.getGlobalResourceValue("VendorItemCode");
                            OutstandingPOQty = WebCommon.getGlobalResourceValue("OutstandingPOQty");
                            DeliveryNoteQty = WebCommon.getGlobalResourceValue("DeliveryNoteQty");
                            DeliveredQty = WebCommon.getGlobalResourceValue("DeliveredQty");
                            QtyOverDelivered = WebCommon.getGlobalResourceValue("QtyOverDelivered");
                            ReceivedQuantity = WebCommon.getGlobalResourceValue("ReceivedQuantity1");
                            ShortageQuantity = WebCommon.getGlobalResourceValue("ShortageQuantity");
                            DamagedQuantity = WebCommon.getGlobalResourceValue("DamagedQuantity");
                            DamagedDescription = WebCommon.getGlobalResourceValue("DamagedDescription");
                            RequiredCode = WebCommon.getGlobalResourceValue("RequiredCode");
                            UOM = WebCommon.getGlobalResourceValue("UOM");
                            CodeReceived = WebCommon.getGlobalResourceValue("CodeReceived");
                            QuantityAdvisedOnDNote = WebCommon.getGlobalResourceValue("QuantityAdvisedOnDNote");
                            QuantityPaperworkAmendedto = WebCommon.getGlobalResourceValue("QuantityPaperworkAmendedto");
                            POPackSize = WebCommon.getGlobalResourceValue("POPackSize");
                            PackSizeofgooddelivered = WebCommon.getGlobalResourceValue("PackSizeofgooddelivered");
                            strLine = WebCommon.getGlobalResourceValue("Line");
                            strODCode = WebCommon.getGlobalResourceValue("ODCode");
                            strVikingCode = WebCommon.getGlobalResourceValue("VikingCode");
                            strOriginalPOQty = WebCommon.getGlobalResourceValue("OriginalPOQty");
                            strType = WebCommon.getGlobalResourceValue("Type");
                            strDiscrepancyQty = WebCommon.getGlobalResourceValue("DiscrepancyQty");
                            strHi = WebCommon.getGlobalResourceValue("Hi");
                            strShuttleDiscMessage = WebCommon.getGlobalResourceValue("ShuttleDiscMessage");
                            strShortage = WebCommon.getGlobalResourceValue("Shortage");
                            strManyThanks = WebCommon.getGlobalResourceValue("ManyThanks");
                            ChaseQuantity = WebCommon.getGlobalResourceValue("ChaseQuantity");
                            Comments = WebCommon.getGlobalResourceValue("Comments");


                            #endregion

                            string sProductGrid = getDiscrepancyItemGrid(discrepancyType, lstDisLog, lstDetails, out sDiscrepancyRelatedText, out sLine1, out sLine2);

                            if (System.IO.File.Exists(templateFile.ToLower()))
                            {

                                if (discrepancyType.ToLower() == "querydiscaccept")
                                    EmailSubjectText = WebCommon.getGlobalResourceValue("QueryDiscrepancyAcceptanceLetter");
                                else if (discrepancyType.ToLower() == "querydiscreject")
                                    EmailSubjectText = WebCommon.getGlobalResourceValue("QueryDiscrepancyRejectionLetter");
                                else if (discrepancyType.ToLower() == "deleteddiscrepancy")
                                    EmailSubjectText = WebCommon.getGlobalResourceValue("DeletedDiscrepanciesLetterNew").Replace("##VDRNo##", lstDisLog[0].VDRNo);
                                else
                                    EmailSubjectText = WebCommon.getGlobalResourceValue("EmailSubjectTextForSendCommunication").Replace("##VDRNo##", lstDisLog[0].VDRNo);

                                string siteAddress = string.Empty, accountPayable = string.Empty, mailBodyToSaveInDB = string.Empty;
                                htmlBody = oSendCommunicationCommon.getMailBody(iDiscrepancyLogID, discrepancyType, lstDisLog, templateFile, sProductGrid, sLine1,
                                    sDiscrepancyRelatedText, out siteAddress, out accountPayable, string.Empty);

                                if (discrepancyType.ToLower().Equals("querydiscaccept") || discrepancyType.ToLower().Equals("querydiscreject"))
                                {
                                    htmlBody = htmlBody.Replace("{QDAccept_01}", WebCommon.getGlobalResourceValue("QDAccept_01").Replace("##dd/mm/yyyy##", queryDiscDate));
                                    htmlBody = htmlBody.Replace("{VendorMessage}", vendorComment);
                                    htmlBody = htmlBody.Replace("{QDAccept_02}", WebCommon.getGlobalResourceValue("QDAccept_02"));
                                    htmlBody = htmlBody.Replace("{QDReject_01}", WebCommon.getGlobalResourceValue("QDReject_01"));
                                    htmlBody = htmlBody.Replace("{ODMessage}", odComment);
                                    htmlBody = htmlBody.Replace("{PurchaseOrderNumber}", lstDisLog[0].PurchaseOrderNumber);
                                }


                                htmlBody = htmlBody.Replace("{POHash}", WebCommon.getGlobalResourceValue("POHash"));
                                htmlBody = htmlBody.Replace("{InvoiceNumHash}", WebCommon.getGlobalResourceValue("InvoiceNumHash"));
                                htmlBody = htmlBody.Replace("{DiscrepancyNo}", WebCommon.getGlobalResourceValue("DiscrepancyNo"));
                                htmlBody = htmlBody.Replace("{DearSirMadam}", WebCommon.getGlobalResourceValue("DearSirMadam"));
                                htmlBody = htmlBody.Replace("{InvoiceQueryComm1Line1}", WebCommon.getGlobalResourceValue("InvoiceQueryComm1Line1"));
                                htmlBody = htmlBody.Replace("{InvoiceQueryComm1Line2}", WebCommon.getGlobalResourceValue("InvoiceQueryComm1Line2"));
                                htmlBody = htmlBody.Replace("{Yoursfaithfully}", WebCommon.getGlobalResourceValue("Yoursfaithfully"));
                                htmlBody = htmlBody.Replace("{PortalLink}", "<a href=" + sPortalLink + ">" + sPortalLink + " </a>");

                                //internal comments...
                                htmlBody = htmlBody.Replace("{Comments}", Comments);

                                List<DiscrepancyBE> lstDisLogSiteVendor = null;
                                if (discrepancyType.ToLower() == "invoice query")
                                {
                                    lstDisLogSiteVendor = getDiscrepancyLogSiteVendorDetail(iDiscrepancyLogID, discrepancyType, out siteAddress, out accountPayable, "L");
                                }
                                if (discrepancyType.ToLower() == "invoice query")
                                {
                                    htmlBody = htmlBody.Replace("{InvoiceValue}", lstDisLog[0].DeliveryNoteNumber);
                                    htmlBody = htmlBody.Replace("{DiscrepancyValue}", lstDisLog[0].VDRNo);

                                    htmlBody = htmlBody.Replace("##SITENAME##", lstDisLog[0].Site.SiteName.Split('-').ElementAtOrDefault(1));

                                    htmlBody = htmlBody.Replace("{APClerkletter}", WebCommon.getGlobalResourceValue("APClerkletter"));

                                    htmlBody = htmlBody.Replace("{GoodsINTeam}", WebCommon.getGlobalResourceValue("GoodsINTeam"));
                                    htmlBody = htmlBody.Replace("{MediatorTeam}", WebCommon.getGlobalResourceValue("MediatorTeam"));

                                    htmlBody = htmlBody.Replace("{INVAPClerkContactNo}", dt.Rows[0][0].ToString());

                                    htmlBody = htmlBody.Replace("{APClerkName}", lstDisLogSiteVendor[0].StockPlannerName);
                                    htmlBody = !string.IsNullOrWhiteSpace(lstDisLogSiteVendor[0].StockPlannerContact)
                                        ? htmlBody.Replace("{APClerkContactNo}", lstDisLogSiteVendor[0].StockPlannerContact)
                                        : htmlBody.Replace("{APClerkContactNo}", string.Empty);

                                    htmlBody = htmlBody.Replace("{POValue}", lstDisLog[0].PurchaseOrderNumber);

                                    htmlBody = htmlBody.Replace("{GridValue}", sProductGrid);

                                }


                                #region Logic to set the Shuttle types ...
                                strShortage = string.Empty;
                                for (int intIndex = 0; intIndex < lstDetails.Count; intIndex++)
                                {
                                    if (!string.IsNullOrEmpty(strShortage) && !string.IsNullOrEmpty(lstDetails[intIndex].ShuttleType))
                                        strShortage = string.Format("{0}, {1}", strShortage, lstDetails[intIndex].ShuttleType);
                                    else
                                        strShortage = lstDetails[intIndex].ShuttleType;
                                }
                                #endregion
                                if (discrepancyType.ToLower() == "invoice" && sLetterType == "lettertype1")
                                    htmlBody = htmlBody.Replace("{APCommentsText}", lstWorkFlowActionDetails[1].Comment);
                                else if (discrepancyType.ToLower() == "invoice" && (sLetterType == "lettertype2" || sLetterType == "lettertype3"))
                                {
                                    htmlBody = htmlBody.Replace("{Comments} :", "");
                                    htmlBody = htmlBody.Replace("{APCommentsText}", "");
                                }

                                htmlBody = htmlBody.Replace("{Shortage}", strShortage);
                                htmlBody = htmlBody.Replace("{ManyThanksText}", strManyThanks);
                                htmlBody = htmlBody.Replace("{Comments}", Comments);

                                mailBodyToSaveInDB = htmlBody.Replace("{logoInnerPath}", oSendCommunicationCommon.getAbsolutePath());



                                retValue = oSendCommunicationCommon.saveMail(iDiscrepancyLogID, sMailAddress[i], mailBodyToSaveInDB, EmailSubjectText, objLanguage.LanguageID,
                                    "communication1", MailSentInLanguage, queryDiscID);
                                if (retValue == null)
                                    oSendCommunicationCommon.errorLog(iDiscrepancyLogID, EmailSubjectText, toAddress, htmlBody, "Error in save mail to database");



                                if (objLanguage.LanguageID == VendorLanguageId)
                                {
                                    clsEmail oclsEmail = new clsEmail();
                                    oclsEmail.sendMail(sMailAddress[i], htmlBody, EmailSubjectText, sFromAddress, true);

                                    sSendMailLink.Append("<a target='_blank' href='" + Common.EncryptQuery("DISLog_ReSendCommunication.aspx?communicationid=" + retValue + "&CommunicationLevel=Communication1") + "' onclick='window.open(this.href, this.target,\"scrollbars=1,left=100px,top=100px,width=1000px,height=800px\"); return false;'>" + sMailAddress[i] + "</a>" + System.Environment.NewLine);
                                }

                            }
                        }
                    }
                    //setting default Resourse file
                    Page.UICulture = Convert.ToString(Session["CultureInfo"]);
                    return retValue;
                }
                else
                {
                    oSendCommunicationCommon.errorLog(iDiscrepancyLogID, EmailSubjectText, toAddress, htmlBody, "No Data for Language");
                    return null;
                }
            }
            else
            {
                oSendCommunicationCommon.errorLog(iDiscrepancyLogID, EmailSubjectText, toAddress, htmlBody, "No Data in DiscrepancyLog table");
                return null;
            }


        }
        catch (Exception ex)
        {
            oSendCommunicationCommon.errorLog(iDiscrepancyLogID, EmailSubjectText, toAddress, htmlBody, ex.Message);
            return null;
        }
    }

    //Used in case of Letters
    public string sendAndSaveLetter(int iDiscrepancyLogID, string discrepancyType, int iVendorID)
    {
        List<DiscrepancyBE> lstDisLog = oSendCommunicationCommon.getDiscrepancyLogDetail("GetDiscrepancyLog", Convert.ToInt32(iDiscrepancyLogID), discrepancyType);
        List<DiscrepancyBE> lstDetails = oSendCommunicationCommon.getDiscrepancyItemDetail("GetDiscrepancyItem", Convert.ToInt32(iDiscrepancyLogID), discrepancyType);
        string htmlBody = string.Empty;
        string sDiscrepancyRelatedText, sLine1, sLine2 = string.Empty;

        if (lstDisLog != null && lstDisLog.Count > 0)
        {
            List<string> lstLanguage = oSendCommunicationCommon.getLanguageByVendorID(iVendorID);
            if (lstLanguage != null && lstLanguage.Count > 0)
            {
                for (int i = 0; i < lstLanguage.Count; i++)
                {
                    #region get file using languageid
                    //path = path.Replace(@"\bin\debug", "");                    

                    string templateFile = null;
                    templateFile = path + templatesPath + "/";

                    if (discrepancyType.ToLower() == "generic")
                        templateFile += "dis_initialcommunicationGeneric";
                    if (discrepancyType.ToLower() == "invoice query")
                        templateFile += "InvoiceQuery";
                    else
                        templateFile += "dis_initialcommunication";

                    ////int? languageID = (int?)(dsLanguage.Tables[0].Rows[i]["languageid"] == DBNull.Value ? 0 : dsLanguage.Tables[0].Rows[i]["languageid"]);
                    //templateFile += "." + (string.IsNullOrWhiteSpace(lstLanguage[i]) ? "english" : lstLanguage[i]) + ".htm";
                    templateFile += ".english.htm";

                    // Changing Resourse file according to language
                    if ((lstLanguage[i].ToLower() == "english"))
                    {
                        //Page.UICulture = "en-US";
                        Page.UICulture = clsConstants.EnglishISO;
                    }
                    else if (lstLanguage[i].ToLower() == "french")
                    {
                        //Page.UICulture = "fr";
                        Page.UICulture = clsConstants.FranceISO;
                    }
                    else if (lstLanguage[i].ToLower() == "german")
                    {
                        //Page.UICulture = "de"; // German
                        Page.UICulture = clsConstants.GermanyISO;
                    }
                    else if (lstLanguage[i].ToLower() == "dutch")
                    {
                        //Page.UICulture = "nl"; // Dutch
                        Page.UICulture = clsConstants.NederlandISO;
                    }
                    else if (lstLanguage[i].ToLower() == "spanish")
                    {
                        //Page.UICulture = "es"; // Spanish
                        Page.UICulture = clsConstants.SpainISO;
                    }
                    else if (lstLanguage[i].ToLower() == "italian")
                    {
                        //Page.UICulture = "it"; // Italy
                        Page.UICulture = clsConstants.ItalyISO;
                    }
                    else if (lstLanguage[i].ToLower() == "czech")
                    {
                        //Page.UICulture = "cz"; // Czech
                        Page.UICulture = clsConstants.CzechISO;
                    }
                    else
                    {
                        //Page.UICulture = "en-US";
                        Page.UICulture = clsConstants.EnglishISO;
                    }
                    #endregion                    

                    string sProductGrid = getDiscrepancyItemGrid(discrepancyType, lstDisLog, lstDetails, out sDiscrepancyRelatedText, out sLine1, out sLine2);
                    if (System.IO.File.Exists(templateFile.ToLower()))
                    {
                        using (StreamReader sReader = new StreamReader(templateFile.ToLower()))
                        {
                            #region mailBody
                            string siteAddress, accountPayable = string.Empty;
                            List<DiscrepancyBE> lstDisLogSiteVendor = getDiscrepancyLogSiteVendorDetail(iDiscrepancyLogID, discrepancyType, out siteAddress, out accountPayable, "L");

                            htmlBody = sReader.ReadToEnd();
                            htmlBody = htmlBody.Replace("{DearSir/Madam}", WebCommon.getGlobalResourceValue("DearSirMadam"));
                            htmlBody = htmlBody.Replace("{OurPuchaseOrderNumber}", WebCommon.getGlobalResourceValue("OurPuchaseOrderNumber"));
                            htmlBody = htmlBody.Replace("{Yoursfaithfully}", WebCommon.getGlobalResourceValue("Yoursfaithfully"));
                            htmlBody = htmlBody.Replace("{Inventory Manager}", WebCommon.getGlobalResourceValue("InventoryManager"));
                            htmlBody = htmlBody.Replace("{YourDeliveryNoteNumber}", WebCommon.getGlobalResourceValue("YourDeliveryNoteNumber"));
                            htmlBody = htmlBody.Replace("{ReferenceText}", WebCommon.getGlobalResourceValue("ReferenceText"));
                            htmlBody = htmlBody.Replace("{SUPPLIERDISCREPANCYREPORT}", WebCommon.getGlobalResourceValue("SUPPLIERDISCREPANCYREPORT"));
                            htmlBody = htmlBody.Replace("{VENDORDISCREPANCYREPORT}", WebCommon.getGlobalResourceValue("VENDORDISCREPANCYREPORT"));
                            htmlBody = htmlBody.Replace("{VDRNOText}", WebCommon.getGlobalResourceValue("VDRNOText"));
                            htmlBody = htmlBody.Replace("{DateofDelivery}", WebCommon.getGlobalResourceValue("DateofDelivery"));
                            htmlBody = htmlBody.Replace("{Date}", WebCommon.getGlobalResourceValue("Date"));

                            htmlBody = htmlBody.Replace("{POHash}", WebCommon.getGlobalResourceValue("POHash"));
                            htmlBody = htmlBody.Replace("{InvoiceNumHash}", WebCommon.getGlobalResourceValue("InvoiceNumHash"));
                            htmlBody = htmlBody.Replace("{DiscrepancyNo}", WebCommon.getGlobalResourceValue("DiscrepancyNo"));
                            htmlBody = htmlBody.Replace("{DearSirMadam}", WebCommon.getGlobalResourceValue("DearSirMadam"));
                            htmlBody = htmlBody.Replace("{InvoiceQueryComm1Line1}", WebCommon.getGlobalResourceValue("InvoiceQueryComm1Line1"));
                            htmlBody = htmlBody.Replace("{InvoiceQueryComm1Line2}", WebCommon.getGlobalResourceValue("InvoiceQueryComm1Line2"));
                            htmlBody = htmlBody.Replace("{Yoursfaithfully}", WebCommon.getGlobalResourceValue("Yoursfaithfully"));

                            htmlBody = htmlBody.Replace("{logoInnerPath}", oSendCommunicationCommon.getAbsolutePath());
                            htmlBody = htmlBody.Replace("{SiteAddress}", siteAddress);
                            htmlBody = htmlBody.Replace("{CommunicationDate}", System.DateTime.Now.ToString("dd/MM/yyyy"));
                            htmlBody = htmlBody.Replace("{VendorCode}", lstDisLogSiteVendor[0].Vendor.Vendor_No);
                            htmlBody = htmlBody.Replace("{VendorName}", lstDisLogSiteVendor[0].Vendor.VendorName);
                            htmlBody = htmlBody.Replace("{VendorAddress1}", lstDisLogSiteVendor[0].Vendor.address1);
                            htmlBody = htmlBody.Replace("{VDRNo}", lstDisLog[0].VDRNo);
                            htmlBody = htmlBody.Replace("{VendorAddress2}", lstDisLogSiteVendor[0].Vendor.address2);
                            htmlBody = htmlBody.Replace("{VMPPIN}", lstDisLogSiteVendor[0].Vendor.VMPPIN);
                            htmlBody = htmlBody.Replace("{VMPPOU}", lstDisLogSiteVendor[0].Vendor.VMPPOU);
                            htmlBody = htmlBody.Replace("{VendorCity}", lstDisLogSiteVendor[0].Vendor.city);
                            if (discrepancyType.ToLower() == "invoice query")
                            {
                                htmlBody = htmlBody.Replace("{InvoiceValue}", lstDisLog[0].DeliveryNoteNumber);
                            }
                            htmlBody = htmlBody.Replace("{DiscrepancyValue}", lstDisLog[0].VDRNo);
                            if (discrepancyType.ToLower() == "nopurchaseordernumber")
                                htmlBody = htmlBody.Replace("{PurchaseOrderNumber}", WebCommon.getGlobalResourceValue("NotQuotedOnPaperwork"));
                            else if (discrepancyType.ToLower() == "generic")
                            {
                                if (!string.IsNullOrEmpty(lstDisLog[0].PurchaseOrderNumber))
                                    htmlBody = htmlBody.Replace("{PurchaseOrderNumber}", lstDisLog[0].PurchaseOrderNumber);
                                else
                                {
                                    htmlBody = htmlBody.Replace("{PurchaseOrderNumber}", string.Empty);
                                }
                            }
                            else
                                htmlBody = htmlBody.Replace("{PurchaseOrderNumber}", lstDisLog[0].PurchaseOrderNumber);

                            if (discrepancyType.ToLower() == "presentationissue")
                                htmlBody = htmlBody.Replace("{CarrierRow}", "<tr><td style='font-family:Arial;font-size:12' colspan='2'><br /><font style='font-weight:bold'>Carrier Name:</font>&nbsp;" + lstDisLog[0].CarrierName + "</td></tr>");
                            else
                                htmlBody = htmlBody.Replace("{CarrierRow}", "");

                            htmlBody = htmlBody.Replace("{DeliveryNoteNumber}", lstDisLog[0].DeliveryNoteNumber);

                            if (discrepancyType.ToLower() == "prematureinvoicereceipt" || discrepancyType.ToLower() == "prematureinvoice")
                                htmlBody = htmlBody.Replace("{DeliveryNoteNumberText}", "Invoice Number");
                            else
                                htmlBody = htmlBody.Replace("{DeliveryNoteNumberText}", "Delivery Note Number");

                            htmlBody = htmlBody.Replace("{DeliveryDate}", lstDisLog[0].DeliveryArrivedDate == null ? "" : oSendCommunicationCommon.convertDateTime(lstDisLog[0].DeliveryArrivedDate.Value));

                            htmlBody = htmlBody.Replace("{DiscrepancyRelatedText}", sDiscrepancyRelatedText);
                            htmlBody = !string.IsNullOrEmpty(sProductGrid)
                                ? htmlBody.Replace("{ProductGrid}", "<tr><td style='font-family:Arial;font-size:12' colspan='2'><br />" + sProductGrid + "</td></tr>")
                                : htmlBody.Replace("{ProductGrid}", "");
                            htmlBody = htmlBody.Replace("{Line1}", sLine1);

                            if (discrepancyType.ToLower() == "generic")
                            {
                                htmlBody = htmlBody.Replace("{InventoryManagerName}", lstDisLog[0].OfficeDepotContactName);
                                htmlBody = htmlBody.Replace("<tr><td style=\"font-weight:bold;font-family:Arial;font-size:12\" colspan=\"2\">Inventory Manager</td></tr>", string.Empty);
                                htmlBody = htmlBody.Replace("{InventoryManagerNumber}", lstDisLog[0].OfficeDepotContactPhone);
                            }
                            else if (discrepancyType.ToLower() == "invoice query")
                            {
                                htmlBody = htmlBody.Replace("{APClerkName}", lstDisLogSiteVendor[0].StockPlannerName);
                                if (!string.IsNullOrWhiteSpace(lstDisLogSiteVendor[0].StockPlannerContact))
                                    htmlBody = htmlBody.Replace("{APClerkContactNo}", lstDisLogSiteVendor[0].StockPlannerContact);
                                else
                                    htmlBody = htmlBody.Replace("({APClerkContactNo})", string.Empty);

                                htmlBody = htmlBody.Replace("{POValue}", lstDisLog[0].PurchaseOrderNumber);

                            }
                            else if (discrepancyType.ToLower() == "invoice")
                            {
                                if (lstDisLogSiteVendor[0].VDRNo.ToCharArray()[0].ToString() == "U")
                                {
                                    htmlBody = htmlBody.Replace("{AccountsPayableName}", lstDisLogSiteVendor[0].StockPlannerName);
                                    if (!string.IsNullOrWhiteSpace(lstDisLogSiteVendor[0].StockPlannerContact))
                                        htmlBody = htmlBody.Replace("{AccountsPayableNumber}", lstDisLogSiteVendor[0].StockPlannerContact);
                                    else
                                        htmlBody = htmlBody.Replace("({AccountsPayableNumber})", string.Empty);

                                }
                                else
                                {
                                    htmlBody = htmlBody.Replace("{AccountsPayableName}", lstDisLogSiteVendor[0].AccountPaybleName);
                                    if (!string.IsNullOrWhiteSpace(lstDisLogSiteVendor[0].AccountPayblePhoneNo))
                                        htmlBody = htmlBody.Replace("{AccountsPayableNumber}", lstDisLogSiteVendor[0].AccountPayblePhoneNo);
                                    else
                                        htmlBody = htmlBody.Replace("({AccountsPayableNumber})", string.Empty);

                                }
                            }
                            else
                            {
                                htmlBody = htmlBody.Replace("{InventoryManagerName}", lstDisLog[0].StockPlannerName);
                                htmlBody = htmlBody.Replace("{InventoryManagerNumber}", lstDisLog[0].StockPlannerContact);
                            }
                            htmlBody = lstDisLogSiteVendor[0].VDRNo.ToCharArray()[0].ToString() == "U"
                                ? htmlBody.Replace("{AccountPayable}", WebCommon.getGlobalResourceValue("StockPlanner"))
                                : htmlBody.Replace("{AccountPayable}", accountPayable);
                            htmlBody = htmlBody.Replace("<table align=\"center\" width=\"1200px\">", "<table align='center' width='800px'>");
                            #endregion
                            saveLetterToShowInWorkFlow(iDiscrepancyLogID, lstDisLog, lstDetails, lstDisLogSiteVendor, siteAddress, accountPayable, discrepancyType, iVendorID);
                        }
                    }
                }
                Page.UICulture = Convert.ToString(Session["CultureInfo"]);
                return htmlBody;
            }
            return "";
        }

        return "";
    }
    public void saveLetterToShowInWorkFlow(int iDiscrepancyLogID, List<DiscrepancyBE> lstDisLog, List<DiscrepancyBE> lstDetails, List<DiscrepancyBE> lstDisLogSiteVendor, string siteAddress, string accountPayable, string discrepancyType, int iVendorID)
    {

        string letterBody = string.Empty;
        string sDiscrepancyRelatedText, sLine1, sLine2 = string.Empty;

        if (lstDisLog != null && lstDisLog.Count > 0)
        {

            #region get file for english to show letter in view communication link in workflow
            //path = path.Replace(@"\bin\debug", "");

            string templateFile = null;
            templateFile = path + templatesPath + "/";

            if (discrepancyType.ToLower() == "generic")
                templateFile += "dis_initialcommunicationGeneric";
            else
                templateFile += "dis_initialcommunication";

            templateFile += ".english.htm";

            #endregion

            string sProductGrid = getDiscrepancyItemGrid(discrepancyType, lstDisLog, lstDetails, out sDiscrepancyRelatedText, out sLine1, out sLine2);
            if (System.IO.File.Exists(templateFile.ToLower()))
            {
                using (StreamReader sReader = new StreamReader(templateFile.ToLower()))
                {
                    #region mailBody

                    letterBody = sReader.ReadToEnd();
                    getLetterBody(ref letterBody, lstDisLog, lstDetails, lstDisLogSiteVendor, siteAddress, accountPayable, discrepancyType, sDiscrepancyRelatedText, sProductGrid, sLine1, "{logoInnerPath}");

                    #endregion
                    string DiscrepancyLetterID = Convert.ToString(System.Guid.NewGuid());
                    oSendCommunicationCommon.saveLetter(iDiscrepancyLogID, letterBody, DiscrepancyLetterID);
                    sLetterLink.Clear();
                    sLetterLink.Append("<a target='_blank' href='" + Common.EncryptQuery("DISLog_Letter.aspx?DiscrepancyLetterID=" + DiscrepancyLetterID) + "' onclick='window.open(this.href, this.target,\"scrollbars=1,toolbar=1,left=100px,top=50px,resizable=1,width=1100px,height=850px\"); return false;'>View Communication</a>" + System.Environment.NewLine);
                }
            }

        }
    }
    private void getLetterBody(ref string letterBody, List<DiscrepancyBE> lstDisLog, List<DiscrepancyBE> lstDetails, List<DiscrepancyBE> lstDisLogSiteVendor, string siteAddress, string accountPayable, string discrepancyType, string sDiscrepancyRelatedText, string sProductGrid, string sLine1, string logoInnerPath)
    {


        letterBody = letterBody.Replace("{DearSir/Madam}", WebCommon.getGlobalResourceValue("DearSirMadam"));
        letterBody = letterBody.Replace("{OurPuchaseOrderNumber}", WebCommon.getGlobalResourceValue("OurPuchaseOrderNumber"));
        letterBody = letterBody.Replace("{Yoursfaithfully}", WebCommon.getGlobalResourceValue("Yoursfaithfully"));
        letterBody = letterBody.Replace("{InventoryManager}", WebCommon.getGlobalResourceValue("InventoryManager"));
        letterBody = letterBody.Replace("{AccountsPayable}", WebCommon.getGlobalResourceValue("AccountsPayable"));
        letterBody = letterBody.Replace("{YourDeliveryNoteNumber}", WebCommon.getGlobalResourceValue("YourDeliveryNoteNumber"));
        letterBody = letterBody.Replace("{ReferenceText}", WebCommon.getGlobalResourceValue("ReferenceText"));
        letterBody = letterBody.Replace("{SUPPLIERDISCREPANCYREPORT}", WebCommon.getGlobalResourceValue("SUPPLIERDISCREPANCYREPORT"));
        letterBody = letterBody.Replace("{VENDORDISCREPANCYREPORT}", WebCommon.getGlobalResourceValue("VENDORDISCREPANCYREPORT"));
        letterBody = letterBody.Replace("{VDRNOText}", WebCommon.getGlobalResourceValue("VDRNOText"));
        letterBody = letterBody.Replace("{DateofDelivery}", WebCommon.getGlobalResourceValue("DateofDelivery"));
        letterBody = letterBody.Replace("{Date}", WebCommon.getGlobalResourceValue("Date"));


        letterBody = letterBody.Replace("{logoInnerPath}", logoInnerPath);
        letterBody = letterBody.Replace("{SiteAddress}", siteAddress);
        letterBody = letterBody.Replace("{CommunicationDate}", System.DateTime.Now.ToString("dd/MM/yyyy"));
        letterBody = letterBody.Replace("{VendorCode}", lstDisLogSiteVendor[0].Vendor.Vendor_No);
        letterBody = letterBody.Replace("{VendorName}", lstDisLogSiteVendor[0].Vendor.VendorName);
        letterBody = letterBody.Replace("{VendorAddress1}", lstDisLogSiteVendor[0].Vendor.address1);
        letterBody = letterBody.Replace("{VDRNo}", lstDisLog[0].VDRNo);
        letterBody = letterBody.Replace("{VendorAddress2}", lstDisLogSiteVendor[0].Vendor.address2);
        letterBody = letterBody.Replace("{VMPPIN}", lstDisLogSiteVendor[0].Vendor.VMPPIN);
        letterBody = letterBody.Replace("{VMPPOU}", lstDisLogSiteVendor[0].Vendor.VMPPOU);
        letterBody = letterBody.Replace("{VendorCity}", lstDisLogSiteVendor[0].Vendor.city);
        if (discrepancyType.ToLower() == "nopurchaseordernumber")
            letterBody = letterBody.Replace("{PurchaseOrderNumber}", WebCommon.getGlobalResourceValue("NotQuotedOnPaperwork"));
        else if (discrepancyType.ToLower() == "generic")
        {
            if (!string.IsNullOrEmpty(lstDisLogSiteVendor[0].PurchaseOrderNumber))
                letterBody = letterBody.Replace("{PurchaseOrderNumber}", lstDisLog[0].PurchaseOrderNumber);
            else

                letterBody = letterBody.Replace("{PurchaseOrderNumber}", string.Empty);

        }
        else
            letterBody = letterBody.Replace("{PurchaseOrderNumber}", lstDisLog[0].PurchaseOrderNumber);

        if (discrepancyType.ToLower() == "presentationissue")
            letterBody = letterBody.Replace("{CarrierRow}", "<tr><td style='font-family:Arial;font-size:12' colspan='2'><br /><font style='font-weight:bold'>Carrier Name:</font>&nbsp;" + lstDisLog[0].CarrierName + "</td></tr>");
        else
            letterBody = letterBody.Replace("{CarrierRow}", "");

        letterBody = letterBody.Replace("{DeliveryNoteNumber}", lstDisLog[0].DeliveryNoteNumber);
        letterBody = letterBody.Replace("{DeliveryDate}", lstDisLog[0].DeliveryArrivedDate == null ? "" : oSendCommunicationCommon.convertDateTime(lstDisLog[0].DeliveryArrivedDate.Value));
        // letterBody = letterBody.Replace("{Reference}", lstDisLog[0].PaperworkReference);

        letterBody = letterBody.Replace("{DiscrepancyRelatedText}", sDiscrepancyRelatedText);
        if (!string.IsNullOrEmpty(sProductGrid))
            letterBody = letterBody.Replace("{ProductGrid}", "<tr><td style='font-family:Arial;font-size:12' colspan='2'><br />" + sProductGrid + "</td></tr>");
        else
            letterBody = letterBody.Replace("{ProductGrid}", "");
        letterBody = letterBody.Replace("{Line1}", sLine1);
        if (discrepancyType.ToLower() == "generic")
        {
            letterBody = letterBody.Replace("{InventoryManagerName}", lstDisLog[0].OfficeDepotContactName);
            letterBody = letterBody.Replace("<tr><td style=\"font-weight:bold;font-family:Arial;font-size:12\" colspan=\"2\">Inventory Manager</td></tr>", string.Empty);
            letterBody = letterBody.Replace("{InventoryManagerNumber}", lstDisLog[0].OfficeDepotContactPhone);


        }
        else if (discrepancyType.ToLower() == "invoice")
        {
            if (lstDisLogSiteVendor[0].VDRNo.ToCharArray()[0].ToString() == "U")
            {
                letterBody = letterBody.Replace("{AccountsPayableName}", lstDisLogSiteVendor[0].StockPlannerName);
                if (!string.IsNullOrWhiteSpace(lstDisLogSiteVendor[0].StockPlannerContact))
                    letterBody = letterBody.Replace("{AccountsPayableNumber}", lstDisLogSiteVendor[0].StockPlannerContact);
                else
                    letterBody = letterBody.Replace("({AccountsPayableNumber})", string.Empty);

            }
            else
            {
                letterBody = letterBody.Replace("{AccountsPayableName}", lstDisLogSiteVendor[0].AccountPaybleName);

                if (!string.IsNullOrWhiteSpace(lstDisLogSiteVendor[0].AccountPayblePhoneNo))
                    letterBody = letterBody.Replace("{AccountsPayableNumber}", lstDisLogSiteVendor[0].AccountPayblePhoneNo);
                else
                    letterBody = letterBody.Replace("({AccountsPayableNumber})", string.Empty);
            }
        }
        else
        {
            letterBody = letterBody.Replace("{InventoryManagerName}", lstDisLog[0].StockPlannerName);
            letterBody = letterBody.Replace("{InventoryManagerNumber}", lstDisLog[0].StockPlannerContact);
        }
        if (lstDisLogSiteVendor[0].VDRNo.ToCharArray()[0].ToString() == "U")
        {
            letterBody = letterBody.Replace("{AccountPayable}", WebCommon.getGlobalResourceValue("StockPlanner"));
        }
        else
        {
            letterBody = letterBody.Replace("{AccountPayable}", accountPayable);
        }
        letterBody = letterBody.Replace("<table align=\"center\" width=\"1200px\">", "<table align='center' width='800px'>");

    }
    public List<DiscrepancyBE> getDiscrepancyLogSiteVendorDetail(int iDiscrepancyLogID, string discrepancyType, out string siteAddress, out string accountPayable, string sMailOrLetter)
    {
        DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
        DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();
        oDiscrepancyBE.Action = "GetDiscrepancySiteVendorDetail";
        oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(iDiscrepancyLogID);
        oDiscrepancyBE.DiscrepancyType = discrepancyType;
        List<DiscrepancyBE> lstDisLogSiteVendor = oDiscrepancyBAL.GetDiscrepancyLogSiteVendorDetailsBAL(oDiscrepancyBE);

        siteAddress = accountPayable = string.Empty;
        //  if (!string.IsNullOrEmpty(lstDisLogSiteVendor[0].Site.SiteName) && sMailOrLetter.ToLower() != "l") { siteAddress = Convert.ToString(lstDisLogSiteVendor[0].Site.SiteName); }
        if (!string.IsNullOrEmpty(lstDisLogSiteVendor[0].Site.SiteAddressLine1)) { siteAddress = Convert.ToString(lstDisLogSiteVendor[0].Site.SiteAddressLine1); }
        if (!string.IsNullOrEmpty(lstDisLogSiteVendor[0].Site.SiteAddressLine2)) { siteAddress = siteAddress + ", " + Convert.ToString(lstDisLogSiteVendor[0].Site.SiteAddressLine2); }
        if (!string.IsNullOrEmpty(lstDisLogSiteVendor[0].Site.SiteAddressLine3)) { siteAddress = siteAddress + ", " + Convert.ToString(lstDisLogSiteVendor[0].Site.SiteAddressLine3); }
        if (!string.IsNullOrEmpty(lstDisLogSiteVendor[0].Site.SiteAddressLine4)) { siteAddress = siteAddress + ", " + Convert.ToString(lstDisLogSiteVendor[0].Site.SiteAddressLine4); }
        if (!string.IsNullOrEmpty(lstDisLogSiteVendor[0].Site.SiteAddressLine5)) { siteAddress = siteAddress + ", " + Convert.ToString(lstDisLogSiteVendor[0].Site.SiteAddressLine5); }
        if (!string.IsNullOrEmpty(lstDisLogSiteVendor[0].Site.SiteAddressLine6)) { siteAddress = siteAddress + ", " + Convert.ToString(lstDisLogSiteVendor[0].Site.SiteAddressLine6); }
        if (!string.IsNullOrEmpty(lstDisLogSiteVendor[0].Site.SitePincode)) { siteAddress = siteAddress + ", " + Convert.ToString(lstDisLogSiteVendor[0].Site.SitePincode); }

        if (!string.IsNullOrEmpty(lstDisLogSiteVendor[0].Site.APAddressLine1)) { accountPayable = Convert.ToString(lstDisLogSiteVendor[0].Site.APAddressLine1); }
        if (!string.IsNullOrEmpty(lstDisLogSiteVendor[0].Site.APAddressLine2)) { accountPayable = accountPayable + ", " + Convert.ToString(lstDisLogSiteVendor[0].Site.APAddressLine2); }
        if (!string.IsNullOrEmpty(lstDisLogSiteVendor[0].Site.APAddressLine3)) { accountPayable = accountPayable + ", " + Convert.ToString(lstDisLogSiteVendor[0].Site.APAddressLine3); }
        if (!string.IsNullOrEmpty(lstDisLogSiteVendor[0].Site.APAddressLine4)) { accountPayable = accountPayable + ", " + Convert.ToString(lstDisLogSiteVendor[0].Site.APAddressLine4); }
        if (!string.IsNullOrEmpty(lstDisLogSiteVendor[0].Site.APAddressLine5)) { accountPayable = accountPayable + ", " + Convert.ToString(lstDisLogSiteVendor[0].Site.APAddressLine5); }
        if (!string.IsNullOrEmpty(lstDisLogSiteVendor[0].Site.APAddressLine6)) { accountPayable = accountPayable + ", " + Convert.ToString(lstDisLogSiteVendor[0].Site.APAddressLine6); }
        if (!string.IsNullOrEmpty(lstDisLogSiteVendor[0].Site.APPincode)) { accountPayable = accountPayable + ", " + Convert.ToString(lstDisLogSiteVendor[0].Site.APPincode); }

        return lstDisLogSiteVendor;
    }
    public string getDiscrepancyItemGrid(string discrepancyType, List<DiscrepancyBE> lstDisLog, List<DiscrepancyBE> lstDetails, out string sDiscrepancyRelatedtext, out string sLine1, out string sLine2)
    {
        string Suppliercode = WebCommon.getGlobalResourceValue("Suppliercode");
        sDiscrepancyRelatedtext = sLine1 = sLine2 = string.Empty;
        StringBuilder sProductDetail = new StringBuilder();
        if (lstDisLog != null && lstDisLog.Count > 0 && lstDetails != null && lstDetails.Count > 0)
        {
            sProductDetail.Append("<table style='background: none repeat scroll 0 0 #FFFFFF;border: 1px solid #DADADA;cellspacing=0 cellpadding=0 border=0 border-collapse:collapse;width=100%'>");
            #region Item Grid

            switch (discrepancyType.ToLower())
            {

                case "over":
                    sProductDetail.Append("<tr style='color:#934500;border-bottom: 1px solid #E8E8E8;font-weight: bold;font-family:Arial ;font-size:12;vertical-align: top;'><td width='15%'>" + OurCode + "</cc1:ucLabel></td><td width='35%'>" + Description + "</td><td width='10%'>" + VendorItemCode + "</td><td  width='10%'>" + OutstandingPOQty + "</td><td  width='10%'>" + DeliveryNoteQty + "</td><td  width='10%'>" + DeliveredQty + "</td><td  width='10%'>" + QtyOverDelivered + "</td></tr>");
                    for (int i = 0; i < lstDetails.Count; i++)
                    {
                        sProductDetail.Append("<tr style='background-color: #F7F6F3;font-size:12;font-family:Arial ;'><td>" + lstDetails[i].ODSKUCode + "</td><td>" + lstDetails[i].ProductDescription + "</td><td>" + lstDetails[i].VendorCode);
                        sProductDetail.Append("</td><td>" + lstDetails[i].OutstandingQuantity + "</td><td>" + lstDetails[i].DeliveredNoteQuantity + "</td><td>" + lstDetails[i].DeliveredQuantity + "</td>");
                        sProductDetail.Append("<td>" + lstDetails[i].OversQuantity + "</td></tr>");
                    }
                    //sDiscrepancyRelatedtext = "Product received against the above delivery were over delivered against our Purchase Order as detailed below";
                    //sLine1 = "Please contact the undersigned urgently so that resolution on this issue can be reached.<br /><br />In all communication please ensure that you quote the Discrepancy Report Number.";                    
                    break;
                case "shortage":
                    sProductDetail.Append("<tr style='color:#934500;border-bottom: 1px solid #E8E8E8;font-weight: bold;font-family:Arial ;font-size:12;vertical-align: top;'><td width='15%'>" + OurCode + "</td><td width='35%'> " + Description + "</td><td width='10%'>" + VendorItemCode + "</td><td  width='10%'>" + OutstandingPOQty + "</td><td  width='10%'>" + DeliveryNoteQty + "</td><td  width='10%'>" + ReceivedQuantity + "</td><td  width='10%'>" + ShortageQuantity + "</td></tr>");
                    for (int i = 0; i < lstDetails.Count; i++)
                    {
                        decimal dReceivedQuantity = (lstDetails[i].DeliveredNoteQuantity ?? 0) - (lstDetails[i].ShortageQuantity ?? 0);
                        sProductDetail.Append("<tr style='background-color: #F7F6F3;font-size:12;font-family:Arial ;'><td>" + lstDetails[i].ODSKUCode + "</td><td>" + lstDetails[i].ProductDescription + "</td><td>" + lstDetails[i].VendorCode);
                        sProductDetail.Append("</td><td>" + lstDetails[i].OutstandingQuantity + "</td><td>" + lstDetails[i].DeliveredNoteQuantity + "</td><td>" + dReceivedQuantity + "</td><td>" + lstDetails[i].ShortageQuantity + "</td></tr>");
                    }
                    //sDiscrepancyRelatedtext = "Product received against the above delivery note were short delivered as per the following details.";
                    //sLine1 = "In this first instance please contact the undersigned urgently so that they can advise as to whether the short delivered quantities are still required.<br /><br />Please ensure that you quote the Discrepancy Report Number in all communication relating to this issue.";
                    break;
                case "goodsreceiveddamaged":
                    sProductDetail.Append("<tr style='color:#934500;border-bottom: 1px solid #E8E8E8;font-weight: bold;font-family:Arial ;font-size:12;vertical-align: top;'><td width='15%'>" + OurCode + "</td><td width='35%'>" + Description + "</td><td width='10%'>" + VendorItemCode + "</td><td  width='10%'>" + OutstandingPOQty + "</td><td  width='10%'>" + DamagedQuantity + "</td><td  width='10%'>" + DamagedDescription + "</td></tr>");
                    for (int i = 0; i < lstDetails.Count; i++)
                    {
                        sProductDetail.Append("<tr style='background-color: #F7F6F3;font-size:12;font-family:Arial ;'><td>" + lstDetails[i].ODSKUCode + "</td><td>" + lstDetails[i].ProductDescription + "</td><td>" + lstDetails[i].VendorCode);
                        sProductDetail.Append("</td><td>" + lstDetails[i].OutstandingQuantity + "</td><td>" + lstDetails[i].DamageQuantity + "</td><td>" + lstDetails[i].DamageDescription + "</td></tr>");
                    }
                    //sDiscrepancyRelatedtext = "Product received against the above delivery note were damaged as detailed below.";
                    //sLine1 = "In this first instance please contact the undersigned urgently so that they can advise as to whether the damaged quantities are still required.<br /><br />Please ensure that you quote the Discrepancy Report Number in all communication relating to this issue.";                    
                    break;
                case "nopurchaseordernumber":
                    sProductDetail.Append("<tr style='color:#934500;border-bottom: 1px solid #E8E8E8;font-weight: bold;font-family:Arial ;font-size:12;vertical-align: top;'><td width='15%'>" + OurCode + "</td><td width='35%'>" + Description + "</td><td width='10%'>" + VendorItemCode + "</td><td  width='10%'>" + ReceivedQuantity + "</td></tr>");
                    for (int i = 0; i < lstDetails.Count; i++)
                    {
                        sProductDetail.Append("<tr style='background-color: #F7F6F3;font-size:12;font-family:Arial ;'><td>" + lstDetails[i].ODSKUCode + "</td><td>" + lstDetails[i].ProductDescription + "</td><td>" + lstDetails[i].VendorCode);
                        sProductDetail.Append("</td><td>" + lstDetails[i].DeliveredQuantity + "</td></tr>");
                    }
                    //sDiscrepancyRelatedtext = "The above delivery did not quote the Office Depot Purchase Order on the documentation." + "<br /><br /> We are unable to receipt stock without a valid Purchase Order Number. Please contact the undersigned <u><b>urgently</b></u> so that we can confirm the correct Purchase Order Number.<br /><br />Please ensure that the invoice relating to this delivery quotes our Purchase Order Number and that when referencing this discrepancy please ensure that the Discrepancy number is quoted" + "<br /><br />Please find detail of what was received catalogued below<br /><br />";
                    break;
                case "incorrectproduct":
                    sProductDetail.Append("<tr style='color:#934500;border-bottom: 1px solid #E8E8E8;font-weight: bold;font-family:Arial ;font-size:12;vertical-align: top;'><td width='10%'>" + RequiredCode + "</td><td width='20%'>" + Description + "</td><td width='10%'>" + VendorItemCode + "</td><td  width='10%'>" + ReceivedQuantity + "</td><td  width='10%'>" + UOM + "</td><td  width='10%'>" + CodeReceived + "</td><td  width='10%'>" + Description + "</td><td width='10%'>" + VendorItemCode + "</td><td width='10%'>" + UOM + "</td></tr>");
                    for (int i = 0; i < lstDetails.Count; i++)
                    {
                        sProductDetail.Append("<tr style='background-color: #F7F6F3;font-size:12;font-family:Arial ;'><td>" + lstDetails[i].ODSKUCode + "</td><td>" + lstDetails[i].ProductDescription + "</td><td>" + lstDetails[i].VendorCode + "</td><td>" + lstDetails[i].DeliveredQuantity + "</td><td>" + lstDetails[i].UOM + "</td><td>" + lstDetails[i].CorrectODSKUCode + "</td><td>" + lstDetails[i].CorrectProductDescription + "</td><td>" + lstDetails[i].CorrectVendorCode + "</td><td>" + lstDetails[i].CorrectUOM + "</td></tr>");
                    }
                    // sDiscrepancyRelatedtext = "The above delivery contained incorrect products as per our Purchase Order Number, details as follows:";
                    //sLine1 = "Please contact the undersigned urgently so that resolution on this issue can be reached.<br /><br />In all communication please ensure that you quote the Discrepancy Report Number.";                    
                    break;
                case "paperworkamended":
                    if (getCommunicationWithEscalation(lstDisLog[0].SiteID))
                    {
                        // This is for escalation    
                        sProductDetail.Append("<tr style='color:#934500;border-bottom: 1px solid #E8E8E8;font-weight: bold;font-family:Arial ;font-size:12;vertical-align: top;'><td width='10%'>" + OurCode + "</td><td width='35%'>" + Description + "</td><td width='10%'>" + VendorItemCode + "</td><td  width='10%'>" + QuantityAdvisedOnDNote + "</td><td  width='10%'>" + QuantityPaperworkAmendedto + "</td></tr>");
                        for (int i = 0; i < lstDetails.Count; i++)
                        {
                            sProductDetail.Append("<tr style='background-color: #F7F6F3;font-size:12;font-family:Arial ;'><td>" + lstDetails[i].ODSKUCode + "</td><td>" + lstDetails[i].ProductDescription + "</td><td>" + lstDetails[i].VendorCode + "</td><td>" + lstDetails[i].DeliveredNoteQuantity + "</td><td>" + lstDetails[i].AmendedQuantity + "</td></tr>");
                        }
                    }
                    else
                    {
                        // This is no escalation
                        sProductDetail.Append("<tr style='color:#934500;border-bottom: 1px solid #E8E8E8;font-weight: bold;font-family:Arial ;font-size:12;vertical-align: top;'><td width='10%'>" + OurCode + "</td><td width='35%'>" + Description + "</td><td width='10%'>" + VendorItemCode + "</td><td  width='10%'>" + QuantityAdvisedOnDNote + "</td><td  width='10%'>" + QuantityPaperworkAmendedto + "</td></tr>");
                        for (int i = 0; i < lstDetails.Count; i++)
                        {
                            sProductDetail.Append("<tr style='background-color: #F7F6F3;font-size:12;font-family:Arial ;'><td>" + lstDetails[i].ODSKUCode + "</td><td>" + lstDetails[i].ProductDescription + "</td><td>" + lstDetails[i].VendorCode + "</td><td>" + lstDetails[i].DeliveredNoteQuantity + "</td><td>" + lstDetails[i].AmendedQuantity + "</td></tr>");
                        }
                        //sDiscrepancyRelatedtext = "The above delivery paperwork was amended. Please find details catalogued below.";
                        //sLine1 = "Please ensure that the associated invoice is for the amended quantity, as this is what was physically received and receipted.";
                    }
                    break;
                case "wrongpacksize":
                    sProductDetail.Append("<tr style='color:#934500;border-bottom: 1px solid #E8E8E8;font-weight: bold;font-family:Arial ;font-size:12;vertical-align: top;'><td width='15%'>" + OurCode + "</td><td width='40%'>" + Description + "</td><td width='15%'>" + VendorItemCode + "</td><td  width='15%'>" + POPackSize + "</td><td  width='15%'>" + WebCommon.getGlobalResourceValue("PackSizeofgooddeliveredNew") + "</td><td  width='15%'>" + NoofPacksReceived + "</td><td  width='15%' class='style10'>" + WebCommon.getGlobalResourceValue("#Units") + "</td></tr>");
                    for (int i = 0; i < lstDetails.Count; i++)
                    {
                        sProductDetail.Append("<tr style='background-color: #F7F6F3;font-size:12;font-family:Arial ;'><td>" + lstDetails[i].ODSKUCode + "</td><td>" + lstDetails[i].ProductDescription + "</td><td>" + lstDetails[i].VendorCode + "</td><td>" + lstDetails[i].PackSizeOrdered + "</td><td>" + lstDetails[i].PackSizeReceived + "</td><td>" + lstDetails[i].WrongPackReceived + "</td><td>" + Convert.ToInt32(lstDetails[i].PackSizeReceived) * lstDetails[i].WrongPackReceived + "</td></tr>");
                    }
                    //sDiscrepancyRelatedtext = "The above delivery contained different pack sizes from those shown on our purchase order as detailed below.";
                    //sLine1 = "Please contact the undersigned to discuss these differences, in order that we may amend our records if necessary.";
                    break;
                case "nopaperwork":
                    sProductDetail.Append("<tr style='color:#934500;border-bottom: 1px solid #E8E8E8;font-weight: bold;font-family:Arial ;font-size:12;vertical-align: top;'><td width='15%'>" + OurCode + "</td><td width='40%'>" + Description + "</td><td width='15%'>" + VendorItemCode + "</td><td  width='15%'>" + ReceivedQuantity + "</td></tr>");
                    for (int i = 0; i < lstDetails.Count; i++)
                    {
                        sProductDetail.Append("<tr style='background-color: #F7F6F3;font-size:12;font-family:Arial ;'><td>" + lstDetails[i].ODSKUCode + "</td><td>" + lstDetails[i].ProductDescription + "</td><td>" + lstDetails[i].VendorCode + "</td><td>" + lstDetails[i].DeliveredQuantity + "</td></tr>");
                    }
                    break;
                case "invoice":
                    sProductDetail.Append("<tr style='color:#934500;border-bottom: 1px solid #E8E8E8;font-weight: bold;font-family:Arial ;font-size:12;vertical-align: top;'><td width='15%'>" + OurCode + "</td><td width='40%'>" + Description + "</td><td width='15%'>" + VendorItemCode + "</td><td  width='15%'>" + ChaseQuantity + "</td></tr>");
                    for (int i = 0; i < lstDetails.Count; i++)
                    {
                        sProductDetail.Append("<tr style='background-color: #F7F6F3;font-size:12;font-family:Arial ;'><td>" + lstDetails[i].ODSKUCode + "</td><td>" + lstDetails[i].ProductDescription + "</td><td>" + lstDetails[i].VendorCode + "</td><td>" + lstDetails[i].ChaseQuantity + "</td></tr>");
                    }
                    break;

                case "qualityissue":
                    sProductDetail.Append("<tr style='color:#934500;border-bottom: 1px solid #E8E8E8;font-weight: bold;font-family:Arial ;font-size:12;vertical-align: top;'><td width='5%'>"
                       + strLine + "</td><td width='10%'>" + strODCode
                       + "</td><td width='10%'>" + strVikingCode + "</td><td width='15%'>" + VendorItemCode + "</td><td width='30%'>" 
                       + Description + "</td><td  width='10%'>" + UOM + "</td><td width='10%'>"
                       + strOriginalPOQty + "</td><td  width = '10%' > " + OutstandingPOQty + " </td><td  width = '10%' > " 
                       + DeliveredQty + "</td></tr>");
                    for (int i = 0; i < lstDetails.Count; i++)
                    {
                        sProductDetail.Append("<tr style='background-color: #F7F6F3;font-size:12;font-family:Arial ;'><td>"
                            + lstDetails[i].Line_no + "</td><td>" + lstDetails[i].ODSKUCode + "</td><td>" + lstDetails[i].DirectCode + "</td><td>" + lstDetails[i].VendorCode);
                        sProductDetail.Append("</td><td>" + lstDetails[i].ProductDescription + "</td><td>" + lstDetails[i].UOM + "</td><td>" + lstDetails[i].OriginalQuantity + "</td><td>"
                            + lstDetails[i].OutstandingQuantity
                            + "</td><td>" + lstDetails[i].DeliveredQuantity + "</td>");
                        sProductDetail.Append("</tr>");
                    }
                    break;
                case "invoice query":
                    sProductDetail.Append("<tr style='color:#934500;border-bottom: 1px solid #E8E8E8;font-weight: bold;font-family:Arial ;font-size:12;vertical-align: top;'><td width='5%'>"
                       + strLine + "</td><td width='10%'>" + strODCode
                       + "</td><td width='10%'>" + strVikingCode + "</td><td width='15%'>" + VendorItemCode + "</td><td width='30%'>" + Description + "</td><td  width='10%'>" + UOM + "</td><td width='10%'>"
                       + strOriginalPOQty + "</td><td  width='15%'>" + WebCommon.getGlobalResourceValue("ReceiptedQty") + "</td><td  width='10%'>" + WebCommon.getGlobalResourceValue("InvoiceQTY") + "</td></tr>");
                    for (int i = 0; i < lstDetails.Count; i++)
                    {
                        sProductDetail.Append("<tr style='background-color: #F7F6F3;font-size:12;font-family:Arial ;'><td>"
                            + lstDetails[i].Line_no + "</td><td>" + lstDetails[i].ODSKUCode + "</td><td>" + lstDetails[i].DirectCode + "</td><td>" + lstDetails[i].VendorCode);
                        sProductDetail.Append("</td><td>" + lstDetails[i].ProductDescription + "</td><td>" + lstDetails[i].UOM + "</td><td>" + lstDetails[i].OriginalQuantity + "</td><td>"
                            + lstDetails[i].ReceiptedQty
                            + "</td><td>" + lstDetails[i].InvoiceQty + "</td>");
                        sProductDetail.Append("</tr>");
                    }
                    break;

                case "shuttle":
                    sProductDetail.Append("<tr style='color:#934500;border-bottom: 1px solid #E8E8E8;font-weight: bold;font-family:Arial ;font-size:12;vertical-align: top;'><td width='5%'>"
                        + strLine + "</td><td width='10%'>" + strODCode
                        + "</td><td width='10%'>" + strVikingCode + "</td><td width='30%'>" + Description + "</td><td width='10%'>"
                        + strOriginalPOQty + "</td><td  width='10%'>" + OutstandingPOQty + "</td><td  width='5%'>" + UOM
                        + "</td><td  width='15%'>" + strType + "</td><td  width='10%'>" + strDiscrepancyQty + "</td></tr>");
                    for (int i = 0; i < lstDetails.Count; i++)
                    {
                        sProductDetail.Append("<tr style='background-color: #F7F6F3;font-size:12;font-family:Arial ;'><td>"
                            + lstDetails[i].Line_no + "</td><td>" + lstDetails[i].ODSKUCode + "</td><td>" + lstDetails[i].DirectCode);
                        sProductDetail.Append("</td><td>" + lstDetails[i].ProductDescription + "</td><td>" + lstDetails[i].OriginalQuantity + "</td><td>"
                            + lstDetails[i].OutstandingQuantity + "</td><td>" + lstDetails[i].UOM + "</td><td>" + lstDetails[i].ShuttleType
                            + "</td><td>" + lstDetails[i].DiscrepancyQty + "</td>");
                        sProductDetail.Append("</tr>");
                    }
                    break;

                case "itemnotonpo":
                    sProductDetail.Append("<tr style='color:#934500;border-bottom: 1px solid #E8E8E8;font-weight: bold;font-family:Arial ;font-size:12;vertical-align: top;'><td width='15%'>"
                        + strODCode + "</td><td width='15%' class='style10'>"
                         + strVikingCode + "</td><td width='20%' class='style10'>" + Suppliercode + "</td><td width='30%' class='style10'>"
                         + Description + "</td><td  width='10%' class='style10'>" + ReceivedQuantity + "</td><td  width='10%' class='style10'>"
                         + UOM + "</td></tr>");
                    for (int i = 0; i < lstDetails.Count; i++)
                    {
                        sProductDetail.Append("<tr style='background-color: #F7F6F3;font-size:12;font-family:Arial ;'><td>"
                            + lstDetails[i].ODSKUCode + "</td><td>"
                            + lstDetails[i].DirectCode + "</td><td>" + lstDetails[i].VendorCode + "</td><td>"
                            + lstDetails[i].ProductDescription + "</td><td>" + lstDetails[i].DeliveredQuantity + "</td><td>" + lstDetails[i].UOM + "</td></tr>");
                    }
                    break;
            }
            #endregion
            sProductDetail.Append("</table>");
        }
        else
        {
            #region No Item Grid
            switch (discrepancyType.ToLower())
            {
                case "presentationissue":
                    //if (lstDisLog != null && lstDisLog.Count > 0)
                    //{
                    //    sDiscrepancyRelatedtext = "A delivery was scheduled into our Distribution Centre in ##POSiteAddress## today. The delivery failed requirements as outlined in out Supplier Guideline, Subsequent deliveries which fail our requirements maybe refused.<br /><br />Please find details of the problem catalogued below.<br /><br /><font style='font-weight:bold;font-size:12;font-family:Arial;'>DELIVERY NOT PRE BOOKED IN ACCORDANCE WITH SUPPLIER GUIDELINES.</font>";
                    //}
                    //sLine1 = "Please advise of remedial action taken to address the issue by logging onto our discrepancy portal and following the instruction.<br />Portal link is noted below<br />##PortalLink##" ;                    
                    break;
                case "incorrectaddress":
                    //if (lstDisLog != null && lstDisLog.Count > 0 && lstDisLog[0].Site!=null )
                    //{
                    //    sDiscrepancyRelatedtext = "The above delivery was received at our warehouse in ##SiteName##; however the correct delivery address is ##POSiteAddress## as shown on the Purchase Order.";
                    //}
                    //sLine1="In the first instance please contact undersigned to confirm of action taken.";
                    break;
                case "failpalletspecification":
                    //sDiscrepancyRelatedtext = "The delivery received from yourselves on the above date relating to the above Purchase Order Number, failed requirements as outlined in our Supplier Guidelines.<br /><br />Please find details of the problem catalogued below:<br /><br /><font style='font-weight:bold;font-size:12;font-family:Arial;'>THESE PRODUCTS ARE STORED BY BLOCK STACKING. THE PALLETS NEED TO BE STACKED LEVEL WITH AN EVEN/LEVEL TOP.</font>";
                    //sLine1 = "In the first instance please contact the undersigned so as the";
                    break;
                case "prematureinvoice":
                    //sDiscrepancyRelatedtext = "The invoice received from yourselves on the above date relating to the above Purchase Order Number. Appears to have been sent in advance of the required goods.";
                    //sLine1 = "Please contact the undersigned to confirm required actions.";
                    break;
                case "qualityissue":
                    //if (lstDisLog != null && lstDisLog.Count > 0)
                    //{
                    //    sDiscrepancyRelatedtext = "The good received had a packaging / quality issue as noted below.<br /><br />##QualityIssueComment##";
                    //}
                    //sLine1 = "Please contact the undersigned to confirm required actions.";
                    break;
                case "generic":
                    //if (lstDisLog != null && lstDisLog.Count > 0)
                    //{
                    //    sDiscrepancyRelatedtext = "We want to notify you of a Discrepancy as noted below<br /><br />##InternalComments##";
                    //    sLine1 = "Please action as following<br /><br />##GenericDiscrepancyActionRequired##<br /><br /><br /><br />If you have any queries please contact the undersigned";
                    //}
                    break;
            }
            #endregion
        }

        if (discrepancyType.ToLower() == "paperworkamended" || discrepancyType.ToLower() == "generic" || discrepancyType.ToLower() == "presentationissue")
        {
            if (getCommunicationWithEscalation(lstDisLog[0].SiteID))
                oSendCommunicationCommon.getResourceValue("Communication1", discrepancyType + "_WithEscalation", out sDiscrepancyRelatedtext, out sLine1);
            else
                oSendCommunicationCommon.getResourceValue("Communication1", discrepancyType + "_NoEscalation", out sDiscrepancyRelatedtext, out sLine1);
        }
        else if (discrepancyType.ToLower() == "invoice")
        {
            oSendCommunicationCommon.getResourceValue("Communication1", discrepancyType + "_" + sLetterType, out sDiscrepancyRelatedtext, out sLine1);
        }
        else
            oSendCommunicationCommon.getResourceValue("Communication1", discrepancyType, out sDiscrepancyRelatedtext, out sLine1, lstDisLog[0].GoodsReturnedDriver);


        if (lstDisLog != null && lstDisLog[0] != null && sDiscrepancyRelatedtext != null)
        {
            sDiscrepancyRelatedtext = sDiscrepancyRelatedtext.Replace("##QualityIssueComment##", lstDisLog[0].QualityIssueComment);
            sDiscrepancyRelatedtext = sDiscrepancyRelatedtext.Replace("##InternalComments##", lstDisLog[0].InternalComments);
            sDiscrepancyRelatedtext = sDiscrepancyRelatedtext.Replace("##GenericDiscrepancyActionRequired##", lstDisLog[0].GenericDiscrepancyActionRequired);
            sDiscrepancyRelatedtext = sDiscrepancyRelatedtext.Replace("##POSiteAddress##", lstDisLog[0].POSiteName);
            sDiscrepancyRelatedtext = sDiscrepancyRelatedtext.Replace("##SiteName##", lstDisLog[0].Site.SiteName);
            sDiscrepancyRelatedtext = sDiscrepancyRelatedtext.Replace("##FailPalletSpecificationComment##", lstDisLog[0].FailPalletSpecificationComment);
            sDiscrepancyRelatedtext = sDiscrepancyRelatedtext.Replace("##GenericDiscrepancyIssueComment##", lstDisLog[0].GenericDiscrepancyIssueComment);
            sDiscrepancyRelatedtext = sDiscrepancyRelatedtext.Replace("##PresentationIssueComment##", lstDisLog[0].PersentationIssueComment);
            sDiscrepancyRelatedtext = sDiscrepancyRelatedtext.Replace("{QtyToBeSelected}", lstDisLog[0].QtyToBeSelected);



            if (!string.IsNullOrEmpty(sLine1))
            {

                sLine1 = sLine1.Replace("##PortalLink##", "<a href='" + sPortalLink + "'>" + sPortalLink + " </a>");
                sLine1 = sLine1.Replace("##GenericDiscrepancyActionRequired##", lstDisLog[0].GenericDiscrepancyActionRequired);
                //    sLine1 = sLine1.Replace("##FreightCharges##", Convert.ToString(lstDisLog[0].Freight_Charges));
            }
        }

        return sProductDetail.ToString();
    }
    public bool getCommunicationWithEscalation(int? siteID)
    {
        MAS_SiteBE oMAS_SiteBE = new MAS_SiteBE();
        MAS_SiteBAL oMAS_SiteBAL = new MAS_SiteBAL();
        oMAS_SiteBE.Action = "GetSiteDisSettings";
        oMAS_SiteBE.SiteID = siteID ?? 0;
        oMAS_SiteBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
        List<MAS_SiteBE> lstSiteDetails = oMAS_SiteBAL.GetSiteMisSettingBAL(oMAS_SiteBE);
        if (lstSiteDetails != null && lstSiteDetails.Count > 0 && lstSiteDetails[0].DIS_CommunicationWithEscalationType.Trim().ToUpper() != "N")
            return true;
        else
            return false;
    }

    public int? sendInitialMailToSPForReservandItemNotOnPODiscrepancy(int iDiscrepancyLogID, List<DiscrepancyBE> lstDisLog, List<DiscrepancyBE> lstDetails, List<DISLog_WorkFlowBE> lstWorkFlowActionDetails, string discrepancyType,
        List<DiscrepancyBE> lstStockPlanner, int queryDiscID, bool IsFromVendor = false)
    {
        string EmailSubjectText = string.Empty;
        string htmlBody = string.Empty;
        int? retValue = null;

        string sDiscrepancyRelatedText, sLine1, sLine2 = string.Empty;

        DataTable dt = oSendCommunicationCommon.GetUserPhoneNumber("GetuserPhoneNumber", Convert.ToInt32(Session["UserID"]));

        if (lstStockPlanner != null && lstStockPlanner.Count > 0)
        {
            string SPLanguage = lstStockPlanner[0].User.Language;
            if (SPLanguage != null)
            {
                string sMailAddress = lstStockPlanner[0].StockPlannerEmailID;

                int? SPLanguageId = Convert.ToInt32(lstStockPlanner[0].User.LanguageID);

                APPBOK_CommunicationBAL oAPPBOK_CommunicationBAL = new APPBOK_CommunicationBAL();
                List<MAS_LanguageBE> oLanguages = oAPPBOK_CommunicationBAL.GetLanguages();

                foreach (MAS_LanguageBE objLanguage in oLanguages)
                {
                    bool MailSentInLanguage = false;

                    if (objLanguage.Language.ToLower() == SPLanguage.ToLower())
                    {
                        MailSentInLanguage = true;
                    }

                    #region get file using languageid
                    //path = path.Replace(@"\bin\debug", "");                       

                    string templateFile = null;
                    templateFile = path + templatesPath + "/";

                    if (discrepancyType.ToLower() == "itemnotonpo")
                        templateFile += "dis_InitialCommunicationToSPItemNotOnPo";
                    else
                        templateFile += "dis_InitialCommunicationToSPReservation"; // for reservation discrepancy type

                    templateFile += ".english.htm";


                    // Changing Resourse file according to language                            

                    switch (objLanguage.LanguageID)
                    {
                        case 1:
                            Page.UICulture = clsConstants.EnglishISO; //"en-US"; // English
                            break;
                        case 2:
                            Page.UICulture = clsConstants.FranceISO; //"fr"; // France
                            break;
                        case 3:
                            Page.UICulture = clsConstants.GermanyISO; //"de"; // German
                            break;
                        case 4:
                            Page.UICulture = clsConstants.NederlandISO; //"nl"; // Dutch
                            break;
                        case 5:
                            Page.UICulture = clsConstants.SpainISO; //"es"; // Spanish
                            break;
                        case 6:
                            Page.UICulture = clsConstants.ItalyISO; //"it"; // Italy
                            break;
                        case 7:
                            Page.UICulture = clsConstants.CzechISO; //"cz"; // Czech
                            break;
                        default:
                            Page.UICulture = clsConstants.EnglishISO; // "en-US"; // English
                            break;
                    }

                    #endregion

                    #region Logic to get the exact maltilingual text based on the vendor user language ...
                    OurCode = WebCommon.getGlobalResourceValue("OurCode");
                    Description = WebCommon.getGlobalResourceValue("Description1");
                    VendorItemCode = WebCommon.getGlobalResourceValue("VendorItemCode");
                    OutstandingPOQty = WebCommon.getGlobalResourceValue("OutstandingPOQty");
                    DeliveryNoteQty = WebCommon.getGlobalResourceValue("DeliveryNoteQty");
                    DeliveredQty = WebCommon.getGlobalResourceValue("DeliveredQty");
                    QtyOverDelivered = WebCommon.getGlobalResourceValue("QtyOverDelivered");
                    ReceivedQuantity = WebCommon.getGlobalResourceValue("ReceivedQuantity1");
                    ShortageQuantity = WebCommon.getGlobalResourceValue("ShortageQuantity");
                    DamagedQuantity = WebCommon.getGlobalResourceValue("DamagedQuantity");
                    DamagedDescription = WebCommon.getGlobalResourceValue("DamagedDescription");
                    RequiredCode = WebCommon.getGlobalResourceValue("RequiredCode");
                    UOM = WebCommon.getGlobalResourceValue("UOM");
                    CodeReceived = WebCommon.getGlobalResourceValue("CodeReceived");
                    QuantityAdvisedOnDNote = WebCommon.getGlobalResourceValue("QuantityAdvisedOnDNote");
                    QuantityPaperworkAmendedto = WebCommon.getGlobalResourceValue("QuantityPaperworkAmendedto");
                    POPackSize = WebCommon.getGlobalResourceValue("POPackSize");
                    PackSizeofgooddelivered = WebCommon.getGlobalResourceValue("PackSizeofgooddelivered");
                    strLine = WebCommon.getGlobalResourceValue("Line");
                    strODCode = WebCommon.getGlobalResourceValue("ODCode");
                    strVikingCode = WebCommon.getGlobalResourceValue("VikingCode");
                    strOriginalPOQty = WebCommon.getGlobalResourceValue("OriginalPOQty");
                    strType = WebCommon.getGlobalResourceValue("Type");
                    strDiscrepancyQty = WebCommon.getGlobalResourceValue("DiscrepancyQty");
                    strHi = WebCommon.getGlobalResourceValue("Hi");
                    strShuttleDiscMessage = WebCommon.getGlobalResourceValue("ShuttleDiscMessage");
                    strShortage = WebCommon.getGlobalResourceValue("Shortage");
                    strManyThanks = WebCommon.getGlobalResourceValue("ManyThanks");
                    ChaseQuantity = WebCommon.getGlobalResourceValue("ChaseQuantity");
                    Comments = WebCommon.getGlobalResourceValue("Comments");


                    #endregion

                    string sProductGrid = getDiscrepancyItemGrid(discrepancyType, lstDisLog, lstDetails, out sDiscrepancyRelatedText, out sLine1, out sLine2);
                    if (System.IO.File.Exists(templateFile.ToLower()))
                    {
                        EmailSubjectText = WebCommon.getGlobalResourceValue("InitialCommToSPMailSubject");

                        string siteAddress = string.Empty, accountPayable = string.Empty, mailBodyToSaveInDB = string.Empty;
                        htmlBody = oSendCommunicationCommon.getMailBody(iDiscrepancyLogID, discrepancyType, lstDisLog, templateFile, sProductGrid, sLine1,
                            sDiscrepancyRelatedText, out siteAddress, out accountPayable, string.Empty, IsFromVendor);

                        htmlBody = htmlBody.Replace("{POHash}", WebCommon.getGlobalResourceValue("POHash"));
                        htmlBody = htmlBody.Replace("{InvoiceNumHash}", WebCommon.getGlobalResourceValue("InvoiceNumHash"));
                        htmlBody = htmlBody.Replace("{DiscrepancyNo}", WebCommon.getGlobalResourceValue("DiscrepancyNo"));
                        htmlBody = htmlBody.Replace("{DearSirMadam}", WebCommon.getGlobalResourceValue("DearSirMadam"));
                        htmlBody = htmlBody.Replace("{InvoiceQueryComm1Line1}", WebCommon.getGlobalResourceValue("InvoiceQueryComm1Line1"));
                        htmlBody = htmlBody.Replace("{InvoiceQueryComm1Line2}", WebCommon.getGlobalResourceValue("InvoiceQueryComm1Line2"));
                        htmlBody = htmlBody.Replace("{Yoursfaithfully}", WebCommon.getGlobalResourceValue("Yoursfaithfully"));
                        htmlBody = htmlBody.Replace("{PortalLink}", "<a href=" + sPortalLink + ">" + sPortalLink + " </a>");
                        htmlBody = htmlBody.Replace("{DearSP}", WebCommon.getGlobalResourceValue("DearSP"));
                        htmlBody = htmlBody.Replace("{InitialCommToSPText1}", WebCommon.getGlobalResourceValue("InitialCommToSPText1"));
                        htmlBody = htmlBody.Replace("{InitialCommToSPText2}", WebCommon.getGlobalResourceValue("InitialCommToSPText2"));
                        htmlBody = htmlBody.Replace("{Detailsbelow}", WebCommon.getGlobalResourceValue("Detailsbelow"));

                        List<DiscrepancyBE> lstDisLogSiteVendor = null;
                        if (discrepancyType.ToLower() == "invoice query")
                        {
                            lstDisLogSiteVendor = getDiscrepancyLogSiteVendorDetail(iDiscrepancyLogID, discrepancyType, out siteAddress, out accountPayable, "L");
                        }
                        if (discrepancyType.ToLower() == "invoice query")
                        {
                            htmlBody = htmlBody.Replace("{InvoiceValue}", lstDisLog[0].DeliveryNoteNumber);
                            htmlBody = htmlBody.Replace("{DiscrepancyValue}", lstDisLog[0].VDRNo);

                            htmlBody = htmlBody.Replace("##SITENAME##", lstDisLog[0].Site.SiteName.Split('-').ElementAtOrDefault(1));

                            htmlBody = htmlBody.Replace("{APClerkletter}", WebCommon.getGlobalResourceValue("APClerkletter"));

                            htmlBody = htmlBody.Replace("{GoodsINTeam}", WebCommon.getGlobalResourceValue("GoodsINTeam"));
                            htmlBody = htmlBody.Replace("{MediatorTeam}", WebCommon.getGlobalResourceValue("MediatorTeam"));

                            htmlBody = htmlBody.Replace("{INVAPClerkContactNo}", dt.Rows[0][0].ToString());

                            htmlBody = htmlBody.Replace("{APClerkName}", lstDisLogSiteVendor[0].StockPlannerName);
                            htmlBody = !string.IsNullOrWhiteSpace(lstDisLogSiteVendor[0].StockPlannerContact)
                                ? htmlBody.Replace("{APClerkContactNo}", lstDisLogSiteVendor[0].StockPlannerContact)
                                : htmlBody.Replace("{APClerkContactNo}", string.Empty);

                            htmlBody = htmlBody.Replace("{POValue}", lstDisLog[0].PurchaseOrderNumber);

                            htmlBody = htmlBody.Replace("{GridValue}", sProductGrid);

                        }


                        #region Logic to set the Shuttle types ...
                        strShortage = string.Empty;
                        for (int intIndex = 0; intIndex < lstDetails.Count; intIndex++)
                        {
                            strShortage = !string.IsNullOrEmpty(strShortage) && !string.IsNullOrEmpty(lstDetails[intIndex].ShuttleType)
                                ? string.Format("{0}, {1}", strShortage, lstDetails[intIndex].ShuttleType)
                                : lstDetails[intIndex].ShuttleType;
                        }
                        #endregion


                        if (discrepancyType.ToLower() == "invoice" && sLetterType == "lettertype1")
                            htmlBody = htmlBody.Replace("{APCommentsText}", lstWorkFlowActionDetails[1].Comment);
                        else if (discrepancyType.ToLower() == "invoice" && (sLetterType == "lettertype2" || sLetterType == "lettertype3"))
                        {
                            htmlBody = htmlBody.Replace("{Comments} :", "");
                            htmlBody = htmlBody.Replace("{APCommentsText}", "");
                        }

                        htmlBody = htmlBody.Replace("{Shortage}", strShortage);
                        htmlBody = htmlBody.Replace("{ManyThanksText}", strManyThanks);
                        htmlBody = htmlBody.Replace("{Comments}", Comments);

                        mailBodyToSaveInDB = htmlBody.Replace("{logoInnerPath}", oSendCommunicationCommon.getAbsolutePath());

                        retValue = oSendCommunicationCommon.saveMail(iDiscrepancyLogID, sMailAddress, mailBodyToSaveInDB, EmailSubjectText, objLanguage.LanguageID,
                            "communication1", MailSentInLanguage, queryDiscID);
                        if (retValue == null)
                            oSendCommunicationCommon.errorLog(iDiscrepancyLogID, EmailSubjectText, lstStockPlanner[0].StockPlannerEmailID, htmlBody, "Error in save mail to database");

                        if (objLanguage.LanguageID == SPLanguageId)
                        {
                            clsEmail oclsEmail = new clsEmail();
                            oclsEmail.sendMail(sMailAddress, htmlBody, EmailSubjectText, sFromAddress, true);

                            sSendMailLink.Append("<a target='_blank' href='" + Common.EncryptQuery("DISLog_ReSendCommunication.aspx?communicationid=" + retValue + "&CommunicationLevel=Communication1") + "' onclick='window.open(this.href, this.target,\"scrollbars=1,left=100px,top=100px,width=1000px,height=800px\"); return false;'>" + sMailAddress + "</a>" + System.Environment.NewLine);
                        }

                    }
                }

                //setting default Resourse file
                Page.UICulture = Convert.ToString(Session["CultureInfo"]);
                return retValue;
            }
            else
            {
                oSendCommunicationCommon.errorLog(iDiscrepancyLogID, EmailSubjectText, lstStockPlanner[0].StockPlannerEmailID, htmlBody, "No Data for Language");
                return null;
            }

        }
        return retValue;
    }


    StringBuilder sbMessage = new StringBuilder();
    public void SendMailToVendor(string toAddress, string language, List<APPBOK_BookingBE> lstBooking, string templatePathName,
        string BookingType, string IsBookingAmended, string IsBookingEditWithMail, MAS_SiteBE singleSiteSetting,
        string DeliveryDate, string DeliveryTime, string Pallets, string Lifts, string Cartons, string Lines, string BookingComments, List<MAS_LanguageBE> oLanguages, int? UserID)
    {
        if (!string.IsNullOrEmpty(toAddress))
        {
            string htmlBody = string.Empty;
            if (UserID == null)
                UserID = 1;


            string templatePath = null;
            templatePath = path + templatePathName;
            string LanguageFile = string.Empty;

            APPBOK_CommunicationBAL oAPPBOK_CommunicationBAL = new APPBOK_CommunicationBAL();
            clsEmail oclsEmail = new clsEmail();

            foreach (MAS_LanguageBE objLanguage in oLanguages)
            {
                bool MailSentInLanguage = false;

                if (objLanguage.Language.ToLower() == language.ToLower())
                {
                    MailSentInLanguage = true;
                }

                if (MailSentInLanguage)
                {
                    sbMessage.AppendLine("6.BookingID :- " + Convert.ToString(lstBooking[0].BookingID));
                    sbMessage.AppendLine("6.UserID :- " + Convert.ToString(UserID));
                    
                }

                LanguageFile = lstBooking[0].BookingTypeID == 5
                    ? BookingType == "PNTD" ? "ProvisionalBookingMadeLetter.english.htm" : "Provisional.english.htm"
                    : "Confirmation.english.htm";

                if (!System.IO.File.Exists(templatePath + LanguageFile.ToLower()))
                {
                    LanguageFile = lstBooking[0].BookingTypeID == 5
                        ? BookingType == "PNTD" ? "ProvisionalBookingMadeLetter.english.htm" : "Provisional.english.htm"
                        : "Confirmation.english.htm";
                }

                if (MailSentInLanguage)
                    sbMessage.AppendLine("6.LanguageFile :- " + Convert.ToString(LanguageFile));

                using (StreamReader sReader = new StreamReader(templatePath + LanguageFile.ToLower()))
                {
                    #region mailBody
                    htmlBody = sReader.ReadToEnd();

                    /* Changed for [ProvisionalBookingMadeLetter.english.htm] file */
                    htmlBody = htmlBody.Replace("{ProvisionalBookingMLText1}", WebCommon.getGlobalResourceValueByLangID("ProvisionalBookingMLText1", objLanguage.LanguageID));
                    htmlBody = htmlBody.Replace("{ProvisionalBookingMLText2}", WebCommon.getGlobalResourceValueByLangID("ProvisionalBookingMLText2", objLanguage.LanguageID));
                    htmlBody = htmlBody.Replace("{ProvisionalBookingMLText3}", WebCommon.getGlobalResourceValueByLangID("ProvisionalBookingMLText3", objLanguage.LanguageID));
                    htmlBody = htmlBody.Replace("{ProvisionalBookingMLText4}", WebCommon.getGlobalResourceValueByLangID("ProvisionalBookingMLText4", objLanguage.LanguageID));

                    /* Changed for [Provisional.english.htm] file */
                    htmlBody = htmlBody.Replace("{ProvisionalText1}", WebCommon.getGlobalResourceValueByLangID("ProvisionalText1", objLanguage.LanguageID));
                    htmlBody = htmlBody.Replace("{ProvisionalText2}", WebCommon.getGlobalResourceValueByLangID("ProvisionalText2", objLanguage.LanguageID));
                    htmlBody = htmlBody.Replace("{ProvisionalText3}", WebCommon.getGlobalResourceValueByLangID("ProvisionalText3", objLanguage.LanguageID));
                    htmlBody = htmlBody.Replace("{ProvisionalText4}", WebCommon.getGlobalResourceValueByLangID("ProvisionalText4", objLanguage.LanguageID));
                    htmlBody = htmlBody.Replace("{ProvisionalText5}", WebCommon.getGlobalResourceValueByLangID("ProvisionalText5", objLanguage.LanguageID));
                    htmlBody = htmlBody.Replace("{ProvisionalText6}", WebCommon.getGlobalResourceValueByLangID("ProvisionalText6", objLanguage.LanguageID));
                    htmlBody = htmlBody.Replace("{ProvisionalText7}", WebCommon.getGlobalResourceValueByLangID("ProvisionalText7", objLanguage.LanguageID));

                    /* Changed for [Confirmation.english.htm] file */
                    htmlBody = htmlBody.Replace("{ConfirmationText1}", WebCommon.getGlobalResourceValueByLangID("ConfirmationText1", objLanguage.LanguageID));
                    htmlBody = htmlBody.Replace("{ConfirmationText2}", WebCommon.getGlobalResourceValueByLangID("ConfirmationText2", objLanguage.LanguageID));
                    htmlBody = htmlBody.Replace("{ConfirmationText3}", WebCommon.getGlobalResourceValueByLangID("ConfirmationText3", objLanguage.LanguageID));
                    htmlBody = htmlBody.Replace("{ConfirmationText4}", WebCommon.getGlobalResourceValueByLangID("ConfirmationText4", objLanguage.LanguageID));
                    htmlBody = htmlBody.Replace("{ConfirmationText5}", WebCommon.getGlobalResourceValueByLangID("ConfirmationText5", objLanguage.LanguageID));
                    htmlBody = htmlBody.Replace("{ConfirmationText6}", WebCommon.getGlobalResourceValueByLangID("ConfirmationText6", objLanguage.LanguageID));

                    if (lstBooking[0].IsVenodrPalletChecking || lstBooking[0].ISPM15CountryPalletChecking)
                        htmlBody = htmlBody.Replace("{IsVendorPalletCheckingForISPM15}", WebCommon.getGlobalResourceValueByLangID("IsVendorPalletCheckingForISPM15", objLanguage.LanguageID));
                    else if (lstBooking[0].IsStandardPalletChecking)
                        htmlBody = htmlBody.Replace("{IsVendorPalletCheckingForISPM15}", WebCommon.getGlobalResourceValueByLangID("IsStandardPalletCheckingForISPM15", objLanguage.LanguageID));
                    else
                        htmlBody = htmlBody.Replace("{IsVendorPalletCheckingForISPM15}", string.Empty);

                    htmlBody = lstBooking[0].IsEnableHazardouesItemPrompt
                         ? htmlBody.Replace("{IsEnableHazardouesItemPrompt}", WebCommon.getGlobalResourceValueByLangID("DeliveryContainsHazardeousProducts", objLanguage.LanguageID))
                         : htmlBody.Replace("{IsEnableHazardouesItemPrompt}", "");

                    /* Changed For All letters files */
                    htmlBody = htmlBody.Replace("{BookingRefValue}", lstBooking[0].BookingRef);

                    htmlBody = htmlBody.Replace("{Date}", WebCommon.getGlobalResourceValueByLangID("Date", objLanguage.LanguageID));
                    htmlBody = htmlBody.Replace("{DateValue}", DeliveryDate);

                    htmlBody = htmlBody.Replace("{Time}", WebCommon.getGlobalResourceValueByLangID("Time", objLanguage.LanguageID));
                    htmlBody = htmlBody.Replace("{TimeActual}", WebCommon.getGlobalResourceValueByLangID("TimeActual", objLanguage.LanguageID));
                    htmlBody = htmlBody.Replace("{TimeValue}", DeliveryTime);

                    htmlBody = htmlBody.Replace("{Carrier}", WebCommon.getGlobalResourceValueByLangID("Carrier", objLanguage.LanguageID));
                    htmlBody = htmlBody.Replace("{CarrierValue}", lstBooking[0].Carrier.CarrierName);

                    htmlBody = htmlBody.Replace("{VehicleTypeActual}", WebCommon.getGlobalResourceValueByLangID("VehicleTypeActual", objLanguage.LanguageID));
                    htmlBody = htmlBody.Replace("{VehicleType}", WebCommon.getGlobalResourceValueByLangID("VehicleType", objLanguage.LanguageID));
                    htmlBody = htmlBody.Replace("{VehicleTypeValue}", lstBooking[0].VehicleType.VehicleType);

                    htmlBody = htmlBody.Replace("{NumberofPallets}", WebCommon.getGlobalResourceValueByLangID("NumberofPallets", objLanguage.LanguageID));
                    htmlBody = htmlBody.Replace("{PalletsValue}", Pallets);

                    htmlBody = htmlBody.Replace("{NumberofLifts}", WebCommon.getGlobalResourceValueByLangID("NumberofLifts", objLanguage.LanguageID));
                    htmlBody = htmlBody.Replace("{LiftsValue}", Lifts);

                    htmlBody = htmlBody.Replace("{NumberofCartons}", WebCommon.getGlobalResourceValueByLangID("NumberofCartons", objLanguage.LanguageID));
                    htmlBody = htmlBody.Replace("{CartonsValue}", Cartons);

                    htmlBody = htmlBody.Replace("{NumberofPOLines}", WebCommon.getGlobalResourceValueByLangID("NumberofPOLines", objLanguage.LanguageID));
                    htmlBody = htmlBody.Replace("{LinesValue}", Lines);

                    htmlBody = htmlBody.Replace("{BookedPONo}", WebCommon.getGlobalResourceValueByLangID("BookedPONo", objLanguage.LanguageID));
                    htmlBody = htmlBody.Replace("{BookedPOsValue}", lstBooking[0].PurchaseOrders.TrimStart(',').TrimEnd(','));

                    //---Stage 18 Point 3-----//
                    if (BookingComments != string.Empty && BookingComments != "")
                    {
                        htmlBody = htmlBody.Replace("{trVisible}", "");
                        htmlBody = htmlBody.Replace("{AdditionalInfo}", WebCommon.getGlobalResourceValueByLangID("AdditionalInfo", objLanguage.LanguageID));
                        htmlBody = htmlBody.Replace("{AdditionalInfoValue}", BookingComments);
                    }
                    else
                    {
                        htmlBody = htmlBody.Replace("{trVisible}", "none");
                    }


                    //htmlBody = htmlBody.Replace("{VendorName}", vendorName);
                    htmlBody = htmlBody.Replace("{PortalLink}", "<a href=" + sPortalLink + ">" + sPortalLink + " </a>");
                    //---Stage 7 Point 29-----//
                    if (singleSiteSetting != null && singleSiteSetting.NonTimeStart != string.Empty)
                    {
                        htmlBody = htmlBody.Replace("{tblVisible}", "");
                        htmlBody = htmlBody.Replace("{SiteNameValue}", singleSiteSetting.SiteName);
                        htmlBody = htmlBody.Replace("{NonTimedStartTimeValue}", singleSiteSetting.NonTimeStart);
                        htmlBody = htmlBody.Replace("{NonTimedEndTimeValue}", singleSiteSetting.NonTimeEnd);
                    }
                    else
                    {
                        htmlBody = htmlBody.Replace("{tblVisible}", "none");
                    }

                    htmlBody = htmlBody.Replace("{Yoursfaithfully}", WebCommon.getGlobalResourceValueByLangID("Yoursfaithfully", objLanguage.LanguageID));

                    //------------------------//
                    #endregion
                }

                string sFromAddress = Convert.ToString(ConfigurationManager.AppSettings["fromAddress"]);

                // Resending of Delivery mails
                if (MailSentInLanguage)
                {
                    sbMessage.AppendLine("7.IsBookingAmended :- " + Convert.ToString(lstBooking[0].IsBookingAmended));
                    sbMessage.AppendLine("8.IsBookingEditWithMail :- " + Convert.ToString(IsBookingEditWithMail));
                }

                //Stage 6 point Point 8
                if (lstBooking[0].IsBookingAmended)
                {
                    if (IsBookingAmended == "True")
                    {
                        if ((IsBookingEditWithMail != null) && (IsBookingEditWithMail == "true"))
                        {
                            oAPPBOK_CommunicationBAL.AddItemBAL(lstBooking[0].BookingID, sFromAddress, toAddress, "Amendment of your Booking", htmlBody, UserID, CommunicationType.Enum.BookingAmended, objLanguage.LanguageID, MailSentInLanguage, MailSentInLanguage);
                            if (objLanguage.Language.ToLower() == language.ToLower())
                            {
                                oclsEmail.sendMail(toAddress, htmlBody, "Amendment of your Booking", sFromAddress, true);
                            }
                        }
                        else if ((IsBookingEditWithMail != null) && (IsBookingEditWithMail == "false"))
                        {
                            oAPPBOK_CommunicationBAL.AddItemBAL(lstBooking[0].BookingID, sFromAddress, toAddress, "Amendment of your Booking", htmlBody, UserID, CommunicationType.Enum.BookingAmended, objLanguage.LanguageID, MailSentInLanguage, false);
                        }
                        else
                        {
                            oAPPBOK_CommunicationBAL.AddItemBAL(lstBooking[0].BookingID, sFromAddress, toAddress, "Amendment of your Booking", htmlBody, UserID, CommunicationType.Enum.BookingAmended, objLanguage.LanguageID, MailSentInLanguage, MailSentInLanguage);
                            if (objLanguage.Language.ToLower() == language.ToLower())
                            {
                                oclsEmail.sendMail(toAddress, htmlBody, "Amendment of your Booking", sFromAddress, true);
                            }
                        }
                    }
                }
                else
                {
                    if (MailSentInLanguage)
                        sbMessage.AppendLine("9.BookingID :- " + Convert.ToString(lstBooking[0].BookingID));

                    if (objLanguage.Language.ToLower() == language.ToLower())
                    {
                        oclsEmail.sendMail(toAddress, htmlBody, "Confirmation of your Booking", sFromAddress, true);
                    }

                    oAPPBOK_CommunicationBAL.AddItemBAL(lstBooking[0].BookingID, sFromAddress, toAddress, "Confirmation of your Booking", htmlBody, UserID, CommunicationType.Enum.BookingConfirmation, objLanguage.LanguageID, MailSentInLanguage, MailSentInLanguage);

                    if (MailSentInLanguage)
                    {
                        sbMessage.AppendLine("10.EmailId :- " + Convert.ToString(toAddress));
                        sbMessage.AppendLine("11.BookingID :- " + Convert.ToString(lstBooking[0].BookingID));
                        sbMessage.AppendLine("------------------------------------------------------");
                    }
                }

                if (MailSentInLanguage)
                    LogUtility.SaveTraceLogEntry(sbMessage);

                if (objLanguage.Language.ToLower() == language.ToLower())
                {
                    //Preadvice notice
                    if (lstBooking[0].PreAdviseNotification.ToUpper() == "Y")
                    {
                        string htmlBody2 = string.Empty;

                        string templatePath2 = null;
                        templatePath2 = path + templatePathName;
                        string LanguageFile2 = "Pre-advise.english.htm";

                        using (StreamReader sReader = new StreamReader(templatePath2 + LanguageFile2.ToLower()))
                        {
                            htmlBody2 = sReader.ReadToEnd();
                            htmlBody2 = htmlBody2.Replace("{DearSirMadam}", WebCommon.getGlobalResourceValueByLangID("DearSirMadam", objLanguage.LanguageID));
                            htmlBody2 = htmlBody2.Replace("{PreAdviseText1}", WebCommon.getGlobalResourceValueByLangID("PreAdviseText1", objLanguage.LanguageID));
                            htmlBody2 = htmlBody2.Replace("{PreAdviseText2}", WebCommon.getGlobalResourceValueByLangID("PreAdviseText2", objLanguage.LanguageID));
                            htmlBody2 = htmlBody2.Replace("{PreAdviseText3}", WebCommon.getGlobalResourceValueByLangID("PreAdviseText3", objLanguage.LanguageID));
                            htmlBody2 = htmlBody2.Replace("{Yoursfaithfully}", WebCommon.getGlobalResourceValueByLangID("Yoursfaithfully", objLanguage.LanguageID));
                        }
                        sbMessage.AppendLine("12.EmailId :- " + Convert.ToString(toAddress));
                        oclsEmail.sendMail(toAddress, htmlBody2, WebCommon.getGlobalResourceValueByLangID("PreAdviseNotification", objLanguage.LanguageID), sFromAddress, true);
                    }
                }
            }
        }
    }

    public void SendISPM15MailToVendor(string toAddress, string language, List<MAS_LanguageBE> oLanguages, string templatePathName, int? UserID)
    {
        if (!string.IsNullOrEmpty(toAddress))
        {
            string htmlBody = string.Empty;

            if (UserID == null)
                UserID = 1;

            string templatePath = null;

            templatePath = path + templatePathName + "ISPM15.english.htm";
            
            clsEmail oclsEmail = new clsEmail();

            foreach (MAS_LanguageBE objLanguage in oLanguages)
            {
                bool MailSentInLanguage = false;

                if (objLanguage.Language.ToLower() == language.ToLower())
                {
                    MailSentInLanguage = true;
                }

                using (StreamReader sReader = new StreamReader(templatePath))
                {
                    #region mailBody
                    htmlBody = sReader.ReadToEnd();

                    /* Changed for [ProvisionalBookingMadeLetter.english.htm] file */

                    htmlBody = htmlBody.Replace("{Dear}", WebCommon.getGlobalResourceValueByLangID("Dear", objLanguage.LanguageID));
                    htmlBody = htmlBody.Replace("{Vendor/Carrier}", WebCommon.getGlobalResourceValueByLangID("Vendor/Carrier", objLanguage.LanguageID));
                    htmlBody = htmlBody.Replace("{ISPM15Msg1}", WebCommon.getGlobalResourceValueByLangID("ISPM15Msg1", objLanguage.LanguageID));
                    htmlBody = htmlBody.Replace("{ISPM15Msg2}", WebCommon.getGlobalResourceValueByLangID("ISPM15Msg2", objLanguage.LanguageID));

                    /* Changed for [Provisional.english.htm] file */
                    htmlBody = htmlBody.Replace("{ISPM15Msg3}", WebCommon.getGlobalResourceValueByLangID("ISPM15Msg3", objLanguage.LanguageID));
                    htmlBody = htmlBody.Replace("{ISPM15Msg4}", WebCommon.getGlobalResourceValueByLangID("ISPM15Msg4", objLanguage.LanguageID));
                    htmlBody = htmlBody.Replace("{ISPM15Msg5}", WebCommon.getGlobalResourceValueByLangID("ISPM15Msg5", objLanguage.LanguageID));
                    htmlBody = htmlBody.Replace("{ISPM15Msg6}", WebCommon.getGlobalResourceValueByLangID("ISPM15Msg6", objLanguage.LanguageID));
                    htmlBody = htmlBody.Replace("{ISPM15Msg7}", WebCommon.getGlobalResourceValueByLangID("ISPM15Msg7", objLanguage.LanguageID));

                    htmlBody = htmlBody.Replace("{VIPAdmin}", WebCommon.getGlobalResourceValueByLangID("VIPAdmin", objLanguage.LanguageID));
                    htmlBody = htmlBody.Replace("{Yoursfaithfully}", WebCommon.getGlobalResourceValueByLangID("Yoursfaithfully", objLanguage.LanguageID));

                    //------------------------//
                    #endregion
                }

                string sFromAddress = Convert.ToString(ConfigurationManager.AppSettings["fromAddress"]);

                if (objLanguage.Language.ToLower() == language.ToLower())
                {
                    oclsEmail.sendMail(toAddress, htmlBody, "ISPM15 Pallet Check", sFromAddress, true);
                }
            }
        }
    }
    public int? SendAndSaveMailForDiscrepancyComments(int iDiscrepancy, string discrepancyType, BaseControlLibrary.ucTextbox txtAltEmail,
           BaseControlLibrary.ucTextbox ucTextBox1, int? vendorID,
           string odComment = "", string CommentBy = "")
    {
        try
        {
            string sToAddress = string.Empty;
            if (txtAltEmail != null && !string.IsNullOrEmpty(txtAltEmail.Text))
                sToAddress = txtAltEmail.Text;
            else if (ucTextBox1 != null && !string.IsNullOrEmpty(ucTextBox1.Text))
                sToAddress = ucTextBox1.Text;
            sToAddress = sToAddress.Replace(" ", "");

            if (!string.IsNullOrEmpty(sToAddress))
            {
                iVendorID = vendorID;
                return SendAndSaveMailForDiscrepancyComments(iDiscrepancy, discrepancyType, sToAddress,
                    odComment, CommentBy);
            }
            else
                oSendCommunicationCommon.showErrorMessage();

            return null;
        }
        catch (Exception)
        {
            return null;
        }
    }

    public int? SendAndSaveMailForDiscrepancyComments(int iDiscrepancy, string discrepancyType,
          string ucTextBox1, int? vendorID,
          string odComment = "", string CommentBy = "")
    {
        try
        {
            string sToAddress = string.Empty;

            if (ucTextBox1 != null && !string.IsNullOrEmpty(ucTextBox1))
                sToAddress = ucTextBox1;

            sToAddress = sToAddress.Replace(" ", "");

            if (!string.IsNullOrEmpty(sToAddress))
            {
                iVendorID = vendorID;
                return SendAndSaveMailForDiscrepancyComments(iDiscrepancy, discrepancyType, sToAddress,
                    odComment, CommentBy);
            }
            else
                oSendCommunicationCommon.showErrorMessage();

            return null;
        }
        catch (Exception)
        {
            return null;
        }
    }

    public int? SendAndSaveMailForDiscrepancyComments(int iDiscrepancyLogID, string discrepancyType, string toAddress, string odComment = "", string CommentBy = "")
    {
        string EmailSubjectText = string.Empty;
        string htmlBody = string.Empty;

        try
        {
            List<DiscrepancyBE> lstDisLog = oSendCommunicationCommon.getDiscrepancyLogDetail("GetDiscrepancyLog", Convert.ToInt32(iDiscrepancyLogID), discrepancyType);
            List<DiscrepancyBE> lstDetails = oSendCommunicationCommon.getDiscrepancyItemDetail("GetDiscrepancyItem", Convert.ToInt32(iDiscrepancyLogID), discrepancyType);
            List<DISLog_WorkFlowBE> lstWorkFlowActionDetails = oSendCommunicationCommon.getDiscrepancyWorkFlowActionDetail("GetWorkFlowActionDetails", Convert.ToInt32(iDiscrepancyLogID));

            string sDiscrepancyRelatedText = string.Empty;
            string sLine1 = string.Empty;
            string sLine2 = string.Empty;

            //vendor Email
            if (lstDisLog != null && lstDisLog.Count > 0)
            {
                int? retValue = null;

                DataSet dsLanguage = new DataSet();

                if (CommentBy == "Vendor")
                {
                    dsLanguage = oSendCommunicationCommon.getLanguage(lstDisLog[0].StockPlannerUserID);
                }
                else
                {
                    dsLanguage = oSendCommunicationCommon.getLanguage(iVendorID, toAddress);
                }

                if (dsLanguage != null && dsLanguage.Tables.Count > 0 && dsLanguage.Tables[0].Rows.Count > 0)
                {
                    string[] sMailAddress = toAddress.Split(',');

                    return SendMailForVendor(iDiscrepancyLogID, discrepancyType, toAddress, odComment, CommentBy, ref EmailSubjectText, ref htmlBody, lstDisLog, sDiscrepancyRelatedText, sLine1, ref retValue, dsLanguage, sMailAddress);
                }
                else
                {
                    oSendCommunicationCommon.errorLog(iDiscrepancyLogID, EmailSubjectText, toAddress, htmlBody, "No Data for Language");
                    return null;
                }
            }
            else
            {
                oSendCommunicationCommon.errorLog(iDiscrepancyLogID, EmailSubjectText, toAddress, htmlBody, "No Data in DiscrepancyLog table");
                return null;
            }
        }
        catch (Exception ex)
        {
            oSendCommunicationCommon.errorLog(iDiscrepancyLogID, EmailSubjectText, toAddress, htmlBody, ex.Message);
            return null;
        }
    }

    private int? SendMailForVendor(int iDiscrepancyLogID, string discrepancyType, string toAddress, string odComment,
                 string CommentBy, ref string EmailSubjectText, ref string htmlBody, List<DiscrepancyBE> lstDisLog,
                 string sDiscrepancyRelatedText, string sLine1, ref int? retValue, DataSet dsLanguage, string[] sMailAddress)
    {
        if (CommentBy == "Vendor")
        {
            #region GoodsIn
            dsLanguage = oSendCommunicationCommon.getLanguage(Convert.ToInt32(lstDisLog[0].CreatedById));

            if (dsLanguage != null && dsLanguage.Tables.Count > 0 && dsLanguage.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < dsLanguage.Tables[0].Rows.Count; i++)
                {
                    string Language = dsLanguage.Tables[0].Rows[i]["Language"].ToString();

                    int? VendorLanguageId = Convert.ToInt32(dsLanguage.Tables[0].Rows[i]["languageid"]);

                    APPBOK_CommunicationBAL oAPPBOK_CommunicationBAL = new APPBOK_CommunicationBAL();
                    List<MAS_LanguageBE> oLanguages = oAPPBOK_CommunicationBAL.GetLanguages();

                    sMailAddress = null;

                    sMailAddress = new string[] { dsLanguage.Tables[0].Rows[0][0].ToString() };

                    foreach (MAS_LanguageBE objLanguage in oLanguages)
                    {
                        bool MailSentInLanguage = false;

                        if (objLanguage.Language.ToLower() == Language.ToLower())
                        {
                            MailSentInLanguage = true;
                        }

                        #region get file using languageid  

                        string templateFile = null;

                        templateFile = path + templatesPathComments + "/";

                        templateFile += "dis_Comments_Vendor_GoodsIn.english.htm";

                        switch (objLanguage.LanguageID)
                        {
                            case 1:
                                Page.UICulture = clsConstants.EnglishISO; //"en-US"; // English
                                break;
                            case 2:
                                Page.UICulture = clsConstants.FranceISO; //"fr"; // France
                                break;
                            case 3:
                                Page.UICulture = clsConstants.GermanyISO; //"de"; // German
                                break;
                            case 4:
                                Page.UICulture = clsConstants.NederlandISO; //"nl"; // Dutch
                                break;
                            case 5:
                                Page.UICulture = clsConstants.SpainISO; //"es"; // Spanish
                                break;
                            case 6:
                                Page.UICulture = clsConstants.ItalyISO; //"it"; // Italy
                                break;
                            case 7:
                                Page.UICulture = clsConstants.CzechISO; //"cz"; // Czech
                                break;
                            default:
                                Page.UICulture = clsConstants.EnglishISO; // "en-US"; // English
                                break;
                        }

                        #endregion 

                        string sProductGrid = string.Empty;

                        if (System.IO.File.Exists(templateFile.ToLower()))
                        {
                            EmailSubjectText = WebCommon.getGlobalResourceValue("DiscrepancyCommentSubject").Replace("##VDRNo##", lstDisLog[0].VDRNo);

                            string siteAddress = string.Empty, accountPayable = string.Empty, mailBodyToSaveInDB = string.Empty;

                            htmlBody = oSendCommunicationCommon.getMailBody(iDiscrepancyLogID,
                                       discrepancyType, lstDisLog, templateFile, sProductGrid,
                                       sLine1, sDiscrepancyRelatedText,
                                       out siteAddress, out accountPayable, string.Empty);

                            htmlBody = htmlBody.Replace("{POHash}", WebCommon.getGlobalResourceValue("POHash"));
                            htmlBody = htmlBody.Replace("{DiscrepancyNo}", WebCommon.getGlobalResourceValue("DiscrepancyNo"));
                            htmlBody = htmlBody.Replace("{DearSirMadam}", WebCommon.getGlobalResourceValue("DearSirMadam"));
                            htmlBody = htmlBody.Replace("{Yoursfaithfully}", WebCommon.getGlobalResourceValue("Yoursfaithfully"));

                            htmlBody = htmlBody.Replace("{PortalLink}", "<a href=" + sPortalLink + ">" + sPortalLink + " </a>");
                            htmlBody = htmlBody.Replace("{DiscrepancyComment}", odComment);
                            htmlBody = htmlBody.Replace("{AdditionalComments}", WebCommon.getGlobalResourceValue("AdditionalComments"));
                            htmlBody = htmlBody.Replace("{DiscrepancyCommentsDisplay}", WebCommon.getGlobalResourceValue("DiscrepancyCommentsDisplay"));
                            htmlBody = htmlBody.Replace("{DiscrepancyCommentsBy}", WebCommon.getGlobalResourceValue("DiscrepancyCommentsBy").Replace("##WarehousePlannerVendor##", CommentBy));

                            mailBodyToSaveInDB = htmlBody.Replace("{logoInnerPath}", oSendCommunicationCommon.getAbsolutePath());

                            retValue = oSendCommunicationCommon.saveMail(iDiscrepancyLogID,
                            sMailAddress[i], mailBodyToSaveInDB, EmailSubjectText, objLanguage.LanguageID, "discrepancyComment", MailSentInLanguage, 0);

                            if (retValue == null)
                            {
                                oSendCommunicationCommon.errorLog(iDiscrepancyLogID, EmailSubjectText, toAddress, htmlBody, "Error in save mail to database");
                            }

                            if (objLanguage.LanguageID == VendorLanguageId)
                            {
                                clsEmail oclsEmail = new clsEmail();
                                oclsEmail.sendMail(sMailAddress[i], htmlBody, EmailSubjectText, sFromAddress, true);
                                sSendMailLink.Append("<a target='_blank' href='" + Common.EncryptQuery("DISLog_ReSendCommunication.aspx?communicationid=" + retValue + "&CommunicationLevel=Communication1") + "' onclick='window.open(this.href, this.target,\"scrollbars=1,left=100px,top=100px,width=1000px,height=800px\"); return false;'>" + sMailAddress[i] + "</a>" + System.Environment.NewLine);
                            }
                        }
                    }
                }
            }
            #endregion

            #region StockPlannerUser
            dsLanguage = oSendCommunicationCommon.getLanguage(lstDisLog[0].StockPlannerUserID);
            if (dsLanguage != null && dsLanguage.Tables.Count > 0 && dsLanguage.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < dsLanguage.Tables[0].Rows.Count; i++)
                {
                    string Language = dsLanguage.Tables[0].Rows[i]["Language"].ToString();

                    int? VendorLanguageId = Convert.ToInt32(dsLanguage.Tables[0].Rows[i]["languageid"]);

                    APPBOK_CommunicationBAL oAPPBOK_CommunicationBAL = new APPBOK_CommunicationBAL();

                    List<MAS_LanguageBE> oLanguages = oAPPBOK_CommunicationBAL.GetLanguages();

                    sMailAddress = null;

                    sMailAddress = new string[] { dsLanguage.Tables[0].Rows[0][0].ToString() };

                    foreach (MAS_LanguageBE objLanguage in oLanguages)
                    {
                        bool MailSentInLanguage = false;

                        if (objLanguage.Language.ToLower() == Language.ToLower())
                        {
                            MailSentInLanguage = true;
                        }

                        #region get file using languageid  

                        string templateFile = null;

                        templateFile = path + templatesPathComments + "/";

                        templateFile += "dis_Comments_Vendor.english.htm";

                        switch (objLanguage.LanguageID)
                        {
                            case 1:
                                Page.UICulture = clsConstants.EnglishISO; //"en-US"; // English
                                break;
                            case 2:
                                Page.UICulture = clsConstants.FranceISO; //"fr"; // France
                                break;
                            case 3:
                                Page.UICulture = clsConstants.GermanyISO; //"de"; // German
                                break;
                            case 4:
                                Page.UICulture = clsConstants.NederlandISO; //"nl"; // Dutch
                                break;
                            case 5:
                                Page.UICulture = clsConstants.SpainISO; //"es"; // Spanish
                                break;
                            case 6:
                                Page.UICulture = clsConstants.ItalyISO; //"it"; // Italy
                                break;
                            case 7:
                                Page.UICulture = clsConstants.CzechISO; //"cz"; // Czech
                                break;
                            default:
                                Page.UICulture = clsConstants.EnglishISO; // "en-US"; // English
                                break;
                        }

                        #endregion


                        string sProductGrid = string.Empty;

                        if (System.IO.File.Exists(templateFile.ToLower()))
                        {
                            EmailSubjectText = WebCommon.getGlobalResourceValue("DiscrepancyCommentSubject").Replace("##VDRNo##", lstDisLog[0].VDRNo);
                            string siteAddress = string.Empty, accountPayable = string.Empty, mailBodyToSaveInDB = string.Empty;
                            htmlBody = oSendCommunicationCommon.getMailBody(iDiscrepancyLogID, discrepancyType, lstDisLog, templateFile, sProductGrid, sLine1, sDiscrepancyRelatedText, out siteAddress, out accountPayable, string.Empty);
                            htmlBody = htmlBody.Replace("{POHash}", WebCommon.getGlobalResourceValue("POHash"));
                            htmlBody = htmlBody.Replace("{DiscrepancyNo}", WebCommon.getGlobalResourceValue("DiscrepancyNo"));
                            htmlBody = htmlBody.Replace("{DearSirMadam}", WebCommon.getGlobalResourceValue("DearSirMadam"));
                            htmlBody = htmlBody.Replace("{Yoursfaithfully}", WebCommon.getGlobalResourceValue("Yoursfaithfully"));
                            htmlBody = htmlBody.Replace("{PortalLink}", "<a href=" + sPortalLink + ">" + sPortalLink + " </a>");
                            htmlBody = htmlBody.Replace("{DiscrepancyComment}", odComment);
                            htmlBody = htmlBody.Replace("{AdditionalComments}", WebCommon.getGlobalResourceValue("AdditionalComments"));
                            htmlBody = htmlBody.Replace("{DiscrepancyCommentsDisplay}", WebCommon.getGlobalResourceValue("DiscrepancyCommentsDisplay"));
                            htmlBody = htmlBody.Replace("{DiscrepancyCommentsBy}", WebCommon.getGlobalResourceValue("DiscrepancyCommentsBy").Replace("##WarehousePlannerVendor##", CommentBy));

                            mailBodyToSaveInDB = htmlBody.Replace("{logoInnerPath}", oSendCommunicationCommon.getAbsolutePath());

                            retValue = oSendCommunicationCommon.saveMail(iDiscrepancyLogID, sMailAddress[i], mailBodyToSaveInDB, EmailSubjectText, objLanguage.LanguageID, "discrepancyComment", MailSentInLanguage, 0);

                            if (retValue == null)
                                oSendCommunicationCommon.errorLog(iDiscrepancyLogID,
                                    EmailSubjectText,
                                    toAddress, htmlBody, "Error in save mail to database");

                            if (objLanguage.LanguageID == VendorLanguageId)
                            {
                                clsEmail oclsEmail = new clsEmail();
                                oclsEmail.sendMail(sMailAddress[i], htmlBody, EmailSubjectText, sFromAddress, true);
                                sSendMailLink.Append("<a target='_blank' href='" + Common.EncryptQuery("DISLog_ReSendCommunication.aspx?communicationid=" + retValue + "&CommunicationLevel=Communication1") + "' onclick='window.open(this.href, this.target,\"scrollbars=1,left=100px,top=100px,width=1000px,height=800px\"); return false;'>" + sMailAddress[i] + "</a>" + System.Environment.NewLine);
                            }
                        }
                    }
                }
            }
            #endregion
        }

        if (CommentBy == "OD - Goods In")
        {
            #region vendor
            for (int i = 0; i < dsLanguage.Tables[0].Rows.Count; i++)
            {
                string Language = dsLanguage.Tables[0].Rows[i]["Language"].ToString();
                int? VendorLanguageId = Convert.ToInt32(dsLanguage.Tables[0].Rows[i]["languageid"]);
                APPBOK_CommunicationBAL oAPPBOK_CommunicationBAL = new APPBOK_CommunicationBAL();

                List<MAS_LanguageBE> oLanguages = oAPPBOK_CommunicationBAL.GetLanguages();

                foreach (MAS_LanguageBE objLanguage in oLanguages)
                {
                    bool MailSentInLanguage = false;

                    if (objLanguage.Language.ToLower() == Language.ToLower())
                    {
                        MailSentInLanguage = true;
                    }

                    #region get file using languageid  

                    string templateFile = null;

                    templateFile = path + templatesPathComments + "/";

                    templateFile += "dis_CommentsbyGoodIn.english.htm";

                    switch (objLanguage.LanguageID)
                    {
                        case 1:
                            Page.UICulture = clsConstants.EnglishISO; //"en-US"; // English
                            break;
                        case 2:
                            Page.UICulture = clsConstants.FranceISO; //"fr"; // France
                            break;
                        case 3:
                            Page.UICulture = clsConstants.GermanyISO; //"de"; // German
                            break;
                        case 4:
                            Page.UICulture = clsConstants.NederlandISO; //"nl"; // Dutch
                            break;
                        case 5:
                            Page.UICulture = clsConstants.SpainISO; //"es"; // Spanish
                            break;
                        case 6:
                            Page.UICulture = clsConstants.ItalyISO; //"it"; // Italy
                            break;
                        case 7:
                            Page.UICulture = clsConstants.CzechISO; //"cz"; // Czech
                            break;
                        default:
                            Page.UICulture = clsConstants.EnglishISO; // "en-US"; // English
                            break;
                    }

                    #endregion                  

                    string sProductGrid = string.Empty;

                    if (System.IO.File.Exists(templateFile.ToLower()))
                    {
                        EmailSubjectText = WebCommon.getGlobalResourceValue("DiscrepancyCommentSubject")
                            .Replace("##VDRNo##", lstDisLog[0].VDRNo);

                        string siteAddress = string.Empty, accountPayable = string.Empty, mailBodyToSaveInDB = string.Empty;

                        htmlBody = oSendCommunicationCommon.getMailBody(iDiscrepancyLogID,
                                   discrepancyType, lstDisLog, templateFile, sProductGrid, sLine1, sDiscrepancyRelatedText,
                                   out siteAddress, out accountPayable, string.Empty);

                        htmlBody = htmlBody.Replace("{DiscrepancyNo}", WebCommon.getGlobalResourceValue("DiscrepancyNo"));
                        htmlBody = htmlBody.Replace("{DearSirMadam}", WebCommon.getGlobalResourceValue("DearSirMadam"));
                        htmlBody = htmlBody.Replace("{Yoursfaithfully}", WebCommon.getGlobalResourceValue("Yoursfaithfully"));

                        htmlBody = htmlBody.Replace("{PortalLink}", "<a href=" + sPortalLink + ">" + sPortalLink + " </a>");
                        htmlBody = htmlBody.Replace("{DiscrepancyComment}", odComment);
                        htmlBody = htmlBody.Replace("{AdditionalComments}", WebCommon.getGlobalResourceValue("AdditionalComments"));
                        htmlBody = htmlBody.Replace("{DiscrepancyCommentsDisplay}", WebCommon.getGlobalResourceValue("DiscrepancyCommentsDisplay"));
                        htmlBody = htmlBody.Replace("{DiscrepancyCommentsBy}", WebCommon.getGlobalResourceValue("DiscrepancyCommentsBy").Replace("##WarehousePlannerVendor##", CommentBy));
                        htmlBody = htmlBody.Replace("{GoodsInContactNo}", lstDisLog[0].GoodsInContactNo);
                        mailBodyToSaveInDB = htmlBody.Replace("{logoInnerPath}", oSendCommunicationCommon.getAbsolutePath());

                        retValue = oSendCommunicationCommon.saveMail(iDiscrepancyLogID, sMailAddress[i], mailBodyToSaveInDB, EmailSubjectText, objLanguage.LanguageID, "discrepancyComment", MailSentInLanguage, 0);

                        if (retValue == null)
                        {
                            oSendCommunicationCommon.errorLog(iDiscrepancyLogID, EmailSubjectText, toAddress, htmlBody, "Error in save mail to database");
                        }
                        if (objLanguage.LanguageID == VendorLanguageId)
                        {
                            clsEmail oclsEmail = new clsEmail();
                            oclsEmail.sendMail(sMailAddress[i], htmlBody, EmailSubjectText, sFromAddress, true);
                            sSendMailLink.Append("<a target='_blank' href='" + Common.EncryptQuery("DISLog_ReSendCommunication.aspx?communicationid=" + retValue + "&CommunicationLevel=Communication1") + "' onclick='window.open(this.href, this.target,\"scrollbars=1,left=100px,top=100px,width=1000px,height=800px\"); return false;'>" + sMailAddress[i] + "</a>" + System.Environment.NewLine);
                        }
                    }
                }
            }
            #endregion

            #region StockPlannerUser
            dsLanguage = oSendCommunicationCommon.getLanguage(lstDisLog[0].StockPlannerUserID);
            if (dsLanguage != null && dsLanguage.Tables.Count > 0 && dsLanguage.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < dsLanguage.Tables[0].Rows.Count; i++)
                {
                    string Language = dsLanguage.Tables[0].Rows[i]["Language"].ToString();

                    int? VendorLanguageId = Convert.ToInt32(dsLanguage.Tables[0].Rows[i]["languageid"]);

                    APPBOK_CommunicationBAL oAPPBOK_CommunicationBAL = new APPBOK_CommunicationBAL();

                    List<MAS_LanguageBE> oLanguages = oAPPBOK_CommunicationBAL.GetLanguages();

                    sMailAddress = null;

                    sMailAddress = new string[] { dsLanguage.Tables[0].Rows[0][0].ToString() };

                    foreach (MAS_LanguageBE objLanguage in oLanguages)
                    {
                        bool MailSentInLanguage = false;

                        if (objLanguage.Language.ToLower() == Language.ToLower())
                        {
                            MailSentInLanguage = true;
                        }

                        #region get file using languageid  

                        string templateFile = null;

                        templateFile = path + templatesPathComments + "/";

                        templateFile += "dis_Comments_SPbyGoodsIn.english.htm";

                        switch (objLanguage.LanguageID)
                        {
                            case 1:
                                Page.UICulture = clsConstants.EnglishISO; //"en-US"; // English
                                break;
                            case 2:
                                Page.UICulture = clsConstants.FranceISO; //"fr"; // France
                                break;
                            case 3:
                                Page.UICulture = clsConstants.GermanyISO; //"de"; // German
                                break;
                            case 4:
                                Page.UICulture = clsConstants.NederlandISO; //"nl"; // Dutch
                                break;
                            case 5:
                                Page.UICulture = clsConstants.SpainISO; //"es"; // Spanish
                                break;
                            case 6:
                                Page.UICulture = clsConstants.ItalyISO; //"it"; // Italy
                                break;
                            case 7:
                                Page.UICulture = clsConstants.CzechISO; //"cz"; // Czech
                                break;
                            default:
                                Page.UICulture = clsConstants.EnglishISO; // "en-US"; // English
                                break;
                        }

                        #endregion


                        string sProductGrid = string.Empty;

                        if (System.IO.File.Exists(templateFile.ToLower()))
                        {
                            EmailSubjectText = WebCommon.getGlobalResourceValue("DiscrepancyCommentSubject").Replace("##VDRNo##", lstDisLog[0].VDRNo);
                            string siteAddress = string.Empty, accountPayable = string.Empty, mailBodyToSaveInDB = string.Empty;
                            htmlBody = oSendCommunicationCommon.getMailBody(iDiscrepancyLogID, discrepancyType, lstDisLog, templateFile, sProductGrid, sLine1, sDiscrepancyRelatedText, out siteAddress, out accountPayable, string.Empty);
                            htmlBody = htmlBody.Replace("{POHash}", WebCommon.getGlobalResourceValue("POHash"));
                            htmlBody = htmlBody.Replace("{DiscrepancyNo}", WebCommon.getGlobalResourceValue("DiscrepancyNo"));
                            htmlBody = htmlBody.Replace("{DearSirMadam}", WebCommon.getGlobalResourceValue("DearSirMadam"));
                            htmlBody = htmlBody.Replace("{Yoursfaithfully}", WebCommon.getGlobalResourceValue("Yoursfaithfully"));
                            htmlBody = htmlBody.Replace("{PortalLink}", "<a href=" + sPortalLink + ">" + sPortalLink + " </a>");
                            htmlBody = htmlBody.Replace("{DiscrepancyComment}", odComment);
                            htmlBody = htmlBody.Replace("{AdditionalComments}", WebCommon.getGlobalResourceValue("AdditionalComments"));
                            htmlBody = htmlBody.Replace("{DiscrepancyCommentsDisplay}", WebCommon.getGlobalResourceValue("DiscrepancyCommentsDisplay"));
                            htmlBody = htmlBody.Replace("{DiscrepancyCommentsBy}", WebCommon.getGlobalResourceValue("DiscrepancyCommentsBy").Replace("##WarehousePlannerVendor##", CommentBy));
                            htmlBody = htmlBody.Replace("{GoodsInContactNo}", lstDisLog[0].GoodsInContactNo);
                            mailBodyToSaveInDB = htmlBody.Replace("{logoInnerPath}", oSendCommunicationCommon.getAbsolutePath());

                            retValue = oSendCommunicationCommon.saveMail(iDiscrepancyLogID, sMailAddress[i], mailBodyToSaveInDB, EmailSubjectText, objLanguage.LanguageID, "discrepancyComment", MailSentInLanguage, 0);

                            if (retValue == null)
                                oSendCommunicationCommon.errorLog(iDiscrepancyLogID,
                                    EmailSubjectText,
                                    toAddress, htmlBody, "Error in save mail to database");

                            if (objLanguage.LanguageID == VendorLanguageId)
                            {
                                clsEmail oclsEmail = new clsEmail();
                                oclsEmail.sendMail(sMailAddress[i], htmlBody, EmailSubjectText, sFromAddress, true);
                                sSendMailLink.Append("<a target='_blank' href='" + Common.EncryptQuery("DISLog_ReSendCommunication.aspx?communicationid=" + retValue + "&CommunicationLevel=Communication1") + "' onclick='window.open(this.href, this.target,\"scrollbars=1,left=100px,top=100px,width=1000px,height=800px\"); return false;'>" + sMailAddress[i] + "</a>" + System.Environment.NewLine);
                            }
                        }
                    }
                }
            }
            #endregion
        }

        if (CommentBy == "OD - Stock Planner")
        {
            #region vendor
            for (int i = 0; i < dsLanguage.Tables[0].Rows.Count; i++)
            {
                string Language = dsLanguage.Tables[0].Rows[i]["Language"].ToString();
                int? VendorLanguageId = Convert.ToInt32(dsLanguage.Tables[0].Rows[i]["languageid"]);
                APPBOK_CommunicationBAL oAPPBOK_CommunicationBAL = new APPBOK_CommunicationBAL();
                List<MAS_LanguageBE> oLanguages = oAPPBOK_CommunicationBAL.GetLanguages();

                foreach (MAS_LanguageBE objLanguage in oLanguages)
                {
                    bool MailSentInLanguage = false;

                    if (objLanguage.Language.ToLower() == Language.ToLower())
                    {
                        MailSentInLanguage = true;
                    }

                    #region get file using languageid  

                    string templateFile = null;

                    templateFile = path + templatesPathComments + "/";

                    templateFile += "dis_Comments.english.htm";

                    switch (objLanguage.LanguageID)
                    {
                        case 1:
                            Page.UICulture = clsConstants.EnglishISO; //"en-US"; // English
                            break;
                        case 2:
                            Page.UICulture = clsConstants.FranceISO; //"fr"; // France
                            break;
                        case 3:
                            Page.UICulture = clsConstants.GermanyISO; //"de"; // German
                            break;
                        case 4:
                            Page.UICulture = clsConstants.NederlandISO; //"nl"; // Dutch
                            break;
                        case 5:
                            Page.UICulture = clsConstants.SpainISO; //"es"; // Spanish
                            break;
                        case 6:
                            Page.UICulture = clsConstants.ItalyISO; //"it"; // Italy
                            break;
                        case 7:
                            Page.UICulture = clsConstants.CzechISO; //"cz"; // Czech
                            break;
                        default:
                            Page.UICulture = clsConstants.EnglishISO; // "en-US"; // English
                            break;
                    }

                    #endregion                  

                    string sProductGrid = string.Empty;

                    if (System.IO.File.Exists(templateFile.ToLower()))
                    {
                        EmailSubjectText = WebCommon.getGlobalResourceValue("DiscrepancyCommentSubject")
                            .Replace("##VDRNo##", lstDisLog[0].VDRNo);

                        string siteAddress = string.Empty, accountPayable = string.Empty, mailBodyToSaveInDB = string.Empty;

                        htmlBody = oSendCommunicationCommon.getMailBody(iDiscrepancyLogID,
                                   discrepancyType, lstDisLog, templateFile, sProductGrid, sLine1, sDiscrepancyRelatedText,
                                   out siteAddress, out accountPayable, string.Empty);

                        htmlBody = htmlBody.Replace("{DiscrepancyNo}", WebCommon.getGlobalResourceValue("DiscrepancyNo"));
                        htmlBody = htmlBody.Replace("{DearSirMadam}", WebCommon.getGlobalResourceValue("DearSirMadam"));
                        htmlBody = htmlBody.Replace("{Yoursfaithfully}", WebCommon.getGlobalResourceValue("Yoursfaithfully"));

                        htmlBody = htmlBody.Replace("{PortalLink}", "<a href=" + sPortalLink + ">" + sPortalLink + " </a>");
                        htmlBody = htmlBody.Replace("{DiscrepancyComment}", odComment);
                        htmlBody = htmlBody.Replace("{AdditionalComments}", WebCommon.getGlobalResourceValue("AdditionalComments"));
                        htmlBody = htmlBody.Replace("{DiscrepancyCommentsDisplay}", WebCommon.getGlobalResourceValue("DiscrepancyCommentsDisplay"));
                        htmlBody = htmlBody.Replace("{DiscrepancyCommentsBy}", WebCommon.getGlobalResourceValue("DiscrepancyCommentsBy").Replace("##WarehousePlannerVendor##", CommentBy));

                        mailBodyToSaveInDB = htmlBody.Replace("{logoInnerPath}", oSendCommunicationCommon.getAbsolutePath());

                        retValue = oSendCommunicationCommon.saveMail(iDiscrepancyLogID, sMailAddress[i], mailBodyToSaveInDB, EmailSubjectText, objLanguage.LanguageID, "discrepancyComment", MailSentInLanguage, 0);

                        if (retValue == null)
                        {
                            oSendCommunicationCommon.errorLog(iDiscrepancyLogID, EmailSubjectText, toAddress, htmlBody, "Error in save mail to database");
                        }
                        if (objLanguage.LanguageID == VendorLanguageId)
                        {
                            clsEmail oclsEmail = new clsEmail();
                            oclsEmail.sendMail(sMailAddress[i], htmlBody, EmailSubjectText, sFromAddress, true);
                            sSendMailLink.Append("<a target='_blank' href='" + Common.EncryptQuery("DISLog_ReSendCommunication.aspx?communicationid=" + retValue + "&CommunicationLevel=Communication1") + "' onclick='window.open(this.href, this.target,\"scrollbars=1,left=100px,top=100px,width=1000px,height=800px\"); return false;'>" + sMailAddress[i] + "</a>" + System.Environment.NewLine);
                        }
                    }
                }
            }
            #endregion

            #region GoodsIn
            dsLanguage = oSendCommunicationCommon.getLanguage(Convert.ToInt32(lstDisLog[0].CreatedById));

            if (dsLanguage != null && dsLanguage.Tables.Count > 0 && dsLanguage.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < dsLanguage.Tables[0].Rows.Count; i++)
                {
                    string Language = dsLanguage.Tables[0].Rows[i]["Language"].ToString();

                    int? VendorLanguageId = Convert.ToInt32(dsLanguage.Tables[0].Rows[i]["languageid"]);

                    APPBOK_CommunicationBAL oAPPBOK_CommunicationBAL = new APPBOK_CommunicationBAL();
                    List<MAS_LanguageBE> oLanguages = oAPPBOK_CommunicationBAL.GetLanguages();

                    sMailAddress = null;

                    sMailAddress = new string[] { dsLanguage.Tables[0].Rows[0][0].ToString() };

                    foreach (MAS_LanguageBE objLanguage in oLanguages)
                    {
                        bool MailSentInLanguage = false;

                        if (objLanguage.Language.ToLower() == Language.ToLower())
                        {
                            MailSentInLanguage = true;
                        }

                        #region get file using languageid  

                        string templateFile = null;

                        templateFile = path + templatesPathComments + "/";

                        templateFile += "dis_Comments_GoodsIn_StockPlanner.english.htm";

                        switch (objLanguage.LanguageID)
                        {
                            case 1:
                                Page.UICulture = clsConstants.EnglishISO; //"en-US"; // English
                                break;
                            case 2:
                                Page.UICulture = clsConstants.FranceISO; //"fr"; // France
                                break;
                            case 3:
                                Page.UICulture = clsConstants.GermanyISO; //"de"; // German
                                break;
                            case 4:
                                Page.UICulture = clsConstants.NederlandISO; //"nl"; // Dutch
                                break;
                            case 5:
                                Page.UICulture = clsConstants.SpainISO; //"es"; // Spanish
                                break;
                            case 6:
                                Page.UICulture = clsConstants.ItalyISO; //"it"; // Italy
                                break;
                            case 7:
                                Page.UICulture = clsConstants.CzechISO; //"cz"; // Czech
                                break;
                            default:
                                Page.UICulture = clsConstants.EnglishISO; // "en-US"; // English
                                break;
                        }

                        #endregion 

                        string sProductGrid = string.Empty;

                        if (System.IO.File.Exists(templateFile.ToLower()))
                        {
                            EmailSubjectText = WebCommon.getGlobalResourceValue("DiscrepancyCommentSubject").Replace("##VDRNo##", lstDisLog[0].VDRNo);

                            string siteAddress = string.Empty, accountPayable = string.Empty, mailBodyToSaveInDB = string.Empty;

                            htmlBody = oSendCommunicationCommon.getMailBody(iDiscrepancyLogID,
                                       discrepancyType, lstDisLog, templateFile, sProductGrid,
                                       sLine1, sDiscrepancyRelatedText,
                                       out siteAddress, out accountPayable, string.Empty);

                            htmlBody = htmlBody.Replace("{POHash}", WebCommon.getGlobalResourceValue("POHash"));
                            htmlBody = htmlBody.Replace("{DiscrepancyNo}", WebCommon.getGlobalResourceValue("DiscrepancyNo"));
                            htmlBody = htmlBody.Replace("{DearSirMadam}", WebCommon.getGlobalResourceValue("DearSirMadam"));
                            htmlBody = htmlBody.Replace("{Yoursfaithfully}", WebCommon.getGlobalResourceValue("Yoursfaithfully"));

                            htmlBody = htmlBody.Replace("{PortalLink}", "<a href=" + sPortalLink + ">" + sPortalLink + " </a>");
                            htmlBody = htmlBody.Replace("{DiscrepancyComment}", odComment);
                            htmlBody = htmlBody.Replace("{AdditionalComments}", WebCommon.getGlobalResourceValue("AdditionalComments"));
                            htmlBody = htmlBody.Replace("{DiscrepancyCommentsDisplay}", WebCommon.getGlobalResourceValue("DiscrepancyCommentsDisplay"));
                            htmlBody = htmlBody.Replace("{DiscrepancyCommentsBy}", WebCommon.getGlobalResourceValue("DiscrepancyCommentsBy").Replace("##WarehousePlannerVendor##", CommentBy));

                            mailBodyToSaveInDB = htmlBody.Replace("{logoInnerPath}", oSendCommunicationCommon.getAbsolutePath());

                            retValue = oSendCommunicationCommon.saveMail(iDiscrepancyLogID,
                            sMailAddress[i], mailBodyToSaveInDB, EmailSubjectText, objLanguage.LanguageID, "discrepancyComment", MailSentInLanguage, 0);

                            if (retValue == null)
                            {
                                oSendCommunicationCommon.errorLog(iDiscrepancyLogID, EmailSubjectText, toAddress, htmlBody, "Error in save mail to database");
                            }

                            if (objLanguage.LanguageID == VendorLanguageId)
                            {
                                clsEmail oclsEmail = new clsEmail();
                                oclsEmail.sendMail(sMailAddress[i], htmlBody, EmailSubjectText, sFromAddress, true);
                                sSendMailLink.Append("<a target='_blank' href='" + Common.EncryptQuery("DISLog_ReSendCommunication.aspx?communicationid=" + retValue + "&CommunicationLevel=Communication1") + "' onclick='window.open(this.href, this.target,\"scrollbars=1,left=100px,top=100px,width=1000px,height=800px\"); return false;'>" + sMailAddress[i] + "</a>" + System.Environment.NewLine);
                            }
                        }
                    }
                }
            }
            #endregion         
        }

        //setting default Resourse file
        Page.UICulture = Convert.ToString(Session["CultureInfo"]);
        return retValue;
    }
}

// This is for Type 1
public class sendCommunicationCommon : System.Web.UI.Page
{

    public string templatesPath = "emailtemplates";
    public string sPortalLink = Convert.ToString(ConfigurationManager.AppSettings["Portallink"]) + @"/ModuleUI/Security/Login.aspx";
    public string sFromAddress = Convert.ToString(ConfigurationManager.AppSettings["fromAddress"]);
    string path = AppDomain.CurrentDomain.BaseDirectory.ToLower();

    public DataSet getLanguage(int? vendorID, string sSentTo)
    {
        DiscrepancyMailBE oDiscrepancyMailBE = new DiscrepancyMailBE();
        DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();
        oDiscrepancyMailBE.Action = "GetLanguageIDForCommunication";
        oDiscrepancyMailBE.vendorID = vendorID;
        oDiscrepancyMailBE.sentTo = sSentTo;
        DataSet dsDisLanguageID = oDiscrepancyBAL.getLanguageIDforCommunicationBAL(oDiscrepancyMailBE);
        return dsDisLanguageID;
    }

    public DataSet getLanguage(int? UserID)
    {
        DiscrepancyMailBE oDiscrepancyMailBE = new DiscrepancyMailBE();
        DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();
        oDiscrepancyMailBE.Action = "GetLanguageIDForCommunicationForUsers";
        oDiscrepancyMailBE.UserID = UserID;
        DataSet dsDisLanguageID = oDiscrepancyBAL.getLanguageIDforCommunicationBAL(oDiscrepancyMailBE);
        return dsDisLanguageID;
    }

    public List<string> getLanguageByVendorID(int VendorID)
    {
        List<string> lstLanguage = new List<string>();
        UP_VendorBE oUP_VendorBE = new UP_VendorBE();
        UP_VendorBAL oUP_VendorBAL = new UP_VendorBAL();
        List<UP_VendorBE> lstDetails = new List<UP_VendorBE>();
        oUP_VendorBE.VendorID = VendorID;
        oUP_VendorBE.Action = "ShowVendorByID";
        lstDetails = oUP_VendorBAL.GetVendorByIdBAL(oUP_VendorBE);

        foreach (UP_VendorBE detail in lstDetails)
        {
            lstLanguage.Add(detail.Language);
        }
        return lstLanguage;
    }

    //External IP is not able to pick correct path .so we hardcord logopath.
    public string getAbsolutePath()
    {
        string LogoPath = Convert.ToString(ConfigurationManager.AppSettings["Portallink"]);
        string absolutePath = LogoPath + "/Images/OfficeDepotLogo.jpg";
        return absolutePath;
    }

    public string convertDateTime(object dateTime)
    {
        if (dateTime != null)
        {
            try
            {
                DateTime dt = Convert.ToDateTime(dateTime);
                return dt.ToString("dd/MM/yyyy");
            }
            catch
            { return ""; }
        }
        else
            return "";
    }
    public void showErrorMessage()
    {
        string errorMessage = WebCommon.getGlobalResourceValue("BlankEmailAddress");
        if (string.IsNullOrEmpty(errorMessage))
            errorMessage = "Please enter email address";
        ScriptManager.RegisterClientScriptBlock((Page)HttpContext.Current.CurrentHandler, this.GetType(), "Message", "alert('" + errorMessage + "')", true);

        return;
    }
    public string getLanguageAndTemplateFile(string discrepancyType, int? vendorID, out int? languageID, string sCommunicationType, string sentAddress)
    {
        DataSet dsLanguage = getLanguage(vendorID, sentAddress);
        languageID = null;

        string templateFile = null;
        templateFile = path + templatesPath + "/" + sCommunicationType;

        if (dsLanguage != null && dsLanguage.Tables.Count > 0 && dsLanguage.Tables[0].Rows.Count > 0)
        {
            templateFile += ".english.htm";
        }
        return templateFile;
    }
    public List<DiscrepancyBE> getDiscrepancyLogDetail(string Action, int iDiscrepancyLogID, string discrepancyType)
    {
        // GET DiscrepancyLogDetails
        DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
        DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();
        oDiscrepancyBE.Action = Action;
        oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(iDiscrepancyLogID);
        oDiscrepancyBE.DiscrepancyType = discrepancyType;
        return oDiscrepancyBAL.GetDiscrepancyLogDetailsBAL(oDiscrepancyBE);
    }
    public List<DiscrepancyBE> getDiscrepancyItemDetail(string Action, int iDiscrepancyLogID, string discrepancyType)
    {
        // GET DiscrepancyLogItemDetails
        DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
        DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();
        oDiscrepancyBE.Action = Action;
        oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(iDiscrepancyLogID);
        oDiscrepancyBE.DiscrepancyType = discrepancyType;
        return oDiscrepancyBAL.GetDiscrepancyItemDetailsBAL(oDiscrepancyBE);
    }

    public DataTable GetUserPhoneNumber(string Action, int UserID)
    {
        // GET DiscrepancyLogItemDetails
        DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
        DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();
        oDiscrepancyBE.Action = Action;
        oDiscrepancyBE.UserID = Convert.ToInt32(UserID);
        return oDiscrepancyBAL.GetUserPhoneNumberBAL(oDiscrepancyBE);
    }

    public List<DISLog_WorkFlowBE> getDiscrepancyWorkFlowActionDetail(string Action, int iDiscrepancyLogID)
    {
        DISLog_WorkFlowBE oDISLog_WorkFlowBE = new DISLog_WorkFlowBE();
        DISLog_WorkFlowBAL oDISLog_WorkFlowBAL = new DISLog_WorkFlowBAL();
        oDISLog_WorkFlowBE.Action = "GetWorkFlowActionDetails";
        oDISLog_WorkFlowBE.DiscrepancyLogID = iDiscrepancyLogID;
        List<DISLog_WorkFlowBE> lstWorkFlowActionDetails = oDISLog_WorkFlowBAL.GetWorkFlowActionsBAL(oDISLog_WorkFlowBE);
        return lstWorkFlowActionDetails;
    }
    public DataSet getDiscrepancyCommunicationDetail(string Action, int iDiscrepancyLogID, string sCurrentCommunicationLevel)
    {
        DiscrepancyMailBE oDiscrepancyMailBE = new DiscrepancyMailBE();
        DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();
        oDiscrepancyMailBE.Action = Action;
        oDiscrepancyMailBE.discrepancyLogID = Convert.ToInt32(iDiscrepancyLogID);
        oDiscrepancyMailBE.communicationLevel = sCurrentCommunicationLevel;
        return oDiscrepancyBAL.GetDiscrepancyDetailBAL(oDiscrepancyMailBE);
    }
    public void getCommunicationDate(DataSet dsCommuniation, out DateTime? dtCommunication1SentDate, out DateTime? dtCommunication2SentDate, out DateTime? dtCommunication3SentDate, out DateTime? dtCommunication4SentDate)
    {
        dtCommunication1SentDate = dtCommunication2SentDate = dtCommunication3SentDate = dtCommunication4SentDate = null;
        if (dsCommuniation != null && dsCommuniation.Tables.Count > 0 && dsCommuniation.Tables[0].Rows.Count > 0)
        {
            for (int i = 0; i < dsCommuniation.Tables[0].Rows.Count; i++)
            {
                if (Convert.ToString(dsCommuniation.Tables[0].Rows[i]["CommunicationLevel"]).ToLower() == "communication1")
                    dtCommunication1SentDate = Convert.ToDateTime(dsCommuniation.Tables[0].Rows[i]["sentdate"]);
                else if (Convert.ToString(dsCommuniation.Tables[0].Rows[i]["CommunicationLevel"]).ToLower() == "communication2")
                    dtCommunication2SentDate = Convert.ToDateTime(dsCommuniation.Tables[0].Rows[i]["sentdate"]);
                else if (Convert.ToString(dsCommuniation.Tables[0].Rows[i]["CommunicationLevel"]).ToLower() == "communication3")
                    dtCommunication3SentDate = Convert.ToDateTime(dsCommuniation.Tables[0].Rows[i]["sentdate"]);
                else if (Convert.ToString(dsCommuniation.Tables[0].Rows[i]["CommunicationLevel"]).ToLower() == "communication4")
                    dtCommunication4SentDate = Convert.ToDateTime(dsCommuniation.Tables[0].Rows[i]["sentdate"]);
            }
        }
    }
    public List<DiscrepancyBE> getDiscrepancyLogSiteVendorDetail(int iDiscrepancyLogID, string discrepancyType, out string siteAddress, out string accountPayable, string sMailOrLetter)
    {
        DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
        DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();
        oDiscrepancyBE.Action = "GetDiscrepancySiteVendorDetail";
        oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(iDiscrepancyLogID);
        oDiscrepancyBE.DiscrepancyType = discrepancyType;
        List<DiscrepancyBE> lstDisLogSiteVendor = oDiscrepancyBAL.GetDiscrepancyLogSiteVendorDetailsBAL(oDiscrepancyBE);

        siteAddress = accountPayable = string.Empty;
        //   if (!string.IsNullOrEmpty(lstDisLogSiteVendor[0].Site.SiteName) && sMailOrLetter.ToLower() != "l") { siteAddress = Convert.ToString(lstDisLogSiteVendor[0].Site.SiteName); }
        if (!string.IsNullOrEmpty(lstDisLogSiteVendor[0].Site.SiteAddressLine1)) { siteAddress = Convert.ToString(lstDisLogSiteVendor[0].Site.SiteAddressLine1); }
        if (!string.IsNullOrEmpty(lstDisLogSiteVendor[0].Site.SiteAddressLine2)) { siteAddress = siteAddress + ", " + Convert.ToString(lstDisLogSiteVendor[0].Site.SiteAddressLine2); }
        if (!string.IsNullOrEmpty(lstDisLogSiteVendor[0].Site.SiteAddressLine3)) { siteAddress = siteAddress + ", " + Convert.ToString(lstDisLogSiteVendor[0].Site.SiteAddressLine3); }
        if (!string.IsNullOrEmpty(lstDisLogSiteVendor[0].Site.SiteAddressLine4)) { siteAddress = siteAddress + ", " + Convert.ToString(lstDisLogSiteVendor[0].Site.SiteAddressLine4); }
        if (!string.IsNullOrEmpty(lstDisLogSiteVendor[0].Site.SiteAddressLine5)) { siteAddress = siteAddress + ", " + Convert.ToString(lstDisLogSiteVendor[0].Site.SiteAddressLine5); }
        if (!string.IsNullOrEmpty(lstDisLogSiteVendor[0].Site.SiteAddressLine6)) { siteAddress = siteAddress + ", " + Convert.ToString(lstDisLogSiteVendor[0].Site.SiteAddressLine6); }
        if (!string.IsNullOrEmpty(lstDisLogSiteVendor[0].Site.SitePincode)) { siteAddress = siteAddress + ", " + Convert.ToString(lstDisLogSiteVendor[0].Site.SitePincode); }

        if (!string.IsNullOrEmpty(lstDisLogSiteVendor[0].Site.APAddressLine1)) { accountPayable = Convert.ToString(lstDisLogSiteVendor[0].Site.APAddressLine1); }
        if (!string.IsNullOrEmpty(lstDisLogSiteVendor[0].Site.APAddressLine2)) { accountPayable = accountPayable + ", " + Convert.ToString(lstDisLogSiteVendor[0].Site.APAddressLine2); }
        if (!string.IsNullOrEmpty(lstDisLogSiteVendor[0].Site.APAddressLine3)) { accountPayable = accountPayable + ", " + Convert.ToString(lstDisLogSiteVendor[0].Site.APAddressLine3); }
        if (!string.IsNullOrEmpty(lstDisLogSiteVendor[0].Site.APAddressLine4)) { accountPayable = accountPayable + ", " + Convert.ToString(lstDisLogSiteVendor[0].Site.APAddressLine4); }
        if (!string.IsNullOrEmpty(lstDisLogSiteVendor[0].Site.APAddressLine5)) { accountPayable = accountPayable + ", " + Convert.ToString(lstDisLogSiteVendor[0].Site.APAddressLine5); }
        if (!string.IsNullOrEmpty(lstDisLogSiteVendor[0].Site.APAddressLine6)) { accountPayable = accountPayable + ", " + Convert.ToString(lstDisLogSiteVendor[0].Site.APAddressLine6); }
        if (!string.IsNullOrEmpty(lstDisLogSiteVendor[0].Site.APPincode)) { accountPayable = accountPayable + ", " + Convert.ToString(lstDisLogSiteVendor[0].Site.APPincode); }

        return lstDisLogSiteVendor;
    }

    public string getMailBody(int iDiscrepancyLogID, string discrepancyType, List<DiscrepancyBE> lstDisLog, string templateFile,
        string sProductGrid, string sLine1, string sDiscrepancyRelatedText, out string siteAddress, out string accountPayable, string InvAction = "",
        bool IsFromVendor = false)
    {
        string htmlBody = null;
        using (StreamReader sReader = new StreamReader(templateFile.ToLower()))
        {
            List<DiscrepancyBE> lstDisLogSiteVendor = getDiscrepancyLogSiteVendorDetail(iDiscrepancyLogID, discrepancyType, out siteAddress, out accountPayable, "M");
            htmlBody = sReader.ReadToEnd();

            htmlBody = htmlBody.Replace("{DearSir/Madam}", WebCommon.getGlobalResourceValue("DearSirMadam"));
            htmlBody = htmlBody.Replace("{OurPuchaseOrderNumber}", WebCommon.getGlobalResourceValue("OurPuchaseOrderNumber"));
            htmlBody = htmlBody.Replace("{Yoursfaithfully}", WebCommon.getGlobalResourceValue("Yoursfaithfully"));

            htmlBody = htmlBody.Replace("{Inventory Manager}", WebCommon.getGlobalResourceValue("InventoryManager"));

            htmlBody = htmlBody.Replace("{GoodsInManager}", WebCommon.getGlobalResourceValue("GoodsInManager"));

            htmlBody = lstDisLog[0].VDRNo.ToCharArray()[0].ToString() == "U"
                ? htmlBody.Replace("{AccountsPayable}", WebCommon.getGlobalResourceValue("StockPlanner"))
                : htmlBody.Replace("{AccountsPayable}", WebCommon.getGlobalResourceValue("AccountsPayable"));


            htmlBody = htmlBody.Replace("{YourDeliveryNoteNumber}", WebCommon.getGlobalResourceValue("YourDeliveryNoteNumber"));
            htmlBody = htmlBody.Replace("{ReferenceText}", WebCommon.getGlobalResourceValue("ReferenceText"));
            htmlBody = htmlBody.Replace("{SUPPLIERDISCREPANCYREPORT}", WebCommon.getGlobalResourceValue("SUPPLIERDISCREPANCYREPORT"));
            htmlBody = htmlBody.Replace("{VENDORDISCREPANCYREPORT}", WebCommon.getGlobalResourceValue("VENDORDISCREPANCYREPORT"));
            htmlBody = htmlBody.Replace("{VDRNOText}", WebCommon.getGlobalResourceValue("VDRNOText"));
            htmlBody = htmlBody.Replace("{DateofDelivery}", WebCommon.getGlobalResourceValue("DateofDelivery"));
            htmlBody = htmlBody.Replace("{Date}", WebCommon.getGlobalResourceValue("Date"));
            htmlBody = htmlBody.Replace("{Date}", WebCommon.getGlobalResourceValue("Date"));


            htmlBody = htmlBody.Replace("{SiteAddress}", siteAddress);
            htmlBody = htmlBody.Replace("{CommunicationDate}", System.DateTime.Now.ToString("dd/MM/yyyy"));
            htmlBody = htmlBody.Replace("{VendorCode}", lstDisLogSiteVendor[0].Vendor.Vendor_No);
            htmlBody = htmlBody.Replace("{VendorName}", lstDisLogSiteVendor[0].Vendor.VendorName);
            htmlBody = htmlBody.Replace("{VendorAddress1}", lstDisLogSiteVendor[0].Vendor.address1);
            htmlBody = htmlBody.Replace("{VDRNo}", lstDisLog[0].VDRNo);
            htmlBody = htmlBody.Replace("{VendorAddress2}", lstDisLogSiteVendor[0].Vendor.address2);
            htmlBody = htmlBody.Replace("{VMPPIN}", lstDisLogSiteVendor[0].Vendor.VMPPIN);
            htmlBody = htmlBody.Replace("{VMPPOU}", lstDisLogSiteVendor[0].Vendor.VMPPOU);
            htmlBody = htmlBody.Replace("{VendorCity}", lstDisLogSiteVendor[0].Vendor.city);

            //InternalComments
            if (htmlBody.Contains("{InternalComments}"))
            {
                htmlBody = htmlBody.Replace("{InternalComments}", lstDisLog[0].InternalComments);
            }
            //ActionComments
            if (htmlBody.Contains("{ActionComments}"))
            {
                htmlBody = htmlBody.Replace("{ActionComments}", "<b>Action Comments: </b>" + lstDisLog[0].ActionComments);
            }

            //Decision
            if (htmlBody.Contains("{Decision}"))
            {
                htmlBody = htmlBody.Replace("{Decision}", "<b>Decision: </b>" + lstDisLog[0].Decision);
            }

            string InvActionText = string.Empty;
            InvActionText = InvAction.ToString() == "Reduce Purchase Order"
                ? WebCommon.getGlobalResourceValue("InvActionReducePO")
                : WebCommon.getGlobalResourceValue("InvActionResentByVendor");

            if (discrepancyType.ToLower() == "nopurchaseordernumber")
                htmlBody = htmlBody.Replace("{PurchaseOrderNumber}", "Not quoted on paperwork");

            else if (discrepancyType.ToLower() == "generic")
            {
                htmlBody = !string.IsNullOrEmpty(lstDisLog[0].PurchaseOrderNumber)
                    ? htmlBody.Replace("{PurchaseOrderNumber}", lstDisLog[0].PurchaseOrderNumber)
                    : htmlBody.Replace("{PurchaseOrderNumber}", string.Empty);
            }
            else
            {
                htmlBody = htmlBody.Replace("{PurchaseOrderNumber}", lstDisLog[0].PurchaseOrderNumber);
            }


            htmlBody = discrepancyType.ToLower() == "presentationissue"
                ? htmlBody.Replace("{CarrierRow}", "<tr><td style='font-family:Arial;font-size:12' colspan='2'><br /><font style='font-weight:bold'>Carrier Name:</font>&nbsp;" + lstDisLog[0].CarrierName + "</td></tr>")
                : htmlBody.Replace("{CarrierRow}", "");

            htmlBody = discrepancyType.ToLower() == "qtydifferenceshortage"
                ? htmlBody.Replace(" {InventoryAction}", "<tr><td style='font-family:Arial;font-size:12' colspan='2'><br />" + InvActionText + "</td></tr>")
                : htmlBody.Replace(" {InventoryAction}", "");


            htmlBody = htmlBody.Replace("{DeliveryNoteNumber}", lstDisLog[0].DeliveryNoteNumber);

            htmlBody = discrepancyType.ToLower() == "prematureinvoicereceipt" || discrepancyType.ToLower() == "prematureinvoice" || discrepancyType.ToLower() == "invoice"
                ? htmlBody.Replace("{DeliveryNoteNumberText}", WebCommon.getGlobalResourceValue("InvoiceNumberText"))
                : htmlBody.Replace("{DeliveryNoteNumberText}", WebCommon.getGlobalResourceValue("DeliveryNoteNumberText"));

            htmlBody = htmlBody.Replace("{DeliveryDate}", lstDisLog[0].DeliveryArrivedDate == null ? "" : convertDateTime(lstDisLog[0].DeliveryArrivedDate.Value));


            if (discrepancyType.ToLower() == "failpalletspecification")
            {
                sDiscrepancyRelatedText = sDiscrepancyRelatedText.Replace("##FailPalletSpecificationComment##", lstDisLog[0].FailPalletSpecificationComment);
            }

            htmlBody = htmlBody.Replace("{DiscrepancyRelatedText}", sDiscrepancyRelatedText);

            htmlBody = !string.IsNullOrEmpty(sProductGrid)
                ? htmlBody.Replace("{ProductGrid}", "<tr><td style='font-family:Arial;font-size:12' colspan='2'><br />" + sProductGrid + "</td></tr>")
                : htmlBody.Replace("{ProductGrid}", "");

            htmlBody = htmlBody.Replace("{Line1}", sLine1);

            if (discrepancyType.ToLower() == "generic")
            {
                htmlBody = htmlBody.Replace("{InventoryManagerName}", lstDisLog[0].OfficeDepotContactName);
                string removeInventoryManagerRow = "<tr><td style=\"font-weight:bold;font-family:Arial;font-size:12\" colspan=\"2\">{InventoryManager}</td></tr>";
                htmlBody = htmlBody.Replace(removeInventoryManagerRow, string.Empty);
                htmlBody = !string.IsNullOrWhiteSpace(lstDisLog[0].OfficeDepotContactPhone)
                    ? htmlBody.Replace("{InventoryManagerNumber}", lstDisLog[0].OfficeDepotContactPhone)
                    : htmlBody.Replace("({InventoryManagerNumber})", string.Empty);

            }
            else if (discrepancyType.ToLower() == "invoice")
            {
                if (lstDisLogSiteVendor[0].VDRNo.ToCharArray()[0].ToString() == "U")
                {
                    htmlBody = htmlBody.Replace("{AccountsPayableName}", lstDisLogSiteVendor[0].StockPlannerName);
                    htmlBody = !string.IsNullOrWhiteSpace(lstDisLogSiteVendor[0].StockPlannerContact)
                        ? htmlBody.Replace("{AccountsPayableNumber}", lstDisLogSiteVendor[0].StockPlannerContact)
                        : htmlBody.Replace("({AccountsPayableNumber})", string.Empty);

                }
                else
                {
                    htmlBody = htmlBody.Replace("{AccountsPayableName}", lstDisLogSiteVendor[0].AccountPaybleName);

                    htmlBody = !string.IsNullOrWhiteSpace(lstDisLogSiteVendor[0].AccountPayblePhoneNo)
                        ? htmlBody.Replace("{AccountsPayableNumber}", lstDisLogSiteVendor[0].AccountPayblePhoneNo)
                        : htmlBody.Replace("({AccountsPayableNumber})", string.Empty);
                }
            }
            else
            {
                htmlBody = htmlBody.Replace("{InventoryManagerName}", lstDisLog[0].StockPlannerName);

                htmlBody = htmlBody.Replace("{GoodsInManagerName}", lstDisLog[0].GoodsIn);

                if (!string.IsNullOrWhiteSpace(lstDisLog[0].StockPlannerContact))
                {
                    htmlBody = htmlBody.Replace("{InventoryManagerNumber}", lstDisLog[0].StockPlannerContact);
                }
                else
                {
                    htmlBody = htmlBody.Replace("({InventoryManagerNumber})", string.Empty);
                    htmlBody = htmlBody.Replace("{InventoryManagerNumber}", string.Empty);
                }
            }

            /* 
                Date : 02/June/2014
                [Stage 10 Requirement] : Here again changing the [Inventory Manager Title] if that is exist in database then 
                that will show in email otherwise previous functionality will work as earlier.
             */
            // Here in place of {InventoryManager} Stock Planner text being set as per client requirement, Excel tab no 208.            
            string strInventoryMgr = WebCommon.getGlobalResourceValue("StockPlanner");

            htmlBody = !string.IsNullOrEmpty(lstDisLog[0].JobTitle)
                ? htmlBody.Replace("{InventoryManager}", lstDisLog[0].JobTitle)
                : !string.IsNullOrWhiteSpace(strInventoryMgr)
                    ? htmlBody.Replace("{InventoryManager}", strInventoryMgr)
                    : htmlBody.Replace("{InventoryManager}", string.Empty);

            htmlBody = lstDisLog[0].VDRNo.ToCharArray()[0].ToString() == "U"
                ? htmlBody.Replace("{AccountPayable}", strInventoryMgr)
                : htmlBody.Replace("{AccountPayable}", accountPayable);

            htmlBody = htmlBody.Replace("{Hi}", WebCommon.getGlobalResourceValue("Hi"));
            htmlBody = htmlBody.Replace("{ShuttleDiscMessage}", WebCommon.getGlobalResourceValue("ShuttleDiscMessage"));
            htmlBody = htmlBody.Replace("{TypeText}", WebCommon.getGlobalResourceValue("Type"));
            htmlBody = htmlBody.Replace("{DearSirMadam}", WebCommon.getGlobalResourceValue("DearSirMadam"));
            htmlBody = htmlBody.Replace("{ODUsersName}", Convert.ToString(Session["UserName"]));
            htmlBody = htmlBody.Replace("{ODUsersPhone}", Convert.ToString(Session["UserPhoneNo"]));

            if (discrepancyType.ToLower().Equals("deleteddiscrepancy") || discrepancyType.ToLower().Equals("deleteddiscrepancy"))
            {
                htmlBody = htmlBody.Replace("{DiscrepancyHash}", WebCommon.getGlobalResourceValue("DiscrepancyNo"));
                htmlBody = htmlBody.Replace("{DiscrepancyHashValue}", Convert.ToString(lstDisLog[0].VDRNo));
                htmlBody = htmlBody.Replace("{OurPurchaseOrderHash}", WebCommon.getGlobalResourceValue("OurPurchaseOrderHash"));
                htmlBody = htmlBody.Replace("{OurPurchaseOrderHashValue}", Convert.ToString(lstDisLog[0].PurchaseOrderNumber));

                htmlBody = htmlBody.Replace("{DiscrepancyType}", WebCommon.getGlobalResourceValue("DiscrepancyType"));
                htmlBody = htmlBody.Replace("{DiscrepancyTypeValue}", Convert.ToString(lstDisLog[0].ProductDescription));
                htmlBody = htmlBody.Replace("{DeliveryDateKey}", WebCommon.getGlobalResourceValue("DeliveryDate"));
                DateTime dtmDiscrepancyLogDate = Convert.ToDateTime(lstDisLog[0].DiscrepancyLogDate);
                htmlBody = htmlBody.Replace("{DeliveryDateValue}", dtmDiscrepancyLogDate.ToString("dd/MM/yyyy"));

                htmlBody = htmlBody.Replace("{DeletedDiscrepancyMessage1}", WebCommon.getGlobalResourceValue("DeletedDiscrepancyMessage1"));
                htmlBody = htmlBody.Replace("{DeletedDiscrepancyMessage2}", WebCommon.getGlobalResourceValue("DeletedDiscrepancyMessage2"));

                htmlBody = htmlBody.Replace("{OurPurchaseOrderHash}", WebCommon.getGlobalResourceValue("PONumber"));
                htmlBody = htmlBody.Replace("{DeliveryNoteHash}", WebCommon.getGlobalResourceValue("DeliveryNoteNew"));
                htmlBody = htmlBody.Replace("{VDRNumberHash}", WebCommon.getGlobalResourceValue("VDRNumber"));
                htmlBody = htmlBody.Replace("{DeletedDiscrepancyMessageNew}", WebCommon.getGlobalResourceValue("DeletedDiscrepancyContent"));
                htmlBody = htmlBody.Replace("{ManyThanksNew}", WebCommon.getGlobalResourceValue("ManyThanksNew"));
                htmlBody = htmlBody.Replace("{DeliveryNoteHashValue}", lstDisLog[0].DeliveryNoteNumber);
            }

            if (discrepancyType.ToLower() == "invoice query")
            {
                DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();
                DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
                oDiscrepancyBE.Action = "GetInvoicequeryGINCOMMENT";
                oDiscrepancyBE.DiscrepancyLogID = iDiscrepancyLogID;
                oDiscrepancyBE.UserID = Convert.ToInt32(Session["UserID"]);
                DataSet ds = oDiscrepancyBAL.GetDiscrepancyDetailsBAL(oDiscrepancyBE);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    htmlBody = htmlBody.Replace("{InvoiceQueryComm2Line1}", WebCommon.getGlobalResourceValue("InvoiceQueryComm2Line1").Replace("##dd/mm/yyyy##", Common.ToDateTimeInMMDDYYYY(ds.Tables[0].Rows[0]["SentDate"])));

                    htmlBody = htmlBody.Replace("{InvoiceQueryComm3Line1}", WebCommon.getGlobalResourceValue("InvoiceQueryComm3Line1").Replace("##dd/mm/yyyy##", Common.ToDateTimeInMMDDYYYY(ds.Tables[0].Rows[0]["SentDate"])));
                    htmlBody = htmlBody.Replace("{GOODSINCOMMENTS}", ds.Tables[1].Rows[0]["Comment"].ToString());

                    htmlBody = htmlBody.Replace("{INVAPClerkContactNo}", ds.Tables[2].Rows[0][0].ToString());
                }


                htmlBody = htmlBody.Replace("{APClerkletter}", WebCommon.getGlobalResourceValue("APClerkletter"));
                htmlBody = htmlBody.Replace("{GoodsINTeam}", WebCommon.getGlobalResourceValue("GoodsINTeam"));
                htmlBody = htmlBody.Replace("{MediatorTeam}", WebCommon.getGlobalResourceValue("MediatorTeam"));

                htmlBody = htmlBody.Replace("{KindRegards}", WebCommon.getGlobalResourceValue("KindRegards"));
                htmlBody = htmlBody.Replace("{POHash}", WebCommon.getGlobalResourceValue("POHash"));
                htmlBody = htmlBody.Replace("{InvoiceNumHash}", WebCommon.getGlobalResourceValue("InvoiceNumHash"));
                htmlBody = htmlBody.Replace("{DiscrepancyNo}", WebCommon.getGlobalResourceValue("DiscrepancyNo"));
                htmlBody = htmlBody.Replace("{DearSirMadam}", WebCommon.getGlobalResourceValue("DearSirMadam"));
                htmlBody = htmlBody.Replace("{InvoiceQueryComm1Line1}", WebCommon.getGlobalResourceValue("InvoiceQueryComm1Line1"));
                htmlBody = htmlBody.Replace("{InvoiceQueryComm1Line2}", WebCommon.getGlobalResourceValue("InvoiceQueryComm1Line2"));

                htmlBody = htmlBody.Replace("{InvoiceQueryComm2Line1}", WebCommon.getGlobalResourceValue("InvoiceQueryComm2Line1"));
                htmlBody = htmlBody.Replace("{InvoiceQueryComm2Line2}", WebCommon.getGlobalResourceValue("InvoiceQueryComm2Line2"));


                htmlBody = htmlBody.Replace("{InvoiceQueryComm3Line1}", WebCommon.getGlobalResourceValue("InvoiceQueryComm3Line1"));
                htmlBody = htmlBody.Replace("{InvoiceQueryComm3Line2}", WebCommon.getGlobalResourceValue("InvoiceQueryComm3Line2"));

                htmlBody = htmlBody.Replace("{InvoiceQueryComm3Line3}", WebCommon.getGlobalResourceValue("InvoiceQueryComm3Line3"));
                htmlBody = htmlBody.Replace("{InvoiceQueryComm3Line4}", WebCommon.getGlobalResourceValue("InvoiceQueryComm3Line4"));

                htmlBody = htmlBody.Replace("{InvoiceQueryComm3Line5}", WebCommon.getGlobalResourceValue("InvoiceQueryComm3Line5"));
                htmlBody = htmlBody.Replace("{InvoiceQueryComm3Line6}", WebCommon.getGlobalResourceValue("InvoiceQueryComm3Line6"));

                htmlBody = htmlBody.Replace("{InvoiceQueryComm4Line1}", WebCommon.getGlobalResourceValue("InvoiceQueryComm4Line1"));
                htmlBody = htmlBody.Replace("{InvoiceQueryComm4Line2}", WebCommon.getGlobalResourceValue("InvoiceQueryComm4Line2"));

                htmlBody = htmlBody.Replace("{InvoiceQueryComm5Line1}", WebCommon.getGlobalResourceValue("InvoiceQueryComm5Line1"));
                htmlBody = htmlBody.Replace("{InvoiceQueryComm5Line2}", WebCommon.getGlobalResourceValue("InvoiceQueryComm5Line2"));

                htmlBody = htmlBody.Replace("{InvoiceQueryComm6Line1}", WebCommon.getGlobalResourceValue("InvoiceQueryComm6Line1"));
                htmlBody = htmlBody.Replace("{InvoiceQueryComm6Line2}", WebCommon.getGlobalResourceValue("InvoiceQueryComm6Line2"));

                htmlBody = htmlBody.Replace("{InvoiceQueryComm7Line1}", WebCommon.getGlobalResourceValue("InvoiceQueryComm7Line1"));
                htmlBody = htmlBody.Replace("{InvoiceQueryComm7Line2}", WebCommon.getGlobalResourceValue("InvoiceQueryComm7Line2"));

                htmlBody = htmlBody.Replace("{Yoursfaithfully}", WebCommon.getGlobalResourceValue("Yoursfaithfully"));
                htmlBody = htmlBody.Replace("{PortalLink}", "<a href=" + sPortalLink + ">" + sPortalLink + " </a>");

                if (discrepancyType.ToLower() == "invoice query")
                {
                    htmlBody = htmlBody.Replace("{InvoiceValue}", lstDisLog[0].DeliveryNoteNumber);
                    htmlBody = htmlBody.Replace("{DiscrepancyValue}", lstDisLog[0].VDRNo);

                    htmlBody = htmlBody.Replace("{APClerkName}", lstDisLogSiteVendor[0].StockPlannerName);
                    htmlBody = !string.IsNullOrWhiteSpace(lstDisLogSiteVendor[0].StockPlannerContact)
                        ? htmlBody.Replace("{APClerkContactNo}", lstDisLogSiteVendor[0].StockPlannerContact)
                        : htmlBody.Replace("{APClerkContactNo}", string.Empty);

                    htmlBody = htmlBody.Replace("{POValue}", lstDisLog[0].PurchaseOrderNumber);
                    htmlBody = htmlBody.Replace("{GridValue}", sProductGrid);
                    htmlBody = htmlBody.Replace("##DiscrepancyValue##", lstDisLog[0].VDRNo);

                }
            }

            if (discrepancyType.ToLower() == "qtydifferenceovers")
            {
                htmlBody = htmlBody.Replace("##PortalLink##", "<a href=" + sPortalLink + ">" + sPortalLink + " </a>");
            }

            if (discrepancyType.ToLower() == "itemnotonpo")
            {
                htmlBody = htmlBody.Replace("{VDRNo}", lstDisLog[0].VDRNo);
                htmlBody = htmlBody.Replace("{DiscrepancyNo}", WebCommon.getGlobalResourceValue("DiscrepancyNo"));
                htmlBody = htmlBody.Replace("{DeliveryNoteNumberText}", WebCommon.getGlobalResourceValue("DeliveryNoteNumberText"));
                htmlBody = htmlBody.Replace("{DeliveryNoteNumber}", lstDisLog[0].DeliveryNoteNumber);

                if (IsFromVendor)
                {
                    htmlBody = htmlBody.Replace("{ItemNotOnPoComm1Line1}", WebCommon.getGlobalResourceValue("ItemNotOnPoComm1Line1"));
                    htmlBody = htmlBody.Replace("{Warehouse}", lstDisLog[0].Site.SiteName.Split('-').ElementAtOrDefault(1));
                    htmlBody = htmlBody.Replace("{ItemNotOnPoComm1Line2}", WebCommon.getGlobalResourceValue("ItemNotOnPoComm1Line2"));
                    htmlBody = htmlBody.Replace("{ItemNotOnPoComm1PortalLinkMsg}", WebCommon.getGlobalResourceValue("ItemNotOnPoComm1PortalLinkMsg"));
                }
                else
                {
                    htmlBody = htmlBody.Replace("{ItemNotOnPoComm1Line1}", WebCommon.getGlobalResourceValue("ItemNotOnPoComm1Line1New"));
                    htmlBody = htmlBody.Replace("{Warehouse}", lstDisLog[0].Site.SiteName.Split('-').ElementAtOrDefault(1));
                    htmlBody = htmlBody.Replace("{ItemNotOnPoComm1Line2}", WebCommon.getGlobalResourceValue("ItemNotOnPoComm1Line2New"));
                    htmlBody = htmlBody.Replace("{ItemNotOnPoComm1PortalLinkMsg}", WebCommon.getGlobalResourceValue("ItemNotOnPoComm1PortalLinkMsgNew"));
                }

                htmlBody = !string.IsNullOrWhiteSpace(lstDisLogSiteVendor[0].Vendor.county)
                    ? htmlBody.Replace("{VendorCountry}", lstDisLogSiteVendor[0].Vendor.county)
                    : htmlBody.Replace("{VendorCountry}", string.Empty);

                htmlBody = htmlBody.Replace("{StockPlannerName}", lstDisLogSiteVendor[0].StockPlannerName);

                htmlBody = !string.IsNullOrWhiteSpace(lstDisLogSiteVendor[0].StockPlannerContact)
                    ? htmlBody.Replace("{StockPlannerContact}", lstDisLogSiteVendor[0].StockPlannerContact)
                    : htmlBody.Replace("{StockPlannerContact}", string.Empty);
            }
        }
        return htmlBody;
    }

    public int? saveMail(int iDiscrepancyLogID, string sentTo, string mailBody, string mailSubject, int? languageID, string sCommunicationLevel, bool MailSentInLanguage = false, int queryDiscID = 0, bool IsFromQuery = false)
    {
        DiscrepancyMailBE oDiscrepancyMailBE = new DiscrepancyMailBE();
        oDiscrepancyMailBE.Action = "saveDiscrepancyMail";
        oDiscrepancyMailBE.discrepancyLogID = iDiscrepancyLogID;
        oDiscrepancyMailBE.sentTo = sentTo;
        oDiscrepancyMailBE.mailBody = mailBody;
        oDiscrepancyMailBE.mailSubject = mailSubject;
        oDiscrepancyMailBE.sentDate = System.DateTime.Now;
        oDiscrepancyMailBE.languageID = languageID;
        oDiscrepancyMailBE.communicationLevel = sCommunicationLevel;
        oDiscrepancyMailBE.QueryDiscrepancy = new DISLog_QueryDiscrepancyBE();
        oDiscrepancyMailBE.QueryDiscrepancy.QueryDiscrepancyID = queryDiscID;
        oDiscrepancyMailBE.MailSentInLanguage = MailSentInLanguage;
        oDiscrepancyMailBE.IsFromQuery = IsFromQuery;
        DiscrepancyBAL oNewDiscrepancyBAL = new DiscrepancyBAL();
        return oNewDiscrepancyBAL.saveDiscrepancyMailBAL(oDiscrepancyMailBE);
    }
    public int? saveLetter(int iDiscrepancyLogID, string letterBody, string sDiscrepancyLetterID)
    {
        DiscrepancyMailBE oDiscrepancyMailBE = new DiscrepancyMailBE();
        oDiscrepancyMailBE.Action = "saveDiscrepancyLetter";
        oDiscrepancyMailBE.discrepancyLogID = iDiscrepancyLogID;
        oDiscrepancyMailBE.DiscrepancyLetterID = sDiscrepancyLetterID;
        oDiscrepancyMailBE.LoggedDate = System.DateTime.Now;
        oDiscrepancyMailBE.LetterBody = letterBody;
        DiscrepancyBAL oNewDiscrepancyBAL = new DiscrepancyBAL();
        return oNewDiscrepancyBAL.saveDiscrepancyLetterBAL(oDiscrepancyMailBE);
    }

    public void getResourceValue(string sEscalationLevel, string sDiscrepancyType, out string sDisAboveGridText, out string sDisBelowGridText, bool GoodsReturnedDriver = false)
    {
        if (GoodsReturnedDriver == true)
        {
            sDisAboveGridText = WebCommon.getGlobalResourceValue(sEscalationLevel + "_" + sDiscrepancyType + "_" + "AboveGridTextNew");
        }
        else
        {
            if (sDiscrepancyType == "over")
            {
                sDisAboveGridText = WebCommon.getGlobalResourceValue(sEscalationLevel + "_" + sDiscrepancyType + "_" + "AboveGridTextOverNew");
            }
            else if (sDiscrepancyType == "qtydifferenceovers")
            {
                sDisAboveGridText = WebCommon.getGlobalResourceValue(sEscalationLevel + "_" + sDiscrepancyType + "_" + "AboveGridTextQtyDiffNew");
            }
            else
            {
                sDisAboveGridText = WebCommon.getGlobalResourceValue(sEscalationLevel + "_" + sDiscrepancyType + "_" + "AboveGridText");
            }
        }
        if (GoodsReturnedDriver == true && sEscalationLevel == "Communication2")
        {
            sDisBelowGridText = WebCommon.getGlobalResourceValue(sEscalationLevel + "_" + sDiscrepancyType + "_" + "BelowGridTextNew");
        }
        else
        {
            sDisBelowGridText = WebCommon.getGlobalResourceValue(sEscalationLevel + "_" + sDiscrepancyType + "_" + "BelowGridText");
        }
    }
    public void errorLog(int iDiscrepancyID, string sSubject, string sSentTo, string sBody, string sErrorDescription)
    {
        DiscrepancyMailBE oDiscrepancyMailBE = new DiscrepancyMailBE();
        oDiscrepancyMailBE.Action = "errorLog";
        oDiscrepancyMailBE.discrepancyLogID = iDiscrepancyID;
        oDiscrepancyMailBE.mailSubject = sSubject;
        oDiscrepancyMailBE.sentTo = sSentTo;
        oDiscrepancyMailBE.mailBody = sBody;
        oDiscrepancyMailBE.sentDate = DateTime.Now.Date;
        oDiscrepancyMailBE.Status = 'O';
        oDiscrepancyMailBE.errorDescription = sErrorDescription.Length > 3000 ? sErrorDescription.Substring(0, 3000) : sErrorDescription;
        DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();
        oDiscrepancyBAL.errorLogBAL(oDiscrepancyMailBE);
    }

    public List<DiscrepancyBE> GetSPInitialCommDetails(string Action, int iStockPlannerID)
    {
        // GET DiscrepancyLogDetails
        DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
        DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();
        oDiscrepancyBE.Action = Action;
        oDiscrepancyBE.StockPlannerID = Convert.ToInt32(iStockPlannerID);
        return oDiscrepancyBAL.GetSPInitialCommDetailsBAL(oDiscrepancyBE);
    }

    public List<DiscrepancyBE> GetSPOnHolidayCoverInitialCommDetails(string Action, int iStockPlannerID, int iSiteID)
    {
        // GET DiscrepancyLogDetails
        DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
        DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();
        oDiscrepancyBE.Action = Action;
        oDiscrepancyBE.StockPlannerID = Convert.ToInt32(iStockPlannerID);
        oDiscrepancyBE.SiteID = Convert.ToInt32(iSiteID);
        return oDiscrepancyBAL.GetStockPlannerOnHolidayCoverBAL(oDiscrepancyBE);
    }
}

// This is for all level except communication1 level
public class sendCommunicationAllLevel : sendCommunicationCommon
{

    string path = AppDomain.CurrentDomain.BaseDirectory.ToLower();
    sendCommunicationCommon oSendCommunicationCommon = new sendCommunicationCommon();
    //string path = AppDomain.CurrentDomain.BaseDirectory.ToLower();
    public string sendCommunicationByEMail(int iDiscrepancyLogID, string discrepancyType, string sCurrentCommunicationLevel, string sNextCommunicationLevel, string sInvAction = "")
    {
        string mailSubjectText = string.Empty;
        string toAddress = string.Empty;
        string htmlBody = string.Empty;
        int? identityValue;
        StringBuilder sSendMailLink = new StringBuilder();
        try
        {
            List<DiscrepancyBE> lstDisLog = getDiscrepancyLogDetail("GetDiscrepancyLog", iDiscrepancyLogID, discrepancyType);
            List<DiscrepancyBE> lstDetails = getDiscrepancyItemDetail("GetDiscrepancyItem", iDiscrepancyLogID, discrepancyType);
            if (lstDisLog != null && lstDisLog.Count > 0)
            {
                DataSet dsCommunication = getDiscrepancyCommunicationDetail("getDiscrepancyCommunicationDetail", iDiscrepancyLogID, sCurrentCommunicationLevel);
                DateTime? dtCommunication1SentDate, dtCommunication2SentDate, dtCommunication3SentDate, dtCommunication4SentDate = null;
                getCommunicationDate(dsCommunication, out dtCommunication1SentDate, out dtCommunication2SentDate, out dtCommunication3SentDate, out dtCommunication4SentDate);

                if (dsCommunication != null && dsCommunication.Tables.Count > 0 && dsCommunication.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < dsCommunication.Tables[0].Rows.Count; i++)
                    {
                        toAddress = Convert.ToString(dsCommunication.Tables[0].Rows[i]["sentTo"]);
                        int? languageID = (int?)(dsCommunication.Tables[0].Rows[i]["languageid"] == DBNull.Value ? 0 : dsCommunication.Tables[0].Rows[i]["languageid"]);

                        bool MailSentInLanguage1 = false;
                        string templateFile = null;

                        if (discrepancyType.ToLower() == "invoice query")
                        {
                            templateFile = path + templatesPath + "/" + sNextCommunicationLevel + "/" + "InvoiceQuery.english.htm";
                        }
                        else if (discrepancyType.ToLower() == "itemnotonpo")
                        {
                            templateFile = path + templatesPath + "/" + sNextCommunicationLevel + "/" + "itemnotonpo.english.htm";
                        }
                        else
                        {
                            templateFile = path + templatesPath + "/" + sNextCommunicationLevel + "/" + "dis_Communication.english.htm";
                        }


                        // Changing Resourse file according to language
                        if ((Convert.ToInt16(dsCommunication.Tables[0].Rows[i]["languageid"]) == 1) || (dsCommunication.Tables[0].Rows[i]["language"] == DBNull.Value))
                        {
                            Page.UICulture = clsConstants.EnglishISO; //"en-US";
                        }
                        else if ((Convert.ToInt16(dsCommunication.Tables[0].Rows[i]["languageid"]) == 2))
                        {
                            Page.UICulture = clsConstants.FranceISO;//"fr";
                        }
                        else if ((Convert.ToInt16(dsCommunication.Tables[0].Rows[i]["languageid"]) == 3))
                        {
                            Page.UICulture = clsConstants.GermanyISO;//"de"; // German
                        }
                        else if ((Convert.ToInt16(dsCommunication.Tables[0].Rows[i]["languageid"]) == 4))
                        {
                            Page.UICulture = clsConstants.NederlandISO;//"nl"; // Dutch
                        }
                        else if ((Convert.ToInt16(dsCommunication.Tables[0].Rows[i]["languageid"]) == 5))
                        {
                            Page.UICulture = clsConstants.SpainISO; //"es"; // Spanish
                        }
                        else if ((Convert.ToInt16(dsCommunication.Tables[0].Rows[i]["languageid"]) == 6))
                        {
                            Page.UICulture = clsConstants.ItalyISO;//"it"; // Italy
                        }
                        else if ((Convert.ToInt16(dsCommunication.Tables[0].Rows[i]["languageid"]) == 7))
                        {
                            Page.UICulture = clsConstants.CzechISO;//"cz"; // Czech
                        }

                        if (System.IO.File.Exists(templateFile.ToLower()))
                        {
                            string siteAddress, accountPayable, sDisAboveGridText, sDisBelowGridText = string.Empty;
                            string sProductGrid = getDiscrepancyItemGrid(discrepancyType, lstDisLog, lstDetails, out sDisAboveGridText, out sDisBelowGridText, convertDateTime(dtCommunication1SentDate), convertDateTime(dtCommunication2SentDate), convertDateTime(dtCommunication3SentDate), convertDateTime(dtCommunication4SentDate), sNextCommunicationLevel);

                            htmlBody = getMailBody(iDiscrepancyLogID, discrepancyType,
                                lstDisLog, templateFile,
                                sProductGrid, sDisBelowGridText, sDisAboveGridText,
                                out siteAddress, out accountPayable, sInvAction, true);

                            if (discrepancyType.ToLower() == "itemnotonpo")
                            {
                                htmlBody = htmlBody.Replace("{POHash}", WebCommon.getGlobalResourceValue("POHash"));
                                htmlBody = htmlBody.Replace("{InvoiceNumHash}", WebCommon.getGlobalResourceValue("InvoiceNumHash"));
                                htmlBody = htmlBody.Replace("{DiscrepancyNo}", WebCommon.getGlobalResourceValue("DiscrepancyNo"));
                                htmlBody = htmlBody.Replace("{DearSirMadam}", WebCommon.getGlobalResourceValue("DearSirMadam"));
                                htmlBody = htmlBody.Replace("{InvoiceQueryComm1Line1}", WebCommon.getGlobalResourceValue("InvoiceQueryComm1Line1"));
                                htmlBody = htmlBody.Replace("{InvoiceQueryComm1Line2}", WebCommon.getGlobalResourceValue("InvoiceQueryComm1Line2"));
                                htmlBody = htmlBody.Replace("{Yoursfaithfully}", WebCommon.getGlobalResourceValue("Yoursfaithfully"));

                                htmlBody = htmlBody.Replace("{PortalLink}", "<a href=" + sPortalLink + ">" + sPortalLink + " </a>");
                            }

                            /* Logic to set the Nax PO No in letters. */
                            var NaxPoDetails = lstDetails.Find(x => !string.IsNullOrEmpty(x.NaxPONo));
                            if (NaxPoDetails != null)
                            {
                                if (!string.IsNullOrEmpty(NaxPoDetails.NaxPONo))
                                {
                                    htmlBody = htmlBody.Replace("{NaxPuchaseOrderNumber}", WebCommon.getGlobalResourceValue("NaxPuchaseOrderNumber"));
                                    htmlBody = htmlBody.Replace("{NaxActualPONumber}", Convert.ToString(NaxPoDetails.NaxPONo));
                                }
                                else
                                {
                                    htmlBody = htmlBody.Replace("{NaxPuchaseOrderNumber}:", string.Empty);
                                    htmlBody = htmlBody.Replace("{NaxActualPONumber}", string.Empty);
                                }
                            }
                            else
                            {
                                htmlBody = htmlBody.Replace("{NaxPuchaseOrderNumber}:", string.Empty);
                                htmlBody = htmlBody.Replace("{NaxActualPONumber}", string.Empty);
                            }

                            string mailBodyToSaveInDB = htmlBody.Replace("{logoInnerPath}", getAbsolutePath());
                            // mailSubjectText = "Vendor Discrepancy Report " + lstDisLog[0].VDRNo + " : Urgent Responses Required";

                            if (discrepancyType.ToLower() == "querydiscaccept")
                                mailSubjectText = WebCommon.getGlobalResourceValue("QueryDiscrepancyAcceptanceLetter");
                            else if (discrepancyType.ToLower() == "querydiscreject")
                                mailSubjectText = WebCommon.getGlobalResourceValue("QueryDiscrepancyRejectionLetter");
                            else if (discrepancyType.ToLower() == "deleteddiscrepancy")
                                mailSubjectText = WebCommon.getGlobalResourceValue("DeletedDiscrepanciesLetter");
                            else
                                mailSubjectText = WebCommon.getGlobalResourceValue("EmailSubjectTextForCommunication2").Replace("##VDRNo##", lstDisLog[0].VDRNo);

                            try
                            {
                                //Sending mail only to vendors where MailSentInLanguage = true instaed of sending to vendors with all languages
                                if (dsCommunication.Tables[0].Rows[i]["MailSentInLanguage"].ToString() == "True")
                                {
                                    clsEmail oclsEmail = new clsEmail();
                                    oclsEmail.sendMail(toAddress, htmlBody, mailSubjectText, sFromAddress, true);
                                    MailSentInLanguage1 = true;
                                }
                            }
                            catch (Exception innerex)
                            {
                                errorLog(iDiscrepancyLogID, mailSubjectText, toAddress, htmlBody, innerex.Message);
                            }

                            identityValue = saveMail(iDiscrepancyLogID, toAddress, mailBodyToSaveInDB, mailSubjectText, languageID, sNextCommunicationLevel.ToEscalationLevelOfficeDepotStyle(), MailSentInLanguage1);

                            if (MailSentInLanguage1)// Will create link only for vendor to whom mails are sent
                            {
                                if (identityValue != null)
                                    sSendMailLink.Append("<a target='_blank' href='" + Common.EncryptQuery("DISLog_ReSendCommunication.aspx?communicationid=" + identityValue.ToString() + "&CommunicationLevel=" + sNextCommunicationLevel) + "' onclick='window.open(this.href, this.target,\"scrollbars=1,left=100px,top=100px,width=1000px,height=800px\"); return false;'>" + toAddress + "</a>" + System.Environment.NewLine);
                                else
                                    sSendMailLink.Append("<a target='_blank' href='" + Common.EncryptQuery("DISLog_ReSendCommunication.aspx?communicationid=") + "' onclick='window.open(this.href, this.target,\"scrollbars=1,left=100px,top=100px,width=1000px,height=800px\"); return false;'>" + toAddress + "</a>" + System.Environment.NewLine);
                            }
                        }
                        else
                        {
                            errorLog(iDiscrepancyLogID, mailSubjectText, toAddress, htmlBody, "Template file not found");
                        }
                    }
                    //setting default Resourse file
                    Page.UICulture = Convert.ToString(Session["CultureInfo"]);
                }
            }
            else
                errorLog(iDiscrepancyLogID, mailSubjectText, toAddress, htmlBody, "No Data in DiscrepancyLog table");
        }
        catch (Exception ex)
        {
            errorLog(iDiscrepancyLogID, mailSubjectText, toAddress, htmlBody, ex.Message);
            return null;
        }
        return sSendMailLink.ToString();
    }
    private string getDiscrepancyItemGrid(string discrepancyType, List<DiscrepancyBE> lstDisLog, List<DiscrepancyBE> lstDetails, out string sDisAboveGridText, out string sDisBelowGridText, string sCommunication1SentDate, string sCommunication2SentDate, string sCommunication3SentDate, string sCommunication4SentDate, string sNextCommunicationLevel)
    {
        string Suppliercode = WebCommon.getGlobalResourceValue("Suppliercode");
        string OurCode = WebCommon.getGlobalResourceValue("OurCode");
        string Description = WebCommon.getGlobalResourceValue("Description1");
        string VendorItemCode = WebCommon.getGlobalResourceValue("VendorItemCode");
        string OutstandingPOQty = WebCommon.getGlobalResourceValue("OutstandingPOQty");
        string DeliveryNoteQty = WebCommon.getGlobalResourceValue("DeliveryNoteQty");
        string DeliveredQty = WebCommon.getGlobalResourceValue("DeliveredQty");
        string QtyOverDelivered = WebCommon.getGlobalResourceValue("QtyOverDelivered");
        string ReceivedQuantity = WebCommon.getGlobalResourceValue("ReceivedQuantity1");
        string ShortageQuantity = WebCommon.getGlobalResourceValue("ShortageQuantity");
        string DamagedQuantity = WebCommon.getGlobalResourceValue("DamagedQuantity");
        string DamagedDescription = WebCommon.getGlobalResourceValue("DamagedDescription");
        string RequiredCode = WebCommon.getGlobalResourceValue("RequiredCode");
        string UOM = WebCommon.getGlobalResourceValue("UOM");
        string CodeReceived = WebCommon.getGlobalResourceValue("CodeReceived");
        string POPackSize = WebCommon.getGlobalResourceValue("POPackSize");
        string PackSizeofgooddelivered = WebCommon.getGlobalResourceValue("PackSizeofgooddelivered");
        string strLine = WebCommon.getGlobalResourceValue("Line");
        string strODCode = WebCommon.getGlobalResourceValue("ODCode");
        string strVikingCode = WebCommon.getGlobalResourceValue("VikingCode");
        string strOriginalPOQty = WebCommon.getGlobalResourceValue("OriginalPOQty");

        sDisAboveGridText = sDisBelowGridText = string.Empty;
        StringBuilder sProductDetail = new StringBuilder();

        if (lstDisLog != null && lstDisLog.Count > 0 && lstDetails != null && lstDetails.Count > 0)
        {
            sProductDetail.Append("<table style='background: none repeat scroll 0 0 #FFFFFF;border: 1px solid #DADADA;cellspacing=0 cellpadding=0 border=0 border-collapse:collapse;width=100%'>");
            #region Item Grid
            switch (discrepancyType.ToLower())
            {
                case "qtydifferenceovers":
                    sProductDetail.Append("<tr style='color:#934500;border-bottom: 1px solid #E8E8E8;font-weight: bold;font-family:Arial ;font-size:12;vertical-align: top;'><td width='15%'>" + OurCode + "</td><td width='35%'> " + Description + "</td><td width='10%'>" + VendorItemCode + "</td><td  width='10%'>" + OutstandingPOQty + "</td><td  width='10%'>" + DeliveryNoteQty + "</td><td  width='10%'>" + DeliveredQty + "</td><td  width='10%'>" + QtyOverDelivered + "</td></tr>");
                    for (int i = 0; i < lstDetails.Count; i++)
                    {
                        sProductDetail.Append("<tr style='background-color: #F7F6F3;font-size:12;font-family:Arial ;'><td>" + lstDetails[i].ODSKUCode + "</td><td>" + lstDetails[i].ProductDescription + "</td><td>" + lstDetails[i].VendorCode);
                        sProductDetail.Append("</td><td>" + lstDetails[i].OutstandingQuantity + "</td><td>" + lstDetails[i].DeliveredNoteQuantity + "</td><td>" + lstDetails[i].DeliveredQuantity + "</td>");
                        sProductDetail.Append("<td>" + lstDetails[i].OversQuantity + "</td></tr>");
                    }
                    break;
                case "qtydifferenceshortage":
                    sProductDetail.Append("<tr style='color:#934500;border-bottom: 1px solid #E8E8E8;font-weight: bold;font-family:Arial ;font-size:12;vertical-align: top;'><td width='15%'>" + OurCode + "</td><td width='35%'> " + Description + "</td><td width='10%'>" + VendorItemCode + "</td><td  width='10%'>" + OutstandingPOQty + "</td><td  width='10%'>" + DeliveryNoteQty + "</td><td  width='10%'>" + ReceivedQuantity + "</td><td  width='10%'>" + ShortageQuantity + "</td></tr>");
                    for (int i = 0; i < lstDetails.Count; i++)
                    {
                        decimal dReceivedQuantity = (lstDetails[i].DeliveredNoteQuantity ?? 0) - (lstDetails[i].ShortageQuantity ?? 0);
                        sProductDetail.Append("<tr style='background-color: #F7F6F3;font-size:12;font-family:Arial ;'><td>" + lstDetails[i].ODSKUCode + "</td><td>" + lstDetails[i].ProductDescription + "</td><td>" + lstDetails[i].VendorCode);
                        sProductDetail.Append("</td><td>" + lstDetails[i].OutstandingQuantity + "</td><td>" + lstDetails[i].DeliveredNoteQuantity + "</td><td>" + dReceivedQuantity + "</td><td>" + lstDetails[i].ShortageQuantity + "</td></tr>");
                    }
                    break;
                case "goodsreceiveddamaged":
                    sProductDetail.Append("<tr style='color:#934500;border-bottom: 1px solid #E8E8E8;font-weight: bold;font-family:Arial ;font-size:12;vertical-align: top;'><td width='15%'>" + OurCode + "</td><td width='35%'> " + Description + "</td><td width='10%'>" + VendorItemCode + "</td><td  width='10%'>" + ReceivedQuantity + "</td><td  width='10%'>" + DamagedQuantity + "</td><td  width='10%'>" + DamagedDescription + "</td></tr>");
                    for (int i = 0; i < lstDetails.Count; i++)
                    {
                        sProductDetail.Append("<tr style='background-color: #F7F6F3;font-size:12;font-family:Arial ;'><td>" + lstDetails[i].ODSKUCode + "</td><td>" + lstDetails[i].ProductDescription + "</td><td>" + lstDetails[i].VendorCode);
                        sProductDetail.Append("</td><td>" + lstDetails[i].OutstandingQuantity + "</td><td>" + lstDetails[i].DamageQuantity + "</td><td>" + lstDetails[i].DamageDescription + "</td></tr>");
                    }
                    break;
                case "nopurchaseordernumber":
                    sProductDetail.Append("<tr style='color:#934500;border-bottom: 1px solid #E8E8E8;font-weight: bold;font-family:Arial ;font-size:12;vertical-align: top;'><td width='15%'>" + OurCode + "</td><td width='35%'> " + Description + "</td><td width='10%'>" + VendorItemCode + "</td><td  width='10%'>" + ReceivedQuantity + "</td></tr>");
                    for (int i = 0; i < lstDetails.Count; i++)
                    {
                        sProductDetail.Append("<tr style='background-color: #F7F6F3;font-size:12;font-family:Arial ;'><td>" + lstDetails[i].ODSKUCode + "</td><td>" + lstDetails[i].ProductDescription + "</td><td>" + lstDetails[i].VendorCode);
                        sProductDetail.Append("</td><td>" + lstDetails[i].DeliveredQuantity + "</td></tr>");
                    }
                    break;
                case "incorrectproductcode":
                    sProductDetail.Append("<tr style='color:#934500;border-bottom: 1px solid #E8E8E8;font-weight: bold;font-family:Arial ;font-size:12;vertical-align: top;'><td width='10%'>" + RequiredCode + "</td><td width='20%'>" + Description + "</td><td width='10%'>" + VendorItemCode + "</td><td  width='10%'>" + ReceivedQuantity + "</td><td  width='10%'>" + UOM + "</td><td  width='10%'>" + CodeReceived + "</td><td  width='10%'>" + Description + "</td><td width='10%'>" + VendorItemCode + "</td><td width='10%'>" + UOM + "</td></tr>");
                    for (int i = 0; i < lstDetails.Count; i++)
                    {
                        sProductDetail.Append("<tr style='background-color: #F7F6F3;font-size:12;font-family:Arial ;'><td>" + lstDetails[i].ODSKUCode + "</td><td>" + lstDetails[i].ProductDescription + "</td><td>" + lstDetails[i].VendorCode + "</td><td>" + lstDetails[i].DeliveredQuantity + "</td><td>" + lstDetails[i].UOM + "</td><td>" + lstDetails[i].CorrectODSKUCode + "</td><td>" + lstDetails[i].CorrectProductDescription + "</td><td>" + lstDetails[i].CorrectVendorCode + "</td><td>" + lstDetails[i].CorrectUOM + "</td></tr>");
                    }
                    break;
                case "incorrectpacksize":
                    sProductDetail.Append("<tr style='color:#934500;border-bottom: 1px solid #E8E8E8;font-weight: bold;font-family:Arial ;font-size:12;vertical-align: top;'><td width='15%'>" + OurCode + "</td><td width='40%'>" + Description + "</td><td width='15%'>" + VendorItemCode + "</td><td  width='15%'>" + POPackSize + "</td><td  width='15%'>" + WebCommon.getGlobalResourceValue("PackSizeofgooddeliveredNew") + "</td><td  width='15%'>" + WebCommon.getGlobalResourceValue("NoofPacksReceived") + "</td><td  width='15%' class='style10'>" + WebCommon.getGlobalResourceValue("#Units") + "</td></tr>");
                    for (int i = 0; i < lstDetails.Count; i++)
                    {
                        sProductDetail.Append("<tr style='background-color: #F7F6F3;font-size:12;font-family:Arial ;'><td>" + lstDetails[i].ODSKUCode + "</td><td>" + lstDetails[i].ProductDescription + "</td><td>" + lstDetails[i].VendorCode + "</td><td>" + lstDetails[i].PackSizeOrdered + "</td><td>" + lstDetails[i].PackSizeReceived + "</td><td>" + lstDetails[i].WrongPackReceived + "</td><td>" + Convert.ToInt32(lstDetails[i].PackSizeReceived) * lstDetails[i].WrongPackReceived + "</td></tr>");
                    }
                    break;
                case "nopaperwork":
                    sProductDetail.Append("<tr style='color:#934500;border-bottom: 1px solid #E8E8E8;font-weight: bold;font-family:Arial ;font-size:12;vertical-align: top;'><td width='15%'>" + OurCode + "</td><td width='40%'>" + Description + "</td><td width='15%'>" + VendorItemCode + "</td><td  width='15%'>" + ReceivedQuantity + "</td></tr>");
                    for (int i = 0; i < lstDetails.Count; i++)
                    {
                        sProductDetail.Append("<tr style='background-color: #F7F6F3;font-size:12;font-family:Arial ;'><td>" + lstDetails[i].ODSKUCode + "</td><td>" + lstDetails[i].ProductDescription + "</td><td>" + lstDetails[i].VendorCode + "</td><td>" + lstDetails[i].DeliveredQuantity + "</td></tr>");
                    }
                    break;
                case "invoice query":
                    sProductDetail.Append("<tr style='color:#934500;border-bottom: 1px solid #E8E8E8;font-weight: bold;font-family:Arial ;font-size:12;vertical-align: top;'><td width='5%'>"
                       + strLine + "</td><td width='10%'>" + strODCode
                       + "</td><td width='10%'>" + strVikingCode + "</td><td width='15%'>" + VendorItemCode + "</td><td width='30%'>" + Description + "</td><td  width='10%'>" + UOM + "</td><td width='10%'>"
                       + strOriginalPOQty + "</td><td  width='15%'>" + WebCommon.getGlobalResourceValue("ReceiptedQty") + "</td><td  width='10%'>" + WebCommon.getGlobalResourceValue("InvoiceQTY") + "</td></tr>");
                    for (int i = 0; i < lstDetails.Count; i++)
                    {
                        sProductDetail.Append("<tr style='background-color: #F7F6F3;font-size:12;font-family:Arial ;'><td>"
                            + lstDetails[i].Line_no + "</td><td>" + lstDetails[i].ODSKUCode + "</td><td>" + lstDetails[i].DirectCode + "</td><td>" + lstDetails[i].VendorCode);
                        sProductDetail.Append("</td><td>" + lstDetails[i].ProductDescription + "</td><td>" + lstDetails[i].UOM + "</td><td>" + lstDetails[i].OriginalQuantity + "</td><td>"
                            + lstDetails[i].ReceiptedQty
                            + "</td><td>" + lstDetails[i].InvoiceQty + "</td>");
                        sProductDetail.Append("</tr>");
                    }
                    break;
                case "itemnotonpo":
                    sProductDetail.Append("<tr style='color:#934500;border-bottom: 1px solid #E8E8E8;font-weight: bold;font-family:Arial ;font-size:12;vertical-align: top;'><td width='15%'>"
                        + strODCode + "</td><td width='15%' class='style10'>"
                         + strVikingCode + "</td><td width='20%' class='style10'>" + Suppliercode + "</td><td width='30%' class='style10'>"
                         + Description + "</td><td  width='10%' class='style10'>" + ReceivedQuantity + "</td><td  width='10%' class='style10'>"
                         + UOM + "</td></tr>");
                    for (int i = 0; i < lstDetails.Count; i++)
                    {
                        sProductDetail.Append("<tr style='background-color: #F7F6F3;font-size:12;font-family:Arial ;'><td>"
                            + lstDetails[i].ODSKUCode + "</td><td>"
                            + lstDetails[i].DirectCode + "</td><td>" + lstDetails[i].VendorCode + "</td><td>"
                            + lstDetails[i].ProductDescription + "</td><td>" + lstDetails[i].DeliveredQuantity + "</td><td>" + lstDetails[i].UOM + "</td></tr>");
                    }
                    break;
            }
            #endregion
            sProductDetail.Append("</table>");
        }
        else
        {
            #region No Item Grid
            switch (discrepancyType.ToLower())
            {
                case "qualityissue":
                    break;
                case "presentationissue":
                    break;
                case "incorrectdeliveryaddress":
                    break;
                case "failpalletspecification":
                    break;
                case "prematureinvoicereceipt":
                    break;
                case "genericdiscrepance":
                    break;
            }
            #endregion
        }

        getResourceValue(sNextCommunicationLevel.ToEscalationLevelOfficeDepotStyle(), discrepancyType, out sDisAboveGridText, out sDisBelowGridText, lstDisLog[0].GoodsReturnedDriver);
        if (lstDisLog != null && lstDisLog.Count > 0 && !string.IsNullOrEmpty(sDisAboveGridText))
        {
            //string sCommunicationSentDate = string.Empty;
            //if (sCommunication4SentDate.IsDate())
            //    sCommunicationSentDate = sCommunication4SentDate;
            //else if (sCommunication3SentDate.IsDate())
            //    sCommunicationSentDate = sCommunication3SentDate;
            //else if (sCommunication2SentDate.IsDate())
            //    sCommunicationSentDate = sCommunication2SentDate;
            //else if (sCommunication1SentDate.IsDate())
            //    sCommunicationSentDate = sCommunication1SentDate;

            string sCommunicationSentDate = string.Empty;
            if (!string.IsNullOrEmpty(sCommunication4SentDate))
                sCommunicationSentDate = sCommunication4SentDate;
            else if (!string.IsNullOrEmpty(sCommunication3SentDate))
                sCommunicationSentDate = sCommunication3SentDate;
            else if (!string.IsNullOrEmpty(sCommunication2SentDate))
                sCommunicationSentDate = sCommunication2SentDate;
            else if (!string.IsNullOrEmpty(sCommunication1SentDate))
                sCommunicationSentDate = sCommunication1SentDate;

            sDisAboveGridText = sDisAboveGridText.Replace("##DiscrepancyLogDate##", sCommunicationSentDate);
            sDisAboveGridText = sDisAboveGridText.Replace("##Communication2SentDate##", sCommunicationSentDate);
            sDisAboveGridText = sDisAboveGridText.Replace("##Communication3SentDate##", sCommunicationSentDate);
            sDisAboveGridText = sDisAboveGridText.Replace("##Communication4SentDate##", sCommunicationSentDate);
            sDisAboveGridText = sDisAboveGridText.Replace("##QualityIssueComment##", lstDisLog[0].QualityIssueComment);
            sDisAboveGridText = sDisAboveGridText.Replace("##DeliveredSiteAddress##", lstDisLog[0].Site.SiteName);
            sDisAboveGridText = sDisAboveGridText.Replace("##POSiteAddress##", lstDisLog[0].POSiteName);
            sDisAboveGridText = sDisAboveGridText.Replace("##GenericDiscrepancyIssueComment##", lstDisLog[0].GenericDiscrepancyIssueComment);

            if (lstDisLog[0].QtyToBeSelected == "Outstanding Purchase Order Quantity")
            {
                sDisAboveGridText = sDisAboveGridText.Replace("{QtySelected}", WebCommon.getGlobalResourceValue("OutstandingPOQuantitySelected"));
            }
            else
            {
                sDisAboveGridText = sDisAboveGridText.Replace("{QtySelected}", WebCommon.getGlobalResourceValue("DeliveryNoteQuantitySelected"));
            }

            if (!string.IsNullOrEmpty(sDisBelowGridText))
            {
                sDisBelowGridText = sDisBelowGridText.Replace("##PortalLink##", "<a href='" + sPortalLink + "'>" + sPortalLink + " </a>");
                if (lstDisLog[0].NumberOfPallets == (int?)null)
                    sDisBelowGridText = sDisBelowGridText.Replace("##FreightCharges##", Convert.ToString(lstDisLog[0].Freight_Charges));
                else
                    sDisBelowGridText = sDisBelowGridText.Replace("##FreightCharges##", Convert.ToString(lstDisLog[0].TotalLabourCost));
                sDisBelowGridText = sDisBelowGridText.Replace("##PurchaseOrderNumber##", lstDisLog[0].PurchaseOrderNumber);
            }
            if (!string.IsNullOrEmpty(sDisAboveGridText) && sDisAboveGridText.Contains("##FreightCharges##"))
            {
                sDisAboveGridText = sDisAboveGridText.Replace("##PortalLink##", "<a href='" + sPortalLink + "'>" + sPortalLink + " </a>");
                if (lstDisLog[0].NumberOfPallets == (int?)null)
                    sDisAboveGridText = sDisAboveGridText.Replace("##FreightCharges##", Convert.ToString(lstDisLog[0].Freight_Charges));
                else
                    sDisAboveGridText = sDisAboveGridText.Replace("##FreightCharges##", Convert.ToString(lstDisLog[0].TotalLabourCost));
                sDisAboveGridText = sDisAboveGridText.Replace("##PurchaseOrderNumber##", lstDisLog[0].PurchaseOrderNumber);
            }
        }
        return sProductDetail.ToString();
    }
    public string SendMailAndGetMailLinks(int DiscrepancyLogID, string discrepancyType, string sCurrentCommunicationLevel, string sNextCommunicationLevel, string sInvAction = "")
    {
        StringBuilder sSendMailLink = new StringBuilder();
        return sendCommunicationByEMail(DiscrepancyLogID, discrepancyType, sCurrentCommunicationLevel, sNextCommunicationLevel, sInvAction);

    }

    public string GetVendorEmails(int VendorID)
    {
        MASSIT_VendorBE oMASSIT_VendorBE = new MASSIT_VendorBE();
        APPSIT_VendorBAL oAPPSIT_VendorBAL = new APPSIT_VendorBAL();
        string VendorEmail = string.Empty;
        List<MASSIT_VendorBE> lstVendorDetails = null;

        oMASSIT_VendorBE.Action = "GetVendorEmail"; // from user table
        oMASSIT_VendorBE.SiteVendorID = VendorID;

        lstVendorDetails = oAPPSIT_VendorBAL.GetVendorContactDetailsBAL(oMASSIT_VendorBE);
        if (lstVendorDetails != null && lstVendorDetails.Count > 0)
        {

            foreach (MASSIT_VendorBE item in lstVendorDetails)
            {
                if (item.Vendor.VendorContactEmail == null || item.Vendor.VendorContactEmail == "")
                {
                    VendorEmail = GetVendorDefaultEmail(VendorID);
                }
                else
                {
                    VendorEmail += item.Vendor.VendorContactEmail.ToString() + ", ";
                }
            }
        }
        else
        {
            VendorEmail = GetVendorDefaultEmail(VendorID);
        }
        return VendorEmail;
    }
    private string GetVendorDefaultEmail(int VendorID)
    {
        MASSIT_VendorBE oMASSIT_VendorBE = new MASSIT_VendorBE();
        APPSIT_VendorBAL oAPPSIT_VendorBAL = new APPSIT_VendorBAL();
        List<MASSIT_VendorBE> lstVendorDetails = null;
        string VendorEmail = string.Empty;
        oMASSIT_VendorBE.Action = "GetContactDetails"; // from Up_vendor table
        oMASSIT_VendorBE.SiteVendorID = VendorID;

        lstVendorDetails = oAPPSIT_VendorBAL.GetVendorContactDetailsBAL(oMASSIT_VendorBE);
        foreach (MASSIT_VendorBE item in lstVendorDetails)
        {
            if (!string.IsNullOrEmpty(item.Vendor.VendorContactEmail))
            {
                VendorEmail += item.Vendor.VendorContactEmail.ToString() + ", ";
            }
        }
        return VendorEmail;




    }
}
    


public static class ConvertToOfficeDepotStyle
{
    public static string ToEscalationLevelOfficeDepotStyle(this string str)
    {
        switch (str)
        {
            case "communication1":
                str = "Communication1";  //InitialCommunication
                break;
            case "communication2":
                str = "Communication2";  //firstescalation
                break;
            case "communication3":
                str = "Communication3";  //secondescalation
                break;
            case "communication4":
                str = "Communication4";  //thirdescalation
                break;
        }
        return str;
    }
}
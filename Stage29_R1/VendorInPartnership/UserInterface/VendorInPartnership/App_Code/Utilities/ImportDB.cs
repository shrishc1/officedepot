﻿using BusinessEntities.ModuleBE.Upload;
using BusinessLogicLayer.ModuleBAL.Upload;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.IO;
using System.Linq;

/// <summary>
/// Summary description for ImportDB
/// </summary>
public class ImportDB
{
    public ImportDB()
    {
        //
        // TODO: Add constructor logic here
        //

    }

    public void ReadTextFile()
    {
        string line;
        string sSQL = string.Empty;
        string sValue = string.Empty;
        string datatype = string.Empty;

        SqlConnection sqlCon = new SqlConnection("Data Source=172.29.9.3;Initial Catalog=OD-VendorsInPartnership;Persist Security Info=True;User ID=officedepot;Password=officedepot");
        sqlCon.Open();

        string sSQLType = "SELECT t.name AS type_name   " +
                                   "FROM sys.columns AS c " +
                                   "JOIN sys.types AS t ON c.user_type_id=t.user_type_id " +
                                   "WHERE c.object_id = OBJECT_ID('UP_SKU')" +
                                   "ORDER BY c.column_id";

        SqlCommand sqlComm = new SqlCommand(sSQLType, sqlCon);
        SqlDataReader reader = sqlComm.ExecuteReader();

        ArrayList al = new ArrayList();

        while (reader.Read())
        {
            object[] values = new object[reader.FieldCount];
            reader.GetValues(values);
            al.Add(values);
        }

        reader.Close();

        // Read the file and display it line by line.
        using (StreamReader file = new StreamReader(@"D:\Projects\OfficeDepot\DATA FILES\UK_PurchaseOrderdet.txt"))
        {
            file.ReadLine();
            while ((line = file.ReadLine()) != null)
            {
                //Vendor
                //sSQL = "INSERT INTO [VendorDetail](Vendor_Country,[Vendor_No],[Vendor_Name],[Vendor Contact Name] ,[Vendor Contact Number],[Vendor Contact Fax]           ,[Vendor Contact Email] ,[address 1],[address 2] ,[city],[county] ,[VMPPIN] ,[VMPPOU],[VMTCC1] ,[VMTEL1],[Fax_country_code] ,[Fax_no])";

                //SKU Item
                //sSQL = "INSERT INTO [UP_SKU] " +
                //       "([Direct_SKU] ,[OD_SKU_NO] ,[DESCRIPTION] ,[Vendor_Code] ,[Warehouse] " +
                //       ",[Date] ,[Qty_Sold_Yesterday],[Qty_On_Hand],[qty_on_backorder],[Balance] " +
                //       ",[Item_Val],[Valuated_Stock],[Currency],[Vendor_no],[Vendor_Name] ,[subvndr] " +
                //       ",[Product_Lead_time],[Leadtime_Variance]" +
                //       ",[UOM]" +
                //       ",[Buyer_no]" +
                //       ",[Item_Category]" +
                //       ",[Item_classification]" +
                //       ",[New_Item]" +
                //       ",[ICASUN]" +
                //       ",[ILAYUN]" +
                //       ",[IPALUN]" +
                //       ",[IMINQT]" +
                //       ",[IBMULT]" +
                //       ",[Category])";

                //PO 
                sSQL = "INSERT INTO [UP_PurchaseOrderDetails]" +
                       "([Purchase_order] " +
                       ",[Line_No]" +
                       ",[Warehouse]" +
                       ",[Vendor_No]" +
                       ",[subvndr]" +
                       ",[Direct_code]" +
                       ",[OD_Code]" +
                       ",[Vendor_Code]" +
                       ",[Product_description]" +
                       ",[UOM]" +
                       ",[Original_quantity]" +
                       ",[Outstanding_Qty]" +
                       ",[PO_cost]" +
                       ",[Order_raised]" +
                       ",[Original_due_date]" +
                       ",[Expected_date]" +
                       ",[Buyer_no]" +
                       ",[Item_classification]" +
                       ",[Item_type]" +
                       ",[Item_Category]" +
                       ",[Other])";


                sValue = string.Empty;

                char[] delimiters = new char[] { '|' };
                string[] parts = line.Split(delimiters); // ;;, StringSplitOptions.RemoveEmptyEntries);
                for (int i = 0; i < parts.Length; i++)
                {
                    reader.Read();
                    string dType = reader[0].ToString();

                    string s = parts[i].ToString().Replace("'", "`");

                    if (sValue == string.Empty)
                        sValue = sValue + "'" + s + "'";
                    else
                        sValue = sValue + ",'" + s + "'";

                }

                sSQL = sSQL + " Values (" + sValue + ")";
                SqlCommand cmdIns = new SqlCommand(sSQL, sqlCon);
                //cmdIns.ExecuteNonQuery();

            }

            file.Close();
        }
        // Suspend the screen.
        //Console.ReadLine();
    }

    public void ReadSKU()
    {
        string line;
        string sSQL = string.Empty;
        string sValue = string.Empty;
        string datatype = string.Empty;

        SqlConnection sqlCon = new SqlConnection("Data Source=172.29.9.3;Initial Catalog=OD-VendorsInPartnership;Persist Security Info=True;User ID=officedepot;Password=officedepot");
        sqlCon.Open();

        string sSQLType = "SELECT t.name AS type_name   " +
                                   "FROM sys.columns AS c " +
                                   "JOIN sys.types AS t ON c.user_type_id=t.user_type_id " +
                                   "WHERE c.object_id = OBJECT_ID('UP_SKU')" +
                                   "ORDER BY c.column_id";

        SqlCommand sqlComm = new SqlCommand(sSQLType, sqlCon);
        SqlDataReader reader = sqlComm.ExecuteReader();

        ArrayList al = new ArrayList();

        while (reader.Read())
        {
            object[] values = new object[reader.FieldCount];
            reader.GetValues(values);
            al.Add(values);
        }

        reader.Close();

        // Read the file and display it line by line.
        using (StreamReader file = new StreamReader(@"D:\Projects\OfficeDepot\DATA FILES\UK_item_output.txt"))
        {
            file.ReadLine();
            while ((line = file.ReadLine()) != null)
            {

                //SKU Item
                sSQL = "INSERT INTO [UP_SKU] " +
                       "([Direct_SKU] ,[OD_SKU_NO] ,[DESCRIPTION] ,[Vendor_Code] ,[Warehouse] " +
                       ",[Date] ,[Qty_Sold_Yesterday],[Qty_On_Hand],[qty_on_backorder],[Balance] " +
                       ",[Item_Val],[Valuated_Stock],[Currency],[Vendor_no],[Vendor_Name] ,[subvndr] " +
                       ",[Product_Lead_time],[Leadtime_Variance]" +
                       ",[UOM]" +
                       ",[Buyer_no]" +
                       ",[Item_Category]" +
                       ",[Item_classification]" +
                       ",[New_Item]" +
                       ",[ICASUN]" +
                       ",[ILAYUN]" +
                       ",[IPALUN]" +
                       ",[IMINQT]" +
                       ",[IBMULT]" +
                       ",[Category]) values (";




                sValue = string.Empty;

                char[] delimiters = new char[] { '|' };
                string[] parts = line.Split(delimiters); // ;;, StringSplitOptions.RemoveEmptyEntries);
                for (int i = 0; i < parts.Length; i++)
                {
                    reader.Read();
                    string dType = reader[0].ToString();

                    string s = parts[i].ToString().Replace("'", "`");

                    if (sValue == string.Empty)
                        sValue = sValue + "'" + s + "'";
                    else
                        sValue = sValue + ",'" + s + "'";




                }

                sSQL = sSQL + " Values (" + sValue + ")";
                SqlCommand cmdIns = new SqlCommand(sSQL, sqlCon);
                //cmdIns.ExecuteNonQuery();

            }

            file.Close();
        }
        // Suspend the screen.
        //Console.ReadLine();
    }

    public void GetFirstSheetFirstCellData(string TableName, string excelFile)
    {
        //Open the connection to the Excel file
        String connectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=\"" + excelFile + "\";Extended Properties=\"Excel 8.0;HDR=Yes;\"";
        OleDbConnection conn = new OleDbConnection(connectionString);
        conn.Open();

        //Find out the Sheet & Range names
        DataTable xlsSchema = conn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
        string firstSheetName = (string)xlsSchema.Rows[0]["TABLE_NAME"];

        //Build the SQL command and open the data reader
        String commandText = "Select * From [" + firstSheetName + "]";
        OleDbCommand select = new OleDbCommand(commandText, conn);
        OleDbDataReader reader = select.ExecuteReader();

        //Get the name and type of the first column
        //DataTable sheetSchema = reader.GetSchemaTable();
        //String strReturn = "";
        //foreach (DataRow dr in sheetSchema.Rows)
        //{
        //    strReturn += " ColumnName = " + (string)sheetSchema.Rows[0]["ColumnName"];
        //    strReturn += " ColumnDataType = " + sheetSchema.Rows[0]["DataType"].ToString();
        //}

        string sSQL = string.Empty;
        string sValue = string.Empty;
        string datatype = string.Empty;

        SqlConnection sqlCon = new SqlConnection("Data Source=172.29.9.117;Initial Catalog=OfficeDepot;Persist Security Info=True;User ID=sa;Password=spice");
        sqlCon.Open();

        if (TableName == "BuyerDetails")
        {
            while (reader.Read())
            {
                sSQL = "Insert into BuyerDetails(Buyer_Name,Buyer_Number,Contact_No,Email_address,Fax_No) ";
                sValue = string.Empty;
                for (Int32 i32Column = 0; i32Column < reader.FieldCount; i32Column++)
                {
                    if (sValue == string.Empty)
                        sValue = sValue + "'" + reader[i32Column].ToString().Replace("'", "`") + "'";
                    else
                        sValue = sValue + ",'" + reader[i32Column].ToString().Replace("'", "`") + "'";
                }
                sSQL = sSQL + " Values (" + sValue + ")";
                SqlCommand cmdIns = new SqlCommand(sSQL, sqlCon);
                cmdIns.ExecuteNonQuery();
            }
        }
        else if (TableName == "PurchaseOrderDetails")
        {
            while (reader.Read())
            {
                sSQL = "INSERT INTO [PurchaseOrderDetails] ([Purchase_order] ,[PO_line],[Warehouse],[Vendor_No],[Direct_code],[OD_Code],[Vendor_Code],[Product_description],[UOM],[Original_quantity],[Outstanding_Qty],[PO_cost],[Order_raised],[Original_due_date],[Expected_date],[Buyer_no],[Item_classification],[Item_type],[Item_Category],[Other]) ";
                sValue = string.Empty;
                for (Int32 i32Column = 0; i32Column < reader.FieldCount; i32Column++)
                {
                    if (sValue == string.Empty)
                        sValue = sValue + "'" + reader[i32Column].ToString().Replace("'", "`") + "'";
                    else
                        sValue = sValue + ",'" + reader[i32Column].ToString().Replace("'", "`") + "'";
                }
                sSQL = sSQL + " Values (" + sValue + ")";
                SqlCommand cmdIns = new SqlCommand(sSQL, sqlCon);
                cmdIns.ExecuteNonQuery();
            }
        }
        else if (TableName == "ReceiptInformation")
        {
            while (reader.Read())
            {
                sSQL = "INSERT INTO [ReceiptInformation] ([Viking_sku_no],[OD_SKU_Number],[Vendor_prod_code],[Warehouse],[PO_number],[Qty_receipted],[Other])";
                sValue = string.Empty;
                for (Int32 i32Column = 0; i32Column < reader.FieldCount; i32Column++)
                {
                    if (sValue == string.Empty)
                        sValue = sValue + "'" + reader[i32Column].ToString().Replace("'", "`") + "'";
                    else
                        sValue = sValue + ",'" + reader[i32Column].ToString().Replace("'", "`") + "'";
                }
                sSQL = sSQL + " Values (" + sValue + ")";
                SqlCommand cmdIns = new SqlCommand(sSQL, sqlCon);
                cmdIns.ExecuteNonQuery();
            }
        }
        else if (TableName == "SKUStockDetail")
        {
            while (reader.Read())
            {
                sSQL = "INSERT INTO [SKUStockDetail]([Direct_SKU] ,[OD_SKU_NO] ,[Vendor_Code]  ,[Warehouse]  ,[date],[Qty_sold_yesterday]           ,[qty_on_hand]           ,[qty_on_backorder]           ,[Item_Val_GBP]           ,[Valuated_Stock]           ,[Currency]           ,[Vendor_no]           ,[Vendor_Name]           ,[Product_Lead_time]           ,[UOM]           ,[Buyer_no]           ,[Item_Category]           ,[Item_classification])";
                sValue = string.Empty;
                for (Int32 i32Column = 0; i32Column < reader.FieldCount; i32Column++)
                {
                    if (sValue == string.Empty)
                        sValue = sValue + "'" + reader[i32Column].ToString().Replace("'", "`") + "'";
                    else
                        sValue = sValue + ",'" + reader[i32Column].ToString().Replace("'", "`") + "'";
                }
                sSQL = sSQL + " Values (" + sValue + ")";
                SqlCommand cmdIns = new SqlCommand(sSQL, sqlCon);
                cmdIns.ExecuteNonQuery();
            }
        }
        else if (TableName == "VendorDetail")
        {
            while (reader.Read())
            {
                sSQL = "INSERT INTO [VendorDetail]([Vendor_No],[Vendor_Name],[Vendor Contact Name] ,[Vendor Contact Number],[Vendor Contact Fax]           ,[Vendor Contact Email]           ,[VMPAD1]           ,[VMPAD2]           ,[VMPCIT]           ,[VMPCNT]           ,[VMPPIN]           ,[VMPPOU]           ,[VMTCC1]           ,[VMTEL1]           ,[Fax_country_code]           ,[Fax_no])";
                sValue = string.Empty;
                for (Int32 i32Column = 0; i32Column < reader.FieldCount; i32Column++)
                {
                    if (sValue == string.Empty)
                        sValue = sValue + "'" + reader[i32Column].ToString().Replace("'", "`") + "'";
                    else
                        sValue = sValue + ",'" + reader[i32Column].ToString().Replace("'", "`") + "'";
                }
                sSQL = sSQL + " Values (" + sValue + ")";
                SqlCommand cmdIns = new SqlCommand(sSQL, sqlCon);
                cmdIns.ExecuteNonQuery();
            }
        }



        #region commented
        //If there are any rows returned, read the first cell value
        //string firstCellValue = null;
        //if (reader.HasRows)
        //{
        //    reader.Read();
        //    firstCellValue = reader.GetValue(0).ToString();
        //}
        //else
        //    firstCellValue = "No Rows Returned";

        //Close and Dispose
        #endregion

        xlsSchema.Dispose();
        //sheetSchema.Dispose();
        reader.Close();
        //  cmd.Dispose();
        conn.Close();
        conn.Dispose();



        #region commented
        //return firstSheetName + ": " + firstColumnName + " = " + firstCellValue + " (" + firstColumnDataType + ")";
        #endregion
    }

    /// -------------- Added on 16 Feb 2013 -------------------------- START ---

    /// <summary>
    /// 
    /// </summary>
    /// <param name="ColumnName"></param>
    /// <param name="Description"></param>
    /// <param name="ColumnLength"></param>
    /// <param name="FileName"></param>
    /// <returns></returns>
    public string GetErrorMessage(string ColumnName, string Description, int ColumnLength)
    {
        var Message = ColumnName + " length greater than " + ColumnLength + " for " + Description + ",";
        return Message;
    }

    /// <summary>
    /// Gets the Date of a File to be Uploaded.  
    /// </summary>
    /// <param name="Filename">string</param>
    /// <returns>DateTime</returns>
    public DateTime GetFileDate(string Filename)
    {
        string[] File = Filename.Split('.');
        string[] strArray = File[0].Split('_');

        var Date = new DateTime(
            Convert.ToInt32(strArray[strArray.Length - 2].Substring(4, 4).ToString()),
            Convert.ToInt32(strArray[strArray.Length - 2].Substring(2, 2).ToString()),
            Convert.ToInt32(strArray[strArray.Length - 2].Substring(0, 2).ToString()),
            Convert.ToInt32(strArray[strArray.Length - 1].Substring(0, 2).ToString()),
            Convert.ToInt32(strArray[strArray.Length - 1].Substring(2, 2).ToString()),
            0);

        return Date;
    }

    /// <summary>
    /// Gets the Date of a RMS File to be Uploaded.  
    /// </summary>
    /// <param name="Filename">string</param>
    /// <returns>DateTime</returns>
    public DateTime GetRMSFileDate(string Filename)
    {
        string[] File = Filename.Split('.');
        string[] strArray = File[0].Split('_');

        var Date = new DateTime(
            Convert.ToInt32(strArray[1].Substring(4, 4).ToString()),
            Convert.ToInt32(strArray[1].Substring(2, 2).ToString()),
            Convert.ToInt32(strArray[1].Substring(0, 2).ToString()),
           0, 0, 0);

        return Date;
    }

    /// <summary>
    /// Check To Be Uploaded File Date Is Greater
    /// </summary>
    /// <returns>bool</returns>
    public static bool CheckToBeUploadedFileDateIsGreater(string Filename)
    {
        bool Flag = false;

        UP_ManualFileUploadBE oUP_ManualFileUploadBE = new UP_ManualFileUploadBE();

        oUP_ManualFileUploadBE.Action = "GetManualUploadData";
        UP_ManualFileUploadBAL oUP_ManualFileUploadBAL = new UP_ManualFileUploadBAL();
        List<UP_ManualFileUploadBE> UP_ManualFileUploadBEList = new List<UP_ManualFileUploadBE>();
        UP_ManualFileUploadBEList = oUP_ManualFileUploadBAL.GetManualUploadBAL(oUP_ManualFileUploadBE);

        var fileNameToCheck = Filename.Substring(0, Filename.Length - 14);
        string[] strArray = Filename.Split('_');
        var CountryName = strArray[0].ToString();
        var FileType = strArray[1].ToString();

        var Date = new DateTime(
             Convert.ToInt32(strArray[strArray.Length - 2].Substring(4, 4).ToString()),
             Convert.ToInt32(strArray[strArray.Length - 2].Substring(2, 2).ToString()),
             Convert.ToInt32(strArray[strArray.Length - 2].Substring(0, 2).ToString()),
             Convert.ToInt32(strArray[strArray.Length - 1].Substring(0, 2).ToString()),
             Convert.ToInt32(strArray[strArray.Length - 1].Substring(2, 2).ToString()),
             0);

        List<UP_ManualFileUploadBE> objList = new List<UP_ManualFileUploadBE>();

        foreach (UP_ManualFileUploadBE obj in UP_ManualFileUploadBEList)
        {
            if (obj.DownloadedFilename.ToLower().Trim().Contains(fileNameToCheck.ToLower().Trim()))
            {
                objList.Add(obj);
            }
        }

        int result = 0;
        if (objList.Count > 0)
        {
            var maxdate = DateTime.Parse(objList.Max(obj => obj.DateUploaded).ToString());
            result = DateTime.Compare(Date, maxdate);
        }
        else
            result = 1;

        if ((result > 0))
        {
            Flag = true;
        }

        return Flag;
    }

    /// -------------- Added on 16 Feb 2013 -------------------------- END ---
    /// 



    /// <summary>
    /// Check To Be Uploaded File Date Is Greater
    /// </summary>
    /// <returns>bool</returns>
    public static bool CheckToBeRMSFileDateIsGreater(string Filename)
    {
        bool Flag = false;

        UP_ManualFileUploadBE oUP_ManualFileUploadBE = new UP_ManualFileUploadBE();

        oUP_ManualFileUploadBE.Action = "GetRMSUploadData";
        UP_ManualFileUploadBAL oUP_ManualFileUploadBAL = new UP_ManualFileUploadBAL();
        List<UP_ManualFileUploadBE> UP_ManualFileUploadBEList = new List<UP_ManualFileUploadBE>();
        UP_ManualFileUploadBEList = oUP_ManualFileUploadBAL.GetRMSUploadBAL(oUP_ManualFileUploadBE);

        string[] strArray = Filename.Split('_');
        string CountryName = strArray[0].ToString();


        var Date = new DateTime(
             Convert.ToInt32(strArray[2].Substring(4, 4).ToString()),
             Convert.ToInt32(strArray[2].Substring(2, 2).ToString()),
             Convert.ToInt32(strArray[2].Substring(0, 2).ToString()),
             Convert.ToInt32(strArray[3].Substring(0, 2).ToString()),
             Convert.ToInt32(strArray[3].Substring(2, 2).ToString()),
             0);

        //List<UP_ManualFileUploadBE> objList = new List<UP_ManualFileUploadBE>();

        UP_ManualFileUploadBEList = UP_ManualFileUploadBEList.Where(r => r.DownloadedFilename.Contains(CountryName)).ToList();

        int result = 0;
        if (UP_ManualFileUploadBEList.Count > 0)
        {

            var maxdate = DateTime.Parse(UP_ManualFileUploadBEList.Max(obj => obj.DateUploaded).ToString());
            result = DateTime.Compare(Date, maxdate);

        }
        else
            result = 1;


        if ((result > 0))
        {
            Flag = true;
        }

        return Flag;
    }
}
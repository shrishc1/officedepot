﻿using System;
using System.Text;

using Utilities;
using WebUtilities;

/// <summary>
/// Summary description for WorkflowHTML
/// </summary>

public class WorkflowHTML
{
    public WorkflowHTML()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    #region First Block HTML Functions

    /// <summary>
    /// Generate HTML for date, user id, action, po number, 
    /// debit note number, coll date, carrier, coll auth number, 
    /// GoodsCollectedOn, CarriageCharge, credit note number and comments
    /// </summary>
    /// <param name="pRole"></param>
    /// <param name="pDate"></param>
    /// <param name="pCollectionDate"></param>
    /// <param name="pGoodsCollectedOn"></param>
    /// <param name="pColor"></param>
    /// <param name="pAction"></param>
    /// <param name="pUserId"></param>
    /// <param name="pComments"></param>
    /// <param name="pPurchaseOrderNumber"></param>
    /// <param name="pDebitNoteNumber"></param>
    /// <param name="pCarrier"></param>
    /// <param name="pCollAuthNumber"></param>
    /// <param name="pCarriageCharge"></param>
    /// <param name="pCreditNoteNumber"></param>
    /// <returns></returns>

    public string function1(//string pRole,
                            DateTime pDate,
                            DateTime? pCollectionDate,
                            DateTime? pGoodsCollectedOn,
                            string pColor,
                            string pAction = "",
                            string pAction1 = "",
                            string pUserName = "",
                            string pComments = "",
                            string pPurchaseOrderNumber = "",
                            string pDebitNoteNumber = "",
                            string pCarrier = "",
                            string pCollAuthNumber = "",
                            string pCarriageCharge = "",
                            string pCreditNoteNumber = "",
                            string pReason = "",
                            string pActionTaken = "",
                            string pCost = "",
                            string pLabourCost = "",
                            string pAdminCost = "",
                            string pPallets = "",
                            string pPackSizeOrdered = "",
                            string pPackSizeReceived = "",
                            string pNoEscalationOnFromPage = "",
                            string pNoEscalationOn = "",
                            string pAmount = "",
                            int? pViewCommLink = null,
                            string pType = "",
                            string pOversReason = "",
                            string pTrackingNumber = "",
                            string pCartons = "",
                            string pCostCenter = "",
                            DateTime? pCollectionDateItemNotOnPo = null,
                            string pBottomComments = ""

        )
    {

        string sDate = WebCommon.getGlobalResourceValue("Date");
        string sUserName = WebCommon.getGlobalResourceValue("UserName");
        string sAction = WebCommon.getGlobalResourceValue("Action");
        string sComments = WebCommon.getGlobalResourceValue("Comments");
        string sPurchaseOrderNumber = WebCommon.getGlobalResourceValue("PurchaseOrderNumber");
        string sDebitNoteNumber = WebCommon.getGlobalResourceValue("DebitNoteNumber");
        string sCollectionDate = WebCommon.getGlobalResourceValue("CollectionDate");
        string sCollectionAuthorisationNumber = WebCommon.getGlobalResourceValue("CollectionAuthorisationNumber");
        string sCarrier = WebCommon.getGlobalResourceValue("Carrier");
        string sGoodsCollectedOn = WebCommon.getGlobalResourceValue("GoodsCollectedOn");
        string sCarriageCharge = WebCommon.getGlobalResourceValue("CarriageCharge");
        string sCreditNoteNumber = WebCommon.getGlobalResourceValue("CreditNoteNumber");
        string sReason = WebCommon.getGlobalResourceValue("Reason");
        string sActionTaken = WebCommon.getGlobalResourceValue("ActionTaken");
        string sCost = WebCommon.getGlobalResourceValue("Cost");
        string sLabourCost = WebCommon.getGlobalResourceValue("LabourCost");
        string sAdminCost = WebCommon.getGlobalResourceValue("AdminCost");
        string sNoOfPallets = "#" + WebCommon.getGlobalResourceValue("ActualPallets");
        string sPackSizeOrdered = WebCommon.getGlobalResourceValue("PackSizeOrdered");
        string sPackSizeReceived = WebCommon.getGlobalResourceValue("PackSizeReceived");
        string sNoEscalationOn = WebCommon.getGlobalResourceValue("NoEscalationOn");
        string sAmount = WebCommon.getGlobalResourceValue("Amount");
        string sType = WebCommon.getGlobalResourceValue("Type");
        string sOversReason = WebCommon.getGlobalResourceValue("OversReason");
        string sNoOfCartons = "#" + WebCommon.getGlobalResourceValue("ActualCartons");
        string sTrackingNumber = WebCommon.getGlobalResourceValue("TrackingNumber");

        string sCostCenter = WebCommon.getGlobalResourceValue("CostCenter");

        string sCollectionDateItemNotOnPo = WebCommon.getGlobalResourceValue("CollectionDate");


        StringBuilder sHTML = new StringBuilder();

        if (pColor.ToLower() == "yellow")
        {
            pColor = "f9eec4";
            sHTML.Append("<table class='yellow-bg'>");
        }

        if (pColor.ToLower() == "blue")
        {
            pColor = "EAF2FC";
            sHTML.Append("<table class='blue-bg'>");
        }
        if (pColor.ToLower() == "white")
        {
            sHTML.Append("<table  background-color:#" + pColor + "'>");
        }
        if (pColor.ToLower() == "red")
        {
            pColor = "FF3300";
            sHTML.Append("<table class='red-bg'>");
            //sHTML.Append("<table style='background-color:#" + pColor + "; color:White; width:100%;'>");
        }
        if (pColor.ToLower() == "brown")
        {
            pColor = "#f6bc81";
            sHTML.Append("<table class='brown-bg'>");
        }

        if (pDate != null)
        {
            sHTML.Append("<tr>");
            sHTML.Append("<td style='font-style;font-weight:bold'>" + sDate + "&nbsp;&nbsp;</td>");
            sHTML.Append("<td>:</td>");
            sHTML.Append("<td>" + pDate.ToString("dd/MM/yyyy") + "</td>");
            sHTML.Append("</tr>");
        }
        if (pReason != "")
        {
            sHTML.Append("<tr>");
            sHTML.Append("<td style='font-style;font-weight:bold'>" + sReason + "&nbsp;&nbsp;</td>");
            sHTML.Append("<td>:</td>");
            sHTML.Append("<td>" + pReason + "</td>");
            sHTML.Append("</tr>");
        }

        if (pUserName != "")
        {
            sHTML.Append("<tr>");
            sHTML.Append("<td style='font-style;font-weight:bold'>" + sUserName + "</td>");
            sHTML.Append("<td>:</td>");
            sHTML.Append("<td>" + pUserName + "</td>");
            sHTML.Append("</tr>");
        }

        if (pOversReason != "")
        {
            sHTML.Append("<tr>");
            sHTML.Append("<td style='font-style;font-weight:bold'>" + sOversReason + "&nbsp;&nbsp;</td>");
            sHTML.Append("<td>:</td>");
            sHTML.Append("<td>" + pOversReason + "</td>");
            sHTML.Append("</tr>");
        }

        if (pAction1 != "")
        {
            sHTML.Append("<tr>");
            sHTML.Append("<td style='font-style;font-weight:bold'>" + sAction + "&nbsp;&nbsp;</td>");
            sHTML.Append("<td>:</td>");
            sHTML.Append("<td>" + pAction1 + "</td>");
            sHTML.Append("</tr>");
        }
        if (pDebitNoteNumber != "")
        {
            sHTML.Append("<tr>");
            sHTML.Append("<td style='font-style;font-weight:bold'>" + sDebitNoteNumber + "&nbsp;&nbsp;</td>");
            sHTML.Append("<td>:</td>");
            sHTML.Append("<td>" + pDebitNoteNumber + "</td>");
            sHTML.Append("</tr>");
        }

        if (pPurchaseOrderNumber != "")
        {
            sHTML.Append("<tr>");
            sHTML.Append("<td style='font-style;font-weight:bold'>" + sPurchaseOrderNumber + "&nbsp;&nbsp;</td>");
            sHTML.Append("<td>:</td>");
            sHTML.Append("<td>" + pPurchaseOrderNumber + "</td>");
            sHTML.Append("</tr>");
        }

        if (pCollectionDate != null)
        {
            sHTML.Append("<tr>");
            sHTML.Append("<td style='font-style;font-weight:bold'>" + sCollectionDate + "&nbsp;&nbsp;</td>");
            sHTML.Append("<td>:</td>");
            sHTML.Append("<td>" + Convert.ToDateTime(pCollectionDate).ToString("dd/MM/yyyy") + "</td>");
            sHTML.Append("</tr>");
        }

        if (pCarrier != "")
        {
            sHTML.Append("<tr>");
            sHTML.Append("<td style='font-style;font-weight:bold'>" + sCarrier + "&nbsp;&nbsp;</td>");
            sHTML.Append("<td>:</td>");
            sHTML.Append("<td>" + pCarrier + "</td>");
            sHTML.Append("</tr>");
        }

        if (pCollectionDateItemNotOnPo != null)
        {
            sHTML.Append("<tr>");
            sHTML.Append("<td style='font-style;font-weight:bold'>" + sCollectionDateItemNotOnPo + "&nbsp;&nbsp;</td>");
            sHTML.Append("<td>:</td>");
            sHTML.Append("<td>" + Convert.ToDateTime(pCollectionDateItemNotOnPo).ToString("dd/MM/yyyy") + "</td>");
            sHTML.Append("</tr>");
        }
        if (pTrackingNumber != "")
        {
            sHTML.Append("<tr>");
            sHTML.Append("<td style='font-style;font-weight:bold'>" + sTrackingNumber + "&nbsp;&nbsp;</td>");
            sHTML.Append("<td>:</td>");
            sHTML.Append("<td>" + pTrackingNumber + "</td>");
            sHTML.Append("</tr>");
        }

        if (pCollAuthNumber != "")
        {
            sHTML.Append("<tr>");
            sHTML.Append("<td style='font-style;font-weight:bold'>" + sCollectionAuthorisationNumber + "&nbsp;&nbsp;</td>");
            sHTML.Append("<td>:</td>");
            sHTML.Append("<td>" + pCollAuthNumber + "</td>");
            sHTML.Append("</tr>");
        }

        if (pGoodsCollectedOn != null)
        {
            sHTML.Append("<tr>");
            sHTML.Append("<td style='font-style;font-weight:bold'>" + sGoodsCollectedOn + "&nbsp;&nbsp;</td>");
            sHTML.Append("<td>:</td>");
            sHTML.Append("<td>" + Convert.ToDateTime(pGoodsCollectedOn).ToString("dd/MM/yyyy") + "</td>");
            sHTML.Append("</tr>");
        }

        if (pCarriageCharge != "")
        {
            sHTML.Append("<tr>");
            sHTML.Append("<td style='font-style;font-weight:bold'>" + sCarriageCharge + "&nbsp;&nbsp;</td>");
            sHTML.Append("<td>:</td>");
            sHTML.Append("<td>" + pCarriageCharge + "</td>");
            sHTML.Append("</tr>");
        }

        if (pCreditNoteNumber != "")
        {
            sHTML.Append("<tr>");
            sHTML.Append("<td style='font-style;font-weight:bold'>" + sCreditNoteNumber + "&nbsp;&nbsp;</td>");
            sHTML.Append("<td>:</td>");
            sHTML.Append("<td>" + pCreditNoteNumber + "</td>");
            sHTML.Append("</tr>");
        }

        if (pActionTaken != "")
        {
            sHTML.Append("<tr>");
            sHTML.Append("<td style='font-style;font-weight:bold'>" + sActionTaken + "&nbsp;&nbsp;</td>");
            sHTML.Append("<td>:</td>");
            sHTML.Append("<td>" + pActionTaken + "</td>");
            sHTML.Append("</tr>");
        }

        if (pPallets != "")
        {
            sHTML.Append("<tr>");
            sHTML.Append("<td style='font-style;font-weight:bold'>" + sNoOfPallets + "&nbsp;&nbsp;</td>");
            sHTML.Append("<td>:</td>");
            sHTML.Append("<td>" + pPallets + "</td>");
            sHTML.Append("</tr>");
        }

        if (pCartons != "")
        {
            sHTML.Append("<tr>");
            sHTML.Append("<td style='font-style;font-weight:bold'>" + sNoOfCartons + "&nbsp;&nbsp;</td>");
            sHTML.Append("<td>:</td>");
            sHTML.Append("<td>" + pCartons + "</td>");
            sHTML.Append("</tr>");
        }

        if (pCost != "")
        {
            sHTML.Append("<tr>");
            sHTML.Append("<td style='font-style;font-weight:bold'>" + sCost + "&nbsp;&nbsp;</td>");
            sHTML.Append("<td>:</td>");
            sHTML.Append("<td>" + pCost + "</td>");
            sHTML.Append("</tr>");
        }

        if (pLabourCost != "")
        {
            sHTML.Append("<tr>");
            sHTML.Append("<td style='font-style;font-weight:bold'>" + sLabourCost + "&nbsp;&nbsp;</td>");
            sHTML.Append("<td>:</td>");
            sHTML.Append("<td>" + pLabourCost + "</td>");
            sHTML.Append("</tr>");
        }

        if (pAdminCost != "")
        {
            sHTML.Append("<tr>");
            sHTML.Append("<td style='font-style;font-weight:bold'>" + sAdminCost + "&nbsp;&nbsp;</td>");
            sHTML.Append("<td>:</td>");
            sHTML.Append("<td>" + pAdminCost + "</td>");
            sHTML.Append("</tr>");
        }

        if (pPackSizeOrdered != "")
        {
            sHTML.Append("<tr>");
            sHTML.Append("<td style='font-style;font-weight:bold'>" + sPackSizeOrdered + "&nbsp;&nbsp;</td>");
            sHTML.Append("<td>:</td>");
            sHTML.Append("<td>" + pPackSizeOrdered + "</td>");
            sHTML.Append("</tr>");
        }

        if (pPackSizeReceived != "")
        {
            sHTML.Append("<tr>");
            sHTML.Append("<td style='font-style;font-weight:bold'>" + sPackSizeReceived + "&nbsp;&nbsp;</td>");
            sHTML.Append("<td>:</td>");
            sHTML.Append("<td>" + pPackSizeReceived + "</td>");
            sHTML.Append("</tr>");
        }
        if (pNoEscalationOnFromPage != "")
        {
            sHTML.Append("<tr>");
            sHTML.Append("<td style='font-style;font-weight:bold'>" + sNoEscalationOn + "&nbsp;&nbsp;</td>");
            sHTML.Append("<td>:</td>");
            sHTML.Append("<td>" + pNoEscalationOn + "</td>");
            sHTML.Append("</tr>");
        }

        if (pAmount != "")
        {
            sHTML.Append("<tr>");
            sHTML.Append("<td style='font-style;font-weight:bold'>" + sAmount + "&nbsp;&nbsp;</td>");
            sHTML.Append("<td>:</td>");
            sHTML.Append("<td>" + pAmount + "</td>");
            sHTML.Append("</tr>");
        }

        if (pType != "")
        {
            sHTML.Append("<tr>");
            sHTML.Append("<td style='font-style;font-weight:bold'>" + sType + "&nbsp;&nbsp;</td>");
            sHTML.Append("<td>:</td>");
            sHTML.Append("<td>" + pType + "</td>");
            sHTML.Append("</tr>");
        }

        if (pCostCenter != "")
        {
            sHTML.Append("<tr>");
            sHTML.Append("<td style='font-style;font-weight:bold'>" + sCostCenter + "&nbsp;&nbsp;</td>");
            sHTML.Append("<td>:</td>");
            sHTML.Append("<td>" + pCostCenter + "</td>");
            sHTML.Append("</tr>");
        }

        if (pComments != "")
        {
            sHTML.Append("<tr>");
            sHTML.Append("<td  style='font-style;font-weight:bold'>" + sComments + "&nbsp;&nbsp;</td>");
            sHTML.Append("<td colspan='2'>:</td>");
            sHTML.Append("</tr>");

            sHTML.Append("<tr>");
            sHTML.Append("<td colspan='3' style='font-style:italic;font-weight:bold'>" + pComments + "</td>");
            sHTML.Append("</tr>");
        }

        if (pBottomComments != "")
        {
            sHTML.Append("<tr>");
            sHTML.Append("<td colspan='3' align='left' style='font-style:italic;font-weight:bold'>" + pBottomComments + "</td>");
            sHTML.Append("</tr>");
        }

        if (pViewCommLink.HasValue)
        {
            sHTML.Append("<tr>");

            sHTML.Append("<td align='right' colspan='3'>" +
               "<a href='" + Common.EncryptQuery("Dis_ReturnNote.aspx?ReturnnoteID=" + pViewCommLink.Value) +
               "' onclick='window.open(this.href, this.target,\"scrollbars=1,left=100px,top=100px,width=1000px,height=800px\"); return false;'>View Return Note</a>&nbsp;&nbsp;</td>");

            sHTML.Append("</tr>");
        }

        //sHTML.Append("<br/><br/>");
        sHTML.Append("</table>");

        return sHTML.ToString();
    }

    /// <summary>
    /// Generate HTML for date, sent to,top comments, bottom comments, view communication, 
    /// collection date, carrier, collection authorisation number, action and comments
    /// </summary>
    /// <param name="pRole"></param>
    /// <param name="pDate"></param>
    /// <param name="pCollectionDate"></param>
    /// <param name="pColor"></param>
    /// <param name="pSentTo"></param>
    /// <param name="pTopComments"></param>
    /// <param name="pBottomComments"></param>
    /// <param name="pAction"></param>
    /// <param name="pViewCommNumber"></param>
    /// <param name="pViewCommLink"></param>
    /// <param name="pCarrier"></param>
    /// <param name="pCollAuthNumber"></param>
    /// <returns></returns>
    public string function2(//string pRole,
                            DateTime pDate,
                            DateTime? pCollectionDate,
                            string pColor,
                            string pSentTo = "",
                            string pTopComments = "",
                            string pBottomComments = "",
                            string pAction = "",
                            string pAction1 = "",
                            string pViewCommNumber = "",
                            string pViewCommLink = "",
                            string pCarrier = "",
                            string pCollAuthNumber = "",
                            string pComments = "",
                            string pFromPage = "")
    {

        string sDate = WebCommon.getGlobalResourceValue("Date");
        string sSentTo = WebCommon.getGlobalResourceValue("SentTo");
        string sActionRequired = WebCommon.getGlobalResourceValue("ActionRequired");
        string sViewCommunication = WebCommon.getGlobalResourceValue("ViewCommunication");
        string sCollectionDate = WebCommon.getGlobalResourceValue("CollectionDate");
        string sCollectionAuthorisationNumber = WebCommon.getGlobalResourceValue("CollectionAuthorisationNumber");
        string sCarrier = WebCommon.getGlobalResourceValue("Carrier");
        string sComments = WebCommon.getGlobalResourceValue("Comments");


        StringBuilder sHTML = new StringBuilder();



        if (pColor.ToLower() == "yellow")
        {
            pColor = "f9eec4";
            sHTML.Append("<table class='yellow-bg'>");
        }

        if (pColor.ToLower() == "blue")
        {
            pColor = "EAF2FC";
            sHTML.Append("<table class='blue-bg'>");
        }

        if (pColor.ToLower() == "red")
        {
            pColor = "FF3300";
            sHTML.Append("<table class='red-bg'>");
            //sHTML.Append("<table style='background-color:#" + pColor + "; color:White; width:100%;'>");
        }
        //sHTML.Append("<table style='Width:100%; background-color:#" + pColor + "'>");

        /*if (pAction != "") {
            sHTML.Append("<tr>");
            sHTML.Append("<td colspan='3' align='center'>");
            sHTML.Append("<span style='border-bottom:1px solid'>" + pRole + "</span>\r\n");
            sHTML.Append("<span style='border-bottom:1px solid'>" + pAction + "</span>");
            sHTML.Append("</td>");
            sHTML.Append("</tr>");
            sHTML.Append("<br/>");
        }
        else {
            sHTML.Append("<tr>");
            sHTML.Append("<td colspan='3' align='center'>");
            sHTML.Append(pRole);
            sHTML.Append("</td>");
            sHTML.Append("</tr>");
        }*/

        if (pTopComments != "")
        {
            sHTML.Append("<tr>");
            sHTML.Append("<td colspan='3' align='left'>");
            sHTML.Append(pTopComments);
            sHTML.Append("</td>");
            sHTML.Append("</tr>");
        }

        if (pFromPage == "")
        {
            sHTML.Append("<tr>");
            sHTML.Append("<td style='font-style;font-weight:bold'>" + sDate + "&nbsp;&nbsp;</td>");
            sHTML.Append("<td>:</td>");
            sHTML.Append("<td>" + pDate.ToString("dd/MM/yyyy") + "</td>");
            sHTML.Append("</tr>");
        }

        if (pSentTo != "")
        {
            sHTML.Append("<tr>");
            sHTML.Append("<td style='font-style;font-weight:bold'>" + sSentTo + "&nbsp;&nbsp;</td>");
            sHTML.Append("<td>:</td>");
            sHTML.Append("<td>");

            //pSentTo = pSentTo.Trim(',');
            //foreach (string str in pSentTo.Split(','))
            //{
            //    sHTML.Append(str + "<br />");
            //}

            sHTML.Append(pSentTo);
            sHTML.Append("</td>");
            sHTML.Append("</tr>");
        }

        if (pCollectionDate != null)
        {
            sHTML.Append("<tr>");
            sHTML.Append("<td style='font-style;font-weight:bold'>" + sCollectionDate + "&nbsp;&nbsp;</td>");
            sHTML.Append("<td>:</td>");
            sHTML.Append("<td>" + Convert.ToDateTime(pCollectionDate).ToString("dd/MM/yyyy") + "</td>");
            sHTML.Append("</tr>");
        }

        if (pCarrier != "")
        {
            sHTML.Append("<tr>");
            sHTML.Append("<td style='font-style;font-weight:bold'>" + sCarrier + "&nbsp;&nbsp;</td>");
            sHTML.Append("<td>:</td>");
            sHTML.Append("<td>" + pCarrier + "</td>");
            sHTML.Append("</tr>");
        }

        if (pCollAuthNumber != "")
        {
            sHTML.Append("<tr>");
            sHTML.Append("<td style='font-style;font-weight:bold'>" + sCollectionAuthorisationNumber + "&nbsp;&nbsp;</td>");
            sHTML.Append("<td>:</td>");
            sHTML.Append("<td>" + pCollAuthNumber + "</td>");
            sHTML.Append("</tr>");
        }
        if (pBottomComments != "")
        {
            sHTML.Append("<tr>");
            sHTML.Append("<td colspan='3' align='left' style='font-style:italic;font-weight:bold'>" + pBottomComments + "</td>");
            sHTML.Append("</tr>");
        }
        if (pComments != "")
        {
            sHTML.Append("<tr>");
            sHTML.Append("<td style='font-style;font-weight:bold'>" + sComments + "&nbsp;&nbsp;</td>");
            sHTML.Append("<td>:</td>");
            sHTML.Append("<td style='font-style:italic;font-weight:bold'>" + pComments + "</td>");
            sHTML.Append("</tr>");
        }
        if (pViewCommLink != "")
        {
            sHTML.Append("<tr>");
            sHTML.Append("<td align='left' colspan='3'>" + pViewCommLink + "&nbsp;&nbsp;</td>");
            sHTML.Append("</tr>");
        }


        sHTML.Append("</table>");

        return sHTML.ToString();
    }

    /// <summary>
    /// Generate HTML without any data just only role 
    /// </summary>
    /// <param name="pRole"></param>
    /// <param name="pAction"></param>
    /// <returns></returns>
    public string function3(//string pRole,
                            string pColor,
                            DateTime? pDate,
                            string pComments = "",
                            string pAction = "")
    {

        string sDate = WebCommon.getGlobalResourceValue("Date");
        string sComments = WebCommon.getGlobalResourceValue("Comments");

        StringBuilder sHTML = new StringBuilder();
        if (pColor.ToLower() == "yellow")
        {
            pColor = "f9eec4";
            sHTML.Append("<table class='yellow-bg'>");
        }

        if (pColor.ToLower() == "blue")
        {
            pColor = "EAF2FC";
            sHTML.Append("<table class='blue-bg'>");
        }

        if (pColor.ToLower() == "red")
        {
            pColor = "FF3300";
            sHTML.Append("<table class='red-bg'>");
            //sHTML.Append("<table style='background-color:#" + pColor + "; color:White; width:100%;'>");
        }

        sHTML.Append("<table style='Width:100%;  background-color:#" + pColor + "'>");

        /*if (pAction != "") {
            sHTML.Append("<tr>");
            sHTML.Append("<td colspan='3' align='center'>");
            sHTML.Append("<span style='border-bottom:1px solid'>" + pRole + "</span>\r\n");
            sHTML.Append("<span style='border-bottom:1px solid'>" + pAction + "</span>");
            sHTML.Append("</td>");
            sHTML.Append("</tr>");
        }
        else {
            sHTML.Append("<tr>");
            sHTML.Append("<td colspan='3' style='border-bottom:1px solid' align='center'>");
            sHTML.Append(pRole);
            sHTML.Append("</td>");
            sHTML.Append("</tr>");
        }*/
        if (pDate != null)
        {
            sHTML.Append("<tr>");
            sHTML.Append("<td style='font-style;font-weight:bold'>" + sDate + "&nbsp;&nbsp;</td>");
            sHTML.Append("<td>:</td>");
            sHTML.Append("<td>" + Convert.ToDateTime(pDate).ToString("dd/MM/yyyy") + "</td>");
            sHTML.Append("</tr>");
        }
        else
        {
            sHTML.Append("<tr>");
            sHTML.Append("<td>&nbsp;</td>");
            sHTML.Append("<td>&nbsp;</td>");
            sHTML.Append("<td>&nbsp;</td>");
            sHTML.Append("</tr>");
        }
        if (pComments != "")
        {
            sHTML.Append("<tr>");
            sHTML.Append("<td style='font-style;font-weight:bold'>" + sComments + "&nbsp;&nbsp;</td>");
            sHTML.Append("<td>:</td>");
            sHTML.Append("<td>" + pComments + "</td>");
            sHTML.Append("</tr>");
        }
        else
        {
            sHTML.Append("<tr>");
            sHTML.Append("<td>&nbsp;</td>");
            sHTML.Append("<td>&nbsp;</td>");
            sHTML.Append("<td>&nbsp;</td>");
            sHTML.Append("</tr>");
        }
        sHTML.Append("<tr>");
        sHTML.Append("<td>&nbsp;</td>");
        sHTML.Append("<td>&nbsp;</td>");
        sHTML.Append("<td>&nbsp;</td>");
        sHTML.Append("</tr>");

        //sHTML.Append("<br/><br/>");
        sHTML.Append("</table>");

        return sHTML.ToString();
    }

    /// <summary>
    /// Generate HTML for Sent on, Sent to, Sent from, 
    /// comments, action and view communication link
    /// </summary>
    /// <param name="pRole"></param>
    /// <param name="pColor"></param>
    /// <param name="pSentOn"></param>
    /// <param name="pSentTo"></param>
    /// <param name="pSentFrom"></param>
    /// <param name="pTopComments"></param>
    /// <param name="pBottomComments"></param>
    /// <param name="pAction"></param>
    /// <param name="pViewCommLink"></param>
    /// <returns></returns>
    public string function4(//string pRole,                                
                            string pColor,
                            string pAction = "",
                            DateTime? pSentOn = null,
                            string pSentTo = "",
                            string pSentFrom = "",
                            string pTopComments = "",
                            string pBottomComments = "",
                            string pViewCommNumber = "",
                            string pViewCommLink = "",
                            DateTime? pDate = null,
                            string pLabourCost = "")
    {

        string sDate = WebCommon.getGlobalResourceValue("Date");
        string sSentOn = WebCommon.getGlobalResourceValue("SentOn");
        string sSentTo = WebCommon.getGlobalResourceValue("SentTo");
        string sSentFrom = WebCommon.getGlobalResourceValue("SentFrom");
        string sActionRequired = WebCommon.getGlobalResourceValue("ActionRequired");
        string sViewCommunication = WebCommon.getGlobalResourceValue("ViewCommunication");
        string sLabourCost = WebCommon.getGlobalResourceValue("LabourCost");

        StringBuilder sHTML = new StringBuilder();
        if (pColor.ToLower() == "yellow")
        {
            pColor = "f9eec4";
            sHTML.Append("<table class='yellow-bg'>");
        }

        if (pColor.ToLower() == "blue")
        {
            pColor = "EAF2FC";
            sHTML.Append("<table class='blue-bg'>");
        }

        if (pColor.ToLower() == "red")
        {
            pColor = "FF3300";
            sHTML.Append("<table class='red-bg'>");
            //sHTML.Append("<table style='background-color:#" + pColor + "; color:White; width:100%;'>");
        }


        // sHTML.Append("<table style='Width:100%; background-color:#" + pColor + "'>");

        /* if (pAction != "") {
             sHTML.Append("<tr>");
             sHTML.Append("<td colspan='3' align='center'>");
             sHTML.Append("<span style='border-bottom:1px solid'>" + pRole + "</span>\r\n");
             sHTML.Append("<span style='border-bottom:1px solid'>" + pAction + "</span>");
             sHTML.Append("</td>");
             sHTML.Append("</tr>");
             sHTML.Append("<br/>");
         }
         else {
             sHTML.Append("<tr>");
             sHTML.Append("<td colspan='3' align='center'>");
             sHTML.Append(pRole);
             sHTML.Append("</td>");
             sHTML.Append("</tr>");
         }*/

        if (pTopComments != "")
        {
            sHTML.Append("<tr>");
            sHTML.Append("<td colspan='3' align='left'>");
            sHTML.Append(pTopComments);
            sHTML.Append("</td>");
            sHTML.Append("</tr>");
        }

        if (pDate != null)
        {
            sHTML.Append("<tr>");
            sHTML.Append("<td style='font-style;font-weight:bold'>" + sDate + "&nbsp;&nbsp;</td>");
            sHTML.Append("<td>:</td>");
            sHTML.Append("<td>" + Convert.ToDateTime(pDate).ToString("dd/MM/yyyy") + "</td>");
            sHTML.Append("</tr>");
        }
        if (pSentOn != null)
        {
            sHTML.Append("<tr>");
            sHTML.Append("<td style='font-style;font-weight:bold'>" + sSentOn + "&nbsp;&nbsp;</td>");
            sHTML.Append("<td>:</td>");
            sHTML.Append("<td>" + Convert.ToDateTime(pSentOn).ToString("dd/MM/yyyy") + "</td>");
            sHTML.Append("</tr>");
        }
        if (pLabourCost != "")
        {
            sHTML.Append("<tr>");
            sHTML.Append("<td style='font-style;font-weight:bold'>" + sLabourCost + "&nbsp;&nbsp;</td>");
            sHTML.Append("<td>:</td>");
            sHTML.Append("<td>" + pLabourCost + "</td>");
            sHTML.Append("</tr>");
        }
        if (pSentTo != "")
        {
            sHTML.Append("<tr>");
            sHTML.Append("<td style='font-style;font-weight:bold'>" + sSentTo + "&nbsp;&nbsp;</td>");
            sHTML.Append("<td>:</td>");
            sHTML.Append("<td>");

            //pSentTo = pSentTo.Trim(',');
            //foreach (string str in pSentTo.Split(','))
            //{
            //    sHTML.Append(str + "<br />");
            //}
            sHTML.Append(pSentTo);
            sHTML.Append("</td>");
            sHTML.Append("</tr>");
        }
        //if (pSentFrom != "") {
        //    sHTML.Append("<tr>");
        //    sHTML.Append("<td style='font-style;font-weight:bold'>" + sSentFrom + "&nbsp;&nbsp;</td>");
        //    sHTML.Append("<td>:</td>");
        //    sHTML.Append("<td>" + pSentFrom + "</td>");
        //    sHTML.Append("</tr>");
        //}

        if (pBottomComments != "")
        {
            sHTML.Append("<tr>");
            sHTML.Append("<td colspan='3' align='left' style='font-style:italic;font-weight:bold'>" + pBottomComments + "</td>");
            sHTML.Append("</tr>");
        }


        if (pViewCommLink != "")
        {
            sHTML.Append("<tr>");
            sHTML.Append("<td align='left' colspan='3'>" + pViewCommLink + "&nbsp;&nbsp;</td>");
            sHTML.Append("</tr>");
        }



        //sHTML.Append("<br/><br/>");
        sHTML.Append("</table>");
        return sHTML.ToString();
    }


    public string function5(DateTime pDate, string pColor, string pUserName = "", string pAgreeWithDiscrepancy = "",
        string pPickerName = "", string pComments = "")
    {

        string sDate = WebCommon.getGlobalResourceValue("Date");
        string sUserName = WebCommon.getGlobalResourceValue("UserName");
        string sAgreeWithDiscrepancy = WebCommon.getGlobalResourceValue("AgreeWithDiscrepancy");
        string sPicker = WebCommon.getGlobalResourceValue("Picker");
        string sComments = WebCommon.getGlobalResourceValue("Comments");
        StringBuilder sHTML = new StringBuilder();

        if (pColor.ToLower() == "yellow")
        {
            pColor = "f9eec4";
            sHTML.Append("<table class='yellow-bg'>");
        }

        if (pColor.ToLower() == "blue")
        {
            pColor = "EAF2FC";
            sHTML.Append("<table class='blue-bg'>");
        }
        if (pColor.ToLower() == "white")
        {
            sHTML.Append("<table  background-color:#" + pColor + "'>");
        }

        if (pColor.ToLower() == "red")
        {
            pColor = "FF3300";
            sHTML.Append("<table class='red-bg'>");
            //sHTML.Append("<table style='background-color:#" + pColor + "; color:White; width:100%;'>");
        }

        if (pDate != null)
        {
            sHTML.Append("<tr>");
            sHTML.Append("<td style='font-style;font-weight:bold'>" + sDate + "&nbsp;&nbsp;</td>");
            sHTML.Append("<td>:</td>");
            sHTML.Append("<td>" + pDate.ToString("dd/MM/yyyy") + "</td>");
            sHTML.Append("</tr>");
        }

        if (!string.IsNullOrEmpty(pUserName))
        {
            sHTML.Append("<tr>");
            sHTML.Append("<td style='font-style;font-weight:bold'>" + sUserName + "</td>");
            sHTML.Append("<td>:</td>");
            sHTML.Append("<td>" + pUserName + "</td>");
            sHTML.Append("</tr>");
        }

        if (!string.IsNullOrEmpty(pPickerName))
        {
            sHTML.Append("<tr>");
            sHTML.Append("<td style='font-style;font-weight:bold'>" + sAgreeWithDiscrepancy + "&nbsp;&nbsp;</td>");
            sHTML.Append("<td>:</td>");
            sHTML.Append("<td>" + pAgreeWithDiscrepancy + "</td>");
            sHTML.Append("</tr>");
        }

        if (!string.IsNullOrEmpty(pPickerName))
        {
            sHTML.Append("<tr>");
            sHTML.Append("<td style='font-style;font-weight:bold'>" + sPicker + "&nbsp;&nbsp;</td>");
            sHTML.Append("<td>:</td>");
            sHTML.Append("<td>" + pPickerName + "</td>");
            sHTML.Append("</tr>");
        }

        if (!string.IsNullOrEmpty(pComments))
        {
            sHTML.Append("<tr>");
            sHTML.Append("<td  style='font-style;font-weight:bold'>" + sComments + "&nbsp;&nbsp;</td>");
            sHTML.Append("<td colspan='2'>:</td>");
            sHTML.Append("</tr>");

            sHTML.Append("<tr>");
            sHTML.Append("<td colspan='3' style='font-style:italic;font-weight:bold'>" + pComments + "</td>");
            sHTML.Append("</tr>");
        }

        sHTML.Append("</table>");
        return sHTML.ToString();
    }


    public string function6(DateTime pDate, string pColor, string pUserName = "", string pAction = "", string pGoodsBookedInStockCounted = "",
       string pReason = "", string pDiscrepancyNo = "", string pComments = "", string pNaxAlternatePO = "", DateTime? pAdjustmentDate = null, string pAdjustmentQty = "")
    {

        string sDate = WebCommon.getGlobalResourceValue("Date");
        string sUserName = WebCommon.getGlobalResourceValue("UserName");
        string sAction = WebCommon.getGlobalResourceValue("Action");
        string sGoodsBookedInStockCounted = WebCommon.getGlobalResourceValue("GoodsBookedInStockCounted");
        string sDiscrepancyNo = WebCommon.getGlobalResourceValue("DiscrepancyNo");
        string sResolution = WebCommon.getGlobalResourceValue("Resolution");
        string sReason = WebCommon.getGlobalResourceValue("Reason");
        string sComments = WebCommon.getGlobalResourceValue("Comments");
        string sNaxAlternatePO = WebCommon.getGlobalResourceValue("NAXAlternatePONo");
        string sAdjustmentDate = WebCommon.getGlobalResourceValue("AdjustmentDate");
        string sAdjustmentQty = WebCommon.getGlobalResourceValue("AdjustmentQty");
        StringBuilder sHTML = new StringBuilder();

        if (pColor.ToLower() == "yellow")
        {
            pColor = "f9eec4";
            sHTML.Append("<table class='yellow-bg'>");
        }

        if (pColor.ToLower() == "blue")
        {
            pColor = "EAF2FC";
            sHTML.Append("<table class='blue-bg'>");
        }
        if (pColor.ToLower() == "white")
        {
            sHTML.Append("<table  background-color:#" + pColor + "'>");
        }

        if (pColor.ToLower() == "red")
        {
            pColor = "FF3300";
            sHTML.Append("<table class='red-bg'>");
            //sHTML.Append("<table style='background-color:#" + pColor + "; color:White; width:100%;'>");
        }

        if (pDate != null)
        {
            sHTML.Append("<tr>");
            sHTML.Append("<td style='font-style;font-weight:bold'>" + sDate + "&nbsp;&nbsp;</td>");
            sHTML.Append("<td>:</td>");
            sHTML.Append("<td>" + pDate.ToString("dd/MM/yyyy") + "</td>");
            sHTML.Append("</tr>");
        }

        if (!string.IsNullOrEmpty(pUserName))
        {
            sHTML.Append("<tr>");
            sHTML.Append("<td style='font-style;font-weight:bold'>" + sUserName + "</td>");
            sHTML.Append("<td>:</td>");
            sHTML.Append("<td>" + pUserName + "</td>");
            sHTML.Append("</tr>");
        }

        if (!string.IsNullOrEmpty(pAction))
        {
            sHTML.Append("<tr>");
            sHTML.Append("<td style='font-style;font-weight:bold'>" + sAction + "</td>");
            sHTML.Append("<td>:</td>");
            sHTML.Append("<td>" + pAction + "</td>");
            sHTML.Append("</tr>");
        }

        if (!string.IsNullOrEmpty(pReason))
        {
            if (pAction == "Dispute is not valid")
            {
                sHTML.Append("<tr>");
                sHTML.Append("<td style='font-style;font-weight:bold'>" + sReason + "</td>");
                sHTML.Append("<td>:</td>");
                sHTML.Append("<td>" + pReason + "</td>");
                sHTML.Append("</tr>");
            }
            else
            {
                sHTML.Append("<tr>");
                sHTML.Append("<td style='font-style;font-weight:bold'>" + sResolution + "</td>");
                sHTML.Append("<td>:</td>");
                sHTML.Append("<td>" + pReason + "</td>");
                sHTML.Append("</tr>");
            }
        }

        if (!string.IsNullOrEmpty(pGoodsBookedInStockCounted))
        {

            pGoodsBookedInStockCounted = pGoodsBookedInStockCounted + " :";
            sHTML.Append("<tr>");
            sHTML.Append("<td  colspan='3' style='font-style;font-weight:bold'>" + sGoodsBookedInStockCounted + "&nbsp;&nbsp;</td>");
            //sHTML.Append("<td colspan='2'>:</td>");
            sHTML.Append("</tr>");

            sHTML.Append("<tr>");
            sHTML.Append("<td colspan='3' style='font-style:italic'>" + pGoodsBookedInStockCounted + "</td>");
            sHTML.Append("</tr>");
        }

        if (!string.IsNullOrEmpty(pDiscrepancyNo))
        {
            sHTML.Append("<tr>");
            sHTML.Append("<td style='font-style;font-weight:bold'>" + sDiscrepancyNo + "&nbsp;&nbsp;</td>");
            sHTML.Append("<td>:</td>");
            sHTML.Append("<td>" + pDiscrepancyNo + "</td>");
            sHTML.Append("</tr>");
        }

        if (!string.IsNullOrEmpty(pNaxAlternatePO))
        {
            sHTML.Append("<tr>");
            sHTML.Append("<td style='font-style;font-weight:bold'>" + sNaxAlternatePO + "&nbsp;&nbsp;</td>");
            sHTML.Append("<td>:</td>");
            sHTML.Append("<td>" + pNaxAlternatePO + "</td>");
            sHTML.Append("</tr>");
        }


        if (pAdjustmentDate != null)
        {
            sHTML.Append("<tr>");
            sHTML.Append("<td style='font-style;font-weight:bold'>" + sAdjustmentDate + "&nbsp;&nbsp;</td>");
            sHTML.Append("<td>:</td>");
            sHTML.Append("<td>" + Convert.ToDateTime(pAdjustmentDate).ToString("dd/MM/yyyy") + "</td>");
            sHTML.Append("</tr>");
        }
        if (!string.IsNullOrEmpty(pAdjustmentQty))
        {
            sHTML.Append("<tr>");
            sHTML.Append("<td style='font-style;font-weight:bold'>" + sAdjustmentQty + "&nbsp;&nbsp;</td>");
            sHTML.Append("<td>:</td>");
            sHTML.Append("<td>" + pAdjustmentQty + "</td>");
            sHTML.Append("</tr>");
        }

        if (!string.IsNullOrEmpty(pComments))
        {
            sHTML.Append("<tr>");
            sHTML.Append("<td  style='font-style;font-weight:bold'>" + sComments + "&nbsp;&nbsp;</td>");
            sHTML.Append("<td colspan='2'>:</td>");
            sHTML.Append("</tr>");

            sHTML.Append("<tr>");
            sHTML.Append("<td colspan='3' style='font-style:italic;font-weight:bold'>" + pComments + "</td>");
            sHTML.Append("</tr>");
        }

        sHTML.Append("</table>");
        return sHTML.ToString();
    }


    public string function7(DateTime pDate, string pColor, string pAction = "", string pDeletedBy = "", string pSentTo = "", string pTopComments = "")
    {

        string sDate = WebCommon.getGlobalResourceValue("Date");
        string sAction = WebCommon.getGlobalResourceValue("Action");
        string sDeletedBy = WebCommon.getGlobalResourceValue("DeletedBy");
        string sSentTo = WebCommon.getGlobalResourceValue("SentTo");


        StringBuilder sHTML = new StringBuilder();


        if (pColor.ToLower() == "aqua")
        {
            pColor = "{00,CC,FF}";
            sHTML.Append("<table class='aqua-bg'>");
        }

        if (pTopComments != "")
        {
            sHTML.Append("<tr>");
            sHTML.Append("<td colspan='3' align='left'>");
            sHTML.Append(pTopComments);
            sHTML.Append("</td>");
            sHTML.Append("</tr>");
        }

        if (!string.IsNullOrEmpty(pAction))
        {
            sHTML.Append("<tr>");
            sHTML.Append("<td style='font-style;font-weight:bold'>" + sAction + "</td>");
            sHTML.Append("<td>:</td>");
            sHTML.Append("<td>" + pAction + "</td>");
            sHTML.Append("</tr>");
        }

        if (pDate != null)
        {
            sHTML.Append("<tr>");
            sHTML.Append("<td style='font-style;font-weight:bold'>" + sDate + "&nbsp;&nbsp;</td>");
            sHTML.Append("<td>:</td>");
            sHTML.Append("<td>" + pDate.ToString("dd/MM/yyyy") + "</td>");
            sHTML.Append("</tr>");
        }

        if (!string.IsNullOrEmpty(pDeletedBy))
        {
            sHTML.Append("<tr>");
            sHTML.Append("<td style='font-style;font-weight:bold'>" + sDeletedBy + "</td>");
            sHTML.Append("<td>:</td>");
            sHTML.Append("<td>" + pDeletedBy + "</td>");
            sHTML.Append("</tr>");
        }
        if (pSentTo != "")
        {
            sHTML.Append("<tr>");
            sHTML.Append("<td style='font-style;font-weight:bold'>" + sSentTo + "&nbsp;&nbsp;</td>");
            sHTML.Append("<td>:</td>");
            sHTML.Append("<td>");
            sHTML.Append(pSentTo);
            sHTML.Append("</td>");
            sHTML.Append("</tr>");
        }
        sHTML.Append("</table>");
        return sHTML.ToString();
    }

    public string function8(string pColor, string pTopComments = "")
    {
        StringBuilder sHTML = new StringBuilder();

        if (pColor.ToLower() == "green")
        {
            pColor = "ccffcc";
            sHTML.Append("<table class='lightgreen-bg'>");
        }

        if (pTopComments != "")
        {
            sHTML.Append("<tr>");
            sHTML.Append("<td style='height:30px'></td>");
            sHTML.Append("</tr>");
            sHTML.Append("<tr>");
            sHTML.Append("<td  style='font-style:italic;font-weight:bold;'>" + pTopComments + "</td>");
            sHTML.Append("</tr>");
        }


        sHTML.Append("</table>");
        return sHTML.ToString();
    }

    public string function11(string GoodsReturnType,
                            DateTime pDate,
                            DateTime? pCollectionDate,
                            DateTime? pGoodsCollectedOn,
                            string pColor,
                            string pAction = "",
                            string pAction1 = "",
                            string pUserName = "",
                            string pComments = "",
                            string pPurchaseOrderNumber = "",
                            string pDebitNoteNumber = "",
                            string pCarrier = "",
                            string pCollAuthNumber = "",
                            string pCarriageCharge = "",
                            string pCreditNoteNumber = "",
                            string pReason = "",
                            string pActionTaken = "",
                            string pCost = "",
                            string pLabourCost = "",
                            string pAdminCost = "",
                            string pPallets = "",
                            string pPackSizeOrdered = "",
                            string pPackSizeReceived = "",
                            string pNoEscalationOnFromPage = "",
                            string pNoEscalationOn = "",
                            string pAmount = "",
                            int? pViewCommLink = null,
                            string pType = "",
                            string pOversReason = "",
                            string pTrackingNumber = "",
                            string pCartons = ""

        )
    {

        string sDate = WebCommon.getGlobalResourceValue("Date");
        string sUserName = WebCommon.getGlobalResourceValue("UserName");
        string sAction = WebCommon.getGlobalResourceValue("Action");
        string sComments = WebCommon.getGlobalResourceValue("Comments");
        string sPurchaseOrderNumber = WebCommon.getGlobalResourceValue("PurchaseOrderNumber");
        string sDebitNoteNumber = WebCommon.getGlobalResourceValue("DebitNoteNumber");
        string sCollectionDate = WebCommon.getGlobalResourceValue("CollectionDate");
        string sCollectionAuthorisationNumber = WebCommon.getGlobalResourceValue("CollectionAuthorisationNumber");
        string sCarrier = WebCommon.getGlobalResourceValue("Carrier");
        string sGoodsCollectedOn = WebCommon.getGlobalResourceValue("GoodsCollectedOn");
        string sCarriageCharge = WebCommon.getGlobalResourceValue("CarriageCharge");
        string sCreditNoteNumber = WebCommon.getGlobalResourceValue("CreditNoteNumber");
        string sReason = WebCommon.getGlobalResourceValue("Reason");
        string sActionTaken = WebCommon.getGlobalResourceValue("ActionTaken");
        string sCost = WebCommon.getGlobalResourceValue("Cost");
        string sLabourCost = WebCommon.getGlobalResourceValue("LabourCost");
        string sAdminCost = WebCommon.getGlobalResourceValue("AdminCost");
        string sNoOfPallets = "#" + WebCommon.getGlobalResourceValue("ActualPallets");
        string sPackSizeOrdered = WebCommon.getGlobalResourceValue("PackSizeOrdered");
        string sPackSizeReceived = WebCommon.getGlobalResourceValue("PackSizeReceived");
        string sNoEscalationOn = WebCommon.getGlobalResourceValue("NoEscalationOn");
        string sAmount = WebCommon.getGlobalResourceValue("Amount");
        string sType = WebCommon.getGlobalResourceValue("Type");
        string sOversReason = WebCommon.getGlobalResourceValue("OversReason");
        string sNoOfCartons = "#" + WebCommon.getGlobalResourceValue("ActualCartons");
        string sTrackingNumber = WebCommon.getGlobalResourceValue("TrackingNumber");
        string strGoodsReturntype = string.Empty;
        if (GoodsReturnType == "D")
        {
            strGoodsReturntype = "Goods dispose";
        }
        if (GoodsReturnType == "O")
        {
            strGoodsReturntype = "Goods return to vendor";
        }
        if (GoodsReturnType == "V")
        {
            strGoodsReturntype = "Goods collected";
        }
        StringBuilder sHTML = new StringBuilder();

        if (pColor.ToLower() == "yellow")
        {
            pColor = "f9eec4";
            sHTML.Append("<table class='yellow-bg'>");
        }

        if (pColor.ToLower() == "blue")
        {
            pColor = "EAF2FC";
            sHTML.Append("<table class='blue-bg'>");
        }
        if (pColor.ToLower() == "white")
        {
            sHTML.Append("<table  background-color:#" + pColor + "'>");
        }
        if (pColor.ToLower() == "red")
        {
            pColor = "FF3300";
            sHTML.Append("<table class='red-bg'>");
            //sHTML.Append("<table style='background-color:#" + pColor + "; color:White; width:100%;'>");
        }
        if (pColor.ToLower() == "brown")
        {
            pColor = "#f6bc81";
            sHTML.Append("<table class='brown-bg'>");
        }

        if (pDate != null)
        {
            sHTML.Append("<tr>");
            sHTML.Append("<td style='font-style;font-weight:bold'>" + sDate + "&nbsp;&nbsp;</td>");
            sHTML.Append("<td>:</td>");
            sHTML.Append("<td>" + pDate.ToString("dd/MM/yyyy") + "</td>");
            sHTML.Append("</tr>");
        }
        if (pReason != "")
        {
            sHTML.Append("<tr>");
            sHTML.Append("<td style='font-style;font-weight:bold'>" + sReason + "&nbsp;&nbsp;</td>");
            sHTML.Append("<td>:</td>");
            sHTML.Append("<td>" + pReason + "</td>");
            sHTML.Append("</tr>");
        }

        if (pUserName != "")
        {
            sHTML.Append("<tr>");
            sHTML.Append("<td style='font-style;font-weight:bold'>" + sUserName + "</td>");
            sHTML.Append("<td>:</td>");
            sHTML.Append("<td>" + pUserName + "</td>");
            sHTML.Append("</tr>");
        }
        //***************************
        if (GoodsReturnType != "")
        {
            sHTML.Append("<tr>");
            sHTML.Append("<td style='font-style;font-weight:bold'>" + sActionTaken + "</td>");
            sHTML.Append("<td>:</td>");
            sHTML.Append("<td>" + strGoodsReturntype + "</td>");
            sHTML.Append("</tr>");
        }
        //***************************
        if (pOversReason != "")
        {
            sHTML.Append("<tr>");
            sHTML.Append("<td style='font-style;font-weight:bold'>" + sOversReason + "&nbsp;&nbsp;</td>");
            sHTML.Append("<td>:</td>");
            sHTML.Append("<td>" + pOversReason + "</td>");
            sHTML.Append("</tr>");
        }

        if (pAction1 != "")
        {
            sHTML.Append("<tr>");
            sHTML.Append("<td style='font-style;font-weight:bold'>" + sAction + "&nbsp;&nbsp;</td>");
            sHTML.Append("<td>:</td>");
            sHTML.Append("<td>" + pAction1 + "</td>");
            sHTML.Append("</tr>");
        }
        if (pDebitNoteNumber != "")
        {
            sHTML.Append("<tr>");
            sHTML.Append("<td style='font-style;font-weight:bold'>" + sDebitNoteNumber + "&nbsp;&nbsp;</td>");
            sHTML.Append("<td>:</td>");
            sHTML.Append("<td>" + pDebitNoteNumber + "</td>");
            sHTML.Append("</tr>");
        }



        if (pCollectionDate != null)
        {
            sHTML.Append("<tr>");
            sHTML.Append("<td style='font-style;font-weight:bold'>" + sCollectionDate + "&nbsp;&nbsp;</td>");
            sHTML.Append("<td>:</td>");
            sHTML.Append("<td>" + Convert.ToDateTime(pCollectionDate).ToString("dd/MM/yyyy") + "</td>");
            sHTML.Append("</tr>");
        }

        if (!string.IsNullOrEmpty(pCarrier))
        {
            sHTML.Append("<tr>");
            sHTML.Append("<td style='font-style;font-weight:bold'>" + sCarrier + "&nbsp;&nbsp;</td>");
            sHTML.Append("<td>:</td>");
            sHTML.Append("<td>" + pCarrier + "</td>");
            sHTML.Append("</tr>");
        }



        if (pCollAuthNumber != "")
        {
            sHTML.Append("<tr>");
            sHTML.Append("<td style='font-style;font-weight:bold'>" + sCollectionAuthorisationNumber + "&nbsp;&nbsp;</td>");
            sHTML.Append("<td>:</td>");
            sHTML.Append("<td>" + pCollAuthNumber + "</td>");
            sHTML.Append("</tr>");
        }

        if (pGoodsCollectedOn != null)
        {
            sHTML.Append("<tr>");
            sHTML.Append("<td style='font-style;font-weight:bold'>" + sGoodsCollectedOn + "&nbsp;&nbsp;</td>");
            sHTML.Append("<td>:</td>");
            sHTML.Append("<td>" + Convert.ToDateTime(pGoodsCollectedOn).ToString("dd/MM/yyyy") + "</td>");
            sHTML.Append("</tr>");
        }

        if (!string.IsNullOrEmpty(pCarriageCharge))
        {
            sHTML.Append("<tr>");
            sHTML.Append("<td style='font-style;font-weight:bold'>" + sCarriageCharge + "&nbsp;&nbsp;</td>");
            sHTML.Append("<td>:</td>");
            sHTML.Append("<td>" + pCarriageCharge + "</td>");
            sHTML.Append("</tr>");
        }


        //if (pCreditNoteNumber != "")
        //{
        //    sHTML.Append("<tr>");
        //    sHTML.Append("<td style='font-style;font-weight:bold'>" + sCreditNoteNumber + "&nbsp;&nbsp;</td>");
        //    sHTML.Append("<td>:</td>");
        //    sHTML.Append("<td>" + pCreditNoteNumber + "</td>");
        //    sHTML.Append("</tr>");
        //}

        //if (pActionTaken != "")
        //{
        //    sHTML.Append("<tr>");
        //    sHTML.Append("<td style='font-style;font-weight:bold'>" + sActionTaken + "&nbsp;&nbsp;</td>");
        //    sHTML.Append("<td>:</td>");
        //    sHTML.Append("<td>" + pActionTaken + "</td>");
        //    sHTML.Append("</tr>");
        //}

        if (pPallets != "")
        {
            sHTML.Append("<tr>");
            sHTML.Append("<td style='font-style;font-weight:bold'>" + sNoOfPallets + "&nbsp;&nbsp;</td>");
            sHTML.Append("<td>:</td>");
            sHTML.Append("<td>" + pPallets + "</td>");
            sHTML.Append("</tr>");
        }

        if (pCartons != "")
        {
            sHTML.Append("<tr>");
            sHTML.Append("<td style='font-style;font-weight:bold'>" + sNoOfCartons + "&nbsp;&nbsp;</td>");
            sHTML.Append("<td>:</td>");
            sHTML.Append("<td>" + pCartons + "</td>");
            sHTML.Append("</tr>");
        }

        if (pCost != "")
        {
            sHTML.Append("<tr>");
            sHTML.Append("<td style='font-style;font-weight:bold'>" + sCost + "&nbsp;&nbsp;</td>");
            sHTML.Append("<td>:</td>");
            sHTML.Append("<td>" + pCost + "</td>");
            sHTML.Append("</tr>");
        }

        if (pLabourCost != "")
        {
            sHTML.Append("<tr>");
            sHTML.Append("<td style='font-style;font-weight:bold'>" + sLabourCost + "&nbsp;&nbsp;</td>");
            sHTML.Append("<td>:</td>");
            sHTML.Append("<td>" + pLabourCost + "</td>");
            sHTML.Append("</tr>");
        }

        if (pAdminCost != "")
        {
            sHTML.Append("<tr>");
            sHTML.Append("<td style='font-style;font-weight:bold'>" + sAdminCost + "&nbsp;&nbsp;</td>");
            sHTML.Append("<td>:</td>");
            sHTML.Append("<td>" + pAdminCost + "</td>");
            sHTML.Append("</tr>");
        }

        if (pPackSizeOrdered != "")
        {
            sHTML.Append("<tr>");
            sHTML.Append("<td style='font-style;font-weight:bold'>" + sPackSizeOrdered + "&nbsp;&nbsp;</td>");
            sHTML.Append("<td>:</td>");
            sHTML.Append("<td>" + pPackSizeOrdered + "</td>");
            sHTML.Append("</tr>");
        }

        if (pPackSizeReceived != "")
        {
            sHTML.Append("<tr>");
            sHTML.Append("<td style='font-style;font-weight:bold'>" + sPackSizeReceived + "&nbsp;&nbsp;</td>");
            sHTML.Append("<td>:</td>");
            sHTML.Append("<td>" + pPackSizeReceived + "</td>");
            sHTML.Append("</tr>");
        }
        if (pNoEscalationOnFromPage != "")
        {
            sHTML.Append("<tr>");
            sHTML.Append("<td style='font-style;font-weight:bold'>" + sNoEscalationOn + "&nbsp;&nbsp;</td>");
            sHTML.Append("<td>:</td>");
            sHTML.Append("<td>" + pNoEscalationOn + "</td>");
            sHTML.Append("</tr>");
        }

        if (pAmount != "")
        {
            sHTML.Append("<tr>");
            sHTML.Append("<td style='font-style;font-weight:bold'>" + sAmount + "&nbsp;&nbsp;</td>");
            sHTML.Append("<td>:</td>");
            sHTML.Append("<td>" + pAmount + "</td>");
            sHTML.Append("</tr>");
        }

        if (pType != "")
        {
            sHTML.Append("<tr>");
            sHTML.Append("<td style='font-style;font-weight:bold'>" + sType + "&nbsp;&nbsp;</td>");
            sHTML.Append("<td>:</td>");
            sHTML.Append("<td>" + pType + "</td>");
            sHTML.Append("</tr>");
        }

        if (pComments != "")
        {
            sHTML.Append("<tr>");
            sHTML.Append("<td  style='font-style;font-weight:bold'>" + sComments + "&nbsp;&nbsp;</td>");
            sHTML.Append("<td colspan='2'>:</td>");
            sHTML.Append("</tr>");

            sHTML.Append("<tr>");
            sHTML.Append("<td colspan='3' style='font-style:italic;font-weight:bold'>" + pComments + "</td>");
            sHTML.Append("</tr>");
        }
        if (pViewCommLink.HasValue)
        {
            sHTML.Append("<tr>");

            sHTML.Append("<td align='right' colspan='3'>" +
               "<a href='" + Common.EncryptQuery("Dis_ReturnNote.aspx?ReturnnoteID=" + pViewCommLink.Value) +
               "' onclick='window.open(this.href, this.target,\"scrollbars=1,left=100px,top=100px,width=1000px,height=800px\"); return false;'>View Return Note</a>&nbsp;&nbsp;</td>");

            sHTML.Append("</tr>");
        }

        //sHTML.Append("<br/><br/>");
        sHTML.Append("</table>");

        return sHTML.ToString();
    }

    public string function12(string GoodsReturnType,
                           DateTime pDate,
                           DateTime? pCollectionDate,
                           string pGoodsCollectedOn,
                           string pColor,
                           string pCollAuthNumber,
                           string pComments,
                           string pUserName,
                           int? pViewCommLink,
                           string SentTo,
                           string Carrier,
                           string pBottomComments = null
       )
    {

        string sDate = WebCommon.getGlobalResourceValue("Date");
        string sUserName = WebCommon.getGlobalResourceValue("UserName");
        string sAction = WebCommon.getGlobalResourceValue("Action");
        string sComments = WebCommon.getGlobalResourceValue("Comments");
        string sPurchaseOrderNumber = WebCommon.getGlobalResourceValue("PurchaseOrderNumber");
        string sDebitNoteNumber = WebCommon.getGlobalResourceValue("DebitNoteNumber");
        string sCollectionDate = WebCommon.getGlobalResourceValue("CollectionDate");
        string sCollectionAuthorisationNumber = WebCommon.getGlobalResourceValue("CollectionAuthorisationNumber");
        string sCarrier = WebCommon.getGlobalResourceValue("Carrier");
        string sGoodsCollectedOn = WebCommon.getGlobalResourceValue("GoodsCollectedOn");
        string sCarriageCharge = WebCommon.getGlobalResourceValue("CarriageCharge");
        string sCreditNoteNumber = WebCommon.getGlobalResourceValue("CreditNoteNumber");
        string sReason = WebCommon.getGlobalResourceValue("Reason");
        string sActionTaken = WebCommon.getGlobalResourceValue("ActionTaken");
        string sCost = WebCommon.getGlobalResourceValue("Cost");
        string sLabourCost = WebCommon.getGlobalResourceValue("LabourCost");
        string sAdminCost = WebCommon.getGlobalResourceValue("AdminCost");
        string sNoOfPallets = "#" + WebCommon.getGlobalResourceValue("ActualPallets");
        string sPackSizeOrdered = WebCommon.getGlobalResourceValue("PackSizeOrdered");
        string sPackSizeReceived = WebCommon.getGlobalResourceValue("PackSizeReceived");
        string sNoEscalationOn = WebCommon.getGlobalResourceValue("NoEscalationOn");
        string sAmount = WebCommon.getGlobalResourceValue("Amount");
        string sType = WebCommon.getGlobalResourceValue("Type");
        string sOversReason = WebCommon.getGlobalResourceValue("OversReason");
        string sNoOfCartons = "#" + WebCommon.getGlobalResourceValue("ActualCartons");
        string sTrackingNumber = WebCommon.getGlobalResourceValue("TrackingNumber");
        string sSentTo = WebCommon.getGlobalResourceValue("SentTo");
        string strGoodsReturntype = string.Empty;
        if (GoodsReturnType == "D")
        {
            strGoodsReturntype = "Goods dispose";
        }
        if (GoodsReturnType == "O")
        {
            strGoodsReturntype = "Goods return to vendor";
        }
        if (GoodsReturnType == "V")
        {
            strGoodsReturntype = "Goods collected";
        }
        StringBuilder sHTML = new StringBuilder();

        if (pColor.ToLower() == "yellow")
        {
            pColor = "f9eec4";
            sHTML.Append("<table class='yellow-bg'>");
        }

        if (pColor.ToLower() == "blue")
        {
            pColor = "EAF2FC";
            sHTML.Append("<table class='blue-bg'>");
        }
        if (pColor.ToLower() == "white")
        {
            sHTML.Append("<table  background-color:#" + pColor + "'>");
        }
        if (pColor.ToLower() == "red")
        {
            pColor = "FF3300";
            sHTML.Append("<table class='red-bg'>");
            //sHTML.Append("<table style='background-color:#" + pColor + "; color:White; width:100%;'>");
        }
        if (pColor.ToLower() == "brown")
        {
            pColor = "#f6bc81";
            sHTML.Append("<table class='brown-bg'>");
        }

        if (pDate != null)
        {
            sHTML.Append("<tr>");
            sHTML.Append("<td style='font-style;font-weight:bold'>" + sDate + "&nbsp;&nbsp;</td>");
            sHTML.Append("<td>:</td>");
            sHTML.Append("<td>" + pDate.ToString("dd/MM/yyyy") + "</td>");
            sHTML.Append("</tr>");
        }

        if (!string.IsNullOrEmpty(SentTo))
        {
            sHTML.Append("<tr>");
            sHTML.Append("<td style='font-style;font-weight:bold'>" + sSentTo + "</td>");
            sHTML.Append("<td>:</td>");
            sHTML.Append("<td>" + SentTo + "</td>");
            sHTML.Append("</tr>");
        }
        if (string.IsNullOrEmpty(SentTo) && pUserName != "")
        {
            sHTML.Append("<tr>");
            sHTML.Append("<td style='font-style;font-weight:bold'>" + sUserName + "</td>");
            sHTML.Append("<td>:</td>");
            sHTML.Append("<td>" + pUserName + "</td>");
            sHTML.Append("</tr>");
        }
        //***************************
        if (GoodsReturnType != "")
        {
            sHTML.Append("<tr>");
            sHTML.Append("<td style='font-style;font-weight:bold'>" + sActionTaken + "</td>");
            sHTML.Append("<td>:</td>");
            sHTML.Append("<td>" + strGoodsReturntype + "</td>");
            sHTML.Append("</tr>");
        }
        //***************************
        if (!string.IsNullOrEmpty(pGoodsCollectedOn) && !string.IsNullOrEmpty(SentTo))
        {
            sHTML.Append("<tr>");
            sHTML.Append("<td style='font-style;font-weight:bold'>" + sCollectionDate + "&nbsp;&nbsp;</td>");
            sHTML.Append("<td>:</td>");
            sHTML.Append("<td>" + pGoodsCollectedOn + "</td>");
            sHTML.Append("</tr>");
        }
        if (string.IsNullOrEmpty(SentTo) && pCollectionDate != null)
        {
            sHTML.Append("<tr>");
            sHTML.Append("<td style='font-style;font-weight:bold'>" + sCollectionDate + "&nbsp;&nbsp;</td>");
            sHTML.Append("<td>:</td>");
            sHTML.Append("<td>" + Convert.ToDateTime(pCollectionDate).ToString("dd/MM/yyyy") + "</td>");
            sHTML.Append("</tr>");
        }
        if (!string.IsNullOrEmpty(Carrier))
        {
            sHTML.Append("<tr>");
            sHTML.Append("<td style='font-style;font-weight:bold'>" + sCarrier + "&nbsp;&nbsp;</td>");
            sHTML.Append("<td>:</td>");
            sHTML.Append("<td>" + Carrier + "</td>");
            sHTML.Append("</tr>");
        }


        if (!string.IsNullOrEmpty(pCollAuthNumber))
        {
            sHTML.Append("<tr>");
            sHTML.Append("<td style='font-style;font-weight:bold'>" + sCollectionAuthorisationNumber + "&nbsp;&nbsp;</td>");
            sHTML.Append("<td>:</td>");
            sHTML.Append("<td>" + pCollAuthNumber + "</td>");
            sHTML.Append("</tr>");
        }

        if (string.IsNullOrEmpty(SentTo) && pGoodsCollectedOn != null)
        {
            sHTML.Append("<tr>");
            sHTML.Append("<td style='font-style;font-weight:bold'>" + sGoodsCollectedOn + "&nbsp;&nbsp;</td>");
            sHTML.Append("<td>:</td>");
            sHTML.Append("<td>" + pGoodsCollectedOn + "</td>");
            sHTML.Append("</tr>");
        }
        if (pComments != "")
        {
            sHTML.Append("<tr>");
            sHTML.Append("<td  style='font-style;font-weight:bold'>" + sComments + "&nbsp;&nbsp;</td>");
            sHTML.Append("<td colspan='2'>:</td>");
            sHTML.Append("</tr>");

            sHTML.Append("<tr>");
            sHTML.Append("<td colspan='3' style='font-style:italic;font-weight:bold'>" + pComments + "</td>");
            sHTML.Append("</tr>");
        }
        if (!string.IsNullOrEmpty(pBottomComments))
        {
            sHTML.Append("<tr>");
            sHTML.Append("<td colspan='3' align='left' style='font-style:italic;font-weight:bold'>" + pBottomComments + "</td>");
            sHTML.Append("</tr>");
        }
        if (pViewCommLink.HasValue)
        {
            sHTML.Append("<tr>");

            sHTML.Append("<td align='right' colspan='3'>" +
               "<a href='" + Common.EncryptQuery("Dis_ReturnNote.aspx?ReturnnoteID=" + pViewCommLink.Value) +
               "' onclick='window.open(this.href, this.target,\"scrollbars=1,left=100px,top=100px,width=1000px,height=800px\"); return false;'>View Return Note</a>&nbsp;&nbsp;</td>");

            sHTML.Append("</tr>");
        }

        //sHTML.Append("<br/><br/>");
        sHTML.Append("</table>");

        return sHTML.ToString();
    }
    //public string function6(DateTime pDate,
    //                        DateTime? pCollectionDate,
    //                        DateTime? pGoodsCollectedOn,
    //                        string pColor,
    //                        string pAction = "",
    //                        string pAction1 = "",
    //                        string pUserName = "",
    //                        string pComments = "",
    //                        string pPurchaseOrderNumber = "",
    //                        string pDebitNoteNumber = "",
    //                        string pCarrier = "",
    //                        string pCollAuthNumber = "",
    //                        string pCarriageCharge = "",
    //                        string pCreditNoteNumber = "",
    //                        string pReason = "",
    //                        string pActionTaken = "",
    //                        string pCost = "",
    //                        string pLabourCost = "",
    //                        string pAdminCost = "",
    //                        string pPallets = "",
    //                        string pPackSizeOrdered = "",
    //                        string pPackSizeReceived = "",
    //                        string pNoEscalationOnFromPage = "",
    //                        string pNoEscalationOn = "",
    //                        string pAmount = "",
    //                        int? pViewCommLink = null,
    //                        string pType = "")
    //{

    //    string sDate = WebCommon.getGlobalResourceValue("Date");
    //    string sUserName = WebCommon.getGlobalResourceValue("UserName");
    //    string sAction = WebCommon.getGlobalResourceValue("Action");
    //    string sComments = WebCommon.getGlobalResourceValue("Comments");
    //    string sPurchaseOrderNumber = WebCommon.getGlobalResourceValue("PurchaseOrderNumber");
    //    string sDebitNoteNumber = WebCommon.getGlobalResourceValue("DebitNoteNumber");
    //    string sCollectionDate = WebCommon.getGlobalResourceValue("CollectionDate");
    //    string sCollectionAuthorisationNumber = WebCommon.getGlobalResourceValue("CollectionAuthorisationNumber");
    //    string sCarrier = WebCommon.getGlobalResourceValue("Carrier");
    //    string sGoodsCollectedOn = WebCommon.getGlobalResourceValue("GoodsCollectedOn");
    //    string sCarriageCharge = WebCommon.getGlobalResourceValue("CarriageCharge");
    //    string sCreditNoteNumber = WebCommon.getGlobalResourceValue("CreditNoteNumber");
    //    string sReason = WebCommon.getGlobalResourceValue("Reason");
    //    string sActionTaken = WebCommon.getGlobalResourceValue("ActionTaken");
    //    string sCost = WebCommon.getGlobalResourceValue("Cost");
    //    string sLabourCost = WebCommon.getGlobalResourceValue("LabourCost");
    //    string sAdminCost = WebCommon.getGlobalResourceValue("AdminCost");
    //    string sNoOfPallets = WebCommon.getGlobalResourceValue("#Pallets");
    //    string sPackSizeOrdered = WebCommon.getGlobalResourceValue("PackSizeOrdered");
    //    string sPackSizeReceived = WebCommon.getGlobalResourceValue("PackSizeReceived");
    //    string sNoEscalationOn = WebCommon.getGlobalResourceValue("NoEscalationOn");
    //    string sAmount = WebCommon.getGlobalResourceValue("Amount");
    //    string sType = WebCommon.getGlobalResourceValue("Type");

    //    StringBuilder sHTML = new StringBuilder();

    //    if (pColor.ToLower() == "yellow")
    //    {
    //        pColor = "f9eec4";
    //        sHTML.Append("<table class='yellow-bg'>");
    //    }

    //    if (pColor.ToLower() == "blue")
    //    {
    //        pColor = "EAF2FC";
    //        sHTML.Append("<table class='blue-bg'>");
    //    }
    //    if (pColor.ToLower() == "white")
    //    {
    //        sHTML.Append("<table  background-color:#" + pColor + "'>");
    //    }

    //    if (pDate != null)
    //    {
    //        sHTML.Append("<tr>");
    //        sHTML.Append("<td style='font-style;font-weight:bold'>" + sDate + "&nbsp;&nbsp;</td>");
    //        sHTML.Append("<td>:</td>");
    //        sHTML.Append("<td>" + pDate.ToString("dd/MM/yyyy") + "</td>");
    //        sHTML.Append("</tr>");
    //    }
    //    if (pReason != "")
    //    {
    //        sHTML.Append("<tr>");
    //        sHTML.Append("<td style='font-style;font-weight:bold'>" + sReason + "&nbsp;&nbsp;</td>");
    //        sHTML.Append("<td>:</td>");
    //        sHTML.Append("<td>" + pReason + "</td>");
    //        sHTML.Append("</tr>");
    //    }

    //    if (pUserName != "")
    //    {
    //        sHTML.Append("<tr>");
    //        sHTML.Append("<td style='font-style;font-weight:bold'>" + sUserName + "</td>");
    //        sHTML.Append("<td>:</td>");
    //        sHTML.Append("<td>" + pUserName + "</td>");
    //        sHTML.Append("</tr>");
    //    }

    //    if (pAction1 != "")
    //    {
    //        sHTML.Append("<tr>");
    //        sHTML.Append("<td style='font-style;font-weight:bold'>" + sAction + "&nbsp;&nbsp;</td>");
    //        sHTML.Append("<td>:</td>");
    //        sHTML.Append("<td>" + pAction1 + "</td>");
    //        sHTML.Append("</tr>");
    //    }
    //    if (pDebitNoteNumber != "")
    //    {
    //        sHTML.Append("<tr>");
    //        sHTML.Append("<td style='font-style;font-weight:bold'>" + sDebitNoteNumber + "&nbsp;&nbsp;</td>");
    //        sHTML.Append("<td>:</td>");
    //        sHTML.Append("<td>" + pDebitNoteNumber + "</td>");
    //        sHTML.Append("</tr>");
    //    }

    //    if (pPurchaseOrderNumber != "")
    //    {
    //        sHTML.Append("<tr>");
    //        sHTML.Append("<td style='font-style;font-weight:bold'>" + sPurchaseOrderNumber + "&nbsp;&nbsp;</td>");
    //        sHTML.Append("<td>:</td>");
    //        sHTML.Append("<td>" + pPurchaseOrderNumber + "</td>");
    //        sHTML.Append("</tr>");
    //    }

    //    if (pCollectionDate != null)
    //    {
    //        sHTML.Append("<tr>");
    //        sHTML.Append("<td style='font-style;font-weight:bold'>" + sCollectionDate + "&nbsp;&nbsp;</td>");
    //        sHTML.Append("<td>:</td>");
    //        sHTML.Append("<td>" + Convert.ToDateTime(pCollectionDate).ToString("dd/MM/yyyy") + "</td>");
    //        sHTML.Append("</tr>");
    //    }

    //    if (pCarrier != "")
    //    {
    //        sHTML.Append("<tr>");
    //        sHTML.Append("<td style='font-style;font-weight:bold'>" + sCarrier + "&nbsp;&nbsp;</td>");
    //        sHTML.Append("<td>:</td>");
    //        sHTML.Append("<td>" + pCarrier + "</td>");
    //        sHTML.Append("</tr>");
    //    }

    //    if (pCollAuthNumber != "")
    //    {
    //        sHTML.Append("<tr>");
    //        sHTML.Append("<td style='font-style;font-weight:bold'>" + sCollectionAuthorisationNumber + "&nbsp;&nbsp;</td>");
    //        sHTML.Append("<td>:</td>");
    //        sHTML.Append("<td>" + pCollAuthNumber + "</td>");
    //        sHTML.Append("</tr>");
    //    }

    //    if (pGoodsCollectedOn != null)
    //    {
    //        sHTML.Append("<tr>");
    //        sHTML.Append("<td style='font-style;font-weight:bold'>" + sGoodsCollectedOn + "&nbsp;&nbsp;</td>");
    //        sHTML.Append("<td>:</td>");
    //        sHTML.Append("<td>" + Convert.ToDateTime(pGoodsCollectedOn).ToString("dd/MM/yyyy") + "</td>");
    //        sHTML.Append("</tr>");
    //    }

    //    if (pCarriageCharge != "")
    //    {
    //        sHTML.Append("<tr>");
    //        sHTML.Append("<td style='font-style;font-weight:bold'>" + sCarriageCharge + "&nbsp;&nbsp;</td>");
    //        sHTML.Append("<td>:</td>");
    //        sHTML.Append("<td>" + pCarriageCharge + "</td>");
    //        sHTML.Append("</tr>");
    //    }

    //    if (pCreditNoteNumber != "")
    //    {
    //        sHTML.Append("<tr>");
    //        sHTML.Append("<td style='font-style;font-weight:bold'>" + sCreditNoteNumber + "&nbsp;&nbsp;</td>");
    //        sHTML.Append("<td>:</td>");
    //        sHTML.Append("<td>" + pCreditNoteNumber + "</td>");
    //        sHTML.Append("</tr>");
    //    }

    //    if (pActionTaken != "")
    //    {
    //        sHTML.Append("<tr>");
    //        sHTML.Append("<td style='font-style;font-weight:bold'>" + sActionTaken + "&nbsp;&nbsp;</td>");
    //        sHTML.Append("<td>:</td>");
    //        sHTML.Append("<td>" + pActionTaken + "</td>");
    //        sHTML.Append("</tr>");
    //    }

    //    if (pPallets != "")
    //    {
    //        sHTML.Append("<tr>");
    //        sHTML.Append("<td style='font-style;font-weight:bold'>" + sNoOfPallets + "&nbsp;&nbsp;</td>");
    //        sHTML.Append("<td>:</td>");
    //        sHTML.Append("<td>" + pPallets + "</td>");
    //        sHTML.Append("</tr>");
    //    }

    //    if (pCost != "")
    //    {
    //        sHTML.Append("<tr>");
    //        sHTML.Append("<td style='font-style;font-weight:bold'>" + sCost + "&nbsp;&nbsp;</td>");
    //        sHTML.Append("<td>:</td>");
    //        sHTML.Append("<td>" + pCost + "</td>");
    //        sHTML.Append("</tr>");
    //    }

    //    if (pLabourCost != "")
    //    {
    //        sHTML.Append("<tr>");
    //        sHTML.Append("<td style='font-style;font-weight:bold'>" + sLabourCost + "&nbsp;&nbsp;</td>");
    //        sHTML.Append("<td>:</td>");
    //        sHTML.Append("<td>" + pLabourCost + "</td>");
    //        sHTML.Append("</tr>");
    //    }

    //    if (pAdminCost != "")
    //    {
    //        sHTML.Append("<tr>");
    //        sHTML.Append("<td style='font-style;font-weight:bold'>" + sAdminCost + "&nbsp;&nbsp;</td>");
    //        sHTML.Append("<td>:</td>");
    //        sHTML.Append("<td>" + pAdminCost + "</td>");
    //        sHTML.Append("</tr>");
    //    }

    //    if (pPackSizeOrdered != "")
    //    {
    //        sHTML.Append("<tr>");
    //        sHTML.Append("<td style='font-style;font-weight:bold'>" + sPackSizeOrdered + "&nbsp;&nbsp;</td>");
    //        sHTML.Append("<td>:</td>");
    //        sHTML.Append("<td>" + pPackSizeOrdered + "</td>");
    //        sHTML.Append("</tr>");
    //    }

    //    if (pPackSizeReceived != "")
    //    {
    //        sHTML.Append("<tr>");
    //        sHTML.Append("<td style='font-style;font-weight:bold'>" + sPackSizeReceived + "&nbsp;&nbsp;</td>");
    //        sHTML.Append("<td>:</td>");
    //        sHTML.Append("<td>" + pPackSizeReceived + "</td>");
    //        sHTML.Append("</tr>");
    //    }
    //    if (pNoEscalationOnFromPage != "")
    //    {
    //        sHTML.Append("<tr>");
    //        sHTML.Append("<td style='font-style;font-weight:bold'>" + sNoEscalationOn + "&nbsp;&nbsp;</td>");
    //        sHTML.Append("<td>:</td>");
    //        sHTML.Append("<td>" + pNoEscalationOn + "</td>");
    //        sHTML.Append("</tr>");
    //    }

    //    if (pAmount != "")
    //    {
    //        sHTML.Append("<tr>");
    //        sHTML.Append("<td style='font-style;font-weight:bold'>" + sAmount + "&nbsp;&nbsp;</td>");
    //        sHTML.Append("<td>:</td>");
    //        sHTML.Append("<td>" + pAmount + "</td>");
    //        sHTML.Append("</tr>");
    //    }

    //    if (pType != "")
    //    {
    //        sHTML.Append("<tr>");
    //        sHTML.Append("<td style='font-style;font-weight:bold'>" + sType + "&nbsp;&nbsp;</td>");
    //        sHTML.Append("<td>:</td>");
    //        sHTML.Append("<td>" + pType + "</td>");
    //        sHTML.Append("</tr>");
    //    }

    //    if (pComments != "")
    //    {
    //        sHTML.Append("<tr>");
    //        sHTML.Append("<td  style='font-style;font-weight:bold'>" + sComments + "&nbsp;&nbsp;</td>");
    //        sHTML.Append("<td colspan='2'>:</td>");
    //        sHTML.Append("</tr>");

    //        sHTML.Append("<tr>");
    //        sHTML.Append("<td colspan='3' style='font-style:italic;font-weight:bold'>" + pComments + "</td>");
    //        sHTML.Append("</tr>");
    //    }
    //    if (pViewCommLink.HasValue)
    //    {
    //        sHTML.Append("<tr>");

    //        sHTML.Append("<td align='right' colspan='3'>" +
    //           "<a href='" + Common.EncryptQuery("Dis_ReturnNote.aspx?ReturnnoteID=" + pViewCommLink.Value) +
    //           "' onclick='window.open(this.href, this.target,\"scrollbars=1,left=100px,top=100px,width=1000px,height=800px\"); return false;'>View Return Note</a>&nbsp;&nbsp;</td>");

    //        sHTML.Append("</tr>");
    //    }

    //    //sHTML.Append("<br/><br/>");
    //    sHTML.Append("</table>");

    //    return sHTML.ToString();
    //}


    public string function13(DateTime pDate,
                           string DiscrePancyText = "",
                           string pColor = "",
                           string pComments = "",
                           string pUserName = "",
                           string pAction = "",
                           string pPO = "",
                           string pInvoice = "",
                           string QuantityChosen = "",
                           string DiscrepancyCreated = "",
                           string DeliveryNoteChecked = "",
                           string DeliveryNoteInvalid = "",
                           string PhysicalInventoryCheck = "",
                           string HostSystemChecked = "",
                           string SentTo = "",
                           string SentOn = "",
                           string DebitAmount = "",
                           string DiscrePancyTextBottom = "",
                           string pViewCommLink = "",
                           string issue = "",
                           string creditNote = "",
                           string aPDocument = "",
                           string debitNote = "",
                           string currency = "",
                           string vendorVATRef = ""

       )
    {

        string sDate = WebCommon.getGlobalResourceValue("Date");
        string sUserName = WebCommon.getGlobalResourceValue("UserName");
        string sAction = WebCommon.getGlobalResourceValue("Action");
        string sComments = WebCommon.getGlobalResourceValue("Comments");
        string sInvoiceNumHash = WebCommon.getGlobalResourceValue("InvoiceNumHash");
        string sSentTo = WebCommon.getGlobalResourceValue("SentTo");
        string sPO = WebCommon.getGlobalResourceValue("PO");
        string sDiscrepancyCreated = WebCommon.getGlobalResourceValue("DiscrepancyCreated");
        string sDeliveryNoteChecked = WebCommon.getGlobalResourceValue("DeliveryNoteChecked");
        string sDeliveryNoteInvalid = WebCommon.getGlobalResourceValue("DeliveryNoteInvalid");
        string sPhysicalInventoryCheck = WebCommon.getGlobalResourceValue("PhysicalInventoryCheck");
        string sHostSystemChecked = WebCommon.getGlobalResourceValue("HostSystemChecked");
        string sDebitAmount = WebCommon.getGlobalResourceValue("DebitAmount");
        string sQuantityChosen = WebCommon.getGlobalResourceValue("QuantityChosen");
        string sSentOn = WebCommon.getGlobalResourceValue("SentOn");
        string sIssue = WebCommon.getGlobalResourceValue("GIN015WOrkflow4");
        string sCreditNote = WebCommon.getGlobalResourceValue("CreditNote#");
        string sAPDocument = WebCommon.getGlobalResourceValue("APDocument#");
        string sDebitNote = WebCommon.getGlobalResourceValue("DebitNote#");
        string sCurrency = WebCommon.getGlobalResourceValue("Currency");
        string sVendorVATRef = WebCommon.getGlobalResourceValue("VendorVATRef");
        StringBuilder sHTML = new StringBuilder();

        if (pColor.ToLower() == "yellow")
        {
            pColor = "f9eec4";
            sHTML.Append("<table class='yellow-bg'>");
        }

        if (pColor.ToLower() == "blue")
        {
            pColor = "EAF2FC";
            sHTML.Append("<table class='blue-bg'>");
        }
        if (pColor.ToLower() == "white")
        {
            sHTML.Append("<table  background-color:#" + pColor + "'>");
        }
        if (pColor.ToLower() == "red")
        {
            pColor = "FF3300";
            sHTML.Append("<table class='red-bg'>");
            //sHTML.Append("<table style='background-color:#" + pColor + "; color:White; width:100%;'>");
        }
        if (pColor.ToLower() == "brown")
        {
            pColor = "#f6bc81";
            sHTML.Append("<table class='brown-bg'>");
        }

        if (pDate != null)
        {
            sHTML.Append("<tr>");
            sHTML.Append("<td style='font-style;font-weight:bold'>" + sDate + "&nbsp;&nbsp;</td>");
            sHTML.Append("<td>:</td>");
            sHTML.Append("<td>" + pDate.ToString("dd/MM/yyy") + "</td>");
            sHTML.Append("</tr>");
        }


        if (!string.IsNullOrEmpty(pUserName))
        {
            sHTML.Append("<tr>");
            sHTML.Append("<td style='font-style;font-weight:bold'>" + sUserName + "</td>");
            sHTML.Append("<td>:</td>");
            sHTML.Append("<td>" + pUserName + "</td>");
            sHTML.Append("</tr>");
        }

        if (!string.IsNullOrEmpty(pAction))
        {
            sHTML.Append("<tr>");
            sHTML.Append("<td style='font-style;font-weight:bold'>" + sAction + "</td>");
            sHTML.Append("<td>:</td>");
            sHTML.Append("<td>" + pAction + "</td>");
            sHTML.Append("</tr>");
        }
        if (!string.IsNullOrEmpty(pPO))
        {
            sHTML.Append("<tr>");
            sHTML.Append("<td style='font-style;font-weight:bold'>" + sPO + "</td>");
            sHTML.Append("<td>:</td>");
            sHTML.Append("<td>" + pPO + "</td>");
            sHTML.Append("</tr>");
        }


        if (!string.IsNullOrEmpty(pInvoice))
        {
            sHTML.Append("<tr>");
            sHTML.Append("<td style='font-style;font-weight:bold'>" + sInvoiceNumHash + "</td>");
            sHTML.Append("<td>:</td>");
            sHTML.Append("<td>" + pInvoice + "</td>");
            sHTML.Append("</tr>");
        }
        if (!string.IsNullOrEmpty(QuantityChosen))
        {
            sHTML.Append("<tr>");
            sHTML.Append("<td style='font-style;font-weight:bold'>" + sQuantityChosen + "</td>");
            sHTML.Append("<td>:</td>");
            sHTML.Append("<td>" + QuantityChosen + "</td>");
            sHTML.Append("</tr>");
        }

        if (!string.IsNullOrEmpty(DiscrepancyCreated))
        {
            sHTML.Append("<tr>");
            sHTML.Append("<td style='font-style;font-weight:bold'>" + sDiscrepancyCreated + "</td>");
            sHTML.Append("<td>:</td>");
            sHTML.Append("<td>" + DiscrepancyCreated + "</td>");
            sHTML.Append("</tr>");
        }

        if (!string.IsNullOrEmpty(DeliveryNoteChecked))
        {
            sHTML.Append("<tr>");
            sHTML.Append("<td style='font-style;font-weight:bold'>" + sDeliveryNoteChecked + "</td>");
            sHTML.Append("<td>:</td>");
            sHTML.Append("<td>" + DeliveryNoteChecked + "</td>");
            sHTML.Append("</tr>");
        }
        if (!string.IsNullOrEmpty(DeliveryNoteInvalid))
        {
            sHTML.Append("<tr>");
            sHTML.Append("<td style='font-style;font-weight:bold'>" + sDeliveryNoteInvalid + "</td>");
            sHTML.Append("<td>:</td>");
            sHTML.Append("<td>" + DeliveryNoteInvalid + "</td>");
            sHTML.Append("</tr>");
        }
        if (!string.IsNullOrEmpty(PhysicalInventoryCheck))
        {
            sHTML.Append("<tr>");
            sHTML.Append("<td style='font-style;font-weight:bold'>" + sPhysicalInventoryCheck + "</td>");
            sHTML.Append("<td>:</td>");
            sHTML.Append("<td>" + PhysicalInventoryCheck + "</td>");
            sHTML.Append("</tr>");
        }

        if (!string.IsNullOrEmpty(HostSystemChecked))
        {
            sHTML.Append("<tr>");
            sHTML.Append("<td style='font-style;font-weight:bold'>" + sHostSystemChecked + "</td>");
            sHTML.Append("<td>:</td>");
            sHTML.Append("<td>" + HostSystemChecked + "</td>");
            sHTML.Append("</tr>");
        }

        if (!string.IsNullOrEmpty(DebitAmount))
        {
            sHTML.Append("<tr>");
            sHTML.Append("<td style='font-style;font-weight:bold'>" + sDebitAmount + "</td>");
            sHTML.Append("<td>:</td>");
            sHTML.Append("<td>" + DebitAmount + "</td>");
            sHTML.Append("</tr>");
        }

        if (!string.IsNullOrEmpty(currency))
        {
            sHTML.Append("<tr>");
            sHTML.Append("<td style='font-style;font-weight:bold'>" + sCurrency + "</td>");
            sHTML.Append("<td>:</td>");
            sHTML.Append("<td>" + currency + "</td>");
            sHTML.Append("</tr>");
        }

        if (!string.IsNullOrEmpty(vendorVATRef))
        {
            sHTML.Append("<tr>");
            sHTML.Append("<td style='font-style;font-weight:bold'>" + sVendorVATRef + "</td>");
            sHTML.Append("<td>:</td>");
            sHTML.Append("<td>" + vendorVATRef + "</td>");
            sHTML.Append("</tr>");
        }

        if (SentTo != "")
        {
            sHTML.Append("<tr>");
            sHTML.Append("<td style='font-style;font-weight:bold'>" + sSentTo + "&nbsp;&nbsp;</td>");
            sHTML.Append("<td>:</td>");
            sHTML.Append("<td>");
            sHTML.Append(SentTo);
            sHTML.Append("</td>");
            sHTML.Append("</tr>");
        }



        if (SentOn != "")
        {
            sHTML.Append("<tr>");
            sHTML.Append("<td style='font-style;font-weight:bold'>" + sSentOn + "&nbsp;&nbsp;</td>");
            sHTML.Append("<td>:</td>");
            sHTML.Append("<td>");
            sHTML.Append(SentOn);
            sHTML.Append("</td>");
            sHTML.Append("</tr>");
        }


        if (issue != "")
        {
            sHTML.Append("<tr>");
            sHTML.Append("<td style='font-style;font-weight:bold'>" + sIssue + "&nbsp;&nbsp;</td>");
            sHTML.Append("<td>:</td>");
            sHTML.Append("<td>");
            sHTML.Append(issue);
            sHTML.Append("</td>");
            sHTML.Append("</tr>");
        }
        if (debitNote != "")
        {
            sHTML.Append("<tr>");
            sHTML.Append("<td style='font-style;font-weight:bold'>" + sDebitNote + "&nbsp;&nbsp;</td>");
            sHTML.Append("<td>:</td>");
            sHTML.Append("<td>");
            sHTML.Append(debitNote);
            sHTML.Append("</td>");
            sHTML.Append("</tr>");
        }
        if (creditNote != "")
        {
            sHTML.Append("<tr>");
            sHTML.Append("<td style='font-style;font-weight:bold'>" + sCreditNote + "&nbsp;&nbsp;</td>");
            sHTML.Append("<td>:</td>");
            sHTML.Append("<td>");
            sHTML.Append(creditNote);
            sHTML.Append("</td>");
            sHTML.Append("</tr>");
        }
        if (aPDocument != "")
        {
            sHTML.Append("<tr>");
            sHTML.Append("<td style='font-style;font-weight:bold'>" + sAPDocument + "&nbsp;&nbsp;</td>");
            sHTML.Append("<td>:</td>");
            sHTML.Append("<td>");
            sHTML.Append(aPDocument);
            sHTML.Append("</td>");
            sHTML.Append("</tr>");
        }

        if (pComments != "")
        {
            sHTML.Append("<tr>");
            sHTML.Append("<td  style='font-style;font-weight:bold'>" + sComments + "&nbsp;&nbsp;</td>");
            sHTML.Append("<td colspan='2'>:</td>");
            sHTML.Append("</tr>");

            sHTML.Append("<tr>");
            sHTML.Append("<td colspan='3' style='font-style:italic;font-weight:bold'>" + pComments + "</td>");
            sHTML.Append("</tr>");
        }
        if (!string.IsNullOrEmpty(DiscrePancyTextBottom))
        {
            sHTML.Append("<tr>");
            sHTML.Append("<td colspan='3' align='left' style='font-style:italic;font-weight:bold'>" + DiscrePancyTextBottom + "</td>");
            sHTML.Append("</tr>");
        }

        if (pViewCommLink != "")
        {
            sHTML.Append("<tr>");
            sHTML.Append("<td align='left' colspan='3'>" + pViewCommLink + "&nbsp;&nbsp;</td>");
            sHTML.Append("</tr>");
        }
        //sHTML.Append("<br/><br/>");
        sHTML.Append("</table>");

        return sHTML.ToString();
    }
    public string function14(string DiscrePancyText = "", string pColor = "")
    {
        StringBuilder sHTML = new StringBuilder();

        if (pColor.ToLower() == "yellow")
        {
            pColor = "f9eec4";
            sHTML.Append("<tr class='yellow-bg'> ");
        }

        if (pColor.ToLower() == "blue")
        {
            pColor = "EAF2FC";
            sHTML.Append("<tr class='blue-bg'> ");
        }
        if (!string.IsNullOrEmpty(DiscrePancyText))
        {

            sHTML.Append("<td colspan=8 align=center style=font-style:italic;font-weight:bold>" + DiscrePancyText + "</td>");
            sHTML.Append("</tr>");
        }

        return sHTML.ToString();
    }

    #endregion
}

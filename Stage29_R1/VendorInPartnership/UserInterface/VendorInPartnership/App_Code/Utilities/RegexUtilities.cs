﻿using System;
using System.Globalization;
using System.Text.RegularExpressions;

public class RegexUtilities
{
    static bool invalid = false;

    public static bool IsValidEmail(string emailString)
    {
        invalid = false;
        if (String.IsNullOrEmpty(emailString))
            return false;

        // Use IdnMapping class to convert Unicode domain names.
        emailString = Regex.Replace(emailString, @"(@)(.+)$", DomainMapper);
        if (invalid)
            return false;

        return Regex.IsMatch(emailString, @"^[A-Za-z0-9](([_\.\-\']?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([\.\-]?[a-zA-Z0-9]+)*)\.([A-Za-z]{2,})$",
               RegexOptions.IgnoreCase);
    }

    public static bool IsDecimalTwoPlace(string decimalString)
    {
        Regex regex = new Regex(@"^\d{0,8}(\.\d{0,2})?$");
        return regex.IsMatch(decimalString);
    }

    private static string DomainMapper(Match match)
    {
        // IdnMapping class with default property values.
        IdnMapping idn = new IdnMapping();

        string domainName = match.Groups[2].Value;
        try
        {
            domainName = idn.GetAscii(domainName);
        }
        catch (ArgumentException)
        {
            invalid = true;
        }
        return match.Groups[1].Value + domainName;
    }
}
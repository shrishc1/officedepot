﻿using BusinessEntities.ModuleBE.Appointment.Booking;
using BusinessLogicLayer.ModuleBAL.Appointment.Booking;
using System;
using System.Collections.Generic;
using System.Data;

/// <summary>
/// Summary description for ScheduleAvailability
/// </summary>
public static class ScheduleAvailability
{


    //[System.Web.Services.WebMethod(EnableSession = true)]
    public static List<string[]> getdisabledDays(int imonth, int iyear, bool isPageLoad)
    {
        APPBOK_BookingBE oAPPBOK_BookingBE = new APPBOK_BookingBE();
        APPBOK_BookingBAL oAPPBOK_BookingBAL = new APPBOK_BookingBAL();

        if (imonth == 0)
        {
            imonth = DateTime.Now.Month;
            iyear = DateTime.Now.Year;
        }

        DataTable dtBookings = new DataTable();

        if (System.Web.HttpContext.Current.Session["CurrentSiteId"] == null)
            System.Web.HttpContext.Current.Session["CurrentSiteId"] = System.Web.HttpContext.Current.Session["SiteId"];


        if (isPageLoad)
            System.Web.HttpContext.Current.Session["beforeShow"] = "false";

        if (Convert.ToString(System.Web.HttpContext.Current.Session["LastSiteID"]) == Convert.ToString(System.Web.HttpContext.Current.Session["CurrentSiteId"]))
        {
            if (imonth == Convert.ToInt32(System.Web.HttpContext.Current.Session["LastMonth"]))
            {
                if (Convert.ToString(System.Web.HttpContext.Current.Session["beforeShow"]) == "true")
                {
                    dtBookings = (DataTable)System.Web.HttpContext.Current.Session["dtBookings"];
                }
                else
                {
                    System.Web.HttpContext.Current.Session["beforeShow"] = "true";
                    dtBookings = oAPPBOK_BookingBAL.GetScheduleAvailabilityBAL(Convert.ToInt32(System.Web.HttpContext.Current.Session["CurrentSiteId"]), DateTime.Now.Year, DateTime.Now.Month);
                    imonth = DateTime.Now.Month;
                    System.Web.HttpContext.Current.Session["dtBookings"] = dtBookings;
                }
            }
            else if (imonth != Convert.ToInt32(System.Web.HttpContext.Current.Session["LastMonth"]))
            {
                System.Web.HttpContext.Current.Session["beforeShow"] = "true";
                dtBookings = oAPPBOK_BookingBAL.GetScheduleAvailabilityBAL(Convert.ToInt32(System.Web.HttpContext.Current.Session["CurrentSiteId"]), iyear, imonth);
                System.Web.HttpContext.Current.Session["dtBookings"] = dtBookings;
            }
        }
        else
        {
            if (imonth == Convert.ToInt32(System.Web.HttpContext.Current.Session["LastMonth"]))
            {
                System.Web.HttpContext.Current.Session["beforeShow"] = "true";
                dtBookings = oAPPBOK_BookingBAL.GetScheduleAvailabilityBAL(Convert.ToInt32(System.Web.HttpContext.Current.Session["CurrentSiteId"]), DateTime.Now.Year, DateTime.Now.Month);
                imonth = DateTime.Now.Month;
                System.Web.HttpContext.Current.Session["dtBookings"] = dtBookings;
            }
            else if (imonth != Convert.ToInt32(System.Web.HttpContext.Current.Session["LastMonth"]))
            {
                System.Web.HttpContext.Current.Session["beforeShow"] = "true";
                dtBookings = oAPPBOK_BookingBAL.GetScheduleAvailabilityBAL(Convert.ToInt32(System.Web.HttpContext.Current.Session["CurrentSiteId"]), iyear, imonth);
                System.Web.HttpContext.Current.Session["dtBookings"] = dtBookings;
            }
        }

        System.Web.HttpContext.Current.Session["LastSiteID"] = System.Web.HttpContext.Current.Session["CurrentSiteId"];
        System.Web.HttpContext.Current.Session["LastMonth"] = Convert.ToString(imonth);

        string[] rowsValues = new string[dtBookings.Rows.Count];
        var list = new List<string[]>();
        for (int i = 0; i < dtBookings.Rows.Count; i++)
        {
            string col1 = Convert.ToDateTime(dtBookings.Rows[i][0]).ToString("M-d-yyyy");
            string col2 = dtBookings.Rows[i][1].ToString();
            list.Add(new[] { col1, col2 });
        }
        return list;

        //string[,] days = new string[dtBookings.Rows.Count, 2];
        //string[] daystooltip = new string[dtBookings.Rows.Count];

        //for (int row = 0; row < dtBookings.Rows.Count; ++row) {
        //    days[row, 0] = Convert.ToDateTime(dtBookings.Rows[row][0]).ToString("M-d-yyyy");
        //    days[row, 1] = dtBookings.Rows[row][1].ToString();
        //}
        //return days;
    }
}
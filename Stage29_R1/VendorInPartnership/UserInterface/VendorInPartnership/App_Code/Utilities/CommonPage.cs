﻿using AA.switchprotocol;
using BaseControlLibrary;
using BusinessEntities.ModuleBE.Appointment.CountrySetting;
using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using BusinessEntities.ModuleBE.Discrepancy.LogDiscrepancy;
using BusinessEntities.ModuleBE.Security;
using BusinessLogicLayer.ModuleBAL.Appointment.CountrySetting;
using BusinessLogicLayer.ModuleBAL.Appointment.SiteSettings;
using BusinessLogicLayer.ModuleBAL.Discrepancy.LogDiscrepancy;
using BusinessLogicLayer.ModuleBAL.Security;
using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Utilities;
using WebUtilities;


public class CommonPage : System.Web.UI.Page
{
    public System.Text.StringBuilder sLetterLink = new System.Text.StringBuilder();
    public CommonPage()
    {
    }

    protected override void OnInit(EventArgs e)
    {
        string CurrPage = Request.Url.PathAndQuery;
        var loginRequired = Request.QueryString["NoLogin"] == null || !Convert.ToString(Request.QueryString["NoLogin"]).Equals("1");
        if (!string.IsNullOrEmpty(GetQueryStringValue("NoLogin")))
        {
            loginRequired = !Convert.ToString(GetQueryStringValue("NoLogin")).Equals("1");
        }
        if (Session["UserID"] == null && !CurrPage.Contains("Login.aspx") && !CurrPage.Contains("RegisterExternalUser") && !CurrPage.ToLower().Contains("mgmtreports") && loginRequired)
        {
            Session.Abandon();
            EncryptQueryString("~/ModuleUI/Security/Login.aspx");
        }
        ////////// Authenticate Screen////////////
        string Role = string.Empty;
        if (Session["Role"] != null && Session["Role"].ToString() != "")
        {
            Role = Session["Role"].ToString().Trim().ToLower();
        }


        base.OnInit(e);
        if (ScriptManager.GetCurrent(Page) == null)
        {
            ScriptManager scriptManager = new ScriptManager();
            Page.Form.Controls.AddAt(0, scriptManager);
        }

        setCurrentPageCulture();
    }
    public bool IsGridViewFind
    {
        get
        {
            if (ViewState["IsGridViewFind"] == null)
                ViewState["IsGridViewFind"] = false;
            return (bool)ViewState["IsGridViewFind"];
        }
        set { ViewState["IsGridViewFind"] = value; }
    }


    protected override void OnPreRender(EventArgs e)
    {

        base.OnPreRender(e); // If you delete this line you will have the error.

        if (!Page.IsPostBack)
        {
            GlobalResourceFields(this.Page);
        }
        else
        {// update grid view headers only
            if (IsGridViewFind)
                LocalizeGridHeader(this.Page);
        }
    }

    //------------------SSL Implementation Start-----------------------//
    protected override void OnLoad(EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            string scheme = Request.Url.Scheme;

            if (Globals.Settings.Urls.IsNavigationRequired)
            {
                if (_issecure)
                {
                    if (scheme != "https")
                    {
                        Response.Redirect(
                            Globals.Settings.Urls.BaseSecureUrl +
                            Request.RawUrl);
                    }
                }
                else
                {
                    if (scheme != "http")
                    {
                        string to =
                            Globals.Settings.Urls.BaseUrl +
                            Server.UrlEncode(Request.RawUrl);

                        Server.Transfer("~/Tranz.aspx?to=" + to);
                    }
                }
            }
        }
        base.OnLoad(e);
    }

    private bool _issecure = false;

    protected bool IsSecure
    {
        get
        {
            return _issecure;
        }
        set
        {
            _issecure = value;
        }
    }
    //------------------SSL Implementation End-----------------------//

    public void LocalizeGridHeader(Control root)
    {
        if (root is ucGridView)
        {
            LocalizeGridHeader((ucGridView)root);
        }

        foreach (Control child in root.Controls)
        {
            LocalizeGridHeader(child);
        }
    }

    public int GlobalResourceFields(Control root)
    {
        return GlobalResourceFieldsWithCulture(root);
    }

    public int GlobalResourceFieldsWithCulture(Control root)
    {
        string rootID = string.Empty;

        if (root is ucLabel)
        {
            rootID = root.ID.Replace("lbl", "");
            if (rootID.Contains("_"))
            {
                rootID = rootID.Substring(0, rootID.IndexOf('_'));
            }
            if (((ucLabel)root).Text.Trim() != ":")
            {
                if (!string.IsNullOrEmpty(WebCommon.getGlobalResourceValue(rootID)))
                {
                    ((ucLabel)root).Text = (String)WebCommon.getGlobalResourceValue(rootID);
                }
            }
            else
            {
                ((ucLabel)root).Text = ":";
            }
            if (((ucLabel)root).isRequired == true)
            {
                ((ucLabel)root).ForeColor = System.Drawing.Color.Maroon;
            }
        }
        else if (root is ucButton)
        {
            rootID = root.ID.Replace("btn", "");
            if (rootID.Contains("_"))
            {
                rootID = rootID.Substring(0, rootID.IndexOf('_'));
            }
            if (!string.IsNullOrEmpty(WebCommon.getGlobalResourceValue(rootID)))
                ((ucButton)root).Text = (String)WebCommon.getGlobalResourceValue(rootID);
        }
        else if (root is ucPanel)
        {
            rootID = root.ID.Replace("pnl", "");
            if (rootID.Contains("_"))
            {
                rootID = rootID.Substring(0, rootID.IndexOf('_'));
            }

            if (!string.IsNullOrEmpty(WebCommon.getGlobalResourceValue(rootID)))
            {
                ((ucPanel)root).GroupingText = (String)WebCommon.getGlobalResourceValue(rootID);
            }

        }
        else if (root is ucLinkButton)
        {
            rootID = root.ID.Replace("lnk", "");
            if (rootID.Contains("_"))
            {
                rootID = rootID.Substring(0, rootID.IndexOf('_'));
            }
            if (!string.IsNullOrEmpty(WebCommon.getGlobalResourceValue(rootID)))
            {
                ((ucLinkButton)root).Text = (String)WebCommon.getGlobalResourceValue(rootID);
            }
        }
        else if (root is ucRadioButton)
        {
            rootID = root.ID.Replace("rdo", "");
            if (rootID.Contains("_"))
            {
                rootID = rootID.Substring(0, rootID.IndexOf('_'));
            }
            if (!string.IsNullOrEmpty(WebCommon.getGlobalResourceValue(rootID)))
            {
                ((ucRadioButton)root).Text = (String)WebCommon.getGlobalResourceValue(rootID);
            }
        }
        else if (root is ucCheckbox)
        {
            rootID = root.ID.Replace("chk", "");
            if (rootID.Contains("_"))
            {
                rootID = rootID.Substring(0, rootID.IndexOf('_'));
            }
            if (!string.IsNullOrEmpty(WebCommon.getGlobalResourceValue(rootID)))
            {
                ((ucCheckbox)root).Text = (String)WebCommon.getGlobalResourceValue(rootID);
            }
        }
        else if (root is RequiredFieldValidator)
        {
            rootID = root.ID.Replace("rfv", "");
            if (rootID.Contains("_"))
            {
                rootID = rootID.Substring(0, rootID.IndexOf('_'));
            }
            if (!string.IsNullOrEmpty(WebCommon.getGlobalResourceValue(rootID)))
            {
                ((RequiredFieldValidator)root).ErrorMessage = (String)WebCommon.getGlobalResourceValue(rootID);
            }
        }
        else if (root is CompareValidator)
        {
            rootID = root.ID.Replace("cmpv", "");
            if (rootID.Contains("_"))
            {
                rootID = rootID.Substring(0, rootID.IndexOf('_'));
            }
            if (!string.IsNullOrEmpty(WebCommon.getGlobalResourceValue(rootID)))
            {
                ((CompareValidator)root).ErrorMessage = (String)WebCommon.getGlobalResourceValue(rootID);
            }
        }
        else if (root is RangeValidator)
        {
            rootID = root.ID.Replace("ranv", "");
            if (rootID.Contains("_"))
            {
                rootID = rootID.Substring(0, rootID.IndexOf('_'));
            }
            if (!string.IsNullOrEmpty(WebCommon.getGlobalResourceValue(rootID)))
            {
                ((RangeValidator)root).ErrorMessage = (String)WebCommon.getGlobalResourceValue(rootID);
            }
        }
        else if (root is CustomValidator)
        {
            rootID = root.ID.Replace("cusv", "");
            if (rootID.Contains("_"))
            {
                rootID = rootID.Substring(0, rootID.IndexOf('_'));
            }
            if (!string.IsNullOrEmpty(WebCommon.getGlobalResourceValue(rootID)))
            {
                ((CustomValidator)root).ErrorMessage = (String)WebCommon.getGlobalResourceValue(rootID);
            }
        }
        else if (root is RegularExpressionValidator)
        {
            rootID = root.ID.Replace("rev", "");
            if (rootID.Contains("_"))
            {
                rootID = rootID.Substring(0, rootID.IndexOf('_'));
            }
            if (!string.IsNullOrEmpty(WebCommon.getGlobalResourceValue(rootID)))
            {
                ((RegularExpressionValidator)root).ErrorMessage = (String)WebCommon.getGlobalResourceValue(rootID);
            }
        }
        else if (root is ucRadioButtonList)
        {

            ucRadioButtonList rbl = (ucRadioButtonList)root;

            for (int i = 0; i < rbl.Items.Count; i++)
            {
                string value = rbl.Items[i].Text;
                rootID = value.Replace("lst", "");
                if (rootID.Contains("_"))
                {
                    rootID = rootID.Substring(0, rootID.IndexOf('_'));
                }
                if (!string.IsNullOrEmpty(WebCommon.getGlobalResourceValue(rootID)))
                {
                    rbl.Items[i].Text = (String)WebCommon.getGlobalResourceValue(rootID);
                }
            }
        }
        else if (root is ucGridView)
        {
            IsGridViewFind = true;
            LocalizeGridHeader((ucGridView)root);
        }

        foreach (Control child in root.Controls)
        {
            GlobalResourceFieldsWithCulture(child);
        }
        return 0;
    }

    public void LocalizeGridHeader(GridView root)
    {
        LocalizeGridHeader((ucGridView)root);
    }

    private void setCurrentPageCulture()
    {
        Page.Culture = clsConstants.EnglishISO;

        if (Session["CultureInfo"] != null)
        {
            Page.UICulture = Convert.ToString(Session["CultureInfo"]);
        }
        else
        {
            if (System.Web.HttpContext.Current.Request.UserLanguages != null)
            {
                if (System.Web.HttpContext.Current.Request.UserLanguages[0].ToLower() == "en-gb" || System.Web.HttpContext.Current.Request.UserLanguages[0].ToLower() == "en-us")
                {

                    Page.UICulture = clsConstants.EnglishISO;
                }
                else
                {
                    Page.UICulture = System.Web.HttpContext.Current.Request.UserLanguages[0];
                }
            }
        }


    }

    public void LocalizeGridHeader(ucGridView root)
    {


        GridViewRow headerRow = (GridViewRow)((ucGridView)root).HeaderRow;

        if (headerRow != null)
        {
            for (int i = 0; i < headerRow.Cells.Count; i++)
            {
                if (((System.Web.UI.WebControls.DataControlFieldCell)(headerRow.Cells[i])) != null && ((System.Web.UI.WebControls.DataControlFieldCell)(headerRow.Cells[i])).ContainingField != null)
                {




                    //New Code
                    string sSortExpression = ((System.Web.UI.WebControls.DataControlFieldCell)(headerRow.Cells[i])).ContainingField.SortExpression;
                    string headerText = ((System.Web.UI.WebControls.DataControlFieldCell)(headerRow.Cells[i])).ContainingField.HeaderText;
                    string headerTextLocal = Convert.ToString(WebCommon.getGlobalResourceValue(headerText.Replace(" ", "")));



                    if (headerText.Trim() != "SelectAllCheckBox")
                    {

                        if (sSortExpression != string.Empty)
                        {
                            if (((System.Web.UI.WebControls.DataControlFieldCell)(headerRow.Cells[i])).Controls.Count > 0)
                                ((System.Web.UI.WebControls.LinkButton)((System.Web.UI.WebControls.DataControlFieldCell)(headerRow.Cells[i])).Controls[0]).Text = Convert.ToString(headerTextLocal) != string.Empty ? headerTextLocal : headerText;
                            else

                                headerRow.Cells[i].Text = Convert.ToString(headerTextLocal) != string.Empty ? headerTextLocal : headerText;


                        }
                        else
                        {

                            headerRow.Cells[i].Text = Convert.ToString(headerTextLocal) != string.Empty ? headerTextLocal : headerText;
                        }
                    }

                }
            }
        }
    }

    public virtual void CountrySelectedIndexChanged()
    {
        return;
    }

    public virtual void SiteSelectedIndexChanged()
    {
        return;
    }
    public virtual void TemplateSelectedIndexChanged()
    {
        return;
    }

    public virtual bool CountryPrePage_Load()
    {
        return true;
    }

    public virtual void CountryPost_Load()
    {

    }

    public virtual void CountryCustomAction()
    {

    }

    public virtual bool SitePrePage_Load()
    {
        return true;
    }

    public virtual void SitePost_Load()
    {

    }

    public virtual bool PreVendorNo_Change()
    {
        return true;
    }

    public virtual void PostVendorNo_Change()
    {
    }

    public virtual bool PreDate_Change()
    {
        return true;
    }

    public virtual void PostDate_Change()
    {
    }

    protected void SortGrid(object sender, GridViewSortEventArgs e)
    {
        //public void sortGrid( string sortExpression,string sortDirection)
        ViewState["sortDirection"] = ViewState["sortDirection"] == null || Convert.ToString(ViewState["sortDirection"]) == "DESC" ? "ASC" : "DESC";

        if (ViewState["sortDirection"].ToString() == "ASC")
            e.SortDirection = SortDirection.Ascending;
        else
            e.SortDirection = SortDirection.Descending;

        ((ucGridView)sender).DataSource = SubSort(e);
        ((ucGridView)sender).DataBind();


    }

    protected void SortGridView(object sender, GridViewSortEventArgs e)
    {
        ViewState["sortDirection"] = ViewState["sortDirection"] == null || Convert.ToString(ViewState["sortDirection"]) == "DESC" ? "ASC" : "DESC";

        if (ViewState["sortDirection"].ToString() == "ASC")
            e.SortDirection = SortDirection.Ascending;
        else
            e.SortDirection = SortDirection.Descending;

        ((GridView)sender).DataSource = SubSort(e);
        ((GridView)sender).DataBind();
    }

    public virtual BusinessEntities.BaseBe[] SubSort(GridViewSortEventArgs e)
    {
        return null;
    }

    public void RedirectToPage(string PreviousPage)
    {
        string redirectPage = string.Empty;
        if (PreviousPage == null) return;
        else if (PreviousPage == "CloseCancel")
            redirectPage = "../../DIS_SearchResultCloseCancel.aspx?PreviousPage=usercontrol";
        else if (PreviousPage == "WorkList")
            redirectPage = "../../DIS_WorklistEdit.aspx?PreviousPage=usercontrol";
        else if (PreviousPage == "SearchResult")
            redirectPage = "../../DIS_SearchResult.aspx?PreviousPage=usercontrol";
        else if (PreviousPage.Trim().ToUpper() == "SearchResultVendor".ToUpper())
            redirectPage = "../../DIS_SearchResultVendor.aspx?PreviousPage=SearchResultVendor";
        ClientScript.RegisterStartupScript(GetType(), "Load", "<script type='text/javascript'>window.parent.location.href = '" + EncryptQuery(redirectPage) + "'; </script>");
    }

    public string GeneratePassword()
    {
        //Initiate objects & vars    
        Random random = new Random();
        String randomString = "";
        int randNumber;
        //Loop ‘length’ times to generate a random number or character

        int Position1 = random.Next(0, 2);
        int Position2 = random.Next(3, 5);
        int Position4 = random.Next(6, 7);
        for (int i = 0; i < 8; i++)
        {

            if (i == Position1)
            {
                randNumber = random.Next(97, 123); //char {a-z}
            }
            else if (i == Position2)
            {
                randNumber = random.Next(65, 90); //char {A-Z}
            }
            else if (i == Position4)
            {
                randNumber = random.Next(48, 58); //int {0-9}
            }
            else if (random.Next(1, 3) == 1)
                randNumber = random.Next(97, 123); //char {a-z}
            else if (random.Next(4, 6) == 2)
                randNumber = random.Next(65, 90); //char {A-Z}           
            else
                randNumber = random.Next(48, 58); //int {0-9}

            //append random char or digit to random string
            randomString = randomString + (char)randNumber;
        }

        return randomString;
    }

    public static string GetImagesString(int DiscrepancyLogID)
    {
        StringBuilder sb = new StringBuilder();
        DISLog_ImagesBE oDISLog_ImagesBE = new DISLog_ImagesBE();
        DISLog_ImagesBAL oDISLog_ImagesBAL = new DISLog_ImagesBAL();

        oDISLog_ImagesBE.Action = "ShowAll";
        oDISLog_ImagesBE.Discrepancy = new DiscrepancyBE();
        oDISLog_ImagesBE.Discrepancy.DiscrepancyLogID = DiscrepancyLogID;

        List<DISLog_ImagesBE> lstImage = oDISLog_ImagesBAL.GetDISLogImageDetailsBAL(oDISLog_ImagesBE);

        if (lstImage != null && lstImage.Count > 0)
        {
            sb.Append("<ul>");
            foreach (DISLog_ImagesBE item in lstImage)
            {
                sb.Append(@"<li><a href='../../../Images/Discrepancy/" + item.ImageName + @"'></a></li>");
            }
            sb.Append("</ul>");
        }
        return sb.ToString(); ;
    }

    public static string GetImagesStringNewLayout(int DiscrepancyLogID)
    {
        StringBuilder sb = new StringBuilder();
        DISLog_ImagesBE oDISLog_ImagesBE = new DISLog_ImagesBE();
        DISLog_ImagesBAL oDISLog_ImagesBAL = new DISLog_ImagesBAL();

        oDISLog_ImagesBE.Action = "ShowAll";
        oDISLog_ImagesBE.Discrepancy = new DiscrepancyBE();
        oDISLog_ImagesBE.Discrepancy.DiscrepancyLogID = DiscrepancyLogID;

        List<DISLog_ImagesBE> lstImage = oDISLog_ImagesBAL.GetDISLogImageDetailsBAL(oDISLog_ImagesBE);

        if (lstImage != null && lstImage.Count > 0)
        {

            foreach (DISLog_ImagesBE item in lstImage)
            {
                sb.Append(@"<a href='../../../Images/Discrepancy/" + item.ImageName + @"' rel='shadowbox[gal]' title='" + item.ImageName + "' target='_blank'></a>");
            }

        }
        return sb.ToString(); ;
    }


    public static string GetPODStringNewLayout(int DiscrepancyLogID)
    {
        StringBuilder sb = new StringBuilder();
        DISLog_ImagesBE oDISLog_ImagesBE = new DISLog_ImagesBE();
        DISLog_ImagesBAL oDISLog_ImagesBAL = new DISLog_ImagesBAL();

        oDISLog_ImagesBE.Action = "ShowAllInvoiceQuery";
        oDISLog_ImagesBE.Discrepancy = new DiscrepancyBE();
        oDISLog_ImagesBE.Discrepancy.DiscrepancyLogID = DiscrepancyLogID;

        List<DISLog_ImagesBE> lstImage = oDISLog_ImagesBAL.GetDISLogPODDetailsBAL(oDISLog_ImagesBE);

        if (lstImage != null && lstImage.Count > 0)
        {

            foreach (DISLog_ImagesBE item in lstImage)
            {
                sb.Append(@"<a href='../../../Images/Discrepancy/" + item.ImageName + @"' rel='shadowbox[gal123]' title='" + item.ImageName + "' target='_blank'></a>");
            }

        }
        return sb.ToString(); ;
    }

    public static string GetVendorEmails(int VendorID)
    {
        MASSIT_VendorBE oMASSIT_VendorBE = new MASSIT_VendorBE();
        APPSIT_VendorBAL oAPPSIT_VendorBAL = new APPSIT_VendorBAL();
        string VendorEmail = string.Empty;
        List<MASSIT_VendorBE> lstVendorDetails = null;

        oMASSIT_VendorBE.Action = "GetVendorEmail"; // from user table
        oMASSIT_VendorBE.SiteVendorID = VendorID;

        lstVendorDetails = oAPPSIT_VendorBAL.GetVendorContactDetailsBAL(oMASSIT_VendorBE);
        if (lstVendorDetails != null && lstVendorDetails.Count > 0)
        {

            foreach (MASSIT_VendorBE item in lstVendorDetails)
            {
                if (item.Vendor.VendorContactEmail == null || item.Vendor.VendorContactEmail == "")
                {
                    VendorEmail = GetVendorDefaultEmail(VendorID);
                }
                else
                {
                    VendorEmail += item.Vendor.VendorContactEmail.ToString() + ", ";
                }
            }
        }
        else
        {
            VendorEmail = GetVendorDefaultEmail(VendorID);
        }
        return VendorEmail;
    }

    private static string GetVendorDefaultEmail(int VendorID)
    {
        MASSIT_VendorBE oMASSIT_VendorBE = new MASSIT_VendorBE();
        APPSIT_VendorBAL oAPPSIT_VendorBAL = new APPSIT_VendorBAL();
        List<MASSIT_VendorBE> lstVendorDetails = null;
        string VendorEmail = string.Empty;
        oMASSIT_VendorBE.Action = "GetContactDetails"; // from Up_vendor table
        oMASSIT_VendorBE.SiteVendorID = VendorID;

        lstVendorDetails = oAPPSIT_VendorBAL.GetVendorContactDetailsBAL(oMASSIT_VendorBE);
        foreach (MASSIT_VendorBE item in lstVendorDetails)
        {
            if (!string.IsNullOrEmpty(item.Vendor.VendorContactEmail))
            {
                VendorEmail += item.Vendor.VendorContactEmail.ToString() + ", ";
            }
        }
        return VendorEmail;
    }

    public static string[] GetVendorEmailsWithLanguage(int VendorID)
    {
        string[] VendorData = new string[2];
        MASSIT_VendorBE oMASSIT_VendorBE = new MASSIT_VendorBE();
        APPSIT_VendorBAL oAPPSIT_VendorBAL = new APPSIT_VendorBAL();
        string VendorEmail = string.Empty;
        List<MASSIT_VendorBE> lstVendorDetails = null;

        oMASSIT_VendorBE.Action = "GetVendorEmail"; // from user table
        oMASSIT_VendorBE.SiteVendorID = VendorID;

        lstVendorDetails = oAPPSIT_VendorBAL.GetVendorContactDetailsBAL(oMASSIT_VendorBE);
        if (lstVendorDetails != null && lstVendorDetails.Count > 0)
        {

            foreach (MASSIT_VendorBE item in lstVendorDetails)
            {
                if (item.Vendor.VendorContactEmail == null || item.Vendor.VendorContactEmail == "")
                {
                    VendorData = GetVendorDefaultEmailWithLanguage(VendorID);
                }
                else
                {
                    VendorData[0] += item.Vendor.VendorContactEmail.ToString() + ",";
                    VendorData[1] += item.Vendor.Language.ToString() + ",";
                }
            }
        }
        else
        {
            VendorData = GetVendorDefaultEmailWithLanguage(VendorID);
        }
        return VendorData;
    }

    public static string[] GetVendorOTIFContactWithLanguage(int VendorID)
    {
        string[] VendorData = new string[2];
        MASSIT_VendorBE oMASSIT_VendorBE = new MASSIT_VendorBE();
        APPSIT_VendorBAL oAPPSIT_VendorBAL = new APPSIT_VendorBAL();
        List<MASSIT_VendorBE> lstVendorDetails = null;

        oMASSIT_VendorBE.Action = "GetVendorOTIFContact";
        oMASSIT_VendorBE.SiteVendorID = VendorID;

        lstVendorDetails = oAPPSIT_VendorBAL.GetVendorContactDetailsBAL(oMASSIT_VendorBE);
        if (lstVendorDetails != null && lstVendorDetails.Count > 0)
        {

            foreach (MASSIT_VendorBE item in lstVendorDetails)
            {
                if (item.Vendor.VendorContactEmail == null || item.Vendor.VendorContactEmail == "")
                {
                    VendorData = GetVendorDefaultEmailWithLanguageID(VendorID);
                }
                else
                {
                    VendorData[0] += item.Vendor.VendorContactEmail.ToString() + ",";
                    VendorData[1] += item.Vendor.LanguageID.ToString() + ",";
                }
            }
        }
        else
        {
            VendorData = GetVendorDefaultEmailWithLanguageID(VendorID);
        }
        return VendorData;
    }


    public static string GetDiscrepancyContactsOfVendor(int vendorId, int siteId)
    {
        var emailIds = string.Empty;
        if (vendorId > 0)
        {
            SCT_UserBE oSCT_UserBE = new SCT_UserBE();
            SCT_UserBAL oSCT_UserBAL = new SCT_UserBAL();
            oSCT_UserBE.Action = "GetContactDetailsDiscrepancy";
            oSCT_UserBE.VendorID = vendorId;
            oSCT_UserBE.SiteId = siteId;
            List<SCT_UserBE> lstVendorDetails1 = oSCT_UserBAL.GetUserDetailsBAL(oSCT_UserBE);
            if (lstVendorDetails1 != null && lstVendorDetails1.Count > 0)
            {
                foreach (SCT_UserBE item in lstVendorDetails1)
                    emailIds += item.EmailId.ToString() + ", ";

                emailIds = emailIds.Trim(new char[] { ',', ' ' });
            }
        }

        if (string.IsNullOrEmpty(emailIds))
            emailIds = System.Configuration.ConfigurationManager.AppSettings["DefaultEmailAddress"];

        return emailIds;
    }

    public static List<SCT_UserBE> GetDiscrepancyContactDetailsOfVendor(int vendorId, int siteId)
    {
        var lstuserDetails = new List<SCT_UserBE>();
        if (vendorId > 0)
        {
            var oSCT_UserBE = new SCT_UserBE();
            var oSCT_UserBAL = new SCT_UserBAL();
            oSCT_UserBE.Action = "GetContactDetailsDiscrepancy";
            oSCT_UserBE.VendorID = vendorId;
            oSCT_UserBE.SiteId = siteId;
            lstuserDetails = oSCT_UserBAL.GetUserDetailsBAL(oSCT_UserBE);
        }
        return lstuserDetails;
    }

    public static string[] GetVendorEmailsWithLanguage(int VendorID, int SiteID)
    {
        string[] VendorData = new string[2];
        MASSIT_VendorBE oMASSIT_VendorBE = new MASSIT_VendorBE();
        APPSIT_VendorBAL oAPPSIT_VendorBAL = new APPSIT_VendorBAL();
        string VendorEmail = string.Empty;
        List<MASSIT_VendorBE> lstVendorDetails = null;

        oMASSIT_VendorBE.Action = "GetVendorEmailForBooking"; // from user table
        oMASSIT_VendorBE.SiteVendorID = VendorID;
        oMASSIT_VendorBE.SiteID = SiteID;

        StringBuilder sb = new StringBuilder();
        
        sb.Append("Action :GetVendorEmailForBooking \r\n");
        sb.Append("SiteVendorID :" + VendorID + "\r\n");
        sb.Append("SiteID :" + SiteID + "\r\n");
        LogUtility.WriteTrace(sb);

        lstVendorDetails = oAPPSIT_VendorBAL.GetVendorContactDetailsBAL(oMASSIT_VendorBE);
        if (lstVendorDetails != null && lstVendorDetails.Count > 0)
        {

            foreach (MASSIT_VendorBE item in lstVendorDetails)
            {
                if (item.Vendor.VendorContactEmail == null || item.Vendor.VendorContactEmail == "")
                {
                    VendorData = GetVendorDefaultEmailWithLanguage(VendorID);
                }
                else
                {
                    VendorData[0] += item.Vendor.VendorContactEmail.ToString() + ",";
                    VendorData[1] += item.Vendor.Language.ToString() + ",";
                }

                sb = new StringBuilder();
                sb.Append("VendorData[0]"+ VendorData[0] + "\r\n");
                sb.Append("VendorData[1] :" + VendorData[1] + "\r\n");
                LogUtility.WriteTrace(sb);
            }
        }
        else
        {
            VendorData = GetVendorDefaultEmailWithLanguage(VendorID, SiteID);
        }

        return VendorData;
    }

    private static string[] GetVendorDefaultEmailWithLanguage(int VendorID)
    {
        string[] VendorData = new string[2];
        MASSIT_VendorBE oMASSIT_VendorBE = new MASSIT_VendorBE();
        APPSIT_VendorBAL oAPPSIT_VendorBAL = new APPSIT_VendorBAL();
        List<MASSIT_VendorBE> lstVendorDetails = null;
        oMASSIT_VendorBE.Action = "GetContactDetails"; // from Up_vendor table
        oMASSIT_VendorBE.SiteVendorID = VendorID;
        VendorData[0] = "";
        VendorData[1] = "";

        lstVendorDetails = oAPPSIT_VendorBAL.GetVendorContactDetailsBAL(oMASSIT_VendorBE);
        foreach (MASSIT_VendorBE item in lstVendorDetails)
        {
            if (!string.IsNullOrEmpty(item.Vendor.VendorContactEmail))
            {
                VendorData[0] += item.Vendor.VendorContactEmail.ToString() + ",";
                VendorData[1] += item.Vendor.Language.ToString() + ",";
            }
        }
        return VendorData;
    }

    private static string[] GetVendorDefaultEmailWithLanguageID(int VendorID)
    {
        string[] VendorData = new string[2];
        MASSIT_VendorBE oMASSIT_VendorBE = new MASSIT_VendorBE();
        APPSIT_VendorBAL oAPPSIT_VendorBAL = new APPSIT_VendorBAL();
        List<MASSIT_VendorBE> lstVendorDetails = null;
        oMASSIT_VendorBE.Action = "GetContactDetails"; // from Up_vendor table
        oMASSIT_VendorBE.SiteVendorID = VendorID;
        VendorData[0] = "";
        VendorData[1] = "";

        lstVendorDetails = oAPPSIT_VendorBAL.GetVendorContactDetailsBAL(oMASSIT_VendorBE);
        foreach (MASSIT_VendorBE item in lstVendorDetails)
        {
            if (!string.IsNullOrEmpty(item.Vendor.VendorContactEmail))
            {
                VendorData[0] += item.Vendor.VendorContactEmail.ToString() + ",";
                VendorData[1] += item.Vendor.LanguageID.ToString() + ",";
            }
        }
        if (!string.IsNullOrEmpty(VendorData[0]))
        {
            VendorData[0] = System.Configuration.ConfigurationManager.AppSettings["DefaultEmailAddress"];
            VendorData[1] = "1";
        }

        return VendorData;
    }

    private static string[] GetVendorDefaultEmailWithLanguage(int VendorID, int pSiteID)
    {
        string[] VendorData = new string[2];
        MASSIT_VendorBE oMASSIT_VendorBE = new MASSIT_VendorBE();
        APPSIT_VendorBAL oAPPSIT_VendorBAL = new APPSIT_VendorBAL();
        List<MASSIT_VendorBE> lstVendorDetails = null;
        oMASSIT_VendorBE.Action = "GetContactDetailsForBooking"; // from Up_vendor table
        oMASSIT_VendorBE.SiteVendorID = VendorID;
        oMASSIT_VendorBE.SiteID = pSiteID;

        VendorData[0] = "";
        VendorData[1] = "";

        StringBuilder sb = new StringBuilder();
        sb.Append("Action :GetContactDetailsForBooking \r\n");
        sb.Append("SiteVendorID :" + VendorID + "\r\n");
        sb.Append("SiteID :" + pSiteID + "\r\n");
        LogUtility.WriteTrace(sb);


        lstVendorDetails = oAPPSIT_VendorBAL.GetVendorContactDetailsBAL(oMASSIT_VendorBE);
        foreach (MASSIT_VendorBE item in lstVendorDetails)
        {
            if (!string.IsNullOrEmpty(item.Vendor.VendorContactEmail))
            {
                VendorData[0] += item.Vendor.VendorContactEmail.ToString() + ",";
                VendorData[1] += item.Vendor.Language.ToString() + ",";

                sb = new StringBuilder();
                sb.Append("From item.Vendor.VendorContactEmail \r\n");
                sb.Append("VendorData EmailID: " + VendorData[0] + "and Language " + VendorData[1] + " \r\n");
                LogUtility.WriteTrace(sb);               
            }
        }

        string str = "=======================================================================================================================";
        sb = new StringBuilder();
        sb.Append(str + "\r\n");

        if (!string.IsNullOrWhiteSpace(VendorData[0]) || VendorData[0] == "")
        {
            VendorData[0] = System.Configuration.ConfigurationManager.AppSettings["DefaultEmailAddress"];
            VendorData[1] = "English";
            sb = new StringBuilder();
            sb.Append("From Default mail \r\n");
            sb.Append("VendorData EmailID:" + VendorData[0] + "and Language" + VendorData[1] + " \r\n");
            LogUtility.WriteTrace(sb);
        }

        string str1 = "=======================================================================================================================";
        sb = new StringBuilder();
        sb.Append(str1 + "\r\n");
        return VendorData;
    }

    public static string[] GetCarrierDefaultEmailWithLanguage(int CarrierID)
    {
        string[] CarrierData = new string[2];
        SCT_UserBE oSCT_UserBE = new SCT_UserBE();
        SCT_UserBAL oSCT_UserBAL = new SCT_UserBAL();

        oSCT_UserBE.Action = "GetCarrierEmail";
        oSCT_UserBE.UserID = Convert.ToInt32(CarrierID);
        CarrierData[0] = "";
        CarrierData[1] = "";

        List<SCT_UserBE> lstUser = oSCT_UserBAL.GetUserEmailByIDBAL(oSCT_UserBE);

        foreach (SCT_UserBE item in lstUser)
        {
            if (!string.IsNullOrEmpty(item.EmailId))
            {
                CarrierData[0] += item.EmailId.ToString() + ",";
                CarrierData[1] += item.Language.ToString() + ",";
            }
        }

        return CarrierData;
    }

    public static string[] GetCarrierEmailsWithLanguage(int CarrierID, int SiteID)
    {
        string[] CarrierData = new string[2];
        CarrierData[0] = "";
        CarrierData[1] = "";

        APPCNT_CarrierBAL oAPPCNT_CarrierBAL = new APPCNT_CarrierBAL();
        MASCNT_CarrierBE oMASCNT_CarrierBE = new MASCNT_CarrierBE();
        oMASCNT_CarrierBE.Action = "GetCarrierUsersEmailsWithLanguage";
        oMASCNT_CarrierBE.CarrierID = CarrierID;
        oMASCNT_CarrierBE.SiteID = SiteID;
        var lstCarrier = oAPPCNT_CarrierBAL.GetCarrierUsersEmailsWithLanguageBAL(oMASCNT_CarrierBE);

        foreach (MASCNT_CarrierBE item in lstCarrier)
        {
            if (!string.IsNullOrEmpty(item.User.EmailId))
            {
                CarrierData[0] += item.User.EmailId + ",";
                CarrierData[1] += item.User.Language + ",";
            }
        }
        return CarrierData;
    }

    public virtual bool PreExportToExcel()
    {
        return true;
    }

    public virtual void PostExportToExcel()
    {
        return;
    }

    public string EncryptQuery(string pURL)
    {
        return Common.EncryptQuery(pURL);
    }

    public void EncryptQueryString(string pURL)
    {
        if (pURL.Contains("?"))
        {
            string[] urlSplit = pURL.Split('?');
            string EncriptedQueryStripg = Encryption.Encrypt(urlSplit[1]);
            Response.Redirect(urlSplit[0] + "?" + EncriptedQueryStripg, false);
        }
        else
        {
            Response.Redirect(pURL, false);
        }
    }

    public string EncryptURL(string pURL)
    {
        if (pURL.Contains("?"))
        {
            string[] urlSplit = pURL.Split('?');
            string EncriptedQueryStripg = Encryption.Encrypt(urlSplit[1]);
            return urlSplit[0] + "?" + EncriptedQueryStripg;
        }
        else
        {
            return pURL;
        }
    }

    public string GetQueryStringValue(string pArgumentName)
    {
        string QueryStringValue = "";
        string strReq = "";
        bool bFound = false;

        strReq = Request.Url.ToString();
        strReq = strReq.Substring(strReq.IndexOf('?') + 1);

        if (!string.IsNullOrEmpty(strReq))
        {
            strReq = Encryption.Decrypt(strReq);
            string[] QueryStringCollection = strReq.Split('&');
            for (int count = 0; count < QueryStringCollection.Length; count++)
            {
                if (QueryStringCollection[count].Split('=')[0].ToLower() == pArgumentName.ToLower())
                {
                    QueryStringValue = QueryStringCollection[count].Split('=')[1];
                    bFound = true;
                    break;
                }
            }

            if (!bFound)
                QueryStringValue = null;
        }
        return QueryStringValue;
    }

    bool AuthenticateUserSubScreen()
    {
        bool retValue = false;
        try
        {
            string strScreenUrl = "";
            string[] arScreenUrl = new string[2];
            char[] seprator = { '?' };
            char[] sepratorSlash = { '/' };
            int UserId = 0;
            int returnValue = 0;
            SCT_UserScreenBAL oSCT_UserScreenBAL = new SCT_UserScreenBAL();
            SCT_UserScreenBE oSCT_UserScreenBE = new SCT_UserScreenBE();
            UserId = Convert.ToInt32(Session["UserID"]);
            strScreenUrl = HttpContext.Current.Request.RawUrl;
            if (strScreenUrl.Contains("?"))
            {
                arScreenUrl = strScreenUrl.Split(seprator[0]);
                strScreenUrl = arScreenUrl[0];
            }

            if (strScreenUrl.Contains("/"))
            {
                string[] arScreenUrlSlash = strScreenUrl.Split(sepratorSlash[0]);
                strScreenUrl = arScreenUrlSlash[arScreenUrlSlash.Length - 1];
            }

            oSCT_UserScreenBE.Action = "AuthenticateUserSubScreen";
            oSCT_UserScreenBE.UserID = UserId;
            oSCT_UserScreenBE.Screen = new SCT_ScreenBE();
            oSCT_UserScreenBE.Screen.ScreenUrl = strScreenUrl;
            returnValue = oSCT_UserScreenBAL.AuthenticateSubScreen(oSCT_UserScreenBE);

            retValue = returnValue > 0;
        }
        catch (Exception ex)
        {
            LogUtility.SaveErrorLogEntry(ex);
        }
        return retValue;
    }

}


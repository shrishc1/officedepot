using System;

namespace AA.switchprotocol.UI
{
    public class SecurePage : CommonPage
    {
        protected override void OnLoad(EventArgs e)
        {
            IsSecure = true;
            base.OnLoad(e);
        }
    }
}
using System.Web.Configuration;

namespace AA.switchprotocol
{
    public static class Globals
    {
        public readonly static SwitchProtocolSection Settings =
            (SwitchProtocolSection)WebConfigurationManager.
                GetSection("SwitchProtocol");
    }
}

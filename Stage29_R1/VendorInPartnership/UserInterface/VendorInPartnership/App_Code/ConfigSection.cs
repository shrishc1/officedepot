using System.Configuration;

namespace AA.switchprotocol
{
    public class SwitchProtocolSection : ConfigurationSection
    {
        [ConfigurationProperty("urls", IsRequired = true)]
        public UrlsFormElement Urls
        {
            get
            {
                return (UrlsFormElement)base["urls"];
            }
        }
    }

    public class UrlsFormElement : ConfigurationElement
    {
        [ConfigurationProperty("baseUrl", IsRequired = true)]
        public string BaseUrl
        {
            get
            {
                return (string)base["baseUrl"];
            }
        }

        [ConfigurationProperty("baseSecureUrl", IsRequired = true)]
        public string BaseSecureUrl
        {
            get
            {
                return (string)base["baseSecureUrl"];
            }
        }

        [ConfigurationProperty("isNavigationRequired", IsRequired = true)]
        public bool IsNavigationRequired
        {
            get
            {
                return (bool)base["isNavigationRequired"];
            }
        }
    }
}

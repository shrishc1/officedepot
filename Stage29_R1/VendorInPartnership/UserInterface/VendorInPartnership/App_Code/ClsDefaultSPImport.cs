﻿using BusinessEntities.ModuleBE.Upload;
using BusinessLogicLayer.ModuleBAL.Upload;
using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;

public class ClsDefaultSPImport
{
    public ClsDefaultSPImport()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public string UploadSPImport(string boFilePath, string boFileName)
    {
        DataTable dt = new DataTable();
        DataRow dr;

        DataTable errorDt = new DataTable();
        DataRow errorDr;

        ImportDB importDB = new ImportDB();
        DateTime ImportDate = importDB.GetFileDate(boFileName);

        //-------------------------------------
        int? ManualFileUploadID = 0;
        int SKU_Index = 0;
        int Site_Index = 0;
        int Description_Index = 0;
        int StockPlanner_Index = 0;
        int ReasonCode_Index = 0;
        int Comments_Index = 0;
        int TotalRecordCount = 0;
        int Purchasing_Group_Index = 0;
        int Plant_Index = 0;

        try
        {
            try
            {
                using (StreamReader oStreamReader = new StreamReader(boFilePath))
                {
                    GlobalVariable.textFileLine = oStreamReader.ReadLine().ToLower();
                    var columns = GlobalVariable.textFileLine.Split(GlobalVariable.delimiters);

                    if (columns.Length != 8)
                        return "Invalid File";

                    foreach (string col in columns)
                    {
                        switch (col.ToLower())
                        {
                            case "plant":
                                dt.Columns.Add(new DataColumn("Plant"));
                                Plant_Index = Array.IndexOf(columns, "plant");
                                break;
                            case "sku":
                                dt.Columns.Add(new DataColumn("SKU"));
                                SKU_Index = Array.IndexOf(columns, "sku");
                                break;
                            case "sitename":
                                dt.Columns.Add(new DataColumn("Sitename"));
                                Site_Index = Array.IndexOf(columns, "sitename");
                                break;
                            case "description":
                                dt.Columns.Add(new DataColumn("Description"));
                                Description_Index = Array.IndexOf(columns, "description");
                                break;

                            case "stockplanner":
                                dt.Columns.Add(new DataColumn("StockPlanner"));
                                StockPlanner_Index = Array.IndexOf(columns, "stockplanner");
                                break;

                            case "purchasing group":
                                dt.Columns.Add(new DataColumn("PurchagingGroup"));
                                Purchasing_Group_Index = Array.IndexOf(columns, "purchasing group");
                                break;

                            case "reasoncode":
                                dt.Columns.Add(new DataColumn("ReasonCode"));
                                ReasonCode_Index = Array.IndexOf(columns, "reasoncode");
                                break;

                            case "comments":
                                dt.Columns.Add(new DataColumn("ExpediteComments"));
                                Comments_Index = Array.IndexOf(columns, "comments");
                                break;

                            default:
                                break;
                        }
                    }

                    dt.Columns.Add(new DataColumn("UpdatedDate", typeof(System.DateTime)));
                    errorDt.Columns.Add(new DataColumn("MasDefaultUploadID", typeof(System.Int32)));
                    errorDt.Columns.Add(new DataColumn("ErrorDescription", typeof(System.String)));
                    errorDt.Columns.Add(new DataColumn("UpdatedDate", typeof(System.DateTime)));

                    int iCount = 0;
                    DateTime? tempDate = DateTime.Now;

                    while ((GlobalVariable.textFileLine = oStreamReader.ReadLine()) != null)
                    {
                        string errorMessage = string.Empty;
                        errorDr = errorDt.NewRow();
                        iCount++;
                        dr = dt.NewRow();

                        GlobalVariable.textFileFieldValues = GlobalVariable.textFileLine.Split(GlobalVariable.delimiters);
                        if (GlobalVariable.textFileFieldValues.Length != dt.Columns.Count - 1)
                        {
                            errorMessage += "Data Incomplete at position " + iCount.ToString();
                            errorDr["ErrorDescription"] = errorMessage.Substring(0, errorMessage.Length - 1) + " in file " + boFilePath;
                            errorDr["MasDefaultUploadID"] = ManualFileUploadID;
                            errorDt.Rows.Add(errorDr);
                            continue;
                        }


                        for (int i = 0; i <= GlobalVariable.textFileFieldValues.Length - 1; i++)
                        {
                            GlobalVariable.textFileFieldValues[i] = GlobalVariable.textFileFieldValues[i].Replace("'", "''");
                        }

                        GlobalVariable.textFileFieldValues[SKU_Index] = FormatDirectSKU(GlobalVariable.textFileFieldValues[SKU_Index]);
                        GlobalVariable.textFileFieldValues[Site_Index] = (GlobalVariable.textFileFieldValues[Site_Index]);
                        GlobalVariable.textFileFieldValues[Description_Index] = (GlobalVariable.textFileFieldValues[Description_Index]);
                        GlobalVariable.textFileFieldValues[StockPlanner_Index] = (GlobalVariable.textFileFieldValues[StockPlanner_Index]);
                        GlobalVariable.textFileFieldValues[Plant_Index] = (GlobalVariable.textFileFieldValues[Plant_Index]);
                        GlobalVariable.textFileFieldValues[Purchasing_Group_Index] = (GlobalVariable.textFileFieldValues[Purchasing_Group_Index]);
                        GlobalVariable.textFileFieldValues[ReasonCode_Index] = (GlobalVariable.textFileFieldValues[ReasonCode_Index]);
                        GlobalVariable.textFileFieldValues[Comments_Index] = (GlobalVariable.textFileFieldValues[Comments_Index]);

                        //--------------------- Added on 13 Feb 2013 - End ---------------- 

                        dr.ItemArray = GlobalVariable.textFileFieldValues;
                        dr["UpdatedDate"] = ImportDate;
                        dt.Rows.Add(dr);

                        TotalRecordCount += 1;

                    }

                    SqlBulkCopy oSqlBulkCopy = new SqlBulkCopy(GlobalVariable.sqlCon, SqlBulkCopyOptions.TableLock, null)
                    {
                        DestinationTableName = "Up_StageSKUDefaultPlanner"
                    };

                    oSqlBulkCopy.ColumnMappings.Add("Plant", "Plant");
                    oSqlBulkCopy.ColumnMappings.Add("SiteName", "SiteName");
                    oSqlBulkCopy.ColumnMappings.Add("SKU", "SKU");
                    oSqlBulkCopy.ColumnMappings.Add("Description", "Description");
                    oSqlBulkCopy.ColumnMappings.Add("PurchagingGroup", "PurchagingGroup");
                    oSqlBulkCopy.ColumnMappings.Add("StockPlanner", "StockPlanner");
                    oSqlBulkCopy.ColumnMappings.Add("ExpediteComments", "ExpediteComments");
                    oSqlBulkCopy.ColumnMappings.Add("ReasonCode", "ReasonCode");
                    oSqlBulkCopy.ColumnMappings.Add("UpdatedDate", "UpdatedDate");

                    oSqlBulkCopy.BulkCopyTimeout = GlobalVariable.CommandTimeOutTime; ;
                    oSqlBulkCopy.BatchSize = dt.Rows.Count;
                    oSqlBulkCopy.WriteToServer(dt);
                    oSqlBulkCopy.Close();
                }
            }
            catch
            {
                return "Invalid File";
            }

            try
            {
                UP_ManualFileUploadBE oUP_ManualFileUploadBE = new UP_ManualFileUploadBE();
                oUP_ManualFileUploadBE.Action = "InsertManualDefaultStockPlannerUpload";
                oUP_ManualFileUploadBE.DateUploaded = DateTime.Now;
                oUP_ManualFileUploadBE.DownloadedFilename = boFileName;
                oUP_ManualFileUploadBE.UserID = Convert.ToInt32(System.Web.HttpContext.Current.Session["UserId"]);
                oUP_ManualFileUploadBE.TotalBORecord = dt.Rows.Count;
                oUP_ManualFileUploadBE.TotalSKURecord = errorDt.Rows.Count;
                UP_ManualFileUploadBAL oUP_ManualFileUploadBAL = new UP_ManualFileUploadBAL();
                ManualFileUploadID = oUP_ManualFileUploadBAL.addEditManualUploadBAL(oUP_ManualFileUploadBE);

            }
            catch
            {
                return string.Empty;
            }

            foreach (DataRow row in errorDt.Rows)
            {
                row["MasDefaultUploadID"] = ManualFileUploadID;
            }

            if (errorDt != null && errorDt.Rows.Count > 0)
            {
                SqlBulkCopy oSqlBulkErrorCopy = new SqlBulkCopy(GlobalVariable.sqlCon, SqlBulkCopyOptions.TableLock, null);
                oSqlBulkErrorCopy.DestinationTableName = "Up_ErrorSKUDefaultPlanner";
                oSqlBulkErrorCopy.ColumnMappings.Add("MasDefaultUploadID", "MasDefaultUploadID");
                oSqlBulkErrorCopy.ColumnMappings.Add("UpdatedDate", "UpdatedDate");
                oSqlBulkErrorCopy.ColumnMappings.Add("ErrorDescription", "ErrorDescription");
                oSqlBulkErrorCopy.BulkCopyTimeout = GlobalVariable.CommandTimeOutTime;
                oSqlBulkErrorCopy.BatchSize = errorDt.Rows.Count;
                oSqlBulkErrorCopy.WriteToServer(errorDt);
                oSqlBulkErrorCopy.Close();
            }
            //--------------------- Added on 14 Feb 2013 -----------------------

            return string.Empty;
        }
        catch
        {
            return string.Empty;
        }
    }

    private string RemoveDecimal(string val)
    {
        string retVal;
        retVal = RemoveSpace(val);

        if (retVal != null)
        {
            if (retVal.Contains("."))
            {
                retVal = retVal.Substring(0, retVal.IndexOf("."));
            }
            if (retVal.Contains("-"))
            {
                retVal = '-' + (retVal.Trim('-'));
            }
            if (retVal.Contains("/"))
            {
                string[] arrDate = retVal.Split('/');
                retVal = arrDate[2] + "-" + arrDate[1] + "-" + arrDate[0];
            }
        }
        if (string.IsNullOrEmpty(retVal))
        {
            retVal = "0";
        }
        return retVal;
    }

    private string FormatDirectSKU(string val)
    {
        string retVal;
        retVal = RemoveSpace(val);
        if (string.IsNullOrEmpty(retVal))
        {
            retVal = "0";
        }
        return retVal;
    }

    private string RemoveSpace(string val)
    {
        string retVal;
        if (!string.IsNullOrEmpty(val))
            retVal = val.Trim(' ');
        else
            retVal = null;
        return retVal;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="ColumnName"></param>
    /// <param name="Description"></param>
    /// <param name="ColumnLength"></param>
    /// <param name="FileName"></param>
    /// <returns></returns>
    public string GetErrorMessage(string ColumnName, string Description, int ColumnLength)
    {
        var Message = ColumnName + " length greater than " + ColumnLength + " for " + Description + ",";
        return Message;
    }
}


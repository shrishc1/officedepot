﻿/// <summary>
/// This class having the Constants variables.
/// </summary>
public class clsConstants
{
    /* Discrepancies Prefix according to [MAS_DiscrepancyType] database table. */
    public const string OversPrefix = "A";
    public const string ShortagePrefix = "J";
    public const string GoodReceivedDamagedPrefix = "B";
    public const string NoPurchaseOrderPrefix = "C";
    public const string NoPaperworkPrefix = "D";
    public const string IncorrectProductPrefix = "E";
    public const string PresentationIssuePrefix = "F";
    public const string IncorrectAddressPrefix = "G";
    public const string PaperworkAmendedPrefix = "H";
    public const string WrongPackSizePrefix = "I";
    public const string FailPalletSpecificationPrefix = "K";
    public const string QualityIssuePrefix = "L";
    public const string PrematureInvoiceReceiptPrefix = "M";
    public const string GenericDiscrepancyPrefix = "N";
    public const string ShuttleDiscrepancyPrefix = "S";
    public const string ReservationDiscrepancyPrefix = "R";
    public const string InvoiceDiscrepancyPrefix = "T";

    /* ISO Language name */
    //public const string EnglishISO = "en-US";
    //public const string FranceISO = "fr-FR";
    //public const string GermanyISO = "de-DE";
    //public const string SpainISO = "es-MX";
    //public const string ItalyISO = "it-IT";
    //public const string NederlandISO = "nl-NL";
    //public const string CzechISO = "cs-CZ";
    //public const string SwedenISO = "se-SE";

    public const string EnglishISO = "en-US";   // English
    public const string FranceISO = "fr";       // France
    public const string GermanyISO = "de";      // German
    public const string SpainISO = "es";        // Spanish
    public const string ItalyISO = "it";        // Italy
    public const string NederlandISO = "nl";    // Dutch : Netherland
    public const string CzechISO = "cs";        // Czech
    public const string SwedenISO = "se";       // Sweden

    /* Languages according to [MAS_Language] database table. */
    public const string English = "English";
    public const string French = "French";
    public const string German = "German";
    public const string Spanish = "Spanish";
    public const string Italian = "Italian";
    public const string Dutch = "Dutch";
    public const string Czech = "Czech";
    public const string Sweden = "Sweden";

}
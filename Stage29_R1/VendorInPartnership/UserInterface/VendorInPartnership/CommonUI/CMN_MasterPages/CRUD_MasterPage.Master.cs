﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using BusinessEntities.ModuleBE.Security;
using BusinessLogicLayer.ModuleBAL.Security;
using BusinessEntities.ModuleBE.ReportRequest;
using BusinessLogicLayer.ModuleBAL.ReportRequest;
using System.Collections;
using Utilities;
using WebUtilities;
using System.Text;
using System.Linq;
using BusinessLogicLayer.ModuleBAL.StockOverview;
using BusinessEntities.ModuleBE.StockOverview;
using BusinessEntities.ModuleBE.Languages;
using BusinessLogicLayer.ModuleBAL.Languages;
using System.Web;
using System.Configuration;

public partial class Common_UI_CMN_MasterPages_CRUD_MasterPage : System.Web.UI.MasterPage
{
    protected string ConfirmMessage = WebCommon.getGlobalResourceValue("ConfirmMessage");
     protected void Page_Load(object sender, EventArgs e)
     {
        var IsDev = ConfigurationManager.AppSettings.AllKeys.Any(key => key == "IsDev")
            && Convert.ToBoolean(ConfigurationManager.AppSettings["IsDev"].ToString());
        if (IsDev)
        {
            divFixed.Visible = true;
            toplink.Style.Remove("background");
            toplink.Style.Add("background-color", "blue");
        }
        else
        {
            divFixed.Visible = false;
            toplink.Style.Remove("background-color");
            toplink.Style.Add("background","url(/Images/header-topbg.jpg) top repeat-x");
        }

        if (Session["UserID"] != null && Session["UserID"].ToString().Trim() != "")
        {
            UserName.InnerText = Session["LoginID"].ToString().Trim();
        }
        else
        {
            string LoginPath = ResolveClientUrl("../../ModuleUI/Security/Login.aspx");           
            Response.Redirect(LoginPath);
        }

        //Add by Abhinav to fix the language cache lost problem
        if    (System.Web.HttpRuntime.Cache["LanguageTagsEnglish"] == null
            || System.Web.HttpRuntime.Cache["LanguageTagsFrench"] == null
            || System.Web.HttpRuntime.Cache["LanguageTagsGerman"] == null
            || System.Web.HttpRuntime.Cache["LanguageTagsDutch"] == null
            || System.Web.HttpRuntime.Cache["LanguageTagsSpanish"] == null
            || System.Web.HttpRuntime.Cache["LanguageTagsItalian"] == null
            || System.Web.HttpRuntime.Cache["LanguageTagsCzech"] == null) 
        {
            Utility.LoadLanguageTags();
        }

        if (!Page.IsPostBack)
        {
            GetGeneratedReportCount();           
            GenerateMenu();           
        }

        lblModuleText.Text = Session["ModuleName"].ToString().ToLower() == "homepage" ? Session["ModuleName"].ToString() : "";

        Session["ModuleName"] = "";
    }

    protected override void OnPreRender(EventArgs e)
    {
        if (!Page.IsPostBack)
        { 
        }
    }

    #region Method

    System.Text.StringBuilder sbMemu = new System.Text.StringBuilder();

    string EndModuleString = "</ul></li>";

    private string GetStartModuleString(string ModuleName)
    {
        if (ModuleName.Equals("AdminSettings"))
        {
            ModuleName = WebCommon.getGlobalResourceValue(ModuleName);
            return "<li style=\"z-index:9999\" id=\"liAdminSetting\"><a href='#'>" + ModuleName + "</a><ul>";
        }
        else
        {
            ModuleName = WebCommon.getGlobalResourceValue(ModuleName);
            return "<li style=\"z-index:9999\"><a href='#'>" + ModuleName + "</a><ul>";
        }
    }
    int bookingCount = 0;
    private string GetScreenString(string ScreenID, string ScreenName, string ScreenUrl, string sModuletext)
    {        
        ScreenUrl = ScreenUrl.Replace("../../", "");
        int ind = HttpContext.Current.Request.Url.AbsoluteUri.IndexOf("ModuleUI");
        string HostURL = HttpContext.Current.Request.Url.AbsoluteUri.Substring(0, ind);

        string Path = Common.EncryptQuery(ResolveClientUrl(HostURL + ScreenUrl) + "?ModuleName=" + sModuletext);
        string strReturnString = "";
        if (ScreenName == "ManageDelivery" || ScreenName == "ManageBooking")
        {
            if (bookingCount == 0)
            {
                ScreenName = WebCommon.getGlobalResourceValue("BookingDelivery");
                strReturnString = "<li style=\"z-index:9999\"><a style=\"z-index:9999\" href='" + Path + "'>" + ScreenName + "</a></li>";
                bookingCount++;
            }
        }
        else
        {
            ScreenName = WebCommon.getGlobalResourceValue(ScreenName);
            strReturnString = "<li style=\"z-index:9999\"><a style=\"z-index:9999\" href='" + Path + "'>" + ScreenName + "</a></li>";
        }
        return strReturnString;
    }
    private bool IsAllowedScreenToCurrenUser(string ScreenID)
    {
        return true;
    }
    private void GenerateMenu()
    {
        if (Session["MenuHTML"] != null) {
            ltMenu.Text = Session["MenuHTML"].ToString();
            return;
        }
        SCT_UserBE oSCT_UserBE = new SCT_UserBE();
        SCT_UserBAL oSCT_UserBAL = new SCT_UserBAL();
        oSCT_UserBE.UserID = Convert.ToInt32(Session["UserID"]);
        oSCT_UserBE.Action = "GetUserOverview";
        List<SCT_UserBE> lstUser = new List<SCT_UserBE>();
        lstUser = oSCT_UserBAL.GetUsers(oSCT_UserBE);

        if (lstUser[0].AccountStatus.Trim().ToUpper() != "Awaiting Activation".ToUpper())
        {
            lnkChangePassword.Visible = true;
            string sModuletext = "";
            if (Session["Role"].ToString().ToLower() == "vendor")
            {
                ltMenu.Text = GetVendorMenu();
            }
            else if (Session["Role"].ToString().ToLower() == "carrier")
            {
                ltMenu.Text = GetCarrierMenu();
            }
            else
            {
                GetModule("0", sModuletext);               
                ltMenu.Text = sbMemu.ToString();
            }

            Session["MenuHTML"] = ltMenu.Text;
           
        }
        else
        {
            lnkChangePassword.Visible = false;
        }
    }

    private string GetVendorMenu()
    {
        int ind = HttpContext.Current.Request.Url.AbsoluteUri.IndexOf("ModuleUI");
        string HostURL = HttpContext.Current.Request.Url.AbsoluteUri.Substring(0, ind);

        string BookingPath = Common.EncryptQuery(ResolveClientUrl(HostURL + "ModuleUI/Appointment/Booking/APPBok_BookingOverview.aspx?ModuleName=" + WebCommon.getGlobalResourceValue("AppScheduling")));
        string DiscPath = Common.EncryptQuery(ResolveClientUrl(HostURL + "ModuleUI/Discrepancy/DIS_WorkListEdit.aspx?ModuleName=" + WebCommon.getGlobalResourceValue("Discrepancies")));
        string OtifReportPath = Common.EncryptQuery(ResolveClientUrl(HostURL + "ModuleUI/OnTimeInFull/ViewOTIFReport.aspx?ModuleName=" + WebCommon.getGlobalResourceValue("OnTimeInFull")));
        string StockOverviewPath = Common.EncryptQuery(ResolveClientUrl(HostURL + "ModuleUI/StockOverview/BackOrder/VendorView.aspx?ModuleName=" + WebCommon.getGlobalResourceValue("StockOverview")));
        string VendorScorecardPath = Common.EncryptQuery(ResolveClientUrl(HostURL + "ModuleUI/VendorScorecard/Report/VendorScoreCardSearch.aspx?ModuleName=" + WebCommon.getGlobalResourceValue("VendorScorecard")));
        string DiscSearchPath = Common.EncryptQuery(ResolveClientUrl(HostURL + "ModuleUI/Discrepancy/DIS_SearchResultVendor.aspx?ModuleName=" + WebCommon.getGlobalResourceValue("Discrepancies")));
        string HolidayClosurePath = Common.EncryptQuery(ResolveClientUrl(HostURL + "ModuleUI/Appointment/SiteSettings/APPSIT_HolidaySetupOverview.aspx?ModuleName=" + WebCommon.getGlobalResourceValue("SiteHolidayClosures")));
        string BookingHistoryPath = Common.EncryptQuery(ResolveClientUrl(HostURL + "ModuleUI/Appointment/Booking/APPBok_VendorBookingOverview.aspx?ModuleName=" + WebCommon.getGlobalResourceValue("AppScheduling")));
        string QueryDiscrepancyPath = Common.EncryptQuery(ResolveClientUrl(HostURL + "ModuleUI/Discrepancy/DISLog_QueryDiscrepancy.aspx?ModuleName=" + WebCommon.getGlobalResourceValue("Discrepancies")));
        string ChangePasswordPath = Common.EncryptQuery(ResolveClientUrl(HostURL + "ModuleUI/Security/ChangePassword.aspx?ModuleName=" + WebCommon.getGlobalResourceValue("MyAccount")));
        string AmendmyAccountDetailsPath = Common.EncryptQuery(ResolveClientUrl(HostURL + "ModuleUI/Security/SCT_RegisterExternalUserEdit.aspx?ModuleName=" + WebCommon.getGlobalResourceValue("MyAccount")));
        string ViewEDIAgreementPath = Common.EncryptQuery(ResolveClientUrl(HostURL + "Documents/Vendor Score Card.pdf?ModuleName=" + WebCommon.getGlobalResourceValue("MyAccount")));
        string ViewTradeEntitiesPath = Common.EncryptQuery(ResolveClientUrl(HostURL + "ModuleUI/GlobalSettings/TradeEntitiesOverview.aspx?ModuleName=" + WebCommon.getGlobalResourceValue("MyAccount")));
        string SearchDebitPath = Common.EncryptQuery(ResolveClientUrl(HostURL + "ModuleUI/Discrepancy/AccountsPayable/SearchDebit.aspx?ModuleName=" + WebCommon.getGlobalResourceValue("AccountsPayable")));
        string OpenPurchaseOrderVendorView = Common.EncryptQuery(ResolveClientUrl(HostURL + "ModuleUI/Appointment/Reports/OpenPurchaseOrderVendorView.aspx?ModuleName=" + WebCommon.getGlobalResourceValue("OpenPurchaseOrderVendorView")));
        string StockTurnVendorView = Common.EncryptQuery(ResolveClientUrl(HostURL + "ModuleUI/StockOverview/StockTurn/StockTurnVendorReport.aspx?ModuleName=" + WebCommon.getGlobalResourceValue("StockOverview")));
        string ForecastAccuracyReport = Common.EncryptQuery(ResolveClientUrl(HostURL + "ModuleUI/StockOverview/SKT_ForecastAccuracyReport.aspx?ModuleName=" + WebCommon.getGlobalResourceValue("ForecastAccuracyReport")));

        ArrayList VendorMenuCollection = new ArrayList();
   
        VendorMenuCollection.Add("DefaultDashboard.aspx");
        VendorMenuCollection.Add("SupplierGuideline.aspx");
        VendorMenuCollection.Add("ChangePassword.aspx");


        SCT_UserBE oSCT_UserBE = new SCT_UserBE();
        SCT_UserBAL oSCT_UserBAL = new SCT_UserBAL();
        oSCT_UserBE.UserID = Convert.ToInt32(Session["UserID"]);
        oSCT_UserBE.Action = "GetUserOverview";
        List<SCT_UserBE> lstUser = new List<SCT_UserBE>();
        lstUser = oSCT_UserBAL.GetUsers(oSCT_UserBE);


        StringBuilder sb = new StringBuilder();
        //Create Appoinment Scheduling Menu
        if (lstUser[0].AccountStatus.Trim().ToUpper() != "Awaiting Activation".ToUpper())
        {

            if (lstUser.Count > 0 && lstUser[0].SchedulingContact == 'Y')
            {
                sb.AppendLine("<li style=\"z-index:9999\"><a href='#'>" + WebCommon.getGlobalResourceValue("AppScheduling") + "</a><ul>");
                // check if sites are assigned to vendor
                SCT_UserBE oNewSCT_UserBE = new SCT_UserBE();
                oNewSCT_UserBE.UserID = Convert.ToInt32(Session["UserID"]);

                // Used top 1 vendor id for now. Need to come back on this again.
                oSCT_UserBE.Action = "GetCountrySpecificSettings";
                List<SCT_UserBE> lstUserSettings = oSCT_UserBAL.GetCountrySpecificSettingsBAL(oSCT_UserBE);
                if (lstUserSettings.Count > 0)
                {
                    oNewSCT_UserBE.VendorID = lstUserSettings[0].VendorID;
                    
                }

                oNewSCT_UserBE.Action = "GetVendorSite";
                oNewSCT_UserBE = oSCT_UserBAL.GetUserDefaultsBAL(oNewSCT_UserBE);
               
                // Create Booking Page only when site are assigned to that venor
                if (oNewSCT_UserBE.Site != null && oNewSCT_UserBE.Site.SiteID != 0)
                {
                    sb.AppendLine("<li style=\"z-index:9999\"><a href='" + BookingPath + "'>" + WebCommon.getGlobalResourceValue("YourScheduleAddBookings") + "</a></li>");
                    sb.AppendLine("<li style=\"z-index:9999\"><a href='" + BookingHistoryPath + "'>" + WebCommon.getGlobalResourceValue("PreviousBookings") + "</a></li>");
                    sb.AppendLine("<li style=\"z-index:9999\"><a href='" + HolidayClosurePath + "'>" + WebCommon.getGlobalResourceValue("DaysClosedForOD") + "</a></li>");
                    sb.AppendLine("<li style=\"z-index:9999\"><a href='" + OpenPurchaseOrderVendorView + "'>" + WebCommon.getGlobalResourceValue("OpenPurchaseOrderVendorView") + "</a></li>");


                    VendorMenuCollection.Add("APPBok_BookingOverview.aspx");
                    VendorMenuCollection.Add("APPBok_BookingEdit.aspx");
                    VendorMenuCollection.Add("APPRcv_BookingHistory.aspx");
                    VendorMenuCollection.Add("APPBok_VendorBookingOverview.aspx");
                    VendorMenuCollection.Add("APPBok_WindowBookingEdit.aspx");
                    VendorMenuCollection.Add("APPRcv_Communication.aspx");
                    VendorMenuCollection.Add("APPBok_Confirm.aspx");
                    VendorMenuCollection.Add("APPSIT_HolidaySetupOverview.aspx");
                    VendorMenuCollection.Add("OpenPurchaseOrderVendorView.aspx"); 
                }
                else
                {
                    string SiteNotExistMessage = WebCommon.getGlobalResourceValue("SiteNotExistMessage");
                    sb.AppendLine("<li style=\"z-index:9999\"><a href='#' onclick=\"alert('" + SiteNotExistMessage + "');\">" + WebCommon.getGlobalResourceValue("BookingOD") + "</a></li>");

                 }
                sb.AppendLine("</ul>");
            }

            //Create Discrepancy Menu

            if (lstUser.Count > 0 && lstUser[0].DiscrepancyContact == 'Y')
            {
                sb.AppendLine("<li style=\"z-index:9999\"><a href='#'>" + WebCommon.getGlobalResourceValue("Discrepancies") + "</a>");
                sb.AppendLine("<ul>");
                sb.AppendLine("<li style=\"z-index:9999\"><a href='" + DiscPath + "'>" + WebCommon.getGlobalResourceValue("OpenDiscrepancyWorklist") + "</a></li>");
                sb.AppendLine("<li style=\"z-index:9999\"><a href='" + DiscSearchPath + "'>" + WebCommon.getGlobalResourceValue("SearchDiscrepancies") + "</a></li>");
                sb.AppendLine("<li style=\"z-index:9999\"><a href='" + QueryDiscrepancyPath + "'>" + WebCommon.getGlobalResourceValue("AddQueryToDiscrepancy") + "</a></li>");
                sb.AppendLine("<li style=\"z-index:9999\"><a href='" + SearchDebitPath + "'>" + WebCommon.getGlobalResourceValue("SearchDebit") + "</a></li>");


                VendorMenuCollection.Add("DIS_WorkListEdit.aspx");
                VendorMenuCollection.Add("DIS_SearchResultVendor.aspx");
                VendorMenuCollection.Add("DiscrepancyDisputesReport.aspx");

                VendorMenuCollection.Add("DISLog_QueryDiscrepancy.aspx");           
                VendorMenuCollection.Add("DISLog_FailPalletSpecification.aspx");
                VendorMenuCollection.Add("DISLog_GenericDescrepancy.aspx");
                VendorMenuCollection.Add("DISLog_GoodsReceivedDamaged.aspx");
                VendorMenuCollection.Add("DISLog_IncorrectAddress.aspx");
                VendorMenuCollection.Add("DISLog_IncorrectProduct.aspx");
                VendorMenuCollection.Add("DISLog_InvoiceDiscrepancy.aspx");
                VendorMenuCollection.Add("DISLog_NoPaperwork.aspx");
                VendorMenuCollection.Add("DISLog_NoPurchaseOrder.aspx");
                VendorMenuCollection.Add("DISLog_Overs.aspx");
                VendorMenuCollection.Add("DISLog_PaperworkAmended.aspx");
                VendorMenuCollection.Add("DISLog_PrematureInvoiceReceipt.aspx");
                VendorMenuCollection.Add("DISLog_PresentationIssue.aspx");
                VendorMenuCollection.Add("DISLog_QualityIssue.aspx");
                VendorMenuCollection.Add("DISLog_Reservation.aspx");
                VendorMenuCollection.Add("DISLog_Shortage.aspx");
                VendorMenuCollection.Add("DISLog_Shuttle.aspx");
                VendorMenuCollection.Add("DISLog_WrongPackSize.aspx");

                VendorMenuCollection.Add("SearchDebit.aspx");
                VendorMenuCollection.Add("DebiRaise.aspx");

                sb.AppendLine("</ul>");
            }

            //Create OTIF Menu
            if (lstUser.Count > 0 && lstUser[0].OTIFContact == 'Y')
            {
                sb.AppendLine("<li style=\"z-index:9999\"><a href='#'>" + WebCommon.getGlobalResourceValue("OTIF") + "</a>");
                sb.AppendLine("<ul>");
                sb.AppendLine("<li style=\"z-index:9999\"><a href='" + OtifReportPath + "'>" + WebCommon.getGlobalResourceValue("YourOTIFReports") + "</a></li>");
                VendorMenuCollection.Add("ViewOTIFReport.aspx");
                sb.AppendLine("</ul>");
            }

            //Create StockOverview Menu
            if (lstUser.Count > 0 && lstUser[0].OTIFContact == 'Y')
            {
                if (IsVendorSubscribeForPenalty())
                {
                    sb.AppendLine("<li style=\"z-index:9999\"><a href='#'>" + WebCommon.getGlobalResourceValue("StockOverview") + "</a>");
                    sb.AppendLine("<ul>");
                    sb.AppendLine("<li style=\"z-index:9999\"><a href='#'>" + WebCommon.getGlobalResourceValue("BackOrder") + "</a>");
                    sb.AppendLine("<ul>");
                    sb.AppendLine("<li style=\"z-index:9999\"><a href='" + StockOverviewPath + "'>" + WebCommon.getGlobalResourceValue("VendorView") + "</a></li>");                    
                    sb.AppendLine("</ul>");

                    sb.AppendLine("<li style=\"z-index:9999\"><a href='#'>" + WebCommon.getGlobalResourceValue("Stock Turn") + "</a>");

                    sb.AppendLine("<ul>");
                    sb.AppendLine("<li style=\"z-index:9999\"><a href='" + StockTurnVendorView + "'>" + WebCommon.getGlobalResourceValue("VendorView") + "</a></li>");
                    sb.AppendLine("</ul>");   
                    sb.AppendLine("</li>");
                    //sb.AppendLine("<li style=\"z-index:9999\"><a href='" + ForecastAccuracyReport + "'>" + WebCommon.getGlobalResourceValue("ForecastAccuracyReport") + "</a>");
                    sb.AppendLine("</ul>");
                  
                    VendorMenuCollection.Add("VendorView.aspx");
                }
            }

            //Create Vendor ScoreCard Menu
            if (lstUser.Count > 0 && lstUser[0].ScorecardContact == 'Y')
            {
                sb.AppendLine("<li style=\"z-index:9999\"><a href='#'>" + WebCommon.getGlobalResourceValue("Scorecard") + "</a>");
                sb.AppendLine("<ul>");
                sb.AppendLine("<li style=\"z-index:9999\"><a href='" + VendorScorecardPath + "'>" + WebCommon.getGlobalResourceValue("YourScorecard") + "</a></li>");
                VendorMenuCollection.Add("VendorScoreCardSearch.aspx");
                VendorMenuCollection.Add("/VendorScoreCardMain.aspx");
                VendorMenuCollection.Add("ScorecardMonthly.rdlc");
                VendorMenuCollection.Add("ScorecardQuarterly.rdlc");
                VendorMenuCollection.Add("ExportScorecardMonthly.rdlc");
                VendorMenuCollection.Add("ExportScorecardQuarterly.rdlc");
                sb.AppendLine("</ul>");
            }

            //My Account Menu
            sb.AppendLine("<li style=\"z-index:9999\"><a href='#'>" + WebCommon.getGlobalResourceValue("MyAccount") + "</a>");
            sb.AppendLine("<ul>");
            sb.AppendLine("<li style=\"z-index:9999\"><a href='" + ChangePasswordPath + "'>" + WebCommon.getGlobalResourceValue("ChangePassword") + "</a></li>");
            sb.AppendLine("<li style=\"z-index:9999\"><a href='" + AmendmyAccountDetailsPath + "'>" + WebCommon.getGlobalResourceValue("ViewAmendAccountDetails") + "</a></li>");
            sb.AppendLine("<li style=\"z-index:9999\"><a href='" + ViewEDIAgreementPath + "'>" + WebCommon.getGlobalResourceValue("ViewEDIAgreement") + "</a></li>");
            sb.AppendLine("<li style=\"z-index:9999\"><a href='" + ViewTradeEntitiesPath + "'>" + WebCommon.getGlobalResourceValue("TradeEntities") + "</a></li>");
            
            VendorMenuCollection.Add("ChangePassword.aspx");
            VendorMenuCollection.Add("SCT_RegisterExternalUserEdit.aspx");
            VendorMenuCollection.Add("TradeEntitiesOverview.aspx"); 
            //AmendmyAccountDetails
            sb.AppendLine("</ul>");


            sb.AppendLine("</ul></li>");

            string requestURL = Request.Url.AbsolutePath;
            bool vendorAuthorized = false;

            foreach (string VendorMenu in VendorMenuCollection)
            {
                if (requestURL.ToLower().Contains(VendorMenu.ToLower()))
                {
                    vendorAuthorized = true;
                    break;
                }
            }

            if (!vendorAuthorized)
            {
                Session.Abandon();
                Response.Redirect("~/ModuleUI/Security/Login.aspx");
            }
        }
        return sb.ToString();
    }

    private string GetCarrierMenu()
    {
        SCT_UserBE oSCT_UserBE = new SCT_UserBE();
        SCT_UserBAL oSCT_UserBAL = new SCT_UserBAL();
        oSCT_UserBE.UserID = Convert.ToInt32(Session["UserID"]);
        oSCT_UserBE.Action = "GetUserOverview";
        List<SCT_UserBE> lstUser = new List<SCT_UserBE>();
        lstUser = oSCT_UserBAL.GetUsers(oSCT_UserBE);

        int ind = HttpContext.Current.Request.Url.AbsoluteUri.IndexOf("ModuleUI");
        string HostURL = HttpContext.Current.Request.Url.AbsoluteUri.Substring(0, ind);

        string BookingHistoryPath = Common.EncryptQuery(ResolveClientUrl(HostURL + "ModuleUI/Appointment/Booking/APPBok_VendorBookingOverview.aspx?ModuleName=" + WebCommon.getGlobalResourceValue("PreviousBookings")));
        string HolidayClosurePath = Common.EncryptQuery(ResolveClientUrl(HostURL + "ModuleUI/Appointment/SiteSettings/APPSIT_HolidaySetupOverview.aspx?ModuleName=" + WebCommon.getGlobalResourceValue("DaysClosedForOD")));
        string BookingPath = Common.EncryptQuery(ResolveClientUrl(HostURL + "ModuleUI/Appointment/Booking/APPBok_BookingOverview.aspx?ModuleName=" + WebCommon.getGlobalResourceValue("YourScheduleAddBookings")));


        ArrayList CarrierMenuCollection = new ArrayList();
        CarrierMenuCollection.Add("DefaultDashboard.aspx");
        CarrierMenuCollection.Add("ChangePassword.aspx");
        CarrierMenuCollection.Add("SupplierGuideline.aspx");
        CarrierMenuCollection.Add("APPSIT_HolidaySetupOverview.aspx");
        CarrierMenuCollection.Add("APPBok_VendorBookingOverview.aspx");
        CarrierMenuCollection.Add("APPRcv_BookingHistory.aspx");
        CarrierMenuCollection.Add("APPBok_BookingOverview.aspx");
        CarrierMenuCollection.Add("APPBok_WindowBookingEdit.aspx");
        CarrierMenuCollection.Add("APPBok_CarrierBookingWindowEdit.aspx");
        CarrierMenuCollection.Add("APPBok_Confirm.aspx");
        CarrierMenuCollection.Add("APPBok_CarrierBookingEdit.aspx");
        CarrierMenuCollection.Add("APPBok_BookingEdit.aspx");
        CarrierMenuCollection.Add("SCT_TermsAndConditions");




        StringBuilder sb = new StringBuilder();
        sb.AppendLine("<li style=\"z-index:9999\"><a href='#'>" + WebCommon.getGlobalResourceValue("AppScheduling") + "</a><ul>");

        // check if sites are assigned to Carrier
        SCT_UserBE oNewSCT_UserBE = new SCT_UserBE();
        oNewSCT_UserBE.Action = "GetCarrierSite";
        oNewSCT_UserBE.UserID = Convert.ToInt32(Session["UserID"]);
        oNewSCT_UserBE = oSCT_UserBAL.GetUserDefaultsBAL(oNewSCT_UserBE);
        // Create Booking Page only when site are assigned to that carrier
        if (oNewSCT_UserBE.Site != null && oNewSCT_UserBE.Site.SiteID != 0)
        {
            sb.AppendLine("<li style=\"z-index:9999\"><a href='" + BookingPath + "'>" + WebCommon.getGlobalResourceValue("YourScheduleAddBookings") + "</a></li>");
            sb.AppendLine("<li style=\"z-index:9999\"><a href='" + BookingHistoryPath + "'>" + WebCommon.getGlobalResourceValue("PreviousBookings") + "</a></li>");
            sb.AppendLine("<li style=\"z-index:9999\"><a href='" + HolidayClosurePath + "'>" + WebCommon.getGlobalResourceValue("DaysClosedForOD") + "</a></li>");

        }
        else
        {
            string SiteNotExistMessage = WebCommon.getGlobalResourceValue("SiteNotExistMessage");
            sb.AppendLine("<li style=\"z-index:9999\"><a href='#' onclick=\"alert('" + SiteNotExistMessage + "');\">" + WebCommon.getGlobalResourceValue("BookingOD") + "</a></li>");
        }
        sb.AppendLine("</ul></li>");

        string requestURL = Request.Url.AbsolutePath;
        bool carrierAuthorized = false;

        foreach (string CarrierMenu in CarrierMenuCollection)
        {
            if (requestURL.ToLower().Contains(CarrierMenu.ToLower()))
            {
                carrierAuthorized = true;
                break;
            }
        }

        if (!carrierAuthorized)
        {
            Session.Abandon();
            Response.Redirect("~/ModuleUI/Security/Login.aspx");
        }

        return sb.ToString();
    }

    private void GetModule(string ParentModuleID, string sModuletext)
    {
        SCT_ModuleBAL oSCT_ModuleBAL = new SCT_ModuleBAL();
        SCT_ModuleBE oSCT_ModuleBE = new SCT_ModuleBE();
        String FirstModule = "";




        oSCT_ModuleBE.Action = "GetModule";
        oSCT_ModuleBE.ParentModuleID = Convert.ToInt32(ParentModuleID);

        List<SCT_ModuleBE> lstModules = new List<SCT_ModuleBE>();

        lstModules = oSCT_ModuleBAL.GetModuleBAL(oSCT_ModuleBE);

        //-----Stage 7 Point 8---
        if (Convert.ToInt32(ParentModuleID) == 1)
        {
            lstModules = lstModules.OrderByDescending(x => x.ModuleID).ToList();
        }
        //----------------------

        oSCT_ModuleBAL = null;
        FirstModule = sModuletext;

        for (int iCount = 0; iCount <= lstModules.Count - 1; iCount++)
        {
            sbMemu.AppendLine(GetStartModuleString(lstModules[iCount].ModuleName));

            if (FirstModule.Length == 0)
                sModuletext = lstModules[iCount].ModuleName;
            else
                sModuletext = FirstModule + " - " + lstModules[iCount].ModuleName;


            GetModule(lstModules[iCount].ModuleID.ToString(), sModuletext);
            GetScreen(lstModules[iCount].ModuleID.ToString(), sModuletext);

            sbMemu.AppendLine(EndModuleString);
        }
        FirstModule = "";

    }

    private void GetScreen(string ModuleID, string sModuletext)
    {

        SCT_UserScreenBE oSCT_UserScreenBE = new SCT_UserScreenBE();
        SCT_UserScreenBAL oSCT_UserScreenBAL = new SCT_UserScreenBAL();

        oSCT_UserScreenBE.Screen = new SCT_ScreenBE();
        oSCT_UserScreenBE.Action = "GetUserScreen";
        oSCT_UserScreenBE.Screen.ModuleID = Convert.ToInt32(ModuleID);
        oSCT_UserScreenBE.UserID = Convert.ToInt32(Session["UserID"]);

        List<SCT_UserScreenBE> lstUserScreen = new List<SCT_UserScreenBE>();

        lstUserScreen = oSCT_UserScreenBAL.GetUserScreenBAL(oSCT_UserScreenBE);

        string ProvisionalScreen = string.Empty;
        if (ModuleID == "1")
        {
            List<SCT_UserScreenBE> lstProvisionalScreen = new List<SCT_UserScreenBE>();
            if (lstUserScreen != null && lstUserScreen.Count > 0)
            {
                lstProvisionalScreen = lstUserScreen.FindAll(delegate(SCT_UserScreenBE p) { return p.Screen.ScreenName == "ProvisionalBookings"; });
                if (lstProvisionalScreen != null && lstProvisionalScreen.Count == 1)
                {
                    ProvisionalScreen = GetScreenString(lstProvisionalScreen[0].ScreenID.ToString(), lstProvisionalScreen[0].Screen.ScreenName.ToString(), lstProvisionalScreen[0].Screen.ScreenUrl.ToString(), sModuletext);
                    lstUserScreen = lstUserScreen.FindAll(delegate(SCT_UserScreenBE p) { return p.Screen.ScreenName != "ProvisionalBookings"; });
                }
            }
        }

        oSCT_UserScreenBAL = null;
        bool isProvisionalAdded = false;
        for (int iCount = 0; iCount <= lstUserScreen.Count - 1; iCount++)
        {
            if (ModuleID == "1" && lstUserScreen[iCount].Screen.ScreenName.ToString() == "ManageBooking")
            {
                if (ProvisionalScreen != string.Empty)
                {
                    sbMemu.AppendLine(ProvisionalScreen);
                    isProvisionalAdded = true;
                }
            }
            sbMemu.AppendLine(GetScreenString(lstUserScreen[iCount].ScreenID.ToString(), lstUserScreen[iCount].Screen.ScreenName.ToString(), lstUserScreen[iCount].Screen.ScreenUrl.ToString(), sModuletext));

        }

        if (!isProvisionalAdded)
        {
            if (ProvisionalScreen != string.Empty)
            {
                sbMemu.AppendLine(ProvisionalScreen);
            }
        }
    }

    private void GetGeneratedReportCount()
    {
        ReportRequestBE oReportRequestBE = new ReportRequestBE();
        ReportRequestBAL oReportRequestBAL = new ReportRequestBAL();
        oReportRequestBE.Action = "GetGeneratedReportCount";
        oReportRequestBE.UserID = Convert.ToInt32(Session["UserId"]);

        List<ReportRequestBE> ReportRequestBEList = new List<ReportRequestBE>();
        ReportRequestBEList = oReportRequestBAL.GetGeneratedReportCountBAL(oReportRequestBE);
        if (ReportRequestBEList.Count > 0)
        {
            if (!string.IsNullOrEmpty(ReportRequestBEList[0].GeneratedReportCount))
            {
                if (ReportRequestBEList[0].GeneratedReportCount.Trim() == "0")
                    lblreportCount.Visible = false;

                lblreportCount.Text = ReportRequestBEList[0].GeneratedReportCount;
            }
            else
            {
                lblreportCount.Visible = false;
            }
        }
        else
        {
            lblreportCount.Visible = false;
        }
    }

    private bool IsVendorSubscribeForPenalty()
    {
        bool IsVendorSubscribe = false;
        try
        {
            BackOrderBAL oBackOrderBAL = new BackOrderBAL();
            BackOrderBE oBackOrderBE = new BackOrderBE();
            oBackOrderBE.User = new SCT_UserBE();
            oBackOrderBE.Action = "CheckVendorSubscribeforPenalty";
            oBackOrderBE.User.UserID = Convert.ToInt32(Session["UserID"]);
            IsVendorSubscribe = oBackOrderBAL.CheckVendorSubscribeBAL(oBackOrderBE);
        }
        catch (Exception ex)
        {
            LogUtility.SaveErrorLogEntry(ex);
        }
        return IsVendorSubscribe;
    }

    #endregion

    #region Events

    protected void btnLogout_Click(object sender, EventArgs e)
    {
        Session.Abandon();
        Cache.Remove("LanguageTagslist");
        Response.Redirect("~/ModuleUI/Security/Login.aspx");
    }

    protected void btnHome_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/ModuleUI/Dashboard/DefaultDashboard.aspx");
    }

    protected void lnkChangePassword_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/ModuleUI/Security/ChangePassword.aspx");
    }

    #endregion
}

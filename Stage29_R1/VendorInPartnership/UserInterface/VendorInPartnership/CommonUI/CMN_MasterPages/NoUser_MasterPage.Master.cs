﻿using System;
using System.Collections.Generic;

using System.Web.UI;

using BusinessEntities.ModuleBE.Security;
using BusinessLogicLayer.ModuleBAL.Security;
using BaseControlLibrary;
using Utilities; using WebUtilities;

public partial class NoUser_MasterPage : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //Add by Abhinav to fix the language cache lost problem
        if (System.Web.HttpRuntime.Cache["LanguageTagsEnglish"] == null
           || System.Web.HttpRuntime.Cache["LanguageTagsFrench"] == null
           || System.Web.HttpRuntime.Cache["LanguageTagsGerman"] == null
           || System.Web.HttpRuntime.Cache["LanguageTagsDutch"] == null
           || System.Web.HttpRuntime.Cache["LanguageTagsSpanish"] == null
           || System.Web.HttpRuntime.Cache["LanguageTagsItalian"] == null
           || System.Web.HttpRuntime.Cache["LanguageTagsCzech"] == null) {
            Utility.LoadLanguageTags();
        }
        if (!Page.IsPostBack) {
            if (Request.CurrentExecutionFilePath.Contains("SCT_RegisterExternalUser.aspx"))
                divLoginDetails.Visible = true;
            else if (Request.CurrentExecutionFilePath.Contains("MgmtReports"))
                divGuest.Visible = true;
        }
    }

    protected override void OnPreRender(EventArgs e) {
        if (!Page.IsPostBack) {
            CommonPage cm = new CommonPage();
            cm.GlobalResourceFields(this.Page);
        }
    }
   

    #region Method

    System.Text.StringBuilder sbMemu = new System.Text.StringBuilder();

    string EndModuleString = "</ul></li>";
    
    private string GetStartModuleString(string ModuleName) {
        return "<li><a href='#'>" + ModuleName + "</a><ul>";
    }
    
    private string GetScreenString(string ScreenID,string ScreenName,string ScreenUrl) {        
            string Path = Common.EncryptQuery(ResolveClientUrl(ScreenUrl));
            return "<li><a href='" + Path + "'>" + ScreenName + "</a></li>";       
    }

    private bool IsAllowedScreenToCurrenUser(string ScreenID) {
        return true;
    }
    private void GenerateMenu() {
        GetModule("0");
        //ltMenu.Text = sbMemu.ToString();
    }

    private void GetModule(string ParentModuleID) {      
        SCT_ModuleBAL oSCT_ModuleBAL = new SCT_ModuleBAL();
        SCT_ModuleBE oSCT_ModuleBE = new SCT_ModuleBE();

        oSCT_ModuleBE.Action = "GetModule";
        oSCT_ModuleBE.ParentModuleID = Convert.ToInt32(ParentModuleID);

        List<SCT_ModuleBE> lstModules = new List<SCT_ModuleBE>();

        lstModules = oSCT_ModuleBAL.GetModuleBAL(oSCT_ModuleBE);
        oSCT_ModuleBAL = null;
        for (int iCount = 0; iCount <= lstModules.Count - 1; iCount++) {            
            sbMemu.AppendLine(GetStartModuleString(lstModules[iCount].ModuleName));
            GetModule(lstModules[iCount].ModuleID.ToString());
            GetScreen(lstModules[iCount].ModuleID.ToString());
            sbMemu.AppendLine(EndModuleString);
        }        
    }

    private void GetScreen(string ModuleID) {      
               
        SCT_UserScreenBE oSCT_UserScreenBE = new SCT_UserScreenBE();
        SCT_UserScreenBAL oSCT_UserScreenBAL = new SCT_UserScreenBAL();

        oSCT_UserScreenBE.Screen = new SCT_ScreenBE();
        oSCT_UserScreenBE.Action = "GetUserScreen";
        oSCT_UserScreenBE.Screen.ModuleID = Convert.ToInt32(ModuleID);
        oSCT_UserScreenBE.UserID = Convert.ToInt32(Session["UserID"]);

        List<SCT_UserScreenBE> lstUserScreen = new List<SCT_UserScreenBE>();

        lstUserScreen = oSCT_UserScreenBAL.GetUserScreenBAL(oSCT_UserScreenBE);
        oSCT_UserScreenBAL = null;
        for (int iCount = 0; iCount <= lstUserScreen.Count - 1; iCount++) {
            sbMemu.AppendLine(GetScreenString(lstUserScreen[iCount].ScreenID.ToString(), lstUserScreen[iCount].Screen.ScreenName.ToString(), lstUserScreen[iCount].Screen.ScreenUrl.ToString()));    
        }   
    }
    
    #endregion

    #region Events

    protected void btnLogout_Click(object sender, EventArgs e)
    {
        Session.Abandon();
        Response.Redirect("~/ModuleUI/Security/Login.aspx");
    }

    #endregion
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.StockOverview;
using BusinessLogicLayer.ModuleBAL.StockOverview;

public partial class MultiSelectDeclineReasonCode : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e) {
        if (!IsPostBack) {
            BindReasonCode();
        }
    }

    private void BindReasonCode() {
        MAS_DeclineReasonCodeBE oMAS_DeclineReasonCodeBE = new MAS_DeclineReasonCodeBE();
        MAS_DeclineReasonCodeBAL oMAS_DeclineReasonCodeBAL = new MAS_DeclineReasonCodeBAL();

        oMAS_DeclineReasonCodeBE.Action = sqlAction.ShowAll;

        List<MAS_DeclineReasonCodeBE> lstMAS_DeclineReasonCodeBE = oMAS_DeclineReasonCodeBAL.GetDeclineReasonCodeBAL(oMAS_DeclineReasonCodeBE);

        if (lstMAS_DeclineReasonCodeBE != null && lstMAS_DeclineReasonCodeBE.Count > 0) {
            FillControls.FillListBox(ref lstLeft, lstMAS_DeclineReasonCodeBE, "ReasonCode", "DeclineReasonCodeID");
        }
    }

    private string _selectedDeclineReasonCodeIDs = string.Empty;
    
    public void SetCodeOnPostBack() {
        lstRight.Items.Clear();
        string[] strSelectedName = hiddenSelectedName.Value.TrimEnd(',').TrimEnd(' ').Split(',');
        string[] strSelectedIDs = hiddenSelectedIDs.Value.TrimEnd(',').Split(',');

        if (strSelectedIDs.Length > 0)
            this.BindReasonCode();

        for (int iCount = 0; iCount < strSelectedIDs.Length; iCount++) {
            if (!string.IsNullOrEmpty(strSelectedIDs[iCount]) && !string.IsNullOrWhiteSpace(strSelectedIDs[iCount])) {
                lstRight.Items.Add(new ListItem(strSelectedName[iCount].ToString().Trim(), strSelectedIDs[iCount].ToString().Trim()));
                RemoveCode(strSelectedIDs[iCount].ToString().Trim());
            }
        }
    }
    public void RemoveCode(string APid) {
        if (lstLeft.Items.FindByValue(APid.Trim()) != null)
            lstLeft.Items.RemoveAt(lstLeft.Items.IndexOf(lstLeft.Items.FindByValue(APid.Trim())));
    }
    public string SelectedDeclineReasonCodeIDs {
        get {
            if (!string.IsNullOrEmpty(hiddenSelectedIDs.Value)) {
                return hiddenSelectedIDs.Value.Trim(',');
            }
            return null;
        }
    }

    public string SelectedReasonCode {
        get {
            string _selectedReasonCode = string.Empty;

            if (!string.IsNullOrEmpty(hiddenSelectedName.Value)) {
                _selectedReasonCode = hiddenSelectedName.Value;
            }

            if (_selectedReasonCode.Length > 0)
                _selectedReasonCode = _selectedReasonCode.Substring(0, _selectedReasonCode.Length - 2);

            return _selectedReasonCode;
        }
    }

    protected virtual void btnMoveRightAll_Click(object sender, EventArgs e) {
        FillControls.MoveAllItems(lstLeft, lstRight);
    }

    protected virtual void btnMoveRight_Click(object sender, EventArgs e) {
        if (lstLeft.SelectedItem != null) {
            FillControls.MoveOneItem(lstLeft, lstRight);
        }
    }

    protected virtual void btnMoveLeft_Click(object sender, EventArgs e) {
        if (lstRight.SelectedItem != null) {
            FillControls.MoveOneItem(lstRight, lstLeft);
        }
    }

    protected virtual void btnMoveLeftAll_Click(object sender, EventArgs e) {
        FillControls.MoveAllItems(lstRight, lstLeft);
    }
}
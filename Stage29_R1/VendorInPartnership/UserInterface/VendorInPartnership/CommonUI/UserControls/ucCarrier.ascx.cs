﻿using System;
using System.Collections.Generic;
using BusinessEntities.ModuleBE.Security;
using BusinessLogicLayer.ModuleBAL.Security;
using Utilities;
using WebUtilities;

public partial class ucCarrier : System.Web.UI.UserControl
{
    public event EventHandler btnSearchClick;

    private int searchCountryID;
    public int SearchCountryID
    {
        get { return searchCountryID; }
        set { searchCountryID = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        btnSearchCarrier.Text = WebCommon.getGlobalResourceValue("SearchCarrier");
    }

    protected void btnSearchCarrier_Click(object sender, EventArgs e)
    {
        btnSearchClick(sender, e);
        BindCarrier();
    }

    private void BindCarrier()
    {
        SCT_UserBAL oUP_CarrierBAL = new SCT_UserBAL();
        SCT_UserBE oUP_CarrierBE = new SCT_UserBE();
        oUP_CarrierBE.Action = "SearchCarriers";
        oUP_CarrierBE.CarrierName = Common.HtmlEncode(txtCarrier.Text);
        oUP_CarrierBE.UserID = Convert.ToInt32(Session["UserID"]);
        List<SCT_UserBE> lstUPCarrier = new List<SCT_UserBE>();
        lstUPCarrier = oUP_CarrierBAL.GetCarrierDetailsBAL(oUP_CarrierBE);
        if (lstUPCarrier.Count > 0)
            FillControls.FillListBox(ref lstLeft, lstUPCarrier, "CarrierName", "CarrierID");
        else
            lstLeft.Items.Clear();        
    }

    public void RemoveCarrier(string CarrierName, string CarrierId)
    {
        if (lstLeft.Items.FindByValue(CarrierId.Trim()) != null)
            lstLeft.Items.RemoveAt(lstLeft.Items.IndexOf(lstLeft.Items.FindByValue(CarrierId.Trim())));
    }

    public void SearchCarrierClick(string carrierNo)
    {
        txtCarrier.Text = carrierNo;
        BindCarrier();
    }
}
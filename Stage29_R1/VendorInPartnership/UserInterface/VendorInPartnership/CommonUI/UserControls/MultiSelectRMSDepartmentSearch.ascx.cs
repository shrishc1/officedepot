﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.Upload;
using BusinessLogicLayer.ModuleBAL.Upload;

public partial class MultiSelectRMSDepartmentSearch : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //if (IsPostBack)
        //{
        //    SetRMSDepartmentOnPostBack();
        //}        
    }

    private void BindRMSDepartment()
    {
        UP_RMSDepartmentBE oUP_RMSDepartmentBE = new UP_RMSDepartmentBE();
        UP_RMSDepartmentBAL oUP_RMSDepartmentBAL = new UP_RMSDepartmentBAL();

        oUP_RMSDepartmentBE.Action = "GetRmsDepartment";
        if (!string.IsNullOrEmpty(txtRmsdepartment.Text))
        { oUP_RMSDepartmentBE.Dept_Name = txtRmsdepartment.Text; }

        List<UP_RMSDepartmentBE> lstRMSDepartment = new List<UP_RMSDepartmentBE>();
        lstRMSDepartment = oUP_RMSDepartmentBAL.GetRMSDepartmentBAL(oUP_RMSDepartmentBE);

        if (lstRMSDepartment.Count > 0)
            FillControls.FillListBox(ref lstLeft, lstRMSDepartment, "Dept_Name", "Dept_No");

        string[] strSelectedIDs = hiddenSelectedIDs.Value.TrimEnd(',').Split(',');
        for (int iCount = 0; iCount < strSelectedIDs.Length; iCount++)
        {
            if (!string.IsNullOrEmpty(strSelectedIDs[iCount]) && !string.IsNullOrWhiteSpace(strSelectedIDs[iCount]))
            {
                RemoveRMSDepartment(strSelectedIDs[iCount]);
            }
        }
    }

    private string _selectedRMSDepartmentIDs = string.Empty;

    public string SelectedRMSDepartmentIDs
    {
        get
        {
            if (!string.IsNullOrEmpty(hiddenSelectedIDs.Value))
            {
                return hiddenSelectedIDs.Value.Trim(',');
            }
            return null;
        }
        set        
        {
            hiddenSelectedIDs.Value = value ;
        }
        
    }

    public string SelectedRMSDepartmentName
    {
        get
        {
            string _selectedRMSDepartmentName = string.Empty;

            if (!string.IsNullOrEmpty(hiddenSelectedName.Value))
            {
                _selectedRMSDepartmentName = hiddenSelectedName.Value;
            }
            if (_selectedRMSDepartmentName.Length > 0)
                _selectedRMSDepartmentName = _selectedRMSDepartmentName.Substring(0, _selectedRMSDepartmentName.Length - 2);

            return _selectedRMSDepartmentName;
        }
        set
        {
            hiddenSelectedName.Value = value ;
        }
    }

    protected virtual void btnMoveRightAll_Click(object sender, EventArgs e)
    {
        FillControls.MoveAllItems(lstLeft, lstRight);
    }

    protected virtual void btnMoveRight_Click(object sender, EventArgs e)
    {
        if (lstLeft.SelectedItem != null)
        {
            FillControls.MoveOneItem(lstLeft, lstRight);
        }
    }

    protected virtual void btnMoveLeft_Click(object sender, EventArgs e)
    {
        if (lstRight.SelectedItem != null)
        {
            FillControls.MoveOneItem(lstRight, lstLeft);
        }
    }

    protected virtual void btnMoveLeftAll_Click(object sender, EventArgs e)
    {
        FillControls.MoveAllItems(lstRight, lstLeft);
    }

    public void SetRMSDepartmentOnPostBack()
    {
        lstRight.Items.Clear();    
        string[] strSelectedName = hiddenSelectedName.Value.TrimEnd(',').TrimEnd(' ').Split(',');
        string[] strSelectedIDs = hiddenSelectedIDs.Value.TrimEnd(',').Split(',');

        if (strSelectedIDs.Length > 0)
            this.BindRMSDepartment();

        for (int iCount = 0; iCount < strSelectedIDs.Length; iCount++)
        {
            if (!string.IsNullOrEmpty(strSelectedIDs[iCount]) && !string.IsNullOrWhiteSpace(strSelectedIDs[iCount]))
            {
                lstRight.Items.Add(new ListItem(strSelectedName[iCount].ToString().Trim(), strSelectedIDs[iCount].ToString().Trim()));
                RemoveRMSDepartment(strSelectedIDs[iCount].ToString().Trim());
            }
        }
    }

    public void RemoveRMSDepartment(string RMSDepartmentId)
    {
        if (lstLeft.Items.FindByValue(RMSDepartmentId.Trim()) != null)
            lstLeft.Items.RemoveAt(lstLeft.Items.IndexOf(lstLeft.Items.FindByValue(RMSDepartmentId.Trim())));
    }
    protected void btnRmsDepartment_Click(object sender, EventArgs e)
    {
        lstLeft.Items.Clear();
        this.BindRMSDepartment();       
        ReBindRightList();
    }
    private void ReBindRightList()
    {
        lstRight.Items.Clear();
        if (!string.IsNullOrWhiteSpace(hdnListRightCategory.Value))
        {
            string[] RMSDepartment = hdnListRightCategory.Value.Split('$');
            for (int index = 0; index < RMSDepartment.Length; index++)
            {
                string[] RMSDepartments = RMSDepartment[index].Split('#');
                ListItem listItem = new ListItem(RMSDepartments[0], RMSDepartments[1]);
                lstRight.Items.Add(listItem);
            }
            hdnListRightCategory.Value = String.Empty;
        }
    }




}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.Security;
using BusinessLogicLayer.ModuleBAL.Security;
using System.Data;

public partial class MultiSelectAPSearch : System.Web.UI.UserControl
{

    public string SelectedStockPlannerIDs
    {
        get
        {
            if (!string.IsNullOrEmpty(hiddenSelectedIDs.Value))
            {
                return hiddenSelectedIDs.Value.Trim(',');
            }
            return null;
        }
    }
    public string SelectedSPName
    {
        get
        {
            string _selectedSPName = string.Empty;

            if (!string.IsNullOrEmpty(hiddenSelectedName.Value))
            {
                _selectedSPName = hiddenSelectedName.Value;
            }

            if (_selectedSPName.Length > 0)
                _selectedSPName = _selectedSPName.Substring(0, _selectedSPName.Length - 2);

            return _selectedSPName;
        }
    }

    #region Events
    protected void Page_Load(object sender, EventArgs e)
    {
       
    }
    public void BindApDetails()
    {
        ucLBLeft.Items.Clear();
        BindStockPlanner();
        ReBindRightList();
    }
    protected void btnSearchUser_Click(object sender, EventArgs e)
    {
        BindApDetails();
    }
    #endregion

    #region Methods

    private void BindStockPlanner()
    {
      
        SCT_UserBE oSCT_UserBE = new SCT_UserBE();
        SCT_UserBAL oSCT_UserBAL = new SCT_UserBAL();
        oSCT_UserBE.Action = "ShowAll";
        oSCT_UserBE.RoleName = "OD - Accounts Payable";
        if (!string.IsNullOrWhiteSpace(txtUserName.Text.Trim()))
            oSCT_UserBE.UserSearchText = txtUserName.Text.Trim();

        List<SCT_UserBE> lstUsers = oSCT_UserBAL.GetUserDetailsBAL(oSCT_UserBE);
        if (lstUsers != null && lstUsers.Count > 0)
        {
            FillControls.FillListBox(ref ucLBLeft, lstUsers, "APUserName", "UserID");
            
        }
    }  

    private void ReBindRightList()
    {
        ucLBRight.Items.Clear();
        if (!string.IsNullOrWhiteSpace(hdnListRightUser.Value))
        {
            string[] Emails = hdnListRightUser.Value.Split('$');
            for (int index = 0; index < Emails.Length; index++)
            {
                string[] EmailIds = Emails[index].Split('#');
                ListItem listItem = new ListItem(EmailIds[0], EmailIds[1]);
                ucLBRight.Items.Add(listItem);
            }
            hdnListRightUser.Value = String.Empty;
        }
    }

    #endregion

    public void SetAPOnPostBack()
    {
        ucLBRight.Items.Clear();
        string[] strSelectedName = hiddenSelectedName.Value.TrimEnd(',').Split(',');
        string[] strSelectedIDs = hiddenSelectedIDs.Value.TrimEnd(',').Split(',');

        if (strSelectedIDs.Length > 0)
        {
            BindStockPlanner();
            ReBindRightList();
        }

        for (int iCount = 0; iCount < strSelectedIDs.Length; iCount++)
        {
            if (!string.IsNullOrEmpty(strSelectedIDs[iCount]) && !string.IsNullOrWhiteSpace(strSelectedIDs[iCount]))
            {
                ucLBRight.Items.Add(new ListItem(strSelectedName[iCount].ToString().Trim(), strSelectedIDs[iCount].ToString().Trim()));
                RemoveVendorRole(strSelectedIDs[iCount].ToString().Trim());
            }
        }
    }

    private void RemoveVendorRole(string RoleId)
    {
        if (ucLBLeft.Items.FindByValue(RoleId.Trim()) != null)
            ucLBLeft.Items.RemoveAt(ucLBLeft.Items.IndexOf(ucLBLeft.Items.FindByValue(RoleId.Trim())));
    }


}
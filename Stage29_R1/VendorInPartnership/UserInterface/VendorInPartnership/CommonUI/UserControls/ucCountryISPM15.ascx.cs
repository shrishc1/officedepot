﻿using System;
using System.Web.UI;
using System.Collections.Generic;

using BusinessEntities.ModuleBE.AdminFunctions;
using BusinessLogicLayer.ModuleBAL.AdminFunctions;

using System.Linq;
using BaseControlLibrary;

public partial class ucCountryISPM15 : UserControl
{

    #region Public Property

    bool isAllRequired = false;
    public bool IsAllRequired
    {
        get
        {
            return isAllRequired;
        }
        set
        {
            isAllRequired = value;
        }
    }

    bool isAllDefault = false;
    public bool IsAllDefault
    {
        get
        {
            return isAllDefault;
        }
        set
        {
            isAllDefault = value;
        }
    }

    public CommonPage CurrentPage
    {
        get;
        set;
    }

    public ucDropdownList innerControlddlCountry
    {
        get
        {
            return this.ddlCountry;
        }
    }

    string _CountryIDs;
    public string CountryIDs
    {
        get { return (string)ViewState["_CountryIDs"]; }
    }
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (CurrentPage.CountryPrePage_Load())
            {
                BindAllCountry();
                CurrentPage.CountryPost_Load();
            }
            CurrentPage.CountryCustomAction();
        }
    }

    #region Methods

    public void BindAllCountry(int id = 0)
    {
        MAS_CountryBE oMAS_CountryBE = new MAS_CountryBE();
        MAS_CountryBAL oMAS_CountryBAL = new MAS_CountryBAL();

        oMAS_CountryBE.Action = "GetISPM15Country";
        oMAS_CountryBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
        oMAS_CountryBE.User.UserID = 0;

        List<MAS_CountryBE> lstCountry = new List<MAS_CountryBE>();

        lstCountry = oMAS_CountryBAL.GetCountryISPM15BAL(oMAS_CountryBE);

        if (lstCountry.Count > 0)
        {
            FillControls.FillDropDown(ref ddlCountry, lstCountry.ToList(), "CountryName", "CountryID","--Select--");
        }

        if (id > 0)
        {
            ddlCountry.SelectedValue = id.ToString();
        }
    }

    #endregion

    protected void ddlCountry_SelectedIndexChanged(object sender, EventArgs e)
    {
        CurrentPage.CountrySelectedIndexChanged();
    }
}

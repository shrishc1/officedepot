﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MultiSelectSubvdr.ascx.cs" Inherits="CommonUI_UserControls_MultiSelectSubvdr" %>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>

<script lang="javascript" type="text/javascript">
    $(document).ready(function () {
        var lstitem = document.getElementById('<%= LstRight.ClientID %>');
        if (lstitem.options.length == 0) {
            document.getElementById('<%= hiddenSelectedIDs.ClientID %>').value = "";
            document.getElementById('<%= hiddenSelectedName.ClientID %>').value = "";
        }
    });
</script>

<asp:HiddenField ID="hiddenSelectedIDs" runat="server" />
<asp:HiddenField ID="hiddenSelectedName" runat="server" />
<asp:HiddenField ID="hiddenSubVendorIDs" runat="server" />
<asp:HiddenField ID="hdnIsMoveAllRequired" runat="server" />

<table>
    <tr>
        <td>
            <cc1:ucListBox ID="LstLeft" runat="server" Height="100px" Width="320px">
            </cc1:ucListBox>
        </td>
        <td style="text-align: center" class="search-controls-btn">
            <div id="divMoveAllRight" runat="server" visible="false">
                <input type="button" id="BtnMoveRightAll" value=">>" class="button" style="width: 35px"
                    onclick="Javascript: MoveAll('<%= LstLeft.ClientID %>', '<%= LstRight.ClientID %>','<%= hiddenSelectedIDs.ClientID %>',' <%= hiddenSelectedName.ClientID %>');" />
            </div>
            &nbsp;
            <div>
                <input type="button" id="BtnMoveRight" value=">" class="button" style="width: 35px"
                    onclick="Javascript: MoveItem('<%= LstLeft.ClientID %>', '<%= LstRight.ClientID %>','<%= hiddenSelectedIDs.ClientID %>','<%= hiddenSelectedName.ClientID %>');" />
            </div>
            &nbsp;
            <div>
                <input type="button" id="BtnMoveLeft" value="<" class="button" style="width: 35px"
                    onclick="Javascript: MoveItem('<%= LstRight.ClientID %>', '<%= LstLeft.ClientID %>','<%= hiddenSelectedIDs.ClientID %>','<%= hiddenSelectedName.ClientID %>');" />
            </div>
            &nbsp;
            <div id="divMoveAllLeft" runat="server" visible="false">
                <input type="button" id="BtnMoveLeftAll" value="<<" class="button" style="width: 35px"
                    onclick="Javascript: MoveAll('<%= LstRight.ClientID %>', '<%= LstLeft.ClientID %>','<%= hiddenSelectedIDs.ClientID %>','<%= hiddenSelectedName.ClientID %>');" />
            </div>
        </td>
        <td>
            <cc1:ucListBox ID="LstRight" runat="server" Height="100px" Width="320px">
            </cc1:ucListBox>
        </td>
    </tr>
</table>
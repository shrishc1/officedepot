﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucVendorSelectionTemplate.ascx.cs"
    Inherits="ucVendorSelectionTemplate" %>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<table>
    <tr>
        <td style="font-weight: bold;">
            <cc1:ucLabel ID="lblVendorSelectionTemplate" runat="server"></cc1:ucLabel>
        </td>
        <td>
            :
        </td>
        <td>
            <cc1:ucDropdownList ID="drpTemplateName" runat="server" Width="150px" AutoPostBack="True"
                OnSelectedIndexChanged="drpTemplateName_SelectedIndexChanged">
            </cc1:ucDropdownList>
        </td>
    </tr>
</table>
<asp:HiddenField ID="hiddenSelectedTemplateIDs" runat="server" />

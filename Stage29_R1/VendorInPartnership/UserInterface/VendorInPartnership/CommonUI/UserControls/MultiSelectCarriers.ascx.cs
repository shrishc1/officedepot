﻿using System;
using System.Web.UI.WebControls;

public partial class MultiSelectCarriers : System.Web.UI.UserControl
{
    private int searchCountryID;
    public int SearchCountryID
    {
        get { return searchCountryID; }
        set
        {
            searchCountryID = value;
            ucCarrier.SearchCountryID = value;
        }
    }

    private string _selectedCarrierIDs = string.Empty;
    public string SelectedCarrierIDs
    {
        get
        {
            if (!string.IsNullOrEmpty(hiddenSelectedIDs.Value))
            {
                return hiddenSelectedIDs.Value.Trim(',');
            }
            return null;
        }
        set
        {
            hiddenSelectedIDs.Value += value + ",";
        }
    }

    public string SelectedCarrierName
    {
        get
        {
            string _selectedSPName = string.Empty;

            if (!string.IsNullOrEmpty(hiddenSelectedName.Value))
            {
                return hiddenSelectedName.Value.Trim(',');
            }
            return null;
        }
        set
        {
            hiddenSelectedName.Value += value + ",";
        }

    }

    protected void Page_Load(object sender, EventArgs e)
    {
        ucCarrier.btnSearchClick += new EventHandler(ucCarrier_btnSearchClick);
    }

    void ucCarrier_btnSearchClick(object sender, EventArgs e)
    {
        #region Commented Code ..
        //if (lstRight != null)
        //{
        //    if (lstRight.Items.Count == 0)
        //    {
        //        hiddenSelectedIDs.Value = string.Empty;
        //        hiddenSelectedName.Value = string.Empty;
        //    }
        //    else if (lstRight.Items.Count == 1)
        //    {
        //        if (string.IsNullOrEmpty(lstRight.Items[0].Text))
        //        {
        //            hiddenSelectedIDs.Value = string.Empty;
        //            hiddenSelectedName.Value = string.Empty;
        //        }
        //    }
        //}
        #endregion

        lstRight.Items.Clear();
        hiddenSelectedIDs.Value = string.Empty;
        hiddenSelectedName.Value = string.Empty;
    }

    public void setCarriersOnPostBack()
    {
        lstRight.Items.Clear();
        string[] strSelectedName = hiddenSelectedName.Value.Split(',');
        string[] strSelectedIDs = hiddenSelectedIDs.Value.Split(',');
        for (int iCount = 0; iCount < strSelectedIDs.Length; iCount++)
        {
            if (!string.IsNullOrEmpty(strSelectedIDs[iCount]) && !string.IsNullOrWhiteSpace(strSelectedIDs[iCount]))
            {
                lstRight.Items.Add(new ListItem(strSelectedName[iCount].ToString().Trim(), strSelectedIDs[iCount].ToString().Trim()));
                ucCarrier.RemoveCarrier(strSelectedName[iCount].ToString().Trim(), strSelectedIDs[iCount].ToString().Trim());
            }
        }
    }

    public void SearchCarrierClick(string carrierNo)
    {
        ucCarrier.SearchCarrierClick(carrierNo);
    }
}
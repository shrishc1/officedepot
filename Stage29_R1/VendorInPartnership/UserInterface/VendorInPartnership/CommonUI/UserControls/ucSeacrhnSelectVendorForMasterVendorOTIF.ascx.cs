﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using BusinessLogicLayer.ModuleBAL.Appointment.SiteSettings;

public partial class ucSeacrhnSelectVendorForMasterVendorOTIF : System.Web.UI.UserControl
{
    #region Public Property

    public string VendorNo
    {
        get
        {
            // return hdnVendorID.Value;
            return "1";
        }
    }

    public int SiteID
    {
        get { return Convert.ToInt32(ViewState["SiteID"]); }
        set
        {
            ucVendorForCountryForMasterVendorOTIF.SiteID = value;
            ViewState["SiteID"] = value;
        }
    }

    public int CountryID
    {
        get { return Convert.ToInt32(ViewState["CountryID"]); }
        set
        {
            ucVendorForCountryForMasterVendorOTIF.CountryID = value;
            ViewState["CountryID"] = value;
        }
    }

    public bool IsChildRequired
    {
        get { return Convert.ToBoolean(ViewState["IsChildRequired"]); }
        set
        {
            ucVendorForCountryForMasterVendorOTIF.IsChildRequired = value;
            ViewState["IsChildRequired"] = value;
        }
    }

    public bool IsParentRequired
    {
        get { return Convert.ToBoolean(ViewState["IsParentRequired"]); }
        set
        {
            ucVendorForCountryForMasterVendorOTIF.IsParentRequired = value;
            ViewState["IsParentRequired"] = value;
        }
    }

    public bool IsStandAloneRequired
    {
        get { return Convert.ToBoolean(ViewState["IsStandAloneRequired"]); }
        set
        {
            ucVendorForCountryForMasterVendorOTIF.IsStandAloneRequired = value;
            ViewState["IsStandAloneRequired"] = value;
        }
    }

    public bool IsGrandParentRequired
    {
        get { return Convert.ToBoolean(ViewState["IsGrandParentRequired"]); }
        set
        {
            ucVendorForCountryForMasterVendorOTIF.IsGrandParentRequired = value;
            ViewState["IsGrandParentRequired"] = value;
        }
    }

    /// <summary>
    /// 0= All 1= Europen 2=Local
    /// </summary>
    public int EuropeOrLocal {
        get { return Convert.ToInt32(ViewState["EuropeOrLocal"]); }
        set {
            ucVendorForCountryForMasterVendorOTIF.EuropeOrLocal = value;
            ViewState["EuropeOrLocal"] = value;
        }
    }

    public void BindVendor()
    {
        ucVendorForCountryForMasterVendorOTIF.BindVendor();
    }

    public void BindVendorByText() {
        ucVendorForCountryForMasterVendorOTIF.BindVendorByText();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        ucVendorForCountryForMasterVendorOTIF.IsBindVendorByDefault = true;
    }

    public HiddenField innerControlHiddenField
    {
        get
        {
            return this.hiddenSelectedIDs;
        }
    }

    public HiddenField innerControlHiddenField2
    {
        get
        {
            return this.hiddenSelectedName;
        }
    }

    private string _selectedVendorIDs = string.Empty;
    public string SelectedVendorIDs
    {
        get
        {
            if (!string.IsNullOrEmpty(hiddenSelectedIDs.Value))
            {
                return hiddenSelectedIDs.Value.Trim(',');
            }
            return null;
        }
        set
        {
            hiddenSelectedIDs.Value += value + ",";
        }
    }

    public string SelectedVendorName
    {
        get
        {
            string _selectedSPName = string.Empty;

            if (!string.IsNullOrEmpty(hiddenSelectedName.Value))
            {
                _selectedSPName = hiddenSelectedName.Value;
            }

            if (_selectedSPName.Length > 0)
                _selectedSPName = _selectedSPName.Substring(0, _selectedSPName.Length - 2);

            return _selectedSPName;
        }
        set
        {
            hiddenSelectedName.Value += value + ",";
        }
    }

    public void setVendorsOnPostBack() {
        lstSelectedVendor.Items.Clear();
        string[] strSelectedName = hiddenSelectedName.Value.Split(',');
        string[] strSelectedIDs = hiddenSelectedIDs.Value.Split(',');
        for (int iCount = 0; iCount < strSelectedName.Length - 1; iCount++) {
            lstSelectedVendor.Items.Add(new ListItem(strSelectedName[iCount].ToString().Trim(), strSelectedIDs[iCount].ToString().Trim()));
            RemoveVendor(strSelectedName[iCount].ToString().Trim(), strSelectedIDs[iCount].ToString().Trim());
        }
    }

    public void RemoveVendor(string VendorName, string VendorId) {
        
        ListBox lstLeft = (ListBox)ucVendorForCountryForMasterVendorOTIF.FindControl("lstLeft");
        if (lstLeft.Items.FindByValue(VendorId.Trim()) != null)
            lstLeft.Items.RemoveAt(lstLeft.Items.IndexOf(lstLeft.Items.FindByValue(VendorId.Trim())));
    }
    #endregion
}
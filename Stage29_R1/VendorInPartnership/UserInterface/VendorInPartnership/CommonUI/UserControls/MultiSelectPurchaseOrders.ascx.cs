﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Utilities; using WebUtilities;
using BusinessLogicLayer.ModuleBAL.Discrepancy.LogDiscrepancy;
using BusinessEntities.ModuleBE.Discrepancy.LogDiscrepancy;
using BusinessEntities.ModuleBE.Upload;
using BusinessLogicLayer.ModuleBAL.Upload;
using BaseControlLibrary;


public partial class MultiSelectPurchaseOrders : ucUserControlBase
{

    protected void Page_Load(object sender, EventArgs e)
    {
        
    }
    public string SelectedPO {
        get {
            string _selectedPO = null;

            if (!string.IsNullOrEmpty(hiddenSelectedId.Value)) {
                _selectedPO = hiddenSelectedId.Value;
            }
            if (_selectedPO != null)
                if (_selectedPO.Length > 0)
                    _selectedPO = _selectedPO.Substring(0, _selectedPO.Length - 1);

            return _selectedPO;
        }
        set {
            hiddenSelectedId.Value += value + ","; 
        }
    }

  
}

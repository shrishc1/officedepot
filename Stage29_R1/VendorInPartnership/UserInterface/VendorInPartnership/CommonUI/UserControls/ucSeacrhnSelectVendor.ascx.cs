﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using BusinessLogicLayer.ModuleBAL.Appointment.SiteSettings;

public partial class SeacrhnSelectVendor : System.Web.UI.UserControl
{
    #region Public Property

    //public CommonPage CurrentPage
    //{
    //    get;
    //    set;
    //}

    public string VendorNo
    {
        get
        {
            return hdnVendorID.Value;
        }
    }


    public int SiteID
    {
        get { return Convert.ToInt32(ViewState["SiteID"]); }
        set { ViewState["SiteID"] = value; }
    }



    public int CountryID
    {
        get { return Convert.ToInt32(ViewState["CountryID"]); }
        set { ViewState["CountryID"] = value; }
    }

    public bool IsChildRequired
    {
        get { return Convert.ToBoolean(ViewState["IsChildRequired"]); }
        set { ViewState["IsChildRequired"] = value; }
    }

    public bool IsParentRequired
    {
        get { return Convert.ToBoolean(ViewState["IsParentRequired"]); }
        set { ViewState["IsParentRequired"] = value; }
    }

    public bool IsStandAloneRequired
    {
        get { return Convert.ToBoolean(ViewState["IsStandAloneRequired"]); }
        set { ViewState["IsStandAloneRequired"] = value; }
    }

    public bool IsGrandParentRequired
    {
        get { return Convert.ToBoolean(ViewState["IsGrandParentRequired"]); }
        set { ViewState["IsGrandParentRequired"] = value; }
    }
    #endregion


    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            lstVendor.Items.Clear();
            txtVenderName2.Text = string.Empty;
            hdnVendorID.Value = string.Empty;
            FillVendor();
        }
    }

    public void ClearSearch()
    {
        lstVendor.Items.Clear();
        txtVenderName2.Text = string.Empty;
        hdnVendorID.Value = string.Empty;
    }

    public void FillVendor()
    {
        UP_VendorBAL oUP_VendorBAL = new UP_VendorBAL();
        UP_VendorBE oUP_VendorBE = new UP_VendorBE();

        oUP_VendorBE.Action = "SearchVendor";
        oUP_VendorBE.VendorName = txtVenderName2.Text.Trim();
        oUP_VendorBE.Site = new BusinessEntities.ModuleBE.AdminFunctions.MAS_SiteBE();

        oUP_VendorBE.IsChildRequired = IsChildRequired != null ? IsChildRequired : (bool?)null;
        oUP_VendorBE.IsGrandParentRequired = IsGrandParentRequired != null ? IsGrandParentRequired : (bool?)null;
        oUP_VendorBE.IsParentRequired = IsParentRequired != null ? IsParentRequired : (bool?)null;
        oUP_VendorBE.IsStandAloneRequired = IsStandAloneRequired != null ? IsStandAloneRequired : (bool?)null;

        if (CountryID == 0)
        {
            oUP_VendorBE.Site.SiteCountryID = -1; //Convert.ToInt32(Session["SiteCountryID"].ToString());
            if (SiteID == 0)
                oUP_VendorBE.Site.SiteID = Convert.ToInt32(Session["SiteID"].ToString());
            else
                oUP_VendorBE.Site.SiteID = SiteID;
        }
        else
            oUP_VendorBE.Site.SiteCountryID = CountryID;


        List<UP_VendorBE> lstUPVendor = oUP_VendorBAL.GetCarrierDetailsBAL(oUP_VendorBE);

        FillControls.FillListBox(ref lstVendor, lstUPVendor.ToList(), "VendorName", "VendorID");

        /// --- Added for Showing Inative Vendors --- 27 Feb 2013
        //foreach (ListItem item in lstVendor.Items)
        //{
        //    for (int i = 0; i < lstUPVendor.Count; i++)
        //    {
        //        if (item.Value == lstUPVendor[i].VendorID.ToString() && lstUPVendor[i].IsActiveVendor == "N")
        //        {
        //            this.lstVendor.Items[i].Attributes.Add("Style", "background-color:RED;");
        //        }
        //    }
        //}

        for (int i = 0; i < lstUPVendor.Count; i++)
        {
            if (lstUPVendor[i].VendorID > 0)
                if (Convert.ToInt32(lstVendor.Items[i].Value) == lstUPVendor[i].VendorID && lstUPVendor[i].IsActiveVendor == "N")
                    this.lstVendor.Items[i].Attributes.Add("Style", "background-color:RED;");
        }
        /// --- Added for Showing Inative Vendors --- 27 Feb 2013
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {

        FillVendor();

    }

    public List<UP_VendorBE> IsValidVendorCode()
    {
        UP_VendorBAL oUP_VendorBAL = new UP_VendorBAL();
        UP_VendorBE oUP_VendorBE = new UP_VendorBE();

        oUP_VendorBE.Action = "ValidateVendor";
        //oUP_VendorBE.Vendor_No = txtVendorNo.Text.Trim();
        oUP_VendorBE.Site = new BusinessEntities.ModuleBE.AdminFunctions.MAS_SiteBE();
        //if (SiteID == 0)
        //    oUP_VendorBE.Site.SiteID = Convert.ToInt32(Session["SiteID"].ToString());
        //else
        //    oUP_VendorBE.Site.SiteID = SiteID;

        if (CountryID == 0)
        {
            oUP_VendorBE.Site.SiteCountryID = -1; //Convert.ToInt32(Session["SiteCountryID"].ToString());
            if (SiteID == 0)
                oUP_VendorBE.Site.SiteID = Convert.ToInt32(Session["SiteID"].ToString());
            else
                oUP_VendorBE.Site.SiteID = SiteID;
        }
        else
            oUP_VendorBE.Site.SiteCountryID = CountryID;


        return oUP_VendorBAL.ValidateVendorBAL(oUP_VendorBE);

    }


    protected void btnMoveRightAll_Click(object sender, EventArgs e)
    {
        if (lstVendor.Items.Count > 0)
            FillControls.MoveAllItems(lstVendor, lstSelectedVendor);
    }
    protected void btnMoveRight_Click(object sender, EventArgs e)
    {
        if (lstVendor.SelectedItem != null)
        {
            FillControls.MoveOneItem(lstVendor, lstSelectedVendor);
        }
    }
    protected void btnMoveLeft_Click(object sender, EventArgs e)
    {
        if (lstSelectedVendor.SelectedItem != null)
        {
            FillControls.MoveOneItem(lstSelectedVendor, lstVendor);
        }
    }
    protected void btnMoveLeftAll_Click(object sender, EventArgs e)
    {
        if (lstSelectedVendor.Items.Count > 0)
        {
            FillControls.MoveAllItems(lstSelectedVendor, lstVendor);
        }
    }

    protected void Up1_Init(object sender, EventArgs e)
    {
        if (ScriptManager.GetCurrent(Page) == null)
        {
            ScriptManager sMgr = new ScriptManager();
            sMgr.EnablePartialRendering = true;
            phControls.Controls.Add(sMgr);
        }
    }
}
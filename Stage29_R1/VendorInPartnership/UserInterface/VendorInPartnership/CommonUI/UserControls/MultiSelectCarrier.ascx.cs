﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.Security;
using BusinessLogicLayer.ModuleBAL.Security;
using Utilities; using WebUtilities;
public partial class SeacrhnSelectCarrier : System.Web.UI.UserControl
{
   

    protected void Page_Load(object sender, EventArgs e)
    {        
        if (!Page.IsPostBack) {
            lstCarrier.Items.Clear();
             FillCarrier();
        }
    }

    public void FillCarrier()
    {
        SCT_UserBAL oUP_CarrierBAL = new SCT_UserBAL();
        SCT_UserBE oUP_CarrierBE = new SCT_UserBE();
        oUP_CarrierBE.Action = "SearchCarrier";
        oUP_CarrierBE.UserID = Convert.ToInt32(Session["UserID"]);
        List<SCT_UserBE> lstUPCarrier = new List<SCT_UserBE>();
        lstUPCarrier = oUP_CarrierBAL.GetCarrierDetailsBAL(oUP_CarrierBE);

        FillControls.FillListBox(ref lstCarrier, lstUPCarrier.ToList(), "CarrierName", "CarrierID");
    }

    
    protected void btnMoveRightAll_Click(object sender, EventArgs e) {
        if (lstCarrier.Items.Count > 0)
            FillControls.MoveAllItems(lstCarrier, lstSelectedCarrier);
    }
    protected void btnMoveRight_Click(object sender, EventArgs e) {
        if (lstCarrier.SelectedItem != null)
        {
            FillControls.MoveOneItem(lstCarrier, lstSelectedCarrier);
        }
    }
    protected void btnMoveLeft_Click(object sender, EventArgs e) {
        if (lstSelectedCarrier.SelectedItem != null)
        {
            FillControls.MoveOneItem(lstSelectedCarrier, lstCarrier);
        }
    }
    protected void btnMoveLeftAll_Click(object sender, EventArgs e) {
        if (lstSelectedCarrier.Items.Count > 0)
        {
            FillControls.MoveAllItems(lstSelectedCarrier, lstCarrier);
        }
    }
    private string _selectedCarrierIDs = string.Empty;
    //public string SelectedCarrierIDs
    //{
    //    get
    //    {
    //        _selectedCarrierIDs = string.Empty;
    //        if (lstSelectedCarrier.Items.Count > 0)
    //        {
    //            foreach (ListItem item in lstSelectedCarrier.Items)
    //            {
    //                _selectedCarrierIDs += "," + item.Value;
    //            }
    //            return _selectedCarrierIDs.Trim(',');
    //        }
    //        return null;
    //    }
    //}

    //public string SelectedCarrierName
    //{
    //    get
    //    {
    //        string _selectedCarrierName = string.Empty;

    //        foreach (ListItem item in lstSelectedCarrier.Items)
    //            _selectedCarrierName += item.Text + ", ";

    //        if (_selectedCarrierName.Length > 0)
    //            _selectedCarrierName = _selectedCarrierName.Substring(0, _selectedCarrierName.Length - 2);

    //        return _selectedCarrierName;
    //    }
    //}


    public string SelectedCarrierIDs
    {
        get
        {
            if (!string.IsNullOrEmpty(hiddenSelectedIDs.Value))
            {
                return hiddenSelectedIDs.Value.Trim(',');
            }
            return null;
        }
    }

    public string SelectedCarrierName
    {
        get
        {
            string _selectedCarrierName = string.Empty;

            if (!string.IsNullOrEmpty(hiddenSelectedName.Value))
            {
                _selectedCarrierName = hiddenSelectedName.Value;
            }

            if (_selectedCarrierName.Length > 0)
                _selectedCarrierName = _selectedCarrierName.Substring(0, _selectedCarrierName.Length - 2);

            return _selectedCarrierName;
        }
    }
    
}
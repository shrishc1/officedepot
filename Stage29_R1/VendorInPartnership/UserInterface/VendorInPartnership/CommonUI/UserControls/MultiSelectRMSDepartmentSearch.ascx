﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MultiSelectRMSDepartmentSearch.ascx.cs"
    Inherits="MultiSelectRMSDepartmentSearch" %>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<script language="javascript" type="text/javascript">
    $(document).ready(function () {
        var lstitem = document.getElementById('<%= lstRight.ClientID %>');
        if (lstitem.options.length == 0) {
            document.getElementById('<%= hiddenSelectedIDs.ClientID %>').value = "";
            document.getElementById('<%= hiddenSelectedName.ClientID %>').value = "";
        }
    });

    function ReBindListValue() {
        var varLstRightUser = document.getElementById('<%=lstRight.ClientID%>');
        var varHdnListRight = $("input[id*=hdnListRightCategory]");
        $("input[id*=hdnListRightCategory]").val('');
        if (varLstRightUser != null) {
            for (var i = 0; i < varLstRightUser.options.length; i++) {
                if ($("input[id*=hdnListRightCategory]").val() == '') {
                    $("input[id*=hdnListRightCategory]").val(varLstRightUser.options[i].text + '#'
                        + varLstRightUser.options[i].value);
                }
                else {
                    $("input[id*=hdnListRightCategory]").val($("input[id*=hdnListRightCategory]").val() + '$' + varLstRightUser.options[i].text + '#'
                        + varLstRightUser.options[i].value);
                }
            }
        }
    }

</script>
<table>
    <tr>
        <td colspan="3">
            <cc1:ucTextbox ID="txtRmsdepartment" runat="server" MaxLength="50" Width="170px"></cc1:ucTextbox>
            &nbsp;
            <cc1:ucButton CssClass="button" runat="server" ID="btnRmsDepartment" OnClientClick="ReBindListValue();"
                Text="Search RMS Department" onclick="btnRmsDepartment_Click" />
            <asp:HiddenField ID="hdnListRightCategory" runat="server" />
        </td>
    </tr>
    <tr>
        <td>
            <cc1:ucListBox ID="lstLeft" runat="server" Height="100px" Width="320px">
            </cc1:ucListBox>
        </td>
        <td align="center" class="search-controls-btn">
            <div>
                <input type="button" id="btnMoveRightAll" value=">>" class="button" style="width: 35px"
                    onclick="Javascript:MoveAll('<%= lstLeft.ClientID %>', '<%= lstRight.ClientID %>','<%= hiddenSelectedIDs.ClientID %>','<%= hiddenSelectedName.ClientID %>');" />
            </div>
            &nbsp;
            <div>
                <input type="button" id="btnMoveRight" value=">" class="button" style="width: 35px"
                    onclick="Javascript:MoveItem('<%= lstLeft.ClientID %>', '<%= lstRight.ClientID %>','<%= hiddenSelectedIDs.ClientID %>','<%= hiddenSelectedName.ClientID %>');" />
            </div>
            &nbsp;
            <div>
                <input type="button" id="btnMoveLeft" value="<" class="button" style="width: 35px"
                    onclick="Javascript:MoveItem('<%= lstRight.ClientID %>', '<%= lstLeft.ClientID %>','<%= hiddenSelectedIDs.ClientID %>','<%= hiddenSelectedName.ClientID %>');" />
            </div>
            &nbsp;
            <div>
                <input type="button" id="btnMoveLeftAll" value="<<" class="button" style="width: 35px"
                    onclick="Javascript:MoveAll('<%= lstRight.ClientID %>', '<%= lstLeft.ClientID %>','<%= hiddenSelectedIDs.ClientID %>','<%= hiddenSelectedName.ClientID %>');" />
            </div>
        </td>
        <td>
            <cc1:ucListBox ID="lstRight" runat="server" Height="100px" Width="320px" EnableViewState="true">
            </cc1:ucListBox>
            <asp:HiddenField ID="hiddenSelectedIDs" runat="server"/>
            <asp:HiddenField ID="hiddenSelectedName" runat="server" />
        </td>
    </tr>
</table>

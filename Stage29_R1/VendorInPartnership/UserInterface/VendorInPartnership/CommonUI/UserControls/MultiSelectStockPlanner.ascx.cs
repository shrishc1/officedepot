﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.Security;
using BusinessLogicLayer.ModuleBAL.Security;
using System.Data;

public partial class MultiSelectStockPlanner : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindStockPlanner();
            PreSelectedStockPlanner();
        }
    }

    private void BindStockPlanner() {
        SCT_UserBE oSCT_UserBE = new SCT_UserBE();
        SCT_UserBAL oMAS_UserBAL = new SCT_UserBAL();
         
        oSCT_UserBE.Action = "GetStockPlannersWithOutCountry";
        oSCT_UserBE.UserID = Convert.ToInt32(Session["UserID"]);
        if (!string.IsNullOrWhiteSpace(txtUserName.Text.Trim()))
            oSCT_UserBE.UserSearchText = txtUserName.Text.Trim();

        DataTable dtStockPlanner = oMAS_UserBAL.GetStockPlannerWithCountryBAL(oSCT_UserBE);
        if (dtStockPlanner.Rows.Count > 0) {
            FillControls.FillListBox(ref ucLBLeft, dtStockPlanner, "StockPlannerWithCountry", "StockPlannerID");
        }
    }


    public void PreSelectedStockPlanner() {

        if (Session["UserRoleId"] != null && Convert.ToInt32(Session["UserRoleId"]) == 6) {
            ucLBRight.Items.Add(new ListItem(Convert.ToString(Session["UserName"]), Convert.ToString(Session["UserID"])));
            hiddenSelectedIDs.Value = Convert.ToString(Session["UserID"]) + ",";
            hiddenSelectedName.Value = Convert.ToString(Session["UserName"]) + " ,";
        }
    }

    public void clearsearch() {
        ucLBRight.Items.Clear();
        hiddenSelectedIDs.Value = string.Empty;
        hiddenSelectedName.Value = string.Empty;
    }

    public void SearchStockPlannerClick(string StockPlonnerNo)
    {
        txtUserName.Text = StockPlonnerNo;
        BindStockPlanner();
    }
    private string _selectedStockPlannerIDs = string.Empty;
    

    public string SelectedStockPlannerUserIDs {
        get {
            if (!string.IsNullOrEmpty(hiddenSelectedIDs.Value)) {
                hiddenSelectedIDs.Value.Replace(",,", ",").Trim(',');
            }
            return hiddenSelectedIDs.Value;
        }
    }

    public string SelectedStockPlannerIDs
    {
        get
        {
            if (!string.IsNullOrEmpty(hiddenSelectedIDs.Value))
            {                
                SCT_UserBE oSCT_UserBE = new SCT_UserBE();
                SCT_UserBAL oMAS_UserBAL = new SCT_UserBAL();

                oSCT_UserBE.Action = "GetStockPlannerIdsByUserIds";
                oSCT_UserBE.UserIds = hiddenSelectedIDs.Value.Replace(",,", ",").Trim(',');

                DataTable dtStockPlanner = oMAS_UserBAL.GetStockPlannerWithCountryByUserIdsBAL(oSCT_UserBE);

                string result = String.Join(",", dtStockPlanner.AsEnumerable().Select(row => row.Field<int>("StockPlannerID")));
                return result;
            }
            return null;
        }
        set
        {
            hiddenSelectedIDs.Value += value + ",";
        }
    }

    public string SelectedSPIDsForOB
    {
        get
        {
            if (!string.IsNullOrEmpty(hiddenSelectedIDs.Value))
            {
                SCT_UserBE oSCT_UserBE = new SCT_UserBE();
                SCT_UserBAL oMAS_UserBAL = new SCT_UserBAL();

                oSCT_UserBE.Action = "GetStockPlannerIdsByUserIds";
                oSCT_UserBE.UserIds = hiddenSelectedIDs.Value.Replace(",,", ",").Trim(',');

                DataTable dtStockPlanner = oMAS_UserBAL.GetStockPlannerWithCountryByUserIdsBAL(oSCT_UserBE);

                string result = String.Join(",", dtStockPlanner.AsEnumerable().Select(row => row.Field<int>("StockPlannerID")));
                result = result + ",";
                return result;
            }          
            return null;
        }
        set
        {
            hiddenSelectedIDs.Value += value + ",";
        }
    }


    public string SelectedSPName
    {
        get
        {
            string _selectedSPName = string.Empty;

            if (!string.IsNullOrEmpty(hiddenSelectedName.Value))
            {
                _selectedSPName = hiddenSelectedName.Value;
            }

            if (_selectedSPName.Length > 0)
                _selectedSPName = _selectedSPName.Substring(0, _selectedSPName.Length - 1);

            return _selectedSPName;
        }
        set
        {
            hiddenSelectedName.Value += value + ",";
        }
    }

    public string SelectedSPNameForOB
    {
        get
        {
            string _selectedSPName = string.Empty;

            if (!string.IsNullOrEmpty(hiddenSelectedName.Value))
            {
                _selectedSPName = hiddenSelectedName.Value;
            }
            return _selectedSPName;
        }
        set
        {
            hiddenSelectedName.Value += value + ",";
        }
    }

    protected virtual void btnMoveRightAll_Click(object sender, EventArgs e)
    {
        FillControls.MoveAllItems(ucLBLeft, ucLBRight);
    }

    protected virtual void btnMoveRight_Click(object sender, EventArgs e)
    {
        if (ucLBLeft.SelectedItem != null)
        {
            FillControls.MoveOneItem(ucLBLeft, ucLBRight);
        }
    }

    protected virtual void btnMoveLeft_Click(object sender, EventArgs e)
    {
        if (ucLBRight.SelectedItem != null)
        {
            FillControls.MoveOneItem(ucLBRight, ucLBLeft);
        }
    }

    protected virtual void btnMoveLeftAll_Click(object sender, EventArgs e)
    {
        FillControls.MoveAllItems(ucLBRight, ucLBLeft);
    }

    protected void btnSearchUser_Click(object sender, EventArgs e) {
        ucLBLeft.Items.Clear();
        BindStockPlanner();
        ReBingRightList();
    }

    private void ReBingRightList() {
        ucLBRight.Items.Clear();
        if (!string.IsNullOrWhiteSpace(hdnListRightUser.Value)) {
            string[] Emails = hdnListRightUser.Value.Split('$');
            for (int index = 0; index < Emails.Length; index++) {
                string[] EmailIds = Emails[index].Split('#');
                ListItem listItem = new ListItem(EmailIds[0], EmailIds[1]);
                ucLBRight.Items.Add(listItem);
            }
            hdnListRightUser.Value = String.Empty;
        }
    }


    public void SetSPOnPostBack() {
        ucLBRight.Items.Clear();
        string[] strSelectedName = hiddenSelectedName.Value.TrimEnd(',').TrimEnd(' ').Split(',');
        string[] strSelectedIDs = hiddenSelectedIDs.Value.TrimEnd(',').Split(',');

        if (strSelectedIDs.Length > 0)
            this.BindStockPlanner();
        if (strSelectedName.Length == strSelectedIDs.Length)
        {
            for (int iCount = 0; iCount < strSelectedIDs.Length; iCount++)
            {
                if (!string.IsNullOrEmpty(strSelectedIDs[iCount]) && !string.IsNullOrWhiteSpace(strSelectedIDs[iCount]))
                {
                    ucLBRight.Items.Add(new ListItem(strSelectedName[iCount].ToString().Trim(), strSelectedIDs[iCount].ToString().Trim()));
                    RemoveSP(strSelectedIDs[iCount].ToString().Trim());
                }
            }
        }
    }

    public void setStockPlannerOnPostBack()
    {
        ucLBRight.Items.Clear();
        string[] strSelectedName = hiddenSelectedName.Value.Split(',');
        string[] strSelectedIDs = hiddenSelectedIDs.Value.Replace(",,", ",").Replace(",,", "").Split(',');
        for (int iCount = 0; iCount < strSelectedIDs.Length; iCount++)
        {
            if (!string.IsNullOrEmpty(strSelectedIDs[iCount]) && !string.IsNullOrWhiteSpace(strSelectedIDs[iCount]) && (strSelectedName.Length == strSelectedIDs.Length))
            {
                ucLBRight.Items.Add(new ListItem(strSelectedName[iCount].ToString().Trim(), strSelectedIDs[iCount].ToString().Trim()));
                ucLBLeft.Items.Remove(new ListItem(strSelectedName[iCount].ToString().Trim(), strSelectedIDs[iCount].ToString().Trim()));
            }
        }
    }

    public void RemoveSP(string SPID) {
        if (ucLBLeft.Items.FindByValue(SPID.Trim()) != null)
            ucLBLeft.Items.RemoveAt(ucLBLeft.Items.IndexOf(ucLBLeft.Items.FindByValue(SPID.Trim())));
    }
}
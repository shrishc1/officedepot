﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucSeacrhVendorForDisc.ascx.cs" Inherits="ucSeacrhVendorForDisc" %>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<script language="javascript" type="text/javascript">
    function lstVendor_DoubleClick() {
        document.getElementById('<%=btnSelect.ClientID%>').click();
    }

    function showVendor() {
        $find('VendorViewer').show();
        document.getElementById('<%=isSearch.ClientID%>').value = "true";
        return false;
    }

    function setValues() {

        var e = document.getElementById('<%=lstVendor.ClientID%>');
        var strVendorVal = e.options[e.selectedIndex].value;
        var strVendor = e.options[e.selectedIndex].text;

        document.getElementById('<%=hdnVendorID.ClientID%>').value = strVendorVal;
        document.getElementById('<%=txtVendorNo.ClientID%>').value = strVendor.split('-')[0];
        document.getElementById('<%=SelectedVendorName.ClientID%>').innerHTML = trim(strVendor);
        document.getElementById('<%=hdnVendorName.ClientID%>').value = strVendor;
        $find('VendorViewer').hide();
        document.getElementById('<%=btnHiddensetValues.ClientID%>').click();
        return false;
    }

    function vendorNoChanged() {
        if (trim(document.getElementById('<%=txtVendorNo.ClientID%>').value) != "") {
            document.getElementById('<%=btnHidden.ClientID%>').click();
        }
        else {
            document.getElementById('<%=hdnVendorID.ClientID%>').value = "";
            document.getElementById('<%=txtVendorNo.ClientID%>').value = "";
            document.getElementById('<%=SelectedVendorName.ClientID%>').innerHTML = "";
            document.getElementById('<%=hdnVendorName.ClientID%>').value = "";
        }
    }

    function trim(stringToTrim) {
        return stringToTrim.replace(/^\s+|\s+$/g, "");
    }
    function ltrim(stringToTrim) {
        return stringToTrim.replace(/^\s+/, "");
    }
    function rtrim(stringToTrim) {
        return stringToTrim.replace(/\s+$/, "");
    }

</script>

<asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
        <asp:HiddenField ID="hdnVendorID" runat="server" />
        <asp:HiddenField ID="hdnParentVendorID" runat="server" />
        <asp:HiddenField ID="hdnVendorName" runat="server" />
        <asp:Button ID="btnHidden" runat="server" OnClick="btnHidden_Click" style="display:none;" />
        <asp:Button ID="btnHiddensetValues" runat="server" OnClick="btnHiddensetValues_Click" style="display:none;" />
        <asp:HiddenField ID="isSearch" runat="server" Value="false" />

        <table  cellspacing="0" cellpadding="0" border="0">
            <tr valign="top">
                <td style="font-weight: bold;" width="40px">          
                    <cc1:ucTextbox ID="txtVendorNo" runat="server" Width="33px" 
                        MaxLength="6" AutoPostBack="true" 
                        ontextchanged="txtVendorNo_TextChanged" ></cc1:ucTextbox>
                </td>
                 <td style="font-weight: bold;" width="20px;">
                    <input type="image" id="imgVendor" name="imgVendor" src="<%= ResolveClientUrl("../../Images/Search.gif")%>" style="height:16px; width:16px;" onclick="return showVendor();" />
                </td>
                 <td style="font-weight: bold;" width="300px;">
                   <cc1:ucLabel ID="SelectedVendorName"  BorderWidth="0" runat="server" Width="250px" ></cc1:ucLabel>
                                        
                </td>
            </tr>
      </table>
        <%---Report Viewer popup start--%>
        <asp:Button ID="btnViewVendor" runat="Server" Style="display: none" />
        <ajaxToolkit:ModalPopupExtender ID="mdlVendorViewer" runat="server" TargetControlID="btnViewVendor"
            PopupControlID="pnlbtnViewVendorViewer" BackgroundCssClass="modalBackground"
            BehaviorID="VendorViewer" DropShadow="false" />
        <asp:Panel ID="pnlbtnViewVendorViewer" runat="server" Style="display: none; width: 500px;">
            <a href="#" onclick="javascript:$find('VendorViewer').hide();return false;" class="close-btn">                
            </a>
            <div style="width: 100%; overflow-y: hidden; overflow-x: hidden; background-color: #fff;
                padding: 5px; border: 2px solid #ccc; text-align: left;">
                <table width="100%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                    <tr>
                        <td style="font-weight: bold;" width="25%">
                            <cc1:ucLabel ID="lblVendorName" runat="server" Text="Vendor Name"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;" width="5%">
                            :
                        </td>
                        <td width="50%">
                            <cc1:ucTextbox ID="txtVenderName2" runat="server" Width="147px"></cc1:ucTextbox>
                        </td>
                        <td width="20%">
                            <cc1:ucButton OnClick="btnSearch_Click" ID="btnSearch" runat="server" 
                                Text="Search" CssClass="button" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            &nbsp;
                        </td>
                        <td style="font-weight: bold;" align="left" colspan="2">                           
                           <cc1:ucListBox ID="lstVendor" runat="server" Width="99%" Height="200px" ondblclick="lstVendor_DoubleClick" ></cc1:ucListBox>
                        </td>                      
                    </tr>
                    <tr>
                          <td colspan="4" align="center">
                            <%--<input type="button" width="60px" value="Select" class="button" onclick="setValues();"/>--%>
                            <cc1:ucButton OnClick="btnSelect_Click" ID="btnSelect" runat="server" 
                                Text="Select" CssClass="button" Width="60px" />
                            &nbsp;
                            <%--<input type="button" width="60px" value="Cancel" class="button" onclick="javascript:$find('VendorViewer').hide();return false;" />--%>
                            <cc1:ucButton OnClick="btnPopupCancel_Click" ID="btnPopupCancel" runat="server" 
                                Text="Cancel" CssClass="button" Width="60px" />
                        </td>
                    </tr>
                    <tr><td colspan="4">&nbsp;</td></tr>
                </table>
            </div>
        </asp:Panel>

        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td align="center">
                                    <asp:UpdateProgress runat="server" ID="PageUpdateProgress" AssociatedUpdatePanelID="UpdatePanel1">
                                        <ProgressTemplate>
                                            <div align="center" style="background-color: #000; top: 0px; left: 0px; bottom: 0px;
                                                right: 0px; padding-top: 20%; margin: 0; width: 100%; height: 50%; overflow: hidden;
                                                position: absolute; z-index: 1000; filter: alpha(opacity=50); opacity: 0.5;">
                                                <asp:Image ID="imgWait" runat="server" ImageUrl="~/Images/Ajaxloading.gif" ImageAlign="Middle" />
                                            </div>
                                        </ProgressTemplate>
                                    </asp:UpdateProgress>
                                </td>
                            </tr>
                        </table>

    </ContentTemplate>
</asp:UpdatePanel>
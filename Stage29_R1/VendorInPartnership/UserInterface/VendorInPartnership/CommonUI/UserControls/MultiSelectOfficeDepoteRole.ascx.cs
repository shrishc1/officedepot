﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLogicLayer.ModuleBAL.Security;
using BusinessEntities.ModuleBE.Security;

public partial class MultiSelectOfficeDepoteRole : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            this.BindODRoles();
        }
    }

    private void BindODRoles()
    {
        var oSCT_TemplateBAL = new SCT_TemplateBAL();
        var oSCT_UserRoleBE = new SCT_UserRoleBE();
        oSCT_UserRoleBE.Action = "GetAllUsers";

        var lstRole = oSCT_TemplateBAL.GetUserRolesBAL(oSCT_UserRoleBE);
        lstRole = lstRole.FindAll(x => !x.RoleName.Equals("Vendor") && !x.RoleName.Equals("Carrier"));
        oSCT_TemplateBAL = null;
        lstLeft.Items.Clear();
        if (lstRole.Count > 0)
            FillControls.FillListBox(ref lstLeft, lstRole, "RoleName", "UserRoleID");
    }

    public string SelectedOfficeDepotRoleIDs
    {
        get
        {
            if (!string.IsNullOrEmpty(hiddenSelectedIDs.Value))
            {
                return hiddenSelectedIDs.Value.Trim(',');
            }
            return null;
        }
    }

    public string SelectedOfficeDepotRoles
    {
        get
        {
            string selectedOfficeDepotRoles = string.Empty;

            if (!string.IsNullOrEmpty(hiddenSelectedName.Value))
            {
                selectedOfficeDepotRoles = hiddenSelectedName.Value;
            }
            if (selectedOfficeDepotRoles.Length > 0)
                selectedOfficeDepotRoles = selectedOfficeDepotRoles.Substring(0, selectedOfficeDepotRoles.Length - 2);

            return selectedOfficeDepotRoles;
        }
    }

    protected virtual void btnMoveRightAll_Click(object sender, EventArgs e)
    {
        FillControls.MoveAllItems(lstLeft, lstRight);
    }

    protected virtual void btnMoveRight_Click(object sender, EventArgs e)
    {
        if (lstLeft.SelectedItem != null)
        {
            FillControls.MoveOneItem(lstLeft, lstRight);
        }
    }

    protected virtual void btnMoveLeft_Click(object sender, EventArgs e)
    {
        if (lstRight.SelectedItem != null)
        {
            FillControls.MoveOneItem(lstRight, lstLeft);
        }
    }

    protected virtual void btnMoveLeftAll_Click(object sender, EventArgs e)
    {
        FillControls.MoveAllItems(lstRight, lstLeft);
    }

    public void SetODRoleOnPostBack()
    {
        lstRight.Items.Clear();
        string[] strSelectedName = hiddenSelectedName.Value.TrimEnd(',').Split(',');
        string[] strSelectedIDs = hiddenSelectedIDs.Value.TrimEnd(',').Split(',');

        if(strSelectedIDs.Length>0)
            this.BindODRoles();

        for (int iCount = 0; iCount < strSelectedIDs.Length; iCount++)
        {
            if (!string.IsNullOrEmpty(strSelectedIDs[iCount]) && !string.IsNullOrWhiteSpace(strSelectedIDs[iCount]))
            {
                lstRight.Items.Add(new ListItem(strSelectedName[iCount].ToString().Trim(), strSelectedIDs[iCount].ToString().Trim()));
                RemoveODRole(strSelectedIDs[iCount].ToString().Trim());
            }
        }
    }

    private void RemoveODRole(string ODId)
    {
        if (lstLeft.Items.FindByValue(ODId.Trim()) != null)
            lstLeft.Items.RemoveAt(lstLeft.Items.IndexOf(lstLeft.Items.FindByValue(ODId.Trim())));
    }
}
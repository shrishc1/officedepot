﻿using System;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.AdminFunctions;
using BusinessLogicLayer.ModuleBAL.AdminFunctions;
using System.Collections.Generic;

public partial class ModuleUI_Discrepancy_Report_UserControl_MultiSelectCountry : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            this.BindCountry();
        }
    }

    public void BindCountry()
    {
        MAS_CountryBE oMAS_CountryBE = new MAS_CountryBE();
        MAS_CountryBAL oMAS_CountryBAL = new MAS_CountryBAL();

        oMAS_CountryBE.Action = "ShowAll";
        oMAS_CountryBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
        oMAS_CountryBE.User.UserID = Convert.ToInt32(Session["UserID"]);

        List<MAS_CountryBE> lstCountry = new List<MAS_CountryBE>();
        lstCountry = oMAS_CountryBAL.GetCountryBAL(oMAS_CountryBE);

        if (lstCountry.Count > 0)
            FillControls.FillListBox(ref lstLeft, lstCountry, "CountryName", "CountryID");
    }

    private string _selectedCountryIDs = string.Empty;
    //public string SelectedCountryIDs
    //{
    //    get
    //    {
    //        _selectedCountryIDs = string.Empty;
    //        if (lstRight.Items.Count > 0)
    //        {
    //            foreach (ListItem item in lstRight.Items)
    //                _selectedCountryIDs += "," + item.Value;

    //            return _selectedCountryIDs.Trim(',');
    //        }
    //        return null;
    //    }
    //}

    //public string SelectedCountryName
    //{
    //    get
    //    {
    //        string _selectedCountryName = string.Empty;

    //        foreach (ListItem item in lstRight.Items)
    //            _selectedCountryName += item.Text + ", ";

    //        if (_selectedCountryName.Length > 0)
    //            _selectedCountryName = _selectedCountryName.Substring(0, _selectedCountryName.Length - 2);

    //        return _selectedCountryName;
    //    }
    //}

    public string SelectedCountryIDs
    {
        get
        {
            if (!string.IsNullOrEmpty(hiddenSelectedIDs.Value))
            {
                return hiddenSelectedIDs.Value.Trim(',');
            }
            return null;
        }
        set
        {
            hiddenSelectedIDs.Value += value + ",";
        }
    }

    public string SelectedCountryName
    {
        get
        {
            string _selectedCountryName = string.Empty;

            if (!string.IsNullOrEmpty(hiddenSelectedName.Value))
            {
                _selectedCountryName = hiddenSelectedName.Value;
            }
            if (_selectedCountryName.Length > 0)
                _selectedCountryName = _selectedCountryName.Substring(0, _selectedCountryName.Length - 2);

            return _selectedCountryName;
        }
        set
        {
            hiddenSelectedName.Value += value + ",";
        }
    }

    protected virtual void btnMoveRightAll_Click(object sender, EventArgs e)
    {
        FillControls.MoveAllItems(lstLeft, lstRight);
    }

    protected virtual void btnMoveRight_Click(object sender, EventArgs e)
    {
        if (lstLeft.SelectedItem != null)
        {
            FillControls.MoveOneItem(lstLeft, lstRight);
        }
    }

    protected virtual void btnMoveLeft_Click(object sender, EventArgs e)
    {
        if (lstRight.SelectedItem != null)
        {
            FillControls.MoveOneItem(lstRight, lstLeft);
        }
    }

    protected virtual void btnMoveLeftAll_Click(object sender, EventArgs e)
    {
        FillControls.MoveAllItems(lstRight, lstLeft);
    }

    public void SetCountryOnPostBack()
    {
        lstRight.Items.Clear();
        string[] strSelectedName = hiddenSelectedName.Value.TrimEnd(',').TrimEnd(' ').Split(',');
        string[] strSelectedIDs = hiddenSelectedIDs.Value.TrimEnd(',').Split(',');

        if (strSelectedIDs.Length > 0)
            this.BindCountry();

        for (int iCount = 0; iCount < strSelectedIDs.Length; iCount++)
        {
            if (!string.IsNullOrEmpty(strSelectedIDs[iCount]) && !string.IsNullOrWhiteSpace(strSelectedIDs[iCount]))
            {
                lstRight.Items.Add(new ListItem(strSelectedName[iCount].ToString().Trim(), strSelectedIDs[iCount].ToString().Trim()));
                RemoveCountry(strSelectedIDs[iCount].ToString().Trim());
            }
        }
    }

    public void RemoveCountry(string CountryId)
    {
        if (lstLeft.Items.FindByValue(CountryId.Trim()) != null)
            lstLeft.Items.RemoveAt(lstLeft.Items.IndexOf(lstLeft.Items.FindByValue(CountryId.Trim())));
    }
}
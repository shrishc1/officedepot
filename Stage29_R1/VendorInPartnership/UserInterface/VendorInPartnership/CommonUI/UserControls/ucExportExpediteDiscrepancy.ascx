﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucExportExpediteDiscrepancy.ascx.cs"
    Inherits="ucExportExpediteDiscrepancy" %>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<div>
    <span>
        <cc1:ucButton ID="btnExportToExcel" runat="server" CssClass="exporttoexcel" OnClick="btnExport_Click"
            Width="109px" Height="20px" />
    </span><span>
        <cc1:ucGridView ID="gvDisLogExport" Width="110%" runat="server" CssClass="grid" OnRowDataBound="gvDisLog_RowDataBound" Style="overflow: auto; display: none;">
            <Columns>
                <asp:TemplateField HeaderText="Site">
                    <HeaderStyle HorizontalAlign="Left" Width="300px" />
                    <ItemStyle HorizontalAlign="Left" />
                    <ItemTemplate>
                        <cc1:ucLiteral ID="ltSiteName" runat="server" Text='<%#Eval("SiteName") %>'></cc1:ucLiteral>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField HeaderText="Status" DataField="DiscrepancyStatusDiscreption" AccessibleHeaderText="false">
                    <HeaderStyle Width="100px" HorizontalAlign="Left" />
                    <ItemStyle HorizontalAlign="Left" />
                </asp:BoundField>
                <asp:TemplateField HeaderText="VDR Number" SortExpression="VDRNo">
                    <HeaderStyle Width="80px" HorizontalAlign="Left" />
                    <ItemStyle HorizontalAlign="Left" />
                    <ItemTemplate>
                        <asp:HyperLink ID="hypLinkToDisDetails" runat="server" Text='<%#Eval("VDRNo") %>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField HeaderText="VDR Discription" DataField="DiscrepancyDiscripton">
                    <HeaderStyle HorizontalAlign="Left" Width="300px" />
                    <ItemStyle HorizontalAlign="Left" />
                </asp:BoundField>
                <asp:TemplateField HeaderText="Query" SortExpression="Query">
                    <HeaderStyle HorizontalAlign="Left" Width="200px" />
                    <ItemStyle HorizontalAlign="Left" />
                    <ItemTemplate>
                        <cc1:ucLiteral ID="ltQuery" runat="server" Text='<%#Eval("Query") %>'></cc1:ucLiteral>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField HeaderText="GI" DataField="GI" SortExpression="GI">
                    <HeaderStyle HorizontalAlign="Left" Width="300px" />
                    <ItemStyle HorizontalAlign="Left" />
                </asp:BoundField>
                <asp:BoundField HeaderText="INV" DataField="INV" SortExpression="INV">
                    <HeaderStyle HorizontalAlign="Left" Width="300px" />
                    <ItemStyle HorizontalAlign="Left" />
                </asp:BoundField>
                <asp:BoundField HeaderText="AP" DataField="AP" SortExpression="AP">
                    <HeaderStyle HorizontalAlign="Left" Width="300px" />
                    <ItemStyle HorizontalAlign="Left" />
                </asp:BoundField>
                <asp:BoundField HeaderText="VEN" DataField="VEN" SortExpression="VEN">
                    <HeaderStyle HorizontalAlign="Left" Width="300px" />
                    <ItemStyle HorizontalAlign="Left" />
                </asp:BoundField>
                <asp:TemplateField HeaderText="CreateDate" SortExpression="DiscrepancyLogDate">
                    <HeaderStyle HorizontalAlign="Left" Width="200px" />
                    <ItemStyle HorizontalAlign="Left" />
                    <ItemTemplate>
                        <cc1:ucLiteral ID="ltDiscrepancyLogDate" runat="server" Text='<%#Eval("DiscrepancyLogDate", "{0:dd/MM/yyyy}") %>'></cc1:ucLiteral>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField HeaderText="Delivery Note" DataField="DeliveryNoteNumber">
                    <HeaderStyle HorizontalAlign="Left" Width="100px" />
                    <ItemStyle HorizontalAlign="Left" />
                </asp:BoundField>
                <asp:BoundField HeaderText="Vendor" DataField="VendorNoName">
                    <HeaderStyle HorizontalAlign="Left" Width="400px" />
                    <ItemStyle HorizontalAlign="Left" Wrap="true" />
                </asp:BoundField>
                <asp:BoundField HeaderText="PO Number" DataField="PurchaseOrderNumber">
                    <HeaderStyle HorizontalAlign="Left" Width="100px" />
                    <ItemStyle HorizontalAlign="Left" />
                </asp:BoundField>
                <asp:TemplateField HeaderText="CreatedBy" SortExpression="FirstName">
                    <HeaderStyle HorizontalAlign="Left" Width="400px" />
                    <ItemStyle HorizontalAlign="Left" />
                    <ItemTemplate>
                        <cc1:ucLiteral ID="ltUserName" runat="server" Text='<%#Eval("FirstName") %>'></cc1:ucLiteral>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField HeaderText="Stock Planner" DataField="StockPlannerName">
                    <HeaderStyle HorizontalAlign="Left" Width="400px" />
                    <ItemStyle HorizontalAlign="Left" />
                </asp:BoundField>

                <asp:BoundField HeaderText="Requested Discrepancy Charge" DataField="PTotal"
                    SortExpression="DiscrepancyCharge">
                    <HeaderStyle HorizontalAlign="Left" Width="100px" />
                    <ItemStyle HorizontalAlign="Left" />
                </asp:BoundField>
                <asp:BoundField HeaderText="Goods in Action" DataField="GINActionCategory"
                    SortExpression="GINActionCategory">
                    <HeaderStyle HorizontalAlign="Left" Width="100px" />
                    <ItemStyle HorizontalAlign="Left" />
                </asp:BoundField>
                <asp:BoundField HeaderText="Inventory Action" DataField="InventoryActionCategory"
                    SortExpression="InventoryActionCategory">
                    <HeaderStyle HorizontalAlign="Left" Width="100px" />
                    <ItemStyle HorizontalAlign="Left" />
                </asp:BoundField>
                <asp:BoundField HeaderText="Acounts Payable Action" DataField="APActionCategory"
                    SortExpression="APActionCategory">
                    <HeaderStyle HorizontalAlign="Left" Width="100px" />
                    <ItemStyle HorizontalAlign="Left" />
                </asp:BoundField>
                <asp:BoundField HeaderText="Vendor Action" DataField="VendorActionCategory"
                    SortExpression="VendorActionCategory">
                    <HeaderStyle HorizontalAlign="Left" Width="100px" />
                    <ItemStyle HorizontalAlign="Left" />
                </asp:BoundField>
            </Columns>
        </cc1:ucGridView>
    </span>
</div>

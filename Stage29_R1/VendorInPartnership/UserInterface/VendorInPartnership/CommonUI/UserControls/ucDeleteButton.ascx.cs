﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using BaseControlLibrary;


    public partial class ucDeleteButton : UserControl {
        private string navigateUrl;

        #region Public Property
        public string NavigateUrl {
            get {
                return navigateUrl;
            }
            set {
                navigateUrl = value;
            }
        }
        #endregion

        protected void btnDelete_Click(object sender, EventArgs e) {

        }
    }

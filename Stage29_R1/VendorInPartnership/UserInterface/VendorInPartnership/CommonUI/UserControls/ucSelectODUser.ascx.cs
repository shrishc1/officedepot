﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using BusinessEntities.ModuleBE.Discrepancy;
using BusinessLogicLayer.ModuleBAL.Discrepancy;


public partial class ucSelectODUser : System.Web.UI.UserControl
{
    public int CountryID {
        get {
            return ViewState["CountryID"] != null ? (int)ViewState["CountryID"] : 0;
        }
        set {
            ViewState["CountryID"] = value;
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    //private DataTable dtUser {
    //    get {
    //        return ViewState["dtUser"] != null ? (DataTable)ViewState["dtUser"] : null;
    //    }
    //    set {
    //        ViewState["dtUser"] = value;
    //    }
    //}

    public void ClearAndBindODUser() {

        MediatorBE oMediatorBE = new MediatorBE();
        MediatorBAL oMediatorBAL = new MediatorBAL();

        oMediatorBE.Action = "GetExistingMediatorUser";
        oMediatorBE.Country = new BusinessEntities.ModuleBE.AdminFunctions.MAS_CountryBE();
        oMediatorBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();

        oMediatorBE.Country.CountryID = CountryID;
      
        DataTable lstUser = oMediatorBAL.GetMediatorUserBAL(oMediatorBE);


        if (lstUser != null && lstUser.Rows.Count > 0) {
            txtODUser.Text = lstUser.Rows[0]["UserName"].ToString();
            hiddenSelectedUserID.Value = lstUser.Rows[0]["UserID"].ToString(); ;
            hiddenSelectedUserName.Value = lstUser.Rows[0]["UserName"].ToString(); ;
        }

        
    }

    private void BindODUser() {
        ucLBLeft.Items.Clear();

        MediatorBE oMediatorBE = new MediatorBE();
        MediatorBAL oMediatorBAL = new MediatorBAL();

        oMediatorBE.Action = "GetMediatorUser";
        oMediatorBE.Country = new BusinessEntities.ModuleBE.AdminFunctions.MAS_CountryBE();
        oMediatorBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();

        oMediatorBE.Country.CountryID = CountryID;
        if (!string.IsNullOrWhiteSpace(txtUserName.Text.Trim()))
            oMediatorBE.User.UserName = txtUserName.Text.Trim();

       DataTable lstUser = oMediatorBAL.GetMediatorUserBAL(oMediatorBE);

        
        if (lstUser != null && lstUser.Rows.Count > 0) {
            FillControls.FillListBox(ref ucLBLeft, lstUser, "UserName", "UserID");
        }
    }

    private string _selectedUserID = string.Empty;

    public string SelectedUserID {
        get {
            if (!string.IsNullOrEmpty(hiddenSelectedUserID.Value)) {
                return hiddenSelectedUserID.Value;
            }
            return null;
        }
    }


    public string SelectedUserName {
        get {
            string _selectedSPName = string.Empty;

            if (!string.IsNullOrEmpty(hiddenSelectedUserName.Value)) {
                return hiddenSelectedUserName.Value;
            }
            return _selectedSPName;
        }
    }

    protected void btnSearchUser_Click(object sender, EventArgs e) {
        BindODUser();
    }

    public void RemoveUserID(string UserID) {
        if (ucLBLeft.Items.FindByValue(UserID.Trim()) != null)
            ucLBLeft.Items.RemoveAt(ucLBLeft.Items.IndexOf(ucLBLeft.Items.FindByValue(UserID.Trim())));
    }

}
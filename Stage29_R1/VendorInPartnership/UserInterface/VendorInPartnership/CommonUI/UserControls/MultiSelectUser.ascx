﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MultiSelectUser.ascx.cs" Inherits="MultiSelectUser" %>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>

<asp:UpdatePanel ID="UpdatePanelVendor" runat="server">
    <ContentTemplate>
        <script type="text/javascript">
            function ReBindListValue() {
                var varLstRightUser = document.getElementById('<%=lstRightUser.ClientID%>');
                var varHdnListRight = $("input[id*=hdnListRightUser]");
                $("input[id*=hdnListRightUser]").val('');
                if (varLstRightUser != null) {
                    for (var i = 0; i < varLstRightUser.options.length; i++) {
                        if ($("input[id*=hdnListRightUser]").val() == '') {
                            $("input[id*=hdnListRightUser]").val(varLstRightUser.options[i].text + '#'
                        + varLstRightUser.options[i].value);
                        }
                        else {
                            $("input[id*=hdnListRightUser]").val($("input[id*=hdnListRightUser]").val() + '$' + varLstRightUser.options[i].text + '#'
                        + varLstRightUser.options[i].value);
                        }
                    }
                }
            }
        </script>
        <table>
            <tr>
                <td>
                    <table border="0">
                        <tr>
                            <td align="left" style="width: 30%">
                                <table>
                                    <tr>
                                        <td>
                                            <cc1:ucTextbox ID="txtUserName" runat="server" MaxLength="15" Width="170px"></cc1:ucTextbox>
                                        </td>
                                        <td>
                                            <cc1:ucButton CssClass="button" runat="server" ID="btnSearchUser"
                                            OnClientClick="ReBindListValue();" OnClick="btnSearchUser_Click" />
                                            <asp:HiddenField  ID="hdnListRightUser" runat="server"/>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <cc1:ucListBox ID="lstLeft" runat="server" Height="150px" Width="320px">
                                </cc1:ucListBox>
                            </td>
                        </tr>
                    </table>
                </td>
               <td align="center" class="search-controls-btn">
                    <div>
                        <input type="button" id="Button1" value=">>" class="button" style="width: 35px" onclick="Javascript:MoveAll('<%= lstLeft.ClientID %>','<%= lstRightUser.ClientID %>', '<%= hiddenSelectedIDs.ClientID %>','<%= hiddenSelectedName.ClientID %>');" />
                    </div>
                    &nbsp;
                    <div>
                        <input type="button" id="btnMoveRight" value=">" class="button" style="width: 35px"
                            onclick="Javascript:MoveItem('<%= lstLeft.ClientID %>', '<%= lstRightUser.ClientID %>','<%= hiddenSelectedIDs.ClientID %>','<%= hiddenSelectedName.ClientID %>');" />
                    </div>
                    &nbsp;
                    <div>
                        <input type="button" id="btnMoveLeft" value="<" class="button" style="width: 35px"
                            onclick="Javascript:MoveItem('<%= lstRightUser.ClientID %>', '<%= lstLeft.ClientID %>','<%= hiddenSelectedIDs.ClientID %>','<%= hiddenSelectedName.ClientID %>');" />
                    </div>
                    &nbsp;
                    <div>
                        <input type="button" id="btnMoveLeftAll" value="<<" class="button" style="width: 35px"
                            onclick="Javascript:MoveAll('<%= lstRightUser.ClientID %>', '<%= lstLeft.ClientID %>','<%= hiddenSelectedIDs.ClientID %>','<%= hiddenSelectedName.ClientID %>');" />
                    </div>
                </td>
                <td>
                    <cc1:ucListBox ID="lstRightUser" runat="server" Height="150px" Width="320px">
                    </cc1:ucListBox>
                    <asp:HiddenField ID="hiddenSelectedIDs" runat="server" />
                    <asp:HiddenField ID="hiddenSelectedName" runat="server" />
                </td>
            </tr>
        </table>
    </ContentTemplate>
</asp:UpdatePanel>

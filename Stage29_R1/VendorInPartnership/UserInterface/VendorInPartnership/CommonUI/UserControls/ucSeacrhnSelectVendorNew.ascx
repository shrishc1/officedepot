﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucSeacrhnSelectVendorNew.ascx.cs"
    Inherits="ucSeacrhnSelectVendorNew" %>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<%@ Register Src="~/CommonUI/UserControls/ucVendorForCountry.ascx" TagName="ucVendorForCountry"
    TagPrefix="cc2" %>
<table>
    <tr>
        <td>
            <cc2:ucVendorForCountry ID="ucVendorForCountry" runat="server" />
        </td>
        <td align="center" class="search-controls-btn">
            <div>
                <input type="button" id="btnMoveRightAll" value=">>" class="button" style="width: 35px"
                    onclick="Javascript:MoveAll('<%=ucVendorForCountry.FindControl("lstLeft").ClientID %>', '<%= lstSelectedVendor.ClientID %>','<%= hiddenSelectedIDs.ClientID %>','<%= hiddenSelectedName.ClientID %>');" />
            </div>
            &nbsp;
            <div>
                <input type="button" id="btnMoveRight" value=">" class="button" style="width: 35px"
                    onclick="Javascript:MoveItem('<%=ucVendorForCountry.FindControl("lstLeft").ClientID %>', '<%= lstSelectedVendor.ClientID %>','<%= hiddenSelectedIDs.ClientID %>','<%= hiddenSelectedName.ClientID %>');" />
            </div>
            &nbsp;
            <div>
                <input type="button" id="btnMoveLeft" value="<" class="button" style="width: 35px"
                    onclick="Javascript:MoveItem('<%= lstSelectedVendor.ClientID %>', '<%=ucVendorForCountry.FindControl("lstLeft").ClientID %>','<%= hiddenSelectedIDs.ClientID %>','<%= hiddenSelectedName.ClientID %>');" />
            </div>
            &nbsp;
            <div>
                <input type="button" id="btnMoveLeftAll" value="<<" class="button" style="width: 35px"
                    onclick="Javascript:MoveAll('<%= lstSelectedVendor.ClientID %>', '<%=ucVendorForCountry.FindControl("lstLeft").ClientID %>','<%= hiddenSelectedIDs.ClientID %>','<%= hiddenSelectedName.ClientID %>');" />
            </div>
        </td>
        <td>
            <cc1:ucListBox ID="lstSelectedVendor" runat="server" Height="150px" Width="320px">
            </cc1:ucListBox>
            <asp:HiddenField ID="hiddenSelectedIDs" runat="server" />
            <asp:HiddenField ID="hiddenSelectedName" runat="server" />
        </td>
    </tr>
</table>

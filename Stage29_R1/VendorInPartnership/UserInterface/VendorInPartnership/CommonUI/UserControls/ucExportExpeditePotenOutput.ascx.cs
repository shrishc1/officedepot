﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebUtilities;
using BusinessEntities.ModuleBE.StockOverview;
using BusinessLogicLayer.ModuleBAL.StockOverview;
using System.Collections;
using Utilities;
using System.Data;

public partial class ucExportExpeditePotenOutput : System.Web.UI.UserControl
{
    private string fileName;

    #region Public Property

    public CommonPage CurrentPage
    {
        get;
        set;
    }

    public string FileName
    {
        get
        {
            return fileName;
        }
        set
        {
            fileName = value;
        }
    }

    #endregion

    #region Events

    protected void Page_PreRender(object sender, EventArgs e)
    {
        //this.btnExportToExcel.Text = string.Empty;
        //this.btnExportToExcel.Width = Unit.Pixel(18);
    }

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnExport_Click(object sender, EventArgs e)
    {
        fileName = fileName.Replace(" ", "");
        ExportExcel(fileName);
       // this.BindGridFromSessionSearchCriteria();
       // WebCommon.Export(fileName, gvPotentialOutputExcelExp);
    }

    protected void ExportExcel(string fileName)
    {
         ExpediteStockBE oExpediteStockBE = new ExpediteStockBE();
        ExpediteStockBAL oExpediteStockBAL = new ExpediteStockBAL();
        var dt = new DataTable();
        if (Session["ExpediteStockOutput"] != null)
        {
            var htExpediteStockOutput = (Hashtable)Session["ExpediteStockOutput"];
            if ((htExpediteStockOutput.ContainsKey("isExpeditesubfilter") && htExpediteStockOutput["isExpeditesubfilter"] != null) && Convert.ToBoolean(htExpediteStockOutput["isExpeditesubfilter"]) == true)
            {
                oExpediteStockBE.SelectedSiteIDs = (htExpediteStockOutput.ContainsKey("AdditionalSelectedSiteIDs") && htExpediteStockOutput["AdditionalSelectedSiteIDs"] != null) ? htExpediteStockOutput["AdditionalSelectedSiteIDs"].ToString() : null;
                if (oExpediteStockBE.SelectedSiteIDs == "0")
                {
                    oExpediteStockBE.SelectedSiteIDs = (htExpediteStockOutput.ContainsKey("SelectedSiteIDs") && htExpediteStockOutput["SelectedSiteIDs"] != null) ? htExpediteStockOutput["SelectedSiteIDs"].ToString() : null;
                }
                oExpediteStockBE.SelectedIncludedVendorIDs = (htExpediteStockOutput.ContainsKey("AdditionalSelectedIncludedVendorIDs") && htExpediteStockOutput["AdditionalSelectedIncludedVendorIDs"] != null) ? htExpediteStockOutput["AdditionalSelectedIncludedVendorIDs"].ToString() : null;
                oExpediteStockBE.SelectedVIKINGSKU = (htExpediteStockOutput.ContainsKey("AdditionalSelectedVIKINGSKU") && htExpediteStockOutput["AdditionalSelectedVIKINGSKU"] != null) ? htExpediteStockOutput["AdditionalSelectedVIKINGSKU"].ToString() : null;
                oExpediteStockBE.SelectedODSKU = (htExpediteStockOutput.ContainsKey("AdditionalSelectedODSKU") && htExpediteStockOutput["AdditionalSelectedODSKU"] != null) ? htExpediteStockOutput["AdditionalSelectedODSKU"].ToString() : null;
                oExpediteStockBE.SortingDay = (htExpediteStockOutput.ContainsKey("SortingDay") && htExpediteStockOutput["SortingDay"] != null) ? htExpediteStockOutput["SortingDay"].ToString() : string.Empty;
            }
            else
            {
                oExpediteStockBE.SelectedSiteIDs = (htExpediteStockOutput.ContainsKey("SelectedSiteIDs") && htExpediteStockOutput["SelectedSiteIDs"] != null) ? htExpediteStockOutput["SelectedSiteIDs"].ToString() : null;
                oExpediteStockBE.SelectedIncludedVendorIDs = (htExpediteStockOutput.ContainsKey("SelectedIncludedVendorIDs") && htExpediteStockOutput["SelectedIncludedVendorIDs"] != null) ? htExpediteStockOutput["SelectedIncludedVendorIDs"].ToString() : null;
                oExpediteStockBE.SelectedVIKINGSKU = (htExpediteStockOutput.ContainsKey("SelectedVIKINGSKU") && htExpediteStockOutput["SelectedVIKINGSKU"] != null) ? htExpediteStockOutput["SelectedVIKINGSKU"].ToString() : null;
                oExpediteStockBE.SelectedODSKU = (htExpediteStockOutput.ContainsKey("SelectedODSKU") && htExpediteStockOutput["SelectedODSKU"] != null) ? htExpediteStockOutput["SelectedODSKU"].ToString() : null;
            }            

            oExpediteStockBE.SelectedStockPlannerIDs = (htExpediteStockOutput.ContainsKey("SelectedStockPlannerIDs") && htExpediteStockOutput["SelectedStockPlannerIDs"] != null) ? htExpediteStockOutput["SelectedStockPlannerIDs"].ToString() : null;
         
            oExpediteStockBE.SelectedExcludedVendorIDs = (htExpediteStockOutput.ContainsKey("SelectedExcludedVendorIDs") && htExpediteStockOutput["SelectedExcludedVendorIDs"] != null) ? htExpediteStockOutput["SelectedExcludedVendorIDs"].ToString() : null;
            
            oExpediteStockBE.SelectedItemClassification = (htExpediteStockOutput.ContainsKey("SelectedItemClassification") && htExpediteStockOutput["SelectedItemClassification"] != null) ? htExpediteStockOutput["SelectedItemClassification"].ToString() : null;
            oExpediteStockBE.SelectedMRPType = (htExpediteStockOutput.ContainsKey("SelectedMRPType") && htExpediteStockOutput["SelectedMRPType"] != null) ? htExpediteStockOutput["SelectedMRPType"].ToString() : null;
            oExpediteStockBE.SelectedPurcGroup = (htExpediteStockOutput.ContainsKey("SelectedPurcGroup") && htExpediteStockOutput["SelectedPurcGroup"] != null) ? htExpediteStockOutput["SelectedPurcGroup"].ToString() : null;
            oExpediteStockBE.SelectedMatGrp = (htExpediteStockOutput.ContainsKey("SelectedMatGrp") && htExpediteStockOutput["SelectedMatGrp"] != null) ? htExpediteStockOutput["SelectedMatGrp"].ToString() : null;
           
            oExpediteStockBE.SelectedSkuType = (htExpediteStockOutput.ContainsKey("SelectedSkuType") && htExpediteStockOutput["SelectedSkuType"] != null) ? Convert.ToInt32(htExpediteStockOutput["SelectedSkuType"].ToString()) : 0;
            oExpediteStockBE.SelectedDisplay = (htExpediteStockOutput.ContainsKey("SelectedDisplay") && htExpediteStockOutput["SelectedDisplay"] != null) ? Convert.ToInt32(htExpediteStockOutput["SelectedDisplay"].ToString()) : 0;
            oExpediteStockBE.POStatusSearchCriteria = (htExpediteStockOutput.ContainsKey("POStatus") && htExpediteStockOutput["POStatus"] != null) ? Convert.ToInt32(htExpediteStockOutput["POStatus"].ToString()) : 0;
            //gvPotentialOutput.PageIndex = Convert.ToInt32(htExpediteStockOutput["PageIndex"].ToString());
            oExpediteStockBE.SortBy = (htExpediteStockOutput.ContainsKey("SortBy") && htExpediteStockOutput["SortBy"] != null) ? htExpediteStockOutput["SortBy"].ToString() : null;
            oExpediteStockBE.CommentStatus = (htExpediteStockOutput.ContainsKey("CommentStatus") && htExpediteStockOutput["CommentStatus"] != null) ? Convert.ToInt32(htExpediteStockOutput["CommentStatus"].ToString()) : 0;

            oExpediteStockBE.DaysStock = (htExpediteStockOutput.ContainsKey("DaysStock") && htExpediteStockOutput["DaysStock"] != null) ? htExpediteStockOutput["DaysStock"].ToString() : null;
            oExpediteStockBE.MinForecastedSales = (htExpediteStockOutput.ContainsKey("MinForecastedSales") && htExpediteStockOutput["MinForecastedSales"] != null) ? htExpediteStockOutput["MinForecastedSales"].ToString() : "0";
            oExpediteStockBE.StockPlannerGroupingID = (htExpediteStockOutput.ContainsKey("SelectedStockPlannerGroupingIDs") && htExpediteStockOutput["SelectedStockPlannerGroupingIDs"] != null) ? htExpediteStockOutput["SelectedStockPlannerGroupingIDs"].ToString() : null;
            oExpediteStockBE.SkugroupingID = (htExpediteStockOutput.ContainsKey("SelectedSkuGroupingIDs") && htExpediteStockOutput["SelectedSkuGroupingIDs"] != null) ? Convert.ToString(htExpediteStockOutput["SelectedSkuGroupingIDs"]) : string.Empty;

            oExpediteStockBE.IsPriority = (htExpediteStockOutput.ContainsKey("PriorityItems") && htExpediteStockOutput["PriorityItems"] != null) ? Convert.ToBoolean(htExpediteStockOutput["PriorityItems"]) : false;
            if (fileName == "ExpediteStock")
            {
                oExpediteStockBE.Action = "GetPotentialOutputExport";
            }
            else
            {
                oExpediteStockBE.Action = "GetPotentialOutputWeekWiseExport";
            }
            dt = oExpediteStockBAL.GetPotentialOutputExportBAL(oExpediteStockBE);
            if (dt.Rows.Count > 0)
            {
                HttpContext context = HttpContext.Current;
                //**************

                //****************
                string date = DateTime.Now.ToString("ddMMyyyy");
                string time = DateTime.Now.ToString("HH:mm");
                context.Response.ContentType = "text/vnd.ms-excel";
                context.Response.AddHeader("Content-Disposition", string.Format("attachment; filename={0}_{1}_{2}", fileName, date, time + ".xls"));


                //context.Response.ContentType = "text/csv";
                //context.Response.AddHeader("Content-Disposition", "attachment; filename=abc.csv");
                if (fileName == "ExpediteStock")
                {
                    var lstExpediteDayDays = this.Get20DaysDate(Convert.ToDateTime(dt.Rows[0]["UpdatedDate"]));
                    if (lstExpediteDayDays.Count > 0)
                    {
                        for (int i = 0; i < dt.Columns.Count; i++)
                        {
                            if (i > 2)
                            {
                                if (i > 3)
                                {
                                    context.Response.Write("\t");
                                }
                                //context.Response.Write(dt.Columns[i].ColumnName);

                                switch (dt.Columns[i].ColumnName.ToString())
                                {
                                    case "D1":
                                        context.Response.Write("|" + lstExpediteDayDays[0].UpdateDate + "|");
                                        break;
                                    case "D2":
                                        context.Response.Write("|" + lstExpediteDayDays[1].UpdateDate + "|");
                                        break;
                                    case "D3":
                                        context.Response.Write("|" + lstExpediteDayDays[2].UpdateDate + "|");
                                        break;
                                    case "D4":
                                        context.Response.Write("|" + lstExpediteDayDays[3].UpdateDate + "|");
                                        break;
                                    case "D5":
                                        context.Response.Write("|" + lstExpediteDayDays[4].UpdateDate + "|");
                                        break;
                                    case "D6":
                                        context.Response.Write("|" + lstExpediteDayDays[5].UpdateDate + "|");
                                        break;
                                    case "D7":
                                        context.Response.Write("|" + lstExpediteDayDays[6].UpdateDate + "|");

                                        break;
                                    case "D8":
                                        context.Response.Write("|" + lstExpediteDayDays[7].UpdateDate + "|");
                                        break;
                                    case "D9":
                                        context.Response.Write("|" + lstExpediteDayDays[8].UpdateDate + "|");
                                        break;
                                    case "D10":
                                        context.Response.Write("|" + lstExpediteDayDays[9].UpdateDate + "|");
                                        break;
                                    case "D11":
                                        context.Response.Write("|" + lstExpediteDayDays[10].UpdateDate + "|");
                                        break;
                                    case "D12":
                                        context.Response.Write("|" + lstExpediteDayDays[11].UpdateDate + "|");
                                        break;
                                    case "D13":
                                        context.Response.Write("|" + lstExpediteDayDays[12].UpdateDate + "|");
                                        break;
                                    case "D14":
                                        context.Response.Write("|" + lstExpediteDayDays[13].UpdateDate + "|");
                                        break;
                                    case "D15":
                                        context.Response.Write("|" + lstExpediteDayDays[14].UpdateDate + "|");
                                        break;
                                    case "D16":
                                        context.Response.Write("|" + lstExpediteDayDays[15].UpdateDate + "|");
                                        break;
                                    case "D17":
                                        context.Response.Write("|" + lstExpediteDayDays[16].UpdateDate + "|");
                                        break;
                                    case "D18":
                                        context.Response.Write("|" + lstExpediteDayDays[17].UpdateDate + "|");
                                        break;
                                    case "D19":
                                        context.Response.Write("|" + lstExpediteDayDays[18].UpdateDate + "|");
                                        break;
                                    case "D20":
                                        context.Response.Write("|" + lstExpediteDayDays[19].UpdateDate + "|");
                                        break;
                                    default:
                                        context.Response.Write(dt.Columns[i].ColumnName);
                                        break;
                                }
                            }
                        }
                    }
                }
                else
                {
                    for (int i = 0; i < dt.Columns.Count; i++)
                    {
                        if (i > 2)
                        {
                            if (i > 3)
                            {
                                context.Response.Write("\t");
                            }
                            //context.Response.Write(dt.Columns[i].ColumnName);

                            context.Response.Write(dt.Columns[i].ColumnName);
                        }
                    }
                }

                context.Response.Write(Environment.NewLine);

                //Write data
                foreach (DataRow row in dt.Rows)
                {

                    for (int i = 0; i < dt.Columns.Count; i++)
                    {
                        if (i > 2)
                        {

                            if (i > 3)
                            {
                                context.Response.Write("\t");
                            }

                            DateTime dateTime;
                            DateTime.TryParse(row.ItemArray[i].ToString(), out dateTime);
                            if (dateTime.Year != 1)
                            {
                                var text = dateTime.ToString("dd/MM/yyyy");
                                context.Response.Write(text);
                            }
                            else
                            {
                                context.Response.Write(row.ItemArray[i].ToString());
                            }

                            //context.Response.Write(row.ItemArray[i].ToString());
                        }

                    }
                    context.Response.Write(Environment.NewLine);
                }
                context.Response.End();
            }
        }
    }
    protected void gvPotentialOutput_OnRowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType != DataControlRowType.Header && e.Row.RowType != DataControlRowType.Footer)
        {
            #region Logic to set the color of cell based on their Color status ...

            var lblCommentColor = e.Row.FindControl("lblCommentColor") as Literal;
            if (lblCommentColor != null)
                e.Row.Cells[0].BackColor = GetColor(lblCommentColor.Text);

            var StatusValue = e.Row.FindControl("lblStatusValue") as Label;
            if (StatusValue != null)
                e.Row.Cells[1].BackColor = GetColor(StatusValue.Text);

            var lblFirstWeekMONColor = e.Row.FindControl("lblFirstWeekMONColor") as Literal;
            if (lblFirstWeekMONColor != null)
                e.Row.Cells[23].BackColor = GetColor(lblFirstWeekMONColor.Text);

            var lblFirstWeekTUEColor = e.Row.FindControl("lblFirstWeekTUEColor") as Literal;
            if (lblFirstWeekTUEColor != null)
                e.Row.Cells[24].BackColor = GetColor(lblFirstWeekTUEColor.Text);

            var lblFirstWeekWEDColor = e.Row.FindControl("lblFirstWeekWEDColor") as Literal;
            if (lblFirstWeekWEDColor != null)
                e.Row.Cells[25].BackColor = GetColor(lblFirstWeekWEDColor.Text);

            var lblFirstWeekTHUColor = e.Row.FindControl("lblFirstWeekTHUColor") as Literal;
            if (lblFirstWeekTHUColor != null)
                e.Row.Cells[26].BackColor = GetColor(lblFirstWeekTHUColor.Text);

            var lblFirstWeekFRIColor = e.Row.FindControl("lblFirstWeekFRIColor") as Literal;
            if (lblFirstWeekFRIColor != null)
                e.Row.Cells[27].BackColor = GetColor(lblFirstWeekFRIColor.Text);

            var lblSecondWeekMONColor = e.Row.FindControl("lblSecondWeekMONColor") as Literal;
            if (lblSecondWeekMONColor != null)
                e.Row.Cells[28].BackColor = GetColor(lblSecondWeekMONColor.Text);

            var lblSecondWeekTUEColor = e.Row.FindControl("lblSecondWeekTUEColor") as Literal;
            if (lblSecondWeekTUEColor != null)
                e.Row.Cells[29].BackColor = GetColor(lblSecondWeekTUEColor.Text);

            var lblSecondWeekWEDColor = e.Row.FindControl("lblSecondWeekWEDColor") as Literal;
            if (lblSecondWeekWEDColor != null)
                e.Row.Cells[30].BackColor = GetColor(lblSecondWeekWEDColor.Text);

            var lblSecondWeekTHUColor = e.Row.FindControl("lblSecondWeekTHUColor") as Literal;
            if (lblSecondWeekTHUColor != null)
                e.Row.Cells[31].BackColor = GetColor(lblSecondWeekTHUColor.Text);

            var lblSecondWeekFRIColor = e.Row.FindControl("lblSecondWeekFRIColor") as Literal;
            if (lblSecondWeekFRIColor != null)
                e.Row.Cells[32].BackColor = GetColor(lblSecondWeekFRIColor.Text);

            var lblThirdWeekMONColor = e.Row.FindControl("lblThirdWeekMONColor") as Literal;
            if (lblThirdWeekMONColor != null)
                e.Row.Cells[33].BackColor = GetColor(lblThirdWeekMONColor.Text);

            var lblThirdWeekTUEColor = e.Row.FindControl("lblThirdWeekTUEColor") as Literal;
            if (lblThirdWeekTUEColor != null)
                e.Row.Cells[34].BackColor = GetColor(lblThirdWeekTUEColor.Text);

            var lblThirdWeekWEDColor = e.Row.FindControl("lblThirdWeekWEDColor") as Literal;
            if (lblThirdWeekWEDColor != null)
                e.Row.Cells[35].BackColor = GetColor(lblThirdWeekWEDColor.Text);

            var lblThirdWeekTHUColor = e.Row.FindControl("lblThirdWeekTHUColor") as Literal;
            if (lblThirdWeekTHUColor != null)
                e.Row.Cells[36].BackColor = GetColor(lblThirdWeekTHUColor.Text);

            var lblThirdWeekFRIColor = e.Row.FindControl("lblThirdWeekFRIColor") as Literal;
            if (lblThirdWeekFRIColor != null)
                e.Row.Cells[37].BackColor = GetColor(lblThirdWeekFRIColor.Text);

            var lblFourthWeekMONColor = e.Row.FindControl("lblFourthWeekMONColor") as Literal;
            if (lblFourthWeekMONColor != null)
                e.Row.Cells[38].BackColor = GetColor(lblFourthWeekMONColor.Text);

            var lblFourthWeekTUEColor = e.Row.FindControl("lblFourthWeekTUEColor") as Literal;
            if (lblFourthWeekTUEColor != null)
                e.Row.Cells[39].BackColor = GetColor(lblFourthWeekTUEColor.Text);

            var lblFourthWeekWEDColor = e.Row.FindControl("lblFourthWeekWEDColor") as Literal;
            if (lblFourthWeekWEDColor != null)
                e.Row.Cells[40].BackColor = GetColor(lblFourthWeekWEDColor.Text);

            var lblFourthWeekTHUColor = e.Row.FindControl("lblFourthWeekTHUColor") as Literal;
            if (lblFourthWeekTHUColor != null)
                e.Row.Cells[41].BackColor = GetColor(lblFourthWeekTHUColor.Text);

            var lblFourthWeekFRIColor = e.Row.FindControl("lblFourthWeekFRIColor") as Literal;
            if (lblFourthWeekFRIColor != null)
                e.Row.Cells[42].BackColor = GetColor(lblFourthWeekFRIColor.Text);

            #endregion

            var UpdatedDateValue = e.Row.FindControl("lblUpdatedDateValue") as Literal;
            if (UpdatedDateValue != null && ViewState["UpdatedDateValue"] == null)
            {
                ViewState["UpdatedDateValue"] = UpdatedDateValue.Text;
            }

            var lnkEarliestAdviseDateValue = e.Row.FindControl("lnkEarliestAdviseDateValue") as HyperLink;
            if (lnkEarliestAdviseDateValue != null)
                if (string.IsNullOrEmpty(lnkEarliestAdviseDateValue.Text) || string.IsNullOrWhiteSpace(lnkEarliestAdviseDateValue.Text))
                    lnkEarliestAdviseDateValue.Text = "NA";
        }
    }

    private System.Drawing.Color GetColor(string colorText)
    {
        var color = new System.Drawing.Color();
        switch (colorText)
        {
            case "R":
                color = System.Drawing.Color.Red;
                break;
            case "O":
                color = System.Drawing.Color.Orange;
                break;
            case "B":
                color = System.Drawing.Color.SkyBlue;
                break;
            case "G":
                color = System.Drawing.Color.LightGreen;
                break;
            case "Y":
                color = System.Drawing.Color.Yellow;
                break;
            default:
                break;
        }

        return color;
    }

    private void BindGridFromSessionSearchCriteria()
    {

        ExpediteStockBE oExpediteStockBE = new ExpediteStockBE();
        ExpediteStockBAL oExpediteStockBAL = new ExpediteStockBAL();
        if (Session["ExpediteStockOutput"] != null)
        {
            var htExpediteStockOutput = (Hashtable)Session["ExpediteStockOutput"];
            oExpediteStockBE.SelectedStockPlannerIDs = (htExpediteStockOutput.ContainsKey("SelectedStockPlannerIDs") && htExpediteStockOutput["SelectedStockPlannerIDs"] != null) ? htExpediteStockOutput["SelectedStockPlannerIDs"].ToString() : "";
            oExpediteStockBE.SelectedIncludedVendorIDs = (htExpediteStockOutput.ContainsKey("SelectedIncludedVendorIDs") && htExpediteStockOutput["SelectedIncludedVendorIDs"] != null) ? htExpediteStockOutput["SelectedIncludedVendorIDs"].ToString() : "";
            oExpediteStockBE.SelectedExcludedVendorIDs = (htExpediteStockOutput.ContainsKey("SelectedExcludedVendorIDs") && htExpediteStockOutput["SelectedExcludedVendorIDs"] != null) ? htExpediteStockOutput["SelectedExcludedVendorIDs"].ToString() : "";
            oExpediteStockBE.SelectedSiteIDs = (htExpediteStockOutput.ContainsKey("SelectedSiteIDs") && htExpediteStockOutput["SelectedSiteIDs"] != null) ? htExpediteStockOutput["SelectedSiteIDs"].ToString() : "";
            oExpediteStockBE.SelectedItemClassification = (htExpediteStockOutput.ContainsKey("SelectedItemClassification") && htExpediteStockOutput["SelectedItemClassification"] != null) ? htExpediteStockOutput["SelectedItemClassification"].ToString() : "";
            oExpediteStockBE.SelectedMRPType = (htExpediteStockOutput.ContainsKey("SelectedMRPType") && htExpediteStockOutput["SelectedMRPType"] != null) ? htExpediteStockOutput["SelectedMRPType"].ToString() : "";
            oExpediteStockBE.SelectedPurcGroup = (htExpediteStockOutput.ContainsKey("SelectedPurcGroup") && htExpediteStockOutput["SelectedPurcGroup"] != null) ? htExpediteStockOutput["SelectedPurcGroup"].ToString() : "";
            oExpediteStockBE.SelectedMatGrp = (htExpediteStockOutput.ContainsKey("SelectedMatGrp") && htExpediteStockOutput["SelectedMatGrp"] != null) ? htExpediteStockOutput["SelectedMatGrp"].ToString() : "";
            oExpediteStockBE.SelectedVIKINGSKU = (htExpediteStockOutput.ContainsKey("SelectedVIKINGSKU") && htExpediteStockOutput["SelectedVIKINGSKU"] != null) ? htExpediteStockOutput["SelectedVIKINGSKU"].ToString() : "";
            oExpediteStockBE.SelectedODSKU = (htExpediteStockOutput.ContainsKey("SelectedODSKU") && htExpediteStockOutput["SelectedODSKU"] != null) ? htExpediteStockOutput["SelectedODSKU"].ToString() : "";
            oExpediteStockBE.SelectedSkuType = (htExpediteStockOutput.ContainsKey("SelectedSkuType") && htExpediteStockOutput["SelectedSkuType"] != null) ? Convert.ToInt32(htExpediteStockOutput["SelectedSkuType"].ToString()) : 0;
            oExpediteStockBE.SelectedDisplay = (htExpediteStockOutput.ContainsKey("SelectedDisplay") && htExpediteStockOutput["SelectedDisplay"] != null) ? Convert.ToInt32(htExpediteStockOutput["SelectedDisplay"].ToString()) : 0;
            //gvPotentialOutput.PageIndex = Convert.ToInt32(htExpediteStockOutput["PageIndex"].ToString());
            oExpediteStockBE.SortBy = (htExpediteStockOutput.ContainsKey("SortBy") && htExpediteStockOutput["SortBy"] != null) ? htExpediteStockOutput["SortBy"].ToString() : string.Empty;
            oExpediteStockBE.CommentStatus = (htExpediteStockOutput.ContainsKey("CommentStatus") && htExpediteStockOutput["CommentStatus"] != null) ? Convert.ToInt32(htExpediteStockOutput["CommentStatus"].ToString()) : 0;
            oExpediteStockBE.Action = "GetPotentialOutputExcel";

            var lstExpediteStock = oExpediteStockBAL.GetPotentialOutputBAL(oExpediteStockBE);
            if (lstExpediteStock != null && lstExpediteStock.Count > 0)
                gvPotentialOutputExcelExp.DataSource = lstExpediteStock;
            else
                gvPotentialOutputExcelExp.DataSource = null;

            gvPotentialOutputExcelExp.DataBind();

            #region Logic to set the 20 days header text ...
            var lstExpediteDayDays = this.Get20DaysDate(Convert.ToDateTime(ViewState["UpdatedDateValue"]));
            if (lstExpediteDayDays.Count > 0)
            {
                #region Changing header of Export Excel Grid ...
                for (int index = 0; index < gvPotentialOutputExcelExp.Columns.Count; index++)
                {
                    switch (gvPotentialOutputExcelExp.Columns[index].HeaderText)
                    {
                        case "FirstWeekMon":
                            gvPotentialOutputExcelExp.HeaderRow.Cells[index].Text = lstExpediteDayDays[0].UpdateDate;
                            break;
                        case "FirstWeekTue":
                            gvPotentialOutputExcelExp.HeaderRow.Cells[index].Text = lstExpediteDayDays[1].UpdateDate;
                            break;
                        case "FirstWeekWed":
                            gvPotentialOutputExcelExp.HeaderRow.Cells[index].Text = lstExpediteDayDays[2].UpdateDate;
                            break;
                        case "FirstWeekThu":
                            gvPotentialOutputExcelExp.HeaderRow.Cells[index].Text = lstExpediteDayDays[3].UpdateDate;
                            break;
                        case "FirstWeekFri":
                            gvPotentialOutputExcelExp.HeaderRow.Cells[index].Text = lstExpediteDayDays[4].UpdateDate;
                            break;
                        case "SecondWeekMon":
                            gvPotentialOutputExcelExp.HeaderRow.Cells[index].Text = lstExpediteDayDays[5].UpdateDate;
                            break;
                        case "SecondWeekTue":
                            gvPotentialOutputExcelExp.HeaderRow.Cells[index].Text = lstExpediteDayDays[6].UpdateDate;
                            break;
                        case "SecondWeekWed":
                            gvPotentialOutputExcelExp.HeaderRow.Cells[index].Text = lstExpediteDayDays[7].UpdateDate;
                            break;
                        case "SecondWeekThu":
                            gvPotentialOutputExcelExp.HeaderRow.Cells[index].Text = lstExpediteDayDays[8].UpdateDate;
                            break;
                        case "SecondWeekFri":
                            gvPotentialOutputExcelExp.HeaderRow.Cells[index].Text = lstExpediteDayDays[9].UpdateDate;
                            break;
                        case "ThirdWeekMon":
                            gvPotentialOutputExcelExp.HeaderRow.Cells[index].Text = lstExpediteDayDays[10].UpdateDate;
                            break;
                        case "ThirdWeekTue":
                            gvPotentialOutputExcelExp.HeaderRow.Cells[index].Text = lstExpediteDayDays[11].UpdateDate;
                            break;
                        case "ThirdWeekWed":
                            gvPotentialOutputExcelExp.HeaderRow.Cells[index].Text = lstExpediteDayDays[12].UpdateDate;
                            break;
                        case "ThirdWeekThu":
                            gvPotentialOutputExcelExp.HeaderRow.Cells[index].Text = lstExpediteDayDays[13].UpdateDate;
                            break;
                        case "ThirdWeekFri":
                            gvPotentialOutputExcelExp.HeaderRow.Cells[index].Text = lstExpediteDayDays[14].UpdateDate;
                            break;
                        case "FourthWeekMon":
                            gvPotentialOutputExcelExp.HeaderRow.Cells[index].Text = lstExpediteDayDays[15].UpdateDate;
                            break;
                        case "FourthWeekTue":
                            gvPotentialOutputExcelExp.HeaderRow.Cells[index].Text = lstExpediteDayDays[16].UpdateDate;
                            break;
                        case "FourthWeekWed":
                            gvPotentialOutputExcelExp.HeaderRow.Cells[index].Text = lstExpediteDayDays[17].UpdateDate;
                            break;
                        case "FourthWeekThu":
                            gvPotentialOutputExcelExp.HeaderRow.Cells[index].Text = lstExpediteDayDays[18].UpdateDate;
                            break;
                        case "FourthWeekFri":
                            gvPotentialOutputExcelExp.HeaderRow.Cells[index].Text = lstExpediteDayDays[19].UpdateDate;
                            break;
                    }
                }
                #endregion
            }

            #endregion
        }
    }

    private List<ExpediteStockBE> Get20DaysDate(DateTime updateDate)
    {
        var loopMaxCount = 35;
        var dayDateMaxCount = 20;
        var updateDateValue = updateDate;
        var dayUpdateDateValue = updateDateValue.ToString("ddd");
        var lstExpediteStock = new List<ExpediteStockBE>();
        for (int index = 0; index < loopMaxCount; index++)
        {
            if (lstExpediteStock.Count < dayDateMaxCount)
            {
                var nextDate = new DateTime();
                var nextDay = string.Empty;
                if (index.Equals(0))
                {
                    nextDate = updateDateValue;
                    nextDay = nextDate.ToString("ddd");
                }
                else
                {
                    nextDate = updateDateValue.AddDays(index);
                    nextDay = nextDate.ToString("ddd");
                }

                if (nextDay.Equals("Mon") || nextDay.Equals("Tue") || nextDay.Equals("Wed") || nextDay.Equals("Thu") || nextDay.Equals("Fri"))
                {
                    var expediteStock = new ExpediteStockBE();
                    expediteStock.UpdateDay = string.Format("{0}{1}", nextDay, index);
                    var day = Convert.ToString(nextDate.Date.Day);
                    var month = Convert.ToString(nextDate.Date.Month);

                    if (day.Length.Equals(1))
                        day = string.Format("0{0}", day);

                    if (month.Length.Equals(1))
                        month = string.Format("0{0}", month);

                    expediteStock.UpdateDate = string.Format("{0}/{1}", day, month);
                    lstExpediteStock.Add(expediteStock);
                }
            }
            else
                break;
        }

        return lstExpediteStock;
    }

    public string EncryptQuery(string pURL)
    {
        return Common.EncryptQuery(pURL);
    }

    public void EncryptQueryString(string pURL)
    {
        if (pURL.Contains("?"))
        {
            string[] urlSplit = pURL.Split('?');
            string EncriptedQueryStripg = Encryption.Encrypt(urlSplit[1]);
            Response.Redirect(urlSplit[0] + "?" + EncriptedQueryStripg);
        }
        else
        {
            Response.Redirect(pURL);
        }
    }

    public string EncryptURL(string pURL)
    {
        if (pURL.Contains("?"))
        {
            string[] urlSplit = pURL.Split('?');
            string EncriptedQueryStripg = Encryption.Encrypt(urlSplit[1]);
            return urlSplit[0] + "?" + EncriptedQueryStripg;
        }
        else
        {
            return pURL;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using BusinessLogicLayer.ModuleBAL.Appointment.SiteSettings;

public partial class ucMultiSelectVendor : System.Web.UI.UserControl
{
    #region Public Property

    //public string VendorNo
    // {
    //     get
    //     {
    //         return hdnVendorID.Value;
    //     }
    // }

    //public int CountryID
    //{
    //    get { return Convert.ToInt32(ViewState["CountryID"]); }
    //    set { ViewState["CountryID"] = value; }
    //}

    public int SiteID
    {
        get { return Convert.ToInt32(ViewState["SiteID"]); }
        set { ViewState["SiteID"] = value; }
    }

    private int searchCountryID;
    public int SearchCountryID
    {
        get { return searchCountryID; }
        set { searchCountryID = value; }
    }

    #endregion
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            lstVendor.Items.Clear();
            txtVendorNo.Text = string.Empty;
            hdnVendorID.Value = string.Empty;
            //this.BindVendor();
        }
    }

    public void ClearSearch()
    {
        lstVendor.Items.Clear();
        txtVendorNo.Text = string.Empty;
        hdnVendorID.Value = string.Empty;
    }
    
    private void BindVendor()
    {
        var oUP_VendorBAL = new UP_VendorBAL();
        var oUP_VendorBE = new UP_VendorBE();
        oUP_VendorBE.Action = "GetVendorByName";
        oUP_VendorBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
        oUP_VendorBE.User.UserID = Convert.ToInt32(Session["Userid"]);
        oUP_VendorBE.VendorName = txtVendorNo.Text;
        if (SearchCountryID != 0 && SearchCountryID != (-1))
            oUP_VendorBE.CountryID = SearchCountryID;

        var lstUPVendor = new List<UP_VendorBE>();
        lstUPVendor = oUP_VendorBAL.GetVendorByUserCountryBAL(oUP_VendorBE);
        if (lstUPVendor.Count > 0)
        {
           // txtVendorNumber.Text = "";
            FillControls.FillListBox(ref lstVendor, lstUPVendor, "VendorName", "VendorID");
            for (int i = 0; i < lstUPVendor.Count; i++)
            {
                if (lstUPVendor[i].VendorID > 0)
                    if (Convert.ToInt32(lstVendor.Items[i].Value) == lstUPVendor[i].VendorID && lstUPVendor[i].IsActiveVendor == "N")
                        this.lstVendor.Items[i].Attributes.Add("Style", "background-color:RED;");
            }

            /* Logic to remove the right side existed vendor. */
            for (int i = 0; i < lstSelectedVendor.Items.Count; i++)
            {
                var currentVendor = lstSelectedVendor.Items[i];
                if (lstVendor.Items.Contains(currentVendor))
                    lstVendor.Items.Remove(currentVendor);
            }
        }
        else
        {
            lstVendor.Items.Clear();
        }
    }
    public void SearchVendorClick(string VendorName)
    {
        txtVendorNo.Text = VendorName;
        BindVendor();
    }
    public void SetVendorOnPostBack()
    {
        lstVendor.Items.Clear();
        string[] strSelectedName = hiddenSelectedName.Value.TrimEnd(',').TrimEnd(' ').Split(',');
        string[] strSelectedIDs = hiddenSelectedIDs.Value.TrimEnd(',').Split(',');

        if (strSelectedIDs.Length > 0)
            this.BindVendor();

        for (int iCount = 0; iCount < strSelectedIDs.Length; iCount++)
        {
            if (!string.IsNullOrEmpty(strSelectedIDs[iCount]) && !string.IsNullOrWhiteSpace(strSelectedIDs[iCount]))
            {
                lstVendor.Items.Add(new ListItem(strSelectedName[iCount].ToString().Trim(), strSelectedIDs[iCount].ToString().Trim()));
                RemoveVendor(strSelectedIDs[iCount].ToString().Trim());
            }
        }
    }
    public void RemoveVendor(string VendorIdID)
    {
        if (lstVendor.Items.FindByValue(VendorIdID.Trim()) != null)
            lstVendor.Items.RemoveAt(lstVendor.Items.IndexOf(lstVendor.Items.FindByValue(VendorIdID.Trim())));
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        this.BindVendor();
    }

    public List<UP_VendorBE> IsValidVendorCode()
    {
        var oUP_VendorBAL = new UP_VendorBAL();
        var oUP_VendorBE = new UP_VendorBE();

        oUP_VendorBE.Action = "ValidateVendor";
        //oUP_VendorBE.Vendor_No = txtVendorNo.Text.Trim();
        oUP_VendorBE.Site = new BusinessEntities.ModuleBE.AdminFunctions.MAS_SiteBE();
        //if (SiteID == 0)
        //    oUP_VendorBE.Site.SiteID = Convert.ToInt32(Session["SiteID"].ToString());
        //else
        //    oUP_VendorBE.Site.SiteID = SiteID;

        if (searchCountryID == 0)
        {
            oUP_VendorBE.Site.SiteCountryID = -1; //Convert.ToInt32(Session["SiteCountryID"].ToString());
            if (SiteID == 0)
                oUP_VendorBE.Site.SiteID = Convert.ToInt32(Session["SiteID"].ToString());
            else
                oUP_VendorBE.Site.SiteID = SiteID;
        }
        else
            oUP_VendorBE.Site.SiteCountryID = searchCountryID;


        return oUP_VendorBAL.ValidateVendorBAL(oUP_VendorBE);

    }


    protected void btnMoveRightAll_Click(object sender, EventArgs e)
    {
        if (lstVendor.Items.Count > 0)
            FillControls.MoveAllItems(lstVendor, lstSelectedVendor);
    }

    protected void btnMoveRight_Click(object sender, EventArgs e)
    {
        if (lstVendor.SelectedItem != null)
        {
            FillControls.MoveOneItem(lstVendor, lstSelectedVendor);
        }
    }

    protected void btnMoveLeft_Click(object sender, EventArgs e)
    {
        if (lstSelectedVendor.SelectedItem != null)
        {
            FillControls.MoveOneItem(lstSelectedVendor, lstVendor);
        }
    }

    protected void btnMoveLeftAll_Click(object sender, EventArgs e)
    {
        if (lstSelectedVendor.Items.Count > 0)
        {
            FillControls.MoveAllItems(lstSelectedVendor, lstVendor);
        }
    }

    protected void Up1_Init(object sender, EventArgs e)
    {
        if (ScriptManager.GetCurrent(Page) == null)
        {
            var sMgr = new ScriptManager();
            sMgr.EnablePartialRendering = true;
            phControls.Controls.Add(sMgr);
        }
    }
    protected void btnSearchByVendorNo_Click(object sender, EventArgs e)
    {
         var oUP_VendorBAL = new UP_VendorBAL();
        var oUP_VendorBE = new UP_VendorBE();
        oUP_VendorBE.Action = "GetAllLevelVendorDetailsByVendorNo";

        oUP_VendorBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
        oUP_VendorBE.User.UserID = Convert.ToInt32(Session["Userid"]);
        oUP_VendorBE.Vendor_No = txtVendorNo.Text;

        if (SearchCountryID != 0 && SearchCountryID != (-1))
            oUP_VendorBE.CountryID = SearchCountryID;

        var lstUPVendor = new List<UP_VendorBE>();
        lstUPVendor = oUP_VendorBAL.GetVendorByUserCountryBAL(oUP_VendorBE);
        if (lstUPVendor.Count > 0)
        {
            txtVendorNo.Text = oUP_VendorBE.Vendor_No;
            FillControls.FillListBox(ref lstVendor, lstUPVendor, "VendorName", "VendorID");
            for (int i = 0; i < lstUPVendor.Count; i++)
            {
                if (lstUPVendor[i].VendorID > 0)
                    if (Convert.ToInt32(lstVendor.Items[i].Value) == lstUPVendor[i].VendorID && lstUPVendor[i].IsActiveVendor == "N")
                        this.lstVendor.Items[i].Attributes.Add("Style", "background-color:RED;");
            }

            /* Logic to remove the right side existed vendor. */
            for (int i = 0; i < lstSelectedVendor.Items.Count; i++)
            {
                var currentVendor = lstSelectedVendor.Items[i];
                if (lstVendor.Items.Contains(currentVendor))
                    lstVendor.Items.Remove(currentVendor);
            }
        }
        else
        {
            lstVendor.Items.Clear();
        }
        
    }
}
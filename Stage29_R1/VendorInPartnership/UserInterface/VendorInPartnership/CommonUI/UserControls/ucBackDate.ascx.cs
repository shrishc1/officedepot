﻿using System;
using System.Web.UI;

public partial class ucBackDate : UserControl
{
    #region Public Properties

    public CommonPage CurrentPage {
        get;
        set;
    }

    public System.Web.UI.HtmlControls.HtmlInputText innerControltxtDate {
        get { return this.txtUCDate; }
    }

    public bool AutoPostBack {
        set;
        get;
    }

    public DateTime GetDate {
        get { return Utilities.Common.TextToDateFormat(txtUCDate.Value); }
    }
    #endregion

    protected void Page_Load(object sender, EventArgs e) {
        hdnAuto.Value = AutoPostBack.ToString();
    }

    protected void btnHidden_Click(object sender, EventArgs e) {
        if (CurrentPage.PreDate_Change()) {
        }
        CurrentPage.PostDate_Change();
    }
}


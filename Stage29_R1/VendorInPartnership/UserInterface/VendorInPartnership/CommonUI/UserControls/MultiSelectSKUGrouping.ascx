﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MultiSelectSKUGrouping.ascx.cs" Inherits="CommonUI_UserControls_MultiSelectSKUGrouping" %>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<script language="javascript" type="text/javascript">
    $(document).ready(function () {
        var lstitem = document.getElementById('<%= lstRight.ClientID %>');
        if (lstitem.options.length == 0) {
            document.getElementById('<%= hiddenSelectedIDs.ClientID %>').value = "";
            document.getElementById('<%= hiddenSelectedName.ClientID %>').value = "";
        }
    });

</script>

<table>
    <tr>
        <td>
            <cc1:ucListBox ID="lstLeft" runat="server" Height="100px" Width="320px">
            </cc1:ucListBox>
        </td>
         <td align="center" class="search-controls-btn">
            <div id="divMoveAllRight" runat="server" visible="false">
                <input type="button" id="btnMoveRightAll" value=">>" class="button" style="width: 35px"
                    onclick="Javascript:MoveAll('<%= lstLeft.ClientID %>', '<%= lstRight.ClientID %>','<%= hiddenSelectedIDs.ClientID %>','<%= hiddenSelectedName.ClientID %>');" />
            </div>
            &nbsp;
            <div>
                <input type="button" id="btnMoveRight" value=">" class="button" style="width: 35px"
                    onclick="Javascript:MoveItem('<%= lstLeft.ClientID %>', '<%= lstRight.ClientID %>','<%= hiddenSelectedIDs.ClientID %>','<%= hiddenSelectedName.ClientID %>');" />
            </div>
            &nbsp;
            <div>
                <input type="button" id="btnMoveLeft" value="<" class="button" style="width: 35px"
                    onclick="Javascript:MoveItem('<%= lstRight.ClientID %>', '<%= lstLeft.ClientID %>','<%= hiddenSelectedIDs.ClientID %>','<%= hiddenSelectedName.ClientID %>');" />
            </div>
            &nbsp;
            <div id="divMoveAllLeft" runat="server" visible="false">
                <input type="button" id="btnMoveLeftAll" value="<<" class="button" style="width: 35px"
                    onclick="Javascript:MoveAll('<%= lstRight.ClientID %>', '<%= lstLeft.ClientID %>','<%= hiddenSelectedIDs.ClientID %>','<%= hiddenSelectedName.ClientID %>');" />
            </div>
        </td>
        <td>
            <cc1:ucListBox ID="lstRight" runat="server" Height="100px" Width="320px">
            </cc1:ucListBox>
            <asp:HiddenField ID="hiddenSelectedIDs" runat="server" />
            <asp:HiddenField ID="hiddenSelectedName" runat="server" />
            <asp:HiddenField ID="hiddenSkuGroupingIDs" runat="server" />
            <asp:HiddenField ID="hdnIsMoveAllRequired" runat="server" />
        </td>
    </tr>
</table>

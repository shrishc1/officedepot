﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using BusinessLogicLayer.ModuleBAL.Appointment.SiteSettings;
using Utilities;
using WebUtilities;

public partial class ucVendorForCountryForMasterVendorOTIF : System.Web.UI.UserControl
{
    public int SiteID
    {
        get { return Convert.ToInt32(ViewState["SiteID"]); }
        set { ViewState["SiteID"] = value; }
    }

    public int CountryID
    {
        get { return Convert.ToInt32(ViewState["CountryID"]); }
        set { ViewState["CountryID"] = value; }
    }

    public bool IsChildRequired
    {
        get { return Convert.ToBoolean(ViewState["IsChildRequired"]); }
        set { ViewState["IsChildRequired"] = value; }
    }

    public bool IsParentRequired
    {
        get { return Convert.ToBoolean(ViewState["IsParentRequired"]); }
        set { ViewState["IsParentRequired"] = value; }
    }

    public bool IsStandAloneRequired
    {
        get { return Convert.ToBoolean(ViewState["IsStandAloneRequired"]); }
        set { ViewState["IsStandAloneRequired"] = value; }
    }

    public bool IsGrandParentRequired
    {
        get { return Convert.ToBoolean(ViewState["IsGrandParentRequired"]); }
        set { ViewState["IsGrandParentRequired"] = value; }
    }

    public bool IsBindVendorByDefault {
        get { return Convert.ToBoolean(ViewState["IsBindVendorByDefault"]); }
        set { ViewState["IsBindVendorByDefault"] = value; }
    }

    public int EuropeOrLocal {   //0= All 1= Europen 2=Local
        get { return Convert.ToInt32(ViewState["EuropeOrLocal"]); }
        set { ViewState["EuropeOrLocal"] = value; }
    }
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (IsBindVendorByDefault==false)
                BindVendor();
        }
        btnSearchVendor.Text = WebCommon.getGlobalResourceValue("SearchVendor");
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        BindVendor();
    }

    public void BindVendorByText() {
        if(!string.IsNullOrEmpty(txtVendorNo.Text.Trim()))
        BindVendor();
    }

    public void BindVendor()
    {
        UP_VendorBAL oUP_VendorBAL = new UP_VendorBAL();
        UP_VendorBE oUP_VendorBE = new UP_VendorBE();

        oUP_VendorBE.Action = "SearchVendor";
        oUP_VendorBE.VendorName = txtVendorNo.Text.Trim();
        oUP_VendorBE.Site = new BusinessEntities.ModuleBE.AdminFunctions.MAS_SiteBE();

        oUP_VendorBE.IsChildRequired = IsChildRequired != null ? IsChildRequired : (bool?)null;
        oUP_VendorBE.IsGrandParentRequired = IsGrandParentRequired != null ? IsGrandParentRequired : (bool?)null;
        oUP_VendorBE.IsParentRequired = IsParentRequired != null ? IsParentRequired : (bool?)null;
        oUP_VendorBE.IsStandAloneRequired = IsStandAloneRequired != null ? IsStandAloneRequired : (bool?)null;
        oUP_VendorBE.EuropeanorLocal = EuropeOrLocal != null ? EuropeOrLocal : 0;

        if (CountryID == 0)
        {
            oUP_VendorBE.Site.SiteCountryID = -1; //Convert.ToInt32(Session["SiteCountryID"].ToString());
            if (SiteID == 0)
                oUP_VendorBE.Site.SiteID = Convert.ToInt32(Session["SiteID"].ToString());
            else
                oUP_VendorBE.Site.SiteID = SiteID;
        }
        else
            oUP_VendorBE.Site.SiteCountryID = CountryID;


        List<UP_VendorBE> lstUPVendor = new List<UP_VendorBE>();
        lstUPVendor = oUP_VendorBAL.GetCarrierDetailsBAL(oUP_VendorBE);
        if (lstUPVendor.Count > 0)
        {
            FillControls.FillListBox(ref lstLeft, lstUPVendor.ToList(), "VendorName", "VendorID");
            /// --- Added for Showing Inative Vendors --- 27 Feb 2013
            for (int i = 0; i < lstUPVendor.Count; i++)
            {
                if (lstUPVendor[i].VendorID > 0)
                    if (Convert.ToInt32(lstLeft.Items[i].Value) == lstUPVendor[i].VendorID && lstUPVendor[i].IsActiveVendor == "N")
                        this.lstLeft.Items[i].Attributes.Add("Style", "background-color:RED;");
            }
            /// --- Added for Showing Inative Vendors --- 27 Feb 2013

        }
        else
        {
            lstLeft.Items.Clear();
        }
    }

    [WebMethod]
    public string GetGlobalConsolidateVendorIds(int vendorId)
    {
        string strVendorIds = string.Empty;
        UP_VendorBAL oUP_VendorBAL = new UP_VendorBAL();
        UP_VendorBE oUP_VendorBE = new UP_VendorBE();
        oUP_VendorBE.Action = "GetGlobalConsolidateVendorIds";
        oUP_VendorBE.VendorID = vendorId;
        strVendorIds = oUP_VendorBAL.GetGlobalConsolidateVendorIdsBAL(oUP_VendorBE);
        return strVendorIds;
    }
}
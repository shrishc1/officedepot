﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucExportExpediteTrackingReport.ascx.cs"
    Inherits="ucExportExpediteTrackingReport" %>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<div>
    <span>
        <cc1:ucButton ID="btnExportToExcel" runat="server" CssClass="exporttoexcel" OnClick="btnExport_Click"
            Width="109px" Height="20px" />
    </span><span>
        <cc1:ucGridView ID="gvTrackingReportExportExcel" Width="1500px" Height="80%" Visible="false"
            runat="server" CssClass="grid gvclass" GridLines="Both">
            <Columns>
                <asp:TemplateField HeaderText="Status" SortExpression="Status">
                    <HeaderStyle Width="100px" HorizontalAlign="Left" />
                    <ItemTemplate>
                        <%--<cc1:ucLinkButton runat="server" ID="lnkStatusValue" CommandName="Status" Text='<%#Eval("Status") %>'></cc1:ucLinkButton>--%>
                        <cc1:ucLabel runat="server" ID="lblStatusValue" Text='<%#Eval("Status") %>'></cc1:ucLabel>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Left" Width="100px" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="VendorNumber" SortExpression="Vendor.Vendor_No">
                    <HeaderStyle Width="100px" HorizontalAlign="Left" />
                    <ItemTemplate>
                        <cc1:ucLabel runat="server" ID="lblVendorNoValue" Text='<%#Eval("Vendor.Vendor_No") %>'></cc1:ucLabel>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Left" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="VendorName" SortExpression="Vendor.VendorName">
                    <HeaderStyle Width="200px" HorizontalAlign="Left" />
                    <ItemTemplate>
                        <cc1:ucLabel runat="server" ID="lblVendorNameValue" Text='<%#Eval("Vendor.VendorName") %>'></cc1:ucLabel>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Left" Width="200px" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Site" SortExpression="Site.SiteName">
                    <HeaderStyle Width="100px" HorizontalAlign="Left" />
                    <ItemTemplate>
                        <asp:HiddenField ID="hdnSiteId" Value='<%#Eval("Site.SiteID") %>' runat="server" />
                        <cc1:ucLabel runat="server" ID="lblSiteNameValue" Text='<%#Eval("Site.SiteName") %>'></cc1:ucLabel>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Left" Width="100px" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="OfficeDepotCode" SortExpression="PurchaseOrder.OD_Code">
                    <HeaderStyle Width="100px" HorizontalAlign="Left" />
                    <ItemTemplate>
                        <cc1:ucLabel runat="server" ID="lblOD_CodeValue" Text='<%#Eval("PurchaseOrder.OD_Code") %>'></cc1:ucLabel>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Left" Width="100px" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="VikingCode" SortExpression="PurchaseOrder.Direct_code">
                    <HeaderStyle Width="100px" HorizontalAlign="Left" />
                    <ItemTemplate>
                        <cc1:ucLabel runat="server" ID="lblVikingCodeValue" Text='<%#Eval("PurchaseOrder.Direct_code") %>'></cc1:ucLabel>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Left" Width="100px" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="POStatus" SortExpression="POStatus">
                    <HeaderStyle Width="100px" HorizontalAlign="Left" />
                    <ItemTemplate>
                        <%--<cc1:ucLinkButton runat="server" ID="lblPOStatusValue" CommandName="POStatus" Text='<%#Eval("POStatus") %>'></cc1:ucLinkButton>--%>
                        <asp:HyperLink ID="hlPOStatusValue" runat="server" Text='<%# Eval("POStatus") %>'
                            NavigateUrl='<%# EncryptQuery("OpenPurchaseListing.aspx?SiteID=" + Eval("Site.SiteID") + "&SKUID=" + Eval("SKUID") + "&VendorID=" + Eval("Vendor.VendorID")+ "&Status=" + Eval("Status"))%>'></asp:HyperLink>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Left" Width="100px" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Description1" SortExpression="PurchaseOrder.Product_description">
                    <HeaderStyle Width="300px" HorizontalAlign="Left" />
                    <ItemTemplate>
                        <cc1:ucLabel runat="server" ID="lblProductDesValue" Text='<%#Eval("PurchaseOrder.Product_description") %>'></cc1:ucLabel>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Left" Width="300px" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="StockPlannerNumber" SortExpression="PurchaseOrder.StockPlannerNo">
                    <HeaderStyle Width="100px" HorizontalAlign="Left" />
                    <ItemTemplate>
                        <cc1:ucLabel runat="server" ID="lblStockPlannerNoValue" Text='<%#Eval("PurchaseOrder.StockPlannerNo") %>'></cc1:ucLabel>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Left" Width="100px" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="StockPlannerName" SortExpression="PurchaseOrder.StockPlannerName">
                    <HeaderStyle Width="100px" HorizontalAlign="Left" />
                    <ItemTemplate>
                        <cc1:ucLabel runat="server" ID="lblStockPlannerNameValue" Text='<%#Eval("PurchaseOrder.StockPlannerName") %>'></cc1:ucLabel>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Left" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="VendorCode" SortExpression="PurchaseOrder.Vendor_Code">
                    <HeaderStyle Width="100px" HorizontalAlign="Left" />
                    <ItemTemplate>
                        <cc1:ucLabel runat="server" ID="lblVendorCodeValue" Text='<%#Eval("PurchaseOrder.Vendor_Code") %>'></cc1:ucLabel>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Left" Width="100px" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="ItemClassification" SortExpression="PurchaseOrder.Item_classification">
                    <HeaderStyle Width="100px" HorizontalAlign="Left" />
                    <ItemTemplate>
                        <cc1:ucLabel runat="server" ID="lblItemClassificationValue" Text='<%#Eval("PurchaseOrder.Item_classification") %>'></cc1:ucLabel>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Left" Width="100px" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="DateLastUpdated" SortExpression="CommentDateLastUpdated">
                    <HeaderStyle Width="100px" HorizontalAlign="Left" />
                    <ItemTemplate>
                        <cc1:ucLabel runat="server" ID="lblCommentDateLastUpdatedValue" Text='<%#Eval("CommentDateLastUpdated", "{0:dd/MM/yyyy}") %>'></cc1:ucLabel>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Left" Width="100px" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="NextUpdateDue" SortExpression="CommentUpdateValidTo">
                    <HeaderStyle Width="100px" HorizontalAlign="Left" />
                    <ItemTemplate>
                        <cc1:ucLabel runat="server" ID="lblCommentUpdateValidToValue" Text='<%#Eval("CommentUpdateValidTo", "{0:dd/MM/yyyy}") %>'></cc1:ucLabel>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Left" Width="100px" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="MostCurrentComment" SortExpression="MostCurrentComment">
                    <HeaderStyle Width="20%" HorizontalAlign="Left" />
                    <ItemTemplate>
                        <cc1:ucLabel runat="server" ID="lblMostCurrentCommentValue" Text='<%#Eval("MostCurrentComment") %>'></cc1:ucLabel>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Left" Width="20%" />
                </asp:TemplateField>
            </Columns>
        </cc1:ucGridView>
    </span>
</div>

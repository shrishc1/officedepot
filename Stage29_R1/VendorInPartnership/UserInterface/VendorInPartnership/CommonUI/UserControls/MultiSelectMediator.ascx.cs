﻿using BusinessEntities.ModuleBE.Security;
using BusinessLogicLayer.ModuleBAL.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class CommonUI_UserControls_MultiSelectMediator : System.Web.UI.UserControl
{
    //public bool isMoveAllRequired
    //{
    //    set
    //    {
    //        hdnIsMoveAllRequired.Value = value.ToString();
    //    }
    //}


    //private bool _isMoveAllRequired
    //{
    //    get
    //    {
    //        return !string.IsNullOrEmpty(hdnIsMoveAllRequired.Value) ? Convert.ToBoolean(hdnIsMoveAllRequired.Value) : true;
    //    }
    //}


    protected void Page_Load(object sender, EventArgs e)
    {
        //if (_isMoveAllRequired)
        //{
        //    divMoveAllLeft.Visible = divMoveAllRight.Visible = true;
        //}

        if (!IsPostBack)
        {
            //  SCT_UserBE oNewSCT_UserBE = new SCT_UserBE();
            //  SCT_UserBAL oSCT_UserBAL = new SCT_UserBAL();        

            //  oNewSCT_UserBE.Action = "ShowAllODMediators";

            ////  oNewSCT_UserBE.UserID = Convert.ToInt32(Session["UserID"]);           


            //  List<SCT_UserBE> lstODManger = new List<SCT_UserBE>();
            //  lstODManger = oSCT_UserBAL.GetODMediatorBAL(oNewSCT_UserBE);
            //  if (lstODManger.Count > 0)
            //  {
            //      hiddenMediatorUserIDs.Value = string.Join(",", lstODManger.Select(x => x.UserID).ToArray());

            //      FillControls.FillListBox(ref lstLeft, lstODManger, "FirstName", "UserID");
            //  }

            BindODManagers();
            PreSelectedODMediator();
        }
    }


    public void BindODManagers()
    {
        SCT_UserBE oNewSCT_UserBE = new SCT_UserBE();
        SCT_UserBAL oSCT_UserBAL = new SCT_UserBAL();

        oNewSCT_UserBE.Action = "ShowAllODMediators";

        //  oNewSCT_UserBE.UserID = Convert.ToInt32(Session["UserID"]);      

        if(txtMediator.Text.Trim() != null)
        {
            oNewSCT_UserBE.FirstName = txtMediator.Text.Trim();

        }

        List<SCT_UserBE> lstODManger = new List<SCT_UserBE>();
        lstODManger = oSCT_UserBAL.GetODMediatorBAL(oNewSCT_UserBE);
       
        if (lstODManger.Count > 0)
        {
            hiddenMediatorUserIDs.Value = string.Join(",", lstODManger.Select(x => x.UserID).ToArray());

            FillControls.FillListBox(ref lstLeft, lstODManger, "FirstName", "UserID");
        }
    }

    public void PreSelectedODMediator()
    {

        if (Session["Role"] != null && Convert.ToString(Session["Role"]) == "OD - Manager")
        {
            lstRight.Items.Add(new ListItem(Convert.ToString(Session["UserName"]), Convert.ToString(Session["UserID"])));
            hiddenSelectedIDs.Value = Convert.ToString(Session["UserID"]) + ",";
            hiddenSelectedName.Value = Convert.ToString(Session["UserName"]) + " ,";
        }
    }

    public void SearchMediatorClick(string MediatorName)
    {
        txtMediator.Text = MediatorName;
        BindODManagers();
    }

    public void setODManagerOnPostBack()
    {
        lstRight.Items.Clear();
        string[] strSelectedName = hiddenSelectedName.Value.Split(',');
        string[] strSelectedIDs = hiddenSelectedIDs.Value.Replace(",,", ",").Replace(",,", "").Split(',');
        for (int iCount = 0; iCount < strSelectedIDs.Length; iCount++)
        {
            if (!string.IsNullOrEmpty(strSelectedIDs[iCount]) && !string.IsNullOrWhiteSpace(strSelectedIDs[iCount]) && (strSelectedName.Length == strSelectedIDs.Length))
            {
                lstRight.Items.Add(new ListItem(strSelectedName[iCount].ToString().Trim(), strSelectedIDs[iCount].ToString().Trim()));
                lstLeft.Items.Remove(new ListItem(strSelectedName[iCount].ToString().Trim(), strSelectedIDs[iCount].ToString().Trim()));
            }
        }
    }


    protected void btnSearch_Click(object sender, EventArgs e)
    {
        this.BindODManagers();
    }

    public HiddenField innerControlHiddenSelectedID
    {
        get
        {
            return this.hiddenSelectedIDs;
        }
    }

    public HiddenField innerControlHiddenSelectedName
    {
        get
        {
            return this.hiddenSelectedName;
        }
    }

    public bool RequiredUserDefaultMediator = true;
    public string SelectedMediatorUserID
    {
        get
        {
            if (!string.IsNullOrEmpty(hiddenSelectedIDs.Value))
            {
                return hiddenSelectedIDs.Value.Trim(',');
            }
            else if (!string.IsNullOrEmpty(hiddenMediatorUserIDs.Value) && RequiredUserDefaultMediator == false)
            {
                return hiddenMediatorUserIDs.Value;
            }
            return null;
        }
        set
        {
            hiddenSelectedIDs.Value += value + ",";
        }
    }

    protected virtual void btnMoveRightAll_Click(object sender, EventArgs e)
    {
        FillControls.MoveAllItems(lstLeft, lstRight);
    }

    protected virtual void btnMoveRight_Click(object sender, EventArgs e)
    {
        if (lstLeft.SelectedItem != null)
        {
            FillControls.MoveOneItem(lstLeft, lstRight);
        }
    }

    protected virtual void btnMoveLeft_Click(object sender, EventArgs e)
    {
        if (lstRight.SelectedItem != null)
        {
            FillControls.MoveOneItem(lstRight, lstLeft);
        }
    }

    protected virtual void btnMoveLeftAll_Click(object sender, EventArgs e)
    {
        FillControls.MoveAllItems(lstRight, lstLeft);
    }

}
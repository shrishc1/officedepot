﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.Security;
using BusinessLogicLayer.ModuleBAL.Security;

public partial class CommonUI_UserControls_MultiSelectStockPlannerGrouping : System.Web.UI.UserControl
{
    public bool isMoveAllRequired {
        set {
            hdnIsMoveAllRequired.Value = value.ToString();
        }
    }


    private bool _isMoveAllRequired {
        get {
            return !string.IsNullOrEmpty(hdnIsMoveAllRequired.Value) ? Convert.ToBoolean(hdnIsMoveAllRequired.Value) : false ;
        }       
    }


    bool isHubRequired = true;
    public bool IsHubRequired {
        get {
            return isHubRequired;
        }
        set {
            isHubRequired = value;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (_isMoveAllRequired) {
            divMoveAllLeft.Visible = divMoveAllRight.Visible = true;
        }

        if (!IsPostBack)
        {
            StockPlannerGroupingsBE oStockPlannerGroupingsBE = new StockPlannerGroupingsBE();
            StockPlannerGroupingsBAL oStockPlannerGroupingsBAL = new StockPlannerGroupingsBAL();
            oStockPlannerGroupingsBE.Action = "ShowAll"; 
            List<StockPlannerGroupingsBE> lstStockPlannerGrouping = new List<StockPlannerGroupingsBE>();
            lstStockPlannerGrouping = oStockPlannerGroupingsBAL.GetStockPlannerGroupingsBAL(oStockPlannerGroupingsBE);
            if (lstStockPlannerGrouping.Count > 0)
            {
                hiddenStockPlannerGroupingIDs.Value = string.Join(",", lstStockPlannerGrouping.Select(x => x.StockPlannerGroupingsID).ToArray());

                FillControls.FillListBox(ref lstLeft, lstStockPlannerGrouping, "StockPlannerGroupings", "StockPlannerGroupingsID");
            }
        }
    }

    public void BindStockPlannerGrouping()
    {
        StockPlannerGroupingsBE oStockPlannerGroupingsBE = new StockPlannerGroupingsBE();
        StockPlannerGroupingsBAL oStockPlannerGroupingsBAL = new StockPlannerGroupingsBAL();

        oStockPlannerGroupingsBE.Action = "ShowAll";
        List<StockPlannerGroupingsBE> lstStockPlannerGrouping = new List<StockPlannerGroupingsBE>();
        lstStockPlannerGrouping = oStockPlannerGroupingsBAL.GetStockPlannerGroupingsBAL(oStockPlannerGroupingsBE);
        // lstStockPlannerGrouping.RemoveAll(x => x.StockPlannerGroupingID == Convert.ToInt32(strStockPlannerGroupingid));
        if (lstStockPlannerGrouping.Count > 0)
        {
            hiddenStockPlannerGroupingIDs.Value = string.Join(",", lstStockPlannerGrouping.Select(x => x.StockPlannerGroupingsID).ToArray());

            FillControls.FillListBox(ref lstLeft, lstStockPlannerGrouping, "StockPlannerGroupings", "StockPlannerGroupingsID");
        }
    }
    private string _selectedStockPlannerGroupingIDs = string.Empty;
   
    public bool RequiredUserDefaultStockPlannerGrouping = true;

    public string SelectedStockPlannerGroupingIDs {
        get {
            if (!string.IsNullOrEmpty(hiddenSelectedIDs.Value)) {
                return hiddenSelectedIDs.Value.Trim(',');               
            }
            else if (!string.IsNullOrEmpty(hiddenStockPlannerGroupingIDs.Value) && RequiredUserDefaultStockPlannerGrouping == false)
            {
                return hiddenStockPlannerGroupingIDs.Value;
            }            
            return null;
        }
        set {
            hiddenSelectedIDs.Value += value + ",";
        }
    }

    public string StockPlannerGroupingIds { get; set; }
    public string SelectedStockPlannerGroupingIDsForOB
    {
        get
        {
            if (!string.IsNullOrEmpty(hiddenSelectedIDs.Value))
            {
               
                return hiddenSelectedIDs.Value;
            }
            else if (!string.IsNullOrEmpty(hiddenStockPlannerGroupingIDs.Value) && RequiredUserDefaultStockPlannerGrouping == false)
            {
                return hiddenStockPlannerGroupingIDs.Value;
            }
            return null;
        }
        set
        {
            hiddenSelectedIDs.Value += value + ",";
        }
    }

    public string SelectedStockPlannerGroupingName {
        get {
            string _selectedStockPlannerGroupingName = string.Empty;

            if (!string.IsNullOrEmpty(hiddenSelectedName.Value)) {
                _selectedStockPlannerGroupingName = hiddenSelectedName.Value;
            }

            //Commented by Abhinav - it was not giving full name 
            //if (_selectedStockPlannerGroupingName.Length > 0)
            //    _selectedStockPlannerGroupingName = _selectedStockPlannerGroupingName.Substring(0, _selectedStockPlannerGroupingName.Length - 2);

            return _selectedStockPlannerGroupingName;
        }
        set
        {
            hiddenSelectedName.Value += value + ",";
        }
    }

    public HiddenField innerControlHiddenSelectedID
    {
        get
        {
            return this.hiddenSelectedIDs;
        }
    }

    public HiddenField innerControlHiddenSelectedName
    {
        get
        {
            return this.hiddenSelectedName;
        }
    }

    public void setStockPlannerGroupingsOnPostBack()
    {
        lstRight.Items.Clear();
        string[] strSelectedName = hiddenSelectedName.Value.Split(',');
        string[] strSelectedIDs = hiddenSelectedIDs.Value.Replace(",,", ",").Replace(",,", "").Split(',');
        for (int iCount = 0; iCount < strSelectedIDs.Length; iCount++)
        {
            if (!string.IsNullOrEmpty(strSelectedIDs[iCount]) && !string.IsNullOrWhiteSpace(strSelectedIDs[iCount]) && (strSelectedName.Length == strSelectedIDs.Length))
            {
                lstRight.Items.Add(new ListItem(strSelectedName[iCount].ToString().Trim(), strSelectedIDs[iCount].ToString().Trim()));
                lstLeft.Items.Remove(new ListItem(strSelectedName[iCount].ToString().Trim(), strSelectedIDs[iCount].ToString().Trim()));
            }
        }
    }

    protected virtual void btnMoveRightAll_Click(object sender, EventArgs e)
    {
        FillControls.MoveAllItems(lstLeft, lstRight);
    }

    protected virtual void btnMoveRight_Click(object sender, EventArgs e)
    {
        if (lstLeft.SelectedItem != null)
        {
            FillControls.MoveOneItem(lstLeft, lstRight);
        }
    }

    protected virtual void btnMoveLeft_Click(object sender, EventArgs e)
    {
        if (lstRight.SelectedItem != null)
        {
            FillControls.MoveOneItem(lstRight, lstLeft);
        }
    }

    protected virtual void btnMoveLeftAll_Click(object sender, EventArgs e)
    {
        FillControls.MoveAllItems(lstRight, lstLeft);
    }
}
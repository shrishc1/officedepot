﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucExportExpeditePotenOutput.ascx.cs"
    Inherits="ucExportExpeditePotenOutput" %>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<div>
    <span>
        <cc1:ucButton ID="btnExportToExcel" runat="server" CssClass="exporttoexcel" OnClick="btnExport_Click"
            Width="109px" Height="20px" />
    </span><span>
        <cc1:ucGridView ID="gvPotentialOutputExcelExp" OnRowDataBound="gvPotentialOutput_OnRowDataBound"
            runat="server" CssClass="grid gvclass searchgrid-1" GridLines="Both" Visible="false">
            <Columns>
                <asp:TemplateField HeaderText="C" SortExpression="Comment">
                    <HeaderStyle Width="10px" HorizontalAlign="Left" />
                    <ItemTemplate>
                        <asp:Literal runat="server" ID="lblUpdatedDateValue" Text='<%# Eval("UpdatedDate") %>' Visible="false"></asp:Literal>
                        <asp:Literal runat="server" ID="lblCommentColor" Visible="false" Text='<%#Eval("CommentColor") %>'></asp:Literal>
                        <asp:HyperLink ID="lnkCommentValue" runat="server" Text='<%# Eval("Comment") %>'
                            NavigateUrl='<%# EncryptQuery("ExpediteComments.aspx?SiteID=" + Eval("Site.SiteID") + "&SKUID=" + Eval("SKUID") + "&VendorID=" + Eval("Vendor.VendorID")+ "&Status=" + Eval("Status"))%>'></asp:HyperLink>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="S" SortExpression="Status">
                    <HeaderStyle Width="10px" HorizontalAlign="Left" />
                    <ItemTemplate>
                        <%--<cc1:ucLinkButton runat="server" ID="lnkStatusValue" CommandName="Status" Text='<%#Eval("Status") %>'></cc1:ucLinkButton>--%>
                        <cc1:ucLabel runat="server" ID="lblStatusValue" Font-Bold="true" Text='<%#Eval("Status") %>'></cc1:ucLabel>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Left" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="VendorNumber" SortExpression="Vendor.Vendor_No">
                    <HeaderStyle Width="30px" HorizontalAlign="Left" />
                    <ItemTemplate>
                        <cc1:ucLabel runat="server" ID="lblVendorNoValue" Text='<%#Eval("Vendor.Vendor_No") %>'></cc1:ucLabel>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Left" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Site" SortExpression="Site.SiteName">
                    <HeaderStyle Width="30px" HorizontalAlign="Left" />
                    <ItemTemplate>
                        <%--<cc1:ucLabel runat="server" ID="lblSiteId" Text='<%# Eval("Site.SiteID") %>' Visible="false"></cc1:ucLabel>--%>
                        <cc1:ucLabel runat="server" ID="lblSiteNameValue" Text='<%#Eval("Site.SiteName") %>'></cc1:ucLabel>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Left" Width="30px" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="VikingCode" SortExpression="PurchaseOrder.Direct_code">
                    <HeaderStyle Width="30px" HorizontalAlign="Left" />
                    <ItemTemplate>
                        <cc1:ucLabel runat="server" ID="lblVikingCodeValue" Text='<%#Eval("PurchaseOrder.Direct_code") %>'></cc1:ucLabel>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Left" Width="30px" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="PO" SortExpression="PurchaseOrder.Purchase_order">
                    <HeaderStyle Width="30px" HorizontalAlign="Left" />
                    <ItemTemplate>
                        <asp:HyperLink ID="lblPOValue" runat="server" Text='<%# Eval("POStatus") %>' NavigateUrl='<%# EncryptQuery("OpenPurchaseListing.aspx?SiteID=" + Eval("Site.SiteID") + "&SKUID=" + Eval("SKUID") + "&VendorID=" + Eval("Vendor.VendorID")+ "&Status=" + Eval("Status"))%>'></asp:HyperLink>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Left" Width="30px" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Description1" SortExpression="PurchaseOrder.Product_description">
                    <HeaderStyle Width="150px" HorizontalAlign="Left" />
                    <ItemTemplate>
                        <cc1:ucLabel runat="server" ID="lblProductDesValue" Text='<%#Eval("PurchaseOrder.Product_description") %>'></cc1:ucLabel>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Left" Width="150px" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="EarliestAdviseDate" SortExpression="EarliestAdviseDate">
                    <HeaderStyle Width="50px" HorizontalAlign="Left" />
                    <ItemTemplate>
                        <asp:HyperLink ID="lnkEarliestAdviseDateValue" runat="server" Text='<%#Eval("EarliestAdviseDate", "{0:dd/MM/yyyy}") %>'
                            NavigateUrl='<%# EncryptQuery("ExpediteBookingDate.aspx?SiteID=" + Eval("Site.SiteID") + "&SKUID=" + Eval("SKUID") + "&VendorID=" + Eval("Vendor.VendorID") +"&BookingDate=" + Eval("EarliestAdviseDate", "{0:MM/dd/yyyy}"))%>'></asp:HyperLink>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Left" Width="50px" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="VendorName" SortExpression="Vendor.VendorName">
                    <HeaderStyle Width="80px" HorizontalAlign="Left" />
                    <ItemTemplate>
                        <cc1:ucLabel runat="server" ID="lblVendorNameValue" Text='<%#Eval("Vendor.VendorName") %>'></cc1:ucLabel>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Left" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="OfficeDepotCode" SortExpression="PurchaseOrder.OD_Code">
                    <HeaderStyle Width="50px" HorizontalAlign="Left" />
                    <ItemTemplate>
                        <cc1:ucLabel runat="server" ID="lblOD_CodeValue" Text='<%#Eval("PurchaseOrder.OD_Code") %>'></cc1:ucLabel>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Left" Width="50px" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="StockPlannerNumber" SortExpression="PurchaseOrder.StockPlannerNo">
                    <HeaderStyle Width="30px" HorizontalAlign="Left" />
                    <ItemTemplate>
                        <cc1:ucLabel runat="server" ID="lblStockPlannerNoValue" Text='<%#Eval("PurchaseOrder.StockPlannerNo") %>'></cc1:ucLabel>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Left" Width="30px" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="StockPlannerName" SortExpression="PurchaseOrder.StockPlannerName">
                    <HeaderStyle Width="60px" HorizontalAlign="Left" />
                    <ItemTemplate>
                        <cc1:ucLabel runat="server" ID="lblStockPlannerNameValue" Text='<%#Eval("PurchaseOrder.StockPlannerName") %>'></cc1:ucLabel>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Left" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="VendorCode" SortExpression="PurchaseOrder.Vendor_Code">
                    <HeaderStyle Width="50px" HorizontalAlign="Left" />
                    <ItemTemplate>
                        <cc1:ucLabel runat="server" ID="lblVendorCodeValue" Text='<%#Eval("PurchaseOrder.Vendor_Code") %>'></cc1:ucLabel>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Left" Width="50px" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="ItemClass" SortExpression="PurchaseOrder.Item_classification">
                    <HeaderStyle Width="20px" HorizontalAlign="Left" />
                    <ItemTemplate>
                        <cc1:ucLabel runat="server" ID="lblItemClassificationValue" Text='<%#Eval("PurchaseOrder.Item_classification") %>'></cc1:ucLabel>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Left" Width="20px" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="MRP">
                    <HeaderStyle Width="25px" HorizontalAlign="Left" />
                    <ItemTemplate>
                        <cc1:ucLabel runat="server" ID="lblMRPValue" Text='<%#Eval("MRP") %>'></cc1:ucLabel>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Left" Width="25px" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="PurGrp">
                    <HeaderStyle Width="25px" HorizontalAlign="Left" />
                    <ItemTemplate>
                        <cc1:ucLabel runat="server" ID="lblPurGrpValue" Text='<%#Eval("PurGrp") %>'></cc1:ucLabel>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Left" Width="25px" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="MatGrp">
                    <HeaderStyle Width="25px" HorizontalAlign="Left" />
                    <ItemTemplate>
                        <cc1:ucLabel runat="server" ID="lblMatGrpValue" Text='<%#Eval("MatGrp") %>'></cc1:ucLabel>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Left" Width="25px" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="DateLastUpdated" SortExpression="CommentDateLastUpdated">
                    <HeaderStyle Width="50px" HorizontalAlign="Left" />
                    <ItemTemplate>
                        <cc1:ucLabel runat="server" ID="lblCommentDateLastUpdatedValue" Text='<%#Eval("CommentDateLastUpdated", "{0:dd/MM/yyyy}") %>'></cc1:ucLabel>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Left" Width="50px" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="UpdateValidTo" SortExpression="CommentUpdateValidTo">
                    <HeaderStyle Width="50px" HorizontalAlign="Left" />
                    <ItemTemplate>
                        <cc1:ucLabel runat="server" ID="lblCommentUpdateValidToValue" Text='<%#Eval("CommentUpdateValidTo", "{0:dd/MM/yyyy}") %>'></cc1:ucLabel>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Left" Width="50px" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="CBC" SortExpression="CB">
                    <HeaderStyle Width="25px" HorizontalAlign="Left" />
                    <ItemTemplate>
                        <asp:HyperLink ID="lnkCBValue" runat="server" Target="_blank" Text='<%# Eval("CB") %>'
                            NavigateUrl='<%# EncryptQuery("~/ModuleUI/StockOverview/BackOrder/BackOrderHistory.aspx?SKUID=" + Eval("SKUID"))%>'></asp:HyperLink>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Left" Width="25px" />
                </asp:TemplateField>
                <asp:BoundField HeaderText="CBQ" DataField="CBQ" />
                <asp:TemplateField HeaderText="FB" SortExpression="FB">
                    <HeaderStyle Width="25px" HorizontalAlign="Left" />
                    <ItemTemplate>
                        <cc1:ucLabel runat="server" ID="lblFBValue" Text='<%#Eval("FB") %>'></cc1:ucLabel>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Left" Width="25px" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="SOH" SortExpression="SOH">
                    <HeaderStyle Width="30px" HorizontalAlign="Left" />
                    <ItemTemplate>
                        <asp:HyperLink ID="lnkSOHValue" runat="server" Text='<%# Eval("SOH") %>' NavigateUrl='<%# EncryptQuery("StockonHandOverview.aspx?SiteID=" + Eval("Site.SiteID") + "&SKUID=" + Eval("SKUID") + "&VendorID=" + Eval("Vendor.VendorID") + "&ODSku=" + Eval("PurchaseOrder.OD_Code"))%>'></asp:HyperLink>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Left" Width="30px" />
                </asp:TemplateField>
                <%--Week Columns started from here...--%>
                <asp:TemplateField HeaderText="FirstWeekMon" SortExpression="FirstWeekMON">
                    <HeaderStyle Width="25px" HorizontalAlign="Left" />
                    <ItemTemplate>
                        <cc1:ucLabel runat="server" ID="lblFirstWeekMon" Text='<%# Eval("FirstWeekMON") %>'></cc1:ucLabel>
                        <asp:Literal runat="server" ID="lblFirstWeekMONColor" Text='<%# Eval("FirstWeekMONColor") %>'
                            Visible="false"></asp:Literal>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Left" Width="25px" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="FirstWeekTue" SortExpression="FirstWeekTUE">
                    <HeaderStyle Width="25px" HorizontalAlign="Left" />
                    <ItemTemplate>
                        <cc1:ucLabel runat="server" ID="lblFirstWeekTue" Text='<%# Eval("FirstWeekTUE") %>'></cc1:ucLabel>
                        <asp:Literal runat="server" ID="lblFirstWeekTUEColor" Text='<%# Eval("FirstWeekTUEColor") %>'
                            Visible="false"></asp:Literal>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Left" Width="25px" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="FirstWeekWed" SortExpression="FirstWeekWED">
                    <HeaderStyle Width="25px" HorizontalAlign="Left" />
                    <ItemTemplate>
                        <cc1:ucLabel runat="server" ID="lblFirstWeekWed" Text='<%# Eval("FirstWeekWED") %>'></cc1:ucLabel>
                        <asp:Literal runat="server" ID="lblFirstWeekWEDColor" Text='<%# Eval("FirstWeekWEDColor") %>'
                            Visible="false"></asp:Literal>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Left" Width="25px" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="FirstWeekThu" SortExpression="FirstWeekTHU">
                    <HeaderStyle Width="25px" HorizontalAlign="Left" />
                    <ItemTemplate>
                        <cc1:ucLabel runat="server" ID="lblFirstWeekThu" Text='<%# Eval("FirstWeekTHU") %>'></cc1:ucLabel>
                        <asp:Literal runat="server" ID="lblFirstWeekTHUColor" Text='<%# Eval("FirstWeekTHUColor") %>'
                            Visible="false"></asp:Literal>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Left" Width="25px" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="FirstWeekFri" SortExpression="Line_no">
                    <HeaderStyle Width="25px" HorizontalAlign="Left" />
                    <ItemTemplate>
                        <cc1:ucLabel runat="server" ID="lblFirstWeekFri" Text='<%# Eval("FirstWeekFri") %>'></cc1:ucLabel>
                        <asp:Literal runat="server" ID="lblFirstWeekFRIColor" Text='<%# Eval("FirstWeekFRIColor") %>'
                            Visible="false"></asp:Literal>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Left" Width="25px" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="SecondWeekMon" SortExpression="SecondWeekMON">
                    <HeaderStyle Width="25px" HorizontalAlign="Left" />
                    <ItemTemplate>
                        <cc1:ucLabel runat="server" ID="lblSecondWeekMon" Text='<%# Eval("SecondWeekMON") %>'></cc1:ucLabel>
                        <asp:Literal runat="server" ID="lblSecondWeekMONColor" Text='<%# Eval("SecondWeekMONColor") %>'
                            Visible="false"></asp:Literal>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Left" Width="25px" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="SecondWeekTue" SortExpression="SecondWeekTUE">
                    <HeaderStyle Width="25px" HorizontalAlign="Left" />
                    <ItemTemplate>
                        <cc1:ucLabel runat="server" ID="lblSecondWeekTue" Text='<%# Eval("SecondWeekTUE") %>'></cc1:ucLabel>
                        <asp:Literal runat="server" ID="lblSecondWeekTUEColor" Text='<%# Eval("SecondWeekTUEColor") %>'
                            Visible="false"></asp:Literal>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Left" Width="25px" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="SecondWeekWed" SortExpression="SecondWeekWED">
                    <HeaderStyle Width="25px" HorizontalAlign="Left" />
                    <ItemTemplate>
                        <cc1:ucLabel runat="server" ID="lblSecondWeekWed" Text='<%# Eval("SecondWeekWED") %>'></cc1:ucLabel>
                        <asp:Literal runat="server" ID="lblSecondWeekWEDColor" Text='<%# Eval("SecondWeekWEDColor") %>'
                            Visible="false"></asp:Literal>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Left" Width="25px" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="SecondWeekThu" SortExpression="SecondWeekTHU">
                    <HeaderStyle Width="25px" HorizontalAlign="Left" />
                    <ItemTemplate>
                        <cc1:ucLabel runat="server" ID="lblSecondWeekThu" Text='<%# Eval("SecondWeekTHU") %>'></cc1:ucLabel>
                        <asp:Literal runat="server" ID="lblSecondWeekTHUColor" Text='<%# Eval("SecondWeekTHUColor") %>'
                            Visible="false"></asp:Literal>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Left" Width="25px" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="SecondWeekFri" SortExpression="SecondWeekFRI">
                    <HeaderStyle Width="25px" HorizontalAlign="Left" />
                    <ItemTemplate>
                        <cc1:ucLabel runat="server" ID="lblSecondWeekFri" Text='<%# Eval("SecondWeekFRI") %>'></cc1:ucLabel>
                        <asp:Literal runat="server" ID="lblSecondWeekFRIColor" Text='<%# Eval("SecondWeekFRIColor") %>'
                            Visible="false"></asp:Literal>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Left" Width="25px" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="ThirdWeekMon" SortExpression="ThirdWeekMON">
                    <HeaderStyle Width="25px" HorizontalAlign="Left" />
                    <ItemTemplate>
                        <cc1:ucLabel runat="server" ID="lblThirdWeekMon" Text='<%# Eval("ThirdWeekMON") %>'></cc1:ucLabel>
                        <asp:Literal runat="server" ID="lblThirdWeekMONColor" Text='<%# Eval("ThirdWeekMONColor") %>'
                            Visible="false"></asp:Literal>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Left" Width="25px" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="ThirdWeekTue" SortExpression="ThirdWeekTUE">
                    <HeaderStyle Width="25px" HorizontalAlign="Left" />
                    <ItemTemplate>
                        <cc1:ucLabel runat="server" ID="lblThirdWeekTue" Text='<%# Eval("ThirdWeekTUE") %>'></cc1:ucLabel>
                        <asp:Literal runat="server" ID="lblThirdWeekTUEColor" Text='<%# Eval("ThirdWeekTUEColor") %>'
                            Visible="false"></asp:Literal>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Left" Width="25px" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="ThirdWeekWed" SortExpression="ThirdWeekWED">
                    <HeaderStyle Width="25px" HorizontalAlign="Left" />
                    <ItemTemplate>
                        <cc1:ucLabel runat="server" ID="lblThirdWeekWed" Text='<%# Eval("ThirdWeekWED") %>'></cc1:ucLabel>
                        <asp:Literal runat="server" ID="lblThirdWeekWEDColor" Text='<%# Eval("ThirdWeekWEDColor") %>'
                            Visible="false"></asp:Literal>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Left" Width="25px" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="ThirdWeekThu" SortExpression="ThirdWeekTHU">
                    <HeaderStyle Width="25px" HorizontalAlign="Left" />
                    <ItemTemplate>
                        <cc1:ucLabel runat="server" ID="lblThirdWeekThu" Text='<%# Eval("ThirdWeekTHU") %>'></cc1:ucLabel>
                        <asp:Literal runat="server" ID="lblThirdWeekTHUColor" Text='<%# Eval("ThirdWeekTHUColor") %>'
                            Visible="false"></asp:Literal>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Left" Width="25px" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="ThirdWeekFri" SortExpression="ThirdWeekFRI">
                    <HeaderStyle Width="25px" HorizontalAlign="Left" />
                    <ItemTemplate>
                        <cc1:ucLabel runat="server" ID="lblThirdWeekFri" Text='<%# Eval("ThirdWeekFRI") %>'></cc1:ucLabel>
                        <asp:Literal runat="server" ID="lblThirdWeekFRIColor" Text='<%# Eval("ThirdWeekFRIColor") %>'
                            Visible="false"></asp:Literal>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Left" Width="25px" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="FourthWeekMon" SortExpression="FourthWeekMON">
                    <HeaderStyle Width="25px" HorizontalAlign="Left" />
                    <ItemTemplate>
                        <cc1:ucLabel runat="server" ID="lblFourthWeekMon" Text='<%# Eval("FourthWeekMON") %>'></cc1:ucLabel>
                        <asp:Literal runat="server" ID="lblFourthWeekMONColor" Text='<%# Eval("FourthWeekMONColor") %>'
                            Visible="false"></asp:Literal>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Left" Width="25px" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="FourthWeekTue" SortExpression="FourthWeekTUE">
                    <HeaderStyle Width="25px" HorizontalAlign="Left" />
                    <ItemTemplate>
                        <cc1:ucLabel runat="server" ID="lblFourthWeekTue" Text='<%# Eval("FourthWeekTUE") %>'></cc1:ucLabel>
                        <asp:Literal runat="server" ID="lblFourthWeekTUEColor" Text='<%# Eval("FourthWeekTUEColor") %>'
                            Visible="false"></asp:Literal>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Left" Width="25px" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="FourthWeekWed" SortExpression="FourthWeekWED">
                    <HeaderStyle Width="25px" HorizontalAlign="Left" />
                    <ItemTemplate>
                        <cc1:ucLabel runat="server" ID="lblFourthWeekWed" Text='<%# Eval("FourthWeekWED") %>'></cc1:ucLabel>
                        <asp:Literal runat="server" ID="lblFourthWeekWEDColor" Text='<%# Eval("FourthWeekWEDColor") %>'
                            Visible="false"></asp:Literal>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Left" Width="25px" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="FourthWeekThu" SortExpression="FourthWeekTHU">
                    <HeaderStyle Width="25px" HorizontalAlign="Left" />
                    <ItemTemplate>
                        <cc1:ucLabel runat="server" ID="lblFourthWeekThu" Text='<%# Eval("FourthWeekTHU") %>'></cc1:ucLabel>
                        <asp:Literal runat="server" ID="lblFourthWeekTHUColor" Text='<%# Eval("FourthWeekTHUColor") %>'
                            Visible="false"></asp:Literal>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Left" Width="25px" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="FourthWeekFri" SortExpression="FourthWeekFRI">
                    <HeaderStyle Width="25px" HorizontalAlign="Left" />
                    <ItemTemplate>
                        <cc1:ucLabel runat="server" ID="lblFourthWeekFri" Text='<%# Eval("FourthWeekFRI") %>'></cc1:ucLabel>
                        <asp:Literal runat="server" ID="lblFourthWeekFRIColor" Text='<%# Eval("FourthWeekFRIColor") %>'
                            Visible="false"></asp:Literal>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Left" Width="25px" />
                </asp:TemplateField>
            </Columns>
        </cc1:ucGridView>
    </span>
</div>

﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucDate2.ascx.cs" Inherits="CommonUI_UserControls_ucDate2" %>
<script type="text/javascript">

  Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequest);
  function EndRequest(sender, args) {
      if (args.get_error() == undefined) {
          var d = new Date();
          var objDOB = document.getElementById('<%=txtUCDate2.ClientID %>');
          $(objDOB).datepicker({ showOtherMonths: true,
              selectOtherMonths: true, changeMonth: true, changeYear: true,
              minDate: new Date(2013, 0, 1),
              yearRange: '2013:+100',
              dateFormat: 'dd/mm/yy',
              firstDay: 1,
              numberOfMonths: 2,
              onSelect: function (dateText, inst) {
                  if (document.getElementById('<%=hdnAuto2.ClientID %>').value == "True") {
                      document.getElementById('<%=hdnPostBackDone2.ClientID%>').value = "false";
                      document.getElementById('<%=btnHidden2.ClientID %>').click();
                  }
              }
          });
      } 
  }
   $(function () {
            var d = new Date();
            var objDOB = document.getElementById('<%=txtUCDate2.ClientID %>');
            $(objDOB).datepicker({ showOtherMonths: true,
                selectOtherMonths: true, changeMonth: true, changeYear: true,
                minDate: new Date(2013, 0, 1),
                yearRange: '2013:+100',
                dateFormat: 'dd/mm/yy',
                firstDay: 1,
                numberOfMonths: 2,
                onSelect: function (dateText, inst) {
                    if (document.getElementById('<%=hdnAuto2.ClientID %>').value == "True") {
                        document.getElementById('<%=hdnPostBackDone2.ClientID%>').value = "false";
                        document.getElementById('<%=btnHidden2.ClientID %>').click();
                    }
                }
            });
        });
    //}

    function GetFoc() {
        //        if (document.getElementById('<%=hdnAuto2.ClientID %>').value == "True") {
        //            if (navigator.appName.indexOf("Microsoft Internet Explorer") == -1) {
        //                document.getElementById('<%=hdnPostBackDone2.ClientID%>').value = "false";
        //                document.getElementById('<%=btnPostForm2.ClientID %>').click();
        //            }
        //        }
    }

    function SetFoc() {
        if (document.getElementById('<%=hdnAuto2.ClientID %>').value == "True") {
            document.getElementById('<%=txtUCDate2.ClientID %>').focus();
        }
    }



</script>
<input type="text" id="txtUCDate2" runat="server" style="width: 67px;" readonly="readonly" onblur="GetFoc()" onclick="SetFoc()"/>
<asp:Button ID="btnHidden2" runat="server" OnClick="btnHidden_Click" style="display:none;" />
<asp:Button ID="btnPostForm2" runat="server" OnClick="btnPostForm_Click" style="display:none;" />
<asp:HiddenField ID="hdnAuto2" runat="server" />
<asp:HiddenField ID="hdnPostBackDone2" runat="server" />

﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.Upload;
using BusinessLogicLayer.ModuleBAL.Upload;
using Utilities; using WebUtilities;


public partial class MultiSelectDiscrepancyNumber : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    public string SelectedDiscrepancyNo {
        get {
            string _selectedDiscrepancyNo = null;

            if (!string.IsNullOrEmpty(hiddenSelectedId.Value)) {
                _selectedDiscrepancyNo = hiddenSelectedId.Value;
            }
            if (_selectedDiscrepancyNo != null)
                if (_selectedDiscrepancyNo.Length > 0)
                    _selectedDiscrepancyNo = _selectedDiscrepancyNo.Substring(0, _selectedDiscrepancyNo.Length - 1);

            return _selectedDiscrepancyNo;
        }
        set {
            hiddenSelectedId.Value = value + ",";
        }
    }
}
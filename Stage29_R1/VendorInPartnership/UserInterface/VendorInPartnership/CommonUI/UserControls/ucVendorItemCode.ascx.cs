﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class CommonUI_UserControls_ucVendorItemCode : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
    }

    public string SelectedVendorItemName
    {
        get
        {
            string _selectedVendorItemName = null;

            if (!string.IsNullOrEmpty(hiddenSelectedId.Value))
            {
                _selectedVendorItemName = hiddenSelectedId.Value;
            }
            if (_selectedVendorItemName != null)
                if (_selectedVendorItemName.Length > 0)
                    _selectedVendorItemName = _selectedVendorItemName.Substring(0, _selectedVendorItemName.Length - 1);

            return _selectedVendorItemName;
        }
        set
        {
            hiddenSelectedId.Value = value + ",";
        }
    }
    public void setVikingVendorItemOnPostBack()
    {
        string[] strSelectedName = hiddenSelectedName.Value.TrimEnd(',').TrimEnd(' ').Split(',');
        string[] strSelectedIDs = hiddenSelectedId.Value.TrimEnd(',').Split(',');

        for (int iCount = 0; iCount < strSelectedIDs.Length; iCount++)
        {
            if (!string.IsNullOrEmpty(strSelectedIDs[iCount]) && !string.IsNullOrWhiteSpace(strSelectedIDs[iCount]))
            {
                lstRight.Items.Add(new ListItem(strSelectedName[iCount].ToString().Trim(), strSelectedIDs[iCount].ToString().Trim()));

            }
        }
    }
}
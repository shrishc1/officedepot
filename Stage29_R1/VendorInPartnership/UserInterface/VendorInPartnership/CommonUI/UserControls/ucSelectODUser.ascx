﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucSelectODUser.ascx.cs"
    Inherits="ucSelectODUser" %>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<asp:UpdatePanel ID="UpdatePanelVendor" runat="server">
    <ContentTemplate>
        <script language="javascript" type="text/javascript">
            $(function () {
                getODDetails();
            });

            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_endRequest(function () {
                getODDetails();
            });

            function getODDetails() {
                $("[id$='ucLBLeft']").change(function () {
                    //alert('hi');
                    $("[id$='txtODUser']").val($("[id$='ucLBLeft'] option:selected").text());
                    $("[id$='hiddenSelectedUserID']").val($("[id$='ucLBLeft'] option:selected").val());
                    $("[id$='hiddenSelectedUserName']").val($("[id$='ucLBLeft'] option:selected").text());
                });
            }

        </script>
        <table>
            <tr>
                <td colspan="3" align="left">
                    <cc1:ucTextbox ID="txtUserName" runat="server" MaxLength="15" Width="170px"></cc1:ucTextbox>
                    &nbsp;
                    <cc1:ucButton CssClass="button" runat="server" ID="btnSearch" OnClick="btnSearchUser_Click" />
                    <asp:HiddenField ID="hdnListRightUser" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    <cc1:ucListBox ID="ucLBLeft" runat="server" Height="100px" Width="320px">
                    </cc1:ucListBox>
                    <asp:HiddenField ID="hiddenSelectedUserID" runat="server" />
                    <asp:HiddenField ID="hiddenSelectedUserName" runat="server" />
                </td>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                </td>
                <td valign="top">
                    <cc1:ucLabel ID="lblMediator" Text="Mediator" runat="server"></cc1:ucLabel><br />
                     <cc1:ucTextbox ID="txtODUser" Enabled="false" runat="server" MaxLength="15" Width="270px"></cc1:ucTextbox>
                </td>
            </tr>
        </table>
    </ContentTemplate>
</asp:UpdatePanel>


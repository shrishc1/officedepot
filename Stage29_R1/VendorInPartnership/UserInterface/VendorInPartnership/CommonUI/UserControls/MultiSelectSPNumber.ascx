﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MultiSelectSPNumber.ascx.cs" Inherits="CommonUI_UserControls_MultiSelectSPNumber" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>

 <script language="javascript" type="text/javascript">
            $(document).ready(function () {
                var lstitem = document.getElementById('<%= lstRight.ClientID %>');
                if (lstitem.options.length == 0) {
                    document.getElementById('<%= hiddenSelectedIDs.ClientID %>').value = "";
                    document.getElementById('<%= hiddenSelectedSPName.ClientID %>').value = "";
                }
            });

           <%-- function ReBindListValue() {
                var varLstRightUser = document.getElementById('<%=ucLBRight.ClientID%>');
                var varHdnListRight = $("input[id*=hdnListRightUser]");
                $("input[id*=hdnListRightUser]").val('');
                if (varLstRightUser != null) {
                    for (var i = 0; i < varLstRightUser.options.length; i++) {
                        if ($("input[id*=hdnListRightUser]").val() == '') {
                            $("input[id*=hdnListRightUser]").val(varLstRightUser.options[i].text + '#'
                        + varLstRightUser.options[i].value);
                        }
                        else {
                            $("input[id*=hdnListRightUser]").val($("input[id*=hdnListRightUser]").val() + '$' + varLstRightUser.options[i].text + '#'
                        + varLstRightUser.options[i].value);
                        }
                    }
                }                
            }--%>
        </script>


<table>
   <tr>                 
            <td align="left" valign="middle">
                <cc1:ucTextbox ID="txtSPNumber" runat="server" MaxLength="15" Width="90px"></cc1:ucTextbox>&nbsp;&nbsp;
                <cc1:ucButton  CssClass="button" runat="server" ID="btnSearchPlannerNo" OnClick="btnSearch_Click" />&nbsp;&nbsp;                               
            </td>
        </tr>
      
        <tr>         
          <td>
            <cc1:ucListBox ID="lstLeft" runat="server" Height="100px" Width="320px">
            </cc1:ucListBox>
        </td>
         <td align="center" class="search-controls-btn">
            <div id="divMoveAllRight" runat="server" visible="false">
                <input type="button" id="btnMoveRightAll" value=">>" class="button" style="width: 35px"
                    onclick="Javascript:MoveAll('<%= lstLeft.ClientID %>', '<%= lstRight.ClientID %>','<%= hiddenSelectedIDs.ClientID %>','<%= hiddenSelectedSPName.ClientID %>');" />
            </div>
            &nbsp;
            <div>
                <input type="button" id="btnMoveRight" value=">" class="button" style="width: 35px"
                    onclick="Javascript:MoveItem('<%= lstLeft.ClientID %>', '<%= lstRight.ClientID %>','<%= hiddenSelectedIDs.ClientID %>','<%= hiddenSelectedSPName.ClientID %>');" />
            </div>
            &nbsp;
            <div>
                <input type="button" id="btnMoveLeft" value="<" class="button" style="width: 35px"
                    onclick="Javascript:MoveItem('<%= lstRight.ClientID %>', '<%= lstLeft.ClientID %>','<%= hiddenSelectedIDs.ClientID %>','<%= hiddenSelectedSPName.ClientID %>');" />
            </div>
            &nbsp;
            <div id="divMoveAllLeft" runat="server" visible="false">
                <input type="button" id="btnMoveLeftAll" value="<<" class="button" style="width: 35px"
                    onclick="Javascript:MoveAll('<%= lstRight.ClientID %>', '<%= lstLeft.ClientID %>','<%= hiddenSelectedIDs.ClientID %>','<%= hiddenSelectedSPName.ClientID %>');" />
            </div>
        </td>
        <td>
            <cc1:ucListBox ID="lstRight" runat="server" Height="100px" Width="320px">
            </cc1:ucListBox>
            <asp:HiddenField ID="hiddenSelectedIDs" runat="server" />
            <asp:HiddenField ID="hiddenSelectedSPName" runat="server" />
            <asp:HiddenField ID="hiddenSPNumberUserIDs" runat="server" />
            <asp:HiddenField ID="hdnIsMoveAllRequired" runat="server" />
        </td>
    </tr>
</table>
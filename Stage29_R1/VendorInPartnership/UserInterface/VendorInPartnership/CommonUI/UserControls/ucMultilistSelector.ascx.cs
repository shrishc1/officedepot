﻿using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;

public partial class CommonUI_UserControls_ucMultilistSelector : System.Web.UI.UserControl
{
    // public DataTable ListData;

    private string _selectedItems = string.Empty;
    public string SelectedItems
    {
        get
        {
            if (lstRight.Items.Count > 0)
            {
                foreach (ListItem item in lstRight.Items)
                {
                    _selectedItems += "," + item.Value;
                }
                return _selectedItems;
            }
            return null;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
    }

    public void FillDataList<T>(List<T> ListData, string ValueField, string TextField)
    {
        FillControls.FillListBox(ref lstLeft, ListData, TextField, ValueField);
    }

    protected void btnMoveRightAll_Click(object sender, EventArgs e)
    {
        FillControls.MoveAllItems(lstLeft, lstRight);
    }

    protected void btnMoveRight_Click(object sender, EventArgs e)
    {
        if (lstLeft.SelectedItem != null)
        {
            FillControls.MoveOneItem(lstLeft, lstRight);
        }
    }

    protected void btnMoveLeft_Click(object sender, EventArgs e)
    {
        if (lstRight.SelectedItem != null)
        {
            //FillControls.MoveOneItem(lstRight, lstLeft);
            string CurrentItemText = lstRight.SelectedItem.Text;
            string CurrentItemValue = lstRight.SelectedItem.Value;
            int CussrentItemIndex = lstRight.SelectedIndex;

            lstLeft.Items.Add(new ListItem(CurrentItemText, CurrentItemValue));
            lstRight.Items.RemoveAt(CussrentItemIndex);
        }
    }

    protected void btnMoveLeftAll_Click(object sender, EventArgs e)
    {
        FillControls.MoveAllItems(lstRight, lstLeft);
    }
}
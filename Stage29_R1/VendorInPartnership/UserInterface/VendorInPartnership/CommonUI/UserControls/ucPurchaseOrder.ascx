﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucPurchaseOrder.ascx.cs"
    Inherits="ucPurchaseOrder" %>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="ucSite.ascx" TagName="ucSite" TagPrefix="cc2" %>
<asp:UpdatePanel ID="UpdatePanelPO" runat="server">
    <ContentTemplate>
        <table border="0">
            <tr>
                <td align="left" style="width: 54%">
                    <table border="0" class="top-settings">
                        <tr>
                            <td style="width: 10%">
                            </td>
                            <td style="font-weight: bold; width: 40%">
                                <cc1:ucLabel ID="lblSitePrefixName" runat="server" Text="Site"></cc1:ucLabel>
                            </td>
                            <td style="width: 5%">
                                :
                            </td>
                            <td style="width: 35%">
                                <cc2:ucSite ID="ucSite" runat="server" />
                            </td>
                            <td style="width: 10%">
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 10%">
                            </td>
                            <td style="font-weight: bold; width: 40%">
                                <cc1:ucLabel ID="lblPurchaseOrderNumber" runat="server" Text="Purchase Order Number"
                                    isRequired="true"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold; width: 5%">
                                :
                            </td>
                            <td style="font-weight: bold; width: 35%">
                                <cc1:ucTextbox ID="txtPurchaseOrderNumber" runat="server" Width="60px" MaxLength="11"
                                    AutoPostBack="true" OnTextChanged="txtPurchaseOrder_TextChanged"></cc1:ucTextbox>
                                &nbsp;
                                <asp:RequiredFieldValidator ID="rfvPurchaseOrderNumberValidation" runat="server"
                                    ControlToValidate="txtPurchaseOrderNumber" Display="None" ValidationGroup="Go">
                                </asp:RequiredFieldValidator>
                            </td>
                            <td style="width: 10%">
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 10%">
                            </td>
                            <td style="font-weight: bold;">
                                <cc1:ucLabel ID="lblPurchaseOrderDate" runat="server" Text="Purchase Order Date"></cc1:ucLabel>
                            </td>
                            <td>
                                :
                            </td>
                            <td>
                                <cc1:ucDropdownList ID="ddlPurchaseOrderDate" runat="server" Width="150px" AutoPostBack="True"
                                    OnSelectedIndexChanged="ddlPurchaseOrderDate_SelectedIndexChanged">
                                    <asp:ListItem Text="--Select--" Value="0"></asp:ListItem>
                                </cc1:ucDropdownList>
                            </td>
                            <td style="width: 10%">
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 10%">
                            </td>
                            <td style="font-weight: bold;">
                                <cc1:ucLabel ID="lblVendor" runat="server" Text="Vendor"></cc1:ucLabel>
                            </td>
                            <td>
                                :
                            </td>
                            <td>
                                <cc1:ucLabel ID="lblVendorValue" runat="server"></cc1:ucLabel>
                            </td>
                            <td style="width: 10%">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="5" align="center">
                                <cc1:ucLabel ID="lblInvalidPurchaseOrder" runat="server" Text="Invalid Purchase Order Number." Visible="false" Font-Bold="true" ForeColor="Red" ></cc1:ucLabel>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:HiddenField ID="hiddenSelectedSiteId" runat="server" />
                                <asp:HiddenField ID="hiddenSelectedSiteName" runat="server" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </ContentTemplate>
</asp:UpdatePanel>

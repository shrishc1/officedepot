﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class MultiSelectTypeofCommunication : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            this.BindCommunications();
        }
    }

    private void BindCommunications()
    {
        lstLeft.Items.Clear();
        ListItem listItem = new ListItem("Vendor Communication", "V");
        lstLeft.Items.Add(listItem);
        listItem = new ListItem("Carrier Communication", "C");
        lstLeft.Items.Add(listItem);
        listItem = new ListItem("OD Communication", "OD");
        lstLeft.Items.Add(listItem);
    }

    public string SelectedBindCommunicationValues
    {
        get
        {
            if (!string.IsNullOrEmpty(hiddenSelectedIDs.Value))
            {
                return hiddenSelectedIDs.Value.Trim(',');
            }
            return null;
        }
    }

    public string SelectedBindCommunicationText
    {
        get
        {
            string _selectedVendorRoles = string.Empty;

            if (!string.IsNullOrEmpty(hiddenSelectedName.Value))
            {
                _selectedVendorRoles = hiddenSelectedName.Value;
            }
            if (_selectedVendorRoles.Length > 0)
                _selectedVendorRoles = _selectedVendorRoles.Substring(0, _selectedVendorRoles.Length - 2);

            return _selectedVendorRoles;
        }
    }

    protected virtual void btnMoveRightAll_Click(object sender, EventArgs e)
    {
        FillControls.MoveAllItems(lstLeft, lstRight);
    }

    protected virtual void btnMoveRight_Click(object sender, EventArgs e)
    {
        if (lstLeft.SelectedItem != null)
        {
            FillControls.MoveOneItem(lstLeft, lstRight);
        }
    }

    protected virtual void btnMoveLeft_Click(object sender, EventArgs e)
    {
        if (lstRight.SelectedItem != null)
        {
            FillControls.MoveOneItem(lstRight, lstLeft);
        }
    }

    protected virtual void btnMoveLeftAll_Click(object sender, EventArgs e)
    {
        FillControls.MoveAllItems(lstRight, lstLeft);
    }

    public void SetVendorRoleOnPostBack()
    {
        lstRight.Items.Clear();
        //lstLeft.Items.Clear();
        string[] strSelectedName = hiddenSelectedName.Value.TrimEnd(',').Split(',');
        string[] strSelectedIDs = hiddenSelectedIDs.Value.TrimEnd(',').Split(',');

        if (strSelectedIDs.Length > 0)
            this.BindCommunications();

        for (int iCount = 0; iCount < strSelectedIDs.Length; iCount++)
        {
            if (!string.IsNullOrEmpty(strSelectedIDs[iCount]) && !string.IsNullOrWhiteSpace(strSelectedIDs[iCount]))
            {
                lstRight.Items.Add(new ListItem(strSelectedName[iCount].ToString().Trim(), strSelectedIDs[iCount].ToString().Trim()));
                RemoveVendorRole(strSelectedIDs[iCount].ToString().Trim());
            }
        }
    }

    private void RemoveVendorRole(string RoleId)
    {
        if (lstLeft.Items.FindByValue(RoleId.Trim()) != null)
            lstLeft.Items.RemoveAt(lstLeft.Items.IndexOf(lstLeft.Items.FindByValue(RoleId.Trim())));
    }
}
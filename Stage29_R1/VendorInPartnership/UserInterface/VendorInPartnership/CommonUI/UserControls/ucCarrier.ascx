﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucCarrier.ascx.cs" Inherits="ucCarrier" %>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<asp:UpdatePanel ID="UpdatePanelVendor" runat="server">
    <contenttemplate>
         <table border="0">
            <tr>
                <td align="left" valign="middle">
                    <cc1:ucTextbox ID="txtCarrier" runat="server" MaxLength="15" Width="170px"></cc1:ucTextbox>&nbsp;&nbsp;<cc1:ucButton CssClass="button" runat="server" Text="Search Carrier" ID="btnSearchCarrier" OnClick="btnSearchCarrier_Click" />
                </td>
            </tr>            
            <tr align="left">
                <td align="left" colspan="2">
                    <cc1:ucListBox ID="lstLeft" runat="server" Height="150px" Width="320px">
                    </cc1:ucListBox>
                </td>
            </tr>
        </table>
    </contenttemplate>
</asp:UpdatePanel>
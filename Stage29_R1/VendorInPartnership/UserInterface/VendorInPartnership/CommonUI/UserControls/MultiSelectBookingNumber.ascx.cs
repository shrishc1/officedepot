﻿using System;

public partial class MultiSelectBookingNumber : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    public string SelectedBookingNo {
        get {
            string _selectedBookingNo = null;

            if (!string.IsNullOrEmpty(hiddenSelectedId.Value)) {
                _selectedBookingNo = hiddenSelectedId.Value;
            }
            if (_selectedBookingNo != null)
                if (_selectedBookingNo.Length > 0)
                    _selectedBookingNo = _selectedBookingNo.Substring(0, _selectedBookingNo.Length - 1);

            return _selectedBookingNo;
        }
        set {
            hiddenSelectedId.Value = value + ",";
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using BusinessLogicLayer.ModuleBAL.Appointment.SiteSettings;

public partial class ucSeacrhnSelectVendorNew : System.Web.UI.UserControl
{
    #region Public Property
    
    public string VendorNo {
        get {
           // return hdnVendorID.Value;
            return "1";
        }
    }

    public int SiteID {
        get { return Convert.ToInt32(ViewState["SiteID"])  ; }
        set {
            ucVendorForCountry.SiteID = value;
            ViewState["SiteID"] = value; 
            }
    }

    public int CountryID {
        get { return Convert.ToInt32(ViewState["CountryID"]); }
        set {
            ucVendorForCountry.CountryID = value;
            ViewState["CountryID"] = value; 
        }
    }

    public bool IsChildRequired {
        get { return Convert.ToBoolean(ViewState["IsChildRequired"]); }
        set {
            ucVendorForCountry.IsChildRequired = value; 
            ViewState["IsChildRequired"] = value; 
        }
    }

    public bool IsParentRequired {
        get { return Convert.ToBoolean(ViewState["IsParentRequired"]); }
        set {
            ucVendorForCountry.IsParentRequired = value; 
            ViewState["IsParentRequired"] = value; 
        }
    }

    public bool IsStandAloneRequired {
        get { return Convert.ToBoolean(ViewState["IsStandAloneRequired"]); }
        set {
            ucVendorForCountry.IsStandAloneRequired = value; 
            ViewState["IsStandAloneRequired"] = value; 
        }
    }

    public bool IsGrandParentRequired {
        get { return Convert.ToBoolean(ViewState["IsGrandParentRequired"]); }
        set {
            ucVendorForCountry.IsGrandParentRequired = value; 
            ViewState["IsGrandParentRequired"] = value; 
        }
    }

    public bool IsCountryRequiredWithVendor
    {
        get { return Convert.ToBoolean(ViewState["IsCountryRequiredWithVendor"]); }
        set
        {
            ucVendorForCountry.IsCountryRequiredWithVendor = value;
            ViewState["IsCountryRequiredWithVendor"] = value;
        }
    }

    public void BindVendor() {
        ucVendorForCountry.BindVendor();
    }

    protected void Page_Load(object sender, EventArgs e) {

    }

    private string _selectedVendorIDs = string.Empty;
    public string SelectedVendorIDs {
        get {
            if (!string.IsNullOrEmpty(hiddenSelectedIDs.Value)) {
                return hiddenSelectedIDs.Value.Trim(',');
            }
            return null;
        }
        set {
            hiddenSelectedIDs.Value += value + ",";
        }
    }

    public string SelectedVendorName {
        get {
            string _selectedSPName = string.Empty;

            if (!string.IsNullOrEmpty(hiddenSelectedName.Value)) {
                _selectedSPName = hiddenSelectedName.Value;
            }

            if (_selectedSPName.Length > 0)
                _selectedSPName = _selectedSPName.Substring(0, _selectedSPName.Length - 2);

            return _selectedSPName;
        }
    }

    
    #endregion    
}
﻿using System;

public partial class MultiSelectDateRange : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        
    }

    public string SelectedDateRange {
        get {
            string _selectedDateRange = null;

            if (!string.IsNullOrEmpty(hiddenSelectedName.Value)) {
                _selectedDateRange = hiddenSelectedName.Value;
            }
            if (_selectedDateRange != null)
                if (_selectedDateRange.Length > 0)
                    _selectedDateRange = _selectedDateRange.Substring(0, _selectedDateRange.Length - 2);

            return _selectedDateRange;
        }
    }
}
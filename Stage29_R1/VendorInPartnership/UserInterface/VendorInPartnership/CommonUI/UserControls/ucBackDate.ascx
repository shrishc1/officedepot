﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucBackDate.ascx.cs" Inherits="ucBackDate" %>
<script type="text/javascript">
    function pageLoad() {
        $(function () {
            var d = new Date();
            var objDOB = document.getElementById('<%=txtUCDate.ClientID %>');
            $(objDOB).datepicker({ showOtherMonths: true,
                selectOtherMonths: true, changeMonth: true, changeYear: true,
                minDate: new Date(2013, 0, 1),
                maxDate: new Date(d.setDate(d.getDate() - 1)),
                yearRange: '2013:+100',
                dateFormat: 'dd/mm/yy',
                onSelect: function (selectedDate) {
                    if (document.getElementById('<%=hdnAuto.ClientID %>').value == "True")
                        document.getElementById('<%=btnHidden.ClientID %>').click();
                }
            });
        });
    } 
</script>
<input type="text" id="txtUCDate" runat="server" style="width: 67px;" readonly="readonly"/>
<asp:Button ID="btnHidden" runat="server" OnClick="btnHidden_Click" style="display:none;" />
<asp:HiddenField ID="hdnAuto" runat="server" />

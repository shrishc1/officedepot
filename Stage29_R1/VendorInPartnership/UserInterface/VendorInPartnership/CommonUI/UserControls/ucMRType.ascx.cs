﻿using System;
using BusinessEntities.ModuleBE.StockOverview;
using BusinessLogicLayer.ModuleBAL.StockOverview;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
public partial class ucMRType : System.Web.UI.UserControl
{
    public string SelectedMRPTypes
    {
        get
        {
            string _selectedName = string.Empty;

            if (!string.IsNullOrEmpty(hiddenSelectedName.Value))
            {
                _selectedName = hiddenSelectedName.Value;
            }

            if (_selectedName.Length > 0)
                _selectedName = _selectedName.Substring(0, _selectedName.Length - 2);

            return _selectedName;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            this.BindAllMRPType();
        }
    }

    public void BindAllMRPType()
    {
        var expediteStockBE = new ExpediteStockBE();
        expediteStockBE.Action = "GetAllMRP";
        var expediteStockBAL = new ExpediteStockBAL();
        var lstexpediteStock = expediteStockBAL.GetAllMrpPurcMatGroupBAL(expediteStockBE);
        if (lstexpediteStock != null && lstexpediteStock.Count > 0)
            FillControls.FillListBox(ref lstLeft, lstexpediteStock, "MRP", "MRP");
        else
            lstLeft.Items.Clear();   
    }
    public void setMRTypeOnPostBack()
    {
        lstLeft.Items.Clear();
        string[] strSelectedName = hiddenSelectedName.Value.TrimEnd(',').TrimEnd(' ').Split(',');
        string[] strSelectedIDs = hiddenSelectedIDs.Value.TrimEnd(',').Split(',');

        if (strSelectedIDs.Length > 0)
            this.BindAllMRPType();

        for (int iCount = 0; iCount < strSelectedIDs.Length; iCount++)
        {
            if (!string.IsNullOrEmpty(strSelectedIDs[iCount]) && !string.IsNullOrWhiteSpace(strSelectedIDs[iCount]))
            {
                lstRight.Items.Add(new ListItem(strSelectedName[iCount].ToString().Trim(), strSelectedIDs[iCount].ToString().Trim()));
                RemoveMRType(strSelectedIDs[iCount].ToString().Trim());
            }
        }
    }
    public void RemoveMRType(string SPID)
    {
        if (lstLeft.Items.FindByValue(SPID.Trim()) != null)
            lstLeft.Items.RemoveAt(lstLeft.Items.IndexOf(lstLeft.Items.FindByValue(SPID.Trim())));
    }
    protected virtual void btnMoveRightAll_Click(object sender, EventArgs e)
    {
        FillControls.MoveAllItems(lstLeft, lstRight);
    }

    protected virtual void btnMoveRight_Click(object sender, EventArgs e)
    {
        if (lstLeft.SelectedItem != null)
        {
            FillControls.MoveOneItem(lstLeft, lstRight);
        }
    }

    protected virtual void btnMoveLeft_Click(object sender, EventArgs e)
    {
        if (lstRight.SelectedItem != null)
        {
            FillControls.MoveOneItem(lstRight, lstLeft);
        }
    }

    protected virtual void btnMoveLeftAll_Click(object sender, EventArgs e)
    {
        FillControls.MoveAllItems(lstRight, lstLeft);
    }
}
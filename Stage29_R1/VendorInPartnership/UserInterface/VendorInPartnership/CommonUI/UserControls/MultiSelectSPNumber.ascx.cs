﻿using BusinessEntities.ModuleBE.Security;
using BusinessLogicLayer.ModuleBAL.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class CommonUI_UserControls_MultiSelectSPNumber : System.Web.UI.UserControl
{

    public bool isMoveAllRequired
    {
        set
        {
            hdnIsMoveAllRequired.Value = value.ToString();
        }
    }


    private bool _isMoveAllRequired
    {
        get
        {
            return !string.IsNullOrEmpty(hdnIsMoveAllRequired.Value) ? Convert.ToBoolean(hdnIsMoveAllRequired.Value) : false;
        }
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        if (_isMoveAllRequired)
        {
            divMoveAllLeft.Visible = divMoveAllRight.Visible = true;
        }



    }
    public void SearchStockPlannerByNumberClick(string StockPlonnerNo)
    {
        txtSPNumber.Text = StockPlonnerNo;
        BindSPByNumber();
    }

    public void BindSPByNumber()
    {
        SCT_UserBE oNewSCT_UserBE = new SCT_UserBE();
        SCT_UserBAL oSCT_UserBAL = new SCT_UserBAL();

        oNewSCT_UserBE.Action = "ShowAllSPByNumber";

        //  oNewSCT_UserBE.UserID = Convert.ToInt32(Session["UserID"]);      

        if (txtSPNumber.Text.Trim() != null)
        {
            oNewSCT_UserBE.StockPlannerNumber = txtSPNumber.Text.Trim();

        }

        List<SCT_UserBE> lstStockPlannerNo = new List<SCT_UserBE>();
        lstStockPlannerNo = oSCT_UserBAL.GetSPBySPNumberBAL(oNewSCT_UserBE);

        if (lstStockPlannerNo.Count > 0)
        {
            hiddenSPNumberUserIDs.Value = string.Join(",", lstStockPlannerNo.Select(x => x.UserID).ToArray());

            FillControls.FillListBox(ref lstLeft, lstStockPlannerNo, "FirstName", "UserID");
        }
        else
        {
            lstLeft.Items.Clear();
        }
    }


    public void setStockPlannerOnPostBack()
    {
        lstRight.Items.Clear();
        string[] strSelectedName = hiddenSelectedSPName.Value.Split(',');
        string[] strSelectedIDs = hiddenSelectedIDs.Value.Replace(",,", ",").Replace(",,", "").Split(',');
        for (int iCount = 0; iCount < strSelectedIDs.Length; iCount++)
        {
            if (!string.IsNullOrEmpty(strSelectedIDs[iCount]) && !string.IsNullOrWhiteSpace(strSelectedIDs[iCount]) && (strSelectedName.Length == strSelectedIDs.Length))
            {
                lstRight.Items.Add(new ListItem(strSelectedName[iCount].ToString().Trim(), strSelectedIDs[iCount].ToString().Trim()));
                lstLeft.Items.Remove(new ListItem(strSelectedName[iCount].ToString().Trim(), strSelectedIDs[iCount].ToString().Trim()));
            }
        }
    }


    protected void btnSearch_Click(object sender, EventArgs e)
    {
        this.BindSPByNumber();
    }

    public HiddenField innerControlHiddenSelectedID
    {
        get
        {
            return this.hiddenSelectedIDs;
        }
    }
      
    public string SelectedSPNumberUserIDs
    {
        get
        {
            if (!string.IsNullOrEmpty(hiddenSelectedIDs.Value))
            {
                return hiddenSelectedIDs.Value.Trim(',');
            }
            else if (!string.IsNullOrEmpty(hiddenSPNumberUserIDs.Value))
            {
                return hiddenSPNumberUserIDs.Value;
            }
            return null;
        }
        set
        {
            hiddenSelectedIDs.Value += value + ",";
        }
    }

    protected virtual void btnMoveRightAll_Click(object sender, EventArgs e)
    {
        FillControls.MoveAllItems(lstLeft, lstRight);
    }

    protected virtual void btnMoveRight_Click(object sender, EventArgs e)
    {
        if (lstLeft.SelectedItem != null)
        {
            FillControls.MoveOneItem(lstLeft, lstRight);
        }
    }

    protected virtual void btnMoveLeft_Click(object sender, EventArgs e)
    {
        if (lstRight.SelectedItem != null)
        {
            FillControls.MoveOneItem(lstRight, lstLeft);
        }
    }

    protected virtual void btnMoveLeftAll_Click(object sender, EventArgs e)
    {
        FillControls.MoveAllItems(lstRight, lstLeft);
    }



}
﻿using System;

using System.Web.UI;
using BusinessEntities.ModuleBE.Appointment.Booking;
using BusinessLogicLayer.ModuleBAL.Appointment.Booking;
using System.Data;


public partial class ucSchedulingDate : UserControl {

    #region Public Properties

    public CommonPage CurrentPage {
        get;
        set;
    }

    public System.Web.UI.HtmlControls.HtmlInputText innerControltxtDate {
        get { return this.txtUCDate; }
    }

    public bool AutoPostBack {
        set;
        get;
    }

    public DateTime GetDate {
        get { return Utilities.Common.TextToDateFormat(txtUCDate.Value); }

    }
    public void SetDate() {
        txtUCDate.Value = null;
    }

    public Boolean isPostBackDone {
        get { return hdnPostBackDone.Value != "" ? Convert.ToBoolean(hdnPostBackDone.Value) : false; }

    }

    public string DateClientId {
        get { return txtUCDate.ClientID; }
    }

    #endregion
    protected void Page_Init(object sender, EventArgs e) {
        if (Session["Role"] != null && (Session["Role"].ToString().ToLower() == "vendor" || Session["Role"].ToString().ToLower() == "carrier")) {
            hdndispablePast.Value = "true";
        }
        else {
            hdndispablePast.Value = "false";
        }
        //txtUCDate.Attributes.Add("class", "hasDatepicker");
        if (!Page.IsPostBack)
            ScriptManager.RegisterStartupScript(this, this.GetType(), "popup", "<script>LoadDate();</script>", false);
    }

    public void LoadDates() {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "popup", "<script>LoadDate();</script>", false);
    }

    protected void Page_Load(object sender, EventArgs e) {
        hdnAuto.Value = AutoPostBack.ToString();

       
    }

    protected void btnHidden_Click(object sender, EventArgs e) {
        if (CurrentPage.PreDate_Change()) {
        }
        CurrentPage.PostDate_Change();
        hdnPostBackDone.Value = "true";
    }

    protected void btnPostForm_Click(object sender, EventArgs e) {
        hdnPostBackDone.Value = "true";
    }

    
}


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class MultiSelectVendorRole : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            this.BindVendorRole();
        }
    }

    private void BindVendorRole()
    {
        lstLeft.Items.Clear();
        ListItem listItem = new ListItem("Scheduling", "Scheduling");
        lstLeft.Items.Add(listItem);
        listItem = new ListItem("Discrepancy", "Discrepancy");
        lstLeft.Items.Add(listItem);
        listItem = new ListItem("OTIF", "OTIF");
        lstLeft.Items.Add(listItem);
        listItem = new ListItem("Scorecard", "Scorecard");
        lstLeft.Items.Add(listItem);
        listItem = new ListItem("Inventory POC", "InventoryPOC");
        lstLeft.Items.Add(listItem);
        listItem = new ListItem("Accounts Payable POC", "AccountsPayablePOC");
        lstLeft.Items.Add(listItem);
        listItem = new ListItem("Procurement POC", "ProcurementPOC");
        lstLeft.Items.Add(listItem);
        listItem = new ListItem("Executive POC", "ExecutivePOC");
        lstLeft.Items.Add(listItem);    
    }
    
    public string SelectedVendorRoleIDs
    {
        get
        {
            if (!string.IsNullOrEmpty(hiddenSelectedIDs.Value))
            {
                return hiddenSelectedIDs.Value.Trim(',');
            }
            return null;
        }
    }

    public string SelectedVendorRoles
    {
        get
        {
            string _selectedVendorRoles = string.Empty;

            if (!string.IsNullOrEmpty(hiddenSelectedName.Value))
            {
                _selectedVendorRoles = hiddenSelectedName.Value;
            }
            if (_selectedVendorRoles.Length > 0)
                _selectedVendorRoles = _selectedVendorRoles.Substring(0, _selectedVendorRoles.Length - 2);

            return _selectedVendorRoles;
        }
    }

    protected virtual void btnMoveRightAll_Click(object sender, EventArgs e)
    {
        FillControls.MoveAllItems(lstLeft, lstRight);
    }

    protected virtual void btnMoveRight_Click(object sender, EventArgs e)
    {
        if (lstLeft.SelectedItem != null)
        {
            FillControls.MoveOneItem(lstLeft, lstRight);
        }
    }

    protected virtual void btnMoveLeft_Click(object sender, EventArgs e)
    {
        if (lstRight.SelectedItem != null)
        {
            FillControls.MoveOneItem(lstRight, lstLeft);
        }
    }

    protected virtual void btnMoveLeftAll_Click(object sender, EventArgs e)
    {
        FillControls.MoveAllItems(lstRight, lstLeft);
    }

    public void SetVendorRoleOnPostBack()
    {
        lstRight.Items.Clear();
        string[] strSelectedName = hiddenSelectedName.Value.TrimEnd(',').Split(',');
        string[] strSelectedIDs = hiddenSelectedIDs.Value.TrimEnd(',').Split(',');

        if (strSelectedIDs.Length > 0)
            this.BindVendorRole();

        for (int iCount = 0; iCount < strSelectedIDs.Length; iCount++)
        {
            if (!string.IsNullOrEmpty(strSelectedIDs[iCount]) && !string.IsNullOrWhiteSpace(strSelectedIDs[iCount]))
            {
                lstRight.Items.Add(new ListItem(strSelectedName[iCount].ToString().Trim(), strSelectedIDs[iCount].ToString().Trim()));
                RemoveVendorRole(strSelectedIDs[iCount].ToString().Trim());
            }
        }
    }

    private void RemoveVendorRole(string RoleId)
    {
        if (lstLeft.Items.FindByValue(RoleId.Trim()) != null)
            lstLeft.Items.RemoveAt(lstLeft.Items.IndexOf(lstLeft.Items.FindByValue(RoleId.Trim())));
    }
}
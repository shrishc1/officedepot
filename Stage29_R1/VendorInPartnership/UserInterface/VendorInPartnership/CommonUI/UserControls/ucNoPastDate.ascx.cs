﻿using System;

using System.Web.UI;


public partial class ucNoPastDate : UserControl {

        #region Public Properties

        public CommonPage CurrentPage {
            get;
            set;
        }

        public System.Web.UI.HtmlControls.HtmlInputText innerControltxtDate {
            get { return this.txtNoPastDate; }
        }

        public bool AutoPostBack {
            set;
            get;
        }

        public DateTime GetDate {
            get { return Utilities.Common.TextToDateFormat(txtNoPastDate.Value); }
           
        }
        public void SetDate() {
        txtNoPastDate.Value = null;
        }

        public Boolean isPostBackDone {
            get { return hdnPostBackDone.Value != "" ? Convert.ToBoolean(hdnPostBackDone.Value) : false; }

        }

        public string DateClientId
        {
            get { return txtNoPastDate.ClientID; }
        }
        
        #endregion

        protected void Page_Load(object sender, EventArgs e) {
            hdnAuto.Value = AutoPostBack.ToString();
        }

        protected void btnHidden_Click(object sender, EventArgs e) {
            if (CurrentPage.PreDate_Change()) {
            }
            CurrentPage.PostDate_Change();
            hdnPostBackDone.Value = "true";           
        }

        protected void btnPostForm_Click(object sender, EventArgs e) {           
            hdnPostBackDone.Value = "true";
        }
    }


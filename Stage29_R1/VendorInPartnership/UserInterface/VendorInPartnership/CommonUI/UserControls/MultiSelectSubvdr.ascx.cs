﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.Security;
using BusinessLogicLayer.ModuleBAL.Security;
using BusinessEntities.ModuleBE.StockOverview;
using BusinessLogicLayer.ModuleBAL.StockOverview;

public partial class CommonUI_UserControls_MultiSelectSubvdr : System.Web.UI.UserControl
{
    public bool IsMoveAllRequired
    {
        set
        {
            hdnIsMoveAllRequired.Value = value.ToString();
        }
    }
    private bool Get_isMoveAllRequired()
    {
        return !string.IsNullOrEmpty(hdnIsMoveAllRequired.Value) ? Convert.ToBoolean(hdnIsMoveAllRequired.Value) : false;
    }

    bool isHubRequired = true;
    public bool IsHubRequired
    {
        get
        {
            return isHubRequired;
        }
        set
        {
            isHubRequired = value;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Get_isMoveAllRequired())
        {
            divMoveAllLeft.Visible = divMoveAllRight.Visible = true;
        }

        if (!IsPostBack)
        {
            POSubVendorBe pOSubVendor = new POSubVendorBe();
            SubVendorBAL subVendorBAL = new SubVendorBAL();
            pOSubVendor.Action = "ShowAll";
            List<POSubVendorBe> lstSubVendor = new List<POSubVendorBe>();
            lstSubVendor = subVendorBAL.GetPOSubVendorBAL(pOSubVendor);
            if (lstSubVendor.Count > 0)
            {
                hiddenSubVendorIDs.Value = string.Join(",", lstSubVendor.Select(x => x.POSubVendorID).ToArray());
                FillControls.FillListBox(ref LstLeft, lstSubVendor, "Subvndr", "POSubVendorID");
            }
        }
    }

    public void BindSubVendor()
    {
        POSubVendorBe pOSubVendor = new POSubVendorBe();
        SubVendorBAL subVendorBAL = new SubVendorBAL();
        pOSubVendor.Action = "ShowAll";
        List<POSubVendorBe> lstSubVendor = new List<POSubVendorBe>();
        lstSubVendor = subVendorBAL.GetPOSubVendorBAL(pOSubVendor);
        if (lstSubVendor.Count > 0)
        {
            hiddenSubVendorIDs.Value = string.Join(",", lstSubVendor.Select(x => x.POSubVendorID).ToArray());
            FillControls.FillListBox(ref LstLeft, lstSubVendor, "Subvndr", "POSubVendorID");
        }
    }

    private string _selectedSubVendorIDs = string.Empty;

    private bool requiredUserDefaultSubVendor = true;
    public string SelectedSubVendorIDs
    {
        get
        {
            if (!string.IsNullOrEmpty(hiddenSelectedIDs.Value))
            {
                return hiddenSelectedIDs.Value.Trim(',');
            }
            else if (!string.IsNullOrEmpty(hiddenSubVendorIDs.Value) && RequiredUserDefaultSubVendor == false)
            {
                return hiddenSubVendorIDs.Value;
            }
            return null;
        }
        set
        {
            hiddenSelectedIDs.Value += value + ",";
        }
    }
    public string SubvendorIds { get; set; }
    public string SelectedSubVendorIDsForOB
    {
        get
        {
            if (!string.IsNullOrEmpty(hiddenSelectedIDs.Value))
            {

                return hiddenSelectedIDs.Value;
            }
            else if (!string.IsNullOrEmpty(hiddenSubVendorIDs.Value) && RequiredUserDefaultSubVendor == false)
            {
                return hiddenSubVendorIDs.Value;
            }
            return null;
        }
        set
        {
            hiddenSelectedIDs.Value += value + ",";
        }
    }
    public string SelectedSubVendorName
    {
        get
        {
            string _SelectedSubVendorName = string.Empty;
            if (!string.IsNullOrEmpty(hiddenSelectedName.Value))
            {
                _SelectedSubVendorName = hiddenSelectedName.Value;
            }
            return _SelectedSubVendorName;
        }
        set
        {
            hiddenSelectedName.Value += value + ",";
        }
    }
    public HiddenField InnerControlHiddenSelectedID
    {
        get
        {
            return this.hiddenSelectedIDs;
        }
    }
    public HiddenField InnerControlHiddenSelectedName
    {
        get
        {
            return this.hiddenSelectedName;
        }
    }
    public string SelectedSubVendorIDs1
    {
        get
        {
            return SelectedSubVendorIDs2;
        }

        set
        {
            SelectedSubVendorIDs2 = value;
        }
    }
    public string SelectedSubVendorIDs2
    {
        get
        {
            return _selectedSubVendorIDs;
        }

        set
        {
            _selectedSubVendorIDs = value;
        }
    }

    public bool RequiredUserDefaultSubVendor
    {
        get
        {
            return requiredUserDefaultSubVendor;
        }

        set
        {
            requiredUserDefaultSubVendor = value;
        }
    }

    public void SetSubVendorOnPostBack()
    {
        LstRight.Items.Clear();
        string[] strSelectedName = hiddenSelectedName.Value.Split(',');
        string[] strSelectedIDs = hiddenSelectedIDs.Value.Replace(",,", ",").Replace(",,", "").Split(',');
        for (int iCount = 0; iCount < strSelectedIDs.Length; iCount++)
        {
            if (!string.IsNullOrEmpty(strSelectedIDs[iCount])
                && !string.IsNullOrWhiteSpace(strSelectedIDs[iCount])
                && (strSelectedName.Length == strSelectedIDs.Length))
            {
                LstRight.Items.Add(new ListItem(strSelectedName[iCount].ToString().Trim(), strSelectedIDs[iCount].ToString().Trim()));
                LstLeft.Items.Remove(new ListItem(strSelectedName[iCount].ToString().Trim(), strSelectedIDs[iCount].ToString().Trim()));
            }
        }
    }
    protected virtual void BtnMoveRightAll_Click(object sender, EventArgs e)
    {
        FillControls.MoveAllItems(LstLeft, LstRight);
    }
    protected virtual void BtnMoveRight_Click(object sender, EventArgs e)
    {
        if (LstLeft.SelectedItem != null)
        {
            FillControls.MoveOneItem(LstLeft, LstRight);
        }
    }
    protected virtual void BtnMoveLeft_Click(object sender, EventArgs e)
    {
        if (LstRight.SelectedItem != null)
        {
            FillControls.MoveOneItem(LstRight, LstLeft);
        }
    }
    protected virtual void BtnMoveLeftAll_Click(object sender, EventArgs e)
    {
        FillControls.MoveAllItems(LstRight, LstLeft);
    }
}
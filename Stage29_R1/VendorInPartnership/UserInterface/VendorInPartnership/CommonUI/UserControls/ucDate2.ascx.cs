﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class CommonUI_UserControls_ucDate2 : UserControl
{

    #region Public Properties

    public CommonPage CurrentPage
    {
        get;
        set;
    }

    public System.Web.UI.HtmlControls.HtmlInputText innerControltxtDate
    {
        get { return this.txtUCDate2; }
    }

    public bool AutoPostBack
    {
        set;
        get;
    }

    public DateTime GetDate
    {
        get { return Utilities.Common.TextToDateFormat(txtUCDate2.Value); }

    }
    public void SetDate()
    {
        txtUCDate2.Value = null;
    }

    public Boolean isPostBackDone
    {
        get { return hdnPostBackDone2.Value != "" ? Convert.ToBoolean(hdnPostBackDone2.Value) : false; }

    }

    public string DateClientId
    {
        get { return txtUCDate2.ClientID; }
    }

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        hdnAuto2.Value = AutoPostBack.ToString();
    }

    protected void btnHidden_Click(object sender, EventArgs e)
    {
        if (CurrentPage.PreDate_Change())
        {
        }
        CurrentPage.PostDate_Change();
        hdnPostBackDone2.Value = "true";
    }

    protected void btnPostForm_Click(object sender, EventArgs e)
    {
        hdnPostBackDone2.Value = "true";
    }
}


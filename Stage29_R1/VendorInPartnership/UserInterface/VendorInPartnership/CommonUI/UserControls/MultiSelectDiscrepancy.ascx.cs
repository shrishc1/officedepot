﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.Upload;
using BusinessLogicLayer.ModuleBAL.Upload;
using Utilities;
using WebUtilities;


public partial class MultiSelectDiscrepancy : UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
    }

    public string SelectedDiscrepancy
    {
        get
        {
            string _selectedDiscrepancy = null;

            if (!string.IsNullOrEmpty(hiddenSelectedId.Value))
            {
                _selectedDiscrepancy = hiddenSelectedId.Value;
            }
            if (_selectedDiscrepancy != null)
                if (_selectedDiscrepancy.Length > 0)
                    _selectedDiscrepancy = _selectedDiscrepancy.Substring(0, _selectedDiscrepancy.Length - 1);

            return _selectedDiscrepancy;
        }
        set
        {
            hiddenSelectedId.Value = value + ",";
        }
    }



}
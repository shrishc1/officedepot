﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.Security;
using BusinessLogicLayer.ModuleBAL.Security;
using System.Data;

public partial class ucSelectStockplanner : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //if (!IsPostBack)
        //{
        //    BindStockPlanner();
        //}
    }

    private DataTable dtStockPlanner {
        get {
            return ViewState["dtStockPlanner"] != null ? (DataTable)ViewState["dtStockPlanner"] : null;
        }
        set {
            ViewState["dtStockPlanner"] = value;
        }
    }

    public void ClearAndBindStockPlanner( string StockPlannerID , string StockPlannerName = "", string SPFullname= "") {
        txtUserName.Text = StockPlannerName;
        BindStockPlanner(SPFullname);
        ucLBLeft.SelectedIndex = ucLBLeft.Items.IndexOf(ucLBLeft.Items.FindByText(SPFullname));
        //hiddenSelectedIDs1.Value = StockPlannerID;
        //hiddenSelectedName.Value = StockPlannerName;  
       
    }

    private void BindStockPlanner(string SPFullname = "") {
        ucLBLeft.Items.Clear();
        //SCT_UserBE oSCT_UserBE = new SCT_UserBE();
        //SCT_UserBAL oMAS_UserBAL = new SCT_UserBAL();

        //oSCT_UserBE.Action = "GetStockPlannersWithCountry";
        //oSCT_UserBE.UserID = Convert.ToInt32(Session["UserID"]);
        //if (!string.IsNullOrWhiteSpace(txtUserName.Text.Trim()))
        //    oSCT_UserBE.UserSearchText = txtUserName.Text.Trim();

        //dtStockPlanner = oMAS_UserBAL.GetStockPlannerWithCountryBAL(oSCT_UserBE);
        //if (dtStockPlanner.Rows.Count > 0) {
        //    FillControls.FillListBox(ref ucLBLeft, dtStockPlanner, "StockPlannerWithCountry", "StockPlannerID");
        //}
        SCT_UserBE oSCT_UserBE = new SCT_UserBE();
        SCT_UserBAL oMAS_UserBAL = new SCT_UserBAL();

        //oSCT_UserBE.Action = "GetStockPlannersWithCountry"; 
        oSCT_UserBE.Action = "GetStockPlannersWithOutCountry";
        oSCT_UserBE.UserID = Convert.ToInt32(Session["UserID"]);
        if (!string.IsNullOrWhiteSpace(txtUserName.Text.Trim()))
            oSCT_UserBE.UserSearchText = txtUserName.Text.Trim();

        dtStockPlanner = oMAS_UserBAL.GetStockPlannerWithCountryBAL(oSCT_UserBE);

        if (dtStockPlanner.Rows.Count > 0) {
            if (SPFullname != "") {
                hiddenSelectedIDs1.Value = dtStockPlanner.Select("StockPlannerWithCountry = '" + SPFullname + "'")[0].ItemArray[0].ToString();
                hiddenSelectedName.Value = dtStockPlanner.Select("StockPlannerWithCountry = '" + SPFullname + "'")[0].ItemArray[1].ToString();
            }
            else {
                hiddenSelectedIDs1.Value = dtStockPlanner.Rows[0]["StockPlannerID"].ToString();
                hiddenSelectedName.Value = dtStockPlanner.Rows[0]["StockPlannerWithCountry"].ToString();
            }
        }

        if (dtStockPlanner.Rows.Count > 0)
        {
            FillControls.FillListBox(ref ucLBLeft, dtStockPlanner, "StockPlannerWithCountry", "StockPlannerID");
        }
    }

    private string _selectedStockPlannerIDs = string.Empty;
   
    public string SelectedStockPlannerIDs
    {
        get
        {
            if (!string.IsNullOrEmpty(hiddenSelectedIDs1.Value))
            {
                //return hiddenSelectedIDs1.Value.Trim(',');
                SCT_UserBE oSCT_UserBE = new SCT_UserBE();
                SCT_UserBAL oMAS_UserBAL = new SCT_UserBAL();

                //oSCT_UserBE.Action = "GetStockPlannersWithCountry"; 
                oSCT_UserBE.Action = "GetStockPlannerIdsByUserIds";
                oSCT_UserBE.UserIds = hiddenSelectedIDs1.Value.Replace(",,", ",").Trim(',');

                DataTable dtStockPlanner = oMAS_UserBAL.GetStockPlannerWithCountryByUserIdsBAL(oSCT_UserBE);

                string result = String.Join(",", dtStockPlanner.AsEnumerable().Select(row => row.Field<int>("StockPlannerID")));
                return result;
            }
            return null;
        }
    }


    public string SelectedSPName
    {
        get
        {
            string _selectedSPName = string.Empty;

            if (!string.IsNullOrEmpty(hiddenSelectedName.Value))
            {

                _selectedSPName = hiddenSelectedName.Value.Split('-').Length >= 2 ? hiddenSelectedName.Value.Split('-')[1] : string.Empty;
            }           

            return _selectedSPName;
        }
    }


    public string SelectedSPPhone {
        get {
            string _selectedSPPhone = string.Empty;

            if (!string.IsNullOrEmpty(hiddenSelectedIDs1.Value)) {
                _selectedSPPhone = dtStockPlanner.Select("StockPlannerID = " + hiddenSelectedIDs1.Value.Trim())[0].ItemArray[2].ToString();
            }           

            return _selectedSPPhone;
        }
    }

    public string SelectedSPEmail {
        get {
            string _selectedSPEmail = string.Empty;

            if (!string.IsNullOrEmpty(hiddenSelectedIDs1.Value)) {
                _selectedSPEmail = dtStockPlanner.Select("StockPlannerID = " + hiddenSelectedIDs1.Value.Trim())[0].ItemArray[3].ToString();
            }

            return _selectedSPEmail;
        }
    }

    protected void btnSearchUser_Click(object sender, EventArgs e) {
         BindStockPlanner();
        
    }  

   
    public void RemoveSP(string SPID) {
        if (ucLBLeft.Items.FindByValue(SPID.Trim()) != null)
            ucLBLeft.Items.RemoveAt(ucLBLeft.Items.IndexOf(ucLBLeft.Items.FindByValue(SPID.Trim())));
    }

}
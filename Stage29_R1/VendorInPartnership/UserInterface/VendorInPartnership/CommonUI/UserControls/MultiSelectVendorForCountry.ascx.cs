﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class MultiSelectVendorForCountry : System.Web.UI.UserControl
{
    private int searchCountryID;
    public int SearchCountryID
    {
        get { return searchCountryID; }
        set
        {
            searchCountryID = value;
            ucVendor.SearchCountryID = value;
        }
    }

    private string _selectedVendorIDs = string.Empty;
    public string SelectedVendorIDs
    {
        get
        {
            if (!string.IsNullOrEmpty(hiddenSelectedIDs.Value))
            {
                return hiddenSelectedIDs.Value.Trim(',');
            }
            return null;
        }
        set
        {
            hiddenSelectedIDs.Value += value + ",";
        }
    }

    public HiddenField innerControlHiddenField
    {
        get
        {
            return this.hiddenSelectedIDs;
        }
    }

    public HiddenField innerControlHiddenField2
    {
        get
        {
            return this.hiddenSelectedName;
        }
    }

    public string FunctionCalled
    {

        set
        {

            ucVendor.FunctionCalled = value;
        }
    }



    public string SelectedVendorName
    {
        get
        {
            string _selectedSPName = string.Empty;

            if (!string.IsNullOrEmpty(hiddenSelectedName.Value))
            {
                _selectedSPName = hiddenSelectedName.Value;
            }

            //Commented by Abhinav - it was not giving full name in OTIF Detailed Reports
            //if (_selectedSPName.Length > 0)
            //_selectedSPName = _selectedSPName.Substring(0, _selectedSPName.Length - 2);

            return _selectedSPName;
        }
        set
        {
            hiddenSelectedName.Value += value + ",";
        }

    }
    protected void Page_Load(object sender, EventArgs e)
    {
        ucVendor.btnSearchClick += new EventHandler(ucVendor_btnSearchClick);
    }
    void ucVendor_btnSearchClick(object sender, EventArgs e)
    {
        lstRight.Items.Clear();
        hiddenSelectedIDs.Value = string.Empty;
        hiddenSelectedName.Value = string.Empty;
    }
    public void setVendorsOnPostBack()
    {
        lstRight.Items.Clear();
        string[] strSelectedName = hiddenSelectedName.Value.Split(',');
        string[] strSelectedIDs = hiddenSelectedIDs.Value.Split(',');
        for (int iCount = 0; iCount < strSelectedIDs.Length; iCount++)
        {
            if (!string.IsNullOrEmpty(strSelectedIDs[iCount]) && !string.IsNullOrWhiteSpace(strSelectedIDs[iCount]))
            {
                lstRight.Items.Add(new ListItem(strSelectedName[iCount].ToString().Trim(), strSelectedIDs[iCount].ToString().Trim()));
                ucVendor.RemoveVendor(strSelectedName[iCount].ToString().Trim(), strSelectedIDs[iCount].ToString().Trim());
            }
        }
    }

    public void SearchVendorClick(string vendorNo)
    {
        ucVendor.SearchVendorClick(vendorNo);
    }
 
}
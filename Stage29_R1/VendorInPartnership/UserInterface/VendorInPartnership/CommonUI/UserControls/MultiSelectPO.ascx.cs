﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.Upload;
using BusinessLogicLayer.ModuleBAL.Upload;
using Utilities;
using WebUtilities;

public partial class MultiSelectPO : UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        // txtPurchaseOrder.Attributes.Add("onkeydown", "if(event.which || event.keyCode){if ((event.which == 13) || (event.keyCode == 13)) {document.getElementById('" + btnMoveRight.UniqueID + "').click();return false;}} else {return true}; ");
    }

    public string SelectedPO
    {
        get
        {
            string _selectedPO = null;

            if (!string.IsNullOrEmpty(hiddenSelectedId.Value))
            {
                _selectedPO = hiddenSelectedId.Value;
            }
            if (_selectedPO != null)
                if (_selectedPO.Length > 0)
                    _selectedPO = _selectedPO.Substring(0, _selectedPO.Length - 1);

            return _selectedPO;
        }
        set
        {
            hiddenSelectedId.Value = value + ","; ;
        }
    }

    public void SetPOOnPostBack()
    {
        lstRight.Items.Clear();
        string[] strSelectedName = hiddenSelectedName.Value.TrimEnd(',').Split(',');
        for (int iCount = 0; iCount < strSelectedName.Length; iCount++)
        {
            if (!string.IsNullOrEmpty(strSelectedName[iCount]) && !string.IsNullOrWhiteSpace(strSelectedName[iCount]))
            {
                lstRight.Items.Add(new ListItem(strSelectedName[iCount].ToString().Trim(), strSelectedName[iCount].ToString().Trim()));
            }
        }
    }

}
﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucNoPastDate.ascx.cs" Inherits="ucNoPastDate" %>
<script type="text/javascript">  
 Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequest);
 function EndRequest(sender, args) {
     if (args.get_error() == undefined) {
         var d = new Date();
         var objDOB = document.getElementById('<%=txtNoPastDate.ClientID %>');
         $(objDOB).datepicker({ showOtherMonths: true,
             selectOtherMonths: true, changeMonth: true, changeYear: true,
             minDate: new Date(2013, 0, 1),
             yearRange: '2013:+100',
             dateFormat: 'dd/mm/yy',
             firstDay: 1,
             numberOfMonths: 2,
             beforeShowDay: noWeekendsOrHolidays,
             onSelect: function (dateText, inst) {
                 if (document.getElementById('<%=hdnAuto.ClientID %>').value == "True") {
                     document.getElementById('<%=hdnPostBackDone.ClientID%>').value = "false";
                     document.getElementById('<%=btnHidden.ClientID %>').click();
                 }
             }
         });
     }
 }
    function nationalDays(date) {
        var m = date.getMonth(), d = date.getDate(), y = date.getFullYear();
        var todaydate = new Date();
        if (new Date(y, m, d) < new Date(todaydate.getFullYear(), todaydate.getMonth(), todaydate.getDate())) {
            return [false, 'gray', 'past day'];
        }

        return[true, 'actday', 'Available'];
    }

    function noWeekendsOrHolidays(date) {
        var noWeekend = jQuery.datepicker.noWeekends(date);
       // return noWeekend[0] ? nationalDays(date) : noWeekend;
        return nationalDays(date);
    }


    function pageLoad() {
        $(function () {            
            var d = new Date();
            var objDOB = document.getElementById('<%=txtNoPastDate.ClientID %>');
            $(objDOB).datepicker({ showOtherMonths: true,
                selectOtherMonths: true, changeMonth: true, changeYear: true,
                minDate: new Date(2013, 0, 1),
                yearRange: '2013:+100',
                dateFormat: 'dd/mm/yy',
                firstDay: 1,
                numberOfMonths: 2,
                beforeShowDay: noWeekendsOrHolidays,
                onSelect: function (dateText, inst) {
                    if (document.getElementById('<%=hdnAuto.ClientID %>').value == "True") {
                        document.getElementById('<%=hdnPostBackDone.ClientID%>').value = "false";
                        document.getElementById('<%=btnHidden.ClientID %>').click();
                    }
                }
            });
        });
    }

    function GetFoc() {

    }

    function SetFoc() {
        if (document.getElementById('<%=hdnAuto.ClientID %>').value == "True") {           
            document.getElementById('<%=txtNoPastDate.ClientID %>').focus();
        }
    }



</script>
<input type="text" id="txtNoPastDate" runat="server" style="width: 67px;" readonly="readonly" onblur="GetFoc()" onclick="SetFoc()"/>
<asp:Button ID="btnHidden" runat="server" OnClick="btnHidden_Click" style="display:none;" />
<asp:Button ID="btnPostForm" runat="server" OnClick="btnPostForm_Click" style="display:none;" />
<asp:HiddenField ID="hdnAuto" runat="server" />
<asp:HiddenField ID="hdnPostBackDone" runat="server" />

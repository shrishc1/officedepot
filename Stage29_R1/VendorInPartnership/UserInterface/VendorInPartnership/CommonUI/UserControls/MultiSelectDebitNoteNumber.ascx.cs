﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class MultiSelectDebitNoteNumber : UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        // txtPurchaseOrder.Attributes.Add("onkeydown", "if(event.which || event.keyCode){if ((event.which == 13) || (event.keyCode == 13)) {document.getElementById('" + btnMoveRight.UniqueID + "').click();return false;}} else {return true}; ");
    }

    public string SelectedDebitNoteNumber
    {
        get
        {
            string _selectedDebitNoteNumber = null;

            if (!string.IsNullOrEmpty(hiddenSelectedId.Value))
            {
                _selectedDebitNoteNumber = hiddenSelectedId.Value;
            }
            if (_selectedDebitNoteNumber != null)
                if (_selectedDebitNoteNumber.Length > 0)
                    _selectedDebitNoteNumber = _selectedDebitNoteNumber.Substring(0, _selectedDebitNoteNumber.Length - 1);

            return _selectedDebitNoteNumber;
        }
        set
        {
            hiddenSelectedId.Value = value + ","; ;
        }
    }

    public void SetDebitNoteNumberOnPostBack()
    {
        lstRight.Items.Clear();
        string[] strSelectedName = hiddenSelectedName.Value.TrimEnd(',').Split(',');
        for (int iCount = 0; iCount < strSelectedName.Length; iCount++)
        {
            if (!string.IsNullOrEmpty(strSelectedName[iCount]) && !string.IsNullOrWhiteSpace(strSelectedName[iCount]))
            {
                lstRight.Items.Add(new ListItem(strSelectedName[iCount].ToString().Trim(), strSelectedName[iCount].ToString().Trim()));                
            }
        }
    }
}
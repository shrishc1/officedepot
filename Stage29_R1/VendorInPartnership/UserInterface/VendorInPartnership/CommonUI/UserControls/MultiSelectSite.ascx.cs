﻿using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.AdminFunctions;
using BusinessLogicLayer.ModuleBAL.AdminFunctions;
using System.Linq;

public partial class MultiSelectSite : System.Web.UI.UserControl
{
    public bool isMoveAllRequired {
        set {
            hdnIsMoveAllRequired.Value = value.ToString();
        }
    }


    private bool _isMoveAllRequired {
        get {
            return !string.IsNullOrEmpty(hdnIsMoveAllRequired.Value) ? Convert.ToBoolean(hdnIsMoveAllRequired.Value) : false ;
        }       
    }


    bool isHubRequired = true;
    public bool IsHubRequired {
        get {
            return isHubRequired;
        }
        set {
            isHubRequired = value;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (_isMoveAllRequired) {
            divMoveAllLeft.Visible = divMoveAllRight.Visible = true;
        }

        if (!IsPostBack)
        {
            MAS_SiteBE oMAS_SiteBE = new MAS_SiteBE();
            MAS_SiteBAL oMAS_SiteBAL = new MAS_SiteBAL();

            oMAS_SiteBE.Action = "ShowAll";

            oMAS_SiteBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
            oMAS_SiteBE.User.UserID = Convert.ToInt32(Session["UserID"]);
            oMAS_SiteBE.IsHubRequired = isHubRequired;

            //oMAS_SiteBE.SiteCountryID = Convert.ToInt32(CountryID.ToString());

            List<MAS_SiteBE> lstSite = new List<MAS_SiteBE>();
            lstSite = oMAS_SiteBAL.GetSiteBAL(oMAS_SiteBE);
            if (lstSite.Count > 0)
            {
                hiddenUserSiteIDs.Value = string.Join(",", lstSite.Select(x=>x.SiteID).ToArray());               

                FillControls.FillListBox(ref lstLeft, lstSite, "SiteCountryName", "SiteID");
            }
        }
    }

    public void BindSite()
    {
        MAS_SiteBE oMAS_SiteBE = new MAS_SiteBE();
        MAS_SiteBAL oMAS_SiteBAL = new MAS_SiteBAL();

        oMAS_SiteBE.Action = "ShowAll";

        oMAS_SiteBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
        oMAS_SiteBE.User.UserID = Convert.ToInt32(Session["UserID"]);
        oMAS_SiteBE.IsHubRequired = isHubRequired;
        string strSiteid = siteIds;
        //oMAS_SiteBE.SiteCountryID = Convert.ToInt32(CountryID.ToString());

        List<MAS_SiteBE> lstSite = new List<MAS_SiteBE>();
        lstSite = oMAS_SiteBAL.GetSiteBAL(oMAS_SiteBE);
       // lstSite.RemoveAll(x => x.SiteID == Convert.ToInt32(strSiteid));
        if (lstSite.Count > 0)
        {
            hiddenUserSiteIDs.Value = string.Join(",", lstSite.Select(x => x.SiteID).ToArray());

            FillControls.FillListBox(ref lstLeft, lstSite, "SiteCountryName", "SiteID");
        }
    }
    private string _selectedSiteIDs = string.Empty;
    //public string SelectedSiteIDs
    //{
    //    get
    //    {
    //        _selectedSiteIDs = string.Empty;
    //        if (lstRight.Items.Count > 0)
    //        {
    //            foreach (ListItem item in lstRight.Items)
    //            {
    //                _selectedSiteIDs += "," + item.Value;
    //            }
    //            return _selectedSiteIDs.Trim(',');
    //        }
    //        return null;
    //    }
    //}

    //public string SelectedSiteName
    //{
    //    get
    //    {
    //        string _selectedSiteName = string.Empty;

    //        foreach (ListItem item in lstRight.Items)
    //            _selectedSiteName += item.Text + ", ";

    //        if (_selectedSiteName.Length > 0)
    //            _selectedSiteName = _selectedSiteName.Substring(0, _selectedSiteName.Length - 2);

    //        return _selectedSiteName;
    //    }
    //}
    public bool RequiredUserDefaultSite = true;

    public string SelectedSiteIDs {
        get {
            if (!string.IsNullOrEmpty(hiddenSelectedIDs.Value)) {
                return hiddenSelectedIDs.Value.Trim(',');               
            }
            else if (!string.IsNullOrEmpty(hiddenUserSiteIDs.Value) && RequiredUserDefaultSite == false) {
                return hiddenUserSiteIDs.Value;
            }            
            return null;
        }
        set {
            hiddenSelectedIDs.Value += value + ",";
        }
    }

    public string siteIds { get; set; }
    public string SelectedSiteIDsForOB
    {
        get
        {
            if (!string.IsNullOrEmpty(hiddenSelectedIDs.Value))
            {
               
                return hiddenSelectedIDs.Value;
            }
            else if (!string.IsNullOrEmpty(hiddenUserSiteIDs.Value) && RequiredUserDefaultSite == false)
            {
                return hiddenUserSiteIDs.Value;
            }
            return null;
        }
        set
        {
            hiddenSelectedIDs.Value += value + ",";
        }
    }

    public string SelectedSiteName {
        get {
            string _selectedSiteName = string.Empty;

            if (!string.IsNullOrEmpty(hiddenSelectedName.Value)) {
                _selectedSiteName = hiddenSelectedName.Value;
            }

            //Commented by Abhinav - it was not giving full name 
            //if (_selectedSiteName.Length > 0)
            //    _selectedSiteName = _selectedSiteName.Substring(0, _selectedSiteName.Length - 2);

            return _selectedSiteName;
        }
        set
        {
            hiddenSelectedName.Value += value + ",";
        }
    }

    public HiddenField innerControlHiddenSelectedID
    {
        get
        {
            return this.hiddenSelectedIDs;
        }
    }

    public HiddenField innerControlHiddenSelectedName
    {
        get
        {
            return this.hiddenSelectedName;
        }
    }

    public void setSitesOnPostBack()
    {
        lstRight.Items.Clear();
        string[] strSelectedName = hiddenSelectedName.Value.Split(',');
        string[] strSelectedIDs = hiddenSelectedIDs.Value.Replace(",,", ",").Replace(",,", "").Split(',');
        for (int iCount = 0; iCount < strSelectedIDs.Length; iCount++)
        {
            if (!string.IsNullOrEmpty(strSelectedIDs[iCount]) && !string.IsNullOrWhiteSpace(strSelectedIDs[iCount]) && (strSelectedName.Length == strSelectedIDs.Length))
            {
                lstRight.Items.Add(new ListItem(strSelectedName[iCount].ToString().Trim(), strSelectedIDs[iCount].ToString().Trim()));
                lstLeft.Items.Remove(new ListItem(strSelectedName[iCount].ToString().Trim(), strSelectedIDs[iCount].ToString().Trim()));
            }
        }
    }

    protected virtual void btnMoveRightAll_Click(object sender, EventArgs e)
    {
        FillControls.MoveAllItems(lstLeft, lstRight);
    }

    protected virtual void btnMoveRight_Click(object sender, EventArgs e)
    {
        if (lstLeft.SelectedItem != null)
        {
            FillControls.MoveOneItem(lstLeft, lstRight);
        }
    }

    protected virtual void btnMoveLeft_Click(object sender, EventArgs e)
    {
        if (lstRight.SelectedItem != null)
        {
            FillControls.MoveOneItem(lstRight, lstLeft);
        }
    }

    protected virtual void btnMoveLeftAll_Click(object sender, EventArgs e)
    {
        FillControls.MoveAllItems(lstRight, lstLeft);
    }
}
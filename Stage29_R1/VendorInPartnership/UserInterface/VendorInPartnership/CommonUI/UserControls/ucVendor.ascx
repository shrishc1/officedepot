﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucVendor.ascx.cs" Inherits="ucVendor" %>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<asp:UpdatePanel ID="UpdatePanelVendor" runat="server">
    <ContentTemplate>
         <table border="0">
            <tr>
                <td align="left" valign="middle">
                    <cc1:ucTextbox ID="txtVendorNo" runat="server" MaxLength="15" Width="90px"></cc1:ucTextbox>&nbsp;<cc1:ucButton CssClass="button" runat="server" ID="btnSearchVendor" OnClick="btnSearch_Click" />&nbsp;
                    &nbsp;<cc1:ucButton CssClass="button" runat="server" ID="btnSearchVendorNumber"  OnClick="btnSearchByVendorNo_Click" />
                   
                </td>
            </tr>            
            <tr align="left">
                <td align="left">
                    <cc1:ucListBox ID="lstLeft" runat="server" Height="150px" Width="320px">
                    </cc1:ucListBox>
                </td>
            </tr>
        </table>
    </ContentTemplate>
</asp:UpdatePanel>

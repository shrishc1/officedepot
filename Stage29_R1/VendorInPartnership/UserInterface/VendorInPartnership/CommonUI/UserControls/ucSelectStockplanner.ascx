﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucSelectStockplanner.ascx.cs"
    Inherits="ucSelectStockplanner" %>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<asp:UpdatePanel ID="UpdatePanelVendor" runat="server">
    <ContentTemplate>
        <script language="javascript" type="text/javascript">
            $(function () {
                getSPDetails();
            });

            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_endRequest(function () {
                getSPDetails();
            });

            function getSPDetails() {
                $("[id$='ucLBLeft']").change(function () {
                    //alert('hi');
                    $("[id$='txtStockPlanner']").val($("[id$='ucLBLeft'] option:selected").text());
                    $("[id$='hiddenSelectedIDs1']").val($("[id$='ucLBLeft'] option:selected").val());
                    $("[id$='hiddenSelectedName']").val($("[id$='ucLBLeft'] option:selected").text());
                });
            }

        </script>
        <table>
            <tr>
                <td colspan="3" align="left">
                    <cc1:ucTextbox ID="txtUserName" runat="server" MaxLength="15" Width="170px"></cc1:ucTextbox>
                    &nbsp;
                    <cc1:ucButton CssClass="button" runat="server" ID="btnSearchStockPlanner" OnClick="btnSearchUser_Click" />
                    <asp:HiddenField ID="hdnListRightUser" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    <cc1:ucListBox ID="ucLBLeft" runat="server" Height="100px" Width="320px">
                    </cc1:ucListBox>
                    <asp:HiddenField ID="hiddenSelectedIDs1" runat="server" />
                    <asp:HiddenField ID="hiddenSelectedName" runat="server" />
                </td>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                </td>
                <td valign="top">
                    <cc1:ucLabel ID="lblStockPlanner" runat="server"></cc1:ucLabel><br />
                     <cc1:ucTextbox ID="txtStockPlanner" Enabled="false" runat="server" MaxLength="15" Width="270px"></cc1:ucTextbox>
                </td>
            </tr>
        </table>
    </ContentTemplate>
</asp:UpdatePanel>

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLogicLayer.ModuleBAL.Security;
using BusinessEntities.ModuleBE.Security;
using BaseControlLibrary;

public partial class ucVendorSelectionTemplate : UserControl
{
    public CommonPage CurrentPage
    {
        get;
        set;
    }

    public ucUserControlBase CurrentUserControl
    {
        get;
        set;
    }
    public ucDropdownList innerControlddlTemplate
    {
        get
        {
            return this.drpTemplateName;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
            BindTemplateName();
    }



    private void BindTemplateName()
    {
        SCT_TemplateBAL oSCT_TemplateBAL = new SCT_TemplateBAL();
        SCT_TemplateBE oSCT_TemplateBE = new SCT_TemplateBE();
        oSCT_TemplateBE.Action = "ShowVendorTemplate";

        List<SCT_TemplateBE> lstTemplate = oSCT_TemplateBAL.GetVendorTemplate(oSCT_TemplateBE);
        oSCT_TemplateBAL = null;
        if (lstTemplate.Count > 0)
        {
            FillControls.FillDropDown(ref drpTemplateName, lstTemplate, "TemplateName", "VendorTemplateID", "Select");
        }
    }



    protected void drpTemplateName_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (CurrentPage != null)
            CurrentPage.TemplateSelectedIndexChanged();
    }

   
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BaseControlLibrary;
using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using BusinessLogicLayer.ModuleBAL.Appointment.SiteSettings;
using Utilities;
using WebUtilities;


public partial class ucSeacrhVendor : System.Web.UI.UserControl
{
    #region Public Property

    public CommonPage CurrentPage
    {
        get;
        set;
    }

    public string VendorNo
    {
        get
        {
            return hdnVendorID.Value;
        }
        set { hdnVendorID.Value = value; }

    }

    public ucTextbox innerControlVendorNo
    {
        get
        {
            return this.txtVendorNo;
        }
         set
        {
            this.txtVendorNo = value;
        }
    }

    public ucLabel innerControlVendorName
    {
        get
        {
            return this.SelectedVendorName;
        }
        set
        {
            this.SelectedVendorName = value;
        }
    }


    public int SiteID
    {
        get { return Convert.ToInt32(ViewState["SiteID"]); }
        set { ViewState["SiteID"] = value; }
    }



    public int CountryID
    {
        get { return Convert.ToInt32(ViewState["CountryID"]); }
        set { ViewState["CountryID"] = value; }
    }

    public bool IsChildRequired
    {
        get { return Convert.ToBoolean(ViewState["IsChildRequired"]); }
        set { ViewState["IsChildRequired"] = value; }
    }

    public bool IsParentRequired
    {
        get { return Convert.ToBoolean(ViewState["IsParentRequired"]); }
        set { ViewState["IsParentRequired"] = value; }
    }

    public bool IsStandAloneRequired
    {
        get { return Convert.ToBoolean(ViewState["IsStandAloneRequired"]); }
        set { ViewState["IsStandAloneRequired"] = value; }
    }

    public bool IsGrandParentRequired
    {
        get { return Convert.ToBoolean(ViewState["IsGrandParentRequired"]); }
        set { ViewState["IsGrandParentRequired"] = value; }
    }

    public string FunctionCalled {
        get { return Convert.ToString(ViewState["FunctionCalled"]); }
        set { ViewState["FunctionCalled"] = value; }
    }

    // bind vendor name on page load
    public string IsLoadBindVendor
    {
        get { return Convert.ToString(ViewState["IsLoadBindVendor"]); }
        set { ViewState["IsLoadBindVendor"] = value; }
    }

    // set textbox value on page load
    public string txtVendorNoVal
    {
        set { ViewState["txtVendorNoVal"] = value; }
    }

    private string _VendorCode;
    public string VendorCode
    {
        set { _VendorCode = value; }
    }
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        lstVendor.Attributes.Add("onDblClick", "lstVendor_DoubleClick();");   

        if (!Page.IsPostBack)
        {

            txtVenderName2.Text = string.Empty;
            txtVendorNo.Text = string.Empty;
            txtVendorNo.Text = _VendorCode;
            lstVendor.Items.Clear();
            if (ViewState["IsLoadBindVendor"] != null)
            {
                txtVendorNo_TextChanged(sender, e);
                ViewState["IsLoadBindVendor"] = null;
            }
        }
        
    }

    public void ClearSearch()
    {
        lstVendor.Items.Clear();
        txtVenderName2.Text = string.Empty;
        hdnVendorID.Value = string.Empty;
        txtVendorNo.Text = string.Empty;
        SelectedVendorName.Text = string.Empty;
        hdnVendorName.Value = string.Empty;
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        List<UP_VendorBE> lstUPVendor;
        if (FunctionCalled != null && FunctionCalled == "GetAllVendor") {
            lstUPVendor = GetAllVendor(false);
        }
        else {
            lstUPVendor = GetVendor(false);
        }
        if (lstUPVendor.Count == 0)
        {
            string errorMeesage = WebCommon.getGlobalResourceValue("ValidVendorCode");
            ScriptManager.RegisterStartupScript(this.txtVendorNo, this.txtVendorNo.GetType(), "alert1", "alert('" + errorMeesage + "')", true);
        }
        FillControls.FillListBox(ref lstVendor, lstUPVendor.ToList(), "VendorName", "VendorID");

        /// --- Added for Showing Inative Vendors --- 27 Feb 2013
        //foreach (ListItem item in lstVendor.Items)
        //{
        //    for (int i = 0; i < lstUPVendor.Count; i++)
        //    {
        //        if (item.Value == lstUPVendor[i].VendorID.ToString() && lstUPVendor[i].IsActiveVendor == "N")
        //        {
        //            this.lstVendor.Items[i].Attributes.Add("Style", "background-color:RED;");
        //        }
        //    }
        //}

        for (int i = 0; i < lstUPVendor.Count; i++)
        {
            if (lstUPVendor[i].VendorID > 0)
                if (Convert.ToInt32(lstVendor.Items[i].Value) == lstUPVendor[i].VendorID && lstUPVendor[i].IsActiveVendor == "N")
                    this.lstVendor.Items[i].Attributes.Add("Style", "background-color:RED;");
        }
        /// --- Added for Showing Inative Vendors --- 27 Feb 2013

        mdlVendorViewer.Show();

    }

    private List<UP_VendorBE> GetVendor(bool doValidate)
    {
        UP_VendorBAL oUP_VendorBAL = new UP_VendorBAL();
        UP_VendorBE oUP_VendorBE = new UP_VendorBE();

        oUP_VendorBE.Action = "SearchVendor";
        
        oUP_VendorBE.Site = new BusinessEntities.ModuleBE.AdminFunctions.MAS_SiteBE();
        if (doValidate == true)
            oUP_VendorBE.Vendor_No = txtVendorNo.Text.Trim() == string.Empty ? "" : txtVendorNo.Text.Trim();
        else
            oUP_VendorBE.VendorName = txtVenderName2.Text.Trim();

        oUP_VendorBE.IsChildRequired = IsChildRequired != null ? IsChildRequired : (bool?)null;
        oUP_VendorBE.IsGrandParentRequired = IsGrandParentRequired != null ? IsGrandParentRequired : (bool?)null;
        oUP_VendorBE.IsParentRequired = IsParentRequired != null ? IsParentRequired : (bool?)null;
        oUP_VendorBE.IsStandAloneRequired = IsStandAloneRequired != null ? IsStandAloneRequired : (bool?)null;

        if (CountryID == 0)
        {
            oUP_VendorBE.Site.SiteCountryID = -1; //Convert.ToInt32(Session["SiteCountryID"].ToString());
            if (SiteID == 0)
                oUP_VendorBE.Site.SiteID = Convert.ToInt32(Session["SiteID"].ToString());
            else
                oUP_VendorBE.Site.SiteID = SiteID;
        }
        else
            oUP_VendorBE.Site.SiteCountryID = CountryID;


        List<UP_VendorBE> lstUPVendor  = oUP_VendorBAL.GetCarrierDetailsBAL(oUP_VendorBE);
        return lstUPVendor;
    }


    private List<UP_VendorBE> GetAllVendor(bool doValidate){
        UP_VendorBAL oUP_VendorBAL = new UP_VendorBAL();
        UP_VendorBE oUP_VendorBE = new UP_VendorBE();

        oUP_VendorBE.Action = "SearchVendor";
        oUP_VendorBE.VendorName = txtVenderName2.Text.Trim();
        oUP_VendorBE.Site = new BusinessEntities.ModuleBE.AdminFunctions.MAS_SiteBE();
        if (doValidate)
            oUP_VendorBE.Vendor_No = txtVendorNo.Text.Trim() == string.Empty ? "" : txtVendorNo.Text.Trim();

        oUP_VendorBE.IsChildRequired = IsChildRequired != null ? IsChildRequired : (bool?)null;
        oUP_VendorBE.IsGrandParentRequired = IsGrandParentRequired != null ? IsGrandParentRequired : (bool?)null;
        oUP_VendorBE.IsParentRequired = IsParentRequired != null ? IsParentRequired : (bool?)null;
        oUP_VendorBE.IsStandAloneRequired = IsStandAloneRequired != null ? IsStandAloneRequired : (bool?)null;

        List<UP_VendorBE> lstUPVendor = oUP_VendorBAL.GetCarrierDetailsBAL(oUP_VendorBE);
        return lstUPVendor;
    }


    public List<UP_VendorBE> IsValidVendorCode()
    {
        if (FunctionCalled != null && FunctionCalled == "GetAllVendor") {
            return GetAllVendor(true);
        }
        else {
            return GetVendor(true);
        }

        
    }

    protected void btnHidden_Click(object sender, EventArgs e)
    {
        if (CurrentPage.PreVendorNo_Change())
        {
            if (txtVendorNo.Text.Trim() != string.Empty)
            {
                List<UP_VendorBE> lstUPVendor = IsValidVendorCode();
                if (lstUPVendor.Count > 0)
                {
                    hdnVendorID.Value = lstUPVendor[0].VendorID.ToString();
                    hdnParentVendorID.Value = lstUPVendor[0].ParentVendorID.ToString();
                    txtVendorNo.Text = lstUPVendor[0].Vendor_No.ToString();
                    SelectedVendorName.Text = lstUPVendor[0].VendorName.ToString();
                    hdnVendorName.Value = lstUPVendor[0].VendorName.ToString();
                    CurrentPage.PostVendorNo_Change();
                }
                else
                {
                    hdnVendorID.Value = string.Empty;
                    hdnParentVendorID.Value = string.Empty;
                    txtVendorNo.Text = string.Empty;
                    SelectedVendorName.Text = string.Empty;
                    hdnVendorName.Value = string.Empty;

                    ScriptManager.RegisterStartupScript(this.txtVendorNo, this.txtVendorNo.GetType(), "alert", "alert('Please enter a valid Vendor Code.')", true);
                    return;
                }
            }
            else
            {
                hdnVendorID.Value = string.Empty;
                hdnParentVendorID.Value = string.Empty;
                txtVendorNo.Text = string.Empty;
                SelectedVendorName.Text = string.Empty;
                hdnVendorName.Value = string.Empty;
            }
        }

    }

    protected void btnHiddensetValues_Click(object sender, EventArgs e)
    {
        if (CurrentPage.PreVendorNo_Change())
        {

        }
        //CurrentPage.PostVendorNo_Change();
    }

    protected void btnSelect_Click(object sender, EventArgs e) {
        if (lstVendor.SelectedIndex >= 0) {
            string strVendorVal = lstVendor.SelectedItem.Value;
            string strVendor = lstVendor.SelectedItem.Text;
            SelectedVendorName.Text = strVendor;
            txtVendorNo.Text = strVendor.Split('-')[0];
            hdnVendorName.Value = strVendor;
            hdnVendorID.Value = strVendorVal.Trim();
            List<UP_VendorBE> lstUPVendor = IsValidVendorCode();            
            if (lstUPVendor.Count > 0) {                
                hdnParentVendorID.Value = lstUPVendor[0].ParentVendorID.ToString();
            }

            lstVendor.Items.Clear();
            mdlVendorViewer.Hide();
            CurrentPage.PostVendorNo_Change();
        }
        else {
            mdlVendorViewer.Show();
        }
    }
    
    protected void txtVendorNo_TextChanged(object sender, EventArgs e)
    {
        //ScriptManager.RegisterStartupScript(this.txtVendorNo, this.txtVendorNo.GetType(), "setvalue", "vendorNoChanged();", true);

        if (ViewState["txtVendorNoVal"] != null)
        {
            txtVendorNo.Text = Convert.ToString(ViewState["txtVendorNoVal"]);
            ViewState["txtVendorNoVal"] = null;
        }
        if(txtVendorNo.Text.Trim() != string.Empty) {
            btnHidden_Click(sender, e);
        }
        else {
            hdnVendorID.Value = string.Empty;
            hdnParentVendorID.Value = string.Empty;
            txtVendorNo.Text = string.Empty;
            SelectedVendorName.Text = string.Empty;
            hdnVendorName.Value = string.Empty;
        }
    }

    protected void btnPopupCancel_Click(object sender, EventArgs e) {
        lstVendor.Items.Clear();
        mdlVendorViewer.Hide();
    }
    
}
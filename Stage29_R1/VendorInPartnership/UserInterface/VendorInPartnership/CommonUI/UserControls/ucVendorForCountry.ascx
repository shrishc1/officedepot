﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucVendorForCountry.ascx.cs" Inherits="ucVendorForCountry" %>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<asp:UpdatePanel ID="UpdatePanelVendor" runat="server">
    <ContentTemplate>
         <table border="0">
            <tr>
                <td align="left" style="width: 30%">
                    <table>
                        <tr>
                            <td>
                                <cc1:ucTextbox ID="txtVendorNo" runat="server" MaxLength="15" Width="170px"></cc1:ucTextbox>
                                <asp:RequiredFieldValidator ID="rfvRequiredVendorName" runat="server" ControlToValidate="txtVendorNo" Display="None" ValidationGroup="Search"></asp:RequiredFieldValidator>
                            </td>
                            <td>
                                <cc1:ucButton CssClass="button" runat="server" ID="btnSearchVendor" OnClick="btnSearch_Click" Text="Search" ValidationGroup="Search"/>
                                 <asp:ValidationSummary ID="vSummary" runat="server" ShowMessageBox="true" ShowSummary="false"
                            Style="color: Red" ValidationGroup="Search" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <cc1:ucListBox ID="lstLeft" runat="server" Height="150px" Width="320px">
                    </cc1:ucListBox>
                </td>
            </tr>
        </table>
    </ContentTemplate>
</asp:UpdatePanel>

﻿using System;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.AdminFunctions;
using BusinessLogicLayer.ModuleBAL.AdminFunctions;
using System.Collections.Generic;
using BusinessEntities.ModuleBE.Security;
using BusinessLogicLayer.ModuleBAL.Security;

public partial class MultiSelectAP : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            SCT_UserBE oSCT_UserBE = new SCT_UserBE();
            SCT_UserBAL oSCT_UserBAL = new SCT_UserBAL();
            oSCT_UserBE.Action = "ShowAll";
            oSCT_UserBE.RoleName = "OD - Accounts Payable";
            List<SCT_UserBE> lstUsers = oSCT_UserBAL.GetUserDetailsBAL(oSCT_UserBE);
            if (lstUsers != null && lstUsers.Count > 0)
            {
                FillControls.FillListBox(ref lstLeft, lstUsers, "APUserName", "UserID");
            }
        }
    }

    public void BindAP()
    {
        SCT_UserBE oSCT_UserBE = new SCT_UserBE();
        SCT_UserBAL oSCT_UserBAL = new SCT_UserBAL();
        oSCT_UserBE.Action = "ShowAll";
        oSCT_UserBE.RoleName = "OD - Accounts Payable";
        List<SCT_UserBE> lstUsers = oSCT_UserBAL.GetUserDetailsBAL(oSCT_UserBE);
        if (lstUsers != null && lstUsers.Count > 0)
        {
            FillControls.FillListBox(ref lstLeft, lstUsers, "APUserName", "UserID");
        }
    }

    private string _selectedAPIDs = string.Empty;
    //public string SelectedAPIDs
    //{
    //    get
    //    {
    //        _selectedAPIDs = string.Empty;

    //        if (lstRight.Items.Count > 0)
    //        {
    //            foreach (ListItem item in lstRight.Items)
    //            {
    //                _selectedAPIDs += "," + item.Value;
    //            }
    //            return _selectedAPIDs.Trim(',');
    //        }
    //        return null;
    //    }
    //}

    //public string SelectedAPName
    //{
    //    get
    //    {
    //        string _selectedAPName = string.Empty;

    //        foreach (ListItem item in lstRight.Items)
    //            _selectedAPName += item.Text + ", ";

    //        if (_selectedAPName.Length > 0)
    //           _selectedAPName = _selectedAPName.Substring(0, _selectedAPName.Length - 2);

    //        return _selectedAPName;
    //    }
    //}
    public void SetAPOnPostBack()
    {
        lstRight.Items.Clear();
        string[] strSelectedName = hiddenSelectedName.Value.TrimEnd(',').TrimEnd(' ').Split(',');
        string[] strSelectedIDs = hiddenSelectedIDs.Value.TrimEnd(',').Split(',');

        if (strSelectedIDs.Length > 0)
            this.BindAP();

        for (int iCount = 0; iCount < strSelectedIDs.Length; iCount++)
        {
            if (!string.IsNullOrEmpty(strSelectedIDs[iCount]) && !string.IsNullOrWhiteSpace(strSelectedIDs[iCount]))
            {
                lstRight.Items.Add(new ListItem(strSelectedName[iCount].ToString().Trim(), strSelectedIDs[iCount].ToString().Trim()));
                RemoveAP(strSelectedIDs[iCount].ToString().Trim());
            }
        }
    }
    public void RemoveAP(string APid)
    {
        if (lstLeft.Items.FindByValue(APid.Trim()) != null)
            lstLeft.Items.RemoveAt(lstLeft.Items.IndexOf(lstLeft.Items.FindByValue(APid.Trim())));
    }
    public string SelectedAPIDs
    {
        get
        {
            if (!string.IsNullOrEmpty(hiddenSelectedIDs.Value))
            {
                return hiddenSelectedIDs.Value.Trim(',');
            }
            return null;
        }
    }

    public string SelectedAPName
    {
        get
        {
            string _selectedAPName = string.Empty;

            if (!string.IsNullOrEmpty(hiddenSelectedName.Value))
            {
                _selectedAPName = hiddenSelectedName.Value;
            }

            if (_selectedAPName.Length > 0)
                _selectedAPName = _selectedAPName.Substring(0, _selectedAPName.Length - 2);

            return _selectedAPName;
        }
    }

    protected virtual void btnMoveRightAll_Click(object sender, EventArgs e)
    {
        FillControls.MoveAllItems(lstLeft, lstRight);
    }

    protected virtual void btnMoveRight_Click(object sender, EventArgs e)
    {
        if (lstLeft.SelectedItem != null)
        {
            FillControls.MoveOneItem(lstLeft, lstRight);
        }
    }

    protected virtual void btnMoveLeft_Click(object sender, EventArgs e)
    {
        if (lstRight.SelectedItem != null)
        {
            FillControls.MoveOneItem(lstRight, lstLeft);
        }
    }

    protected virtual void btnMoveLeftAll_Click(object sender, EventArgs e)
    {
        FillControls.MoveAllItems(lstRight, lstLeft);
    }
}
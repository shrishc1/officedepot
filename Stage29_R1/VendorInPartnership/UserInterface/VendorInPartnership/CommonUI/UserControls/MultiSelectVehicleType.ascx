﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MultiSelectVehicleType.ascx.cs"
    Inherits="SeacrhnSelectVehicleType" %>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<script language="javascript" type="text/javascript">
    $(document).ready(function () {
        var lstitem = document.getElementById('<%= lstSelectedVehicleType.ClientID %>');
        if (lstitem.options.length == 0) {
            document.getElementById('<%= hiddenSelectedIDs.ClientID %>').value = "";
            document.getElementById('<%= hiddenSelectedName.ClientID %>').value = "";
        }
    });

</script>
<table width="100%">
    <tr>
        <td>
            <cc1:ucListBox ID="lstVehicleType" runat="server" Height="150px" Width="320px">
            </cc1:ucListBox>
        </td>
        <td align="center" class="search-controls-btn">
            <div>
                <input type="button" id="btnMoveRightAll" value=">>" class="button" style="width: 35px"
                    onclick="Javascript:MoveAll('<%= lstVehicleType.ClientID %>', '<%= lstSelectedVehicleType.ClientID %>','<%= hiddenSelectedIDs.ClientID %>','<%= hiddenSelectedName.ClientID %>');" />
            </div>
            &nbsp;
            <div>
                <input type="button" id="btnMoveRight" value=">" class="button" style="width: 35px"
                    onclick="Javascript:MoveItem('<%= lstVehicleType.ClientID %>', '<%= lstSelectedVehicleType.ClientID %>','<%= hiddenSelectedIDs.ClientID %>','<%= hiddenSelectedName.ClientID %>');" />
            </div>
            &nbsp;
            <div>
                <input type="button" id="btnMoveLeft" value="<" class="button" style="width: 35px"
                    onclick="Javascript:MoveItem('<%= lstSelectedVehicleType.ClientID %>', '<%= lstVehicleType.ClientID %>','<%= hiddenSelectedIDs.ClientID %>','<%= hiddenSelectedName.ClientID %>');" />
            </div>
            &nbsp;
            <div>
                <input type="button" id="btnMoveLeftAll" value="<<" class="button" style="width: 35px"
                    onclick="Javascript:MoveAll('<%= lstSelectedVehicleType.ClientID %>', '<%= lstVehicleType.ClientID %>','<%= hiddenSelectedIDs.ClientID %>','<%= hiddenSelectedName.ClientID %>');" />
            </div>
        </td>
        <td>
            <cc1:ucListBox ID="lstSelectedVehicleType" runat="server" Height="150px" Width="320px">
            </cc1:ucListBox>
            <asp:HiddenField ID="hiddenSelectedIDs" runat="server" />
            <asp:HiddenField ID="hiddenSelectedName" runat="server" />
        </td>
    </tr>
</table>

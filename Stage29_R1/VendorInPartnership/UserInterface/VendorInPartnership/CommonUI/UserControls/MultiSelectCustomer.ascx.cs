﻿using BusinessEntities.ModuleBE.Security;
using BusinessLogicLayer.ModuleBAL.Security;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class MultiSelectCustomer : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindCustomer();            
        }
    }

    private void BindCustomer()
    {
        MASCNT_CustomerSetupBE oMASCNT_CustomerSetupBE = new MASCNT_CustomerSetupBE();
        MASCNT_CustomerSetupBAL oMASCNT_CustomerSetupBAL = new MASCNT_CustomerSetupBAL();

        oMASCNT_CustomerSetupBE.Action = "SearchCustomer";
        if (!string.IsNullOrWhiteSpace(txtUserName.Text.Trim()))
            oMASCNT_CustomerSetupBE.CustomerName = txtUserName.Text.Trim();

        List<MASCNT_CustomerSetupBE> mASCNT_CustomerSetupBEs = oMASCNT_CustomerSetupBAL.SearchCustomersBAL(oMASCNT_CustomerSetupBE);
        if (mASCNT_CustomerSetupBEs.Count > 0)
        {
            FillControls.FillListBox(ref ucLBLeft, mASCNT_CustomerSetupBEs, "CustomerName", "CustomerId");
        }
    }    

    public void clearsearch()
    {
        ucLBRight.Items.Clear();
        hiddenSelectedIDs.Value = string.Empty;
        hiddenSelectedName.Value = string.Empty;
    }
   
    //private string _selectedStockPlannerIDs = string.Empty;    

    public string SelectedCustomerIDs
    {
        get
        {
            if (!string.IsNullOrEmpty(hiddenSelectedIDs.Value))
            {
                hiddenSelectedIDs.Value.Replace(",,", ",").Trim(',');
            }
            return hiddenSelectedIDs.Value;
        }
    } 
    

    public string SelectedCustomerName
    {
        get
        {
            string _selectedSPName = string.Empty;

            if (!string.IsNullOrEmpty(hiddenSelectedName.Value))
            {
                _selectedSPName = hiddenSelectedName.Value;
            }

            if (_selectedSPName.Length > 0)
                _selectedSPName = _selectedSPName.Substring(0, _selectedSPName.Length - 1);

            return _selectedSPName;
        }
        set
        {
            hiddenSelectedName.Value += value + ",";
        }
    }    

    protected virtual void btnMoveRightAll_Click(object sender, EventArgs e)
    {
        FillControls.MoveAllItems(ucLBLeft, ucLBRight);
    }

    protected virtual void btnMoveRight_Click(object sender, EventArgs e)
    {
        if (ucLBLeft.SelectedItem != null)
        {
            FillControls.MoveOneItem(ucLBLeft, ucLBRight);
        }
    }

    protected virtual void btnMoveLeft_Click(object sender, EventArgs e)
    {
        if (ucLBRight.SelectedItem != null)
        {
            FillControls.MoveOneItem(ucLBRight, ucLBLeft);
        }
    }

    protected virtual void btnMoveLeftAll_Click(object sender, EventArgs e)
    {
        FillControls.MoveAllItems(ucLBRight, ucLBLeft);
    }

    protected void btnSearchUser_Click(object sender, EventArgs e)
    {
        ucLBLeft.Items.Clear();
        BindCustomer();
        ReBingRightList();
    }

    private void ReBingRightList()
    {
        ucLBRight.Items.Clear();
        if (!string.IsNullOrWhiteSpace(hdnListRightUser.Value))
        {
            string[] Emails = hdnListRightUser.Value.Split('$');
            for (int index = 0; index < Emails.Length; index++)
            {
                string[] EmailIds = Emails[index].Split('#');
                ListItem listItem = new ListItem(EmailIds[0], EmailIds[1]);
                ucLBRight.Items.Add(listItem);
            }
            hdnListRightUser.Value = String.Empty;
        }
    }


    public void SetSPOnPostBack()
    {
        ucLBRight.Items.Clear();
        string[] strSelectedName = hiddenSelectedName.Value.TrimEnd(',').TrimEnd(' ').Split(',');
        string[] strSelectedIDs = hiddenSelectedIDs.Value.TrimEnd(',').Split(',');

        if (strSelectedIDs.Length > 0)
            this.BindCustomer();
        if (strSelectedName.Length == strSelectedIDs.Length)
        {
            for (int iCount = 0; iCount < strSelectedIDs.Length; iCount++)
            {
                if (!string.IsNullOrEmpty(strSelectedIDs[iCount]) && !string.IsNullOrWhiteSpace(strSelectedIDs[iCount]))
                {
                    ucLBRight.Items.Add(new ListItem(strSelectedName[iCount].ToString().Trim(), strSelectedIDs[iCount].ToString().Trim()));
                    RemoveSP(strSelectedIDs[iCount].ToString().Trim());
                }
            }
        }
    }

    public void setStockPlannerOnPostBack()
    {
        ucLBRight.Items.Clear();
        string[] strSelectedName = hiddenSelectedName.Value.Split(',');
        string[] strSelectedIDs = hiddenSelectedIDs.Value.Replace(",,", ",").Replace(",,", "").Split(',');
        for (int iCount = 0; iCount < strSelectedIDs.Length; iCount++)
        {
            if (!string.IsNullOrEmpty(strSelectedIDs[iCount]) && !string.IsNullOrWhiteSpace(strSelectedIDs[iCount]) && (strSelectedName.Length == strSelectedIDs.Length))
            {
                ucLBRight.Items.Add(new ListItem(strSelectedName[iCount].ToString().Trim(), strSelectedIDs[iCount].ToString().Trim()));
                ucLBLeft.Items.Remove(new ListItem(strSelectedName[iCount].ToString().Trim(), strSelectedIDs[iCount].ToString().Trim()));
            }
        }
    }

    public void RemoveSP(string SPID)
    {
        if (ucLBLeft.Items.FindByValue(SPID.Trim()) != null)
            ucLBLeft.Items.RemoveAt(ucLBLeft.Items.IndexOf(ucLBLeft.Items.FindByValue(SPID.Trim())));
    }
    
}
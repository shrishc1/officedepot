﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.Security;
using BusinessLogicLayer.ModuleBAL.Security;
using BusinessEntities.ModuleBE.StockOverview;
using BusinessLogicLayer.ModuleBAL.StockOverview;

public partial class CommonUI_UserControls_MultiSelectSKUGrouping : System.Web.UI.UserControl
{
    public bool isMoveAllRequired {
        set {
            hdnIsMoveAllRequired.Value = value.ToString();
        }
    }


    private bool _isMoveAllRequired {
        get {
            return !string.IsNullOrEmpty(hdnIsMoveAllRequired.Value) ? Convert.ToBoolean(hdnIsMoveAllRequired.Value) : false ;
        }       
    }


    bool isHubRequired = true;
    public bool IsHubRequired {
        get {
            return isHubRequired;
        }
        set {
            isHubRequired = value;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (_isMoveAllRequired) {
            divMoveAllLeft.Visible = divMoveAllRight.Visible = true;
        }

        if (!IsPostBack)
        {
            SKUGroupingBE oSKUGroupingBE = new SKUGroupingBE();
            SKUGroupingBAL oSKUGroupingBAL = new SKUGroupingBAL();
            oSKUGroupingBE.Action = "ShowAll"; 
            List<SKUGroupingBE> lstSkuGrouping = new List<SKUGroupingBE>();
            lstSkuGrouping = oSKUGroupingBAL.GetStockPlannerGroupingsBAL(oSKUGroupingBE);
            if (lstSkuGrouping.Count > 0)
            {
                hiddenSkuGroupingIDs.Value = string.Join(",", lstSkuGrouping.Select(x => x.SKugroupingID).ToArray());

                FillControls.FillListBox(ref lstLeft, lstSkuGrouping, "GroupName", "SKugroupingID");
            }
        }
    }

    public void BindSkuGrouping()
    {
        SKUGroupingBE oSKUGroupingBE = new SKUGroupingBE();
        SKUGroupingBAL oSKUGroupingBAL = new SKUGroupingBAL();

        oSKUGroupingBE.Action = "ShowAll";
        List<SKUGroupingBE> lstSkuGrouping = new List<SKUGroupingBE>();
        lstSkuGrouping = oSKUGroupingBAL.GetStockPlannerGroupingsBAL(oSKUGroupingBE);
        // lstSkuGrouping.RemoveAll(x => x.SkuGroupingID == Convert.ToInt32(strSkuGroupingid));
        if (lstSkuGrouping.Count > 0)
        {
            hiddenSkuGroupingIDs.Value = string.Join(",", lstSkuGrouping.Select(x => x.SKugroupingID).ToArray());

            FillControls.FillListBox(ref lstLeft, lstSkuGrouping, "GroupName", "SKugroupingID");
        }
    }
    private string _selectedSkuGroupingIDs = string.Empty;
   
    public bool RequiredUserDefaultSkuGrouping = true;

    public string SelectedSkuGroupingIDs {
        get {
            if (!string.IsNullOrEmpty(hiddenSelectedIDs.Value)) {
                return hiddenSelectedIDs.Value.Trim(',');               
            }
            else if (!string.IsNullOrEmpty(hiddenSkuGroupingIDs.Value) && RequiredUserDefaultSkuGrouping == false)
            {
                return hiddenSkuGroupingIDs.Value;
            }            
            return null;
        }
        set {
            hiddenSelectedIDs.Value += value + ",";
        }
    }

    public string SkuGroupingIds { get; set; }
    public string SelectedSkuGroupingIDsForOB
    {
        get
        {
            if (!string.IsNullOrEmpty(hiddenSelectedIDs.Value))
            {
               
                return hiddenSelectedIDs.Value;
            }
            else if (!string.IsNullOrEmpty(hiddenSkuGroupingIDs.Value) && RequiredUserDefaultSkuGrouping == false)
            {
                return hiddenSkuGroupingIDs.Value;
            }
            return null;
        }
        set
        {
            hiddenSelectedIDs.Value += value + ",";
        }
    }

    public string SelectedSkuGroupingName {
        get {
            string _selectedSkuGroupingName = string.Empty;

            if (!string.IsNullOrEmpty(hiddenSelectedName.Value)) {
                _selectedSkuGroupingName = hiddenSelectedName.Value;
            }

            //Commented by Abhinav - it was not giving full name 
            //if (_selectedSkuGroupingName.Length > 0)
            //    _selectedSkuGroupingName = _selectedSkuGroupingName.Substring(0, _selectedSkuGroupingName.Length - 2);

            return _selectedSkuGroupingName;
        }
        set
        {
            hiddenSelectedName.Value += value + ",";
        }
    }

    public HiddenField innerControlHiddenSelectedID
    {
        get
        {
            return this.hiddenSelectedIDs;
        }
    }

    public HiddenField innerControlHiddenSelectedName
    {
        get
        {
            return this.hiddenSelectedName;
        }
    }

    public void setSkuGroupingsOnPostBack()
    {
        lstRight.Items.Clear();
        string[] strSelectedName = hiddenSelectedName.Value.Split(',');
        string[] strSelectedIDs = hiddenSelectedIDs.Value.Replace(",,", ",").Replace(",,", "").Split(',');
        for (int iCount = 0; iCount < strSelectedIDs.Length; iCount++)
        {
            if (!string.IsNullOrEmpty(strSelectedIDs[iCount]) && !string.IsNullOrWhiteSpace(strSelectedIDs[iCount]) && (strSelectedName.Length == strSelectedIDs.Length))
            {
                lstRight.Items.Add(new ListItem(strSelectedName[iCount].ToString().Trim(), strSelectedIDs[iCount].ToString().Trim()));
                lstLeft.Items.Remove(new ListItem(strSelectedName[iCount].ToString().Trim(), strSelectedIDs[iCount].ToString().Trim()));
            }
        }
    }

    protected virtual void btnMoveRightAll_Click(object sender, EventArgs e)
    {
        FillControls.MoveAllItems(lstLeft, lstRight);
    }

    protected virtual void btnMoveRight_Click(object sender, EventArgs e)
    {
        if (lstLeft.SelectedItem != null)
        {
            FillControls.MoveOneItem(lstLeft, lstRight);
        }
    }

    protected virtual void btnMoveLeft_Click(object sender, EventArgs e)
    {
        if (lstRight.SelectedItem != null)
        {
            FillControls.MoveOneItem(lstRight, lstLeft);
        }
    }

    protected virtual void btnMoveLeftAll_Click(object sender, EventArgs e)
    {
        FillControls.MoveAllItems(lstRight, lstLeft);
    }
}
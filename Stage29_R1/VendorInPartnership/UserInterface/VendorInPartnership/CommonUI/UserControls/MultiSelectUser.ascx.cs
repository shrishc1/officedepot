﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using BusinessLogicLayer.ModuleBAL.Appointment.SiteSettings;
using Utilities;
using WebUtilities;
using BusinessEntities.ModuleBE.Security;
using BusinessLogicLayer.ModuleBAL.Security;

public partial class MultiSelectUser : UserControl
{
    private string _selectedUserIDs = string.Empty;
    public string SelectedUserIDs
    {
        get
        {
            if (!string.IsNullOrEmpty(hiddenSelectedIDs.Value))
            {
                return hiddenSelectedIDs.Value.Trim(',');
            }
            return null;
        }
        set
        {
            hiddenSelectedIDs.Value += value + ",";
        }
    }
    public string SelectedUserName
    {
        get
        {
            string _selectedSPName = string.Empty;

            if (!string.IsNullOrEmpty(hiddenSelectedName.Value))
            {
                _selectedSPName = hiddenSelectedName.Value;
            }

            if (_selectedSPName.Length > 0)
                _selectedSPName = _selectedSPName.Substring(0, _selectedSPName.Length - 2);

            return _selectedSPName;
        }
    }

    private string setActionText;
    public string SetActionText
    {
        get { return setActionText; }
        set { setActionText = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        btnSearchUser.Text = WebCommon.getGlobalResourceValue("SearchUser");
    }

    protected void btnSearchUser_Click(object sender, EventArgs e)
    {
        BindUsers();
    }

    private string GetActionText()
    {
        var getActionText = string.Empty;
        if (!string.IsNullOrEmpty(setActionText))
        {
            if (setActionText.Equals("GetUsersForDeletedDiscripancy"))
            {
                getActionText = "GetUsersForDeletedDiscripancy";
            }
            else
            {
                getActionText = "GetUsersByName";
            }
        }
        else 
        {
            getActionText = "GetUsersByName";
        }

        return getActionText;
    }

    private void BindUsers()
    {
        SCT_UserBAL oSCT_UserBAL = new SCT_UserBAL();
        SCT_UserBE oSCT_UserBE = new SCT_UserBE();
        oSCT_UserBE.Action = this.GetActionText();
        oSCT_UserBE.FirstName = txtUserName.Text; // This text is being set in [@UserSearch] parameter in DAL layer.        
        List<SCT_UserBE> lstSCT_UserBE = new List<SCT_UserBE>();
        lstSCT_UserBE = oSCT_UserBAL.GetAllUsersBAL(oSCT_UserBE);
        if (lstSCT_UserBE.Count > 0)
            FillControls.FillListBox(ref lstLeft, lstSCT_UserBE, "UserName", "UserID");
        else
            lstLeft.Items.Clear();

        ReBingRightList();
    }

    private void ReBingRightList()
    {
        lstRightUser.Items.Clear();
        if (!string.IsNullOrWhiteSpace(hdnListRightUser.Value))
        {
            string[] Emails = hdnListRightUser.Value.Split('$');
            for (int index = 0; index < Emails.Length; index++)
            {
                string[] EmailIds = Emails[index].Split('#');
                ListItem listItem = new ListItem(EmailIds[0], EmailIds[1]);
                lstRightUser.Items.Add(listItem);
            }
            hdnListRightUser.Value = String.Empty;
        }
    }

    public void RemoveVendor(string VendorName, string VendorId)
    {
        if (lstLeft.Items.FindByValue(VendorId.Trim()) != null)
            lstLeft.Items.RemoveAt(lstLeft.Items.IndexOf(lstLeft.Items.FindByValue(VendorId.Trim())));
    }
        
    public void setVendorsOnPostBack()
    {
        lstRightUser.Items.Clear();
        string[] strSelectedName = hiddenSelectedName.Value.Split(',');
        string[] strSelectedIDs = hiddenSelectedIDs.Value.Split(',');
        for (int iCount = 0; iCount < strSelectedName.Length - 1; iCount++)
        {
            lstRightUser.Items.Add(new ListItem(strSelectedName[iCount].ToString().Trim(), strSelectedIDs[iCount].ToString().Trim()));
            RemoveVendor(strSelectedName[iCount].ToString().Trim(), strSelectedIDs[iCount].ToString().Trim());
        }
    }  
}
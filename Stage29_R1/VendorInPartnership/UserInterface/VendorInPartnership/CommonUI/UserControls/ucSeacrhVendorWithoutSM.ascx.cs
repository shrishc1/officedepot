﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using WebUtilities;
using BusinessLogicLayer.ModuleBAL.Appointment.SiteSettings;
using BaseControlLibrary;

public partial class ucSeacrhVendorWithoutSM : System.Web.UI.UserControl
{
    #region Public Property

    public CommonPage CurrentPage
    {
        get;
        set;
    }

    private string vendorNo;
    public string VendorNo
    {
        get
        {
            return hdnVendorID.Value;
        }
        set { hdnVendorID.Value = value; }
    }

    public ucLabel innerControlVendorName
    {
        get
        {
            return this.SelectedVendorName;
        }
        set
        {
            this.SelectedVendorName = value;
        }
    }


    public int SiteID
    {
        get { return Convert.ToInt32(ViewState["SiteID"]); }
        set { ViewState["SiteID"] = value; }
    }



    public int CountryID
    {
        get { return Convert.ToInt32(ViewState["CountryID"]); }
        set { ViewState["CountryID"] = value; }
    }

    public bool? IsChildRequired
    {
        get { return Convert.ToBoolean(ViewState["IsChildRequired"]); }
        set { ViewState["IsChildRequired"] = value; }
    }

    public bool? IsParentRequired
    {
        get { return Convert.ToBoolean(ViewState["IsParentRequired"]); }
        set { ViewState["IsParentRequired"] = value; }
    }

    public bool? IsStandAloneRequired
    {
        get { return Convert.ToBoolean(ViewState["IsStandAloneRequired"]); }
        set { ViewState["IsStandAloneRequired"] = value; }
    }

    public bool? IsGrandParentRequired
    {
        get { return Convert.ToBoolean(ViewState["IsGrandParentRequired"]); }
        set { ViewState["IsGrandParentRequired"] = value; }
    }

    public string FunctionCalled
    {
        get { return Convert.ToString(ViewState["FunctionCalled"]); }
        set { ViewState["FunctionCalled"] = value; }
    }

    private string _VendorCode;
    public string VendorCode
    {
        set { _VendorCode = value; }
    }

    public bool IsInActiveVendorRequired
    {
        get { return Convert.ToBoolean(ViewState["IsInActiveVendorRequired"]); }
        set { ViewState["IsInActiveVendorRequired"] = value; }
    }

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        lstVendor.Attributes.Add("onDblClick", "lstVendor_DoubleClick();");
        if (!Page.IsPostBack)
        {

            txtVenderName2.Text = string.Empty;
            txtVendorNo.Text = string.Empty;
            txtVendorNo.Text = _VendorCode;
            lstVendor.Items.Clear();
        }

    }

    public void ClearSearch()
    {
        lstVendor.Items.Clear();
        txtVenderName2.Text = string.Empty;
        hdnVendorID.Value = string.Empty;
        txtVendorNo.Text = string.Empty;
        SelectedVendorName.Text = string.Empty;
        hdnVendorName.Value = string.Empty;
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        List<UP_VendorBE> lstUPVendor;
        List<UP_VendorBE> lstNewUPVendor = new List<UP_VendorBE>();

        if (FunctionCalled != null && FunctionCalled == "GetAllVendor")
        {
            lstUPVendor = GetAllVendor(false);
        }
        else
        {
            lstUPVendor = GetVendor(false);
        }
        if (lstUPVendor.Count == 0)
        {
            string errorMeesage = WebCommon.getGlobalResourceValue("ValidVendorCode");
            ScriptManager.RegisterStartupScript(this.txtVendorNo, this.txtVendorNo.GetType(), "alert1", "alert('" + errorMeesage + "')", true);
        }
        /// --- Added for Showing Inative Vendors --- 04 Mar 2013
        if (IsInActiveVendorRequired == false)
        {
            foreach (UP_VendorBE objVendor in lstUPVendor.ToList())
            {
                if (objVendor.IsActiveVendor == "Y")
                    lstNewUPVendor.Add(objVendor);
            }
            FillControls.FillListBox(ref lstVendor, lstNewUPVendor.ToList(), "VendorName", "VendorID");
        }
        else
        {
            FillControls.FillListBox(ref lstVendor, lstUPVendor.ToList(), "VendorName", "VendorID");

            foreach (ListItem item in lstVendor.Items)
            {
                for (int i = 0; i < lstUPVendor.Count; i++)
                {
                    if (item.Value == lstUPVendor[i].VendorID.ToString() && lstUPVendor[i].IsActiveVendor == "N")
                    {
                        this.lstVendor.Items[i].Attributes.Add("Style", "background-color:RED;");
                    }
                }
            }
        }
        /// --- Added for Showing Inative Vendors --- 04 Mar 2013

        mdlVendorViewer.Show();

    }

    private List<UP_VendorBE> GetVendor(bool doValidate)
    {
        UP_VendorBAL oUP_VendorBAL = new UP_VendorBAL();
        UP_VendorBE oUP_VendorBE = new UP_VendorBE();

        oUP_VendorBE.Action = "SearchVendor";

        oUP_VendorBE.Site = new BusinessEntities.ModuleBE.AdminFunctions.MAS_SiteBE();
        if (doValidate)
            oUP_VendorBE.Vendor_No = txtVendorNo.Text.Trim() == string.Empty ? "" : txtVendorNo.Text.Trim();
        else
            oUP_VendorBE.VendorName = txtVenderName2.Text.Trim();

        oUP_VendorBE.IsChildRequired = IsChildRequired != null ? IsChildRequired : (bool?)null;
        oUP_VendorBE.IsGrandParentRequired = IsGrandParentRequired != null ? IsGrandParentRequired : (bool?)null;
        oUP_VendorBE.IsParentRequired = IsParentRequired != null ? IsParentRequired : (bool?)null;
        oUP_VendorBE.IsStandAloneRequired = IsStandAloneRequired != null ? IsStandAloneRequired : (bool?)null;

        if (CountryID == 0)
        {
            oUP_VendorBE.Site.SiteCountryID = -1;
            oUP_VendorBE.Site.SiteID = SiteID == 0 ? Convert.ToInt32(Session["SiteID"].ToString()) : SiteID;
        }
        else
            oUP_VendorBE.Site.SiteCountryID = CountryID;


        List<UP_VendorBE> lstUPVendor = oUP_VendorBAL.GetCarrierDetailsBAL(oUP_VendorBE);
        return lstUPVendor;
    }


    private List<UP_VendorBE> GetAllVendor(bool doValidate)
    {
        UP_VendorBAL oUP_VendorBAL = new UP_VendorBAL();
        UP_VendorBE oUP_VendorBE = new UP_VendorBE();

        oUP_VendorBE.Action = "SearchVendor";
        oUP_VendorBE.VendorName = txtVenderName2.Text.Trim();
        oUP_VendorBE.Site = new BusinessEntities.ModuleBE.AdminFunctions.MAS_SiteBE();
        if (doValidate)
            oUP_VendorBE.Vendor_No = txtVendorNo.Text.Trim() == string.Empty ? "" : txtVendorNo.Text.Trim();

        oUP_VendorBE.IsChildRequired = IsChildRequired != null ? IsChildRequired : (bool?)null;
        oUP_VendorBE.IsGrandParentRequired = IsGrandParentRequired != null ? IsGrandParentRequired : (bool?)null;
        oUP_VendorBE.IsParentRequired = IsParentRequired != null ? IsParentRequired : (bool?)null;
        oUP_VendorBE.IsStandAloneRequired = IsStandAloneRequired != null ? IsStandAloneRequired : (bool?)null;


        List<UP_VendorBE> lstUPVendor = oUP_VendorBAL.GetCarrierDetailsBAL(oUP_VendorBE);
        return lstUPVendor;
    }


    public List<UP_VendorBE> IsValidVendorCode()
    {
        if (FunctionCalled != null && FunctionCalled == "GetAllVendor")
        {
            return GetAllVendor(true);
        }
        else
        {
            return GetVendor(true);
        }
    }

    protected void btnHidden_Click(object sender, EventArgs e)
    {
        if (CurrentPage.PreVendorNo_Change())
        {
            if (txtVendorNo.Text.Trim() != string.Empty)
            {
                List<UP_VendorBE> lstNewUPVendor = new List<UP_VendorBE>();
                List<UP_VendorBE> lstUPVendor = new List<UP_VendorBE>();

                /// --- Added for Showing Inative Vendors --- 04 Mar 2013
                if (!IsInActiveVendorRequired)
                {
                    lstNewUPVendor = IsValidVendorCode();
                    foreach (UP_VendorBE objVendor in lstNewUPVendor.ToList())
                    {
                        if (objVendor.IsActiveVendor == "Y")
                            lstUPVendor.Add(objVendor);
                    }
                }
                else
                {
                    lstUPVendor = IsValidVendorCode();
                }
                /// --- Added for Showing Inative Vendors --- 04 Mar 2013

                if (lstUPVendor.Count > 0)
                {
                    hdnVendorID.Value = lstUPVendor[0].VendorID.ToString();
                    hdnParentVendorID.Value = lstUPVendor[0].ParentVendorID.ToString();
                    txtVendorNo.Text = lstUPVendor[0].Vendor_No.ToString();
                    SelectedVendorName.Text = lstUPVendor[0].VendorName.ToString();
                    hdnVendorName.Value = lstUPVendor[0].VendorName.ToString();
                    CurrentPage.PostVendorNo_Change();
                }
                else
                {
                    hdnVendorID.Value = string.Empty;
                    hdnParentVendorID.Value = string.Empty;
                    txtVendorNo.Text = string.Empty;
                    SelectedVendorName.Text = string.Empty;
                    hdnVendorName.Value = string.Empty;

                    ScriptManager.RegisterStartupScript(this.txtVendorNo, this.txtVendorNo.GetType(), "alert", "alert('Please enter a valid Vendor Code.')", true);
                    return;
                }
            }
            else
            {
                hdnVendorID.Value = string.Empty;
                hdnParentVendorID.Value = string.Empty;
                txtVendorNo.Text = string.Empty;
                SelectedVendorName.Text = string.Empty;
                hdnVendorName.Value = string.Empty;
            }
        }
    }

    protected void btnHiddensetValues_Click(object sender, EventArgs e)
    {
        if (CurrentPage.PreVendorNo_Change())
        {
        }
    }

    protected void btnSelect_Click(object sender, EventArgs e)
    {
        if (lstVendor.SelectedIndex >= 0)
        {
            SetUserControlBaseProperties();
            string strVendorVal = lstVendor.SelectedItem.Value;
            string strVendor = lstVendor.SelectedItem.Text;
            SelectedVendorName.Text = strVendor;
            txtVendorNo.Text = strVendor.Split('-')[0];
            hdnVendorName.Value = strVendor;
            hdnVendorID.Value = strVendorVal.Trim();
            List<UP_VendorBE> lstUPVendor = IsValidVendorCode();
            if (lstUPVendor.Count > 0)
            {
                hdnParentVendorID.Value = lstUPVendor[0].ParentVendorID.ToString();
            }

            lstVendor.Items.Clear();
            mdlVendorViewer.Hide();
            CurrentPage.PostVendorNo_Change();
        }
        else
        {
            mdlVendorViewer.Show();
        }
    }

    protected void txtVendorNo_TextChanged(object sender, EventArgs e)
    {
        if (txtVendorNo.Text.Trim() != string.Empty)
        {

            SetUserControlBaseProperties();
            btnHidden_Click(sender, e);
        }
        else
        {
            hdnVendorID.Value = string.Empty;
            hdnParentVendorID.Value = string.Empty;
            txtVendorNo.Text = string.Empty;
            SelectedVendorName.Text = string.Empty;
            hdnVendorName.Value = string.Empty;
        }
    }

    protected void btnPopupCancel_Click(object sender, EventArgs e)
    {
        lstVendor.Items.Clear();
        mdlVendorViewer.Hide();
    }

    private void SetUserControlBaseProperties()
    {
        this.CurrentPage = (CommonPage)this.Page;
    }
}


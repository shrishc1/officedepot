﻿using System;
using System.Web.UI;
using System.Collections.Generic;
using System.Linq;
using BusinessEntities.ModuleBE.AdminFunctions;
using BusinessLogicLayer.ModuleBAL.AdminFunctions;
using BaseControlLibrary; 
using System.Web.UI.WebControls;


public partial class ucSite : UserControl
{

    public event EventHandler ddlSite_DropChange;

    List<MAS_SiteBE> lstSite = new List<MAS_SiteBE>();
    #region Public Property

    public MAS_SiteBE oSiteBE
    {
        get;
        set;
    }

    public ucDropdownList innerControlddlSite
    {
        get
        {
            return this.ddlSite;
        }
    }

    public CommonPage CurrentPage
    {
        get;
        set;
    }


    public ucUserControlBase CurrentUserControl
    {
        get;
        set;
    }


    public int SetSelectedValue
    {
        set;
        get;
    }

    int _CountryID = 0;
    public int CountryID
    {
        get
        {
            return _CountryID;
        }
        set
        {
            _CountryID = value;
        }
    }

    string _CountryIDs = string.Empty;
    public string CountryIDs
    {
        get
        {
            return _CountryIDs;
        }
        set
        {
            _CountryIDs = value;
        }
    }

    public int? PreCountryID
    {
        get
        {
            return ViewState["PreCountryID"] != null ? Convert.ToInt32(ViewState["PreCountryID"].ToString()) : (int?)null;
        }
        set
        {
            ViewState["PreCountryID"] = value;
        }
    }

    bool schedulingContact = false;
    public bool SchedulingContact
    {
        get
        {
            return schedulingContact;
        }
        set
        {
            schedulingContact = value;
        }
    }

    bool discrepancyContact = false;
    public bool DiscrepancyContact
    {
        get
        {
            return discrepancyContact;
        }
        set
        {
            discrepancyContact = value;
        }
    }

    bool isAllRequired = false;
    public bool IsAllRequired
    {
        get
        {
            return isAllRequired;
        }
        set
        {
            isAllRequired = value;
        }
    }

    bool isSelectRequired = false;
    public bool IsSelectRequired
    {
        get
        {
            return isSelectRequired;
        }
        set
        {
            isSelectRequired = value;
        }
    }

    bool isAllDefault = false;
    public bool IsAllDefault
    {
        get
        {
            return isAllDefault;
        }
        set
        {
            isAllDefault = value;
        }
    }

    string timeSlotWindow = string.Empty;
    public string TimeSlotWindow
    {
        get
        {
            return timeSlotWindow;
        }
        set
        {
            timeSlotWindow = value;
        }
    }

    public string UserSiteIDs
    {
        get
        {
            if (!string.IsNullOrEmpty(hiddenUserSiteIDs.Value))
            {
                return hiddenUserSiteIDs.Value;
            }
            return null;
        }
    }

    public bool IsBookingExclusions
    {
        get
        {
            return ViewState["IsBookingExclusions"] != null ? Convert.ToBoolean(ViewState["IsBookingExclusions"].ToString()) : false;
        }
        set
        {
            ViewState["IsBookingExclusions"] = value;
        }
    }


    public bool IsFromBookingOverview
    {
        get
        {
            return ViewState["IsFromBookingOverview"] != null ? Convert.ToBoolean(ViewState["IsFromBookingOverview"].ToString()) : false;
        }
        set
        {
            ViewState["IsFromBookingOverview"] = value;
        }
    }

    bool isAllowAllSiteWithoutUserId = false;
    public bool IsAllowAllSiteWithoutUserId
    {
        get
        {
            return isAllowAllSiteWithoutUserId;
        }
        set
        {
            isAllowAllSiteWithoutUserId = value;
        }
    }

    public bool IsExpediteSites
    {
        get
        {
            return ViewState["IsExpediteSites"] != null ? Convert.ToBoolean(ViewState["IsExpediteSites"].ToString()) : false;
        }
        set
        {
            ViewState["IsExpediteSites"] = value;
        }
    }

    public string siteIDs
    {
        get
        {
            return ViewState["siteIDs"] != null ? Convert.ToString(ViewState["siteIDs"].ToString()) : string.Empty;
        }
        set
        {
            ViewState["siteIDs"] = value;
        }
    }
    public bool IsEnableHazardouesItemPrompt
    {
        get
        {
            return ViewState["IsEnableHazardouesItemPrompt"] != null ? Convert.ToBoolean(ViewState["IsEnableHazardouesItemPrompt"].ToString()) : false;
        }
        set
        {
            ViewState["IsEnableHazardouesItemPrompt"] = value;
        }
    }
    #endregion


    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (CurrentPage == null)
            {
                if (CurrentUserControl.SitePrePage_Load())
                {
                    BindSites();
                    CurrentUserControl.SitePost_Load();
                }
            }
            else
                if (CurrentPage.SitePrePage_Load())
                {
                    BindSites();
                    CurrentPage.SitePost_Load();
                }
        }
    }


    public void BindSites()
    {
        MAS_SiteBE oMAS_SiteBE = new MAS_SiteBE();
        MAS_SiteBAL oMAS_SiteBAL = new MAS_SiteBAL();
        oMAS_SiteBE.Action = Session["Role"].ToString().ToLower() == "vendor" ? "GetVendorUserSite" : "ShowAll";
        oMAS_SiteBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
        if (!IsAllowAllSiteWithoutUserId)
            oMAS_SiteBE.User.UserID = Convert.ToInt32(Session["UserID"]);

        oMAS_SiteBE.SiteCountryID = Convert.ToInt32(CountryID.ToString());
        oMAS_SiteBE.SchedulingContact = SchedulingContact;
        oMAS_SiteBE.DiscrepancyContact = DiscrepancyContact;

        if (!string.IsNullOrEmpty(TimeSlotWindow))
            oMAS_SiteBE.TimeSlotWindow = TimeSlotWindow;
        else
            oMAS_SiteBE.TimeSlotWindow = string.Empty;
        if (Convert.ToBoolean(ViewState["IsExpediteSites"]))
        {
            oMAS_SiteBE.SiteIDs = Convert.ToString(ViewState["siteIDs"]);
            ViewState["IsExpediteSites"] = null;
            ViewState["siteIDs"] = null;
        }

        this.lstSite = oMAS_SiteBAL.GetSiteBAL(oMAS_SiteBE);


        if (lstSite.Count > 0)
        {

            hiddenUserSiteIDs.Value = string.Join(",", lstSite.Select(x => x.SiteID).ToArray());

            if (IsAllRequired)
                FillControls.FillDropDown(ref ddlSite, this.lstSite.ToList(), "SiteDescription", "SiteID", "--All--");
            else if (isSelectRequired)
                FillControls.FillDropDown(ref ddlSite, this.lstSite.ToList(), "SiteDescription", "SiteID", "--Select--");
            else
                FillControls.FillDropDown(ref ddlSite, this.lstSite.ToList(), "SiteDescription", "SiteID");

            if (!IsAllDefault && Session["SiteID"] != null)
                ddlSite.SelectedIndex = ddlSite.Items.IndexOf(ddlSite.Items.FindByValue(Session["SiteID"].ToString()));


            if (ddlSite.SelectedItem.Value != "0")
                oSiteBE = this.lstSite.Find(s => s.SiteID == Convert.ToInt32(ddlSite.SelectedItem.Value));
            else
                oSiteBE = this.lstSite[0];

            if (CountryID == 0 && ddlSite.Items.Count >= 0 && IsAllRequired)

                CountryID = oSiteBE.SiteCountryID ?? 0;
            PreCountryID = oSiteBE.SiteCountryID ?? 0;
            IsBookingExclusions = oSiteBE.IsBookingExclusions != null && oSiteBE.IsBookingExclusions;
            IsEnableHazardouesItemPrompt = oSiteBE.IsEnableHazardouesItemPrompt;
        }
        else
        {
            FillControls.FillDropDown(ref ddlSite, this.lstSite.ToList(), "SiteDescription", "SiteID", "--Select--");
        }

        if (lstSite.Count > 0)
        {
            _CountryIDs = string.Empty;
            _CountryIDs = string.Join(",", lstSite.Select(x => x.SiteCountryID).ToArray());
        }
    }



    protected void ddlSite_SelectedIndexChanged(object sender, EventArgs e)
    {

        SelectedIndexChanged();
        if (CurrentPage == null)
            CurrentUserControl.SiteSelectedIndexChanged();
        else
            CurrentPage.SiteSelectedIndexChanged();

        if (Convert.ToBoolean(ViewState["IsFromBookingOverview"]))
        {
            ddlSite_DropChange(sender, e);
        }
    }

    public void SelectedIndexChanged()
    {
        MAS_SiteBE oMAS_SiteBE = new MAS_SiteBE();
        MAS_SiteBAL oMAS_SiteBAL = new MAS_SiteBAL();

        oMAS_SiteBE.Action = "ShowAll";

        oMAS_SiteBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
        oMAS_SiteBE.User.UserID = Convert.ToInt32(Session["UserID"]);
        oMAS_SiteBE.SiteCountryID = Convert.ToInt32(CountryID.ToString());

        if (!string.IsNullOrEmpty(TimeSlotWindow))
            oMAS_SiteBE.TimeSlotWindow = TimeSlotWindow;
        else
            oMAS_SiteBE.TimeSlotWindow = string.Empty;

        if (Convert.ToBoolean(ViewState["IsExpediteSites"]) == true)
        {
            oMAS_SiteBE.SiteIDs = Convert.ToString(ViewState["siteIDs"]);
            ViewState["IsExpediteSites"] = null;
            ViewState["siteIDs"] = null;
        }

        lstSite = oMAS_SiteBAL.GetSiteBAL(oMAS_SiteBE);
        //oSiteBE = this.lstSite.Find(delegate(MAS_SiteBE s) { return s.SiteID == Convert.ToInt32(ddlSite.SelectedItem.Value); });
        oSiteBE = this.lstSite.Find(s => s.SiteID == Convert.ToInt32(ddlSite.SelectedItem.Value));
        if (oSiteBE != null)
        {
            _CountryID = Convert.ToInt32(oSiteBE.SiteCountryID);
            PreCountryID = Convert.ToInt32(oSiteBE.SiteCountryID);
            IsBookingExclusions = oSiteBE.IsBookingExclusions != null ? oSiteBE.IsBookingExclusions : false;
            IsEnableHazardouesItemPrompt = oSiteBE.IsEnableHazardouesItemPrompt;
        }
        else
        {
            _CountryID = 0;
            PreCountryID = 0;
            IsBookingExclusions = false;
            IsEnableHazardouesItemPrompt = false;
        }

    }
}

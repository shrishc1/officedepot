﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucSeacrhnSelectVendor.ascx.cs"
    Inherits="SeacrhnSelectVendor" %>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<script language="javascript" type="text/javascript">

    function setValues() {
        var e = document.getElementById('<%=lstVendor.ClientID%>');
        var strVendorVal = e.options[e.selectedIndex].value;
        var strVendor = e.options[e.selectedIndex].text;

        document.getElementById('<%=hdnVendorID.ClientID%>').value = strVendorVal;


        return false;
    }

    function disableEnterKey(e) {
        var key;
        if (window.event)
            key = window.event.keyCode; //IE
        else
            key = e.which; //firefox     

        return (key != 13);
    }
</script>

<asp:PlaceHolder ID="phControls" runat="server" ></asp:PlaceHolder>
<asp:UpdatePanel ID="Up1" runat="server" oninit="Up1_Init">
    <ContentTemplate>
        <asp:HiddenField ID="hdnVendorID" runat="server" />
      
                    <table width="100%" cellspacing="1" cellpadding="0" border="0" align="center" class="top-settingsNoBorder">
                        <tr>
                            <td style="font-weight: bold;" width="9%" nowrap>
                                <cc1:ucLabel ID="lblVendorName" runat="server" Text="Vendor Name"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold;" width="1%">
                                :
                            </td>
                            <td width="30%">
                                <cc1:ucTextbox ID="txtVenderName2" runat="server" Width="230px" onkeyup="return disableKeyPress(event)"></cc1:ucTextbox>
                            </td>
                            <td align="center" width="10%">
                                <cc1:ucButton OnClick="btnSearch_Click" ID="btnSearch" runat="server" Text="Search"
                                    CssClass="button" />
                            </td>
                            <td align="center" width="50%">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="5">
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold;" align="left" colspan="3">
                                <cc1:ucListBox ID="lstVendor" runat="server" Width="320px" Height="200px">
                                </cc1:ucListBox>
                            </td>
                            <td valign="middle" align="center">
                                <div>
                                    <cc1:ucButton ID="btnMoveRightAll" runat="server" Text=">>" CssClass="button" Width="35px"
                                        OnClick="btnMoveRightAll_Click" /></div>
                                &nbsp;
                                <div>
                                    <cc1:ucButton ID="btnMoveRight" runat="server" Text=">" CssClass="button" Width="35px"
                                        OnClick="btnMoveRight_Click" /></div>
                                &nbsp;
                                <div>
                                    <cc1:ucButton ID="btnMoveLeft" runat="server" Text="<" CssClass="button" Width="35px"
                                        OnClick="btnMoveLeft_Click" /></div>
                                &nbsp;
                                <div>
                                    <cc1:ucButton ID="btnMoveLeftAll" runat="server" Text="<<" CssClass="button" Width="35px"
                                        OnClick="btnMoveLeftAll_Click" /></div>
                            </td>
                            <td>
                                <cc1:ucListBox ID="lstSelectedVendor" runat="server" Width="320px" Height="200px">
                                </cc1:ucListBox>
                            </td>
                        </tr>
                    </table>
                
           <%-- </tr>
        </table>--%>
    </ContentTemplate>
    <Triggers>
        <asp:AsyncPostBackTrigger ControlID="btnMoveRightAll" EventName="Click" />
        <asp:AsyncPostBackTrigger ControlID="btnMoveRight" EventName="Click" />
        <asp:AsyncPostBackTrigger ControlID="btnMoveLeft" EventName="Click" />
        <asp:AsyncPostBackTrigger ControlID="btnMoveLeftAll" EventName="Click" />
    </Triggers>
</asp:UpdatePanel>

﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucMultilistSelector.ascx.cs"
    Inherits="CommonUI_UserControls_ucMultilistSelector" %>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<table>
    <tr>
        <td>
            <cc1:ucListBox ID="lstLeft" runat="server" Height="320px" Width="320px">
            </cc1:ucListBox>
        </td>
        <td align="center" class="search-controls-btn">
            <div>
                <cc1:ucButton ID="btnMoveRightAll" runat="server" Text=">>" CssClass="button" 
                    Width="35px" onclick="btnMoveRightAll_Click" />
            </div>
            &nbsp;
            <div>
                <cc1:ucButton ID="btnMoveRight" runat="server" Text=">" CssClass="button" 
                    Width="35px" onclick="btnMoveRight_Click" />
            </div>
            &nbsp;
            <div>
                <cc1:ucButton ID="btnMoveLeft" runat="server" Text="<" CssClass="button" 
                    Width="35px" onclick="btnMoveLeft_Click" />
            </div>
            &nbsp;
            <div>
                <cc1:ucButton ID="btnMoveLeftAll" runat="server" Text="<<" CssClass="button" 
                    Width="35px" onclick="btnMoveLeftAll_Click" />
            </div>
        </td>
        <td>
            <cc1:ucListBox ID="lstRight" runat="server" Height="320px" Width="320px">
            </cc1:ucListBox>
        </td>
    </tr>
</table>

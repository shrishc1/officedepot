﻿using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.AdminFunctions;
using BusinessLogicLayer.ModuleBAL.AdminFunctions;

public partial class MultiSelectItemCategory : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            lstLeft.Items.Add("A");
            lstLeft.Items.Add("B");
            lstLeft.Items.Add("C");
        }
    }

    private string _selectedItems = string.Empty;
    //public string SelectedItems
    //{
    //    get
    //    {
    //        _selectedItems = string.Empty;
    //        if (lstRight.Items.Count > 0)
    //        {
    //            foreach (ListItem item in lstRight.Items)
    //            {
    //                _selectedItems += "," + item.Value;
    //            }
    //            return _selectedItems.Trim(',');
    //        }
    //        return null;
    //    }
    //}

    public string SelectedItems
    {
        get
        {
            if (!string.IsNullOrEmpty(hiddenSelectedIDs.Value))
            {
                return hiddenSelectedIDs.Value.Trim(',');
            }
            return null;
        }
    }

    protected virtual void btnMoveRightAll_Click(object sender, EventArgs e)
    {
        FillControls.MoveAllItems(lstLeft, lstRight);
    }

    protected virtual void btnMoveRight_Click(object sender, EventArgs e)
    {
        if (lstLeft.SelectedItem != null)
        {
            FillControls.MoveOneItem(lstLeft, lstRight);
        }
    }

    protected virtual void btnMoveLeft_Click(object sender, EventArgs e)
    {
        if (lstRight.SelectedItem != null)
        {
            FillControls.MoveOneItem(lstRight, lstLeft);
        }
    }

    protected virtual void btnMoveLeftAll_Click(object sender, EventArgs e)
    {
        FillControls.MoveAllItems(lstRight, lstLeft);
    }
}
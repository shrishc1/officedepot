﻿using System;
using System.Collections.Generic;

using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Utilities; using WebUtilities;

namespace CommonUI.UserControls {
    public partial class ucAddButton : UserControl {
        private string navigateUrl;

        #region Public Property
        public string NavigateUrl {
            get {
                return navigateUrl;
            }
            set {
                navigateUrl = value;
            }
        }
        #endregion

        protected void btnAdd_Click(object sender, EventArgs e) {
            EncryptQueryString(this.navigateUrl);
        }

        private void EncryptQueryString(string pURL)
        {
            if (pURL.Contains("?"))
            {
                string[] urlSplit = pURL.Split('?');
                string EncriptedQueryStripg = Encryption.Encrypt(urlSplit[1]);
                Response.Redirect(urlSplit[0] + "?" + EncriptedQueryStripg);
            }
            else
            {
                Response.Redirect(pURL);
            }
        }


    }

}
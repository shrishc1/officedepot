﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using BusinessLogicLayer.ModuleBAL.Appointment.SiteSettings;
public partial class SeacrhnSelectVehicleType : System.Web.UI.UserControl
{
   

    protected void Page_Load(object sender, EventArgs e)
    {        
        if (!Page.IsPostBack) {
            lstVehicleType.Items.Clear();
            FillVehicleType();
        }
    }

    public void FillVehicleType()
    {
        MASSIT_VehicleTypeBE oMASSIT_VehicleTypeBE = new MASSIT_VehicleTypeBE();
        APPSIT_VehicleTypeBAL oMASSIT_VehicleTypeBAL = new APPSIT_VehicleTypeBAL();
        oMASSIT_VehicleTypeBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
        oMASSIT_VehicleTypeBE.User.UserID = Convert.ToInt32(Session["UserID"]);
        oMASSIT_VehicleTypeBE.Action = "SearchVehicleType";
         List<MASSIT_VehicleTypeBE> lstUPVehicleType = new List<MASSIT_VehicleTypeBE>();
        lstUPVehicleType = oMASSIT_VehicleTypeBAL.GetVehicleTypeBAL(oMASSIT_VehicleTypeBE);

        FillControls.FillListBox(ref lstVehicleType, lstUPVehicleType.ToList(), "VehicleType", "VehicleTypeID");
    }

    
    protected void btnMoveRightAll_Click(object sender, EventArgs e) {
        if (lstVehicleType.Items.Count > 0)
            FillControls.MoveAllItems(lstVehicleType, lstSelectedVehicleType);
    }
    protected void btnMoveRight_Click(object sender, EventArgs e) {
        if (lstVehicleType.SelectedItem != null)
        {
            FillControls.MoveOneItem(lstVehicleType, lstSelectedVehicleType);
        }
    }
    protected void btnMoveLeft_Click(object sender, EventArgs e) {
        if (lstSelectedVehicleType.SelectedItem != null)
        {
            FillControls.MoveOneItem(lstSelectedVehicleType, lstVehicleType);
        }
    }
    protected void btnMoveLeftAll_Click(object sender, EventArgs e) {
        if (lstSelectedVehicleType.Items.Count > 0)
        {
            FillControls.MoveAllItems(lstSelectedVehicleType, lstVehicleType);
        }
    }
    private string _selectedVehicleTypeIDs = string.Empty;
    //public string SelectedVehicleTypeIDs
    //{
    //    get
    //    {
    //        _selectedVehicleTypeIDs = string.Empty;
    //        if (lstSelectedVehicleType.Items.Count > 0)
    //        {
    //            foreach (ListItem item in lstSelectedVehicleType.Items)
    //            {
    //                _selectedVehicleTypeIDs += "," + item.Value;
    //            }
    //            return _selectedVehicleTypeIDs.Trim(',');
    //        }
    //        return null;
    //    }
    //}

    //public string SelectedVehicleTypeName
    //{
    //    get
    //    {
    //        string _selectedVehicleTypeName = string.Empty;

    //        foreach (ListItem item in lstSelectedVehicleType.Items)
    //            _selectedVehicleTypeName += item.Text + ", ";

    //        if (_selectedVehicleTypeName.Length > 0)
    //            _selectedVehicleTypeName = _selectedVehicleTypeName.Substring(0, _selectedVehicleTypeName.Length - 2);

    //        return _selectedVehicleTypeName;
    //    }
    //}

    public string SelectedVehicleTypeIDs
    {
        get
        {
            if (!string.IsNullOrEmpty(hiddenSelectedIDs.Value))
            {
                return hiddenSelectedIDs.Value.Trim(',');
            }
            return null;
        }
    }

    public string SelectedVehicleTypeName
    {
        get
        {
            string _selectedVehicleTypeName = string.Empty;

            if (!string.IsNullOrEmpty(hiddenSelectedName.Value))
            {
                _selectedVehicleTypeName = hiddenSelectedName.Value;
            }

            if (_selectedVehicleTypeName.Length > 0)
                _selectedVehicleTypeName = _selectedVehicleTypeName.Substring(0, _selectedVehicleTypeName.Length - 2);

            return _selectedVehicleTypeName;
        }
    }

    
}
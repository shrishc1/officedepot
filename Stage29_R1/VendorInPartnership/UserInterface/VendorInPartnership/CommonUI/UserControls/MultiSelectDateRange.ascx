﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MultiSelectDateRange.ascx.cs" Inherits="MultiSelectDateRange" %>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<table border="0">
    <tr>
       <td>
         <table cellpadding="0" cellspacing="0">
                                <tr>
                                    <td style="font-weight: bold; width: 6em;">
                                        <cc1:ucTextbox ID="txtFromDate"  runat="server" ReadOnly="True" ClientIDMode="Static"
                                          CssClass="date"   Width="70px" />
                                    </td>
                                    <td style="font-weight: bold; width: 4em; text-align: center">
                                        <cc1:ucLabel ID="lblTo" runat="server" Text="To"></cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold; width: 6em;">
                                        <cc1:ucTextbox ID="txtToDate" ClientIDMode="Static" runat="server" 
                                            ReadOnly="True" CssClass="date" Width="70px" />
                                    </td>
                                </tr>
                            </table>
        </td>
       
        <td align="center" class="search-controls-btn">
            <div>
                <input type="button" id="btnMoveRight" value=">" class="button" style="width: 35px" 
                    onclick="Javascript:MoveTextItem('<%= txtFromDate.ClientID %>', '<%= lstRight.ClientID %>','<%= hiddenSelectedId.ClientID %>','<%= hiddenSelectedName.ClientID %>');" />
            </div>
            &nbsp;
            <div>
                <input type="button" id="btnMoveLeft" value="<" class="button" style="width: 35px"
                    onclick="Javascript:MoveTextItemLeft('<%= lstRight.ClientID %>', '<%= txtFromDate.ClientID %>','<%= hiddenSelectedId.ClientID %>','<%= hiddenSelectedName.ClientID %>');" />
            </div>
        </td>
        <td>
            <cc1:ucListBox ID="lstRight" runat="server" Height="100px" Width="320px">
            </cc1:ucListBox>
            <asp:HiddenField ID="hiddenSelectedId" runat="server" />
            <asp:HiddenField ID="hiddenSelectedName" runat="server" />
        </td>
    </tr>
</table>

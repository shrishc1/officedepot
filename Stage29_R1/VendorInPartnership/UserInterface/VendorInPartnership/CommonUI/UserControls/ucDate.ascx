﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucDate.ascx.cs" Inherits="ucDate" %>
<script type="text/javascript">  
 Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequest);
 function EndRequest(sender, args) {
     if (args.get_error() == undefined) {
         var d = new Date();
         var objDOB = document.getElementById('<%=txtUCDate.ClientID %>');
         $(objDOB).datepicker({ showOtherMonths: true,
             selectOtherMonths: true, changeMonth: true, changeYear: true,
             minDate: new Date(2013, 0, 1),
             yearRange: '2013:+100',
             dateFormat: 'dd/mm/yy',
             firstDay: 1,
             numberOfMonths: 2,
             onSelect: function (dateText, inst) {
                 if (document.getElementById('<%=hdnAuto.ClientID %>').value == "True") {
                     document.getElementById('<%=hdnPostBackDone.ClientID%>').value = "false";
                     document.getElementById('<%=btnHidden.ClientID %>').click();
                 }
             }
         });
     }
 }
    function pageLoad() {
        $(function () {            
            var d = new Date();
            var objDOB = document.getElementById('<%=txtUCDate.ClientID %>');
            $(objDOB).datepicker({ showOtherMonths: true,
                selectOtherMonths: true, changeMonth: true, changeYear: true,
                minDate: new Date(2013, 0, 1),
                yearRange: '2013:+100',
                dateFormat: 'dd/mm/yy',
                firstDay: 1,
                numberOfMonths: 2,                
                onSelect: function (dateText, inst) {
                    if (document.getElementById('<%=hdnAuto.ClientID %>').value == "True") {
                        document.getElementById('<%=hdnPostBackDone.ClientID%>').value = "false";
                        document.getElementById('<%=btnHidden.ClientID %>').click();
                    }
                }
            });
        });
    }

    function GetFoc() {
//        if (document.getElementById('<%=hdnAuto.ClientID %>').value == "True") {
//            if (navigator.appName.indexOf("Microsoft Internet Explorer") == -1) {
//                document.getElementById('<%=hdnPostBackDone.ClientID%>').value = "false";
//                document.getElementById('<%=btnPostForm.ClientID %>').click();
//            }
//        }
    }

    function SetFoc() {
        if (document.getElementById('<%=hdnAuto.ClientID %>').value == "True") {           
            document.getElementById('<%=txtUCDate.ClientID %>').focus();
        }
    }



</script>
<input type="text" id="txtUCDate" runat="server" style="width: 67px;" readonly="readonly" onblur="GetFoc()" onclick="SetFoc()"/>
<asp:Button ID="btnHidden" runat="server" OnClick="btnHidden_Click" style="display:none;" />
<asp:Button ID="btnPostForm" runat="server" OnClick="btnPostForm_Click" style="display:none;" />
<asp:HiddenField ID="hdnAuto" runat="server" />
<asp:HiddenField ID="hdnPostBackDone" runat="server" />

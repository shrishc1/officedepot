﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucCountryISPM15.ascx.cs" Inherits="ucCountryISPM15" %>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>


<cc1:ucPanel ID="pnlCountryOld" runat="server">
    <cc1:ucDropdownList ID="ddlCountry" runat="server" Width="150px"
        OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged">
    </cc1:ucDropdownList> 
</cc1:ucPanel>
 
﻿using System;
using System.Web.UI;
using System.Collections.Generic;

using BusinessEntities.ModuleBE.AdminFunctions;
using BusinessLogicLayer.ModuleBAL.AdminFunctions;

using System.Linq;
using BaseControlLibrary;

public partial class ucCountrySite : UserControl {

    #region Public Property

   
    bool isAllRequired = false;
    public bool IsAllRequired
    {
        get
        {
            return isAllRequired;
        }
        set
        {
            isAllRequired = value;
        }
    }

    bool isAllDefault = false;
    public bool IsAllDefault {
        get {
            return isAllDefault;
        }
        set {
            isAllDefault = value;
        }
    }

    public CommonPage CurrentPage {
        get;
        set;
    }

    public ucDropdownList innerControlddlCountry {
        get {
            return this.ddlCountry;
        }
    }

    string _CountryIDs;
    public string CountryIDs {
        get { return (string)ViewState["_CountryIDs"]; }
    }
    #endregion

    protected void Page_Load(object sender, EventArgs e) {
        if (!IsPostBack) {
            if (CurrentPage.CountryPrePage_Load())
            {
                BindCountry();
                CurrentPage.CountryPost_Load();
            }
            CurrentPage.CountryCustomAction();
        }
    }
   
    #region Methods
    protected void BindCountry() {
        MAS_CountryBE oMAS_CountryBE = new MAS_CountryBE();
        MAS_CountryBAL oMAS_CountryBAL = new MAS_CountryBAL();

        oMAS_CountryBE.Action = "ShowAll";
        oMAS_CountryBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
        oMAS_CountryBE.User.UserID = Convert.ToInt32(Session["UserID"]); 

        List<MAS_CountryBE> lstCountry = new List<MAS_CountryBE>();
        lstCountry = oMAS_CountryBAL.GetCountryBAL(oMAS_CountryBE);
        lstCountry = lstCountry.Where(x => x.CountryID != 1).ToList();
        ViewState["_CountryIDs"] = string.Join(",", lstCountry.Select(c => c.CountryID).ToArray());

        if (lstCountry.Count > 0)
        {
            if (IsAllRequired)
            {
                FillControls.FillDropDown(ref ddlCountry, lstCountry.ToList(), "CountryName", "CountryID", "--All--");
            }
            else
            {
                FillControls.FillDropDown(ref ddlCountry, lstCountry.ToList(), "CountryName", "CountryID", "--Select--");
            }
        }
        if (Session["SiteCountryID"] != null) {
            if (IsAllDefault == false)               
                ddlCountry.SelectedIndex = ddlCountry.Items.IndexOf(ddlCountry.Items.FindByValue(Session["SiteCountryID"].ToString()));
        }
    }
    #endregion

    protected void ddlCountry_SelectedIndexChanged(object sender, EventArgs e) {
        CurrentPage.CountrySelectedIndexChanged();
    }
}

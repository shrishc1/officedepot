﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucVendorCountry.ascx.cs" Inherits="ucVendorCountryl" %>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<asp:UpdatePanel ID="UpdatePanelVendor" runat="server">
    <ContentTemplate>
         <table border="0">
            <tr>
                <td align="left" valign="middle">
                    <cc1:ucTextbox ID="txtVendorNo" runat="server" MaxLength="15" Width="170px"></cc1:ucTextbox>&nbsp;&nbsp;<cc1:ucButton CssClass="button" runat="server" ID="btnSearchVendor" OnClick="btnSearch_Click" />
                </td>
            </tr>            
            <tr align="left">
                <td align="left">
                    <cc1:ucListBox ID="lstLeft" runat="server" Height="150px" Width="317px">
                    </cc1:ucListBox>
                </td>
            </tr>
        </table>
    </ContentTemplate>
</asp:UpdatePanel>
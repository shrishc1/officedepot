﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MultiSelectPO.ascx.cs"
    Inherits="MultiSelectPO" %>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<script language="javascript" type="text/javascript">
 $(document).ready(function () {
        var lstitem = document.getElementById('<%= lstRight.ClientID %>');
        if (lstitem.options.length == 0) {
            document.getElementById('<%= hiddenSelectedId.ClientID %>').value = "";
            document.getElementById('<%= hiddenSelectedName.ClientID %>').value = "";
        }
    });


    function PresEnter1() {
        
        if (event.which || event.keyCode) {
            if ((event.which == 13) || (event.keyCode == 13)) {
                
                     MoveTextItem('<%= txtPurchaseOrder.ClientID %>', '<%= lstRight.ClientID %>', '<%= hiddenSelectedId.ClientID %>', '<%= hiddenSelectedName.ClientID %>');
                     return false;
                 }
             } 
            else { return true };
    }

        
</script>
    <table border="0">
        <tr>
            <td>
                <cc1:ucTextbox ID="txtPurchaseOrder" runat="server" MaxLength="100" Width="315px" onkeypress="return PresEnter1(event)" ></cc1:ucTextbox>
            </td>
            <td align="center" class="search-controls-btn">
                <div>
                    <input type="button" id="btnMoveRight" value=">" class="button" style="width: 35px" 
                        onclick="Javascript:MoveTextItem('<%= txtPurchaseOrder.ClientID %>', '<%= lstRight.ClientID %>','<%= hiddenSelectedId.ClientID %>','<%= hiddenSelectedName.ClientID %>');" />
                </div>
                &nbsp;
                <div>
                    <input type="button" id="btnMoveLeft" value="<" class="button" style="width: 35px"
                        onclick="Javascript:MoveTextItemLeft('<%= lstRight.ClientID %>', '<%= txtPurchaseOrder.ClientID %>','<%= hiddenSelectedId.ClientID %>','<%= hiddenSelectedName.ClientID %>');" />
                </div>
            </td>
            <td>
                <cc1:ucListBox ID="lstRight" runat="server" Height="100px" Width="320px">
                </cc1:ucListBox>
                <asp:HiddenField ID="hiddenSelectedId" runat="server" />
                <asp:HiddenField ID="hiddenSelectedName" runat="server" />
            </td>
        </tr>
    </table>

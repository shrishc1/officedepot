﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MultiSelectPurchaseOrders.ascx.cs"
    Inherits="MultiSelectPurchaseOrders" %>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="ucSite.ascx" TagName="ucSite" TagPrefix="cc2" %>
<%@ Register Src="~/CommonUI/UserControls/ucPurchaseOrder.ascx" TagName="ucPurchaseOrder"
    TagPrefix="cc1" %>
<script language="javascript" type="text/javascript">
    function GetUsercontrolsValue() {
       
        var siteId = '<%=ucPurchaseOrder.FindControl("hiddenSelectedSiteId").ClientID%>';
        var siteName = '<%=ucPurchaseOrder.FindControl("hiddenSelectedSiteName").ClientID%>';
        MovePurchaseOrderItem(siteId, siteName, '<%=ucPurchaseOrder.FindControl("txtPurchaseOrderNumber").ClientID %>',
                            '<%=ucPurchaseOrder.FindControl("ddlPurchaseOrderDate").ClientID%>',
                            '<%= lstRight.ClientID %>',
                            '<%= hiddenSelectedId.ClientID %>');
    }
    function RemoveItem() {        
        var lstPO = document.getElementById('<%= lstRight.ClientID %>');
        var HiddenSelectedId = document.getElementById('<%= hiddenSelectedId.ClientID %>');
        var selIndex = lstPO.selectedIndex;
        
        if (selIndex != -1) {
            if (lstPO.options[selIndex].selected) 
            {
                var SelValue = lstPO.options[selIndex].value + ',';
                lstPO.options[selIndex] = null;
                HiddenSelectedId.value = HiddenSelectedId.value.replace(SelValue, ""); // Remove from Hidden List
                
             }
        }
    }

</script>
<table border="0">
    <tr>
        <td>
            <cc1:ucPurchaseOrder ID="ucPurchaseOrder" runat="server" />
        </td>
        <td align="center" class="search-controls-btn">
            <div>
                <input type="button" id="Button1" value=">" class="button" style="width: 35px" onclick="Javascript:GetUsercontrolsValue();" />
            </div>
            &nbsp;
            <div>
                <%-- <input type="button" id="btnMoveLeft" value="<" class="button" style="width: 35px"
                            onclick="Javascript:MoveTextItemLeft('<%= lstRight.ClientID %>', '<%= txtPurchaseOrderNumber.ClientID %>','<%= hiddenSelectedId.ClientID %>','<%= hiddenSelectedName.ClientID %>');" />--%>
            </div>
        </td>
        <td>
            <cc1:ucListBox ID="lstRight" runat="server" Height="100px" Width="320px">
            </cc1:ucListBox>
            <input type="button" id="btnRemove" onclick="RemoveItem();" class="button" value="Remove Item" />
            <asp:HiddenField ID="hiddenSelectedId" runat="server" />
            <asp:HiddenField ID="hiddenSelectedName" runat="server" />
        </td>
    </tr>
</table>

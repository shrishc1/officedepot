﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.Appointment.CountrySetting;
using BusinessLogicLayer.ModuleBAL.Appointment.CountrySetting;

public partial class MultiSelectBookingType : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack) {
            BindRegionDeliveryTypeSetup();
            //MASSIT_BookingTypeExclusionBE oBookingTypeBE = new MASSIT_BookingTypeExclusionBE();
            //APPSIT_BookingTypeExclusionBAL oBookingTypeBAL = new APPSIT_BookingTypeExclusionBAL();

            //oBookingTypeBE.Action = "getBookingType";

            //List<MASSIT_BookingTypeExclusionBE> lstBookingType = new List<MASSIT_BookingTypeExclusionBE>();
            //lstBookingType = oBookingTypeBAL.GetBookingTypeBAL(oBookingTypeBE);
            //if (lstBookingType.Count > 0) 
            //{
            //    FillControls.FillListBox(ref lstLeft, lstBookingType, "BookingTypeDescription", "BookingTypeID");
            //}
        }
    }

    protected void BindRegionDeliveryTypeSetup() {
        MASCNT_DeliveryTypeBE oMASCNT_DeliveryTypeBE = new MASCNT_DeliveryTypeBE();
        APPCNT_DeliveryTypeBAL oMASCNT_DeliveryTypeBAL = new APPCNT_DeliveryTypeBAL();

        oMASCNT_DeliveryTypeBE.Action = "ShowAll";
        oMASCNT_DeliveryTypeBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
        oMASCNT_DeliveryTypeBE.User.UserID = Convert.ToInt32(Session["UserID"]);

        List<MASCNT_DeliveryTypeBE> lstDeliveryType = oMASCNT_DeliveryTypeBAL.GetDeliveryTypeDetailsBAL(oMASCNT_DeliveryTypeBE);

        if (lstDeliveryType.Count > 0) {

            FillControls.FillListBox(ref lstLeft, lstDeliveryType, "CountryDeliveryType", "DeliveryTypeID");
        }
    }
    public string SelectedDeliveryTypeIDs {
        get {
            if (!string.IsNullOrEmpty(hiddenSelectedIDs.Value)) {
                return hiddenSelectedIDs.Value.Trim(',');
            }
            return null;
        }
        set {
            hiddenSelectedIDs.Value += value + ",";
        }
    }

    public string SelectedDeliveryType {
        get {
            string _selectedDeliveryType = string.Empty;

            if (!string.IsNullOrEmpty(hiddenSelectedName.Value)) {
                _selectedDeliveryType = hiddenSelectedName.Value;
            }

            if (_selectedDeliveryType.Length > 0)
                _selectedDeliveryType = _selectedDeliveryType.Substring(0, _selectedDeliveryType.Length - 2);

            return _selectedDeliveryType;
        }

    }
}
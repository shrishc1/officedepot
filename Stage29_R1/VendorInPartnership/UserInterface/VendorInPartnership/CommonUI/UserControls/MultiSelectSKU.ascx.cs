﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.Upload;
using BusinessLogicLayer.ModuleBAL.Upload;
using Utilities; using WebUtilities;

public partial class MultiSelectSKU : UserControl {
    protected void Page_Load(object sender, EventArgs e) {
    }

  
    public string SelectedSKUName {
        get 
        {
            string _selectedSKUName = null;

            if (!string.IsNullOrEmpty(hiddenSelectedId.Value)) {
                _selectedSKUName = hiddenSelectedId.Value;
            }
            if (_selectedSKUName != null)
                if (_selectedSKUName.Length > 0)
                    _selectedSKUName = _selectedSKUName.Substring(0, _selectedSKUName.Length - 1);

            return _selectedSKUName;
        }
        set
        {
            hiddenSelectedId.Value = value+"," ;
        }
    }
    public void setODSkuOnPostBack()
    {
        string[] strSelectedName = hiddenSelectedId.Value.TrimEnd(',').TrimEnd(' ').Split(',');
        string[] strSelectedIDs = hiddenSelectedId.Value.TrimEnd(',').Split(',');

        for (int iCount = 0; iCount < strSelectedIDs.Length; iCount++)
        {
            if (!string.IsNullOrEmpty(strSelectedIDs[iCount]) && !string.IsNullOrWhiteSpace(strSelectedIDs[iCount]))
            {
                lstRight.Items.Add(new ListItem(strSelectedName[iCount].ToString().Trim(), strSelectedIDs[iCount].ToString().Trim()));
            }
        }
    }
}





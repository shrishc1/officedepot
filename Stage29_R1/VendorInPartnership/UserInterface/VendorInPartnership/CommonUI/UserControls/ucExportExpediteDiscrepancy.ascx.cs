﻿using BusinessEntities.ModuleBE.Discrepancy.LogDiscrepancy;
using BusinessLogicLayer.ModuleBAL.Discrepancy.LogDiscrepancy;
using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Utilities;

public partial class ucExportExpediteDiscrepancy : System.Web.UI.UserControl
{
    private string fileName;

    #region Public Property

    public CommonPage CurrentPage
    {
        get;
        set;
    }

    public string FileName
    {
        get
        {
            return fileName;
        }
        set
        {
            fileName = value;
        }
    }

    #endregion

    #region Events
    protected void Page_PreRender(object sender, EventArgs e)
    {
    }
    #endregion
    protected void Page_Load(object sender, EventArgs e)
    {
    }
    protected void btnExport_Click(object sender, EventArgs e)
    {
        fileName = fileName.Replace(" ", "");

        BindGrid(fileName);
    }
    private void BindGrid(string fileName)
    {
        DiscrepancyBAL oNewDiscrepancyBAL = new DiscrepancyBAL();

        if (Session["SearchCriteria"] != null)
        {
            DiscrepancyBE oNewDiscrepancyBE = (DiscrepancyBE)Session["SearchCriteria"];
            oNewDiscrepancyBE.Action = "GetDiscrepancyLogExport";
            DataTable lstDisLog = oNewDiscrepancyBAL.GetDiscrepancyLogDetailBAL(oNewDiscrepancyBE);
            gvDisLogExport.DataSource = lstDisLog;
            gvDisLogExport.DataBind();

            if (lstDisLog != null)
            {
                gvDisLogExport.DataSource = lstDisLog;
                gvDisLogExport.DataBind();
            }

            HttpContext context = HttpContext.Current;
            string date = DateTime.Now.ToString("ddMMyyyy");
            string time = DateTime.Now.ToString("HH:mm");
            context.Response.ContentType = "application/vnd.xls";

            context.Response.AddHeader("Content-Disposition", string.Format("attachment; filename={0}_{1}_{2}", fileName, date, time + ".xls"));


            string style = @"<style> .text { mso-number-format:\@;text-align:left;border:.5pt solid black;font-size:10.0pt; font-family:Calibri;} </style> ";
            string stylenum = @"<style> .textnum { mso-number-format:General;text-align:left;border:.5pt solid black;font-size:10.0pt; font-family:Calibri;} </style> ";

            context.Response.Charset = "utf-8";
            context.Response.ContentEncoding = System.Text.Encoding.GetEncoding("windows-1250");


            using (System.IO.StringWriter sw = new System.IO.StringWriter())
            {
                using (HtmlTextWriter htw = new HtmlTextWriter(sw))
                {
                    // Create a form to contain the grid
                    Table table = new Table();

                    // add the header row to the table
                    if (gvDisLogExport.HeaderRow != null)
                    {
                        //for left align
                        for (int columnIndex = 0; columnIndex < gvDisLogExport.HeaderRow.Cells.Count; columnIndex++)
                        {
                            gvDisLogExport.HeaderRow.Cells[columnIndex].Attributes.Add("class", "text");
                            gvDisLogExport.HeaderRow.Cells[columnIndex].Style.Add("background-color", "#DBDBDB");
                        }
                        PrepareControlForExport(gvDisLogExport.HeaderRow);
                        table.Rows.Add(gvDisLogExport.HeaderRow);
                    }

                    // add each of the data rows to the table
                    foreach (GridViewRow row in gvDisLogExport.Rows)
                    {
                        PrepareControlForExport(row);
                        HtmlGenericControl ht = (HtmlGenericControl)row.FindControl("div1");
                        if (ht != null)
                            ht.InnerHtml = ht.InnerHtml.Replace("<br/>", ", ");
                        for (int columnIndex = 0; columnIndex < row.Cells.Count; columnIndex++)
                        {
                            row.Cells[columnIndex].Attributes.Add("class", "text");
                        }
                        table.Rows.Add(row);
                    }

                    // add the footer row to the table
                    if (gvDisLogExport.FooterRow != null)
                    {
                        PrepareControlForExport(gvDisLogExport.FooterRow);
                        table.Rows.Add(gvDisLogExport.FooterRow);
                    }

                    // render the table into the htmlwriter
                    table.RenderControl(htw);
                    context.Response.Write(style);
                    context.Response.Write(sw.ToString());

                    context.Response.Flush(); // Sends all currently buffered output to the client.
                    context.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                    context.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                }
            }
        }
    }

    private static void PrepareControlForExport(Control control, int iRow = 0, bool isHideSecondColumn = false)
    {
        for (int i = 0; i < control.Controls.Count; i++)
        {
            Control current = control.Controls[i];

            if (current is Label)
            {
                control.Controls.Remove(current);
                control.Controls.AddAt(i, new LiteralControl((current as Label).Text));
            }
            if (current is LinkButton)
            {
                control.Controls.Remove(current);
                control.Controls.AddAt(i, new LiteralControl((current as LinkButton).Text));
            }
            else if (current is ImageButton)
            {
                control.Controls.Remove(current);
                control.Controls.AddAt(i, new LiteralControl((current as ImageButton).AlternateText));
            }
            else if (current is HyperLink)
            {
                control.Controls.Remove(current);
                control.Controls.AddAt(i, new LiteralControl((current as HyperLink).Text));
            }
            else if (current is DropDownList)
            {
                control.Controls.Remove(current);
                control.Controls.AddAt(i, new LiteralControl((current as DropDownList).SelectedItem.Text));
            }
            else if (current is CheckBox)
            {
                control.Controls.Remove(current);
                if (!isHideSecondColumn)
                {
                    control.Controls.AddAt(i, new LiteralControl((current as CheckBox).Checked ? "True" : "False"));
                }
            }
            else if (current is RadioButton)
            {
                control.Controls.Remove(current);
                if (!isHideSecondColumn)
                {
                    control.Controls.AddAt(i, new LiteralControl((current as RadioButton).Checked ? "True" : "False"));
                }
            }
            else if (current is HiddenField)
            {
                control.Controls.Remove(current);
            }
            else if (current is Button)
            {
                control.Controls.Remove(current);
            }
            else if (current is TextBox)
            {
                control.Controls.Remove(current);
                control.Controls.AddAt(i, new LiteralControl((current as TextBox).Text));
            }
            if (current.HasControls())
            {
                PrepareControlForExport(current, isHideSecondColumn: isHideSecondColumn);
            }
        }
    }

    protected void gvDisLog_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType != DataControlRowType.Header && e.Row.RowType != DataControlRowType.Footer)
        {
            HyperLink hypLinkToDisDetails = (HyperLink)e.Row.FindControl("hypLinkToDisDetails");
            HiddenField hdnDisType = (HiddenField)e.Row.FindControl("hdnDisType");
            HiddenField hdnDisLogID = (HiddenField)e.Row.FindControl("hdnDisLogID");
            HiddenField hdnUserID = (HiddenField)e.Row.FindControl("hdnUserID");
            HiddenField hdnWorkFlowID = (HiddenField)e.Row.FindControl("hdnWorkFlowID");
            string status = e.Row.Cells[1].Text.ToString();
            string VDRNo = hypLinkToDisDetails.Text;

            string TodaysDis = Convert.ToString(ViewState["TodayDiscrepancy"]);

            TodaysDis = string.IsNullOrEmpty(TodaysDis) ? "SearchDiscrepancy" : Convert.ToString(ViewState["TodayDiscrepancy"]);
        }
    }
    public string EncryptQuery(string pURL)
    {
        return Common.EncryptQuery(pURL);
    }
    public void EncryptQueryString(string pURL)
    {
        if (pURL.Contains("?"))
        {
            string[] urlSplit = pURL.Split('?');
            string EncriptedQueryStripg = Encryption.Encrypt(urlSplit[1]);
            Response.Redirect(urlSplit[0] + "?" + EncriptedQueryStripg);
        }
        else
        {
            Response.Redirect(pURL);
        }
    }
    public string EncryptURL(string pURL)
    {
        if (pURL.Contains("?"))
        {
            string[] urlSplit = pURL.Split('?');
            string EncriptedQueryStripg = Encryption.Encrypt(urlSplit[1]);
            return urlSplit[0] + "?" + EncriptedQueryStripg;
        }
        else
        {
            return pURL;
        }
    }

}
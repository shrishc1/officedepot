﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.Upload;
using BusinessLogicLayer.ModuleBAL.Upload;

public partial class CommonUI_UserControls_MultiSelectRMSCategory : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e) {
        if (!IsPostBack) {
            this.BindRMSCategory();
        }
    }

    private void BindRMSCategory() {
        MAS_RMSCategoryBE oMAS_RMSCategoryBE = new MAS_RMSCategoryBE();
        MAS_RMSCategoryBAL oMAS_RMSCategoryBAL = new MAS_RMSCategoryBAL();

        oMAS_RMSCategoryBE.Action = "ShowAll";
        
        List<MAS_RMSCategoryBE> lstRMSCategory = new List<MAS_RMSCategoryBE>();
        lstRMSCategory = oMAS_RMSCategoryBAL.GetRMSCategoryBAL(oMAS_RMSCategoryBE);

        if (lstRMSCategory.Count > 0)
            FillControls.FillListBox(ref lstLeft, lstRMSCategory, "CategoryName", "RMSCategoryID");
    }

    private string _selectedRMSCategoryIDs = string.Empty;    

    public string SelectedRMSCategoryIDs {
        get {
            if (!string.IsNullOrEmpty(hiddenSelectedIDs.Value)) {
                return hiddenSelectedIDs.Value.Trim(',');
            }
            return null;
        }
    }

    public string SelectedRMSCategoryName {
        get {
            string _selectedRMSCategoryName = string.Empty;

            if (!string.IsNullOrEmpty(hiddenSelectedName.Value)) {
                _selectedRMSCategoryName = hiddenSelectedName.Value;
            }
            if (_selectedRMSCategoryName.Length > 0)
                _selectedRMSCategoryName = _selectedRMSCategoryName.Substring(0, _selectedRMSCategoryName.Length - 2);

            return _selectedRMSCategoryName;
        }
    }

    protected virtual void btnMoveRightAll_Click(object sender, EventArgs e) {
        FillControls.MoveAllItems(lstLeft, lstRight);
    }

    protected virtual void btnMoveRight_Click(object sender, EventArgs e) {
        if (lstLeft.SelectedItem != null) {
            FillControls.MoveOneItem(lstLeft, lstRight);
        }
    }

    protected virtual void btnMoveLeft_Click(object sender, EventArgs e) {
        if (lstRight.SelectedItem != null) {
            FillControls.MoveOneItem(lstRight, lstLeft);
        }
    }

    protected virtual void btnMoveLeftAll_Click(object sender, EventArgs e) {
        FillControls.MoveAllItems(lstRight, lstLeft);
    }

    public void SetRMSCategoryOnPostBack() {
        lstRight.Items.Clear();
        string[] strSelectedName = hiddenSelectedName.Value.TrimEnd(',').TrimEnd(' ').Split(',');
        string[] strSelectedIDs = hiddenSelectedIDs.Value.TrimEnd(',').Split(',');

        if (strSelectedIDs.Length > 0)
            this.BindRMSCategory();

        for (int iCount = 0; iCount < strSelectedIDs.Length; iCount++) {
            if (!string.IsNullOrEmpty(strSelectedIDs[iCount]) && !string.IsNullOrWhiteSpace(strSelectedIDs[iCount])) {
                lstRight.Items.Add(new ListItem(strSelectedName[iCount].ToString().Trim(), strSelectedIDs[iCount].ToString().Trim()));
                RemoveRMSCategory(strSelectedIDs[iCount].ToString().Trim());
            }
        }
    }

    public void RemoveRMSCategory(string RMSCategoryId) {
        if (lstLeft.Items.FindByValue(RMSCategoryId.Trim()) != null)
            lstLeft.Items.RemoveAt(lstLeft.Items.IndexOf(lstLeft.Items.FindByValue(RMSCategoryId.Trim())));
    }
}
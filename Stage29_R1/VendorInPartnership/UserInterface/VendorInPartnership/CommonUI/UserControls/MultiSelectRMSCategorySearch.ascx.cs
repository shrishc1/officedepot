﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLogicLayer.ModuleBAL.Upload;
using BusinessEntities.ModuleBE.Upload;

public partial class MultiSelectRMSCategorySearch : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
          //  this.BindRMSCategory();
        }      
    }

    private void BindRMSCategory()
    {
        MAS_RMSCategoryBE oMAS_RMSCategoryBE = new MAS_RMSCategoryBE();
        MAS_RMSCategoryBAL oMAS_RMSCategoryBAL = new MAS_RMSCategoryBAL();

        oMAS_RMSCategoryBE.Action = "GetRmsCategory";
        if (!string.IsNullOrEmpty(txtRmsCategory.Text))
        { oMAS_RMSCategoryBE.CategoryName = txtRmsCategory.Text; }

        List<MAS_RMSCategoryBE> lstRMSCategory = new List<MAS_RMSCategoryBE>();
        lstRMSCategory = oMAS_RMSCategoryBAL.GetRMSAllCategoryBAL(oMAS_RMSCategoryBE);
      
        if (lstRMSCategory.Count > 0)
            FillControls.FillListBox(ref lstLeft, lstRMSCategory, "CategoryName", "RMSCategoryID");

        string[] strSelectedIDs = hiddenSelectedIDs.Value.TrimEnd(',').Split(',');
        for (int iCount = 0; iCount < strSelectedIDs.Length; iCount++)
        {
            if (!string.IsNullOrEmpty(strSelectedIDs[iCount]) && !string.IsNullOrWhiteSpace(strSelectedIDs[iCount]))
            {
                RemoveRMSCategory(strSelectedIDs[iCount]);
            }
        }
    }

    private string _selectedRMSCategoryIDs = string.Empty;

    public string SelectedRMSCategoryIDs
    {
        get
        {
            if (!string.IsNullOrEmpty(hiddenSelectedIDs.Value))
            {
                return hiddenSelectedIDs.Value.Trim(',');
            }
            return null;
        }
    }

    public string SelectedRMSCategoryName
    {
        get
        {
            string _selectedRMSCategoryName = string.Empty;

            if (!string.IsNullOrEmpty(hiddenSelectedName.Value))
            {
                _selectedRMSCategoryName = hiddenSelectedName.Value;
            }
            if (_selectedRMSCategoryName.Length > 0)
                _selectedRMSCategoryName = _selectedRMSCategoryName.Substring(0, _selectedRMSCategoryName.Length - 2);

            return _selectedRMSCategoryName;
        }
    }

    protected virtual void btnMoveRightAll_Click(object sender, EventArgs e)
    {
        FillControls.MoveAllItems(lstLeft, lstRight);
    }

    protected virtual void btnMoveRight_Click(object sender, EventArgs e)
    {
        if (lstLeft.SelectedItem != null)
        {
            FillControls.MoveOneItem(lstLeft, lstRight);
        }
    }

    protected virtual void btnMoveLeft_Click(object sender, EventArgs e)
    {
        if (lstRight.SelectedItem != null)
        {
            FillControls.MoveOneItem(lstRight, lstLeft);
        }
    }

    protected virtual void btnMoveLeftAll_Click(object sender, EventArgs e)
    {
        FillControls.MoveAllItems(lstRight, lstLeft);
    }

    public void SetRMSCategoryOnPostBack()
    {
        lstRight.Items.Clear();
        string[] strSelectedName = hiddenSelectedName.Value.TrimEnd(',').TrimEnd(' ').Split(',');
        string[] strSelectedIDs = hiddenSelectedIDs.Value.TrimEnd(',').Split(',');

        if (strSelectedIDs.Length > 0)
            this.BindRMSCategory();

        for (int iCount = 0; iCount < strSelectedIDs.Length; iCount++)
        {
            if (!string.IsNullOrEmpty(strSelectedIDs[iCount]) && !string.IsNullOrWhiteSpace(strSelectedIDs[iCount]))
            {
                lstRight.Items.Add(new ListItem(strSelectedName[iCount].ToString().Trim(), strSelectedIDs[iCount].ToString().Trim()));
                RemoveRMSCategory(strSelectedIDs[iCount].ToString().Trim());
            }
        }
    }

    public void RemoveRMSCategory(string RMSCategoryId)
    {
        if (lstLeft.Items.FindByValue(RMSCategoryId.Trim()) != null)
            lstLeft.Items.RemoveAt(lstLeft.Items.IndexOf(lstLeft.Items.FindByValue(RMSCategoryId.Trim())));
    }
    protected void btnRMSCategory_Click(object sender, EventArgs e)
    {
        lstLeft.Items.Clear();
        BindRMSCategory();
        ReBindRightList();
    }
    private void ReBindRightList()
    {
        lstRight.Items.Clear();
        if (!string.IsNullOrWhiteSpace(hdnListRightCategory.Value))
        {
            string[] RMSCategory = hdnListRightCategory.Value.Split('$');
            for (int index = 0; index < RMSCategory.Length; index++)
            {
                string[] RMSCategorys = RMSCategory[index].Split('#');
                ListItem listItem = new ListItem(RMSCategorys[0], RMSCategorys[1]);
                lstRight.Items.Add(listItem);
            }
            hdnListRightCategory.Value = String.Empty;
        }
    }

}
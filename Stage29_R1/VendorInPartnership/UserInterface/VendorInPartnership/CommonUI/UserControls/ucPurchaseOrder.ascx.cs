﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Utilities; using WebUtilities;
using BusinessLogicLayer.ModuleBAL.Discrepancy.LogDiscrepancy;
using BusinessEntities.ModuleBE.Discrepancy.LogDiscrepancy;
using BusinessEntities.ModuleBE.Upload;
using BusinessLogicLayer.ModuleBAL.Upload;
using BaseControlLibrary;

public partial class ucPurchaseOrder : ucUserControlBase
{

    public CommonPage CurrentPage {
        get {
            return ucSite.CurrentPage;
        }
        set {
            ucSite.CurrentPage = value;
        }
    }

    public ucUserControlBase CurrentUserControl {
        get {
            return ucSite.CurrentUserControl;
        }
        set {
            ucSite.CurrentUserControl = value;
        }
    }

    protected void Page_InIt(object sender, EventArgs e) {
        ucSite.CurrentUserControl = this;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        txtPurchaseOrderNumber.Attributes.Add("autocomplete", "off");
    }

    public override void SitePost_Load() {
        base.SitePost_Load();
        if (!IsPostBack) {
            ucSite.innerControlddlSite.SelectedIndex = 0;
            ucSite.innerControlddlSite.AutoPostBack = true;

        }
    }

    public override void SiteSelectedIndexChanged() {
        if (txtPurchaseOrderNumber.Text != string.Empty) {
            GetPurchaseOrderList();
        }

    }

    protected void ddlPurchaseOrderDate_SelectedIndexChanged(object sender, EventArgs e) {
        lblVendorValue.Text = ddlPurchaseOrderDate.SelectedItem.Value.ToString();
    }
    protected void txtPurchaseOrder_TextChanged(object sender, EventArgs e) {
        GetPurchaseOrderList();
    }
    private void GetPurchaseOrderList() {
        lblInvalidPurchaseOrder.Visible = false;
        DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
        DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();      
        oDiscrepancyBE.Action = "GetPurchaseOrderDate";
        oDiscrepancyBE.SiteID = Convert.ToInt32(ucSite.innerControlddlSite.SelectedValue);
        hiddenSelectedSiteId.Value = ucSite.innerControlddlSite.SelectedValue;
        hiddenSelectedSiteName.Value = ucSite.innerControlddlSite.SelectedItem.ToString();
        oDiscrepancyBE.PurchaseOrderNumber = txtPurchaseOrderNumber.Text.Trim();

        List<DiscrepancyBE> lstDetails = oDiscrepancyBAL.GetPurchaseOrderDateListBAL(oDiscrepancyBE);
        if (lstDetails.Count > 0 && lstDetails != null) {
            FillControls.FillDropDown(ref ddlPurchaseOrderDate, lstDetails, "PurchaseOrder_Order_raised", "VendorNoName");
            lblVendorValue.Text = ddlPurchaseOrderDate.SelectedItem.Value.ToString();

        }
        else {
            lblInvalidPurchaseOrder.Visible = true;
            lblVendorValue.Text = null;
            ddlPurchaseOrderDate.Items.Clear();
            ddlPurchaseOrderDate.Items.Insert(0, new ListItem("---Select---", "0"));
           // string InvalidPurchaseOrder = WebCommon.getGlobalResourceValue("InvalidPurchaseOrder");
            //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('" + InvalidPurchaseOrder + "')", true);
        }
    }
}
﻿using System;
using System.Web.UI.WebControls;
using System.Collections.Generic;

public partial class CommonUI_UserControls_msItemClassification : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            lstLeft.Items.Add("A");
            lstLeft.Items.Add("B");
            lstLeft.Items.Add("C");
            lstLeft.Items.Add("D");
            lstLeft.Items.Add("Other");
        }
       
    }
    public void BindClassification()
    {
        lstLeft.Items.Clear();
        lstLeft.Items.Add("A");
        lstLeft.Items.Add("B");
        lstLeft.Items.Add("C");
        lstLeft.Items.Add("D");
        lstLeft.Items.Add("Other");
    }
    private string _selectedItemClassification = string.Empty;
    //public string selectedItemClassification
    //{
    //    get
    //    {
    //        _selectedItemClassification = string.Empty;
    //        if (lstRight.Items.Count > 0)
    //        {
    //            foreach (ListItem item in lstRight.Items)
    //            {
    //                _selectedItemClassification += "," + item.Value;
    //            }
    //            return _selectedItemClassification.Trim(',');
    //        }
    //        return null;
    //    }
    //}

    public string selectedItemClassification
    {
        get
        {
            if (!string.IsNullOrEmpty(hiddenSelectedIDs.Value))
            {
                return hiddenSelectedIDs.Value.Trim(',');
            }
            return null;
        }
    }

    

    protected virtual void btnMoveRightAll_Click(object sender, EventArgs e)
    {
        FillControls.MoveAllItems(lstLeft, lstRight);
    }

    protected virtual void btnMoveRight_Click(object sender, EventArgs e)
    {
        if (lstLeft.SelectedItem != null)
        {
            FillControls.MoveOneItem(lstLeft, lstRight);
        }
    }

    protected virtual void btnMoveLeft_Click(object sender, EventArgs e)
    {
        if (lstRight.SelectedItem != null)
        {
            FillControls.MoveOneItem(lstRight, lstLeft);
        }
    }

    protected virtual void btnMoveLeftAll_Click(object sender, EventArgs e)
    {
        FillControls.MoveAllItems(lstRight, lstLeft);
    }
    public void setItemClassificationOnPostBack()
    {
        lstRight.Items.Clear();
        string[] strSelectedName = hiddenSelectedName.Value.Split(',');
        string[] strSelectedIDs = hiddenSelectedIDs.Value.Replace(",,", ",").Replace(",,", "").Split(',');
        for (int iCount = 0; iCount < strSelectedIDs.Length; iCount++)
        {
            if (!string.IsNullOrEmpty(strSelectedIDs[iCount]) && !string.IsNullOrWhiteSpace(strSelectedIDs[iCount]) && (strSelectedName.Length == strSelectedIDs.Length))
            {
                lstRight.Items.Add(new ListItem(strSelectedName[iCount].ToString().Trim(), strSelectedIDs[iCount].ToString().Trim()));
                lstLeft.Items.Remove(new ListItem(strSelectedName[iCount].ToString().Trim(), strSelectedIDs[iCount].ToString().Trim()));
            }
        }
    }
}
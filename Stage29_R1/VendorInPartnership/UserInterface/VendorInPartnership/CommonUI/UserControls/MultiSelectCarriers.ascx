﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MultiSelectCarriers.ascx.cs" Inherits="MultiSelectCarriers" %>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/ucCarrier.ascx" TagName="ucCarrier" TagPrefix="cc2" %>
<script language="javascript" type="text/javascript">
    function SetCarrierHiddenFields(ctrlLstRight, ctrlHiddenSelectedId, ctrlHiddenSelectedName) {        
        var HiddenSelectedId = document.getElementById(ctrlHiddenSelectedId);
        var HiddenSelectedName = document.getElementById(ctrlHiddenSelectedName);
        var lstitem = document.getElementById(ctrlLstRight);
        if (lstitem != null) {
            HiddenSelectedId.value = '';
            HiddenSelectedName.value = '';
            for (var index = 0; index < lstitem.options.length; index++) {
                var theOption = new Option;
                if (index == 0) {
                    HiddenSelectedId.value = lstitem.options[index].value;
                    HiddenSelectedName.value = lstitem.options[index].text;
                }
                else {
                    HiddenSelectedId.value = HiddenSelectedId.value + ',' + lstitem.options[index].value;
                    HiddenSelectedName.value = HiddenSelectedName.value + ',' + lstitem.options[index].text;
                }
            }
        }
    }
</script>
<table>
    <tr>
        <td>
            <cc2:ucCarrier ID="ucCarrier" runat="server" />
        </td>
        <td align="center" class="search-controls-btn">
            <div>
                <input type="button" id="Button1" value=">>" class="button" style="width: 35px" onclick="Javascript:MoveAll('<%=ucCarrier.FindControl("lstLeft").ClientID %>','<%= lstRight.ClientID %>', '<%= hiddenSelectedIDs.ClientID %>','<%= hiddenSelectedName.ClientID %>');SetCarrierHiddenFields('<%= lstRight.ClientID %>','<%= hiddenSelectedIDs.ClientID %>','<%= hiddenSelectedName.ClientID %>');" />
            </div>
            &nbsp;
            <div>
                <input type="button" id="btnMoveRight" value=">" class="button" style="width: 35px"
                    onclick="Javascript:MoveItem('<%=ucCarrier.FindControl("lstLeft").ClientID %>', '<%= lstRight.ClientID %>','<%= hiddenSelectedIDs.ClientID %>','<%= hiddenSelectedName.ClientID %>');SetCarrierHiddenFields('<%= lstRight.ClientID %>','<%= hiddenSelectedIDs.ClientID %>','<%= hiddenSelectedName.ClientID %>');" />
            </div>
            &nbsp;
            <div>
                <input type="button" id="btnMoveLeft" value="<" class="button" style="width: 35px"
                    onclick="Javascript:MoveItem('<%= lstRight.ClientID %>', '<%=ucCarrier.FindControl("lstLeft").ClientID %>','<%= hiddenSelectedIDs.ClientID %>','<%= hiddenSelectedName.ClientID %>');SetCarrierHiddenFields('<%= lstRight.ClientID %>','<%= hiddenSelectedIDs.ClientID %>','<%= hiddenSelectedName.ClientID %>');" />
            </div>
            &nbsp;
            <div>
                <input type="button" id="btnMoveLeftAll" value="<<" class="button" style="width: 35px"
                    onclick="Javascript:MoveAll('<%= lstRight.ClientID %>', '<%=ucCarrier.FindControl("lstLeft").ClientID %>','<%= hiddenSelectedIDs.ClientID %>','<%= hiddenSelectedName.ClientID %>');SetCarrierHiddenFields('<%= lstRight.ClientID %>','<%= hiddenSelectedIDs.ClientID %>','<%= hiddenSelectedName.ClientID %>');" />
            </div>
        </td>
        <td style="padding-top:27px;">
            <cc1:ucListBox ID="lstRight" runat="server" Height="150px" Width="320px">
            </cc1:ucListBox>
            <asp:HiddenField ID="hiddenSelectedIDs" runat="server" />
            <asp:HiddenField ID="hiddenSelectedName" runat="server" />
        </td>
    </tr>
</table>


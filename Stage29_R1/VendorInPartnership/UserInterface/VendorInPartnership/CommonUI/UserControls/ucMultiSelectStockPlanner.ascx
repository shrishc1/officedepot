﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucMultiSelectStockPlanner.ascx.cs" Inherits="ucMultiSelectStockPlanner" %>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%--<asp:UpdatePanel ID="UpdatePanelVendor" runat="server">
    <ContentTemplate>--%>
        <script language="javascript" type="text/javascript">
            $(document).ready(function () {
                var lstitem = document.getElementById('<%= ucLBRight.ClientID %>');
                if (lstitem.options.length == 0) {
                    document.getElementById('<%= hiddenSelectedIDs.ClientID %>').value = "";
                    document.getElementById('<%= hiddenSelectedName.ClientID %>').value = "";
                }
            });

            function ReBindListValue() {
                var varLstRightUser = document.getElementById('<%=ucLBRight.ClientID%>');
                var varHdnListRight = $("input[id*=hdnListRightUser]");
                $("input[id*=hdnListRightUser]").val('');
                if (varLstRightUser != null) {
                    for (var i = 0; i < varLstRightUser.options.length; i++) {
                        if ($("input[id*=hdnListRightUser]").val() == '') {
                            $("input[id*=hdnListRightUser]").val(varLstRightUser.options[i].text + '#'
                        + varLstRightUser.options[i].value);
                        }
                        else {
                            $("input[id*=hdnListRightUser]").val($("input[id*=hdnListRightUser]").val() + '$' + varLstRightUser.options[i].text + '#'
                        + varLstRightUser.options[i].value);
                        }
                    }
                }                
            }
        </script>
        <table>
            <tr>
                <td colspan="3" align="left">
                      <asp:UpdatePanel ID="UpdatePanelVendor" runat="server" >
                   <ContentTemplate>
                    <cc1:ucTextbox ID="txtUserName" runat="server" MaxLength="15" Width="170px"></cc1:ucTextbox>
                    &nbsp;
                    <cc1:ucButton CssClass="button" runat="server" ID="btnSearchStockPlanner" OnClientClick="ReBindListValue();"
                        OnClick="btnSearchUser_Click" />
                    <asp:HiddenField ID="hdnListRightUser" runat="server" />
                       </ContentTemplate>
                   </asp:UpdatePanel>
                </td>
            </tr>
            <tr>
                <td>
                     <asp:UpdatePanel ID="UpdatePanel1" runat="server" >
                   <ContentTemplate>
                    <cc1:ucListBox ID="ucLBLeft" runat="server" Height="100px" Width="320px">
                    </cc1:ucListBox>
                         </ContentTemplate>
                   </asp:UpdatePanel>
                </td>
                <td align="center" class="search-controls-btn">
                    <%--<div>
                <input type="button" id="btnMoveRightAll" value=">>" class="button" style="width: 35px"
                    onclick="Javascript:MoveAll('<%= ucLBLeft.ClientID %>', '<%= ucLBRight.ClientID %>','<%= hiddenSelectedIDs.ClientID %>','<%= hiddenSelectedName.ClientID %>');" />
            </div>
            &nbsp;--%>
                    <div>
                        <input type="button" id="btnMoveRight" value=">" class="button" style="width: 35px"
                            onclick="Javascript:MoveItem('<%= ucLBLeft.ClientID %>', '<%= ucLBRight.ClientID %>','<%= hiddenSelectedIDs.ClientID %>','<%= hiddenSelectedName.ClientID %>');" />
                    </div>
                    &nbsp;
                    <div>
                        <input type="button" id="btnMoveLeft" value="<" class="button" style="width: 35px"
                            onclick="Javascript:MoveItem('<%= ucLBRight.ClientID %>', '<%= ucLBLeft.ClientID %>','<%= hiddenSelectedIDs.ClientID %>','<%= hiddenSelectedName.ClientID %>');" />
                    </div>
                    <%--  &nbsp;
            <div>
                <input type="button" id="btnMoveLeftAll" value="<<" class="button" style="width: 35px"
                    onclick="Javascript:MoveAll('<%= ucLBRight.ClientID %>', '<%= ucLBLeft.ClientID %>','<%= hiddenSelectedIDs.ClientID %>','<%= hiddenSelectedName.ClientID %>');" />
            </div>--%>
                </td>
                <td>
                    <cc1:ucListBox ID="ucLBRight" runat="server" Height="100px" Width="320px">
                    </cc1:ucListBox>
                    <asp:HiddenField ID="hiddenSelectedIDs" runat="server" />
                    <asp:HiddenField ID="hiddenSelectedName" runat="server" />
                </td>
            </tr>
        </table>
<%--</ContentTemplate>
</asp:UpdatePanel>--%>
﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebUtilities;

public partial class ucExportToExcel : UserControl
{
    private GridView gridViewControl;
    private string fileName;
    private bool isHideHidden = false;
    private bool isExportUsersOverview = false;
    private bool isAutoGenratedGridview = false;
    private bool isHideSecondColumn = false;

    #region Public Property

    public GridView GridViewControl
    {
        get
        {
            return gridViewControl;
        }
        set
        {
            gridViewControl = value;
        }
    }

    public string FileName
    {
        get
        {
            return fileName;
        }
        set
        {
            fileName = value;
        }
    }

    public bool IsHideHidden
    {
        get
        {
            return isHideHidden;
        }
        set
        {
            isHideHidden = value;
        }
    }

    public bool IsExportUsersOverview
    {
        get
        {
            return isExportUsersOverview;
        }
        set
        {
            isExportUsersOverview = value;
        }
    }

    public bool IsAutoGenratedGridview
    {
        get
        {
            return isAutoGenratedGridview;
        }
        set
        {
            isAutoGenratedGridview = value;
        }
    }

    public CommonPage CurrentPage
    {
        get;
        set;
    }

    private int[] textColNumber;

    public int[] TextColNumber
    {
        get { return textColNumber; }
        set { textColNumber = value; }
    }

    #endregion Public Property

    #region Methods

    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (IsHideHidden)
        {
            this.btnExportToExcel.Text = string.Empty;
            this.btnExportToExcel.Width = Unit.Pixel(18);
        }
    }

    #endregion Methods

    protected void btnExport_Click(object sender, EventArgs e)
    {
        try
        {
            if (CurrentPage != null && !CurrentPage.PreExportToExcel())
                return;

            fileName = fileName.Replace(" ", "");
            if (isExportUsersOverview)
            {
                WebCommon.ExportOnlyUsersOverview(fileName, gridViewControl);
                this.btnExportToExcel.Text = string.Empty;
            }
            else
            {
                WebCommon.ExportHideHidden(fileName, gridViewControl, textColNumber, IsHideHidden, isAutoGenratedGridview, isHideSecondColumn);
            }
        }
        catch
        {
            throw;
        }
    }
}
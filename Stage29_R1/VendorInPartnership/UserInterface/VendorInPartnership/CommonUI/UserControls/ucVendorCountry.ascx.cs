﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using BusinessLogicLayer.ModuleBAL.Appointment.SiteSettings;
using Utilities;
using WebUtilities;
public partial class ucVendorCountryl : System.Web.UI.UserControl
{
    public event EventHandler btnSearchClick;

    private int searchCountryID;
    public int SearchCountryID
    {
        get { return searchCountryID; }
        set { searchCountryID = value; }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        { }
        btnSearchVendor.Text = WebCommon.getGlobalResourceValue("SearchVendor");
    }
    public int MasterParentID
    {
        get
        {
            return Convert.ToInt32(ViewState["MasterParentID"]);
        }
        set { ViewState["MasterParentID"] = value; }
    }

    public string FunctionCalled
    {
        get { return Convert.ToString(ViewState["FunctionCalled"]); }
        set { ViewState["FunctionCalled"] = value; }
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        btnSearchClick(sender, e);
        UserControl ucCountory = (UserControl)this.Parent.Parent.FindControl("ddlCountry");
        DropDownList ddlCountry = (DropDownList)ucCountory.FindControl("ddlCountry");
        int CountryId = Convert.ToInt32(ddlCountry.SelectedValue);
        BindVendor(CountryId);
    }

    private void BindVendor(int CountryId)
    {
        UP_VendorBAL oUP_VendorBAL = new UP_VendorBAL();
        UP_VendorBE oUP_VendorBE = new UP_VendorBE();

        oUP_VendorBE.Action = "GetVendorByName";
        oUP_VendorBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
        oUP_VendorBE.User.UserID = Convert.ToInt32(Session["Userid"]);
        oUP_VendorBE.VendorName = txtVendorNo.Text;
        if (CountryId != 0 && CountryId != (-1))
            oUP_VendorBE.CountryID = CountryId;

        List<UP_VendorBE> lstUPVendor = new List<UP_VendorBE>();
        lstUPVendor = oUP_VendorBAL.GetVendorByUserCountryBAL(oUP_VendorBE);
        if (lstUPVendor.Count > 0)
        {
            FillControls.FillListBox(ref lstLeft, lstUPVendor, "VendorName", "VendorID");

            /// --- Added for Showing Inative Vendors --- 27 Feb 2013
            //foreach (ListItem item in lstLeft.Items)
            //{
            //    for (int i = 0; i < lstUPVendor.Count; i++)
            //    {
            //        if (item.Value == lstUPVendor[i].VendorID.ToString() && lstUPVendor[i].IsActiveVendor == "N")
            //        {
            //            this.lstLeft.Items[i].Attributes.Add("Style", "background-color:RED;");
            //        }
            //    }
            //}

            for (int i = 0; i < lstUPVendor.Count; i++)
            {
                if (lstUPVendor[i].VendorID > 0)
                    if (Convert.ToInt32(lstLeft.Items[i].Value) == lstUPVendor[i].VendorID && lstUPVendor[i].IsActiveVendor == "N")
                        this.lstLeft.Items[i].Attributes.Add("Style", "background-color:RED;");
            }
            /// --- Added for Showing Inative Vendors --- 27 Feb 2013

        }
        else
        {
            lstLeft.Items.Clear();
        }
    }


    private void BindVendorByVendorId()
    {
        UP_VendorBAL oUP_VendorBAL = new UP_VendorBAL();
        UP_VendorBE oUP_VendorBE = new UP_VendorBE();

        oUP_VendorBE.Action = "GetAllLevelVendorDetailsByUserID";
        oUP_VendorBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
        oUP_VendorBE.User.UserID = Convert.ToInt32(Session["UserID"]);
        oUP_VendorBE.Site = new BusinessEntities.ModuleBE.AdminFunctions.MAS_SiteBE();
        oUP_VendorBE.Site.SiteID = 0;
        oUP_VendorBE.VendorName = txtVendorNo.Text;

        List<UP_VendorBE> lstUPVendor = new List<UP_VendorBE>();
        lstUPVendor = oUP_VendorBAL.GetVendorByUserIdBAL(oUP_VendorBE);

        //Stage 7 V2 Point 12g------------//
        List<UP_VendorBE> lstUPVendorName = new List<UP_VendorBE>();
        if (lstUPVendor != null && lstUPVendor.Count > 0)
        {

            lstUPVendorName = lstUPVendor.FindAll(delegate(UP_VendorBE v) { return Convert.ToString(v.VendorFlag) == "G"; });  //.Where(v => Convert.ToString(v.VendorFlag) == "G").ToList();           //
            if (lstUPVendorName != null && lstUPVendorName.Count > 0)
                MasterParentID = lstUPVendorName[0].VendorID;
            else
            {
                lstUPVendorName = lstUPVendor.FindAll(delegate(UP_VendorBE v) { return Convert.ToString(v.VendorFlag).Equals("P") && v.ParentVendorID == -1; });
                if (lstUPVendorName != null && lstUPVendorName.Count > 0)
                    MasterParentID = lstUPVendorName[0].VendorID;
                else
                {
                    lstUPVendorName = lstUPVendor.FindAll(delegate(UP_VendorBE v) { return v.VendorFlag == (char?)null && v.ParentVendorID == -1; });
                    if (lstUPVendorName != null && lstUPVendorName.Count > 0)
                        MasterParentID = lstUPVendorName[0].VendorID;
                }
            }
        }
        FillControls.FillListBox(ref lstLeft, lstUPVendor, "VendorName", "VendorID");
    }



    public void RemoveVendor(string VendorName, string VendorId)
    {
        if (lstLeft.Items.FindByValue(VendorId.Trim()) != null)
            lstLeft.Items.RemoveAt(lstLeft.Items.IndexOf(lstLeft.Items.FindByValue(VendorId.Trim())));
    }

    public void SearchVendorClick(string vendorNo)
    {
        txtVendorNo.Text = vendorNo;
       // BindVendor();
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebUtilities;
using BusinessEntities.ModuleBE.StockOverview;
using BusinessLogicLayer.ModuleBAL.StockOverview;
using System.Collections;
using Utilities;

public partial class ucExportExpediteTrackingReport : System.Web.UI.UserControl
{
    private string fileName;

    #region Public Property

    public CommonPage CurrentPage
    {
        get;
        set;
    }

    public string FileName
    {
        get
        {
            return fileName;
        }
        set
        {
            fileName = value;
        }
    }

    #endregion

    #region Events

    protected void Page_PreRender(object sender, EventArgs e)
    {
        //this.btnExportToExcel.Text = string.Empty;
        //this.btnExportToExcel.Width = Unit.Pixel(18);
    }

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnExport_Click(object sender, EventArgs e)
    {
        fileName = fileName.Replace(" ", "");
        this.BindGridFromSessionSearchCriteria();
        WebCommon.Export(fileName, gvTrackingReportExportExcel);
    }

    private void BindGridFromSessionSearchCriteria()
    {
        ExpediteStockBE oExpediteStockBE = new ExpediteStockBE();
        ExpediteStockBAL oExpediteStockBAL = new ExpediteStockBAL();
        if (Session["ExpediteTrackingReport"] != null)
        {
            Hashtable htExpediteTrackingReport = (Hashtable)Session["ExpediteTrackingReport"];
            oExpediteStockBE.SelectedStockPlannerIDs = (htExpediteTrackingReport.ContainsKey("SelectedStockPlannerIDs") && htExpediteTrackingReport["SelectedStockPlannerIDs"] != null) ? htExpediteTrackingReport["SelectedStockPlannerIDs"].ToString() : "";
            oExpediteStockBE.SelectedIncludedVendorIDs = (htExpediteTrackingReport.ContainsKey("SelectedIncludedVendorIDs") && htExpediteTrackingReport["SelectedIncludedVendorIDs"] != null) ? htExpediteTrackingReport["SelectedIncludedVendorIDs"].ToString() : "";
            oExpediteStockBE.SelectedExcludedVendorIDs = (htExpediteTrackingReport.ContainsKey("SelectedExcludedVendorIDs") && htExpediteTrackingReport["SelectedExcludedVendorIDs"] != null) ? htExpediteTrackingReport["SelectedExcludedVendorIDs"].ToString() : "";
            oExpediteStockBE.SelectedSiteIDs = (htExpediteTrackingReport.ContainsKey("SelectedSiteIDs") && htExpediteTrackingReport["SelectedSiteIDs"] != null) ? htExpediteTrackingReport["SelectedSiteIDs"].ToString() : "";
            oExpediteStockBE.SelectedItemClassification = (htExpediteTrackingReport.ContainsKey("SelectedItemClassification") && htExpediteTrackingReport["SelectedItemClassification"] != null) ? htExpediteTrackingReport["SelectedItemClassification"].ToString() : "";
            oExpediteStockBE.SelectedSkuType = (htExpediteTrackingReport.ContainsKey("SelectedSkuType") && htExpediteTrackingReport["SelectedSkuType"] != null) ? Convert.ToInt32(htExpediteTrackingReport["SelectedSkuType"].ToString()) : 0;
            oExpediteStockBE.SelectedDisplay = (htExpediteTrackingReport.ContainsKey("SelectedDisplay") && htExpediteTrackingReport["SelectedDisplay"] != null) ? Convert.ToInt32(htExpediteTrackingReport["SelectedDisplay"].ToString()) : 0;
            oExpediteStockBE.SelectedUpdate = (htExpediteTrackingReport.ContainsKey("SelectedUpdate") && htExpediteTrackingReport["SelectedUpdate"] != null) ? Convert.ToInt32(htExpediteTrackingReport["SelectedUpdate"].ToString()) : 0;
            gvTrackingReportExportExcel.PageIndex = Convert.ToInt32(htExpediteTrackingReport["PageIndex"].ToString());            
            oExpediteStockBE.Action = "GetTrackingReportDataExcel";

            var lstExpediteStock = oExpediteStockBAL.GetPotentialOutputBAL(oExpediteStockBE);
            /* Filtering the Update logic on list collection.*/
            //var lastFirstWorkingDate = GetPastWorkingDate(DateTime.Now, 1);
            //var lastSecondWorkingDate = GetPastWorkingDate(DateTime.Now, 2);
            var lastThirdWorkingDate = GetPastWorkingDate(DateTime.Now, 3);
            var dateBeforThreeDays = DateTime.Now.AddDays(-3);
            switch (oExpediteStockBE.SelectedUpdate)
            {
                case 1:
                    break;
                case 2:
                    lstExpediteStock = lstExpediteStock.FindAll(es => es.CommentDateLastUpdated < lastThirdWorkingDate || es.CommentDateLastUpdated == null);
                    break;
                case 3:
                    lstExpediteStock = lstExpediteStock.FindAll(es => es.CommentUpdateValidTo > DateTime.Now);
                    break;
            }

            if (lstExpediteStock != null && lstExpediteStock.Count > 0)
                gvTrackingReportExportExcel.DataSource = lstExpediteStock;
            else
                gvTrackingReportExportExcel.DataSource = null;

            gvTrackingReportExportExcel.DataBind();
        }
    }

    private DateTime GetPastWorkingDate(DateTime datetime, int pastday)
    {
        var dt = datetime.AddDays(-pastday);
        if (Convert.ToString(dt.DayOfWeek).Equals("Saturday"))
            dt = dt.AddDays(-1);
        if (Convert.ToString(dt.DayOfWeek).Equals("Sunday"))
            dt = dt.AddDays(-2);

        return dt;
    }

    private DateTime GetFutureWorkingDate(DateTime datetime, int pastday)
    {
        var dt = datetime.AddDays(pastday);
        if (Convert.ToString(dt.DayOfWeek).Equals("Saturday"))
            dt = dt.AddDays(1);
        if (Convert.ToString(dt.DayOfWeek).Equals("Sunday"))
            dt = dt.AddDays(2);

        return dt;
    }

    public string EncryptQuery(string pURL)
    {
        return Common.EncryptQuery(pURL);
    }

    public void EncryptQueryString(string pURL)
    {
        if (pURL.Contains("?"))
        {
            string[] urlSplit = pURL.Split('?');
            string EncriptedQueryStripg = Encryption.Encrypt(urlSplit[1]);
            Response.Redirect(urlSplit[0] + "?" + EncriptedQueryStripg);
        }
        else
        {
            Response.Redirect(pURL);
        }
    }

    public string EncryptURL(string pURL)
    {
        if (pURL.Contains("?"))
        {
            string[] urlSplit = pURL.Split('?');
            string EncriptedQueryStripg = Encryption.Encrypt(urlSplit[1]);
            return urlSplit[0] + "?" + EncriptedQueryStripg;
        }
        else
        {
            return pURL;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.AdminFunctions;
using BusinessLogicLayer.ModuleBAL.AdminFunctions;
using System.Linq;
using BusinessEntities.ModuleBE.Discrepancy.Report;

public partial class MultiSelectDiscType : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            #region Logic to bind the lstleft ...
            lstLeft.Items.Add("Overs");
            lstLeft.Items.Add("Shortage");
            lstLeft.Items.Add("Goods Received Damaged");
            lstLeft.Items.Add("No Purchase Order");
            lstLeft.Items.Add("No Paperwork");
            lstLeft.Items.Add("Incorrect Product");
            lstLeft.Items.Add("Presentation Issue");
            lstLeft.Items.Add("Incorrect Address");
            lstLeft.Items.Add("Paperwork Amended");
            lstLeft.Items.Add("Wrong Pack Size");
            lstLeft.Items.Add("Fail Pallet Specification");
            lstLeft.Items.Add("Quality Issue");
            lstLeft.Items.Add("Premature Invoice Receipt");
            lstLeft.Items.Add("Generic Discrepancy");
            lstLeft.Items.Add("Out Of Date");
            #endregion
        }
    }


    private string _selectedSiteIDs = string.Empty;
    //public string SelectedSiteIDs
    //{
    //    get
    //    {
    //        _selectedSiteIDs = string.Empty;
    //        if (lstRight.Items.Count > 0)
    //        {
    //            foreach (ListItem item in lstRight.Items)
    //            {
    //                _selectedSiteIDs += "," + item.Value;
    //            }
    //            return _selectedSiteIDs.Trim(',');
    //        }
    //        return null;
    //    }
    //}

    //public string SelectedSiteName
    //{
    //    get
    //    {
    //        string _selectedSiteName = string.Empty;

    //        foreach (ListItem item in lstRight.Items)
    //            _selectedSiteName += item.Text + ", ";

    //        if (_selectedSiteName.Length > 0)
    //            _selectedSiteName = _selectedSiteName.Substring(0, _selectedSiteName.Length - 2);

    //        return _selectedSiteName;
    //    }
    //}
    public bool RequiredUserDefaultSite = true;
    //public string SelectedSiteIDs
    //{
    //    get
    //    {
    //        if (!string.IsNullOrEmpty(hiddenSelectedIDs.Value))
    //        {
    //            return hiddenSelectedIDs.Value.Trim(',');
    //        }
    //        else if (!string.IsNullOrEmpty(hiddenUserSiteIDs.Value) && RequiredUserDefaultSite == false)
    //        {
    //            return hiddenUserSiteIDs.Value;
    //        }
    //        return null;
    //    }
    //    set
    //    {
    //        hiddenSelectedIDs.Value += value + ",";
    //    }
    //}

    public string SelectedTypes
    {
        get
        {
            string _selectedSiteName = string.Empty;

            if (!string.IsNullOrEmpty(hiddenSelectedName.Value))
            {
                _selectedSiteName = hiddenSelectedName.Value;
            }

            if (_selectedSiteName.Length > 0)
                _selectedSiteName = _selectedSiteName.Substring(0, _selectedSiteName.Length - 2);

            return _selectedSiteName;
        }
    }

    protected virtual void btnMoveRightAll_Click(object sender, EventArgs e)
    {
        FillControls.MoveAllItems(lstLeft, lstRight);
    }

    protected virtual void btnMoveRight_Click(object sender, EventArgs e)
    {
        if (lstLeft.SelectedItem != null)
        {
            FillControls.MoveOneItem(lstLeft, lstRight);
        }
    }

    protected virtual void btnMoveLeft_Click(object sender, EventArgs e)
    {
        if (lstRight.SelectedItem != null)
        {
            FillControls.MoveOneItem(lstRight, lstLeft);
        }
    }

    protected virtual void btnMoveLeftAll_Click(object sender, EventArgs e)
    {
        FillControls.MoveAllItems(lstRight, lstLeft);
    }
}
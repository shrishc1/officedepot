﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucSchedulingDate.ascx.cs"
    Inherits="ucSchedulingDate" %>
<style type="text/css">
  
  td.red span.ui-state-default, span.ui-widget-content span.ui-state-default, span.ui-widget-header span.ui-state-default
    {
        border: 1px  #ff6600 solid !important;padding: 1px 0 1px 1px !important;background: #f4be9b !important;overflow:hidden;color:Black;
    }
 
  td.red a span.ui-state-default, span.ui-widget-content span.ui-state-default, span.ui-widget-header span.ui-state-default
    {
       background:  #ff6600 url(images/bg.png) 50% 50% repeat-x !important;  border: 1px  #ff6600 solid !important;
    }
     
    td.gray span.ui-state-default, span.ui-widget-content span.ui-state-default, span.ui-widget-header span.ui-state-default
    {
        color:White;
        background: Gray;
        border: 1px  black solid !important;
    }


        
td.highlight {border: none !important;padding: 1px 0 1px 1px !important;background: none !important;overflow:hidden;}
td.highlight a {background:  #f0ddd1 url(images/bg.png) 50% 50% repeat-x !important;  border: 1px  #f4bf9b solid !important;}

td.highlightgray {border: none !important;padding: 1px 0 1px 1px !important;background: none !important;overflow:hidden;}
td.highlightgray a {background:lightgray url(images/bg.png) 50% 50% repeat-x !important;  border: 1px  gray solid !important;}


td.actday span.ui-state-default, .ui-widget-content .ui-state-default, .ui-widget-header .ui-state-default {
    border: 1px solid #cccccc;
    background: #f6f6f6 url(images/ui-bg_glass_100_f6f6f6_1x400.png) 50% 50% repeat-x;
    font-weight: bold;
    color: #1c94c4;
}
    
</style>
<script type="text/javascript">


    var disabledDays = [];  // = ["12-21-2015", "12-24-2015", "12-27-2015", "12-28-2015", "1-13-2016"];
    var siteCloser = [];  // = ["12-25-2015", "12-31-2015", "1-1-2016"];
    var CloserReason = [];

    var NoticePeriod = [];  // = ["12-25-2015", "12-31-2015", "1-1-2016"];


    /* utility functions */
    function nationalDays(date) {
        var m = date.getMonth(), d = date.getDate(), y = date.getFullYear();

        var todaydate = new Date();
        if (new Date(y, m, d) < new Date(todaydate.getFullYear(), todaydate.getMonth(), todaydate.getDate())) {
            if (document.getElementById('<%=hdndispablePast.ClientID%>').value == "true") {
                return [false, 'gray', 'past day'];
            }
            else {
                return [true, 'highlightgray', 'past day'];
            }
        }


        var indx = parseInt($.inArray((m + 1) + '-' + d + '-' + y, siteCloser));

        if ($.inArray((m + 1) + '-' + d + '-' + y, siteCloser) != -1) {
            if (document.getElementById('<%=hdndispablePast.ClientID%>').value == "true") {
                return [false, 'gray', CloserReason[indx]];
            } else {
                return [true, 'highlightgray', CloserReason[indx]];
            }
        }


        if ($.inArray((m + 1) + '-' + d + '-' + y, NoticePeriod) != -1) {
            if (document.getElementById('<%=hdndispablePast.ClientID%>').value == "true") {
                return [false, 'gray', 'Insufficient Booking Notice'];
            } else {
                return [true, 'highlightgray', 'Insufficient Booking Notice'];
            }
        }


        if ($.inArray((m + 1) + '-' + d + '-' + y, disabledDays) != -1) {
            if (document.getElementById('<%=hdndispablePast.ClientID%>').value == "true") {
                return [false, 'red', 'No capacity'];
            } else {
                return [true, 'highlight', 'No capacity'];
            }
        }


        return [true, 'actday', 'Available'];
    }

    function noWeekendsOrHolidays(date) {

        var noWeekend = jQuery.datepicker.noWeekends(date);
        return noWeekend[0] ? nationalDays(date) : noWeekend;
    }

    var objDOB, datestr, selyear, selmonth;

    function pageLoad() {
        $(function () {

            objDOB = document.getElementById('<%=txtUCDate.ClientID %>');

            if (objDOB.value != '') {
                datestr = objDOB.value.split('/');
                selyear = datestr[2];
                selmonth = datestr[1];
            }
            else {
                selyear = 0;
                selmonth = 0;
            }

            $(objDOB).datepicker({ showOtherMonths: true,
                selectOtherMonths: true, changeMonth: true, changeYear: true,
                minDate: new Date(2013, 0, 1),
                yearRange: '2013:+100',
                dateFormat: 'dd/mm/yy',
                firstDay: 1,
                numberOfMonths: 2,
                showOtherMonths: true,

                beforeShowDay: noWeekendsOrHolidays,

                beforeShow: function (event, ui) {
                    setTimeout(function () {
                        $('#ui-datepicker-div').append('<span><table border="0"><tr><td style="width:14%;">Available</td><td style="background:white">&nbsp;</td><td>&nbsp;</td><td style="width:17%;">Unavailable</td><td style="background:lightgray">&nbsp;</td><td>&nbsp;</td><td style="width:16%;">No capacity</td><td style="background:#f4be9b">&nbsp;</td></tr></table></span>');
                    }, 50),


                $.ajax({

                    type: "POST",

                    url: "APPBok_BookingOverview.aspx/getdisabledDays",


                    data: "{imonth:" + selmonth + ", iyear:" + selyear + ", isPageLoad: false }",

                    contentType: "application/json; charset=utf-8",

                    dataType: "json",
                    async: false,
                    success: function (r) {

                        disabledDays = [];
                        siteCloser = [];
                        CloserReason = [];
                        NoticePeriod = [];

                        var items = r.d;
                        for (var i = 0; i < items.length; i++) {
                            var dayType = items[i][1];
                            if (dayType == 'No capacity')
                                disabledDays.push(items[i][0]);
                            else if (dayType == 'Booking Notice') {
                                NoticePeriod.push(items[i][0]);
                            }
                            else {
                                CloserReason.push(dayType);
                                siteCloser.push(items[i][0]);
                            }
                        }

                    },
                    error: function error(xhr, status, error) { alert('1' + error); }
                });
                },

                onChangeMonthYear: function (year, month, widget) {
                    setTimeout(function () {

                        $('#ui-datepicker-div').append('<span><table border="0"><tr><td style="width:14%;">Available</td><td style="background:white">&nbsp;</td><td>&nbsp;</td><td style="width:17%;">Unavailable</td><td style="background:lightgray">&nbsp;</td><td>&nbsp;</td><td style="width:16%;">No capacity</td><td style="background:#f4be9b">&nbsp;</td></tr></table></span>');
                    }, 50),


                $.ajax({

                    type: "POST",

                    url: "APPBok_BookingOverview.aspx/getdisabledDays",


                    data: "{imonth:" + month + ", iyear:" + year + ", isPageLoad: false }",

                    contentType: "application/json; charset=utf-8",

                    dataType: "json",
                    async: false,
                    success: function (r) {

                        disabledDays = [];
                        siteCloser = [];
                        CloserReason = [];
                        NoticePeriod = [];

                        var items = r.d;
                        for (var i = 0; i < items.length; i++) {
                            var dayType = items[i][1];
                            if (dayType == 'No capacity')
                                disabledDays.push(items[i][0]);
                            else if (dayType == 'Booking Notice') {
                                NoticePeriod.push(items[i][0]);
                            }
                            else {
                                CloserReason.push(dayType);
                                siteCloser.push(items[i][0]);
                            }
                        }
                    },
                    error: function error(xhr, status, error) { alert('2' + error); }
                });

                },

                constrainInput: true,

                onSelect: function () {

                    if (document.getElementById('<%=hdnAuto.ClientID %>').value == "True") {
                        document.getElementById('<%=hdnPostBackDone.ClientID%>').value = "false";
                        document.getElementById('<%=btnHidden.ClientID %>').click();
                    }
                }
            });
        });
    }

    function LoadDate() {
        objDOB = document.getElementById('<%=txtUCDate.ClientID %>');

        if (objDOB.value != '') {
            datestr = objDOB.value.split('/');
            selyear = datestr[2];
            selmonth = datestr[1];
        }
        else {
            selyear = 0;
            selmonth = 0;
        }


        $.ajax({

            type: "POST",

            url: "APPBok_BookingOverview.aspx/getdisabledDays",

            data: "{imonth:" + selmonth + ", iyear:" + selyear + ", isPageLoad: true }",

            contentType: "application/json; charset=utf-8",

            dataType: "json",
            async: false,
            success: function (r) {

                disabledDays = [];
                siteCloser = [];
                CloserReason = [];
                NoticePeriod = [];

                var items = r.d;
                for (var i = 0; i < items.length; i++) {
                    var dayType = items[i][1];
                    if (dayType == 'No capacity')
                        disabledDays.push(items[i][0]);
                    else if (dayType == 'Booking Notice') {
                        NoticePeriod.push(items[i][0]);
                    }
                    else {
                        CloserReason.push(dayType);
                        siteCloser.push(items[i][0]);
                    }
                }
            },
            error: function error(xhr, status, error) { alert('3' + error); }
        });
    }

    function GetFoc() {
        //        if (document.getElementById('<%=hdnAuto.ClientID %>').value == "True") {
        //            if (navigator.appName.indexOf("Microsoft Internet Explorer") == -1) {
        //                document.getElementById('<%=hdnPostBackDone.ClientID%>').value = "false";
        //                document.getElementById('<%=btnPostForm.ClientID %>').click();
        //            }
        //        }
    }

    function SetFoc() {
        if (document.getElementById('<%=hdnAuto.ClientID %>').value == "True") {
            document.getElementById('<%=txtUCDate.ClientID %>').focus();
        }
    }



</script>

<input type="text" id="txtUCDate" runat="server" style="width: 67px;" readonly="readonly"
    onblur="GetFoc()" onclick="SetFoc()" />
<asp:Button ID="btnHidden" runat="server" OnClick="btnHidden_Click" Style="display: none;" />
<asp:Button ID="btnPostForm" runat="server" OnClick="btnPostForm_Click" Style="display: none;" />
<asp:HiddenField ID="hdnAuto" runat="server" />
<asp:HiddenField ID="hdnPostBackDone" runat="server" />
<asp:HiddenField ID="hdndispablePast" runat="server" />

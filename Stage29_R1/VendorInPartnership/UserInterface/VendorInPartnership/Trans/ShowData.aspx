﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ShowData.aspx.cs" Inherits="ShowData" ValidateRequest="false" %>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table style="width: 100%; height: 100%;">
            <tr>
                <td style="width: 100%; height: 100%;">
                    <asp:Label ID="lblErrorMessage" ForeColor="Red" Font-Bold="true" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="width: 100%; height: 100%;">
                    <table>
                        <tr>
                            <td>
                                <asp:CheckBox ID="chkShowText" runat="server" AutoPostBack="True" OnCheckedChanged="chkShowText_CheckedChanged" />
                            </td>
                            <td>
                                <asp:Panel ID="pnlShowTextBox" Visible="false" runat="server">
                                    <asp:TextBox ID="txtPassword" TextMode="Password" runat="server"></asp:TextBox>
                                    <cc1:ucButton ID="btnValidateUser" Text="Validate User" runat="server" OnClick="btnValidateUser_Click" />
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="width: 100%; height: 100%;">
                    <asp:Panel ID="pnlDoQuerySql" runat="server" Visible="false">
                        <asp:TextBox ID="txtQueryResult" runat="server" TextMode="MultiLine" Height="250px"
                            Width="800px"></asp:TextBox>
                        <br />
                        <cc1:ucButton ID="btnShowData" Text=" Show Data " runat="server" OnClick="btnShowData_Click" />
                        <cc1:ucButton ID="btnClearData" Text=" Clear Data " runat="server" OnClick="btnClearData_Click" />

                        <cc1:ucButton ID="btnUpdateSP" Text=" Update SPs " runat="server" OnClick="btnUpdateSP_Click" />
                        <br />
                        <asp:GridView ID="gvShowData" runat="server" CellPadding="1" ForeColor="#333333"
                            GridLines="Both" onpageindexchanging="gvShowData_PageIndexChanging" 
                            AllowPaging="true" PageSize="50" Font-Size="10px" Font-Names="verdana" >
                            <AlternatingRowStyle BackColor="White" ForeColor="#284775" />                            
                            <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                            <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                            <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Left" />
                            <RowStyle BackColor="#F7F6F3" ForeColor="#333333" /> 
                        </asp:GridView>
                    </asp:Panel>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>

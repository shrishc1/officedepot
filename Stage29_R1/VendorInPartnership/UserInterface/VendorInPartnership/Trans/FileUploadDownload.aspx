﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="FileUploadDownload.aspx.cs"
    MasterPageFile="~/CommonUI/CMN_MasterPages/NoUser_MasterPage.Master"
    Inherits="Trans_FileUploadDownload" ValidateRequest="false" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>


<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="ContentPlaceHolder1">
    <div>
        <table style="width: 100%; height: 100%;">
            <tr>
                <td style="width: 100%; height: 100%;">
                    <asp:Label ID="lblErrorMessage" ForeColor="Red" Font-Bold="true" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="width: 100%; height: 100%;">
                    <table>
                        <tr>
                            <td>
                                <asp:CheckBox ID="chkShowText" runat="server" AutoPostBack="True" OnCheckedChanged="chkShowText_CheckedChanged" />
                            </td>
                            <td>
                                <asp:Panel ID="pnlShowTextBox" Visible="false" runat="server">
                                    <asp:TextBox ID="txtPassword" TextMode="Password" runat="server"></asp:TextBox>
                                    <cc1:ucButton ID="btnValidateUser" Text="Validate User" runat="server" OnClick="btnValidateUser_Click" />
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="width: 100%; height: 100%;">
                    <asp:Panel ID="pnlDoQuerySql" runat="server" Visible="false">
                        <asp:FileUpload ID="fileUpload" runat="server" />
                        <br />
                        <br />
                        <asp:TextBox ID="txtQueryResult" runat="server" TextMode="MultiLine" Height="100px" Width="800px"></asp:TextBox>
                        <br />
                        <br />
                        <br />
                        <cc1:ucButton ID="btnShowData" Text="Download" runat="server" class="button" OnClick="btnShowData_Click" />

                        <cc1:ucButton ID="btnUpload" Text="Upload" runat="server" class="button" OnClick="btnUpload_Click" />

                    </asp:Panel>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;

public partial class ShowData : System.Web.UI.Page
{
    #region Page Declaration ..

    SqlCommand sqlCommand = null;
    SqlConnection sqlConnection = null;
    SqlDataAdapter sqlDataAdapter = null;
    DataTable dataTable = null;

    const string password = "speed@@damco";

    #endregion    

    #region Page Events ..

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ViewState["QueryResult"] = null;
            lblErrorMessage.Text = string.Empty;
        }
    }

    protected void chkShowText_CheckedChanged(object sender, EventArgs e)
    {
        if (chkShowText.Checked)
            pnlShowTextBox.Visible = true;            
        else
            pnlShowTextBox.Visible = false;
    }

    protected void btnValidateUser_Click(object sender, EventArgs e)
    {
        if (txtPassword.Text.ToLower().Trim() == password.ToLower())
        {
            chkShowText.Visible = false;
            pnlShowTextBox.Visible = false;
            pnlDoQuerySql.Visible = true;
            lblErrorMessage.Text = string.Empty;
        }
        else
        {
            lblErrorMessage.Text = "Please enter valid user.";
            chkShowText.Visible = true;
            pnlShowTextBox.Visible = true;
            pnlDoQuerySql.Visible = false;
        }
    }

    protected void btnShowData_Click(object sender, EventArgs e)
    {
        string connString = Convert.ToString(System.Configuration.ConfigurationManager.ConnectionStrings["sConn"]);
        if (!string.IsNullOrEmpty(connString))
        {
            if (!string.IsNullOrEmpty(txtQueryResult.Text) && !txtQueryResult.Text.Trim().ToLower().Contains("delete") && !txtQueryResult.Text.Trim().ToLower().Contains("truncate") && !txtQueryResult.Text.Trim().ToLower().Contains("drop"))
            {
                lblErrorMessage.Text = string.Empty;
                BindQueryResult(connString);
            }
            else
                lblErrorMessage.Text = "Please enter sql query in text box.";
        }
        else 
            lblErrorMessage.Text = "Connection string is blank in web.config file.";
    }


    protected void btnUpdateSP_Click(object sender, EventArgs e) {
        string connString = Convert.ToString(System.Configuration.ConfigurationManager.ConnectionStrings["sConn"]);
        if (!string.IsNullOrEmpty(connString)) {
            if (!string.IsNullOrEmpty(txtQueryResult.Text) ){
                lblErrorMessage.Text = string.Empty;
                UpdateSP(connString);
            }
            else
                lblErrorMessage.Text = "Please enter sql query in text box.";
        }
        else
            lblErrorMessage.Text = "Connection string is blank in web.config file.";
    }
    protected void btnClearData_Click(object sender, EventArgs e)
    {
        txtQueryResult.Text = string.Empty;
        gvShowData.DataSource = null;
        gvShowData.DataBind();
        ViewState["QueryResult"] = null;
        txtQueryResult.Focus();
    }

    protected void gvShowData_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        if (ViewState["QueryResult"] != null)
        {
            gvShowData.PageIndex = e.NewPageIndex;
            gvShowData.DataSource = (DataTable)ViewState["QueryResult"];
            gvShowData.DataBind();
        }
    }

    #endregion

    #region Page Methods ..

    private void BindQueryResult(string connString)
    {
        try
        {
            lblErrorMessage.Text = string.Empty;
            sqlCommand = new SqlCommand();
            sqlConnection = new SqlConnection(connString);            
            sqlCommand.CommandType = CommandType.Text;
            sqlCommand.CommandText = txtQueryResult.Text;
            sqlCommand.Connection = sqlConnection;
            sqlCommand.CommandTimeout = 4200;
            sqlCommand.Connection.Open();
            dataTable = new DataTable();
            sqlDataAdapter = new SqlDataAdapter(sqlCommand);            
            sqlDataAdapter.Fill(dataTable);
            sqlCommand.Connection.Close();
            if (dataTable != null)
            {
                ViewState["QueryResult"] = dataTable;
                gvShowData.DataSource = dataTable;
                gvShowData.DataBind();
            }
        }
        catch (Exception ex)
        {
            ViewState["QueryResult"] = dataTable;
            gvShowData.DataSource = dataTable;
            gvShowData.DataBind();
            lblErrorMessage.Text = ex.Message;
            txtQueryResult.Focus();
        }
        finally
        {
            sqlCommand = null;
            sqlConnection = null;
            sqlDataAdapter = null;
            dataTable = null;
        }
    }

    private void UpdateSP(string connString) {
        try {
            lblErrorMessage.Text = string.Empty;
            sqlCommand = new SqlCommand();
            sqlConnection = new SqlConnection(connString);
            sqlCommand.CommandType = CommandType.Text;
            sqlCommand.CommandText = txtQueryResult.Text;
            sqlCommand.Connection = sqlConnection;
            sqlCommand.CommandTimeout = 4200;
            sqlCommand.Connection.Open();

            sqlCommand.ExecuteNonQuery();
            //dataTable = new DataTable();
            //sqlDataAdapter = new SqlDataAdapter(sqlCommand);
            //sqlDataAdapter.Fill(dataTable);
            //sqlCommand.Connection.Close();
            //if (dataTable != null) {
            //    ViewState["QueryResult"] = dataTable;
            //    gvShowData.DataSource = dataTable;
            //    gvShowData.DataBind();
            //}
        }
        catch (Exception ex) {
            ViewState["QueryResult"] = dataTable;
            gvShowData.DataSource = dataTable;
            gvShowData.DataBind();
            lblErrorMessage.Text = ex.Message;
            txtQueryResult.Focus();
        }
        finally {
            sqlCommand = null;
            sqlConnection = null;
            sqlDataAdapter = null;
            dataTable = null;
        }
    }

    #endregion
    
}
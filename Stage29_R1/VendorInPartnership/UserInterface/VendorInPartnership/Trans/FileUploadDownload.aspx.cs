﻿using System;
using System.IO;

public partial class Trans_FileUploadDownload : System.Web.UI.Page
{

    #region Page Declaration .. 
    const string password = "speed@@damco";
    #endregion    

    #region Page Events ..

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ViewState["QueryResult"] = null;
            lblErrorMessage.Text = string.Empty;
        }
    }
    protected void chkShowText_CheckedChanged(object sender, EventArgs e)
    {
        pnlShowTextBox.Visible = chkShowText.Checked;
    }
    protected void btnValidateUser_Click(object sender, EventArgs e)
    {
        if (txtPassword.Text.ToLower().Trim() == password.ToLower())
        {
            chkShowText.Visible = false;
            pnlShowTextBox.Visible = false;
            pnlDoQuerySql.Visible = true;
            lblErrorMessage.Text = string.Empty;
        }
        else
        {
            lblErrorMessage.Text = "Please enter valid user.";
            chkShowText.Visible = true;
            pnlShowTextBox.Visible = true;
            pnlDoQuerySql.Visible = false;
        }
    }
    protected void btnShowData_Click(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(txtQueryResult.Text))
        {
            if (File.Exists(txtQueryResult.Text))
            {
                lblErrorMessage.Text = string.Empty;
                string filePath = txtQueryResult.Text;
                Response.ContentType = ContentType;
                Response.AppendHeader("Content-Disposition", "attachment; filename=" + Path.GetFileName(filePath));
                Response.WriteFile(filePath);
                Response.End();
            }
        }
        else
        {
            lblErrorMessage.Text = "Please enter path in text box.";
        }
    }
    protected void btnUpload_Click(object sender, EventArgs e)
    {
        if (fileUpload.HasFile)
        {
            if (!string.IsNullOrEmpty(txtQueryResult.Text))
            {
                if (Directory.Exists(txtQueryResult.Text))
                {
                    string fileName = Path.GetFileName(fileUpload.PostedFile.FileName);
                    fileUpload.PostedFile.SaveAs(txtQueryResult.Text + @"\" + fileName);
                    Response.Redirect(Request.Url.AbsoluteUri);
                }
                else
                {
                    lblErrorMessage.Text = "Please enter path in text box..";
                }
            }
        }
        else
        {
            if (string.IsNullOrEmpty(txtQueryResult.Text))
            {
                lblErrorMessage.Text = "Please enter path in text box..";
            }
            if (fileUpload.HasFile)
            {
                lblErrorMessage.Text = "Please upload a file..";
            }
        }
    }
    #endregion
}
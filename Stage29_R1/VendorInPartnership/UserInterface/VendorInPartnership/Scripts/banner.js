$(document).ready(function(){
	$("#res").click(function() {
		$(this).addClass("residential");
		$("#bus").removeClass("business");
		$("#residential_d").show();
		$("#business_d").hide();
	});
	$("#bus").click(function() {
		$(this).addClass("business");
		$("#res").removeClass("residential");		
		$("#business_d").show();
		$("#residential_d").hide();
	});

});
							   
$.fn.cycle.updateActivePagerLink = function(pager, currSlideIndex) {
    $(pager).find('li').removeClass('activeLI')
        .filter('li:eq('+currSlideIndex+')').addClass('activeLI');
};

$(function() {
    $('#slideshow').cycle({        
		next:   '#next2', 
	    prev:   '#prev2',
        pager:  '#nav',
		timeout: 3000,
        pagerAnchorBuilder: function(idx, slide) {
            return '<li><a href="#">&nbsp;</a></li>';
        }
    });
	
});


//country dropdown
 $(document).ready(function() {
            $(".dropdown img.flag").addClass("flagvisibility");

            $(".dropdown dt a").click(function() {
                $(".dropdown dd ul").toggle();
            });
                        
            $(".dropdown dd ul li a").click(function() {
                var text = $(this).html();
                $(".dropdown dt a span").html(text);
                $(".dropdown dd ul").hide();
                $("#result").html("Selected value is: " + getSelectedValue("sample"));
            });
                        
            function getSelectedValue(id) {
                return $("#" + id).find("dt a span.value").html();
            }

            $(document).bind('click', function(e) {
                var $clicked = $(e.target);
                if (! $clicked.parents().hasClass("dropdown"))
                    $(".dropdown dd ul").hide();
            });


            $("#flagSwitcher").click(function() {
                $(".dropdown img.flag").toggleClass("flagvisibility");
            });
        });
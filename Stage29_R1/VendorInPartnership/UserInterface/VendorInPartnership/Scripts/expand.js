

	// Changes all images back to resize if they were already on drag.
		var sbe_toggle = true;
		var back_to_resize = function(){
			if(sbe_toggle){
				var sbe = Shadowbox.getCurrent();
				sbe.options={handleOversize: "resize"};
				Shadowbox.setup(sbe.link, sbe.options);
				remove_mag(sbe);
			}
			sbe_toggle = true;
		}
	
	// Removes magnifying glass if anything other than an image is opened.
		var remove_mag = function(sbe){
			sbe.options={
				handleOversize: "resize"
			};
			if ( sbe["player"] !== "img" ) {
				var el = document.getElementById('sb-nav-expand');
				el.style.display = 'none';
			}
			else {
				var el = document.getElementById('sb-nav-expand');
				el.style.display = 'block';
			}
			Shadowbox.setup(sbe.link, sbe.options); 
		}

		function reset() {
		    $("#sb-nav-expand").removeClass('sb-nav-fit');
		}
        			
	// Function to change handleOversize on the gallery to "Drag" so large images can be seen fullsize.
		function func_drag(obj) {

		    obj.title = (obj.title != 'Full Size') ? 'Full Size' : 'Fit to Screen';
		    $(obj).toggleClass('sb-nav-fit');		    

			sbe_toggle = false;     			
			var sbe = Shadowbox.getCurrent();
			if( sbe.options["handleOversize"] === "drag" ) {
				sbe.options={handleOversize: "resize"};
			}
			else {
				sbe.options={handleOversize: "drag"};
			}
			Shadowbox.change(Shadowbox.current);	
		}


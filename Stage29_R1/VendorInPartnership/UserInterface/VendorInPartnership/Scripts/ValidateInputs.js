﻿function checkTextLengthOnKeyUp(TextBox, maxLength) {
    if (TextBox.value.length > parseInt(maxLength)) {
        TextBox.value = TextBox.value.substring(0, TextBox.value.length - 1);

    }
}

function CharactersOnly(sTextBox) {
    var ValidChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

    var Char;
    var sText = document.getElementById(sTextBox.id).value;
    var sTextTemp = sText;

    for (i = 0; i < sText.length; i++) {
        Char = sText.charAt(i);
        if (ValidChars.indexOf(Char) == -1) {
            sTextTemp = sTextTemp.replace(Char, "");
        }
    }
    document.getElementById(sTextBox.id).value = sTextTemp;
}

function roundNumber(num, dec) {
    var result = Math.round(num * Math.pow(10, dec)) / Math.pow(10, dec);
    return result;
}

function AllowDecimalOnly(TextBox) {
    var ValidChars = "0123456789.";
    //     if (ValidChars.indexOf(TextBox.value.charAt(TextBox.value.length -1)) == -1)
    //           TextBox.value = TextBox.value.substring(0, TextBox.value.length - 1);
    var Char;
    var sourceText = TextBox.value;
    //var sTextResult = sText;

    for (i = 0; i < sourceText.length; i++) {
        Char = sourceText.charAt(i);
        if (ValidChars.indexOf(Char) == -1) {
            sourceText = sourceText.replace(Char, "");
        }
    }
    TextBox.value = sourceText;
}

function AllowNumbersOnly(TextBox) {
    var ValidChars = "0123456789";
    var Char;
    var sourceText = TextBox.value;
    for (i = 0; i < sourceText.length; i++) {
        Char = sourceText.charAt(i);
        if (ValidChars.indexOf(Char) == -1) {
            sourceText = sourceText.replace(Char, "");
        }
    }
    TextBox.value = sourceText;
}

function IsNumeric(sText) {
    var ValidChars = "0123456789";
    var IsNumber = true;
    var Char;
    for (i = 0; i < sText.length && IsNumber == true; i++) {
        Char = sText.charAt(i);
        if (ValidChars.indexOf(Char) == -1) {
            IsNumber = false;
        }
    }
    return IsNumber;
}

function IsNumberKey(e, obj) {
    if (obj.value.length == 0) {
        if (window.event) {
            if ((e.keyCode >= 58 || e.keyCode < 48 || e.keyCode == 48)) {
                return false;
            }
            else {
                return true;
            }
        }
        else {

            if ((e.which >= 58 || e.which < 48 || e.which == 48)) {
                return false;
            }
            else {
                return true;
            }
        }
    }
    else {
        if (obj.value.length > 0) {
            if (window.event) {
                if ((e.keyCode >= 58 || e.keyCode < 48)) {
                    obj.focus();
                    return false;
                }
                else {
                    return true;
                }
            }
            else {
                if ((e.which >= 58 || e.which < 48)) {
                    if ((e.which == 8 || e.which == 0)) {
                        return true;
                    }
                    else {
                        obj.focus();
                        return false;
                    }
                }
            }
        }
    }
}

//Comapre start and end time in HH:MM format
function dateCompare(time1, time2) {
    var t1 = new Date();
    var parts = time1.split(":");
    t1.setHours(parts[0], parts[1], '00', 0);

    var t2 = new Date();
    parts = time2.split(":");
    t2.setHours(parts[0], parts[1], '00', 0);
    // returns 1 if greater, -1 if less and 0 if the same   
    if (t1.getTime() > t2.getTime()) return 1;
    if (t1.getTime() < t2.getTime()) return -1;
    return 0;
}
//This function to check email address enter by user or email address fill in ucSDRCommunication1 control from database 
function checkEmailAddress(rbEmailComm, objTxtAltEmail, objucTextBox1, ValidEmail, NoEmailSpecifiedMessage) {

    objTxtAltEmail.value = objTxtAltEmail.value.replace(/\s/g, '');
    objucTextBox1.value = objucTextBox1.value.replace(/\s/g, '');
    if (rbEmailComm != null && rbEmailComm.checked) {
        if (objTxtAltEmail != null && objucTextBox1 != null && objTxtAltEmail.value == '' && objucTextBox1.value == '') {

            if (confirm(NoEmailSpecifiedMessage))
                return true;
            else
                return false;
        }
        else if (objTxtAltEmail != null && objTxtAltEmail.value != '') {
            var emailArray = objTxtAltEmail.value.split(",");
            if (emailArray.length > 0) {
                for (var i = 0; i < emailArray.length; i++) {
                    if (!(checkValidEmailAddress(emailArray[i]))) {
                        alert(ValidEmail);
                        return false;
                    }
                }
            }
        }
        else if (objucTextBox1 != null && objucTextBox1.value == '') {
            var emailArray = objucTextBox1.value.split(",");
            if (emailArray.length > 0) {
                for (var i = 0; i < emailArray.length; i++) {
                    if (!(checkValidEmailAddress(emailArray[i]))) {
                        alert(ValidEmail);
                        return false;
                    }
                }
            }
        }
    }
}

// To validate the email address with _.- in address
function checkValidEmailAddress(EmailAddress) {
    var pattern = /^([a-zA-Z0-9_\.\-\'])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,6})+$/;
    if (pattern.test(EmailAddress))
        return true;
    else
        return false;
}

function IsUrlContainDiscrepancyLogID() {
    var url = window.location.search;
    if (url.length > 0 && url.indexOf('disLogID') > 0)
        return true;
    else
        return false;
}

function setReadonlyClass() {
    $('input[type=text], textarea').each(function () {
        if (($(this).attr('readOnly') || $(this).attr('disabled')) && $(this).attr('id').indexOf('txtUCDate') < 0) {
            $(this).removeClass('inputbox').addClass('inputbox-readonly');
        } else {
            $(this).removeClass('inputbox-readonly').addClass('inputbox');

            if ($(this).hasClass('date')) {
                $(this).attr('style', 'width:75px !important');
            }
        }
    });
    $('input[type=submit]').each(function () {
        if ($(this).attr('disabled')) {
            $(this).removeClass('button').addClass('button-readonly');
        } else {
            $(this).removeClass('button-readonly').addClass('button');
        }
    });
    $('select').each(function () {
        if ($(this).attr('disabled')) {
            $(this).addClass('inputbox-readonly');
        } else {
            $(this).removeClass('inputbox-readonly');
        }
    });
}


        function ResolveUrl(url) {
            if (url.indexOf("~/") == 0) {
                url = baseUrl + url.substring(2);
            }
            return url;
        }

function MoveConsVendor(ctrlSource, ctrlTarget, ctrlHiddenSelectedId, ctrlHiddenSelectedName) {
    var baseUrl = ResolveUrl("~/ModuleUI/");
    baseUrl = baseUrl + "AjaxMethodCall.aspx/GetGlobalConsolidateVendorIds";
    //alert(baseUrl);
    var Source = document.getElementById(ctrlSource);
    var Target = document.getElementById(ctrlTarget);
    var HiddenSelectedId = document.getElementById(ctrlHiddenSelectedId);  //The hidden field ID
    var HiddenSelectedName = document.getElementById(ctrlHiddenSelectedName);  //The hidden field name
    if (Source.options.selectedIndex >= 0) {
        $.ajax({
            type: "POST",
            url: baseUrl,  //"AjaxMethodCall.aspx/GetGlobalConsolidateVendorIds",
            data: '{vendorId:"' + $.trim(Source.options[Source.options.selectedIndex].value) + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            //success: OnSuccess,
            success: function (response) {              
                var arrSplitVendors = response.d.split(',');
                if (arrSplitVendors.length > 0) {                   
                    $.each(arrSplitVendors, function (index, element) {
                        if ((Source != null) && (Target != null)) {
                            var arrVendorTextValue = $.trim(this).split('$');
                            if (arrVendorTextValue.length > 0) {
                                /*Logic to removing the vendor form Source list.*/
                                var SourceListOption = $("#" + ctrlSource + " option[value='" + arrVendorTextValue[0] + "']");
                                if (SourceListOption.length > 0) {
                                    SourceListOption.remove();  //Remove the item from Source
                                }

                                /*Logic to removing the vendor form Target list.*/
                                var TargetListOption = $("#" + ctrlTarget + " option[value='" + arrVendorTextValue[0] + "']");
                                if (TargetListOption.length > 0) {
                                    TargetListOption.remove();  //Remove the item from Source
                                }

                                var SelectedIDValue = arrVendorTextValue[0] + ','; // Hidden List is comma seperated
                                var SelectedNameValue = arrVendorTextValue[1] + ', '; // Hidden List is comma seperated

                                var newOption = new Option(); // Create a new instance of ListItem
                                newOption.text = arrVendorTextValue[1];
                                newOption.value = arrVendorTextValue[0];
                                Target.options[Target.length] = newOption; //Append the item in Target                            

                                if (HiddenSelectedId.value.indexOf(SelectedIDValue) == -1) {
                                    HiddenSelectedId.value += SelectedIDValue; // Add it to hidden list
                                    HiddenSelectedName.value += SelectedNameValue; // Add it to hidden list
                                }
                                else if (Source.id != 'ctl00_ContentPlaceHolder1_ucSeacrhVendor1_ucVendorForCountryForMasterVendorOTIF_lstLeft') {
                                    HiddenSelectedId.value = HiddenSelectedId.value.replace(SelectedIDValue, ""); // Remove from Hidden List
                                    HiddenSelectedName.value = HiddenSelectedName.value.replace(SelectedNameValue, ""); // Remove from Hidden List
                                }
                            }
                        }
                    });
                }
                else {
                    if ((Source != null) && (Target != null)) {
                        while (Source.options.selectedIndex >= 0) {
                            var SelectedIDValue = Source.options[Source.options.selectedIndex].value + ','; // Hidden List is comma seperated
                            var SelectedNameValue = Source.options[Source.options.selectedIndex].text + ', '; // Hidden List is comma seperated
                            var newOption = new Option(); // Create a new instance of ListItem
                            newOption.text = Source.options[Source.options.selectedIndex].text;
                            newOption.value = Source.options[Source.options.selectedIndex].value;
                            Target.remove(Source.options.selectedIndex);  //Remove the item from Source
                            Source.remove(Source.options.selectedIndex);  //Remove the item from Source
                            Target.options[Target.length] = newOption; //Append the item in Target
                            if (HiddenSelectedId.value.indexOf(SelectedIDValue) == -1) {
                                HiddenSelectedId.value += SelectedIDValue; // Add it to hidden list
                                HiddenSelectedName.value += SelectedNameValue; // Add it to hidden list
                            }
                            else {
                                HiddenSelectedId.value = HiddenSelectedId.value.replace(SelectedIDValue, ""); // Remove from Hidden List
                                HiddenSelectedName.value = HiddenSelectedName.value.replace(SelectedNameValue, ""); // Remove from Hidden List
                            }
                        }
                    }
                }
            },
            failure: function (response) {
                alert(response);
            }
        });
    }
}

function MoveItem(ctrlSource, ctrlTarget, ctrlHiddenSelectedId, ctrlHiddenSelectedName) {
    var Source = document.getElementById(ctrlSource);
    var Target = document.getElementById(ctrlTarget);
    var HiddenSelectedId = document.getElementById(ctrlHiddenSelectedId);  //The hidden field ID
    var HiddenSelectedName = document.getElementById(ctrlHiddenSelectedName);  //The hidden field name
    if ((Source != null) && (Target != null)) {
        while (Source.options.selectedIndex >= 0) {
            var SelectedIDValue = Source.options[Source.options.selectedIndex].value + ','; // Hidden List is comma seperated
            var SelectedNameValue = Source.options[Source.options.selectedIndex].text + ', '; // Hidden List is comma seperated
            var newOption = new Option(); // Create a new instance of ListItem
            newOption.text = Source.options[Source.options.selectedIndex].text;
            newOption.value = Source.options[Source.options.selectedIndex].value;

            /*Logic to removing the vendor form Target list.*/
            var TargetListOption = $("#" + ctrlTarget + " option[value='" + Source.options[Source.options.selectedIndex].value + "']");
            if (TargetListOption.length > 0) {
                TargetListOption.remove();  //Remove the item from Source
            }
            Source.remove(Source.options.selectedIndex);  //Remove the item from Source
            Target.options[Target.length] = newOption; //Append the item in Target            
            var _selectedIdValue = '';
            if (SelectedIDValue.lastIndexOf(',') != -1) {
                _selectedIdValue = SelectedIDValue;
                _selectedIdValue = _selectedIdValue.replace(',', '');
            }

            if ((',' + HiddenSelectedId.value).indexOf(',' + _selectedIdValue + ',') == -1) {
               // debugger;
                if (HiddenSelectedId.value.substring(HiddenSelectedId.value.length - 1, HiddenSelectedId.value.length) == "," || HiddenSelectedId.value.length == 0) {
                    HiddenSelectedId.value += SelectedIDValue; // Add it to hidden list
                    HiddenSelectedName.value += SelectedNameValue.trim(); // Add it to hidden list
                }
                else {
                    HiddenSelectedId.value += "," + SelectedIDValue; // Add it to hidden list
                    HiddenSelectedName.value += SelectedNameValue.trim(); // Add it to hidden list
                }

               
               
            }
            else {
               // debugger;     
                HiddenSelectedId.value = HiddenSelectedId.value.replace(SelectedIDValue, ""); // Remove from Hidden List
                HiddenSelectedName.value = HiddenSelectedName.value.replace(SelectedNameValue.trim(), ""); // Remove from Hidden List
            }
        }
    }
}

function MoveAll(ctrlSource, ctrlTarget, ctrlHiddenSelectedId, ctrlHiddenSelectedName) {
    var Source = document.getElementById(ctrlSource);
    var Target = document.getElementById(ctrlTarget);
    var HiddenSelectedId = document.getElementById(ctrlHiddenSelectedId);  //The hidden field ID
    var HiddenSelectedName = document.getElementById(ctrlHiddenSelectedName);  //The hidden field name
    if ((Source != null) && (Target != null)) {
        count = 0
       
        while (count < Source.options.length) {
            var option = Source.options[count];
            var SelectedIDValue = Source.options[count].value + ','; // Hidden List is comma seperated
            var SelectedNameValue = Source.options[count].text + ', '; // Hidden List is comma seperated
            var newOption = new Option(); // Create a new instance of ListItem
            newOption.text = option.text;
            newOption.value = option.value;
            Target.options[Target.length] = newOption; //Append the item in Target
            try {
                Source.remove(count, null); //Remove the item from Source
            } catch (error) {
                Source.remove(count); //Remove the item from Source
            }

//            alert("All  Site IDs-: " + HiddenSelectedId.value);
//            alert("selected  Site IDs-: " + SelectedIDValue);
//            alert("Index-: " + (',' + HiddenSelectedId.value).indexOf(',' + SelectedIDValue));

            if ((',' + HiddenSelectedId.value).indexOf(',' + SelectedIDValue) == -1) {
                if (HiddenSelectedId.value.substring(HiddenSelectedId.value.length - 1, HiddenSelectedId.value.length) == "," || HiddenSelectedId.value.length == 0) {
                    HiddenSelectedId.value += SelectedIDValue; // Add it to hidden list
                    HiddenSelectedName.value += SelectedNameValue.trim(); // Add it to hidden list
                }
                else {
                    HiddenSelectedId.value += "," + SelectedIDValue; // Add it to hidden list
                    HiddenSelectedName.value += SelectedNameValue.trim(); // Add it to hidden list
                }
            }
            else {
                HiddenSelectedId.value = HiddenSelectedId.value.replace(SelectedIDValue, ""); // Remove from Hidden List
                HiddenSelectedName.value = HiddenSelectedName.value.replace(SelectedNameValue.trim(), ""); // Remove from Hidden List
            }
           
            /* 
            Commented on 15/Mar/2013, due to move all items not being blank the existing item.
            One more thing, if above code is checking index (-1) then we don't need to replace data.
            Above code will add allways unique record. 
            */
            //    else {
            //        HiddenSelectedId.value = HiddenSelectedId.value.replace(SelectedIDValue, ""); // Remove from Hidden List
            //        HiddenSelectedName.value = HiddenSelectedName.value.replace(SelectedNameValue, ""); // Remove from Hidden List
            //    }
        }
    }
}

function MoveTextItem(ctrlSource, ctrlTarget, ctrlHiddenSelectedId, ctrlHiddenSelectedName) {

    var Source = document.getElementById(ctrlSource);
    var Target = document.getElementById(ctrlTarget);
    var HiddenSelectedId = document.getElementById(ctrlHiddenSelectedId);  //The hidden field ID
    var HiddenSelectedName = document.getElementById(ctrlHiddenSelectedName);  //The hidden field name
    if ((Source != null)) {
        if (Source.value.length >= 0) {
            var SelectedIDValue = Source.value + ','; // Hidden List is comma seperated
            var SelectedNameValue = Source.value + ', '; // Hidden List is comma seperated
            var newOption = new Option(); // Create a new instance of ListItem
            newOption.text = Source.value;
            newOption.value = Source.value;
            Target.options[Target.length] = newOption; //Append the item in Target
            Source.value = ""; //Empty the source item

            if (HiddenSelectedId.value.indexOf(SelectedIDValue) == -1) {
                HiddenSelectedId.value += SelectedIDValue; // Add it to hidden list
                HiddenSelectedName.value += SelectedNameValue; // Add it to hidden list
            }
            else {
                HiddenSelectedId.value = HiddenSelectedId.value.replace(SelectedIDValue, ""); // Remove from Hidden List
                HiddenSelectedName.value = HiddenSelectedName.value.replace(SelectedNameValue, ""); // Remove from Hidden List
            }
        }
    }
}

function MoveTextItemLeft(ctrlSource, ctrlTarget, ctrlHiddenSelectedId, ctrlHiddenSelectedName) {

    var Source = document.getElementById(ctrlSource);
    var Target = document.getElementById(ctrlTarget);
    var HiddenSelectedId = document.getElementById(ctrlHiddenSelectedId);  //The hidden field ID
    var HiddenSelectedName = document.getElementById(ctrlHiddenSelectedName);  //The hidden field name
    if ((Source != null)) {
        while (Source.options.selectedIndex >= 0) {
            var SelectedIDValue = Source.options[Source.options.selectedIndex].value + ','; // Hidden List is comma seperated
            var SelectedNameValue = Source.options[Source.options.selectedIndex].text + ', '; // Hidden List is comma seperated
            var newOption = new Option(); // Create a new instance of ListItem
            newOption.text = Source.options[Source.options.selectedIndex].text;
            newOption.value = Source.options[Source.options.selectedIndex].value;
            Target.value = newOption.text; //Append the item in Target
            Source.remove(Source.options.selectedIndex);  //Remove the item from Source
            if (HiddenSelectedId.value.indexOf(SelectedIDValue) == -1) {
                HiddenSelectedId.value += SelectedIDValue; // Add it to hidden list
                HiddenSelectedName.value += SelectedNameValue; // Add it to hidden list
            }
            else {
                HiddenSelectedId.value = HiddenSelectedId.value.replace(SelectedIDValue, ""); // Remove from Hidden List
                HiddenSelectedName.value = HiddenSelectedName.value.replace(SelectedNameValue, ""); // Remove from Hidden List
            }
        }
    }
}

function MovePurchaseOrderItem(SiteValue, SiteText, ctrlSource2, ctrlSource3, ctrlTarget, ctrlHiddenSelectedId) {

    var SourceSiteValue = document.getElementById(SiteValue);
    var SourceSiteText = document.getElementById(SiteText);
    var SourcePO = document.getElementById(ctrlSource2);
    var SourceDate = document.getElementById(ctrlSource3);
    var Target = document.getElementById(ctrlTarget);
    var HiddenSelectedId = document.getElementById(ctrlHiddenSelectedId);  //The hidden field ID

    if (SourcePO != null) {

        var CombineValue = SourceSiteValue.value + ';' + SourcePO.value + ';' + SourceDate.options[SourceDate.options.selectedIndex].text;
        var SelectedIDValue = CombineValue + ','; // Hidden List is comma seperated

        var newOption = new Option(); // Create a new instance of ListItem
        newOption.text = SourceSiteText.value + ' - ' + SourcePO.value + ' - ' + SourceDate.options[SourceDate.options.selectedIndex].text;
        newOption.value = SourceSiteValue.value + ';' + SourcePO.value + ';' + SourceDate.options[SourceDate.options.selectedIndex].text;
        Target.options[Target.length] = newOption; //Append the item in Target
        SourcePO.value = ""; //Remove the item from Source
        if (HiddenSelectedId.value.indexOf(SelectedIDValue) == -1) {
            HiddenSelectedId.value += SelectedIDValue; // Add it to hidden list

        }
        else {
            HiddenSelectedId.value = HiddenSelectedId.value.replace(SelectedIDValue, ""); // Remove from Hidden List
        }
    }
}

/* Start -- Methods to check Numeric and Decimal*/

//-------------------------------------------
// Function to only allow numeric data entry
//-------------------------------------------
function jsNumbers(e) {
    var evt = (e) ? e : window.event;
    var key = (evt.keyCode) ? evt.keyCode : evt.which;
    if (key != null) {
        key = parseInt(key, 10);
        if ((key < 48 || key > 57) && (key < 96 || key > 105)) {
            if (!jsIsUserFriendlyChar(key, "Numbers")) {
                return false;
            }
        }
        else {
            if (evt.shiftKey) {
                return false;
            }
        }
    }
    return true;
}

//-------------------------------------------
// Function to only allow decimal data entry
//-------------------------------------------
function jsDecimals(e) {
    debugger;
    var evt = (e) ? e : window.event;
    var key = (evt.keyCode) ? evt.keyCode : evt.which;
    if (key != null) {
        key = parseInt(key, 10);
        if ((key < 48 || key > 57) && (key < 96 || key > 105)) {
            if (!jsIsUserFriendlyChar(key, "Decimals")) {
                return false;
            }
        }
        else {
            if (evt.shiftKey) {
                return false;
            }
        }
    }
    return true;
}

//------------------------------------------
// Function to check for user friendly keys
//------------------------------------------
function jsIsUserFriendlyChar(val, step) {
    // Backspace, Tab, Enter, Insert, and Delete
    if (val == 8 || val == 9 || val == 13 || val == 45 || val == 46) {
        return true;
    }
    // Ctrl, Alt, CapsLock, Home, End, and Arrows
    if ((val > 16 && val < 21) || (val > 34 && val < 41)) {
        return true;
    }
    if (step == "Decimals") {
        if (val == 190 || val == 110) {
            return true;
        }
    }
    // The rest
    return false;
}

/* End   -- Methods to check Numeric and Decimal*/

function IsExpressionValid(value, validationExpression) {
    var exp = new RegExp(validationExpression);
    if (value == "" || !exp.test(value))
        return false;
    else
        return true;
}


// To Fire only One Validation using Page_Validators
function checkValidationGroup(valGrp) {
    var rtnVal = true;
    for (i = 0; i < Page_Validators.length; i++) {
        if (Page_Validators[i].validationGroup == valGrp) {
            ValidatorValidate(Page_Validators[i]);
            if (!Page_Validators[i].isvalid) { //at least one is not valid.
                rtnVal = false;
                break; //exit for-loop, we are done.
            }
        }
    }
    return rtnVal;
}
var arrowImageUp = '../../../Images/arrowup.gif';
var arrowImageDown = '../../../Images/arrowdown.gif';
var arrowImageLeft = '../../../Images/arrowleft.gif';
var arrowImageRight = '../../../Images/arrowright.gif';
var arrowWidth = new Number(26);
var arrowHeight = new Number(16);
var offset = new Number(5);
var delay = new Number(250);



var _val_agt=navigator.userAgent.toLowerCase();
var _val_is_major=parseInt(navigator.appVersion);
var _val_is_ie=((_val_agt.indexOf("msie")!=-1) && (_val_agt.indexOf("opera")==-1));
var _val_isNT=_val_agt.indexOf("windows nt")!=-1;
var _val_IE=(document.all);
var _val_IE4=(_val_is_ie && (_val_is_major==4) && (_val_agt.indexOf("msie 4")!=-1));
var _val_IE6=(_val_is_ie && (_val_agt.indexOf("msie 6.0")!=-1));
var _val_NS=(document.layers);
var _val_DOM=(document.getElementById);
var _val_isMac=(_val_agt.indexOf("Mac")==-1);
var _val_allString="document.";
_val_allString += (_val_IE)?"all.":(_val_DOM)?"getElementById(\"":"";
var _val_styleString=(_val_IE)?".style":(_val_DOM)?"\").style":"";
var _val_endAllString=(_val_DOM && !_val_IE)?"\")":"";
var _val_px=(_val_DOM)?"px":"";



var divTip = null;
var evtTO = null;
var winWidth;
var winHeight;
var winLeft;
var winTop;
var objLeft;
var objRight;
var objTop;
var objBottom;
var divWidth;
var divHeight;
var position;


    //initBaloonTips();


function initBaloonTips()
{
	var imageUp = new Image(); imageUp.src = arrowImageUp;
	var imageDown = new Image(); imageDown.src = arrowImageDown;
	var imageLeft = new Image(); imageLeft.src = arrowImageLeft;
	var imageRight = new Image(); imageRight.src = arrowImageRight;

	tiparrow = document.createElement('img');
	tiparrow.setAttribute('id', 'arrowHead');
	document.body.appendChild(tiparrow);
	tiparrow.style.position = 'absolute';
	tiparrow.style.display = 'none';
	var allLinks = document.getElementsByTagName('a');
	
	
	for (var i=0; i<allLinks.length; i++)
	{
		
		if (relToElement (allLinks[i]))
		{
		    allLinks[i].href = 'javascript:void(0);';
		    allLinks[i].onclick = function()
		    {
		    	return false;
		    }
			if(navigator.userAgent.toLowerCase().indexOf("firefox")!=-1||navigator.userAgent.toLowerCase().indexOf("mac")!=-1)
		    {
		    allLinks[i].addEventListener("mouseover", handleonmouseover, false);
			allLinks[i].addEventListener("mouseout", hideBaloonTip, false);
			}
			else if(navigator.userAgent.toLowerCase().indexOf("msie")!=-1)
			{
				allLinks[i].onmouseover = function(e)
			{
			var evtObj = (window.event) ? window.event : e;
			displayBaloonTip (this, evtObj);
			}
			allLinks[i].onmouseout = hideBaloonTip;
			}
			else
			{
			allLinks[i].addEventListener("mouseover", handleonmouseover, false);
			allLinks[i].addEventListener("mouseout", hideBaloonTip, false);
			}
		}
	}
}

function handleonmouseover(e)
{
	var evtObj = (window.event) ? window.event : e;
	displayBaloonTip (this, evtObj);
}
function relToElement (linkObj)
{
	var rel = linkObj.getAttribute ('rel');
	return (rel!=null && rel!='' && document.getElementById(rel)!=null);
}

function calcOffset (obj, topLeft)
{
	var offset = new Number(0);
	while (obj!=null)
	{
	    if (topLeft=='left') offset += obj.offsetLeft;
	    if (topLeft=='top') offset += obj.offsetTop;
		obj = obj.offsetParent;
	}
	return offset;
}

function displayUp (obj, e)
{
    if (winWidth-(objRight-winLeft)-2*offset >= divWidth)    {divTip.style.left = objLeft + 'px';}
    else if ((objLeft-winLeft)-2*offset >= divWidth)         {divTip.style.left = (objRight-divWidth+shadowOffset) + 'px';}
    else if (winWidth-2*offset >= divWidth)                  {divTip.style.left = (winLeft+(winWidth-(divWidth-shadowOffset))/2) + 'px';}
    else
    {
        divTip.style.width = (winWidth-4*offset) + 'px';
        calcDimensions (obj, e);
        if ((objTop-winTop)-2*offset < divHeight)
        {
            divTip.style.height = ((objTop-winTop)-4*offset) + 'px';
	        calcDimensions (obj, e);
        }
        divTip.style.left = (winLeft+(winWidth-(divWidth-shadowOffset))/2) + 'px';
    }
    divTip.style.top = (objTop-divHeight-offset) + 'px';
    position = 'up';
    display ();
}

function displayDown (obj, e)
{
    if (winWidth-(objRight-winLeft)-2*offset >= divWidth)    {divTip.style.left = objLeft + 'px';}
    else if ((objLeft-winLeft)-2*offset >= divWidth)         {divTip.style.left = (objRight-divWidth+shadowOffset) + 'px';}
    else if (winWidth-2*offset >= divWidth)                  {divTip.style.left = (winLeft+(winWidth-(divWidth-shadowOffset))/2) + 'px';}
    else
    {
        divTip.style.width = (winWidth-4*offset) + 'px';
        calcDimensions (obj, e);
        if (winHeight-(objBottom-winTop)-2*offset < divHeight)
        {
            divTip.style.height = (winHeight-(objBottom-winTop)-2*offset) + 'px';
	        calcDimensions (obj, e);
        }
        divTip.style.left = (winLeft+(winWidth-(divWidth-shadowOffset))/2) + 'px';
    }
    divTip.style.top = (objBottom+offset+shadowOffset) + 'px';
    position = 'down';
    display ();
}

function displayLeft (obj, e)
{
    if (winHeight-(objBottom-winTop)-2*offset >= divHeight)  {divTip.style.top = objTop + 'px';}
    else if ((objTop-winTop)-2*offset >= divHeight)          {divTip.style.top = (objBottom-divHeight+shadowOffset) + 'px';}
    else if (winHeight-2*offset >= divHeight)                {divTip.style.top = (winTop+(winHeight-(divHeight-shadowOffset))/2) + 'px';}
    else
    {
        divTip.style.height = (winHeight-4*offset) + 'px';
        calcDimensions (obj, e);
        if ((objLeft-winLeft)-2*offset < divWidth)
        {
            divTip.style.width = ((objLeft-winLeft)-2*offset) + 'px';
            calcDimensions (obj, e);
        }
        divTip.style.top = (winTop+(winHeight-(divHeight-shadowOffset))/2) + 'px';
    }
    divTip.style.left = (objLeft-divWidth-offset) + 'px';
    position = 'left';
    display ();
}

function displayRight (obj, e)
{
    if (winHeight-(objBottom-winTop)-2*offset >= divHeight)  {divTip.style.top = objTop + 'px';}
    else if ((objTop-winTop)-2*offset >= divHeight)          {divTip.style.top = (objBottom-divHeight+shadowOffset) + 'px';}
    else if (winHeight-2*offset >= divHeight)                {divTip.style.top = (winTop+(winHeight-(divHeight-shadowOffset))/2) + 'px';}
    else
    {
        divTip.style.height = (winHeight-4*offset) + 'px';
        calcDimensions (obj, e);
        if (winWidth-(objRight-winLeft)-2*offset < divWidth)
        {
            divTip.style.width = (winWidth-(objRight-winLeft)-2*offset) + 'px';
            calcDimensions (obj, e);
        }
        divTip.style.top = (winTop+(winHeight-(divHeight-shadowOffset))/2) + 'px';
    }
    divTip.style.left = (objRight+offset+shadowOffset) + 'px';
    position = 'right';
    display ();
}
function display ()
{
	if(navigator.userAgent.toLowerCase().indexOf("msie")!=-1)
	{
	if(document.getElementById('iframetop')!=null)
    {
		//Fix for overlapping dropdowns
		// show layer
        var layer = document.getElementById(divTip.id);
        layer.style.display = 'block';
        // show IFRAME
        var iframe = document.getElementById('iframetop');
        iframe.style.display = 'block';
        iframe.style.width = layer.offsetWidth-5;
        iframe.style.height = layer.offsetHeight-5;
        iframe.style.left = layer.offsetLeft;
        iframe.style.top = layer.offsetTop;
    }   
    }
}
function calcDimensions (obj, e)
{
	winWidth = new Number (document.body.clientWidth);
	winHeight = new Number (document.body.clientHeight);
	winLeft = new Number (document.body.scrollLeft);
    winTop = new Number (document.body.scrollTop);
    objLeft = new Number (calcOffset(obj,'left'));
    objRight = new Number (calcOffset(obj,'left')+obj.offsetWidth);
    objTop = new Number (calcOffset(obj,'top'));
    objBottom = new Number (calcOffset(obj,'top')+obj.offsetHeight);
	divWidth = new Number (divTip.offsetWidth);
	divHeight = new Number (divTip.offsetHeight);
	position = '';
	shadowOffset = (divTip.filters && divTip.filters[0]) ? divTip.filters[0].Strength-1 : 0;
}

function displayBaloonTip (obj, e)
{
	if (window.event)
	{
		e.cancelBubble = true;
	}
	if (divTip!=null)
	{
        divTip.style.visibility = 'hidden';
        divTip = null;
		cancelHideBaloonTip();
	}

	divTip = document.getElementById(obj.rel);
	divTip.style.left = '';
	divTip.style.top = '';
	divTip.style.width = '';
	divTip.style.height = '';
    divTip.onmouseover = cancelHideBaloonTip;
    divTip.onmouseout = hideBaloonTip;
	if (typeof obj.tipwidth != 'undefined')
	{	
        divTip.style.width = obj.tipwidth + 'px';
	}

	calcDimensions (obj, e);

	if (typeof obj.pref != 'undefined')
	{
		switch (obj.pref)
		{
			case 'up':
				displayUp (obj, e);
				break;
			case 'down':
				displayDown (obj, e);
				break;
			case 'left':
				displayLeft (obj, e);
				break;
			case 'right':
				displayRight (obj, e);
				break;
		}
	}
	else
	{
		if ((objTop-winTop)-2*offset >= divHeight)				{displayUp (obj, e);}
		else if (winHeight-(objBottom-winTop)-2*offset >= divHeight)		{displayDown (obj, e);}
		else
		{
    		if ((objLeft-winLeft)-2*offset >= divWidth)						{displayLeft (obj, e);}
    		else if (winWidth-(objRight-winLeft)-2*offset >= divWidth)		{displayRight (obj, e);}
    		else
    		{
				if ((objLeft-winLeft) > winWidth-(objRight-winLeft))
				{
					divTip.style.width = ((objLeft-winLeft)-4*offset) + 'px';
					calcDimensions (obj, e);
					displayLeft (obj, e);
				}
				else
				{
					divTip.style.width = ((winWidth-(objRight-winLeft))-4*offset) + 'px';
					calcDimensions (obj, e);
					displayRight (obj, e);
				}
    		}
		}
	}

    divTip.style.visibility = 'visible';

	var tipArrow = document.getElementById('arrowHead');
	switch (position)
	{
        case 'up':
            tipArrow.src = arrowImageUp;
            tipArrow.style.left = (objLeft+objRight-arrowWidth)/2 + 'px';
            tipArrow.style.top = (objTop-arrowHeight) + 'px';
            break;
        case 'down':
            tipArrow.src = arrowImageDown;
            tipArrow.style.left = (objLeft+objRight-arrowWidth)/2 + 'px';
            tipArrow.style.top = (objBottom) + 'px';
            break;
        case 'left':
            tipArrow.src = arrowImageLeft;
            tipArrow.style.top = (objTop+objBottom-arrowWidth)/2 + 'px';
            tipArrow.style.left = (objLeft-arrowHeight) + 'px';
            break;
        case 'right':
            tipArrow.src = arrowImageRight;
            tipArrow.style.top = (objTop+objBottom-arrowWidth)/2 + 'px';
            tipArrow.style.left = (objRight) + 'px';
            break;
	}
	switch (position)
	{
        case 'up':
        case 'down':
            tipArrow.style.width = arrowWidth + 'px';
            tipArrow.style.height = arrowHeight + 'px';
            break;
        case 'left':
        case 'right':
            tipArrow.style.width = arrowHeight + 'px';
            tipArrow.style.height = arrowWidth + 'px';
            break;
	}
	if(navigator.userAgent.indexOf("Firefox")!=-1)
	{document.body.appendChild(tiparrow);}
	tiparrow.style.visibility = 'visible';
	tiparrow.style.display = '';
	
}

function cancelHideBaloonTip()
{
	if (evtTO)
	{
        clearTimeout (evtTO);
	}
}

function hideBaloonTip()
{
	if(navigator.userAgent.toLowerCase().indexOf("msie")!=-1)
	{
	if(document.getElementById('iframetop')!=null)
	{
		// hide IFRAME
		var iframe = document.getElementById('iframetop');
		iframe.style.display = 'none';
		// hide layer
		var layer = document.getElementById(divTip.id);
		layer.style.visibility = 'hidden';	    
    }
    }
	var codeStr = '';
	codeStr += 'try';
	codeStr += '{';
	codeStr += 'divTip.style.visibility=\'hidden\';';
	codeStr += 'divTip = null;';
	codeStr += 'tiparrow.style.visibility=\'hidden\'';
	codeStr += '}';
	codeStr += 'catch (e)';
	codeStr += '{';
	codeStr += '}';
	cancelHideBaloonTip();
	evtTO = setTimeout (codeStr, delay);

}
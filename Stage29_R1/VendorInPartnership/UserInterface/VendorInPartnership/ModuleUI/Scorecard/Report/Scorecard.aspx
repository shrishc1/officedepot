﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    AutoEventWireup="true" CodeFile="Scorecard.aspx.cs" Inherits="ModuleUI_Scorecard_Report_Scorecard" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="SM1" runat="server">
    </asp:ScriptManager>
    <rsweb:ReportViewer ID="ScorecardReportViewer" runat="server" Width="990px" DocumentMapCollapsed="True"
        Font-Names="Verdana" Font-Size="8pt" InteractiveDeviceInfos="(Collection)" WaitMessageFont-Names="Verdana"
        WaitMessageFont-Size="14pt" Height="416px" OnDrillthrough="ScorecardReportViewer_Drillthrough"
        ShowZoomControl="false" ShowRefreshButton="false" ShowPrintButton="false">
    </rsweb:ReportViewer>
    <label id="UserName" runat="server" />
    <cc1:ucButton ID="UcButton1" runat="server" Text="Generate Report" CssClass="button"
        OnClick="btnGenerateReport_Click" />
</asp:Content>

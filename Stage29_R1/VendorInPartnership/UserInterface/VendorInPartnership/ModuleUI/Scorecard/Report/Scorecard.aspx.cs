﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Reporting.WebForms;
using System.Data.SqlClient;
using System.Data;

public partial class ModuleUI_Scorecard_Report_Scorecard : CommonPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        

    }


    protected void ScorecardReportViewer_Drillthrough(object sender, DrillthroughEventArgs e)
    {
        DataSet Level1 = new DataSet();
        DataSet Level2 = new DataSet();
        DataSet objDataSet = new DataSet();
        string lblParameter = string.Empty;
        ReportParameterInfoCollection DrillThroughValues = e.Report.GetParameters();
        //foreach (ReportParameterInfo d in DrillThroughValues)
        //{
        //    lblParameter = d.Values[0].ToString().Trim();
        //}

        LocalReport localreport = (LocalReport)e.Report;
        SqlConnection objSqlConnection = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["sConn"].ToString());
        objSqlConnection.Open();

        SqlCommand objSqlCommand = new SqlCommand("spDemoTableForChart", objSqlConnection);
        objSqlCommand.Parameters.Add("@Action", SqlDbType.Int).Value = 2;
        objSqlCommand.CommandType = CommandType.StoredProcedure;
        SqlDataAdapter objSqlDataAdapter = new SqlDataAdapter(objSqlCommand);
        objSqlDataAdapter.Fill(Level1);

        SqlCommand objSqlCommand1 = new SqlCommand("spDemoTableForChart", objSqlConnection);
        objSqlCommand1.Parameters.Add("@Action", SqlDbType.Int).Value = 3;
        objSqlCommand1.CommandType = CommandType.StoredProcedure;
        SqlDataAdapter objSqlDataAdapter1 = new SqlDataAdapter(objSqlCommand1);
        objSqlDataAdapter1.Fill(Level2);


        SqlCommand objSqlCommand2 = new SqlCommand("spDemoTableForChart", objSqlConnection);
        objSqlCommand2.Parameters.Add("@Action", SqlDbType.Int).Value = 1;
        objSqlCommand2.CommandType = CommandType.StoredProcedure;
        SqlDataAdapter objSqlDataAdapter2 = new SqlDataAdapter(objSqlCommand2);
        objSqlDataAdapter2.Fill(objDataSet);

        ReportDataSource rds = new ReportDataSource("dtScorecard", objDataSet.Tables[0]);
        ReportDataSource rds1 = new ReportDataSource("dtScorecardLevel1", Level1.Tables[0]);
        ReportDataSource rds2 = new ReportDataSource("dtScorecardLevel2", Level2.Tables[0]);
        localreport.DataSources.Clear();
        localreport.DataSources.Add(rds);
        localreport.DataSources.Add(rds1);
        localreport.DataSources.Add(rds2);
        localreport.Refresh();

    }
    protected void btnGenerateReport_Click(object sender, EventArgs e)
    {
        DataSet objDataSet = new DataSet();
        SqlConnection objSqlConnection = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["sConn"].ToString());
        objSqlConnection.Open();
        SqlCommand objSqlCommand = new SqlCommand("spDemoTableForChart", objSqlConnection);
        objSqlCommand.Parameters.Add("@Action", SqlDbType.Int).Value = 1;
        objSqlCommand.CommandType = CommandType.StoredProcedure;
        SqlDataAdapter objSqlDataAdapter = new SqlDataAdapter(objSqlCommand);
        objSqlDataAdapter.Fill(objDataSet);

        ReportDataSource rds = new ReportDataSource("dtScorecard", objDataSet.Tables[0]);
        ScorecardReportViewer.LocalReport.ReportPath = Server.MapPath("~") + "\\ModuleUI\\Scorecard\\Report\\Scorecard.rdlc";
        ScorecardReportViewer.LocalReport.DataSources.Clear();
        ScorecardReportViewer.LocalReport.DataSources.Add(rds);
    }
}
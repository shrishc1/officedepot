﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    AutoEventWireup="true" CodeFile="CMN_CommunicationErrorOverView.aspx.cs" Inherits="ModuleUI_Security_CMN_CommunicationErrorOverView" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h2>
        <cc1:ucLabel ID="lblCommunicationError" runat="server" Text="Communication Error"></cc1:ucLabel>
    </h2>
    <div style='overflow-x: auto; width: 1000px'>
        <cc1:ucGridView runat="server" AutoGenerateColumns="false" ID="gvCommunicationError"
            CssClass="grid" AllowSorting="true">
            <Columns>
                <asp:BoundField HeaderText="Error No." SortExpression="CommunicationErrorID" DataField="CommunicationErrorID">
                    <HeaderStyle Width="5%" HorizontalAlign="Left" />
                    <ItemStyle HorizontalAlign="Left" />
                </asp:BoundField>
                <asp:BoundField HeaderText="VDR No." SortExpression="VDRNo" DataField="VDRNo">
                    <HeaderStyle Width="8%" HorizontalAlign="Left" />
                    <ItemStyle HorizontalAlign="Left" />
                </asp:BoundField>
                <asp:BoundField HeaderText="Sent on" SortExpression="sentDate" DataField="sentDate"
                    DataFormatString="{0:dd/MM/yyyy}">
                    <HeaderStyle Width="12%" HorizontalAlign="Left" />
                    <ItemStyle HorizontalAlign="Left" />
                </asp:BoundField>
                <asp:BoundField HeaderText="Sent To" SortExpression="SentTo" DataField="SentTo">
                    <HeaderStyle Width="15%" HorizontalAlign="Left" />
                    <ItemStyle HorizontalAlign="Left" />
                </asp:BoundField>
                <asp:BoundField HeaderText="Subject" SortExpression="Subject" DataField="Subject">
                    <HeaderStyle Width="25%" HorizontalAlign="Left" />
                    <ItemStyle HorizontalAlign="Left" />
                </asp:BoundField>
                <%--    <asp:BoundField HeaderText="Body" SortExpression="Body" DataField="Body">
                    <HeaderStyle Width="8%" HorizontalAlign="Left" />
                    <ItemStyle HorizontalAlign="Left" />
                </asp:BoundField>--%>
                <asp:BoundField HeaderText="Error Description" SortExpression="ErrorDescription"
                    DataField="ErrorDescription">
                    <HeaderStyle Width="30%" HorizontalAlign="Left" />
                    <ItemStyle HorizontalAlign="Left" />
                </asp:BoundField>
                <asp:TemplateField HeaderText="Close">
                    <HeaderStyle Width="50px" HorizontalAlign="center" />
                    <ItemTemplate>
                        <asp:ImageButton ID="btnClose" runat="server" ImageUrl="~/Images/delete.gif" OnCommand="btnClose_Click"
                            CommandArgument='<%#Eval("CommunicationErrorID") %>' />
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" />
                </asp:TemplateField>
            </Columns>
        </cc1:ucGridView>
    </div>
</asp:Content>

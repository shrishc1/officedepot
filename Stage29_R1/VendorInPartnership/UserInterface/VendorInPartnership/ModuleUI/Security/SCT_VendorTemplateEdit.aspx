﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    AutoEventWireup="true" CodeFile="SCT_VendorTemplateEdit.aspx.cs" Inherits="ModuleUI_Security_SCT_VendorTemplateEdit" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectVendor.ascx" TagName="MultiSelectVendor"
    TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript">
        function confirmDelete() {
            return confirm('<%= deleteMessage %>');
        }
 </script>
 
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <h2>
        <cc1:ucLabel ID="lblVendorSelectionTemplate" runat="server"></cc1:ucLabel>
    </h2>
    
    <table width="100%">
    <tr>
    <td align="right">
    <cc1:ucButton ID="btnExportToExcel" runat="server" Visible="true" CssClass="exporttoexcel" OnClick="btnExport_Click"
                             Width="109px" Height="20px" />
    </td>
    </tr>
    </table>
    
                             <br />
                             <br />
<div class="right-shadow">
        <div class="formbox">
            <table width="45%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                <tr>
                    <td style="width: 5%">
                    </td>
                    <td style="font-weight: bold;" width="28%">
                        <cc1:ucLabel runat="server" ID="lblTemplateName" Width="150px" isRequired="true"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold;" width="5%" align="center">
                        :
                    </td>
                    <td style="font-weight: bold;" width="57%">
                        &nbsp;
                        <cc1:ucTextbox runat="server" ID="txtTemplate" CssClass="inputbox" ClientIDMode="Static"
                            MaxLength="50" />
                        <asp:RequiredFieldValidator ID="rfvTemplateRquired" runat="server" ControlToValidate="txtTemplate"
                             ValidationGroup="save" Display="None" />
                        <asp:RegularExpressionValidator ID="revTemplateValidate" runat="server"  ValidationGroup="save"  Display="None"
                            ControlToValidate="txtTemplate" ValidationExpression="^[a-zA-Z ]+$"></asp:RegularExpressionValidator>
                    </td>
                    <td style="width: 5%">
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td style="font-weight: bold">
                        <cc1:ucLabel runat="server" ID="lblVendor" Width="150px" isRequired="true"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold;" align="center">
                        :
                    </td>
                    <td>
                        <cc1:MultiSelectVendor runat="server" ID="msVendor" />
                    </td>
                </tr>
            </table>
         
        </div>
    </div>
    <div class="bottom-shadow">
    </div>
    <div class="button-row">
                <asp:ValidationSummary runat="server" ID="vs" ShowMessageBox="true" ShowSummary="false"
                    ValidationGroup="save" />
                <cc1:ucButton runat="server" Text="Save" ID="btnSave" OnClick="btnSave_Click" CssClass="button"
                    ValidationGroup="save" />                
                <cc1:ucButton runat="server" Text="Delete" Visible="false" ID="btnDelete" OnClick="btnDelete_Click" CssClass="button" 
                OnClientClick="return confirmDelete();"/>
                <cc1:ucButton ID="btnBack" runat="server" CssClass="button" Text="Back" OnClick="btnBack_Click" />
            </div>
</asp:Content>

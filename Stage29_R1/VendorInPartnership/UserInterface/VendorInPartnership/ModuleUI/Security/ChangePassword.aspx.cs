﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Utilities; using WebUtilities;
using BusinessEntities.ModuleBE.Security;
using BusinessLogicLayer.ModuleBAL.Security;


public partial class ModuleUI_Security_ChangePassword : AA.switchprotocol.UI.SecurePage
{
    private string IsPasswordStrongMessage = WebCommon.getGlobalResourceValue("IsPasswordStrongMessage");

    protected void Page_Load(object sender, EventArgs e)
    {
        txtCurrentPassword.Focus();
    }

    protected void btnOK_Click(object sender, EventArgs e)
    {
        SCT_UserBE oSCT_UserBE = new SCT_UserBE();
        SCT_UserBAL oSCT_UserBAL = new SCT_UserBAL();

       
        string NewPassword = txtNewPassword.Text;
        string ConfirmedNewPassword = txtConfirmPassword.Text;

        if (!Common.IsPasswordStrong(txtNewPassword.Text)) {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + IsPasswordStrongMessage + "');", true);
            return;
        }

        if (NewPassword == ConfirmedNewPassword)
        {
            oSCT_UserBE.Action = "ChangePassword";
            oSCT_UserBE.UserID = Convert.ToInt32(Session["UserID"]);
            oSCT_UserBE.Password = txtCurrentPassword.Text;

            int? statusFlag = oSCT_UserBAL.ChangePasswordBAL(oSCT_UserBE, NewPassword);
            oSCT_UserBAL = null;
            string PasswordChangedMessage;
            if (statusFlag == 1)
            {
                PasswordChangedMessage = WebCommon.getGlobalResourceValue("PasswordChanged");
                ScriptManager.RegisterClientScriptBlock(btnOK, this.btnOK.GetType(), "alert", "alert('" + PasswordChangedMessage + "')", true);
                //EncryptQueryString("../Dashboard/DefaultDashboard.aspx");
                txtConfirmPassword.Text = "";
                txtCurrentPassword.Text = "";
                txtNewPassword.Text="";

            }
            else
            {
                PasswordChangedMessage = WebCommon.getGlobalResourceValue("PasswordNotChanged");
                ScriptManager.RegisterClientScriptBlock(btnOK, this.btnOK.GetType(), "alert", "alert('" + PasswordChangedMessage + ".')", true);
                return;
            }

           
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        EncryptQueryString("../Dashboard/DefaultDashboard.aspx");
    }
}
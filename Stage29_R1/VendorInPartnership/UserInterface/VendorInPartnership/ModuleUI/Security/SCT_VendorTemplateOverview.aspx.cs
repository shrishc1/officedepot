﻿using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.Security;
using BusinessLogicLayer.ModuleBAL.Security;

public partial class ModuleUI_Security_SCT_VendorTemplateOverview : CommonPage
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            BindVendorTemplate();
        }
        ucExportToExcel1.GridViewControl = gvVendorTemplate;
        ucExportToExcel1.FileName = "VendorTemplate";
    }
    #region Methods

    protected void BindVendorTemplate()
    {

        SCT_TemplateBE oSCT_TemplateBE = new SCT_TemplateBE();
        SCT_TemplateBAL oSCT_TemplateBAL = new SCT_TemplateBAL();

        oSCT_TemplateBE.Action = "ShowVendorTemplate";

        List<SCT_TemplateBE> lstTemplate = oSCT_TemplateBAL.GetVendorTemplate(oSCT_TemplateBE);
        oSCT_TemplateBAL = null;
        if (lstTemplate.Count > 0)
        {
            gvVendorTemplate.DataSource = lstTemplate;
            gvVendorTemplate.DataBind();
            ViewState["lstTemplate"] = lstTemplate;
        }
    }
    public override BusinessEntities.BaseBe[] SubSort(GridViewSortEventArgs e)
    {
        return Utilities.GenericListHelper<SCT_TemplateBE>.SortList((List<SCT_TemplateBE>)ViewState["lstTemplate"], e.SortExpression, e.SortDirection).ToArray();
    }

    #endregion
}
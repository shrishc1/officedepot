﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SCT_RegisterExternalUser.aspx.cs"
    MasterPageFile="~/CommonUI/CMN_MasterPages/NoUser_MasterPage.Master" Inherits="SCT_RegisterExternalUser"
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="../../CommonUI/UserControls/ucCountry.ascx" TagName="ucCountry"
    TagPrefix="uc1" %>
<%@ Register Src="~/CommonUI/UserControls/ucSeacrhVendorWithoutSM.ascx" TagName="ucSeacrhVendorWithoutSM"
    TagPrefix="cc4" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" ScriptMode="Release"></asp:ScriptManager>
    <script src="../../Scripts/ToolTipScript.js" type="text/javascript"></script>
    <script src="../../Scripts/balloontip.js" type="text/javascript"></script>
    <link href="../../Css/balloontip.css" rel="stylesheet" type="text/css" />
    
    <script type='text/javascript' language="javascript">

        var varAlredySelectedVendor = '<%= this.AlredySelectedVendor %>';

        var _oldonerror = window.onerror;
        window.onerror = function (errorMsg, url, lineNr) { return true; };
        function setToolTip() {

            initBaloonTips();
        }

        document.onmousemove = positiontip

        function checkVendorSelected(source, args) {
        }

        function sendIdForMoveRight(cControl) {
            var varControlId = cControl.id;
            var varControlIndex = varControlId.lastIndexOf('_');
            var varControlPrefix = varControlId.substring(0, varControlIndex) + '_';
            var varLstLeft = varControlPrefix + 'lstLeft'
            var varLstRight = varControlPrefix + 'lstRight'
            var varHiddenSelectedIDs = varControlPrefix + 'hiddenSelectedIDs'
            var varHiddenSelectedName = varControlPrefix + 'hiddenSelectedName'
            var varHiddenSearchedVendorText = varControlPrefix + 'hiddenSearchedVendorText'
            var varHiddenSearchedVendorValue = varControlPrefix + 'hiddenSearchedVendorValue'

            var varSource = document.getElementById(varLstLeft);
            var varDestination = document.getElementById(varLstRight);
            var varSearchedVendorText = document.getElementById(varHiddenSearchedVendorText);
            var varSearchedVendorValue = document.getElementById(varHiddenSearchedVendorValue);

            if (varDestination.options.length == 0 && varSource.options.selectedIndex >= 0) {
                varSearchedVendorText.value = varSource.options[varSource.options.selectedIndex].text;
                varSearchedVendorValue.value = varSource.options[varSource.options.selectedIndex].value;
                MoveItem(varLstLeft, varLstRight, varHiddenSelectedIDs, varHiddenSelectedName);
            }
            else if (varDestination.options.length != 0) {
                alert(varAlredySelectedVendor);
            }
            return false;
        }

        function sendIdForMoveLeft(cControl) {
            var varControlId = cControl.id;
            var varControlIndex = varControlId.lastIndexOf('_');
            var varControlPrefix = varControlId.substring(0, varControlIndex) + '_';
            var varLstLeft = varControlPrefix + 'lstLeft'
            var varLstRight = varControlPrefix + 'lstRight'
            var varHiddenSelectedIDs = varControlPrefix + 'hiddenSelectedIDs'
            var varHiddenSelectedName = varControlPrefix + 'hiddenSelectedName'
            MoveItem(varLstRight, varLstLeft, varHiddenSelectedIDs, varHiddenSelectedName);
            return false;
        }

        function countryDropDownEnableDisable(cControl) {
            var varControlId = cControl.id;
            var varControlIndex = varControlId.lastIndexOf('_');
            var varControlPrefix = varControlId.substring(0, varControlIndex) + '_';
            var varChkRptCountry = varControlPrefix + 'chkRptCountry'
            var varDdlRptCarrier = varControlPrefix + 'ddlRptCarrier'

            var chkRptCountry = document.getElementById(varChkRptCountry);
            var ddlRptCarrier = document.getElementById(varDdlRptCarrier);

            if (chkRptCountry.checked == true) {
                ddlRptCarrier.disabled = false;
            }
            else {
                ddlRptCarrier.disabled = true;
            }
        }

        function showVendor(cControl) {
            var varControlId = cControl.id;
            var varControlIndex = varControlId.lastIndexOf('_');
            var varControlPrefix = varControlId.substring(0, varControlIndex) + '_';

            var varMdlVendorViewer = varControlPrefix + 'mdlVendorViewer'
            var varIsSearch = varControlPrefix + 'isSearch'

            $find(varMdlVendorViewer).show();
            document.getElementById(varIsSearch).value = "true";
            return false;
        }


    </script>

    <input type="hidden" id="scroll1" runat="server" />
    <input type="hidden" id="scroll2" runat="server" />
    <div id="divVolume" runat="server" class="balloonstyle">
    </div>
    <div id="dhtmltooltip" class="balloonstyle">
    </div>
    <iframe id="iframetop" scrolling="no" frameborder="0" class="iballoonstyle" width="0"
        height="0"></iframe>
    <h2>
        <cc1:ucLabel ID="lblRegisterAsNewUser" runat="server" Text="Register As New User"></cc1:ucLabel>
    </h2>
   <noscript><div style="color:Red;font-size:11px;font-weight:bold;"><center>Java Script is disabled on your browser, so some of the functionality may not work as expected. <br />
Please enable Java Script or upgrade to a Java Script-capable browser to use Vendors In Partnership portal.</center></div></noscript>

    <%--<asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>--%>
            <div class="right-shadow">
                <div class="formbox">
                    <cc1:ucMultiView ID="mvExternalUserEdit" runat="server" ActiveViewIndex="0">
                        <cc1:ucView ID="vwCountrySelection" runat="server">
                            <!--Need to put code for country selection....-->
                            <cc1:ucPanel ID="pnlCountrySelectionCountrySpecSett" runat="server" GroupingText="Country Selection Country Spec Sett" CssClass="fieldset-form" style="display:none;" >
                                <table width="100%" cellspacing="5" align="center" cellpadding="0" border="0" class="top-settingsNoBorder">
                                    <tr>
                                        <td align="center" valign="top" style="width:8%;">
                                            <cc1:ucLiteral ID="ltEnglishCountrySpecSett" runat="server"></cc1:ucLiteral>
                                        </td>
                                        <td align="center" valign="top" style="width:8%;">
                                            <cc1:ucLiteral ID="ltFranceCountrySpecSett" runat="server"></cc1:ucLiteral>
                                        </td>
                                        <td align="center" valign="top" style="width:8%;">
                                            <cc1:ucLiteral ID="ltGermanyCountrySpecSett" runat="server"></cc1:ucLiteral>
                                        </td>
                                        <td align="center" valign="top" style="width:8%;">
                                            <cc1:ucLiteral ID="ltSpainCountrySpecSett" runat="server"></cc1:ucLiteral>
                                        </td>
                                        <td align="center" valign="top" style="width:8%;">
                                            <cc1:ucLiteral ID="ltItalyCountrySpecSett" runat="server"></cc1:ucLiteral>
                                        </td>
                                        <td align="center" valign="top" style="width:8%;">
                                            <cc1:ucLiteral ID="ltNederlandCountrySpecSett" runat="server"></cc1:ucLiteral>
                                        </td>
                                        <td align="center" valign="top" style="width:8%;">
                                            <cc1:ucLiteral ID="lblCzechCountrySpecSett" runat="server"
                                            ></cc1:ucLiteral>
                                        </td>
                                        <%--<td align="center" valign="middle">                                            
                                            <cc1:ucLabel ID="lblSwedenCountrySpecSett" runat="server"
                                            Text="<%$ Resources:WebResources, SwedenCountrySpecSett %>" ></cc1:ucLabel>
                                        </td>--%>
                                    </tr>
                                    <tr>
                                        <td align="center" valign="middle" style="width:8%;">
                                            <cc1:ucImageButton ID="btnEnglishCountrySpecSett" 
                                            ImageUrl="~/Images/flags/UnitedKingdomFlag.JPG" runat="server" 
                                                onclick="btnEnglishCountrySpecSett_Click"></cc1:ucImageButton>
                                        </td>
                                        <td align="center" valign="middle" style="width:8%;">
                                            <cc1:ucImageButton ID="btnFranceCountrySpecSett" 
                                            ImageUrl="~/Images/flags/FranceFlag.JPG" runat="server" 
                                                onclick="btnFranceCountrySpecSett_Click"></cc1:ucImageButton>
                                        </td>
                                        <td align="center" valign="middle" style="width:8%;">
                                            <cc1:ucImageButton ID="btnGermanyCountrySpecSett" 
                                            ImageUrl="~/Images/flags/GermanyFlag.JPG" runat="server" 
                                                onclick="btnGermanyCountrySpecSett_Click"></cc1:ucImageButton>
                                        </td>
                                        <td align="center" valign="middle" style="width:8%;">
                                            <cc1:ucImageButton ID="btnSpainCountrySpecSett" 
                                            ImageUrl="~/Images/flags/SpainFlag.JPG" runat="server" 
                                                onclick="btnSpainCountrySpecSett_Click"></cc1:ucImageButton>
                                        </td>
                                        <td align="center" valign="middle" style="width:8%;">
                                            <cc1:ucImageButton ID="btnItalyCountrySpecSett" 
                                            ImageUrl="~/Images/flags/ItalyFlag.JPG" runat="server" 
                                                onclick="btnItalyCountrySpecSett_Click"></cc1:ucImageButton>
                                        </td>
                                        <td align="center" valign="middle" style="width:8%;">
                                            <cc1:ucImageButton ID="btnNederlandCountrySpecSett" 
                                            ImageUrl="~/Images/flags/NetherlandFlag.JPG" runat="server" 
                                                onclick="btnNederlandCountrySpecSett_Click"></cc1:ucImageButton>
                                        </td>
                                        <td align="center" valign="middle" style="width:8%;">
                                            <asp:ImageButton ID="btnCzechCountrySpecSett" 
                                            ImageUrl="~/Images/flags/CzechRepublicFlag.JPG" runat="server" 
                                                onclick="btnCzechCountrySpecSett_Click"></asp:ImageButton>
                                        </td>
                                        <%--<td align="center" valign="middle">
                                            <asp:ImageButton ID="btnSwedenCountrySpecSett" 
                                            ImageUrl="~/Images/flags/SwedenFlag.JPG" runat="server" 
                                                onclick="btnSwedenCountrySpecSett_Click"></asp:ImageButton>
                                        </td>--%>
                                    </tr>
                                </table>
                            </cc1:ucPanel>
                        </cc1:ucView>
                        <cc1:ucView ID="vwUserType" runat="server">
                            <cc1:ucPanel ID="pnlRegisterAs" runat="server" GroupingText="Register As" CssClass="fieldset-form">
                                <table width="50%" cellspacing="5" align="center" cellpadding="0" border="0" class="top-settingsNoBorder">
                                    <tr>
                                        <td align="center">
                                            <cc1:ucButton ID="btnVendor" runat="server" Text="Vendor" CssClass="button" OnClick="btnVendor_Click" />
                                            &nbsp;&nbsp;
                                            <cc1:ucButton ID="btnCarrier" runat="server" Text="Carrier" CssClass="button" OnClick="btnCarrier_Click" />
                                        </td>
                                    </tr>
                                </table>
                            </cc1:ucPanel>
                        </cc1:ucView>
                        <cc1:ucView ID="vwPersonalDetails" runat="server">
                            <cc1:ucPanel ID="pnlPersonalDetails" runat="server" GroupingText="Personal Details"
                                CssClass="fieldset-form">
                                <table width="100%" cellspacing="5" cellpadding="0" class="form-table">
                                    <tr>
                                        <td style="font-weight: bold;" width="9%">
                                            <cc1:ucLabel ID="lblUserIdEmail" runat="server" Text="User ID/Email" isRequired="true"></cc1:ucLabel>
                                        </td>
                                        <td style="font-weight: bold;" width="1%" align="center">
                                            :
                                        </td>
                                        <td style="font-weight: bold;" width="40%">
                                            <cc1:ucTextbox ID="txtUserIDEmail" runat="server" Width="220px" MaxLength="50"></cc1:ucTextbox>
                                            <asp:RequiredFieldValidator ID="rfvUsernameReq" runat="server" ControlToValidate="txtUserIDEmail"
                                                ValidationGroup="Save" ErrorMessage="Please enter User ID/Email" Display="None"></asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ID="revUserIDEmailreq" ControlToValidate="txtUserIDEmail"
                                                runat="server" ValidationGroup="Save" ErrorMessage="Please enter Correct User ID/Email"
                                                Display="None" ValidationExpression="^[\w-\.\']{1,}\@([\da-zA-Z-]{1,}\.){1,3}[\da-zA-Z-]{2,6}$" />
                                        </td>
                                        <td style="font-weight: bold;" align="left" width="50%" colspan="3">
                                            &nbsp;
                                        </td>                                        
                                    </tr>
                                    <tr>
                                        <td style="font-weight: bold">
                                            <cc1:ucLabel ID="lblPassword" runat="server" Text="Password" isRequired="true"></cc1:ucLabel>
                                            <asp:HiddenField ID="hiddenPassword" runat="server" />
                                        </td>
                                        <td style="font-weight: bold;" align="center" runat="server" id="tdpassword">
                                            :
                                        </td>
                                        <td colspan="4">
                                            <cc1:ucTextbox ID="txtPassword" runat="server" Width="130px" MaxLength="50" TextMode="Password"></cc1:ucTextbox>
                                            <asp:RequiredFieldValidator ID="rfvPasswordReq" runat="server" ControlToValidate="txtPassword"
                                                ValidationGroup="Save" ErrorMessage="Please enter Password" Display="None"></asp:RequiredFieldValidator>
                                         &nbsp;<cc1:ucLabel ID="lblUserIdEmailLitral" runat="server"></cc1:ucLabel></td>
                                      
                                    </tr>
                                    <tr>
                                        <td style="font-weight: bold">
                                            <cc1:ucLabel ID="lblFirstName" runat="server" Text="First Name" isRequired="true"></cc1:ucLabel>
                                        </td>
                                        <td style="font-weight: bold;" align="center">
                                            :
                                        </td>
                                        <td>
                                            <cc1:ucTextbox ID="txtFirstName" runat="server" Width="130px" MaxLength="50"></cc1:ucTextbox>
                                            <asp:RequiredFieldValidator ID="rfvFirstNameReq" runat="server" ControlToValidate="txtFirstName"
                                                ValidationGroup="Save" ErrorMessage="Please enter First Name" Display="None"></asp:RequiredFieldValidator>
                                        </td>
                                        <td style="font-weight: bold">
                                            <cc1:ucLabel ID="lblLastName" runat="server" Text="Last Name" isRequired="true"></cc1:ucLabel>
                                        </td>
                                        <td style="font-weight: bold;" align="center">
                                            :
                                        </td>
                                        <td>
                                            <cc1:ucTextbox ID="txtLastName" runat="server" Width="130px" MaxLength="50"></cc1:ucTextbox>
                                            <asp:RequiredFieldValidator ID="rfvLastNameReq" runat="server" ControlToValidate="txtLastName"
                                                ValidationGroup="Save" ErrorMessage="Please enter Last Name" Display="None"></asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: bold">
                                            <cc1:ucLabel ID="lblTelephone" runat="server" Text="Telephone" isRequired="true"></cc1:ucLabel>
                                        </td>
                                        <td style="font-weight: bold;" align="center">
                                            :
                                        </td>
                                        <td>
                                            <cc1:ucTextbox ID="txtTelephone" runat="server" Width="220px" MaxLength="50"></cc1:ucTextbox>
                                            <asp:RequiredFieldValidator ID="rfvTelephoneReq" runat="server" ControlToValidate="txtTelephone"
                                                ValidationGroup="Save" ErrorMessage="Please enter Telephone No" Display="None"></asp:RequiredFieldValidator>
                                        </td>
                                        <td style="font-weight: bold">
                                            <cc1:ucLabel ID="lblFax" runat="server" Text="Fax"></cc1:ucLabel>
                                        </td>
                                        <td style="font-weight: bold;" align="center">
                                            :
                                        </td>
                                        <td>
                                            <cc1:ucTextbox ID="txtFax" runat="server" Width="220px" MaxLength="50"></cc1:ucTextbox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: bold">
                                            <cc1:ucLabel ID="lblLanguage" runat="server" Text="Language" isRequired="true" Visible="false"></cc1:ucLabel>
                                        </td>
                                        <td style="font-weight: bold;" align="center">
                                            <%--:--%>
                                        </td>
                                        <td colspan="4">
                                            <cc1:ucDropdownList ID="ddlLanguage" runat="server" Width="150px" Visible="false" ValidationGroup="NotRequired">
                                            </cc1:ucDropdownList>
                                            <%--<asp:RequiredFieldValidator ID="rfvLanguageReq" runat="server" ControlToValidate="ddlLanguage"
                                                ValidationGroup="Save" ErrorMessage="Please select Language" Display="None" InitialValue="0" Visible="false"></asp:RequiredFieldValidator>--%>
                                        </td>
                                    </tr>
                                </table>
                            </cc1:ucPanel>
                            <br />
                            <%--  <asp:UpdatePanel ID="up1" runat="server">
                                <ContentTemplate>--%>
                            <cc1:ucPanel ID="pnlCountrySpecific" runat="server" GroupingText="Country Specific Settings"
                                CssClass="fieldset-form" Style="display: block" Visible="false">
                                <table width="100%" cellspacing="5" cellpadding="0" class="form-table" id="tabblock"
                                    onmouseover="setToolTip();">
                                    <tr style="display: block">
                                        <td style="font-weight: bold;" width="10%">
                                            <cc1:ucLabel ID="lblCountry" runat="server" Text="Country" isRequired="true"></cc1:ucLabel>
                                        </td>
                                        <td style="font-weight: bold;" width="1%" align="center">
                                            :
                                        </td>
                                        <td style="font-weight: bold;" width="89%" colspan="2">
                                            <cc1:ucDropdownList ID="ddlCountry" runat="server" Width="150px" AutoPostBack="True"
                                                OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged">
                                            </cc1:ucDropdownList>
                                            <asp:RequiredFieldValidator ID="rfvCountryReq" runat="server" ControlToValidate="ddlCountry"
                                                ValidationGroup="Save" ErrorMessage="Please select Country" Display="None" InitialValue="0"></asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr id="trVendor" runat="server">
                                        <td style="font-weight: bold">
                                            <cc1:ucLabel ID="lblVendorNo" runat="server" Text="Vendor No" isRequired="true"></cc1:ucLabel>
                                        </td>
                                        <td style="font-weight: bold;" align="center">
                                            :
                                        </td>
                                        <td width="2%">
                                            <a href="javascript:void(0)" rel="ssn123" tipwidth="350" onmouseover="ddrivetip('ssn123','#ededed','350');"
                                                onmouseout="hideddrivetip();">
                                                <img src="../../Images/info_button1.gif" align="absMiddle" border="0" />
                                            </a>
                                        </td>
                                        <td>
                                            <!--Removed Search Vendor Control-->
                                        </td>
                                    </tr>
                                    <tr id="trVendorOptions" runat="server">
                                        <td colspan="4">
                                            <table width="100%">
                                                <tr>
                                                    <td style="font-weight: bold; width: 23%">
                                                        <cc1:ucLabel ID="lblVendorCommOptions" runat="server" Text="Please select the applicable option(s) "></cc1:ucLabel>
                                                    </td>
                                                    <td style="font-weight: bold; width: 1%">
                                                        :
                                                    </td>
                                                    <td style="font-weight: bold; width: 23%">
                                                        <cc1:ucCheckbox ID="chkAppointmentScheduling" runat="server" />
                                                    </td>
                                                    <td style="font-weight: bold; width: 23%">
                                                        <cc1:ucCheckbox ID="chkDiscrepancies" runat="server" />
                                                    </td>
                                                    <td style="font-weight: bold; width: 15%">
                                                        <cc1:ucCheckbox ID="chkOTIF" runat="server" />
                                                    </td>
                                                    <td style="font-weight: bold; width: 15%">
                                                        <cc1:ucCheckbox ID="chkScorecard" runat="server" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr id="trCarrier" runat="server">
                                        <td style="font-weight: bold">
                                            <cc1:ucLabel ID="lblCarrier" runat="server" Text="Carrier" isRequired="true"></cc1:ucLabel>
                                        </td>
                                        <td style="font-weight: bold;" align="center">
                                            :
                                        </td>
                                        <td colspan="2">
                                            <cc1:ucDropdownList ID="ddlCarrier" runat="server" Width="150px" OnSelectedIndexChanged="ddlCarrier_SelectedIndexChanged"
                                                AutoPostBack="true">
                                            </cc1:ucDropdownList>
                                            <asp:RequiredFieldValidator ID="rfvCarrierReq" runat="server" ControlToValidate="ddlCarrier"
                                                ValidationGroup="Save" ErrorMessage="Please select Carrier" Display="None" InitialValue="0">
                                            </asp:RequiredFieldValidator>
                                            <cc1:ucTextbox runat="server" ID="txtOtherCarrier" Width="150px" />
                                            <asp:RequiredFieldValidator ID="rfvCarrierRequired" runat="server" ControlToValidate="txtOtherCarrier"
                                                ValidationGroup="Save" ErrorMessage="Carrier Required" Display="None">
                                            </asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    
                                </table>
                            </cc1:ucPanel>                            
                            <cc1:ucPanel ID="pnlCountrySpecificSettings" runat="server" GroupingText="Country Specific Settings"
                                CssClass="fieldset-form" Style="display: block">
                                <table width="100%" cellspacing="5" cellpadding="0" class="form-table" id="tabblocks">
                                    <tr id="tr1" runat="server">
                                        <td colspan="4">
                                            <table width="100%">
                                                <tr>
                                                    <td style="font-weight: bold; width: 100%">
                                                        <cc1:ucLabel ID="lblCountrySpecificLabel" runat="server" Text="Vendor No" isRequired="true"></cc1:ucLabel>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="font-weight: bold; width: 100%" class="checkboxlist">
                                                        <cc1:ucCheckboxList ID="cblCountry" RepeatColumns="10" RepeatDirection="Horizontal"
                                                            runat="server" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="font-weight: bold; width: 100%" align="right">
                                                        <div class="button-row">
                                                            <cc1:ucButton ID="btnBack_1" runat="server" CssClass="button"
                                                                OnClick="btnBack_1_Click" ValidationGroup="Saves" />
                                                            <cc1:ucButton ID="btnProceed" runat="server" CssClass="button"
                                                                OnClick="btnProceed_Click" ValidationGroup="Save" />
                                                            <%--<asp:ValidationSummary ID="VSSave" runat="server" ValidationGroup="Save" ShowSummary="false"
                                                                ShowMessageBox="true" />--%>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </cc1:ucPanel>

                            <asp:UpdatePanel runat="server" ID="UpVendorDetails">
                                <ContentTemplate>
                               

                              <%---Report Viewer popup start--%>
                            <asp:Button ID="btnViewVendor" runat="Server" Style="display: none" />
                            <ajaxtoolkit:modalpopupextender ID="mdlVendorViewer" runat="server" TargetControlID="btnViewVendor"
                                PopupControlID="pnlbtnViewVendorViewer" 
                                 BackgroundCssClass="modalBackground" BehaviorID="VendorViewer" 
                                DropShadow="false" />


                             <asp:Panel ID="pnlbtnViewVendorViewer" runat="server"  Style="display:none; width: 500px;">
                                 <!--Romoved VendorViewer ancor tag from here-->
                                <div style="width: 100%; overflow-y: hidden; overflow-x: hidden; background-color: #fff;
                                    padding: 5px; border: 2px solid #ccc; text-align: left;">
                                    <table width="100%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                                        <tr>
                                            <td style="font-weight: bold;" width="25%">
                                                <cc1:ucLabel ID="lblVendorName" runat="server" Text="Vendor Name"></cc1:ucLabel>
                                            </td>
                                            <td style="font-weight: bold;" width="5%">
                                                :
                                            </td>
                                            <td width="50%">
                                                <cc1:ucTextbox ID="txtVenderName2" runat="server" Width="147px"></cc1:ucTextbox>
                                            </td>
                                            <td width="20%">
                                                <cc1:ucButton OnClick="btnSearch_Click" ID="btnSearch" runat="server" 
                                                    Text="Search" CssClass="button" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                &nbsp;
                                            </td>
                                            <td style="font-weight: bold;" align="left" colspan="2">                           
                                                <cc1:ucListBox ID="lstVendor" runat="server" Width="99%" Height="200px" ondblclick="lstVendor_DoubleClick" ></cc1:ucListBox>
                                                </td>                      
                                            </tr>
                                            <tr>
                                                    <td colspan="4" align="center">
                                                <cc1:ucButton OnClick="btnSelect_Click" ID="btnSelect" runat="server"  
                                                    Text="Select" CssClass="button" Width="60px" />
                                                &nbsp;
                                                <cc1:ucButton OnClick="btnPopupCancel_Click" ID="btnPopupCancel" runat="server" 
                                                    Text="Cancel" CssClass="button" Width="60px" />
                                                </td>
                                            </tr>
                                                <tr>
                                                    <td colspan="4">&nbsp;</td>
                                                </tr>
                                            </table>
                                            </div>
                                        </asp:Panel>

                            <cc1:ucPanel ID="pnlVendorDetailsRegisterExternalUsers" runat="server" GroupingText="Vendor Details Register External Users"
                                CssClass="fieldset-form" Style="display: block" Visible="false">
                                <asp:HiddenField ID="hdntxtVendorNo" runat="server" />
                                <asp:HiddenField ID="hdnCountryID" runat="server" />
                                <asp:HiddenField ID="hdnVendorName" runat="server" />
                                <asp:HiddenField ID="hdnrowNumber" runat="server" />
                                
                                



                            
                              <table width="100%" cellspacing="5" cellpadding="0" class="form-table" id="tblVendorDetailsRegisterExternalUsers">
                                    <tr id="tr2" runat="server">
                                        <td colspan="4">
                                            <table width="100%">
                                                <tr>
                                                    <td style="font-weight: bold; width: 95%">
                                                        <cc1:ucLiteral ID="ltVendorDetailsTextRegisterExternalUsers" runat="server"></cc1:ucLiteral>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="font-weight: bold; width: 100%">
                                                        <asp:Repeater ID="rptVendorDetailsREU" runat="server" 
                                                            onitemdatabound="rptVendorDetailsREU_ItemDataBound" 
                                                            onitemcommand="rptVendorDetailsREU_ItemCommand">
                                                            <ItemTemplate>
                                                                <table width="100%" cellspacing="5" cellpadding="0" class="form-table">
                                                                    <tr>
                                                                        <td style="width:10%"></td>
                                                                        <td style="width:5%;" align="left" valign="middle">
                                                                            <cc1:ucLabel ID="lblCountryVendorDetailsREU" runat="server"></cc1:ucLabel>
                                                                            
                                                                            <asp:HiddenField ID="hdnVendorID" runat="server" />
                                                                            <asp:HiddenField  ID="hdnParentVendorID" runat="server" />
                                                                        </td>  
                                                                        <td style="width:2%;" valign="middle">
                                                                            <cc1:ucLabel ID="lblCountry" runat="server" isRequired="true"></cc1:ucLabel>
                                                                        </td>                                                                        
                                                                        <td style="width:83%;"> </td>                                                                      
                                                                    </tr>
                                                                   

                                                                    <tr valign="top">
                                                                    <td style="width:10%"></td>
                                                                        <td style="font-weight: bold;text-align:left;" width="5%" valign="middle">
                                                                            <cc1:ucTextbox ID="txtVendorNo" runat="server" Width="33px"
                                                                                MaxLength="6" AutoPostBack="true" 
                                                                                ontextchanged="txtVendorNo_TextChanged" ></cc1:ucTextbox>
                                                                        </td>
                                                                            <td style="font-weight: bold;" width="2%;" valign="middle">       
                                                                              <asp:HiddenField ID="hdnCountryIdVendorDetailsREU" runat="server" />          
                                                                            <asp:ImageButton ID="imgVendor" runat="server" ImageUrl="~/Images/Search.gif" 
                                                                            style="height:16px; width:16px;" OnClientClick="return showVendorimg(this);" />
                                                                        </td>
                                                                            <td style="font-weight: bold;" width="83%;">
                                                                            <cc1:ucLabel ID="SelectedVendorName"  BorderWidth="0" runat="server" Width="250px" ></cc1:ucLabel>                                        
                                                                        </td>
                                                                    </tr>
                                                                    

                                                                   

                                                                   <%-- <tr>
                                                                        <td style="width:6%;" valign="middle">
                                                                            <cc1:ucLabel ID="lblSearch" runat="server" isRequired="true" Visible="false"></cc1:ucLabel>
                                                                        </td>
                                                                        <td style="width:20%;" valign="middle">
                                                                            <cc1:ucTextbox ID="txtSearchVendorDetailsREU" runat="server" Visible="false"></cc1:ucTextbox>                                                                            
                                                                        </td>                                                                        
                                                                        <td style="width:2%;" valign="middle">
                                                                        </td>
                                                                        <td style="width:48%;" align="left" valign="middle">
                                                                            <div class="button-row" style="text-align:left !important;">                                                                                
                                                                                <cc1:ucButton ID="btnSearchVendorDetailsREU" Visible="false" CommandName="SearchVendorDetailsREU" CssClass="button" runat="server" />
                                                                            </div>
                                                                            <asp:HiddenField ID="hiddenSearchedVendorText" runat="server" />
                                                                            <asp:HiddenField ID="hiddenSearchedVendorValue" runat="server" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="4">
                                                                            <cc1:ucPanel ID="pnlSearchResult" runat="server" Visible="false">
                                                                                <table width="100%" cellspacing="5" cellpadding="0" class="form-table">
                                                                                    <tr>
                                                                                        <td colspan="2" style="width: 26%;" valign="middle">
                                                                                            <cc1:ucLabel ID="lblSearchResultVendorDetailsREU" runat="server" isRequired="true"></cc1:ucLabel>
                                                                                        </td>
                                                                                        <td style="width: 2%;" valign="middle">
                                                                                        </td>
                                                                                        <td style="width: 48%;" align="left" valign="middle">
                                                                                            <cc1:ucLabel ID="lblYourVendorVendorDetailsREU" runat="server" isRequired="true"></cc1:ucLabel>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td colspan="2" valign="top" style="width: 26%;">
                                                                                            <cc1:ucListBox ID="lstLeft" runat="server" Height="150px" Width="320px">
                                                                                            </cc1:ucListBox>
                                                                                        </td>
                                                                                        <td style="width: 2%;" valign="middle">
                                                                                            &nbsp;
                                                                                            <div>                                                                                                
                                                                                                <asp:Button ID="btnMoveRight" Text=">" runat="server" CssClass="button" style="width: 35px" 
                                                                                                    OnClientClick="return sendIdForMoveRight(this)" />
                                                                                            </div>
                                                                                            &nbsp;
                                                                                            <div>
                                                                                                <asp:Button ID="btnMoveLeft" Text="<" runat="server" CssClass="button" style="width: 35px" 
                                                                                                    OnClientClick="return sendIdForMoveLeft(this)" />                                                                                             
                                                                                            </div>
                                                                                            &nbsp;
                                                                                        </td>
                                                                                        <td style="width: 48%;" align="left" valign="top">
                                                                                            <cc1:ucListBox ID="lstRight" runat="server" Height="20px" Width="320px">
                                                                                            </cc1:ucListBox>
                                                                                            <asp:HiddenField ID="hiddenSelectedIDs" runat="server" />
                                                                                            <asp:HiddenField ID="hiddenSelectedName" runat="server" />
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </cc1:ucPanel>
                                                                        </td>
                                                                    </tr>--%>
                                                                </table>
                                                            </ItemTemplate>
                                                        </asp:Repeater>                                                        
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div class="button-row">
                                                            <cc1:ucButton ID="btnBack_4" runat="server" CssClass="button"
                                                                OnClick="btnBack_4_Click" ValidationGroup="Saves" />
                                                            <cc1:ucButton ID="btnProceed_1" runat="server" CssClass="button"
                                                                OnClick="btnProceed_1_Click" ValidationGroup="Save" />
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>

                            </cc1:ucPanel>
                             </ContentTemplate>
                            <Triggers> 
                                <asp:PostBackTrigger ControlID="btnProceed_1"  /> 
                                  <asp:PostBackTrigger ControlID="btnBack_4"  /> 
                            </Triggers>
                            </asp:UpdatePanel>

                            <cc1:ucPanel ID="pnlCountrySettings" runat="server" CssClass="fieldset-form" Visible="false">
                                <table width="100%" cellspacing="5" cellpadding="0" class="form-table">
                                    <tr>
                                        <td style="height:100%; width:100%;" valign="top">
                                            <table>
                                                <tr>
                                                    <td align="left" colspan="2">
                                                        <cc1:ucLabel ID="lblCountrySettingsText" runat="server"></cc1:ucLabel>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                    <table>
                                                        <asp:Repeater ID="rptCountrySettings" runat="server" 
                                                            onitemdatabound="rptCountrySettings_ItemDataBound">
                                                            <HeaderTemplate>                                                                
                                                                    <tr>
                                                                        <th>
                                                                            <cc1:ucLabel ID="lblCountry" runat="server"></cc1:ucLabel>                    
                                                                        </th>
                                                                        <th>
                                                                            <cc1:ucLabel ID="lblCarrier" runat="server"></cc1:ucLabel>                    
                                                                        </th>
                                                                    </tr>
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                    <tr>
                                                                        <td>
                                                                            <cc1:ucCheckbox ID="chkRptCountry" Text='<%# Bind("CountryName") %>' runat="server" />
                                                                            <asp:HiddenField ID="hiddenRptCountryId" Value='<%# Bind("CountryID") %>' runat="server" />
                                                                        </td>
                                                                        <td>
                                                                            <cc1:ucDropdownList ID="ddlRptCarrier" runat="server"></cc1:ucDropdownList>
                                                                        </td>
                                                                    </tr>                                                                
                                                            </ItemTemplate>
                                                        </asp:Repeater>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2" align="right">
                                                        <div class="button-row">
                                                            <cc1:ucButton ID="btnSave_1" runat="server" CssClass="button"
                                                                OnClick="btnSave_1_Click" ValidationGroup="Save" />
                                                            <cc1:ucButton ID="btnBack_3" runat="server" CssClass="button"
                                                                OnClick="btnBack_3_Click" ValidationGroup="Saves" />
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </cc1:ucPanel>
                        </cc1:ucView>
                        <cc1:ucView ID="vwModules" runat="server">
                            <cc1:ucPanel ID="pnlModules" runat="server" GroupingText="Modules" CssClass="fieldset-form" 
                                Style="display: block">
                                <table width="100%" cellspacing="5" cellpadding="0" class="form-table">
                                    <tr>
                                        <td>
                                            <cc1:ucLabel ID="lblSelectOptionsNew" runat="server"></cc1:ucLabel>                                            
                                        </td>
                                    </tr>
                                    <%-- Appointment Scheduling --%>
                                    <tr>
                                        <td>
                                            <table width="100%" cellspacing="5" cellpadding="0" class="form-table">
                                                <tr>
                                                    <td>
                                                        <div class="button-row" style="text-align:left">
                                                            <cc1:ucLabel ID="lblAppointmentScheduling" CssClass="button" runat="server"></cc1:ucLabel>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <cc1:ucLabel ID="lblSelectAppointmentSchedulingSitesNew" runat="server"></cc1:ucLabel>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="checkboxlist">
                                                        <asp:DataList ID="dlAppointmentScheduling" runat="server" RepeatColumns="5" RepeatDirection="Horizontal" 
                                                            OnItemDataBound="dlAppointmentScheduling_ItemDataBound"
                                                            ItemStyle-VerticalAlign="Top" AlternatingItemStyle-VerticalAlign="Top">
                                                            <ItemTemplate>                                                                
                                                                <table width="100%" cellspacing="5" cellpadding="0" class="form-table">
                                                                    <tr>
                                                                        <td>
                                                                            <cc1:ucLabel ID="lblCountryAS" runat="server" Font-Bold="true"></cc1:ucLabel>
                                                                            <asp:HiddenField ID="hdnCountryIdAS" runat="server" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <cc1:ucCheckboxList ID="cblAppointmentScheduling" runat="server" RepeatColumns="1"
                                                                                RepeatDirection="Horizontal">
                                                                            </cc1:ucCheckboxList>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </ItemTemplate>                                                            
                                                        </asp:DataList>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    
                                    <%-- Discrepancies --%>
                                    <tr>
                                        <td>
                                            <table width="100%" cellspacing="5" cellpadding="0" class="form-table">
                                                <tr>
                                                    <td>
                                                        <div class="button-row" style="text-align:left">
                                                            <cc1:ucLabel ID="lblDiscrepancies_1" CssClass="button" runat="server"></cc1:ucLabel>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <cc1:ucLabel ID="lblSelectDiscrepanciesSitesNew" runat="server"></cc1:ucLabel>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="checkboxlist">
                                                        <asp:DataList ID="dlDiscrepancies" runat="server"  RepeatColumns="5" RepeatDirection="Horizontal"
                                                            onitemdatabound="dlDiscrepancies_ItemDataBound" 
                                                            ItemStyle-VerticalAlign="Top" AlternatingItemStyle-VerticalAlign="Top">
                                                            <ItemTemplate>
                                                                <table width="100%" cellspacing="5" cellpadding="0" class="form-table">
                                                                    <tr>
                                                                        <td valign="top">
                                                                            <cc1:ucLabel ID="lblCountryD" runat="server" Font-Bold="true"></cc1:ucLabel>
                                                                            <asp:HiddenField ID="hdnCountryIdD" runat="server" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td valign="top">
                                                                            <cc1:ucCheckboxList ID="cblDiscrepancies" runat="server">
                                                                            </cc1:ucCheckboxList>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </ItemTemplate>
                                                        </asp:DataList>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>


                                    <%-- Invoice Issues phase 21 R2 invode discrepancy --%>
                                    <tr>
                                        <td>
                                            <table width="100%" cellspacing="5" cellpadding="0" class="form-table">
                                                <tr>
                                                    <td>
                                                        <div class="button-row" style="text-align:left">
                                                            <cc1:ucLabel ID="lblInvoiceIssue_1" CssClass="button" runat="server"></cc1:ucLabel>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <cc1:ucLabel ID="lblSelectInvoiceIssueSitesNew" runat="server"></cc1:ucLabel>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="checkboxlist">
                                                        <asp:DataList ID="dlInvoiceIssue" runat="server"  RepeatColumns="5" RepeatDirection="Horizontal"
                                                            onitemdatabound="dlInvoiceIssue_ItemDataBound" 
                                                            ItemStyle-VerticalAlign="Top" AlternatingItemStyle-VerticalAlign="Top">
                                                            <ItemTemplate>
                                                                <table width="100%" cellspacing="5" cellpadding="0" class="form-table">
                                                                    <tr>
                                                                        <td valign="top">
                                                                            <cc1:ucLabel ID="lblCountryD" runat="server" Font-Bold="true"></cc1:ucLabel>
                                                                            <asp:HiddenField ID="hdnCountryIdD" runat="server" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td valign="top">
                                                                            <cc1:ucCheckboxList ID="cblInvoiceIssue" runat="server">
                                                                            </cc1:ucCheckboxList>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </ItemTemplate>
                                                        </asp:DataList>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>

                                    <%-- OTIF --%>
                                    <tr>
                                        <td>
                                            <table width="100%" cellspacing="5" cellpadding="0" class="form-table">
                                                <tr>
                                                    <td>
                                                        <div class="button-row" style="text-align:left">
                                                            <cc1:ucLabel ID="lblOTIF" CssClass="button" runat="server"></cc1:ucLabel>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <cc1:ucLabel ID="lblSelectOTIFSitesNew" runat="server"></cc1:ucLabel>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="checkboxlist">
                                                        <asp:DataList ID="dlOTIF" runat="server" RepeatColumns="5" RepeatDirection="Horizontal"
                                                            onitemdatabound="dlOTIF_ItemDataBound">
                                                            <ItemTemplate>
                                                                <table width="100%" cellspacing="5" cellpadding="0" class="form-table">
                                                                    <tr>
                                                                        <td>
                                                                            <cc1:ucCheckboxList ID="cblOTIF" runat="server"  Font-Bold="true"
                                                                                RepeatColumns="5" RepeatDirection="Horizontal">
                                                                            </cc1:ucCheckboxList>
                                                                            <%--<cc1:ucCheckbox ID="cblOTIF" runat="server" />--%>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </ItemTemplate>
                                                        </asp:DataList>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>

                                    <%-- Scorecard --%>
                                    
                                    <tr id="Scorecard" runat="server" >
                                        <td>
                                            <table width="100%" cellspacing="5" cellpadding="0"  class="form-table">
                                                <tr>
                                                    <td>
                                                        <div class="button-row" style="text-align:left">
                                                            <cc1:ucLabel ID="lblScorecard" CssClass="button" runat="server"></cc1:ucLabel>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <cc1:ucLabel ID="lblSelectScorecardSitesNew" runat="server"></cc1:ucLabel>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="checkboxlist">
                                                        <asp:DataList ID="dlScorecard" runat="server" RepeatColumns="5" RepeatDirection="Horizontal" 
                                                            onitemdatabound="dlScorecard_ItemDataBound">
                                                            <ItemTemplate>
                                                                <table width="100%" cellspacing="5" cellpadding="0" class="form-table">                                                                    
                                                                    <tr>
                                                                        <td>
                                                                            <cc1:ucCheckboxList ID="cblScorecard" runat="server" Font-Bold="true"
                                                                                RepeatColumns="5" RepeatDirection="Horizontal" CellSpacing="5" CellPadding="5">
                                                                            </cc1:ucCheckboxList>
                                                                            <%--<cc1:ucCheckbox ID="cblScorecard" runat="server" />--%>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </ItemTemplate>
                                                        </asp:DataList>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    

                                    <%-- Back & Complete Registration Button Section --%>
                                    <tr>
                                        <td>
                                            <div class="button-row">
                                                <cc1:ucButton ID="btnBack_2" runat="server" CssClass="button" 
                                                    OnClick="btnBack_2_Click" ValidationGroup="CompleteRegistration" />
                                                <cc1:ucButton ID="btnCompleteRegistration" runat="server" CssClass="button" 
                                                    OnClick="btnCompleteRegistration_Click" ValidationGroup="CompleteRegistration" />
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </cc1:ucPanel>
                        </cc1:ucView>
                    </cc1:ucMultiView>
                   
                </div>
            </div>

            


            <div class="bottom-shadow">
            </div>
            <div class="balloonstyle" id="ssn123">
                <span class="bla8blue">If you do not know your office depot vendor number,<br />
                    click on the icon to search for your vendor name</span>
                <%--<cc1:ucLabel runat="server" ID="lblVendorTooltip" CssClass="bla8blue" Text="If you do not know your office depot vendor number,<br/> click on the icon to search for your vendor name" />
                --%>
            </div>
            <div class="button-row">
                <cc1:ucButton ID="btnSave1" runat="server" Text="Save" CssClass="button" OnClick="btnSave_Click"
                    Visible="false" ValidationGroup="Save" />
                <cc1:ucButton ID="btnBlockUser1" runat="server" Text="Block User" CssClass="button"
                    ValidationGroup="Save" Visible="false" OnClick="btnBlockUser_Click" />
                <cc1:ucButton ID="btnUnblockUser1" runat="server" Text="Unblock User" CssClass="button"
                    ValidationGroup="Save" Visible="false" OnClick="btnUnblockUser_Click" />
                <cc1:ucButton ID="btnDeleteUser1" runat="server" Text="Delete User" CssClass="button"
                    ValidationGroup="Save" Visible="false" OnClick="btnDeleteUser_Click" />
                <cc1:ucButton ID="btnBack1" runat="server" Text="Back" CssClass="button" OnClick="btnBack_Click"
                    Visible="false" />
                <asp:ValidationSummary ID="VSSave1" runat="server" ValidationGroup="Save" ShowSummary="false"
                    ShowMessageBox="true" />
            </div>
        <%--</ContentTemplate>
    </asp:UpdatePanel>--%>

    <script type='text/javascript' language="javascript">
        var pnlCountry = document.getElementById("<%=pnlCountrySelectionCountrySpecSett.ClientID%>");
        if (pnlCountry) {
            pnlCountry.style.display = "";
        }       
       
    </script>
    <script type="text/javascript">
        var inputVendorNo;
        var inputVendorNoSelectedName;
        var inputCountryID;
        function UpdatevendortNumberdetails(VendorNoValue, VendorNoSelectednameValue) {
            //  $('#' + inputVendorNo).trigger("change");
            $('#' + inputVendorNo).attr('value', VendorNoSelectednameValue.split('-')[0]);
            $('#' + inputVendorNoSelectedName).html(VendorNoSelectednameValue);
            return false;
        }
        function GetSelectedRow(lnk) {
            //debugger;
            var row = lnk.parentNode.parentNode;
            inputVendorNo = $(row.cells[1].innerHTML).attr('id');
            inputVendorNoSelectedName = $(row.cells[3].innerHTML).attr('id');
            inputCountryID = $(row.cells[2].innerHTML).attr('id');
            document.getElementById('<%= hdntxtVendorNo.ClientID%>').value = $(row.cells[1].innerHTML).attr('value');
            document.getElementById('<%= hdnCountryID.ClientID%>').value = $(row.cells[2].innerHTML).attr('value');
            var getIndexByID = inputVendorNo;
            document.getElementById('<%= hdnrowNumber.ClientID%>').value = parseInt(getIndexByID.replace('ctl00_ContentPlaceHolder1_rptVendorDetailsREU_ctl', '').split('_')[0]);
            //            alert($(row.cells[1].innerHTML).attr('value'));
            //            alert(document.getElementById('<%= hdntxtVendorNo.ClientID%>').value);
            //            alert(document.getElementById('<%= hdnCountryID.ClientID%>').value);
            //            alert(hdntxtVendorNo);
            return false;
        }

        function showVendorimg(input) {
            $find('VendorViewer').show();
            GetSelectedRow(input);
            return false;
        }

        //        function GettxtChnageCountryId(lnk) {
        //            debugger;
        //            var row = lnk.parentNode.parentNode;
        //            inputCountryID = $(row.cells[1].innerHTML).attr('id');
        //            document.getElementById('<%= hdnCountryID.ClientID%>').value = $(row.cells[1].innerHTML).attr('value');
        //        }

    </script>
</asp:Content>

    

﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SCT_RegisterExternalUserEdit.aspx.cs"
    MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master" Inherits="SCT_RegisterExternalUserEdit"
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="../../CommonUI/UserControls/ucCountry.ascx" TagName="ucCountry"
    TagPrefix="uc1" %>
<%@ Register Src="../../CommonUI/UserControls/ucSeacrhVendor.ascx" TagName="ucSeacrhVendor"
    TagPrefix="cc3" %>
    <%@ Register Src="~/CommonUI/UserControls/ucSeacrhVendorWithoutSM.ascx" TagName="ucSeacrhVendorWithoutSM"
    TagPrefix="cc4" %>

    <%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript" language="javascript">

        var varAlredySelectedVendor = '<%= this.AlredySelectedVendor %>';
        function checkVendorSelected(source, args) {

        }

        function ConfirmDeletion() {
            return confirm('<%=deleteMessage%>');
        }

        function ConfirmRejectedUserDeletion() {
            return confirm('<%=RejectedUserdeleteMessage%>');
        }

        function showVendor(cControl) {
            var varControlId = cControl.id;
            var varControlIndex = varControlId.lastIndexOf('_');
            var varControlPrefix = varControlId.substring(0, varControlIndex) + '_';

            var varMdlVendorViewer = varControlPrefix + 'mdlVendorViewer'
            var varIsSearch = varControlPrefix + 'isSearch'

            $find(varMdlVendorViewer).show();
            document.getElementById(varIsSearch).value = "true";
            return false;
        }

        function ShowAlert(cntl_id) {
            var cntl = document.getElementById(cntl_id);
            var status;
            if (cntl.checked == false) {
                if (confirm('Are you sure you want to remove the country - All Country maintained details will be lost')) {
                    status = true;
                }
                else {
                    cntl.checked = true;
                    status = false;
                }
            }
            else {
                status = true;
            }
            __doPostBack("'" + cntl_id + "'", '');
            return status;
        }

        $(document).ready(function () {
            var $this = $('#rblApprovalStatus');
            $this.parents('tr').next().hide();
            $this.parents('tr').next().next().hide();
            var rfvCommentRequired = document.getElementById('<%=rfvRejectionCommentRequired.ClientID %>');
            ValidatorEnable(rfvCommentRequired, false);

        });

        function checkedChanged(rblApproveStatus) {
            $this = $(rblApproveStatus);
            var status = $(':checked', $this).val();
            var rfvCommentRequired = document.getElementById('<%=rfvRejectionCommentRequired.ClientID %>');
            if (status == 'approve') {

                $this.parents('tr').next().hide();
                $this.parents('tr').next().next().hide();

                ValidatorEnable(rfvCommentRequired, false);
            }
            else {
                $this.parents('tr').next().show();
                $this.parents('tr').next().next().show();

                ValidatorEnable(rfvCommentRequired, true);
            }
        }

        function Save_ClientClick() {
            var status = $('#rblApprovalStatus input:radio:checked').val();
            if (status == 'approve') {
                var rfvCommentRequired = document.getElementById('<%=rfvRejectionCommentRequired.ClientID %>');

                ValidatorEnable(rfvCommentRequired, false);
            }
        }
       

       
    </script>
    <asp:ScriptManager ID="ScriptManager1" runat="server" ScriptMode="Release">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="up1" runat="server">
        <ContentTemplate>
            <h2>
                <%--<cc1:ucLabel ID="lblRegisterAsNewUserEdit" runat="server" Text="Edit User Profile"></cc1:ucLabel>--%>
                <cc1:ucLabel ID="lblRegisterAsNewUserEditNew" runat="server"></cc1:ucLabel>
            </h2>
            <div class="right-shadow">
                <div class="formbox">
                    <cc1:ucPanel ID="pnlPersonalDetails" runat="server" GroupingText="Personal Details"
                        CssClass="fieldset-form">
                        <table width="100%" cellspacing="5" cellpadding="0" class="form-table">
                            <tr>
                                <td style="font-weight: bold;" width="10%">
                                    <cc1:ucLabel ID="lblUserIdEmail" runat="server" Text="User ID/Email" isRequired="true"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold;" width="1%" align="center">
                                    :
                                </td>
                                <td style="font-weight: bold;" width="39%">
                                    <cc1:ucTextbox ID="txtUserIDEmail" runat="server" Width="220px" MaxLength="50"></cc1:ucTextbox>
                                    <asp:RequiredFieldValidator ID="rfvUsernameReq" runat="server" ControlToValidate="txtUserIDEmail"
                                        ValidationGroup="Save" ErrorMessage="Please enter User ID/Email" Display="None"></asp:RequiredFieldValidator>
                                </td>
                                <td style="font-weight: bold;" width="7%">
                                </td>
                                <td style="font-weight: bold;" width="1%">
                                </td>
                                <td style="font-weight: bold;" width="42%">
                                </td>
                            </tr>
                            <tr>
                                <td style="font-weight: bold">
                                    <cc1:ucLabel ID="lblPassword" runat="server" Text="Password" isRequired="true"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold;" align="center" runat="server" id="tdpassword">
                                    :
                                </td>
                                <td colspan="4">
                                    <cc1:ucTextbox ID="txtPassword" runat="server" Width="130px" MaxLength="50" TextMode="Password"></cc1:ucTextbox>
                                    <asp:RequiredFieldValidator ID="rfvPasswordReq" runat="server" ControlToValidate="txtPassword"
                                        ValidationGroup="Save" ErrorMessage="Please enter Password" Display="None"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td style="font-weight: bold">
                                    <cc1:ucLabel ID="lblFirstName" runat="server" Text="First Name" isRequired="true"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold;" align="center">
                                    :
                                </td>
                                <td>
                                    <cc1:ucTextbox ID="txtFirstName" runat="server" Width="130px" MaxLength="50"></cc1:ucTextbox>
                                    <asp:RequiredFieldValidator ID="rfvFirstNameReq" runat="server" ControlToValidate="txtFirstName"
                                        ValidationGroup="Save" ErrorMessage="Please enter First Name" Display="None"></asp:RequiredFieldValidator>
                                </td>
                                <td style="font-weight: bold">
                                    <cc1:ucLabel ID="lblLastName" runat="server" Text="Last Name" isRequired="true"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold;" align="center">
                                    :
                                </td>
                                <td>
                                    <cc1:ucTextbox ID="txtLastName" runat="server" Width="130px" MaxLength="50"></cc1:ucTextbox>
                                    <asp:RequiredFieldValidator ID="rfvLastNameReq" runat="server" ControlToValidate="txtLastName"
                                        ValidationGroup="Save" ErrorMessage="Please enter Last Name" Display="None"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td style="font-weight: bold">
                                    <cc1:ucLabel ID="lblTelephone" runat="server" Text="Telephone" isRequired="true"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold;" align="center">
                                    :
                                </td>
                                <td>
                                    <cc1:ucTextbox ID="txtTelephone" runat="server" Width="220px" MaxLength="50"></cc1:ucTextbox>
                                    <asp:RequiredFieldValidator ID="rfvTelephoneReq" runat="server" ControlToValidate="txtTelephone"
                                                ValidationGroup="Save" ErrorMessage="Please enter Telephone No" Display="None"></asp:RequiredFieldValidator>
                                </td>
                                <td style="font-weight: bold">
                                    <cc1:ucLabel ID="lblFax" runat="server" Text="Fax"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold;" align="center">
                                    :
                                </td>
                                <td>
                                    <cc1:ucTextbox ID="txtFax" runat="server" Width="220px" MaxLength="50"></cc1:ucTextbox>
                                </td>
                            </tr>
                            <tr>
                                <td style="font-weight: bold">
                                    <cc1:ucLabel ID="lblLanguage" runat="server" Text="Language" isRequired="true"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold;" align="center">
                                    :
                                </td>
                                <td>
                                    <cc1:ucDropdownList ID="ddlLanguage" runat="server" Width="150px">
                                    </cc1:ucDropdownList>
                                    <asp:RequiredFieldValidator ID="rfvLanguageReq" runat="server" ControlToValidate="ddlLanguage"
                                        ValidationGroup="Save" ErrorMessage="Please select Language" Display="None" InitialValue="0"></asp:RequiredFieldValidator>
                                </td>
                                <td colspan="3">&nbsp;
                                   <%-- <cc1:ucRadioButtonList ID="rblVendorUpdate" runat="server" Visible="false">
                                        <asp:ListItem Text="Edit Existing" Value="EditExisting"></asp:ListItem>
                                        <asp:ListItem Text="Adding New Country" Value="AddingNewCountry"></asp:ListItem>
                                    </cc1:ucRadioButtonList>--%>
                                </td>
                            </tr>
                        </table>
                        <div style="text-align:right"><cc1:ucButton ID="btnSaveChanges" 
                                ValidationGroup="Save" runat="server" CssClass="button" 
                                onclick="btnSaveChanges_Click" /></div>
                    </cc1:ucPanel>
                    <br />
               
                            
                    <asp:Panel ID="pnlVendorSction" runat="server" Visible="false">
                        <cc1:ucPanel ID="pnlCountrySpecific" runat="server" GroupingText="Country Specific Settings"
                            CssClass="fieldset-form" Style="display: block">
                            <table width="100%" cellspacing="5" cellpadding="0" class="form-table">
                                <tr>
                                    <td colspan="10">
                                        <cc1:ucLabel ID="lblCountrySpecificLabel" runat="server"></cc1:ucLabel>
                                    </td>
                                </tr>
                                <asp:Repeater ID="rptCountry" runat="server" OnItemDataBound="rptCountry_ItemDataBound"
                                    OnItemCreated="rptCountry_ItemCreated">
                                    <ItemTemplate>
                                        <%# (Container.ItemIndex + 5) % 5 == 0 ? "<tr>" : string.Empty %>
                                        <td>
                                            <asp:CheckBox ID="chkCountry" runat="server" />
                                        </td>
                                        <td width="19%">
                                            <asp:Label ID="lblCountryName" runat="server" Text='<%# Eval("CountryName") %>'></asp:Label>
                                            <asp:HiddenField ID="hdnCountryID" runat="server" Value='<%# Eval("CountryID") %>' />
                                        </td>
                                        <%# (Container.ItemIndex + 5) % 5 == 4 ? "</tr>" : string.Empty %>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </table>
                            <div style="text-align: right">
                                <cc1:ucButton ID="btnUpdate" runat="server" CssClass="button" OnClick="btnUpdate_Click"
                                    Style="display: none;" /></div>
                        </cc1:ucPanel>

                            

                              <%---Report Viewer popup start--%>
                            <asp:Button ID="btnViewVendor" runat="Server" Style="display: none" />
                            <ajaxtoolkit:modalpopupextender ID="mdlVendorViewer" runat="server" TargetControlID="btnViewVendor"
                                PopupControlID="pnlbtnViewVendorViewer" 
                                 BackgroundCssClass="modalBackground" BehaviorID="VendorViewer" 
                                DropShadow="false" />


                             <asp:Panel ID="pnlbtnViewVendorViewer" runat="server"  Style="display:none; width: 500px;">
                                 <!--Romoved VendorViewer ancor tag from here-->
                                <div style="width: 100%; overflow-y: hidden; overflow-x: hidden; background-color: #fff;
                                    padding: 5px; border: 2px solid #ccc; text-align: left;">
                                    <table width="100%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                                        <tr>
                                            <td style="font-weight: bold;" width="25%">
                                                <cc1:ucLabel ID="lblVendorName" runat="server" Text="Vendor Name"></cc1:ucLabel>
                                            </td>
                                            <td style="font-weight: bold;" width="5%">
                                                :
                                            </td>
                                            <td width="50%">
                                                <cc1:ucTextbox ID="txtVenderName2" runat="server" Width="147px"></cc1:ucTextbox>
                                            </td>
                                            <td width="20%">
                                                <cc1:ucButton OnClick="btnSearch_Click" ID="btnSearch" runat="server" 
                                                    Text="Search" CssClass="button" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                &nbsp;
                                            </td>
                                            <td style="font-weight: bold;" align="left" colspan="2">                           
                                                <cc1:ucListBox ID="lstVendor" runat="server" Width="99%" Height="200px" ondblclick="lstVendor_DoubleClick" ></cc1:ucListBox>
                                                </td>                      
                                            </tr>
                                            <tr>
                                                    <td colspan="4" align="center">
                                                <cc1:ucButton OnClick="btnSelect_Click" ID="btnSelect" runat="server"  
                                                    Text="Select" CssClass="button" Width="60px" />
                                                &nbsp;
                                                <cc1:ucButton OnClick="btnPopupCancel_Click" ID="btnPopupCancel" runat="server" 
                                                    Text="Cancel" CssClass="button" Width="60px" />
                                                </td>
                                            </tr>
                                                <tr>
                                                    <td colspan="4">&nbsp;</td>
                                                </tr>
                                            </table>
                                            </div>
                                        </asp:Panel>

                            <cc1:ucPanel ID="pnlVendorDetail" runat="server" CssClass="fieldset-form">
                             <asp:HiddenField ID="hdntxtVendorNo" runat="server" />
                                    <asp:HiddenField ID="hdnCountryID" runat="server" />
                                    <asp:HiddenField ID="hdnVendorName" runat="server" />
                                    <asp:HiddenField ID="hdnrowNumber" runat="server" />
                                
                           
                            <table width="100%" cellspacing="5" cellpadding="0" class="form-table" id="tblVendorDetailsRegisterExternalUsers">
                                <tr id="tr2" runat="server">
                                    <td colspan="4">
                                        <table width="100%">
                                            <tr>
                                                <td style="font-weight: bold; width: 95%">
                                                    <cc1:ucLiteral ID="ltVendorDetailsTextRegisterExternalUsers" runat="server"></cc1:ucLiteral>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="font-weight: bold; width: 100%">
                                                    <asp:Repeater ID="rptVendorDetailsREU" runat="server" OnItemDataBound="rptVendorDetailsREU_ItemDataBound"
                                                        OnItemCommand="rptVendorDetailsREU_ItemCommand">
                                                        <ItemTemplate>
                                                            <asp:Panel ID="pnlVendorDetails" runat="server">
                                                                <table width="100%" cellspacing="5" cellpadding="0" class="form-table">
                                                                    <tr>
                                                                     <td style="width:8%">
                                                                     <cc1:ucLabel ID="lblCountry" runat="server" style="color:Maroon;" Text="Country"></cc1:ucLabel>
                                                                       <asp:HiddenField ID="hdnVendorID" runat="server" />
                                                                            <asp:HiddenField  ID="hdnParentVendorID" runat="server" />
                                                                      </td>
                                                                      <td  style="width: 3%;" align="left" valign="middle">
                                                                            <cc1:ucLabel ID="lblCountryVendorDetailsREU" runat="server"></cc1:ucLabel>
                                                                       </td>
                                                                         <td style="width:2%;"> </td> 
                                                                         <td style="width:87%;"></td>    
                                                                    </tr>
                                                                    <tr>
                                                                    <td style="width:8%">
                                                                     <cc1:ucLabel ID="lblVendorNumber" runat="server" style="color:Maroon;" Text="Vendor #"></cc1:ucLabel>
                                                                    </td>
                                                                        <td style="font-weight: bold;" width="3%" valign="middle">
                                                                           <cc1:ucTextbox ID="txtVendorNo" runat="server" Width="33px" 
                                                                                MaxLength="6" AutoPostBack="true" 
                                                                                ontextchanged="txtVendorNo_TextChanged" ></cc1:ucTextbox>
                                                                        </td>
                                                                            <td style="font-weight: bold;" width="2%;" valign="middle">       
                                                                              <asp:HiddenField ID="hdnCountryIdVendorDetailsREU" runat="server" />          
                                                                            <asp:ImageButton ID="imgVendor" runat="server" ImageUrl="~/Images/Search.gif" 
                                                                            style="height:16px; width:16px;" OnClientClick="return showVendorimg(this);" />
                                                                        </td>
                                                                            <td style="font-weight: bold;" width="87%;">
                                                                            <cc1:ucLabel ID="SelectedVendorName"  BorderWidth="0" runat="server" Width="250px" Text='<%# Eval("VendorName") %>'></cc1:ucLabel>                                        
                                                                        </td>
                                                                    </tr>
                                                            </asp:Panel>
                                                            </table>
                                                        </ItemTemplate>
                                                    </asp:Repeater>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </cc1:ucPanel>
                     

                        <cc1:ucPanel ID="pnlModules" runat="server" CssClass="fieldset-form">
                            <table width="100%" cellspacing="5" cellpadding="0" class="form-table">
                                <tr>
                                    <td colspan="10">
                                        <cc1:ucLabel ID="lblSelectOptions" runat="server"></cc1:ucLabel>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="10">
                                        <cc1:ucLabel ID="lblAppointmentScheduling" runat="server" CssClass="button"></cc1:ucLabel>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="10">
                                        <cc1:ucLabel ID="lblSelectAppointmentSchedulingSites" runat="server"></cc1:ucLabel>
                                    </td>
                                </tr>
                                <asp:Repeater ID="rptAppointmentCountry" runat="server" OnItemDataBound="rptAppointmentCountry_ItemDataBound">
                                    <ItemTemplate>
                                        <%# (Container.ItemIndex + 5) % 5 == 0 ? "<tr>" : string.Empty %>
                                        <td valign="top">
                                            <b>
                                                <%# Eval("Country")%></b>
                                            <asp:HiddenField ID="hdnCountryID" runat="server" Value='<%# Eval("CountryID") %>' />
                                            <br />
                                            <table width="100%" cellspacing="5" cellpadding="0" class="form-table">
                                                <asp:Repeater ID="rptAppointmentCountrySites" runat="server" OnItemDataBound="rptAppointmentCountrySites_ItemDataBound">
                                                    <ItemTemplate>
                                                        <tr>
                                                            <td width="2%">
                                                                <asp:CheckBox ID="chkSite" runat="server" />
                                                            </td>
                                                            <td>
                                                                <%# Eval("SiteName") %>
                                                                <asp:HiddenField ID="hdnSiteID" runat="server" Value='<%# Eval("SiteID") %>' />
                                                            </td>
                                                        </tr>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </table>
                                        </td>
                                        <%# (Container.ItemIndex + 5) % 5 == 4 ? "</tr>" : string.Empty %>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </table>

                            <table width="100%" cellspacing="5" cellpadding="0" class="form-table">
                                <tr>
                                    <td colspan="10">
                                        <cc1:ucLabel ID="lblDiscrepancies" runat="server" CssClass="button"></cc1:ucLabel>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="10">
                                        <cc1:ucLabel ID="lblSelectDiscrepanciesSites" runat="server"></cc1:ucLabel>
                                    </td>
                                </tr>
                                <asp:Repeater ID="rptDiscrepanciesCountry" runat="server" OnItemDataBound="rptDiscrepanciesCountry_ItemDataBound">
                                    <ItemTemplate>
                                        <%# (Container.ItemIndex + 5) % 5 == 0 ? "<tr>" : string.Empty %>
                                        <td valign="top">
                                            <b>
                                                <%# Eval("Country")%></b>
                                            <asp:HiddenField ID="hdnCountryID" runat="server" Value='<%# Eval("CountryID") %>' />
                                            <br />
                                            <table width="100%" cellspacing="5" cellpadding="0" class="form-table">
                                                <asp:Repeater ID="rptDiscrepanciesCountrySites" runat="server" OnItemDataBound="rptDiscrepanciesCountrySites_ItemDataBound">
                                                    <ItemTemplate>
                                                        <tr>
                                                            <td width="2%">
                                                                <asp:CheckBox ID="chkSite" runat="server" />
                                                            </td>
                                                            <td>
                                                                <%# Eval("SiteName") %>
                                                                <asp:HiddenField ID="hdnSiteID" runat="server" Value='<%# Eval("SiteID") %>' />
                                                            </td>
                                                        </tr>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </table>
                                        </td>
                                        <%# (Container.ItemIndex + 5) % 5 == 4 ? "</tr>" : string.Empty %>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </table>


                            <table width="100%" cellspacing="5" cellpadding="0" class="form-table">
                                <tr>
                                    <td colspan="10">
                                        <cc1:ucLabel ID="lblInvoiceIssue_1" runat="server" CssClass="button"></cc1:ucLabel>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="10">
                                        <cc1:ucLabel ID="lblSelectInvoiceIssueSites" runat="server"></cc1:ucLabel>
                                    </td>
                                </tr>
                                <asp:Repeater ID="rptInvoiceIssueCountry" runat="server" OnItemDataBound="rptInvoiceIssueCountry_ItemDataBound">
                                    <ItemTemplate>
                                        <%# (Container.ItemIndex + 5) % 5 == 0 ? "<tr>" : string.Empty %>
                                        <td valign="top">
                                            <b>
                                                <%# Eval("Country")%></b>
                                            <asp:HiddenField ID="hdnCountryID" runat="server" Value='<%# Eval("CountryID") %>' />
                                            <br />
                                            <table width="100%" cellspacing="5" cellpadding="0" class="form-table">
                                                <asp:Repeater ID="rptInvoiceIssueCountrySites" runat="server" OnItemDataBound="rptInvoiceIssueCountrySites_ItemDataBound">
                                                    <ItemTemplate>
                                                        <tr>
                                                            <td width="2%">
                                                                <asp:CheckBox ID="chkSite" runat="server" />
                                                            </td>
                                                            <td>
                                                                <%# Eval("SiteName") %>
                                                                <asp:HiddenField ID="hdnSiteID" runat="server" Value='<%# Eval("SiteID") %>' />
                                                            </td>
                                                        </tr>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </table>
                                        </td>
                                        <%# (Container.ItemIndex + 5) % 5 == 4 ? "</tr>" : string.Empty %>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </table>

                            <table width="100%" cellspacing="5" cellpadding="0" class="form-table">
                                <tr>
                                    <td colspan="10">
                                        <cc1:ucLabel ID="lblOTIF" runat="server" CssClass="button"></cc1:ucLabel>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="10">
                                        <cc1:ucLabel ID="lblSelectOTIFSites" runat="server"></cc1:ucLabel>
                                    </td>
                                </tr>
                                <asp:Repeater ID="rptOtifCountry" runat="server" OnItemDataBound="rptOtifCountry_ItemDataBound">
                                    <ItemTemplate>
                                        <%# (Container.ItemIndex + 5) % 5 == 0 ? "<tr>" : string.Empty %>
                                        <td width="2%">
                                            <asp:CheckBox ID="chkCountry" runat="server" />
                                        </td>
                                        <td>
                                            <b>
                                                <%# Eval("Country")%></b>
                                            <asp:HiddenField ID="hdnCountryID" runat="server" Value='<%# Eval("CountryID") %>' />
                                        </td>
                                        <%# (Container.ItemIndex + 5) % 5 == 4 ? "</tr>" : string.Empty %>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </table>
                            <div id="ScoreCard" runat="server">
                            <table width="100%" cellspacing="5" cellpadding="0" class="form-table">
                                <tr>
                                    <td colspan="10">
                                        <cc1:ucLabel ID="lblScorecard" runat="server" CssClass="button"></cc1:ucLabel>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="10">
                                        <cc1:ucLabel ID="lblSelectScorecardSites" runat="server"></cc1:ucLabel>
                                    </td>
                                </tr>
                                <asp:Repeater ID="rptScorecardCountry" runat="server" OnItemDataBound="rptScorecardCountry_ItemDataBound">
                                    <ItemTemplate>
                                        <%# (Container.ItemIndex + 5) % 5 == 0 ? "<tr>" : string.Empty %>
                                        <td width="2%">
                                            <asp:CheckBox ID="chkCountry" runat="server" />
                                        </td>
                                        <td>
                                            <b>
                                                <%# Eval("Country")%></b>
                                            <asp:HiddenField ID="hdnCountryID" runat="server" Value='<%# Eval("CountryID") %>' />
                                        </td>
                                        <%# (Container.ItemIndex + 5) % 5 == 4 ? "</tr>" : string.Empty %>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </table>
                            </div>
                        </cc1:ucPanel>
                    </asp:Panel>
                   
                    <asp:Panel ID="pnlCarrierSction" runat="server" Visible="false">
                        <cc1:ucPanel ID="pnlCountrySettings" runat="server" CssClass="fieldset-form" Style="display: block">
                            <table width="100%" cellspacing="5" cellpadding="0" class="form-table" id="tblCarrierCountry">
                                <tr>
                                    <td colspan="3">
                                        <cc1:ucLabel ID="lblCountrySettingsText" runat="server"></cc1:ucLabel>
                                    </td>
                                </tr>
                                <asp:Repeater ID="rptCarrierCountry" runat="server" OnItemDataBound="rptCarrierCountry_ItemDataBound" OnItemCreated="rptCarrierCountry_ItemCreated">
                                    <HeaderTemplate>
                                        <tr>
                                            <th colspan="2" style="text-align:left;">Country</th><th style="text-align:left;">Carrier</th>
                                        </tr>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                    <tr>
                                        <td>
                                            <asp:CheckBox ID="chkCountry" runat="server" CssClass="chkCarrierCountry" />
                                        </td>
                                        <td width="19%">
                                            <asp:Label id="lblCountryName" runat="server" Text= '<%# Eval("CountryName") %>'></asp:Label>
                                            <asp:HiddenField ID="hdnCountryID" runat="server" Value='<%# Eval("CountryID") %>' />
                                        </td>
                                        <td>
                                            <cc1:ucDropdownList ID="ddlCarrier" runat="server"></cc1:ucDropdownList>
                                        </td>
                                    </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </table>
                        </cc1:ucPanel>
                        <cc1:ucPanel ID="pnlSites" runat="server" CssClass="fieldset-form">
                            <table width="100%" cellspacing="5" cellpadding="0" class="form-table">
                                 <tr>
                                    <td colspan="10">
                                        <cc1:ucLabel ID="lblSelectAppointmentSchedulingSites_1" runat="server"></cc1:ucLabel>
                                    </td>
                                </tr>
                                <asp:Repeater ID="rptCarrierSitesCountry" runat="server" OnItemDataBound="rptCarrierSitesCountry_ItemDataBound">
                                    <ItemTemplate>
                                        <%# (Container.ItemIndex + 5) % 5 == 0 ? "<tr>" : string.Empty %>
                                        <td valign="top">
                                            <b>
                                                <%# Eval("Country")%></b>
                                            <asp:HiddenField ID="hdnCountryID" runat="server" Value='<%# Eval("CountryID") %>' />
                                            <br />
                                            <table width="100%" cellspacing="5" cellpadding="0" class="form-table">
                                                <asp:Repeater ID="rptCarrierSites" runat="server" OnItemDataBound="rptCarrierSites_ItemDataBound">
                                                    <ItemTemplate>
                                                        <tr>
                                                            <td width="2%">
                                                                <asp:CheckBox ID="chkSite" runat="server" />
                                                            </td>
                                                            <td>
                                                                <%# Eval("SiteName") %>
                                                                <asp:HiddenField ID="hdnSiteID" runat="server" Value='<%# Eval("SiteID") %>' />
                                                            </td>
                                                        </tr>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </table>
                                        </td>
                                        <%# (Container.ItemIndex + 5) % 5 == 4 ? "</tr>" : string.Empty %>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </table>
                        </cc1:ucPanel>
                    </asp:Panel>
                    <cc1:ucPanel ID="pnlRejectionComment" runat="server" GroupingText="Rejection Commentsfdf"
                        CssClass="fieldset-form" Style="display: none">
                        <table>
                            <tr>
                                <td colspan="2">
                                    <cc1:ucTextbox ID="txtRejectComments" runat="server" onkeyup="checkTextLengthOnKeyUp(this,1000);"
                                        CssClass="textarea" TextMode="MultiLine" Width="920px"></cc1:ucTextbox>
                                </td>
                            </tr>
                        </table>
                    </cc1:ucPanel>
                    <cc1:ucPanel ID="pnlApprove" runat="server" CssClass="fieldset-form" GroupingText=""
                        Style="display: block" Width="100%">
                        <table width="100%" cellspacing="5" cellpadding="0" class="form-table">
                            <tr>
                                <td width="100%">
                                    <table width="100%">
                                        <tr>
                                            <td style="font-weight: bold;" width="14%">
                                                <cc1:ucLabel ID="lblApprovalStatus" runat="server" Text="Approval Status"></cc1:ucLabel>
                                            </td>
                                            <td style="font-weight: bold;" width="1%">
                                                :
                                            </td>
                                            <td style="font-weight: bold;" width="85%">
                                                <cc1:ucRadioButtonList runat="server" ClientIDMode="Static" ID="rblApprovalStatus"
                                                    RepeatDirection="Horizontal" onclick="javascript:checkedChanged(this);" CellPadding="5"
                                                    CellSpacing="10">
                                                    <asp:ListItem Value="approve" Text="Approve" Selected="true" />
                                                    <asp:ListItem Value="reject" Text="Reject" />
                                                </cc1:ucRadioButtonList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: bold;" width="14%">
                                                <cc1:ucLabel ID="lblRejectionComment" runat="server" Text="Rejection Comments"></cc1:ucLabel>
                                            </td>
                                            <td style="font-weight: bold;" width="1%">
                                                :
                                            </td>
                                            <td width="85%">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td colspan="3">
                                                <cc1:ucTextbox ID="txtRejectCommentsR" runat="server" onkeyup="checkTextLengthOnKeyUp(this,1000);"
                                                    CssClass="textarea" TextMode="MultiLine" Width="900px"></cc1:ucTextbox>
                                                <asp:RequiredFieldValidator ID="rfvRejectionCommentRequired" ClientIDMode="Static"
                                                    runat="server" ControlToValidate="txtRejectCommentsR" Display="None" Style="display: none;"
                                                    ValidationGroup="Save" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </cc1:ucPanel>
                </div>
            </div>
            <div class="bottom-shadow">
            </div>
        </ContentTemplate>
<%--                     <Triggers> 
                                <asp:PostBackTrigger ControlID="btnUpdate"  />                                   
                                  <asp:PostBackTrigger ControlID="btnSelect"  /> 
                                  
                            </Triggers>--%>
                  </asp:UpdatePanel>
    
    <div class="button-row">
        <cc1:ucButton ID="btnSave" runat="server" Text="Save" CssClass="button" OnClick="btnSave_Click"
            Visible="false" ValidationGroup="Save" />
        <cc1:ucButton ID="btnResendApprovalMail" runat="server" Text="Resend Approval Mail"
            Visible="false" CssClass="button" OnClick="btnResendApprovalMail_Click" />
        <cc1:ucButton ID="btnBlockUser" runat="server" Text="Block User" CssClass="button"
            ValidationGroup="Save" Visible="false" OnClick="btnBlockUser_Click" />
        <cc1:ucButton ID="btnUnblockUser" runat="server" Text="Unblock User" CssClass="button"
            ValidationGroup="Save" Visible="false" OnClick="btnUnblockUser_Click" />
        <cc1:ucButton ID="btnDeleteRejectedUser" runat="server" Text="Delete Rejected User"
            CssClass="button" ValidationGroup="Save" Visible="false" OnClientClick="return ConfirmRejectedUserDeletion();"
            OnClick="btnDeleteRejectedUser_Click" />
        <cc1:ucButton ID="btnDeleteUser" runat="server" Text="Delete User" CssClass="button"
            ValidationGroup="Save" Visible="false" OnClick="btnDeleteUser_Click" OnClientClick="return ConfirmDeletion();" />                
        <cc1:ucButton ID="btnSave_1" runat="server" Text="Save" CssClass="button" OnClientClick="Save_ClientClick();"
            OnClick="btnSave_1_Click" ValidationGroup="Save" />
        <cc1:ucButton ID="btnProceed" runat="server" Text="Proceed" CssClass="button" 
            Visible="false" onclick="btnProceed_Click" />
        <cc1:ucButton ID="btnBack" runat="server" Text="Back" CssClass="button" OnClick="btnBack_Click"
            Visible="false" />
        <asp:ValidationSummary ID="VSSave" runat="server" ValidationGroup="Save" ShowSummary="false"
            ShowMessageBox="true" />
        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="Save_1" ShowSummary="false"
            ShowMessageBox="true" />
    </div>
   <script type="text/javascript">
       var inputVendorNo;
       var inputVendorNoSelectedName;
       var inputCountryID;
       function UpdatevendortNumberdetails(VendorNoValue, VendorNoSelectednameValue) {
           //  $('#' + inputVendorNo).trigger("change");
           $('#' + inputVendorNo).attr('value', VendorNoSelectednameValue.split('-')[0]);
           $('#' + inputVendorNoSelectedName).html(VendorNoSelectednameValue);
           return false;
       }
       function GetSelectedRow(lnk) {
           //debugger;
           var row = lnk.parentNode.parentNode;
           inputVendorNo = $(row.cells[1].innerHTML).attr('id');
           inputVendorNoSelectedName = $(row.cells[3].innerHTML).attr('id');
           inputCountryID = $(row.cells[2].innerHTML).attr('id');
           document.getElementById('<%= hdntxtVendorNo.ClientID%>').value = $(row.cells[1].innerHTML).attr('value');
           document.getElementById('<%= hdnCountryID.ClientID%>').value = $(row.cells[2].innerHTML).attr('value');
           var getIndexByID = inputVendorNo;
           document.getElementById('<%= hdnrowNumber.ClientID%>').value = parseInt(getIndexByID.replace('ctl00_ContentPlaceHolder1_rptVendorDetailsREU_ctl', '').split('_')[0]);
           //            alert($(row.cells[1].innerHTML).attr('value'));
           //            alert(document.getElementById('<%= hdntxtVendorNo.ClientID%>').value);
           //            alert(document.getElementById('<%= hdnCountryID.ClientID%>').value);
           //            alert(hdntxtVendorNo);
           return false;
       }

       function showVendorimg(input) {
           $find('VendorViewer').show();
           GetSelectedRow(input);
           return false;
       }
     

    </script>
    </asp:Content>

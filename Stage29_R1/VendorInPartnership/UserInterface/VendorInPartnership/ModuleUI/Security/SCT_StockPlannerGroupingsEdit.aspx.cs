﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.Security;
using BusinessLogicLayer.ModuleBAL.Security;
using WebUtilities;

public partial class SCT_StockPlannerGroupingsEdit : CommonPage
{
    protected string deleteMessage = WebCommon.getGlobalResourceValue("DeleteMessage");
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack) {
            if (GetQueryStringValue("StockPlannerGroupingsID") != null) {

                List<StockPlannerGroupingsBE> oStockPlannerGroupingsBEList = new List<StockPlannerGroupingsBE>();
                StockPlannerGroupingsBAL oStockPlannerGroupingsBAL = new StockPlannerGroupingsBAL();

                StockPlannerGroupingsBE oNewStockPlannerGroupingsBE = new StockPlannerGroupingsBE();

                oNewStockPlannerGroupingsBE.StockPlannerGroupingsID = Convert.ToInt32(GetQueryStringValue("StockPlannerGroupingsID"));

                if (GetQueryStringValue("opr") == "del") {
                    oNewStockPlannerGroupingsBE.Action = "ShowById";
                    oStockPlannerGroupingsBEList = oStockPlannerGroupingsBAL.GetStockPlannerGroupingsBAL(oNewStockPlannerGroupingsBE);

                    if (oStockPlannerGroupingsBEList != null && oStockPlannerGroupingsBEList.Count > 0) {
                        txtGroupName.Text = oStockPlannerGroupingsBEList[0].StockPlannerGroupings;
                        btnSave.Visible = false;
                        txtGroupName.Enabled = false;

                        
                    }
                }
                else if (GetQueryStringValue("opr") == "show") {
                    oNewStockPlannerGroupingsBE.Action = "ShowUsersById";

                    oStockPlannerGroupingsBEList = oStockPlannerGroupingsBAL.GetStockPlannerGroupingsBAL(oNewStockPlannerGroupingsBE);

                    tblshow.Style.Add("display", "");
                    tdShow.Style.Add("display", "");

                    btnDelete.Visible = false;
                    btnSave.Visible = false;

                    if (oStockPlannerGroupingsBEList != null && oStockPlannerGroupingsBEList.Count > 0) {
                        txtGroupName.Text = oStockPlannerGroupingsBEList[0].StockPlannerGroupings;
                        UcGridView1.DataSource = oStockPlannerGroupingsBEList;
                        UcGridView1.DataBind();
                        ViewState["oStockPlannerGroupingsBEList"] = oStockPlannerGroupingsBEList;
                    }
                }     
            }
            else {
                btnDelete.Visible = false;
            }
        }
        ucExportToExcel1.GridViewControl = UcGridView1;
        ucExportToExcel1.FileName = "Stock Planner Groupings";
    }


    public override BusinessEntities.BaseBe[] SubSort(GridViewSortEventArgs e) {
        return Utilities.GenericListHelper<StockPlannerGroupingsBE>.SortList((List<StockPlannerGroupingsBE>)ViewState["oStockPlannerGroupingsBEList"], e.SortExpression, e.SortDirection).ToArray();
    }

    protected void btnSave_Click(object sender, EventArgs e) {
        
        StockPlannerGroupingsBAL oStockPlannerGroupingsBAL = new StockPlannerGroupingsBAL();

        StockPlannerGroupingsBE oNewStockPlannerGroupingsBE = new StockPlannerGroupingsBE();

        oNewStockPlannerGroupingsBE.Action = "Save";
        oNewStockPlannerGroupingsBE.StockPlannerGroupings = txtGroupName.Text.Trim();
        oNewStockPlannerGroupingsBE.oUserBE = new SCT_UserBE();
        oNewStockPlannerGroupingsBE.oUserBE.UserID = Convert.ToInt32(Session["UserID"]);
        int? iResult = oStockPlannerGroupingsBAL.addEditStockPlannerGroupingsBAL(oNewStockPlannerGroupingsBE);

        if (iResult == -1) {
            string errorMessage = WebCommon.getGlobalResourceValue("SPGNameExists");
            Page.ClientScript.RegisterStartupScript(this.GetType(), "Message", "alert('" + errorMessage + "')", true);
        }
        else {
            EncryptQueryString("SCT_StockPlannerGroupings.aspx");
        }
    }

    protected void btnDelete_Click(object sender, EventArgs e) {
        if (GetQueryStringValue("StockPlannerGroupingsID") != null) {
            List<StockPlannerGroupingsBE> oStockPlannerGroupingsBEList = new List<StockPlannerGroupingsBE>();
            StockPlannerGroupingsBAL oStockPlannerGroupingsBAL = new StockPlannerGroupingsBAL();

            StockPlannerGroupingsBE oNewStockPlannerGroupingsBE = new StockPlannerGroupingsBE();

            oNewStockPlannerGroupingsBE.Action = "Delete";
            oNewStockPlannerGroupingsBE.oUserBE = new SCT_UserBE();
            oNewStockPlannerGroupingsBE.oUserBE.UserID = Convert.ToInt32(Session["UserID"]);
            oNewStockPlannerGroupingsBE.StockPlannerGroupingsID = Convert.ToInt32(GetQueryStringValue("StockPlannerGroupingsID"));
            int? iResult = oStockPlannerGroupingsBAL.addEditStockPlannerGroupingsBAL(oNewStockPlannerGroupingsBE);

            if (iResult == -1) {
                string errorMessage = WebCommon.getGlobalResourceValue("SPGExistsInUser");
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Message", "alert('" + errorMessage + "')", true);
            }
            else {
                EncryptQueryString("SCT_StockPlannerGroupings.aspx");
            }
        }
    }

    protected void btnBack_Click(object sender, EventArgs e) {
        EncryptQueryString("SCT_StockPlannerGroupings.aspx");
    }
}
﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SCT_DefaultStockPlannerUpdate.aspx.cs" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    Inherits="ModuleUI_Security_SCT_DefaultStockPlannerUpdate" MaintainScrollPositionOnPostback="true" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectCountry.ascx" TagName="MultiSelectCountry"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectSite.ascx" TagName="MultiSelectSite"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectSKU.ascx" TagName="MultiSelectSKU"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectVendor.ascx" TagName="MultiSelectVendor"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectStockPlanner.ascx" TagName="MultiSelectStockPlanner"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/ucVendorSelectionTemplate.ascx" TagName="VendorSelectTemplate"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectCustomer.ascx" TagName="MultiSelectCustomer"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/ucVikingSku.ascx" TagName="ucVikingSku"
    TagPrefix="cc1" %>
<%@ Register TagName="ucExportButton" TagPrefix="uc1" Src="~/CommonUI/UserControls/ucExportToExcel.ascx" %>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2>
        <cc1:ucLabel ID="lblDefaultStockPlannerUpdate" runat="server" Text="Default Stock Planner Update"></cc1:ucLabel>
    </h2>
    <script type="text/javascript" language="javascript">
        $(document).ready(function () {
            $("body").keypress(function (e) {
                if (e.which == 13 || e.keyCode == 13) {
                    $('#<%=btnGenerateReport.ClientID %>').click();
                    return false;
                }
            });
        });
    </script>
    <asp:ScriptManager ID="SM1" runat="server" AsyncPostBackTimeout="2400">
    </asp:ScriptManager>
    <asp:HiddenField ID="hdnJSFromDt" runat="server" />
    <asp:HiddenField ID="hdnJSToDt" runat="server" />
    <div class="right-shadow">
        <div class="formbox">
            <cc1:ucPanel ID="UcDataPanel" runat="server">
                <table width="95%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                    <tr>
                        <td align="right">
                            <cc1:VendorSelectTemplate runat="server" ID="ucVendorTemplateSelect" />
                        </td>
                    </tr>
                </table>
                <table width="95%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">

                    <tr>
                        <td style="font-weight: bold; width: 15%">
                            <cc1:ucLabel ID="lblCountry" runat="server" Text="Country">
                            </cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold; width: 1%">
                            <cc1:ucLabel ID="UcLabel5" runat="server">:</cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold; width: 84%">
                            <cc1:MultiSelectCountry runat="server" ID="msCountry" />
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblSite" runat="server" Text="Site">
                            </cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="UcLabel4" runat="server">:</cc1:ucLabel>
                        </td>
                        <td>
                            <cc1:MultiSelectSite runat="server" ID="msSite" />
                        </td>
                    </tr>

                    <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblVendor" runat="server" Text="Vendor">
                            </cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="UcLabel2" runat="server">:</cc1:ucLabel>
                        </td>
                        <td>
                            <cc1:MultiSelectVendor runat="server" ID="msVendor" />
                        </td>
                    </tr>

                    <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblCustomer" runat="server" Text="Customer">
                            </cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="UcLabel7" runat="server">:</cc1:ucLabel>
                        </td>
                        <td>
                            <cc1:MultiSelectCustomer runat="server" ID="msCustomer" />
                        </td>
                    </tr>

                    <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblStockPlannerItemFile" runat="server" Text="Stock Planner <br/>(Item File)">
                            </cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="Label17" runat="server">:</cc1:ucLabel>
                        </td>
                        <td>
                            <cc1:MultiSelectStockPlanner runat="server" ID="msStockPlanner" />
                        </td>
                    </tr>

                    <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblStockPlannerVIPDefault" runat="server" Text="Stock Planner <br/>(VIP Default)">
                            </cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="UcLabel6" runat="server">:</cc1:ucLabel>
                        </td>
                        <td>
                            <cc1:MultiSelectStockPlanner runat="server" ID="msStockPlannerDefault" />
                        </td>
                    </tr>



                    <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblODSKUCode" runat="server" Text="OD SKU Code">
                            </cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="UcLabel8" runat="server">:</cc1:ucLabel>
                        </td>
                        <td>
                            <cc1:MultiSelectSKU runat="server" ID="msSKU" />
                        </td>
                    </tr>

                    <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblVikingSku" runat="server" Text="Viking Sku">
                            </cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="UcLabel3" runat="server">:</cc1:ucLabel>
                        </td>
                        <td>
                            <%--<cc1:MultiSelectSKU runat="server" ID="msVikingSKU" />--%>
                            <cc1:ucVikingSku ID="msVikingSKU" runat="server" />
                        </td>
                    </tr>

                    <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblExcludeItemEnding" runat="server" Text="Exclude Items Ending With">
                            </cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="UcLabel9" runat="server">:</cc1:ucLabel>
                        </td>
                        <td>
                            <cc1:MultiSelectSKU runat="server" ID="msExcludeItems" />
                        </td>
                    </tr>

                    <tr>
                        <td colspan="3">
                            <table width="100%">

                                <tr style="height: 6em;">
                                    <td width="15%" style="font-weight: bold;">
                                        <cc1:ucLabel ID="lblPlannersMatch" runat="server" Text="Do planners match in item file & VIP">
                                        </cc1:ucLabel>
                                    </td>
                                    <td width="1%" style="font-weight: bold;">:
                                    </td>
                                    <td width="84%">
                                        <table cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style="font-weight: bold;" class="checkbox-list">
                                                    <cc1:ucRadioButton ID="rdoYes" runat="server" Text="Yes" GroupName="ReportType" />
                                                </td>
                                                <td style="font-weight: bold;" class="checkbox-list">
                                                    <cc1:ucRadioButton ID="rdoNo" runat="server" Text="No" GroupName="ReportType" />
                                                </td>
                                                <td style="font-weight: bold;" class="checkbox-list">
                                                    <cc1:ucRadioButton ID="rdoShowAll" runat="server" Text="Show All" GroupName="ReportType"
                                                        Checked="true" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <table width="100%">
                                            <tr>
                                                <td width="15%" style="font-weight: bold;">
                                                    <cc1:ucLabel ID="lblShowSOHItems" runat="server" Text="Show only items with stock on hand">
                                                    </cc1:ucLabel>
                                                </td>
                                                <td width="1%" style="font-weight: bold;">:
                                                </td>
                                                <td width="84%">
                                                    <table cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <td style="font-weight: bold;" class="checkbox-list">
                                                                <cc1:ucRadioButton ID="rdoYes_1" runat="server" Text="Yes" GroupName="ReportView" Checked="true" />
                                                            </td>
                                                            <td style="font-weight: bold;" class="checkbox-list">
                                                                <cc1:ucRadioButton ID="rdoNo_1" runat="server" Text="No" GroupName="ReportView" />
                                                            </td>
                                                            <td style="font-weight: bold;" class="checkbox-list">
                                                                <cc1:ucRadioButton ID="rdoShowAll_1" runat="server" Text="Show All" GroupName="ReportView" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>


                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="right" colspan="3">
                            <div class="button-row">
                                <cc1:ucButton ID="btnGenerateReport" runat="server" Text="Generate Report" CssClass="button"
                                    OnClick="btnGenerateReport_Click" />
                            </div>
                        </td>
                    </tr>
                </table>
            </cc1:ucPanel>
            <cc1:ucPanel ID="UcReportPanel" runat="server" Visible="false">
                <table style="width: 100%;">
                    <tr>
                        <td style="text-align: right">
                            <uc1:ucExportButton ID="btnExportToExcel1" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucGridView ID="gvResourceEditor" runat="server" AutoGenerateColumns="False"
                                ShowFooter="True"
                                CssClass="grid" CellPadding="0" Width="100%" GridLines="None" AllowPaging="true"
                                PageSize="50" OnPageIndexChanging="gvResourceEditor_PageIndexChanging" EmptyDataText="No Records Found"
                                EmptyDataRowStyle-ForeColor="Red" EmptyDataRowStyle-HorizontalAlign="Center"
                                OnRowDataBound="gvResourceEditor_RowDataBound">
                                <AlternatingRowStyle BackColor="White" ForeColor="#284775"></AlternatingRowStyle>
                                <Columns>
                                    <asp:TemplateField HeaderText="Site">
                                        <%-- <EditItemTemplate>
                                                <asp:Literal ID="ltKey1" runat="server" Text='<%# Eval("Site") %>'></asp:Literal>                                               
                                            </EditItemTemplate>--%>
                                        <ItemTemplate>
                                            <asp:Literal ID="ltKey2" runat="server" Text='<%# Eval("Site") %>'></asp:Literal>
                                            <asp:HiddenField ID="hdnSiteId" runat="server" Value='<%# Eval("SiteID") %>' />
                                            <asp:HiddenField ID="hdnSKUID" runat="server" Value='<%# Eval("SKUID") %>' />
                                            <asp:HiddenField ID="hdnStockPlannerID" runat="server" Value='<%# Eval("StockPlannerIDItemFile") %>' />
                                            <asp:HiddenField ID="hdnCustomerID" runat="server" Value='<%# Eval("CustomerID") %>' />
                                            <asp:HiddenField ID="hdnStockPlannerIDDefault" runat="server" Value='<%# Eval("StockPlannerIDDefault") %>' />
                                        </ItemTemplate>
                                        <HeaderStyle Width="10%" HorizontalAlign="Left" />
                                        <ItemStyle Width="10%" HorizontalAlign="Left" Wrap="true" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="ODSKU">
                                        <HeaderStyle BorderWidth="0" BorderColor="White" BorderStyle="None" />
                                        <%-- <EditItemTemplate>
                                                <asp:Literal ID="ltValue1" runat="server" Text='<%# Eval("OD_SKU_NO") %>'></asp:Literal>
                                            </EditItemTemplate>--%>
                                        <ItemTemplate>
                                            <asp:Literal ID="ltValue2" runat="server" Text='<%# Bind("OD_SKU_NO") %>'></asp:Literal>
                                        </ItemTemplate>
                                        <HeaderStyle Width="8%" HorizontalAlign="Left" />
                                        <ItemStyle Wrap="True" Width="8%" HorizontalAlign="Left" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="VikingCode">
                                        <HeaderStyle BorderWidth="0" BorderColor="White" BorderStyle="None" />
                                        <%-- <EditItemTemplate>
                                                <asp:Literal ID="ltVikingCode1" runat="server" Text='<%# Eval("Direct_SKU") %>'></asp:Literal>
                                            </EditItemTemplate>--%>
                                        <ItemTemplate>
                                            <asp:Literal ID="ltVikingCode2" runat="server" Text='<%# Bind("Direct_SKU") %>'></asp:Literal>
                                        </ItemTemplate>
                                        <HeaderStyle Width="8%" HorizontalAlign="Left" />
                                        <ItemStyle Wrap="True" Width="8%" HorizontalAlign="Left" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Description">
                                        <HeaderStyle BorderWidth="0" BorderColor="White" BorderStyle="None" />
                                        <%--<EditItemTemplate>
                                                <asp:Literal ID="ltDescription1" runat="server" Text='<%# Eval("Description") %>'></asp:Literal>
                                            </EditItemTemplate>--%>
                                        <ItemTemplate>
                                            <asp:Literal ID="ltDescription2" runat="server" Text='<%# Bind("Description") %>'></asp:Literal>
                                        </ItemTemplate>
                                        <HeaderStyle Width="10%" HorizontalAlign="Left" />
                                        <ItemStyle Wrap="True" Width="10%" HorizontalAlign="Left" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="StockPlannerItemFile">
                                        <HeaderStyle BorderWidth="0" BorderColor="White" BorderStyle="None" />
                                        <%-- <EditItemTemplate>
                                                <asp:Literal ID="ltStockPlannerItemFile1" runat="server" Text='<%# Eval("StockPlannerItemFile") %>'></asp:Literal>
                                            </EditItemTemplate>--%>
                                        <ItemTemplate>
                                            <asp:Literal ID="ltStockPlannerItemFile2" runat="server" Text='<%# Bind("StockPlannerItemFile") %>'></asp:Literal>
                                        </ItemTemplate>
                                        <HeaderStyle Width="10%" HorizontalAlign="Left" />
                                        <ItemStyle Wrap="True" Width="10%" HorizontalAlign="Left" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="PlannerLinkedTo">
                                        <HeaderStyle BorderWidth="0" BorderColor="White" BorderStyle="None" />
                                        <%--<EditItemTemplate>
                                                <asp:Literal ID="ltPlannerLinkedTo1" runat="server" Text='<%# Eval("PlannerLinkedTo") %>'></asp:Literal>
                                            </EditItemTemplate>--%>
                                        <ItemTemplate>
                                            <asp:Literal ID="ltPlannerLinkedTo2" runat="server" Text='<%# Bind("PlannerLinkedTo") %>'></asp:Literal>
                                        </ItemTemplate>
                                        <HeaderStyle Width="10%" HorizontalAlign="Left" />
                                        <ItemStyle Wrap="True" Width="10%" HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="StockPlannerVIPDefault">
                                        <HeaderStyle BorderWidth="0" BorderColor="White" BorderStyle="None" />
                                        <ItemTemplate>
                                            <cc1:ucDropdownList ID="ddlVIPDefaultSP" runat="server" Visible="<%# IsInEditMode %>" Width="110">                                                
                                            </cc1:ucDropdownList>
                                            <asp:Literal ID="ltStockPlannerVIPDefault" runat="server" Visible="<%# !(bool) IsInEditMode %>" Text='<%# Bind("VIPDefaultStockPlanner") %>'></asp:Literal>
                                        </ItemTemplate>
                                        <HeaderStyle Width="10%" HorizontalAlign="Left" />
                                        <ItemStyle Wrap="True" Width="10%" HorizontalAlign="Left" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="PlannerMatch">
                                        <HeaderStyle BorderWidth="0" BorderColor="White" BorderStyle="None" />
                                        <%-- <EditItemTemplate>
                                                <asp:Literal ID="ltPlannerMatch1" runat="server" Text='<%# Eval("PlannerMatch") %>'></asp:Literal>
                                            </EditItemTemplate>--%>
                                        <ItemTemplate>
                                            <asp:Literal ID="ltPlannerMatch2" runat="server" Text='<%# Bind("PlannerMatch") %>'></asp:Literal>
                                        </ItemTemplate>
                                        <HeaderStyle Width="8%" HorizontalAlign="Left" />
                                        <ItemStyle Wrap="True" Width="8%" HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Customer">
                                        <HeaderStyle BorderWidth="0" BorderColor="White" BorderStyle="None" />
                                        <ItemTemplate>
                                            <cc1:ucDropdownList ID="ddlCustomer" Visible="<%# IsInEditMode %>" Width="110" runat="server">                                                
                                            </cc1:ucDropdownList>
                                            <asp:Literal ID="ltCustomer" runat="server" Visible="<%# !(bool) IsInEditMode %>" Text='<%# Bind("CustomerName") %>'></asp:Literal>
                                        </ItemTemplate>
                                        <HeaderStyle Width="10%" HorizontalAlign="Left" />
                                        <ItemStyle Wrap="True" Width="10%" HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="QOH">
                                        <HeaderStyle BorderWidth="0" BorderColor="White" BorderStyle="None" />
                                        <%--<EditItemTemplate>
                                                <asp:Literal ID="ltQOH1" runat="server" Text='<%# Eval("QOH") %>'></asp:Literal>
                                            </EditItemTemplate>--%>
                                        <ItemTemplate>
                                            <asp:Literal ID="ltQOH2" runat="server" Text='<%# Bind("QOH") %>'></asp:Literal>
                                        </ItemTemplate>
                                        <HeaderStyle Width="8%" HorizontalAlign="Left" />
                                        <ItemStyle Wrap="True" Width="8%" HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="VendorName">
                                        <HeaderStyle BorderWidth="0" BorderColor="White" BorderStyle="None" />
                                        <%-- <EditItemTemplate>
                                                <asp:Literal ID="ltVendorName1" runat="server" Text='<%# Eval("VendorName") %>'></asp:Literal>
                                            </EditItemTemplate>--%>
                                        <ItemTemplate>
                                            <asp:Literal ID="ltVendorName2" runat="server" Text='<%# Bind("VendorName") %>'></asp:Literal>
                                        </ItemTemplate>
                                        <HeaderStyle Width="10%" HorizontalAlign="Left" />
                                        <ItemStyle Wrap="True" Width="10%" HorizontalAlign="Left" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="VendorPlanner">
                                        <HeaderStyle BorderWidth="0" BorderColor="White" BorderStyle="None" />
                                        <%--<EditItemTemplate>
                                                <asp:Literal ID="ltVendorPlanner1" runat="server" Text='<%# Eval("VendorPlanner") %>'></asp:Literal>
                                            </EditItemTemplate>--%>
                                        <ItemTemplate>
                                            <asp:Literal ID="ltVendorPlanner2" runat="server" Text='<%# Bind("VendorPlanner") %>'></asp:Literal>
                                        </ItemTemplate>
                                        <HeaderStyle Width="10%" HorizontalAlign="Left" />
                                        <ItemStyle Wrap="True" Width="10%" HorizontalAlign="Left" />
                                    </asp:TemplateField>

                                </Columns>
                                <RowStyle BackColor="#F7F6F3" ForeColor="#333333"></RowStyle>
                            </cc1:ucGridView>

                            <cc1:ucGridView ID="gvResourceExcel" runat="server" AutoGenerateColumns="False"
                                ShowFooter="True"
                                CssClass="grid" CellPadding="0" Width="100%" GridLines="None" AllowPaging="true"
                                PageSize="50" OnPageIndexChanging="gvResourceEditor_PageIndexChanging" EmptyDataText="No Records Found"
                                EmptyDataRowStyle-ForeColor="Red" EmptyDataRowStyle-HorizontalAlign="Center"
                                Visible="false">
                                <AlternatingRowStyle BackColor="White" ForeColor="#284775"></AlternatingRowStyle>
                                <Columns>
                                    <asp:TemplateField HeaderText="Site">
                                        <ItemTemplate>
                                            <asp:Literal ID="ltKey2" runat="server" Text='<%# Eval("Site") %>'></asp:Literal>
                                            <asp:HiddenField ID="hdnSiteId" runat="server" Value='<%# Eval("SiteID") %>' />
                                            <asp:HiddenField ID="hdnSKUID" runat="server" Value='<%# Eval("SKUID") %>' />
                                            <asp:HiddenField ID="hdnStockPlannerID" runat="server" Value='<%# Eval("StockPlannerIDItemFile") %>' />
                                            <asp:HiddenField ID="hdnCustomerID" runat="server" Value='<%# Eval("CustomerID") %>' />
                                            <asp:HiddenField ID="hdnStockPlannerIDDefault" runat="server" Value='<%# Eval("StockPlannerIDDefault") %>' />
                                        </ItemTemplate>
                                        <HeaderStyle Width="10%" HorizontalAlign="Left" />
                                        <ItemStyle Width="10%" HorizontalAlign="Left" Wrap="true" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="ODSKU">
                                        <HeaderStyle BorderWidth="0" BorderColor="White" BorderStyle="None" />
                                        <ItemTemplate>
                                            <asp:Literal ID="ltValue2" runat="server" Text='<%# Bind("OD_SKU_NO") %>'></asp:Literal>
                                        </ItemTemplate>
                                        <HeaderStyle Width="8%" HorizontalAlign="Left" />
                                        <ItemStyle Wrap="True" Width="8%" HorizontalAlign="Left" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="VikingCode">
                                        <HeaderStyle BorderWidth="0" BorderColor="White" BorderStyle="None" />
                                        <ItemTemplate>
                                            <asp:Literal ID="ltVikingCode2" runat="server" Text='<%# Bind("Direct_SKU") %>'></asp:Literal>
                                        </ItemTemplate>
                                        <HeaderStyle Width="8%" HorizontalAlign="Left" />
                                        <ItemStyle Wrap="True" Width="8%" HorizontalAlign="Left" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Description">
                                        <HeaderStyle BorderWidth="0" BorderColor="White" BorderStyle="None" />
                                        <ItemTemplate>
                                            <asp:Literal ID="ltDescription2" runat="server" Text='<%# Bind("Description") %>'></asp:Literal>
                                        </ItemTemplate>
                                        <HeaderStyle Width="10%" HorizontalAlign="Left" />
                                        <ItemStyle Wrap="True" Width="10%" HorizontalAlign="Left" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="StockPlannerItemFile">
                                        <HeaderStyle BorderWidth="0" BorderColor="White" BorderStyle="None" />
                                        <ItemTemplate>
                                            <asp:Literal ID="ltStockPlannerItemFile2" runat="server" Text='<%# Bind("StockPlannerItemFile") %>'></asp:Literal>
                                        </ItemTemplate>
                                        <HeaderStyle Width="10%" HorizontalAlign="Left" />
                                        <ItemStyle Wrap="True" Width="10%" HorizontalAlign="Left" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="PlannerLinkedTo">
                                        <HeaderStyle BorderWidth="0" BorderColor="White" BorderStyle="None" />
                                        <ItemTemplate>
                                            <asp:Literal ID="ltPlannerLinkedTo2" runat="server" Text='<%# Bind("PlannerLinkedTo") %>'></asp:Literal>
                                        </ItemTemplate>
                                        <HeaderStyle Width="10%" HorizontalAlign="Left" />
                                        <ItemStyle Wrap="True" Width="10%" HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="StockPlannerVIPDefault">
                                        <HeaderStyle BorderWidth="0" BorderColor="White" BorderStyle="None" />

                                        <ItemTemplate>
                                            <asp:Literal ID="ltStockPlannerVIPDefault" runat="server" Visible="<%# !(bool) IsInEditMode %>" Text='<%# Bind("VIPDefaultStockPlanner") %>'></asp:Literal>
                                        </ItemTemplate>
                                        <HeaderStyle Width="10%" HorizontalAlign="Left" />
                                        <ItemStyle Wrap="True" Width="10%" HorizontalAlign="Left" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="PlannerMatch">
                                        <HeaderStyle BorderWidth="0" BorderColor="White" BorderStyle="None" />
                                        <ItemTemplate>
                                            <asp:Literal ID="ltPlannerMatch2" runat="server" Text='<%# Bind("PlannerMatch") %>'></asp:Literal>
                                        </ItemTemplate>
                                        <HeaderStyle Width="8%" HorizontalAlign="Left" />
                                        <ItemStyle Wrap="True" Width="8%" HorizontalAlign="Left" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Customer">
                                        <HeaderStyle BorderWidth="0" BorderColor="White" BorderStyle="None" />
                                        <ItemTemplate>
                                            <asp:Literal ID="ltCustomer" runat="server" Visible="<%# !(bool) IsInEditMode %>" Text='<%# Bind("CustomerName") %>'></asp:Literal>
                                        </ItemTemplate>
                                        <HeaderStyle Width="10%" HorizontalAlign="Left" />
                                        <ItemStyle Wrap="True" Width="10%" HorizontalAlign="Left" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="QOH">
                                        <HeaderStyle BorderWidth="0" BorderColor="White" BorderStyle="None" />

                                        <ItemTemplate>
                                            <asp:Literal ID="ltQOH2" runat="server" Text='<%# Bind("QOH") %>'></asp:Literal>
                                        </ItemTemplate>
                                        <HeaderStyle Width="8%" HorizontalAlign="Left" />
                                        <ItemStyle Wrap="True" Width="8%" HorizontalAlign="Left" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="VendorName">
                                        <HeaderStyle BorderWidth="0" BorderColor="White" BorderStyle="None" />
                                        <ItemTemplate>
                                            <asp:Literal ID="ltVendorName2" runat="server" Text='<%# Bind("VendorName") %>'></asp:Literal>
                                        </ItemTemplate>
                                        <HeaderStyle Width="10%" HorizontalAlign="Left" />
                                        <ItemStyle Wrap="True" Width="10%" HorizontalAlign="Left" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="VendorPlanner">
                                        <HeaderStyle BorderWidth="0" BorderColor="White" BorderStyle="None" />

                                        <ItemTemplate>
                                            <asp:Literal ID="ltVendorPlanner2" runat="server" Text='<%# Bind("VendorPlanner") %>'></asp:Literal>
                                        </ItemTemplate>
                                        <HeaderStyle Width="10%" HorizontalAlign="Left" />
                                        <ItemStyle Wrap="True" Width="10%" HorizontalAlign="Left" />
                                    </asp:TemplateField>

                                </Columns>
                                <RowStyle BackColor="#F7F6F3" ForeColor="#333333"></RowStyle>
                            </cc1:ucGridView>
                        </td>
                    </tr>
                </table>
                <div class="bottom-shadow">
                </div>
                <div class="button-row">
                    <cc1:ucButton ID="btnEdit" runat="server" Text="Edit" CssClass="button" OnClick="btnEdit_Click" />
                    <cc1:ucButton ID="btnSave" runat="server" Text="Save" CssClass="button" OnClick="btnSave_Click" Visible="false" />
                    <cc1:ucButton ID="btnCancel" runat="server" Text="Cancel" CssClass="button" OnClick="btnCancel_Click" Visible="false" />
                    <cc1:ucButton ID="btnBack" runat="server" Text="Back" CssClass="button" OnClick="btnBack_Click" />
                </div>
            </cc1:ucPanel>
        </div>
    </div>
</asp:Content>


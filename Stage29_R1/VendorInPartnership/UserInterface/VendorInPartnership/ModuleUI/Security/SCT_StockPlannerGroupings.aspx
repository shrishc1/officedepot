﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    CodeFile="SCT_StockPlannerGroupings.aspx.cs" Inherits="SCT_StockPlannerGroupings"
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/ucAddButton.ascx" TagName="ucAddButton"
    TagPrefix="cc2" %>
<%@ Register Src="~/CommonUI/UserControls/ucExportToExcel.ascx" TagName="ucExportToExcel"
    TagPrefix="cc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2>
        <cc1:ucLabel ID="lblStockPlannerGroupings" runat="server"></cc1:ucLabel>
    </h2>
    <div class="right-shadow">
        <div class="formbox">
            <div class="button-row">
                <cc2:ucAddButton ID="btnAdd" runat="server" NavigateUrl="~/ModuleUI/Security/SCT_StockPlannerGroupingsEdit.aspx" />
                
                              <cc1:ucButton ID="ucExportToExcel1" runat="server" CssClass="exporttoexcel" Text="Export To Excel" OnClick="btnExport_Click"
                        Width="109px" Height="20px"  />                
            </div>
            <cc1:ucGridView ID="UcGridView1" Width="100%" runat="server" CssClass="grid" OnSorting="SortGrid"
                AllowSorting="true">
                <Columns>
                    <asp:TemplateField HeaderText="Name" SortExpression="StockPlannerGroupings">
                        <HeaderStyle Width="50%" HorizontalAlign="Left" />
                        <ItemTemplate>
                            <asp:HyperLink ID="hpStockPlannerGroupings" runat="server" Text='<%# Eval("StockPlannerGroupings") %>'
                                NavigateUrl='<%# EncryptQuery("SCT_StockPlannerGroupingsEdit.aspx?opr=del&StockPlannerGroupingsID="+ Eval("StockPlannerGroupingsID")) %>'></asp:HyperLink>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="#Users" SortExpression="UserCount">
                        <HeaderStyle HorizontalAlign="Left" Width="50%" />
                        <ItemStyle HorizontalAlign="Left" />
                        <ItemTemplate>
                            <asp:HyperLink ID="hpStockPlannerGroupings" runat="server" Text='<%# Eval("UserCount") %>'
                                NavigateUrl='<%# EncryptQuery("SCT_StockPlannerGroupingsEdit.aspx?opr=show&StockPlannerGroupingsID="+ Eval("StockPlannerGroupingsID")) %>'></asp:HyperLink>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </cc1:ucGridView>
            <asp:GridView runat="server" ID="gvExport" AutoGenerateColumns="true" Visible="false">
            </asp:GridView>
        </div>
    </div>
</asp:Content>

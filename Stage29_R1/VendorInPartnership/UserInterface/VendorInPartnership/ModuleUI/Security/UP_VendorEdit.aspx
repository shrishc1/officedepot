﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    AutoEventWireup="true" CodeFile="UP_VendorEdit.aspx.cs" Inherits="UP_VendorEdit" MaintainScrollPositionOnPostback="true" %>
<%@ Register Src="../../CommonUI/UserControls/ucCountry.ascx" TagName="ucCountry"
    TagPrefix="cc2" %>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script type="text/javascript">
        $(document).ready(function () {
            $("body").keypress(function (e) {
                if (e.which == 13 || e.keyCode == 13) {
                    $('#<%=btnSearch.ClientID %>').click();
                    return false;
                }
            });
        });
    </script>
    <h2>
        <cc1:ucLabel ID="lblVendorEdit" runat="server" Text="Vendor Edit"></cc1:ucLabel>
    </h2>
    <div style='overflow-x: auto; width: 990px' class="grid">
    <br />
    <table width="40%" cellspacing="1" cellpadding="0" border="0" align="center" class="top-settings">
            <tr>
                <td width="9%" style="font-weight: bold;">
                    <cc1:ucLabel ID="lblCountry" runat="server" />
                </td>
                <td style="font-weight: bold; width: 1%">
                    :
                </td>
                <td width="20%">
                    <cc2:ucCountry runat="server" ID="ddlCountry" />
                </td>
                <td width="70%">
                    <cc1:ucButton runat="server" ID="btnSearch" CssClass="button" 
                        onclick="btnSearch_Click"  />
                </td>
            </tr>
        </table>
        <br />
        <cc1:ucGridView ID="gvVendorEdit" runat="server" AutoGenerateColumns="false" 
            AllowPaging="true" PageSize="25"  CssClass="grid" 
            onpageindexchanging="gvVendorEdit_PageIndexChanging">
            <Columns>
                <asp:TemplateField HeaderText="Update">
                    <HeaderStyle Width="50px" HorizontalAlign="center" />
                    <ItemTemplate>
                        <asp:ImageButton ID="btnUpdate" ImageUrl="~/Images/update.jpg" runat="server"
                            CommandArgument='<%#Eval("VendorID") %>' CommandName="Update"
                            OnCommand="btnUpdate_Click" /></ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Delete">
                    <HeaderStyle Width="50px" HorizontalAlign="center" />
                    <ItemTemplate>
                        <asp:ImageButton ID="btnDelete" runat="server" ImageUrl="~/Images/delete.gif" OnCommand="btnDelete_Click"
                            CommandArgument='<%#Eval("VendorID") %>' /></ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" />
                </asp:TemplateField>
                <asp:BoundField DataField="CountryName" HeaderText="Vendor Country" SortExpression="Vendor_Country" />
                <asp:BoundField DataField="Vendor_No" HeaderText="Vendor No" SortExpression="Vendor_No" />
                <asp:BoundField DataField="VendorName" HeaderText="Name" SortExpression="Vendor_Name" />
                <asp:BoundField DataField="VendorContactName" HeaderText="Contact Name" SortExpression="Vendor_Contact_Name" />
                <asp:BoundField DataField="VendorContactNumber" HeaderText="Contact Number" SortExpression="Vendor_Contact_Number" />
                <asp:BoundField DataField="VendorContactFax" HeaderText="Fax" SortExpression="Vendor_Contact_Fax" />
                <asp:BoundField DataField="VendorContactEmail" HeaderText="Email" SortExpression="Vendor_Contact_Email" />
                <asp:BoundField DataField="Fax_country_code" HeaderText="Fax Country Code" SortExpression="Fax_country_code" />
                <asp:BoundField DataField="Fax_no" HeaderText="Fax No" SortExpression="Fax_no" />
                <asp:BoundField DataField="Buyer" HeaderText="Buyer" SortExpression="Buyer" />
                <asp:BoundField DataField="address1" HeaderText="Address 1" SortExpression="address1" />
                <asp:BoundField DataField="address2" HeaderText="Address 2" SortExpression="address2" />
                <asp:BoundField DataField="city" HeaderText="City" SortExpression="city" />
                <asp:BoundField DataField="county" HeaderText="County" SortExpression="county" />
                <asp:BoundField DataField="VMPPIN" HeaderText="VMPPIN" SortExpression="VMPPIN" />
                <asp:BoundField DataField="VMPPOU" HeaderText="VMPPOU" SortExpression="VMPPOU" />
                <asp:BoundField DataField="VMTCC1" HeaderText="VMTCC1" SortExpression="VMTCC1" />
                <asp:BoundField DataField="VMTEL1" HeaderText="VMTEL1" SortExpression="VMTEL1" />
               
            </Columns>
        </cc1:ucGridView>
    </div>
</asp:Content>

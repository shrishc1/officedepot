﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.Security;
using BusinessLogicLayer.ModuleBAL.Security;
using WebUtilities;
using System.Data;

public partial class SCT_StockPlannerGroupings : CommonPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack) {
            BindStockPlannerGroupings();
        }
       
    }

    private void BindStockPlannerGroupings() {
        List<StockPlannerGroupingsBE> oStockPlannerGroupingsBEList = new List<StockPlannerGroupingsBE>();
        StockPlannerGroupingsBAL oStockPlannerGroupingsBAL = new StockPlannerGroupingsBAL();

        StockPlannerGroupingsBE oNewStockPlannerGroupingsBE = new StockPlannerGroupingsBE();

        oNewStockPlannerGroupingsBE.Action = "ShowAllSPGroupings";

        oStockPlannerGroupingsBEList = oStockPlannerGroupingsBAL.GetStockPlannerGroupingsBAL(oNewStockPlannerGroupingsBE);

        if (oStockPlannerGroupingsBEList != null && oStockPlannerGroupingsBEList.Count > 0) {
            UcGridView1.DataSource = oStockPlannerGroupingsBEList;
            UcGridView1.DataBind();
            ViewState["oStockPlannerGroupingsBEList"] = oStockPlannerGroupingsBEList;
        }
    }
    protected void btnExport_Click(object sender, EventArgs e)
    {
        StockPlannerGroupingsBAL oStockPlannerGroupingsBAL = new StockPlannerGroupingsBAL();

        StockPlannerGroupingsBE oNewStockPlannerGroupingsBE = new StockPlannerGroupingsBE();

        oNewStockPlannerGroupingsBE.Action = "SPGroupingsExport";
        DataTable dt = new DataTable();
        dt = oStockPlannerGroupingsBAL.BindStockPlannerGroupingBAL(oNewStockPlannerGroupingsBE);
        if (dt.Rows.Count > 0)
        {
            gvExport.DataSource = dt;
            gvExport.DataBind();
        }
        else
        {
            gvExport.DataSource = null;
            gvExport.DataBind();
        }
        WebCommon.ExportHideHidden("StockPlannerGroupings", gvExport, null, false, true);   

    }
    
    public override BusinessEntities.BaseBe[] SubSort(GridViewSortEventArgs e) {
        return Utilities.GenericListHelper<StockPlannerGroupingsBE>.SortList((List<StockPlannerGroupingsBE>)ViewState["oStockPlannerGroupingsBEList"], e.SortExpression, e.SortDirection).ToArray();
    }
}
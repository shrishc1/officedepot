﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    AutoEventWireup="true" CodeFile="Template.aspx.cs" Inherits="ModuleUI_Security_UserScreen_Template" %>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script type="text/javascript">
        function btnSave_ClientClick() {
            var lstReadScreenID = new Array();
            var lstWriteScreenID = new Array();
            $('input:checked').each(function () {
                if ($(this).attr('id').substring(0, 1) == 'R') {
                    lstReadScreenID.push($(this).attr('data-screen-id').toString());
                }
                else {
                    lstWriteScreenID.push($(this).attr('data-screen-id').toString());
                }
            });
            $('#hfReadScreenID').val(lstReadScreenID);
            $('#hfWriteScreenID').val(lstWriteScreenID);
        }
    </script>
    <div>
        <asp:Label runat="server" ID="lblTemplate" Width="50px">Template</asp:Label>
        <asp:TextBox runat="server" ID="txtTemplate" ClientIDMode="Static" />
        <br />
        <asp:Label runat="server" ID="lblRoles" Width="50px">For Role Type</asp:Label>
        <asp:DropDownList ID="ddlRoles" runat="server" />
        <asp:HiddenField id="hfReadScreenID" runat="server" ClientIDMode="Static" />
        <asp:HiddenField id="hfWriteScreenID" runat="server" ClientIDMode="Static" />
    </div>
    <asp:Literal runat="server" ID="ltMenu"></asp:Literal>
    <cc1:ucButton runat="server" Text="Save" ID="btnSave" 
        OnClientClick="btnSave_ClientClick();" onclick="btnSave_Click" />
</asp:Content>

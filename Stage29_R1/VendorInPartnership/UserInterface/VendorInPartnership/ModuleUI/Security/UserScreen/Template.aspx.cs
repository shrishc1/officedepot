﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI;
using BusinessEntities.ModuleBE.Security;
using BusinessLogicLayer.ModuleBAL.Security;
using DataAccessLayer;
using System.Web.Services;

public partial class ModuleUI_Security_UserScreen_Template : CommonPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            GenerateScreenTable();
        }
    }

    protected override void OnPreRender(EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            CommonPage cm = new CommonPage();
            cm.GlobalResourceFields(this.Page);
        }
    }


    #region Method

    System.Text.StringBuilder sbMemu = new System.Text.StringBuilder();

    string TableStartString = @"<table><thead><tr><td>Screen</td><td>Read</td><td>Write</td></tr></thead><tbody>";
    string TableEndString = "</tbody></table>";

    private string GetStartModuleString(string ModuleName, int ParentID)
    {
        if (ParentID == 0)
            return "<tr><td>" + ModuleName + "</td><td></td><td></td></tr>";
        else
            return "<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + ModuleName + "</td><td></td><td></td></tr>";
    }

    private string GetScreenString(string ScreenID, string ScreenName, string ScreenUrl)
    {
        //string Path = ResolveClientUrl(ScreenUrl);
        return "<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + ScreenName + "</td><td><input type='checkbox' data-screen-id='" + ScreenID + "' id='R" + ScreenID + "' /></td><td><input type='checkbox'data-screen-id='" + ScreenID + "' + id='W" + ScreenID + "'/></td></tr>";
    }

    private bool IsAllowedScreenToCurrenUser(string ScreenID)
    {
        return true;
    }
    private void GenerateScreenTable()
    {
        GetModule("0");
        ltMenu.Text = TableStartString + sbMemu.ToString() + TableEndString;
    }

    private void GetModule(string ParentModuleID)
    {
        SCT_ModuleBAL oSCT_ModuleBAL = new SCT_ModuleBAL();
        SCT_ModuleBE oSCT_ModuleBE = new SCT_ModuleBE();

        oSCT_ModuleBE.Action = "GetModule";
        oSCT_ModuleBE.ParentModuleID = Convert.ToInt32(ParentModuleID);

        List<SCT_ModuleBE> lstModules = new List<SCT_ModuleBE>();

        lstModules = oSCT_ModuleBAL.GetModuleBAL(oSCT_ModuleBE);
        oSCT_ModuleBAL = null;
        for (int iCount = 0; iCount <= lstModules.Count - 1; iCount++)
        {
            sbMemu.AppendLine(GetStartModuleString(lstModules[iCount].ModuleName, Convert.ToInt32(ParentModuleID)));
            GetModule(lstModules[iCount].ModuleID.ToString());
            GetScreen(lstModules[iCount].ModuleID.ToString());

        }
    }

    private void GetScreen(string ModuleID)
    {

        SCT_UserScreenBE oSCT_UserScreenBE = new SCT_UserScreenBE();
        SCT_UserScreenBAL oSCT_UserScreenBAL = new SCT_UserScreenBAL();

        oSCT_UserScreenBE.Screen = new SCT_ScreenBE();
        oSCT_UserScreenBE.Action = "GetUserScreen";
        oSCT_UserScreenBE.Screen.ModuleID = Convert.ToInt32(ModuleID);
        oSCT_UserScreenBE.UserID = Convert.ToInt32(Session["UserID"]);

        List<SCT_UserScreenBE> lstUserScreen = new List<SCT_UserScreenBE>();

        lstUserScreen = oSCT_UserScreenBAL.GetUserScreenBAL(oSCT_UserScreenBE);
        oSCT_UserScreenBAL = null;
        for (int iCount = 0; iCount <= lstUserScreen.Count - 1; iCount++)
        {
            sbMemu.AppendLine(GetScreenString(lstUserScreen[iCount].ScreenID.ToString(), lstUserScreen[iCount].Screen.ScreenName.ToString(), lstUserScreen[iCount].Screen.ScreenUrl.ToString()));
        }
    }
    #endregion
    protected void btnSave_Click(object sender, EventArgs e)
    {
        string[] readscreen = hfReadScreenID.Value.Split(',');
        string[] writescreen = hfWriteScreenID.Value.Split(',');

    }
}
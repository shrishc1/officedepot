﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    AutoEventWireup="true" CodeFile="SCT_TermsAndConditions.aspx.cs" Inherits="ModuleUI_Security_SCT_TermsAndConditions" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h2>
        <cc1:ucLabel ID="lblUSERINDEMNITYAGREEMENT" runat="server" ></cc1:ucLabel>
    </h2>
    <div >
        <div >
            <table width="100%"  cellspacing="5" cellpadding="0"  >
                <tr>
                    <td>
                        <iframe id="ifrmTermsAndCondition" runat="server" frameborder="0" 
                            height="480px"  width="100%" style="overflow:auto"  >
                        </iframe>
                    </td>
                </tr>
                <tr>
                    <td>
                        <cc1:ucButton ID="btnAccept" runat="server"  CssClass="button" 
                            onclick="btnAccept_Click" />
                        <cc1:ucButton ID="btnDecline" runat="server"  CssClass="button" 
                            onclick="btnDecline_Click"/>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>

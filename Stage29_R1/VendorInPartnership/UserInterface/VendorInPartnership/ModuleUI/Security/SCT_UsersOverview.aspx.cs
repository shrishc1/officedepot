﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLogicLayer.ModuleBAL.Security;
using BusinessEntities.ModuleBE.Security;
using System.Collections.Generic;
using System.Linq;
using WebUtilities;
using BusinessEntities.ModuleBE.AdminFunctions;
using BusinessLogicLayer.ModuleBAL.AdminFunctions;
using BaseControlLibrary;
using Utilities;
using System.Data;

public partial class SCT_UsersOverview : CommonPage
{
    #region Declarations ...


    public SortDirection GridViewSortDirection {
        get {
            if (ViewState["GridViewSortDirection"] == null)
                ViewState["GridViewSortDirection"] = SortDirection.Ascending;
            return (SortDirection)ViewState["GridViewSortDirection"];
        }
        set { ViewState["GridViewSortDirection"] = value; }
    }

    public string SortDir {
        get {
            if (GridViewSortDirection == SortDirection.Ascending) {
                return "DESC";
            }
            else {
                return "ASC";
            }
        }
    }

    public string GridViewSortExp {
        get {
            if (ViewState["GridViewSortExp"] == null)
                ViewState["GridViewSortExp"] = "LoginID";
            return (string)ViewState["GridViewSortExp"];
        }
        set { ViewState["GridViewSortExp"] = value; }
    }
    
    protected string AllRejectedUserdeleteMessage = WebCommon.getGlobalResourceValue("AllRejectedUserdeleteMessage");
    const int PageSize = 30;
    const int DefaultPageIndex = 1;
    List<SCT_UserBE> lstUserSettings = new List<SCT_UserBE>();
    List<SCT_UserBE> lstCountrySettings = new List<SCT_UserBE>();

    #endregion

    #region Events ...

    protected void Page_InIt(object sender, EventArgs e)
    {
        if (ScriptManager.GetCurrent(Page) == null)
        {
            ScriptManager sMgr = new ScriptManager();
            Page.Form.Controls.AddAt(0, sMgr);
        }

        ddlCountry.CurrentPage = this;
        ddlCountry.IsAllRequired = true;
        btnExportToExcel1.GridViewControl = gvUserExcelExport;
        btnExportToExcel1.FileName = "UserOverview";
        btnExportToExcel1.IsExportUsersOverview = true;
        msVendor.VendorFillOnSearch = true;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        //txtUserNameValue.Focus();
    }

    protected override void OnPreRender(EventArgs e)
    {
        if (!IsPostBack)
        {
            GetReadOnlyModule();
            //ddlCountry.innerControlddlCountry.Items.Insert(0, new ListItem("--Select--", "-1"));
            //ddlCountry.innerControlddlCountry.SelectedIndex = 0;

            if (GetQueryStringValue("NewLoginRequest") != null && GetQueryStringValue("NewLoginRequest") == "Y") {
                ddlStatus.SelectedValue = "Registered Awaiting Approval";
                rdoOfficeDepotUserOverview.Checked = true;
            }

            BindRole();

            if ((GetQueryStringValue("NewLoginRequest") != null && GetQueryStringValue("NewLoginRequest").ToString() == "Y") || (Session["UsersOverSearch"] != null))
                BindUserGrid();            
            else
                rdoOfficeDepotUserOverview.Checked = true;

            if (SortDir == "ASC") {
                GridViewSortDirection = SortDirection.Descending;
            }
            else {
                GridViewSortDirection = SortDirection.Ascending;
            }
        }

        //btnExportToExcel1.GridViewControl = gvUser;
        //btnExportToExcel1.FileName = "UserOverview";

        //ddlCountry.CurrentPage = this;
        //ddlSeacrhVendor.CurrentPage = this;
        //ddlSeacrhVendor.FunctionCalled = "GetAllVendor";
        base.OnPreRender(e);

        msVendor.setVendorsOnPostBack();
        msCarrier.setCarriersOnPostBack();
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try {
            SCT_UserBAL oSCT_UserBAL = new SCT_UserBAL();
            SCT_UserBE oSCT_UserBE = new SCT_UserBE();
            List<SCT_UserBE> lstUsers = new List<SCT_UserBE>();

            oSCT_UserBE.Action = "GetUserOverviewNew";
            oSCT_UserBE.AccountStatus = string.IsNullOrEmpty(ddlStatus.SelectedValue) ? null : ddlStatus.SelectedValue;
            //oSCT_UserBE.CountryId = ddlCountry.innerControlddlCountry.SelectedIndex > 0 ? Convert.ToInt32(ddlCountry.innerControlddlCountry.SelectedItem.Value) : (int?)null;
            oSCT_UserBE.CountryId = Convert.ToInt32(ddlCountry.innerControlddlCountry.SelectedItem.Value);

            ucListBox lstCarrierRight = msCarrier.FindControl("lstRight") as ucListBox;
            if (lstCarrierRight.Items.Count <= 0) {
                msCarrier.SelectedCarrierIDs = string.Empty;
                msCarrier.SelectedCarrierName = string.Empty;
            }

            #region Vendor search related ..
            if (!string.IsNullOrEmpty(msVendor.SelectedVendorIDs))
                oSCT_UserBE.VendorIDs = msVendor.SelectedVendorIDs;

            if (!string.IsNullOrEmpty(msVendor.SelectedVendorName))
                oSCT_UserBE.VendorName = msVendor.SelectedVendorName;

            if (!string.IsNullOrEmpty(msCarrier.SelectedCarrierIDs))
                oSCT_UserBE.CarrierIDs = msCarrier.SelectedCarrierIDs;

            if (!string.IsNullOrEmpty(msCarrier.SelectedCarrierName))
                oSCT_UserBE.CarrierName = msCarrier.SelectedCarrierName;

            oSCT_UserBE.UserRoleID = ddlRole.SelectedIndex > 0 ? Convert.ToInt32(ddlRole.SelectedItem.Value) : (int?)null;
            oSCT_UserBE.RoleName = Convert.ToString(ddlRole.SelectedItem.Text);

            TextBox txtSearchedVendor = msVendor.FindControl("ucVendor").FindControl("txtVendorNo") as TextBox;
            if (txtSearchedVendor != null)
                oSCT_UserBE.SearchVendorText = txtSearchedVendor.Text;

            TextBox txtSearchedCarrier = msCarrier.FindControl("ucCarrier").FindControl("txtCarrier") as TextBox;
            if (txtSearchedCarrier != null)
                oSCT_UserBE.SearchCarrierText = txtSearchedCarrier.Text;

            if (txtUserNameValue != null)
                oSCT_UserBE.UserSearchText = txtUserNameValue.Text;
            #endregion

            msVendor.setVendorsOnPostBack();
            msCarrier.setCarriersOnPostBack();

            if (rdoCarrierOverview.Checked)
                oSCT_UserBE.ReportType = "CarrierOverview";            
            else if (rdoVendorOverview.Checked)
                oSCT_UserBE.ReportType = "VendorOverview";
            else if (rdoSchedulingOverview.Checked)
                oSCT_UserBE.ReportType = "SchedulingOverview";
            else if (rdoDiscrepancyOverview.Checked)
                oSCT_UserBE.ReportType = "DiscrepancyOverview";
            else if (rdoOfficeDepotUserOverview.Checked)
                oSCT_UserBE.ReportType = "OfficeDepotUserOverview";
            else if (rdoVendorPOCOverview.Checked)
                oSCT_UserBE.ReportType = "VendorPOCOverview";
          

            if (isParameterconflicts(oSCT_UserBE)) { return; }


            //********** Vendor ***************
            #region Retain Vendor
            TextBox txtVendo = (TextBox)msVendor.FindControl("ucVendor").FindControl("txtVendorNo");
            HiddenField hiddenSelectedIDs = (HiddenField)msVendor.FindControl("hiddenSelectedIDs");
            HiddenField hiddenSelectedName = (HiddenField)msVendor.FindControl("hiddenSelectedName");
            string txtVendor = (txtVendo.Text != null) ? txtVendo.Text : "";
            string VendorId = (hiddenSelectedIDs.Value != null) ? hiddenSelectedIDs.Value : "";
            string VendorName = (hiddenSelectedIDs.Value != null) ? hiddenSelectedIDs.Value : "";
            TextBox txtVendorId = msVendor.FindControl("ucVendor").FindControl("txtVendorNo") as TextBox;

            bool IsSearchedByVendorNo = (msVendor.IsSearchedByVendorNo != null) ? Convert.ToBoolean(msVendor.IsSearchedByVendorNo) : false;
            string txtVendorIdText = txtVendorId.Text;
            ucListBox lstRightVendor = msVendor.FindControl("lstRight") as ucListBox;
            ucListBox lstLeftVendor = msVendor.FindControl("ucVendor").FindControl("lstLeft") as ucListBox;
            string strVendorId = string.Empty;
            string strVendorName = string.Empty;
            int value;
            lstLeftVendor.Items.Clear();
            if (lstLeftVendor != null && lstRightVendor != null && (!string.IsNullOrEmpty(VendorId) || !string.IsNullOrEmpty(VendorId)))
            {
                lstLeftVendor.Items.Clear();
                //lstRightVendor.Items.Clear();

                if (IsSearchedByVendorNo == true)
                {
                    msVendor.SearchVendorNumberClick(txtVendor);
                    //parsing successful 
                }
                else
                {
                    msVendor.SearchVendorClick(txtVendor);
                    //parsing failed. 
                }

                if (!string.IsNullOrEmpty(VendorId))
                {
                    string[] strIncludeVendorIDs = VendorId.Split(',');
                    string[] strIncludeSelectedNames = hiddenSelectedName.Value.Split(',');
                    for (int index = 0; index < strIncludeVendorIDs.Length; index++)
                    {
                        ListItem listItem = lstLeftVendor.Items.FindByValue(strIncludeVendorIDs[index]);
                        if (listItem != null)
                        {

                            strVendorId += strIncludeVendorIDs[index] + ",";
                            strVendorName += strIncludeSelectedNames[index] + ",";
                            lstRightVendor.Items.Add(listItem);
                            lstLeftVendor.Items.Remove(listItem);
                        }
                    }
                }

                HiddenField hdnSelectedVendor = msVendor.FindControl("hiddenSelectedIDs") as HiddenField;
                hdnSelectedVendor.Value = strVendorId;


                hiddenSelectedName.Value = strVendorName;
            }
            # endregion 

            lstUsers = oSCT_UserBAL.GetUsersBAL(oSCT_UserBE);
            oSCT_UserBAL = null;
            
            gvUser.PageIndex = 0;
            
            
            SetGridColumns();

            gvUser.DataSource = null;
            gvUser.DataBind();
            gvUserExcelExport.DataSource = null;
            gvUserExcelExport.DataBind();

            gvUser.DataSource = lstUsers;
            gvUser.DataBind();
            gvUserExcelExport.DataSource = lstUsers;
            gvUserExcelExport.DataBind();
            ViewState["lstUsers"] = lstUsers;

            if (oSCT_UserBE.ReportType == "VendorOverview")
                GridViewSortExp = "Vendor.Vendor_Name";
            else
                GridViewSortExp = "LoginID";
            GridViewSortDirection = SortDirection.Ascending;

            Session["UsersOverSearch"] = oSCT_UserBE;
        }
        catch (Exception ex) {
            LogUtility.SaveErrorLogEntry(ex);
        }
    }

    private bool isParameterconflicts(SCT_UserBE oSCT_UserBE) {
        bool isError = false;    
        if (oSCT_UserBE.ReportType == "CarrierOverview" && (oSCT_UserBE.RoleName != "Carrier" || !string.IsNullOrEmpty(oSCT_UserBE.VendorIDs))) {           
            isError = true;
        }
        else if (oSCT_UserBE.ReportType == "OfficeDepotUserOverview" &&
            (oSCT_UserBE.RoleName == "Carrier" || oSCT_UserBE.RoleName == "Vendor" || !string.IsNullOrEmpty(oSCT_UserBE.VendorIDs) || !string.IsNullOrEmpty(oSCT_UserBE.CarrierIDs))) {
            isError = true;
        }
        else if ((oSCT_UserBE.ReportType == "SchedulingOverview" || oSCT_UserBE.ReportType == "DiscrepancyOverview" 
            || oSCT_UserBE.ReportType == "VendorOverview" || oSCT_UserBE.ReportType == "VendorPOCOverview") &&
                    (oSCT_UserBE.RoleName != "Vendor" || !string.IsNullOrEmpty(oSCT_UserBE.CarrierIDs))) {
            isError = true;
        }    

        if (isError) {
            string errMsg = "Please check you selection parameter as there is a conflict.";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "popup", "<script>alert('" + errMsg + "');</script>", false);
        }
        return isError;
    }

    private void SortGridView(string sortExpression, SortDirection direction) {
        List<SCT_UserBE> lstUsers = new List<SCT_UserBE>();
        try {
            if (ViewState["lstUsers"] != null) {

                lstUsers = Utilities.GenericListHelper<SCT_UserBE>.SortList((List<SCT_UserBE>)ViewState["lstUsers"], sortExpression, direction);


                SetGridColumns();
                gvUser.DataSource = lstUsers;
                gvUser.DataBind();

                CommonPage c = new CommonPage();
                c.LocalizeGridHeader(gvUser);
            }
        }
        catch (Exception ex) {
            LogUtility.SaveErrorLogEntry(ex);
        }

    }

    private void SetGridColumns(string GridID = "User") {
        //Hide Show Columns
        //gvUser.Columns.OfType<TemplateField>().Cast<TemplateField>().ToList().ForEach(col => col.Visible = true);
        //gvUser.Columns.OfType<BoundField>().Cast<BoundField>().ToList().ForEach(col => col.Visible = true);

        ucGridView gvUserOverview = gvUser;
        if (GridID == "Export")
        {
            gvUserOverview = gvUserExcelExport;
        }
        
        gvUserOverview.Columns[1].Visible = false;  // Carrier Name
        gvUserOverview.Columns[2].Visible = false;  // Default Site        
        gvUserOverview.Columns[3].Visible = false;  // VendorName
        gvUserOverview.Columns[4].Visible = false;  // VendorNo
        gvUserOverview.Columns[5].Visible = false;  // VendorEuropeanorLocal
        gvUserOverview.Columns[6].Visible = false;  // EUConsolidationCode
        gvUserOverview.Columns[7].Visible = false;  // LocalConsolidationCode       
        gvUserOverview.Columns[10].Visible = false;  // Email address POC
        gvUserOverview.Columns[12].Visible = false; //Role Name
        gvUserOverview.Columns[13].Visible = false; //Role Template
        gvUserOverview.Columns[14].Visible = false; //OD Job Title

        if (rdoVendorPOCOverview.Checked) {
            gvUserOverview.Columns[9].Visible = false;  // User ID           
            gvUserOverview.Columns[15].Visible = false; //Account Status
            gvUserOverview.Columns[16].Visible = false; //Date Created
            gvUserOverview.Columns[17].Visible = false; //Date Last logged On
            gvUserOverview.Columns[18].Visible = false; //Number of Elapsed Days
        }
        else {
            gvUserOverview.Columns[9].Visible = true;  // User ID            
            gvUserOverview.Columns[15].Visible = true; //Account Status
            gvUserOverview.Columns[16].Visible = true; //Date Created
            gvUserOverview.Columns[17].Visible = true; //Date Last logged On
            gvUserOverview.Columns[18].Visible = true; //Number of Elapsed Days
        }

        gvUserOverview.Columns[19].Visible = false;  // AssignedSite
        gvUserOverview.Columns[20].Visible = false;  // EnabledSite        
        gvUserOverview.Columns[21].Visible = false;  // JobTitle
        gvUserOverview.Columns[22].Visible = false;  // SchedulingContact
        gvUserOverview.Columns[23].Visible = false;  // DiscrepancyContact
        gvUserOverview.Columns[24].Visible = false;  // OTIFContact
        gvUserOverview.Columns[25].Visible = false;  // ScorecardContact
        gvUserOverview.Columns[26].Visible = false;  // InventoryPOC
        gvUserOverview.Columns[27].Visible = false;  // ProcurementPOC
        gvUserOverview.Columns[28].Visible = false;  // MerchandisingPOC
        gvUserOverview.Columns[29].Visible = false;  // ExecutivePOC
        gvUserOverview.Columns[30].Visible = false;  // Invoiceissues

        if (rdoCarrierOverview.Checked) {
            gvUserOverview.Columns[1].Visible = true;  // Carrier Name
            gvUserOverview.Width = Unit.Pixel(1300);
        }
        else if (rdoVendorOverview.Checked)   
        {
            gvUserOverview.Columns[3].Visible = true;  // VendorName
            gvUserOverview.Columns[4].Visible = true;  // VendorNo
            gvUserOverview.Columns[5].Visible = true;  // VendorEuropeanorLocal
            gvUserOverview.Columns[6].Visible = true;  // EUConsolidationCode
            gvUserOverview.Columns[7].Visible = true;  // LocalConsolidationCode           
            gvUserOverview.Columns[22].Visible = true;  // SchedulingContact
            gvUserOverview.Columns[23].Visible = true;  // DiscrepancyContact
            gvUserOverview.Columns[24].Visible = true;  // OTIFContact
            gvUserOverview.Columns[25].Visible = true;  // ScorecardContact
            gvUserOverview.Columns[30].Visible = true;  // Invoiceissues
            gvUserOverview.Width = Unit.Pixel(2300);
        }
        else if (rdoSchedulingOverview.Checked)                   
        {
            gvUserOverview.Columns[3].Visible = true;  // VendorName
            gvUserOverview.Columns[4].Visible = true;  // VendorNo
            gvUserOverview.Columns[5].Visible = true;  // VendorEuropeanorLocal
            gvUserOverview.Columns[6].Visible = true;  // EUConsolidationCode
            gvUserOverview.Columns[7].Visible = true;  // LocalConsolidationCode
            gvUserOverview.Columns[19].Visible = true;  // AssignedSite
            gvUserOverview.Columns[20].Visible = true;  // EnabledSite
            gvUserOverview.Width = Unit.Pixel(1900);
        }
        else if (rdoDiscrepancyOverview.Checked) {
            gvUserOverview.Columns[3].Visible = true;  // VendorName
            gvUserOverview.Columns[4].Visible = true;  // VendorNo
            gvUserOverview.Columns[5].Visible = true;  // VendorEuropeanorLocal
            gvUserOverview.Columns[6].Visible = true;  // EUConsolidationCode
            gvUserOverview.Columns[7].Visible = true;  // LocalConsolidationCode
            gvUserOverview.Columns[19].Visible = true;  // AssignedSite
            gvUserOverview.Width = Unit.Pixel(1600);
        }
        else if (rdoOfficeDepotUserOverview.Checked) {
            gvUserOverview.Columns[12].Visible = true; //Role Name
            gvUserOverview.Columns[13].Visible = true; //Role Template
            gvUserOverview.Columns[2].Visible = true;  // Default Site
            gvUserOverview.Columns[14].Visible = true; //OD Job Title
            gvUserOverview.Width = Unit.Pixel(1300);
        }
        else if (rdoVendorPOCOverview.Checked) {
            gvUserOverview.Columns[3].Visible = true;  // VendorName
            gvUserOverview.Columns[4].Visible = true;  // VendorNo
            gvUserOverview.Columns[5].Visible = true;  // VendorEuropeanorLocal
            gvUserOverview.Columns[6].Visible = true;  // EUConsolidationCode
            gvUserOverview.Columns[7].Visible = true;  // LocalConsolidationCode
            gvUserOverview.Columns[10].Visible = true;  // Email address POC  
            gvUserOverview.Columns[21].Visible = true;  // JobTitle
            gvUserOverview.Columns[26].Visible = true;  // InventoryPOC
            gvUserOverview.Columns[27].Visible = true;  // ProcurementPOC
            gvUserOverview.Columns[28].Visible = true;  // MerchandisingPOC
            gvUserOverview.Columns[29].Visible = true;  // ExecutivePOC                       
            gvUserOverview.Width = Unit.Pixel(1600);
        }

        if (GridID == "Export")
            return;

        //if (GridID == "Export") {
        //    //int RemoveAtIndex = -1;
        //    //int ColumnCount = gvUserOverview.Columns.Count;
        //    //for (int columnIndex = 0; columnIndex < ColumnCount; columnIndex++) {
        //    //    RemoveAtIndex ++;
        //    //    if (gvUserOverview.Columns[RemoveAtIndex].Visible == false) {
        //    //        gvUserOverview.Columns.RemoveAt(RemoveAtIndex);
        //    //        RemoveAtIndex--;
        //    //    }
        //    //}
        //    return;
        //}        

        SetGridColumns("Export");
      }

    protected void btnDeleteRejectedUser_Click(object sender, EventArgs e)
    {
        SCT_UserBE oSCT_UserBE = new SCT_UserBE();
        SCT_UserBAL oSCT_UserBAL = new SCT_UserBAL();
        oSCT_UserBE.oStockPlannerGroupings = new StockPlannerGroupingsBE();
        oSCT_UserBE.Action = "DeleteRejectedUser";
        int? iResult = oSCT_UserBAL.addEditUserBAL(oSCT_UserBE);
        oSCT_UserBAL = null;
        if (iResult == 0)
        {
            EncryptQueryString("SCT_UsersOverview.aspx");
        }
    }

    protected void rptCountry_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            if (lstUserSettings.FindAll(x => x.CountryId == ((MAS_CountryBE)e.Item.DataItem).CountryID).Count > 0)
            {
                CheckBox chkCountry = (CheckBox)e.Item.FindControl("chkCountry");
                chkCountry.Checked = true;
            }
        }
    }

    protected void rptAppointmentCountry_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Repeater rptAppointmentCountrySites = (Repeater)e.Item.FindControl("rptAppointmentCountrySites");
            List<SCT_UserBE> lstSites = lstUserSettings.FindAll(x => x.CountryId == ((SCT_UserBE)e.Item.DataItem).CountryId && x.SiteId != null).ToList();
            rptAppointmentCountrySites.DataSource = lstSites;
            rptAppointmentCountrySites.DataBind();
        }
    }

    protected void rptDiscrepanciesCountry_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Repeater rptDiscrepanciesCountrySites = (Repeater)e.Item.FindControl("rptDiscrepanciesCountrySites");
            List<SCT_UserBE> lstSites = lstUserSettings.FindAll(x => x.CountryId == ((SCT_UserBE)e.Item.DataItem).CountryId && x.SiteId != null).ToList();
            rptDiscrepanciesCountrySites.DataSource = lstSites;
            rptDiscrepanciesCountrySites.DataBind();
        }
    }

    protected void rptInvoiceIssueCountry_ItemDataBound(object sender, RepeaterItemEventArgs e) {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem) {
            Repeater rptInvoiceIssueCountrySites = (Repeater)e.Item.FindControl("rptInvoiceIssueCountrySites");
            List<SCT_UserBE> lstSites = lstUserSettings.FindAll(x => x.CountryId == ((SCT_UserBE)e.Item.DataItem).CountryId && x.SiteId != null).ToList();
            rptInvoiceIssueCountrySites.DataSource = lstSites;
            rptInvoiceIssueCountrySites.DataBind();
        }
    }

    protected void rptAppointmentCountrySites_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            CheckBox chkSite = (CheckBox)e.Item.FindControl("chkSite");
            if (((SCT_UserBE)e.Item.DataItem).SchedulingContact == 'Y')
            {
                chkSite.Checked = true;
            }
        }
    }

    protected void rptDiscrepanciesCountrySites_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            CheckBox chkSite = (CheckBox)e.Item.FindControl("chkSite");
            if (((SCT_UserBE)e.Item.DataItem).DiscrepancyContact == 'Y')
            {
                chkSite.Checked = true;
            }
        }
    }

    protected void rptInvoiceIssueCountrySites_ItemDataBound(object sender, RepeaterItemEventArgs e) {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem) {
            CheckBox chkSite = (CheckBox)e.Item.FindControl("chkSite");
            if (((SCT_UserBE)e.Item.DataItem).InvoiceIssueContact == 'Y') {
                chkSite.Checked = true;
            }
        }
    }

    protected void rptOtifCountry_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            CheckBox chkCountry = (CheckBox)e.Item.FindControl("chkCountry");
            if (((SCT_UserBE)e.Item.DataItem).OTIFContact == 'Y')
            {
                chkCountry.Checked = true;
            }
        }
    }

    protected void rptScorecardCountry_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            CheckBox chkCountry = (CheckBox)e.Item.FindControl("chkCountry");
            if (((SCT_UserBE)e.Item.DataItem).ScorecardContact == 'Y')
            {
                chkCountry.Checked = true;
            }
        }
    }

    protected void rptCarrierCountry_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Repeater rptCarrierCountrySites = (Repeater)e.Item.FindControl("rptCarrierCountrySites");
            List<SCT_UserBE> lstSites = lstUserSettings.FindAll(x => x.CountryId == ((SCT_UserBE)e.Item.DataItem).CountryId && x.SiteId != null).ToList();
            rptCarrierCountrySites.DataSource = lstSites;
            rptCarrierCountrySites.DataBind();
        }
    }

    protected void rptCarrierCountrySites_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            CheckBox chkSite = (CheckBox)e.Item.FindControl("chkSite");
            if (((SCT_UserBE)e.Item.DataItem).SchedulingContact == 'Y')
            {
                chkSite.Checked = true;
            }
        }
    }

    protected void gvUser_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            ucLinkButton lnkDetails = (ucLinkButton)e.Row.FindControl("lnkDetails");
            if (((SCT_UserBE)e.Row.DataItem).UserRoleID != 2 && ((SCT_UserBE)e.Row.DataItem).UserRoleID != 3)
            {
                lnkDetails.Visible = false;
            }

            HyperLink hlLoginID = (HyperLink)e.Row.FindControl("hlLoginID");
            if (((SCT_UserBE)e.Row.DataItem).LoginID == "--NA--"){
                hlLoginID.Visible = false;
            }
            if (ViewState["ReadOnly"] != null && Convert.ToBoolean(ViewState["ReadOnly"]) == true)
            {
                HyperLink hplink = (HyperLink)e.Row.FindControl("hpVendorLink");
                hplink.NavigateUrl = EncryptQuery("../GlobalSettings/VendorEdit.aspx?ReadOnly=1&PageFrom=UserList&VendorID=" + DataBinder.Eval(e.Row.DataItem, "Vendor.VendorID"));

              
                if (DataBinder.Eval(e.Row.DataItem, "UserRoleID").ToString() == "2" || DataBinder.Eval(e.Row.DataItem, "UserRoleID").ToString() == "3")
                {
                    if (DataBinder.Eval(e.Row.DataItem, "AccountStatus").ToString().ToLower() == "registered awaiting approval" || DataBinder.Eval(e.Row.DataItem, "AccountStatus").ToString().ToLower() == "edited awaiting approval")
                    {
                        hlLoginID.NavigateUrl = EncryptQuery("~/ModuleUI/Security/SCT_RegisterExternalUserEdit.aspx?ReadOnly=1&UserID=" + DataBinder.Eval(e.Row.DataItem, "UserID") + "&UStatus=RAA");
                    }
                    else
                    {
                        hlLoginID.NavigateUrl = EncryptQuery("~/ModuleUI/Security/SCT_RegisterExternalUserEdit.aspx?ReadOnly=1&UserID=" + DataBinder.Eval(e.Row.DataItem, "UserID"));
                    }
                }
                else
                {
                    hlLoginID.NavigateUrl = EncryptQuery("~/ModuleUI/Security/SCT_UserEdit.aspx?ReadOnly=1&UserID=" + DataBinder.Eval(e.Row.DataItem, "UserID"));
                }

            }

        }
    }

    protected void gvUser_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "VendorModulesDetails")
        {
            string[] splitVals = Convert.ToString(e.CommandArgument).Split(new char[] { ',' });
            int UserID = 0;
            int UserRoleID = 0;
            if (splitVals.Length > 0)
            {
                UserID = Convert.ToInt32(splitVals[0]);
            }
            if (splitVals.Length > 1)
            {
                UserRoleID = Convert.ToInt32(splitVals[1]);
            }

            bindOtherSettings(UserID, UserRoleID);
            mdlVendorModulesDetails.Show();
        }
    }

    protected void gvUser_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvUser.PageIndex = e.NewPageIndex;
        //BindUserGrid(e.NewPageIndex);

        SortGridView(GridViewSortExp, GridViewSortDirection);
    }

    #endregion

    #region Methods ...

    private void BindRole()
    {
        SCT_TemplateBAL oSCT_TemplateBAL = new SCT_TemplateBAL();
        SCT_UserRoleBE oSCT_UserRoleBE = new SCT_UserRoleBE();
        oSCT_UserRoleBE.Action = "GetAllUsers";

        List<SCT_UserRoleBE> lstRole = oSCT_TemplateBAL.GetUserRolesBAL(oSCT_UserRoleBE);
        oSCT_TemplateBAL = null;
        if (lstRole.Count > 0)
        {
            FillControls.FillDropDown(ref ddlRole, lstRole, "RoleName", "UserRoleID", "Select");
        }
    }

    private void BindUserGrid(int pageIndex = 0)
    {
        //SCT_UserBAL oSCT_UserBAL = new SCT_UserBAL();
        SCT_UserBE oSCT_UserBE = new SCT_UserBE();
        List<SCT_UserBE> lstUsers = new List<SCT_UserBE>();

        if (Session["UsersOverSearch"] != null && GetQueryStringValue("NewLoginRequest") == null)
        {
            oSCT_UserBE = (SCT_UserBE)Session["UsersOverSearch"];

            #region Setting controls of Searched Criteria ..
            /* Country .. */
            if (ddlCountry.innerControlddlCountry.Items.Count > 0 && oSCT_UserBE.CountryId != null)
            {
                ddlCountry.innerControlddlCountry.SelectedIndex =
                    ddlCountry.innerControlddlCountry.Items.IndexOf(ddlCountry.innerControlddlCountry.Items.FindByValue(
                    Convert.ToString(oSCT_UserBE.CountryId)));
            }

            /*user name*/
            if (!string.IsNullOrEmpty(oSCT_UserBE.UserSearchText))
                txtUserNameValue.Text = oSCT_UserBE.UserSearchText;
            //---------//

            /* Status .. */
            if (ddlStatus.Items.Count > 0 && !string.IsNullOrEmpty(oSCT_UserBE.AccountStatus))
            {
                ddlStatus.SelectedIndex = ddlStatus.Items.IndexOf(ddlStatus.Items.FindByValue(oSCT_UserBE.AccountStatus));
            }

            /* Vendor .. */
            HiddenField hiddenSelectedIDs = msVendor.FindControl("hiddenSelectedIDs") as HiddenField;
            if (hiddenSelectedIDs != null && !string.IsNullOrEmpty(oSCT_UserBE.VendorIDs))
                hiddenSelectedIDs.Value = oSCT_UserBE.VendorIDs;

            HiddenField hiddenSelectedName = msVendor.FindControl("hiddenSelectedName") as HiddenField;
            if (hiddenSelectedName != null && !string.IsNullOrEmpty(oSCT_UserBE.VendorName))
                hiddenSelectedName.Value = oSCT_UserBE.VendorName;            

            ucListBox lstRight = msVendor.FindControl("lstRight") as ucListBox;
            ucListBox lstLeft = msVendor.FindControl("ucVendor").FindControl("lstLeft") as ucListBox;
            if (lstLeft != null && lstRight != null && !string.IsNullOrEmpty(oSCT_UserBE.SearchVendorText) && !string.IsNullOrEmpty(oSCT_UserBE.VendorIDs))
            {
                lstLeft.Items.Clear();
                lstRight.Items.Clear();
                msVendor.SearchVendorClick(oSCT_UserBE.SearchVendorText);

                string[] strVendorIDs = oSCT_UserBE.VendorIDs.Split(',');
                for (int index = 0; index < strVendorIDs.Length; index++)
                {
                    ListItem listItem = lstLeft.Items.FindByValue(strVendorIDs[index]);
                    if (listItem != null)
                    {
                        lstRight.Items.Add(listItem);
                        lstLeft.Items.Remove(listItem);
                    }
                }
            }


            /* Carrier .. */
            HiddenField hiddenSelectedCarrierIDs = msCarrier.FindControl("hiddenSelectedIDs") as HiddenField;
            if (hiddenSelectedCarrierIDs != null && !string.IsNullOrEmpty(oSCT_UserBE.CarrierIDs))
                hiddenSelectedCarrierIDs.Value = oSCT_UserBE.CarrierIDs;

            HiddenField hiddenSelectedCarrierName = msCarrier.FindControl("hiddenSelectedName") as HiddenField;
            if (hiddenSelectedCarrierName != null && !string.IsNullOrEmpty(oSCT_UserBE.CarrierName))
                hiddenSelectedCarrierName.Value = oSCT_UserBE.CarrierName;

            ucListBox lstCarrierRight = msCarrier.FindControl("lstRight") as ucListBox;
            ucListBox lstCarrierLeft = msCarrier.FindControl("ucCarrier").FindControl("lstLeft") as ucListBox;
            if (lstCarrierLeft != null && lstCarrierRight != null && !string.IsNullOrEmpty(oSCT_UserBE.SearchCarrierText) 
                && !string.IsNullOrEmpty(oSCT_UserBE.CarrierIDs))
            {
                lstCarrierLeft.Items.Clear();
                lstCarrierRight.Items.Clear();
                msCarrier.SearchCarrierClick(oSCT_UserBE.SearchCarrierText);

                string[] strCarrierIDs = oSCT_UserBE.CarrierIDs.Split(',');
                for (int index = 0; index < strCarrierIDs.Length; index++)
                {
                    ListItem listItem = lstLeft.Items.FindByValue(strCarrierIDs[index]);
                    if (listItem != null)
                    {
                        lstCarrierRight.Items.Add(listItem);
                        lstCarrierLeft.Items.Remove(listItem);
                    }
                }
            }

            /* Role .. */
            if (ddlRole.Items.Count > 0 && oSCT_UserBE.UserRoleID != null)
            {
                ddlRole.SelectedIndex = ddlRole.Items.IndexOf(ddlRole.Items.FindByValue(Convert.ToString(oSCT_UserBE.UserRoleID)));
            }

            //REPORT TYPE//
            if (oSCT_UserBE.ReportType == "CarrierOverview")
                rdoCarrierOverview.Checked = true;
            else if (oSCT_UserBE.ReportType == "VendorOverview")
                rdoVendorOverview.Checked = true;
            else if (oSCT_UserBE.ReportType == "SchedulingOverview")
                rdoSchedulingOverview.Checked = true;
            else if (oSCT_UserBE.ReportType == "DiscrepancyOverview")
                rdoDiscrepancyOverview.Checked = true;
            else if (oSCT_UserBE.ReportType == "OfficeDepotUserOverview")
                rdoOfficeDepotUserOverview.Checked = true;
            else if (oSCT_UserBE.ReportType == "VendorPOCOverview")
                rdoVendorPOCOverview.Checked = true;
            //-----------//

            #endregion
        }
        else
        {
            oSCT_UserBE.Action = "GetUserOverviewNew";
            if (GetQueryStringValue("NewLoginRequest") != null && GetQueryStringValue("NewLoginRequest").ToString() == "Y") {
                oSCT_UserBE.AccountStatus = "Registered Awaiting Approval";
                oSCT_UserBE.ReportType = "OfficeDepotUserOverview";
                oSCT_UserBE.CountryId = Convert.ToInt32(ddlCountry.innerControlddlCountry.SelectedItem.Value);              
            }

            oSCT_UserBE.PageSize = PageSize;

            if (pageIndex > 0)
                oSCT_UserBE.PageIndex = pageIndex;
            else
                oSCT_UserBE.PageIndex = DefaultPageIndex;

        }       

        lstUsers = GetUsers(oSCT_UserBE);

        ViewState["lstUsers"] = lstUsers;

        //gvUser.DataSource = lstUsers;
        //gvUser.DataBind();
        SortGridView(GridViewSortExp, GridViewSortDirection);

        //-- Add By Abhinav
        gvUserExcelExport.DataSource = lstUsers;
        gvUserExcelExport.DataBind();
        //--------------

       
        //Session["UsersOverSearch"] = null;
    }

    //Add By Abhinav -- 18 July 2013 -- To get users from existing Viewstste
    private List<SCT_UserBE> GetUsers(SCT_UserBE oSCT_UserBE) {
        SCT_UserBAL oSCT_UserBAL = new SCT_UserBAL();        
        List<SCT_UserBE> lstUsers = new List<SCT_UserBE>();

        if (ViewState["lstUsers"] == null) {
            lstUsers = oSCT_UserBAL.GetUsersBAL(oSCT_UserBE);
            oSCT_UserBAL = null;
        }
        else {
            lstUsers = (List<SCT_UserBE>)ViewState["lstUsers"];
        }
        return lstUsers;
    }
    //----------------------------//

    public override BusinessEntities.BaseBe[] SubSort(GridViewSortEventArgs e)
    {
        GridViewSortExp = e.SortExpression;
        if (SortDir == "ASC") {
            GridViewSortDirection = SortDirection.Ascending;
        }
        else {
            GridViewSortDirection = SortDirection.Descending;
        }
        return Utilities.GenericListHelper<SCT_UserBE>.SortList((List<SCT_UserBE>)ViewState["lstUsers"], e.SortExpression, GridViewSortDirection).ToArray();
    }

    private void bindOtherSettings(int UserID, int UserRoleID)
    {
        SCT_UserBE oSCT_UserBE = new SCT_UserBE();
        oSCT_UserBE.Action = "GetCountrySpecificSettings";
        oSCT_UserBE.UserID = UserID;
        SCT_UserBAL oSCT_UserBAL = new SCT_UserBAL();
        lstUserSettings = oSCT_UserBAL.GetCountrySpecificSettingsBAL(oSCT_UserBE);

        foreach (SCT_UserBE item in lstUserSettings)
        {
            if (lstCountrySettings.FindAll(x => x.CountryId == item.CountryId && x.VendorID == item.VendorID).Count <= 0)
            {
                lstCountrySettings.Add(item);
            }
        }

        bindCountry();

        if (UserRoleID == 2) //Vendor
        {
            pnlVendorSction.Visible = true;
            pnlCarrierSction.Visible = false;

            rptVendorDetails.DataSource = lstCountrySettings;
            rptVendorDetails.DataBind();

            rptAppointmentCountry.DataSource = lstCountrySettings;
            rptAppointmentCountry.DataBind();

            rptDiscrepanciesCountry.DataSource = lstCountrySettings;
            rptDiscrepanciesCountry.DataBind();

            rptInvoiceIssueCountry.DataSource = lstCountrySettings;
            rptInvoiceIssueCountry.DataBind();

            rptOtifCountry.DataSource = lstCountrySettings;
            rptOtifCountry.DataBind();

            rptScorecardCountry.DataSource = lstCountrySettings;
            rptScorecardCountry.DataBind();
        }
        else if (UserRoleID == 3) //Vendor
        {
            pnlVendorSction.Visible = false;
            pnlCarrierSction.Visible = true;

            rptCarrierDetails.DataSource = lstCountrySettings;
            rptCarrierDetails.DataBind();

            rptCarrierCountry.DataSource = lstCountrySettings;
            rptCarrierCountry.DataBind();
        }
    }

    private void bindCountry()
    {
        MAS_CountryBE oMAS_CountryBE = new MAS_CountryBE();
        MAS_CountryBAL oMAS_CountryBAL = new MAS_CountryBAL();

        oMAS_CountryBE.Action = "ShowAllCountry";
        oMAS_CountryBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
        oMAS_CountryBE.User.UserID = 0;

        List<MAS_CountryBE> lstCountry = new List<MAS_CountryBE>();
        lstCountry = oMAS_CountryBAL.GetCountryBAL(oMAS_CountryBE);

        rptCountry.DataSource = lstCountry;
        rptCountry.DataBind();
    }
    public void GetReadOnlyModule()
    {
        SCT_ModuleBAL oSCT_ModuleBAL = new SCT_ModuleBAL();
        SCT_ModuleBE oSCT_ModuleBE = new SCT_ModuleBE();

        oSCT_ModuleBE.Action = "GetReadOnlyModule";
        oSCT_ModuleBE.ScreenName = "UserOverview";
        DataTable dt = oSCT_ModuleBAL.GetReadOnlyModuleBAL(oSCT_ModuleBE);
        if (dt.Rows.Count > 0)
        {
            ViewState["ReadOnly"] = null;
        }
        else
        {
            ViewState["ReadOnly"] = true;
        }
    }

    #endregion
}
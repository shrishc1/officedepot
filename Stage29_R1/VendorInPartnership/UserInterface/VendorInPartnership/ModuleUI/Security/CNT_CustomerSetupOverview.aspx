﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    CodeFile="CNT_CustomerSetupOverview.aspx.cs" Inherits="ModuleUI_Security_CNT_CustomerSetupOverview" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="../../CommonUI/UserControls/ucAddButton.ascx" TagName="ucAddButton"
    TagPrefix="cc2" %>
<%@ Register Src="../../CommonUI/UserControls/ucExportToExcel.ascx" TagName="ucExportToExcel"
    TagPrefix="cc2" %>
<%@ Register Src="../../CommonUI/UserControls/ucCountry.ascx" TagName="ucCountry"
    TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2>
        <cc1:ucLabel ID="lblCustomerSetup" runat="server" Text="Customer Setup"></cc1:ucLabel>
    </h2>
    <table class="top-settings" width="30%">
        <tr>
            <td style="font-weight: bold;" align="center">
                <cc1:ucLabel ID="lblCountry" runat="server" Text="Country"></cc1:ucLabel>&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;&nbsp;
                <uc1:ucCountry ID="ucCountry" runat="server" />
            </td>
            
        </tr>
    </table>
    <div class="button-row">
        <cc2:ucAddButton ID="btnAdd" runat="server" NavigateUrl="CNT_CustomerSetupEdit.aspx" />
        <cc2:ucExportToExcel ID="ucExportToExcel1" runat="server" />
    </div>
    <table width="100%">
        <tr>
            <td align="center">
                <cc1:ucGridView ID="UcGridView1" Width="100%" runat="server" CssClass="grid" onsorting="SortGrid" AllowSorting="true">
                    <Columns>
                        <asp:TemplateField HeaderText="Country" SortExpression="CountryName">
                            <HeaderStyle HorizontalAlign="Left" Width="10%" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <cc1:ucLiteral ID="ltCountryName" runat="server" Text='<%# Eval("CountryName") %>'></cc1:ucLiteral>
                            </ItemTemplate>
                        </asp:TemplateField>

                         <asp:TemplateField HeaderText="Customer Name" SortExpression="CustomerName">
                            <HeaderStyle Width="10%" HorizontalAlign="Left" />
                            <ItemTemplate>
                                <asp:HyperLink ID="hpCarrier" runat="server" Text='<%# Eval("CustomerName") %>'  NavigateUrl='<%# EncryptQuery("CNT_CustomerSetupEdit.aspx?CustomerID="+ Eval("CustomerID")) %>'></asp:HyperLink>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:TemplateField>       
                        
                       <asp:TemplateField HeaderText="Updated By" SortExpression="UpdatedByUser">
                            <HeaderStyle HorizontalAlign="Left" Width="10%" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <cc1:ucLiteral ID="ltUpdatedByUser" runat="server" Text='<%# Eval("UpdatedByUser") %>'></cc1:ucLiteral>
                            </ItemTemplate>
                        </asp:TemplateField>
                       
                        <asp:TemplateField HeaderText="Updated On" SortExpression="UpdateDate">
                            <HeaderStyle HorizontalAlign="Left" Width="10%" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <cc1:ucLiteral ID="ltUpdateDate" runat="server" Text='<%# Eval("UpdateDate") %>'></cc1:ucLiteral>
                            </ItemTemplate>
                        </asp:TemplateField>

                    </Columns>
                </cc1:ucGridView>
            </td>
        </tr>
    </table>
</asp:Content>


﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Login.aspx.cs" Inherits="Login" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Office Depot</title>
    <link href="../../Css/style.css" rel="stylesheet" type="text/css" />

     <%--<script language="JavaScript">        
         //window.location = "https://172.29.9.4:4431/ModuleUI/Security/Login.aspx";
    </script>--%>

    <script type="text/javascript" language="javascript">
        var validEmailMessage = '<%= ValidEmail %>';
        var emailRegExp = '^[A-Za-z0-9](([_\.\-\']?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([\.\-]?[a-zA-Z0-9]+)*)\.([A-Za-z]{2,})$';

        function LoginClick() {
            document.getElementById('btnLogin').click();
        }

        function IsEmailAddress(emailVal) {            
            var varEmail = document.getElementById('txtUserName');
            if (varEmail != null && varEmail.value != '') {
                if (!IsFieldValid(varEmail.value, emailRegExp)) {
                    alert(validEmailMessage);
                    return false;
                }
                else {
                    return true;
                }
            }
            else {
                alert(validEmailMessage);
                return false;
            }
        }

        function IsFieldValid(value, validationExpression) {
            var exp = new RegExp(validationExpression);
            if (value == "" || !exp.test(value))
                return false;
            else
                return true;
        }
    </script>
        
    <style type="text/css">
        .fixed-btn {
            position: fixed;
            top: 37px;
            right: 30px;
            height: 25px;
            width: 50px;
            z-index: 999;           
            font-size: 20px;
            box-shadow: 10px 5px 8px #888888;
            color: red;
            font-weight: bold;
            border: 1px solid red;
            border-radius: 5px;
            padding: 6px 10px 1px 13px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">

    
    <div id="container-login">
        <!-- start header section -->
        <div id="header">
            <!-- start top menu -->
            <!-- <div class="top-link"></div> -->
            <!-- end top menu -->
            <div class="login-banner">
            </div>
        </div>
        <!-- end header section -->
        <!-- start login section -->

        

        <noscript><div style="color:Red;font-size:11px;font-weight:bold;"><center>Java Script is disabled on your browser, so some of the functionality may not work as expected. <br />
Please enable Java Script or upgrade to a Java Script-capable browser to use Vendors In Partnership portal.</center></div></noscript>

        <asp:Panel ID="pnlLogin" runat="server" DefaultButton="lnkSubmit" style="display:none;">
            <div id="login">
                <div class="login-box">
                    <ul>
                        <li>
                            <div class="error" style="display: none" runat="server" id="divError">
                                <cc1:ucLabel ID="lblLoginError" runat="server"></cc1:ucLabel>
                            </div>
                            <div class="error" style="display: none" runat="server" id="divBlocked">
                                <cc1:ucLabel ID="lblBlockedMsg" runat="server"></cc1:ucLabel>
                            </div>
                        </li>
                        <li>
                            <label for="name">
                                <cc1:ucLabel ID="lblUserName" runat="server" Text="User Name" style="font-size:13px"></cc1:ucLabel>
                                (<cc1:ucLabel ID="lblYourEmailAddress" runat="server" Text="your email address"></cc1:ucLabel>)
                                </label>
                            <div class="inputbox">
                                <input name="txtUserName" id="txtUserName" type="text" runat="server" />
                                <asp:RequiredFieldValidator ID="rfvUserNameRequired" runat="server" ErrorMessage="Please enter User Name."
                                    Display="None" ValidationGroup="Submit" ControlToValidate="txtUserName"></asp:RequiredFieldValidator>                                
                            </div>
                        </li>
                        <li>
                            <label for="pass">
                                <cc1:ucLabel ID="lblPassword" runat="server" Text="Password" style="font-size:13px"></cc1:ucLabel>
                                </label>
                            <div class="inputbox">
                                <input name="txtPassword" id="txtPassword" clientidmode="Static" type="password"
                                    runat="server" />
                                <asp:RequiredFieldValidator ID="rfvPasswordRequired" runat="server" ErrorMessage="Please enter Password."
                                    Display="None" ValidationGroup="Submit" ControlToValidate="txtPassword"></asp:RequiredFieldValidator>
                            </div>
                        </li>
                        <li class="button-row"><span class="submit">
                            <cc1:ucLinkButton ID="lnkSubmit" runat="server" Text="Submit" OnClick="btnLogin_Click" ValidationGroup="Submit"></cc1:ucLinkButton></span>
                              <asp:ValidationSummary ID="vs1" runat="server" ValidationGroup="Submit" ShowSummary="false"
                            ShowMessageBox="true" />
                        </li>
                        <li class="link">
                            <cc1:ucLinkButton runat="server" ID="lnkForgotPassword" OnClick="lnkForgotPassword_Click"
                                Text="Forgot Password" OnClientClick="return IsEmailAddress();" />
                        </li>
                        <li class="link"><a href="SCT_RegisterExternalUser.aspx" style="color: White">
                            <cc1:ucLabel ID="lblRegisterUser" runat="server" Text="Register As New User?"></cc1:ucLabel>
                        </a></li>
                    </ul>
                </div>
            </div>
        </asp:Panel>


       
        <!-- end login section -->
        <!-- start footer section -->
        <div id="footer">
            
            Copyright &copy; 2011 by Office Depot, Inc. All rights reserved.
        </div>
        <!-- end footer section -->
    </div>

     <asp:Panel ID="divFixed" CssClass="fixed-btn" runat="server" >
                <label class="btn btn--white-bg" href="#">DEV</label>
     </asp:Panel> 

    </form>
</body>
</html>

<script type="text/javascript" language="javascript">
    var pnlLogin = document.getElementById("<%=pnlLogin.ClientID%>");
    if (pnlLogin) {
        pnlLogin.style.display = "";
    }       
    //document.getElementById("divFixed").style.display = "block";
</script>
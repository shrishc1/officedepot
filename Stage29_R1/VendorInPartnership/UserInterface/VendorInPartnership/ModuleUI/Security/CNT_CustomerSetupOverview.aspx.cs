﻿using BusinessEntities.ModuleBE.Security;
using BusinessLogicLayer.ModuleBAL.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ModuleUI_Security_CNT_CustomerSetupOverview : CommonPage
{
    protected void Page_Prerender(object sender, EventArgs e)
    {

    }
    protected void Page_Init(object sender, EventArgs e)
    {
        ucCountry.CurrentPage = this;
        ucCountry.IsAllRequired = true;
        ucCountry.IsAllDefault = true;
        ucCountry.innerControlddlCountry.AutoPostBack = true;
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindRegionCustomer();
        }
        ucExportToExcel1.GridViewControl = UcGridView1;
        ucExportToExcel1.FileName = "CustomerOverview";
    }

    #region Methods

    protected void BindRegionCustomer()
    {
        MASCNT_CustomerSetupBE oMASCNT_CustomerSetupBE = new MASCNT_CustomerSetupBE();
        MASCNT_CustomerSetupBAL oMASCNT_CustomerSetupBAL = new MASCNT_CustomerSetupBAL();

        oMASCNT_CustomerSetupBE.Action = "ShowAll";
        if (ucCountry.innerControlddlCountry.SelectedItem != null)
        oMASCNT_CustomerSetupBE.CountryID = Convert.ToInt32(ucCountry.innerControlddlCountry.SelectedItem.Value);

        List<MASCNT_CustomerSetupBE> mASCNT_CustomerSetupBEs = oMASCNT_CustomerSetupBAL.GetCustomersBAL(oMASCNT_CustomerSetupBE);

        if (mASCNT_CustomerSetupBEs.Count > 0)
        {
            UcGridView1.DataSource = mASCNT_CustomerSetupBEs;
            UcGridView1.DataBind();
            ViewState["lstCustomer"] = mASCNT_CustomerSetupBEs;
        }
    }

    public override BusinessEntities.BaseBe[] SubSort(GridViewSortEventArgs e)
    {
        return Utilities.GenericListHelper<MASCNT_CustomerSetupBE>.SortList((List<MASCNT_CustomerSetupBE>)ViewState["lstCustomer"], e.SortExpression, e.SortDirection).ToArray();
    }

    #endregion    
    #region Events
    public override void CountrySelectedIndexChanged()
    {
        BindRegionCustomer();
    }
    public override void CountryPost_Load()
    {        
                  
    }
    #endregion
}
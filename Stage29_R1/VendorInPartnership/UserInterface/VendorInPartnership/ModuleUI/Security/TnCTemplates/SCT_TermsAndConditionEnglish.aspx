﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SCT_TermsAndConditionEnglish.aspx.cs"
    Inherits="ModuleUI_Security_SCT_TermsAndConditionEnglish" %>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Office Depot</title>
    <link rel="stylesheet" type="text/css" href="../../../Css/style.css" />
    <link type="text/css" href="../../../Css/ui-lightness/jquery-ui-1.8.5.custom.css"
        rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="../../../Css/superfish.css" media="screen" />
    
 </head>
<body>
    <form id="form1" runat="server">
    <div>
        <table width="100%"  cellspacing="5" cellpadding="0" class="form-table">
            <tr>
                <td>
                 <cc1:ucLabel ID="lblTermsConditionsText1" runat="server" ></cc1:ucLabel>
                  <%--Agreement between Office Depot and the party specified on the registration form.    --%>               
                </td>
            </tr>
            <tr>
                <td>
                 <cc1:ucLabel ID="lblTermsConditionsText2" runat="server" ></cc1:ucLabel>
                 <%--   In consideration of Office Depot giving us a password to enable us to have access
                    to certain confidential delivery information accessible and obtainable via Office
                    Depot's web site on the Internet including but not limited to booking and Purchase
                    Order data, USER hereby agrees as follows:--%>
                </td>
            </tr>
            <tr>
                <td>
                 <cc1:ucLabel ID="lblTermsConditionsText3" runat="server" ></cc1:ucLabel>
                   <%-- 1. USER shall keep their Password and all Information with respect to bookings and
                    any information garnered from the VIP portal with Office Depot confidential, and
                    not reveal them to any third party.We shall further ensure that such directors, officers, 
                    and employees keep your Password and all such Information confidential and not reveal them to any third party.--%>
                </td>
            </tr>
            <tr>
                <td>
                 <cc1:ucLabel ID="lblTermsConditionsText4" runat="server" ></cc1:ucLabel>
                   <%-- 2. Should your Password and/or any Information with respect to Office Depot&#39;s
                    VIP portal be revealed except as permitted by the preceding paragraph, USER shall
                    promptly notify Office Depot of such an occurrence and USER shall defend, indemnify
                    and hold Office Depot harmless from and against any complaint, claim, liability,
                    loss, damage and expense, including legal fees, arising out of, or in connection
                    with, such revelation, or any subsequent misuse, alteration, misappropriation, or
                    fraudulent use of such Password and/or Information.--%>
                </td>
            </tr>
            <tr>
                <td>
                 <cc1:ucLabel ID="lblTermsConditionsText5" runat="server" ></cc1:ucLabel>
                   <%-- 3. USER shall not make any attempt to ascertain or obtain a Password assigned to
                    any third party by Office Depot or to make access to Information by using any Password other than
                    your Password.--%>
                </td>
            </tr>
            <tr>
                <td>
                 <cc1:ucLabel ID="lblTermsConditionsText6" runat="server" ></cc1:ucLabel>
                   <%-- 4. USER confirms that Office Depot shall be free of all liability from revealing
                    your Password, or any Information gained through the use of your Password,
                    by any third party, unless such disclosure was caused by Office Depot&#39;s negligence.--%>
                </td>
            </tr>
            <tr>
                <td>
                 <cc1:ucLabel ID="lblTermsConditionsText7" runat="server" ></cc1:ucLabel>
                   <%-- 5. USER understands that Office Depot DOES NOT WARRANT OR GUARANTEE that: a. The
                    data on its web site is accurate; b. Access to the web site will be uninterrupted;
                    c. The web site will not be changed in the future to provide less data, more data
                    or different data; or d. Office Depot will always maintain a web site.--%>
                </td>
            </tr>
            <tr>
                <td>
                 <cc1:ucLabel ID="lblTermsConditionsText8" runat="server" ></cc1:ucLabel>  </td>
                   <%-- 6. USER understands that Office Depot shall have no liability whatsoever, including
                    (without limitation) liability for loss of profit, indirect, special or consequential
                    losses or damages, arising from our use of its web site.--%>
                
            </tr>
            <tr>
                <td>
                 <cc1:ucLabel ID="lblTermsConditionsText9" runat="server"></cc1:ucLabel>  </td>
                   <%-- 7. Office Depot will use information provided by the USER for identification and
                    contact related to its service and applications in order to establish service for
                    the USER. In carrying out its services, Office Depot may provide information to
                    its agents, group companies, subcontractors or independent contractors to the extent
                    that such information is required for maintenance or service improvements. Office
                    Depot will do its best to supervise the security management of the aforementioned
                    parties in order to safeguard such information.--%>
              
            </tr>
           
        </table>
    </div>
    </form>
</body>
</html>

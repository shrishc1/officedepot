﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SCT_TermsAndConditionItaly.aspx.cs" 
Inherits="ModuleUI_Security_TnCTemplates_SCT_TermsAndConditionItaly" %>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Office Depot</title>
    <link rel="stylesheet" type="text/css" href="../../../Css/style.css" />
    <link type="text/css" href="../../../Css/ui-lightness/jquery-ui-1.8.5.custom.css"
        rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="../../../Css/superfish.css" media="screen" />
    
 </head>
<body>
    <form id="form1" runat="server">
    <div>
        <table width="100%"  cellspacing="5" cellpadding="0" class="form-table">
            <tr>
                <td>
                    Accordo tra Office Depot e la parte specificata nel modulo di registrazione. 
                </td>
            </tr>
            <tr>
                <td>
                   Tenuto conto che Office Depot ha messo a Sua disposizione una password di accesso a determinate informazioni
                    confidenziali di consegna, accessibili ed ottenibili dal sito di Office Depot su internet e contenenti, tra gli altri, dati su 
                    prenotazioni e ordini di acquisto, con la presente L'UTENTE acconsente ai seguenti termini: 
                </td>
            </tr>
            <tr>
                <td>
                    1. L'UTENTE deve mantenere la riservatezza con Office Depot in merito alla Sua Password e tutte le informazioni riguardanti 
                    le prenotazioni ed altre informazioni acquisite dal portale VIP, non rivelandole a terzi. Si richiede inoltre che direttori, 
                    responsabili e dipendenti mantengano la riservatezza sulla Password e su tali informazioni, non rivelandole a terzi. 
                </td>
            </tr>
            <tr>
                <td>
                    2. Nel caso in cui la Sua Password e/o qualsiasi Informazione concernente il portale VIP di Office Depot venga rivelata al di 
                    fuori delle condizioni sopraindicate, l'UTENTE deve immediatamente avvisare Office Depot di tale avvenimento e l'UTENTE 
                    deve difendere, indennizzare e proteggere Office Depot da qualsiasi lamentela, reclamo, ostacolo, perdita, danno o spesa, 
                    incluse le spese legali, che risultano emergere in merito a, o con riferimento a tale rivelazione, o da qualsiasi uso 
                    inappropriato, alterazione, appropriazione o uso fraudolento di tale Password e/o Informazione. 
                </td>
            </tr>
            <tr>
                <td>
                    3. L'UTENTE non deve tentare di entrare in possesso della Password assegnata a terzi da Office Depot o di accedere alle 
                        Informazioni utilizzando una Password diversa da quella di cui si è in possesso.
                </td>
            </tr>
            <tr>
                <td>
                    4.  L'UTENTE conferma che Office Depot sarà libero da ogni responsabilità per la rivelazione della Sua Password, o di 
                    qualsiasi Informazione acquisita attraverso l'uso della Sua Password da parte di terzi, eccetto nei casi in cui tali 
                    rivelazioni non sono causate da una negligenza di Office Depot.  
                </td>
            </tr>
            <tr>
                <td>
                    5.  L'UTENTE riconosce che Office Depot NON ASSICURA O GARANTISCE che: a. I dati sul suo sito web siano accurati; 
                    b. L'accesso al sito web sarà ininterrotto; c. Il sito web non verrà cambiato in futuro per fornire più dati, 
                    meno dati o dati differenti; o d. Office Depot manterrà sempre un sito web. 
                </td>
            </tr>
            <tr>
                <td>
                    6. L'UTENTE riconosce che Office Depot sarà libero da ogni tipo di responsabilità, inclusa la responsabilità (illimitata) per la 
                    perdita di utili, perdite o danni indiretti, speciali o significativi, derivati dal Suo utilizzo del sito web. 
                </td>
            </tr>
            <tr>
                <td>
                    7. Office Depot utilizzerà le informazioni fornite dall'UTENTE per identificarlo e contattarlo in merito ai suoi servizi ed 
                    applicazioni allo scopo di attivare il servizio per l'UTENTE. Nel prestare i suoi servizi, Office Depot può fornire informazioni ai 
                    suoi agenti, aziende consociate, subappaltatori, o appaltatori indipendenti solamente nei casi in cui queste informazioni sono
                    richieste per manutenzione o miglioramenti del servizio. Office Depot farà tutto il possibile per sorvegliare la gestione della 
                    sicurezza nei soggetti sopraindicati per salvaguardare tali informazioni. 
                </td>
            </tr>
           
        </table>
    </div>
    </form>
</body>
</html>





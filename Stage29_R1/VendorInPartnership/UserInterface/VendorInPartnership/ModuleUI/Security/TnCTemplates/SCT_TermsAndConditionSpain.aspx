﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SCT_TermsAndConditionSpain.aspx.cs" 
Inherits="ModuleUI_Security_TnCTemplates_SCT_TermsAndConditionSpain" %>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Office Depot</title>
    <link rel="stylesheet" type="text/css" href="../../../Css/style.css" />
    <link type="text/css" href="../../../Css/ui-lightness/jquery-ui-1.8.5.custom.css"
        rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="../../../Css/superfish.css" media="screen" />
    
 </head>
<body>
    <form id="form1" runat="server">
    <div>
        <table width="100%"  cellspacing="5" cellpadding="0" class="form-table">
            <tr>
                <td>
                   Acuerdo entre Office Depot y la parte especificada en el formulario de registración.
                </td>
            </tr>
            <tr>
                <td>
                    Dado que Office Depot nos brinda una contraseña para permitirnos el acceso a cierta información confidencial de entrega, 
                    accesible y obtenible mediante el sitio web de Office Depot en Internet, que incluye, pero no está limitado, a la información de 
                    las reservas y órdenes de compra, el USUARIO aquí presente acepta lo siguiente:
                </td>
            </tr>
            <tr>
                <td>
                    1. El USUARIO deberá mantener su Contraseña y toda la Información relacionada con las reservas y cualquier información 
                    obtenida del portal VIP confidencial para con Office Depot, y que no la revelará a terceros. Asimismo nosotros aseguramos 
                    que los directores, oficiales y empleados mantendrán su Contraseña y dicha Información confidencial y que no la revelarán a 
                    terceros.
                </td>
            </tr>
            <tr>
                <td>
                    2. En caso de que su Contraseña y/o cualquier Información relacionada con el portal VIP de Office Depot sea revelada, 
                    excepto según lo permitido por el párrafo precedente, el USUARIO notificará de manera apropiada a Office Depot de tal 
                    acontecimiento, y el USUARIO defenderá, indemnizará y mantendrá a Office Depot libre de toda culpa por todo reclamo, 
                    queja, responsabilidad, pérdida, daños y costos, incluyendo honorarios legales que pudieran surgir en relación con tal
                    revelación, o consecuente mal uso, alteración, usurpación o uso fraudulento de la Contraseña y/o Información.
                </td>
            </tr>
            <tr>
                <td>
                    3.  El USUARIO no intentará averiguar u obtener la Contraseña asignada por Office Depot a un tercero, ni acceder a la 
                    Información utilizando una Contraseña diferente a su propia Contraseña.
                </td>
            </tr>
            <tr>
                <td>
                    4. El USUARIO confirma que Office Depot estará libre de toda responsabilidad por revelar su Contraseña, o cualquier 
                    Información obtenida mediante el uso de su Contraseña por parte de un tercero, a menos que tal divulgación haya sido 
                    provocada por la negligencia de Office Depot.
                </td>
            </tr>
            <tr>
                <td>
                    5. El USUARIO comprende que Office Depot NO GARANTIZA que: a. La información de su sitio web sea precisa; 
                    b. El acceso al sitio web sea ininterrumpido; c. El sitio web no sea cambiado en el futuro para proveer menos 
                    datos, más datos o datos diferentes; o d. Office Depot siempre mantendrá un sitio web.
                </td>
            </tr>
            <tr>
                <td>
                    6. El USUARIO comprende que Office Depot no tendrá responsabilidad por nada, incluyendo, sin límites, 
                    responsabilidad por pérdidas de ganancias, pérdidas o daños indirectos, especiales o consecuentes, que 
                    surgieran del uso de su sitio web.
                </td>
            </tr>
            <tr>
                <td>
                    7. Office Depot utilizará la información provista por el USUARIO para identificación y contacto, en relación con su servicio y
                     las aplicaciones, para establecer el servicio para el USUARIO. Al llevar a cabo sus servicios, Office Depot puede proveer 
                     información a sus agentes, compañías grupales, subcontratistas o contratistas independientes, siempre y cuando tal 
                     información sea requerida para mantenimiento o mejoras en el servicio. Office Depot hará lo mejor para supervisar la administración 
                     de la seguridad de las partes arriba mencionadas, y así salvaguardar dicha información.
                </td>
            </tr>
           
        </table>
    </div>
    </form>
</body>
</html>




﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SCT_TermsAndConditionGerman.aspx.cs" Inherits="ModuleUI_Security_TnCTemplates_SCT_TermsAndConditionGerman" %>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Office Depot</title>
    <link rel="stylesheet" type="text/css" href="../../../Css/style.css" />
    <link type="text/css" href="../../../Css/ui-lightness/jquery-ui-1.8.5.custom.css"
        rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="../../../Css/superfish.css" media="screen" />
    
 </head>
<body>
    <form id="form1" runat="server">
    <div>
        <table width="100%"  cellspacing="5" cellpadding="0" class="form-table">
            <tr>
                <td>
                    Vereinbarung zwischen Office Depot und der Partei, die in der Registrierierung angegeben wurde.
                </td>
            </tr>
            <tr>
                <td>
                   In Anbetracht dessen, dass Ofice Depot uns ein Passwort gibt um gewisse vertrauliche Lieferungsinformationen abrufen 
                   zu können, erhältlich durch Office Depots Webseite im Internet, welche Buchungen und Bestellungsdaten beinhalten. 
                   Der BENUTZER akzeptiert wie folgt:
                </td>
            </tr>
            <tr>
                <td>
                    1.  BENUTZER müssen ihr Passwort und alle Informationen bezüglich Buchungen und jegliche anderen Informationen, 
                    die aus dem VIP Portal von Office Depot erhalten wurden, streng vertraulich behandeln und sie keinesfalls an 
                    diverse Drittparteien weitergeben. Wir versichern ebenfalls, dass leitende Figuren, Beamte und Angestellte ihr 
                    Passwort und alle anderen Informationen streng vertraulich behandeln und sie nicht an Drittparteien weitergeben.
                </td>
            </tr>
            <tr>
                <td>
                    2. Sollte Ihr Passwort und/oder jegliche Informationen bezüglich Buchungen und auch jede Information aus dem VIP Portal 
                    offenbart werden, außer es wurde duch den vorherigen Paragraphen genehmigt, sollte der BENUTZER Office Depot 
                    sofort darüber in Kenntnis setzen und der BENUTZER muss Office Depot verteidigen, entschädigen und freihalten
                     von jeglichen Beschwerden, Forderungen, Haftungen, Verlusten, Schäden und Kosten, welche Anwaltskosten 
                     einschließen. Daraus ergibt sich, oder in Verbindung mit solch einer Enthüllung oder jeglichem nachfolgenden Missbrauch,
                     Veränderung, Unterschlagung oder betrügerische Benutzung von solch einem Passwort/oder Information.
                </td>
            </tr>
            <tr>
                <td>
                    3. BENUTZER dürfen keinen Versuch starten, das Passwort einer Drittpartie von Office Depot herauszufinden oder zu 
                    erhalten oder sich Zugang zu Informationen zu verschaffen mit der Nutzung jedes anderen Passworts, 
                    außer ihrem persönlichen.
                </td>
            </tr>
            <tr>
                <td>
                    4.BENUTZER bestätigt, dass Office Depot frei von jeglicher Haftung bezüglich der Enthüllung ihres Passwortes 
                    oder jegliche Informationen, die durch das Benutzen Ihres Passwortes erhalten wurde durch jegliche Drittpartei, 
                    ist, außer es ist durch die Fahrlässigkeit von Office Depot bedingt. 
                </td>
            </tr>
            <tr>
                <td>
                    5.BENUTZER verstehen, dass Office Depot GARANTIERT ODER BERECHTIGT NICHT, dass: a. Die Daten auf der Webseite exakt sind;
                     b. Der Zugang zur Webseite ununterbrochen sein wird; c. Die Webseite in der Zukunft nicht geändert wird, 
                     sohingehend, dass sie weniger, mehr oder andere Daten beinhaltet; or d. Office Depot wird die Webseite 
                     immer warten.
                </td>
            </tr>
            <tr>
                <td>
                    6. BENUTZER versteht, dass Ofice Depot keinerlei Haftung jeglicher Art für Verlust von Gewinn, indirekte, 
                    spezielle oder überhebliche Verluste oder Schäden, bedingt durch unsere Benutzung Ihrer Webseite.
                </td>
            </tr>
            <tr>
                <td>
                    7. Office Depot wird Informationen, die durch den BENUTZER bereitgestellt wurde für die Identifizierung und 
                    den Kontakt verbunden mit dem Service und Anmeldungen, um den Service für den BENUTZER zu gewährleisten. 
                    In der Ausführung des Services kann Office Depot Informationen ihren Agenten, Partnerfirmen, Zulieferern 
                    oder unabhängigen Unternehmern bereitstellen in dem Ausmaß, dass solch eine Information für die Wartung 
                    oder Serviceverbesserungen benötigt wird. Office Depot wird sein bestes tun, um das Sicherheitsmanagement 
                    der obengenannten Parteien zu beaufsichtigen, um solche Informationen zu schützen.
                </td>
            </tr>
           
        </table>
    </div>
    </form>
</body>
</html>


﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SCT_TermsAndConditionFrench.aspx.cs" 
Inherits="ModuleUI_Security_TnCTemplates_SCT_TermsAndConditionFrench" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Office Depot</title>
    <link rel="stylesheet" type="text/css" href="../../../Css/style.css" />
    <link type="text/css" href="../../../Css/ui-lightness/jquery-ui-1.8.5.custom.css"
        rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="../../../Css/superfish.css" media="screen" />
    
 </head>
<body>
    <form id="form1" runat="server">
    <div>
        <table width="100%"  cellspacing="5" cellpadding="0" class="form-table">
            <tr>
                <td>
                    Accord entre Office Depot et la partie indiquée sur le formulaire d'inscription.
                </td>
            </tr>
            <tr>
                <td>
                    En contrepartie du mot de passe qui nous ait donné par Office Depot afin de nous permettre d'avoir accès à certaines 
                    informations de livraison confidentielles disponibles via le site internet d’Office Depot incluant mais n’étant pas limité aux 
                    données des commandes et des ordres d’achat, l’UTILISATEUR accepte les conditions suivantes: 
                </td>
            </tr>
            <tr>
                <td>
                    1. L'UTILISATEUR devra garder ses Mots de passe et toutes les informations relatives aux commandes et toute information
                     recueillie à partir du portail VIP avec Office Depot confidentielles, et ne pas les révéler à un tiers. Nous devons également 
                     nous assurer que les administrateurs, dirigeants, et employés gardent votre mot de passe et toute information confidentielle 
                     et ne les révèlent pas à un tiers.
                </td>
            </tr>
            <tr>
                <td>
                    2. Si votre mot de passe et / ou toute information en ce qui concerne le portail VIP Office Depot est révélé, sauf dans la 
                    mesure permise par le paragraphe précédent, l'UTILISATEUR devra informer rapidement Office Depot d'un tel événement et 
                    l’UTILISATEUR devra défendre, indemniser et éviter à Office Depot toute plainte, réclamation, responsabilité, perte,
                     dommages et frais, y compris les frais juridiques, découlant de, ou en relation avec, d’une telle révélation, ou toute 
                     utilisation abusive ultérieure, la modification, le détournement ou l’utilisation frauduleuse du Mot de passe par exemple 
                     et / ou de l'information.
                </td>
            </tr>
            <tr>
                <td>
                    3. L'UTILISATEUR ne doit faire aucune tentative pour vérifier ou obtenir un Mot de passe attribué à une tierce partie par 
                    Office Depot ou pour rendre l'accès à l'information en utilisant n'importe quel Mot de passe autre que votre Mot de passe
                </td>
            </tr>
            <tr>
                <td>
                    4. L’UTILISATEUR confirme que Office Depot devra être exempt de toute responsabilité pour révéler votre Mot de passe, 
                    ou toute information acquise grâce à l'utilisation de votre Mot de passe, par une tierce partie, sauf si cette divulgation a été 
                    causé par la négligence d'Office Depot.
                </td>
            </tr>
            <tr>
                <td>
                    5. L'UTILISATEUR reconnaît qu’Office Depot NE MANDANTE OU NE GARANTIT PAS ce qui suit: a. Les données sur son site web sont exactes;
                     b. L'accès au site Web sera ininterrompu; c. Le site web ne seront pas modifié dans le futur pour fournir moins de données, 
                     plus de données ou des données différentes, ou d. Office Depot va toujours maintenir un site Web.
                </td>
            </tr>
            <tr>
                <td>
                    6. L'UTILISATEUR reconnaît qu’Office Depot n'assume aucune responsabilité que ce soit, y compris (sans s'y limiter) la 
                    responsabilité pour la perte de profits, les pertes indirectes, particuliers ou consécutifs ou des dommages-intérêts, 
                    découlant de notre utilisation de son site web.
                </td>
            </tr>
            <tr>
                <td>
                    7. Office Depot va utiliser l'information fournie par l'UTILISATEUR pour l'identification et les coordonnées relatives à son 
                    service et les applications afin d'établir ce service pour l'UTILISATEUR. Dans le cadre de ses services,
                     Office Depot peut fournir des informations à ses agents des sociétés du groupe, sous-traitants ou entrepreneurs indépendants dans la 
                     mesure où ces informations sont nécessaires pour l'amélioration de la maintenance ou du service. Office Depot va faire de 
                     son mieux pour superviser la gestion de la sécurité des parties mentionnées ci-dessus afin de protéger ces informations.
                </td>
            </tr>
           
        </table>
    </div>
    </form>
</body>
</html>


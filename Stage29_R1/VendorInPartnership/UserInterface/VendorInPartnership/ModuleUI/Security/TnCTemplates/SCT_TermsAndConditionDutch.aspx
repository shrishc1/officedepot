﻿<%@ Page Title="" Language="C#"  AutoEventWireup="true" CodeFile="SCT_TermsAndConditionDutch.aspx.cs" Inherits="ModuleUI_Security_TnCTemplates_SCT_TermsAndConditionDutch" %>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Office Depot</title>
    <link rel="stylesheet" type="text/css" href="../../../Css/style.css" />
    <link type="text/css" href="../../../Css/ui-lightness/jquery-ui-1.8.5.custom.css"
        rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="../../../Css/superfish.css" media="screen" />
    
 </head>
<body>
    <form id="form1" runat="server">
    <div>
        <table width="100%"  cellspacing="5" cellpadding="0" class="form-table">
            <tr>
                <td>
                    Overeenkomst tussen Office Depot en de partij gespecifieerd op het inschrijfformulier.
                </td>
            </tr>
            <tr>
                <td>
                   In overweging van Office Depot heeft u ons een wachtwoord gegeven om ons toegang te schaffen tot bepaalde 
                   vertrouwelijke levering informatie, toegankelijk en verkrijgbaar via de website van Office Depot  op het internet met inbegrip 
                   van maar niet beperkt tot boekings- en bestellingsgegevens, GEBRUIKER stemt ermee in als volgt:
                </td>
            </tr>
            <tr>
                <td>
                    1. GEBRUIKER zal zijn wachtwoord en alle informatie met betrekking tot boekingen en alle informatie vergaard uit HET VIP-portaal
                      van Office Depot vertrouwelijk houden, en deze niet onthullen aan derden. Wij zullen erop toezien dat bestuurders, 
                      functionarissen en werknemers uw wachtwoord en al deze informatie vertrouwelijk houden en deze niet aan derden
                       onthullen.
                </td>
            </tr>
            <tr>
                <td>
                    2. Mocht uw wachtwoord en/of informatie met betrekking tot het VIP-portaal van Office Depot geopenbaard worden,
                     behalve zoals toegestaan ​​door de vorige paragraaf, zal de GEBRUIKER onmiddellijk in kennis van Office Depot 
                     worden genomen voor een dergelijke gebeurtenis en de GEBRUIKER zal zich kunnen verdedigen en
                      Office Depot schadeloos stellen en vrijwaren van en tegen elke klacht, claim, aansprakelijkheid, 
                      verlies, schade en kosten, waaronder juridische kosten, voortvloeiend uit of in verband met een 
                      dergelijke openbaring, of een latere misbruik, wijziging, verduistering, of frauduleus gebruik van een dergelijk wachtwoord en/of informatie.
                </td>
            </tr>
            <tr>
                <td>
                    3. GEBRUIKER zal geen enkele poging ondernemen om na te gaan of te verkrijgen van een wachtwoord toegewezen aan derden door Office Depot 
                     of de toegang tot informatie met behulp van een wachtwoord dat niet uw wachtwoord is.
                </td>
            </tr>
            <tr>
                <td>
                    4.GEBRUIKER bevestigt dat Office Depot vrij zal zijn van alle aansprakelijkheid voor het onthullen 
                    van uw wachtwoord, of informatie verkregen door het gebruik van uw wachtwoord, door derden, 
                    tenzij dit is veroorzaakt door nalatigheid van Office Depot.
                </td>
            </tr>
            <tr>
                <td>
                    5. GEBRUIKER erkent dat Office Depot GEEN GARANTIE GEEFT dat: a). De gegevens op de website juist zijn; 
                    b). De toegang tot de website kan worden ononderbroken; 
                    c). De website niet zal worden gewijzigd in de toekomst door minder gegevens, meer gegevens of andere gegevens te verstrekken; of d). Office Depot altijd een website zal onderhouden.
                </td>
            </tr>
            <tr>
                <td>
                    6. GEBRUIKER erkent dat Office Depot geen enkele aansprakelijkheid zal hebben, inclusief (zonder beperking)
                     aansprakelijkheid voor gederfde winst, indirecte, bijzondere of gevolgschade of schade als gevolg van gebruik van onze 
                     website.
                </td>
            </tr>
            <tr>
                <td>
                    7. Office Depot zal gebruik maken van informatie die door de GEBRUIKER is verschaft voor identificatie en voor contact op te 
                    nemen in verband met haar dienstverlening en toepassingen, om de dienstverlening vast te stellen voor de GEBRUIKER. Bij de 
                    uitvoering van haar diensten, kan Office Depot informatie verstrekken aan haar agenten, groepsmaatschappijen, 
                    onderaannemers of onafhankelijke contractanten in de mate dat deze informatie nodig is voor onderhoud of verbetering van 
                    de dienstverlening. Office Depot zal zijn best doen om het security management van de bovengenoemde partijen te 
                    begeleiden om deze informatie te beschermen.
                </td>
            </tr>
           
        </table>
    </div>
    </form>
</body>
</html>




﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    CodeFile="SCT_UserEdit.aspx.cs" Inherits="SCT_UserEdit" MaintainScrollPositionOnPostback="true" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="../../CommonUI/UserControls/ucCountry.ascx" TagName="ucCountry"
    TagPrefix="uc1" %>
<%@ Register Src="../../CommonUI/UserControls/ucSeacrhVendor.ascx" TagName="ucSeacrhVendor"
    TagPrefix="cc3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script language="javascript" type="text/javascript">
        function SelectSingleRadiobutton(rdbtnid) {
            var rdBtn = document.getElementById(rdbtnid);
            var rdBtnList = document.getElementsByTagName("input");
            for (i = 0; i < rdBtnList.length; i++) {
                if (rdBtnList[i].type == "radio" && rdBtnList[i].id != rdBtn.id) {
                    rdBtnList[i].checked = false;
                }
            }
        }
        function checkStockPlannerSelected(source, args) {
            var Role = document.getElementById('<%=ddlRole.ClientID %>').value;

            if (Role == 6) {
                var StockPlanner = document.getElementById('<%=uclstStockPlannerLeft.ClientID %>');

                if (StockPlanner.options.length <= 0) {
                    args.IsValid = false;
                }
            }
        }
        function confirmDelete() {
            return confirm('<%=deleteMessage%>');
        }

    </script>
    <style type="text/css">
        .alignleft {
            text-align: left !important;
        }
    </style>
    <h2>
        <cc1:ucLabel ID="lblUserSetup" runat="server" Text="User Setup"></cc1:ucLabel>
    </h2>
    <asp:ScriptManager ID="SM1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="up1" runat="server">
        <ContentTemplate>
            <div class="right-shadow">
                <div class="formbox">
                    <cc1:ucMultiView ID="mvUserSetting" runat="server" ActiveViewIndex="0">
                        <cc1:ucView ID="vwUserSettings" runat="server">
                            <cc1:ucPanel ID="pnlPersonalDetails" runat="server" GroupingText="Personal Details"
                                CssClass="fieldset-form">
                                <table width="100%" cellspacing="5" cellpadding="0" class="form-table">
                                    <tr>
                                        <td style="font-weight: bold;" width="14%">
                                            <cc1:ucLabel ID="lblUserIdEmail" runat="server" Text="User ID/Email" isRequired="true"></cc1:ucLabel>
                                        </td>
                                        <td style="font-weight: bold;" width="1%" align="center">:
                                        </td>
                                        <td style="font-weight: bold;">
                                            <cc1:ucTextbox ID="txtUserIDEmail" runat="server" Width="220px" MaxLength="50"></cc1:ucTextbox>
                                            <asp:RegularExpressionValidator ID="revUserIDEmailreq" ControlToValidate="txtUserIDEmail"
                                                runat="server" ValidationGroup="Save" ErrorMessage="Please enter Correct User ID/Email"
                                                Display="None" ValidationExpression="^[\w-\.\']{1,}\@([\da-zA-Z-]{1,}\.){1,3}[\da-zA-Z-]{2,6}$" />
                                            <asp:RequiredFieldValidator ID="rfvUsernameReq" runat="server" ControlToValidate="txtUserIDEmail"
                                                ValidationGroup="Save" ErrorMessage="Please enter User ID/Email" Display="None"></asp:RequiredFieldValidator>
                                        </td>
                                        <td style="font-weight: bold">
                                            <cc1:ucLabel ID="lblPassword" runat="server" Text="Password" Visible="false"></cc1:ucLabel>
                                        </td>
                                        <td style="font-weight: bold;" align="center">
                                            <cc1:ucLabel ID="UcLabel1" runat="server" Text=":" Visible="false"></cc1:ucLabel>
                                        </td>
                                        <td>
                                            <cc1:ucLabel ID="lblPasswordText" runat="server" Width="130px" Visible="false"></cc1:ucLabel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: bold">
                                            <cc1:ucLabel ID="lblFirstName" runat="server" Text="First Name" isRequired="true"></cc1:ucLabel>
                                        </td>
                                        <td style="font-weight: bold;" align="center">:
                                        </td>
                                        <td>
                                            <cc1:ucTextbox ID="txtFirstName" runat="server" Width="130px" MaxLength="50"></cc1:ucTextbox>
                                            <asp:RequiredFieldValidator ID="rfvFirstNameReq" runat="server" ControlToValidate="txtFirstName"
                                                ValidationGroup="Save" ErrorMessage="Please enter First Name" Display="None"></asp:RequiredFieldValidator>
                                        </td>
                                        <td style="font-weight: bold">
                                            <cc1:ucLabel ID="lblLastName" runat="server" Text="Last Name" isRequired="true"></cc1:ucLabel>
                                        </td>
                                        <td style="font-weight: bold;" align="center">:
                                        </td>
                                        <td>
                                            <cc1:ucTextbox ID="txtLastName" runat="server" Width="130px" MaxLength="50"></cc1:ucTextbox>
                                            <asp:RequiredFieldValidator ID="rfvLastNameReq" runat="server" ControlToValidate="txtLastName"
                                                ValidationGroup="Save" ErrorMessage="Please enter Last Name" Display="None"></asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: bold">
                                            <cc1:ucLabel ID="lblTelephone" runat="server" Text="Telephone" isRequired="true"></cc1:ucLabel>
                                        </td>
                                        <td style="font-weight: bold;" align="center">:
                                        </td>
                                        <td>
                                            <cc1:ucTextbox ID="txtTelephone" runat="server" Width="220px" MaxLength="50"></cc1:ucTextbox>
                                            <asp:RequiredFieldValidator ID="rfvTelephoneReq" runat="server" ControlToValidate="txtTelephone"
                                                ValidationGroup="Save" ErrorMessage="Please enter Telephone No" Display="None"></asp:RequiredFieldValidator>
                                        </td>
                                        <td style="font-weight: bold">
                                            <cc1:ucLabel ID="lblFax" runat="server" Text="Fax"></cc1:ucLabel>
                                        </td>
                                        <td style="font-weight: bold;" align="center">:
                                        </td>
                                        <td>
                                            <cc1:ucTextbox ID="txtFax" runat="server" Width="220px" MaxLength="50"></cc1:ucTextbox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: bold">
                                            <cc1:ucLabel ID="lblLanguage" runat="server" Text="Language" isRequired="true"></cc1:ucLabel>
                                        </td>
                                        <td style="font-weight: bold;" align="center">:
                                        </td>
                                        <td>
                                            <cc1:ucDropdownList ID="ddlLanguage" runat="server" Width="220px">
                                            </cc1:ucDropdownList>
                                            <asp:RequiredFieldValidator ID="rfvLanguageReq" runat="server" ControlToValidate="ddlLanguage"
                                                ValidationGroup="Save" ErrorMessage="Please select Language" Display="None" InitialValue="0"></asp:RequiredFieldValidator>
                                        </td>
                                        <td style="font-weight: bold">
                                            <cc1:ucLabel ID="lblJobTitle" runat="server" Text="Job Title"></cc1:ucLabel>
                                        </td>
                                        <td style="font-weight: bold;" align="center">:
                                        </td>
                                        <td>
                                            <cc1:ucTextbox ID="txtJobTitleValue" runat="server" Width="220px" MaxLength="50"></cc1:ucTextbox>
                                        </td>
                                    </tr>
                                </table>
                            </cc1:ucPanel>
                            <br />
                            <cc1:ucPanel ID="pnlAccessPermissions" runat="server" GroupingText="Access Permissions"
                                CssClass="fieldset-form">
                                <table width="100%" cellspacing="5" cellpadding="0" class="form-table">
                                    <tr>
                                        <td style="font-weight: bold;" width="14%">
                                            <cc1:ucLabel ID="lblRole" runat="server" Text="Role" isRequired="true"></cc1:ucLabel>
                                        </td>
                                        <td style="font-weight: bold;" width="1%" align="center">:
                                        </td>
                                        <td style="font-weight: bold;" width="20%">
                                            <cc1:ucDropdownList ID="ddlRole" runat="server" Width="150px" OnSelectedIndexChanged="ddlRole_SelectedIndexChanged"
                                                AutoPostBack="true">
                                            </cc1:ucDropdownList>
                                            <asp:RequiredFieldValidator ID="rfvRoleReq" runat="server" ControlToValidate="ddlRole"
                                                ValidationGroup="Save" ErrorMessage="Please select Role" Display="None" InitialValue="0"></asp:RequiredFieldValidator>
                                        </td>
                                        <td style="width: 16%;">
                                            <cc1:ucLabel ID="lblReceiveDiscEmailAlertMsg" runat="server" Text="Receive discrepancy email alert :" Visible="false"></cc1:ucLabel>
                                        </td>
                                        <td style="width: 1%"></td>
                                        <td style="width: 31%">
                                            <cc1:ucCheckbox ID="chkReceiveDiscEmailAlert" runat="server" Checked="false" Visible="false" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: bold">
                                            <cc1:ucLabel ID="lblAuthorizeRefusal" runat="server" Text="Authorize Refusal"></cc1:ucLabel>
                                        </td>
                                        <td style="font-weight: bold;" align="center">:
                                        </td>
                                        <td>
                                            <cc1:ucCheckbox ID="chkAuthorizeRefusal" runat="server" />
                                        </td>
                                        <td>
                                            <cc1:ucCheckbox ID="chkIsMediator" runat="server" Visible="false" />
                                        </td>
                                        <td colspan="2"></td>
                                    </tr>
                                    <tr id="trAccountStatus" runat="server" style="display: none">
                                        <td style="font-weight: bold">
                                            <cc1:ucLabel ID="lblAccountStatus" runat="server" Text="Account Status"></cc1:ucLabel>
                                        </td>
                                        <td style="font-weight: bold;" align="center">:
                                        </td>
                                        <td>
                                            <cc1:ucLabel ID="lblAccountStatusValue" runat="server"></cc1:ucLabel>
                                        </td>
                                        <td colspan="3"></td>
                                    </tr>
                                    <tr id="trsp" runat="server" style="display: none">
                                        <td>
                                            <cc1:ucLabel ID="lblStockPlannerGroupings" runat="server"></cc1:ucLabel>
                                        </td>
                                        <td style="font-weight: bold;" align="center">:
                                        </td>
                                        <td>
                                            <cc1:ucDropdownList ID="ddlStockPlannerGroupings" runat="server" Width="150px">
                                            </cc1:ucDropdownList>
                                        </td>
                                        <td colspan="3"></td>
                                    </tr>
                                </table>
                            </cc1:ucPanel>
                            <br />
                            <cc1:ucPanel ID="pnlCountrySpecific" runat="server" GroupingText="Country Specific Settings"
                                CssClass="fieldset-form" Style="display: none">
                                <table width="100%" cellspacing="5" cellpadding="0" border="0" class="form-table">
                                    <tr>
                                        <td style="font-weight: bold;" width="14%">
                                            <cc1:ucLabel ID="lblCountry" runat="server" Text="Country" isRequired="true">
                                            </cc1:ucLabel>
                                        </td>
                                        <td style="font-weight: bold;" align="left" width="1%">:
                                        </td>
                                        <td style="font-weight: bold;" colspan="3" width="85%" align="left">
                                            <uc1:ucCountry ID="ucCountry" runat="server" width="150px" />
                                        </td>
                                    </tr>
                                    <tr id="trStockPlanner" runat="server">
                                        <td style="font-weight: bold" valign="top" width="14%">
                                            <cc1:ucLabel ID="lblStockPlanner" runat="server" Text="Stock Planner" isRequired="true"></cc1:ucLabel>
                                        </td>
                                        <td style="font-weight: bold;" align="center" valign="top" width="1%">:
                                        </td>
                                        <td width="20%">
                                            <asp:UpdatePanel ID="UpCountry" runat="server">
                                                <ContentTemplate>
                                                    <cc1:ucListBox ID="uclstStockPlannerRight" runat="server" Height="200px" Width="320px">
                                                    </cc1:ucListBox>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </td>
                                        <td valign="middle" align="center" style="width: 10%">
                                            <div>
                                                <cc1:ucButton ID="btnMoveRightAllStockPlanner" runat="server" Text=">>" CssClass="button"
                                                    Width="35px" OnClick="btnMoveRightAllStockPlanner_Click" />
                                            </div>
                                            &#160;&#160;
                                            <div>
                                                <cc1:ucButton ID="btnMoveRightStockPlanner" runat="server" Text="&gt;" CssClass="button"
                                                    Width="35px" OnClick="btnMoveRightStockPlanner_Click" />
                                            </div>
                                            &#160;&#160;
                                            <div>
                                                <cc1:ucButton ID="btnMoveLeftStockPlanner" runat="server" Text="&lt;" CssClass="button"
                                                    Width="35px" OnClick="btnMoveLeftStockPlanner_Click" />
                                            </div>
                                            &#160;&#160;
                                            <div>
                                                <cc1:ucButton ID="btnMoveLeftAllStockPlanner" runat="server" Text="&lt;&lt;" CssClass="button"
                                                    Width="35px" OnClick="btnMoveLeftAllStockPlanner_Click" />
                                            </div>
                                        </td>
                                        <td style="width: 55%">
                                            <cc1:ucListBox ID="uclstStockPlannerLeft" runat="server" Height="200px" Width="320px">
                                            </cc1:ucListBox>
                                            <asp:CustomValidator ID="cusvStockPlannerReq" runat="server" ClientValidationFunction="checkStockPlannerSelected"
                                                ValidationGroup="Save" Display="None"></asp:CustomValidator>
                                        </td>
                                    </tr>
                                </table>
                            </cc1:ucPanel>
                        </cc1:ucView>
                        <cc1:ucView ID="vwSiteSettings" runat="server">
                            <cc1:ucPanel ID="pnlRoleTemplate" runat="server" GroupingText="Role Template"
                                CssClass="fieldset-form">
                                <table width="100%" cellspacing="5" cellpadding="0" class="form-table">
                                    <tr>
                                        <td style="font-weight: bold; width: 14%">
                                            <cc1:ucLabel ID="lblRoleTemplate" runat="server" Text="Role Template" isRequired="true"></cc1:ucLabel>
                                        </td>
                                        <td style="font-weight: bold; width: 1%">:
                                        </td>
                                        <td style="font-weight: bold; width: 15%">
                                            <cc1:ucDropdownList ID="ddlTemplateReport" runat="server" Width="150px">
                                            </cc1:ucDropdownList>
                                            <asp:RequiredFieldValidator ID="rfvTemplateRoleReq" runat="server" ControlToValidate="ddlTemplateReport"
                                                ValidationGroup="SaveTemplate" ErrorMessage="Please select Role Template" Display="None"
                                                InitialValue="0"></asp:RequiredFieldValidator>
                                        </td>
                                        <td style="font-weight: bold; width: 70%">
                                            <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Images/Preview.jpg"
                                                OnClick="btnPreviewReportTemplate_Click" />
                                        </td>
                                    </tr>
                                </table>
                            </cc1:ucPanel>
                            <cc1:ucPanel ID="pnlAccessingRights" runat="server" GroupingText="Assigning Access Rights" Visible="false"
                                CssClass="fieldset-form">
                                <table width="100%" cellspacing="5" cellpadding="0" class="form-table">
                                    <tr>
                                        <td style="font-weight: bold; width: 14%">
                                            <cc1:ucLabel ID="lblTemplateMaster" runat="server" Text="Template Master " isRequired="true"></cc1:ucLabel>
                                        </td>
                                        <td style="font-weight: bold; width: 1%">:
                                        </td>
                                        <td style="font-weight: bold; width: 15%">
                                            <cc1:ucDropdownList ID="ddlTemplateMaster" runat="server" Width="150px">
                                            </cc1:ucDropdownList>
                                        </td>
                                        <td style="font-weight: bold; width: 70%">
                                            <asp:ImageButton ID="btnPreviewMasterTemplate" runat="server" ImageUrl="~/Images/Preview.jpg"
                                                OnClick="btnPreviewMasterTemplate_Click" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <cc1:ucLabel ID="lblTemplateReport" runat="server" Text="Template Report" isRequired="true"></cc1:ucLabel>
                                        </td>
                                        <td>:
                                        </td>
                                        <td>
                                            <cc1:ucDropdownList ID="ddlTemplateReport_1" runat="server" Width="150px">
                                            </cc1:ucDropdownList>
                                        </td>
                                        <td>
                                            <asp:ImageButton ID="btnPreviewReportTemplate" runat="server" ImageUrl="~/Images/Preview.jpg"
                                                OnClick="btnPreviewReportTemplate_Click" />
                                        </td>
                                    </tr>
                                </table>
                            </cc1:ucPanel>
                            <cc1:ucPanel ID="pnlSiteSettings" runat="server" GroupingText="Template for Transaction"
                                CssClass="fieldset-form">
                                <table width="100%" cellspacing="5" cellpadding="0" class="form-table">
                                    <tr>
                                        <td style="font-weight: bold;">
                                            <cc1:ucLabel ID="lblCountry_1" runat="server" Text="Country" isRequired="true"></cc1:ucLabel>
                                        </td>
                                        <td style="font-weight: bold;" align="center">:
                                        </td>
                                        <td style="font-weight: bold;" colspan="2">
                                            <cc1:ucDropdownList AutoPostBack="true" ID="ddlCountry" runat="server" Width="150px"
                                                OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: bold; width: 14%" valign="top">
                                            <cc1:ucLabel ID="lblSites" runat="server" Text="Sites" isRequired="true"></cc1:ucLabel>
                                        </td>
                                        <td style="font-weight: bold; width: 1%" align="center" valign="top">:
                                        </td>
                                        <td style="width: 20%">
                                            <cc1:ucListBox ID="uclstSiteLeft" runat="server" Height="200px" Width="160px">
                                            </cc1:ucListBox>
                                        </td>
                                        <td valign="middle" align="center" style="width: 10%">
                                            <table>
                                                <tr>
                                                    <td>
                                                        <cc1:ucButton ID="btnMoveRightSite" runat="server" Text=">" CssClass="button" Width="35px"
                                                            OnClick="btnMoveRightSite_Click" /></td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td valign="top" style="width: 55%">
                                            <cc1:ucGridView ID="grdSelectedSites" Width="100%" runat="server" CssClass="grid"
                                                OnRowDataBound="grdSelectedSites_RowDataBound" OnRowCommand="grdSelectedSites_RowCommand">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Default">
                                                        <HeaderStyle Width="5%" CssClass="alignleft" HorizontalAlign="Left" />
                                                        <ItemTemplate>
                                                            <asp:RadioButton ID="rdoDefault" runat="server" GroupName="Default" OnClick="javascript:SelectSingleRadiobutton(this.id)" />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Site Name">
                                                        <HeaderStyle Width="55%" HorizontalAlign="Left" CssClass="alignleft" />
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblSites" Text='<%#Eval("SelectedSites") %>' runat="server"></asp:Label>
                                                            <asp:HiddenField ID="hdnSiteID" runat="server" Value='<%#Eval("Siteid") %>' />
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Template" Visible="false">
                                                        <HeaderStyle CssClass="alignleft" Width="20%" HorizontalAlign="Left" />
                                                        <ItemTemplate>
                                                            <asp:DropDownList ID="ddlTemplates" runat="server" Width="100px" OnSelectedIndexChanged="ddlTemplates__SelectedIndexChanged"
                                                                AutoPostBack="true">
                                                            </asp:DropDownList>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Preview" Visible="false">
                                                        <HeaderStyle Width="10%" HorizontalAlign="Left" />
                                                        <ItemTemplate>
                                                            <asp:ImageButton ID="btnPreviewTemplate" runat="server" ImageUrl="~/Images/Preview.jpg" />
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Remove">
                                                        <HeaderStyle Width="10%" HorizontalAlign="Left" />
                                                        <ItemTemplate>
                                                            <asp:ImageButton ID="btnRemove" ImageUrl="~/Images/delete.jpg" runat="server" CommandArgument='<%#Eval("Siteid") %>'
                                                                CommandName="Remove" OnCommand="btnRemove_Click" />
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:TemplateField>
                                                </Columns>
                                            </cc1:ucGridView>
                                        </td>
                                    </tr>
                                </table>
                            </cc1:ucPanel>
                        </cc1:ucView>
                    </cc1:ucMultiView>
                </div>
            </div>
            <div class="bottom-shadow">
            </div>
            <div class="button-row">
                <cc1:ucButton ID="btnProceed" runat="server" Text="Proceed" CssClass="button"
                    Visible="false" OnClick="btnProceed_Click" />
                <cc1:ucButton ID="btnSaveProceed" runat="server" Text="Save and Proceed" CssClass="button"
                    OnClick="btnSaveProceed_Click" ValidationGroup="Save" />
                <cc1:ucButton ID="btnSave" runat="server" Text="Save" CssClass="button" Visible="false"
                    ValidationGroup="Save" OnClick="btnSave_Click" />
                <cc1:ucButton ID="btnResendApprovalMail" runat="server" Text="Resend Approval Mail"
                    CssClass="button" OnClick="btnResendApprovalMail_Click" Visible="false" />
                <cc1:ucButton ID="btnBlockUser" runat="server" Text="Block User" CssClass="button"
                    ValidationGroup="Save" Visible="false" OnClick="btnBlockUser_Click" />
                <cc1:ucButton ID="btnUnblockUser" runat="server" Text="Unblock User" CssClass="button"
                    ValidationGroup="Save" Visible="false" OnClick="btnUnblockUser_Click" />
                <cc1:ucButton ID="btnDeleteUser" runat="server" Text="Delete User" CssClass="button"
                    OnClientClick="return confirmDelete();" ValidationGroup="Save" Visible="false"
                    OnClick="btnDeleteUser_Click" />
                <cc1:ucButton ID="btnBack" runat="server" Text="Back" CssClass="button" OnClick="btnBack_Click" />
                <asp:ValidationSummary ID="VSSave" runat="server" ValidationGroup="Save" ShowSummary="false"
                    ShowMessageBox="true" />
                <asp:ValidationSummary ID="vsSaveTemplate" runat="server" ValidationGroup="SaveTemplate"
                    ShowSummary="false" ShowMessageBox="true" />
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

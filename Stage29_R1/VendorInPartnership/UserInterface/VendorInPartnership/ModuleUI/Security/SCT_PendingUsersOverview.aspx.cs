﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLogicLayer.ModuleBAL.Security;
using BusinessEntities.ModuleBE.Security;
using System.Collections.Generic;
using System.Linq;
using WebUtilities;
using BusinessEntities.ModuleBE.AdminFunctions;
using BusinessLogicLayer.ModuleBAL.AdminFunctions;
using BaseControlLibrary;
using Utilities;

public partial class SCT_PendingUsersOverview : CommonPage
{
    #region Declarations ...


    public SortDirection GridViewSortDirection {
        get {
            if (ViewState["GridViewSortDirection"] == null)
                ViewState["GridViewSortDirection"] = SortDirection.Ascending;
            return (SortDirection)ViewState["GridViewSortDirection"];
        }
        set { ViewState["GridViewSortDirection"] = value; }
    }

    public string SortDir {
        get {
            if (GridViewSortDirection == SortDirection.Ascending) {
                return "DESC";
            }
            else {
                return "ASC";
            }
        }
    }

    public string GridViewSortExp {
        get {
            if (ViewState["GridViewSortExp"] == null)
                ViewState["GridViewSortExp"] = "LoginID";
            return (string)ViewState["GridViewSortExp"];
        }
        set { ViewState["GridViewSortExp"] = value; }
    }
    
    protected string AllRejectedUserdeleteMessage = WebCommon.getGlobalResourceValue("AllRejectedUserdeleteMessage");
    const int PageSize = 30;
    const int DefaultPageIndex = 1;
    List<SCT_UserBE> lstUserSettings = new List<SCT_UserBE>();
    List<SCT_UserBE> lstCountrySettings = new List<SCT_UserBE>();

    #endregion

    #region Events ...

    protected void Page_InIt(object sender, EventArgs e)
    {
        if (ScriptManager.GetCurrent(Page) == null)
        {
            ScriptManager sMgr = new ScriptManager();
            Page.Form.Controls.AddAt(0, sMgr);
        }

        ddlCountry.CurrentPage = this;
        btnExportToExcel1.GridViewControl = gvUserExcelExport;
        btnExportToExcel1.FileName = "PendingAwaitingApproval";
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        txtUserNameValue.Focus();
    }

    protected override void OnPreRender(EventArgs e)
    {
        if (!IsPostBack)
        {
            ddlCountry.innerControlddlCountry.Items.Insert(0, new ListItem("--Select--", "-1"));
            ddlCountry.innerControlddlCountry.SelectedIndex = 0;

            //if (GetQueryStringValue("NewLoginRequest") != null && GetQueryStringValue("NewLoginRequest") == "Y")
            //    ddlStatus.SelectedValue = "Registered Awaiting Approval";

            BindRole();
            BindUserGrid();

            if (SortDir == "ASC") {
                GridViewSortDirection = SortDirection.Descending;
            }
            else {
                GridViewSortDirection = SortDirection.Ascending;
            }
        }

        //btnExportToExcel1.GridViewControl = gvUser;
        //btnExportToExcel1.FileName = "UserOverview";

        //ddlCountry.CurrentPage = this;
        //ddlSeacrhVendor.CurrentPage = this;
        //ddlSeacrhVendor.FunctionCalled = "GetAllVendor";
        base.OnPreRender(e);

        msVendor.setVendorsOnPostBack();
        msCarrier.setCarriersOnPostBack();
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        SCT_UserBAL oSCT_UserBAL = new SCT_UserBAL();
        SCT_UserBE oSCT_UserBE = new SCT_UserBE();
        List<SCT_UserBE> lstUsers = new List<SCT_UserBE>();

        oSCT_UserBE.Action = "GetUserOverview";
        oSCT_UserBE.AccountStatus = string.IsNullOrEmpty(ddlStatus.SelectedValue) ? null : ddlStatus.SelectedValue;
        oSCT_UserBE.CountryId = ddlCountry.innerControlddlCountry.SelectedIndex > 0 ? Convert.ToInt32(ddlCountry.innerControlddlCountry.SelectedItem.Value) : (int?)null;

        ucListBox lstCarrierRight = msCarrier.FindControl("lstRight") as ucListBox;
        if (lstCarrierRight.Items.Count <= 0) 
        {
            msCarrier.SelectedCarrierIDs = string.Empty;
            msCarrier.SelectedCarrierName = string.Empty;
        }

        #region Vendor search related ..
        if (!string.IsNullOrEmpty(msVendor.SelectedVendorIDs))
            oSCT_UserBE.VendorIDs = msVendor.SelectedVendorIDs;

        if (!string.IsNullOrEmpty(msVendor.SelectedVendorName))
            oSCT_UserBE.VendorName = msVendor.SelectedVendorName;

        if (!string.IsNullOrEmpty(msCarrier.SelectedCarrierIDs))
            oSCT_UserBE.CarrierIDs = msCarrier.SelectedCarrierIDs;

        if (!string.IsNullOrEmpty(msCarrier.SelectedCarrierName))
            oSCT_UserBE.CarrierName = msCarrier.SelectedCarrierName;

        oSCT_UserBE.UserRoleID = ddlRole.SelectedIndex > 0 ? Convert.ToInt32(ddlRole.SelectedItem.Value) : (int?)null;

        TextBox txtSearchedVendor = msVendor.FindControl("ucVendor").FindControl("txtVendorNo") as TextBox;
        if (txtSearchedVendor != null)
            oSCT_UserBE.SearchVendorText = txtSearchedVendor.Text;

        TextBox txtSearchedCarrier = msCarrier.FindControl("ucCarrier").FindControl("txtCarrier") as TextBox;
        if (txtSearchedCarrier != null)
            oSCT_UserBE.SearchCarrierText = txtSearchedCarrier.Text;

        if (txtUserNameValue != null)
            oSCT_UserBE.UserSearchText = txtUserNameValue.Text;
        #endregion

        msVendor.setVendorsOnPostBack();
        msCarrier.setCarriersOnPostBack();

        if (ddlCountry.innerControlddlCountry.SelectedIndex == 0)
        oSCT_UserBE.SelectedSites = ddlCountry.CountryIDs;

        lstUsers = oSCT_UserBAL.GetUsers(oSCT_UserBE);

        //if (ddlStatus.SelectedIndex == 0)
        //lstUsers = lstUsers.Where(U => U.AccountStatus=="Registered Awaiting Approval" || U.AccountStatus=="Edited Awaiting Approval").ToList();

        oSCT_UserBAL = null;
        gvUser.PageIndex = 0;
        gvUser.DataSource = lstUsers;
        gvUser.DataBind();
        gvUserExcelExport.DataSource = lstUsers;
        gvUserExcelExport.DataBind();
        ViewState["lstUsers"] = lstUsers;

       
        GridViewSortExp = "LoginID";
        GridViewSortDirection = SortDirection.Ascending;

        Session["PendingUsersOverSearch"] = oSCT_UserBE;
    }

    private void SortGridView(string sortExpression, SortDirection direction) {
        List<SCT_UserBE> lstUsers = new List<SCT_UserBE>();
        try {
            if (ViewState["lstUsers"] != null) {

                lstUsers = Utilities.GenericListHelper<SCT_UserBE>.SortList((List<SCT_UserBE>)ViewState["lstUsers"], sortExpression, direction);

                gvUser.DataSource = lstUsers;
                gvUser.DataBind();

                CommonPage c = new CommonPage();
                c.LocalizeGridHeader(gvUser);
            }
        }
        catch (Exception ex) {
            LogUtility.SaveErrorLogEntry(ex);
        }

    }
   

    protected void rptCountry_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            if (lstUserSettings.FindAll(x => x.CountryId == ((MAS_CountryBE)e.Item.DataItem).CountryID).Count > 0)
            {
                CheckBox chkCountry = (CheckBox)e.Item.FindControl("chkCountry");
                chkCountry.Checked = true;
            }
        }
    }

    protected void rptAppointmentCountry_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Repeater rptAppointmentCountrySites = (Repeater)e.Item.FindControl("rptAppointmentCountrySites");
            List<SCT_UserBE> lstSites = lstUserSettings.FindAll(x => x.CountryId == ((SCT_UserBE)e.Item.DataItem).CountryId && x.SiteId != null).ToList();
            rptAppointmentCountrySites.DataSource = lstSites;
            rptAppointmentCountrySites.DataBind();
        }
    }

    protected void rptDiscrepanciesCountry_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Repeater rptDiscrepanciesCountrySites = (Repeater)e.Item.FindControl("rptDiscrepanciesCountrySites");
            List<SCT_UserBE> lstSites = lstUserSettings.FindAll(x => x.CountryId == ((SCT_UserBE)e.Item.DataItem).CountryId && x.SiteId != null).ToList();
            rptDiscrepanciesCountrySites.DataSource = lstSites;
            rptDiscrepanciesCountrySites.DataBind();
        }
    }

    protected void rptAppointmentCountrySites_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            CheckBox chkSite = (CheckBox)e.Item.FindControl("chkSite");
            if (((SCT_UserBE)e.Item.DataItem).SchedulingContact == 'Y')
            {
                chkSite.Checked = true;
            }
        }
    }

    protected void rptDiscrepanciesCountrySites_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            CheckBox chkSite = (CheckBox)e.Item.FindControl("chkSite");
            if (((SCT_UserBE)e.Item.DataItem).DiscrepancyContact == 'Y')
            {
                chkSite.Checked = true;
            }
        }
    }

    protected void rptOtifCountry_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            CheckBox chkCountry = (CheckBox)e.Item.FindControl("chkCountry");
            if (((SCT_UserBE)e.Item.DataItem).OTIFContact == 'Y')
            {
                chkCountry.Checked = true;
            }
        }
    }

    protected void rptScorecardCountry_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            CheckBox chkCountry = (CheckBox)e.Item.FindControl("chkCountry");
            if (((SCT_UserBE)e.Item.DataItem).ScorecardContact == 'Y')
            {
                chkCountry.Checked = true;
            }
        }
    }

    protected void rptCarrierCountry_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Repeater rptCarrierCountrySites = (Repeater)e.Item.FindControl("rptCarrierCountrySites");
            List<SCT_UserBE> lstSites = lstUserSettings.FindAll(x => x.CountryId == ((SCT_UserBE)e.Item.DataItem).CountryId && x.SiteId != null).ToList();
            rptCarrierCountrySites.DataSource = lstSites;
            rptCarrierCountrySites.DataBind();
        }
    }

    protected void rptCarrierCountrySites_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            CheckBox chkSite = (CheckBox)e.Item.FindControl("chkSite");
            if (((SCT_UserBE)e.Item.DataItem).SchedulingContact == 'Y')
            {
                chkSite.Checked = true;
            }
        }
    }

    protected void gvUser_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            ucLinkButton lnkDetails = (ucLinkButton)e.Row.FindControl("lnkDetails");
            if (((SCT_UserBE)e.Row.DataItem).UserRoleID != 2 && ((SCT_UserBE)e.Row.DataItem).UserRoleID != 3)
            {
                lnkDetails.Visible = false;
            }
        }
    }

    protected void gvUser_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "VendorModulesDetails")
        {
            string[] splitVals = Convert.ToString(e.CommandArgument).Split(new char[] { ',' });
            int UserID = 0;
            int UserRoleID = 0;
            if (splitVals.Length > 0)
            {
                UserID = Convert.ToInt32(splitVals[0]);
            }
            if (splitVals.Length > 1)
            {
                UserRoleID = Convert.ToInt32(splitVals[1]);
            }

            bindOtherSettings(UserID, UserRoleID);
            mdlVendorModulesDetails.Show();
        }
    }

    protected void gvUser_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvUser.PageIndex = e.NewPageIndex;
        //BindUserGrid(e.NewPageIndex);

        SortGridView(GridViewSortExp, GridViewSortDirection);
    }

    #endregion

    #region Methods ...

    private void BindRole()
    {
        SCT_TemplateBAL oSCT_TemplateBAL = new SCT_TemplateBAL();
        SCT_UserRoleBE oSCT_UserRoleBE = new SCT_UserRoleBE();
        oSCT_UserRoleBE.Action = "GetAllUsers";

        List<SCT_UserRoleBE> lstRole = oSCT_TemplateBAL.GetUserRolesBAL(oSCT_UserRoleBE);
        lstRole = lstRole.Where(R => R.UserRoleID == 2 || R.UserRoleID == 3).ToList();
        oSCT_TemplateBAL = null;
        if (lstRole.Count > 0)
        {
            FillControls.FillDropDown(ref ddlRole, lstRole, "RoleName", "UserRoleID", "Select");
        }
    }

    private void BindUserGrid(int pageIndex = 0)
    {
        //SCT_UserBAL oSCT_UserBAL = new SCT_UserBAL();
        SCT_UserBE oSCT_UserBE = new SCT_UserBE();
        List<SCT_UserBE> lstUsers = new List<SCT_UserBE>();
        if (Session["PendingUsersOverSearch"] != null)
        {
            oSCT_UserBE = (SCT_UserBE)Session["PendingUsersOverSearch"];

            #region Setting controls of Searched Criteria ..
            /* Country .. */
            if (ddlCountry.innerControlddlCountry.Items.Count > 0 && oSCT_UserBE.CountryId != null)
            {
                ddlCountry.innerControlddlCountry.SelectedIndex =
                    ddlCountry.innerControlddlCountry.Items.IndexOf(ddlCountry.innerControlddlCountry.Items.FindByValue(
                    Convert.ToString(oSCT_UserBE.CountryId)));
            }

            /* Status .. */
            if (ddlStatus.Items.Count > 0 && !string.IsNullOrEmpty(oSCT_UserBE.AccountStatus))
            {
                ddlStatus.SelectedIndex = ddlStatus.Items.IndexOf(ddlStatus.Items.FindByValue(oSCT_UserBE.AccountStatus));
            }

            /* Vendor .. */
            HiddenField hiddenSelectedIDs = msVendor.FindControl("hiddenSelectedIDs") as HiddenField;
            if (hiddenSelectedIDs != null && !string.IsNullOrEmpty(oSCT_UserBE.VendorIDs))
                hiddenSelectedIDs.Value = oSCT_UserBE.VendorIDs;

            HiddenField hiddenSelectedName = msVendor.FindControl("hiddenSelectedName") as HiddenField;
            if (hiddenSelectedName != null && !string.IsNullOrEmpty(oSCT_UserBE.VendorName))
                hiddenSelectedName.Value = oSCT_UserBE.VendorName;            

            ucListBox lstRight = msVendor.FindControl("lstRight") as ucListBox;
            ucListBox lstLeft = msVendor.FindControl("ucVendor").FindControl("lstLeft") as ucListBox;
            if (lstLeft != null && lstRight != null && !string.IsNullOrEmpty(oSCT_UserBE.SearchVendorText) && !string.IsNullOrEmpty(oSCT_UserBE.VendorIDs))
            {
                lstLeft.Items.Clear();
                lstRight.Items.Clear();
                msVendor.SearchVendorClick(oSCT_UserBE.SearchVendorText);

                string[] strVendorIDs = oSCT_UserBE.VendorIDs.Split(',');
                for (int index = 0; index < strVendorIDs.Length; index++)
                {
                    ListItem listItem = lstLeft.Items.FindByValue(strVendorIDs[index]);
                    if (listItem != null)
                    {
                        lstRight.Items.Add(listItem);
                        lstLeft.Items.Remove(listItem);
                    }
                }
            }


            /* Carrier .. */
            HiddenField hiddenSelectedCarrierIDs = msCarrier.FindControl("hiddenSelectedIDs") as HiddenField;
            if (hiddenSelectedCarrierIDs != null && !string.IsNullOrEmpty(oSCT_UserBE.CarrierIDs))
                hiddenSelectedCarrierIDs.Value = oSCT_UserBE.CarrierIDs;

            HiddenField hiddenSelectedCarrierName = msCarrier.FindControl("hiddenSelectedName") as HiddenField;
            if (hiddenSelectedCarrierName != null && !string.IsNullOrEmpty(oSCT_UserBE.CarrierName))
                hiddenSelectedCarrierName.Value = oSCT_UserBE.CarrierName;

            ucListBox lstCarrierRight = msCarrier.FindControl("lstRight") as ucListBox;
            ucListBox lstCarrierLeft = msCarrier.FindControl("ucCarrier").FindControl("lstLeft") as ucListBox;
            if (lstCarrierLeft != null && lstCarrierRight != null && !string.IsNullOrEmpty(oSCT_UserBE.SearchCarrierText) 
                && !string.IsNullOrEmpty(oSCT_UserBE.CarrierIDs))
            {
                lstCarrierLeft.Items.Clear();
                lstCarrierRight.Items.Clear();
                msCarrier.SearchCarrierClick(oSCT_UserBE.SearchCarrierText);

                string[] strCarrierIDs = oSCT_UserBE.CarrierIDs.Split(',');
                for (int index = 0; index < strCarrierIDs.Length; index++)
                {
                    ListItem listItem = lstLeft.Items.FindByValue(strCarrierIDs[index]);
                    if (listItem != null)
                    {
                        lstCarrierRight.Items.Add(listItem);
                        lstCarrierLeft.Items.Remove(listItem);
                    }
                }
            }

            /* Role .. */
            if (ddlRole.Items.Count > 0 && oSCT_UserBE.UserRoleID != null)
            {
                ddlRole.SelectedIndex = ddlRole.Items.IndexOf(ddlRole.Items.FindByValue(Convert.ToString(oSCT_UserBE.UserRoleID)));
            }
            #endregion
        }
        else
        {
            oSCT_UserBE.Action = "GetUserOverview";
            //if (GetQueryStringValue("NewLoginRequest") != null && GetQueryStringValue("NewLoginRequest").ToString() == "Y")
            oSCT_UserBE.AccountStatus = "PendingApproval";  // "Registered Awaiting Approval";

            //oSCT_UserBE.CountryIds = ddlCountry.innerControlddlCountry.

            if (ddlCountry.innerControlddlCountry.SelectedIndex == 0)
                oSCT_UserBE.SelectedSites = ddlCountry.CountryIDs;

            oSCT_UserBE.PageSize = PageSize;

            if (pageIndex > 0)
                oSCT_UserBE.PageIndex = pageIndex;
            else
                oSCT_UserBE.PageIndex = DefaultPageIndex;

        }       

        lstUsers = GetUsers(oSCT_UserBE);

        ViewState["lstUsers"] = lstUsers;

        //gvUser.DataSource = lstUsers;
        //gvUser.DataBind();
        SortGridView(GridViewSortExp, GridViewSortDirection);

        //-- Add By Abhinav
        gvUserExcelExport.DataSource = lstUsers;
        gvUserExcelExport.DataBind();
        //--------------

       
        //Session["PendingUsersOverSearch"] = null;
    }

    //Add By Abhinav -- 18 July 2013 -- To get users from existing Viewstste
    private List<SCT_UserBE> GetUsers(SCT_UserBE oSCT_UserBE) {
        SCT_UserBAL oSCT_UserBAL = new SCT_UserBAL();        
        List<SCT_UserBE> lstUsers = new List<SCT_UserBE>();

        if (ViewState["lstUsers"] == null) {
            lstUsers = oSCT_UserBAL.GetUsers(oSCT_UserBE);

            oSCT_UserBAL = null;
        }
        else {
            lstUsers = (List<SCT_UserBE>)ViewState["lstUsers"];
        }
        return lstUsers;
    }
    //----------------------------//

    public override BusinessEntities.BaseBe[] SubSort(GridViewSortEventArgs e)
    {
        GridViewSortExp = e.SortExpression;
        if (SortDir == "ASC") {
            GridViewSortDirection = SortDirection.Ascending;
        }
        else {
            GridViewSortDirection = SortDirection.Descending;
        }
        return Utilities.GenericListHelper<SCT_UserBE>.SortList((List<SCT_UserBE>)ViewState["lstUsers"], e.SortExpression, GridViewSortDirection).ToArray();
    }

    private void bindOtherSettings(int UserID, int UserRoleID)
    {
        SCT_UserBE oSCT_UserBE = new SCT_UserBE();
        oSCT_UserBE.Action = "GetCountrySpecificSettings";
        oSCT_UserBE.UserID = UserID;
        SCT_UserBAL oSCT_UserBAL = new SCT_UserBAL();
        lstUserSettings = oSCT_UserBAL.GetCountrySpecificSettingsBAL(oSCT_UserBE);

        foreach (SCT_UserBE item in lstUserSettings)
        {
            if (lstCountrySettings.FindAll(x => x.CountryId == item.CountryId && x.VendorID == item.VendorID).Count <= 0)
            {
                lstCountrySettings.Add(item);
            }
        }

        bindCountry();

        if (UserRoleID == 2) //Vendor
        {
            pnlVendorSction.Visible = true;
            pnlCarrierSction.Visible = false;

            rptVendorDetails.DataSource = lstCountrySettings;
            rptVendorDetails.DataBind();

            rptAppointmentCountry.DataSource = lstCountrySettings;
            rptAppointmentCountry.DataBind();

            rptDiscrepanciesCountry.DataSource = lstCountrySettings;
            rptDiscrepanciesCountry.DataBind();

            rptOtifCountry.DataSource = lstCountrySettings;
            rptOtifCountry.DataBind();

            rptScorecardCountry.DataSource = lstCountrySettings;
            rptScorecardCountry.DataBind();
        }
        else if (UserRoleID == 3) //Vendor
        {
            pnlVendorSction.Visible = false;
            pnlCarrierSction.Visible = true;

            rptCarrierDetails.DataSource = lstCountrySettings;
            rptCarrierDetails.DataBind();

            rptCarrierCountry.DataSource = lstCountrySettings;
            rptCarrierCountry.DataBind();
        }
    }

    private void bindCountry()
    {
        MAS_CountryBE oMAS_CountryBE = new MAS_CountryBE();
        MAS_CountryBAL oMAS_CountryBAL = new MAS_CountryBAL();

        oMAS_CountryBE.Action = "ShowAllCountry";
        oMAS_CountryBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
        oMAS_CountryBE.User.UserID = 0;

        List<MAS_CountryBE> lstCountry = new List<MAS_CountryBE>();
        lstCountry = oMAS_CountryBAL.GetCountryBAL(oMAS_CountryBE);

        rptCountry.DataSource = lstCountry;
        rptCountry.DataBind();
    }

    #endregion
}
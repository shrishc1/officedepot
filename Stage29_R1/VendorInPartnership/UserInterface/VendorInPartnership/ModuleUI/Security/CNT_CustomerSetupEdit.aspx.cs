﻿using BusinessEntities.ModuleBE.Security;
using BusinessLogicLayer.ModuleBAL.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebUtilities;

public partial class ModuleUI_Security_CustomerSetupEdit : CommonPage
{
    protected string deleteMessage = WebCommon.getGlobalResourceValue("DeleteMessage");
    protected void Page_Init(object sender, EventArgs e)
    {
        ucCountry.CurrentPage = this;

    }
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    #region Methods

    private void GetCarrier()
    {
        MASCNT_CustomerSetupBE oMASCNT_CustomerSetupBE = new MASCNT_CustomerSetupBE();
        MASCNT_CustomerSetupBAL oMASCNT_CustomerSetupBAL = new MASCNT_CustomerSetupBAL();


        if (GetQueryStringValue("CustomerID") != null)
        {

            oMASCNT_CustomerSetupBE.Action = "ShowAll";
            oMASCNT_CustomerSetupBE.CustomerID = Convert.ToInt32(GetQueryStringValue("CustomerID"));

            List<MASCNT_CustomerSetupBE> mASCNT_CustomerSetupBEs = oMASCNT_CustomerSetupBAL.GetCustomersBAL(oMASCNT_CustomerSetupBE);
           
            if (mASCNT_CustomerSetupBEs != null && mASCNT_CustomerSetupBEs.Count > 0)
            {
                ucCountry.innerControlddlCountry.SelectedIndex = ucCountry.innerControlddlCountry.Items.IndexOf(ucCountry.innerControlddlCountry.Items.FindByValue(mASCNT_CustomerSetupBEs[0].CountryID.ToString()));
                txtCustomerName.Text = Server.HtmlDecode(mASCNT_CustomerSetupBEs[0].CustomerName);               
            }

            ucCountry.innerControlddlCountry.Enabled = false;
            //txtCustomerName.Enabled = false;
        }
        else
        {

            btnDelete.Visible = false;
        }
    }

    #endregion
    #region Events


    protected void btnSave_Click(object sender, EventArgs e)
    {
        MASCNT_CustomerSetupBE oMASCNT_CustomerSetupBE = new MASCNT_CustomerSetupBE();
        MASCNT_CustomerSetupBAL oMASCNT_CustomerSetupBAL = new MASCNT_CustomerSetupBAL();

        oMASCNT_CustomerSetupBE.CustomerName = Server.HtmlEncode(txtCustomerName.Text.Trim());
        oMASCNT_CustomerSetupBE.CountryID = Convert.ToInt32(ucCountry.innerControlddlCountry.SelectedItem.Value);
        oMASCNT_CustomerSetupBE.UpdatedBy = Convert.ToInt32(Session["UserID"]);
        if (GetQueryStringValue("CustomerID") != null)
        {            
            oMASCNT_CustomerSetupBE.CustomerID = Convert.ToInt32(GetQueryStringValue("CustomerID"));
        }
        oMASCNT_CustomerSetupBE.Action = "CheckExistance";
        int? count = oMASCNT_CustomerSetupBAL.addEditCustomerDetailsBAL(oMASCNT_CustomerSetupBE);

        if (count > 0)
        {
            string errorMessage = WebCommon.getGlobalResourceValue("CustomerExists");
            Page.ClientScript.RegisterStartupScript(this.GetType(), "Message", "alert('" + errorMessage + "')", true);
        }
        else
        {
            if (GetQueryStringValue("CustomerID") != null)         
                oMASCNT_CustomerSetupBE.Action = "EditCustomer";        
            else
                oMASCNT_CustomerSetupBE.Action = "AddCustomer";

            oMASCNT_CustomerSetupBAL.addEditCustomerDetailsBAL(oMASCNT_CustomerSetupBE);
            EncryptQueryString("CNT_CustomerSetupOverview.aspx");
        }
    }
    protected void btnDelete_Click(object sender, EventArgs e)
    {
        MASCNT_CustomerSetupBE oMASCNT_CustomerSetupBE = new MASCNT_CustomerSetupBE();
        MASCNT_CustomerSetupBAL oMASCNT_CustomerSetupBAL = new MASCNT_CustomerSetupBAL();
        oMASCNT_CustomerSetupBE.Action = "DeleteCustomer";
        oMASCNT_CustomerSetupBE.UpdatedBy = Convert.ToInt32(Session["UserID"]);

        if (GetQueryStringValue("CustomerID") != null)
        {
            oMASCNT_CustomerSetupBE.CustomerID = Convert.ToInt32(GetQueryStringValue("CustomerID"));
            oMASCNT_CustomerSetupBAL.addEditCustomerDetailsBAL(oMASCNT_CustomerSetupBE);
        }
        EncryptQueryString("CNT_CustomerSetupOverview.aspx");
    }
    protected void btnBack_Click(object sender, EventArgs e)
    {
        EncryptQueryString("CNT_CustomerSetupOverview.aspx");
    }
    public override void CountrySelectedIndexChanged()
    {

    }

    public override void CountryPost_Load()
    {
        if (!IsPostBack)
        {
            GetCarrier();
        }
    }

    #endregion
}
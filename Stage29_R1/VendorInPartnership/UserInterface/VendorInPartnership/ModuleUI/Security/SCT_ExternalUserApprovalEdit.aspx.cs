﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.AdminFunctions;
using BusinessEntities.ModuleBE.Security;
using BusinessLogicLayer.ModuleBAL.AdminFunctions;
using BusinessLogicLayer.ModuleBAL.Security;
using Utilities;
using WebUtilities;
using System.Configuration;

public partial class ExternalUserApprovalEdit : CommonPage
{
    string sPortalLink = Convert.ToString(ConfigurationManager.AppSettings["Portallink"]) + @"/ModuleUI/Security/Login.aspx";
    string path = AppDomain.CurrentDomain.BaseDirectory.ToLower();
    string templatePath = @"/EmailTemplates/Generic/";

    List<SCT_UserBE> lstUserSettings = new List<SCT_UserBE>();
    List<SCT_UserBE> lstCountrySettings = new List<SCT_UserBE>();

    protected void Page_Init(object sender, EventArgs e)
    {

    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (GetQueryStringValue("ReadOnly") == "1")
        {
            pnlApprove.Enabled = false;
            pnlCarrierDetail.Enabled = false;
            pnlCarrierSction.Enabled = false;
            pnlCountrySettings.Enabled = false;
            pnlModules.Enabled = false;
            pnlPersonalDetails.Enabled = false;
            pnlSites.Enabled = false;
            pnlVendorDetail.Enabled = false;
            pnlVendorSction.Enabled = false;
            btnSave.Visible = false;
            btnProceed.Visible = true;
        }
        else
        {
            pnlApprove.Enabled = true;
            pnlCarrierDetail.Enabled = true;
            pnlCarrierSction.Enabled = true;
            pnlCountrySettings.Enabled = true;
            pnlModules.Enabled = true;
            pnlPersonalDetails.Enabled = true;
            pnlSites.Enabled = true;
            pnlVendorDetail.Enabled = true;
            pnlVendorSction.Enabled = true;
            btnSave.Visible = true;
            btnProceed.Visible = false;
        }
        if (GetQueryStringValue("UserID") != null)
        {
            bindUser(Convert.ToInt32(GetQueryStringValue("UserID")));
            bindOtherSettings();
        }
        else
        {
            btnSave.Visible = false;
        }
     

    }

    private void bindOtherSettings()
    {
        SCT_UserBE oSCT_UserBE = new SCT_UserBE();
        oSCT_UserBE.Action = "GetCountrySpecificSettings";
        oSCT_UserBE.UserID = Convert.ToInt32(GetQueryStringValue("UserID"));
        SCT_UserBAL oSCT_UserBAL = new SCT_UserBAL();
        lstUserSettings = oSCT_UserBAL.GetCountrySpecificSettingsBAL(oSCT_UserBE);

        foreach (SCT_UserBE item in lstUserSettings)
        {
            if (lstCountrySettings.FindAll(x => x.CountryId == item.CountryId && x.VendorID == item.VendorID).Count <= 0)
            {
                lstCountrySettings.Add(item);
            }
        }

        bindCountry();

        if (ViewState["UserRole"].ToString() == "2") //Vendor
        {
            pnlVendorSction.Visible = true;

            rptVendorDetails.DataSource = lstCountrySettings;
            rptVendorDetails.DataBind();

            rptAppointmentCountry.DataSource = lstCountrySettings;
            rptAppointmentCountry.DataBind();

            rptDiscrepanciesCountry.DataSource = lstCountrySettings;
            rptDiscrepanciesCountry.DataBind();

            rptInvoiceIssueCountry.DataSource = lstCountrySettings;
            rptInvoiceIssueCountry.DataBind();

            rptOtifCountry.DataSource = lstCountrySettings;
            rptOtifCountry.DataBind();

            rptScorecardCountry.DataSource = lstCountrySettings;
            rptScorecardCountry.DataBind();
        }
        else if (ViewState["UserRole"].ToString() == "3") //Vendor
        {
            pnlCarrierSction.Visible = true;

            rptCarrierDetails.DataSource = lstCountrySettings;
            rptCarrierDetails.DataBind();

            rptCarrierCountry.DataSource = lstCountrySettings;
            rptCarrierCountry.DataBind();
        }
    }

    private void bindCountry()
    {
        MAS_CountryBE oMAS_CountryBE = new MAS_CountryBE();
        MAS_CountryBAL oMAS_CountryBAL = new MAS_CountryBAL();

        oMAS_CountryBE.Action = "ShowAllCountry";
        oMAS_CountryBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
        oMAS_CountryBE.User.UserID = 0;

        List<MAS_CountryBE> lstCountry = new List<MAS_CountryBE>();
        lstCountry = oMAS_CountryBAL.GetCountryBAL(oMAS_CountryBE);

        rptCountry.DataSource = lstCountry;
        rptCountry.DataBind();
    }

    private void bindUser(int iUserID)
    {
        if (GetQueryStringValue("VendorID") != null)
            ViewState["VendorID"] = GetQueryStringValue("VendorID");
        if (GetQueryStringValue("ReadOnly") == "1")
        {
            btnSave.Visible = false;
        }
        else
        {
            btnSave.Visible = true;
        }
        
        SCT_UserBE oSCT_UserBE = new SCT_UserBE();
        SCT_UserBAL oSCT_UserBAL = new SCT_UserBAL();

        oSCT_UserBE.Action = "GetUser";
        oSCT_UserBE.UserID = iUserID;
        List<SCT_UserBE> lstSCT_UserBE = oSCT_UserBAL.GetUserBAL(oSCT_UserBE);
        oSCT_UserBAL = null;
        if (lstSCT_UserBE != null && lstSCT_UserBE.Count > 0)
        {
            oSCT_UserBE = null;
            oSCT_UserBE = lstSCT_UserBE[0];
            UclblUserId.Text = Convert.ToString(oSCT_UserBE.LoginID);
            for (int i = 0; i < oSCT_UserBE.Password.Length; i++)
                UclblPassword.Text += "*";
            UclblFirstName.Text = oSCT_UserBE.FirstName;
            UclblLastName.Text = oSCT_UserBE.Lastname;
            UclblTelephone.Text = oSCT_UserBE.PhoneNumber;
            UclblFax.Text = oSCT_UserBE.FaxNumber;
            UclblLanguage.Text = oSCT_UserBE.Language;
            UclblRole.Text = oSCT_UserBE.RoleName;

            ViewState["UserRole"] = oSCT_UserBE.UserRoleID.ToString();

            // wheather to store loginid and password in viewstate ofr in session.
            ViewState["password"] = oSCT_UserBE.Password;
            ViewState["loginid"] = oSCT_UserBE.LoginID;
        }
        else
            btnSave.Visible = false;
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (GetQueryStringValue("UserID").IsNumeric())
        {
            SCT_UserBE oSCT_UserBE = new SCT_UserBE();
            SCT_UserBAL oSCT_UserBAL = new SCT_UserBAL();

            oSCT_UserBE.Action = "ApproveOrRejectUser";
            oSCT_UserBE.oStockPlannerGroupings = new StockPlannerGroupingsBE();
            if (GetQueryStringValue("UserID") != null)
            {
                oSCT_UserBE.UserID = Convert.ToInt32(GetQueryStringValue("UserID"));

                oSCT_UserBE.AccountStatus = rblApprovalStatus.SelectedValue == "approve" ? "Awaiting Activation" : "Request Rejected";
                oSCT_UserBE.RejectionComments = rblApprovalStatus.SelectedValue == "approve" ? null : txtRejectComments.Text;

                int? iResult = oSCT_UserBAL.UpdateAccountStatus(oSCT_UserBE);
                if (iResult != null && iResult > 0 || iResult == -1)
                {
                    // send communication mail from here. both for approval hand rejection.
                    SendCommunicationMail(UclblUserId.Text);

                    // Delete Permanantly
                    if (rblApprovalStatus.SelectedValue != "approve")
                    {
                        oSCT_UserBE.Action = "DeleteRejectedUser";
                        oSCT_UserBAL.addEditUserBAL(oSCT_UserBE);
                    }

                    oSCT_UserBAL = null;
                    if (iResult == 0)
                    {
                        EncryptQueryString("SCT_UsersOverview.aspx");
                    }

                    if (ViewState["VendorID"] != null)
                    {
                        if (GetQueryStringValue("VendorID") != null)
                            EncryptQueryString("../GlobalSettings/VendorEdit.aspx?VendorID=" + ViewState["VendorID"]);
                        else
                            EncryptQueryString("SCT_UsersOverview.aspx?NewLoginRequest=Y");//to show Registered Awaiting Records
                    }
                    else
                        EncryptQueryString("SCT_UsersOverview.aspx?NewLoginRequest=Y");
                }
            }
        }
    }

    private void SendCommunicationMail(string LoginID)
    {
        sendCommunicationCommon osendCommunicationCommon = new sendCommunicationCommon();

        string htmlBody = string.Empty;
        string mailSubjectText = string.Empty;
        if (rblApprovalStatus.SelectedValue == "approve")
        {
            htmlBody = getAcceptMailBody();
            mailSubjectText = "Request Approved";
        }
        else
        {
            htmlBody = getRejectionMailBody();
            mailSubjectText = "Request Rejected";
        }
        clsEmail oclsEmail = new clsEmail();
        oclsEmail.sendMail(LoginID, htmlBody, mailSubjectText, Convert.ToString(ConfigurationManager.AppSettings["fromAddress"]), true);
        Page.UICulture = Convert.ToString(Session["CultureInfo"]);
    }

    private string getAcceptMailBody()
    {
        string htmlBody = null;
        string Language = UclblLanguage.Text;
        sendCommunicationCommon osendCommunicationCommon = new sendCommunicationCommon();

        templatePath = path + templatePath;

        //string LanguageFile = "ExternalUserCreation." + Language + ".htm";
        //if (!System.IO.File.Exists(templatePath + LanguageFile.ToLower()))
        //{
        //    LanguageFile = "ExternalUserCreation." + "english.htm";
        //}
        string LanguageFile = "ExternalUserCreation.english.htm";

        #region Setting reason as per the language ...
        switch (Language)
        {
            case clsConstants.English:
                Page.UICulture = clsConstants.EnglishISO; // "en-US"; // English
                break;
            case clsConstants.French:
                Page.UICulture = clsConstants.FranceISO; // // France
                break;
            case clsConstants.German:
                Page.UICulture = clsConstants.GermanyISO; //"de"; // German
                break;
            case clsConstants.Dutch:
                Page.UICulture = clsConstants.NederlandISO; //"nl"; // Dutch
                break;
            case clsConstants.Spanish:
                Page.UICulture = clsConstants.SpainISO; //"es"; // Spanish
                break;
            case clsConstants.Italian:
                Page.UICulture = clsConstants.ItalyISO; //"it"; // Italy
                break;
            case clsConstants.Czech:
                Page.UICulture = clsConstants.CzechISO; //"cz"; // Czech
                break;
            default:
                Page.UICulture = clsConstants.EnglishISO; //"en-US"; // English
                break;
        }

        #endregion

        using (StreamReader sReader = new StreamReader(templatePath + LanguageFile.ToLower()))
        {
            htmlBody = sReader.ReadToEnd();
            // htmlBody = htmlBody.Replace("{logoInnerPath}", osendCommunicationCommon.getAbsolutePath());

            htmlBody = htmlBody.Replace("{Congratulations}", WebCommon.getGlobalResourceValue("Congratulations"));
            htmlBody = htmlBody.Replace("{ExternalUserCreationText1}", WebCommon.getGlobalResourceValue("ExternalUserCreationText1"));

            htmlBody = htmlBody.Replace("{UserName}", WebCommon.getGlobalResourceValue("UserName"));
            htmlBody = htmlBody.Replace("{usernameValue}", ViewState["loginid"].ToString());

            htmlBody = htmlBody.Replace("{ExternalUserCreationText2}", WebCommon.getGlobalResourceValue("ExternalUserCreationText2"));
            htmlBody = htmlBody.Replace("{ExternalUserCreationText3}", WebCommon.getGlobalResourceValue("ExternalUserCreationText3"));
            htmlBody = htmlBody.Replace("{ExternalUserCreationText4}", WebCommon.getGlobalResourceValue("ExternalUserCreationText4"));

            htmlBody = htmlBody.Replace("{ClickHereValue}", "<a href='" + sPortalLink + "'>" + sPortalLink + " </a>");

            htmlBody = htmlBody.Replace("{ExternalUserCreationText5}", WebCommon.getGlobalResourceValue("ExternalUserCreationText5"));
            htmlBody = htmlBody.Replace("{Yoursfaithfully}", WebCommon.getGlobalResourceValue("Yoursfaithfully"));
            htmlBody = htmlBody.Replace("{OfficeDepots}", WebCommon.getGlobalResourceValue("OfficeDepots"));            
        }
        return htmlBody;
    }

    private string getRejectionMailBody()
    {
        string htmlBody = null;
        string Language = UclblLanguage.Text;
        sendCommunicationCommon osendCommunicationCommon = new sendCommunicationCommon();

        templatePath = path + templatePath;

        //string LanguageFile = "ExternalUserRejection." + Language + ".htm";
        //if (!System.IO.File.Exists(templatePath + LanguageFile.ToLower()))
        //{
        //    LanguageFile = "ExternalUserRejection." + "english.htm";
        //}
        string LanguageFile = "ExternalUserRejection.english.htm";

        #region Setting reason as per the language ...
        switch (Language)
        {
            case clsConstants.English:
                Page.UICulture = clsConstants.EnglishISO; // "en-US"; // English
                break;
            case clsConstants.French:
                Page.UICulture = clsConstants.FranceISO; // // France
                break;
            case clsConstants.German:
                Page.UICulture = clsConstants.GermanyISO; //"de"; // German
                break;
            case clsConstants.Dutch:
                Page.UICulture = clsConstants.NederlandISO; //"nl"; // Dutch
                break;
            case clsConstants.Spanish:
                Page.UICulture = clsConstants.SpainISO; //"es"; // Spanish
                break;
            case clsConstants.Italian:
                Page.UICulture = clsConstants.ItalyISO; //"it"; // Italy
                break;
            case clsConstants.Czech:
                Page.UICulture = clsConstants.CzechISO; //"cz"; // Czech
                break;
            default:
                Page.UICulture = clsConstants.EnglishISO; //"en-US"; // English
                break;
        }

        #endregion

        using (StreamReader sReader = new StreamReader(templatePath + LanguageFile.ToLower()))
        {
            htmlBody = sReader.ReadToEnd();
            // htmlBody = htmlBody.Replace("{logoInnerPath}", osendCommunicationCommon.getAbsolutePath());

            htmlBody = htmlBody.Replace("{DearSirMadam}", WebCommon.getGlobalResourceValue("DearSirMadam"));
            htmlBody = htmlBody.Replace("{ExternalUserRejText1}", WebCommon.getGlobalResourceValue("ExternalUserRejText1"));

            htmlBody = htmlBody.Replace("{rejectioncommentsValue}", txtRejectComments.Text.Trim());

            htmlBody = htmlBody.Replace("{ExternalUserRejText2}", WebCommon.getGlobalResourceValue("ExternalUserRejText2"));
            htmlBody = htmlBody.Replace("{Yoursfaithfully}", WebCommon.getGlobalResourceValue("Yoursfaithfully"));
            htmlBody = htmlBody.Replace("{OfficeDepots}", WebCommon.getGlobalResourceValue("OfficeDepots"));
        }
        return htmlBody;
    }

    protected void rptCountry_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            if (lstUserSettings.FindAll(x => x.CountryId == ((MAS_CountryBE)e.Item.DataItem).CountryID).Count > 0)
            {
                CheckBox chkCountry = (CheckBox)e.Item.FindControl("chkCountry");
                chkCountry.Checked = true;
            }
        }
    }

    protected void rptAppointmentCountry_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Repeater rptAppointmentCountrySites = (Repeater)e.Item.FindControl("rptAppointmentCountrySites");
            List<SCT_UserBE> lstSites = lstUserSettings.FindAll(x => x.CountryId == ((SCT_UserBE)e.Item.DataItem).CountryId && x.SiteId != null).ToList();
            rptAppointmentCountrySites.DataSource = lstSites;
            rptAppointmentCountrySites.DataBind();
        }
    }

    protected void rptDiscrepanciesCountry_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Repeater rptDiscrepanciesCountrySites = (Repeater)e.Item.FindControl("rptDiscrepanciesCountrySites");
            List<SCT_UserBE> lstSites = lstUserSettings.FindAll(x => x.CountryId == ((SCT_UserBE)e.Item.DataItem).CountryId && x.SiteId != null).ToList();
            rptDiscrepanciesCountrySites.DataSource = lstSites;
            rptDiscrepanciesCountrySites.DataBind();
        }
    }

    protected void rptInvoiceIssueCountry_ItemDataBound(object sender, RepeaterItemEventArgs e) {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem) {
            Repeater rptInvoiceIssueCountrySites = (Repeater)e.Item.FindControl("rptInvoiceIssueCountrySites");
            List<SCT_UserBE> lstSites = lstUserSettings.FindAll(x => x.CountryId == ((SCT_UserBE)e.Item.DataItem).CountryId && x.SiteId != null).ToList();
            rptInvoiceIssueCountrySites.DataSource = lstSites;
            rptInvoiceIssueCountrySites.DataBind();
        }
    }

    protected void rptAppointmentCountrySites_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            CheckBox chkSite = (CheckBox)e.Item.FindControl("chkSite");
            if (((SCT_UserBE)e.Item.DataItem).SchedulingContact == 'Y')
            {
                chkSite.Checked = true;
            }
        }
    }

    protected void rptDiscrepanciesCountrySites_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            CheckBox chkSite = (CheckBox)e.Item.FindControl("chkSite");
            if (((SCT_UserBE)e.Item.DataItem).DiscrepancyContact == 'Y')
            {
                chkSite.Checked = true;
            }
        }
    }

    protected void rptInvoiceIssueCountrySites_ItemDataBound(object sender, RepeaterItemEventArgs e) {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem) {
            CheckBox chkSite = (CheckBox)e.Item.FindControl("chkSite");
            if (((SCT_UserBE)e.Item.DataItem).InvoiceIssueContact == 'Y') {
                chkSite.Checked = true;
            }
        }
    }

    protected void rptOtifCountry_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            CheckBox chkCountry = (CheckBox)e.Item.FindControl("chkCountry");
            if (((SCT_UserBE)e.Item.DataItem).OTIFContact == 'Y')
            {
                chkCountry.Checked = true;
            }
        }
    }

    protected void rptScorecardCountry_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            CheckBox chkCountry = (CheckBox)e.Item.FindControl("chkCountry");
            if (((SCT_UserBE)e.Item.DataItem).ScorecardContact == 'Y')
            {
                chkCountry.Checked = true;
            }
        }
    }

    protected void rptCarrierCountry_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Repeater rptCarrierCountrySites = (Repeater)e.Item.FindControl("rptCarrierCountrySites");
            List<SCT_UserBE> lstSites = lstUserSettings.FindAll(x => x.CountryId == ((SCT_UserBE)e.Item.DataItem).CountryId && x.SiteId != null).ToList();
            rptCarrierCountrySites.DataSource = lstSites;
            rptCarrierCountrySites.DataBind();
        }
    }

    protected void rptCarrierCountrySites_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            CheckBox chkSite = (CheckBox)e.Item.FindControl("chkSite");
            if (((SCT_UserBE)e.Item.DataItem).SchedulingContact == 'Y')
            {
                chkSite.Checked = true;
            }
        }
    }
    protected void btnProceed_Click(object sender, EventArgs e)
    {
        if (GetQueryStringValue("VendorID") != null)
            EncryptQueryString("../GlobalSettings/VendorEdit.aspx?ReadOnly=1&VendorID=" + ViewState["VendorID"]);
        else
            EncryptQueryString("SCT_UsersOverview.aspx?ReadOnly=1&NewLoginRequest=Y");
    }
}
﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    AutoEventWireup="true" CodeFile="SCT_RoleTemplateEdit.aspx.cs" Inherits="SCT_RoleTemplateEdit" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script type="text/javascript">
        $(document).ready(function () {
            var $hfScreenID = $('#hfSelScreenID').val();
            if ($hfScreenID.length != 0) {
                var $arr = $hfScreenID.split(',');
                $.each($arr, function (intIndex, objValue) {
                    var id = '#Sel' + objValue;
                    if ($(id).length == 0) {
                        id = '#Sel' + objValue;
                    }
                    $(id).attr('checked', 'checked');
                });
            }

            //Check - if Search Discrepancies screen is selected make          
            //Work list screen selected and disabled
            var hfSearchDiscrepancies = $('#hfSearchDiscrepancies').val();
            var hfWorkList = $('#hfWorkList').val();
            if ($('#' + hfSearchDiscrepancies).is(':checked')) {
                $('#' + hfWorkList).attr('checked', true);
                $('#' + hfWorkList).attr('disabled', true);
            }
            //////////////////////////////////////////////////
        });

        function btnSave_ClientClick() {
            var lstSelectedScreenID = new Array();
            $('input:checked').each(function () {
                if ($(this).attr('id').substring(0, 3) == 'Sel') {
                    if ($(this).attr('data-screen-id').toString() != "1000") {
                        lstSelectedScreenID.push($(this).attr('data-screen-id').toString());
                    }
                }
            });
            $('#hfSelScreenID').val(lstSelectedScreenID);
        }

        function checkreadcheckbox(obj, isDisabled) {
            $this = $(obj);
            var id = '#R' + $this.attr('data-screen-id');
            if ($this.is(':checked')) {
                $(id).attr('checked', 'checked').attr("disabled", true);
            }
            else {
                if (isDisabled == false) {
                    $(id).removeAttr('checked').removeAttr("disabled");
                }
            }

        }

        function CheckWorkList() {
            //alert("test");
            var hfSearchDiscrepancies = $('#hfSearchDiscrepancies').val();
            var hfWorkList = $('#hfWorkList').val();
            hfSearchDiscrepancies = '#' + hfSearchDiscrepancies;
            if ($(hfSearchDiscrepancies).is(':checked')) {
                $('#' + hfWorkList).attr('checked', true);
                $('#' + hfWorkList).attr('disabled', true);
            }
            else {
                $('#' + hfWorkList).attr('disabled', false);
            }
        }
          
         
        function confirmDelete() {
            return confirm('<%=deleteMessage%>');
        }
        
    
    </script>
    <h2>
        <cc1:ucLabel ID="lblTemplateEdit" runat="server" Text="Role Template Edit"></cc1:ucLabel>
    </h2>
    <div class="right-shadow">
        <div class="formbox">
            <table width="45%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                <tr>
                    <td style="width: 5%">
                    </td>
                    <td style="font-weight: bold;" width="28%">
                        <cc1:ucLabel runat="server" ID="lblTemplate" Width="150px" isRequired="true">Template</cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold;" width="5%" align="center">
                        :
                    </td>
                    <td style="font-weight: bold;" width="57%">
                        <cc1:ucTextbox runat="server" ID="txtTemplate" CssClass="inputbox" ClientIDMode="Static"
                            MaxLength="50" />
                        <asp:RequiredFieldValidator ID="rfvTemplateRquired" runat="server" ControlToValidate="txtTemplate"
                            ErrorMessage="Template name filed cannot be blank." ValidationGroup="save" Display="None" />
                        <asp:RegularExpressionValidator ID="revTemplateValidate" runat="server" ErrorMessage="please input alphabets only."
                            ControlToValidate="txtTemplate" ValidationExpression="^[a-zA-Z]+$"></asp:RegularExpressionValidator>
                    </td>
                    <td style="width: 5%">
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td style="font-weight: bold">
                        <cc1:ucLabel runat="server" ID="lblRoles" Width="150px">For Role Type</cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold;" align="center">
                        :
                    </td>
                    <td>
                        <cc1:ucDropdownList ID="ddlRoles" runat="server" />
                        <asp:HiddenField ID="hfSelScreenID" runat="server" ClientIDMode="Static" />
                        <asp:HiddenField ID="hfSearchDiscrepancies" runat="server" ClientIDMode="Static" />
                        <asp:HiddenField ID="hfWorkList" runat="server" ClientIDMode="Static" />
                    </td>
                </tr>
            </table>
            <asp:Literal runat="server" ID="ltMenu"></asp:Literal>
        </div>
    </div>
    <div class="bottom-shadow">
    </div>
    <div class="button-row">
        <asp:ValidationSummary runat="server" ID="vs" ShowMessageBox="true" ShowSummary="false"
            ValidationGroup="save" />
        <cc1:ucButton runat="server" Text="Save" ID="btnSave" OnClientClick="btnSave_ClientClick();"
            OnClick="btnSave_Click" CssClass="button" ValidationGroup="save" />
       
        <cc1:ucButton ID="btnDelete" runat="server" CssClass="button" Text="" OnClick="btnDelete_Click"
            CausesValidation="false" Visible="false" OnClientClick="return confirmDelete();"  />
       
        <cc1:ucButton ID="btnBack" runat="server" CssClass="button" Text="Back" CausesValidation="false"
            OnClick="btnBack_Click" />
    </div>
</asp:Content>

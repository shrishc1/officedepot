﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master" CodeFile="~/ModuleUI/Security/SCT_RoleTemplateOverview.aspx.cs"
    Inherits="SCT_RoleTemplateOverview" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="../../CommonUI/UserControls/ucAddButton.ascx" TagName="ucAddButton" TagPrefix="cc2" %>
<%@ Register Src="../../CommonUI/UserControls/ucExportToExcel.ascx" TagName="ucExportToExcel" TagPrefix="cc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h2>
        <cc1:ucLabel ID="lblRoleTemplateOverview" runat="server" Text="Role Template Overview"></cc1:ucLabel>
    </h2>
    <div class="button-row">
        <cc2:ucAddButton ID="btnAdd" runat="server" NavigateUrl="SCT_RoleTemplateEdit.aspx" />
        <cc2:ucExportToExcel ID="ucExportToExcel1" runat="server" />
    </div>
    <table width="100%">
        <tr>
            <td align="center" colspan="9">
                <cc1:ucGridView ID="UcGridView1" Width="100%" runat="server" CssClass="grid" OnSorting="SortGrid" AllowSorting="true">
                    <Columns>
                        <asp:TemplateField HeaderText="Template" SortExpression="TemplateName">
                            <HeaderStyle Width="20%" HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <asp:HyperLink ID="hpTemplate" runat="server" Text='<%#Eval("TemplateName") %>' 
                                 NavigateUrl='<%# EncryptQuery("SCT_RoleTemplateEdit.aspx?TemplateID="+ Eval("TemplateID")) %>'></asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="User Role" SortExpression="RoleName">
                            <HeaderStyle Width="80%" HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <asp:Label ID="lblUserRole" runat="server" Text='<%#Eval("RoleName") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </cc1:ucGridView>
            </td>
        </tr>
    </table>
</asp:Content>

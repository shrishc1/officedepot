﻿using BusinessEntities.ModuleBE.Security;
using BusinessLogicLayer.ModuleBAL.Security;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebUtilities;

public partial class ModuleUI_Security_SCT_SPNumberReports : CommonPage
{
    protected string RecordNotFound = WebCommon.getGlobalResourceValue("RecordNotFound");
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //pager1.PageSize = 200;
            //pager1.GenerateGoToSection = false;
            //pager1.GeneratePagerInfoSection = false;
        }
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (GetQueryStringValue("PreviousPage") != null)
            {
                if (Session["SPNumberReports"] != null)
                {
                    RetainSearchData();
                }
            }
        }
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        BindGridView();

    }

    public void BindGridView(int page = 1)
    {
        SCT_UserBAL oSCT_UserBAL = new SCT_UserBAL();
        SCT_UserBE oSCT_UserBE = new SCT_UserBE();

        oSCT_UserBE.Action = "GetSPNumberReport";

        oSCT_UserBE.SelectedStockPlannerIds = msStockPlanner.SelectedStockPlannerIDs;

        oSCT_UserBE.CountryIds = msCountry.SelectedCountryIDs;

        oSCT_UserBE.SPNumberUserIDs = MultiSelectSPNumber.SelectedSPNumberUserIDs;

        this.SetSession(oSCT_UserBE);

        DataSet dt = oSCT_UserBAL.GetSPNumberReportBAL(oSCT_UserBE);
        if (dt != null)
        {
            if (dt.Tables[0].Rows.Count > 0)
            {
                ViewState["DataSource"] = dt.Tables[0];
                grdBind.DataSource = dt.Tables[0];
                grdBind.DataBind();
                grdBindExport.DataSource = dt.Tables[0];
                grdBindExport.DataBind();
                divSummaryGrid.Visible = true;
                btnBack.Visible = true;
                //pager1.ItemCount = dt.Tables[0].Rows.Count;
                lblError.Text = "";
                lblError.Visible = false;
                tblSearch.Visible = false;
                btnExportToExcel.Visible = true;
                oSCT_UserBE = null;

            }
            else
            {
                grdBind.DataSource = null;
                grdBind.DataBind();
                lblError.Visible = true;
                lblError.Text = RecordNotFound;
                tblSearch.Visible = false;
                btnExportToExcel.Visible = false;
                divSummaryGrid.Visible = true;
                btnBack.Visible = true;
            }

        }
        else
        {
            grdBind.DataSource = null;
            grdBind.DataBind();
            lblError.Visible = true;
            lblError.Text = RecordNotFound;
            tblSearch.Visible = false;
            btnExportToExcel.Visible = false;
            btnBack.Visible = true;

        }
    }


    private void SetSession(SCT_UserBE oSCT_UserBE)
    {
        Session["SPNumberReports"] = null;
        Session.Remove("SPNumberReports");

        TextBox txt = (TextBox)msStockPlanner.FindControl("txtUserName");
        HiddenField hdnSelectedStockPlonner = msStockPlanner.FindControl("hiddenSelectedIDs") as HiddenField;
        HiddenField hiddenSelectedName = msStockPlanner.FindControl("hiddenSelectedName") as HiddenField;

        TextBox txtSPNumber = (TextBox)MultiSelectSPNumber.FindControl("txtSPNumber");
        HiddenField hiddenSelectedSPNumberIDs = MultiSelectSPNumber.FindControl("hiddenSelectedIDs") as HiddenField; 

        Hashtable htSPNumberReports = new Hashtable();
        htSPNumberReports.Add("SelectedSPIDs", hdnSelectedStockPlonner.Value);
        htSPNumberReports.Add("SelectedSPNames", hiddenSelectedName.Value);
        htSPNumberReports.Add("SelectedCountryIDs", oSCT_UserBE.CountryIds);
        htSPNumberReports.Add("SPNumberUserIDs", oSCT_UserBE.SPNumberUserIDs);
        htSPNumberReports.Add("SelectedSPUserIDs", msStockPlanner.SelectedStockPlannerUserIDs);
        htSPNumberReports.Add("SelectedSPName", txt.Text);
        htSPNumberReports.Add("txtSPNumber", txtSPNumber.Text);
        htSPNumberReports.Add("hiddenSelectedSPNumberIDs", hiddenSelectedSPNumberIDs.Value);
        Session["SPNumberReports"] = htSPNumberReports;
    }


    public void RetainSearchData()
    {
        Hashtable htSPNumberReports = (Hashtable)Session["SPNumberReports"];

        //*********** StockPlanner ***************
        string StockPlannerText = (htSPNumberReports.ContainsKey("SelectedSPName") && htSPNumberReports["SelectedSPName"] != null) ? htSPNumberReports["SelectedSPName"].ToString() : "";
        string StockPlannerID = (htSPNumberReports.ContainsKey("SelectedSPIDs") && htSPNumberReports["SelectedSPIDs"] != null) ? htSPNumberReports["SelectedSPIDs"].ToString() : "";
        string StockPlannerName = (htSPNumberReports.ContainsKey("SelectedSPNames") && htSPNumberReports["SelectedSPNames"] != null) ? htSPNumberReports["SelectedSPNames"].ToString() : "";
        ListBox lstRight = msStockPlanner.FindControl("ucLBRight") as ListBox;
        ListBox lstLeft = msStockPlanner.FindControl("ucLBLeft") as ListBox;
        string strDtockPlonnerId = string.Empty;
        lstRight.Items.Clear();

        if (lstLeft != null && lstRight != null && !string.IsNullOrEmpty(StockPlannerID))
            lstRight.Items.Clear();
        msStockPlanner.SearchStockPlannerClick(StockPlannerText);
        {
            string[] strStockPlonnerIDs = StockPlannerID.Split(',');
            for (int index = 0; index < strStockPlonnerIDs.Length; index++)
            {
                ListItem listItem = lstLeft.Items.FindByValue(strStockPlonnerIDs[index]);
                if (listItem != null)
                {
                    strDtockPlonnerId += strStockPlonnerIDs[index] + ",";
                    lstRight.Items.Add(listItem);
                    lstLeft.Items.Remove(listItem);
                }

            }
            HiddenField hdnSelectedStockPlonner = msStockPlanner.FindControl("hiddenSelectedIDs") as HiddenField;
            hdnSelectedStockPlonner.Value = strDtockPlonnerId;
        }

        ////////************************* StockPlannerNumbers ***************************
        //string txtSPNumber = (htSPNumberReports.ContainsKey("txtSPNumber") && htSPNumberReports["txtSPNumber"] != null) ? htSPNumberReports["txtSPNumber"].ToString() : "";
        //string hiddenSelectedIDs = (htSPNumberReports.ContainsKey("hiddenSelectedSPNumberIDs") && htSPNumberReports["hiddenSelectedSPNumberIDs"] != null) ? htSPNumberReports["hiddenSelectedSPNumberIDs"].ToString() : "";
        ////string StockPlannerName = (htSPNumberReports.ContainsKey("SelectedSPNames") && htSPNumberReports["SelectedSPNames"] != null) ? htSPNumberReports["SelectedSPNames"].ToString() : "";
        //ListBox lstRightSPNumbers = MultiSelectSPNumber.FindControl("lstRight") as ListBox;
        //ListBox lstLeftSPNumbers = MultiSelectSPNumber.FindControl("lstLeft") as ListBox;
        //string strStockPlonnerId = string.Empty;
        //lstRightSPNumbers.Items.Clear();
        //MultiSelectSPNumber.setStockPlannerOnPostBack();


        //if (lstLeftSPNumbers != null && lstRightSPNumbers != null && !string.IsNullOrEmpty(hiddenSelectedIDs))
        //    lstRightSPNumbers.Items.Clear();
        //MultiSelectSPNumber.SearchStockPlannerByNumberClick(txtSPNumber);
        //{
        //    string[] strStockPlonnerIDs = hiddenSelectedIDs.Split(',');
        //    for (int index = 0; index < strStockPlonnerIDs.Length; index++)
        //    {
        //        ListItem listItem = lstLeftSPNumbers.Items.FindByValue(strStockPlonnerIDs[index]);
        //        if (listItem != null)
        //        {
        //            strStockPlonnerId += strStockPlonnerIDs[index] + ",";
        //            lstRightSPNumbers.Items.Add(listItem);
        //            lstLeftSPNumbers.Items.Remove(listItem);
        //        }

        //    }
        //    //HiddenField hdnSelectedStockPlonner = MultiSelectSPNumber.FindControl("hiddenSelectedIDs") as HiddenField;
        //    //hdnSelectedStockPlonner.Value = strStockPlonnerId;
        //}



        //*********** Country ***************
        string CountryIDs = (htSPNumberReports.ContainsKey("SelectedCountryIDs") && htSPNumberReports["SelectedCountryIDs"] != null) ? htSPNumberReports["SelectedCountryIDs"].ToString() : "";
        ListBox lstRightCountry = msCountry.FindControl("lstRight") as ListBox;
        ListBox lstLeftCountry = msCountry.FindControl("lstLeft") as ListBox;
        string msCountryid = string.Empty;
        lstRightCountry.Items.Clear();
        //lstRightSite.Items.Clear();
        msCountry.SelectedCountryIDs = "";
        msCountry.BindCountry();
        if (lstLeftCountry != null && lstRightCountry != null && !string.IsNullOrEmpty(CountryIDs))
        {
            //lstLeftSite.Items.Clear();
            lstRightCountry.Items.Clear();
            // msCountry.BindCountry();

            string[] strCountryIDs = CountryIDs.Split(',');
            for (int index = 0; index < strCountryIDs.Length; index++)
            {
                ListItem listItem = lstLeftCountry.Items.FindByValue(strCountryIDs[index]);
                if (listItem != null)
                {
                    msCountryid = msCountryid + strCountryIDs[index].ToString() + ",";
                    lstRightCountry.Items.Add(listItem);
                    lstLeftCountry.Items.Remove(listItem);
                }

            }
            if (string.IsNullOrEmpty(msCountry.SelectedCountryIDs))
                msCountry.SelectedCountryIDs = msCountryid.Trim(',');

        }
    }



    protected void btnExport_Click(object sender, EventArgs e)
    {

        WebCommon.ExportHideHidden("SPNumberReports", grdBindExport, null, true, true);
        // ucExportToExcel1.FileName = "Site_SummaryView";

    }

    protected void grdBind_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
                var URL = "";
                dynamic firstCell = e.Row.Cells[1];
                firstCell.Controls.Clear();

                    string firstCellValue = Convert.ToString(e.Row.Cells[0].Text);
                    URL = EncryptQuery("~/ModuleUI/Security/SCT_UserEdit.aspx?PreviousPage=SPNumberReports" + "&UserID=" + Convert.ToString(firstCellValue));

                    firstCell.Controls.Add(new HyperLink
                    {
                        NavigateUrl = URL,
                        Target = "_blank",
                        Text = firstCell.Text
                    });
        }
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        divSummaryGrid.Visible = false;
        btnBack.Visible = false;
        tblSearch.Visible = true;        
         

        EncryptQueryString("SCT_SPNumberReports.aspx?PreviousPage=1");

       

    }

    //public void pager_Command(object sender, CommandEventArgs e)
    //{
    //    int currnetPageIndx = Convert.ToInt32(e.CommandArgument);
    //    pager1.CurrentIndex = currnetPageIndx;
    //    BindGridView(currnetPageIndx);
    //}

    protected void grdBind_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            e.Row.Cells[0].Visible = false; // hides the first column 
        }
        if (e.Row.RowType == DataControlRowType.Header)
        {
            e.Row.Cells[0].Visible = false; // hides the first column
        }
    }
    protected void grdBindExport_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            e.Row.Cells[0].Visible = false; // hides the first column            
        }
        if (e.Row.RowType == DataControlRowType.Header)
        {
            e.Row.Cells[0].Visible = false; // hides the first column
        }
    }


    protected void OnPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        divSummaryGrid.Visible = true;
        btnBack.Visible = true;
        lblError.Text = "";
        lblError.Visible = false;
        btnExportToExcel.Visible = true;
        grdBind.PageIndex = e.NewPageIndex;
        grdBind.DataSource = (DataTable)ViewState["DataSource"];
        grdBind.DataBind();

    }

    

}
﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.AdminFunctions;
using BusinessEntities.ModuleBE.Appointment.CountrySetting;
using BusinessEntities.ModuleBE.Security;
using BusinessLogicLayer.ModuleBAL.AdminFunctions;
using BusinessLogicLayer.ModuleBAL.Appointment.CountrySetting;
using BusinessLogicLayer.ModuleBAL.Security;
using Utilities;
using WebUtilities;
using System.Configuration;
using System.Threading;
using System.Globalization;
using BusinessLogicLayer.ModuleBAL.Appointment.SiteSettings;
using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using BaseControlLibrary;
using System.Collections.Specialized;


public partial class SCT_RegisterExternalUser : CommonPage
{
    #region Private members
    protected string RequestConfirmationMessage = WebCommon.getGlobalResourceValue("RequestConfirmationMessage");
    private string UserExist = WebCommon.getGlobalResourceValue("UserExist");
    private string IsPasswordStrongMessage = WebCommon.getGlobalResourceValue("IsPasswordStrongMessage");
    private string MustSelectVendor = WebCommon.getGlobalResourceValue("MustSelectVendor");
    private string MustSelectCarrier = WebCommon.getGlobalResourceValue("MustSelectCarrier");
    private string SelectCountry = WebCommon.getGlobalResourceValue("SelectCountry");
    private string SelectCarrierForCountry = WebCommon.getGlobalResourceValue("SelectCarrierForCountry");
    string path = AppDomain.CurrentDomain.BaseDirectory.ToLower();
    string templatePath = @"/EmailTemplates/Generic/";
    protected string AlredySelectedVendor = WebCommon.getGlobalResourceValue("AlredySelectedVendor");
    public bool btnSearchGetClicked = false;


    private Repeater vendorDetail;
    public Repeater VendorDetail
    {
        get
        {
            return vendorDetail;
        }
        set
        {
            VendorDetail = value;
        }
    }
    const string constVendor = "Vendor";
    const string constCarrier = "Carrier";
    MAS_SiteBAL oMAS_SiteBAL = null;
    MAS_SiteBE oMAS_SiteBE = null;
    #endregion

    protected void Page_Init(object sender, EventArgs e)
    {
        lblUserIdEmailLitral.Text = WebCommon.getGlobalResourceValue("UserIdEmailLitral");

        if (GetQueryStringValue("ActiveViewIndex") != null) {
            mvExternalUserEdit.ActiveViewIndex = 1;
            ViewState["LanguageId"] = GetQueryStringValue("LangID");
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
       
        if (!IsPostBack)
        {
            if (Session["UserID"] == null)
            {
                Scorecard.Visible = false;
            }
            else
            {
                Scorecard.Visible = true;
            }
            //BindCountry();
            BindCountryCheckBoxList();
            //BindLanguage();
            BindCarrier();
            getUserDetail();
            txtOtherCarrier.Visible = false;
            rfvCarrierRequired.Enabled = false;
            SetCountryLabel();
            
        }
        if (GetQueryStringValue("UserID") != null)
        {
            mvExternalUserEdit.ActiveViewIndex = 1;
        }
        this.txtPassword.Attributes.Add("value", this.txtPassword.Text);
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        int? iResult = 0;
        SCT_UserBE oSCT_UserBE = new SCT_UserBE();
        SCT_UserBAL oSCT_UserBAL = new SCT_UserBAL();
        oSCT_UserBE.oStockPlannerGroupings = new StockPlannerGroupingsBE();
        if (!Common.IsPasswordStrong(txtPassword.Text))
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + IsPasswordStrongMessage + "');", true);
            return;
        }
        oSCT_UserBE.EmailId = txtUserIDEmail.Text.Trim();
        oSCT_UserBE.LoginID = txtUserIDEmail.Text.Trim();
        oSCT_UserBE.Password = txtPassword.Text;
        oSCT_UserBE.FirstName = txtFirstName.Text.Trim();
        oSCT_UserBE.Lastname = txtLastName.Text.Trim();
        oSCT_UserBE.PhoneNumber = txtTelephone.Text.Trim();
        oSCT_UserBE.FaxNumber = txtFax.Text.Trim();
        oSCT_UserBE.LanguageID = ddlLanguage.SelectedIndex > 0 ? Convert.ToInt32(ddlLanguage.SelectedItem.Value) : (int?)null;
        //Vendor Info
        //oSCT_UserBE.VendorID = ucSeacrhVendor1.VendorNo != "" ? Convert.ToInt32(ucSeacrhVendor1.VendorNo) : (int?)null;
        oSCT_UserBE.SchedulingContact = chkAppointmentScheduling.Checked ? 'Y' : 'N';
        oSCT_UserBE.DiscrepancyContact = chkDiscrepancies.Checked ? 'Y' : 'N';
        oSCT_UserBE.OTIFContact = chkOTIF.Checked ? 'Y' : 'N';
        oSCT_UserBE.ScorecardContact = chkScorecard.Checked ? 'Y' : 'N';
        oSCT_UserBE.UserRoleID = Convert.ToInt32(ViewState["UserRole"]);
        oSCT_UserBE.IsDeliveryRefusalAuthorization = 0;
        //Carrier Info
        oSCT_UserBE.CarrierID = ddlCarrier.SelectedIndex > 0 ? Convert.ToInt32(ddlCarrier.SelectedItem.Value) : (int?)null;
        oSCT_UserBE.OtherCarrierName = txtOtherCarrier.Text;
        if (ddlCarrier.SelectedIndex > 0)
        { // explicitly set scheduling contact as true in case of carrier
            oSCT_UserBE.SchedulingContact = 'Y';
        }

        if (GetQueryStringValue("UserID") == null)
        {
            if (CheckForUserExistance(txtUserIDEmail.Text.Trim()))
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + UserExist + "');", true);
                return;
            }

            oSCT_UserBE.IsActive = true;
            oSCT_UserBE.AccountStatus = "Registered Awaiting Approval";


            oSCT_UserBE.Action = "AddUser";
            iResult = oSCT_UserBAL.addEditUserBAL(oSCT_UserBE);
            if (iResult > 0)
            {
                SendExternalUserRequestRegistrationCommunicationMail(oSCT_UserBE.EmailId);
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + RequestConfirmationMessage + "');window.location.href='Login.aspx';", true);
                //EncryptQueryString("Login.aspx");
            }
        }
        else
        {
            oSCT_UserBE.UserID = Convert.ToInt32(GetQueryStringValue("UserID"));
            oSCT_UserBE.Action = "UpdateExternalUser";
            iResult = oSCT_UserBAL.UpdateUserBAL(oSCT_UserBE);
            if (iResult == 0)
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "showalert", "alert('" + RequestConfirmationMessage + "');window.location.href='Login.aspx';", true);
                //EncryptQueryString("Login.aspx");
            }
        }
        oSCT_UserBAL = null;

    }
    private bool CheckForUserExistance(string email)
    {
        SCT_UserBE oSCT_UserBE = new SCT_UserBE();
        oSCT_UserBE.EmailId = email;
        bool IsUserExist = false;
        if (oSCT_UserBE != null && !string.IsNullOrEmpty(oSCT_UserBE.EmailId))
        {
            oSCT_UserBE.Action = "CheckForExistance";
            SCT_UserBAL oSCT_UserBAL = new SCT_UserBAL();
            IsUserExist = oSCT_UserBAL.CheckForUserExistance(oSCT_UserBE);
            oSCT_UserBAL = null;
        }
        return IsUserExist;
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        if (GetQueryStringValue("UserID") != null)
        {
            EncryptQueryString("SCT_UsersOverview.aspx");
        }
        else
        {
            EncryptQueryString("Login.aspx");
        }
    }

    private void BindCountryDropDown()
    {
        MAS_CountryBE oMAS_CountryBE = new MAS_CountryBE();
        MAS_CountryBAL oMAS_CountryBAL = new MAS_CountryBAL();

        oMAS_CountryBE.Action = "ShowAllCountry";
        oMAS_CountryBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
        oMAS_CountryBE.User.UserID = 0;

        List<MAS_CountryBE> lstCountry = new List<MAS_CountryBE>();
        lstCountry = oMAS_CountryBAL.GetCountryBAL(oMAS_CountryBE);
        oMAS_CountryBAL = null;
        if (lstCountry.Count > 0)
        {
            FillControls.FillDropDown(ref ddlCountry, lstCountry.ToList(), "CountryName", "CountryID");
        }
        // ucSeacrhVendor1.CountryID = Convert.ToInt32(ddlCountry.SelectedItem.Value);
    }

    private void BindLanguage()
    {
        SCT_UserBE oSCT_UserBE = new SCT_UserBE();
        SCT_UserBAL oSCT_UserBAL = new SCT_UserBAL();

        oSCT_UserBE.Action = "GetLanguage";
        List<SCT_UserBE> lstLanguage = oSCT_UserBAL.GetLanguagesBAL(oSCT_UserBE);
        oSCT_UserBAL = null;
        if (lstLanguage.Count > 0)
        {
            FillControls.FillDropDown(ref ddlLanguage, lstLanguage, "Language", "LanguageID", "--Select--");
        }
    }

    private void BindCarrier()
    {
        ddlCarrier.Items.Clear();
        MASCNT_CarrierBE oMASCNT_CarrierBE = new MASCNT_CarrierBE();
        APPCNT_CarrierBAL oMASCNT_CarrierBAL = new APPCNT_CarrierBAL();
        oMASCNT_CarrierBE.Action = "ShowAll";
        if (ddlCountry.Items.Count > 0)
            oMASCNT_CarrierBE.CountryID = Convert.ToInt32(ddlCountry.SelectedItem.Value);

        List<MASCNT_CarrierBE> lstCarrier = oMASCNT_CarrierBAL.GetCarrierDetailsBAL(oMASCNT_CarrierBE);
        oMASCNT_CarrierBAL = null;
        FillControls.FillDropDown(ref ddlCarrier, lstCarrier, "CarrierName", "CarrierID", "--Select--");
    }

    protected void btnVendor_Click(object sender, EventArgs e)
    {
        //mvExternalUserEdit.ActiveViewIndex = 1;
        trVendor.Style.Add("display", "block");
        trVendorOptions.Style.Add("display", "block");
        trCarrier.Style.Add("display", "none");
        //btnSave.Visible = false;
        //btnBack.Visible = false;
        //BindLanguage();
        //BindCountry();
        rfvCarrierReq.Enabled = false;
        ViewState["UserRole"] = "2";
        //btnProceedRegisterExternalUser.Visible = true;
        btnProceed.Visible = true;
        //btnSave.Visible = false;
        //btnBack.Visible = false;
        pnlCountrySpecificSettings.Visible = true;
        pnlCountrySettings.Visible = false;
        mvExternalUserEdit.ActiveViewIndex = 2;
        txtPassword.Text = String.Empty;
    }
    protected void btnCarrier_Click(object sender, EventArgs e)
    {
        trVendor.Style.Add("display", "none");
        trVendorOptions.Style.Add("display", "none");
        trCarrier.Style.Add("display", "block");
        ViewState["UserRole"] = "3";
        pnlCountrySpecificSettings.Visible = false;
        pnlCountrySettings.Visible = true;
        mvExternalUserEdit.ActiveViewIndex = 2;
        this.BindCarrierCountryGrid();
        txtPassword.Text = String.Empty;
    }
    protected void ddlCountry_SelectedIndexChanged(object sender, EventArgs e)
    {
        //ucSeacrhVendor1.ClearSearch();
        //ucSeacrhVendor1.CountryID = Convert.ToInt32(ddlCountry.SelectedItem.Value);

        BindCarrier();
    }

    protected void btnBlockUser_Click(object sender, EventArgs e)
    {
        SCT_UserBE oSCT_UserBE = new SCT_UserBE();
        SCT_UserBAL oSCT_UserBAL = new SCT_UserBAL();
        oSCT_UserBE.Action = "UpdateAccountStatus";
        oSCT_UserBE.AccountStatus = "Blocked";
        oSCT_UserBE.UserID = Convert.ToInt32(GetQueryStringValue("UserID").ToString());
        int? iResult = oSCT_UserBAL.UpdateAccountStatus(oSCT_UserBE);
        oSCT_UserBAL = null;
        if (iResult != 0)
        {
            EncryptQueryString("SCT_UsersOverview.aspx");
        }
    }
    protected void btnUnblockUser_Click(object sender, EventArgs e)
    {
        SCT_UserBE oSCT_UserBE = new SCT_UserBE();
        SCT_UserBAL oSCT_UserBAL = new SCT_UserBAL();
        oSCT_UserBE.Action = "UpdateAccountStatus";
        oSCT_UserBE.AccountStatus = "Active";
        oSCT_UserBE.UserID = Convert.ToInt32(GetQueryStringValue("UserID").ToString());
        int? iResult = oSCT_UserBAL.UpdateAccountStatus(oSCT_UserBE);
        oSCT_UserBAL = null;
        if (iResult != 0)
        {
            SendUnblockCommunicationMail(txtUserIDEmail.Text, txtPassword.Text);
            EncryptQueryString("SCT_UsersOverview.aspx");
        }
    }

    protected void btnDeleteUser_Click(object sender, EventArgs e)
    {
        SCT_UserBE oSCT_UserBE = new SCT_UserBE();
        SCT_UserBAL oSCT_UserBAL = new SCT_UserBAL();
        oSCT_UserBE.Action = "DeleteUser";
        oSCT_UserBE.IsActive = false;
        oSCT_UserBE.UserID = Convert.ToInt32(GetQueryStringValue("UserID").ToString());
        oSCT_UserBE.oStockPlannerGroupings = new StockPlannerGroupingsBE();
        int? iResult = oSCT_UserBAL.addEditUserBAL(oSCT_UserBE);
        oSCT_UserBAL = null;
        if (iResult == 0)
        {
            EncryptQueryString("SCT_UsersOverview.aspx");
        }
    }

    private void getUserDetail()
    {
        if (GetQueryStringValue("UserID") != null)
        {
            //btnSave.Visible = true;
            //btnBack.Visible = true;

            SCT_UserBAL oSCT_UserBAL = new SCT_UserBAL();
            SCT_UserBE oSCT_UserBE = new SCT_UserBE();
            oSCT_UserBE.Action = "GetUserOverview";
            oSCT_UserBE.UserID = Convert.ToInt32(GetQueryStringValue("UserID").ToString());
            List<SCT_UserBE> lstUserDetail = oSCT_UserBAL.GetUsers(oSCT_UserBE);
            oSCT_UserBAL = null;
            if (lstUserDetail != null && lstUserDetail.Count > 0)
            {
                txtUserIDEmail.Text = lstUserDetail[0].LoginID;
                txtFirstName.Text = lstUserDetail[0].FirstName;
                txtLastName.Text = lstUserDetail[0].Lastname;
                txtPassword.TextMode = TextBoxMode.SingleLine;

                for (int i = 0; i < lstUserDetail[0].Password.Length; i++)
                    txtPassword.Text += "*";

                lblPassword.Visible = false;
                tdpassword.Visible = false;
                txtPassword.Visible = false;

                txtTelephone.Text = lstUserDetail[0].PhoneNumber;
                txtFax.Text = lstUserDetail[0].FaxNumber;
                if (ddlLanguage.Items.FindByValue(lstUserDetail[0].LanguageID.ToString()) != null)
                    ddlLanguage.SelectedValue = lstUserDetail[0].LanguageID.ToString();
                ddlCountry.Enabled = false;

                if (ddlCountry.Items.FindByValue(lstUserDetail[0].CountryId.ToString()) != null)
                    ddlCountry.SelectedValue = lstUserDetail[0].CountryId.ToString();

                ViewState["UserRole"] = lstUserDetail[0].UserRoleID.ToString();

                if (lstUserDetail[0].UserRoleID.ToString() == "2")//Vendor
                {
                    trVendor.Style.Add("display", "block");
                    trVendorOptions.Style.Add("display", "block");
                    trCarrier.Style.Add("display", "none");
                    rfvCarrierReq.Enabled = false;

                    //ltvendorName.Visible = true;
                    //ucSeacrhVendor1.Visible = false;
                    //ltvendorName.Text = lstUserDetail[0].VendorName.ToString();

                    chkAppointmentScheduling.Checked = lstUserDetail[0].SchedulingContact == 'Y' ? true : false;
                    chkDiscrepancies.Checked = lstUserDetail[0].DiscrepancyContact == 'Y' ? true : false;
                    chkOTIF.Checked = lstUserDetail[0].OTIFContact == 'Y' ? true : false;
                    chkScorecard.Checked = lstUserDetail[0].ScorecardContact == 'Y' ? true : false;



                }
                else if (lstUserDetail[0].UserRoleID.ToString() == "3")//Carrier
                {
                    trVendor.Style.Add("display", "none");
                    trVendorOptions.Style.Add("display", "none");
                    trCarrier.Style.Add("display", "block");
                    //cusvNoSelectedVendor.Enabled = false;
                }


                if (ddlCarrier.Items.FindByValue(lstUserDetail[0].CarrierID.ToString()) != null)
                    ddlCarrier.SelectedValue = lstUserDetail[0].CarrierID.ToString();
                ddlCarrier.Enabled = false;


                txtUserIDEmail.Enabled = false;
                txtPassword.Enabled = false;
                //Buttons Conditions in basis of Account Status
                if (lstUserDetail[0].AccountStatus.ToLower() == "blocked")
                {
                    //btnUnblockUser.Visible = true;
                    //btnDeleteUser.Visible = true;
                }
                if (lstUserDetail[0].AccountStatus.ToLower() == "active")
                {
                    //btnBlockUser.Visible = true;
                    //btnDeleteUser.Visible = true;
                }
                if (lstUserDetail[0].AccountStatus.ToLower() == "awaiting activation")
                {
                    //btnBlockUser.Visible = true;
                    //btnDeleteUser.Visible = true;
                }
                if (lstUserDetail[0].AccountStatus.ToLower() == "registered awaiting approval")
                {

                    //btnDeleteUser.Visible = true;
                }
            }
        }
    }

    #region mail



    private void SendExternalUserRequestRegistrationCommunicationMail(string EmailID)
    {
        sendCommunicationCommon osendCommunicationCommon = new sendCommunicationCommon();
        string htmlBody = getExternalUserRequestRegistrationMailBody();
        string mailSubjectText = "Request Registered with office depot";
        try
        {
            clsEmail oclsEmail = new clsEmail();
            oclsEmail.sendMail(EmailID, htmlBody, mailSubjectText, Convert.ToString(ConfigurationManager.AppSettings["fromAddress"]), true);
            Page.UICulture = Convert.ToString(Session["CultureInfo"]);
        }
        catch (Exception ex)
        {
            //osendCommunicationCommon.errorLog(0, mailSubjectText, EmailID, htmlBody, "Registration mail not sent.");
        }
    }

    private string getExternalUserRequestRegistrationMailBody()
    {
        string htmlBody = null;
        string Language = clsConstants.English; // ddlLanguage.SelectedItem.Text;
        sendCommunicationCommon osendCommunicationCommon = new sendCommunicationCommon();
        templatePath = path + templatePath;

        //string LanguageFile = "ExternalUserRequestRegistration." + Language + ".htm";
        //if (!System.IO.File.Exists(templatePath + LanguageFile.ToLower())) {
        //    LanguageFile = "ExternalUserRequestRegistration." + "english.htm";
        //}
        string LanguageFile = "ExternalUserRequestRegistration.english.htm";

        #region Setting reason as per the language ...
        switch (Language)
        {
            case clsConstants.English:
                Page.UICulture = clsConstants.EnglishISO; // "en-US"; // English
                break;
            case clsConstants.French:
                Page.UICulture = clsConstants.FranceISO; // // France
                break;
            case clsConstants.German:
                Page.UICulture = clsConstants.GermanyISO; //"de"; // German
                break;
            case clsConstants.Dutch:
                Page.UICulture = clsConstants.NederlandISO; //"nl"; // Dutch
                break;
            case clsConstants.Spanish:
                Page.UICulture = clsConstants.SpainISO; //"es"; // Spanish
                break;
            case clsConstants.Italian:
                Page.UICulture = clsConstants.ItalyISO; //"it"; // Italy
                break;
            case clsConstants.Czech:
                Page.UICulture = clsConstants.CzechISO; //"cz"; // Czech
                break;
            default:
                Page.UICulture = clsConstants.EnglishISO; //"en-US"; // English
                break;
        }

        #endregion

        using (StreamReader sReader = new StreamReader(templatePath + LanguageFile.ToLower()))
        {
            htmlBody = sReader.ReadToEnd();
            // htmlBody = htmlBody.Replace("{logoInnerPath}", osendCommunicationCommon.getAbsolutePath());

            htmlBody = htmlBody.Replace("{ExternalUserReqRegText1}", WebCommon.getGlobalResourceValue("ExternalUserReqRegText1"));
            htmlBody = htmlBody.Replace("{Yoursfaithfully}", WebCommon.getGlobalResourceValue("Yoursfaithfully"));
            htmlBody = htmlBody.Replace("{OfficeDepots}", WebCommon.getGlobalResourceValue("OfficeDepots"));
        }
        return htmlBody;
    }

    private void SendUnblockCommunicationMail(string EmailID, string password)
    {
        sendCommunicationCommon osendCommunicationCommon = new sendCommunicationCommon();

        string htmlBody = getUnblockMailBody(password);
        string mailSubjectText = "Account Unblocked";

        clsEmail oclsEmail = new clsEmail();
        oclsEmail.sendMail(EmailID, htmlBody, mailSubjectText, Convert.ToString(ConfigurationManager.AppSettings["fromAddress"]), true);
        Page.UICulture = Convert.ToString(Session["CultureInfo"]);
    }

    private string getUnblockMailBody(string password)
    {
        string htmlBody = null;
        string Language = ddlLanguage.SelectedItem.Text;
        sendCommunicationCommon osendCommunicationCommon = new sendCommunicationCommon();

        templatePath = path + templatePath;

        //string LanguageFile = "UnblockUserAccount." + Language + ".htm";
        //if (!System.IO.File.Exists(templatePath + LanguageFile.ToLower()))
        //{
        //    LanguageFile = "UnblockUserAccount." + "english.htm";
        //}
        string LanguageFile = "UnblockUserAccount.english.htm";

        #region Setting reason as per the language ...
        switch (Language)
        {
            case clsConstants.English:
                Page.UICulture = clsConstants.EnglishISO; // "en-US"; // English
                break;
            case clsConstants.French:
                Page.UICulture = clsConstants.FranceISO; // // France
                break;
            case clsConstants.German:
                Page.UICulture = clsConstants.GermanyISO; //"de"; // German
                break;
            case clsConstants.Dutch:
                Page.UICulture = clsConstants.NederlandISO; //"nl"; // Dutch
                break;
            case clsConstants.Spanish:
                Page.UICulture = clsConstants.SpainISO; //"es"; // Spanish
                break;
            case clsConstants.Italian:
                Page.UICulture = clsConstants.ItalyISO; //"it"; // Italy
                break;
            case clsConstants.Czech:
                Page.UICulture = clsConstants.CzechISO; //"cz"; // Czech
                break;
            default:
                Page.UICulture = clsConstants.EnglishISO; //"en-US"; // English
                break;
        }

        #endregion

        using (StreamReader sReader = new StreamReader(templatePath + LanguageFile.ToLower()))
        {
            htmlBody = sReader.ReadToEnd();
            // htmlBody = htmlBody.Replace("{logoInnerPath}", osendCommunicationCommon.getAbsolutePath());

            htmlBody = htmlBody.Replace("{Congratulations}", WebCommon.getGlobalResourceValue("Congratulations"));
            htmlBody = htmlBody.Replace("{UnblockUserAccText1}", WebCommon.getGlobalResourceValue("UnblockUserAccText1"));

            htmlBody = htmlBody.Replace("{password}", password);

            htmlBody = htmlBody.Replace("{Yoursfaithfully}", WebCommon.getGlobalResourceValue("Yoursfaithfully"));
            htmlBody = htmlBody.Replace("{OfficeDepots}", WebCommon.getGlobalResourceValue("OfficeDepots"));
        }
        return htmlBody;
    }




    #endregion
    protected void ddlCarrier_SelectedIndexChanged(object sender, EventArgs e)
    {
        MASCNT_CarrierBE oMASCNT_CarrierBE = new MASCNT_CarrierBE();
        APPCNT_CarrierBAL oMASCNT_CarrierBAL = new APPCNT_CarrierBAL();

        oMASCNT_CarrierBE.Action = "ShowAll";
        oMASCNT_CarrierBE.CarrierID = Convert.ToInt32(ddlCarrier.SelectedValue);
        if (ddlCountry.Items.Count > 0)
            oMASCNT_CarrierBE.CountryID = Convert.ToInt32(ddlCountry.SelectedValue);

        List<MASCNT_CarrierBE> lstCarrier = oMASCNT_CarrierBAL.GetCarrierDetailsBAL(oMASCNT_CarrierBE);
        oMASCNT_CarrierBAL = null;
        if (lstCarrier[0].IsNarrativeRequired)
        {
            txtOtherCarrier.Visible = true;
            rfvCarrierRequired.Enabled = true;
        }
        else
        {
            txtOtherCarrier.Text = string.Empty;
            txtOtherCarrier.Visible = false;
            rfvCarrierRequired.Enabled = false;
        }
    }

    // Sprint : 1 - Point : 16B
    private void BindCountryCheckBoxList()
    {
        MAS_CountryBE oMAS_CountryBE = new MAS_CountryBE();
        MAS_CountryBAL oMAS_CountryBAL = new MAS_CountryBAL();

        oMAS_CountryBE.Action = "ShowAllCountry";
        oMAS_CountryBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
        oMAS_CountryBE.User.UserID = 0;

        List<MAS_CountryBE> lstCountry = new List<MAS_CountryBE>();
        lstCountry = oMAS_CountryBAL.GetCountryBAL(oMAS_CountryBE);
        oMAS_CountryBAL = null;
        if (lstCountry.Count > 0)
        {
            FillControls.FillCheckBoxList(ref cblCountry, lstCountry.ToList(), "CountryName", "CountryID");
        }

        // Here need to set the check box list data...
        //ucSeacrhVendor1.CountryID = Convert.ToInt32(ddlCountry.SelectedItem.Value);
    }

    protected void btnProceed_Click(object sender, EventArgs e)
    {
        if (CheckForUserExistance(txtUserIDEmail.Text.Trim()))
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + UserExist + "');", true);
            return;
        }
        else if (!Common.IsPasswordStrong(txtPassword.Text))
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + IsPasswordStrongMessage + "');", true);
            return;
        }
        else if (!CheckVendorCountryStatus(cblCountry))
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + SelectCountry + "');", true);
            return;
        }
        else
        {
            hiddenPassword.Value = txtPassword.Text;
            if (string.IsNullOrWhiteSpace(txtPassword.Text)) { txtPassword.Attributes.Add("value", hiddenPassword.Value); }
            List<UP_VendorBE> olstUP_VendorBE = GetCountryCheckedData(cblCountry);
            ViewState["CountryVendorIndex"] = null;
            ViewState["CountryCheckedData"] = olstUP_VendorBE;
            if (olstUP_VendorBE.Count > 0)
            {
                ltVendorDetailsTextRegisterExternalUsers.Text = WebCommon.getGlobalResourceValue("VendorDetailsTextRegisterExternalUsers");
                rptVendorDetailsREU.DataSource = olstUP_VendorBE;
                rptVendorDetailsREU.DataBind();
                pnlVendorDetailsRegisterExternalUsers.Visible = true;
            }
        }

        pnlPersonalDetails.Visible = false;
        pnlCountrySpecificSettings.Visible = false;
        pnlVendorDetailsRegisterExternalUsers.Visible = true;
    }

    private bool CheckVendorCountryStatus(ucCheckboxList cblCountry)
    {
        bool blnStatus = false;
        for (int intIndex = 0; intIndex < cblCountry.Items.Count; intIndex++)
        {
            if (cblCountry.Items[intIndex].Selected)
            {
                blnStatus = true;
                break;
            }
        }
        return blnStatus;
    }

    private List<UP_VendorBE> GetCountryCheckedData(ucCheckboxList oucCheckboxList)
    {
        List<UP_VendorBE> olstUP_VendorBE = new List<UP_VendorBE>();
        UP_VendorBE oUP_VendorBE = null;

        for (int intIndex = 0; intIndex < oucCheckboxList.Items.Count; intIndex++)
        {
            oUP_VendorBE = new UP_VendorBE();
            if (oucCheckboxList.Items[intIndex].Selected)
            {
                oUP_VendorBE.CountryID = Convert.ToInt32(oucCheckboxList.Items[intIndex].Value);
                oUP_VendorBE.CountryName = Convert.ToString(oucCheckboxList.Items[intIndex].Text);
                olstUP_VendorBE.Add(oUP_VendorBE);
            }
        }
        return olstUP_VendorBE;
    }

    protected void btnEnglishCountrySpecSett_Click(object sender, ImageClickEventArgs e)
    {
        //this.SetRegionCulture("en-US");
        ViewState["LanguageId"] = 1;
        this.SetRegionCulture(clsConstants.EnglishISO);
       
    }
    protected void btnFranceCountrySpecSett_Click(object sender, ImageClickEventArgs e)
    {
        //this.SetRegionCulture("fr-FR");
        ViewState["LanguageId"] = 2;
        this.SetRegionCulture(clsConstants.FranceISO);
        
    }
    protected void btnGermanyCountrySpecSett_Click(object sender, ImageClickEventArgs e)
    {
        //this.SetRegionCulture("de-DE");
        ViewState["LanguageId"] = 3;
        this.SetRegionCulture(clsConstants.GermanyISO);
        
    }
    protected void btnSpainCountrySpecSett_Click(object sender, ImageClickEventArgs e)
    {
        //this.SetRegionCulture("es-MX");
        ViewState["LanguageId"] = 5;
        this.SetRegionCulture(clsConstants.SpainISO);
        
    }
    protected void btnItalyCountrySpecSett_Click(object sender, ImageClickEventArgs e)
    {
        //this.SetRegionCulture("it-IT");
        ViewState["LanguageId"] = 6;
        this.SetRegionCulture(clsConstants.ItalyISO);
        
    }
    protected void btnNederlandCountrySpecSett_Click(object sender, ImageClickEventArgs e)
    {
        //this.SetRegionCulture("nl-NL");
        ViewState["LanguageId"] = 4;
        this.SetRegionCulture(clsConstants.NederlandISO);
        
    }
    protected void btnCzechCountrySpecSett_Click(object sender, ImageClickEventArgs e)
    {
        //this.SetRegionCulture("cs-CZ");
        ViewState["LanguageId"] = 7;
        this.SetRegionCulture(clsConstants.CzechISO);
        
    }
    protected void btnSwedenCountrySpecSett_Click(object sender, ImageClickEventArgs e)
    {
        //this.SetRegionCulture("se-SE");
        this.SetRegionCulture(clsConstants.SwedenISO);
        //ViewState["LanguageId"] = 5;
    }

    private void SetRegionCulture(string culture)
    {
        //Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(culture);
        //Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture(culture);
        Page.UICulture = culture;
        Session["CultureInfo"] = culture;

        EncryptQueryString("SCT_RegisterExternalUser.aspx?ActiveViewIndex=1&LangID=" + Convert.ToInt32(ViewState["LanguageId"]));
        //mvExternalUserEdit.ActiveViewIndex = 1;
    }

    private void SetCountryLabel()
    {
        ltEnglishCountrySpecSett.Text = "Click here for <br /> English";
        ltFranceCountrySpecSett.Text = "Cliquez ici pour le <br /> Français";
        ltGermanyCountrySpecSett.Text = "Klicken Sie hier für <br /> die Deutsche";
        ltSpainCountrySpecSett.Text = "Haga clic aquí para <br /> Spainish";
        ltItalyCountrySpecSett.Text = "Clicca qui per <br /> l'italiano";
        ltNederlandCountrySpecSett.Text = "Klik hier voor de <br /> Nederlandse";
        lblCzechCountrySpecSett.Text = "Klikněte zde pro <br /> český";
    }

    private void BindVendor(ucListBox lstucListBox, string strVendorName, int intCountryId)
    {
        UP_VendorBAL oUP_VendorBAL = new UP_VendorBAL();
        UP_VendorBE oUP_VendorBE = new UP_VendorBE();

        oUP_VendorBE.Action = "GetVendorByVendorSearchText";
        oUP_VendorBE.CountryID = intCountryId;
        oUP_VendorBE.VendorName = strVendorName;
        List<UP_VendorBE> lstUPVendor = new List<UP_VendorBE>();
        lstUPVendor = oUP_VendorBAL.GetVendorByVendorSearchBAL(oUP_VendorBE);
        if (lstUPVendor.Count > 0)
        {
            FillControls.FillListBox(ref lstucListBox, lstUPVendor, "VendorName", "VendorID");
        }
        else
        {
            lstucListBox.Items.Clear();
        }
    }
    protected void rptVendorDetailsREU_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            ucLabel lblCountryVendorDetailsREU = (ucLabel)e.Item.FindControl("lblCountryVendorDetailsREU");
            HiddenField hdnCountryIdVendorDetailsREU = (HiddenField)e.Item.FindControl("hdnCountryIdVendorDetailsREU");
            ucSeacrhVendorWithoutSM ucSeacrhVendorWithoutSM1 = (ucSeacrhVendorWithoutSM)e.Item.FindControl("ucSeacrhVendorWithoutSM1");

            if (ViewState["CountryVendorIndex"] != null)
                ViewState["CountryVendorIndex"] = Convert.ToInt32(ViewState["CountryVendorIndex"]) + 1;
            else
                ViewState["CountryVendorIndex"] = 0;

            UP_VendorBE oUP_VendorBE = this.SetVendorDetailsREURepeaterData(Convert.ToInt32(ViewState["CountryVendorIndex"]));
            lblCountryVendorDetailsREU.Text = oUP_VendorBE.CountryName;
            hdnCountryIdVendorDetailsREU.Value = Convert.ToString(oUP_VendorBE.CountryID);
            // HiddenField CountryID = (HiddenField)e.Item.FindControl("hdnCountryid");
            // CountryID.Value = Convert.ToString(oUP_VendorBE.CountryID) ?? "0";
            //// HiddenField txtVendorNo = (HiddenField)e.Item.FindControl("txtVendorNo");
            // txtVendorNo.Value = Convert.ToString(oUP_VendorBE.CountryID) ?? "0";




            //string strVendorVal = lstVendor.SelectedItem.Value;
            //string strVendor = lstVendor.SelectedItem.Text;


            //hdnVendorName.Value = strVendor;
            //hdnVendorID.Value = strVendorVal.Trim();
            //List<UP_VendorBE> lstUPVendor = IsValidVendorCode();
            //if (lstUPVendor.Count > 0)
            //{
            //    hdnParentVendorID.Value = lstUPVendor[0].ParentVendorID.ToString();
            //}





            //if (ucSeacrhVendorWithoutSM1 != null)
            //{
            //    ucSeacrhVendorWithoutSM1.CurrentPage = this;
            //    //ucSeacrhVendorWithoutSM1.IsParentRequired = false;
            //    ucSeacrhVendorWithoutSM1.IsStandAloneRequired = true;
            //    ucSeacrhVendorWithoutSM1.IsChildRequired = true;
            //    //ucSeacrhVendorWithoutSM1.IsGrandParentRequired = false;


            //    ucSeacrhVendorWithoutSM1.IsMultiControlRepeater = true;
            //}
        }
    }

    private UP_VendorBE SetVendorDetailsREURepeaterData(int intIndex)
    {
        List<UP_VendorBE> olstUP_VendorBE = null;
        if (ViewState["CountryCheckedData"] != null)
            olstUP_VendorBE = (List<UP_VendorBE>)ViewState["CountryCheckedData"];

        UP_VendorBE oUP_VendorBE = olstUP_VendorBE[intIndex];
        return oUP_VendorBE;
    }

    protected void btnBack_1_Click(object sender, EventArgs e)
    {
        mvExternalUserEdit.ActiveViewIndex = 1;
    }
    protected void btnProceed_1_Click(object sender, EventArgs e)
    {
        if (!CheckVendorCountryStatus(cblCountry))
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + SelectCountry + "');", true);
            return;
        }
        else if (!CountryVendorEqualStatus())
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + MustSelectVendor + "');", true);
            return;
        }
        else if (!MustSelectVendorStatus())
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + MustSelectVendor + "');", true);
            return;
        }
        else
        {
            if (string.IsNullOrWhiteSpace(txtPassword.Text)) { txtPassword.Attributes.Add("value", hiddenPassword.Value); }
            mvExternalUserEdit.ActiveViewIndex = 3;
            if (ViewState["CountryCheckedData"] != null)
            {
                ViewState["AppointmentSchedulingIndex"] = null;
                dlAppointmentScheduling.DataSource = ViewState["CountryCheckedData"];
                dlAppointmentScheduling.DataBind();

                ViewState["DiscrepanciesIndex"] = null;
                dlDiscrepancies.DataSource = ViewState["CountryCheckedData"];
                dlDiscrepancies.DataBind();

                ViewState["InvoiceIssueIndex"] = null;
                dlInvoiceIssue.DataSource = ViewState["CountryCheckedData"];
                dlInvoiceIssue.DataBind();

                ViewState["OTIFIndex"] = null;
                dlOTIF.DataSource = ViewState["CountryCheckedData"];
                dlOTIF.DataBind();

                ViewState["ScorecardIndex"] = null;
                dlScorecard.DataSource = ViewState["CountryCheckedData"];
                dlScorecard.DataBind();
            }
            this.SetVendorDetailsRepeaterData();
        }
    }

    private bool CountryVendorEqualStatus()
    {
        bool blnCheck = false;
        List<UP_VendorBE> olstUP_VendorBE = GetCountryCheckedData(cblCountry);
        if (olstUP_VendorBE.Count == rptVendorDetailsREU.Items.Count)
        {
            blnCheck = true;
        }
        return blnCheck;
    }

    private bool MustSelectVendorStatus()
    {
        bool blnCheck = true;
        foreach (RepeaterItem item in rptVendorDetailsREU.Items)
        {
            if (item.ItemType == ListItemType.Item || item.ItemType == ListItemType.AlternatingItem)
            {
                TextBox txtVendorNo = (TextBox)item.FindControl("txtVendorNo");
              

                //ucSeacrhVendorWithoutSM ucSeacrhVendorWithoutSM1 = (ucSeacrhVendorWithoutSM)item.FindControl("ucSeacrhVendorWithoutSM1");
                //string VendorId = ucSeacrhVendorWithoutSM1.VendorNo;
                ////HiddenField hiddenSelectedIDs = (HiddenField)item.FindControl("hiddenSelectedIDs");
                //HiddenField hiddenSelectedName = (HiddenField)item.FindControl("hiddenSelectedName");
                //ucListBox lstRight = (ucListBox)item.FindControl("lstRight");
                //if (hiddenSelectedIDs != null)
                //{
                //    if (hiddenSelectedIDs.Value != string.Empty)
                //    {
                //        string[] splitIds = hiddenSelectedIDs.Value.Split(new char[] { ',' });
                //        if (splitIds.Length > 0)
                //        {
                //            VendorId = splitIds[0].Trim();
                //        }
                //    }
                //}

                if (string.IsNullOrWhiteSpace(txtVendorNo.Text))
                {
                    blnCheck = false;
                    break;
                }
            }
        }
        return blnCheck;
    }

    protected void dlAppointmentScheduling_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            ucLabel lblCountryAS = (ucLabel)e.Item.FindControl("lblCountryAS");
            HiddenField hdnCountryIdAS = (HiddenField)e.Item.FindControl("hdnCountryIdAS");
            ucCheckboxList cblAppointmentScheduling = (ucCheckboxList)e.Item.FindControl("cblAppointmentScheduling");
            if (lblCountryAS != null)
            {
                if (ViewState["AppointmentSchedulingIndex"] != null)
                    ViewState["AppointmentSchedulingIndex"] = Convert.ToInt32(ViewState["AppointmentSchedulingIndex"]) + 1;
                else
                    ViewState["AppointmentSchedulingIndex"] = 0;

                UP_VendorBE oUP_VendorBE = this.SetVendorDetailsREURepeaterData(Convert.ToInt32(ViewState["AppointmentSchedulingIndex"]));
                lblCountryAS.Text = oUP_VendorBE.CountryName;
                hdnCountryIdAS.Value = Convert.ToString(oUP_VendorBE.CountryID);
                this.BindCheckBoxListBasedOnCountryId(cblAppointmentScheduling, Convert.ToInt32(oUP_VendorBE.CountryID));
            }
        }
    }
    protected void dlDiscrepancies_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            ucLabel lblCountryD = (ucLabel)e.Item.FindControl("lblCountryD");
            HiddenField hdnCountryIdD = (HiddenField)e.Item.FindControl("hdnCountryIdD");
            ucCheckboxList cblDiscrepancies = (ucCheckboxList)e.Item.FindControl("cblDiscrepancies");
            if (lblCountryD != null)
            {
                if (ViewState["DiscrepanciesIndex"] != null)
                    ViewState["DiscrepanciesIndex"] = Convert.ToInt32(ViewState["DiscrepanciesIndex"]) + 1;
                else
                    ViewState["DiscrepanciesIndex"] = 0;

                UP_VendorBE oUP_VendorBE = this.SetVendorDetailsREURepeaterData(Convert.ToInt32(ViewState["DiscrepanciesIndex"]));
                lblCountryD.Text = oUP_VendorBE.CountryName;
                hdnCountryIdD.Value = Convert.ToString(oUP_VendorBE.CountryID);
                this.BindCheckBoxListBasedOnCountryId(cblDiscrepancies, Convert.ToInt32(oUP_VendorBE.CountryID));
            }
        }
    }

    protected void dlInvoiceIssue_ItemDataBound(object sender, DataListItemEventArgs e) {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem) {
            ucLabel lblCountryD = (ucLabel)e.Item.FindControl("lblCountryD");
            HiddenField hdnCountryIdD = (HiddenField)e.Item.FindControl("hdnCountryIdD");
            ucCheckboxList cblInvoiceIssue = (ucCheckboxList)e.Item.FindControl("cblInvoiceIssue");
            if (lblCountryD != null) {
                if (ViewState["InvoiceIssueIndex"] != null)
                    ViewState["InvoiceIssueIndex"] = Convert.ToInt32(ViewState["InvoiceIssueIndex"]) + 1;
                else
                    ViewState["InvoiceIssueIndex"] = 0;

                UP_VendorBE oUP_VendorBE = this.SetVendorDetailsREURepeaterData(Convert.ToInt32(ViewState["InvoiceIssueIndex"]));
                lblCountryD.Text = oUP_VendorBE.CountryName;
                hdnCountryIdD.Value = Convert.ToString(oUP_VendorBE.CountryID);
                this.BindCheckBoxListBasedOnCountryId(cblInvoiceIssue, Convert.ToInt32(oUP_VendorBE.CountryID));
            }
        }
    }

    private void BindCheckBoxListBasedOnCountryId(ucCheckboxList checkboxList, int countryId)
    {
        oMAS_SiteBAL = new MAS_SiteBAL();
        oMAS_SiteBE = new MAS_SiteBE();
        oMAS_SiteBE.Action = "GetAllSitesBasedCountry";
        oMAS_SiteBE.SiteCountryID = countryId;
        List<MAS_SiteBE> lstMAS_SiteBE = new List<MAS_SiteBE>();
        lstMAS_SiteBE = oMAS_SiteBAL.GetAllSitesBasedCountryBAL(oMAS_SiteBE);
        if (lstMAS_SiteBE.Count > 0)
        {
            checkboxList.DataSource = lstMAS_SiteBE;
            checkboxList.DataTextField = "SiteName";
            checkboxList.DataValueField = "SiteID";
            checkboxList.DataBind();
        }
    }

    protected void dlOTIF_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            //ucCheckbox cblOTIF = (ucCheckbox)e.Item.FindControl("cblOTIF");
            ucCheckboxList cblOTIF = (ucCheckboxList)e.Item.FindControl("cblOTIF");
            if (cblOTIF != null && ViewState["CountryCheckedData"] != null)
            {
                if (ViewState["OTIFIndex"] != null)
                    ViewState["OTIFIndex"] = Convert.ToInt32(ViewState["OTIFIndex"]) + 1;
                else
                    ViewState["OTIFIndex"] = 0;

                UP_VendorBE oUP_VendorBE = this.SetVendorDetailsREURepeaterData(Convert.ToInt32(ViewState["OTIFIndex"]));
                //cblOTIF.Text = oUP_VendorBE.CountryName;
                ListItem listItem = new ListItem(oUP_VendorBE.CountryName, Convert.ToString(oUP_VendorBE.CountryID));
                cblOTIF.Items.Add(listItem);
            }
        }
    }
    protected void dlScorecard_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            //ucCheckbox cblScorecard = (ucCheckbox)e.Item.FindControl("cblScorecard");
            ucCheckboxList cblScorecard = (ucCheckboxList)e.Item.FindControl("cblScorecard");
            if (cblScorecard != null)
            {
                if (ViewState["ScorecardIndex"] != null)
                    ViewState["ScorecardIndex"] = Convert.ToInt32(ViewState["ScorecardIndex"]) + 1;
                else
                    ViewState["ScorecardIndex"] = 0;

                UP_VendorBE oUP_VendorBE = this.SetVendorDetailsREURepeaterData(Convert.ToInt32(ViewState["ScorecardIndex"]));
                //cblScorecard.Text = oUP_VendorBE.CountryName;
                ListItem listItem = new ListItem(oUP_VendorBE.CountryName, Convert.ToString(oUP_VendorBE.CountryID));
                cblScorecard.Items.Add(listItem);
            }
        }
    }

    protected void btnBack_2_Click(object sender, EventArgs e)
    {
        mvExternalUserEdit.ActiveViewIndex = 2;
        pnlPersonalDetails.Visible = false;
        pnlCountrySpecificSettings.Visible = false;
        pnlVendorDetailsRegisterExternalUsers.Visible = true;
        if (string.IsNullOrWhiteSpace(txtPassword.Text)) { txtPassword.Attributes.Add("value", hiddenPassword.Value); }
    }
    protected void btnCompleteRegistration_Click(object sender, EventArgs e)
    {
        if (string.IsNullOrWhiteSpace(txtPassword.Text)) { txtPassword.Attributes.Add("value", hiddenPassword.Value); }

        if (CheckForUserExistance(txtUserIDEmail.Text.Trim()))
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + UserExist + "');", true);
            return;
        }
        else if (!Common.IsPasswordStrong(hiddenPassword.Value))
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + IsPasswordStrongMessage + "');", true);
            return;
        }
        else if (!VendorModuleCheckStatus())
        {
            string MustSelectVendorModule = WebCommon.getGlobalResourceValue("MustSelectVendorModule");
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + MustSelectVendorModule + "')", true);
        }
        else { this.SavePersonalDetail(constVendor); }
    }

    private bool VendorModuleCheckStatus()
    {
        List<UserCountryModulesSettings> lstUserCountryModulesSettings = new List<UserCountryModulesSettings>();
        UserCountryModulesSettings oUserCountryModulesSettings;
        int intIndex = 0;
        for (intIndex = 0; intIndex < cblCountry.Items.Count; intIndex++)
        {
            if (cblCountry.Items[intIndex].Selected)
            {
                oUserCountryModulesSettings = new UserCountryModulesSettings();
                oUserCountryModulesSettings.CountryID = Convert.ToInt32(cblCountry.Items[intIndex].Value);
                lstUserCountryModulesSettings.Add(oUserCountryModulesSettings);
            }
        }

        // Appointment Scheduling
        for (intIndex = 0; intIndex < dlAppointmentScheduling.Items.Count; intIndex++)
        {
            HiddenField hdnCountryIdAS = (HiddenField)dlAppointmentScheduling.Items[intIndex].FindControl("hdnCountryIdAS");
            ucCheckboxList cblAppointmentScheduling =
                (ucCheckboxList)dlAppointmentScheduling.Items[intIndex].FindControl("cblAppointmentScheduling");
            lstUserCountryModulesSettings.Find(x => x.CountryID == Convert.ToInt32(hdnCountryIdAS.Value)).IsSchedulingDefined = false;
            for (int index = 0; index < cblAppointmentScheduling.Items.Count; index++)
            {
                if (cblAppointmentScheduling.Items[index].Selected)
                {
                    lstUserCountryModulesSettings.Find(x => x.CountryID == Convert.ToInt32(hdnCountryIdAS.Value)).IsSchedulingDefined = true;
                }
            }
        }

        // Discrepancies
        for (intIndex = 0; intIndex < dlDiscrepancies.Items.Count; intIndex++)
        {
            HiddenField hdnCountryIdD = (HiddenField)dlDiscrepancies.Items[intIndex].FindControl("hdnCountryIdD");
            ucCheckboxList cblDiscrepancies = (ucCheckboxList)dlDiscrepancies.Items[intIndex].FindControl("cblDiscrepancies");
            lstUserCountryModulesSettings.Find(x => x.CountryID == Convert.ToInt32(hdnCountryIdD.Value)).IsDiscrepancyDefined = false;
            for (int index = 0; index < cblDiscrepancies.Items.Count; index++)
            {
                if (cblDiscrepancies.Items[index].Selected)
                {
                    lstUserCountryModulesSettings.Find(x => x.CountryID == Convert.ToInt32(hdnCountryIdD.Value)).IsDiscrepancyDefined = true;
                }
            }
        }

        //InvoiceIssue
        for (intIndex = 0; intIndex < dlInvoiceIssue.Items.Count; intIndex++) {
            HiddenField hdnCountryIdII = (HiddenField)dlInvoiceIssue.Items[intIndex].FindControl("hdnCountryIdD");
            ucCheckboxList cblInvoiceIssue = (ucCheckboxList)dlInvoiceIssue.Items[intIndex].FindControl("cblInvoiceIssue");
            lstUserCountryModulesSettings.Find(x => x.CountryID == Convert.ToInt32(hdnCountryIdII.Value)).IsInvoiceIssueDefined = false;
            for (int index = 0; index < cblInvoiceIssue.Items.Count; index++) {
                if (cblInvoiceIssue.Items[index].Selected) {
                    lstUserCountryModulesSettings.Find(x => x.CountryID == Convert.ToInt32(hdnCountryIdII.Value)).IsInvoiceIssueDefined = true;
                }
            }
        }

        // OTIF
        for (intIndex = 0; intIndex < dlOTIF.Items.Count; intIndex++)
        {
            ucCheckboxList cblOTIF = (ucCheckboxList)dlOTIF.Items[intIndex].FindControl("cblOTIF");
            if (cblOTIF != null && cblOTIF.Items[0].Selected)
            {
                lstUserCountryModulesSettings.Find(x => x.CountryID == Convert.ToInt32(cblOTIF.Items[0].Value)).IsOTIFDefined = true;
            }
        }

        // Scorecard
        for (intIndex = 0; intIndex < dlScorecard.Items.Count; intIndex++)
        {
            ucCheckboxList cblScorecard = (ucCheckboxList)dlScorecard.Items[intIndex].FindControl("cblScorecard");
            if (cblScorecard != null && cblScorecard.Items[0].Selected)
            {
                lstUserCountryModulesSettings.Find(x => x.CountryID == Convert.ToInt32(cblScorecard.Items[0].Value)).IsScorecardDefined = true;
            }
        }

        if (lstUserCountryModulesSettings.FindAll(x => x.IsSchedulingDefined == false
            && x.IsDiscrepancyDefined == false && x.IsOTIFDefined == false && x.IsScorecardDefined == false).Count > 0)
        {
            return false;
        }
        else { return true; }
    }

    private void SavePersonalDetail(string strSaveType)
    {
        #region Password Settings
        // In case of vendor.
        if (string.IsNullOrWhiteSpace(txtPassword.Text)) { txtPassword.Attributes.Add("value", hiddenPassword.Value); }
        // In case of carrier.
        if (string.IsNullOrWhiteSpace(hiddenPassword.Value)) { hiddenPassword.Value = txtPassword.Text; }
        #endregion

        int? intUserId = 0;
        SCT_UserBE oSCT_UserBE = new SCT_UserBE();
        SCT_UserBAL oSCT_UserBAL = new SCT_UserBAL();

        oSCT_UserBE.EmailId = txtUserIDEmail.Text.Trim();
        oSCT_UserBE.LoginID = txtUserIDEmail.Text.Trim();
        oSCT_UserBE.Password = hiddenPassword.Value;
        oSCT_UserBE.FirstName = txtFirstName.Text.Trim();
        oSCT_UserBE.Lastname = txtLastName.Text.Trim();
        oSCT_UserBE.PhoneNumber = txtTelephone.Text.Trim();
        oSCT_UserBE.FaxNumber = txtFax.Text.Trim();
        oSCT_UserBE.UserRoleID = Convert.ToInt32(ViewState["UserRole"]);
        oSCT_UserBE.LanguageID = Convert.ToInt32(ViewState["LanguageId"]);
        oSCT_UserBE.IsDeliveryRefusalAuthorization = 0;
        oSCT_UserBE.IsActive = true;
        oSCT_UserBE.AccountStatus = "Registered Awaiting Approval";
        oSCT_UserBE.Action = "RegisterNewUser";
        intUserId = oSCT_UserBAL.RegisterNewUserBAL(oSCT_UserBE);
        if (intUserId > 0)
        {
            this.SaveVendorCarrier(strSaveType, intUserId ?? 0);
            this.SendExternalUserRequestRegistrationCommunicationMail(oSCT_UserBE.EmailId);
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + RequestConfirmationMessage + "');window.location.href='Login.aspx';", true);
        }
    }

    private void SaveCountry(int intUserId)
    {
        // Save country sepcific details
        SCT_UserBE oSCT_UserBE = new SCT_UserBE();
        SCT_UserBAL oSCT_UserBAL = new SCT_UserBAL();

        oSCT_UserBE.Action = "UpdateCountries";
        oSCT_UserBE.UserID = intUserId;

        for (int intIndex = 0; intIndex < cblCountry.Items.Count; intIndex++)
        {
            if (cblCountry.Items[intIndex].Selected)
            {
                oSCT_UserBE.CountryIds += cblCountry.Items[intIndex].Value + ",";
            }
        }
        oSCT_UserBAL.UpdateCountriesBAL(oSCT_UserBE);
    }

    private void SaveVendorSettings(int intUserId)
    {
        // Save vendor/carrier details
        SCT_UserBAL oSCT_UserBAL = new SCT_UserBAL();
        SCT_UserBE oSCT_UserBE = new SCT_UserBE();
        oSCT_UserBE.Action = "UpdateUserCountry";
        oSCT_UserBE.UserID = intUserId;

        foreach (RepeaterItem item in rptVendorDetailsREU.Items)
        {
            if (item.ItemType == ListItemType.Item || item.ItemType == ListItemType.AlternatingItem)
            {
                HiddenField hdnCountryIdVendorDetailsREU = (HiddenField)item.FindControl("hdnCountryIdVendorDetailsREU");
                oSCT_UserBE.CountryId = Convert.ToInt32(hdnCountryIdVendorDetailsREU.Value);

                HiddenField hdnVendorID = (HiddenField)item.FindControl("hdnVendorID");

                string VendorId = hdnVendorID.Value;
                //HiddenField hiddenSelectedIDs = (HiddenField)item.FindControl("hiddenSelectedIDs");
                //HiddenField hiddenSelectedName = (HiddenField)item.FindControl("hiddenSelectedName");
                //ucListBox lstRight = (ucListBox)item.FindControl("lstRight");

                //if (hiddenSelectedIDs != null)
                //{
                //    if (hiddenSelectedIDs.Value != string.Empty)
                //    {
                //        string[] splitIds = hiddenSelectedIDs.Value.Split(new char[] { ',' });
                //        if (splitIds.Length > 0)
                //        {
                //            VendorId = splitIds[0].Trim();
                //        }
                //    }
                //}

                if (VendorId != string.Empty)
                {
                    int VendId;
                    if (Int32.TryParse(VendorId, out VendId))
                    {
                        oSCT_UserBE.VendorID = VendId;
                        oSCT_UserBAL.UpdateUserCountryBAL(oSCT_UserBE);
                    }
                }
            }
        }

    }

    private void SaveModule(int intUserId)
    {
        SCT_UserBAL oSCT_UserBAL = new SCT_UserBAL();
        SCT_UserBE oSCT_UserBE = new SCT_UserBE();
        oSCT_UserBE.Action = "UpdateModules";
        oSCT_UserBE.UserID = intUserId;

        int intIndex = 0;
        for (intIndex = 0; intIndex < dlAppointmentScheduling.Items.Count; intIndex++)
        {
            ucCheckboxList cblAppointmentScheduling = (ucCheckboxList)dlAppointmentScheduling.Items[intIndex].FindControl("cblAppointmentScheduling");
            for (int index = 0; index < cblAppointmentScheduling.Items.Count; index++)
            {
                if (cblAppointmentScheduling.Items[index].Selected)
                {
                    oSCT_UserBE.AppointmentSites += cblAppointmentScheduling.Items[index].Value + ",";
                }
            }
        }

        for (intIndex = 0; intIndex < dlDiscrepancies.Items.Count; intIndex++)
        {
            ucCheckboxList cblDiscrepancies = (ucCheckboxList)dlDiscrepancies.Items[intIndex].FindControl("cblDiscrepancies");
            for (int index = 0; index < cblDiscrepancies.Items.Count; index++)
            {
                if (cblDiscrepancies.Items[index].Selected)
                {
                    oSCT_UserBE.DiscrepancySites += cblDiscrepancies.Items[index].Value + ",";
                }
            }
        }

        for (intIndex = 0; intIndex < dlInvoiceIssue.Items.Count; intIndex++) {
            ucCheckboxList cblInvoiceIssue = (ucCheckboxList)dlInvoiceIssue.Items[intIndex].FindControl("cblInvoiceIssue");
            for (int index = 0; index < cblInvoiceIssue.Items.Count; index++) {
                if (cblInvoiceIssue.Items[index].Selected) {
                    oSCT_UserBE.InvoiceIssueSites += cblInvoiceIssue.Items[index].Value + ",";
                }
            }
        }

        for (intIndex = 0; intIndex < dlOTIF.Items.Count; intIndex++)
        {
            ucCheckboxList cblOTIF = (ucCheckboxList)dlOTIF.Items[intIndex].FindControl("cblOTIF");
            if (cblOTIF != null && cblOTIF.Items[0].Selected)
            {
                oSCT_UserBE.OTIFCountries += cblOTIF.Items[0].Value + ",";
            }
        }

        for (intIndex = 0; intIndex < dlScorecard.Items.Count; intIndex++)
        {
            ucCheckboxList cblScorecard = (ucCheckboxList)dlScorecard.Items[intIndex].FindControl("cblScorecard");
            if (cblScorecard != null && cblScorecard.Items[0].Selected)
            {
                oSCT_UserBE.ScorecardCountries += cblScorecard.Items[0].Value + ",";
            }
        }

        oSCT_UserBAL.UpdateModulesBAL(oSCT_UserBE);
    }

    protected void rptVendorDetailsREU_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        //if (e.CommandName == "SearchVendorDetailsREU")
        //{
        //    if (string.IsNullOrWhiteSpace(txtPassword.Text)) { txtPassword.Attributes.Add("value", hiddenPassword.Value); }
        //    ucPanel pnlSearchResult = (ucPanel)e.Item.FindControl("pnlSearchResult");
        //    ucListBox lstLeft = (ucListBox)e.Item.FindControl("lstLeft");
        //    ucTextbox txtSearchVendorDetailsREU = (ucTextbox)e.Item.FindControl("txtSearchVendorDetailsREU");
        //    HiddenField hdnCountryIdVendorDetailsREU = (HiddenField)e.Item.FindControl("hdnCountryIdVendorDetailsREU");
        //    ucSeacrhVendorWithoutSM ucSeacrhVendorWithoutSM1 = (ucSeacrhVendorWithoutSM)e.Item.FindControl("ucSeacrhVendorWithoutSM1");

        //    if (ucSeacrhVendorWithoutSM1 != null)
        //    {
        //        ucSeacrhVendorWithoutSM1.CurrentPage = this;
        //        ucSeacrhVendorWithoutSM1.IsParentRequired = true;
        //        ucSeacrhVendorWithoutSM1.IsStandAloneRequired = true;
        //        ucSeacrhVendorWithoutSM1.IsChildRequired = true;
        //        ucSeacrhVendorWithoutSM1.IsGrandParentRequired = true;
        //        ucSeacrhVendorWithoutSM1.CountryID = Convert.ToInt32(hdnCountryIdVendorDetailsREU.Value);
        //    }

        //    if (!string.IsNullOrWhiteSpace(txtSearchVendorDetailsREU.Text))
        //    {
        //        this.BindVendor(lstLeft, txtSearchVendorDetailsREU.Text, Convert.ToInt32(hdnCountryIdVendorDetailsREU.Value));
        //        pnlSearchResult.Visible = true;
        //    }

        //    this.SetVendorDetailsRepeaterData();
        //}
    }

    private void SetVendorDetailsRepeaterData()
    {
        //for (int intIndex = 0; intIndex < rptVendorDetailsREU.Items.Count; intIndex++)
        //{
        //    ucListBox lstRight = (ucListBox)rptVendorDetailsREU.Items[intIndex].FindControl("lstRight");
        //    HiddenField hiddenSearchedVendorText = (HiddenField)rptVendorDetailsREU.Items[intIndex].FindControl("hiddenSearchedVendorText");
        //    HiddenField hiddenSearchedVendorValue = (HiddenField)rptVendorDetailsREU.Items[intIndex].FindControl("hiddenSearchedVendorValue");

        //    if (lstRight != null && lstRight.Items.Count <= 0 && !string.IsNullOrWhiteSpace(hiddenSearchedVendorText.Value) &&
        //        !string.IsNullOrWhiteSpace(hiddenSearchedVendorValue.Value))
        //    {
        //        lstRight.Items.Add(new ListItem(hiddenSearchedVendorText.Value, hiddenSearchedVendorValue.Value));
        //    }
        //}
    }

    protected void rptCountrySettings_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            HiddenField hiddenRptCountryId = (HiddenField)e.Item.FindControl("hiddenRptCountryId");
            ucDropdownList ddlRptCarrier = (ucDropdownList)e.Item.FindControl("ddlRptCarrier");
            if (ddlRptCarrier != null)
            {
                if (GetAllCarriers(Convert.ToInt32(hiddenRptCountryId.Value)).Count > 0)
                {
                    FillControls.FillDropDown(ref ddlRptCarrier, GetAllCarriers(Convert.ToInt32(hiddenRptCountryId.Value)), "CarrierName", "CarrierID", "--Select--");
                }
            }
        }
    }

    private List<MASCNT_CarrierBE> GetAllCarriers(int countryID)
    {
        APPCNT_CarrierBAL oAPPCNT_CarrierBAL = new APPCNT_CarrierBAL();
        MASCNT_CarrierBE oMASCNT_CarrierBE = new MASCNT_CarrierBE();
        oMASCNT_CarrierBE.Action = "GetAllCarriers";
        oMASCNT_CarrierBE.CountryID = countryID;
        List<MASCNT_CarrierBE> lstMASCNT_CarrierBE = oAPPCNT_CarrierBAL.GetAllCarriersBAL(oMASCNT_CarrierBE);
        return lstMASCNT_CarrierBE;
    }

    private void BindCarrierCountryGrid()
    {
        MAS_CountryBE oMAS_CountryBE = new MAS_CountryBE();
        MAS_CountryBAL oMAS_CountryBAL = new MAS_CountryBAL();
        oMAS_CountryBE.Action = "ShowAllCountry";
        oMAS_CountryBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
        oMAS_CountryBE.User.UserID = 0;
        List<MAS_CountryBE> lstCountry = new List<MAS_CountryBE>();
        lstCountry = oMAS_CountryBAL.GetCountryBAL(oMAS_CountryBE);
        rptCountrySettings.DataSource = lstCountry;
        rptCountrySettings.DataBind();
    }

    protected void btnSave_1_Click(object sender, EventArgs e)
    {
        if (string.IsNullOrWhiteSpace(txtPassword.Text)) { txtPassword.Attributes.Add("value", hiddenPassword.Value); }

        if (CheckForUserExistance(txtUserIDEmail.Text.Trim()))
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + UserExist + "');", true);
            return;
        }
        else if (!Common.IsPasswordStrong(txtPassword.Text))
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + IsPasswordStrongMessage + "');", true);
            return;
        }
        else if (!CarrierCheckStatus())
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + SelectCountry + "');", true);
            return;
        }
        else if (!CarrierCheckAndSelectStatus())
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + MustSelectCarrier + "');", true);
            return;
        }
        else { this.SavePersonalDetail(constCarrier); }

    }

    private bool CarrierCheckStatus()
    {
        bool blnCheckStatus = false;
        for (int intIndex = 0; intIndex < rptCountrySettings.Items.Count; intIndex++)
        {
            ucCheckbox chkRptCountry = (ucCheckbox)rptCountrySettings.Items[intIndex].FindControl("chkRptCountry");
            ucDropdownList ddlRptCarrier = (ucDropdownList)rptCountrySettings.Items[intIndex].FindControl("ddlRptCarrier");
            if (chkRptCountry != null)
            {
                if (chkRptCountry.Checked == true)
                {
                    blnCheckStatus = true;
                    break;
                }
            }
        }
        return blnCheckStatus;
    }

    private bool CarrierCheckAndSelectStatus()
    {
        bool blnCheckSelectStatus = true;
        for (int intIndex = 0; intIndex < rptCountrySettings.Items.Count; intIndex++)
        {
            ucCheckbox chkRptCountry = (ucCheckbox)rptCountrySettings.Items[intIndex].FindControl("chkRptCountry");
            ucDropdownList ddlRptCarrier = (ucDropdownList)rptCountrySettings.Items[intIndex].FindControl("ddlRptCarrier");
            if (chkRptCountry != null && ddlRptCarrier != null)
            {
                if (chkRptCountry.Checked == true && ddlRptCarrier.Items.Count > 0)
                {
                    if (ddlRptCarrier.SelectedIndex == 0)
                    {
                        blnCheckSelectStatus = false;
                        break;
                    }
                }
                else if (chkRptCountry.Checked == true && ddlRptCarrier.Items.Count == 0)
                {
                    blnCheckSelectStatus = false;
                    break;
                }
            }
        }
        return blnCheckSelectStatus;
    }

    protected void btnBack_3_Click(object sender, EventArgs e)
    {
        mvExternalUserEdit.ActiveViewIndex = 1;
    }

    private void SaveCarrierCountrySettings(int intUserId)
    {
        // Save country sepcific details
        SCT_UserBE oSCT_UserBE = new SCT_UserBE();
        SCT_UserBAL oSCT_UserBAL = new SCT_UserBAL();

        oSCT_UserBE.Action = "UpdateCountries";
        oSCT_UserBE.UserID = intUserId;

        foreach (RepeaterItem ritem in rptCountrySettings.Items)
        {
            if (ritem.ItemType == ListItemType.Item || ritem.ItemType == ListItemType.AlternatingItem)
            {
                CheckBox chkCountry = (CheckBox)ritem.FindControl("chkRptCountry");
                if (chkCountry.Checked)
                {
                    HiddenField hdnCountryID = (HiddenField)ritem.FindControl("hiddenRptCountryId");
                    oSCT_UserBE.CountryIds += hdnCountryID.Value + ",";
                }
            }
        }
        oSCT_UserBAL.UpdateCountriesBAL(oSCT_UserBE);

        // Save vendor/carrier details
        oSCT_UserBE.Action = "UpdateUserCountry";
        foreach (RepeaterItem ritem in rptCountrySettings.Items)
        {
            if (ritem.ItemType == ListItemType.Item || ritem.ItemType == ListItemType.AlternatingItem)
            {
                CheckBox chkCountry = (CheckBox)ritem.FindControl("chkRptCountry");
                if (chkCountry.Checked)
                {
                    HiddenField hdnCountryID = (HiddenField)ritem.FindControl("hiddenRptCountryId");
                    oSCT_UserBE.CountryId = Convert.ToInt32(hdnCountryID.Value);
                    ucDropdownList ddlCarrier = (ucDropdownList)ritem.FindControl("ddlRptCarrier");
                    if (!string.IsNullOrWhiteSpace(ddlCarrier.SelectedValue))
                    {
                        oSCT_UserBE.CarrierID = Convert.ToInt32(ddlCarrier.SelectedValue);
                        oSCT_UserBAL.UpdateUserCountryBAL(oSCT_UserBE);
                    }
                }
            }
        }
    }

    private void SaveVendorCarrier(string strSaveType, int intUserId)
    {
        switch (strSaveType)
        {
            case "Vendor":
                this.SaveCountry(intUserId);
                this.SaveVendorSettings(intUserId);
                this.SaveModule(intUserId);
                break;
            case "Carrier":
                this.SaveCarrierCountrySettings(intUserId);
                break;
            default:
                break;
        }
    }

    protected void btnBack_4_Click(object sender, EventArgs e)
    {
        trVendor.Style.Add("display", "block");
        trVendorOptions.Style.Add("display", "block");
        trCarrier.Style.Add("display", "none");
        rfvCarrierReq.Enabled = false;
        ViewState["UserRole"] = "2";
        btnProceed.Visible = true;
        mvExternalUserEdit.ActiveViewIndex = 2;
        this.SetVendorDetailsRepeaterData();
        if (string.IsNullOrWhiteSpace(txtPassword.Text)) { txtPassword.Attributes.Add("value", hiddenPassword.Value); }
        pnlPersonalDetails.Visible = true;
        pnlCountrySpecificSettings.Visible = true;
        pnlVendorDetailsRegisterExternalUsers.Visible = false;
    }

    protected void txtVendorNo_TextChanged(object sender, EventArgs e)
    {
        TextBox tbox = (TextBox)sender;
        RepeaterItem row = (RepeaterItem)tbox.NamingContainer;
        TextBox txtVendorNo = (TextBox)row.FindControl("txtVendorNo");
        Label SelectedVendorName = (Label)row.FindControl("SelectedVendorName");
        HiddenField CountryID = (HiddenField)row.FindControl("hdnCountryIdVendorDetailsREU");

        HiddenField hdnVendorID = (HiddenField)row.FindControl("hdnVendorID");
        HiddenField hdnParentVendorID = (HiddenField)row.FindControl("hdnParentVendorID");

        hdnCountryID.Value = CountryID.Value;
        hdntxtVendorNo.Value = txtVendorNo.Text;

        if (txtVendorNo.Text.Trim() != string.Empty)
        {
            List<UP_VendorBE> lstNewUPVendor = new List<UP_VendorBE>();
            List<UP_VendorBE> lstUPVendor = new List<UP_VendorBE>();

            /// --- Added for Showing Inative Vendors --- 04 Mar 2013

            lstNewUPVendor = IsValidVendorCode();
            foreach (UP_VendorBE objVendor in lstNewUPVendor.ToList())
            {
                if (objVendor.IsActiveVendor == "Y")
                    lstUPVendor.Add(objVendor);
            }

            /// --- Added for Showing Inative Vendors --- 04 Mar 2013

            if (lstUPVendor.Count > 0)
            {
                hdnVendorID.Value = lstUPVendor[0].VendorID.ToString();
                hdnParentVendorID.Value = lstUPVendor[0].ParentVendorID.ToString();
                txtVendorNo.Text = lstUPVendor[0].Vendor_No.ToString();
                SelectedVendorName.Text = lstUPVendor[0].VendorName.ToString();
                hdnVendorName.Value = lstUPVendor[0].VendorName.ToString();

            }
            else
            {
                hdnVendorID.Value = string.Empty;
                hdnParentVendorID.Value = string.Empty;
                txtVendorNo.Text = string.Empty;
                SelectedVendorName.Text = string.Empty;
                hdnVendorName.Value = string.Empty;

                ScriptManager.RegisterStartupScript(this.UpVendorDetails, this.UpVendorDetails.GetType(), "alert", "alert('Please enter a valid Vendor Code.')", true);
                return;
            }
        }
        else
        {
            hdnVendorID.Value = string.Empty;
            hdnParentVendorID.Value = string.Empty;
            txtVendorNo.Text = string.Empty;
            SelectedVendorName.Text = string.Empty;
            hdnVendorName.Value = string.Empty;
        }
    }


    protected void btnSearch_Click(object sender, EventArgs e)
    {
        List<UP_VendorBE> lstUPVendor;
        List<UP_VendorBE> lstNewUPVendor = new List<UP_VendorBE>();
        btnSearchGetClicked = true;

        lstUPVendor = GetVendor();

        if (lstUPVendor.Count == 0)
        {
            string errorMeesage = WebCommon.getGlobalResourceValue("ValidVendorCode");
            ScriptManager.RegisterStartupScript(this.hdntxtVendorNo, this.hdntxtVendorNo.GetType(), "alert1", "alert('" + errorMeesage + "')", true);
        }
        /// --- Added for Showing Inative Vendors --- 04 Mar 2013

        foreach (UP_VendorBE objVendor in lstUPVendor.ToList())
        {
            if (objVendor.IsActiveVendor == "Y")
                lstNewUPVendor.Add(objVendor);
        }
        FillControls.FillListBox(ref lstVendor, lstNewUPVendor.ToList(), "VendorName", "VendorID");

        /// --- Added for Showing Inative Vendors --- 04 Mar 2013

        mdlVendorViewer.Show();
    }

    protected void btnSelect_Click(object sender, EventArgs e)
    {

        if (lstVendor.SelectedIndex >= 0)
        {
            for (int index = 0; index < this.rptVendorDetailsREU.Items.Count; index++)
            {
                if (index == Convert.ToInt32(hdnrowNumber.Value))
                {
                    string strVendorVal = lstVendor.SelectedItem.Value;
                    string strVendor = lstVendor.SelectedItem.Text;
                    Label SelectedVendorName = (this.rptVendorDetailsREU.Items[index].FindControl("SelectedVendorName") as Label);
                    TextBox txtVendorNo = (this.rptVendorDetailsREU.Items[index].FindControl("txtVendorNo") as TextBox);
                    HiddenField hdnVendorID = (this.rptVendorDetailsREU.Items[index].FindControl("hdnVendorID") as HiddenField);
                    HiddenField hdnParentVendorID = (this.rptVendorDetailsREU.Items[index].FindControl("hdnParentVendorID") as HiddenField);

                    SelectedVendorName.Text = strVendor;
                    txtVendorNo.Text = strVendor.Split('-')[0];
                    hdnVendorName.Value = strVendor;
                    hdnVendorID.Value = strVendorVal.Trim();
                    List<UP_VendorBE> lstUPVendor = IsValidVendorCode();
                    if (lstUPVendor.Count > 0)
                    {
                        hdnParentVendorID.Value = lstUPVendor[0].ParentVendorID.ToString();
                    }

                    lstVendor.Items.Clear();
                    mdlVendorViewer.Hide();
                    return;
                }
            }

        }
        else
        {
            mdlVendorViewer.Show();
        }


    }

    public List<UP_VendorBE> IsValidVendorCode()
    {
        return GetVendor();
    }



    protected void btnPopupCancel_Click(object sender, EventArgs e)
    {
        lstVendor.Items.Clear();
        txtVenderName2.Text = string.Empty;
        mdlVendorViewer.Hide();
    }

    private List<UP_VendorBE> GetVendor()
    {
        UP_VendorBAL oUP_VendorBAL = new UP_VendorBAL();
        UP_VendorBE oUP_VendorBE = new UP_VendorBE();

        oUP_VendorBE.Action = "SearchVendor";

        oUP_VendorBE.Site = new BusinessEntities.ModuleBE.AdminFunctions.MAS_SiteBE();
        if (btnSearchGetClicked == true)
        {
            oUP_VendorBE.Vendor_No = string.Empty;
        }
        else if (hdntxtVendorNo.Value.Trim() != "")
            oUP_VendorBE.Vendor_No = hdntxtVendorNo.Value.Trim() == string.Empty ? "" : hdntxtVendorNo.Value.Trim();
        else
            oUP_VendorBE.VendorName = txtVenderName2.Text.Trim();

        if (string.IsNullOrEmpty(oUP_VendorBE.VendorName) && string.IsNullOrEmpty(oUP_VendorBE.Vendor_No)) {
            oUP_VendorBE.VendorName = txtVenderName2.Text.Trim();
        }

        oUP_VendorBE.IsChildRequired = true;
        oUP_VendorBE.IsGrandParentRequired = false;
        oUP_VendorBE.IsParentRequired = false;
        oUP_VendorBE.IsStandAloneRequired = true;

        oUP_VendorBE.Site.SiteCountryID = Convert.ToInt32(hdnCountryID.Value);


        List<UP_VendorBE> lstUPVendor = new List<UP_VendorBE>();
        lstUPVendor = oUP_VendorBAL.GetCarrierDetailsBAL(oUP_VendorBE);
        return lstUPVendor;
    }


}

public class UserCountryModulesSettings
{
    public int CountryID { get; set; }
    public bool IsSchedulingDefined { get; set; }
    public bool IsDiscrepancyDefined { get; set; }
    public bool IsInvoiceIssueDefined { get; set; }
    public bool IsOTIFDefined { get; set; }
    public bool IsScorecardDefined { get; set; }
}
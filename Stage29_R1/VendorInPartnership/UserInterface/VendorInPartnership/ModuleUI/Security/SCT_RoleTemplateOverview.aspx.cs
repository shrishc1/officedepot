﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using BusinessLogicLayer.ModuleBAL.Appointment.SiteSettings;

using BusinessEntities.ModuleBE.Appointment.CountrySetting;
using BusinessLogicLayer.ModuleBAL.Appointment.CountrySetting;
using BusinessEntities.ModuleBE.Security;
using BusinessLogicLayer.ModuleBAL.Security;
public partial class SCT_RoleTemplateOverview : CommonPage 
{
    protected void Page_Load(object sender, EventArgs e)
    {
       
        if (!IsPostBack)
        {
            BindVendor();
        }
        ucExportToExcel1.GridViewControl = UcGridView1;
        ucExportToExcel1.FileName = "Template";
    }
    #region Methods

    protected void BindVendor() {

        SCT_TemplateBE oSCT_TemplateBE = new SCT_TemplateBE();
        SCT_TemplateBAL oSCT_TemplateBAL = new SCT_TemplateBAL();

        oSCT_TemplateBE.Action = "ShowTemplate";

        List<SCT_TemplateBE> lstTemplate = oSCT_TemplateBAL.GetTemplate(oSCT_TemplateBE);
        oSCT_TemplateBAL = null;
        if (lstTemplate.Count > 0)
        {
            UcGridView1.DataSource = lstTemplate;
            UcGridView1.DataBind();
            ViewState["lstTemplate"] = lstTemplate;
        }
    }
    public override BusinessEntities.BaseBe[] SubSort(GridViewSortEventArgs e)
    {
        return Utilities.GenericListHelper<SCT_TemplateBE>.SortList((List<SCT_TemplateBE>)ViewState["lstTemplate"], e.SortExpression, e.SortDirection).ToArray();
    }

    #endregion
}
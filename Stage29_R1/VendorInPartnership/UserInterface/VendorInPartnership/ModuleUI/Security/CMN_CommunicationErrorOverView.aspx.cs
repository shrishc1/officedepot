﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLogicLayer.ModuleBAL.AdminFunctions;
using BusinessEntities.ModuleBE.AdminFunctions;
using System.Web.Services;

public partial class ModuleUI_Security_CMN_CommunicationErrorOverView : CommonPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            BindGrid();
        }
    }

    private void BindGrid()
    {
        CMN_CommunicationErrorLogBAL oCMN_CommunicationErrorLogBAL = new CMN_CommunicationErrorLogBAL();
        CMN_CommunicationErrorLogBE oCMN_CommunicationErrorLogBE = new CMN_CommunicationErrorLogBE();
        oCMN_CommunicationErrorLogBE.Action = "GetFailCommunicationMail";
        List<CMN_CommunicationErrorLogBE> lstCommunicationError = oCMN_CommunicationErrorLogBAL.GetCommunicationErrorLog(oCMN_CommunicationErrorLogBE);
        oCMN_CommunicationErrorLogBAL = null;
        gvCommunicationError.DataSource = lstCommunicationError;
        gvCommunicationError.DataBind();
    }
    protected void btnClose_Click(object sender, CommandEventArgs e)
    {
        CMN_CommunicationErrorLogBAL oCMN_CommunicationErrorLogBAL = new CMN_CommunicationErrorLogBAL();
        CMN_CommunicationErrorLogBE oCMN_CommunicationErrorLogBE = new CMN_CommunicationErrorLogBE();
        oCMN_CommunicationErrorLogBE.Action = "UpdateStatus";
        oCMN_CommunicationErrorLogBE.Status = 'C';
        oCMN_CommunicationErrorLogBE.CommunicationErrorID = Convert.ToInt32(e.CommandArgument.ToString().Trim());
        oCMN_CommunicationErrorLogBAL.UpdateStatus(oCMN_CommunicationErrorLogBE);
        oCMN_CommunicationErrorLogBAL = null;
        BindGrid();
    }
}
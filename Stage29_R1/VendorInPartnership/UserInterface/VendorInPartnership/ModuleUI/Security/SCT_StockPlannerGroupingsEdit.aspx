﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    CodeFile="SCT_StockPlannerGroupingsEdit.aspx.cs" Inherits="SCT_StockPlannerGroupingsEdit"
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/ucExportToExcel.ascx" TagName="ucExportToExcel"
    TagPrefix="cc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script language="javascript" type="text/javascript">
        function confirmDelete() {
            return confirm('<%=deleteMessage%>');
        }
    </script>
    <h2>
        <cc1:ucLabel ID="lblStockPlannerGroupings" runat="server"></cc1:ucLabel>
    </h2>
    <asp:RequiredFieldValidator ID="rfvSPGRequired" runat="server" ControlToValidate="txtGroupName"
        Display="None" ValidationGroup="a">
    </asp:RequiredFieldValidator>
    <asp:ValidationSummary ID="vSummary" runat="server" ShowMessageBox="true" ShowSummary="false"
        Style="color: Red" ValidationGroup="a" />
    <div class="right-shadow">
        <div class="formbox">
            <table width="100%" cellspacing="5" cellpadding="0" class="form-table" id="tbldel"
                runat="server">
                <tr>
                    <td style="font-weight: bold;" width="14%">
                        <cc1:ucLabel ID="lblName" runat="server" Text="Name" isRequired="true"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold;" width="1%" align="center">
                        :
                    </td>
                    <td style="font-weight: bold;">
                        <cc1:ucTextbox ID="txtGroupName" runat="server" Width="220px" MaxLength="50"></cc1:ucTextbox>
                    </td>
                    <td id="tdShow" runat="server" style="display:none;">
                         <div class="button-row">                
                <cc2:ucExportToExcel ID="ucExportToExcel1" runat="server" />
            </div>
                    </td>
                </tr>
            </table>
            
            <table width="100%" cellspacing="5" cellpadding="0" id="tblshow"
                runat="server" style="display: none;">
                <tr>
                    <td>
                        <cc1:ucGridView ID="UcGridView1" Width="100%" runat="server" CssClass="grid" OnSorting="SortGrid"
                            AllowSorting="true">
                            <Columns>
                                <asp:TemplateField HeaderText="User Name" SortExpression="oUserBE.UserName">
                                    <HeaderStyle HorizontalAlign="Left" Width="50%" />
                                    <ItemStyle HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <cc1:ucLiteral ID="ltUserName" runat="server" Text='<%# Eval("oUserBE.UserName") %>'></cc1:ucLiteral>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </cc1:ucGridView>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div class="bottom-shadow">
    </div>
    <div class="button-row">
        <cc1:ucButton ID="btnSave" runat="server" Text="Save" CssClass="button" OnClick="btnSave_Click"
            ValidationGroup="a" />
        <cc1:ucButton ID="btnDelete" runat="server" Text="Delete" CssClass="button" OnClick="btnDelete_Click"
            OnClientClick="return confirmDelete();" />
        <cc1:ucButton ID="btnBack" runat="server" Text="Back" CssClass="button" OnClick="btnBack_Click" />
    </div>
</asp:Content>

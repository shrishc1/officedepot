﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SCT_SPNumberReports.aspx.cs" Inherits="ModuleUI_Security_SCT_SPNumberReports"
    MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master" %>

 <%@ Register Src="~/CommonUI/UserControls/MultiSelectCountry.ascx" TagName="MultiSelectCountry"
    TagPrefix="cc1" %>
   
<%@ Register Src="~/CommonUI/UserControls/MultiSelectStockPlanner.ascx" TagName="MultiSelectStockPlanner"
    TagPrefix="cc1" %>

<%@ Register Src="~/CommonUI/UserControls/MultiSelectSPNumber.ascx" TagName="MultiSelectSPNumber"
    TagPrefix="cc1" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <h2>
        <cc1:ucLabel ID="lblSPNumberReports" runat="server"></cc1:ucLabel>
    </h2>
    <div class="right-shadow">
  
        <div class="formbox">
               <table width="95%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings" id="tblSearch" runat="server">
                
                            
                    <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblStockPlanner" runat="server" Text="Stock Planner"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="Label17" runat="server">:</cc1:ucLabel>
                        </td>
                        <td>
                            <cc1:MultiSelectStockPlanner runat="server" ID="msStockPlanner" />
                        </td>
                    </tr>


                   <tr>
                        <td style="font-weight: bold; width: 10%">
                            <cc1:ucLabel ID="lblCountry" runat="server" Text="Country"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold; width: 1%">
                            <cc1:ucLabel ID="UcLabel5" runat="server">:</cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold; width: 89%">
                            <cc1:MultiSelectCountry runat="server" ID="msCountry" />
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblStockPlannerNumber" runat="server">
                            </cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="Uclbl8" runat="server">:</cc1:ucLabel>
                        </td>
                        <td>
                            <cc1:MultiSelectSPNumber runat="server" ID="MultiSelectSPNumber" />
                             <br />
                        <br />
                        </td>
                       
                    </tr>
                    <tr align="right">
                    <td colspan="4">
                        <cc1:ucButton  runat="server" ID="btnSearch" CssClass="button" OnClick="btnSearch_Click" />
                    </td>
                </tr>  
                    </table> 
              <div id="divSummaryGrid" runat="server" visible="false">
                        
                  <div class="button-row">
                        <br />
                             <%--<cc2:ucexporttoexcel ID="ucExportToExcel1" runat="server"  Visible="false" />     --%>                        
                            <cc1:ucButton ID="btnExportToExcel" runat="server" Visible="false" CssClass="exporttoexcel button" OnClick="btnExport_Click"
                             Width="109px" Height="20px" /> 
                   </div>
            
                   <div class="wmd-view">
                            <div class="dynamic-div">
                                <cc1:ucGridView ID="grdBind" ClientIDMode="Static" Width="100%"  runat="server"
                                    AllowPaging="true" PageSize="20" CssClass="grid modify-grid"  AutoGenerateColumns="true" EnableViewState="false"
                                     OnPageIndexChanging="OnPageIndexChanging" OnRowDataBound="grdBind_RowDataBound"  OnRowCreated="grdBind_RowCreated">
                                    <HeaderStyle Width="20px" HorizontalAlign="Left" Wrap="False"></HeaderStyle>
                                    <RowStyle HorizontalAlign="Left" Width="20px" Wrap="true"></RowStyle>     
                                                                                                       
                                               
                                    <%--<Columns>
                                     <asp:BoundField DataField="UserID" HeaderText="UserID" ItemStyle-Width="150" Visible="false" />
                                     </Columns>
--%>
                                </cc1:ucGridView>
                            </div>
                            <asp:Label ID="lblError" runat="server" Visible="false" />
                        </div>
                        <cc1:ucGridView ID="grdBindExport" Visible="false" ClientIDMode="Static" Width="100%"
                            Height="80%" runat="server" CssClass="grid" 
                            AutoGenerateColumns="true" OnRowCreated="grdBindExport_RowCreated">                          
                            
                        </cc1:ucGridView>

                 <%--  <div class="button-row" >
                        <cc1:PagerV2_8 ID="pager1" runat="server" OnCommand="pager_Command" GenerateGoToSection="false">
                        </cc1:PagerV2_8>
                    </div>--%>

                   <div class="button-row">
                            <br />
                            <br />
                            <cc1:ucButton ID="btnBack" runat="server" Text="Back" align="right"
                                CssClass="button" OnClick="btnBack_Click" />
                        </div>
                 </div>   
                    </div>
        </div>
    


</asp:Content>
﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using BusinessLogicLayer.ModuleBAL.Appointment.SiteSettings;

public partial class UP_VendorEdit : CommonPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        ddlCountry.CurrentPage = this;
        if (!Page.IsPostBack)
        {
           
        }
    }
    protected override void OnPreRender(EventArgs e) {
        if (!IsPostBack) {
          // ddlCountry.innerControlddlCountry.SelectedIndex = 1;
           Bind();
        }
        base.OnPreRender(e);
    }
    private void Bind()
    {
        UP_VendorBE oUP_VendorBE = new UP_VendorBE();
        UP_VendorBAL oUP_VendorBAL = new UP_VendorBAL();

        oUP_VendorBE.Action = "GetVendorForEdit";
        oUP_VendorBE.CountryID =  Convert.ToInt32(ddlCountry.innerControlddlCountry.SelectedItem.Value) ;

        List<UP_VendorBE> lstVendor = oUP_VendorBAL.GetVendorForEdit(oUP_VendorBE);
        oUP_VendorBAL = null;
        gvVendorEdit.DataSource = lstVendor;
        gvVendorEdit.DataBind();
    }

    protected void btnDelete_Click(object sender, CommandEventArgs e)
    {
        UP_VendorBE oUP_VendorBE = new UP_VendorBE();
        UP_VendorBAL oUP_VendorBAL = new UP_VendorBAL();

        oUP_VendorBE.Action = "DeleteVendor";
        oUP_VendorBE.VendorID = Convert.ToInt32(e.CommandArgument.ToString().Trim());


        oUP_VendorBAL.UpdateDeleteVendor(oUP_VendorBE);
        oUP_VendorBAL = null;
        Bind();
    }

    protected void btnUpdate_Click(object sender, CommandEventArgs e)
    {
        UP_VendorBE oUP_VendorBE = new UP_VendorBE();
        UP_VendorBAL oUP_VendorBAL = new UP_VendorBAL();

        oUP_VendorBE.Action = "UpdateVendorDetail";
        oUP_VendorBE.VendorID = Convert.ToInt32(e.CommandArgument.ToString().Trim());

        oUP_VendorBAL.UpdateDeleteVendor(oUP_VendorBE);
        oUP_VendorBAL = null;
        Bind();
    }
    protected void btnSearch_Click(object sender, EventArgs e) {
        Bind();
    }
    protected void gvVendorEdit_PageIndexChanging(object sender, GridViewPageEventArgs e) {
        gvVendorEdit.PageIndex = e.NewPageIndex;
        Bind();
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.Appointment.CountrySetting;
using BusinessLogicLayer.ModuleBAL.Appointment.CountrySetting;
using BusinessLogicLayer.ModuleBAL.Security;
using BusinessEntities.ModuleBE.Security;
using BusinessEntities.ModuleBE.AdminFunctions;
using BusinessLogicLayer.ModuleBAL.AdminFunctions;
using System.Data;
using Utilities;
using WebUtilities;
using System.IO;
using System.Configuration;
using BaseControlLibrary;
using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using BusinessLogicLayer.ModuleBAL.Appointment.SiteSettings;


public partial class SCT_RegisterExternalUserEdit : CommonPage
{
    #region Declarations ...
    string sPortalLink = Convert.ToString(ConfigurationManager.AppSettings["Portallink"]) + @"/ModuleUI/Security/Login.aspx";
    protected string RequestConfirmationMessage = WebCommon.getGlobalResourceValue("RequestConfirmationMessage");
    protected string RequestEditedAwaitingConfirmationMessage = WebCommon.getGlobalResourceValue("RequestEditedAwaitingConfirmationMessage");
    protected string deleteMessage = WebCommon.getGlobalResourceValue("deleteMessage");
    protected string RejectedUserdeleteMessage = WebCommon.getGlobalResourceValue("RejectedUserdeleteMessage");
    string path = AppDomain.CurrentDomain.BaseDirectory.ToLower();
    string templatePath = @"/EmailTemplates/Generic/";
    protected string AlredySelectedVendor = WebCommon.getGlobalResourceValue("AlredySelectedVendor");
    string UpdatedSuccessfully = WebCommon.getGlobalResourceValue("UpdatedSuccessfully");
    public bool btnSearchGetClicked = false;
    #endregion

    #region Events ...

    protected void Page_Init(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            if (GetQueryStringValue("ReadOnly") == "1")
            {
                pnlPersonalDetails.Enabled = false;
                pnlCountrySettings.Enabled = false;
                pnlCountrySpecific.Enabled = false;
                pnlApprove.Enabled = false;
                pnlCarrierSction.Enabled = false;
                pnlModules.Enabled = false;
                pnlPersonalDetails.Enabled = false;
                pnlRejectionComment.Enabled = false;
                pnlSites.Enabled = false;
                pnlVendorDetail.Enabled = false;
                pnlVendorSction.Enabled = false;
                btnBlockUser.Visible = false;
                btnDeleteRejectedUser.Visible = false;
                btnDeleteUser.Visible = false;
                btnResendApprovalMail.Visible = false;
                btnSave.Visible = false;
                btnSaveChanges.Visible = false;
                btnUnblockUser.Visible = false;
                btnUpdate.Visible = false;
                btnProceed.Visible = true;
            }
            else
            {
                pnlPersonalDetails.Enabled = true;
                pnlCountrySettings.Enabled = true;
                pnlCountrySpecific.Enabled = true;
                pnlApprove.Enabled = true;
                pnlCarrierSction.Enabled = true;
                pnlModules.Enabled = true;
                pnlPersonalDetails.Enabled = true;
                pnlRejectionComment.Enabled = true;
                pnlSites.Enabled = true;
                pnlVendorDetail.Enabled = true;
                pnlVendorSction.Enabled = true;
                btnBlockUser.Visible = true;
                btnDeleteRejectedUser.Visible = true;
                btnDeleteUser.Visible = true;
                btnResendApprovalMail.Visible = true;
                btnSave.Visible = true;
                btnSaveChanges.Visible = true;
                btnUnblockUser.Visible = true;
                btnUpdate.Visible = true;
                btnProceed.Visible = false;
            }
            if (GetQueryStringValue("VendorID") != null)
                ViewState["VendorID"] = GetQueryStringValue("VendorID");

            this.ButtonEnability();

        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (GetQueryStringValue("UserID") != null && GetQueryStringValue("UserID") != "")
        {
            if (Session["Role"].ToString().ToLower().Equals("vendor"))
            {

                if (Session["userid"].ToString() != GetQueryStringValue("UserID").ToString())
                {
                    Session.Abandon();
                    Response.Redirect("~/ModuleUI/Security/Login.aspx");
                }
            }
        }


        if (!IsPostBack)
        {
            this.BindLanguage();
            this.getUserDetail();
            this.BindOtherSettings();

            if (Session["Role"].ToString().ToLower().Equals("vendor"))
            {
                //rblVendorUpdate.Visible = true;
                this.VendorDisability();
                ScoreCard.Visible = false;

            }
            else
            { //rblVendorUpdate.Visible = false; 
                ScoreCard.Visible = true;
            }

            if (Session["Role"].ToString().ToLower().Equals("od - stock planner"))
            {
                btnDeleteUser.Enabled = false;
            }


        }
    }


    protected void rptCountry_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            CheckBox chkCountry = (CheckBox)e.Item.FindControl("chkCountry");
            if (lstUserSettings.FindAll(x => x.CountryId == ((MAS_CountryBE)e.Item.DataItem).CountryID).Count > 0)
            {
                chkCountry.Checked = true;
                ViewState["ContryIDs"] += ((MAS_CountryBE)e.Item.DataItem).CountryID.ToString() + ",";
            }
            chkCountry.Attributes.Add("onclick", "return ShowAlert('" + chkCountry.ClientID + "');");
        }
    }

    void chkCountry_CheckedChanged(object sender, EventArgs e)
    {
        //throw new NotImplementedException();
        btnUpdate_Click(sender, e);
        if (Session["Role"].ToString().ToLower().Equals("vendor"))
        {
            this.VendorDisability();
        }
    }

    protected void rptAppointmentCountry_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Repeater rptAppointmentCountrySites = (Repeater)e.Item.FindControl("rptAppointmentCountrySites");
            MAS_SiteBE oMAS_SiteBE = new MAS_SiteBE();
            oMAS_SiteBE.Action = "GetAllSitesBasedCountry";
            oMAS_SiteBE.SiteCountryID = ((SCT_UserBE)e.Item.DataItem).CountryId;
            MAS_SiteBAL oMAS_SiteBAL = new MAS_SiteBAL();
            List<MAS_SiteBE> lstCountrySites = oMAS_SiteBAL.GetAllSitesBasedCountryBAL(oMAS_SiteBE);

            rptAppointmentCountrySites.DataSource = lstCountrySites;
            rptAppointmentCountrySites.DataBind();
        }
    }

    protected void rptDiscrepanciesCountry_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Repeater rptDiscrepanciesCountrySites = (Repeater)e.Item.FindControl("rptDiscrepanciesCountrySites");
            MAS_SiteBE oMAS_SiteBE = new MAS_SiteBE();
            oMAS_SiteBE.Action = "GetAllSitesBasedCountry";
            oMAS_SiteBE.SiteCountryID = ((SCT_UserBE)e.Item.DataItem).CountryId;
            MAS_SiteBAL oMAS_SiteBAL = new MAS_SiteBAL();
            List<MAS_SiteBE> lstCountrySites = oMAS_SiteBAL.GetAllSitesBasedCountryBAL(oMAS_SiteBE);

            rptDiscrepanciesCountrySites.DataSource = lstCountrySites;
            rptDiscrepanciesCountrySites.DataBind();
        }
    }

    protected void rptInvoiceIssueCountry_ItemDataBound(object sender, RepeaterItemEventArgs e) {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem) {
            Repeater rptInvoiceIssueCountrySites = (Repeater)e.Item.FindControl("rptInvoiceIssueCountrySites");
            MAS_SiteBE oMAS_SiteBE = new MAS_SiteBE();
            oMAS_SiteBE.Action = "GetAllSitesBasedCountry";
            oMAS_SiteBE.SiteCountryID = ((SCT_UserBE)e.Item.DataItem).CountryId;
            MAS_SiteBAL oMAS_SiteBAL = new MAS_SiteBAL();
            List<MAS_SiteBE> lstCountrySites = oMAS_SiteBAL.GetAllSitesBasedCountryBAL(oMAS_SiteBE);

            rptInvoiceIssueCountrySites.DataSource = lstCountrySites;
            rptInvoiceIssueCountrySites.DataBind();
        }
    }

    protected void rptAppointmentCountrySites_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            HiddenField hdnCountryID = (HiddenField)e.Item.Parent.Parent.FindControl("hdnCountryID");
            if (hdnCountryID != null)
            {
                int countryId;
                if (Int32.TryParse(hdnCountryID.Value, out countryId))
                {
                    SCT_UserBE oSCT_UserBE = lstUserSettings.Find(x => x.CountryId == countryId && x.SiteId == ((MAS_SiteBE)e.Item.DataItem).SiteID);
                    if (oSCT_UserBE != null)
                    {
                        if (oSCT_UserBE.SchedulingContact == 'Y')
                        {
                            CheckBox chkSite = (CheckBox)e.Item.FindControl("chkSite");
                            chkSite.Checked = true;
                        }
                    }
                }
            }
        }
    }

    protected void rptDiscrepanciesCountrySites_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            HiddenField hdnCountryID = (HiddenField)e.Item.Parent.Parent.FindControl("hdnCountryID");
            if (hdnCountryID != null)
            {
                int countryId;
                if (Int32.TryParse(hdnCountryID.Value, out countryId))
                {
                    SCT_UserBE oSCT_UserBE = lstUserSettings.Find(x => x.CountryId == countryId && x.SiteId == ((MAS_SiteBE)e.Item.DataItem).SiteID);
                    if (oSCT_UserBE != null)
                    {
                        if (oSCT_UserBE.DiscrepancyContact == 'Y')
                        {
                            CheckBox chkSite = (CheckBox)e.Item.FindControl("chkSite");
                            chkSite.Checked = true;
                        }
                    }
                }
            }
        }
    }

    protected void rptInvoiceIssueCountrySites_ItemDataBound(object sender, RepeaterItemEventArgs e) {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem) {
            HiddenField hdnCountryID = (HiddenField)e.Item.Parent.Parent.FindControl("hdnCountryID");
            if (hdnCountryID != null) {
                int countryId;
                if (Int32.TryParse(hdnCountryID.Value, out countryId)) {
                    SCT_UserBE oSCT_UserBE = lstUserSettings.Find(x => x.CountryId == countryId && x.SiteId == ((MAS_SiteBE)e.Item.DataItem).SiteID);
                    if (oSCT_UserBE != null) {
                        if (oSCT_UserBE.InvoiceIssueContact == 'Y') {
                            CheckBox chkSite = (CheckBox)e.Item.FindControl("chkSite");
                            chkSite.Checked = true;
                        }
                    }
                }
            }
        }
    }

    protected void rptOtifCountry_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            CheckBox chkCountry = (CheckBox)e.Item.FindControl("chkCountry");
            if (((SCT_UserBE)e.Item.DataItem).OTIFContact == 'Y')
            {
                chkCountry.Checked = true;
            }
        }
    }

    protected void rptScorecardCountry_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            CheckBox chkCountry = (CheckBox)e.Item.FindControl("chkCountry");
            if (((SCT_UserBE)e.Item.DataItem).ScorecardContact == 'Y')
            {
                chkCountry.Checked = true;
            }
        }
    }

    protected void rptCarrierCountry_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            SCT_UserBE oSCT_UserBE = lstUserSettings.Find(x => x.CountryId == ((MAS_CountryBE)e.Item.DataItem).CountryID);
            CheckBox chkCountry = (CheckBox)e.Item.FindControl("chkCountry");
            if (oSCT_UserBE != null)
            {
                chkCountry.Checked = true;
            }
            chkCountry.Attributes.Add("onclick", "return ShowAlert('" + chkCountry.ClientID + "');");

            ucDropdownList ddlCarrier = (ucDropdownList)e.Item.FindControl("ddlCarrier");

            MASCNT_CarrierBE oMASCNT_CarrierBE = new MASCNT_CarrierBE();
            oMASCNT_CarrierBE.Action = "GetAllCarriers";
            oMASCNT_CarrierBE.CountryID = ((MAS_CountryBE)e.Item.DataItem).CountryID;
            APPCNT_CarrierBAL oAPPCNT_CarrierBAL = new APPCNT_CarrierBAL();
            List<MASCNT_CarrierBE> lstCarriers = oAPPCNT_CarrierBAL.GetAllCarriersBAL(oMASCNT_CarrierBE);

            if (lstCarriers.Count > 0)
            {
                FillControls.FillDropDown(ref ddlCarrier, lstCarriers, "CarrierName", "CarrierID", "--Select--");
            }
            if (oSCT_UserBE != null)
            {
                if (oSCT_UserBE.CarrierID != null)
                {
                    if (ddlCarrier.Items.FindByValue(oSCT_UserBE.CarrierID.Value.ToString()) != null)
                    {
                        ddlCarrier.Items.FindByValue(oSCT_UserBE.CarrierID.Value.ToString()).Selected = true;
                    }
                }
            }

        }
    }

    protected void rptCarrierCountry_ItemCreated(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            CheckBox chkCountry = (CheckBox)e.Item.FindControl("chkCountry");
            chkCountry.AutoPostBack = true;
            chkCountry.CheckedChanged += new EventHandler(chkCountry_CheckedChanged);
        }
    }

    protected void rptCarrierSitesCountry_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Repeater rptCarrierSites = (Repeater)e.Item.FindControl("rptCarrierSites");
            MAS_SiteBE oMAS_SiteBE = new MAS_SiteBE();
            oMAS_SiteBE.Action = "GetAllSitesBasedCountry";
            oMAS_SiteBE.SiteCountryID = ((SCT_UserBE)e.Item.DataItem).CountryId;
            MAS_SiteBAL oMAS_SiteBAL = new MAS_SiteBAL();
            List<MAS_SiteBE> lstCountrySites = oMAS_SiteBAL.GetAllSitesBasedCountryBAL(oMAS_SiteBE);

            rptCarrierSites.DataSource = lstCountrySites;
            rptCarrierSites.DataBind();
        }
    }

    protected void rptCarrierSites_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            HiddenField hdnCountryID = (HiddenField)e.Item.Parent.Parent.FindControl("hdnCountryID");
            if (hdnCountryID != null)
            {
                int countryId;
                if (Int32.TryParse(hdnCountryID.Value, out countryId))
                {
                    SCT_UserBE oSCT_UserBE = lstUserSettings.Find(x => x.CountryId == countryId && x.SiteId == ((MAS_SiteBE)e.Item.DataItem).SiteID);
                    if (oSCT_UserBE != null)
                    {
                        if (oSCT_UserBE.SchedulingContact == 'Y')
                        {
                            CheckBox chkSite = (CheckBox)e.Item.FindControl("chkSite");
                            chkSite.Checked = true;
                        }
                    }
                }
            }
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        SCT_UserBE oSCT_UserBE = new SCT_UserBE();
        SCT_UserBAL oSCT_UserBAL = new SCT_UserBAL();
        if (GetQueryStringValue("UserID") != null)
            oSCT_UserBE.UserID = Convert.ToInt32(GetQueryStringValue("UserID"));
        else
            oSCT_UserBE.UserID = Convert.ToInt32(Convert.ToString(Session["UserID"]));

        if (ViewState["UserRole"].ToString() == "2") //Vendor
        {
            //Validations
            bool IsCountryValid = false;
            foreach (RepeaterItem ritem in rptCountry.Items)
            {
                if (ritem.ItemType == ListItemType.Item || ritem.ItemType == ListItemType.AlternatingItem)
                {
                   
                    CheckBox chkCountry = (CheckBox)ritem.FindControl("chkCountry");
                    if (chkCountry.Checked)
                    {
                        IsCountryValid = true;
                        HiddenField hdnCountryID = (HiddenField)ritem.FindControl("hdnCountryID");
                        oSCT_UserBE.CountryIds += hdnCountryID.Value + ",";
                    }
                }
            }

            bool IsVendorValid = true;
            foreach (RepeaterItem item in rptVendorDetailsREU.Items)
            {
                if (item.ItemType == ListItemType.Item || item.ItemType == ListItemType.AlternatingItem)
                {
                    HiddenField hdnVendorID = (HiddenField)item.FindControl("hdnVendorID");

                    string VendorId = hdnVendorID.Value;
                    if (string.IsNullOrWhiteSpace(VendorId))
                    {
                        IsVendorValid = false;
                        break;
                    }
                }
            }

            bool IsModulesValid = false;
            if (GetQueryStringValue("UserID") != null)
                oSCT_UserBE.UserID = Convert.ToInt32(GetQueryStringValue("UserID"));
            else
                oSCT_UserBE.UserID = Convert.ToInt32(Convert.ToString(Session["UserID"]));

            List<UserCountryModulesSettingsExternal> lstUserCountryModulesSettings = new List<UserCountryModulesSettingsExternal>();
            UserCountryModulesSettingsExternal oUserCountryModulesSettings;
            foreach (RepeaterItem citem in rptCountry.Items)
            {
                if (citem.ItemType == ListItemType.Item || citem.ItemType == ListItemType.AlternatingItem)
                {
                    CheckBox chkCountry = (CheckBox)citem.FindControl("chkCountry");
                    if (chkCountry.Checked)
                    {
                        HiddenField hdnCountryID = (HiddenField)citem.FindControl("hdnCountryID");
                        oUserCountryModulesSettings = new UserCountryModulesSettingsExternal();
                        oUserCountryModulesSettings.CountryID = Convert.ToInt32(hdnCountryID.Value);
                        lstUserCountryModulesSettings.Add(oUserCountryModulesSettings);
                    }
                }
            }

            foreach (RepeaterItem citem in rptAppointmentCountry.Items)
            {
                if (citem.ItemType == ListItemType.Item || citem.ItemType == ListItemType.AlternatingItem)
                {
                    HiddenField hdnCountryID = (HiddenField)citem.FindControl("hdnCountryID");
                    lstUserCountryModulesSettings.Find(x => x.CountryID == Convert.ToInt32(hdnCountryID.Value)).IsSchedulingDefined = false;

                    Repeater rptAppointmentCountrySites = (Repeater)citem.FindControl("rptAppointmentCountrySites");
                    if (rptAppointmentCountrySites != null)
                    {
                        foreach (RepeaterItem sitem in rptAppointmentCountrySites.Items)
                        {
                            if (sitem.ItemType == ListItemType.Item || sitem.ItemType == ListItemType.AlternatingItem)
                            {
                                CheckBox chkSite = (CheckBox)sitem.FindControl("chkSite");
                                if (chkSite.Checked)
                                {
                                    HiddenField hdnSiteID = (HiddenField)sitem.FindControl("hdnSiteID");
                                    oSCT_UserBE.AppointmentSites += hdnSiteID.Value + ",";
                                    lstUserCountryModulesSettings.Find(x => x.CountryID == Convert.ToInt32(hdnCountryID.Value)).IsSchedulingDefined = true;
                                }
                            }
                        }
                    }
                }
            }

            foreach (RepeaterItem citem in rptDiscrepanciesCountry.Items)
            {
                if (citem.ItemType == ListItemType.Item || citem.ItemType == ListItemType.AlternatingItem)
                {
                    HiddenField hdnCountryID = (HiddenField)citem.FindControl("hdnCountryID");
                    lstUserCountryModulesSettings.Find(x => x.CountryID == Convert.ToInt32(hdnCountryID.Value)).IsDiscrepancyDefined = false;

                    Repeater rptDiscrepanciesCountrySites = (Repeater)citem.FindControl("rptDiscrepanciesCountrySites");
                    if (rptDiscrepanciesCountrySites != null)
                    {
                        foreach (RepeaterItem sitem in rptDiscrepanciesCountrySites.Items)
                        {
                            if (sitem.ItemType == ListItemType.Item || sitem.ItemType == ListItemType.AlternatingItem)
                            {
                                CheckBox chkSite = (CheckBox)sitem.FindControl("chkSite");
                                if (chkSite.Checked)
                                {
                                    HiddenField hdnSiteID = (HiddenField)sitem.FindControl("hdnSiteID");
                                    oSCT_UserBE.DiscrepancySites += hdnSiteID.Value + ",";
                                    lstUserCountryModulesSettings.Find(x => x.CountryID == Convert.ToInt32(hdnCountryID.Value)).IsDiscrepancyDefined = true;
                                }
                            }
                        }
                    }
                }
            }

            foreach (RepeaterItem citem in rptInvoiceIssueCountry.Items) {
                if (citem.ItemType == ListItemType.Item || citem.ItemType == ListItemType.AlternatingItem) {
                    HiddenField hdnCountryID = (HiddenField)citem.FindControl("hdnCountryID");
                    lstUserCountryModulesSettings.Find(x => x.CountryID == Convert.ToInt32(hdnCountryID.Value)).IsInvoiceIssueDefined = false;

                    Repeater rptInvoiceIssueCountrySites = (Repeater)citem.FindControl("rptInvoiceIssueCountrySites");
                    if (rptInvoiceIssueCountrySites != null) {
                        foreach (RepeaterItem sitem in rptInvoiceIssueCountrySites.Items) {
                            if (sitem.ItemType == ListItemType.Item || sitem.ItemType == ListItemType.AlternatingItem) {
                                CheckBox chkSite = (CheckBox)sitem.FindControl("chkSite");
                                if (chkSite.Checked) {
                                    HiddenField hdnSiteID = (HiddenField)sitem.FindControl("hdnSiteID");
                                    oSCT_UserBE.InvoiceIssueSites += hdnSiteID.Value + ",";
                                    lstUserCountryModulesSettings.Find(x => x.CountryID == Convert.ToInt32(hdnCountryID.Value)).IsInvoiceIssueDefined = true;
                                }
                            }
                        }
                    }
                }
            }

            foreach (RepeaterItem citem in rptInvoiceIssueCountry.Items) {
                if (citem.ItemType == ListItemType.Item || citem.ItemType == ListItemType.AlternatingItem) {
                    HiddenField hdnCountryID = (HiddenField)citem.FindControl("hdnCountryID");
                    lstUserCountryModulesSettings.Find(x => x.CountryID == Convert.ToInt32(hdnCountryID.Value)).IsInvoiceIssueDefined = false;

                    Repeater rptInvoiceIssueCountrySites = (Repeater)citem.FindControl("rptInvoiceIssueCountrySites");
                    if (rptInvoiceIssueCountrySites != null) {
                        foreach (RepeaterItem sitem in rptInvoiceIssueCountrySites.Items) {
                            if (sitem.ItemType == ListItemType.Item || sitem.ItemType == ListItemType.AlternatingItem) {
                                CheckBox chkSite = (CheckBox)sitem.FindControl("chkSite");
                                if (chkSite.Checked) {
                                    HiddenField hdnSiteID = (HiddenField)sitem.FindControl("hdnSiteID");
                                    oSCT_UserBE.InvoiceIssueSites += hdnSiteID.Value + ",";
                                    lstUserCountryModulesSettings.Find(x => x.CountryID == Convert.ToInt32(hdnCountryID.Value)).IsInvoiceIssueDefined = true;
                                }
                            }
                        }
                    }
                }
            }

            foreach (RepeaterItem citem in rptOtifCountry.Items)
            {
                if (citem.ItemType == ListItemType.Item || citem.ItemType == ListItemType.AlternatingItem)
                {
                    HiddenField hdnCountryID = (HiddenField)citem.FindControl("hdnCountryID");
                    lstUserCountryModulesSettings.Find(x => x.CountryID == Convert.ToInt32(hdnCountryID.Value)).IsOTIFDefined = false;

                    CheckBox chkCountry = (CheckBox)citem.FindControl("chkCountry");
                    if (chkCountry.Checked)
                    {
                        oSCT_UserBE.OTIFCountries += hdnCountryID.Value + ",";
                        lstUserCountryModulesSettings.Find(x => x.CountryID == Convert.ToInt32(hdnCountryID.Value)).IsOTIFDefined = true;
                    }
                }
            }

            foreach (RepeaterItem citem in rptScorecardCountry.Items)
            {
                if (citem.ItemType == ListItemType.Item || citem.ItemType == ListItemType.AlternatingItem)
                {
                    HiddenField hdnCountryID = (HiddenField)citem.FindControl("hdnCountryID");
                    lstUserCountryModulesSettings.Find(x => x.CountryID == Convert.ToInt32(hdnCountryID.Value)).IsScorecardDefined = false;

                    CheckBox chkCountry = (CheckBox)citem.FindControl("chkCountry");
                    if (chkCountry.Checked)
                    {
                        oSCT_UserBE.ScorecardCountries += hdnCountryID.Value + ",";
                        lstUserCountryModulesSettings.Find(x => x.CountryID == Convert.ToInt32(hdnCountryID.Value)).IsScorecardDefined = true;
                    }
                }
            }

            if (lstUserCountryModulesSettings.FindAll(x => x.IsSchedulingDefined == false
                && x.IsDiscrepancyDefined == false && x.IsOTIFDefined == false && x.IsScorecardDefined == false).Count > 0)
            {
                IsModulesValid = false;
            }
            else
            {
                IsModulesValid = true;
            }

            if (!IsCountryValid || !IsVendorValid || !IsModulesValid)
            {
                if (!IsCountryValid)
                {
                    string SelectCountry = WebCommon.getGlobalResourceValue("SelectCountry");
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + SelectCountry + "')", true);
                }
                else if (!IsVendorValid)
                {
                    string MustSelectVendor = WebCommon.getGlobalResourceValue("MustSelectVendor");
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + MustSelectVendor + "')", true);
                }
                else if (!IsModulesValid)
                {
                    string MustSelectModule = WebCommon.getGlobalResourceValue("MustSelectModule");
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + MustSelectModule + "')", true);
                }
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + RequestConfirmationMessage + "')", true);

                SavePeronalDetails();

                oSCT_UserBE.Action = "UpdateCountries";
                if (!string.IsNullOrWhiteSpace(oSCT_UserBE.CountryIds))
                {
                    oSCT_UserBAL.UpdateCountriesBAL(oSCT_UserBE);
                }

                SaveVendorSettings();

                oSCT_UserBE.Action = "UpdateModules";
                oSCT_UserBAL.UpdateModulesBAL(oSCT_UserBE);

                if (GetQueryStringValue("VendorID") != null)
                {
                    EncryptQueryString("../GlobalSettings/VendorEdit.aspx?VendorID=" + GetQueryStringValue("VendorID"));
                }
                else
                {
                    EncryptQueryString("SCT_UsersOverview.aspx");
                }
            }
        }
        else if (ViewState["UserRole"].ToString() == "3") //Carrier
        {
            bool IsCountryValid = false;
            bool IsCarrierCountryValid = true;

            foreach (RepeaterItem ritem in rptCarrierCountry.Items)
            {
                if (ritem.ItemType == ListItemType.Item || ritem.ItemType == ListItemType.AlternatingItem)
                {
                    CheckBox chkCountry = (CheckBox)ritem.FindControl("chkCountry");
                    if (chkCountry.Checked)
                    {
                        IsCountryValid = true;
                        HiddenField hdnCountryID = (HiddenField)ritem.FindControl("hdnCountryID");
                        oSCT_UserBE.CountryIds += hdnCountryID.Value + ",";
                        ucDropdownList ddlCarrier = (ucDropdownList)ritem.FindControl("ddlCarrier");
                        if (string.IsNullOrWhiteSpace(ddlCarrier.SelectedValue) || ddlCarrier.SelectedValue == "0")
                        {
                            IsCarrierCountryValid = false;
                            break;
                        }
                    }
                }
            }

            if (!IsCountryValid || !IsCarrierCountryValid)
            {
                if (!IsCountryValid)
                {
                    string SelectCountry = WebCommon.getGlobalResourceValue("SelectCountry");
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + SelectCountry + "')", true);
                }
                else if (!IsCarrierCountryValid)
                {
                    string MustSelectCarrier = WebCommon.getGlobalResourceValue("MustSelectCarrier");
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + MustSelectCarrier + "')", true);
                }
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + RequestConfirmationMessage + "')", true);

                SavePeronalDetails();

                oSCT_UserBE.Action = "UpdateCountries";
                if (!string.IsNullOrWhiteSpace(oSCT_UserBE.CountryIds))
                {
                    oSCT_UserBAL.UpdateCountriesBAL(oSCT_UserBE);
                }

                SaveCarrierCountrySettings();

                SaveCarrierSitesSettings();
                EncryptQueryString("SCT_UsersOverview.aspx");
            }
        }
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        if (GetQueryStringValue("UStatus") != null && GetQueryStringValue("UStatus") == "PRAA")
        {
            EncryptQueryString("SCT_PendingUsersOverview.aspx");
        }
        else if (GetQueryStringValue("UserID") != null)
        {
            EncryptQueryString("SCT_UsersOverview.aspx");
        }
        else if (Session["UserID"] != null && GetQueryStringValue("UserID") == null)
        {
            EncryptQueryString("~/ModuleUI/Dashboard/DefaultDashboard.aspx");
        }
        else
        {
            EncryptQueryString("Login.aspx");
        }
    }

    protected void btnBlockUser_Click(object sender, EventArgs e)
    {
        SCT_UserBE oSCT_UserBE = new SCT_UserBE();
        SCT_UserBAL oSCT_UserBAL = new SCT_UserBAL();
        oSCT_UserBE.Action = "UpdateAccountStatus";
        oSCT_UserBE.AccountStatus = "Blocked";
        oSCT_UserBE.UserID = Convert.ToInt32(GetQueryStringValue("UserID").ToString());
        int? iResult = oSCT_UserBAL.UpdateAccountStatus(oSCT_UserBE);
        oSCT_UserBAL = null;
        if (iResult != 0)
        {
            EncryptQueryString("SCT_UsersOverview.aspx");
        }
    }

    protected void btnUnblockUser_Click(object sender, EventArgs e)
    {
        SCT_UserBE oSCT_UserBE = new SCT_UserBE();
        SCT_UserBAL oSCT_UserBAL = new SCT_UserBAL();
        oSCT_UserBE.Action = "UpdateAccountStatus";
        oSCT_UserBE.AccountStatus = "Active";
        oSCT_UserBE.UserID = Convert.ToInt32(GetQueryStringValue("UserID").ToString());
        int? iResult = oSCT_UserBAL.UpdateAccountStatus(oSCT_UserBE);
        oSCT_UserBAL = null;
        if (iResult != 0)
        {
            SendUnblockCommunicationMail(txtUserIDEmail.Text, ViewState["Password"].ToString());
            EncryptQueryString("SCT_UsersOverview.aspx");
        }
    }

    protected void btnDeleteUser_Click(object sender, EventArgs e)
    {
        SCT_UserBE oSCT_UserBE = new SCT_UserBE();
        SCT_UserBAL oSCT_UserBAL = new SCT_UserBAL();
        oSCT_UserBE.Action = "DeleteUser";
        oSCT_UserBE.IsActive = false;
        oSCT_UserBE.UserID = Convert.ToInt32(GetQueryStringValue("UserID").ToString());
        oSCT_UserBE.oStockPlannerGroupings = new StockPlannerGroupingsBE();
        int? iResult = oSCT_UserBAL.addEditUserBAL(oSCT_UserBE);
        oSCT_UserBAL = null;
        if (iResult == 0)
        {
            EncryptQueryString("SCT_UsersOverview.aspx");
        }
    }

    protected void btnDeleteRejectedUser_Click(object sender, EventArgs e)
    {
        SCT_UserBE oSCT_UserBE = new SCT_UserBE();
        SCT_UserBAL oSCT_UserBAL = new SCT_UserBAL();
        oSCT_UserBE.Action = "DeleteRejectedUser";
        oSCT_UserBE.UserID = Convert.ToInt32(GetQueryStringValue("UserID").ToString());
        oSCT_UserBE.oStockPlannerGroupings = new StockPlannerGroupingsBE();
        int? iResult = oSCT_UserBAL.addEditUserBAL(oSCT_UserBE);
        oSCT_UserBAL = null;
        if (iResult == 0)
        {
            EncryptQueryString("SCT_UsersOverview.aspx");
        }
    }

    protected void btnSaveChanges_Click(object sender, EventArgs e)
    {
        SavePeronalDetails();
        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + UpdatedSuccessfully + "')", true);
    }

    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        if (ViewState["UserRole"].ToString() == "2") //Vendor
        {
            foreach (RepeaterItem ritem in rptCountry.Items)
            {
                if (ritem.ItemType == ListItemType.Item || ritem.ItemType == ListItemType.AlternatingItem)
                {
                    CheckBox chkCountry = (CheckBox)ritem.FindControl("chkCountry");
                    HiddenField hdnCountryID = (HiddenField)ritem.FindControl("hdnCountryID");
                    Label lblCountryName = (Label)ritem.FindControl("lblCountryName");
                    SCT_UserBE temp = lstCountrySettings.Find(x => x.CountryId == Convert.ToInt32(hdnCountryID.Value));
                    if (chkCountry.Checked)
                    {
                        if (temp == null)
                        {
                            lstCountrySettings.Add(new SCT_UserBE { UserID = Convert.ToInt32(GetQueryStringValue("UserID")), CountryId = Convert.ToInt32(hdnCountryID.Value), Country = lblCountryName.Text });
                        }
                    }
                    else
                    {
                        if (temp != null)
                        {
                            lstCountrySettings.Remove(temp);
                            lstUserSettings.RemoveAll(x => x.CountryId == Convert.ToInt32(hdnCountryID.Value));
                        }
                    }
                }
            }
        }
        else if (ViewState["UserRole"].ToString() == "3") //Carrier
        {
            foreach (RepeaterItem ritem in rptCarrierCountry.Items)
            {
                if (ritem.ItemType == ListItemType.Item || ritem.ItemType == ListItemType.AlternatingItem)
                {
                    CheckBox chkCountry = (CheckBox)ritem.FindControl("chkCountry");
                    HiddenField hdnCountryID = (HiddenField)ritem.FindControl("hdnCountryID");
                    Label lblCountryName = (Label)ritem.FindControl("lblCountryName");
                    SCT_UserBE temp = lstCountrySettings.Find(x => x.CountryId == Convert.ToInt32(hdnCountryID.Value));
                    if (chkCountry.Checked)
                    {
                        if (temp == null)
                        {
                            lstCountrySettings.Add(new SCT_UserBE { UserID = Convert.ToInt32(GetQueryStringValue("UserID")), CountryId = Convert.ToInt32(hdnCountryID.Value), Country = lblCountryName.Text });
                        }
                    }
                    else
                    {
                        if (temp != null)
                        {
                            lstCountrySettings.Remove(temp);
                            lstUserSettings.RemoveAll(x => x.CountryId == Convert.ToInt32(hdnCountryID.Value));
                            ucDropdownList ddlCarrier = (ucDropdownList)ritem.FindControl("ddlCarrier");
                            if (ddlCarrier != null)
                            {
                                if (ddlCarrier.Items.Count > 0)
                                {
                                    ddlCarrier.SelectedIndex = 0;
                                }
                            }
                        }
                    }
                }
            }
        }
        bindVendorAndModules();
    }

    protected void rptVendorDetailsREU_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            ucLabel lblCountryVendorDetailsREU = (ucLabel)e.Item.FindControl("lblCountryVendorDetailsREU");
            HiddenField hdnCountryIdVendorDetailsREU = (HiddenField)e.Item.FindControl("hdnCountryIdVendorDetailsREU");
            HiddenField hdnVendorID = (HiddenField)e.Item.FindControl("hdnVendorID");
            ucLabel SelectedVendorName = (ucLabel)e.Item.FindControl("SelectedVendorName");
            //ucSeacrhVendorWithoutSM ucSeacrhVendorWithoutSM1 = (ucSeacrhVendorWithoutSM)e.Item.FindControl("ucSeacrhVendorWithoutSM1");

            SCT_UserBE oSCT_UserBE = (SCT_UserBE)e.Item.DataItem;

            lblCountryVendorDetailsREU.Text = oSCT_UserBE.Country;
            hdnCountryIdVendorDetailsREU.Value = Convert.ToString(oSCT_UserBE.CountryId);
            SelectedVendorName.Text = oSCT_UserBE.VendorName;
            hdnVendorID.Value = Convert.ToString(oSCT_UserBE.VendorID);


            ///--- Added on 4 Mar 2013 ------
            //ucSeacrhVendorWithoutSM1.IsInActiveVendorRequired = true;
            ///--- Added on 4 Mar 2013 ------
            //if (ucSeacrhVendorWithoutSM1 != null)
            //{
            //    ucLabel SelectedVendorName = (ucLabel)ucSeacrhVendorWithoutSM1.FindControl("SelectedVendorName");
            //    SelectedVendorName.Text = oSCT_UserBE.VendorName;
            //    ucSeacrhVendorWithoutSM1.VendorNo = oSCT_UserBE.VendorID.ToString();

            //    ucSeacrhVendorWithoutSM1.CurrentPage = this;
            //    ucSeacrhVendorWithoutSM1.IsParentRequired = true;
            //    ucSeacrhVendorWithoutSM1.IsStandAloneRequired = true;
            //    ucSeacrhVendorWithoutSM1.IsChildRequired = true;
            //    ucSeacrhVendorWithoutSM1.IsGrandParentRequired = true;
            //    ucSeacrhVendorWithoutSM1.IsMultiControlRepeater = true;

            //    CountryID.Value = Convert.ToString( oSCT_UserBE.CountryId)!= "0" ? Convert.ToString( oSCT_UserBE.CountryId) : "0" ;
            //    ucSeacrhVendorWithoutSM1.CountryID = oSCT_UserBE.CountryId ?? 0;
            //}
        }
    }

    protected void rptVendorDetailsREU_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "SearchVendorDetailsREU")
        {
            ucPanel pnlSearchResult = (ucPanel)e.Item.FindControl("pnlSearchResult");
            ucListBox lstLeft = (ucListBox)e.Item.FindControl("lstLeft");
            ucTextbox txtSearchVendorDetailsREU = (ucTextbox)e.Item.FindControl("txtSearchVendorDetailsREU");
            HiddenField hdnCountryIdVendorDetailsREU = (HiddenField)e.Item.FindControl("hdnCountryIdVendorDetailsREU");
            if (!string.IsNullOrWhiteSpace(txtSearchVendorDetailsREU.Text))
            {
                this.BindVendor(lstLeft, txtSearchVendorDetailsREU.Text, Convert.ToInt32(hdnCountryIdVendorDetailsREU.Value));
                pnlSearchResult.Visible = true;
            }

            this.SetVendorDetailsRepeaterData();
        }
    }

    protected void btnSave_1_Click(object sender, EventArgs e)
    {
        SCT_UserBE oSCT_UserBE = new SCT_UserBE();
        SCT_UserBAL oSCT_UserBAL = new SCT_UserBAL();
        //oSCT_UserBE.UserID = Convert.ToInt32(GetQueryStringValue("UserID"));
        if (GetQueryStringValue("UserID") != null)
            oSCT_UserBE.UserID = Convert.ToInt32(GetQueryStringValue("UserID"));
        else
            oSCT_UserBE.UserID = Convert.ToInt32(Convert.ToString(Session["UserID"]));

        if (ViewState["UserRole"].ToString() == "2") //Vendor
        {
            #region Vendor save process ...
            //Validations
            bool IsCountryValid = false;
            foreach (RepeaterItem ritem in rptCountry.Items)
            {
                if (ritem.ItemType == ListItemType.Item || ritem.ItemType == ListItemType.AlternatingItem)
                {
                    CheckBox chkCountry = (CheckBox)ritem.FindControl("chkCountry");
                    if (chkCountry.Checked)
                    {
                        IsCountryValid = true;
                        HiddenField hdnCountryID = (HiddenField)ritem.FindControl("hdnCountryID");
                        oSCT_UserBE.CountryIds += hdnCountryID.Value + ",";
                    }
                }
            }

            bool IsVendorValid = true;
            foreach (RepeaterItem item in rptVendorDetailsREU.Items)
            {
                if (item.ItemType == ListItemType.Item || item.ItemType == ListItemType.AlternatingItem)
                {
                    HiddenField hdnVendorID = (HiddenField)item.FindControl("hdnVendorID");

                    string VendorId = hdnVendorID.Value;
                    if (string.IsNullOrWhiteSpace(VendorId))
                    {
                        IsVendorValid = false;
                        break;
                    }
                }
            }

            bool IsModulesValid = false;
            //oSCT_UserBE.UserID = Convert.ToInt32(GetQueryStringValue("UserID"));
            if (GetQueryStringValue("UserID") != null)
                oSCT_UserBE.UserID = Convert.ToInt32(GetQueryStringValue("UserID"));
            else
                oSCT_UserBE.UserID = Convert.ToInt32(Convert.ToString(Session["UserID"]));

            List<UserCountryModulesSettingsExternal> lstUserCountryModulesSettings = new List<UserCountryModulesSettingsExternal>();
            UserCountryModulesSettingsExternal oUserCountryModulesSettings;
            foreach (RepeaterItem citem in rptCountry.Items)
            {
                if (citem.ItemType == ListItemType.Item || citem.ItemType == ListItemType.AlternatingItem)
                {
                    CheckBox chkCountry = (CheckBox)citem.FindControl("chkCountry");
                    if (chkCountry.Checked)
                    {
                        HiddenField hdnCountryID = (HiddenField)citem.FindControl("hdnCountryID");
                        oUserCountryModulesSettings = new UserCountryModulesSettingsExternal();
                        oUserCountryModulesSettings.CountryID = Convert.ToInt32(hdnCountryID.Value);
                        lstUserCountryModulesSettings.Add(oUserCountryModulesSettings);
                    }
                }
            }

            foreach (RepeaterItem citem in rptAppointmentCountry.Items)
            {
                if (citem.ItemType == ListItemType.Item || citem.ItemType == ListItemType.AlternatingItem)
                {
                    HiddenField hdnCountryID = (HiddenField)citem.FindControl("hdnCountryID");
                    lstUserCountryModulesSettings.Find(x => x.CountryID == Convert.ToInt32(hdnCountryID.Value)).IsSchedulingDefined = false;

                    Repeater rptAppointmentCountrySites = (Repeater)citem.FindControl("rptAppointmentCountrySites");
                    if (rptAppointmentCountrySites != null)
                    {
                        foreach (RepeaterItem sitem in rptAppointmentCountrySites.Items)
                        {
                            if (sitem.ItemType == ListItemType.Item || sitem.ItemType == ListItemType.AlternatingItem)
                            {
                                CheckBox chkSite = (CheckBox)sitem.FindControl("chkSite");
                                if (chkSite.Checked)
                                {
                                    HiddenField hdnSiteID = (HiddenField)sitem.FindControl("hdnSiteID");
                                    oSCT_UserBE.AppointmentSites += hdnSiteID.Value + ",";
                                    lstUserCountryModulesSettings.Find(x => x.CountryID == Convert.ToInt32(hdnCountryID.Value)).IsSchedulingDefined = true;
                                }
                            }
                        }
                    }
                }
            }

            foreach (RepeaterItem citem in rptDiscrepanciesCountry.Items)
            {
                if (citem.ItemType == ListItemType.Item || citem.ItemType == ListItemType.AlternatingItem)
                {
                    HiddenField hdnCountryID = (HiddenField)citem.FindControl("hdnCountryID");
                    lstUserCountryModulesSettings.Find(x => x.CountryID == Convert.ToInt32(hdnCountryID.Value)).IsDiscrepancyDefined = false;

                    Repeater rptDiscrepanciesCountrySites = (Repeater)citem.FindControl("rptDiscrepanciesCountrySites");
                    if (rptDiscrepanciesCountrySites != null)
                    {
                        foreach (RepeaterItem sitem in rptDiscrepanciesCountrySites.Items)
                        {
                            if (sitem.ItemType == ListItemType.Item || sitem.ItemType == ListItemType.AlternatingItem)
                            {
                                CheckBox chkSite = (CheckBox)sitem.FindControl("chkSite");
                                if (chkSite.Checked)
                                {
                                    HiddenField hdnSiteID = (HiddenField)sitem.FindControl("hdnSiteID");
                                    oSCT_UserBE.DiscrepancySites += hdnSiteID.Value + ",";
                                    lstUserCountryModulesSettings.Find(x => x.CountryID == Convert.ToInt32(hdnCountryID.Value)).IsDiscrepancyDefined = true;
                                }
                            }
                        }
                    }
                }
            }


            foreach (RepeaterItem citem in rptInvoiceIssueCountry.Items) {
                if (citem.ItemType == ListItemType.Item || citem.ItemType == ListItemType.AlternatingItem) {
                    HiddenField hdnCountryID = (HiddenField)citem.FindControl("hdnCountryID");
                    lstUserCountryModulesSettings.Find(x => x.CountryID == Convert.ToInt32(hdnCountryID.Value)).IsInvoiceIssueDefined = false;

                    Repeater rptInvoiceIssueCountrySites = (Repeater)citem.FindControl("rptInvoiceIssueCountrySites");
                    if (rptInvoiceIssueCountrySites != null) {
                        foreach (RepeaterItem sitem in rptInvoiceIssueCountrySites.Items) {
                            if (sitem.ItemType == ListItemType.Item || sitem.ItemType == ListItemType.AlternatingItem) {
                                CheckBox chkSite = (CheckBox)sitem.FindControl("chkSite");
                                if (chkSite.Checked) {
                                    HiddenField hdnSiteID = (HiddenField)sitem.FindControl("hdnSiteID");
                                    oSCT_UserBE.InvoiceIssueSites += hdnSiteID.Value + ",";
                                    lstUserCountryModulesSettings.Find(x => x.CountryID == Convert.ToInt32(hdnCountryID.Value)).IsInvoiceIssueDefined = true;
                                }
                            }
                        }
                    }
                }
            }


            foreach (RepeaterItem citem in rptOtifCountry.Items)
            {
                if (citem.ItemType == ListItemType.Item || citem.ItemType == ListItemType.AlternatingItem)
                {
                    HiddenField hdnCountryID = (HiddenField)citem.FindControl("hdnCountryID");
                    lstUserCountryModulesSettings.Find(x => x.CountryID == Convert.ToInt32(hdnCountryID.Value)).IsOTIFDefined = false;

                    CheckBox chkCountry = (CheckBox)citem.FindControl("chkCountry");
                    if (chkCountry.Checked)
                    {
                        oSCT_UserBE.OTIFCountries += hdnCountryID.Value + ",";
                        lstUserCountryModulesSettings.Find(x => x.CountryID == Convert.ToInt32(hdnCountryID.Value)).IsOTIFDefined = true;
                    }
                }
            }

            foreach (RepeaterItem citem in rptScorecardCountry.Items)
            {
                if (citem.ItemType == ListItemType.Item || citem.ItemType == ListItemType.AlternatingItem)
                {
                    HiddenField hdnCountryID = (HiddenField)citem.FindControl("hdnCountryID");
                    lstUserCountryModulesSettings.Find(x => x.CountryID == Convert.ToInt32(hdnCountryID.Value)).IsScorecardDefined = false;

                    CheckBox chkCountry = (CheckBox)citem.FindControl("chkCountry");
                    if (chkCountry.Checked)
                    {
                        oSCT_UserBE.ScorecardCountries += hdnCountryID.Value + ",";
                        lstUserCountryModulesSettings.Find(x => x.CountryID == Convert.ToInt32(hdnCountryID.Value)).IsScorecardDefined = true;
                    }
                }
            }

            if (lstUserCountryModulesSettings.FindAll(x => x.IsSchedulingDefined == false
                && x.IsDiscrepancyDefined == false && x.IsOTIFDefined == false && x.IsScorecardDefined == false).Count > 0)
            {
                IsModulesValid = false;
            }
            else
            {
                IsModulesValid = true;
            }

            if (!IsCountryValid || !IsVendorValid || !IsModulesValid)
            {
                if (!IsCountryValid)
                {
                    string SelectCountry = WebCommon.getGlobalResourceValue("SelectCountry");
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + SelectCountry + "')", true);
                }
                else if (!IsVendorValid)
                {
                    string MustSelectVendor = WebCommon.getGlobalResourceValue("MustSelectVendor");
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + MustSelectVendor + "')", true);
                }
                else if (!IsModulesValid)
                {
                    string MustSelectModule = WebCommon.getGlobalResourceValue("MustSelectModule");
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + MustSelectModule + "')", true);
                }
            }
            else
            {

                if (GetQueryStringValue("UserID") == null && Session["UserID"] != null &&
                    (Convert.ToString(ViewState["AccountStatus"]).Equals("Edited Awaiting Approval")))
                {
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + RequestEditedAwaitingConfirmationMessage + "')", true);
                    //if (!ViewState["ContryIDs"].Equals(oSCT_UserBE.CountryIds)) {
                    //    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + RequestEditedAwaitingConfirmationMessage + "')", true);
                    //}
                    //else {
                    //    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + UpdatedSuccessfully + "')", true);
                    //}
                }
                else if (!Convert.ToString(ViewState["AccountStatus"]).Equals("Active"))
                {
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + RequestConfirmationMessage + "')", true);
                }
                else if (!ViewState["ContryIDs"].Equals(oSCT_UserBE.CountryIds))
                {
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + RequestEditedAwaitingConfirmationMessage + "')", true);
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + UpdatedSuccessfully + "')", true);
                }

                var userRoleId = Convert.ToInt32(Session["UserRoleId"]);
                if ((userRoleId.Equals(1) && rblApprovalStatus.SelectedValue.Equals("approve")) ||
                    Convert.ToString(ViewState["UserRole"]).Equals("2") && !rblApprovalStatus.SelectedValue.Equals("reject"))
                {
                    this.SavePeronalDetails();

                    oSCT_UserBE.Action = "UpdateCountries";
                    oSCT_UserBE.UpdatedRoleBy = Convert.ToString(Session["Role"]).ToLower();
                    if (!string.IsNullOrWhiteSpace(oSCT_UserBE.CountryIds))
                        oSCT_UserBAL.UpdateCountriesBAL(oSCT_UserBE);

                    SaveVendorSettings();

                    oSCT_UserBE.Action = "UpdateModules";
                    oSCT_UserBE.UpdatedRoleBy = Convert.ToString(Session["Role"]).ToLower();
                    oSCT_UserBAL.UpdateModulesBAL(oSCT_UserBE);

                    if (Convert.ToString(ViewState["AccountStatus"]).Equals("Edited Awaiting Approval"))
                        this.SaveApprovalStatusVendor();
                    else
                        this.SaveApprovalStatus();
                }
                else if (userRoleId.Equals(1) && rblApprovalStatus.SelectedValue.Equals("reject"))
                {
                    if (Convert.ToString(ViewState["AccountStatus"]).Equals("Edited Awaiting Approval"))
                        this.SaveApprovalStatusVendor();
                    else
                        this.SaveApprovalStatus();
                }

                if (Session["Role"].ToString().ToLower().Equals("vendor"))
                {
                    this.VendorDisability();
                }
            }
            #endregion
        }
        else if (ViewState["UserRole"].ToString() == "3") //Carrier
        {
            #region Carrier save process ...
            bool IsCountryValid = false;
            bool IsCarrierCountryValid = true;

            foreach (RepeaterItem ritem in rptCarrierCountry.Items)
            {
                if (ritem.ItemType == ListItemType.Item || ritem.ItemType == ListItemType.AlternatingItem)
                {
                    CheckBox chkCountry = (CheckBox)ritem.FindControl("chkCountry");
                    if (chkCountry.Checked)
                    {
                        IsCountryValid = true;
                        HiddenField hdnCountryID = (HiddenField)ritem.FindControl("hdnCountryID");
                        oSCT_UserBE.CountryIds += hdnCountryID.Value + ",";
                        ucDropdownList ddlCarrier = (ucDropdownList)ritem.FindControl("ddlCarrier");
                        if (string.IsNullOrWhiteSpace(ddlCarrier.SelectedValue) || ddlCarrier.SelectedValue == "0")
                        {
                            IsCarrierCountryValid = false;
                            break;
                        }
                    }
                }
            }

            if (!IsCountryValid || !IsCarrierCountryValid)
            {
                if (!IsCountryValid)
                {
                    string SelectCountry = WebCommon.getGlobalResourceValue("SelectCountry");
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + SelectCountry + "')", true);
                }
                else if (!IsCarrierCountryValid)
                {
                    string MustSelectCarrier = WebCommon.getGlobalResourceValue("MustSelectCarrier");
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + MustSelectCarrier + "')", true);
                }
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + RequestConfirmationMessage + "')", true);

                this.SavePeronalDetails();

                oSCT_UserBE.Action = "UpdateCountries";
                if (!string.IsNullOrWhiteSpace(oSCT_UserBE.CountryIds))
                {
                    oSCT_UserBAL.UpdateCountriesBAL(oSCT_UserBE);
                }

                this.SaveCarrierCountrySettings();
                this.SaveCarrierSitesSettings();
                this.SaveApprovalStatus();
                //EncryptQueryString("SCT_UsersOverview.aspx");
            }
            #endregion
        }
    }

    protected void rptCountry_ItemCreated(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            CheckBox chkCountry = (CheckBox)e.Item.FindControl("chkCountry");
            chkCountry.AutoPostBack = true;
            chkCountry.CheckedChanged += new EventHandler(chkCountry_CheckedChanged);
        }
    }

    #endregion

    #region Methods ...

    public List<SCT_UserBE> lstUserSettings
    {
        get
        {
            if (ViewState["lstUserSettings"] != null)
            {
                return (List<SCT_UserBE>)ViewState["lstUserSettings"];
            }
            else
            {
                return new List<SCT_UserBE>();
            }
        }
        set
        {
            ViewState["lstUserSettings"] = value;
        }
    }

    public List<SCT_UserBE> lstCountrySettings
    {
        get
        {
            if (ViewState["lstCountrySettings"] != null)
            {
                return (List<SCT_UserBE>)ViewState["lstCountrySettings"];
            }
            else
            {
                return new List<SCT_UserBE>();
            }
        }
        set
        {
            ViewState["lstCountrySettings"] = value;
        }
    }

    private void BindOtherSettings()
    {
        SCT_UserBE oSCT_UserBE = new SCT_UserBE();
        oSCT_UserBE.Action = "GetCountrySpecificSettings";
        oSCT_UserBE.UpdatedRoleBy = Convert.ToString(Session["Role"]).ToLower();
        if (GetQueryStringValue("UserID") != null)
            oSCT_UserBE.UserID = Convert.ToInt32(GetQueryStringValue("UserID"));
        else
            oSCT_UserBE.UserID = Convert.ToInt32(Convert.ToString(Session["UserID"]));
        SCT_UserBAL oSCT_UserBAL = new SCT_UserBAL();
        lstUserSettings = oSCT_UserBAL.GetCountrySpecificSettingsBAL(oSCT_UserBE);

        List<SCT_UserBE> templstCountrySettings = this.lstCountrySettings;

        foreach (SCT_UserBE item in lstUserSettings)
        {
            if (templstCountrySettings.FindAll(x => x.CountryId == item.CountryId && (
                (x.VendorID == item.VendorID && ViewState["UserRole"].ToString() == "2") ||
                (x.CarrierID == item.CarrierID && ViewState["UserRole"].ToString() == "3"))).Count <= 0)
            {
                templstCountrySettings.Add(item);
            }
        }
        this.lstCountrySettings = templstCountrySettings;

        MAS_CountryBE oMAS_CountryBE = new MAS_CountryBE();
        MAS_CountryBAL oMAS_CountryBAL = new MAS_CountryBAL();

        oMAS_CountryBE.Action = "ShowAllCountry";
        oMAS_CountryBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
        oMAS_CountryBE.User.UserID = 0;

        List<MAS_CountryBE> lstCountry = new List<MAS_CountryBE>();
        lstCountry = oMAS_CountryBAL.GetCountryBAL(oMAS_CountryBE);

        if (ViewState["UserRole"].ToString() == "2") //Vendor
        {
            pnlVendorSction.Visible = true;

            rptCountry.DataSource = lstCountry;
            rptCountry.DataBind();
        }
        else if (ViewState["UserRole"].ToString() == "3") //Vendor
        {
            pnlCarrierSction.Visible = true;

            rptCarrierCountry.DataSource = lstCountry;
            rptCarrierCountry.DataBind();

            rptCarrierSitesCountry.DataSource = lstCountrySettings;
            rptCarrierSitesCountry.DataBind();
        }

        bindVendorAndModules();
    }

    private void bindVendorAndModules()
    {
        if (ViewState["UserRole"].ToString() == "2") //Vendor
        {
            ltVendorDetailsTextRegisterExternalUsers.Text = WebCommon.getGlobalResourceValue("VendorDetailsTextRegisterExternalUsers");
            rptVendorDetailsREU.DataSource = lstCountrySettings;
            rptVendorDetailsREU.DataBind();

            rptAppointmentCountry.DataSource = lstCountrySettings;
            rptAppointmentCountry.DataBind();

            rptDiscrepanciesCountry.DataSource = lstCountrySettings;
            rptDiscrepanciesCountry.DataBind();

            rptInvoiceIssueCountry.DataSource = lstCountrySettings;
            rptInvoiceIssueCountry.DataBind();

            rptOtifCountry.DataSource = lstCountrySettings;
            rptOtifCountry.DataBind();

            rptScorecardCountry.DataSource = lstCountrySettings;
            rptScorecardCountry.DataBind();
        }
        else if (ViewState["UserRole"].ToString() == "3") //Vendor
        {
            rptCarrierSitesCountry.DataSource = lstCountrySettings;
            rptCarrierSitesCountry.DataBind();
        }
    }

    private void BindLanguage()
    {
        SCT_UserBE oSCT_UserBE = new SCT_UserBE();
        SCT_UserBAL oSCT_UserBAL = new SCT_UserBAL();

        oSCT_UserBE.Action = "GetLanguage";
        List<SCT_UserBE> lstLanguage = oSCT_UserBAL.GetLanguagesBAL(oSCT_UserBE);
        oSCT_UserBAL = null;
        if (lstLanguage.Count > 0)
        {
            FillControls.FillDropDown(ref ddlLanguage, lstLanguage, "Language", "LanguageID", "--Select--");
        }

    }

    private void getUserDetail()
    {
        var oSCT_UserBAL = new SCT_UserBAL();
        var oSCT_UserBE = new SCT_UserBE();

        if (GetQueryStringValue("UserID") != null)
        {
            if (GetQueryStringValue("UStatus") == null)
            {
                if (GetQueryStringValue("ReadOnly") == "1")
                {
                    btnSave.Visible = false;
                    btnBack.Visible = false;
                }
                else
                {
                    btnSave.Visible = true;
                    btnBack.Visible = true;
                }

            }
            #region Get User Data In case of OD User updation...
            oSCT_UserBE.Action = "GetUserOverview";
            oSCT_UserBE.UserID = Convert.ToInt32(GetQueryStringValue("UserID").ToString());
            List<SCT_UserBE> lstUserDetail = oSCT_UserBAL.GetUsers(oSCT_UserBE);
            oSCT_UserBAL = null;
            if (lstUserDetail != null && lstUserDetail.Count > 0)
            {
                txtUserIDEmail.Text = lstUserDetail[0].LoginID;
                ViewState["loginid"] = lstUserDetail[0].LoginID;
                txtFirstName.Text = lstUserDetail[0].FirstName;
                txtLastName.Text = lstUserDetail[0].Lastname;
                ViewState["Password"] = lstUserDetail[0].Password;
                txtPassword.TextMode = TextBoxMode.SingleLine;
                ViewState["AccountStatus"] = lstUserDetail[0].AccountStatus;

                for (int i = 0; i < lstUserDetail[0].Password.Length; i++)
                    txtPassword.Text += "*";

                lblPassword.Visible = false;
                tdpassword.Visible = false;
                txtPassword.Visible = false;

                txtTelephone.Text = lstUserDetail[0].PhoneNumber;
                txtFax.Text = lstUserDetail[0].FaxNumber;
                if (ddlLanguage.Items.FindByValue(lstUserDetail[0].LanguageID.ToString()) != null)
                    ddlLanguage.SelectedValue = lstUserDetail[0].LanguageID.ToString();

                txtRejectComments.Text = lstUserDetail[0].RejectionComments;
                txtRejectComments.ReadOnly = true;

                ViewState["UserRole"] = lstUserDetail[0].UserRoleID.ToString();

                txtUserIDEmail.Enabled = false;
                txtPassword.Enabled = false;
                //Buttons Conditions in basis of Account Status
                if (lstUserDetail[0].AccountStatus.ToLower() == "blocked")
                {
                    if (GetQueryStringValue("ReadOnly") == "1")
                    {
                        btnUnblockUser.Visible = false;
                        btnDeleteUser.Visible = false;
                    }
                    else
                    {
                        btnUnblockUser.Visible = true;
                        btnDeleteUser.Visible = true;
                    }

                }
                if (lstUserDetail[0].AccountStatus.ToLower() == "active" && GetQueryStringValue("UStatus") == null)
                {
                    if (GetQueryStringValue("ReadOnly") == "1")
                    {
                        btnBlockUser.Visible = false;
                        btnDeleteUser.Visible = false;
                        btnResendApprovalMail.Visible = false;
                    }
                    else
                    {
                        btnBlockUser.Visible = true;
                        btnDeleteUser.Visible = true;
                        btnResendApprovalMail.Visible = true;
                    }

                }
                if (lstUserDetail[0].AccountStatus.ToLower() == "request rejected" && GetQueryStringValue("UStatus") == null)
                {
                    if (GetQueryStringValue("ReadOnly") == "1")
                    {
                        btnDeleteRejectedUser.Visible = false;
                    }
                    else
                    {
                        btnDeleteRejectedUser.Visible = true;
                    }

                    pnlRejectionComment.Style.Add("display", "block");
                }
                if (lstUserDetail[0].AccountStatus.ToLower() == "registered awaiting approval" && GetQueryStringValue("UStatus") == null)
                {
                    if (GetQueryStringValue("ReadOnly") == "1")
                    {
                        btnDeleteUser.Visible = false;
                    }
                    else
                    {
                        btnDeleteUser.Visible = true;
                    }

                }
                if (lstUserDetail[0].AccountStatus.ToLower() == "awaiting activation" && GetQueryStringValue("UStatus") == null)
                {
                    if (GetQueryStringValue("ReadOnly") == "1")
                    {
                        btnResendApprovalMail.Visible = false;
                        btnDeleteUser.Visible = false;
                    }
                    else
                    {
                        btnResendApprovalMail.Visible = true;
                        btnDeleteUser.Visible = true;
                    }

                }
            }
            #endregion
        }
        else
        {
            #region Get Vendor Data In case of Vendor Login ...
            oSCT_UserBE.Action = "GetUserOverview";
            oSCT_UserBE.UserID = Convert.ToInt32(Convert.ToString(Session["UserID"]));
            List<SCT_UserBE> lstUserDetail = oSCT_UserBAL.GetUsers(oSCT_UserBE);
            oSCT_UserBAL = null;
            if (lstUserDetail != null && lstUserDetail.Count > 0)
            {
                txtUserIDEmail.Text = lstUserDetail[0].LoginID;
                ViewState["loginid"] = lstUserDetail[0].LoginID;
                txtFirstName.Text = lstUserDetail[0].FirstName;
                txtLastName.Text = lstUserDetail[0].Lastname;
                ViewState["Password"] = lstUserDetail[0].Password;
                txtPassword.TextMode = TextBoxMode.SingleLine;
                ViewState["AccountStatus"] = lstUserDetail[0].AccountStatus;

                for (int i = 0; i < lstUserDetail[0].Password.Length; i++)
                    txtPassword.Text += "*";

                lblPassword.Visible = false;
                tdpassword.Visible = false;
                txtPassword.Visible = false;

                txtTelephone.Text = lstUserDetail[0].PhoneNumber;
                txtFax.Text = lstUserDetail[0].FaxNumber;
                if (ddlLanguage.Items.FindByValue(lstUserDetail[0].LanguageID.ToString()) != null)
                    ddlLanguage.SelectedValue = lstUserDetail[0].LanguageID.ToString();

                txtRejectComments.Text = lstUserDetail[0].RejectionComments;
                txtRejectComments.ReadOnly = true;

                ViewState["UserRole"] = lstUserDetail[0].UserRoleID.ToString();

                txtUserIDEmail.Enabled = false;
                txtPassword.Enabled = false;
                //Buttons Conditions in basis of Account Status
                //if (lstUserDetail[0].AccountStatus.ToLower() == "blocked")
                //{
                //    btnUnblockUser.Visible = true;
                //    btnDeleteUser.Visible = true;
                //}
                //if (lstUserDetail[0].AccountStatus.ToLower() == "active" && GetQueryStringValue("UStatus") == null)
                //{
                //    btnBlockUser.Visible = true;
                //    btnDeleteUser.Visible = true;
                //    btnResendApprovalMail.Visible = true;
                //}
                //if (lstUserDetail[0].AccountStatus.ToLower() == "request rejected" && GetQueryStringValue("UStatus") == null)
                //{
                //    btnDeleteRejectedUser.Visible = true;
                //    pnlRejectionComment.Style.Add("display", "block");
                //}
                //if (lstUserDetail[0].AccountStatus.ToLower() == "registered awaiting approval" && GetQueryStringValue("UStatus") == null)
                //{
                //    btnDeleteUser.Visible = true;
                //}
                //if (lstUserDetail[0].AccountStatus.ToLower() == "awaiting activation" && GetQueryStringValue("UStatus") == null)
                //{
                //    btnResendApprovalMail.Visible = true;
                //    btnDeleteUser.Visible = true;
                //}
            }
            #endregion
        }
    }

    private void SendUnblockCommunicationMail(string EmailID, string password)
    {
        sendCommunicationCommon osendCommunicationCommon = new sendCommunicationCommon();

        string htmlBody = getUnblockMailBody(password);
        string mailSubjectText = "Account Unblocked";

        clsEmail oclsEmail = new clsEmail();
        oclsEmail.sendMail(EmailID, htmlBody, mailSubjectText, Convert.ToString(ConfigurationManager.AppSettings["fromAddress"]), true);
        Page.UICulture = Convert.ToString(Session["CultureInfo"]);
    }

    private string getUnblockMailBody(string password)
    {
        string htmlBody = null;
        string Language = ddlLanguage.SelectedItem.Text;
        sendCommunicationCommon osendCommunicationCommon = new sendCommunicationCommon();

        templatePath = path + templatePath;

        //string LanguageFile = "UnblockUserAccount." + Language + ".htm";
        //if (!System.IO.File.Exists(templatePath + LanguageFile.ToLower()))
        //{
        //    LanguageFile = "UnblockUserAccount." + "english.htm";
        //}
        string LanguageFile = "UnblockUserAccount.english.htm";

        #region Setting reason as per the language ...
        switch (Language)
        {
            case clsConstants.English:
                Page.UICulture = clsConstants.EnglishISO; // "en-US"; // English
                break;
            case clsConstants.French:
                Page.UICulture = clsConstants.FranceISO; // // France
                break;
            case clsConstants.German:
                Page.UICulture = clsConstants.GermanyISO; //"de"; // German
                break;
            case clsConstants.Dutch:
                Page.UICulture = clsConstants.NederlandISO; //"nl"; // Dutch
                break;
            case clsConstants.Spanish:
                Page.UICulture = clsConstants.SpainISO; //"es"; // Spanish
                break;
            case clsConstants.Italian:
                Page.UICulture = clsConstants.ItalyISO; //"it"; // Italy
                break;
            case clsConstants.Czech:
                Page.UICulture = clsConstants.CzechISO; //"cz"; // Czech
                break;
            default:
                Page.UICulture = clsConstants.EnglishISO; //"en-US"; // English
                break;
        }

        #endregion

        using (StreamReader sReader = new StreamReader(templatePath + LanguageFile.ToLower()))
        {
            htmlBody = sReader.ReadToEnd();
            //  htmlBody = htmlBody.Replace("{password}", password);

            htmlBody = htmlBody.Replace("{Congratulations}", WebCommon.getGlobalResourceValue("Congratulations"));
            htmlBody = htmlBody.Replace("{UnblockUserAccText1}", WebCommon.getGlobalResourceValue("UnblockUserAccText1"));
            htmlBody = htmlBody.Replace("{Yoursfaithfully}", WebCommon.getGlobalResourceValue("Yoursfaithfully"));
            htmlBody = htmlBody.Replace("{OfficeDepots}", WebCommon.getGlobalResourceValue("OfficeDepots"));
        }
        return htmlBody;
    }

    protected void btnResendApprovalMail_Click(object sender, EventArgs e)
    {
        sendCommunicationCommon osendCommunicationCommon = new sendCommunicationCommon();

        string htmlBody = string.Empty;
        string mailSubjectText = string.Empty;

        htmlBody = getAcceptMailBody(txtUserIDEmail.Text.Trim());
        mailSubjectText = "Request Approved";
        clsEmail oclsEmail = new clsEmail();
        oclsEmail.sendMail(txtUserIDEmail.Text.Trim(), htmlBody, mailSubjectText, Convert.ToString(ConfigurationManager.AppSettings["fromAddress"]), true);
        Page.UICulture = Convert.ToString(Session["CultureInfo"]);
    }

    private string getAcceptMailBody(string EmailId)
    {
        string htmlBody = null;
        string Language = ddlLanguage.SelectedItem.Text;
        sendCommunicationCommon osendCommunicationCommon = new sendCommunicationCommon();

        templatePath = path + templatePath;

        //string LanguageFile = "ExternalUserCreation." + Language + ".htm";
        //if (!System.IO.File.Exists(templatePath + LanguageFile.ToLower()))
        //{
        //    LanguageFile = "ExternalUserCreation." + "english.htm";
        //}
        string LanguageFile = "ExternalUserCreation.english.htm";

        #region Setting reason as per the language ...
        switch (Language)
        {
            case clsConstants.English:
                Page.UICulture = clsConstants.EnglishISO; // "en-US"; // English
                break;
            case clsConstants.French:
                Page.UICulture = clsConstants.FranceISO; // // France
                break;
            case clsConstants.German:
                Page.UICulture = clsConstants.GermanyISO; //"de"; // German
                break;
            case clsConstants.Dutch:
                Page.UICulture = clsConstants.NederlandISO; //"nl"; // Dutch
                break;
            case clsConstants.Spanish:
                Page.UICulture = clsConstants.SpainISO; //"es"; // Spanish
                break;
            case clsConstants.Italian:
                Page.UICulture = clsConstants.ItalyISO; //"it"; // Italy
                break;
            case clsConstants.Czech:
                Page.UICulture = clsConstants.CzechISO; //"cz"; // Czech
                break;
            default:
                Page.UICulture = clsConstants.EnglishISO; //"en-US"; // English
                break;
        }

        #endregion

        using (StreamReader sReader = new StreamReader(templatePath + LanguageFile.ToLower()))
        {
            htmlBody = sReader.ReadToEnd();
            // htmlBody = htmlBody.Replace("{logoInnerPath}", osendCommunicationCommon.getAbsolutePath());

            htmlBody = htmlBody.Replace("{Congratulations}", WebCommon.getGlobalResourceValue("Congratulations"));
            htmlBody = htmlBody.Replace("{ExternalUserCreationText1}", WebCommon.getGlobalResourceValue("ExternalUserCreationText1"));

            htmlBody = htmlBody.Replace("{UserName}", WebCommon.getGlobalResourceValue("UserName"));
            htmlBody = htmlBody.Replace("{usernameValue}", EmailId);

            htmlBody = htmlBody.Replace("{ExternalUserCreationText2}", WebCommon.getGlobalResourceValue("ExternalUserCreationText2"));
            htmlBody = htmlBody.Replace("{ExternalUserCreationText3}", WebCommon.getGlobalResourceValue("ExternalUserCreationText3"));
            htmlBody = htmlBody.Replace("{ExternalUserCreationText4}", WebCommon.getGlobalResourceValue("ExternalUserCreationText4"));

            htmlBody = htmlBody.Replace("{ClickHereValue}", "<a href='" + sPortalLink + "'>" + sPortalLink + " </a>");

            htmlBody = htmlBody.Replace("{ExternalUserCreationText5}", WebCommon.getGlobalResourceValue("ExternalUserCreationText5"));
            htmlBody = htmlBody.Replace("{Yoursfaithfully}", WebCommon.getGlobalResourceValue("Yoursfaithfully"));
            htmlBody = htmlBody.Replace("{OfficeDepots}", WebCommon.getGlobalResourceValue("OfficeDepots"));
        }
        return htmlBody;
    }

    private void SavePeronalDetails()
    {
        // Save personal details
        SCT_UserBE oSCT_UserBE = new SCT_UserBE();
        SCT_UserBAL oSCT_UserBAL = new SCT_UserBAL();
        oSCT_UserBE.oStockPlannerGroupings = new StockPlannerGroupingsBE();
        oSCT_UserBE.Action = "GetUserOverview";
        if (GetQueryStringValue("UserID") != null)
            oSCT_UserBE.UserID = Convert.ToInt32(GetQueryStringValue("UserID"));
        else
            oSCT_UserBE.UserID = Convert.ToInt32(Convert.ToString(Session["UserID"]));

        List<SCT_UserBE> lstUserDetail = oSCT_UserBAL.GetUsers(oSCT_UserBE);
        if (lstUserDetail.Count > 0)
        {
            oSCT_UserBE = lstUserDetail[0];
        }

        oSCT_UserBE.EmailId = txtUserIDEmail.Text.Trim();
        oSCT_UserBE.LoginID = txtUserIDEmail.Text.Trim();
        oSCT_UserBE.Password = txtPassword.Text;
        oSCT_UserBE.FirstName = txtFirstName.Text.Trim();
        oSCT_UserBE.Lastname = txtLastName.Text.Trim();
        oSCT_UserBE.PhoneNumber = txtTelephone.Text.Trim();
        oSCT_UserBE.FaxNumber = txtFax.Text.Trim();
        oSCT_UserBE.LanguageID = ddlLanguage.SelectedIndex > 0 ? Convert.ToInt32(ddlLanguage.SelectedItem.Value) : (int?)null;

        oSCT_UserBE.Action = "UpdateExternalUser";
        oSCT_UserBAL.UpdateUserBAL(oSCT_UserBE);
    }

    private void SaveCarrierCountrySettings()
    {
        // Save country sepcific details
        SCT_UserBE oSCT_UserBE = new SCT_UserBE();
        SCT_UserBAL oSCT_UserBAL = new SCT_UserBAL();

        // Save vendor/carrier details
        oSCT_UserBE.Action = "UpdateUserCountry";
        //oSCT_UserBE.UserID = Convert.ToInt32(GetQueryStringValue("UserID"));
        if (GetQueryStringValue("UserID") != null)
            oSCT_UserBE.UserID = Convert.ToInt32(GetQueryStringValue("UserID"));
        else
            oSCT_UserBE.UserID = Convert.ToInt32(Convert.ToString(Session["UserID"]));

        foreach (RepeaterItem ritem in rptCarrierCountry.Items)
        {
            if (ritem.ItemType == ListItemType.Item || ritem.ItemType == ListItemType.AlternatingItem)
            {
                CheckBox chkCountry = (CheckBox)ritem.FindControl("chkCountry");
                if (chkCountry.Checked)
                {
                    HiddenField hdnCountryID = (HiddenField)ritem.FindControl("hdnCountryID");
                    oSCT_UserBE.CountryId = Convert.ToInt32(hdnCountryID.Value);
                    ucDropdownList ddlCarrier = (ucDropdownList)ritem.FindControl("ddlCarrier");
                    if (!string.IsNullOrWhiteSpace(ddlCarrier.SelectedValue))
                    {
                        oSCT_UserBE.CarrierID = Convert.ToInt32(ddlCarrier.SelectedValue);
                        oSCT_UserBAL.UpdateUserCountryBAL(oSCT_UserBE);
                    }
                }
            }
        }
    }

    private void SaveCarrierSitesSettings()
    {
        // Save module details
        SCT_UserBAL oSCT_UserBAL = new SCT_UserBAL();
        SCT_UserBE oSCT_UserBE = new SCT_UserBE();
        oSCT_UserBE.Action = "UpdateModules";
        //oSCT_UserBE.UserID = Convert.ToInt32(GetQueryStringValue("UserID"));
        if (GetQueryStringValue("UserID") != null)
            oSCT_UserBE.UserID = Convert.ToInt32(GetQueryStringValue("UserID"));
        else
            oSCT_UserBE.UserID = Convert.ToInt32(Convert.ToString(Session["UserID"]));

        foreach (RepeaterItem citem in rptCarrierSitesCountry.Items)
        {
            if (citem.ItemType == ListItemType.Item || citem.ItemType == ListItemType.AlternatingItem)
            {
                Repeater rptCarrierSites = (Repeater)citem.FindControl("rptCarrierSites");
                if (rptCarrierSites != null)
                {
                    foreach (RepeaterItem sitem in rptCarrierSites.Items)
                    {
                        if (sitem.ItemType == ListItemType.Item || sitem.ItemType == ListItemType.AlternatingItem)
                        {
                            CheckBox chkSite = (CheckBox)sitem.FindControl("chkSite");
                            if (chkSite.Checked)
                            {
                                HiddenField hdnSiteID = (HiddenField)sitem.FindControl("hdnSiteID");
                                oSCT_UserBE.AppointmentSites += hdnSiteID.Value + ",";
                            }
                        }
                    }
                }
            }
        }

        oSCT_UserBAL.UpdateModulesBAL(oSCT_UserBE);
    }

    private void SaveVendorSettings()
    {
        // Save vendor/carrier details
        SCT_UserBAL oSCT_UserBAL = new SCT_UserBAL();
        SCT_UserBE oSCT_UserBE = new SCT_UserBE();
        oSCT_UserBE.Action = "UpdateUserCountry";
        //oSCT_UserBE.UserID = Convert.ToInt32(GetQueryStringValue("UserID"));
        if (GetQueryStringValue("UserID") != null)
            oSCT_UserBE.UserID = Convert.ToInt32(GetQueryStringValue("UserID"));
        else
            oSCT_UserBE.UserID = Convert.ToInt32(Convert.ToString(Session["UserID"]));

        foreach (RepeaterItem item in rptVendorDetailsREU.Items)
        {
            if (item.ItemType == ListItemType.Item || item.ItemType == ListItemType.AlternatingItem)
            {
                HiddenField hdnCountryIdVendorDetailsREU = (HiddenField)item.FindControl("hdnCountryIdVendorDetailsREU");
                oSCT_UserBE.CountryId = Convert.ToInt32(hdnCountryIdVendorDetailsREU.Value);

                HiddenField hdnVendorID = (HiddenField)item.FindControl("hdnVendorID");

                string VendorId = hdnVendorID.Value;
                if (VendorId != string.Empty)
                {
                    int VendId;
                    if (Int32.TryParse(VendorId, out VendId))
                    {
                        oSCT_UserBE.UpdatedRoleBy = Convert.ToString(Session["Role"]).ToLower();
                        oSCT_UserBE.VendorID = VendId;
                        oSCT_UserBAL.UpdateUserCountryBAL(oSCT_UserBE);
                    }
                }
            }
        }
    }

    private bool SaveModuleSettings()
    {
        // Save module details
        SCT_UserBAL oSCT_UserBAL = new SCT_UserBAL();
        SCT_UserBE oSCT_UserBE = new SCT_UserBE();
        oSCT_UserBE.Action = "UpdateModules";
        //oSCT_UserBE.UserID = Convert.ToInt32(GetQueryStringValue("UserID"));
        if (GetQueryStringValue("UserID") != null)
            oSCT_UserBE.UserID = Convert.ToInt32(GetQueryStringValue("UserID"));
        else
            oSCT_UserBE.UserID = Convert.ToInt32(Convert.ToString(Session["UserID"]));

        List<UserCountryModulesSettingsExternal> lstUserCountryModulesSettings = new List<UserCountryModulesSettingsExternal>();
        UserCountryModulesSettingsExternal oUserCountryModulesSettings;
        foreach (RepeaterItem citem in rptCountry.Items)
        {
            if (citem.ItemType == ListItemType.Item || citem.ItemType == ListItemType.AlternatingItem)
            {
                CheckBox chkCountry = (CheckBox)citem.FindControl("chkCountry");
                if (chkCountry.Checked)
                {
                    HiddenField hdnCountryID = (HiddenField)citem.FindControl("hdnCountryID");
                    oUserCountryModulesSettings = new UserCountryModulesSettingsExternal();
                    oUserCountryModulesSettings.CountryID = Convert.ToInt32(hdnCountryID.Value);
                    lstUserCountryModulesSettings.Add(oUserCountryModulesSettings);
                }
            }
        }

        foreach (RepeaterItem citem in rptAppointmentCountry.Items)
        {
            if (citem.ItemType == ListItemType.Item || citem.ItemType == ListItemType.AlternatingItem)
            {
                HiddenField hdnCountryID = (HiddenField)citem.FindControl("hdnCountryID");
                lstUserCountryModulesSettings.Find(x => x.CountryID == Convert.ToInt32(hdnCountryID.Value)).IsSchedulingDefined = false;

                Repeater rptAppointmentCountrySites = (Repeater)citem.FindControl("rptAppointmentCountrySites");
                if (rptAppointmentCountrySites != null)
                {
                    foreach (RepeaterItem sitem in rptAppointmentCountrySites.Items)
                    {
                        if (sitem.ItemType == ListItemType.Item || sitem.ItemType == ListItemType.AlternatingItem)
                        {
                            CheckBox chkSite = (CheckBox)sitem.FindControl("chkSite");
                            if (chkSite.Checked)
                            {
                                HiddenField hdnSiteID = (HiddenField)sitem.FindControl("hdnSiteID");
                                oSCT_UserBE.AppointmentSites += hdnSiteID.Value + ",";
                                lstUserCountryModulesSettings.Find(x => x.CountryID == Convert.ToInt32(hdnCountryID.Value)).IsSchedulingDefined = true;
                            }
                        }
                    }
                }
            }
        }

        foreach (RepeaterItem citem in rptDiscrepanciesCountry.Items)
        {
            if (citem.ItemType == ListItemType.Item || citem.ItemType == ListItemType.AlternatingItem)
            {
                HiddenField hdnCountryID = (HiddenField)citem.FindControl("hdnCountryID");
                lstUserCountryModulesSettings.Find(x => x.CountryID == Convert.ToInt32(hdnCountryID.Value)).IsDiscrepancyDefined = false;

                Repeater rptDiscrepanciesCountrySites = (Repeater)citem.FindControl("rptDiscrepanciesCountrySites");
                if (rptDiscrepanciesCountrySites != null)
                {
                    foreach (RepeaterItem sitem in rptDiscrepanciesCountrySites.Items)
                    {
                        if (sitem.ItemType == ListItemType.Item || sitem.ItemType == ListItemType.AlternatingItem)
                        {
                            CheckBox chkSite = (CheckBox)sitem.FindControl("chkSite");
                            if (chkSite.Checked)
                            {
                                HiddenField hdnSiteID = (HiddenField)sitem.FindControl("hdnSiteID");
                                oSCT_UserBE.DiscrepancySites += hdnSiteID.Value + ",";
                                lstUserCountryModulesSettings.Find(x => x.CountryID == Convert.ToInt32(hdnCountryID.Value)).IsDiscrepancyDefined = true;
                            }
                        }
                    }
                }
            }
        }


        foreach (RepeaterItem citem in rptInvoiceIssueCountry.Items) {
            if (citem.ItemType == ListItemType.Item || citem.ItemType == ListItemType.AlternatingItem) {
                HiddenField hdnCountryID = (HiddenField)citem.FindControl("hdnCountryID");
                lstUserCountryModulesSettings.Find(x => x.CountryID == Convert.ToInt32(hdnCountryID.Value)).IsInvoiceIssueDefined = false;

                Repeater rptInvoiceIssueCountrySites = (Repeater)citem.FindControl("rptInvoiceIssueCountrySites");
                if (rptInvoiceIssueCountrySites != null) {
                    foreach (RepeaterItem sitem in rptInvoiceIssueCountrySites.Items) {
                        if (sitem.ItemType == ListItemType.Item || sitem.ItemType == ListItemType.AlternatingItem) {
                            CheckBox chkSite = (CheckBox)sitem.FindControl("chkSite");
                            if (chkSite.Checked) {
                                HiddenField hdnSiteID = (HiddenField)sitem.FindControl("hdnSiteID");
                                oSCT_UserBE.InvoiceIssueSites += hdnSiteID.Value + ",";
                                lstUserCountryModulesSettings.Find(x => x.CountryID == Convert.ToInt32(hdnCountryID.Value)).IsInvoiceIssueDefined = true;
                            }
                        }
                    }
                }
            }
        }

        foreach (RepeaterItem citem in rptOtifCountry.Items)
        {
            if (citem.ItemType == ListItemType.Item || citem.ItemType == ListItemType.AlternatingItem)
            {
                HiddenField hdnCountryID = (HiddenField)citem.FindControl("hdnCountryID");
                lstUserCountryModulesSettings.Find(x => x.CountryID == Convert.ToInt32(hdnCountryID.Value)).IsOTIFDefined = false;

                CheckBox chkCountry = (CheckBox)citem.FindControl("chkCountry");
                if (chkCountry.Checked)
                {
                    oSCT_UserBE.OTIFCountries += hdnCountryID.Value + ",";
                    lstUserCountryModulesSettings.Find(x => x.CountryID == Convert.ToInt32(hdnCountryID.Value)).IsOTIFDefined = true;
                }
            }
        }

        foreach (RepeaterItem citem in rptScorecardCountry.Items)
        {
            if (citem.ItemType == ListItemType.Item || citem.ItemType == ListItemType.AlternatingItem)
            {
                HiddenField hdnCountryID = (HiddenField)citem.FindControl("hdnCountryID");
                lstUserCountryModulesSettings.Find(x => x.CountryID == Convert.ToInt32(hdnCountryID.Value)).IsScorecardDefined = false;

                CheckBox chkCountry = (CheckBox)citem.FindControl("chkCountry");
                if (chkCountry.Checked)
                {
                    oSCT_UserBE.ScorecardCountries += hdnCountryID.Value + ",";
                    lstUserCountryModulesSettings.Find(x => x.CountryID == Convert.ToInt32(hdnCountryID.Value)).IsScorecardDefined = true;
                }
            }
        }

        if (lstUserCountryModulesSettings.FindAll(x => x.IsSchedulingDefined == false
            && x.IsDiscrepancyDefined == false && x.IsOTIFDefined == false && x.IsScorecardDefined == false).Count > 0)
        {
            return false;
        }
        else
        {
            oSCT_UserBAL.UpdateModulesBAL(oSCT_UserBE);
            return true;
        }
    }

    private void SetVendorDetailsRepeaterData()
    {
        for (int intIndex = 0; intIndex < rptVendorDetailsREU.Items.Count; intIndex++)
        {
            ucListBox lstRight = (ucListBox)rptVendorDetailsREU.Items[intIndex].FindControl("lstRight");
            HiddenField hiddenSearchedVendorText = (HiddenField)rptVendorDetailsREU.Items[intIndex].FindControl("hiddenSearchedVendorText");
            HiddenField hiddenSearchedVendorValue = (HiddenField)rptVendorDetailsREU.Items[intIndex].FindControl("hiddenSearchedVendorValue");

            if (lstRight != null && lstRight.Items.Count <= 0 && !string.IsNullOrWhiteSpace(hiddenSearchedVendorText.Value) &&
                !string.IsNullOrWhiteSpace(hiddenSearchedVendorValue.Value))
            {
                lstRight.Items.Add(new ListItem(hiddenSearchedVendorText.Value, hiddenSearchedVendorValue.Value));
            }
        }
    }

    private void BindVendor(ucListBox lstucListBox, string strVendorName, int intCountryId)
    {
        UP_VendorBAL oUP_VendorBAL = new UP_VendorBAL();
        UP_VendorBE oUP_VendorBE = new UP_VendorBE();

        oUP_VendorBE.Action = "GetVendorByVendorSearchText";
        oUP_VendorBE.CountryID = intCountryId;
        oUP_VendorBE.VendorName = strVendorName;
        List<UP_VendorBE> lstUPVendor = new List<UP_VendorBE>();
        lstUPVendor = oUP_VendorBAL.GetVendorByVendorSearchBAL(oUP_VendorBE);
        if (lstUPVendor.Count > 0)
        {
            FillControls.FillListBox(ref lstucListBox, lstUPVendor, "VendorName", "VendorID");
        }
        else
        {
            lstucListBox.Items.Clear();
        }
    }

    private void ButtonEnability()
    {        
        if (GetQueryStringValue("UStatus") != null)
        {
            btnSave.Visible = false;
            btnResendApprovalMail.Visible = false;
            btnBlockUser.Visible = false;
            btnUnblockUser.Visible = false;
            btnDeleteRejectedUser.Visible = false;
            btnDeleteUser.Visible = false;
            btnBack.Visible = true;
            btnSave_1.Visible = true;
            pnlApprove.Visible = true;
            lblRegisterAsNewUserEditNew.Text = WebCommon.getGlobalResourceValue("RegisterAsNewUserEdit");
        }
        else if (GetQueryStringValue("UStatus") == null && GetQueryStringValue("UserID") == null)
        {
            btnSave.Visible = false;
            btnResendApprovalMail.Visible = false;
            btnBlockUser.Visible = false;
            btnUnblockUser.Visible = false;
            btnDeleteRejectedUser.Visible = false;
            btnDeleteUser.Visible = false;
            btnBack.Visible = true;
            btnSave_1.Visible = true;
            pnlApprove.Visible = false;
            lblRegisterAsNewUserEditNew.Text = WebCommon.getGlobalResourceValue("AmendmyAccountDetails");
        }
        else if (GetQueryStringValue("ReadOnly") == "1")
        {
            btnSave.Visible = false;
            btnResendApprovalMail.Visible = false;
            btnBlockUser.Visible = false;
            btnUnblockUser.Visible = false;
            btnDeleteRejectedUser.Visible = false;
            btnDeleteUser.Visible = false;
            btnBack.Visible = false;
            btnSave_1.Visible = false;
            pnlApprove.Visible = false;
            lblRegisterAsNewUserEditNew.Text = WebCommon.getGlobalResourceValue("RegisterAsNewUserEdit");
        }
        else
        {
            btnSave.Visible = true;
            btnResendApprovalMail.Visible = true;
            btnBlockUser.Visible = true;
            btnUnblockUser.Visible = true;
            btnDeleteRejectedUser.Visible = true;
            btnDeleteUser.Visible = true;
            btnBack.Visible = true;
            btnSave_1.Visible = false;
            pnlApprove.Visible = false;
            lblRegisterAsNewUserEditNew.Text = WebCommon.getGlobalResourceValue("RegisterAsNewUserEdit");
        }
    }

    private void SaveApprovalStatus()
    {
        var oSCT_UserBE = new SCT_UserBE();
        var oSCT_UserBAL = new SCT_UserBAL();
        oSCT_UserBE.oStockPlannerGroupings = new StockPlannerGroupingsBE();
        if (GetQueryStringValue("UserID").IsNumeric())
        {
            oSCT_UserBE.Action = "ApproveOrRejectUser";
            if (GetQueryStringValue("UserID") != null)
            {
                oSCT_UserBE.UserID = Convert.ToInt32(GetQueryStringValue("UserID"));

                oSCT_UserBE.AccountStatus = rblApprovalStatus.SelectedValue == "approve" ? "Awaiting Activation" : "Request Rejected";
                oSCT_UserBE.RejectionComments = rblApprovalStatus.SelectedValue == "approve" ? null : txtRejectCommentsR.Text;

                int? iResult = oSCT_UserBAL.UpdateAccountStatus(oSCT_UserBE);
                if (iResult != null && iResult > 0 || iResult == -1)
                {
                    // send communication mail from here. both for approval hand rejection.
                    SendCommunicationMail(txtUserIDEmail.Text);

                    // Delete Permanantly
                    if (rblApprovalStatus.SelectedValue != "approve")
                    {
                        oSCT_UserBE.Action = "DeleteRejectedUser";
                        oSCT_UserBAL.addEditUserBAL(oSCT_UserBE);
                    }

                    oSCT_UserBAL = null;
                    if (iResult == 0)
                    {
                        EncryptQueryString("SCT_UsersOverview.aspx");
                    }

                    if (ViewState["VendorID"] != null)
                    {
                        if (GetQueryStringValue("VendorID") != null)
                            EncryptQueryString("../GlobalSettings/VendorEdit.aspx?VendorID=" + ViewState["VendorID"]);
                        else
                            EncryptQueryString("SCT_UsersOverview.aspx?NewLoginRequest=Y");//to show Registered Awaiting Records
                    }
                    else
                    {
                        if (GetQueryStringValue("UStatus") != null && GetQueryStringValue("UStatus") == "PRAA")
                            EncryptQueryString("SCT_PendingUsersOverview.aspx");
                        else
                            EncryptQueryString("SCT_UsersOverview.aspx?NewLoginRequest=Y");
                    }
                }
            }
        }
    }

    private void SaveApprovalStatusVendor()
    {
        var oSCT_UserBE = new SCT_UserBE();
        var oSCT_UserBAL = new SCT_UserBAL();
        oSCT_UserBE.oStockPlannerGroupings = new StockPlannerGroupingsBE();
        if (GetQueryStringValue("UserID").IsNumeric())
        {
            oSCT_UserBE.Action = "ApproveOrRejectUser";
            if (GetQueryStringValue("UserID") != null)
            {
                oSCT_UserBE.UserID = Convert.ToInt32(GetQueryStringValue("UserID"));

                if (Convert.ToString(ViewState["AccountStatus"]).Equals("Edited Awaiting Approval"))
                    oSCT_UserBE.AccountStatus = "Active";
                else
                    oSCT_UserBE.AccountStatus = rblApprovalStatus.SelectedValue == "approve" ? "Awaiting Activation" : "Request Rejected";

                oSCT_UserBE.RejectionComments = rblApprovalStatus.SelectedValue == "approve" ? null : txtRejectCommentsR.Text;

                int? iResult = oSCT_UserBAL.UpdateAccountStatus(oSCT_UserBE);
                if (iResult != null && iResult > 0 || iResult == -1)
                {
                    // send communication mail from here. both for approval hand rejection.
                    SendCommunicationMailVendor(txtUserIDEmail.Text);

                    // Delete Permanantly
                    if (rblApprovalStatus.SelectedValue.Equals("reject") && !Convert.ToString(ViewState["AccountStatus"]).Equals("Edited Awaiting Approval"))
                    {
                        oSCT_UserBE.Action = "DeleteRejectedUser";
                        oSCT_UserBAL.addEditUserBAL(oSCT_UserBE);
                    }
                    else if ((rblApprovalStatus.SelectedValue.Equals("reject") || rblApprovalStatus.SelectedValue.Equals("approve"))
                        && Convert.ToString(ViewState["AccountStatus"]).Equals("Edited Awaiting Approval"))
                    {
                        oSCT_UserBE.Action = "DeleteUserPendingRejectedData";
                        oSCT_UserBE.UserID = Convert.ToInt32(GetQueryStringValue("UserID"));
                        oSCT_UserBAL.DeleteUserPendingRejectedDataBAL(oSCT_UserBE);
                    }


                    oSCT_UserBAL = null;
                    if (iResult == 0)
                    {
                        EncryptQueryString("SCT_UsersOverview.aspx");
                    }

                    if (ViewState["VendorID"] != null)
                    {
                        if (GetQueryStringValue("VendorID") != null)
                            EncryptQueryString("../GlobalSettings/VendorEdit.aspx?VendorID=" + ViewState["VendorID"]);
                        else
                            EncryptQueryString("SCT_UsersOverview.aspx?NewLoginRequest=Y");//to show Registered Awaiting Records
                    }
                    else
                    {
                        if (GetQueryStringValue("UStatus") != null && GetQueryStringValue("UStatus") == "PRAA")
                            EncryptQueryString("SCT_PendingUsersOverview.aspx");
                        else
                            EncryptQueryString("SCT_UsersOverview.aspx?NewLoginRequest=Y");
                    }
                }
            }
        }
        #region Cpmmented code ...
        //else if (GetQueryStringValue("UserID") == null && Session["UserID"] != null)
        //{
        //    //Edited Awaiting Approval
        //    oSCT_UserBE.Action = "UpdateVendorStatusByVendor";
        //    if (Session["UserID"] != null)
        //    {
        //        oSCT_UserBE.UserID = Convert.ToInt32(Session["UserID"]);
        //        oSCT_UserBE.AccountStatus = "Edited Awaiting Approval";
        //        int? iResult = oSCT_UserBAL.UpdateAccountStatus(oSCT_UserBE);
        //        if (iResult != null && iResult > 0 || iResult == -1) { }
        //    }
        //}
        #endregion
    }

    private void SendCommunicationMail(string LoginID)
    {
        sendCommunicationCommon osendCommunicationCommon = new sendCommunicationCommon();

        string htmlBody = string.Empty;
        string mailSubjectText = string.Empty;
        if (rblApprovalStatus.SelectedValue == "approve")
        {
            htmlBody = getAcceptMailBody();
            mailSubjectText = "Request Approved";
        }
        else
        {
            htmlBody = getRejectionMailBody();
            mailSubjectText = "Request Rejected";
        }
        clsEmail oclsEmail = new clsEmail();
        oclsEmail.sendMail(LoginID, htmlBody, mailSubjectText, Convert.ToString(ConfigurationManager.AppSettings["fromAddress"]), true);
        Page.UICulture = Convert.ToString(Session["CultureInfo"]);
    }

    private void SendCommunicationMailVendor(string LoginID)
    {
        sendCommunicationCommon osendCommunicationCommon = new sendCommunicationCommon();

        string htmlBody = string.Empty;
        string mailSubjectText = string.Empty;
        if (rblApprovalStatus.SelectedValue.Equals("approve"))
        {
            htmlBody = getAcceptMailBodyVendor();
            mailSubjectText = "Request Approved";
        }
        else
        {
            htmlBody = getRejectionMailBodyVendor();
            mailSubjectText = "Request Rejected";
        }
        clsEmail oclsEmail = new clsEmail();
        oclsEmail.sendMail(LoginID, htmlBody, mailSubjectText, Convert.ToString(ConfigurationManager.AppSettings["fromAddress"]), true);
        Page.UICulture = Convert.ToString(Session["CultureInfo"]);
    }

    private string getAcceptMailBody()
    {
        string htmlBody = null;
        string Language = ddlLanguage.SelectedItem.Text;
        sendCommunicationCommon osendCommunicationCommon = new sendCommunicationCommon();

        templatePath = path + templatePath;

        //string LanguageFile = "ExternalUserCreation." + Language + ".htm";
        //if (!System.IO.File.Exists(templatePath + LanguageFile.ToLower()))
        //{
        //    LanguageFile = "ExternalUserCreation." + "english.htm";
        //}
        string LanguageFile = "ExternalUserCreation.english.htm";

        #region Setting reason as per the language ...
        switch (Language)
        {
            case clsConstants.English:
                Page.UICulture = clsConstants.EnglishISO; // "en-US"; // English
                break;
            case clsConstants.French:
                Page.UICulture = clsConstants.FranceISO; // // France
                break;
            case clsConstants.German:
                Page.UICulture = clsConstants.GermanyISO; //"de"; // German
                break;
            case clsConstants.Dutch:
                Page.UICulture = clsConstants.NederlandISO; //"nl"; // Dutch
                break;
            case clsConstants.Spanish:
                Page.UICulture = clsConstants.SpainISO; //"es"; // Spanish
                break;
            case clsConstants.Italian:
                Page.UICulture = clsConstants.ItalyISO; //"it"; // Italy
                break;
            case clsConstants.Czech:
                Page.UICulture = clsConstants.CzechISO; //"cz"; // Czech
                break;
            default:
                Page.UICulture = clsConstants.EnglishISO; //"en-US"; // English
                break;
        }

        #endregion

        using (StreamReader sReader = new StreamReader(templatePath + LanguageFile.ToLower()))
        {
            htmlBody = sReader.ReadToEnd();
            // htmlBody = htmlBody.Replace("{logoInnerPath}", osendCommunicationCommon.getAbsolutePath());

            htmlBody = htmlBody.Replace("{Congratulations}", WebCommon.getGlobalResourceValue("Congratulations"));
            htmlBody = htmlBody.Replace("{ExternalUserCreationText1}", WebCommon.getGlobalResourceValue("ExternalUserCreationText1"));

            htmlBody = htmlBody.Replace("{UserName}", WebCommon.getGlobalResourceValue("UserName"));
            htmlBody = htmlBody.Replace("{usernameValue}", ViewState["loginid"].ToString());

            htmlBody = htmlBody.Replace("{ExternalUserCreationText2}", WebCommon.getGlobalResourceValue("ExternalUserCreationText2"));
            htmlBody = htmlBody.Replace("{ExternalUserCreationText3}", WebCommon.getGlobalResourceValue("ExternalUserCreationText3"));
            htmlBody = htmlBody.Replace("{ExternalUserCreationText4}", WebCommon.getGlobalResourceValue("ExternalUserCreationText4"));

            htmlBody = htmlBody.Replace("{ClickHereValue}", "<a href='" + sPortalLink + "'>" + sPortalLink + " </a>");

            htmlBody = htmlBody.Replace("{ExternalUserCreationText5}", WebCommon.getGlobalResourceValue("ExternalUserCreationText5"));
            htmlBody = htmlBody.Replace("{Yoursfaithfully}", WebCommon.getGlobalResourceValue("Yoursfaithfully"));
            htmlBody = htmlBody.Replace("{OfficeDepots}", WebCommon.getGlobalResourceValue("OfficeDepots"));
        }
        return htmlBody;
    }

    private string getRejectionMailBody()
    {
        string htmlBody = null;
        string Language = ddlLanguage.SelectedItem.Text;
        sendCommunicationCommon osendCommunicationCommon = new sendCommunicationCommon();

        templatePath = path + templatePath;

        //string LanguageFile = "ExternalUserRejection." + Language + ".htm";
        //if (!System.IO.File.Exists(templatePath + LanguageFile.ToLower()))
        //{
        //    LanguageFile = "ExternalUserRejection." + "english.htm";
        //}
        string LanguageFile = "ExternalUserRejection.english.htm";

        #region Setting reason as per the language ...
        switch (Language)
        {
            case clsConstants.English:
                Page.UICulture = clsConstants.EnglishISO; // "en-US"; // English
                break;
            case clsConstants.French:
                Page.UICulture = clsConstants.FranceISO; // // France
                break;
            case clsConstants.German:
                Page.UICulture = clsConstants.GermanyISO; //"de"; // German
                break;
            case clsConstants.Dutch:
                Page.UICulture = clsConstants.NederlandISO; //"nl"; // Dutch
                break;
            case clsConstants.Spanish:
                Page.UICulture = clsConstants.SpainISO; //"es"; // Spanish
                break;
            case clsConstants.Italian:
                Page.UICulture = clsConstants.ItalyISO; //"it"; // Italy
                break;
            case clsConstants.Czech:
                Page.UICulture = clsConstants.CzechISO; //"cz"; // Czech
                break;
            default:
                Page.UICulture = clsConstants.EnglishISO; //"en-US"; // English
                break;
        }

        #endregion

        using (StreamReader sReader = new StreamReader(templatePath + LanguageFile.ToLower()))
        {
            htmlBody = sReader.ReadToEnd();
            // htmlBody = htmlBody.Replace("{logoInnerPath}", osendCommunicationCommon.getAbsolutePath());

            htmlBody = htmlBody.Replace("{DearSirMadam}", WebCommon.getGlobalResourceValue("DearSirMadam"));
            htmlBody = htmlBody.Replace("{ExternalUserRejText1}", WebCommon.getGlobalResourceValue("ExternalUserRejText1"));

            htmlBody = htmlBody.Replace("{rejectioncommentsValue}", txtRejectCommentsR.Text.Trim());

            htmlBody = htmlBody.Replace("{ExternalUserRejText2}", WebCommon.getGlobalResourceValue("ExternalUserRejText2"));
            htmlBody = htmlBody.Replace("{Yoursfaithfully}", WebCommon.getGlobalResourceValue("Yoursfaithfully"));
            htmlBody = htmlBody.Replace("{OfficeDepots}", WebCommon.getGlobalResourceValue("OfficeDepots"));
        }
        return htmlBody;
    }

    private string getAcceptMailBodyVendor()
    {
        string htmlBody = null;
        string Language = ddlLanguage.SelectedItem.Text;
        sendCommunicationCommon osendCommunicationCommon = new sendCommunicationCommon();

        templatePath = path + templatePath;

        //string LanguageFile = "UserApprove." + Language + ".htm";
        //if (!System.IO.File.Exists(templatePath + LanguageFile.ToLower()))
        //{
        //    LanguageFile = "UserApprove." + "english.htm";
        //}
        string LanguageFile = "UserApprove." + "english.htm";

        #region Setting reason as per the language ...
        switch (Language)
        {
            case clsConstants.English:
                Page.UICulture = clsConstants.EnglishISO; // "en-US"; // English
                break;
            case clsConstants.French:
                Page.UICulture = clsConstants.FranceISO; // // France
                break;
            case clsConstants.German:
                Page.UICulture = clsConstants.GermanyISO; //"de"; // German
                break;
            case clsConstants.Dutch:
                Page.UICulture = clsConstants.NederlandISO; //"nl"; // Dutch
                break;
            case clsConstants.Spanish:
                Page.UICulture = clsConstants.SpainISO; //"es"; // Spanish
                break;
            case clsConstants.Italian:
                Page.UICulture = clsConstants.ItalyISO; //"it"; // Italy
                break;
            case clsConstants.Czech:
                Page.UICulture = clsConstants.CzechISO; //"cz"; // Czech
                break;
            default:
                Page.UICulture = clsConstants.EnglishISO; //"en-US"; // English
                break;
        }

        #endregion

        using (StreamReader sReader = new StreamReader(templatePath + LanguageFile.ToLower()))
        {
            htmlBody = sReader.ReadToEnd();
            htmlBody = htmlBody.Replace("{DearSirMadam}", WebCommon.getGlobalResourceValue("DearSirMadam"));
            htmlBody = htmlBody.Replace("{UserApproveText1}", WebCommon.getGlobalResourceValue("UserApproveText1"));
            htmlBody = htmlBody.Replace("{ManyThanks}", WebCommon.getGlobalResourceValue("ManyThanks"));

            htmlBody = htmlBody.Replace("{AccountDate}", System.DateTime.Now.ToString("dd/MM/yyyy"));
            htmlBody = htmlBody.Replace("{VIPAdminNameValue}", Convert.ToString(Session["UserName"]));
            htmlBody = htmlBody.Replace("{VIPAdminEmailValue}", Convert.ToString(Session["LoginID"]));
        }
        return htmlBody;
    }

    private string getRejectionMailBodyVendor()
    {
        string htmlBody = null;
        string Language = ddlLanguage.SelectedItem.Text;
        sendCommunicationCommon osendCommunicationCommon = new sendCommunicationCommon();

        templatePath = path + templatePath;

        //string LanguageFile = "UserReject." + Language + ".htm";
        //if (!System.IO.File.Exists(templatePath + LanguageFile.ToLower()))
        //{
        //    LanguageFile = "UserReject." + "english.htm";
        //}
        string LanguageFile = "UserReject.english.htm";

        #region Setting reason as per the language ...
        switch (Language)
        {
            case clsConstants.English:
                Page.UICulture = clsConstants.EnglishISO; // "en-US"; // English
                break;
            case clsConstants.French:
                Page.UICulture = clsConstants.FranceISO; // // France
                break;
            case clsConstants.German:
                Page.UICulture = clsConstants.GermanyISO; //"de"; // German
                break;
            case clsConstants.Dutch:
                Page.UICulture = clsConstants.NederlandISO; //"nl"; // Dutch
                break;
            case clsConstants.Spanish:
                Page.UICulture = clsConstants.SpainISO; //"es"; // Spanish
                break;
            case clsConstants.Italian:
                Page.UICulture = clsConstants.ItalyISO; //"it"; // Italy
                break;
            case clsConstants.Czech:
                Page.UICulture = clsConstants.CzechISO; //"cz"; // Czech
                break;
            default:
                Page.UICulture = clsConstants.EnglishISO; //"en-US"; // English
                break;
        }

        #endregion

        using (StreamReader sReader = new StreamReader(templatePath + LanguageFile.ToLower()))
        {
            htmlBody = sReader.ReadToEnd();

            htmlBody = htmlBody.Replace("{DearSirMadam}", WebCommon.getGlobalResourceValue("DearSirMadam"));
            htmlBody = htmlBody.Replace("{UserRejectText1}", WebCommon.getGlobalResourceValue("UserRejectText1"));

            htmlBody = htmlBody.Replace("{AccountDate}", System.DateTime.Now.ToString("dd/MM/yyyy"));
            htmlBody = htmlBody.Replace("{RejectionCommentValue}", txtRejectCommentsR.Text.Trim());

            htmlBody = htmlBody.Replace("{UserRejectText2}", WebCommon.getGlobalResourceValue("UserRejectText2"));
            htmlBody = htmlBody.Replace("{ManyThanks}", WebCommon.getGlobalResourceValue("ManyThanks"));

            htmlBody = htmlBody.Replace("{VIPAdminNameValue}", Convert.ToString(Session["UserName"]));
            htmlBody = htmlBody.Replace("{VIPAdminEmailValue}", Convert.ToString(Session["LoginID"]));
        }
        return htmlBody;
    }

    private void VendorDisability()
    {
        /* Disabling the Country specific settings */
        var lstExistingCountry = new List<string>();
        for (int index = 0; index < rptVendorDetailsREU.Items.Count; index++)
        {
            var lblCountryVendorDetailsREU = (ucLabel)rptVendorDetailsREU.Items[index].FindControl("lblCountryVendorDetailsREU");
            var ucSeacrhVendorWithoutSM1 = (ucSeacrhVendorWithoutSM)rptVendorDetailsREU.Items[index].FindControl("ucSeacrhVendorWithoutSM1");
            if (ucSeacrhVendorWithoutSM1 != null)
            {
                var lblSelectedVendorName = (ucLabel)ucSeacrhVendorWithoutSM1.FindControl("SelectedVendorName");
                if (lblSelectedVendorName != null)
                {
                    if (!string.IsNullOrEmpty(lblSelectedVendorName.Text) && !string.IsNullOrWhiteSpace(lblSelectedVendorName.Text))
                    {
                        var txtVendorNo = (ucTextbox)ucSeacrhVendorWithoutSM1.FindControl("txtVendorNo");
                        if (txtVendorNo != null)
                            txtVendorNo.Enabled = false;

                        var imgVendor = (ImageButton)ucSeacrhVendorWithoutSM1.FindControl("imgVendor");
                        if (imgVendor != null)
                            imgVendor.Enabled = false;


                        if (lblCountryVendorDetailsREU != null)
                            if (!string.IsNullOrEmpty(lblCountryVendorDetailsREU.Text) && !string.IsNullOrWhiteSpace(lblCountryVendorDetailsREU.Text))
                                lstExistingCountry.Add(lblCountryVendorDetailsREU.Text);
                    }
                }
            }
        }

        for (int index = 0; index < rptCountry.Items.Count; index++)
        {
            var lblCountryName = (Label)rptCountry.Items[index].FindControl("lblCountryName");
            var chkRptCountry = (CheckBox)rptCountry.Items[index].FindControl("chkCountry");
            if (chkRptCountry != null && lblCountryName != null)
            {
                if (lstExistingCountry.Contains(lblCountryName.Text))
                {
                    if (chkRptCountry.Checked)
                        chkRptCountry.Enabled = false;
                }
            }
        }

        #region Commented Code ...
        /* Disabling the Vendor details settings */
        //for (int index = 0; index < rptVendorDetailsREU.Items.Count; index++)
        //{            
        //    var ucSeacrhVendorWithoutSM1 = (ucSeacrhVendorWithoutSM)rptVendorDetailsREU.Items[index].FindControl("ucSeacrhVendorWithoutSM1");
        //    if (ucSeacrhVendorWithoutSM1 != null)
        //    {
        //        var lblSelectedVendorName = (ucLabel)ucSeacrhVendorWithoutSM1.FindControl("SelectedVendorName");
        //        if (lblSelectedVendorName != null)
        //        {
        //            if (!string.IsNullOrEmpty(lblSelectedVendorName.Text) && !string.IsNullOrWhiteSpace(lblSelectedVendorName.Text))
        //            {
        //                var txtVendorNo = (ucTextbox)ucSeacrhVendorWithoutSM1.FindControl("txtVendorNo");
        //                if (txtVendorNo != null)
        //                    txtVendorNo.Enabled = false;

        //                var imgVendor = (ImageButton)ucSeacrhVendorWithoutSM1.FindControl("imgVendor");
        //                if (imgVendor != null)
        //                    imgVendor.Enabled = false;
        //            }
        //        }
        //    }
        //}
        #endregion
    }

    #endregion
    protected void btnProceed_Click(object sender, EventArgs e)
    {
        if (GetQueryStringValue("VendorID") != null)
        {
            EncryptQueryString("../GlobalSettings/VendorEdit.aspx?ReadOnly=1&VendorID=" + GetQueryStringValue("VendorID"));
        }
        else
        {
            EncryptQueryString("SCT_UsersOverview.aspx");
        }
    }

    protected void txtVendorNo_TextChanged(object sender, EventArgs e)
    {
        TextBox tbox = (TextBox)sender;
        RepeaterItem row = (RepeaterItem)tbox.NamingContainer;
        TextBox txtVendorNo = (TextBox)row.FindControl("txtVendorNo");
        Label SelectedVendorName = (Label)row.FindControl("SelectedVendorName");
        HiddenField CountryID = (HiddenField)row.FindControl("hdnCountryIdVendorDetailsREU");

        HiddenField hdnVendorID = (HiddenField)row.FindControl("hdnVendorID");
        HiddenField hdnParentVendorID = (HiddenField)row.FindControl("hdnParentVendorID");

        hdnCountryID.Value = CountryID.Value;
        hdntxtVendorNo.Value = txtVendorNo.Text;

        if (txtVendorNo.Text.Trim() != string.Empty)
        {
            List<UP_VendorBE> lstNewUPVendor = new List<UP_VendorBE>();
            List<UP_VendorBE> lstUPVendor = new List<UP_VendorBE>();

            /// --- Added for Showing Inative Vendors --- 04 Mar 2013

            lstNewUPVendor = IsValidVendorCode();
            foreach (UP_VendorBE objVendor in lstNewUPVendor.ToList())
            {
                if (objVendor.IsActiveVendor == "Y")
                    lstUPVendor.Add(objVendor);
            }

            /// --- Added for Showing Inative Vendors --- 04 Mar 2013

            if (lstUPVendor.Count > 0)
            {

                hdnVendorID.Value = lstUPVendor[0].VendorID.ToString();
                hdnParentVendorID.Value = lstUPVendor[0].ParentVendorID.ToString();
                txtVendorNo.Text = lstUPVendor[0].Vendor_No.ToString();
                SelectedVendorName.Text = lstUPVendor[0].VendorName.ToString();
                hdnVendorName.Value = lstUPVendor[0].VendorName.ToString();

                foreach (RepeaterItem ritem in rptCountry.Items)
                {
                    //if (ritem.ItemType == ListItemType.Item || ritem.ItemType == ListItemType.AlternatingItem)
                    //{
                    CheckBox chkCountry = (CheckBox)ritem.FindControl("chkCountry");
                    HiddenField hiddenCountryID = (HiddenField)ritem.FindControl("hdnCountryID");
                    if (chkCountry.Checked)
                    {
                        foreach (var item in lstCountrySettings)
                        {
                            if (string.IsNullOrEmpty(item.VendorName))
                            {
                                if (Convert.ToInt32(hiddenCountryID.Value) == Convert.ToInt32(CountryID.Value))
                                {
                                    lstCountrySettings.Where(w => w.CountryId == Convert.ToInt32(hdnCountryID.Value)).ToList().ForEach(i => i.VendorName = SelectedVendorName.Text);
                                    lstCountrySettings.Where(w => w.CountryId == Convert.ToInt32(hdnCountryID.Value)).ToList().ForEach(i => i.VendorID = Convert.ToInt32(hdnVendorID.Value));
                                }
                            }
                        }

                    }
                    // }
                }

            }
            else
            {
                hdnVendorID.Value = string.Empty;
                hdnParentVendorID.Value = string.Empty;
                txtVendorNo.Text = string.Empty;
                SelectedVendorName.Text = string.Empty;
                hdnVendorName.Value = string.Empty;

                ScriptManager.RegisterStartupScript(this.up1, this.up1.GetType(), "alert", "alert('Please enter a valid Vendor Code.')", true);
                return;
            }
        }
        else
        {
            hdnVendorID.Value = string.Empty;
            hdnParentVendorID.Value = string.Empty;
            txtVendorNo.Text = string.Empty;
            SelectedVendorName.Text = string.Empty;
            hdnVendorName.Value = string.Empty;
        }
    }



    protected void btnSearch_Click(object sender, EventArgs e)
    {
        List<UP_VendorBE> lstUPVendor;
        List<UP_VendorBE> lstNewUPVendor = new List<UP_VendorBE>();
        btnSearchGetClicked = true;

        lstUPVendor = GetVendor();

        if (lstUPVendor.Count == 0)
        {
            string errorMeesage = WebCommon.getGlobalResourceValue("ValidVendorCode");
            ScriptManager.RegisterStartupScript(this.hdntxtVendorNo, this.hdntxtVendorNo.GetType(), "alert1", "alert('" + errorMeesage + "')", true);
        }
        /// --- Added for Showing Inative Vendors --- 04 Mar 2013

        foreach (UP_VendorBE objVendor in lstUPVendor.ToList())
        {
            if (objVendor.IsActiveVendor == "Y")
                lstNewUPVendor.Add(objVendor);
        }
        FillControls.FillListBox(ref lstVendor, lstNewUPVendor.ToList(), "VendorName", "VendorID");

        /// --- Added for Showing Inative Vendors --- 04 Mar 2013

        mdlVendorViewer.Show();
    }


    protected void btnSelect_Click(object sender, EventArgs e)
    {

        if (lstVendor.SelectedIndex >= 0)
        {
            for (int index = 0; index < this.rptVendorDetailsREU.Items.Count; index++)
            {
                if (index == Convert.ToInt32(hdnrowNumber.Value))
                {
                    string strVendorVal = lstVendor.SelectedItem.Value;
                    string strVendor = lstVendor.SelectedItem.Text;
                    Label SelectedVendorName = (this.rptVendorDetailsREU.Items[index].FindControl("SelectedVendorName") as Label);
                    TextBox txtVendorNo = (this.rptVendorDetailsREU.Items[index].FindControl("txtVendorNo") as TextBox);
                    HiddenField hdnVendorID = (this.rptVendorDetailsREU.Items[index].FindControl("hdnVendorID") as HiddenField);
                    HiddenField hdnParentVendorID = (this.rptVendorDetailsREU.Items[index].FindControl("hdnParentVendorID") as HiddenField);
                    HiddenField hdnCountryIdVendorDetailsREU = (this.rptVendorDetailsREU.Items[index].FindControl("hdnCountryIdVendorDetailsREU") as HiddenField);

                    SelectedVendorName.Text = strVendor;
                    txtVendorNo.Text = strVendor.Split('-')[0];
                    hdnVendorName.Value = strVendor;
                    hdnVendorID.Value = strVendorVal.Trim();
                    List<UP_VendorBE> lstUPVendor = IsValidVendorCode();
                    if (lstUPVendor.Count > 0)
                    {
                        hdnParentVendorID.Value = lstUPVendor[0].ParentVendorID.ToString();
                    }

                    lstVendor.Items.Clear();
                    mdlVendorViewer.Hide();


                    foreach (RepeaterItem ritem in rptCountry.Items)
                    {
                        //if (ritem.ItemType == ListItemType.Item || ritem.ItemType == ListItemType.AlternatingItem)
                        //{
                        CheckBox chkCountry = (CheckBox)ritem.FindControl("chkCountry");
                        HiddenField hdnCountryID = (HiddenField)ritem.FindControl("hdnCountryID");
                        if (chkCountry.Checked)
                        {
                            foreach (var item in lstCountrySettings)
                            {
                                if (string.IsNullOrEmpty(item.VendorName))
                                {
                                    if (Convert.ToInt32(hdnCountryID.Value) == Convert.ToInt32(hdnCountryIdVendorDetailsREU.Value))
                                    {
                                        lstCountrySettings.Where(w => w.CountryId == Convert.ToInt32(hdnCountryID.Value)).ToList().ForEach(i => i.VendorName = strVendor);
                                        lstCountrySettings.Where(w => w.CountryId == Convert.ToInt32(hdnCountryID.Value)).ToList().ForEach(i => i.VendorID =Convert.ToInt32(hdnVendorID.Value));
                                    }
                                }
                            }

                        }
                        // }
                    }
                    return;
                }
            }

        }
        else
        {
            mdlVendorViewer.Show();
        }


    }


    public List<UP_VendorBE> IsValidVendorCode()
    {
        return GetVendor();
    }



    protected void btnPopupCancel_Click(object sender, EventArgs e)
    {
        lstVendor.Items.Clear();
        txtVenderName2.Text = string.Empty;
        mdlVendorViewer.Hide();
    }


    private List<UP_VendorBE> GetVendor()
    {
        UP_VendorBAL oUP_VendorBAL = new UP_VendorBAL();
        UP_VendorBE oUP_VendorBE = new UP_VendorBE();

        oUP_VendorBE.Action = "SearchVendor";

        oUP_VendorBE.Site = new BusinessEntities.ModuleBE.AdminFunctions.MAS_SiteBE();
        if (btnSearchGetClicked == true)
        {
            oUP_VendorBE.Vendor_No = string.Empty;
        }        
        else if (hdntxtVendorNo.Value.Trim() != "")
            oUP_VendorBE.Vendor_No = hdntxtVendorNo.Value.Trim() == string.Empty ? "" : hdntxtVendorNo.Value.Trim();
        else
            oUP_VendorBE.VendorName = txtVenderName2.Text.Trim();

        if (string.IsNullOrEmpty(oUP_VendorBE.VendorName) && string.IsNullOrEmpty(oUP_VendorBE.Vendor_No)) {
            oUP_VendorBE.VendorName = txtVenderName2.Text.Trim();
        }
        oUP_VendorBE.IsChildRequired = true;
        oUP_VendorBE.IsGrandParentRequired = false;
        oUP_VendorBE.IsParentRequired = false;
        oUP_VendorBE.IsStandAloneRequired = true;

        oUP_VendorBE.Site.SiteCountryID = Convert.ToInt32(hdnCountryID.Value);


        List<UP_VendorBE> lstUPVendor = new List<UP_VendorBE>();
        lstUPVendor = oUP_VendorBAL.GetCarrierDetailsBAL(oUP_VendorBE);
        return lstUPVendor;
    }


}


public class UserCountryModulesSettingsExternal
{
    public int CountryID { get; set; }
    public bool IsSchedulingDefined { get; set; }
    public bool IsDiscrepancyDefined { get; set; }
    public bool IsInvoiceIssueDefined { get; set; }
    public bool IsOTIFDefined { get; set; }
    public bool IsScorecardDefined { get; set; }
}
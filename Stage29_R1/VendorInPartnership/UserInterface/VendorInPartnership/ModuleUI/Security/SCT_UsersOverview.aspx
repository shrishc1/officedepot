﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    CodeFile="SCT_UsersOverview.aspx.cs" Inherits="SCT_UsersOverview" MaintainScrollPositionOnPostback="true" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="../../CommonUI/UserControls/ucAddButton.ascx" TagName="ucAddButton"
    TagPrefix="cc2" %>
<%@ Register Src="~/CommonUI/UserControls/ucExportToExcel.ascx" TagName="ucExportToExcel"
    TagPrefix="cc2" %>
<%@ Register Src="../../CommonUI/UserControls/ucCountry.ascx" TagName="ucCountry"
    TagPrefix="cc2" %>
<%--<%@ Register Src="~/CommonUI/UserControls/ucSeacrhVendor.ascx" TagName="ucSeacrhVendor"
    TagPrefix="cc2" %>--%>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectVendor.ascx" TagName="MultiSelectVendor"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectCarriers.ascx" TagName="MultiSelectCarriers"
    TagPrefix="cc3" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <script type="text/javascript" language="javascript">
        function ConfirmRejectedUserDeletion() {
            return confirm('<%=AllRejectedUserdeleteMessage%>');
        }
        $(document).ready(function () {
            $("body").keypress(function (e) {
                if (e.which == 13 || e.keyCode == 13) {
                    $('#<%=btnSearch.ClientID %>').click();
                    return false;
                }
            });
        });
    </script>
    <h2>
        <cc1:ucLabel ID="lblUsersOverview" runat="server" Text="Users Overview"></cc1:ucLabel>
    </h2>
    <div class="button-row">
        <cc2:ucAddButton ID="btnAdd" runat="server" NavigateUrl="SCT_UserEdit.aspx" />
        <cc2:ucExportToExcel ID="btnExportToExcel1" runat="server" />
    </div>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table width="100%" cellspacing="1" cellpadding="0" border="0" align="center" class="top-settings">
                <tr>
                    <td width="12%" style="font-weight: bold;">
                        <cc1:ucLabel ID="lblCountry" runat="server" />
                    </td>
                    <td style="font-weight: bold; width: 1%">
                        :
                    </td>
                    <td width="27%">
                        <cc2:ucCountry runat="server" ID="ddlCountry" />
                    </td>
                    <td width="15%" style="font-weight: bold;">
                        <cc1:ucLabel ID="lblStatus" runat="server" />
                    </td>
                    <td style="font-weight: bold; width: 1%">
                        :
                    </td>
                    <td width="34%">
                        <cc1:ucDropdownList runat="server" ID="ddlStatus" Width="150px">
                            <asp:ListItem Selected="True" Text="--Select--" Value="" />
                            <asp:ListItem Text="Active" Value="Active" />
                            <asp:ListItem Text="Registered Awaiting Approval" Value="Registered Awaiting Approval" />
                            <asp:ListItem Text="Edited Awaiting Approval" Value="Edited Awaiting Approval" />
                            <asp:ListItem Text="Awaiting Activation" Value="Awaiting Activation" />
                            <asp:ListItem Text="Blocked" Value="Blocked" />
                        </cc1:ucDropdownList>
                    </td>
                </tr>
                <tr>
                    <td width="5%" style="font-weight: bold;">
                        <%--<cc1:ucLabel ID="lblUserName" Text="User Name" runat="server" />--%>
                        <cc1:ucLabel ID="lblUserNameEmailId" Text="User Name" runat="server" />
                    </td>
                    <td style="font-weight: bold; width: 1%">
                        :
                    </td>
                    <td width="34%">
                        <cc1:ucTextbox runat="server" ID="txtUserNameValue" />
                    </td>
                    <td width="15%" style="font-weight: bold;">
                        <cc1:ucLabel ID="lblRole" runat="server" Text="Role"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold; width: 1%">
                        :
                    </td>
                    <td width="34%">
                        <cc1:ucDropdownList ID="ddlRole" runat="server" Width="150px">
                        </cc1:ucDropdownList>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
        <ContentTemplate>
            <table width="100%" cellspacing="1" cellpadding="0" border="0" align="center" class="top-settings">
                <tr>
                    <td width="5%" style="font-weight: bold;">
                        <cc1:ucLabel ID="lblVendor" runat="server" />
                    </td>
                    <td style="font-weight: bold; width: 1%">
                        :
                    </td>
                    <td width="84%" colspan="4">
                        <%--<cc2:ucSeacrhVendor runat="server" ID="ddlSeacrhVendor" Visible="false" />--%>
                        <cc1:MultiSelectVendor runat="server" ID="msVendor" />
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="UpdatePanel3" runat="server">
        <ContentTemplate>
            <table width="100%" cellspacing="1" cellpadding="0" border="0" align="center" class="top-settings">
                <tr>
                    <td width="5%" style="font-weight: bold;">
                        <cc1:ucLabel ID="lblCarrier" runat="server" />
                    </td>
                    <td style="font-weight: bold; width: 1%">
                        :
                    </td>
                    <td width="84%" colspan="4">
                        <cc3:MultiSelectCarriers runat="server" ID="msCarrier" />
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
    <table width="100%" cellspacing="1" cellpadding="0" border="0" align="center" class="top-settings">
        <tr>
            <td width="5%" style="font-weight: bold;">
                <cc1:ucLabel ID="lblReportType" runat="server" />
            </td>
            <td style="font-weight: bold; width: 1%">
                :
            </td>
            <td style="font-weight: bold;" class="checkbox-list">
                <cc1:ucRadioButton ID="rdoCarrierOverview" runat="server" Text="Carrier Overview"
                    GroupName="ReportVersion" />
            </td>
            <td style="font-weight: bold;" class="checkbox-list">
                <cc1:ucRadioButton ID="rdoVendorOverview" runat="server" Text="User Overview" GroupName="ReportVersion" />
            </td>
            <td style="font-weight: bold;" class="checkbox-list">
                <cc1:ucRadioButton ID="rdoVendorPOCOverview" runat="server" Text="Vendor POC Overview"
                    GroupName="ReportVersion" />
            </td>
            <td style="font-weight: bold;" class="checkbox-list">
                <cc1:ucRadioButton ID="rdoSchedulingOverview" runat="server" Text="Scheduling Overview"
                    GroupName="ReportVersion" />
            </td>
            <td style="font-weight: bold;" class="checkbox-list">
                <cc1:ucRadioButton ID="rdoDiscrepancyOverview" runat="server" Text="Discrepancy  Overview"
                    GroupName="ReportVersion" />
            </td>
            <td style="font-weight: bold;" class="checkbox-list">
                <cc1:ucRadioButton ID="rdoOfficeDepotUserOverview" runat="server" Text="Office Depot User Overview"
                    GroupName="ReportVersion" />
            </td>
        </tr>
    </table>
    <asp:UpdatePanel ID="UpdatePanel4" runat="server">
        <ContentTemplate>
            <table width="100%" cellspacing="1" cellpadding="0" border="0" align="center" class="top-settings">
                <tr align="right">
                    <td colspan="6">
                        &nbsp;
                    </td>
                </tr>
                <tr align="right">
                    <td colspan="6">
                        <cc1:ucButton runat="server" ID="btnSearch" CssClass="button" OnClick="btnSearch_Click" />
                    </td>
                </tr>
                <tr align="right">
                    <td colspan="6">
                        <cc1:ucButton ID="btnDeleteRejectedUser" runat="server" Text="Delete Rejected User"
                            CssClass="button" OnClientClick="return ConfirmRejectedUserDeletion();" OnClick="btnDeleteRejectedUser_Click" />
                    </td>
                </tr>
            </table>
            <br />
            <div style="width: 990px; overflow-x: scroll;">
                <cc1:ucGridView runat="server" AutoGenerateColumns="false" ID="gvUser" CssClass="grid"
                    OnSorting="SortGrid" AllowSorting="true" OnRowCommand="gvUser_RowCommand" OnRowDataBound="gvUser_RowDataBound"
                    AllowPaging="True" OnPageIndexChanging="gvUser_PageIndexChanging" PageSize="30">
                    <Columns>
                        <asp:BoundField HeaderText="Country" SortExpression="Country" DataField="Country">
                            <HeaderStyle Width="100" HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                        <asp:BoundField HeaderText="CarrierName" SortExpression="CarrierName" DataField="CarrierName">
                            <HeaderStyle Width="130" HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                        <asp:BoundField HeaderText="DefaultSite" SortExpression="DefaultSite" DataField="DefaultSite">
                            <HeaderStyle Width="130" HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                        <asp:TemplateField HeaderText="Vendor No" SortExpression="Vendor.Vendor_No">
                            <HeaderStyle Width="90" HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <cc1:ucLabel ID="ltVendorNo" runat="server" Text='<%# Eval("Vendor.Vendor_No") %>'></cc1:ucLabel>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="VendorName" SortExpression="Vendor.Vendor_Name">
                            <HeaderStyle Width="200" HorizontalAlign="Left" />
                            <ItemTemplate>
                                <asp:HyperLink ID="hpVendorLink" runat="server" NavigateUrl='<%# EncryptQuery("../GlobalSettings/VendorEdit.aspx?PageFrom=UserList&VendorID="+Eval("Vendor.VendorID")) %>'
                                    Text='<%#Eval("Vendor.Vendor_Name") %>'></asp:HyperLink>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="European / Local Vendor" SortExpression="Vendor.VendorEuropeanorLocal">
                            <HeaderStyle Width="100" HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <cc1:ucLabel ID="ltVendorEuropeanorLocal" runat="server" Text='<%# Eval("Vendor.VendorEuropeanorLocal") %>'></cc1:ucLabel>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Local Consolidation Code" SortExpression="Vendor.LocalConsolidationCode">
                            <HeaderStyle Width="100" HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <cc1:ucLabel ID="ltLocalConsolidationCode" runat="server" Text='<%# Eval("Vendor.LocalConsolidationCode") %>'></cc1:ucLabel>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="EU Consolidation Code" SortExpression="Vendor.EUConsolidationCode">
                            <HeaderStyle Width="100" HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <cc1:ucLabel ID="ltEUConsolidationCode" runat="server" Text='<%# Eval("Vendor.EUConsolidationCode") %>'></cc1:ucLabel>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField HeaderText="User Name" SortExpression="FullName" DataField="FullName">
                            <HeaderStyle Width="160" HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                        <asp:TemplateField HeaderText="User ID" SortExpression="LoginID">
                            <HeaderStyle Width="150" HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <asp:HyperLink ID="hlLoginID" runat="server" Text='<%#Eval("LoginID") %>' NavigateUrl='<%# (Eval("UserRoleID").ToString() == "2" || Eval("UserRoleID").ToString() == "3") ? (Eval("AccountStatus").ToString().ToLower()== "registered awaiting approval" || Eval("AccountStatus").ToString().ToLower()== "edited awaiting approval") ? (EncryptQuery("~/ModuleUI/Security/SCT_RegisterExternalUserEdit.aspx?UserID="+ Eval("UserID")+"&UStatus=RAA")) :(EncryptQuery("~/ModuleUI/Security/SCT_RegisterExternalUserEdit.aspx?UserID="+ Eval("UserID"))) : (EncryptQuery("~/ModuleUI/Security/SCT_UserEdit.aspx?UserID="+ Eval("UserID"))) %>'></asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField HeaderText="Email ID" SortExpression="EmailId" DataField="EmailId">
                            <HeaderStyle Width="100" HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                        <asp:BoundField HeaderText="Phone No" SortExpression="PhoneNumber" DataField="PhoneNumber">
                            <HeaderStyle Width="100" HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                        <asp:BoundField HeaderText="Role" SortExpression="RoleName" DataField="RoleName">
                            <HeaderStyle Width="100" HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                        <asp:BoundField HeaderText="Role Template" SortExpression="TemplateName" DataField="TemplateName">
                            <HeaderStyle Width="130" HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField>

                        <asp:BoundField HeaderText="JobTitle" SortExpression="JobTitle" DataField="JobTitle">
                            <HeaderStyle Width="150" HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField>

                        <asp:BoundField HeaderText="Status" SortExpression="AccountStatus" DataField="AccountStatus">
                            <HeaderStyle Width="150" HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                        <asp:TemplateField HeaderText="Date Created" SortExpression="CratedDate">
                            <HeaderStyle Width="100" HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <cc1:ucLabel ID="lblCreatedDate" runat="server" Text='<%# Eval("CratedDate", "{0:dd/MM/yyyy}") %>'></cc1:ucLabel>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Date Last logged On" SortExpression="LastLoggedOn">
                            <HeaderStyle Width="100" HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <cc1:ucLabel ID="lblExpectedDeliveryDate" runat="server" Text='<%# Eval("LastLoggedOn", "{0:dd/MM/yyyy}") %>'></cc1:ucLabel>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField HeaderText="Number of Elapsed Days" SortExpression="Elapseddays"
                            DataField="Elapseddays">
                            <HeaderStyle Width="130" HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                        <asp:TemplateField HeaderText="Assigned Site" SortExpression="Vendor.AssignedSite">
                            <HeaderStyle Width="350" HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <cc1:ucLabel ID="ltAssignedSite" runat="server" Text='<%# Eval("Vendor.AssignedSite") %>'></cc1:ucLabel>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Enabled Site" SortExpression="Vendor.EnabledSite">
                            <HeaderStyle Width="350" HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <cc1:ucLabel ID="ltEnabledSite" runat="server" Text='<%# Eval("Vendor.EnabledSite") %>'></cc1:ucLabel>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Job Title" SortExpression="VendorPOC.RoleTitle">
                            <HeaderStyle Width="150" HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <cc1:ucLabel ID="ltJobTitle" runat="server" Text='<%# Eval("VendorPOC.RoleTitle") %>'></cc1:ucLabel>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField HeaderText="Appointment Scheduling" SortExpression="SchedulingContact"
                            DataField="SchedulingContact">
                            <HeaderStyle Width="130" HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                        <asp:BoundField HeaderText="Discrepancy" SortExpression="DiscrepancyContact" DataField="DiscrepancyContact">
                            <HeaderStyle Width="130" HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                        <asp:BoundField HeaderText="OTIF" SortExpression="OTIFContact" DataField="OTIFContact">
                            <HeaderStyle Width="130" HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                        <asp:BoundField HeaderText="Scorecard" SortExpression="ScorecardContact" DataField="ScorecardContact">
                            <HeaderStyle Width="130" HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                        <asp:TemplateField HeaderText="Inventory POC" SortExpression="VendorPOC.InventoryPOC">
                            <HeaderStyle Width="100" HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <cc1:ucLabel ID="ltInventoryPOC" runat="server" Text='<%# Eval("VendorPOC.InventoryPOC") %>'></cc1:ucLabel>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Procurement POC" SortExpression="VendorPOC.ProcurementPOC">
                            <HeaderStyle Width="100" HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <cc1:ucLabel ID="ltProcurementPOC" runat="server" Text='<%# Eval("VendorPOC.ProcurementPOC") %>'></cc1:ucLabel>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Merchandising POC" SortExpression="VendorPOC.MerchandisingPOC">
                            <HeaderStyle Width="100" HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <cc1:ucLabel ID="ltMerchandisingPOC" runat="server" Text='<%# Eval("VendorPOC.MerchandisingPOC") %>'></cc1:ucLabel>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Executive POC" SortExpression="VendorPOC.ExecutivePOC">
                            <HeaderStyle Width="100" HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <cc1:ucLabel ID="ltExecutivePOC" runat="server" Text='<%# Eval("VendorPOC.ExecutivePOC") %>'></cc1:ucLabel>
                            </ItemTemplate>
                        </asp:TemplateField>
                          <asp:BoundField HeaderText="Invoice Issues" SortExpression="InvoiceIssueContact" DataField="InvoiceIssueContact">
                            <HeaderStyle Width="100" HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                        <asp:TemplateField>
                            <HeaderStyle Width="100" HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <cc1:ucLinkButton ID="lnkDetails" runat="server" CommandName="VendorModulesDetails"
                                    CommandArgument='<%#Eval("UserID") + "," + Eval("UserRoleID") %>'>Details</cc1:ucLinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </cc1:ucGridView>
                <cc1:ucGridView runat="server" AutoGenerateColumns="false" ID="gvUserExcelExport"
                    CssClass="grid" OnSorting="SortGrid" AllowSorting="true" Visible="false">
                    <Columns>
                        <asp:BoundField HeaderText="Country" SortExpression="Country" DataField="Country">
                            <HeaderStyle Width="100" HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                        <asp:BoundField HeaderText="CarrierName" SortExpression="CarrierName" DataField="CarrierName">
                            <HeaderStyle Width="130" HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                        <asp:BoundField HeaderText="DefaultSite" SortExpression="DefaultSite" DataField="DefaultSite">
                            <HeaderStyle Width="130" HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                        <asp:TemplateField HeaderText="Vendor No" SortExpression="Vendor.Vendor_No">
                            <HeaderStyle Width="90" HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <cc1:ucLabel ID="ltVendorNo" runat="server" Text='<%# Eval("Vendor.Vendor_No") %>'></cc1:ucLabel>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="VendorName" SortExpression="Vendor.Vendor_Name">
                            <HeaderStyle Width="200" HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <cc1:ucLabel ID="ltVendorName" runat="server" Text='<%# Eval("Vendor.Vendor_Name") %>'></cc1:ucLabel>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="European / Local Vendor" SortExpression="Vendor.VendorEuropeanorLocal">
                            <HeaderStyle Width="100" HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <cc1:ucLabel ID="ltVendorEuropeanorLocal" runat="server" Text='<%# Eval("Vendor.VendorEuropeanorLocal") %>'></cc1:ucLabel>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Local Consolidation Code" SortExpression="Vendor.LocalConsolidationCode">
                            <HeaderStyle Width="100" HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <cc1:ucLabel ID="ltLocalConsolidationCode" runat="server" Text='<%# Eval("Vendor.LocalConsolidationCode") %>'></cc1:ucLabel>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="EU Consolidation Code" SortExpression="Vendor.EUConsolidationCode">
                            <HeaderStyle Width="100" HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <cc1:ucLabel ID="ltEUConsolidationCode" runat="server" Text='<%# Eval("Vendor.EUConsolidationCode") %>'></cc1:ucLabel>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField HeaderText="User Name" DataField="FullName">
                            <HeaderStyle Width="160" HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                        <asp:BoundField HeaderText="User ID" DataField="LoginID">
                            <HeaderStyle Width="160" HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                        <asp:BoundField HeaderText="Email ID" SortExpression="EmailId" DataField="EmailId">
                            <HeaderStyle Width="100" HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                        <asp:BoundField HeaderText="Phone No" SortExpression="PhoneNumber" DataField="PhoneNumber">
                            <HeaderStyle Width="100" HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                        <asp:BoundField HeaderText="Role" SortExpression="RoleName" DataField="RoleName">
                            <HeaderStyle Width="100" HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                        <asp:BoundField HeaderText="Role Template" SortExpression="TemplateName" DataField="TemplateName">
                            <HeaderStyle Width="130" HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField>

                         <asp:BoundField HeaderText="JobTitle" SortExpression="JobTitle" DataField="JobTitle">
                            <HeaderStyle Width="150" HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField>

                        <asp:BoundField HeaderText="Status" SortExpression="AccountStatus" DataField="AccountStatus">
                            <HeaderStyle Width="150" HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                        <asp:TemplateField HeaderText="Date Created" SortExpression="CratedDate">
                            <HeaderStyle Width="100" HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <cc1:ucLabel ID="lblCreatedDate" runat="server" Text='<%# Bind("CratedDate", "{0:dd/MM/yyyy}") %>'></cc1:ucLabel>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Date Last logged On" SortExpression="LastLoggedOn">
                            <HeaderStyle Width="100" HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <cc1:ucLabel ID="lblExpectedDeliveryDate" runat="server" Text='<%# Bind("LastLoggedOn", "{0:dd/MM/yyyy}") %>'></cc1:ucLabel>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField HeaderText="Number of Elapsed Days" SortExpression="Elapseddays"
                            DataField="Elapseddays">
                            <HeaderStyle Width="130" HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                        <asp:TemplateField HeaderText="Assigned Site" SortExpression="Vendor.AssignedSite">
                            <HeaderStyle Width="350" HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <cc1:ucLabel ID="ltAssignedSite" runat="server" Text='<%# Eval("Vendor.AssignedSite") %>'></cc1:ucLabel>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Enabled Site" SortExpression="Vendor.EnabledSite">
                            <HeaderStyle Width="350" HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <cc1:ucLabel ID="ltEnabledSite" runat="server" Text='<%# Eval("Vendor.EnabledSite") %>'></cc1:ucLabel>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Job Title" SortExpression="VendorPOC.RoleTitle">
                            <HeaderStyle Width="150" HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <cc1:ucLabel ID="ltJobTitle" runat="server" Text='<%# Eval("VendorPOC.RoleTitle") %>'></cc1:ucLabel>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField HeaderText="Appointment Scheduling" SortExpression="SchedulingContact"
                            DataField="SchedulingContact">
                            <HeaderStyle Width="130" HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                        <asp:BoundField HeaderText="Discrepancy" SortExpression="DiscrepancyContact" DataField="DiscrepancyContact">
                            <HeaderStyle Width="130" HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                        <asp:BoundField HeaderText="OTIF" SortExpression="OTIFContact" DataField="OTIFContact">
                            <HeaderStyle Width="130" HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                        <asp:BoundField HeaderText="Scorecard" SortExpression="ScorecardContact" DataField="ScorecardContact">
                            <HeaderStyle Width="130" HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                        <asp:TemplateField HeaderText="Inventory POC" SortExpression="VendorPOC.InventoryPOC">
                            <HeaderStyle Width="100" HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <cc1:ucLabel ID="ltInventoryPOC" runat="server" Text='<%# Eval("VendorPOC.InventoryPOC") %>'></cc1:ucLabel>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Procurement POC" SortExpression="VendorPOC.ProcurementPOC">
                            <HeaderStyle Width="100" HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <cc1:ucLabel ID="ltProcurementPOC" runat="server" Text='<%# Eval("VendorPOC.ProcurementPOC") %>'></cc1:ucLabel>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Merchandising POC" SortExpression="VendorPOC.MerchandisingPOC">
                            <HeaderStyle Width="100" HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <cc1:ucLabel ID="ltMerchandisingPOC" runat="server" Text='<%# Eval("VendorPOC.MerchandisingPOC") %>'></cc1:ucLabel>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Executive POC" SortExpression="VendorPOC.ExecutivePOC">
                            <HeaderStyle Width="100" HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <cc1:ucLabel ID="ltExecutivePOC" runat="server" Text='<%# Eval("VendorPOC.ExecutivePOC") %>'></cc1:ucLabel>
                            </ItemTemplate>
                        </asp:TemplateField>
                          <asp:BoundField HeaderText="Invoice Issues" SortExpression="InvoiceIssueContact" DataField="InvoiceIssueContact">
                            <HeaderStyle Width="100" HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                    </Columns>
                </cc1:ucGridView>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="updVendorModulesDetails" runat="server">
        <ContentTemplate>
            <asp:Button ID="btnVendorModulesDetails" runat="Server" Style="display: none" />
            <ajaxToolkit:ModalPopupExtender ID="mdlVendorModulesDetails" runat="server" TargetControlID="btnVendorModulesDetails"
                PopupControlID="pnlVendorModulesDetails" BackgroundCssClass="modalBackground"
                BehaviorID="VendorModulesDetails" DropShadow="false" />
            <asp:Panel ID="pnlVendorModulesDetails" runat="server" Style="display: none;" Width="80%">
                <div style="overflow-y: scroll; overflow-x: hidden; background-color: #fff; padding: 5px;
                    border: 2px solid #ccc; max-height: 600px;">
                    <cc1:ucPanel ID="pnlCountrySettings" runat="server" CssClass="fieldset-form">
                        <table width="100%" cellspacing="5" cellpadding="0" class="form-table">
                            <asp:Repeater ID="rptCountry" runat="server" OnItemDataBound="rptCountry_ItemDataBound">
                                <ItemTemplate>
                                    <%# (Container.ItemIndex + 5) % 5 == 0 ? "<tr>" : string.Empty %>
                                    <td>
                                        <asp:CheckBox ID="chkCountry" runat="server" Enabled="false" />
                                    </td>
                                    <td width="19%">
                                        <%# Eval("CountryName") %>
                                    </td>
                                    <%# (Container.ItemIndex + 5) % 5 == 4 ? "</tr>" : string.Empty %>
                                </ItemTemplate>
                            </asp:Repeater>
                        </table>
                    </cc1:ucPanel>
                    <asp:Panel ID="pnlVendorSction" runat="server" Visible="false">
                        <cc1:ucPanel ID="pnlVendorDetail" runat="server" CssClass="fieldset-form">
                            <table width="100%" cellspacing="5" cellpadding="0" class="form-table">
                                <asp:Repeater ID="rptVendorDetails" runat="server">
                                    <ItemTemplate>
                                        <tr>
                                            <td width="5%">
                                                <cc1:ucLabel ID="lblCountry" runat="server" Enabled="false"></cc1:ucLabel>:
                                            </td>
                                            <td>
                                                <%# Eval("Country") %>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="5%">
                                                <cc1:ucLabel ID="lblVendor" runat="server"></cc1:ucLabel>:
                                            </td>
                                            <td>
                                                <asp:HyperLink ID="HLVendor" runat="server" Text='<%#Eval("VendorName") %>' NavigateUrl='<%# EncryptQuery("~/ModuleUI/GlobalSettings/VendorEdit.aspx?PageFrom=UserList&VendorID="+ Eval("VendorID")) %>'></asp:HyperLink>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </table>
                        </cc1:ucPanel>
                        <cc1:ucPanel ID="pnlModules" runat="server" CssClass="fieldset-form">
                            <br />
                            <cc1:ucLabel ID="lblAppointmentScheduling" runat="server" CssClass="button"></cc1:ucLabel>
                            <table width="100%" cellspacing="5" cellpadding="0" class="form-table">
                                <asp:Repeater ID="rptAppointmentCountry" runat="server" OnItemDataBound="rptAppointmentCountry_ItemDataBound">
                                    <ItemTemplate>
                                        <%# (Container.ItemIndex + 5) % 5 == 0 ? "<tr>" : string.Empty %>
                                        <td valign="top">
                                            <b>
                                                <%# Eval("Country") %></b>
                                            <br />
                                            <table width="100%" cellspacing="5" cellpadding="0" class="form-table">
                                                <asp:Repeater ID="rptAppointmentCountrySites" runat="server" OnItemDataBound="rptAppointmentCountrySites_ItemDataBound">
                                                    <ItemTemplate>
                                                        <tr>
                                                            <td width="2%">
                                                                <asp:CheckBox ID="chkSite" runat="server" Enabled="false" />
                                                            </td>
                                                            <td>
                                                                <%# Eval("SiteName") %>
                                                            </td>
                                                        </tr>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </table>
                                        </td>
                                        <%# (Container.ItemIndex + 5) % 5 == 4 ? "</tr>" : string.Empty %>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </table>
                            <br />
                            <cc1:ucLabel ID="lblDiscrepancies" runat="server" CssClass="button"></cc1:ucLabel>
                            <table width="100%" cellspacing="5" cellpadding="0" class="form-table">
                                <asp:Repeater ID="rptDiscrepanciesCountry" runat="server" OnItemDataBound="rptDiscrepanciesCountry_ItemDataBound">
                                    <ItemTemplate>
                                        <%# (Container.ItemIndex + 5) % 5 == 0 ? "<tr>" : string.Empty %>
                                        <td valign="top">
                                            <b>
                                                <%# Eval("Country") %></b>
                                            <br />
                                            <table width="100%" cellspacing="5" cellpadding="0" class="form-table">
                                                <asp:Repeater ID="rptDiscrepanciesCountrySites" runat="server" OnItemDataBound="rptDiscrepanciesCountrySites_ItemDataBound">
                                                    <ItemTemplate>
                                                        <tr>
                                                            <td width="2%">
                                                                <asp:CheckBox ID="chkSite" runat="server" Enabled="false" />
                                                            </td>
                                                            <td>
                                                                <%# Eval("SiteName") %>
                                                            </td>
                                                        </tr>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </table>
                                        </td>
                                        <%# (Container.ItemIndex + 5) % 5 == 4 ? "</tr>" : string.Empty %>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </table>

                            <br />
                            <cc1:ucLabel ID="lblInvoiceIssue_1" runat="server" CssClass="button"></cc1:ucLabel>
                            <table width="100%" cellspacing="5" cellpadding="0" class="form-table">
                                <asp:Repeater ID="rptInvoiceIssueCountry" runat="server" OnItemDataBound="rptInvoiceIssueCountry_ItemDataBound">
                                    <ItemTemplate>
                                        <%# (Container.ItemIndex + 5) % 5 == 0 ? "<tr>" : string.Empty %>
                                        <td valign="top">
                                            <b>
                                                <%# Eval("Country") %></b>
                                            <br />
                                            <table width="100%" cellspacing="5" cellpadding="0" class="form-table">
                                                <asp:Repeater ID="rptInvoiceIssueCountrySites" runat="server" onitemdatabound="rptInvoiceIssueCountrySites_ItemDataBound">
                                                    <ItemTemplate>
                                                        <tr>
                                                            <td width="2%">
                                                                <asp:CheckBox ID="chkSite" runat="server" Enabled="false" />
                                                            </td>
                                                            <td>
                                                                <%# Eval("SiteName") %>
                                                            </td>
                                                        </tr>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </table>
                                        </td>
                                        <%# (Container.ItemIndex + 5) % 5 == 4 ? "</tr>" : string.Empty %>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </table>

                            <br />
                            <cc1:ucLabel ID="lblOTIF" runat="server" CssClass="button"></cc1:ucLabel>
                            <table width="100%" cellspacing="5" cellpadding="0" class="form-table">
                                <asp:Repeater ID="rptOtifCountry" runat="server" OnItemDataBound="rptOtifCountry_ItemDataBound">
                                    <ItemTemplate>
                                        <%# (Container.ItemIndex + 5) % 5 == 0 ? "<tr>" : string.Empty %>
                                        <td width="2%">
                                            <asp:CheckBox ID="chkCountry" runat="server" Enabled="false" />
                                        </td>
                                        <td>
                                            <b>
                                                <%# Eval("Country") %></b>
                                        </td>
                                        <%# (Container.ItemIndex + 5) % 5 == 4 ? "</tr>" : string.Empty %>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </table>
                            <br />
                            <cc1:ucLabel ID="lblScorecard" runat="server" CssClass="button"></cc1:ucLabel>
                            <table width="100%" cellspacing="5" cellpadding="0" class="form-table">
                                <asp:Repeater ID="rptScorecardCountry" runat="server" OnItemDataBound="rptScorecardCountry_ItemDataBound">
                                    <ItemTemplate>
                                        <%# (Container.ItemIndex + 5) % 5 == 0 ? "<tr>" : string.Empty %>
                                        <td width="2%">
                                            <asp:CheckBox ID="chkCountry" runat="server" Enabled="false" />
                                        </td>
                                        <td>
                                            <b>
                                                <%# Eval("Country") %></b>
                                        </td>
                                        <%# (Container.ItemIndex + 5) % 5 == 4 ? "</tr>" : string.Empty %>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </table>
                        </cc1:ucPanel>
                    </asp:Panel>
                    <asp:Panel ID="pnlCarrierSction" runat="server" Visible="false">
                        <cc1:ucPanel ID="pnlCarrierDetail" runat="server" CssClass="fieldset-form">
                            <table width="100%" cellspacing="5" cellpadding="0" class="form-table">
                                <asp:Repeater ID="rptCarrierDetails" runat="server">
                                    <ItemTemplate>
                                        <tr>
                                            <td width="5%">
                                                <cc1:ucLabel ID="lblCountry" runat="server" Enabled="false"></cc1:ucLabel>:
                                            </td>
                                            <td>
                                                <%# Eval("Country") %>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="5%">
                                                <cc1:ucLabel ID="lblCarrier" runat="server"></cc1:ucLabel>:
                                            </td>
                                            <td>
                                                <asp:Label ID="lblCarrierName" runat="server" Text='<%#Eval("CarrierName") %>' />
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </table>
                        </cc1:ucPanel>
                        <cc1:ucPanel ID="pnlSites" runat="server" CssClass="fieldset-form">
                            <br />
                            <table width="100%" cellspacing="5" cellpadding="0" class="form-table">
                                <asp:Repeater ID="rptCarrierCountry" runat="server" OnItemDataBound="rptCarrierCountry_ItemDataBound">
                                    <ItemTemplate>
                                        <%# (Container.ItemIndex + 5) % 5 == 0 ? "<tr>" : string.Empty %>
                                        <td valign="top">
                                            <b>
                                                <%# Eval("Country") %></b>
                                            <br />
                                            <table width="100%" cellspacing="5" cellpadding="0" class="form-table">
                                                <asp:Repeater ID="rptCarrierCountrySites" runat="server" OnItemDataBound="rptCarrierCountrySites_ItemDataBound">
                                                    <ItemTemplate>
                                                        <tr>
                                                            <td width="2%">
                                                                <asp:CheckBox ID="chkSite" runat="server" Enabled="false" />
                                                            </td>
                                                            <td>
                                                                <%# Eval("SiteName") %>
                                                            </td>
                                                        </tr>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </table>
                                        </td>
                                        <%# (Container.ItemIndex + 5) % 5 == 4 ? "</tr>" : string.Empty %>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </table>
                        </cc1:ucPanel>
                    </asp:Panel>
                </div>
                <div style="text-align: center; background-color: #fff; padding: 5px;">
                    <cc1:ucButton ID="btnOK" runat="server" CssClass="button" /></div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

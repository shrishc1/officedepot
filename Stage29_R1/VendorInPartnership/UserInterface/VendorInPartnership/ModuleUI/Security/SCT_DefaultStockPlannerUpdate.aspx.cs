﻿using BaseControlLibrary;
using BusinessEntities.ModuleBE.Security;
using BusinessLogicLayer.ModuleBAL.Security;
using Microsoft.Reporting.WebForms;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ModuleUI_Security_SCT_DefaultStockPlannerUpdate : CommonPage
{

    int? ReportRequest = 0;
    //ReportRequestBE oReportRequestBE = new ReportRequestBE();
    //ReportRequestBAL oReportRequestBAL = new ReportRequestBAL();

    #region ReportVariables
    Warning[] warnings;
    string[] streamIds;
    string mimeType = string.Empty;
    string encoding = string.Empty;
    string extension = string.Empty;
    string reportFileName = string.Empty;
    //protected bool IsInEditMode;
    public bool IsInEditMode
    {
        get
        {
            return ViewState["IsInEditMode"] != null ? Convert.ToBoolean(ViewState["IsInEditMode"].ToString()) : false;
        }
        set
        {
            ViewState["IsInEditMode"] = value;
        }
    }
    #endregion

    protected void Page_PreRender(object sender, EventArgs e)
    {

        if (!Page.IsPostBack)
        {
            if (Session["StockPlannerOutput"] != null)
            {
                RetainPlannerData();
            }
        }

    }

    protected void Page_Init(object sender, EventArgs e)
    {
        ucVendorTemplateSelect.CurrentPage = this;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (ViewState["dtStockPlanner"] != null)
        {
            btnExportToExcel1.CurrentPage = this;
            btnExportToExcel1.IsHideHidden = true;
            btnExportToExcel1.GridViewControl = gvResourceExcel;
            btnExportToExcel1.FileName = "DefaultStockPlanners";
            btnExportToExcel1.Visible = true;
        }
    }

    /// <summary>
    /// Method to show Vendor on the basis of selected Template
    /// </summary>
    public override void TemplateSelectedIndexChanged()
    {
        base.TemplateSelectedIndexChanged();

        int VendorTemplateId = Convert.ToInt32(ucVendorTemplateSelect.innerControlddlTemplate.SelectedValue);

        ListBox lstSelectedVendor = (ListBox)msVendor.FindControl("lstRight");
        ListBox lstVendor = (ListBox)msVendor.FindControl("ucVendor").FindControl("lstLeft");

        if (VendorTemplateId > 0)
        {

            SCT_TemplateBAL oSCT_TemplateBAL = new SCT_TemplateBAL();
            SCT_TemplateBE oSCT_TemplateBE = new SCT_TemplateBE();

            oSCT_TemplateBE.VendorTemplateID = VendorTemplateId;
            oSCT_TemplateBE.Action = "GetVendorTemplateInfobyID";
            List<SCT_TemplateBE> lstVendorTemplate = oSCT_TemplateBAL.GetVendorTemplateById(oSCT_TemplateBE);

            if (lstVendorTemplate != null && lstVendorTemplate.Count > 0)
            {
                lstSelectedVendor.Items.Clear();
                msVendor.innerControlHiddenField.Value = string.Empty;
                msVendor.innerControlHiddenField2.Value = string.Empty;
                for (int iCount = 0; iCount < lstVendorTemplate.Count; iCount++)
                {
                    lstSelectedVendor.Items.Add(new ListItem(lstVendorTemplate[iCount].Vendor.Vendor_Name, lstVendorTemplate[iCount].Vendor.VendorID.ToString()));
                    msVendor.SelectedVendorIDs = lstVendorTemplate[iCount].Vendor.VendorID.ToString();
                    msVendor.SelectedVendorName = lstVendorTemplate[iCount].Vendor.Vendor_Name.ToString();
                }
            }
        }
        else
        {
            lstSelectedVendor.Items.Clear();
            msVendor.innerControlHiddenField.Value = string.Empty;
            msVendor.innerControlHiddenField2.Value = string.Empty;
        }
    }

    protected void btnGenerateReport_Click(object sender, EventArgs e)
    {
        GenerateData();
    }

    private void GenerateData()
    {
        DefaultStockPlannerBE oReportRequestBE = new DefaultStockPlannerBE();

        oReportRequestBE.Action = "GetDefaultStockPlanner";
        oReportRequestBE.CountryIDs = msCountry.SelectedCountryIDs;
        oReportRequestBE.SiteIDs = msSite.SelectedSiteIDs;
        oReportRequestBE.VendorIDs = msVendor.SelectedVendorIDs;
        oReportRequestBE.CustomerIDs = msCustomer.SelectedCustomerIDs;
        oReportRequestBE.StockPlannerIDs = msStockPlanner.SelectedStockPlannerIDs;
        oReportRequestBE.VIPDefaultStockPlannerIDs = msStockPlannerDefault.SelectedStockPlannerIDs;
        oReportRequestBE.OD_SKU_No = msSKU.SelectedSKUName;
        oReportRequestBE.Viking_SKU = msVikingSKU.SelectedSKUName;
        oReportRequestBE.ExcludeItems = msExcludeItems.SelectedSKUName;
        oReportRequestBE.PlannerMatch = rdoShowAll.Checked ? 2 : rdoNo.Checked ? 0 : rdoYes.Checked ? 1 : 2;
        oReportRequestBE.SOH = rdoShowAll_1.Checked ? 0 : rdoNo_1.Checked ? 2 : rdoYes_1.Checked ? 1 : 0;

        msCountry.SetCountryOnPostBack();
        msSite.setSitesOnPostBack();
        msVendor.setVendorsOnPostBack();
        msCustomer.setStockPlannerOnPostBack();
        msStockPlanner.SetSPOnPostBack();
        msStockPlannerDefault.SetSPOnPostBack();
        msSKU.setODSkuOnPostBack();
        msVikingSKU.setVikingSkuOnPostBack();
        //msExcludeItems.setODSkuOnPostBack();


        MASCNT_CustomerSetupBAL mASCNT_CustomerSetupBAL = new MASCNT_CustomerSetupBAL();
        DataTable dtStockPlanner = mASCNT_CustomerSetupBAL.GetDefaultStockPlannerReportBAL(oReportRequestBE);

        ViewState["dtStockPlanner"] = dtStockPlanner;

        SetSession(oReportRequestBE);

        GetDefaultStockPlanner();
    }
    private void SetSession(DefaultStockPlannerBE oReportRequestBE)
    {
        Session["StockPlannerOutput"] = null;
        Session.Remove("StockPlannerOutput");

        Hashtable htStockPlannerOutput = new Hashtable();

        htStockPlannerOutput.Add("SelectedVIKINGSKU", oReportRequestBE.Viking_SKU);
        htStockPlannerOutput.Add("SelectedODSKU", oReportRequestBE.OD_SKU_No);
        htStockPlannerOutput.Add("SelectedExcludeItems", oReportRequestBE.ExcludeItems);

        Session["StockPlannerOutput"] = htStockPlannerOutput;
    }

    private void RetainPlannerData()
    {
        Hashtable htStockPlannerOutput = (Hashtable)Session["StockPlannerOutput"];

        //*********************** Viking Sku
        string vikingSkuId = (htStockPlannerOutput.ContainsKey("SelectedVIKINGSKU") && htStockPlannerOutput["SelectedVIKINGSKU"] != null) ? htStockPlannerOutput["SelectedVIKINGSKU"].ToString() : "";
        ucListBox lstRightViking = msVikingSKU.FindControl("lstRight") as ucListBox;
        string hdnSkuid = string.Empty;
        lstRightViking.Items.Clear();
        if (lstRightViking != null && !string.IsNullOrEmpty(vikingSkuId))
        {
            HiddenField hdfSelectedId = msVikingSKU.FindControl("hiddenSelectedId") as HiddenField;
            lstRightViking.Items.Clear();
            string[] strVikingIDs = vikingSkuId.Split(',');
            for (int index = 0; index < strVikingIDs.Length; index++)
            {
                string Item = strVikingIDs[index];
                if (!string.IsNullOrEmpty(Item))
                {
                    ListItem listItem = new ListItem();
                    listItem.Text = Item;
                    hdnSkuid += Item + ",";
                    lstRightViking.Items.Add(listItem);
                }
            }
            hdfSelectedId.Value = hdnSkuid;
        }
        //************** Od sku
        string OdSkuId = (htStockPlannerOutput.ContainsKey("SelectedODSKU") && htStockPlannerOutput["SelectedODSKU"] != null) ? htStockPlannerOutput["SelectedODSKU"].ToString() : "";
        ucListBox lstRightOD = msSKU.FindControl("lstRight") as ucListBox;
        string hdnValue = string.Empty;
        lstRightOD.Items.Clear();
        if (lstRightOD != null && !string.IsNullOrEmpty(OdSkuId))
        {
            HiddenField hdfSelectedId = msSKU.FindControl("hiddenSelectedId") as HiddenField;
            lstRightOD.Items.Clear();
            string[] strODIDs = OdSkuId.Split(',');
            for (int index = 0; index < strODIDs.Length; index++)
            {
                string Item = strODIDs[index];
                if (!string.IsNullOrEmpty(Item))
                {
                    ListItem listItem = new ListItem();
                    listItem.Text = Item;
                    lstRightOD.Items.Add(listItem);
                    hdnValue += Item + ",";
                }
            }
            hdfSelectedId.Value = hdnValue;
        }

        //************** exclude items
        string OdSkuexclude = (htStockPlannerOutput.ContainsKey("SelectedExcludeItems") && htStockPlannerOutput["SelectedExcludeItems"] != null) ? htStockPlannerOutput["SelectedExcludeItems"].ToString() : "";
        ucListBox lstRightexcludeOD = msExcludeItems.FindControl("lstRight") as ucListBox;
        string hdnexcludeValue = string.Empty;
        lstRightexcludeOD.Items.Clear();
        if (lstRightexcludeOD != null && !string.IsNullOrEmpty(OdSkuexclude))
        {
            HiddenField hdfexcludeSelectedId = msExcludeItems.FindControl("hiddenSelectedId") as HiddenField;
            lstRightexcludeOD.Items.Clear();
            string[] strODIDs = OdSkuexclude.Split(',');
            for (int index = 0; index < strODIDs.Length; index++)
            {
                string Item = strODIDs[index];
                if (!string.IsNullOrEmpty(Item))
                {
                    ListItem listItem = new ListItem();
                    listItem.Text = Item;
                    lstRightexcludeOD.Items.Add(listItem);
                    hdnexcludeValue += Item + ",";
                }
            }
            hdfexcludeSelectedId.Value = hdnexcludeValue;
        }
    }

    private void GetDefaultStockPlanner()
    {
        DataTable dtStockPlanner = new DataTable();
        if (ViewState["dtStockPlanner"] != null)
        {
            dtStockPlanner = (DataTable)ViewState["dtStockPlanner"];
        }

        gvResourceEditor.DataSource = dtStockPlanner;
        gvResourceEditor.DataBind();

        gvResourceExcel.DataSource = dtStockPlanner;
        gvResourceExcel.DataBind();

        UcDataPanel.Visible = false;
        UcReportPanel.Visible = true;

    }

    protected void gvResourceEditor_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvResourceEditor.PageIndex = e.NewPageIndex;
        GetDefaultStockPlanner();
    }

    protected void btnEdit_Click(object sender, EventArgs e)
    {
        IsInEditMode = true;
        btnEdit.Visible = !(IsInEditMode);
        btnCancel.Visible = btnSave.Visible = IsInEditMode;

        MASCNT_CustomerSetupBAL mASCNT_CustomerSetupBAL = new MASCNT_CustomerSetupBAL();

        //stock planners
        List<StockPlannerWithSiteBE> stockPlannerWithSites = mASCNT_CustomerSetupBAL.GetStockPlannerWithSiteBAL("GetStockPlannerBySite");
        ViewState["stockPlannerWithSites"] = stockPlannerWithSites;

        //customers
        List<MASCNT_CustomerSetupBE> customerSetupBEs = mASCNT_CustomerSetupBAL.GetCustomersBySiteBAL("GetCustomersBySite");
        ViewState["customerSetupBEs"] = customerSetupBEs;

        GetDefaultStockPlanner();

        btnExportToExcel1.Visible = false;

    }
    protected void btnBack_Click(object sender, EventArgs e)
    {
        UcDataPanel.Visible = true;
        UcReportPanel.Visible = false;

        msSKU.SelectedSKUName = string.Empty;
        msVikingSKU.SelectedSKUName = string.Empty;

        RetainPlannerData();
    }

    protected void gvResourceEditor_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (IsInEditMode)
        {
            List<StockPlannerWithSiteBE> stockPlannerWithSites = new List<StockPlannerWithSiteBE>();
            List<MASCNT_CustomerSetupBE> customerSetupBEs = new List<MASCNT_CustomerSetupBE>();

            if (ViewState["stockPlannerWithSites"] != null)
            {
                stockPlannerWithSites = (List<StockPlannerWithSiteBE>)ViewState["stockPlannerWithSites"];
            }

            if (ViewState["customerSetupBEs"] != null)
            {
                customerSetupBEs = (List<MASCNT_CustomerSetupBE>)ViewState["customerSetupBEs"];
            }

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                HiddenField siteId = (HiddenField)e.Row.FindControl("hdnSiteId");
                ucDropdownList ddlVIPDefaultSP = (ucDropdownList)e.Row.FindControl("ddlVIPDefaultSP");
                ucDropdownList ddlCustomer = (ucDropdownList)e.Row.FindControl("ddlCustomer");
                HiddenField hdnCustomerID = (HiddenField)e.Row.FindControl("hdnCustomerID");
                HiddenField hdnStockPlannerIDDefault = (HiddenField)e.Row.FindControl("hdnStockPlannerIDDefault");

                List<StockPlannerWithSiteBE> stockPlanners = stockPlannerWithSites.Where(w => w.SiteID == Convert.ToInt32(siteId.Value)).ToList();
                if (stockPlanners.Count > 0)
                {
                    FillControls.FillDropDown(ref ddlVIPDefaultSP, stockPlanners, "StockPlanner", "StockPlannerID", "--NA--");

                    ddlVIPDefaultSP.SelectedIndex = ddlVIPDefaultSP.Items.IndexOf(ddlVIPDefaultSP.Items.FindByValue(hdnStockPlannerIDDefault.Value));
                }

                List<MASCNT_CustomerSetupBE> customers = customerSetupBEs.Where(w => w.SiteID == Convert.ToInt32(siteId.Value)).ToList();
                if (customers.Count > 0)
                {
                    FillControls.FillDropDown(ref ddlCustomer, customers, "CustomerName", "CustomerID", "--NA--");

                    ddlCustomer.SelectedIndex = ddlCustomer.Items.IndexOf(ddlCustomer.Items.FindByValue(hdnCustomerID.Value));
                }

            }
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        //CReating Table    
        DataTable SPTable = new DataTable();

        // Adding Columns    
        DataColumn COLUMN = new DataColumn();
        COLUMN.ColumnName = "SKUID";
        COLUMN.DataType = typeof(int);
        SPTable.Columns.Add(COLUMN);

        COLUMN = new DataColumn();
        COLUMN.ColumnName = "VIPStockPlannerID";
        COLUMN.DataType = typeof(int);
        SPTable.Columns.Add(COLUMN);

        COLUMN = new DataColumn();
        COLUMN.ColumnName = "CustomerID";
        COLUMN.DataType = typeof(int);
        SPTable.Columns.Add(COLUMN);

        COLUMN = new DataColumn();
        COLUMN.ColumnName = "PlannerMatch";
        COLUMN.DataType = typeof(bool);
        SPTable.Columns.Add(COLUMN);

        foreach (GridViewRow item in gvResourceEditor.Rows)
        {
            HiddenField hdnSKUID = (HiddenField)item.FindControl("hdnSKUID");
            HiddenField hdnStockPlannerID = (HiddenField)item.FindControl("hdnStockPlannerID");
            ucDropdownList ddlVIPDefaultSP = (ucDropdownList)item.FindControl("ddlVIPDefaultSP");
            ucDropdownList ddlCustomer = (ucDropdownList)item.FindControl("ddlCustomer");

            //if (Convert.ToInt32(ddlVIPDefaultSP.SelectedValue) > 0 || Convert.ToInt32(ddlCustomer.SelectedValue) > 0)
            //{
            DataRow DR = SPTable.NewRow();
            DR[0] = Convert.ToInt32(hdnSKUID.Value);
            DR[1] = !string.IsNullOrEmpty(ddlVIPDefaultSP.SelectedValue) ? Convert.ToInt32(ddlVIPDefaultSP.SelectedValue) : 0;
            DR[2] = !string.IsNullOrEmpty(ddlCustomer.SelectedValue) ? Convert.ToInt32(ddlCustomer.SelectedValue) : 0;
            DR[3] = ddlVIPDefaultSP.SelectedValue == hdnStockPlannerID.Value ? true : false;
            SPTable.Rows.Add(DR);
            //}
        }

        MASCNT_CustomerSetupBAL mASCNT_CustomerSetupBAL = new MASCNT_CustomerSetupBAL();
        mASCNT_CustomerSetupBAL.SaveDefaultStockPlannerBAL(SPTable);


        IsInEditMode = false;
        btnEdit.Visible = !(IsInEditMode);
        btnCancel.Visible = btnSave.Visible = IsInEditMode;

        GenerateData();
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        IsInEditMode = false;
        btnEdit.Visible = !(IsInEditMode);
        btnCancel.Visible = btnSave.Visible = IsInEditMode;

        GetDefaultStockPlanner();
    }
}

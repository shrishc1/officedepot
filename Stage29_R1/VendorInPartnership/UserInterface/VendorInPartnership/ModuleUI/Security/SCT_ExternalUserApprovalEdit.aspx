﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    AutoEventWireup="true" CodeFile="SCT_ExternalUserApprovalEdit.aspx.cs" Inherits="ExternalUserApprovalEdit" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="../../CommonUI/UserControls/ucCountry.ascx" TagName="ucCountry"
    TagPrefix="uc1" %>
<%@ Register Src="../../CommonUI/UserControls/ucSeacrhVendor.ascx" TagName="ucSeacrhVendor"
    TagPrefix="cc3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="sp1" runat="server">
    </asp:ScriptManager>
    <script language="javascript" type="text/javascript">

        $(document).ready(function () {
            var $this = $('#rblApprovalStatus');
            $this.parents('tr').next().hide();
            $this.parents('tr').next().next().hide();
            var rfvCommentRequired = document.getElementById('<%=rfvRejectionCommentRequired.ClientID %>');
            ValidatorEnable(rfvCommentRequired, false);
        });

        function checkedChanged(rblApproveStatus) {
            $this = $(rblApproveStatus);
            var status = $(':checked', $this).val();
            var rfvCommentRequired = document.getElementById('<%=rfvRejectionCommentRequired.ClientID %>');
            if (status == 'approve') {

                $this.parents('tr').next().hide();
                $this.parents('tr').next().next().hide();

                ValidatorEnable(rfvCommentRequired, false);
            }
            else {
                $this.parents('tr').next().show();
                $this.parents('tr').next().next().show();

                ValidatorEnable(rfvCommentRequired, true);
            }
        }

        function Save_ClientClick() {
            var status = $('#rblApprovalStatus input:radio:checked').val();
            if (status == 'approve') {
                var rfvCommentRequired = document.getElementById('<%=rfvRejectionCommentRequired.ClientID %>');

                ValidatorEnable(rfvCommentRequired, false);
            }
        }

    </script>
    <h2>
        <cc1:ucLabel ID="lblApproveUser" runat="server" Text="Approve User"></cc1:ucLabel></h2>
    <asp:UpdatePanel ID="up1" runat="server">
        <ContentTemplate>
            <div class="right-shadow">
                <div class="formbox">
                    <cc1:ucPanel ID="pnlPersonalDetails" runat="server" GroupingText="Personal Details"
                        CssClass="fieldset-form">
                        <table width="100%" cellspacing="5" cellpadding="0" class="form-table">
                            <tr>
                                <td style="font-weight: bold;" width="10%">
                                    <cc1:ucLabel ID="lblUserIdEmail" runat="server" Text="User ID/Email"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold;" width="1%" align="center">
                                    :
                                </td>
                                <td style="font-weight: bold;" width="39%">
                                    <cc1:ucLabel ID="UclblUserId" runat="server" Text=""></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold;" width="7%">
                                </td>
                                <td style="font-weight: bold;" width="1%">
                                </td>
                                <td style="font-weight: bold;" width="42%">
                                </td>
                            </tr>
                            <tr style="display: none">
                                <td style="font-weight: bold">
                                    <cc1:ucLabel ID="lblPassword" runat="server" Text="Password"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold;" align="center">
                                    :
                                </td>
                                <td colspan="4">
                                    <cc1:ucLabel ID="UclblPassword" runat="server" Text=""></cc1:ucLabel>
                                </td>
                            </tr>
                            <tr>
                                <td style="font-weight: bold">
                                    <cc1:ucLabel ID="lblFirstName" runat="server" Text="First Name"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold;" align="center">
                                    :
                                </td>
                                <td>
                                    <cc1:ucLabel ID="UclblFirstName" runat="server" Text=""></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold">
                                    <cc1:ucLabel ID="lblLastName" runat="server" Text="Last Name"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold;" align="center">
                                    :
                                </td>
                                <td>
                                    <cc1:ucLabel ID="UclblLastName" runat="server" Text=""></cc1:ucLabel>
                                </td>
                            </tr>
                            <tr>
                                <td style="font-weight: bold">
                                    <cc1:ucLabel ID="lblContact" runat="server" Text="Contact #"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold;" align="center">
                                    :
                                </td>
                                <td>
                                    <cc1:ucLabel ID="UclblTelephone" runat="server" Text=""></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold">
                                    <cc1:ucLabel ID="lblFax" runat="server" Text="Fax"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold;" align="center">
                                    :
                                </td>
                                <td>
                                    <cc1:ucLabel ID="UclblFax" runat="server" Text=""></cc1:ucLabel>
                                </td>
                            </tr>
                            <tr>
                                <td style="font-weight: bold">
                                    <cc1:ucLabel ID="lblLanguage" runat="server" Text="Language"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold;" align="center">
                                    :
                                </td>
                                <td colspan="4">
                                    <cc1:ucLabel ID="UclblLanguage" runat="server" Text=""></cc1:ucLabel>
                                </td>
                            </tr>
                            <tr>
                                <td style="font-weight: bold">
                                    <cc1:ucLabel ID="lblRole" runat="server" Text="Role"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold;" align="center">
                                    :
                                </td>
                                <td colspan="4">
                                    <cc1:ucLabel ID="UclblRole" runat="server" Text=""></cc1:ucLabel>
                                </td>
                            </tr>
                        </table>
                    </cc1:ucPanel>
                    <cc1:ucPanel ID="pnlCountrySettings" runat="server" CssClass="fieldset-form">
                        <table width="100%" cellspacing="5" cellpadding="0" class="form-table">
                            <asp:Repeater ID="rptCountry" runat="server" OnItemDataBound="rptCountry_ItemDataBound">
                                <ItemTemplate>
                                    <%# (Container.ItemIndex + 5) % 5 == 0 ? "<tr>" : string.Empty %>
                                    <td>
                                        <asp:CheckBox ID="chkCountry" runat="server" Enabled="false" />
                                    </td>
                                    <td width="19%">
                                        <%# Eval("CountryName") %>
                                    </td>
                                    <%# (Container.ItemIndex + 5) % 5 == 4 ? "</tr>" : string.Empty %>
                                </ItemTemplate>
                            </asp:Repeater>
                        </table>
                    </cc1:ucPanel>
                    <asp:Panel ID="pnlVendorSction" runat="server" Visible="false">
                    <cc1:ucPanel ID="pnlVendorDetail" runat="server" CssClass="fieldset-form">
                        <table width="100%" cellspacing="5" cellpadding="0" class="form-table">
                            <asp:Repeater ID="rptVendorDetails" runat="server">
                                <ItemTemplate>
                                    <tr>
                                        <td width="5%">
                                            <cc1:ucLabel ID="lblCountry" runat="server" Enabled="false"></cc1:ucLabel>:
                                        </td>
                                        <td>
                                            <%# Eval("Country") %>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="5%">
                                            <cc1:ucLabel ID="lblVendor" runat="server"></cc1:ucLabel>:
                                        </td>
                                        <td>
                                            <%# Eval("VendorName")%>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                        </table>
                    </cc1:ucPanel>
                    <cc1:ucPanel ID="pnlModules" runat="server" CssClass="fieldset-form">
                       <br />
                        <cc1:ucLabel ID="lblAppointmentScheduling" runat="server" CssClass="button"></cc1:ucLabel>
                            
                        <table width="100%" cellspacing="5" cellpadding="0" class="form-table">
                            <asp:Repeater ID="rptAppointmentCountry" runat="server" 
                                onitemdatabound="rptAppointmentCountry_ItemDataBound">
                                <ItemTemplate>
                                    <%# (Container.ItemIndex + 5) % 5 == 0 ? "<tr>" : string.Empty %>
                                    <td valign="top">
                                        <b>
                                            <%# Eval("Country") %></b>
                                        <br />
                                        <table width="100%" cellspacing="5" cellpadding="0" class="form-table">
                                            <asp:Repeater ID="rptAppointmentCountrySites" runat="server" OnItemDataBound="rptAppointmentCountrySites_ItemDataBound">
                                                <ItemTemplate>
                                                    <tr>
                                                        <td width="2%">
                                                            <asp:CheckBox ID="chkSite" runat="server"  Enabled="false" />
                                                        </td>
                                                        <td>
                                                            <%# Eval("SiteName") %>
                                                        </td>
                                                    </tr>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </table>
                                    </td>
                                    <%# (Container.ItemIndex + 5) % 5 == 4 ? "</tr>" : string.Empty %>
                                </ItemTemplate>
                            </asp:Repeater>
                        </table>
                        <br />
                        <cc1:ucLabel ID="lblDiscrepancies" runat="server" CssClass="button"></cc1:ucLabel>
                        <table width="100%" cellspacing="5" cellpadding="0" class="form-table">
                            <asp:Repeater ID="rptDiscrepanciesCountry" runat="server" 
                                onitemdatabound="rptDiscrepanciesCountry_ItemDataBound">
                                <ItemTemplate>
                                    <%# (Container.ItemIndex + 5) % 5 == 0 ? "<tr>" : string.Empty %>
                                    <td valign="top">
                                        <b>
                                            <%# Eval("Country") %></b>
                                        <br />
                                        <table width="100%" cellspacing="5" cellpadding="0" class="form-table">
                                            <asp:Repeater ID="rptDiscrepanciesCountrySites" runat="server" onitemdatabound="rptDiscrepanciesCountrySites_ItemDataBound">
                                                <ItemTemplate>
                                                    <tr>
                                                        <td width="2%">
                                                            <asp:CheckBox ID="chkSite" runat="server" Enabled="false" />
                                                        </td>
                                                        <td>
                                                            <%# Eval("SiteName") %>
                                                        </td>
                                                    </tr>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </table>
                                    </td>
                                    <%# (Container.ItemIndex + 5) % 5 == 4 ? "</tr>" : string.Empty %>
                                </ItemTemplate>
                            </asp:Repeater>
                        </table>

                        <br />
                        <cc1:ucLabel ID="lblInvoiceIssue_1" runat="server" CssClass="button"></cc1:ucLabel>
                        <table width="100%" cellspacing="5" cellpadding="0" class="form-table">
                            <asp:Repeater ID="rptInvoiceIssueCountry" runat="server" 
                                onitemdatabound="rptInvoiceIssueCountry_ItemDataBound">
                                <ItemTemplate>
                                    <%# (Container.ItemIndex + 5) % 5 == 0 ? "<tr>" : string.Empty %>
                                    <td valign="top">
                                        <b>
                                            <%# Eval("Country") %></b>
                                        <br />
                                        <table width="100%" cellspacing="5" cellpadding="0" class="form-table">
                                            <asp:Repeater ID="rptInvoiceIssueCountrySites" runat="server" onitemdatabound="rptInvoiceIssueCountrySites_ItemDataBound">
                                                <ItemTemplate>
                                                    <tr>
                                                        <td width="2%">
                                                            <asp:CheckBox ID="chkSite" runat="server" Enabled="false" />
                                                        </td>
                                                        <td>
                                                            <%# Eval("SiteName") %>
                                                        </td>
                                                    </tr>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </table>
                                    </td>
                                    <%# (Container.ItemIndex + 5) % 5 == 4 ? "</tr>" : string.Empty %>
                                </ItemTemplate>
                            </asp:Repeater>
                        </table>
                        <br />
                        <cc1:ucLabel ID="lblOTIF" runat="server" CssClass="button"></cc1:ucLabel>
                        <table width="100%" cellspacing="5" cellpadding="0" class="form-table">
                            <asp:Repeater ID="rptOtifCountry" runat="server" OnItemDataBound="rptOtifCountry_ItemDataBound">
                                <ItemTemplate>
                                    <%# (Container.ItemIndex + 5) % 5 == 0 ? "<tr>" : string.Empty %>
                                    <td width="2%">
                                        <asp:CheckBox ID="chkCountry" runat="server" Enabled="false" />
                                    </td>
                                    <td>
                                        <b><%# Eval("Country") %></b>
                                    </td>
                                    <%# (Container.ItemIndex + 5) % 5 == 4 ? "</tr>" : string.Empty %>
                                </ItemTemplate>
                            </asp:Repeater>
                        </table>
                        <br />
                        <cc1:ucLabel ID="lblScorecard" runat="server" CssClass="button"></cc1:ucLabel>
                        <table width="100%" cellspacing="5" cellpadding="0" class="form-table">
                            <asp:Repeater ID="rptScorecardCountry" runat="server" OnItemDataBound="rptScorecardCountry_ItemDataBound">
                                <ItemTemplate>
                                    <%# (Container.ItemIndex + 5) % 5 == 0 ? "<tr>" : string.Empty %>
                                    <td width="2%">
                                        <asp:CheckBox ID="chkCountry" runat="server" Enabled="false" />
                                    </td>
                                    <td>
                                        <b><%# Eval("Country") %></b>
                                    </td>
                                    <%# (Container.ItemIndex + 5) % 5 == 4 ? "</tr>" : string.Empty %>
                                </ItemTemplate>
                            </asp:Repeater>
                        </table>
                    </cc1:ucPanel>
                    </asp:Panel>
                    <asp:Panel ID="pnlCarrierSction" runat="server" Visible="false">
                        <cc1:ucPanel ID="pnlCarrierDetail" runat="server" CssClass="fieldset-form">
                            <table width="100%" cellspacing="5" cellpadding="0" class="form-table">
                                <asp:Repeater ID="rptCarrierDetails" runat="server">
                                    <ItemTemplate>
                                        <tr>
                                            <td width="5%">
                                                <cc1:ucLabel ID="lblCountry" runat="server" Enabled="false"></cc1:ucLabel>:
                                            </td>
                                            <td>
                                                <%# Eval("Country") %>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="5%">
                                                <cc1:ucLabel ID="lblCarrier" runat="server"></cc1:ucLabel>:
                                            </td>
                                            <td>
                                                <%# Eval("CarrierName")%>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </table>
                        </cc1:ucPanel>
                       <cc1:ucPanel ID="pnlSites" runat="server" CssClass="fieldset-form">
                       <br />
                       <table width="100%" cellspacing="5" cellpadding="0" class="form-table">
                            <asp:Repeater ID="rptCarrierCountry" runat="server" 
                                onitemdatabound="rptCarrierCountry_ItemDataBound">
                                <ItemTemplate>
                                    <%# (Container.ItemIndex + 5) % 5 == 0 ? "<tr>" : string.Empty %>
                                    <td valign="top">
                                        <b>
                                            <%# Eval("Country") %></b>
                                        <br />
                                        <table width="100%" cellspacing="5" cellpadding="0" class="form-table">
                                            <asp:Repeater ID="rptCarrierCountrySites" runat="server" OnItemDataBound="rptCarrierCountrySites_ItemDataBound">
                                                <ItemTemplate>
                                                    <tr>
                                                        <td width="2%">
                                                            <asp:CheckBox ID="chkSite" runat="server"  Enabled="false" />
                                                        </td>
                                                        <td>
                                                            <%# Eval("SiteName") %>
                                                        </td>
                                                    </tr>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </table>
                                    </td>
                                    <%# (Container.ItemIndex + 5) % 5 == 4 ? "</tr>" : string.Empty %>
                                </ItemTemplate>
                            </asp:Repeater>
                        </table>
                    </cc1:ucPanel>
                    </asp:Panel>
                    <cc1:ucPanel ID="pnlApprove" runat="server" CssClass="fieldset-form" GroupingText=""
                        Style="display: block" Width="100%">
                        <table width="100%" cellspacing="5" cellpadding="0" class="form-table">
                            <tr>
                                <td width="100%">
                                    <table width="100%">
                                        <tr>
                                            <td style="font-weight: bold;" width="14%">
                                                <cc1:ucLabel ID="lblApprovalStatus" runat="server" Text="Approval Status"></cc1:ucLabel>
                                            </td>
                                            <td style="font-weight: bold;" width="1%">
                                                :
                                            </td>
                                            <td style="font-weight: bold;" width="85%">
                                                <cc1:ucRadioButtonList runat="server" ClientIDMode="Static" ID="rblApprovalStatus"
                                                    RepeatDirection="Horizontal" onclick="javascript:checkedChanged(this);" CellPadding="5"
                                                    CellSpacing="10">
                                                    <asp:ListItem Value="approve" Text="Approve" Selected="true" />
                                                    <asp:ListItem Value="reject" Text="Reject" />
                                                </cc1:ucRadioButtonList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: bold;" width="14%">
                                                <cc1:ucLabel ID="lblRejectionComment" runat="server" Text="Rejection Comments"></cc1:ucLabel>
                                            </td>
                                            <td style="font-weight: bold;" width="1%">
                                                :
                                            </td>
                                            <td width="85%">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td colspan="3">
                                                <cc1:ucTextbox ID="txtRejectComments" runat="server" onkeyup="checkTextLengthOnKeyUp(this,1000);"
                                                    CssClass="textarea" TextMode="MultiLine" Width="900px"></cc1:ucTextbox>
                                                <asp:RequiredFieldValidator ID="rfvRejectionCommentRequired" ClientIDMode="Static"
                                                    runat="server" ControlToValidate="txtRejectComments" Display="None" Style="display: none;"
                                                    ValidationGroup="Save" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </cc1:ucPanel>
                </div>
            </div>
            <div class="bottom-shadow">
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <div class="button-row">
    <cc1:ucButton ID="btnProceed" runat="server" Text="Proceed" CssClass="button" Visible="false"
            onclick="btnProceed_Click"/>
        <cc1:ucButton ID="btnSave" runat="server" Text="Save" CssClass="button" OnClientClick="Save_ClientClick();"
            OnClick="btnSave_Click" ValidationGroup="Save" />
        <asp:ValidationSummary ID="VSSave" runat="server" ValidationGroup="Save" ShowSummary="false"
            ShowMessageBox="true" />

    </div>
</asp:Content>

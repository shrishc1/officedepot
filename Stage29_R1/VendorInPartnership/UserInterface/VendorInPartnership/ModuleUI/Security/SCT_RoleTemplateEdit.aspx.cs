﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccessLayer;
using System.Web.Services;
using BusinessEntities.ModuleBE.Security;
using BusinessLogicLayer.ModuleBAL.Security;
using Utilities;
using WebUtilities;

public partial class SCT_RoleTemplateEdit : CommonPage {
    protected string deleteMessage = WebCommon.getGlobalResourceValue("DeleteMessage");
    protected void Page_Load(object sender, EventArgs e) {
        txtTemplate.Focus();
        if (!Page.IsPostBack) {

            GenerateScreenTable();

            // populate the data in dropdown list role
            SCT_TemplateBAL oSCT_TemplateBAL = new SCT_TemplateBAL();
            SCT_UserRoleBE oSCT_UserRoleBE = new SCT_UserRoleBE();
            SCT_TemplateBE oSCT_TemplateBE = new SCT_TemplateBE();

            oSCT_UserRoleBE.Action = "GetODUsers";

            ddlRoles.DataSource = oSCT_TemplateBAL.GetUserRolesBAL(oSCT_UserRoleBE);
            oSCT_TemplateBAL = null;
            ddlRoles.DataTextField = "RoleName";
            ddlRoles.DataValueField = "UserRoleID";
            ddlRoles.DataBind();
            if (GetQueryStringValue("TemplateID") != null) {
                int templateID = Convert.ToInt32(GetQueryStringValue("TemplateID"));
                oSCT_TemplateBE.TemplateID = templateID;
                PopulateFieldValues(oSCT_TemplateBE);
                btnDelete.Visible = true;

                btnDelete.Visible = templateID == 1 ? false : true;
                btnSave.Visible = templateID == 1 ? false : true;
            }
        }
    }

    private void PopulateFieldValues(SCT_TemplateBE oSCT_TemplateBE) {
        SCT_TemplateBAL oSCT_TemplateBAL = new SCT_TemplateBAL();
        oSCT_TemplateBE.Action = "GetTemplateInfo";
        oSCT_TemplateBE = oSCT_TemplateBAL.GetTemplateInfo(oSCT_TemplateBE);
        oSCT_TemplateBAL = null;
        txtTemplate.Text = oSCT_TemplateBE.TemplateName;
        ddlRoles.SelectedIndex = ddlRoles.Items.IndexOf(ddlRoles.Items.FindByText(oSCT_TemplateBE.RoleName));
        // hfReadScreenID.Value = oSCT_TemplateBE.ScreenWithReadPermission;
        //  hfWriteScreenID.Value = oSCT_TemplateBE.ScreenWithWritePermission;
        hfSelScreenID.Value = oSCT_TemplateBE.SelectedScreen;
    }

    protected override void OnPreRender(EventArgs e) {
        if (!Page.IsPostBack) {
            CommonPage cm = new CommonPage();
            cm.GlobalResourceFields(this.Page);
        }
    }


    #region Method



    System.Text.StringBuilder sbMemu = new System.Text.StringBuilder();


    string TableEndString = "</tbody></table>";

    string prevModuleName = "";
    string backGroundColor = "";
    int moduleCount = 2;
    private string GetStartModuleString(string ModuleName, int ParentID) {
        string StartModuleString = string.Empty;
        ModuleName = WebCommon.getGlobalResourceValue(ModuleName);

        if (ParentID == 0 && prevModuleName != ModuleName && prevModuleName != "") {
            sbMemu.AppendLine(TableEndString);
        }
        backGroundColor = "background-color: #f7f6f3; color: #333333;";
        if (ParentID == 0) //style='text-align:left;padding-left:10px;'
        {
            prevModuleName = ModuleName;
            StartModuleString = @"<br/><table class='grid' cellspacing='0' cellpadding='0' width='80%'><thead style='border: thin solid black;font-weight: bold;'><tr><th style='text-align:left;padding-left:10px;'>" + ModuleName + "</th><th></th></tr></thead><tbody>";
        }
        else {
            StartModuleString = @"<tr style='font-weight: bold;" + backGroundColor + "'><td class='template-heading' align='left'>" + ModuleName + "</td><td></td></tr>";
        }
        moduleCount++;
        return StartModuleString;
    }
    private string GetScreenString(string ScreenID, string ScreenName, string ScreenUrl, char FunctionType, int ParentModuleID) {
        string ScreenString = string.Empty;
        string ScreenParamName = string.Empty;
        string CheckBoxString = string.Empty;
        ScreenParamName = ScreenName;
        ScreenName = WebCommon.getGlobalResourceValue(ScreenName);

        if (ScreenParamName == "ChangePassword") {
            CheckBoxString = "<input type='checkbox' checked='checked' disabled='disabled' data-screen-id='" + ScreenID + "' id='Sel" + ScreenID + "' />";
        }
        else if (ScreenParamName == "SearchDiscrepancies") {
            hfSearchDiscrepancies.Value = "Sel" + ScreenID.ToString();
            CheckBoxString = "<input type='checkbox' onclick='CheckWorkList();' data-screen-id='" + ScreenID + "' id='Sel" + ScreenID + "' />";
        }
        else if (ScreenParamName == "WorkList") {
            hfWorkList.Value = "Sel" + ScreenID.ToString();
            CheckBoxString = "<input type='checkbox' data-screen-id='" + ScreenID + "' id='Sel" + ScreenID + "' />";
        }
        else {
            CheckBoxString = "<input type='checkbox' data-screen-id='" + ScreenID + "' id='Sel" + ScreenID + "' />";
        }

        if (ParentModuleID == 0) {
            ScreenString = "<tr><td class='template-heading' align='left'>" + ScreenName + "</td>"
                                 + "<td align='right' style='padding-right:30px'>" + CheckBoxString + "</td></tr>";

        }
        else {
            ScreenString = "<tr><td class='template-heading-content' align='left'>" + ScreenName + "</td>"
                          + "<td align='right' style='padding-right:30px'>" + CheckBoxString + "</td></tr>";
        }

        // for read only menu
        if (ScreenParamName == "VendorSetup")
        {                
            ScreenString += "<tr><td class='template-heading-content' align='left'>" + WebCommon.getGlobalResourceValue("VendorSetupReadOnly") + "</td>"
                         + "<td align='right' style='padding-right:30px'><input type='checkbox' disabled checked=true data-screen-id='" + 1000 + "' id='Sel" + 1000 + "' /></td></tr>";          
        }

        if (ScreenParamName == "UserOverview")
        {
            ScreenString += "<tr><td class='template-heading-content' align='left'>" + WebCommon.getGlobalResourceValue("UserOverviewOnly") + "</td>"
                         + "<td align='right' style='padding-right:30px'><input type='checkbox' disabled checked=true data-screen-id='" + 1000 + "' id='Sel" + 1000 + "' /></td></tr>";
        }
        // read only menu end
        return ScreenString;
    }

    private bool IsAllowedScreenToCurrenUser(string ScreenID) {
        return true;
    }
    private void GenerateScreenTable() {
        GetModule("0");
        //ltMenu.Text = TableStartString + sbMemu.ToString() + TableEndString;
        ltMenu.Text = sbMemu.ToString() + TableEndString;
    }

    private void GetModule(string ParentModuleID) {
        SCT_ModuleBAL oSCT_ModuleBAL = new SCT_ModuleBAL();
        SCT_ModuleBE oSCT_ModuleBE = new SCT_ModuleBE();

        oSCT_ModuleBE.Action = "GetModule";
        oSCT_ModuleBE.ParentModuleID = Convert.ToInt32(ParentModuleID);

        List<SCT_ModuleBE> lstModules = new List<SCT_ModuleBE>();

        lstModules = oSCT_ModuleBAL.GetModuleBAL(oSCT_ModuleBE);
        oSCT_ModuleBAL = null;
        for (int iCount = 0; iCount <= lstModules.Count - 1; iCount++) {
            sbMemu.AppendLine(GetStartModuleString(lstModules[iCount].ModuleName, Convert.ToInt32(ParentModuleID)));
            GetModule(lstModules[iCount].ModuleID.ToString());
        }
        GetScreen(ParentModuleID);
    }

    private void GetScreen(string ModuleID) {

        SCT_UserScreenBE oSCT_ModuleScreenBE = new SCT_UserScreenBE();
        SCT_UserScreenBAL oSCT_ModuleScreenBAL = new SCT_UserScreenBAL();

        oSCT_ModuleScreenBE.Screen = new SCT_ScreenBE();
        oSCT_ModuleScreenBE.Module = new SCT_ModuleBE();
        oSCT_ModuleScreenBE.Action = "GetModuleScreen";
        oSCT_ModuleScreenBE.Screen.ModuleID = Convert.ToInt32(ModuleID);
        oSCT_ModuleScreenBE.UserID = Convert.ToInt32(Session["UserID"]);

        List<SCT_UserScreenBE> lstModuleScreen = new List<SCT_UserScreenBE>();

        lstModuleScreen = oSCT_ModuleScreenBAL.GetModuleScreenBAL(oSCT_ModuleScreenBE);
        oSCT_ModuleScreenBAL = null;
        for (int iCount = 0; iCount <= lstModuleScreen.Count - 1; iCount++) {
            sbMemu.AppendLine(GetScreenString(lstModuleScreen[iCount].ScreenID.ToString(), lstModuleScreen[iCount].Screen.ScreenName.ToString(), lstModuleScreen[iCount].Screen.ScreenUrl.ToString(), lstModuleScreen[iCount].Screen.FunctionType, lstModuleScreen[iCount].Module.ParentModuleID));
        }
    }
    #endregion
    protected void btnSave_Click(object sender, EventArgs e) {
        try {
            if (string.IsNullOrEmpty(hfSelScreenID.Value)) {
                string ScreenRequired = WebCommon.getGlobalResourceValue("ScreenRequired");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + ScreenRequired + "')", true);
                return;
            }
            SCT_TemplateBE oSCT_TemplateBE = new SCT_TemplateBE();
            SCT_TemplateBAL oSCT_TemplateBAL = new SCT_TemplateBAL();

            oSCT_TemplateBE.SelectedScreen = hfSelScreenID.Value.ToString().Trim(',');
            oSCT_TemplateBE.TemplateName = string.IsNullOrWhiteSpace(txtTemplate.Text.Trim()) ? string.Empty : txtTemplate.Text.Trim();
            oSCT_TemplateBE.ForRole = Convert.ToInt32(ddlRoles.SelectedValue);

            if (GetQueryStringValue("TemplateID") == null) {
                oSCT_TemplateBE.Action = "Insert";
            }
            else {
                oSCT_TemplateBE.Action = "Update";
                oSCT_TemplateBE.TemplateID = Convert.ToInt32(GetQueryStringValue("TemplateID").ToString());
            }

            oSCT_TemplateBAL.SavePermissionToDB(oSCT_TemplateBE);
            oSCT_TemplateBAL = null;
            EncryptQueryString("SCT_RoleTemplateOverview.aspx");
        }
        catch { }
    }
    protected void btnBack_Click(object sender, EventArgs e) {
        if (GetQueryStringValue("UserID") != null) {
            EncryptQueryString("SCT_UserEdit.aspx?UserId=" + GetQueryStringValue("UserID").ToString() + "&FromPage=SCT_RoleTemplateEdit");
        }
        else
            EncryptQueryString("SCT_RoleTemplateOverview.aspx");
    }

    protected void btnDelete_Click(object sender, EventArgs e) {
        try {           
            SCT_TemplateBE oSCT_TemplateBE = new SCT_TemplateBE();
            SCT_TemplateBAL oSCT_TemplateBAL = new SCT_TemplateBAL();
            oSCT_TemplateBE.Action = "Delete";
            oSCT_TemplateBE.TemplateName = string.IsNullOrWhiteSpace(txtTemplate.Text.Trim()) ? string.Empty : txtTemplate.Text.Trim();          
            oSCT_TemplateBE.TemplateID = Convert.ToInt32(GetQueryStringValue("TemplateID").ToString());
            int? iResult = oSCT_TemplateBAL.DeleteTemplateByIdBAL(oSCT_TemplateBE);
            oSCT_TemplateBAL = null;
            if (iResult == 1)
                EncryptQueryString("SCT_RoleTemplateOverview.aspx");
            else {
                string ScreenRequired = WebCommon.getGlobalResourceValue("Templatealreadyassigned");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + ScreenRequired + "')", true);               
            }
        }
        catch { }
    }
}
﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    AutoEventWireup="true" CodeFile="ChangePassword.aspx.cs" Inherits="ModuleUI_Security_ChangePassword" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h2>
        <cc1:ucLabel ID="lblChangePassword" runat="server" Text="Change Password"></cc1:ucLabel>
    </h2>
    <div class="right-shadow">
        <div class="formbox">
            <table width="65%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                <tr>
                    <td style="font-weight: bold; width: 30%">
                        <cc1:ucLabel ID="lblCurrentPassword" runat="server" Text="Please enter Current Password"
                            isRequired="true"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold; width: 1%">
                        <cc1:ucLabel ID="Label17" runat="server">:</cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold; width: 69%">
                        <cc1:ucTextbox ID="txtCurrentPassword" TextMode="Password" runat="server" Width="200px"
                            MaxLength="50"></cc1:ucTextbox>
                        <asp:RequiredFieldValidator ID="rfvCurrentPasswordRequired" runat="server" ErrorMessage="Please enter Current Password"
                            Display="None" ValidationGroup="OK" ControlToValidate="txtCurrentPassword"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td style="font-weight: bold;">
                        <cc1:ucLabel ID="lblNewPassword" runat="server" Text="Enter New Password" isRequired="true"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold;">
                        <cc1:ucLabel ID="UcLabel2" runat="server">:</cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold;">
                        <cc1:ucTextbox ID="txtNewPassword" TextMode="Password" runat="server" Width="200px"
                            MaxLength="50"></cc1:ucTextbox>
                        <asp:RequiredFieldValidator ID="rfvNewPasswordRequired" runat="server" ErrorMessage="Please enter New Password"
                            Display="None" ValidationGroup="OK" ControlToValidate="txtNewPassword"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td style="font-weight: bold;">
                        <cc1:ucLabel ID="lblConfirmPassword" runat="server" Text="Confirm New Password" isRequired="true"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold;">
                        <cc1:ucLabel ID="UcLabel4" runat="server">:</cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold;">
                        <cc1:ucTextbox ID="txtConfirmPassword" TextMode="Password" runat="server" Width="200px"
                            MaxLength="50"></cc1:ucTextbox>
                        <asp:RequiredFieldValidator ID="rfvConfirmPasswordRequired" runat="server" ErrorMessage="Please enter Confirm Password"
                            Display="None" ValidationGroup="OK" ControlToValidate="txtConfirmPassword"></asp:RequiredFieldValidator>
                        <asp:CompareValidator ID="cmpvPasswordCompare" runat="server" ErrorMessage="New Password and Confirm Password does not match"
                            Display="None" ValidationGroup="OK" Type="String" Operator="Equal" ControlToValidate="txtConfirmPassword"
                            ControlToCompare="txtNewPassword" />
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td colspan="2" align="right">
                        <cc1:ucButton ID="btnOK" runat="server" Text="OK" CssClass="button" OnClick="btnOK_Click"
                            ValidationGroup="OK" />
                        <cc1:ucButton ID="btnCancel" runat="server" Text="Cancel" class="button" OnClick="btnCancel_Click" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:ValidationSummary ID="vs1" runat="server" ValidationGroup="OK" ShowSummary="false"
                            ShowMessageBox="true" />
                    </td>
                    <td>
                        
                    </td>
                    <td><cc1:ucLabel ID="lblIsPasswordStrongMessage" runat="server" ForeColor="Red"></cc1:ucLabel></td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>

﻿using System;
using System.Collections.Generic;
using BusinessEntities.ModuleBE.Security;
using BusinessLogicLayer.ModuleBAL.Security;
using Utilities;
using WebUtilities;
using System.Web.UI;
using System.IO;
using System.Data;
using System.Globalization;
using System.Configuration;
using AA.switchprotocol;
using BusinessEntities.ModuleBE.Languages;
using BusinessLogicLayer.ModuleBAL.Languages;
using System.Web;
using System.Linq;

public partial class Login : AA.switchprotocol.UI.SecurePage
{

    protected string ValidEmail = WebCommon.getGlobalResourceValue("ValidEmail");
    
    #region Events
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["ModuleName"] = "";

        divFixed.Visible= (ConfigurationManager.AppSettings.AllKeys.Any(key => key == "IsDev") && 
            Convert.ToBoolean(ConfigurationManager.AppSettings["IsDev"].ToString()));
        
        //Add by Abhinav to fix the language cache lost problem
        if (System.Web.HttpRuntime.Cache["LanguageTagsEnglish"] == null
           || System.Web.HttpRuntime.Cache["LanguageTagsFrench"] == null
           || System.Web.HttpRuntime.Cache["LanguageTagsGerman"] == null
           || System.Web.HttpRuntime.Cache["LanguageTagsDutch"] == null
           || System.Web.HttpRuntime.Cache["LanguageTagsSpanish"] == null
           || System.Web.HttpRuntime.Cache["LanguageTagsItalian"] == null
           || System.Web.HttpRuntime.Cache["LanguageTagsCzech"] == null) {
            Utility.LoadLanguageTags();
        }
    }
    protected void btnLogin_Click(object sender, EventArgs e)
    {
        try
        {
            //if (ViewState["Final_Counter"] == null) {
            //    ViewState["Final_Counter"] = 0;
            //}

            if (txtUserName.Value.Trim() != "" && txtPassword.Value.Trim() != "")
            {
                SCT_UserBE oSCT_UserBE = new SCT_UserBE();
                SCT_UserBAL oSCT_UserBAL = new SCT_UserBAL();

                oSCT_UserBE.Action = "GetDetails";
                oSCT_UserBE.LoginID = txtUserName.Value.Trim();
                oSCT_UserBE.Password = txtPassword.Value.Trim();

                List<SCT_UserBE> lstSCT_UserBE = oSCT_UserBAL.GetUserDetailsBAL(oSCT_UserBE);
                if (lstSCT_UserBE.Count > 0 && lstSCT_UserBE[0].LoginID != null)
                {
                    if (lstSCT_UserBE[0].AccountStatus.ToLower() == "active" || lstSCT_UserBE[0].AccountStatus.ToLower() == "awaiting activation"
                        || lstSCT_UserBE[0].AccountStatus.Equals("Edited Awaiting Approval"))
                    {
                        //ViewState["Final_Counter"] = 0;
                        divError.Style["display"] = "none";
                        divBlocked.Style["display"] = "none";

                        Session["LoginID"] = txtUserName.Value.Trim();
                        Session["UserID"] = lstSCT_UserBE[0].UserID.ToString();
                        Session["Role"] = lstSCT_UserBE[0].RoleName;
                        Session["UserName"] = lstSCT_UserBE[0].FullName.ToString();
                        Session["UserPhoneNo"] = lstSCT_UserBE[0].PhoneNumber;
                        Session["UserRoleId"] = lstSCT_UserBE[0].UserRoleID;
                        //Session["UserCarrierID"] = lstSCT_UserBE[0].CarrierID.ToString();

                        if (Session["Role"].ToString().ToLower() == "vendor")
                        {
                            //vendor site
                            SCT_UserBE oNewSCT_UserBE = new SCT_UserBE();
                            oNewSCT_UserBE.Action = "GetVendorSite";
                            // oNewSCT_UserBE.VendorID = lstSCT_UserBE[0].VendorID;
                            oNewSCT_UserBE.UserID = lstSCT_UserBE[0].UserID;

                            oNewSCT_UserBE = oSCT_UserBAL.GetUserDefaultsBAL(oNewSCT_UserBE);

                            if (oNewSCT_UserBE.Site != null && oNewSCT_UserBE.Site.SiteID != 0)
                            {
                                Session["SiteID"] = oNewSCT_UserBE.Site.SiteID.ToString();
                                Session["SiteCountryID"] = oNewSCT_UserBE.Site.SiteCountryID.ToString();
                            }

                        }
                        else if (Session["Role"].ToString().ToLower() == "carrier")
                        {
                            //carrier site
                            SCT_UserBE oNewSCT_UserBE = new SCT_UserBE();
                            oNewSCT_UserBE.Action = "GetCarrierSite";
                            oNewSCT_UserBE.UserID = lstSCT_UserBE[0].UserID;

                            oNewSCT_UserBE = oSCT_UserBAL.GetUserDefaultsBAL(oNewSCT_UserBE);
                            if (oNewSCT_UserBE.Site != null && oNewSCT_UserBE.Site.SiteID != 0)
                            {
                                Session["SiteID"] = oNewSCT_UserBE.Site.SiteID.ToString();
                                Session["SiteCountryID"] = oNewSCT_UserBE.Site.SiteCountryID.ToString();
                            }

                        }
                        else
                        {
                            oSCT_UserBE.Action = "GetDefault";
                            oSCT_UserBE.UserID = lstSCT_UserBE[0].UserID;
                            oSCT_UserBE.RoleName = Session["Role"].ToString();
                            oSCT_UserBE = oSCT_UserBAL.GetUserDefaultsBAL(oSCT_UserBE);

                            if (oSCT_UserBE.Site != null && oSCT_UserBE.Site.SiteID != 0)
                            {
                                Session["SiteID"] = oSCT_UserBE.Site.SiteID.ToString();
                                Session["SiteCountryID"] = oSCT_UserBE.Site.SiteCountryID.ToString();
                            }
                            else
                            {

                                string SiteNotAllocated = WebCommon.getGlobalResourceValue("SiteNotAllocated");
                                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + SiteNotAllocated + "');window.location.href='Login.aspx';", true);
                                return;
                            }
                        }


                        //Get Language
                        oSCT_UserBE.Action = "GetUserLanguageCode";
                        oSCT_UserBE.EmailId = txtUserName.Value.Trim();
                        DataSet dsLanguage = new DataSet();
                        dsLanguage = oSCT_UserBAL.GetLanguageIdBAL(oSCT_UserBE);

                        if (dsLanguage != null && dsLanguage.Tables.Count > 0 && dsLanguage.Tables[0].Rows.Count > 0)
                        {
                            Page.UICulture = dsLanguage.Tables[0].Rows[0]["ISOLanguageName"].ToString();
                            Session["CultureInfo"] = dsLanguage.Tables[0].Rows[0]["ISOLanguageName"].ToString();
                            Session["UserLanguage"] = dsLanguage.Tables[0].Rows[0]["Language"].ToString();
                        }
                        else
                        {
                            Page.UICulture = clsConstants.EnglishISO;
                            Session["CultureInfo"] = clsConstants.EnglishISO;
                            Session["UserLanguage"] = clsConstants.English;
                        }

                        System.Web.HttpRuntime.Cache.Insert("UserLanguage", Session["UserLanguage"], null,
                               System.Web.Caching.Cache.NoAbsoluteExpiration,
                               System.Web.Caching.Cache.NoSlidingExpiration);

                        //--------------------------------------------------
                        if (lstSCT_UserBE[0].AccountStatus.Trim().ToUpper() == "Awaiting Activation".ToUpper())
                        {
                            oSCT_UserBAL = null;
                            EncryptQueryString("SCT_TermsAndConditions.aspx");
                        }
                        //Sprint 2 - update Loggin time
                        oSCT_UserBE.Action = "UpdateLoggedInTime";
                        oSCT_UserBE.UserID = lstSCT_UserBE[0].UserID;
                        oSCT_UserBE.Loggedin_Browser = Request.Browser.Type;
                        oSCT_UserBAL.UpdateAccountStatus(oSCT_UserBE);
                        oSCT_UserBAL = null;
                        //-----------------//
                        EncryptQueryString("~/ModuleUI/Dashboard/DefaultDashboard.aspx");

                    }
                    else if (lstSCT_UserBE[0].AccountStatus.ToLower() == "blocked")
                    {
                        //ViewState["Final_Counter"] = Convert.ToInt16(ViewState["Final_Counter"]) + 1;
                        divBlocked.Style["display"] = "block";
                        divError.Style["display"] = "none";
                    }

                }
                else
                {
                    divError.Style["display"] = "block";
                    divBlocked.Style["display"] = "none";
                }

                //if (Convert.ToInt32(ViewState["Final_Counter"]) == 3) {
                //    string UserHasBeenBlocked = WebCommon.getGlobalResourceValue("UserHasBeenBlocked");

                //    SCT_UserBE oSCT_UserBE = new SCT_UserBE();
                //    SCT_UserBAL oSCT_UserBAL = new SCT_UserBAL();
                //    oSCT_UserBE.Action = "UpdateAccountStatus";
                //    oSCT_UserBE.AccountStatus = "Blocked";
                //    oSCT_UserBE.LoginID = txtUserName.Value.Trim();
                //    int? iResult = oSCT_UserBAL.UpdateAccountStatus(oSCT_UserBE);

                //    ClientScript.RegisterClientScriptBlock(this.GetType(), "Message", "alert('" + UserHasBeenBlocked + "');", true);
                //    return;
                //}
            }
        }
        catch (Exception ex)
        {
            LogUtility.SaveErrorLogEntry(ex);
        }
    }
    protected void lnkForgotPassword_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            SCT_UserBE oSCT_UserBE = new SCT_UserBE();
            SCT_UserBAL oSCT_UserBAL = new SCT_UserBAL();
            oSCT_UserBE.Action = "IsValidEmailId";
            oSCT_UserBE.EmailId = txtUserName.Value.Trim();
            int isEmailIdValid = Convert.ToInt32(oSCT_UserBAL.IsValidEmailIdBAL(oSCT_UserBE));

            if (!string.IsNullOrWhiteSpace(txtUserName.Value.Trim()) && isEmailIdValid > 0)
            {
                oSCT_UserBAL = new SCT_UserBAL();
                oSCT_UserBE.Action = "UpdateUserPassword";
                string Password = GeneratePassword();
                oSCT_UserBE.Password = Password;
                oSCT_UserBE.LoginID = txtUserName.Value.Trim();
                int? iResult = oSCT_UserBAL.UpdateAccountStatus(oSCT_UserBE);
                oSCT_UserBAL = null;
                if (iResult != 0)
                {
                    //TODO: send email containing new credintials.
                    SendCommunicationMail(Password, txtUserName.Value.Trim());

                    string NewPasswordSent = WebCommon.getGlobalResourceValue("NewPasswordSent");
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + NewPasswordSent + "')", true);
                    return;
                }
                else if (iResult == 0)
                {
                    //string ValidEmail = WebCommon.getGlobalResourceValue("ValidEmail");
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + ValidEmail + "')", true);
                    return;
                }
            }
            else
            {
                //string ValidEmail = WebCommon.getGlobalResourceValue("ValidEmail");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + ValidEmail + "')", true);
                return;

                //    string UserNameRequired = WebCommon.getGlobalResourceValue("UserNameRequired");
                //    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + UserNameRequired + "')", true);
                //    return;
            }
        }
    }
    #endregion

    #region Methods
    
    protected override void InitializeCulture()
    {
        //Session["CultureInfo"] = "en-GB";
        //Page.Culture =  "en-GB";
        //Page.UICulture = "en-GB";
    }
    private void GetCultureByLanguage(string sLanguage)
    {
        string sCulture = clsConstants.EnglishISO;
        switch (sLanguage.ToLower())
        {
            case "english":
                sCulture = clsConstants.EnglishISO;
                break;
            default:
                sCulture = clsConstants.EnglishISO;
                break;
        }
        Session["CultureInfo"] = sCulture;
    }
    private void SendCommunicationMail(string password, string Username)
    {
        sendCommunicationCommon osendCommunicationCommon = new sendCommunicationCommon();

        string htmlBody = getMailBody(password, Username);
        string mailSubjectText = "Forgot Password ";

        clsEmail oclsEmail = new clsEmail();
        oclsEmail.sendMail(txtUserName.Value.Trim(), htmlBody, mailSubjectText, Convert.ToString(ConfigurationManager.AppSettings["fromAddress"]), true);
        Page.UICulture = Convert.ToString(Session["CultureInfo"]);
    }
    private string getMailBody(string password, string Username)
    {
        // Get Default Language Id of User
        SCT_UserBE oSCT_UserBE = new SCT_UserBE();
        SCT_UserBAL oSCT_UserBAL = new SCT_UserBAL();
        oSCT_UserBE.Action = "CheckForExistance";
        oSCT_UserBE.EmailId = Username;
        DataSet dsLanguage = new DataSet();
        dsLanguage = oSCT_UserBAL.GetLanguageIdBAL(oSCT_UserBE);
        oSCT_UserBAL = null;
        string Language = string.Empty;
        if (dsLanguage.Tables[0].Rows.Count > 0)
        {
            Language = dsLanguage.Tables[0].Rows[0]["Language"].ToString();
        }


        string htmlBody = null;
        sendCommunicationCommon osendCommunicationCommon = new sendCommunicationCommon();
        string path = AppDomain.CurrentDomain.BaseDirectory.ToLower();
        string templatePath = @"/EmailTemplates/Generic/";
        templatePath = path + templatePath;

        //string LanguageFile = "ForgotPassword." + Language + ".htm";
        //if (!System.IO.File.Exists(templatePath + LanguageFile.ToLower()))
        //{
        //    LanguageFile = "ForgotPassword." + "english.htm";
        //}
        string LanguageFile = "ForgotPassword.english.htm";

        //#region Setting reason as per the language ...
        //switch (Language)
        //{
        //    case clsConstants.English:
        //        Page.UICulture = clsConstants.EnglishISO; // "en-US"; // English
        //        break;
        //    case clsConstants.French:
        //        Page.UICulture = clsConstants.FranceISO; // // France
        //        break;
        //    case clsConstants.German:
        //        Page.UICulture = clsConstants.GermanyISO; //"de"; // German
        //        break;
        //    case clsConstants.Dutch:
        //        Page.UICulture = clsConstants.NederlandISO; //"nl"; // Dutch
        //        break;
        //    case clsConstants.Spanish:
        //        Page.UICulture = clsConstants.SpainISO; //"es"; // Spanish
        //        break;
        //    case clsConstants.Italian:
        //        Page.UICulture = clsConstants.ItalyISO; //"it"; // Italy
        //        break;
        //    case clsConstants.Czech:
        //        Page.UICulture = clsConstants.CzechISO; //"cz"; // Czech
        //        break;
        //    default:
        //        Page.UICulture = clsConstants.EnglishISO; //"en-US"; // English
        //        break;
        //}

        //#endregion

        using (StreamReader sReader = new StreamReader(templatePath + LanguageFile.ToLower()))
        {
            htmlBody = sReader.ReadToEnd();
            // htmlBody = htmlBody.Replace("{logoInnerPath}", osendCommunicationCommon.getAbsolutePath());

            htmlBody = htmlBody.Replace("{ForgotPasswordText1}", WebCommon.getGlobalResourceValue("ForgotPasswordText1", Language));
            htmlBody = htmlBody.Replace("{passwordValue}", password);
            htmlBody = htmlBody.Replace("{Yoursfaithfully}", WebCommon.getGlobalResourceValue("Yoursfaithfully", Language));
            htmlBody = htmlBody.Replace("{OfficeDepots}", WebCommon.getGlobalResourceValue("OfficeDepots", Language));
        }
        return htmlBody;
    }
    #endregion
}
﻿using System;
using BusinessEntities.ModuleBE.Security;
using BusinessLogicLayer.ModuleBAL.Security;

public partial class ModuleUI_Security_SCT_TermsAndConditions : CommonPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        this.ifrmTermsAndCondition.Attributes["src"] = "../../ModuleUI/Security/TnCTemplates/SCT_TermsAndConditionEnglish.aspx";

        //if (Session["CultureInfo"] != null) {
        //    if (Session["CultureInfo"].ToString() == "fr") 
        //    {
        //        this.ifrmTermsAndCondition.Attributes["src"] = "../../ModuleUI/Security/TnCTemplates/SCT_TermsAndConditionFrench.aspx";
        //    }
        //    else if(Session["CultureInfo"].ToString() == "de")
        //    {
        //        this.ifrmTermsAndCondition.Attributes["src"] = "../../ModuleUI/Security/TnCTemplates/SCT_TermsAndConditionGerman.aspx";
        //    }
        //    else if (Session["CultureInfo"].ToString() == "nl")
        //    {
        //        this.ifrmTermsAndCondition.Attributes["src"] = "../../ModuleUI/Security/TnCTemplates/SCT_TermsAndConditionDutch.aspx";
        //    }
        //    else if (Session["CultureInfo"].ToString() == "es") 
        //    {
        //        this.ifrmTermsAndCondition.Attributes["src"] = "../../ModuleUI/Security/TnCTemplates/SCT_TermsAndConditionSpain.aspx";
        //    }
        //    else if (Session["CultureInfo"].ToString() == "it") {
        //        this.ifrmTermsAndCondition.Attributes["src"] = "../../ModuleUI/Security/TnCTemplates/SCT_TermsAndConditionItaly.aspx";
        //    }
        //    else if (Session["CultureInfo"].ToString() == "cz") {
        //        this.ifrmTermsAndCondition.Attributes["src"] = "../../ModuleUI/Security/TnCTemplates/SCT_TermsAndConditionEnglish.aspx";
        //    }
        //    else {
        //        this.ifrmTermsAndCondition.Attributes["src"] = "../../ModuleUI/Security/TnCTemplates/SCT_TermsAndConditionEnglish.aspx";
        //    }

        //}
        //else {
        //    this.ifrmTermsAndCondition.Attributes["src"] = "../../ModuleUI/Security/TnCTemplates/SCT_TermsAndConditionEnglish.aspx";
        //}

        
    }
    protected void btnAccept_Click(object sender, EventArgs e)
    {
        SCT_UserBE oSCT_UserBE = new SCT_UserBE();
        SCT_UserBAL oSCT_UserBAL = new SCT_UserBAL();
        oSCT_UserBE.Action = "UpdateAccountStatus";
        oSCT_UserBE.AccountStatus = "Active";
        oSCT_UserBE.UserID=Convert.ToInt32(Session["UserID"]);
        oSCT_UserBAL.UpdateAccountStatus(oSCT_UserBE);
        //Sprint 2 - update Loggin time
        oSCT_UserBE.Action = "UpdateLoggedInTime";
        oSCT_UserBAL.UpdateAccountStatus(oSCT_UserBE);
        //-----------------//
        oSCT_UserBAL = null;

        EncryptQueryString("../Dashboard/DefaultDashboard.aspx");
    }
    protected void btnDecline_Click(object sender, EventArgs e)
    {
        Session.Abandon();
        EncryptQueryString("Login.aspx");
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebUtilities;
using BusinessEntities.ModuleBE.Security;
using BusinessLogicLayer.ModuleBAL.Security;
using System.Data;
using Utilities;

public partial class ModuleUI_Security_SCT_VendorTemplateEdit : CommonPage
{
    protected string deleteMessage = WebCommon.getGlobalResourceValue("DeleteMessage");
    protected void Page_InIt(object sender, EventArgs e)
    {
        msVendor.VendorFillOnSearch = true;
        msVendor.VendorRemoveFromLeft = true;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        txtTemplate.Focus();
        if (!Page.IsPostBack)
        {
            if (GetQueryStringValue("VendorTemplateID") != null)
            {
                GetVendorTemplate();
                btnDelete.Visible = true;
            }
            else
            {
                btnDelete.Visible = false;    
            }           
        }
       
    }

    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        // msVendor.setVendorsOnPostBack();
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            if (string.IsNullOrEmpty(msVendor.SelectedVendorIDs))
            {
                string vendorRequired = WebCommon.getGlobalResourceValue("MustSelectVendorName");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + vendorRequired + "')", true);
                return;
            }
            SCT_TemplateBE oSCT_TemplateBE = new SCT_TemplateBE();
            SCT_TemplateBAL oSCT_TemplateBAL = new SCT_TemplateBAL();


            oSCT_TemplateBE.TemplateName = string.IsNullOrWhiteSpace(txtTemplate.Text.Trim()) ? string.Empty : txtTemplate.Text.Trim();
            oSCT_TemplateBE.CreatedBy = Session["UserName"].ToString();
            oSCT_TemplateBE.CreatedOn = DateTime.Now;
            oSCT_TemplateBE.SelectedVendorIds = msVendor.SelectedVendorIDs;

            if (GetQueryStringValue("VendorTemplateID") == null)
            {
                oSCT_TemplateBE.Action = "InsertVendorTemplate";
            }
            else
            {
                oSCT_TemplateBE.Action = "UpdateVendorTemplate";
                oSCT_TemplateBE.VendorTemplateID = Convert.ToInt32(GetQueryStringValue("VendorTemplateID").ToString());
            }

            DataSet ds = oSCT_TemplateBAL.SaveVendorTemplate(oSCT_TemplateBE);

            if (ds != null && ds.Tables.Count > 0)
            {
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    if (Convert.ToString( dr["TemplateNameAlreadyExist"]) == "TemplateNameAlreadyExist")
                    {
                        string TemplateNameAlreadyExist = WebCommon.getGlobalResourceValue("TemplateNameAlreadyExist");
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + TemplateNameAlreadyExist + "')", true);
                        return;
                    }
                }
            }

            oSCT_TemplateBAL = null;
            EncryptQueryString("SCT_VendorTemplateOverview.aspx");
        }
        catch (Exception ex)
        {
            LogUtility.SaveErrorLogEntry(ex);
        }
    }
    protected void btnBack_Click(object sender, EventArgs e)
    {

        EncryptQueryString("SCT_VendorTemplateOverview.aspx");
    }


    protected void btnDelete_Click(object sender, EventArgs e)
    {
        try
        {           
            SCT_TemplateBE oSCT_TemplateBE = new SCT_TemplateBE();
            SCT_TemplateBAL oSCT_TemplateBAL = new SCT_TemplateBAL();
            
            if (GetQueryStringValue("VendorTemplateID") != null)
            {
                oSCT_TemplateBE.Action = "DeleteVendorTemplate";
                oSCT_TemplateBE.VendorTemplateID = Convert.ToInt32(GetQueryStringValue("VendorTemplateID").ToString());              
            }

            
             int? result = oSCT_TemplateBAL.DeleteVendorTemplateByIdBAL(oSCT_TemplateBE);


            oSCT_TemplateBAL = null;
            EncryptQueryString("SCT_VendorTemplateOverview.aspx");
        }
        catch (Exception ex)
        {
            LogUtility.SaveErrorLogEntry(ex);
        }
    }

   

    #region Methods

    private void GetVendorTemplate()
    {
        SCT_TemplateBAL oSCT_TemplateBAL = new SCT_TemplateBAL();
        SCT_TemplateBE oSCT_TemplateBE = new SCT_TemplateBE();

        oSCT_TemplateBE.VendorTemplateID = Convert.ToInt32(GetQueryStringValue("VendorTemplateID"));
        oSCT_TemplateBE.Action = "GetVendorTemplateInfobyID";
        List<SCT_TemplateBE> lstVendorTemplate = oSCT_TemplateBAL.GetVendorTemplateById(oSCT_TemplateBE);

        ListBox lstSelectedVendor = (ListBox)msVendor.FindControl("lstRight");
        ListBox lstVendor = (ListBox)msVendor.FindControl("ucVendor").FindControl("lstLeft");

        if (lstVendorTemplate != null && lstVendorTemplate.Count > 0)
        {
            txtTemplate.Text = lstVendorTemplate[0].TemplateName;

            for (int iCount = 0; iCount < lstVendorTemplate.Count; iCount++)
            {
                lstSelectedVendor.Items.Add(new ListItem(lstVendorTemplate[iCount].Vendor.Vendor_Name, lstVendorTemplate[iCount].Vendor.VendorID.ToString()));
                // lstVendor.Items.Remove(new ListItem(lstVendorTemplate[iCount].Vendor.Vendor_No + " - " + lstVendorTemplate[iCount].Vendor.Vendor_Name, lstVendorTemplate[iCount].Vendor.VendorID.ToString()));
                msVendor.SelectedVendorIDs = lstVendorTemplate[iCount].Vendor.VendorID.ToString();
               
            }
        }      
    }

    protected void btnExport_Click(object sender, EventArgs e)
    {
        ExportVendorlstExcel();
    }

     protected void ExportVendorlstExcel()
     {
        SCT_TemplateBAL oSCT_TemplateBAL = new SCT_TemplateBAL();
        SCT_TemplateBE oSCT_TemplateBE = new SCT_TemplateBE();
          
        oSCT_TemplateBE.VendorTemplateID = Convert.ToInt32(GetQueryStringValue("VendorTemplateID"));
        oSCT_TemplateBE.Action = "GetVendorTemplateInfobyIDForExport";

        DataSet dt = oSCT_TemplateBAL.GetVendorTemplateByIdForExportBAL(oSCT_TemplateBE);
        ExportToExcelVendorList(dt.Tables[0], "Vendor_Template");
     }


     public void ExportToExcelVendorList(DataTable dtExport, string fileName)
     {
         HttpContext context = HttpContext.Current;
         string date = DateTime.Now.ToString("ddMMyyyy");
         string time = DateTime.Now.ToString("HH:mm");
         context.Response.ContentType = "text/vnd.ms-excel";
         context.Response.AddHeader("Content-Disposition", string.Format("attachment; filename={0}_{1}_{2}", fileName, date, time + ".xls"));
         if (dtExport.Rows.Count > 0)
         {

             for (int i = 0; i < dtExport.Columns.Count; i++)
             {
                 context.Response.Write(dtExport.Columns[i].ColumnName);
                 context.Response.Write("\t");
             }
         }

         context.Response.Write(Environment.NewLine);

         //Write data
         foreach (DataRow row in dtExport.Rows)
         {

             for (int i = 0; i < dtExport.Columns.Count; i++)
             {
                 string rowdata = string.Empty;
                 rowdata = Convert.ToString(row.ItemArray[i]) == "" ? "" : Convert.ToString(row.ItemArray[i]);
                 context.Response.Write(rowdata);
                 context.Response.Write("\t");
             }
             context.Response.Write(Environment.NewLine);
         }
         context.Response.End();
     }
       

    #endregion
}
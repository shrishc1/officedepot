﻿using BusinessEntities.ModuleBE.AdminFunctions;
using BusinessEntities.ModuleBE.Security;
using BusinessLogicLayer.ModuleBAL.AdminFunctions;
using BusinessLogicLayer.ModuleBAL.Security;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Utilities;
using WebUtilities;

public partial class SCT_UserEdit : CommonPage
{
    DataTable dt = new DataTable();
    protected string NewUserMessage = WebCommon.getGlobalResourceValue("NewUserMessage");
    protected string NewUserSitesMessage = WebCommon.getGlobalResourceValue("NewUserSitesMessage");
    protected string SelectDefaultSite = WebCommon.getGlobalResourceValue("SelectDefaultSite");
    protected string SelectAtleaseOneSite = WebCommon.getGlobalResourceValue("SelectAtleaseOneSite");
    protected string TemplateReq = WebCommon.getGlobalResourceValue("TemplateReq");
    private string UserExist = WebCommon.getGlobalResourceValue("UserExist");
    private string UserSiteNotExist = WebCommon.getGlobalResourceValue("UserSiteNotExist");
    private string UserNotCreated = WebCommon.getGlobalResourceValue("UserNotCreated");
    protected string deleteMessage = WebCommon.getGlobalResourceValue("deleteMessage");
    string sPortalLink = Convert.ToString(ConfigurationManager.AppSettings["Portallink"]) + @"/ModuleUI/Security/Login.aspx";

    public static string UserId = string.Empty;

    string path = AppDomain.CurrentDomain.BaseDirectory.ToLower();
    string templatePath = @"/EmailTemplates/Generic/";

    protected void Page_Init(object sender, EventArgs e)
    {
        ucCountry.CurrentPage = this;
    }
    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (GetQueryStringValue("ReadOnly") == "1")
            {
                pnlAccessingRights.Enabled = false;
                pnlAccessPermissions.Enabled = false;
                pnlCountrySpecific.Enabled = false;
                pnlPersonalDetails.Enabled = false;
                pnlSiteSettings.Enabled = false;
                btnBlockUser.Visible = false;
                btnDeleteUser.Visible = false;
                btnMoveLeftAllStockPlanner.Visible = false;
                btnMoveLeftStockPlanner.Visible = false;
                btnMoveRightAllStockPlanner.Visible = false;
                btnMoveRightSite.Visible = false;
                btnPreviewMasterTemplate.Visible = false;
                btnPreviewReportTemplate.Visible = false;
                btnResendApprovalMail.Visible = false;
                btnSave.Visible = false;
                btnSaveProceed.Visible = false;
                btnUnblockUser.Visible = false;
                btnProceed.Visible = true;
            }
            else
            {
                pnlAccessingRights.Enabled = true;
                pnlAccessPermissions.Enabled = true;
                pnlCountrySpecific.Enabled = true;
                pnlPersonalDetails.Enabled = true;
                pnlSiteSettings.Enabled = true;
                btnBlockUser.Visible = true;
                btnDeleteUser.Visible = true;
                btnMoveLeftAllStockPlanner.Visible = true;
                btnMoveLeftStockPlanner.Visible = true;
                btnMoveRightAllStockPlanner.Visible = true;
                btnMoveRightSite.Visible = true;
                btnPreviewMasterTemplate.Visible = true;
                btnPreviewReportTemplate.Visible = true;
                btnResendApprovalMail.Visible = true;
                btnSave.Visible = true;
                btnSaveProceed.Visible = true;
                btnUnblockUser.Visible = true;
                btnProceed.Visible = false;
                btnBack.Visible = true;

            }
            getUserDetail();
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            ucCountry.innerControlddlCountry.AutoPostBack = true;
            BindRole();
            BindLanguage();
            BindCountry();
        }
    }

    #region Events
    protected void btnSaveProceed_Click(object sender, EventArgs e)
    {
        SCT_UserBE oSCT_UserBE = new SCT_UserBE();
        SCT_UserBAL oSCT_UserBAL = new SCT_UserBAL();

        oSCT_UserBE.EmailId = txtUserIDEmail.Text.Trim();
        if (GetQueryStringValue("UserID") == null)
        {
            if (CheckForUserExistance(oSCT_UserBE))
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + UserExist + "');", true);
                return;
            }
            BindTemplateDropdown();
            BindSites();
            BindEmptyGrid();
        }
        else
        {
            SaveUser();
            GetUserSelectedSites();
            BindSitesAfterSelection();
        }
        if (GetQueryStringValue("ReadOnly") == "1")
        {
            btnSave.Visible = false;
            btnSaveProceed.Visible = false;
        }
        else
        {
            btnSave.Visible = true;
            btnSaveProceed.Visible = false;
        }
        mvUserSetting.ActiveViewIndex = 1;
    }

    private string SaveUser()
    {
        int? iResultUserId = 0;
        SCT_UserBE oSCT_UserBE = new SCT_UserBE();
        SCT_UserBAL oSCT_UserBAL = new SCT_UserBAL();
        oSCT_UserBE.Discrepancy = new BusinessEntities.ModuleBE.Discrepancy.LogDiscrepancy.DiscrepancyBE();
        oSCT_UserBE.oStockPlannerGroupings = new StockPlannerGroupingsBE();
        oSCT_UserBE.EmailId = txtUserIDEmail.Text.Trim();
        oSCT_UserBE.LoginID = txtUserIDEmail.Text.Trim();
        oSCT_UserBE.FirstName = txtFirstName.Text.Trim();
        oSCT_UserBE.Lastname = txtLastName.Text.Trim();
        oSCT_UserBE.PhoneNumber = txtTelephone.Text.Trim();
        oSCT_UserBE.FaxNumber = txtFax.Text.Trim();
        oSCT_UserBE.LanguageID = ddlLanguage.SelectedIndex > 0 ? Convert.ToInt32(ddlLanguage.SelectedItem.Value) : (int?)null;
        //change for the Role
        oSCT_UserBE.UserRoleID = ddlRole.SelectedIndex > 0 ? Convert.ToInt32(ddlRole.SelectedItem.Value) : (int?)null;
        oSCT_UserBE.IsDeliveryRefusalAuthorization = chkAuthorizeRefusal.Checked ? 1 : 0;
        oSCT_UserBE.IsActAsMediator = chkIsMediator.Checked ? 1 : 0;

        if (ddlRole.SelectedItem.Value == "6") // StockPlanner
        {
            oSCT_UserBE.Discrepancy.IsInitialDiscCommunicationToSP = chkReceiveDiscEmailAlert.Checked ? true : false;
        }

        oSCT_UserBE.JobTitle = txtJobTitleValue.Text;

        oSCT_UserBE.oStockPlannerGroupings = new StockPlannerGroupingsBE();
        if (ddlStockPlannerGroupings.SelectedIndex > 0)
            oSCT_UserBE.oStockPlannerGroupings.StockPlannerGroupingsID = Convert.ToInt32(ddlStockPlannerGroupings.SelectedItem.Value);

        if (uclstStockPlannerLeft.Items.Count > 0)
        {
            for (int i = 0; i < uclstStockPlannerLeft.Items.Count; i++)
            {
                oSCT_UserBE.SelectedStockPlannerIds += uclstStockPlannerLeft.Items[i].Value + ",";
            }
            oSCT_UserBE.SelectedStockPlannerIds = oSCT_UserBE.SelectedStockPlannerIds.Trim(',');
        }
        else
            oSCT_UserBE.SelectedStockPlannerIds = null;

        if (GetQueryStringValue("UserID") == null)
        {
            if (CheckForUserExistance(oSCT_UserBE))
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + UserExist + "');", true);
                return "-1";
            }
            oSCT_UserBE.IsActive = true;
            oSCT_UserBE.AccountStatus = "Registered Awaiting Approval";
            ViewState["AccountStatus"] = "Registered Awaiting Approval";
            oSCT_UserBE.Action = "AddUser";
            iResultUserId = oSCT_UserBAL.addEditUserBAL(oSCT_UserBE);
            ViewState["UserId"] = iResultUserId.ToString();

            if (iResultUserId <= 0)
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + UserNotCreated + "');", true);
                return "-1";
            }
        }
        else
        {
            oSCT_UserBE.UserID = Convert.ToInt32(GetQueryStringValue("UserID"));
            oSCT_UserBE.Action = "UpdateUser";
            oSCT_UserBAL.UpdateUserBAL(oSCT_UserBE);
        }

        UserId = GetQueryStringValue("UserID") == null ? ViewState["UserId"].ToString() : GetQueryStringValue("UserID").ToString();
        return UserId;
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        int iAllEmpty = 0;
        foreach (GridViewRow gv in grdSelectedSites.Rows)
        {
            if (grdSelectedSites.Rows.Count == 1 && ViewState["IsEmptyGrid"] != null && ViewState["IsEmptyGrid"].ToString() == "yes")
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + SelectAtleaseOneSite + "')", true);
                return;
            }

            DropDownList ddlTemplate = (DropDownList)gv.FindControl("ddlTemplates");
            RadioButton rbDefault = (RadioButton)gv.FindControl("rdoDefault");

            if (ddlTemplate.SelectedIndex == 0)
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + TemplateReq + "')", true);
                return;
            }

            if (!rbDefault.Checked)
            {
                iAllEmpty++;
            }
        }
        if (iAllEmpty > 0 && iAllEmpty == grdSelectedSites.Rows.Count)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + SelectDefaultSite + "')", true);
            return;
        }

        string res = string.Empty;
        res = SaveUser();

        if (res != "-1")
        {
            if (mvUserSetting.ActiveViewIndex == 1)
            {
                SaveSites();
            }
            else
            {
                EncryptQueryString("SCT_UsersOverview.aspx");
            }
        }
    }
    private void SaveSites()
    {
        int? iResultUserSite = 0;

        //Update User Templates and Account Status
        SCT_UserBE oSCT_UserBE = new SCT_UserBE();
        SCT_UserBAL oSCT_UserBAL = new SCT_UserBAL();
        oSCT_UserBE.Action = "UpdateUserTemplates";
        oSCT_UserBE.AccountStatus = ViewState["AccountStatus"].ToString() == "Active" ? "Active" : "Awaiting Activation";
        oSCT_UserBE.UserID = GetQueryStringValue("UserID") != null
            ? Convert.ToInt32(GetQueryStringValue("UserID").ToString())
            : Convert.ToInt32(ViewState["UserId"]);
        oSCT_UserBE.MasterTemplateID = ddlTemplateReport.SelectedIndex > 0 ? Convert.ToInt32(ddlTemplateReport.SelectedItem.Value) : (int?)null;
        oSCT_UserBE.ReportTemplateID = ddlTemplateReport.SelectedIndex > 0 ? Convert.ToInt32(ddlTemplateReport.SelectedItem.Value) : (int?)null;
        int? iResult = oSCT_UserBAL.UpdateUserTemplatesBAL(oSCT_UserBE);

        //Deleting rows from usersite in case of edit
        if (GetQueryStringValue("UserID") != null)
        {
            oSCT_UserBE.Action = "UpdateUserSites";
            oSCT_UserBE.UserID = Convert.ToInt32(GetQueryStringValue("UserID").ToString());

            iResultUserSite = oSCT_UserBAL.AddUserSitesBAL(oSCT_UserBE);
        }
        //Update Pasword for user
        oSCT_UserBE.Action = "UpdatePassword";
        oSCT_UserBE.Password = GeneratePassword();
        ViewState["password"] = oSCT_UserBE.Password;
        int? iResultPassword = oSCT_UserBAL.UpdateAccountStatus(oSCT_UserBE);

        foreach (GridViewRow gv in grdSelectedSites.Rows)
        {
            DropDownList ddlTemplate = (DropDownList)gv.FindControl("ddlTemplates");
            RadioButton rbDefault = (RadioButton)gv.FindControl("rdoDefault");
            HiddenField SelectedSiteId = (HiddenField)gv.FindControl("hdnSiteID");
            oSCT_UserBE.SiteId = Convert.ToInt32(SelectedSiteId.Value);
            oSCT_UserBE.IsDefaultSite = rbDefault.Checked;
            oSCT_UserBE.TemplateID = ddlTemplateReport.SelectedIndex > 0 ? Convert.ToInt32(ddlTemplateReport.SelectedItem.Value) : (int?)null;
            oSCT_UserBE.Action = "AddUserSites";
            oSCT_UserBE.UserID = GetQueryStringValue("UserID") != null
                ? Convert.ToInt32(GetQueryStringValue("UserID").ToString())
                : Convert.ToInt32(ViewState["UserId"]);

            iResultUserSite = oSCT_UserBAL.AddUserSitesBAL(oSCT_UserBE);

        }
        if (ViewState["AccountStatus"].ToString() == "Registered Awaiting Approval")
        {
            SendApprovalCommunicationMail(txtUserIDEmail.Text, ViewState["password"].ToString());
        }
        EncryptQueryString("SCT_UsersOverview.aspx");
    }
    protected void btnBack_Click(object sender, EventArgs e)
    {
        if (GetQueryStringValue("PreviousPage") == "SPNumberReports")
        {
            ScriptManager.RegisterClientScriptBlock(up1, Page.GetType(), "script", "window.close()", true);
        }
        else
        {
            EncryptQueryString("SCT_UsersOverview.aspx");
        }
    }
    protected void btnMoveLeftSite_Click(object sender, EventArgs e)
    {
        DataTable dt = (DataTable)ViewState["SelectedSite"];
        DataRow drInner = dt.NewRow();
        int SiteID = 0;
        string SiteDescription = string.Empty;

        foreach (GridViewRow gv in grdSelectedSites.Rows)
        {
            DropDownList ddlTemplate = (DropDownList)gv.FindControl("ddlTemplates");
            RadioButton rbDefault = (RadioButton)gv.FindControl("rdoDefault");
            HiddenField SelectedSiteId = (HiddenField)gv.FindControl("hdnSiteID");

            if (rbDefault.Checked)
            {
                SiteID = Convert.ToInt32(SelectedSiteId.Value);
            }
        }

        if (dt.Rows.Count > 1)
        {
            SiteDescription = dt.Select("SiteID = " + SiteID)[0]["SelectedSites"].ToString();
            dt.Rows.Remove(dt.Select("SiteID= " + SiteID)[0]);
            dt.AcceptChanges();

            dt.Rows[0]["IsDefaultSite"] = "True";

            grdSelectedSites.DataSource = dt;
            grdSelectedSites.DataBind();

            ListItem listItemCollection = new ListItem();
            listItemCollection.Value = SiteID.ToString();
            listItemCollection.Text = SiteDescription.ToString();
            uclstSiteLeft.Items.Add(listItemCollection);
        }
        else
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('User must have at least one default site.');", true);
        }
    }
    protected void btnMoveRightSite_Click(object sender, EventArgs e)
    {
        //Move selected site to Grid
        if (uclstSiteLeft.SelectedItem != null)
        {
            DataTable dtLocal = new DataTable();
            dtLocal.Columns.Add("SelectedSites");

            dtLocal.Columns.Add("Siteid");

            dtLocal.Columns.Add("TemplateID");

            dtLocal.Columns.Add("IsDefaultSite");

            if (ViewState["SelectedSite"] != null && GetQueryStringValue("UserID") == null)
            {
                dt = (DataTable)ViewState["SelectedSite"];
                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        DataRow drInner = dtLocal.NewRow();

                        drInner["SelectedSites"] = dr["SelectedSites"].ToString();

                        drInner["Siteid"] = dr["Siteid"].ToString();

                        drInner["TemplateID"] = null;

                        drInner["IsDefaultSite"] = false;

                        dtLocal.Rows.Add(drInner);
                    }
                }
            }
            else if (ViewState["SelectedSite"] != null && GetQueryStringValue("UserID") != null)
            {
                dt = (DataTable)ViewState["SelectedSite"];
                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        DataRow drInner = dtLocal.NewRow();

                        drInner["SelectedSites"] = dr["SelectedSites"].ToString();

                        drInner["Siteid"] = dr["Siteid"].ToString();

                        drInner["TemplateID"] = dr["TemplateID"].ToString();

                        drInner["IsDefaultSite"] = dr["IsDefaultSite"].ToString();

                        dtLocal.Rows.Add(drInner);
                    }
                }
            }


            DataRow drLocal = dtLocal.NewRow();
            drLocal["SelectedSites"] = uclstSiteLeft.SelectedItem.Text;

            drLocal["Siteid"] = uclstSiteLeft.SelectedItem.Value;
            drLocal["TemplateID"] = null;

            drLocal["IsDefaultSite"] = false;
            // added ono 26 mar 2013
            bool isTobeAdded = true;

            if (dtLocal.Rows.Count > 0)
            {
                foreach (DataRow dr in dtLocal.Rows)
                {
                    if (dr[0].ToString() == drLocal["SelectedSites"].ToString())
                    {
                        isTobeAdded = false;
                        break;
                    }
                }

                if (isTobeAdded)
                    dtLocal.Rows.Add(drLocal);
            }
            else
            {
                dtLocal.Rows.Add(drLocal);
            }
            ViewState["SelectedSite"] = dtLocal;

            grdSelectedSites.DataSource = dtLocal;
            grdSelectedSites.DataBind();

            uclstSiteLeft.Items.RemoveAt(uclstSiteLeft.SelectedIndex);

            ViewState["IsEmptyGrid"] = "No";
        }
    }

    protected void btnRemove_Click(object sender, CommandEventArgs e)
    {
        if (e.CommandName == "Remove")
        {
            if (e.CommandArgument != null)
            {
                DataTable dtLocal = new DataTable();
                dtLocal.Columns.Add("SelectedSites");

                dtLocal.Columns.Add("Siteid");

                dtLocal.Columns.Add("TemplateID");

                dtLocal.Columns.Add("IsDefaultSite");

                if (ViewState["SelectedSite"] != null)
                {
                    dt = (DataTable)ViewState["SelectedSite"];
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        foreach (DataRow dr in dt.Rows)
                        {
                            DataRow drInner = dtLocal.NewRow();

                            if (dr["Siteid"].ToString().Trim() != e.CommandArgument.ToString().Trim())
                            {
                                drInner["SelectedSites"] = dr["SelectedSites"].ToString();

                                drInner["Siteid"] = dr["Siteid"].ToString();

                                drInner["TemplateID"] = dr["TemplateID"].ToString();

                                drInner["IsDefaultSite"] = dr["IsDefaultSite"].ToString();

                                dtLocal.Rows.Add(drInner);

                            }
                            if (dr["Siteid"].ToString().Trim() == e.CommandArgument.ToString().Trim())
                            {
                                uclstSiteLeft.Items.Add(new ListItem(dr["SelectedSites"].ToString(), dr["Siteid"].ToString()));
                            }

                        }
                        if (dtLocal.Rows.Count == 0)
                        {
                            BindEmptyGrid();
                        }
                        else
                        {
                            ViewState["SelectedSite"] = dtLocal;

                            grdSelectedSites.DataSource = dtLocal;
                            grdSelectedSites.DataBind();
                        }
                    }
                }
            }
        }
    }

    protected void btnMoveRightAllStockPlanner_Click(object sender, EventArgs e)
    {
        if (uclstStockPlannerRight.Items.Count > 0)
            FillControls.MoveAllItems(uclstStockPlannerRight, uclstStockPlannerLeft);
    }

    protected void btnMoveRightStockPlanner_Click(object sender, EventArgs e)
    {
        if (uclstStockPlannerRight.SelectedItem != null)
        {
            FillControls.MoveOneItem(uclstStockPlannerRight, uclstStockPlannerLeft);
        }
    }

    protected void btnMoveLeftStockPlanner_Click(object sender, EventArgs e)
    {
        if (uclstStockPlannerLeft.SelectedItem != null)
        {
            FillControls.MoveOneItem(uclstStockPlannerLeft, uclstStockPlannerRight);
        }
    }

    protected void btnMoveLeftAllStockPlanner_Click(object sender, EventArgs e)
    {
        if (uclstStockPlannerLeft.Items.Count > 0)
        {
            FillControls.MoveAllItems(uclstStockPlannerLeft, uclstStockPlannerRight);
        }
    }

    protected void ddlRole_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlRole.SelectedItem.Value == "6")
        {
            pnlCountrySpecific.Style.Add("display", "block");
            BindStockPlanners();
            BindSelectedStockPlanners();
            trStockPlanner.Style.Add("display", "block");
            chkIsMediator.Visible = false;

            trsp.Style.Add("display", "");
            BindStockPlannerGroupings();

            lblReceiveDiscEmailAlertMsg.Visible = true;
            chkReceiveDiscEmailAlert.Visible = true;
        }
        else if (ddlRole.SelectedItem.Value == "9")
        {
            chkIsMediator.Visible = true;
            pnlCountrySpecific.Style.Add("display", "none");

            lblReceiveDiscEmailAlertMsg.Visible = false;
            chkReceiveDiscEmailAlert.Visible = false;
        }
        else
        {
            pnlCountrySpecific.Style.Add("display", "none");
            chkIsMediator.Visible = false;
            lblReceiveDiscEmailAlertMsg.Visible = false;
            chkReceiveDiscEmailAlert.Visible = false;
        }
    }

    protected void grdSelectedSites_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            if (GetQueryStringValue("UserID") != null)
            {
                RadioButton rbDefault = e.Row.FindControl("rdoDefault") as RadioButton;
                string sTemplateid = DataBinder.Eval(e.Row.DataItem, "TemplateID").ToString();
                string sIsDafaultSite = DataBinder.Eval(e.Row.DataItem, "IsDefaultSite").ToString();
                rbDefault.Checked = Convert.ToBoolean(sIsDafaultSite);
            }
        }
    }

    protected void btnBlockUser_Click(object sender, EventArgs e)
    {
        SCT_UserBE oSCT_UserBE = new SCT_UserBE();
        SCT_UserBAL oSCT_UserBAL = new SCT_UserBAL();
        oSCT_UserBE.Action = "UpdateAccountStatus";
        oSCT_UserBE.AccountStatus = "Blocked";
        oSCT_UserBE.UserID = Convert.ToInt32(GetQueryStringValue("UserID").ToString());
        int? iResult = oSCT_UserBAL.UpdateAccountStatus(oSCT_UserBE);
        if (iResult != 0)
        {
            EncryptQueryString("SCT_UsersOverview.aspx");
        }
    }

    protected void btnUnblockUser_Click(object sender, EventArgs e)
    {
        SCT_UserBE oSCT_UserBE = new SCT_UserBE();
        SCT_UserBAL oSCT_UserBAL = new SCT_UserBAL();
        oSCT_UserBE.Action = "UpdateAccountStatus";
        oSCT_UserBE.AccountStatus = "Active";
        string Password = GeneratePassword();
        oSCT_UserBE.Password = Password;
        oSCT_UserBE.UserID = Convert.ToInt32(GetQueryStringValue("UserID").ToString());
        int? iResult = oSCT_UserBAL.UpdateAccountStatus(oSCT_UserBE);
        if (iResult != 0)
        {
            SendUnblockCommunicationMail(txtUserIDEmail.Text.Trim(), Password);
            EncryptQueryString("SCT_UsersOverview.aspx");
        }
    }

    protected void btnDeleteUser_Click(object sender, EventArgs e)
    {
        SCT_UserBE oSCT_UserBE = new SCT_UserBE();
        SCT_UserBAL oSCT_UserBAL = new SCT_UserBAL();
        oSCT_UserBE.oStockPlannerGroupings = new StockPlannerGroupingsBE();
        oSCT_UserBE.Action = "DeleteUser";
        oSCT_UserBE.IsActive = false;
        oSCT_UserBE.UserID = Convert.ToInt32(GetQueryStringValue("UserID").ToString());
        int? iResult = oSCT_UserBAL.addEditUserBAL(oSCT_UserBE);
        if (iResult == 0)
        {
            EncryptQueryString("SCT_UsersOverview.aspx");
        }
    }

    protected void ddlCountry_SelectedIndexChanged(object sender, EventArgs e)
    {
        BindSitesAfterSelection();
    }


    #endregion

    #region Methods
    private void BindCountry()
    {
        MAS_CountryBE oMAS_CountryBE = new MAS_CountryBE();
        MAS_CountryBAL oMAS_CountryBAL = new MAS_CountryBAL();

        oMAS_CountryBE.Action = "ShowAll";
        oMAS_CountryBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
        oMAS_CountryBE.User.UserID = Convert.ToInt32(Session["UserID"]);
        List<MAS_CountryBE> lstCountry = oMAS_CountryBAL.GetCountryBAL(oMAS_CountryBE);
        if (lstCountry.Count > 0)
        {
            FillControls.FillDropDown(ref ddlCountry, lstCountry.ToList(), "CountryName", "CountryID");
        }
        if (Session["SiteCountryID"] != null)
        {
            ddlCountry.SelectedIndex = ddlCountry.Items.IndexOf(ddlCountry.Items.FindByValue(Session["SiteCountryID"].ToString()));
        }

    }
    private void BindLanguage()
    {
        SCT_UserBE oSCT_UserBE = new SCT_UserBE();
        SCT_UserBAL oSCT_UserBAL = new SCT_UserBAL();

        oSCT_UserBE.Action = "GetLanguage";
        List<SCT_UserBE> lstLanguage = oSCT_UserBAL.GetLanguagesBAL(oSCT_UserBE);
        if (lstLanguage.Count > 0)
        {
            FillControls.FillDropDown(ref ddlLanguage, lstLanguage, "Language", "LanguageID", "Select");
        }

    }
    private bool CheckForUserExistance(SCT_UserBE oSCT_UserBE)
    {
        bool IsUserExist = false;
        if (oSCT_UserBE != null && !string.IsNullOrEmpty(oSCT_UserBE.EmailId))
        {
            oSCT_UserBE.Action = "CheckForExistance";
            SCT_UserBAL oSCT_UserBAL = new SCT_UserBAL();
            IsUserExist = oSCT_UserBAL.CheckForUserExistance(oSCT_UserBE);
        }
        return IsUserExist;
    }
    private void BindRole()
    {
        SCT_TemplateBAL oSCT_TemplateBAL = new SCT_TemplateBAL();
        SCT_UserRoleBE oSCT_UserRoleBE = new SCT_UserRoleBE();
        oSCT_UserRoleBE.Action = "GetODUsers";

        List<SCT_UserRoleBE> lstRole = oSCT_TemplateBAL.GetUserRolesBAL(oSCT_UserRoleBE);
        if (lstRole.Count > 0)
        {
            FillControls.FillDropDown(ref ddlRole, lstRole, "RoleName", "UserRoleID", "Select");
        }
    }

    private bool CheckForSites()
    {
        SCT_UserBE ONewSCT_UserBE = new SCT_UserBE();
        SCT_UserBAL oSCT_UserBAL = new SCT_UserBAL();
        ONewSCT_UserBE.Action = "GetUserSites";

        ONewSCT_UserBE.UserID = Convert.ToInt32(GetQueryStringValue("UserID").ToString());

        DataTable dt = oSCT_UserBAL.GetUserSitesBAL(ONewSCT_UserBE);
        if (dt != null && dt.Rows.Count > 0)
            return true;
        else
            return false;
    }

    List<MAS_SiteBE> lstSite;
    private void BindSites()
    {
        MAS_SiteBE oMAS_SiteBE = new MAS_SiteBE();
        MAS_SiteBAL oMAS_SiteBAL = new MAS_SiteBAL();

        oMAS_SiteBE.Action = "ShowAll";
        oMAS_SiteBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
        oMAS_SiteBE.User.UserID = Convert.ToInt32(Session["UserID"]);
        oMAS_SiteBE.SiteCountryID = Convert.ToInt32(ddlCountry.SelectedItem.Value);
        lstSite = oMAS_SiteBAL.GetSiteBAL(oMAS_SiteBE);
        if (lstSite.Count > 0)
        {
            FillControls.FillListBox(ref uclstSiteLeft, lstSite, "SiteDescription", "SiteID");
        }
    }

    //call only in edit mode..displays only those sites which are not selected by user
    private void BindSitesAfterSelection()
    {
        MAS_SiteBE oMAS_SiteBE = new MAS_SiteBE();
        MAS_SiteBAL oMAS_SiteBAL = new MAS_SiteBAL();
        oMAS_SiteBE.Action = "GetSitesAfterSelection";
        oMAS_SiteBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
        oMAS_SiteBE.User.UserID = Convert.ToInt32(Session["UserID"]);
        oMAS_SiteBE.SiteCountryID = Convert.ToInt32(ddlCountry.SelectedItem.Value);
        //Its a Selected user id 
        oMAS_SiteBE.SiteMangerUserID = GetQueryStringValue("UserID") != null ? Convert.ToInt32(GetQueryStringValue("UserID").ToString()) : Convert.ToInt32(ViewState["UserId"]);
        List<MAS_SiteBE> lstSite = oMAS_SiteBAL.GetSelectedSiteBAL(oMAS_SiteBE);
        if (lstSite.Count > 0)
        {
            FillControls.FillListBox(ref uclstSiteLeft, lstSite, "SiteDescription", "SiteID");
        }
    }

    private void BindStockPlanners()
    {
        SCT_UserBE oSCT_UserBE = new SCT_UserBE();
        SCT_UserBAL oSCT_UserBAL = new SCT_UserBAL();
        oSCT_UserBE.Action = "GetStockPlanners";
        oSCT_UserBE.CountryId = Convert.ToInt32(ucCountry.innerControlddlCountry.SelectedItem.Value);
        List<SCT_UserBE> lstStockPlanners = oSCT_UserBAL.GetStockPlannerBAL(oSCT_UserBE);
        if (lstStockPlanners.Count > 0)
        {
            FillControls.FillListBox(ref uclstStockPlannerRight, lstStockPlanners, "StockPlannerCountry", "StockPlannerID");
        }
        else
        {
            FillControls.FillListBox(ref uclstStockPlannerRight, lstStockPlanners, "StockPlannerCountry", "StockPlannerID");
        }
    }

    private void BindSelectedStockPlanners()
    {
        int UserID = 0;

        if (!string.IsNullOrEmpty(Convert.ToString(GetQueryStringValue("UserID"))))
        {
            UserID = Convert.ToInt32(GetQueryStringValue("UserID").ToString());
        }

        //Get selected stock Planners (Bind Right Stock Planners List)
        SCT_UserBE oSCT_UserBE = new SCT_UserBE();
        SCT_UserBAL oSCT_UserBAL = new SCT_UserBAL();

        oSCT_UserBE.Action = "GetSelectedStockPlanners";
        oSCT_UserBE.UserID = UserID;

        List<SCT_UserBE> lstStockPlannersRight = oSCT_UserBAL.GetStockPlannerBAL(oSCT_UserBE);

        if (lstStockPlannersRight.Count > 0)
        {
            FillControls.FillListBox(ref uclstStockPlannerLeft, lstStockPlannersRight, "StockPlannerCountry", "StockPlannerID");
        }

        //Get stock Planners (Bind Left Stock Planners List)
        oSCT_UserBE.Action = "GetStockPlanners";
        oSCT_UserBE.UserID = UserID;
        oSCT_UserBE.CountryId = Convert.ToInt32(ucCountry.innerControlddlCountry.SelectedItem.Value);
        List<SCT_UserBE> lstStockPlannersLeft = oSCT_UserBAL.GetStockPlannerBAL(oSCT_UserBE);
        if (lstStockPlannersLeft.Count > 0)
            FillControls.FillListBox(ref uclstStockPlannerRight, lstStockPlannersLeft, "StockPlannerCountry", "StockPlannerID");
        else
        {
            uclstStockPlannerRight.Items.Clear();
        }
    }

    public override void CountrySelectedIndexChanged()
    {
        if (ddlRole.SelectedItem.Value == "6")
        {

            if (GetQueryStringValue("UserID") != null)
            {
                BindSelectedStockPlanners();
            }
            BindStockPlanners();
        }
    }

    private void BindTemplateDropdown()
    {
        SCT_TemplateBE oSCT_TemplateBE = new SCT_TemplateBE();
        oSCT_TemplateBE.ForRole = Convert.ToInt32(ddlRole.SelectedItem.Value);
        // populate the data in dropdown list rele Template
        SCT_TemplateBAL oSCT_TemplateBAL = new SCT_TemplateBAL();
        List<SCT_TemplateBE> lstTemplates = oSCT_TemplateBAL.FillRoleDropDown(oSCT_TemplateBE);
        if (lstTemplates.Count > 0)
        {
            FillControls.FillDropDown(ref ddlTemplateMaster, lstTemplates, "TemplateName", "TemplateID", "Select");
            FillControls.FillDropDown(ref ddlTemplateReport, lstTemplates, "TemplateName", "TemplateID", "Select");
        }
        else
        {
            ddlTemplateMaster.Items.Add(new ListItem("-- Select --", "0"));
            ddlTemplateReport.Items.Add(new ListItem("-- Select --", "0"));
        }
    }

    private void BindEmptyGrid()
    {
        DataTable dtEmpty = new DataTable();

        dtEmpty.Columns.Add("SelectedSites");
        dtEmpty.Columns.Add("Siteid");
        dtEmpty.Columns.Add("TemplateID");
        dtEmpty.Columns.Add("IsDefaultSite");

        DataRow drRow = dtEmpty.NewRow();
        drRow["SelectedSites"] = "No Selected Sites";
        drRow["Siteid"] = "";
        drRow["TemplateID"] = "";
        drRow["IsDefaultSite"] = false;


        if (ViewState["SelectedSite"] != null)
        {
            ViewState["SelectedSite"] = null;
        }

        dtEmpty.Rows.Add(drRow);
        grdSelectedSites.DataSource = dtEmpty;
        grdSelectedSites.DataBind();
        foreach (GridViewRow gv in grdSelectedSites.Rows)
        {
            ImageButton btnRemove = (ImageButton)gv.FindControl("btnRemove");
            btnRemove.Visible = false;

            ImageButton btnPreview = (ImageButton)gv.FindControl("btnPreviewTemplate");
            btnPreview.Visible = false;

            DropDownList ddlTemplate = (DropDownList)gv.FindControl("ddlTemplates");
            ddlTemplate.Visible = false;

            RadioButton rbDefault = (RadioButton)gv.FindControl("rdoDefault");
            rbDefault.Visible = false;
        }
        ViewState["IsEmptyGrid"] = "yes";
    }

    private void getUserDetail()
    {
        if (GetQueryStringValue("UserID") != null)
        {

            txtUserIDEmail.Attributes.Add("readonly", "readonly");

            SCT_UserBAL oSCT_UserBAL = new SCT_UserBAL();
            SCT_UserBE oSCT_UserBE = new SCT_UserBE();
            oSCT_UserBE.Action = "GetUserOverview";
            oSCT_UserBE.UserID = Convert.ToInt32(GetQueryStringValue("UserID").ToString());

            List<SCT_UserBE> lstUserDetail = oSCT_UserBAL.GetUsers(oSCT_UserBE);
            if (lstUserDetail != null && lstUserDetail.Count > 0)
            {
                txtUserIDEmail.Text = lstUserDetail[0].LoginID;
                txtFirstName.Text = lstUserDetail[0].FirstName;
                txtLastName.Text = lstUserDetail[0].Lastname;

                lblPasswordText.Text = lstUserDetail[0].Password;
                ViewState["password"] = lstUserDetail[0].Password;
                txtTelephone.Text = lstUserDetail[0].PhoneNumber;
                txtFax.Text = lstUserDetail[0].FaxNumber;
                txtJobTitleValue.Text = lstUserDetail[0].JobTitle;
                if (ddlLanguage.Items.FindByValue(lstUserDetail[0].LanguageID.ToString()) != null)
                    ddlLanguage.SelectedValue = lstUserDetail[0].LanguageID.ToString();

                if (ddlRole.Items.FindByValue(lstUserDetail[0].UserRoleID.ToString()) != null)
                    ddlRole.SelectedValue = lstUserDetail[0].UserRoleID.ToString();


                if (ddlRole.SelectedItem.Value == "6")
                {
                    pnlCountrySpecific.Style.Add("display", "block");
                    BindStockPlanners();
                    BindSelectedStockPlanners();
                    trStockPlanner.Style.Add("display", "block");
                    chkIsMediator.Visible = false;

                    trsp.Style.Add("display", "");
                    BindStockPlannerGroupings();

                    ddlStockPlannerGroupings.SelectedIndex = ddlStockPlannerGroupings.Items.IndexOf(ddlStockPlannerGroupings.Items.FindByValue(lstUserDetail[0].oStockPlannerGroupings.StockPlannerGroupingsID.ToString()));

                    lblReceiveDiscEmailAlertMsg.Visible = true;
                    chkReceiveDiscEmailAlert.Visible = true;

                }
                else if (ddlRole.SelectedItem.Value == "9")
                {
                    chkIsMediator.Visible = true;
                    pnlCountrySpecific.Style.Add("display", "none");
                    lblReceiveDiscEmailAlertMsg.Visible = false;
                    chkReceiveDiscEmailAlert.Visible = false;
                }
                else
                {
                    pnlCountrySpecific.Style.Add("display", "none");
                    chkIsMediator.Visible = false;
                    lblReceiveDiscEmailAlertMsg.Visible = false;
                    chkReceiveDiscEmailAlert.Visible = false;
                }

                chkAuthorizeRefusal.Checked = Convert.ToBoolean(lstUserDetail[0].IsDeliveryRefusalAuthorization);
                chkIsMediator.Checked = Convert.ToBoolean(lstUserDetail[0].IsActAsMediator);

                chkReceiveDiscEmailAlert.Checked = lstUserDetail[0].Discrepancy.IsInitialDiscCommunicationToSP;

                BindTemplateDropdown();
                //Master Templates Data
                if (ddlTemplateReport.Items.FindByValue(lstUserDetail[0].MasterTemplateID.ToString()) != null)
                    ddlTemplateReport.SelectedValue = lstUserDetail[0].MasterTemplateID.ToString();

                if (ddlTemplateReport.Items.FindByValue(lstUserDetail[0].ReportTemplateID.ToString()) != null)
                    ddlTemplateReport.SelectedValue = lstUserDetail[0].ReportTemplateID.ToString();

                trAccountStatus.Style.Add("Display", "");
                lblAccountStatusValue.Text = lstUserDetail[0].AccountStatus.ToString();
                ViewState["AccountStatus"] = lstUserDetail[0].AccountStatus.ToString();

                if (lstUserDetail[0].AccountStatus.ToLower() == "blocked")
                {
                    if (GetQueryStringValue("ReadOnly") == "1")
                    {
                        btnUnblockUser.Visible = false;
                        btnDeleteUser.Visible = false;
                    }
                    else
                    {
                        btnUnblockUser.Visible = true;
                        btnDeleteUser.Visible = true;
                    }

                }
                if (lstUserDetail[0].AccountStatus.ToLower() == "active")
                {
                    if (GetQueryStringValue("ReadOnly") == "1")
                    {
                        btnBlockUser.Visible = false;
                        btnDeleteUser.Visible = false;
                    }
                    else
                    {
                        btnBlockUser.Visible = true;
                        btnDeleteUser.Visible = true;
                    }
                }

                if (lstUserDetail[0].AccountStatus.ToLower() == "registered awaiting approval")
                {
                    if (GetQueryStringValue("ReadOnly") == "1")
                    {
                        btnDeleteUser.Visible = false;
                    }
                    else
                    {
                        btnDeleteUser.Visible = true;
                    }
                }

                if (lstUserDetail[0].AccountStatus.ToLower() == "awaiting activation" || lstUserDetail[0].AccountStatus.ToLower() == "active")
                {
                    if (GetQueryStringValue("ReadOnly") == "1")
                    {
                        btnResendApprovalMail.Visible = false;
                        btnDeleteUser.Visible = false;
                    }
                    else
                    {
                        btnResendApprovalMail.Visible = true;
                        btnDeleteUser.Visible = true;
                    }
                }
            }
        }
    }
    private void BindStockPlannerGroupings()
    {
        StockPlannerGroupingsBAL oStockPlannerGroupingsBAL = new StockPlannerGroupingsBAL();
        StockPlannerGroupingsBE oNewStockPlannerGroupingsBE = new StockPlannerGroupingsBE();
        oNewStockPlannerGroupingsBE.Action = "ShowAllSPGroupings";
        List<StockPlannerGroupingsBE> oStockPlannerGroupingsBEList = oStockPlannerGroupingsBAL.GetStockPlannerGroupingsBAL(oNewStockPlannerGroupingsBE);

        if (oStockPlannerGroupingsBEList != null && oStockPlannerGroupingsBEList.Count > 0)
        {
            FillControls.FillDropDown(ref ddlStockPlannerGroupings, oStockPlannerGroupingsBEList, "StockPlannerGroupings", "StockPlannerGroupingsID", "Select");
        }
    }
    private void GetUserSelectedSites()
    {
        SCT_UserBAL oSCT_UserBAL = new SCT_UserBAL();
        SCT_UserBE oSCT_UserBE = new SCT_UserBE();
        oSCT_UserBE.Action = "GetUserSites";
        oSCT_UserBE.UserID = Convert.ToInt32(GetQueryStringValue("UserID").ToString());

        DataTable dt = oSCT_UserBAL.GetUserSitesBAL(oSCT_UserBE);
        if (dt.Rows.Count > 0)
        {
            grdSelectedSites.DataSource = dt;
            grdSelectedSites.DataBind();

            ViewState["SelectedSite"] = dt;

        }
        else
        {
            BindEmptyGrid();
        }
    }

    #endregion

    #region mail

    private void SendApprovalCommunicationMail(string EmailID, string Password)
    {
        sendCommunicationCommon osendCommunicationCommon = new sendCommunicationCommon();
        string htmlBody = getApprovalMailBody(EmailID, Password);
        string mailSubjectText = "Account Activated";
        clsEmail oclsEmail = new clsEmail();
        oclsEmail.sendMail(EmailID, htmlBody, mailSubjectText, Convert.ToString(ConfigurationManager.AppSettings["fromAddress"]), true);
        Page.UICulture = Convert.ToString(Session["CultureInfo"]);
    }

    private void SendUnblockCommunicationMail(string EmailID, string password)
    {
        sendCommunicationCommon osendCommunicationCommon = new sendCommunicationCommon();

        string htmlBody = getUnblockMailBody(password);
        string mailSubjectText = "Account Unblocked";

        clsEmail oclsEmail = new clsEmail();
        oclsEmail.sendMail(EmailID, htmlBody, mailSubjectText, Convert.ToString(ConfigurationManager.AppSettings["fromAddress"]), true);
        Page.UICulture = Convert.ToString(Session["CultureInfo"]);
    }

    private string getApprovalMailBody(string EmailID, string password)
    {
        string htmlBody = null;
        string Language = ddlLanguage.SelectedItem.Text;
        sendCommunicationCommon osendCommunicationCommon = new sendCommunicationCommon();

        templatePath = path + templatePath;

        string LanguageFile = "Approval.english.htm";

        #region Setting reason as per the language ...
        switch (Language)
        {
            case clsConstants.English:
                Page.UICulture = clsConstants.EnglishISO; // "en-US"; // English
                break;
            case clsConstants.French:
                Page.UICulture = clsConstants.FranceISO; // // France
                break;
            case clsConstants.German:
                Page.UICulture = clsConstants.GermanyISO; //"de"; // German
                break;
            case clsConstants.Dutch:
                Page.UICulture = clsConstants.NederlandISO; //"nl"; // Dutch
                break;
            case clsConstants.Spanish:
                Page.UICulture = clsConstants.SpainISO; //"es"; // Spanish
                break;
            case clsConstants.Italian:
                Page.UICulture = clsConstants.ItalyISO; //"it"; // Italy
                break;
            case clsConstants.Czech:
                Page.UICulture = clsConstants.CzechISO; //"cz"; // Czech
                break;
            default:
                Page.UICulture = clsConstants.EnglishISO; //"en-US"; // English
                break;
        }

        #endregion

        using (StreamReader sReader = new StreamReader(templatePath + LanguageFile.ToLower()))
        {
            htmlBody = sReader.ReadToEnd();

            htmlBody = htmlBody.Replace("{Congratulations}", WebCommon.getGlobalResourceValue("Congratulations"));
            htmlBody = htmlBody.Replace("{ApprovalText1}", WebCommon.getGlobalResourceValue("ApprovalText1"));

            htmlBody = htmlBody.Replace("{ClickHereValue}", "<a href='" + sPortalLink + "'>" + sPortalLink + " </a>");

            htmlBody = htmlBody.Replace("{UserName}", WebCommon.getGlobalResourceValue("UserName"));
            htmlBody = htmlBody.Replace("{usernameValue}", EmailID);

            htmlBody = htmlBody.Replace("{Password}", WebCommon.getGlobalResourceValue("Password"));
            htmlBody = htmlBody.Replace("{passwordValue}", password);

            htmlBody = htmlBody.Replace("{Yoursfaithfully}", WebCommon.getGlobalResourceValue("Yoursfaithfully"));
            htmlBody = htmlBody.Replace("{OfficeDepots}", WebCommon.getGlobalResourceValue("OfficeDepots"));
        }
        return htmlBody;
    }

    private string getUnblockMailBody(string password)
    {
        string htmlBody = null;
        string Language = ddlLanguage.SelectedItem.Text;
        sendCommunicationCommon osendCommunicationCommon = new sendCommunicationCommon();

        templatePath = path + templatePath;

        string LanguageFile = "UnblockUserAccount.english.htm";

        #region Setting reason as per the language ...
        switch (Language)
        {
            case clsConstants.English:
                Page.UICulture = clsConstants.EnglishISO; // "en-US"; // English
                break;
            case clsConstants.French:
                Page.UICulture = clsConstants.FranceISO; // // France
                break;
            case clsConstants.German:
                Page.UICulture = clsConstants.GermanyISO; //"de"; // German
                break;
            case clsConstants.Dutch:
                Page.UICulture = clsConstants.NederlandISO; //"nl"; // Dutch
                break;
            case clsConstants.Spanish:
                Page.UICulture = clsConstants.SpainISO; //"es"; // Spanish
                break;
            case clsConstants.Italian:
                Page.UICulture = clsConstants.ItalyISO; //"it"; // Italy
                break;
            case clsConstants.Czech:
                Page.UICulture = clsConstants.CzechISO; //"cz"; // Czech
                break;
            default:
                Page.UICulture = clsConstants.EnglishISO; //"en-US"; // English
                break;
        }

        #endregion

        using (StreamReader sReader = new StreamReader(templatePath + LanguageFile.ToLower()))
        {
            htmlBody = sReader.ReadToEnd();
            htmlBody = htmlBody.Replace("{Congratulations}", WebCommon.getGlobalResourceValue("Congratulations"));
            htmlBody = htmlBody.Replace("{UnblockUserAccText1}", WebCommon.getGlobalResourceValue("UnblockUserAccText1"));
            htmlBody = htmlBody.Replace("{Yoursfaithfully}", WebCommon.getGlobalResourceValue("Yoursfaithfully"));
            htmlBody = htmlBody.Replace("{OfficeDepots}", WebCommon.getGlobalResourceValue("OfficeDepots"));
        }
        return htmlBody;
    }

    #endregion

    protected void btnPreviewMasterTemplate_Click(object sender, ImageClickEventArgs e)
    {
        EncryptQueryString("~/ModuleUI/Security/SCT_RoleTemplateEdit.aspx?UserID=" + UserId + "&TemplateID=" + ddlTemplateReport.SelectedItem.Value);

    }
    protected void btnPreviewReportTemplate_Click(object sender, ImageClickEventArgs e)
    {
        EncryptQueryString("~/ModuleUI/Security/SCT_RoleTemplateEdit.aspx?UserID=" + UserId + "&TemplateID=" + ddlTemplateReport.SelectedItem.Value);
    }
    protected void grdSelectedSites_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        GridViewRow row = (GridViewRow)(((ImageButton)e.CommandSource).NamingContainer);
        DropDownList drpTemplate = (DropDownList)row.FindControl("ddlTemplates");
        EncryptQueryString("~/ModuleUI/Security/SCT_RoleTemplateEdit.aspx?UserID=" + UserId + "&TemplateID=" + drpTemplate.SelectedItem.Value);
    }
    protected void btnResendApprovalMail_Click(object sender, EventArgs e)
    {
        SCT_UserBE oSCT_UserBE = new SCT_UserBE();
        SCT_UserBAL oSCT_UserBAL = new SCT_UserBAL();
        oSCT_UserBE.Action = "UpdateUserPassword";
        oSCT_UserBE.Password = GeneratePassword();
        oSCT_UserBE.LoginID = txtUserIDEmail.Text.Trim();
        ViewState["newpassword"] = oSCT_UserBE.Password;
        oSCT_UserBAL.UpdateAccountStatus(oSCT_UserBE);
        SendApprovalCommunicationMail(txtUserIDEmail.Text, ViewState["newpassword"].ToString());
        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('Approval mail has been resend.');", true);
    }
    protected void ddlTemplates__SelectedIndexChanged(object sender, EventArgs e)
    {
        DataTable dtLocal = new DataTable();
        dtLocal.Columns.Add("SelectedSites");
        dtLocal.Columns.Add("Siteid");
        dtLocal.Columns.Add("TemplateID");
        dtLocal.Columns.Add("IsDefaultSite");

        foreach (GridViewRow gv in grdSelectedSites.Rows)
        {
            DropDownList ddlTemplate = (DropDownList)gv.FindControl("ddlTemplates");
            RadioButton rbDefault = (RadioButton)gv.FindControl("rdoDefault");
            HiddenField SelectedSiteId = (HiddenField)gv.FindControl("hdnSiteID");
            Label lblSite = (Label)gv.FindControl("lblSites");
            DataRow drLocal = dtLocal.NewRow();
            drLocal["SelectedSites"] = lblSite.Text;
            drLocal["Siteid"] = SelectedSiteId.Value;
            drLocal["TemplateID"] = ddlTemplate.SelectedIndex > 0 ? Convert.ToInt32(ddlTemplate.SelectedItem.Value) : (int?)null;
            drLocal["IsDefaultSite"] = rbDefault.Checked;
            dtLocal.Rows.Add(drLocal);
        }
        ViewState["SelectedSite"] = dtLocal;
    }
    protected void btnProceed_Click(object sender, EventArgs e)
    {
        if (ViewState["Click"] == null)
        {
            btnSaveProceed_Click(sender, e);
            btnBack.Visible = false;
            ViewState["Click"] = 1;
        }
        else
        {
            EncryptQueryString("SCT_UsersOverview.aspx");
        }
    }
}
﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    AutoEventWireup="true" CodeFile="DefaultDashboard.aspx.cs" Inherits="ModuleUI_Dashboard_DefaultDashboard" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link href="../../Css/tab.css" rel="stylesheet" type="text/css" />
    <link href="../../Css/jScrollPane.css" rel="stylesheet" type="text/css" />
    <link href="../../Css/jquery.hrzAccordion.defaults.css" rel="stylesheet" type="text/css" />
    <script src="../../Scripts/jquery.cycle.all.2.72.js" type="text/javascript" language="javascript"></script>
    <script src="../../Scripts/banner.js" type="text/javascript" language="javascript"></script>
    <script type="text/javascript" src="../../Scripts/jquery.mousewheel.js"></script>
    <script type="text/javascript" src="../../Scripts/jScrollPane.js"></script>
    <script type="text/javascript" src="../../Scripts/common.js"></script>
    <script type="text/javascript" src="../../Scripts/jquery-ui-personalized-1.js"></script>
    <script src="../../Scripts/tab-refresh.js" type="text/javascript"></script>
    <script type="text/javascript" src="../../Scripts/jquery.hrzAccordion.js"></script>
    <script type="text/javascript" src="../../Scripts/jquery.hrzAccordion.examples.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('#tab-me').tabs();
            $('.scroll-pane').jScrollPane();
            // this initialises the demo scollpanes on the page.
            $('#pane1').jScrollPane({ showArrows: true, scrollbarWidth: 3 });
            $('#pane2').jScrollPane({ showArrows: true, scrollbarWidth: 3 });
            $('#pane3').jScrollPane({ showArrows: true, scrollbarWidth: 3 });
            $('#pane4').jScrollPane({ showArrows: true, scrollbarWidth: 3 });
            // $('#pane5').jScrollPane({ showArrows: true, scrollbarWidth: 3 });
           $('#pane7, #<%=paneUK.ClientID%>, #<%=paneGermany.ClientID%>, #<%=paneItaly.ClientID%>').jScrollPane({ showArrows: true, scrollbarWidth: 3 });
        });

        $(document).ready(function () {
            $('link[href="../../Css/ui-lightness/jquery-ui-1.8.5.custom.css"]').remove();
            $('li').each(function () {
                if ($(this).html() == '') {
                    $(this).remove();
                }
            });
        });
        //        $(document).ready(function () {
        //            var homepage = '<%= WebUtilities.WebCommon.getGlobalResourceValue("homepage")%>';
        //            $('#lblModuleText').text(homepage);
        //        });
        function rotateBanners(elem) {
            var active = $(elem + " img.active");
            var next = active.next();
            if (next.length == 0)
                next = $(elem + " img:first");
            active.removeClass("active").fadeOut(200);
            next.addClass("active").fadeIn(200);
        }

        function prepareRotator(elem) {
            $(elem + " img").fadeOut(0);
            $(elem + " img:first").fadeIn(0).addClass("active");
        }

        function startRotator(elem) {
            prepareRotator(elem);
            setInterval("rotateBanners('" + elem + "')", 2500);
        }

    </script>
    <script type="text/javascript">
        $(window).load(function () {
            startRotator(".flash-banner");
        })
    </script>

    <style type="text/css">
        .ui-tabs-hide {
            position: absolute;
            left: -10000px;
            display: block;
        }
    </style>

    <style type="text/css">  
        .flash-banner img { position: absolute; }
        body {  
            margin: 0;  
            background: #e6e6e6;  
        }  
        .showSlide {  
            display: none  
        }  
            .showSlide img {  
                width: 100%;  
            }  
        .slidercontainer {  
            max-width: 1000px;  
            position: relative;  
            margin: auto;  
        }  
        .left, .right {  
            cursor: pointer;  
            position: absolute;  
            top: 50%;  
            width: auto;  
            padding: 16px;  
            margin-top: -22px;  
            color: white;  
            font-weight: bold;  
            font-size: 18px;  
            transition: 0.6s ease;  
            border-radius: 0 3px 3px 0;  
        }  
        .right {  
            right: 0;  
            border-radius: 3px 0 0 3px;  
        }  
            .left:hover, .right:hover {  
                background-color: rgba(115, 115, 115, 0.8);  
            }  
        .content {  
            color: #eff5d4;  
            font-size: 30px;  
            padding: 8px 12px;  
            position: absolute;  
            top: 10px;  
            width: 100%;  
            text-align: center;  
        }  
        .active {  
            background-color: #717171;  
        }  
        /* Fading animation */  
        .fade {  
            -webkit-animation-name: fade;  
            -webkit-animation-duration: 1.5s;  
            animation-name: fade;  
            animation-duration: 1.5s;  
        }  
        @-webkit-keyframes fade {  
            from {  
                opacity: .4  
            }  
            to {  
                opacity: 1  
            }  
        }  
  
        @keyframes fade {  
            from {  
                opacity: .4  
            }  
            to {  
                opacity: 1  
            }  
        }  
    </style>  

    <div>
        <!-- start content section -->
        <div>
           <!-- slider section -->
            <div class="flash-banner" style="width:992px;height:182px;margin-top: -5px;margin-left: 5px;">             
 
	        <img src="/Images/banner1.jpg" alt="b1" /> 
    
	        <img src="/Images/banner2.jpg" alt="b2"/> 
    
	        <img src="/Images/banner3.jpg" alt="b3"/> 
    
	        <img src="/Images/banner4.jpg" alt="b4"/> 
    
	        <img src="/Images/banner5.jpg" alt="b5"/> 
    
	        <img src="/Images/banner6.jpg" alt="b6"/> 
    
	        <img src="/Images/banner7.jpg" alt="b7"/>   
     

               <%--  <object width="992" style=" position: relative;z-index: 1;" height="182" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,29,0"
                    classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000">
                   
                    <param value="../../Images/homepage-banner.swf" name="movie">
                    <param value="high" name="quality">
                    <param value="transparent" name="wmode">
                    <embed width="992" height="182" wmode="transparent" type="application/x-shockwave-flash"
                        pluginspage="http://www.macromedia.com/go/getflashplayer" quality="high" src="../../Images/homepage-banner.swf">
                </object>--%>
            </div>
           <!-- slider section -->

            <div class="top-content">
                <!-- start about us -->
                <div class="contenttab">
                    <ul id="tab-me">
                        <li id="firstcurrent" class="first"><a href="#tab1" style="color: #BA1311; margin-left: 0px; padding-left: 18px;">
                            <table width="100%" cellspacing="0" cellpadding="0" border="0">
                                <tr>
                                    <td height="35">
                                        <cc1:ucLabel ID="lblVipNews" runat="server"></cc1:ucLabel>
                                    </td>
                                </tr>
                            </table>
                        </a></li>
                        <li><a href="#tab2" class="first2">
                            <table width="100%" cellspacing="0" cellpadding="0" border="0">
                                <tr>
                                    <td height="35">
                                        <cc1:ucLabel ID="lblAboutUs" runat="server"></cc1:ucLabel>
                                    </td>
                                </tr>
                            </table>
                        </a></li>
                        <li><a href="#tab3" class="first3">
                            <table width="100%" cellspacing="0" cellpadding="0" border="0">
                                <tr>
                                    <td height="35">
                                        <cc1:ucLabel ID="lblFeedback" runat="server"></cc1:ucLabel>
                                    </td>
                                </tr>
                            </table>
                        </a></li>
                        <li class="last"><a href="#tab4" class="first4">
                            <table width="100%" cellspacing="0" cellpadding="0" border="0">
                                <tr>
                                    <td height="35">
                                        <cc1:ucLabel ID="lblSupplyGuideline" runat="server"></cc1:ucLabel>
                                    </td>
                                </tr>
                            </table>
                        </a></li>
                    </ul>
                    <div class="clear">
                    </div>
                    <div id="tab1">
                        <div class="scroll-pane" id="pane1">
                            <%= WebUtilities.WebCommon.getGlobalResourceValue("DashBoard_VIPNews")%>
                        </div>
                    </div>
                    <div id="tab2">
                        <div class="scroll-pane" id="pane2">
                            <%= WebUtilities.WebCommon.getGlobalResourceValue("DashBoard_AboutUS") %>
                        </div>
                    </div>
                    <div id="tab3">
                        <div class="scroll-pane" id="pane3">
                            <%= WebUtilities.WebCommon.getGlobalResourceValue("DashBoard_FeedBack")%>
                        </div>
                    </div>
                    <div id="tab4">
                        <div id="pane4" class="scroll-pane">
                            <%= WebUtilities.WebCommon.getGlobalResourceValue("DashBoard_SupplierGuidelines")%>
                        </div>
                    </div>
                </div>
                <!-- end about us -->
                <!-- start your dashboard -->
                <div class="your-dashboard">
                    <ul class="dashboard-tab">
                        <li><a href="#">
                            <table width="100%" cellspacing="0" cellpadding="0" border="0">
                                <tr>
                                    <td height="35">
                                        <cc1:ucLabel ID="lblYourDashboard" runat="server"></cc1:ucLabel>
                                    </td>
                                </tr>
                            </table>
                        </a></li>
                    </ul>
                    <div id="pane5">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="dashbaord-list">
                            <tr>
                                <td>
                                    <cc1:ucLabel ID="lblPersonalDashboardDetail" ForeColor="Red" Font-Bold="true" runat="server"
                                        Text="Please find noted below, details and links to your personal dashboard."></cc1:ucLabel>
                                </td>
                            </tr>
                        </table>
                        <div id="fvCountDiscrepancy" runat="server" class="dashbaord-list">
                            <ul class="inner-tab-list">
                                <li>
                                    <cc1:ucLabel ID="lblEditBooking" runat="server" Text="If you require to make, edit, view or delete a delivery booking " />
                                    <asp:LinkButton ID="lnkClickHere" runat="server" Text=" Click here" OnClick="lnkClickHere_Click"> </asp:LinkButton>
                                </li>
                                <li>
                                    <cc1:ucLabel ID="lblDeliverySchedule" runat="server" Text="To view today's delivery schedule" />
                                </li>
                                <li>
                                    <cc1:ucLabel ID="lblFutureDateBooking" runat="server" Text="To view future dates and bookings please click here." />
                                </li>
                                <li>
                                    <cc1:ucLabel ID="lblprovisionalbookingscount" runat="server" Visible="false" />
                                    <asp:LinkButton ID="lnkClickHere_2" runat="server" Text=" Click here" OnClick="lnkClickHere_2_Click" Visible="false"> </asp:LinkButton>
                                </li>
                                <li>
                                    <cc1:ucLabel ID="lblUnschedulePONew" runat="server" Text="To view unscheduled/order Purchase Orders click here" />
                                </li>
                                <li>
                                    <cc1:ucLabel ID="lblOpenDiscrepancyCountNew" runat="server" Text="You currently have open Discrepancies." />
                                </li>
                                <li>
                                    <cc1:ucLabel ID="lblSearchDiscrepancy" runat="server" Text="If you require to search for a Discrepancy, please  click here" />
                                </li>
                                <li>
                                    <cc1:ucLabel ID="lblRaiseDiscrepancy" runat="server" Text="If you require to raise a Discrepancy, please select  Discrepancy option in the menu bar." />
                                </li>
                                <li>
                                    <cc1:ucLabel ID="lblOtifScorecard" runat="server" Text="Click here to view your current On Time in Full (OTIF) delivery performance report" />
                                </li>
                                <li>
                                    <cc1:ucLabel ID="lblPerformanceScoreCard" runat="server" Text="Click here to view your current performance Score Card" />
                                </li>
                                <li>
                                    <%-- <table>
                                        <tr>
                                            <td>--%>
                                    <cc1:ucLabel ID="lblRefusedDeliveries_1" runat="server" /><cc1:ucLinkButton ID="lnkClickHere_1" runat="server" OnClick="lnkClickHere_1_Click"></cc1:ucLinkButton><cc1:ucLabel ID="lblRefusedDeliveriesToView" runat="server" />
                                    <%-- </td>
                                        </tr>
                                    </table> --%>                                   
                                </li>
                                <li>
                                    <cc1:ucLabel ID="lblInvBOPenaltyReviewNew" runat="server" /><cc1:ucLinkButton ID="lnkClickHere_Inv" runat="server" OnClick="lnkClickHereINV_Click"></cc1:ucLinkButton><cc1:ucLabel ID="lblToReview_Inv" runat="server" />
                                </li>
                                <li>
                                    <cc1:ucLabel ID="lblVendorBOPenaltyReview" runat="server" /><cc1:ucLinkButton ID="lnkClickHere_Ven" runat="server" OnClick="lnkClickHereVen_Click"></cc1:ucLinkButton><cc1:ucLabel ID="lblToReview_Ven" runat="server" />
                                </li>
                                <li>
                                    <cc1:ucLabel ID="lblDisputeBOPenaltyReview" runat="server" /><cc1:ucLinkButton ID="lnkClickHere_Dis" runat="server" OnClick="lnkClickHereDispute_Click"></cc1:ucLinkButton><cc1:ucLabel ID="lblToReview_Dis" runat="server" />
                                </li>
                            </ul>
                        </div>
                        <div id="fvAdminDashboard" runat="server" class="dashbaord-list">
                            <ul class="inner-tab-list">
                                <li>
                                    <cc1:ucLabel ID="lblFailedCommunications" runat="server" /></li>
                                <li>
                                    <cc1:ucLabel ID="lblNewLogInRequests" runat="server" />
                                </li>
                                <li>
                                    <cc1:ucLabel ID="lblVendorsMissingDetails" runat="server" /></li>
                                <li>
                                    <cc1:ucLabel ID="lblFailedUploaded" runat="server" Text="There are XX  files which have failed to be uploaded click here to action"></cc1:ucLabel></li>
                            </ul>
                        </div>

                        <div id="fvManager" runat="server" class="dashbaord-list" visible="false">
                            <ul class="inner-tab-list">
                                <li>
                                    <cc1:ucLabel ID="lblViewSPOTIFReport" runat="server" /></li>
                                <li>
                                    <cc1:ucLabel ID="lblViewVendorScorecard" runat="server" />
                                </li>
                                <li>
                                    <cc1:ucLabel ID="lblViewStockOverview" runat="server" /></li>
                                <li>
                                    <cc1:ucLabel ID="lblViewSchedulingDetail" runat="server"></cc1:ucLabel></li>
                            </ul>
                        </div>

                    </div>
                </div>
                <!-- end your dashboard -->
                <div class="clear">
                </div>
            </div>
            <!-- start contact details -->
            <div class="contact-details">
                <div class="singlecolumncontent">

                    <ul class="contect-list">

                        <li >
                            <div class="handle">
                            </div>
                            <div>
                                <div class="country-heading" runat="server" id="headUK">
                                  
                                </div>
                                <div id="paneUK" runat="server" class="scroll-pane">
                                </div>
                            </div>
                        </li>

                       <%-- <li>
                            <div class="handle">
                            </div>
                            <div class="country-heading" runat="server" id="headFrance">
                                
                            </div>
                            <div id="paneFrance" runat="server" class="scroll-pane">
                                Container 2
                            </div>
                        </li>--%>

                        <li>
                            <div class="handle">
                            </div>
                            <div class="country-heading" runat="server" id="headGermany">                                
                            </div>
                            <div id="paneGermany" runat="server" class="scroll-pane">
                                Container 3
                            </div>
                        </li>

                        <%--<li style="display: none">
                            <div class="handle">
                            </div>
                            <div class="country-heading" runat="server" id="headNetherland">
                               
                            </div>
                            <div id="paneNetherland" runat="server" class="scroll-pane">
                                Container 4
                            </div>
                        </li>--%>

                       <%-- <li>
                            <div class="handle">
                            </div>
                            <div class="country-heading" runat="server" id="headBelgium">
                                
                            </div>
                            <div id="paneBelgium" runat="server" class="scroll-pane">
                                Container 5
                            </div>
                        </li>--%>

                      <%--  <li>
                            <div class="handle">
                            </div>
                            <div class="country-heading" runat="server" id="headSpain">
                               
                            </div>
                            <div id="paneSpain" runat="server" class="scroll-pane">
                                Container 6
                            </div>
                        </li>--%>

                        <li>
                            <div class="handle">
                            </div>
                            <div class="country-heading" runat="server" id="headItaly">
                                
                            </div>
                            <div id="paneItaly" runat="server" class="scroll-pane">
                                Container 7
                            </div>
                        </li>

                       <%--  <li style="display: none">
                            <div class="handle">
                            </div>
                            <div class="country-heading" runat="server" id="headSwitzerland">
                               
                            </div>
                            <div id="paneSwitzerland" runat="server" class="scroll-pane">
                                Container 8
                            </div>
                        </li>--%>
                       <%-- <li>
                            <div class="handle">
                            </div>
                            <div class="country-heading" runat="server" id="headCzechRepublic">
                                
                            </div>
                            <div id="paneCzechRepublic" runat="server" class="scroll-pane">
                                Container 9
                            </div>
                        </li>
                        <li>
                            <div class="handle">
                            </div>
                            <div class="country-heading" runat="server" id="headSweden">
                                
                            </div>
                            <div id="paneSweden" runat="server" class="scroll-pane">
                                Container 10
                            </div>
                        </li>--%>

                    </ul>
                </div>
                <div class="clear">
                </div>
            </div>
            <!-- end contact details -->
        </div>
        <!-- end content section -->
    </div>

    <div id="footer">
        <cc1:ucLabel ID="lblCopyRight" runat="server"></cc1:ucLabel>
    </div>
</asp:Content>

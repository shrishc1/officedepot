﻿using System;
using System.Data;
using System.Linq;
using System.Text;
using BusinessEntities.ModuleBE.AdminFunctions;
using BusinessEntities.ModuleBE.Discrepancy.LogDiscrepancy;
using BusinessLogicLayer.ModuleBAL.AdminFunctions;
using BusinessLogicLayer.ModuleBAL.Discrepancy.LogDiscrepancy;
using BusinessEntities.ModuleBE.Security;
using BusinessLogicLayer.ModuleBAL.Security;
using System.Collections.Generic;
using System.Web.UI;
using Utilities;
using WebUtilities;
using BusinessLogicLayer.ModuleBAL.Upload;
using BusinessEntities.ModuleBE.Upload;
using BusinessEntities.ModuleBE.Appointment.Booking;
using BusinessLogicLayer.ModuleBAL.Appointment.Booking;
using BusinessLogicLayer.ModuleBAL.StockOverview;
using BusinessEntities.ModuleBE.StockOverview;

public partial class ModuleUI_Dashboard_DefaultDashboard : CommonPage
{


    public static string HeadString;
    protected void Page_InIt(object sender, EventArgs e)
    {


    }

    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);
        GetDashboradData();
    }



    protected void Page_Load(object sender, EventArgs e)
    {
        MAS_SiteBAL oMAS_SiteBAL = new MAS_SiteBAL();
        MAS_SiteBE oMAS_SiteBE = new MAS_SiteBE();
        oMAS_SiteBE.Action = "GetSpecificCountrySite";
        DataTable dt = oMAS_SiteBAL.GetCountrySite(oMAS_SiteBE);    
        oMAS_SiteBAL = null;
        setHtml(dt);
        //GetDashboradData();
        Session["ModuleName"] = "HomePage";
    }

    private void GetDashboradData()
    {
        DiscrepancyBE oNewDiscrepancyBE = new DiscrepancyBE();
        DiscrepancyBAL oNewDiscrepancyBAL = new DiscrepancyBAL();
        SCT_UserBAL oSCT_UserBAL = new SCT_UserBAL();
        SCT_UserBE oSCT_UserBE = null;


        oNewDiscrepancyBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();

        oNewDiscrepancyBE.User.UserID = Convert.ToInt32(Session["UserID"]);
        oNewDiscrepancyBE.User.RoleName = Session["Role"] as string;
        DataTable dtDisLog = new DataTable();

        if (oNewDiscrepancyBE.User.RoleName == "GlobalAdministrator")
        {
            fvCountDiscrepancy.Visible = false;
            oSCT_UserBE = new SCT_UserBE();
            GetNewRegistrationCount(oSCT_UserBE);
        }
        else if (oNewDiscrepancyBE.User.RoleName == "OD - Manager")
        {
            fvCountDiscrepancy.Visible = false;
            fvAdminDashboard.Visible = false;
            fvManager.Visible = true;
        }

        else
        {

            oNewDiscrepancyBE.Action = "GetOpenWorkflowEntry";
            dtDisLog = oNewDiscrepancyBAL.GetOpenWorkflowEntry(oNewDiscrepancyBE);
            oNewDiscrepancyBAL = null;
            fvAdminDashboard.Visible = false;
            if (dtDisLog != null && dtDisLog.Rows.Count > 0)
            {
                lblOpenDiscrepancyCountNew.Text = lblOpenDiscrepancyCountNew.Text.Replace("#OpenDiscrepancyCount", dtDisLog.Rows[0]["DiscrepancyCount"].ToString());
            }
            #region Setting Refused Delivery Click Url With Text

            oSCT_UserBE = new SCT_UserBE();
            oSCT_UserBE.Action = "TodayRefusedDeliveriesCount";
            oSCT_UserBE.UserID = Convert.ToInt32(Session["UserId"]);
            int intGetTodayRefusedDeliveriesCount = oSCT_UserBAL.GetTodayRefusedDeliveriesCount(oSCT_UserBE) ?? 0;

            lblRefusedDeliveries_1.Text = WebCommon.getGlobalResourceValue("#RefusedDeliveries_1").Replace("#drt#", Convert.ToString(intGetTodayRefusedDeliveriesCount));
           // lblRefusedDeliveriesToView.Text = WebCommon.getGlobalResourceValue("#RefusedDeliveries_2View");
            #endregion

            switch (oNewDiscrepancyBE.User.RoleName)
            {
                case "OD - Goods In":

                    lblUnschedulePONew.Visible = false;
                    lblRaiseDiscrepancy.Visible = false;
                    lblPerformanceScoreCard.Visible = false;
                    lblOtifScorecard.Visible = false;
                    lblEditBooking.Visible = false;
                    lnkClickHere.Visible = false;
                    lblSearchDiscrepancy.Visible = false;
                    if (CheckValidLinks("Discrepancies"))
                    {
                        lblOpenDiscrepancyCountNew.Visible = true;
                        lblRaiseDiscrepancy.Visible = true;
                    }
                    else
                    {
                        lblOpenDiscrepancyCountNew.Visible = false;
                        lblRaiseDiscrepancy.Visible = false;
                    }
                    if (CheckValidLinks("AppScheduling"))
                    {
                        lblDeliverySchedule.Visible = true;
                        lblFutureDateBooking.Visible = true;
                        lblprovisionalbookingscount.Visible = true;
                        lnkClickHere_2.Visible = true;

                        APPBOK_BookingBE oAPPBOK_BookingBE = new APPBOK_BookingBE();
                        APPBOK_BookingBAL oAPPBOK_BookingBAL = new APPBOK_BookingBAL();
                        oAPPBOK_BookingBE.Action = "GetProvisionalBookings";
                        oAPPBOK_BookingBE.UserID = Convert.ToInt32(Session["UserId"]);
                        int? ProvisionalBookings = oAPPBOK_BookingBAL.UpdateBookingStatusByIdBAL(oAPPBOK_BookingBE);

                        lblprovisionalbookingscount.Text = WebCommon.getGlobalResourceValue("provisionalbookingscount").Replace("##Count##", ProvisionalBookings.ToString());
                    }
                    else
                    {
                        lblDeliverySchedule.Visible = false;
                        lblFutureDateBooking.Visible = false;
                    }
                    lblRefusedDeliveries_1.Visible = false;
                    lblRefusedDeliveriesToView.Visible = false;
                    lnkClickHere_1.Visible = false;

                    lblInvBOPenaltyReviewNew.Visible = false;
                    lnkClickHere_Inv.Visible = false;
                    lblToReview_Inv.Visible = false;
                    lblVendorBOPenaltyReview.Visible = false;
                    lnkClickHere_Ven.Visible = false;
                    lblToReview_Ven.Visible = false;
                    lblDisputeBOPenaltyReview.Visible = false;
                    lnkClickHere_Dis.Visible = false;
                    lblToReview_Dis.Visible = false;

                    break;
                case "OD - Accounts Payable":
                    lblUnschedulePONew.Visible = false;
                    lblRaiseDiscrepancy.Visible = false;
                    lblPerformanceScoreCard.Visible = false;
                    lblOtifScorecard.Visible = false;
                    lblDeliverySchedule.Visible = false;
                    lblFutureDateBooking.Visible = false;
                    lblEditBooking.Visible = false;
                    lnkClickHere.Visible = false;
                    if (CheckValidLinks("Discrepancies"))
                    {
                        lblOpenDiscrepancyCountNew.Visible = true;
                        lblSearchDiscrepancy.Visible = true;
                    }
                    else
                    {
                        lblOpenDiscrepancyCountNew.Visible = false;
                        lblSearchDiscrepancy.Visible = false;
                    }
                    lblRefusedDeliveries_1.Visible = false;
                    lblRefusedDeliveriesToView.Visible = false;
                    lnkClickHere_1.Visible = false;

                    lblInvBOPenaltyReviewNew.Visible = false;
                    lnkClickHere_Inv.Visible = false;
                    lblToReview_Inv.Visible = false;
                    lblVendorBOPenaltyReview.Visible = false;
                    lnkClickHere_Ven.Visible = false;
                    lblToReview_Ven.Visible = false;
                    lblDisputeBOPenaltyReview.Visible = false;
                    lnkClickHere_Dis.Visible = false;
                    lblToReview_Dis.Visible = false;
                    break;
                case "Vendor":

                    lblUnschedulePONew.Visible = false;
                    lblRaiseDiscrepancy.Visible = false;
                    lblDeliverySchedule.Visible = false;
                    lblSearchDiscrepancy.Visible = false;
                    lblFutureDateBooking.Visible = false;
                    oSCT_UserBE = new SCT_UserBE();
                    oSCT_UserBAL = new SCT_UserBAL();
                    oSCT_UserBE.UserID = Convert.ToInt32(Session["UserID"]);
                    oSCT_UserBE.Action = "GetUserOverview";
                    List<SCT_UserBE> lstUser = new List<SCT_UserBE>();
                    lstUser = oSCT_UserBAL.GetUsers(oSCT_UserBE);
                    oSCT_UserBAL = null;
                    if (lstUser.Count > 0 && lstUser[0].SchedulingContact == 'N')
                    {
                        lblEditBooking.Visible = false;
                        lnkClickHere.Visible = false;
                    }
                    if (lstUser.Count > 0 && lstUser[0].DiscrepancyContact == 'N')
                    {
                        lblOpenDiscrepancyCountNew.Visible = false;
                    }
                    if (lstUser.Count > 0 && lstUser[0].OTIFContact == 'N')
                    {
                        lblOtifScorecard.Visible = false;
                    }
                    if (lstUser.Count > 0 && lstUser[0].ScorecardContact == 'N')
                    {
                        lblPerformanceScoreCard.Visible = false;
                    }
                    lblRefusedDeliveries_1.Visible = false;
                    lblRefusedDeliveriesToView.Visible = false;
                    lnkClickHere_1.Visible = false;

                    GetSKUPenaltyCount("GetVendorPenaltySKUCount");
                    lblInvBOPenaltyReviewNew.Visible = false;
                    lnkClickHere_Inv.Visible = false;
                    lblToReview_Inv.Visible = false;                    
                    lblDisputeBOPenaltyReview.Visible = false;
                    lnkClickHere_Dis.Visible = false;
                    lblToReview_Dis.Visible = false;
                    if (IsVendorSubscribeForPenalty())
                    {
                        lblVendorBOPenaltyReview.Visible = true;
                        lnkClickHere_Ven.Visible = true;
                        lblToReview_Ven.Visible = true;
                    }
                    else
                    {
                        lblVendorBOPenaltyReview.Visible = false;
                        lnkClickHere_Ven.Visible = false;
                        lblToReview_Ven.Visible = false;
                    }

                    break;
                case "OD - Stock Planner":
                    lblUnschedulePONew.Visible = true;
                    lblRaiseDiscrepancy.Visible = false;
                    lblPerformanceScoreCard.Visible = false;
                    lblOtifScorecard.Visible = false;
                    lblEditBooking.Visible = false;
                    lnkClickHere.Visible = false;
                    lblSearchDiscrepancy.Visible = false;
                    lblFutureDateBooking.Visible = false;
                    if (CheckValidLinks("Discrepancies"))
                    {
                        lblOpenDiscrepancyCountNew.Visible = true;
                    }
                    else
                    {
                        lblOpenDiscrepancyCountNew.Visible = false;
                    }
                    if (CheckValidLinks("AppScheduling"))
                    {
                        lblDeliverySchedule.Visible = true;
                        lblUnschedulePONew.Visible = true;

                    }
                    else
                    {
                        lblDeliverySchedule.Visible = false;
                        lblUnschedulePONew.Visible = false;
                    }
                    lblRefusedDeliveries_1.Visible = true;
                    lblRefusedDeliveriesToView.Visible = true;
                    lnkClickHere_1.Visible = true;

                    GetSKUPenaltyCount("GetInventoryPenaltySKUCount");
                    GetDisputeSKUPenaltyCount("GetDisputeReviewCount");
                    lblInvBOPenaltyReviewNew.Visible = true;
                    lnkClickHere_Inv.Visible = true;
                    lblToReview_Inv.Visible = true;
                    lblVendorBOPenaltyReview.Visible = false;
                    lnkClickHere_Ven.Visible = false;
                    lblToReview_Ven.Visible = false;
                    lblDisputeBOPenaltyReview.Visible = true;
                    lnkClickHere_Dis.Visible = true;
                    lblToReview_Dis.Visible = true;
                    break;

                case "Carrier":

                    lblDeliverySchedule.Visible = false;
                    lblOpenDiscrepancyCountNew.Visible = false;
                    lblSearchDiscrepancy.Visible = false;
                    lblFutureDateBooking.Visible = false;
                    lblUnschedulePONew.Visible = false;
                    lblRaiseDiscrepancy.Visible = false;
                    lblPerformanceScoreCard.Visible = false;
                    lblOtifScorecard.Visible = false;
                    lblRefusedDeliveries_1.Visible = false;
                    lblRefusedDeliveriesToView.Visible = false;
                    lnkClickHere_1.Visible = false;

                    lblInvBOPenaltyReviewNew.Visible = false;
                    lnkClickHere_Inv.Visible = false;
                    lblToReview_Inv.Visible = false;
                    lblVendorBOPenaltyReview.Visible = false;
                    lnkClickHere_Ven.Visible = false;
                    lblToReview_Ven.Visible = false;
                    lblDisputeBOPenaltyReview.Visible = false;
                    lnkClickHere_Dis.Visible = false;
                    lblToReview_Dis.Visible = false;
                    break;
                case "OD - Site Manager":
                    fvCountDiscrepancy.Visible = false;
                    lblRefusedDeliveries_1.Visible = false;
                    lblRefusedDeliveriesToView.Visible = false;
                    lnkClickHere_1.Visible = false;

                    lblInvBOPenaltyReviewNew.Visible = false;
                    lnkClickHere_Inv.Visible = false;
                    lblToReview_Inv.Visible = false;
                    lblVendorBOPenaltyReview.Visible = false;
                    lnkClickHere_Ven.Visible = false;
                    lblToReview_Ven.Visible = false;
                    lblDisputeBOPenaltyReview.Visible = false;
                    lnkClickHere_Dis.Visible = false;
                    lblToReview_Dis.Visible = false;
                    break;
                default:
                    lblRefusedDeliveries_1.Visible = false;
                    lblRefusedDeliveriesToView.Visible = false;
                    lnkClickHere_1.Visible = false;

                    lblInvBOPenaltyReviewNew.Visible = false;
                    lnkClickHere_Inv.Visible = false;
                    lblToReview_Inv.Visible = false;
                    lblVendorBOPenaltyReview.Visible = false;
                    lnkClickHere_Ven.Visible = false;
                    lblToReview_Ven.Visible = false;
                    lblDisputeBOPenaltyReview.Visible = false;
                    lnkClickHere_Dis.Visible = false;
                    lblToReview_Dis.Visible = false;
                    break;
            }
        }

        if (lblDeliverySchedule.Visible == true)
        {
            string Querystring = string.Empty;
            Querystring = Encryption.Encrypt("DashboardDeliverySchedule=Yes").ToString();
            lblDeliverySchedule.Text = lblDeliverySchedule.Text.Replace("/APPBok_BookingOverview.aspx", "/APPBok_BookingOverview.aspx?" + Querystring + "");
        }

    }

    protected void lnkClickHere_Click(object sender, EventArgs e)
    {
        SCT_UserBE oSCT_UserBE = new SCT_UserBE();
        SCT_UserBAL oSCT_UserBAL = new SCT_UserBAL();
        if (Session["Role"].ToString().ToLower() == "vendor")
        {
            oSCT_UserBE.UserID = Convert.ToInt32(Session["UserID"]);
            oSCT_UserBE.Action = "GetUserOverview";
            List<SCT_UserBE> lstUser = new List<SCT_UserBE>();
            lstUser = oSCT_UserBAL.GetUsers(oSCT_UserBE);
            if (lstUser.Count > 0 && lstUser[0].SchedulingContact == 'Y')
            {
                SCT_UserBE oNewSCT_UserBE = new SCT_UserBE();
                oNewSCT_UserBE.Action = "GetVendorSite";
                oNewSCT_UserBE.UserID = Convert.ToInt32(Session["UserID"]);
                oNewSCT_UserBE.VendorID = lstUser[0].VendorID;
                oNewSCT_UserBE = oSCT_UserBAL.GetUserDefaultsBAL(oNewSCT_UserBE);
                oSCT_UserBAL = null;
                // Create Booking Page only when site are assigned to that venor
                if (oNewSCT_UserBE.Site != null && oNewSCT_UserBE.Site.SiteID != 0)
                {
                    EncryptQueryString("../Appointment/Booking/APPBok_BookingOverview.aspx");
                }
                else
                {
                    string Message = WebCommon.getGlobalResourceValue("SiteNotAllocated");
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + Message + "');", true);
                    return;
                }
            }
            else
            {
                string Message = WebCommon.getGlobalResourceValue("SiteNotExistMessage");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + Message + "');", true);
                return;
            }
        }
        else if (Session["Role"].ToString().ToLower() == "carrier")
        {
            oSCT_UserBE.Action = "GetCarrierSite";
            oSCT_UserBE.UserID = Convert.ToInt32(Session["UserID"]);
            oSCT_UserBE = oSCT_UserBAL.GetUserDefaultsBAL(oSCT_UserBE);
            oSCT_UserBAL = null;
            // Create Booking Page only when site are assigned to that venor
            if (oSCT_UserBE.Site != null && oSCT_UserBE.Site.SiteID != 0)
            {
                EncryptQueryString("../Appointment/Booking/APPBok_BookingOverview.aspx");
            }
            else
            {
                string Message = WebCommon.getGlobalResourceValue("SiteNotAllocated");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + Message + "');", true);
                return;
            }
        }
    }

    protected void lnkClickHere_2_Click(object sender, EventArgs e)
    {
        EncryptQueryString("../Appointment/ProvisionalBookings.aspx");
    }

    private bool CheckValidLinks(string Menuitem)
    {
        bool IsValidLink = false;
        SCT_UserBE oSCT_UserBE = new SCT_UserBE();
        SCT_UserBAL oSCT_UserBAL = new SCT_UserBAL();

        oSCT_UserBE.Action = "GetValidlinksForUser";
        oSCT_UserBE.UserID = Convert.ToInt32(Session["UserID"]); ;

        DataTable ValidLinksData = new DataTable();
        ValidLinksData = oSCT_UserBAL.GetValidlinksForUserBAL(oSCT_UserBE);
        oSCT_UserBAL = null;
        if (ValidLinksData != null && ValidLinksData.Rows.Count > 0)
        {
            for (int i = 0; i < ValidLinksData.Rows.Count; i++)
            {
                if (ValidLinksData.Rows[i]["ModuleName"].ToString() == Menuitem)
                {
                    IsValidLink = true;
                    break;
                }
                else
                    IsValidLink = false;
            }
        }
        else
            IsValidLink = false;

        return IsValidLink;

    }

    private void GetNewRegistrationCount(SCT_UserBE oSCT_UserBE)
    {
        oSCT_UserBE.RoleName = "GlobalAdministrator";
        SCT_UserBAL oSCT_UserBAL = new SCT_UserBAL();

        oSCT_UserBE.Action = "GetAdminDashboardData";
        oSCT_UserBE.AccountStatus = "Registered Awaiting Approval";
        oSCT_UserBE.UserID = Convert.ToInt32(Session["UserID"]);


        DataTable AdminDashboardData = new DataTable();
        AdminDashboardData = oSCT_UserBAL.GetAdminDashboardData(oSCT_UserBE);
        oSCT_UserBAL = null;
        if (AdminDashboardData != null && AdminDashboardData.Rows.Count > 0)
        {
            DataRow dr = AdminDashboardData.Rows[0];
            lblFailedCommunications.Text = lblFailedCommunications.Text.Replace("#Failedcommunication", dr[2].ToString());
            lblNewLogInRequests.Text = lblNewLogInRequests.Text.Replace(" #NewLoginRequests", dr[1].ToString());
            lblVendorsMissingDetails.Text = lblVendorsMissingDetails.Text.Replace("#VendorsMissingDetails", dr[0].ToString());

        }
        GetFailedUploadCount();
    }

    private void GetSKUPenaltyCount(string Action)
    {
        try
        {
            BackOrderBAL oBackOrderBAL = new BackOrderBAL();
            BackOrderBE oBackOrderBE = new BackOrderBE();
            oBackOrderBE.User = new SCT_UserBE();
            oBackOrderBE.Action = Action;
            oBackOrderBE.User.UserID = Convert.ToInt32(Session["UserID"]);
            List<BackOrderBE> lstBackOrderBE = oBackOrderBAL.GetPenaltyCountOnDashboardBAL(oBackOrderBE);
            if (lstBackOrderBE != null && lstBackOrderBE.Count > 0)
            {
                if (!string.IsNullOrEmpty(lstBackOrderBE[0].BackOrder))
                {
                    lblInvBOPenaltyReviewNew.Text = lblInvBOPenaltyReviewNew.Text.Replace("#BOCount#", lstBackOrderBE[0].BackOrder.ToString());
                }
                else 
                {
                    lblInvBOPenaltyReviewNew.Text = lblInvBOPenaltyReviewNew.Text.Replace("#BOCount#", "");
                }
                lblVendorBOPenaltyReview.Text = lblVendorBOPenaltyReview.Text.Replace("#VBOPR#", lstBackOrderBE[0].SKUCountOnDashboard.ToString());
                lblInvBOPenaltyReviewNew.Text = lblInvBOPenaltyReviewNew.Text.Replace("#IBOPR#", lstBackOrderBE[0].SKUCountOnDashboard.ToString());
               
            }
        }
        catch (Exception ex)
        {
            LogUtility.SaveErrorLogEntry(ex);
        }
    }

    private bool IsVendorSubscribeForPenalty()
    {
        bool IsVendorSubscribe = false;
        try
        {
            BackOrderBAL oBackOrderBAL = new BackOrderBAL();
            BackOrderBE oBackOrderBE = new BackOrderBE();
            oBackOrderBE.User = new SCT_UserBE();
            oBackOrderBE.Action = "CheckVendorSubscribeforPenalty";
            oBackOrderBE.User.UserID = Convert.ToInt32(Session["UserID"]);
            IsVendorSubscribe = oBackOrderBAL.CheckVendorSubscribeBAL(oBackOrderBE);

        }
        catch (Exception ex)
        {
            LogUtility.SaveErrorLogEntry(ex);
        }
        return IsVendorSubscribe;
    }


    private void GetDisputeSKUPenaltyCount(string Action)
    {
        try
        {
            BackOrderBAL oBackOrderBAL = new BackOrderBAL();
            BackOrderBE oBackOrderBE = new BackOrderBE();

            oBackOrderBE.User = new SCT_UserBE();

            oBackOrderBE.Action = Action;
            oBackOrderBE.User.UserID = Convert.ToInt32(Session["UserID"]);

            List<BackOrderBE> lstBackOrderBE = oBackOrderBAL.GetPenaltyCountOnDashboardBAL(oBackOrderBE);
            if (lstBackOrderBE != null && lstBackOrderBE.Count > 0)
            {
                lblDisputeBOPenaltyReview.Text = lblDisputeBOPenaltyReview.Text.Replace("#DBOPR#", lstBackOrderBE[0].SKUCountOnDashboard.ToString());
            }
        }
        catch (Exception ex)
        {
            LogUtility.SaveErrorLogEntry(ex);
        }
    }



    private void GetFailedUploadCount()
    {
        string Datefrom, DateTo = string.Empty;
        Datefrom = DateTime.Now.AddDays(Convert.ToInt16(System.Configuration.ConfigurationManager.AppSettings["DataImportBackDay"])).ToString("dd/MM/yyyy");
        DateTo = DateTime.Now.ToString("dd/MM/yyyy");
        UP_DataImportSchedulerBE oUP_DataImportSchedulerBE = new UP_DataImportSchedulerBE();
        UP_DataImportSchedulerBAL oUP_PurchaseOrderDetailBAL = new UP_DataImportSchedulerBAL();
        List<UP_DataImportSchedulerBE> lstUP_DataImportScheduler = new List<UP_DataImportSchedulerBE>();
        oUP_DataImportSchedulerBE.Action = "GetDataImportSchedulerErrorCount";
        oUP_DataImportSchedulerBE.DateFrom = string.IsNullOrEmpty(Datefrom) ? (DateTime?)null : Common.GetMM_DD_YYYY(Datefrom);
        oUP_DataImportSchedulerBE.DateTo = string.IsNullOrEmpty(DateTo) ? (DateTime?)null : Common.GetMM_DD_YYYY(DateTo);
        int? ErrorCount = oUP_PurchaseOrderDetailBAL.GetDataImportDetailErrorCountBAL(oUP_DataImportSchedulerBE);
        oUP_PurchaseOrderDetailBAL = null;

        lblFailedUploaded.Text = lblFailedUploaded.Text.Replace("#FailedUpload", Convert.ToString(ErrorCount));

    }

    private void setHtml(DataTable dtCountrySite)
    {
        StringBuilder strContactDetails = new StringBuilder();
        HeadString = @"<table cellpadding='0'cellspacing='0' width='100%' border='0'>
                                        <tr>
                                            <td colspan='2'>
                                                <span> <label>" + WebCommon.getGlobalResourceValue("CONTACTDETAIL1") + @"</label></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width='48%' align='left'>
                                                 <label>" + WebUtilities.WebCommon.getGlobalResourceValue("DistributionCentre") + @"</label>
                                            </td>
                                            <td align='left'>
                                                 <label>" + WebUtilities.WebCommon.getGlobalResourceValue("AssociatedAccountsPayableContact") + @"</label>
                                            </td>
                                        </tr>
                                    </table>"; 

        var lstCountryNames = (from r in dtCountrySite.AsEnumerable()
                               select r["countryName"]).Distinct().ToList();
         

        foreach (var sCountryName in lstCountryNames)
        {
            var query = (from myRow in dtCountrySite.AsEnumerable()
                         where myRow.Field<string>("countryName").ToLower().StartsWith(Convert.ToString(sCountryName).ToLower())
                         select myRow).ToList();
             
            foreach (var lst in query)
            {
                if ((lst["IsActive"] != DBNull.Value) && (Convert.ToBoolean(lst["IsActive"]) == true))
                {
                    //Remove Lanken (germany) and Paper Hub (UK) as per Anthony's Mail on 02 Feb 2015

                    if (!string.IsNullOrEmpty(Convert.ToString(lst["siteprefix"])) && Convert.ToString(lst["sitenumber"]) != "H" && Convert.ToString(lst["sitenumber"]) != "X")
                    {
                        //strContactDetails.Append("<p>");
                        #region Distribution Centre
                        strContactDetails.Append("<div class=\"add-left\">");
                        //if (!string.IsNullOrEmpty(Convert.ToString(lst["sitename"])))
                        //    strContactDetails.Append(lst["siteprefix"] + " - " + lst["sitename"] + "</br>");
                        if (!string.IsNullOrEmpty(Convert.ToString(lst["siteaddressline1"])))
                            strContactDetails.Append(lst["siteaddressline1"] + "</br>");
                        if (!string.IsNullOrEmpty(Convert.ToString(lst["siteaddressline2"])))
                            strContactDetails.Append(lst["siteaddressline2"] + "</br>");
                        if (!string.IsNullOrEmpty(Convert.ToString(lst["siteaddressline3"])))
                            strContactDetails.Append(lst["siteaddressline3"] + "</br>");
                        if (!string.IsNullOrEmpty(Convert.ToString(lst["siteaddressline4"])))
                            strContactDetails.Append(lst["siteaddressline4"] + "</br>");
                        if (!string.IsNullOrEmpty(Convert.ToString(lst["siteaddressline5"])))
                            strContactDetails.Append(lst["siteaddressline5"] + "</br>");
                        if (!string.IsNullOrEmpty(Convert.ToString(lst["siteaddressline6"])))
                            strContactDetails.Append(lst["siteaddressline6"] + "</br>");
                        if (!string.IsNullOrEmpty(Convert.ToString(lst["sitePinCode"])))
                            strContactDetails.Append(lst["sitePinCode"] + "</br>");

                        strContactDetails.Append("<strong>" + WebUtilities.WebCommon.getGlobalResourceValue("ReceivingOffice") + " </br>" + "</strong>");

                        if (!string.IsNullOrEmpty(Convert.ToString(lst["Phonenumbers"])))
                        strContactDetails.Append("Phone #&nbsp;&nbsp;: " + lst["Phonenumbers"] + "</br>");
                        if (!string.IsNullOrEmpty(Convert.ToString(lst["FaxNumber"])))
                        strContactDetails.Append("Fax #&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: " + lst["FaxNumber"] + "</br>");
                        
                        
                        if (!string.IsNullOrEmpty(Convert.ToString(lst["EmailID"])))
                        strContactDetails.Append("Email ID &nbsp;&nbsp;: " + "<a href=\"mailto:" + lst["EmailID"] + "\">" + lst["EmailID"] + "</a></br>");

                        if (!string.IsNullOrEmpty(Convert.ToString(lst["OfficeHour"])))
                            strContactDetails.Append("Office Hour&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: " + lst["OfficeHour"] + "</br>");


                        strContactDetails.Append("</div>");
                        #endregion
                        #region Account Payable Details
                        strContactDetails.Append("<div class=\"add-right\">");

                        if (!string.IsNullOrEmpty(Convert.ToString(lst["APaddressline1"])))
                            strContactDetails.Append(lst["APaddressline1"] + "</br>");
                        if (!string.IsNullOrEmpty(Convert.ToString(lst["APaddressline2"])))
                            strContactDetails.Append(lst["APaddressline2"] + "</br>");
                        if (!string.IsNullOrEmpty(Convert.ToString(lst["APaddressline3"])))
                            strContactDetails.Append(lst["APaddressline3"] + "</br>");
                        if (!string.IsNullOrEmpty(Convert.ToString(lst["APaddressline4"])))
                            strContactDetails.Append(lst["APaddressline4"] + "</br>");
                        if (!string.IsNullOrEmpty(Convert.ToString(lst["APaddressline5"])))
                            strContactDetails.Append(lst["APaddressline5"] + "</br>");
                        if (!string.IsNullOrEmpty(Convert.ToString(lst["APaddressline6"])))
                            strContactDetails.Append(lst["APaddressline6"] + "</br>");

                        strContactDetails.Append("</div>");
                        #endregion
                        strContactDetails.Append("<div class=\"clear\"></div>");
                        //strContactDetails.Append("</p>");
                        strContactDetails.Append("<hr>");
                    }
                }
            }

            setContactDetailsToPane(Convert.ToString(sCountryName).ToLower(), strContactDetails);

            strContactDetails.Clear(); 
        }
    }

    private void setContactDetailsToPane(string sCountryName, StringBuilder strContactDetails)
    {       
        if (sCountryName.Contains("ukandire"))
        {
            paneUK.InnerHtml = strContactDetails.ToString();
            headUK.InnerHtml = HeadString;
        }

        //else if (sCountryName.Contains("france"))
        //{
        //    paneFrance.InnerHtml = strContactDetails.ToString();
        //    headFrance.InnerHtml = HeadString;
        //}

        else if (sCountryName.Contains("germany"))
        {
            paneGermany.InnerHtml = strContactDetails.ToString();
            headGermany.InnerHtml = HeadString;
        }

        //else if (sCountryName.Contains("netherlands (cdc)")) {
        //    paneBelgium.InnerHtml = strContactDetails.ToString();
        //    headBelgium.InnerHtml = HeadString;
        //}
        //else if (sCountryName.Contains("netherland") || sCountryName.Contains("neatherland"))
        //{
        //    paneNetherland.InnerHtml = strContactDetails.ToString();
        //    headNetherland.InnerHtml = HeadString;
        //}
        //else if (sCountryName.Contains("spain"))
        //{
        //    paneSpain.InnerHtml = strContactDetails.ToString();
        //    headSpain.InnerHtml = HeadString;
        //}
        //else if (sCountryName.Contains("belgium")) {
        //    paneBelgium.InnerHtml = strContactDetails.ToString();
        //    headBelgium.InnerHtml = HeadString;
        //}

        else if (sCountryName.Contains("italy"))
        {
            paneItaly.InnerHtml = strContactDetails.ToString();
            headItaly.InnerHtml = HeadString;
        }

        //else if (sCountryName.Contains("switzerland"))
        //{
        //    paneSwitzerland.InnerHtml = strContactDetails.ToString();
        //    headSwitzerland.InnerHtml = HeadString;
        //}
        //else if (sCountryName.Contains("czech"))
        //{
        //    paneCzechRepublic.InnerHtml = strContactDetails.ToString();
        //    headCzechRepublic.InnerHtml = HeadString;
        //}
        //else if (sCountryName.Contains("sweden"))
        //{
        //    paneSweden.InnerHtml = strContactDetails.ToString();
        //    headSweden.InnerHtml = HeadString;
        //}
    }

    protected void lnkClickHere_1_Click(object sender, EventArgs e)
    {
        //string strRefusedDelUrl = "<a href=\"../Appointment/Booking/APPBok_BookingOverview.aspx\">Click here</a>";
        EncryptQueryString("~/ModuleUI/Appointment/Booking/APPBok_BookingOverview.aspx?RefusedDel=YES");
    }

    protected void lnkClickHereINV_Click(object sender, EventArgs e)
    {
        EncryptQueryString("~/ModuleUI/StockOverview/PenaltyReview/InventoryReview1.aspx?FromDashboard=YES");
    }

    protected void lnkClickHereDispute_Click(object sender, EventArgs e)
    {
        EncryptQueryString("~/ModuleUI/StockOverview/BackOrder/InventoryReview2.aspx?FromDashboard=YES");
    }
    protected void lnkClickHereVen_Click(object sender, EventArgs e)
    {
        EncryptQueryString("~/ModuleUI/StockOverview/BackOrder/VendorView.aspx?FromDashboard=YES");
    }

}
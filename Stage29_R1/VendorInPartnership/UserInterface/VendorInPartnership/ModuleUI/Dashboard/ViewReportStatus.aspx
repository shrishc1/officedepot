﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    EnableEventValidation="false" AutoEventWireup="true" CodeFile="ViewReportStatus.aspx.cs"
    Inherits="ViewReportStatus" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script language="javascript" type="text/javascript">
        function confirmDelete() {
            return confirm('<%=deleteMessage%>');
        }
        $(document).ready(function () {
            $("body").keypress(function (e) {
                if (e.which == 13 || e.keyCode == 13) {
                    $('#<%=btnRefreshReportStatus.ClientID %>').click();
                    return false;
                }
            });
        });
    </script>
    <h2>
        <cc1:ucLabel ID="lblReportQueue" runat="server" Text="Report Queue"></cc1:ucLabel>
    </h2>
    <table width="30%" cellspacing="1" cellpadding="0" border="0" align="center" class="top-settings">
        <tr>
            <td style="width: 50%">
                <cc1:ucRadioButton ID="rdoMyRequest" runat="server" Checked="true" Text="My Request"
                    GroupName="RequestType" OnCheckedChanged="rdoMyRequest_CheckedChanged" AutoPostBack="true" />
            </td>
            <td>
                <cc1:ucRadioButton ID="rdoAllRequest" runat="server" Checked="false" Text="All Request"
                    GroupName="RequestType" OnCheckedChanged="rdoAllRequest_CheckedChanged" AutoPostBack="true" />

            </td>
        </tr>
    </table>
    <div class="button-row">
        <span style="width: 300px; float: right; margin-top: -2px;" id="PoFile" runat="server">
            <asp:Label ID="lblPOfile" runat="server" Text="Download Open PO File"></asp:Label>
            <asp:LinkButton ID="lnkPoFile" runat="server" Text=" Open Purchase Order Report" OnClick="PoFile_Click" Style="color: red"></asp:LinkButton>
        </span>

    </div>
    <div class="button-row">
        <cc1:ucButton ID="btnRefreshReportStatus" runat="server" Text="Refresh Report Status" CssClass="button"
            OnClick="btnRefreshReport_Click" />
    </div>
    <table width="96%">

        <tr>
            <td align="center" colspan="9">
                <div style="overflow: scroll; width: 980px;">
                    <cc1:ucGridView ID="grdReportRequest" Width="100%" runat="server" CssClass="grid"
                        AutoGenerateColumns="False" AllowSorting="True" OnSorting="SortGrid" OnRowDataBound="grdReportRequest_RowDataBound">
                        <Columns>
                            <asp:BoundField HeaderText="ReportPosition" DataField="ReportPosition" SortExpression="ReportPosition">
                                <HeaderStyle Width="5%" HorizontalAlign="Left" />
                                <ItemStyle HorizontalAlign="Left" Width="5%" VerticalAlign="Top" />
                            </asp:BoundField>
                            <asp:BoundField HeaderText="RequestStatus" DataField="RequestStatus" SortExpression="RequestStatus">
                                <HeaderStyle HorizontalAlign="Left" Width="5%" />
                                <ItemStyle HorizontalAlign="Left" Width="5%" VerticalAlign="Top" />
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="ViewReport">
                                <HeaderStyle Width="5%" HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <asp:ImageButton Visible='<%# Eval("RequestStatus").ToString() == "Generated" || Eval("RequestStatus").ToString() == "Generated / Viewed" ? true : false %>'
                                        CommandName="ViewReport" CommandArgument='<%#Eval("ReportRequestID") %>' ID="btnViewReport" ImageUrl="~/Images/excel-icon.png"
                                        runat="server" Text="View Report" OnCommand="btnViewReport_Click" />

                                    <asp:HiddenField ID="hdnReportName" runat="server" Value='<%#Eval("ReportName") %>' />
                                    <asp:HiddenField ID="hdnReportType" runat="server" Value='<%#Eval("ReportType") %>' />
                                    <asp:HiddenField ID="hdnRequestTime" runat="server" Value='<%#Eval("RequestTime") %>' />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" />
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="RequestTime" DataField="RequestTime" SortExpression="RequestTime"
                                DataFormatString="{0:dd/MM/yyyy hh:mm:ss}">
                                <HeaderStyle HorizontalAlign="Left" Width="5%" />
                                <ItemStyle HorizontalAlign="Left" Width="5%" VerticalAlign="Top" />
                            </asp:BoundField>
                            <asp:BoundField HeaderText="ReportGeneratedTime" DataField="ReportGeneratedTime" SortExpression="ReportGeneratedTime"
                                DataFormatString="{0:dd/MM/yyyy hh:mm:ss}">
                                <HeaderStyle HorizontalAlign="Left" Width="5%" />
                                <ItemStyle HorizontalAlign="Left" Width="5%" VerticalAlign="Top" />
                            </asp:BoundField>
                            <asp:BoundField HeaderText="User Name" DataField="UserName" SortExpression="UserName">
                                <HeaderStyle Width="10%" HorizontalAlign="Left" />
                                <ItemStyle Width="10%" HorizontalAlign="Left" VerticalAlign="Top" />
                            </asp:BoundField>
                            <%--<asp:BoundField HeaderText="Module Name" DataField="ModuleName" SortExpression="ModuleName">
                                <HeaderStyle Width="5%" HorizontalAlign="Left" />
                                <ItemStyle HorizontalAlign="Left"  VerticalAlign="Top"/>
                            </asp:BoundField>
                            --%>
                            <asp:BoundField HeaderText="ReportName" DataField="ReportNameAndType" SortExpression="ReportNameAndType">
                                <HeaderStyle HorizontalAlign="Left" Width="15%" />
                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="15%" />
                            </asp:BoundField>

                            <asp:BoundField HeaderText="Country" DataField="CountryName" SortExpression="CountryName">
                                <HeaderStyle HorizontalAlign="Left" Width="5%" />
                                <ItemStyle HorizontalAlign="Left" Width="5%" VerticalAlign="Top" />
                            </asp:BoundField>
                            <asp:BoundField HeaderText="Site" DataField="SiteName" SortExpression="SiteName">
                                <HeaderStyle HorizontalAlign="Left" Width="5%" />
                                <ItemStyle HorizontalAlign="Left" Width="5%" VerticalAlign="Top" />
                            </asp:BoundField>
                            <asp:BoundField HeaderText="Vendor" DataField="VendorName" SortExpression="VendorName">
                                <HeaderStyle HorizontalAlign="Left" Width="5%" />
                                <ItemStyle HorizontalAlign="Left" Width="5%" VerticalAlign="Top" />
                            </asp:BoundField>
                            <asp:BoundField HeaderText="Stock Planner" DataField="StockPlannerName" SortExpression="StockPlannerName">
                                <HeaderStyle HorizontalAlign="Left" Width="5%" />
                                <ItemStyle HorizontalAlign="Left" Width="5%" VerticalAlign="Top" />
                            </asp:BoundField>
                            <asp:BoundField HeaderText="OD SKU Number" DataField="OD_SKU_No" SortExpression="OD_SKU_No">
                                <HeaderStyle HorizontalAlign="Left" Width="5%" />
                                <ItemStyle HorizontalAlign="Left" Width="5%" VerticalAlign="Top" />
                            </asp:BoundField>
                            <asp:BoundField HeaderText="Item Class" DataField="ItemClassification" SortExpression="ItemClassification">
                                <HeaderStyle HorizontalAlign="Left" Width="5%" />
                                <ItemStyle HorizontalAlign="Left" Width="5%" VerticalAlign="Top" />
                            </asp:BoundField>
                            <asp:BoundField HeaderText="Date Range" DataField="DateRange" SortExpression="DateRange">
                                <HeaderStyle HorizontalAlign="Left" Width="10%" />
                                <ItemStyle HorizontalAlign="Left" Width="10%" VerticalAlign="Top" />
                            </asp:BoundField>
                            <%--<asp:BoundField HeaderText="Date To" DataField="DateTo" SortExpression="DateTo" DataFormatString="{0:dd/MM/yyyy}">
                                <HeaderStyle HorizontalAlign="Left" Width="5%" />
                                <ItemStyle HorizontalAlign="Left"  VerticalAlign="Top"/>
                            </asp:BoundField>--%>
                            <asp:BoundField HeaderText="Purchase Order" DataField="PurchaseOrder" SortExpression="PurchaseOrder">
                                <HeaderStyle HorizontalAlign="Left" Width="5%" />
                                <ItemStyle HorizontalAlign="Left" Width="5%" VerticalAlign="Top" />
                            </asp:BoundField>
                            <%--<asp:BoundField HeaderText="Purchase Order Due" DataField="PurchaseOrderDue" SortExpression="PurchaseOrderDue">
                                <HeaderStyle HorizontalAlign="Left" Width="5%" />
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField HeaderText="Purchase Order Received" DataField="PurchaseOrderReceived"
                                SortExpression="PurchaseOrderReceived">
                                <HeaderStyle HorizontalAlign="Left" Width="5%" />
                                <ItemStyle HorizontalAlign="Left"  VerticalAlign="Top"
                            </asp:BoundField>--%>

                            <asp:TemplateField HeaderText="Delete">
                                <HeaderStyle Width="5px" HorizontalAlign="center" />
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnDeleteRequest" runat="server" ImageUrl="~/Images/delete.gif"
                                        OnClientClick="return confirmDelete();" OnCommand="btnDeleteRequest_Click"
                                        CommandArgument='<%#Eval("ReportRequestID") %>' />
                                    <asp:HiddenField ID="hdnRequestStatus" runat="server" Value='<%#Eval("RequestStatus") %>' />
                                    <asp:HiddenField ID="hdnUserId" runat="server" Value='<%#Eval("UserId") %>' />

                                </ItemTemplate>
                                <ItemStyle Width="5px" HorizontalAlign="Center" VerticalAlign="Top" />
                            </asp:TemplateField>
                        </Columns>
                    </cc1:ucGridView>
                </div>
            </td>
        </tr>
    </table>
</asp:Content>

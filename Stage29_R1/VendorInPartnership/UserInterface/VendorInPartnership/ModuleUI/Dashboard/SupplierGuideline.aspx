﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    AutoEventWireup="true" CodeFile="SupplierGuideline.aspx.cs" Inherits="SupplierGuideline" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript">
        function DisplayCountryMap(country) {
            var mapUrl = '../../Images/Maps/' + country + 'Map.jpg';
            $('#countryMap').attr('src', mapUrl);
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h2>
        <span style="width: 40px; float: left;margin-top:-2px;display:none;">
          <a href="../../Documents/SupplierGuidelines/CDC_Guidelines.pdf">
            <img src="../../Images/flags/EuropeanUnionFlag2.JPG" alt="EuropeanUnion" width="30" height="20" />
             </a>
        </span><span style="float: left;width: 85%;">
            <cc1:ucLabel runat="server" ID="lblEuropeanUnion" Text="Office Depot European Supplier Guidelines" />
        </span>
    </h2>
    <div >
        <div style="font-size: 13px; width: 100%; color: Red; float: left;padding-left:5px;padding-top:20px;">
            <cc1:ucLabel ID="lblSupplierGuideline" runat="server" />
        </div>
        <div class="clear"></div>
        <div style="width: 25%; float: left;">
            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="dashbaord-list">
                <tr>
                    <td style="width: 25%">
                        <a href="../../Documents/SupplierGuidelines/UK-Guidelines.pdf" target="_blank">
                            <img src="../../Images/flags/UnitedKingdomFlag.JPG" alt="United Kingdom" onmouseover="DisplayCountryMap('UnitedKingdom');" />
                        </a>
                    </td>
                    <td>
                       <a href="../../Documents/SupplierGuidelines/UK-Guidelines.pdf" target="_blank">   UnitedKingdom </a>
                    </td>
                </tr>
                <tr>
                    <td>
                        <a href="../../Documents/SupplierGuidelines/Ireland-Guidelines.pdf" target="_blank">
                            <img src="../../Images/flags/IrelandFlag.JPG" alt="IreLand" onmouseover="DisplayCountryMap('Ireland');" />
                        </a>
                    </td>
                    <td>
                       <a href="../../Documents/SupplierGuidelines/Ireland-Guidelines.pdf" target="_blank">  Ireland  </a>
                    </td>
                </tr>
                <tr>
                    <td>
                        <a href="../../Documents/SupplierGuidelines/France-Guidelines.pdf" target="_blank">
                            <img src="../../Images/flags/FranceFlag.JPG" alt="France" onmouseover="DisplayCountryMap('France');" />
                        </a>
                    </td>
                    <td>
                        <a href="../../Documents/SupplierGuidelines/France-Guidelines.pdf" target="_blank"> France  </a>
                    </td>
                </tr>
                <tr>
                    <td>
                        <a href="../../Documents/SupplierGuidelines/Germany-Guidelines.pdf" target="_blank">
                            <img src="../../Images/flags/GermanyFlag.JPG" alt="Germany" onmouseover="DisplayCountryMap('Germany');" />
                        </a>
                    </td>
                    <td>
                       <a href="../../Documents/SupplierGuidelines/Germany-Guidelines.pdf" target="_blank">  Germany  </a>
                    </td>
                </tr>
                <tr>
                    <td>
                        <a href="../../Documents/SupplierGuidelines/Netherlands-Guidelines.pdf" target="_blank">
                            <img src="../../Images/flags/NetherlandFlag.JPG" alt="NetherLand" onmouseover="DisplayCountryMap('NetherLand');" />
                        </a>
                    </td>
                    <td>
                       <a href="../../Documents/SupplierGuidelines/Netherlands-Guidelines.pdf" target="_blank">  Netherlands  </a>
                    </td>
                </tr>
                <tr>
                    <td>
                        <a href="../../Documents/SupplierGuidelines/Spain-Guidelines.pdf" target="_blank">
                            <img src="../../Images/flags/SpainFlag.JPG" alt="Spain" onmouseover="DisplayCountryMap('Spain');" />
                        </a>
                    </td>
                    <td>
                        <a href="../../Documents/SupplierGuidelines/Spain-Guidelines.pdf" target="_blank"> Spain  </a>
                    </td> 
                </tr>
                <tr style="display:none;">
                    <td>
                        <a href="../../Documents/SupplierGuidelines/Belgium_Guidelines.pdf">
                            <img src="../../Images/flags/BelgiumFlag.JPG" alt="Belgium" onmouseover="DisplayCountryMap('Belgium');" />
                        </a>
                    </td>
                    <td>
                       <a href="../../Documents/SupplierGuidelines/Belgium_Guidelines.pdf"> Belgium  </a>
                    </td>
                </tr>
                <tr>
                    <td>
                        <a href="../../Documents/SupplierGuidelines/Italy-Guidelines.pdf" target="_blank">
                            <img src="../../Images/flags/ItalyFlag.JPG" alt="Italy" onmouseover="DisplayCountryMap('Italy');" />
                        </a>
                    </td>
                    <td>
                        <a href="../../Documents/SupplierGuidelines/Italy-Guidelines.pdf" target="_blank">  Italy  </a>
                    </td>
                </tr>
                <tr>
                    <td>
                        <a href="../../Documents/SupplierGuidelines/Sweden-Guidelines.pdf" target="_blank">
                            <img src="../../Images/flags/SwedenFlag.JPG" alt="Sweden" onmouseover="DisplayCountryMap('Sweden');" />
                        </a>
                    </td>
                    <td>
                        <a href="../../Documents/SupplierGuidelines/Sweden-Guidelines.pdf" target="_blank"> Sweden  </a>
                    </td>
                </tr>
                <tr>
                    <td>
                        <a href="../../Documents/SupplierGuidelines/Czech-Guidelines.pdf" target="_blank">
                            <img src="../../Images/flags/CzechRepublicFlag.JPG" alt="Czech Repulic" onmouseover="DisplayCountryMap('CzechRepublic');" />
                        </a>
                    </td>
                    <td>
                      <a href="../../Documents/SupplierGuidelines/Czech-Guidelines.pdf" target="_blank">  Czech Repulic </a>
                    </td>
                </tr>
                <tr style="display:none;">
                    <td>
                        <a href="../../Documents/SupplierGuidelines/Switzerland_Guidelines.pdf">
                            <img src="../../Images/flags/SwitzerlandFlag.JPG" alt="SwitzerLand" onmouseover="DisplayCountryMap('SwitzerLand');" />
                        </a>
                    </td>
                    <td>
                       <a href="../../Documents/SupplierGuidelines/Switzerland_Guidelines.pdf">  Switzerland  </a>
                    </td>
                </tr>
            </table>
        </div>
        <div style="width: 69%; float: right; border: '1';">
            <img src="../../Images/Maps/UnitedKingdomMap.jpg" id="countryMap" alt="Country Map" style="float:right;" />
        </div>
        <div class="clear"></div>
    </div>
</asp:Content>

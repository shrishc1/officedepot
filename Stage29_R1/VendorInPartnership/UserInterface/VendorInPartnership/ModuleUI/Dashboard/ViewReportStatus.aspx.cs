﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.ReportRequest;
using BusinessLogicLayer.ModuleBAL.ReportRequest;
using System.IO;
using System.Configuration;
using Utilities;
using WebUtilities;


public partial class ViewReportStatus : CommonPage
{

    #region ReportVariables


    string mimeType = string.Empty;
    string reportFileName = string.Empty;

    #endregion
    protected string deleteMessage = WebCommon.getGlobalResourceValue("DeleteMessage");
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            BindReportRequestGrid();
            var POEmailIds = ConfigurationManager.AppSettings["POEmailIds"];
            PoFile.Visible = POEmailIds.Split(',').Contains(Session["LoginID"].ToString(), StringComparer.InvariantCultureIgnoreCase);
        }
    }

    private void BindReportRequestGrid()
    {

        try
        {
            ReportRequestBE oReportRequestBE = new ReportRequestBE();
            ReportRequestBAL oReportRequestBAL = new ReportRequestBAL();

            oReportRequestBE.Action = "GetReportRequest";
            oReportRequestBE.UserID = rdoMyRequest.Checked ? Convert.ToInt32(Session["UserID"]) : (int?)null;
            //oReportRequestBE.ModuleName = GetQueryStringValue("ModuleName");

            List<ReportRequestBE> ReportRequestList = new List<ReportRequestBE>();
            ReportRequestList = oReportRequestBAL.GetReportRequestBAL(oReportRequestBE);
            oReportRequestBAL = null;
            grdReportRequest.DataSource = ReportRequestList;
            grdReportRequest.DataBind();
            ViewState["ReportRequestList"] = ReportRequestList;
        }
        catch (Exception ex)
        {
            Utilities.LogUtility.SaveErrorLogEntry(ex);
        }
    }

    public override BusinessEntities.BaseBe[] SubSort(GridViewSortEventArgs e)
    {
        return Utilities.GenericListHelper<ReportRequestBE>.SortList((List<ReportRequestBE>)ViewState["ReportRequestList"], e.SortExpression, e.SortDirection).ToArray();
    }

    protected void btnDeleteRequest_Click(object sender, CommandEventArgs e)
    {
        int ReportRequestID = Convert.ToInt32(e.CommandArgument);
        ReportRequestBE oReportRequestBE = new ReportRequestBE();
        ReportRequestBAL oReportRequestBAL = new ReportRequestBAL();
        oReportRequestBE.Action = "DeleteReportRequest";
        oReportRequestBE.ReportRequestID = ReportRequestID;
        int? Result = oReportRequestBAL.DeleteReportRequestBAL(oReportRequestBE);
        oReportRequestBAL = null;
        BindReportRequestGrid();
    }

    protected void btnViewReport_Click(object sender, CommandEventArgs e)
    {

        if (e.CommandName == "ViewReport")
        {

            string ReportName = string.Empty;
            string ReportType = string.Empty;
            string UserId = string.Empty;
            DateTime dt = DateTime.Now;

            //Updating Report Status once view report button is clicked
            int ReportRequestID = Convert.ToInt32(e.CommandArgument);
            ReportRequestBE oReportRequestBE = new ReportRequestBE();
            ReportRequestBAL oReportRequestBAL = new ReportRequestBAL();

            oReportRequestBE.Action = "UpdateReportStatus";
            oReportRequestBE.RequestStatus = "Generated / Viewed";
            oReportRequestBE.ReportRequestID = ReportRequestID;

            int? Result = oReportRequestBAL.UpdateReportStatusBAL(oReportRequestBE);

            //Get Information on basis of Report Request Id.
            oReportRequestBE.Action = "GetReportRequest";
            oReportRequestBE.UserID = rdoMyRequest.Checked ? Convert.ToInt32(Session["UserID"]) : (int?)null;
            //oReportRequestBE.ModuleName = GetQueryStringValue("ModuleName");
            oReportRequestBE.ReportRequestID = ReportRequestID;

            List<ReportRequestBE> ReportRequestList = new List<ReportRequestBE>();
            ReportRequestList = oReportRequestBAL.GetReportRequestBAL(oReportRequestBE);
            oReportRequestBAL = null;
            if (ReportRequestList.Count > 0)
            {
                ReportName = ReportRequestList[0].ReportName.ToString();
                ReportType = ReportRequestList[0].ReportType.ToString();
                UserId = ReportRequestList[0].UserID.ToString();
                dt = Convert.ToDateTime(ReportRequestList[0].RequestTime.ToString());
            }

            string requestTime = "";
            if (dt.Day.ToString().Length == 1)
                requestTime = requestTime + "0" + dt.Day.ToString();
            else
                requestTime = requestTime + dt.Day.ToString();

            if (dt.Month.ToString().Length == 1)
                requestTime = requestTime + "0" + dt.Month.ToString();
            else
                requestTime = requestTime + dt.Month.ToString();

            requestTime = requestTime + dt.Year.ToString();

            requestTime = requestTime + "_" + dt.Hour.ToString() + dt.Minute.ToString() + dt.Second.ToString();

            //Open Excel file from path
            string ReportOutputPath = ConfigurationManager.AppSettings["QueueReportPath"];
            ReportOutputPath = ReportOutputPath + "\\" + ReportRequestID + "_" + ReportName + "_";
            if (string.IsNullOrEmpty(ReportType))
                ReportOutputPath = ReportOutputPath + UserId + "_" + requestTime + ".xls";
            else
                ReportOutputPath = ReportOutputPath + ReportType + "_" + UserId + "_" + requestTime + ".xls";

            if (File.Exists(ReportOutputPath))
            {
                FileStream sourceFile = new FileStream(ReportOutputPath, FileMode.Open);
                float FileSize;
                FileSize = sourceFile.Length;
                byte[] getContent = new byte[(int)FileSize];
                sourceFile.Read(getContent, 0, (int)sourceFile.Length);
                sourceFile.Close();
                Response.ClearContent();
                Response.ClearHeaders();
                Response.Buffer = true;
                Response.ContentType = "application/vnd.ms-excel";
                Response.AddHeader("Content-Length", getContent.Length.ToString());
                Response.AddHeader("Content-Disposition", "attachment; filename=" + HttpUtility.UrlEncode(ReportOutputPath));
                Response.BinaryWrite(getContent);
                Response.Flush();
                Response.End();
            }
            else
            {
                string ExcelFileNotExist = WebCommon.getGlobalResourceValue("ExcelFileNotExist");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + ExcelFileNotExist + "')", true);
                return;
            }
        }
    }

    protected void btnRefreshReport_Click(object sender, EventArgs e)
    {
        BindReportRequestGrid();
    }

    protected void rdoMyRequest_CheckedChanged(object sender, EventArgs e)
    {
        BindReportRequestGrid();
    }

    protected void rdoAllRequest_CheckedChanged(object sender, EventArgs e)
    {
        BindReportRequestGrid();
    }

    protected void grdReportRequest_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType != DataControlRowType.Header
            && e.Row.RowType != DataControlRowType.Footer)
        {

            ImageButton DelImgButton = (ImageButton)e.Row.FindControl("btnDeleteRequest");
            HiddenField hdnRequestStatus = (HiddenField)e.Row.FindControl("hdnRequestStatus");
            HiddenField hdnUserId = (HiddenField)e.Row.FindControl("hdnUserId");

            //if (hdnRequestStatus.Value.ToLower() == "pending" || hdnRequestStatus.Value.ToLower() == "generated / viewed") {
            if (Session["Role"].ToString() == "GlobalAdministrator")
            {
                DelImgButton.Visible = true;
            }
            else if (Session["Role"].ToString() != "GlobalAdministrator")
            {
                if (Convert.ToInt32(Session["UserID"]) == Convert.ToInt32(hdnUserId.Value))
                {
                    DelImgButton.Visible = true;
                }
                else
                {
                    DelImgButton.Visible = false;
                }
            }
            //}
            else
            {
                DelImgButton.Visible = false;
            }

        }
    }

    protected void PoFile_Click(object sender, EventArgs e)
    {
        var fileName = ConfigurationManager.AppSettings["PODataFile"];
        FileInfo file = new FileInfo(fileName);
        Context.Response.Clear();
        Context.Response.ClearHeaders();
        Context.Response.ClearContent();
        Context.Response.AddHeader("Content-Disposition", "attachment; filename=" + file.Name);
        Context.Response.AddHeader("Content-Length", file.Length.ToString());
        Context.Response.ContentType = "text/plain";
        Context.Response.Flush();
        Context.Response.TransmitFile(file.FullName);
        Context.Response.End();
    }
}
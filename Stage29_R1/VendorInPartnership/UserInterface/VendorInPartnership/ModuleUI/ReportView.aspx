﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    AutoEventWireup="true" CodeFile="ReportView.aspx.cs" Inherits="ReportView" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
 <asp:ScriptManager ID="SM1" runat="server" AsyncPostBackTimeout="1200">
  </asp:ScriptManager>
    <div class="right-shadow">
        <div class="formbox">
            <rsweb:ReportViewer ID="ReportViewer1" runat="server" Height="550px" Width="950px"
                DocumentMapCollapsed="True" AsyncRendering="true">
            </rsweb:ReportViewer>          
        </div>
        <div style="text-align:right;">
            <br />
            <cc1:ucbutton id="BackSearch" runat="server" cssclass="button" onclick="BackSearch_Click" />
        </div>
    </div>
</asp:Content>

﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    CodeFile="OTIFRulesSetting.aspx.cs" Inherits="OTIFRulesSetting" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2>
        <cc1:ucLabel ID="lblRulesSettings" runat="server" Text=" Otif Rules Settings"></cc1:ucLabel>
    </h2>
    <asp:ScriptManager ID="sn" runat="server">
    </asp:ScriptManager>
    <div class="right-shadow">
        <div class="formbox">
            <asp:UpdatePanel ID="pnlUP1" runat="server">
                <ContentTemplate>
                    <table width="90%" cellspacing="5" cellpadding="0" class="form-table">
                        <tr>
                            <td>
                                <cc1:ucPanel ID="pnlAbsolute" runat="server" GroupingText="Absolute" CssClass="fieldset-form">
                                    <table width="90%" cellspacing="5" cellpadding="0" class="form-table">
                                        <tr>
                                            <td style="font-weight: bold; width: 30%">
                                                <cc1:ucLabel ID="lblDaysAllowedEarly_1" runat="server" Text="lblDaysAllowedEarly_1"></cc1:ucLabel>
                                            </td>
                                            <td style="font-weight: bold; width: 5%">
                                                <cc1:ucLabel ID="UcLabel3" runat="server">:</cc1:ucLabel>
                                            </td>
                                            <td style="font-weight: bold; width: 65%">
                                                <cc1:ucTextbox ID="txtDaysAllowedEarly_1" onkeyup="AllowNumbersOnly(this);" onkeypress="return IsNumberKey(event, this);" runat="server"
                                                    Width="40px" MaxLength="2"></cc1:ucTextbox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: bold;">
                                                <cc1:ucLabel ID="lblDaysAllowedLate_1" runat="server" Text="lblDaysAllowedLate_1"></cc1:ucLabel>
                                            </td>
                                            <td style="font-weight: bold; width: 5px">
                                                <cc1:ucLabel ID="UcLabel5" runat="server">:</cc1:ucLabel>
                                            </td>
                                            <td style="font-weight: bold;">
                                                <cc1:ucTextbox ID="txtDaysAllowedLate_1" onkeyup="AllowNumbersOnly(this);" onkeypress="return IsNumberKey(event, this);" runat="server"
                                                    Width="40px" MaxLength="2"></cc1:ucTextbox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: bold;">
                                                <cc1:ucLabel ID="lblNumberofDeliveriesAllowed_1" runat="server" Text="lblNumberofDeliveriesAllowed_1"></cc1:ucLabel>
                                            </td>
                                            <td style="font-weight: bold; width: 5px">
                                                <cc1:ucLabel ID="UcLabel2" runat="server">:</cc1:ucLabel>
                                            </td>
                                            <td style="font-weight: bold;">
                                                <cc1:ucTextbox ID="txtNumberofDeliveriesAllowed_1" onkeyup="AllowNumbersOnly(this);"  onkeypress="return IsNumberKey(event, this);"
                                                    runat="server" Width="40px" MaxLength="2"></cc1:ucTextbox>
                                            </td>
                                        </tr>
                                    </table>
                                </cc1:ucPanel>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <cc1:ucPanel ID="pnlNewOTIFMeasure" runat="server" GroupingText="New OTIF Measure" CssClass="fieldset-form">
                                    <table width="90%" cellspacing="5" cellpadding="0" class="form-table">
                                        <tr>
                                            <td style="font-weight: bold; width: 30%">
                                                <cc1:ucLabel ID="lblDaysAllowedEarly_2" runat="server" Text="lblDaysAllowedEarly_2"></cc1:ucLabel>
                                            </td>
                                            <td style="font-weight: bold; width: 5%">
                                                <cc1:ucLabel ID="UcLabel4" runat="server">:</cc1:ucLabel>
                                            </td>
                                            <td style="font-weight: bold; width: 65%">
                                                <cc1:ucTextbox ID="txtDaysAllowedEarly_2" onkeyup="AllowNumbersOnly(this);" onkeypress="return IsNumberKey(event, this);" runat="server"
                                                    Width="40px" MaxLength="2"></cc1:ucTextbox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: bold;">
                                                <cc1:ucLabel ID="lblDaysAllowedLate_2" runat="server" Text="lblDaysAllowedLate_2"></cc1:ucLabel>
                                            </td>
                                            <td style="font-weight: bold; width: 5px">
                                                <cc1:ucLabel ID="UcLabel8" runat="server">:</cc1:ucLabel>
                                            </td>
                                            <td style="font-weight: bold;">
                                                <cc1:ucTextbox ID="txtDaysAllowedLate_2" onkeyup="AllowNumbersOnly(this);" onkeypress="return IsNumberKey(event, this);" runat="server"
                                                    Width="40px" MaxLength="2"></cc1:ucTextbox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: bold;">
                                                <cc1:ucLabel ID="lblNumberofDeliveriesAllowed_2" runat="server" Text="lblNumberofDeliveriesAllowed_2"></cc1:ucLabel>
                                            </td>
                                            <td style="font-weight: bold; width: 5px">
                                                <cc1:ucLabel ID="UcLabel12" runat="server">:</cc1:ucLabel>
                                            </td>
                                            <td style="font-weight: bold;">
                                                <cc1:ucTextbox ID="txtNumberofDeliveriesAllowed_2" onkeyup="AllowNumbersOnly(this);" onkeypress="return IsNumberKey(event, this);"
                                                    runat="server" Width="40px" MaxLength="2"></cc1:ucTextbox>
                                            </td>
                                        </tr>
                                    </table>
                                </cc1:ucPanel>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <cc1:ucPanel ID="pnlOldOTIFMeasure" runat="server" GroupingText="Old OTIF Measure"
                                    CssClass="fieldset-form">
                                    <table width="90%" cellspacing="5" cellpadding="0" class="form-table">
                                        <tr>
                                            <td style="font-weight: bold; width: 30%">
                                                <cc1:ucLabel ID="lblDaysAllowedEarly_3" runat="server" Text="lblDaysAllowedEarly_3"></cc1:ucLabel>
                                            </td>
                                            <td style="font-weight: bold; width: 5%">
                                                <cc1:ucLabel ID="UcLabel11" runat="server">:</cc1:ucLabel>
                                            </td>
                                            <td style="font-weight: bold; width: 65%">
                                                <cc1:ucTextbox ID="txtDaysAllowedEarly_3" onkeyup="AllowNumbersOnly(this);" onkeypress="return IsNumberKey(event, this);" runat="server"
                                                    Width="40px" MaxLength="2"></cc1:ucTextbox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: bold;">
                                                <cc1:ucLabel ID="lblDaysAllowedLate_3" runat="server" Text="lblDaysAllowedLate_3"></cc1:ucLabel>
                                            </td>
                                            <td style="font-weight: bold; width: 5px">
                                                <cc1:ucLabel ID="UcLabel14" runat="server">:</cc1:ucLabel>
                                            </td>
                                            <td style="font-weight: bold;">
                                                <cc1:ucTextbox ID="txtDaysAllowedLate_3" onkeyup="AllowNumbersOnly(this);" onkeypress="return IsNumberKey(event, this);" runat="server"
                                                    Width="40px" MaxLength="2"></cc1:ucTextbox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: bold;">
                                                <cc1:ucLabel ID="lblNumberofDeliveriesAllowed_3" runat="server" Text="lblNumberofDeliveriesAllowed_3"></cc1:ucLabel>
                                            </td>
                                            <td style="font-weight: bold; width: 5px">
                                                <cc1:ucLabel ID="UcLabel16" runat="server">:</cc1:ucLabel>
                                            </td>
                                            <td style="font-weight: bold;">
                                                <cc1:ucTextbox ID="txtNumberofDeliveriesAllowed_3" onkeyup="AllowNumbersOnly(this);" onkeypress="return IsNumberKey(event, this);"
                                                    runat="server" Width="40px" MaxLength="2"></cc1:ucTextbox>
                                            </td>
                                        </tr>
                                    </table>
                                </cc1:ucPanel>
                            </td>
                        </tr>
                    </table>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
    <div class="bottom-shadow">
    </div>
    <div class="button-row">
        <cc1:ucButton ID="btnSave" runat="server" Text="Save" CssClass="button" OnClick="btnSave_Click" />
        <%-- <cc1:ucButton ID="btnDelete" runat="server" Text="Delete" CssClass="button" />
        <cc1:ucButton ID="btnBack" runat="server" Text="Back" CssClass="button" />--%>
    </div>
</asp:Content>

﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.OnTimeInFull;
using BusinessEntities.ModuleBE.Upload;
using BusinessLogicLayer.ModuleBAL.OnTimeInFull;
using System.Collections;
using WebUtilities;
using Utilities;
using System.Web;

public partial class ApplyOTIFExclusionOverview : CommonPage
{
    #region Declarations ...
    private const int gridPageSize = 50;
    private bool IsGoClicked = false;
    #endregion

    #region Events ...

    protected void Page_InIt(object sender, EventArgs e)
    {
        ucSite.CurrentPage = this;
        ucSite.IsAllRequired = true;
    }

    public override void SitePost_Load()
    {
        if (!IsPostBack)
        {
            ucSite.innerControlddlSite.SelectedIndex = 0;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (!string.IsNullOrEmpty(GetQueryStringValue("Datefrom")))
            {
                txtFromDate.Text = GetQueryStringValue("Datefrom");
                if (!string.IsNullOrEmpty(GetQueryStringValue("DateTo")))
                {
                    txtToDate.Text = GetQueryStringValue("DateTo");
                }
                else
                {
                    txtToDate.Text = GetQueryStringValue("Datefrom");
                }
            }
            else
            {
                txtFromDate.Text = DateTime.Now.AddDays(-DateTime.Now.Day + 1).ToString("dd/MM/yyyy");
                txtToDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
            }
            hdnJSFromDt.Value = txtFromDate.Text;
            hdnJSToDt.Value = txtToDate.Text;
            pager1.PageSize = gridPageSize;
            pager1.GenerateGoToSection = false;
            pager1.GeneratePagerInfoSection = false;
            pager1.Visible = false;
            if (GetQueryStringValue("PreviousPage") == null)
                BindGridOnSession();
            //if (!string.IsNullOrEmpty(GetQueryStringValue("Datefrom")))
            //{               
               
            //}
        }

       
        txtPOSearch.Focus();
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (GetQueryStringValue("PreviousPage") != null && GetQueryStringValue("PreviousPage").ToString() == "PO")
        {
            if (Session["OTIFExclusion"] != null)
            {
                if (IsGoClicked == false)
                    GetSession();
            }
        }
        if (!string.IsNullOrEmpty(GetQueryStringValue("Datefrom")) && IsGoClicked==false)
        {
            ucSite.innerControlddlSite.SelectedValue = GetQueryStringValue("SiteID");
            BindGrid();
        }
    }
    protected void btnBack_Click(object sender, EventArgs e)
    {
        Page.ClientScript.RegisterStartupScript(this.GetType(), "script", "window.close();", true);
    }
    protected void btnGo_Click(object sender, EventArgs e)
    {
   
        //ApplyOTIFExclusionBE applyOTIFExclusionBE = new ApplyOTIFExclusionBE();
        //ApplyOTIFExclusionBAL applyOTIFExclusionBAL = new ApplyOTIFExclusionBAL();

        //applyOTIFExclusionBE.Action = "GetSelectedApplySKUExclusion";
        //List<ApplyOTIFExclusionBE> lstApplyOTIFExclusion = new List<ApplyOTIFExclusionBE>();
        //try
        //{
        //    applyOTIFExclusionBE.PurchaseOrderID = txtPOSearch.Text == string.Empty ? 0 : Convert.ToInt32(txtPOSearch.Text);
        //}
        //catch
        //{
        //    applyOTIFExclusionBE.PurchaseOrderID = 0;
        //}

        //applyOTIFExclusionBE.SelectedVendorIDs = msVendor.SelectedVendorIDs;
        //lstApplyOTIFExclusion = applyOTIFExclusionBAL.GetSelectedApplySKUExclusionBAL(applyOTIFExclusionBE);
        //applyOTIFExclusionBAL = null;
        //grdRevisedleadListing.DataSource = lstApplyOTIFExclusion;
        //grdRevisedleadListing.DataBind();
        //ViewState["lstSkuSites"] = lstApplyOTIFExclusion;
        IsGoClicked = true;
        BindGrid(1);
        msVendor.setVendorsOnPostBack();
    }

    protected void grdRevisedleadListing_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
     
        if (ViewState["lstSkuSites"] != null)
        {
            grdRevisedleadListing.DataSource = (List<ApplyOTIFExclusionBE>)ViewState["lstSkuSites"];
            grdRevisedleadListing.PageIndex = e.NewPageIndex;
            if (Session["OTIFExclusion"] != null)
            {
                Hashtable htOTIFExclusion = (Hashtable)Session["OTIFExclusion"];
                htOTIFExclusion.Remove("PageIndex");
                htOTIFExclusion.Add("PageIndex", e.NewPageIndex);
                Session["OTIFExclusion"] = htOTIFExclusion;
            }
            grdRevisedleadListing.DataBind();
        }

    }

    public void pager_Command(object sender, CommandEventArgs e)
    {
        int currnetPageIndx = Convert.ToInt32(e.CommandArgument);
        pager1.CurrentIndex = currnetPageIndx;
        BindGrid(currnetPageIndx);
    }

    protected void btnExport_Click(object sender, EventArgs e)
    {
        IsGoClicked = true;
        var applyOTIFExclusionBE = new ApplyOTIFExclusionBE();
        var applyOTIFExclusionBAL = new ApplyOTIFExclusionBAL();
        applyOTIFExclusionBE.Action = "GetApplySKUExclusion";
        if (GetQueryStringValue("SiteID") != null && IsGoClicked == false)
        {
            applyOTIFExclusionBE.SiteID = Convert.ToInt32(GetQueryStringValue("SiteID"));
        }
        else
        {
            applyOTIFExclusionBE.SiteID = ucSite.innerControlddlSite.SelectedIndex > 0 ? Convert.ToInt32(ucSite.innerControlddlSite.SelectedValue) : 0;
        }   
      

        try
        {
            applyOTIFExclusionBE.Purchase_order = txtPOSearch.Text;
        }
        catch
        {
            applyOTIFExclusionBE.Purchase_order = null;
        }

        applyOTIFExclusionBE.SelectedVendorIDs = msVendor.SelectedVendorIDs;

        applyOTIFExclusionBE.GridCurrentPageNo = 0;
        applyOTIFExclusionBE.GridPageSize = 0;

        if (!string.IsNullOrEmpty(GetQueryStringValue("Datefrom")) && !string.IsNullOrEmpty(GetQueryStringValue("DateTo")) && IsGoClicked == false)
        {
            applyOTIFExclusionBE.DateFrom = Common.GetYYYY_MM_DD(GetQueryStringValue("Datefrom"));
            applyOTIFExclusionBE.DateTo = Common.GetYYYY_MM_DD(GetQueryStringValue("DateTo"));
            applyOTIFExclusionBE.IsFromExcludedPoPage = true;
        }
        else if (!string.IsNullOrEmpty(GetQueryStringValue("Datefrom")) && IsGoClicked == false)
        {
            applyOTIFExclusionBE.DateFrom = Common.GetYYYY_MM_DD(GetQueryStringValue("Datefrom"));
        }
        else
        {
            applyOTIFExclusionBE.DateFrom = Common.GetYYYY_MM_DD(hdnJSFromDt.Value);
            applyOTIFExclusionBE.DateTo = Common.GetYYYY_MM_DD(hdnJSToDt.Value);
            applyOTIFExclusionBE.IsDateCheck = true;
        }
        //if (!string.IsNullOrEmpty(GetQueryStringValue("Datefrom")))
        //{
        //    applyOTIFExclusionBE.DateFrom = Common.GetYYYY_MM_DD(GetQueryStringValue("Datefrom"));
        //}
        //else 
        //{
        //    applyOTIFExclusionBE.DateFrom = Common.GetYYYY_MM_DD(hdnJSFromDt.Value);
        //    applyOTIFExclusionBE.DateTo = Common.GetYYYY_MM_DD(hdnJSToDt.Value);
        //    applyOTIFExclusionBE.IsDateCheck = true;
        //}

        applyOTIFExclusionBE.SKU = new UP_SKUBE();
         applyOTIFExclusionBE.SKU.OD_SKU_NO = txtSku.Text.Trim();
        applyOTIFExclusionBE.SKU.Direct_SKU = txtCATCode.Text.Trim();


        var lstApplyOTIFExclusion = new List<ApplyOTIFExclusionBE>();
        lstApplyOTIFExclusion = applyOTIFExclusionBAL.GetApplySKUExclusionBAL(applyOTIFExclusionBE);

        if (lstApplyOTIFExclusion.Count > 0 && lstApplyOTIFExclusion != null)
        {
            grdRevisedleadListingExcel.DataSource = lstApplyOTIFExclusion;
            
        }
        else
        {
            grdRevisedleadListingExcel.DataSource = null;
        }

        grdRevisedleadListingExcel.DataBind();
        applyOTIFExclusionBAL = null;
        WebCommon.ExportHideHidden("ApplyOTIFExclusion", grdRevisedleadListingExcel);
    }

    #endregion

    #region Methods ...
    protected void BindGrid(int Page = 1)
    {
        var applyOTIFExclusionBE = new ApplyOTIFExclusionBE();
        var applyOTIFExclusionBAL = new ApplyOTIFExclusionBAL();
        applyOTIFExclusionBE.Action = "GetApplySKUExclusion";
        if (GetQueryStringValue("SiteID") != null && IsGoClicked==false)
        {
            applyOTIFExclusionBE.SiteID =Convert.ToInt32(GetQueryStringValue("SiteID"));
        }
        else
        {
            applyOTIFExclusionBE.SiteID = ucSite.innerControlddlSite.SelectedIndex > 0 ? Convert.ToInt32(ucSite.innerControlddlSite.SelectedValue) : 0;
        }      

        try
        {
            applyOTIFExclusionBE.Purchase_order = txtPOSearch.Text;
        }
        catch
        {
            applyOTIFExclusionBE.Purchase_order = string.Empty;
        }
        applyOTIFExclusionBE.SelectedVendorIDs = msVendor.SelectedVendorIDs;

        applyOTIFExclusionBE.GridCurrentPageNo = Page;
        applyOTIFExclusionBE.GridPageSize = gridPageSize;
        if (!string.IsNullOrEmpty(GetQueryStringValue("Datefrom")) && !string.IsNullOrEmpty(GetQueryStringValue("DateTo")) && IsGoClicked == false)
        {
            applyOTIFExclusionBE.DateFrom = Common.GetYYYY_MM_DD(GetQueryStringValue("Datefrom"));
            applyOTIFExclusionBE.DateTo = Common.GetYYYY_MM_DD(GetQueryStringValue("DateTo"));
            applyOTIFExclusionBE.IsFromExcludedPoPage = true;
            btnBack.Visible = true;
            tblPanel.Visible = false;
            btnAdd.Visible = false;
        }
        else if (!string.IsNullOrEmpty(GetQueryStringValue("Datefrom")) && IsGoClicked == false)
        {
            applyOTIFExclusionBE.DateFrom = Common.GetYYYY_MM_DD(GetQueryStringValue("Datefrom"));
            btnBack.Visible = true;
            tblPanel.Visible = false;
            btnAdd.Visible = false;
        }
        else 
        {
           applyOTIFExclusionBE.DateFrom = Common.GetYYYY_MM_DD(hdnJSFromDt.Value);
            applyOTIFExclusionBE.DateTo = Common.GetYYYY_MM_DD(hdnJSToDt.Value);
            applyOTIFExclusionBE.IsDateCheck = true;
            btnBack.Visible = false;
            tblPanel.Visible = true;
            btnAdd.Visible = true;
        }

        applyOTIFExclusionBE.SKU = new UP_SKUBE();
        applyOTIFExclusionBE.SKU.OD_SKU_NO = txtSku.Text.Trim();
        applyOTIFExclusionBE.SKU.Direct_SKU = txtCATCode.Text.Trim();


        this.SetSession(applyOTIFExclusionBE);

        var lstApplyOTIFExclusion = new List<ApplyOTIFExclusionBE>();
        lstApplyOTIFExclusion = applyOTIFExclusionBAL.GetApplySKUExclusionBAL(applyOTIFExclusionBE);

        if (lstApplyOTIFExclusion.Count > 0 && lstApplyOTIFExclusion != null)
        {
            grdRevisedleadListing.DataSource = lstApplyOTIFExclusion;
            pager1.ItemCount = lstApplyOTIFExclusion[0].TotalRecords;           
            btnExportToExcel.Visible = true;
            txtFromDate.Text = hdnJSFromDt.Value;
            txtToDate.Text = hdnJSToDt.Value;      
      
            if (pager1.ItemCount > gridPageSize)
                pager1.Visible = true;
            else
                pager1.Visible = false;
        }
        else
        {
            txtFromDate.Text = hdnJSFromDt.Value;
            txtToDate.Text = hdnJSToDt.Value;
            grdRevisedleadListing.DataSource = null;
            pager1.Visible = false;
            btnExportToExcel.Visible = false;
        }
        grdRevisedleadListing.PageIndex = Page;
        pager1.CurrentIndex = Page;
        grdRevisedleadListing.DataBind();
        applyOTIFExclusionBAL = null;

       
        ViewState["lstSkuSites"] = lstApplyOTIFExclusion;
    }

    private void SetSession(ApplyOTIFExclusionBE applyOTIFExclusionBE)
    {
        Session.Remove("OTIFExclusion");
        Session["OTIFExclusion"] = null;
        Hashtable htOTIFExclusion = new Hashtable();
        htOTIFExclusion.Add("SelectedSiteId", applyOTIFExclusionBE.SiteID);
        htOTIFExclusion.Add("SelectedPo", applyOTIFExclusionBE.Purchase_order);
        htOTIFExclusion.Add("SelectedVendorID", msVendor.SelectedVendorIDs);
        htOTIFExclusion.Add("SelectedVendorName", msVendor.SelectedVendorName);

        htOTIFExclusion.Add("DateFrom", hdnJSFromDt.Value);
        htOTIFExclusion.Add("DateTo", hdnJSToDt.Value);

        htOTIFExclusion.Add("IsDateCheck", applyOTIFExclusionBE.IsDateCheck);
       
        htOTIFExclusion.Add("PageIndex", applyOTIFExclusionBE.GridCurrentPageNo);

        htOTIFExclusion.Add("SKU", applyOTIFExclusionBE.SKU.OD_SKU_NO);
        htOTIFExclusion.Add("Viking", applyOTIFExclusionBE.SKU.Direct_SKU);     


        Session["OTIFExclusion"] = htOTIFExclusion;
    }

    private void GetSession()
    {
        var applyOTIFExclusionBE = new ApplyOTIFExclusionBE();
        var applyOTIFExclusionBAL = new ApplyOTIFExclusionBAL();

        if (Session["OTIFExclusion"] != null)
        {
            Hashtable htOTIFExclusion = (Hashtable)Session["OTIFExclusion"];


            applyOTIFExclusionBE.SiteID = (htOTIFExclusion.ContainsKey("SelectedSiteId") && htOTIFExclusion["SelectedSiteId"] != null) ? Convert.ToInt32(htOTIFExclusion["SelectedSiteId"].ToString()) : 0;
            ucSite.innerControlddlSite.SelectedValue = htOTIFExclusion["SelectedSiteId"].ToString();

            applyOTIFExclusionBE.Purchase_order = (htOTIFExclusion.ContainsKey("SelectedPo") && htOTIFExclusion["SelectedPo"] != null) ? htOTIFExclusion["SelectedPo"].ToString() : null;
            txtPOSearch.Text = (htOTIFExclusion.ContainsKey("SelectedPo") && htOTIFExclusion["SelectedPo"] != null) ? htOTIFExclusion["SelectedPo"].ToString() : null;

            applyOTIFExclusionBE.SelectedVendorIDs = (htOTIFExclusion.ContainsKey("SelectedVendorID") && htOTIFExclusion["SelectedVendorID"] != null) ? htOTIFExclusion["SelectedVendorID"].ToString() : null;

            applyOTIFExclusionBE.DateFrom = (htOTIFExclusion.ContainsKey("DateFrom") && htOTIFExclusion["DateFrom"] != null) ? Common.GetYYYY_MM_DD(htOTIFExclusion["DateFrom"].ToString()) : null;
            applyOTIFExclusionBE.DateTo = (htOTIFExclusion.ContainsKey("DateTo") && htOTIFExclusion["DateTo"] != null) ? Common.GetYYYY_MM_DD(htOTIFExclusion["DateTo"].ToString()) : null;
           
            applyOTIFExclusionBE.IsDateCheck = (htOTIFExclusion.ContainsKey("IsDateCheck") && htOTIFExclusion["IsDateCheck"] != null) ? Convert.ToBoolean(htOTIFExclusion["IsDateCheck"]) : false;


            txtFromDate.Text = (htOTIFExclusion.ContainsKey("DateFrom") && htOTIFExclusion["DateFrom"] != null) ? htOTIFExclusion["DateFrom"].ToString() : null;
            txtToDate.Text = (htOTIFExclusion.ContainsKey("DateTo") && htOTIFExclusion["DateTo"] != null) ? htOTIFExclusion["DateTo"].ToString() : null;
            hdnJSFromDt.Value = txtFromDate.Text;
            hdnJSToDt.Value = txtToDate.Text;

            applyOTIFExclusionBE.SKU = new BusinessEntities.ModuleBE.Upload.UP_SKUBE(); 
            applyOTIFExclusionBE.SKU.OD_SKU_NO = (htOTIFExclusion.ContainsKey("SKU") && htOTIFExclusion["SKU"] != null) ? htOTIFExclusion["SKU"].ToString() : "";
            applyOTIFExclusionBE.SKU.Direct_SKU = (htOTIFExclusion.ContainsKey("Viking") && htOTIFExclusion["Viking"] != null) ? htOTIFExclusion["Viking"].ToString() : "";
            txtSku.Text = (htOTIFExclusion.ContainsKey("SKU") && htOTIFExclusion["SKU"] != null) ? htOTIFExclusion["SKU"].ToString() : "";
            txtCATCode.Text = (htOTIFExclusion.ContainsKey("Viking") && htOTIFExclusion["Viking"] != null) ? htOTIFExclusion["Viking"].ToString() : "";

            if (htOTIFExclusion["SelectedVendorID"] != null)
            {
                msVendor.SelectedVendorIDs = "";
                msVendor.SelectedVendorName = "";
                msVendor.SelectedVendorIDs = htOTIFExclusion["SelectedVendorID"].ToString();
                msVendor.SelectedVendorName = htOTIFExclusion["SelectedVendorName"].ToString();
                msVendor.setVendorsOnPostBack();
            }


            applyOTIFExclusionBE.GridCurrentPageNo = Convert.ToInt32(htOTIFExclusion["PageIndex"].ToString());
            applyOTIFExclusionBE.GridPageSize = gridPageSize;


            applyOTIFExclusionBE.Action = "GetApplySKUExclusion";
            var lstApplyOTIFExclusion = new List<ApplyOTIFExclusionBE>();
            lstApplyOTIFExclusion = applyOTIFExclusionBAL.GetApplySKUExclusionBAL(applyOTIFExclusionBE);


            if (lstApplyOTIFExclusion != null && lstApplyOTIFExclusion.Count > 0)
            {
                grdRevisedleadListing.DataSource = lstApplyOTIFExclusion;
                pager1.ItemCount = lstApplyOTIFExclusion[0].TotalRecords;               
                btnExportToExcel.Visible = true;
                if (pager1.ItemCount > gridPageSize)
                    pager1.Visible = true;
                else
                    pager1.Visible = false;
            }
            else
            {
                grdRevisedleadListing.DataSource = null;
                pager1.Visible = false;
                btnExportToExcel.Visible = false; 
            }

            grdRevisedleadListing.DataBind();
            pager1.CurrentIndex = Convert.ToInt32(htOTIFExclusion["PageIndex"].ToString());
            if (grdRevisedleadListing.Rows.Count > 0)
                btnExportToExcel.Visible = true;

            applyOTIFExclusionBAL = null;
            ViewState["lstSkuSites"] = lstApplyOTIFExclusion;
        }
    }


    public override BusinessEntities.BaseBe[] SubSort(GridViewSortEventArgs e)
    {
        int pageindex = pager1.CurrentIndex;
        BindGrid(pageindex);
        return Utilities.GenericListHelper<ApplyOTIFExclusionBE>.SortList((List<ApplyOTIFExclusionBE>)ViewState["lstSkuSites"], e.SortExpression, e.SortDirection).ToArray();
    }

    private void BindGridOnSession()
    {
        if (Session["OTIFExclusion"] != null)
        {
            Hashtable htOTIFExclusion = (Hashtable)Session["OTIFExclusion"];
            var sessionPageIndex = Convert.ToInt32(htOTIFExclusion["PageIndex"].ToString());

            int pageindex = pager1.CurrentIndex;
            if (sessionPageIndex != pageindex || pageindex == 1)
            {
                // BindGrid(pageindex);
            }
        }
        //else
        //{
        //    BindGrid();
        //}
    }

    #endregion
}
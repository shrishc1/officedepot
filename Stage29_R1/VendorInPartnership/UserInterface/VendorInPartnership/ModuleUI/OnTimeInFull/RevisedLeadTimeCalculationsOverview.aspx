﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    CodeFile="RevisedLeadTimeCalculationsOverview.aspx.cs" Inherits="RevisedLeadTimeCalculationsOverview" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="../../CommonUI/UserControls/ucAddButton.ascx" TagName="ucAddButton"
    TagPrefix="cc2" %>
<%@ Register Src="../../CommonUI/UserControls/ucCountry.ascx" TagName="ucCountry"
    TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript">
        $(document).ready(function () {
            $("body").keypress(function (e) {
                if (e.which == 13 || e.keyCode == 13) {
                    $('#<%=btnSearch.ClientID %>').click();
                    return false;
                }
            });
        });

        $(document).ready(function () {
            // HideShowReportView();
            var fullDate = new Date();
            var twoDigitMonth = ((fullDate.getMonth().length + 1) === 1) ? (fullDate.getMonth() + 1) : '0' + (fullDate.getMonth() + 1);
            var currentDate = fullDate.getDate() + "/" + twoDigitMonth + "/" + fullDate.getFullYear();
            $('#txtToDate,#txtFromDate').datepicker({ showOtherMonths: true,
                selectOtherMonths: true, changeMonth: true, changeYear: true,
                minDate: new Date(2013, 0, 1),
                yearRange: '2013:+100',
                dateFormat: 'dd/mm/yy'
            });


        });
        function setValue1(target) {
            document.getElementById('<%=hdnJSFromDt.ClientID %>').value = target.value;
        }

        function setValue2(target) {
            document.getElementById('<%=hdnJSToDt.ClientID %>').value = target.value;
        }


    </script>
    <h2>
        <cc1:ucLabel ID="lblRevisedDueDate" runat="server" Text="Revised Lead Time Calculations"></cc1:ucLabel>
    </h2>
     <asp:HiddenField ID="hdnJSFromDt" runat="server" />
    <asp:HiddenField ID="hdnJSToDt" runat="server" />
    <div class="button-row">
        <cc2:ucAddButton ID="btnAdd" runat="server" NavigateUrl="RevisedLeadTimeCalculationsEdit.aspx" />
        <cc1:ucButton ID="btnExportToExcel" runat="server" CssClass="exporttoexcel" OnClick="btnExport_Click"
            Width="109px" Height="20px" Visible="false" />
    </div>
 
    <table width="100%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
        <tr>
            <td style="font-weight: bold; width: 2%">
                <cc1:ucLabel ID="lblCountry" runat="server" Text="Country"></cc1:ucLabel>
            </td>
            <td style="font-weight: bold; width: 1%">
                :
            </td>
            <td style="width: 12%">
                <uc1:ucCountry ID="ddlCountry" runat="server" Width="150px" />
            </td> 
             <td style="width: 3% ;font-weight: bold;">
                 <cc1:ucLabel ID="lblSkuWithHash" runat="server"></cc1:ucLabel>
             </td>
             <td style=" width: 1%; font-weight: bold;">
                :
             </td>
               <td style="width: 16%; font-weight: bold;">
                   <cc1:ucTextbox ID="txtSku" Width="150px" runat="server"></cc1:ucTextbox>
             </td>
        </tr>
        <tr>
            <td style="font-weight: bold;">
                <cc1:ucLabel ID="lblPoNo" runat="server" Text="PO #"></cc1:ucLabel>
            </td>
            <td style="font-weight: bold;">
                :
            </td>
            <td style="font-weight: bold;">
                
                <cc1:ucTextbox ID="txtPOSearch" runat="server" Width="146px"></cc1:ucTextbox>
            </td>
            <td style="font-weight: bold;">
                <cc1:ucLabel ID="lblCATCode" runat="server"></cc1:ucLabel>
            </td>
              <td style="font-weight: bold;">
                  :
              </td>
              <td style="font-weight: bold;">
                   <cc1:ucTextbox ID="txtCATCode" Width="150px" runat="server"></cc1:ucTextbox>
              </td>
        </tr>
        <tr>
        <td  style="font-weight: bold;  text-align: left">
            <cc1:ucLabel ID="lblDateFrom" runat="server" Text="Date From" Width="80px"  ></cc1:ucLabel>
        </td>
         <td style="font-weight: bold;">
                :
            </td>
        <td style="font-weight: bold;  text-align: left">
         <cc1:ucTextbox ID="txtFromDate" ClientIDMode="Static" runat="server" onchange="setValue1(this)"
                  ReadOnly="True" Width="100px" />
           &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
           <cc1:ucLabel ID="lblDateTo" runat="server" Text="Date To :" Width="60px"  ></cc1:ucLabel>
                <cc1:ucTextbox ID="txtToDate"  ClientIDMode="Static" runat="server" onchange="setValue2(this)"
                    ReadOnly="True" Width="100px" /> 
            </td> 
            <td align="right" colspan="3">
                <cc1:ucButton ID="btnSearch" runat="server" CausesValidation="true" OnClick="btnGo_Click"></cc1:ucButton>
            </td>                                                                         
                                                 
        </tr>        

    </table>
    <div style="width: 100%; overflow: auto" id="divPrint">
        <table cellspacing="1" cellpadding="0" border="0" align="center" width="100%">
            <tr>
                <td>
                    <cc1:ucGridView ID="grdRevisedleadListing" OnPageIndexChanging="grdRevisedleadListing_PageIndexChanging"
                        Width="100%" runat="server" CssClass="grid" OnSorting="SortGrid" AllowSorting="true"
                        EmptyDataText="No Records Found" EmptyDataRowStyle-ForeColor="Red" EmptyDataRowStyle-HorizontalAlign="Center">
                        <Columns>
                            <asp:BoundField HeaderText="Country" DataField="CountryName" SortExpression="CountryName">
                                <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="Purchase Order" SortExpression="Purchase_order">
                                <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                <ItemTemplate>
                                    <asp:HyperLink ID="hpPurchaseOrder" runat="server" Text='<%# Eval("Purchase_order")  %>'
                                        NavigateUrl='<%# EncryptQuery("RevisedLeadTimeCalculationsEdit.aspx?PurchaseOrderID="+Eval("PurchaseOrderID")+"&Warehouse="+Eval("Warehouse")+"&PurchaseOrderNo="+Eval("Purchase_order")+"&SiteID="+Eval("SiteId")+"&OrderRaised=" +Eval("Order_raised")) %>'></asp:HyperLink>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="OD Product Code" DataField="ProductDescription" SortExpression="ProductDescription">
                                <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField HeaderText="Original Due Date" DataField="OriginalDueDate" SortExpression="OriginalDueDate">
                                <HeaderStyle Width="100px" HorizontalAlign="center" />
                                <ItemStyle HorizontalAlign="center" />
                            </asp:BoundField>
                            <asp:BoundField HeaderText="Revised Date" DataField="RevisedDueDate" DataFormatString="{0:dd/MM/yyyy}"
                                SortExpression="RevisedDueDate">
                                <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField HeaderText="Applied By" DataField="AppliedBy" 
                                SortExpression="AppliedBy">
                                <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                             <asp:BoundField HeaderText="Applied On" DataField="RevisedDueDateAppliedOn" 
                                SortExpression="RevisedDueDateAppliedOn">
                                <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                             <asp:BoundField HeaderText="Comments" DataField="Reason" SortExpression="Reason">
                                <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                           
                        </Columns>
                    </cc1:ucGridView>
                    <cc1:PagerV2_8 ID="pager1" runat="server" OnCommand="pager_Command" GenerateGoToSection="true">
                    </cc1:PagerV2_8>
                </td>
            </tr>
            <tr>
                <td>
                    <cc1:ucGridView ID="grdRevisedleadListingExcel" Width="100%" runat="server" CssClass="grid"
                        Visible="false">
                        <Columns>
                            <asp:BoundField HeaderText="Country" DataField="CountryName" SortExpression="CountryName">
                                <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="Purchase Order" SortExpression="Purchase_order">
                                <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                <ItemTemplate>
                                    <asp:HyperLink ID="hpPurchaseOrder" runat="server" Text='<%# Eval("Purchase_order")  %>'
                                        NavigateUrl='<%# EncryptQuery("RevisedLeadTimeCalculationsEdit.aspx?PurchaseOrderID="+Eval("PurchaseOrderID")) %>'></asp:HyperLink>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="OD Product Code" DataField="ProductDescription" SortExpression="ProductDescription">
                                <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                             <asp:BoundField HeaderText="Original Due Date" DataField="OriginalDueDate" SortExpression="OriginalDueDate">
                                <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField HeaderText="Revised Date" DataField="RevisedDueDate" DataFormatString="{0:dd/MM/yyyy}"
                                SortExpression="RevisedDueDate">
                                <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                             <asp:BoundField HeaderText="Applied By" DataField="AppliedBy" 
                                SortExpression="AppliedBy">
                                <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                             <asp:BoundField HeaderText="Comments" DataField="Reason" SortExpression="Reason">
                                <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                        </Columns>
                    </cc1:ucGridView>
                </td>
            </tr>
        </table>
    </div>
   
</asp:Content>

﻿using BusinessEntities.ModuleBE.Upload;
using BusinessLogicLayer.ModuleBAL.Upload;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Utilities;
using WebUtilities;

public partial class ModuleUI_OnTimeInFull_DeletedOtifExclusionRevisedView : CommonPage
{
   
    protected void Page_InIt(object sender, EventArgs e)
    {
        ucSite.CurrentPage = this;
        ucSite.IsAllRequired = true;
    }

    public override void SitePost_Load()
    {

        if (!IsPostBack)
        {
            ucSite.innerControlddlSite.SelectedIndex = 0;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if(!IsPostBack)
        {
            txtFromDate.Text = DateTime.Now.AddDays(-DateTime.Now.Day + 1).ToString("dd/MM/yyyy");
            txtToDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
            hdnJSFromDt.Value = txtFromDate.Text;
            hdnJSToDt.Value = txtToDate.Text;
        }
    }

    protected void btnGo_Click(object sender, EventArgs e)
    {
        BindGrid(1);
        msVendor.setVendorsOnPostBack();
    }
    protected void gvDisplayRecords_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        if (ViewState["lstSites"] != null)
        {
            gvDisplayRecords.PageIndex = e.NewPageIndex;
            gvDisplayRecords.DataSource = (DataTable)ViewState["lstSites"];
            gvDisplayRecords.DataBind();           
        }
    }
    

    protected void BindGrid(int Page = 1)
    {
        Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE = new Up_PurchaseOrderDetailBE();
        UP_PurchaseOrderDetailBAL oUP_PurchaseOrderDetailBAL = new UP_PurchaseOrderDetailBAL();

        if (rdoRevisedDate.Checked)
        {
            oUp_PurchaseOrderDetailBE.Action = "GetDeletedRevisedDateRecords";
        }
        else if(rdoApplyHit.Checked)
        {
            oUp_PurchaseOrderDetailBE.Action = "GetDeletedApplyOtifHitRecords";
        }
        else if (rdoApplyExclusion.Checked)
        {
            oUp_PurchaseOrderDetailBE.Action = "GetDeltedApplyExclusionRecords";
        }


        oUp_PurchaseOrderDetailBE.SiteID = ucSite.innerControlddlSite.SelectedIndex > 0 ? Convert.ToInt32(ucSite.innerControlddlSite.SelectedValue) : 0;

        try
        {
            oUp_PurchaseOrderDetailBE.Purchase_order = txtPOSearch.Text;
        }
        catch
        {
            oUp_PurchaseOrderDetailBE.Purchase_order = "0";
        }
        
        oUp_PurchaseOrderDetailBE.DateFrom = Common.GetYYYY_MM_DD(hdnJSFromDt.Value);
        oUp_PurchaseOrderDetailBE.DateTo = Common.GetYYYY_MM_DD(hdnJSToDt.Value);
        oUp_PurchaseOrderDetailBE.SelectedVendorIDs = msVendor.SelectedVendorIDs;

        oUp_PurchaseOrderDetailBE.SKU = new UP_SKUBE();
        oUp_PurchaseOrderDetailBE.SKU.Direct_SKU = txtSku.Text.Trim();
        oUp_PurchaseOrderDetailBE.VikingSku = txtCATCode.Text.Trim();        
       
        DataSet ds =  oUP_PurchaseOrderDetailBAL.GetDeletedOtifExclusionRevisedBAL(oUp_PurchaseOrderDetailBE);


        if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
        {
            gvDisplayRecords.DataSource = ds.Tables[0];
            gvDisplayRecords.DataBind();
            grdBindExport.DataSource = ds.Tables[0];
            grdBindExport.DataBind();
            btnExportToExcel.Visible = true;            
        }
        else
        {
            gvDisplayRecords.DataSource = null;
            gvDisplayRecords.DataBind();
            btnExportToExcel.Visible = false;
        }

        txtFromDate.Text = hdnJSFromDt.Value;
        txtToDate.Text = hdnJSToDt.Value;        

        

        oUP_PurchaseOrderDetailBAL = null;

        ViewState["lstSites"] = ds.Tables[0];
    }

    protected void btnExport_Click(object sender, EventArgs e)
    {
        //Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE = new Up_PurchaseOrderDetailBE();
        //UP_PurchaseOrderDetailBAL oUP_PurchaseOrderDetailBAL = new UP_PurchaseOrderDetailBAL();

        //if (rdoRevisedDate.Checked)
        //{
        //    oUp_PurchaseOrderDetailBE.Action = "GetDeletedRevisedDateRecords";
        //}
        //else if (rdoApplyHit.Checked)
        //{
        //    oUp_PurchaseOrderDetailBE.Action = "GetDeletedApplyHitRecords";
        //}
        //else if (rdoApplyExclusion.Checked)
        //{
        //    oUp_PurchaseOrderDetailBE.Action = "GetDeltedApplyExclusionRecords";
        //}

        //oUp_PurchaseOrderDetailBE.SiteID = ucSite.innerControlddlSite.SelectedIndex > 0 ? Convert.ToInt32(ucSite.innerControlddlSite.SelectedValue) : 0;

        //try
        //{
        //    oUp_PurchaseOrderDetailBE.Purchase_order = txtPOSearch.Text;
        //}
        //catch
        //{
        //    oUp_PurchaseOrderDetailBE.Purchase_order = "0";
        //}

        //oUp_PurchaseOrderDetailBE.GridCurrentPageNo = 0;
        //oUp_PurchaseOrderDetailBE.GridPageSize = 0;

        //oUp_PurchaseOrderDetailBE.DateFrom = Common.GetYYYY_MM_DD(hdnJSFromDt.Value);
        //oUp_PurchaseOrderDetailBE.DateTo = Common.GetYYYY_MM_DD(hdnJSToDt.Value);

        //oUp_PurchaseOrderDetailBE.SKU = new UP_SKUBE();
        //oUp_PurchaseOrderDetailBE.SKU.Direct_SKU = txtSku.Text.Trim();
        //oUp_PurchaseOrderDetailBE.VikingSku = txtCATCode.Text.Trim();

        //DataSet ds = oUP_PurchaseOrderDetailBAL.GetDeletedOtifExclusionRevisedBAL(oUp_PurchaseOrderDetailBE);

        //if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
        //{
        //    gvDisplayRecords.DataSource = ds;
            
        //}
        //else
        //{
        //    gvDisplayRecords.DataSource = null;
        //}

        //gvDisplayRecords.DataBind();

        //oUP_PurchaseOrderDetailBAL = null;

        if (rdoRevisedDate.Checked)
        {
            WebCommon.ExportHideHidden("DeletedRevisedDatesDetail", grdBindExport, null, false, true);            
        }
        else if (rdoApplyHit.Checked)
        {
            WebCommon.ExportHideHidden("DeletedApplyOTIFHistDetail", grdBindExport, null, false, true);            
        }
        else if (rdoApplyExclusion.Checked)
        {
            WebCommon.ExportHideHidden("DeletedApplyOTIFExclusionDetail", grdBindExport, null, false, true);            
        }

       
    }
}
﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    AutoEventWireup="true" CodeFile="ApplyOTIFExclusionOverview.aspx.cs" Inherits="ApplyOTIFExclusionOverview" %>

<%@ Register Src="~/CommonUI/UserControls/ucSeacrhVendor.ascx" TagName="ucSeacrhVendor"
    TagPrefix="cc3" %>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="../../CommonUI/UserControls/ucAddButton.ascx" TagName="ucAddButton"
    TagPrefix="cc2" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectVendor.ascx" TagName="MultiSelectVendor"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/ucSite.ascx" TagName="ucSite" TagPrefix="cc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript">
        $(document).ready(function () {
            $("body").keypress(function (e) {
                if (e.which == 13 || e.keyCode == 13) {
                    $('#<%=btnSearch.ClientID %>').click();
                    return false;
                }
            });
        });

        $(document).ready(function () {
            // HideShowReportView();
            var fullDate = new Date();
            var twoDigitMonth = ((fullDate.getMonth().length + 1) === 1) ? (fullDate.getMonth() + 1) : '0' + (fullDate.getMonth() + 1);
            var currentDate = fullDate.getDate() + "/" + twoDigitMonth + "/" + fullDate.getFullYear();
            $('#txtToDate,#txtFromDate').datepicker({ showOtherMonths: true,
                selectOtherMonths: true, changeMonth: true, changeYear: true,
                minDate: new Date(2013, 0, 1),
                yearRange: '2013:+100',
                dateFormat: 'dd/mm/yy'
            });


        });
        function setValue1(target) {
            document.getElementById('<%=hdnJSFromDt.ClientID %>').value = target.value;           
        }

        function setValue2(target) {
            document.getElementById('<%=hdnJSToDt.ClientID %>').value = target.value;
        }       

    </script>
    <asp:ScriptManager ID="SM1" runat="server">
    </asp:ScriptManager>
    <h2>
        <cc1:ucLabel ID="lblApplyOTIFExclusion" runat="server" Text="Apply OTIF Exclusion"></cc1:ucLabel>
    </h2>
     <asp:HiddenField ID="hdnJSFromDt" runat="server" />
    <asp:HiddenField ID="hdnJSToDt" runat="server" />
    <div class="button-row">
        <cc2:ucAddButton ID="btnAdd" runat="server" NavigateUrl="ApplyOTIFExclusionEdit.aspx" />
        <cc1:ucButton ID="btnExportToExcel" runat="server" CssClass="exporttoexcel" OnClick="btnExport_Click"
            Width="109px" Height="20px" Visible="false"/>
    </div>
    <table width="100%" cellspacing="1" cellpadding="0" border="0" align="center" class="top-settings" id="tblPanel" runat="server">
        <tr>
            <td style="font-weight: bold; width: 5%">
                <cc1:ucLabel ID="lblSitePrefixName" runat="server" Text="Site"></cc1:ucLabel>
            </td>
            <td style="font-weight: bold; width: 1%">
                :
            </td>
            <td style="width: 10%;font-weight: bold;">
                &nbsp;<cc2:ucSite ID="ucSite" runat="server" />
                </td>
           <%-- &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; 
                 &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;&nbsp; &nbsp; &nbsp;
                  &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;--%>                 
            
                  <td align="center">
                  <cc1:ucLabel ID="lblSkuWithHash" runat="server" style="font-weight: bold;"></cc1:ucLabel> 
                  </td>
                  <td style="font-weight: bold;">
                        :
                     </td>
                    <td>
                   <cc1:ucTextbox ID="txtSku" Width="150px" runat="server"></cc1:ucTextbox>     
                     </td>        
        </tr>
        <tr>
            <td style="font-weight: bold;">
                <cc1:ucLabel ID="lblPoNo" runat="server" Text="PO #"></cc1:ucLabel>
            </td>
            <td style="font-weight: bold;">
                :
            </td>
            <td style="font-weight: bold; padding-left: 3px">
               <cc1:ucTextbox ID="txtPOSearch" runat="server" Width="170px"></cc1:ucTextbox>
               </td>
                <%-- &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;          
                     &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;&nbsp;&nbsp; &nbsp; &nbsp;
                      &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;&nbsp;&nbsp; &nbsp; &nbsp;--%>
                      <td align="center">
                <cc1:ucLabel ID="lblCATCode" runat="server" style="font-weight: bold;"></cc1:ucLabel>
                </td>
                 <td style="font-weight: bold;">
                        :
                     </td>
                     <td>
                      <cc1:ucTextbox ID="txtCATCode" Width="150px" runat="server"></cc1:ucTextbox>
                    </td> 
        </tr>
        <tr>
            <td style="font-weight: bold;">
                <cc1:ucLabel ID="lblVendorNo" runat="server" Text="Vendor No"></cc1:ucLabel>
            </td>
            <td style="font-weight: bold;">
                :
            </td>
            <td style="font-weight: bold;" colspan="4">
                <cc1:MultiSelectVendor runat="server" ID="msVendor" />
            </td>
        </tr>
        <tr>
          <td  style="font-weight: bold; text-align:left">
            <cc1:ucLabel ID="lblDateFrom" runat="server" Text="Date From" Width="65px" ></cc1:ucLabel>
                
         </td> 
         <td style="font-weight: bold;">
                :
            </td>
         <td colspan="2">
         <cc1:ucTextbox ID="txtFromDate" ClientIDMode="Static" runat="server" onchange="setValue1(this)"
                    ReadOnly="True" Width="100px" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                     <cc1:ucLabel ID="lblDateTo" runat="server" Text="Date To" Width="48px"  ></cc1:ucLabel>
                 <cc1:ucTextbox ID="txtToDate"  ClientIDMode="Static" runat="server" onchange="setValue2(this)"
                    ReadOnly="True" Width="100px" /> 
         </td>                                      
        
            <td align="center" colspan="2">
                <cc1:ucButton ID="btnSearch" runat="server" CausesValidation="true" OnClick="btnGo_Click"></cc1:ucButton>
            </td>
        </tr>
    </table>

     
    <div style="width: 100%; overflow: auto" id="divPrint" >
        <table cellspacing="1" cellpadding="0" border="0" align="center" width="100%">
            <tr>
                <td >
                    <cc1:ucGridView ID="grdRevisedleadListing" OnPageIndexChanging="grdRevisedleadListing_PageIndexChanging"
                        Width="100%" runat="server" CssClass="grid" OnSorting="SortGrid" AllowSorting="true"
                        EmptyDataText="No Records Found" EmptyDataRowStyle-ForeColor="Red" EmptyDataRowStyle-HorizontalAlign="Center">
                        <Columns>
                            <asp:BoundField HeaderText="Country" DataField="CountryName" SortExpression="CountryName">
                                <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField HeaderText="Site" DataField="SiteName" SortExpression="SiteName">
                                <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField HeaderText="Vendor Name" DataField="VendorName" SortExpression="VendorName">
                                <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="Purchase Order" SortExpression="Purchase_order">
                                <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                <ItemTemplate>
                                    <asp:HyperLink ID="hpPurchaseOrder" runat="server" Text='<%# Eval("Purchase_order")  %>'
                                        NavigateUrl='<%# EncryptQuery("ApplyOTIFExclusionEdit.aspx?PurchaseOrderID="+Eval("Purchase_order") +"&OrderRaised=" +Eval("Order_raised") +"&Warehouse=" +Eval("Warehouse") +"&ExclusionID=" +Eval("ApplyExclusionID")+"&SiteID=" +Eval("SiteID") +"&UserName="+Eval("UserName") +"&DateApplied="+Eval("DateApplied") +"&VendorID="+Eval("VendorID")) %>'></asp:HyperLink>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="OD Product Code" DataField="ProductDescription" SortExpression="ProductDescription">
                                <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                             <asp:TemplateField HeaderText="Original Due Date" SortExpression="OriginalDueDate">
                                            <HeaderStyle Width="100px" HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <cc1:ucLabel ID="ltOriginalDueDate" runat="server" Text='<%# Eval("OriginalDueDate") != null ?  Convert.ToDateTime(Eval("OriginalDueDate").ToString()).ToString("dd/MM/yyyy") : ""%>'></cc1:ucLabel>
                                            </ItemTemplate>
                            </asp:TemplateField>
                               
                            <asp:BoundField HeaderText="Who Applied The Hit" DataField="WhoAppliedTheOtifExclusion" 
                                SortExpression="WhoAppliedTheOtifExclusion">
                                <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField HeaderText="Date Applied" DataField="DateApplied" DataFormatString="{0:dd/MM/yyyy}"
                                SortExpression="DateApplied">
                                <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField HeaderText="Applied By" DataField="UserName" 
                                SortExpression="UserName">
                                <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField HeaderText="Reason Why" DataField="Reason" DataFormatString="{0:dd/MM/yyyy}"
                                SortExpression="Reason">
                                <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField HeaderText="Order Raised" DataField="OrderRaised" DataFormatString="{0:dd/MM/yyyy}"
                                Visible="false">
                                <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                        </Columns>
                    </cc1:ucGridView>
                    <cc1:PagerV2_8 ID="pager1" runat="server" OnCommand="pager_Command" GenerateGoToSection="true">
                    </cc1:PagerV2_8>
                </td>
            </tr>
            <tr>
                <td>
                    <cc1:ucGridView ID="grdRevisedleadListingExcel" Width="100%" runat="server" CssClass="grid"
                        Visible="false">
                        <Columns>
                            <asp:BoundField HeaderText="Country" DataField="CountryName" SortExpression="CountryName">
                                <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField HeaderText="Site" DataField="SiteName" SortExpression="SiteName">
                                <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField HeaderText="Vendor Name" DataField="VendorName" SortExpression="VendorName">
                                <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="Purchase Order" SortExpression="Purchase_order">
                                <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                <ItemTemplate>
                                    <asp:HyperLink ID="hpPurchaseOrder" runat="server" Text='<%# Eval("Purchase_order")  %>'
                                        NavigateUrl='<%# EncryptQuery("ApplyOTIFExclusionEdit.aspx?PurchaseOrderID="+Eval("Purchase_order") +"&OrderRaised=" +Eval("Order_raised") +"&ExclusionID=" +Eval("ApplyExclusionID")) %>'></asp:HyperLink>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="OD Product Code" DataField="ProductDescription" SortExpression="ProductDescription">
                                <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                              <asp:TemplateField HeaderText="Original Due Date" SortExpression="OriginalDueDate" >
                                            <HeaderStyle Width="100px" HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <cc1:ucLabel ID="lblOriginalDueDate" runat="server" Text='<%# Eval("OriginalDueDate") != null ?  Convert.ToDateTime(Eval("OriginalDueDate").ToString()).ToString("dd/MM/yyyy") : ""%>'></cc1:ucLabel>
                                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Who Applied The Hit" DataField="WhoAppliedTheOtifExclusion" DataFormatString="{0:dd/MM/yyyy}"
                                SortExpression="WhoAppliedTheOtifExclusion">
                                <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField HeaderText="Date Applied" DataField="DateApplied" DataFormatString="{0:dd/MM/yyyy}"
                                SortExpression="DateApplied">
                                <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                               <asp:BoundField HeaderText="Applied By" DataField="UserName" 
                                SortExpression="UserName">
                                <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField HeaderText="Reason Why" DataField="Reason" DataFormatString="{0:dd/MM/yyyy}"
                                SortExpression="Reason">
                                <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            
                        </Columns>
                    </cc1:ucGridView>
                </td>
            </tr>
        </table>
    </div>
      <div class="button-row">
            <asp:Button ID="btnBack" Text="Back" runat="server" Visible="false" OnClick="btnBack_Click" />
        </div>
</asp:Content>

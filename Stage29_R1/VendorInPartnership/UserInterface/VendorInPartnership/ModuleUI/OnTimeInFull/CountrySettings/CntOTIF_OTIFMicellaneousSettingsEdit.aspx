﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    CodeFile="CntOTIF_OTIFMicellaneousSettingsEdit.aspx.cs" Inherits="CntOTIF_OTIFMicellaneousSettingsEdit" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="../../../CommonUI/UserControls/ucSeacrhnSelectVendor.ascx" TagName="ucSeacrhnSelectVendor"
    TagPrefix="cc4" %>
<%@ Register Src="~/CommonUI/UserControls/ucCountry.ascx" TagName="ucCountry" TagPrefix="uc1" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <script language="javascript" type="text/javascript">

         var _oldonerror = window.onerror;
         window.onerror = function (errorMsg, url, lineNr) { return true; };


         function checkVendorSelected(source, args) {
             if (document.getElementById('<%=ucSeacrhVendor1.FindControl("lstSelectedVendor").ClientID%>') != null) {
                 var lstSelectedProductCode = document.getElementById('<%=ucSeacrhVendor1.FindControl("lstSelectedVendor").ClientID%>');

                 if (lstSelectedProductCode.options.length < 1) {
                    
                     args.IsValid = false;

                 }
             }
         }

         </script>

    <h2>
        <cc1:ucLabel ID="lblOTIFMicellaneousSettings" runat="server" Text="OTIF Miscellaneous Settings"></cc1:ucLabel>
    </h2>

    <div class="right-shadow">
        <div class="formbox">
            <table  id="trAdd" runat="server" width="100%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                <tr>
                    <td width="1%"></td>
                    <td style="font-weight: bold; width: 10%">
                        <cc1:ucLabel ID="lblCountry" runat="server" Text="Country"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold; width: 5%">
                        <cc1:ucLabel ID="UcLabel17" runat="server">:</cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold; width: 84" colspan="3">
                        <uc1:ucCountry ID="ucCountry" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td colspan="5">
                     <cc4:ucSeacrhnSelectVendor ID="ucSeacrhVendor1" runat="server" />
                      <%--<asp:CustomValidator ID="cusvNoSelectedVendor" runat="server" ClientValidationFunction="checkVendorSelected"
                      ValidationGroup="Save" Display="None"></asp:CustomValidator>--%>

                    </td>
                </tr>
                </table>
               <table id="trEdit" runat="server" width="90%" cellspacing="5" cellpadding="0" class="form-table">
               <tr>
                    <td width="1%"></td>
                    <td style="font-weight: bold; width: 7%">
                    <cc1:ucLabel ID="lblCountry_1" runat="server" Text="Country"></cc1:ucLabel>
                    </td>
               
                    <td style="font-weight: bold; width: 1%"> <cc1:ucLabel ID="UcLabel1" runat="server">:</cc1:ucLabel></td>
                    <td style="font-weight: bold; width: 30%">  <cc1:ucLabel ID="lblCountryEdit" runat="server" ></cc1:ucLabel></td>
                    <td style="font-weight: bold; width: 10%">
                    <cc1:ucLabel ID="lblVendorName" runat="server" Text="Vendor Name"></cc1:ucLabel>
                    </td >
            
                    <td style="font-weight: bold; width:1%">    <cc1:ucLabel ID="UcLabel2" runat="server">:</cc1:ucLabel></td>
                    <td style="font-weight: bold; width: 50%">  <cc1:ucLabel ID="lblVendorNameEdit" runat="server" ></cc1:ucLabel></td>
               </tr>
               
               </table>
                <table width="90%" cellspacing="5" cellpadding="0" class="form-table">
                <tr>
                    <td colspan="5">
                        <cc1:ucPanel ID="pnlOTIFCommunicationRules" runat="server" GroupingText="OTIF Communication Rules"
                            CssClass="fieldset-form">
                            <table width="90%" cellspacing="5" cellpadding="0" class="form-table">
                                <tr>
                                    <td style="font-weight: bold;" colspan="2">
                                        <cc1:ucLabel ID="lblPreferredFrequency" runat="server" Text="Please select preferred frequency for vendor"></cc1:ucLabel>
                                    </td>
                                </tr>                                <tr class="nobold">
                                  
                                    <td style="width: 100px;">
                                        <cc1:ucRadioButton ID="rdoMonthly" runat="server" GroupName="LeadTime" />
                                    </td>
                                    
                                    <td style="width: 100px;">
                                        <cc1:ucRadioButton ID="rdoNever" runat="server" GroupName="LeadTime" />
                                    </td>
                                </tr>
                            </table>
                        </cc1:ucPanel>
                    </td>
                </tr>
                <tr>
                    <td colspan="5">
                        <cc1:ucPanel ID="pnlLineLevelPenaltyCharge" runat="server" GroupingText="Line Level Penalty Charge"
                            CssClass="fieldset-form">
                            <table width="99%" cellspacing="5" cellpadding="0" class="form-table">
                                <tr>
                                    <td>
                                        <cc1:ucPanel ID="pnlAbsolute" runat="server" GroupingText="Absolute" CssClass="fieldset-form">
                                            <table width="90%" cellspacing="5" cellpadding="0" class="form-table">
                                                <tr>
                                                    <td style="font-weight: bold;width: 47%">
                                                        <cc1:ucLabel ID="lblOTIFRate_1" runat="server" Text="lblOTIFRate_1"></cc1:ucLabel>
                                                    </td>
                                                    <td style="font-weight: bold; width:2%">
                                                        <cc1:ucLabel ID="UcLabel3" runat="server">:</cc1:ucLabel>
                                                    </td>
                                                    <td style="font-weight: bold; width:8%">
                                                        <cc1:ucTextbox ID="txtOTIFRate_1" runat="server" Width="40px" MaxLength="6" onkeyup="AllowDecimalOnly(this);"></cc1:ucTextbox>
                                                       
                                                        <asp:RegularExpressionValidator ID="revAbsolutePenaltyBelowPercentage" ValidationExpression="^\d{0,8}(\.\d{0,2})?$"
                                                            runat="server" ControlToValidate="txtOTIFRate_1" Display="None" ValidationGroup="Save"></asp:RegularExpressionValidator>
                                                    </td>
                                                    <td style="width:43%" align="left">%</td>
                                                </tr>
                                                <tr>
                                                    <td style="font-weight: bold;">
                                                        <cc1:ucLabel ID="lblPanaltyValue_1" runat="server" Text="lblPanaltyValue_1"></cc1:ucLabel>
                                                    </td>
                                                    <td style="font-weight: bold; width: 5px">
                                                        <cc1:ucLabel ID="UcLabel5" runat="server">:</cc1:ucLabel>
                                                    </td>
                                                    <td style="font-weight: bold;">
                                                        <cc1:ucTextbox ID="txtPanaltyValue_1" runat="server" Width="40px" MaxLength="6" onkeyup="AllowDecimalOnly(this);"></cc1:ucTextbox>
                                                          <asp:RegularExpressionValidator ID="revAbsoluteAppliedPenaltyPercentage" ValidationExpression="^\d{0,8}(\.\d{0,2})?$"
                                                              runat="server" ControlToValidate="txtPanaltyValue_1" Display="None" ValidationGroup="Save"></asp:RegularExpressionValidator>
                                                    </td>
                                                    <td style="width:43%" align="left">%</td>
                                                </tr>
                                            </table>
                                        </cc1:ucPanel>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <cc1:ucPanel ID="pnlTolerated" runat="server" GroupingText="Tolerated" CssClass="fieldset-form">
                                            <table width="90%" cellspacing="5" cellpadding="0" class="form-table">
                                                <tr>
                                                    <td style="font-weight: bold;width: 47%">
                                                        <cc1:ucLabel ID="lblOTIFRate_2" runat="server" Text="lblOTIFRate_2"></cc1:ucLabel>
                                                    </td>
                                                    <td style="font-weight: bold; width: 2%">
                                                        <cc1:ucLabel ID="UcLabel4" runat="server">:</cc1:ucLabel>
                                                    </td>
                                                    <td style="font-weight: bold; width: 8%"">
                                                        <cc1:ucTextbox ID="txtOTIFRate_2" runat="server" Width="40px" MaxLength="6" onkeyup="AllowDecimalOnly(this);"></cc1:ucTextbox>
                                                          <asp:RegularExpressionValidator ID="revToleratedPenaltyBelowPercentage" ValidationExpression="^\d{0,8}(\.\d{0,2})?$"
                                                              runat="server" ControlToValidate="txtOTIFRate_2" Display="None" ValidationGroup="Save"></asp:RegularExpressionValidator>
                                                    </td>
                                                    <td style="width:43%" align="left">%</td>
                                                </tr>
                                                <tr>
                                                    <td style="font-weight: bold;">
                                                        <cc1:ucLabel ID="lblPanaltyValue_2" runat="server" Text="lblPanaltyValue_2"></cc1:ucLabel>
                                                    </td>
                                                    <td style="font-weight: bold; width: 5px">
                                                        <cc1:ucLabel ID="UcLabel7" runat="server">:</cc1:ucLabel>
                                                    </td>
                                                    <td style="font-weight: bold;">
                                                        <cc1:ucTextbox ID="txtPanaltyValue_2" runat="server" Width="40px" MaxLength="6" onkeyup="AllowDecimalOnly(this);"></cc1:ucTextbox>
                                                          <asp:RegularExpressionValidator ID="revToleratedAppliedPenaltyPercentage" ValidationExpression="^\d{0,8}(\.\d{0,2})?$"
                                                              runat="server" ControlToValidate="txtPanaltyValue_2" Display="None" ValidationGroup="Save"></asp:RegularExpressionValidator>
                                                    </td>
                                                    <td style="width:43%" align="left">%</td>
                                                </tr>
                                            </table>
                                        </cc1:ucPanel>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <cc1:ucPanel ID="pnlOfficeDepotMeasure" runat="server" GroupingText="Office Depot Measure"
                                            CssClass="fieldset-form">
                                            <table width="90%" cellspacing="5" cellpadding="0" class="form-table">
                                                <tr>
                                                    <td style="font-weight: bold;width: 47%">
                                                        <cc1:ucLabel ID="lblOTIFRate_3" runat="server" Text="lblOTIFRate_3"></cc1:ucLabel>
                                                    </td>
                                                    <td style="font-weight: bold; width: 2%">
                                                        <cc1:ucLabel ID="UcLabel9" runat="server">:</cc1:ucLabel>
                                                    </td>
                                                    <td style="font-weight: bold; width: 8%">
                                                        <cc1:ucTextbox ID="txtOTIFRate_3" runat="server" Width="40px" MaxLength="6" onkeyup="AllowDecimalOnly(this);"></cc1:ucTextbox>
                                                          <asp:RegularExpressionValidator ID="revODMeasurePenaltyBelowPercentage" ValidationExpression="^\d{0,8}(\.\d{0,2})?$"
                                                              runat="server" ControlToValidate="txtOTIFRate_3" Display="None" ValidationGroup="Save"></asp:RegularExpressionValidator>
                                                    </td>
                                                    <td style="width:43%" align="left">%</td>
                                                </tr>
                                                <tr>
                                                    <td style="font-weight: bold;">
                                                        <cc1:ucLabel ID="lblPanaltyValue_3" runat="server" Text="lblPanaltyValue_3"></cc1:ucLabel>
                                                    </td>
                                                    <td style="font-weight: bold; width: 5px">
                                                        <cc1:ucLabel ID="UcLabel11" runat="server">:</cc1:ucLabel>
                                                    </td>
                                                    <td style="font-weight: bold;">
                                                        <cc1:ucTextbox ID="txtPanaltyValue_3" runat="server" Width="40px" MaxLength="6" onkeyup="AllowDecimalOnly(this);"></cc1:ucTextbox> 
                                                          <asp:RegularExpressionValidator ID="revODMeasureAppliedPenaltyPercentage" ValidationExpression="^\d{0,8}(\.\d{0,2})?$"
                                                              runat="server" ControlToValidate="txtPanaltyValue_3" Display="None" ValidationGroup="Save"></asp:RegularExpressionValidator>
                                                    </td>
                                                    <td>%</td>
                                                </tr>
                                            </table>
                                        </cc1:ucPanel>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <cc1:ucPanel ID="pnlBackOrderValueRate" runat="server" GroupingText="Back Order Value Rate"
                                            CssClass="fieldset-form">
                                            <table width="90%" cellspacing="5" cellpadding="0" class="form-table">
                                                <tr>
                                                    <td style="font-weight: bold;width: 47%">
                                                        <cc1:ucLabel ID="lblBackOrderValueApplied" runat="server" Text="Please state the value to be applied to all Back Order"></cc1:ucLabel>
                                                    </td>
                                                    <td style="font-weight: bold; width: 2%">
                                                        <cc1:ucLabel ID="UcLabel6" runat="server">:</cc1:ucLabel>
                                                    </td>
                                                    <td style="font-weight: bold;width:8%">
                                                        <cc1:ucTextbox ID="txtBackOrderValue" runat="server" Width="40px" MaxLength="6" onkeyup="AllowDecimalOnly(this);"></cc1:ucTextbox>
                                                          <asp:RegularExpressionValidator ID="revBackOrderValueValidation" ValidationExpression="^\d{0,8}(\.\d{0,2})?$"
                                                              runat="server" ControlToValidate="txtBackOrderValue" Display="None" ValidationGroup="Save"></asp:RegularExpressionValidator>
                                                        
                                                    </td>
                                                    <td style="font-weight: bold;width:43%"> <cc1:ucDropdownList ID="drpRate" runat="server" Width="60px">
                                                       <asp:ListItem Text="EURO" Value="1"></asp:ListItem>
                                                       <asp:ListItem Text="GBP" Value="2"></asp:ListItem>
                                                        </cc1:ucDropdownList></td>

                                                </tr>
                                               
                                            </table>
                                        </cc1:ucPanel>
                                    </td>
                                </tr>
                                <tr>
                                        <td>
                                         <%-- <asp:ValidationSummary ID="vSummary" runat="server" ShowMessageBox="true" ShowSummary="false"
                                                Style="color: Red" ValidationGroup="Save" />--%>
                                        </td>
                                 </tr>
                            </table>
                        </cc1:ucPanel>
                    </td>
                </tr>
            </table>
        </div>
    </div>
     <asp:UpdatePanel ID="updpnlError" runat="server">
        <ContentTemplate>
            <asp:Button ID="btnErrorMsg" runat="Server" CausesValidation="false" Style="display: none"  />
            <ajaxToolkit:ModalPopupExtender ID="mdlErrorMsg" runat="server" TargetControlID="btnErrorMsg"
                PopupControlID="pnlErrorMsg" BackgroundCssClass="modalBackground" BehaviorID="ErrorMsg"
                DropShadow="false" />
            <asp:Panel ID="pnlErrorMsg" runat="server" Style="display: none;">
                <div style="overflow-y: hidden; overflow-x: hidden; background-color: #fff; padding: 5px;
                    border: 2px solid #ccc; text-align: left;">
                   <table cellspacing="5" cellpadding="0" border="0" align="center" class="top-setting-Popup">
                        <tr>
                            <td style="font-weight: bold;">
                                <cc1:ucLiteral ID="ltWarningMessage" runat="server" Text="This Operation will result in overwritting of settings for the selected vendor(s). Do you want to continue?"></cc1:ucLiteral>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <cc1:ucButton ID="btnErrorMsgOK" runat="server" Text="OK" CssClass="button" OnCommand="btnErrorMsgOK_Click" CausesValidation="false" />
                                <cc1:ucButton ID="btnCancel" runat="server" Text="Cancel" CssClass="button" OnCommand="btnCancel_Click" CausesValidation="false"/>

                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnErrorMsgOK" />
            <asp:AsyncPostBackTrigger ControlID="btnCancel" />
        </Triggers>
    </asp:UpdatePanel>
    <div class="bottom-shadow">
    </div>
    <div class="button-row">
        <cc1:ucButton ID="btnSave" runat="server" Text="Save" CssClass="button" ValidationGroup="Save"
            onclick="btnSave_Click" />
    <%--    <cc1:ucButton ID="btnDelete" runat="server" Text="Delete" CssClass="button" />--%>
        <cc1:ucButton ID="btnBack" runat="server" Text="Back" CssClass="button" 
            onclick="btnBack_Click" />
    </div>

      
</asp:Content>

﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using BusinessEntities.ModuleBE.OnTimeInFull.CountrySetting;
using BusinessLogicLayer.ModuleBAL.OnTimeInFull.CountrySetting;

public partial class CntOTIF_FutureDateSetupOverview : CommonPage 
{
    protected void Page_Load(object sender, EventArgs e)
    {
      

        if (!IsPostBack)
            BindFutureDatePO();
        ucExportToExcel1.GridViewControl = grdFutureDatePO;
        ucExportToExcel1.FileName = "FutureDateSetupOverview";
    }

    #region Methods

    protected void BindFutureDatePO()
    {
        CntOTIF_FutureDateSetupBE oCntOTIF_FutureDateSetupBE = new CntOTIF_FutureDateSetupBE();
        CntOTIF_FutureDateSetupBAL oCntOTIF_FutureDateSetupBAL = new CntOTIF_FutureDateSetupBAL();
        oCntOTIF_FutureDateSetupBE.Action = "ShowAll";
        oCntOTIF_FutureDateSetupBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
        oCntOTIF_FutureDateSetupBE.User.UserID = Convert.ToInt32(Session["UserID"]);
        List<CntOTIF_FutureDateSetupBE> lstOTIF_FutureDateList = oCntOTIF_FutureDateSetupBAL.GetCntOTIF_FutureDateSetupDetailsBAL(oCntOTIF_FutureDateSetupBE);
        oCntOTIF_FutureDateSetupBAL = null;
        if (lstOTIF_FutureDateList != null && lstOTIF_FutureDateList.Count > 0) {
            grdFutureDatePO.DataSource = lstOTIF_FutureDateList;
            grdFutureDatePO.DataBind();
            ViewState["lstSites"] = lstOTIF_FutureDateList;
        }
    }

    public override BusinessEntities.BaseBe[] SubSort(GridViewSortEventArgs e)
    {
        return Utilities.GenericListHelper<CntOTIF_FutureDateSetupBE>.SortList((List<CntOTIF_FutureDateSetupBE>)ViewState["lstSites"], e.SortExpression, e.SortDirection).ToArray();
    }


    #endregion
}
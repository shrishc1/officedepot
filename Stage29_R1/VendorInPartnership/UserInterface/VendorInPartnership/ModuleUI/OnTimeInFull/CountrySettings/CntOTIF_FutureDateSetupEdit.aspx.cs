﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using BusinessEntities.ModuleBE.OnTimeInFull.CountrySetting;
using BusinessLogicLayer.ModuleBAL.OnTimeInFull.CountrySetting;

public partial class CntOTIF_FutureDateSetupEdit : CommonPage {
    protected void Page_Load(object sender, EventArgs e) {
      

        if (!IsPostBack) {
            ddlDay.Items.Clear();
            ddlDay.Items.Insert(0,new ListItem("Select", "0"));
            ddlMonth.SelectedIndex = ddlMonth.Items.IndexOf(ddlMonth.Items.FindByValue("0"));
            ddlDay.SelectedIndex = ddlDay.Items.IndexOf(ddlDay.Items.FindByValue("0"));
            GetCountryFutureDatePO();
        }
    }

    #region Methods

    public void BindDays(int pMonth) {
        ArrayList arrLeftDays = new ArrayList();        
        switch (pMonth) {
            case 0:
                ddlDay.Items.Clear();
                ddlDay.Items.Insert(0, new ListItem("Select", "0"));
                ddlMonth.SelectedIndex = ddlMonth.Items.IndexOf(ddlMonth.Items.FindByValue("0"));
                ddlDay.SelectedIndex = ddlDay.Items.IndexOf(ddlDay.Items.FindByValue("0"));
                break;
            case 1:case 3:case 5:
            case 7:case 8:case 10:case 12:
                arrLeftDays.Add("0");                
                BindDaysAccordingToMonth(arrLeftDays);
                break;
            case 2:
                arrLeftDays.Add("29"); arrLeftDays.Add("30"); arrLeftDays.Add("31");
                BindDaysAccordingToMonth(arrLeftDays);
                break;
            case 4:case 6:
            case 9:case 11:
                arrLeftDays.Add("31");
                BindDaysAccordingToMonth(arrLeftDays);
                break;
        } 
    }

    public void BindDaysAccordingToMonth(ArrayList pLeftDays) {
        ddlDay.Items.Clear();
        ddlDay.Items.Insert(0, new ListItem("Select", "0"));
        for (int i = 1; i <= 31; i++) {
            if (!pLeftDays.Contains(i.ToString())) {
                ddlDay.Items.Add(new ListItem(i.ToString(), i.ToString()));
            }
        }
    }

    public void GetCountryFutureDatePO() {
        if (GetQueryStringValue("CountryID") != null && GetQueryStringValue("CountryID").ToString() != "") {
            CntOTIF_FutureDateSetupBE oCntOTIF_FutureDateSetupBE = new CntOTIF_FutureDateSetupBE();
            CntOTIF_FutureDateSetupBAL oCntOTIF_FutureDateSetupBAL = new CntOTIF_FutureDateSetupBAL();
            oCntOTIF_FutureDateSetupBE.Action = "ShowAll";
            oCntOTIF_FutureDateSetupBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
            oCntOTIF_FutureDateSetupBE.User.UserID = Convert.ToInt32(Session["UserID"]);
            oCntOTIF_FutureDateSetupBE.CountryID = Convert.ToInt32(GetQueryStringValue("CountryID"));
            List<CntOTIF_FutureDateSetupBE> lstCountryFutureDatePO = oCntOTIF_FutureDateSetupBAL.GetCntOTIF_FutureDateSetupDetailsBAL(oCntOTIF_FutureDateSetupBE);
            oCntOTIF_FutureDateSetupBAL = null;
            if (lstCountryFutureDatePO != null && lstCountryFutureDatePO.Count > 0) {
                CountryNameWithFutureDate.Text = lstCountryFutureDatePO[0].Country.CountryName.ToString();
                if (lstCountryFutureDatePO[0].FutureDatePOMonth != null && lstCountryFutureDatePO[0].FutureDatePODay != null) {
                    BindDays(Convert.ToInt32(lstCountryFutureDatePO[0].FutureDatePOMonth));
                    ddlMonth.SelectedIndex = ddlMonth.Items.IndexOf(ddlMonth.Items.FindByValue(lstCountryFutureDatePO[0].FutureDatePOMonth.ToString()));
                    ddlDay.SelectedIndex = ddlDay.Items.IndexOf(ddlDay.Items.FindByValue(lstCountryFutureDatePO[0].FutureDatePODay.ToString()));
                }
                else {
                    ddlDay.Items.Clear();
                    ddlDay.Items.Insert(0,new ListItem("Select", "0"));
                    ddlMonth.SelectedIndex = ddlMonth.Items.IndexOf(ddlMonth.Items.FindByValue("0"));
                    ddlDay.SelectedIndex = ddlDay.Items.IndexOf(ddlDay.Items.FindByValue("0"));
                }
            }
        }
    }

    #endregion

    #region Events

    protected void ddlMonth_SelectedIndexChanged(object sender, EventArgs e) {        
        int Month = Convert.ToInt32(ddlMonth.SelectedItem.Value.ToString());
        BindDays(Month);                                                       
    }
    
    protected void btnSave_Click(object sender, EventArgs e) {
        if (GetQueryStringValue("CountryID") != null && GetQueryStringValue("CountryID").ToString() != "") {
            CntOTIF_FutureDateSetupBE oCntOTIF_FutureDateSetupBE = new CntOTIF_FutureDateSetupBE();
            CntOTIF_FutureDateSetupBAL oCntOTIF_FutureDateSetupBAL = new CntOTIF_FutureDateSetupBAL();
            oCntOTIF_FutureDateSetupBE.Action = "Edit";
            oCntOTIF_FutureDateSetupBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
            oCntOTIF_FutureDateSetupBE.User.UserID = Convert.ToInt32(Session["UserID"]);
            oCntOTIF_FutureDateSetupBE.CountryID = Convert.ToInt32(GetQueryStringValue("CountryID"));
            oCntOTIF_FutureDateSetupBE.FutureDatePOMonth = Convert.ToInt32(ddlMonth.SelectedItem.Value);
            oCntOTIF_FutureDateSetupBE.FutureDatePODay = Convert.ToInt32(ddlDay.SelectedItem.Value);
            oCntOTIF_FutureDateSetupBAL.addEditCntOTIF_FutureDateSetupDetailsBAL(oCntOTIF_FutureDateSetupBE);
            oCntOTIF_FutureDateSetupBAL = null;
            EncryptQueryString("CntOTIF_FutureDateSetupOverview.aspx");
        }
    }
    protected void btnBack_Click(object sender, EventArgs e) {
        EncryptQueryString("CntOTIF_FutureDateSetupOverview.aspx");
    }

    #endregion
}
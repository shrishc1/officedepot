﻿using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using BusinessLogicLayer.ModuleBAL.Appointment.SiteSettings;
using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class CntOTIF_OTIFMicellaneousSettingsOverview : CommonPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindGrid();
        }
        ucExportToExcel.GridViewControl = grdOTIFCountrySetting;
        ucExportToExcel.FileName = "OTIF-MiscellaneousSettingsOverview";
    }

    #region Methods

    protected void BindGrid()
    {
        UP_VendorBE oUP_VendorBE = new UP_VendorBE();
        UP_VendorBAL oUP_VendorBAL = new UP_VendorBAL();
        oUP_VendorBE.Action = "GetOTIFCountrySettings";
        oUP_VendorBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
        oUP_VendorBE.User.UserID = Convert.ToInt32(Session["UserID"]);
        List<UP_VendorBE> lstConsolidatesVendor = oUP_VendorBAL.GetOTIFCountrySettingsBAL(oUP_VendorBE);
        grdOTIFCountrySetting.DataSource = lstConsolidatesVendor;
        grdOTIFCountrySetting.DataBind();
        ViewState["lstSites"] = lstConsolidatesVendor;
    }

    #endregion
    protected void grdOTIFCountrySetting_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            string Backordervalue = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "OTIF_BackOrderValue"));
            string CurrencyName = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "CurrencyName"));
            if (Backordervalue != "")
            {
                e.Row.Cells[9].Text = Backordervalue + " " + CurrencyName;
            }
        }
    }
    public override BusinessEntities.BaseBe[] SubSort(GridViewSortEventArgs e)
    {
        return Utilities.GenericListHelper<UP_VendorBE>.SortList((List<UP_VendorBE>)ViewState["lstSites"], e.SortExpression, e.SortDirection).ToArray();
    }
    protected void btnOtifScheduler_Click(object sender, EventArgs e)
    {
        try
        {
            System.Diagnostics.Process.Start(System.Configuration.ConfigurationManager.AppSettings["OTIFSchedulerExe"]);
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('File Not Found.')", true);
            Utilities.LogUtility.SaveErrorLogEntry(ex);
        }
    }
}
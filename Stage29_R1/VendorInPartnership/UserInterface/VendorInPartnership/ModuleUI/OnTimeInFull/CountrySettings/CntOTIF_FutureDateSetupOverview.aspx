﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    CodeFile="CntOTIF_FutureDateSetupOverview.aspx.cs" Inherits="CntOTIF_FutureDateSetupOverview" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register src="../../../CommonUI/UserControls/ucAddButton.ascx" tagname="ucAddButton" tagprefix="cc2" %>
<%@ Register src="../../../CommonUI/UserControls/ucExportToExcel.ascx" tagname="ucExportToExcel" tagprefix="cc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2>
        <cc1:ucLabel ID="lblFutureDateSetup" runat="server" Text="Future Date Setting"></cc1:ucLabel>
    </h2>

    <div class="button-row">       
        <cc2:ucExportToExcel ID="ucExportToExcel1" runat="server" />
    </div>

    <table width="100%">
        <tr>
            <td align="center" colspan="9">
                <cc1:ucGridView ID="grdFutureDatePO" Width="100%" runat="server" CssClass="grid" onsorting="SortGrid" AllowSorting="true">
                    <Columns>                       
                          <asp:TemplateField HeaderText="Country" SortExpression="Country.CountryName">
                            <HeaderStyle Width="20%" HorizontalAlign="Left" />                            
                            <ItemTemplate>                                
                                <asp:HyperLink ID="hlCountry" 
                                    runat="server"
                                    NavigateUrl='<%# EncryptQuery("CntOTIF_FutureDateSetupEdit.aspx?CountryID="+Eval("CountryID"))%>' 
                                    Text='<%#Eval("Country.CountryName") %>'>                                    
                                 </asp:HyperLink>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:TemplateField>  

                        <asp:BoundField HeaderText="Future Due Date" DataField="FutureDate" SortExpression="FutureDate">
                            <HeaderStyle Width="80%" HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                    </Columns>
                </cc1:ucGridView>
            </td>
        </tr>
    </table>
</asp:Content>

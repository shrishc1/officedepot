﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
   CodeFile="CntOTIF_OTIFMicellaneousSettingsOverview.aspx.cs" Inherits="CntOTIF_OTIFMicellaneousSettingsOverview" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register src="../../../CommonUI/UserControls/ucAddButton.ascx" tagname="ucAddButton" tagprefix="cc2" %>
<%@ Register src="../../../CommonUI/UserControls/ucExportToExcel.ascx" tagname="ucExportToExcel" tagprefix="cc2" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2>
        <cc1:ucLabel ID="lblOTIFMiscellaneousSettings" runat="server" Text="OTIF Miscellaneous Settings"></cc1:ucLabel>
    </h2>
     <div class="button-row">
                <cc1:ucButton ID="btnOtifScheduler" runat="server" Text="Run OTIF Scheduler" CssClass="button"
                    OnClick="btnOtifScheduler_Click" />
            </div>
    <div class="button-row">
        <cc2:ucAddButton ID="btnAdd" runat="server" NavigateUrl="CntOTIF_OTIFMicellaneousSettingsEdit.aspx" />
        <cc2:ucExportToExcel ID="ucExportToExcel" runat="server" />
    </div>
    <table width="100%">
        <tr>
            <td align="center" >
                <cc1:ucGridView ID="grdOTIFCountrySetting" Width="100%" runat="server" 
                    CssClass="grid" onrowdatabound="grdOTIFCountrySetting_RowDataBound" onsorting="SortGrid" AllowSorting="true">
                    <Columns>
                        <asp:BoundField HeaderText="Country" DataField="CountryName" SortExpression ="CountryName">
                            <HeaderStyle Width="100px" HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField>

                        <asp:BoundField HeaderText="Vendor No" DataField="Vendor_No" SortExpression ="Vendor_No">
                            <HeaderStyle Width="100px" HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                        
                        <asp:TemplateField HeaderText="Vendor Name" SortExpression="VendorName">
                            <HeaderStyle Width="100px" HorizontalAlign="Left" />
                             <ItemTemplate>
                                <asp:HyperLink ID="hpVendorName" runat="server" Text='<%# Eval("VendorName") %>' NavigateUrl='<%# EncryptQuery("CntOTIF_OTIFMicellaneousSettingsEdit.aspx?VendorId="+ Eval("VendorID")) %>'></asp:HyperLink>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:TemplateField>
                       
                         <asp:BoundField HeaderText="OTIF  Communication Frequency" DataField="OTIF_PreferedFrequency" SortExpression="OTIF_PreferedFrequency">
                            <HeaderStyle Width="100px" HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>

                        <asp:BoundField HeaderText="Absolute OTIF Penalty %" DataField="OTIF_AbsolutePenaltyBelowPercentage" SortExpression="OTIF_AbsolutePenaltyBelowPercentage">
                            <HeaderStyle Width="100px" HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField>

                         <asp:BoundField HeaderText="Absolute Penalty Value %" DataField="OTIF_AbsoluteAppliedPenaltyPercentage" SortExpression="OTIF_AbsoluteAppliedPenaltyPercentage">
                            <HeaderStyle Width="100px" HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField>

                       
                        <asp:BoundField HeaderText="ToleratedOTIFPenalty" DataField="OTIF_ToleratedPenaltyBelowPercentage" SortExpression="OTIF_ToleratedPenaltyBelowPercentage">
                            <HeaderStyle Width="100px" HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField>

                         <asp:BoundField HeaderText="ToleratedPenaltyValue" DataField="OTIF_ToleratedAppliedPenaltyPercentage" SortExpression="OTIF_ToleratedAppliedPenaltyPercentage">
                            <HeaderStyle Width="100px" HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField>

                         <asp:BoundField HeaderText="ODMeasureOTIFPenalty" DataField="OTIF_ODMeasurePenaltyBelowPercentage" SortExpression="OTIF_ODMeasurePenaltyBelowPercentage">
                            <HeaderStyle Width="100px" HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField>

                        <asp:BoundField HeaderText="ODMeasurePenaltyValue" DataField="OTIF_ODMeasureAppliedPenaltyPercentage" SortExpression="OTIF_ODMeasureAppliedPenaltyPercentage">
                            <HeaderStyle Width="100px" HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                                                

                        <asp:BoundField HeaderText="Back Order Value Rate" DataField="OTIF_BackOrderValue" SortExpression="OTIF_BackOrderValue">
                            <HeaderStyle Width="100px" HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField>

                       
                    </Columns>
                </cc1:ucGridView>
            </td>
        </tr>        
    </table>

    

</asp:Content>

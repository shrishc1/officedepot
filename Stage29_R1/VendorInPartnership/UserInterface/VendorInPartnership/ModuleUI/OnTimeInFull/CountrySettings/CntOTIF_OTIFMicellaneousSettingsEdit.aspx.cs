﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using Utilities; using WebUtilities;
using BaseControlLibrary;
using BusinessEntities.ModuleBE.Upload;
using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using BusinessLogicLayer.ModuleBAL.Appointment.SiteSettings;

public partial class CntOTIF_OTIFMicellaneousSettingsEdit : CommonPage {

    protected void Page_Init(object sender, EventArgs e) {
        ucCountry.CurrentPage = this;
        ucSeacrhVendor1.IsStandAloneRequired = true;
        ucSeacrhVendor1.IsParentRequired = true;
        ucSeacrhVendor1.IsChildRequired = true;
    }

    protected void Page_Prerender(object sender, EventArgs e) {
        if (!IsPostBack) {
            ucSeacrhVendor1.CountryID = Convert.ToInt32(ucCountry.innerControlddlCountry.SelectedItem.Value);

        }

    }

    protected void Page_Load(object sender, EventArgs e) {
        

        if (!IsPostBack) {

            ucCountry.innerControlddlCountry.AutoPostBack = true;
            GetOTIFCountrySettings();
        }
    }

    #region Methods

    public override void CountrySelectedIndexChanged() {
        base.CountrySelectedIndexChanged();
        ListBox lstSelectedVendor = (ListBox)ucSeacrhVendor1.FindControl("lstSelectedVendor");
        ucSeacrhVendor1.CountryID = Convert.ToInt32(ucCountry.innerControlddlCountry.SelectedItem.Value);
        ucSeacrhVendor1.ClearSearch();
        lstSelectedVendor.Items.Clear();
        ucSeacrhVendor1.FillVendor();
    }

    private void GetOTIFCountrySettings() {

        if (GetQueryStringValue("VendorId") != null) {
            trAdd.Visible = false;
            trEdit.Visible = true;
            UP_VendorBE oUP_VendorBE = new UP_VendorBE();
            UP_VendorBAL oUP_VendorBAL = new UP_VendorBAL();

            oUP_VendorBE.Action = "GetOTIFCountrySettings";
            oUP_VendorBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
            oUP_VendorBE.User.UserID = Convert.ToInt32(Session["UserID"]);
            oUP_VendorBE.VendorID = Convert.ToInt32(GetQueryStringValue("VendorID"));

            List<UP_VendorBE> lstVendorOtifSettings = new List<UP_VendorBE>();
            lstVendorOtifSettings = oUP_VendorBAL.GetOTIFCountrySettingsBAL(oUP_VendorBE);
            oUP_VendorBAL = null;

            lblCountryEdit.Text = lstVendorOtifSettings[0].CountryName;
            lblVendorNameEdit.Text = lstVendorOtifSettings[0].VendorName;

            if (lstVendorOtifSettings != null && lstVendorOtifSettings.Count > 0) {
              
                if (lstVendorOtifSettings[0].OTIF_PreferedFrequency.Trim() == "M") {
                    rdoMonthly.Checked = true;
                }
               
                else {

                    rdoNever.Checked = true;
                }

                txtOTIFRate_1.Text = Convert.ToString(lstVendorOtifSettings[0].OTIF_AbsolutePenaltyBelowPercentage);
                txtPanaltyValue_1.Text = Convert.ToString(lstVendorOtifSettings[0].OTIF_AbsoluteAppliedPenaltyPercentage);
                txtOTIFRate_2.Text = Convert.ToString(lstVendorOtifSettings[0].OTIF_ToleratedPenaltyBelowPercentage);
                txtPanaltyValue_2.Text = Convert.ToString(lstVendorOtifSettings[0].OTIF_ToleratedAppliedPenaltyPercentage);
                txtOTIFRate_3.Text = Convert.ToString(lstVendorOtifSettings[0].OTIF_ODMeasurePenaltyBelowPercentage);
                txtPanaltyValue_3.Text = Convert.ToString(lstVendorOtifSettings[0].OTIF_ODMeasureAppliedPenaltyPercentage);
                txtBackOrderValue.Text = Convert.ToString(lstVendorOtifSettings[0].OTIF_BackOrderValue);
                drpRate.SelectedValue = Convert.ToString(lstVendorOtifSettings[0].CurrencyID);

            }
        }
        else {
            trAdd.Visible = true;
            trEdit.Visible = false;
            rdoNever.Checked = true;
        }


    }

    #endregion
    protected void btnBack_Click(object sender, EventArgs e) {
        EncryptQueryString("CntOTIF_OTIFMicellaneousSettingsOverview.aspx");
    }
    protected void btnSave_Click(object sender, EventArgs e) {

        UP_VendorBE oUP_VendorBE = new UP_VendorBE();
        UP_VendorBAL oUP_VendorBAL = new UP_VendorBAL();
        

        if (GetQueryStringValue("VendorID") != null) {

            oUP_VendorBE.ConsolidatedVendors = GetQueryStringValue("VendorID").ToString();

        }
        else {
            ListBox lstSelectedVendor = (ListBox)ucSeacrhVendor1.FindControl("lstSelectedVendor");
            if (lstSelectedVendor.Items.Count == 0)
            {
                string NoSelectedVendor = WebCommon.getGlobalResourceValue("NoSelectedVendor");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('" + NoSelectedVendor + "')", true);
                return;
            }

            if (lstSelectedVendor.Items.Count > 1) {
                oUP_VendorBE.ConsolidatedVendors = lstSelectedVendor.Items[0].Value + ",";
                for (int i = 1; i < lstSelectedVendor.Items.Count - 1; i++) {
                    oUP_VendorBE.ConsolidatedVendors += lstSelectedVendor.Items[i].Value + ",";
                }
                oUP_VendorBE.ConsolidatedVendors += lstSelectedVendor.Items[lstSelectedVendor.Items.Count - 1].Value;
            }
            else {
                oUP_VendorBE.ConsolidatedVendors = lstSelectedVendor.Items[0].Value;
            }
        }

        


        //Check is setting exist for selected vendor. If yes then give warning message else proceed.

        oUP_VendorBE.Action = "CheckExistingSettingsForVendor";
        oUP_VendorBE.ConsolidatedVendors = oUP_VendorBE.ConsolidatedVendors;
        DataTable dtExistingVendor = oUP_VendorBAL.CheckExistingSettingsForVendorBAL(oUP_VendorBE);
        oUP_VendorBAL = null;
        if (dtExistingVendor != null && dtExistingVendor.Rows.Count > 0) {

            mdlErrorMsg.Show();
        }
        else {
            SaveMiscellenousSettings();
        }



    }

    

    protected void btnErrorMsgOK_Click(object sender, CommandEventArgs e) {
        SaveMiscellenousSettings();
       
    }

    private void SaveMiscellenousSettings() {
        UP_VendorBE oUP_VendorBE = new UP_VendorBE();
        UP_VendorBAL oUP_VendorBAL = new UP_VendorBAL();
        oUP_VendorBE.Action = "AddEditOTIFCountrySettings";
        oUP_VendorBE.CountryID = Convert.ToInt32(ucCountry.innerControlddlCountry.SelectedValue);
        ListBox lstSelectedVendor = (ListBox)ucSeacrhVendor1.FindControl("lstSelectedVendor");

        if (GetQueryStringValue("VendorID") != null) {

            oUP_VendorBE.ConsolidatedVendors = GetQueryStringValue("VendorID").ToString();

        }
        else {
            if (lstSelectedVendor.Items.Count > 1) {
                oUP_VendorBE.ConsolidatedVendors = lstSelectedVendor.Items[0].Value + ",";
                for (int i = 1; i < lstSelectedVendor.Items.Count - 1; i++) {
                    oUP_VendorBE.ConsolidatedVendors += lstSelectedVendor.Items[i].Value + ",";
                }
                oUP_VendorBE.ConsolidatedVendors += lstSelectedVendor.Items[lstSelectedVendor.Items.Count - 1].Value;
            }
            else {
                oUP_VendorBE.ConsolidatedVendors = lstSelectedVendor.Items[0].Value;
            }
        }

        oUP_VendorBE.OTIF_PreferedFrequency = rdoMonthly.Checked ? "M" : "N";
        oUP_VendorBE.OTIF_AbsolutePenaltyBelowPercentage = txtOTIFRate_1.Text != string.Empty ? Convert.ToDecimal(txtOTIFRate_1.Text) : (decimal?)null;
        oUP_VendorBE.OTIF_AbsoluteAppliedPenaltyPercentage = txtPanaltyValue_1.Text != string.Empty ? Convert.ToDecimal(txtPanaltyValue_1.Text) : (decimal?)null;
        oUP_VendorBE.OTIF_ToleratedPenaltyBelowPercentage = txtOTIFRate_2.Text != string.Empty ? Convert.ToDecimal(txtOTIFRate_2.Text) : (decimal?)null;
        oUP_VendorBE.OTIF_ToleratedAppliedPenaltyPercentage = txtPanaltyValue_2.Text != string.Empty ? Convert.ToDecimal(txtPanaltyValue_2.Text) : (decimal?)null;
        oUP_VendorBE.OTIF_ODMeasurePenaltyBelowPercentage = txtOTIFRate_3.Text != string.Empty ? Convert.ToDecimal(txtOTIFRate_3.Text) : (decimal?)null;
        oUP_VendorBE.OTIF_ODMeasureAppliedPenaltyPercentage = txtPanaltyValue_3.Text != string.Empty ? Convert.ToDecimal(txtPanaltyValue_3.Text) : (decimal?)null;
        oUP_VendorBE.OTIF_BackOrderValue = txtBackOrderValue.Text != string.Empty ? Convert.ToDecimal(txtBackOrderValue.Text) : (decimal?)null;
        oUP_VendorBE.CurrencyID = Convert.ToInt32(drpRate.SelectedValue);

        int? iResult = oUP_VendorBAL.addEditAddEditOTIFCountrySettingsBAL(oUP_VendorBE);
        oUP_VendorBAL = null;

        if (iResult == 0) {
            string saveMessage = WebCommon.getGlobalResourceValue("SavedMessage");
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('" + saveMessage + "')", true);
        }
        EncryptQueryString("CntOTIF_OTIFMicellaneousSettingsOverview.aspx");
    }

    protected void btnCancel_Click(object sender, CommandEventArgs e) {
        mdlErrorMsg.Hide();
    }

}
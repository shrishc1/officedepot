﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    AutoEventWireup="true" CodeFile="SwitchingOTIFExclusion.aspx.cs" Inherits="SwitchingOTIFExclusion" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2>
        <cc1:ucLabel ID="lblOTIFExclusionAutomation" runat="server" Text="OTIF Exclusion Automation"></cc1:ucLabel>
    </h2>
    <div class="right-shadow">
        <div class="formbox">
            <table width="100%" cellspacing="1" cellpadding="0" border="0" align="center" class="top-settings">
                <tr>
                    <td style="font-weight: bold; width: 30%">
                        <cc1:ucLabel ID="lblExcludePOBookingProcess" runat="server" Text="Exclude PO lines during booking process"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold; width: 2%">
                        :
                    </td>
                    <td style="width: 70%">
                        <cc1:ucRadioButtonList ID="rdoBookingProcess" CssClass="radio-fix" runat="server" RepeatDirection="Horizontal">
                            <asp:ListItem Text="lstYes" Value="Yes"></asp:ListItem>
                            <asp:ListItem Text="lstNo" Value="No" ></asp:ListItem>
                        </cc1:ucRadioButtonList>
                    </td>
                </tr>
            </table><br />
            <table width="100%" cellspacing="1" cellpadding="0" border="0" align="center" class="top-settings">
                <tr>
                    <td style="font-weight: bold; width: 30%">
                        <cc1:ucLabel ID="lblExcludePObyScheduler" runat="server" Text="Exclude PO lines using daily scheduler"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold; width: 2%">
                        :
                    </td>
                    <td style=" width: 70%;">
                        <cc1:ucRadioButtonList ID="rdoschedulingProcess" CssClass="radio-fix" runat="server" RepeatDirection="Horizontal">
                            <asp:ListItem Text="lstYes" Value="Yes"></asp:ListItem>
                            <asp:ListItem Text="lstNo" Value="No"></asp:ListItem>
                        </cc1:ucRadioButtonList>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div class="bottom-shadow">
    </div>
    <div class="button-row">
        <cc1:ucButton ID="btnSave" runat="server" OnClick="btnSave_Click"></cc1:ucButton>
    </div>
</asp:Content>

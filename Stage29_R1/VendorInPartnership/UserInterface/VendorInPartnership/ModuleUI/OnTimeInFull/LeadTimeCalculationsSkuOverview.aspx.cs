﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.Upload;
using WebUtilities;
using BusinessLogicLayer.ModuleBAL.OnTimeInFull.Report;
using System.Collections;
using Utilities;

public partial class LeadTimeCalculationsSkuOverview : CommonPage
{

    #region Declarations ...
    private const int gridPageSize = 50;
    private bool IsGoClicked = false;
    #endregion

    #region Events

    protected void Page_InIt(object sender, EventArgs e)
    {
        ucSite.CurrentPage = this;
        ucSite.IsAllRequired = true;
    }

    public override void SitePost_Load()
    {

        if (!IsPostBack)
        {
            ucSite.innerControlddlSite.SelectedIndex = 0;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            txtFromDate.Text = DateTime.Now.AddDays(-DateTime.Now.Day + 1).ToString("dd/MM/yyyy");
            txtToDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
            hdnJSFromDt.Value = txtFromDate.Text;
            hdnJSToDt.Value = txtToDate.Text;
            pager1.PageSize = gridPageSize;
            pager1.GenerateGoToSection = false;
            pager1.GeneratePagerInfoSection = false;
            pager1.Visible = false;

            

            if (GetQueryStringValue("PreviousPage") == null)
                BindGridOnSession();
        }
        txtPOSearch.Focus();
    }




    protected void Page_PreRender(object sender, EventArgs e)
    {

        //Bind PO Number and Site if coming from PO Reconciliation in Close click
        if (GetQueryStringValue("PurchaseOrderNo") != null && GetQueryStringValue("SiteId") != null)
        {
            btnBack.Visible = true;
            btnAdd.Visible = false;
            txtPOSearch.Text = Convert.ToString(GetQueryStringValue("PurchaseOrderNo"));
            ucSite.innerControlddlSite.SelectedValue = Convert.ToString(GetQueryStringValue("SiteId"));
            BindGrid(1);
        }

        if (GetQueryStringValue("PreviousPage") != null && GetQueryStringValue("PreviousPage").ToString() == "PO")
        {
            if (Session["OTIFHIT"] != null)
            {
                if (IsGoClicked == false)
                    GetSession();
            }
        }  
    }


    protected void btnGo_Click(object sender, EventArgs e)
    {
        //    OTIF_LeadTimeSKUBE oOTIF_LeadTimeSKUBE = new OTIF_LeadTimeSKUBE();
        //    OTIFReportBAL oOTIF_ReportBAL = new OTIFReportBAL();

        //    oOTIF_LeadTimeSKUBE.Action = "GetSelectedPOLeadListSKU";
        //    List<OTIF_LeadTimeSKUBE> lstRevisedLeadListing = new List<OTIF_LeadTimeSKUBE>();
        //    try
        //    {
        //        oOTIF_LeadTimeSKUBE.Purchase_order = txtPOSearch.Text == string.Empty ? 0 : Convert.ToInt32(txtPOSearch.Text);
        //    }
        //    catch
        //    {
        //        oOTIF_LeadTimeSKUBE.Purchase_order = 0;
        //    }

        //    oOTIF_LeadTimeSKUBE.SelectedVendorIDs = msVendor.SelectedVendorIDs;

        //    lstRevisedLeadListing = oOTIF_ReportBAL.GetSelectedPOLeadListSkuBAL(oOTIF_LeadTimeSKUBE);

        //    oOTIF_ReportBAL = null;
        //    grdRevisedleadListing.DataSource = lstRevisedLeadListing;

        //    grdRevisedleadListing.DataBind();


        //    grdExport.DataSource = lstRevisedLeadListing;

        //    grdExport.DataBind();

        //    ViewState["lstSkuSites"] = lstRevisedLeadListing;


        IsGoClicked = true;
        BindGrid(1);
        msVendor.setVendorsOnPostBack();
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        // Will be visible only when coming from ScheduledPOReconciliation page.
        EncryptQueryString("../Appointment/Reports/ScheduledPOReconciliation.aspx?PreviousPage=OTIFHit");
    }


    protected void grdRevisedleadListing_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        if (ViewState["lstSkuSites"] != null)
        {
            grdRevisedleadListing.DataSource = (List<OTIF_LeadTimeSKUBE>)ViewState["lstSkuSites"];
            grdRevisedleadListing.PageIndex = e.NewPageIndex;
            if (Session["OTIFHIT"] != null)
            {
                Hashtable htOTIFHIT = (Hashtable)Session["OTIFHIT"];
                htOTIFHIT.Remove("PageIndex");
                htOTIFHIT.Add("PageIndex", e.NewPageIndex);
                Session["OTIFHIT"] = htOTIFHIT;
            }
            grdRevisedleadListing.DataBind();
        }
    }

    public void pager_Command(object sender, CommandEventArgs e)
    {
        int currnetPageIndx = Convert.ToInt32(e.CommandArgument);
        pager1.CurrentIndex = currnetPageIndx;
        BindGrid(currnetPageIndx);
    }

    protected void btnExport_Click(object sender, EventArgs e)
    {
        OTIF_LeadTimeSKUBE oOTIF_LeadTimeSKUBE = new OTIF_LeadTimeSKUBE();
        OTIFReportBAL oOTIF_ReportBAL = new OTIFReportBAL();

       


        oOTIF_LeadTimeSKUBE.Action = "GetRevisedLeadListSKU";

        oOTIF_LeadTimeSKUBE.SiteID = ucSite.innerControlddlSite.SelectedIndex > 0 ? Convert.ToInt32(ucSite.innerControlddlSite.SelectedValue) : 0;
        try
        {
            oOTIF_LeadTimeSKUBE.Purchase_order = txtPOSearch.Text;
        }
        catch
        {
            oOTIF_LeadTimeSKUBE.Purchase_order = null;
        }
        oOTIF_LeadTimeSKUBE.SelectedVendorIDs = msVendor.SelectedVendorIDs;

        oOTIF_LeadTimeSKUBE.GridCurrentPageNo = 0;
        oOTIF_LeadTimeSKUBE.GridPageSize = 0;
       
        oOTIF_LeadTimeSKUBE.DateFrom = Common.GetYYYY_MM_DD(hdnJSFromDt.Value);
        oOTIF_LeadTimeSKUBE.DateTo = Common.GetYYYY_MM_DD(hdnJSToDt.Value);

        oOTIF_LeadTimeSKUBE.SKU = new UP_SKUBE();
        oOTIF_LeadTimeSKUBE.SKU.OD_SKU_NO = txtSku.Text.Trim();
        oOTIF_LeadTimeSKUBE.SKU.Direct_SKU = txtCATCode.Text.Trim();
       
        List<OTIF_LeadTimeSKUBE> lstRevisedLeadListing = new List<OTIF_LeadTimeSKUBE>();
        lstRevisedLeadListing = oOTIF_ReportBAL.GetRevisedSKULeadListBAL(oOTIF_LeadTimeSKUBE);

        if (lstRevisedLeadListing != null && lstRevisedLeadListing.Count > 0)
        {
            grdExport.DataSource = lstRevisedLeadListing;
        }
        else
        {
            grdExport.DataSource = null;
        }

        grdExport.DataBind();
        oOTIF_ReportBAL = null;
        WebCommon.ExportHideHidden("ApplyOTIFHit", grdExport);    
    }

    #endregion

    #region Methods

    protected void BindGrid(int Page = 1)
    {
        OTIF_LeadTimeSKUBE oOTIF_LeadTimeSKUBE = new OTIF_LeadTimeSKUBE();
        OTIFReportBAL oOTIF_ReportBAL = new OTIFReportBAL();

        oOTIF_LeadTimeSKUBE.Action = "GetRevisedLeadListSKU";

        oOTIF_LeadTimeSKUBE.SiteID = ucSite.innerControlddlSite.SelectedIndex > 0 ? Convert.ToInt32(ucSite.innerControlddlSite.SelectedValue) : 0;
        try
        {
            oOTIF_LeadTimeSKUBE.Purchase_order = txtPOSearch.Text;
        }
        catch
        {
            oOTIF_LeadTimeSKUBE.Purchase_order = string.Empty;
        }
        oOTIF_LeadTimeSKUBE.SelectedVendorIDs = msVendor.SelectedVendorIDs;

        oOTIF_LeadTimeSKUBE.GridCurrentPageNo = Page;
        oOTIF_LeadTimeSKUBE.GridPageSize = gridPageSize;

        oOTIF_LeadTimeSKUBE.DateFrom = Common.GetYYYY_MM_DD(hdnJSFromDt.Value);
        oOTIF_LeadTimeSKUBE.DateTo = Common.GetYYYY_MM_DD(hdnJSToDt.Value);

        oOTIF_LeadTimeSKUBE.SKU = new UP_SKUBE();
        oOTIF_LeadTimeSKUBE.SKU.OD_SKU_NO = txtSku.Text.Trim();
        oOTIF_LeadTimeSKUBE.SKU.Direct_SKU = txtCATCode.Text.Trim();

        this.SetSession(oOTIF_LeadTimeSKUBE);

        List<OTIF_LeadTimeSKUBE> lstRevisedLeadListing = new List<OTIF_LeadTimeSKUBE>();
        lstRevisedLeadListing = oOTIF_ReportBAL.GetRevisedSKULeadListBAL(oOTIF_LeadTimeSKUBE);

        if (lstRevisedLeadListing != null && lstRevisedLeadListing.Count > 0)
        {
            grdRevisedleadListing.DataSource = lstRevisedLeadListing;
            pager1.ItemCount = lstRevisedLeadListing[0].TotalRecords;           
            btnExportToExcel.Visible = true;
            txtFromDate.Text = hdnJSFromDt.Value;
            txtToDate.Text = hdnJSToDt.Value;      
      
            if (pager1.ItemCount > gridPageSize)
                pager1.Visible = true;
            else
                pager1.Visible = false;
        }
        else
        {
            txtFromDate.Text = hdnJSFromDt.Value;
            txtToDate.Text = hdnJSToDt.Value;
            grdRevisedleadListing.DataSource = null;
            pager1.Visible = false;
            btnExportToExcel.Visible = false;
        }

        grdRevisedleadListing.PageIndex = Page;
        pager1.CurrentIndex = Page;

        grdRevisedleadListing.DataBind();

        oOTIF_ReportBAL = null;

        ViewState["lstSkuSites"] = lstRevisedLeadListing;
    }

    private void SetSession(OTIF_LeadTimeSKUBE oOTIF_LeadTimeSKUBE)
    {
        Session.Remove("OTIFHIT");
        Session["OTIFHIT"] = null;



        Hashtable htOTIFHIT = new Hashtable();
        htOTIFHIT.Add("SelectedSiteId", oOTIF_LeadTimeSKUBE.SiteID);
        htOTIFHIT.Add("SelectedPo", oOTIF_LeadTimeSKUBE.Purchase_order);

        htOTIFHIT.Add("SelectedVendorID", msVendor.SelectedVendorIDs);
        htOTIFHIT.Add("SelectedVendorName", msVendor.SelectedVendorName);
        
        htOTIFHIT.Add("DateFrom", hdnJSFromDt.Value);
        htOTIFHIT.Add("DateTo", hdnJSToDt.Value);

        htOTIFHIT.Add("SKU", oOTIF_LeadTimeSKUBE.SKU.OD_SKU_NO);
        htOTIFHIT.Add("Viking", oOTIF_LeadTimeSKUBE.SKU.Direct_SKU);       

        htOTIFHIT.Add("PageIndex", oOTIF_LeadTimeSKUBE.GridCurrentPageNo);

        Session["OTIFHIT"] = htOTIFHIT;
    }


    private void GetSession()
    {
        OTIF_LeadTimeSKUBE oOTIF_LeadTimeSKUBE = new OTIF_LeadTimeSKUBE();
        OTIFReportBAL oOTIF_ReportBAL = new OTIFReportBAL();
        if (Session["OTIFHIT"] != null)
        {
            Hashtable htOTIFHIT = (Hashtable)Session["OTIFHIT"];

            oOTIF_LeadTimeSKUBE.SiteID = (htOTIFHIT.ContainsKey("SelectedSiteId") && htOTIFHIT["SelectedSiteId"] != null) ? Convert.ToInt32(htOTIFHIT["SelectedSiteId"].ToString()) : 0;
            ucSite.innerControlddlSite.SelectedValue = htOTIFHIT["SelectedSiteId"].ToString();

            oOTIF_LeadTimeSKUBE.Purchase_order = (htOTIFHIT.ContainsKey("SelectedPo") && htOTIFHIT["SelectedPo"] != null) ? htOTIFHIT["SelectedPo"].ToString() : null;
            txtPOSearch.Text = (htOTIFHIT.ContainsKey("SelectedPo") && htOTIFHIT["SelectedPo"] != null) ? htOTIFHIT["SelectedPo"].ToString() : null;

            oOTIF_LeadTimeSKUBE.SelectedVendorIDs = (htOTIFHIT.ContainsKey("SelectedVendorID") && htOTIFHIT["SelectedVendorID"] != null) ? htOTIFHIT["SelectedVendorID"].ToString() : null;

            oOTIF_LeadTimeSKUBE.DateFrom = (htOTIFHIT.ContainsKey("DateFrom") && htOTIFHIT["DateFrom"] != null) ? Common.GetYYYY_MM_DD(htOTIFHIT["DateFrom"].ToString()) : null;
            oOTIF_LeadTimeSKUBE.DateTo = (htOTIFHIT.ContainsKey("DateTo") && htOTIFHIT["DateTo"] != null) ? Common.GetYYYY_MM_DD(htOTIFHIT["DateTo"].ToString()) : null;

            txtFromDate.Text = (htOTIFHIT.ContainsKey("DateFrom") && htOTIFHIT["DateFrom"] != null) ? htOTIFHIT["DateFrom"].ToString() : null;
            txtToDate.Text = (htOTIFHIT.ContainsKey("DateTo") && htOTIFHIT["DateTo"] != null) ? htOTIFHIT["DateTo"].ToString() : null;
            hdnJSFromDt.Value = txtFromDate.Text;
            hdnJSToDt.Value = txtToDate.Text;

            oOTIF_LeadTimeSKUBE.SKU = new BusinessEntities.ModuleBE.Upload.UP_SKUBE();  
            oOTIF_LeadTimeSKUBE.SKU.OD_SKU_NO = (htOTIFHIT.ContainsKey("SKU") && htOTIFHIT["SKU"] != null) ? htOTIFHIT["SKU"].ToString() : null;
            oOTIF_LeadTimeSKUBE.SKU.Direct_SKU = (htOTIFHIT.ContainsKey("Viking") && htOTIFHIT["Viking"] != null) ? htOTIFHIT["Viking"].ToString() : null;
            txtSku.Text = (htOTIFHIT.ContainsKey("SKU") && htOTIFHIT["SKU"] != null) ? htOTIFHIT["SKU"].ToString() : "";
            txtCATCode.Text = (htOTIFHIT.ContainsKey("Viking") && htOTIFHIT["Viking"] != null) ? htOTIFHIT["Viking"].ToString() : "";



            if (htOTIFHIT["SelectedVendorID"] != null)
            {
                msVendor.SelectedVendorIDs = "";
                msVendor.SelectedVendorName = "";
                msVendor.SelectedVendorIDs = htOTIFHIT["SelectedVendorID"].ToString();
                msVendor.SelectedVendorName = htOTIFHIT["SelectedVendorName"].ToString();
                msVendor.setVendorsOnPostBack();
            }


            oOTIF_LeadTimeSKUBE.GridCurrentPageNo = Convert.ToInt32(htOTIFHIT["PageIndex"].ToString());
            oOTIF_LeadTimeSKUBE.GridPageSize = gridPageSize;


            oOTIF_LeadTimeSKUBE.Action = "GetRevisedLeadListSKU";
            List<OTIF_LeadTimeSKUBE> lstRevisedLeadListing = new List<OTIF_LeadTimeSKUBE>();
            lstRevisedLeadListing = oOTIF_ReportBAL.GetRevisedSKULeadListBAL(oOTIF_LeadTimeSKUBE);


            if (lstRevisedLeadListing != null && lstRevisedLeadListing.Count > 0)
            {
                grdRevisedleadListing.DataSource = lstRevisedLeadListing;
                pager1.ItemCount = lstRevisedLeadListing[0].TotalRecords;               
                btnExportToExcel.Visible = true;
                if (pager1.ItemCount > gridPageSize)
                    pager1.Visible = true;
                else
                    pager1.Visible = false;
            }
            else
            {
                grdRevisedleadListing.DataSource = null;
                pager1.Visible = false;
                btnExportToExcel.Visible = false;
            }

            grdRevisedleadListing.DataBind();



            pager1.CurrentIndex = Convert.ToInt32(htOTIFHIT["PageIndex"].ToString());


            if (grdRevisedleadListing.Rows.Count > 0)
                btnExportToExcel.Visible = true;

            oOTIF_ReportBAL = null;
            ViewState["lstSkuSites"] = lstRevisedLeadListing;
        }
    }


    public override BusinessEntities.BaseBe[] SubSort(GridViewSortEventArgs e)
    {
        int pageindex = pager1.CurrentIndex;
        BindGrid(pageindex);
        return Utilities.GenericListHelper<OTIF_LeadTimeSKUBE>.SortList((List<OTIF_LeadTimeSKUBE>)ViewState["lstSkuSites"], e.SortExpression, e.SortDirection).ToArray();
    }


    private void BindGridOnSession()
    {
        if (Session["OTIFHIT"] != null)
        {
            Hashtable htOTIFHIT = (Hashtable)Session["OTIFHIT"];
            var sessionPageIndex = Convert.ToInt32(htOTIFHIT["PageIndex"].ToString());

            int pageindex = pager1.CurrentIndex;
            if (sessionPageIndex != pageindex || pageindex == 1)
            {
                //BindGrid(pageindex);
            }
        }
        //else
        //    BindGrid();
    }

    #endregion
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.OnTimeInFull;
using BusinessLogicLayer.ModuleBAL.OnTimeInFull;
using WebUtilities;

public partial class SwitchingOTIFExclusion : CommonPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack) {
            GetSettings();
        }
    }

    private void GetSettings() {
        OTIFRulesSettingBE oOTIFRulesSettingBE = new OTIFRulesSettingBE();
        OTIFRulesSettingBAL oOTIFRulesSettingBAL = new OTIFRulesSettingBAL();

        oOTIFRulesSettingBE.Action = "GetOTIFExclusionAuto";

        oOTIFRulesSettingBE = oOTIFRulesSettingBAL.GetOTIFExclusionAutoBAL(oOTIFRulesSettingBE);

        if (oOTIFRulesSettingBE.BookingProcess)
            rdoBookingProcess.SelectedIndex = 0;
        else
            rdoBookingProcess.SelectedIndex = 1;

        if (oOTIFRulesSettingBE.DailyScheduler)
            rdoschedulingProcess.SelectedIndex = 0;
        else
            rdoschedulingProcess.SelectedIndex = 1;
    }

    protected void btnSave_Click(object sender, EventArgs e) {
        OTIFRulesSettingBE oOTIFRulesSettingBE = new OTIFRulesSettingBE();
        OTIFRulesSettingBAL oOTIFRulesSettingBAL = new OTIFRulesSettingBAL();

        oOTIFRulesSettingBE.Action = "AddOTIFExclusionAuto";
        oOTIFRulesSettingBE.BookingProcess = rdoBookingProcess.SelectedItem.Value == "Yes" ? true : false;
        oOTIFRulesSettingBE.DailyScheduler = rdoschedulingProcess.SelectedItem.Value == "Yes" ? true : false;



        int? result = oOTIFRulesSettingBAL.AddOTIFExclusionAutoBAL(oOTIFRulesSettingBE);
        oOTIFRulesSettingBE = null;
        if (result > 0) {
            string saveMessage = WebCommon.getGlobalResourceValue("SavedMessage");
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('" + saveMessage + "')", true);
        }
    }


}
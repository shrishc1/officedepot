﻿using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using BusinessEntities.ModuleBE.OnTimeInFull.Report;
using BusinessLogicLayer.ModuleBAL.Appointment.SiteSettings;
using BusinessLogicLayer.ModuleBAL.OnTimeInFull.Report;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebUtilities;

public partial class ViewOTIFReport : CommonPage
{
    string ReportPath, DateTableName = string.Empty;
    string VendorId = string.Empty;
    protected string strMustSelectVendorName = WebCommon.getGlobalResourceValue("MustSelectVendorName");

    public int MasterParentID
    {
        get
        {
            return Convert.ToInt32(ViewState["MasterParentID"]);
        }
        set
        {
            ViewState["MasterParentID"] = value;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            this.BindVendor();
            this.GetMasterVendorID();

            DataTable dt = GetLastYears();
            grdReportLinksVendor.DataSource = dt;
            grdReportLinksVendor.DataBind();

            dt = DateTime.Now.Month > 1 ? GetLast12Months(DateTime.Now) : GetLast12Months(DateTime.Now.AddYears(-1));

            if (dt != null && dt.Rows.Count > 0)
            {
                grdReportLinks.DataSource = dt;
                grdReportLinks.DataBind();
            }
            else
            {
                divcurrent.Style.Add("display", "none");
            }

            dt = DateTime.Now.Month > 1 ? GetLast12Months(DateTime.Now.AddYears(-1)) : GetLast12Months(DateTime.Now.AddYears(-2));

            grdReportLinksPrev.DataSource = dt;
            grdReportLinksPrev.DataBind();

            dt = DateTime.Now.Month > 1 ? GetLast12Months(DateTime.Now.AddYears(-2)) : GetLast12Months(DateTime.Now.AddYears(-3));

            if (dt != null && dt.Rows.Count > 0)
            {
                grdReportLinksPrev2.DataSource = dt;
                grdReportLinksPrev2.DataBind();
            }
            else
            {
                CurrentNew.Style.Add("display", "none");
            }
        }
    }

    private DataTable GetLastYears()
    {
        DataTable dt = new DataTable();
        dt.Columns.Add("Year");

        for (int i = DateTime.Now.Year; i >= 2018; i--)
        {

            if (i == DateTime.Now.Year)
            {
                if (DateTime.Now.Month > 1)
                {
                    DataRow dr = dt.NewRow();
                    dr["Year"] = i;
                    dt.Rows.Add(dr);
                }
            }
            else
            {
                DataRow dr = dt.NewRow();
                dr["Year"] = i;
                dt.Rows.Add(dr);
            }
        }
        return dt;
    }

    private DataTable GetLast12Months(DateTime PassedDate)
    {
        DataTable dt = new DataTable();
        dt.Columns.Add("Month");
        dt.Columns.Add("Year");

        if (PassedDate.Year < DateTime.Now.Year)
        {
            for (int i = 1; i <= 12; i++)
            {
                DataRow dr = dt.NewRow();
                dr["Month"] = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(i);
                dr["Year"] = PassedDate.Year;
                if (PassedDate.Year > 2015)
                    dt.Rows.Add(dr);
            }
        }
        else
        {
            for (int i = 1; i <= DateTime.Now.Month - 1; i++)
            {
                DataRow dr = dt.NewRow();
                dr["Month"] = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(i);
                dr["Year"] = DateTime.Now.Year;
                if (DateTime.Now.Year > 2015)
                    dt.Rows.Add(dr);

            }
        }
        return dt;
    }

    protected void btnView_Click(object sender, CommandEventArgs e)
    {
        string Filepath = string.Empty;

        if (e.CommandName == "OtifReport")
        {
            if (ddlVendor.Items.Count > 0 && ddlVendor.SelectedIndex == 0)
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + strMustSelectVendorName + "');", true);
                ddlVendor.Focus();
                return;
            }
            else
            {
                LinkButton Monthlink = (LinkButton)sender;
                string MonthLink = Monthlink.Text;
                string[] MonthYear = MonthLink.Split('-');
                UpdateVendorTracking(MonthLink);

                UP_VendorBE oUP_VendorBE = new UP_VendorBE();
                UP_VendorBAL oUP_VendorBAL = new UP_VendorBAL();

                oUP_VendorBE.VendorID = Convert.ToInt32(ddlVendor.SelectedItem.Value);
                oUP_VendorBE.Action = "ShowVendorByID";
                List<UP_VendorBE> lstDetails = oUP_VendorBAL.GetVendorByIdBAL(oUP_VendorBE);

                if (lstDetails!=null && lstDetails.Count > 0)
                {
                    string Vendor_No = lstDetails[0].Vendor_No;
                    string Vendor_Country = lstDetails[0].CountryName;

                    string Year = MonthYear[1];
                    string Month = MonthYear[0];
                    Year = Year.Trim();
                    Month = Month.Trim();
                    string MonthValue = String.Empty;

                    switch (Month)
                    {
                        case "January":
                            MonthValue = "01";
                            break;
                        case "February":
                            MonthValue = "02";
                            break;
                        case "March":
                            MonthValue = "03";
                            break;
                        case "April":
                            MonthValue = "04";
                            break;
                        case "May":
                            MonthValue = "05";
                            break;
                        case "June":
                            MonthValue = "06";
                            break;
                        case "July":
                            MonthValue = "07";
                            break;
                        case "August":
                            MonthValue = "08";
                            break;
                        case "September":
                            MonthValue = "09";
                            break;
                        case "October":
                            MonthValue = "10";
                            break;
                        case "November":
                            MonthValue = "11";
                            break;
                        case "December":
                            MonthValue = "12";
                            break;
                    }
                   
                    Filepath = ConfigurationManager.AppSettings["VendorOTIFMonthlyReportPath"] + "\\" + Year + "\\" + MonthValue + "\\" + Vendor_Country + "_" + Vendor_No + "_" + "VendorOTIFReport.xls";
                }
            }
        }
        else if (e.CommandName == "ScoreCardReportVendor")
        {
            LinkButton Yearlink = (LinkButton)sender;
            string Year = Yearlink.Text;
            Filepath = ConfigurationManager.AppSettings["VendorOTIFMonthlyReportPath"] + "\\" + Year + "\\" + Convert.ToString(MasterParentID) + ".xls";
        }

        if (File.Exists(Filepath))
        {
            try
            {
                FileStream sourceFile = new FileStream(Filepath, FileMode.Open);
                float FileSize;
                FileSize = sourceFile.Length;
                byte[] getContent = new byte[(int)FileSize];
                sourceFile.Read(getContent, 0, (int)sourceFile.Length);
                sourceFile.Close();
                Response.ClearContent();
                Response.ClearHeaders();
                Response.Buffer = true;
                Response.ContentType = "application/vnd.ms-excel";
                Response.AddHeader("Content-Length", getContent.Length.ToString());
                Response.AddHeader("Content-Disposition", "attachment; filename=" + Filepath);
                Response.BinaryWrite(getContent);
                Response.Flush();
                Response.End();
            }
            catch (Exception ex)
            {
                Utilities.LogUtility.SaveErrorLogEntry(ex);
            }
        }
        else
        {
            string ExcelFileNotExist = WebCommon.getGlobalResourceValue("ExcelFileNotExist");
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + ExcelFileNotExist + "')", true);
        }
    }

    private void UpdateVendorTracking(string MonthLink)
    {
        OTIFReportBE oOTIFReportBE = new OTIFReportBE();
        OTIFReportBAL oOTIFReportBAL = new OTIFReportBAL();

        oOTIFReportBE.Action = "GetVendorNumberOfTimeAccessesed";
        oOTIFReportBE.UserId = Convert.ToInt32(Session["UserID"]);
        oOTIFReportBE.ReportMonth = MonthLink;
        DataSet dsReportAccessedNumber = oOTIFReportBAL.GetVendorNumberOfTimeAccessesedBAL(oOTIFReportBE);
        if (dsReportAccessedNumber != null && dsReportAccessedNumber.Tables[0].Rows.Count > 0)
        {
            string NumberOfTimeAccessed = dsReportAccessedNumber.Tables[0].Rows[0]["NumberofTimeAccessed"].ToString();
            oOTIFReportBE.Action = "UpdateTrackingInfo";
            oOTIFReportBE.NumberOfTimeAccessed = string.IsNullOrEmpty(NumberOfTimeAccessed) ? 1 : Convert.ToInt32(NumberOfTimeAccessed) + 1;
            int? VendorTrackingResult = oOTIFReportBAL.UpdateOTIFVendorTrackingBAL(oOTIFReportBE);
        }
    }

    private void GetMasterVendorID()
    {
        UP_VendorBAL oUP_VendorBAL = new UP_VendorBAL();
        UP_VendorBE oUP_VendorBE = new UP_VendorBE();

        oUP_VendorBE.Action = "GetAllLevelVendorDetailsByUserIDNew";
        oUP_VendorBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
        oUP_VendorBE.User.UserID = Convert.ToInt32(Session["UserID"]);
        oUP_VendorBE.Site = new BusinessEntities.ModuleBE.AdminFunctions.MAS_SiteBE();
        oUP_VendorBE.Site.SiteID = 0;

        List<UP_VendorBE> lstUPVendor = oUP_VendorBAL.GetVendorByUserIdBAL(oUP_VendorBE);

        List<UP_VendorBE> lstUPVendorName = new List<UP_VendorBE>();

        if (lstUPVendor != null && lstUPVendor.Count > 0)
        {
            lstUPVendorName = lstUPVendor.FindAll(delegate (UP_VendorBE v) { return Convert.ToString(v.VendorFlag) == "G"; });

            if (lstUPVendorName != null && lstUPVendorName.Count > 0)
            {
                MasterParentID = lstUPVendorName[0].VendorID;
            }
            else
            {
                lstUPVendorName = lstUPVendor.FindAll(delegate (UP_VendorBE v) { return Convert.ToString(v.VendorFlag).Equals("P") && v.ParentVendorID == -1; });
                if (lstUPVendorName != null && lstUPVendorName.Count > 0)
                {
                    MasterParentID = lstUPVendorName[0].VendorID;
                }
                else
                {
                    lstUPVendorName = lstUPVendor.FindAll(delegate (UP_VendorBE v) { return v.VendorFlag == (char?)null && v.ParentVendorID == -1; });
                    if (lstUPVendorName != null && lstUPVendorName.Count > 0)
                    {
                        MasterParentID = lstUPVendorName[0].VendorID;
                    }
                }
            }

            if (MasterParentID == 0)
            {
                lstUPVendorName = lstUPVendor.FindAll(delegate (UP_VendorBE v) { return v.VendorFlag == (char?)null && v.ParentVendorID > 0; });
                if (lstUPVendorName != null && lstUPVendorName.Count > 0)
                    MasterParentID = lstUPVendorName[0].ParentVendorID ?? 0;
            }

            Page.Response.Write("<script>console.log('" + MasterParentID + "');</script>");
        }
    }

    private void BindVendor()
    {
        UP_VendorBAL oUP_VendorBAL = new UP_VendorBAL();
        UP_VendorBE oUP_VendorBE = new UP_VendorBE();

        oUP_VendorBE.Action = "GetVendorDetailsByUserIDNew";
        oUP_VendorBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
        oUP_VendorBE.User.UserID = Convert.ToInt32(Session["UserID"]);
        oUP_VendorBE.Site = new BusinessEntities.ModuleBE.AdminFunctions.MAS_SiteBE();
        oUP_VendorBE.Site.SiteID = 0;

        List<UP_VendorBE> lstUPVendor = oUP_VendorBAL.GetVendorByUserIdBAL(oUP_VendorBE);

        if (lstUPVendor != null && lstUPVendor.Count > 0)
            lstUPVendor = lstUPVendor.FindAll(delegate (UP_VendorBE v) { return !Convert.ToString(v.VendorFlag).Equals("P"); });
        FillControls.FillDropDown(ref ddlVendor, lstUPVendor, "VendorCountryName", "VendorID", "--Select--");
    }
}
﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.Upload;
using BusinessLogicLayer.ModuleBAL.Upload;
using Utilities;
using WebUtilities;
using BusinessLogicLayer.ModuleBAL.Discrepancy.LogDiscrepancy;
using BusinessEntities.ModuleBE.Discrepancy.LogDiscrepancy;
using System.Linq;
using System.Text;

public partial class RevisedLeadTimeCalculationsEdit : CommonPage
{
    protected string NoSelectedProductCode = WebCommon.getGlobalResourceValue("NoSelectedProductCode");
    protected string RevisedDateCheck = WebCommon.getGlobalResourceValue("RevisedDateCheck");

    protected void Page_InIt(object sender, EventArgs e)
    {
        ucSite.CurrentPage = this;
    }

    public override void SitePost_Load()
    {
        base.SitePost_Load();
        if (!IsPostBack)
        {
            ucSite.innerControlddlSite.SelectedIndex = 0;
            ucSite.innerControlddlSite.AutoPostBack = true;
            if (GetQueryStringValue("PurchaseOrderID") != null)
            {
                ucSite.innerControlddlSite.SelectedIndex = ucSite.innerControlddlSite.Items.IndexOf(ucSite.innerControlddlSite.Items.FindByValue(GetQueryStringValue("SiteId")));
                GetPurchaseOrderList();
                
                //btnDelete.Visible = true;
            }
            else
            {
                //btnDelete.Visible = false;
                
            }
        }
    }
    public override void SiteSelectedIndexChanged()
    {
        if (txtPurchaseOrderNumber.Text != string.Empty)
        {
            GetPurchaseOrderList();
        }

    }

    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            getRevisedLeadByPurchaseOrder();

            if (GetQueryStringValue("PurchaseOrderID") != null)
            {
                DisableControls();
                cusvNoSelectedProductCode.Enabled = false;
            }
            else
            {
                cusvNoSelectedProductCode.Enabled = true;
            }
        }
        txtPurchaseOrderNumber.Attributes.Add("autocomplete", "off");
    }
    protected void btnGo_Click(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(lblDueDateValue.Text))
        {
            if (Common.TextToDateFormat(lblDueDateValue.Text) < DateTime.Now.Date)
            {
                string saveMessage = WebCommon.getGlobalResourceValue("RevisedDateApplyCheck");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('" + saveMessage + "')", true);
                return;
            }
        }

        //Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE = new Up_PurchaseOrderDetailBE();
        //UP_PurchaseOrderDetailBAL oUP_PurchaseOrderDetailBAL = new UP_PurchaseOrderDetailBAL();
        //oUp_PurchaseOrderDetailBE.Action = "CheckExistance";      
        //oUp_PurchaseOrderDetailBE.Purchase_order = txtPurchaseOrderNumber.Text.Trim();



        //List<Up_PurchaseOrderDetailBE> lstODProductCodes = new List<Up_PurchaseOrderDetailBE>();
        //lstODProductCodes = oUP_PurchaseOrderDetailBAL.GetODProductCodesBAL(oUp_PurchaseOrderDetailBE);

        //if (lstODProductCodes.Count > 0 && lstODProductCodes != null)
        //{
        lstProductCode.Items.Clear();
        GetOPProductCodes();
        lstselectedProductCode.Items.Clear();
        //}
        //else
        //{
        //    string saveMessage = WebCommon.getGlobalResourceValue("InvalidPurchaseOrder");
        //    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('" + saveMessage + "')", true);
        //}
    }

    private void GetOPProductCodes()
    {
        Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE = new Up_PurchaseOrderDetailBE();
        UP_PurchaseOrderDetailBAL oUP_PurchaseOrderDetailBAL = new UP_PurchaseOrderDetailBAL();
        oUp_PurchaseOrderDetailBE.Action = "GetRevisedDueDate_ODProductCodes";
        oUp_PurchaseOrderDetailBE.Purchase_order = txtPurchaseOrderNumber.Text.Trim();
        if (GetQueryStringValue("OrderRaised") != null)
        {
            oUp_PurchaseOrderDetailBE.Order_raised = Convert.ToDateTime(GetQueryStringValue("OrderRaised").ToString());
        }
        else
        {
            oUp_PurchaseOrderDetailBE.Order_raised = ddlPurchaseOrderDate.SelectedItem.Text == "--Select--" ? (DateTime?)null : Common.GetMM_DD_YYYY(ddlPurchaseOrderDate.SelectedItem.Text);
        }
        
        oUp_PurchaseOrderDetailBE.Warehouse = (!string.IsNullOrEmpty(Convert.ToString(GetQueryStringValue("Warehouse") != null))) ? GetQueryStringValue("Warehouse") : null;

        List<Up_PurchaseOrderDetailBE> lstODProductCodes = new List<Up_PurchaseOrderDetailBE>();
        lstODProductCodes = oUP_PurchaseOrderDetailBAL.GetODProductCodesBAL(oUp_PurchaseOrderDetailBE);
        oUP_PurchaseOrderDetailBAL = null;

        var itemsExclusion = lstODProductCodes.Where(x => x.ApplyExclusionID != 0);
        var IsRevisedDatePresent = lstODProductCodes.Any(x => x.RevisedDueDate != null);


        foreach (var item in itemsExclusion)
        {
            ViewState["ProductDescExclusion"] += item.ProductDescription + " ,";
        }

        var itemsOTIFHit = lstODProductCodes.Where(x => x.SKUExclusionID != 0);

        foreach (var item in itemsOTIFHit)
        {
            ViewState["ProductDescOTIFHit"] += item.ProductDescription + " ,";
        }

        if (lstODProductCodes.Count > 0 && lstODProductCodes != null)
        {
            if (IsRevisedDatePresent == true && GetQueryStringValue("PurchaseOrderNo") == null)
            {
                string saveMessage = WebCommon.getGlobalResourceValue("NoProductCodeExist");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('" + saveMessage + "')", true);
            }
            else
            {
                FillControls.FillListBox(ref lstProductCode, lstODProductCodes, "ProductDescription", "SKUID");
            }

        }
        else
        {
            if (GetQueryStringValue("PurchaseOrderID") == null)
            {
                string saveMessage = WebCommon.getGlobalResourceValue("NoProductCodeExist");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('" + saveMessage + "')", true);
            }
        }
    }
    protected void btnMoveRightAll_Click(object sender, EventArgs e)
    {
        if (lstProductCode.Items.Count > 0)
            FillControls.MoveAllItems(lstProductCode, lstselectedProductCode);
    }
    protected void btnMoveRight_Click(object sender, EventArgs e)
    {
        if (lstProductCode.SelectedItem != null)
        {
            FillControls.MoveOneItem(lstProductCode, lstselectedProductCode);
        }
    }
    protected void btnMoveLeft_Click(object sender, EventArgs e)
    {
        if (lstselectedProductCode.SelectedItem != null)
        {
            FillControls.MoveOneItem(lstselectedProductCode, lstProductCode);
            if (lstselectedProductCode.Items.Count > 0)
            {
                lstselectedProductCode.SelectedIndex = 0;
            }

        }
    }
    protected void btnMoveLeftAll_Click(object sender, EventArgs e)
    {
        if (lstselectedProductCode.Items.Count > 0)
        {
            FillControls.MoveAllItems(lstselectedProductCode, lstProductCode);
        }
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (GetQueryStringValue("PurchaseOrderID") != null)
        {
            DeleteSelectedProductCodes();
        }

        Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE = new Up_PurchaseOrderDetailBE();
        UP_PurchaseOrderDetailBAL oUP_PurchaseOrderDetailBAL = new UP_PurchaseOrderDetailBAL();

        oUp_PurchaseOrderDetailBE.RevisedDueDate = Common.GetMM_DD_YYYY(txtRevisedDate.innerControltxtDate.Value);
        //DateTime RevisedDueDate = Convert.ToDateTime(oUp_PurchaseOrderDetailBE.RevisedDueDate); 
        //DateTime now = DateTime.Now;
        //int result = DateTime.Compare(RevisedDueDate, now);
        //if (result == -1)
        //{
        oUp_PurchaseOrderDetailBE.Purchase_order = txtPurchaseOrderNumber.Text;

        oUp_PurchaseOrderDetailBE.RevisedDueDateUserName = Session["UserName"].ToString();
        oUp_PurchaseOrderDetailBE.Reason = txtReason.Text.Trim();

        if (lstselectedProductCode.Items.Count > 0)
        {
            if (lstselectedProductCode.Items.Count > 1)
            {
                oUp_PurchaseOrderDetailBE.SelectedProductCodes = lstselectedProductCode.Items[0].Value + ",";
                oUp_PurchaseOrderDetailBE.ProductDescription = lstselectedProductCode.Items[0].Text + ",";


                for (int i = 1; i < lstselectedProductCode.Items.Count - 1; i++)
                {
                    oUp_PurchaseOrderDetailBE.SelectedProductCodes += lstselectedProductCode.Items[i].Value + ",";
                    oUp_PurchaseOrderDetailBE.ProductDescription += lstselectedProductCode.Items[i].Text + ",";

                }
                oUp_PurchaseOrderDetailBE.SelectedProductCodes += lstselectedProductCode.Items[lstselectedProductCode.Items.Count - 1].Value;
                oUp_PurchaseOrderDetailBE.ProductDescription += lstselectedProductCode.Items[lstselectedProductCode.Items.Count - 1].Text;
            }
            else
            {
                oUp_PurchaseOrderDetailBE.SelectedProductCodes = lstselectedProductCode.Items[0].Value;
                oUp_PurchaseOrderDetailBE.ProductDescription = lstselectedProductCode.Items[0].Text;
            }
        }

        if (GetQueryStringValue("PurchaseOrderID") != null)
        {
            if (!string.IsNullOrEmpty(Convert.ToString(hdnCurrentRevisedDueDate.Value)))
                oUp_PurchaseOrderDetailBE.CurrentRevisedDueDate = Common.GetMM_DD_YYYY(Convert.ToString(hdnCurrentRevisedDueDate.Value));
            oUp_PurchaseOrderDetailBE.Action = "UpdateRevisedDates";
        }
        else
        {
            oUp_PurchaseOrderDetailBE.Action = "AddRevisedDates";
        }

        StringBuilder AlreadyExclusionHitForPOMessage = new StringBuilder(); ;
        if (ViewState["ProductDescExclusion"] != null || ViewState["ProductDescOTIFHit"] != null)
        {
            if (ViewState["ProductDescExclusion"] != null)
            {
                string[] productDesc = Convert.ToString(ViewState["ProductDescExclusion"]).TrimEnd(',').Split(',');

                if (lstselectedProductCode.Items.Count > 0)
                {
                    for (int i = 0; i < productDesc.Length; i++)
                    {
                        for (int j = 0; j < oUp_PurchaseOrderDetailBE.ProductDescription.Split(',').Length; j++)
                        {
                            if (Convert.ToString(oUp_PurchaseOrderDetailBE.ProductDescription.Split(',')[j]).Trim() == productDesc[i].Trim())
                            {
                                // string[] ProductSku = productDesc[i].Split('-');
                                // AlreadyExclusionHitForPOMessage.Append("- Please Remove odsku- " + productDesc[i] + " from the select list <br /> ");
                                AlreadyExclusionHitForPOMessage.Append(WebCommon.getGlobalResourceValue("AlreadyExclusionHitForPO")).Replace("{ProductDescription}", productDesc[i].Split('-').ElementAtOrDefault(0));
                                // AlreadyExclusionHitForPOMessage.AppendLine();
                                // string AlreadyExclusionHitForPOMessage = WebCommon.getGlobalResourceValue("AlreadyExclusionHitForPO");                        
                                break;
                            }
                        }
                    }
                }
            }

            if (ViewState["ProductDescOTIFHit"] != null)
            {
                string[] productDesc = Convert.ToString(ViewState["ProductDescOTIFHit"]).TrimEnd(',').Split(',');

                if (lstselectedProductCode.Items.Count > 0)
                {
                    for (int i = 0; i < productDesc.Length; i++)
                    {
                        for (int j = 0; j < oUp_PurchaseOrderDetailBE.ProductDescription.Split(',').Length; j++)
                        {
                            if (Convert.ToString(oUp_PurchaseOrderDetailBE.ProductDescription.Split(',')[j]).Trim() == productDesc[i].Trim())
                            {
                                // string[] ProductSku = productDesc[i].Split('-');
                                // AlreadyExclusionHitForPOMessage.Append("- Please Remove odsku- " + productDesc[i] + " from the select list <br /> ");
                                AlreadyExclusionHitForPOMessage.Append(WebCommon.getGlobalResourceValue("AlreadyOTIFHitForPO")).Replace("{ProductDescription}", productDesc[i].Split('-').ElementAtOrDefault(0));
                                // AlreadyExclusionHitForPOMessage.AppendLine();
                                // string AlreadyExclusionHitForPOMessage = WebCommon.getGlobalResourceValue("AlreadyExclusionHitForPO");                        
                                break;
                            }
                        }
                    }
                }
            }
        }

        if (string.IsNullOrEmpty(AlreadyExclusionHitForPOMessage.ToString()))
        {
            int? iResult = oUP_PurchaseOrderDetailBAL.addEditUP_PurchaseOrderDetailBAL(oUp_PurchaseOrderDetailBE);
            oUP_PurchaseOrderDetailBAL = null;

            if (iResult == 0)
            {
                string saveMessage = WebCommon.getGlobalResourceValue("SavedMessage");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('" + saveMessage + "')", true);
                //EncryptQueryString("RevisedLeadTimeCalculationsOverview.aspx?PreviousPage=PO");

                if (GetQueryStringValue("PreviousPage") != null)
                {
                    if (Convert.ToString(GetQueryStringValue("PreviousPage")) == "PRO")
                        EncryptQueryString("Reports/RushOrder.aspx?PreviousPage=PO");
                    else
                        EncryptQueryString("RevisedLeadTimeCalculationsOverview.aspx?PreviousPage=PO");
                }
                else
                {
                    EncryptQueryString("RevisedLeadTimeCalculationsOverview.aspx?PreviousPage=PO");
                }
            }
        }
        else
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('" + AlreadyExclusionHitForPOMessage + "')", true);
        }
        //}
        //else if(result==1)
        //{
        //    string saveMessage = WebCommon.getGlobalResourceValue("NotDateValidMessage");
        //    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('" + saveMessage + "')", true);
        //}


    }
    protected void btnBack_Click(object sender, EventArgs e)
    {
        //EncryptQueryString("RevisedLeadTimeCalculationsOverview.aspx?PreviousPage=PO");
        if (GetQueryStringValue("PreviousPage") != null)
        {
            if (Convert.ToString(GetQueryStringValue("PreviousPage")) == "PRO")
                EncryptQueryString("Reports/RushOrder.aspx?PreviousPage=PO");
            else
                EncryptQueryString("RevisedLeadTimeCalculationsOverview.aspx?PreviousPage=PO");
        }
        else
        {
            EncryptQueryString("RevisedLeadTimeCalculationsOverview.aspx?PreviousPage=PO");
        }
    }

    private void getRevisedLeadByPurchaseOrder()
    {
        if (GetQueryStringValue("PurchaseOrderID") != null)
        {
            //trSite.Visible = false;
            //trPurchaseOrderDate.Visible = false;
            //trVendor.Visible = false;

            Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE = new Up_PurchaseOrderDetailBE();
            UP_PurchaseOrderDetailBAL oUP_PurchaseOrderDetailBAL = new UP_PurchaseOrderDetailBAL();
            oUp_PurchaseOrderDetailBE.Action = "GetRevisedLeadList";
            oUp_PurchaseOrderDetailBE.GridCurrentPageNo = 0;
            oUp_PurchaseOrderDetailBE.GridPageSize = 0;
            oUp_PurchaseOrderDetailBE.PurchaseOrderID = Convert.ToInt32(GetQueryStringValue("PurchaseOrderID"));

            List<Up_PurchaseOrderDetailBE> lstRevisedLeadListing = new List<Up_PurchaseOrderDetailBE>();
            lstRevisedLeadListing = oUP_PurchaseOrderDetailBAL.GetRevisedLeadListBAL(oUp_PurchaseOrderDetailBE);
            oUP_PurchaseOrderDetailBAL = null;
            if (lstRevisedLeadListing != null && lstRevisedLeadListing.Count > 0)
            {
                txtPurchaseOrderNumber.Text = lstRevisedLeadListing[0].Purchase_order;
                hdnCurrentRevisedDueDate.Value = txtRevisedDate.innerControltxtDate.Value = Convert.ToDateTime(lstRevisedLeadListing[0].RevisedDueDate).ToString("dd/MM/yyyy");


                GetOPProductCodes();
                getSelectedPurchaseOrder(txtRevisedDate.innerControltxtDate.Value);

                txtReason.Text = lstRevisedLeadListing[0].Reason;

                btnGo.Visible = false;
            }
            else
            {
                if (GetQueryStringValue("PurchaseOrderNo") != null)
                {
                    txtPurchaseOrderNumber.Text = Convert.ToString(GetQueryStringValue("PurchaseOrderNo"));
                    GetOPProductCodes();
                    btnGo.Visible = false;
                }
            }
        }
    }

    private void getSelectedPurchaseOrder(string Date)
    {
        Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE = new Up_PurchaseOrderDetailBE();
        UP_PurchaseOrderDetailBAL oUP_PurchaseOrderDetailBAL = new UP_PurchaseOrderDetailBAL();
        oUp_PurchaseOrderDetailBE.Action = "GetSelectedODProductCodes";
        oUp_PurchaseOrderDetailBE.Purchase_order = txtPurchaseOrderNumber.Text;
        oUp_PurchaseOrderDetailBE.RevisedDueDate = Common.GetMM_DD_YYYY(Date);


        List<Up_PurchaseOrderDetailBE> lstODProductCodes = new List<Up_PurchaseOrderDetailBE>();
        lstODProductCodes = oUP_PurchaseOrderDetailBAL.GetODProductCodesBAL(oUp_PurchaseOrderDetailBE);
        oUP_PurchaseOrderDetailBAL = null;
        if (lstODProductCodes.Count > 0 && lstODProductCodes != null)
        {
            for (int i = 0; i < lstODProductCodes.Count; i++)
            {
                ListItem index = lstProductCode.Items.FindByText(lstODProductCodes[i].ProductDescription);
                lstProductCode.Items.Remove(index);
            }
            FillControls.FillListBox(ref lstselectedProductCode, lstODProductCodes, "ProductDescription", "SKUID");

            foreach (var item in lstODProductCodes)
            {
                ViewState["ProductDescRevised"] += item.ProductDescription + ",";
                ViewState["ProductDescRevisedSelectedValue"] += item.SKUID + ",";
            }

        }
        else
        {
            //string saveMessage = WebCommon.getGlobalResourceValue("NoProductCodeExist");
            //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('" + saveMessage + "')", true);
        }
    }


    protected void ddlPurchaseOrderDate_SelectedIndexChanged(object sender, EventArgs e)
    {
        lblVendorValue.Text = ddlPurchaseOrderDate.SelectedItem.Value.ToString().Split('~')[0];
        lblDueDateValue.Text = ddlPurchaseOrderDate.SelectedItem.Value.ToString().Split('~')[1];
    }

    protected void txtPurchaseOrder_TextChanged(object sender, EventArgs e)
    {
        GetPurchaseOrderList();

    }
    private void GetPurchaseOrderList()
    {
        DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
        DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();

        oDiscrepancyBE.Action = "GetPurchaseOrderDate";
        if (GetQueryStringValue("SiteID") == null)
        {
            oDiscrepancyBE.SiteID = Convert.ToInt32(ucSite.innerControlddlSite.SelectedValue);
        }
        else
        {
            oDiscrepancyBE.SiteID = Convert.ToInt32(GetQueryStringValue("SiteID"));
        }
        oDiscrepancyBE.PurchaseOrderNumber = txtPurchaseOrderNumber.Text.Trim();

        List<DiscrepancyBE> lstDetails = oDiscrepancyBAL.GetPurchaseOrderDateListBAL(oDiscrepancyBE);
        oDiscrepancyBAL = null;
        if (lstDetails.Count > 0 && lstDetails != null)
        {
            FillControls.FillDropDown(ref ddlPurchaseOrderDate, lstDetails, "PurchaseOrder_Order_raised", "VendorNoName");
            lblVendorValue.Text = ddlPurchaseOrderDate.SelectedItem.Value.ToString().Split('~')[0];
            lblDueDateValue.Text = ddlPurchaseOrderDate.SelectedItem.Value.ToString().Split('~')[1];
        }
        else
        {
            string saveMessage = WebCommon.getGlobalResourceValue("InvalidPurchaseOrder");
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('" + saveMessage + "')", true);
        }
    }

    protected void btnDelete_Click(object sender, EventArgs e)
    {
        Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE = new Up_PurchaseOrderDetailBE();
        UP_PurchaseOrderDetailBAL oUP_PurchaseOrderDetailBAL = new UP_PurchaseOrderDetailBAL();

        if (GetQueryStringValue("PurchaseOrderID") != null)
        {
            string PurchaseOrderNumber = txtPurchaseOrderNumber.Text.Trim();

            //if (PurchaseOrderNumber.Equals(Convert.ToString(GetQueryStringValue("PurchaseOrderNo"))))
            //{
            oUp_PurchaseOrderDetailBE.Purchase_order = txtPurchaseOrderNumber.Text.Trim();
            //}
            //else
            //{
            //    string saveMessage = WebCommon.getGlobalResourceValue("InvalidPurchaseOrder");
            //    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('" + saveMessage + "')", true);
            //    return;
            //}


            if (!string.IsNullOrEmpty(Convert.ToString(hdnCurrentRevisedDueDate.Value)))
            {
                //string CurrentRevisedDueDate = Convert.ToString(hdnCurrentRevisedDueDate.Value);
                //string RevisedDueDate = Convert.ToString(txtRevisedDate.innerControltxtDate.Value);

                //if (CurrentRevisedDueDate.Equals(RevisedDueDate))
                //{
                oUp_PurchaseOrderDetailBE.CurrentRevisedDueDate = Common.GetMM_DD_YYYY(Convert.ToString(hdnCurrentRevisedDueDate.Value));
                //}
                //else
                //{
                //    string InvalidRevisedDueDateMessage = WebCommon.getGlobalResourceValue("InvalidRevisedDueDate");
                //    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('" + InvalidRevisedDueDateMessage + "')", true);
                //    return;
                //}
            }
            oUp_PurchaseOrderDetailBE.Action = "DeleteRevisedDueDates";
        }

        oUp_PurchaseOrderDetailBE.UserID = Convert.ToInt32(Session["UserID"]);


        //int SiteID = Convert.ToInt32(ucSite.innerControlddlSite.SelectedItem.Value);

        //if(SiteID == Convert.ToInt32(GetQueryStringValue("SiteID")) )
        //{
        //    oUp_PurchaseOrderDetailBE.SiteID = Convert.ToInt32(GetQueryStringValue("SiteID"));
        //}
        //else
        //{
        //    string InvalidSiteMessage = WebCommon.getGlobalResourceValue("InvalidSite");
        //    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('" + InvalidSiteMessage + "')", true);
        //    return;
        //}


        if (GetQueryStringValue("SiteID") != null)
        {
            oUp_PurchaseOrderDetailBE.SiteID = Convert.ToInt32(GetQueryStringValue("SiteID"));
        }
        else
        {
            oUp_PurchaseOrderDetailBE.SiteID = Convert.ToInt32(ucSite.innerControlddlSite.SelectedItem.Value);
        }

        if (lstselectedProductCode.Items.Count > 0)
        {
            if (lstselectedProductCode.Items.Count > 1)
            {
                oUp_PurchaseOrderDetailBE.SelectedProductCodes = lstselectedProductCode.Items[0].Value + ",";
                oUp_PurchaseOrderDetailBE.ProductDescription = lstselectedProductCode.Items[0].Text + ",";


                for (int i = 1; i < lstselectedProductCode.Items.Count - 1; i++)
                {
                    oUp_PurchaseOrderDetailBE.SelectedProductCodes += lstselectedProductCode.Items[i].Value + ",";
                    oUp_PurchaseOrderDetailBE.ProductDescription += lstselectedProductCode.Items[i].Text + ",";

                }
                oUp_PurchaseOrderDetailBE.SelectedProductCodes += lstselectedProductCode.Items[lstselectedProductCode.Items.Count - 1].Value;
                oUp_PurchaseOrderDetailBE.ProductDescription += lstselectedProductCode.Items[lstselectedProductCode.Items.Count - 1].Text;
            }
            else
            {
                oUp_PurchaseOrderDetailBE.SelectedProductCodes = lstselectedProductCode.Items[0].Value;
                oUp_PurchaseOrderDetailBE.ProductDescription = lstselectedProductCode.Items[0].Text;
            }
        }

        StringBuilder NotRevisedForPOMessage = new StringBuilder(); ;
        if (ViewState["ProductDescRevised"] != null)
        {
            string[] productDesc = Convert.ToString(ViewState["ProductDescRevised"]).TrimEnd(',').Split(',');

            for (int j = 0; j < oUp_PurchaseOrderDetailBE.ProductDescription.Split(',').Length; j++)
            {
                if (!productDesc.Contains(Convert.ToString(oUp_PurchaseOrderDetailBE.ProductDescription.Split(',')[j]).Trim()))
                {
                    NotRevisedForPOMessage.Append(WebCommon.getGlobalResourceValue("NotRevisedForPO")).Replace("{ProductDescription}", Convert.ToString(oUp_PurchaseOrderDetailBE.ProductDescription.Split(',')[j]));
                }

            }
        }

        if (string.IsNullOrEmpty(NotRevisedForPOMessage.ToString()))
        {
            int? iResult = oUP_PurchaseOrderDetailBAL.DeleteRevisedDueDatesBAL(oUp_PurchaseOrderDetailBE);
            oUP_PurchaseOrderDetailBAL = null;

            if (iResult == 0)
            {
                string saveMessage = WebCommon.getGlobalResourceValue("SavedMessage");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('" + saveMessage + "')", true);
                //EncryptQueryString("RevisedLeadTimeCalculationsOverview.aspx?PreviousPage=PO");

                if (GetQueryStringValue("PreviousPage") != null)
                {
                    if (Convert.ToString(GetQueryStringValue("PreviousPage")) == "PRO")
                        EncryptQueryString("Reports/RushOrder.aspx?PreviousPage=PO");
                    else
                        EncryptQueryString("RevisedLeadTimeCalculationsOverview.aspx?PreviousPage=PO");
                }
                else
                {
                    EncryptQueryString("RevisedLeadTimeCalculationsOverview.aspx?PreviousPage=PO");
                }
            }
        }
        else
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('" + NotRevisedForPOMessage + "')", true);
        }
    }

    protected void DisableControls()
    {
        ucSite.innerControlddlSite.Enabled = false;
        txtPurchaseOrderNumber.Enabled = false;
        ddlPurchaseOrderDate.Enabled = false;
        //txtRevisedDate.innerControltxtDate.Disabled = true;
    }

    private void DeleteSelectedProductCodes()
    {

        Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE = new Up_PurchaseOrderDetailBE();
        UP_PurchaseOrderDetailBAL oUP_PurchaseOrderDetailBAL = new UP_PurchaseOrderDetailBAL();

        if (GetQueryStringValue("PurchaseOrderID") != null)
        {
            string PurchaseOrderNumber = txtPurchaseOrderNumber.Text.Trim();

            oUp_PurchaseOrderDetailBE.Purchase_order = txtPurchaseOrderNumber.Text.Trim();

            if (!string.IsNullOrEmpty(Convert.ToString(hdnCurrentRevisedDueDate.Value)))
            {
                oUp_PurchaseOrderDetailBE.CurrentRevisedDueDate = Common.GetMM_DD_YYYY(Convert.ToString(hdnCurrentRevisedDueDate.Value));
            }
            oUp_PurchaseOrderDetailBE.Action = "DeleteRevisedDueDates";
        }

        oUp_PurchaseOrderDetailBE.UserID = Convert.ToInt32(Session["UserID"]);

        if (GetQueryStringValue("SiteID") != null)
        {
            oUp_PurchaseOrderDetailBE.SiteID = Convert.ToInt32(GetQueryStringValue("SiteID"));
        }
        else
        {
            oUp_PurchaseOrderDetailBE.SiteID = Convert.ToInt32(ucSite.innerControlddlSite.SelectedItem.Value);
        }

        string[] productCodesOld = Convert.ToString(ViewState["ProductDescRevisedSelectedValue"]).TrimEnd(',').Split(',');


        List<ListItemData> ProductCodesOld = new List<ListItemData>();

        foreach (var item in productCodesOld)
        {
            ListItemData obj = new ListItemData();
            obj.SelectedProductCodesToBeDeleted = item;
            ProductCodesOld.Add(obj);
        }

        if (lstselectedProductCode.Items.Count > 0)
        {
            if (lstselectedProductCode.Items.Count > 1)
            {
                oUp_PurchaseOrderDetailBE.SelectedProductCodes = lstselectedProductCode.Items[0].Value + ",";
                oUp_PurchaseOrderDetailBE.ProductDescription = lstselectedProductCode.Items[0].Text + ",";


                for (int i = 1; i < lstselectedProductCode.Items.Count - 1; i++)
                {
                    oUp_PurchaseOrderDetailBE.SelectedProductCodes += lstselectedProductCode.Items[i].Value + ",";
                    oUp_PurchaseOrderDetailBE.ProductDescription += lstselectedProductCode.Items[i].Text + ",";

                }
                oUp_PurchaseOrderDetailBE.SelectedProductCodes += lstselectedProductCode.Items[lstselectedProductCode.Items.Count - 1].Value;
                oUp_PurchaseOrderDetailBE.ProductDescription += lstselectedProductCode.Items[lstselectedProductCode.Items.Count - 1].Text;
            }
            else
            {
                oUp_PurchaseOrderDetailBE.SelectedProductCodes = lstselectedProductCode.Items[0].Value;
                oUp_PurchaseOrderDetailBE.ProductDescription = lstselectedProductCode.Items[0].Text;
            }
        }

        List<ListItemData> ListDataToBeDeleted = new List<ListItemData>();

        if (oUp_PurchaseOrderDetailBE.SelectedProductCodes != null)
        {
            List<ListItemData> ProductCodesNew = new List<ListItemData>();

            foreach (var item in oUp_PurchaseOrderDetailBE.SelectedProductCodes.Split(','))
            {
                ListItemData obj = new ListItemData();
                obj.SelectedProductCodesToBeDeleted = item;
                ProductCodesNew.Add(obj);
            }

            ProductCodesOld.RemoveAll(r => ProductCodesNew.Any(a => a.SelectedProductCodesToBeDeleted == r.SelectedProductCodesToBeDeleted));
            ListDataToBeDeleted = ProductCodesOld;
        }
        else
        {
            ListDataToBeDeleted = ProductCodesOld;
        }

        oUp_PurchaseOrderDetailBE.SelectedProductCodes = string.Empty;


        foreach (ListItemData items in ListDataToBeDeleted)
        {
            oUp_PurchaseOrderDetailBE.SelectedProductCodes += items.SelectedProductCodesToBeDeleted + ",";
        }

        oUp_PurchaseOrderDetailBE.SelectedProductCodes = oUp_PurchaseOrderDetailBE.SelectedProductCodes.TrimEnd(',');

        if (ListDataToBeDeleted != null && ListDataToBeDeleted.Count > 0)
        {
            int? iResult = oUP_PurchaseOrderDetailBAL.DeleteRevisedDueDatesBAL(oUp_PurchaseOrderDetailBE);
            oUP_PurchaseOrderDetailBAL = null;
        }

    }

    public class ListItemData
    {
        public string SelectedProductCodesToBeDeleted { get; set; }
    }

}
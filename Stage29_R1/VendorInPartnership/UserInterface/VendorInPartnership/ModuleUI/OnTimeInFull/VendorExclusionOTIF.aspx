﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    AutoEventWireup="true" CodeFile="VendorExclusionOTIF.aspx.cs" Inherits="ModuleUI_OnTimeInFull_VendorExclusionOTIF" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="../../CommonUI/UserControls/ucExportToExcel.ascx" TagName="ucExportToExcel"
    TagPrefix="cc2" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h2>
        <cc1:ucLabel ID="lblVendorExclusionFromOTIF" runat="server"></cc1:ucLabel>
    </h2>
      <Style type="text/css">
   .row1{
   color:#333333;background-color:#F7F6F3;
   }
   .row2
   {color:#284775;background-color:#FFFFFF;      
   }
   </Style>
    <div class="formbox">
        <table width="79%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings"
            id="tblSearch" runat="server">
            <tr>
                <td style="width: 5%">
                </td>
                <td style="font-weight: bold;" width="28%">
                    <cc1:ucLabel ID="lblCountry" runat="server" Text="Country" isRequired="true"></cc1:ucLabel>
                </td>
                <td style="font-weight: bold;" width="5%" align="center">
                    :
                </td>
                <td style="font-weight: bold;" width="57%">
                    <cc1:ucDropdownList ID="ddlCountry" runat="server" Width="150px">
                    </cc1:ucDropdownList>
                    <cc1:ucLabel ID="lblEditCountry" runat="server" Text=""></cc1:ucLabel>
                </td>
                <td style="width: 5%">
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td style="font-weight: bold">
                    <cc1:ucLabel ID="lblVendor" runat="server" Text="Vendor" isRequired="true"></cc1:ucLabel>
                </td>
                <td style="font-weight: bold;" align="center">
                    :
                </td>
                <td>
                    <cc1:ucTextbox ID="txtVendorNo" runat="server" MaxLength="15" Width="90px"></cc1:ucTextbox>&nbsp;
                    <cc1:ucButton CssClass="button" runat="server" ID="btnSearchVendor" OnClick="btnSearch_Click" />&nbsp;
                    &nbsp;<cc1:ucButton CssClass="button" runat="server" ID="btnSearchVendorNumber" OnClick="btnSearchByVendorNo_Click" />
                    <div id="pnlVendor" runat="server">
                        <table>
                            <tr>
                                <td>
                                    <cc1:ucListBox ID="lstLeft" runat="server" Height="150px" Width="320px">
                                    </cc1:ucListBox>
                                </td>
                                <td align="center">
                                    <div>
                                        <input type="button" id="Button1" value=">>" class="button" style="width: 35px" onclick="Javascript:MoveAll('<%= lstLeft.ClientID %>', '<%=lstRight.ClientID %>','<%= hiddenSelectedIDs.ClientID %>','<%= hiddenSelectedName.ClientID %>');" />
                                    </div>
                                    &nbsp;
                                    <div>
                                        <input type="button" id="btnMoveRight" value=">" class="button" style="width: 35px"
                                            onclick="Javascript:MoveItem('<%=lstLeft.ClientID %>', '<%= lstRight.ClientID %>','<%= hiddenSelectedIDs.ClientID %>','<%= hiddenSelectedName.ClientID %>');" />
                                    </div>
                                    &nbsp;
                                    <div>
                                        <input type="button" id="btnMoveLeft" value="<" class="button" style="width: 35px"
                                            onclick="Javascript:MoveItem('<%= lstRight.ClientID %>', '<%=lstLeft.ClientID %>','<%= hiddenSelectedIDs.ClientID %>','<%= hiddenSelectedName.ClientID %>');" />
                                    </div>
                                    &nbsp;
                                    <div>
                                        <input type="button" id="btnMoveLeftAll" value="<<" class="button" style="width: 35px"
                                            onclick="Javascript:MoveAll('<%= lstRight.ClientID %>', '<%=lstLeft.ClientID %>','<%= hiddenSelectedIDs.ClientID %>','<%= hiddenSelectedName.ClientID %>');" />
                                    </div>
                                </td>
                                <td>
                                    <cc1:ucListBox ID="lstRight" runat="server" Height="150px" Width="320px">
                                    </cc1:ucListBox>
                                    <asp:HiddenField ID="hiddenSelectedIDs" runat="server" />
                                    <asp:HiddenField ID="hiddenSelectedName" runat="server" />
                                </td>
                            </tr>
                        </table>
                    </div>
                    <%--  <cc1:MultiSelectVendor runat="server" ID="msVendor" />--%>
                </td>
            </tr>
            <tr>
                <td colspan="8" align="center">
                    <cc1:ucRadioButtonList ID="rblStatus" runat="server" RepeatDirection="Horizontal">
                        <asp:ListItem  Text="AllOnly" Value="0"></asp:ListItem>
                        <asp:ListItem Selected="True" Text="Excluded" Value="1"></asp:ListItem>
                        <asp:ListItem Text="Not Excluded" Value="2"></asp:ListItem>
                    </cc1:ucRadioButtonList>
                </td>
            </tr>
            <tr>
                <td colspan="8">
                    <div class="button-row">
                        <cc1:ucButton ID="btnSearch1" runat="server" CssClass="button" Text="Search" OnClick="btnSearch_Click1" />
                    </div>
                </td>
            </tr>
        </table>
        <div class="button-row">
            <cc1:ucButton ID="ucExportToExcel1" runat="server" CssClass="exporttoexcel" Text="Export To Excel"
                OnClick="btnExport_Click" Width="109px" Height="20px" Visible="false" />
        </div>
        <asp:HiddenField ID="hdnGridCurrentPageNo" runat="server" />
        <asp:HiddenField ID="hdnGridPageSize" runat="server" />
        <table width="100%" id="tblGrid" runat="server">
            <tr>
                <td align="center">
                    <asp:GridView ID="grdVendorExclusion" Width="100%" runat="server" AutoGenerateColumns="false"
                        CssClass="grid"  OnRowDataBound="grdVendorExclusion_RowDataBound">
                              <RowStyle CssClass="row1" />
                    <AlternatingRowStyle CssClass="row2" />
                        <EmptyDataTemplate>
                            No Records Found</EmptyDataTemplate>
                        <Columns>
                            <asp:BoundField DataField="Country" HeaderText="Country">
                                <ItemStyle HorizontalAlign="left" />
                                 <HeaderStyle HorizontalAlign="left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="Vendor" HeaderText="Vendor">
                                <ItemStyle HorizontalAlign="left" />
                                 <HeaderStyle HorizontalAlign="left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="AddedBy" HeaderText="Added By">
                                <ItemStyle HorizontalAlign="left" />
                                 <HeaderStyle HorizontalAlign="left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="DateAdded" HeaderText="Date Added">
                                <ItemStyle HorizontalAlign="left" />
                                 <HeaderStyle HorizontalAlign="left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="Status" HeaderText="Status">
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:BoundField DataField="VendorId" HeaderText="VendorId"></asp:BoundField>
                            <asp:BoundField DataField="CountryID" HeaderText="CountryID"></asp:BoundField>
                            <asp:BoundField DataField="IsActive" HeaderText="IsActive"></asp:BoundField>
                        </Columns>
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td align="left" colspan="4">
                    <cc1:PagerV2_8 ID="pager1" runat="server" OnCommand="pager_Command" GenerateGoToSection="true">
                    </cc1:PagerV2_8>
                </td>
            </tr>
        </table>
        <%--<div class="formbox">--%>
            <table width="70%" id="tblgrdVendorWise2" runat="server" cellpadding="5" cellspacing="5" align="center">
                <%--<tr><td>
       <table width="100%" id="tblgrdVendorWise1" runat="server" cellpadding="4" cellspacing="2">--%>
                <tr>
                    <td>
                        <b>
                            <cc1:ucLabel ID="lblCountry_1" runat="server" Text="Country"></cc1:ucLabel></b>
                    </td>
                    <td>
                        <b>: </b>
                    </td>
                    <td>
                        <cc1:ucLabel ID="lblCountryValue" runat="server"></cc1:ucLabel>
                    </td>
                    <td>
                        <b>
                            <cc1:ucLabel ID="lblVendor_1" runat="server" Text="Vendor"></cc1:ucLabel></b>
                    </td>
                    <td>
                        <b>: </b>
                    </td>
                    <td>
                        <cc1:ucLabel ID="lblVendorValue" runat="server"></cc1:ucLabel>
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>
                            <cc1:ucLabel ID="lblUpdatedBy" runat="server" Text="Updated By"></cc1:ucLabel></b>
                    </td>
                    <td>
                        <b>: </b>
                    </td>
                    <td>
                        <cc1:ucLabel ID="lblUpdatedByValue" runat="server"></cc1:ucLabel>
                    </td>
                    <td>
                        <b>
                            <cc1:ucLabel ID="lblUpdatedOn" runat="server" Text="Updated On"></cc1:ucLabel></b>
                    </td>
                    <td>
                        <b>: </b>
                    </td>
                    <td>
                        <cc1:ucLabel ID="lblUpdatedOnValue" runat="server"></cc1:ucLabel>
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>
                            <cc1:ucLabel ID="lblCurrentStatus" runat="server" Text="Current Status"></cc1:ucLabel></b>
                    </td>
                    <td>
                        <b>: </b>
                    </td>
                    <td>
                        <cc1:ucLabel ID="lblCurrentStatusValue" runat="server"></cc1:ucLabel>
                    </td>
                </tr>
                <%-- </table>
     </td></tr>  --%>
                <tr>
                    <td align="left" colspan="8">
                      <cc1:ucPanel ID="pnlHistory" runat="server" GroupingText="History" CssClass="fieldset-form">
                        <asp:GridView ID="grdVendorWise" Visible="false" Width="100%" runat="server" AutoGenerateColumns="false"
                            CssClass="grid">
                                 <RowStyle CssClass="row1" />
                    <AlternatingRowStyle CssClass="row2" />
                            <EmptyDataTemplate>
                                No Records Found</EmptyDataTemplate>
                            <Columns>
                                <asp:BoundField DataField="DateAdded" HeaderText="Date">
                                    <ItemStyle HorizontalAlign="left" />
                                    <HeaderStyle HorizontalAlign="left" />
                                </asp:BoundField>
                                <asp:BoundField DataField="Time" HeaderText="Time">
                                    <ItemStyle HorizontalAlign="left" />
                                     <HeaderStyle HorizontalAlign="left" />
                                </asp:BoundField>
                                <asp:BoundField DataField="AddedBy" HeaderText="Added By">
                                    <ItemStyle HorizontalAlign="left" />
                                     <HeaderStyle HorizontalAlign="left" />
                                </asp:BoundField>
                                <asp:BoundField DataField="Status" HeaderText="Status">
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:BoundField>
                            </Columns>
                        </asp:GridView>
                        </cc1:ucPanel>
                    </td>
                </tr>
                <tr><td colspan="8">
                <div class="button-row" id="divButton" runat="server">
               <cc1:ucButton ID="btnBack" runat="server"  Text="Back" Visible="false" 
                        CssClass="button" onclick="btnBack_Click"/>
                        <cc1:ucButton ID="btnActivate" runat="server"  Text="Activate" Visible="false" 
                        CssClass="button" onclick="btnActivate_Click" />
                        <cc1:ucButton ID="btnDeActivate" runat="server"  Text="Deactivate" Visible="false" 
                        CssClass="button" onclick="btnDeActivate_Click" />
            </div>
                </td></tr>
            </table>
        </div>
   <%-- </div>--%>
</asp:Content>

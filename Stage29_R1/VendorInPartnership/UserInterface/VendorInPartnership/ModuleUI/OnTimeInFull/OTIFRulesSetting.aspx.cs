﻿using System;

using System.Web.UI;
using BusinessEntities.ModuleBE.OnTimeInFull;
using BusinessLogicLayer.ModuleBAL.OnTimeInFull;
using Utilities; using WebUtilities;

public partial class OTIFRulesSetting : CommonPage
{
    string strCorrectValueValidation = WebCommon.getGlobalResourceValue("CorrectValueValidation");

    protected void Page_Load(object sender, EventArgs e)
    {
        txtDaysAllowedEarly_1.Focus();
        if (!IsPostBack)
        {
            OTIFRulesSettingBE oOTIFRulesSettingBE = new OTIFRulesSettingBE();
            OTIFRulesSettingBAL OTIFRulesSettingBAL = new OTIFRulesSettingBAL();
            oOTIFRulesSettingBE.Action = "GetOTIFRules";

            OTIFRulesSettingBE oNewOTIFRulesSettingBE = OTIFRulesSettingBAL.GetOTIFRules(oOTIFRulesSettingBE);
            OTIFRulesSettingBAL = null;
            txtDaysAllowedEarly_1.Text = oNewOTIFRulesSettingBE.AbsoluteDaysAllowedEarly.ToString();
            txtDaysAllowedEarly_2.Text = oNewOTIFRulesSettingBE.ToleratedDaysAllowedEarly1.ToString();
            txtDaysAllowedEarly_3.Text = oNewOTIFRulesSettingBE.ODMeasureDaysAllowedEarly11.ToString();
            txtDaysAllowedLate_1.Text = oNewOTIFRulesSettingBE.AbsoluteDaysAllowedLate.ToString();
            txtDaysAllowedLate_2.Text = oNewOTIFRulesSettingBE.ToleratedDaysAllowedLate1.ToString();
            txtDaysAllowedLate_3.Text = oNewOTIFRulesSettingBE.ODMeasureDaysAllowedLate11.ToString();
            txtNumberofDeliveriesAllowed_1.Text = oNewOTIFRulesSettingBE.AbsoluteNumberOfDeliveriesAllowed.ToString();
            txtNumberofDeliveriesAllowed_2.Text = oNewOTIFRulesSettingBE.ToleratedNumberOfDeliveriesAllowed1.ToString();
            txtNumberofDeliveriesAllowed_3.Text = oNewOTIFRulesSettingBE.ODMeasureNumberOfDeliveriesAllowed11.ToString();
            ViewState["RuleSettingID"] = oNewOTIFRulesSettingBE.RuleSettingID;
        }

    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (txtDaysAllowedEarly_1.Text.Trim() != string.Empty && !validationFunctions.IsNumeric(txtDaysAllowedEarly_1.Text.Trim().Replace(".", string.Empty)))
        {
            ClientScript.RegisterStartupScript(this.GetType(), "alert", "<script>alert('" + strCorrectValueValidation + "');</script>");
            txtDaysAllowedEarly_1.Focus();
            return;
        }

        if (txtDaysAllowedLate_1.Text.Trim() != string.Empty && !validationFunctions.IsNumeric(txtDaysAllowedLate_1.Text.Trim().Replace(".", string.Empty)))
        {
            ClientScript.RegisterStartupScript(this.GetType(), "alert", "<script>alert('" + strCorrectValueValidation + "');</script>");
            txtDaysAllowedLate_1.Focus();
            return;
        }

        if (txtNumberofDeliveriesAllowed_1.Text.Trim() != string.Empty && !validationFunctions.IsNumeric(txtNumberofDeliveriesAllowed_1.Text.Trim().Replace(".", string.Empty)))
        {
            ClientScript.RegisterStartupScript(this.GetType(), "alert", "<script>alert('" + strCorrectValueValidation + "');</script>");
            txtNumberofDeliveriesAllowed_1.Focus();
            return;
        }

        if (txtDaysAllowedEarly_2.Text.Trim() != string.Empty && !validationFunctions.IsNumeric(txtDaysAllowedEarly_2.Text.Trim().Replace(".", string.Empty)))
        {
            ClientScript.RegisterStartupScript(this.GetType(), "alert", "<script>alert('" + strCorrectValueValidation + "');</script>");
            txtDaysAllowedEarly_2.Focus();
            return;
        }

        if (txtDaysAllowedLate_2.Text.Trim() != string.Empty && !validationFunctions.IsNumeric(txtDaysAllowedLate_2.Text.Trim().Replace(".", string.Empty)))
        {
            ClientScript.RegisterStartupScript(this.GetType(), "alert", "<script>alert('" + strCorrectValueValidation + "');</script>");
            txtDaysAllowedLate_2.Focus();
            return;
        }

        if (txtNumberofDeliveriesAllowed_2.Text.Trim() != string.Empty && !validationFunctions.IsNumeric(txtNumberofDeliveriesAllowed_2.Text.Trim().Replace(".", string.Empty)))
        {
            ClientScript.RegisterStartupScript(this.GetType(), "alert", "<script>alert('" + strCorrectValueValidation + "');</script>");
            txtNumberofDeliveriesAllowed_2.Focus();
            return;
        }

        if (txtDaysAllowedEarly_3.Text.Trim() != string.Empty && !validationFunctions.IsNumeric(txtDaysAllowedEarly_3.Text.Trim().Replace(".", string.Empty)))
        {
            ClientScript.RegisterStartupScript(this.GetType(), "alert", "<script>alert('" + strCorrectValueValidation + "');</script>");
            txtDaysAllowedEarly_3.Focus();
            return;
        }

        if (txtDaysAllowedLate_3.Text.Trim() != string.Empty && !validationFunctions.IsNumeric(txtDaysAllowedLate_3.Text.Trim().Replace(".", string.Empty)))
        {
            ClientScript.RegisterStartupScript(this.GetType(), "alert", "<script>alert('" + strCorrectValueValidation + "');</script>");
            txtDaysAllowedLate_3.Focus();
            return;
        }

        if (txtNumberofDeliveriesAllowed_3.Text.Trim() != string.Empty && !validationFunctions.IsNumeric(txtNumberofDeliveriesAllowed_3.Text.Trim().Replace(".", string.Empty)))
        {
            ClientScript.RegisterStartupScript(this.GetType(), "alert", "<script>alert('" + strCorrectValueValidation + "');</script>");
            txtNumberofDeliveriesAllowed_3.Focus();
            return;
        }

        OTIFRulesSettingBE oOTIFRulesSettingBE = new OTIFRulesSettingBE();
        OTIFRulesSettingBAL OTIFRulesSettingBAL = new OTIFRulesSettingBAL();

        oOTIFRulesSettingBE.AbsoluteDaysAllowedEarly = string.IsNullOrEmpty(txtDaysAllowedEarly_1.Text) ? 0 : Convert.ToInt32(txtDaysAllowedEarly_1.Text);
        oOTIFRulesSettingBE.AbsoluteDaysAllowedLate = string.IsNullOrEmpty(txtDaysAllowedLate_1.Text) ? 0 : Convert.ToInt32(txtDaysAllowedLate_1.Text);
        oOTIFRulesSettingBE.AbsoluteNumberOfDeliveriesAllowed = string.IsNullOrEmpty(txtNumberofDeliveriesAllowed_1.Text) ? 0 : Convert.ToInt32(txtNumberofDeliveriesAllowed_1.Text);
        oOTIFRulesSettingBE.ODMeasureDaysAllowedEarly11 = string.IsNullOrEmpty(txtDaysAllowedEarly_3.Text) ? 0 : Convert.ToInt32(txtDaysAllowedEarly_3.Text);
        oOTIFRulesSettingBE.ODMeasureDaysAllowedLate11 = string.IsNullOrEmpty(txtDaysAllowedLate_3.Text) ? 0 : Convert.ToInt32(txtDaysAllowedLate_3.Text);
        oOTIFRulesSettingBE.ODMeasureNumberOfDeliveriesAllowed11 = string.IsNullOrEmpty(txtNumberofDeliveriesAllowed_3.Text) ? 0 : Convert.ToInt32(txtNumberofDeliveriesAllowed_3.Text);
        oOTIFRulesSettingBE.ToleratedDaysAllowedEarly1 = string.IsNullOrEmpty(txtDaysAllowedEarly_2.Text) ? 0 : Convert.ToInt32(txtDaysAllowedEarly_2.Text);
        oOTIFRulesSettingBE.ToleratedDaysAllowedLate1 = string.IsNullOrEmpty(txtDaysAllowedLate_2.Text) ? 0 : Convert.ToInt32(txtDaysAllowedLate_2.Text);
        oOTIFRulesSettingBE.ToleratedNumberOfDeliveriesAllowed1 = string.IsNullOrEmpty(txtNumberofDeliveriesAllowed_2.Text) ? 0 : Convert.ToInt32(txtNumberofDeliveriesAllowed_2.Text);
        if (Convert.ToInt32(ViewState["RuleSettingID"]) == 0)
        {
            oOTIFRulesSettingBE.Action = "AddOTIFRules";
        }
        else
        {
            oOTIFRulesSettingBE.Action = "UpdateOTIFRules";
            oOTIFRulesSettingBE.RuleSettingID = Convert.ToInt32(ViewState["RuleSettingID"]);
        }

        int? result = OTIFRulesSettingBAL.AddOTIFRulesBAL(oOTIFRulesSettingBE);
        OTIFRulesSettingBAL = null;
        if (result > 0)
        {
            string saveMessage = WebCommon.getGlobalResourceValue("SavedMessage");
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('" + saveMessage + "')", true);
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.Upload;
using BusinessLogicLayer.ModuleBAL.Upload;
using Utilities;
using WebUtilities;
using BusinessLogicLayer.ModuleBAL.Discrepancy.LogDiscrepancy;
using BusinessEntities.ModuleBE.Discrepancy.LogDiscrepancy;
using BusinessLogicLayer.ModuleBAL.OnTimeInFull.Report;
using System.Globalization;
using System.Linq;
using System.Text;

public partial class ModuleUI_OnTimeInFull_LeadTimeCalculationsSkuEdit : CommonPage
{

    protected string NoSelectedProductCode = WebCommon.getGlobalResourceValue("NoSelectedProductCode");
    //protected string RevisedDateCheck = WebCommon.getGlobalResourceValue("RevisedDateCheck");
  //  string ProductDesc = string.Empty;
    protected void Page_InIt(object sender, EventArgs e)
    {
        ucSite.CurrentPage = this;
    }

    public override void SitePost_Load()
    {
        base.SitePost_Load();
        if (!IsPostBack)
        {
            ucSite.innerControlddlSite.SelectedIndex = 0;
            ucSite.innerControlddlSite.AutoPostBack = true;

        }
    }
    public override void SiteSelectedIndexChanged()
    {
        if (txtPurchaseOrderNumber.Text != string.Empty)
        {
            GetPurchaseOrderList();
        }

    }

    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {


            getRevisedLeadByPurchaseOrder();

            if (GetQueryStringValue("PurchaseOrderID") != null)
            {
                //btnDelete.Visible = true;
                cusvNoSelectedProductCode.Enabled = false;
            }
            else
            {
                //btnDelete.Visible = false;
                cusvNoSelectedProductCode.Enabled = true;
            }
        }
        txtPurchaseOrderNumber.Attributes.Add("autocomplete", "off");
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        //Bind PO Number and Site if coming from PO Reconciliation in Close click
        if (!IsPostBack)
        {
            if (GetQueryStringValue("PurchaseOrderNo") != null && GetQueryStringValue("SiteId") != null)
            {
                txtPurchaseOrderNumber.Text = Convert.ToString(GetQueryStringValue("PurchaseOrderNo"));
                ucSite.innerControlddlSite.SelectedValue = Convert.ToString(GetQueryStringValue("SiteId"));
                GetPurchaseOrderList();
                btnGo_Click(this, null);
            }
        }
    }

    protected void btnGo_Click(object sender, EventArgs e)
    {

        lstProductCode.Items.Clear();
        GetOPProductCodes();
        lstselectedProductCode.Items.Clear();
        getSelectedPurchaseOrder();

    }

    private void GetOPProductCodes()
    {
        Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE = new Up_PurchaseOrderDetailBE();
        UP_PurchaseOrderDetailBAL oUP_PurchaseOrderDetailBAL = new UP_PurchaseOrderDetailBAL();
        oUp_PurchaseOrderDetailBE.Action = "GetApplyOTIFHit_ODProductCodes";
        oUp_PurchaseOrderDetailBE.Purchase_order = txtPurchaseOrderNumber.Text.Trim();
        oUp_PurchaseOrderDetailBE.Warehouse = (!string.IsNullOrEmpty(Convert.ToString(GetQueryStringValue("Warehouse") != null))) ? GetQueryStringValue("Warehouse") : null;
        if (GetQueryStringValue("OrderRaised") != null)
        {
            oUp_PurchaseOrderDetailBE.Order_raised = Convert.ToDateTime(GetQueryStringValue("OrderRaised").ToString());
        }
        else
            oUp_PurchaseOrderDetailBE.Order_raised = ddlPurchaseOrderDate.SelectedItem.Text == "--Select--" ? (DateTime?)null : Common.GetMM_DD_YYYY(ddlPurchaseOrderDate.SelectedItem.Text);

        //oUp_PurchaseOrderDetailBE.SiteID = Convert.ToInt32(ucSite.innerControlddlSite.SelectedValue);

        List<Up_PurchaseOrderDetailBE> lstODProductCodes = new List<Up_PurchaseOrderDetailBE>();
        lstODProductCodes = oUP_PurchaseOrderDetailBAL.GetODProductCodesBAL(oUp_PurchaseOrderDetailBE);

        var items = lstODProductCodes.Where(x => x.ApplyExclusionID != 0);
       
        foreach (var item in items)
        {         
          ViewState["ProductDesc"] += item.ProductDescription + " ,";
        }


        List<Up_PurchaseOrderDetailBE> lstODProductCodesbySite = new List<Up_PurchaseOrderDetailBE>();
        if ((GetQueryStringValue("PageMode") != null) && (Convert.ToString(GetQueryStringValue("PageMode")) == "Add"))
        {
            lstODProductCodesbySite = lstODProductCodes.Where(od => od.SKUExclusionID != 0).ToList();
        }

        oUP_PurchaseOrderDetailBAL = null;

        if (lstODProductCodes.Count > 0 && lstODProductCodes != null)
        {
            if ((GetQueryStringValue("PageMode") != null) && (Convert.ToString(GetQueryStringValue("PageMode")) == "Add"))
            {
                //if (string.IsNullOrEmpty(lstODProductCodes[0].Reason) 
                if (lstODProductCodesbySite == null || lstODProductCodesbySite.Count == 0)
                {
                    FillControls.FillListBox(ref lstProductCode, lstODProductCodes, "ProductDescription", "SKUID");
                }
                else
                {
                    string saveMessage = WebCommon.getGlobalResourceValue("OTIFHitForPOExists");
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('" + saveMessage + "')", true);
                    return;
                }
            }
            else if (GetQueryStringValue("PurchaseOrderID") != null)
            {
                FillControls.FillListBox(ref lstProductCode, lstODProductCodes, "ProductDescription", "SKUID");
            }
            //else if (GetQueryStringValue("PurchaseOrderID") == null && GetQueryStringValue("SiteId") == null)
            //{
            //    string saveMessage = WebCommon.getGlobalResourceValue("OTIFHitForPOExists");
            //    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('" + saveMessage + "')", true);
            //}


        }
        else
        {
            FillControls.FillListBox(ref lstProductCode, lstODProductCodes, "ProductDescription", "SKUID");
        }
    }
    protected void btnMoveRightAll_Click(object sender, EventArgs e)
    {
        if (lstProductCode.Items.Count > 0)
            FillControls.MoveAllItems(lstProductCode, lstselectedProductCode);
    }
    protected void btnMoveRight_Click(object sender, EventArgs e)
    {
        if (lstProductCode.SelectedItem != null)
        {
            FillControls.MoveOneItem(lstProductCode, lstselectedProductCode);
        }
    }
    protected void btnMoveLeft_Click(object sender, EventArgs e)
    {
        if (lstselectedProductCode.SelectedItem != null)
        {
            FillControls.MoveOneItem(lstselectedProductCode, lstProductCode);
            if (lstselectedProductCode.Items.Count > 0 )
            {
                lstselectedProductCode.SelectedIndex = 0;
            }
        }
    }
    protected void btnMoveLeftAll_Click(object sender, EventArgs e)
    {
        if (lstselectedProductCode.Items.Count > 0)
        {
            FillControls.MoveAllItems(lstselectedProductCode, lstProductCode);
        }
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (GetQueryStringValue("PurchaseOrderID") != null)
        {
            DeleteSelectedProductCodes();
        }

        OTIF_LeadTimeSKUBE oOTIF_LeadTimeSKUBE = new OTIF_LeadTimeSKUBE();
        OTIFReportBAL oOTIF_ReportBAL = new OTIFReportBAL();

        // oUp_PurchaseOrderDetailBE.RevisedDueDate = Common.GetMM_DD_YYYY(txtRevisedDate.innerControltxtDate.Value);
        //DateTime RevisedDueDate = Convert.ToDateTime(oUp_PurchaseOrderDetailBE.RevisedDueDate); 
        //DateTime now = DateTime.Now;
        //int result = DateTime.Compare(RevisedDueDate, now);
        //if (result == -1)
        //{
        oOTIF_LeadTimeSKUBE.Purchase_order = txtPurchaseOrderNumber.Text;
        if (GetQueryStringValue("SiteID") != null)
        {
            oOTIF_LeadTimeSKUBE.SiteID = Convert.ToInt32(GetQueryStringValue("SiteID"));
        }
        else
        {
            oOTIF_LeadTimeSKUBE.SiteID = Convert.ToInt32(ucSite.innerControlddlSite.SelectedItem.Value);
        }
        oOTIF_LeadTimeSKUBE.UserName = Session["UserName"].ToString();
        oOTIF_LeadTimeSKUBE.Reason = txtReason.Text;
        if (GetQueryStringValue("OrderRaised") == null)
        {
            oOTIF_LeadTimeSKUBE.Order_raised = ddlPurchaseOrderDate.SelectedItem.Text == "--Select--" ? (DateTime?)null : Common.GetMM_DD_YYYY(ddlPurchaseOrderDate.SelectedItem.Text);
        }
        else
        {
            oOTIF_LeadTimeSKUBE.Order_raised = Convert.ToDateTime(GetQueryStringValue("OrderRaised"));
        }
        if (lstselectedProductCode.Items.Count > 0)
        {
            if (lstselectedProductCode.Items.Count > 1)
            {
                oOTIF_LeadTimeSKUBE.SelectedProductCodes = lstselectedProductCode.Items[0].Value + ",";

                oOTIF_LeadTimeSKUBE.ProductDescription = lstselectedProductCode.Items[0].Text + ",";

                for (int i = 1; i < lstselectedProductCode.Items.Count - 1; i++)
                {
                    oOTIF_LeadTimeSKUBE.SelectedProductCodes += lstselectedProductCode.Items[i].Value + ",";

                    oOTIF_LeadTimeSKUBE.ProductDescription += lstselectedProductCode.Items[i].Text + ",";
                }
                oOTIF_LeadTimeSKUBE.SelectedProductCodes += lstselectedProductCode.Items[lstselectedProductCode.Items.Count - 1].Value;
                oOTIF_LeadTimeSKUBE.ProductDescription += lstselectedProductCode.Items[lstselectedProductCode.Items.Count - 1].Text;
            }
            else
            {
                oOTIF_LeadTimeSKUBE.SelectedProductCodes = lstselectedProductCode.Items[0].Value;
                oOTIF_LeadTimeSKUBE.ProductDescription = lstselectedProductCode.Items[0].Text;
            }
        }

        if (GetQueryStringValue("PurchaseOrderID") != null && GetQueryStringValue("SiteId") == null)
        {
            oOTIF_LeadTimeSKUBE.Action = "UpdateRevisedDatesSKU";

            if (GetQueryStringValue("OrderRaised") != null)
            {
                oOTIF_LeadTimeSKUBE.Order_raised = Convert.ToDateTime(GetQueryStringValue("OrderRaised").ToString());
            }
            else
                oOTIF_LeadTimeSKUBE.Order_raised = ddlPurchaseOrderDate.SelectedItem.Text == "--Select--" ? (DateTime?)null : Common.GetMM_DD_YYYY(ddlPurchaseOrderDate.SelectedItem.Text);

        }
        else if (GetQueryStringValue("PurchaseOrderID") != null && GetQueryStringValue("SiteId") != null)
        {
            oOTIF_LeadTimeSKUBE.Action = "UpdateRevisedDatesSKU";

            if (GetQueryStringValue("OrderRaised") != null)
            {
                oOTIF_LeadTimeSKUBE.Order_raised = Convert.ToDateTime(GetQueryStringValue("OrderRaised").ToString());
            }
            else
                oOTIF_LeadTimeSKUBE.Order_raised = ddlPurchaseOrderDate.SelectedItem.Text == "--Select--" ? (DateTime?)null : Common.GetMM_DD_YYYY(ddlPurchaseOrderDate.SelectedItem.Text);

        }

        else
        {
            oOTIF_LeadTimeSKUBE.Action = "AddRevisedDatesSKU";
        }
        StringBuilder AlreadyExclusionHitForPOMessage = new StringBuilder(); ;
        if (ViewState["ProductDesc"] != null)
        {
            string[] productDesc = Convert.ToString(ViewState["ProductDesc"]).TrimEnd(',').Split(',');

            if (lstselectedProductCode.Items.Count > 0)
            {
                for (int i = 0; i < productDesc.Length; i++)
                {
                    for (int j = 0; j < oOTIF_LeadTimeSKUBE.ProductDescription.Split(',').Length; j++)
                    {
                        if (Convert.ToString(oOTIF_LeadTimeSKUBE.ProductDescription.Split(',')[j]).Trim() == productDesc[i].Trim())
                        {
                            // string[] ProductSku = productDesc[i].Split('-');
                            // AlreadyExclusionHitForPOMessage.Append("- Please Remove odsku- " + productDesc[i] + " from the select list <br /> ");
                            AlreadyExclusionHitForPOMessage.Append(WebCommon.getGlobalResourceValue("AlreadyExclusionHitForPO")).Replace("{ProductDescription}", productDesc[i].Split('-').ElementAtOrDefault(0));
                            // AlreadyExclusionHitForPOMessage.AppendLine();
                            // string AlreadyExclusionHitForPOMessage = WebCommon.getGlobalResourceValue("AlreadyExclusionHitForPO");                        
                            break;
                        }
                    }
                }
            }
            
        }
        if (string.IsNullOrEmpty(AlreadyExclusionHitForPOMessage.ToString()))
        {

            int? iResult = oOTIF_ReportBAL.addEditUP_RevisedSKULeadTimeDetailBAL(oOTIF_LeadTimeSKUBE);
            oOTIF_ReportBAL = null;

            if (iResult == 0)
            {
                string saveMessage = WebCommon.getGlobalResourceValue("SavedMessage");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('" + saveMessage + "')", true);
                if (GetQueryStringValue("PreviousPage") != null && Convert.ToString(GetQueryStringValue("PreviousPage")) == "PORec")
                {
                    EncryptQueryString("../Appointment/Reports/ScheduledPOReconciliation.aspx?PreviousPage=OTIFHit");
                }
                else
                {
                    EncryptQueryString("LeadTimeCalculationsSkuOverview.aspx?PreviousPage=PO");
                }

            }

        }
        else
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('" + AlreadyExclusionHitForPOMessage + "')", true);
        }


        


        //}
        //else if(result==1)
        //{
        //    string saveMessage = WebCommon.getGlobalResourceValue("NotDateValidMessage");
        //    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('" + saveMessage + "')", true);
        //}


    }

    //protected void btnDelete_Click(object sender, EventArgs e)
    //{
    //    OTIF_LeadTimeSKUBE oOTIF_LeadTimeSKUBE = new OTIF_LeadTimeSKUBE();
    //    OTIFReportBAL oOTIF_ReportBAL = new OTIFReportBAL();

    //    oOTIF_LeadTimeSKUBE.Action = "DeleteApplyOtifHit";

    //    oOTIF_LeadTimeSKUBE.Purchase_order = txtPurchaseOrderNumber.Text;

    //    if (GetQueryStringValue("SiteID") != null)
    //    {
    //        oOTIF_LeadTimeSKUBE.SiteID = Convert.ToInt32(GetQueryStringValue("SiteID"));
    //    }
    //    else
    //    {
    //        oOTIF_LeadTimeSKUBE.SiteID = Convert.ToInt32(ucSite.innerControlddlSite.SelectedItem.Value);
    //    }

    //    if (GetQueryStringValue("UserName") != null)
    //    {
    //        oOTIF_LeadTimeSKUBE.UserName = Convert.ToString(GetQueryStringValue("UserName")); // Who Applied The Hit
    //    }

    //    if (GetQueryStringValue("DateApplied") != null)
    //    {
    //        oOTIF_LeadTimeSKUBE.DateApplied = Convert.ToDateTime(GetQueryStringValue("DateApplied")); 
    //    }

    //    oOTIF_LeadTimeSKUBE.Reason = txtReason.Text;

    //    oOTIF_LeadTimeSKUBE.UserID = Convert.ToInt32(Session["UserID"]); // User Deleting the items

    //    if (GetQueryStringValue("OrderRaised") == null)
    //    {
    //        oOTIF_LeadTimeSKUBE.Order_raised = ddlPurchaseOrderDate.SelectedItem.Text == "--Select--" ? (DateTime?)null : Common.GetMM_DD_YYYY(ddlPurchaseOrderDate.SelectedItem.Text);
    //    }
    //    else
    //    {
    //        oOTIF_LeadTimeSKUBE.Order_raised = Convert.ToDateTime(GetQueryStringValue("OrderRaised"));
    //    }

    //    if (lstselectedProductCode.Items.Count > 0)
    //    {
    //        if (lstselectedProductCode.Items.Count > 1)
    //        {
    //            oOTIF_LeadTimeSKUBE.SelectedProductCodes = lstselectedProductCode.Items[0].Value + ",";

    //            oOTIF_LeadTimeSKUBE.ProductDescription = lstselectedProductCode.Items[0].Text + ",";

    //            for (int i = 1; i < lstselectedProductCode.Items.Count - 1; i++)
    //            {
    //                oOTIF_LeadTimeSKUBE.SelectedProductCodes += lstselectedProductCode.Items[i].Value + ",";

    //                oOTIF_LeadTimeSKUBE.ProductDescription += lstselectedProductCode.Items[i].Text + ",";
    //            }
    //            oOTIF_LeadTimeSKUBE.SelectedProductCodes += lstselectedProductCode.Items[lstselectedProductCode.Items.Count - 1].Value;
    //            oOTIF_LeadTimeSKUBE.ProductDescription += lstselectedProductCode.Items[lstselectedProductCode.Items.Count - 1].Text;
    //        }
    //        else
    //        {
    //            oOTIF_LeadTimeSKUBE.SelectedProductCodes = lstselectedProductCode.Items[0].Value;
    //            oOTIF_LeadTimeSKUBE.ProductDescription = lstselectedProductCode.Items[0].Text;
    //        }
    //    }

    //    StringBuilder NotHitForPOMessage = new StringBuilder(); ;
        

    //    if (ViewState["ProductDescApplyOtifHit"] != null)
    //    {
    //        string[] productDesc = Convert.ToString(ViewState["ProductDescApplyOtifHit"]).TrimEnd(',').Split(',');

    //        for (int j = 0; j < oOTIF_LeadTimeSKUBE.ProductDescription.Split(',').Length; j++)
    //        {
    //            if (!productDesc.Contains(Convert.ToString(oOTIF_LeadTimeSKUBE.ProductDescription.Split(',')[j]).Trim()))
    //            {
    //                NotHitForPOMessage.Append(WebCommon.getGlobalResourceValue("NotHitForPO")).Replace("{ProductDescription}", Convert.ToString(oOTIF_LeadTimeSKUBE.ProductDescription.Split(',')[j]));
    //            }

    //        }
    //    }

    //    if (string.IsNullOrEmpty(NotHitForPOMessage.ToString()))
    //    {

    //        int? iResult = oOTIF_ReportBAL.DeleteOtifHitDetailsBAL(oOTIF_LeadTimeSKUBE);
    //        oOTIF_ReportBAL = null;

    //        if (iResult == 0)
    //        {
    //            string saveMessage = WebCommon.getGlobalResourceValue("SavedMessage");
    //            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('" + saveMessage + "')", true);
    //            if (GetQueryStringValue("PreviousPage") != null && Convert.ToString(GetQueryStringValue("PreviousPage")) == "PORec")
    //            {
    //                EncryptQueryString("../Appointment/Reports/ScheduledPOReconciliation.aspx?PreviousPage=OTIFHit");
    //            }
    //            else
    //            {
    //                EncryptQueryString("LeadTimeCalculationsSkuOverview.aspx?PreviousPage=PO");
    //            }

    //        }

    //    }
    //    else
    //    {
    //        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('" + NotHitForPOMessage + "')", true);
    //    }



    //}

    protected void btnBack_Click(object sender, EventArgs e)
    {
        if (GetQueryStringValue("PreviousPage") != null && Convert.ToString(GetQueryStringValue("PreviousPage")) == "PORec")
        {
            EncryptQueryString("../Appointment/Reports/ScheduledPOReconciliation.aspx?PreviousPage=OTIFHit");
        }
        else
        {
            EncryptQueryString("LeadTimeCalculationsSkuOverview.aspx?PreviousPage=PO");
        }
    }

    private void getRevisedLeadByPurchaseOrder()
    {
        if (GetQueryStringValue("PurchaseOrderID") != null && GetQueryStringValue("OrderRaised") != null)
        {
            trSite.Visible = false;
            trPurchaseOrderDate.Visible = false;
            trVendor.Visible = false;
            txtPurchaseOrderNumber.Enabled = false;

            //Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE = new Up_PurchaseOrderDetailBE();
            //UP_PurchaseOrderDetailBAL oUP_PurchaseOrderDetailBAL = new UP_PurchaseOrderDetailBAL();
            //oUp_PurchaseOrderDetailBE.Action = "GetRevisedLeadListSKU";
            //oUp_PurchaseOrderDetailBE.PurchaseOrderID = Convert.ToInt32(GetQueryStringValue("PurchaseOrderID"));

            //List<Up_PurchaseOrderDetailBE> lstRevisedLeadListing = new List<Up_PurchaseOrderDetailBE>();
            OTIF_LeadTimeSKUBE oOTIF_LeadTimeSKUBE = new OTIF_LeadTimeSKUBE();
            OTIFReportBAL oOTIF_ReportBAL = new OTIFReportBAL();

            oOTIF_LeadTimeSKUBE.Action = "GetRevisedLeadListSKU";
            oOTIF_LeadTimeSKUBE.Purchase_order = Convert.ToString(GetQueryStringValue("PurchaseOrderID"));
            if (GetQueryStringValue("OrderRaised") != null)
            {
                oOTIF_LeadTimeSKUBE.Order_raised = Convert.ToDateTime(GetQueryStringValue("OrderRaised"));
            }

            oOTIF_LeadTimeSKUBE.GridCurrentPageNo = 1;
            oOTIF_LeadTimeSKUBE.GridPageSize = 10;

            List<OTIF_LeadTimeSKUBE> lstRevisedLeadListing = new List<OTIF_LeadTimeSKUBE>();
            lstRevisedLeadListing = oOTIF_ReportBAL.GetRevisedSKULeadListBAL(oOTIF_LeadTimeSKUBE);
            oOTIF_ReportBAL = null;
            //lstRevisedLeadListing = oUP_PurchaseOrderDetailBAL.GetRevisedSKULeadListBAL(oUp_PurchaseOrderDetailBE);
            // oUP_PurchaseOrderDetailBAL = null;
            if (lstRevisedLeadListing.Count > 0)
            {
                txtPurchaseOrderNumber.Text = lstRevisedLeadListing[0].Purchase_order;
                //txtRevisedDate.innerControltxtDate.Value = Convert.ToDateTime(lstRevisedLeadListing[0].RevisedDueDate).ToString("dd/MM/yyyy");

                txtReason.Text = lstRevisedLeadListing[0].Reason;
                GetOPProductCodes();
                getSelectedPurchaseOrder();
                btnGo.Visible = false;
            }
        }
    }

    private void getSelectedPurchaseOrder()
    {
        Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE = new Up_PurchaseOrderDetailBE();
        UP_PurchaseOrderDetailBAL oUP_PurchaseOrderDetailBAL = new UP_PurchaseOrderDetailBAL();
        oUp_PurchaseOrderDetailBE.Action = "GetSelectedSKUCodes";
        oUp_PurchaseOrderDetailBE.Purchase_order = txtPurchaseOrderNumber.Text;
        //oUp_PurchaseOrderDetailBE.RevisedDueDate = Common.GetMM_DD_YYYY(Date);
        if (GetQueryStringValue("OrderRaised") != null)
        {
            oUp_PurchaseOrderDetailBE.Order_raised = Convert.ToDateTime(GetQueryStringValue("OrderRaised").ToString());
        }
        else
            oUp_PurchaseOrderDetailBE.Order_raised = ddlPurchaseOrderDate.SelectedItem.Text == "--Select--" ? (DateTime?)null : Common.GetMM_DD_YYYY(ddlPurchaseOrderDate.SelectedItem.Text);


        List<Up_PurchaseOrderDetailBE> lstODProductCodes = new List<Up_PurchaseOrderDetailBE>();
        lstODProductCodes = oUP_PurchaseOrderDetailBAL.GetSKUCodesBAL(oUp_PurchaseOrderDetailBE);
        oUP_PurchaseOrderDetailBAL = null;
        if (lstODProductCodes.Count > 0 && lstODProductCodes != null)
        {
            FillControls.FillListBox(ref lstselectedProductCode, lstODProductCodes, "ProductDescription", "SKUID");

            foreach (var item in lstODProductCodes)
            {
                ViewState["ProductDescApplyOtifHit"] += item.ProductDescription + ",";
                ViewState["ProductDescApplyOtifHitSelectedValue"] +=  item.SKUID + ",";
            }
        }
        else
        {
            //string saveMessage = WebCommon.getGlobalResourceValue("NoProductCodeExist");
            //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('" + saveMessage + "')", true);
        }
        foreach (ListItem lst in lstselectedProductCode.Items)
        {
            if (lstProductCode.Items.Contains(lst))
                lstProductCode.Items.Remove(lst);
        }
    }


    protected void ddlPurchaseOrderDate_SelectedIndexChanged(object sender, EventArgs e)
    {
        lblVendorValue.Text = ddlPurchaseOrderDate.SelectedItem.Value.ToString();
    }

    protected void txtPurchaseOrder_TextChanged(object sender, EventArgs e)
    {
        GetPurchaseOrderList();

    }
    private void GetPurchaseOrderList()
    {
        DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
        DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();

        oDiscrepancyBE.Action = "GetPurchaseOrderDate";
        oDiscrepancyBE.SiteID = Convert.ToInt32(ucSite.innerControlddlSite.SelectedValue);
        oDiscrepancyBE.PurchaseOrderNumber = txtPurchaseOrderNumber.Text.Trim();

        List<DiscrepancyBE> lstDetails = oDiscrepancyBAL.GetPurchaseOrderDateListBAL(oDiscrepancyBE);
        oDiscrepancyBAL = null;
        if (lstDetails.Count > 0 && lstDetails != null)
        {
            FillControls.FillDropDown(ref ddlPurchaseOrderDate, lstDetails, "PurchaseOrder_Order_raised", "VendorNoName");
            lblVendorValue.Text = ddlPurchaseOrderDate.SelectedItem.Value.ToString();

        }
        else
        {
            string saveMessage = WebCommon.getGlobalResourceValue("InvalidPurchaseOrder");
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('" + saveMessage + "')", true);
        }
    }

    private void DeleteSelectedProductCodes()
    {
        OTIF_LeadTimeSKUBE oOTIF_LeadTimeSKUBE = new OTIF_LeadTimeSKUBE();
        OTIFReportBAL oOTIF_ReportBAL = new OTIFReportBAL();

        oOTIF_LeadTimeSKUBE.Action = "DeleteApplyOtifHit";

        oOTIF_LeadTimeSKUBE.Purchase_order = txtPurchaseOrderNumber.Text;

        if (GetQueryStringValue("SiteID") != null)
        {
            oOTIF_LeadTimeSKUBE.SiteID = Convert.ToInt32(GetQueryStringValue("SiteID"));
        }
        else
        {
            oOTIF_LeadTimeSKUBE.SiteID = Convert.ToInt32(ucSite.innerControlddlSite.SelectedItem.Value);
        }

        if (GetQueryStringValue("UserName") != null)
        {
            oOTIF_LeadTimeSKUBE.UserName = Convert.ToString(GetQueryStringValue("UserName")); // Who Applied The Hit
        }

        if (GetQueryStringValue("DateApplied") != null)
        {
            oOTIF_LeadTimeSKUBE.DateApplied = Convert.ToDateTime(GetQueryStringValue("DateApplied"));
        }

        oOTIF_LeadTimeSKUBE.Reason = txtReason.Text;

        oOTIF_LeadTimeSKUBE.UserID = Convert.ToInt32(Session["UserID"]); // User Deleting the items

        if (GetQueryStringValue("OrderRaised") == null)
        {
            oOTIF_LeadTimeSKUBE.Order_raised = ddlPurchaseOrderDate.SelectedItem.Text == "--Select--" ? (DateTime?)null : Common.GetMM_DD_YYYY(ddlPurchaseOrderDate.SelectedItem.Text);
        }
        else
        {
            oOTIF_LeadTimeSKUBE.Order_raised = Convert.ToDateTime(GetQueryStringValue("OrderRaised"));
        }

        string[] productCodesOld = Convert.ToString(ViewState["ProductDescApplyOtifHitSelectedValue"]).TrimEnd(',').Split(',');
        //ViewState["ProductDescApplyOtifHitSelectedValue"]

        List<ListItemData> ProductCodesOld = new List<ListItemData>();

        foreach (var item in productCodesOld)
        {
            ListItemData obj = new ListItemData();
            obj.SelectedProductCodesToBeDeleted = item;
            ProductCodesOld.Add(obj);
        }


        if (lstselectedProductCode.Items.Count > 0)
        {
            if (lstselectedProductCode.Items.Count > 1)
            {
                oOTIF_LeadTimeSKUBE.SelectedProductCodes = lstselectedProductCode.Items[0].Value + ",";

                oOTIF_LeadTimeSKUBE.ProductDescription = lstselectedProductCode.Items[0].Text + ",";

                for (int i = 1; i < lstselectedProductCode.Items.Count - 1; i++)
                {
                    oOTIF_LeadTimeSKUBE.SelectedProductCodes += lstselectedProductCode.Items[i].Value + ",";

                    oOTIF_LeadTimeSKUBE.ProductDescription += lstselectedProductCode.Items[i].Text + ",";
                }
                oOTIF_LeadTimeSKUBE.SelectedProductCodes += lstselectedProductCode.Items[lstselectedProductCode.Items.Count - 1].Value;
                oOTIF_LeadTimeSKUBE.ProductDescription += lstselectedProductCode.Items[lstselectedProductCode.Items.Count - 1].Text;
            }
            else
            {
                oOTIF_LeadTimeSKUBE.SelectedProductCodes = lstselectedProductCode.Items[0].Value;
                oOTIF_LeadTimeSKUBE.ProductDescription = lstselectedProductCode.Items[0].Text;
            }
        }
        List<ListItemData> ListDataToBeDeleted = new List<ListItemData>();

        if (oOTIF_LeadTimeSKUBE.SelectedProductCodes != null)
        {
            List<ListItemData> ProductCodesNew = new List<ListItemData>();

            foreach (var item in oOTIF_LeadTimeSKUBE.SelectedProductCodes.Split(','))
            {
                ListItemData obj = new ListItemData();
                obj.SelectedProductCodesToBeDeleted = item;
                ProductCodesNew.Add(obj);
            }

            ProductCodesOld.RemoveAll(r => ProductCodesNew.Any(a => a.SelectedProductCodesToBeDeleted == r.SelectedProductCodesToBeDeleted));
            ListDataToBeDeleted = ProductCodesOld;
        }
        else
        {
            ListDataToBeDeleted = ProductCodesOld;
        }

        oOTIF_LeadTimeSKUBE.SelectedProductCodes = string.Empty;
       

        foreach (ListItemData items in ListDataToBeDeleted)
        {
            oOTIF_LeadTimeSKUBE.SelectedProductCodes += items.SelectedProductCodesToBeDeleted + ",";            
        }

        oOTIF_LeadTimeSKUBE.SelectedProductCodes = oOTIF_LeadTimeSKUBE.SelectedProductCodes.TrimEnd(',');
        

       
        if (ListDataToBeDeleted != null && ListDataToBeDeleted.Count>0)
        {
            int? iResult = oOTIF_ReportBAL.DeleteOtifHitDetailsBAL(oOTIF_LeadTimeSKUBE);
            oOTIF_ReportBAL = null;            
        }        
    }


    public class ListItemData
    {
        public string SelectedProductCodesToBeDeleted { get; set; }
    }
}
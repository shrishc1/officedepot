﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ViewOTIFReport.aspx.cs" Inherits="ViewOTIFReport"
    MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master" MaintainScrollPositionOnPostback="true" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2>
        <cc1:ucLabel ID="lblViewOTIFReports" runat="server" Text="View OTIF Reports"></cc1:ucLabel>
    </h2>

    <div class="right-shadow">
        <div class="formbox">
            <cc1:ucPanel ID="pnlSummaryReports" runat="server" GroupingText="Summary Report(s)"
                CssClass="fieldset-form">
                <cc1:ucLabel ID="lblClickOnYear_1" Text="Please click on year to view report" runat="server"></cc1:ucLabel><br />
                <br />
                <div style="width: 18%; vertical-align: top" class="left-nav-month-list">
                    <cc1:ucGridView ID="grdReportLinksVendor" Width="100%" runat="server" AutoGenerateColumns="False"
                        ShowHeader="false" CellPadding="0" GridLines="None" CssClass="NoAnchor">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkReport" runat="server" Text='<%# String.Format("{0}",Eval("Year")) %>'
                                        CommandName="ScoreCardReportVendor" OnCommand="btnView_Click"></asp:LinkButton>
                                </ItemTemplate>
                                <ItemStyle Height="20px" HorizontalAlign="Left" />
                            </asp:TemplateField>
                        </Columns>
                    </cc1:ucGridView>
                </div>
            </cc1:ucPanel>
            <cc1:ucPanel ID="pnlMonthlyReport" runat="server" GroupingText="Monthly Detailed Report(s)"
                CssClass="fieldset-form">
                <div>
                    <div class="clear">
                    </div>

                    <table>
                        <tr>
                            <td style="text-align: left; width: 100%" colspan="3">
                                <br />
                                <cc1:ucLabel ID="lblViewOTIFReport" runat="server"></cc1:ucLabel>
                            </td>
                        </tr>
                    </table>

                    <div id="divSearchButtons" style="margin-bottom: 15px; text-align: center;">
                        <table width="90%" cellspacing="1" cellpadding="0" border="0" align="center" class="top-settings">
                            <tr>
                                <td style="font-weight: bold; text-align: right; width: 30%">
                                    <cc1:ucLabel ID="lblVendor" runat="server"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold; text-align: center; width: 2%">:
                                </td>
                                <td style="font-weight: bold; text-align: left; width: 70%">
                                    <cc1:ucDropdownList ID="ddlVendor" runat="server">
                                    </cc1:ucDropdownList>
                                </td>
                            </tr>

                        </table>
                    </div>

                    <div runat="server" id="divcurrent" style="width: 18%; float: left; vertical-align: top; margin-right: 15px; margin-top: 15px;" class="left-nav-month-list">
                        <cc1:ucGridView ID="grdReportLinks" Width="100%" runat="server" AutoGenerateColumns="False"
                            ShowHeader="false" CellPadding="0" GridLines="None" CssClass="NoAnchor">
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkReport" runat="server" Text='<%# String.Format("{0} - {1}", Eval("Month"),Eval("Year")) %>'
                                            CommandName="OtifReport" OnCommand="btnView_Click"></asp:LinkButton>
                                    </ItemTemplate>
                                    <ItemStyle Height="20px" HorizontalAlign="Left" />
                                </asp:TemplateField>
                            </Columns>
                        </cc1:ucGridView>
                    </div>

                    <div style="width: 18%; float: left; vertical-align: top; margin-top: 15px;" class="left-nav-month-list">
                        <cc1:ucGridView ID="grdReportLinksPrev" Width="100%" runat="server" AutoGenerateColumns="False"
                            ShowHeader="false" CellPadding="0" GridLines="None" CssClass="NoAnchor">
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkReport" runat="server" Text='<%# String.Format("{0} - {1}", Eval("Month"),Eval("Year")) %>'
                                            CommandName="OtifReport" OnCommand="btnView_Click"></asp:LinkButton>
                                    </ItemTemplate>
                                    <ItemStyle Height="20px" HorizontalAlign="Left" />
                                </asp:TemplateField>
                            </Columns>
                        </cc1:ucGridView>
                    </div>

                    <div runat="server" id="CurrentNew" style="width: 18%; float: left; vertical-align: top; margin-top: 15px; margin-left: 15px;" class="left-nav-month-list">
                        <cc1:ucGridView ID="grdReportLinksPrev2" Width="100%" runat="server" AutoGenerateColumns="False"
                            ShowHeader="false" CellPadding="0" GridLines="None" CssClass="NoAnchor">
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkReport" runat="server" Text='<%# String.Format("{0} - {1}", Eval("Month"),Eval("Year")) %>'
                                            CommandName="OtifReport" OnCommand="btnView_Click"></asp:LinkButton>
                                    </ItemTemplate>
                                    <ItemStyle Height="20px" HorizontalAlign="Left" />
                                </asp:TemplateField>
                            </Columns>
                        </cc1:ucGridView>
                    </div>

                    <div class="clear">
                    </div>
                </div>
            </cc1:ucPanel>
        </div>
    </div>

</asp:Content>

﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="DeletedOtifExclusionRevisedView.aspx.cs" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
     Inherits="ModuleUI_OnTimeInFull_DeletedOtifExclusionRevisedView" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="../../CommonUI/UserControls/ucAddButton.ascx" TagName="ucAddButton"
    TagPrefix="cc2" %>
<%@ Register Src="../../CommonUI/UserControls/ucCountry.ascx" TagName="ucCountry"
    TagPrefix="uc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectVendor.ascx" TagName="MultiSelectVendor"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/ucSite.ascx" TagName="ucSite" TagPrefix="cc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript">
        $(document).ready(function () {
            $("body").keypress(function (e) {
                if (e.which == 13 || e.keyCode == 13) {
                    $('#<%=btnSearch.ClientID %>').click();
                    return false;
                }
            });
        });

        $(document).ready(function () {
            // HideShowReportView();
            var fullDate = new Date();
            var twoDigitMonth = ((fullDate.getMonth().length + 1) === 1) ? (fullDate.getMonth() + 1) : '0' + (fullDate.getMonth() + 1);
            var currentDate = fullDate.getDate() + "/" + twoDigitMonth + "/" + fullDate.getFullYear();
            $('#txtToDate,#txtFromDate').datepicker({ showOtherMonths: true,
                selectOtherMonths: true, changeMonth: true, changeYear: true,
                minDate: new Date(2013, 0, 1),
                yearRange: '2013:+100',
                dateFormat: 'dd/mm/yy'
            });


        });
        function setValue1(target) {
            document.getElementById('<%=hdnJSFromDt.ClientID %>').value = target.value;
        }

        function setValue2(target) {
            document.getElementById('<%=hdnJSToDt.ClientID %>').value = target.value;
        }


    </script>
    <asp:ScriptManager ID="SM1" runat="server">
    </asp:ScriptManager>
    <h2>
        <cc1:ucLabel ID="lblDeletedOtifExclusionRevisedHeading" runat="server" Text="Deleted OTIF Hits/Exclusions/Revised Dates"></cc1:ucLabel>
    </h2>
     <asp:HiddenField ID="hdnJSFromDt" runat="server" />
    <asp:HiddenField ID="hdnJSToDt" runat="server" />
    <div class="button-row">        
        <cc1:ucButton ID="btnExportToExcel" runat="server" CssClass="exporttoexcel" OnClick="btnExport_Click"
            Width="109px" Height="20px" Visible="false" />
    </div>
 
    <table width="100%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
        <tr>
            <td style="font-weight: bold; width: 2%">
                <cc1:ucLabel ID="lblSite" runat="server" Text="Site"></cc1:ucLabel>
            </td>
            <td style="font-weight: bold; width: 1%">
                :
            </td>
            <td style="width: 12%">
                <cc2:ucSite ID="ucSite" runat="server" />
            </td> 
             <td style="width: 3% ;font-weight: bold;">
                 <cc1:ucLabel ID="lblSkuWithHash" runat="server"></cc1:ucLabel>
             </td>
             <td style=" width: 1%; font-weight: bold;">
                :
             </td>
               <td style="width: 16%; font-weight: bold;">
                   <cc1:ucTextbox ID="txtSku" Width="150px" runat="server"></cc1:ucTextbox>
             </td>
        </tr>
        <tr>
            <td style="font-weight: bold;">
                <cc1:ucLabel ID="lblPoNo" runat="server" Text="PO #"></cc1:ucLabel>
            </td>
            <td style="font-weight: bold;">
                :
            </td>
            <td style="font-weight: bold;">
                
                <cc1:ucTextbox ID="txtPOSearch" runat="server" Width="146px"></cc1:ucTextbox>
            </td>
            <td style="font-weight: bold;">
                <cc1:ucLabel ID="lblCATCode" runat="server"></cc1:ucLabel>
            </td>
              <td style="font-weight: bold;">
                  :
              </td>
              <td style="font-weight: bold;">
                   <cc1:ucTextbox ID="txtCATCode" Width="150px" runat="server"></cc1:ucTextbox>
              </td>
        </tr>
        <tr>
            <td style="font-weight: bold;">
                <cc1:ucLabel ID="lblVendorNo" runat="server" Text="Vendor No"></cc1:ucLabel>
            </td>
            <td style="font-weight: bold;">
                :
            </td>
            <td style="font-weight: bold;" colspan="4">
                <cc1:MultiSelectVendor runat="server" ID="msVendor" />
            </td>
        </tr>
        <tr>
        <td  style="font-weight: bold;  text-align: left">
            <cc1:ucLabel ID="lblDateFrom" runat="server" Text="Date From" Width="80px"  ></cc1:ucLabel>
        </td>
         <td style="font-weight: bold;">
                :
            </td>
        <td style="font-weight: bold;  text-align: left">
         <cc1:ucTextbox ID="txtFromDate" ClientIDMode="Static" runat="server" onchange="setValue1(this)"
                  ReadOnly="True" Width="100px" />
           &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
           <cc1:ucLabel ID="lblDateTo" runat="server" Text="Date To :" Width="60px"  ></cc1:ucLabel>
                <cc1:ucTextbox ID="txtToDate"  ClientIDMode="Static" runat="server" onchange="setValue2(this)"
                    ReadOnly="True" Width="100px" /> 
            </td>                                
        </tr> 
        <tr>
        <td  style="font-weight: bold;  text-align: left">
           
        </td>
         <td style="font-weight: bold;">
                
            </td>
        <td style="font-weight: bold;  text-align: left">
            <br />
           
        <cc1:ucRadioButton ID="rdoApplyHit" runat="server" Text="Apply Hit" Checked="true"   GroupName="Type"  />
            <cc1:ucRadioButton ID="rdoApplyExclusion" runat="server" Text="Apply Exclusion"    GroupName="Type"  />
         <cc1:ucRadioButton ID="rdoRevisedDate" runat="server" Text="Revised Date" GroupName="Type"  />                                                               
           </td>   
            <td align="right" colspan="3">
                 <br />
                <cc1:ucButton ID="btnSearch" runat="server" CausesValidation="true" OnClick="btnGo_Click"></cc1:ucButton>
            </td>                                     
        </tr>       

    </table>

    <div style="width: 100%; overflow: auto" id="divPrint">
        <table cellspacing="1" cellpadding="0" border="0" align="center" width="100%">
            <tr>
                <td>
                    <cc1:ucGridView ID="gvDisplayRecords" ClientIDMode="Static" OnPageIndexChanging="gvDisplayRecords_PageIndexChanging"
                         AutoGenerateColumns="true"  Width="100%" runat="server" CssClass="grid modify-grid" AllowPaging="true" PageSize="50"
                        EmptyDataText="No Records Found" EmptyDataRowStyle-ForeColor="Red" EmptyDataRowStyle-HorizontalAlign="Center">
                        <RowStyle HorizontalAlign="Center"></RowStyle>
                    </cc1:ucGridView>                    
                </td>
            </tr>
            </table>
        <cc1:ucGridView ID="grdBindExport" Visible="false" ClientIDMode="Static" Width="100%"
                            Height="80%" runat="server" CssClass="grid" GridLines="Both" 
                            AutoGenerateColumns="true">                          
                            
                        </cc1:ucGridView>
    </div>

    </asp:Content>
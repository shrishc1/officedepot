﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master" AutoEventWireup="true" 
CodeFile="ApplyOTIFExclusionEdit.aspx.cs" Inherits="ApplyOTIFExclusionEdit" %>

<%--<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
</asp:Content>--%>

<%@ Register TagName="ucDate" TagPrefix="cc2" Src="~/CommonUI/UserControls/ucDate.ascx" %>
<%@ Register Src="../../CommonUI/UserControls/ucSite.ascx" TagName="ucSite" TagPrefix="cc2" %>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script language="javascript" type="text/javascript">
        function checkProductCodeSelected(source, args) {
            var lstSelectedProductCode = document.getElementById('<%=lstselectedProductCode.ClientID %>');

            if (lstSelectedProductCode.options.length < 1) {

                args.IsValid = false;

            }
        }
    
    </script>
    <asp:ScriptManager ID="SP1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UP1" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <asp:RequiredFieldValidator ID="rfvRevisedDateCheck" runat="server" Display="None"
                ControlToValidate="txtRevisedDate$txtUCDate" ValidateEmptyText="true" ValidationGroup="Save"
                SetFocusOnError="true"></asp:RequiredFieldValidator>
            <asp:ValidationSummary ID="VS" runat="server" ValidationGroup="Save" ShowMessageBox="true"
                ShowSummary="false" />
            <h2>
                <cc1:ucLabel ID="lblApplyOTIFExclusion" runat="server" Text="Apply OTIF Exclusion"></cc1:ucLabel>                
            </h2>
            <div class="right-shadow">
                <div class="formbox">
                    <table width="40%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                        <tr id="trSite" runat="server">
                            <td style="width: 10%">
                            </td>
                            <td style="font-weight: bold; width: 40%">
                                <cc1:ucLabel ID="lblSitePrefixName" runat="server" Text="Site"></cc1:ucLabel>
                            </td>
                            <td style="width: 5%">
                                :
                            </td>
                            <td style="width: 35%">
                                <cc2:ucSite ID="ucSite" runat="server" />
                            </td>
                            <td style="width: 10%">
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 10%">
                            </td>
                            <td style="font-weight: bold; width: 40%">
                                <cc1:ucLabel ID="lblPurchaseOrderNumber" runat="server" Text="Purchase Order Number"
                                    isRequired="true"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold; width: 5%">
                                :
                            </td>
                            <td style="font-weight: bold; width: 35%">
                                <cc1:ucTextbox ID="txtPurchaseOrderNumber" runat="server" Width="60px" MaxLength="11"
                                    AutoPostBack="true" OnTextChanged="txtPurchaseOrder_TextChanged"></cc1:ucTextbox>
                                &nbsp;
                                <asp:RequiredFieldValidator ID="rfvPurchaseOrderNumberValidation" runat="server"
                                    ControlToValidate="txtPurchaseOrderNumber" Display="None" ValidationGroup="Go">
                                </asp:RequiredFieldValidator>
                            </td>
                            <td style="width: 10%">
                            </td>
                        </tr>
                        <tr id="trPurchaseOrderDate" runat="server">
                            <td style="width: 10%">
                            </td>
                            <td style="font-weight: bold;">
                                <cc1:ucLabel ID="lblPurchaseOrderDate" runat="server" Text="Purchase Order Date"></cc1:ucLabel>
                            </td>
                            <td>
                                :
                            </td>
                            <td>
                                <cc1:ucDropdownList ID="ddlPurchaseOrderDate" runat="server" Width="150px" AutoPostBack="True"
                                    OnSelectedIndexChanged="ddlPurchaseOrderDate_SelectedIndexChanged">
                                    <asp:ListItem Text="--Select--" Value="0"></asp:ListItem>
                                </cc1:ucDropdownList>
                            </td>
                            <td style="width: 10%">
                            </td>
                        </tr>
                        <tr id="trVendor" runat="server">
                            <td style="width: 10%">
                            </td>
                            <td style="font-weight: bold;">
                                <cc1:ucLabel ID="lblVendor" runat="server" Text="Vendor"></cc1:ucLabel>
                            </td>
                            <td>
                                :
                            </td>
                            <td>
                                <cc1:ucLabel ID="lblVendorValue" runat="server"></cc1:ucLabel>
                            </td>
                            <td style="width: 10%">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="5" align="center">
                                <cc1:ucButton ID="btnGo" runat="server" Text="Go" CssClass="button" OnClick="btnGo_Click"
                                    ValidationGroup="Go" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <asp:ValidationSummary ID="vSummary" runat="server" ShowMessageBox="true" ShowSummary="false"
                                    Style="color: Red" ValidationGroup="Go" />
                            </td>
                        </tr>
                    </table>
                    <cc1:ucPanel ID="pnlOfficeDepotProductCode" runat="server" GroupingText="Office Depot Product Code"
                        CssClass="fieldset-form">
                        <table width="80%" cellspacing="5" cellpadding="0">
                            <tr align="left">
                                <td width="40%">
                                    <cc1:ucListBox ID="lstProductCode" runat="server" Height="200px" Width="320px">
                                    </cc1:ucListBox>
                                </td>
                                <td valign="middle" align="center" width="20%">
                                    <div>
                                        <cc1:ucButton ID="btnMoveRightAll" runat="server" Text=">>" CssClass="button" Width="35px"
                                            OnClick="btnMoveRightAll_Click" /></div>
                                    &nbsp;
                                    <div>
                                        <cc1:ucButton ID="btnMoveRight" runat="server" Text=">" CssClass="button" Width="35px"
                                            OnClick="btnMoveRight_Click" /></div>
                                    &nbsp;
                                    <div>
                                        <cc1:ucButton ID="btnMoveLeft" runat="server" Text="<" CssClass="button" Width="35px"
                                            OnClick="btnMoveLeft_Click" /></div>
                                    &nbsp;
                                    <div>
                                        <cc1:ucButton ID="btnMoveLeftAll" runat="server" Text="<<" CssClass="button" Width="35px"
                                            OnClick="btnMoveLeftAll_Click" /></div>
                                </td>
                                <td width="40%">
                                    <cc1:ucListBox ID="lstselectedProductCode" runat="server" Height="200px" Width="320px">
                                    </cc1:ucListBox>
                                    <asp:CustomValidator ID="cusvNoSelectedProductCode" runat="server" ClientValidationFunction="checkProductCodeSelected"
                                        ValidationGroup="Save" Display="None"></asp:CustomValidator>
                                </td>
                            </tr>
                        </table>
                        <table width="80%">
                            <tr>
                                <td style="font-weight: bold;" width="10%">
                                    <cc1:ucLabel ID="lblReason" runat="server" Text="Reason" isRequired="true"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold;" width="1%">
                                    :
                                </td>
                                <td style="font-weight: bold;" align="left" width="89%">
                                    <cc2:ucDate ID="txtRevisedDate" Visible="false" runat="server" AutoPostBack="false" />
                                    <cc1:ucTextBox ID="txtReason" Width="600" runat="server" AutoPostBack="False" />
                                    <%-- <BDP:BasicDatePicker ID="txtRevisedDate" runat="server" Columns="2" DisplayType="TextBox"
                                        TextBoxColumns="7" DateFormat="dd/MM/yyyy">
                                    </BDP:BasicDatePicker>--%>
                                     <asp:RequiredFieldValidator ID="rfvReasonRequired" runat="server"
                                    ControlToValidate="txtReason" Display="None" ValidationGroup="Save">
                                </asp:RequiredFieldValidator>
                                </td>
                            </tr>
                        </table>
                    </cc1:ucPanel>
                </div>
            </div>
            <div class="bottom-shadow">
            </div>
            <div class="button-row">
                <cc1:ucButton ID="btnSave" runat="server" Text="Save" CssClass="button" OnClick="btnSave_Click"
                   ValidationGroup="Save"  />
                <%--<cc1:ucButton ID="btnDelete" runat="server" Text="Delete" CssClass="button" OnClick="btnDelete_Click" ValidationGroup="Save" />--%>
                <cc1:ucButton ID="btnBack" runat="server" Text="Back" CssClass="button" OnClick="btnBack_Click" />
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>



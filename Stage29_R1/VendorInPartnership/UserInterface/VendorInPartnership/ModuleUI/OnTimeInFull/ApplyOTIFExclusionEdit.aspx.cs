﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.Discrepancy.LogDiscrepancy;
using BusinessEntities.ModuleBE.OnTimeInFull;
using BusinessEntities.ModuleBE.Upload;
using BusinessLogicLayer.ModuleBAL.Discrepancy.LogDiscrepancy;
using BusinessLogicLayer.ModuleBAL.OnTimeInFull;
using BusinessLogicLayer.ModuleBAL.Upload;
using Utilities;
using WebUtilities;
using System.Linq;
using System.Text;

public partial class ApplyOTIFExclusionEdit : CommonPage
{
    #region Declarations ...
    protected string NoSelectedProductCode = WebCommon.getGlobalResourceValue("NoSelectedProductCode");
   
    #endregion

    #region Events ...
    protected void Page_InIt(object sender, EventArgs e)
    {
        ucSite.CurrentPage = this;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            getRevisedLeadByPurchaseOrder();

            if (GetQueryStringValue("PurchaseOrderID") != null)
            {
                //btnDelete.Visible = true;
                cusvNoSelectedProductCode.Enabled = false;
            }
            else
            {
                //btnDelete.Visible = false;
                cusvNoSelectedProductCode.Enabled = true; 
            }
        }
        txtPurchaseOrderNumber.Attributes.Add("autocomplete", "off");
    }

    public override void SitePost_Load()
    {
        base.SitePost_Load();
        if (!IsPostBack)
        {
            ucSite.innerControlddlSite.SelectedIndex = 0;
            ucSite.innerControlddlSite.AutoPostBack = true;

        }
    }

    public override void SiteSelectedIndexChanged()
    {
        lstProductCode.Items.Clear();
        lstselectedProductCode.Items.Clear();
        if (txtPurchaseOrderNumber.Text != string.Empty)
        {
            GetPurchaseOrderList();
        }
    }

    protected void btnGo_Click(object sender, EventArgs e)
    {
       
        if (IsPOValidForSite())
        {
            if (!this.CheckExistingExclusion(txtPurchaseOrderNumber.Text, Convert.ToInt32(ucSite.innerControlddlSite.SelectedValue)))
            {
                lstProductCode.Items.Clear();
                GetOPProductCodes();
                lstselectedProductCode.Items.Clear();
                getSelectedPurchaseOrder();
            }
            else
            {
                string sitePOCombinationMessage = WebCommon.getGlobalResourceValue("ExclusionForPOExists");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('" + sitePOCombinationMessage + "')", true);
            }
        }
        else
        {
            string saveMessage = WebCommon.getGlobalResourceValue("InvalidPurchaseOrder");
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('" + saveMessage + "')", true);
        }
    }

    protected void btnMoveRightAll_Click(object sender, EventArgs e)
    {
        if (lstProductCode.Items.Count > 0)
            FillControls.MoveAllItems(lstProductCode, lstselectedProductCode);
    }

    protected void btnMoveRight_Click(object sender, EventArgs e)
    {
        if (lstProductCode.SelectedItem != null)
        {
            FillControls.MoveOneItem(lstProductCode, lstselectedProductCode);
        }
    }

    protected void btnMoveLeft_Click(object sender, EventArgs e)
    {
        if (lstselectedProductCode.SelectedItem != null)
        {
            FillControls.MoveOneItem(lstselectedProductCode, lstProductCode);
            if (lstselectedProductCode.Items.Count > 0)
            {
                lstselectedProductCode.SelectedIndex = 0;
            }

        }
    }

    protected void btnMoveLeftAll_Click(object sender, EventArgs e)
    {
        if (lstselectedProductCode.Items.Count > 0)
        {
            FillControls.MoveAllItems(lstselectedProductCode, lstProductCode);
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (GetQueryStringValue("PurchaseOrderID") != null)
        {
            DeleteSelectedProductCodes();
        }

        #region Logic to get the Vendor details for selected PO ...
        //DateTime dtmPODate = Common.GetMM_DD_YYYY(ddlPurchaseOrderDate.SelectedItem.Text);
        List<DiscrepancyBE> lstDetails = (List<DiscrepancyBE>)ViewState["GetPurchaseOrderDateData"];
        var vendor = new DiscrepancyBE();
        if (lstDetails != null)
            vendor = lstDetails.Find(gpd => gpd.PurchaseOrder_Order_raised.Equals(ddlPurchaseOrderDate.SelectedItem.Text));
        #endregion

        ApplyOTIFExclusionBE applyOTIFExclusionBE = new ApplyOTIFExclusionBE();
        ApplyOTIFExclusionBAL applyOTIFExclusionBAL = new ApplyOTIFExclusionBAL();

        applyOTIFExclusionBE.Purchase_order = txtPurchaseOrderNumber.Text;
        applyOTIFExclusionBE.SiteID = Convert.ToInt32(ucSite.innerControlddlSite.SelectedItem.Value);
        applyOTIFExclusionBE.UserName = Session["UserName"].ToString();
        applyOTIFExclusionBE.Reason = txtReason.Text;
        applyOTIFExclusionBE.Order_raised = ddlPurchaseOrderDate.SelectedItem.Text == "--Select--" ? (DateTime?)null : Common.GetMM_DD_YYYY(ddlPurchaseOrderDate.SelectedItem.Text);
        if (vendor.VendorID != null)
            applyOTIFExclusionBE.VendorID = Convert.ToInt32(vendor.VendorID);

        if (lstselectedProductCode.Items.Count > 0)
        {
            if (lstselectedProductCode.Items.Count > 1)
            {
                applyOTIFExclusionBE.SelectedProductCodes = lstselectedProductCode.Items[0].Value + ",";

                applyOTIFExclusionBE.ProductDescription = lstselectedProductCode.Items[0].Text + ",";

                for (int i = 1; i < lstselectedProductCode.Items.Count - 1; i++)
                {
                    applyOTIFExclusionBE.SelectedProductCodes += lstselectedProductCode.Items[i].Value + ",";

                    applyOTIFExclusionBE.ProductDescription += lstselectedProductCode.Items[i].Text + ",";

                }
                applyOTIFExclusionBE.SelectedProductCodes += lstselectedProductCode.Items[lstselectedProductCode.Items.Count - 1].Value;
                applyOTIFExclusionBE.ProductDescription += lstselectedProductCode.Items[lstselectedProductCode.Items.Count - 1].Text;
            }
            else
            {
                applyOTIFExclusionBE.SelectedProductCodes = lstselectedProductCode.Items[0].Value;
                applyOTIFExclusionBE.ProductDescription = lstselectedProductCode.Items[0].Text;
            }
        }

        if (GetQueryStringValue("PurchaseOrderID") != null)
        {
            if (!string.IsNullOrEmpty(GetQueryStringValue("ExclusionID")))
                applyOTIFExclusionBE.ApplyExclusionID = Convert.ToInt32(GetQueryStringValue("ExclusionID"));

            applyOTIFExclusionBE.Action = "UpdateApplyOTIFExclusion";

            if (GetQueryStringValue("OrderRaised") != null)
            {
                applyOTIFExclusionBE.Order_raised = Convert.ToDateTime(GetQueryStringValue("OrderRaised").ToString());
            }
            else
                applyOTIFExclusionBE.Order_raised = ddlPurchaseOrderDate.SelectedItem.Text == "--Select--" ? (DateTime?)null : Common.GetMM_DD_YYYY(ddlPurchaseOrderDate.SelectedItem.Text);

        }
        else
        {
            applyOTIFExclusionBE.Action = "AddApplyOTIFExclusion";
        }

        StringBuilder AlreadyOTIFHitForPOMessage = new StringBuilder(); ;
        if (ViewState["ProductDesc"] != null)
        {
            string[] productDesc = Convert.ToString(ViewState["ProductDesc"]).TrimEnd(',').Split(',');

            if (lstselectedProductCode.Items.Count > 0)
            {
                for (int i = 0; i < productDesc.Length; i++)
                {
                    for (int j = 0; j < applyOTIFExclusionBE.ProductDescription.Split(',').Length; j++)
                    {
                        if (Convert.ToString(applyOTIFExclusionBE.ProductDescription.Split(',')[j]).Trim() == productDesc[i].Trim())
                        {
                            //string[] ProductSku = productDesc[i].Split('-');
                            // AlreadyExclusionHitForPOMessage.Append("- Please Remove odsku- " + productDesc[i] + " from the select list <br /> ");
                            AlreadyOTIFHitForPOMessage.Append(WebCommon.getGlobalResourceValue("AlreadyOTIFHitForPO")).Replace("{ProductDescription}", productDesc[i].Split('-').ElementAtOrDefault(0));
                            // AlreadyExclusionHitForPOMessage.AppendLine();
                            // string AlreadyExclusionHitForPOMessage = WebCommon.getGlobalResourceValue("AlreadyExclusionHitForPO");                        
                            break;
                        }
                    }
                }
            }
        }

        if (string.IsNullOrEmpty(AlreadyOTIFHitForPOMessage.ToString()))
        {

            int? iResult = applyOTIFExclusionBAL.addEditUP_ApplyOTIFExclusionBAL(applyOTIFExclusionBE);
            applyOTIFExclusionBAL = null;

            if (iResult == 0)
            {
                string saveMessage = WebCommon.getGlobalResourceValue("SavedMessage");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('" + saveMessage + "')", true);
                EncryptQueryString("ApplyOTIFExclusionOverview.aspx?PreviousPage=PO");
            }
        }
        else
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('" + AlreadyOTIFHitForPOMessage + "')", true);
        }
    }

    //protected void btnDelete_Click(object sender, EventArgs e)
    //{
    //    //#region Logic to get the Vendor details for selected PO ...
    //    ////DateTime dtmPODate = Common.GetMM_DD_YYYY(ddlPurchaseOrderDate.SelectedItem.Text);
    //    //List<DiscrepancyBE> lstDetails = (List<DiscrepancyBE>)ViewState["GetPurchaseOrderDateData"];
    //    //var vendor = new DiscrepancyBE();
    //    //if (lstDetails != null)
    //    //    vendor = lstDetails.Find(gpd => gpd.PurchaseOrder_Order_raised.Equals(ddlPurchaseOrderDate.SelectedItem.Text));
    //    //#endregion

    //    ApplyOTIFExclusionBE applyOTIFExclusionBE = new ApplyOTIFExclusionBE();
    //    ApplyOTIFExclusionBAL applyOTIFExclusionBAL = new ApplyOTIFExclusionBAL();

    //    applyOTIFExclusionBE.Action = "DeleteApplyOtifHitExclusion";

    //    applyOTIFExclusionBE.Purchase_order = txtPurchaseOrderNumber.Text;

    //    if (GetQueryStringValue("UserName") != null)
    //    {
    //        applyOTIFExclusionBE.UserName = Convert.ToString(GetQueryStringValue("UserName")); // Who Applied The Hit
    //    }

    //    if (GetQueryStringValue("DateApplied") != null)
    //    {
    //        applyOTIFExclusionBE.DateApplied = Convert.ToDateTime(GetQueryStringValue("DateApplied"));
    //    }

    //    applyOTIFExclusionBE.Reason = txtReason.Text;

    //    if (GetQueryStringValue("SiteID") != null)
    //    {
    //        applyOTIFExclusionBE.SiteID = Convert.ToInt32(GetQueryStringValue("SiteID").ToString());
    //    }
    //    else
    //    {
    //        applyOTIFExclusionBE.SiteID = Convert.ToInt32(ucSite.innerControlddlSite.SelectedItem.Value);
    //    }

    //    if (GetQueryStringValue("OrderRaised") == null)
    //    {
    //        applyOTIFExclusionBE.Order_raised = ddlPurchaseOrderDate.SelectedItem.Text == "--Select--" ? (DateTime?)null : Common.GetMM_DD_YYYY(ddlPurchaseOrderDate.SelectedItem.Text);
    //    }
    //    else
    //    {
    //        applyOTIFExclusionBE.Order_raised = Convert.ToDateTime(GetQueryStringValue("OrderRaised").ToString());
    //    }

    //    applyOTIFExclusionBE.UserID = Convert.ToInt32(Session["UserID"]); // User Deleting the items

    //    //if (vendor.VendorID != null)
    //    //    applyOTIFExclusionBE.VendorID = Convert.ToInt32(vendor.VendorID);

    //    if (GetQueryStringValue("VendorID") != null)
    //    {
    //        applyOTIFExclusionBE.VendorID = Convert.ToInt32(GetQueryStringValue("VendorID"));
    //    }

    //    if (lstselectedProductCode.Items.Count > 0)
    //    {
    //        if (lstselectedProductCode.Items.Count > 1)
    //        {
    //            applyOTIFExclusionBE.SelectedProductCodes = lstselectedProductCode.Items[0].Value + ",";

    //            applyOTIFExclusionBE.ProductDescription = lstselectedProductCode.Items[0].Text + ",";

    //            for (int i = 1; i < lstselectedProductCode.Items.Count - 1; i++)
    //            {
    //                applyOTIFExclusionBE.SelectedProductCodes += lstselectedProductCode.Items[i].Value + ",";

    //                applyOTIFExclusionBE.ProductDescription += lstselectedProductCode.Items[i].Text + ",";

    //            }
    //            applyOTIFExclusionBE.SelectedProductCodes += lstselectedProductCode.Items[lstselectedProductCode.Items.Count - 1].Value;
    //            applyOTIFExclusionBE.ProductDescription += lstselectedProductCode.Items[lstselectedProductCode.Items.Count - 1].Text;
    //        }
    //        else
    //        {
    //            applyOTIFExclusionBE.SelectedProductCodes = lstselectedProductCode.Items[0].Value;
    //            applyOTIFExclusionBE.ProductDescription = lstselectedProductCode.Items[0].Text;
    //        }
    //    }

    //    if (GetQueryStringValue("PurchaseOrderID") != null)
    //    {
    //        if (!string.IsNullOrEmpty(GetQueryStringValue("ExclusionID")))
    //            applyOTIFExclusionBE.ApplyExclusionID = Convert.ToInt32(GetQueryStringValue("ExclusionID"));
    //    }

    //    StringBuilder NotExclusionForPOMessage = new StringBuilder(); ;        

    //    if (ViewState["ProductDescApplyOtifExclusion"] != null)
    //    {
    //        string[] productDesc = Convert.ToString(ViewState["ProductDescApplyOtifExclusion"]).TrimEnd(',').Split(',');

    //        for (int j = 0; j < applyOTIFExclusionBE.ProductDescription.Split(',').Length; j++)
    //        {
    //            if (!productDesc.Contains(Convert.ToString(applyOTIFExclusionBE.ProductDescription.Split(',')[j]).Trim()))
    //            {
    //                NotExclusionForPOMessage.Append(WebCommon.getGlobalResourceValue("NotExclusionForPO")).Replace("{ProductDescription}", Convert.ToString(applyOTIFExclusionBE.ProductDescription.Split(',')[j]));
    //            }

    //        }
    //    }

    //    if (string.IsNullOrEmpty(NotExclusionForPOMessage.ToString()))
    //    {

    //        int? iResult = applyOTIFExclusionBAL.DeleteApplyOtifHitDetailsBAL(applyOTIFExclusionBE);
    //        applyOTIFExclusionBAL = null;

    //        if (iResult == 0)
    //        {
    //            string saveMessage = WebCommon.getGlobalResourceValue("SavedMessage");
    //            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('" + saveMessage + "')", true);
    //            EncryptQueryString("ApplyOTIFExclusionOverview.aspx?PreviousPage=PO");
    //        }
    //    }
    //    else
    //    {
    //        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('" + NotExclusionForPOMessage + "')", true);
    //    }

    //}

    protected void btnBack_Click(object sender, EventArgs e)
    {
        EncryptQueryString("ApplyOTIFExclusionOverview.aspx?PreviousPage=PO");
    }

    protected void ddlPurchaseOrderDate_SelectedIndexChanged(object sender, EventArgs e)
    {
        lblVendorValue.Text = ddlPurchaseOrderDate.SelectedItem.Value.ToString();
    }

    protected void txtPurchaseOrder_TextChanged(object sender, EventArgs e)
    {
        lstProductCode.Items.Clear();
        lstselectedProductCode.Items.Clear();
        GetPurchaseOrderList();

    }
    #endregion

    #region Methods ...
    private void GetOPProductCodes()
    {
        Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE = new Up_PurchaseOrderDetailBE();
        UP_PurchaseOrderDetailBAL oUP_PurchaseOrderDetailBAL = new UP_PurchaseOrderDetailBAL();
        oUp_PurchaseOrderDetailBE.Action = "GetApplyOTIFExclusion_ODPoductCodes";
        oUp_PurchaseOrderDetailBE.Purchase_order = txtPurchaseOrderNumber.Text.Trim();
        oUp_PurchaseOrderDetailBE.Warehouse = (!string.IsNullOrEmpty(Convert.ToString(GetQueryStringValue("Warehouse") != null))) ? GetQueryStringValue("Warehouse") : null;
        if (GetQueryStringValue("OrderRaised") != null)
        {
            oUp_PurchaseOrderDetailBE.Order_raised = Convert.ToDateTime(GetQueryStringValue("OrderRaised").ToString());
        }
        else
            oUp_PurchaseOrderDetailBE.Order_raised = ddlPurchaseOrderDate.SelectedItem.Text == "--Select--" ? (DateTime?)null : Common.GetMM_DD_YYYY(ddlPurchaseOrderDate.SelectedItem.Text);

        List<Up_PurchaseOrderDetailBE> lstODProductCodes = new List<Up_PurchaseOrderDetailBE>();
        lstODProductCodes = oUP_PurchaseOrderDetailBAL.GetODProductCodesBAL(oUp_PurchaseOrderDetailBE);
        oUP_PurchaseOrderDetailBAL = null;

        var items = lstODProductCodes.Where(x => x.SKUExclusionID != 0);
        
        foreach (var item in items)
        {
            ViewState["ProductDesc"] += item.ProductDescription + " ,";
        }
        

        if (lstODProductCodes.Count > 0 && lstODProductCodes != null)
        {
            FillControls.FillListBox(ref lstProductCode, lstODProductCodes, "ProductDescription", "SKUID");
        }
        else
        {
            if (GetQueryStringValue("PurchaseOrderID") == null)
            {
                string saveMessage = WebCommon.getGlobalResourceValue("NoProductCodeExist");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('" + saveMessage + "')", true);
            }
        }
    }

    private bool CheckExistingExclusion(string purchaseOrderNo = "", int siteId = 0)
    {
        int applyExclusionID = 0;
        if (!string.IsNullOrEmpty(GetQueryStringValue("ExclusionID")))
            applyExclusionID = Convert.ToInt32(GetQueryStringValue("ExclusionID"));

        bool IsExclusionExist = false;
        ApplyOTIFExclusionBE applyOTIFExclusionBE = new ApplyOTIFExclusionBE();
        ApplyOTIFExclusionBAL applyOTIFExclusionBAL = new ApplyOTIFExclusionBAL();
        applyOTIFExclusionBE.Action = "CheckApplyExclusionExistance";
        applyOTIFExclusionBE.ApplyExclusionID = applyExclusionID;
        applyOTIFExclusionBE.Purchase_order = purchaseOrderNo;
        applyOTIFExclusionBE.SiteID = siteId;
        IsExclusionExist = applyOTIFExclusionBAL.CheckApplyExclusionExistanceBAL(applyOTIFExclusionBE);
        applyOTIFExclusionBAL = null;
        return IsExclusionExist;
    }

    private void getRevisedLeadByPurchaseOrder()
    {
        if (GetQueryStringValue("PurchaseOrderID") != null && GetQueryStringValue("OrderRaised") != null)
        {
            trSite.Visible = false;
            trPurchaseOrderDate.Visible = false;
            trVendor.Visible = false;
            txtPurchaseOrderNumber.Enabled = false;

            ApplyOTIFExclusionBE applyOTIFExclusionBE = new ApplyOTIFExclusionBE();
            ApplyOTIFExclusionBAL applyOTIFExclusionBAL = new ApplyOTIFExclusionBAL();

            applyOTIFExclusionBE.Action = "GetApplySKUExclusion";
            applyOTIFExclusionBE.Purchase_order = Convert.ToString(GetQueryStringValue("PurchaseOrderID"));
            if (GetQueryStringValue("OrderRaised") != null)
            {
                applyOTIFExclusionBE.Order_raised = Convert.ToDateTime(GetQueryStringValue("OrderRaised").ToString());
            }

            List<ApplyOTIFExclusionBE> lstApplyOTIFExclusion = new List<ApplyOTIFExclusionBE>();
            lstApplyOTIFExclusion = applyOTIFExclusionBAL.GetApplySKUExclusionBAL(applyOTIFExclusionBE);
            applyOTIFExclusionBAL = null;
            if (lstApplyOTIFExclusion.Count > 0)
            {
                txtPurchaseOrderNumber.Text = lstApplyOTIFExclusion[0].Purchase_order;
                //txtRevisedDate.innerControltxtDate.Value = Convert.ToDateTime(lstRevisedLeadListing[0].RevisedDueDate).ToString("dd/MM/yyyy");
                txtReason.Text = lstApplyOTIFExclusion[0].Reason;
                GetOPProductCodes();
                getSelectedPurchaseOrder();
                btnGo.Visible = false;
            }
        }
    }

    private void getSelectedPurchaseOrder()
    {
        ApplyOTIFExclusionBE applyOTIFExclusionBE = new ApplyOTIFExclusionBE();
        ApplyOTIFExclusionBAL applyOTIFExclusionBAL = new ApplyOTIFExclusionBAL();

        applyOTIFExclusionBE.Action = "GetSelectedApplySKUCodes";
        applyOTIFExclusionBE.Purchase_order = txtPurchaseOrderNumber.Text;
        if (GetQueryStringValue("OrderRaised") != null)
            applyOTIFExclusionBE.Order_raised = Convert.ToDateTime(GetQueryStringValue("OrderRaised").ToString());
        else
            applyOTIFExclusionBE.Order_raised = ddlPurchaseOrderDate.SelectedItem.Text == "--Select--" ? (DateTime?)null : Common.GetMM_DD_YYYY(ddlPurchaseOrderDate.SelectedItem.Text);

        List<ApplyOTIFExclusionBE> lstApplyOTIFExclusion = new List<ApplyOTIFExclusionBE>();
        lstApplyOTIFExclusion = applyOTIFExclusionBAL.GetSelectedApplySKUCodesBAL(applyOTIFExclusionBE);
        applyOTIFExclusionBAL = null;
        if (lstApplyOTIFExclusion.Count > 0 && lstApplyOTIFExclusion != null)
        {
            FillControls.FillListBox(ref lstselectedProductCode, lstApplyOTIFExclusion, "ProductDescription", "SKUID");

            foreach (var item in lstApplyOTIFExclusion)
            {
                ViewState["ProductDescApplyOtifExclusion"] += item.ProductDescription + ",";
                ViewState["ProductDescApplyOtifExclusionSelectedValue"] += item.SKUID + ",";
            }
        }
        else
        {
            //string saveMessage = WebCommon.getGlobalResourceValue("NoProductCodeExist");
            //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('" + saveMessage + "')", true);
        }
        foreach (ListItem lst in lstselectedProductCode.Items)
        {
            if (lstProductCode.Items.Contains(lst))
                lstProductCode.Items.Remove(lst);
        }
    }

    private void GetPurchaseOrderList()
    {
        DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
        DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();

        oDiscrepancyBE.Action = "GetPurchaseOrderDate";
        oDiscrepancyBE.SiteID = Convert.ToInt32(ucSite.innerControlddlSite.SelectedValue);
        oDiscrepancyBE.PurchaseOrderNumber = txtPurchaseOrderNumber.Text.Trim();

        List<DiscrepancyBE> lstDetails = oDiscrepancyBAL.GetPurchaseOrderDateListBAL(oDiscrepancyBE);
        ViewState["GetPurchaseOrderDateData"] = lstDetails;
        oDiscrepancyBAL = null;
        if (lstDetails.Count > 0 && lstDetails != null)
        {
            FillControls.FillDropDown(ref ddlPurchaseOrderDate, lstDetails, "PurchaseOrder_Order_raised", "VendorNoName");
            lblVendorValue.Text = ddlPurchaseOrderDate.SelectedItem.Value.ToString();

        }
        else
        {
            string saveMessage = WebCommon.getGlobalResourceValue("InvalidPurchaseOrder");
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('" + saveMessage + "')", true);
        }
    }

    private bool IsPOValidForSite()
    {
        bool blnStatus = false;
        DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
        DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();
        oDiscrepancyBE.Action = "GetPurchaseOrderDate";
        oDiscrepancyBE.SiteID = Convert.ToInt32(ucSite.innerControlddlSite.SelectedValue);
        oDiscrepancyBE.PurchaseOrderNumber = txtPurchaseOrderNumber.Text.Trim();
        List<DiscrepancyBE> lstDetails = oDiscrepancyBAL.GetPurchaseOrderDateListBAL(oDiscrepancyBE);
        oDiscrepancyBAL = null;
        if (lstDetails.Count > 0 && lstDetails != null)
            blnStatus = true;
        else
            blnStatus = false;        

        return blnStatus;
    }

    private void DeleteSelectedProductCodes()
    {
        ApplyOTIFExclusionBE applyOTIFExclusionBE = new ApplyOTIFExclusionBE();
        ApplyOTIFExclusionBAL applyOTIFExclusionBAL = new ApplyOTIFExclusionBAL();

        applyOTIFExclusionBE.Action = "DeleteApplyOtifHitExclusion";

        applyOTIFExclusionBE.Purchase_order = txtPurchaseOrderNumber.Text;

        if (GetQueryStringValue("UserName") != null)
        {
            applyOTIFExclusionBE.UserName = Convert.ToString(GetQueryStringValue("UserName")); // Who Applied The Hit
        }

        if (GetQueryStringValue("DateApplied") != null)
        {
            applyOTIFExclusionBE.DateApplied = Convert.ToDateTime(GetQueryStringValue("DateApplied"));
        }

        applyOTIFExclusionBE.Reason = txtReason.Text;

        if (GetQueryStringValue("SiteID") != null)
        {
            applyOTIFExclusionBE.SiteID = Convert.ToInt32(GetQueryStringValue("SiteID").ToString());
        }
        else
        {
            applyOTIFExclusionBE.SiteID = Convert.ToInt32(ucSite.innerControlddlSite.SelectedItem.Value);
        }

        if (GetQueryStringValue("OrderRaised") == null)
        {
            applyOTIFExclusionBE.Order_raised = ddlPurchaseOrderDate.SelectedItem.Text == "--Select--" ? (DateTime?)null : Common.GetMM_DD_YYYY(ddlPurchaseOrderDate.SelectedItem.Text);
        }
        else
        {
            applyOTIFExclusionBE.Order_raised = Convert.ToDateTime(GetQueryStringValue("OrderRaised").ToString());
        }

        applyOTIFExclusionBE.UserID = Convert.ToInt32(Session["UserID"]); // User Deleting the items

        //if (vendor.VendorID != null)
        //    applyOTIFExclusionBE.VendorID = Convert.ToInt32(vendor.VendorID);

        if (GetQueryStringValue("VendorID") != null)
        {
            applyOTIFExclusionBE.VendorID = Convert.ToInt32(GetQueryStringValue("VendorID"));
        }

        string[] productCodesOld = Convert.ToString(ViewState["ProductDescApplyOtifExclusionSelectedValue"]).TrimEnd(',').Split(',');
        

        List<ListItemData> ProductCodesOld = new List<ListItemData>();

        foreach (var item in productCodesOld)
        {
            ListItemData obj = new ListItemData();
            obj.SelectedProductCodesToBeDeleted = item;
            ProductCodesOld.Add(obj);
        }


        if (lstselectedProductCode.Items.Count > 0)
        {
            if (lstselectedProductCode.Items.Count > 1)
            {
                applyOTIFExclusionBE.SelectedProductCodes = lstselectedProductCode.Items[0].Value + ",";

                applyOTIFExclusionBE.ProductDescription = lstselectedProductCode.Items[0].Text + ",";

                for (int i = 1; i < lstselectedProductCode.Items.Count - 1; i++)
                {
                    applyOTIFExclusionBE.SelectedProductCodes += lstselectedProductCode.Items[i].Value + ",";

                    applyOTIFExclusionBE.ProductDescription += lstselectedProductCode.Items[i].Text + ",";

                }
                applyOTIFExclusionBE.SelectedProductCodes += lstselectedProductCode.Items[lstselectedProductCode.Items.Count - 1].Value;
                applyOTIFExclusionBE.ProductDescription += lstselectedProductCode.Items[lstselectedProductCode.Items.Count - 1].Text;
            }
            else
            {
                applyOTIFExclusionBE.SelectedProductCodes = lstselectedProductCode.Items[0].Value;
                applyOTIFExclusionBE.ProductDescription = lstselectedProductCode.Items[0].Text;
            }
        }

        if (GetQueryStringValue("PurchaseOrderID") != null)
        {
            if (!string.IsNullOrEmpty(GetQueryStringValue("ExclusionID")))
                applyOTIFExclusionBE.ApplyExclusionID = Convert.ToInt32(GetQueryStringValue("ExclusionID"));
        }

        List<ListItemData> ListDataToBeDeleted = new List<ListItemData>();

        if (applyOTIFExclusionBE.SelectedProductCodes != null)
        {
            List<ListItemData> ProductCodesNew = new List<ListItemData>();

            foreach (var item in applyOTIFExclusionBE.SelectedProductCodes.Split(','))
            {
                ListItemData obj = new ListItemData();
                obj.SelectedProductCodesToBeDeleted = item;
                ProductCodesNew.Add(obj);
            }

            ProductCodesOld.RemoveAll(r => ProductCodesNew.Any(a => a.SelectedProductCodesToBeDeleted == r.SelectedProductCodesToBeDeleted));
            ListDataToBeDeleted = ProductCodesOld;
        }
        else
        {
            ListDataToBeDeleted = ProductCodesOld;
        }

        applyOTIFExclusionBE.SelectedProductCodes = string.Empty;


        foreach (ListItemData items in ListDataToBeDeleted)
        {
            applyOTIFExclusionBE.SelectedProductCodes += items.SelectedProductCodesToBeDeleted + ",";
        }

        applyOTIFExclusionBE.SelectedProductCodes = applyOTIFExclusionBE.SelectedProductCodes.TrimEnd(',');

        if (ListDataToBeDeleted != null && ListDataToBeDeleted.Count > 0)
        {
            int? iResult = applyOTIFExclusionBAL.DeleteApplyOtifHitDetailsBAL(applyOTIFExclusionBE);
            applyOTIFExclusionBAL = null;
        }

        }

    public class ListItemData
    {
        public string SelectedProductCodesToBeDeleted { get; set; }
    }

    #endregion
}
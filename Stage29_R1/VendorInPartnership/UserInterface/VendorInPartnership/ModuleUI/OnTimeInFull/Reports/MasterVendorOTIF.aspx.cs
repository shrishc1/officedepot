﻿using System;
using System.Data;
using BusinessEntities.ModuleBE.OnTimeInFull.Report;
using BusinessLogicLayer.ModuleBAL.OnTimeInFull.Report;
using Utilities;
using WebUtilities;
using Microsoft.Reporting.WebForms;
using BusinessEntities.ModuleBE.ReportRequest;
using BusinessLogicLayer.ModuleBAL.ReportRequest;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLogicLayer.ModuleBAL.Security;
using BusinessEntities.ModuleBE.Security;
using System.Collections.Generic;


public partial class MasterVendorOTIF : CommonPage
{
    int? ReportRequest = 0;
    ReportRequestBE oReportRequestBE = new ReportRequestBE();
    ReportRequestBAL oReportRequestBAL = new ReportRequestBAL();

    protected void Page_Init(object sender, EventArgs e)
    {
        ucVendorTemplateSelect.CurrentPage = this;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack) {           
            int YearFrom = DateTime.Now.Year;
            int YearTo = DateTime.Now.Year - 10;
            while (YearFrom > YearTo) {
                if (YearFrom >= 2013)
                    ddlYear.Items.Add(YearFrom.ToString());
                YearFrom--;
            }
        }

        TextBox txtVendor = Utilities.Common.FindControlRecursive(this.Page.Controls[0], "txtVendorNo") as TextBox;
        if (txtVendor != null)
            txtVendor.Focus();
    }

    protected void Page_Prerender(object sender, EventArgs e) {
        if (!IsPostBack) {
            ucSeacrhVendor1.IsParentRequired = false;
            ucSeacrhVendor1.IsStandAloneRequired = true;
            ucSeacrhVendor1.IsGrandParentRequired = true;
            ucSeacrhVendor1.IsChildRequired = true;
            ucSeacrhVendor1.CountryID = -2;
            ucSeacrhVendor1.EuropeOrLocal = 0;
            //ucSeacrhVendor1.BindVendor();           
        }       
    }


    /// <summary>
    /// Method to show Vendor on the basis of selected Template
    /// </summary>
    public override void TemplateSelectedIndexChanged()
    {
        base.TemplateSelectedIndexChanged();

        int VendorTemplateId = Convert.ToInt32(ucVendorTemplateSelect.innerControlddlTemplate.SelectedValue);

        ListBox lstSelectedVendor = (ListBox)ucSeacrhVendor1.FindControl("lstSelectedVendor");
        ListBox lstVendor = (ListBox)ucSeacrhVendor1.FindControl("ucVendorForCountryForMasterVendorOTIF").FindControl("lstLeft");

        if (VendorTemplateId > 0)
        {
            SCT_TemplateBAL oSCT_TemplateBAL = new SCT_TemplateBAL();
            SCT_TemplateBE oSCT_TemplateBE = new SCT_TemplateBE();

            oSCT_TemplateBE.VendorTemplateID = VendorTemplateId;
            oSCT_TemplateBE.Action = "GetVendorTemplateInfobyID";
            List<SCT_TemplateBE> lstVendorTemplate = oSCT_TemplateBAL.GetVendorTemplateById(oSCT_TemplateBE);


            if (lstVendorTemplate != null && lstVendorTemplate.Count > 0)
            {
                lstSelectedVendor.Items.Clear();
                ucSeacrhVendor1.innerControlHiddenField.Value = string.Empty;
                ucSeacrhVendor1.innerControlHiddenField2.Value = string.Empty;
                for (int iCount = 0; iCount < lstVendorTemplate.Count; iCount++)
                {
                    lstSelectedVendor.Items.Add(new ListItem(lstVendorTemplate[iCount].Vendor.Vendor_No +" - "+lstVendorTemplate[iCount].Vendor.VendorContactName , lstVendorTemplate[iCount].Vendor.VendorID.ToString()));
                    ucSeacrhVendor1.SelectedVendorIDs = lstVendorTemplate[iCount].Vendor.VendorID.ToString();
                    ucSeacrhVendor1.SelectedVendorName = lstVendorTemplate[iCount].Vendor.Vendor_No + " - " + lstVendorTemplate[iCount].Vendor.VendorContactName;

                }
            }
        }
        else
        {
            lstSelectedVendor.Items.Clear();
            ucSeacrhVendor1.innerControlHiddenField.Value = string.Empty;
            ucSeacrhVendor1.innerControlHiddenField2.Value = string.Empty;
        }
    }


    protected void btnGenerateReport_Click(object sender, EventArgs e) {
        oReportRequestBE.Action = "InsertReportRequest";
        oReportRequestBE.ModuleName = "OTIF";
        oReportRequestBE.ReportName = "Master Vendor OTIF Report";
        if (rdoMonthlyNew.Checked == true)  // For Monthly (new measure)
        {
            oReportRequestBE.ReportAction = "OTIF_MasterVendorReportNewMeasure";
            oReportRequestBE.ReportNamePath = "MasterVendorOtifReport.rdlc";
            oReportRequestBE.ReportDatatableName = "spOTIF_Reports";
            oReportRequestBE.ReportType = "Monthly";
        }
        else if (rdoMonthlyOld.Checked == true) // For Monthly (old measure)
        {
            oReportRequestBE.ReportAction = "OTIF_MasterVendorReportOldMeasure";
            oReportRequestBE.ReportNamePath = "MasterVendorOtifReport.rdlc";
            oReportRequestBE.ReportDatatableName = "spOTIF_Reports";
            oReportRequestBE.ReportType = "Monthly";
        }
        else if (rdoMonthlyAbs.Checked == true) // For Monthly (Absolute)
        {
            oReportRequestBE.ReportAction = "OTIF_MasterVendorReportAbsolute";
            oReportRequestBE.ReportNamePath = "MasterVendorOtifReport.rdlc";
            oReportRequestBE.ReportDatatableName = "spOTIF_Reports";
            oReportRequestBE.ReportType = "Monthly";
        }

        oReportRequestBE.EuropeanorLocal = rdoEuropeanVendorOnly.Checked ? 1 : rdoLocalVendorOnly.Checked ? 2 : 3;

        oReportRequestBE.VendorIDs = ucSeacrhVendor1.SelectedVendorIDs;
        oReportRequestBE.VendorName = ucSeacrhVendor1.SelectedVendorName;
        oReportRequestBE.PurchaseOrderDue = 1;
       
        oReportRequestBE.DateFrom = string.IsNullOrEmpty(ddlYear.SelectedValue.ToString()) ? (DateTime?)null : Common.GetMM_DD_YYYY("01/01/" + ddlYear.SelectedValue.ToString());
        DateTime now = DateTime.Now;
        string sDateTo = DateTime.Now.AddDays(-now.Day).ToString("dd/MM/yyyy");
        oReportRequestBE.DateTo = Common.GetMM_DD_YYYY(sDateTo);      

        oReportRequestBE.RequestStatus = "Pending";
        oReportRequestBE.UserID = Convert.ToInt32(Session["UserID"]);
        oReportRequestBE.RequestTime = DateTime.Now;

        ReportRequest = oReportRequestBAL.addReportRequestBAL(oReportRequestBE);
        if (ReportRequest > 0) {
            string ReportRequestSubmitted = WebCommon.getGlobalResourceValue("ReportRequestSubmitted");
            ReportRequestSubmitted = ReportRequestSubmitted + " : " + Convert.ToString(ReportRequest);
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + ReportRequestSubmitted + "')", true);

            if (((HiddenField)ucSeacrhVendor1.FindControl("hiddenSelectedIDs")) != null)
                ((HiddenField)ucSeacrhVendor1.FindControl("hiddenSelectedIDs")).Value = string.Empty;

            if (((HiddenField)ucSeacrhVendor1.FindControl("hiddenSelectedName")) != null)
                ((HiddenField)ucSeacrhVendor1.FindControl("hiddenSelectedName")).Value = string.Empty;
            
            return;
        }       
    }
    protected void rdoReportVersion_CheckedChanged(object sender, EventArgs e) {
        if (rdoAllOnly.Checked)
            ucSeacrhVendor1.EuropeOrLocal = 0;
        else if (rdoEuropeanVendorOnly.Checked) {
            ucSeacrhVendor1.EuropeOrLocal = 1;
        }
        else if (rdoLocalVendorOnly.Checked) {
            ucSeacrhVendor1.EuropeOrLocal = 2;
        }      
        ucSeacrhVendor1.BindVendorByText();
    }
}
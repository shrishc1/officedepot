﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.OnTimeInFull.Report;
using BusinessLogicLayer.ModuleBAL.OnTimeInFull.Report;
using BusinessEntities.ModuleBE.AdminFunctions;
using BusinessLogicLayer.ModuleBAL.AdminFunctions;
using Utilities;
using WebUtilities;
using BusinessLogicLayer.ModuleBAL.Security;
using BusinessEntities.ModuleBE.Security;


public partial class VendorReportingExclusion : CommonPage
{
    # region Events

    protected string deleteMessage = WebCommon.getGlobalResourceValue("DeleteMessage");
    protected void Page_Prerender(object sender, EventArgs e)
    {
        msSite.RequiredUserDefaultSite = false;
        if (!IsPostBack)
        {
            GetVendorExclusionByID();
        }
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        ucVendorTemplateSelect.CurrentPage = this;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            txtFromDate.Text = DateTime.Now.AddDays(-DateTime.Now.Day + 1).ToString("dd/MM/yyyy");
            txtToDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
            hdnJSFromDt.Value = txtFromDate.Text;
            hdnJSToDt.Value = txtToDate.Text;

        }


    }
    protected void btnApply_Click(object sender, EventArgs e)
    {

        OTIFReportBE oOTIFReportBE = new OTIFReportBE();
        OTIFReportBAL oOTIFReportBAL = new OTIFReportBAL();
        //Update mode
        if (GetQueryStringValue("ExclusionID") != null)
        {
            oOTIFReportBE.Action = "UpdateVendorExclusion";
            oOTIFReportBE.ExclusionID = Convert.ToInt32(GetQueryStringValue("ExclusionID"));
        }
        else
        {
            oOTIFReportBE.Action = "AddVendorExclusion"; //Add mode
            if (CheckExistingExclusion())
            {
                string ExclusionExist = WebCommon.getGlobalResourceValue("ExclusionExist");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + ExclusionExist + "');", true);
                return;
            }
        }

        oOTIFReportBE.ExclusionNarrative = txtExclusionNarrative.Text;
        oOTIFReportBE.AppliedBy = Session["UserName"].ToString();
        oOTIFReportBE.ApplicableDate = DateTime.Now;
        oOTIFReportBE.SelectedExcludedVendorId = msVendor.SelectedVendorIDs;
        oOTIFReportBE.SelectedExcludedSiteId = msSite.SelectedSiteIDs;
        oOTIFReportBE.SelectedExcludedSKUsName = msSKU.SelectedSKUName;
        oOTIFReportBE.SelectedExcludedDeliveryTypeId = MultiSelectBookingType.SelectedDeliveryTypeIDs;
        oOTIFReportBE.SelectedExcludedBookingNo = MultiSelectBookingNumber.SelectedBookingNo;
        oOTIFReportBE.SelectedExcludedDiscrepancyNo = MultiSelectDiscrepancyNumber.SelectedDiscrepancyNo;
        /* Commented due to Stage 5: Sheet No : 54A requirement. */
        //oOTIFReportBE.SelectedExcludedPurchaseNo = msPO.SelectedPO;        
        oOTIFReportBE.ExcludedDateFrom = string.IsNullOrEmpty(txtFromDate.Text) ? (DateTime?)null : Common.GetMM_DD_YYYY(hdnJSFromDt.Value);
        oOTIFReportBE.ExcludedDateTo = string.IsNullOrEmpty(txtToDate.Text) ? (DateTime?)null : Common.GetMM_DD_YYYY(hdnJSToDt.Value);

        int? result = oOTIFReportBAL.AddEditVendorExclusionBAL(oOTIFReportBE);
        oOTIFReportBAL = null;

        if (result > 0)
        {

            string saveMessage = WebCommon.getGlobalResourceValue("SavedMessage");
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('" + saveMessage + "')", true);
            EncryptQueryString("VendorReportingExclusionOverview.aspx");

        }
        else
        {
            string DataNotSaved = WebCommon.getGlobalResourceValue("DataNotSaved");
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + DataNotSaved + "');", true);
            return;
        }

    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        EncryptQueryString("VendorReportingExclusionOverview.aspx");
    }

    protected void btnDelete_Click(object sender, EventArgs e)
    {
        OTIFReportBE oOTIFReportBE = new OTIFReportBE();
        OTIFReportBAL oOTIFReportBAL = new OTIFReportBAL();
        //Delete mode
        if (GetQueryStringValue("ExclusionID") != null)
        {
            oOTIFReportBE.Action = "DeleteVendorExclusion";
            oOTIFReportBE.ExclusionID = Convert.ToInt32(GetQueryStringValue("ExclusionID"));
            int? result = oOTIFReportBAL.DeleteVendorExclusionBAL(oOTIFReportBE);
            oOTIFReportBAL = null;
            if (result > 0)
            {
                EncryptQueryString("VendorReportingExclusionOverview.aspx");
            }
        }
    }
    #endregion

    #region Methods
    //returns list of vendor exclusion
    private void GetVendorExclusionByID()
    {
        OTIFReportBE oOTIFReportBE = new OTIFReportBE();
        OTIFReportBAL oOTIFReportBAL = new OTIFReportBAL();
        if (GetQueryStringValue("ExclusionID") != null)
        {
            btnDelete.Visible = true;
            oOTIFReportBE.Action = "GetVendorExclusion";
            oOTIFReportBE.ExclusionID = Convert.ToInt32(GetQueryStringValue("ExclusionID"));
            List<OTIFReportBE> lstVendorExclusion = new List<OTIFReportBE>();
            lstVendorExclusion = oOTIFReportBAL.GetVendorExclusionBAL(oOTIFReportBE);

            if (lstVendorExclusion != null && lstVendorExclusion.Count > 0)
            {

                string ExcludedSKUsName = lstVendorExclusion[0].SelectedExcludedSKUsName;
                string BookingNos = lstVendorExclusion[0].SelectedExcludedBookingNo;
                string DiscrepancyNos = lstVendorExclusion[0].SelectedExcludedDiscrepancyNo;
                string SiteIDs = lstVendorExclusion[0].SelectedExcludedSiteId;
                string DeliveryTypeIDs = lstVendorExclusion[0].SelectedExcludedDeliveryTypeId;


                txtExclusionNarrative.Text = lstVendorExclusion[0].ExclusionNarrative;
                txtFromDate.Text = Convert.ToDateTime(lstVendorExclusion[0].ExcludedDateFrom).ToString("dd/MM/yyyy");
                txtToDate.Text = Convert.ToDateTime(lstVendorExclusion[0].ExcludedDateTo).ToString("dd/MM/yyyy");

                //finding controls from user controls
                ListBox lstExcludedSKUsName = (ListBox)msSKU.FindControl("lstRight");

                ListBox lstExcludedBookingNos = (ListBox)MultiSelectBookingNumber.FindControl("lstRight");

                ListBox lstExcludedDiscrepancyNos = (ListBox)MultiSelectDiscrepancyNumber.FindControl("lstRight");

                /* Commented due to Stage 5: Sheet No : 54A requirement. */
                //ListBox lstExcludedPurchaseOrderNos = (ListBox)msPO.FindControl("lstRight");

                ListBox lstExcludedSiteIDs = (ListBox)msSite.FindControl("lstRight");
                ListBox lstSiteIDs = (ListBox)msSite.FindControl("lstLeft");

                ListBox lstExcludedBookingTypeIDs = (ListBox)MultiSelectBookingType.FindControl("lstRight");
                ListBox lstBookingTypeIDs = (ListBox)MultiSelectBookingType.FindControl("lstLeft");

                ListBox lstExcludedVendorIDs = (ListBox)msVendor.FindControl("lstRight");
                UserControl UserControlVendorIDs = (UserControl)msVendor.FindControl("ucVendor");
                ListBox lstVendorIDs = (ListBox)UserControlVendorIDs.FindControl("lstLeft");

                //Assinging values to control and hidden fields of user control


                if (!string.IsNullOrEmpty(ExcludedSKUsName))
                {
                    string[] aExcludedSKUsName = ExcludedSKUsName.Split(',');
                    for (int i = 0; i < aExcludedSKUsName.Length; i++)
                    {
                        lstExcludedSKUsName.Items.Add(new ListItem(aExcludedSKUsName[i], aExcludedSKUsName[i]));
                    }

                    msSKU.SelectedSKUName = ExcludedSKUsName;
                }

                if (!string.IsNullOrEmpty(BookingNos))
                {
                    string[] aBookingNos = BookingNos.Split(',');
                    for (int i = 0; i < aBookingNos.Length; i++)
                    {
                        lstExcludedBookingNos.Items.Add(new ListItem(aBookingNos[i], aBookingNos[i]));
                    }
                    MultiSelectBookingNumber.SelectedBookingNo = BookingNos;
                }

                if (!string.IsNullOrEmpty(DiscrepancyNos))
                {
                    string[] aDiscrepancyNos = DiscrepancyNos.Split(',');
                    for (int i = 0; i < aDiscrepancyNos.Length; i++)
                    {
                        lstExcludedDiscrepancyNos.Items.Add(new ListItem(aDiscrepancyNos[i], aDiscrepancyNos[i]));
                    }
                    MultiSelectDiscrepancyNumber.SelectedDiscrepancyNo = DiscrepancyNos;
                }



                // Getting Excluded Sites Id
                oOTIFReportBE.Action = "GetVendorExcludedSites";
                oOTIFReportBE.SelectedExcludedSiteId = SiteIDs;
                List<OTIFReportBE> lstVendorExcludedSites = new List<OTIFReportBE>();
                lstVendorExcludedSites = oOTIFReportBAL.GetVendorExcludedSitesBAL(oOTIFReportBE);

                if (lstVendorExcludedSites != null && lstVendorExcludedSites.Count > 0)
                {
                    for (int iCount = 0; iCount < lstVendorExcludedSites.Count; iCount++)
                    {
                        lstExcludedSiteIDs.Items.Add(new ListItem(lstVendorExcludedSites[iCount].SiteName, lstVendorExcludedSites[iCount].SelectedExcludedSiteId));
                        lstSiteIDs.Items.Remove(new ListItem(lstVendorExcludedSites[iCount].SiteName, lstVendorExcludedSites[iCount].SelectedExcludedSiteId));
                        msSite.SelectedSiteIDs = lstVendorExcludedSites[iCount].SelectedExcludedSiteId;
                    }
                }

                // Getting Excluded Purchase Orders
                oOTIFReportBE.Action = "GetVendorExcludedPurchaseOrder";
                oOTIFReportBE.ExclusionID = Convert.ToInt32(GetQueryStringValue("ExclusionID"));
                List<OTIFReportBE> lstVendorExcludedPO = new List<OTIFReportBE>();
                lstVendorExcludedPO = oOTIFReportBAL.GetVendorExcludedPOBAL(oOTIFReportBE);

                /* Commented due to Stage 5: Sheet No : 54A requirement. */
                //if (lstVendorExcludedPO != null && lstVendorExcludedPO.Count > 0)
                //{                    
                    //for (int iCount = 0; iCount < lstVendorExcludedPO.Count; iCount++)
                    //{
                    //    lstExcludedPurchaseOrderNos.Items.Add(new ListItem(lstVendorExcludedPO[iCount].SitePurchaseOrderRaisedName, lstVendorExcludedPO[iCount].SitePurchaseOrderRaisedIds));
                    //    msPO.SelectedPO = lstVendorExcludedPO[iCount].SitePurchaseOrderRaisedIds;
                    //}
                //}

                // Getting Excluded Booking Type Id
                oOTIFReportBE.Action = "GetVendorExcludedDeliveryType";
                oOTIFReportBE.SelectedExcludedDeliveryTypeId = DeliveryTypeIDs;
                List<OTIFReportBE> lstVendorExcludedDeliveryType = new List<OTIFReportBE>();
                lstVendorExcludedDeliveryType = oOTIFReportBAL.GetVendorExcludedDeliveryTypeBAL(oOTIFReportBE);

                if (lstVendorExcludedDeliveryType != null && lstVendorExcludedDeliveryType.Count > 0)
                {
                    for (int iCount = 0; iCount < lstVendorExcludedDeliveryType.Count; iCount++)
                    {
                        lstExcludedBookingTypeIDs.Items.Add(new ListItem(lstVendorExcludedDeliveryType[iCount].CountryDeliveryType, lstVendorExcludedDeliveryType[iCount].SelectedExcludedDeliveryTypeId));
                        lstBookingTypeIDs.Items.Remove(new ListItem(lstVendorExcludedDeliveryType[iCount].CountryDeliveryType, lstVendorExcludedDeliveryType[iCount].SelectedExcludedDeliveryTypeId));
                        MultiSelectBookingType.SelectedDeliveryTypeIDs = lstVendorExcludedDeliveryType[iCount].SelectedExcludedDeliveryTypeId;
                    }
                }

                // Getting Excluded Vendor Id
                oOTIFReportBE.Action = "GetExcludedVendorId";
                oOTIFReportBE.ExclusionID = Convert.ToInt32(GetQueryStringValue("ExclusionID")); ;
                List<OTIFReportBE> lstExcludedVendor = new List<OTIFReportBE>();
                lstExcludedVendor = oOTIFReportBAL.GetExcludedVendorBAL(oOTIFReportBE);

                if (lstExcludedVendor != null && lstExcludedVendor.Count > 0)
                {
                    for (int iCount = 0; iCount < lstExcludedVendor.Count; iCount++)
                    {
                        lstExcludedVendorIDs.Items.Add(new ListItem(lstExcludedVendor[iCount].VendorName, lstExcludedVendor[iCount].SelectedExcludedVendorId));
                        lstVendorIDs.Items.Remove(new ListItem(lstExcludedVendor[iCount].VendorName, lstExcludedVendor[iCount].SelectedExcludedVendorId));
                        msVendor.SelectedVendorIDs = lstExcludedVendor[iCount].SelectedExcludedVendorId;
                    }
                }
                oOTIFReportBAL = null;
            }

        }
    }

    private bool CheckExistingExclusion()
    {
        bool IsExclusionExist = false;
        OTIFReportBE oOTIFReportBE = new OTIFReportBE();
        OTIFReportBAL oOTIFReportBAL = new OTIFReportBAL();
        oOTIFReportBE.Action = "CheckExistingExclusion";
        oOTIFReportBE.ExclusionNarrative = txtExclusionNarrative.Text.Trim();
        IsExclusionExist = oOTIFReportBAL.CheckForExclusionExistanceBAL(oOTIFReportBE);
        oOTIFReportBAL = null;
        return IsExclusionExist;
    }

    /// <summary>
    /// Method to show Vendor on the basis of selected Template
    /// </summary>
    public override void TemplateSelectedIndexChanged()
    {
        base.TemplateSelectedIndexChanged();

        int VendorTemplateId = Convert.ToInt32(ucVendorTemplateSelect.innerControlddlTemplate.SelectedValue);

        ListBox lstSelectedVendor = (ListBox)msVendor.FindControl("lstRight");
        ListBox lstVendor = (ListBox)msVendor.FindControl("ucVendor").FindControl("lstLeft");

        if (VendorTemplateId > 0)
        {

            SCT_TemplateBAL oSCT_TemplateBAL = new SCT_TemplateBAL();
            SCT_TemplateBE oSCT_TemplateBE = new SCT_TemplateBE();

            oSCT_TemplateBE.VendorTemplateID = VendorTemplateId;
            oSCT_TemplateBE.Action = "GetVendorTemplateInfobyID";
            List<SCT_TemplateBE> lstVendorTemplate = oSCT_TemplateBAL.GetVendorTemplateById(oSCT_TemplateBE);

            if (lstVendorTemplate != null && lstVendorTemplate.Count > 0)
            {
                lstSelectedVendor.Items.Clear();
                msVendor.innerControlHiddenField.Value = string.Empty;
                msVendor.innerControlHiddenField2.Value = string.Empty;
                for (int iCount = 0; iCount < lstVendorTemplate.Count; iCount++)
                {
                    lstSelectedVendor.Items.Add(new ListItem(lstVendorTemplate[iCount].Vendor.Vendor_Name, lstVendorTemplate[iCount].Vendor.VendorID.ToString()));
                    msVendor.SelectedVendorIDs = lstVendorTemplate[iCount].Vendor.VendorID.ToString();
                    msVendor.SelectedVendorName = lstVendorTemplate[iCount].Vendor.Vendor_Name.ToString();
                }
            }
        }
        else
        {
            lstSelectedVendor.Items.Clear();
            msVendor.innerControlHiddenField.Value = string.Empty;
            msVendor.innerControlHiddenField2.Value = string.Empty;
        }
    }

    #endregion

}
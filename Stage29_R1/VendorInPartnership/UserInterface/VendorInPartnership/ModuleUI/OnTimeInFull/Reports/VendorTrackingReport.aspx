﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    AutoEventWireup="true" CodeFile="VendorTrackingReport.aspx.cs" Inherits="VendorTrackingReport" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h2>
        <cc1:ucLabel ID="lblVendorTrackingReport" runat="server" Text="Vendor's OTIF Tracking Report"></cc1:ucLabel>
    </h2>
    <div class="right-shadow, formbox">
        <panel>
   
        <div style="width: 100%; text-align: center;">
            <cc1:ucLabel ID="lblYear" runat="server" Text="Year" />&nbsp;&nbsp;&nbsp;&nbsp;
            <cc1:ucTextbox runat="server" ID="txtYear" Text="2011" onkeyup="AllowNumbersOnly(this);" MaxLength="4"
                Width="8em" /><br />
            <br />
            <br />
            <cc1:ucButton ID="btnJanuary" Text="January" runat="server" CssClass="button" Width="10em" />&nbsp;&nbsp;&nbsp;&nbsp;
            <cc1:ucButton ID="UcButton1" Text="February" runat="server" CssClass="button" Width="10em" />&nbsp;&nbsp;&nbsp;&nbsp;
            <cc1:ucButton ID="UcButton2" Text="March" runat="server" CssClass="button" Width="10em" />&nbsp;&nbsp;&nbsp;&nbsp;
            <cc1:ucButton ID="UcButton3" Text="April" runat="server" CssClass="button" Width="10em" />
            <br />
            <br />
            <cc1:ucButton ID="UcButton4" Text="May" runat="server" CssClass="button" Width="10em" />&nbsp;&nbsp;&nbsp;&nbsp;
            <cc1:ucButton ID="UcButton5" Text="June" runat="server" CssClass="button" Width="10em" />&nbsp;&nbsp;&nbsp;&nbsp;
            <cc1:ucButton ID="UcButton6" Text="July" runat="server" CssClass="button" Width="10em" />&nbsp;&nbsp;&nbsp;&nbsp;
            <cc1:ucButton ID="UcButton7" Text="August" runat="server" CssClass="button" Width="10em" />
            <br />
            <br />
            <cc1:ucButton ID="UcButton8" Text="September" runat="server" CssClass="button" Width="10em" />&nbsp;&nbsp;&nbsp;&nbsp;
            <cc1:ucButton ID="UcButton9" Text="Octuber" runat="server" CssClass="button" Width="10em" />&nbsp;&nbsp;&nbsp;&nbsp;
            <cc1:ucButton ID="UcButton10" Text="November" runat="server" CssClass="button" Width="10em" />&nbsp;&nbsp;&nbsp;&nbsp;
            <cc1:ucButton ID="UcButton11" Text="December" runat="server" CssClass="button" Width="10em" />
        </div>
        </panel>
    </div>
</asp:Content>

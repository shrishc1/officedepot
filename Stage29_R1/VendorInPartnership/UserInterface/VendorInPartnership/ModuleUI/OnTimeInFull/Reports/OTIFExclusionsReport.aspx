﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="OTIFExclusionsReport.aspx.cs"
    MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    Inherits="ModuleUI_OnTimeInFull_Reports_OTIFExclusionsReport" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectCountry.ascx" TagName="MultiSelectCountry"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectSite.ascx" TagName="MultiSelectSite"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectSKU.ascx" TagName="MultiSelectSKU"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectVendor.ascx" TagName="MultiSelectVendor"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectStockPlanner.ascx" TagName="MultiSelectStockPlanner"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectItemClassification.ascx" TagName="MultiSelectItemClassification"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/ucVendorSelectionTemplate.ascx" TagName="VendorSelectTemplate"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectSKUGrouping.ascx" TagName="MultiSelectSKUGrouping"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectStockPlannerGrouping.ascx" TagName="MultiSelectStockPlannerGrouping"
    TagPrefix="cc1" %>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>

<%@ Register Src="~/CommonUI/UserControls/ucExportToExcel.ascx" TagName="ucExportToExcel"
    TagPrefix="cc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2>
        <cc1:ucLabel ID="lblOTIFExclusionsReport" runat="server"></cc1:ucLabel>
    </h2>
    <script type="text/javascript" language="javascript">
        function setValue1(target) {
            document.getElementById('<%=hdnJSFromDt.ClientID %>').value = target.value;
        }
        function setValue2(target) {
            document.getElementById('<%=hdnJSToDt.ClientID %>').value = target.value;
        }
        $(document).ready(function () {
            $('#txtToDate,#txtFromDate').datepicker({
                showOtherMonths: true,
                selectOtherMonths: true, changeMonth: true, changeYear: true,
                minDate: new Date(2013, 0, 1),
                yearRange: '2013:+100',
                dateFormat: 'dd/mm/yy', maxDate: $('#txtToDate').val()
            });
        });
        $(document).ready(function () {
            $("body").keypress(function (e) {
                if (e.which == 13 || e.keyCode == 13) {
                    $('#<%=btnGenerateReport.ClientID %>').click();
                    return false;
                }
            });
        });
    </script>
    <asp:ScriptManager ID="SM1" runat="server" AsyncPostBackTimeout="1200">
    </asp:ScriptManager>
    <asp:HiddenField ID="hdnJSFromDt" runat="server" />
    <asp:HiddenField ID="hdnJSToDt" runat="server" />
    <div class="right-shadow">
        <div class="formbox">
            <cc1:ucPanel ID="UcDataPanel" runat="server">
                <table width="95%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                    <tr style="height: 3em;">
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblDateFrom" runat="server" Text="Date From"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="UcLabel1" runat="server">:</cc1:ucLabel>
                        </td>
                        <td>
                            <div id="UcPanelDate">
                                <table cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td style="font-weight: bold; width: 6em;">
                                            <cc1:ucTextbox ID="txtFromDate" ClientIDMode="Static" runat="server" onchange="setValue1(this)"
                                                ReadOnly="True" Width="70px" />
                                        </td>
                                        <td style="font-weight: bold; width: 4em; text-align: center">
                                            <cc1:ucLabel ID="lblTo" runat="server" Text="To"></cc1:ucLabel>
                                        </td>
                                        <td style="font-weight: bold; width: 6em;">
                                            <cc1:ucTextbox ID="txtToDate" ClientIDMode="Static" runat="server" onchange="setValue2(this)"
                                                ReadOnly="True" Width="70px" />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold; width: 10%">
                            <cc1:ucLabel ID="lblReportType" runat="server" Text="Report Type"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold; width: 1%">
                            <cc1:ucLabel ID="UcLabel6" runat="server">:</cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold; width: 89%" colspan="2">
                            <cc1:ucRadioButtonList ID="rblListReportType" runat="server" RepeatDirection="Horizontal" Style="width: 39%;">
                                <asp:ListItem Selected="True">By Site</asp:ListItem>
                                <asp:ListItem>By Planner</asp:ListItem>
                                <asp:ListItem>By Vendor</asp:ListItem>
                            </cc1:ucRadioButtonList>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold; width: 10%">
                            <cc1:ucLabel ID="lblCountry" runat="server" Text="Country"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold; width: 1%">
                            <cc1:ucLabel ID="UcLabel5" runat="server">:</cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold; width: 89%">
                            <cc1:MultiSelectCountry runat="server" ID="msCountry" />
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblSite" runat="server" Text="Site"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="UcLabel4" runat="server">:</cc1:ucLabel>
                        </td>
                        <td>
                            <cc1:MultiSelectSite runat="server" ID="msSite" />
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblStockPlanner" runat="server" Text="Stock Planner"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="Label17" runat="server">:</cc1:ucLabel>
                        </td>
                        <td>
                            <cc1:MultiSelectStockPlanner runat="server" ID="msStockPlanner" />
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblInventoryGroup" runat="server">
                            </cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="Uclbl8" runat="server">:</cc1:ucLabel>
                        </td>
                        <td>
                            <cc1:MultiSelectStockPlannerGrouping runat="server" ID="multiSelectStockPlannerGrouping" />
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblVendor" runat="server" Text="Vendor"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="UcLabel2" runat="server">:</cc1:ucLabel>
                        </td>
                        <td>
                            <cc1:MultiSelectVendor runat="server" ID="msVendor" />
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblODSKUCode" runat="server" Text="OD SKU Code"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="UcLabel8" runat="server">:</cc1:ucLabel>
                        </td>
                        <td>
                            <cc1:MultiSelectSKU runat="server" ID="msSKU" />
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblSKUgrouping" runat="server" Text="Sku Grouping">
                            </cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="UcLabel81" runat="server">:</cc1:ucLabel>
                        </td>
                        <td>
                            <cc1:MultiSelectSKUGrouping runat="server" ID="multiSelectSKUGrouping" />
                        </td>
                    </tr>
                    <tr style="height: 3em;">
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td align="right" colspan="3">
                            <div class="button-row">
                                <cc1:ucButton ID="btnGenerateReport" runat="server" Text="Generate Report" CssClass="button"
                                    OnClick="btnGenerateReport_Click" />
                            </div>
                        </td>
                    </tr>
                </table>
            </cc1:ucPanel>

            <cc1:ucPanel ID="pnlExcel" runat="server">
                  <div class="button-row">
                    <cc2:ucExportToExcel ID="ucExportToExcel1" runat="server" Visible="false" />
                </div>
            </cc1:ucPanel>

            <cc1:ucPanel ID="pnlSKUExclusion" runat="server" Visible="false" onscroll="getScroll1(this);" Style="overflow: scroll">

                <asp:GridView ID="grdSkuExclusion" runat="server" OnRowDataBound="grdSkuExclusion_RowDataBound"
                    AutoGenerateColumns="false" CssClass="grid gvclass"
                    CellPadding="0" Width="1600px"
                    AllowPaging="true" PageSize="45">
                    <Columns>
                        <asp:TemplateField HeaderText="Type">
                            <HeaderStyle Width="100px" />
                            <ItemStyle HorizontalAlign="Center" />
                            <HeaderTemplate>
                                <asp:Label ID="lblTypeHeader" runat="server"></asp:Label>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <cc1:ucLabel ID="lblType" runat="server" Text='<%# Eval("[Ttype]") %>'></cc1:ucLabel>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField HeaderText="Total" DataField="Total" SortExpression="Total">
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                        <asp:BoundField HeaderText="Exclusions (Manual)" DataField="Manual" SortExpression="[Exclusions (Manual)]">
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                        <asp:BoundField HeaderText="% Exluded Manually" DataField="perManual" SortExpression="[% Exluded Manually]">
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField HeaderText="Exclusions (Automated)" DataField="automated" SortExpression="[Exclusions (Automated)]">
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField HeaderText="% Excluded Automatically" DataField="perautomated" SortExpression="[% Excluded Automatically]">
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField HeaderText="Total Excluded" DataField="totexclude" SortExpression="[Total Excluded]">
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField HeaderText="Total % Excluded" DataField="pertotexclude" SortExpression="[Total % Excluded]">
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField HeaderText="Hit Applied" DataField="hit" SortExpression="[Hit Applied]">
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField HeaderText="% Hit Applied" DataField="perhit" SortExpression="[% Hit Applied]">
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField HeaderText="Revised Due Date" DataField="revise" SortExpression="[Revised Due Date]">
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField HeaderText="% Revised" DataField="perrevise" SortExpression="[% Revised]">
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField HeaderText="Total Hit/Exclude/Revised" DataField="alltotal" SortExpression="[Total Hit/Exclude/Revised]">
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField HeaderText="Total %" DataField="peralltotal" SortExpression="[Total %]">
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                    </Columns>
                </asp:GridView>
            </cc1:ucPanel>
            <div class="bottom-shadow">
            </div>
            <div class="button-row">
                <asp:Button ID="btnBack" runat="server" Text="Back" OnClick="btnBack_Click" />
            </div>
        </div>

        <div>
            <asp:GridView ID="grdExport" runat="server" OnRowDataBound="grdSkuExclusion_RowDataBound"
                AutoGenerateColumns="false" Style="display: none;">
                <Columns>
                    <asp:TemplateField HeaderText="Type">
                        <HeaderStyle Width="100px" />
                        <ItemStyle HorizontalAlign="Center" />
                        <HeaderTemplate>
                            <asp:Label ID="lblTypeHeader" runat="server"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <cc1:ucLabel ID="lblType" runat="server" Text='<%# Eval("[Ttype]") %>'></cc1:ucLabel>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField HeaderText="Total" DataField="Total" SortExpression="Total">
                        <HeaderStyle HorizontalAlign="Left" />
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:BoundField>
                    <asp:BoundField HeaderText="Exclusions (Manual)" DataField="Manual" SortExpression="[Exclusions (Manual)]">
                        <HeaderStyle HorizontalAlign="Left" />
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:BoundField>
                    <asp:BoundField HeaderText="% Exluded Manually" DataField="perManual" SortExpression="[% Exluded Manually]">
                        <HeaderStyle HorizontalAlign="Left" />
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:BoundField>
                    <asp:BoundField HeaderText="Exclusions (Automated)" DataField="automated" SortExpression="[Exclusions (Automated)]">
                        <HeaderStyle HorizontalAlign="Left" />
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:BoundField>
                    <asp:BoundField HeaderText="% Excluded Automatically" DataField="perautomated" SortExpression="[% Excluded Automatically]">
                        <HeaderStyle HorizontalAlign="Left" />
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:BoundField>
                    <asp:BoundField HeaderText="Total Excluded" DataField="totexclude" SortExpression="[Total Excluded]">
                        <HeaderStyle HorizontalAlign="Left" />
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:BoundField>
                    <asp:BoundField HeaderText="Total % Excluded" DataField="pertotexclude" SortExpression="[Total % Excluded]">
                        <HeaderStyle HorizontalAlign="Left" />
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:BoundField>
                    <asp:BoundField HeaderText="Hit Applied" DataField="hit" SortExpression="[Hit Applied]">
                        <HeaderStyle HorizontalAlign="Left" />
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:BoundField>
                    <asp:BoundField HeaderText="% Hit Applied" DataField="perhit" SortExpression="[% Hit Applied]">
                        <HeaderStyle HorizontalAlign="Left" />
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:BoundField>
                    <asp:BoundField HeaderText="Revised Due Date" DataField="revise" SortExpression="[Revised Due Date]">
                        <HeaderStyle HorizontalAlign="Left" />
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:BoundField>
                    <asp:BoundField HeaderText="% Revised" DataField="perrevise" SortExpression="[% Revised]">
                        <HeaderStyle HorizontalAlign="Left" />
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:BoundField>
                    <asp:BoundField HeaderText="Total Hit/Exclude/Revised" DataField="alltotal" SortExpression="[Total Hit/Exclude/Revised]">
                        <HeaderStyle HorizontalAlign="Left" />
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:BoundField>
                    <asp:BoundField HeaderText="Total %" DataField="peralltotal" SortExpression="[Total %]">
                        <HeaderStyle HorizontalAlign="Left" />
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:BoundField>
                </Columns>
            </asp:GridView>


        </div>

    </div>
</asp:Content>

﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="VendorReportingExclusionOverview.aspx.cs" Inherits="VendorReportingExclusionOverview" 
MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="../../../CommonUI/UserControls/ucAddButton.ascx" TagName="ucAddButton"
    TagPrefix="cc2" %>
<%@ Register Src="../../../CommonUI/UserControls/ucExportToExcel.ascx" TagName="ucExportToExcel"
    TagPrefix="cc2" %>

    <asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2>
        <cc1:ucLabel ID="lblVendorReportingExclusionOverview" runat="server" Text="Vendor Reporting Exclusion Overview"></cc1:ucLabel>
    </h2>
    <div class="button-row">
        <cc2:ucAddButton ID="btnAdd" runat="server" NavigateUrl="VendorReportingExclusion.aspx" />
        <cc2:ucExportToExcel ID="ucExportToExcel" runat="server" />
    </div>
    <table width="100%">
        <tr>
            <td align="center">
                <cc1:ucGridView ID="grdVendorReportingExclusion" Width="100%" runat="server" CssClass="grid" onsorting="SortGrid" AllowSorting="true">
                    <Columns>
                       
                        <asp:TemplateField HeaderText="RationaleNarrative" SortExpression="ExclusionNarrative">
                            <HeaderStyle Width="60%" HorizontalAlign="Left" />
                            <ItemTemplate>
                                <asp:HyperLink ID="hpExclusionID" runat="server" Text='<%# Eval("ExclusionNarrative")  %>'
                                    NavigateUrl='<%# EncryptQuery("VendorReportingExclusion.aspx?ExclusionID="+Eval("ExclusionID")) %>'></asp:HyperLink>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:TemplateField>
                        <asp:BoundField HeaderText="Who applied exclusion" DataField="AppliedBy" SortExpression="AppliedBy">
                            <HeaderStyle Width="20%" HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                        <asp:BoundField HeaderText="Date Exclusion was applied" DataField="ApplicableDate" DataFormatString="{0:dd/MM/yyyy}" SortExpression="ApplicableDate">
                            <HeaderStyle Width="20%" HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                    </Columns>
                </cc1:ucGridView>
            </td>
        </tr>
    </table>
</asp:Content>
﻿using BusinessEntities.ModuleBE.OnTimeInFull;
using BusinessLogicLayer.ModuleBAL.OnTimeInFull;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Utilities;
using WebUtilities;

public partial class ModuleUI_OnTimeInFull_Reports_ExcludedPOReport : CommonPage
{
    protected string siteRequired = WebCommon.getGlobalResourceValue("SiteRequired");
    protected string NoRecordsFound = WebCommon.getGlobalResourceValue("NoRecordsFound");
    protected void Page_Init(object sender, EventArgs e)
    {
        ddlSite.CurrentPage = this;
        ddlSite.IsAllRequired = true;
        ddlSite.SchedulingContact = true;
        ddlSite.CurrentPage = this;
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        btnExportToExcel1.GridViewControl = UcGridViewExport;
        btnExportToExcel1.FileName = "Excluded_POs_Report-Total";

        if (!IsPostBack)
        {
            txtDateFrom.innerControltxtDate.Value = DateTime.Now.AddDays(-DateTime.Now.Day).AddDays(1).ToString("dd/MM/yyyy");
            txtDateTo.innerControltxtDate.Value = DateTime.Now.ToString("dd/MM/yyyy");
            if (!string.IsNullOrEmpty(GetQueryStringValue("IsAllFilter")))
            {
                BindGridView();
                lblExcludedPOsReportnew.Text = WebCommon.getGlobalResourceValue("ExcludedPOAll");
                btnBack.Visible = true;
            }
            else
            {
                lblExcludedPOsReportnew.Text = WebCommon.getGlobalResourceValue("ExcludedPO");
                btnBack.Visible = false;
            }
        }

    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        BindGridView();
    }

    private void BindGridView()
    {
        ApplyOTIFExclusionBE objApplyOTIFExclusionBE = new ApplyOTIFExclusionBE();
        ApplyOTIFExclusionBAL objApplyOTIFExclusionBAL = new ApplyOTIFExclusionBAL();
        if (!string.IsNullOrEmpty(GetQueryStringValue("IsAllFilter")))
        {
            objApplyOTIFExclusionBE.Action = "SubDetailSiteWise";
            objApplyOTIFExclusionBE.SiteID = 0;
            objApplyOTIFExclusionBE.DateFrom = Common.GetYYYY_MM_DD(GetQueryStringValue("DateFrom"));
            if (!string.IsNullOrEmpty(GetQueryStringValue("DateTo")))
            {
                objApplyOTIFExclusionBE.DateTo = Common.GetYYYY_MM_DD(GetQueryStringValue("DateTo"));
            }
            else
            {
                objApplyOTIFExclusionBE.DateTo = Common.GetYYYY_MM_DD(GetQueryStringValue("DateFrom"));
            }
        }
        else
        {
            objApplyOTIFExclusionBE.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedValue);
            if (objApplyOTIFExclusionBE.SiteID == 0)
            {
                objApplyOTIFExclusionBE.Action = "AllSiteWise";
            }
            objApplyOTIFExclusionBE.DateFrom = Common.GetYYYY_MM_DD(txtDateFrom.innerControltxtDate.Value);
            objApplyOTIFExclusionBE.DateTo = Common.GetYYYY_MM_DD(txtDateTo.innerControltxtDate.Value);
        }


        List<ApplyOTIFExclusionBE> list = objApplyOTIFExclusionBAL.GetExcludedPoBAL(objApplyOTIFExclusionBE);


        if (string.IsNullOrEmpty(GetQueryStringValue("IsAllFilter")))
        {
            list.ToList().ForEach(x => x.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedValue));
        }
        if (!string.IsNullOrEmpty(GetQueryStringValue("IsAllFilter")))
        {
            tblSearch.Visible = false;
            btnExportToExcel2.Visible = true;
            Session["ApplyOTIFExclusionBE"] = objApplyOTIFExclusionBE;
        }
        if (list.Count > 0)
        {
            UcGridView1.DataSource = list;
            UcGridView1.DataBind();
            UcGridViewExport.DataSource = list;
            UcGridViewExport.DataBind();
            lblError.Text = string.Empty;

        }
        else
        {
            UcGridView1.DataSource = null;
            UcGridView1.DataBind();
            UcGridViewExport.DataSource = null;
            UcGridViewExport.DataBind();
            lblError.Text = NoRecordsFound;
        }
    }
    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (UcGridView1.Rows.Count > 0)
        {
            GridViewRow row = UcGridView1.Rows[UcGridView1.Rows.Count - 1];
            if (Convert.ToInt32(ddlSite.innerControlddlSite.SelectedValue) == 0)
            {
                HyperLink lnkAll = (HyperLink)row.FindControl("lnkAll");
                lnkAll.NavigateUrl = EncryptQuery("../ApplyOTIFExclusionOverview.aspx?&DateTo=" + txtDateTo.innerControltxtDate.Value + "&DateFrom=" + txtDateFrom.innerControltxtDate.Value + "&SiteID=" + ddlSite.innerControlddlSite.SelectedValue);
            }
            else
            {
                HyperLink lblDateFrom = (HyperLink)row.FindControl("lblDateFrom");
                lblDateFrom.NavigateUrl = EncryptQuery("../ApplyOTIFExclusionOverview.aspx?&DateTo=" + txtDateTo.innerControltxtDate.Value + "&DateFrom=" + txtDateFrom.innerControltxtDate.Value + "&SiteID=" + ddlSite.innerControlddlSite.SelectedValue);
            }
        }
    }

    protected void UcGridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            HyperLink hdnDateFrom = (HyperLink)e.Row.FindControl("lblDateFrom");
            HyperLink lnkAll = (HyperLink)e.Row.FindControl("lnkAll");
            Label lblshowAll = (Label)e.Row.FindControl("lblshowAll");
            Label lblSite = (Label)e.Row.FindControl("lblSiteName");
            if (!string.IsNullOrEmpty(GetQueryStringValue("IsAllFilter")))
            {
                UcGridView1.Columns[3].HeaderText = "Count of working days";
                UcGridView1.Columns[4].HeaderText = "Capacity Full";
                hdnDateFrom.Visible = false;
                lnkAll.Visible = false;
                lblshowAll.Visible = true;
                UcGridView1.Columns[0].Visible = true;
                if (lblSite.Text == "ZZReplace")
                {
                    lblSite.Text = "";
                }
                if (lblshowAll.Text == "Total")
                {
                    e.Row.Cells[0].ForeColor = Color.Black;
                    e.Row.Cells[1].ForeColor = Color.Black;
                    e.Row.Cells[2].ForeColor = Color.Black;
                    e.Row.Cells[3].ForeColor = Color.Black;
                    e.Row.Cells[4].ForeColor = Color.Black;
                    e.Row.Cells[5].ForeColor = Color.Black;
                    e.Row.Cells[6].ForeColor = Color.Black;
                    e.Row.Cells[7].ForeColor = Color.Black;
                    e.Row.Cells[8].ForeColor = Color.Black;
                    e.Row.Cells[9].ForeColor = Color.Black;
                    e.Row.Cells[10].ForeColor = Color.Black;
                    e.Row.Cells[11].ForeColor = Color.Black;
                    e.Row.Cells[12].ForeColor = Color.Black;
                    e.Row.Cells[13].ForeColor = Color.Black;
                    e.Row.Cells[0].Font.Bold = true;
                    e.Row.Cells[1].Font.Bold = true;
                    e.Row.Cells[2].Font.Bold = true;
                    e.Row.Cells[3].Font.Bold = true;
                    e.Row.Cells[4].Font.Bold = true;
                    e.Row.Cells[5].Font.Bold = true;
                    e.Row.Cells[6].Font.Bold = true;
                    e.Row.Cells[7].Font.Bold = true;
                    e.Row.Cells[8].Font.Bold = true;
                    e.Row.Cells[9].Font.Bold = true;
                    e.Row.Cells[10].Font.Bold = true;
                    e.Row.Cells[11].Font.Bold = true;
                    e.Row.Cells[12].Font.Bold = true;
                    e.Row.Cells[13].Font.Bold = true;
                }
            }
            else
            {
                if (Convert.ToInt32(ddlSite.innerControlddlSite.SelectedValue) == 0)
                {

                    hdnDateFrom.Visible = false;
                    lnkAll.Visible = true;
                    lblshowAll.Visible = false;
                    UcGridView1.Columns[0].Visible = false;
                    UcGridView1.Columns[2].Visible = false;
                    UcGridView1.Columns[3].HeaderText = "Count of working days lost";
                    UcGridView1.Columns[4].HeaderText = "# Sites full";

                }
                else
                {
                    hdnDateFrom.Visible = true;
                    lnkAll.Visible = false;
                    lblshowAll.Visible = false;
                    UcGridView1.Columns[0].Visible = false;
                    UcGridView1.Columns[2].Visible = true;
                    UcGridView1.Columns[3].HeaderText = "Count of working days";
                    UcGridView1.Columns[4].HeaderText = "Capacity Full";
                }
            }
        }
    }

    protected void UcGridView2_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Label lblDateFrm = (Label)e.Row.FindControl("lblDateFrom");
            Label lblSite = (Label)e.Row.FindControl("lblSiteName");
            if (!string.IsNullOrEmpty(GetQueryStringValue("IsAllFilter")))
            {
                UcGridViewExport.Columns[3].HeaderText = "Date Capacity Reached";
                UcGridViewExport.Columns[4].HeaderText = "Capacity Full";
                lblDateFrm.Visible = false;

                UcGridViewExport.Columns[0].Visible = true;
                if (lblSite.Text == "ZZReplace")
                {
                    lblSite.Text = "";
                }
            }
            else
            {
                if (Convert.ToInt32(ddlSite.innerControlddlSite.SelectedValue) == 0)
                {
                    lblDateFrm.Visible = false;
                    UcGridViewExport.Columns[0].Visible = false;
                    UcGridViewExport.Columns[2].Visible = false;
                    UcGridViewExport.Columns[3].HeaderText = "Count of working days lost";
                    UcGridViewExport.Columns[4].HeaderText = "# Sites full";

                }
                else
                {
                    lblDateFrm.Visible = true;

                    UcGridViewExport.Columns[0].Visible = false;
                    UcGridViewExport.Columns[2].Visible = true;
                    UcGridViewExport.Columns[3].HeaderText = "Count of working days";
                    UcGridViewExport.Columns[4].HeaderText = "Capacity Full";
                }
            }
        }
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        Page.ClientScript.RegisterStartupScript(this.GetType(), "script", "window.close()", true);
    }

    protected void btnExport_Click(object sender, EventArgs e)
    {
        BindGridviewExcel();
    }
    public void BindGridviewExcel()
    {

        if (Session["ApplyOTIFExclusionBE"] != null)
        {
            ApplyOTIFExclusionBAL objApplyOTIFExclusionBAL = new ApplyOTIFExclusionBAL();
            ApplyOTIFExclusionBE objApplyOTIFExclusionBE = (ApplyOTIFExclusionBE)Session["ApplyOTIFExclusionBE"];
            objApplyOTIFExclusionBE.Action = "Export";
            DataTable dt = objApplyOTIFExclusionBAL.GetExcludedPoExportBAL(objApplyOTIFExclusionBE);
            ExportToExcelDetailedTracker(dt, "Excluded_PO_Report-All_Warehouses");
        }
    }

    public void ExportToExcelDetailedTracker(DataTable dtExport, string fileName)
    {
        var a = dtExport.AsEnumerable().Where(x => x.Field<string>("site") == "ZZReplace");
        foreach (var item in a)
        {
            item.SetField("site", "");
        }
        HttpContext context = HttpContext.Current;
        string date = DateTime.Now.ToString("ddMMyyyy");
        string time = DateTime.Now.ToString("HH:mm");
        context.Response.ContentType = "text/vnd.ms-excel";
        context.Response.AddHeader("Content-Disposition", string.Format("attachment; filename={0}_{1}_{2}", fileName, date, time + ".xls"));
        if (dtExport.Rows.Count > 0)
        {

            for (int i = 0; i < dtExport.Columns.Count; i++)
            {
                context.Response.Write(dtExport.Columns[i].ColumnName);
                context.Response.Write("\t");
            }
        }

        context.Response.Write(Environment.NewLine);

        //Write data
        foreach (DataRow row in dtExport.Rows)
        {

            for (int i = 0; i < dtExport.Columns.Count; i++)
            {
                string rowdata = string.Empty;
                rowdata = Convert.ToString(row.ItemArray[i]) == "" ? "" : Convert.ToString(row.ItemArray[i]);
                context.Response.Write(rowdata);
                context.Response.Write("\t");
            }
            context.Response.Write(Environment.NewLine);
        }
        context.Response.End();
    }
}
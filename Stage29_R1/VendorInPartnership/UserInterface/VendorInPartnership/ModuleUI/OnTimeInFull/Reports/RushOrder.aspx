﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    AutoEventWireup="true" CodeFile="RushOrder.aspx.cs" Inherits="ModuleUI_OnTimeInFull_Reports_RushOrder" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/ucVendorSelectionTemplate.ascx" TagName="VendorSelectTemplate"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectStockPlanner.ascx" TagName="MultiSelectStockPlanner"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectVendor.ascx" TagName="MultiSelectVendor"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectSite.ascx" TagName="MultiSelectSite"
    TagPrefix="cc1" %>
<%@ Register TagName="ucExportButton" TagPrefix="uc1" Src="~/CommonUI/UserControls/ucExportToExcel.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript" language="javascript">
        function setValue1(target) {
            document.getElementById('<%=hdnJSFromDt.ClientID %>').value = target.value;
        }

        function setValue2(target) {
            document.getElementById('<%=hdnJSToDt.ClientID %>').value = target.value;
        }

        $(document).ready(function () {
            $("body").keypress(function (e) {
                if (e.which == 13 || e.keyCode == 13) {
                    $('#<%=btnSearch.ClientID %>').click();
                    return false;
                }
            });
        });
    </script>
    <h2>
        <cc1:ucLabel ID="lblRushOrder" runat="server" Text="Rush Order"></cc1:ucLabel>
    </h2>
    <asp:HiddenField ID="hdnJSFromDt" runat="server" />
    <asp:HiddenField ID="hdnJSToDt" runat="server" />
    <asp:ScriptManager ID="SM1" runat="server" AsyncPostBackTimeout="1200">
    </asp:ScriptManager>
    <div class="right-shadow">
        <div class="formbox">
            <cc1:ucPanel ID="UcDataPanel" runat="server">
                <table width="95%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                    <tr>
                        <td align="right">
                            <cc1:VendorSelectTemplate runat="server" ID="ucVendorTemplateSelect" />
                        </td>
                    </tr>
                </table>
                <table width="95%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                    <tr>
                        <td style="font-weight: bold; width: 10%">
                            <cc1:ucLabel ID="lblStockPlanner" runat="server" Text="Stock Planner"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold; width: 1%">
                            <cc1:ucLabel ID="Label17" runat="server">:</cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold; width: 89%">
                            <cc1:MultiSelectStockPlanner runat="server" ID="msStockPlanner" />
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblVendor" runat="server" Text="Vendor"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="UcLabel2" runat="server">:</cc1:ucLabel>
                        </td>
                        <td>
                            <cc1:MultiSelectVendor runat="server" ID="msVendor" />
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblSite" runat="server" Text="Site"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="UcLabel4" runat="server">:</cc1:ucLabel>
                        </td>
                        <td>
                            <cc1:MultiSelectSite runat="server" ID="msSite" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <table width="100%">
                                <tr style="height: 3em;">
                                    <td style="font-weight: bold;">
                                        <cc1:ucLabel ID="lblDateFrom" runat="server" Text="Date From"></cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold;">
                                        <cc1:ucLabel ID="UcLabel1" runat="server">:</cc1:ucLabel>
                                    </td>
                                    <td>
                                        <table cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style="font-weight: bold; width: 6em;">
                                                    <cc1:ucTextbox ID="txtFromDate" ClientIDMode="Static" runat="server" onchange="setValue1(this)"
                                                        CssClass="date" ReadOnly="True" Width="70px" />
                                                </td>
                                                <td style="font-weight: bold; width: 4em; text-align: center">
                                                    <cc1:ucLabel ID="lblTo" runat="server" Text="To"></cc1:ucLabel>
                                                </td>
                                                <td style="font-weight: bold; width: 6em;">
                                                    <cc1:ucTextbox ID="txtToDate" ClientIDMode="Static" runat="server" onchange="setValue2(this)"
                                                        CssClass="date" ReadOnly="True" Width="70px" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr style="height: 3em;">
                                    <td width="10%" style="font-weight: bold;">
                                        <cc1:ucLabel ID="lblStatus" runat="server" Text="Status"></cc1:ucLabel>
                                    </td>
                                    <td width="1%" style="font-weight: bold;">
                                        <cc1:ucLabel ID="UcLabel6" runat="server">:</cc1:ucLabel>
                                    </td>
                                    <td width="89%">
                                        <table cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style="font-weight: bold;" class="checkbox-list">
                                                    <cc1:ucRadioButton ID="rdoAllOnly" runat="server" Text="All" GroupName="ReportType"
                                                        Checked="true" />
                                                </td>
                                                <td style="font-weight: bold;" class="checkbox-list">
                                                    <cc1:ucRadioButton ID="rdoAmended" runat="server" Text="Amended" GroupName="ReportType" />
                                                </td>
                                                <td style="font-weight: bold;" class="checkbox-list">
                                                    <cc1:ucRadioButton ID="rdoNotAmended" runat="server" Text="Not Amended" GroupName="ReportType" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="right" colspan="3">
                            <div class="button-row">
                                <cc1:ucButton ID="btnSearch" runat="server" Text="Search" CssClass="button" OnClick="btnSearch_Click" />
                            </div>
                        </td>
                    </tr>
                </table>
            </cc1:ucPanel>
            <cc1:ucPanel ID="UcReportPanel" runat="server" Visible="false">
                <div class="button-row">
                    <uc1:ucexportbutton id="btnExportToExcel" runat="server" />
                </div>
                <%--<div style="width: 980px; overflow: scroll;">--%>
                    <cc1:ucGridView ID="gvRushOrder"  runat="server" CssClass="grid gvclass searchgrid-1" OnSorting="SortGrid"
                        PageSize="50" AllowPaging="true" PagerStyle-HorizontalAlign="left" AllowSorting="true"
                         OnPageIndexChanging="gvRushOrder_PageIndexChanging">
                        <EmptyDataTemplate>
                            <div style="text-align: center">
                                <cc1:ucLabel ID="lblRecordNotFound" isRequired="true" runat="server" Text="No record found"></cc1:ucLabel>
                            </div>
                        </EmptyDataTemplate>
                        <Columns>
                            <asp:TemplateField HeaderText="PO" SortExpression="Purchase_order">
                                <HeaderStyle Width="50px" HorizontalAlign="Left" />
                                <ItemTemplate>
                                    <asp:HyperLink ID="hpPurchaseOrder" runat="server" Text='<%# Eval("Purchase_order")  %>'
                                        NavigateUrl='<%# EncryptQuery("../RevisedLeadTimeCalculationsEdit.aspx?PreviousPage=PRO&PurchaseOrderID="+Eval("PurchaseOrderID")+"&Warehouse="+Eval("Warehouse")+ "&SiteId=" + Eval("SiteID")+"&PurchaseOrderNo="+Eval("Purchase_order")) %>'></asp:HyperLink>
                                    <%--<asp:HyperLink ID="hpPurchaseOrder" runat="server" Text='<%# Eval("Purchase_order")  %>'
                                        NavigateUrl='<%# EncryptQuery("../RevisedLeadTimeCalculationsEdit.aspx?PreviousPage=PRO&PurchaseOrderNo=" + Eval("Purchase_order") + "&SiteId=" + Eval("SiteID") +"&PurchaseOrderID=" + Eval("PurchaseOrderID") + "&PageMode=Add")%>'></asp:HyperLink>--%>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="StockPlannerNumber" DataField="StockPlannerNo"
                                AccessibleHeaderText="false" SortExpression="StockPlannerNo">
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:BoundField ItemStyle-Wrap="false" HeaderText=" StockPlNameSDR" DataField="StockPlannerName"
                                AccessibleHeaderText="false" SortExpression="StockPlannerName">
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:BoundField ItemStyle-Wrap="false" HeaderText="VendorNumber" DataField="Vendor_No"
                                AccessibleHeaderText="false" SortExpression="Vendor_No">
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:BoundField ItemStyle-Wrap="false" HeaderText="VendorName" DataField="VendorName"
                                AccessibleHeaderText="false" SortExpression="VendorName">
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:BoundField HeaderText="ODSKU" DataField="OD_SKU_NO" SortExpression="OD_SKU_NO"
                                ItemStyle-Wrap="false">
                                <HeaderStyle HorizontalAlign="Center"/>
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:BoundField HeaderText="VikingSKU" DataField="Direct_SKU" SortExpression="Direct_SKU"
                                ItemStyle-Wrap="false">
                                <HeaderStyle HorizontalAlign="Center"/>
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:BoundField HeaderText="Description1" DataField="Product_description" SortExpression="Product_description"
                                ItemStyle-Wrap="false">
                                <HeaderStyle HorizontalAlign="Left" />
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField HeaderText="LeadTime" DataField="Product_Lead_time" SortExpression="Product_Lead_time"
                                ItemStyle-Wrap="false">
                                <HeaderStyle HorizontalAlign="Center"/>
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:BoundField HeaderText="RaisedDate" DataFormatString="{0:dd-MM-yyyy}" ItemStyle-Wrap="false"
                                DataField="Order_raised" SortExpression="Order_raised">
                                <HeaderStyle HorizontalAlign="Center"/>
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:BoundField HeaderText="DueDate" DataFormatString="{0:dd-MM-yyyy}" ItemStyle-Wrap="false"
                                DataField="Original_due_date" SortExpression="Original_due_date">
                            </asp:BoundField>
                            <asp:BoundField HeaderText="Variance" DataField="Variance" AccessibleHeaderText="false"
                                ItemStyle-Wrap="false" SortExpression="Variance">
                                 <HeaderStyle HorizontalAlign="Center"/>
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:BoundField HeaderText="RevisedDueDate" DataFormatString="{0:dd-MM-yyyy}" ItemStyle-Wrap="false"
                                DataField="RevisedDueDate" SortExpression="RevisedDueDate">
                                <HeaderStyle HorizontalAlign="Center"/>
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                        </Columns>
                    </cc1:ucGridView>
                    <cc1:ucGridView ID="tempGridView" Visible="false" Width="100%" runat="server" CssClass="grid"
                        OnSorting="SortGrid" AllowSorting="true" Style="overflow: auto;">
                        <Columns>
                            <asp:TemplateField HeaderText="PO" SortExpression="Purchase_order">
                                <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                <ItemTemplate>
                                    <asp:Label ID="lblPOrders" runat="server" Text='<%# Eval("Purchase_order")  %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:TemplateField>
                            <asp:BoundField ItemStyle-Wrap="false" HeaderText="StockPlannerNumber" DataField="StockPlannerNo"
                                AccessibleHeaderText="false" SortExpression="StockPlannerNo">
                                <HeaderStyle Width="100px" HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:BoundField ItemStyle-Wrap="false" HeaderText=" StockPlNameSDR" DataField="StockPlannerName"
                                AccessibleHeaderText="false" SortExpression="StockPlannerName">
                                <HeaderStyle Width="100px" HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:BoundField ItemStyle-Wrap="false" HeaderText="VendorNumber" DataField="Vendor_No"
                                AccessibleHeaderText="false" SortExpression="Vendor_No">
                                <HeaderStyle Width="100px" HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:BoundField ItemStyle-Wrap="false" HeaderText="VendorName" DataField="VendorName"
                                AccessibleHeaderText="false" SortExpression="VendorName">
                                <HeaderStyle Width="100px" HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:BoundField HeaderText="ODSKU" DataField="OD_SKU_NO" SortExpression="OD_SKU_NO"
                                ItemStyle-Wrap="false">
                                <HeaderStyle HorizontalAlign="Center" Width="400px" />
                                <ItemStyle HorizontalAlign="Center" Wrap="false" />
                            </asp:BoundField>
                            <asp:BoundField HeaderText="VikingSKU" DataField="Direct_SKU" SortExpression="Direct_SKU"
                                ItemStyle-Wrap="false">
                                <HeaderStyle HorizontalAlign="Center" Width="400px" />
                                <ItemStyle HorizontalAlign="Center" Wrap="false" />
                            </asp:BoundField>
                            <asp:BoundField HeaderText="Description1" DataField="Product_description" SortExpression="Product_description"
                                ItemStyle-Wrap="false">
                                <HeaderStyle HorizontalAlign="Center" Width="100px" />
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:BoundField HeaderText="LeadTime" DataField="Product_Lead_time" SortExpression="Product_Lead_time"
                                ItemStyle-Wrap="false">
                                <HeaderStyle HorizontalAlign="Center" Width="400px" />
                                <ItemStyle HorizontalAlign="Center" Wrap="false" />
                            </asp:BoundField>
                            <asp:BoundField HeaderText="RaisedDate" DataFormatString="{0:dd-MM-yyyy}" ItemStyle-Wrap="false"
                                DataField="Order_raised" SortExpression="Order_raised">
                                <HeaderStyle HorizontalAlign="Center" Width="300px" />
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:BoundField HeaderText="DueDate" DataFormatString="{0:dd-MM-yyyy}" ItemStyle-Wrap="false"
                                DataField="Original_due_date" SortExpression="Original_due_date">
                                <HeaderStyle HorizontalAlign="Center" Width="300px" />
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:BoundField HeaderText="Variance" DataField="Variance" AccessibleHeaderText="false"
                                ItemStyle-Wrap="false" SortExpression="Variance">
                                <HeaderStyle Width="100px" HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:BoundField HeaderText="RevisedDueDate" DataFormatString="{0:dd-MM-yyyy}" ItemStyle-Wrap="false"
                                DataField="RevisedDueDate" SortExpression="RevisedDueDate">
                                <HeaderStyle HorizontalAlign="Center" Width="300px" />
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                        </Columns>
                    </cc1:ucGridView>
              <%--  </div>--%>
                <div class="button-row">
                    <cc1:ucButton ID="btnBack" runat="server" Text="Search" CssClass="button" OnClick="btnBack_Click" />
                </div>
            </cc1:ucPanel>
        </div>
    </div>
</asp:Content>

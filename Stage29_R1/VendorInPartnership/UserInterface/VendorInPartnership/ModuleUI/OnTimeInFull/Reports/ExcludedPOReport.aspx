﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    AutoEventWireup="true" CodeFile="ExcludedPOReport.aspx.cs" Inherits="ModuleUI_OnTimeInFull_Reports_ExcludedPOReport" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/ucSite.ascx" TagName="ucSite" TagPrefix="cc2" %>
<%@ Register Src="~/CommonUI/UserControls/ucDate.ascx" TagName="ucDate" TagPrefix="cc5" %>
<%@ Register Src="~/CommonUI/UserControls/ucDate2.ascx" TagName="ucDate" TagPrefix="cc6" %>
<%@ Register TagName="ucExportButton" TagPrefix="uc1" Src="~/CommonUI/UserControls/ucExportToExcel.ascx" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h2>
        <cc1:ucLabel ID="lblExcludedPOsReportnew" runat="server"></cc1:ucLabel>
    </h2>
    <%--        <script type="text/javascript">
            function Validate() {             
              if ($("select[id$='ddlSite'] option:selected").val() == 0) {
                    alert('<%=siteRequired%>');
                    $("select[id$='ddlSite']").focus();
                    return false;
                }                     
            } 
        </script>--%>
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div class="right-shadow">
        <asp:UpdatePanel ID="pnlUP1" runat="server">
            <ContentTemplate>
                <div class="formbox">
                    <table width="90%" cellspacing="5" cellpadding="0" class="form-table" id="tblSearch"
                        runat="server">
                        <tr>
                            <td style="font-weight: bold;">
                                <cc1:ucLabel ID="lblSite" runat="server"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold;">
                                <cc1:ucLabel ID="UcLabel1" runat="server">:</cc1:ucLabel>
                            </td>
                            <td style>
                                <cc2:ucSite ID="ddlSite" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold;">
                                <cc1:ucLabel ID="lblDateFrom" runat="server"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold;">
                                <cc1:ucLabel ID="UcLabel3" runat="server">:</cc1:ucLabel>
                            </td>
                            <td>
                                <cc5:ucDate ClientIDMode="Static" ID="txtDateFrom" runat="server" />
                                <asp:RequiredFieldValidator ID="rfvDateFromRequired" runat="server" Display="None"
                                    ControlToValidate="txtDateFrom$txtUCDate" ValidateEmptyText="true" ValidationGroup="a"
                                    SetFocusOnError="true"></asp:RequiredFieldValidator>
                            </td>
                            <td style="font-weight: bold;">
                                <cc1:ucLabel ID="lblDateTo" runat="server"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold;">
                                <cc1:ucLabel ID="UcLabel4" runat="server">:</cc1:ucLabel>
                            </td>
                            <td>
                                <cc6:ucDate ClientIDMode="Static" ID="txtDateTo" runat="server" />
                                <asp:RequiredFieldValidator ID="rfvDateToRequired" runat="server" Display="None"
                                    ControlToValidate="txtDateTo$txtUCDate2" ValidateEmptyText="true" ValidationGroup="a"
                                    SetFocusOnError="true"></asp:RequiredFieldValidator>
                            </td>
                            <td>
                                <cc1:ucButton ID="btnSearch" runat="server" CssClass="button" OnClick="btnSearch_Click"
                                    OnClientClick="return Validate();" Text="Search" />
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <uc1:ucExportButton ID="btnExportToExcel1" runat="server" />
                            </td>
                        </tr>
                    </table>
                    <div class="button-row">                       
                        <cc1:ucButton ID="btnExportToExcel2" runat="server" Visible="false" Text="Export To Excel" CssClass="exporttoexcel button" OnClick="btnExport_Click"
                             Width="109px" Height="20px" />
                    </div>
                </div>
                <%-- <tr>
                                <td colspan="4">--%>
                <asp:Label ID="lblError" runat="server"></asp:Label>
                <br />
                <br />
                <cc1:ucGridView ID="UcGridView1" runat="server" CssClass="grid" Width="100%" OnRowDataBound="UcGridView1_RowDataBound">
                    <Columns>
                        <asp:TemplateField HeaderText="Site" Visible="false">
                            <HeaderStyle HorizontalAlign="center" Width="10%" />
                            <ItemTemplate>
                                <asp:Label ID="lblSiteName" runat="server" Text='<%#Eval("SiteName") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="center" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Date">
                            <HeaderStyle HorizontalAlign="center" Width="10%" />
                            <ItemStyle HorizontalAlign="center" />
                            <ItemTemplate>
                                <asp:HyperLink ID="lblDateFrom" runat="server" Target="_blank" Text='<%#Eval("DateFrom") %>' NavigateUrl='<%# EncryptQuery("../ApplyOTIFExclusionOverview.aspx?&DateFrom="+Eval("DateFrom")+"&SiteID="+Eval("SiteID"))%>'></asp:HyperLink>
                                <asp:HyperLink ID="lnkAll" Visible="false" runat="server" Text='<%#Eval("DateFrom") %>'
                                    Target="_blank" NavigateUrl='<%# EncryptQuery("../Reports/ExcludedPOReport.aspx?&DateFrom="+Eval("DateFrom")+"&IsAllFilter= true")%>'></asp:HyperLink>
                                <asp:Label ID="lblshowAll" Visible="false" runat="server" Text='<%#Eval("DateFrom") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Date Capacity Reached">
                            <HeaderStyle HorizontalAlign="center" Width="10%" />
                            <ItemTemplate>
                                <asp:Label ID="ltDateCapacityReached" runat="server" Text='<%#Eval("CapacityReachDate") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="center" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Count of working days">
                            <HeaderStyle HorizontalAlign="center" Width="10%" />
                            <ItemTemplate>
                                <asp:Label ID="ltCountofworkingdays" runat="server" Text='<%#Eval("WorkinDaysCount") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="center" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Capacity Full">
                            <HeaderStyle HorizontalAlign="center" Width="10%" />
                            <ItemTemplate>
                                <asp:Label ID="lblCapacityFull" runat="server" Text='<%#Eval("CapacityFull") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="center" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Lines Due">
                            <HeaderStyle HorizontalAlign="center" Width="10%" />
                            <ItemStyle HorizontalAlign="center" />
                            <ItemTemplate>
                                <asp:Label ID="lblLinesDue" runat="server" Text='<%#Eval("LinesDue") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Lines Excluded Automated">
                            <HeaderStyle HorizontalAlign="center" Width="10%" />
                            <ItemStyle HorizontalAlign="center" />
                            <ItemTemplate>
                                <asp:Label ID="lblLinesExcluded" runat="server" Text='<%#Eval("LinesExcluded") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Lines Excluded Manually">
                            <HeaderStyle HorizontalAlign="center" Width="10%" />
                            <ItemStyle HorizontalAlign="center" />
                            <ItemTemplate>
                                <asp:Label ID="lblLinesExcludedManually" runat="server" Text='<%#Eval("LinesExcludedManually") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="% Excluded">
                            <HeaderStyle HorizontalAlign="center" Width="10%" />
                            <ItemStyle HorizontalAlign="center" />
                            <ItemTemplate>
                                <asp:Label ID="lblPerExcluded" runat="server" Text='<%#Eval("PerExcluded") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Applied Hits Automated">
                            <HeaderStyle HorizontalAlign="center" Width="10%" />
                            <ItemStyle HorizontalAlign="center" />
                            <ItemTemplate>
                                <asp:Label ID="lblAppliedHitAutomated" runat="server" Text='<%#Eval("AppliedHitAutomated") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Applied Hits Manual">
                            <HeaderStyle HorizontalAlign="center" Width="10%" />
                            <ItemStyle HorizontalAlign="center" />
                            <ItemTemplate>
                                <asp:Label ID="lblAppliedHitManually" runat="server" Text='<%#Eval("AppliedHitManually") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="% with Hits">
                            <HeaderStyle HorizontalAlign="center" Width="10%" />
                            <ItemStyle HorizontalAlign="center" />
                            <ItemTemplate>
                                <asp:Label ID="lblPercentwithHits" runat="server" Text='<%#Eval("PercentwithHits") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="# Exceptions">
                            <HeaderStyle HorizontalAlign="center" Width="10%" />
                            <ItemStyle HorizontalAlign="center" />
                            <ItemTemplate>
                                <asp:Label ID="lblExceptions" runat="server" Text='<%#Eval("Exceptions") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Overall % Exceptions">
                            <HeaderStyle HorizontalAlign="center" Width="10%" />
                            <ItemStyle HorizontalAlign="center" />
                            <ItemTemplate>
                                <asp:Label ID="lblOverallExceptions" runat="server" Text='<%#Eval("OverallExceptions") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </cc1:ucGridView>
                <cc1:ucGridView ID="UcGridViewExport" runat="server" CssClass="grid" Visible="false"
                    Width="100%" OnRowDataBound="UcGridView2_RowDataBound">
                    <Columns>
                        <asp:TemplateField HeaderText="Site">
                            <HeaderStyle HorizontalAlign="center" Width="10%" />
                            <ItemTemplate>
                                <asp:Label ID="lblSiteName" runat="server" Text='<%#Eval("SiteName") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="center" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Date">
                            <HeaderStyle HorizontalAlign="center" Width="10%" />
                            <ItemStyle HorizontalAlign="center" />
                            <ItemTemplate>
                                <asp:Label ID="lblDateFrom" runat="server" Text='<%#Eval("DateFrom") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Date Capacity Reached">
                            <HeaderStyle HorizontalAlign="center" Width="10%" />
                            <ItemTemplate>
                                <asp:Label ID="ltDateCapacityReached" runat="server" Text='<%#Eval("CapacityReachDate") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="center" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Count of working days">
                            <HeaderStyle HorizontalAlign="center" Width="10%" />
                            <ItemTemplate>
                                <asp:Label ID="ltCountofworkingdays" runat="server" Text='<%#Eval("WorkinDaysCount") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="center" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Capacity Full">
                            <HeaderStyle HorizontalAlign="center" Width="10%" />
                            <ItemTemplate>
                                <asp:Label ID="lblCapacityFull" runat="server" Text='<%#Eval("CapacityFull") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="center" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Lines Due">
                            <HeaderStyle HorizontalAlign="center" Width="10%" />
                            <ItemStyle HorizontalAlign="center" />
                            <ItemTemplate>
                                <asp:Label ID="lblLinesDue" runat="server" Text='<%#Eval("LinesDue") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Lines Excluded Automated">
                            <HeaderStyle HorizontalAlign="center" Width="10%" />
                            <ItemStyle HorizontalAlign="center" />
                            <ItemTemplate>
                                <asp:Label ID="lblLinesExcluded" runat="server" Text='<%#Eval("LinesExcluded") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Lines Excluded Manually">
                            <HeaderStyle HorizontalAlign="center" Width="10%" />
                            <ItemStyle HorizontalAlign="center" />
                            <ItemTemplate>
                                <asp:Label ID="lblLinesExcludedManually" runat="server" Text='<%#Eval("LinesExcludedManually") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="% Excluded">
                            <HeaderStyle HorizontalAlign="center" Width="10%" />
                            <ItemStyle HorizontalAlign="center" />
                            <ItemTemplate>
                                <asp:Label ID="lblPerExcluded" runat="server" Text='<%#Eval("PerExcluded") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Applied Hits Automated">
                            <HeaderStyle HorizontalAlign="center" Width="10%" />
                            <ItemStyle HorizontalAlign="center" />
                            <ItemTemplate>
                                <asp:Label ID="lblAppliedHitAutomated" runat="server" Text='<%#Eval("AppliedHitAutomated") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Applied Hits Manual">
                            <HeaderStyle HorizontalAlign="center" Width="10%" />
                            <ItemStyle HorizontalAlign="center" />
                            <ItemTemplate>
                                <asp:Label ID="lblAppliedHitManually" runat="server" Text='<%#Eval("AppliedHitManually") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="% with Hits">
                            <HeaderStyle HorizontalAlign="center" Width="10%" />
                            <ItemStyle HorizontalAlign="center" />
                            <ItemTemplate>
                                <asp:Label ID="lblPercentwithHits" runat="server" Text='<%#Eval("PercentwithHits") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="# Exceptions">
                            <HeaderStyle HorizontalAlign="center" Width="10%" />
                            <ItemStyle HorizontalAlign="center" />
                            <ItemTemplate>
                                <asp:Label ID="lblExceptions" runat="server" Text='<%#Eval("Exceptions") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Overall % Exceptions">
                            <HeaderStyle HorizontalAlign="center" Width="10%" />
                            <ItemStyle HorizontalAlign="center" />
                            <ItemTemplate>
                                <asp:Label ID="lblOverallExceptions" runat="server" Text='<%#Eval("OverallExceptions") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </cc1:ucGridView>
                <%--   </td>
                            </tr>--%>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="btnExportToExcel1" />
                  <asp:PostBackTrigger ControlID="btnExportToExcel2" />
            </Triggers>
        </asp:UpdatePanel>
        <br />
        <br />
        <div class="button-row">
            <asp:Button ID="btnBack" Text="Back" runat="server" Visible="false" OnClick="btnBack_Click" />
        </div>
    </div>
    </div>
</asp:Content>

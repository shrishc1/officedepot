﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="VendorReportingExclusion.aspx.cs"
    MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master" Inherits="VendorReportingExclusion" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectSite.ascx" TagName="MultiSelectSite"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectSKU.ascx" TagName="MultiSelectSKU"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectPurchaseOrders.ascx" TagName="MultiSelectPO"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectVendor.ascx" TagName="MultiSelectVendor"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectDiscrepancyNumber.ascx" TagName="MultiSelectDiscrepancyNumber"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectBookingNumber.ascx" TagName="MultiSelectBookingNumber"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectBookingType.ascx" TagName="MultiSelectBookingType"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectDateRange.ascx" TagName="MultiSelectDateRange"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/ucVendorSelectionTemplate.ascx" TagName="VendorSelectTemplate"
    TagPrefix="cc1" %>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2>
        <cc1:ucLabel ID="lblVendorReportingExclusion" runat="server" Text="Vendor Reporting Exclusion"></cc1:ucLabel>
    </h2>
    <script type="text/javascript" language="javascript">
        function setValue1(target) {
            document.getElementById('<%=hdnJSFromDt.ClientID %>').value = target.value;
        }

        function setValue2(target) {
            document.getElementById('<%=hdnJSToDt.ClientID %>').value = target.value;
        }


        function chkVendorID(sender, args) {
            //            if (document.getElementById('<%=msVendor.FindControl("hiddenSelectedIDs").ClientID%>') != null) {
            //                if (document.getElementById('<%=msVendor.FindControl("hiddenSelectedIDs").ClientID%>').value == "") {
            //                    //args.IsValid = false;
            //                }

            //            }
        }

        function confirmDelete() {
            return confirm('<%=deleteMessage%>');
        }
        
    </script>
    <asp:ScriptManager ID="SM1" runat="server" AsyncPostBackTimeout="2400">
    </asp:ScriptManager>
    <asp:HiddenField ID="hdnJSFromDt" runat="server" />
    <asp:HiddenField ID="hdnJSToDt" runat="server" />
    <div class="right-shadow">
        <div class="formbox">
              <table width="95%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                    <tr>
                        <td align="right">
                            <cc1:VendorSelectTemplate runat="server" ID="ucVendorTemplateSelect" />
                        </td>
                    </tr>
                </table>
            <table width="100%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                <tr>
                    <td style="font-weight: bold;" colspan="3">
                        <table width="100%">
                            <tr>
                                <td width="25%">
                                    <cc1:ucLabel ID="lblVendorRationale" runat="server" isRequired="true" Text="Rationale/Narrative for the exclusion"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold;" width="2%">
                                    <cc1:ucLabel ID="UcLabel3" runat="server">:</cc1:ucLabel>
                                </td>
                                <td width="73%">
                                    <cc1:ucTextbox ID="txtExclusionNarrative" runat="server" MaxLength="1000" Width="98%"></cc1:ucTextbox>
                                    <asp:RequiredFieldValidator ID="rfvExclusionNarrativeRequired" runat="server" ControlToValidate="txtExclusionNarrative"
                                        Display="None" ValidationGroup="apply">
                                    </asp:RequiredFieldValidator>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <cc1:ucPanel ID="pnlDateDependent" runat="server" GroupingText="Date Dependant" CssClass="fieldset-form">
                <table width="90%" cellspacing="1" cellpadding="0" class="form-table">
                    <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblDateFrom" runat="server" Text="Date From"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="UcLabel1" runat="server">:</cc1:ucLabel>
                        </td>
                        <td align="left">
                            <table cellpadding="0" cellspacing="0">
                                <tr>
                                    <td style="font-weight: bold; width: 6em;">
                                        <cc1:ucTextbox ID="txtFromDate" ClientIDMode="Static" runat="server" onchange="setValue1(this)"
                                            ReadOnly="True" CssClass="date" Width="70px" />
                                    </td>
                                    <td style="font-weight: bold; width: 4em; text-align: center">
                                        <cc1:ucLabel ID="lblTo" runat="server" Text="To"></cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold; width: 6em;">
                                        <cc1:ucTextbox ID="txtToDate" ClientIDMode="Static" runat="server" onchange="setValue2(this)"
                                            ReadOnly="True" CssClass="date" Width="70px" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold;" width="14%">
                            <cc1:ucLabel ID="lblVendor" isRequired="true" runat="server" Text="Vendor"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;" width="1%">
                            <cc1:ucLabel ID="UcLabel2" runat="server">:</cc1:ucLabel>
                        </td>
                        <td width="85%">
                            <cc1:MultiSelectVendor runat="server" ID="msVendor" />
                            <asp:CustomValidator ID="cusvVendorRequired" runat="server" ClientValidationFunction="chkVendorID"
                                Display="None" ValidationGroup="apply">
                            </asp:CustomValidator>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold;" width="14%">
                            <cc1:ucLabel ID="lblSite" runat="server" Text="Site"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;" width="1%">
                            <cc1:ucLabel ID="UcLabel4" runat="server">:</cc1:ucLabel>
                        </td>
                        <td width="85%">
                            <cc1:MultiSelectSite runat="server" ID="msSite" />
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblODSKUCode" runat="server" Text="OD SKU Code"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="UcLabel8" runat="server">:</cc1:ucLabel>
                        </td>
                        <td>
                            <cc1:MultiSelectSKU runat="server" ID="msSKU" />
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblDeliveryType" runat="server" Text="Delivery Type"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="UcLabel7" runat="server">:</cc1:ucLabel>
                        </td>
                        <td>
                            <cc1:MultiSelectBookingType runat="server" ID="MultiSelectBookingType" />
                        </td>
                    </tr>
                </table>
            </cc1:ucPanel>
            <cc1:ucPanel ID="pnlOtherExclusions" runat="server" GroupingText="Other Exclusions"
                CssClass="fieldset-form">
                <table width="90%" cellspacing="0" cellpadding="0" class="form-table">
                    <%--<tr>
                        <td style="font-weight: bold;" width="14%">
                            <cc1:ucLabel ID="lblPurchaseOrderSDR" runat="server" Text="Purchase Order"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;" width="1%">
                            <cc1:ucLabel ID="UcLabel9" runat="server">:</cc1:ucLabel>
                        </td>
                        <td width="85%">
                            <cc1:MultiSelectPO runat="server" ID="msPO" />
                        </td>
                    </tr>--%>
                    <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblDiscrepancyNumber" runat="server" Text="Discrepancy Number"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="UcLabel5" runat="server">:</cc1:ucLabel>
                        </td>
                        <td>
                            <cc1:MultiSelectDiscrepancyNumber runat="server" ID="MultiSelectDiscrepancyNumber" />
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblBookingNumber" runat="server" Text="Booking Number"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="UcLabel6" runat="server">:</cc1:ucLabel>
                        </td>
                        <td>
                            <cc1:MultiSelectBookingNumber runat="server" ID="MultiSelectBookingNumber" />
                        </td>
                    </tr>
                </table>
            </cc1:ucPanel>
        </div>
    </div>
    <div class="bottom-shadow">
    </div>
    <asp:UpdatePanel ID="Up1" runat="server">
        <contenttemplate>
            <div class="button-row">
                <cc1:ucButton ID="btnApply" runat="server" Text="Apply" CssClass="button" ValidationGroup="apply"
                    OnClick="btnApply_Click" />
                <asp:ValidationSummary ID="vs1" runat="server" ValidationGroup="apply" ShowMessageBox="true"
                    ShowSummary="false" />
                <cc1:ucButton ID="btnDelete" runat="server" Text="Delete" CssClass="button" OnClick="btnDelete_Click"
                    OnClientClick=" return confirmDelete();" Visible="false" />
                <cc1:ucButton ID="btnCancel" runat="server" Text="Cancel" CssClass="button" OnClick="btnCancel_Click" />
            </div>
        </contenttemplate>
    </asp:UpdatePanel>
</asp:Content>

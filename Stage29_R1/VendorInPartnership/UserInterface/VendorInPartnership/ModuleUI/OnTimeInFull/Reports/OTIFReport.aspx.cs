﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.ReportRequest;
using BusinessEntities.ModuleBE.Security;
using BusinessLogicLayer.ModuleBAL.ReportRequest;
using BusinessLogicLayer.ModuleBAL.Security;
using Microsoft.Reporting.WebForms;
using Utilities;
using WebUtilities;

public partial class OTIFReport : CommonPage
{

    int? ReportRequest = 0;
    ReportRequestBE oReportRequestBE = new ReportRequestBE();
    ReportRequestBAL oReportRequestBAL = new ReportRequestBAL();

    #region ReportVariables
    Warning[] warnings;
    string[] streamIds;
    string mimeType = string.Empty;
    string encoding = string.Empty;
    string extension = string.Empty;
    string reportFileName = string.Empty;
    #endregion

    protected void Page_Init(object sender, EventArgs e)
    {
        ucVendorTemplateSelect.CurrentPage = this;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            txtFromDate.Text = DateTime.Now.AddDays(-DateTime.Now.Day + 1).ToString("dd/MM/yyyy");
            txtToDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
            hdnJSFromDt.Value = txtFromDate.Text;
            hdnJSToDt.Value = txtToDate.Text;
            int YearFrom = DateTime.Now.Year;
            int YearTo = DateTime.Now.Year - 10;
            while (YearFrom > YearTo)
            {
                if (YearFrom >= 2013)
                    ddlYear.Items.Add(YearFrom.ToString());
                YearFrom--;
            }
        }
    }

    /// <summary>
    /// Method to show Vendor on the basis of selected Template
    /// </summary>
    public override void TemplateSelectedIndexChanged()
    {
        base.TemplateSelectedIndexChanged();

        int VendorTemplateId = Convert.ToInt32(ucVendorTemplateSelect.innerControlddlTemplate.SelectedValue);

        ListBox lstSelectedVendor = (ListBox)msVendor.FindControl("lstRight");
        ListBox lstVendor = (ListBox)msVendor.FindControl("ucVendor").FindControl("lstLeft");

        if (VendorTemplateId > 0)
        {

            SCT_TemplateBAL oSCT_TemplateBAL = new SCT_TemplateBAL();
            SCT_TemplateBE oSCT_TemplateBE = new SCT_TemplateBE();

            oSCT_TemplateBE.VendorTemplateID = VendorTemplateId;
            oSCT_TemplateBE.Action = "GetVendorTemplateInfobyID";
            List<SCT_TemplateBE> lstVendorTemplate = oSCT_TemplateBAL.GetVendorTemplateById(oSCT_TemplateBE);

            if (lstVendorTemplate != null && lstVendorTemplate.Count > 0)
            {
                lstSelectedVendor.Items.Clear();
                msVendor.innerControlHiddenField.Value = string.Empty;
                msVendor.innerControlHiddenField2.Value = string.Empty;
                for (int iCount = 0; iCount < lstVendorTemplate.Count; iCount++)
                {
                    lstSelectedVendor.Items.Add(new ListItem(lstVendorTemplate[iCount].Vendor.Vendor_Name, lstVendorTemplate[iCount].Vendor.VendorID.ToString()));
                    msVendor.SelectedVendorIDs = lstVendorTemplate[iCount].Vendor.VendorID.ToString();
                    msVendor.SelectedVendorName = lstVendorTemplate[iCount].Vendor.Vendor_Name.ToString();
                }
            }
        }
        else
        {
            lstSelectedVendor.Items.Clear();
            msVendor.innerControlHiddenField.Value = string.Empty;
            msVendor.innerControlHiddenField2.Value = string.Empty;
        }
    }

    protected void btnGenerateReport_Click(object sender, EventArgs e)
    {

        oReportRequestBE.Action = "InsertReportRequest";
        oReportRequestBE.ModuleName = "OTIF";
        oReportRequestBE.ReportName = "OTIF Report";
        /*if (rdoOldMonthlyOTIF.Checked == true)  // For Monthly Old Measure
        {
            oReportRequestBE.ReportAction = "OTIF_ODLineLevelMonthlyReport";
            oReportRequestBE.ReportNamePath = "ODLineLevelMonthlyReportNew.rdlc";
            oReportRequestBE.ReportDatatableName = "dtODLineLevelMonthlyReportNew";
            oReportRequestBE.ReportType = "Monthly";
        }
        if (rdoNewMonthlyOTIF.Checked == true)  // For Monthly New Measure
        {
            oReportRequestBE.ReportAction = "OTIF_ODLineLevelMonthlyReportNewMesaure";
            oReportRequestBE.ReportNamePath = "ODLineLevelMonthlyReportNew.rdlc";
            oReportRequestBE.ReportDatatableName = "dtODLineLevelMonthlyReportNew";
            oReportRequestBE.ReportType = "Monthly";
        }*/

        if (rdoMonthly.Checked == true)  // For Monthly
        {
            if (rdoNew.Checked)
            {
                oReportRequestBE.ReportAction = "OTIF_ODLineLevelMonthlyReportNewMesaure";
                oReportRequestBE.ReportNamePath = "ODLineLevelMonthlyReportNew.rdlc";
                oReportRequestBE.ReportDatatableName = "dtODLineLevelMonthlyReportNew";
                oReportRequestBE.ReportType = "Monthly";
            }
            else if (rdoOld.Checked)
            {
                oReportRequestBE.ReportAction = "OTIF_ODLineLevelMonthlyReport";
                oReportRequestBE.ReportNamePath = "ODLineLevelMonthlyReportNew.rdlc";
                oReportRequestBE.ReportDatatableName = "dtODLineLevelMonthlyReportNew";
                oReportRequestBE.ReportType = "Monthly";
            }
            else
            {
                oReportRequestBE.ReportAction = "OTIF_ODLineLevelMonthlyReportAbsolute";
                oReportRequestBE.ReportNamePath = "ODLineLevelMonthlyReportNew.rdlc";
                oReportRequestBE.ReportDatatableName = "dtODLineLevelMonthlyReportNew";
                oReportRequestBE.ReportType = "Monthly";
            }
        }

        if (rdoSummary.Checked == true) // For Summary
        {
            //oReportRequestBE.ReportAction = "OTIF_ODLineLevelSummaryReport";
            //oReportRequestBE.ReportNamePath = "ODLineLevelSummaryReport.rdlc";
            //oReportRequestBE.ReportDatatableName = "dtODLineLevelSummaryReportNew";
            //oReportRequestBE.ReportType = "Summary";


            if (rdoNew.Checked)
            {
                oReportRequestBE.ReportAction = "OTIF_ODLineLevelSummaryReportNew";
                oReportRequestBE.ReportNamePath = "ODLineLevelSummaryReport.rdlc";
                oReportRequestBE.ReportDatatableName = "dtODLineLevelSummaryReportNew";
                oReportRequestBE.ReportType = "Summary";
            }
            else if (rdoOld.Checked)
            {
                oReportRequestBE.ReportAction = "OTIF_ODLineLevelSummaryReport";
                oReportRequestBE.ReportNamePath = "ODLineLevelSummaryReport.rdlc";
                oReportRequestBE.ReportDatatableName = "dtODLineLevelSummaryReportNew";
                oReportRequestBE.ReportType = "Summary";
            }
            else
            {
                oReportRequestBE.ReportAction = "OTIF_ODLineLevelSummaryReportAbsolute";
                oReportRequestBE.ReportNamePath = "ODLineLevelSummaryReport.rdlc";
                oReportRequestBE.ReportDatatableName = "dtODLineLevelSummaryReportNew";
                oReportRequestBE.ReportType = "Summary";
            }

        }
        if (rdoDetail.Checked == true) // For Detail
        {
            if (Session["Role"].ToString().ToLower() == "vendor")
            {
                oReportRequestBE.ReportAction = "OTIF_VendorLineLevelDetailedReport";
                oReportRequestBE.ReportNamePath = "VendorLineLevelDetailedReport.rdlc";
                oReportRequestBE.ReportDatatableName = "dtVendorLineLevelDetailedReport";

            }
            else
            {
                oReportRequestBE.ReportAction = "OTIF_ODLineLevelDetailedReport";
                if (rdoVendorView.Checked == true)
                    oReportRequestBE.ReportNamePath = "ODLineLevelDetailedReport.rdlc";
                else if (rdoStockPlannerViewOtif.Checked == true)
                    oReportRequestBE.ReportNamePath = "ODLineLevelDetailedReportSimple.rdlc";
                oReportRequestBE.ReportDatatableName = "dtODLineLevelDetailedReportNew";
            }
            oReportRequestBE.ReportType = "Detail";
        }

        oReportRequestBE.CountryIDs = msCountry.SelectedCountryIDs;
        oReportRequestBE.CountryName = msCountry.SelectedCountryName;
        oReportRequestBE.SiteIDs = msSite.SelectedSiteIDs;
        oReportRequestBE.SiteName = msSite.SelectedSiteName;
        oReportRequestBE.VendorIDs = msVendor.SelectedVendorIDs;
        oReportRequestBE.VendorName = msVendor.SelectedVendorName;
        oReportRequestBE.ItemClassification = msItemClassification.selectedItemClassification;
        oReportRequestBE.OfficeDepotSKU = msSKU.SelectedSKUName;
        oReportRequestBE.PurchaseOrder = msPO.SelectedPO;
        oReportRequestBE.StockPlannerGroupingIDs = multiSelectStockPlannerGrouping.SelectedStockPlannerGroupingIDs;
        oReportRequestBE.CustomerIDs = msCustomer.SelectedCustomerIDs;

        if (!string.IsNullOrEmpty(msStockPlanner.SelectedStockPlannerUserIDs)) {
            oReportRequestBE.StockPlannerIDs = msStockPlanner.SelectedStockPlannerIDs;
            oReportRequestBE.StockPlannerName = msStockPlanner.SelectedSPName;
        }

        oReportRequestBE.SelectedRMSCategoryIDs = MultiSelectRMSCategory1.SelectedRMSCategoryIDs;
        oReportRequestBE.SkugroupingID = multiSelectSKUGrouping.SelectedSkuGroupingIDs;
        if (rdoMonthly.Checked)
        {
            oReportRequestBE.DateFrom = string.IsNullOrEmpty(ddlYear.SelectedValue.ToString()) ? (DateTime?)null : Common.GetMM_DD_YYYY("01/01/" + ddlYear.SelectedValue.ToString());
            //DateTime now = DateTime.Now;
            //string sDateTo = DateTime.Now.AddDays(-now.Day).ToString("dd/MM/yyyy");
            //oReportRequestBE.DateTo = Common.GetMM_DD_YYYY(sDateTo);
            oReportRequestBE.DateTo = string.IsNullOrEmpty(ddlYear.SelectedValue.ToString()) ? (DateTime?)null : Common.GetMM_DD_YYYY("31/12/" + ddlYear.SelectedValue.ToString());
        }
        else
        {
            oReportRequestBE.DateFrom = string.IsNullOrEmpty(txtFromDate.Text) ? (DateTime?)null : Common.GetMM_DD_YYYY(hdnJSFromDt.Value);
            oReportRequestBE.DateTo = string.IsNullOrEmpty(txtToDate.Text) ? (DateTime?)null : Common.GetMM_DD_YYYY(hdnJSToDt.Value);
        }
        if (chkPODue.Checked)
            oReportRequestBE.PurchaseOrderDue = 1;
        else
            oReportRequestBE.PurchaseOrderDue = 0;

        if (chkPOReceived.Checked)
            oReportRequestBE.PurchaseOrderReceived = 1;
        else
            oReportRequestBE.PurchaseOrderReceived = 0;

        oReportRequestBE.RequestStatus = "Pending";
        oReportRequestBE.UserID = Convert.ToInt32(Session["UserID"]);
        oReportRequestBE.RequestTime = DateTime.Now;

        ReportRequest = oReportRequestBAL.addReportRequestBAL(oReportRequestBE);

        //clean selected data//
        msVendor.ClearSearch();
        msStockPlanner.clearsearch();

        if (!string.IsNullOrEmpty(oReportRequestBE.StockPlannerName))
            if (oReportRequestBE.StockPlannerName == Convert.ToString(Session["UserName"]))
                msStockPlanner.PreSelectedStockPlanner();


        if (ReportRequest > 0)
        {
            string ReportRequestSubmitted = WebCommon.getGlobalResourceValue("ReportRequestSubmitted");
            ReportRequestSubmitted = ReportRequestSubmitted + " : " + Convert.ToString(ReportRequest);
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + ReportRequestSubmitted + "')", true);
            //Response.Redirect("OTIFReport.aspx");
            return;
        }



        //---------------------------------------





        //OTIFReportBE oOTIFReportBE = new OTIFReportBE();
        //OTIFReportBAL oOTIFReportBAL = new OTIFReportBAL();
        //string sDateFrom = string.Empty;
        //string sDateTo = string.Empty;


        //oOTIFReportBE.SelectedCountryIDs = msCountry.SelectedCountryIDs;
        //oOTIFReportBE.SelectedSiteIDs = msSite.SelectedSiteIDs;
        //oOTIFReportBE.SelectedStockPlannerIDs = msStockPlanner.SelectedStockPlannerIDs;
        //oOTIFReportBE.SelectedVendorIDs = msVendor.SelectedVendorIDs;
        //oOTIFReportBE.SelectedSKUCodes = msSKU.SelectedSKUName;
        //oOTIFReportBE.SelectedPurchaseOrderNo = msPO.SelectedPO;
        //oOTIFReportBE.SelectedItemClassification = msItemClassification.selectedItemClassification;
        //if (rdoMonthly.Checked) {

        //    oOTIFReportBE.DateFrom = string.IsNullOrEmpty(ddlYear.SelectedValue.ToString()) ? (DateTime?)null : Common.GetMM_DD_YYYY("01/01/" + ddlYear.SelectedValue.ToString());
        //    DateTime now = DateTime.Now;
        //    sDateTo = DateTime.Now.AddDays(-now.Day).ToString("dd/MM/yyyy");
        //    oOTIFReportBE.DateTo = Common.GetMM_DD_YYYY(sDateTo);

        //    sDateFrom = "01/01/" + ddlYear.SelectedValue.ToString();
        //}
        //else {
        //    oOTIFReportBE.DateFrom = string.IsNullOrEmpty(txtFromDate.Text) ? (DateTime?)null : Common.GetMM_DD_YYYY(hdnJSFromDt.Value);
        //    oOTIFReportBE.DateTo = string.IsNullOrEmpty(txtToDate.Text) ? (DateTime?)null : Common.GetMM_DD_YYYY(hdnJSToDt.Value);

        //    sDateFrom = hdnJSFromDt.Value;
        //    sDateTo = hdnJSToDt.Value;

        //}
        //if (chkPODue.Checked)
        //    oOTIFReportBE.PurchaseOrderDue = 1;
        //else
        //    oOTIFReportBE.PurchaseOrderDue = 0;

        //if (chkPOReceived.Checked)
        //    oOTIFReportBE.PurchaseOrderReceived = 1;
        //else
        //    oOTIFReportBE.PurchaseOrderReceived = 0;

        //if (rdoSummary.Checked) {
        //    oOTIFReportBE.Action = "OTIF_ODLineLevelSummaryReport";
        //    Session["ReportPath"] = "\\ModuleUI\\OnTimeInFull\\Reports\\RDLC\\ODLineLevelSummaryReport.rdlc";
        //    Session["DateTableName"] = "dtODLineLevelSummaryReportNew";
        //    reportFileName = "OTIF_ODLineLevelSummaryReport";

        //    DataSet dsOTIFReport = oOTIFReportBAL.GetOTIFReportBAL(oOTIFReportBE);
        //    oOTIFReportBAL = null;

        //    ReportDataSource rdsOTIFReport = new ReportDataSource(Session["DateTableName"].ToString(), dsOTIFReport.Tables[0]);
        //    ReportViewer1.LocalReport.ReportPath = Server.MapPath("~") + Session["ReportPath"].ToString();

        //    ReportParameter[] reportParameter = new ReportParameter[9];
        //    reportParameter[0] = new ReportParameter("Country", msCountry.SelectedCountryName);
        //    reportParameter[1] = new ReportParameter("Site", msSite.SelectedSiteName);
        //    reportParameter[2] = new ReportParameter("Vendor", msVendor.SelectedVendorName);
        //    reportParameter[3] = new ReportParameter("DateFrom", sDateFrom);
        //    reportParameter[4] = new ReportParameter("DateTo", sDateTo);
        //    reportParameter[5] = new ReportParameter("ItemClassification", msItemClassification.selectedItemClassification);
        //    reportParameter[6] = new ReportParameter("ODSKUCode", msSKU.SelectedSKUName);
        //    reportParameter[7] = new ReportParameter("PurchaseOrder", msPO.SelectedPO);
        //    reportParameter[8] = new ReportParameter("StockPlanner", msStockPlanner.SelectedSPName);


        //    ReportViewer1.LocalReport.SetParameters(reportParameter);

        //    ReportViewer1.LocalReport.DataSources.Clear();
        //    ReportViewer1.LocalReport.DataSources.Add(rdsOTIFReport);

        //    byte[] bytes = ReportViewer1.LocalReport.Render("Excel", null, out mimeType, out encoding, out extension, out streamIds, out warnings);
        //    Response.Buffer = true;
        //    Response.Clear();
        //    Response.ContentType = mimeType;
        //    Response.AddHeader("content-disposition", "attachment; filename = " + reportFileName + "." + extension);
        //    Response.BinaryWrite(bytes);
        //    Response.Flush();
        //}
        //else if (rdoDetail.Checked) {
        //    if (Session["Role"].ToString().ToLower() == "vendor") {
        //        oOTIFReportBE.Action = "OTIF_VendorLineLevelDetailedReport";
        //        // Session["ReportPath"] = "\\ModuleUI\\OnTimeInFull\\Reports\\RDLC\\VendorLineLevelDetailedReport.rdlc";
        //        //  Session["DateTableName"] = "dtVendorLineLevelDetailedReport";
        //        reportFileName = "OTIF_VendorLineLevelDetailedReport";

        //        /*********************************** Server Report********************************/
        //        <add key="ReportServerPath" value="http://cons-119a:8080/ReportServer_SQLEXPRESS" />

        //        ReportViewer1.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Remote;
        //        ReportViewer1.ServerReport.Timeout = 4200000;
        //        ReportViewer1.ServerReport.ReportServerUrl = new Uri(Convert.ToString(ConfigurationManager.AppSettings["ReportServerPath"])); // Report Server URL


        //        ReportViewer1.ServerReport.ReportPath = "/OfficeDepotReports/ODLineLevelDetailedReport"; // Report Name
        //        //ReportViewer1.ShowParameterPrompts = false;
        //        sDateFrom = Common.GetYYYY_MM_DD(sDateFrom);
        //        sDateTo = Common.GetYYYY_MM_DD(sDateTo);
        //        ReportParameter[] Param = new ReportParameter[18];
        //        Param[0] = new ReportParameter("Country", msCountry.SelectedCountryName);
        //        Param[1] = new ReportParameter("Site", msSite.SelectedSiteName);
        //        Param[2] = new ReportParameter("Vendor", msVendor.SelectedVendorName);
        //        Param[3] = new ReportParameter("DateFrom", sDateFrom);
        //        Param[4] = new ReportParameter("DateTo", sDateTo);
        //        Param[5] = new ReportParameter("ItemClassification", msItemClassification.selectedItemClassification);
        //        Param[6] = new ReportParameter("ODSKUCode", msSKU.SelectedSKUName);
        //        Param[7] = new ReportParameter("PurchaseOrder", msPO.SelectedPO);
        //        Param[8] = new ReportParameter("StockPlanner", msStockPlanner.SelectedSPName);
        //        Param[9] = new ReportParameter("SelectedCountryIDs", msCountry.SelectedCountryIDs);
        //        Param[10] = new ReportParameter("SelectedSiteIDs", msSite.SelectedSiteIDs);
        //        Param[11] = new ReportParameter("SelectedStockPlannerIDs", msStockPlanner.SelectedStockPlannerIDs);
        //        Param[12] = new ReportParameter("SelectedVendorIDs", msVendor.SelectedVendorIDs);
        //        Param[13] = new ReportParameter("OfficeDepotSKU", msSKU.SelectedSKUName);
        //        Param[14] = new ReportParameter("SelectedPurchaseOrders", msPO.SelectedPO);
        //        Param[15] = new ReportParameter("SelectedItemClassification", msItemClassification.selectedItemClassification);
        //        Param[16] = new ReportParameter("PurchaseOrderDue", Convert.ToString(oOTIFReportBE.PurchaseOrderDue));
        //        Param[17] = new ReportParameter("PurchaseOrderReceived", Convert.ToString(oOTIFReportBE.PurchaseOrderReceived));

        //        ReportViewer1.ServerReport.SetParameters(Param);

        //        try {
        //            byte[] bytes = ReportViewer1.ServerReport.Render("Excel", null, out mimeType, out encoding, out extension, out streamIds, out warnings); Response.Buffer = true;
        //            Response.Clear();
        //            Response.ContentType = mimeType;
        //            Response.AddHeader("content-disposition", "attachment; filename = " + reportFileName + "." + extension);
        //            Response.BinaryWrite(bytes);
        //            Response.Flush();
        //        }
        //        catch (Exception ex) {
        //            LogUtility.SaveErrorLogEntry(ex);
        //        }

        //    }
        //    else {
        //        oOTIFReportBE.Action = "OTIF_ODLineLevelDetailedReport";
        //        // Session["ReportPath"] = "\\ModuleUI\\OnTimeInFull\\Reports\\RDLC\\ODLineLevelDetailedReport.rdlc";
        //        // Session["DateTableName"] = "dtODLineLevelDetailedReportNew";
        //        reportFileName = "OTIF_ODLineLevelDetailedReport";

        //        /*********************************** Server Report********************************/

        //        ReportViewer1.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Remote;
        //        ReportViewer1.ServerReport.Timeout = 4200000;
        //        ReportViewer1.ServerReport.ReportServerUrl = new Uri(Convert.ToString(ConfigurationManager.AppSettings["ReportServerPath"])); // Report Server URL
        //        ReportViewer1.ServerReport.ReportPath = "/OfficeDepotReports/ODLineLevelDetailedReport"; // Report Name
        //        //ReportViewer1.ShowParameterPrompts = false;
        //        sDateFrom = Common.GetYYYY_MM_DD(sDateFrom);
        //        sDateTo = Common.GetYYYY_MM_DD(sDateTo);
        //        ReportParameter[] Param = new ReportParameter[18];
        //        Param[0] = new ReportParameter("Country", msCountry.SelectedCountryName);
        //        Param[1] = new ReportParameter("Site", msSite.SelectedSiteName);
        //        Param[2] = new ReportParameter("Vendor", msVendor.SelectedVendorName);
        //        Param[3] = new ReportParameter("DateFrom", sDateFrom);
        //        Param[4] = new ReportParameter("DateTo", sDateTo);
        //        Param[5] = new ReportParameter("ItemClassification", msItemClassification.selectedItemClassification);
        //        Param[6] = new ReportParameter("ODSKUCode", msSKU.SelectedSKUName);
        //        Param[7] = new ReportParameter("PurchaseOrder", msPO.SelectedPO);
        //        Param[8] = new ReportParameter("StockPlanner", msStockPlanner.SelectedSPName);
        //        Param[9] = new ReportParameter("SelectedCountryIDs", msCountry.SelectedCountryIDs);
        //        Param[10] = new ReportParameter("SelectedSiteIDs", msSite.SelectedSiteIDs);
        //        Param[11] = new ReportParameter("SelectedStockPlannerIDs", msStockPlanner.SelectedStockPlannerIDs);
        //        Param[12] = new ReportParameter("SelectedVendorIDs", msVendor.SelectedVendorIDs);
        //        Param[13] = new ReportParameter("OfficeDepotSKU", msSKU.SelectedSKUName);
        //        Param[14] = new ReportParameter("SelectedPurchaseOrders", msPO.SelectedPO);
        //        Param[15] = new ReportParameter("SelectedItemClassification", msItemClassification.selectedItemClassification);
        //        Param[16] = new ReportParameter("PurchaseOrderDue", Convert.ToString(oOTIFReportBE.PurchaseOrderDue));
        //        Param[17] = new ReportParameter("PurchaseOrderReceived", Convert.ToString(oOTIFReportBE.PurchaseOrderReceived));

        //        ReportViewer1.ShowCredentialPrompts = false;
        //        ReportViewer1.ServerReport.ReportServerCredentials = new OTIFReportServerCredentials("54.247.171.29", "manish-mathur", "7qR=ZP@sr7");


        //        ReportViewer1.ServerReport.SetParameters(Param);

        //        try {
        //            byte[] bytes = ReportViewer1.ServerReport.Render("Excel", null, out mimeType, out encoding, out extension, out streamIds, out warnings);
        //            Response.Buffer = true;
        //            Response.Clear();
        //            Response.ContentType = mimeType;
        //            Response.AddHeader("content-disposition", "attachment; filename = " + reportFileName + "." + extension);
        //            Response.BinaryWrite(bytes);
        //            Response.Flush();
        //        }
        //        catch (Exception ex) {
        //            LogUtility.SaveErrorLogEntry(ex);
        //        }

        //    }
        //}
        //else {
        //    oOTIFReportBE.Action = "OTIF_ODLineLevelMonthlyReport";
        //    Session["ReportPath"] = "\\ModuleUI\\OnTimeInFull\\Reports\\RDLC\\ODLineLevelMonthlyReportNew.rdlc";
        //    Session["DateTableName"] = "dtODLineLevelMonthlyReportNew";
        //    reportFileName = "OTIF_ODLineLevelMonthlyReport";

        //    DataSet dsOTIFReport = oOTIFReportBAL.GetOTIFReportBAL(oOTIFReportBE);
        //    oOTIFReportBAL = null;

        //    ReportDataSource rdsOTIFReport = new ReportDataSource(Session["DateTableName"].ToString(), dsOTIFReport.Tables[0]);
        //    ReportViewer1.LocalReport.ReportPath = Server.MapPath("~") + Session["ReportPath"].ToString();

        //    ReportParameter[] reportParameter = new ReportParameter[9];
        //    reportParameter[0] = new ReportParameter("Country", msCountry.SelectedCountryName);
        //    reportParameter[1] = new ReportParameter("Site", msSite.SelectedSiteName);
        //    reportParameter[2] = new ReportParameter("Vendor", msVendor.SelectedVendorName);
        //    reportParameter[3] = new ReportParameter("DateFrom", sDateFrom);
        //    reportParameter[4] = new ReportParameter("DateTo", sDateTo);
        //    reportParameter[5] = new ReportParameter("ItemClassification", msItemClassification.selectedItemClassification);
        //    reportParameter[6] = new ReportParameter("ODSKUCode", msSKU.SelectedSKUName);
        //    reportParameter[7] = new ReportParameter("PurchaseOrder", msPO.SelectedPO);
        //    reportParameter[8] = new ReportParameter("StockPlanner", msStockPlanner.SelectedSPName);


        //    ReportViewer1.LocalReport.SetParameters(reportParameter);

        //    ReportViewer1.LocalReport.DataSources.Clear();
        //    ReportViewer1.LocalReport.DataSources.Add(rdsOTIFReport);

        //    byte[] bytes = ReportViewer1.LocalReport.Render("Excel", null, out mimeType, out encoding, out extension, out streamIds, out warnings);
        //    Response.Buffer = true;
        //    Response.Clear();
        //    Response.ContentType = mimeType;
        //    Response.AddHeader("content-disposition", "attachment; filename = " + reportFileName + "." + extension);
        //    Response.BinaryWrite(bytes);
        //    Response.Flush();

        //}

    }



}

//[Serializable]
//public sealed class OTIFReportServerCredentials :
//    IReportServerCredentials {

//    private string _userName;
//    private string _password;
//    private string _domain;


//    public OTIFReportServerCredentials(string domain, string user, string pwd) {
//        _userName = user;
//        _password = pwd;
//        _domain = domain;
//    }
//    public WindowsIdentity ImpersonationUser {
//        get {
//            // Use the default Windows user.  Credentials will be
//            // provided by the NetworkCredentials property.
//            return null;
//        }
//    }

//    public ICredentials NetworkCredentials {
//        get {
//            // Use default identity.
//            return new NetworkCredential(_userName, _password, _domain);
//        }

//    }

//    public bool GetFormsCredentials(out Cookie authCookie,
//                out string userName, out string password,
//                out string authority) {
//        authCookie = null;
//        userName = null;
//        password = null;
//        authority = null;

//        // Not using form credentials
//        return false;
//    }
//}

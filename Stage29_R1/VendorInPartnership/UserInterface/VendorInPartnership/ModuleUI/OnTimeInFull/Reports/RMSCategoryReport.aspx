﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master" AutoEventWireup="true" CodeFile="RMSCategoryReport.aspx.cs" Inherits="RMSCategoryReport" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectCountry.ascx" TagName="MultiSelectCountry"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectSite.ascx" TagName="MultiSelectSite"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectSKU.ascx" TagName="MultiSelectSKU"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectPO.ascx" TagName="MultiSelectPO"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectVendor.ascx" TagName="MultiSelectVendor"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectStockPlanner.ascx" TagName="MultiSelectStockPlanner"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectItemClassification.ascx" TagName="MultiSelectItemClassification"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/ucVendorSelectionTemplate.ascx" TagName="VendorSelectTemplate"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectRMSCategory.ascx" TagName="MultiSelectRMSCategory"
    TagPrefix="cc1" %>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <h2>
        <cc1:ucLabel ID="lblRMSCategoryReport" runat="server" Text=""></cc1:ucLabel>
    </h2>
    <script type="text/javascript" >
        function setValue1(target) {
            document.getElementById('<%=hdnJSFromDt.ClientID %>').value = target.value;
        }

        function setValue2(target) {
            document.getElementById('<%=hdnJSToDt.ClientID %>').value = target.value;
        }

        function checkFlag() {
            var datePanel = document.getElementById('UcPanelDate');
            var yearPanel = document.getElementById('UcPanelYear');
          
            var rdoSummary = document.getElementById('<%=rdoSummary.ClientID %>');
            var rdoMonthly = document.getElementById('<%=rdoMonthly.ClientID %>');
            //var rdoNewMonthlyOTIF = document.getElementById('=rdoNewMonthlyOTIF.ClientID ');
            var lblDateRange = document.getElementById('<%=lblDateFrom.ClientID %>');
            if (rdoMonthly.checked) {
                yearPanel.style.display = "block";
                datePanel.style.display = "none";
                lblDateRange.innerText = "Select Year";
            } else {
                yearPanel.style.display = "none";
                datePanel.style.display = "block";
                lblDateRange.innerText = "Date From";
            }
        }
        $(document).ready(function () {
            HideShowReportView();
            $('#txtToDate,#txtFromDate').datepicker({
                showOtherMonths: true,
                selectOtherMonths: true, changeMonth: true, changeYear: true,
                minDate: new Date(2013, 0, 1),
                yearRange: '2013:+100',
                dateFormat: 'dd/mm/yy', maxDate: $('#txtToDate').val()
            });

            $("[id$='rdoDetail']").click(function () {
                HideShowReportView();
            });
            $("[id$='rdoSummary']").click(function () {
                HideShowReportView();
            });
            $("[id$='rdoMonthly']").click(function () {
                HideShowReportView();
            });
        });
        function HideShowReportView() {
            //if ($("[id$='rdoDetail']").is(':checked')) {
            //    $('#tdReportView').hide();
            //}
            if ($("[id$='rdoSummary']").is(':checked')) {
                $('#tdReportView').show();
            }
            if ($("[id$='rdoMonthly']").is(':checked')) {
                $('#tdReportView').show();
            }
        }

        $(document).ready(function () {
            $("body").keypress(function (e) {
                if (e.which == 13 || e.keyCode == 13) {
                    $('#<%=btnGenerateReport.ClientID %>').click();
                    return false;
                }
            });
        });
    </script>
    <asp:ScriptManager ID="SM1" runat="server" AsyncPostBackTimeout="2400">
    </asp:ScriptManager>
    <asp:HiddenField ID="hdnJSFromDt" runat="server" />
    <asp:HiddenField ID="hdnJSToDt" runat="server" />
    <div class="right-shadow">
        <div class="formbox">
            <cc1:ucPanel ID="UcDataPanel" runat="server">
                <table width="95%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                    <tr>
                        <td align="right">
                            <cc1:VendorSelectTemplate runat="server" ID="ucVendorTemplateSelect" />
                        </td>
                    </tr>
                </table>
                <table width="95%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                    <tr>
                        <td style="font-weight: bold; width: 10%">
                            <cc1:ucLabel ID="lblCountry" runat="server" Text="Country"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold; width: 1%">
                            <cc1:ucLabel ID="UcLabel5" runat="server">:</cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold; width: 89%">
                            <cc1:MultiSelectCountry runat="server" ID="msCountry" />
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblSite" runat="server" Text="Site"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="UcLabel4" runat="server">:</cc1:ucLabel>
                        </td>
                        <td>
                            <cc1:MultiSelectSite runat="server" ID="msSite" />
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblStockPlanner" runat="server" Text="Stock Planner"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="Label17" runat="server">:</cc1:ucLabel>
                        </td>
                        <td>
                            <cc1:MultiSelectStockPlanner runat="server" ID="msStockPlanner" />
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblCategory" runat="server"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            :
                        </td>
                        <td>
                            <cc1:MultiSelectRMSCategory runat="server" ID="MultiSelectRMSCategory1" />
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblVendor" runat="server" Text="Vendor"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="UcLabel2" runat="server">:</cc1:ucLabel>
                        </td>
                        <td>
                            <cc1:MultiSelectVendor runat="server" ID="msVendor" />
                        </td>
                    </tr>
                   
                    <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblItemClassification" runat="server" Text="Item Classification"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="UcLabel10" runat="server">:</cc1:ucLabel>
                        </td>
                        <td>
                            <cc1:MultiSelectItemClassification runat="server" ID="msItemClassification" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <table width="100%">
                                <tr style="height: 3em;">
                                    <td width="10%" style="font-weight: bold;">
                                        <cc1:ucLabel ID="lblReportType" runat="server" Text="Report Type"></cc1:ucLabel>
                                    </td>
                                    <td width="1%" style="font-weight: bold;">
                                        <cc1:ucLabel ID="UcLabel6" runat="server">:</cc1:ucLabel>
                                    </td>
                                    <td width="89%">
                                        <table cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style="font-weight: bold;" class="checkbox-list">
                                                    <cc1:ucRadioButton ID="rdoSummary" runat="server" Text="Summary" GroupName="ReportType"
                                                        Checked="true" onClick="checkFlag()" />
                                                </td>
                                                <td style="font-weight: bold;" class="checkbox-list">
                                                    <cc1:ucRadioButton ID="rdoMonthly" runat="server" Text="Monthly" GroupName="ReportType"
                                                        onClick="checkFlag()" />
                                                </td>
                                                <%--   <td style="font-weight: bold;" class="checkbox-list">
                                                    <cc1:ucRadioButton ID="rdoNewMonthlyOTIF" runat="server" Text="New Monthly OTIF"
                                                        GroupName="ReportType" onClick="checkFlag()" />
                                                </td>--%>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr id="tdReportView">
                                    <td colspan="3">
                                        <table width="100%">
                                            <tr style="height: 3em;">
                                                <td width="10%" style="font-weight: bold;">
                                                    <cc1:ucLabel ID="lblReportView" runat="server" Text="Report View"></cc1:ucLabel>
                                                </td>
                                                <td width="1%" style="font-weight: bold;">
                                                    <cc1:ucLabel ID="UcLabel7" runat="server">:</cc1:ucLabel>
                                                </td>
                                                <td width="89%">
                                                    <table cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <td style="font-weight: bold;" class="checkbox-list">
                                                                <cc1:ucRadioButton ID="rdoNew" runat="server" Text="New" GroupName="ReportView" Checked="true" />
                                                            </td>
                                                            <td style="font-weight: bold;" class="checkbox-list">
                                                                <cc1:ucRadioButton ID="rdoOld" runat="server" Text="Old" GroupName="ReportView" />
                                                            </td>
                                                            <td style="font-weight: bold;" class="checkbox-list">
                                                                <cc1:ucRadioButton ID="rdoAbsolute" runat="server" Text="Absolute" GroupName="ReportView" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr style="height: 3em;">
                                    <td style="font-weight: bold;">
                                        <cc1:ucLabel ID="lblDateFrom" runat="server" Text="Date From"></cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold;">
                                        <cc1:ucLabel ID="UcLabel1" runat="server">:</cc1:ucLabel>
                                    </td>
                                    <td>
                                        <div id="UcPanelDate">
                                            <table cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td style="font-weight: bold; width: 6em;">
                                                        <cc1:ucTextbox ID="txtFromDate" ClientIDMode="Static" runat="server"  onChange="setValue1(this)"
                                                            ReadOnly="True" Width="70px" />
                                                    </td>
                                                    <td style="font-weight: bold; width: 4em; text-align: center">
                                                        <cc1:ucLabel ID="lblTo" runat="server" Text="To"></cc1:ucLabel>
                                                    </td>
                                                    <td style="font-weight: bold; width: 6em;">
                                                        <cc1:ucTextbox ID="txtToDate" ClientIDMode="Static" runat="server" onchange="setValue2(this)"
                                                            ReadOnly="True" Width="70px" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div id="UcPanelYear" style="display: none">
                                            <table cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td colspan="2" style="font-weight: bold; width: 6em;">
                                                        <cc1:ucDropdownList ID="ddlYear" runat="server" Width="60px">
                                                        </cc1:ucDropdownList>
                                                    </td>
                                                </tr>
                                            </table>
                                            <script>
                                                checkFlag();
                                            </script>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr style="height: 3em;">
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                            <table cellpadding="0" cellspacing="0" class="form-table">
                                <tr>
                                    <td style="font-weight: bold; width: 50%;" class="checkbox-list">
                                        <cc1:ucRadioButton ID="chkPODue" GroupName="gpDue" runat="server" Text="Purchase Order Due"
                                            Checked="true" />
                                    </td>
                                    <td style="font-weight: bold; width: 50%;" class="checkbox-list">
                                        <cc1:ucRadioButton ID="chkPOReceived" GroupName="gpDue" runat="server" Text="Purchase Order Received" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="right" colspan="3">
                            <div class="button-row">
                                <cc1:ucButton ID="btnGenerateReport" runat="server" Text="Generate Report" OnClick="btnGenerateReport_Click" CssClass="button"
                                   />
                            </div>
                        </td>
                    </tr>
                </table>
            </cc1:ucPanel>
            <cc1:ucPanel ID="UcReportPanel" runat="server" Visible="false">
                <table style="width: 100%;">
                    <tr>
                        <td>
                            <%--<rsweb:ReportViewer ID="ReportViewer1" runat="server" Height="550px" Width="950px"
                                DocumentMapCollapsed="True" Font-Names="Verdana" Font-Size="8pt" InteractiveDeviceInfos="(Collection)"
                                WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt">
                            </rsweb:ReportViewer>--%>
                        </td>
                    </tr>
                </table>
            </cc1:ucPanel>
        </div>
    </div>
</asp:Content>


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.ReportRequest;
using BusinessEntities.ModuleBE.Security;
using BusinessLogicLayer.ModuleBAL.ReportRequest;
using BusinessLogicLayer.ModuleBAL.Security;
using Microsoft.Reporting.WebForms;
using Utilities;
using WebUtilities;

public partial class RMSCategoryReport : CommonPage
{
    int? ReportRequest = 0;
    ReportRequestBE oReportRequestBE = new ReportRequestBE();
    ReportRequestBAL oReportRequestBAL = new ReportRequestBAL();
    protected void Page_Init(object sender, EventArgs e)
    {
        ucVendorTemplateSelect.CurrentPage = this;
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        { 
            txtFromDate.Text = DateTime.Now.AddDays(-DateTime.Now.Day + 1).ToString("dd/MM/yyyy");
            txtToDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
            hdnJSFromDt.Value = txtFromDate.Text;
            hdnJSToDt.Value = txtToDate.Text;
            int YearFrom = DateTime.Now.Year;
            int YearTo = DateTime.Now.Year - 10;
            while (YearFrom > YearTo)
            {
                if (YearFrom >= 2013)
                    ddlYear.Items.Add(YearFrom.ToString());
                YearFrom--;
            }
        }
    }
    /// <summary>
    /// Method to show Vendor on the basis of selected Template
    /// </summary>
    public override void TemplateSelectedIndexChanged()
    {
        base.TemplateSelectedIndexChanged();

        int VendorTemplateId = Convert.ToInt32(ucVendorTemplateSelect.innerControlddlTemplate.SelectedValue);

        ListBox lstSelectedVendor = (ListBox)msVendor.FindControl("lstRight");
        ListBox lstVendor = (ListBox)msVendor.FindControl("ucVendor").FindControl("lstLeft");

        if (VendorTemplateId > 0)
        {

            SCT_TemplateBAL oSCT_TemplateBAL = new SCT_TemplateBAL();
            SCT_TemplateBE oSCT_TemplateBE = new SCT_TemplateBE();

            oSCT_TemplateBE.VendorTemplateID = VendorTemplateId;
            oSCT_TemplateBE.Action = "GetVendorTemplateInfobyID";
            List<SCT_TemplateBE> lstVendorTemplate = oSCT_TemplateBAL.GetVendorTemplateById(oSCT_TemplateBE);

            if (lstVendorTemplate != null && lstVendorTemplate.Count > 0)
            {
                lstSelectedVendor.Items.Clear();
                msVendor.innerControlHiddenField.Value = string.Empty;
                msVendor.innerControlHiddenField2.Value = string.Empty;
                for (int iCount = 0; iCount < lstVendorTemplate.Count; iCount++)
                {
                    lstSelectedVendor.Items.Add(new ListItem(lstVendorTemplate[iCount].Vendor.Vendor_Name, lstVendorTemplate[iCount].Vendor.VendorID.ToString()));
                    msVendor.SelectedVendorIDs = lstVendorTemplate[iCount].Vendor.VendorID.ToString();
                    msVendor.SelectedVendorName = lstVendorTemplate[iCount].Vendor.Vendor_Name.ToString();
                }
            }
        }
        else
        {
            lstSelectedVendor.Items.Clear();
            msVendor.innerControlHiddenField.Value = string.Empty;
            msVendor.innerControlHiddenField2.Value = string.Empty;
        }
    }
    protected void btnGenerateReport_Click(object sender, EventArgs e)
    {
        oReportRequestBE.Action = "InsertReportRequest";
        oReportRequestBE.ReportName = "RMS Category Report";
        oReportRequestBE.ModuleName = "RMSCategory";
        if (rdoMonthly.Checked == true)  // For Monthly
        {
            if (rdoNew.Checked)
            {
                oReportRequestBE.ReportAction = "RMS_ODMonthlyReportNewMesaure";
                oReportRequestBE.ReportNamePath = "RMSCategoryMonthlyReport.rdlc";
                oReportRequestBE.ReportDatatableName = "dtRMSCategoryMonthlyReport";
                oReportRequestBE.ReportType = "Monthly";
            }
            else if (rdoOld.Checked)
            {
                oReportRequestBE.ReportAction = "RMS_ODMonthlyReport";
                oReportRequestBE.ReportNamePath = "RMSCategoryMonthlyReport.rdlc";
                oReportRequestBE.ReportDatatableName = "dtRMSCategoryMonthlyReport";
                oReportRequestBE.ReportType = "Monthly";
            }
            else
            {
                oReportRequestBE.ReportAction = "RMS_ODMonthlyReportAbsolute";
                oReportRequestBE.ReportNamePath = "RMSCategoryMonthlyReport.rdlc";
                oReportRequestBE.ReportDatatableName = "dtRMSCategoryMonthlyReport";
                oReportRequestBE.ReportType = "Monthly";
            }
        }

        if (rdoSummary.Checked == true) // For Summary
        {
            if (rdoNew.Checked)
            {
                oReportRequestBE.ReportAction = "RMS_ODSummaryReportNew";
                oReportRequestBE.ReportNamePath = "RMSCategorySummaryReport.rdlc";
                oReportRequestBE.ReportDatatableName = "dtRMSCategorySummaryReport";
                oReportRequestBE.ReportType = "Summary";
            }
            else if (rdoOld.Checked)
            {
                oReportRequestBE.ReportAction = "RMS_ODSummaryReport";
                oReportRequestBE.ReportNamePath = "RMSCategorySummaryReport.rdlc";
                oReportRequestBE.ReportDatatableName = "dtRMSCategorySummaryReport";
                oReportRequestBE.ReportType = "Summary";
            }
            else
            {
                oReportRequestBE.ReportAction = "RMS_ODSummaryReportAbsolute";
                oReportRequestBE.ReportNamePath = "RMSCategorySummaryReport.rdlc";
                oReportRequestBE.ReportDatatableName = "dtRMSCategorySummaryReport";
                oReportRequestBE.ReportType = "Summary";
            }

        }
        oReportRequestBE.CountryIDs = msCountry.SelectedCountryIDs;
        oReportRequestBE.CountryName = msCountry.SelectedCountryName;
        oReportRequestBE.SiteIDs = msSite.SelectedSiteIDs;
        oReportRequestBE.SiteName = msSite.SelectedSiteName;
        oReportRequestBE.VendorIDs = msVendor.SelectedVendorIDs;
        oReportRequestBE.VendorName = msVendor.SelectedVendorName;
        oReportRequestBE.ItemClassification = msItemClassification.selectedItemClassification;
        oReportRequestBE.StockPlannerIDs = msStockPlanner.SelectedStockPlannerIDs;
        oReportRequestBE.StockPlannerName = msStockPlanner.SelectedSPName;
        oReportRequestBE.SelectedRMSCategoryIDs = MultiSelectRMSCategory1.SelectedRMSCategoryIDs;
        if (rdoMonthly.Checked)
        {
            oReportRequestBE.DateFrom = string.IsNullOrEmpty(ddlYear.SelectedValue.ToString()) ? (DateTime?)null : Common.GetMM_DD_YYYY("01/01/" + ddlYear.SelectedValue.ToString());
            string sDateTo = string.Empty;
            if (Convert.ToInt32(ddlYear.SelectedValue.ToString()) < DateTime.Now.Year)
            {
                DateTime todate;
                if (!string.IsNullOrEmpty(ddlYear.SelectedValue.ToString()))
                {
                    todate = Common.GetMM_DD_YYYY("01/01/" + ddlYear.SelectedValue.ToString());
                    todate = todate.AddMonths(12);
                    todate = todate.AddDays(-1);
                    sDateTo = todate.ToString();
                }
                oReportRequestBE.DateTo = Convert.ToDateTime(sDateTo);
            }
            else
            {
                DateTime now = DateTime.Now;
                sDateTo = DateTime.Now.AddDays(-now.Day).ToString("dd/MM/yyyy");
                oReportRequestBE.DateTo = Common.GetMM_DD_YYYY(sDateTo);
            }
        }
        else
        {
            oReportRequestBE.DateFrom = string.IsNullOrEmpty(txtFromDate.Text) ? (DateTime?)null : Common.GetMM_DD_YYYY(hdnJSFromDt.Value);
            oReportRequestBE.DateTo = string.IsNullOrEmpty(txtToDate.Text) ? (DateTime?)null : Common.GetMM_DD_YYYY(hdnJSToDt.Value);
        }
        if (chkPODue.Checked)
            oReportRequestBE.PurchaseOrderDue = 1;
        else
            oReportRequestBE.PurchaseOrderDue = 0;

        if (chkPOReceived.Checked)
            oReportRequestBE.PurchaseOrderReceived = 1;
        else
            oReportRequestBE.PurchaseOrderReceived = 0;

        oReportRequestBE.RequestStatus = "Pending";
        oReportRequestBE.UserID = Convert.ToInt32(Session["UserID"]);
        oReportRequestBE.RequestTime = DateTime.Now;

        ReportRequest = oReportRequestBAL.addReportRequestBAL(oReportRequestBE);
        if (ReportRequest > 0)
        {
            string ReportRequestSubmitted = WebCommon.getGlobalResourceValue("ReportRequestSubmitted");
            ReportRequestSubmitted = ReportRequestSubmitted + " : " + Convert.ToString(ReportRequest);
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + ReportRequestSubmitted + "')", true);
            return;
        }
    }
}
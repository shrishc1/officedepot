﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using BusinessLogicLayer.ModuleBAL.Appointment.SiteSettings;
using BusinessLogicLayer.ModuleBAL.OnTimeInFull.Report;
using BusinessEntities.ModuleBE.OnTimeInFull.Report;
using Microsoft.Reporting.WebForms;
using Utilities;
using WebUtilities;
using System.Configuration;
using System.IO;
using System.Globalization;
using System.Data;
using BusinessLogicLayer.ModuleBAL.Security;
using BusinessEntities.ModuleBE.Security;

public partial class StockPlannerOTIFReport : CommonPage
{
    #region ReportVariables
    Warning[] warnings;
    string[] streamIds;
    string mimeType = string.Empty;
    string encoding = string.Empty;
    string extension = string.Empty;
    string reportFileName = string.Empty;
    #endregion

    protected void Page_Init(object sender, EventArgs e)
    {
        ucVendorTemplateSelect.CurrentPage = this;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (mvSPOtifReport.ActiveViewIndex == 0) {
            btnGenerateReport.Visible = false;
        }
        if (!IsPostBack) {
           
            int YearFrom = DateTime.Now.Year;
            int YearTo = DateTime.Now.Year - 10;
            while (YearFrom > YearTo) {
                if (YearFrom >= 2013) {
                    ddlYear.Items.Add(YearFrom.ToString());
                    ddlYear2.Items.Add(YearFrom.ToString());
                }
                YearFrom--;
            }

           
            

        }
    }
    protected void btnDetailReport_Click(object sender, EventArgs e) {
        mvSPOtifReport.ActiveViewIndex = 1;
        btnGenerateReport.Visible = true;
    }
    protected void btnMonthlySummaryReport_Click(object sender, EventArgs e) {
        mvSPOtifReport.ActiveViewIndex = 2;
        btnGenerateReport.Visible = true;
        cusvNoSelectedVendor.Enabled = false;

    }

    /// <summary>
    /// Method to show Vendor on the basis of selected Template
    /// </summary>
    public override void TemplateSelectedIndexChanged()
    {
        base.TemplateSelectedIndexChanged();

        int VendorTemplateId = Convert.ToInt32(ucVendorTemplateSelect.innerControlddlTemplate.SelectedValue);

        ListBox lstSelectedVendor = (ListBox)msVendor.FindControl("lstRight");
        ListBox lstVendor = (ListBox)msVendor.FindControl("ucVendor").FindControl("lstLeft");

        if (VendorTemplateId > 0)
        {

            SCT_TemplateBAL oSCT_TemplateBAL = new SCT_TemplateBAL();
            SCT_TemplateBE oSCT_TemplateBE = new SCT_TemplateBE();

            oSCT_TemplateBE.VendorTemplateID = VendorTemplateId;
            oSCT_TemplateBE.Action = "GetVendorTemplateInfobyID";
            List<SCT_TemplateBE> lstVendorTemplate = oSCT_TemplateBAL.GetVendorTemplateById(oSCT_TemplateBE);

            if (lstVendorTemplate != null && lstVendorTemplate.Count > 0)
            {
                lstSelectedVendor.Items.Clear();
                msVendor.innerControlHiddenField.Value = string.Empty;
                msVendor.innerControlHiddenField2.Value = string.Empty;
                for (int iCount = 0; iCount < lstVendorTemplate.Count; iCount++)
                {
                    lstSelectedVendor.Items.Add(new ListItem(lstVendorTemplate[iCount].Vendor.Vendor_Name, lstVendorTemplate[iCount].Vendor.VendorID.ToString()));
                    msVendor.SelectedVendorIDs = lstVendorTemplate[iCount].Vendor.VendorID.ToString();
                    msVendor.SelectedVendorName = lstVendorTemplate[iCount].Vendor.Vendor_Name.ToString();
                }
            }
        }
        else
        {
            lstSelectedVendor.Items.Clear();
            msVendor.innerControlHiddenField.Value = string.Empty;
            msVendor.innerControlHiddenField2.Value = string.Empty;
        }
    }

    protected void btnGenerateReport_Click(object sender, EventArgs e) {
        OTIFReportBE oOTIFReportBE = new OTIFReportBE();
        OTIFReportBAL oOTIFReportBAL = new OTIFReportBAL();
        string sDateFrom = string.Empty;
        string sDateTo = string.Empty;
        
        if (mvSPOtifReport.ActiveViewIndex == 1) {


            UP_VendorBE oUP_VendorBE = new UP_VendorBE();
            UP_VendorBAL oUP_VendorBAL = new UP_VendorBAL();
            List<UP_VendorBE> lstDetails = new List<UP_VendorBE>();
            oUP_VendorBE.VendorID = Convert.ToInt32(lstLeft.SelectedItem.Value);
            oUP_VendorBE.Action = "ShowVendorByID";
            lstDetails = oUP_VendorBAL.GetVendorByIdBAL(oUP_VendorBE);

            string Vendor_No = lstDetails[0].Vendor_No;
            string Vendor_Country = lstDetails[0].CountryName;            

            string Year = ddlYear2.SelectedItem.Value;
            string Month = ddlMonth.SelectedItem.Value;
            Year = Year.Trim();
            Month = Month.Trim();


            string Filepath = ConfigurationManager.AppSettings["VendorOTIFMonthlyReportPath"] + "\\" + Year + "\\" + Month + "\\" + Vendor_Country + "_" + Vendor_No + "_" + "VendorOTIFReport.xls";
            if (File.Exists(Filepath)) {
                FileStream sourceFile = new FileStream(Filepath, FileMode.Open);
                float FileSize;
                FileSize = sourceFile.Length;
                byte[] getContent = new byte[(int)FileSize];
                sourceFile.Read(getContent, 0, (int)sourceFile.Length);
                sourceFile.Close();
                Response.ClearContent();
                Response.ClearHeaders();
                Response.Buffer = true;
                Response.ContentType = "application/vnd.ms-excel";
                Response.AddHeader("Content-Length", getContent.Length.ToString());
                Response.AddHeader("Content-Disposition", "attachment; filename=" + Filepath);
                Response.BinaryWrite(getContent);
                Response.Flush();
                Response.End();
            }
            else {
                string ExcelFileNotExist = WebCommon.getGlobalResourceValue("ExcelFileNotExist");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + ExcelFileNotExist + "')", true);
                return;
            }


        }
        else if (mvSPOtifReport.ActiveViewIndex == 2) {
            oOTIFReportBE.SelectedVendorIDs = msVendor.SelectedVendorIDs;
            oOTIFReportBE.SelectedStockPlannerIDs = msStockPlanner.SelectedStockPlannerIDs;
            oOTIFReportBE.DateFrom = string.IsNullOrEmpty(ddlYear.SelectedValue.ToString()) ? (DateTime?)null : Common.GetMM_DD_YYYY("01/01/" + ddlYear.SelectedValue.ToString());
            DateTime now = DateTime.Now;
            sDateTo = DateTime.Now.AddDays(-now.Day).ToString("dd/MM/yyyy");
            oOTIFReportBE.DateTo = Common.GetMM_DD_YYYY(sDateTo);

            sDateFrom = "01/01/" + ddlYear.SelectedValue.ToString();

            
            //changes as pe rthe Anthony mail sent on 10 oct 2016 ,  show NEW calc instead of OLD
            oOTIFReportBE.Action = "OTIF_ODLineLevelMonthlyReportNewMesaure"; // "OTIF_ODLineLevelMonthlyReport";
            Session["ReportPath"] = "\\ModuleUI\\OnTimeInFull\\Reports\\RDLC\\ODLineLevelMonthlyReportNew.rdlc";
            Session["DateTableName"] = "dtODLineLevelMonthlyReportNew";
            reportFileName = "OTIF_ODLineLevelMonthlyReport";

            DataSet dsOTIFReport = oOTIFReportBAL.GetOTIFReportBAL(oOTIFReportBE);
            oOTIFReportBAL = null;

            ReportDataSource rdsOTIFReport = new ReportDataSource(Session["DateTableName"].ToString(), dsOTIFReport.Tables[0]);
            ReportViewer1.LocalReport.ReportPath = Server.MapPath("~") + Session["ReportPath"].ToString();

            ReportParameter[] reportParameter = new ReportParameter[9];

            reportParameter[0] = new ReportParameter("Country",string.Empty);
            reportParameter[1] = new ReportParameter("Site", string.Empty);
            reportParameter[2] = new ReportParameter("Vendor", msVendor.SelectedVendorName);
            reportParameter[3] = new ReportParameter("DateFrom", sDateFrom);
            reportParameter[4] = new ReportParameter("DateTo", sDateTo);
            reportParameter[5] = new ReportParameter("ItemClassification", string.Empty);
            reportParameter[6] = new ReportParameter("ODSKUCode", string.Empty);
            reportParameter[7] = new ReportParameter("PurchaseOrder", string.Empty);
            reportParameter[8] = new ReportParameter("StockPlanner", msStockPlanner.SelectedSPName);


            ReportViewer1.LocalReport.SetParameters(reportParameter);

            ReportViewer1.LocalReport.DataSources.Clear();
            ReportViewer1.LocalReport.DataSources.Add(rdsOTIFReport);

            byte[] bytes = ReportViewer1.LocalReport.Render("Excel", null, out mimeType, out encoding, out extension, out streamIds, out warnings);
            Response.Buffer = true;
            Response.Clear();
            Response.ContentType = mimeType;
            Response.AddHeader("content-disposition", "attachment; filename = " + reportFileName + "." + extension);
            Response.BinaryWrite(bytes);
            Response.Flush();
        }

    }
    protected void btnSearchVendor_Click(object sender, EventArgs e) {
        UP_VendorBAL oUP_VendorBAL = new UP_VendorBAL();
        UP_VendorBE oUP_VendorBE = new UP_VendorBE();

        oUP_VendorBE.Action = "GetVendorByName";
        oUP_VendorBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
        oUP_VendorBE.User.UserID = Convert.ToInt32(Session["Userid"]);
        oUP_VendorBE.VendorName = txtVendorNo.Text;

        List<UP_VendorBE> lstUPVendor = new List<UP_VendorBE>();
        lstUPVendor = oUP_VendorBAL.GetVendorByUserCountryBAL(oUP_VendorBE);
        oUP_VendorBAL = null;
        if (lstUPVendor.Count > 0) {
            FillControls.FillListBox(ref lstLeft, lstUPVendor, "VendorName", "VendorID");
            /// --- Added for Showing Inative Vendors --- 27 Feb 2013
            //foreach (ListItem item in lstLeft.Items)
            //{
            //    for (int i = 0; i < lstUPVendor.Count; i++)
            //    {
            //        if (item.Value == lstUPVendor[i].VendorID.ToString() && lstUPVendor[i].IsActiveVendor == "N")
            //        {
            //            this.lstLeft.Items[i].Attributes.Add("Style", "background-color:RED;");
            //        }
            //    }
            //}

            for (int i = 0; i < lstUPVendor.Count; i++)
            {
                if (lstUPVendor[i].VendorID > 0)
                    if (Convert.ToInt32(lstLeft.Items[i].Value) == lstUPVendor[i].VendorID && lstUPVendor[i].IsActiveVendor == "N")
                        this.lstLeft.Items[i].Attributes.Add("Style", "background-color:RED;");
            }
            /// --- Added for Showing Inative Vendors --- 27 Feb 2013
        }
    }
}
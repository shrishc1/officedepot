﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.OnTimeInFull.Report;
using BusinessLogicLayer.ModuleBAL.OnTimeInFull.Report;

public partial class VendorReportingExclusionOverview : CommonPage
{
    protected void Page_Load(object sender, EventArgs e) {
        if (!Page.IsPostBack) {
            BindGrid();
        }

        ucExportToExcel.GridViewControl = grdVendorReportingExclusion;
        ucExportToExcel.FileName = "VendorReportingExclusion";
    }

    #region Methods

    protected void BindGrid() {
        OTIFReportBE oOTIFReportBE = new OTIFReportBE();
        OTIFReportBAL oOTIFReportBAL = new OTIFReportBAL();
        oOTIFReportBE.Action = "GetVendorExclusion";
        List<OTIFReportBE> lstVendorExclusion = new List<OTIFReportBE>();
        lstVendorExclusion = oOTIFReportBAL.GetVendorExclusionBAL(oOTIFReportBE);
        oOTIFReportBAL = null;
        grdVendorReportingExclusion.DataSource = lstVendorExclusion;
        grdVendorReportingExclusion.DataBind();
        ViewState["lstVendorExclusion"] = lstVendorExclusion;
    }
    public override BusinessEntities.BaseBe[] SubSort(GridViewSortEventArgs e) {
        return Utilities.GenericListHelper<OTIFReportBE>.SortList((List<OTIFReportBE>)ViewState["lstVendorExclusion"], e.SortExpression, e.SortDirection).ToArray();
    }
    #endregion
}
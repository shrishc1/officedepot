﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.ReportRequest;
using BusinessEntities.ModuleBE.Security;
using BusinessLogicLayer.ModuleBAL.ReportRequest;
using BusinessLogicLayer.ModuleBAL.Security;
using Microsoft.Reporting.WebForms;
using Utilities;
using WebUtilities;

public partial class ModuleUI_OnTimeInFull_Reports_OTIFExclusionsReport : CommonPage
{
    DataSet dataSet = new DataSet();
    ReportRequestBE oReportRequestBE = new ReportRequestBE();
    ReportRequestBAL oReportRequestBAL = new ReportRequestBAL();

    public string ByType
    {
        get
        {
            if (ViewState["ByType"] == null)
                ViewState["ByType"] = SortDirection.Ascending;
            return (string)ViewState["ByType"];
        }
        set { ViewState["ByType"] = value; }
    }

    #region ReportVariables
    //Warning[] warnings;
    //string[] streamIds;
    //string mimeType = string.Empty;
    //string encoding = string.Empty;
    //string extension = string.Empty;
    //string reportFileName = string.Empty;
    #endregion
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            txtFromDate.Text = DateTime.Now.AddDays(-DateTime.Now.Day + 1).ToString("dd/MM/yyyy");
            txtToDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
            hdnJSFromDt.Value = txtFromDate.Text;
            hdnJSToDt.Value = txtToDate.Text;
            btnBack.Visible = false;            
        }
        ucExportToExcel1.Visible = false;
        ucExportToExcel1.CurrentPage = this;
        ucExportToExcel1.GridViewControl = grdExport;
        ucExportToExcel1.FileName = "OtifExclusionReport";
    }
    protected void btnGenerateReport_Click(object sender, EventArgs e)
    {
        oReportRequestBE.Action = "OtifExclusionReport";

        if (rblListReportType.SelectedValue == "By Site")
        {
            oReportRequestBE.ReportAction = "BySite";
        }
        if (rblListReportType.SelectedValue == "By Planner")
        {
            oReportRequestBE.ReportAction = "ByPlanner";
        }
        if (rblListReportType.SelectedValue == "By Vendor")
        {
            oReportRequestBE.ReportAction = "ByVendor";
        }
        ByType = oReportRequestBE.ReportAction;
        oReportRequestBE.CountryIDs = msCountry.SelectedCountryIDs;
        oReportRequestBE.SiteIDs = msSite.SelectedSiteIDs;
        oReportRequestBE.VendorIDs = msVendor.SelectedVendorIDs;
        oReportRequestBE.OfficeDepotSKU = msSKU.SelectedSKUName;

        oReportRequestBE.StockPlannerGroupingIDs = multiSelectStockPlannerGrouping.SelectedStockPlannerGroupingIDs;

        if (!string.IsNullOrEmpty(msStockPlanner.SelectedStockPlannerUserIDs))
        {
            oReportRequestBE.StockPlannerIDs = msStockPlanner.SelectedStockPlannerIDs;
            oReportRequestBE.StockPlannerName = msStockPlanner.SelectedSPName;
        }

        oReportRequestBE.SkugroupingID = multiSelectSKUGrouping.SelectedSkuGroupingIDs;

        oReportRequestBE.DateFrom = string.IsNullOrEmpty(hdnJSFromDt.Value) ? (DateTime?)null : Utilities.Common.TextToDateFormat(hdnJSFromDt.Value);

        oReportRequestBE.DateTo = string.IsNullOrEmpty(hdnJSToDt.Value) ? (DateTime?)null : Utilities.Common.TextToDateFormat(hdnJSToDt.Value);

        dataSet = oReportRequestBAL.GetOtifExclusionReportBAL(oReportRequestBE);

        msVendor.ClearSearch();
        msStockPlanner.clearsearch();

        if (dataSet != null && dataSet.Tables.Count > 0)
        {
            UcDataPanel.Visible = false;
            pnlSKUExclusion.Visible = true;
            grdSkuExclusion.DataSource = dataSet;
            grdSkuExclusion.DataBind();
            btnBack.Visible = true;
            ucExportToExcel1.Visible = true;

            grdExport.DataSource = dataSet;
            grdExport.DataBind();
        }
        else
        {
            UcDataPanel.Visible = true;
            btnBack.Visible = false;
            pnlSKUExclusion.Visible = false;
            ucExportToExcel1.Visible = false;
        }
    }
    protected void grdSkuExclusion_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Header)
        {
            Label lblType = (Label)e.Row.FindControl("lblTypeHeader");

            if (ByType == "BySite")
            {
                lblType.Text = "By Site";
            }
            if (ByType == "ByPlanner")
            {
                lblType.Text = "By Planner";
            }
            if (ByType == "ByVendor")
            {
                lblType.Text = "By Vendor";
            }
        }
    }
    protected void btnBack_Click(object sender, EventArgs e)
    {
        UcDataPanel.Visible = true;
        pnlSKUExclusion.Visible = false;
        btnBack.Visible = false;
    }
    
}
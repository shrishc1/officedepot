﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="MasterVendorOTIF.aspx.cs"
    MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master" Inherits="MasterVendorOTIF" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Register Src="~/CommonUI/UserControls/ucSeacrhnSelectVendorForMasterVendorOTIF.ascx"
    TagName="ucSeacrhnSelectVendorForMasterVendorOTIF" TagPrefix="cc4" %>
<%@ Register Src="~/CommonUI/UserControls/ucVendorSelectionTemplate.ascx" TagName="VendorSelectTemplate"
    TagPrefix="cc1" %>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript">
        $(document).ready(function () {
            $("body").keypress(function (e) {
                if (e.which == 13 || e.keyCode == 13) {
                    $('#<%=btnGenerateReport.ClientID %>').click();
                    return false;
                }
            });
        });
    </script>
    <h2>
        <cc1:ucLabel ID="lblMasterVendorOTIFReport" runat="server" Text="Master Vendor OTIF Report"></cc1:ucLabel>
    </h2>
    <asp:ScriptManager ID="SM1" runat="server" AsyncPostBackTimeout="1200">
    </asp:ScriptManager>
    <asp:HiddenField ID="hdnJSFromDt" runat="server" />
    <asp:HiddenField ID="hdnJSToDt" runat="server" />
    <div class="right-shadow">
        <div class="formbox">
            <cc1:ucPanel ID="UcDataPanel" runat="server">
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <table width="100%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                            <tr style="height: 3em;">
                                <td width="10%" style="font-weight: bold;">
                                    <cc1:ucLabel ID="lblReportVersion" runat="server" Text="Report Version"></cc1:ucLabel>
                                </td>
                                <td width="1%" style="font-weight: bold;">
                                    :
                                </td>
                                <td width="100%">
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td style="font-weight: bold;" class="checkbox-list">
                                                <cc1:ucRadioButton ID="rdoEuropeanVendorOnly" runat="server" Text="European Vendor Only"
                                                    GroupName="ReportVersion" AutoPostBack="True" OnCheckedChanged="rdoReportVersion_CheckedChanged" />
                                            </td>
                                            <td style="font-weight: bold;" class="checkbox-list">
                                                <cc1:ucRadioButton ID="rdoLocalVendorOnly" runat="server" Text="Local Vendor Only"
                                                    GroupName="ReportVersion" AutoPostBack="True" OnCheckedChanged="rdoReportVersion_CheckedChanged" />
                                            </td>
                                            <td style="font-weight: bold;" class="checkbox-list">
                                                <cc1:ucRadioButton ID="rdoAllOnly" runat="server" Text="All" GroupName="ReportVersion"
                                                    Checked="true" AutoPostBack="True" OnCheckedChanged="rdoReportVersion_CheckedChanged" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <table width="100%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                    <tr>
                        <td align="right">
                            <cc1:VendorSelectTemplate runat="server" ID="ucVendorTemplateSelect" />
                        </td>
                    </tr>
                </table>
                <table width="100%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                    <tr>
                        <td style="font-weight: bold;width:10%;">
                            <cc1:ucLabel ID="lblVendor" runat="server" Text="Vendor"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            :
                        </td>
                        <td>
                            <cc4:ucSeacrhnSelectVendorForMasterVendorOTIF ID="ucSeacrhVendor1" runat="server" />
                            <%--<cc4:ucSeacrhnSelectVendorNew ID="ucSeacrhVendor1" runat="server" />--%>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <table width="100%">
                                <tr style="height: 3em;">
                                    <td width="10%" style="font-weight: bold;">
                                        <cc1:ucLabel ID="lblReportType" runat="server" Text="Report Type"></cc1:ucLabel>
                                    </td>
                                    <td width="1%" style="font-weight: bold;">
                                        :
                                    </td>
                                    <td width="89%">
                                        <table cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style="font-weight: bold;" class="checkbox-list">
                                                    <cc1:ucRadioButton ID="rdoMonthlyNew" runat="server" Text="Monthly (NEW Measure)"
                                                        GroupName="ReportType" Checked="true" />
                                                </td>
                                                <td style="font-weight: bold;" class="checkbox-list">
                                                    <cc1:ucRadioButton ID="rdoMonthlyOld" runat="server" Text="Monthly (OLD Measure)"
                                                        GroupName="ReportType" />
                                                </td>
                                                <td style="font-weight: bold;" class="checkbox-list">
                                                    <cc1:ucRadioButton ID="rdoMonthlyAbs" runat="server" Text="Monthly (Absolute)" GroupName="ReportType" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td colspan="2" style="font-weight: bold; width: 6em;">
                                        <cc1:ucDropdownList ID="ddlYear" runat="server" Width="60px">
                                        </cc1:ucDropdownList>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="right" colspan="3">
                            <div class="button-row">
                                <cc1:ucButton ID="btnGenerateReport" runat="server" Text="Generate Report" CssClass="button"
                                    OnClick="btnGenerateReport_Click" />
                            </div>
                        </td>
                    </tr>
                </table>
            </cc1:ucPanel>
        </div>
    </div>
</asp:Content>

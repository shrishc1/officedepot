﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.ReportRequest;
using BusinessLogicLayer.ModuleBAL.ReportRequest;
using BusinessLogicLayer.ModuleBAL.Security;
using BusinessEntities.ModuleBE.Security;
using BusinessEntities.ModuleBE.Upload;
using BusinessLogicLayer.ModuleBAL.Upload;
using Utilities;
using System.Collections;

public partial class ModuleUI_OnTimeInFull_Reports_RushOrder : CommonPage {
    //ReportRequestBE oReportRequestBE = new ReportRequestBE();
    //ReportRequestBAL oReportRequestBAL = new ReportRequestBAL();    

    protected void Page_Init(object sender, EventArgs e) {
        ucVendorTemplateSelect.CurrentPage = this;
        if (!Page.IsPostBack) {
            if (GetQueryStringValue("PreviousPage") != null && GetQueryStringValue("PreviousPage").ToString() != "") {
                string pageFrom = GetQueryStringValue("PreviousPage").ToString();
                if (Session["RushOrder"] != null && pageFrom == "PO") {
                    GetSession();
                    BindGrid();
                }
            }
        }
    }

    protected void Page_Load(object sender, EventArgs e) {
        btnExportToExcel.GridViewControl = tempGridView;
        btnExportToExcel.Page = this;
        btnExportToExcel.FileName = "RushOrderReport";

        if (!IsPostBack) {
            txtFromDate.Text = DateTime.Now.AddDays(-1).ToString("dd/MM/yyyy");
            txtToDate.Text = DateTime.Now.AddDays(-1).ToString("dd/MM/yyyy");
            hdnJSFromDt.Value = txtFromDate.Text;
            hdnJSToDt.Value = txtToDate.Text;
        }
        msStockPlanner.SetSPOnPostBack();
        msSite.setSitesOnPostBack();
        msVendor.setVendorsOnPostBack();
    }

    /// <summary>
    /// Method to show Vendor on the basis of selected Template
    /// </summary>
    public override void TemplateSelectedIndexChanged() {
        base.TemplateSelectedIndexChanged();

        int VendorTemplateId = Convert.ToInt32(ucVendorTemplateSelect.innerControlddlTemplate.SelectedValue);

        ListBox lstSelectedVendor = (ListBox)msVendor.FindControl("lstRight");
        ListBox lstVendor = (ListBox)msVendor.FindControl("ucVendor").FindControl("lstLeft");

        if (VendorTemplateId > 0) {

            SCT_TemplateBAL oSCT_TemplateBAL = new SCT_TemplateBAL();
            SCT_TemplateBE oSCT_TemplateBE = new SCT_TemplateBE();

            oSCT_TemplateBE.VendorTemplateID = VendorTemplateId;
            oSCT_TemplateBE.Action = "GetVendorTemplateInfobyID";
            List<SCT_TemplateBE> lstVendorTemplate = oSCT_TemplateBAL.GetVendorTemplateById(oSCT_TemplateBE);

            if (lstVendorTemplate != null && lstVendorTemplate.Count > 0) {
                lstSelectedVendor.Items.Clear();
                msVendor.innerControlHiddenField.Value = string.Empty;
                msVendor.innerControlHiddenField2.Value = string.Empty;
                for (int iCount = 0; iCount < lstVendorTemplate.Count; iCount++) {
                    lstSelectedVendor.Items.Add(new ListItem(lstVendorTemplate[iCount].Vendor.Vendor_Name, lstVendorTemplate[iCount].Vendor.VendorID.ToString()));
                    msVendor.SelectedVendorIDs = lstVendorTemplate[iCount].Vendor.VendorID.ToString();
                    msVendor.SelectedVendorName = lstVendorTemplate[iCount].Vendor.Vendor_Name.ToString();
                }
            }
        }
        else {
            lstSelectedVendor.Items.Clear();
            msVendor.innerControlHiddenField.Value = string.Empty;
            msVendor.innerControlHiddenField2.Value = string.Empty;
        }
    }

    protected void btnSearch_Click(object sender, EventArgs e) {
        BindGrid();
    }

    protected void btnBack_Click(object sender, EventArgs e) {
        UcDataPanel.Visible = true;
        UcReportPanel.Visible = false;
        //GetSession();
    }

    public override BusinessEntities.BaseBe[] SubSort(GridViewSortEventArgs e) {
        return Utilities.GenericListHelper<Up_PurchaseOrderDetailBE>.SortList((List<Up_PurchaseOrderDetailBE>)Cache["lstRushOrder"], e.SortExpression, e.SortDirection).ToArray();
    }

    protected void gvRushOrder_PageIndexChanging(object sender, GridViewPageEventArgs e) {
        if (Cache["lstRushOrder"] != null) {
            List<Up_PurchaseOrderDetailBE> lstRushOrder = Cache["lstRushOrder"] as List<Up_PurchaseOrderDetailBE>;
            gvRushOrder.PageIndex = e.NewPageIndex;
            gvRushOrder.DataSource = lstRushOrder;
            gvRushOrder.DataBind();
        }

    }

    private void BindGrid() {
        UcDataPanel.Visible = false;
        UcReportPanel.Visible = true;

        List<Up_PurchaseOrderDetailBE> lstRushOrder = null;
        
        UP_PurchaseOrderDetailBAL objPOBal = new UP_PurchaseOrderDetailBAL();
        Up_PurchaseOrderDetailBE oNewPOBE = new Up_PurchaseOrderDetailBE();

        if (!string.IsNullOrEmpty(msSite.SelectedSiteIDs))
            oNewPOBE.SelectedSiteIDs = msSite.SelectedSiteIDs;

        if (!string.IsNullOrEmpty(msVendor.SelectedVendorIDs))
            oNewPOBE.SelectedVendorIDs = msVendor.SelectedVendorIDs;

        if (!string.IsNullOrEmpty(msStockPlanner.SelectedStockPlannerIDs))
            oNewPOBE.SelectedStockPlannerIDs = msStockPlanner.SelectedStockPlannerIDs;

        oNewPOBE.FromDate = string.IsNullOrEmpty(txtFromDate.Text) ? (DateTime?)null : Common.GetMM_DD_YYYY(hdnJSFromDt.Value);
        oNewPOBE.ToDate = string.IsNullOrEmpty(txtToDate.Text) ? (DateTime?)null : Common.GetMM_DD_YYYY(hdnJSToDt.Value);

        oNewPOBE.Action = "GetRushPOReport";
        if (rdoAmended.Checked) {
            oNewPOBE.DisplayOption = "Amended";
        }
        else if (rdoNotAmended.Checked) {
            oNewPOBE.DisplayOption = "NotAmended";
        }
        lstRushOrder = objPOBal.GetRushReportBAL(oNewPOBE);
        SetSession(oNewPOBE.DisplayOption);
        Cache["lstRushOrder"] = lstRushOrder;
        if (lstRushOrder.Count > 0) {
            gvRushOrder.DataSource = lstRushOrder;
            gvRushOrder.PageIndex = 0;
            gvRushOrder.DataBind();
            btnExportToExcel.Visible = true;
        }
        else {
            gvRushOrder.DataSource = null;
            gvRushOrder.DataBind();
            btnExportToExcel.Visible = false;
        }

        //for export to excel
        tempGridView.DataSource = lstRushOrder;
        tempGridView.DataBind();
    }

    private void SetSession(string selectedRbtn) {
        Session["RushOrder"] = null;
        Hashtable htOverdueBooking = new Hashtable();
        htOverdueBooking.Add("FromDate", hdnJSFromDt.Value);
        htOverdueBooking.Add("ToDate", hdnJSToDt.Value);

        htOverdueBooking.Add("SiteIDs", msSite.SelectedSiteIDsForOB);
        htOverdueBooking.Add("SelectedSiteName", msSite.SelectedSiteName);

        htOverdueBooking.Add("SpIDs", msStockPlanner.SelectedSPIDsForOB);
        htOverdueBooking.Add("SelectedSpName", msStockPlanner.SelectedSPNameForOB);

        htOverdueBooking.Add("VendorIDs", msVendor.SelectedVendorIDs);
        htOverdueBooking.Add("SelectedVendorName", msVendor.SelectedVendorName);
        htOverdueBooking.Add("Display", selectedRbtn);

        htOverdueBooking.Add("PageIndex", 0);


        Session["RushOrder"] = htOverdueBooking;
    }

    private void GetSession() {
        if (Session["RushOrder"] != null) {
            Hashtable htOverdueBooking = (Hashtable)Session["RushOrder"];

            txtFromDate.Text = htOverdueBooking.ContainsKey("FromDate") ? htOverdueBooking["FromDate"].ToString() : "";
            txtToDate.Text = htOverdueBooking.ContainsKey("ToDate") ? htOverdueBooking["ToDate"].ToString() : "";
            hdnJSFromDt.Value = txtFromDate.Text;
            hdnJSToDt.Value = txtToDate.Text;

            if (htOverdueBooking["SiteIDs"] != null) {
                msSite.innerControlHiddenSelectedID.Value = htOverdueBooking["SiteIDs"].ToString();
                msSite.innerControlHiddenSelectedName.Value = htOverdueBooking["SelectedSiteName"].ToString();
                msSite.setSitesOnPostBack();
            }
            if (htOverdueBooking["SpIDs"] != null) {
                ((HiddenField)msStockPlanner.FindControl("hiddenSelectedIDs")).Value = htOverdueBooking["SpIDs"].ToString();
                ((HiddenField)msStockPlanner.FindControl("hiddenSelectedName")).Value = htOverdueBooking["SelectedSpName"].ToString();
                msStockPlanner.SetSPOnPostBack();
            }

            if (htOverdueBooking["VendorIDs"] != null) {
                ((HiddenField)msVendor.FindControl("hiddenSelectedIDs")).Value = htOverdueBooking["VendorIDs"].ToString();
                ((HiddenField)msVendor.FindControl("hiddenSelectedName")).Value = htOverdueBooking["SelectedVendorName"].ToString();
                msVendor.setVendorsOnPostBack();
            }

            if (htOverdueBooking["Display"] != null) {
                rdoAllOnly.Checked = false;
                rdoAmended.Checked = false;
                rdoNotAmended.Checked = false;
                string selectedRbtn = htOverdueBooking["Display"].ToString();
                if (selectedRbtn.Equals("Amended"))
                    rdoAmended.Checked = true;
                else if (selectedRbtn.Equals("NotAmended"))
                    rdoNotAmended.Checked = true;
                else
                    rdoAllOnly.Checked = true;
            }

            gvRushOrder.PageIndex = Convert.ToInt32(htOverdueBooking["PageIndex"].ToString());
        }
    }
}
﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    AutoEventWireup="true" CodeFile="StockPlannerOTIFReport.aspx.cs" Inherits="StockPlannerOTIFReport" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectVendor.ascx" TagName="MultiSelectVendor"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectStockPlanner.ascx" TagName="MultiSelectStockPlanner"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/ucVendorSelectionTemplate.ascx" TagName="VendorSelectTemplate"
    TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script type="text/javascript" language="javascript">

        function checkVendorSelected(source, args) {
            if (document.getElementById('<%=lstLeft.ClientID%>') != null) {
                var obj = document.getElementById('<%=lstLeft.ClientID%>');
                if (obj.value == '') {
                    args.IsValid = false;
                }
            }
        }

        function ShowSelectedVendorName(ele) {
            var lblObj = document.getElementById('<%=lblSelectedVendorValue.ClientID%>');
            var lstObj = ele.options[ele.selectedIndex].text;
            if (lstObj != '') {
                $(lblObj).text(lstObj);
            }


        }
        $(document).ready(function () {
            $("body").keypress(function (e) {
                if (e.which == 13 || e.keyCode == 13) {
                    $('#<%=btnGenerateReport.ClientID %>').click();
                    return false;
                }
            });
        });
    </script>
    <h2>
        <cc1:ucLabel ID="lblStockPlannerOTIFReport" runat="server"></cc1:ucLabel>
    </h2>
    <asp:ScriptManager ID="SM1" runat="server" AsyncPostBackTimeout="1200">
    </asp:ScriptManager>
    <asp:HiddenField ID="hdnJSFromDt" runat="server" />
    <asp:HiddenField ID="hdnJSToDt" runat="server" />
    <div class="right-shadow">
        <div class="formbox">
            <cc1:ucMultiView ID="mvSPOtifReport" runat="server" ActiveViewIndex="0">
                <cc1:ucView ID="vwReportType" runat="server">
                    <cc1:ucPanel ID="pnlReportType" runat="server" GroupingText="Report Type" CssClass="fieldset-form">
                        <table width="70%" cellspacing="5" align="center" cellpadding="0" border="0" class="top-settingsNoBorder">
                            <tr>
                                <td align="center">
                                    <cc1:ucButton ID="btnDetailedReport" runat="server" Text="DetailedReport" CssClass="button"
                                        OnClick="btnDetailReport_Click" />&#160;&#160;
                                    <cc1:ucButton ID="btnMonthlySummaryReport" runat="server" Text="Monthly Summary Report"
                                        CssClass="button" OnClick="btnMonthlySummaryReport_Click" />
                                </td>
                            </tr>
                        </table>
                    </cc1:ucPanel>
                </cc1:ucView>
                <cc1:ucView ID="vwDetailedreport" runat="server">
                    <table width="60%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                        <tr>
                            <td>
                            </td>
                            <td width="20%">
                                <cc1:ucLabel ID="lblVendor" runat="server" Text="Vendor" isRequired="true"></cc1:ucLabel>
                            </td>
                            <td>
                                :
                            </td>
                            <td>
                                <table>
                                    <tr>
                                        <td>
                                            <table>
                                                <tr>
                                                    <td>
                                                        <cc1:ucTextbox ID="txtVendorNo" runat="server" MaxLength="15" Width="170px"></cc1:ucTextbox>
                                                    </td>
                                                    <td>
                                                        <cc1:ucButton CssClass="button" runat="server" ID="btnSearchVendor" OnClick="btnSearchVendor_Click" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td style="width: 20%;" align="center">
                                        </td>
                                        <td style="width: 40%">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <cc1:ucListBox ID="lstLeft" runat="server" Height="150px" Width="320px" onchange="javascript:ShowSelectedVendorName(this);">
                                            </cc1:ucListBox>
                                            <asp:CustomValidator ID="cusvNoSelectedVendor" runat="server" ClientValidationFunction="checkVendorSelected"
                                                ValidationGroup="CheckVendor" Display="None"></asp:CustomValidator>
                                            <asp:ValidationSummary ID="vSummary" runat="server" ShowMessageBox="true" ShowSummary="false"
                                                Style="color: Red" ValidationGroup="CheckVendor" />
                                        </td>
                                        <td align="center">
                                            <div>
                                                &nbsp;</div>
                                            &nbsp;&nbsp;<div>
                                                &nbsp;</div>
                                            &nbsp;&nbsp;<div>
                                                &nbsp;</div>
                                        </td>
                                        <td>
                                            &nbsp;&nbsp;
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 5%">
                            </td>
                            <td>
                                <cc1:ucLabel ID="lblSelectedVendor" runat="server" Text="Selected Vendor" isRequired="true"></cc1:ucLabel>
                            </td>
                            <td>
                                :
                            </td>
                            <td>
                                <cc1:ucLabel ID="lblSelectedVendorValue" runat="server" Visible="true" Font-Bold="true"></cc1:ucLabel>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 5%">
                            </td>
                            <td>
                                <cc1:ucLabel ID="lblMonth" runat="server" isRequired="true"></cc1:ucLabel>
                            </td>
                            <td>
                                :
                            </td>
                            <td>
                                <div id="UcPanelDate">
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td style="font-weight: bold; width: 6em;">
                                                <cc1:ucDropdownList ID="ddlMonth" runat="server" Width="60px">
                                                    <asp:ListItem Text="January" Value="01"></asp:ListItem>
                                                    <asp:ListItem Text="February" Value="02"></asp:ListItem>
                                                    <asp:ListItem Text="March" Value="03"></asp:ListItem>
                                                    <asp:ListItem Text="April" Value="04"></asp:ListItem>
                                                    <asp:ListItem Text="May" Value="05"></asp:ListItem>
                                                    <asp:ListItem Text="June" Value="06"></asp:ListItem>
                                                    <asp:ListItem Text="July" Value="07"></asp:ListItem>
                                                    <asp:ListItem Text="August" Value="08"></asp:ListItem>
                                                    <asp:ListItem Text="September" Value="09"></asp:ListItem>
                                                    <asp:ListItem Text="October" Value="10"></asp:ListItem>
                                                    <asp:ListItem Text="November" Value="11"></asp:ListItem>
                                                    <asp:ListItem Text="December" Value="12"></asp:ListItem>
                                                </cc1:ucDropdownList>
                                            </td>
                                            <td style="font-weight: bold; width: 4em; text-align: center">
                                                <cc1:ucLabel ID="lblYear" runat="server" Text="Year"></cc1:ucLabel>
                                            </td>
                                            <td style="font-weight: bold; width: 6em;">
                                                <cc1:ucDropdownList ID="ddlYear2" runat="server" Width="60px">
                                                </cc1:ucDropdownList>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>
                    </table>
                </cc1:ucView>
                <cc1:ucView ID="vwMonthlySummaryReport" runat="server">
                    <cc1:ucPanel ID="UcDataPanel" runat="server">
                        <table width="95%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                            <tr>
                                <td align="right">
                                    <cc1:VendorSelectTemplate runat="server" ID="ucVendorTemplateSelect" />
                                </td>
                            </tr>
                        </table>
                        <table width="95%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                            <tr>
                                <td style="font-weight: bold;">
                                    <cc1:ucLabel ID="UcLabel1" runat="server" Text="Vendor"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold;">
                                    <cc1:ucLabel ID="UcLabel2" runat="server">:</cc1:ucLabel>
                                </td>
                                <td>
                                    <cc1:MultiSelectVendor runat="server" ID="msVendor" />
                                </td>
                            </tr>
                            <tr>
                                <td style="font-weight: bold;">
                                    <cc1:ucLabel ID="lblStockPlanner" runat="server" Text="Stock Planner"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold;">
                                    <cc1:ucLabel ID="Label17" runat="server">:</cc1:ucLabel>
                                </td>
                                <td>
                                    <cc1:MultiSelectStockPlanner runat="server" ID="msStockPlanner" />
                                </td>
                            </tr>
                            <tr>
                                <td style="font-weight: bold;">
                                    <cc1:ucLabel ID="lblSelectYear" runat="server" Text="Select Year"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold;">
                                    <cc1:ucLabel ID="UcLabel4" runat="server">:</cc1:ucLabel>
                                </td>
                                <td align="left">
                                    <div id="UcPanelYear">
                                        <table cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td colspan="2" style="font-weight: bold; width: 6em;">
                                                    <cc1:ucDropdownList ID="ddlYear" runat="server" Width="60px">
                                                    </cc1:ucDropdownList>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </cc1:ucPanel>
                </cc1:ucView>
            </cc1:ucMultiView>
            <cc1:ucPanel ID="UcReportPanel" runat="server" Visible="false">
                <table style="width: 100%;">
                    <tr>
                        <td>
                            <rsweb:ReportViewer ID="ReportViewer1" runat="server" Height="550px" Width="950px"
                                DocumentMapCollapsed="True" Font-Names="Verdana" Font-Size="8pt" InteractiveDeviceInfos="(Collection)"
                                WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt">
                            </rsweb:ReportViewer>
                        </td>
                    </tr>
                </table>
            </cc1:ucPanel>
        </div>
    </div>
    <div class="button-row" style="width: 98%">
        <cc1:ucButton ID="btnGenerateReport" runat="server" Text="Generate Report" CssClass="button"
            ValidationGroup="CheckVendor" OnClick="btnGenerateReport_Click" />
    </div>
</asp:Content>

﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.ReportRequest;
using BusinessEntities.ModuleBE.Security;
using BusinessLogicLayer.ModuleBAL.ReportRequest;
using BusinessLogicLayer.ModuleBAL.Security;
using Microsoft.Reporting.WebForms;
using Utilities;
using WebUtilities;

public partial class OTIFLeadTimeVarianceReport : CommonPage
{
    int? ReportRequest = 0;
    ReportRequestBE oReportRequestBE = new ReportRequestBE();
    ReportRequestBAL oReportRequestBAL = new ReportRequestBAL();

    #region ReportVariables
    Warning[] warnings;
    string[] streamIds;
    string mimeType = string.Empty;
    string encoding = string.Empty;
    string extension = string.Empty;
    string reportFileName = string.Empty;
    #endregion

    protected void Page_Init(object sender, EventArgs e)
    {
        ucVendorTemplateSelect.CurrentPage = this;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            txtFromDate.Text = DateTime.Now.AddDays(-DateTime.Now.Day + 1).ToString("dd/MM/yyyy");
            txtToDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
            hdnJSFromDt.Value = txtFromDate.Text;
            hdnJSToDt.Value = txtToDate.Text;
            int YearFrom = DateTime.Now.Year;
            int YearTo = DateTime.Now.Year - 10;
           
        }
    }

    /// <summary>
    /// Method to show Vendor on the basis of selected Template
    /// </summary>
    public override void TemplateSelectedIndexChanged()
    {
        base.TemplateSelectedIndexChanged();

        int VendorTemplateId = Convert.ToInt32(ucVendorTemplateSelect.innerControlddlTemplate.SelectedValue);

        ListBox lstSelectedVendor = (ListBox)msVendor.FindControl("lstRight");
        ListBox lstVendor = (ListBox)msVendor.FindControl("ucVendor").FindControl("lstLeft");

        if (VendorTemplateId > 0)
        {

            SCT_TemplateBAL oSCT_TemplateBAL = new SCT_TemplateBAL();
            SCT_TemplateBE oSCT_TemplateBE = new SCT_TemplateBE();

            oSCT_TemplateBE.VendorTemplateID = VendorTemplateId;
            oSCT_TemplateBE.Action = "GetVendorTemplateInfobyID";
            List<SCT_TemplateBE> lstVendorTemplate = oSCT_TemplateBAL.GetVendorTemplateById(oSCT_TemplateBE);

            if (lstVendorTemplate != null && lstVendorTemplate.Count > 0)
            {
                lstSelectedVendor.Items.Clear();
                msVendor.innerControlHiddenField.Value = string.Empty;
                msVendor.innerControlHiddenField2.Value = string.Empty;
                for (int iCount = 0; iCount < lstVendorTemplate.Count; iCount++)
                {
                    lstSelectedVendor.Items.Add(new ListItem(lstVendorTemplate[iCount].Vendor.Vendor_Name, lstVendorTemplate[iCount].Vendor.VendorID.ToString()));
                    msVendor.SelectedVendorIDs = lstVendorTemplate[iCount].Vendor.VendorID.ToString();
                    msVendor.SelectedVendorName = lstVendorTemplate[iCount].Vendor.Vendor_Name.ToString();
                }
            }
        }
        else
        {
            lstSelectedVendor.Items.Clear();
            msVendor.innerControlHiddenField.Value = string.Empty;
            msVendor.innerControlHiddenField2.Value = string.Empty;
        }
    }

    protected void btnGenerateReport_Click(object sender, EventArgs e)
    {

        oReportRequestBE.Action = "InsertReportRequest";
        oReportRequestBE.ModuleName = "OTIF";
        oReportRequestBE.ReportName = "Lead Time Variance";
        if (rdoSummary.Checked == true)  // For Summary
        {
            oReportRequestBE.ReportAction = "OTIF_LeadTimeVarianceSummaryReport";
            oReportRequestBE.ReportNamePath = "LeadTimeVarianceSummaryReport.rdlc";
            oReportRequestBE.ReportDatatableName = "dtLeadTimeVarianceSummaryReport";
            oReportRequestBE.ReportType = "Summary";
        }
        if (rdoDetail.Checked == true) // For Detal
        {
            oReportRequestBE.ReportAction = "OTIF_LeadTimeVarianceDetailedReport";
            oReportRequestBE.ReportNamePath = "LeadTimeVarianceDetailedReport.rdlc";
            oReportRequestBE.ReportDatatableName = "dtLeadTimeVarianceDetailedReport";
            oReportRequestBE.ReportType = "Detail";
        }
        oReportRequestBE.CountryIDs = msCountry.SelectedCountryIDs;
        oReportRequestBE.CountryName = msCountry.SelectedCountryName;
        oReportRequestBE.SiteIDs = msSite.SelectedSiteIDs;
        oReportRequestBE.SiteName = msSite.SelectedSiteName;
        oReportRequestBE.StockPlannerIDs = msStockPlanner.SelectedStockPlannerIDs;
        oReportRequestBE.VendorIDs = msVendor.SelectedVendorIDs;
        oReportRequestBE.VendorName = msVendor.SelectedVendorName;
        oReportRequestBE.OfficeDepotSKU = msSKU.SelectedSKUName;
        oReportRequestBE.PurchaseOrder = msPO.SelectedPO;
        oReportRequestBE.ItemClassification = msItemClassification.selectedItemClassification;
        oReportRequestBE.DateFrom = string.IsNullOrEmpty(txtFromDate.Text) ? (DateTime?)null : Utilities.Common.TextToDateFormat(hdnJSFromDt.Value);
        oReportRequestBE.DateTo = string.IsNullOrEmpty(txtToDate.Text) ? (DateTime?)null : Utilities.Common.TextToDateFormat(hdnJSToDt.Value);
       
        oReportRequestBE.RequestStatus = "Pending";
        oReportRequestBE.UserID = Convert.ToInt32(Session["UserID"]);
        oReportRequestBE.RequestTime = DateTime.Now;

        ReportRequest = oReportRequestBAL.addReportRequestBAL(oReportRequestBE);
        if (ReportRequest > 0) {
            string ReportRequestSubmitted = WebCommon.getGlobalResourceValue("ReportRequestSubmitted");
            ReportRequestSubmitted = ReportRequestSubmitted + " : " + Convert.ToString(ReportRequest);
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + ReportRequestSubmitted + "')", true);
            return;
        }


        //UcDataPanel.Visible = false;
        //UcReportPanel.Visible = true;

        //OTIFReportBE oOTIFReportBE = new OTIFReportBE();
        //OTIFReportBAL oOTIFReportBAL = new OTIFReportBAL();
        //string sDateFrom = string.Empty;
        //string sDateTo = string.Empty;
     

        //oOTIFReportBE.SelectedCountryIDs = msCountry.SelectedCountryIDs;
        //oOTIFReportBE.SelectedSiteIDs = msSite.SelectedSiteIDs;
        //oOTIFReportBE.SelectedStockPlannerIDs = msStockPlanner.SelectedStockPlannerIDs;
        //oOTIFReportBE.SelectedVendorIDs = msVendor.SelectedVendorIDs;
        //oOTIFReportBE.SelectedSKUCodes = msSKU.SelectedSKUName;
        //oOTIFReportBE.SelectedPurchaseOrderNo = msPO.SelectedPO;
        //oOTIFReportBE.SelectedItemClassification = msItemClassification.selectedItemClassification;
        //oOTIFReportBE.DateFrom = string.IsNullOrEmpty(txtFromDate.Text) ? (DateTime?)null : Common.GetMM_DD_YYYY(hdnJSFromDt.Value);
        //oOTIFReportBE.DateTo = string.IsNullOrEmpty(txtToDate.Text) ? (DateTime?)null : Common.GetMM_DD_YYYY(hdnJSToDt.Value);

        //sDateFrom = hdnJSFromDt.Value;
        //sDateTo = hdnJSToDt.Value;


        //if (rdoSummary.Checked)
        //{
        //    oOTIFReportBE.Action = "OTIF_LeadTimeVarianceSummaryReport";
        //    Session["ReportPath"] = "\\ModuleUI\\OnTimeInFull\\Reports\\RDLC\\LeadTimeVarianceSummaryReport.rdlc";
        //    Session["DateTableName"] = "dtLeadTimeVarianceSummaryReport";
        //    reportFileName = "OTIF_VarianceSummaryReport";
        //}
        //else
        //{
        //    oOTIFReportBE.Action = "OTIF_LeadTimeVarianceDetailedReport";
        //    Session["ReportPath"] = "\\ModuleUI\\OnTimeInFull\\Reports\\RDLC\\LeadTimeVarianceDetailedReport.rdlc";
        //    Session["DateTableName"] = "dtLeadTimeVarianceDetailedReport";
        //    reportFileName = "OTIF_VarianceDetailReport";
        //}
        //DataSet dsOTIFReport = oOTIFReportBAL.GetOTIFReportBAL(oOTIFReportBE);
        //oOTIFReportBAL = null; 

        //ReportDataSource rdsOTIFReport = new ReportDataSource(Session["DateTableName"].ToString(), dsOTIFReport.Tables[0]);
        //ReportViewer1.LocalReport.ReportPath = Server.MapPath("~") + Session["ReportPath"].ToString();

        //ReportViewer1.LocalReport.DataSources.Clear();
        //ReportViewer1.LocalReport.DataSources.Add(rdsOTIFReport);

        //ReportParameter[] reportParameter = new ReportParameter[9];
        //reportParameter[0] = new ReportParameter("Country", msCountry.SelectedCountryName);
        //reportParameter[1] = new ReportParameter("Site", msSite.SelectedSiteName);
        //reportParameter[2] = new ReportParameter("Vendor", msVendor.SelectedVendorName);
        //reportParameter[3] = new ReportParameter("DateFrom", sDateFrom);
        //reportParameter[4] = new ReportParameter("DateTo", sDateTo);
        //reportParameter[5] = new ReportParameter("ItemClassification", msItemClassification.selectedItemClassification);
        //reportParameter[6] = new ReportParameter("ODSKUCode", msSKU.SelectedSKUName);
        //reportParameter[7] = new ReportParameter("StockPlanner", msStockPlanner.SelectedStockPlannerIDs);
        //reportParameter[8] = new ReportParameter("PurchaseOrder", msPO.SelectedPO);

       
        //ReportViewer1.LocalReport.SetParameters(reportParameter);
        //ReportViewer1.Visible = true;



        //byte[] bytes = ReportViewer1.LocalReport.Render("Excel", null, out mimeType, out encoding, out extension, out streamIds, out warnings);

        //Response.Buffer = true;
        //Response.Clear();
        //Response.ContentType = mimeType;
        //Response.AddHeader("content-disposition", "attachment; filename = " + reportFileName + "." + extension);
        //Response.BinaryWrite(bytes);
        //Response.Flush(); 

    }

}
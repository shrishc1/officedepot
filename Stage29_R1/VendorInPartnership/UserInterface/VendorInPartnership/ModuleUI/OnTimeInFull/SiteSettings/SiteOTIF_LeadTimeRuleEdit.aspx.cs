﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using Utilities; using WebUtilities;
using BusinessEntities.ModuleBE.OnTimeInFull.SiteSetting;
using BusinessLogicLayer.ModuleBAL.OnTimeInFull.SiteSetting;

public partial class SiteOTIF_LeadTimeRuleEdit : CommonPage {
    protected void Page_Load(object sender, EventArgs e) {
       

        if (!IsPostBack) {
            GetSiteOTIFLeadTime();
        }
    }
    #region Methods
    public void GetSiteOTIFLeadTime() {
        if (GetQueryStringValue("SiteID") != null && GetQueryStringValue("SiteID").ToString() != "") {
            SiteOTIF_LeadTimeRuleBE oSiteOTIF_LeadTimeRuleBE = new SiteOTIF_LeadTimeRuleBE();
            SiteOTIF_LeadTimeRuleBAL oSiteOTIF_LeadTimeRuleBAL = new SiteOTIF_LeadTimeRuleBAL();
            oSiteOTIF_LeadTimeRuleBE.Action = "ShowAll";
            oSiteOTIF_LeadTimeRuleBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
            oSiteOTIF_LeadTimeRuleBE.User.UserID = Convert.ToInt32(Session["UserID"]);
            oSiteOTIF_LeadTimeRuleBE.SiteID = Convert.ToInt32(GetQueryStringValue("SiteID").ToString());
            List<SiteOTIF_LeadTimeRuleBE> lstOTIFLeadTimeRules = oSiteOTIF_LeadTimeRuleBAL.GetSiteOTIFLeadTimeDetailsBAL(oSiteOTIF_LeadTimeRuleBE);
            oSiteOTIF_LeadTimeRuleBAL = null;
            if (lstOTIFLeadTimeRules != null && lstOTIFLeadTimeRules.Count > 0) {
                OTIFSite.Text = lstOTIFLeadTimeRules[0].Site.SiteDescription.ToString();
                if (lstOTIFLeadTimeRules[0].Site.OTI_LeadTime.ToLower() == "calender days") {
                    rdoCalendarDays.Checked = true;
                    rdoWorkingDays.Checked = false;
                }
                else {
                    rdoCalendarDays.Checked = false;
                    rdoWorkingDays.Checked = true;
                }
            }
        }
    }
    #endregion

    

   

  

    #region Events

    protected void btnSave_Click(object sender, EventArgs e) {
        if (GetQueryStringValue("SiteID") != null && GetQueryStringValue("SiteID").ToString() != "") {
            SiteOTIF_LeadTimeRuleBE oSiteOTIF_LeadTimeRuleBE = new SiteOTIF_LeadTimeRuleBE();
            SiteOTIF_LeadTimeRuleBAL oSiteOTIF_LeadTimeRuleBAL = new SiteOTIF_LeadTimeRuleBAL();
            oSiteOTIF_LeadTimeRuleBE.Action = "Edit";
            oSiteOTIF_LeadTimeRuleBE.SiteID = Convert.ToInt32(GetQueryStringValue("SiteID").ToString());
            oSiteOTIF_LeadTimeRuleBE.Site = new BusinessEntities.ModuleBE.AdminFunctions.MAS_SiteBE();
            if (rdoCalendarDays.Checked) {
                oSiteOTIF_LeadTimeRuleBE.Site.OTI_LeadTime = "C";
            }
            else {
                oSiteOTIF_LeadTimeRuleBE.Site.OTI_LeadTime = "W";
            }
            oSiteOTIF_LeadTimeRuleBAL.addEditSiteOTIFLeadTimeDetailsBAL(oSiteOTIF_LeadTimeRuleBE);
            oSiteOTIF_LeadTimeRuleBAL = null;
            EncryptQueryString("SiteOTIF_LeadTimeRuleOverview.aspx");
        }
    }
    protected void btnBack_Click(object sender, EventArgs e) {
        EncryptQueryString("SiteOTIF_LeadTimeRuleOverview.aspx");
    }

    #endregion
}
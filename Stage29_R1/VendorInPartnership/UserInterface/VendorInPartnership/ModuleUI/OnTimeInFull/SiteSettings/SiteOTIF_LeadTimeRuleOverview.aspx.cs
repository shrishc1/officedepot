﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using Utilities; using WebUtilities;
using BusinessEntities.ModuleBE.OnTimeInFull.SiteSetting;
using BusinessLogicLayer.ModuleBAL.OnTimeInFull.SiteSetting;

public partial class SiteOTIF_LeadTimeRuleOverview : CommonPage {
    protected void Page_Load(object sender, EventArgs e) {
       

        if (!IsPostBack) {
            BindOTIF_LeadTimeRules();
        }
        btnExportToExcel1.GridViewControl = grdOTIFLeadTimeRules;
        btnExportToExcel1.FileName = "LeadTimeRuleOverview";
    }

    #region Methods
    
    public void BindOTIF_LeadTimeRules(){
        SiteOTIF_LeadTimeRuleBE oSiteOTIF_LeadTimeRuleBE = new SiteOTIF_LeadTimeRuleBE();
        SiteOTIF_LeadTimeRuleBAL oSiteOTIF_LeadTimeRuleBAL = new SiteOTIF_LeadTimeRuleBAL();
        oSiteOTIF_LeadTimeRuleBE.Action = "ShowAll";
        oSiteOTIF_LeadTimeRuleBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
        oSiteOTIF_LeadTimeRuleBE.User.UserID = Convert.ToInt32(Session["UserID"]);
        List<SiteOTIF_LeadTimeRuleBE> lstOTIF_LeadTimeList = oSiteOTIF_LeadTimeRuleBAL.GetSiteOTIFLeadTimeDetailsBAL(oSiteOTIF_LeadTimeRuleBE);
        oSiteOTIF_LeadTimeRuleBAL = null;
        if (lstOTIF_LeadTimeList != null && lstOTIF_LeadTimeList.Count > 0) {
            grdOTIFLeadTimeRules.DataSource = lstOTIF_LeadTimeList;
            grdOTIFLeadTimeRules.DataBind();
            ViewState["lstSites"] = lstOTIF_LeadTimeList;
        }
    }
    public override BusinessEntities.BaseBe[] SubSort(GridViewSortEventArgs e)
    {
        return Utilities.GenericListHelper<SiteOTIF_LeadTimeRuleBE>.SortList((List<SiteOTIF_LeadTimeRuleBE>)ViewState["lstSites"], e.SortExpression, e.SortDirection).ToArray();
    }


    #endregion
}
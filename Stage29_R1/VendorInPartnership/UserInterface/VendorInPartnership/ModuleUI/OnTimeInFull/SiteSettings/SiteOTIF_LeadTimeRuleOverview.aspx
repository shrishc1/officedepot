﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
 CodeFile="SiteOTIF_LeadTimeRuleOverview.aspx.cs" Inherits="SiteOTIF_LeadTimeRuleOverview" %>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register src="../../../CommonUI/UserControls/ucAddButton.ascx" tagname="ucAddButton" tagprefix="cc2" %>
<%@ Register src="../../../CommonUI/UserControls/ucExportToExcel.ascx" tagname="ucExportToExcel" tagprefix="cc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2>
        <cc1:ucLabel ID="lblLeadTimeRule" runat="server" Text="Lead Time Rule"></cc1:ucLabel>
    </h2>
    <div class="button-row">    
        <cc2:ucExportToExcel ID="btnExportToExcel1" runat="server"/>
    </div>
    <table width="100%">              
        <tr>
            <td align="center" colspan="9">
                <cc1:ucGridView ID="grdOTIFLeadTimeRules" Width="100%" runat="server" CssClass="grid" onsorting="SortGrid" AllowSorting="true">
                    <Columns>                                                
                        <asp:TemplateField HeaderText="Site" SortExpression="Site.SiteDescription">
                            <HeaderStyle Width="20%" HorizontalAlign="Left" />                            
                            <ItemTemplate>                               
                                <asp:HyperLink ID="hlSite" 
                                    runat="server"
                                    NavigateUrl='<%# EncryptQuery("SiteOTIF_LeadTimeRuleEdit.aspx?SiteID="+ Eval("SiteID"))%>' 
                                    Text='<%#Eval("Site.SiteDescription") %>'>                                    
                                 </asp:HyperLink>                                
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:TemplateField>   
                        <asp:TemplateField  HeaderText="Lead Time based on"  SortExpression="Site.OTI_LeadTime">
                            <ItemTemplate>
                                <cc1:ucLabel ID="OTI_LeadTime" runat="server" Text='<%#Eval("Site.OTI_LeadTime") %>'></cc1:ucLabel>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Left" Width="80%" />
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:TemplateField>                      
                    </Columns>                                       
                </cc1:ucGridView>               
            </td>
        </tr>             
    </table>    
</asp:Content>

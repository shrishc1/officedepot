﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    CodeFile="SiteOTIF_LeadTimeRuleEdit.aspx.cs" Inherits="SiteOTIF_LeadTimeRuleEdit" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/ucSite.ascx" TagName="ucSite" TagPrefix="cc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2>
        <cc1:ucLabel ID="lblLeadTimeRule" runat="server" Text="Lead Time Rule"></cc1:ucLabel>
    </h2>
    <script language="javascript" type="text/javascript">
        function showHideDiv() {
            var divstyle = new String();
            divstyle = document.getElementById("divVendor").style.visibility;

            if (divstyle.toLowerCase() == "visible") {
                document.getElementById("divVendor").style.visibility = "hidden";
            }
            else {
                document.getElementById("divVendor").style.visibility = "visible";
            }
            return false;
        }
    </script>
    <div class="right-shadow">
        <div class="formbox">
            <table width="35%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                <tr>
                    <td style="width: 25%">
                    </td>
                    <td style="font-weight: bold; width: 15%">
                        <cc1:ucLabel ID="lblSitePrefixName" runat="server" Text="Site"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold; width: 5px">
                        :
                    </td>
                    <td style="width: 35%" colspan="2">
                        <cc1:ucLabel ID="OTIFSite" runat="server"></cc1:ucLabel>
                    </td>
                    <td style="width: 20%">
                    </td>
                </tr>
            </table>
            <cc1:ucPanel ID="pnlLeadTimebase" runat="server" GroupingText="Lead Time as per extract is based on"
                CssClass="fieldset-form">
                <table width="100%" cellspacing="5" cellpadding="0" class="form-table">
                    <tr class="nobold">
                        <td style="width: 20%;" align="left">
                            <cc1:ucRadioButton ID="rdoCalendarDays" runat="server" GroupName="LeadTime" />
                        </td>
                        <td style="width: 80%;" align="left">
                            <cc1:ucRadioButton ID="rdoWorkingDays" runat="server" GroupName="LeadTime" />
                        </td>
                    </tr>
                </table>
            </cc1:ucPanel>
        </div>
    </div>
    <div id="divVendor" style="visibility: hidden; border-style: dotted; border-color: inherit;
        border-width: 1px; position: absolute; left: 300px; top: 375px; width: 400px;
        height: 205px;">
        <table width="100%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settingsNoBorder">
            <tr>
                <td style="font-weight: bold;" nowarp>
                   <cc1:ucLabel ID="lblVendorName" runat="server">:</cc1:ucLabel>
                </td>
                <td style="font-weight: bold;">
                    <cc1:ucLabel ID="UcLabel2" runat="server">:</cc1:ucLabel>
                </td>
                <td style="font-weight: bold;">
                    <cc1:ucTextbox ID="txtVenderName2" runat="server" Width="147px"></cc1:ucTextbox>
                </td>
                <td>
                    <div class="button-row">
                        <cc1:ucButton ID="btnSearch" runat="server" Width="60px" Text="Search" CssClass="button" />
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    &nbsp;
                </td>
                <td style="font-weight: bold;" align="left">
                    <asp:ListBox ID="lstVendor" runat="server" Width="150px">
                        <asp:ListItem>Vendor1</asp:ListItem>
                        <asp:ListItem>Vendor2</asp:ListItem>
                        <asp:ListItem>Vendor3</asp:ListItem>
                    </asp:ListBox>
                </td>
            </tr>
        </table>
    </div>
    <div class="bottom-shadow">
    </div>
    <div class="button-row">
        <cc1:ucButton ID="btnSave" runat="server" Text="Save" CssClass="button" OnClick="btnSave_Click" />
        <%--<cc1:ucButton ID="btnDelete" runat="server" Text="Delete" CssClass="button" />--%>
        <cc1:ucButton ID="btnBack" runat="server" Text="Back" CssClass="button" OnClick="btnBack_Click" />
    </div>
</asp:Content>

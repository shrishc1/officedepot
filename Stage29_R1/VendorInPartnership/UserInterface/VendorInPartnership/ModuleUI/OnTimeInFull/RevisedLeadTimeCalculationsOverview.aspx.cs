﻿using System;
using System.Collections.Generic;

using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using BusinessEntities.ModuleBE.Upload;
using BusinessLogicLayer.ModuleBAL.Upload;
using Utilities;
using WebUtilities;
using System.Collections.Generic;
using System.Collections;


public partial class RevisedLeadTimeCalculationsOverview : CommonPage
{
    #region Declarations ...
    private const int gridPageSize = 50;
    #endregion

    protected void Page_Init(object sender, EventArgs e)
    {
        ddlCountry.CurrentPage = this;
        ddlCountry.IsAllRequired = true;

    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            txtFromDate.Text = DateTime.Now.AddDays(-DateTime.Now.Day + 1).ToString("dd/MM/yyyy");
            txtToDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
            hdnJSFromDt.Value = txtFromDate.Text;
            hdnJSToDt.Value = txtToDate.Text;
            pager1.PageSize = gridPageSize;
            pager1.GenerateGoToSection = false;
            pager1.GeneratePagerInfoSection = false;
            pager1.Visible = false;
        }
    }

    public override void CountryPost_Load()
    {

        if (!IsPostBack)
        {
            ddlCountry.innerControlddlCountry.SelectedIndex = 0;
        }
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (GetQueryStringValue("PreviousPage") != null && GetQueryStringValue("PreviousPage").ToString() == "PO")
        {
            if (Session["RevisedLeadTime"] != null)
            {
                GetSession();
            }
        }
        else
        {
            int pageindex = pager1.CurrentIndex;
            // BindGrid(pageindex);

        }
    }



    protected void btnGo_Click(object sender, EventArgs e)
    {
        BindGrid(1);
    }

    protected void grdRevisedleadListing_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        if (ViewState["lstSites"] != null)
        {
            grdRevisedleadListing.DataSource = (List<Up_PurchaseOrderDetailBE>)ViewState["lstSites"];
            grdRevisedleadListing.PageIndex = e.NewPageIndex;
            if (Session["RevisedLeadTime"] != null)
            {
                Hashtable htRevisedLeadTime = (Hashtable)Session["RevisedLeadTime"];
                htRevisedLeadTime.Remove("PageIndex");
                htRevisedLeadTime.Add("PageIndex", e.NewPageIndex);
                Session["RevisedLeadTime"] = htRevisedLeadTime;
            }
            grdRevisedleadListing.DataBind();
        }
    }

    protected void btnExport_Click(object sender, EventArgs e)
    {
        Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE = new Up_PurchaseOrderDetailBE();
        UP_PurchaseOrderDetailBAL oUP_PurchaseOrderDetailBAL = new UP_PurchaseOrderDetailBAL();

        oUp_PurchaseOrderDetailBE.SKU = new UP_SKUBE();
        oUp_PurchaseOrderDetailBE.Action = "GetRevisedLeadList";

        oUp_PurchaseOrderDetailBE.CountryID = ddlCountry.innerControlddlCountry.SelectedIndex > 0 ? Convert.ToInt32(ddlCountry.innerControlddlCountry.SelectedValue) : 0;

        try
        {
            oUp_PurchaseOrderDetailBE.Purchase_order = txtPOSearch.Text;
        }
        catch
        {
            oUp_PurchaseOrderDetailBE.Purchase_order = "0";
        }

        oUp_PurchaseOrderDetailBE.GridCurrentPageNo = 0;
        oUp_PurchaseOrderDetailBE.GridPageSize = 0;
        
        oUp_PurchaseOrderDetailBE.DateFrom = Common.GetYYYY_MM_DD(hdnJSFromDt.Value);
        oUp_PurchaseOrderDetailBE.DateTo = Common.GetYYYY_MM_DD(hdnJSToDt.Value);

        oUp_PurchaseOrderDetailBE.SKU.Direct_SKU = txtSku.Text.Trim();
        oUp_PurchaseOrderDetailBE.VikingSku = txtCATCode.Text.Trim();
       
        List<Up_PurchaseOrderDetailBE> lstRevisedLeadListing = new List<Up_PurchaseOrderDetailBE>();
        lstRevisedLeadListing = oUP_PurchaseOrderDetailBAL.GetRevisedLeadListBAL(oUp_PurchaseOrderDetailBE);


        if (lstRevisedLeadListing != null && lstRevisedLeadListing.Count > 0)
        {
            grdRevisedleadListingExcel.DataSource = lstRevisedLeadListing;
        }
        else
        {
            grdRevisedleadListingExcel.DataSource = null;
        }
        grdRevisedleadListingExcel.DataBind();

        oUP_PurchaseOrderDetailBAL = null;

        WebCommon.Export("RevisedLeadDue", grdRevisedleadListingExcel);
    }

    public void pager_Command(object sender, CommandEventArgs e)
    {
        int currnetPageIndx = Convert.ToInt32(e.CommandArgument);
        pager1.CurrentIndex = currnetPageIndx;
        BindGrid(currnetPageIndx);
    }

    #region Methods

    protected void BindGrid(int Page = 1)
    {
        Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE = new Up_PurchaseOrderDetailBE();
        UP_PurchaseOrderDetailBAL oUP_PurchaseOrderDetailBAL = new UP_PurchaseOrderDetailBAL();
        oUp_PurchaseOrderDetailBE.Action = "GetRevisedLeadList";

        oUp_PurchaseOrderDetailBE.CountryID = ddlCountry.innerControlddlCountry.SelectedIndex > 0 ? Convert.ToInt32(ddlCountry.innerControlddlCountry.SelectedValue) : 0;

        try
        {
            oUp_PurchaseOrderDetailBE.Purchase_order = txtPOSearch.Text;
        }
        catch
        {
            oUp_PurchaseOrderDetailBE.Purchase_order = "0";
        }

        oUp_PurchaseOrderDetailBE.GridCurrentPageNo = Page;
        oUp_PurchaseOrderDetailBE.GridPageSize = gridPageSize;
        oUp_PurchaseOrderDetailBE.DateFrom = Common.GetYYYY_MM_DD(hdnJSFromDt.Value);
        oUp_PurchaseOrderDetailBE.DateTo = Common.GetYYYY_MM_DD(hdnJSToDt.Value);

        oUp_PurchaseOrderDetailBE.SKU = new UP_SKUBE();
        oUp_PurchaseOrderDetailBE.SKU.Direct_SKU = txtSku.Text.Trim();
        oUp_PurchaseOrderDetailBE.VikingSku = txtCATCode.Text.Trim();
        
        this.SetSession(oUp_PurchaseOrderDetailBE);

        List<Up_PurchaseOrderDetailBE> lstRevisedLeadListing = new List<Up_PurchaseOrderDetailBE>();
        lstRevisedLeadListing = oUP_PurchaseOrderDetailBAL.GetRevisedLeadListBAL(oUp_PurchaseOrderDetailBE);


        if (lstRevisedLeadListing != null && lstRevisedLeadListing.Count > 0)
        {
             grdRevisedleadListing.DataSource = lstRevisedLeadListing;
            pager1.ItemCount = lstRevisedLeadListing[0].TotalRecords;
            btnExportToExcel.Visible = true;
            if (pager1.ItemCount > gridPageSize)
                pager1.Visible = true;
            else
                pager1.Visible = false;
        }
        else
        {
            grdRevisedleadListing.DataSource = null;
            pager1.Visible = false;
            btnExportToExcel.Visible = false;
        }
       
        txtFromDate.Text = hdnJSFromDt.Value;
        txtToDate.Text = hdnJSToDt.Value;
        grdRevisedleadListing.PageIndex = Page;
        pager1.CurrentIndex = Page;

        grdRevisedleadListing.DataBind();

        oUP_PurchaseOrderDetailBAL = null;

        ViewState["lstSites"] = lstRevisedLeadListing;
    }


    private void SetSession(Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE)
    {
        Session["RevisedLeadTime"] = null;
        Session.Remove("RevisedLeadTime");

        Hashtable htRevisedLeadTime = new Hashtable();
        htRevisedLeadTime.Add("SelectedCountryId", oUp_PurchaseOrderDetailBE.CountryID);
        htRevisedLeadTime.Add("SelectedPo", oUp_PurchaseOrderDetailBE.Purchase_order);
       
        htRevisedLeadTime.Add("DateFrom", hdnJSFromDt.Value);
        htRevisedLeadTime.Add("DateTo", hdnJSToDt.Value);

        htRevisedLeadTime.Add("SKU", oUp_PurchaseOrderDetailBE.SKU.Direct_SKU);
        htRevisedLeadTime.Add("Viking", oUp_PurchaseOrderDetailBE.VikingSku);

        htRevisedLeadTime.Add("PageIndex", oUp_PurchaseOrderDetailBE.GridCurrentPageNo);

        Session["RevisedLeadTime"] = htRevisedLeadTime;


    }


    private void GetSession()
    {
        Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE = new Up_PurchaseOrderDetailBE();
        UP_PurchaseOrderDetailBAL oUP_PurchaseOrderDetailBAL = new UP_PurchaseOrderDetailBAL();
        if (Session["RevisedLeadTime"] != null)
        {
            Hashtable htRevisedLeadTime = (Hashtable)Session["RevisedLeadTime"];


            oUp_PurchaseOrderDetailBE.CountryID = (htRevisedLeadTime.ContainsKey("SelectedCountryId") && htRevisedLeadTime["SelectedCountryId"] != null) ? Convert.ToInt32(htRevisedLeadTime["SelectedCountryId"].ToString()) : 0;
            ddlCountry.innerControlddlCountry.SelectedValue = htRevisedLeadTime["SelectedCountryId"].ToString();

            oUp_PurchaseOrderDetailBE.Purchase_order = (htRevisedLeadTime.ContainsKey("SelectedPo") && htRevisedLeadTime["SelectedPo"] != null) ? htRevisedLeadTime["SelectedPo"].ToString() : "0";
            txtPOSearch.Text = (htRevisedLeadTime.ContainsKey("SelectedPo") && htRevisedLeadTime["SelectedPo"] != null) ? htRevisedLeadTime["SelectedPo"].ToString() : "0";

            oUp_PurchaseOrderDetailBE.GridCurrentPageNo = Convert.ToInt32(htRevisedLeadTime["PageIndex"].ToString());
            oUp_PurchaseOrderDetailBE.GridPageSize = gridPageSize;

            oUp_PurchaseOrderDetailBE.DateFrom = (htRevisedLeadTime.ContainsKey("DateFrom") && htRevisedLeadTime["DateFrom"] != null) ? Common.GetYYYY_MM_DD(htRevisedLeadTime["DateFrom"].ToString()) : null;
            oUp_PurchaseOrderDetailBE.DateTo = (htRevisedLeadTime.ContainsKey("DateTo") && htRevisedLeadTime["DateTo"] != null) ? Common.GetYYYY_MM_DD(htRevisedLeadTime["DateTo"].ToString()) : null;


            txtFromDate.Text = (htRevisedLeadTime.ContainsKey("DateFrom") && htRevisedLeadTime["DateFrom"] != null) ? htRevisedLeadTime["DateFrom"].ToString() : null;
            txtToDate.Text = (htRevisedLeadTime.ContainsKey("DateTo") && htRevisedLeadTime["DateTo"] != null) ? htRevisedLeadTime["DateTo"].ToString() : null;
            hdnJSFromDt.Value = txtFromDate.Text;
            hdnJSToDt.Value = txtToDate.Text;

            oUp_PurchaseOrderDetailBE.SKU = new BusinessEntities.ModuleBE.Upload.UP_SKUBE(); 
            oUp_PurchaseOrderDetailBE.SKU.Direct_SKU = (htRevisedLeadTime.ContainsKey("SKU") && htRevisedLeadTime["SKU"] != null) ? htRevisedLeadTime["SKU"].ToString() : "";
            oUp_PurchaseOrderDetailBE.VikingSku = (htRevisedLeadTime.ContainsKey("Viking") && htRevisedLeadTime["Viking"] != null) ? htRevisedLeadTime["Viking"].ToString() : "";
            txtSku.Text = (htRevisedLeadTime.ContainsKey("SKU") && htRevisedLeadTime["SKU"] != null) ? htRevisedLeadTime["SKU"].ToString() : "";
            txtCATCode.Text = (htRevisedLeadTime.ContainsKey("Viking") && htRevisedLeadTime["Viking"] != null) ? htRevisedLeadTime["Viking"].ToString() : "";


            oUp_PurchaseOrderDetailBE.Action = "GetRevisedLeadList";
            List<Up_PurchaseOrderDetailBE> lstRevisedLeadListing = new List<Up_PurchaseOrderDetailBE>();
            lstRevisedLeadListing = oUP_PurchaseOrderDetailBAL.GetRevisedLeadListBAL(oUp_PurchaseOrderDetailBE);


            if (lstRevisedLeadListing != null && lstRevisedLeadListing.Count > 0)
            {
                grdRevisedleadListing.DataSource = lstRevisedLeadListing;
                pager1.ItemCount = lstRevisedLeadListing[0].TotalRecords;
                btnExportToExcel.Visible = true;
                if (pager1.ItemCount > gridPageSize)
                    pager1.Visible = true;
                else
                    pager1.Visible = false;
            }
            else
            {
                grdRevisedleadListing.DataSource = null;
                pager1.Visible = false;
                btnExportToExcel.Visible = false;
            }

            grdRevisedleadListing.DataBind();

            ViewState["lstSites"] = lstRevisedLeadListing;

            pager1.CurrentIndex = Convert.ToInt32(htRevisedLeadTime["PageIndex"].ToString());


            if (grdRevisedleadListing.Rows.Count > 0)
                btnExportToExcel.Visible = true;

            oUP_PurchaseOrderDetailBAL = null;

        }
    }

    public override BusinessEntities.BaseBe[] SubSort(GridViewSortEventArgs e)
    {

        int pageindex = pager1.CurrentIndex;
        BindGrid(pageindex);
        return Utilities.GenericListHelper<Up_PurchaseOrderDetailBE>.SortList((List<Up_PurchaseOrderDetailBE>)ViewState["lstSites"], e.SortExpression, e.SortDirection).ToArray();
    }


    #endregion
}
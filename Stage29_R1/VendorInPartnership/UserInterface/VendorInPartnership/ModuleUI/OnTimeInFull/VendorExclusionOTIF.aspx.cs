﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebUtilities;
using BusinessEntities.ModuleBE.Appointment.CountrySetting;
using BusinessLogicLayer.ModuleBAL.Appointment.CountrySetting;
using BusinessEntities.ModuleBE.AdminFunctions;
using BusinessLogicLayer.ModuleBAL.AdminFunctions;
using BusinessEntities.ModuleBE.VendorScorecard;
using BusinessLogicLayer.ModuleBAL.VendorScorecard;
using BusinessEntities.ModuleBE.OnTimeInFull;
using BusinessLogicLayer.ModuleBAL.OnTimeInFull;
using System.Data;

public partial class ModuleUI_OnTimeInFull_VendorExclusionOTIF : CommonPage
{
    protected bool IsVendorByNameClicked = false;
    protected bool IsVendorByNumberClicked = false;
    protected string AutoEmailPoError = WebCommon.getGlobalResourceValue("AutoEmailPoError");
    protected string selectCheckboxToAccept = WebCommon.getGlobalResourceValue("SelectCheckboxToAction");
    bool IsExportClicked = false;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            pager1.PageSize = 200;
            ucExportToExcel1.Visible = false;
            pager1.GenerateGoToSection = false;
            pager1.GeneratePagerInfoSection = false;
            pager1.Visible = false;
            BindCountry();
            BindGrid();
        }

    }

    protected void BindGrid(int Page = 1)
    {
        var applyOTIFExclusionBE = new ApplyOTIFExclusionBE();
        var applyOTIFExclusionBAL = new ApplyOTIFExclusionBAL();
        if (string.IsNullOrEmpty(GetQueryStringValue("VendorID")))
        {
            applyOTIFExclusionBE.Action = "BindVendorExclusion";
            applyOTIFExclusionBE.ExcludedStatus = Convert.ToInt32(rblStatus.SelectedValue);
            applyOTIFExclusionBE.CountryID = Convert.ToInt32(ddlCountry.SelectedValue);
        }
        else
        {
            applyOTIFExclusionBE.Action = "BindVendorExclusionDetails";
            if (!string.IsNullOrEmpty(GetQueryStringValue("Status")))
            {
                applyOTIFExclusionBE.ExcludedStatus = Convert.ToInt32(GetQueryStringValue("Status"));
            }
            else
            {
                applyOTIFExclusionBE.ExcludedStatus = null;
            }
            if (!string.IsNullOrEmpty(GetQueryStringValue("VendorId")))
            {
                applyOTIFExclusionBE.VendorID = Convert.ToInt32(GetQueryStringValue("VendorId"));
            }
            if (!string.IsNullOrEmpty(GetQueryStringValue("CountryID")))
            {
                applyOTIFExclusionBE.CountryID = Convert.ToInt32(GetQueryStringValue("CountryID"));
            }
        }

        applyOTIFExclusionBE.SelectedVendorIDs = SelectedVendorIDs;

        if (IsExportClicked)
        {
            applyOTIFExclusionBE.GridCurrentPageNo = 0;
            applyOTIFExclusionBE.GridPageSize = 0;
        }
        else
        {
            applyOTIFExclusionBE.GridCurrentPageNo = Page;
            applyOTIFExclusionBE.GridPageSize = 200;
        }


        DataSet ds = applyOTIFExclusionBAL.GetVendorExclusionBAL(applyOTIFExclusionBE);

        DataTable dt = new DataTable();
        dt = ds.Tables[0];
        if (dt.Rows.Count > 0 && dt != null)
        {

            ucExportToExcel1.Visible = true;

            if (string.IsNullOrEmpty(GetQueryStringValue("VendorID")))
            {
                grdVendorExclusion.DataSource = dt;
                grdVendorExclusion.DataBind();
                grdVendorExclusion.Visible = true;
                grdVendorWise.Visible = false;
                pager1.Visible = true;
                pager1.ItemCount = Convert.ToInt32(dt.Rows[0]["TotalRecord"]);
                ViewState["TotalRecord"] = dt.Rows[0]["TotalRecord"];
                pager1.CurrentIndex = Page;
                tblSearch.Visible = true;
                btnBack.Visible = false;
                tblgrdVendorWise2.Visible = false;
                divButton.Visible = false;
            }
            else
            {
                if (ds.Tables[1].Rows.Count > 0)
                {
                    divButton.Visible = true;
                }
                else
                {
                    divButton.Visible = false;
                }
                tblSearch.Visible = false;
                pager1.Visible = false;
                lblCountryValue.Text = Convert.ToString(ds.Tables[0].Rows[0]["Country"]);
                lblVendorValue.Text = Convert.ToString(ds.Tables[0].Rows[0]["Vendor"]);
                lblUpdatedByValue.Text = Convert.ToString(ds.Tables[0].Rows[0]["AddedBy"]);
                lblUpdatedOnValue.Text = Convert.ToString(ds.Tables[0].Rows[0]["DateAdded"]);
                if (!string.IsNullOrEmpty(GetQueryStringValue("Status")))
                {
                    lblCurrentStatusValue.Text = Convert.ToInt32(GetQueryStringValue("Status")) == 1 ? "Active" : "Deactive";
                }
                else
                {
                    lblCurrentStatusValue.Text = "-";
                }
                if (string.IsNullOrEmpty(GetQueryStringValue("VendorID")))
                {
                    dt = ds.Tables[0];
                }
                else
                {
                    dt = ds.Tables[1];
                }
                grdVendorWise.DataSource = dt;
                grdVendorWise.DataBind();
                grdVendorExclusion.Visible = false;
                grdVendorWise.Visible = true;
                btnBack.Visible = true;
                tblgrdVendorWise2.Visible = true;
                divButton.Visible = true;
                if (string.IsNullOrEmpty(GetQueryStringValue("Status")))
                {
                    btnActivate.Visible = true;
                    btnDeActivate.Visible = false;
                }
                else
                {
                    if (Convert.ToInt32(GetQueryStringValue("Status")) == 1)
                    {
                        btnActivate.Visible = false;
                        btnDeActivate.Visible = true;
                    }
                    else
                    {
                        btnActivate.Visible = true;
                        btnDeActivate.Visible = false;
                    }

                }
            }
        }
        else
        {
            pager1.Visible = false;
            ucExportToExcel1.Visible = false;
            grdVendorExclusion.DataSource = null;
            grdVendorExclusion.DataBind();
            grdVendorWise.DataSource = null;
            grdVendorWise.DataBind();
            btnBack.Visible = false;
            tblgrdVendorWise2.Visible = false;
            divButton.Visible = false;
            if (string.IsNullOrEmpty(GetQueryStringValue("VendorID")))
            {
                tblSearch.Visible = true;
            }
            else
            {
                tblSearch.Visible = false;
            }
        }
        ViewState["hiddenSelectedIDs"] = hiddenSelectedIDs.Value;
        ViewState["hiddenSelectedName"] = hiddenSelectedName.Value;
        //hiddenSelectedIDs.Value = string.Empty;
        //hiddenSelectedName.Value = string.Empty;
    }

    //public override BusinessEntities.BaseBe[] SubSort(GridViewSortEventArgs e)
    //{
    //    return Utilities.GenericListHelper<MASCNT_AutoEmailBE>.SortList((List<MASCNT_AutoEmailBE>)ViewState["lstAutoEmailBE"], e.SortExpression, e.SortDirection).ToArray();
    //}

    protected void ddlCountry_SelectedIndexChanged(object sender, EventArgs e)
    {
        // BindVendor();
        txtVendorNo.Text = "";
        pager1.ItemCount = Convert.ToInt32(ViewState["TotalRecord"]);
    }

    private string _selectedVendorIDs = string.Empty;
    public string SelectedVendorIDs
    {
        get
        {
            if (!string.IsNullOrEmpty(hiddenSelectedIDs.Value))
            {
                return hiddenSelectedIDs.Value.Substring(0, hiddenSelectedIDs.Value.Length - 1);
            }
            return null;
        }
        set
        {
            hiddenSelectedIDs.Value += value + ",";
        }
    }

    protected void BindCountry()
    {
        MAS_CountryBE oMAS_CountryBE = new MAS_CountryBE();
        MAS_CountryBAL oMAS_CountryBAL = new MAS_CountryBAL();

        oMAS_CountryBE.Action = "ShowAll";
        oMAS_CountryBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
        oMAS_CountryBE.User.UserID = Convert.ToInt32(Session["UserID"]);

        List<MAS_CountryBE> lstCountry = new List<MAS_CountryBE>();
        lstCountry = oMAS_CountryBAL.GetCountryBAL(oMAS_CountryBE);
        oMAS_CountryBAL = null;
        if (lstCountry.Count > 0)
        {
            FillControls.FillDropDown(ref ddlCountry, lstCountry.ToList(), "CountryName", "CountryID", "All");
        }
        if (Session["SiteCountryID"] != null)
        {
            ddlCountry.SelectedIndex = ddlCountry.Items.IndexOf(ddlCountry.Items.FindByValue(Session["SiteCountryID"].ToString()));
        }
    }

    protected void BindVendor()
    {
        MAS_VendorScoreCardBE objVendorScoreCardBE = new MAS_VendorScoreCardBE();
        MAS_VendorScoreCardBAL objVendorScoreCardBAL = new MAS_VendorScoreCardBAL();

        objVendorScoreCardBE.Action = "GetVendorByCountryVendorExclusionOTIF";
        objVendorScoreCardBE.CountryID = Convert.ToInt32(ddlCountry.SelectedValue);

        if (IsVendorByNameClicked == true)
        {
            objVendorScoreCardBE.VendorName = txtVendorNo.Text.Trim();
        }
        else if (IsVendorByNumberClicked == true)
        {
            objVendorScoreCardBE.VendorNo = txtVendorNo.Text.Trim();
        }

        List<MAS_VendorScoreCardBE> lstUPVendor = new List<MAS_VendorScoreCardBE>();
        lstUPVendor = objVendorScoreCardBAL.GetVendorByCountryBAL(objVendorScoreCardBE);
        objVendorScoreCardBAL = null;
        if (lstUPVendor.Count > 0)
        {
            FillControls.FillListBox(ref lstLeft, lstUPVendor, "VendorName", "VendorID");
        }
        else
        {
            lstLeft.Items.Clear();
            lstRight.Items.Clear();
        }

    }


    #region Events

    protected virtual void btnMoveRightAll_Click(object sender, EventArgs e)
    {
        FillControls.MoveAllItems(lstLeft, lstRight);
    }

    protected virtual void btnMoveRight_Click(object sender, EventArgs e)
    {
        if (lstLeft.SelectedItem != null)
        {
            FillControls.MoveOneItem(lstLeft, lstRight);
        }
    }

    protected virtual void btnMoveLeft_Click(object sender, EventArgs e)
    {
        if (lstRight.SelectedItem != null)
        {
            FillControls.MoveOneItem(lstRight, lstLeft);
        }
    }

    protected virtual void btnMoveLeftAll_Click(object sender, EventArgs e)
    {
        FillControls.MoveAllItems(lstRight, lstLeft);
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        IsVendorByNameClicked = true;
        IsVendorByNumberClicked = false;
        lstRight.Items.Clear();
        hiddenSelectedIDs.Value = string.Empty;
        hiddenSelectedName.Value = string.Empty;
        BindVendor();
        pager1.ItemCount = Convert.ToInt32(ViewState["TotalRecord"]);

    }

    protected void btnSearchByVendorNo_Click(object sender, EventArgs e)
    {
        IsVendorByNameClicked = false;
        IsVendorByNumberClicked = true;
        lstRight.Items.Clear();
        hiddenSelectedIDs.Value = string.Empty;
        hiddenSelectedName.Value = string.Empty;
        BindVendor();
        pager1.ItemCount = Convert.ToInt32(ViewState["TotalRecord"]);
    }
    #endregion

    public void pager_Command(object sender, CommandEventArgs e)
    {
        int currnetPageIndx = Convert.ToInt32(e.CommandArgument);
        pager1.CurrentIndex = currnetPageIndx;
        BindGrid(currnetPageIndx);
    }

    protected void btnExport_Click(object sender, EventArgs e)
    {
        IsExportClicked = true;
        BindGrid();
        if (string.IsNullOrEmpty(GetQueryStringValue("Vendorid")))
        {           
            WebCommon.ExportHideHidden("VendorExclusion", grdVendorExclusion);
        }
        else
        {
            WebCommon.ExportHideHidden("VendorWiseExclusion", grdVendorWise);
        }
    }

    public void SetVendorOnPostBack()
    {
        string[] strSelectedName = null;
        string[] strSelectedIDs = null;
        lstRight.Items.Clear();
        if (!string.IsNullOrEmpty(Convert.ToString(ViewState["hiddenSelectedName"])))
        {
            strSelectedName = Convert.ToString(ViewState["hiddenSelectedName"]).Split(',');
        }
        if (!string.IsNullOrEmpty(Convert.ToString(ViewState["hiddenSelectedIDs"])))
        {
            strSelectedIDs = Convert.ToString(ViewState["hiddenSelectedIDs"]).Split(',');
        }
        //string[] strSelectedName = hiddenSelectedName.Value.TrimEnd(',').TrimEnd(' ').Split(',');
        //string[] strSelectedIDs = hiddenSelectedIDs.Value.TrimEnd(',').Split(',');
        if (strSelectedName != null && strSelectedIDs != null)
        {
            for (int i = 0; i < strSelectedName.Length; i++)
            {
                lstRight.Items.Add(new ListItem(strSelectedName[i].ToString(), strSelectedIDs[i].ToString()));
                lstLeft.Items.Remove(new ListItem(strSelectedName[i].ToString(), strSelectedIDs[i].ToString()));
                lstLeft.Items.Remove(new ListItem(null, null));
                lstRight.Items.Remove(new ListItem(null, null));
            }
        }

    }
    protected void btnSearch_Click1(object sender, EventArgs e)
    {
        BindGrid();
        SetVendorOnPostBack();
    }

    protected void grdVendorExclusion_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Header)
        {
            e.Row.Cells[5].Visible = false;
            e.Row.Cells[6].Visible = false;
            e.Row.Cells[7].Visible = false;
        }
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            var URL = "";
            dynamic firstCell = e.Row.Cells[1];
            //if (!string.IsNullOrEmpty(e.Row.Cells[7].Text) && e.Row.Cells[7].Text!="&nbsp;")
            //{
            int? status;
            firstCell.Controls.Clear();
            if (e.Row.Cells[7].Text == "True")
            {
                status = 1;
            }
            else if (e.Row.Cells[7].Text == "False")
            {
                status = 0;
            }
            else
            {
                status = null;
            }
            URL = EncryptQuery("VendorExclusionOTIF.aspx?VendorID=" + e.Row.Cells[5].Text + "&CountryID=" + e.Row.Cells[6].Text + "&Status=" + status);
            firstCell.Controls.Add(new HyperLink
            {
                NavigateUrl = URL,
                Text = firstCell.Text,
                Target = "_blank"
            });
            // }
            e.Row.Cells[5].Visible = false;
            e.Row.Cells[6].Visible = false;
            e.Row.Cells[7].Visible = false;
        }
    }
    protected void btnBack_Click(object sender, EventArgs e)
    {
        Page.ClientScript.RegisterStartupScript(this.GetType(), "script", "window.close()", true);       
    }
    protected void btnActivate_Click(object sender, EventArgs e)
    {
        var applyOTIFExclusionBE = new ApplyOTIFExclusionBE();
        var applyOTIFExclusionBAL = new ApplyOTIFExclusionBAL();

        applyOTIFExclusionBE.Action = "UpdateVendorExclusionDetails";
        applyOTIFExclusionBE.ExcludedStatus = 1;
        applyOTIFExclusionBE.VendorID = Convert.ToInt32(GetQueryStringValue("VendorId"));

        applyOTIFExclusionBE.CountryID = Convert.ToInt32(GetQueryStringValue("CountryID"));
        applyOTIFExclusionBE.UserID = Convert.ToInt32(Session["UserID"]);
        applyOTIFExclusionBAL.GetVendorExclusionBAL(applyOTIFExclusionBE);
        int? status = null;
        status = 1;

        EncryptQueryString("VendorExclusionOTIF.aspx?VendorID=" + GetQueryStringValue("VendorId") + "&CountryID=" + GetQueryStringValue("CountryID") + "&Status=" + status);
    }
    protected void btnDeActivate_Click(object sender, EventArgs e)
    {
        var applyOTIFExclusionBE = new ApplyOTIFExclusionBE();
        var applyOTIFExclusionBAL = new ApplyOTIFExclusionBAL();

        applyOTIFExclusionBE.Action = "UpdateVendorExclusionDetails";
        applyOTIFExclusionBE.ExcludedStatus = 0;
        applyOTIFExclusionBE.VendorID = Convert.ToInt32(GetQueryStringValue("VendorId"));

        applyOTIFExclusionBE.CountryID = Convert.ToInt32(GetQueryStringValue("CountryID"));
        applyOTIFExclusionBE.UserID = Convert.ToInt32(Session["UserID"]);
        applyOTIFExclusionBAL.GetVendorExclusionBAL(applyOTIFExclusionBE);
        int? status = null;
        status = 0;
        EncryptQueryString("VendorExclusionOTIF.aspx?VendorID=" + GetQueryStringValue("VendorId") + "&CountryID=" + GetQueryStringValue("CountryID") + "&Status=" + status);
    }
}

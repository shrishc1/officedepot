﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.Discrepancy.Report;
using BusinessLogicLayer.ModuleBAL.Discrepancy.Report;
using Microsoft.Reporting.WebForms;
using System.Collections;

public partial class ReportView : CommonPage
{
    //DataSet dsSubDetailDataSet = new DataSet();
    //DataTable dsSubDetailDataTable = new DataTable();
    //DataTable dsSubRowDetail = new DataTable();
    //DataTable dsDetail = new DataTable();
    //int recordCounter = 0;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ReportViewer1.ShowRefreshButton = false;
            ReportParameter[] rptParam = new ReportParameter[5];
            ReportDataSource rds = new ReportDataSource();
            if (Session["DateTableName"] != null && Session["DataSetObject"] != null)
            {
                rds = new ReportDataSource(Session["DateTableName"].ToString(), (DataTable)Session["DataSetObject"]);
                this.ReportViewer1.LocalReport.Refresh();
                ReportViewer1.LocalReport.DataSources.Clear();
                ReportViewer1.LocalReport.DataSources.Add(rds);
                ReportViewer1.LocalReport.ReportPath = Server.MapPath("~") + Session["ReportPath"].ToString();
            }

            if (Session["ReportParameter"] != null)
            {
                rptParam = (ReportParameter[])Session["ReportParameter"];
                ReportViewer1.LocalReport.SetParameters(rptParam);
            }
        }
    }

    protected void BackSearch_Click(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(GetQueryStringValue("PageName")) && !string.IsNullOrWhiteSpace(GetQueryStringValue("PageName")))
        {
            switch (GetQueryStringValue("PageName").Trim().ToUpper())
            {
                case "DTRS":
                    EncryptQueryString("~/ModuleUI/Discrepancy/Report/DiscrepancyTrackingReportSearch.aspx");
                    break;
                case "CDPAPS":
                    EncryptQueryString("~/ModuleUI/Discrepancy/Report/CreditDebitPostedAndPendingSearch.aspx");
                    break;
                case "VTS":
                    EncryptQueryString("~/ModuleUI/Discrepancy/Report/VendorTurnaroundSearch.aspx");
                    break;
                case "DRS":
                    EncryptQueryString("~/ModuleUI/Discrepancy/Report/DiscrepancyReportSearch.aspx");
                    break;
                case "VFFS":
                    EncryptQueryString("~/ModuleUI/Discrepancy/Report/VendorFeedbackformSearch.aspx");
                    break;
                case "ORS":
                    EncryptQueryString("~/ModuleUI/Discrepancy/Report/OversReportingSearch.aspx");
                    break;
                case "SDR":
                    EncryptQueryString("~/ModuleUI/Discrepancy/Report/ShuttleDiscrepancyReport.aspx");
                    break;
                case "DDR":
                    EncryptQueryString("~/ModuleUI/Discrepancy/Report/DeletedDiscrepanciesReport.aspx");
                    break;
                case "NRS":
                    EncryptQueryString("~/ModuleUI/Discrepancy/Report/NaxReport.aspx");
                    break;
                case "VRC":
                    EncryptQueryString("~/ModuleUI/Discrepancy/Report/VendorChargesReportSearch.aspx");
                    break; 
                default:
                    break;
            }
        }
    }
}

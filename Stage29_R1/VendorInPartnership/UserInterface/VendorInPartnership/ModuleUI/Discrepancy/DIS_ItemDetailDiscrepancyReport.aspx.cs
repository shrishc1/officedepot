﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.Discrepancy.LogDiscrepancy;
using BusinessLogicLayer.ModuleBAL.Discrepancy.LogDiscrepancy;
using WebUtilities;
using BusinessEntities.ModuleBE.AdminFunctions;
using BusinessLogicLayer.ModuleBAL.AdminFunctions;
using Utilities;
using System.Data;

public partial class ModuleUI_Discrepancy_DIS_ItemDetailDiscrepancyReport : CommonPage
{

    #region Page Level Declarations
    DiscrepancyBE oNewDiscrepancyBE = null;
    DiscrepancyBAL oNewDiscrepancyBAL = null;
    string PleaseEnterNumericValueForSearchBySku = WebCommon.getGlobalResourceValue("PleaseEnterNumericValueForSearchBySku");
    #endregion

    protected void Page_Prerender(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //  ucSeacrhVendor1.IsParentRequired = true;
            //ucSeacrhVendor1.CountryID = -1;
            //  ucSeacrhVendor1.FillVendor();
            txtDiscrepancyDateRangeFrom.Attributes.Add("readonly", "readonly");
            txtDiscrepancyDateRangeTo.Attributes.Add("readonly", "readonly");
        }
    }

    protected void Page_InIt(object sender, EventArgs e)
    {
        //  ucSeacrhVendor1.IsParentRequired = true;
        //  ucSeacrhVendor1.IsStandAloneRequired = true;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        ucExportToExcel1.CurrentPage = this;
        ucExportToExcel1.GridViewControl = gvDisLog;
        ucExportToExcel1.IsAutoGenratedGridview = true;
        ucExportToExcel1.FileName = "ItemDetailDiscrepancyReport";

        Label lblModuleText = (Label)Page.Master.FindControl("lblModuleText");
        if (mvDiscrepancyList.ActiveViewIndex == 0)
        {
            lblModuleText.Text = WebCommon.getGlobalResourceValue("DiscrepanciesSearchCriteria"); // "Discrepancies - Search Criteria";
            lblSearchResultT.Text = WebCommon.getGlobalResourceValue("SearchCriteria"); // "Search Criteria";
            Session["PageMode"] = "Search";
        }
        //else {
        //    lblModuleText.Text = WebCommon.getGlobalResourceValue("DiscrepanciesSearchResult"); // "Discrepancies - Search Result";
        //    lblSearchResultT.Text = WebCommon.getGlobalResourceValue("SearchResult"); // "Search Result";
        //}

        if (!IsPostBack)
        {
            btnReturntoSearchCriteria.Visible = false;
            mvDiscrepancyList.ActiveViewIndex = 0;
            BindDiscrepancyType();
            BindSiteList();
            //BindStockPlanners();

            //coming from the add image page
            if (GetQueryStringValue("FromPage") != null && GetQueryStringValue("FromPage").ToString().Trim() == "images"
                || Request.QueryString["FromPage"] != null && Request.QueryString["FromPage"].ToString().Trim() == "images"
                || GetQueryStringValue("PreviousPage") != null && GetQueryStringValue("PreviousPage").ToString() == "usercontrol"
                || GetQueryStringValue("PreviousPage") != null && GetQueryStringValue("PreviousPage").ToString() == "SearchResult")
            {

                mvDiscrepancyList.ActiveViewIndex = 1;
                btnSearch.Visible = false;

                /* This will call in case new discrepancy creation. */
                if (GetQueryStringValue("SiteId") != null && GetQueryStringValue("DisDate") != null)
                {
                    Session["PageMode"] = "Todays";
                    lblModuleText.Text = WebCommon.getGlobalResourceValue("TodaysDiscrepancies");
                    lblSearchResultT.Text = WebCommon.getGlobalResourceValue("TodaysDiscrepancies");
                    ViewState["TodayDiscrepancy"] = "TodayDiscrepancy";
                    this.BindGrid();

                }
                else
                {
                    Session["PageMode"] = "Search";
                    lblModuleText.Text = WebCommon.getGlobalResourceValue("DiscrepanciesSearchResult");
                    lblSearchResultT.Text = WebCommon.getGlobalResourceValue("SearchResult");

                    if (Session["SearchCriteria"] != null)
                        this.BindGrid((DiscrepancyBE)Session["SearchCriteria"]);
                    else
                        this.BindGrid();

                }
            }
        }
        if (GetQueryStringValue("AdminSearchPage") != null && GetQueryStringValue("DicrepancyNo") != null)
        {

            mvDiscrepancyList.ActiveViewIndex = 1;
            lblModuleText.Text = WebCommon.getGlobalResourceValue("DiscrepanciesSearchResult");
            lblSearchResultT.Text = WebCommon.getGlobalResourceValue("SearchResult");
            btnSearch.Visible = false;
            btnReturntoSearchCriteria.Visible = true;
            txtDiscrepancyNo.Text = GetQueryStringValue("VDRNO");
            this.BindGrid();
            var DiscrepancyRaiseMsg = GetQueryStringValue("DicrepancyNo");
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('" + DiscrepancyRaiseMsg + "')", true);

        }
        else if (GetQueryStringValue("AdminSearchPage") != null && GetQueryStringValue("VDRNO") != null)
        {
            txtDiscrepancyNo.Text = GetQueryStringValue("VDRNO");
            mvDiscrepancyList.ActiveViewIndex = 1;
            lblModuleText.Text = WebCommon.getGlobalResourceValue("DiscrepanciesSearchResult");
            lblSearchResultT.Text = WebCommon.getGlobalResourceValue("SearchResult");
            btnSearch.Visible = false;
            btnReturntoSearchCriteria.Visible = true;
            this.BindGrid();
        }

    }

    #region SearchCriteria
    private void BindDiscrepancyType()
    {
        DiscrepancyBE oNewDiscrepancyBE = new DiscrepancyBE();
        DiscrepancyBAL oNewDiscrepancyBAL = new DiscrepancyBAL();

        oNewDiscrepancyBE.Action = "GetDiscrepancyType";


        List<DiscrepancyBE> lstDiscrepancyType = oNewDiscrepancyBAL.GetDiscrepancyTypeBAL(oNewDiscrepancyBE);

        if (lstDiscrepancyType != null)
        {
            lstDiscrepancyType.RemoveAll(a => a.DiscrepancyType == "Presentation Issue" || a.DiscrepancyType == "Incorrect Delivery Address" || a.DiscrepancyType == "Fail Pallet Specification"|| a.DiscrepancyType == "Premature Invoice Receipt"|| a.DiscrepancyType == "Generic Discrepancy");
            FillControls.FillDropDown(ref ddlDiscrepancyType, lstDiscrepancyType, "DiscrepancyType", "DiscrepancyTypeID");
            
        }

        oNewDiscrepancyBAL = null;
    }

    private void BindSiteList()
    {
        if (GetQueryStringValue("SiteId") == null)
        {
            MAS_SiteBE oMAS_SiteBE = new MAS_SiteBE();
            MAS_SiteBAL oMAS_SiteBAL = new MAS_SiteBAL();
            oMAS_SiteBE.Action = "ShowAll";

            oMAS_SiteBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
            oMAS_SiteBE.User.UserID = Convert.ToInt32(Session["UserID"]);
            oMAS_SiteBE.SiteCountryID = 0;
            List<MAS_SiteBE> lstSite = new List<MAS_SiteBE>();
            lstSite = oMAS_SiteBAL.GetSiteMisSettingBAL(oMAS_SiteBE);
            if (lstSite.Count > 0 && lstSite != null)
            {
                FillControls.FillListBox(ref UclstSiteList, lstSite, "SiteDescription", "SiteID");
            }
            oMAS_SiteBAL = null;
        }

    }

    //private void BindStockPlanners() {
    //    DiscrepancyBE oNewDiscrepancyBE = new DiscrepancyBE();
    //    DiscrepancyBAL oNewDiscrepancyBAL = new DiscrepancyBAL();

    //    oNewDiscrepancyBE.Action = "GetStockPlanners";


    //    List<DiscrepancyBE> lstStockPlanners = oNewDiscrepancyBAL.GetStockPlannersBAL(oNewDiscrepancyBE);
    //    oNewDiscrepancyBAL = null;
    //    if (lstStockPlanners != null) {
    //        FillControls.FillDropDown(ref ddlStockPlanner, lstStockPlanners.OrderBy(sp => sp.StockPlannerNO).ToList(), "StockPlannerNo", "UserID", "---Select---");
    //    }

    //}


    protected void btnSearch_Click(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(txtSearchBySkuText.Text) || !string.IsNullOrWhiteSpace(txtSearchBySkuText.Text))
        {
            if (!txtSearchBySkuText.Text.IsNumeric())
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + PleaseEnterNumericValueForSearchBySku + "')", true);
                return;
            }
        }

        mvDiscrepancyList.ActiveViewIndex = 1;
        Label lblModuleText = (Label)Page.Master.FindControl("lblModuleText");
        lblModuleText.Text = WebCommon.getGlobalResourceValue("DiscrepanciesSearchResult");
        lblSearchResultT.Text = WebCommon.getGlobalResourceValue("SearchResult");
        btnSearch.Visible = false;
        btnReturntoSearchCriteria.Visible = true;
        this.BindGrid();
    }

    protected virtual void btnMoveRight_Click(object sender, EventArgs e)
    {
        if (UclstSiteList.SelectedItem != null)
        {
            FillControls.MoveOneItem(UclstSiteList, UclstSiteSelected);
        }
    }

    protected virtual void btnMoveLeft_Click(object sender, EventArgs e)
    {
        if (UclstSiteSelected.SelectedItem != null)
        {
            FillControls.MoveOneItem(UclstSiteSelected, UclstSiteList);
        }
    }

    //protected void btnScheduler_Click(object sender, EventArgs e) {
    //    System.Diagnostics.Process.Start(System.Configuration.ConfigurationManager.AppSettings["EscalationSchedulerExe"]);
    //}

    #endregion

    #region SearchListing
    private void BindGrid(DiscrepancyBE discrepancy = null)
    {
        oNewDiscrepancyBAL = new DiscrepancyBAL();
        List<DiscrepancyBE> lstDisLog = null;
        if (discrepancy == null)
        {
            oNewDiscrepancyBE = new DiscrepancyBE();
            oNewDiscrepancyBE.Action = "GetItemDetailDiscrepancyReport";

            if (GetQueryStringValue("SiteId") != null && GetQueryStringValue("DisDate") != null)
            {
                var DisDate = GetQueryStringValue("DisDate");
                oNewDiscrepancyBE.SCDiscrepancyDateFrom = string.IsNullOrEmpty(DisDate) ? (DateTime?)null : Common.GetMM_DD_YYYY(DisDate);
                oNewDiscrepancyBE.SCDiscrepancyDateTo = string.IsNullOrEmpty(DisDate) ? (DateTime?)null : Common.GetMM_DD_YYYY(DisDate);
                oNewDiscrepancyBE.UserID = Session["UserID"] != null ? Convert.ToInt32(Session["UserID"]) : (int?)null;
                oNewDiscrepancyBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
                oNewDiscrepancyBE.User.RoleTypeFlag = ddlActionPendingWithDropDown.SelectedItem.Value.Trim();
                oNewDiscrepancyBE.SCSelectedSiteIDs = GetQueryStringValue("SiteId");
            }
            else
            {
                //Search Creteria
                oNewDiscrepancyBE.SCDiscrepancyTypeID = Convert.ToInt32(ddlDiscrepancyType.SelectedItem.Value);
                oNewDiscrepancyBE.SCDiscrepancyNo = txtDiscrepancyNo.Text != string.Empty ? txtDiscrepancyNo.Text.Trim() : null;
                oNewDiscrepancyBE.SCPurchaseOrderNo = txtPurchaseOrder.Text != string.Empty ? txtPurchaseOrder.Text.Trim() : null;
                oNewDiscrepancyBE.SCPurchaseOrderDate = string.IsNullOrEmpty(txtPurchaseOrderDate.Text) ? (DateTime?)null : Common.GetMM_DD_YYYY(txtPurchaseOrderDate.Text);
                oNewDiscrepancyBE.SCDeliveryNoteNumber = txtDeliveryNote.Text != string.Empty ? txtDeliveryNote.Text.Trim() : null;
                oNewDiscrepancyBE.SCDiscrepancyDateFrom = string.IsNullOrEmpty(txtDiscrepancyDateRangeFrom.Text) ? (DateTime?)null : Common.GetMM_DD_YYYY(txtDiscrepancyDateRangeFrom.Text);
                oNewDiscrepancyBE.SCDiscrepancyDateTo = string.IsNullOrEmpty(txtDiscrepancyDateRangeTo.Text) ? (DateTime?)null : Common.GetMM_DD_YYYY(txtDiscrepancyDateRangeTo.Text);
                oNewDiscrepancyBE.UserID = Session["UserID"] != null ? Convert.ToInt32(Session["UserID"]) : (int?)null;
                oNewDiscrepancyBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
                oNewDiscrepancyBE.User.RoleTypeFlag = ddlActionPendingWithDropDown.SelectedItem.Value.Trim();

                //Site
                if (!string.IsNullOrEmpty(hiddenSelectedIDs.Value))
                {
                    oNewDiscrepancyBE.SCSelectedSiteIDs = hiddenSelectedIDs.Value.Trim(',');
                }
                else
                    oNewDiscrepancyBE.SCSelectedSiteIDs = null;

                oNewDiscrepancyBE.SCSelectedVendorIDs = msVendor.SelectedVendorIDs;
                oNewDiscrepancyBE.SCSelectedSPIDs = msStockPlanner.SelectedStockPlannerIDs;
                oNewDiscrepancyBE.DiscrepancyStatus = string.IsNullOrEmpty(ddlDiscrepancyStatus.SelectedValue) ? null : ddlDiscrepancyStatus.SelectedValue;
                oNewDiscrepancyBE.QueryDiscrepancy = new DISLog_QueryDiscrepancyBE();
                oNewDiscrepancyBE.QueryDiscrepancy.Query = ddlQueryStatus.SelectedValue;
                oNewDiscrepancyBE.EscalationLevel = ddlEscalation.SelectedIndex != 0 ? ddlEscalation.SelectedItem.Value : string.Empty;
                oNewDiscrepancyBE.SearchByODSku = txtSearchBySkuText.Text;
                oNewDiscrepancyBE.SearchByCatCode = txtSearchByCatCodeText.Text;
            }

            DataSet ds = new DataSet();
            ds = oNewDiscrepancyBAL.GetItemDetailDiscrepancyReportBAL(oNewDiscrepancyBE);
            if (ds.Tables[0].Rows.Count > 0)
            {
                gvDisLog.DataSource = ds;
                gvDisLog.DataBind();
                ViewState["lstSites"] = lstDisLog;
                Session["SearchCriteria"] = oNewDiscrepancyBE;
                hiddenSelectedIDs.Value = string.Empty;
            }
            else
            {
                gvDisLog.DataSource = null;
                gvDisLog.DataBind();
                
            }
        }
        else
        {
            mvDiscrepancyList.ActiveViewIndex = 1;
            btnSearch.Visible = false;
            btnReturntoSearchCriteria.Visible = true;

            if (discrepancy.SCDiscrepancyTypeID != null)
                if (discrepancy.SCDiscrepancyTypeID > 0)
                    ddlDiscrepancyType.SelectedIndex = ddlDiscrepancyType.Items.IndexOf(ddlDiscrepancyType.Items.FindByValue(Convert.ToString(discrepancy.SCDiscrepancyTypeID)));

            if (!string.IsNullOrEmpty(discrepancy.SCDiscrepancyNo))
                txtDiscrepancyNo.Text = discrepancy.SCDiscrepancyNo;

            if (!string.IsNullOrEmpty(discrepancy.SCPurchaseOrderNo))
                txtPurchaseOrder.Text = discrepancy.SCPurchaseOrderNo;

            if (discrepancy.SCPurchaseOrderDate != null)
                txtPurchaseOrderDate.Text = Convert.ToString(discrepancy.SCPurchaseOrderDate);

            if (!string.IsNullOrEmpty(discrepancy.SCDeliveryNoteNumber))
                txtDeliveryNote.Text = discrepancy.SCDeliveryNoteNumber;

            if (discrepancy.SCDiscrepancyDateFrom != null)
                txtDiscrepancyDateRangeFrom.Text = Convert.ToString(discrepancy.SCDiscrepancyDateFrom);

            if (discrepancy.SCDiscrepancyDateTo != null)
                txtDiscrepancyDateRangeTo.Text = Convert.ToString(discrepancy.SCDiscrepancyDateTo);

            if (!string.IsNullOrEmpty(discrepancy.User.RoleTypeFlag))
                ddlActionPendingWithDropDown.SelectedIndex =
                    ddlActionPendingWithDropDown.Items.IndexOf(ddlActionPendingWithDropDown.Items.FindByValue(Convert.ToString(discrepancy.User.RoleTypeFlag)));

            if (!string.IsNullOrEmpty(discrepancy.SCSelectedSiteIDs))
                hiddenSelectedIDs.Value = discrepancy.SCSelectedSiteIDs;

            if (!string.IsNullOrEmpty(discrepancy.SCSelectedVendorIDs))
                msVendor.SelectedVendorIDs = discrepancy.SCSelectedVendorIDs;

            //if (discrepancy.SCSPUserID != null)
            //    if (discrepancy.SCSPUserID > 0)
            //        ddlStockPlanner.SelectedIndex = ddlStockPlanner.Items.IndexOf(ddlStockPlanner.Items.FindByValue(Convert.ToString(discrepancy.SCSPUserID)));

            if (!string.IsNullOrEmpty(discrepancy.DiscrepancyStatus))
                ddlDiscrepancyStatus.SelectedIndex = ddlDiscrepancyStatus.Items.IndexOf(ddlDiscrepancyStatus.Items.FindByValue(Convert.ToString(discrepancy.DiscrepancyStatus)));

            if (!string.IsNullOrEmpty(discrepancy.QueryDiscrepancy.Query))
                ddlDiscrepancyStatus.SelectedIndex = ddlDiscrepancyStatus.Items.IndexOf(ddlDiscrepancyStatus.Items.FindByValue(discrepancy.QueryDiscrepancy.Query));

            if (!string.IsNullOrEmpty(discrepancy.EscalationLevel))
                ddlEscalation.SelectedIndex = ddlEscalation.Items.IndexOf(ddlEscalation.Items.FindByValue(Convert.ToString(discrepancy.EscalationLevel)));

            if (!string.IsNullOrEmpty(discrepancy.SearchByODSku))
                txtSearchBySkuText.Text = discrepancy.SearchByODSku;

            if (!string.IsNullOrEmpty(discrepancy.SearchByCatCode))
                txtSearchByCatCodeText.Text = discrepancy.SearchByCatCode;

            lstDisLog = oNewDiscrepancyBAL.GetDiscrepancyLogDetailsBAL(discrepancy);
            Session["SearchCriteria"] = discrepancy;
        }

        if (lstDisLog != null)
        {
            gvDisLog.DataSource = lstDisLog;
            gvDisLog.DataBind();
            ViewState["lstSites"] = lstDisLog;
        }

        if (gvDisLog.Rows.Count > 0)
        {
            //tblGrid.Visible = true;
            ucExportToExcel1.Visible = true;
        }
        else
        {
           // tblGrid.Visible = false;
            ucExportToExcel1.Visible = false;
        }

        oNewDiscrepancyBAL = null;
    }

    protected void gvDisLog_RowDataBound(object sender, GridViewRowEventArgs e)
    {
      
    }

    public override BusinessEntities.BaseBe[] SubSort(GridViewSortEventArgs e)
    {
        return Utilities.GenericListHelper<DiscrepancyBE>.SortList((List<DiscrepancyBE>)ViewState["lstSites"], e.SortExpression, e.SortDirection).ToArray();
    }

    protected void btnReturntoSearchCriteria_Click(object sender, EventArgs e)
    {
        mvDiscrepancyList.ActiveViewIndex = 0;
        btnSearch.Visible = true;
        btnReturntoSearchCriteria.Visible = false;
        Label lblModuleText = (Label)Page.Master.FindControl("lblModuleText");
        lblModuleText.Text = WebCommon.getGlobalResourceValue("DiscrepanciesSearchResult");
        lblSearchResultT.Text = WebCommon.getGlobalResourceValue("SearchResult");

        HiddenField hdnSelectedVendorId = (HiddenField)msVendor.FindControl("hiddenSelectedIDs");
        hdnSelectedVendorId.Value = string.Empty;
        hiddenSelectedIDs.Value = string.Empty;
        hiddenSelectedName.Value = string.Empty;
        Session["SearchCriteria"] = null;
    }
    #endregion
}
﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master" AutoEventWireup="true" CodeFile="DIS_ReassignDiscrepancy.aspx.cs" Inherits="DIS_ReassignDiscrepancy" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectVendorForCountry.ascx" TagName="MultiSelectVendor"
    TagPrefix="cc2" %>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectStockPlannerNew.ascx" TagName="MultiSelectStockPlanner"
    TagPrefix="cc1" %>
<%@ Register Src="../../CommonUI/UserControls/ucCountry.ascx" TagName="ucCountry"
    TagPrefix="uc1" %>
    <%@ Register Src="~/CommonUI/UserControls/MultiSelectAPSearch.ascx" TagName="MultiSelectAccountPayable" TagPrefix="cc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <script type="text/javascript">
        function CheckUncheckVendor(obj) {
            var rouNum = $(obj).parents().find('input[type="hidden"]').val();
            if (rouNum != '') {
                if (!obj.checked) {
                    var ids = $('#<%= hdnFldSelectedValues.ClientID %>').val();
                    ids = ids.replace(rouNum + ',', "");
                    ids = ids.replace(rouNum, "");
                    $('#<%= hdnFldSelectedValues.ClientID %>').val(ids);
                }
                else {
                    var ids = $('#<%= hdnFldSelectedValues.ClientID %>').val();
                    if (ids != '') {
                        ids += ',' + rouNum;
                    }
                    else
                        ids = rouNum;
                    $('#<%= hdnFldSelectedValues.ClientID %>').val(ids);

                   }
            }
           
           
        }
        function CheckSelect()
        {
            var hdnFldSelectedValues = $('#<%= hdnFldSelectedValues.ClientID %>').val();
            var gridCheckedCount=0;
            if (hdnFldSelectedValues == '') {
                $("#<%=gdvDiscrepancy.ClientID%> input[id*='chkSelect']:checkbox").each(function (index) {
                    if ($(this).is(':checked'))
                        gridCheckedCount++;
                });
                if (gridCheckedCount < 1) {
                    alert('<%=SelectOneDiscrepancy%>');
                    return false;
                }
                else {
                    if (confirm('<%=AreuSureMessage%>'))
                        return true;
                    else
                        return false;
                }
            }
            else
            {
                if (confirm('<%=AreuSureMessage%>'))
                    return true;
                else
                    return false;
            }
        }
        $(document).ready(function () {
            $("body").keypress(function (e) {
                if (e.which == 13 || e.keyCode == 13) {
                    $('#<%=btnSearch.ClientID %>').click();
                    return false;
                }
            });
        });
    </script>
     <asp:ScriptManager ID="sm1" runat="server">
    </asp:ScriptManager>
      <h2>
        <cc1:ucLabel ID="lblReassignDiscrepanciesTitle" Text="" runat="server"></cc1:ucLabel>
    </h2>
     <asp:HiddenField ID="hdnFldSelectedValues" runat="server" />
     <div class="right-shadow">
        <div class="formbox">
             <cc1:ucPanel ID="pnlSearchScreen" runat="server">
              <div class="button-row">
                 <table width="90%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                    
                     <tr>
                        <td style="font-weight: bold; width:20%;text-align:left">
                           <cc1:ucRadioButton  id="rdoInventory" runat="server" GroupName="abc" 
                                checked="true"   OnCheckedChanged="rdoInventory_CheckedChanged" 
                                AutoPostBack="True"  />      
                        </td>
                        <td>
                        </td>
                        <td style="font-weight: bold; width:30%;text-align:left"> 
                             <cc1:ucRadioButton  id="rdoAccountsPayable" runat="server" GroupName="abc" 
                               OnCheckedChanged="rdoAccountsPayable_CheckedChanged" AutoPostBack="True" />   
                        </td>
                        
                     </tr>

                     <tr>
                        <td style="font-weight: bold; width:20%;text-align:left">
                            <cc1:ucLabel ID="lblCountry" runat="server" Text="Country"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold; width: 1%">
                            :
                        </td>
                        <td align="left">
                            <uc1:ucCountry ID="ddlCountry" runat="server" width="150px" />
                        </td>
                        <td style="font-weight: bold; width:30%;text-align:right"> <cc1:ucLabel ID="lblDiscrepancy" runat="server" Text="Country"></cc1:ucLabel></td>
                         <td style="font-weight: bold; width: 1%">
                              :
                         </td>
                         <td align="left">
                             <cc1:ucTextbox ID="txtDiscrepancy" width="150px" runat="server"></cc1:ucTextbox>
                         </td>
                     </tr>
                     
                     
                     <tr id="trStockPlanner" runat="server" visible="true">
                         <td style="font-weight: bold; width:20%;text-align:left">
                            <cc1:ucLabel ID="lblStockPlanner" runat="server" Text="Stock Planner"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="Label17" runat="server">:</cc1:ucLabel>
                        </td>
                         <td style="font-weight: bold;" colspan="4">
                             

                                       <cc1:MultiSelectStockPlanner runat="server" ID="msStockPlanner" />

                             </td>
                        </tr>
                         <tr id="trAccountPayable" runat="server" visible="false">
                         <td style="font-weight: bold; width:20%;text-align:left">
                            <cc1:ucLabel ID="lblAccountPayable" runat="server" Text=""></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="UcLabel2" runat="server">:</cc1:ucLabel>
                        </td>
                         <td style="font-weight: bold;" colspan="4">
                             

                                      <cc2:MultiSelectAccountPayable runat="server" ID="msAccountPayable" />

                             </td>
                        </tr>
                        <tr>
                         <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblVendor" runat="server" Text="Vendor"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            :
                        </td>
                            <td style="font-weight: bold;" colspan="4">
                              
                                <cc2:MultiSelectVendor runat="server" ID="msVendor" />
                                            
                                      
                            </td>
                        </tr>
                     <tr>
                         <td colspan="6">
                              <cc1:ucButton ID="btnSearch" runat="server" Text="Search" CssClass="button" OnClick="btnSearch_Click" />
                         </td>
                     </tr>
                     </table>
                </div>
               </cc1:ucPanel>
            <cc1:ucPanel ID="pnlPotentialOutput" Visible="false" runat="server">
                <table width="100%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                <tr><td colspan="3" align="center"><asp:Label ID="lblmsg" runat="server"></asp:Label> </td></tr>
                <tr>
                    <td colspan="3">
                        <cc1:ucGridView ID="gdvDiscrepancy" OnRowDataBound="gdvDiscrepancy_RowDataBound" OnPageIndexChanging="gdvDiscrepancy_PageIndexChanging" Width="100%" runat="server" CssClass="grid"
                            AllowPaging="true" PageSize="50" EmptyDataText="No Record Found" EmptyDataRowStyle-HorizontalAlign="Center" AllowSorting="true" OnSorting="SortGrid">
                            <Columns>
                                <asp:TemplateField HeaderText="SelectAllCheckBox">
                                    <HeaderStyle HorizontalAlign="Left" Width="100px" />
                                    <ItemStyle Width="100px" HorizontalAlign="Left" VerticalAlign="Middle" />
                                    <HeaderTemplate>
                                        <cc1:ucCheckbox runat="server" ID="chkSelectAllText" CssClass="checkbox-input" OnCheckedChanged="chkSelectAllText_CheckedChanged"  AutoPostBack="true" />
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:CheckBox runat="server" ViewStateMode="Enabled" ID="chkSelect" onclick="CheckUncheckVendor(this);"  />
                                        <asp:HiddenField ID="hdndiscrepancyLogid" runat="server" Value='<%# Eval("DiscrepancyLogID") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Discrepancy" SortExpression="VDRNo">
                                    <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <cc1:ucLabel ID="lbldiscrepancy" runat="server" Text='<%# Eval("VDRNo") %>'></cc1:ucLabel>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Vendor" SortExpression="VendorNoName">
                                    <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <cc1:ucLabel ID="lblVendorNoValue" runat="server" Text='<%# Eval("VendorNoName") %>'></cc1:ucLabel>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Type" SortExpression="DiscrepancyType">
                                    <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <cc1:ucLabel ID="lbldiscrepancyType" runat="server" Text='<%# Eval("DiscrepancyType") %>'></cc1:ucLabel>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="StockPlanner" SortExpression="StockPlannerName">
                                    <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <cc1:ucLiteral ID="ltstockPlanner" Text='<%# Eval("StockPlannerName") %>'
                                            runat="server" />

                                             <cc1:ucLiteral ID="ltAccountPaybleName" Visible="false" Text='<%# Eval("AccountPaybleName") %>'
                                            runat="server" />
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                            </Columns>
                        </cc1:ucGridView>
                    </td>
                </tr>
                    <tr id="trAssign" runat="server">
                        <td width="80%">&nbsp;</td>
                        <td width="15%" style="font-weight: bold"><cc1:ucLabel ID="lblReassignto" runat="server" Text=""></cc1:ucLabel></td>
                        <td width="5%">
                            <cc1:ucDropdownList ID="ddlStockPlanner" Width="200px" runat="server"></cc1:ucDropdownList>
                        </td>
                    </tr>
                    <tr>
                         <td colspan="2">&nbsp;</td>
                        <td align="right">
                            <cc1:ucButton ID="UcButton1" runat="server"  OnClick="UcButton1_Click" Width="120px" Text="Reassign" CssClass="button" OnClientClick="return CheckSelect();" />
                            &nbsp;<cc1:ucButton ID="btnBack_1" runat="server" Text="Back" CssClass="button"  OnClick="btnBack_1_Click" />
                        </td>
                    </tr>
            </table>
            </cc1:ucPanel>
            </div>
         </div>
</asp:Content>


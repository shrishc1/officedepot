﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    CodeFile="DIS_MediatorSetup.aspx.cs" Inherits="DIS_MediatorSetup" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="../../CommonUI/UserControls/ucExportToExcel.ascx" TagName="ucExportToExcel"
    TagPrefix="cc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2>
        <cc1:ucLabel ID="lblMediatorSetup" runat="server"></cc1:ucLabel>
    </h2>
    <div class="button-row">
        <cc2:ucExportToExcel ID="ucExportToExcel1" runat="server" />
    </div>
    <table width="100%">
        <tr>
            <td align="center">
                <cc1:ucGridView ID="grdMediator" Width="100%" runat="server" CssClass="grid" OnSorting="SortGrid"
                    AllowSorting="true">
                    <Columns>
                        <asp:TemplateField HeaderText="Country" SortExpression="Country.CountryName">
                            <HeaderStyle HorizontalAlign="Left" Width="10%" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <asp:HyperLink ID="hpcountry" runat="server" Text='<%# Eval("Country.CountryName") %>'
                                    NavigateUrl='<%# EncryptQuery("DIS_MediatorSetupEdit.aspx?CountryID="+ Eval("Country.CountryID") + "&CountryName=" + Eval("Country.CountryName") ) %>'></asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Mediator" SortExpression="Mediator">
                            <HeaderStyle Width="10%" HorizontalAlign="Left" />
                            <ItemTemplate>
                                <cc1:ucLiteral ID="ltMediator" runat="server" Text='<%# Eval("User.Username") %>'></cc1:ucLiteral>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:TemplateField>
                    </Columns>
                </cc1:ucGridView>
            </td>
        </tr>
    </table>
</asp:Content>

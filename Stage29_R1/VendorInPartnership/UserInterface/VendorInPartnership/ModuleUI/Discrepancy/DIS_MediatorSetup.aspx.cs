﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLogicLayer.ModuleBAL.Discrepancy;
using BusinessEntities.ModuleBE.Discrepancy;

public partial class DIS_MediatorSetup : CommonPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack) {
            BindMediator();
        }
        ucExportToExcel1.GridViewControl = grdMediator;
        ucExportToExcel1.FileName = "MediatorOverView";
    }

    private void BindMediator() {
        MediatorBE oMediatorBE = new MediatorBE();
        MediatorBAL oMediatorBAL = new MediatorBAL();

        oMediatorBE.Action = "ShowAll";

        List<MediatorBE> lstMediatorBE = oMediatorBAL.GetMediatorBAL(oMediatorBE);

        if (lstMediatorBE != null && lstMediatorBE.Count > 0) {
            grdMediator.DataSource = lstMediatorBE;
            grdMediator.DataBind();
            ViewState["lstMediatorBE"] = lstMediatorBE;
        }     
    }

    public override BusinessEntities.BaseBe[] SubSort(GridViewSortEventArgs e) {
        return Utilities.GenericListHelper<MediatorBE>.SortList((List<MediatorBE>)ViewState["lstMediatorBE"], e.SortExpression, e.SortDirection).ToArray();
    }
}
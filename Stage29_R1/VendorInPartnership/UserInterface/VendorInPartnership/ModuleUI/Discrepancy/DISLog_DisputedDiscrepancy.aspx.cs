﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.Discrepancy.LogDiscrepancy;
using BusinessLogicLayer.ModuleBAL.Discrepancy.LogDiscrepancy;

public partial class DISLog_DisputedDiscrepancy : CommonPage
{
    #region Local Declarations ...
    #endregion

    #region Events ...

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            UIUtility.PageValidateForODUser();
            this.BindDisputedDiscrepancy();
        }
    }

    protected void gvDisputedDiscrepancy_RowDataBound(object sender, GridViewRowEventArgs e) 
    {
        if (e.Row.RowType != DataControlRowType.Header && e.Row.RowType != DataControlRowType.Footer)
        {
            var hypLinkToDisDetails = (HyperLink)e.Row.FindControl("hypLinkToDisDetails");
            var hdnDisLogID = (HiddenField)e.Row.FindControl("hdnDisLogID");
            var hdnQueryDisId = (HiddenField)e.Row.FindControl("hdnQueryDisId");            
            if (hypLinkToDisDetails != null)
            {
                hypLinkToDisDetails.NavigateUrl = EncryptQuery("DISLog_QueryDiscrepancy.aspx?disLogID=" + hdnDisLogID.Value
                    + "&VDRNo=" + hypLinkToDisDetails.Text + "&QueryDisID=" + hdnQueryDisId.Value);
            }
        }
    }

    #endregion

    #region Methods ...

    public override BusinessEntities.BaseBe[] SubSort(GridViewSortEventArgs e)
    {
        return Utilities.GenericListHelper<DiscrepancyBE>.SortList((List<DiscrepancyBE>)ViewState["lstDisputedDisc"], e.SortExpression, e.SortDirection).ToArray();
    }

    private void BindDisputedDiscrepancy()
    {
        var queryDiscrepancyBAL = new DISLog_QueryDiscrepancyBAL();
        var discrepancyBE = new DiscrepancyBE();
        discrepancyBE.Action = "GetDisputedDiscrepancy";
        discrepancyBE.UserID = Convert.ToInt32(Session["UserID"]);
        var lstDisputedDisc = queryDiscrepancyBAL.GetDiscrepancyLogDetailsBAL(discrepancyBE);
        if (lstDisputedDisc != null && lstDisputedDisc.Count > 0)
        {
            gvDisputedDiscrepancy.DataSource = lstDisputedDisc;
            ViewState["lstDisputedDisc"] = lstDisputedDisc;
        }
        else
        {
            gvDisputedDiscrepancy.DataSource = null;
            ViewState["lstDisputedDisc"] = null;
        }

        gvDisputedDiscrepancy.DataBind();         
    }

    #endregion    
}
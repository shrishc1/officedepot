﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ModuleUI_Discrepancy_PriceQuery_DISPrice_workflow : CommonPage
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void NextClick(object sender, EventArgs e)
    {
        Response.Redirect("DISPriceVendor_Query.aspx");
    }

    protected void BackClick(object sender, EventArgs e)
    {
        Response.Redirect("DISPriceProcur_Entry.aspx");
    }
}
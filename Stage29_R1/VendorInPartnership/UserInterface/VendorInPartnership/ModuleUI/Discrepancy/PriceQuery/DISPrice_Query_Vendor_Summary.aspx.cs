﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


public class GridPQByVendorSummary2
{
    public string MonthOrYear { get; set; }
    public string Vendor { get; set; }
    public string Debits { get; set; }
    public string Actioned { get; set; }
    public string Approved { get; set; }
    public string Cancelled { get; set; }
    public string Outstanding { get; set; }
    public string TotalValue { get; set; }
    public string ValueDebited { get; set; }
    public string ValueCancelled { get; set; }
    public string NotActionedYet { get; set; }

}

public partial class ModuleUI_Discrepancy_PriceQuery_DISPrice_Query_Vendor_Summary : CommonPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        List<GridPQByVendorSummary2> lst = new List<GridPQByVendorSummary2>();
        GridPQByVendorSummary2 obj = new GridPQByVendorSummary2
        {
            MonthOrYear = "2015 Jan",
            Vendor = "012-CPD",
            Debits = "391",
            Actioned = "388",
            Approved = "120",
            Cancelled = "265",
            Outstanding = "3",
            TotalValue = "12319.34",
            ValueDebited = "4091.44",
            ValueCancelled = "7188.51",
            NotActionedYet = "1039.39",

        };
        lst.Add(obj);

        obj = new GridPQByVendorSummary2
        {
            MonthOrYear = "2015 Feb",
            Vendor = "UKC001-CPD",
            Debits = "87",
            Actioned = "64",
            Approved = "40",
            Cancelled = "24",
            Outstanding = "23",
            TotalValue = "938.35",
            ValueDebited = "612.43",
            ValueCancelled = "191.32",
            NotActionedYet = "134.6",

        };
        lst.Add(obj);

        grdPQByVendorSummary.DataSource = lst;
        grdPQByVendorSummary.DataBind();
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        Response.Redirect("DISPrice_Query_Reports.aspx");
    }
}
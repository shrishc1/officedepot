﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    CodeFile="DISPriceReassign_Search.aspx.cs" Inherits="ModuleUI_Discrepancy_PriceQuery_DISPriceReassign_Search" %>

<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register TagName="ucDate" TagPrefix="cc2" Src="~/CommonUI/UserControls/ucDate.ascx" %>
<%@ Register Src="../UserControl/ucSDRCommunication.ascx" TagName="ucSDRCommunication"
    TagPrefix="uc2" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectVendor.ascx" TagName="MultiSelectVendor"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectName.ascx" TagName="MultiSelectName"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectAP.ascx" TagName="MultiSelectAP"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectAPSearch.ascx" TagName="MultiSelectAPSearch"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectSite.ascx" TagName="MultiSelectSite"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectSKU.ascx" TagName="MultiSelectSKU"
    TagPrefix="cc1" %>
<%@ Register Src="../../../CommonUI/UserControls/ucSite.ascx" TagName="ucSite" TagPrefix="cc2" %>
<%@ Register Src="../UserControl/APAction/APAction.ascx" TagName="APAction" TagPrefix="uc2" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectVendor.ascx" TagName="MultiSelectVendor"
    TagPrefix="cc3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script language="javascript" src="../../../Scripts/JSON2.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">

    </script>
    <asp:ScriptManager ID="sp1" runat="server">
    </asp:ScriptManager>
    <asp:HiddenField ID="hdnSiteID" runat="server" />
    <asp:UpdatePanel ID="up1" runat="server">
        <ContentTemplate>
            <h2>
                <cc1:ucLabel ID="lblReassignPriceQuerySearch" runat="server"></cc1:ucLabel>
            </h2>
            <div class="right-shadow">
                <div class="formbox">
                    <cc1:ucMultiView ID="mvOvers" runat="server" ActiveViewIndex="0">
                        <cc1:ucView ID="vwReassignPriceQurSearch" runat="server">
                             <table width="80%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                                <tr>
                                    <td style="font-weight: bold;">
                                        <cc1:ucLabel ID='lblCountry' runat="server">
                                        </cc1:ucLabel>
                                    </td>
                                    <td>
                                        :
                                    </td>
                                    <td>
                                        <cc1:ucDropdownList ID="ddlCountry" runat="server" Width="150px">
                                            <asp:ListItem Text="--Select--" Value="0">
                                            </asp:ListItem>
                                            <asp:ListItem Text="UKandIRE" Value="1">
                                            </asp:ListItem>
                                            <asp:ListItem Text="France" Value="2">
                                            </asp:ListItem>
                                        </cc1:ucDropdownList>
                                    </td>
                                    <td style="font-weight: bold;">
                                        <cc1:ucLabel ID='lblOpenWith' runat="server">
                                        </cc1:ucLabel>:
                                    </td>
                                    <td>
                                        :
                                    </td>
                                    <td>
                                        <cc1:ucDropdownList ID="ddlOpenWith" runat="server" Width="150px">
                                            <asp:ListItem Text="Account Payable" Value="1" selected="true">
                                            </asp:ListItem>
                                            <asp:ListItem Text="Procurement" Value="2">
                                            </asp:ListItem>
                                        </cc1:ucDropdownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="font-weight: bold;">
                                        <cc1:ucLabel ID='lblDebitNo' runat="server">
                                        </cc1:ucLabel>
                                    </td>
                                    <td>
                                        :
                                    </td>
                                    <td colspan="4">
                                        <cc1:ucTextbox ID='txtDebitNo' runat="server" Text="D01209129" Width="150px">
                                        </cc1:ucTextbox>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="font-weight: bold;">
                                        <cc1:ucLabel ID='lblProcOrApName' runat="server">
                                        </cc1:ucLabel>
                                    </td>
                                    <td>
                                        :
                                    </td>
                                    <td colspan="4">
                                        <cc1:MultiSelectName runat="server" ID="MultiSelectName" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="font-weight: bold;">
                                        <cc1:ucLabel ID='lblVendor' runat="server">
                                        </cc1:ucLabel>
                                    </td>
                                    <td>
                                        :
                                    </td>
                                    <td colspan="4">
                                        <cc1:MultiSelectVendor runat="server" ID="msVendor" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="font-weight: bold;">
                                        <cc1:ucLabel ID='lblSKU' runat="server">
                                        </cc1:ucLabel>
                                    </td>
                                    <td>
                                        :
                                    </td>
                                    <td colspan="4">
                                        <cc1:MultiSelectSKU runat="server" ID="MultiSelectSKU" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="font-weight: bold;">
                                        <cc1:ucLabel ID='UcLabel3' runat="server" Text="CAT Code">
                                        </cc1:ucLabel>
                                    </td>
                                    <td>
                                        :
                                    </td>
                                    <td colspan="4">
                                        <cc1:MultiSelectSKU runat="server" ID="MultiSelectSKU1" />
                                    </td>
                                </tr>
                            </table>
                        </cc1:ucView>
                    </cc1:ucMultiView>
                </div>
            </div>
            <div class="button-row">
                <cc1:ucButton ID="btnSearch" OnClick="SearchClick" runat="server" class="button"
                    UseSubmitBehavior="false" />
            </div>
            <%--<asp:Button ID="btnBack" runat="server" Text="Back" OnClick="BackClick" />
            <asp:Button ID="Button1" runat="server" Text="Next" OnClick="NextClick" />--%>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

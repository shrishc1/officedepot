﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    CodeFile="DISPrice_Query_Reports_By_Date.aspx.cs" Inherits="ModuleUI_Discrepancy_PriceQuery_DISPrice_Query_Reports_By_Date" %>

<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register TagName="ucDate" TagPrefix="cc2" Src="~/CommonUI/UserControls/ucDate.ascx" %>
<%@ Register Src="../UserControl/ucSDRCommunication.ascx" TagName="ucSDRCommunication"
    TagPrefix="uc2" %>
<%@ Register Src="../../../CommonUI/UserControls/ucSite.ascx" TagName="ucSite" TagPrefix="cc2" %>
<%@ Register Src="../UserControl/APAction/APAction.ascx" TagName="APAction" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script language="javascript" src="../../../Scripts/JSON2.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">

    </script>
    <asp:ScriptManager ID="sp1" runat="server">
    </asp:ScriptManager>
    <asp:HiddenField ID="hdnSiteID" runat="server" />
    <asp:UpdatePanel ID="up1" runat="server">
        <ContentTemplate>
            <%--Grid 1--%>
            <h2>
                <cc1:ucLabel ID="lblAPSummaryReport" runat="server"></cc1:ucLabel>
            </h2>
            <br />
            <div>
                <table>
                    <tr>
                        <td style="font-weight: bold">
                            <cc1:ucLabel runat="server" ID="lblDateFrom" Text="Date From"></cc1:ucLabel>
                        </td>
                        <td>
                            :
                        </td>
                        <td>
                        <cc1:ucLabel runat="server" ID="UcLabel5" Text="25/11/201"></cc1:ucLabel>
                           <%-- <cc1:ucTextbox ID="txtFromDate" Text="25/11/2015" ClientIDMode="Static" runat="server"
                                onchange="setValue1(this)" ReadOnly="True" Width="70px" />--%>
                            <%--<cc1:ucLabel runat="server" ID="UcLabel4" Text="01/05/2015"></cc1:ucLabel>--%>
                        </td>
                        <td>
                            &nbsp;&nbsp;&nbsp;&nbsp;
                        </td>
                        <td style="font-weight: bold">
                            <cc1:ucLabel runat="server" ID="lblDateTo"></cc1:ucLabel>
                        </td>
                        <td>
                            :
                        </td>
                        <td>
                            <cc1:ucLabel runat="server" ID="UcLabel4" Text="30/11/201"></cc1:ucLabel>
                            <%--<cc1:ucTextbox ID="txtToDate" Text="30/11/2015" ClientIDMode="Static" runat="server"
                                onchange="setValue2(this)" ReadOnly="True" Width="70px" />--%>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold">
                            <cc1:ucLabel runat="server" ID="lblDepartment"></cc1:ucLabel>
                        </td>
                        <td>
                            :
                        </td>
                        <td>
                            Account Payable
                        </td>
                    </tr>
                </table>
            </div>
            <br />
            <div class="right-shadow">
                <div class="formbox">
                    <cc1:ucMultiView ID="mvOvers" runat="server" ActiveViewIndex="0">
                        <cc1:ucView ID="vwPriceDetail" runat="server">
                            <cc1:ucGridView ID="grdPriceQryTrackReport" Width="100%" runat="server" CssClass="grid">
                                <Columns>
                                    <asp:TemplateField HeaderText="AP Name">
                                        <HeaderStyle Width="8%" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hplImageName" runat="server" NavigateUrl="DISPriceUser_Reports.aspx?id=APSummary"
                                                Text='<%# Eval("APName") %>'></asp:HyperLink>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="# Debits">
                                        <HeaderStyle Width="8%" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="lblDebits" Text='<%# Eval("Debits") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Actioned">
                                        <HeaderStyle Width="8%" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="lblActioned" Text='<%# Eval("Actioned") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Approved">
                                        <HeaderStyle Width="8%" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="UcLabel1" Text='<%# Eval("Approved") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Cancelled">
                                        <HeaderStyle Width="8%" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="UcLabel2" Text='<%# Eval("Cancelled") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Outstanding">
                                        <HeaderStyle Width="8%" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="lblOutstanding" Text='<%# Eval("Outstanding") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Total Value">
                                        <HeaderStyle Width="8%" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="lblTotalValue" Text='<%# Eval("TotalValue") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Value Debited">
                                        <HeaderStyle Width="8%" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="UcLabel3" Text='<%# Eval("ValueDebited") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Value Cancelled">
                                        <HeaderStyle Width="8%" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="lblValueCancelled" Text='<%# Eval("ValueCancelled") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Not Actioned Yet">
                                        <HeaderStyle Width="8%" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="lblNotActionedYet" Text='<%# Eval("NotActionedYet") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                </Columns>
                            </cc1:ucGridView>
                        </cc1:ucView>
                    </cc1:ucMultiView>
                </div>
            </div>
            <br />
            <div class="button-row">
                <cc1:ucButton ID="btnBack" runat="server" class="button" OnClick="btnBack_Click" />
            </div>
            <br />
            <%--<asp:Button ID="btnBack" runat="server" Text="Back" OnClick="BackClick" />
            <asp:Button ID="Button1" runat="server" Text="Next" OnClick="NextClick" />--%>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

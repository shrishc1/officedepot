﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ModuleUI_Discrepancy_PriceQuery_DISPriceProcur_Entry : CommonPage
{
    
    protected void Page_Load(object sender, EventArgs e)
    {

        List<GridProcurEntry> lst = new List<GridProcurEntry>();
        GridProcurEntry obj = new GridProcurEntry
        {
            Text1 = "Hello",
            Text2 = "World",

            LineNo = "10",
            ODCode = "46589",
            VikCode = "46589",
            VendorCode = "DF46589",
            Desc = "2 in 1 Desk AND CLIP  FAN",
            OriginalPoQty = "123",
            OutstandingPoQty = "20",
            UOM = "",
            POItemCost = "7.19",
            PoTotalCost = "999",
            InvoicedItemPrice = "7.15",
            QtyInvoiced = "139",
            InvoicedCost = "998.85",
            PriceDiff = "5.56",
            Correctprice = "",

        };
        //lst.Add(obj);


        obj = new GridProcurEntry
        {
            
            LineNo = "40",
            ODCode = "121784",
            VikCode = "MM30135",
            VendorCode = "DF1210",
            Desc = "DESK FAN WE 3 SPEED 12IN",
            OriginalPoQty = "315",
            OutstandingPoQty = "20",
            UOM = "",
            POItemCost = "11.11",
            PoTotalCost = "3499.65",
            InvoicedItemPrice = "11.01",
            QtyInvoiced = "310",
            InvoicedCost = "3413.1",
            PriceDiff = "86.55",
            Correctprice = "PO",

        };
        lst.Add(obj);

        grdPurchaseOrderDetail.DataSource = lst;
        grdPurchaseOrderDetail.DataBind();
    }

    protected void NextClick(object sender, EventArgs e)
    {
        Response.Redirect("DISPriceDebit_Library.aspx");

    }

    protected void btnWorkflowClicked(object sender, EventArgs e)
    {
        Response.Redirect("DISPrice_workflow.aspx");

    }

    protected void BackClick(object sender, EventArgs e)
    {
        Response.Redirect("DISPriceSearch_Price_Query_Results.aspx");

    }
}

public class GridProcurEntry
{
    public string Text1 { get; set; }
    public string Text2 { get; set; }

    public string LineNo { get; set; }
    public string ODCode { get; set; }
    public string VikCode { get; set; }
    public string VendorCode { get; set; }
    public string Desc { get; set; }
    public string OriginalPoQty { get; set; }
    public string OutstandingPoQty { get; set; }
    public string UOM { get; set; }
    public string POItemCost { get; set; }
    public string PoTotalCost { get; set; }
    public string InvoicedItemPrice { get; set; }
    public string QtyInvoiced { get; set; }
    public string InvoicedCost { get; set; }
    public string PriceDiff { get; set; }
    public string Correctprice { get; set; }
}
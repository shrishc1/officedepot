﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    CodeFile="DISPriceVendor_Price_Query.aspx.cs" Inherits="ModuleUI_Discrepancy_PriceQuery_DISPriceVendor_Price_Query" %>

<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register TagName="ucDate" TagPrefix="cc2" Src="~/CommonUI/UserControls/ucDate.ascx" %>
<%@ Register Src="../UserControl/ucSDRCommunication.ascx" TagName="ucSDRCommunication"
    TagPrefix="uc2" %>
<%@ Register Src="../../../CommonUI/UserControls/ucSite.ascx" TagName="ucSite" TagPrefix="cc2" %>
<%@ Register Src="../UserControl/APAction/APAction.ascx" TagName="APAction" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script language="javascript" src="../../../Scripts/JSON2.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">

        function showDiv() {
            var control = document.getElementById("raiseQryActionDiv");

            //            if (control.style.display == "block" || control.style.display == "")
            //                control.style.display = "none";
            //            else
            control.style.display = "block";
        }

       
    </script>
    <asp:ScriptManager ID="sp1" runat="server">
    </asp:ScriptManager>
    <asp:HiddenField ID="hdnSiteID" runat="server" />
    <asp:UpdatePanel ID="up1" runat="server">
        <ContentTemplate>
            <h2 id="heading">
                <cc1:ucLabel ID="lblVendorPriceQueryView2" runat="server"></cc1:ucLabel>
            </h2>
            <h2>
                <cc1:ucLabel ID="UcLabel3" runat="server" Text="Price Query - PQ00000001"></cc1:ucLabel>
            </h2>
            <div class="right-shadow">
                <div class="formbox">
                    <cc1:ucMultiView ID="mvOvers" runat="server" ActiveViewIndex="0">
                        <cc1:ucView ID="vwPriceDetail" runat="server">
                            <div class="button-row">
                                <cc1:ucButton ID="btnRaiseQuery" OnClick="RaiseQryClick" runat="server"
                                    UseSubmitBehavior="false" class="button" />
                            </div>
                            <br />
                            <table width="100%" cellspacing="0" cellpadding="0" class="form-table">
                                <tr valign="top">
                                    <td width="30%">
                                        <cc1:ucPanel ID="pnlVendor" runat="server" CssClass="fieldset-form">
                                            <table width="100%" cellspacing="1" cellpadding="0" class="form-table" height="140px">
                                                <tr>
                                                    <td style="font-weight: bold;">
                                                        <cc1:ucLabel ID="lblVendor" runat="server" Text=""></cc1:ucLabel>
                                                    </td>
                                                    <td style="font-weight: bold;">
                                                        :
                                                    </td>
                                                    <td class="nobold">
                                                        <cc1:ucLabel ID="UcLabel2" runat="server" Text="455-PIK A PAK Home Electrical"></cc1:ucLabel>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="font-weight: bold;">
                                                        <cc1:ucLabel ID="lblContact" runat="server" Text=""></cc1:ucLabel>
                                                    </td>
                                                    <td style="font-weight: bold;">
                                                        :
                                                    </td>
                                                    <td class="nobold">
                                                        <cc1:ucLabel ID="UcLabel4" runat="server" Text="123456789"></cc1:ucLabel>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="font-weight: bold;">
                                                        <cc1:ucLabel ID="lblVendorVATHash" runat="server" Text=""></cc1:ucLabel>
                                                    </td>
                                                    <td style="font-weight: bold;">
                                                        :
                                                    </td>
                                                    <td class="nobold">
                                                        <cc1:ucLabel ID="UcLabel7" runat="server" Text="1234"></cc1:ucLabel>
                                                    </td>
                                                </tr>
                                            </table>
                                        </cc1:ucPanel>
                                    </td>
                                    <td width="2%">
                                        &nbsp;
                                    </td>
                                    <td width="35%">
                                        <cc1:ucPanel ID="pnlDebitReference" runat="server" CssClass="fieldset-form">
                                            <table width="100%" cellspacing="1" cellpadding="0" class="form-table" height="140px">
                                                <tr>
                                                    <td style="font-weight: bold;">
                                                        <cc1:ucLabel ID="lblSite_1" runat="server" Text=""></cc1:ucLabel>
                                                    </td>
                                                    <td style="font-weight: bold;">
                                                        :
                                                    </td>
                                                    <td class="nobold">
                                                        <cc1:ucLabel ID="UcLabel13" runat="server" Text="LEI-Leicester"></cc1:ucLabel>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="font-weight: bold;">
                                                        <cc1:ucLabel ID="lblPurchaseOrderNo" runat="server" Text=""></cc1:ucLabel>
                                                    </td>
                                                    <td style="font-weight: bold;">
                                                        :
                                                    </td>
                                                    <td class="nobold">
                                                        <cc1:ucLabel ID="UcLabel15" runat="server" Text="1234   11/16/2015"></cc1:ucLabel>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="font-weight: bold;">
                                                        <cc1:ucLabel ID="lblInvoice" runat="server" Text=""></cc1:ucLabel>
                                                    </td>
                                                    <td style="font-weight: bold;">
                                                        :
                                                    </td>
                                                    <td class="nobold">
                                                        <cc1:ucLabel ID="UcLabel10" runat="server" Text=""></cc1:ucLabel>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="font-weight: bold;">
                                                        <cc1:ucLabel ID="lblOfficeDepotVATHash" runat="server" Text=""></cc1:ucLabel>
                                                    </td>
                                                    <td style="font-weight: bold;">
                                                        :
                                                    </td>
                                                    <td class="nobold">
                                                        <cc1:ucLabel ID="UcLabel20" runat="server" Text=""></cc1:ucLabel>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="font-weight: bold;">
                                                        <cc1:ucLabel ID="lblAPAssociate" runat="server" Text=""></cc1:ucLabel>
                                                    </td>
                                                    <td style="font-weight: bold;">
                                                        :
                                                    </td>
                                                    <td class="nobold">
                                                        <cc1:ucLabel ID="UcLabel22" runat="server" Text="O Jura"></cc1:ucLabel>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="font-weight: bold;">
                                                        <cc1:ucLabel ID="lblProcurementContact" runat="server" Text=""></cc1:ucLabel>
                                                    </td>
                                                    <td style="font-weight: bold;">
                                                        :
                                                    </td>
                                                    <td class="nobold">
                                                        <cc1:ucLabel ID="UcLabel24" runat="server" Text="J Procurement"></cc1:ucLabel>
                                                    </td>
                                                </tr>
                                            </table>
                                        </cc1:ucPanel>
                                    </td>
                                    <td width="2%">
                                        &nbsp;
                                    </td>
                                    <td width="31%" rowspan="7" valign="top">
                                        <cc1:ucPanel ID="pnlCommunicationDetail" runat="server" GroupingText="" CssClass="fieldset-form">
                                            <table width="100%" cellspacing="0" cellpadding="0" class="form-table" height="140px">
                                                <tr>
                                                    <td>
                                                        <uc2:ucSDRCommunication ID="ucSDRCommunication2" runat="server" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </cc1:ucPanel>
                                    </td>
                                </tr>
                            </table>
                            <br />
                            <cc1:ucGridView ID="grdVendorPQ1" Width="100%" runat="server" CssClass="grid">
                                <Columns>
                                    <asp:TemplateField HeaderText="Line">
                                        <HeaderStyle Width="8%" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="UcLabel16" Text='<%# Eval("LineNo") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Office Depot Code">
                                        <HeaderStyle Width="8%" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="UcLabel17" Text='<%# Eval("ODCode") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Viking Code">
                                        <HeaderStyle Width="8%" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="UcLabel18" Text='<%# Eval("VikCode") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Vendor Item Code">
                                        <HeaderStyle Width="8%" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="UcLabel19" Text='<%# Eval("VendorCode") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Description">
                                        <HeaderStyle Width="35%" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="UcLabel21" Text='<%# Eval("Desc") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Original PO Quantity">
                                        <HeaderStyle Width="5%" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="UcLabel23" Text='<%# Eval("OriginalPoQty") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Outstanding PO Quantity">
                                        <HeaderStyle Width="5%" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="UcLabel25" Text='<%# Eval("OutstandingPoQty") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="UOM">
                                        <HeaderStyle Width="5%" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="UcLabel26" Text='<%# Eval("UOM") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="PO Item Cost">
                                        <HeaderStyle Width="5%" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="UcLabel27" Text='<%# Eval("POItemCost") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="PO Total Cost">
                                        <HeaderStyle Width="5%" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="UcLabel28" Text='<%# Eval("PoTotalCost") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Invoiced Item Price">
                                        <HeaderStyle Width="5%" HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="UcLabel29" Text='<%# Eval("InvoicedItemPrice") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Qty Invoiced">
                                        <HeaderStyle Width="5%" HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="UcLabel30" Text='<%# Eval("QtyInvoiced") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Invoiced Cost">
                                        <HeaderStyle Width="8%" HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <asp:Label ID="Label1" runat="server" Text='<%# Eval("InvoicedCost") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Price Difference">
                                        <HeaderStyle Width="8%" HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <asp:Label ID="Label2" runat="server" Text='<%# Eval("PriceDiff") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                </Columns>
                            </cc1:ucGridView>
                            <br />
                            <div id="raiseQryActionDiv" >
                                <cc1:ucPanel ID="pnlAction" Visible=false runat="server" CssClass="fieldset-form">
                                    <table width="100%" cellspacing="5" cellpadding="0" border="0" class="form-table">
                                        <tr>
                                            <td>
                                                <cc1:ucLabel ID="UcLabel1" runat="server" CssClass="action-required-heading" isRequired="true"
                                                    Font-Size="13px" Text="Please enter a full description of your query relating to this Price Query. We in turn will investigate and will provide feedback once the investigation is completed."></cc1:ucLabel>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: bold;">
                                                <asp:TextBox ID="TextBox1" runat="server" TextMode="MultiLine" Width="98%" MaxLength="1000"
                                                    Height="70px"></asp:TextBox>
                                            </td>
                                        </tr>
                                    </table>
                                </cc1:ucPanel>
                                <!-- Button Save/Back Section -->
                                <div class="button-row">
                                    <cc1:ucButton ID="BackDebitLibVendor" Visible="false" runat="server" Text="Back" class="button" OnClick="BackDebitLibVendorClick" />
                                    <cc1:ucButton ID="BackDebitLib" runat="server" Visible="false" Text="Back" class="button" OnClick="BackDebitLibClick" />
                                    <cc1:ucButton ID="UcButton1" runat="server"  Visible="false" Text="Back" class="button" OnClick="BackClick" />
                                    <cc1:ucButton ID="UcButton2" runat="server" Visible="false" Text="Save" class="button" OnClick="btnSave_Click" />
                                </div>
                            </div>
                            <%--<div id="AddCommentsDiv" style="display: none">
                                <cc1:ucPanel ID="pnlAction" GroupingText="Action" runat="server" CssClass="fieldset-form">
                                    <table width="100%" cellspacing="5" cellpadding="0" border="0" class="form-table">
                                        <tr>
                                            <td>
                                                <cc1:ucLabel ID="lblUC1" runat="server" isRequired="true" Font-Size="13px" CssClass="action-required-heading"
                                                    Text="Please enter a full description of your query relating to this Price Query. We in turn will investigate and will provide feedback once the investigation is completed."></cc1:ucLabel>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: bold;">
                                                <asp:TextBox ID="txtInternalComments" runat="server" CssClass="inputbox" TextMode="MultiLine"
                                                    Width="98%" MaxLength="1000" Height="70px"></asp:TextBox>
                                            </td>
                                        </tr>
                                    </table>
                                </cc1:ucPanel>
                                <table id="tblButtons" width="100%" cellspacing="0" cellpadding="0" class="form-table">
                                    <tr>
                                        <td>
                                            <!-- Button Save/Back Section -->
                                            <div class="button-row">
                                                <cc1:ucButton ID="btnBack" runat="server" Text="Back" class="button" />
                                                <cc1:ucButton ID="btnSave" runat="server" Text="Save" class="button" />
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>--%>
                        </cc1:ucView>
                    </cc1:ucMultiView>
                </div>
            </div>
            <%--<asp:Button ID="btnBack" runat="server" Text="Back" OnClick="BackClick" />
            <asp:Button ID="Button1" runat="server" Text="Next" OnClick="NextClick" />--%>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

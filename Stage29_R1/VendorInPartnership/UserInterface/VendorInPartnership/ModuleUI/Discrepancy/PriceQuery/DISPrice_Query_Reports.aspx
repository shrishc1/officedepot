﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    CodeFile="DISPrice_Query_Reports.aspx.cs" Inherits="ModuleUI_Discrepancy_PriceQuery_DISPrice_Query_Reports" %>

<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register TagName="ucDate" TagPrefix="cc2" Src="~/CommonUI/UserControls/ucDate.ascx" %>
<%@ Register Src="../UserControl/ucSDRCommunication.ascx" TagName="ucSDRCommunication"
    TagPrefix="uc2" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectVendor.ascx" TagName="MultiSelectVendor"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectAPSearch.ascx" TagName="MultiSelectAPSearch"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectProcurementSearch.ascx" TagName="MultiSelectProcurementSearch"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectSite.ascx" TagName="MultiSelectSite"
    TagPrefix="cc1" %>
<%@ Register Src="../../../CommonUI/UserControls/ucSite.ascx" TagName="ucSite" TagPrefix="cc2" %>
<%@ Register Src="../UserControl/APAction/APAction.ascx" TagName="APAction" TagPrefix="uc2" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectVendor.ascx" TagName="MultiSelectVendor"
    TagPrefix="cc3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script language="javascript" src="../../../Scripts/JSON2.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">

    </script>
    <asp:ScriptManager ID="sp1" runat="server">
    </asp:ScriptManager>
    <asp:HiddenField ID="hdnSiteID" runat="server" />
    <asp:UpdatePanel ID="up1" runat="server">
        <ContentTemplate>
            <h2>
                <cc1:ucLabel ID="lblPriceQueryReports" runat="server"></cc1:ucLabel>
            </h2>
            <div class="right-shadow">
                <div class="formbox">
                    <cc1:ucMultiView ID="mvOvers" runat="server" ActiveViewIndex="0">
                        <cc1:ucView ID="vwPriceQryRep" runat="server">
                            <table width="80%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                                <tr>
                                    <td style="font-weight: bold;">
                                        <cc1:ucLabel ID='lblVendor' runat="server">
                                        </cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold;">
                                        :
                                    </td>
                                    <td>
                                        <cc1:MultiSelectVendor runat="server" ID="msVendor" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="font-weight: bold;">
                                        <cc1:ucLabel ID='lblAPName' runat="server">
                                        </cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold;">
                                        :
                                    </td>
                                    <td>
                                        <cc1:MultiSelectAPSearch runat="server" ID="MultiSelectAPSearch" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="font-weight: bold;">
                                        <cc1:ucLabel ID='lblProcurmentName' runat="server">
                                        </cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold;">
                                        :
                                    </td>
                                    <td>
                                        <cc1:MultiSelectProcurementSearch runat="server" ID="MultiSelectProcurementSearch" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="font-weight: bold;">
                                        <cc1:ucLabel ID='lblSite' runat="server">
                                        </cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold;">
                                        :
                                    </td>
                                    <td>
                                        <cc1:MultiSelectSite runat="server" ID="msSite" />
                                    </td>
                                    <%-- <td colspan="2">
                                        <cc1:ucListBox ID="UcListBox15" Height="150px" Width="100%" runat="server">
                                        </cc1:ucListBox>
                                    </td>
                                    <td>
                                        <div>
                                            <cc1:ucButton ID="btnMoveAllRight" runat="server" Text=">>" CssClass="button" Width="35px" /></div>
                                        &nbsp;
                                        <div>
                                            <cc1:ucButton ID="btnMoveRight3" runat="server" Text=">" CssClass="button" Width="35px" /></div>
                                        &nbsp;
                                        <div>
                                            <cc1:ucButton ID="btnMoveLeft3" runat="server" Text="<" CssClass="button" Width="35px" /></div>
                                        &nbsp;
                                        <div>
                                            <cc1:ucButton ID="btnMoveAllLeft" runat="server" Text="<<" CssClass="button" Width="35px" /></div>
                                    </td>
                                    <td colspan="2">
                                        <cc1:ucListBox ID="UcListBox16" Height="150px" Width="100%" runat="server">
                                        </cc1:ucListBox>
                                    </td>--%>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td colspan="2">
                                        <table width="100%">
                                            <tr>
                                                <td width="25%" style="text-align: left;">
                                                    <cc1:ucRadioButton runat="server" ID="rdoAPSummary" GroupName="PQReport" />
                                                </td>
                                                <td width="25%" style="text-align: left;">
                                                    <cc1:ucRadioButton runat="server" ID="rdoAPDetail" GroupName="PQReport" />
                                                </td>
                                                <td width="25%" style="text-align: left;">
                                                    <cc1:ucRadioButton runat="server" ID="rdoProcurementSummary" GroupName="PQReport" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="25%" style="text-align: left;">
                                                    <cc1:ucRadioButton runat="server" ID="rdoProcurementDetail" GroupName="PQReport" />
                                                </td>
                                                <td width="25%" style="text-align: left;">
                                                    <cc1:ucRadioButton runat="server" ID="rdoVendorSummary" GroupName="PQReport" />
                                                </td>
                                                <td width="25%" style="text-align: left;">
                                                    <cc1:ucRadioButton runat="server" ID="rdoVendorDetail" GroupName="PQReport" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="25%" style="text-align: left;">
                                                    <cc1:ucRadioButton runat="server" ID="rdoOutcome" GroupName="PQReport" />
                                                </td>
                                                <td width="25%" style="text-align: left;">
                                                    <cc1:ucRadioButton runat="server" ID="rdoMonthVendor" GroupName="PQReport" />
                                                </td>
                                                <td width="25%" style="text-align: left;">
                                                    <cc1:ucRadioButton runat="server" ID="rdoMatchRateVendor" GroupName="PQReport" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </cc1:ucView>
                    </cc1:ucMultiView>
                </div>
            </div>
            <div class="button-row">
                <cc1:ucButton ID="btnSearch" runat="server" class="button" OnClick="search_Clicked" />
            </div>
            <%--<asp:Button ID="btnBack" runat="server" Text="Back" OnClick="BackClick" />
            <asp:Button ID="Button1" runat="server" Text="Next" OnClick="NextClick" />--%>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ModuleUI_Discrepancy_PriceQuery_DISPriceSearch_Price_Query : CommonPage
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void NextClick(object sender, EventArgs e)
    {
        Response.Redirect("DISPriceSearch_Price_Query_Results.aspx");
    }

    protected void BackClick(object sender, EventArgs e)
    {
        Response.Redirect("DISPriceDebit_Review_Detail.aspx");
    }

    protected void SearchBtnClick(object sender, EventArgs e)
    {
        if (ddlActionPendingWith.SelectedValue == "1")//AP-bs2
        {
            Response.Redirect("DISPriceSearch_Price_Query_Results.aspx?id=ap");
        }
        else if (ddlActionPendingWith.SelectedValue == "2")//Proc-OP5
        {
            Response.Redirect("DISPriceSearch_Price_Query_Results2.aspx?id=proc");
        }
        else if (ddlActionPendingWith.SelectedValue == "3")//vendor-s3
        {
            Response.Redirect("DISPriceSearch_Price_Query_Results3.aspx?id=vendor");
        }

        else if (ddlActionPendingWith.SelectedValue == "4")//vendor-s3
        {
            Response.Redirect("DISPriceSearch_Price_Query_Results3.aspx?id=all");
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ModuleUI_Discrepancy_PriceQuery_DISPriceVendor_User_Setup : CommonPage
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void NextClick(object sender, EventArgs e)
    {
        Response.Redirect("DISPriceVendor_PQ_View.aspx");

    }

    protected void BackClick(object sender, EventArgs e)
    {
        Response.Redirect("DISPriceProcurement_Role.aspx");

    }

}
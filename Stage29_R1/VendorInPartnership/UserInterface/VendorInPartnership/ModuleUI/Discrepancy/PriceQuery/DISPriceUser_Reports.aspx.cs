﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


public class GridPriceQueryByUserReport
{

    public string DebitNo { get; set; }
    public string Status { get; set; }
    public string Site { get; set; }
    public string Vendor { get; set; }
    public string ODSKU { get; set; }
    public string CatCode { get; set; }
    public string VendorCode { get; set; }
    public string POItemPrice { get; set; }
    public string InvoiceItemPrice { get; set; }
    public string InvoiceQty { get; set; }

    public string POCost { get; set; }
    public string InvoiceCost { get; set; }
    public string PriceDifference { get; set; }
    public string Action { get; set; }
    public string ProcurementContact { get; set; }

}


public partial class ModuleUI_Discrepancy_PriceQuery_DISPriceUser_Reports : CommonPage
{
    protected void Page_Load(object sender, EventArgs e)
    {

        List<GridPriceQueryByUserReport> lst3 = new List<GridPriceQueryByUserReport>();

        GridPriceQueryByUserReport obj3 = new GridPriceQueryByUserReport
        {

            DebitNo = "D02390910",
            Status = "Open",
            Site = "Ashton",
            Vendor = "012-CPD",
            ODSKU = "3281818",
            CatCode = "ER-1234",
            VendorCode = "ZZ-1138D",
            POItemPrice = "10",
            InvoiceItemPrice = "11",
            InvoiceQty = "100",

            POCost = "1000",
            InvoiceCost = "1100",
            PriceDifference = "100",
            Action = "Debited",
            ProcurementContact = "J Procrurement",

        };
        lst3.Add(obj3);

        obj3 = new GridPriceQueryByUserReport
        {

            DebitNo = "D48234820",
            Status = "Closed",
            Site = "Leicester",
            Vendor = "012-CPD",
            ODSKU = "3281818",
            CatCode = "BK93238",
            VendorCode = "BK93238",
            POItemPrice = "3.2",
            InvoiceItemPrice = "3.5",
            InvoiceQty = "250",

            POCost = "800",
            InvoiceCost = "875",
            PriceDifference = "75",
            Action = "Cancelled",
            ProcurementContact = "J Procrurement",

        };
        lst3.Add(obj3);

        grdPriceQueryByUserReport.DataSource = lst3;
        grdPriceQueryByUserReport.DataBind();
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        string id = Request.QueryString["id"];

        if (id != null && id == "APSummary")
        {
            Response.Redirect("DISPrice_Query_Reports_By_Date.aspx");
        }
        else
        {
            Response.Redirect("DISPrice_Query_Reports.aspx");
        }
    }
}
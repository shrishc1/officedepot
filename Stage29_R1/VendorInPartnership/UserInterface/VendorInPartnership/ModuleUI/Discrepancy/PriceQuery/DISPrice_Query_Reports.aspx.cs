﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ModuleUI_Discrepancy_PriceQuery_DISPrice_Query_Reports : CommonPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        msSite.isMoveAllRequired = true;
    }

    protected void BackClick(object sender, EventArgs e)
    {
        Response.Redirect("DISPriceVendor_Query.aspx");
    }


    protected void NextClick(object sender, EventArgs e)
    {
        Response.Redirect("DISPrice_Query_Reports_By_Date.aspx");
    }

    protected void search_Clicked(object sender, EventArgs e)
    {
        if (rdoAPSummary.Checked)
        {
            Response.Redirect("DISPrice_Query_Reports_By_Date.aspx");
        }

        if (rdoAPDetail.Checked)
        {
            Response.Redirect("DISPriceUser_Reports.aspx");
        }

        if (rdoVendorSummary.Checked)
        {
            Response.Redirect("DISPriceVendor_Summary_Report.aspx");
        }

        if (rdoVendorDetail.Checked)
        {
            Response.Redirect("DISPriceVendorDetailReport.aspx");
        }

        if (rdoMonthVendor.Checked)
        {
            Response.Redirect("DISPrice_Query_Vendor_Summary.aspx");
        }
        
    }
}
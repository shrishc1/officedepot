﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


public class GridPriceQueryTrackReport
{

    public string APName { get; set; }
    public string Debits { get; set; }
    public string Actioned { get; set; }
    public string Approved { get; set; }
    public string Cancelled { get; set; }
    public string Outstanding { get; set; }
    public string TotalValue { get; set; }
    public string ValueDebited { get; set; }
    public string ValueCancelled { get; set; }
    public string NotActionedYet { get; set; }

}



public partial class ModuleUI_Discrepancy_PriceQuery_DISPrice_Query_Reports_By_Date : CommonPage
{
    protected void Page_Load(object sender, EventArgs e)
    {

        List<GridPriceQueryTrackReport> lst4 = new List<GridPriceQueryTrackReport>();
        GridPriceQueryTrackReport obj4 = new GridPriceQueryTrackReport
        {

            APName = "Alina Savu",
            Debits = "23",
            Actioned = "21",
            Approved = "4",
            Cancelled = "15",
            Outstanding = "2",
            TotalValue = "1398.91",
            ValueDebited = "112.3",
            ValueCancelled = "1039.39",
            NotActionedYet = "247.22",

        };
        lst4.Add(obj4);

        obj4 = new GridPriceQueryTrackReport
        {

            APName = "Oana Jurca",
            Debits = "12",
            Actioned = "12",
            Approved = "8",
            Cancelled = "4",
            Outstanding = "0",
            TotalValue = "391.34",
            ValueDebited = "210.1",
            ValueCancelled = "181.24",
            NotActionedYet = "0",

        };
        lst4.Add(obj4);

        grdPriceQryTrackReport.DataSource = lst4;
        grdPriceQryTrackReport.DataBind();



    }


    protected void btnBack_Click(object sender, EventArgs e)
    {
        Response.Redirect("DISPrice_Query_Reports.aspx");
    }
}
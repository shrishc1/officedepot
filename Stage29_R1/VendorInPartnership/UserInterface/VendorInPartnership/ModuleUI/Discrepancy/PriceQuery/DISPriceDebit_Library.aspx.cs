﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ModuleUI_Discrepancy_PriceQuery_DISPriceDebit_Library : CommonPage
{
    protected void Page_Load(object sender, EventArgs e)
    {

        List<GridDebitLib> lst = new List<GridDebitLib>();
        GridDebitLib obj = new GridDebitLib
        {

            Status = "Active",
            VenderNo = "103 - REALLY USEFUL PRODUCTS",
            PQNum = "",
            DateDebitRaised = "9/8/2014",
            RaisedBy = "Adriana Buta",
            DebitNoteNum = "DNUK0000002",
            DebitReason = "",
            PONum = "11650",
            ValueOfDebit = "221.3",
            currency = "GBP",
            SentTo = "craditcontr@usefulproduct.com",
            CancelDate = "",
            CancleName = "",
            CancleSentTo = "",

        };
        lst.Add(obj);

        obj = new GridDebitLib
        {

            Status = "Cancelled",
            VenderNo = "025 - ACCO UK LTD",
            PQNum = "PQ00000001",
            DateDebitRaised = "9/2/2014",
            RaisedBy = "Adriana Buta",
            DebitNoteNum = "DNUK0000001",
            DebitReason = "",
            PONum = "10972",
            ValueOfDebit = "49.36",
            currency = "GBP",
            SentTo = "VIPAdm@OfficeDepot.com",
            CancelDate = "1/1/2015",
            CancleName = "A Buta",
            CancleSentTo = "creditc@reallyus.com",

        };
        lst.Add(obj);


        obj = new GridDebitLib
        {

            Status = "Pending",
            VenderNo = "00163 - BROTHER INTERNATIONAL GMBH FX",
            PQNum = "PQ00000002",
            DateDebitRaised = "8/25/2014",
            RaisedBy = "Adriana Buta",
            DebitNoteNum = "DNDE0000002",
            DebitReason = "",
            PONum = "10972",
            ValueOfDebit = "49.36",
            currency = "EURO",
            SentTo = "VIPAdmin@OfficeDepot.com",
            CancelDate = "",
            CancleName = "",
            CancleSentTo = "",

        };
        lst.Add(obj);
        
        
        grdDebitLibrary.DataSource = lst;
        grdDebitLibrary.DataBind();
    }

    protected void NextClick(object sender, EventArgs e)
    {
        Response.Redirect("DISPriceDebit_Review.aspx");

    }

    protected void BackClick(object sender, EventArgs e)
    {
        Response.Redirect("DISPriceProcur_Entry.aspx");

    }

}

public class GridDebitLib
{
    public string Text1 { get; set; }
    public string Text2 { get; set; }

    public string Status { get; set; }
    public string VenderNo { get; set; }
    public string PQNum { get; set; }
    public string DateDebitRaised { get; set; }
    public string RaisedBy { get; set; }
    public string DebitNoteNum { get; set; }
    public string DebitReason { get; set; }
    public string PONum { get; set; }
    public string ValueOfDebit { get; set; }
    public string currency { get; set; }
    public string SentTo { get; set; }
    public string CancelDate { get; set; }
    public string CancleName { get; set; }
    public string CancleSentTo { get; set; }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ModuleUI_Discrepancy_PriceQuery_DISPriceDebit_Review : CommonPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        List<GridDebitReview> lst = new List<GridDebitReview>();

        GridDebitReview obj = new GridDebitReview
        {
            Site = "ASH-Ashton",
            PriceQueryNum = "PQ000012354",
            DebitNoteNum = "DNUK0000001",
            VenderNum = "103 REALLY USEFUL PRODUCT",
            PONum = "11642",
            SKU = "1928269",
            CatCode = "EA-123",
            Description = "PA 20 IPAD OPTICLUDE",
            POItemPrice = "10.00",
            POCost = "100.00",
            InvoiceItemPrice = "11.00",
            InvoiceCost = "110",
            QtyInvoiced = "10",
            PriceDiff = "1.00",
            ValueOfDebit = "10",
            Currency = "GBP",
            DateDebitRaised = "9/8/2014",
            RaisedBy = "Andriana Buta",
        };

        lst.Add(obj);

        obj = new GridDebitReview
        {
            Site = "MSL-MSL",
            PriceQueryNum = "PQ000000001",
            DebitNoteNum = "DNUK0000002",
            VenderNum = "025 - ACCO UK LTD",
            PONum = "11642",
            SKU = "1928269",
            CatCode = "EA-123",
            Description = "BX26 GOWN STANDARD - LARGE 3M",
            POItemPrice = "10.00",
            POCost = "184.80",
            InvoiceItemPrice = "11.00",
            InvoiceCost = "262.68",
            QtyInvoiced = "10",
            PriceDiff = "0.59",
            ValueOfDebit = "10",
            Currency = "EUR",
            DateDebitRaised = "9/2/2014",
            RaisedBy = "Andriana Buta",
        };

        lst.Add(obj);

        grdDebitReview.DataSource = lst;
        grdDebitReview.DataBind();
    }

    protected void NextClick(object sender, EventArgs e)
    {
        Response.Redirect("DISPriceDebit_Review_Detail.aspx");

    }

    protected void BackClick(object sender, EventArgs e)
    {
        Response.Redirect("DISPriceDebit_Library.aspx");

    }
}


public class GridDebitReview
{
    public string Site { get; set; }
    public string PriceQueryNum { get; set; }
    public string DebitNoteNum { get; set; }
    public string VenderNum { get; set; }
    public string PONum { get; set; }
    public string SKU { get; set; }
    public string CatCode { get; set; }
    public string Description { get; set; }
    public string POItemPrice { get; set; }
    public string POCost { get; set; }
    public string InvoiceItemPrice { get; set; }
    public string InvoiceCost{ get; set; }
    public string QtyInvoiced { get; set; }
    public string PriceDiff { get; set; }
    public string ValueOfDebit { get; set; }
    public string Currency { get; set; }
    public string DateDebitRaised { get; set; }
    public string RaisedBy { get; set; }
}
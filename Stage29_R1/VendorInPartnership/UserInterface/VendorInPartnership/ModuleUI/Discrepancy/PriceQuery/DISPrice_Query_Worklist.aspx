﻿<%@ Page Language="C#" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    AutoEventWireup="true" CodeFile="DISPrice_Query_Worklist.aspx.cs" Inherits="ModuleUI_Discrepancy_PriceQuery_DISPrice_Query_Worklist" %>

<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register TagName="ucDate" TagPrefix="cc2" Src="~/CommonUI/UserControls/ucDate.ascx" %>
<%@ Register Src="../UserControl/ucSDRCommunication.ascx" TagName="ucSDRCommunication"
    TagPrefix="uc2" %>
<%@ Register Src="../../../CommonUI/UserControls/ucSite.ascx" TagName="ucSite" TagPrefix="cc2" %>
<%@ Register Src="../UserControl/APAction/APAction.ascx" TagName="APAction" TagPrefix="uc2" %>
<%@ Register Src="../../../CommonUI/UserControls/ucSeacrhVendor.ascx" TagName="ucSeacrhVendor"
    TagPrefix="cc3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script language="javascript" src="../../../Scripts/JSON2.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        function createDebit() {
            //            alert();
            var chkbox = document.getElementById("ctl00_ContentPlaceHolder1_grdPriceQryWorklist_ctl02_ucchk");
            var chkbox2 = document.getElementById("ctl00_ContentPlaceHolder1_grdPriceQryWorklist_ctl03_ucchk");

            if (chkbox.checked || chkbox2.checked) {

                if (confirm('Do you want to create Debit?')) {
                    return true;
                } else {
                    return false;
                }
            }
            else {
                alert("please select a checkbox")
            }
        }

        function cancelDebit() {

            var chkbox = document.getElementById("ctl00_ContentPlaceHolder1_grdPriceQryWorklist_ctl02_ucchk");
            var chkbox2 = document.getElementById("ctl00_ContentPlaceHolder1_grdPriceQryWorklist_ctl03_ucchk");

            if (chkbox.checked || chkbox2.checked) {
                if (confirm('Do you want to cancel Debit?')) {
                    return true;
                } else {
                    return false;
                }
            }
            else {
                alert("please select a checkbox")
            }
        }

        $(document).ready(function () {
            $('#ctl00_ContentPlaceHolder1_grdPriceQryWorklist_ctl01_chkAllOnly').change(function () {
                if ($(this).is(":checked")) {
                    document.getElementById("ctl00_ContentPlaceHolder1_grdPriceQryWorklist_ctl02_ucchk").checked = true;
                    document.getElementById("ctl00_ContentPlaceHolder1_grdPriceQryWorklist_ctl03_ucchk").checked = true;

                }
                else {
                    document.getElementById("ctl00_ContentPlaceHolder1_grdPriceQryWorklist_ctl02_ucchk").checked = false;
                    document.getElementById("ctl00_ContentPlaceHolder1_grdPriceQryWorklist_ctl03_ucchk").checked = false;
                }
            });
        });

    </script>
    <asp:ScriptManager ID="sp1" runat="server">
    </asp:ScriptManager>
    <asp:HiddenField ID="hdnSiteID" runat="server" />
    <asp:UpdatePanel ID="up1" runat="server">
        <ContentTemplate>
            <h2>
                <cc1:ucLabel ID="lblPriceQueryWorklist" runat="server"></cc1:ucLabel>
            </h2>
            <table width="99%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                <tr>
                    <td style="font-weight: bold;">
                        <cc1:ucLabel ID="lblVendorNumber" runat="server"></cc1:ucLabel>
                    </td>
                    <td>
                        <cc1:ucTextbox ID="txt1" runat="server"></cc1:ucTextbox>
                        <input type="image" id="Image1" name="imgVendor" src="<%= ResolveClientUrl("../../../Images/Search.gif")%>"
                            style="height: 16px; width: 16px;" onclick="return showVendor();" />
                    </td>
                    <td style="font-weight: bold;">
                        <cc1:ucLabel ID="lblSKU" runat="server"></cc1:ucLabel>
                    </td>
                    <td>
                        <cc1:ucTextbox ID="txtSKU" runat="server"></cc1:ucTextbox>
                    </td>
                    <td style="font-weight: bold;">
                        <cc1:ucLabel ID="lblCatCode" runat="server"></cc1:ucLabel>
                    </td>
                    <td>
                        <cc1:ucTextbox ID="txtCatCode" runat="server"></cc1:ucTextbox>
                    </td>
                    <td style="font-weight: bold;">
                        <cc1:ucLabel ID="lblPO" runat="server"></cc1:ucLabel>
                    </td>
                    <td>
                        <cc1:ucTextbox ID="txtPO" runat="server"></cc1:ucTextbox>
                    </td>
                    <td>
                        <cc1:ucButton ID="btnFilter" runat="server" class="button" Visible="true" />
                    </td>
                </tr>
            </table>
            <br />
            <div class="right-shadow">
                <div class="formbox">
                    <cc1:ucMultiView ID="mvOvers" runat="server" ActiveViewIndex="0">
                        <cc1:ucView ID="vwPQWorklist" runat="server">
                            <br />
                            <cc1:ucGridView ID="grdPriceQryWorklist" Width="100%" runat="server" CssClass="grid">
                                <Columns>
                                    <asp:TemplateField HeaderText="SelectAllCheckBox">
                                        <HeaderStyle HorizontalAlign="Center" Width="8%" />
                                        <ItemStyle Width="20%" HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <HeaderTemplate>
                                            <cc1:ucCheckbox runat="server" ID="chkAllOnly" CssClass="checkbox-input" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <cc1:ucCheckbox runat="server" ID="ucchk" CssClass="checkbox-input" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Site">
                                        <HeaderStyle Width="8%" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="UcLabel1" Text='<%# Eval("Site") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Price Query #">
                                        <HeaderStyle Width="8%" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hplImageName" runat="server" Text='<%# Eval("PriceQueryNum") %>'
                                                NavigateUrl="DISPriceDebit_Review_Detail.aspx?id=WorkList"></asp:HyperLink>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Debit Note Number">
                                        <HeaderStyle Width="8%" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hplImageName" runat="server" Text='<%# Eval("DebitNoteNum") %>'
                                                NavigateUrl=''></asp:HyperLink>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Vender #">
                                        <HeaderStyle Width="8%" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="UcLabel2" Text='<%# Eval("VenderNum") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="PO #">
                                        <HeaderStyle Width="35%" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="UcLabel3" Text='<%# Eval("PONum") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="SKU">
                                        <HeaderStyle Width="5%" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="UcLabel4" Text='<%# Eval("SKU") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="CAT Code">
                                        <HeaderStyle Width="5%" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="UcLabel5" Text='<%# Eval("CatCode") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Description">
                                        <HeaderStyle Width="5%" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="UcLabel6" Text='<%# Eval("Description") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="PO Item Price">
                                        <HeaderStyle Width="5%" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="UcLabel7" Text='<%# Eval("POItemPrice") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="PO Cost">
                                        <HeaderStyle Width="5%" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="UcLabel8" Text='<%# Eval("POCost") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Invoice Item Price">
                                        <HeaderStyle Width="5%" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="UcLabel9" Text='<%# Eval("InvoiceItemPrice") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Invoice Cost">
                                        <HeaderStyle Width="5%" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="UcLabel10" Text='<%# Eval("InvoiceCost") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="QTY invoiced">
                                        <HeaderStyle Width="5%" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="UcLabel11" Text='<%# Eval("QtyInvoiced") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Price Difference">
                                        <HeaderStyle Width="5%" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="UcLabel12" Text='<%# Eval("PriceDiff") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Value of Debit">
                                        <HeaderStyle Width="5%" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="UcLabel13" Text='<%# Eval("ValueOfDebit") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Currency">
                                        <HeaderStyle Width="5%" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="UcLabel14" Text='<%# Eval("Currency") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Date Debit Raised">
                                        <HeaderStyle Width="5%" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="UcLabel15" Text='<%# Eval("DateDebitRaised") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Raised By">
                                        <HeaderStyle Width="5%" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="UcLabel16" Text='<%# Eval("RaisedBy") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                </Columns>
                            </cc1:ucGridView>
                        </cc1:ucView>
                    </cc1:ucMultiView>
                </div>
            </div>
            <div class="button-row">
                <cc1:ucButton ID="btnCreateDebit" runat="server" class="button" Visible="true" OnClientClick="return createDebit();" />
                <cc1:ucButton ID="btnCancelDebit" runat="server" class="button" Visible="true" OnClientClick="return cancelDebit();" />
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <%--<asp:Button ID="Button1" runat="server" Text="Back" OnClick="BackClick" />
    <asp:Button ID="btn2" OnClick="NextClick" runat="server" Text="Next" />--%>
</asp:Content>

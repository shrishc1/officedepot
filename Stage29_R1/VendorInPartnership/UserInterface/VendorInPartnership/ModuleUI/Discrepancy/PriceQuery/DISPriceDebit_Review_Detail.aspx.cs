﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ModuleUI_Discrepancy_PriceQuery_DISPriceDebit_Review_Detail : CommonPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string id = Request.QueryString["id"];

        if (id == "WorkList")
        {
            btnBackWorkList.Visible = true;
            btnBack.Visible = false;
        }
        else if (id == null)
        {
            btnBackWorkList.Visible = false;
            btnBack.Visible = true;
        }

        grdPurchaseOrderDetail.DataSource = new List<Item>
                {
                    new Item {
                        
                        LineNo = "10",
                        ODCode = "46589",
                        VikCode = "46589",
                        VendorCode = "DF46589",
                        Desc= "2 in 1 Desk AND CLIP  FAN",
                        OriginalPoQty = "123",
                        OutstandingPoQty = "20",
                        UOM = "",
                        POItemCost = "7.19",
                        PoTotalCost = "999.41",
                        InvoicedItemPrice = "7.2",
                        QtyInvoiced = "200",
                        PriceDiff = "0.01",
                        ValueOfDebit="200"
                    
                    }
                };

        grdPurchaseOrderDetail.DataBind();
    }

    //protected void BackClick(object sender, EventArgs e)
    //{
    //    Response.Redirect("DISPriceDebit_Review.aspx");
    //}
    //DISPriceSearch_Price_Query_Results2.aspx

    protected void BackClick(object sender, EventArgs e)
    {
        Response.Redirect("DISPriceSearch_Price_Query_Results2.aspx");
    }

    protected void NextClick(object sender, EventArgs e)
    {
        Response.Redirect("DISPriceSearch_Price_Query.aspx");
    }

    protected void btnWorkFlowClicked(object sender, EventArgs e)
    {
        string id = Request.QueryString["id"];

        if (id != null)
        {
            Response.Redirect("DISPrice_workflow2.aspx?id=" + id);
        }

        else
        {
            Response.Redirect("DISPrice_workflow2.aspx");
        }

    }


    protected void BackWorkListClick(object sender, EventArgs e)
    {
        string id = Request.QueryString["id"];

        Response.Redirect("DISPrice_Query_Worklist.aspx?id=" + id);
    }
}


public class Item
{

    public string LineNo { get; set; }
    public string ODCode { get; set; }
    public string VikCode { get; set; }
    public string VendorCode { get; set; }
    public string Desc { get; set; }
    public string OriginalPoQty { get; set; }
    public string OutstandingPoQty { get; set; }
    public string UOM { get; set; }
    public string POItemCost { get; set; }
    public string PoTotalCost { get; set; }
    public string InvoicedItemPrice { get; set; }
    public string QtyInvoiced { get; set; }
    public string PriceDiff { get; set; }
    public string ValueOfDebit { get; set; }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public class GridProcurRole
{
    public string Devision { get; set; }

    public string DivName { get; set; }
    public string GroupNo { get; set; }
    public string GrpName { get; set; }
    public string Dept { get; set; }
    public string DptName { get; set; }
}

public partial class ModuleUI_Discrepancy_PriceQuery_DISPriceProcurement_Role : CommonPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        List<GridProcurRole> lst = new List<GridProcurRole>();
        GridProcurRole obj = new GridProcurRole
        {
            Devision = "14",
            DivName = "Business Machines and Technology",
            GroupNo = "601",
            GrpName = "Business Machines and Accessories",
            Dept = "192",
            DptName = "1702",
        };
        lst.Add(obj);

        obj = new GridProcurRole
        {
            Devision = "14",
            DivName = "Business Machines and Technology",
            GroupNo = "601",
            GrpName = "Business Machines and Accessories",
            Dept = "203",
            DptName = "1316",
        };
        lst.Add(obj);


        obj = new GridProcurRole
        {
            Devision = "11",
            DivName = "Facilities Mngt, Envelopes, Mailing, Seasonal, Dated",
            GroupNo = "117",
            GrpName = "Facilities Mngt",
            Dept = "133",
            DptName = "1107",
        };
        lst.Add(obj);
        
        grdProcurRole.DataSource = lst;
        grdProcurRole.DataBind();
    }

    protected void BackClick(object sender, EventArgs e)
    {
        Response.Redirect("DISPriceReassign_Query_Results.aspx");
    }

    protected void NextClick(object sender, EventArgs e)
    {
        Response.Redirect("DISPriceVendor_User_Setup.aspx");
    }


    protected void btnClick(object sender, EventArgs e)
    {
        mvOvers.ActiveViewIndex = 1;
    }

    //
    protected void ShowView1(object sender, EventArgs e)
    {
        mvOvers.ActiveViewIndex = 0;
    }





}
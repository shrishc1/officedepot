﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public class GridPriceQuery
{

    public string LineNo { get; set; }
    public string ODCode { get; set; }
    public string VikCode { get; set; }
    public string VendorCode { get; set; }
    public string Desc { get; set; }
    public string OriginalPoQty { get; set; }
    public string OutstandingPoQty { get; set; }
    public string UOM { get; set; }
    public string POItemCost { get; set; }
    public string PoTotalCost { get; set; }
    public string InvoicedItemPrice { get; set; }
    public string QtyInvoiced { get; set; }
    public string PriceDiff { get; set; }
}

public partial class ModuleUI_Discrepancy_PriceQuery_DISPriceInitial_Entry : CommonPage
{
    protected void Page_InIt(object sender, EventArgs e)
    {
        ucSite.CurrentPage = this;
    }
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnLog_Click(object sender, EventArgs e)
    {
        try
        {
            mvOvers.ActiveViewIndex = 1;
            btnLogPriceQuery.Visible = false;
            btnBack.Visible = true;
            btnBack.Enabled = true;
            btnSave.Visible = true;
            btnSave.Enabled = true;
            //backButton.Visible = true;


            List<GridPriceQuery> lst = new List<GridPriceQuery>();
            GridPriceQuery obj = new GridPriceQuery
            {

                LineNo = "10",
                ODCode = "46589",
                VikCode = "46589",
                VendorCode = "DF46589",
                Desc = "2 in 1 Desk AND CLIP  FAN",
                OriginalPoQty = "123",
                OutstandingPoQty = "20",
                UOM = "",
                POItemCost = "7.19",
                PoTotalCost = "999",
                InvoicedItemPrice = "",
                QtyInvoiced = "",
                PriceDiff = "",

            };
            lst.Add(obj);


            obj = new GridPriceQuery
            {

                LineNo = "20",
                ODCode = "7654891",
                VikCode = "7654891",
                VendorCode = "IG9703",
                Desc = "Air cooler LED DISP1XXD",
                OriginalPoQty = "20",
                OutstandingPoQty = "20",
                UOM = "",
                POItemCost = "49.98",
                PoTotalCost = "999.60",
                InvoicedItemPrice = "",
                QtyInvoiced = "",
                PriceDiff = "",

            };
            lst.Add(obj);


            obj = new GridPriceQuery
            {

                LineNo = "30",
                ODCode = "5647891",
                VikCode = "5647891",
                VendorCode = "DF9703",
                Desc = "Desk Fan Speed 92 WE SIN",
                OriginalPoQty = "600",
                OutstandingPoQty = "0",
                UOM = "",
                POItemCost = "8.88",
                PoTotalCost = "5328.00",
                InvoicedItemPrice = "",
                QtyInvoiced = "",
                PriceDiff = "",

            };
            lst.Add(obj);

            grdPurchaseOrderDetail.DataSource = lst;
            grdPurchaseOrderDetail.DataBind();
        }
        catch (Exception ex)
        {
        }
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        mvOvers.ActiveViewIndex = 0;
        //backButton.Visible = false;
        btnLogPriceQuery.Visible = true;
        btnBack.Visible = false;
        btnSave.Visible = false;
    }

    //

    protected void btnSave_Click(object sender, EventArgs e)
    {
        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Debit Raised successfuly and your debit number is DNUK000001 \\nPrice Query Raised successfuly and your Price Query number is PQ00000001')", true);
        mvOvers.ActiveViewIndex = 0;

        btnLogPriceQuery.Visible = true;
        btnBack.Visible = false;
        btnSave.Visible = false;
    }
}


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public class GridPQByVendorDetail
{

    public string DebitNo { get; set; }
    public string Status { get; set; }
    public string Site { get; set; }
    public string ODSKU { get; set; }
    public string CatCode { get; set; }
    public string VendorCode { get; set; }
    public string POItemPrice { get; set; }
    public string InvoiceItemPrice { get; set; }
    public string InvoiceQty { get; set; }

    public string POCost { get; set; }
    public string InvoiceCost { get; set; }
    public string PriceDifference { get; set; }
    public string Action { get; set; }
    public string APContact { get; set; }
    public string ProcurementContact { get; set; }

}

public partial class ModuleUI_Discrepancy_PriceQuery_DISPriceVendorDetailReport : CommonPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        
        List<GridPQByVendorDetail> lst = new List<GridPQByVendorDetail>();

        GridPQByVendorDetail obj = new GridPQByVendorDetail
        {
            DebitNo = "D02390910",
            Status = "Open",
            Site = "Leicester",
            ODSKU = "3281818",
            CatCode = "ER-1234",
            VendorCode = "ZZ-1138D",
            POItemPrice = "10",
            InvoiceItemPrice = "11",
            InvoiceQty = "100",

            POCost = "1000",
            InvoiceCost = "1100",
            PriceDifference = "100",
            Action = "Debited",
            APContact = "A Savu",
            ProcurementContact = "J Procrurement",

        };
        lst.Add(obj);

        obj = new GridPQByVendorDetail
        {
            DebitNo = "D48234820",
            Status = "Closed",
            Site = "Leicester",
            ODSKU = "3281818",
            CatCode = "ER-1234",
            VendorCode = "BK93238",
            POItemPrice = "3.2",
            InvoiceItemPrice = "3.5",
            InvoiceQty = "250",

            POCost = "1000",
            InvoiceCost = "1100",
            PriceDifference = "100",
            Action = "Cancelled",
            APContact = "A Savu",
            ProcurementContact = "J Procrurement",

        };
        lst.Add(obj);

        grdPQByVendorDetail.DataSource = lst;
        grdPQByVendorDetail.DataBind();
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        string id = Request.QueryString["id"];

        if (id != null && id == "VendorSummary")
        {
            Response.Redirect("DISPriceVendor_Summary_Report.aspx");
        }
        else if (id != null && id == "MonVen")
        {
            Response.Redirect("DISPrice_Query_Vendor_Summary.aspx");
        }
        else
        {
            Response.Redirect("DISPrice_Query_Reports.aspx");
        }
    }
}
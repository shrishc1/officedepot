﻿<%@ Page Language="C#" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    AutoEventWireup="true" CodeFile="DISPriceSearch_Price_Query.aspx.cs" Inherits="ModuleUI_Discrepancy_PriceQuery_DISPriceSearch_Price_Query" %>

<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register TagName="ucDate" TagPrefix="cc2" Src="~/CommonUI/UserControls/ucDate.ascx" %>
<%@ Register Src="../UserControl/ucSDRCommunication.ascx" TagName="ucSDRCommunication"
    TagPrefix="uc2" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectSite.ascx" TagName="MultiSelectSite"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectVendor.ascx" TagName="MultiSelectVendor"
    TagPrefix="cc1" %>
<%@ Register Src="../../../CommonUI/UserControls/ucSite.ascx" TagName="ucSite" TagPrefix="cc2" %>
<%@ Register Src="../UserControl/APAction/APAction.ascx" TagName="APAction" TagPrefix="uc2" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectVendor.ascx" TagName="MultiSelectVendor"
    TagPrefix="cc3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script language="javascript" src="../../../Scripts/JSON2.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">

    </script>
    <asp:ScriptManager ID="sp1" runat="server">
    </asp:ScriptManager>
    <asp:HiddenField ID="hdnSiteID" runat="server" />
    <asp:UpdatePanel ID="up1" runat="server">
        <ContentTemplate>
            <h2>
                <cc1:ucLabel ID="lblSearchPriceQuery" runat="server"></cc1:ucLabel>
            </h2>
            <div class="right-shadow">
                <div class="formbox">
                    <cc1:ucMultiView ID="mvOvers" runat="server" ActiveViewIndex="0">
                        <cc1:ucView ID="vwPriceDetail" runat="server">
                            <table width="83%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                                <tr>
                                    <td style="font-weight: bold;">
                                        <cc1:ucLabel ID='lblPriceQueryNumHash' runat="server"></cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold;">
                                        :
                                    </td>
                                    <td>
                                        <cc1:ucTextbox ID='UcTextbox4' runat="server"></cc1:ucTextbox>
                                    </td>
                                    <td style="font-weight: bold;">
                                        <cc1:ucLabel ID='lblStatus' runat="server"></cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold;">
                                        :
                                    </td>
                                    <td>
                                        <cc1:ucDropdownList ID="ddlStatus" runat="server">
                                            <asp:ListItem Text="--Select--" Value="0"></asp:ListItem>
                                            <asp:ListItem Text="Active" Value="1"></asp:ListItem>
                                            <asp:ListItem Text="Inactive" Value="2"></asp:ListItem>
                                            <asp:ListItem Text="Pending" Value="3"></asp:ListItem>
                                        </cc1:ucDropdownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="font-weight: bold;">
                                        <cc1:ucLabel ID='lblDateFrom' runat="server"></cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold;">
                                        :
                                    </td>
                                    <td>
                                        <cc1:ucTextbox ID="txtFromDate" Text="25/11/2015" ClientIDMode="Static" runat="server"
                                            onchange="setValue1(this)" ReadOnly="True" />
                                    </td>
                                    <td style="font-weight: bold;">
                                        <cc1:ucLabel ID='lblDateTo' runat="server"></cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold;">
                                        :
                                    </td>
                                    <td>
                                        <cc1:ucTextbox ID="txtToDate" Text="30/11/2015" ClientIDMode="Static" runat="server"
                                            onchange="setValue1(this)" ReadOnly="True" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="font-weight: bold;">
                                        <cc1:ucLabel ID='lblSite' runat="server"></cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold;">
                                        :
                                    </td>
                                    <td colspan="4" >
                                        <cc1:MultiSelectSite runat="server" ID="MultiSelectSite" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="font-weight: bold;">
                                        <cc1:ucLabel ID='lblPO' runat="server"></cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold;">
                                        :
                                    </td>
                                    <td>
                                        <cc1:ucTextbox ID='txtPO' runat="server"></cc1:ucTextbox>
                                    </td>
                                    <td style="font-weight: bold;">
                                        <cc1:ucLabel ID='lblInvoiceNumHash' runat="server"></cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold;">
                                        :
                                    </td>
                                    <td>
                                        <cc1:ucTextbox ID='txtInvoiceNum' runat="server"></cc1:ucTextbox>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="font-weight: bold;">
                                        <cc1:ucLabel ID='lblAPClerk' runat="server"></cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold;">
                                        :
                                    </td>
                                    <td>
                                        <cc1:ucTextbox ID='txtAPClerk' runat="server"></cc1:ucTextbox>
                                    </td>
                                    <td style="font-weight: bold;">
                                        <cc1:ucLabel ID='lblProcurement' runat="server"></cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold;">
                                        :
                                    </td>
                                    <td>
                                        <cc1:ucTextbox ID='txtProcurement' runat="server"></cc1:ucTextbox>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="font-weight: bold;">
                                        <cc1:ucLabel ID='lblSearchBySku' runat="server"></cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold;">
                                        :
                                    </td>
                                    <td>
                                        <cc1:ucTextbox ID='txtSearchBySku' runat="server"></cc1:ucTextbox>
                                    </td>
                                    <td style="font-weight: bold;">
                                        <cc1:ucLabel ID='lblSearchByCatCode' runat="server"></cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold;">
                                        :
                                    </td>
                                    <td>
                                        <cc1:ucTextbox ID='txtSearchByCatCode' runat="server"></cc1:ucTextbox>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="font-weight: bold;">
                                        <cc1:ucLabel ID='lblActionPendingWith' runat="server"></cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold;">
                                        :
                                    </td>
                                    <td>
                                        <cc1:ucDropdownList ID="ddlActionPendingWith" runat="server">
                                            <asp:ListItem Text="--Select--" Value="0"></asp:ListItem>
                                            <asp:ListItem Text="Procurement" Value="1"></asp:ListItem>
                                            <%--OP5  DISPriceProcur_Entry.aspx--%>
                                            <asp:ListItem Text="Account Payable" Value="2"></asp:ListItem>
                                            <%--BS2  DISPriceDebit_Review_Detail.aspx--%>
                                            <asp:ListItem Text="Vendor" Value="3"></asp:ListItem>
                                            <%--screen3-  DISPriceVendor_Price_Query.aspx--%>
                                            <asp:ListItem Text="All" Value="4" Selected="True"></asp:ListItem>
                                            <%--screen3 with no action-  DISPriceVendor_Price_Query.aspx--%>
                                        </cc1:ucDropdownList>
                                    </td>
                                </tr>
                                <td style="font-weight: bold;">
                                    <cc1:ucLabel ID='lblVendor' runat="server"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold;">
                                    :
                                </td>
                                <td colspan="4">
                                    <cc1:MultiSelectVendor runat="server" ID="msVendor" />
                                </td>
                            </table>
                        </cc1:ucView>
                    </cc1:ucMultiView>
                </div>
            </div>
            <%-- <asp:Button ID="btnBack" runat="server" Text="Back" OnClick="BackClick" />
            <asp:Button ID="Button1" runat="server" Text="Next" OnClick="NextClick" />--%>
            <div class="button-row">
                <cc1:ucButton ID="btnSearch" runat="server" class="button" UseSubmitBehavior="false"
                    OnClick="SearchBtnClick" />
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ModuleUI_Discrepancy_PriceQuery_DISPriceReassign_Search : CommonPage
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void SearchClick(object sender, EventArgs e)
    {
        string ddlOpen = ddlOpenWith.SelectedValue;
        if (ddlOpen == "1")
        {
            Response.Redirect("DISPriceReassign_Query_Results.aspx?id=AP");
        }
        else if (ddlOpen == "2")
        {
            Response.Redirect("DISPriceReassign_Query_Results.aspx?id=Proc");
        }
    }

    protected void BackClick(object sender, EventArgs e)
    {
        Response.Redirect("DISPrice_Query_Worklist.aspx");
    }
}
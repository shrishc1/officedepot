﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


public class GridPriceQryResult
{

    public string Site { get; set; }
    public string PriceQueryNo { get; set; }
    public string DebitNo { get; set; }
    public string Status { get; set; }
    public string AP { get; set; }
    public string Procurement { get; set; }
    public string PONo { get; set; }
    public string InvoiceNo { get; set; }
    public string Vendor { get; set; }
    public string APClerk { get; set; }
    public string ProcurementName { get; set; }
    public string DateCreated { get; set; }
    public string CreatedBy { get; set; }
    public string DateClosed { get; set; }
    public string ClosedBy { get; set; }
    public string ElapsedTime { get; set; }
    public string OutCome { get; set; }
}

public partial class ModuleUI_Discrepancy_PriceQuery_DISPriceSearch_Price_Query_Results : CommonPage
{
    protected void Page_Load(object sender, EventArgs e)
    {

        List<GridPriceQryResult> lst = new List<GridPriceQryResult>();
        GridPriceQryResult obj = new GridPriceQryResult
        {
            Site = "ASH-Ashton",
            PriceQueryNo = "PQ000000001",
            DebitNo = "",
            Status = "Pending",
            AP = "Open",
            Procurement = "",
            PONo = "12345",
            InvoiceNo = "98765412345",
            Vendor = "012-CPD",
            APClerk = "O Jurca",
            ProcurementName = "Paul Curement",
            DateCreated = "2/1/2015",
            CreatedBy = "B Crowder",
            DateClosed = "2/1/2015",
            ClosedBy = "B Crowder",
            ElapsedTime = "dd hh mm",
            OutCome = "PO",
        };
        lst.Add(obj);

        obj = new GridPriceQryResult
        {
            Site = "LEI - Leicester",
            PriceQueryNo = "PQ000000002",
            DebitNo = "",
            Status = "Active",
            AP = "Open",
            Procurement = "",
            PONo = "23818",
            InvoiceNo = "38233790",
            Vendor = "025-Acco",
            APClerk = "A Savu",
            ProcurementName = "Brian Currency",
            DateCreated = "2/2/2015",
            CreatedBy = "B Crowder",
            DateClosed = "2/2/2015",
            ClosedBy = "B Crowder",
            ElapsedTime = "dd hh mm",
            OutCome = "Invoice",
        };
        lst.Add(obj);


        grdPurchaseOrderDetail.DataSource = lst;
        grdPurchaseOrderDetail.DataBind();

    }

    protected void BackClick(object sender, EventArgs e)
    {
        Response.Redirect("DISPriceSearch_Price_Query.aspx");
    }

    protected void NextClick(object sender, EventArgs e)
    {
        Response.Redirect("DISPrice_Query_Worklist.aspx");
    }
}


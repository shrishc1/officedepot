﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    CodeFile="DISPriceProcurement_Role.aspx.cs" Inherits="ModuleUI_Discrepancy_PriceQuery_DISPriceProcurement_Role" %>

<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register TagName="ucDate" TagPrefix="cc2" Src="~/CommonUI/UserControls/ucDate.ascx" %>
<%@ Register Src="../UserControl/ucSDRCommunication.ascx" TagName="ucSDRCommunication"
    TagPrefix="uc2" %>
<%@ Register Src="../../../CommonUI/UserControls/ucSite.ascx" TagName="ucSite" TagPrefix="cc2" %>
<%@ Register Src="../UserControl/APAction/APAction.ascx" TagName="APAction" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script language="javascript" src="../../../Scripts/JSON2.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">

    </script>
    <asp:ScriptManager ID="sp1" runat="server">
    </asp:ScriptManager>
    <asp:HiddenField ID="hdnSiteID" runat="server" />
    <asp:UpdatePanel ID="up1" runat="server">
        <ContentTemplate>
            <%--<h2>
                <cc1:ucLabel ID="lblPriceQuery" runat="server" Text="Price Query"></cc1:ucLabel>
            </h2>--%>
            <div class="right-shadow">
                <div class="formbox">
                    <cc1:ucMultiView ID="mvOvers" runat="server" ActiveViewIndex="1">
                        <%--View1--%>
                        <cc1:ucView ID="vwInitialEntry" runat="server">
                            <cc1:ucPanel ID="pnlProcurementUserDetails" runat="server" CssClass="fieldset-form">
                                <table width="100%" cellspacing="1" cellpadding="0" class="form-table">
                                    <tr>
                                        <td style="font-weight: bold;">
                                            <cc1:ucLabel ID="lblName" runat="server"></cc1:ucLabel>
                                        </td>
                                        <td style="font-weight: bold;">
                                            :
                                        </td>
                                        <td class="nobold">
                                            <cc1:ucLabel ID="UcLabel19" runat="server" Text="Joe Procurement"></cc1:ucLabel>
                                        </td>
                                        <td style="font-weight: bold;">
                                            <cc1:ucLabel ID="lblName_1" runat="server"></cc1:ucLabel>
                                        </td>
                                        <td style="font-weight: bold;">
                                            :
                                        </td>
                                        <td class="nobold">
                                            <cc1:ucLabel ID="UcLabel2" runat="server" Text="Joe Procurement"></cc1:ucLabel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: bold;">
                                            <cc1:ucLabel ID="lblPhoneNumber" runat="server"></cc1:ucLabel>
                                        </td>
                                        <td style="font-weight: bold;">
                                            :
                                        </td>
                                        <td class="nobold">
                                            <cc1:ucLabel ID="UcLabel4" runat="server" Text="+44 0121 123 5789"></cc1:ucLabel>
                                        </td>
                                        <td style="font-weight: bold;">
                                            <cc1:ucLabel ID="lblPhoneNumber_1" runat="server"></cc1:ucLabel>
                                        </td>
                                        <td style="font-weight: bold;">
                                            :
                                        </td>
                                        <td class="nobold">
                                            <cc1:ucLabel ID="UcLabel6" runat="server" Text="+44 0121 123 5789"></cc1:ucLabel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: bold;">
                                            <cc1:ucLabel ID="lblCategories" runat="server"></cc1:ucLabel>
                                        </td>
                                        <td style="font-weight: bold;">
                                            :
                                        </td>
                                        <td class="nobold">
                                            <cc1:ucLabel ID="UcLabel8" runat="server" Text="120"></cc1:ucLabel>
                                        </td>
                                    </tr>
                                </table>
                            </cc1:ucPanel>
                            <br />
                            <div class="button-row">
                                <cc1:ucButton ID="btnAdd" runat="server" class="button" OnClick="btnClick" />
                                <cc1:ucButton ID="btnReassign" runat="server" class="button" OnClick="btnClick" />
                                <cc1:ucButton ID="btnRemove" runat="server" class="button" OnClick="btnClick" />
                            </div>
                        </cc1:ucView>
                        <%--2nd view--%>
                        <cc1:ucView ID="vwPriceDetail" runat="server">
                            <div>
                                <table width="100%" cellspacing="1" cellpadding="0" class="form-table">
                                    <tr>
                                        <td style="font-weight: bold;">
                                            <cc1:ucLabel ID="lblDivision" runat="server"></cc1:ucLabel>
                                        </td>
                                        <td style="font-weight: bold;">
                                            :
                                        </td>
                                        <td class="nobold">
                                            <cc1:ucTextbox ID="txtDevision" runat="server"></cc1:ucTextbox>
                                        </td>
                                        <td style="font-weight: bold;">
                                            <cc1:ucLabel ID="lblDivisionName" runat="server" ></cc1:ucLabel>
                                        </td>
                                        <td style="font-weight: bold;">
                                            :
                                        </td>
                                        <td class="nobold">
                                            <cc1:ucTextbox ID="UcTextbox1" runat="server"></cc1:ucTextbox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: bold;">
                                            <cc1:ucLabel ID="lblGroupNo" runat="server" ></cc1:ucLabel>
                                        </td>
                                        <td style="font-weight: bold;">
                                            :
                                        </td>
                                        <td class="nobold">
                                            <cc1:ucTextbox ID="UcTextbox2" runat="server"></cc1:ucTextbox>
                                        </td>
                                        <td style="font-weight: bold;">
                                            <cc1:ucLabel ID="lblGroupName" runat="server" ></cc1:ucLabel>
                                        </td>
                                        <td style="font-weight: bold;">
                                            :
                                        </td>
                                        <td class="nobold">
                                            <cc1:ucTextbox ID="UcTextbox3" runat="server"></cc1:ucTextbox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: bold;">
                                            <cc1:ucLabel ID="lblDepartment" runat="server" ></cc1:ucLabel>
                                        </td>
                                        <td style="font-weight: bold;">
                                            :
                                        </td>
                                        <td class="nobold">
                                            <cc1:ucTextbox ID="UcTextbox4" runat="server"></cc1:ucTextbox>
                                        </td>
                                        <td style="font-weight: bold;">
                                            <cc1:ucLabel ID="lblDeptName" runat="server" ></cc1:ucLabel>
                                        </td>
                                        <td style="font-weight: bold;">
                                            :
                                        </td>
                                        <td class="nobold">
                                            <cc1:ucTextbox ID="UcTextbox5" runat="server"></cc1:ucTextbox>
                                        </td>
                                        <td class="nobold">
                                            <cc1:ucButton ID="btnSearch" runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: bold;">
                                            <cc1:ucLabel ID="lblProcurementName" runat="server" ></cc1:ucLabel>
                                        </td>
                                        <td style="font-weight: bold;">
                                            :
                                        </td>
                                        <td class="nobold">
                                            <cc1:ucLabel ID="UcLabel14" runat="server" Text="Joe Procurement"></cc1:ucLabel>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <br />
                            <cc1:ucGridView ID="grdProcurRole" Width="100%" runat="server" CssClass="grid">
                                <Columns>
                                    <asp:TemplateField HeaderText="SelectAllCheckBox">
                                        <HeaderStyle HorizontalAlign="Center" Width="8%" />
                                        <ItemStyle Width="20%" HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <HeaderTemplate>
                                            <cc1:ucCheckbox runat="server" ID="chkAllOnly" CssClass="checkbox-input" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <cc1:ucCheckbox runat="server" ID="ucchk" CssClass="checkbox-input" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Devision">
                                        <HeaderStyle Width="8%" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="UcLabel1" Text='<%# Eval("Devision") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Div Name">
                                        <HeaderStyle Width="8%" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="UcLabel3" Text='<%# Eval("DivName") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Group No">
                                        <HeaderStyle Width="8%" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="UcLabel5" Text='<%# Eval("GroupNo") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Grp Name">
                                        <HeaderStyle Width="35%" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="UcLabel7" Text='<%# Eval("GrpName") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Dept">
                                        <HeaderStyle Width="5%" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="UcLabel9" Text='<%# Eval("Dept") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Dpt Name">
                                        <HeaderStyle Width="5%" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="UcLabel10" Text='<%# Eval("DptName") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                </Columns>
                            </cc1:ucGridView>
                            <div class="button-row">
                                <cc1:ucButton ID="btnBack" runat="server" class="button" OnClick="ShowView1" />
                            </div>
                            <br />
                        </cc1:ucView>
                    </cc1:ucMultiView>
                    <br />
                    <div>
                        <cc1:ucButton ID="UcButton4" runat="server" Text="Back" class="button" OnClick="BackClick" />
                        <cc1:ucButton ID="UcButton5" runat="server" Text="Next" class="button" OnClick="NextClick" />
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

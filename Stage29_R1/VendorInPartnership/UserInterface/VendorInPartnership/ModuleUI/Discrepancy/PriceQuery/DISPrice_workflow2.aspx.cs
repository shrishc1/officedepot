﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ModuleUI_Discrepancy_PriceQuery_DISPrice_workflow2 : CommonPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string id = Request.QueryString["id"];

        if (id != null && id == "WorkList")
        {
            btnWorklistBack.Visible = true;
            btnBack.Visible = false;
        }
        else
        {
            btnWorklistBack.Visible = false;
            btnBack.Visible = true;
        }
    }

    protected void BackClick(object sender, EventArgs e)
    {
        Response.Redirect("DISPriceDebit_Review_Detail.aspx");//BS2
    }

    protected void WorkListBackClick(object sender, EventArgs e)
    {
        Response.Redirect("DISPriceDebit_Review_Detail.aspx?id=WorkList");//BS2
    }
}
﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    CodeFile="DISPriceVendor_Query.aspx.cs" Inherits="ModuleUI_Discrepancy_PriceQuery_DISPriceVendor_Query" %>

<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register TagName="ucDate" TagPrefix="cc2" Src="~/CommonUI/UserControls/ucDate.ascx" %>
<%@ Register Src="../UserControl/ucSDRCommunication.ascx" TagName="ucSDRCommunication"
    TagPrefix="uc2" %>
<%@ Register Src="../../../CommonUI/UserControls/ucSite.ascx" TagName="ucSite" TagPrefix="cc2" %>
<%@ Register Src="../UserControl/APAction/APAction.ascx" TagName="APAction" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script language="javascript" src="../../../Scripts/JSON2.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">

    </script>
    <asp:ScriptManager ID="sp1" runat="server">
    </asp:ScriptManager>
    <asp:HiddenField ID="hdnSiteID" runat="server" />
    <asp:UpdatePanel ID="up1" runat="server">
        <ContentTemplate>
            <h2>
                <cc1:ucLabel ID="lblVendorqueriesPriceQuery" runat="server" Text=""></cc1:ucLabel>
            </h2>
            <h2>
                <cc1:ucLabel ID="UcLabel23" Text="Price Query - PQ00000001" runat="server"></cc1:ucLabel>
            </h2>
            <div class="right-shadow">
                <div class="formbox">
                    <cc1:ucMultiView ID="mvOvers" runat="server" ActiveViewIndex="0">
                        <cc1:ucView ID="vwInitialEntry" runat="server">
                            <table width="100%" cellspacing="0" cellpadding="0" class="form-table">
                                <tr valign="top">
                                    <td width="30%">
                                        <cc1:ucPanel ID="pnlVendor" runat="server" GroupingText="" CssClass="fieldset-form">
                                            <table width="100%" cellspacing="1" cellpadding="0" class="form-table" height="140px">
                                                <tr>
                                                    <td style="font-weight: bold;">
                                                        <cc1:ucLabel ID="lblVendor" runat="server" Text=""></cc1:ucLabel>
                                                    </td>
                                                    <td style="font-weight: bold;">
                                                        :
                                                    </td>
                                                    <td class="nobold">
                                                        <cc1:ucLabel ID="UcLabel2" runat="server" Text="455-PIK A PAK Home Electrical"></cc1:ucLabel>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="font-weight: bold;">
                                                        <cc1:ucLabel ID="lblContact" runat="server" Text=""></cc1:ucLabel>
                                                    </td>
                                                    <td style="font-weight: bold;">
                                                        :
                                                    </td>
                                                    <td class="nobold">
                                                        <cc1:ucLabel ID="UcLabel4" runat="server" Text="123456789"></cc1:ucLabel>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="font-weight: bold;">
                                                        <cc1:ucLabel ID="VendorVATHash" runat="server" Text=""></cc1:ucLabel>
                                                    </td>
                                                    <td style="font-weight: bold;">
                                                        :
                                                    </td>
                                                    <td class="nobold">
                                                        <cc1:ucLabel ID="UcLabel7" runat="server" Text="1234"></cc1:ucLabel>
                                                    </td>
                                                </tr>
                                            </table>
                                        </cc1:ucPanel>
                                    </td>
                                    <td width="2%">
                                        &nbsp;
                                    </td>
                                    <td width="35%">
                                        <cc1:ucPanel ID="pnlDebitReference" runat="server" GroupingText="" CssClass="fieldset-form">
                                            <table width="100%" cellspacing="1" cellpadding="0" class="form-table" height="140px">
                                                <tr>
                                                    <td style="font-weight: bold;">
                                                        <cc1:ucLabel ID="lblSite" runat="server" Text=""></cc1:ucLabel>
                                                    </td>
                                                    <td style="font-weight: bold;">
                                                        :
                                                    </td>
                                                    <td class="nobold">
                                                        <cc1:ucLabel ID="UcLabel13" runat="server" Text="LEI-Leicester"></cc1:ucLabel>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="font-weight: bold;">
                                                        <cc1:ucLabel ID="lblPurchaseOrderNo" runat="server" Text=""></cc1:ucLabel>
                                                    </td>
                                                    <td style="font-weight: bold;">
                                                        :
                                                    </td>
                                                    <td class="nobold">
                                                        <cc1:ucLabel ID="UcLabel15" runat="server" Text="1234   11/16/2015"></cc1:ucLabel>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="font-weight: bold;">
                                                        <cc1:ucLabel ID="lblInvoiceNumHash" runat="server" Text=""></cc1:ucLabel>
                                                    </td>
                                                    <td style="font-weight: bold;">
                                                        :
                                                    </td>
                                                    <td class="nobold">
                                                        <cc1:ucLabel ID="UcLabel10" runat="server" Text=""></cc1:ucLabel>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="font-weight: bold;">
                                                        <cc1:ucLabel ID="lblOfficeDepotVATHash" runat="server" Text=""></cc1:ucLabel>
                                                    </td>
                                                    <td style="font-weight: bold;">
                                                        :
                                                    </td>
                                                    <td class="nobold">
                                                        <cc1:ucLabel ID="UcLabel20" runat="server" Text=""></cc1:ucLabel>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="font-weight: bold;">
                                                        <cc1:ucLabel ID="lblAPAssociate" runat="server" Text=""></cc1:ucLabel>
                                                    </td>
                                                    <td style="font-weight: bold;">
                                                        :
                                                    </td>
                                                    <td class="nobold">
                                                        <cc1:ucLabel ID="UcLabel22" runat="server" Text="O Jura"></cc1:ucLabel>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="font-weight: bold;">
                                                        <cc1:ucLabel ID="lblProcurementContact" runat="server" Text=""></cc1:ucLabel>
                                                    </td>
                                                    <td style="font-weight: bold;">
                                                        :
                                                    </td>
                                                    <td class="nobold">
                                                        <cc1:ucLabel ID="UcLabel24" runat="server" Text="J Procurement"></cc1:ucLabel>
                                                    </td>
                                                </tr>
                                            </table>
                                        </cc1:ucPanel>
                                    </td>
                                    <td width="2%">
                                        &nbsp;
                                    </td>
                                    <td width="31%" rowspan="7" valign="top">
                                        <cc1:ucPanel ID="pnlCommunicationDetail" runat="server" GroupingText="" CssClass="fieldset-form">
                                            <table width="100%" cellspacing="0" cellpadding="0" class="form-table" height="140px">
                                                <tr>
                                                    <td>
                                                        <uc2:ucSDRCommunication ID="ucSDRCommunication2" runat="server" />
                                                    </td>
                                                    <%--<td>
                                                        <cc1:ucTextarea ID="UcTextarea3" runat="server" Height="50%" Width="95%" Text="pamela.co.uk@centuary.com"></cc1:ucTextarea>
                                                    </td>
                                                    <td>
                                                        <cc1:ucTextarea ID="UcTextarea4" runat="server" Height="50%" Width="95%"></cc1:ucTextarea>
                                                    </td>--%>
                                                </tr>
                                            </table>
                                        </cc1:ucPanel>
                                    </td>
                                </tr>
                            </table>
                            <br />
                            <cc1:ucGridView ID="grdVendorQueryDetails" Width="100%" runat="server" CssClass="grid">
                                <Columns>
                                    <asp:TemplateField HeaderText="Line">
                                        <HeaderStyle Width="8%" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="UcLabel1" Text='<%# Eval("LineNo") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Office Depot Code">
                                        <HeaderStyle Width="8%" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="UcLabel3" Text='<%# Eval("ODCode") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Viking Code">
                                        <HeaderStyle Width="8%" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="UcLabel5" Text='<%# Eval("VikCode") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Vendor Item Code">
                                        <HeaderStyle Width="8%" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="UcLabel6" Text='<%# Eval("VendorCode") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Description">
                                        <HeaderStyle Width="35%" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="UcLabel8" Text='<%# Eval("Desc") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Original PO Quantity">
                                        <HeaderStyle Width="5%" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="UcLabel9" Text='<%# Eval("OriginalPoQty") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Outstanding PO Quantity">
                                        <HeaderStyle Width="5%" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="UcLabel11" Text='<%# Eval("OutstandingPoQty") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="UOM">
                                        <HeaderStyle Width="5%" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="UcLabel12" Text='<%# Eval("UOM") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="PO Item Cost">
                                        <HeaderStyle Width="5%" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="UcLabel14" Text='<%# Eval("POItemCost") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="PO Total Cost">
                                        <HeaderStyle Width="5%" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="UcLabel17" Text='<%# Eval("PoTotalCost") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Invoiced Item Price">
                                        <HeaderStyle Width="5%" HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="UcLabel18" Text='<%# Eval("InvoicedItemPrice") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Qty Invoiced">
                                        <HeaderStyle Width="5%" HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="UcLabel19" Text='<%# Eval("QtyInvoiced") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Invoiced Cost">
                                        <HeaderStyle Width="5%" HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="UcLabel21" Text='<%# Eval("InvoicedCost") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Price Difference">
                                        <HeaderStyle Width="8%" HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="UcLabel22" Text='<%# Eval("PriceDiff") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Correct Price">
                                        <HeaderStyle Width="5%" HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <asp:TextBox ID="TextBox1" runat="server" Width="50px" MaxLength="6"></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </cc1:ucGridView>
                            <br />
                            <cc1:ucPanel ID="pnlVendorsQuery" GroupingText="" runat="server" CssClass="fieldset-form">
                                <div width="90%" height="50px">
                                    <cc1:ucLabel ID="UcLabel16" runat="server" Text="Please double check pricing as this is incorrect"></cc1:ucLabel></div>
                            </cc1:ucPanel>
                            <br />
                            <cc1:ucPanel ID="pnlActionRequired" GroupingText="" runat="server" CssClass="fieldset-form">
                                <cc1:ucLabel ID="UcLabel1" runat="server" CssClass="action-required-heading" isRequired="true"
                                    Font-Size="13px" Text="Please review the above query from the vendor for this Price Query and investigate. After investigation, enter whether you agree or disagree with the vendor's claim."></cc1:ucLabel>
                                <%--<div style="background-color: Orange" width="90%" height="20px">
                                    <cc1:ucLabel ID="xyz" runat="server" Text="Please review the above query from the vendor for this Price Query and investigate. After investigation, enter whether you agree or disagree with the vendor's claim."></cc1:ucLabel></div>--%>
                                <table width="100%" cellspacing="5" cellpadding="0" border="0" class="form-table">
                                    <tr>
                                        <td style="font-weight: bold; text-align: left;">
                                            <cc1:ucRadioButton ID="rdoAcceptQuery" GroupName="ActionReq" runat="server" Text="" />
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <cc1:ucRadioButton ID="rdoRejectQuery" GroupName="ActionReq" runat="server" Text="" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <cc1:ucLabel ID="lblPleaseEnterComments" runat="server"></cc1:ucLabel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: bold;">
                                            <cc1:ucTextarea ID="txtInternalComments" CssClass="inputbox" runat="server" Height="50"
                                                Width="98%"></cc1:ucTextarea>
                                        </td>
                                    </tr>
                                </table>
                            </cc1:ucPanel>
                        </cc1:ucView>
                    </cc1:ucMultiView>
                </div>
            </div>
            <br />
            <div class="button-row">
                <cc1:ucButton ID="btnSave" runat="server" Text="" class="button" UseSubmitBehavior="false" OnClick="BackClick"
                    Visible="true" />
                <cc1:ucButton ID="btnBack" runat="server" Text="" class="button" Visible="true" OnClick="BackClick" />
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <%-- <asp:Button ID="Button1" runat="server" Text="Back" OnClick="BackClick" />
    <asp:Button ID="Button2" runat="server" Text="Next" OnClick="NextClick" />--%>
</asp:Content>

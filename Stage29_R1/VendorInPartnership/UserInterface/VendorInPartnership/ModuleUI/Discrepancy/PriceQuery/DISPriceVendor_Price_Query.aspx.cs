﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ModuleUI_Discrepancy_PriceQuery_DISPriceVendor_Price_Query : CommonPage
{
    protected void Page_Load(object sender, EventArgs e)
    {

        var id = Request.QueryString["id"];

        if (id == "vendor")
        {

            btnRaiseQuery.Visible = true;
            pnlAction.Visible = false;
            UcButton1.Visible = true;
            UcButton2.Visible = false;
            BackDebitLib.Visible = false;
        }

        else if (id == "all")
        {
            //lblVendorPriceQueryView2.Visible = false;
            btnRaiseQuery.Visible = false;
            pnlAction.Visible = false;
            UcButton1.Visible = true;
            UcButton2.Visible = false;
            BackDebitLib.Visible = false;

        }

        else if (id == "DebitLibrary")
        {
            //lblVendorPriceQueryView2.Visible = false;
            btnRaiseQuery.Visible = false;
            pnlAction.Visible = false;
            UcButton1.Visible = false;
            UcButton2.Visible = false;
            BackDebitLib.Visible = true;
        }
        else if (id == "DebitLibVendor")
        {
            //lblVendorPriceQueryView2.Visible = false;
            btnRaiseQuery.Visible = false;
            pnlAction.Visible = false;
            UcButton1.Visible = false;
            UcButton2.Visible = false;
            BackDebitLib.Visible = false;
            BackDebitLibVendor.Visible = true;
        }
        List<GridVendorPQ1> lst1 = new List<GridVendorPQ1>();
        GridVendorPQ1 obj1 = new GridVendorPQ1
        {

            LineNo = "10",
            ODCode = "46589",
            VikCode = "46589",
            VendorCode = "DF46589",
            Desc = "2 in 1 Desk AND CLIP  FAN",
            OriginalPoQty = "123",
            OutstandingPoQty = "20",
            UOM = "",
            POItemCost = "7.19",
            PoTotalCost = "999.41",
            InvoicedItemPrice = "7.2",
            QtyInvoiced = "200",
            PriceDiff = "0.01",
            InvoicedCost = "993.58",
            ValueOfDebit = "200"
        };
        lst1.Add(obj1);


        obj1 = new GridVendorPQ1
        {

            LineNo = "40",
            ODCode = "121784",
            VikCode = "MM30135",
            VendorCode = "DF1210",
            Desc = "DESK FAN SPEED 3 12N",
            OriginalPoQty = "135",
            OutstandingPoQty = "0",
            UOM = "",
            POItemCost = "11.19",
            PoTotalCost = "999.41",
            InvoicedItemPrice = "7.2",
            QtyInvoiced = "200",
            PriceDiff = "0.01",
            InvoicedCost = "993.58",
            ValueOfDebit = "200"
        };
        //lst1.Add(obj1);


        grdVendorPQ1.DataSource = lst1;
        grdVendorPQ1.DataBind();
    }

    protected void BackClick(object sender, EventArgs e)
    {
        var id = Request.QueryString["id"];

        Response.Redirect("DISPriceSearch_Price_Query_Results3.aspx?id=" + id);
    }

    protected void BackDebitLibVendorClick(object sender, EventArgs e)
    {
        var id = Request.QueryString["id"];

        Response.Redirect("DISPrice_PQ_View_Detail.aspx?id=" + id);
    }

    protected void BackDebitLibClick(object sender, EventArgs e)
    {
        //var id = Request.QueryString["id"];

        Response.Redirect("DISPriceDebit_Library.aspx");
    }

    protected void RaiseQryClick(object sender, EventArgs e)
    {
        pnlAction.Visible = true;
        UcButton2.Visible = true;
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Your query has been raised for PQ00000001 number')", true);

    }
}


public class GridVendorPQ1
{

    public string LineNo { get; set; }
    public string ODCode { get; set; }
    public string VikCode { get; set; }
    public string VendorCode { get; set; }
    public string Desc { get; set; }
    public string OriginalPoQty { get; set; }
    public string OutstandingPoQty { get; set; }
    public string UOM { get; set; }
    public string POItemCost { get; set; }
    public string PoTotalCost { get; set; }
    public string InvoicedItemPrice { get; set; }
    public string QtyInvoiced { get; set; }
    public string PriceDiff { get; set; }
    public string InvoicedCost { get; set; }
    public string ValueOfDebit { get; set; }
}
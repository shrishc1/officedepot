﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
 CodeFile="DISPrice_workflow2.aspx.cs" Inherits="ModuleUI_Discrepancy_PriceQuery_DISPrice_workflow2" %>

<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="../UserControl/APAction/APAction.ascx" TagName="APAction" TagPrefix="uc2" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePageMethods="true">
    </asp:ScriptManager>
    <h2>
        <cc1:ucLabel runat="server" ID="lblPriceQueryWorkFlow"></cc1:ucLabel>
    </h2>
    <asp:UpdatePanel ID="updpnlMain" runat="server">
        <ContentTemplate>
            <div>
                <table border="0" id="tblWorkflow1" cellpadding="0" cellspacing="0" width="100%"
                    class="workflow-grid">
                    <thead>
                        <tr style="height: 30px">
                            <th style="width: 250px;">
                                <cc1:ucLabel runat="server" ID="lblAccountsPayable"></cc1:ucLabel>
                            </th>
                            <th style="width: 250px;">
                                <cc1:ucLabel runat="server" ID="lblProcurement"></cc1:ucLabel>
                            </th>
                            <th style="width: 250px;">
                                <cc1:ucLabel runat="server" ID="lblVendor"></cc1:ucLabel>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td valign="top" style="width: 250px;" class="yellow-bg">
                                <table class="yellow-bg">
                                    <tbody>
                                        <tr>
                                            <td style="font-weight: bold">
                                                <cc1:ucLabel runat="server" ID="lblDate"></cc1:ucLabel>
                                            </td>
                                            <td>
                                                :
                                            </td>
                                            <td>
                                                21/10/2015
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: bold">
                                                <cc1:ucLabel runat="server" ID="lblUserName"></cc1:ucLabel>
                                            </td>
                                            <td>
                                                :
                                            </td>
                                            <td>
                                                Super Admin
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: bold">
                                                <cc1:ucLabel runat="server" ID="lblAction"></cc1:ucLabel>
                                            </td>
                                            <td>
                                                :
                                            </td>
                                            <td>
                                                Overs Discrepancy Created
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: bold">
                                                <cc1:ucLabel runat="server" ID="lblPO"></cc1:ucLabel>
                                            </td>
                                            <td>
                                                :
                                            </td>
                                            <td>
                                                12345
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: bold">
                                                <cc1:ucLabel runat="server" ID="lblItem"></cc1:ucLabel>
                                            </td>
                                            <td>
                                                :
                                            </td>
                                            <td>
                                                0321654
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: bold">
                                                <cc1:ucLabel runat="server" ID="lblComments"></cc1:ucLabel>
                                            </td>
                                            <td>
                                                :
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                            <td valign="top" style="width: 250px;" class="blue-bg">
                                <table class="blue-bg">
                                    <tbody>
                                        <tr>
                                            <td colspan="3" align="left" style="font-weight: bold">
                                                Build onto Procurement worklist with following request
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="3" align="left" style="font-style: italic;">
                                                New price query created. Please review details and enter if PO or Invoice is correct
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                            <td valign="top" style="width: 250px;" class="blue-bg">
                                <table style="width: 100%;" class="blue-bg">
                                    <tbody>
                                        <tr>
                                            <td style="font-weight: bold">
                                                <cc1:ucLabel runat="server" ID="lblDate_1"></cc1:ucLabel>
                                            </td>
                                            <td>
                                                :
                                            </td>
                                            <td>
                                                21/10/2015
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: bold">
                                                <cc1:ucLabel runat="server" ID="lblSentTo_1"></cc1:ucLabel>
                                            </td>
                                            <td>
                                                :
                                            </td>
                                            <td>
                                                joeVendor@vendor.com
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: bold" colspan="3">
                                                Notification Communication Sent
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" style="width: 250px;" class="blue-bg">
                                <table class="blue-bg">
                                    <tbody>
                                        <tr>
                                            <td style="font-weight: bold">
                                                <cc1:ucLabel runat="server" ID="lblDate_2"></cc1:ucLabel>
                                            </td>
                                            <td>
                                                :
                                            </td>
                                            <td>
                                                21/10/2015
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: bold">
                                                <cc1:ucLabel runat="server" ID="lblSentTo_2"></cc1:ucLabel>
                                            </td>
                                            <td>
                                                :
                                            </td>
                                            <td>
                                                A SA VU
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="3">
                                                PO price was correct, please confirm debit is cancelled
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                            <td valign="top" style="width: 250px;" class="yellow-bg">
                                <table class="yellow-bg">
                                    <tbody>
                                        <tr>
                                            <td style="font-weight: bold">
                                                <cc1:ucLabel runat="server" ID="lblDate_3"></cc1:ucLabel>
                                            </td>
                                            <td>
                                                :
                                            </td>
                                            <td>
                                                21/10/2015
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: bold">
                                                <cc1:ucLabel runat="server" ID="lblUser"></cc1:ucLabel>
                                            </td>
                                            <td>
                                                :
                                            </td>
                                            <td>
                                                Joe Procurement
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: bold">
                                                <cc1:ucLabel runat="server" ID="lblAction_1"></cc1:ucLabel>
                                            </td>
                                            <td>
                                                :
                                            </td>
                                            <td>
                                                PO Correct, cancel debit
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: bold">
                                                <cc1:ucLabel runat="server" ID="lblItem_1"></cc1:ucLabel>
                                            </td>
                                            <td>
                                                :
                                            </td>
                                            <td>
                                                0291281
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: bold">
                                                <cc1:ucLabel runat="server" ID="lblComments_1"></cc1:ucLabel>
                                            </td>
                                            <td>
                                                :
                                            </td>
                                            <td>
                                                Checked PO 12345 and this had correct price
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                            <td valign="top" style="width: 250px;">
                                <table style="width: 100%;">
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" style="width: 250px;" class="yellow-bg">
                                <table class="yellow-bg">
                                    <tbody>
                                        <tr>
                                            <td style="font-weight: bold">
                                                <cc1:ucLabel runat="server" ID="lblDate_4"></cc1:ucLabel>
                                            </td>
                                            <td>
                                                :
                                            </td>
                                            <td>
                                                21/10/2015
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: bold">
                                                <cc1:ucLabel runat="server" ID="lblUser_1"></cc1:ucLabel>
                                            </td>
                                            <td>
                                                :
                                            </td>
                                            <td>
                                                A SA VU
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: bold">
                                                <cc1:ucLabel runat="server" ID="lblAction_2"></cc1:ucLabel>
                                            </td>
                                            <td>
                                                :
                                            </td>
                                            <td>
                                                Debit Cancelled
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: bold">
                                                <cc1:ucLabel runat="server" ID="lblComments_2"></cc1:ucLabel>
                                            </td>
                                            <td>
                                                :
                                            </td>
                                            <td>
                                                Done
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                            <td valign="top" style="width: 250px;">
                                <table style="width: 100%;">
                                </table>
                            </td>
                            <td valign="top" style="width: 250px;" class="blue-bg">
                                <table class="blue-bg">
                                    <tbody>
                                        <tr>
                                            <td style="font-weight: bold">
                                                <cc1:ucLabel runat="server" ID="lblDate_5"></cc1:ucLabel>
                                            </td>
                                            <td>
                                                :
                                            </td>
                                            <td>
                                                21/10/2015
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: bold">
                                                <cc1:ucLabel runat="server" ID="lblSentTo_3"></cc1:ucLabel>
                                            </td>
                                            <td>
                                                :
                                            </td>
                                            <td>
                                                joevendor@vendor.com
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="3">
                                                Debit cancellation sent
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: bold">
                                                <cc1:ucLabel runat="server" ID="lblItem_3"></cc1:ucLabel>
                                            </td>
                                            <td>
                                                :
                                            </td>
                                            <td>
                                                0291281
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: bold">
                                                <cc1:ucLabel runat="server" ID="lblComments_5"></cc1:ucLabel>
                                            </td>
                                            <td>
                                                :
                                            </td>
                                            <td>
                                                Checked PO 12345 and this had correct price
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <br />
            <div class="button-row">
                <cc1:ucButton ID="btnBack" runat="server" class="button" OnClick="BackClick" />
                <cc1:ucButton ID="btnWorklistBack" Text="Back" runat="server" class="button" OnClick="WorkListBackClick" />
            </div>
            <%--<asp:Button ID="btnBack" runat="server" Text="Back" OnClick="BackClick" />
            <asp:Button ID="Button1" runat="server" Text="Next" OnClick="NextClick" />--%>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

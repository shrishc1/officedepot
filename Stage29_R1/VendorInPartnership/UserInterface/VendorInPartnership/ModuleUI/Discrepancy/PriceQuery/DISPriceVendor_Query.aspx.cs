﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ModuleUI_Discrepancy_PriceQuery_DISPriceVendor_Query : CommonPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        List<GridVendorDetails> lst = new List<GridVendorDetails>();
        GridVendorDetails obj = new GridVendorDetails
        {

            LineNo = "10",
            ODCode = "46589",
            VikCode = "46589",
            VendorCode = "DF46589",
            Desc = "2 in 1 Desk AND CLIP  FAN",
            OriginalPoQty = "123",
            OutstandingPoQty = "20",
            UOM = "",
            POItemCost = "7.19",
            PoTotalCost = "999",
            InvoicedItemPrice = "7.15",
            QtyInvoiced = "139",
            InvoicedCost = "998.85",
            PriceDiff = "5.56",
            Correctprice = "",

        };
        lst.Add(obj);

        obj = new GridVendorDetails
        {

            LineNo = "40",
            ODCode = "121784",
            VikCode = "MM30135",
            VendorCode = "DF1210",
            Desc = "DESK FAN WE 3 SPEED 12IN",
            OriginalPoQty = "315",
            OutstandingPoQty = "20",
            UOM = "",
            POItemCost = "11.11",
            PoTotalCost = "3499.65",
            InvoicedItemPrice = "11.01",
            QtyInvoiced = "310",
            InvoicedCost = "3413.1",
            PriceDiff = "86.55",
            Correctprice = "PO",

        };
        lst.Add(obj);

        grdVendorQueryDetails.DataSource = lst;
        grdVendorQueryDetails.DataBind();
    }


    protected void BackClick(object sender, EventArgs e)
    {
        string id = Request.QueryString["id"];

        Response.Redirect("DISPrice_Disputed_PQ_Search_Result.aspx?id=" + id); ;
    }
}

public class GridVendorDetails
{
    public string LineNo { get; set; }
    public string ODCode { get; set; }
    public string VikCode { get; set; }
    public string VendorCode { get; set; }
    public string Desc { get; set; }
    public string OriginalPoQty { get; set; }
    public string OutstandingPoQty { get; set; }
    public string UOM { get; set; }
    public string POItemCost { get; set; }
    public string PoTotalCost { get; set; }
    public string InvoicedItemPrice { get; set; }
    public string QtyInvoiced { get; set; }
    public string InvoicedCost { get; set; }
    public string PriceDiff { get; set; }
    public string Correctprice { get; set; }
}
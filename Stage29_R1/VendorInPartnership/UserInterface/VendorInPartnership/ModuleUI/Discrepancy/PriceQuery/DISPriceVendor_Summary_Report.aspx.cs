﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


public class GridPQByVendorSummary
{

    public string Vendor { get; set; }
    public string Debits { get; set; }
    public string Actioned { get; set; }
    public string Approved { get; set; }
    public string Cancelled { get; set; }
    public string Outstanding { get; set; }
    public string TotalValue { get; set; }
    public string ValueDebited { get; set; }
    public string ValueCancelled { get; set; }
    public string NotActionedYet { get; set; }

}


public partial class ModuleUI_Discrepancy_PriceQuery_DISPriceVendor_Summary_Report : CommonPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        List<GridPQByVendorSummary> lst2 = new List<GridPQByVendorSummary>();
        GridPQByVendorSummary obj2 = new GridPQByVendorSummary
        {
            Vendor = "012-CPD",
            Debits = "391",
            Actioned = "388",
            Approved = "120",
            Cancelled = "265",
            Outstanding = "3",
            TotalValue = "12319.34",
            ValueDebited = "4091.44",
            ValueCancelled = "7188.51",
            NotActionedYet = "1039.39",
        };
        lst2.Add(obj2);

        obj2 = new GridPQByVendorSummary
        {
            Vendor = "025-Acco",
            Debits = "11",
            Actioned = "7",
            Approved = "5",
            Cancelled = "2",
            Outstanding = "4",
            TotalValue = "124.44",
            ValueDebited = "80",
            ValueCancelled = "12.1",
            NotActionedYet = "32.34",
        };
        lst2.Add(obj2);

        grdPQByVendorSummary.DataSource = lst2;
        grdPQByVendorSummary.DataBind();
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        Response.Redirect("DISPrice_Query_Reports.aspx");
    }
}
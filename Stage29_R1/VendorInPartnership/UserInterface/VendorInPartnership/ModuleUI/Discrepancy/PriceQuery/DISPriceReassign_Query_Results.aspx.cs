﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ModuleUI_Discrepancy_PriceQuery_DISPriceReassign_Query_Results : CommonPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string id = Request.QueryString["id"];

        if (id == "Proc")
        {
            ddlProc.Visible = true;
            ddlAP.Visible = false;
        }
        else if (id == "AP")
        {
            ddlProc.Visible = false;
            ddlAP.Visible = true;
        }

        List<GridReassQryResults> lst = new List<GridReassQryResults>();
        GridReassQryResults obj = new GridReassQryResults
        {

            DebitNo = "D0000001",
            Site = "ASH-Ashton",
            Vendor = "012-CPD",
            ODSku = "0139811",
            CatCode = "123-BK",
            Description = "PEN BLACK",
            APName = "Oana Jurca",
            ProcurementName = "Joe Procurement",

        };
        lst.Add(obj);

        obj = new GridReassQryResults
        {

            DebitNo = "D0000002",
            Site = "LEI-Leicester",
            Vendor = "012-CPD",
            ODSku = "3928128",
            CatCode = "EE-123",
            Description = "FOLDER BLUE",
            APName = "Alina Savu",
            ProcurementName = "Martin Procurement",

        };
        lst.Add(obj);

        grdReassQryResults.DataSource = lst;
        grdReassQryResults.DataBind();
    }

    protected void BackClick(object sender, EventArgs e)
    {
        Response.Redirect("DISPriceReassign_Search.aspx");
    }

    protected void NextClick(object sender, EventArgs e)
    {
        Response.Redirect("DISPriceProcurement_Role.aspx");
    }
}

public class GridReassQryResults
{
    public string DebitNo { get; set; }
    public string Site { get; set; }
    public string Vendor { get; set; }
    public string ODSku { get; set; }
    public string CatCode { get; set; }
    public string Description { get; set; }
    public string APName { get; set; }
    public string ProcurementName { get; set; }



}
﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    CodeFile="DISPriceReassign_Query_Results.aspx.cs" Inherits="ModuleUI_Discrepancy_PriceQuery_DISPriceReassign_Query_Results" %>

<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register TagName="ucDate" TagPrefix="cc2" Src="~/CommonUI/UserControls/ucDate.ascx" %>
<%@ Register Src="../UserControl/ucSDRCommunication.ascx" TagName="ucSDRCommunication"
    TagPrefix="uc2" %>
<%@ Register Src="../../../CommonUI/UserControls/ucSite.ascx" TagName="ucSite" TagPrefix="cc2" %>
<%@ Register Src="../UserControl/APAction/APAction.ascx" TagName="APAction" TagPrefix="uc2" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectVendor.ascx" TagName="MultiSelectVendor"
    TagPrefix="cc3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script language="javascript" src="../../../Scripts/JSON2.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        function reassignClick() {

            var chkBox = document.getElementById("ctl00_ContentPlaceHolder1_grdReassQryResults_ctl02_UcCheckbox1");
            var chkBox2 = document.getElementById("ctl00_ContentPlaceHolder1_grdReassQryResults_ctl03_UcCheckbox1");
            //
            if (chkBox.checked || chkBox2.checked) {

                if (confirm('Are you sure to reassign price queries?')) {
                    return true;
                } else {
                    return false;
                }
            }
            else {
                alert("Plaese select a checkbox");
            }
        }
    </script>
    <asp:ScriptManager ID="sp1" runat="server">
    </asp:ScriptManager>
    <asp:HiddenField ID="hdnSiteID" runat="server" />
    <asp:UpdatePanel ID="up1" runat="server">
        <ContentTemplate>
            <h2>
                <cc1:ucLabel ID="lblReassignPriceQueryResults" runat="server"></cc1:ucLabel>
            </h2>
            <div class="right-shadow">
                <div class="formbox">
                    <cc1:ucMultiView ID="mvOvers" runat="server" ActiveViewIndex="0">
                        <cc1:ucView ID="vwPriceDetail" runat="server">
                            <cc1:ucGridView ID="grdReassQryResults" Width="100%" runat="server" CssClass="grid">
                                <Columns>
                                    <asp:TemplateField HeaderText="">
                                        <HeaderStyle Width="4%" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucCheckbox runat="server" ID="UcCheckbox1"></cc1:ucCheckbox>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Debit #">
                                        <HeaderStyle Width="8%" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="UcLabel1" Text='<%# Eval("DebitNo") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Site">
                                        <HeaderStyle Width="8%" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="UcLabel2" Text='<%# Eval("Site") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="OD SKU">
                                        <HeaderStyle Width="8%" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="UcLabel3" Text='<%# Eval("ODSku") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="CAT Code">
                                        <HeaderStyle Width="8%" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="UcLabel4" Text='<%# Eval("CatCode") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Description">
                                        <HeaderStyle Width="8%" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="UcLabel5" Text='<%# Eval("Description") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="AP Name">
                                        <HeaderStyle Width="8%" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="UcLabel6" Text='<%# Eval("APName") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Procurement Name">
                                        <HeaderStyle Width="8%" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="UcLabel7" Text='<%# Eval("ProcurementName") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                </Columns>
                            </cc1:ucGridView>
                            <br />
                            <div class="button-row">
                                <div>
                                    <cc1:ucLabel runat="server" ID="lblReassignTo"></cc1:ucLabel>:
                                    <cc1:ucDropdownList ID="ddlAP" runat="server" Width="150px" AutoPostBack="True">
                                        <asp:ListItem Text="--select--" Value="0"></asp:ListItem>
                                        <asp:ListItem Text="Barry Buyer" Value="1" Selected="True"></asp:ListItem>
                                        <asp:ListItem Text="Alina Savu" Value="2"></asp:ListItem>
                                    </cc1:ucDropdownList>
                                    <cc1:ucDropdownList ID="ddlProc" runat="server" Width="150px" AutoPostBack="True">
                                        <asp:ListItem Text="--select--" Value="0"></asp:ListItem>
                                        <asp:ListItem Text="Joe Procurement" Value="1" Selected="True"></asp:ListItem>
                                        <asp:ListItem Text="Martin Procurement" Value="2"></asp:ListItem>
                                    </cc1:ucDropdownList>
                                </div>
                                <br />
                            </div>
                        </cc1:ucView>
                    </cc1:ucMultiView>
                </div>
            </div>
            <div class="button-row">
                <cc1:ucButton ID="btnReassign" OnClientClick=" return reassignClick();" runat="server"
                    class="button" />
                <cc1:ucButton ID="btnBack" runat="server" class="button" OnClick="BackClick" />
            </div>
            <%--<asp:Button ID="btnBack_1" runat="server" Text="Back" OnClick="BackClick" />
            <asp:Button ID="btnNext" runat="server" Text="Next" OnClick="NextClick" />--%>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

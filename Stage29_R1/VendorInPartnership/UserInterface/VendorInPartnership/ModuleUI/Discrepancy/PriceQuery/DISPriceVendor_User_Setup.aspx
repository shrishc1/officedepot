﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    CodeFile="DISPriceVendor_User_Setup.aspx.cs" Inherits="ModuleUI_Discrepancy_PriceQuery_DISPriceVendor_User_Setup" %>

<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register TagName="ucDate" TagPrefix="cc2" Src="~/CommonUI/UserControls/ucDate.ascx" %>
<%@ Register Src="../UserControl/ucSDRCommunication.ascx" TagName="ucSDRCommunication"
    TagPrefix="uc2" %>
<%@ Register Src="../../../CommonUI/UserControls/ucSite.ascx" TagName="ucSite" TagPrefix="cc2" %>
<%@ Register Src="../UserControl/APAction/APAction.ascx" TagName="APAction" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script language="javascript" src="../../../Scripts/JSON2.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">

    </script>
    <asp:ScriptManager ID="sp1" runat="server">
    </asp:ScriptManager>
    <asp:HiddenField ID="hdnSiteID" runat="server" />
    <asp:UpdatePanel ID="up1" runat="server">
        <ContentTemplate>
            <h2>
                <cc1:ucLabel ID="lblVendorUserSetup" runat="server" Text=""></cc1:ucLabel>
            </h2>
            <div class="right-shadow">
                <div class="formbox">
                    <cc1:ucMultiView ID="mvOvers" runat="server" ActiveViewIndex="0">
                        <%--View1--%>
                        <cc1:ucView ID="vwInitialEntry" runat="server">
                            <cc1:ucPanel ID="UcPanel1" runat="server" GroupingText="" CssClass="fieldset-form">
                                <table width="100%" cellspacing="1" cellpadding="0" class="form-table">
                                    <tr>
                                        <td>
                                            <table>
                                                <tr>
                                                    <td>
                                                        <cc1:ucButton runat="server" ID="btnOTIF" Text="" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <cc1:ucLabel ID="lblOTIF1" runat="server" Text="Please select which countries you require to receive your On Time In Full reports for"></cc1:ucLabel>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <cc1:ucCheckbox runat="server" ID="chkOTIF1" Text="UKandIRE" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table>
                                                <tr>
                                                    <td>
                                                        <cc1:ucButton runat="server" ID="btnScorecard" Text="" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <cc1:ucLabel ID="lblScorecard1" runat="server" Text="Please select the countries you require to view your evaluation of performance"></cc1:ucLabel>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <cc1:ucCheckbox runat="server" ID="chkScorecard1" Text="UKandIRE" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table>
                                                <tr>
                                                    <td>
                                                        <cc1:ucButton runat="server" ID="btnInventory" Text="" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <cc1:ucLabel ID="lblInventory1" runat="server" Text="Please select the countries where you are the contact for our inventory team"></cc1:ucLabel>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <cc1:ucCheckbox runat="server" ID="chkInventory1" Text="UKandIRE" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </cc1:ucPanel>
                        </cc1:ucView>
                    </cc1:ucMultiView>
                    <br />
                </div>
            </div>
            <div>
                <cc1:ucButton ID="btnBack" runat="server" Text="Back" class="button" OnClick="BackClick" />
                <cc1:ucButton ID="btnNext" runat="server" Text="Next" class="button" OnClick="NextClick" />
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

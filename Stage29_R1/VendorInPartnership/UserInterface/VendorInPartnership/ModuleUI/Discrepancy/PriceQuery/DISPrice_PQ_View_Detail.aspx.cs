﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ModuleUI_Discrepancy_PriceQuery_DISPrice_PQ_View_Detail : CommonPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        List<GridVendorPQ2> lst2 = new List<GridVendorPQ2>();
        GridVendorPQ2 obj2 = new GridVendorPQ2
        {

            Status = "Active",
            Site = "ASH- Ashton",
            PriceQueryNum = "PQ0000001",
            DebitNoteNo = "DNUK000001",
            VendorNo = "012-CPD",
            DebitRaiseDate = "1/1/2015",
            RaisedBy = "O Jurca",
            PONum = "12345",
            VendorItemCode = "239238-19",
            ValueOfDebit = "108.01",
            Currency = "Pound",
            SentTo = "joe.vendor@vendor.com",
        };
        lst2.Add(obj2);

        obj2 = new GridVendorPQ2
        {

            Status = "Active",
            Site = "LEI-Leicester",
            PriceQueryNum = "PQ0000002",
            DebitNoteNo = "DNUK000002",
            VendorNo = "23948-CPD",
            DebitRaiseDate = "1/2/2015",
            RaisedBy = "A Savu",
            PONum = "54321",
            VendorItemCode = "S29238",
            ValueOfDebit = "28.87",
            Currency = "Pound",
            SentTo = "joe.vendor@vendor.com",
        };
        lst2.Add(obj2);

        UcGridView1.DataSource = lst2;
        UcGridView1.DataBind();
    }
    //

    protected void BackBtnClick(object sender, EventArgs e)
    {
        Response.Redirect("DISPriceVendor_PQ_View.aspx");
    }

}


public class GridVendorPQ2
{

    public string Status { get; set; }
    public string Site { get; set; }
    public string PriceQueryNum { get; set; }
    public string DebitNoteNo { get; set; }
    public string VendorNo { get; set; }
    public string DebitRaiseDate { get; set; }
    public string RaisedBy { get; set; }
    public string PONum { get; set; }
    public string VendorItemCode { get; set; }
    public string ValueOfDebit { get; set; }
    public string Currency { get; set; }
    public string SentTo { get; set; }
}
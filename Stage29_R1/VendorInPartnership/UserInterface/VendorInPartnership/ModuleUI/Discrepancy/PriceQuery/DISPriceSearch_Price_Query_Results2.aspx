﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
 CodeFile="DISPriceSearch_Price_Query_Results2.aspx.cs" Inherits="ModuleUI_Discrepancy_PriceQuery_DISPriceSearch_Price_Query_Results2" %>

<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register TagName="ucDate" TagPrefix="cc2" Src="~/CommonUI/UserControls/ucDate.ascx" %>
<%@ Register Src="../UserControl/ucSDRCommunication.ascx" TagName="ucSDRCommunication"
    TagPrefix="uc2" %>
<%@ Register Src="../../../CommonUI/UserControls/ucSite.ascx" TagName="ucSite" TagPrefix="cc2" %>
<%@ Register Src="../UserControl/APAction/APAction.ascx" TagName="APAction" TagPrefix="uc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script language="javascript" src="../../../Scripts/JSON2.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">

    </script>
    <asp:ScriptManager ID="sp1" runat="server">
    </asp:ScriptManager>
    <asp:HiddenField ID="hdnSiteID" runat="server" />
    <asp:UpdatePanel ID="up1" runat="server">
        <ContentTemplate>
            <h2>
                <cc1:ucLabel ID="lblSearchPriceQueryResults" runat="server"></cc1:ucLabel>
            </h2>
            <div class="right-shadow">
                <div class="formbox">
                    <cc1:ucMultiView ID="mvOvers" runat="server" ActiveViewIndex="0">
                        <cc1:ucView ID="vwPriceDetail" runat="server">
                            <cc1:ucGridView ID="grdPurchaseOrderDetail" Width="100%" runat="server" CssClass="grid">
                                <Columns>
                                    <asp:TemplateField HeaderText="Site">
                                        <HeaderStyle Width="8%" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="UcLabel1" Text='<%# Eval("Site") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Price Query #">
                                        <HeaderStyle Width="8%" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                        <%--Redirect to BS2 screen--%>
                                            <asp:HyperLink ID="hplImageName" runat="server" Text='<%# Eval("PriceQueryNo") %>'
                                                NavigateUrl='<%# EncryptQuery("DISPriceDebit_Review_Detail.aspx")%>'></asp:HyperLink>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Debit #">
                                        <HeaderStyle Width="8%" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="UcLabel3" Text='<%# Eval("DebitNo") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Status">
                                        <HeaderStyle Width="8%" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="UcLabel4" Text='<%# Eval("Status") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="AP">
                                        <HeaderStyle Width="8%" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="UcLabel5" Text='<%# Eval("AP") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Procurement">
                                        <HeaderStyle Width="8%" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="UcLabel6" Text='<%# Eval("Procurement") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="PO #">
                                        <HeaderStyle Width="8%" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="UcLabel7" Text='<%# Eval("PONo") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Invoice #">
                                        <HeaderStyle Width="8%" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="UcLabel8" Text='<%# Eval("InvoiceNo") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Vendor">
                                        <HeaderStyle Width="8%" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="UcLabel9" Text='<%# Eval("Vendor") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="AP Clerk">
                                        <HeaderStyle Width="8%" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="UcLabel10" Text='<%# Eval("APClerk") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Procurement Name">
                                        <HeaderStyle Width="8%" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="UcLabel11" Text='<%# Eval("ProcurementName") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Date Created">
                                        <HeaderStyle Width="8%" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="UcLabel12" Text='<%# Eval("DateCreated") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Created By">
                                        <HeaderStyle Width="8%" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="UcLabel13" Text='<%# Eval("CreatedBy") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Date Closed">
                                        <HeaderStyle Width="8%" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="UcLabel14" Text='<%# Eval("DateClosed") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Closed By">
                                        <HeaderStyle Width="8%" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="UcLabel15" Text='<%# Eval("ClosedBy") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Elapsed Time">
                                        <HeaderStyle Width="8%" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="UcLabel16" Text='<%# Eval("ElapsedTime") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Outcome">
                                        <HeaderStyle Width="8%" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="UcLabel17" Text='<%# Eval("Outcome") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                </Columns>
                            </cc1:ucGridView>
                        </cc1:ucView>
                    </cc1:ucMultiView>
                </div>
            </div>
            <div class="button-row">
                <asp:Button ID="Button2" runat="server" Text="Back" OnClick="BackClick" />
            </div>
            <%-- <asp:Button ID="btnBack" runat="server" Text="Back" OnClick="BackClick" />
            <asp:Button ID="Button1" runat="server" Text="Next" OnClick="NextClick" />--%>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

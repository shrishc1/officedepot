﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ModuleUI_Discrepancy_PriceQuery_DISPriceVendor_PQ_View : CommonPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //List<GridVendorPQ1> lst1 = new List<GridVendorPQ1>();
        //GridVendorPQ1 obj1 = new GridVendorPQ1
        //{

        //    LineNo = "10",
        //    ODCode = "46589",
        //    VikCode = "46589",
        //    VendorCode = "DF46589",
        //    Desc = "2 in 1 Desk AND CLIP  FAN",
        //    OriginalPoQty = "123",
        //    OutstandingPoQty = "20",
        //    UOM = "",
        //    POItemCost = "7.19",
        //    PoTotalCost = "999.41",
        //    InvoicedItemPrice = "7.2",
        //    QtyInvoiced = "200",
        //    PriceDiff = "0.01",
        //    InvoicedCost = "993.58",
        //    ValueOfDebit = "200"
        //};
        //lst1.Add(obj1);


        //obj1 = new GridVendorPQ1
        //{

        //    LineNo = "40",
        //    ODCode = "121784",
        //    VikCode = "MM30135",
        //    VendorCode = "DF1210",
        //    Desc = "DESK FAN SPEED 3 12N",
        //    OriginalPoQty = "135",
        //    OutstandingPoQty = "0",
        //    UOM = "",
        //    POItemCost = "11.19",
        //    PoTotalCost = "999.41",
        //    InvoicedItemPrice = "7.2",
        //    QtyInvoiced = "200",
        //    PriceDiff = "0.01",
        //    InvoicedCost = "993.58",
        //    ValueOfDebit = "200"
        //};
        //lst1.Add(obj1);


        //grdVendorPQ1.DataSource = lst1;
        //grdVendorPQ1.DataBind();


        //List<GridVendorPQ2> lst2 = new List<GridVendorPQ2>();
        //GridVendorPQ2 obj2 = new GridVendorPQ2
        //{

        //    Status = "Active",
        //    Site = "ASH- Ashton",
        //    DebitNoteNo = "DO123456789",
        //    VendorNo = "012-CPD",
        //    DebitRaiseDate = "1/1/2015",
        //    RaisedBy = "O Jurca",
        //    PONum = "12345",
        //    VendorItemCode = "239238-19",
        //    ValueOfDebit = "108.01",
        //    Currency = "Pound",
        //    SentTo = "joe.vendor@vendor.com",
        //};
        //lst2.Add(obj2);

        //obj2 = new GridVendorPQ2
        //{

        //    Status = "Active",
        //    Site = "LEI-Leicester",
        //    DebitNoteNo = "D04847818",
        //    VendorNo = "23948-CPD",
        //    DebitRaiseDate = "1/2/2015",
        //    RaisedBy = "A Savu",
        //    PONum = "54321",
        //    VendorItemCode = "S29238",
        //    ValueOfDebit = "28.87",
        //    Currency = "Pound",
        //    SentTo = "joe.vendor@vendor.com",
        //};
        //lst2.Add(obj2);

        //UcGridView1.DataSource = lst2;
        //UcGridView1.DataBind();
    }

    protected void BackClick(object sender, EventArgs e)
    {
        Response.Redirect("DISPriceVendor_User_Setup.aspx");
    }

    protected void NextClick(object sender, EventArgs e)
    {
        Response.Redirect("DISPrice_workflow.aspx");
    }
    protected void DebitNo_Clicked(object sender, EventArgs e)
    {
        //hypDebitNoteNumber.NavigateUrl = EncryptQuery("PriceQuery/DISPriceVendor_Price_Query.aspx");
        //Response.Redirect("DISPriceVendor_Price_Query.aspx");
    }
    protected void SearchBtnClick(object sender, EventArgs e)
    {
        Response.Redirect("DISPrice_PQ_View_Detail.aspx");
    }
}


//public class GridVendorPQ2
//{

//    public string Status { get; set; }
//    public string Site { get; set; }
//    public string DebitNoteNo { get; set; }
//    public string VendorNo { get; set; }
//    public string DebitRaiseDate { get; set; }
//    public string RaisedBy { get; set; }
//    public string PONum { get; set; }
//    public string VendorItemCode { get; set; }
//    public string ValueOfDebit { get; set; }
//    public string Currency { get; set; }
//    public string SentTo { get; set; }
//}

//public class GridVendorPQ1
//{

//    public string LineNo { get; set; }
//    public string ODCode { get; set; }
//    public string VikCode { get; set; }
//    public string VendorCode { get; set; }
//    public string Desc { get; set; }
//    public string OriginalPoQty { get; set; }
//    public string OutstandingPoQty { get; set; }
//    public string UOM { get; set; }
//    public string POItemCost { get; set; }
//    public string PoTotalCost { get; set; }
//    public string InvoicedItemPrice { get; set; }
//    public string QtyInvoiced { get; set; }
//    public string PriceDiff { get; set; }
//    public string InvoicedCost { get; set; }
//    public string ValueOfDebit { get; set; }
//}


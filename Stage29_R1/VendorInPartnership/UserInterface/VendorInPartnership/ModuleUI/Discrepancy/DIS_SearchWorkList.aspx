﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="DIS_SearchWorkList.aspx.cs"
    MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master" Inherits="DIS_SearchWorkList" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2>
        <cc1:ucLabel ID="lblWorklistSelectionCriteria" runat="server" Text="Worklist Selection Criteria"></cc1:ucLabel>
    </h2>
    <div class="right-shadow">
        <div class="formbox">
            <table width="50%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                <tr>
                    <td style="font-weight: bold;">
                        <cc1:ucLabel ID="UcLabel5" runat="server" Text="Goods In Site"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold;">
                        <cc1:ucLabel ID="UcLabel6" runat="server">:</cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold;">
                        <cc1:ucDropdownList ID="ddlSite" runat="server" Width="150px">
                        </cc1:ucDropdownList>
                    </td>
                    <td style="font-weight: bold;">
                        <cc1:ucCheckbox ID="UcCheckbox2" runat="server" />
                        &nbsp;
                        <cc1:ucLabel ID="UcLabel7" runat="server" Text="All"></cc1:ucLabel>
                    </td>
                </tr>
                <tr>
                    <td style="font-weight: bold; width: 40%">
                        <cc1:ucLabel ID="lblAccountPaybleClark" runat="server" Text="Account Payble Clark"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold; width: 5%">
                        <cc1:ucLabel ID="Label17" runat="server">:</cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold; width: 45%">
                        <cc1:ucDropdownList ID="ddlAccountPaybleClark" runat="server" Width="150px">
                        </cc1:ucDropdownList>
                    </td>
                    <td style="font-weight: bold; width: 10%">
                        <cc1:ucCheckbox ID="chkClarkAll" runat="server" />
                        &nbsp;
                        <cc1:ucLabel ID="UcLabel1" runat="server" Text="All"></cc1:ucLabel>
                    </td>
                </tr>
                <tr>
                    <td style="font-weight: bold;">
                        <cc1:ucLabel ID="UcLabel2" runat="server" Text="Rebuyer"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold;">
                        <cc1:ucLabel ID="UcLabel3" runat="server">:</cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold;">                        
                        <cc1:ucTextbox ID="UcTextbox1" runat="server" Width="144px"></cc1:ucTextbox>
                    </td>
                    <td style="font-weight: bold;">
                        &nbsp;                        
                    </td>
                </tr>

                 <tr>
                    <td style="font-weight: bold;">
                        <cc1:ucLabel ID="UcLabel9" runat="server" Text="Vendor Name"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold;">
                        <cc1:ucLabel ID="UcLabel10" runat="server">:</cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold;">
                        <cc1:ucTextbox ID="UcTextbox2" runat="server" Width="144px"></cc1:ucTextbox>                       
                    </td>
                    <td style="font-weight: bold;">
                        &nbsp;
                    </td>
                </tr>

            </table>
        </div>
    </div>
    <div class="button-row">
        <cc1:ucButton ID="btnSearch" runat="server" Text="Search" CssClass="button" OnClick="btnSearch_Click" />
    </div>
</asp:Content>

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using BaseControlLibrary;
using BusinessEntities.ModuleBE.Discrepancy.LogDiscrepancy;
using BusinessLogicLayer.ModuleBAL.Discrepancy.LogDiscrepancy;
using WebUtilities;

public partial class DISLog_QueryDiscrepancy : CommonPage
{
    #region Global Declaration ...
    System.Text.StringBuilder sSendMailLink = new System.Text.StringBuilder();
    protected string ValidEmail = WebCommon.getGlobalResourceValue("ValidEmail");
    protected string NoEmailSpecifiedMessage = WebCommon.getGlobalResourceValue("NoEmailSpecifiedMessage");
    string strThisDiscrepancyWasNotFoundOrIsNotLinkedToYourAccount = WebCommon.getGlobalResourceValue("ThisDiscrepancyWasNotFoundOrIsNotLinkedToYourAccount");
    string strPleaseEnterTheCorrectDiscrepancyNumberOrUseSearchDiscrepancies = WebCommon.getGlobalResourceValue("PleaseEnterTheCorrectDiscrepancyNumberOrUseSearchDiscrepancies");
    string strThereIsCurrentlyAlreadyAnOpenQueryBeingInvestigated = WebCommon.getGlobalResourceValue("ThereIsCurrentlyAlreadyAnOpenQueryBeingInvestigated");
    string strPleaseWaitUntilYouHaveReceivedAReplyForThisBefore = WebCommon.getGlobalResourceValue("PleaseWaitUntilYouHaveReceivedAReplyForThisBefore");
    protected string EncURL = string.Empty;
    protected string EncryptcUrl = string.Empty;
    #endregion

    #region Events ...


    protected void Page_Init(object sender, EventArgs e)
    {
        ddlSite.CurrentPage = this;
        ddlSite.IsAllRequired = true;
    }
    protected void Page_Load(object sender, EventArgs e)
    {

        EncURL = "../Discrepancy/Report/DiscrepancyDisputesReport.aspx" ;

        ucExportToExcel1.CurrentPage = this;
        ucExportToExcel1.GridViewControl = gvExport;
        ucExportToExcel1.FileName = "DiscrepancyDispute";

        if (Session["UserID"] != null) // To show View Dispute button to Vendor only
        {
            if (Session["Role"].ToString().ToLower() == "vendor")
                btnViewDisportReport.Visible = true;
        }

        this.GetVendorEmailIds();
        if (!IsPostBack)
        {
            BindDisputedDis();
            if (GetQueryStringValue("disLogID") != null && GetQueryStringValue("VDRNo") != null)
            {
                this.BindActionsRadiobutton();
                this.SetQueryDiscrepancyDetail();
                txtDiscrepancyNumber.Text = Convert.ToString(GetQueryStringValue("VDRNo"));
                this.GetDeliveryDetails(txtDiscrepancyNumber.Text);
                pnlAction.Visible = false;
                pnlVendorsQuery.Visible = true;
                pnlODAction.Visible = true;
            }
            else
            {
                pnlAction.Visible = true;
                pnlVendorsQuery.Visible = false;
                pnlODAction.Visible = false;
            }

            //Add by Gourvi for opening Discrepancy in Edit Mode
            if (GetQueryStringValue("Mode") != null && GetQueryStringValue("Mode").ToString() == "Readonly")
            {
                pnlAction.Visible = false;
                pnlVendorsQuery.Visible = true;
                pnlODAction.Visible = false;
                btnSave.Visible = false;
                btnBack.Visible = false;
                btnReadonlyBack.Attributes.Add("style", "display:''");
                btnAddComment.Visible = false;
            }
        }
        if((GetQueryStringValue("RaiseQueryVendor")!=null))
        {
            pnlAction.Visible = true;
            pnlVendorsQuery.Visible = false;
            pnlODAction.Visible = false;
        }
      
        txtDiscrepancyNumber.Focus();
    }

    protected void btnAddComment_Click(object sender, EventArgs e)
    {
        txtUserComments.Text = string.Empty;
        lblErrorMsg.Text = string.Empty;
        lblUserT.Text = Convert.ToString(Session["UserName"]);
        lblDateT.Text = DateTime.Now.Date.ToString("dd/MM/yyyy");
        if (GetQueryStringValue("VDRNo") != null)
            lblDiscrepancyNoT.Text = Convert.ToString(GetQueryStringValue("VDRNo"));
        mdlAddComment.Show();
        txtUserComments.Focus();
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        this.GetDeliveryDetails(txtDiscrepancyNumber.Text);
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        this.SaveUpdate();
        //if (GetQueryStringValue("Mode") != null && GetQueryStringValue("Mode").ToString() == "Readonly")
        //{
            BindDisputedDis();
       // }
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        var vdrNo = GetQueryStringValue("VDRNo") != null ? GetQueryStringValue("VDRNo") : string.Empty;
        if (string.IsNullOrEmpty(vdrNo))
        {
            mvDiscrepancyShortageCriteria.ActiveViewIndex = 0;
            lblDiscrepancyNoAndType.Visible = false;
            btnAddComment.Visible = false;
        }
        else
        {
            if ((GetQueryStringValue("RaiseQueryVendor") != null) && (Session["Role"].ToString().ToLower() == "vendor"))
            {
                EncryptcUrl = EncryptQuery("~/ModuleUI/Discrepancy/DIS_SearchResultVendor.aspx?PreviousPage=SEARCHRESULTVENDOR");
                Response.Redirect(EncryptcUrl);
            }
            else
            {
                EncryptcUrl = EncryptQuery("~/ModuleUI/Discrepancy/DIS_SearchResult.aspx?AdminSearchPage=Yes&VDRNO="+vdrNo);
                Response.Redirect(EncryptcUrl);
            }

            Response.Redirect("~/ModuleUI/Discrepancy/DISLog_DisputedDiscrepancy.aspx");
        }
    }

    protected void btnSave_1_Click(object sender, EventArgs e)
    {
    }

    protected void btnBack_2_Click(object sender, EventArgs e)
    {
    }
  
    

    protected void gvDisDispute_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvDisDispute.PageIndex = e.NewPageIndex;
       
        BindDisputedDis();
    }

    protected void gvDisDispute_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType != DataControlRowType.Header && e.Row.RowType != DataControlRowType.Footer)
        {
            var hypLinkToDisDetails = (HyperLink)e.Row.FindControl("hypLinkToDisDetails");
            var hdnDisLogID = (HiddenField)e.Row.FindControl("hdnDisLogID");
            var hdnQueryDisId = (HiddenField)e.Row.FindControl("hdnQueryDisId");
            if (hypLinkToDisDetails != null)
            {
                hypLinkToDisDetails.NavigateUrl = EncryptQuery("DISLog_QueryDiscrepancy.aspx?disLogID=" + hdnDisLogID.Value
                    + "&VDRNo=" + hypLinkToDisDetails.Text + "&QueryDisID=" + hdnQueryDisId.Value + "&Mode=Readonly");
            }
        }
    }

    #endregion

    #region Methods ...

    //public override BusinessEntities.BaseBe[] SubSort(GridViewSortEventArgs e)
    //{
    //    return Utilities.GenericListHelper<DiscrepancyBE>.SortList((List<DiscrepancyBE>)ViewState["lstSites"], e.SortExpression, e.SortDirection).ToArray();
    //}

    private void GetDeliveryDetails(string DiscrepacnyNumber)
    {
        if (!string.IsNullOrEmpty(DiscrepacnyNumber) && !string.IsNullOrWhiteSpace(DiscrepacnyNumber))
        {
            var oNewDiscrepancyBE = new DiscrepancyBE();
            var oNewDiscrepancyBAL = new DiscrepancyBAL();
            oNewDiscrepancyBE.Action = "GetDiscrepancyLog";
            if (Session["UserID"] != null)
            {
                oNewDiscrepancyBE.UserID = Convert.ToInt32(Session["UserID"]);
                if (Session["Role"].ToString().ToLower() == "vendor")
                    oNewDiscrepancyBE.UserIdForConsVendors = Convert.ToInt32(Session["UserID"]);
            }
            oNewDiscrepancyBE.SCDiscrepancyNo = txtDiscrepancyNumber.Text;
            oNewDiscrepancyBE.IsDelete = true;
            var lstDisLog = oNewDiscrepancyBAL.GetDiscrepancyLogDetailsBAL(oNewDiscrepancyBE);
            oNewDiscrepancyBAL = null;
            if (lstDisLog != null && lstDisLog.Count > 0)
            {
                var discrepancyLogID = lstDisLog[0].DiscrepancyLogID != null ? Convert.ToInt32(lstDisLog[0].DiscrepancyLogID) : 0;

                // Logic to check if current discrepancy is pending to take an action for GoodsIn user.
                if (!this.IsAlreadyDisputeExist(discrepancyLogID))
                {
                    ViewState["DiscrepancyLogID"] = discrepancyLogID;
                    ViewState["VendorID"] = lstDisLog[0].VendorID;
                    var discrepancyTypeID = lstDisLog[0].DiscrepancyTypeID != null ? Convert.ToInt32(lstDisLog[0].DiscrepancyTypeID) : 0;
                    lblSiteValue.Text = lstDisLog[0].Site.SiteName;
                    lblPurchaseOrderValue.Text = lstDisLog[0].PurchaseOrderNumber;
                    lblPurchaseOrderDateValue.Text = "- " + (Convert.ToDateTime(lstDisLog[0].PurchaseOrderDate)).ToString("dd/MM/yyyy");
                    lblIssueRaisedDateValue.Text = (Convert.ToDateTime(lstDisLog[0].DiscrepancyLogDate)).ToString("dd/MM/yyyy");
                    lblIssueRaisedTimeValue.Text = "- " + (Convert.ToDateTime(lstDisLog[0].DiscrepancyLogDate)).ToString("HH:MM");
                    lblDeliveryNoValue.Text = lstDisLog[0].DeliveryNoteNumber.ToString();
                    lblDeliveryDateValue.Text = "- " + (Convert.ToDateTime(lstDisLog[0].DeliveryArrivedDate)).ToString("dd/MM/yyyy");
                    lblVendorNumberValue.Text = lstDisLog[0].VendorNoName.ToString();
                    lblContactValue.Text = lstDisLog[0].Vendor.VendorContactNumber.ToString();
                    lblStockPlannerNoValue.Text = lstDisLog[0].StockPlannerNO.ToString();
                    lblPlannerContactNoValue.Text = lstDisLog[0].StockPlannerContact.ToString();
                    var intSiteId = lstDisLog[0].Site.SiteID;
                    var strVDRNo = lstDisLog[0].VDRNo.ToString();
                    ucSDRCommunication1.VendorEmailList = lstDisLog[0].CommunicationTo;
                    ucSDRCommunication1.innerControlRdoCommunication = lstDisLog[0].CommunicationType.Value;
                    ucSDRCommunication1.innerControlRdoLetter.Enabled = true;
                    ucSDRCommunication1.innerControlRdoEmail.Enabled = true;
                    ucSDRCommunication1.innerControlEmailList.Enabled = true;
                    ucSDRCommunication1.innerControlAltEmailList.Enabled = true;
                    lblDiscrepancyNoAndType.Text = String.Format("{0} - {1}", UIUtility.GetDiscrepancyDisplayName(discrepancyTypeID), txtDiscrepancyNumber.Text);
                    this.GetContactPurchaseOrderDetails(discrepancyLogID);
                    this.HideShowQueryDiscGridColumns(discrepancyTypeID);
                    this.RefreshPage(1);
                }
                else
                {
                    lblMessageFirst.Text = strThereIsCurrentlyAlreadyAnOpenQueryBeingInvestigated;
                    lblMessageSecond.Text = strPleaseWaitUntilYouHaveReceivedAReplyForThisBefore;
                    mdlShowError.Show();
                }
            }
            else
            {
                lblMessageFirst.Text = strThisDiscrepancyWasNotFoundOrIsNotLinkedToYourAccount;
                lblMessageSecond.Text = strPleaseEnterTheCorrectDiscrepancyNumberOrUseSearchDiscrepancies;
                mdlShowError.Show();
            }
        }
        else
        {
            lblMessageFirst.Text = strThisDiscrepancyWasNotFoundOrIsNotLinkedToYourAccount;
            lblMessageSecond.Text = strPleaseEnterTheCorrectDiscrepancyNumberOrUseSearchDiscrepancies;
            mdlShowError.Show();
        }
    }

    private void GetContactPurchaseOrderDetails(int discrepancyLogID)
    {
        var oDiscrepancyBE = new DiscrepancyBE();
        var oDiscrepancyBAL = new DiscrepancyBAL();
        if (discrepancyLogID > 0)
        {
            oDiscrepancyBE.Action = "GetDiscrepancyItem";
            oDiscrepancyBE.DiscrepancyLogID = discrepancyLogID;
            var lstDetails = oDiscrepancyBAL.GetDiscrepancyItemDetailsBAL(oDiscrepancyBE);
            if (lstDetails.Count > 0 && lstDetails != null)
            {
                gvQueryDiscrepancy.DataSource = lstDetails;
                gvQueryDiscrepancy.DataBind();
                ViewState["lstSitesViewMode"] = lstDetails;
            }
        }
    }

    private void HideShowQueryDiscGridColumns(int discrepancyTypeId = 0)
    {
        // Here getting all column collection to show as per the discrepancy ...
        var lstColumns = this.GetColumnCollection(discrepancyTypeId);

        // Here setting all the columns to visible false ...
        gvQueryDiscrepancy.Columns.Cast<TemplateField>()
            .ToList().ForEach(col => col.Visible = false);

        // Here setting the column visible true as per the discepancy setting ...
        gvQueryDiscrepancy.Columns.Cast<TemplateField>()
            .Where(c => lstColumns.Contains(WebCommon.getGlobalResourceValue(c.HeaderText.Trim().Replace(" ", string.Empty).ToUpper()).ToUpper().Replace(" ", string.Empty)))
            .ToList().ForEach(col => col.Visible = true);
    }

    private List<string> GetColumnCollection(int discrepancyTypeId = 0)
    {
        var lstColumns = new List<string>();

        #region Logic to set the columns based on Discrepancy ...
        switch (discrepancyTypeId)
        {
            case 1: //strType = WebCommon.getGlobalResourceValue("Overs");
                lstColumns = new List<string>{
                    WebCommon.getGlobalResourceValue("Line").Trim().Replace(" ",string.Empty).ToUpperInvariant(),
                    WebCommon.getGlobalResourceValue("OfficeDepotCode").Trim().Replace(" ",string.Empty).ToUpperInvariant(), 
                    WebCommon.getGlobalResourceValue("VikingCode").Trim().Replace(" ",string.Empty).ToUpperInvariant(), 
                    WebCommon.getGlobalResourceValue("VendorItemCode").Trim().Replace(" ",string.Empty).ToUpperInvariant(), 
                    WebCommon.getGlobalResourceValue("DESCRIPTION").Trim().Replace(" ",string.Empty).ToUpperInvariant(), 
                    WebCommon.getGlobalResourceValue("OriginalPOQuantity").Trim().Replace(" ",string.Empty).ToUpperInvariant(), 
                    WebCommon.getGlobalResourceValue("OutstandingPOQuantity").Trim().Replace(" ",string.Empty).ToUpperInvariant(), 
                    WebCommon.getGlobalResourceValue("UOM").Trim().Replace(" ",string.Empty).ToUpperInvariant(), 
                    WebCommon.getGlobalResourceValue("DeliveryNoteQuantity").Trim().Replace(" ",string.Empty).ToUpperInvariant(), 
                    WebCommon.getGlobalResourceValue("DeliveredQuantity").Trim().Replace(" ",string.Empty).ToUpperInvariant(), 
                    WebCommon.getGlobalResourceValue("OversQuantity").Trim().Replace(" ",string.Empty).ToUpperInvariant() 
                };
                break;
            case 2: //strType = WebCommon.getGlobalResourceValue("Shortage");
                lstColumns = new List<string>{
                    WebCommon.getGlobalResourceValue("Line").Trim().Replace(" ",string.Empty).ToUpperInvariant(),
                    WebCommon.getGlobalResourceValue("OfficeDepotCode").Trim().Replace(" ",string.Empty).ToUpperInvariant(), 
                    WebCommon.getGlobalResourceValue("VikingCode").Trim().Replace(" ",string.Empty).ToUpperInvariant(), 
                    WebCommon.getGlobalResourceValue("VendorItemCode").Trim().Replace(" ",string.Empty).ToUpperInvariant(), 
                    WebCommon.getGlobalResourceValue("DESCRIPTION").Trim().Replace(" ",string.Empty).ToUpperInvariant(), 
                    WebCommon.getGlobalResourceValue("OriginalPOQuantity").Trim().Replace(" ",string.Empty).ToUpperInvariant(), 
                    WebCommon.getGlobalResourceValue("OutstandingPOQuantity").Trim().Replace(" ",string.Empty).ToUpperInvariant(), 
                    WebCommon.getGlobalResourceValue("UOM").Trim().Replace(" ",string.Empty).ToUpperInvariant(), 
                    WebCommon.getGlobalResourceValue("DeliveryNoteQuantity").Trim().Replace(" ",string.Empty).ToUpperInvariant(), 
                    WebCommon.getGlobalResourceValue("Shortage").Trim().Replace(" ",string.Empty).ToUpperInvariant()
                };
                break;
            case 3: //strType = WebCommon.getGlobalResourceValue("GoodsReceivedDamaged");
                lstColumns = new List<string>{
                    WebCommon.getGlobalResourceValue("Line").Trim().Replace(" ",string.Empty).ToUpperInvariant(),
                    WebCommon.getGlobalResourceValue("OfficeDepotCode").Trim().Replace(" ",string.Empty).ToUpperInvariant(), 
                    WebCommon.getGlobalResourceValue("VikingCode").Trim().Replace(" ",string.Empty).ToUpperInvariant(), 
                    WebCommon.getGlobalResourceValue("VendorItemCode").Trim().Replace(" ",string.Empty).ToUpperInvariant(), 
                    WebCommon.getGlobalResourceValue("DESCRIPTION").Trim().Replace(" ",string.Empty).ToUpperInvariant(), 
                    WebCommon.getGlobalResourceValue("OutstandingPOQuantity").Trim().Replace(" ",string.Empty).ToUpperInvariant(), 
                    WebCommon.getGlobalResourceValue("UOM").Trim().Replace(" ",string.Empty).ToUpperInvariant(), 
                    WebCommon.getGlobalResourceValue("DamageQuantity").Trim().Replace(" ",string.Empty).ToUpperInvariant(), 
                    WebCommon.getGlobalResourceValue("DamageDescription").Trim().Replace(" ",string.Empty).ToUpperInvariant()
                };
                break;
            case 4: //strType = WebCommon.getGlobalResourceValue("NoPurchaseorder");
                lstColumns = new List<string>{
                    WebCommon.getGlobalResourceValue("Line").Trim().Replace(" ",string.Empty).ToUpperInvariant(),
                    WebCommon.getGlobalResourceValue("OfficeDepotCode").Trim().Replace(" ",string.Empty).ToUpperInvariant(), 
                    WebCommon.getGlobalResourceValue("VikingCode").Trim().Replace(" ",string.Empty).ToUpperInvariant(), 
                    WebCommon.getGlobalResourceValue("VendorItemCode").Trim().Replace(" ",string.Empty).ToUpperInvariant(), 
                    WebCommon.getGlobalResourceValue("DESCRIPTION").Trim().Replace(" ",string.Empty).ToUpperInvariant(), 
                    WebCommon.getGlobalResourceValue("OriginalPOQuantity").Trim().Replace(" ",string.Empty).ToUpperInvariant(), 
                    WebCommon.getGlobalResourceValue("OutstandingPOQuantity").Trim().Replace(" ",string.Empty).ToUpperInvariant(), 
                    WebCommon.getGlobalResourceValue("UOM").Trim().Replace(" ",string.Empty).ToUpperInvariant(), 
                    WebCommon.getGlobalResourceValue("ReceivedQty").Trim().Replace(" ",string.Empty).ToUpperInvariant()                 
                };
                break;
            case 5: //strType = WebCommon.getGlobalResourceValue("NoPaperwork");
                lstColumns = new List<string>{
                    WebCommon.getGlobalResourceValue("Line").Trim().Replace(" ",string.Empty).ToUpperInvariant(),
                    WebCommon.getGlobalResourceValue("OfficeDepotCode").Trim().Replace(" ",string.Empty).ToUpperInvariant(), 
                    WebCommon.getGlobalResourceValue("VikingCode").Trim().Replace(" ",string.Empty).ToUpperInvariant(), 
                    WebCommon.getGlobalResourceValue("VendorItemCode").Trim().Replace(" ",string.Empty).ToUpperInvariant(), 
                    WebCommon.getGlobalResourceValue("DESCRIPTION").Trim().Replace(" ",string.Empty).ToUpperInvariant(), 
                    WebCommon.getGlobalResourceValue("OriginalPOQuantity").Trim().Replace(" ",string.Empty).ToUpperInvariant(), 
                    WebCommon.getGlobalResourceValue("OutstandingPOQuantity").Trim().Replace(" ",string.Empty).ToUpperInvariant(), 
                    WebCommon.getGlobalResourceValue("UOM").Trim().Replace(" ",string.Empty).ToUpperInvariant(), 
                    WebCommon.getGlobalResourceValue("ReceivedQty").Trim().Replace(" ",string.Empty).ToUpperInvariant()                   
                };
                break;
            case 6: //strType = WebCommon.getGlobalResourceValue("IncorrectProduct");
                lstColumns = new List<string>{
                    WebCommon.getGlobalResourceValue("Line").Trim().Replace(" ",string.Empty).ToUpperInvariant(),
                    WebCommon.getGlobalResourceValue("OfficeDepotCode").Trim().Replace(" ",string.Empty).ToUpperInvariant(), 
                    WebCommon.getGlobalResourceValue("VikingCode").Trim().Replace(" ",string.Empty).ToUpperInvariant(), 
                    WebCommon.getGlobalResourceValue("DESCRIPTION").Trim().Replace(" ",string.Empty).ToUpperInvariant(), 
                    WebCommon.getGlobalResourceValue("UOM").Trim().Replace(" ",string.Empty).ToUpperInvariant(), 

                    WebCommon.getGlobalResourceValue("ReceivedQty").Trim().Replace(" ",string.Empty).ToUpperInvariant(),                     
                    WebCommon.getGlobalResourceValue("ProductReceivedCode").Trim().Replace(" ",string.Empty).ToUpperInvariant(),                     
                    WebCommon.getGlobalResourceValue("CorrectProductDescription").Trim().Replace(" ",string.Empty).ToUpperInvariant(),                     
                    WebCommon.getGlobalResourceValue("CorrectUOM").Trim().Replace(" ",string.Empty).ToUpperInvariant()                  
                };
                break;
            case 7: //strType = WebCommon.getGlobalResourceValue("PresentationIssue");
                break;
            case 8: //strType = WebCommon.getGlobalResourceValue("IncorrectAddress");
                break;
            case 9: //strType = WebCommon.getGlobalResourceValue("PaperworkAmended");
                lstColumns = new List<string>{
                    WebCommon.getGlobalResourceValue("Line").Trim().Replace(" ",string.Empty).ToUpperInvariant(),
                    WebCommon.getGlobalResourceValue("OfficeDepotCode").Trim().Replace(" ",string.Empty).ToUpperInvariant(), 
                    WebCommon.getGlobalResourceValue("VikingCode").Trim().Replace(" ",string.Empty).ToUpperInvariant(), 
                    WebCommon.getGlobalResourceValue("VendorItemCode").Trim().Replace(" ",string.Empty).ToUpperInvariant(), 
                    WebCommon.getGlobalResourceValue("DESCRIPTION").Trim().Replace(" ",string.Empty).ToUpperInvariant(), 
                    WebCommon.getGlobalResourceValue("UOM").Trim().Replace(" ",string.Empty).ToUpperInvariant(), 
                    WebCommon.getGlobalResourceValue("OutstandingPOQuantity").Trim().Replace(" ",string.Empty).ToUpperInvariant(),                     
                    WebCommon.getGlobalResourceValue("DeliveryNoteQuantity").Trim().Replace(" ",string.Empty).ToUpperInvariant(),                     
                    WebCommon.getGlobalResourceValue("QuantityAmendedTo").Trim().Replace(" ",string.Empty).ToUpperInvariant()                 
                };
                break;
            case 10: //strType = WebCommon.getGlobalResourceValue("WrongPackSize");
                lstColumns = new List<string>{
                    WebCommon.getGlobalResourceValue("Line").Trim().Replace(" ",string.Empty).ToUpperInvariant(),
                    WebCommon.getGlobalResourceValue("OfficeDepotCode").Trim().Replace(" ",string.Empty).ToUpperInvariant(), 
                    WebCommon.getGlobalResourceValue("VikingCode").Trim().Replace(" ",string.Empty).ToUpperInvariant(), 
                    WebCommon.getGlobalResourceValue("VendorItemCode").Trim().Replace(" ",string.Empty).ToUpperInvariant(), 
                    WebCommon.getGlobalResourceValue("DESCRIPTION").Trim().Replace(" ",string.Empty).ToUpperInvariant(), 
                    WebCommon.getGlobalResourceValue("UOM").Trim().Replace(" ",string.Empty).ToUpperInvariant(), 
                    WebCommon.getGlobalResourceValue("OutstandingPOQuantity").Trim().Replace(" ",string.Empty).ToUpperInvariant(),                     
                    WebCommon.getGlobalResourceValue("PackSizeOrdered").Trim().Replace(" ",string.Empty).ToUpperInvariant(),                     
                    WebCommon.getGlobalResourceValue("PackSizeReceived").Trim().Replace(" ",string.Empty).ToUpperInvariant(),                     
                    "NoofPacksReceived".ToUpperInvariant()                    
                };
                break;
            case 11: //strType = WebCommon.getGlobalResourceValue("FailPalletSpecification");
                break;
            case 12: //strType = WebCommon.getGlobalResourceValue("QualityIssues");  
                lstColumns = new List<string>{
                    WebCommon.getGlobalResourceValue("Line").Trim().Replace(" ",string.Empty).ToUpperInvariant(),
                    WebCommon.getGlobalResourceValue("OfficeDepotCode").Trim().Replace(" ",string.Empty).ToUpperInvariant(),
                    WebCommon.getGlobalResourceValue("VikingCode").Trim().Replace(" ",string.Empty).ToUpperInvariant(),
                    WebCommon.getGlobalResourceValue("VendorItemCode").Trim().Replace(" ",string.Empty).ToUpperInvariant(),
                    WebCommon.getGlobalResourceValue("DESCRIPTION").Trim().Replace(" ",string.Empty).ToUpperInvariant(),
                    WebCommon.getGlobalResourceValue("OriginalPOQuantity").Trim().Replace(" ",string.Empty).ToUpperInvariant(),
                    WebCommon.getGlobalResourceValue("OutstandingPOQuantity").Trim().Replace(" ",string.Empty).ToUpperInvariant(),
                    WebCommon.getGlobalResourceValue("UOM").Trim().Replace(" ",string.Empty).ToUpperInvariant(),
                    WebCommon.getGlobalResourceValue("Quantity").Trim().Replace(" ",string.Empty).ToUpperInvariant()
                 };
                break;
            case 13: //strType = WebCommon.getGlobalResourceValue("PrematureInvoiceReceipt");
                break;
            case 14: //strType = WebCommon.getGlobalResourceValue("GenericDiscrepancy");
                break;
            case 15: //strType = WebCommon.getGlobalResourceValue("ShuttleDiscrepancy");
                lstColumns = new List<string>{
                    WebCommon.getGlobalResourceValue("Line").Trim().Replace(" ",string.Empty).ToUpperInvariant(),
                    WebCommon.getGlobalResourceValue("OfficeDepotCode").Trim().Replace(" ",string.Empty).ToUpperInvariant(), 
                    WebCommon.getGlobalResourceValue("VikingCode").Trim().Replace(" ",string.Empty).ToUpperInvariant(), 
                    WebCommon.getGlobalResourceValue("DESCRIPTION").Trim().Replace(" ",string.Empty).ToUpperInvariant(), 
                    WebCommon.getGlobalResourceValue("OriginalPOQuantity").Trim().Replace(" ",string.Empty).ToUpperInvariant(),                     
                    WebCommon.getGlobalResourceValue("OutstandingPOQuantity").Trim().Replace(" ",string.Empty).ToUpperInvariant(),                     
                    WebCommon.getGlobalResourceValue("UOM").Trim().Replace(" ",string.Empty).ToUpperInvariant(), 
                    WebCommon.getGlobalResourceValue("Type").Trim().Replace(" ",string.Empty).ToUpperInvariant(), 
                    WebCommon.getGlobalResourceValue("DiscrepancyQty").Trim().Replace(" ",string.Empty).ToUpperInvariant()                    
                };
                break;
            case 16: //strType = WebCommon.getGlobalResourceValue("ReservationIssue");
                lstColumns = new List<string>{
                    WebCommon.getGlobalResourceValue("Line").Trim().Replace(" ",string.Empty).ToUpperInvariant(),
                    WebCommon.getGlobalResourceValue("OfficeDepotCode").Trim().Replace(" ",string.Empty).ToUpperInvariant(), 
                    WebCommon.getGlobalResourceValue("VikingCode").Trim().Replace(" ",string.Empty).ToUpperInvariant(), 
                    WebCommon.getGlobalResourceValue("DESCRIPTION").Trim().Replace(" ",string.Empty).ToUpperInvariant(), 
                    WebCommon.getGlobalResourceValue("OriginalPOQuantity").Trim().Replace(" ",string.Empty).ToUpperInvariant(), 
                    WebCommon.getGlobalResourceValue("OutstandingPOQuantity").Trim().Replace(" ",string.Empty).ToUpperInvariant(), 
                    WebCommon.getGlobalResourceValue("ReceivedQty").Trim().Replace(" ",string.Empty).ToUpperInvariant(),    
                    WebCommon.getGlobalResourceValue("UOM").Trim().Replace(" ",string.Empty).ToUpperInvariant(), 
                    WebCommon.getGlobalResourceValue("DeliveredQuantity").Trim().Replace(" ",string.Empty).ToUpperInvariant(),  
                    WebCommon.getGlobalResourceValue("Issue").Trim().Replace(" ",string.Empty).ToUpperInvariant()                   
                };
                break;
            case 18: //strType = WebCommon.getGlobalResourceValue("POD");
                lstColumns = new List<string>{
                    WebCommon.getGlobalResourceValue("Line").Trim().Replace(" ",string.Empty).ToUpperInvariant(),
                    WebCommon.getGlobalResourceValue("OfficeDepotCode").Trim().Replace(" ",string.Empty).ToUpperInvariant(), 
                    WebCommon.getGlobalResourceValue("VikingCode").Trim().Replace(" ",string.Empty).ToUpperInvariant(), 
                    WebCommon.getGlobalResourceValue("VendorItemCode").Trim().Replace(" ",string.Empty).ToUpperInvariant(),
                    WebCommon.getGlobalResourceValue("DESCRIPTION").Trim().Replace(" ",string.Empty).ToUpperInvariant(), 
                     WebCommon.getGlobalResourceValue("UOM").Trim().Replace(" ",string.Empty).ToUpperInvariant(), 
                    WebCommon.getGlobalResourceValue("OriginalPOQuantity").Trim().Replace(" ",string.Empty).ToUpperInvariant(), 
                    WebCommon.getGlobalResourceValue("OutstandingPOQuantity").Trim().Replace(" ",string.Empty).ToUpperInvariant(),
                    WebCommon.getGlobalResourceValue("ChaseQty").Trim().Replace(" ",string.Empty).ToUpperInvariant()              
                };
                break;
            case 19: //strType = WebCommon.getGlobalResourceValue("INVOICE QUERY");
                lstColumns = new List<string>{
                    WebCommon.getGlobalResourceValue("Line").Trim().Replace(" ",string.Empty).ToUpperInvariant(),
                    WebCommon.getGlobalResourceValue("OfficeDepotCode").Trim().Replace(" ",string.Empty).ToUpperInvariant(), 
                    WebCommon.getGlobalResourceValue("VikingCode").Trim().Replace(" ",string.Empty).ToUpperInvariant(), 
                    WebCommon.getGlobalResourceValue("VendorItemCode").Trim().Replace(" ",string.Empty).ToUpperInvariant(),
                    WebCommon.getGlobalResourceValue("DESCRIPTION").Trim().Replace(" ",string.Empty).ToUpperInvariant(), 
                    WebCommon.getGlobalResourceValue("UOM").Trim().Replace(" ",string.Empty).ToUpperInvariant(), 
                    WebCommon.getGlobalResourceValue("OriginalPOQuantity").Trim().Replace(" ",string.Empty).ToUpperInvariant(), 
                    WebCommon.getGlobalResourceValue("QtyReceiptedByOD").Trim().Replace(" ",string.Empty).ToUpperInvariant(),
                    WebCommon.getGlobalResourceValue("InvoiceQty").Trim().Replace(" ",string.Empty).ToUpperInvariant()              
                };
                break;
            case 20: //strType = WebCommon.getGlobalResourceValue("ITEM NOT ON PO");
                lstColumns = new List<string>{
                    WebCommon.getGlobalResourceValue("Line").Trim().Replace(" ",string.Empty).ToUpperInvariant(),
                    WebCommon.getGlobalResourceValue("OfficeDepotCode").Trim().Replace(" ",string.Empty).ToUpperInvariant(),
                    WebCommon.getGlobalResourceValue("VikingCode").Trim().Replace(" ",string.Empty).ToUpperInvariant(),
                    WebCommon.getGlobalResourceValue("Suppliercode").Trim().Replace(" ",string.Empty).ToUpperInvariant(),
                    WebCommon.getGlobalResourceValue("DESCRIPTION").Trim().Replace(" ",string.Empty).ToUpperInvariant(),
                    WebCommon.getGlobalResourceValue("OriginalPOQuantity").Trim().Replace(" ",string.Empty).ToUpperInvariant(),
                     WebCommon.getGlobalResourceValue("OutstandingPOQuantity").Trim().Replace(" ",string.Empty).ToUpperInvariant(),
                    WebCommon.getGlobalResourceValue("ReceivedQty").Trim().Replace(" ",string.Empty).ToUpperInvariant(),
                    WebCommon.getGlobalResourceValue("UOM").Trim().Replace(" ",string.Empty).ToUpperInvariant()
                };
                break;
        }
        #endregion

        return lstColumns;
    }

    private void SaveUpdate()
    {
        var queryDiscrepancyBAL = new DISLog_QueryDiscrepancyBAL();
        var queryDiscrepancyBE = new DISLog_QueryDiscrepancyBE();
        /* ADD Dispute Section by Vendor */
        if (ViewState["DiscrepancyLogID"] != null && GetQueryStringValue("disLogID") == null)
        {
            /* Vendor internal comment validaion ... */
            if (string.IsNullOrEmpty(txtInternalComments.Text) || string.IsNullOrWhiteSpace(txtInternalComments.Text))
            {
                txtInternalComments.Focus();
                var saveMessage = WebCommon.getGlobalResourceValue("PleaseEnterYourQuery");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('" + saveMessage + "')", true);
                return;
            }
            else
            {
                queryDiscrepancyBE.Action = "AddQueryDiscrepancy";
                queryDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(ViewState["DiscrepancyLogID"]);
                queryDiscrepancyBE.VendorComment = txtInternalComments.Text;
                queryDiscrepancyBE.VendorUserId = Convert.ToInt32(Session["UserID"]);
                var addQueryStatus = queryDiscrepancyBAL.addQueryDiscrepancyBAL(queryDiscrepancyBE);
                InsertHTML(Convert.ToInt32(ViewState["DiscrepancyLogID"]), Convert.ToInt32(ViewState["DiscrepancyLogID"]));

                var vendorQueryConfir = WebCommon.getGlobalResourceValue("VendorQueryRaisedConfirmation").Replace("##VDRNO##", txtDiscrepancyNumber.Text);
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('" + vendorQueryConfir + "')", true);

                this.RefreshPage(0);
                txtDiscrepancyNumber.Text = string.Empty;
            }
        }

                 /* VENDOR QUERY UPDATE BY RAISE QUERY BUTTON ... */ 
            if (GetQueryStringValue("disLogID") != null && GetQueryStringValue("RaiseQueryVendor")!=null)
            {
                if (string.IsNullOrEmpty(txtInternalComments.Text) || string.IsNullOrWhiteSpace(txtInternalComments.Text))
                {
                    txtInternalComments.Focus();
                    var saveMessage = WebCommon.getGlobalResourceValue("PleaseEnterYourQuery");
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('" + saveMessage + "')", true);
                    return;
                }
                else
                {
                    queryDiscrepancyBE.Action = "AddQueryDiscrepancy";
                    queryDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(ViewState["DiscrepancyLogID"]);
                    queryDiscrepancyBE.VendorComment = txtInternalComments.Text;
                    queryDiscrepancyBE.VendorUserId = Convert.ToInt32(Session["UserID"]);
                    var addQueryStatus = queryDiscrepancyBAL.addQueryDiscrepancyBAL(queryDiscrepancyBE);
                    InsertHTML(Convert.ToInt32(ViewState["DiscrepancyLogID"]), Convert.ToInt32(ViewState["DiscrepancyLogID"]));
                    var vendorQueryConfir = WebCommon.getGlobalResourceValue("VendorQueryRaisedConfirmation").Replace("##VDRNO##", txtDiscrepancyNumber.Text);
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('" + vendorQueryConfir + "')", true);
                  //  this.RefreshPage(0);
                   // txtDiscrepancyNumber.Text = string.Empty;
                    string EncryptcUrl = string.Empty;

                    if (Session["Role"].ToString().ToLower() != "vendor")
                    {
                        EncryptQueryString("~/ModuleUI/Discrepancy/DIS_SearchResult.aspx?AdminSearchPage=Yes&DicrepancyNo=" + vendorQueryConfir + "&VDRNO=" + txtDiscrepancyNumber.Text);
                      
                    }
                    else
                    {
                        EncryptQueryString("~/ModuleUI/Discrepancy/DIS_SearchResultVendor.aspx?PreviousPage=SEARCHRESULTVENDOR&Message=yes" + "&DN=" + vendorQueryConfir+"&VDRNO="+txtDiscrepancyNumber.Text);
                    }
                }

            }

        /* OD Reply Section by Goods-In user */
        else if (GetQueryStringValue("disLogID") != null)
        {
            var disLogID = GetQueryStringValue("disLogID") != null ? Convert.ToInt32(GetQueryStringValue("disLogID")) : 0;
            var queryDisId = GetQueryStringValue("QueryDisID") != null ? Convert.ToInt32(GetQueryStringValue("QueryDisID")) : 0;
            if (disLogID > 0 && queryDisId > 0)
            {
                #region Radio button action validation ...
                var rblActionsCheck = false;
                for (var index = 0; index < rblActions.Items.Count; index++)
                {
                    if (rblActions.Items[index].Selected.Equals(true))
                    {
                        rblActionsCheck = true;
                        break;
                    }
                }
                if (!rblActionsCheck)
                {
                    rblActions.Focus();
                    string saveMessage = WebCommon.getGlobalResourceValue("PleaseSelectAnyActionFromAcceptRejectOrPendingInvQuery");
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('" + saveMessage + "')", true);
                    return;
                }
                #endregion

           


                /* OD comment validaion ... */
                if ((string.IsNullOrEmpty(txtODComment.Text) || string.IsNullOrWhiteSpace(txtODComment.Text))
                    && (!rblActions.SelectedValue.Equals("P")))
                {
                    txtODComment.Focus();
                    string saveMessage = WebCommon.getGlobalResourceValue("PleaseEnterYourResponseToTheVendorHere");
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('" + saveMessage + "')", true);
                    return;
                }
                else
                {
                    queryDiscrepancyBE.Action = "UpdateQueryDiscrepancy";
                    queryDiscrepancyBE.DiscrepancyLogID = disLogID;
                    queryDiscrepancyBE.QueryDiscrepancyID = queryDisId;
                    queryDiscrepancyBE.GoodsInComment = txtODComment.Text;
                    queryDiscrepancyBE.GoodsInAction = rblActions.SelectedValue;
                    queryDiscrepancyBE.GoodsInUserId = Convert.ToInt32(Session["UserID"]);
                    var addQueryStatus = queryDiscrepancyBAL.addQueryDiscrepancyBAL(queryDiscrepancyBE);
                    if (rblActions.SelectedValue.Equals("A") || rblActions.SelectedValue.Equals("R"))
                    {
                        this.SaveAndSendDiscCommunication(disLogID, queryDisId);
                        this.InsertHTML(disLogID, queryDisId);
                    }
                    txtODComment.Text = string.Empty;
                    txtVendorInternalComments.Text = string.Empty;
                    Response.Redirect("~/ModuleUI/Discrepancy/DISLog_DisputedDiscrepancy.aspx");
                }
            }
        }
    }

    private void RefreshPage(int activeViewIndex = 0)
    {
        txtInternalComments.Text = string.Empty;
        if (activeViewIndex.Equals(0))
        {
            mvDiscrepancyShortageCriteria.ActiveViewIndex = 0;
            lblDiscrepancyNoAndType.Visible = false;
            btnAddComment.Visible = false;
            btnViewWF.Visible = false;
        }
        else
        {
            mvDiscrepancyShortageCriteria.ActiveViewIndex = activeViewIndex;
            lblDiscrepancyNoAndType.Visible = true;
            btnAddComment.Visible = true;
            if(Session["Role"].ToString() != "Vendor")
                  btnViewWF.Visible = true;
        }
    }

    private bool IsAlreadyDisputeExist(int discrepancyLogID = 0)
    {
        var isExist = false;
        var vdrNo = GetQueryStringValue("VDRNo") != null ? GetQueryStringValue("VDRNo") : string.Empty;

        if (string.IsNullOrEmpty(vdrNo))
        {
            var queryDiscrepancyBAL = new DISLog_QueryDiscrepancyBAL();
            var queryDiscrepancyBE = new DISLog_QueryDiscrepancyBE();
            if (discrepancyLogID > 0)
            {
                queryDiscrepancyBE.Action = "IsAlreadyDisputeExist";
                queryDiscrepancyBE.DiscrepancyLogID = discrepancyLogID;
                var lstDetails = queryDiscrepancyBAL.IsAlreadyDisputeExistBAL(queryDiscrepancyBE);
                if (lstDetails > 0)
                    isExist = true;
            }
        }
        return isExist;
    }

    private void BindActionsRadiobutton()
    {
        var itemAcceptQuery = new ListItem(WebCommon.getGlobalResourceValue("AcceptQuery"), "A");
        rblActions.Items.Add(itemAcceptQuery);
        var itemRejectQuery = new ListItem(WebCommon.getGlobalResourceValue("RejectQuery"), "R");
        rblActions.Items.Add(itemRejectQuery);
        var itemPendingInvestigation = new ListItem(WebCommon.getGlobalResourceValue("PendingInvestigation"), "P");
        rblActions.Items.Add(itemPendingInvestigation);
    }

    private void SetQueryDiscrepancyDetail()
    {
        var queryDiscrepancyBAL = new DISLog_QueryDiscrepancyBAL();
        var queryDiscrepancyBE = new DISLog_QueryDiscrepancyBE();
        if (GetQueryStringValue("disLogID") != null && GetQueryStringValue("QueryDisID") != null)
        {
            var disLogID = GetQueryStringValue("disLogID") != null ? Convert.ToInt32(GetQueryStringValue("disLogID")) : 0;
            var queryDisId = GetQueryStringValue("QueryDisID") != null ? Convert.ToInt32(GetQueryStringValue("QueryDisID")) : 0;
            if (disLogID > 0 && queryDisId > 0)
            {
                queryDiscrepancyBE.Action = "GetQueryDiscrepancy";
                queryDiscrepancyBE.QueryDiscrepancyID = queryDisId;
                queryDiscrepancyBE.DiscrepancyLogID = disLogID;
                var getQueryDiscrepancy = queryDiscrepancyBAL.GetQueryDiscrepancyBAL(queryDiscrepancyBE);
                if (getQueryDiscrepancy != null && getQueryDiscrepancy.Count > 0)
                {
                    txtVendorInternalComments.Text = getQueryDiscrepancy[0].VendorComment;
                    var vendorRaisedDate = Utilities.Common.GetDD_MM_YYYY(Convert.ToString(getQueryDiscrepancy[0].VendorQueryDate));
                    /* Here 11 is the count of the time from end. */
                    vendorRaisedDate = vendorRaisedDate.Substring(0, vendorRaisedDate.Length - 11);
                    ViewState["VendorQueryDiscRaisedDate"] = vendorRaisedDate;

                    //added by Gourvi for Phase 8 Point 4,4a
                    if (GetQueryStringValue("Mode") != null && GetQueryStringValue("Mode").ToString() == "Readonly")
                    {
                        txtODComment.Text = getQueryDiscrepancy[0].GoodsInComment;
                        rblActions.SelectedValue = getQueryDiscrepancy[0].GoodsInAction;
                        txtODComment.Enabled = false;
                        rblActions.Enabled = false;
                    }
                }
            }
        }
    }

    private void GetVendorEmailIds()
    {
        ucTextbox ucVendorEmailList = (ucTextbox)ucSDRCommunication1.FindControl("ucVendorEmailList");
        if (ucVendorEmailList != null)
            ViewState["ucVendorEmailList"] = ucVendorEmailList.Text;
    }

    private void InsertHTML(int discrepancyLogID, int queryDisId)
    {
        var discrepancyBE = new DiscrepancyBE();
        var discrepancyBAL = new DiscrepancyBAL();
        var workflowHTML = new WorkflowHTML();
        if (ViewState["DiscrepancyLogID"] != null && GetQueryStringValue("disLogID") == null)
        {
            discrepancyBE.GINHTML = workflowHTML.function3("White", null, string.Empty, string.Empty);
            discrepancyBE.GINActionRequired = false;
            discrepancyBE.VENHTML = workflowHTML.function1(DateTime.Now, null, null, "Red",
                string.Empty, WebCommon.getGlobalResourceValue("QueryCreated"), Convert.ToString(Session["UserName"]));
            discrepancyBE.VenActionRequired = false;
        }
        // Default section will call in case of the GoodsIn type user ...
        else if (GetQueryStringValue("disLogID") != null)
        {
            var VendorEmailID = ViewState["ucVendorEmailList"] == null ? string.Empty : Convert.ToString(ViewState["ucVendorEmailList"]);
            discrepancyBE.GINHTML = workflowHTML.function1(DateTime.Now, null, null, "Red",
                string.Empty, WebCommon.getGlobalResourceValue("DisputedQuery"), Convert.ToString(Session["UserName"]));
            discrepancyBE.GINActionRequired = false;
            discrepancyBE.VENHTML = workflowHTML.function2(DateTime.Now, null, "Blue", VendorEmailID, string.Empty, string.Empty,
                string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, txtODComment.Text, string.Empty);
            discrepancyBE.VenActionRequired = false;
        }

        discrepancyBE.INVHTML = workflowHTML.function3("White", null, string.Empty, string.Empty);
        discrepancyBE.INVActionRequired = false;
        discrepancyBE.APHTML = workflowHTML.function3("White", null, string.Empty, string.Empty);
        discrepancyBE.APActionRequired = false;
        discrepancyBE.Action = "QueryDiscrepancyInsertHTML";
        discrepancyBE.DiscrepancyLogID = discrepancyLogID;
        discrepancyBE.LoggedDateTime = DateTime.Now;
        discrepancyBE.LevelNumber = 1;
        discrepancyBE.QueryDiscrepancy = new DISLog_QueryDiscrepancyBE();
        discrepancyBE.QueryDiscrepancy.QueryDiscrepancyID = queryDisId;
        int? iResult = discrepancyBAL.addWorkFlowHTMLsBAL(discrepancyBE);
        discrepancyBAL = null;
        discrepancyBE = null;
    }

    private int? SaveAndSendDiscCommunication(int discrepancyLogID, int queryDisId)
    {
        int? vendorID = ViewState["VendorID"] == null ? 0 : Convert.ToInt32(ViewState["VendorID"].ToString());
        if (((RadioButton)ucSDRCommunication1.FindControl("rdoEmailComm")) != null && ((RadioButton)ucSDRCommunication1.FindControl("rdoEmailComm")).Checked)
        {
            if (((BaseControlLibrary.ucTextbox)ucSDRCommunication1.FindControl("txtAltEmail")) != null && ((BaseControlLibrary.ucTextbox)ucSDRCommunication1.FindControl("ucVendorEmailList")) != null)
            {
                var sendComm = new sendCommunication();
                var discType = string.Empty;
                if (rblActions.SelectedValue.Equals("A"))
                    discType = "QueryDiscAccept";
                if (rblActions.SelectedValue.Equals("R"))
                    discType = "QueryDiscReject";

                int? retVal = sendComm.sendCommunicationByEMail(discrepancyLogID, discType,
                    ((BaseControlLibrary.ucTextbox)ucSDRCommunication1.FindControl("txtAltEmail")),
                    ((BaseControlLibrary.ucTextbox)ucSDRCommunication1.FindControl("ucVendorEmailList")), vendorID,
                    Convert.ToString(ViewState["VendorQueryDiscRaisedDate"]), txtVendorInternalComments.Text, txtODComment.Text, queryDisId);
                sSendMailLink = sendComm.sSendMailLink;
                return retVal;
            }
        }
        return null;
    }

    protected void BindDisputedDis()
    {
        DISLog_QueryDiscrepancyBAL objqueryDiscrepancyBAL = new DISLog_QueryDiscrepancyBAL();
        DISLog_QueryDiscrepancyBE objqueryDiscrepancyBE = new DISLog_QueryDiscrepancyBE();

        objqueryDiscrepancyBE.Action = "GetDiscrepancyDisputes";
        objqueryDiscrepancyBE.Query = ddlQueryStatus.SelectedValue.ToString();
        objqueryDiscrepancyBE.SCSelectedSiteIDs = ddlSite.innerControlddlSite.SelectedValue.ToString();

        if (Session["UserID"] != null)
        {
                if (Session["Role"].ToString().ToLower() == "vendor")
                objqueryDiscrepancyBE.UserIdForConsVendors = Convert.ToInt32(Session["UserID"]);
        }

        List<DISLog_QueryDiscrepancyBE> lstDisputedDis = new List<DISLog_QueryDiscrepancyBE>();

        lstDisputedDis = objqueryDiscrepancyBAL.GetDiscrepancyDisputesBAL(objqueryDiscrepancyBE);

        gvDisDispute.DataSource = lstDisputedDis;
        gvDisDispute.DataBind();
        gvExport.DataSource = lstDisputedDis;
        gvExport.DataBind();
       
    }

    #endregion
    protected void btSearch_Click(object sender, EventArgs e)
    {
        BindDisputedDis();
    }
    protected void btnViewWF_Click(object sender, EventArgs e)
    {
        if (GetQueryStringValue("disLogID") != null)
        {
            //string url = string.Format("DisLog_WorkflowForAPaction.aspx?disLogID={0}&VDRNo={1}&TodayDis={2}&SiteID={3}&DisDate={4}&UserID={5}", GetQueryStringValue("disLogID").ToString(), GetQueryStringValue("VDRNo").ToString(), GetQueryStringValue("TodayDis"), GetQueryStringValue("SiteId"), GetQueryStringValue("DisDate"), GetQueryStringValue("UserID"));
            string url = string.Format("LogDiscrepancy/DISLog_Workflow.aspx?disLogID={0}&VDRNo={1}", GetQueryStringValue("disLogID").ToString(), GetQueryStringValue("VDRNo").ToString());
            EncryptQueryString(url);
        }
    }
}
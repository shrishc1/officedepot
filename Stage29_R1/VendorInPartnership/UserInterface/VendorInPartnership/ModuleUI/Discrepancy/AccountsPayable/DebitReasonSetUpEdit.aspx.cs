﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.Discrepancy.APAction;
using BusinessLogicLayer.ModuleBAL.Discrepancy.CountrySetting;
using WebUtilities;

public partial class AccountsPayable_DebitReasonSetUpEdit : CommonPage
{
    string entereddebitreasontypealreadyexist = WebCommon.getGlobalResourceValue("Entereddebitreasontypealreadyexist");
    string pleaseenterthereason = WebCommon.getGlobalResourceValue("Pleaseenterthereason");

    protected void Page_Load(object sender, EventArgs e)
    {
        UIUtility.PageValidateForODUser();
        if (!IsPostBack)
        {
            GetDebitReasonType();
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(txtDebitReason.Text) || string.IsNullOrWhiteSpace(txtDebitReason.Text))
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('" + pleaseenterthereason + "')", true);
            txtDebitReason.Focus();
            return;
        }
        else
        {
            var debitReasonTypeBAL = new DIS_DebitReasonTypeBAL();
            var apActionBE = new APActionBE();

            if (GetQueryStringValue("DebitReasonID") != null)
            {
                apActionBE.Action = "CheckExistanceUpdate";
                apActionBE.Reason = txtDebitReason.Text.Trim();
                apActionBE.ReasonType = "DR"; /* Debit Reason */
                apActionBE.ReasonTypeID = Convert.ToInt32(GetQueryStringValue("DebitReasonID"));
                if (debitReasonTypeBAL.CheckExistanceBAL(apActionBE) > 0)
                {
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('" + entereddebitreasontypealreadyexist + "')", true);
                    txtDebitReason.Focus();
                    return;
                }
                else
                {
                    apActionBE.Action = "Update";
                }
            }
            else
            {
                apActionBE.Action = "CheckExistance";
                apActionBE.Reason = txtDebitReason.Text.Trim();
                apActionBE.ReasonType = "DR"; /* Debit Reason */
                if (debitReasonTypeBAL.CheckExistanceBAL(apActionBE) > 0)
                {
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('" + entereddebitreasontypealreadyexist + "')", true);
                    txtDebitReason.Focus();
                    return;
                }
                else
                {
                    apActionBE.Action = "Add";
                }
            }

            apActionBE.Reason = txtDebitReason.Text.Trim();
            apActionBE.ReasonType = "DR"; /* Debit Reason */
            apActionBE.UserID = Convert.ToInt32(Session["UserID"]);
            apActionBE.IsBOPenaltyRelated = chkIsBOPenaltyRelated.Checked;
            debitReasonTypeBAL.addEditReasonTypeBAL(apActionBE);
            EncryptQueryString("DebitReasonSetUpOverview.aspx");
        }
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        EncryptQueryString("DebitReasonSetUpOverview.aspx");
    }

    private void GetDebitReasonType()
    {
        if (GetQueryStringValue("DebitReasonID") != null)
        {
            int reasonTypeID = Convert.ToInt32(GetQueryStringValue("DebitReasonID"));
            var debitReasonTypeBAL = new DIS_DebitReasonTypeBAL();
            var apActionBE = new APActionBE();
            apActionBE.Action = "GetReasonTypeSetUp";
            apActionBE.ReasonType = "DR"; /* Debit Reason */
            var resultActionBE = debitReasonTypeBAL.GetReasonTypeBAL(apActionBE).Find(x => x.ReasonTypeID.Equals(reasonTypeID));
            if (resultActionBE != null)
            {
                txtDebitReason.Text = resultActionBE.Reason;
                chkIsBOPenaltyRelated.Checked = resultActionBE.IsBOPenaltyRelated;
            }
        }
    }
}
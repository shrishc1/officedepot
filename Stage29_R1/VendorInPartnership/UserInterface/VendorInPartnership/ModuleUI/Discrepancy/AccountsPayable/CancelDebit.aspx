﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    AutoEventWireup="true" CodeFile="CancelDebit.aspx.cs" Inherits="AccountsPayable_CancelDebit" %>

<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="~/ModuleUI/Discrepancy/UserControl/ucSDRCommunicationAPListing.ascx"
    TagName="ucSDRCommunication" TagPrefix="uc2" %>
<%@ Register Src="~/CommonUI/UserControls/ucVendorSelectionTemplate.ascx" TagName="VendorSelectTemplate"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectVendor.ascx" TagName="MultiSelectVendor"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectDebitType.ascx" TagName="ucMultiSelectDebitType"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectAPSearch.ascx" TagName="ucMultiSelectAP"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/ucExportToExcel.ascx" TagName="ucExportToExcel"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectDebitNoteNumber.ascx" TagName="ucMultiSelectDebitNoteNumber"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectPO.ascx" TagName="ucMultiSelectPO"
    TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script language="javascript" type="text/javascript">
        function getScroll1(ctrDiv) {
            document.getElementById('<%=scroll2.ClientID%>').value = ctrDiv.scrollLeft;
        }

        function SetRadioButton(spanChk) {
            //            var rdoButtonSelectedIndex = 0;
            //            $("input[type='radio'][id$='rblSelect']").each(function (index, element) {
            //                if (spanChk.id == $(this)[0].id) {
            //                    rdoButtonSelectedIndex = index;
            //                }
            //            });

            $("input[type='radio'][id$='rblSelect']").removeAttr("checked");
            spanChk.checked = true;

            //            $("#hdnSelectedIndexVal").val(rdoButtonSelectedIndex);
        }
        $(document).ready(function () {
            $("body").keypress(function (e) {
                if (e.which == 13 || e.keyCode == 13) {
                    $('#<%=btnSearch.ClientID %>').click();
                    return false;
                }
            });
        });
    </script>
    <input type="hidden" id="scroll2" runat="server" />
    <%-- <div class="button-row">
            <cc1:ucExportToExcel ID="ucExportToExcel1" runat="server" />
        </div>--%>
    <asp:ScriptManager ID="sp1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="up1" runat="server">
        <ContentTemplate>
            <h2>
                <cc1:ucLabel ID="lblCancelDebit" runat="server"></cc1:ucLabel>
            </h2>
            <div class="right-shadow">
                <div class="formbox">
                    <cc1:ucPanel ID="pnlSearchDebit1" runat="server">
                        <table width="100%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                            <tr>
                                <td align="right">
                                    <cc1:VendorSelectTemplate runat="server" ID="ucVendorTemplateSelect" />
                                </td>
                            </tr>
                        </table>
                        <table width="100%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                            <tr>
                                <td style="font-weight: bold; width: 15%">
                                    <cc1:ucLabel ID="lblDebitNoteNumber" runat="server"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold;">
                                    :
                                </td>
                                <td>
                                    <cc1:ucMultiSelectDebitNoteNumber runat="server" ID="msDebitNoteNumber" />
                                </td>
                            </tr>
                            <tr>
                                <td style="font-weight: bold; width: 15%">
                                    <cc1:ucLabel ID="lblDebitType" runat="server"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold;">
                                    :
                                </td>
                                <td>
                                    <cc1:ucMultiSelectDebitType runat="server" ID="msDebitType" />
                                </td>
                            </tr>
                            <tr>
                                <td style="font-weight: bold;">
                                    <cc1:ucLabel ID="lblVendor" runat="server"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold;">
                                    :
                                </td>
                                <td>
                                    <cc1:MultiSelectVendor runat="server" ID="msVendor" />
                                </td>
                            </tr>
                            <tr id="trAPClerk" runat="server">
                                <td style="font-weight: bold;">
                                    <cc1:ucLabel ID="lblRaisedBy" runat="server"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold;">
                                    :
                                </td>
                                <td>
                                    <cc1:ucMultiSelectAP ID="msAP" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td style="font-weight: bold;">
                                    <cc1:ucLabel ID="lblPurchaseOrderNumber" Text="Purchase Order Number" runat="server"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold;">
                                    :
                                </td>
                                <td>
                                    <cc1:ucMultiSelectPO ID="msPurchaseOrderNumber" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    <table width="100%">
                                        <tr>
                                            <td style="font-weight: bold; width: 15%">
                                                <cc1:ucLabel ID="lblDateDebitwasRaised" runat="server"></cc1:ucLabel>
                                            </td>
                                            <td style="font-weight: bold; width: 1%">
                                                :
                                            </td>
                                            <td align="left">
                                                <table width="25%" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td style="font-weight: bold; width: 6em;">
                                                            <asp:TextBox ID="txtFromDate" runat="server" ClientIDMode="Static" class="date" />
                                                            <%--<cc1:ucTextbox ID="txtFromDate" runat="server" ClientIDMode="Static" CssClass="date"
                                                        onchange="setValue1(this)" ReadOnly="True" Width="70px" />--%>
                                                        </td>
                                                        <td style="font-weight: bold; width: 4em; text-align: center">
                                                            &nbsp;&nbsp;<cc1:ucLabel ID="lblTo" runat="server" Text="To"></cc1:ucLabel>&nbsp;&nbsp;
                                                        </td>
                                                        <td style="font-weight: bold; width: 6em;">
                                                            <asp:TextBox ID="txtToDate" runat="server" ClientIDMode="Static" class="date" />
                                                            <%--<cc1:ucTextbox ID="txtToDate" runat="server" ClientIDMode="Static" CssClass="date"
                                                        onchange="setValue2(this)" ReadOnly="True" Width="70px" />--%>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <%--  <tr>
                                    <td style="font-weight: bold;">
                                        <cc1:ucLabel ID="lblDebitStatus" runat="server"></cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold;">
                                        :
                                    </td>
                                    <td>
                                        <cc1:ucDropdownList runat="server" ID="ddlDebitStatus" Width="110px">
                                            <asp:ListItem Text="Active" Value="A"></asp:ListItem>
                                            <asp:ListItem Text="Cancelled" Value="C"></asp:ListItem>
                                            <asp:ListItem Text="All" Value="All"></asp:ListItem>
                                        </cc1:ucDropdownList>
                                    </td>
                                </tr>--%>
                            <tr>
                                <td align="right" colspan="3">
                                    <div class="button-row">
                                        <cc1:ucButton ID="btnSearch" runat="server" Text="Search" CssClass="button" OnClick="btnSearch_Click" />
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </cc1:ucPanel>
                    <cc1:ucPanel ID="pnlDebitLibrary1" runat="server">
                        <div class="button-row">
                            <cc1:ucExportToExcel ID="ucExportToExcel1" runat="server" />

                        </div>
                        <cc1:ucMultiView ID="mvCancelDebit" runat="server" ActiveViewIndex="0">
                            <!-- Search Query Discrepancy Section -->
                            <cc1:ucView ID="vwDNN" runat="server">
                                <%--<table width="50%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                                <tr>
                                    <td style="font-weight: bold; width: 60%" align="right">
                                        <cc1:ucLabel ID="lblDebitNoteNumber" runat="server" Text="Debit Note Number"></cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold; width: 20%" align="center">
                                        <cc1:ucTextbox ID="txtDebitNoteNumber" runat="server"></cc1:ucTextbox>
                                    </td>
                                    <td style="width: 20%" align="left">
                                        <cc1:ucButton ID="btnSearch" Text="Search" runat="server" OnClick="btnSearch_Click"
                                            CssClass="button" />
                                    </td>
                                </tr>
                            </table>--%>
                                <%--<div style="width: 100%; height: 440px; overflow: scroll" id="divPrint" class="fixedTable"
                                onscroll="getScroll1(this);">--%>
                                <table cellspacing="1" cellpadding="0" border="0" align="center" class="form-table">
                                    <tr>
                                        <td style="">
                                            <cc1:ucPanel ID="pnlCancelaDebit"  runat="server" CssClass="fieldset-form">
                                                <cc1:ucGridView ID="gvCancelDebit" runat="server" AutoGenerateColumns="false" CssClass="grid gvclass "
                                                    CellPadding="0" GridLines="Both" Width="950px" AllowPaging="true" PageSize="50"
                                                    OnPageIndexChanging="gvCancelDebit_PageIndexChanging" OnRowDataBound="gvCancelDebit_RowDataBound">
                                                    <AlternatingRowStyle BackColor="White" ForeColor="#284775"></AlternatingRowStyle>
                                                    <EmptyDataTemplate>
                                                        <div style="text-align: center">                                                            
                                                            <cc1:ucLabel ID="lblRecordNotFound" isRequired="true" runat="server" Text="No record found"></cc1:ucLabel>
                                                        </div>
                                                    </EmptyDataTemplate>
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="Select">
                                                            <HeaderStyle HorizontalAlign="Left" Width="20px" />
                                                            <ItemStyle Width="20px" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                            <ItemTemplate>
                                                                <cc1:ucLabel runat="server" ID="lblVendorIDValue" Text='<%#Eval("VendorID") %>' Visible="false"></cc1:ucLabel>
                                                                <cc1:ucLabel runat="server" ID="lblDebitRaiseIdValue" Text='<%#Eval("DebitRaiseId") %>'
                                                                    Visible="false"></cc1:ucLabel>
                                                                <cc1:ucLabel runat="server" ID="lblDiscrepancyLogIDValue" Text='<%#Eval("DiscrepancyLogID") %>'
                                                                    Visible="false"></cc1:ucLabel>
                                                                <cc1:ucLabel runat="server" ID="lblDiscrepancyTypeIDValue" Text='<%#Eval("DiscrepancyTypeID") %>'
                                                                    Visible="false"></cc1:ucLabel>
                                                                <cc1:ucLabel runat="server" ID="lblCreatedByIdValue" Text='<%#Eval("CreatedById") %>'
                                                                    Visible="false"></cc1:ucLabel>
                                                                <asp:RadioButton runat="server" CssClass="innerCheckBox" ID="rblSelect" onclick='<%# "javascript:SetRadioButton(this);" %>' />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <%--<asp:TemplateField HeaderText="Status" SortExpression="Status">
                                                        <HeaderStyle Width="30px" HorizontalAlign="Left" />
                                                        <ItemTemplate>
                                                            <cc1:ucLabel runat="server" ID="lblStatusValue" Text='<%#Eval("Status") %>'></cc1:ucLabel>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" Width="30px" />
                                                    </asp:TemplateField>--%>
                                                        <asp:TemplateField HeaderText="Vendor #" SortExpression="Vendor.Vendor_No">
                                                            <HeaderStyle Width="70px" HorizontalAlign="Left" />
                                                            <ItemTemplate>
                                                                <cc1:ucLabel runat="server" ID="ltVendorNoValue" Text='<%#Eval("Vendor.Vendor_No") %>'></cc1:ucLabel>
                                                            </ItemTemplate>
                                                            <ItemStyle HorizontalAlign="Left" Width="70px" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Vendor Name" SortExpression="Vendor.VendorName">
                                                            <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                                            <ItemTemplate>
                                                                <cc1:ucLabel runat="server" ID="ltVendor" Text='<%#Eval("Vendor.VendorName") %>'></cc1:ucLabel>
                                                            </ItemTemplate>
                                                            <ItemStyle HorizontalAlign="Left" Width="150px" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Discrepancy # (if applicable)">
                                                            <HeaderStyle Width="50px" HorizontalAlign="Left" />
                                                            <ItemTemplate>
                                                                <asp:HyperLink ID="hlVDRNoValue" runat="server" Text='<%# Eval("VDRNo") %>' Target="_blank"></asp:HyperLink>
                                                            </ItemTemplate>
                                                            <ItemStyle HorizontalAlign="Left" Width="50px" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Date Debit Raised">
                                                            <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                                            <ItemStyle HorizontalAlign="Left" Width="100px" />
                                                            <ItemTemplate>
                                                                <cc1:ucLabel ID="lblDateDebitRaised" runat="server" Text='<%# Eval("DebitRaiseDate", "{0:dd/MM/yyyy}") %>'></cc1:ucLabel>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:BoundField HeaderText="Raised By" DataField="CreatedBy">
                                                            <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                                            <ItemStyle HorizontalAlign="left" Width="100px" />
                                                        </asp:BoundField>
                                                        <asp:TemplateField HeaderText="Debit Note Number">
                                                            <HeaderStyle Width="50px" HorizontalAlign="Left" />
                                                            <ItemTemplate>
                                                                <asp:HyperLink ID="hlDebitNoteNoValue" runat="server" Text='<%#Eval("DebitNoteNo") %>'
                                                                    NavigateUrl='<%# EncryptQuery("~/ModuleUI/Discrepancy/LogDiscrepancy/DISLog_ReSendCommunication.aspx?CommTitle=DebitRaised&DebitRaiseId=" + Eval("DebitRaiseId") + "&CommunicationType=DebitRaised") %>'
                                                                    Target='_blank' onclick='window.open(this.href, this.target,"scrollbars=1,left=100px,top=20px,width=1000px,height=620px,toolbar=0,scrollbars=0,status=0"); return false;'></asp:HyperLink>
                                                            </ItemTemplate>
                                                            <ItemStyle HorizontalAlign="Left" Width="50px" />
                                                        </asp:TemplateField>
                                                        <asp:BoundField HeaderText="Debit Reason" DataField="Reason">
                                                            <HeaderStyle Width="150px" HorizontalAlign="Left" />
                                                            <ItemStyle HorizontalAlign="Left" Width="150px" />
                                                        </asp:BoundField>
                                                        <asp:BoundField HeaderText="PO" DataField="PurchaseOrderNumber">
                                                            <HeaderStyle Width="120px" HorizontalAlign="Left" />
                                                            <ItemStyle HorizontalAlign="Left" Width="120px" />
                                                        </asp:BoundField>
                                                        <asp:BoundField HeaderText="Value of Debit" DataField="StateValueDebited">
                                                            <HeaderStyle Width="50px" HorizontalAlign="Left" />
                                                            <ItemStyle HorizontalAlign="Left" Width="50px" />
                                                        </asp:BoundField>
                                                        <asp:TemplateField HeaderText="Currency">
                                                            <HeaderStyle Width="50px" HorizontalAlign="Left" />
                                                            <ItemStyle HorizontalAlign="Left" Width="50px" />
                                                            <ItemTemplate>
                                                                <cc1:ucLabel ID="lblCurrencyValue" runat="server" Text='<%# Eval("Currency.CurrencyName") %>'></cc1:ucLabel>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Debit Sent to">
                                                            <HeaderStyle Width="150px" HorizontalAlign="Left" />
                                                            <ItemStyle HorizontalAlign="Left" Width="150px" />
                                                            <ItemTemplate>
                                                                <cc1:ucLabel ID="lblCommunicationEmailsValue"   Style="word-wrap:true " runat="server" Text='<%# Eval("CommunicationEmails") %>'></cc1:ucLabel>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </cc1:ucGridView>
                                                <cc1:ucGridView ID="gvCancelDebitExport" runat="server" AutoGenerateColumns="false"
                                                    CssClass="grid gvclass" Visible="false" CellPadding="0" GridLines="Both" Width="950px">
                                                    <AlternatingRowStyle BackColor="White" ForeColor="#284775"></AlternatingRowStyle>
                                                    <EmptyDataTemplate>
                                                        <div style="text-align: center">
                                                            <cc1:ucLabel ID="lblNoData" runat="server" Text="No Records found."></cc1:ucLabel>
                                                        </div>
                                                    </EmptyDataTemplate>
                                                    <Columns>
                                                        <%--<asp:TemplateField HeaderText="Select">
                                                        <HeaderStyle HorizontalAlign="Left" Width="20px" />
                                                        <ItemStyle Width="20px" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemTemplate>
                                                            <asp:RadioButton runat="server" CssClass="innerCheckBox" ViewStateMode="Enabled" ID="rblSelect" />                                                            
                                                        </ItemTemplate>
                                                    </asp:TemplateField>--%>
                                                        <%--<asp:TemplateField HeaderText="Status" SortExpression="Status">
                                                        <HeaderStyle Width="30px" HorizontalAlign="Left" />
                                                        <ItemTemplate>
                                                            <cc1:ucLabel runat="server" ID="lblStatusValue" Text='<%#Eval("Status") %>'></cc1:ucLabel>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" Width="30px" />
                                                    </asp:TemplateField>--%>
                                                        <asp:TemplateField HeaderText="Vendor #" SortExpression="Vendor.Vendor_No">
                                                            <HeaderStyle Width="70px" HorizontalAlign="Left" />
                                                            <ItemTemplate>
                                                                <cc1:ucLabel runat="server" ID="ltVendorNoValue" Text='<%#Eval("Vendor.Vendor_No") %>'></cc1:ucLabel>
                                                            </ItemTemplate>
                                                            <ItemStyle HorizontalAlign="Left" Width="70px" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Vendor Name" SortExpression="Vendor.VendorName">
                                                            <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                                            <ItemTemplate>
                                                                <cc1:ucLabel runat="server" ID="ltVendor" Text='<%#Eval("Vendor.VendorName") %>'></cc1:ucLabel>
                                                            </ItemTemplate>
                                                            <ItemStyle HorizontalAlign="Left" Width="150px" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Discrepancy # (if applicable)">
                                                            <HeaderStyle Width="50px" HorizontalAlign="Left" />
                                                            <ItemTemplate>
                                                                <asp:HyperLink ID="hlVDRNoValue" runat="server" Text='<%# Eval("VDRNo") %>' Target="_blank"
                                                                    NavigateUrl='<%# EncryptQuery("~/ModuleUI/Discrepancy/LogDiscrepancy/DISLog_Overs.aspx?disLogID=893&VDRNo=A000000455&UserID=1&PN=CancelDebit" ) %>'></asp:HyperLink>
                                                            </ItemTemplate>
                                                            <ItemStyle HorizontalAlign="Left" Width="50px" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Date Debit Raised">
                                                            <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                                            <ItemStyle HorizontalAlign="Left" Width="100px" />
                                                            <ItemTemplate>
                                                                <cc1:ucLabel ID="lblDateDebitRaised" runat="server" Text='<%# Eval("DebitRaiseDate", "{0:dd/MM/yyyy}") %>'></cc1:ucLabel>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:BoundField HeaderText="Raised By" DataField="CreatedBy">
                                                            <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                                            <ItemStyle HorizontalAlign="left" Width="100px" />
                                                        </asp:BoundField>
                                                        <asp:TemplateField HeaderText="Debit Note Number">
                                                            <HeaderStyle Width="50px" HorizontalAlign="Left" />
                                                            <ItemTemplate>
                                                                <asp:HyperLink ID="hlDebitNoteNoValue" runat="server" Text='<%# Eval("DebitNoteNo") %>'
                                                                    href='<%# EncryptQuery("Communication.aspx?CommunicationType=DebitCreate" ) %>'
                                                                    Target='_blank' onclick='window.open(this.href, this.target,"scrollbars=1,left=100px,top=20px,width=1000px,height=620px,toolbar=0,scrollbars=0,status=0"); return false;'></asp:HyperLink>
                                                            </ItemTemplate>
                                                            <ItemStyle HorizontalAlign="Left" Width="50px" />
                                                        </asp:TemplateField>
                                                        <asp:BoundField HeaderText="Debit Reason" DataField="Reason">
                                                            <HeaderStyle Width="150px" HorizontalAlign="Left" />
                                                            <ItemStyle HorizontalAlign="Left" Width="150px" />
                                                        </asp:BoundField>
                                                        <asp:BoundField HeaderText="PO" DataField="PurchaseOrderNumber">
                                                            <HeaderStyle Width="120px" HorizontalAlign="Left" />
                                                            <ItemStyle HorizontalAlign="Left" Width="120px" />
                                                        </asp:BoundField>
                                                        <asp:BoundField HeaderText="Value  of Debit" DataField="StateValueDebited">
                                                            <HeaderStyle Width="50px" HorizontalAlign="Left" />
                                                            <ItemStyle HorizontalAlign="Left" Width="50px" />
                                                        </asp:BoundField>
                                                        <asp:TemplateField HeaderText="Currency">
                                                            <HeaderStyle Width="50px" HorizontalAlign="Left" />
                                                            <ItemStyle HorizontalAlign="Left" Width="50px" />
                                                            <ItemTemplate>
                                                                <cc1:ucLabel ID="lblCurrencyValue" runat="server" Text='<%# Eval("Currency.CurrencyName") %>'></cc1:ucLabel>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Debit Sent to">
                                                            <HeaderStyle Width="150px" HorizontalAlign="Left" />
                                                            <ItemStyle HorizontalAlign="Left" Width="150px" />
                                                            <ItemTemplate>
                                                                <cc1:ucLabel ID="lblCommunicationEmailsValue" runat="server" Text='<%# Eval("CommunicationEmails") %>'></cc1:ucLabel>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </cc1:ucGridView>
                                            </cc1:ucPanel>
                                        </td>
                                    </tr>
                                    
                                </table>
                                <%--  </div>--%>
                            </cc1:ucView>
                        </cc1:ucMultiView>
                    </cc1:ucPanel>
                 
                                            <div class="button-row">
                                                <cc1:ucButton ID="btnCancel" runat="server" Text="Cancel" CssClass="button" OnClick="btnCancel_Click"/>
                                                <cc1:ucButton ID="btnBack_1" runat="server" Text="Back" class="button" OnClick="btnBack_1_Click" />
                                            </div>
                                      
                </div>
            </div>
        </ContentTemplate>
         <Triggers>
            <asp:PostBackTrigger ControlID="ucExportToExcel1" />             
        </Triggers>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="updDebitCancel" runat="server">
        <ContentTemplate>
            <asp:Button ID="btnDebitCancelMPE" runat="Server" Style="display: none" />
            <ajaxToolkit:ModalPopupExtender ID="mdlDebitCancel" runat="server" TargetControlID="btnDebitCancelMPE"
                PopupControlID="pnlDebitCancel" BackgroundCssClass="modalBackground" BehaviorID="ShowDebitCancelPopup"
                DropShadow="false" />
            <asp:Panel ID="pnlDebitCancel" runat="server" Style="display: none; width: 630px">
                <div style="overflow-y: hidden; overflow-x: hidden; background-color: #fff; padding: 5px;
                    border: 2px solid #ccc; text-align: left;">
                    <cc1:ucPanel ID="pnlCancelDebit" runat="server" Width="100%" CssClass="fieldset-form"
                        GroupingText="AP Action">
                        <table cellspacing="5" cellpadding="0" align="center" width="100%" class="top-settingsNoBorder-popup">
                            <tr>
                                <td style="width: 24%">
                                    <cc1:ucLabel ID="lblCancelledBy" runat="server"></cc1:ucLabel>
                                </td>
                                <td style="width: 1%">
                                    :
                                </td>
                                <td style="width: 75%">
                                    <cc1:ucLabel ID="lblCancelledByValue" runat="server" Text="Joe Blohhs"></cc1:ucLabel>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <cc1:ucLabel ID="lblCancelledOn" runat="server"></cc1:ucLabel>
                                </td>
                                <td>
                                    :
                                </td>
                                <td>
                                    <cc1:ucLabel ID="lblCancelledOnValue" runat="server" Text="02/06/2014"></cc1:ucLabel>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <cc1:ucLabel ID="lblReasonForCancelling" runat="server"></cc1:ucLabel>
                                </td>
                                <td>
                                    :
                                </td>
                                <td>
                                    <cc1:ucDropdownList ID="ddlReason" runat="server" Width="200px">
                                    </cc1:ucDropdownList>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    <cc1:ucPanel ID="pnlCommunicationDetail" runat="server" GroupingText="Communication Detail"
                                        CssClass="fieldset-form">
                                        <table width="100%" cellspacing="0" cellpadding="0" class="form-table" height="100px">
                                            <tr>
                                                <td>
                                                    <uc2:ucSDRCommunication ID="ucSDRCommunication1" runat="server" />
                                                </td>
                                            </tr>
                                        </table>
                                    </cc1:ucPanel>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    <cc1:ucLabel ID="lblCommentSentToVendor" runat="server" Text="Please enter comment to be sent to Vendor"></cc1:ucLabel>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    <cc1:ucTextbox ID="txtCommentToVendor" runat="server" onkeyup="checkTextLengthOnKeyUp(this,1000);"
                                        TextMode="MultiLine" CssClass="textarea" Width="98%" Height="50px"></cc1:ucTextbox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td align="right" valign="bottom" colspan="3">
                                    <cc1:ucButton ID="btnSave" runat="server" Text="Save" class="button" OnClick="btnSave_Click" />
                                    <cc1:ucButton ID="btnBack" runat="server" Text="Back" class="button" OnClick="btnBack_Click" />
                                    <asp:HiddenField ID="hdnDiscrepancyLogIDValue" runat="server" />
                                    <asp:HiddenField ID="hdnDebitRaiseIdVal" runat="server" />
                                    <asp:HiddenField ID="hdnVendorIdVal" runat="server" />
                                </td>
                            </tr>
                        </table>
                    </cc1:ucPanel>
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

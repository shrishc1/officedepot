﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.Discrepancy.APAction;
using BusinessLogicLayer.ModuleBAL.Discrepancy.CountrySetting;

public partial class AccountsPayable_CancelReasonSetUpOverview : CommonPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        UIUtility.PageValidateForODUser();
        if (!IsPostBack)
        {
            ucExportToExcel1.Visible = false;
            BindCancelReasonSetUpGrid();
        }
        ucExportToExcel1.CurrentPage = this;
        ucExportToExcel1.GridViewControl = gvCancelReasonSetUp;
        ucExportToExcel1.FileName = "CancelDebitReason";
    }

    private void BindCancelReasonSetUpGrid()
    {
        //APActionBE oAPActionBE1 = new APActionBE();
        //APActionBE oAPActionBE2 = new APActionBE();
        //APActionBE oAPActionBE3 = new APActionBE();
        //List<APActionBE> lstAPActionBE = new List<APActionBE>();


        ////oAPActionBE1.CancelReason = "Create in Error";
        ////oAPActionBE1.CreatedBy = "Super Admin";
        ////oAPActionBE1.DateCreated = Convert.ToDateTime("01/01/2014");
        //lstAPActionBE.Add(oAPActionBE1);

        ////oAPActionBE2.CancelReason = "Cancel Reason 2";
        ////oAPActionBE2.CreatedBy = "Super Admin";
        ////oAPActionBE2.DateCreated = Convert.ToDateTime("02/05/2014");
        //lstAPActionBE.Add(oAPActionBE2);

        ////oAPActionBE3.CancelReason = "Cancel Reason 3";
        ////oAPActionBE3.CreatedBy = "Super Admin";
        ////oAPActionBE3.DateCreated = Convert.ToDateTime("06/07/2014");
        //lstAPActionBE.Add(oAPActionBE3);


        //if (lstAPActionBE != null && lstAPActionBE.Count > 0)
        //{
        //    ucExportToExcel1.Visible = true;
        //    gvCancelReasonSetUp.DataSource = lstAPActionBE;
        //    gvCancelReasonSetUp.DataBind();
        //    ViewState["lstAPActionBE"] = lstAPActionBE;
        //}

        var debitReasonTypeBAL = new DIS_DebitReasonTypeBAL();
        var apActionBE = new APActionBE();
        apActionBE.Action = "GetReasonTypeSetUp";
        apActionBE.ReasonType = "CDR"; /* Debit Reason */
        var lstAPActionBE = debitReasonTypeBAL.GetReasonTypeBAL(apActionBE);

        if (lstAPActionBE != null && lstAPActionBE.Count > 0)
        {
            ucExportToExcel1.Visible = true;
            gvCancelReasonSetUp.DataSource = lstAPActionBE;
            gvCancelReasonSetUp.DataBind();
            ViewState["lstAPActionBE"] = lstAPActionBE;
        }
    }

    public override BusinessEntities.BaseBe[] SubSort(GridViewSortEventArgs e)
    {
        return Utilities.GenericListHelper<APActionBE>.SortList((List<APActionBE>)ViewState["lstAPActionBE"], e.SortExpression, e.SortDirection).ToArray();
    }
}
﻿using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.Discrepancy.APAction;
using BusinessEntities.ModuleBE.Discrepancy.LogDiscrepancy;
using BusinessEntities.ModuleBE.Security;
using BusinessLogicLayer.ModuleBAL.Discrepancy.LogDiscrepancy;
using BusinessLogicLayer.ModuleBAL.Security;
using Utilities;
using WebUtilities;
using BaseControlLibrary;

public partial class AccountsPayable_SearchDebit : CommonPage
{
    #region Declarations ...

    protected string Active = WebCommon.getGlobalResourceValue("Active");
    protected string Cancelled = WebCommon.getGlobalResourceValue("Cancelled");

    #endregion

    #region Events
    protected void Page_InIt(object sender, EventArgs e)
    {
        ucVendorTemplateSelect.CurrentPage = this;
        if (Session["Role"].ToString().ToLower() == "vendor")
        {
            msVendor.FunctionCalled = "BindVendorByVendorId";
        }

    }

    protected void Page_Load(object sender, EventArgs e)
    {        
        ucExportToExcel1.CurrentPage = this;
        ucExportToExcel1.FileName = "DebitLibrary";
        ucExportToExcel1.GridViewControl = gvSearchDebitExcel;

        if (Session["Role"].ToString().ToLower() == "vendor")
        {
            trAPClerk.Visible = false;
        }

        if (!IsPostBack)
        {
            pnlSearchDebit1.Visible = true;
            pnlDebitLibrary1.Visible = false;
            ucExportToExcel1.Visible = false;
            //pager1.PageSize = 200;
            //pager1.GenerateGoToSection = false;
            //pager1.GeneratePagerInfoSection = false;

            //----------phase 18 R2 point 10 -------------------------------//
            if (GetQueryStringValue("PreviousPage") != null && GetQueryStringValue("PreviousPage").ToString() == "POInquiry") {
                btnSearch_Click(null,null);
            }
            //------------------------------------------------------------//
        }

        msDebitNoteNumber.SetDebitNoteNumberOnPostBack();
        msAP.SetAPOnPostBack();
        msDebitType.SetDebitTypeOnPostBack();
        msPurchaseOrderNumber.SetPOOnPostBack();
        msVendor.setVendorsOnPostBack();


    }

    public void pager_Command(object sender, CommandEventArgs e)
    {
        int currentPageIndx = Convert.ToInt32(e.CommandArgument);
        //pager1.CurrentIndex = currentPageIndx;
        //BindCancelaDebits(currentPageIndx);
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        lblDebitLibrary.Visible = true;
        lblSearchDebit.Visible = false;
        BindCancelaDebits();
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        this.ClearCriteria();
        pnlSearchDebit1.Visible = true;
        pnlDebitLibrary1.Visible = false;
        ucExportToExcel1.Visible = false;
        lblDebitLibrary.Visible = false;
        lblSearchDebit.Visible = true;

    }

    protected void gvSearchDebit_PageIndexChanging(object sender, GridViewPageEventArgs e) 
    {
        if (ViewState["AllDebitRaise"] != null)
        {
            gvSearchDebit.PageIndex = e.NewPageIndex;
            gvSearchDebit.DataSource = (List<DiscrepancyBE>)ViewState["AllDebitRaise"];
            gvSearchDebit.DataBind();
        }
    }

    protected void gvSearchDebit_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType != DataControlRowType.Header && e.Row.RowType != DataControlRowType.Footer)
        {
            ucLabel lblDiscrepancyLogIDValue = (ucLabel)e.Row.FindControl("lblDiscrepancyLogIDValue");
            if (lblDiscrepancyLogIDValue != null && (!string.IsNullOrEmpty(lblDiscrepancyLogIDValue.Text) && !string.IsNullOrWhiteSpace(lblDiscrepancyLogIDValue.Text)))
            {
                HyperLink hlVDRNoValue = (HyperLink)e.Row.FindControl("hlVDRNoValue");
                ucLabel lblDiscrepancyTypeIDValue = (ucLabel)e.Row.FindControl("lblDiscrepancyTypeIDValue");
                ucLabel lblCreatedByIdValue = (ucLabel)e.Row.FindControl("lblCreatedByIdValue");

                var VDRNo = hlVDRNoValue.Text;
                switch (Convert.ToInt32(lblDiscrepancyTypeIDValue.Text.Trim()))
                {
                    case 1:
                        hlVDRNoValue.NavigateUrl = EncryptQuery("~/ModuleUI/Discrepancy/LogDiscrepancy/DISLog_Overs.aspx?disLogID=" + lblDiscrepancyLogIDValue.Text + "&VDRNo=" + VDRNo + "&UserID=" + lblCreatedByIdValue.Text + "&PN=SearchDebit&status=");
                        break;
                    case 2:
                        hlVDRNoValue.NavigateUrl = EncryptQuery("~/ModuleUI/Discrepancy/LogDiscrepancy/DISLog_Shortage.aspx?disLogID=" + lblDiscrepancyLogIDValue.Text + "&VDRNo=" + VDRNo + "&UserID=" + lblCreatedByIdValue.Text + "&PN=SearchDebit&status=");
                        break;
                    case 3:
                        hlVDRNoValue.NavigateUrl = EncryptQuery("~/ModuleUI/Discrepancy/LogDiscrepancy/DISLog_GoodsReceivedDamaged.aspx?disLogID=" + lblDiscrepancyLogIDValue.Text + "&VDRNo=" + VDRNo + "&UserID=" + lblCreatedByIdValue.Text + "&PN=SearchDebi&status=t");
                        break;
                    case 4:
                        hlVDRNoValue.NavigateUrl = EncryptQuery("~/ModuleUI/Discrepancy/LogDiscrepancy/DISLog_NoPurchaseOrder.aspx?disLogID=" + lblDiscrepancyLogIDValue.Text + "&VDRNo=" + VDRNo + "&UserID=" + lblCreatedByIdValue.Text + "&PN=SearchDebit&status=");
                        break;
                    case 5:
                        hlVDRNoValue.NavigateUrl = EncryptQuery("~/ModuleUI/Discrepancy/LogDiscrepancy/DISLog_NoPaperwork.aspx?disLogID=" + lblDiscrepancyLogIDValue.Text + "&VDRNo=" + VDRNo + "&UserID=" + lblCreatedByIdValue.Text + "&PN=SearchDebit&status=");
                        break;
                    case 6:
                        hlVDRNoValue.NavigateUrl = EncryptQuery("~/ModuleUI/Discrepancy/LogDiscrepancy/DISLog_IncorrectProduct.aspx?disLogID=" + lblDiscrepancyLogIDValue.Text + "&VDRNo=" + VDRNo + "&UserID=" + lblCreatedByIdValue.Text + "&PN=SearchDebit&status=");
                        break;
                    case 7:
                        hlVDRNoValue.NavigateUrl = EncryptQuery("~/ModuleUI/Discrepancy/LogDiscrepancy/DISLog_PresentationIssue.aspx?disLogID=" + lblDiscrepancyLogIDValue.Text + "&VDRNo=" + VDRNo + "&UserID=" + lblCreatedByIdValue.Text + "&PN=SearchDebit&status=");
                        break;
                    case 8:
                        hlVDRNoValue.NavigateUrl = EncryptQuery("~/ModuleUI/Discrepancy/LogDiscrepancy/DISLog_IncorrectAddress.aspx?disLogID=" + lblDiscrepancyLogIDValue.Text + "&VDRNo=" + VDRNo + "&UserID=" + lblCreatedByIdValue.Text + "&PN=SearchDebit&status=");
                        break;
                    case 9:
                        hlVDRNoValue.NavigateUrl = EncryptQuery("~/ModuleUI/Discrepancy/LogDiscrepancy/DISLog_PaperworkAmended.aspx?disLogID=" + lblDiscrepancyLogIDValue.Text + "&VDRNo=" + VDRNo + "&UserID=" + lblCreatedByIdValue.Text + "&PN=SearchDebit&status=");
                        break;
                    case 10:
                        hlVDRNoValue.NavigateUrl = EncryptQuery("~/ModuleUI/Discrepancy/LogDiscrepancy/DISLog_WrongPackSize.aspx?disLogID=" + lblDiscrepancyLogIDValue.Text + "&VDRNo=" + VDRNo + "&UserID=" + lblCreatedByIdValue.Text + "&PN=SearchDebit&status=");
                        break;
                    case 11:
                        hlVDRNoValue.NavigateUrl = EncryptQuery("~/ModuleUI/Discrepancy/LogDiscrepancy/DISLog_FailPalletSpecification.aspx?disLogID=" + lblDiscrepancyLogIDValue.Text + "&VDRNo=" + VDRNo + "&UserID=" + lblCreatedByIdValue.Text + "&PN=SearchDebit&status=");
                        break;
                    case 12:
                        hlVDRNoValue.NavigateUrl = EncryptQuery("~/ModuleUI/Discrepancy/LogDiscrepancy/DISLog_QualityIssue.aspx?disLogID=" + lblDiscrepancyLogIDValue.Text + "&VDRNo=" + VDRNo + "&UserID=" + lblCreatedByIdValue.Text + "&PN=SearchDebit&status=");
                        break;
                    case 13:
                        hlVDRNoValue.NavigateUrl = EncryptQuery("~/ModuleUI/Discrepancy/LogDiscrepancy/DISLog_PrematureInvoiceReceipt.aspx?disLogID=" + lblDiscrepancyLogIDValue.Text + "&VDRNo=" + VDRNo + "&UserID=" + lblCreatedByIdValue.Text + "&PN=SearchDebit&status=");
                        break;
                    case 14:
                        hlVDRNoValue.NavigateUrl = EncryptQuery("~/ModuleUI/Discrepancy/LogDiscrepancy/DISLog_GenericDescrepancy.aspx?disLogID=" + lblDiscrepancyLogIDValue.Text + "&VDRNo=" + VDRNo + "&UserID=" + lblCreatedByIdValue.Text + "&PN=SearchDebit&status=");
                        break;
                    case 15:
                        hlVDRNoValue.NavigateUrl = EncryptQuery("~/ModuleUI/Discrepancy/LogDiscrepancy/DISLog_Shuttle.aspx?disLogID=" + lblDiscrepancyLogIDValue.Text + "&VDRNo=" + VDRNo + "&UserID=" + lblCreatedByIdValue.Text + "&PN=SearchDebit&status=");
                        break;
                    case 16:
                        hlVDRNoValue.NavigateUrl = EncryptQuery("~/ModuleUI/Discrepancy/LogDiscrepancy/DISLog_Reservation.aspx?disLogID=" + lblDiscrepancyLogIDValue.Text + "&VDRNo=" + VDRNo + "&UserID=" + lblCreatedByIdValue.Text + "&PN=SearchDebit&status=");
                        break;
                    case 17:
                        hlVDRNoValue.NavigateUrl = EncryptQuery("~/ModuleUI/Discrepancy/LogDiscrepancy/DISLog_InvoiceDiscrepancy.aspx?disLogID=" + lblDiscrepancyLogIDValue.Text + "&VDRNo=" + VDRNo + "&UserID=" + lblCreatedByIdValue.Text + "&PN=SearchDebit&status=");
                        break;
                    case 18:
                        hlVDRNoValue.NavigateUrl = EncryptQuery("~/ModuleUI/Discrepancy/LogDiscrepancy/DISLog_StockPlannerInvoiceDiscrepancy.aspx?disLogID=" + lblDiscrepancyLogIDValue.Text + "&VDRNo=" + VDRNo + "&UserID=" + lblCreatedByIdValue.Text + "&PN=SearchDebit&status=");
                        break;

                    case 19:
                        hlVDRNoValue.NavigateUrl = EncryptQuery("~/ModuleUI/Discrepancy/LogDiscrepancy/DISLog_InvoiceQueryDiscrepancy.aspx?disLogID=" + lblDiscrepancyLogIDValue.Text + "&VDRNo=" + VDRNo + "&UserID=" + lblCreatedByIdValue.Text + "&PN=SearchDebit&status=");
                        break;
                }
            }
        }
    }

    #endregion

    private void BindCancelaDebits()
    {
        var discrepancyBE = new DiscrepancyBE();
        var discrepancyBAL = new DiscrepancyBAL();

        discrepancyBE.Action = "GetAllDebitRaise";
        if (!string.IsNullOrEmpty(msDebitType.SelectedDebitTypeIDs))
            discrepancyBE.DebitReasonIds = msDebitType.SelectedDebitTypeIDs;

        if (!string.IsNullOrEmpty(msVendor.SelectedVendorIDs))
            discrepancyBE.VendorIds = msVendor.SelectedVendorIDs;

        if (!string.IsNullOrEmpty(msAP.SelectedStockPlannerIDs))
            discrepancyBE.CreatedByIds = msAP.SelectedStockPlannerIDs;

        discrepancyBE.DebitRaiseFromDate = string.IsNullOrEmpty(txtFromDate.Text) ? (DateTime?)null : Common.GetMM_DD_YYYY(txtFromDate.Text);
        discrepancyBE.DebitRaiseToDate = string.IsNullOrEmpty(txtToDate.Text) ? (DateTime?)null : Common.GetMM_DD_YYYY(txtToDate.Text);

        if (ddlDebitStatus.SelectedValue.Equals("A") || ddlDebitStatus.SelectedValue.Equals("C"))
            discrepancyBE.Status = ddlDebitStatus.SelectedValue;

        if (!string.IsNullOrEmpty(msDebitNoteNumber.SelectedDebitNoteNumber))
            discrepancyBE.DebitNoteNumbers = msDebitNoteNumber.SelectedDebitNoteNumber;

        if (!string.IsNullOrEmpty(msPurchaseOrderNumber.SelectedPO))
            discrepancyBE.PONumbers = msPurchaseOrderNumber.SelectedPO;

        discrepancyBE.UserID = Convert.ToInt32(Convert.ToSingle(Session["UserID"]));
        discrepancyBE.UserRoleID = Convert.ToInt32(Convert.ToSingle(Session["UserRoleId"]));

        //----------phase 18 R2 point 10 -------------------------------//
        if (GetQueryStringValue("PreviousPage") != null && GetQueryStringValue("PreviousPage").ToString() == "POInquiry") {
            discrepancyBE.PONumbers = GetQueryStringValue("Purchase_order");
            string[] strparams = new string[3];
            strparams = GetQueryStringValue("Params").Split('~');
            discrepancyBE.VendorIds = strparams[0];
            //discrepancyBE.SiteID = strparams[1];
            discrepancyBE.PONumbers = strparams[2]; 
            discrepancyBE.Status = null;
        } 
        //--------------------------------------------------------------//

        var lstDiscrepancyBE = discrepancyBAL.GetAllDebitRaiseBAL(discrepancyBE);
        gvSearchDebit.DataSource = null;
        gvSearchDebitExcel.DataSource = null;   
        if (lstDiscrepancyBE != null && lstDiscrepancyBE.Count > 0)
        {
            gvSearchDebitExcel.DataSource = lstDiscrepancyBE;
            gvSearchDebit.DataSource = lstDiscrepancyBE;
            ViewState["AllDebitRaise"] = lstDiscrepancyBE;
            ucExportToExcel1.Visible = true;
        }
        gvSearchDebit.DataBind();
        gvSearchDebitExcel.DataBind();

        pnlDebitLibrary1.Visible = true;
        pnlSearchDebit1.Visible = false;
    }

    private void ClearCriteria()
    {
        msVendor.SelectedVendorIDs = string.Empty;
    }
        
    public override void TemplateSelectedIndexChanged()
    {
        base.TemplateSelectedIndexChanged();

        int VendorTemplateId = Convert.ToInt32(ucVendorTemplateSelect.innerControlddlTemplate.SelectedValue);

        ListBox lstSelectedVendor = (ListBox)msVendor.FindControl("lstRight");
        ListBox lstVendor = (ListBox)msVendor.FindControl("ucVendor").FindControl("lstLeft");

        if (VendorTemplateId > 0)
        {

            SCT_TemplateBAL oSCT_TemplateBAL = new SCT_TemplateBAL();
            SCT_TemplateBE oSCT_TemplateBE = new SCT_TemplateBE();

            oSCT_TemplateBE.VendorTemplateID = VendorTemplateId;
            oSCT_TemplateBE.Action = "GetVendorTemplateInfobyID";
            List<SCT_TemplateBE> lstVendorTemplate = oSCT_TemplateBAL.GetVendorTemplateById(oSCT_TemplateBE);

            if (lstVendorTemplate != null && lstVendorTemplate.Count > 0)
            {
                lstSelectedVendor.Items.Clear();
                msVendor.innerControlHiddenField.Value = string.Empty;
                msVendor.innerControlHiddenField2.Value = string.Empty;
                for (int iCount = 0; iCount < lstVendorTemplate.Count; iCount++)
                {
                    lstSelectedVendor.Items.Add(new ListItem(lstVendorTemplate[iCount].Vendor.Vendor_Name, lstVendorTemplate[iCount].Vendor.VendorID.ToString()));
                    msVendor.SelectedVendorIDs = lstVendorTemplate[iCount].Vendor.VendorID.ToString();
                    msVendor.SelectedVendorName = lstVendorTemplate[iCount].Vendor.Vendor_Name.ToString();
                }
            }
        }
        else
        {
            lstSelectedVendor.Items.Clear();
            msVendor.innerControlHiddenField.Value = string.Empty;
            msVendor.innerControlHiddenField2.Value = string.Empty;
        }
    }

    private void BindDebitLibrary(int page = 1)
    {
        APActionBE oApActionBE = new APActionBE();
        APActionBE oApActionBE1 = new APActionBE();
        APActionBE oApActionBE2 = new APActionBE();
        APActionBE oApActionBE3 = new APActionBE();
        List<APActionBE> lstAPActionBE = new List<APActionBE>();

        oApActionBE.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();
        oApActionBE.DebitStatus = "A";
        oApActionBE.Vendor.Vendor_No = "ACCO";
        oApActionBE.Vendor.VendorName = "ACCO CONSOL";
        oApActionBE.DiscrepancyNoteNo = "A000000455";
        oApActionBE.DateDebitRaised = Common.GetMM_DD_YYYY("21/09/2013");
        oApActionBE.RaisedBy = "Super Admin";
        oApActionBE.DebitNoteNumber = "DNFR12345";
        //oApActionBE.DebitReason = "Reason1";
        oApActionBE.ValueofDebit = 50;
        oApActionBE.Currency = "GBP";
        oApActionBE.DebitSentTo = "accotest@btinternet.com";
        oApActionBE.CancelledBy = "";
        oApActionBE.CancelledDate = (DateTime?)null;
        oApActionBE.CancelledSentTo = "";
        oApActionBE.Discrepancy = new BusinessEntities.ModuleBE.Discrepancy.LogDiscrepancy.DiscrepancyBE();
        oApActionBE.Discrepancy.DiscrepancyLogID = 893;
        oApActionBE.Discrepancy.VDRNo = "A000000455";
        oApActionBE.UserID = Convert.ToInt32(Session["UserId"]);
        lstAPActionBE.Add(oApActionBE);


        oApActionBE1.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();
        oApActionBE1.DebitStatus = "C";
        oApActionBE1.Vendor.Vendor_No = "001";
        oApActionBE1.Vendor.VendorName = "Filofax";
        oApActionBE1.DiscrepancyNoteNo = "I000000086";
        oApActionBE1.DateDebitRaised = Common.GetMM_DD_YYYY("15/08/2013");
        oApActionBE1.RaisedBy = "Super Admin";
        oApActionBE1.DebitNoteNumber = "DNUK4343";
        //oApActionBE1.DebitReason = "Reason2";
        oApActionBE1.ValueofDebit = 45;
        oApActionBE1.Currency = "Currency2";
        oApActionBE1.DebitSentTo = "filofax@btinternet.com";
        oApActionBE1.CancelledBy = "Rene Hendrix";
        oApActionBE1.CancelledDate = Common.GetMM_DD_YYYY("18/08/2013");
        oApActionBE1.CancelledSentTo = "accotest@btinternet.com";
        oApActionBE1.Discrepancy = new BusinessEntities.ModuleBE.Discrepancy.LogDiscrepancy.DiscrepancyBE();
        oApActionBE1.Discrepancy.DiscrepancyLogID = 893;
        oApActionBE1.Discrepancy.VDRNo = "A000000455";
        oApActionBE1.UserID = Convert.ToInt32(Session["UserId"]);
        lstAPActionBE.Add(oApActionBE1);

        oApActionBE2.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();
        oApActionBE2.DebitStatus = "A";
        oApActionBE2.Vendor.Vendor_No = "003";
        oApActionBE2.Vendor.VendorName = "BIRO BIC LTD";
        oApActionBE2.DiscrepancyNoteNo = "B000000201";
        oApActionBE2.DateDebitRaised = Common.GetMM_DD_YYYY("10/08/2013");
        oApActionBE2.RaisedBy = "Super Admin";
        oApActionBE2.DebitNoteNumber = "DNUK5453";
        //oApActionBE2.DebitReason = "Reason2";
        oApActionBE2.ValueofDebit = 30;
        oApActionBE2.Currency = "GBP";
        oApActionBE2.DebitSentTo = "filofa@btinternet.com";
        oApActionBE2.CancelledBy = "";
        oApActionBE2.CancelledDate = (DateTime?)null;
        oApActionBE2.CancelledSentTo = "";
        oApActionBE2.Discrepancy = new BusinessEntities.ModuleBE.Discrepancy.LogDiscrepancy.DiscrepancyBE();
        oApActionBE2.Discrepancy.DiscrepancyLogID = 893;
        oApActionBE2.Discrepancy.VDRNo = "A000000455";
        oApActionBE2.UserID = Convert.ToInt32(Session["UserId"]);
        lstAPActionBE.Add(oApActionBE2);

        oApActionBE3.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();
        oApActionBE3.DebitStatus = "A";
        oApActionBE3.Vendor.Vendor_No = "012";
        oApActionBE3.Vendor.VendorName = "CPD";
        oApActionBE3.DiscrepancyNoteNo = "D000003463";
        oApActionBE3.DateDebitRaised = Common.GetMM_DD_YYYY("10/08/2013");
        oApActionBE3.RaisedBy = "Super Admin";
        oApActionBE3.DebitNoteNumber = "DNUK5453";
        //oApActionBE3.DebitReason = "Reason2";
        oApActionBE3.ValueofDebit = 30;
        oApActionBE3.Currency = "GBP";
        oApActionBE3.DebitSentTo = "cpd@gmail.com";
        oApActionBE3.CancelledBy = "";
        oApActionBE3.CancelledDate = (DateTime?)null;
        oApActionBE3.CancelledSentTo = "";
        oApActionBE3.Discrepancy = new BusinessEntities.ModuleBE.Discrepancy.LogDiscrepancy.DiscrepancyBE();
        oApActionBE3.Discrepancy.DiscrepancyLogID = 893;
        oApActionBE3.Discrepancy.VDRNo = "A000000455";
        oApActionBE3.UserID = Convert.ToInt32(Session["UserId"]);
        lstAPActionBE.Add(oApActionBE3);

        lstAPActionBE.Add(oApActionBE);
        lstAPActionBE.Add(oApActionBE1);
        lstAPActionBE.Add(oApActionBE2);
        lstAPActionBE.Add(oApActionBE3);



        if (lstAPActionBE != null && lstAPActionBE.Count > 0)
        {
            ucExportToExcel1.Visible = true;
            gvSearchDebit.DataSource = lstAPActionBE;
            gvSearchDebit.DataBind();
            //pager1.ItemCount = lstAPActionBE[0].TotalRecords;
            //pager1.Visible = true;

        }


        //gvSearchDebit.PageIndex = page;
        //pager1.CurrentIndex = page;

        pnlDebitLibrary1.Visible = true;
        pnlSearchDebit1.Visible = false;




    }

   
}
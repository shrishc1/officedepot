﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using BusinessLogicLayer.ModuleBAL.Discrepancy.LogDiscrepancy;
using BusinessEntities.ModuleBE.Discrepancy.LogDiscrepancy;
using WebUtilities;
using Utilities;
using BaseControlLibrary;
using System.Data;
using BusinessEntities.ModuleBE.Discrepancy.APAction;
using BusinessLogicLayer.ModuleBAL.Discrepancy.CountrySetting;
using BusinessEntities.ModuleBE.StockOverview;
using BusinessLogicLayer.ModuleBAL.StockOverview;
using BusinessLogicLayer.ModuleBAL.AdminFunctions;
using BusinessEntities.ModuleBE.AdminFunctions;
using BusinessLogicLayer.ModuleBAL.Appointment.SiteSettings;
using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using BusinessLogicLayer.ModuleBAL.Appointment.Booking;
using BusinessEntities.ModuleBE.Languages.Languages;
using System.IO;
using System.Configuration;
using System.Text;

public partial class ModuleUI_Discrepancy_AccountsPayable_DebitRaise : CommonPage
{
    #region Declarations ...

    DataSet dsDummy = null; //dummy dataset use for repeater
    DataTable tbDummy = null; //dummy table for dataset
    static BaseControlLibrary.ucTextbox currentTextBox;

    protected string DebitRaisedSuccessfully = WebCommon.getGlobalResourceValue("DebitRaisedSuccessfully");
    protected string AlreadyExistLineNumber = WebCommon.getGlobalResourceValue("AlreadyExistLineNumber");
    protected string AtLeastOne = WebCommon.getGlobalResourceValue("InvoiceAtLeastOne");
    protected string ProductDescription = WebCommon.getGlobalResourceValue("ProductDescription");
    protected string LineNoReq = WebCommon.getGlobalResourceValue("LineNoReq");
    protected string OriginalPOQuantityMandatry = WebCommon.getGlobalResourceValue("OriginalPOQuantityMandatry");
    protected string OutstandingPOQuantityMandatry = WebCommon.getGlobalResourceValue("OutstandingPOQuantityMandatry");
    protected string POItemCostMandatry = WebCommon.getGlobalResourceValue("POItemCostMandatry");
    protected string ValuetobeDebitedMandatry = WebCommon.getGlobalResourceValue("ValuetobeDebitedMandatry");
    protected string ReasonForDebitMandatry = WebCommon.getGlobalResourceValue("ReasonForDebitMandatry");
    string PleaseselecttheReasonforDebitforValuetobeDebited = WebCommon.getGlobalResourceValue("PleaseselecttheReasonforDebitforValuetobeDebited");
    string PleaseentertheValuetobeDebitedforReasonforDebit = WebCommon.getGlobalResourceValue("PleaseentertheValuetobeDebitedforReasonforDebit");
    string PleasefillatleastoneitementryforRaiseaDebitnote = WebCommon.getGlobalResourceValue("PleasefillatleastoneitementryforRaiseaDebitnote");
    string PleaseentertheValuetobeDebited = WebCommon.getGlobalResourceValue("PleaseentertheValuetobeDebited");
    string PleaseselecttheReasonforDebit = WebCommon.getGlobalResourceValue("PleaseselecttheReasonforDebit");
    string ValueInDisputeMandatry = WebCommon.getGlobalResourceValue("ValueInDisputeMandatry");
    string CurrencyMandatry = WebCommon.getGlobalResourceValue("CurrencyMandatry");
    string ReasonMandatry = WebCommon.getGlobalResourceValue("ReasonMandatry");
    string PurchaseOrderNumberValidation = WebCommon.getGlobalResourceValue("PurchaseOrderNumberValidation");
    string SelectAVendor = WebCommon.getGlobalResourceValue("SelectAVendor");
    string PurchaseOrderDebitReasonOther = WebCommon.getGlobalResourceValue("PurchaseOrderDebitReasonOther");
    string PurchaseOrderDebitPriceDifference = WebCommon.getGlobalResourceValue("PurchaseOrderDebitPriceDifference");
    string PleaseentertheQtyInvoicedforenteredInvoicedItemPrice = WebCommon.getGlobalResourceValue("PleaseentertheQtyInvoicedforenteredInvoicedItemPrice");
    string PleaseentertheInvoicedItemPriceforenteredQtyInvoiced = WebCommon.getGlobalResourceValue("PleaseentertheInvoicedItemPriceforenteredQtyInvoiced");
    string PleaseentertheDebitValue = WebCommon.getGlobalResourceValue("PleaseentertheDebitValue");
    string PleaseentertheVendorVATReference = WebCommon.getGlobalResourceValue("PleaseentertheVendorVATReference");
    string PleaseselecttheOfficeDepotVATReference = WebCommon.getGlobalResourceValue("PleaseselecttheOfficeDepotVATReference");
    string DebitNoteAgainstDiscEmailSubject = WebCommon.getGlobalResourceValue("DebitNoteAgainstDiscEmailSubject");

    #endregion

    #region Events ...

    protected void Page_InIt(object sender, EventArgs e)
    {
        ucSite.CurrentPage = this;
        //ucSiteVendor.CurrentPage = this;
        //ucSeacrhVendor1.CurrentPage = this;
        //ucSeacrhVendor1.IsStandAloneRequired = true;
        //ucSeacrhVendor1.IsChildRequired = true;
        //ucSeacrhVendor1.IsParentRequired = true;
        //ucSeacrhVendor1.IsGrandParentRequired = false;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        dsDummy = new DataSet();
        tbDummy = new DataTable();
        //btnConfirm.Visible = false;
        //btnSave.Visible = false;
        //lblRaiseaDebitAgainstPo.Visible = false;
        //lblRaiseaDebitAgainstVendor.Visible = false;

        if (GetQueryStringValue("PageMode") != null && !string.IsNullOrWhiteSpace(GetQueryStringValue("PageMode").ToString()))
        {
            mvDebitRaiseType.ActiveViewIndex = 3;
            GetContactPurchaseOrderDetails();
            btnConfirm.Visible = false;
            btnSave.Visible = false;
            btnProceed.Visible = false;
            // btnBack.Visible = true;
            //lblRaiseaDebitAgainstPo.Visible = true;
            if (rblPriceOther.SelectedIndex.Equals(0))
            {
                lblPurchaseOrderDebitPriceDifference.Visible = true;
                lblPurchaseOrderDebitReasonOther.Visible = false;
            }
            else
            {
                lblPurchaseOrderDebitPriceDifference.Visible = false;
                lblPurchaseOrderDebitReasonOther.Visible = true;
            }

            lblRaiseaDebitAgainstVendor.Visible = false;
            lblRaiseaDebit.Visible = false;
        }
        //if (mvDebitRaiseType.ActiveViewIndex == 2)
        //{
        //    btnConfirm.Visible = true;
        //}
        //if (mvDebitRaiseType.ActiveViewIndex == 3)
        //{
        //    btnSave.Visible = true;
        //}

        if (!IsPostBack)
        {
            this.GetAllReasonsForDebit();
            this.BindCurrency();
            this.BindPriceOtherRadioButton();
        }
    }

    public override void SitePost_Load()
    {
        base.SitePost_Load();
        if (!IsPostBack)
        {
            //ucSite.innerControlddlSite.AutoPostBack = true;
            //ucSiteVendor.innerControlddlSite.AutoPostBack = true;
            //if (GetQueryStringValue("disLogID") == null && !string.IsNullOrEmpty(ucSiteVendor.innerControlddlSite.SelectedValue))
            //{
            //    ucSeacrhVendor1.SiteID = Convert.ToInt32(ucSiteVendor.innerControlddlSite.SelectedValue);
            //}

            ucSite.innerControlddlSite.AutoPostBack = true;
            this.SetPOODVATReference();
        }
    }

    public override void SiteSelectedIndexChanged()
    {
        //TextBox txtVendor = (TextBox)ucSeacrhVendor1.FindControl("txtVendorNo");
        //Label lblSelectedVendorName = (Label)ucSeacrhVendor1.FindControl("SelectedVendorName");
        //txtVendor.Text = string.Empty;
        //lblSelectedVendorName.Text = string.Empty;

        //if (GetQueryStringValue("disLogID") == null && !string.IsNullOrEmpty(ucSiteVendor.innerControlddlSite.SelectedValue))
        //    ucSeacrhVendor1.SiteID = Convert.ToInt32(ucSiteVendor.innerControlddlSite.SelectedValue);

        this.SetPOODVATReference();

        #region Clearing PO details ...
        txtPurchaseOrder.Text = string.Empty;
        ddlPurchaseOrderDate.Items.Clear();
        ListItem listItem = new ListItem();
        listItem.Text = "--Select--";
        listItem.Value = "0";
        ddlPurchaseOrderDate.Items.Add(listItem);
        lblVendorValue.Text = string.Empty;
        txtVendorVATReferenceValue.Text = string.Empty;
        txtInvoiceNo.Text = string.Empty;
        txtPurchaseOrder.Focus();
        #endregion
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        switch (mvDebitRaiseType.ActiveViewIndex)
        {
            case 4:
                lblRaiseaDebit.Visible = false;
                lblRaiseaDebitAgainstPo.Visible = false;
                lblPurchaseOrderDebitPriceDifference.Visible = false;
                lblPurchaseOrderDebitReasonOther.Visible = false;

                lblRaiseaDebitAgainstVendor.Visible = true;
                mvDebitRaiseType.ActiveViewIndex = 2;
                btnProceed.Visible = false;
                btnConfirm.Visible = true;
                btnSave.Visible = false;
                break;
            case 3:
                lblRaiseaDebit.Visible = false;
                lblRaiseaDebitAgainstPo.Visible = true;

                if (rblPriceOther.SelectedIndex.Equals(0))
                {
                    lblPurchaseOrderDebitPriceDifference.Visible = false;
                    lblPurchaseOrderDebitReasonOther.Visible = false;
                }
                else
                {
                    lblPurchaseOrderDebitPriceDifference.Visible = false;
                    lblPurchaseOrderDebitReasonOther.Visible = false;
                }

                lblRaiseaDebitAgainstVendor.Visible = false;
                mvDebitRaiseType.ActiveViewIndex = 1;
                btnProceed.Visible = false;
                btnConfirm.Visible = true;
                btnSave.Visible = false;
                break;
            case 2:
                lblRaiseaDebit.Visible = true;
                lblRaiseaDebitAgainstPo.Visible = false;
                lblPurchaseOrderDebitPriceDifference.Visible = false;
                lblPurchaseOrderDebitReasonOther.Visible = false;

                lblRaiseaDebitAgainstVendor.Visible = false;
                mvDebitRaiseType.ActiveViewIndex = 0;
                btnBack.Visible = false;
                btnProceed.Visible = true;
                btnConfirm.Visible = false;
                break;
            case 1:
                lblRaiseaDebit.Visible = true;
                lblRaiseaDebitAgainstPo.Visible = false;
                lblPurchaseOrderDebitPriceDifference.Visible = false;
                lblPurchaseOrderDebitReasonOther.Visible = false;

                lblRaiseaDebitAgainstVendor.Visible = false;
                mvDebitRaiseType.ActiveViewIndex = 0;
                btnBack.Visible = false;
                btnProceed.Visible = true;
                btnConfirm.Visible = false;
                break;
        }
    }

    protected void btnProceed_Click(object sender, EventArgs e)
    {
        if (rdoDebitLinkedToPO.Checked)
        {
            lblRaiseaDebit.Visible = false;
            lblRaiseaDebitAgainstPo.Visible = true;
            //if (rblPriceOther.SelectedIndex.Equals(0))
            //{
            //    lblRaiseaDebitAgainstPo.Visible = true;
            //    lblPurchaseOrderDebitPriceDifference.Visible = false;
            //    lblPurchaseOrderDebitReasonOther.Visible = false;
            //}
            //else
            //{
            //    lblRaiseaDebitAgainstPo.Visible = true;
            //    lblPurchaseOrderDebitPriceDifference.Visible = false;
            //    lblPurchaseOrderDebitReasonOther.Visible = false;
            //}

            lblPurchaseOrderDebitPriceDifference.Visible = false;
            lblPurchaseOrderDebitReasonOther.Visible = false;
            lblRaiseaDebitAgainstVendor.Visible = false;
            mvDebitRaiseType.ActiveViewIndex = 1;
        }
        else if (rdoDebitLinkedtoVendor.Checked)
        {
            #region Clearing PO details ...
            txtPurchaseOrder.Text = string.Empty;
            ddlPurchaseOrderDate.Items.Clear();
            ListItem listItem = new ListItem();
            listItem.Text = "--Select--";
            listItem.Value = "0";
            ddlPurchaseOrderDate.Items.Add(listItem);
            lblVendorValue.Text = string.Empty;
            txtVendorVATReferenceValue.Text = string.Empty;
            txtInvoiceNo.Text = string.Empty;
            txtPurchaseOrder.Focus();
            #endregion

            lblRaiseaDebit.Visible = false;
            //lblRaiseaDebitAgainstPo.Visible = false;
            lblPurchaseOrderDebitPriceDifference.Visible = false;
            lblPurchaseOrderDebitReasonOther.Visible = false;

            lblRaiseaDebitAgainstVendor.Visible = true;
            mvDebitRaiseType.ActiveViewIndex = 2;
        }

        btnBack.Visible = true;
        btnProceed.Visible = false;
        btnConfirm.Visible = true;
    }

    protected void btnConfirm_Click(object sender, EventArgs e)
    {
        if (rdoDebitLinkedToPO.Checked)
        {
            if (string.IsNullOrEmpty(txtPurchaseOrder.Text) && string.IsNullOrWhiteSpace(txtPurchaseOrder.Text))
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + PurchaseOrderNumberValidation + "')", true);
                txtPurchaseOrder.Focus();
                return;
            }

            if (string.IsNullOrEmpty(txtVendorVATReferenceValue.Text) && string.IsNullOrWhiteSpace(txtVendorVATReferenceValue.Text))
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + PleaseentertheVendorVATReference + "')", true);
                txtPurchaseOrder.Focus();
                return;
            }

            mvDebitRaiseType.ActiveViewIndex = 3;
            lblRaiseaDebit.Visible = false;
            //lblRaiseaDebitAgainstPo.Visible = true;
            if (rblPriceOther.SelectedIndex.Equals(0))
            {
                lblRaiseaDebitAgainstPo.Visible = false;
                lblPurchaseOrderDebitPriceDifference.Visible = true;
                lblPurchaseOrderDebitReasonOther.Visible = false;
                lblDebitValue.Visible = true;
                lblDebitValueColon.Visible = true;
                txtDebitValueText.Visible = true;
            }
            else
            {
                lblRaiseaDebitAgainstPo.Visible = false;
                lblPurchaseOrderDebitPriceDifference.Visible = false;
                lblPurchaseOrderDebitReasonOther.Visible = true;
                lblDebitValue.Visible = false;
                lblDebitValueColon.Visible = false;
                txtDebitValueText.Visible = false;
            }

            lblRaiseaDebitAgainstVendor.Visible = false;
            this.GetDeliveryDetails();
            this.GetContactPurchaseOrderDetails();
        }
        else if (rdoDebitLinkedtoVendor.Checked)
        {
            if ((string.IsNullOrEmpty(hdnLocalvendorID.Value) && string.IsNullOrWhiteSpace(hdnLocalvendorID.Value)) || hdnLocalvendorID.Value.Equals("0"))
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + SelectAVendor + "')", true);
                txtPurchaseOrder.Focus();
                return;
            }

            mvDebitRaiseType.ActiveViewIndex = 4;
            lblRaiseaDebit.Visible = false;
            //lblRaiseaDebitAgainstPo.Visible = false;
            lblPurchaseOrderDebitPriceDifference.Visible = false;
            lblPurchaseOrderDebitReasonOther.Visible = false;

            lblRaiseaDebitAgainstVendor.Visible = true;
            ucSDRCommunication2.FillAPContacts(Convert.ToInt32(hdnLocalvendorID.Value));
            this.SetPOVendorVATReference(Convert.ToInt32(hdnLocalvendorID.Value));
            this.SetVendODVATReference();

            var index = hdnLocalvendorname.Value.IndexOf(")");
            if (index > 0)
                lblVendorNumberValue_Vendor.Text = hdnLocalvendorname.Value.Remove(0, index + 1);

        }

        if (rblPriceOther.SelectedIndex.Equals(1))
        {
            gvLogInvoiceAddLine.DataSource = null;
            gvLogInvoiceAddLine.DataBind();
            gvLogInvoiceAddLine.Visible = true;
            this.IncreaseRow();
        }
        else
        {
            gvLogInvoiceAddLine.DataSource = null;
            gvLogInvoiceAddLine.DataBind();
            gvLogInvoiceAddLine.Visible = false;
        }

        btnSave.Visible = true;
        btnBack.Visible = true;
        btnConfirm.Visible = false;
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        #region Validations ...
        if (rdoDebitLinkedToPO.Checked)
        {
            bool checkStatus = false;

            /* For Price Query Radio Button Option : Grid Validation */
            if (rblPriceOther.SelectedIndex.Equals(0))
            {
                #region PO Price Query Option Grid Validation ...
                foreach (GridViewRow row in gvLogInvoice.Rows)
                {
                    var txtInvoicedItemPriceValue = (TextBox)row.FindControl("txtInvoicedItemPriceValue");
                    var txtQtyInvoicedValue = (TextBox)row.FindControl("txtQtyInvoicedValue");
                    var hdnPriceDifferenceValue = (HiddenField)row.FindControl("hdnPriceDifferenceValue");
                    var lblPriceDifferenceValue = (Label)row.FindControl("lblPriceDifferenceValue");

                    if (lblPriceDifferenceValue != null && hdnPriceDifferenceValue != null)
                    {
                        lblPriceDifferenceValue.Text = hdnPriceDifferenceValue.Value;
                    }

                    if ((!string.IsNullOrEmpty(txtInvoicedItemPriceValue.Text) && !string.IsNullOrWhiteSpace(txtInvoicedItemPriceValue.Text)) &&
                        (string.IsNullOrEmpty(txtQtyInvoicedValue.Text) && string.IsNullOrWhiteSpace(txtQtyInvoicedValue.Text)))
                    {
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + PleaseentertheQtyInvoicedforenteredInvoicedItemPrice + "')", true);
                        txtInvoicedItemPriceValue.Focus();
                        return;
                    }
                    else if ((!string.IsNullOrEmpty(txtQtyInvoicedValue.Text) && !string.IsNullOrWhiteSpace(txtQtyInvoicedValue.Text)) &&
                        (string.IsNullOrEmpty(txtInvoicedItemPriceValue.Text) && string.IsNullOrWhiteSpace(txtInvoicedItemPriceValue.Text)))
                    {
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + PleaseentertheInvoicedItemPriceforenteredQtyInvoiced + "')", true);
                        txtInvoicedItemPriceValue.Focus();
                        return;
                    }
                    else if ((string.IsNullOrEmpty(hdnPriceDifferenceValue.Value) && string.IsNullOrWhiteSpace(hdnPriceDifferenceValue.Value)))
                    {
                        if (!checkStatus)
                            checkStatus = false;
                    }
                    else
                    {
                        checkStatus = true;
                    }
                }
                #endregion

                if (!checkStatus)
                {
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + PleasefillatleastoneitementryforRaiseaDebitnote + "')", true);
                    gvLogInvoice.Focus();
                    return;
                }

                if (ddlCurrencyPO.Items.Count.Equals(0))
                {
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + CurrencyMandatry + "')", true);
                    ddlCurrencyPO.Focus();
                    return;
                }

                if (ddlCurrencyPO.SelectedIndex.Equals(0))
                {
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + CurrencyMandatry + "')", true);
                    ddlCurrencyPO.Focus();
                    return;
                }

                if (string.IsNullOrEmpty(txtDebitValueText.Text) && string.IsNullOrWhiteSpace(txtDebitValueText.Text))
                {
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + PleaseentertheDebitValue + "')", true);
                    txtDebitValueText.Focus();
                    return;
                }
            }
            else /* For Other Radio Button Option : Grid Validation */
            {
                #region PO Other Option Grid Validation ...
                foreach (GridViewRow row in gvLogInvoice.Rows)
                {
                    var txtValueDebited = (ucTextbox)row.FindControl("txtValueDebited");
                    var ddlGRDReason = (ucDropdownList)row.FindControl("ddlReason");
                    if ((!string.IsNullOrEmpty(txtValueDebited.Text) && !string.IsNullOrWhiteSpace(txtValueDebited.Text)) && ddlGRDReason.SelectedIndex.Equals(0))
                    {
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + PleaseselecttheReasonforDebitforValuetobeDebited + "')", true);
                        ddlGRDReason.Focus();
                        return;
                    }
                    else if (ddlGRDReason.SelectedIndex > 0 && (string.IsNullOrEmpty(txtValueDebited.Text) && string.IsNullOrWhiteSpace(txtValueDebited.Text)))
                    {
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + PleaseentertheValuetobeDebitedforReasonforDebit + "')", true);
                        txtValueDebited.Focus();
                        return;
                    }
                    else if (ddlGRDReason.SelectedIndex.Equals(0) && (string.IsNullOrEmpty(txtValueDebited.Text) && string.IsNullOrWhiteSpace(txtValueDebited.Text))
                        && !checkStatus.Equals(true))
                    {
                        checkStatus = false;
                    }
                    else
                    {
                        checkStatus = true;
                    }
                }

                foreach (GridViewRow row in gvLogInvoiceAddLine.Rows)
                {
                    var txtValueDebited = (ucTextbox)row.FindControl("txtValueDebited");
                    var ddlGRDReason = (ucDropdownList)row.FindControl("ddlReason");
                    if ((!string.IsNullOrEmpty(txtValueDebited.Text) && !string.IsNullOrWhiteSpace(txtValueDebited.Text)) && ddlGRDReason.SelectedIndex.Equals(0))
                    {
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + PleaseselecttheReasonforDebitforValuetobeDebited + "')", true);
                        ddlGRDReason.Focus();
                        return;
                    }
                    else if (ddlGRDReason.SelectedIndex > 0 && (string.IsNullOrEmpty(txtValueDebited.Text) && string.IsNullOrWhiteSpace(txtValueDebited.Text)))
                    {
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + PleaseentertheValuetobeDebitedforReasonforDebit + "')", true);
                        txtValueDebited.Focus();
                        return;
                    }
                    else if (ddlGRDReason.SelectedIndex.Equals(0) && (string.IsNullOrEmpty(txtValueDebited.Text) && string.IsNullOrWhiteSpace(txtValueDebited.Text))
                        && !checkStatus.Equals(true))
                    {
                        checkStatus = false;
                    }
                    else
                    {
                        checkStatus = true;
                    }
                }
                #endregion

                if (!checkStatus)
                {
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + PleasefillatleastoneitementryforRaiseaDebitnote + "')", true);
                    gvLogInvoice.Focus();
                    return;
                }

                if (ddlCurrencyPO.Items.Count.Equals(0))
                {
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + CurrencyMandatry + "')", true);
                    ddlCurrencyPO.Focus();
                    return;
                }

                if (ddlCurrencyPO.SelectedIndex.Equals(0))
                {
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + CurrencyMandatry + "')", true);
                    ddlCurrencyPO.Focus();
                    return;
                }
            }
        }
        else if (rdoDebitLinkedtoVendor.Checked)
        {
            if (string.IsNullOrEmpty(txtVendVendorVATReference.Text) && string.IsNullOrWhiteSpace(txtVendVendorVATReference.Text))
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + PleaseentertheVendorVATReference + "')", true);
                txtVendVendorVATReference.Focus();
                return;
            }

            if (ddlVendOfficeDepotVATReference.Items.Count.Equals(0))
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + PleaseselecttheOfficeDepotVATReference + "')", true);
                ddlVendOfficeDepotVATReference.Focus();
                return;
            }

            if (ddlVendOfficeDepotVATReference.SelectedIndex.Equals(0))
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + PleaseselecttheOfficeDepotVATReference + "')", true);
                ddlVendOfficeDepotVATReference.Focus();
                return;
            }

            if (ddlCurrency.Items.Count.Equals(0))
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + CurrencyMandatry + "')", true);
                ddlCurrency.Focus();
                return;
            }

            if (ddlCurrency.SelectedIndex.Equals(0))
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + CurrencyMandatry + "')", true);
                ddlCurrency.Focus();
                return;
            }

            if (string.IsNullOrEmpty(txtValueTobeDebited.Text) && string.IsNullOrWhiteSpace(txtValueTobeDebited.Text))
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + PleaseentertheValuetobeDebited + "')", true);
                txtValueTobeDebited.Focus();
                return;
            }

            if (ddlReason.Items.Count.Equals(0))
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + PleaseselecttheReasonforDebit + "')", true);
                ddlReason.Focus();
                return;
            }

            if (ddlReason.SelectedIndex.Equals(0))
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + PleaseselecttheReasonforDebit + "')", true);
                ddlReason.Focus();
                return;
            }
        }
        #endregion

        #region Data Saving Processes ...
        var emailIds = string.Empty;
        var discrepancyBE = new DiscrepancyBE();
        var discrepancyBAL = new DiscrepancyBAL();
        discrepancyBE.Action = "AddDebitRaise";

        if (rdoDebitLinkedToPO.Checked)
        {
            /* For Price Query Radio Button Option */
            if (rblPriceOther.SelectedIndex.Equals(0))
            {
                discrepancyBE.DebitRaseType = "POPQ"; // Here POPQ means "Purchase Order Price Query"
            }
            else /* For Other Radio Button Option */
            {
                discrepancyBE.DebitRaseType = "POO"; // Here POO means "Purchase Order Other"
            }

            discrepancyBE.PurchaseOrderNumber = lblPurchaseOrderValue.Text;
            discrepancyBE.PORaisedDate = Common.GetMM_DD_YYYY(ddlPurchaseOrderDate.SelectedItem.Text);
            discrepancyBE.SiteID = Convert.ToInt32(ucSite.innerControlddlSite.SelectedValue);
            discrepancyBE.VendorID = Convert.ToInt32(ViewState["VendorID"]);
            discrepancyBE.InvoiceNo = lblInvoiceNoValue.Text;
            discrepancyBE.ReferenceNo = string.Empty;
            discrepancyBE.VendorVatReference = lblVendorVATHashValue.Text;

            if (ViewState["ODVatCodeID"] != null)
                discrepancyBE.ODVatReferenceID = Convert.ToInt32(ViewState["ODVatCodeID"]);

            discrepancyBE.DebitTotalValue = this.GetTotalValueDebited();

            var txtCommEmailList = (ucTextbox)ucSDRCommunication1.FindControl("txtVendorEmailList");
            var txtAdditionalEmailList = (ucTextbox)ucSDRCommunication1.FindControl("txtAdditionalEmailList");
            if (txtCommEmailList != null && txtAdditionalEmailList != null)
            {
                if ((!string.IsNullOrEmpty(txtCommEmailList.Text) && !string.IsNullOrWhiteSpace(txtCommEmailList.Text)) &&
                    (!string.IsNullOrEmpty(txtAdditionalEmailList.Text) && !string.IsNullOrWhiteSpace(txtAdditionalEmailList.Text)))
                {
                    emailIds = string.Format("{0},{1}", txtCommEmailList.Text, txtAdditionalEmailList.Text);
                }
                else if ((!string.IsNullOrEmpty(txtCommEmailList.Text) && !string.IsNullOrWhiteSpace(txtCommEmailList.Text)))
                {
                    emailIds = txtCommEmailList.Text;
                }
                else if ((!string.IsNullOrEmpty(txtAdditionalEmailList.Text) && !string.IsNullOrWhiteSpace(txtAdditionalEmailList.Text)))
                {
                    emailIds = txtAdditionalEmailList.Text;
                }
            }

            discrepancyBE.CommunicationEmails = emailIds;
            discrepancyBE.InternalComments = txtPOCommnets.Text;
            if (ddlCurrencyPO.SelectedIndex > 0)
            {
                discrepancyBE.Currency = new CurrencyBE();
                discrepancyBE.Currency.CurrencyId = Convert.ToInt32(ddlCurrencyPO.SelectedValue);
            }
        }
        else if (rdoDebitLinkedtoVendor.Checked)
        {
            discrepancyBE.DebitRaseType = "V";
            discrepancyBE.PurchaseOrderNumber = string.Empty;
            //discrepancyBE.SiteID = Convert.ToInt32(ucSiteVendor.innerControlddlSite.SelectedValue);            
            discrepancyBE.VendorID = Convert.ToInt32(hdnLocalvendorID.Value);
            var lstVendor = this.GetVendorDetails(Convert.ToInt32(hdnLocalvendorID.Value));
            if (lstVendor != null && lstVendor.Count > 0)
            {
                discrepancyBE.Country = new MAS_CountryBE();
                if (lstVendor[0].CountryID != null)
                    discrepancyBE.Country.CountryID = Convert.ToInt32(lstVendor[0].CountryID);
            }

            discrepancyBE.InvoiceNo = string.Empty;
            discrepancyBE.ReferenceNo = lblReferenceValue_Vendor.Text;
            discrepancyBE.VendorVatReference = txtVendVendorVATReference.Text;

            if (ddlVendOfficeDepotVATReference.SelectedIndex > 0)
                discrepancyBE.ODVatReferenceID = Convert.ToInt32(ddlVendOfficeDepotVATReference.SelectedValue);

            discrepancyBE.DebitTotalValue = this.GetTotalValueDebited();

            var txtCommEmailList = (ucTextbox)ucSDRCommunication2.FindControl("txtVendorEmailList");
            var txtAdditionalEmailList = (ucTextbox)ucSDRCommunication2.FindControl("txtAdditionalEmailList");
            if (txtCommEmailList != null && txtAdditionalEmailList != null)
            {
                if ((!string.IsNullOrEmpty(txtCommEmailList.Text) && !string.IsNullOrWhiteSpace(txtCommEmailList.Text)) &&
                    (!string.IsNullOrEmpty(txtAdditionalEmailList.Text) && !string.IsNullOrWhiteSpace(txtAdditionalEmailList.Text)))
                {
                    emailIds = string.Format("{0},{1}", txtCommEmailList.Text, txtAdditionalEmailList.Text);
                }
                else if ((!string.IsNullOrEmpty(txtCommEmailList.Text) && !string.IsNullOrWhiteSpace(txtCommEmailList.Text)))
                {
                    emailIds = txtCommEmailList.Text;
                }
                else if ((!string.IsNullOrEmpty(txtAdditionalEmailList.Text) && !string.IsNullOrWhiteSpace(txtAdditionalEmailList.Text)))
                {
                    emailIds = txtAdditionalEmailList.Text;
                }
            }

            discrepancyBE.CommunicationEmails = emailIds;
            discrepancyBE.InternalComments = txtVendorComment.Text;
            if (ddlCurrency.SelectedIndex > 0)
            {
                discrepancyBE.Currency = new CurrencyBE();
                discrepancyBE.Currency.CurrencyId = Convert.ToInt32(ddlCurrency.SelectedValue);
            }
        }

        discrepancyBE.Status = "A"; /* A is an Active status of Debit Raise */
        discrepancyBE.UserID = Convert.ToInt32(Session["UserID"]);
        var debitRaiseId = discrepancyBAL.addDebitRaiseBAL(discrepancyBE);
        if (debitRaiseId > 0)
        {
            if (rdoDebitLinkedToPO.Checked)
            {
                #region Existing Grid Item Line data saving ...
                foreach (GridViewRow row in gvLogInvoice.Rows)
                {
                    var txtValueDebited = (ucTextbox)row.FindControl("txtValueDebited");
                    var hdnPriceDifferenceValue = (HiddenField)row.FindControl("hdnPriceDifferenceValue");
                    if ((!string.IsNullOrEmpty(txtValueDebited.Text) && !string.IsNullOrWhiteSpace(txtValueDebited.Text))
                        || (!string.IsNullOrEmpty(hdnPriceDifferenceValue.Value) && !string.IsNullOrWhiteSpace(hdnPriceDifferenceValue.Value)))
                    {
                        var lblLineNo = (ucLabel)row.FindControl("lblLineNo");
                        var lblOfficeDepotCodeValue = (ucLabel)row.FindControl("lblOfficeDepotCodeValue");
                        var lblVikingCodeValue = (ucLabel)row.FindControl("lblVikingCodeValue");
                        var lblVendorItemCodeValue = (ucLabel)row.FindControl("lblVendorItemCodeValue");
                        var lblDescriptionValue = (ucLabel)row.FindControl("lblDescriptionValue");
                        var lblOriginalPOQuantity1 = (ucLabel)row.FindControl("lblOriginalPOQuantity1");
                        var lblOutstandingQty1 = (ucLabel)row.FindControl("lblOutstandingQty1");
                        var lblUoM1 = (ucLabel)row.FindControl("lblUoM1");
                        var lblPOItemCost = (Label)row.FindControl("lblPOItemCost");
                        var lblTotalCost = (ucLabel)row.FindControl("lblTotalCost");
                        var ddlGRDReason = (ucDropdownList)row.FindControl("ddlReason");
                        var txtInvoicedItemPriceValue = (TextBox)row.FindControl("txtInvoicedItemPriceValue");
                        var txtQtyInvoicedValue = (TextBox)row.FindControl("txtQtyInvoicedValue");

                        var discrepancy = new DiscrepancyBE();
                        discrepancy.Action = "AddDebitRaiseItem";
                        discrepancy.DebitRaiseId = debitRaiseId;
                        discrepancy.Line_no = Convert.ToInt32(lblLineNo.Text);
                        discrepancy.ODSKUCode = lblOfficeDepotCodeValue.Text;
                        discrepancy.DirectCode = lblVikingCodeValue.Text;
                        discrepancy.VendorCode = lblVendorItemCodeValue.Text;
                        discrepancy.ProductDescription = lblDescriptionValue.Text;
                        discrepancy.OriginalQuantity = Convert.ToInt32(lblOriginalPOQuantity1.Text);
                        discrepancy.OutstandingQuantity = Convert.ToInt32(lblOutstandingQty1.Text);
                        discrepancy.UOM = lblUoM1.Text;
                        discrepancy.POItemCost = Convert.ToDecimal(lblPOItemCost.Text);
                        discrepancy.POTotalCost = Convert.ToDecimal(lblTotalCost.Text);

                        /* For Price Query Radio Button Option */
                        if (rblPriceOther.SelectedIndex.Equals(0))
                        {
                            discrepancy.InvoicedItemPrice = Convert.ToDecimal(txtInvoicedItemPriceValue.Text);
                            discrepancy.QtyInvoiced = Convert.ToDecimal(txtQtyInvoicedValue.Text);
                            discrepancy.PriceDifference = Convert.ToDecimal(hdnPriceDifferenceValue.Value);
                            discrepancy.StateValueDebited = Convert.ToDecimal(hdnPriceDifferenceValue.Value);
                        }
                        else /* For Other Radio Button Option */
                        {
                            discrepancy.StateValueDebited = Convert.ToDecimal(txtValueDebited.Text);
                            if (ddlGRDReason.SelectedIndex > 0)
                                discrepancy.DebitReasonId = Convert.ToInt32(ddlGRDReason.SelectedValue);
                        }

                        discrepancyBAL.addDebitRaiseItemBAL(discrepancy);
                    }
                }

                #endregion

                #region New Grid Item Line data saving ...
                foreach (GridViewRow row in gvLogInvoiceAddLine.Rows)
                {
                    var txtValueDebited = (ucTextbox)row.FindControl("txtValueDebited");
                    if (!string.IsNullOrEmpty(txtValueDebited.Text) && !string.IsNullOrWhiteSpace(txtValueDebited.Text))
                    {
                        var txtLineNo = (ucTextbox)row.FindControl("txtLineNo");
                        var txtOfficeDepotCode = (ucTextbox)row.FindControl("txtOfficeDepotCode");
                        var txtVikingCode = (ucTextbox)row.FindControl("txtVikingCode");
                        var txtVendorItemCode = (ucTextbox)row.FindControl("txtVendorItemCode");
                        var txtDescription = (ucTextbox)row.FindControl("txtDescription");
                        var txtOriginalPOQuantity = (ucTextbox)row.FindControl("txtOriginalPOQuantity");
                        var txtOutstandingQty = (ucTextbox)row.FindControl("txtOutstandingQty");
                        var txtUoM = (ucTextbox)row.FindControl("txtUoM");
                        var txtPOItemCost = (ucTextbox)row.FindControl("txtPOItemCost");
                        var txtPOTotalCost = (ucTextbox)row.FindControl("txtPOTotalCost");
                        var ddlGRDReason = (ucDropdownList)row.FindControl("ddlReason");

                        var discrepancy = new DiscrepancyBE();
                        discrepancy.Action = "AddDebitRaiseItem";
                        discrepancy.DebitRaiseId = debitRaiseId;
                        discrepancy.Line_no = Convert.ToInt32(txtLineNo.Text);
                        discrepancy.ODSKUCode = txtOfficeDepotCode.Text;
                        discrepancy.DirectCode = txtVikingCode.Text;
                        discrepancy.VendorCode = txtVendorItemCode.Text;
                        discrepancy.ProductDescription = txtDescription.Text;
                        discrepancy.OriginalQuantity = Convert.ToInt32(txtOriginalPOQuantity.Text);
                        discrepancy.OutstandingQuantity = Convert.ToInt32(txtOutstandingQty.Text);
                        discrepancy.UOM = txtUoM.Text;
                        discrepancy.POItemCost = Convert.ToDecimal(txtPOItemCost.Text);
                        discrepancy.POTotalCost = Convert.ToDecimal(txtPOTotalCost.Text);
                        discrepancy.StateValueDebited = Convert.ToDecimal(txtValueDebited.Text);

                        if (ddlGRDReason.SelectedIndex > 0)
                            discrepancy.DebitReasonId = Convert.ToInt32(ddlGRDReason.SelectedValue);

                        discrepancyBAL.addDebitRaiseItemBAL(discrepancy);
                    }
                }
                #endregion
            }
            else if (rdoDebitLinkedtoVendor.Checked)
            {
                var discrepancy = new DiscrepancyBE();
                discrepancy.Action = "AddDebitRaiseItem";
                discrepancy.DebitRaiseId = debitRaiseId;
                discrepancy.StateValueDebited = Convert.ToDecimal(txtValueTobeDebited.Text);
                if (ddlReason.SelectedIndex > 0)
                    discrepancy.DebitReasonId = Convert.ToInt32(ddlReason.SelectedValue);
                discrepancyBAL.addDebitRaiseItemBAL(discrepancy);
            }

            #region Getting here Saved Debit Raise data ...
            if (debitRaiseId > 0)
            {
                discrepancyBE = new DiscrepancyBE();
                discrepancyBE.Action = "GetAllDebitRaise";
                discrepancyBE.DebitRaiseId = debitRaiseId;
                var lstDebitRaised = discrepancyBAL.GetAllDebitRaiseBAL(discrepancyBE);
                if (lstDebitRaised != null && lstDebitRaised.Count > 0)
                {
                    ViewState["DebitNoteNo"] = lstDebitRaised[0].DebitNoteNo;
                    DebitRaisedSuccessfully = DebitRaisedSuccessfully.Replace("##RaisedDebitNumber##", lstDebitRaised[0].DebitNoteNo);
                    DebitNoteAgainstDiscEmailSubject = DebitNoteAgainstDiscEmailSubject.Replace("##DebitNoteNumber##", lstDebitRaised[0].DebitNoteNo);
                    #region Logic to Save & Send email ...

                    #region Getting here Debit Raised Detailed Info ...
                    var lstDebitRaisedDetails = new List<DiscrepancyBE>();
                    if (rdoDebitLinkedToPO.Checked)
                    {
                        discrepancyBE = new DiscrepancyBE();
                        discrepancyBE.Action = "GetAllDebitRaiseDetails";
                        discrepancyBE.DebitRaiseId = debitRaiseId;
                        lstDebitRaisedDetails = discrepancyBAL.GetAllDebitRaiseDetailsBAL(discrepancyBE);
                    }
                    #endregion

                    var oAPPBOK_CommunicationBAL = new APPBOK_CommunicationBAL();
                    var oLanguages = oAPPBOK_CommunicationBAL.GetLanguages();
                    foreach (MAS_LanguageBE objLanguage in oLanguages)
                    {
                        bool MailSentInLanguage = false;
                        if (objLanguage.LanguageID.Equals(lstDebitRaised[0].Vendor.LanguageID))
                            MailSentInLanguage = true;

                        var path = AppDomain.CurrentDomain.BaseDirectory.ToLower();
                        var templateFile = string.Empty;
                        if (rdoDebitLinkedToPO.Checked)
                        {
                            if (rblPriceOther.SelectedIndex.Equals(0))
                                templateFile = string.Format(@"{0}emailtemplates/communication1/DebitNotePOPriceQuery.english.htm", path);
                            else
                                templateFile = string.Format(@"{0}emailtemplates/communication1/DebitNotePOOther.english.htm", path);
                        }
                        else if (rdoDebitLinkedtoVendor.Checked)
                        {
                            templateFile = string.Format(@"{0}emailtemplates/communication1/DebitNoteVendor.english.htm", path);
                        }

                        #region Setting reason as per the language ...
                        switch (objLanguage.LanguageID)
                        {
                            case 1:
                                Page.UICulture = clsConstants.EnglishISO; // "en-US"; // English
                                break;
                            case 2:
                                Page.UICulture = clsConstants.FranceISO; // // France
                                break;
                            case 3:
                                Page.UICulture = clsConstants.GermanyISO; //"de"; // German
                                break;
                            case 4:
                                Page.UICulture = clsConstants.NederlandISO; //"nl"; // Dutch
                                break;
                            case 5:
                                Page.UICulture = clsConstants.SpainISO; //"es"; // Spanish
                                break;
                            case 6:
                                Page.UICulture = clsConstants.ItalyISO; //"it"; // Italy
                                break;
                            case 7:
                                Page.UICulture = clsConstants.CzechISO; //"cz"; // Czech
                                break;
                            default:
                                Page.UICulture = clsConstants.EnglishISO; //"en-US"; // English
                                break;
                        }

                        #endregion

                        #region  Prepairing html body format ...
                        string htmlBody = null;
                        using (StreamReader sReader = new StreamReader(templateFile.ToLower()))
                        {
                            htmlBody = sReader.ReadToEnd();
                            htmlBody = htmlBody.Replace("{VendorNumber}", lstDebitRaised[0].Vendor.Vendor_No);
                            htmlBody = htmlBody.Replace("{DebitNoteNumber}", WebCommon.getGlobalResourceValue("DebitNoteNumber"));
                            htmlBody = htmlBody.Replace("{DebitNoteNumberValue}", lstDebitRaised[0].DebitNoteNo);
                            htmlBody = htmlBody.Replace("{VendorName}", lstDebitRaised[0].Vendor.VendorName);
                            htmlBody = htmlBody.Replace("{OurVATReference}", WebCommon.getGlobalResourceValue("OurVATReference"));

                            if (!string.IsNullOrEmpty(lstDebitRaised[0].OurVATReference) && !string.IsNullOrWhiteSpace(lstDebitRaised[0].OurVATReference))
                                htmlBody = htmlBody.Replace("{OurVATReferenceValue}", lstDebitRaised[0].OurVATReference);
                            else
                                htmlBody = htmlBody.Replace("{OurVATReferenceValue}", string.Empty);

                            htmlBody = htmlBody.Replace("{VendorAddress1}", lstDebitRaised[0].Vendor.address1);
                            htmlBody = htmlBody.Replace("{YourVATReference}", WebCommon.getGlobalResourceValue("YourVATReference"));

                            if (!string.IsNullOrEmpty(lstDebitRaised[0].YourVATReference) && !string.IsNullOrWhiteSpace(lstDebitRaised[0].YourVATReference))
                                htmlBody = htmlBody.Replace("{YourVATReferenceValue}", lstDebitRaised[0].YourVATReference);
                            else
                                htmlBody = htmlBody.Replace("{YourVATReferenceValue}", string.Empty);

                            htmlBody = htmlBody.Replace("{VendorAddress2}", lstDebitRaised[0].Vendor.address2);

                            if (!string.IsNullOrEmpty(lstDebitRaised[0].Vendor.VMPPIN) && !string.IsNullOrWhiteSpace(lstDebitRaised[0].Vendor.VMPPIN))
                                htmlBody = htmlBody.Replace("{VMPPIN}", lstDebitRaised[0].Vendor.VMPPIN);
                            else
                                htmlBody = htmlBody.Replace("{VMPPIN}", string.Empty);

                            if (!string.IsNullOrEmpty(lstDebitRaised[0].Vendor.VMPPOU) && !string.IsNullOrWhiteSpace(lstDebitRaised[0].Vendor.VMPPOU))
                                htmlBody = htmlBody.Replace("{VMPPOU}", lstDebitRaised[0].Vendor.VMPPOU);
                            else
                                htmlBody = htmlBody.Replace("{VMPPOU}", string.Empty);

                            if (!string.IsNullOrEmpty(lstDebitRaised[0].Vendor.city) && !string.IsNullOrWhiteSpace(lstDebitRaised[0].Vendor.city))
                                htmlBody = htmlBody.Replace("{VendorCity}", lstDebitRaised[0].Vendor.city);
                            else
                                htmlBody = htmlBody.Replace("{VendorCity}", string.Empty);

                            htmlBody = htmlBody.Replace("{DearSirMadam}", WebCommon.getGlobalResourceValue("DearSirMadam"));
                            htmlBody = htmlBody.Replace("{Date}", WebCommon.getGlobalResourceValue("Date"));
                            htmlBody = htmlBody.Replace("{DateValue}", DateTime.Now.Date.ToString("dd/MM/yyyy"));

                            if (rdoDebitLinkedToPO.Checked)
                            {
                                if (rblPriceOther.SelectedIndex.Equals(0))
                                {
                                    htmlBody = htmlBody.Replace("{DebitReason}", WebCommon.getGlobalResourceValue("DebitReason"));
                                    htmlBody = htmlBody.Replace("{DebitReasonValue}", WebCommon.getGlobalResourceValue("PriceDifference"));
                                    htmlBody = htmlBody.Replace("{DebitLetterPOPQMessage1}", WebCommon.getGlobalResourceValue("DebitLetterPOPQMessage1"));

                                    if (!string.IsNullOrEmpty(lstDebitRaised[0].PurchaseOrderNumber))
                                        htmlBody = htmlBody.Replace("{PONOValue}", lstDebitRaised[0].PurchaseOrderNumber);
                                    else
                                        htmlBody = htmlBody.Replace("{PONOValue}", string.Empty);

                                    htmlBody = htmlBody.Replace("{DebitLetterPOPQMessage2}", WebCommon.getGlobalResourceValue("DebitLetterPOPQMessage2"));

                                    var poRaisedDate = Convert.ToDateTime(lstDebitRaised[0].PORaisedDate);
                                    htmlBody = htmlBody.Replace("{DebitRaisedDate}", poRaisedDate.ToString("dd/MM/yyyy"));

                                    htmlBody = htmlBody.Replace("{DebitValue}", WebCommon.getGlobalResourceValue("DebitValue"));
                                    if (lstDebitRaised[0].DebitTotalValue != null)
                                        htmlBody = htmlBody.Replace("{DebitActualValue}", lstDebitRaised[0].DebitTotalValue.ToString());
                                    else
                                        htmlBody = htmlBody.Replace("{DebitActualValue}", "0");

                                    htmlBody = htmlBody.Replace("{Currency}", WebCommon.getGlobalResourceValue("Currency"));
                                    if (lstDebitRaised[0].Currency != null)
                                    {
                                        if (!string.IsNullOrEmpty(lstDebitRaised[0].Currency.CurrencyName))
                                            htmlBody = htmlBody.Replace("{CurrencyValue}", lstDebitRaised[0].Currency.CurrencyName);
                                        else { htmlBody = htmlBody.Replace("{CurrencyValue}", string.Empty); }
                                    }
                                    else { htmlBody = htmlBody.Replace("{CurrencyValue}", string.Empty); }

                                    var strLine = WebCommon.getGlobalResourceValue("Line");
                                    var strOfficeDepotCode = WebCommon.getGlobalResourceValue("OfficeDepotCode");
                                    var strVikingCode = WebCommon.getGlobalResourceValue("VikingCode");
                                    var strVendorItemCode = WebCommon.getGlobalResourceValue("VendorItemCode");
                                    var strDescription1 = WebCommon.getGlobalResourceValue("Description1");
                                    var strOriginalPOQuantity = WebCommon.getGlobalResourceValue("OriginalPOQuantity");
                                    var strOutstandingPOQuantity = WebCommon.getGlobalResourceValue("OutstandingPOQuantity");
                                    var strUOM = WebCommon.getGlobalResourceValue("UOM");
                                    var strPOItemCost = WebCommon.getGlobalResourceValue("POItemCost");
                                    var strPOTotalCost = WebCommon.getGlobalResourceValue("POTotalCost");
                                    var strInvoicedItemPrice = WebCommon.getGlobalResourceValue("InvoicedItemPrice");
                                    var strQtyInvoiced = WebCommon.getGlobalResourceValue("QtyInvoiced");
                                    var strPriceDifference = WebCommon.getGlobalResourceValue("PriceDifference");

                                    var sbDebitRaisePOPQ = new StringBuilder();
                                    sbDebitRaisePOPQ.Append("<table style='background: none repeat scroll 0 0 #FFFFFF;border: 1px solid #DADADA;cellspacing=0 cellpadding=0 border=0 border-collapse:collapse;width=100%'>");
                                    sbDebitRaisePOPQ.Append("<tr style='color:#934500;border-bottom: 1px solid #E8E8E8;font-weight: bold;font-family:Arial ;font-size:12;vertical-align: top;'>"
                                        + "<td width='10%'>" + strLine + "</cc1:ucLabel></td>"
                                        + "<td width='10%'>" + strOfficeDepotCode + "</td>"
                                        + "<td width='10%'>" + strVikingCode + "</td>"
                                        + "<td  width='10%'>" + strVendorItemCode + "</td>"
                                        + "<td  width='25%'>" + strDescription1 + "</td>"
                                        + "<td  width='10%'>" + strOriginalPOQuantity + "</td>"
                                        + "<td  width='10%'>" + strOutstandingPOQuantity + "</td>"
                                        + "<td  width='10%'>" + strUOM + "</td>"
                                        + "<td  width='10%'>" + strPOItemCost + "</td>"
                                        + "<td  width='10%'>" + strPOTotalCost + "</td>"
                                        + "<td  width='10%'>" + strInvoicedItemPrice + "</td>"
                                        + "<td  width='10%'>" + strQtyInvoiced + "</td>"
                                        + "<td  width='10%'>" + strPriceDifference + "</td></tr>");

                                    for (int index = 0; index < lstDebitRaisedDetails.Count; index++)
                                    {
                                        sbDebitRaisePOPQ.Append("<tr style='background-color: #F7F6F3;font-size:12;font-family:Arial ;'>"
                                        + "<td width='10%'>" + lstDebitRaisedDetails[index].Line_no + "</cc1:ucLabel></td>"
                                        + "<td width='10%'>" + lstDebitRaisedDetails[index].ODSKUCode + "</td>"
                                        + "<td width='10%'>" + lstDebitRaisedDetails[index].VikingCode + "</td>"
                                        + "<td  width='10%'>" + lstDebitRaisedDetails[index].VendorCode + "</td>"
                                        + "<td  width='25%'>" + lstDebitRaisedDetails[index].ProductDescription + "</td>"
                                        + "<td  width='10%'>" + lstDebitRaisedDetails[index].OriginalQuantity + "</td>"
                                        + "<td  width='10%'>" + lstDebitRaisedDetails[index].OutstandingQuantity + "</td>"
                                        + "<td  width='10%'>" + lstDebitRaisedDetails[index].UOM + "</td>"
                                        + "<td  width='10%'>" + lstDebitRaisedDetails[index].POItemCost + "</td>"
                                        + "<td  width='10%'>" + lstDebitRaisedDetails[index].POTotalCost + "</td>"
                                        + "<td  width='10%'>" + lstDebitRaisedDetails[index].InvoicedItemPrice + "</td>"
                                        + "<td  width='10%'>" + lstDebitRaisedDetails[index].QtyInvoiced + "</td>"
                                        + "<td  width='10%'>" + lstDebitRaisedDetails[index].PriceDifference + "</td></tr>");
                                    }
                                    sbDebitRaisePOPQ.Append("</table>");
                                    htmlBody = htmlBody.Replace("{DebitLetterPOPQGridDetails}", sbDebitRaisePOPQ.ToString());                                   
                                }
                                else
                                {
                                    htmlBody = htmlBody.Replace("{DebitLetterPOOMessage1}", WebCommon.getGlobalResourceValue("DebitLetterPOOMessage1"));

                                    if (!string.IsNullOrEmpty(lstDebitRaised[0].PurchaseOrderNumber))
                                        htmlBody = htmlBody.Replace("{PONOValue}", lstDebitRaised[0].PurchaseOrderNumber);
                                    else
                                        htmlBody = htmlBody.Replace("{PONOValue}", string.Empty);

                                    htmlBody = htmlBody.Replace("{DebitLetterPOOMessage2}", WebCommon.getGlobalResourceValue("DebitLetterPOOMessage2"));

                                    var poRaisedDate = Convert.ToDateTime(lstDebitRaised[0].PORaisedDate);
                                    htmlBody = htmlBody.Replace("{DebitRaisedDate}", poRaisedDate.ToString("dd/MM/yyyy"));
                                    
                                    htmlBody = htmlBody.Replace("{DebitValue}", WebCommon.getGlobalResourceValue("DebitValue"));
                                    if (lstDebitRaised[0].DebitTotalValue != null)
                                        htmlBody = htmlBody.Replace("{DebitActualValue}", lstDebitRaised[0].DebitTotalValue.ToString());
                                    else
                                        htmlBody = htmlBody.Replace("{DebitActualValue}", "0");

                                    htmlBody = htmlBody.Replace("{Currency}", WebCommon.getGlobalResourceValue("Currency"));
                                    if (lstDebitRaised[0].Currency != null)
                                    {
                                        if (!string.IsNullOrEmpty(lstDebitRaised[0].Currency.CurrencyName))
                                            htmlBody = htmlBody.Replace("{CurrencyValue}", lstDebitRaised[0].Currency.CurrencyName);
                                        else { htmlBody = htmlBody.Replace("{CurrencyValue}", string.Empty); }
                                    }
                                    else
                                    { htmlBody = htmlBody.Replace("{CurrencyValue}", string.Empty); }

                                    htmlBody = htmlBody.Replace("{DebitLetterPOOMessage3}", WebCommon.getGlobalResourceValue("DebitLetterPOOMessage3"));

                                    var strLine = WebCommon.getGlobalResourceValue("Line");
                                    var strOfficeDepotCode = WebCommon.getGlobalResourceValue("OfficeDepotCode");
                                    var strVikingCode = WebCommon.getGlobalResourceValue("VikingCode");
                                    var strVendorItemCode = WebCommon.getGlobalResourceValue("VendorItemCode");
                                    var strDescription1 = WebCommon.getGlobalResourceValue("Description1");
                                    var strOriginalPOQuantity = WebCommon.getGlobalResourceValue("OriginalPOQuantity");
                                    var strOutstandingPOQuantity = WebCommon.getGlobalResourceValue("OutstandingPOQuantity");
                                    var strUOM = WebCommon.getGlobalResourceValue("UOM");
                                    var strPOItemCost = WebCommon.getGlobalResourceValue("POItemCost");
                                    var strPOTotalCost = WebCommon.getGlobalResourceValue("POTotalCost");
                                    var strValueDebited = WebCommon.getGlobalResourceValue("ValueDebited");
                                    var strReasonForDebit = WebCommon.getGlobalResourceValue("ReasonForDebit");

                                    var sbDebitRaisePOO = new StringBuilder();
                                    sbDebitRaisePOO.Append("<table style='background: none repeat scroll 0 0 #FFFFFF;border: 1px solid #DADADA;cellspacing=0 cellpadding=0 border=0 border-collapse:collapse;width=100%'>");
                                    sbDebitRaisePOO.Append("<tr style='color:#934500;border-bottom: 1px solid #E8E8E8;font-weight: bold;font-family:Arial ;font-size:12;vertical-align: top;'>"
                                        + "<td width='10%'>" + strLine + "</cc1:ucLabel></td>"
                                        + "<td width='10%'>" + strOfficeDepotCode + "</td>"
                                        + "<td width='10%'>" + strVikingCode + "</td>"
                                        + "<td  width='10%'>" + strVendorItemCode + "</td>"
                                        + "<td  width='25%'>" + strDescription1 + "</td>"
                                        + "<td  width='10%'>" + strOriginalPOQuantity + "</td>"
                                        + "<td  width='10%'>" + strOutstandingPOQuantity + "</td>"
                                        + "<td  width='10%'>" + strUOM + "</td>"
                                        + "<td  width='10%'>" + strPOItemCost + "</td>"
                                        + "<td  width='10%'>" + strPOTotalCost + "</td>"
                                        + "<td  width='10%'>" + strValueDebited + "</td>"
                                        + "<td  width='10%'>" + strReasonForDebit + "</td></tr>");
                                    for (int index = 0; index < lstDebitRaisedDetails.Count; index++)
                                    {
                                        sbDebitRaisePOO.Append("<tr style='background-color: #F7F6F3;font-size:12;font-family:Arial ;'>"
                                        + "<td width='10%'>" + lstDebitRaisedDetails[index].Line_no + "</cc1:ucLabel></td>"
                                        + "<td width='10%'>" + lstDebitRaisedDetails[index].ODSKUCode + "</td>"
                                        + "<td width='10%'>" + lstDebitRaisedDetails[index].VikingCode + "</td>"
                                        + "<td  width='10%'>" + lstDebitRaisedDetails[index].VendorCode + "</td>"
                                        + "<td  width='25%'>" + lstDebitRaisedDetails[index].ProductDescription + "</td>"
                                        + "<td  width='10%'>" + lstDebitRaisedDetails[index].OriginalQuantity + "</td>"
                                        + "<td  width='10%'>" + lstDebitRaisedDetails[index].OutstandingQuantity + "</td>"
                                        + "<td  width='10%'>" + lstDebitRaisedDetails[index].UOM + "</td>"
                                        + "<td  width='10%'>" + lstDebitRaisedDetails[index].POItemCost + "</td>"
                                        + "<td  width='10%'>" + lstDebitRaisedDetails[index].POTotalCost + "</td>"
                                        + "<td  width='10%'>" + lstDebitRaisedDetails[index].StateValueDebited + "</td>"
                                        + "<td  width='10%'>" + lstDebitRaisedDetails[index].Reason + "</td></tr>");
                                    }
                                    sbDebitRaisePOO.Append("</table>");
                                    htmlBody = htmlBody.Replace("{DebitLetterPOOGridDetails}", sbDebitRaisePOO.ToString());
                                }
                            }
                            else if (rdoDebitLinkedtoVendor.Checked)
                            {
                                htmlBody = htmlBody.Replace("{DebitReason}", WebCommon.getGlobalResourceValue("DebitReason"));
                                htmlBody = htmlBody.Replace("{DebitReasonValue}", lstDebitRaised[0].Reason);
                                htmlBody = htmlBody.Replace("{DebitLetterVendorMessage1}", WebCommon.getGlobalResourceValue("DebitLetterVendorMessage1"));
                                if (lstDebitRaised[0].DebitTotalValue != null)
                                    htmlBody = htmlBody.Replace("{DebitActualValue}", lstDebitRaised[0].DebitTotalValue.ToString());

                                htmlBody = htmlBody.Replace("{DebitLetterVendorMessage2}", WebCommon.getGlobalResourceValue("DebitLetterVendorMessage2"));
                                htmlBody = htmlBody.Replace("{DebitLetterVendorComments}", lstDebitRaised[0].Comment);
                                htmlBody = htmlBody.Replace("{DebitLetterVendorMessage3}", WebCommon.getGlobalResourceValue("DebitLetterVendorMessage3"));
                            }

                            htmlBody = htmlBody.Replace("{Yoursfaithfully}", WebCommon.getGlobalResourceValue("Yoursfaithfully"));
                            htmlBody = htmlBody.Replace("{APClerkName}", lstDebitRaised[0].CreatedBy);

                            if (!string.IsNullOrEmpty(lstDebitRaised[0].CreatedByContactNumber) && !string.IsNullOrWhiteSpace(lstDebitRaised[0].CreatedByContactNumber))
                                htmlBody = htmlBody.Replace("{APClerkContactNo}", lstDebitRaised[0].CreatedByContactNumber);
                            else
                                htmlBody = htmlBody.Replace("{APClerkContactNo}", string.Empty);

                            var apAddress = string.Empty;
                            if (!string.IsNullOrEmpty(lstDebitRaised[0].APAddress1) && !string.IsNullOrEmpty(lstDebitRaised[0].APAddress2)
                                && !string.IsNullOrEmpty(lstDebitRaised[0].APAddress3) && !string.IsNullOrEmpty(lstDebitRaised[0].APAddress4)
                                && !string.IsNullOrEmpty(lstDebitRaised[0].APAddress5) && !string.IsNullOrEmpty(lstDebitRaised[0].APAddress6))
                            {
                                apAddress = string.Format("{0},&nbsp;{1},&nbsp;{2},&nbsp;{3},&nbsp;{4},&nbsp;{5}", lstDebitRaised[0].APAddress1, lstDebitRaised[0].APAddress2,
                                    lstDebitRaised[0].APAddress3, lstDebitRaised[0].APAddress4, lstDebitRaised[0].APAddress5, lstDebitRaised[0].APAddress6);
                            }
                            else if (!string.IsNullOrEmpty(lstDebitRaised[0].APAddress1) && !string.IsNullOrEmpty(lstDebitRaised[0].APAddress2)
                                && !string.IsNullOrEmpty(lstDebitRaised[0].APAddress3) && !string.IsNullOrEmpty(lstDebitRaised[0].APAddress4)
                                && !string.IsNullOrEmpty(lstDebitRaised[0].APAddress5))
                            {
                                apAddress = string.Format("{0},&nbsp;{1},&nbsp;{2},&nbsp;{3},&nbsp;{4}", lstDebitRaised[0].APAddress1, lstDebitRaised[0].APAddress2,
                                    lstDebitRaised[0].APAddress3, lstDebitRaised[0].APAddress4, lstDebitRaised[0].APAddress5);
                            }
                            else if (!string.IsNullOrEmpty(lstDebitRaised[0].APAddress1) && !string.IsNullOrEmpty(lstDebitRaised[0].APAddress2)
                               && !string.IsNullOrEmpty(lstDebitRaised[0].APAddress3) && !string.IsNullOrEmpty(lstDebitRaised[0].APAddress4))
                            {
                                apAddress = string.Format("{0},&nbsp;{1},&nbsp;{2},&nbsp;{3}", lstDebitRaised[0].APAddress1, lstDebitRaised[0].APAddress2,
                                    lstDebitRaised[0].APAddress3, lstDebitRaised[0].APAddress4);
                            }
                            else if (!string.IsNullOrEmpty(lstDebitRaised[0].APAddress1) && !string.IsNullOrEmpty(lstDebitRaised[0].APAddress2)
                               && !string.IsNullOrEmpty(lstDebitRaised[0].APAddress3))
                            {
                                apAddress = string.Format("{0},&nbsp;{1},&nbsp;{2}", lstDebitRaised[0].APAddress1, lstDebitRaised[0].APAddress2,
                                    lstDebitRaised[0].APAddress3);
                            }
                            else if (!string.IsNullOrEmpty(lstDebitRaised[0].APAddress1) && !string.IsNullOrEmpty(lstDebitRaised[0].APAddress2))
                            {
                                apAddress = string.Format("{0},&nbsp;{1}", lstDebitRaised[0].APAddress1, lstDebitRaised[0].APAddress2);
                            }
                            else
                            {
                                apAddress = lstDebitRaised[0].APAddress1;
                            }

                            if (!string.IsNullOrEmpty(lstDebitRaised[0].APPincode))
                                apAddress = string.Format("{0},&nbsp;{1}", apAddress, lstDebitRaised[0].APPincode);

                            htmlBody = htmlBody.Replace("{APClerkAddress}", apAddress);

                        }
                        #endregion

                        #region Sending and saving email details ...
                        string[] sMailAddress = emailIds.Split(',');
                        var sentToWithLink = new System.Text.StringBuilder();
                        for (int index = 0; index < sMailAddress.Length; index++)
                            sentToWithLink.Append("<a target='_blank' href='" + Common.EncryptQuery("../LogDiscrepancy/DISLog_ReSendCommunication.aspx?CommTitle=DebitRaised&DebitRaiseId=" + debitRaiseId + "&CommunicationType=DebitRaised") + "' onclick='window.open(this.href, this.target,\"scrollbars=1,left=100px,top=100px,width=1000px,height=800px\"); return false;'>" + sMailAddress[index] + "</a>" + System.Environment.NewLine);

                        var discrepancyMailBE = new DiscrepancyMailBE();
                        discrepancyMailBE.Action = "AddDebitRaiseCommunication";
                        discrepancyMailBE.DebitRaiseId = debitRaiseId;
                        discrepancyMailBE.mailSubject = DebitNoteAgainstDiscEmailSubject;
                        discrepancyMailBE.sentTo = emailIds;
                        discrepancyMailBE.mailBody = htmlBody;
                        discrepancyMailBE.SendByID = Convert.ToInt32(Session["UserID"]);
                        discrepancyMailBE.IsMailSent = MailSentInLanguage;
                        discrepancyMailBE.languageID = objLanguage.LanguageID;
                        discrepancyMailBE.MailSentInLanguage = MailSentInLanguage;
                        discrepancyMailBE.CommTitle = "DebitRaised";
                        discrepancyMailBE.SentToWithLink = sentToWithLink.ToString();
                        var debitRaiseCommId = discrepancyBAL.addDebitRaiseCommunicationBAL(discrepancyMailBE);
                        if (MailSentInLanguage)
                        {   
                            var emailToAddress = emailIds;
                            var emailFromAddress = Convert.ToString(ConfigurationManager.AppSettings["fromAddress"]);
                            var emailToSubject = DebitNoteAgainstDiscEmailSubject;
                            var emailBody = htmlBody;
                            Utilities.clsEmail oclsEmail = new Utilities.clsEmail();
                            oclsEmail.sendMail(emailToAddress, emailBody, emailToSubject, emailFromAddress, true);
                        }
                        #endregion
                    }
                    #endregion
                    Page.UICulture = Convert.ToString(Session["CultureInfo"]);
                }
            }
            #endregion
        }

        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + DebitRaisedSuccessfully + "')", true);
        mvDebitRaiseType.ActiveViewIndex = 0;
        lblRaiseaDebit.Visible = true;
        //lblRaiseaDebitAgainstPo.Visible = false;
        lblPurchaseOrderDebitPriceDifference.Visible = false;
        lblPurchaseOrderDebitReasonOther.Visible = false;
        lblRaiseaDebitAgainstVendor.Visible = false;
        btnBack.Visible = false;
        btnProceed.Visible = true;
        btnConfirm.Visible = false;
        btnSave.Visible = false;
        this.Clear();
        #endregion
    }

    protected void ddlPurchaseOrderDate_SelectedIndexChanged(object sender, EventArgs e)
    {
    }

    protected void gvLogInvoice_RowDataBound(Object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            if (rblPriceOther.SelectedIndex.Equals(0))
            {
                var txtQtyInvoicedValue = (TextBox)e.Row.FindControl("txtQtyInvoicedValue"); // Invoice Qty
                var lblPOItemCost = (Label)e.Row.FindControl("lblPOItemCost"); // PO Item cost
                var txtInvoicedItemPriceValue = (TextBox)e.Row.FindControl("txtInvoicedItemPriceValue"); // Invoice Item Price
                var lblPriceDifferenceValue = (Label)e.Row.FindControl("lblPriceDifferenceValue"); // Price Difference
                var hdnPriceDifferenceValue = (HiddenField)e.Row.FindControl("hdnPriceDifferenceValue"); // Price Difference

                txtQtyInvoicedValue.Attributes.Add("onblur", "SetPOPriceDiff('" + lblPOItemCost.ClientID + "','" + txtQtyInvoicedValue.ClientID + "','" + txtInvoicedItemPriceValue.ClientID + "','" + lblPriceDifferenceValue.ClientID + "','" + hdnPriceDifferenceValue.ClientID + "','" + txtDebitValueText.ClientID + "')");
                txtInvoicedItemPriceValue.Attributes.Add("onblur", "SetPOPriceDiff('" + lblPOItemCost.ClientID + "','" + txtQtyInvoicedValue.ClientID + "','" + txtInvoicedItemPriceValue.ClientID + "','" + lblPriceDifferenceValue.ClientID + "','" + hdnPriceDifferenceValue.ClientID + "','" + txtDebitValueText.ClientID + "')");
            }
            else if (rblPriceOther.SelectedIndex.Equals(1))
            {
                var ddlReason = (ucDropdownList)e.Row.FindControl("ddlReason");
                if (ddlReason != null && ViewState["DebitReason"] != null)
                {
                    var lstAPAction = (List<APActionBE>)ViewState["DebitReason"];
                    if (lstAPAction != null && lstAPAction.Count > 0)
                        FillControls.FillDropDown(ref ddlReason, lstAPAction, "Reason", "ReasonTypeID", "Select");
                }
            }
        }
    }

    public static string InvalidValuesEnteredLocal = WebCommon.getGlobalResourceValue("InvalidValuesEntered");

    [WebMethod]
    public static string CalculatePOPriceDiff(string InvoiceQty, string POItemCost, string InvoiceItemPrice)
    {
        int invoiceQty;
        decimal invoiceItemPrice;
        bool isInvoiceQtyDecimal = int.TryParse(InvoiceQty.Trim(), out invoiceQty);
        bool isInvoiceItemPriceDecimal = decimal.TryParse(InvoiceItemPrice.Trim(), out invoiceItemPrice);

        if (InvoiceQty.Trim() != string.Empty && !isInvoiceQtyDecimal)
            return InvalidValuesEnteredLocal;

        if (InvoiceItemPrice.Trim() != string.Empty && !isInvoiceItemPriceDecimal)
            return InvalidValuesEnteredLocal;

        if (InvoiceQty.Trim() != string.Empty && isInvoiceQtyDecimal && !InvoiceQty.IsNumeric())
            return InvalidValuesEnteredLocal;

        if (InvoiceItemPrice.Trim() != string.Empty && isInvoiceItemPriceDecimal && !InvoiceItemPrice.IsDecimal())
            return InvalidValuesEnteredLocal;

        //if (InvoiceQty.Equals("0") && InvoiceItemPrice.Equals("0"))
        //    return "0";

        if (InvoiceQty.Trim().Length > 0 && POItemCost.Trim().Length > 0 && InvoiceItemPrice.Trim().Length > 0)
        {
            return Convert.ToString(((Convert.ToDecimal(InvoiceQty) * (Convert.ToDecimal(POItemCost))) -
                (Convert.ToDecimal(InvoiceQty) * (Convert.ToDecimal(InvoiceItemPrice)))));
        }

        return string.Empty;
    }

    protected void gvLogInvoiceAddLine_RowDataBound(Object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            var ddlReason = (ucDropdownList)e.Row.FindControl("ddlReason");
            var txtOfficeDepotCode = (ucTextbox)e.Row.FindControl("txtOfficeDepotCode");
            if (ddlReason != null && ViewState["DebitReason"] != null)
            {
                var lstAPAction = (List<APActionBE>)ViewState["DebitReason"];
                if (lstAPAction != null && lstAPAction.Count > 0)
                    FillControls.FillDropDown(ref ddlReason, lstAPAction, "Reason", "ReasonTypeID", "Select");

                var dicDebitReason = (Dictionary<int, string>)ViewState["GVDebitReason"];
                if (dicDebitReason != null)
                {
                    int rowIndex = e.Row.RowIndex;
                    if (dicDebitReason.ContainsKey(rowIndex))
                    {
                        if (!string.IsNullOrEmpty(txtOfficeDepotCode.Text))
                        {
                            var debitReasonValue = dicDebitReason[rowIndex];
                            ddlReason.SelectedValue = debitReasonValue;
                        }
                    }
                }
            }
        }
    }

    protected void txtPurchaseOrder_TextChanged(object sender, EventArgs e)
    {
        this.GetPurchaseOrderList();
        btnConfirm.Visible = true;
    }

    protected void TextBox_TextChanged_txtLineNo(object sender, EventArgs e)
    {
        mvDebitRaiseType.ActiveViewIndex = 3;

        //btnBack.Visible = true;
        btnSave.Visible = true;

        currentTextBox = (BaseControlLibrary.ucTextbox)sender;
        GridViewRow currentGridViewRow = (GridViewRow)currentTextBox.Parent.Parent;
        int ind = currentGridViewRow.RowIndex;

        if (!string.IsNullOrEmpty(currentTextBox.Text) || !string.IsNullOrWhiteSpace(currentTextBox.Text))
        {
            DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
            DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();
            oDiscrepancyBE.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();
            oDiscrepancyBE.Action = "GetContactPurchaseOrderDetails";
            oDiscrepancyBE.PurchaseOrderNumber = txtPurchaseOrder.Text.Trim();
            oDiscrepancyBE.PurchaseOrder = new BusinessEntities.ModuleBE.Upload.Up_PurchaseOrderDetailBE();
            oDiscrepancyBE.PurchaseOrder.Order_raised = txtPurchaseOrder.Text != "" ? Common.GetMM_DD_YYYY(ddlPurchaseOrderDate.SelectedItem.Text) : Common.GetMM_DD_YYYY("15/08/2013");
            oDiscrepancyBE.SiteID = Convert.ToInt32(ucSite.innerControlddlSite.SelectedItem.Value);

            List<DiscrepancyBE> lstDetails = oDiscrepancyBAL.GetContactPurchaseOrderDetailsBAL(oDiscrepancyBE);
            if (lstDetails.Count > 0 && lstDetails != null)
            {
                var LineNO = lstDetails.Find(dbe => dbe.PurchaseOrder.Line_No.ToString() == currentTextBox.Text);

                if (lstDetails[0].Vendor.NewCurrencyID != null)
                {
                   // ddlCurrency.SelectedIndex =Convert.ToInt32(lstDetails[0].Vendor.NewCurrencyID);
                    ddlCurrencyPO.SelectedValue = Convert.ToString(lstDetails[0].Vendor.NewCurrencyID);
                }
                if (LineNO != null)
                {
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + AlreadyExistLineNumber + "')", true);
                    currentTextBox.Text = string.Empty;
                    return;
                }
            }

            // check that line no is already used in previous rows.
            for (int item = 0; item < gvLogInvoiceAddLine.Rows.Count - 1; item++)
            {
                ucTextbox txtLineNo = (ucTextbox)gvLogInvoiceAddLine.Rows[item].FindControl("txtLineNo");
                HiddenField hdnRecordID = (HiddenField)gvLogInvoiceAddLine.Rows[item].FindControl("hdnRecordID");
                if (ind > 0) // so that it checks from 2 row.
                {
                    if (currentTextBox.Text == txtLineNo.Text)
                    {
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + AlreadyExistLineNumber + "')", true);
                        currentTextBox.Text = string.Empty;
                        return;
                    }
                }
            }
        }
    }

    protected void TextBox_TextChanged_txtOfficeDepotCode(object sender, EventArgs e)
    {
        //mvDiscrepancyInvoiceCriteria.ActiveViewIndex = 1;
        //btnLog.Visible = false;
        //btnBack.Visible = true;
        btnSave.Visible = true;

        currentTextBox = (BaseControlLibrary.ucTextbox)sender;
        var currentGridViewRow = (GridViewRow)currentTextBox.Parent.Parent;
        int ind = currentGridViewRow.RowIndex;
        var row = gvLogInvoiceAddLine.Rows[ind];
        var txtLineNo = (ucTextbox)row.FindControl("txtLineNo");
        var txtOfficeDepotCode = (ucTextbox)row.FindControl("txtOfficeDepotCode");
        var txtVikingCode = (ucTextbox)row.FindControl("txtVikingCode");
        var txtVendorItemCode = (ucTextbox)row.FindControl("txtVendorItemCode");
        var txtDescription = (ucTextbox)row.FindControl("txtDescription");
        var txtOriginalPOQuantity = (ucTextbox)row.FindControl("txtOriginalPOQuantity");
        var txtOutstandingQty = (ucTextbox)row.FindControl("txtOutstandingQty");
        var txtUoM = (ucTextbox)row.FindControl("txtUoM");
        var txtPOItemCost = (ucTextbox)row.FindControl("txtPOItemCost");
        var txtPOTotalCost = (ucTextbox)row.FindControl("txtPOTotalCost");
        var txtValueDebited = (ucTextbox)row.FindControl("txtValueDebited");
        var ddlReason = (ucDropdownList)row.FindControl("ddlReason");
        var btnAddLine = (ucButton)row.FindControl("btnAddLine");
        var btnRemove = (ucButton)row.FindControl("btnRemove");

        /* Validation if product code not exist then resetting the grid child controls. */
        if (string.IsNullOrEmpty(currentTextBox.Text) || string.IsNullOrWhiteSpace(currentTextBox.Text))
        {
            txtLineNo.Text = string.Empty;
            txtOfficeDepotCode.Text = string.Empty;
            txtVikingCode.Text = string.Empty;
            txtVendorItemCode.Text = string.Empty;
            txtDescription.Text = string.Empty;
            txtOriginalPOQuantity.Text = string.Empty;
            txtOutstandingQty.Text = string.Empty;
            txtUoM.Text = string.Empty;
            txtPOItemCost.Text = string.Empty;
            txtPOTotalCost.Text = string.Empty;
            txtValueDebited.Text = string.Empty;
            if (ddlReason.Items.Count > 0)
                ddlReason.SelectedIndex = 0;

            txtLineNo.Enabled = false;
            txtOfficeDepotCode.Enabled = true;
            txtVikingCode.Enabled = false;
            txtVendorItemCode.Enabled = false;
            txtDescription.Enabled = false;
            txtOriginalPOQuantity.Enabled = false;
            txtOutstandingQty.Enabled = false;
            txtUoM.Enabled = false;
            txtPOItemCost.Enabled = false;
            txtPOTotalCost.Enabled = false;
            txtValueDebited.Enabled = false;
            ddlReason.Enabled = false;
            btnRemove.Style["display"] = string.Empty;
            return;
        }

        var oDiscrepancyBE = new DiscrepancyBE();
        var oDiscrepancyBAL = new DiscrepancyBAL();
        oDiscrepancyBE.Action = "GetODProductBasedSiteVendorIds";
        oDiscrepancyBE.ODSKUCode = currentTextBox.Text;
        oDiscrepancyBE.SiteID = !string.IsNullOrWhiteSpace(ucSite.innerControlddlSite.SelectedValue) ? Convert.ToInt32(ucSite.innerControlddlSite.SelectedValue) : 0;
        oDiscrepancyBE.VendorID = ViewState["VendorID"] != null ? Convert.ToInt32(ViewState["VendorID"].ToString()) : (int?)null;

        List<DiscrepancyBE> lstODProductDetails = oDiscrepancyBAL.GetODProductDetailsBySiteVendorBAL(oDiscrepancyBE);
        if (lstODProductDetails == null || (lstODProductDetails != null && lstODProductDetails.Count == 0))
        {
            mdlInvoiceViewer.Show();
        }
        else
        {
            //txtLineNo.Text = string.Empty;
            //txtOfficeDepotCode.Text = string.Empty;
            txtVikingCode.Text = lstODProductDetails[0].DirectCode;
            txtVendorItemCode.Text = lstODProductDetails[0].VendorCode;
            txtDescription.Text = lstODProductDetails[0].ProductDescription;
            txtOriginalPOQuantity.Text = string.Empty;
            txtOutstandingQty.Text = string.Empty;
            txtUoM.Text = lstODProductDetails[0].UOM;
            txtPOItemCost.Text = string.Empty;
            txtPOTotalCost.Text = string.Empty;
            txtValueDebited.Text = string.Empty;
            if (ddlReason.Items.Count > 0)
                ddlReason.SelectedIndex = 0;

            txtLineNo.Enabled = true;
            txtOfficeDepotCode.Enabled = false;
            txtVikingCode.Enabled = false;
            txtVendorItemCode.Enabled = false;
            txtDescription.Enabled = false;
            txtUoM.Enabled = false;
            btnAddLine.Style["display"] = "block";
            btnRemove.Style["display"] = "none";
        }
    }

    protected void TextBox_TextChanged_txtOriginalPOQuantity(object sender, EventArgs e)
    {
        currentTextBox = (BaseControlLibrary.ucTextbox)sender;
        var currentGridViewRow = (GridViewRow)currentTextBox.Parent.Parent;
        int ind = currentGridViewRow.RowIndex;
        var row = gvLogInvoiceAddLine.Rows[ind];
        var txtOriginalPOQuantity = (ucTextbox)row.FindControl("txtOriginalPOQuantity");
        var txtPOItemCost = (ucTextbox)row.FindControl("txtPOItemCost");
        var txtPOTotalCost = (ucTextbox)row.FindControl("txtPOTotalCost");
        var txtOutstandingQty = (ucTextbox)row.FindControl("txtOutstandingQty");

        if ((!string.IsNullOrEmpty(txtOriginalPOQuantity.Text) && !string.IsNullOrWhiteSpace(txtOriginalPOQuantity.Text)) &&
            (!string.IsNullOrEmpty(txtPOItemCost.Text) && !string.IsNullOrWhiteSpace(txtPOItemCost.Text)))
        {
            txtPOTotalCost.Text = Convert.ToString(Math.Round(Convert.ToDecimal(txtOriginalPOQuantity.Text) * Convert.ToDecimal(txtPOItemCost.Text), 2));
            txtOutstandingQty.Focus();
        }
    }

    protected void TextBox_TextChanged_txtPOItemCost(object sender, EventArgs e)
    {
        currentTextBox = (BaseControlLibrary.ucTextbox)sender;
        var currentGridViewRow = (GridViewRow)currentTextBox.Parent.Parent;
        int ind = currentGridViewRow.RowIndex;
        var row = gvLogInvoiceAddLine.Rows[ind];
        var txtOriginalPOQuantity = (ucTextbox)row.FindControl("txtOriginalPOQuantity");
        var txtPOItemCost = (ucTextbox)row.FindControl("txtPOItemCost");
        var txtPOTotalCost = (ucTextbox)row.FindControl("txtPOTotalCost");
        var txtValueDebited = (ucTextbox)row.FindControl("txtValueDebited");

        if ((!string.IsNullOrEmpty(txtOriginalPOQuantity.Text) && !string.IsNullOrWhiteSpace(txtOriginalPOQuantity.Text)) &&
            (!string.IsNullOrEmpty(txtPOItemCost.Text) && !string.IsNullOrWhiteSpace(txtPOItemCost.Text)))
        {
            txtPOTotalCost.Text = Convert.ToString(Math.Round(Convert.ToDecimal(txtOriginalPOQuantity.Text) * Convert.ToDecimal(txtPOItemCost.Text), 2));
            txtValueDebited.Focus();
        }
    }

    protected void btnOk_Click(object sender, EventArgs e)
    {
        //mvDiscrepancyInvoiceCriteria.ActiveViewIndex = 1;
        //btnLog.Visible = false;
        //btnBack.Visible = true;
        btnSave.Visible = true;

        GridViewRow currentGridViewRow = (GridViewRow)currentTextBox.Parent.Parent;
        int ind = currentGridViewRow.RowIndex;
        GridViewRow row = gvLogInvoiceAddLine.Rows[ind];
        var txtLineNo = (ucTextbox)row.FindControl("txtLineNo");
        var txtOfficeDepotCode = (ucTextbox)row.FindControl("txtOfficeDepotCode");
        var txtVikingCode = (ucTextbox)row.FindControl("txtVikingCode");
        var txtVendorItemCode = (ucTextbox)row.FindControl("txtVendorItemCode");
        var txtDescription = (ucTextbox)row.FindControl("txtDescription");
        var txtOriginalPOQuantity = (ucTextbox)row.FindControl("txtOriginalPOQuantity");
        var txtOutstandingQty = (ucTextbox)row.FindControl("txtOutstandingQty");
        var txtUoM = (ucTextbox)row.FindControl("txtUoM");
        var txtPOItemCost = (ucTextbox)row.FindControl("txtPOItemCost");
        var txtPOTotalCost = (ucTextbox)row.FindControl("txtPOTotalCost");
        var txtValueDebited = (ucTextbox)row.FindControl("txtValueDebited");
        var ddlReason = (ucDropdownList)row.FindControl("ddlReason");
        var btnAddLine = (ucButton)row.FindControl("btnAddLine");
        var btnRemove = (ucButton)row.FindControl("btnRemove");

        //txtLineNo.Text = string.Empty;
        //txtOfficeDepotCode.Text = string.Empty;
        txtVikingCode.Text = string.Empty;
        txtVendorItemCode.Text = string.Empty;
        txtDescription.Text = string.Empty;
        txtOriginalPOQuantity.Text = string.Empty;
        txtOutstandingQty.Text = string.Empty;
        txtUoM.Text = string.Empty;
        txtPOItemCost.Text = string.Empty;
        txtPOTotalCost.Text = string.Empty;
        txtValueDebited.Text = string.Empty;
        if (ddlReason.Items.Count > 0)
            ddlReason.SelectedIndex = 0;

        txtLineNo.Enabled = true;
        txtOfficeDepotCode.Enabled = true;
        txtVikingCode.Enabled = true;
        txtVendorItemCode.Enabled = true;
        txtDescription.Enabled = true;
        txtOriginalPOQuantity.Enabled = true;
        txtOutstandingQty.Enabled = true;
        txtUoM.Enabled = true;
        txtPOItemCost.Enabled = true;
        txtPOTotalCost.Enabled = true;
        txtValueDebited.Enabled = true;
        ddlReason.Enabled = true;
        btnAddLine.Style["display"] = "block";
        btnRemove.Style["display"] = "none";
        currentTextBox = null;
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        //mvDiscrepancyInvoiceCriteria.ActiveViewIndex = 1;
        //btnLog.Visible = false;
        //btnBack.Visible = true;
        btnSave.Visible = true;

        GridViewRow currentGridViewRow = (GridViewRow)currentTextBox.Parent.Parent;
        int ind = currentGridViewRow.RowIndex;
        GridViewRow row = gvLogInvoiceAddLine.Rows[ind];
        var txtLineNo = (ucTextbox)row.FindControl("txtLineNo");
        var txtOfficeDepotCode = (ucTextbox)row.FindControl("txtOfficeDepotCode");
        var txtVikingCode = (ucTextbox)row.FindControl("txtVikingCode");
        var txtVendorItemCode = (ucTextbox)row.FindControl("txtVendorItemCode");
        var txtDescription = (ucTextbox)row.FindControl("txtDescription");
        var txtOriginalPOQuantity = (ucTextbox)row.FindControl("txtOriginalPOQuantity");
        var txtOutstandingQty = (ucTextbox)row.FindControl("txtOutstandingQty");
        var txtUoM = (ucTextbox)row.FindControl("txtUoM");
        var txtPOItemCost = (ucTextbox)row.FindControl("txtPOItemCost");
        var txtPOTotalCost = (ucTextbox)row.FindControl("txtPOTotalCost");
        var txtValueDebited = (ucTextbox)row.FindControl("txtValueDebited");
        var ddlReason = (ucDropdownList)row.FindControl("ddlReason");
        var btnAddLine = (ucButton)row.FindControl("btnAddLine");
        var btnRemove = (ucButton)row.FindControl("btnRemove");

        //txtLineNo.Text = string.Empty;
        txtOfficeDepotCode.Text = string.Empty;
        txtVikingCode.Text = string.Empty;
        txtVendorItemCode.Text = string.Empty;
        txtDescription.Text = string.Empty;
        txtOriginalPOQuantity.Text = string.Empty;
        txtOutstandingQty.Text = string.Empty;
        txtUoM.Text = string.Empty;
        txtPOItemCost.Text = string.Empty;
        txtPOTotalCost.Text = string.Empty;
        txtValueDebited.Text = string.Empty;
        if (ddlReason.Items.Count > 0)
            ddlReason.SelectedIndex = 0;

        txtLineNo.Enabled = true;
        txtOfficeDepotCode.Enabled = true;
        txtVikingCode.Enabled = false;
        txtVendorItemCode.Enabled = false;
        txtDescription.Enabled = false;
        txtOriginalPOQuantity.Enabled = false;
        txtOutstandingQty.Enabled = false;
        txtUoM.Enabled = false;
        txtPOItemCost.Enabled = false;
        txtPOTotalCost.Enabled = false;
        txtValueDebited.Enabled = false;
        ddlReason.Enabled = false;
        btnAddLine.Style["display"] = "block";
        btnRemove.Style["display"] = "none";
        currentTextBox = null;
    }

    protected void AddLine_Click(object sender, CommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "AddLine" && e.CommandArgument != null)
            {
                ucButton btn = (BaseControlLibrary.ucButton)sender;
                GridViewRow currentGridViewRow = (GridViewRow)btn.Parent.Parent;
                int ind = currentGridViewRow.RowIndex;
                GridViewRow row = gvLogInvoiceAddLine.Rows[ind];
                var txtLineNo = (ucTextbox)row.FindControl("txtLineNo");
                var txtOfficeDepotCode = (ucTextbox)row.FindControl("txtOfficeDepotCode");
                var txtVikingCode = (ucTextbox)row.FindControl("txtVikingCode");
                var txtVendorItemCode = (ucTextbox)row.FindControl("txtVendorItemCode");
                var txtDescription = (ucTextbox)row.FindControl("txtDescription");
                var txtOriginalPOQuantity = (ucTextbox)row.FindControl("txtOriginalPOQuantity");
                var txtOutstandingQty = (ucTextbox)row.FindControl("txtOutstandingQty");
                var txtUoM = (ucTextbox)row.FindControl("txtUoM");
                var txtPOItemCost = (ucTextbox)row.FindControl("txtPOItemCost");
                var txtPOTotalCost = (ucTextbox)row.FindControl("txtPOTotalCost");
                var txtValueDebited = (ucTextbox)row.FindControl("txtValueDebited");
                var ddlReason = (ucDropdownList)row.FindControl("ddlReason");
                var btnAddLine = (ucButton)row.FindControl("btnAddLine");
                var btnRemove = (ucButton)row.FindControl("btnRemove");

                if (!string.IsNullOrEmpty(txtOfficeDepotCode.Text) && !string.IsNullOrWhiteSpace(txtOfficeDepotCode.Text))
                {
                    if (string.IsNullOrEmpty(txtLineNo.Text) && string.IsNullOrWhiteSpace(txtLineNo.Text))
                    {
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + LineNoReq + "')", true);
                        txtLineNo.Focus();
                        return;
                    }

                    if (string.IsNullOrEmpty(txtDescription.Text) && string.IsNullOrWhiteSpace(txtDescription.Text))
                    {
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + ProductDescription + "')", true);
                        txtDescription.Focus();
                        return;
                    }

                    if (string.IsNullOrEmpty(txtOriginalPOQuantity.Text) && string.IsNullOrWhiteSpace(txtOriginalPOQuantity.Text))
                    {
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + OriginalPOQuantityMandatry + "')", true);
                        txtOriginalPOQuantity.Focus();
                        return;
                    }

                    if (string.IsNullOrEmpty(txtOutstandingQty.Text) && string.IsNullOrWhiteSpace(txtOutstandingQty.Text))
                    {
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + OutstandingPOQuantityMandatry + "')", true);
                        txtOutstandingQty.Focus();
                        return;
                    }

                    if (string.IsNullOrEmpty(txtPOItemCost.Text) && string.IsNullOrWhiteSpace(txtPOItemCost.Text))
                    {
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + POItemCostMandatry + "')", true);
                        txtPOItemCost.Focus();
                        return;
                    }

                    if (string.IsNullOrEmpty(txtValueDebited.Text) && string.IsNullOrWhiteSpace(txtValueDebited.Text))
                    {
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + ValuetobeDebitedMandatry + "')", true);
                        txtValueDebited.Focus();
                        return;
                    }

                    if (ddlReason.SelectedIndex.Equals(0))
                    {
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + ReasonForDebitMandatry + "')", true);
                        ddlReason.Focus();
                        return;
                    }

                    if (ViewState["GVDebitReason"] == null)
                    {
                        var dicDebitReason = new Dictionary<int, string>();
                        dicDebitReason.Add(ind, ddlReason.SelectedValue);
                        ViewState["GVDebitReason"] = dicDebitReason;
                    }
                    else
                    {
                        var dicDebitReason = (Dictionary<int, string>)ViewState["GVDebitReason"];
                        dicDebitReason.Add(ind, ddlReason.SelectedValue);
                        ViewState["GVDebitReason"] = dicDebitReason;
                    }

                    btnAdd_Click(null, null);
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + AtLeastOne + "')", true);
                    return;
                }
            }
        }
        catch (Exception ex)
        {

        }
        //lblRaiseaDebitAgainstPo.Visible = true;
        if (rblPriceOther.SelectedIndex.Equals(0))
        {
            lblPurchaseOrderDebitPriceDifference.Visible = true;
            lblPurchaseOrderDebitReasonOther.Visible = false;
        }
        else
        {
            lblPurchaseOrderDebitPriceDifference.Visible = false;
            lblPurchaseOrderDebitReasonOther.Visible = true;
        }
    }

    protected void RemoveImage_Click(object sender, CommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "Remove" && e.CommandArgument != null)
            {
                int iCount = 1;
                tbDummy = null;
                tbDummy = new DataTable();
                AddColumns();
                foreach (GridViewRow item in gvLogInvoiceAddLine.Rows)
                {



                    ucTextbox txtLineNo = (ucTextbox)item.FindControl("txtLineNo");
                    ucTextbox txtOfficeDepotCode = (ucTextbox)item.FindControl("txtOfficeDepotCode");
                    ucTextbox txtVikingCode = (ucTextbox)item.FindControl("txtVikingCode");
                    ucTextbox txtVendorItemCode = (ucTextbox)item.FindControl("txtVendorItemCode");
                    ucTextbox txtDescription = (ucTextbox)item.FindControl("txtDescription");
                    ucTextbox txtOriginalPOQuantity = (ucTextbox)item.FindControl("txtOriginalPOQuantity");
                    ucTextbox txtOutstandingQty = (ucTextbox)item.FindControl("txtOutstandingQty");
                    ucTextbox txtUoM = (ucTextbox)item.FindControl("txtUoM");
                    ucTextbox txtPOItemCost = (ucTextbox)item.FindControl("txtPOItemCost");
                    ucTextbox txtPOTotalCost = (ucTextbox)item.FindControl("txtPOTotalCost");
                    ucTextbox txtValueDebited = (ucTextbox)item.FindControl("txtValueDebited");
                    ucDropdownList drpDebitReason = (ucDropdownList)item.FindControl("ddlReason");
                    ucButton btnAddLine = (ucButton)item.FindControl("btnAddLine");
                    ucButton btnRemove = (ucButton)item.FindControl("btnRemove");


                    HiddenField hdnRecordID = (HiddenField)item.FindControl("hdnRecordID");
                    if (e.CommandArgument.ToString().Trim() != hdnRecordID.Value.ToString().Trim())
                    {
                        tbDummy.Rows.Add(txtLineNo.Text.Trim(),
                                     txtOfficeDepotCode.Text.Trim(),
                                     txtVikingCode.Text.Trim(),
                                     txtVendorItemCode.Text.Trim(),
                                     txtDescription.Text.Trim(),
                                     txtOriginalPOQuantity.Text.Trim(),
                                     txtOutstandingQty.Text.Trim(),
                                     txtUoM.Text.Trim(),
                                      txtPOItemCost.Text.Trim(),
                                      txtPOTotalCost.Text.Trim(),
                                      txtValueDebited.Text.Trim(),
                                      drpDebitReason.SelectedItem.Text.ToString(),
                                     "Remove",
                                     (iCount++).ToString());
                    }
                }

                BindWithGrid();
                EnableDisableGridControls();
            }
        }
        catch (Exception ex)
        {

        }
    }

    protected void gvLogInvoiceAddLine_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Remove" && e.CommandArgument != null)
        {

            int rowIndex = Convert.ToInt32(e.CommandArgument.ToString());
            var dicDebitReason = (Dictionary<int, string>)ViewState["GVDebitReason"];
            if (dicDebitReason != null)
                dicDebitReason.Remove(rowIndex);

        }
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        SearchVendor(ref lstLeft, txtVendorNo.Text, "Local");
    }

    protected void rblPriceOther_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (rblPriceOther.SelectedIndex.Equals(0))
        {
            lblRaiseaDebitAgainstPo.Visible = true;
            lblPurchaseOrderDebitPriceDifference.Visible = false;
            lblPurchaseOrderDebitReasonOther.Visible = false;
        }
        else
        {
            lblRaiseaDebitAgainstPo.Visible = true;
            lblPurchaseOrderDebitPriceDifference.Visible = false;
            lblPurchaseOrderDebitReasonOther.Visible = false;
        }
    }

    #endregion

    #region Methods ...

    private void GetPurchaseOrderList()
    {
        DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
        DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();

        oDiscrepancyBE.Action = "GetPurchaseOrderDate";
        oDiscrepancyBE.SiteID = Convert.ToInt32(ucSite.innerControlddlSite.SelectedValue);
        oDiscrepancyBE.PurchaseOrderNumber = txtPurchaseOrder.Text.Trim();

        List<DiscrepancyBE> lstDetails = oDiscrepancyBAL.GetPurchaseOrderDateListBAL(oDiscrepancyBE);
        ViewState["GetPODateList"] = lstDetails;
        oDiscrepancyBAL = null;
        if (lstDetails.Count > 0 && lstDetails != null)
        {
            FillControls.FillDropDown(ref ddlPurchaseOrderDate, lstDetails, "PurchaseOrder_Order_raised", "RowNumber");
            if (!string.IsNullOrEmpty(ddlPurchaseOrderDate.SelectedItem.Value))
            {
                var PODateDetail = lstDetails.Find(p => p.RowNumber == Convert.ToInt32(ddlPurchaseOrderDate.SelectedItem.Value));
                lblVendorValue.Text = PODateDetail.VendorNoName;
                //ucSDRCommunication1.VendorID = Convert.ToInt32(PODateDetail.VendorID);

                if (PODateDetail.VendorID != null)
                {
                    ucSDRCommunication1.FillAPContacts(Convert.ToInt32(PODateDetail.VendorID));
                    this.SetPOVendorVATReference(Convert.ToInt32(PODateDetail.VendorID));
                }
            }

            //if (!string.IsNullOrEmpty(ucSite.innerControlddlSite.SelectedValue))
            //    ucSDRCommunication1.SiteID = Convert.ToInt32(ucSite.innerControlddlSite.SelectedValue);
            //else
            //    ucSDRCommunication1.SiteID = 0;

        }
        else
        {
            string saveMessage = WebCommon.getGlobalResourceValue("InvalidPurchaseOrder");
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('" + saveMessage + "')", true);

            #region Clearing PO details
            txtPurchaseOrder.Text = string.Empty;
            ddlPurchaseOrderDate.Items.Clear();
            ListItem listItem = new ListItem();
            listItem.Text = "--Select--";
            listItem.Value = "0";
            ddlPurchaseOrderDate.Items.Add(listItem);
            lblVendorValue.Text = string.Empty;
            //lblIssueRaisedDate.Text = string.Empty;
            txtPurchaseOrder.Focus();
            #endregion
        }
    }

    private void GetDeliveryDetails()
    {
        if (GetQueryStringValue("disLogID") != null)
        {
            DiscrepancyBE oNewDiscrepancyBE = new DiscrepancyBE();
            DiscrepancyBAL oNewDiscrepancyBAL = new DiscrepancyBAL();
            oNewDiscrepancyBE.Action = "GetDiscrepancyLog";
            oNewDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
            if (GetQueryStringValue("status").ToLower() == "deleted")
            {
                oNewDiscrepancyBE.IsDelete = true;
            }
            List<DiscrepancyBE> lstDisLog = oNewDiscrepancyBAL.GetDiscrepancyLogDetailsBAL(oNewDiscrepancyBE);
            oNewDiscrepancyBAL = null;
            if (lstDisLog != null && lstDisLog.Count > 0)
            {

            }
        }
        else
        {
            lblVendorNumberValue_PO.Text = lblVendorValue.Text;
            lblSiteValue.Text = ucSite.innerControlddlSite.SelectedItem.Text;
            lblPurchaseOrderValue.Text = txtPurchaseOrder.Text;
            lblPurchaseOrderDateValue.Text = ddlPurchaseOrderDate.SelectedItem.Text;
            lblInvoiceNoValue.Text = txtInvoiceNo.Text;
            lblVendorVATHashValue.Text = txtVendorVATReferenceValue.Text;
            lblOfficeDepotVATHashValue.Text = lblOurVATReferenceValue.Text;
        }
    }

    private void GetContactPurchaseOrderDetails()
    {
        try
        {
            var oDiscrepancyBE = new DiscrepancyBE();
            var oDiscrepancyBAL = new DiscrepancyBAL();
            if (GetQueryStringValue("disLogID") != null)
            {
                gvLogInvoiceViewMode.Style["display"] = string.Empty;
                gvLogInvoice.Style["display"] = "none";
                oDiscrepancyBE.Action = "GetDiscrepancyItem";
                oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
                List<DiscrepancyBE> lstDetails = oDiscrepancyBAL.GetDiscrepancyItemDetailsBAL(oDiscrepancyBE);
                if (lstDetails.Count > 0 && lstDetails != null)
                {
                    gvLogInvoiceViewMode.DataSource = lstDetails;
                    gvLogInvoiceViewMode.DataBind();
                    ViewState["lstSitesViewMode"] = lstDetails;
                }
            }

            else
            {
                gvLogInvoiceViewMode.Style["display"] = "none";
                gvLogInvoice.Style["display"] = string.Empty;
                oDiscrepancyBE.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();
                oDiscrepancyBE.Action = "GetContactPurchaseOrderDetails";
                oDiscrepancyBE.PurchaseOrderNumber = txtPurchaseOrder.Text.Trim();
                oDiscrepancyBE.PurchaseOrder = new BusinessEntities.ModuleBE.Upload.Up_PurchaseOrderDetailBE();
                oDiscrepancyBE.PurchaseOrder.Order_raised = Common.GetMM_DD_YYYY(ddlPurchaseOrderDate.SelectedItem.Text);
                oDiscrepancyBE.SiteID = Convert.ToInt32(ucSite.innerControlddlSite.SelectedItem.Value);

                List<DiscrepancyBE> lstDetails = oDiscrepancyBAL.GetContactPurchaseOrderDetailsBAL(oDiscrepancyBE);
                if (lstDetails.Count > 0 && lstDetails != null)
                {
                    if (lstDetails[0].Vendor.NewCurrencyID != null)
                    {
                       // ddlCurrency.SelectedIndex = Convert.ToInt32(lstDetails[0].Vendor.NewCurrencyID);
                        ddlCurrencyPO.SelectedValue = Convert.ToString(lstDetails[0].Vendor.NewCurrencyID);
                    }
                    if (!string.IsNullOrEmpty(ddlPurchaseOrderDate.SelectedItem.Value))
                    {
                        List<DiscrepancyBE> lstGetPODateList = (List<DiscrepancyBE>)ViewState["GetPODateList"];
                        var PODateDetail = lstGetPODateList.Find(p => p.RowNumber == Convert.ToInt32(ddlPurchaseOrderDate.SelectedItem.Value));
                        //lblVendorNumberValue.Text = PODateDetail.VendorNoName;
                    }

                    lblContactValue_PO.Text = UIUtility.GetUserPhoneNoofVendor(Convert.ToInt32(lstDetails[0].VendorID), Convert.ToInt32(ucSite.innerControlddlSite.SelectedItem.Value));
                    //lblStockPlannerNoValue.Text = lstDetails[0].StockPlannerNO.ToString();
                    //lblPlannerContactNoValue.Text = lstDetails[0].StockPlannerContact.ToString();
                    ViewState["PurchaseOrderID"] = lstDetails[0].PurchaseOrderID != null ? lstDetails[0].PurchaseOrderID.ToString() : null;
                    ViewState["VendorID"] = lstDetails[0].VendorID != null ? lstDetails[0].VendorID.ToString() : null;
                    ViewState["StockPlannerID"] = lstDetails[0].StockPlannerID != null ? lstDetails[0].StockPlannerID.ToString() : null;
                    ViewState["StockPlannerEmailID"] = lstDetails[0].StockPlannerEmailID != null ? lstDetails[0].StockPlannerEmailID.ToString() : null;
                    gvLogInvoice.DataSource = lstDetails;
                    gvLogInvoice.DataBind();
                    ViewState["lstSites"] = lstDetails;
                }
                oDiscrepancyBAL = null;
                //foreach (GridViewRow row in gvLogInvoice.Rows)
                //{
                //    TextBox txtbox = (TextBox)row.FindControl("txtChaseQty");

                //    sp1.RegisterPostBackControl(txtbox);
                //}
            }

            GVLogInvoiceColumnVisibility(rblPriceOther.SelectedIndex);

        }
        catch (Exception ex)
        {

        }
    }

    private void GVLogInvoiceColumnVisibility(int radioIndex = 1)
    {
        switch (radioIndex)
        {
            case 0:
                {
                    gvLogInvoice.Columns[10].Visible = false;
                    gvLogInvoice.Columns[11].Visible = false;
                    gvLogInvoice.Columns[12].Visible = true;
                    gvLogInvoice.Columns[13].Visible = true;
                    gvLogInvoice.Columns[14].Visible = true;
                    break;
                }
            case 1:
                {
                    gvLogInvoice.Columns[10].Visible = true;
                    gvLogInvoice.Columns[11].Visible = true;
                    gvLogInvoice.Columns[12].Visible = false;
                    gvLogInvoice.Columns[13].Visible = false;
                    gvLogInvoice.Columns[14].Visible = false;
                    break;
                }
        }
    }

    protected void btnAdd_Click(object sender, EventArgs e)
    {
        IncreaseRow();
    }

    private void AddColumns()
    {
        tbDummy.Columns.Add("Line_No");
        tbDummy.Columns.Add("OD_Code");
        tbDummy.Columns.Add("Direct_code");
        tbDummy.Columns.Add("Vendor_Code");
        tbDummy.Columns.Add("ProductDescription");
        tbDummy.Columns.Add("Original_quantity");
        tbDummy.Columns.Add("Outstanding_Qty");
        tbDummy.Columns.Add("UOM");
        tbDummy.Columns.Add("PO_cost");
        tbDummy.Columns.Add("PO_Totalcost");
        tbDummy.Columns.Add("ValueDebited");
        tbDummy.Columns.Add("ReasonForDebit");
        tbDummy.Columns.Add("Button");
        tbDummy.Columns.Add("RecordID");
    }

    private void BindWithGrid()
    {
        //add this table to dataset
        try
        {
            dsDummy.Tables.Add(tbDummy);

            //bind this dataset to grid
            gvLogInvoiceAddLine.DataSource = dsDummy;
            gvLogInvoiceAddLine.DataBind();
            ViewState["myDataTable"] = tbDummy;
        }
        catch (Exception ex)
        {
        }
    }

    private void EnableDisableGridControls()
    {
        foreach (GridViewRow item in gvLogInvoiceAddLine.Rows)
        {
            ucTextbox txtLineNo = (ucTextbox)item.FindControl("txtLineNo");
            ucTextbox txtOfficeDepotCode = (ucTextbox)item.FindControl("txtOfficeDepotCode");
            ucTextbox txtVikingCode = (ucTextbox)item.FindControl("txtVikingCode");
            ucTextbox txtVendorItemCode = (ucTextbox)item.FindControl("txtVendorItemCode");
            ucTextbox txtDescription = (ucTextbox)item.FindControl("txtDescription");
            ucTextbox txtOriginalPOQuantity = (ucTextbox)item.FindControl("txtOriginalPOQuantity");
            ucTextbox txtOutstandingQty = (ucTextbox)item.FindControl("txtOutstandingQty");
            ucTextbox txtUoM = (ucTextbox)item.FindControl("txtUoM");
            ucTextbox txtPOItemCost = (ucTextbox)item.FindControl("txtPOItemCost");
            ucTextbox txtPOTotalCost = (ucTextbox)item.FindControl("txtPOTotalCost");
            ucTextbox txtValueDebited = (ucTextbox)item.FindControl("txtValueDebited");
            ucDropdownList drpDebitReason = (ucDropdownList)item.FindControl("ddlReason");
            ucButton btnAddLine = (ucButton)item.FindControl("btnAddLine");
            ucButton btnRemove = (ucButton)item.FindControl("btnRemove");
            btnRemove.Attributes.Remove("style");
            DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
            DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();

            oDiscrepancyBE.Action = "GetODProductDetails";
            oDiscrepancyBE.ODSKUCode = txtOfficeDepotCode.Text.Trim();
            oDiscrepancyBE.SiteID = Convert.ToInt32(ucSite.innerControlddlSite.SelectedValue);
            List<DiscrepancyBE> lstODProductDetails = oDiscrepancyBAL.GetODProductDetailsBAL(oDiscrepancyBE);
            if (lstODProductDetails != null
                && lstODProductDetails.Count > 0
                && txtOfficeDepotCode.Text.Trim() != "")
            {
                txtLineNo.Enabled = true;
                txtVikingCode.Enabled = false;
                txtVendorItemCode.Enabled = false;
                txtOfficeDepotCode.Enabled = false;
                txtDescription.Enabled = false;
                txtUoM.Enabled = false;
                txtOriginalPOQuantity.Enabled = true;
                txtOutstandingQty.Enabled = true;

            }
            else
            {

                txtLineNo.Enabled = true;
                txtVikingCode.Enabled = true;
                txtVendorItemCode.Enabled = true;
                txtOfficeDepotCode.Enabled = true;
                txtDescription.Enabled = true;
                txtUoM.Enabled = true;
                if (txtOriginalPOQuantity != null)
                    txtOriginalPOQuantity.Enabled = true;
                if (txtOutstandingQty != null)
                    txtOutstandingQty.Enabled = true;

            }

            GridViewRow currentGridViewRow = (GridViewRow)txtLineNo.Parent.Parent;
            int ind = currentGridViewRow.RowIndex;
            ind = ind + 1;
            if (ind == gvLogInvoiceAddLine.Rows.Count)
            {
                btnAddLine.Style["display"] = "block";
                btnRemove.Style["display"] = "none";
            }
            else
            {
                btnAddLine.Style["display"] = "none";
                btnRemove.Style["display"] = "block";
            }

        }
    }

    private void IncreaseRow()
    {
        int Total = gvLogInvoiceAddLine.Rows.Count;
        int iCount = 1;
        Total = Total + 1; //increase 1 because of add
        AddColumns();
        foreach (GridViewRow item in gvLogInvoiceAddLine.Rows)
        {


            ucTextbox txtLineNo = (ucTextbox)item.FindControl("txtLineNo");
            ucTextbox txtOfficeDepotCode = (ucTextbox)item.FindControl("txtOfficeDepotCode");
            ucTextbox txtVikingCode = (ucTextbox)item.FindControl("txtVikingCode");
            ucTextbox txtVendorItemCode = (ucTextbox)item.FindControl("txtVendorItemCode");
            ucTextbox txtDescription = (ucTextbox)item.FindControl("txtDescription");
            ucTextbox txtOriginalPOQuantity = (ucTextbox)item.FindControl("txtOriginalPOQuantity");
            ucTextbox txtOutstandingQty = (ucTextbox)item.FindControl("txtOutstandingQty");
            ucTextbox txtUoM = (ucTextbox)item.FindControl("txtUoM");
            ucTextbox txtPOItemCost = (ucTextbox)item.FindControl("txtPOItemCost");
            ucTextbox txtPOTotalCost = (ucTextbox)item.FindControl("txtPOTotalCost");
            ucTextbox txtValueDebited = (ucTextbox)item.FindControl("txtValueDebited");
            ucDropdownList drpDebitReason = (ucDropdownList)item.FindControl("ddlReason");
            ucButton btnAddLine = (ucButton)item.FindControl("btnAddLine");
            ucButton btnRemove = (ucButton)item.FindControl("btnRemove");




            tbDummy.Rows.Add(txtLineNo.Text.Trim(),
                                 txtOfficeDepotCode.Text.Trim(),
                                 txtVikingCode.Text.Trim(),
                                 txtVendorItemCode.Text.Trim(),
                                 txtDescription.Text.Trim(),
                                 txtOriginalPOQuantity.Text.Trim(),
                                 txtOutstandingQty.Text.Trim(),
                                 txtUoM.Text.Trim(),
                                 txtPOItemCost.Text.Trim(),
                                 txtPOTotalCost.Text.Trim(),
                                 txtValueDebited.Text.Trim(),
                                 drpDebitReason.SelectedItem.Text.ToString(),
                                 "Remove",
                                 (iCount++).ToString());

        }

        tbDummy.Rows.Add(string.Empty,
                                 string.Empty,
                                 string.Empty,
                                 string.Empty,
                                 string.Empty,
                                 string.Empty,
                                 string.Empty,
                                 string.Empty,
                                 string.Empty,
                                 string.Empty,
                                 string.Empty,
                                 string.Empty,
                                 "Remove",
                                 (iCount++).ToString());


        BindWithGrid();

        EnableDisableGridControls();
    }

    private void GetAllReasonsForDebit()
    {
        var debitReasonTypeBAL = new DIS_DebitReasonTypeBAL();
        var apActionBE = new APActionBE();
        apActionBE.Action = "GetReasonTypeSetUp";
        apActionBE.ReasonType = "DR"; /* Debit Reason */
        var lstAPActionBE = debitReasonTypeBAL.GetReasonTypeBAL(apActionBE);
        if (lstAPActionBE != null && lstAPActionBE.Count > 0)
        {
            FillControls.FillDropDown(ref ddlReason, lstAPActionBE, "Reason", "ReasonTypeID", "Select");
            ViewState["DebitReason"] = lstAPActionBE;
        }
    }

    private void BindCurrency()
    {
        var currencyBAL = new MAS_CurrencyBAL();
        var currencyBE = new CurrencyBE();
        currencyBE.Action = "ShowAll";
        var lstCurrency = currencyBAL.GetCurrenyBAL(currencyBE);
        if (lstCurrency != null && lstCurrency.Count > 0)
        {
            FillControls.FillDropDown(ref ddlCurrency, lstCurrency, "CurrencyName", "CurrencyID", "Select");
            FillControls.FillDropDown(ref ddlCurrencyPO, lstCurrency, "CurrencyName", "CurrencyID", "Select");
        }
    }

    private void Clear()
    {
        if (rdoDebitLinkedToPO.Checked)
        {
            txtPurchaseOrder.Text = string.Empty;
            ddlPurchaseOrderDate.Items.Clear();
            lblVendorValue.Text = string.Empty;
            txtInvoiceNo.Text = string.Empty;
            lblVendorNumberValue_PO.Text = string.Empty;
            lblContactValue_PO.Text = string.Empty;
            lblSiteValue.Text = string.Empty;
            lblPurchaseOrderValue.Text = string.Empty;
            lblPurchaseOrderDateValue.Text = string.Empty;
            lblInvoiceNoValue.Text = string.Empty;

            var txtCommEmailList = (ucTextbox)ucSDRCommunication1.FindControl("txtVendorEmailList");
            txtCommEmailList.Text = string.Empty;
            var txtAdditionalEmailList = (ucTextbox)ucSDRCommunication1.FindControl("txtAdditionalEmailList");
            txtAdditionalEmailList.Text = string.Empty;

            gvLogInvoice.DataSource = null;
            gvLogInvoice.DataBind();

            gvLogInvoiceAddLine.DataSource = null;
            gvLogInvoiceAddLine.DataBind();

            if (ddlCurrencyPO.Items.Count > 0)
                ddlCurrencyPO.SelectedIndex = 0;

            txtPOCommnets.Text = string.Empty;
        }
        else if (rdoDebitLinkedtoVendor.Checked)
        {
            //TextBox txtVendor = (TextBox)ucSeacrhVendor1.FindControl("txtVendorNo");
            //Label lblSelectedVendorName = (Label)ucSeacrhVendor1.FindControl("SelectedVendorName");
            //txtVendor.Text = string.Empty;
            //lblSelectedVendorName.Text = string.Empty;

            //txtReference.Text = string.Empty;
            lblVendorNumberValue_Vendor.Text = string.Empty;
            lblReferenceValue_Vendor.Text = string.Empty;
            txtValueTobeDebited.Text = string.Empty;

            if (ddlCurrency.Items.Count > 0)
                ddlCurrency.SelectedIndex = 0;

            txtVendorComment.Text = string.Empty;
            var txtCommEmailList = (ucTextbox)ucSDRCommunication2.FindControl("txtVendorEmailList");
            txtCommEmailList.Text = string.Empty;
            var txtAdditionalEmailList = (ucTextbox)ucSDRCommunication2.FindControl("txtAdditionalEmailList");
            txtAdditionalEmailList.Text = string.Empty;
        }
    }

    private void BindPriceOtherRadioButton()
    {
        rblPriceOther.Items.Clear();
        var listItem = new ListItem(WebCommon.getGlobalResourceValue("PriceQuery"), "PQ");
        rblPriceOther.Items.Add(listItem);
        listItem = new ListItem(WebCommon.getGlobalResourceValue("Other"), "O");
        listItem.Selected = true;
        rblPriceOther.Items.Add(listItem);

    }

    private void SetPOODVATReference()
    {
        var masVatCodeBAL = new MAS_VatCodeBAL();
        var masVatCodeBE = new MAS_VatCodeBE();
        masVatCodeBE.Action = "ShowAllODVatCode";
        if (!string.IsNullOrEmpty(ucSite.innerControlddlSite.SelectedValue))
            masVatCodeBE.SiteId = Convert.ToInt32(ucSite.innerControlddlSite.SelectedValue);

        var lstGetODVatCode = masVatCodeBAL.GetODVatCodeBAL(masVatCodeBE);
        if (lstGetODVatCode != null && lstGetODVatCode.Count > 0)
        {
            lblOurVATReferenceValue.Text = lstGetODVatCode[0].VatCode;
            ViewState["ODVatCodeID"] = lstGetODVatCode[0].ODVatCodeID;
        }
        else
            lblOurVATReferenceValue.Text = string.Empty;
    }

    private void SetPOVendorVATReference(int vendorId)
    {
        var masVatCodeBAL = new MAS_VatCodeBAL();
        var masVatCodeBE = new MAS_VatCodeBE();
        masVatCodeBE.Action = "GetVendorVatCodeByVendorId";
        masVatCodeBE.Vendor = new MAS_VendorBE();
        masVatCodeBE.Vendor.VendorID = vendorId;
        var vendorVatCodeDetails = masVatCodeBAL.GetVendorVatCodeByVendorIdBAL(masVatCodeBE);
        if (vendorVatCodeDetails.Vendor != null)
        {
            if (vendorVatCodeDetails.Vendor.NewCurrencyID != null)
            {
                ddlCurrency.SelectedValue =Convert.ToString(vendorVatCodeDetails.Vendor.NewCurrencyID);

            }
            if (rdoDebitLinkedToPO.Checked)
                txtVendorVATReferenceValue.Text = vendorVatCodeDetails.Vendor.VatCode;
            else
                txtVendVendorVATReference.Text = vendorVatCodeDetails.Vendor.VatCode;
        }
        else
        {
            txtVendorVATReferenceValue.Text = string.Empty;
            txtVendVendorVATReference.Text = string.Empty;
        }
    }

    //private void SetVenVendorVATReference()
    //{
    //    var masVatCodeBAL = new MAS_VatCodeBAL();
    //    var masVatCodeBE = new MAS_VatCodeBE();
    //    masVatCodeBE.Action = "GetVendorVatCodeByVendorId";
    //    masVatCodeBE.Vendor = new MAS_VendorBE();
    //    if (ViewState["VendorID"] != null)
    //        masVatCodeBE.Vendor.VendorID = Convert.ToInt32(ucSite.innerControlddlSite.SelectedValue);

    //    var vendorVatCodeDetails = masVatCodeBAL.GetVendorVatCodeByVendorIdBAL(masVatCodeBE);
    //    if (vendorVatCodeDetails.Vendor != null)
    //    {
    //        txtVendVendorVATReference.Text = vendorVatCodeDetails.VatCode;
    //    }
    //}

    private void SetVendODVATReference()
    {
        var masVatCodeBAL = new MAS_VatCodeBAL();
        var masVatCodeBE = new MAS_VatCodeBE();
        masVatCodeBE.Action = "ShowAllODVatCode";
        //if (!string.IsNullOrEmpty(ucSite.innerControlddlSite.SelectedValue))
        //    masVatCodeBE.SiteId = Convert.ToInt32(ucSite.innerControlddlSite.SelectedValue);

        var lstGetODVatCode = masVatCodeBAL.GetODVatCodeBAL(masVatCodeBE);
        if (lstGetODVatCode != null && lstGetODVatCode.Count > 0)
        {
            FillControls.FillDropDown(ref ddlVendOfficeDepotVATReference, lstGetODVatCode, "VatCode", "ODVatCodeID", "Select");
        }
    }

    private decimal GetTotalValueDebited()
    {
        decimal TotalValueDebited = 0;

        if (rdoDebitLinkedToPO.Checked) /* This condition for PO Selection. */
        {
            /* For Price Query Radio Button Option */
            if (rblPriceOther.SelectedIndex.Equals(0))
            {
                if (!string.IsNullOrEmpty(txtDebitValueText.Text) && !string.IsNullOrWhiteSpace(txtDebitValueText.Text))
                    TotalValueDebited = Convert.ToDecimal(txtDebitValueText.Text);

                #region Commented code ...
                /* Logic to get value row by row ... */
                //foreach (GridViewRow row in gvLogInvoice.Rows)
                //{
                //    var hdnPriceDifferenceValue = (HiddenField)row.FindControl("hdnPriceDifferenceValue");
                //    if (!string.IsNullOrEmpty(hdnPriceDifferenceValue.Value) && !string.IsNullOrWhiteSpace(hdnPriceDifferenceValue.Value))
                //    {
                //        if (!string.IsNullOrEmpty(hdnPriceDifferenceValue.Value) && !string.IsNullOrWhiteSpace(hdnPriceDifferenceValue.Value))
                //        {
                //            if (TotalValueDebited.Equals(0))
                //                TotalValueDebited = Convert.ToDecimal(hdnPriceDifferenceValue.Value);
                //            else
                //                TotalValueDebited = TotalValueDebited + Convert.ToDecimal(hdnPriceDifferenceValue.Value);
                //        }
                //    }
                //}
                #endregion
            }
            else /* For Other Radio Button Option */
            {
                /* Existing Grid Item Line data saving */
                foreach (GridViewRow row in gvLogInvoice.Rows)
                {
                    var txtValueDebited = (ucTextbox)row.FindControl("txtValueDebited");
                    if (!string.IsNullOrEmpty(txtValueDebited.Text) && !string.IsNullOrWhiteSpace(txtValueDebited.Text))
                    {
                        if (!string.IsNullOrEmpty(txtValueDebited.Text) && !string.IsNullOrWhiteSpace(txtValueDebited.Text))
                        {
                            if (TotalValueDebited.Equals(0))
                                TotalValueDebited = Convert.ToDecimal(txtValueDebited.Text);
                            else
                                TotalValueDebited = TotalValueDebited + Convert.ToDecimal(txtValueDebited.Text);
                        }
                    }
                }

                /* New Grid Item Line data saving */
                foreach (GridViewRow row in gvLogInvoiceAddLine.Rows)
                {
                    var txtValueDebited = (ucTextbox)row.FindControl("txtValueDebited");
                    if (!string.IsNullOrEmpty(txtValueDebited.Text) && !string.IsNullOrWhiteSpace(txtValueDebited.Text))
                    {
                        if (!string.IsNullOrEmpty(txtValueDebited.Text) && !string.IsNullOrWhiteSpace(txtValueDebited.Text))
                        {
                            if (TotalValueDebited.Equals(0))
                                TotalValueDebited = Convert.ToDecimal(txtValueDebited.Text);
                            else
                                TotalValueDebited = TotalValueDebited + Convert.ToDecimal(txtValueDebited.Text);
                        }
                    }
                }
            }
        }
        else if (rdoDebitLinkedtoVendor.Checked) /* This condition for Vendor Selection. */
        {
            if (!string.IsNullOrEmpty(txtValueTobeDebited.Text) && !string.IsNullOrWhiteSpace(txtValueTobeDebited.Text))
                TotalValueDebited = Convert.ToDecimal(txtValueTobeDebited.Text);
        }

        return TotalValueDebited;
    }

    private List<UP_VendorBE> GetVendorDetails(int vendorId)
    {
        var vendorBE = new UP_VendorBE();
        var vendorBAL = new UP_VendorBAL();
        vendorBE.VendorID = vendorId;
        vendorBE.Action = "ShowVendorByID";
        var lstDetails = new List<UP_VendorBE>();
        lstDetails = vendorBAL.GetVendorByIdBAL(vendorBE);
        return lstDetails;
    }

    private void SearchVendor(ref ucListBox lstList, string VendorName, string ReportType)
    {
        UP_VendorBAL oUP_VendorBAL = new UP_VendorBAL();
        UP_VendorBE oUP_VendorBE = new UP_VendorBE();

        oUP_VendorBE.Action = "GetConsVendorByName";
        oUP_VendorBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
        oUP_VendorBE.User.UserID = Convert.ToInt32(Session["Userid"]);
        oUP_VendorBE.VendorName = VendorName;
        if (ReportType == "Local")
            oUP_VendorBE.VendorFlag = 'P';
        else if (ReportType == "Global")
            oUP_VendorBE.VendorFlag = 'G';

        List<UP_VendorBE> lstUPVendor = new List<UP_VendorBE>();
        lstUPVendor = oUP_VendorBAL.GetVendorByUserCountryBAL(oUP_VendorBE);


        oUP_VendorBAL = null;
        if (lstUPVendor.Count > 0)
        {
            FillControls.FillListBox(ref lstList, lstUPVendor, "VendorName", "VendorID");

            /// --- Added for Showing Inative Vendors --- 27 Feb 2013
            for (int i = 0; i < lstUPVendor.Count; i++)
            {
                if (lstUPVendor[i].VendorID > 0)
                    if (Convert.ToInt32(lstList.Items[i].Value) == lstUPVendor[i].VendorID && lstUPVendor[i].IsActiveVendor == "N")
                        lstList.Items[i].Attributes.Add("Style", "background-color:RED;");
            }
            /// --- Added for Showing Inative Vendors --- 27 Feb 2013
        }
    }

    #endregion
}
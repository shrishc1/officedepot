﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    AutoEventWireup="true" CodeFile="DebitRaise.aspx.cs" Inherits="ModuleUI_Discrepancy_AccountsPayable_DebitRaise" %>

<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register TagName="ucDate" TagPrefix="cc2" Src="~/CommonUI/UserControls/ucDate.ascx" %>
<%@ Register Src="../../../CommonUI/UserControls/ucSite.ascx" TagName="ucSite" TagPrefix="cc2" %>
<%@ Register Src="../../../CommonUI/UserControls/ucSeacrhVendorForDisc.ascx" TagName="ucSeacrhVendor"
    TagPrefix="cc3" %>
<%@ Register Src="../UserControl/ucSDRCommunicationAPListing.ascx" TagName="ucSDRCommunication"
    TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script type="text/javascript">
        function ConfirmMess(obj) {
            var row = $(this).parent("td").parent('tr');
            row.remove();
        }

        function ShowSelectedVendorName(type) {
            if (type == 'Local') {
                var txtObj = $('#<%=txtSelectedVendorValue.ClientID%>');
                var lstObj = $('#<%=lstLeft.ClientID%> option:selected'); //.text();
                var hdnID = $('#<%=hdnLocalvendorID.ClientID%>');
                var hdnVendName = $('#<%=hdnLocalvendorname.ClientID%>');
            }

            if (lstObj.text() != '') {
                $(txtObj).val(lstObj.text());
                $(hdnVendName).val(lstObj.text());
                $(hdnID).val(lstObj.val());
            }
        }

        function RemoveSelectedVendorName(type) {
            if (type == 'Local') {
                var txtObj = $('#<%=txtSelectedVendorValue.ClientID%>');
                $('#<%=lstLeft.ClientID%>').find("option").attr("selected", false);
                var hdnID = $('#<%=hdnLocalvendorID.ClientID%>');
                var hdnVendName = $('#<%=hdnLocalvendorname.ClientID%>');
            }

            $(txtObj).val('');
            $(hdnID).val('0');
            $(hdnVendName).val('');
        }
    
    </script>
    <asp:ScriptManager ID="sp1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="up1" runat="server">
        <ContentTemplate>
            <h2>
                <cc1:ucLabel ID="lblRaiseaDebit" runat="server"></cc1:ucLabel>
                <cc1:ucLabel ID="lblRaiseaDebitAgainstPo" Visible="false" runat="server"></cc1:ucLabel>
                <cc1:ucLabel ID="lblPurchaseOrderDebitReasonOther" Visible="false" runat="server"></cc1:ucLabel>
                <cc1:ucLabel ID="lblPurchaseOrderDebitPriceDifference" Visible="false" runat="server"></cc1:ucLabel>
                <cc1:ucLabel ID="lblRaiseaDebitAgainstVendor" Visible="false" runat="server"></cc1:ucLabel>
            </h2>
            <asp:HiddenField ID="hdnLocalvendorID" runat="server" Value="0" />
            <asp:HiddenField ID="hdnLocalvendorname" runat="server" Value="0" />
            <div class="right-shadow">
                <div class="formbox">
                    <cc1:ucMultiView ID="mvDebitRaiseType" runat="server" ActiveViewIndex="0">
                        <cc1:ucView ID="vwDebitRaiseType" runat="server">
                            <table width="100%" cellspacing="1" cellpadding="0" class="form-table" border="0">
                                <tr>
                                    <td colspan="2">
                                        <cc1:ucLabel ID="lblDebitRaiseTypeDesc" runat="server"></cc1:ucLabel>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 20%">
                                        <cc1:ucRadioButton ID="rdoDebitLinkedToPO" runat="server" Checked="true" GroupName="DebitRaiseType"
                                            AutoPostBack="true" />
                                    </td>
                                    <td style="width: 80%">
                                        <cc1:ucRadioButton ID="rdoDebitLinkedtoVendor" runat="server" GroupName="DebitRaiseType"
                                            AutoPostBack="true" />
                                    </td>
                                </tr>
                            </table>
                        </cc1:ucView>
                        <cc1:ucView ID="vwDebitRaiseByPO" runat="server">
                            <cc1:ucPanel ID="pnlPurchaseOrderDetails" runat="server" GroupingText="Purchase Order Details"
                                CssClass="fieldset-form">
                                <table width="40%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                                    <tr>
                                        <td style="font-weight: bold; width: 40%">
                                            <cc1:ucLabel ID="lblSitePrefixName" runat="server" Text="Site"></cc1:ucLabel>
                                        </td>
                                        <td style="width: 5%">
                                            :
                                        </td>
                                        <td style="width: 55%">
                                            <cc2:ucSite ID="ucSite" runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: bold;">
                                            <cc1:ucLabel ID="lblOurVATReference" runat="server" Text="Our VAT Reference"></cc1:ucLabel>
                                        </td>
                                        <td>
                                            :
                                        </td>
                                        <td>
                                            <%--<cc1:ucTextbox ID="txtOurVATReferenceValue" runat="server" Width="120px" MaxLength="15"></cc1:ucTextbox>--%>
                                            <cc1:ucLabel ID="lblOurVATReferenceValue" runat="server"></cc1:ucLabel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: bold;">
                                            <cc1:ucLabel ID="lblPurchaseOrder" runat="server" Text="Purchase Order" isRequired="true"></cc1:ucLabel>
                                        </td>
                                        <td>
                                            :
                                        </td>
                                        <td>
                                            <cc1:ucTextbox ID="txtPurchaseOrder" runat="server" Width="145px" MaxLength="11"
                                                AutoPostBack="true" OnTextChanged="txtPurchaseOrder_TextChanged"></cc1:ucTextbox>
                                            <asp:RequiredFieldValidator ID="rfvPurchaseOrderNumberValidation" runat="server"
                                                ControlToValidate="txtPurchaseOrder" Display="None" ValidationGroup="LogDiscrepancy">
                                            </asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: bold;">
                                            <cc1:ucLabel ID="lblDate" runat="server" Text="Date"></cc1:ucLabel>
                                        </td>
                                        <td>
                                            :
                                        </td>
                                        <td>
                                            <cc1:ucDropdownList ID="ddlPurchaseOrderDate" runat="server" Width="150px" AutoPostBack="True"
                                                OnSelectedIndexChanged="ddlPurchaseOrderDate_SelectedIndexChanged">
                                                <asp:ListItem Text="--Select--" Value="0"></asp:ListItem>
                                            </cc1:ucDropdownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: bold;">
                                            <cc1:ucLabel ID="lblVendor" runat="server" Text="Vendor"></cc1:ucLabel>
                                        </td>
                                        <td>
                                            :
                                        </td>
                                        <td>
                                            <cc1:ucLabel ID="lblVendorValue" runat="server"></cc1:ucLabel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: bold;">
                                            <cc1:ucLabel ID="lblVendorVATReference" isRequired="true" runat="server" Text="Vendor VAT Reference"></cc1:ucLabel>
                                        </td>
                                        <td>
                                            :
                                        </td>
                                        <td>
                                            <cc1:ucTextbox ID="txtVendorVATReferenceValue" runat="server" Width="145px" MaxLength="20"></cc1:ucTextbox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: bold;">
                                            <cc1:ucLabel ID="lblInvoiceNumber" runat="server" Text="Invoice Number"></cc1:ucLabel>
                                        </td>
                                        <td>
                                            :
                                        </td>
                                        <td>
                                            <cc1:ucTextbox ID="txtInvoiceNo" runat="server" Width="145px" MaxLength="15"></cc1:ucTextbox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3" class="nobold radiobuttonlist">
                                            <cc1:ucRadioButtonList ID="rblPriceOther" CssClass="radio-fix" RepeatColumns="2"
                                                RepeatDirection="Horizontal" runat="server" AutoPostBack="True" OnSelectedIndexChanged="rblPriceOther_SelectedIndexChanged">
                                            </cc1:ucRadioButtonList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3">
                                            <asp:ValidationSummary ID="vSummary" runat="server" ShowMessageBox="true" ShowSummary="false"
                                                Style="color: Red" ValidationGroup="LogDiscrepancy" />
                                        </td>
                                    </tr>
                                </table>
                            </cc1:ucPanel>
                        </cc1:ucView>
                        <cc1:ucView ID="vwDebitRaiseByVendor" runat="server">
                            <cc1:ucPanel ID="pnlLogBasisOfVendor" runat="server" CssClass="fieldset-form">
                                <table width="40%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                                    <%--<tr>
                                        <td style="font-weight: bold; width: 40%">
                                            <cc1:ucLabel ID="UcLabel1" runat="server" Text="Site"></cc1:ucLabel>
                                        </td>
                                        <td style="width: 5%">
                                            :
                                        </td>
                                        <td style="width: 55%">
                                            <cc2:ucSite ID="ucSiteVendor" runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: bold; width: 40%">
                                            <cc1:ucLabel ID="lblVendor_1" runat="server" isRequired="true" Text="Vendor"></cc1:ucLabel>
                                        </td>
                                        <td style="width: 5%">
                                            :
                                        </td>
                                        <td style="width: 55%">
                                            <cc3:ucSeacrhVendor ID="ucSeacrhVendor1" runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: bold; width: 40%">
                                            <cc1:ucLabel ID="lblReferenceText" runat="server"></cc1:ucLabel>
                                        </td>
                                        <td style="width: 5%">
                                            :
                                        </td>
                                        <td style="width: 55%">
                                            <cc1:ucTextbox ID="txtReference" runat="server" Width="120px" MaxLength="15"></cc1:ucTextbox>
                                        </td>
                                    </tr>--%>
                                    <tr>
                                        <td style="width: 2%">
                                        </td>
                                        <td>
                                            <cc1:ucLabel ID="UcLabel1" runat="server" Text="Vendor" isRequired="true"></cc1:ucLabel>
                                        </td>
                                        <td>
                                            :
                                        </td>
                                        <td align="center" valign="middle">
                                            <table border="0">
                                                <tr>
                                                    <td style="width: 40%">
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <cc1:ucTextbox ID="txtVendorNo" runat="server" MaxLength="15" Width="170px"></cc1:ucTextbox>
                                                                </td>
                                                                <td>
                                                                    <cc1:ucButton CssClass="button" runat="server" ID="btnSearchVendor" OnClick="btnSearch_Click" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                    <td style="width: 30%;" align="center">
                                                    </td>
                                                    <td style="width: 30%">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <cc1:ucListBox ID="lstLeft" runat="server" Height="150px" Width="320px">
                                                        </cc1:ucListBox>
                                                    </td>
                                                    <td align="center">
                                                        <div>
                                                            <input type="button" id="btnMoveRight" value=">" class="button" style="width: 35px"
                                                                onclick="Javascript:ShowSelectedVendorName('Local');" /></div>
                                                        &nbsp;
                                                        <div>
                                                            <input type="button" id="btnMoveLeft" value="<" class="button" style="width: 35px"
                                                                onclick="Javascript:RemoveSelectedVendorName('Local');" /></div>
                                                    </td>
                                                    <td>
                                                        &nbsp;
                                                    </td>
                                                </tr>
                                            </table>
                                            <asp:CustomValidator ID="cusvNoSelectedVendor" runat="server" ClientValidationFunction="checkVendorSelected"
                                                ValidationGroup="CheckVendor" Display="None"></asp:CustomValidator>
                                        </td>
                                        <td align="center" valign="middle">
                                            <div style="margin-top: 20px;">
                                                <cc1:ucTextbox ID="txtSelectedVendorValue" runat="server" Font-Bold="true" ReadOnly="true"></cc1:ucTextbox>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                                <br />
                            </cc1:ucPanel>
                        </cc1:ucView>
                        <cc1:ucView ID="vwDebitRaiseByPODetail" runat="server">
                            <table width="100%" cellspacing="0" cellpadding="0" class="form-table">
                                <tr valign="top">
                                    <td width="30%">
                                        <cc1:ucPanel ID="pnlVendor_2" runat="server" CssClass="fieldset-form">
                                            <table width="100%" cellspacing="1" cellpadding="0" class="form-table" style="height: 90px">
                                                <tr>
                                                    <td style="font-weight: bold; width: 30%">
                                                        <cc1:ucLabel ID="lblVendor_2" runat="server" Text="Vendor "></cc1:ucLabel>
                                                    </td>
                                                    <td style="font-weight: bold; width: 2%">
                                                        :
                                                    </td>
                                                    <td class="nobold" style="width: 68%">
                                                        <cc1:ucLabel ID="lblVendorNumberValue_PO" runat="server"></cc1:ucLabel>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="font-weight: bold;">
                                                        <cc1:ucLabel ID="lblContactNo" runat="server" Text="Contact #"></cc1:ucLabel>
                                                    </td>
                                                    <td style="font-weight: bold;">
                                                        :
                                                    </td>
                                                    <td class="nobold">
                                                        <cc1:ucLabel ID="lblContactValue_PO" runat="server"></cc1:ucLabel>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="font-weight: bold;">
                                                        <cc1:ucLabel ID="lblVendorVATHash" runat="server" Text="Vendor VAT #"></cc1:ucLabel>
                                                    </td>
                                                    <td style="font-weight: bold;">
                                                        :
                                                    </td>
                                                    <td class="nobold">
                                                        <cc1:ucLabel ID="lblVendorVATHashValue" runat="server"></cc1:ucLabel>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="3">
                                                    </td>
                                                </tr>
                                            </table>
                                        </cc1:ucPanel>
                                    </td>
                                    <td width="2%">
                                        &nbsp;
                                    </td>
                                    <td width="30%">
                                        <cc1:ucPanel ID="pnlDebitReference" runat="server" GroupingText="Debit Reference"
                                            CssClass="fieldset-form">
                                            <table width="100%" cellspacing="1" cellpadding="0" class="form-table" style="height: 90px">
                                                <tr>
                                                    <td style="font-weight: bold; width: 48%">
                                                        <cc1:ucLabel ID="lblSiteDetail" runat="server"></cc1:ucLabel>
                                                    </td>
                                                    <td style="font-weight: bold; width: 2%">
                                                        :
                                                    </td>
                                                    <td class="nobold" style="width: 50%" colspan="2">
                                                        <cc1:ucLabel ID="lblSiteValue" runat="server" Text="Asthon"></cc1:ucLabel>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="font-weight: bold;">
                                                        <cc1:ucLabel ID="lblPurchaseOrderNo" runat="server"></cc1:ucLabel>
                                                    </td>
                                                    <td style="font-weight: bold;">
                                                        :
                                                    </td>
                                                    <td class="nobold" style="width: 20%">
                                                        <cc1:ucLabel ID="lblPurchaseOrderValue" runat="server" Text="42116"></cc1:ucLabel>
                                                    </td>
                                                    <td class="nobold" style="width: 30%">
                                                        <cc1:ucLabel ID="lblPurchaseOrderDateValue" runat="server"></cc1:ucLabel>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="font-weight: bold;">
                                                        <cc1:ucLabel ID="lblInvoiceNumber_1" runat="server"></cc1:ucLabel>
                                                    </td>
                                                    <td style="font-weight: bold;">
                                                        :
                                                    </td>
                                                    <td class="nobold" colspan="2">
                                                        <cc1:ucLabel ID="lblInvoiceNoValue" runat="server" Text="I01234"></cc1:ucLabel>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="font-weight: bold;">
                                                        <cc1:ucLabel ID="lblOfficeDepotVATHash" runat="server" Text="Office Depot VAT #"></cc1:ucLabel>
                                                    </td>
                                                    <td style="font-weight: bold;">
                                                        :
                                                    </td>
                                                    <td class="nobold" colspan="2">
                                                        <cc1:ucLabel ID="lblOfficeDepotVATHashValue" runat="server"></cc1:ucLabel>
                                                    </td>
                                                </tr>
                                            </table>
                                        </cc1:ucPanel>
                                    </td>
                                    <td width="2%">
                                        &nbsp;
                                    </td>
                                    <td width="46%" rowspan="5" valign="top">
                                        <cc1:ucPanel ID="pnlCommunicationDetail" runat="server" GroupingText="Communication Detail"
                                            CssClass="fieldset-form">
                                            <table width="100%" cellspacing="0" cellpadding="0" class="form-table" style="height: 90px">
                                                <tr>
                                                    <td align="left" valign="top">
                                                        <uc2:ucSDRCommunication ID="ucSDRCommunication1" runat="server" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </cc1:ucPanel>
                                    </td>
                                </tr>
                            </table>
                            <cc1:ucGridView ID="gvLogInvoice" Width="100%" runat="server" CssClass="grid" OnRowDataBound="gvLogInvoice_RowDataBound">
                                <Columns>
                                    <asp:TemplateField HeaderText="Line" SortExpression="PurchaseOrder.Line_No">
                                        <HeaderStyle Width="6%" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="lblLineNo" Text='<%# Eval("PurchaseOrder.Line_No") %>'></cc1:ucLabel>
                                            <asp:HiddenField ID="hdnPurchaseOrderID" runat="server" Value='<%#Eval("PurchaseOrder.PurchaseOrderID") %>' />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Office Depot Code" SortExpression="PurchaseOrder.OD_Code">
                                        <HeaderStyle Width="8%" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="lblOfficeDepotCodeValue" Text='<%# Eval("PurchaseOrder.OD_Code") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Viking Code" SortExpression="PurchaseOrder.Direct_code">
                                        <HeaderStyle Width="8%" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="lblVikingCodeValue" Text='<%# Eval("PurchaseOrder.Direct_code") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Vendor Item Code" SortExpression="PurchaseOrder.Vendor_Code">
                                        <HeaderStyle Width="8%" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="lblVendorItemCodeValue" Text='<%# Eval("PurchaseOrder.Vendor_Code") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Description" SortExpression="PurchaseOrder.ProductDescription">
                                        <HeaderStyle Width="22%" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="lblDescriptionValue" Text='<%# Eval("PurchaseOrder.ProductDescription") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Original PO Quantity" SortExpression="PurchaseOrder.Original_quantity">
                                        <HeaderStyle Width="7%" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="lblOriginalPOQuantity1" Text='<%# Eval("PurchaseOrder.Original_quantity") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Outstanding PO Quantity" SortExpression="PurchaseOrder.Outstanding_Qty">
                                        <HeaderStyle Width="7%" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="lblOutstandingQty1" Text='<%# Eval("PurchaseOrder.Outstanding_Qty") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="UOM" SortExpression="PurchaseOrder.UOM">
                                        <HeaderStyle Width="5%" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="lblUoM1" Text='<%# Eval("PurchaseOrder.UOM") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="PO Item Cost" SortExpression="PurchaseOrder.PO_cost">
                                        <HeaderStyle Width="7%" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <asp:Label ID="lblPOItemCost" Text='<%# Eval("PurchaseOrder.PO_cost") %>' runat="server"></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="PO Total Cost" SortExpression="PurchaseOrder.PO_Totalcost">
                                        <HeaderStyle Width="7%" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="lblTotalCost" Text='<%# Eval("PurchaseOrder.PO_Totalcost") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <%-- Other Option Columns, Index 10,11 --%>
                                    <asp:TemplateField HeaderText="Value To be Debited">
                                        <HeaderStyle Width="5%" HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <cc1:ucTextbox ID="txtValueDebited" runat="server" Width="50px" MaxLength="6" onkeyup="AllowDecimalOnly(this);"></cc1:ucTextbox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Reason for Debit">
                                        <HeaderStyle Width="10%" HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <cc1:ucDropdownList ID="ddlReason" runat="server" Width="100px">
                                            </cc1:ucDropdownList>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <%-- Price Query Columns, Index 12,13,14 --%>
                                    <asp:TemplateField HeaderText="InvoicedItemPrice">
                                        <HeaderStyle Width="5%" HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtInvoicedItemPriceValue" runat="server" Width="50px" MaxLength="6"></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="QtyInvoiced">
                                        <HeaderStyle Width="5%" HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtQtyInvoicedValue" runat="server" Width="50px" MaxLength="6"></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="PriceDifference">
                                        <HeaderStyle Width="5%" HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <asp:Label ID="lblPriceDifferenceValue" CssClass="clsPriceDiff" runat="server"></asp:Label>
                                            <asp:HiddenField ID="hdnPriceDifferenceValue" runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </cc1:ucGridView>
                            <table width="100%">
                                <tr style="height: 20px">
                                    <td align="right">
                                        <cc1:ucButton ID="btnAddLine" runat="server" Text="Add" CssClass="button" Visible="false" />
                                    </td>
                                </tr>
                            </table>
                            <cc1:ucGridView ID="gvLogInvoiceAddLine" runat="server" AutoGenerateColumns="False"
                                CellPadding="0" CssClass="grid" GridLines="None" Width="100%" OnRowDataBound="gvLogInvoiceAddLine_RowDataBound"
                                OnRowCommand="gvLogInvoiceAddLine_RowCommand">
                                <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                                <Columns>
                                    <asp:TemplateField HeaderText="Line">
                                        <HeaderStyle Width="6%" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucTextbox runat="server" ID="txtLineNo" Text='<%# Eval("Line_No") %>' Width="95%"
                                                EnableViewState="true" MaxLength="3" onkeyup="AllowNumbersOnly(this);" AutoPostBack="true"
                                                OnTextChanged="TextBox_TextChanged_txtLineNo"></cc1:ucTextbox>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Office Depot Code">
                                        <HeaderStyle Width="8%" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucTextbox runat="server" ID="txtOfficeDepotCode" Text='<%# Eval("OD_Code") %>'
                                                Width="95%" AutoPostBack="true" MaxLength="10" OnTextChanged="TextBox_TextChanged_txtOfficeDepotCode">
                                            </cc1:ucTextbox>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Viking Code">
                                        <HeaderStyle Width="8%" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucTextbox runat="server" ID="txtVikingCode" Text='<%# Eval("Direct_code") %>'
                                                MaxLength="10" Width="95%"></cc1:ucTextbox>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Vendor Item Code">
                                        <HeaderStyle Width="8%" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucTextbox runat="server" ID="txtVendorItemCode" Text='<%# Eval("Vendor_Code") %>'
                                                MaxLength="10" Width="95%"></cc1:ucTextbox>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Description">
                                        <HeaderStyle Width="22%" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucTextbox runat="server" ID="txtDescription" Text='<%# Eval("ProductDescription") %>'
                                                MaxLength="250" Width="98%"></cc1:ucTextbox>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Original PO Quantity">
                                        <HeaderStyle Width="7%" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucTextbox runat="server" ID="txtOriginalPOQuantity" Text='<%# Eval("Original_quantity") %>'
                                                Width="5em" AutoPostBack="true" OnTextChanged="TextBox_TextChanged_txtOriginalPOQuantity"></cc1:ucTextbox>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Outstanding PO Quantity">
                                        <HeaderStyle Width="7%" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucTextbox runat="server" ID="txtOutstandingQty" Text='<%# Eval("Outstanding_Qty") %>'
                                                Width="5em" onkeyup="AllowDecimalOnly(this);" MaxLength="6" />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="UOM">
                                        <HeaderStyle Width="5%" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucTextbox runat="server" ID="txtUoM" Text='<%# Eval("UOM") %>' Width="92%" MaxLength="5"></cc1:ucTextbox>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="PO Item Cost">
                                        <HeaderStyle Width="7%" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucTextbox runat="server" ID="txtPOItemCost" MaxLength="6" Width="95%" Text='<%#Eval("PO_cost") %>'
                                                AutoPostBack="true" OnTextChanged="TextBox_TextChanged_txtPOItemCost"></cc1:ucTextbox>
                                            <%-- onkeyup="AllowDecimalOnly(this);"--%>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="PO Total Cost">
                                        <HeaderStyle Width="7%" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucTextbox runat="server" ID="txtPOTotalCost" MaxLength="6" onkeyup="AllowDecimalOnly(this);"
                                                Width="95%" Text='<%#Eval("PO_Totalcost") %>' Enabled="false"></cc1:ucTextbox>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Value To be Debited">
                                        <HeaderStyle Width="5%" HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <cc1:ucTextbox runat="server" ID="txtValueDebited" MaxLength="6" onkeyup="AllowDecimalOnly(this);"
                                                Width="95%" Text='<%#Eval("ValueDebited") %>'></cc1:ucTextbox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Reason for Debit">
                                        <HeaderStyle Width="5%" HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <cc1:ucDropdownList ID="ddlReason" runat="server" Width="100px">
                                                <%--  <asp:ListItem Text="--Select--" Value="0"></asp:ListItem>
                                        <asp:ListItem Text="Reason 1" Value="1"></asp:ListItem>
                                        <asp:ListItem Text="Reason 2" Value="2"></asp:ListItem>--%>
                                            </cc1:ucDropdownList>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderStyle Width="5%" HorizontalAlign="right" />
                                        <ItemTemplate>
                                            <cc1:ucButton ID="btnAddLine" runat="server" Text="Add Line" CommandArgument='<%#Eval("RecordID") %>'
                                                CommandName="AddLine" OnCommand="AddLine_Click" CssClass="button" />
                                            <cc1:ucButton ID="btnRemove" runat="server" Text="Remove" CommandArgument='<%#Eval("RecordID") %>'
                                                CommandName="Remove" OnCommand="RemoveImage_Click" CssClass="button" OnClientClick="return ConfirmMess(this);"
                                                Style="display: none" />
                                            <asp:HiddenField ID="hdnRecordID" runat="server" Value='<%#Eval("RecordID") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                            </cc1:ucGridView>
                            <cc1:ucGridView ID="gvLogInvoiceViewMode" runat="server" AutoGenerateColumns="False"
                                CellPadding="0" CssClass="grid" GridLines="None" Width="100%" Visible="false">
                                <%--OnRowDataBound="gvLogInvoiceAddLine_RowDataBound">--%>
                                <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                                <Columns>
                                    <asp:TemplateField HeaderText="Line">
                                        <HeaderStyle Width="6%" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="lblLineNo2" Text='<%# Eval("Line_no") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Office Depot Code">
                                        <HeaderStyle Width="8%" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="lblOfficeDepotCode2" Text='<%# Eval("OD_Code") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Viking Code">
                                        <HeaderStyle Width="8%" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="lblVikingCode2" Text='<%# Eval("Direct_code") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Vendor Item Code">
                                        <HeaderStyle Width="8%" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel ID="lblVendorItemCode1" runat="server" Text='<%# Eval("Vendor_Code") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Description">
                                        <HeaderStyle Width="22%" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="lblDescription2" Text='<%# Eval("ProductDescription") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Original PO Quantity">
                                        <HeaderStyle Width="7%" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="lblOriginalPOQuantity2" Text='<%# Eval("OriginalQuantity") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Outstanding PO Quantity">
                                        <HeaderStyle Width="7%" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="lblOutstandingQty2" Text='<%# Eval("Outstanding_Qty") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="UOM">
                                        <HeaderStyle Width="5%" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="lblUoM2" Text='<%# Eval("UOM") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="PO Item Cost">
                                        <HeaderStyle Width="7%" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="lblPOItemCost2" Text='<%# Eval("PO_cost") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="PO Total Cost">
                                        <HeaderStyle Width="7%" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="lblPOTotalCost2" Text='<%# Eval("PO_Totalcost") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <%-- Other Option Columns, Index 10,11 --%>
                                    <asp:TemplateField HeaderText="Value To be Debited">
                                        <HeaderStyle Width="5%" HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="lblValueDebited2" Text='<%# Eval("ValueDebited") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Reason for Debit">
                                        <HeaderStyle Width="5%" HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="lblReason2" Text='<%# Eval("Reason") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <%-- Price Query Columns, Index 12,13,14 --%>
                                    <asp:TemplateField HeaderText="InvoicedItemPrice">
                                        <HeaderStyle Width="5%" HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="lblInvoicedItemPriceValue" Text='<%# Eval("InvoicedItemPrice") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="QtyInvoiced">
                                        <HeaderStyle Width="5%" HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="lblQtyInvoicedValue" Text='<%# Eval("QtyInvoiced") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="PriceDifference">
                                        <HeaderStyle Width="5%" HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="lblPriceDifferenceValue" Text='<%# Eval("PriceDifference") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                            </cc1:ucGridView>
                            <%--<br />--%>
                            <table width="100%" cellspacing="1" cellpadding="0" class="form-table">
                                <tr>
                                    <td style="width: 16%; font-weight: bold;" align="left">
                                        <cc1:ucLabel ID="lblPleaseSelectCurrency_1" isRequired="true" runat="server"></cc1:ucLabel>
                                    </td>
                                    <td style="width: 1%; font-weight: bold;" align="center">
                                        :
                                    </td>
                                    <td style="width: 16%" align="left">
                                        <cc1:ucDropdownList ID="ddlCurrencyPO" runat="server" Width="150px">
                                        </cc1:ucDropdownList>
                                    </td>
                                    <td style="width: 16%; font-weight: bold;" align="right">
                                        <cc1:ucLabel ID="lblDebitValue" Text="Debit Value" isRequired="true" runat="server"></cc1:ucLabel>
                                    </td>
                                    <td style="width: 1%; font-weight: bold;" align="center">
                                        <cc1:ucLabel ID="lblDebitValueColon" Text=":" runat="server"></cc1:ucLabel>
                                    </td>
                                    <td style="width: 16%" align="left">
                                        <cc1:ucTextbox runat="server" ID="txtDebitValueText" Enabled="false" MaxLength="6"
                                            onkeyup="AllowDecimalOnly(this);" Width="100px"></cc1:ucTextbox>
                                    </td>
                                    <td style="width: 16%; font-weight: bold;" align="left">
                                    </td>
                                    <td style="width: 1%; font-weight: bold;" align="left">
                                    </td>
                                    <td style="width: 16%" align="left">
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="9" align="left">
                                        <cc1:ucPanel ID="pnlPleaseenterComment" GroupingText=" Please enter Comment" runat="server"
                                            CssClass="fieldset-form">
                                            <table width="100%" cellspacing="5" cellpadding="0" border="0" class="form-table">
                                                <tr>
                                                    <td style="font-weight: bold;">
                                                        <cc1:ucTextarea ID="txtPOCommnets" CssClass="inputbox" runat="server" onkeyup="checkTextLengthOnKeyUp(this,1000);"
                                                            Width="98%" Height="60px"></cc1:ucTextarea>
                                                    </td>
                                                </tr>
                                            </table>
                                        </cc1:ucPanel>
                                    </td>
                                </tr>
                            </table>
                        </cc1:ucView>
                        <cc1:ucView ID="vwDebitRaiseByVendorDetail" runat="server">
                            <table width="100%" cellspacing="0" cellpadding="0" class="form-table">
                                <tr valign="top">
                                    <td width="20%">
                                        <cc1:ucPanel ID="pnlVendor_1" runat="server" CssClass="fieldset-form">
                                            <table width="100%" cellspacing="1" cellpadding="0" class="form-table" style="height: 90px">
                                                <tr>
                                                    <td style="font-weight: bold; width: 30%">
                                                        <cc1:ucLabel ID="lblVendor_3" runat="server" Text="Vendor "></cc1:ucLabel>
                                                    </td>
                                                    <td style="font-weight: bold; width: 2%">
                                                        :
                                                    </td>
                                                    <td class="nobold" style="width: 68%">
                                                        <cc1:ucLabel ID="lblVendorNumberValue_Vendor" runat="server" Text="025 - ACCO"></cc1:ucLabel>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="font-weight: bold;">
                                                        <%--<cc1:ucLabel ID="lblContactNo_1" runat="server" Text="Contact #"></cc1:ucLabel>--%>
                                                    </td>
                                                    <td style="font-weight: bold;">
                                                        <%--:--%>
                                                    </td>
                                                    <td class="nobold">
                                                        <%--<cc1:ucLabel ID="lblContactValue_Vendor" runat="server" Text="01264 347725"></cc1:ucLabel>--%>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="3">
                                                    </td>
                                                </tr>
                                            </table>
                                        </cc1:ucPanel>
                                    </td>
                                    <%--<td width="2%">
                                        &nbsp;
                                    </td>
                                    <td width="25%">--%>
                                    <cc1:ucPanel ID="pnlDebitReference_1" runat="server" CssClass="fieldset-form" Visible="false">
                                        <table width="100%" cellspacing="0" cellpadding="0" class="form-table" style="height: 90px">
                                            <tr>
                                                <td style="font-weight: bold; width: 30%">
                                                    <cc1:ucLabel ID="lblReferenceText_1" runat="server"></cc1:ucLabel>
                                                </td>
                                                <td style="font-weight: bold; width: 2%">
                                                    :
                                                </td>
                                                <td class="nobold" style="width: 68%">
                                                    <cc1:ucLabel ID="lblReferenceValue_Vendor" runat="server" Text="Ref1234"></cc1:ucLabel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="font-weight: bold;">
                                                </td>
                                                <td style="font-weight: bold;">
                                                </td>
                                                <td class="nobold">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="3">
                                                </td>
                                            </tr>
                                        </table>
                                    </cc1:ucPanel>
                                    <%--</td>--%>
                                    <td width="2%">
                                        &nbsp;
                                    </td>
                                    <td width="51%" rowspan="5" valign="top">
                                        <cc1:ucPanel ID="pnlCommunicationDetail_1" runat="server" GroupingText="Communication Detail"
                                            CssClass="fieldset-form">
                                            <table width="100%" cellspacing="0" cellpadding="0" class="form-table" style="height: 90px">
                                                <tr>
                                                    <td>
                                                        <uc2:ucSDRCommunication ID="ucSDRCommunication2" runat="server" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </cc1:ucPanel>
                                    </td>
                                </tr>
                            </table>
                            <cc1:ucPanel ID="pnlDebitDetails" runat="server" CssClass="fieldset-form">
                                <table width="100%" cellspacing="0" cellpadding="0" class="form-table" border="0">
                                    <tr>
                                        <td style="width: 20%" align="center">
                                            <cc1:ucLabel ID="lblVendorVATReference_1" isRequired="true" Text="Vendor VAT Reference"
                                                runat="server"></cc1:ucLabel>
                                        </td>
                                        <td style="width: 15%" align="left">
                                            <cc1:ucTextbox ID="txtVendVendorVATReference" runat="server" Width="143px"></cc1:ucTextbox>
                                        </td>
                                        <td style="width: 25%;" align="left">
                                            <div style="margin-left: 15px;">
                                                <cc1:ucLabel ID="lblOfficeDepotVATReference" isRequired="true" Text="Office Depot VAT Reference"
                                                    runat="server"></cc1:ucLabel>
                                            </div>
                                        </td>
                                        <td style="width: 10%" align="left">
                                            <cc1:ucDropdownList ID="ddlVendOfficeDepotVATReference" runat="server" Width="150px">
                                            </cc1:ucDropdownList>
                                        </td>
                                        <td style="width: 15%" align="center">
                                        </td>
                                        <td style="width: 15%" align="left">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 20%" align="center">
                                            <cc1:ucLabel ID="lblPleaseSelectCurrency" isRequired="true" runat="server"></cc1:ucLabel>
                                        </td>
                                        <td style="width: 15%" align="left">
                                            <cc1:ucDropdownList ID="ddlCurrency" runat="server" Width="150px">
                                            </cc1:ucDropdownList>
                                        </td>
                                        <td style="width: 25%;" align="left">
                                            <div style="margin-left: 15px;">
                                                <cc1:ucLabel ID="lblPleaseStateValuetobeDebited" isRequired="true" runat="server"></cc1:ucLabel>
                                            </div>
                                        </td>
                                        <td style="width: 10%" align="left">
                                            <cc1:ucTextbox ID="txtValueTobeDebited" runat="server" Width="143px" MaxLength="6" onkeyup="AllowDecimalOnly(this);"></cc1:ucTextbox>
                                        </td>
                                        <td style="width: 15%" align="center">
                                            <cc1:ucLabel ID="lblReasonForDebit" isRequired="true" runat="server"></cc1:ucLabel>
                                        </td>
                                        <td style="width: 15%" align="left">
                                            <cc1:ucDropdownList ID="ddlReason" runat="server" Width="150px">
                                            </cc1:ucDropdownList>
                                        </td>
                                    </tr>
                                </table>
                            </cc1:ucPanel>
                            <br />
                            <cc1:ucPanel ID="pnlPleaseenterComment_1" GroupingText=" Please enter Comment" runat="server"
                                CssClass="fieldset-form">
                                <table width="100%" cellspacing="5" cellpadding="0" border="0" class="form-table">
                                    <tr>
                                        <td style="font-weight: bold;">
                                            <cc1:ucTextarea ID="txtVendorComment" CssClass="inputbox" runat="server" onkeyup="checkTextLengthOnKeyUp(this,1000);"
                                                Height="60px" Width="98%"></cc1:ucTextarea>
                                        </td>
                                    </tr>
                                </table>
                            </cc1:ucPanel>
                        </cc1:ucView>
                    </cc1:ucMultiView>
                </div>
            </div>
            <asp:Button ID="btnViewNoPurchaseOrder" runat="Server" Style="display: none" />
            <ajaxToolkit:ModalPopupExtender ID="mdlInvoiceViewer" runat="server" TargetControlID="btnViewNoPurchaseOrder"
                PopupControlID="pnlbtnViewNoPurchaseOrderViewer" BackgroundCssClass="modalBackground"
                BehaviorID="NoPurchaseOrderViewer" DropShadow="false" />
            <asp:Panel ID="pnlbtnViewNoPurchaseOrderViewer" runat="server" Style="display: none;
                width: 40%; height: 10%;">
                <div style="width: 100%; height: 100%; overflow-y: hidden; overflow-x: hidden; background-color: #fff;
                    padding: 5px; border: 2px solid #ccc; text-align: left;">
                    <table width="100%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                        <tr>
                            <td style="font-weight: bold;" width="100%">
                                <cc1:ucLabel ID="lblInvalidProductCode" runat="server"></cc1:ucLabel>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <cc1:ucButton ID="btnYes" Width="60px" class="button" runat="server" OnClientClick="javascript:$find('NoPaperWorkViewer').hide();return false;"
                                    OnClick="btnOk_Click" />
                                &nbsp;
                                <cc1:ucButton ID="btnNo" Width="60px" class="button" runat="server" OnClick="btnCancel_Click" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
            <div class="button-row">
                <cc1:ucButton ID="btnProceed" runat="server" Text="Proceed" class="button" OnClick="btnProceed_Click" />
                <cc1:ucButton ID="btnConfirm" runat="server" Text="Confirm" class="button" OnClick="btnConfirm_Click"
                    Visible="false" />
                <cc1:ucButton ID="btnSave" runat="server" Text="Save" class="button" OnClick="btnSave_Click"
                    Visible="false" />
                <%--UseSubmitBehavior="false"--%>
                <cc1:ucButton ID="btnBack" runat="server" Text="Back" class="button" OnClick="btnBack_Click"
                    Visible="false" />
            </div>
            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td align="center">
                        <%--   <asp:UpdateProgress runat="server" ID="PageUpdateProgress" AssociatedUpdatePanelID="up1">
                            <ProgressTemplate>
                                <div align="center" style="background-color: #000; top: 0px; left: 0px; bottom: 0px;
                                    right: 0px; padding-top: 20%; margin: 0; width: 100%; height: 130%; overflow: hidden;
                                    position: absolute; z-index: 1000; filter: alpha(opacity=50); opacity: 0.5;">
                                    <asp:Image ID="imgWait" runat="server" ImageUrl="~/Images/Ajaxloading.gif" ImageAlign="Middle" />
                                </div>
                            </ProgressTemplate>
                        </asp:UpdateProgress>--%>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
        <Triggers>
            <%--  <asp:AsyncPostBackTrigger ControlID="btnProceed" />
            <asp:AsyncPostBackTrigger ControlID="btnConfirm" />
            <asp:AsyncPostBackTrigger ControlID="btnSave" />
            <asp:PostBackTrigger ControlID="btnBack" />--%>
        </Triggers>
    </asp:UpdatePanel>
    <script type="text/javascript">

        function SetPOPriceDiff(lblPOItemCostID, txtQtyInvoicedValueID, txtInvoicedItemPriceValueID, lblPriceDifferenceValueID, hdnPriceDifferenceValueID, txtDebitValueTextID) {
            var POItemCostValue = $('#' + lblPOItemCostID).html();
            var InvoiceQtyValue = $('#' + txtQtyInvoicedValueID).val();
            var InvoiceItemPriceValue = $('#' + txtInvoicedItemPriceValueID).val();
            var $lblPriceDifferenceValue = $('#' + lblPriceDifferenceValueID);
            var $hdnPriceDifferenceValue = $('#' + hdnPriceDifferenceValueID);
            var $DebitValue = $('#' + txtDebitValueTextID);
            var jsonText = JSON.stringify({ InvoiceQty: InvoiceQtyValue, POItemCost: POItemCostValue, InvoiceItemPrice: InvoiceItemPriceValue });

            $.ajax({
                type: "POST",
                url: "DebitRaise.aspx/CalculatePOPriceDiff",
                data: jsonText,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    $lblPriceDifferenceValue.html(msg.d);
                    if ($('#' + lblPriceDifferenceValueID).html().length > 8) {
                        $lblPriceDifferenceValue.html('');
                        alert(msg.d);
                    }
                    else {
                        $lblPriceDifferenceValue.html(msg.d);
                        $hdnPriceDifferenceValue.val(msg.d);
                    }
                    
                    var elems = $(".clsPriceDiff");
                    var debitValue = 0;
                    for (var i = 0; i < elems.length; i++) {
                        if (elems[i].innerHTML != '') {
                            debitValue = parseFloat(debitValue) + parseFloat(elems[i].innerHTML);
                        }
                    }
                    $DebitValue.val(debitValue.toFixed(2));
                }
            });
        }        
    </script>
</asp:Content>

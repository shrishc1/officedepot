﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using BaseControlLibrary;
using BusinessEntities.ModuleBE.Discrepancy.APAction;
using BusinessEntities.ModuleBE.Discrepancy.LogDiscrepancy;
using BusinessLogicLayer.ModuleBAL.Discrepancy.CountrySetting;
using BusinessLogicLayer.ModuleBAL.Discrepancy.LogDiscrepancy;
using Utilities;
using WebUtilities;
using BusinessLogicLayer.ModuleBAL.Appointment.Booking;
using BusinessEntities.ModuleBE.Languages.Languages;
using System.IO;
using System.Configuration;

public partial class AccountsPayable_CancelDebit : CommonPage
{
    string PleaseselectoneitemtoCancelaDebit = WebCommon.getGlobalResourceValue("PleaseselectoneitemtoCancelaDebit");
    string PleaseselectaReasonForCancel = WebCommon.getGlobalResourceValue("PleaseselectaReasonForCancel");
    string DebitNoteCancellationEmailSubject = WebCommon.getGlobalResourceValue("DebitNoteCancellationEmailSubject");

    #region Events ...

    protected void Page_Load(object sender, EventArgs e)
    {
        ucExportToExcel1.CurrentPage = this;
        ucExportToExcel1.FileName = "CancelDebit";
        btnBack_1.Visible = false;
        ucExportToExcel1.GridViewControl = gvCancelDebitExport;       
        if (!IsPostBack)
        {            
            this.BindCancleReason();
            pnlSearchDebit1.Visible = true;
            pnlDebitLibrary1.Visible = false;
            ucExportToExcel1.Visible = false;
            btnCancel.Visible = false;
            btnBack_1.Visible = false;
        }

        msDebitNoteNumber.SetDebitNoteNumberOnPostBack();
        msAP.SetAPOnPostBack();
        msDebitType.SetDebitTypeOnPostBack();
        msPurchaseOrderNumber.SetPOOnPostBack();
        msVendor.setVendorsOnPostBack();
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        ViewState["hlDebitNoteNoValue"] = null;
        this.BindCancelaDebits();       
        btnBack_1.Visible = true;
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        bool checkedStatus = false;
        for (int index = 0; index < gvCancelDebit.Rows.Count; index++)
        {
            var rblSelect = (RadioButton)gvCancelDebit.Rows[index].FindControl("rblSelect");
            var lblVendorIDValue = (ucLabel)gvCancelDebit.Rows[index].FindControl("lblVendorIDValue");
            var lblDebitRaiseIdValue = (ucLabel)gvCancelDebit.Rows[index].FindControl("lblDebitRaiseIdValue");
            var lblDiscrepancyLogIDValue = (ucLabel)gvCancelDebit.Rows[index].FindControl("lblDiscrepancyLogIDValue");
            if (rblSelect.Checked)
            {
                checkedStatus = true;
                hdnVendorIdVal.Value = lblVendorIDValue.Text;
                hdnDebitRaiseIdVal.Value = lblDebitRaiseIdValue.Text;
                hdnDiscrepancyLogIDValue.Value = lblDebitRaiseIdValue.Text;
                break;
            }
        }

        if (checkedStatus)
        {
            lblCancelledByValue.Text = string.Empty;
            lblCancelledOnValue.Text = string.Empty;
            txtCommentToVendor.Text = string.Empty;
            if (ddlReason.Items.Count > 0)
                ddlReason.SelectedIndex = 0;

            lblCancelledByValue.Text = Convert.ToString(Session["UserName"]);
            lblCancelledOnValue.Text = Common.ToDateTimeInMMDDYYYY(DateTime.Now.Date);
            ucSDRCommunication1.FillAPContacts(Convert.ToInt32(hdnVendorIdVal.Value));
            ddlReason.Focus();
            mdlDebitCancel.Show();
        }
        else
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + PleaseselectoneitemtoCancelaDebit + "')", true);
            return;
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (ddlReason.Items.Count.Equals(0))
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + PleaseselectaReasonForCancel + "')", true);
            mdlDebitCancel.Show();
            ddlReason.Focus();
            return;
        }

        if (ddlReason.SelectedIndex.Equals(0))
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + PleaseselectaReasonForCancel + "')", true);
            mdlDebitCancel.Show();
            ddlReason.Focus();
            return;
        }

        var discrepancyBE = new DiscrepancyBE();
        var discrepancyBAL = new DiscrepancyBAL();
        discrepancyBE.Action = "CancelDebit";
        discrepancyBE.CancelById = Convert.ToInt32(Session["UserID"]);
        discrepancyBE.CancelDebitReasonId = Convert.ToInt32(ddlReason.SelectedItem.Value);

        var txtCommEmailList = (ucTextbox)ucSDRCommunication1.FindControl("txtVendorEmailList");
        var txtAdditionalEmailList = (ucTextbox)ucSDRCommunication1.FindControl("txtAdditionalEmailList");
        var emailIds = string.Empty;
        if (txtCommEmailList != null && txtAdditionalEmailList != null)
        {
            if ((!string.IsNullOrEmpty(txtCommEmailList.Text) && !string.IsNullOrWhiteSpace(txtCommEmailList.Text)) &&
                (!string.IsNullOrEmpty(txtAdditionalEmailList.Text) && !string.IsNullOrWhiteSpace(txtAdditionalEmailList.Text)))
            {
                emailIds = string.Format("{0},{1}", txtCommEmailList.Text, txtAdditionalEmailList.Text);
            }
            else if ((!string.IsNullOrEmpty(txtCommEmailList.Text) && !string.IsNullOrWhiteSpace(txtCommEmailList.Text)))
            {
                emailIds = txtCommEmailList.Text;
            }
            else if ((!string.IsNullOrEmpty(txtAdditionalEmailList.Text) && !string.IsNullOrWhiteSpace(txtAdditionalEmailList.Text)))
            {
                emailIds = txtAdditionalEmailList.Text;
            }
        }

        discrepancyBE.CancelCommunicationEmails = emailIds;
        discrepancyBE.CancelVendorComments = txtCommentToVendor.Text.Trim();
        discrepancyBE.DebitRaiseId = Convert.ToInt32(hdnDebitRaiseIdVal.Value);
        var cancelDebitRaiseId = discrepancyBAL.CancelDebitBAL(discrepancyBE);
        if (cancelDebitRaiseId > 0)
        {
            discrepancyBE = new DiscrepancyBE();
            discrepancyBE.Action = "GetAllDebitRaise";
            discrepancyBE.DebitRaiseId = cancelDebitRaiseId;
            var lstDiscrepancyBE = discrepancyBAL.GetAllDebitRaiseBAL(discrepancyBE);
            if (lstDiscrepancyBE != null && lstDiscrepancyBE.Count > 0)
            {
                ViewState["DebitNoteNo"] = lstDiscrepancyBE[0].DebitNoteNo;
                //DebitRaisedSuccessfully = DebitRaisedSuccessfully.Replace("##RaisedDebitNumber##", lstDiscrepancyBE[0].DebitNoteNo);
                DebitNoteCancellationEmailSubject = DebitNoteCancellationEmailSubject.Replace("##DebitNoteNumber##", lstDiscrepancyBE[0].DebitNoteNo);

                #region Logic to Save & Send email ...
                var oAPPBOK_CommunicationBAL = new APPBOK_CommunicationBAL();
                var oLanguages = oAPPBOK_CommunicationBAL.GetLanguages();
                foreach (MAS_LanguageBE objLanguage in oLanguages)
                {
                    bool MailSentInLanguage = false;
                    if (objLanguage.LanguageID.Equals(lstDiscrepancyBE[0].Vendor.LanguageID))
                        MailSentInLanguage = true;

                    var path = AppDomain.CurrentDomain.BaseDirectory.ToLower();
                    //var templatesPath = @"emailtemplates/communication1";
                    var templateFile = string.Format(@"{0}emailtemplates/communication1/CancelDebitNote.english.htm", path);

                    #region Setting reason as per the language ...
                    switch (objLanguage.LanguageID)
                    {
                        case 1:
                            Page.UICulture = clsConstants.EnglishISO; //"en-US"; // English
                            break;
                        case 2:
                            Page.UICulture = clsConstants.FranceISO; //"fr"; // France
                            break;
                        case 3:
                            Page.UICulture = clsConstants.GermanyISO; //"de"; // German
                            break;
                        case 4:
                            Page.UICulture = clsConstants.NederlandISO; //"nl"; // Dutch
                            break;
                        case 5:
                            Page.UICulture = clsConstants.SpainISO; //"es"; // Spanish
                            break;
                        case 6:
                            Page.UICulture = clsConstants.ItalyISO; //"it"; // Italy
                            break;
                        case 7:
                            Page.UICulture = clsConstants.CzechISO; //"cz"; // Czech
                            break;
                        default:
                            Page.UICulture = clsConstants.EnglishISO; // "en-US"; // English
                            break;
                    }

                    #endregion

                    #region  Prepairing html body format ...
                    string htmlBody = null;
                    using (StreamReader sReader = new StreamReader(templateFile.ToLower()))
                    {
                        htmlBody = sReader.ReadToEnd();
                        //htmlBody = htmlBody.Replace("{logoInnerPath}", getAbsolutePath());

                        htmlBody = htmlBody.Replace("{VendorNumber}", lstDiscrepancyBE[0].Vendor.Vendor_No);
                        htmlBody = htmlBody.Replace("{DebitNoteNumber}", WebCommon.getGlobalResourceValue("DebitNoteNumber"));
                        htmlBody = htmlBody.Replace("{DebitNoteNumberValue}", lstDiscrepancyBE[0].DebitNoteNo);
                        htmlBody = htmlBody.Replace("{VendorName}", lstDiscrepancyBE[0].Vendor.VendorName);
                        htmlBody = htmlBody.Replace("{OurVATReference}", WebCommon.getGlobalResourceValue("OurVATReference"));

                        if (!string.IsNullOrEmpty(lstDiscrepancyBE[0].OurVATReference) && !string.IsNullOrWhiteSpace(lstDiscrepancyBE[0].OurVATReference))
                            htmlBody = htmlBody.Replace("{OurVATReferenceValue}", lstDiscrepancyBE[0].OurVATReference);
                        else
                            htmlBody = htmlBody.Replace("{OurVATReferenceValue}", string.Empty);

                        htmlBody = htmlBody.Replace("{VendorAddress1}", lstDiscrepancyBE[0].Vendor.address1);
                        htmlBody = htmlBody.Replace("{YourVATReference}", WebCommon.getGlobalResourceValue("YourVATReference"));

                        if (!string.IsNullOrEmpty(lstDiscrepancyBE[0].YourVATReference) && !string.IsNullOrWhiteSpace(lstDiscrepancyBE[0].YourVATReference))
                            htmlBody = htmlBody.Replace("{YourVATReferenceValue}", lstDiscrepancyBE[0].YourVATReference);
                        else
                            htmlBody = htmlBody.Replace("{YourVATReferenceValue}", string.Empty);

                        htmlBody = htmlBody.Replace("{VendorAddress2}", lstDiscrepancyBE[0].Vendor.address2);
                        htmlBody = htmlBody.Replace("{ReasonforCancellation}", WebCommon.getGlobalResourceValue("ReasonforCancellation"));
                        htmlBody = htmlBody.Replace("{ReasonforCancellationValue}", lstDiscrepancyBE[0].CancelReason);

                        if (!string.IsNullOrEmpty(lstDiscrepancyBE[0].Vendor.VMPPIN) && !string.IsNullOrWhiteSpace(lstDiscrepancyBE[0].Vendor.VMPPIN))
                            htmlBody = htmlBody.Replace("{VMPPIN}", lstDiscrepancyBE[0].Vendor.VMPPIN);
                        else
                            htmlBody = htmlBody.Replace("{VMPPIN}", string.Empty);

                        if (!string.IsNullOrEmpty(lstDiscrepancyBE[0].Vendor.VMPPOU) && !string.IsNullOrWhiteSpace(lstDiscrepancyBE[0].Vendor.VMPPOU))
                            htmlBody = htmlBody.Replace("{VMPPOU}", lstDiscrepancyBE[0].Vendor.VMPPOU);
                        else
                            htmlBody = htmlBody.Replace("{VMPPOU}", string.Empty);

                        if (!string.IsNullOrEmpty(lstDiscrepancyBE[0].Vendor.city) && !string.IsNullOrWhiteSpace(lstDiscrepancyBE[0].Vendor.city))
                            htmlBody = htmlBody.Replace("{VendorCity}", lstDiscrepancyBE[0].Vendor.city);
                        else
                            htmlBody = htmlBody.Replace("{VendorCity}", string.Empty);

                        htmlBody = htmlBody.Replace("{DearSirMadam}", WebCommon.getGlobalResourceValue("DearSirMadam"));
                        htmlBody = htmlBody.Replace("{Date}", WebCommon.getGlobalResourceValue("Date"));

                        var canceledDate = Convert.ToDateTime(lstDiscrepancyBE[0].CanceledDate);
                        htmlBody = htmlBody.Replace("{DateValue}", canceledDate.ToString("dd/MM/yyyy"));

                        htmlBody = htmlBody.Replace("{CancelDebitLetterMessage1}", WebCommon.getGlobalResourceValue("CancelDebitLetterMessage1"));
                        htmlBody = htmlBody.Replace("{DebitNoteNumberValue}", lstDiscrepancyBE[0].DebitNoteNo);
                        htmlBody = htmlBody.Replace("{CancelDebitLetterMessage2}", WebCommon.getGlobalResourceValue("CancelDebitLetterMessage2"));

                        var debitRaiseDate = Convert.ToDateTime(lstDiscrepancyBE[0].DebitRaiseDate);
                        htmlBody = htmlBody.Replace("{DebitNoteRaisedDateValue}", debitRaiseDate.ToString("dd/MM/yyyy"));

                        htmlBody = htmlBody.Replace("{CancelDebitLetterMessage3}", WebCommon.getGlobalResourceValue("CancelDebitLetterMessage3"));
                        htmlBody = htmlBody.Replace("{CancelDebitLetterMessage4}", WebCommon.getGlobalResourceValue("CancelDebitLetterMessage4"));
                        htmlBody = htmlBody.Replace("{DebiNoteCancelComments}", lstDiscrepancyBE[0].CancelVendorComments);
                        htmlBody = htmlBody.Replace("{CancelDebitLetterMessage5}", WebCommon.getGlobalResourceValue("CancelDebitLetterMessage5"));

                        htmlBody = htmlBody.Replace("{Yoursfaithfully}", WebCommon.getGlobalResourceValue("Yoursfaithfully"));
                        htmlBody = htmlBody.Replace("{APClerkName}", lstDiscrepancyBE[0].CancelBy);

                        if (!string.IsNullOrEmpty(lstDiscrepancyBE[0].CancelByContactNumber) && !string.IsNullOrWhiteSpace(lstDiscrepancyBE[0].CancelByContactNumber))
                            htmlBody = htmlBody.Replace("{APClerkContactNo}", lstDiscrepancyBE[0].CancelByContactNumber);
                        else
                            htmlBody = htmlBody.Replace("{APClerkContactNo}", string.Empty);

                        var apAddress = string.Empty;
                        if (!string.IsNullOrEmpty(lstDiscrepancyBE[0].APAddress1) && !string.IsNullOrEmpty(lstDiscrepancyBE[0].APAddress2)
                            && !string.IsNullOrEmpty(lstDiscrepancyBE[0].APAddress3) && !string.IsNullOrEmpty(lstDiscrepancyBE[0].APAddress4)
                            && !string.IsNullOrEmpty(lstDiscrepancyBE[0].APAddress5) && !string.IsNullOrEmpty(lstDiscrepancyBE[0].APAddress6))
                        {
                            apAddress = string.Format("{0},&nbsp;{1},&nbsp;{2},&nbsp;{3},&nbsp;{4},&nbsp;{5}", lstDiscrepancyBE[0].APAddress1, lstDiscrepancyBE[0].APAddress2,
                                lstDiscrepancyBE[0].APAddress3, lstDiscrepancyBE[0].APAddress4, lstDiscrepancyBE[0].APAddress5, lstDiscrepancyBE[0].APAddress6);
                        }
                        else if (!string.IsNullOrEmpty(lstDiscrepancyBE[0].APAddress1) && !string.IsNullOrEmpty(lstDiscrepancyBE[0].APAddress2)
                            && !string.IsNullOrEmpty(lstDiscrepancyBE[0].APAddress3) && !string.IsNullOrEmpty(lstDiscrepancyBE[0].APAddress4)
                            && !string.IsNullOrEmpty(lstDiscrepancyBE[0].APAddress5))
                        {
                            apAddress = string.Format("{0},&nbsp;{1},&nbsp;{2},&nbsp;{3},&nbsp;{4}", lstDiscrepancyBE[0].APAddress1, lstDiscrepancyBE[0].APAddress2,
                                lstDiscrepancyBE[0].APAddress3, lstDiscrepancyBE[0].APAddress4, lstDiscrepancyBE[0].APAddress5);
                        }
                        else if (!string.IsNullOrEmpty(lstDiscrepancyBE[0].APAddress1) && !string.IsNullOrEmpty(lstDiscrepancyBE[0].APAddress2)
                           && !string.IsNullOrEmpty(lstDiscrepancyBE[0].APAddress3) && !string.IsNullOrEmpty(lstDiscrepancyBE[0].APAddress4))
                        {
                            apAddress = string.Format("{0},&nbsp;{1},&nbsp;{2},&nbsp;{3}", lstDiscrepancyBE[0].APAddress1, lstDiscrepancyBE[0].APAddress2,
                                lstDiscrepancyBE[0].APAddress3, lstDiscrepancyBE[0].APAddress4);
                        }
                        else if (!string.IsNullOrEmpty(lstDiscrepancyBE[0].APAddress1) && !string.IsNullOrEmpty(lstDiscrepancyBE[0].APAddress2)
                           && !string.IsNullOrEmpty(lstDiscrepancyBE[0].APAddress3))
                        {
                            apAddress = string.Format("{0},&nbsp;{1},&nbsp;{2}", lstDiscrepancyBE[0].APAddress1, lstDiscrepancyBE[0].APAddress2,
                                lstDiscrepancyBE[0].APAddress3);
                        }
                        else if (!string.IsNullOrEmpty(lstDiscrepancyBE[0].APAddress1) && !string.IsNullOrEmpty(lstDiscrepancyBE[0].APAddress2))
                        {
                            apAddress = string.Format("{0},&nbsp;{1}", lstDiscrepancyBE[0].APAddress1, lstDiscrepancyBE[0].APAddress2);
                        }
                        else
                        {
                            apAddress = lstDiscrepancyBE[0].APAddress1;
                        }

                        if (!string.IsNullOrEmpty(lstDiscrepancyBE[0].APPincode))
                            apAddress = string.Format("{0},&nbsp;{1}", apAddress, lstDiscrepancyBE[0].APPincode);

                        htmlBody = htmlBody.Replace("{APClerkAddress}", apAddress);

                    }
                    #endregion

                    #region Sending and saving email details ...
                    string[] sMailAddress = emailIds.Split(',');
                    var sentToWithLink = new System.Text.StringBuilder();
                    for (int index = 0; index < sMailAddress.Length; index++)
                        sentToWithLink.Append("<a target='_blank' href='" + Common.EncryptQuery("../LogDiscrepancy/DISLog_ReSendCommunication.aspx?DebitRaiseId=" + cancelDebitRaiseId + "&CommTitle=DebitCancel") + "' onclick='window.open(this.href, this.target,\"scrollbars=1,left=100px,top=100px,width=1000px,height=800px\"); return false;'>" + sMailAddress[index] + "</a>" + System.Environment.NewLine);

                    var discrepancyMailBE = new DiscrepancyMailBE();
                    discrepancyMailBE.Action = "AddDebitRaiseCommunication";
                    discrepancyMailBE.DebitRaiseId = cancelDebitRaiseId;
                    discrepancyMailBE.mailSubject = DebitNoteCancellationEmailSubject;
                    discrepancyMailBE.sentTo = emailIds;
                    discrepancyMailBE.mailBody = htmlBody;
                    discrepancyMailBE.SendByID = Convert.ToInt32(Session["UserID"]);
                    discrepancyMailBE.IsMailSent = MailSentInLanguage;
                    discrepancyMailBE.languageID = objLanguage.LanguageID;
                    discrepancyMailBE.MailSentInLanguage = MailSentInLanguage;
                    discrepancyMailBE.CommTitle = "DebitCancel";
                    discrepancyMailBE.SentToWithLink = sentToWithLink.ToString();
                    var debitRaiseCommId = discrepancyBAL.addDebitRaiseCommunicationBAL(discrepancyMailBE);
                    if (MailSentInLanguage)
                    {
                        var emailToAddress = emailIds;
                        var emailFromAddress = Convert.ToString(ConfigurationManager.AppSettings["fromAddress"]);
                        var emailToSubject = DebitNoteCancellationEmailSubject;
                        var emailBody = htmlBody;
                        Utilities.clsEmail oclsEmail = new Utilities.clsEmail();
                        oclsEmail.sendMail(emailToAddress, emailBody, emailToSubject, emailFromAddress, true);
                    }
                    #endregion
                }
                #endregion
            }
        }

        Page.UICulture = Convert.ToString(Session["CultureInfo"]);
        mdlDebitCancel.Hide();
        this.BindCancelaDebits();
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        mdlDebitCancel.Hide();
        pnlSearchDebit1.Visible = true;
        pnlDebitLibrary1.Visible = false;
        ucExportToExcel1.Visible = false;
        btnCancel.Visible = false;
        //lblDebitLibrary.Visible = false;
        //lblSearchDebit.Visible = true;
    }

    protected void btnBack_1_Click(object sender, EventArgs e)
    {
        mdlDebitCancel.Hide();
        pnlSearchDebit1.Visible = true;
        pnlDebitLibrary1.Visible = false;
        ucExportToExcel1.Visible = false;
        btnCancel.Visible = false;
        btnBack_1.Visible = false;
    }

    protected void gvCancelDebit_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvCancelDebit.PageIndex = e.NewPageIndex;
        if (ViewState["AllDebitRaise"] != null)
        {
            gvCancelDebit.DataSource = (List<DiscrepancyBE>)ViewState["AllDebitRaise"];
            gvCancelDebit.DataBind();
        }
    }

    protected void gvCancelDebit_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType != DataControlRowType.Header && e.Row.RowType != DataControlRowType.Footer)
        {
            HyperLink hlDebitNoteNoValue = (HyperLink)e.Row.FindControl("hlDebitNoteNoValue");
            Label lblCommunicationEmailsValue = (Label)e.Row.FindControl("lblCommunicationEmailsValue");
            RadioButton rblSelect = (RadioButton)e.Row.FindControl("rblSelect");

            if (lblCommunicationEmailsValue != null)
            {
                lblCommunicationEmailsValue.Text = lblCommunicationEmailsValue.Text.Replace(",,,,", ",").Replace(",,,", ",").Replace(",,", ",").Replace(",", ", ");
            }

            if (ViewState["hlDebitNoteNoValue"] != null && hlDebitNoteNoValue != null)
            {
                if (hlDebitNoteNoValue.Text.Equals(Convert.ToString(ViewState["hlDebitNoteNoValue"])))
                {
                    rblSelect.Visible = false;
                    ViewState["hlDebitNoteNoValue"] = hlDebitNoteNoValue.Text;
                }
                else
                {
                    ViewState["hlDebitNoteNoValue"] = hlDebitNoteNoValue.Text;
                }
            }
            else if (hlDebitNoteNoValue != null) { ViewState["hlDebitNoteNoValue"] = hlDebitNoteNoValue.Text; }

            ucLabel lblDiscrepancyLogIDValue = (ucLabel)e.Row.FindControl("lblDiscrepancyLogIDValue");
            if (lblDiscrepancyLogIDValue != null && (!string.IsNullOrEmpty(lblDiscrepancyLogIDValue.Text) && !string.IsNullOrWhiteSpace(lblDiscrepancyLogIDValue.Text)))
            {
                HyperLink hlVDRNoValue = (HyperLink)e.Row.FindControl("hlVDRNoValue");
                ucLabel lblDiscrepancyTypeIDValue = (ucLabel)e.Row.FindControl("lblDiscrepancyTypeIDValue");
                ucLabel lblCreatedByIdValue = (ucLabel)e.Row.FindControl("lblCreatedByIdValue");

                var VDRNo = hlVDRNoValue.Text;
                switch (Convert.ToInt32(lblDiscrepancyTypeIDValue.Text.Trim()))
                {
                    case 1:
                        hlVDRNoValue.NavigateUrl = EncryptQuery("~/ModuleUI/Discrepancy/LogDiscrepancy/DISLog_Overs.aspx?disLogID=" + lblDiscrepancyLogIDValue.Text + "&VDRNo=" + VDRNo + "&UserID=" + lblCreatedByIdValue.Text + "&PN=CancelDebit");
                        break;
                    case 2:
                        hlVDRNoValue.NavigateUrl = EncryptQuery("~/ModuleUI/Discrepancy/LogDiscrepancy/DISLog_Shortage.aspx?disLogID=" + lblDiscrepancyLogIDValue.Text + "&VDRNo=" + VDRNo + "&UserID=" + lblCreatedByIdValue.Text + "&PN=CancelDebit");
                        break;
                    case 3:
                        hlVDRNoValue.NavigateUrl = EncryptQuery("~/ModuleUI/Discrepancy/LogDiscrepancy/DISLog_GoodsReceivedDamaged.aspx?disLogID=" + lblDiscrepancyLogIDValue.Text + "&VDRNo=" + VDRNo + "&UserID=" + lblCreatedByIdValue.Text + "&PN=CancelDebit");
                        break;
                    case 4:
                        hlVDRNoValue.NavigateUrl = EncryptQuery("~/ModuleUI/Discrepancy/LogDiscrepancy/DISLog_NoPurchaseOrder.aspx?disLogID=" + lblDiscrepancyLogIDValue.Text + "&VDRNo=" + VDRNo + "&UserID=" + lblCreatedByIdValue.Text + "&PN=CancelDebit");
                        break;
                    case 5:
                        hlVDRNoValue.NavigateUrl = EncryptQuery("~/ModuleUI/Discrepancy/LogDiscrepancy/DISLog_NoPaperwork.aspx?disLogID=" + lblDiscrepancyLogIDValue.Text + "&VDRNo=" + VDRNo + "&UserID=" + lblCreatedByIdValue.Text + "&PN=CancelDebit");
                        break;
                    case 6:
                        hlVDRNoValue.NavigateUrl = EncryptQuery("~/ModuleUI/Discrepancy/LogDiscrepancy/DISLog_IncorrectProduct.aspx?disLogID=" + lblDiscrepancyLogIDValue.Text + "&VDRNo=" + VDRNo + "&UserID=" + lblCreatedByIdValue.Text + "&PN=CancelDebit");
                        break;
                    case 7:
                        hlVDRNoValue.NavigateUrl = EncryptQuery("~/ModuleUI/Discrepancy/LogDiscrepancy/DISLog_PresentationIssue.aspx?disLogID=" + lblDiscrepancyLogIDValue.Text + "&VDRNo=" + VDRNo + "&UserID=" + lblCreatedByIdValue.Text + "&PN=CancelDebit");
                        break;
                    case 8:
                        hlVDRNoValue.NavigateUrl = EncryptQuery("~/ModuleUI/Discrepancy/LogDiscrepancy/DISLog_IncorrectAddress.aspx?disLogID=" + lblDiscrepancyLogIDValue.Text + "&VDRNo=" + VDRNo + "&UserID=" + lblCreatedByIdValue.Text + "&PN=CancelDebit");
                        break;
                    case 9:
                        hlVDRNoValue.NavigateUrl = EncryptQuery("~/ModuleUI/Discrepancy/LogDiscrepancy/DISLog_PaperworkAmended.aspx?disLogID=" + lblDiscrepancyLogIDValue.Text + "&VDRNo=" + VDRNo + "&UserID=" + lblCreatedByIdValue.Text + "&PN=CancelDebit");
                        break;
                    case 10:
                        hlVDRNoValue.NavigateUrl = EncryptQuery("~/ModuleUI/Discrepancy/LogDiscrepancy/DISLog_WrongPackSize.aspx?disLogID=" + lblDiscrepancyLogIDValue.Text + "&VDRNo=" + VDRNo + "&UserID=" + lblCreatedByIdValue.Text + "&PN=CancelDebit");
                        break;
                    case 11:
                        hlVDRNoValue.NavigateUrl = EncryptQuery("~/ModuleUI/Discrepancy/LogDiscrepancy/DISLog_FailPalletSpecification.aspx?disLogID=" + lblDiscrepancyLogIDValue.Text + "&VDRNo=" + VDRNo + "&UserID=" + lblCreatedByIdValue.Text + "&PN=CancelDebit");
                        break;
                    case 12:
                        hlVDRNoValue.NavigateUrl = EncryptQuery("~/ModuleUI/Discrepancy/LogDiscrepancy/DISLog_QualityIssue.aspx?disLogID=" + lblDiscrepancyLogIDValue.Text + "&VDRNo=" + VDRNo + "&UserID=" + lblCreatedByIdValue.Text + "&PN=CancelDebit");
                        break;
                    case 13:
                        hlVDRNoValue.NavigateUrl = EncryptQuery("~/ModuleUI/Discrepancy/LogDiscrepancy/DISLog_PrematureInvoiceReceipt.aspx?disLogID=" + lblDiscrepancyLogIDValue.Text + "&VDRNo=" + VDRNo + "&UserID=" + lblCreatedByIdValue.Text + "&PN=CancelDebit");
                        break;
                    case 14:
                        hlVDRNoValue.NavigateUrl = EncryptQuery("~/ModuleUI/Discrepancy/LogDiscrepancy/DISLog_GenericDescrepancy.aspx?disLogID=" + lblDiscrepancyLogIDValue.Text + "&VDRNo=" + VDRNo + "&UserID=" + lblCreatedByIdValue.Text + "&PN=CancelDebit");
                        break;
                    case 15:
                        hlVDRNoValue.NavigateUrl = EncryptQuery("~/ModuleUI/Discrepancy/LogDiscrepancy/DISLog_Shuttle.aspx?disLogID=" + lblDiscrepancyLogIDValue.Text + "&VDRNo=" + VDRNo + "&UserID=" + lblCreatedByIdValue.Text + "&PN=CancelDebit");
                        break;
                    case 16:
                        hlVDRNoValue.NavigateUrl = EncryptQuery("~/ModuleUI/Discrepancy/LogDiscrepancy/DISLog_Reservation.aspx?disLogID=" + lblDiscrepancyLogIDValue.Text + "&VDRNo=" + VDRNo + "&UserID=" + lblCreatedByIdValue.Text + "&PN=CancelDebit");
                        break;
                    case 17:
                        hlVDRNoValue.NavigateUrl = EncryptQuery("~/ModuleUI/Discrepancy/LogDiscrepancy/DISLog_InvoiceDiscrepancy.aspx?disLogID=" + lblDiscrepancyLogIDValue.Text + "&VDRNo=" + VDRNo + "&UserID=" + lblCreatedByIdValue.Text + "&PN=CancelDebit");
                        break;

                    case 18:
                        hlVDRNoValue.NavigateUrl = EncryptQuery("~/ModuleUI/Discrepancy/LogDiscrepancy/DISLog_StockPlannerInvoiceDiscrepancy.aspx?disLogID=" + lblDiscrepancyLogIDValue.Text + "&VDRNo=" + VDRNo + "&UserID=" + lblCreatedByIdValue.Text + "&PN=CancelDebit");
                        break;
                    case 19:
                        hlVDRNoValue.NavigateUrl = EncryptQuery("~/ModuleUI/Discrepancy/LogDiscrepancy/DISLog_InvoiceQueryDiscrepancy.aspx?disLogID=" + lblDiscrepancyLogIDValue.Text + "&VDRNo=" + VDRNo + "&UserID=" + lblCreatedByIdValue.Text + "&PN=CancelDebit");
                        break;
                }
            }
        }
    }

    #endregion

    #region Methods ...

    private void BindCancelaDebits()
    {
        var discrepancyBE = new DiscrepancyBE();
        var discrepancyBAL = new DiscrepancyBAL();

        discrepancyBE.Action = "GetAllDebitRaise";
        if (!string.IsNullOrEmpty(msDebitType.SelectedDebitTypeIDs))
            discrepancyBE.DebitReasonIds = msDebitType.SelectedDebitTypeIDs;

        if (!string.IsNullOrEmpty(msVendor.SelectedVendorIDs))
            discrepancyBE.VendorIds = msVendor.SelectedVendorIDs;

        if (!string.IsNullOrEmpty(msAP.SelectedStockPlannerIDs))
            discrepancyBE.CreatedByIds = msAP.SelectedStockPlannerIDs;

        discrepancyBE.DebitRaiseFromDate = string.IsNullOrEmpty(txtFromDate.Text) ? (DateTime?)null : Common.GetMM_DD_YYYY(txtFromDate.Text);
        discrepancyBE.DebitRaiseToDate = string.IsNullOrEmpty(txtToDate.Text) ? (DateTime?)null : Common.GetMM_DD_YYYY(txtToDate.Text);

        discrepancyBE.Status = "A";

        if (!string.IsNullOrEmpty(msDebitNoteNumber.SelectedDebitNoteNumber))
            discrepancyBE.DebitNoteNumbers = msDebitNoteNumber.SelectedDebitNoteNumber;

        if (!string.IsNullOrEmpty(msPurchaseOrderNumber.SelectedPO))
            discrepancyBE.PONumbers = msPurchaseOrderNumber.SelectedPO;

        var lstDiscrepancyBE = discrepancyBAL.GetAllDebitRaiseBAL(discrepancyBE);
        gvCancelDebit.DataSource = null;
        gvCancelDebitExport.DataSource = null;
        if (lstDiscrepancyBE != null && lstDiscrepancyBE.Count > 0)
        {
            gvCancelDebit.DataSource = lstDiscrepancyBE;
            gvCancelDebitExport.DataSource = lstDiscrepancyBE;
            ViewState["AllDebitRaise"] = lstDiscrepancyBE;
            ucExportToExcel1.Visible = true;
            btnCancel.Visible = true;
            btnBack_1.Visible = true; ;
        }
        gvCancelDebit.DataBind();
        gvCancelDebitExport.DataBind();       
        pnlDebitLibrary1.Visible = true;
        pnlSearchDebit1.Visible = false;
    }

    private void BindCancelaDebitsOld()
    {
        var discrepancyBE = new DiscrepancyBE();
        var discrepancyBAL = new DiscrepancyBAL();

        discrepancyBE.Action = "GetAllDebitRaise";
        discrepancyBE.Status = "A";
        //if (!string.IsNullOrEmpty(txtDebitNoteNumber.Text) && !string.IsNullOrWhiteSpace(txtDebitNoteNumber.Text))
        //    discrepancyBE.DebitNoteNo = txtDebitNoteNumber.Text.Trim();

        var lstDiscrepancyBE = discrepancyBAL.GetAllDebitRaiseBAL(discrepancyBE);
        gvCancelDebit.DataSource = null;
        gvCancelDebitExport.DataSource = null;
        if (lstDiscrepancyBE != null && lstDiscrepancyBE.Count > 0)
        {
            btnCancel.Visible = true;
            gvCancelDebit.DataSource = lstDiscrepancyBE;
            gvCancelDebitExport.DataSource = lstDiscrepancyBE;
            ViewState["AllDebitRaise"] = lstDiscrepancyBE;
            ucExportToExcel1.Visible = true;
            btnCancel.Visible = true;
        }
        gvCancelDebit.DataBind();
        gvCancelDebitExport.DataBind();
    }

    private void BindCancleReason()
    {
        var debitReasonTypeBAL = new DIS_DebitReasonTypeBAL();
        var apActionBE = new APActionBE();
        apActionBE.Action = "GetReasonTypeSetUp";
        apActionBE.ReasonType = "CDR"; /* Debit Reason */
        var lstAPActionBE = debitReasonTypeBAL.GetReasonTypeBAL(apActionBE);
        if (lstAPActionBE != null && lstAPActionBE.Count > 0)
        {
            FillControls.FillDropDown(ref ddlReason, lstAPActionBE, "Reason", "ReasonTypeID", "Select");
        }
    }

    #endregion
}
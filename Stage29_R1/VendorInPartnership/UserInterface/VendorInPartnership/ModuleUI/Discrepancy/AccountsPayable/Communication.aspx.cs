﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using Utilities;

public partial class ModuleUI_Discrepancy_AccountsPayable_Communication : CommonPage
{
    
    sendCommunicationCommon oSendCommunicationCommon = new sendCommunicationCommon();
    protected void Page_Load(object sender, EventArgs e)
    {
        string path = AppDomain.CurrentDomain.BaseDirectory.ToLower();
        string templatePath = @"/EmailTemplates/AccountsPayable/";

        templatePath = path + templatePath;

        string htmlBody = null;
        string LanguageFile = string.Empty;
        if (GetQueryStringValue("CommunicationType") != null && !string.IsNullOrWhiteSpace(GetQueryStringValue("CommunicationType").ToString()))
        {
            if (GetQueryStringValue("CommunicationType").ToString() == "DebitCreate")
            {
                LanguageFile = "debitcreate.english.htm";
            }
            else if (GetQueryStringValue("CommunicationType").ToString() == "DebitCancel")
            {
                LanguageFile = "debitcancel.english.htm";
            }
        }
        else if (Request.QueryString["CommunicationType"] != null && !string.IsNullOrWhiteSpace(Request.QueryString["CommunicationType"].ToString()))
        {
            if (Request.QueryString["CommunicationType"].ToString() == "DebitCreate")
            {
                LanguageFile = "debitcreate.english.htm";
            }
            else if (Request.QueryString["CommunicationType"].ToString() == "DebitCancel")
            {
                LanguageFile = "debitcancel.english.htm";
            }
        }
        
        if (!System.IO.File.Exists(templatePath + LanguageFile.ToLower()))
        {
            LanguageFile = "debitCreate.english.htm";
        }
        if (!Page.IsPostBack)
        {

            using (StreamReader sReader = new StreamReader(templatePath + LanguageFile.ToLower()))
            {
                var imagepath = oSendCommunicationCommon.getAbsolutePath();

                htmlBody = sReader.ReadToEnd();
                htmlBody = htmlBody.Replace("{logoInnerPath}", imagepath);
            }

            divReSendCommunication.InnerHtml = htmlBody;



        }

    }
}
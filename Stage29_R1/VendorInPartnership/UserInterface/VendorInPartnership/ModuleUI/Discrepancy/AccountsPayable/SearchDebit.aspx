﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    AutoEventWireup="true" CodeFile="SearchDebit.aspx.cs" Inherits="AccountsPayable_SearchDebit" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/ucVendorSelectionTemplate.ascx" TagName="VendorSelectTemplate"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectVendor.ascx" TagName="MultiSelectVendor"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectDebitType.ascx" TagName="ucMultiSelectDebitType"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectAPSearch.ascx" TagName="ucMultiSelectAP"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/ucExportToExcel.ascx" TagName="ucExportToExcel"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectDebitNoteNumber.ascx" TagName="ucMultiSelectDebitNoteNumber"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectPO.ascx" TagName="ucMultiSelectPO"
    TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script src="../../../Scripts/jquery.tooltip.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        function InitializeToolTip() {
            $(".gridViewToolTip").tooltip({
                track: true,
                delay: 0,
                showURL: false,
                fade: 100,
                bodyHandler: function () {
                    return $("#tooltip").html();
                },
                showURL: false
            });
        }

        $(document).ready(function () {
            $("body").keypress(function (e) {
                if (e.which == 13 || e.keyCode == 13) {
                    $('#<%=btnSearch.ClientID %>').click();
                    return false;
                }
            });
        });
        
    </script>
    <script type="text/javascript">
        $(function () {
            InitializeToolTip();
        })
    </script>
    <style type="text/css">
        #tooltip
        {
            position: absolute;
            z-index: 3000;
            border: 1px solid #111;
            background-color: #FEE18D;
            padding: 5px;
            opacity: 1.00;
        }
        #tooltip h3, #tooltip div
        {
            margin: 0;
        }
    </style>
    <asp:ScriptManager ID="SM1" runat="server" AsyncPostBackTimeout="2400">
    </asp:ScriptManager>
    <asp:HiddenField ID="hdnJSFromDt" runat="server" />
    <asp:HiddenField ID="hdnJSToDt" runat="server" />
    <h2>
        <cc1:ucLabel ID="lblSearchDebit" runat="server" Text="Search Debit"></cc1:ucLabel>
        <cc1:ucLabel ID="lblDebitLibrary" runat="server" Text="Debit Library" Visible="false"></cc1:ucLabel>
    </h2>
    <div id="tooltip" style="display: none;">
        <table>
            <tr>
                <td style="white-space: nowrap;">
                    <b>A =
                        <%=Active%></b>
                </td>
            </tr>
            <tr>
                <td style="white-space: nowrap;">
                    <b>C =
                        <%=Cancelled%></b>
                </td>
            </tr>
        </table>
    </div>
    <div class="right-shadow">
        <div class="formbox">
            <cc1:ucPanel ID="pnlSearchDebit1" runat="server">
                <table width="100%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                    <tr>
                        <td align="right">
                            <cc1:VendorSelectTemplate runat="server" ID="ucVendorTemplateSelect" />
                        </td>
                    </tr>
                </table>
                <table width="100%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                    <tr>
                        <td style="font-weight: bold; width: 15%">
                            <cc1:ucLabel ID="lblDebitNoteNumber" runat="server"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            :
                        </td>
                        <td>
                            <cc1:ucMultiSelectDebitNoteNumber runat="server" ID="msDebitNoteNumber" />
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold; width: 15%">
                            <cc1:ucLabel ID="lblDebitType" runat="server"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            :
                        </td>
                        <td>
                            <cc1:ucMultiSelectDebitType runat="server" ID="msDebitType" />
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblVendor" runat="server"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            :
                        </td>
                        <td>
                            <cc1:MultiSelectVendor runat="server" ID="msVendor" />
                        </td>
                    </tr>
                    <tr id="trAPClerk" runat="server">
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblRaisedBy" runat="server"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            :
                        </td>
                        <td>
                            <cc1:ucMultiSelectAP ID="msAP" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblPurchaseOrderNumber" Text="Purchase Order Number" runat="server"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            :
                        </td>
                        <td>
                            <cc1:ucMultiSelectPO ID="msPurchaseOrderNumber" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <table width="100%">
                                <tr>
                                    <td style="font-weight: bold; width: 15%">
                                        <cc1:ucLabel ID="lblDateDebitwasRaised" runat="server"></cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold; width: 1%">
                                        :
                                    </td>
                                    <td align="left">
                                        <table width="25%" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style="font-weight: bold; width: 6em;">
                                                    <asp:TextBox ID="txtFromDate" runat="server" ClientIDMode="Static" class="date" />
                                                    <%--<cc1:ucTextbox ID="txtFromDate" runat="server" ClientIDMode="Static" CssClass="date"
                                                        onchange="setValue1(this)" ReadOnly="True" Width="70px" />--%>
                                                </td>
                                                <td style="font-weight: bold; width: 4em; text-align: center">
                                                    <cc1:ucLabel ID="lblTo" runat="server" Text="To"></cc1:ucLabel>
                                                </td>
                                                <td style="font-weight: bold; width: 6em;">
                                                    <asp:TextBox ID="txtToDate" runat="server" ClientIDMode="Static" class="date" />
                                                    <%--<cc1:ucTextbox ID="txtToDate" runat="server" ClientIDMode="Static" CssClass="date"
                                                        onchange="setValue2(this)" ReadOnly="True" Width="70px" />--%>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblDebitStatus" runat="server"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            :
                        </td>
                        <td>
                            <cc1:ucDropdownList runat="server" ID="ddlDebitStatus" Width="110px">
                                <asp:ListItem Text="Active" Value="A"></asp:ListItem>
                                <asp:ListItem Text="Cancelled" Value="C"></asp:ListItem>
                                <asp:ListItem Text="All" Value="All"></asp:ListItem>
                            </cc1:ucDropdownList>
                        </td>
                    </tr>
                    <tr>
                        <td align="right" colspan="3">
                            <div class="button-row">
                                <cc1:ucButton ID="btnSearch" runat="server" Text="Search" CssClass="button" OnClick="btnSearch_Click" />
                            </div>
                        </td>
                    </tr>
                </table>
            </cc1:ucPanel>
            <cc1:ucPanel ID="pnlDebitLibrary1" runat="server">
                <div class="button-row">
                    <cc1:ucExportToExcel ID="ucExportToExcel1" runat="server" />
                </div>
                <div style="width: 100%; overflow: auto" id="divPrint" class="fixedTable">
                    <table cellspacing="1" cellpadding="0" border="0" align="center" class="form-table">
                        <tr>
                            <td style="font-weight: bold;" align="center">
                                <div style="width: 950px; overflow-x: auto;">
                                    <cc1:ucGridView ID="gvSearchDebit" runat="server" AutoGenerateColumns="false" CssClass="grid gvclass searchgrid-1 "
                                        GridLines="Both" AllowPaging="true" PageSize="50" OnPageIndexChanging="gvSearchDebit_PageIndexChanging"
                                        OnRowDataBound="gvSearchDebit_RowDataBound">
                                        <EmptyDataTemplate>
                                            <div style="text-align: center">                                                
                                                <cc1:ucLabel ID="lblRecordNotFound" runat="server" isRequired="true" Text="No record found"></cc1:ucLabel>
                                            </div>
                                        </EmptyDataTemplate>
                                        <Columns>
                                            <asp:TemplateField HeaderText="S" SortExpression="DebitStatus">
                                                <HeaderStyle Width="10px" HorizontalAlign="Left" />
                                                <ItemTemplate>
                                                    <cc1:ucLabel runat="server" ID="lblVendorIDValue" Text='<%#Eval("VendorID") %>' Visible="false"></cc1:ucLabel>
                                                    <cc1:ucLabel runat="server" ID="lblDebitRaiseIdValue" Text='<%#Eval("DebitRaiseId") %>'
                                                        Visible="false"></cc1:ucLabel>
                                                    <cc1:ucLabel runat="server" ID="lblDiscrepancyLogIDValue" Text='<%#Eval("DiscrepancyLogID") %>'
                                                        Visible="false"></cc1:ucLabel>
                                                    <cc1:ucLabel runat="server" ID="lblDiscrepancyTypeIDValue" Text='<%#Eval("DiscrepancyTypeID") %>'
                                                        Visible="false"></cc1:ucLabel>
                                                    <cc1:ucLabel runat="server" ID="lblCreatedByIdValue" Text='<%#Eval("CreatedById") %>'
                                                        Visible="false"></cc1:ucLabel>
                                                    <asp:HyperLink ID="lblStatus" class="gridViewToolTip" runat="server" Text='<%# Eval("Status") %>'></asp:HyperLink>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" Width="10px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Vendor #" SortExpression="Vendor.Vendor_No">
                                                <HeaderStyle Width="70px" HorizontalAlign="Left" />
                                                <ItemTemplate>
                                                    <cc1:ucLabel runat="server" ID="ltVendorNoValue" Text='<%#Eval("Vendor.Vendor_No") %>'></cc1:ucLabel>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" Width="70px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Vendor Name" SortExpression="Vendor.VendorName">
                                                <HeaderStyle Width="150px" HorizontalAlign="Left" />
                                                <ItemTemplate>
                                                    <cc1:ucLabel runat="server" ID="ltVendor" Text='<%#Eval("Vendor.VendorName") %>'></cc1:ucLabel>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" Width="150px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Discrepancy # (if applicable)">
                                                <HeaderStyle Width="50px" HorizontalAlign="Left" />
                                                <ItemTemplate>
                                                    <asp:HyperLink ID="hlVDRNoValue" runat="server" Text='<%# Eval("VDRNo") %>' Target="_blank"></asp:HyperLink>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" Width="50px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Date Debit Raised">
                                                <HeaderStyle Width="80px" HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Left" Width="80px" />
                                                <ItemTemplate>
                                                    <cc1:ucLabel ID="lblDateDebitRaised" runat="server" Text='<%# Eval("DebitRaiseDate", "{0:dd/MM/yyyy}") %>'></cc1:ucLabel>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField HeaderText="Raised By" DataField="CreatedBy">
                                                <HeaderStyle Width="150px" HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="left" Width="150px" />
                                            </asp:BoundField>
                                            <asp:TemplateField HeaderText="Debit Note Number">
                                                <HeaderStyle Width="50px" HorizontalAlign="Left" />
                                                <ItemTemplate>
                                                    <asp:HyperLink ID="hlDebitNoteNo" runat="server" Text='<%#Eval("DebitNoteNo") %>'
                                                        NavigateUrl='<%# EncryptQuery("~/ModuleUI/Discrepancy/LogDiscrepancy/DISLog_ReSendCommunication.aspx?CommTitle=DebitRaised&DebitRaiseId=" + Eval("DebitRaiseId") + "&CommunicationType=DebitRaised") %>'
                                                        Target='_blank' onclick='window.open(this.href, this.target,"scrollbars=1,left=100px,top=20px,min-width=1000px,height=620px,toolbar=0,scrollbars=1,status=0"); return false;'></asp:HyperLink>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" Width="50px" />
                                            </asp:TemplateField>
                                            <asp:BoundField HeaderText="Debit Reason" DataField="Reason">
                                                <HeaderStyle Width="120px" HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Left" Width="120px" />
                                            </asp:BoundField>
                                            <asp:BoundField HeaderText="PO" DataField="PurchaseOrderNumber">
                                                <HeaderStyle Width="120px" HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Left" Width="120px" />
                                            </asp:BoundField>
                                            <asp:BoundField HeaderText="Value of Debit" DataField="StateValueDebited">
                                                <HeaderStyle Width="50px" HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Left" Width="50px" />
                                            </asp:BoundField>
                                            <asp:TemplateField HeaderText="Currency">
                                                <HeaderStyle Width="50px" HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Left" Width="50px" />
                                                <ItemTemplate>
                                                    <cc1:ucLabel ID="lblCurrencyValue" runat="server" Text='<%# Eval("Currency.CurrencyName") %>'></cc1:ucLabel>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Debit Sent to">
                                                <HeaderStyle Width="150px" HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="left" Width="150px" />
                                                <ItemTemplate>
                                                    <asp:Label ID="lblCommunicationEmailsValue" runat="server" Text='<%#Eval("CommunicationEmails") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="If cancelled (date)">
                                                <HeaderStyle Width="70px" HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="left" Width="70px" />
                                                <ItemTemplate>
                                                    <asp:HyperLink ID="hypLinkToDisDetails" runat="server" Text='<%# Eval("CanceledDate", "{0:dd/MM/yyyy}") %>'
                                                        NavigateUrl='<%# EncryptQuery("~/ModuleUI/Discrepancy/LogDiscrepancy/DISLog_ReSendCommunication.aspx?CommTitle=DebitCancel&DebitRaiseId=" + Eval("DebitRaiseId") + "&CommunicationType=DebitCancel") %>'
                                                        Target='_blank' onclick='window.open(this.href, this.target,"scrollbars=1,left=100px,top=20px,width=1000px,height=620px,toolbar=0,scrollbars=0,status=0"); return false;'></asp:HyperLink>
                                                    <%-- <asp:HyperLink ID="hypLinkToDisDetails" runat="server" Text='<%# Eval("CanceledDate", "{0:dd/MM/yyyy}") %>'
                                                        href='<%# EncryptQuery("Communication.aspx?CommunicationType=DebitCancel" ) %>'
                                                        Target='_blank' onclick='window.open(this.href, this.target,"scrollbars=1,left=100px,top=20px,width=1000px,height=620px,toolbar=0,scrollbars=0,status=0"); return false;'></asp:HyperLink>--%>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField HeaderText="If cancelled (who cancelled)" DataField="CancelBy">
                                                <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="left" Width="100px" />
                                            </asp:BoundField>
                                            <asp:BoundField HeaderText="If cancelled (sent to)" DataField="CancelCommunicationEmails">
                                                <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="left" Width="100px" />
                                            </asp:BoundField>
                                        </Columns>
                                    </cc1:ucGridView>
                                    <cc1:ucGridView ID="gvSearchDebitExcel" runat="server" AutoGenerateColumns="false"
                                        CssClass="grid gvclass searchgrid-1 " GridLines="Both" Visible="false">
                                        <Columns>
                                            <asp:TemplateField HeaderText="S" SortExpression="DebitStatus">
                                                <HeaderStyle Width="10px" HorizontalAlign="Left" />
                                                <ItemTemplate>
                                                    <asp:HyperLink ID="lblStatus" class="gridViewToolTip" runat="server" Text='<%# Eval("Status") %>'></asp:HyperLink>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" Width="10px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Vendor #" SortExpression="Vendor.Vendor_No">
                                                <HeaderStyle Width="70px" HorizontalAlign="Left" />
                                                <ItemTemplate>
                                                    <cc1:ucLabel runat="server" ID="ltVendorNoValue" Text='<%#Eval("Vendor.Vendor_No") %>'></cc1:ucLabel>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" Width="70px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Vendor Name" SortExpression="Vendor.VendorName">
                                                <HeaderStyle Width="150px" HorizontalAlign="Left" />
                                                <ItemTemplate>
                                                    <cc1:ucLabel runat="server" ID="ltVendor" Text='<%#Eval("Vendor.VendorName") %>'></cc1:ucLabel>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" Width="150px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Discrepancy # (if applicable)">
                                                <HeaderStyle Width="50px" HorizontalAlign="Left" />
                                                <ItemTemplate>
                                                    <asp:HyperLink ID="hlVDRNoValue" runat="server" Text='<%# Eval("VDRNo") %>' Target="_blank"></asp:HyperLink>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" Width="50px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Date Debit Raised">
                                                <HeaderStyle Width="80px" HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Left" Width="80px" />
                                                <ItemTemplate>
                                                    <cc1:ucLabel ID="lblDateDebitRaised" runat="server" Text='<%# Eval("DebitRaiseDate", "{0:dd/MM/yyyy}") %>'></cc1:ucLabel>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField HeaderText="Raised By" DataField="CreatedBy">
                                                <HeaderStyle Width="150px" HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="left" Width="150px" />
                                            </asp:BoundField>
                                            <asp:TemplateField HeaderText="Debit Note Number">
                                                <HeaderStyle Width="50px" HorizontalAlign="Left" />
                                                <ItemTemplate>
                                                    <asp:HyperLink ID="hlDebitNoteNo" runat="server" Text='<%# Eval("DebitNoteNo") %>'
                                                        href='<%# EncryptQuery("Communication.aspx?CommunicationType=DebitCreate" ) %>'
                                                        Target='_blank' onclick='window.open(this.href, this.target,"scrollbars=1,left=100px,top=20px,width=1000px,height=620px,toolbar=0,scrollbars=0,status=0"); return false;'></asp:HyperLink>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" Width="50px" />
                                            </asp:TemplateField>
                                            <asp:BoundField HeaderText="Debit Reason" DataField="Reason">
                                                <HeaderStyle Width="120px" HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Left" Width="120px" />
                                            </asp:BoundField>
                                            <asp:BoundField HeaderText="PO" DataField="PurchaseOrderNumber">
                                                <HeaderStyle Width="120px" HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Left" Width="120px" />
                                            </asp:BoundField>
                                            <asp:BoundField HeaderText="Value of Debit" DataField="StateValueDebited">
                                                <HeaderStyle Width="50px" HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Left" Width="50px" />
                                            </asp:BoundField>
                                            <asp:TemplateField HeaderText="Currency">
                                                <HeaderStyle Width="50px" HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Left" Width="50px" />
                                                <ItemTemplate>
                                                    <cc1:ucLabel ID="lblCurrencyValue" runat="server" Text='<%# Eval("Currency.CurrencyName") %>'></cc1:ucLabel>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Debit Sent to">
                                                <HeaderStyle Width="150px" HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="left" Width="150px" />
                                                <ItemTemplate>
                                                    <asp:Label ID="lblCommunicationEmailsValue" runat="server" Text='<%#Eval("CommunicationEmails") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="If cancelled (date)">
                                                <HeaderStyle Width="70px" HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="left" Width="70px" />
                                                <ItemTemplate>
                                                    <asp:HyperLink ID="hypLinkToDisDetails" runat="server" Text='<%# Eval("CanceledDate", "{0:dd/MM/yyyy}") %>'
                                                        href='<%# EncryptQuery("Communication.aspx?CommunicationType=DebitCancel" ) %>'
                                                        Target='_blank' onclick='window.open(this.href, this.target,"scrollbars=1,left=100px,top=20px,width=1000px,height=620px,toolbar=0,scrollbars=0,status=0"); return false;'></asp:HyperLink>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField HeaderText="If cancelled (who cancelled)" DataField="CancelBy">
                                                <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="left" Width="100px" />
                                            </asp:BoundField>
                                            <asp:BoundField HeaderText="If cancelled (sent to)" DataField="CancelCommunicationEmails">
                                                <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="left" Width="100px" />
                                            </asp:BoundField>
                                        </Columns>
                                    </cc1:ucGridView>
                                </div>
                            </td>
                        </tr>
                        <%-- <tr>
                            <td>
                                <cc1:PagerV2_8 ID="pager1" runat="server" OnCommand="pager_Command" GenerateGoToSection="false">
                                </cc1:PagerV2_8>
                            </td>
                        </tr>--%>
                    </table>
                </div>
                <div class="button-row">
                    <cc1:ucButton ID="btnBack" runat="server" Text="Back" CssClass="button" OnClick="btnBack_Click" />
                </div>
            </cc1:ucPanel>
        </div>
    </div>
</asp:Content>

﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master" AutoEventWireup="true"
 CodeFile="CancelReasonSetUpOverview.aspx.cs" Inherits="AccountsPayable_CancelReasonSetUpOverview" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="../../../CommonUI/UserControls/ucAddButton.ascx" TagName="ucAddButton"
    TagPrefix="cc2" %>
<%@ Register Src="../../../CommonUI/UserControls/ucExportToExcel.ascx" TagName="ucExportToExcel"
    TagPrefix="cc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h2>
        <cc1:ucLabel ID="lblCancelDebitReasonType" runat="server" Text="Cancel Debit Reason Type Set Up"></cc1:ucLabel>
    </h2>
    <div class="button-row">
        <cc2:ucAddButton ID="btnAdd" runat="server" NavigateUrl="CancelReasonSetUpEdit.aspx" />
        <cc2:ucExportToExcel ID="ucExportToExcel1" runat="server" />
    </div>
    <table width="100%">
        <tr>
            <td align="center" colspan="9">
                <cc1:ucGridView ID="gvCancelReasonSetUp" Width="100%" runat="server" CssClass="grid"
                    OnSorting="SortGrid" AllowSorting="true">
                     <Columns>
                        <asp:TemplateField HeaderText="Reason" SortExpression="Reason">
                            <HeaderStyle Width="40%" HorizontalAlign="Left" />
                            <ItemTemplate>
                                <asp:HyperLink ID="hpReasonValue" runat="server" Text='<%# Eval("Reason") %>' NavigateUrl='<%# EncryptQuery("CancelReasonSetUpEdit.aspx?DebitReasonID="+ Eval("ReasonTypeID")) %>'></asp:HyperLink>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:TemplateField>
                        <asp:BoundField HeaderText="CreatedBy" ItemStyle-Wrap="false" DataField="UserName" SortExpression="UserName">
                            <HeaderStyle Width="15%" HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                        <asp:BoundField HeaderText="DateCreated" DataFormatString="{0:dd-MM-yyyy}" ItemStyle-Wrap="false"
                            DataField="ReasonDate" SortExpression="ReasonDate">
                            <HeaderStyle HorizontalAlign="Center" Width="45%" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                    </Columns>
                </cc1:ucGridView>
            </td>
        </tr>
        <tr>
            <td>
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
            </td>
        </tr>
    </table>
</asp:Content>

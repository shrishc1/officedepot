﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using BusinessEntities.ModuleBE.Discrepancy.APAction;
using BusinessLogicLayer.ModuleBAL.Discrepancy.CountrySetting;


public partial class AccountsPayable_DebitReasonSetUpOverview : CommonPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        UIUtility.PageValidateForODUser();

        if (!IsPostBack)
        {
            ucExportToExcel1.Visible = false;
            BindDebitReasonSetUpGrid();
        }
        ucExportToExcel1.CurrentPage = this;
        ucExportToExcel1.GridViewControl = gvDebitReasonSetUp;
        ucExportToExcel1.FileName = "DebitReason";
    }

    private void BindDebitReasonSetUpGrid()
    {
        var debitReasonTypeBAL = new DIS_DebitReasonTypeBAL();
        var apActionBE = new APActionBE();
        apActionBE.Action = "GetReasonTypeSetUp";
        apActionBE.ReasonType = "DR"; /* Debit Reason */
        var lstAPActionBE = debitReasonTypeBAL.GetReasonTypeBAL(apActionBE);

        if (lstAPActionBE != null && lstAPActionBE.Count > 0)
        {
            ucExportToExcel1.Visible = true;
            gvDebitReasonSetUp.DataSource = lstAPActionBE;
            gvDebitReasonSetUp.DataBind();
            ViewState["lstAPActionBE"] = lstAPActionBE;
        }
    }

    public override BusinessEntities.BaseBe[] SubSort(GridViewSortEventArgs e)
    {
        return Utilities.GenericListHelper<APActionBE>.SortList((List<APActionBE>)ViewState["lstAPActionBE"], e.SortExpression, e.SortDirection).ToArray();
    }
}
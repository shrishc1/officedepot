﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    AutoEventWireup="true" CodeFile="DebitReasonSetUpEdit.aspx.cs" Inherits="AccountsPayable_DebitReasonSetUpEdit" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h2>
        <cc1:ucLabel ID="lblCreateEditDebitReasonType" runat="server" Text="Create / Edit Debit Reason Type"></cc1:ucLabel>
    </h2>
    <div>
        <asp:RequiredFieldValidator ID="rfvDebitReasonSet" ErrorMessage="Please enter Reason."
            runat="server" ControlToValidate="txtDebitReason" Display="None" ValidationGroup="a"> </asp:RequiredFieldValidator>
        <asp:ValidationSummary ID="vSummary" runat="server" ShowMessageBox="true" ShowSummary="false"
            Style="color: Red" ValidationGroup="a" />
    </div>
    <div class="right-shadow">
        <div class="formbox">
            <table width="60%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                <tr>
                    <td style="width: 5%">
                    </td>
                    <td style="font-weight: bold">
                        <cc1:ucLabel ID="lblReason" runat="server" Text="Reason" isRequired="true"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold" align="center">
                        :
                    </td>
                    <td>
                        <cc1:ucTextbox ID="txtDebitReason" runat="server" Width="90%" MaxLength="100"></cc1:ucTextbox>
                    </td>
                </tr>
                <tr>
                    <td style="width: 5%">
                    </td>
                    <td style="font-weight: bold">
                        <%--<cc1:ucLabel ID="lblIsBOPenaltyRelated" runat="server" Text="Is BO Penalty Related"></cc1:ucLabel>--%>
                    </td>
                    <td style="font-weight: bold" align="center">
                        <%--:--%>
                    </td>
                    <td class="nobold radiobuttonlist">
                        <cc1:ucCheckbox ID="chkIsBOPenaltyRelated" runat="server" />                        
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div class="bottom-shadow">
    </div>
    <div class="button-row">
        <cc1:ucButton ID="btnSave" runat="server" Text="Save" CssClass="button" OnClick="btnSave_Click" />
        <cc1:ucButton ID="btnBack" runat="server" Text="Back" CssClass="button" OnClick="btnBack_Click" />
    </div>
</asp:Content>

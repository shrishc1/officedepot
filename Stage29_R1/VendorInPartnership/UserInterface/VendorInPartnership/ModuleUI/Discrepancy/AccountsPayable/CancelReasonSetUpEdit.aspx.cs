﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.Discrepancy.APAction;
using BusinessLogicLayer.ModuleBAL.Discrepancy.CountrySetting;
using WebUtilities;

public partial class AccountsPayable_CancelReasonSetUpEdit : CommonPage
{
    string entereddebitreasontypealreadyexist = WebCommon.getGlobalResourceValue("Entereddebitreasontypealreadyexist");
    string pleaseenterthereason = WebCommon.getGlobalResourceValue("Pleaseenterthereason");

    protected void Page_Load(object sender, EventArgs e)
    {
        UIUtility.PageValidateForODUser();
        if (!IsPostBack)
        {
            GetDebitReasonType();
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(txtCancelReason.Text) || string.IsNullOrWhiteSpace(txtCancelReason.Text))
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('" + pleaseenterthereason + "')", true);
            txtCancelReason.Focus();
            return;
        }
        else
        {
            var debitReasonTypeBAL = new DIS_DebitReasonTypeBAL();
            var apActionBE = new APActionBE();

            if (GetQueryStringValue("DebitReasonID") != null)
            {
                apActionBE.Action = "CheckExistanceUpdate";
                apActionBE.Reason = txtCancelReason.Text.Trim();
                apActionBE.ReasonType = "CDR"; /* Cancel Debit Reason */
                apActionBE.ReasonTypeID = Convert.ToInt32(GetQueryStringValue("DebitReasonID"));
                if (debitReasonTypeBAL.CheckExistanceBAL(apActionBE) > 0)
                {
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('" + entereddebitreasontypealreadyexist + "')", true);
                    txtCancelReason.Focus();
                    return;
                }
                else
                {
                    apActionBE.Action = "Update";
                }
            }
            else
            {
                apActionBE.Action = "CheckExistance";
                apActionBE.Reason = txtCancelReason.Text.Trim();
                apActionBE.ReasonType = "CDR"; /* Cancel Debit Reason */
                if (debitReasonTypeBAL.CheckExistanceBAL(apActionBE) > 0)
                {
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('" + entereddebitreasontypealreadyexist + "')", true);
                    txtCancelReason.Focus();
                    return;
                }
                else
                {
                    apActionBE.Action = "Add";
                }
            }

            apActionBE.Reason = txtCancelReason.Text.Trim();
            apActionBE.ReasonType = "CDR"; /* Cancel Debit Reason */
            apActionBE.UserID = Convert.ToInt32(Session["UserID"]);
            debitReasonTypeBAL.addEditReasonTypeBAL(apActionBE);
            EncryptQueryString("CancelReasonSetUpOverview.aspx");
        }
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        EncryptQueryString("CancelReasonSetUpOverview.aspx");
    }

    private void GetDebitReasonType()
    {
        if (GetQueryStringValue("DebitReasonID") != null)
        {
            int reasonTypeID = Convert.ToInt32(GetQueryStringValue("DebitReasonID"));
            var debitReasonTypeBAL = new DIS_DebitReasonTypeBAL();
            var apActionBE = new APActionBE();
            apActionBE.Action = "GetReasonTypeSetUp";
            apActionBE.ReasonType = "CDR"; /* Cancel Debit Reason */
            var resultActionBE = debitReasonTypeBAL.GetReasonTypeBAL(apActionBE).Find(x => x.ReasonTypeID.Equals(reasonTypeID));
            if (resultActionBE != null)
            {
                txtCancelReason.Text = resultActionBE.Reason;
            }
        }
    }

    //private void GetCreditReasonType()
    //{
    //    if (GetQueryStringValue("CancelReasonID") != null)
    //    {
    //        txtCancelReason.Text = GetQueryStringValue("CancelReasonID").ToString();

    //    }
    //}
}
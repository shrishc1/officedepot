﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    AutoEventWireup="true" CodeFile="DISLog_QueryDiscrepancy.aspx.cs" Inherits="DISLog_QueryDiscrepancy" %>
<%@ Register Src="~/CommonUI/UserControls/ucSite.ascx" TagName="ucSite" TagPrefix="cc2" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register TagName="ucDate" TagPrefix="cc2" Src="~/CommonUI/UserControls/ucDate.ascx" %>
<%@ Register Src="~/ModuleUI/Discrepancy/UserControl/ucSDRCommunication.ascx" TagName="ucSDRCommunication"
    TagPrefix="uc2" %>
<%@ Register Src="~/CommonUI/UserControls/ucExportToExcel.ascx" TagName="ucExportToExcel"
    TagPrefix="cc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script language="javascript" type="text/javascript">
        $(document).ready(function () {
            $("body").keypress(function (e) {
                if (e.which == 13 || e.keyCode == 13) {
                    $('#<%=btSearch.ClientID %>').click();
                    return false;
                }
            });
        });
        function checkBlankEmailAddress() {
            var rbEmailComm = document.getElementById('<%=ucSDRCommunication1.FindControl("rdoEmailComm").ClientID %>');
            var objTxtAltEmail = document.getElementById('<%=ucSDRCommunication1.FindControl("txtAltEmail").ClientID %>');
            var objucTextBox1 = document.getElementById('<%=ucSDRCommunication1.FindControl("ucVendorEmailList").ClientID %>');
            if (checkEmailAddress(rbEmailComm, objTxtAltEmail, objucTextBox1, '<%=ValidEmail %>', '<%=NoEmailSpecifiedMessage%>') == false) {

                return false;
            }
        }

        function getScroll1(ctrDiv) {
            document.getElementById('<%=scroll2.ClientID%>').value = ctrDiv.scrollLeft;
        }


        function RedirectToSearchPage() {            
                window.location.href = '<%=EncURL%>';           
        }

        $("[id*=rblActions] input").live("click", function () {
            var selectedValue = $(this).val();
            if (selectedValue == 'P') {
                $('#<%=lblPleaseEnterYourResponseToTheVendorHere.ClientID%>').hide();
                $('#<%=txtODComment.ClientID%>').hide();
                $('#<%=txtODComment.ClientID%>').val('');
            }
            else {
                $('#<%=lblPleaseEnterYourResponseToTheVendorHere.ClientID%>').show();
                $('#<%=txtODComment.ClientID%>').show();
            }
        });


    </script>
    <input type="hidden" id="scroll2" runat="server" />
    <asp:ScriptManager ID="sp1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="up1" runat="server">
        <ContentTemplate>
            <h2>
                <cc1:ucLabel ID="lblQueryDiscrepancy" runat="server" Text="Query Discrepancy"></cc1:ucLabel>
            </h2>
            <b>&nbsp;
                <cc1:ucLabel ID="lblDiscrepancyNoAndType" Text="DiscrepancyNoAndType" runat="server"
                    Font-Bold="true" Font-Size="Larger" Visible="false"></cc1:ucLabel></b>
            <!-- Add comment Section -->
            <div class="button-row">
                <cc1:ucButton ID="btnAddComment" Text="Add Comment" runat="server" class="button"
                    OnClick="btnAddComment_Click" Visible="false" />
            </div>
            <table width="100%">
                <tr><td align="right">
                <cc1:ucButton ID="btnViewWF" runat="server" Text="Workflow" class="button" OnClick="btnViewWF_Click"  Visible="false" />
            </td>
                    <td width="5%">&nbsp;</td>
                </tr>
             <tr><td colspan="2">&nbsp;</td></tr>
            </table>
            <!-- Galary Section -->
            <%-- <div id="gallery" style="display: none" runat="server" clientidmode="Static">
                <ul>
                    <li><a href="../../../Images/banner7.jpg"></a></li>
                    <li><a href="../../../Images/banner6.jpg"></a></li>
                    <li><a href="../../../Images/banner5.jpg"></a></li>
                </ul>
            </div>--%>
            <!-- Query Discrepancy Main Design Section -->
            <div class="right-shadow">
                <div class="formbox">
                    <cc1:ucMultiView ID="mvDiscrepancyShortageCriteria" runat="server" ActiveViewIndex="0">
                        <!-- Search Query Discrepancy Section -->
                        <cc1:ucView ID="vwQueryDiscrepancy" runat="server">
                            <div class="button-row">
                                <cc1:ucButton ID="btnViewDisportReport" Text=" View Dispute Report" runat="server" Visible="false"
                                    CssClass="button"  OnClientClick="RedirectToSearchPage();" />
                            </div>
                            <cc1:ucPanel ID="pnlLogaQuery" runat="server" CssClass="fieldset-form">
                                <table width="70%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                                  
                                        <td style="font-weight: bold; width: 100%" colspan="2">
                                            <cc1:ucLabel ID="lblEnterTheDiscrepancyNumberYouWishToCreate" runat="server" Text="Enter the discrepancy number you wish to create a query for then click Search"></cc1:ucLabel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: bold; width: 50%" >
                                            <cc1:ucTextbox ID="txtDiscrepancyNumber" runat="server"></cc1:ucTextbox>
                                        </td>
                                        <td style="width: 50%" align="left" >
                                            <cc1:ucButton ID="btnSearch" Text="Search" runat="server" OnClick="btnSearch_Click"
                                                CssClass="button" />
                                        </td>
                                    </tr>
                                </table>
                            </cc1:ucPanel>

                            <cc1:ucPanel ID="pnlSearchQuery" runat="server" CssClass="fieldset-form">
                                <table width="70%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                                    <tr>
                                        <td style="font-weight: bold;">
                                             <cc1:ucLabel ID="lblSitePrefixName" runat="server"></cc1:ucLabel>
                                            &nbsp;:&nbsp;&nbsp;
                                        </td>
                                        <td><cc2:ucSite ID="ddlSite" runat="server" /></td>
                                        <td style="font-weight: bold;">
                                              <cc1:ucLabel ID="lblStatus" runat="server"></cc1:ucLabel>
                                            &nbsp;:&nbsp;&nbsp;
                                        </td>
                                        <td style="font-weight: bold;">
                                              <asp:DropDownList ID="ddlQueryStatus" runat="server" Width="150px">
                                                <asp:ListItem Text="--All--" Value="" />
                                                <asp:ListItem Text="Open" Value="O" />
                                                <asp:ListItem Text="Closed" Value="C" />
                                            </asp:DropDownList>
                                        </td>
                                        <td>
                                             <cc1:ucButton ID="btSearch" Text="Search" runat="server" 
                                              OnClick="btSearch_Click"   CssClass="button" />
                                        </td>
                                    </tr>
                                   
                                </table>
                            </cc1:ucPanel>

                            <div class="button-row">
                                <cc2:ucExportToExcel ID="ucExportToExcel1" runat="server" />
                            </div>
                            <div style="width: 100%; height: 440px; overflow: scroll" id="divPrint" class="fixedTable"
                                onscroll="getScroll1(this);">
                                <table cellspacing="1" cellpadding="0" class="form-table">
                                    <tr>
                                        <td>
                                            <cc1:ucPanel ID="pnlQueryOverview" runat="server" CssClass="fieldset-form" GroupingText="Query Overview">
                                                <cc1:ucGridView ID="gvDisDispute" runat="server" AutoGenerateColumns="false" CssClass="grid gvclass"
                                                    CellPadding="0" Width="1500px" AllowSorting="false" AllowPaging="true" PageSize="30"
                                                    OnPageIndexChanging="gvDisDispute_PageIndexChanging" OnRowDataBound="gvDisDispute_RowDataBound">
                                                    <AlternatingRowStyle BackColor="White" ForeColor="#284775"></AlternatingRowStyle>
                                                    <EmptyDataTemplate>
                                                        <div style="text-align: center">
                                                            <cc1:ucLabel ID="lblNoData" runat="server" Text="No Records found."></cc1:ucLabel>
                                                        </div>
                                                    </EmptyDataTemplate>
                                                    <Columns>
                                                        <asp:BoundField HeaderText="Vendor #" DataField="Vendor_No">
                                                            <HeaderStyle Width="120px" />
                                                            <ItemStyle HorizontalAlign="Center" Width="120px" />
                                                        </asp:BoundField>
                                                        <asp:BoundField HeaderText="Vendor Name" DataField="Vendor_Name">
                                                            <HeaderStyle Width="200px" />
                                                            <ItemStyle HorizontalAlign="Center" Width="400px" />
                                                        </asp:BoundField>
                                                        <asp:TemplateField HeaderText="Date Query Rasied">
                                                            <HeaderStyle Width="100px" />
                                                            <ItemStyle HorizontalAlign="Center" Width="100px" />
                                                            <ItemTemplate>
                                                                <cc1:ucLabel ID="lblVendorQueryDate" runat="server" Text='<%# Eval("VendorQueryDate", "{0:dd/MM/yyyy}") %>'></cc1:ucLabel>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:BoundField HeaderText="Who raised the issue" DataField="VendorUserName">
                                                            <HeaderStyle Width="150px" />
                                                            <ItemStyle HorizontalAlign="left" Width="200px" />
                                                        </asp:BoundField>
                                                        <asp:BoundField HeaderText="Query Status" DataField="Query">
                                                            <HeaderStyle Width="100px" />
                                                            <ItemStyle HorizontalAlign="Center" Width="100px" />
                                                        </asp:BoundField>
                                                        <asp:TemplateField HeaderText="Date Discrepancy Raised">
                                                            <HeaderStyle Width="100px" />
                                                            <ItemStyle HorizontalAlign="Center" Width="100px" />
                                                            <ItemTemplate>
                                                                <cc1:ucLabel ID="lblDiscrepancyLogDate" runat="server" Text='<%# Eval("Discrepancy.DiscrepancyLogDate", "{0:dd/MM/yyyy}") %>'></cc1:ucLabel>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Discrepancy Number">
                                                            <HeaderStyle Width="150px" />
                                                            <ItemStyle HorizontalAlign="Center" />
                                                            <ItemTemplate>
                                                                <asp:HiddenField ID="hdnDisLogID" runat="server" Value='<%#Eval("Discrepancy.DiscrepancyLogID") %>' />
                                                                <asp:HiddenField ID="hdnQueryDisId" runat="server" Value='<%#Eval("QueryDiscrepancyID") %>' />
                                                                <asp:HyperLink ID="hypLinkToDisDetails" runat="server" Text='<%#Eval("Discrepancy.VDRNo") %>'></asp:HyperLink>
                                                                 
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:BoundField HeaderText="Detail of Query" DataField="VendorComment">
                                                            <HeaderStyle Width="250px" />
                                                            <ItemStyle HorizontalAlign="Left" Width="500px" />
                                                        </asp:BoundField>
                                                        <asp:BoundField HeaderText="Office Depot Response" DataField="GoodsInComment">
                                                            <HeaderStyle Width="250" />
                                                            <ItemStyle HorizontalAlign="Left" Width="500px" />
                                                        </asp:BoundField>
                                                        <asp:BoundField HeaderText="Date of Response" DataField="GoodsInQueryDate" DataFormatString="{0:dd/MM/yyyy}">
                                                            <HeaderStyle Width="100px" />
                                                            <ItemStyle HorizontalAlign="Center" Width="100px" />
                                                        </asp:BoundField>
                                                        <asp:BoundField HeaderText="Responded by" DataField="GoodsInUserName">
                                                            <HeaderStyle Width="150px" />
                                                            <ItemStyle HorizontalAlign="Center" Width="100px" />
                                                        </asp:BoundField>
                                                        <asp:BoundField HeaderText="Elasped Time to Respond" DataField="ElaspedTime">
                                                            <HeaderStyle Width="100px" />
                                                            <ItemStyle HorizontalAlign="Center" Width="100px" />
                                                        </asp:BoundField>
                                                    </Columns>
                                                </cc1:ucGridView>
                                            </cc1:ucPanel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <cc1:ucGridView ID="gvExport" Visible="false" runat="server" AutoGenerateColumns="false" CssClass="grid gvclass"
                                                    CellPadding="0" Width="1500px" AllowSorting="false"
                                                    >
                                                    <AlternatingRowStyle BackColor="White" ForeColor="#284775"></AlternatingRowStyle>
                                                    <EmptyDataTemplate>
                                                        <div style="text-align: center">
                                                            <cc1:ucLabel ID="lblNoData" runat="server" Text="No Records found."></cc1:ucLabel>
                                                        </div>
                                                    </EmptyDataTemplate>
                                                    <Columns>
                                                        <asp:BoundField HeaderText="Vendor #" DataField="Vendor_No">
                                                            <HeaderStyle Width="120px" />
                                                            <ItemStyle HorizontalAlign="Center" Width="120px" />
                                                        </asp:BoundField>
                                                        <asp:BoundField HeaderText="Vendor Name" DataField="Vendor_Name">
                                                            <HeaderStyle Width="200px" />
                                                            <ItemStyle HorizontalAlign="Center" Width="400px" />
                                                        </asp:BoundField>
                                                        <asp:TemplateField HeaderText="Date Query Rasied">
                                                            <HeaderStyle Width="100px" />
                                                            <ItemStyle HorizontalAlign="Center" Width="100px" />
                                                            <ItemTemplate>
                                                                <cc1:ucLabel ID="lblVendorQueryDate" runat="server" Text='<%# Eval("VendorQueryDate", "{0:dd/MM/yyyy}") %>'></cc1:ucLabel>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:BoundField HeaderText="Who raised the issue" DataField="VendorUserName">
                                                            <HeaderStyle Width="150px" />
                                                            <ItemStyle HorizontalAlign="left" Width="200px" />
                                                        </asp:BoundField>
                                                        <asp:BoundField HeaderText="Query Status" DataField="Query">
                                                            <HeaderStyle Width="100px" />
                                                            <ItemStyle HorizontalAlign="Center" Width="100px" />
                                                        </asp:BoundField>
                                                        <asp:TemplateField HeaderText="Date Discrepancy Raised">
                                                            <HeaderStyle Width="100px" />
                                                            <ItemStyle HorizontalAlign="Center" Width="100px" />
                                                            <ItemTemplate>
                                                                <cc1:ucLabel ID="lblDiscrepancyLogDate" runat="server" Text='<%# Eval("Discrepancy.DiscrepancyLogDate", "{0:dd/MM/yyyy}") %>'></cc1:ucLabel>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Discrepancy Number">
                                                            <HeaderStyle Width="150px" />
                                                            <ItemStyle HorizontalAlign="Center" />
                                                            <ItemTemplate>
                                                                <asp:HiddenField ID="hdnDisLogID" runat="server" Value='<%#Eval("Discrepancy.DiscrepancyLogID") %>' />
                                                                <asp:HiddenField ID="hdnQueryDisId" runat="server" Value='<%#Eval("QueryDiscrepancyID") %>' />
                                                                <asp:HyperLink ID="hypLinkToDisDetails" runat="server" Text='<%#Eval("Discrepancy.VDRNo") %>'></asp:HyperLink>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:BoundField HeaderText="Detail of Query" DataField="VendorComment">
                                                            <HeaderStyle Width="250px" />
                                                            <ItemStyle HorizontalAlign="Left" Width="500px" />
                                                        </asp:BoundField>
                                                        <asp:BoundField HeaderText="Office Depot Response" DataField="GoodsInComment">
                                                            <HeaderStyle Width="250" />
                                                            <ItemStyle HorizontalAlign="Left" Width="500px" />
                                                        </asp:BoundField>
                                                        <asp:BoundField HeaderText="Date of Response" DataField="GoodsInQueryDate" DataFormatString="{0:dd/MM/yyyy}">
                                                            <HeaderStyle Width="100px" />
                                                            <ItemStyle HorizontalAlign="Center" Width="100px" />
                                                        </asp:BoundField>
                                                        <asp:BoundField HeaderText="Responded by" DataField="GoodsInUserName">
                                                            <HeaderStyle Width="150px" />
                                                            <ItemStyle HorizontalAlign="Center" Width="100px" />
                                                        </asp:BoundField>
                                                        <asp:BoundField HeaderText="Elasped Time to Respond" DataField="ElaspedTime">
                                                            <HeaderStyle Width="100px" />
                                                            <ItemStyle HorizontalAlign="Center" Width="100px" />
                                                        </asp:BoundField>
                                                    </Columns>
                                                </cc1:ucGridView>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </cc1:ucView>
                        <!-- Add Dispute Section -->
                        <cc1:ucView ID="vwQueryDiscrepancyAddDispute" runat="server">
                            <table width="100%" cellspacing="0" cellpadding="0" class="form-table">
                                <tr valign="top">
                                    <td width="30%">
                                        <cc1:ucPanel ID="pnlDeliveryDetail" runat="server" GroupingText="Delivery Detail"
                                            CssClass="fieldset-form">
                                            <table width="100%" cellspacing="1" cellpadding="0" class="form-table" height="140px">
                                                <tr>
                                                    <td style="font-weight: bold; width: 42%">
                                                        <cc1:ucLabel ID="lblSiteDetail" runat="server" Text="Site"></cc1:ucLabel>
                                                    </td>
                                                    <td style="font-weight: bold; width: 2%">
                                                        :
                                                    </td>
                                                    <td class="nobold" colspan="2">
                                                        <cc1:ucLabel ID="lblSiteValue" runat="server"></cc1:ucLabel>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="font-weight: bold;">
                                                        <cc1:ucLabel ID="lblPurchaseOrderNo" runat="server" Text="Purchase Order #"></cc1:ucLabel>
                                                    </td>
                                                    <td style="font-weight: bold;">
                                                        :
                                                    </td>
                                                    <td class="nobold" style="width: 22%">
                                                        <cc1:ucLabel ID="lblPurchaseOrderValue" runat="server"></cc1:ucLabel>
                                                    </td>
                                                    <td class="nobold" style="width: 34%">
                                                        <cc1:ucLabel ID="lblPurchaseOrderDateValue" runat="server"></cc1:ucLabel>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="font-weight: bold;">
                                                        <cc1:ucLabel ID="lblIssueRaisedDate2" runat="server" Text="Issue Raised Date"></cc1:ucLabel>
                                                    </td>
                                                    <td style="font-weight: bold;">
                                                        :
                                                    </td>
                                                    <td class="nobold">
                                                        <cc1:ucLabel ID="lblIssueRaisedDateValue" runat="server"></cc1:ucLabel>
                                                    </td>
                                                    <td class="nobold">
                                                        <cc1:ucLabel ID="lblIssueRaisedTimeValue" runat="server"></cc1:ucLabel>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="font-weight: bold;">
                                                        <cc1:ucLabel ID="lblDeliveryNo" runat="server" Text="Delivery #"></cc1:ucLabel>
                                                    </td>
                                                    <td style="font-weight: bold;">
                                                        :
                                                    </td>
                                                    <td class="nobold">
                                                        <cc1:ucLabel ID="lblDeliveryNoValue" runat="server"></cc1:ucLabel>
                                                    </td>
                                                    <td class="nobold">
                                                        <cc1:ucLabel ID="lblDeliveryDateValue" runat="server"></cc1:ucLabel>
                                                    </td>
                                                </tr>
                                            </table>
                                        </cc1:ucPanel>
                                    </td>
                                    <td width="2%">
                                        &nbsp;
                                    </td>
                                    <td width="35%">
                                        <cc1:ucPanel ID="pnlContactDetail" runat="server" GroupingText="Contact Detail" CssClass="fieldset-form">
                                            <table width="100%" height="140px">
                                                <tr>
                                                    <td>
                                                        <table width="100%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings"
                                                            height="62px">
                                                            <tr>
                                                                <td style="font-weight: bold; width: 30%">
                                                                    <cc1:ucLabel ID="lblVendor_1" runat="server" Text="Vendor "></cc1:ucLabel>
                                                                </td>
                                                                <td style="font-weight: bold; width: 2%">
                                                                    :
                                                                </td>
                                                                <td class="nobold" style="width: 68%">
                                                                    <cc1:ucLabel ID="lblVendorNumberValue" runat="server"></cc1:ucLabel>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="font-weight: bold;">
                                                                    <cc1:ucLabel ID="lblContactNo" runat="server" Text="Contact #"></cc1:ucLabel>
                                                                </td>
                                                                <td style="font-weight: bold;">
                                                                    :
                                                                </td>
                                                                <td class="nobold">
                                                                    <cc1:ucLabel ID="lblContactValue" runat="server"></cc1:ucLabel>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <table width="100%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings"
                                                            height="61px">
                                                            <tr>
                                                                <td style="font-weight: bold; width: 30%">
                                                                    <cc1:ucLabel ID="lblStockPlanner" runat="server" Text="Stock Planner"></cc1:ucLabel>
                                                                </td>
                                                                <td style="font-weight: bold; width: 2%">
                                                                    :
                                                                </td>
                                                                <td class="nobold" style="width: 68%">
                                                                    <cc1:ucLabel ID="lblStockPlannerNoValue" runat="server"></cc1:ucLabel>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="font-weight: bold;">
                                                                    <cc1:ucLabel ID="lblContactNo_1" runat="server" Text="Contact #"></cc1:ucLabel>
                                                                </td>
                                                                <td style="font-weight: bold;">
                                                                    :
                                                                </td>
                                                                <td class="nobold">
                                                                    <cc1:ucLabel ID="lblPlannerContactNoValue" runat="server"></cc1:ucLabel>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </cc1:ucPanel>
                                    </td>
                                    <td width="2%">
                                        &nbsp;
                                    </td>
                                    <td width="31%" rowspan="7" valign="top">
                                        <cc1:ucPanel ID="pnlCommunicationDetail" runat="server" GroupingText="Communication Detail"
                                            CssClass="fieldset-form">
                                            <table width="100%" cellspacing="0" cellpadding="0" class="form-table" height="140px">
                                                <tr>
                                                    <td>
                                                        <uc2:ucSDRCommunication ID="ucSDRCommunication1" runat="server" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </cc1:ucPanel>
                                    </td>
                                </tr>
                            </table>
                            <cc1:ucPanel ID="pnlGrid" runat="server" ScrollBars="Both" CssClass="fieldset-form">
                                <cc1:ucGridView ID="gvQueryDiscrepancy" Width="98%" Height="80%" runat="server" CssClass="grid"
                                    >
                                    <Columns>
                                        <asp:TemplateField HeaderText="Line" SortExpression="Line_no" Visible="false">
                                            <HeaderStyle Width="8%" HorizontalAlign="Left" />
                                            <ItemTemplate>
                                                <cc1:ucLabel runat="server" ID="lblLineNo2" Text='<%# Eval("Line_no") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="OfficeDepotCode" SortExpression="ODSKUCode" Visible="false">
                                            <HeaderStyle Width="8%" HorizontalAlign="Left" />
                                            <ItemTemplate>
                                                <cc1:ucLabel runat="server" ID="lblOfficeDepotCode2" Text='<%# Eval("ODSKUCode") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="VikingCode" SortExpression="DirectCode" Visible="false">
                                            <HeaderStyle Width="8%" HorizontalAlign="Left" />
                                            <ItemTemplate>
                                                <cc1:ucLabel runat="server" ID="lblVikingCode2" Text='<%# Eval("DirectCode") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="VendorItemCode" SortExpression="VendorCode" Visible="false">
                                            <HeaderStyle Width="8%" HorizontalAlign="Left" />
                                            <ItemTemplate>
                                                <cc1:ucLabel runat="server" ID="lblVendorItemCode2" Text='<%# Eval("VendorCode") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="DESCRIPTION" SortExpression="ProductDescription" Visible="false">
                                            <HeaderStyle Width="35%" HorizontalAlign="Left" />
                                            <ItemTemplate>
                                                <cc1:ucLabel runat="server" ID="lblDescription2" Text='<%# Eval("ProductDescription") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="OriginalPOQuantity" SortExpression="OriginalQuantity"
                                            Visible="false">
                                            <HeaderStyle Width="5%" HorizontalAlign="Left" />
                                            <ItemTemplate>
                                                <cc1:ucLabel runat="server" ID="lblOriginalPOQuantity2" Text='<%# Eval("OriginalQuantity") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="OutstandingPOQuantity" SortExpression="OutstandingQuantity"
                                            Visible="false">
                                            <HeaderStyle Width="5%" HorizontalAlign="Left" />
                                            <ItemTemplate>
                                                <cc1:ucLabel runat="server" ID="lblOutstandingQty2" Text='<%# Eval("OutstandingQuantity") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="ReceivedQty" SortExpression="DeliveredQuantity" Visible="false">
                                            <HeaderStyle Width="5%" HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <cc1:ucLabel runat="server" ID="lblShortage2" Text='<%# Eval("DeliveredQuantity") %>'
                                                    Width="50px"></cc1:ucLabel>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="UOM" SortExpression="UOM" Visible="false">
                                            <HeaderStyle Width="5%" HorizontalAlign="Left" />
                                            <ItemTemplate>
                                                <cc1:ucLabel runat="server" ID="lblUoM2" Text='<%# Eval("UOM") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="DeliveryNoteQuantity" SortExpression="DeliveredNoteQuantity"
                                            Visible="false" ItemStyle-HorizontalAlign="Center">
                                            <HeaderStyle Width="5%" HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <cc1:ucLabel runat="server" ID="lblDeliveryNoteQty2" Text='<%# Eval("DeliveredNoteQuantity") %>'
                                                    Width="50px"></cc1:ucLabel>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Shortage" SortExpression="ShortageQuantity" Visible="false" ItemStyle-HorizontalAlign="Center">
                                            <HeaderStyle Width="5%" HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <cc1:ucLabel runat="server" ID="lblShortage2" Text='<%# Eval("ShortageQuantity") %>'
                                                    Width="50px"></cc1:ucLabel>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="DeliveredQuantity" SortExpression="DeliveredQuantity"
                                            Visible="false" ItemStyle-HorizontalAlign="Center">
                                            <HeaderStyle Width="5%" HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <cc1:ucLabel runat="server" ID="lblShortage2" Text='<%# Eval("DeliveredQuantity") %>'
                                                    Width="50px"></cc1:ucLabel>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="OversQuantity" SortExpression="OversQuantity" Visible="false" ItemStyle-HorizontalAlign="Center">
                                            <HeaderStyle Width="5%" HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <cc1:ucLabel runat="server" ID="lblShortage2" Text='<%# Eval("OversQuantity") %>'
                                                    Width="50px"></cc1:ucLabel>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="DamageQuantity" SortExpression="DamageQuantity" Visible="false">
                                            <HeaderStyle Width="5%" HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <cc1:ucLabel runat="server" ID="lblShortage2" Text='<%# Eval("DamageQuantity") %>'
                                                    Width="50px"></cc1:ucLabel>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="DamageDescription" SortExpression="DamageDescription"
                                            Visible="false">
                                            <HeaderStyle Width="5%" HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <cc1:ucLabel runat="server" ID="lblShortage2" Text='<%# Eval("DamageDescription") %>'
                                                    Width="50px"></cc1:ucLabel>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="QuantityAmendedTo" SortExpression="AmendedQuantity"
                                            Visible="false" ItemStyle-HorizontalAlign="Center">
                                            <HeaderStyle Width="5%" HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <cc1:ucLabel runat="server" ID="lblShortage2" Text='<%# Eval("AmendedQuantity") %>'
                                                    Width="50px"></cc1:ucLabel>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="NoPOEscalate" SortExpression="NoPOEscalate" Visible="false">
                                            <HeaderStyle Width="5%" HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <cc1:ucLabel runat="server" ID="lblShortage2" Text='<%# Eval("NoPOEscalate") %>'
                                                    Width="50px"></cc1:ucLabel>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="PackSizeOrdered" SortExpression="PackSizeOrdered"
                                            Visible="false">
                                            <HeaderStyle Width="5%" HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <cc1:ucLabel runat="server" ID="lblShortage2" Text='<%# Eval("PackSizeOrdered") %>'
                                                    Width="50px"></cc1:ucLabel>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="PackSizeReceived" SortExpression="PackSizeReceived"
                                            Visible="false">
                                            <HeaderStyle Width="5%" HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <cc1:ucLabel runat="server" ID="lblShortage2" Text='<%# Eval("PackSizeReceived") %>'
                                                    Width="50px"></cc1:ucLabel>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="ProductReceivedCode" SortExpression="CorrectODSKUCode"
                                            Visible="false">
                                            <HeaderStyle Width="5%" HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <cc1:ucLabel runat="server" ID="lblShortage2" Text='<%# Eval("CorrectODSKUCode") %>'
                                                    Width="50px"></cc1:ucLabel>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="CorrectProductDescription" SortExpression="CorrectProductDescription"
                                            Visible="false">
                                            <HeaderStyle Width="35%" HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <cc1:ucLabel runat="server" ID="lblShortage2" Text='<%# Eval("CorrectProductDescription") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="CorrectUOM" SortExpression="CorrectUOM" Visible="false">
                                            <HeaderStyle Width="5%" HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <cc1:ucLabel runat="server" ID="lblShortage2" Text='<%# Eval("CorrectUOM") %>' Width="50px"></cc1:ucLabel>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="CorrectVendorCode" SortExpression="CorrectVendorCode"
                                            Visible="false">
                                            <HeaderStyle Width="5%" HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <cc1:ucLabel runat="server" ID="lblShortage2" Text='<%# Eval("CorrectVendorCode") %>'
                                                    Width="50px"></cc1:ucLabel>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="NoofPacksReceived" SortExpression="WrongPackReceived"
                                            Visible="false">
                                            <HeaderStyle Width="5%" HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <cc1:ucLabel runat="server" ID="lblShortage2" Text='<%# Eval("WrongPackReceived") %>'
                                                    Width="50px"></cc1:ucLabel>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Type" SortExpression="ShuttleType" Visible="false" ItemStyle-HorizontalAlign="Center">
                                            <HeaderStyle Width="5%" HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <cc1:ucLabel runat="server" ID="lblShortage2" Text='<%# Eval("ShuttleType") %>' Width="50px"></cc1:ucLabel>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="DiscrepancyQty" SortExpression="DiscrepancyQty" Visible="false" ItemStyle-HorizontalAlign="Center">
                                            <HeaderStyle Width="5%" HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <cc1:ucLabel runat="server" ID="lblShortage2" Text='<%# Eval("DiscrepancyQty") %>'
                                                    Width="50px"></cc1:ucLabel>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Issue" SortExpression="DamageDescription" Visible="false" ItemStyle-HorizontalAlign="Center">
                                            <HeaderStyle Width="9%" HorizontalAlign="Left" />
                                            <ItemTemplate>
                                                <cc1:ucLabel ID="lblDamageDescriptionValue" runat="server" Text='<%# Eval("DamageDescription") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                         <asp:TemplateField HeaderText="Quantity" SortExpression="DeliveredQuantity" Visible="false">
                                            <HeaderStyle Width="5%" HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <cc1:ucLabel runat="server" ID="lblQualtyDeliveredQty" Text='<%# Eval("DeliveredQuantity") %>'
                                                    Width="50px"></cc1:ucLabel>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="ChaseQty" SortExpression="ChaseQuantity" Visible="false">
                                            <HeaderStyle Width="5%" HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <cc1:ucLabel runat="server" ID="lblPODChaseQuantity" Text='<%# Eval("ChaseQuantity") %>'
                                                    Width="50px"></cc1:ucLabel>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:TemplateField>

                                          <asp:TemplateField HeaderText="QtyReceiptedByOD" SortExpression="ReceiptedQty" Visible="false">
                                            <HeaderStyle Width="5%" HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <cc1:ucLabel runat="server" ID="lbl_QtyReceiptedByOD" Text='<%# Eval("ReceiptedQty") %>'
                                                    Width="50px"></cc1:ucLabel>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:TemplateField>

                                         <asp:TemplateField HeaderText="INVOICEQUANTITY" SortExpression="InvoiceQty" Visible="false">
                                        <HeaderStyle Width="5%" HorizontalAlign="Right" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="lbl_InvoiceQty" Text='<%# Eval("InvoiceQty") %>'
                                                Width="50px"></cc1:ucLabel></ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    </Columns>
                                </cc1:ucGridView>
                            </cc1:ucPanel>
                            <cc1:ucPanel ID="pnlAction" GroupingText="Action" runat="server" CssClass="fieldset-form">
                                <table width="100%" cellspacing="5" cellpadding="0" border="0" class="form-table">
                                    <tr>
                                        <td>
                                            <cc1:ucLabel ID="lblPleaseEnterAFullDescriptionOfYourQueryWithRegardsToThisDiscrepancy"
                                                runat="server" isRequired="true" Font-Size="13px" CssClass="action-required-heading"
                                                Text="Please enter a full description of your query relating to this discrepancy. We in turn will investigate and will provide feedback once the investigation is completed."></cc1:ucLabel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: bold;">
                                            <asp:TextBox ID="txtInternalComments" runat="server" CssClass="inputbox" TextMode="MultiLine"
                                                onkeyup="checkTextLengthOnKeyUp(this,1000);" Width="98%" MaxLength="1000" Height="70px"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                            </cc1:ucPanel>
                            <cc1:ucPanel ID="pnlVendorsQuery" GroupingText="Vendor's Query" runat="server" CssClass="fieldset-form">
                                <table width="100%" cellspacing="5" cellpadding="0" border="0" class="form-table">
                                    <tr>
                                        <td style="font-weight: bold;">
                                            <asp:TextBox ID="txtVendorInternalComments" MaxLength="1000" runat="server" CssClass="inputbox"
                                                TextMode="MultiLine" onkeyup="checkTextLengthOnKeyUp(this,1000);" Width="98%"
                                                Height="70px" Enabled="false"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                            </cc1:ucPanel>
                            <cc1:ucPanel ID="pnlODAction" GroupingText="Action" runat="server" CssClass="fieldset-form">
                                <table width="100%" cellspacing="5" cellpadding="0" border="0" class="form-table">
                                    <tr class="action-required-heading">
                                        <td align="center" class="action-required-heading">
                                            <cc1:ucLabel ID="lblPleaseReviewTheAboveQueryFromTheVendor" runat="server" isRequired="true"
                                                Font-Size="13px" CssClass="action-required-heading" Text="Please review the above query from the vendor for this discrepancy and investigate."></cc1:ucLabel>
                                        </td>
                                    </tr>
                                    <tr class="action-required-heading">
                                        <td align="center" class="action-required-heading">
                                            <cc1:ucLabel ID="lblAfterYourInvestigationEnterTheDetailsOfYourFindings" runat="server"
                                                isRequired="true" Font-Size="13px" CssClass="action-required-heading" Text="After your investigation, enter the details of your findings and select if you accept or reject the vendors claim."></cc1:ucLabel>
                                        </td>
                                    </tr>
                                    <%--<tr><td><br /></td></tr>--%>
                                    <tr>
                                        <td>
                                            <cc1:ucRadioButtonList ID="rblActions" runat="server" RepeatColumns="3" RepeatDirection="Horizontal">
                                            </cc1:ucRadioButtonList>
                                        </td>
                                    </tr>
                                    <%--<tr><td><br /></td></tr>--%>
                                    <tr>
                                        <td>
                                            <cc1:ucLabel ID="lblPleaseEnterYourResponseToTheVendorHere" runat="server" Text="Please enter your response to the vendor here :"></cc1:ucLabel>                                            
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: bold;">
                                            <asp:TextBox ID="txtODComment" runat="server" CssClass="inputbox" TextMode="MultiLine"
                                                onkeyup="checkTextLengthOnKeyUp(this,1000);" Width="98%" MaxLength="1000" Height="70px"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                            </cc1:ucPanel>
                            <table width="100%" cellspacing="0" cellpadding="0" class="form-table">
                                <tr>
                                    <td>
                                        <!-- Button Save/Back Section -->
                                        <div class="button-row">
                                            <cc1:ucButton ID="btnBack" runat="server" Text="Back" class="button" OnClick="btnBack_Click" />
                                            <input type="button" id="btnReadonlyBack" runat="server" onclick="window.history.back()"
                                                class="button" style="display: none" value="Back" />
                                            <cc1:ucButton ID="btnSave" runat="server" Text="Save" class="button" OnClick="btnSave_Click" />
                                        </div>
                                    </td>
                                </tr>
                            </table>
                            <%--<iframe id="ifUserControl" runat="server" frameborder="0" scrolling="no" height="350px" visible="false"
                                width="954" style="margin: 0px 0px 0px 0px; background-image: url('../Images/conteint-mainbg.jpg') no-repeat scroll right bottom transparent;">
                            </iframe>--%>
                        </cc1:ucView>
                    </cc1:ucMultiView>
                </div>
            </div>
            <!-- Progress Bar Section -->
            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td align="center">
                        <asp:UpdateProgress runat="server" ID="PageUpdateProgress" AssociatedUpdatePanelID="up1">
                            <ProgressTemplate>
                                <div align="center" style="background-color: #000; top: 0px; left: 0px; bottom: 0px;
                                    right: 0px; padding-top: 20%; margin: 0; width: 100%; height: 120%; overflow: hidden;
                                    position: absolute; z-index: 1000; filter: alpha(opacity=50); opacity: 0.5;">
                                    <asp:Image ID="imgWait" runat="server" ImageUrl="~/Images/Ajaxloading.gif" ImageAlign="Middle" />
                                </div>
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnSave" />
            <asp:AsyncPostBackTrigger ControlID="btnBack" />
            <asp:AsyncPostBackTrigger ControlID="btnAddComment"/>
            <asp:PostBackTrigger ControlID="ucExportToExcel1" />
        </Triggers>
    </asp:UpdatePanel>
    <!-- Add Comment Message Section -->
    <asp:UpdatePanel ID="updpnlAddComment" runat="server">
        <ContentTemplate>
            <asp:Button ID="btnAddCommentMPE" runat="Server" Style="display: none" />
            <ajaxToolkit:ModalPopupExtender ID="mdlAddComment" runat="server" TargetControlID="btnAddCommentMPE"
                PopupControlID="pnlAddComment" BackgroundCssClass="modalBackground" BehaviorID="AddComment"
                DropShadow="false" />
            <asp:Panel ID="pnlAddComment" runat="server" Style="display: none; width: 630px">
                <div style="overflow-y: hidden; overflow-x: hidden; background-color: #fff; padding: 5px;
                    border: 2px solid #ccc; text-align: left;">
                    <table cellspacing="5" cellpadding="0" border="0" align="center" class="top-settingsNoBorder-popup">
                        <tr>
                            <td style="font-weight: bold; width: 15%;">
                                <cc1:ucLabel ID="lblUser" Text="User" runat="server"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold; width: 1%;">
                                :
                            </td>
                            <td style="width: 30%;">
                                <cc1:ucLabel ID="lblUserT" runat="server"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold; width: 10%;">
                                <cc1:ucLabel ID="lblDate" Text="Date" runat="server"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold; width: 1%;">
                                :
                            </td>
                            <td style="width: 30%;">
                                <cc1:ucLabel ID="lblDateT" runat="server"></cc1:ucLabel>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold; width: 15%;">
                                <cc1:ucLabel ID="lblDiscrepancyNo" Text="Discrepancy #" runat="server"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold; width: 1%;">
                                :
                            </td>
                            <td style="width: 80%;" colspan="4">
                                <cc1:ucLabel ID="lblDiscrepancyNoT" runat="server"></cc1:ucLabel>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold; width: 100%;" colspan="6">
                                <cc1:ucLabel ID="lblCommentLabel" Text="Comment" runat="server"></cc1:ucLabel>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold; width: 100%;" colspan="6">
                                <cc1:ucTextarea ID="txtUserComments" Height="150px" Width="600px" runat="server"></cc1:ucTextarea>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" style="width: 80%;" colspan="4">
                                <cc1:ucLabel ID="lblErrorMsg" Font-Bold="true" ForeColor="Red" runat="server"></cc1:ucLabel>
                            </td>
                            <td align="right" style="width: 20%;" colspan="2">
                                <cc1:ucButton ID="btnSave_1" runat="server" Text="Save" CssClass="button" OnCommand="btnSave_1_Click" />
                                &nbsp;
                                <cc1:ucButton ID="btnBack_2" runat="server" Text="Back" CssClass="button" OnCommand="btnBack_2_Click" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnSave_1" />
            <asp:AsyncPostBackTrigger ControlID="btnBack_2" />
            <asp:AsyncPostBackTrigger ControlID="btnAddComment" />
        </Triggers>
    </asp:UpdatePanel>
    <!-- Error Message Section -->
    <asp:UpdatePanel ID="updpnlShowError" runat="server">
        <ContentTemplate>
            <asp:Button ID="btnShowErrorMPE" runat="Server" Style="display: none" />
            <ajaxToolkit:ModalPopupExtender ID="mdlShowError" runat="server" TargetControlID="btnShowErrorMPE"
                PopupControlID="pnlShowError" BackgroundCssClass="modalBackground" BehaviorID="ShowError"
                CancelControlID="btnOk" DropShadow="false" />
            <asp:Panel ID="pnlShowError" runat="server" Style="display: none; width: 630px">
                <div style="overflow-y: hidden; overflow-x: hidden; background-color: #fff; padding: 5px;
                    border: 2px solid #ccc; text-align: left;">
                    <table cellspacing="5" cellpadding="0" border="0" align="center" width="100%" class="top-settingsNoBorder-popup">
                        <tr>
                            <td style="font-weight: bold; text-align: center; width: 100%;">
                                <cc1:ucLabel ID="lblError" Font-Bold="true" Font-Size="Medium" ForeColor="Black"
                                    Text="Error" runat="server"></cc1:ucLabel>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: center; width: 100%;">
                                <cc1:ucLabel ID="lblMessageFirst" Text="" ForeColor="Black" runat="server"></cc1:ucLabel>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: center; width: 100%;">
                                <cc1:ucLabel ID="lblMessageSecond" Text="" ForeColor="Black" runat="server"></cc1:ucLabel>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: center; width: 100%;">
                                <cc1:ucButton ID="btnOk" Text="OK" CssClass="button" Width="80px" runat="server">
                                </cc1:ucButton>
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnShowErrorMPE" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>

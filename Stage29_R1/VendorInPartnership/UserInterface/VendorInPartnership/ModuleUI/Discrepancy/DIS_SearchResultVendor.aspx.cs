﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.AdminFunctions;
using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using BusinessEntities.ModuleBE.Discrepancy.LogDiscrepancy;
using BusinessLogicLayer.ModuleBAL.AdminFunctions;
using BusinessLogicLayer.ModuleBAL.Appointment.SiteSettings;
using BusinessLogicLayer.ModuleBAL.Discrepancy.LogDiscrepancy;
using Utilities;
using WebUtilities;

public partial class DIS_SearchResultVendor : CommonPage
{
    #region Page Level Declarations ...
    DiscrepancyBE oNewDiscrepancyBE = null;
    DiscrepancyBAL oNewDiscrepancyBAL = null;
    
    #endregion

    #region Page Level Events ...

    protected void Page_Prerender(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            txtDiscrepancyDateRangeFrom.Attributes.Add("readonly", "readonly");
            txtDiscrepancyDateRangeTo.Attributes.Add("readonly", "readonly");
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        ucExportToExcel1.CurrentPage = this;
        ucExportToExcel1.GridViewControl = gvDisLog;
        ucExportToExcel1.FileName = "SearchDiscrepanciesVendor";

        Label lblModuleText = (Label)Page.Master.FindControl("lblModuleText");
        if (mvDiscrepancyList.ActiveViewIndex == 0)
        {
            lblModuleText.Text = WebCommon.getGlobalResourceValue("DiscrepanciesSearchCriteria"); // "Discrepancies - Search Criteria";
            lblSearchResult.Text = WebCommon.getGlobalResourceValue("SearchCriteria"); // "Search Criteria";
        }
        else
        {
            lblModuleText.Text = WebCommon.getGlobalResourceValue("DiscrepanciesSearchResult"); // "Discrepancies - Search Result";
            lblSearchResult.Text = WebCommon.getGlobalResourceValue("SearchResult"); // "Search Result";
        }
        if (!IsPostBack)
        {
            btnReturntoSearchCriteria.Visible = false;
            mvDiscrepancyList.ActiveViewIndex = 0;
            BindVendor();
            BindDiscrepancyType();
            BindSiteList();         

            //coming from the add image page
            //if (GetQueryStringValue("FromPage") != null && GetQueryStringValue("FromPage").ToString().Trim() == "images"
            //    || Request.QueryString["FromPage"] != null && Request.QueryString["FromPage"].ToString().Trim() == "images"
            //    || GetQueryStringValue("PreviousPage") != null && GetQueryStringValue("PreviousPage").ToString() == "usercontrol")

            if (GetQueryStringValue("message") != null)
            {
                var DiscrepancyRaiseMsg = GetQueryStringValue("DN");

                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('" + DiscrepancyRaiseMsg + "')", true);
            }
            if (GetQueryStringValue("PreviousPage") != null)
            {
                if (GetQueryStringValue("PreviousPage").Trim().ToUpper().Equals("SearchResultVendor".ToUpper()))
                {
                    lblSearchResult.Text = "Today's Discrepancy Overview";
                    mvDiscrepancyList.ActiveViewIndex = 1;
                    btnSearch.Visible = false;

                    if (Session["SearchCriteriaVendor"] != null)
                        BindGrid((DiscrepancyBE)Session["SearchCriteriaVendor"]);
                    else
                        BindGrid();
                }
            }
        }
    }

    protected void gvDisLog_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType != DataControlRowType.Header
            && e.Row.RowType != DataControlRowType.Footer)
        {
            DateTime dDiscrepancyLogDate = Convert.ToDateTime(DataBinder.Eval(e.Row.DataItem, "DiscrepancyLogDate"));
            e.Row.Cells[4].Text = dDiscrepancyLogDate.ToString("dd/MM/yyyy");
            e.Row.Cells[4].HorizontalAlign = HorizontalAlign.Left;
            HyperLink hypLinkToDisDetails = (HyperLink)e.Row.FindControl("hypLinkToDisDetails");
            HiddenField hdnDisType = (HiddenField)e.Row.FindControl("hdnDisType");
            HiddenField hdnDisLogID = (HiddenField)e.Row.FindControl("hdnDisLogID");
            HiddenField hdnUserID = (HiddenField)e.Row.FindControl("hdnUserID");
            HiddenField hdnWorkFlowID = (HiddenField)e.Row.FindControl("hdnWorkFlowID");
            string status = e.Row.Cells[1].Text.ToString();
            string VDRNo = hypLinkToDisDetails.Text;
            switch (Convert.ToInt32(hdnDisType.Value.Trim()))
            {
                case 1:
                    hypLinkToDisDetails.NavigateUrl = EncryptQuery("LogDiscrepancy/DISLog_Overs.aspx?status=" + status + "&disLogID=" + hdnDisLogID.Value
                        + "&VDRNo=" + VDRNo + "&UserID=" + hdnUserID.Value //+ "&RoleTypeFlag=" + ddlActionPendingWithDropDown.SelectedItem.Value.Trim() 
                        + "&PreviousPage=SearchResultVendor&WorkFlowID=" + hdnWorkFlowID.Value);
                    break;
                case 2:
                    hypLinkToDisDetails.NavigateUrl = EncryptQuery("LogDiscrepancy/DISLog_Shortage.aspx?status=" + status + "&disLogID=" + hdnDisLogID.Value
                        + "&VDRNo=" + VDRNo + "&UserID=" + hdnUserID.Value //+ "&RoleTypeFlag=" + ddlActionPendingWithDropDown.SelectedItem.Value.Trim() 
                        + "&PreviousPage=SearchResultVendor&WorkFlowID=" + hdnWorkFlowID.Value);
                    break;
                case 3:
                    hypLinkToDisDetails.NavigateUrl = EncryptQuery("LogDiscrepancy/DISLog_GoodsReceivedDamaged.aspx?status=" + status + "&disLogID=" + hdnDisLogID.Value
                        + "&VDRNo=" + VDRNo + "&UserID=" + hdnUserID.Value //+ "&RoleTypeFlag=" + ddlActionPendingWithDropDown.SelectedItem.Value.Trim() 
                        + "&PreviousPage=SearchResultVendor&WorkFlowID=" + hdnWorkFlowID.Value);
                    break;
                case 4:
                    hypLinkToDisDetails.NavigateUrl = EncryptQuery("LogDiscrepancy/DISLog_NoPurchaseOrder.aspx?status=" + status + "&disLogID=" + hdnDisLogID.Value
                        + "&VDRNo=" + VDRNo + "&UserID=" + hdnUserID.Value //+ "&RoleTypeFlag=" + ddlActionPendingWithDropDown.SelectedItem.Value.Trim() 
                        + "&PreviousPage=SearchResultVendor&WorkFlowID=" + hdnWorkFlowID.Value);
                    break;
                case 5:
                    hypLinkToDisDetails.NavigateUrl = EncryptQuery("LogDiscrepancy/DISLog_NoPaperwork.aspx?status=" + status + "&disLogID=" + hdnDisLogID.Value
                        + "&VDRNo=" + VDRNo + "&UserID=" + hdnUserID.Value //+ "&RoleTypeFlag=" + ddlActionPendingWithDropDown.SelectedItem.Value.Trim() 
                        + "&PreviousPage=SearchResultVendor&WorkFlowID=" + hdnWorkFlowID.Value);
                    break;
                case 6:
                    hypLinkToDisDetails.NavigateUrl = EncryptQuery("LogDiscrepancy/DISLog_IncorrectProduct.aspx?status=" + status + "&disLogID=" + hdnDisLogID.Value
                        + "&VDRNo=" + VDRNo + "&UserID=" + hdnUserID.Value //+ "&RoleTypeFlag=" + ddlActionPendingWithDropDown.SelectedItem.Value.Trim() 
                        + "&PreviousPage=SearchResultVendor&WorkFlowID=" + hdnWorkFlowID.Value);
                    break;
                case 7:
                    hypLinkToDisDetails.NavigateUrl = EncryptQuery("LogDiscrepancy/DISLog_PresentationIssue.aspx?status=" + status + "&disLogID=" + hdnDisLogID.Value
                        + "&VDRNo=" + VDRNo + "&UserID=" + hdnUserID.Value //+ "&RoleTypeFlag=" + ddlActionPendingWithDropDown.SelectedItem.Value.Trim() 
                        + "&PreviousPage=SearchResultVendor&WorkFlowID=" + hdnWorkFlowID.Value);
                    break;
                case 8:
                    hypLinkToDisDetails.NavigateUrl = EncryptQuery("LogDiscrepancy/DISLog_IncorrectAddress.aspx?status=" + status + "&disLogID=" + hdnDisLogID.Value
                        + "&VDRNo=" + VDRNo + "&UserID=" + hdnUserID.Value //+ "&RoleTypeFlag=" + ddlActionPendingWithDropDown.SelectedItem.Value.Trim() 
                        + "&PreviousPage=SearchResultVendor&WorkFlowID=" + hdnWorkFlowID.Value);
                    break;
                case 9:
                    hypLinkToDisDetails.NavigateUrl = EncryptQuery("LogDiscrepancy/DISLog_PaperworkAmended.aspx?status=" + status + "&disLogID=" + hdnDisLogID.Value
                        + "&VDRNo=" + VDRNo + "&UserID=" + hdnUserID.Value //+ "&RoleTypeFlag=" + ddlActionPendingWithDropDown.SelectedItem.Value.Trim() 
                        + "&PreviousPage=SearchResultVendor&WorkFlowID=" + hdnWorkFlowID.Value);
                    break;
                case 10:
                    hypLinkToDisDetails.NavigateUrl = EncryptQuery("LogDiscrepancy/DISLog_WrongPackSize.aspx?status=" + status + "&disLogID=" + hdnDisLogID.Value
                        + "&VDRNo=" + VDRNo + "&UserID=" + hdnUserID.Value //+ "&RoleTypeFlag=" + ddlActionPendingWithDropDown.SelectedItem.Value.Trim() 
                        + "&PreviousPage=SearchResultVendor&WorkFlowID=" + hdnWorkFlowID.Value);
                    break;
                case 11:
                    hypLinkToDisDetails.NavigateUrl = EncryptQuery("LogDiscrepancy/DISLog_FailPalletSpecification.aspx?status=" + status + "&disLogID=" + hdnDisLogID.Value
                        + "&VDRNo=" + VDRNo + "&UserID=" + hdnUserID.Value //+ "&RoleTypeFlag=" + ddlActionPendingWithDropDown.SelectedItem.Value.Trim() 
                        + "&PreviousPage=SearchResultVendor&WorkFlowID=" + hdnWorkFlowID.Value);
                    break;
                case 12:
                    hypLinkToDisDetails.NavigateUrl = EncryptQuery("LogDiscrepancy/DISLog_QualityIssue.aspx?status=" + status + "&disLogID=" + hdnDisLogID.Value
                        + "&VDRNo=" + VDRNo + "&UserID=" + hdnUserID.Value //+ "&RoleTypeFlag=" + ddlActionPendingWithDropDown.SelectedItem.Value.Trim() 
                        + "&PreviousPage=SearchResultVendor&WorkFlowID=" + hdnWorkFlowID.Value);
                    break;
                case 13:
                    hypLinkToDisDetails.NavigateUrl = EncryptQuery("LogDiscrepancy/DISLog_PrematureInvoiceReceipt.aspx?status=" + status + "&disLogID=" + hdnDisLogID.Value
                        + "&VDRNo=" + VDRNo + "&UserID=" + hdnUserID.Value //+ "&RoleTypeFlag=" + ddlActionPendingWithDropDown.SelectedItem.Value.Trim() 
                        + "&PreviousPage=SearchResultVendor&WorkFlowID=" + hdnWorkFlowID.Value);
                    break;
                case 14:
                    hypLinkToDisDetails.NavigateUrl = EncryptQuery("LogDiscrepancy/DISLog_GenericDescrepancy.aspx?status=" + status + "&disLogID=" + hdnDisLogID.Value
                        + "&VDRNo=" + VDRNo + "&UserID=" + hdnUserID.Value //+ "&RoleTypeFlag=" + ddlActionPendingWithDropDown.SelectedItem.Value.Trim() 
                        + "&PreviousPage=SearchResultVendor&WorkFlowID=" + hdnWorkFlowID.Value);
                    break;
                case 15:
                    hypLinkToDisDetails.NavigateUrl = EncryptQuery("LogDiscrepancy/DISLog_Shuttle.aspx?status=" + status + "&disLogID=" + hdnDisLogID.Value
                        + "&VDRNo=" + VDRNo + "&UserID=" + hdnUserID.Value //+ "&RoleTypeFlag=" + ddlActionPendingWithDropDown.SelectedItem.Value.Trim() 
                        + "&PreviousPage=SearchResultVendor&WorkFlowID=" + hdnWorkFlowID.Value);
                    break;
                case 16:
                    hypLinkToDisDetails.NavigateUrl = EncryptQuery("LogDiscrepancy/DISLog_Reservation.aspx?status=" + status + "&disLogID=" + hdnDisLogID.Value
                        + "&VDRNo=" + VDRNo + "&UserID=" + hdnUserID.Value //+ "&RoleTypeFlag=" + ddlActionPendingWithDropDown.SelectedItem.Value.Trim() 
                        + "&PreviousPage=SearchResultVendor&WorkFlowID=" + hdnWorkFlowID.Value);
                    break;
                case 17:
                    hypLinkToDisDetails.NavigateUrl = EncryptQuery("LogDiscrepancy/DISLog_InvoiceDiscrepancy.aspx?status=" + status + "&disLogID=" + hdnDisLogID.Value
                        + "&VDRNo=" + VDRNo + "&UserID=" + hdnUserID.Value //+ "&RoleTypeFlag=" + ddlActionPendingWithDropDown.SelectedItem.Value.Trim() 
                        + "&PreviousPage=SearchResultVendor&WorkFlowID=" + hdnWorkFlowID.Value);
                    break;

                case 18:
                    hypLinkToDisDetails.NavigateUrl = EncryptQuery("LogDiscrepancy/DISLog_StockPlannerInvoiceDiscrepancy.aspx?status=" + status + "&disLogID=" + hdnDisLogID.Value
                        + "&VDRNo=" + VDRNo + "&UserID=" + hdnUserID.Value //+ "&RoleTypeFlag=" + ddlActionPendingWithDropDown.SelectedItem.Value.Trim() 
                        + "&PreviousPage=SearchResultVendor&WorkFlowID=" + hdnWorkFlowID.Value);
                    break;
                case 19:
                    hypLinkToDisDetails.NavigateUrl = EncryptQuery("LogDiscrepancy/DISLog_InvoiceQueryDiscrepancy.aspx?status=" + status + "&disLogID=" + hdnDisLogID.Value
                        + "&VDRNo=" + VDRNo + "&UserID=" + hdnUserID.Value //+ "&RoleTypeFlag=" + ddlActionPendingWithDropDown.SelectedItem.Value.Trim() 
                        + "&PreviousPage=SearchResultVendor&WorkFlowID=" + hdnWorkFlowID.Value);
                    break;
            }            
            e.Row.Cells[3].HorizontalAlign = HorizontalAlign.Left;
        }
    }

    protected void btnSearch_Click(object sender, EventArgs e) 
    {
        mvDiscrepancyList.ActiveViewIndex = 1;
        Label lblModuleText = (Label)Page.Master.FindControl("lblModuleText");
        lblModuleText.Text = "Discrepancies - Search Result";
        lblSearchResult.Text = "Search Result";
        btnSearch.Visible = false;
        btnReturntoSearchCriteria.Visible = true;

        //hiddenSelectedIDs.Value = string.Empty;
        //for (int index = 0; index < UclstSiteSelected.Items.Count; index++)
        //{
        //    if (UclstSiteSelected.Items.Count == 1)
        //        hiddenSelectedIDs.Value = UclstSiteSelected.Items[index].Value;
        //    else
        //        hiddenSelectedIDs.Value = string.Format("{0},{1}", hiddenSelectedIDs.Value, UclstSiteSelected.Items[index].Value);
        //}
        BindGrid();
    }

    protected void btnReturntoSearchCriteria_Click(object sender, EventArgs e) 
    {
        mvDiscrepancyList.ActiveViewIndex = 0;
        btnSearch.Visible = true;
        btnReturntoSearchCriteria.Visible = false;
        Label lblModuleText = (Label)Page.Master.FindControl("lblModuleText");
        lblModuleText.Text = "Discrepancies - Search Criteria";
        lblSearchResult.Text = "Search Criteria";
        this.SetSearchCriteria();        
        //hiddenSelectedIDs.Value = string.Empty;
        //hiddenSelectedName.Value = string.Empty;
        Session["SearchCriteriaVendor"] = null;
    }

    protected virtual void btnMoveRight_Click(object sender, EventArgs e)
    {
        if (UclstSiteList.SelectedItem != null)
        {
            FillControls.MoveOneItem(UclstSiteList, UclstSiteSelected);
        }
    }

    protected virtual void btnMoveLeft_Click(object sender, EventArgs e)
    {
        if (UclstSiteSelected.SelectedItem != null)
        {
            FillControls.MoveOneItem(UclstSiteSelected, UclstSiteList);
        }
    }

    #endregion

    #region Page Level Methods ...

    public override BusinessEntities.BaseBe[] SubSort(GridViewSortEventArgs e)
    {
        return Utilities.GenericListHelper<DiscrepancyBE>.SortList((List<DiscrepancyBE>)ViewState["lstSites"], e.SortExpression, e.SortDirection).ToArray();
    }

    private void BindDiscrepancyType()
    {
        DiscrepancyBE oNewDiscrepancyBE = new DiscrepancyBE();
        DiscrepancyBAL oNewDiscrepancyBAL = new DiscrepancyBAL();
        oNewDiscrepancyBE.Action = "GetDiscrepancyType";
        List<DiscrepancyBE> lstDiscrepancyType = oNewDiscrepancyBAL.GetDiscrepancyTypeBAL(oNewDiscrepancyBE);
        if (lstDiscrepancyType != null)
        {
            FillControls.FillDropDown(ref ddlDiscrepancyType, lstDiscrepancyType, "DiscrepancyType", "DiscrepancyTypeID", "---Select---");
        }
        oNewDiscrepancyBAL = null;
    }

    private void BindSiteList()
    {
        if (GetQueryStringValue("SiteId") == null)
        {
            MAS_SiteBE oMAS_SiteBE = new MAS_SiteBE();
            MAS_SiteBAL oMAS_SiteBAL = new MAS_SiteBAL();
            oMAS_SiteBE.Action = "ShowAll";

            oMAS_SiteBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
            oMAS_SiteBE.User.UserID = Convert.ToInt32(Session["UserID"]);
            oMAS_SiteBE.SiteCountryID = 0;
            List<MAS_SiteBE> lstSite = new List<MAS_SiteBE>();
            lstSite = oMAS_SiteBAL.GetSiteMisSettingBAL(oMAS_SiteBE);
            if (lstSite.Count > 0 && lstSite != null)
            {
                FillControls.FillListBox(ref UclstSiteList, lstSite, "SiteDescription", "SiteID");
            }
            oMAS_SiteBAL = null;
        }
    }

    private void BindVendor()
    {
        UP_VendorBAL oUP_VendorBAL = new UP_VendorBAL();
        UP_VendorBE oUP_VendorBE = new UP_VendorBE();

        oUP_VendorBE.Action = "GetVendorDetailsByUserID";
        oUP_VendorBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
        oUP_VendorBE.User.UserID = Convert.ToInt32(Session["UserID"]);
        oUP_VendorBE.Site = new BusinessEntities.ModuleBE.AdminFunctions.MAS_SiteBE();
        oUP_VendorBE.Site.SiteID = 0;

        List<UP_VendorBE> lstUPVendor = new List<UP_VendorBE>();
        lstUPVendor = oUP_VendorBAL.GetVendorByUserIdBAL(oUP_VendorBE);
        
        //To Fix Issue raised on 13 June 2013 - When you look at the OTIF and Scorecard, the consolidated ref is showing - only the associated 'child' vendors should be in the drop down list.
        if (lstUPVendor != null && lstUPVendor.Count > 0)
            lstUPVendor = lstUPVendor.FindAll(delegate(UP_VendorBE v) { return !Convert.ToString(v.VendorFlag).Equals("P"); });
        //lstUPVendor = lstUPVendor.FindAll(delegate(UP_VendorBE v) { return v.ParentVendorID != -1 && !Convert.ToString(v.VendorFlag).Equals("P"); });
        //------------//

        FillControls.FillDropDown(ref ddlVendor, lstUPVendor, "VendorCountryName", "VendorID", "--Select--");
    }

    private void BindGrid(DiscrepancyBE discrepancy = null)
    {
        oNewDiscrepancyBAL = new DiscrepancyBAL();
        List<DiscrepancyBE> lstDisLog = null;
        if (discrepancy == null)
        {
            oNewDiscrepancyBE = new DiscrepancyBE();
            oNewDiscrepancyBE.Action = "GetDiscrepancyLogForVendor";

            //Search Creteria
            oNewDiscrepancyBE.SCDiscrepancyTypeID = ddlDiscrepancyType.SelectedIndex != 0 ? Convert.ToInt32(ddlDiscrepancyType.SelectedItem.Value) : (int?)null;
            oNewDiscrepancyBE.SCDiscrepancyNo = txtDiscrepancyNo.Text != string.Empty ? txtDiscrepancyNo.Text.Trim() : null;
            oNewDiscrepancyBE.SCPurchaseOrderNo = txtPurchaseOrder.Text != string.Empty ? txtPurchaseOrder.Text.Trim() : null;
            //oNewDiscrepancyBE.SCPurchaseOrderDate = string.IsNullOrEmpty(txtPurchaseOrderDate.Text) ? (DateTime?)null : Common.GetMM_DD_YYYY(txtPurchaseOrderDate.Text);
            //oNewDiscrepancyBE.SCDeliveryNoteNumber = txtDeliveryNote.Text != string.Empty ? txtDeliveryNote.Text.Trim() : null;
            oNewDiscrepancyBE.SCDiscrepancyDateFrom = string.IsNullOrEmpty(txtDiscrepancyDateRangeFrom.Text) ? (DateTime?)null : Common.GetMM_DD_YYYY(txtDiscrepancyDateRangeFrom.Text);
            oNewDiscrepancyBE.SCDiscrepancyDateTo = string.IsNullOrEmpty(txtDiscrepancyDateRangeTo.Text) ? (DateTime?)null : Common.GetMM_DD_YYYY(txtDiscrepancyDateRangeTo.Text);
            oNewDiscrepancyBE.UserID = Session["UserID"] != null ? Convert.ToInt32(Session["UserID"]) : (int?)null;
            oNewDiscrepancyBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
            //oNewDiscrepancyBE.User.RoleTypeFlag = ddlActionPendingWithDropDown.SelectedItem.Value.Trim(); //+"&FromPage=SearchResultl";

            //Site
            if (!string.IsNullOrEmpty(hiddenSelectedIDs.Value))
                oNewDiscrepancyBE.SCSelectedSiteIDs = hiddenSelectedIDs.Value.Trim(',');
            else
                oNewDiscrepancyBE.SCSelectedSiteIDs = null;

            //oNewDiscrepancyBE.SCSelectedVendorIDs = msVendor.SelectedVendorIDs;
            if (ddlVendor.Items.Count > 0)
                if (ddlVendor.SelectedIndex > 0)
                    oNewDiscrepancyBE.VendorID = Convert.ToInt32(ddlVendor.SelectedValue);

            //oNewDiscrepancyBE.SCSPUserID = ddlStockPlanner.SelectedIndex != 0 ? Convert.ToInt32(ddlStockPlanner.SelectedItem.Value) : (int?)null;
            oNewDiscrepancyBE.DiscrepancyStatus = string.IsNullOrEmpty(ddlStatus.SelectedValue) ? null : ddlStatus.SelectedValue;
            lstDisLog = oNewDiscrepancyBAL.GetDiscrepancyLogDetailsBAL(oNewDiscrepancyBE);

            gvDisLog.DataSource = lstDisLog;
            gvDisLog.DataBind();
            ViewState["lstSites"] = lstDisLog;
            Session["SearchCriteriaVendor"] = oNewDiscrepancyBE;
            hiddenSelectedIDs.Value = string.Empty;
        }
        else
        {
            mvDiscrepancyList.ActiveViewIndex = 1;
            Label lblModuleText = (Label)Page.Master.FindControl("lblModuleText");
            lblModuleText.Text = "Discrepancies - Search Result";
            lblSearchResult.Text = "Search Result";
            btnSearch.Visible = false;
            btnReturntoSearchCriteria.Visible = true;

            if (discrepancy.SCDiscrepancyTypeID != null)
                if (discrepancy.SCDiscrepancyTypeID > 0)
                    ddlDiscrepancyType.SelectedIndex = ddlDiscrepancyType.Items.IndexOf(ddlDiscrepancyType.Items.FindByValue(Convert.ToString(discrepancy.SCDiscrepancyTypeID)));

            if (!string.IsNullOrEmpty(discrepancy.SCDiscrepancyNo))
                txtDiscrepancyNo.Text = discrepancy.SCDiscrepancyNo;

            if (!string.IsNullOrEmpty(discrepancy.SCPurchaseOrderNo))
                txtPurchaseOrder.Text = discrepancy.SCPurchaseOrderNo;

            //if (discrepancy.SCPurchaseOrderDate != null)
            //    txtPurchaseOrderDate.Text = Convert.ToString(discrepancy.SCPurchaseOrderDate);

            //if (!string.IsNullOrEmpty(discrepancy.SCDeliveryNoteNumber))
            //    txtDeliveryNote.Text = discrepancy.SCDeliveryNoteNumber;

            if (discrepancy.SCDiscrepancyDateFrom != null)
                txtDiscrepancyDateRangeFrom.Text = Convert.ToString(discrepancy.SCDiscrepancyDateFrom);

            if (discrepancy.SCDiscrepancyDateTo != null)
                txtDiscrepancyDateRangeTo.Text = Convert.ToString(discrepancy.SCDiscrepancyDateTo);

            //if (!string.IsNullOrEmpty(discrepancy.User.RoleTypeFlag))
            //    ddlActionPendingWithDropDown.SelectedIndex =
            //        ddlActionPendingWithDropDown.Items.IndexOf(ddlActionPendingWithDropDown.Items.FindByValue(Convert.ToString(discrepancy.User.RoleTypeFlag)));

            hiddenSelectedIDs.Value = string.Empty;
            if (!string.IsNullOrEmpty(discrepancy.SCSelectedSiteIDs))
                hiddenSelectedIDs.Value = discrepancy.SCSelectedSiteIDs;

            //if (!string.IsNullOrEmpty(discrepancy.SCSelectedVendorIDs))
            //    msVendor.SelectedVendorIDs = discrepancy.SCSelectedVendorIDs;

            if (ddlVendor.Items.Count > 0)
                ddlVendor.SelectedIndex = ddlVendor.Items.IndexOf(ddlVendor.Items.FindByValue(Convert.ToString(discrepancy.VendorID)));

            //if (discrepancy.SCSPUserID != null)
            //    if (discrepancy.SCSPUserID > 0)
            //        ddlStockPlanner.SelectedIndex = ddlStockPlanner.Items.IndexOf(ddlStockPlanner.Items.FindByValue(Convert.ToString(discrepancy.SCSPUserID)));

            if (!string.IsNullOrEmpty(discrepancy.DiscrepancyStatus))
                ddlStatus.SelectedIndex = ddlStatus.Items.IndexOf(ddlStatus.Items.FindByValue(Convert.ToString(discrepancy.DiscrepancyStatus)));

            lstDisLog = oNewDiscrepancyBAL.GetDiscrepancyLogDetailsBAL(discrepancy);
            Session["SearchCriteriaVendor"] = discrepancy;
        }

        if (lstDisLog != null)
        {
            gvDisLog.DataSource = lstDisLog;
            gvDisLog.DataBind();
            ViewState["lstSites"] = lstDisLog;
        }

        if (gvDisLog.Rows.Count > 0)
        {
            ucExportToExcel1.Visible = true;
        }
        else
        {
            ucExportToExcel1.Visible = false;
        }

        oNewDiscrepancyBAL = null;
    }

    private void SetSearchCriteria()
    {
        if (Session["SearchCriteriaVendor"] != null)
        {
            oNewDiscrepancyBE = new DiscrepancyBE();
            oNewDiscrepancyBE = (DiscrepancyBE)Session["SearchCriteriaVendor"];

            if (oNewDiscrepancyBE.SCDiscrepancyTypeID != null)
                if (oNewDiscrepancyBE.SCDiscrepancyTypeID > 0)
                    ddlDiscrepancyType.SelectedIndex = ddlDiscrepancyType.Items.IndexOf(ddlDiscrepancyType.Items.FindByValue(
                        Convert.ToString(oNewDiscrepancyBE.SCDiscrepancyTypeID)));

            if (!string.IsNullOrEmpty(oNewDiscrepancyBE.SCDiscrepancyNo))
                txtDiscrepancyNo.Text = oNewDiscrepancyBE.SCDiscrepancyNo;

            if (!string.IsNullOrEmpty(oNewDiscrepancyBE.SCPurchaseOrderNo))
                txtPurchaseOrder.Text = oNewDiscrepancyBE.SCPurchaseOrderNo;

            if (oNewDiscrepancyBE.SCDiscrepancyDateFrom != null)
                txtDiscrepancyDateRangeFrom.Text = Convert.ToString(oNewDiscrepancyBE.SCDiscrepancyDateFrom);

            if (oNewDiscrepancyBE.SCDiscrepancyDateTo != null)
                txtDiscrepancyDateRangeTo.Text = Convert.ToString(oNewDiscrepancyBE.SCDiscrepancyDateTo);

            this.BindSiteList();
            if (!string.IsNullOrEmpty(oNewDiscrepancyBE.SCSelectedSiteIDs))
            {
                hiddenSelectedIDs.Value = string.Empty;
                hiddenSelectedIDs.Value = oNewDiscrepancyBE.SCSelectedSiteIDs;
                string[] SiteIds = hiddenSelectedIDs.Value.Split(',');
                UclstSiteSelected.Items.Clear();
                for (int index = 0; index < SiteIds.Length; index++)
                {
                    int selectedIndex = UclstSiteList.Items.IndexOf(UclstSiteList.Items.FindByValue(SiteIds[index].ToString()));
                    ListItem listItem = UclstSiteList.Items.FindByValue(SiteIds[index].ToString());
                    if (listItem != null)
                    {
                        UclstSiteSelected.Items.Add(listItem);
                        UclstSiteList.Items.RemoveAt(selectedIndex);
                    }
                }
            }

            if (ddlVendor.Items.Count > 0)
                ddlVendor.SelectedIndex = ddlVendor.Items.IndexOf(ddlVendor.Items.FindByValue(Convert.ToString(oNewDiscrepancyBE.VendorID)));

            if (!string.IsNullOrEmpty(oNewDiscrepancyBE.DiscrepancyStatus))
                ddlStatus.SelectedIndex = ddlStatus.Items.IndexOf(ddlStatus.Items.FindByValue(Convert.ToString(oNewDiscrepancyBE.DiscrepancyStatus)));
        }
    }

    #endregion
}
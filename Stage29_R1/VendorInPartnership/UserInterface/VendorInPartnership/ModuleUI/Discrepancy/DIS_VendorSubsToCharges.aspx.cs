﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLogicLayer.ModuleBAL.Security;
using BusinessEntities.ModuleBE.Security;
using BusinessEntities.ModuleBE.Discrepancy.LogDiscrepancy;
using BusinessLogicLayer.ModuleBAL.Discrepancy.LogDiscrepancy;
using System.IO;

public partial class VendorSubsToCharges : CommonPage {
    protected void Page_Init(object sender, EventArgs e) {
        ucVendorTemplateSelect.CurrentPage = this;

    }


    protected void Page_Load(object sender, EventArgs e) {

        msVendor.VendorFillOnSearch = true;
        msVendor.setVendorsOnPostBack();
        msCountry.SetCountryOnPostBack();
      
    }

    /// <summary>
    /// Method to show Vendor on the basis of selected Template
    /// </summary>
    public override void TemplateSelectedIndexChanged() {
        base.TemplateSelectedIndexChanged();

        int VendorTemplateId = Convert.ToInt32(ucVendorTemplateSelect.innerControlddlTemplate.SelectedValue);

        ListBox lstSelectedVendor = (ListBox)msVendor.FindControl("lstRight");
        ListBox lstVendor = (ListBox)msVendor.FindControl("ucVendor").FindControl("lstLeft");

        if (VendorTemplateId > 0) {

            SCT_TemplateBAL oSCT_TemplateBAL = new SCT_TemplateBAL();
            SCT_TemplateBE oSCT_TemplateBE = new SCT_TemplateBE();

            oSCT_TemplateBE.VendorTemplateID = VendorTemplateId;
            oSCT_TemplateBE.Action = "GetVendorTemplateInfobyID";
            List<SCT_TemplateBE> lstVendorTemplate = oSCT_TemplateBAL.GetVendorTemplateById(oSCT_TemplateBE);

            if (lstVendorTemplate != null && lstVendorTemplate.Count > 0) {
                lstSelectedVendor.Items.Clear();
                msVendor.innerControlHiddenField.Value = string.Empty;
                msVendor.innerControlHiddenField2.Value = string.Empty;
                for (int iCount = 0; iCount < lstVendorTemplate.Count; iCount++) {
                    lstSelectedVendor.Items.Add(new ListItem(lstVendorTemplate[iCount].Vendor.Vendor_Name, lstVendorTemplate[iCount].Vendor.VendorID.ToString()));
                    msVendor.SelectedVendorIDs = lstVendorTemplate[iCount].Vendor.VendorID.ToString();
                    msVendor.SelectedVendorName = lstVendorTemplate[iCount].Vendor.Vendor_Name.ToString();
                }
            }
        }
        else {
            lstSelectedVendor.Items.Clear();
            msVendor.innerControlHiddenField.Value = string.Empty;
            msVendor.innerControlHiddenField2.Value = string.Empty;
        }
    }

    protected void btnGenerateReport_Click(object sender, EventArgs e) {   
        BindGrid();       
    }

    private void BindGrid() {
        List<VendorSubsToChargesBE> VendorSubsToChargesBEList = new List<VendorSubsToChargesBE>();

        VendorSubsToChargesBE oNewVendorSubsToChargesBE = new VendorSubsToChargesBE();
        VendorSubsToChargesBAL oVendorSubsToChargesBAL = new VendorSubsToChargesBAL();

        oNewVendorSubsToChargesBE.Action = "ShowAll";
        oNewVendorSubsToChargesBE.CountryIDs = msCountry.SelectedCountryIDs;
        oNewVendorSubsToChargesBE.VendorIDs = msVendor.SelectedVendorIDs;
        oNewVendorSubsToChargesBE.ChargeType = ddlChargeType.SelectedItem.Value.Replace(" ", "");
        if (ddlSubscribed.SelectedIndex > 0)
            oNewVendorSubsToChargesBE.Subscribed = ddlSubscribed.SelectedItem.Value;

        VendorSubsToChargesBEList = oVendorSubsToChargesBAL.GetvendorSubscribeBAL(oNewVendorSubsToChargesBE);

        if (VendorSubsToChargesBEList != null && VendorSubsToChargesBEList.Count > 0) {

            UcInputPanel.Visible = false;
            UcResultPanel.Visible = true;
            grdSubs.PageIndex = 0;
            grdSubs.DataSource = VendorSubsToChargesBEList;
            grdSubs.DataBind();

            ViewState["VendorSubsToChargesBEList"] = VendorSubsToChargesBEList;
            RegisterPostBackControl();
        }
        else {
            ViewState["VendorSubsToChargesBEList"] = null;
            ScriptManager.RegisterStartupScript(this.ddlChargeType, this.ddlChargeType.GetType(), "alert", "alert('No Vendor Subscription To Charges.')", true);                       
            return;
        }
    }

    protected void btnBack_Click(object sender, EventArgs e) {
        UcInputPanel.Visible = true;
        UcResultPanel.Visible = false;
    }

    protected void grdSubs_RowCommand(object sender, GridViewCommandEventArgs e) {
        if (e.CommandName == "SAVE") {
            GridViewRow row = (GridViewRow)((Button)e.CommandSource).NamingContainer;

            CheckBox Overs = (CheckBox)row.FindControl("Overs");
            CheckBox Damage = (CheckBox)row.FindControl("Damage");
            CheckBox NoPO = (CheckBox)row.FindControl("NoPO");
            CheckBox NoPaperwork = (CheckBox)row.FindControl("NoPaperwork");
            CheckBox IncorrectProduct = (CheckBox)row.FindControl("IncorrectProduct");
            CheckBox IncorrectAddress = (CheckBox)row.FindControl("IncorrectAddress");
            CheckBox FailedPalletSpec = (CheckBox)row.FindControl("FailedPalletSpec");
            CheckBox IncorrectPackCarriage = (CheckBox)row.FindControl("IncorrectPackCarriage");
            CheckBox IncorrectPackLabour = (CheckBox)row.FindControl("IncorrectPackLabour");
            CheckBox PrematureInvoice = (CheckBox)row.FindControl("PrematureInvoice");
            CheckBox QualityIssueCarriage = (CheckBox)row.FindControl("QualityIssueCarriage");
            CheckBox QualityIssueLabour = (CheckBox)row.FindControl("QualityIssueLabour");


            VendorSubsToChargesBE oNewVendorSubsToChargesBE = new VendorSubsToChargesBE();
            VendorSubsToChargesBAL oVendorSubsToChargesBAL = new VendorSubsToChargesBAL();

            oNewVendorSubsToChargesBE.Action = "Update";
            oNewVendorSubsToChargesBE.VendorID = Convert.ToInt32(e.CommandArgument);            
            oNewVendorSubsToChargesBE.Overs = Overs.Checked;
            oNewVendorSubsToChargesBE.Damage = Damage.Checked;
            oNewVendorSubsToChargesBE.NoPO = NoPO.Checked;
            oNewVendorSubsToChargesBE.NoPaperwork = NoPaperwork.Checked;
            oNewVendorSubsToChargesBE.IncorrectProduct = IncorrectProduct.Checked;
            oNewVendorSubsToChargesBE.IncorrectAddress = IncorrectAddress.Checked;
            oNewVendorSubsToChargesBE.FailedPalletSpec = FailedPalletSpec.Checked;
            oNewVendorSubsToChargesBE.IncorrectPackCarriage = IncorrectPackCarriage.Checked;
            oNewVendorSubsToChargesBE.IncorrectPackLabour = IncorrectPackLabour.Checked;
            oNewVendorSubsToChargesBE.PrematureInvoice = PrematureInvoice.Checked;
            oNewVendorSubsToChargesBE.QualityIssueCarriage = QualityIssueCarriage.Checked;
            oNewVendorSubsToChargesBE.QualityIssueLabour = QualityIssueLabour.Checked;
            oNewVendorSubsToChargesBE.UpdateBy = Convert.ToInt32(Session["UserID"]);
            oVendorSubsToChargesBAL.UpdatevendorSubscribeBAL(oNewVendorSubsToChargesBE);

            Image imgUpdate = (Image)row.FindControl("imgUpdate");
            imgUpdate.Visible = true;

            RegisterPostBackControl();
        }
    }

    private void RegisterPostBackControl() {
        foreach (GridViewRow row in grdSubs.Rows) {
            Button btnUpdate = row.FindControl("btnUpdate") as Button;
            btnUpdate.CssClass = "button";
            ScriptManager.GetCurrent(this).RegisterAsyncPostBackControl(btnUpdate);
        }
    }
       
    protected void grdSubs_PageIndexChanging(object sender, GridViewPageEventArgs e) {
        grdSubs.PageIndex = e.NewPageIndex;
        List<VendorSubsToChargesBE> VendorSubsToChargesBEList = (List<VendorSubsToChargesBE>)ViewState["VendorSubsToChargesBEList"];
        grdSubs.DataSource = VendorSubsToChargesBEList;
        grdSubs.DataBind();
    }

    protected void btnExport_Click(object sender, EventArgs e) {
        List<VendorSubsToChargesBE> VendorSubsToChargesBEList = (List<VendorSubsToChargesBE>)ViewState["VendorSubsToChargesBEList"];
        grdSubsExport.DataSource = VendorSubsToChargesBEList;
        grdSubsExport.DataBind();
        
        Response.ClearContent();
        Response.Buffer = true;
        Response.AddHeader("content-disposition", "attachment;filename=Vendor Subscription To Charges.xls");
        Response.Charset = "";
        Response.ContentType = "application/excel";
        System.IO.StringWriter sw = new System.IO.StringWriter();
        HtmlTextWriter htm = new HtmlTextWriter(sw);
        tblExport.RenderControl(htm);
        Response.Write(sw.ToString());
        Response.End();       

    }


    protected void btnExport_1_Click(object sender, EventArgs e) {
        Response.Clear();
        Response.Buffer = true;
        Response.AddHeader("content-disposition", "attachment;filename=GridViewExport.xls");
        Response.Charset = "";
        Response.ContentType = "application/vnd.ms-excel";
        using (StringWriter sw = new StringWriter()) {
            HtmlTextWriter hw = new HtmlTextWriter(sw);

            //To Export all pages
            grdSubs.AllowPaging = false;
            this.BindGrid();

            //GridView1.HeaderRow.BackColor = Color.White;
            //foreach (TableCell cell in GridView1.HeaderRow.Cells) {
            //    cell.BackColor = GridView1.HeaderStyle.BackColor;
            //}
            //foreach (GridViewRow row in GridView1.Rows) {
            //    row.BackColor = Color.White;
            //    foreach (TableCell cell in row.Cells) {
            //        if (row.RowIndex % 2 == 0) {
            //            cell.BackColor = GridView1.AlternatingRowStyle.BackColor;
            //        }
            //        else {
            //            cell.BackColor = GridView1.RowStyle.BackColor;
            //        }
            //        cell.CssClass = "textmode";
            //    }
            //}

            grdSubs.RenderControl(hw);

            //style to format numbers to string
            string style = @"<style> .textmode { } </style>";
            Response.Write(style);
            Response.Output.Write(sw.ToString());
            Response.Flush();
            Response.End();
        }
    }

    //override the VerifyRenderingInServerForm() to verify the control
    public override void VerifyRenderingInServerForm(Control control) {
        //Required to verify that the control is rendered properly on page
        return;
    }
}
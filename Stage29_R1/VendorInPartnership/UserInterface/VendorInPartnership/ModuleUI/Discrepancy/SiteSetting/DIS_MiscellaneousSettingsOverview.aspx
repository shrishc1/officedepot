﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="DIS_MiscellaneousSettingsOverview.aspx.cs"
    MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master" Inherits="ModuleUI_Discrepancy_DIS_MiscellaneousSettingsOverview" %>

<%@ Register Src="../../../CommonUI/UserControls/ucExportToExcel.ascx" TagName="ucExportToExcel"
    TagPrefix="cc2" %>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2>
        <cc1:ucLabel ID="lblMiscellaneousSettings" runat="server" Text="Miscellaneous Settings"></cc1:ucLabel>
    </h2>
    <div class="button-row">
        <cc2:ucExportToExcel ID="btnExportToExcel1" runat="server" />
    </div>
    <table width="100%">
        <tr>
            <td align="center" colspan="9">
                <cc1:ucGridView ID="grdMiscellneousSettings" Width="100%" runat="server" CssClass="grid"
                    OnRowDataBound="grdMiscellneousSettings_RowDataBound" OnSorting="SortGrid" AllowSorting="true">
                    <Columns>
                        <asp:TemplateField HeaderText="Site" SortExpression="SiteDescription">
                            <HeaderStyle Width="15%" HorizontalAlign="Left" />
                            <ItemTemplate>
                                <asp:HyperLink ID="hpLink" runat="server" Text='<%# Eval("SiteDescription") %>' NavigateUrl='<%# EncryptQuery("DIS_MiscellaneousSettingsEdit.aspx?SiteID="+Eval("SiteID")) %>'></asp:HyperLink>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:TemplateField>
                        <asp:BoundField HeaderText="Work Flow Settings" DataField="DIS_CommunicationWithEscalationType"
                            SortExpression="DIS_CommunicationWithEscalationType">
                            <HeaderStyle HorizontalAlign="Left" Width="25%" />
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                        <asp:BoundField HeaderText="InitialEscalationInDays" DataField="DIS_DaysBeforeEscalationType2"
                            SortExpression="DIS_DaysBeforeEscalationType2">
                            <HeaderStyle HorizontalAlign="Center" Width="20%" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField HeaderText="FinalEscalationInDays" DataField="DIS_DaysBeforeFinalEscalation"
                            SortExpression="DIS_DaysBeforeFinalEscalation">
                            <HeaderStyle HorizontalAlign="Center" Width="20%" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField HeaderText="DeliveryToleranceDaysInDays" DataField="DIS_DaysAfterGoodsReturned"
                            SortExpression="DIS_DaysAfterGoodsReturned">
                            <HeaderStyle HorizontalAlign="Center" Width="20%" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                    </Columns>
                </cc1:ucGridView>
            </td>
        </tr>
    </table>
</asp:Content>

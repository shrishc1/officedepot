﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    CodeFile="DIS_MiscellaneousSettingsEdit.aspx.cs" Inherits="DIS_MiscellaneousSettingsEdit" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register TagPrefix="cc2" TagName="ucSite" Src="~/CommonUI/UserControls/ucSite.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script language="javascript" type="text/javascript">
        function chkinput() {
            var Debitflow = document.getElementById('<%=rdoDebitFlow.ClientID %>');
            var Creditflow = document.getElementById('<%=rdoCreditFlow.ClientID %>');

            if (Debitflow.checked || Creditflow.checked) {
                if (document.getElementById('<%=txtInitialEscalation.ClientID %>').value == "") {
                    alert("<%=InitialEscaltionRequired %>");
                    return false;
                }
                if (document.getElementById('<%=txtFinalEscalation.ClientID %>').value == "") {
                    alert("<%=FinalEscalationRequired %>");
                    return false;
                }
                if (document.getElementById('<%=txtDeliveryToleranceDays.ClientID %>').value == "") {
                    alert("<%=DeliveryToleranceDaysRequired %>");
                    return false;
                }
            }
        }
       
    


    </script>
    <h2>
        <cc1:ucLabel ID="lblDisMicellaneousSettings" runat="server" Text="Miscellaneous Settings"></cc1:ucLabel>
    </h2>
    <div class="right-shadow">
        <div class="formbox">
            <table width="35%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                <tr>
                    <td style="width: 25%">
                    </td>
                    <td style="font-weight: bold; width: 15%">
                        <cc1:ucLabel ID="lblSitePrefixName" runat="server" Text="Site"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold; width: 5px">
                        :
                    </td>
                    <td style="width: 35%" colspan="2">
                        <cc1:ucLabel ID="lblDisSiteName" runat="server"></cc1:ucLabel>
                    </td>
                    <td style="width: 20%">
                    </td>
                </tr>
            </table>
            <cc1:ucPanel ID="pnlWorkFlowSettings" runat="server" GroupingText="Work Flow Settings"
                CssClass="fieldset-form">
                <table width="100%" cellspacing="5" cellpadding="0" class="form-table" align="center">
                    <tr>
                        <td style="width: 33%">
                            <cc1:ucRadioButton ID="rdoDebitFlow" runat="server" GroupName="DebitFlow" Text="With Debit Flow And Communication" />
                        </td>
                        <td style="width: 33%">
                            <cc1:ucRadioButton ID="rdoCreditFlow" runat="server" GroupName="DebitFlow" Text="With Credit Flow And Communication" />
                        </td>
                        <td style="width: 33%">
                            <cc1:ucRadioButton ID="rdoLoggingIssue" runat="server" GroupName="DebitFlow" Text="Logging Of Issue- no escalation flows" />
                        </td>
                    </tr>
                </table>
            </cc1:ucPanel>
            <cc1:ucPanel ID="pnlEscalationSettings" runat="server" GroupingText="Escalation Settings"
                CssClass="fieldset-form">
                <table width="100%" cellspacing="5" cellpadding="0" class="form-table">
                    <tr>
                        <td style="width: 10%">
                            <cc1:ucLabel ID="lblInitialEscalation" runat="server" Text="Initial Escalation"></cc1:ucLabel>
                        </td>
                        <td style="width: 1%">
                            :
                        </td>
                        <td style="width: 26%">
                            <cc1:ucNumericTextbox ID="txtInitialEscalation" runat="server" Width="25px" onkeypress="return IsNumberKey(event, this);"
                                MaxLength="2"></cc1:ucNumericTextbox>
                            &nbsp;<cc1:ucLabel ID="lblInDays1" runat="server"></cc1:ucLabel>
                        </td>

                        <td style="width: 10%">
                           <cc1:ucLabel ID="lblFinalEscalation" runat="server" Text="Final Escalation"></cc1:ucLabel>
                        </td>
                        <td style="width: 1%">
                            :
                        </td>
                        <td style="width: 22%">
                            <cc1:ucNumericTextbox ID="txtFinalEscalation" runat="server" Width="25px" onkeypress="return IsNumberKey(event, this);"
                                MaxLength="2"></cc1:ucNumericTextbox>
                            &nbsp;<cc1:ucLabel ID="lblInDays2" runat="server"></cc1:ucLabel>
                        </td>
                        <td style="width: 16%">
                            <cc1:ucLabel ID="lblDeliveryToleranceDays" runat="server" Text="Delivery Tolerance Days"></cc1:ucLabel>
                        </td>
                        <td style="width: 1%">
                            :
                        </td>
                        <td style="width: 12%">
                            <cc1:ucNumericTextbox ID="txtDeliveryToleranceDays" runat="server" Width="25px" onkeypress="return IsNumberKey(event, this);"
                                MaxLength="2"></cc1:ucNumericTextbox>
                            &nbsp;<cc1:ucLabel ID="lblInDays3" runat="server"></cc1:ucLabel>
                        </td>
                    </tr>
                </table>
            </cc1:ucPanel>
              <cc1:ucPanel ID="pnlODToReturn" runat="server" GroupingText="OD to Return Settings"
                CssClass="fieldset-form">
                <table width="100%" cellspacing="5" cellpadding="0" class="form-table" align="center">
                <tr><td style="width: 10%" colspan="2">
                            <cc1:ucLabel ID="lblReturnSettingsText" runat="server" Text="Office Depot to Return available for vendor returns?"/>
                        </td></tr>
                    <tr>
                     
                        <td style="width: 10%">
                            <cc1:ucRadioButton ID="rblYes" runat="server" GroupName="abc" Text="Yes" />
                        </td>
                        <td >
                            <cc1:ucRadioButton ID="rblNo" runat="server" GroupName="abc" Text="No" />
                        </td>                       
                    </tr>
                </table>
            </cc1:ucPanel>



        </div>
    </div>
    <div class="bottom-shadow">
    </div>
    <div class="button-row">
        <cc1:ucButton ID="btnSave" runat="server" Text="Save" CssClass="button" OnClientClick="return chkinput();"
            OnClick="btnSave_Click1" />
        <cc1:ucButton ID="btnBack" runat="server" Text="Back" CssClass="button" OnClick="btnBack_Click" />
    </div>
</asp:Content>

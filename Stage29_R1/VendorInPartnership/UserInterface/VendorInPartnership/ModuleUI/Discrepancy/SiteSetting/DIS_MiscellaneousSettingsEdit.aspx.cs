﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using System.Collections.Generic;

using Utilities; using WebUtilities;
using BusinessEntities.ModuleBE.AdminFunctions;
using BusinessLogicLayer.ModuleBAL.AdminFunctions;
using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using BusinessLogicLayer.ModuleBAL.Appointment.SiteSettings;


public partial class DIS_MiscellaneousSettingsEdit : CommonPage
{
    protected string InitialEscaltionRequired = WebCommon.getGlobalResourceValue("InitialEscaltionRequired");
    protected string FinalEscalationRequired = WebCommon.getGlobalResourceValue("FinalEscalationRequired");
    protected string DeliveryToleranceDaysRequired = WebCommon.getGlobalResourceValue("DeliveryToleranceDaysRequired");
    string strCorrectValueValidation = WebCommon.getGlobalResourceValue("CorrectValueValidation");


    protected void Page_Load(object sender, EventArgs e)
    {
       
        if (!IsPostBack)
        {
            GetSiteDisSettings();
        }
    }

    #region Methods

  

    public void GetSiteDisSettings()
    {
        MAS_SiteBE oMAS_SiteBE = new MAS_SiteBE();
        MAS_SiteBAL oMAS_SiteBAL = new MAS_SiteBAL();

        oMAS_SiteBE.Action = "ShowAll";
        if (GetQueryStringValue("SiteId") != null)
        {
            oMAS_SiteBE.SiteID = Convert.ToInt32(GetQueryStringValue("SiteId"));
            oMAS_SiteBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
            oMAS_SiteBE.User.UserID = Convert.ToInt32(Session["UserID"]);
            List<MAS_SiteBE> lstSiteDisSettings = oMAS_SiteBAL.GetSiteMisSettingBAL(oMAS_SiteBE);
            if (lstSiteDisSettings != null && lstSiteDisSettings.Count > 0)
            {
                lblDisSiteName.Text = lstSiteDisSettings[0].SiteDescription;

                if (lstSiteDisSettings[0].DIS_CommunicationWithEscalationType.Trim() == "D")
                {
                    rdoDebitFlow.Checked = true;
                }
                else if (lstSiteDisSettings[0].DIS_CommunicationWithEscalationType.Trim() == "C")
                {
                    rdoCreditFlow.Checked = true;
                }
                else
                {
                    rdoLoggingIssue.Checked = true;
                }
                txtInitialEscalation.Text = lstSiteDisSettings[0].DIS_DaysBeforeEscalationType2.ToString();
                txtFinalEscalation.Text = lstSiteDisSettings[0].DIS_DaysBeforeFinalEscalation.ToString();
                txtDeliveryToleranceDays.Text = lstSiteDisSettings[0].DIS_DaysAfterGoodsReturned.ToString();
                if (lstSiteDisSettings[0].IsOdToReturn == true)
                {
                    rblYes.Checked = true;
                }
                else
                {
                    rblNo.Checked = true;
                }



            }
            oMAS_SiteBAL = null;
        }
    }

    #endregion

    #region Events

    protected void btnBack_Click(object sender, EventArgs e)
    {
        EncryptQueryString("DIS_MiscellaneousSettingsOverview.aspx");
    }
    protected void btnSave_Click1(object sender, EventArgs e)
    {
        if (txtInitialEscalation.Text.Trim() != string.Empty && !validationFunctions.IsNumeric(txtInitialEscalation.Text.Trim().Replace(".", string.Empty)))
        {
            ClientScript.RegisterStartupScript(this.GetType(), "alert", "<script>alert('" + strCorrectValueValidation + "');</script>");
            txtInitialEscalation.Focus();
            return;
        }

        if (txtFinalEscalation.Text.Trim() != string.Empty && !validationFunctions.IsNumeric(txtFinalEscalation.Text.Trim().Replace(".", string.Empty)))
        {
            ClientScript.RegisterStartupScript(this.GetType(), "alert", "<script>alert('" + strCorrectValueValidation + "');</script>");
            txtFinalEscalation.Focus();
            return;
        }

        if (txtDeliveryToleranceDays.Text.Trim() != string.Empty && !validationFunctions.IsNumeric(txtDeliveryToleranceDays.Text.Trim().Replace(".", string.Empty)))
        {
            ClientScript.RegisterStartupScript(this.GetType(), "alert", "<script>alert('" + strCorrectValueValidation + "');</script>");
            txtDeliveryToleranceDays.Focus();
            return;
        }
        MAS_SiteBE oMAS_SiteBE = new MAS_SiteBE();
        MAS_SiteBAL oMAS_SiteBAL = new MAS_SiteBAL();

        oMAS_SiteBE.Action = "UpdateSiteDisSettings";
        oMAS_SiteBE.SiteID = Convert.ToInt32(GetQueryStringValue("SiteId"));
        oMAS_SiteBE.DIS_CommunicationWithEscalationType=rdoDebitFlow.Checked?"D":(rdoCreditFlow.Checked?"C":"N");
            
       
        oMAS_SiteBE.DIS_DaysBeforeEscalationType2 = txtInitialEscalation.Text.Trim() != "" ? Convert.ToInt32(txtInitialEscalation.Text.Trim()) : (int?)null;
        oMAS_SiteBE.DIS_DaysBeforeFinalEscalation = txtFinalEscalation.Text.Trim() != "" ? Convert.ToInt32(txtFinalEscalation.Text.Trim()) : (int?)null;
        oMAS_SiteBE.DIS_DaysAfterGoodsReturned = txtDeliveryToleranceDays.Text.Trim() != "" ? Convert.ToInt32(txtDeliveryToleranceDays.Text.Trim()) : (int?)null;
        oMAS_SiteBE.IsOdToReturn = rblYes.Checked == true ? true : false;

        oMAS_SiteBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();

        int? iResult = oMAS_SiteBAL.addEditSiteSettingsBAL(oMAS_SiteBE);
        if (iResult == 0)
        {
            EncryptQueryString("DIS_MiscellaneousSettingsOverview.aspx");
        }
        oMAS_SiteBAL = null;
    }

   

    #endregion
  
}
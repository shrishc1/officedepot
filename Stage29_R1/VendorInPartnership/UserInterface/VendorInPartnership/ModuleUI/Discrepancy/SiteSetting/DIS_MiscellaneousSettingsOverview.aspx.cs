﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.AdminFunctions;
using BusinessLogicLayer.ModuleBAL.AdminFunctions;


public partial class ModuleUI_Discrepancy_DIS_MiscellaneousSettingsOverview : CommonPage
{
    #region Action
    protected void Page_Load(object sender, EventArgs e)
    {
      
        if (!IsPostBack)
        {
            BindGrid();
        }
        btnExportToExcel1.GridViewControl = grdMiscellneousSettings;
        btnExportToExcel1.FileName = "Dis-MiscellaneousSettingsOverview";
    }
    protected void grdMiscellneousSettings_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                string WorkFlowSettings = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "DIS_CommunicationWithEscalationType"));
                if (WorkFlowSettings == "D")
                    e.Row.Cells[1].Text = "Debit Flow";
                else if (WorkFlowSettings == "C")
                    e.Row.Cells[1].Text = "Credit Flow";
                else
                    e.Row.Cells[1].Text = "No escalation";
            }
        }
        catch (Exception ex)
        {

        }
    }
    #endregion

    #region Methods
    private void BindGrid()
    {
        MAS_SiteBE oMAS_SITEBE = new MAS_SiteBE();
        MAS_SiteBAL oMAS_SITEBAL = new MAS_SiteBAL();

        List<MAS_SiteBE> lstSites = new List<MAS_SiteBE>();

        oMAS_SITEBE.Action = "ShowAll";
        oMAS_SITEBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
        oMAS_SITEBE.User.UserID = Convert.ToInt32(Session["UserID"]);

        lstSites = oMAS_SITEBAL.GetSiteMisSettingBAL(oMAS_SITEBE);
        grdMiscellneousSettings.DataSource = lstSites;
        grdMiscellneousSettings.DataBind();
        ViewState["lstSites"] = lstSites;
        oMAS_SITEBAL = null;
    }
    #endregion

    public override BusinessEntities.BaseBe[] SubSort(GridViewSortEventArgs e)
    {
        return Utilities.GenericListHelper<MAS_SiteBE>.SortList((List<MAS_SiteBE>)ViewState["lstSites"], e.SortExpression, e.SortDirection).ToArray();
    }
   
}
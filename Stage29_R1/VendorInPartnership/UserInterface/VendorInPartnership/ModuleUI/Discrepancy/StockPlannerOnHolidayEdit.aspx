﻿<%@ Page Language="C#" AutoEventWireup="true"
    MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    CodeFile="StockPlannerOnHolidayEdit.aspx.cs" Inherits="ModuleUI_Discrepancy_StockPlannerOnHolidayEdit" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>

<%@ Register Src="~/CommonUI/UserControls/ucNoPastDate.ascx" TagPrefix="dd" TagName="ucNoPastDate" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript">
        function RequiredCategory() {
            if (!($("[id$=chkYes]").is(':checked'))) {
                alert('<%= AbsentRequired %>');
                return false;
            } 
          
            if ($("[id$=txtNoPastDate]").val() == undefined || $("[id$=txtNoPastDate]").val() == '') {

                alert('<%= DateOfReturnRequired %>');
                $("[id$=txtNoPastDate]").focus();
                return false;
            }
        }     
    </script>

    <h2>
        <cc1:ucLabel ID="lblStockPlannerHolidayCoverEdit" runat="server"></cc1:ucLabel>
    </h2>
    <asp:ScriptManager ID="ScriptManager1" runat="server" AsyncPostBackTimeout="1200">
    </asp:ScriptManager>
    <asp:HiddenField ID="hdnDateOfReturn" runat="server" />
    <table width="95%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
        <tr>
            <td style="font-weight: bold;">
                <cc1:ucLabel ID="lblStockPlannerName" runat="server" Text="Planner name">
                </cc1:ucLabel>
            </td>
            <td style="font-weight: bold;">:
            </td>
            <td>
                <cc1:ucLabel ID="lblStockPlannerValue" runat="server">
                </cc1:ucLabel>
            </td>
            <td style="font-weight: bold;"></td>
            <td style="font-weight: bold;"></td>
            <td style="font-weight: bold;"></td>
        </tr>
        <tr>
            <td style="font-weight: bold;">
                <cc1:ucLabel ID="lblAbsent" runat="server" Text="Absent">
                </cc1:ucLabel>
            </td>
            <td style="font-weight: bold;">:
            </td>
            <td>
                <cc1:ucCheckbox ID="chkYes" runat="server" Text=" Yes" />
            </td>
            <td style="font-weight: bold;">
                <cc1:ucLabel ID="lblDateOfReturn" runat="server" Text="Date Of Return"> 
                </cc1:ucLabel>
                :
            </td>

            <td>
                <dd:ucNoPastDate runat="server" id="txtDateOfReturn"  style="width: 170px" />               
               
            </td>
            <td style="font-weight: bold;"></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td valign="top" colspan="5">
                <cc1:ucLabel ID="lblSelectPlannerforholidaycover" runat="server"></cc1:ucLabel>
            </td>
        </tr>
        <tr id="trStockPlanner" runat="server">
            <td style="font-weight: bold;" valign="middle">
                <cc1:ucLabel ID="lblPlanner" runat="server" Text="Planner"></cc1:ucLabel>
            </td>
            <td style="font-weight: bold;" valign="middle" width="1%">:
            </td>
            <td width="20%">
                <asp:UpdatePanel ID="UpCountry" runat="server">
                    <ContentTemplate>
                        <cc1:ucListBox ID="uclstStockPlannerLeft" runat="server" Height="200px" Width="320px">
                        </cc1:ucListBox>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
            <td valign="middle" align="center">
                <div>
                    <cc1:ucButton ID="btnMoveRightAllStockPlanner" runat="server" Text=">>" CssClass="button"
                        Width="35px" OnClick="btnMoveRightAllStockPlanner_Click" />
                </div>
                <br />
                <div>
                    <cc1:ucButton ID="btnMoveRightStockPlanner" runat="server" Text="&gt;" CssClass="button"
                        Width="35px" OnClick="btnMoveRightStockPlanner_Click" />
                </div>
                <br />
                <div>
                    <cc1:ucButton ID="btnMoveLeftStockPlanner" runat="server" Text="&lt;" CssClass="button"
                        Width="35px" OnClick="btnMoveLeftStockPlanner_Click" />
                </div>
                <br />
                <div>
                    <cc1:ucButton ID="btnMoveLeftAllStockPlanner" runat="server" Text="&lt;&lt;" CssClass="button"
                        Width="35px" OnClick="btnMoveLeftAllStockPlanner_Click" />
                </div>
            </td>
            <td>
                <cc1:ucListBox ID="uclstStockPlannerRight" runat="server" Height="200px" Width="320px">
                </cc1:ucListBox>
                <asp:CustomValidator ID="cusvStockPlannerReq" runat="server" ClientValidationFunction="checkStockPlannerSelected"
                    ValidationGroup="Save" Display="None"></asp:CustomValidator>
            </td>
        </tr>
    </table>
    <div class="button-row">
        <cc1:ucButton ID="btnSave" runat="server" Visible="false" CssClass="button" ValidationGroup="SPDetail"
            OnClientClick="return RequiredCategory();" OnClick="btnUpdate_Click" />
        <cc1:ucButton ID="btnBack" runat="server" Text="Back" CssClass="button" PostBackUrl="StockPlannerOnHoliday.aspx" />
    </div>
</asp:Content>

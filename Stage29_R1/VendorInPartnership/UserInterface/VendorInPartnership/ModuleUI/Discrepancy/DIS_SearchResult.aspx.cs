﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.AdminFunctions;
using BusinessEntities.ModuleBE.Discrepancy.LogDiscrepancy;
using BusinessLogicLayer.ModuleBAL.AdminFunctions;
using BusinessLogicLayer.ModuleBAL.Discrepancy.LogDiscrepancy;
using Utilities;
using WebUtilities;
using System.Linq;
using BusinessLogicLayer.ModuleBAL.Security;
using BusinessEntities.ModuleBE.Security;
using System.Web;

public partial class DIS_SearchResult : CommonPage
{
    #region Page Level Declarations
    DiscrepancyBE oNewDiscrepancyBE = null;
    DiscrepancyBAL oNewDiscrepancyBAL = null;
    string PleaseEnterNumericValueForSearchBySku = WebCommon.getGlobalResourceValue("PleaseEnterNumericValueForSearchBySku");
    #endregion

    public override void TemplateSelectedIndexChanged()
    {
        base.TemplateSelectedIndexChanged();

        int VendorTemplateId = Convert.ToInt32(ucVendorTemplateSelect.innerControlddlTemplate.SelectedValue);

        ListBox lstSelectedVendor = (ListBox)msVendor.FindControl("lstRight");
        ListBox lstVendor = (ListBox)msVendor.FindControl("ucVendor").FindControl("lstLeft");

        if (VendorTemplateId > 0)
        {

            SCT_TemplateBAL oSCT_TemplateBAL = new SCT_TemplateBAL();
            SCT_TemplateBE oSCT_TemplateBE = new SCT_TemplateBE();

            oSCT_TemplateBE.VendorTemplateID = VendorTemplateId;
            oSCT_TemplateBE.Action = "GetVendorTemplateInfobyID";
            List<SCT_TemplateBE> lstVendorTemplate = oSCT_TemplateBAL.GetVendorTemplateById(oSCT_TemplateBE);

            if (lstVendorTemplate != null && lstVendorTemplate.Count > 0)
            {
                lstSelectedVendor.Items.Clear();
                msVendor.innerControlHiddenField.Value = string.Empty;
                msVendor.innerControlHiddenField2.Value = string.Empty;
                for (int iCount = 0; iCount < lstVendorTemplate.Count; iCount++)
                {
                    lstSelectedVendor.Items.Add(new ListItem(lstVendorTemplate[iCount].Vendor.Vendor_Name, lstVendorTemplate[iCount].Vendor.VendorID.ToString()));
                    msVendor.SelectedVendorIDs = lstVendorTemplate[iCount].Vendor.VendorID.ToString();
                    msVendor.SelectedVendorName = lstVendorTemplate[iCount].Vendor.Vendor_Name.ToString();
                }
            }
        }
        else
        {
            lstSelectedVendor.Items.Clear();
            msVendor.innerControlHiddenField.Value = string.Empty;
            msVendor.innerControlHiddenField2.Value = string.Empty;
        }
    }

    protected void Page_Prerender(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            txtDiscrepancyDateRangeFrom.Attributes.Add("readonly", "readonly");
            txtDiscrepancyDateRangeTo.Attributes.Add("readonly", "readonly");
        }
    }

    protected void Page_InIt(object sender, EventArgs e)
    {
        ucVendorTemplateSelect.CurrentPage = this;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        HttpContext.Current.Response.Cache.SetExpires(DateTime.UtcNow.AddDays(-1));
        HttpContext.Current.Response.Cache.SetValidUntilExpires(false);
        HttpContext.Current.Response.Cache.SetRevalidation(HttpCacheRevalidation.AllCaches);
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.Cache.SetNoStore();

        UIUtility.PageValidateForODUser();

        ucExportExpediteDiscrepancy.CurrentPage = this;
        ucExportExpediteDiscrepancy.FileName = "DiscrepancySearch";
        ucExportExpediteDiscrepancy.Visible = false;

        Label lblModuleText = (Label)Page.Master.FindControl("lblModuleText");
        if (mvDiscrepancyList.ActiveViewIndex == 0)
        {
            lblModuleText.Text = WebCommon.getGlobalResourceValue("DiscrepanciesSearchCriteria"); 
            lblSearchResultT.Text = WebCommon.getGlobalResourceValue("SearchCriteria"); 
            Session["PageMode"] = "Search";
        }

        if (!IsPostBack)
        {
            btnReturntoSearchCriteria.Visible = false;
            mvDiscrepancyList.ActiveViewIndex = 0;
            BindDiscrepancyType();
            BindSiteList();
            Pager1.PageSize = 200;

           

            //coming from the add image page
            if (GetQueryStringValue("FromPage") != null && GetQueryStringValue("FromPage").ToString().Trim() == "images" ||
                GetQueryStringValue("FromPage") != null && GetQueryStringValue("FromPage").ToString().Trim() == "recdash"
                || Request.QueryString["FromPage"] != null && Request.QueryString["FromPage"].ToString().Trim() == "images"
                || GetQueryStringValue("PreviousPage") != null && GetQueryStringValue("PreviousPage").ToString() == "usercontrol"
                || GetQueryStringValue("PreviousPage") != null && GetQueryStringValue("PreviousPage").ToString() == "SearchResult")
            {

                mvDiscrepancyList.ActiveViewIndex = 1;
                btnSearch.Visible = false;

                /* This will call in case new discrepancy creation. */
                if (GetQueryStringValue("SiteId") != null && GetQueryStringValue("DisDate") != null)
                {
                    Session["PageMode"] = "Todays";
                    lblModuleText.Text = WebCommon.getGlobalResourceValue("TodaysDiscrepancies");
                    lblSearchResultT.Text = WebCommon.getGlobalResourceValue("TodaysDiscrepancies");
                    ViewState["TodayDiscrepancy"] = "TodayDiscrepancy";
                    this.BindGrid();

                }
                else
                {
                    Session["PageMode"] = "Search";
                    lblModuleText.Text = WebCommon.getGlobalResourceValue("DiscrepanciesSearchResult");
                    lblSearchResultT.Text = WebCommon.getGlobalResourceValue("SearchResult");

                    if (Session["SearchCriteria"] != null)
                        this.BindGrid((DiscrepancyBE)Session["SearchCriteria"]);
                    else
                        this.BindGrid();
                }
            }

            if (GetQueryStringValue("AdminSearchPage") != null && GetQueryStringValue("DicrepancyNo") != null)
            {

                mvDiscrepancyList.ActiveViewIndex = 1;
                lblModuleText.Text = WebCommon.getGlobalResourceValue("DiscrepanciesSearchResult");
                lblSearchResultT.Text = WebCommon.getGlobalResourceValue("SearchResult");
                btnSearch.Visible = false;
                btnReturntoSearchCriteria.Visible = true;
                txtDiscrepancyNo.Text = GetQueryStringValue("VDRNO");
                this.BindGrid();
                var DiscrepancyRaiseMsg = GetQueryStringValue("DicrepancyNo");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('" + DiscrepancyRaiseMsg + "')", true);

            }
            else if (GetQueryStringValue("AdminSearchPage") != null && GetQueryStringValue("VDRNO") != null)
            {
                txtDiscrepancyNo.Text = GetQueryStringValue("VDRNO");
                mvDiscrepancyList.ActiveViewIndex = 1;
                lblModuleText.Text = WebCommon.getGlobalResourceValue("DiscrepanciesSearchResult");
                lblSearchResultT.Text = WebCommon.getGlobalResourceValue("SearchResult");
                btnSearch.Visible = false;
                btnReturntoSearchCriteria.Visible = true;
                this.BindGrid();
            }
        }
    }

    #region SearchCriteria
    private void BindDiscrepancyType()
    {
        DiscrepancyBE oNewDiscrepancyBE = new DiscrepancyBE();
        DiscrepancyBAL oNewDiscrepancyBAL = new DiscrepancyBAL();

        oNewDiscrepancyBE.Action = "GetDiscrepancyType";


        List<DiscrepancyBE> lstDiscrepancyType = oNewDiscrepancyBAL.GetDiscrepancyTypeBAL(oNewDiscrepancyBE);

        if (lstDiscrepancyType != null)
        {
            FillControls.FillDropDown(ref ddlDiscrepancyType, lstDiscrepancyType, "DiscrepancyType", "DiscrepancyTypeID", "---Select---");
        }
    }

    private void BindSiteList()
    {
        if (GetQueryStringValue("SiteId") == null)
        {
            MAS_SiteBE oMAS_SiteBE = new MAS_SiteBE();
            MAS_SiteBAL oMAS_SiteBAL = new MAS_SiteBAL();
            oMAS_SiteBE.Action = "ShowAll";

            oMAS_SiteBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
            oMAS_SiteBE.User.UserID = Convert.ToInt32(Session["UserID"]);
            oMAS_SiteBE.SiteCountryID = 0;
            List<MAS_SiteBE> lstSite =oMAS_SiteBAL.GetSiteMisSettingBAL(oMAS_SiteBE);
            if (lstSite.Count > 0 && lstSite != null)
            {
                FillControls.FillListBox(ref UclstSiteList, lstSite, "SiteDescription", "SiteID");
            }
        }

    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(txtSearchBySkuText.Text) || !string.IsNullOrWhiteSpace(txtSearchBySkuText.Text))
        {
            if (!txtSearchBySkuText.Text.IsNumeric())
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + PleaseEnterNumericValueForSearchBySku + "')", true);
                return;
            }
        }

        mvDiscrepancyList.ActiveViewIndex = 1;
        Label lblModuleText = (Label)Page.Master.FindControl("lblModuleText");
        lblModuleText.Text = WebCommon.getGlobalResourceValue("DiscrepanciesSearchResult");
        lblSearchResultT.Text = WebCommon.getGlobalResourceValue("SearchResult");
        btnSearch.Visible = false;
        btnReturntoSearchCriteria.Visible = true;
        this.BindGrid();
    }

    protected virtual void btnMoveRight_Click(object sender, EventArgs e)
    {
        if (UclstSiteList.SelectedItem != null)
        {
            FillControls.MoveOneItem(UclstSiteList, UclstSiteSelected);
        }
    }

    protected virtual void btnMoveLeft_Click(object sender, EventArgs e)
    {
        if (UclstSiteSelected.SelectedItem != null)
        {
            FillControls.MoveOneItem(UclstSiteSelected, UclstSiteList);
        }
    }

    #endregion

    #region SearchListing
    private void BindGrid(DiscrepancyBE discrepancy = null, int Page = 1)
    {
        oNewDiscrepancyBAL = new DiscrepancyBAL();
        List<DiscrepancyBE> lstDisLog = null;
        if (discrepancy == null)
        {
            oNewDiscrepancyBE = new DiscrepancyBE();
            oNewDiscrepancyBE.Action = "GetDiscrepancyLog";

            if (GetQueryStringValue("SiteId") != null && GetQueryStringValue("DisDate") != null)
            {
                var DisDate = GetQueryStringValue("DisDate");
                oNewDiscrepancyBE.SCDiscrepancyDateFrom = string.IsNullOrEmpty(DisDate) ? (DateTime?)null : Common.GetMM_DD_YYYY(DisDate);
                oNewDiscrepancyBE.SCDiscrepancyDateTo = string.IsNullOrEmpty(DisDate) ? (DateTime?)null : Common.GetMM_DD_YYYY(DisDate);
                oNewDiscrepancyBE.UserID = Session["UserID"] != null ? Convert.ToInt32(Session["UserID"]) : (int?)null;
                oNewDiscrepancyBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
                oNewDiscrepancyBE.User.RoleTypeFlag = ddlActionPendingWithDropDown.SelectedItem.Value.Trim();
                oNewDiscrepancyBE.SCSelectedSiteIDs = GetQueryStringValue("SiteId");
            }
            else
            {
                //Search Creteria
                oNewDiscrepancyBE.SCDiscrepancyTypeID = ddlDiscrepancyType.SelectedIndex != 0 ? Convert.ToInt32(ddlDiscrepancyType.SelectedItem.Value) : (int?)null;
                oNewDiscrepancyBE.SCDiscrepancyNo = txtDiscrepancyNo.Text != string.Empty ? txtDiscrepancyNo.Text.Trim() : null;
                oNewDiscrepancyBE.SCPurchaseOrderNo = txtPurchaseOrder.Text != string.Empty ? txtPurchaseOrder.Text.Trim() : null;
                oNewDiscrepancyBE.SCPurchaseOrderDate = string.IsNullOrEmpty(txtPurchaseOrderDate.Text) ? (DateTime?)null : Common.GetMM_DD_YYYY(txtPurchaseOrderDate.Text);
                oNewDiscrepancyBE.SCDeliveryNoteNumber = txtDeliveryNote.Text != string.Empty ? txtDeliveryNote.Text.Trim() : null;
                oNewDiscrepancyBE.SCDiscrepancyDateFrom = string.IsNullOrEmpty(txtDiscrepancyDateRangeFrom.Text) ? (DateTime?)null : Common.GetMM_DD_YYYY(txtDiscrepancyDateRangeFrom.Text);
                oNewDiscrepancyBE.SCDiscrepancyDateTo = string.IsNullOrEmpty(txtDiscrepancyDateRangeTo.Text) ? (DateTime?)null : Common.GetMM_DD_YYYY(txtDiscrepancyDateRangeTo.Text);
                oNewDiscrepancyBE.UserID = Session["UserID"] != null ? Convert.ToInt32(Session["UserID"]) : (int?)null;
                oNewDiscrepancyBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
                oNewDiscrepancyBE.User.RoleTypeFlag = ddlActionPendingWithDropDown.SelectedItem.Value.Trim();
                                
                oNewDiscrepancyBE.SCSelectedSiteIDs = !string.IsNullOrEmpty(hiddenSelectedIDs.Value)? hiddenSelectedIDs.Value.Trim(',') :null;              

                oNewDiscrepancyBE.SCSelectedVendorIDs = msVendor.SelectedVendorIDs;
                oNewDiscrepancyBE.SCSelectedSPIDs = msStockPlanner.SelectedStockPlannerIDs;
                oNewDiscrepancyBE.DiscrepancyStatus = string.IsNullOrEmpty(ddlDiscrepancyStatus.SelectedValue) ? null : ddlDiscrepancyStatus.SelectedValue;
                oNewDiscrepancyBE.QueryDiscrepancy = new DISLog_QueryDiscrepancyBE();
                oNewDiscrepancyBE.QueryDiscrepancy.Query = ddlQueryStatus.SelectedValue;
                oNewDiscrepancyBE.EscalationLevel = ddlEscalation.SelectedIndex != 0 ? ddlEscalation.SelectedItem.Value : string.Empty;
                oNewDiscrepancyBE.SearchByODSku = txtSearchBySkuText.Text;
                oNewDiscrepancyBE.SearchByCatCode = txtSearchByCatCodeText.Text;
            }

            oNewDiscrepancyBE.Flag = oNewDiscrepancyBE.DiscrepancyLogID > 0;
            oNewDiscrepancyBE.Page = Page;
            lstDisLog = oNewDiscrepancyBAL.GetDiscrepancyLogDetailsBAL(oNewDiscrepancyBE);

            if (lstDisLog != null && lstDisLog.Count > 0 && ddlActionRequired.SelectedValue != "")
            {
                lstDisLog = lstDisLog.Where(w => w.ActionRequired == ddlActionRequired.SelectedValue).ToList();
            }

            gvDisLog.DataSource = lstDisLog;
            gvDisLog.DataBind();
            ucExportExpediteDiscrepancy.Visible = true;
            ViewState["lstSites"] = lstDisLog;
            Session["SearchCriteria"] = oNewDiscrepancyBE;
            hiddenSelectedIDs.Value = string.Empty;
        }
        else
        {
            mvDiscrepancyList.ActiveViewIndex = 1;
            btnSearch.Visible = false;
            btnReturntoSearchCriteria.Visible = true;

            if (discrepancy.SCDiscrepancyTypeID != null)
                if (discrepancy.SCDiscrepancyTypeID > 0)
                    ddlDiscrepancyType.SelectedIndex = ddlDiscrepancyType.Items.IndexOf(ddlDiscrepancyType.Items.FindByValue(Convert.ToString(discrepancy.SCDiscrepancyTypeID)));

            if (!string.IsNullOrEmpty(discrepancy.SCDiscrepancyNo))
                txtDiscrepancyNo.Text = discrepancy.SCDiscrepancyNo;

            if (!string.IsNullOrEmpty(discrepancy.SCPurchaseOrderNo))
                txtPurchaseOrder.Text = discrepancy.SCPurchaseOrderNo;

            if (discrepancy.SCPurchaseOrderDate != null)
                txtPurchaseOrderDate.Text = Convert.ToString(discrepancy.SCPurchaseOrderDate);

            if (!string.IsNullOrEmpty(discrepancy.SCDeliveryNoteNumber))
                txtDeliveryNote.Text = discrepancy.SCDeliveryNoteNumber;

            if (discrepancy.SCDiscrepancyDateFrom != null)
                txtDiscrepancyDateRangeFrom.Text = Convert.ToString(discrepancy.SCDiscrepancyDateFrom);

            if (discrepancy.SCDiscrepancyDateTo != null)
                txtDiscrepancyDateRangeTo.Text = Convert.ToString(discrepancy.SCDiscrepancyDateTo);

            if (!string.IsNullOrEmpty(discrepancy.User.RoleTypeFlag))
                ddlActionPendingWithDropDown.SelectedIndex =
                    ddlActionPendingWithDropDown.Items.IndexOf(ddlActionPendingWithDropDown.Items.FindByValue(Convert.ToString(discrepancy.User.RoleTypeFlag)));

            if (!string.IsNullOrEmpty(discrepancy.SCSelectedSiteIDs))
                hiddenSelectedIDs.Value = discrepancy.SCSelectedSiteIDs;

            if (!string.IsNullOrEmpty(discrepancy.SCSelectedVendorIDs))
                msVendor.SelectedVendorIDs = discrepancy.SCSelectedVendorIDs;

            if (!string.IsNullOrEmpty(discrepancy.DiscrepancyStatus))
                ddlDiscrepancyStatus.SelectedIndex = ddlDiscrepancyStatus.Items.IndexOf(ddlDiscrepancyStatus.Items.FindByValue(Convert.ToString(discrepancy.DiscrepancyStatus)));

            if (!string.IsNullOrEmpty(discrepancy.QueryDiscrepancy.Query))
                ddlDiscrepancyStatus.SelectedIndex = ddlDiscrepancyStatus.Items.IndexOf(ddlDiscrepancyStatus.Items.FindByValue(discrepancy.QueryDiscrepancy.Query));

            if (!string.IsNullOrEmpty(discrepancy.EscalationLevel))
                ddlEscalation.SelectedIndex = ddlEscalation.Items.IndexOf(ddlEscalation.Items.FindByValue(Convert.ToString(discrepancy.EscalationLevel)));

            if (!string.IsNullOrEmpty(discrepancy.SearchByODSku))
                txtSearchBySkuText.Text = discrepancy.SearchByODSku;

            if (!string.IsNullOrEmpty(discrepancy.SearchByCatCode))
                txtSearchByCatCodeText.Text = discrepancy.SearchByCatCode;

            if (!string.IsNullOrEmpty(discrepancy.ActionRequired))
                ddlActionRequired.SelectedIndex = ddlActionRequired.Items.IndexOf(ddlActionRequired.Items.FindByValue(Convert.ToString(discrepancy.ActionRequired)));

            lstDisLog = oNewDiscrepancyBAL.GetDiscrepancyLogDetailsBAL(discrepancy);

            if (lstDisLog != null && lstDisLog.Count > 0 && ddlActionRequired.SelectedValue != "")
            {
                lstDisLog = lstDisLog.Where(w => w.ActionRequired == ddlActionRequired.SelectedValue).ToList();
            }

            Session["SearchCriteria"] = discrepancy;
        }

        if (lstDisLog != null)
        {
            gvDisLog.DataSource = lstDisLog;
            gvDisLog.DataBind();

            ViewState["lstSites"] = lstDisLog;
            Pager1.PageSize = 200;
            gvDisLog.PageIndex = Page;
            Pager1.CurrentIndex = Page;

            Pager1.ItemCount = lstDisLog != null && lstDisLog.Count > 0 ? lstDisLog[0].TotalRecords : 0;

            Pager1.Visible = true;
        }

        tblGrid.Visible = gvDisLog.Rows.Count > 0; 

        oNewDiscrepancyBAL = null;
    }

    public void pager_Command(object sender, CommandEventArgs e)
    {
        int currnetPageIndx = Convert.ToInt32(e.CommandArgument);
        Pager1.CurrentIndex = currnetPageIndx;
        BindGrid(null,currnetPageIndx);
    }
    protected void gvDisLog_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType != DataControlRowType.Header && e.Row.RowType != DataControlRowType.Footer)
        {
            HyperLink hypLinkToDisDetails = (HyperLink)e.Row.FindControl("hypLinkToDisDetails");
            HiddenField hdnDisType = (HiddenField)e.Row.FindControl("hdnDisType");
            HiddenField hdnDisLogID = (HiddenField)e.Row.FindControl("hdnDisLogID");
            HiddenField hdnUserID = (HiddenField)e.Row.FindControl("hdnUserID");
            HiddenField hdnWorkFlowID = (HiddenField)e.Row.FindControl("hdnWorkFlowID");
            string status = e.Row.Cells[1].Text.ToString();
            string VDRNo = hypLinkToDisDetails.Text;

            string TodaysDis = Convert.ToString(ViewState["TodayDiscrepancy"]);

            TodaysDis = string.IsNullOrEmpty(TodaysDis) ? "SearchDiscrepancy" : Convert.ToString(ViewState["TodayDiscrepancy"]);

            switch (Convert.ToInt32(hdnDisType.Value.Trim()))
            {
                case 1:
                    hypLinkToDisDetails.NavigateUrl = EncryptQuery("LogDiscrepancy/DISLog_Overs.aspx?status=" + status + "&disLogID=" + hdnDisLogID.Value + "&VDRNo=" + VDRNo + "&UserID=" + hdnUserID.Value + "&RoleTypeFlag=" + ddlActionPendingWithDropDown.SelectedItem.Value.Trim() + "&PreviousPage=SearchResult&WorkFlowID=" + hdnWorkFlowID.Value + "&SiteID=" + GetQueryStringValue("SiteId") + "&DisDate=" + GetQueryStringValue("DisDate") + "&TodayDis=TodayDiscrepancy");
                    break;
                case 2:
                    hypLinkToDisDetails.NavigateUrl = EncryptQuery("LogDiscrepancy/DISLog_Shortage.aspx?status=" + status + "&disLogID=" + hdnDisLogID.Value + "&VDRNo=" + VDRNo + "&UserID=" + hdnUserID.Value + "&RoleTypeFlag=" + ddlActionPendingWithDropDown.SelectedItem.Value.Trim() + "&PreviousPage=SearchResult&WorkFlowID=" + hdnWorkFlowID.Value + "&SiteID=" + GetQueryStringValue("SiteId") + "&DisDate=" + GetQueryStringValue("DisDate"));
                    break;
                case 3:
                    hypLinkToDisDetails.NavigateUrl = EncryptQuery("LogDiscrepancy/DISLog_GoodsReceivedDamaged.aspx?status=" + status + "&disLogID=" + hdnDisLogID.Value + "&VDRNo=" + VDRNo + "&UserID=" + hdnUserID.Value + "&RoleTypeFlag=" + ddlActionPendingWithDropDown.SelectedItem.Value.Trim() + "&PreviousPage=SearchResult&WorkFlowID=" + hdnWorkFlowID.Value + "&SiteID=" + GetQueryStringValue("SiteId") + "&DisDate=" + GetQueryStringValue("DisDate"));
                    break;
                case 4:
                    hypLinkToDisDetails.NavigateUrl = EncryptQuery("LogDiscrepancy/DISLog_NoPurchaseOrder.aspx?status=" + status + "&disLogID=" + hdnDisLogID.Value + "&VDRNo=" + VDRNo + "&UserID=" + hdnUserID.Value + "&RoleTypeFlag=" + ddlActionPendingWithDropDown.SelectedItem.Value.Trim() + "&PreviousPage=SearchResult&WorkFlowID=" + hdnWorkFlowID.Value + "&SiteID=" + GetQueryStringValue("SiteId") + "&DisDate=" + GetQueryStringValue("DisDate"));
                    break;
                case 5:
                    hypLinkToDisDetails.NavigateUrl = EncryptQuery("LogDiscrepancy/DISLog_NoPaperwork.aspx?status=" + status + "&disLogID=" + hdnDisLogID.Value + "&VDRNo=" + VDRNo + "&UserID=" + hdnUserID.Value + "&RoleTypeFlag=" + ddlActionPendingWithDropDown.SelectedItem.Value.Trim() + "&PreviousPage=SearchResult&WorkFlowID=" + hdnWorkFlowID.Value + "&SiteID=" + GetQueryStringValue("SiteId") + "&DisDate=" + GetQueryStringValue("DisDate"));
                    break;
                case 6:
                    hypLinkToDisDetails.NavigateUrl = EncryptQuery("LogDiscrepancy/DISLog_IncorrectProduct.aspx?status=" + status + "&disLogID=" + hdnDisLogID.Value + "&VDRNo=" + VDRNo + "&UserID=" + hdnUserID.Value + "&RoleTypeFlag=" + ddlActionPendingWithDropDown.SelectedItem.Value.Trim() + "&PreviousPage=SearchResult&WorkFlowID=" + hdnWorkFlowID.Value + "&SiteID=" + GetQueryStringValue("SiteId") + "&DisDate=" + GetQueryStringValue("DisDate"));
                    break;
                case 7:
                    hypLinkToDisDetails.NavigateUrl = EncryptQuery("LogDiscrepancy/DISLog_PresentationIssue.aspx?status=" + status + "&disLogID=" + hdnDisLogID.Value + "&VDRNo=" + VDRNo + "&UserID=" + hdnUserID.Value + "&RoleTypeFlag=" + ddlActionPendingWithDropDown.SelectedItem.Value.Trim() + "&PreviousPage=SearchResult&WorkFlowID=" + hdnWorkFlowID.Value + "&SiteID=" + GetQueryStringValue("SiteId") + "&DisDate=" + GetQueryStringValue("DisDate"));
                    break;
                case 8:
                    hypLinkToDisDetails.NavigateUrl = EncryptQuery("LogDiscrepancy/DISLog_IncorrectAddress.aspx?status=" + status + "&disLogID=" + hdnDisLogID.Value + "&VDRNo=" + VDRNo + "&UserID=" + hdnUserID.Value + "&RoleTypeFlag=" + ddlActionPendingWithDropDown.SelectedItem.Value.Trim() + "&PreviousPage=SearchResult&WorkFlowID=" + hdnWorkFlowID.Value + "&SiteID=" + GetQueryStringValue("SiteId") + "&DisDate=" + GetQueryStringValue("DisDate"));
                    break;
                case 9:
                    hypLinkToDisDetails.NavigateUrl = EncryptQuery("LogDiscrepancy/DISLog_PaperworkAmended.aspx?status=" + status + "&disLogID=" + hdnDisLogID.Value + "&VDRNo=" + VDRNo + "&UserID=" + hdnUserID.Value + "&RoleTypeFlag=" + ddlActionPendingWithDropDown.SelectedItem.Value.Trim() + "&PreviousPage=SearchResult&WorkFlowID=" + hdnWorkFlowID.Value + "&SiteID=" + GetQueryStringValue("SiteId") + "&DisDate=" + GetQueryStringValue("DisDate"));
                    break;
                case 10:
                    hypLinkToDisDetails.NavigateUrl = EncryptQuery("LogDiscrepancy/DISLog_WrongPackSize.aspx?status=" + status + "&disLogID=" + hdnDisLogID.Value + "&VDRNo=" + VDRNo + "&UserID=" + hdnUserID.Value + "&RoleTypeFlag=" + ddlActionPendingWithDropDown.SelectedItem.Value.Trim() + "&PreviousPage=SearchResult&WorkFlowID=" + hdnWorkFlowID.Value + "&SiteID=" + GetQueryStringValue("SiteId") + "&DisDate=" + GetQueryStringValue("DisDate"));
                    break;
                case 11:
                    hypLinkToDisDetails.NavigateUrl = EncryptQuery("LogDiscrepancy/DISLog_FailPalletSpecification.aspx?status=" + status + "&disLogID=" + hdnDisLogID.Value + "&VDRNo=" + VDRNo + "&UserID=" + hdnUserID.Value + "&RoleTypeFlag=" + ddlActionPendingWithDropDown.SelectedItem.Value.Trim() + "&PreviousPage=SearchResult&WorkFlowID=" + hdnWorkFlowID.Value + "&SiteID=" + GetQueryStringValue("SiteId") + "&DisDate=" + GetQueryStringValue("DisDate"));
                    break;
                case 12:
                    hypLinkToDisDetails.NavigateUrl = EncryptQuery("LogDiscrepancy/DISLog_QualityIssue.aspx?status=" + status + "&disLogID=" + hdnDisLogID.Value + "&VDRNo=" + VDRNo + "&UserID=" + hdnUserID.Value + "&RoleTypeFlag=" + ddlActionPendingWithDropDown.SelectedItem.Value.Trim() + "&PreviousPage=SearchResult&WorkFlowID=" + hdnWorkFlowID.Value + "&SiteID=" + GetQueryStringValue("SiteId") + "&DisDate=" + GetQueryStringValue("DisDate"));
                    break;
                case 13:
                    hypLinkToDisDetails.NavigateUrl = EncryptQuery("LogDiscrepancy/DISLog_PrematureInvoiceReceipt.aspx?status=" + status + "&disLogID=" + hdnDisLogID.Value + "&VDRNo=" + VDRNo + "&UserID=" + hdnUserID.Value + "&RoleTypeFlag=" + ddlActionPendingWithDropDown.SelectedItem.Value.Trim() + "&PreviousPage=SearchResult&WorkFlowID=" + hdnWorkFlowID.Value + "&SiteID=" + GetQueryStringValue("SiteId") + "&DisDate=" + GetQueryStringValue("DisDate"));
                    break;
                case 14:
                    hypLinkToDisDetails.NavigateUrl = EncryptQuery("LogDiscrepancy/DISLog_GenericDescrepancy.aspx?status=" + status + "&disLogID=" + hdnDisLogID.Value + "&VDRNo=" + VDRNo + "&UserID=" + hdnUserID.Value + "&RoleTypeFlag=" + ddlActionPendingWithDropDown.SelectedItem.Value.Trim() + "&PreviousPage=SearchResult&WorkFlowID=" + hdnWorkFlowID.Value + "&SiteID=" + GetQueryStringValue("SiteId") + "&DisDate=" + GetQueryStringValue("DisDate"));
                    break;
                case 15:
                    hypLinkToDisDetails.NavigateUrl = EncryptQuery("LogDiscrepancy/DISLog_Shuttle.aspx?status=" + status + "&disLogID=" + hdnDisLogID.Value + "&VDRNo=" + VDRNo + "&UserID=" + hdnUserID.Value + "&RoleTypeFlag=" + ddlActionPendingWithDropDown.SelectedItem.Value.Trim() + "&PreviousPage=SearchResult&WorkFlowID=" + hdnWorkFlowID.Value + "&SiteID=" + GetQueryStringValue("SiteId") + "&DisDate=" + GetQueryStringValue("DisDate"));
                    break;
                case 16:
                    hypLinkToDisDetails.NavigateUrl = EncryptQuery("LogDiscrepancy/DISLog_Reservation.aspx?status=" + status + "&disLogID=" + hdnDisLogID.Value + "&VDRNo=" + VDRNo + "&UserID=" + hdnUserID.Value + "&RoleTypeFlag=" + ddlActionPendingWithDropDown.SelectedItem.Value.Trim() + "&PreviousPage=SearchResult&WorkFlowID=" + hdnWorkFlowID.Value + "&SiteID=" + GetQueryStringValue("SiteId") + "&DisDate=" + GetQueryStringValue("DisDate"));
                    break;
                case 17:
                    hypLinkToDisDetails.NavigateUrl = EncryptQuery("LogDiscrepancy/DISLog_InvoiceDiscrepancy.aspx?status=" + status + "&disLogID=" + hdnDisLogID.Value + "&VDRNo=" + VDRNo + "&UserID=" + hdnUserID.Value + "&RoleTypeFlag=" + ddlActionPendingWithDropDown.SelectedItem.Value.Trim() + "&PreviousPage=SearchResult&WorkFlowID=" + hdnWorkFlowID.Value + "&SiteID=" + GetQueryStringValue("SiteId") + "&DisDate=" + GetQueryStringValue("DisDate") + "&TodayDis=" + TodaysDis);
                    break;
                case 18:
                    hypLinkToDisDetails.NavigateUrl = EncryptQuery("LogDiscrepancy/DISLog_StockPlannerInvoiceDiscrepancy.aspx?status=" + status + "&disLogID=" + hdnDisLogID.Value + "&VDRNo=" + VDRNo + "&UserID=" + hdnUserID.Value + "&RoleTypeFlag=" + ddlActionPendingWithDropDown.SelectedItem.Value.Trim() + "&PreviousPage=SearchResult&WorkFlowID=" + hdnWorkFlowID.Value + "&SiteID=" + GetQueryStringValue("SiteId") + "&DisDate=" + GetQueryStringValue("DisDate") + "&TodayDis=" + TodaysDis);
                    break;
                case 19:
                    hypLinkToDisDetails.NavigateUrl = EncryptQuery("LogDiscrepancy/DISLog_InvoiceQueryDiscrepancy.aspx?status=" + status + "&disLogID=" + hdnDisLogID.Value + "&VDRNo=" + VDRNo + "&UserID=" + hdnUserID.Value + "&RoleTypeFlag=" + ddlActionPendingWithDropDown.SelectedItem.Value.Trim() + "&PreviousPage=SearchResult&WorkFlowID=" + hdnWorkFlowID.Value + "&SiteID=" + GetQueryStringValue("SiteId") + "&DisDate=" + GetQueryStringValue("DisDate") + "&TodayDis=" + TodaysDis);
                    break;
                case 20:
                    hypLinkToDisDetails.NavigateUrl = EncryptQuery("LogDiscrepancy/DISLog_ItemNotOnPO.aspx?status=" + status + "&disLogID=" + hdnDisLogID.Value + "&VDRNo=" + VDRNo + "&UserID=" + hdnUserID.Value + "&RoleTypeFlag=" + ddlActionPendingWithDropDown.SelectedItem.Value.Trim() + "&PreviousPage=SearchResult&WorkFlowID=" + hdnWorkFlowID.Value + "&SiteID=" + GetQueryStringValue("SiteId") + "&DisDate=" + GetQueryStringValue("DisDate") + "&TodayDis=" + TodaysDis);
                    break;

            }
            e.Row.Cells[3].HorizontalAlign = HorizontalAlign.Left;
        }
    }

    public override BusinessEntities.BaseBe[] SubSort(GridViewSortEventArgs e)
    {
        return Utilities.GenericListHelper<DiscrepancyBE>.SortList((List<DiscrepancyBE>)ViewState["lstSites"], e.SortExpression, e.SortDirection).ToArray();
    }

    protected void btnReturntoSearchCriteria_Click(object sender, EventArgs e)
    {
        mvDiscrepancyList.ActiveViewIndex = 0;
        btnSearch.Visible = true;
        btnReturntoSearchCriteria.Visible = false;
        Label lblModuleText = (Label)Page.Master.FindControl("lblModuleText");
        lblModuleText.Text = WebCommon.getGlobalResourceValue("DiscrepanciesSearchResult");
        lblSearchResultT.Text = WebCommon.getGlobalResourceValue("SearchResult");

        HiddenField hdnSelectedVendorId = (HiddenField)msVendor.FindControl("hiddenSelectedIDs");
        hdnSelectedVendorId.Value = string.Empty;
        hiddenSelectedIDs.Value = string.Empty;
        hiddenSelectedName.Value = string.Empty;
        Session["SearchCriteria"] = null;
    }
    #endregion

}
﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="DIS_UnClosedDiscrepancyList.aspx.cs"  MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
 Inherits="ModuleUI_Discrepancy_DIS_UnclosedDiscrepancyList" %>
 <%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>

 <asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <script type="text/javascript">
         $(document).ready(function () {
             $('[id$=chkSelectAllText]').click(function () {
                 $("[id$='chkSelect']").attr('checked', this.checked);                
             });
             $("[id$='chkSelect']").click(function () {
                 if ($('[id$=chkSelect]:checked').length == $('[id$=chkSelect]').length) {
                     $("[id$=chkSelectAllText]").attr('checked', true);
                 }
                 if (!$(this).is(":checked")) {
                     $("[id$=chkSelectAllText]").attr('checked', false);
                 }
             });
         });                
       
        </script>
<asp:ScriptManager ID="sm1" runat="server">
    </asp:ScriptManager>

    <asp:HiddenField ID="hdnFldSelectedValues" runat="server" />
      <div class="button-row">
         <cc1:ucButton ID="btnExportToExcel" runat="server" CssClass="exporttoexcel button"
                                OnClick="btnExport_Click" Width="109px" Height="20px" />
        <%-- <cc2:ucExportToExcel ID="ucExportToExcel1" runat="server" />--%>
    </div>

     <div class="right-shadow">
        <div class="formbox">
      <cc1:ucPanel ID="pnlPotentialOutput"  runat="server">
            
      <table width="100%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
               <tr>
                    <td colspan="3">
                        <cc1:ucgridview ID="gdvDiscrepancy"  Width="100%" runat="server" CssClass="grid"
                           AllowSorting="true" OnSorting="SortGrid">
                            <Columns>
                             <asp:TemplateField HeaderText="SelectAllCheckBox">
                                    <HeaderStyle HorizontalAlign="Left" Width="100px" />
                                    <ItemStyle Width="100px" HorizontalAlign="Left" VerticalAlign="Middle" />
                                    <HeaderTemplate>
                                        <cc1:ucCheckbox runat="server" ID="chkSelectAllText" CssClass="checkbox-input"/>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:CheckBox runat="server" ViewStateMode="Enabled" ID="chkSelect"   />
                                        <asp:HiddenField ID="hdndiscrepancyLogid" runat="server" Value='<%# Eval("DiscrepancyLogID") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                               <%-- <asp:TemplateField HeaderText="DiscrepancyLogID" SortExpression="DiscrepancyLogID">
                                    <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <cc1:ucLabel ID="lbldiscrepancyLogIDs" runat="server" Text='<%# Eval("DiscrepancyLogID") %>'></cc1:ucLabel>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" />
                                </asp:TemplateField>--%>
                                <asp:TemplateField HeaderText="Discrepancy" SortExpression="VDRNo">
                                    <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <cc1:ucLabel ID="lblDiscNo" runat="server" Text='<%# Eval("VDRNo") %>'></cc1:ucLabel>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                                
                                <asp:TemplateField HeaderText="DiscrepancyLogDate" SortExpression="DiscrepancyLogDate">
                                    <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <cc1:ucLabel ID="lblDiscrepancyLoggedDate" runat="server" Text='<%# Eval("DiscrepancyLogDate", "{0:dd/MM/yyyy}") %>'></cc1:ucLabel>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                                </Columns>

                        </cc1:ucgridview>
                     </td>
                </tr>
                 <tr>
                         
                        <td align="right">
                            <cc1:ucButton ID="btnUpdateUnclosedDiscrepancy" runat="server"  OnClick="UpdateUnclosedDis_Click"  Text="Update Unclosed Discrepancy Status" CssClass="button" OnClientClick="return CheckSelect();" />
                              </td>
                    </tr>
        </table>

        </cc1:ucPanel>
     </div>
     </div>
    
 </asp:Content>
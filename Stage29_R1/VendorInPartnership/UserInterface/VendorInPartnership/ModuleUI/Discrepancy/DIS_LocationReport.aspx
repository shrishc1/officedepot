﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master" AutoEventWireup="true" CodeFile="DIS_LocationReport.aspx.cs" Inherits="DIS_LocationReport" %>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="../../CommonUI/UserControls/ucSite.ascx" TagName="ucSite" TagPrefix="cc2" %>
<%@ Register TagName="ucExportButton" TagPrefix="uc1" Src="~/CommonUI/UserControls/ucExportToExcel.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <script type="text/javascript">
        $(document).ready(function () {
            $("body").keypress(function (e) {
                if (e.which == 13 || e.keyCode == 13) {
                    $('#<%=btnSearch.ClientID %>').click();
                    return false;
                }
            });
        });
    </script>
     <h2>
        <cc1:ucLabel ID="lblLocationReport" Text="Discrepancy Location Report" runat="server"></cc1:ucLabel>
    </h2>
     <div class="right-shadow">
       <%-- <div class="formbox">--%>
            <cc1:ucPanel ID="pnlSearchScreen" runat="server">
              <div class="button-row">
                 <table width="100%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                     <tr>
                         <td width="25%"></td>
                         <td style="font-weight: bold; width: 5%" align="center">
                             <cc1:ucLabel ID="lblSitePrefixName" runat="server" Text="Site"></cc1:ucLabel>
                         </td>
                         <td width="2%">:</td>
                         <td width="20%" align="left">
                              <cc2:ucSite ID="ucSite" runat="server" />
                         </td>
                         <td align="left">
                              <cc1:ucButton ID="btnSearch" runat="server" OnClick="btnSearch_Click" Text="Search" CssClass="button"  />
                             &nbsp;&nbsp; <uc1:ucExportButton ID="btnExportToExcel1" runat="server" />
                         </td>
                     </tr>
                     </table>
                  </div>
             </cc1:ucPanel>
         
                 <table width="100%" cellspacing="5" cellpadding="0" border="0" align="center" >
                     <tr>
                         <td>
                             <cc1:ucGridView ID="gdvLocationReport"  Width="100%" runat="server" CssClass="grid"
                            AllowPaging="true" OnPageIndexChanging="gdvLocationReport_PageIndexChanging" OnRowDataBound="gdvLocationReport_RowDataBound"  PageSize="50" EmptyDataText="No Record Found" EmptyDataRowStyle-HorizontalAlign="Center" >
                            <Columns>
                               
                                <asp:TemplateField HeaderText="Discrepancy" >
                                    <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <asp:HiddenField
                                            ID="hdnDisType" runat="server" Value='<%#Eval("DiscrepancyTypeID") %>' />
                                        <asp:HyperLink ID="hypLinkToDisDetails" runat="server" Text='<%#Eval("VDRNo") %>' />
                                        <asp:HiddenField
                                            ID="hdnDiscrepancylogId" runat="server" Value='<%#Eval("DiscrepancyLogID") %>' />
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Type" >
                                    <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <cc1:ucLabel ID="lbldisType" runat="server" Text='<%# Eval("DiscrepancyType") %>'></cc1:ucLabel>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                                 <asp:TemplateField HeaderText="Location">
                                    <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <cc1:ucLiteral ID="ltLocation" Text='<%# Eval("Location") %>'
                                            runat="server" />
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Delivery Note" >
                                    <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <cc1:ucLabel ID="lblDeliNote" runat="server" Text='<%# Eval("DeliveryNoteNumber") %>'></cc1:ucLabel>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                                 <asp:TemplateField HeaderText="PO" >
                                    <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <cc1:ucLabel ID="lblPOno" runat="server" Text='<%# Eval("PurchaseOrderNumber") %>'></cc1:ucLabel>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Vendor" >
                                    <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <cc1:ucLabel ID="lblVendorNoValue" runat="server" Text='<%# Eval("VendorNoName") %>'></cc1:ucLabel>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                                  <asp:TemplateField HeaderText="Stock Planner" >
                                    <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <cc1:ucLabel ID="lblSP" runat="server" Text='<%# Eval("StockPlannerName") %>'></cc1:ucLabel>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                                 <asp:TemplateField HeaderText="Created By" >
                                    <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <cc1:ucLabel ID="lblCBy" runat="server" Text='<%# Eval("CreatedBy") %>'></cc1:ucLabel>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                                 <asp:TemplateField HeaderText="Created On" >
                                    <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <cc1:ucLabel ID="lblCOn" runat="server" Text='<%# Eval("LogDate") %>'></cc1:ucLabel>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                                 <asp:TemplateField HeaderText="Updated By" >
                                    <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <cc1:ucLabel ID="lblUBy" runat="server" Text='<%# Eval("UpdatedBy") %>'></cc1:ucLabel>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                            </Columns>
                        </cc1:ucGridView>
                         </td>
                     </tr>
                     <tr>
                         <td>
                              <cc1:ucGridView ID="gdvExport"  Width="100%" runat="server" CssClass="grid"
                             EmptyDataText="No Record Found" EmptyDataRowStyle-HorizontalAlign="Center" Visible="false" >
                            <Columns>
                                <asp:TemplateField HeaderText="Discrepancy" >
                                    <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <cc1:ucLabel ID="lblSn" runat="server" Text=''></cc1:ucLabel>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Discrepancy" >
                                    <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <cc1:ucLabel ID="lbldis" runat="server" Text='<%# Eval("VDRNo") %>'></cc1:ucLabel>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Type" >
                                    <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <cc1:ucLabel ID="lbldisType" runat="server" Text='<%# Eval("DiscrepancyType") %>'></cc1:ucLabel>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                                 <asp:TemplateField HeaderText="Location">
                                    <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <cc1:ucLiteral ID="ltLocation" Text='<%# Eval("Location") %>'
                                            runat="server" />
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Delivery Note" >
                                    <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <cc1:ucLabel ID="lblDeliNote" runat="server" Text='<%# Eval("DeliveryNoteNumber") %>'></cc1:ucLabel>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                                 <asp:TemplateField HeaderText="PO" >
                                    <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <cc1:ucLabel ID="lblPOno" runat="server" Text='<%# Eval("PurchaseOrderNumber") %>'></cc1:ucLabel>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Vendor" >
                                    <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <cc1:ucLabel ID="lblVendorNoValue" runat="server" Text='<%# Eval("VendorNoName") %>'></cc1:ucLabel>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                                  <asp:TemplateField HeaderText="Stock Planner" >
                                    <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <cc1:ucLabel ID="lblSP" runat="server" Text='<%# Eval("StockPlannerName") %>'></cc1:ucLabel>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                                 <asp:TemplateField HeaderText="Created By" >
                                    <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <cc1:ucLabel ID="lblCBy" runat="server" Text='<%# Eval("CreatedBy") %>'></cc1:ucLabel>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                                 <asp:TemplateField HeaderText="Created On" >
                                    <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <cc1:ucLabel ID="lblCOn" runat="server" Text='<%# Eval("LogDate") %>'></cc1:ucLabel>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                                 <asp:TemplateField HeaderText="Updated By" >
                                    <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <cc1:ucLabel ID="lblUBy" runat="server" Text='<%# Eval("UpdatedBy") %>'></cc1:ucLabel>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                            </Columns>
                        </cc1:ucGridView>
                         </td>
                     </tr>
                  </table>
       
         <%-- </div>--%>
         </div>
</asp:Content>


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLogicLayer.ModuleBAL.Discrepancy;
using BusinessEntities.ModuleBE.Discrepancy;
using BusinessEntities.ModuleBE.Security;

public partial class ModuleUI_Discrepancy_StockPlannerOnHoliday : CommonPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindStockPlannerHolidays();
        }
        ucExportToExcel.GridViewControl = grdStockPlannerHoliday;
        ucExportToExcel.FileName = "StockPlannerHoliday";
    }

    private void BindStockPlannerHolidays()
    {
        StockPlannerHolidayCoverBE oStockPlannerHolidayCoverBE = new StockPlannerHolidayCoverBE();
        StockPlannerHolidayCoverBAL oStockPlannerHolidayBAL = new StockPlannerHolidayCoverBAL();
        SCT_UserBE oSCT_UserBE = new SCT_UserBE();
        oStockPlannerHolidayCoverBE.Action = "ShowAll";
        var userId = Convert.ToInt32(Session["UserID"]);

        oSCT_UserBE.UserID = userId;
        oStockPlannerHolidayCoverBE.SCT_UserBE = oSCT_UserBE;
        List<StockPlannerHolidayCoverBE> lstStockPlannerHolidayBE = oStockPlannerHolidayBAL.GetStockPlannerHolidaysBAL(oStockPlannerHolidayCoverBE);
        if (lstStockPlannerHolidayBE != null && lstStockPlannerHolidayBE.Count > 0)
        {
            grdStockPlannerHoliday.DataSource = lstStockPlannerHolidayBE;
            grdStockPlannerHoliday.DataBind();
            ViewState["lstStockPlannerHolidayBE"] = lstStockPlannerHolidayBE;
        }
    }

    public override BusinessEntities.BaseBe[] SubSort(GridViewSortEventArgs e)
    {
        return Utilities.GenericListHelper<MediatorBE>.SortList((List<MediatorBE>)ViewState["lstStockPlannerHolidayBE"], e.SortExpression, e.SortDirection).ToArray();
    }
}
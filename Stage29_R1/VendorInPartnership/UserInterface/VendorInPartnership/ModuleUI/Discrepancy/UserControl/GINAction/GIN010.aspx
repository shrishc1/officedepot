﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="GIN010.aspx.cs" Inherits="GINAction_GIN010" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Office Depot</title>
    <link rel="stylesheet" type="text/css" href="../../../../Css/style.css" />
    <link type="text/css" href="../../../../Css/ui-lightness/jquery-ui-1.8.5.custom.css"
        rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="../../../../Css/superfish.css" media="screen" />
    <script type="text/javascript" src="<%= ResolveClientUrl("../../../../Scripts/jquery-1.2.6.min.js")%>"></script>
    <script type="text/javascript" src="<%= ResolveClientUrl("../../../../Scripts/jquery-1.4.2.min.js")%>"></script>
    <script type="text/javascript" src="<%= ResolveClientUrl("../../../../Scripts/jquery-ui-1.8.5.custom.min.js")%>"></script>
    <script type="text/javascript" src="<%= ResolveClientUrl("../../../../Scripts/hoverIntent.js")%>"></script>
    <script type="text/javascript" src="<%= ResolveClientUrl("../../../../Scripts/superfish.js")%>"></script>
    <script type="text/javascript" src="<%= ResolveClientUrl("../../../../Scripts/ValidateInputs.js")%>"></script>
    <script type="text/javascript">
        function StageCompleted() {
            var objLabourCostRequired = document.getElementById('<%=rfvLabourCostRequired.ClientID %>')
           
            var objLabourCostRE = document.getElementById('<%=revLabourCostRE.ClientID %>')
            if (document.getElementById('<%=hdnValue.ClientID %>').value == '1') {

                document.getElementById('<%=hdnValue.ClientID %>').value = '0';
               ValidatorEnable(objLabourCostRequired, false);
                ValidatorEnable(objLabourCostGreaterThanZero, false);

            }
            else {
                document.getElementById('<%=hdnValue.ClientID %>').value = '1';
                ValidatorEnable(objLabourCostRequired, true);
                ValidatorEnable(objLabourCostGreaterThanZero, true);

            }
        }
        function GetLocation() {
            var Location = parent.document.getElementById('ctl00_ContentPlaceHolder1_txtLocation').value;
            document.getElementById('<%=hdfLocation.ClientID%>').value = Location;
            if (checkValidationGroup("a")) {
                document.getElementById('<%=btnSave.ClientID%>').style.visibility = "hidden";
            }
            else {
                document.getElementById('<%=btnSave.ClientID%>').style.visibility = "visible";
                return false;
            }
        }


        function AssignCostCenterDebitAndCredit() {
            var CostCenter = parent.document.getElementById('ctl00_ContentPlaceHolder1_hdnCostCenter');
            document.getElementById('<%=txtCostCenter.ClientID%>').value = CostCenter.value; // Credit section
        }

    </script>
</head>
<body onload="AssignCostCenterDebitAndCredit()">
    <form id="form2" runat="server" method="post">
    <cc1:ucPanel ID="pnlActionRequired" runat="server" Width="100%" GroupingText="Action Required"
        CssClass="fieldset-form" BorderColor="gray" BorderStyle="Solid" BorderWidth="0">
        <table class="form-table" cellpadding="0" cellspacing="0" width="98%" style="margin: 2px;">
            <tr>
                <td style="font-weight: bold;"><asp:HiddenField ID="hdfLocation" runat="server" />
                    <cc1:ucLabel ID="lblGIN010Desc" runat="server" CssClass="action-required-heading"
                        Style="text-align: center;" Text="Please confirm the goods are booked in and rework to the correct pack size against Purchase Order. Add cost of this additional work."></cc1:ucLabel>
                </td>
            </tr>
            <tr>
                <td>
                    <table class="form-table" cellpadding="0" cellspacing="0" width="98%" style="margin: 2px;">
                        <tr>
                            <td width="11%">
                                <cc1:ucLabel ID="lblCostofLabour" runat="server" Text="Cost of Labour"></cc1:ucLabel>
                                <%-- <cc1:ucLabel ID="lblCollectionDate" runat="server" Text="Arranged Collection Date"></cc1:ucLabel>--%>
                            </td>
                            <td width="1%">
                                :
                            </td>
                            <td width="88%">
                                <cc1:ucTextbox ID="txtGIN010CostofLabour" runat="server" onkeyup="AllowDecimalOnly(this);"></cc1:ucTextbox>
                                <asp:RequiredFieldValidator ID="rfvLabourCostRequired" runat="server" Display="None"
                                    ControlToValidate="txtGIN010CostofLabour" ValidationGroup="a"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="revLabourCostRE" ValidationExpression="^\d{0,8}(\.\d{0,2})?$"
                                    runat="server" ControlToValidate="txtGIN010CostofLabour" Display="None" ValidationGroup="a"></asp:RegularExpressionValidator>
                               <%-- <asp:CompareValidator ID="cmpvLabourCostGreaterThanZero" runat="server" ControlToValidate="txtGIN010CostofLabour"
                                    Operator="GreaterThan" ValueToCompare="0" Type="Double" Display="None" ErrorMessage="Labour cost must be greater than zero."
                                    ValidationGroup="a">
                                </asp:CompareValidator>--%>
                            </td>
                        </tr>
                         <tr>
                            <td>
                                <cc1:ucLabel ID="lblCostCenter" runat="server" Text="Cost Centre"></cc1:ucLabel>                               
                            </td>
                            <td width="1%">
                                :
                            </td>
                            <td>
                                <cc1:ucTextbox ID="txtCostCenter" runat="server"></cc1:ucTextbox>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table class="form-table" cellpadding="0" cellspacing="0" width="98%" style="margin: 2px;">
                        <tr>
                            <td width="7%">
                                <cc1:ucLabel ID="lblCommentLabel" runat="server" Text="Comment"></cc1:ucLabel>
                            </td>
                            <td width="93%">
                                :
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <cc1:ucTextbox ID="txtComments" runat="server" onkeyup="checkTextLengthOnKeyUp(this,1000);"
                                    TextMode="MultiLine" CssClass="textarea" Width="98%"></cc1:ucTextbox>
                                <asp:RequiredFieldValidator ID="rfvCommentRequired" runat="server" Text="Please enter comments."
                                    ControlToValidate="txtComments" Display="None" ValidationGroup="a">
                                </asp:RequiredFieldValidator>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table class="form-table" cellpadding="0" cellspacing="0" width="98%" style="margin: 2px;">
                        <tr>
                            <td width="14%" style="display:none;">
                                <cc1:ucLabel ID="lblStageCompleted" runat="server" Text="Stage Completed"></cc1:ucLabel>
                                <asp:ValidationSummary ID="vs1" runat="server" ValidationGroup="a" ShowSummary="false"
                                    ShowMessageBox="true" />
                            </td>
                            <td width="1%" style="display:none;">
                                :
                            </td>
                            <td width="75%" align="left" style="display:none;">
                                <cc1:ucCheckbox ID="chkGIN010StageCompleted" runat="server" Checked="true" />
                                <input type="hidden" runat="server" id="hdnValue" value="1" />
                            </td>
                            <td width="10%" style="display:none;">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" style="display:none;">
                                <cc1:ucLabel ID="lblDiscrepancyNote" runat="server" Text="If  you required to tag a comment without closing the discrepancy<br/>1 -    Deselect Stage Complete<br/>2 -   Enter  the comment<br/>3 -   Save"></cc1:ucLabel>
                            </td>
                            <td align="right" valign="bottom">
                                <cc1:ucButton ID="btnSave" runat="server" Text="Save" class="button" ValidationGroup="a"
                                    OnClick="btnSave_Click" OnClientClick="javascript:GetLocation()" />
                                <cc1:ucButton ID="btnBack" runat="server" Text="Back" class="button" OnClick="btnBack_Click" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </cc1:ucPanel>
    </form>
</body>
</html>

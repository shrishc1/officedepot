﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="GIN014.aspx.cs" Inherits="GINAction_GIN014" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Office Depot</title>
    <link rel="stylesheet" type="text/css" href="../../../../Css/style.css" />
    <link type="text/css" href="../../../../Css/ui-lightness/jquery-ui-1.8.5.custom.css"
        rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="../../../../Css/superfish.css" media="screen" />
    <script type="text/javascript" src="<%= ResolveClientUrl("../../../../Scripts/jquery-1.2.6.min.js")%>"></script>
    <script type="text/javascript" src="<%= ResolveClientUrl("../../../../Scripts/jquery-1.4.2.min.js")%>"></script>
    <script type="text/javascript" src="<%= ResolveClientUrl("../../../../Scripts/jquery-ui-1.8.5.custom.min.js")%>"></script>
    <script type="text/javascript" src="<%= ResolveClientUrl("../../../../Scripts/hoverIntent.js")%>"></script>
    <script type="text/javascript" src="<%= ResolveClientUrl("../../../../Scripts/superfish.js")%>"></script>
    <script type="text/javascript" src="<%= ResolveClientUrl("../../../../Scripts/ValidateInputs.js")%>"></script>
    <script type="text/javascript">


        $(document).ready(function () {

            $("#trOption").hide();  //hide option table including comments

            var rdoValidDispute = '<%=rdoDisputeValid_GIN014a.ClientID %>';
            var rdoValidDispute2 = '<%=rdoDisputeValid_GIN014b.ClientID %>';
            var rdpDisputeNotValid = '<%=rdoDisputeNotValid_GIN014a.ClientID %>';
            var rdpDisputeNotValid2 = '<%=rdoDisputeNotValid_GIN014b.ClientID %>';
            var rdoBookedInAgainstOriginalPO = '<%=rdoBookedInAgainstOriginalPO.ClientID %>';
            var rdoBookedInAgainstNax = '<%=rdoBookedInAgainstNax.ClientID %>';
            var rdoStockAdjustment = '<%=rdoStockAdjustment.ClientID %>';
            var rdoInternalError = '<%=rdoInternalError.ClientID %>';
            var ReqNaxPoNo = document.getElementById('<%=rfvRequiredNaxPONo.ClientID %>');
            var ReqDateofAdjustment = document.getElementById('<%=rfvRequiredDateAdjustment.ClientID %>');
            var ReqAdjustedQuantity = document.getElementById('<%=rfvRequiredAdjustedQuantity.ClientID %>');
            var Comments = document.getElementById('<%=rfvCommentRequired_ValidDispute.ClientID %>');

            ValidatorEnable(ReqNaxPoNo, false);
            ValidatorEnable(ReqDateofAdjustment, false);
            ValidatorEnable(ReqAdjustedQuantity, false);
            ValidatorEnable(Comments, false);

            //Panel 1 valid Radio button
            $("#" + rdoValidDispute).change(function () {
                $("#trOption").show();
                $("#trNAX").hide();
                $("#trAdjustment").hide();
                $("#trCommentLable").hide();
                $("#trCommentText").hide();
                ValidatorEnable(ReqNaxPoNo, false);
                ValidatorEnable(ReqDateofAdjustment, false);
                ValidatorEnable(ReqAdjustedQuantity, false);
                ValidatorEnable(Comments, false);

            });
            // Panel 2 valid Radio button
            $("#" + rdoValidDispute2).change(function () {
                $("#trOption").show();
                $("#trNAX").hide();
                $("#trAdjustment").hide();
                $("#trCommentLable").hide();
                $("#trCommentText").hide();
                ValidatorEnable(ReqNaxPoNo, false);
                ValidatorEnable(ReqDateofAdjustment, false);
                ValidatorEnable(ReqAdjustedQuantity, false);
                ValidatorEnable(Comments, false);
                Uncheck();
            });

            //Panel 1 Not valid Radio button
            $("#" + rdpDisputeNotValid).change(function () {
                $("#trOption").hide();
                $("#trNAX").hide();
                $("#trAdjustment").hide();
                $("#trCommentLable").hide();
                $("#trCommentText").hide();
                ValidatorEnable(ReqNaxPoNo, false);
                ValidatorEnable(ReqDateofAdjustment, false);
                ValidatorEnable(ReqAdjustedQuantity, false);
                ValidatorEnable(Comments, false);

            });



            $("#" + rdoBookedInAgainstOriginalPO).change(function () {
                $("#trNAX").hide();
                $("#trAdjustment").hide();
                $("#trCommentLable").show();
                $("#trCommentText").show();
                ValidatorEnable(ReqNaxPoNo, false);
                ValidatorEnable(ReqDateofAdjustment, false);
                ValidatorEnable(ReqAdjustedQuantity, false);
                ValidatorEnable(Comments, true);

            });

            $("#" + rdoBookedInAgainstNax).change(function () {
                $("#trNAX").show();
                $("#trAdjustment").hide();
                $("#trCommentLable").show();
                $("#trCommentText").show();
                ValidatorEnable(ReqNaxPoNo, true);
                ValidatorEnable(ReqDateofAdjustment, false);
                ValidatorEnable(ReqAdjustedQuantity, false);
                ValidatorEnable(Comments, true);
            });

            $("#" + rdoStockAdjustment).change(function () {
                $("#trNAX").hide();
                $("#trAdjustment").show();
                $("#trCommentLable").show();
                $("#trCommentText").show();
                ValidatorEnable(ReqNaxPoNo, false);
                ValidatorEnable(ReqDateofAdjustment, true);
                ValidatorEnable(ReqAdjustedQuantity, true);
                ValidatorEnable(Comments, true);
            });

            $("#" + rdoInternalError).change(function () {
                $("#trNAX").hide();
                $("#trAdjustment").hide();
                $("#trCommentLable").show();
                $("#trCommentText").show();
                ValidatorEnable(ReqNaxPoNo, false);
                ValidatorEnable(ReqDateofAdjustment, false);
                ValidatorEnable(ReqAdjustedQuantity, false);
                ValidatorEnable(Comments, true);
            });
        });


        function Uncheck() {
            var rdoBookedInAgainstOriginalPO = '<%=rdoBookedInAgainstOriginalPO.ClientID %>';
            var rdoBookedInAgainstNax = '<%=rdoBookedInAgainstNax.ClientID %>';
            var rdoStockAdjustment = '<%=rdoStockAdjustment.ClientID %>';
            var rdoInternalError = '<%=rdoInternalError.ClientID %>';

            $("#" + rdoBookedInAgainstOriginalPO).attr('checked', false);
            $("#" + rdoBookedInAgainstNax).attr('checked', false);
            $("#" + rdoStockAdjustment).attr('checked', false);
            $("#" + rdoInternalError).attr('checked', false);
        }

        function GIN014ActionRequired(sender, args) {
            var objrdo1 = document.getElementById('<%=rdoDisputeValid_GIN014a.ClientID %>');
            var objrdo2 = document.getElementById('<%=rdoDisputeNotValid_GIN014a.ClientID %>');
            var objrdo3 = document.getElementById('<%=rdoDisputeValid_GIN014b.ClientID %>');
            var objrdo4 = document.getElementById('<%=rdoDisputeNotValid_GIN014b.ClientID %>');
            if (objrdo1.checked == false && objrdo2.checked == false && objrdo3.checked == false && objrdo4.checked == false) {
                args.IsValid = false;
            }
        }

        function GIN014SelectReason(sender, args) {
            var objrdo1 = document.getElementById('<%=rdpPODReason1.ClientID %>');
            var objrdo2 = document.getElementById('<%=rdpPODReason2.ClientID %>');
            var objrdo3 = document.getElementById('<%=rdpPODReason3.ClientID %>');
            var objrdo4 = document.getElementById('<%=rdpPODReason4.ClientID %>');
            var objrdo5 = document.getElementById('<%=rdoOther_2.ClientID %>');
            if (objrdo1.checked == false && objrdo2.checked == false && objrdo3.checked == false && objrdo4.checked == false && objrdo5.checked == false) {
                args.IsValid = false;
            }
        }

        function GIN014EnterComments(sender, args) {
            var objrdo1 = document.getElementById('<%=txtNotValidDisputeComment.ClientID %>');
          
            if (objrdo1.value == '') {
                args.IsValid = false;
            }
        }

        function DisputeOptionReqired(sender, args) {
            var objrdoValidDispute = document.getElementById('<%=rdoDisputeValid_GIN014a.ClientID %>');
            var objrdo1 = document.getElementById('<%=rdoBookedInAgainstOriginalPO.ClientID %>');
            var objrdo2 = document.getElementById('<%=rdoBookedInAgainstNax.ClientID %>');
            var objrdo3 = document.getElementById('<%=rdoStockAdjustment.ClientID %>');
            var objrdo4 = document.getElementById('<%=rdoInternalError.ClientID %>');
            if (objrdoValidDispute.checked == true) {
                if (objrdo1.checked == false && objrdo2.checked == false && objrdo3.checked == false && objrdo4.checked == false) {
                    args.IsValid = false;
                }
            }
        }

        function TogglePanelControl() {
          
            //Panel Controls
            var pnlValidDispute = document.getElementById('<%=pnlGIN014a.ClientID %>');
            var pnlNotValidDispute = document.getElementById('<%=pnlGIN014b.ClientID %>');
            var ReqDiscrepancyNo = document.getElementById('<%=rfvRequiredDiscrepancyNo.ClientID %>');
            //Valid Dispute Controls
            var objrdoValidValid = document.getElementById('<%=rdoDisputeValid_GIN014a.ClientID %>');
            var objrdoValidNotValid = document.getElementById('<%=rdoDisputeNotValid_GIN014a.ClientID %>');
            //Not Valid Dispute Controls
            var objrdoNotValidValid = document.getElementById('<%=rdoDisputeValid_GIN014b.ClientID %>');
            var objrdoNotValidNotValid = document.getElementById('<%=rdoDisputeNotValid_GIN014b.ClientID %>');


            if (objrdoValidValid.checked) {
              
                pnlValidDispute.style.display = "block";
                pnlNotValidDispute.style.display = "none";
                objrdoNotValidValid.checked = true;
                ValidatorEnable(ReqDiscrepancyNo, false);
            }
            else if (objrdoValidNotValid.checked) {

                pnlValidDispute.style.display = "none";
                pnlNotValidDispute.style.display = "block";
                objrdoNotValidNotValid.checked = true;
                ValidatorEnable(ReqDiscrepancyNo, true);
            }

            if (objrdoNotValidValid.checked) {

                pnlValidDispute.style.display = "block";
                pnlNotValidDispute.style.display = "none";
                objrdoValidValid.checked = true;
                ValidatorEnable(ReqDiscrepancyNo, false);

            }
            else if (objrdoNotValidNotValid.checked) {

                pnlValidDispute.style.display = "none";
                pnlNotValidDispute.style.display = "block";
                objrdoNotValidNotValid.checked = true;
                ValidatorEnable(ReqDiscrepancyNo, true);

            }
        }

        function StageCompleted() {

        }

        function ToggleDiscrepancyNo() {
            var tdDiscrepancyNo = document.getElementById('tdDiscrepncyNo');
            var objrdoDiscrepancy = document.getElementById('<%=rdpPODReason4.ClientID %>');
            var ReqDiscrepancyNo = document.getElementById('<%=rfvRequiredDiscrepancyNo.ClientID %>');

            if (objrdoDiscrepancy.checked) {
                tdDiscrepancyNo.style.display = "block";
                ValidatorEnable(ReqDiscrepancyNo, true);
            }
            else {
                tdDiscrepancyNo.style.display = "none";
                ValidatorEnable(ReqDiscrepancyNo, false);
            }
        }

    </script>
    <script type="text/javascript">
     function HideShowButton(){
          if (checkValidationGroup("ValidDispute")) {
                document.getElementById('<%=btnValidDisputeSave.ClientID%>').style.visibility = "hidden";
            }
            else {
                document.getElementById('<%=btnValidDisputeSave.ClientID%>').style.visibility = "visible";
                return false;
            }
        }
    </script>
    <script type="text/javascript" language="javascript">
        $(function () {
            $('#txtDateAdjustment').datepicker({ showOtherMonths: true,
                selectOtherMonths: true, changeMonth: true, changeYear: true,
                minDate: new Date(2013, 0, 1),
                yearRange: '2013:+100',
                dateFormat: 'dd/mm/yy'
            });
        });
    </script>
</head>
<body>

    <form id="form2" runat="server" method="post">
    <asp:HiddenField ID="hdnResolution" runat="server" />
    <cc1:ucPanel ID="pnlGIN014a" runat="server" Width="100%" CssClass="fieldset-form"
        GroupingText="Action Required">
        <table class="form-table" cellpadding="5" cellspacing="0" width="96%">
            <tr>
                <td style="font-weight: bold;">
                    <cc1:ucLabel ID="lblGIIN014_1" runat="server" CssClass="action-required-heading" Style="text-align: center;"></cc1:ucLabel>
                </td>
            </tr>
            <tr>
                <td>
                    <table class="form-table" cellpadding="0" cellspacing="0" width="98%">
                        <tr>
                            <td>
                                <table class="form-table" cellpadding="0" cellspacing="0" width="98%" style="margin: 2px;">
                                    <tr>
                                        <td width="30%">
                                            <cc1:ucRadioButton ID="rdoDisputeValid_GIN014a" runat="server" GroupName="DisputeName"
                                                OnClick="TogglePanelControl()" />
                                        </td>
                                        <td width="70%">
                                            <cc1:ucRadioButton ID="rdoDisputeNotValid_GIN014a" runat="server" GroupName="DisputeName"
                                                OnClick="TogglePanelControl()" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                       
                        <tr id="trresolution" runat="server">
                            <td>
                                <table class="form-table" cellpadding="0" cellspacing="0" width="98%" style="margin: 2px;">
                                    <tr>
                                        <td colspan="2">
                                            <cc1:ucLabel ID="lblSelectResolution" runat="server"></cc1:ucLabel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="30%">
                                            <cc1:ucRadioButton ID="rdoGoodsInError" runat="server" GroupName="ReasonError" />
                                        </td>
                                        <td width="70%">
                                            <cc1:ucRadioButton ID="rdoOther" runat="server" GroupName="ReasonError" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr id="trOption">
                            <td>
                                <table class="form-table" cellpadding="0" cellspacing="0" width="98%" style="margin: 2px;">
                                    <tr>
                                        <td>
                                            <cc1:ucLabel ID="lblSelectOption" runat="server"></cc1:ucLabel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="25%">
                                            <cc1:ucRadioButton ID="rdoBookedInAgainstOriginalPO" runat="server" GroupName="DisputeReason" />
                                        </td>
                                        <td width="30%">
                                            <cc1:ucRadioButton ID="rdoBookedInAgainstNax" runat="server" GroupName="DisputeReason" />
                                        </td>
                                        <td width="20%">
                                            <cc1:ucRadioButton ID="rdoStockAdjustment" runat="server" GroupName="DisputeReason" />
                                        </td>
                                        <td width="25%">
                                            <cc1:ucRadioButton ID="rdoInternalError" runat="server" GroupName="DisputeReason" />
                                        </td>
                                    </tr>
                                    <tr id="trNAX">
                                        <td>
                                            <cc1:ucLabel ID="lblNAXAlternatePONo" runat="server"></cc1:ucLabel>
                                        </td>
                                        <td colspan="3">
                                            <cc1:ucTextbox ID="txtNaxPO" runat="server" MaxLength="10"></cc1:ucTextbox>
                                            <asp:RequiredFieldValidator ID="rfvRequiredNaxPONo" runat="server" ControlToValidate="txtNaxPO"
                                                Display="None" ValidationGroup="ValidDispute"></asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr id="trAdjustment">
                                        <td>
                                            <cc1:ucLabel ID="lblDateOfAdjustment" runat="server"></cc1:ucLabel>
                                        </td>
                                        <td>
                                            <cc1:ucTextbox ID="txtDateAdjustment" ClientIDMode="Static" runat="server" CssClass="date"
                                                Width="70px" />
                                            <asp:RequiredFieldValidator ID="rfvRequiredDateAdjustment" runat="server" ControlToValidate="txtDateAdjustment"
                                                Display="None" ValidationGroup="ValidDispute"></asp:RequiredFieldValidator>
                                        </td>
                                        <td>
                                            <cc1:ucLabel ID="lblAdjustedQuantity" runat="server"></cc1:ucLabel>
                                        </td>
                                        <td>
                                            <cc1:ucTextbox ID="txtAdjustedQuantity" runat="server" MaxLength="10" Width="80px"
                                                onkeyup="AllowNumbersOnly(this);"></cc1:ucTextbox>
                                            <asp:RequiredFieldValidator ID="rfvRequiredAdjustedQuantity" runat="server" ControlToValidate="txtAdjustedQuantity"
                                                Display="None" ValidationGroup="ValidDispute"></asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr id="trCommentLable">
                                        <td colspan="4">
                                            <cc1:ucLabel ID="lblComment_1" runat="server"></cc1:ucLabel>
                                        </td>
                                    </tr>
                                    <tr id="trCommentText">
                                        <td colspan="4">
                                            <cc1:ucTextbox ID="txtValidDisputeComment" runat="server" onkeyup="checkTextLengthOnKeyUp(this,1000);"
                                                TextMode="MultiLine" CssClass="textarea" Width="98%"></cc1:ucTextbox>
                                            <asp:RequiredFieldValidator ID="rfvCommentRequired_ValidDispute" runat="server" Text="Please enter comments."
                                                ControlToValidate="txtValidDisputeComment" Display="None" ValidationGroup="ValidDispute"></asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table cellpadding="0" cellspacing="0" class="form-table" width="98%">
                        <tr>
                            <td width="22%" style="display:none;">
                                <cc1:ucLabel ID="lblStageComplete_Valid" runat="server"></cc1:ucLabel>
                                <input type="hidden" runat="server" id="hdnValidDispute" value="1" />
                                <asp:ValidationSummary ID="VSValidDispute" runat="server" ValidationGroup="ValidDispute"
                                    ShowSummary="false" ShowMessageBox="true" />
                            </td>
                            <td width="1%" style="display:none;">
                                :
                            </td>
                            <td align="left" width="53%" style="display:none;">
                                <cc1:ucCheckbox ID="chkGIN014a_StageCompleted" runat="server" Checked="true" onclick="return StageCompleted();" />
                            </td>
                            <td width="14%" style="display:none;">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" style="display:none;">
                                <cc1:ucLabel ID="lblDiscrepancyNote_1" runat="server" Text="If  you required to tag a comment without closing the discrepancy&lt;br/&gt;1 -    Deselect Stage Complete&lt;br/&gt;2 -   Enter  the comment&lt;br/&gt;3 -   Save"></cc1:ucLabel>
                            </td>
                            <td align="right" valign="bottom">
                                <cc1:ucButton ID="btnValidDisputeSave" runat="server" Text="Save" class="button"
                                    ValidationGroup="ValidDispute" OnClientClick="javascript:HideShowButton()" OnClick="btnValidDisputeSave_Click" />
                                <cc1:ucButton ID="btn1_Back" runat="server" Text="Back" class="button" OnClick="btnBack_Click" />
                                <asp:CustomValidator ID="cusvPleaseTakeAnAction_1" runat="server" Text="Please take an action."
                                    ClientValidationFunction="GIN014ActionRequired" Display="None" ValidationGroup="ValidDispute">
                                </asp:CustomValidator>
                                <asp:CustomValidator ID="cusvPleaseSelectOption" runat="server" Text="Please select Option."
                                    ClientValidationFunction="DisputeOptionReqired" Display="None" ValidationGroup="ValidDispute">
                                </asp:CustomValidator>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </cc1:ucPanel>
    <cc1:ucPanel ID="pnlGIN014b" runat="server" Width="100%" CssClass="fieldset-form"
        GroupingText="Action Required" Style="display: none">
        <table class="form-table" cellpadding="5" cellspacing="0" width="96%">
            <tr>
                <td style="font-weight: bold;">
                    <cc1:ucLabel ID="lbl2_GIIN014" runat="server" CssClass="action-required-heading" Style="text-align: center;"
                        ></cc1:ucLabel>
                </td>
            </tr>
            <tr>
                <td>
                    <table class="form-table" cellpadding="0" cellspacing="0" width="98%">
                        <tr>
                            <td width="30%">
                                <cc1:ucRadioButton ID="rdoDisputeValid_GIN014b" runat="server" GroupName="DisputeName"
                                    OnClick="TogglePanelControl()" />
                            </td>
                            <td width="70%">
                                <cc1:ucRadioButton ID="rdoDisputeNotValid_GIN014b" runat="server" GroupName="DisputeName"
                                    OnClick="TogglePanelControl()" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <table class="form-table" cellpadding="0" cellspacing="0" width="98%">
                                    <tr>
                                        <td colspan="2">
                                            <cc1:ucLabel ID="lblSelectReason" runat="server" Text="Select reason"></cc1:ucLabel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <cc1:ucRadioButton ID="rdpPODReason1" runat="server" Text="This POD is not for these goods"
                                                GroupName="DisputeNotValidReason" OnClick="ToggleDiscrepancyNo()" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <cc1:ucRadioButton ID="rdpPODReason2" runat="server" Text="The POD is for this delivery but not for the items or quantity being queried"
                                                GroupName="DisputeNotValidReason" OnClick="ToggleDiscrepancyNo()"></cc1:ucRadioButton>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <cc1:ucRadioButton ID="rdpPODReason3" runat="server" Text="Invalid Dispute as goods have been receipted, Accounts Payable to check"
                                                GroupName="DisputeNotValidReason" OnClick="ToggleDiscrepancyNo()"></cc1:ucRadioButton>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 40%">
                                            <cc1:ucRadioButton ID="rdpPODReason4" runat="server" Text="A discrepency already exists for this query"
                                                GroupName="DisputeNotValidReason" OnClick="ToggleDiscrepancyNo()"></cc1:ucRadioButton>
                                        </td>
                                        <td style="display: none; width: 60%" id="tdDiscrepncyNo">
                                            <table width="100% ">
                                                <tr>
                                                    <td style="width: 100%">
                                                        <cc1:ucLabel ID="lblDiscrepancyNumber" runat="server" Text="Discrepancy Number"></cc1:ucLabel>:
                                                        &nbsp;&nbsp;&nbsp;&nbsp;
                                                        <cc1:ucTextbox ID="txtDiscrepancyNo" runat="server" MaxLength="10"></cc1:ucTextbox>
                                                        <asp:RequiredFieldValidator ID="rfvRequiredDiscrepancyNo" runat="server" Text="Please enter Discrepancy Number."
                                                            ControlToValidate="txtDiscrepancyNo" Display="None" ValidationGroup="NotValidDispute"></asp:RequiredFieldValidator>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <cc1:ucRadioButton ID="rdoOther_2" runat="server" Text="Other" GroupName="DisputeNotValidReason"
                                                OnClick="ToggleDiscrepancyNo()"></cc1:ucRadioButton>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <cc1:ucLabel ID="lblComment_2" runat="server" Text="Please Enter Comments"></cc1:ucLabel>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <cc1:ucTextbox ID="txtNotValidDisputeComment" runat="server" onkeyup="checkTextLengthOnKeyUp(this,1000);"
                                    TextMode="MultiLine" CssClass="textarea" Width="98%"></cc1:ucTextbox>
                               <%-- <asp:RequiredFieldValidator ID="rfvCommentRequired_NotValidDispute" runat="server"
                                    Text="Please enter comments." ControlToValidate="txtNotValidDisputeComment" Display="None"
                                    ValidationGroup="NotValidDispute"></asp:RequiredFieldValidator>--%>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table cellpadding="0" cellspacing="0" class="form-table" width="98%">
                        <tr>
                            <td width="22%" style="display:none;">
                                <cc1:ucLabel ID="lblStageComplete_2" runat="server" Text="Stage Complete"></cc1:ucLabel>
                                <input type="hidden" runat="server" id="Hidden1" value="1" />
                                <asp:ValidationSummary ID="VSNotValidDispute" runat="server" ValidationGroup="NotValidDispute"
                                    ShowSummary="false" ShowMessageBox="true" />
                            </td>
                            <td width="1%" style="display:none;">
                                :
                            </td>
                            <td align="left" width="53%" style="display:none;">
                                <cc1:ucCheckbox ID="chkGIN014b_StageCompleted" runat="server" Checked="true" onclick="return StageCompleted();" />
                            </td>
                            <td width="14%" style="display:none;">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" style="display:none;">
                                <cc1:ucLabel ID="lblDiscrepancyNote_2" runat="server" Text="If  you required to tag a comment without closing the discrepancy&lt;br/&gt;1 -    Deselect Stage Complete&lt;br/&gt;2 -   Enter  the comment&lt;br/&gt;3 -   Save"></cc1:ucLabel>
                            </td>
                            <td align="right" valign="bottom">
                                <cc1:ucButton ID="btnNotValidDisputeSave" runat="server" Text="Save" class="button"
                                    OnClientClick="ToggleDiscrepancyNo()" ValidationGroup="NotValidDispute" OnClick="btnNotValidDisputeSave_Click" />
                                <cc1:ucButton ID="btn2_Back" runat="server" Text="Back" class="button" OnClick="btnBack_Click" />
                                <asp:CustomValidator ID="cusvPleaseSelectReason" runat="server" Text="Please select reason."
                                    ClientValidationFunction="GIN014SelectReason" Display="None" ValidationGroup="NotValidDispute">
                                </asp:CustomValidator>
                                 <asp:CustomValidator ID="cusvCommentRequired_NotValidDispute" runat="server"   Text="Please enter comments."
                                    ClientValidationFunction="GIN014EnterComments" Display="None" ValidationGroup="NotValidDispute">
                                </asp:CustomValidator>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </cc1:ucPanel>
    </form>
</body>
</html>

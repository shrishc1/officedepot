﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

using Utilities;
using WebUtilities;
using BusinessEntities.ModuleBE.Discrepancy.LogDiscrepancy;
using BusinessLogicLayer.ModuleBAL.Discrepancy.LogDiscrepancy;
using BusinessEntities.ModuleBE.AdminFunctions;
using BusinessLogicLayer.ModuleBAL.AdminFunctions;


public partial class GINAction_GIN014 : CommonPage
{
    #region Global variables
    #endregion
    string FollowingRequestMessage = WebCommon.getGlobalResourceValue("FollowingRequestMessage");
    string GIN014Desc = WebCommon.getGlobalResourceValue("GIN013Desc");
    #region Events
    protected void Page_Load(object sender, EventArgs e)
    {
        
        if (!IsPostBack)
        {
            txtDateAdjustment.Text = DateTime.Now.ToString("dd/MM/yyyy");
            txtDateAdjustment.Attributes.Add("readonly", "readonly");
            if (GetQueryStringValue("VDRNO") != null)
            {
                if (GetQueryStringValue("VDRNO").ToArray()[0].ToString() == "U")
                {
                    DISLog_WorkFlowBE oDISLog_WorkFlowBE = new DISLog_WorkFlowBE();
                    DISLog_WorkFlowBAL oDISLog_WorkFlowBAL = new DISLog_WorkFlowBAL();
                    rdpPODReason3.Text = WebCommon.getGlobalResourceValue("PODReason3_new");
                    trresolution.Visible = false;
                    hdnResolution.Value = "False";
                    oDISLog_WorkFlowBE.Action="GetGIN_IsValidDispute_Count";
                    oDISLog_WorkFlowBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
                    int count = oDISLog_WorkFlowBAL.GetGINValidDisputeCountBAL(oDISLog_WorkFlowBE);
                    if (count > 0)
                    {
                        lblGIIN014_1.Text = WebCommon.getGlobalResourceValue("GIN0014NewText");
                        lbl2_GIIN014.Text = WebCommon.getGlobalResourceValue("GIN0014NewText");
                    }
                    else 
                    {
                        lblGIIN014_1.Text = WebCommon.getGlobalResourceValue("GIN014");
                        lbl2_GIIN014.Text = WebCommon.getGlobalResourceValue("GIN014");
                    }
                }
            }
            else
            {
                trresolution.Visible = true;
                hdnResolution.Value = "True";
            }
        }
    }

    protected void btnValidDisputeSave_Click(object sender, EventArgs e)
    {
        btnValidDisputeSave.Enabled = false;
        DISLog_WorkFlowBE oDISLog_WorkFlowBE = new DISLog_WorkFlowBE();
        DISLog_WorkFlowBAL oDISLog_WorkFlowBAL = new DISLog_WorkFlowBAL();
        if (GetQueryStringValue("disLogID") != null && GetQueryStringValue("disLogID").ToString() != "")
        {
            oDISLog_WorkFlowBE.Action = "InsertUpdateWorkflowActions";
            oDISLog_WorkFlowBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
            oDISLog_WorkFlowBE.GIN014_IsValidDispute = rdoDisputeValid_GIN014a.Checked;
            oDISLog_WorkFlowBE.GIN014_ValidDisputeReasonError = rdoGoodsInError.Checked ? "G" : "O";
            oDISLog_WorkFlowBE.GIN014_GoodsDecision = rdoBookedInAgainstOriginalPO.Checked ? 'O' : (rdoBookedInAgainstNax.Checked ? 'N' : (rdoStockAdjustment.Checked ? 'A' : 'E'));
            oDISLog_WorkFlowBE.GIN014_NAX_PO = !string.IsNullOrEmpty(txtNaxPO.Text.Trim()) ? txtNaxPO.Text.Trim() : null;
            oDISLog_WorkFlowBE.GIN014_DateofAdjustment = rdoStockAdjustment.Checked == true ?  (!string.IsNullOrEmpty(txtDateAdjustment.Text.Trim()) ? Common.GetMM_DD_YYYY(txtDateAdjustment.Text.Trim()) : (DateTime?)null) :( DateTime?)null;
            oDISLog_WorkFlowBE.GIN014_AdjustedQuantity = !string.IsNullOrEmpty(txtAdjustedQuantity.Text.Trim()) ? Convert.ToDecimal(txtAdjustedQuantity.Text.Trim()) : (decimal?)null;

            oDISLog_WorkFlowBE.StageCompletedForWhom = "GIN014_StageCompleted";
            if (chkGIN014a_StageCompleted.Checked)
            {
                oDISLog_WorkFlowBE.StageCompleted = true;
            }
            else
            {
                oDISLog_WorkFlowBE.StageCompleted = false;
            }
            oDISLog_WorkFlowBE.Comment = txtValidDisputeComment.Text;

            //update the work flow action table each time for the comments and other fields( if stage is completed)
            oDISLog_WorkFlowBE.DiscrepancyWorkFlowID = Convert.ToInt32(GetQueryStringValue("DisWFlowID").ToString());

            //insert or update the record according to the stage completed status
            int? iResult = oDISLog_WorkFlowBAL.addWorkFlowActionsBAL(oDISLog_WorkFlowBE);

            InsertHtmlForValidDispute(oDISLog_WorkFlowBE);

        }

        //Redirecting to Previous Page
        RedirectToPage(GetQueryStringValue("PreviousPage"));
    }

    protected void btnNotValidDisputeSave_Click(object sender, EventArgs e)
    {
        btnNotValidDisputeSave.Enabled = false;
        DISLog_WorkFlowBE oDISLog_WorkFlowBE = new DISLog_WorkFlowBE();
        DISLog_WorkFlowBAL oDISLog_WorkFlowBAL = new DISLog_WorkFlowBAL();
        if (GetQueryStringValue("disLogID") != null && GetQueryStringValue("disLogID").ToString() != "")
        {
            oDISLog_WorkFlowBE.Action = "InsertUpdateWorkflowActions";
            oDISLog_WorkFlowBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
            oDISLog_WorkFlowBE.GIN014_IsValidDispute = rdoDisputeNotValid_GIN014b.Checked;

            oDISLog_WorkFlowBE.GIN014_NotValidDisputeReasonError = rdpPODReason1.Checked ? "N" : (rdpPODReason2.Checked ? "Q" : (rdpPODReason3.Checked ? "I" : (rdpPODReason4.Checked ? "D" : "O")));

            oDISLog_WorkFlowBE.GIN014_DiscrepancyNoIfExist = rdpPODReason4.Checked == true ? txtDiscrepancyNo.Text.Trim() : null;

            oDISLog_WorkFlowBE.StageCompletedForWhom = "GIN014_StageCompleted";
            if (chkGIN014a_StageCompleted.Checked)
            {
                oDISLog_WorkFlowBE.StageCompleted = true;
            }
            else
            {
                oDISLog_WorkFlowBE.StageCompleted = false;
            }
            oDISLog_WorkFlowBE.Comment = txtNotValidDisputeComment.Text;

            //update the work flow action table each time for the comments and other fields( if stage is completed)
            oDISLog_WorkFlowBE.DiscrepancyWorkFlowID = Convert.ToInt32(GetQueryStringValue("DisWFlowID").ToString());

            //insert or update the record according to the stage completed status
            int? iResult = oDISLog_WorkFlowBAL.addWorkFlowActionsBAL(oDISLog_WorkFlowBE);

            InsertHtmlForNotValidDispute(oDISLog_WorkFlowBE);

        }
        //Redirecting to Previous Page
        RedirectToPage(GetQueryStringValue("PreviousPage"));
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        //Redirecting to Previous Page
        RedirectToPage(GetQueryStringValue("PreviousPage"));
    }
    #endregion

    #region Methods

    private void InsertHtmlForValidDispute(DISLog_WorkFlowBE oDISLog_WorkFlowBE)
    {
        DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
        DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();
        WorkflowHTML oWorkflowHTML = new WorkflowHTML();


        DISLog_WorkFlowBAL oDISLog_WorkFlowBAL = new DISLog_WorkFlowBAL();

        string Action = rdoDisputeValid_GIN014a.Checked ? "Dispute is Valid" : "Dispute is not valid";        
        string Reason = rdoGoodsInError.Checked ? "Goods In Error" : "Other";

        if (chkGIN014a_StageCompleted.Checked)
        {
            if (rdoBookedInAgainstOriginalPO.Checked || rdoInternalError.Checked)
            {
                oDiscrepancyBE.GINHTML = oWorkflowHTML.function6(DateTime.Now, "Yellow", UIUtility.GetODVendorUserName(Convert.ToString(Session["Role"]), Convert.ToString(Session["UserName"]), Convert.ToString(Session["LoginID"])),
                                                                 Action, "", Reason, "", txtValidDisputeComment.Text.Trim(),"",(DateTime?)null,"");
            }
            else if (rdoStockAdjustment.Checked)
            {
                oDiscrepancyBE.GINHTML = oWorkflowHTML.function6(DateTime.Now, "Yellow", UIUtility.GetODVendorUserName(Convert.ToString(Session["Role"]), Convert.ToString(Session["UserName"]), Convert.ToString(Session["LoginID"])),
                                                                 Action, "", Reason, "", txtValidDisputeComment.Text.Trim(), "", Common.GetMM_DD_YYYY(txtDateAdjustment.Text), txtAdjustedQuantity.Text.Trim());
            }
            else if (rdoBookedInAgainstNax.Checked)
            {
                oDiscrepancyBE.GINHTML = oWorkflowHTML.function6(DateTime.Now, "Yellow", UIUtility.GetODVendorUserName(Convert.ToString(Session["Role"]), Convert.ToString(Session["UserName"]), Convert.ToString(Session["LoginID"])),
                                                               Action, "", Reason, "", txtValidDisputeComment.Text.Trim(),txtNaxPO.Text.Trim(),(DateTime?)null, "");
            }

            oDiscrepancyBE.GINActionRequired = false;
            

            oDiscrepancyBE.INVHTML = oWorkflowHTML.function3("White", null, "", "");
            oDiscrepancyBE.INVActionRequired = false;

            string VendorAccountPayablesEmail = string.Empty;

            oDISLog_WorkFlowBE.Action = "GetAccountPayable";
            VendorAccountPayablesEmail = oDISLog_WorkFlowBAL.GetVendorAccountPayablesEmail(oDISLog_WorkFlowBE);
            oDiscrepancyBE.APHTML = oWorkflowHTML.function2(DateTime.Now, null, "Blue", VendorAccountPayablesEmail,
                                                            "Build on to Accounts Payable Work List with the following request",
                                                            "POD was correct.Please  add a comment to update  the vendor");
           

            if (GetQueryStringValue("VDRNO") != null)
            {
                if (GetQueryStringValue("VDRNO").ToArray()[0].ToString() == "U")
                {
                    //oDiscrepancyBE.INVHTML = oWorkflowHTML.function3("White", null, "", "");
                    oDiscrepancyBE.INVHTML = oWorkflowHTML.function4("Blue", "", null, "", "",
              FollowingRequestMessage.Replace("{stockPlannerID}", "").Replace("{Date}", DateTime.Now.ToString("dd/MM/yyyy")),
            WebCommon.getGlobalResourceValue("INV014"));



                    oDiscrepancyBE.INVActionRequired = true;



                    oDiscrepancyBE.INVUserControl = "INV014";

                    oDiscrepancyBE.APHTML = oWorkflowHTML.function3("White", null, "", "");
                    oDiscrepancyBE.APActionRequired = false;                    
                    oDiscrepancyBE.InventoryActionCategory = "AR";  //Action Required
                }
                else {
                    oDiscrepancyBE.APActionRequired = true;
                    oDiscrepancyBE.APUserControl = "ACP004";                   
                    oDiscrepancyBE.APActionCategory = "AR";  //Action Required
                }
            }

            oDiscrepancyBE.VENHTML = oWorkflowHTML.function3("White", null, "", "");
            oDiscrepancyBE.VenActionRequired = false;

            oDiscrepancyBE.Action = "InsertHTML";
            oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
            oDiscrepancyBE.LoggedDateTime = DateTime.Now;
            oDiscrepancyBE.LevelNumber = 1;
            oDiscrepancyBE.ActionTakenBy = "GIN";
            int? iResult1 = oDiscrepancyBAL.addWorkFlowHTMLsBAL(oDiscrepancyBE);
        }
        else
        {
            oDiscrepancyBE.INVHTML = oWorkflowHTML.function3("White", DateTime.Now, txtValidDisputeComment.Text.Trim());
            oDiscrepancyBE.INVActionRequired = false;
            oDiscrepancyBE.Action = "InsertHTML";
            oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
            oDiscrepancyBE.LoggedDateTime = DateTime.Now;
            oDiscrepancyBE.LevelNumber = 1;
            int? iResult = oDiscrepancyBAL.addWorkFlowHTMLsBAL(oDiscrepancyBE);
        }
    }

    private void InsertHtmlForNotValidDispute(DISLog_WorkFlowBE oDISLog_WorkFlowBE)
    {
        DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
        DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();
        WorkflowHTML oWorkflowHTML = new WorkflowHTML();


        DISLog_WorkFlowBAL oDISLog_WorkFlowBAL = new DISLog_WorkFlowBAL();

        string Action = rdoDisputeValid_GIN014a.Checked ? "Dispute is Valid" : "Dispute is not valid";
        string Reason;
        if (rdpPODReason1.Checked)
            Reason = "POD not for these goods";
        else if (rdpPODReason2.Checked)
            Reason = "POD not for items being queried";
        else if (rdpPODReason3.Checked)
            Reason = "Goods Receipted, AP to check";
        else if (rdpPODReason4.Checked)
            Reason = "Discrepancy already exist";
        else
            Reason = "Other";

        string DiscrepancyNo = rdpPODReason4.Checked ? txtDiscrepancyNo.Text : "";

        if (chkGIN014b_StageCompleted.Checked)
        {

            oDiscrepancyBE.GINHTML = oWorkflowHTML.function6(DateTime.Now, "Yellow", UIUtility.GetODVendorUserName(Convert.ToString(Session["Role"]), Convert.ToString(Session["UserName"]), Convert.ToString(Session["LoginID"])),
                                                             Action, "", Reason, DiscrepancyNo, txtNotValidDisputeComment.Text.Trim());
            oDiscrepancyBE.GINActionRequired = false;

            oDiscrepancyBE.INVHTML = oWorkflowHTML.function3("White", null, "", "");
            oDiscrepancyBE.INVActionRequired = false;


            
            
           
            oDiscrepancyBE.VENHTML = oWorkflowHTML.function3("White", null, "", "");
            oDiscrepancyBE.VenActionRequired = false;

            if (GetQueryStringValue("VDRNO") != null)
            {
                if (GetQueryStringValue("VDRNO").ToArray()[0].ToString() == "U")
                {
                  //  oDiscrepancyBE.INVHTML = oWorkflowHTML.function3("White", null, "", "");
                    oDiscrepancyBE.INVHTML = oWorkflowHTML.function4("Blue", "", null, "", "",
             FollowingRequestMessage.Replace("{stockPlannerID}", "").Replace("{Date}", DateTime.Now.ToString("dd/MM/yyyy")),
           WebCommon.getGlobalResourceValue("INV014"));
                    oDiscrepancyBE.INVActionRequired = true;                    
                    oDiscrepancyBE.InventoryActionCategory = "AR";  //Action Required
                    if (rdoDisputeNotValid_GIN014b.Checked)
                    {
                        oDiscrepancyBE.INVUserControl = "INV016";
                    }
                    else
                    {
                        oDiscrepancyBE.INVUserControl = "INV014";
                    }
                    oDiscrepancyBE.APHTML = oWorkflowHTML.function3("White", null, "", "");
                    oDiscrepancyBE.APActionRequired = false;

                }
                else if (GetQueryStringValue("VDRNO").ToArray()[0].ToString() == "T")
                {
                    //  oDiscrepancyBE.INVHTML = oWorkflowHTML.function3("White", null, "", "");
                    string VendorAccountPayablesEmail = string.Empty;

                    oDISLog_WorkFlowBE.Action = "GetAccountPayable";
                    VendorAccountPayablesEmail = oDISLog_WorkFlowBAL.GetVendorAccountPayablesEmail(oDISLog_WorkFlowBE);
                    
                    oDiscrepancyBE.APActionRequired = true;
                    // 

                    
                    if (rdoDisputeNotValid_GIN014b.Checked)
                    {
                        oDiscrepancyBE.APHTML = oWorkflowHTML.function4("Blue", "", null, "", "",
             FollowingRequestMessage.Replace("{stockPlannerID}", "").Replace("{Date}", DateTime.Now.ToString("dd/MM/yyyy")),
           WebCommon.getGlobalResourceValue("AP014"));
                        oDiscrepancyBE.APUserControl = "ACP014";
                    }
                    else
                    {
                        oDiscrepancyBE.APHTML = oWorkflowHTML.function2(DateTime.Now, null, "Blue", VendorAccountPayablesEmail,
                                                                    "Build on to Accounts Payable Work List with the following request",
                                                                    "POD was incorrect. Please add a comment to close the discrepancy");
                        oDiscrepancyBE.APUserControl = "ACP005";
                    }
                    //oDiscrepancyBE.APHTML = oWorkflowHTML.function3("White", null, "", "");
                    //  oDiscrepancyBE.APActionRequired = false;
                    oDiscrepancyBE.APActionCategory = "AR";  //Action Required
                }
            }

            oDiscrepancyBE.Action = "InsertHTML";
            oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
            oDiscrepancyBE.LoggedDateTime = DateTime.Now;
            oDiscrepancyBE.LevelNumber = 1;
            oDiscrepancyBE.ActionTakenBy = "GIN";            
            int? iResult1 = oDiscrepancyBAL.addWorkFlowHTMLsBAL(oDiscrepancyBE);
        }
        else
        {
            oDiscrepancyBE.APHTML = oWorkflowHTML.function3("White", DateTime.Now, txtValidDisputeComment.Text.Trim());
            oDiscrepancyBE.APActionRequired = false;
            oDiscrepancyBE.Action = "InsertHTML";
            oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
            oDiscrepancyBE.LoggedDateTime = DateTime.Now;
            oDiscrepancyBE.LevelNumber = 1;
            oDiscrepancyBE.ActionTakenBy = "GIN";
            int? iResult = oDiscrepancyBAL.addWorkFlowHTMLsBAL(oDiscrepancyBE);
        }
    }

    #endregion

}
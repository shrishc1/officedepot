﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using Utilities; using WebUtilities;
using BusinessEntities.ModuleBE.Discrepancy.LogDiscrepancy;
using BusinessLogicLayer.ModuleBAL.Discrepancy.LogDiscrepancy;
using System.Data;
using BusinessEntities.ModuleBE.Appointment.CountrySetting;
using BusinessEntities.ModuleBE.Discrepancy.LogDiscrepancy;
using BusinessEntities.ModuleBE.Security;
using BusinessLogicLayer.ModuleBAL.Appointment.CountrySetting;
using BusinessLogicLayer.ModuleBAL.Discrepancy.LogDiscrepancy;
using BusinessLogicLayer.ModuleBAL.Security;
public partial class GINAction_GIN001 : CommonPage
{
    #region Global variables

    string NAXPO = string.Empty;

    #endregion
    #region Events

    protected override void OnPreRender(EventArgs e)
    {
        //base.OnPreRender(e);
        //GetAdvisedPurchaseOrder();
        //DiscrepancyBE oDiscrepancyBE1 = new DiscrepancyBE();
        //DiscrepancyBAL oDiscrepancyBAL1 = new DiscrepancyBAL();
        //oDiscrepancyBE1.Action = "GetEsclationType";
        //oDiscrepancyBE1.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
        //string EsclationType = oDiscrepancyBAL1.GetEsclationTypeBAL(oDiscrepancyBE1);       
      
    }
    protected string CommentRequired = "-" + WebCommon.getGlobalResourceValue("CommentRequired");
    string fileUploadError2 = WebCommon.getGlobalResourceValue("fileUploadError2");
    string DiscrepancyError = WebCommon.getGlobalResourceValue("DiscrepancyError");
    string NAXPOError = WebCommon.getGlobalResourceValue("NAXPOError");
    string WrongFile = WebCommon.getGlobalResourceValue("WrongFile");
    string fileName1 = string.Empty;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            chkStageCompleted.Checked = true;

        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
      
        if (GetQueryStringValue("disLogID") != null && GetQueryStringValue("disLogID").ToString() != "")
        {
            //******************

            //string GoodsReturnType = string.Empty;
            DISLog_WorkFlowBE oDISLog_WorkFlowBE = new DISLog_WorkFlowBE();
            DISLog_WorkFlowBAL oDISLog_WorkFlowBAL = new DISLog_WorkFlowBAL();
            oDISLog_WorkFlowBE.Action = "InsertUpdateWorkflowActions";
            oDISLog_WorkFlowBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
            oDISLog_WorkFlowBE.Comment = txtComment.Text;
            oDISLog_WorkFlowBE.StageCompletedForWhom = "GIN015_StageCompleted";

            if (rdoVendorFigure.Checked)
            { 
              
              if (rdoGIN0015Text10.Checked)
              {
                  oDISLog_WorkFlowBE.GIN015_VendorFigurecorrect = "Receiving Error - Stock found, now";
              }
              else if (rdoGIN0015Text11.Checked)
              {
                  oDISLog_WorkFlowBE.GIN015_VendorFigurecorrect = "Booked in as NAX/other PO";
                  oDISLog_WorkFlowBE.GIN015_VendorFigurecorrectNAXPO = txtNAX.Text;

                  if (string.IsNullOrEmpty(txtNAX.Text))
                  {
                      ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('" + NAXPOError + "')", true);
                      return;
                  }
              }
              else if (rdoGIN0015Text12.Checked)
              {
                  oDISLog_WorkFlowBE.GIN015_VendorFigurecorrect = "Receiving Error - Received but warehouse";
              }
              oDISLog_WorkFlowBE.InvoiceResolution = "Invoice Qty Accepted ";
            }
            else
            {
                if (chkGIN0015Text1.Checked)
                {
                    oDISLog_WorkFlowBE.GIN015_DiscrepancyNo = txtDiscreapncy.Text;
                    if (string.IsNullOrEmpty(txtDiscreapncy.Text)) {
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('" + DiscrepancyError + "')", true);
                        return;
                    }
                }
                if (chkGIN0015Text2.Checked)
                {
                    oDISLog_WorkFlowBE.GIN015_DelieveryNoteQualities = true;
                }
                if (chkGIN0015Text3.Checked || chkGIN0015Text2.Checked)
                {
                    if (FileUpload1.HasFile)
                    {
                        if (FileUpload1.FileName.Split('.').ElementAt(1).ToLower() == "jpg"  || FileUpload1.FileName.Split('.').ElementAt(1).ToLower() == "bmp"  || FileUpload1.FileName.Split('.').ElementAt(1).ToLower() == "pdf")
                        {
                            fileName1 = FileUpload1.FileName;
                            FileUpload1.SaveAs(Path.Combine(Server.MapPath("~/images/discrepancy"), fileName1));
                            oDISLog_WorkFlowBE.GIN015_DelieveryNoteInvalid = fileName1;
                        }
                        else
                        {
                            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('" + WrongFile + "')", true);
                            return;
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('" + fileUploadError2 + "')", true);
                        return;
                    }
                }
                oDISLog_WorkFlowBE.InvoiceResolution = "Receipt Qty Accepted";
            }
            //if (EsclationType == "EscalationType3")
            //{
            //    oDISLog_WorkFlowBE.StageCompleted = true;
            //}
            if (chkStageCompleted.Checked)
            {
                oDISLog_WorkFlowBE.StageCompleted = true;
            }
            else
                oDISLog_WorkFlowBE.StageCompleted = false;
            //if (rdoVendorToCollect.Checked)
            //{
            //    oDISLog_WorkFlowBE.GIN_HowGoodsReturned = "V";
            //    GoodsReturnType = "V";
            //}
            //else if (rdoOfficeDepotToReturn.Checked)
            //{
            //    oDISLog_WorkFlowBE.GIN_HowGoodsReturned = "O";
            //    GoodsReturnType = "O";
            //}
            //else if (rdoDisposeOfGood.Checked)
            //{
            //    oDISLog_WorkFlowBE.GIN_HowGoodsReturned = "D";
            //    GoodsReturnType = "D";
            //}
            //update the work flow action table each time for the comments and other fields( if stage is completed)
            oDISLog_WorkFlowBE.DiscrepancyWorkFlowID = Convert.ToInt32(GetQueryStringValue("DisWFlowID").ToString());

            //insert or update the record in workflow action table
            int? iResult = oDISLog_WorkFlowBAL.addWorkFlowActionsBAL(oDISLog_WorkFlowBE);


            oDISLog_WorkFlowBE.Action = "InvoiceInsertDocInformation";
            oDISLog_WorkFlowBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
            oDISLog_WorkFlowBE.DocPath = fileName1;
            oDISLog_WorkFlowBE.Doctype = "Paperwork";
            oDISLog_WorkFlowBAL.addWorkFlowInvoiceQueryActionsBAL(oDISLog_WorkFlowBE);

            //now insert the work flow HTML according to the action taken only if the stage is completed
            DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
            DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();

            string sentToLinks = string.Empty;
            string sPO = string.Empty;

            string issueText=string.Empty;

            if (chkStageCompleted.Checked)
            {
                WorkflowHTML oWorkflowHTML = new WorkflowHTML();
                string sAction = oDISLog_WorkFlowBE.INV001_GoodsKeptDecision == "K" ? "Goods to be kept"
                                : (oDISLog_WorkFlowBE.INV001_GoodsKeptDecision == "N" ? "Goods to be kept NAX order"
                                : "Goods to be returned");

                //string sPO = oDISLog_WorkFlowBE.INV001_GoodsKeptDecision == "K" || oDISLog_WorkFlowBE.INV001_GoodsKeptDecision == "N" ? txtINV001aPurchaseOrder.Text : GetQueryStringValue("PONo").ToString();

                if (rdoVendorFigure.Checked)
                {
                    if(rdoGIN0015Text10.Checked)
                    {
                    issueText=WebCommon.getGlobalResourceValue("GIN015WOrkflow5");
                    }
                    else if(rdoGIN0015Text11.Checked)
                    {
                    issueText=WebCommon.getGlobalResourceValue("GIN015WOrkflow6");
                    }
                    else if(rdoGIN0015Text12.Checked)
                    {
                    issueText=WebCommon.getGlobalResourceValue("GIN015WOrkflow5");
                    }
                    else if(rdoOther.Checked)
                    {
                    issueText=WebCommon.getGlobalResourceValue("Other");
                    }
                    sentToLinks = SendMailAndGetMailLinks(Convert.ToInt32(GetQueryStringValue("disLogID")), "invoice query", "Communication1", "Communication2");

                    oDiscrepancyBE.GINHTML = oWorkflowHTML.function13(pColor: "Yellow", pDate: DateTime.Now,
                    pUserName: Convert.ToString(Session["UserName"]), pAction: WebCommon.getGlobalResourceValue("GIN015WOrkflow3"), issue: issueText, pComments: txtComment.Text);

                    oDiscrepancyBE.GINActionRequired = false;
                    oDiscrepancyBE.INVHTML = oWorkflowHTML.function3("White", null, "", "");
                    oDiscrepancyBE.INVActionRequired = false;
                    oDiscrepancyBE.APHTML = oWorkflowHTML.function13(pColor: "Blue", pDate: DateTime.Now, SentTo: WebCommon.getGlobalResourceValue("AccountsPayable"), DiscrePancyTextBottom: WebCommon.getGlobalResourceValue("GIN015WOrkflow2"));
                    oDiscrepancyBE.APActionRequired = true;
                    oDiscrepancyBE.APUserControl = "ACP007";
                    oDiscrepancyBE.VENHTML = oWorkflowHTML.function3("White", null, "", "");
                    oDiscrepancyBE.VenActionRequired = false;

                    oDiscrepancyBE.Action = "InsertHTML";
                    oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
                    oDiscrepancyBE.LoggedDateTime = DateTime.Now;
                    oDiscrepancyBE.LevelNumber = 1;
                    oDiscrepancyBE.ActionTakenBy = "GIN";
                    oDiscrepancyBE.APActionCategory = "AR";  //Action Required
                    int? iResult1 = oDiscrepancyBAL.addWorkFlowHTMLsBAL(oDiscrepancyBE);
                }
                if (rdoODFigure.Checked)               
                {
                   // WorkflowHTML oWorkflowHTML = new WorkflowHTML();
                    sentToLinks = SendMailAndGetMailLinks(Convert.ToInt32(GetQueryStringValue("disLogID")), "invoice query", "Communication1", "Communication3");
                    oDiscrepancyBE.GINHTML = oWorkflowHTML.function13(pColor: "Yellow", pDate: DateTime.Now,
                    pUserName: Convert.ToString(Session["UserName"]), pAction: WebCommon.getGlobalResourceValue("GIN015WOrkflow"), DiscrepancyCreated: chkGIN0015Text1.Checked == true ? "Yes" : "No", DeliveryNoteChecked: chkGIN0015Text2.Checked == true ? "Yes" : "No", DeliveryNoteInvalid: chkGIN0015Text3.Checked == true ? "Yes" : "No", PhysicalInventoryCheck: chkGIN0015Text5.Checked == true ? "Yes" : "No", HostSystemChecked:chkGIN0015Text6.Checked == true ? "Yes" : "No", pComments: txtComment.Text);
                   
                    oDiscrepancyBE.GINActionRequired = false;
                    oDiscrepancyBE.INVHTML = oWorkflowHTML.function3("White", null, "", "");
                    oDiscrepancyBE.INVActionRequired = false;
                    oDiscrepancyBE.APHTML = oWorkflowHTML.function3("White", null, "", "");
                    oDiscrepancyBE.APActionRequired = false;
                    oDiscrepancyBE.VENHTML = oWorkflowHTML.function2(DateTime.Now, null, "Blue", Convert.ToString(sentToLinks), "", WebCommon.getGlobalResourceValue("GIN015WOrkflow1"));
                    oDiscrepancyBE.VENUserControl = "VEN028";
                    oDiscrepancyBE.VenActionRequired = true;

                    oDiscrepancyBE.Action = "InsertHTML";
                    oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
                    oDiscrepancyBE.LoggedDateTime = DateTime.Now;
                    oDiscrepancyBE.LevelNumber = 1;
                    oDiscrepancyBE.ActionTakenBy = "GIN";
                    oDiscrepancyBE.VendorActionCategory = "AR";  //Action Required
                    int?  iResult1 = oDiscrepancyBAL.addWorkFlowHTMLsBAL(oDiscrepancyBE);
                }
            }

        }
        //Redirecting to Previous Page
        RedirectToPage(GetQueryStringValue("PreviousPage"));
    }

    #endregion

    #region Methods
    private string SendMailAndGetMailLinks(int DiscrepancyLogID, string discrepancyType, string sCurrentCommunicationLevel, string sNextCommunicationLevel)
    {
        sendCommunicationAllLevel osendCommunicationAllLevel = new sendCommunicationAllLevel();
        return osendCommunicationAllLevel.SendMailAndGetMailLinks(DiscrepancyLogID, discrepancyType, sCurrentCommunicationLevel, sNextCommunicationLevel);
        //return osendCommunicationAllLevel.sendCommunicationByEMail(DiscrepancyLogID, discrepancyType, sCurrentCommunicationLevel, sNextCommunicationLevel);
    }
    private void GetAdvisedPurchaseOrder()
    {
        DISLog_WorkFlowBE oDISLog_WorkFlowBE = new DISLog_WorkFlowBE();
        DISLog_WorkFlowBAL oDISLog_WorkFlowBAL = new DISLog_WorkFlowBAL();
        oDISLog_WorkFlowBE.Action = "GetDataForSpecificAction";
        oDISLog_WorkFlowBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
        if (GetQueryStringValue("disLogID") != null && GetQueryStringValue("disLogID").ToString() != "")
        {
            oDISLog_WorkFlowBE.StageCompletedForWhom = "INV001_StageCompleted";//checking VEN012 instead of VEN010 in NoPO and VEN008 in Nopaperwork
        }

        DataTable dtVEN = oDISLog_WorkFlowBAL.GetDataForSpecificActionBAL(oDISLog_WorkFlowBE);
        if (dtVEN != null && dtVEN.Rows.Count > 0)
        {
            NAXPO = dtVEN.Rows[0]["INV001_NAX_PO"].ToString();
        }
    }   
    private void InsertHTML()
    {

        DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
        DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();

        WorkflowHTML oWorkflowHTML = new WorkflowHTML();
        oDiscrepancyBE.GINHTML = oWorkflowHTML.function3("White", DateTime.Now, txtComment.Text, "");


        oDiscrepancyBE.Action = "InsertHTML";
        oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
        oDiscrepancyBE.LoggedDateTime = DateTime.Now;
        oDiscrepancyBE.LevelNumber = 1;
        int? iResult = oDiscrepancyBAL.addWorkFlowHTMLsBAL(oDiscrepancyBE);
    }

    #endregion

    protected void btnBack_Click(object sender, EventArgs e)
    {
        //Redirecting to Previous Page
        RedirectToPage(GetQueryStringValue("PreviousPage"));
    }
    #region Print ReturnNote
    private int? PrintAndSaveReturnNote()
    {
        decimal TotalCost = 0;
        decimal NetTotalCost = 0;
        decimal FrieghtCharges = 0;
        string htmlBody = string.Empty;
        string DiscrepancyType = string.Empty;
        // vendor return address
        DiscrepancyBE oNewDiscrepancyBE = new DiscrepancyBE();
        DiscrepancyBAL oNewDiscrepancyBAL = new DiscrepancyBAL();
        oNewDiscrepancyBE.Action = "GetDiscrepancySiteVendorDetail";
        oNewDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID"));

        List<DiscrepancyBE> lstVendor = oNewDiscrepancyBAL.GetDiscrepancyLogSiteVendorDetailsBAL(oNewDiscrepancyBE);
        if (lstVendor.Count > 0)
            DiscrepancyType = lstVendor[0].DiscrepancyType;
        string VendorAddress = string.Empty;
        if (string.IsNullOrEmpty(lstVendor[0].Vendor.ReturnAddress))
        {
            VendorAddress = lstVendor[0].Vendor.address1 + "<br />" +
                lstVendor[0].Vendor.address2 + "<br />" +
                lstVendor[0].Vendor.city + "<br />" +
                lstVendor[0].Vendor.VMPPIN + " " + lstVendor[0].Vendor.VMPPOU;

        }
        else
        {
            VendorAddress = lstVendor[0].Vendor.ReturnAddress.Replace("\r\n", "<br />");
        }
        // site address
        string SiteAddress = lstVendor[0].Site.SiteAddressLine1 + "<br />" +
           lstVendor[0].Site.SiteAddressLine2 + "<br />" +
           lstVendor[0].Site.SiteAddressLine3 + "<br />" +
           lstVendor[0].Site.SiteAddressLine4 + "<br />" +
           lstVendor[0].Site.SiteAddressLine5 + "<br />" +
           lstVendor[0].Site.SiteAddressLine6 + "<br />" +
           lstVendor[0].Site.SitePincode;

        oNewDiscrepancyBE.Action = "GetDiscrepancyLogITemsForReturnNote";
        List<DiscrepancyBE> lstItems = oNewDiscrepancyBAL.GetDiscrepancyItemsForReturnNote(oNewDiscrepancyBE);
        string DiscrepancyItems = string.Empty;
        string InCorrectDiscrepancyItems = string.Empty;
        int count = lstItems.Count / 2;
        int tempCount = 0;
        if (lstItems != null && lstItems.Count > 0)
        {
            foreach (DiscrepancyBE item in lstItems)
            {
                if (count == 0 || tempCount < count)
                {
                    if (string.IsNullOrEmpty(item.ODSKUCode) && !string.IsNullOrEmpty(item.ProductDescription))
                    {
                        DiscrepancyItems += "<tr><td>" + item.DeliveredQuantity
                                + "</td><td>" + item.UOM
                                + "</td><td>" + item.ODSKUCode
                                + "</td><td>" + item.DirectCode
                                + "</td><td>" + item.ProductDescription;
                            if (lstItems[0].DiscrepancyTypeID == 12)
                            {
                                DiscrepancyItems += "</td><td>" + item.OriginalQuantity;
                                DiscrepancyItems += "</td><td>" + item.OutstandingQuantity;

                                DiscrepancyItems += "</td><td style='text-align:left;'>" + item.ItemVal
                               + "</td><td style='text-align:right;'>";
                            }
                            else
                            {
                                DiscrepancyItems += "</td><td style='text-align:right;'>" + item.ItemVal
                                + "</td><td style='text-align:right;'>";
                            }

                        if (item.DeliveredQuantity.HasValue && !string.IsNullOrEmpty(item.ItemVal))
                        {
                            TotalCost += Convert.ToDecimal(item.ItemVal) * item.DeliveredQuantity.Value;
                            DiscrepancyItems += Convert.ToDecimal(item.ItemVal) * item.DeliveredQuantity.Value;
                        }
                        else
                        {
                            DiscrepancyItems += string.Empty;
                        }
                        DiscrepancyItems += "</td></tr>";
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(item.ODSKUCode))
                        {
                            DiscrepancyItems += "<tr><td>" + item.DeliveredQuantity
                                + "</td><td>" + item.UOM
                                + "</td><td>" + item.ODSKUCode
                                + "</td><td>" + item.DirectCode
                                + "</td><td>" + item.ProductDescription;
                            if (lstItems[0].DiscrepancyTypeID == 12)
                            {
                                DiscrepancyItems += "</td><td>" + item.OriginalQuantity;
                                DiscrepancyItems += "</td><td>" + item.OutstandingQuantity;

                                DiscrepancyItems += "</td><td style='text-align:left;'>" + item.ItemVal
                            + "</td><td style='text-align:right;'>";
                            }
                            else
                            {
                                DiscrepancyItems += "</td><td style='text-align:right;'>" + item.ItemVal
                         + "</td><td style='text-align:right;'>";
                            }
                            if (item.DeliveredQuantity.HasValue && !string.IsNullOrEmpty(item.ItemVal))
                            {
                                TotalCost += Convert.ToDecimal(item.ItemVal) * item.DeliveredQuantity.Value;
                                DiscrepancyItems += Convert.ToDecimal(item.ItemVal) * item.DeliveredQuantity.Value;
                            }
                            else
                            {
                                DiscrepancyItems += string.Empty;
                            }
                            DiscrepancyItems += "</td></tr>";
                        }
                    }
                }
                else
                {
                    InCorrectDiscrepancyItems += "<tr><td>" + item.DeliveredQuantity
                    + "</td><td>" + item.UOM
                    + "</td><td>" + item.ODSKUCode
                    + "</td><td>" + item.DirectCode
                    + "</td><td>" + item.ProductDescription;
                    DiscrepancyItems += "</td></tr>";
                }
                tempCount++;
            }
        }
        //if (!string.IsNullOrEmpty(txtCarriageCharge.Text))
        //{
        //    FrieghtCharges = Convert.ToDecimal(txtCarriageCharge.Text);
        //}
        NetTotalCost = TotalCost + FrieghtCharges;
        string VendorLanguage = lstVendor[0].Vendor.Language;
        //string templateFile = "~/EmailTemplates/ReturnNote/ReturnNote." + VendorLanguage + ".htm";
        string templateFile = string.Empty;
        if (lstItems[0].DiscrepancyTypeID == 12)
        {
            templateFile = "~/EmailTemplates/ReturnNote/ReturnNoteQuality.english.htm";
        }
        else
        {
            templateFile = "~/EmailTemplates/ReturnNote/ReturnNote.english.htm";
        }

        DiscrepancyBE oDiscrepancyBE1 = new DiscrepancyBE();
        DiscrepancyBAL oDiscrepancyBAL1 = new DiscrepancyBAL();
        oDiscrepancyBE1.Action = "GetGoodsCollectionData";
        oDiscrepancyBE1.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
        string Carrier = oDiscrepancyBAL1.GetCarrierBAL(oDiscrepancyBE1);
        #region Setting reason as per the language ...
        switch (VendorLanguage)
        {
            case clsConstants.English:
                Page.UICulture = clsConstants.EnglishISO; // "en-US"; // English
                break;
            case clsConstants.French:
                Page.UICulture = clsConstants.FranceISO; // // France
                break;
            case clsConstants.German:
                Page.UICulture = clsConstants.GermanyISO; //"de"; // German
                break;
            case clsConstants.Dutch:
                Page.UICulture = clsConstants.NederlandISO; //"nl"; // Dutch
                break;
            case clsConstants.Spanish:
                Page.UICulture = clsConstants.SpainISO; //"es"; // Spanish
                break;
            case clsConstants.Italian:
                Page.UICulture = clsConstants.ItalyISO; //"it"; // Italy
                break;
            case clsConstants.Czech:
                Page.UICulture = clsConstants.CzechISO; //"cz"; // Czech
                break;
            default:
                Page.UICulture = clsConstants.EnglishISO; //"en-US"; // English
                break;
        }

        #endregion

        string contactPerson = string.Empty;

        sendCommunicationCommon osendCommunicationCommon = new sendCommunicationCommon();
        if (System.IO.File.Exists(System.Web.HttpContext.Current.Server.MapPath(templateFile.ToLower())))
        {
            using (StreamReader sReader = new StreamReader(System.Web.HttpContext.Current.Server.MapPath(templateFile.ToLower())))
            {
                htmlBody = sReader.ReadToEnd();
                if (DiscrepancyType != "Incorrect Product Code")
                    htmlBody = htmlBody.Replace("block", "none");

                htmlBody = htmlBody.Replace("{VendorDiscrepancyReturnNumber}", WebCommon.getGlobalResourceValue("VendorDiscrepancyReturnNumber"));
                htmlBody = htmlBody.Replace("{VDRNoValue}", lstVendor[0].VDRNo);

                htmlBody = htmlBody.Replace("{logoInnerPath}", osendCommunicationCommon.getAbsolutePath());

                htmlBody = htmlBody.Replace("{From}", WebCommon.getGlobalResourceValue("From"));
                htmlBody = htmlBody.Replace("{SiteAddressValue}", SiteAddress);

                htmlBody = htmlBody.Replace("{VendorDiscrepancyReturnNote}", WebCommon.getGlobalResourceValue("VendorDiscrepancyReturnNote"));

                htmlBody = htmlBody.Replace("{VendorNumberSDR}", WebCommon.getGlobalResourceValue("VendorNumberSDR"));
                htmlBody = htmlBody.Replace("{VendorNumberValue}", lstVendor[0].Vendor.Vendor_No);

                htmlBody = htmlBody.Replace("{VendorName}", WebCommon.getGlobalResourceValue("VendorName"));
                htmlBody = htmlBody.Replace("{VendorNameValue}", string.IsNullOrEmpty(lstVendor[0].Vendor.VendorContactName) ? lstVendor[0].Vendor.VendorName : lstVendor[0].Vendor.VendorContactName);

                htmlBody = htmlBody.Replace("{Returnto}", WebCommon.getGlobalResourceValue("Returnto"));
                htmlBody = htmlBody.Replace("{VendorAddressValue}", VendorAddress);

                htmlBody = htmlBody.Replace("{ReturnNoteText1}", WebCommon.getGlobalResourceValue("ReturnNoteText1"));
                htmlBody = htmlBody.Replace("{ReturnNoteText2}", WebCommon.getGlobalResourceValue("ReturnNoteText2"));

                htmlBody = htmlBody.Replace("{AuthorisedBy}", WebCommon.getGlobalResourceValue("AuthorisedBy"));
                htmlBody = htmlBody.Replace("{VendorAuthorisationReference}", WebCommon.getGlobalResourceValue("VendorAuthorisationReference"));
                htmlBody = htmlBody.Replace("{ReasonforReturn}", WebCommon.getGlobalResourceValue("ReasonforReturn"));


                htmlBody = htmlBody.Replace("{VendorContactValue}", GetVendorContactName());
                if (lstItems != null && lstItems.Count > 0)
                    htmlBody = htmlBody.Replace("{VendorAuthorisationReferenceValue}", lstItems[0].CollectionAuthNumber);
                else
                    htmlBody = htmlBody.Replace("{VendorAuthorisationReferenceValue}", "-");

                htmlBody = htmlBody.Replace("{VENDORDISREPACNYTYPEValue}", lstVendor[0].DiscrepancyType);

                htmlBody = htmlBody.Replace("{QTY}", WebCommon.getGlobalResourceValue("QTY"));
                htmlBody = htmlBody.Replace("{UOM}", WebCommon.getGlobalResourceValue("UOM"));
                htmlBody = htmlBody.Replace("{OURPRODUCTCODE}", WebCommon.getGlobalResourceValue("OURPRODUCTCODE"));
                htmlBody = htmlBody.Replace("{VENDORREFERENCE}", WebCommon.getGlobalResourceValue("VENDORREFERENCE"));
                htmlBody = htmlBody.Replace("{DESCRIPTION}", WebCommon.getGlobalResourceValue("DESCRIPTION"));
                htmlBody = htmlBody.Replace("{COST}", WebCommon.getGlobalResourceValue("COST"));
                htmlBody = htmlBody.Replace("{NET}", WebCommon.getGlobalResourceValue("NET"));

                // quatity return notes changes//
                if (lstItems[0].DiscrepancyTypeID == 12)
                {
                    htmlBody = htmlBody.Replace("{POQuantity}", WebCommon.getGlobalResourceValue("OriginalPOQuantity"));
                    htmlBody = htmlBody.Replace("{OUTSTANDING}", WebCommon.getGlobalResourceValue("OutstandingPOQuantity"));
                }

                htmlBody = htmlBody.Replace("{DiscrepancyItemsValue}", DiscrepancyItems);

                htmlBody = htmlBody.Replace("{ReturnTotal}", WebCommon.getGlobalResourceValue("ReturnTotal"));
                htmlBody = htmlBody.Replace("{TotalCostValue}", TotalCost.ToString());

                htmlBody = htmlBody.Replace("{FreightCharge}", WebCommon.getGlobalResourceValue("FreightCharge"));
                htmlBody = htmlBody.Replace("{FreightChargeValue}", FrieghtCharges.ToString());
                // *******  New changes
                htmlBody = htmlBody.Replace("{ReturnNoteIncorrectProductDec}", WebCommon.getGlobalResourceValue("ReturnNoteIncorrectProductDec"));
                htmlBody = htmlBody.Replace("{DiscrepancyItemsValue1}", InCorrectDiscrepancyItems);
                //******************************
                htmlBody = htmlBody.Replace("{Total}", WebCommon.getGlobalResourceValue("Total"));
                htmlBody = htmlBody.Replace("{NetTotalCostValue}", NetTotalCost.ToString());

                htmlBody = htmlBody.Replace("{PreparedBy}", WebCommon.getGlobalResourceValue("PreparedBy"));
                htmlBody = htmlBody.Replace("{OFFICE_DEPOT_DC_CONTACTValue}", Session["UserName"].ToString());

                htmlBody = htmlBody.Replace("{DatePrepared}", WebCommon.getGlobalResourceValue("DatePrepared"));
                htmlBody = htmlBody.Replace("{DATEPREPAREDValue}", DateTime.Now.ToString("dd/MM/yyyy"));

                htmlBody = htmlBody.Replace("{NumberofPallets}", WebCommon.getGlobalResourceValue("NumberofPallets"));
                htmlBody = htmlBody.Replace("{NumberofPalletsValue}", "");

                htmlBody = htmlBody.Replace("{NumberofCartons}", WebCommon.getGlobalResourceValue("NumberofCartons"));
                htmlBody = htmlBody.Replace("{NumberofCartonsValue}", "");

                htmlBody = htmlBody.Replace("{CollectionReturnDate}", WebCommon.getGlobalResourceValue("CollectionReturnDate"));
                
               htmlBody = htmlBody.Replace("{ColletionDateValue}", DateTime.Now.ToString("dd/MM/yyyy"));
                

                htmlBody = htmlBody.Replace("{Signed}", WebCommon.getGlobalResourceValue("Signed"));

                htmlBody = htmlBody.Replace("{Carrier}", WebCommon.getGlobalResourceValue("Carrier"));
               
                htmlBody = htmlBody.Replace("{CarrierValue}", Carrier);
                htmlBody = htmlBody.Replace("{VEHICLEREG}", WebCommon.getGlobalResourceValue("VEHICLEREG"));
                htmlBody = htmlBody.Replace("{DRIVER}", WebCommon.getGlobalResourceValue("DRIVER"));
                htmlBody = htmlBody.Replace("{SIGN}", WebCommon.getGlobalResourceValue("SIGN"));
                htmlBody = htmlBody.Replace("{Print}", WebCommon.getGlobalResourceValue("Print"));

                // changes for alternate Return note 
                htmlBody = htmlBody.Replace("{PalletExchangeInfo}", WebCommon.getGlobalResourceValue("PalletExchangeInfo"));
                htmlBody = htmlBody.Replace("{OnewayPallets}", WebCommon.getGlobalResourceValue("OneWayPallets"));
                htmlBody = htmlBody.Replace("{EuroPallets}", WebCommon.getGlobalResourceValue("NumberEuroPallets"));
                htmlBody = htmlBody.Replace("{EuroPalletsChanged}", WebCommon.getGlobalResourceValue("EuroPalletsChanged"));
                // changes end

                htmlBody = htmlBody.Replace("{ImportantNotice}", WebCommon.getGlobalResourceValue("ImportantNotice"));
                htmlBody = htmlBody.Replace("{ReturnNoteText3}", WebCommon.getGlobalResourceValue("ReturnNoteText3"));
                htmlBody = htmlBody.Replace("{ReturnNoteText4}", WebCommon.getGlobalResourceValue("ReturnNoteText4"));

                contactPerson = GetAPContact(lstVendor[0].VendorID);
                htmlBody = htmlBody.Replace("{ACCOUNTS PAYABLE CONTACT}", string.IsNullOrEmpty(contactPerson) ? "<Not Available>" : contactPerson);
                if (lstVendor[0].Site.SiteMangerUserID != null)
                    contactPerson = GetDCSiteContact(lstVendor[0].Site.SiteMangerUserID);

                htmlBody = htmlBody.Replace("{OFFICE DEPOT DC SITE CONTACT}", string.IsNullOrEmpty(contactPerson) ? "<Not Available>" : contactPerson);
            }
        }
        Page.UICulture = Convert.ToString(Session["CultureInfo"]);

        // Save into the database
        DiscrepancyReturnNoteBE oDiscrepancyReturnNoteBE = new DiscrepancyReturnNoteBE();
        oDiscrepancyReturnNoteBE.Action = "AddDiscrepancyReturnNote";
        oDiscrepancyReturnNoteBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID"));
        oDiscrepancyReturnNoteBE.LoggedDate = DateTime.Now;
        oDiscrepancyReturnNoteBE.ReturnNoteBody = htmlBody.ToString();

        int? retval = oNewDiscrepancyBAL.AddDiscrepancyReturnNote(oDiscrepancyReturnNoteBE);


        // Print Return note
        Session["LabelHTML"] = htmlBody.ToString();

        string a = "window.open('../../LogDiscrepancy/DIS_PrintLetter.aspx', '', 'left =' + screen.width + ',top=' + screen.height + ',width=0,height=0,toolbar=0,scrollbars=0,status=0')";
        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "pp", a, true);

        return retval;
    }
    private string GetAPContact(int? VendorID)
    {
        string DCSiteContact = string.Empty;
        SCT_UserBE oSCT_UserBE = new SCT_UserBE();
        oSCT_UserBE.VendorID = VendorID.Value;
        oSCT_UserBE.Action = "GetAccountPayableDetail";
        SCT_UserBAL oSCT_UserBAL = new SCT_UserBAL();
        List<SCT_UserBE> lstUser = oSCT_UserBAL.GetUserDetailsBAL(oSCT_UserBE);
        if (lstUser != null && lstUser.Count > 0)
        {
            DCSiteContact = lstUser[0].FirstName + " " + lstUser[0].Lastname + " " + lstUser[0].PhoneNumber;
        }
        return DCSiteContact;
    }

    private string GetVendorContactName()
    {
        string DiscrepancyVendorContact = string.Empty;
        DiscrepancyBE oNewDiscrepancyBE = new DiscrepancyBE();
        DiscrepancyBAL oNewDiscrepancyBAL = new DiscrepancyBAL();
        oNewDiscrepancyBE.Action = "GetDiscrepancyVendorContact";
        oNewDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID"));
        oNewDiscrepancyBE = oNewDiscrepancyBAL.GetDiscrepancyVendorContact(oNewDiscrepancyBE);
        if (!string.IsNullOrEmpty(oNewDiscrepancyBE.Username))
            DiscrepancyVendorContact = oNewDiscrepancyBE.Username;
        return DiscrepancyVendorContact;


    }
    private string GetDCSiteContact(int? UserID)
    {
        string DCSiteContact = string.Empty;
        SCT_UserBE oSCT_UserBE = new SCT_UserBE();
        oSCT_UserBE.UserID = UserID.Value;
        oSCT_UserBE.Action = "ShowAll";
        SCT_UserBAL oSCT_UserBAL = new SCT_UserBAL();
        List<SCT_UserBE> lstUser = oSCT_UserBAL.GetUserDetailsBAL(oSCT_UserBE);
        if (lstUser != null && lstUser.Count > 0)
        {
            DCSiteContact = lstUser[0].FirstName + " " + lstUser[0].Lastname + " " + lstUser[0].PhoneNumber;
        }
        return DCSiteContact;
    }

    
    #endregion
}
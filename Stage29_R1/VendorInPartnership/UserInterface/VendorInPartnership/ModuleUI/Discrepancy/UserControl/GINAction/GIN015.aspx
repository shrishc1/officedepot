﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="GIN015.aspx.cs" Inherits="GINAction_GIN001"
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Office Depot</title>
    <link rel="stylesheet" type="text/css" href="../../../../Css/style.css" />
    <link type="text/css" href="../../../../Css/ui-lightness/jquery-ui-1.8.5.custom.css"
        rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="../../../../Css/superfish.css" media="screen" />
    <script type="text/javascript" src="<%= ResolveClientUrl("../../../../Scripts/jquery-1.2.6.min.js")%>"></script>
    <script type="text/javascript" src="<%= ResolveClientUrl("../../../../Scripts/jquery-1.4.2.min.js")%>"></script>
    <script type="text/javascript" src="<%= ResolveClientUrl("../../../../Scripts/jquery-ui-1.8.5.custom.min.js")%>"></script>
    <script type="text/javascript" src="<%= ResolveClientUrl("../../../../Scripts/hoverIntent.js")%>"></script>
    <script type="text/javascript" src="<%= ResolveClientUrl("../../../../Scripts/superfish.js")%>"></script>
    <script type="text/javascript" src="<%= ResolveClientUrl("../../../../Scripts/ValidateInputs.js")%>"></script>
    <script type="text/javascript">
        $(document).ready(function () {

            $('#<%=lblDiscrepancyNo.ClientID %>').hide();
            $('#<%=txtDiscreapncy.ClientID %>').hide();
            $('#<%=lblGIN0015Text8.ClientID %>').hide();
            $('#<%=FileUpload1.ClientID %>').hide();
            $('#<%=lblGIN0015Text13.ClientID %>').hide();
            $('#<%=txtNAX.ClientID %>').hide();
            // $('#<%=txtComment.ClientID %>').val("");

            $('#<%=btnSave.ClientID%>').click(function () {
                if ($('#<%=txtComment.ClientID%>').val() == '') {
                    alert('<%=CommentRequired %>');
                    return false;
                }
            });

            if ($('#<%=chkGIN0015Text1.ClientID %>').is(":checked")) {
                $('#<%=lblDiscrepancyNo.ClientID %>').show();
                $('#<%=txtDiscreapncy.ClientID %>').show();
            }

            if ($('#<%=chkGIN0015Text3.ClientID %>').is(":checked")) {
                $('#<%=lblGIN0015Text8.ClientID %>').show();
                $('#<%=FileUpload1.ClientID %>').show();
            }

            if ($('#<%=rdoODFigure.ClientID %>').is(":checked")) {

                $('#<%=trVendor.ClientID %>').hide();
                $('#<%=trOD.ClientID %>').show();
            }
            else {
                $('#<%=trVendor.ClientID %>').show();
                $('#<%=trOD.ClientID %>').hide();
            }
            $('#<%=rdoVendorFigure.ClientID %>').click(function () {
                $('#<%=trVendor.ClientID %>').show();
                $('#<%=trOD.ClientID %>').hide();
                $('#<%=txtComment.ClientID %>').val("");
            });

            $('#<%=rdoODFigure.ClientID %>').click(function () {
                $('#<%=trVendor.ClientID %>').hide();
                $('#<%=trOD.ClientID %>').show();
                $('#<%=txtComment.ClientID %>').val("");
            });

            $('#<%=rdoGIN0015Text11.ClientID %>').click(function () {
                $('#<%=lblGIN0015Text13.ClientID %>').show();
                $('#<%=txtNAX.ClientID %>').show();
            });

            $('#<%=rdoGIN0015Text10.ClientID %>').click(function () {
                $('#<%=lblGIN0015Text13.ClientID %>').hide();
                $('#<%=txtNAX.ClientID %>').hide();
            });
            $('#<%=rdoGIN0015Text12.ClientID %>').click(function () {
                $('#<%=lblGIN0015Text13.ClientID %>').hide();
                $('#<%=txtNAX.ClientID %>').hide();
            });
            $('#<%=rdoGIN0015Text11.ClientID %>').click(function () {
                $('#<%=lblGIN0015Text13.ClientID %>').show();
                $('#<%=txtNAX.ClientID %>').show();
            });

            $('#<%=rdoOther.ClientID %>').click(function () {
                $('#<%=lblGIN0015Text13.ClientID %>').hide();
                $('#<%=txtNAX.ClientID %>').hide();
            });

            $('#<%=chkGIN0015Text1.ClientID %>').click(function () {
                if ($(this).is(":checked")) {
                    $('#<%=lblDiscrepancyNo.ClientID %>').show();
                    $('#<%=txtDiscreapncy.ClientID %>').show();
                }
                else if ($(this).is(":not(:checked)")) {
                    $('#<%=lblDiscrepancyNo.ClientID %>').hide();
                    $('#<%=txtDiscreapncy.ClientID %>').hide();
                }
            });

            $('#<%=chkGIN0015Text3.ClientID %>').click(function () {
                if ($(this).is(":checked")) {
                    $('#<%=lblGIN0015Text8.ClientID %>').show();
                    $('#<%=FileUpload1.ClientID %>').show();
                }
                else if ($(this).is(":not(:checked)") && $('#<%=chkGIN0015Text2.ClientID %>')["0"].checked==false) {                   
                    $('#<%=lblGIN0015Text8.ClientID %>').hide();
                    $('#<%=FileUpload1.ClientID %>').hide();
                }
            });

            $('#<%=chkGIN0015Text2.ClientID %>').click(function () {
                if ($(this).is(":checked")) {
                    $('#<%=lblGIN0015Text8.ClientID %>').show();
                    $('#<%=FileUpload1.ClientID %>').show();
                }
                else if ($(this).is(":not(:checked)") && $('#<%=chkGIN0015Text3.ClientID %>')["0"].checked == false) {
                    $('#<%=lblGIN0015Text8.ClientID %>').hide();
                    $('#<%=FileUpload1.ClientID %>').hide();
                }
            });

        });
        function HideShowButton() {
            if (checkValidationGroup("a")) {
                document.getElementById('<%=btnSave.ClientID%>').style.visibility = "hidden";
            }
            else {
                document.getElementById('<%=btnSave.ClientID%>').style.visibility = "visible";
                return false;
            }
        }
    </script>
</head>
<body>
    <form id="form2" runat="server" method="post">
    <cc1:ucPanel ID="pnlActionRequired" runat="server" Width="100%" CssClass="fieldset-form"
        BorderColor="gray" BorderStyle="Solid" BorderWidth="0" GroupingText="Action Required">
        <table class="form-table" cellpadding="3" cellspacing="4" width="96%" style="margin: 2px;">
            <tr style="padding: 15px; margin: 15px">
                <td style="font-weight: bold;">
                    <cc1:ucLabel ID="lblGIN0015" runat="server" Text="There is a quantity mismatch between what the Vendor is claiming was delivered and what is being invoiced.
Please check the attached paperwork against our records and arrange a stock check if required" CssClass="action-required-heading"
                        Style="text-align: center;"></cc1:ucLabel>
                </td>
            </tr>
            <tr>
                <td>
                    <cc1:ucRadioButton ID="rdoVendorFigure" runat="server" GroupName="Figure" Text="Vendor figures are correct" />
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <cc1:ucRadioButton ID="rdoODFigure" runat="server" GroupName="Figure" Text="Office Depot's figures are correct"
                        Checked="true" />
                </td>
            </tr>
            <caption>
                <br />
                <tr>
                    <td>
                        <table id="trOD" runat="server" cellpadding="2" cellspacing="5" class="form-table"
                            width="98%">
                            <tr>
                                <td>
                                    <cc1:ucLabel ID="lblGIN0015Text" runat="server" Text="Please select what actions have been to taken during this investigation"></cc1:ucLabel>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <cc1:ucCheckbox ID="chkGIN0015Text1" runat="server" Text="Discrepancy was raised" />
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    <cc1:ucLabel ID="lblDiscrepancyNo" runat="server" Text="Discrepancy #"></cc1:ucLabel>
                                    &nbsp;&nbsp;&nbsp;&nbsp;
                                    <cc1:ucTextbox ID="txtDiscreapncy" runat="server"></cc1:ucTextbox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <cc1:ucCheckbox ID="chkGIN0015Text2" runat="server" Text="Delivery Note quantities checked" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <cc1:ucCheckbox ID="chkGIN0015Text3" runat="server" Text="Delivery Note invalid" />
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    <cc1:ucLabel ID="lblGIN0015Text8" runat="server" Text="Attach Paperwork"></cc1:ucLabel>
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    <asp:FileUpload ID="FileUpload1" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <cc1:ucLabel ID="lblGIN0015Text4" runat="server" Text="In addition to the above, where relevant the below checks have also taken place"></cc1:ucLabel>
                                    :
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <cc1:ucCheckbox ID="chkGIN0015Text5" runat="server" Text="Physical inventory check carried out" />
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    <cc1:ucCheckbox ID="chkGIN0015Text6" runat="server" Text="Host systems checked for differences" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table id="trVendor" runat="server" cellpadding="0" cellspacing="0" class="form-table"
                            width="98%">
                            <tr>
                                <td>
                                    <cc1:ucLabel ID="lblGIN0015Text9" runat="server" Text="Issue was down to"></cc1:ucLabel>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <cc1:ucRadioButton ID="rdoGIN0015Text10" runat="server" GroupName="Figure2" Text="Receiving Error - Stock found, now" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <cc1:ucRadioButton ID="rdoGIN0015Text11" runat="server" GroupName="Figure2" Text="Booked in as NAX/other PO" />
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    <cc1:ucLabel ID="lblGIN0015Text13" runat="server" Text="NAX/PO #"></cc1:ucLabel>
                                    &nbsp;&nbsp;&nbsp;&nbsp;
                                    <cc1:ucTextbox ID="txtNAX" runat="server"></cc1:ucTextbox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <cc1:ucRadioButton ID="rdoGIN0015Text12" runat="server" GroupName="Figure2" Text="Receiving Error - Received but warehouse" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <cc1:ucRadioButton ID="rdoOther" runat="server" GroupName="Figure2" Text="Other" />
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 100%">
                                    <cc1:ucLabel ID="lblComment" runat="server" Text="Please enter comments"></cc1:ucLabel>
                                    :
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </caption>
            <tr>
                <td>
                    <cc1:ucTextbox ID="txtComment" runat="server" CssClass="textarea" MaxLength="1000"
                        TextMode="MultiLine" Width="99%"></cc1:ucTextbox>
                    <%--<asp:RequiredFieldValidator ID="rfvCommentRequired" runat="server" ControlToValidate="txtComment"
                        Display="None" Text="Please enter comments." ValidationGroup="a"></asp:RequiredFieldValidator>--%>
                </td>
            </tr>
            <tr>
                <td>
                    <table class="form-table" cellpadding="0" cellspacing="0" width="98%">
                        <tr>
                            <td width="25%" style="display: none;">
                                <cc1:ucLabel ID="lblStageCompleted" runat="server" Text="Stage Completed"></cc1:ucLabel>
                                <asp:ValidationSummary ID="vs1" runat="server" ValidationGroup="a" ShowSummary="false"
                                    ShowMessageBox="true" />
                            </td>
                            <td width="1%" style="display: none;">
                                :
                            </td>
                            <td width="64%" align="left" style="display: none;">
                                <cc1:ucCheckbox ID="chkStageCompleted" runat="server" Checked="true" />
                            </td>
                            <td width="10%" style="display: none;">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" style="display: none;">
                                <cc1:ucLabel ID="lblDiscrepancyNote" runat="server" Text="If  you required to tag a comment without closing the discrepancy<br/>1 -    Deselect Stage Complete<br/>2 -   Enter  the comment<br/>3 -   Save"></cc1:ucLabel>
                            </td>
                            <td align="right" valign="bottom">
                                <cc1:ucButton ID="btnSave" runat="server" Text="Save" class="button" ValidationGroup="a"
                                    OnClientClick="javascript:HideShowButton()" OnClick="btnSave_Click" />
                                <cc1:ucButton ID="btnBack" runat="server" Text="Back" class="button" OnClick="btnBack_Click" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </cc1:ucPanel>
    </form>
</body>
</html>

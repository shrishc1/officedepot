﻿using System;
using System.Data;
using BusinessEntities.ModuleBE.Discrepancy.LogDiscrepancy;
using BusinessLogicLayer.ModuleBAL.Discrepancy.LogDiscrepancy;

public partial class GINAction_GIN013 : CommonPage
{
    #region Global variables
    #endregion

    #region Events

    protected void Page_Load(object sender, EventArgs e)
    {
            txtSPComment.Text = this.GetVendorComment();
            txtSPComment.ReadOnly = true;
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        btnSave.Enabled = false;
        DISLog_WorkFlowBE oDISLog_WorkFlowBE = new DISLog_WorkFlowBE();
        DISLog_WorkFlowBAL oDISLog_WorkFlowBAL = new DISLog_WorkFlowBAL();

        if (GetQueryStringValue("disLogID") != null && GetQueryStringValue("disLogID").ToString() != "")
        {
            //**********************************************************
            string Location = hdfLocation.Value;
            DiscrepancyBE oDiscrepancyBE1 = new DiscrepancyBE();
            DiscrepancyBAL oDiscrepancyBAL1 = new DiscrepancyBAL();
            oDiscrepancyBE1.Action = "UpdateLocation";
            oDiscrepancyBE1.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
            oDiscrepancyBE1.Location = Location;
            oDiscrepancyBE1.UserID = Convert.ToInt32(Session["UserID"]);
            string sResult1 = oDiscrepancyBAL1.updateDiscrepancyLocationBAL(oDiscrepancyBE1);
            //**********************************************************
            oDISLog_WorkFlowBE.Action = "InsertUpdateWorkflowActions";
            oDISLog_WorkFlowBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
            oDISLog_WorkFlowBE.StageCompletedForWhom = "GIN013_StageCompleted";
            oDISLog_WorkFlowBE.Comment = txtComments.Text;

            //update the work flow action table each time for the comments and other fields( if stage is completed)
            oDISLog_WorkFlowBE.DiscrepancyWorkFlowID = Convert.ToInt32(GetQueryStringValue("DisWFlowID").ToString());

            //insert or update the record according to the stage completed status
            int? iResult = oDISLog_WorkFlowBAL.addWorkFlowActionsBAL(oDISLog_WorkFlowBE);

            //now insert the work flow HTML according to the action taken only if the stage is completed
            DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
            DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();
            WorkflowHTML oWorkflowHTML = new WorkflowHTML();

            oDiscrepancyBE.GINHTML = oWorkflowHTML.function1(DateTime.Now, null, null, "Yellow", string.Empty,
                "Closing Discrepancy", 
                //Session["UserName"].ToString(), 
                UIUtility.GetODVendorUserName(Convert.ToString(Session["Role"]), Convert.ToString(Session["UserName"]), Convert.ToString(Session["LoginID"])),
                txtComments.Text.Trim());
            oDiscrepancyBE.GINActionRequired = false;

            oDiscrepancyBE.INVHTML = oWorkflowHTML.function3("white", null);
            oDiscrepancyBE.INVActionRequired = false;

            //oDISLog_WorkFlowBE.Action = "GetAccountPayable";
            //string VendorAccountPayablesEmail = oDISLog_WorkFlowBAL.GetVendorAccountPayablesEmail(oDISLog_WorkFlowBE);
            //oDiscrepancyBE.APHTML = oWorkflowHTML.function4("Blue", "", null, VendorAccountPayablesEmail, "", "Build on to Accounts Payable Work list on  " + DateTime.Now.ToString("dd/MM/yyyy") + " with following request", "Please credit/debit the account for the amount to cover labour costs", "", "", DateTime.Now, "");

            oDiscrepancyBE.APHTML = oWorkflowHTML.function3("white", null);
            oDiscrepancyBE.APActionRequired = false;

            oDiscrepancyBE.VENHTML = oWorkflowHTML.function3("White", null, "", "");
            oDiscrepancyBE.VenActionRequired = false;

            oDiscrepancyBE.Action = "InsertHTML";
            oDiscrepancyBE.CloseDiscrepancy = true;
            oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
            oDiscrepancyBE.LoggedDateTime = DateTime.Now;
            oDiscrepancyBE.LevelNumber = 1;
            int? iResult1 = oDiscrepancyBAL.addWorkFlowHTMLsBAL(oDiscrepancyBE);
        }

        //Redirecting to Previous Page
        RedirectToPage(GetQueryStringValue("PreviousPage"));
    }
    #endregion

    #region Methods
    private void InsertHTML()
    {
        DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
        DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();

        WorkflowHTML oWorkflowHTML = new WorkflowHTML();
        oDiscrepancyBE.GINHTML = oWorkflowHTML.function3("White", DateTime.Now, txtComments.Text.Trim(), "");
        oDiscrepancyBE.Action = "InsertHTML";
        oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
        oDiscrepancyBE.LoggedDateTime = DateTime.Now;
        oDiscrepancyBE.LevelNumber = 1;

        int? iResult = oDiscrepancyBAL.addWorkFlowHTMLsBAL(oDiscrepancyBE);
    }
    #endregion

    protected void btnBack_Click(object sender, EventArgs e)
    {
        //Redirecting to Previous Page
        RedirectToPage(GetQueryStringValue("PreviousPage"));
    }

    private string GetVendorComment()
    {
        string strVendorComment = string.Empty;
        DISLog_WorkFlowBE oDISLog_WorkFlowBE = new DISLog_WorkFlowBE();
        DISLog_WorkFlowBAL oDISLog_WorkFlowBAL = new DISLog_WorkFlowBAL();
        if (GetQueryStringValue("disLogID") != null && !string.IsNullOrEmpty(GetQueryStringValue("disLogID").ToString()))
        {
            oDISLog_WorkFlowBE.Action = "GetVendorCollectionData";
            oDISLog_WorkFlowBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID"));
            DataTable dtVendorCollData = oDISLog_WorkFlowBAL.GetVendorCollectionDataBAL(oDISLog_WorkFlowBE);
            if (dtVendorCollData != null && dtVendorCollData.Rows.Count > 0)
                if (dtVendorCollData.Rows[0]["INV012_PassBackToGoodsIn"] != null && dtVendorCollData.Rows[0]["Comment"] != null)
                    strVendorComment = Convert.ToString(dtVendorCollData.Rows[0]["Comment"]);
        }
        return strVendorComment;
    }
}
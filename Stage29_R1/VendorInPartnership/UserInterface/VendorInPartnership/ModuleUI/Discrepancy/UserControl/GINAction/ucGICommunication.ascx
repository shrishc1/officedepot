﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucGICommunication.ascx.cs"
    Inherits="ModuleUI_Discrepancy_UserControl_ucGICommunication" %>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<table width="100%" cellspacing="5" cellpadding="0" class="form-table">
    <tr>
        <td>
            <cc1:ucPanel ID="pnlGIN001" runat="server" Width="65%" CssClass="fieldset-form" BorderColor="gray" BorderStyle="Solid" BorderWidth="1">
                <table class="form-table" cellpadding="5" cellspacing="0" width="600"  style="margin:10px;">
                    <tr style="padding:15px; margin:15px">
                        <td colspan="2" style="font-weight: bold; background-color: Yellow">
                            <cc1:ucLabel ID="lblGIN001" runat="server" Text="Please confirm receipt of the over delivered item against Purchase Order xxxxxxxxxx "></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <cc1:ucLabel ID="lblComment" runat="server" Text="Comment"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <cc1:ucTextbox ID="txtComment" runat="server" onkeyup="checkTextLengthOnKeyUp(this,1000);" TextMode="MultiLine" Width="575px"
                                Height="100px"></cc1:ucTextbox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucButton ID="btnSave" runat="server" Text="Save" class="button" />
                        </td>
                        <td>
                            <cc1:ucCheckbox ID="chkStageCompleted" runat="server" /><cc1:ucLabel ID="lblStageCompleted"
                                runat="server" Text="Stage Completed"></cc1:ucLabel>
                        </td>
                    </tr>
                </table>
            </cc1:ucPanel>
            <cc1:ucPanel ID="pnlGIN002" runat="server" Width="65%" CssClass="fieldset-form" BorderColor="gray" BorderStyle="Solid" BorderWidth="1">
                <table class="form-table" cellpadding="5" cellspacing="0" width="600"  style="margin:10px;">
                    <tr>
                        <td colspan="2" style="font-weight: bold; background-color: Yellow">
                            <cc1:ucLabel ID="lblGIN002Desc" runat="server" Text="Collection has been organised by vendor. Please ensure the goods are collected as per the vendor's instruction "></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <table>
                                <tr>
                                    <td>
                                        <cc1:ucRadioButton ID="rbnGoodsCollected" runat="server" /><cc1:ucLabel ID="lblGoodsCollected"
                                            runat="server" Text="Goods Collected"></cc1:ucLabel>
                                    </td>
                                    <td>
                                        <cc1:ucRadioButton ID="rbnGoodsNotCollected" runat="server" /><cc1:ucLabel ID="lblGoodsNotCollected"
                                            runat="server" Text="Goods Not Collected"></cc1:ucLabel>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <table>
                                <tr>
                                    <td style="width: 70%">
                                        <cc1:ucLabel ID="lblCollectionDate" runat="server" Text="Arranged Collection Date"></cc1:ucLabel>
                                    </td>
                                    <td>
                                        <cc1:ucTextbox ID="txtCollectionDate" runat="server"></cc1:ucTextbox>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 70%">
                                        <cc1:ucLabel ID="lblCarrier" runat="server" Text="Carrier"></cc1:ucLabel>
                                    </td>
                                    <td>
                                        <cc1:ucTextbox ID="txtCarrier" runat="server"></cc1:ucTextbox>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 70%">
                                        <cc1:ucLabel ID="lblCollectionAuthNum" runat="server" Text="Collection Authorisation Number"></cc1:ucLabel>
                                    </td>
                                    <td>
                                        <cc1:ucTextbox ID="txtCollectionAuthNum" runat="server"></cc1:ucTextbox>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <cc1:ucLabel ID="lblGIN002Comment" runat="server" Text="Comment"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <cc1:ucTextbox ID="txtGIN002Comment" runat="server" onkeyup="checkTextLengthOnKeyUp(this,1000);" TextMode="MultiLine" Width="575px"
                                Height="100px"></cc1:ucTextbox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucButton ID="btnGIN002Save" runat="server" Text="Save" class="button" />
                        </td>
                        <td>
                            <cc1:ucCheckbox ID="chkGIN002StageCompleted" runat="server" /><cc1:ucLabel ID="lblGIN002StageCompleted"
                                runat="server" Text="Stage Completed"></cc1:ucLabel>
                        </td>
                    </tr>
                </table>
            </cc1:ucPanel>
            <cc1:ucPanel ID="pnlGIN003" runat="server" Width="65%" CssClass="fieldset-form" BorderColor="gray" BorderStyle="Solid" BorderWidth="1">
                <table class="form-table" cellpadding="5" cellspacing="0" width="600"  style="margin:10px;">
                    <tr>
                        <td colspan="2" style="font-weight: bold; background-color: Yellow">
                            <cc1:ucLabel ID="pnlGIN003Desc" runat="server" Text="Please arrange for goods to be returned to the vendor and confirm the carriage charge"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <table>
                                <tr>
                                    <td style="width: 70%">
                                        <cc1:ucLabel ID="lblCarrierSendingGoodsBack" runat="server" Text="Carrier sending goods back with"></cc1:ucLabel>
                                    </td>
                                    <td>
                                        <cc1:ucTextbox ID="txtCarrierSendingGoodsBack" runat="server"></cc1:ucTextbox>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 70%">
                                        <cc1:ucLabel ID="lblCarrierTrackingNum" runat="server" Text="Carrier Tracking Number"></cc1:ucLabel>
                                    </td>
                                    <td>
                                        <cc1:ucTextbox ID="txtCarrierTrackingNum" runat="server"></cc1:ucTextbox>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 70%">
                                        <cc1:ucLabel ID="lblCarriageCharge" runat="server" Text="Carriage charge to be applied"></cc1:ucLabel>
                                    </td>
                                    <td>
                                        <cc1:ucTextbox ID="txtCarriageCharge" runat="server"></cc1:ucTextbox>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 70%">
                                        <cc1:ucLabel ID="lblNumPalletsExchange" runat="server" Text="Number of Pallets Exchanged"></cc1:ucLabel>
                                    </td>
                                    <td>
                                        <cc1:ucTextbox ID="txtNumPalletsExchange" runat="server"></cc1:ucTextbox>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <cc1:ucLabel ID="lblGIN003Comment" runat="server" Text="Comment"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <cc1:ucTextbox ID="txtGIN003Comment" runat="server" onkeyup="checkTextLengthOnKeyUp(this,1000);" TextMode="MultiLine" Width="575px"
                                Height="100px"></cc1:ucTextbox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucButton ID="btnGIN003Save" runat="server" Text="Save" class="button" />
                        </td>
                        <td>
                            <cc1:ucCheckbox ID="chkGIN003StageCompleted" runat="server" /><cc1:ucLabel ID="lblGIN003StageCompleted"
                                runat="server" Text="Stage Completed"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                            <table>
                                <tr>
                                    <td>
                                        <cc1:ucLabel ID="lblGIN003Note" runat="server" Text="If  you required to tag a comment without closing the discrepancy<br/>1 -    Deslect Stage Complete<br/>2 -   Enter  the comment<br/>3 -   Save"></cc1:ucLabel>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </cc1:ucPanel>
            <cc1:ucPanel ID="pnlGIN004" runat="server" Width="65%" CssClass="fieldset-form" BorderColor="gray" BorderStyle="Solid" BorderWidth="1">
                <table class="form-table" cellpadding="5" cellspacing="0" width="600"  style="margin:10px;">
                    <tr>
                        <td colspan="2" style="font-weight: bold; background-color: Yellow">
                            <cc1:ucLabel ID="lblGIN004Desc" runat="server" Text="Vendor has informed that damaged goods should be scrapped. Please confirm goods have been disposed of."></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <cc1:ucLabel ID="lblGIN004Comment" runat="server" Text="Comment"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <cc1:ucTextbox ID="txtGIN004Comment" runat="server" onkeyup="checkTextLengthOnKeyUp(this,1000);" TextMode="MultiLine" Width="575px"
                                Height="100px"></cc1:ucTextbox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucButton ID="btnGIN004Save" runat="server" Text="Save" class="button" />
                        </td>
                        <td>
                            <cc1:ucCheckbox ID="chkGIN004StageCoompletd" runat="server" /><cc1:ucLabel ID="lblGIN004StageCompleted"
                                runat="server" Text="Stage Completed"></cc1:ucLabel>
                        </td>
                    </tr>
                </table>
            </cc1:ucPanel>
            <cc1:ucPanel ID="pnlGIN005" runat="server" Width="65%" CssClass="fieldset-form" BorderColor="gray" BorderStyle="Solid" BorderWidth="1">
                <table class="form-table" cellpadding="5" cellspacing="0" width="600"  style="margin:10px;">
                    <tr>
                        <td colspan="2" style="font-weight: bold; background-color: Yellow">
                            <cc1:ucLabel ID="lblGIN005Desc" runat="server" Text="Please confirm receipt of the delivered item against Purchase Order xxxxxxxxxx"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <cc1:ucLabel ID="lblGIN005Comment" runat="server" Text="Comment"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <cc1:ucTextbox ID="txtGIN005Comment" runat="server" onkeyup="checkTextLengthOnKeyUp(this,1000);" TextMode="MultiLine" Width="575px"
                                Height="100px"></cc1:ucTextbox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucButton ID="btnGIN005Save" runat="server" Text="Save" class="button" />
                        </td>
                        <td>
                            <cc1:ucCheckbox ID="chkGIN005StageCompleted" runat="server" /><cc1:ucLabel ID="lblGIN00StageCompleted"
                                runat="server" Text="Stage Completed"></cc1:ucLabel>
                        </td>
                    </tr>
                </table>
            </cc1:ucPanel>
            <cc1:ucPanel ID="pnlGIN006" runat="server" Width="65%" CssClass="fieldset-form" BorderColor="gray" BorderStyle="Solid" BorderWidth="1">
                <table class="form-table" cellpadding="5" cellspacing="0" width="600"  style="margin:10px;">
                    <tr>
                        <td colspan="2" style="font-weight: bold; background-color: Yellow">
                            <cc1:ucLabel ID="lblGIN006Desc" runat="server" Text="Items are correct, please confirm goods have been booked in against original Purchase Order"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <cc1:ucLabel ID="lblGIN006Comment" runat="server" Text="Comment"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <cc1:ucTextbox ID="txtGIN006Comment" runat="server" onkeyup="checkTextLengthOnKeyUp(this,1000);" TextMode="MultiLine" Width="575px"
                                Height="100px"></cc1:ucTextbox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucButton ID="btnGIN006Save" runat="server" Text="Save" class="button" />
                        </td>
                        <td>
                            <cc1:ucCheckbox ID="chkGIN006StageCompleted" runat="server" /><cc1:ucLabel ID="lblGIN006StageCompleted"
                                runat="server" Text="Stage Completed"></cc1:ucLabel>
                        </td>
                    </tr>
                </table>
            </cc1:ucPanel>
            <cc1:ucPanel ID="pnlGIN007" runat="server" Width="65%" CssClass="fieldset-form" BorderColor="gray" BorderStyle="Solid" BorderWidth="1">
                <table class="form-table" cellpadding="5" cellspacing="0" width="600"  style="margin:10px;">
                    <tr>
                        <td colspan="2" style="font-weight: bold; background-color: Yellow">
                            <cc1:ucLabel ID="lblGIN007Desc" runat="server" Text="Unexpected item has been delivered. Please confirm booking in against Purchase Order xxxxxxx"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <cc1:ucLabel ID="lblGIN007Comment" runat="server" Text="Comment"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <cc1:ucTextbox ID="txtGIN007Comment" runat="server" onkeyup="checkTextLengthOnKeyUp(this,1000);" TextMode="MultiLine" Width="575px"
                                Height="100px"></cc1:ucTextbox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucButton ID="btnGIN007Save" runat="server" Text="Save" class="button" />
                        </td>
                        <td>
                            <cc1:ucCheckbox ID="chkGIN007StageCompleted" runat="server" /><cc1:ucLabel ID="lblGIN007StageCompleted"
                                runat="server" Text="Stage Completed"></cc1:ucLabel>
                        </td>
                    </tr>
                </table>
            </cc1:ucPanel>
            <cc1:ucPanel ID="pnlGIN008" runat="server" Width="65%" CssClass="fieldset-form" BorderColor="gray" BorderStyle="Solid" BorderWidth="1">
                <table class="form-table" cellpadding="5" cellspacing="0" width="600"  style="margin:10px;">
                    <tr>
                        <td colspan="2" style="font-weight: bold; background-color: Yellow">
                            <cc1:ucLabel ID="lblGIN008Desc" runat="server" Text="Items correct, do not rework.<br/>
                            Please confirm receipt of the delivered item against Purchase Order "></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <cc1:ucLabel ID="lblGIN008Comment" runat="server" Text="Comment"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <cc1:ucTextbox ID="txtGIN008Comment" runat="server" onkeyup="checkTextLengthOnKeyUp(this,1000);" TextMode="MultiLine" Width="575px"
                                Height="100px"></cc1:ucTextbox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucButton ID="btnGIN008Save" runat="server" Text="Save" class="button" />
                        </td>
                        <td>
                            <cc1:ucCheckbox ID="chkGIN008StageCompleted" runat="server" /><cc1:ucLabel ID="lblGIN008StageCompleted"
                                runat="server" Text="Stage Completed"></cc1:ucLabel>
                        </td>
                    </tr>
                </table>
            </cc1:ucPanel>
            <cc1:ucPanel ID="pnlGIN009" runat="server" Width="65%" CssClass="fieldset-form" BorderColor="gray" BorderStyle="Solid" BorderWidth="1">
                <table class="form-table" cellpadding="5" cellspacing="0" width="600"  style="margin:10px;">
                    <tr>
                        <td colspan="2" style="font-weight: bold; background-color: Yellow">
                            <cc1:ucLabel ID="lblGIN009Desc" runat="server" Text="Please confirm the goods are booked in and rework to the correct pack size against Purchase Order"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <cc1:ucLabel ID="lblGIN009Comment" runat="server" Text="Comment"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <cc1:ucTextbox ID="txtGIN009Comment" runat="server" onkeyup="checkTextLengthOnKeyUp(this,1000);" TextMode="MultiLine" Width="575px"
                                Height="100px"></cc1:ucTextbox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucButton ID="btnGIN009Save" runat="server" Text="Save" class="button" />
                        </td>
                        <td>
                            <cc1:ucCheckbox ID="chkGIN009StageCompleted" runat="server" /><cc1:ucLabel ID="lblGIN009StageCompleted"
                                runat="server" Text="Stage Completed"></cc1:ucLabel>
                        </td>
                    </tr>
                </table>
            </cc1:ucPanel>
            <cc1:ucPanel ID="pnlGIN010" runat="server" Width="65%" CssClass="fieldset-form" BorderColor="gray" BorderStyle="Solid" BorderWidth="1">
                <table class="form-table" cellpadding="5" cellspacing="0" width="600"  style="margin:10px;">
                    <tr>
                        <td colspan="2" style="font-weight: bold; background-color: Yellow">
                            <cc1:ucLabel ID="lblGIN010Desc" runat="server" Text="Please confirm the goods are booked in and rework to the correct pack size against Purchase Order. Add cost of this additional work."></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <table>
                                <tr>
                                    <td style="width: 50%">
                                        <cc1:ucLabel ID="lblGIN010CostofLabour" runat="server" Text="Cost of Labour"></cc1:ucLabel>
                                    </td>
                                    <td>
                                        <cc1:ucTextbox ID="txtGIN010CostofLabour" runat="server"></cc1:ucTextbox>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <cc1:ucLabel ID="lblGIN010Comment" runat="server" Text="Comment"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <cc1:ucTextbox ID="txtGIN010Comment" runat="server" onkeyup="checkTextLengthOnKeyUp(this,1000);" TextMode="MultiLine" Width="575px"
                                Height="100px"></cc1:ucTextbox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucButton ID="btnGIN010Save" runat="server" Text="Save" class="button" />
                        </td>
                        <td>
                            <cc1:ucCheckbox ID="chkGIN010StageCompleted" runat="server" /><cc1:ucLabel ID="lblGIN010StageCompleted"
                                runat="server" Text="Stage Completed"></cc1:ucLabel>
                        </td>
                    </tr>
                </table>
            </cc1:ucPanel>
            <cc1:ucPanel ID="pnlGIN011" runat="server" Width="65%" CssClass="fieldset-form" BorderColor="gray" BorderStyle="Solid" BorderWidth="1">
                <table class="form-table" cellpadding="5" cellspacing="0" width="600"  style="margin:10px;">
                    <tr>
                        <td colspan="2" style="font-weight: bold; background-color: Yellow">
                            <cc1:ucLabel ID="lblGIN011Desc" runat="server" Text="Please confirm the goods are booked in and rework to the correct pack size against Purchase Order"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <cc1:ucLabel ID="lblGIN011Comment" runat="server" Text="Comment"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <cc1:ucTextbox ID="txtGIN011Comment" runat="server" onkeyup="checkTextLengthOnKeyUp(this,1000);" TextMode="MultiLine" Width="575px"
                                Height="100px"></cc1:ucTextbox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucButton ID="btnGIN011Save" runat="server" Text="Save" class="button" />
                        </td>
                        <td>
                            <cc1:ucCheckbox ID="chkGIN011StageCompleted" runat="server" /><cc1:ucLabel ID="lblGIN011StageCompleted"
                                runat="server" Text="Stage Completed"></cc1:ucLabel>
                        </td>
                    </tr>
                </table>
            </cc1:ucPanel>
            <cc1:ucPanel ID="pnlGIN012" runat="server" Width="65%" CssClass="fieldset-form" BorderColor="gray" BorderStyle="Solid" BorderWidth="1">
                <table class="form-table" cellpadding="5" cellspacing="0"  width="600"  style="margin:10px;">
                    <tr>
                        <td colspan="2" style="font-weight: bold; background-color: Yellow">
                            <cc1:ucLabel ID="lblGIN012Desc" runat="server" Text="Please confirm the goods are booked in and reworked. Add cost of this additional work"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <table>
                                <tr>
                                    <td style="width: 50%">
                                        <cc1:ucLabel ID="lblGIN012CostofLabour" runat="server" Text="Cost of Labour"></cc1:ucLabel>
                                    </td>
                                    <td>
                                        <cc1:ucTextbox ID="txtGIN012CostofLabour" runat="server"></cc1:ucTextbox>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <cc1:ucLabel ID="lblGIN012Comment" runat="server" Text="Comment"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <cc1:ucTextbox ID="txtGIN012Comment" runat="server" onkeyup="checkTextLengthOnKeyUp(this,1000);" TextMode="MultiLine" Width="575px"
                                Height="100px"></cc1:ucTextbox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucButton ID="btnGIN012Save" runat="server" Text="Save" class="button" />
                        </td>
                        <td>
                            <cc1:ucCheckbox ID="chkGIN012StageCompleted" runat="server" /><cc1:ucLabel ID="lblGIN012StageCompleted"
                                runat="server" Text="Stage Completed"></cc1:ucLabel>
                        </td>
                    </tr>
                </table>
            </cc1:ucPanel>
        </td>
    </tr>
</table>

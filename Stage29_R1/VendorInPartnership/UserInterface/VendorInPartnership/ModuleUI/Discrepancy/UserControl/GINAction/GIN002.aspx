﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="GIN002.aspx.cs" Inherits="GINAction_GIN002" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Office Depot</title>
    <link rel="stylesheet" type="text/css" href="../../../../Css/style.css" />
    <link type="text/css" href="../../../../Css/ui-lightness/jquery-ui-1.8.5.custom.css"
        rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="../../../../Css/superfish.css" media="screen" />
    <script type="text/javascript" src="<%= ResolveClientUrl("../../../../Scripts/jquery-1.2.6.min.js")%>"></script>
    <script type="text/javascript" src="<%= ResolveClientUrl("../../../../Scripts/jquery-1.4.2.min.js")%>"></script>
    <script type="text/javascript" src="<%= ResolveClientUrl("../../../../Scripts/jquery-ui-1.8.5.custom.min.js")%>"></script>
    <script type="text/javascript" src="<%= ResolveClientUrl("../../../../Scripts/hoverIntent.js")%>"></script>
    <script type="text/javascript" src="<%= ResolveClientUrl("../../../../Scripts/superfish.js")%>"></script>
    <script type="text/javascript" src="<%= ResolveClientUrl("../../../../Scripts/ValidateInputs.js")%>"></script>

    <script type="text/javascript">
        $(document).ready(function () {

            $('#<%=btnSave.ClientID%>').click(function () {
                if ($('#<%=txtGIN002Comment.ClientID%>').val() == '') {
                    alert('<%=CommentRequired %>');
                    document.getElementById('<%=btnSave.ClientID%>').style.visibility = "visible";
                    return false;
                }
            });
        });
      </script>
    <script type="text/javascript">
        function SetStageCompleted(args) {
            if (args == '1') {
                document.getElementById('<%=chkGIN002StageCompleted.ClientID %>').checked = true;
            }
            else if (args == '2') {
                document.getElementById('<%=chkGIN002StageCompleted.ClientID %>').checked = false;
            }
        }
        $(function () {
            $('#txtCollectionDate').datepicker({ showOtherMonths: true,
                selectOtherMonths: true, changeMonth: true, changeYear: true,
                minDate: new Date(2013, 0, 1),
                yearRange: '2013:+100',
                dateFormat: 'dd/mm/yy'
            });
        });
        function CollectionStatusRequired(sender, args) {
            var objrdo1 = document.getElementById('<%=rbnGoodsCollected.ClientID %>');
            var objrdo2 = document.getElementById('<%=rbnGoodsNotCollected.ClientID %>');
            if (objrdo1.checked == false && objrdo2.checked == false) {
                args.IsValid = false;
            }
        }

        function GetLocation() {
            //debugger;
            var Location = parent.document.getElementById('ctl00_ContentPlaceHolder1_txtLocation').value;
            document.getElementById('<%=hdfLocation.ClientID%>').value = Location;
            if (checkValidationGroup("a")) {
                document.getElementById('<%=btnSave.ClientID%>').style.visibility = "hidden";
            }
            else {
                document.getElementById('<%=btnSave.ClientID%>').style.visibility = "visible";
                return false;
            }
        }

        function AssignCostCenter() {           
           // debugger;
            var CostCenter = parent.document.getElementById('ctl00_ContentPlaceHolder1_hdnCostCenter');
            document.getElementById('<%=txtCostCenter.ClientID%>').value = CostCenter.value;
        }

    </script>
     
</head>
<body onload="AssignCostCenter()">
    <form id="form1" runat="server">
    <div>
        <cc1:ucPanel ID="pnlActionRequired" runat="server" Width="100%" CssClass="fieldset-form"
            BorderColor="gray" BorderStyle="Solid" BorderWidth="0" GroupingText="Action Required">
            <table class="form-table" cellpadding="0" cellspacing="0" width="98%" style="margin: 2px;">
                <tr>
                    <td style="font-weight: bold;"><asp:HiddenField ID="hdfLocation" runat="server" />
                        <cc1:ucLabel ID="lblGIN002Desc" runat="server" CssClass="action-required-heading"
                            Style="text-align: center;" Text="Collection has been organised by vendor. Please ensure the goods are collected as per the vendor's instruction "></cc1:ucLabel>
                        <input type="hidden" id="hdnCarrierID" runat="server" value="" />
                    </td>
                </tr>
                <tr style="display: none">
                    <td>
                        <table class="form-table" cellpadding="0" cellspacing="0" width="98%" style="margin: 2px;">
                            <tr>
                                <td width="19%">
                                    <cc1:ucLabel ID="lblCollectionStatus" runat="server" Text="Collection Status"></cc1:ucLabel>
                                </td>
                                <td width="1%">
                                    :
                                </td>
                                <td width="20%" align="left">
                                    <cc1:ucRadioButton ID="rbnGoodsCollected" runat="server" GroupName="IsGoodsCollected"
                                        onclick='javascript:return SetStageCompleted(1);' Checked="True" />
                                    <cc1:ucLabel ID="lblGoodsCollected" runat="server" Text="Goods Collected"></cc1:ucLabel>
                                </td>
                                <td width="60%">
                                    <cc1:ucRadioButton ID="rbnGoodsNotCollected" runat="server" GroupName="IsGoodsCollected"
                                        onclick='javascript:return SetStageCompleted(1);' />
                                    <cc1:ucLabel ID="lblGoodsNotCollected" runat="server" Text="Goods Not Collected"></cc1:ucLabel>
                                    <asp:CustomValidator ID="cusvCollectionStatusRequired" runat="server" Text="Please select collection status"
                                        ClientValidationFunction="CollectionStatusRequired" Display="None">
                                    </asp:CustomValidator>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table class="form-table" cellpadding="0" cellspacing="0" width="98%" style="margin: 2px;">
                            <tr>
                                <td width="24%">
                                    <cc1:ucLabel ID="lblCollectionDate" runat="server" Text="Arranged Collection Date"></cc1:ucLabel>
                                </td>
                                <td width="1%">
                                    :
                                </td>
                                <td width="75%">
                                    <asp:TextBox ID="txtCollectionDate" runat="server" ClientIDMode="Static" class="date"
                                        Width="65px" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <cc1:ucLabel ID="lblCarrier" runat="server" Text="Carrier"></cc1:ucLabel>
                                </td>
                                <td>
                                    :
                                </td>
                                <td>
                                    <cc1:ucDropdownList ID="ucTxtCarrierWhoCollectGoods" runat="server">
                                    </cc1:ucDropdownList>
                                    <asp:RequiredFieldValidator ID="rfvCarrierWhoCollectGoodsRequired" runat="server"
                                        InitialValue="0" ControlToValidate="ucTxtCarrierWhoCollectGoods" Display="None"
                                        ValidationGroup="Save" ErrorMessage="Please enter the carrier who collects the goods.">
                                    </asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <cc1:ucLabel ID="lblCollectionAuthNum" runat="server" Text="Collection Authorisation #"></cc1:ucLabel>
                                </td>
                                <td>
                                    :
                                </td>
                                <td>
                                    <cc1:ucTextbox ID="txtCollectionAuthNum" runat="server" ReadOnly="true"></cc1:ucTextbox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <cc1:ucLabel ID="lblNumPalletsExchange" runat="server" Text="Number of Pallets Exchanged"></cc1:ucLabel>
                                </td>
                                <td width="1%">
                                    :
                                </td>
                                <td>
                                    <cc1:ucTextbox ID="txtNumPalletsExchange" runat="server" Text="0" onkeyup="AllowNumbersOnly(this)"></cc1:ucTextbox>
                                    <asp:RequiredFieldValidator ID="rfvNumPalletsExchangeRequired" runat="server" ControlToValidate="txtNumPalletsExchange"
                                        Display="None" ValidationGroup="a" ErrorMessage="Please enter the number of pallets exchanged.">
                                    </asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <cc1:ucLabel ID="lblNumCartonsExchange" runat="server" Text="Number of Cartons Exchanged"></cc1:ucLabel>
                                </td>
                                <td width="1%">
                                    :
                                </td>
                                <td>
                                    <cc1:ucTextbox ID="txtNumCartonsExchange" runat="server" Text="0" onkeyup="AllowNumbersOnly(this)"></cc1:ucTextbox>
                                </td>
                            </tr>
                            <tr>
                            <td>
                                <cc1:ucLabel ID="lblCostCenter" runat="server" Text="Cost Centre"></cc1:ucLabel>
                            </td>
                            <td width="1%">
                                :
                            </td>
                            <td>
                                <cc1:ucTextbox ID="txtCostCenter" runat="server"></cc1:ucTextbox>
                            </td>
                        </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table class="form-table" cellpadding="0" cellspacing="0" width="98%" style="margin: 2px;">
                            <tr>
                                <td width="7%">
                                    <cc1:ucLabel ID="lblCommentLabel" runat="server" Text="Comment"></cc1:ucLabel>
                                </td>
                                <td width="93%">
                                    :
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <cc1:ucTextbox ID="txtGIN002Comment" runat="server" onkeyup="checkTextLengthOnKeyUp(this,1000);"
                                        TextMode="MultiLine" CssClass="textarea" Width="98%"></cc1:ucTextbox>
                                    <asp:RequiredFieldValidator ID="rfvCommentRequired" runat="server" Text="Please enter comments."
                                        ControlToValidate="txtGIN002Comment" Display="None" ValidationGroup="Save"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table class="form-table" cellpadding="0" cellspacing="0" width="98%" style="margin: 2px;">
                            <tr>
                                <td width="24%" >
                                    <cc1:ucLabel ID="lblStageCompleted" runat="server" Text="Stage Completed"></cc1:ucLabel>
                                    <asp:ValidationSummary ID="vs1" runat="server" ValidationGroup="a" ShowSummary="false"
                                        ShowMessageBox="true" />
                                </td>
                                <td width="1%" >
                                    :
                                </td>
                                <td width="20%" align="left" >
                                    <cc1:ucCheckbox ID="chkGIN002StageCompleted" runat="server" Checked="true" />
                                </td>
                                <td width="55%" >
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3" >
                                    <cc1:ucLabel ID="lblDiscrepancyNote" runat="server" Text="If  you required to tag a comment without closing the discrepancy<br/>1 -    Deselect Stage Complete<br/>2 -   Enter  the comment<br/>3 -   Save"></cc1:ucLabel>
                                </td>
                                <td align="right" valign="bottom">
                                    <cc1:ucButton ID="btnSave" runat="server" Text="Save" class="button" ValidationGroup="a"
                                        OnClick="btnSave_Click" OnClientClick="javascript:GetLocation()" />
                                    <cc1:ucButton ID="btnBack" runat="server" Text="Back" class="button" OnClick="btnBack_Click" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </cc1:ucPanel>
    </div>
    </form>
</body>
</html>

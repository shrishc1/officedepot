﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="GIN013.aspx.cs" Inherits="GINAction_GIN013" %>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Office Depot</title>
    <link rel="stylesheet" type="text/css" href="../../../../Css/style.css" />
    <link type="text/css" href="../../../../Css/ui-lightness/jquery-ui-1.8.5.custom.css"
        rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="../../../../Css/superfish.css" media="screen" />
    <script type="text/javascript" src="<%= ResolveClientUrl("../../../../Scripts/jquery-1.2.6.min.js")%>"></script>
    <script type="text/javascript" src="<%= ResolveClientUrl("../../../../Scripts/jquery-1.4.2.min.js")%>"></script>
    <script type="text/javascript" src="<%= ResolveClientUrl("../../../../Scripts/jquery-ui-1.8.5.custom.min.js")%>"></script>
    <script type="text/javascript" src="<%= ResolveClientUrl("../../../../Scripts/hoverIntent.js")%>"></script>
    <script type="text/javascript" src="<%= ResolveClientUrl("../../../../Scripts/superfish.js")%>"></script>
    <script type="text/javascript" src="<%= ResolveClientUrl("../../../../Scripts/ValidateInputs.js")%>"></script>
    <script type="text/javascript">
        function GetLocation() {
            var Location = parent.document.getElementById('ctl00_ContentPlaceHolder1_txtLocation').value;
            document.getElementById('<%=hdfLocation.ClientID%>').value = Location;
            if (checkValidationGroup("a")) {
                document.getElementById('<%=btnSave.ClientID%>').style.visibility = "hidden";
            }
            else {
                document.getElementById('<%=btnSave.ClientID%>').style.visibility = "visible";
                return false;
            }
        }
    </script>
</head>
<body>
   <form id="form2" runat="server" method="post">
   <cc1:ucPanel ID="pnlReply" runat="server" Width="100%" GroupingText="Reply" CssClass="fieldset-form" BorderColor="gray" BorderStyle="Solid" BorderWidth="0">
        <table class="form-table" cellpadding="0" cellspacing="0" width="98%" style="margin: 2px;">
            <tr>
                <td style="font-weight: bold;"><asp:HiddenField ID="hdfLocation" runat="server" />
                    <cc1:ucTextbox ID="txtSPComment" runat="server" TextMode="MultiLine" Width="98%"></cc1:ucTextbox>
                </td>
            </tr>
            </table>
    </cc1:ucPanel>
    <cc1:ucPanel ID="pnlActionRequired" runat="server" Width="100%" GroupingText="Action Required"
        CssClass="fieldset-form" BorderColor="gray" BorderStyle="Solid" BorderWidth="0">
        <table class="form-table" cellpadding="0" cellspacing="0" width="98%" style="margin: 2px;">
            <tr>
                <td style="font-weight: bold;">
                    <cc1:ucLabel ID="GIN013Action" runat="server" CssClass="action-required-heading"
                        Style="text-align: left;" Text="Inventory have responded to this discrepancy. Please review their action and provide comments to close discrepancy"></cc1:ucLabel>
                </td>
            </tr>
            <tr>
                <td>
                    <table class="form-table" cellpadding="0" cellspacing="0" width="98%" style="margin: 2px;">
                        <tr>
                            <td width="20%">
                                <cc1:ucLabel ID="lblCommentRequired" runat="server" Text="Please enter comments"></cc1:ucLabel>
                            </td>
                            <td width="80%">
                                :
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <cc1:ucTextbox ID="txtComments" runat="server" onkeyup="checkTextLengthOnKeyUp(this,1000);"
                                    TextMode="MultiLine" CssClass="textarea" Width="98%"></cc1:ucTextbox>
                                <asp:RequiredFieldValidator ID="rfvCommentRequired" runat="server" Text="Please enter comments."
                                    ControlToValidate="txtComments" Display="None" ValidationGroup="a">
                                </asp:RequiredFieldValidator>
                            </td>                          
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table class="form-table" cellpadding="0" cellspacing="0" width="98%" style="margin: 2px;">
                        <tr>
                            <td width="80%">
                                <asp:ValidationSummary ID="vs1" runat="server" ValidationGroup="a" ShowSummary="false"
                                    ShowMessageBox="true" />
                            </td>
                            <td align="right" valign="bottom">
                                <cc1:ucButton ID="btnSave" runat="server" Text="Save" class="button" ValidationGroup="a"
                                    OnClick="btnSave_Click" OnClientClick="javascript:GetLocation()" />
                                <cc1:ucButton ID="btnBack" runat="server" Text="Back" class="button" OnClick="btnBack_Click" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </cc1:ucPanel>
    </form>
</body>
</html>

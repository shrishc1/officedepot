﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

using Utilities; using WebUtilities;
using BusinessEntities.ModuleBE.Discrepancy.LogDiscrepancy;
using BusinessLogicLayer.ModuleBAL.Discrepancy.LogDiscrepancy;
using BusinessEntities.ModuleBE.AdminFunctions;
using BusinessLogicLayer.ModuleBAL.AdminFunctions;

public partial class GINAction_GIN012 : CommonPage
{
    #region Global variables
    #endregion

    #region Events

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            chkGIN012StageCompleted.Checked = true;           
        }
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        btnSave.Enabled = false;
        DISLog_WorkFlowBE oDISLog_WorkFlowBE = new DISLog_WorkFlowBE();
        DISLog_WorkFlowBAL oDISLog_WorkFlowBAL = new DISLog_WorkFlowBAL();

        if (GetQueryStringValue("disLogID") != null && GetQueryStringValue("disLogID").ToString() != "")
        {
            //**********************************************************
            string Location = hdfLocation.Value;
            DiscrepancyBE oDiscrepancyBE1 = new DiscrepancyBE();
            DiscrepancyBAL oDiscrepancyBAL1 = new DiscrepancyBAL();
            oDiscrepancyBE1.Action = "UpdateLocation";
            oDiscrepancyBE1.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
            oDiscrepancyBE1.Location = Location;
            oDiscrepancyBE1.UserID = Convert.ToInt32(Session["UserID"]);
            string sResult1 = oDiscrepancyBAL1.updateDiscrepancyLocationBAL(oDiscrepancyBE1);
            //**********************************************************
            oDISLog_WorkFlowBE.Action = "InsertUpdateWorkflowActions";
            oDISLog_WorkFlowBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
            oDISLog_WorkFlowBE.StageCompleted = chkGIN012StageCompleted.Checked ? true : false;

          //  if (chkGIN012StageCompleted.Checked)
            //    oDISLog_WorkFlowBE.GIN012_LabourCost = Convert.ToDecimal(txtGIN012CostofLabour.Text.Trim());

            oDISLog_WorkFlowBE.StageCompletedForWhom = "GIN012_StageCompleted";
            oDISLog_WorkFlowBE.Comment = txtComments.Text;

            //update the work flow action table each time for the comments and other fields( if stage is completed)
            oDISLog_WorkFlowBE.DiscrepancyWorkFlowID = Convert.ToInt32(GetQueryStringValue("DisWFlowID").ToString());

            //insert or update the record according to the stage completed status
            int? iResult = oDISLog_WorkFlowBAL.addWorkFlowActionsBAL(oDISLog_WorkFlowBE);

            //now insert the work flow HTML according to the action taken only if the stage is completed
            DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
            DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();
            WorkflowHTML oWorkflowHTML = new WorkflowHTML();


            if (GetQueryStringValue("FromPage") != null && GetQueryStringValue("FromPage").ToString().Trim() == "QualityIssue")
            {

                if (chkGIN012StageCompleted.Checked)
                {

                    oDiscrepancyBE.GINHTML = oWorkflowHTML.function1(DateTime.Now, null,
                                                                    null, "Yellow", "", "",
                                                                    //Session["UserName"].ToString(),
                                                                    UIUtility.GetODVendorUserName(Convert.ToString(Session["Role"]), Convert.ToString(Session["UserName"]), Convert.ToString(Session["LoginID"])),
                                                                    txtComments.Text, "", "", "", "", "", "", "", "", "", "");
                    oDiscrepancyBE.GINActionRequired = false;

                    oDiscrepancyBE.INVHTML = oWorkflowHTML.function3("white", null);
                    oDiscrepancyBE.INVActionRequired = false;

                    oDISLog_WorkFlowBE.Action = "GetAccountPayable";
                    string VendorAccountPayablesEmail = oDISLog_WorkFlowBAL.GetVendorAccountPayablesEmail(oDISLog_WorkFlowBE);

                    oDiscrepancyBE.APHTML = oWorkflowHTML.function4("Blue", "", null, VendorAccountPayablesEmail, "", "Build on to Accounts Payable Work list on  " + DateTime.Now.ToString("dd/MM/yyyy") + " with following request", "Please credit/debit the account for the amount to cover labour costs", "", "", DateTime.Now, "");
                    oDiscrepancyBE.APActionRequired = true;
                    oDiscrepancyBE.APUserControl = "ACP003";

                    oDiscrepancyBE.VENHTML = oWorkflowHTML.function3("white", null);
                    oDiscrepancyBE.VenActionRequired = false;

                    oDiscrepancyBE.Action = "InsertHTML";
                    oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
                    oDiscrepancyBE.LoggedDateTime = DateTime.Now;
                    oDiscrepancyBE.LevelNumber = 1;
                    oDiscrepancyBE.ActionTakenBy = "GIN";
                    oDiscrepancyBE.APActionCategory = "AR";  //Action Required
                    /////////// For Dupilcate Discrepancy in Serach Page- Quality Issue///////////////
                    //if ((string.IsNullOrEmpty(oDiscrepancyBE.VENUserControl)) && oDiscrepancyBE.VenActionRequired == false)
                    //{
                    //    oDiscrepancyBE.Action = "CheckforGin012";
                    //    oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
                    //    DataSet ds = oDiscrepancyBAL.CheckForGin0012DuplicateDisc(oDiscrepancyBE);
                    //    oDiscrepancyBE.VenActionRequired = true;
                    //    oDiscrepancyBE.VENHTML = ds.Tables[0].Rows[0]["VENHTML"].ToString();
                    //    oDiscrepancyBE.VENUserControl = ds.Tables[0].Rows[0]["VENUserControl"].ToString();
                    //}
                    // Commented for resolving of bug as this code is not required anymore - Dated 30-01-2018
                    oDiscrepancyBE.Action = "InsertHTML";
                    int? iResult1 = oDiscrepancyBAL.addWorkFlowHTMLsBAL(oDiscrepancyBE);
                }
                else
                {
                    InsertHTML();
                }
            }
        }
        //Redirecting to Previous Page
        RedirectToPage(GetQueryStringValue("PreviousPage"));
    }
    #endregion

    #region Methods
    private void InsertHTML()
    {
        DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
        DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();

        WorkflowHTML oWorkflowHTML = new WorkflowHTML();
        oDiscrepancyBE.GINHTML = oWorkflowHTML.function3("White", DateTime.Now, txtComments.Text.Trim(), "");
        oDiscrepancyBE.Action = "InsertHTML";
        oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
        oDiscrepancyBE.LoggedDateTime = DateTime.Now;
        oDiscrepancyBE.LevelNumber = 1;

        int? iResult = oDiscrepancyBAL.addWorkFlowHTMLsBAL(oDiscrepancyBE);
    }
    #endregion

    protected void btnBack_Click(object sender, EventArgs e)
    {
        //Redirecting to Previous Page
        RedirectToPage(GetQueryStringValue("PreviousPage"));
    }
}
﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="GIN003.aspx.cs" Inherits="GINAction_GIN003" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Office Depot</title>
    <link rel="stylesheet" type="text/css" href="../../../../Css/style.css" />
    <link type="text/css" href="../../../../Css/ui-lightness/jquery-ui-1.8.5.custom.css"
        rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="../../../../Css/superfish.css" media="screen" />
    <script type="text/javascript" src="<%= ResolveClientUrl("../../../../Scripts/jquery-1.2.6.min.js")%>"></script>
    <script type="text/javascript" src="<%= ResolveClientUrl("../../../../Scripts/jquery-1.4.2.min.js")%>"></script>
    <script type="text/javascript" src="<%= ResolveClientUrl("../../../../Scripts/jquery-ui-1.8.5.custom.min.js")%>"></script>
    <script type="text/javascript" src="<%= ResolveClientUrl("../../../../Scripts/hoverIntent.js")%>"></script>
    <script type="text/javascript" src="<%= ResolveClientUrl("../../../../Scripts/superfish.js")%>"></script>
    <script type="text/javascript" src="<%= ResolveClientUrl("../../../../Scripts/ValidateInputs.js")%>"></script>
    <script type="text/javascript">
        function StageCompleted() {
            var objchkGIN003StageCompleted = document.getElementById('<%=chkGIN003StageCompleted.ClientID %>');
            var objrfvCarrierSendingGoodsBackRequired = document.getElementById('<%=rfvCarrierSendingGoodsBackRequired.ClientID %>');
            var objrfvCarrierTrackingNumRequired = document.getElementById('<%=rfvCarrierTrackingNumRequired.ClientID %>');
            var objrfvCarriageChargeRequired = document.getElementById('<%=rfvCarriageChargeRequired.ClientID %>');
            var objrevCarriageChargeRequired = document.getElementById('<%=revCarriageChargeCorrectFormatRequired.ClientID %>');
            var objrfvNumPalletsExchangeRequired = document.getElementById('<%=rfvNumPalletsExchangeRequired.ClientID %>');
            var objrfvGIN003CommentRequired = document.getElementById('<%=rfvCommentRequired.ClientID %>');

            if (document.getElementById('<%=hdnValue.ClientID %>').value == '1') {

                ValidatorEnable(objrfvCarrierSendingGoodsBackRequired, false);
                ValidatorEnable(objrfvCarrierTrackingNumRequired, false);
                ValidatorEnable(objrfvCarriageChargeRequired, false);
                ValidatorEnable(objrevCarriageChargeRequired, false);
                ValidatorEnable(objrfvNumPalletsExchangeRequired, false);

                objchkGIN003StageCompleted.checked = false;
                document.getElementById('<%=hdnValue.ClientID %>').value = '0';
            }
            else if (document.getElementById('<%=hdnValue.ClientID %>').value == '0') {

                ValidatorEnable(objrfvCarrierSendingGoodsBackRequired, true);
                ValidatorEnable(objrfvCarrierTrackingNumRequired, true);
                ValidatorEnable(objrfvCarriageChargeRequired, true);
                ValidatorEnable(objrevCarriageChargeRequired, true);
                ValidatorEnable(objrfvNumPalletsExchangeRequired, true);

                objchkGIN003StageCompleted.checked = true;
                document.getElementById('<%=hdnValue.ClientID %>').value = '1';
            }
        }
        function GetLocation() {
            var Location = parent.document.getElementById('ctl00_ContentPlaceHolder1_txtLocation').value;
            document.getElementById('<%=hdfLocation.ClientID%>').value = Location;
            if (checkValidationGroup("a")) {
                document.getElementById('<%=btnSave.ClientID%>').style.visibility = "hidden";
            }
            else {
                document.getElementById('<%=btnSave.ClientID%>').style.visibility = "visible";
                return false;
            }
        }
        $(function () {
            $('#txtCollectionDate').datepicker({
                showOtherMonths: true,
                selectOtherMonths: true, changeMonth: true, changeYear: true,
                minDate: new Date(2013, 0, 1),
                yearRange: '2013:+100',
                dateFormat: 'dd/mm/yy'
            });
        });
        function PleaseSelectGoodsBeReturned(sender, args) {
            var objrdo1 = document.getElementById('<%=rdoVendorToCollect.ClientID %>');
            var objrdo2 = document.getElementById('<%=rdoOfficeDepotToReturn.ClientID %>');
            var objrdo3 = document.getElementById('<%=rdoDisposeOfGood.ClientID %>');
            if (objrdo1.checked == false && objrdo2.checked == false && objrdo3.checked == false) {
                args.IsValid = false;
            }
        }

        function Show(sendor)
        {
          
            var objrfvCarrierSendingGoodsBackRequired = document.getElementById('<%=rfvCarrierSendingGoodsBackRequired.ClientID %>');
            var objrfvCarrierTrackingNumRequired = document.getElementById('<%=rfvCarrierTrackingNumRequired.ClientID %>');
            var objrfvCarriageChargeRequired = document.getElementById('<%=rfvCarriageChargeRequired.ClientID %>');
            var objrevCarriageChargeRequired = document.getElementById('<%=revCarriageChargeCorrectFormatRequired.ClientID %>');
            var objrfvNumPalletsExchangeRequired = document.getElementById('<%=rfvNumPalletsExchangeRequired.ClientID %>');

            var objrfvCollectionDateRequired = document.getElementById('<%=rfvCollectionDateRequired.ClientID %>');
            var objrfvCarrierWhoCollectGoodsRequired = document.getElementById('<%=rfvCarrierWhoCollectGoodsRequired.ClientID %>');
            var objrfvCollectionNorequired = document.getElementById('<%=rfvCollectionNorequired.ClientID %>');

            if (sendor.value == 'rdoVendorToCollect')
            {

                document.getElementById('tr2').style.display = 'block';
                document.getElementById('tr1').style.display = 'none';
                ValidatorEnable(objrfvCarrierSendingGoodsBackRequired, false);
                ValidatorEnable(objrfvCarrierTrackingNumRequired, false);
                ValidatorEnable(objrfvCarriageChargeRequired, false);
                ValidatorEnable(objrevCarriageChargeRequired, false);
                ValidatorEnable(objrfvNumPalletsExchangeRequired, false);

                ValidatorEnable(objrfvCollectionDateRequired, true);
                ValidatorEnable(objrfvCarrierWhoCollectGoodsRequired, true);
                ValidatorEnable(objrfvCollectionNorequired, true);

            }
            if (sendor.value == 'rdoOfficeDepotToReturn')
            {

                document.getElementById('tr1').style.display = 'block';
                document.getElementById('tr2').style.display = 'none';
                ValidatorEnable(objrfvCarrierSendingGoodsBackRequired, true);
                ValidatorEnable(objrfvCarrierTrackingNumRequired, true);
                ValidatorEnable(objrfvCarriageChargeRequired, true);
                ValidatorEnable(objrevCarriageChargeRequired, true);
                ValidatorEnable(objrfvNumPalletsExchangeRequired, true);

                ValidatorEnable(objrfvCollectionDateRequired, false);
                ValidatorEnable(objrfvCarrierWhoCollectGoodsRequired, false);
                ValidatorEnable(objrfvCollectionNorequired, false);

            }
            if (sendor.value == 'rdoDisposeOfGood')
            {
                document.getElementById('tr1').style.display = 'none';
                document.getElementById('tr2').style.display = 'none';
                ValidatorEnable(objrfvCarrierSendingGoodsBackRequired, false);
                ValidatorEnable(objrfvCarrierTrackingNumRequired, false);
                ValidatorEnable(objrfvCarriageChargeRequired, false);
                ValidatorEnable(objrevCarriageChargeRequired, false);
                ValidatorEnable(objrfvNumPalletsExchangeRequired, false);

                ValidatorEnable(objrfvCollectionDateRequired, false);
                ValidatorEnable(objrfvCarrierWhoCollectGoodsRequired, false);
                ValidatorEnable(objrfvCollectionNorequired, false);
         
            }
        }
        function myFunction()
        {
            var objrfvCollectionDateRequired = document.getElementById('<%=rfvCollectionDateRequired.ClientID %>');
            var objrfvCarrierWhoCollectGoodsRequired = document.getElementById('<%=rfvCarrierWhoCollectGoodsRequired.ClientID %>');
            var objrfvCollectionNorequired = document.getElementById('<%=rfvCollectionNorequired.ClientID %>');
            ValidatorEnable(objrfvCollectionDateRequired, false);
            ValidatorEnable(objrfvCarrierWhoCollectGoodsRequired, false);
            ValidatorEnable(objrfvCollectionNorequired, false);
            //debugger;        
            var CostCenter = parent.document.getElementById('ctl00_ContentPlaceHolder1_hdnCostCenter');
            document.getElementById('<%=txtCostCenter.ClientID%>').value = CostCenter.value;
        }
    </script>
</head>
<body onload="myFunction()">
    <form id="form2" runat="server">
    <cc1:ucPanel ID="pnlActionRequired" runat="server" Width="100%" CssClass="fieldset-form"
        BorderColor="gray" BorderStyle="Solid" BorderWidth="0" GroupingText="Action Required">
        <table class="form-table" cellpadding="0" cellspacing="0" width="98%" style="margin: 2px;">
            <tr>
                <td style="font-weight: bold;"><asp:HiddenField ID="hdfLocation" runat="server" />
                    <cc1:ucLabel ID="pnlGIN003Desc" runat="server" CssClass="action-required-heading"
                        Style="text-align: center;" Text="Please arrange for goods to be returned to the vendor and confirm the carriage charge"></cc1:ucLabel>
                </td>
            </tr>
            <tr>
                <td>
                    <table class="form-table" cellpadding="0" cellspacing="0" width="98%" style="margin: 2px;">
                      
                    <tr id="trAction" runat="server"  visible="false">
                    <td>
                         <table class="form-table" cellpadding="0" cellspacing="0" width="98%" style="margin: 2px;">
                            <tr>
                                <td colspan="3">
                                    <cc1:ucLabel ID="lblVEN001HowGoodsReturned" runat="server" Text="How should goods be returned?"></cc1:ucLabel>
                                </td>
                            </tr>
                            <tr>
                                <td width="15%">
                                    <cc1:ucRadioButton ID="rdoVendorToCollect" runat="server" GroupName="goodsReturnedOption"
                                        Text="Vendor To Collect"   onclick='javascript:return Show(this);' />
                                </td>
                                <td width="20%">
                                    <cc1:ucRadioButton ID="rdoOfficeDepotToReturn" runat="server" GroupName="goodsReturnedOption"
                                        Text="Office Depot To Return" onclick='javascript:return Show(this);' Checked="true" />
                                    <asp:CustomValidator ID="cusvPleaseSelectGoodsBeReturned" runat="server" ErrorMessage="Please select goods be returned?"
                                        ClientValidationFunction="PleaseSelectGoodsBeReturned" Display="None" ValidationGroup="a">
                                    </asp:CustomValidator>
                                </td>
                                <td align="left">
                                    <cc1:ucRadioButton ID="rdoDisposeOfGood" runat="server" GroupName="goodsReturnedOption"
                                        Text="Dispose of Goods" onclick='javascript:return Show(this);' />
                                    <asp:CustomValidator ID="cusvDisposeofGoods" runat="server"
                                    ValidationGroup="a">
                                    </asp:CustomValidator>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td colspan="2">
                                    <cc1:ucLabel ID="lblCarriageChargeApplied1" runat="server" Text="Carriage charge to be applied &lt;br/&gt;(minimum charge per pallet $$, per carton $$)"
                                        ForeColor="Red"></cc1:ucLabel>
                                </td>
                            
                            </tr>
                        </table>
                    </td>
                </tr>
                         <tr id="tr1"><td>
                           <table width="100%" >
                         <tr>
                            <td width="40%">
                                <cc1:ucLabel ID="lblCarrierSendingGoodsBack" runat="server" Text="Carrier sending goods back with"></cc1:ucLabel>
                            </td>
                            <td width="1%">
                                :
                            </td>
                            <td width="59%">
                                <cc1:ucDropdownList ID="ddlCarrierSendingGoodsBack" runat="server">
                                </cc1:ucDropdownList>
                                <asp:RequiredFieldValidator ID="rfvCarrierSendingGoodsBackRequired" runat="server"
                                    InitialValue="0" ControlToValidate="ddlCarrierSendingGoodsBack" Display="None"
                                    ValidationGroup="a" ErrorMessage="Please select a carrier"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <cc1:ucLabel ID="lblCarrierTrackingNum" runat="server" Text="Carrier Tracking Number"></cc1:ucLabel>
                            </td>
                            <td width="1%">
                                :
                            </td>
                            <td>
                                <cc1:ucTextbox ID="txtCarrierTrackingNum" runat="server"></cc1:ucTextbox>
                                <asp:RequiredFieldValidator ID="rfvCarrierTrackingNumRequired" runat="server" ControlToValidate="txtCarrierTrackingNum"
                                    Display="None" ValidationGroup="a" ErrorMessage="Please enter the carrier tracking number.">
                                </asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr id="trCarrier" runat="server">
                            <td>
                                <cc1:ucLabel ID="lblCarriagechargetobeapplied" runat="server" Text="Carriage charge to be applied"></cc1:ucLabel>
                            </td>
                            <td width="1%" >
                                :
                            </td>
                            <td>
                                <cc1:ucTextbox ID="txtCarriageCharge" runat="server" onkeyup="AllowDecimalOnly(this)"></cc1:ucTextbox>
                                <%--<cc1:ucDropdownList ID="drpRate" runat="server" Width="60px">
                                    <asp:ListItem Text="EURO" Value="1"></asp:ListItem>
                                    <asp:ListItem Text="GBP" Value="2"></asp:ListItem>
                                </cc1:ucDropdownList>--%>
                                <asp:RequiredFieldValidator ID="rfvCarriageChargeRequired" runat="server" ControlToValidate="txtCarriageCharge"
                                    Display="None" ValidationGroup="a" ErrorMessage="Please enter the carrier charge.">
                                </asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="revCarriageChargeCorrectFormatRequired" runat="server"
                                    ControlToValidate="txtCarriageCharge" Display="None" ValidationGroup="a" ErrorMessage="Please enter carriage charge upto 2 decimal digits."
                                    ValidationExpression="^\d{0,8}(\.\d{0,2})?$"></asp:RegularExpressionValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <cc1:ucLabel ID="lblNumPalletsExchange" runat="server" Text="Number of Pallets Exchanged"></cc1:ucLabel>
                            </td>
                            <td width="1%">
                                :
                            </td>
                            <td>
                                <cc1:ucTextbox ID="txtNumPalletsExchange" runat="server" Text="0" onkeyup="AllowNumbersOnly(this)"></cc1:ucTextbox>
                                <asp:RequiredFieldValidator ID="rfvNumPalletsExchangeRequired" runat="server" ControlToValidate="txtNumPalletsExchange"
                                    Display="None" ValidationGroup="a" ErrorMessage="Please enter the number of pallets exchanged.">
                                </asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <cc1:ucLabel ID="lblNumCartonsExchange" runat="server" Text="Number of Cartons Exchanged"></cc1:ucLabel>
                            </td>
                            <td width="1%">
                                :
                            </td>
                            <td>
                                <cc1:ucTextbox ID="txtNumCartonsExchange" runat="server" Text="0" onkeyup="AllowNumbersOnly(this)"></cc1:ucTextbox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <cc1:ucLabel ID="lblCostCenter" runat="server" Text="Cost Centre"></cc1:ucLabel>
                            </td>
                            <td width="1%">
                                :
                            </td>
                            <td>
                                <cc1:ucTextbox ID="txtCostCenter" runat="server"></cc1:ucTextbox>
                            </td>
                        </tr>
                         </table>
                           </td></tr>
                       <tr id="tr2" style="display:none">
                           <td>
                                <table width="100%" >
                                     <tr>
                                <td width="40%">
                                    <cc1:ucLabel ID="lblCollectionDate" runat="server" Text="Arranged Collection Date"></cc1:ucLabel>
                                </td>
                                <td width="1%">
                                    :
                                </td>
                                <td width="51%">
                                    <asp:TextBox ID="txtCollectionDate" runat="server" ClientIDMode="Static" class="date"
                                        Width="65px" />
                                     <asp:RequiredFieldValidator ID="rfvCollectionDateRequired" runat="server" ControlToValidate="txtCollectionDate"
                                    Display="None" ValidationGroup="a" ErrorMessage="Please enter collection date">
                                </asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <cc1:ucLabel ID="lblCarrier" runat="server" Text="Carrier"></cc1:ucLabel>
                                </td>
                                <td>
                                    :
                                </td>
                                <td>
                                    <cc1:ucDropdownList ID="ucTxtCarrierWhoCollectGoods" runat="server">
                                    </cc1:ucDropdownList>
                                    <asp:RequiredFieldValidator ID="rfvCarrierWhoCollectGoodsRequired" runat="server"
                                        InitialValue="0" ControlToValidate="ucTxtCarrierWhoCollectGoods" Display="None"
                                        ValidationGroup="a" ErrorMessage="Please enter the carrier who collects the goods.">
                                    </asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <cc1:ucLabel ID="lblCollectionAuthNum" runat="server" Text="Collection Authorisation #"></cc1:ucLabel>
                                </td>
                                <td>
                                    :
                                </td>
                                <td>
                                    <cc1:ucTextbox ID="txtCollectionAuthNum" runat="server"></cc1:ucTextbox>
                                       <asp:RequiredFieldValidator ID="rfvCollectionNorequired" runat="server" ControlToValidate="txtCollectionAuthNum"
                                    Display="None" ValidationGroup="a" ErrorMessage="Please enter collection authorisation no."/>
                                </td>
                            </tr>
                                </table>
                           </td>
                       </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table class="form-table" cellpadding="0" cellspacing="0" width="98%" style="margin: 2px;">
                        <tr>
                            <td width="7%">
                                <cc1:ucLabel ID="lblCommentLabel" runat="server" Text="Comment"></cc1:ucLabel>
                            </td>
                            <td width="93%">
                                :
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <cc1:ucTextbox ID="txtGIN003Comment" CssClass="textarea" runat="server" TextMode="MultiLine"
                                    Width="99%"></cc1:ucTextbox>
                                <asp:RequiredFieldValidator ID="rfvCommentRequired" runat="server" ControlToValidate="txtGIN003Comment"
                                    Display="None" ValidationGroup="a" ErrorMessage="Please enter the comment.">
                                </asp:RequiredFieldValidator>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table class="form-table" cellpadding="0" cellspacing="0" width="98%" style="margin: 2px;">
                        <tr>
                            <td width="14%" >
                                <cc1:ucLabel ID="lblStageCompleted" runat="server" Text="Stage Completed"></cc1:ucLabel>
                                <asp:ValidationSummary ID="vs1" runat="server" ValidationGroup="a" ShowSummary="false"
                                    ShowMessageBox="true" />
                            </td>
                            <td width="1%" >
                                :
                            </td>
                            <td width="75%" align="left" ><%--style="display:none;"--%>
                                <cc1:ucCheckbox ID="chkGIN003StageCompleted" runat="server" Checked="true" />
                            </td>
                            <td width="10%" >
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" >
                                <cc1:ucLabel ID="lblDiscrepancyNote" runat="server" Text="If  you required to tag a comment without closing the discrepancy<br/>1 -    Deselect Stage Complete<br/>2 -   Enter  the comment<br/>3 -   Save"></cc1:ucLabel>
                            </td>
                            <td  align="right" valign="bottom">
                                <cc1:ucButton ID="btnSave" runat="server" Text="Save" class="button" ValidationGroup="a"
                                    OnClick="btnSave_Click" OnClientClick="javascript:GetLocation()" />
                                <input type="hidden" runat="server" id="hdnValue" value="1" />
                                <cc1:ucButton ID="btnBack" runat="server" Text="Back" class="button" OnClick="btnBack_Click" />
                            </td>
                        </tr> 
                    </table>
                </td>
            </tr>
        </table>
    </cc1:ucPanel>
    </form>
</body>
</html>

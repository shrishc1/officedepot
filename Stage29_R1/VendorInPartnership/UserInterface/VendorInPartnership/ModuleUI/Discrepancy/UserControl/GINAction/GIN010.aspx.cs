﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

using Utilities; using WebUtilities;
using BusinessEntities.ModuleBE.Discrepancy.LogDiscrepancy;
using BusinessLogicLayer.ModuleBAL.Discrepancy.LogDiscrepancy;
using BusinessEntities.ModuleBE.AdminFunctions;
using BusinessLogicLayer.ModuleBAL.AdminFunctions;

public partial class GINAction_GIN010 : CommonPage
{
    #region Global variables
    #endregion

    #region Events

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            chkGIN010StageCompleted.Checked = true;
            if (GetQueryStringValue("PONo") != null && GetQueryStringValue("PONo").ToString() != "")
            {
                lblGIN010Desc.Text = "Please confirm the goods are booked in and rework to the correct pack size against Purchase Order : " + GetQueryStringValue("PONo").ToString() + ".Add cost of this additional work. ";
            }
        }
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        btnSave.Enabled = false;
        DISLog_WorkFlowBE oDISLog_WorkFlowBE = new DISLog_WorkFlowBE();
        DISLog_WorkFlowBAL oDISLog_WorkFlowBAL = new DISLog_WorkFlowBAL();

        if (GetQueryStringValue("disLogID") != null && GetQueryStringValue("disLogID").ToString() != "")
        {
            //**********************************************************
            string Location = hdfLocation.Value;
            DiscrepancyBE oDiscrepancyBE1 = new DiscrepancyBE();
            DiscrepancyBAL oDiscrepancyBAL1 = new DiscrepancyBAL();
            oDiscrepancyBE1.Action = "UpdateLocation";
            oDiscrepancyBE1.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
            oDiscrepancyBE1.Location = Location;
            oDiscrepancyBE1.UserID = Convert.ToInt32(Session["UserID"]);
            string sResult1 = oDiscrepancyBAL1.updateDiscrepancyLocationBAL(oDiscrepancyBE1);
            //**********************************************************
            oDISLog_WorkFlowBE.Action = "InsertUpdateWorkflowActions";
            oDISLog_WorkFlowBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
            oDISLog_WorkFlowBE.StageCompleted = chkGIN010StageCompleted.Checked ? true : false;

            if (chkGIN010StageCompleted.Checked)
                oDISLog_WorkFlowBE.GIN010_LabourCost = Convert.ToDecimal(txtGIN010CostofLabour.Text.Trim());

            oDISLog_WorkFlowBE.StageCompletedForWhom = "GIN010_StageCompleted";
            oDISLog_WorkFlowBE.Comment = txtComments.Text;

            oDISLog_WorkFlowBE.CostCenter = txtCostCenter.Text.Trim();

            //update the work flow action table each time for the comments and other fields( if stage is completed)
            oDISLog_WorkFlowBE.DiscrepancyWorkFlowID = Convert.ToInt32(GetQueryStringValue("DisWFlowID").ToString());

            //insert or update the record according to the stage completed status
            int? iResult = oDISLog_WorkFlowBAL.addWorkFlowActionsBAL(oDISLog_WorkFlowBE);

            //now insert the work flow HTML according to the action taken only if the stage is completed
            DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
            DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();
            WorkflowHTML oWorkflowHTML = new WorkflowHTML();


            if (GetQueryStringValue("FromPage") != null && GetQueryStringValue("FromPage").ToString().Trim() == "WrongPackSize"
                || GetQueryStringValue("FromPage") != null && GetQueryStringValue("FromPage").ToString().Trim() == "QualityIssue")
            {
                if (chkGIN010StageCompleted.Checked)
                {
                 
                    oDiscrepancyBE.GINHTML = oWorkflowHTML.function1(DateTime.Now, null,
                                                                    null, "Yellow", "", "",
                                                                    //Session["UserName"].ToString(),
                                                                    UIUtility.GetODVendorUserName(Convert.ToString(Session["Role"]), Convert.ToString(Session["UserName"]), Convert.ToString(Session["LoginID"])),
                                                                    txtComments.Text, "", "", "", "", "", "", "", "", "", txtGIN010CostofLabour.Text.Trim(),pCostCenter:txtCostCenter.Text);
                    oDiscrepancyBE.GINActionRequired = false;

                    oDiscrepancyBE.INVHTML = oWorkflowHTML.function3("white", null);
                    oDiscrepancyBE.INVActionRequired = false;

                    //if (dtVEN != null && dtVEN.Rows.Count > 0) //if vendor has  taken action
                    //{
                    //    oDiscrepancyBE.APHTML = oWorkflowHTML.function3("white", null);
                    //    oDiscrepancyBE.APActionRequired = false;
                    //}
                    //else
                    //{
                    //  // if vendor has not taken action
                    oDISLog_WorkFlowBE.Action = "GetAccountPayable";
                    string VendorAccountPayablesEmail = oDISLog_WorkFlowBAL.GetVendorAccountPayablesEmail(oDISLog_WorkFlowBE);
                    if (Convert.ToDecimal(txtGIN010CostofLabour.Text) > 0)
                    {
                        oDiscrepancyBE.APHTML = oWorkflowHTML.function4("Blue", "", null, VendorAccountPayablesEmail, "", "Build on to Accounts Payable Work list on  " + DateTime.Now.ToString("dd/MM/yyyy") + " with following reques", "Please credit/debit the account for the amount to cover labour costs", "", "", DateTime.Now, txtGIN010CostofLabour.Text.Trim());
                        oDiscrepancyBE.APActionRequired = true;
                        oDiscrepancyBE.APUserControl = "ACP003";
                        oDiscrepancyBE.APActionCategory = "AR";  //Action Required
                    }
                    else
                    {
                        oDiscrepancyBE.APHTML = oWorkflowHTML.function3("white", null, "", "");
                        oDiscrepancyBE.APActionRequired = false;
                    }
                   // }

                    oDiscrepancyBE.VENHTML = oWorkflowHTML.function3("white", null);
                    oDiscrepancyBE.VenActionRequired = false;

                    oDiscrepancyBE.Action = "InsertHTML";
                    oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
                    oDiscrepancyBE.LoggedDateTime = DateTime.Now;
                    oDiscrepancyBE.LevelNumber = 1;
                    oDiscrepancyBE.ActionTakenBy = "GIN";
                    int? iResult1 = oDiscrepancyBAL.addWorkFlowHTMLsBAL(oDiscrepancyBE);
                }
                else
                {
                    InsertHTML();
                }
            }
        }
        //Redirecting to Previous Page
        RedirectToPage(GetQueryStringValue("PreviousPage"));
    }
    #endregion

    #region Methods
    private void InsertHTML()
    {
        DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
        DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();

        WorkflowHTML oWorkflowHTML = new WorkflowHTML();
        oDiscrepancyBE.GINHTML = oWorkflowHTML.function3("White", DateTime.Now, txtComments.Text.Trim(), "");
        oDiscrepancyBE.Action = "InsertHTML";
        oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
        oDiscrepancyBE.LoggedDateTime = DateTime.Now;
        oDiscrepancyBE.LevelNumber = 1;

        int? iResult = oDiscrepancyBAL.addWorkFlowHTMLsBAL(oDiscrepancyBE);
    }
    #endregion

    protected void btnBack_Click(object sender, EventArgs e)
    {
        //Redirecting to Previous Page
        RedirectToPage(GetQueryStringValue("PreviousPage"));
    }
}
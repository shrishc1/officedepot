﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using BusinessEntities.ModuleBE.Discrepancy.LogDiscrepancy;
using BusinessLogicLayer.ModuleBAL.Discrepancy.LogDiscrepancy;
using System.Collections.Generic;
using Utilities;
using WebUtilities;

public partial class ModuleUI_Discrepancy_UserControl_GINAction_GIN016 : CommonPage
{
    protected string CommentRequired = "-" + WebCommon.getGlobalResourceValue("CommentRequired");

    string FollowingRequestMessage = WebCommon.getGlobalResourceValue("FollowingRequestMessage");
    #region Global variables

    int Siteid = 0;
    int Vendorid = 0;
    string EmailList = null;
    System.Text.StringBuilder sSendMailLink = new System.Text.StringBuilder();

    #endregion

    #region Events
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            chkStageCompleted.Checked = true;
            if (GetQueryStringValue("VDRNO") != null)
            {
                if (GetQueryStringValue("VDRNO").ToArray()[0].ToString() == "U")
                {
                    lblGIN0016Text1.Text = WebCommon.getGlobalResourceValue("GIN0016Text");
                 }
                else if (GetQueryStringValue("VDRNO").ToArray()[0].ToString() == "T")
                {
                    lblGIN0016Text1.Text = WebCommon.getGlobalResourceValue("GIN0016Textb");
                }
            }
        }


    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        btnSave.Enabled = false;
        DISLog_WorkFlowBE oDISLog_WorkFlowBE = new DISLog_WorkFlowBE();
        DISLog_WorkFlowBAL oDISLog_WorkFlowBAL = new DISLog_WorkFlowBAL();
        string sentToLinks = string.Empty;

        if (GetQueryStringValue("disLogID") != null && GetQueryStringValue("disLogID").ToString() != "")
        {
            //**********************************************************
            //string Location = hdfLocation.Value;
            //DiscrepancyBE oDiscrepancyBE1 = new DiscrepancyBE();
            //DiscrepancyBAL oDiscrepancyBAL1 = new DiscrepancyBAL();
            //oDiscrepancyBE1.Action = "UpdateLocation";
            //oDiscrepancyBE1.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
            //oDiscrepancyBE1.Location = Location;
            //oDiscrepancyBE1.UserID = Convert.ToInt32(Session["UserID"]);
            //string sResult1 = oDiscrepancyBAL1.updateDiscrepancyLocationBAL(oDiscrepancyBE1);
            //**********************************************************
            oDISLog_WorkFlowBE.Action = "InsertUpdateWorkflowActions";
            oDISLog_WorkFlowBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
            oDISLog_WorkFlowBE.StageCompleted = chkStageCompleted.Checked ? true : false;

            oDISLog_WorkFlowBE.StageCompletedForWhom = "GIN016_StageCompleted";
            oDISLog_WorkFlowBE.Comment = txtComment.Text;

            //update the work flow action table each time for the comments and other fields( if stage is completed)
            oDISLog_WorkFlowBE.DiscrepancyWorkFlowID = Convert.ToInt32(GetQueryStringValue("DisWFlowID").ToString());

            oDISLog_WorkFlowBE.Decision = rdoAgree.Checked ? "Agree" :"Disagree";
            //insert or update the record according to the stage completed status
            int? iResult = oDISLog_WorkFlowBAL.addWorkFlowActionsBAL(oDISLog_WorkFlowBE);

            //now insert the work flow HTML according to the action taken only if the stage is completed
            DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
            DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();
            WorkflowHTML oWorkflowHTML = new WorkflowHTML();


            if (chkStageCompleted.Checked)
            {
              if (GetQueryStringValue("VDRNO").ToArray()[0].ToString() == "U")
                {
                 if (rdoAgree.Checked)
                    {

                        oDiscrepancyBE.GINHTML = oWorkflowHTML.function6(DateTime.Now, "Yellow", UIUtility.GetODVendorUserName(Convert.ToString(Session["Role"]), Convert.ToString(Session["UserName"]), Convert.ToString(Session["LoginID"]))
                            ,"Agree", "", "", "", txtComment.Text.Trim(),"", (DateTime?)null, "");
                        oDiscrepancyBE.INVUserControl = "INV014";
                    }
                    else
                    {
                        oDiscrepancyBE.INVUserControl = "INV015";
                        oDiscrepancyBE.GINHTML = oWorkflowHTML.function6(DateTime.Now, "Yellow", UIUtility.GetODVendorUserName(Convert.ToString(Session["Role"]), Convert.ToString(Session["UserName"]), Convert.ToString(Session["LoginID"]))
                      , "Disagree", "", "", "", txtComment.Text.Trim(), "", (DateTime?)null, "");
                    }
                    oDiscrepancyBE.GINActionRequired = false;
                    oDiscrepancyBE.INVHTML = oWorkflowHTML.function4("Blue", "", null, "", "",
                        FollowingRequestMessage.Replace("{stockPlannerID}", "").Replace("{Date}", DateTime.Now.ToString("dd/MM/yyyy")),
                        WebCommon.getGlobalResourceValue("INV014"));
                    oDiscrepancyBE.CloseDiscrepancy = false;
              
                    oDiscrepancyBE.INVActionRequired = true;                    
                    oDiscrepancyBE.InventoryActionCategory = "AR";  //Action Required
                    oDiscrepancyBE.APHTML = oWorkflowHTML.function3("white", null);
                    oDiscrepancyBE.APActionRequired = false;
                }
              else if (GetQueryStringValue("VDRNO").ToArray()[0].ToString() == "T")
              {
                  if (rdoAgree.Checked)
                  {

                      oDiscrepancyBE.GINHTML = oWorkflowHTML.function6(DateTime.Now, "Yellow", UIUtility.GetODVendorUserName(Convert.ToString(Session["Role"]), Convert.ToString(Session["UserName"]), Convert.ToString(Session["LoginID"]))
                          , "Agree", "", "", "", txtComment.Text.Trim(), "", (DateTime?)null, "");
                      oDiscrepancyBE.APUserControl = "ACP014";
                  }
                  else
                  {
                      oDiscrepancyBE.APUserControl = "ACP004";
                      oDiscrepancyBE.GINHTML = oWorkflowHTML.function6(DateTime.Now, "Yellow", UIUtility.GetODVendorUserName(Convert.ToString(Session["Role"]), Convert.ToString(Session["UserName"]), Convert.ToString(Session["LoginID"]))
                    , "Disagree", "", "", "", txtComment.Text.Trim(), "", (DateTime?)null, "");
                  }
                  oDiscrepancyBE.GINActionRequired = false;
                  oDiscrepancyBE.APHTML = oWorkflowHTML.function4("Blue", "", null, "", "",
                      FollowingRequestMessage.Replace("{stockPlannerID}", "").Replace("{Date}", DateTime.Now.ToString("dd/MM/yyyy")),
                      WebCommon.getGlobalResourceValue("AP014"));
                  oDiscrepancyBE.CloseDiscrepancy = false;

                  oDiscrepancyBE.APActionRequired = true;

                  oDiscrepancyBE.INVHTML = oWorkflowHTML.function3("white", null);
                  oDiscrepancyBE.INVActionRequired = false;
                    oDiscrepancyBE.APActionCategory = "AR";  //Action Required
                }


                // sendAndSaveDiscrepancyCommunication(Convert.ToInt32(GetQueryStringValue("disLogID").ToString()),"lettertype1");                

                //oDiscrepancyBE.VENHTML = oWorkflowHTML.function2(DateTime.Now, null, "Blue", sSendMailLink.ToString(),
                //                                                        @"Communication sent to vendor", "", "", "", "", "", "", "");

                oDiscrepancyBE.VENHTML = oWorkflowHTML.function3("white", null, "", "");

                oDiscrepancyBE.VenActionRequired = false;

                oDiscrepancyBE.Action = "InsertHTML";

                oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
                oDiscrepancyBE.LoggedDateTime = DateTime.Now;
                oDiscrepancyBE.LevelNumber = 1;
                oDiscrepancyBE.ActionTakenBy = "GIN";               
                int? iResult1 = oDiscrepancyBAL.addWorkFlowHTMLsBAL(oDiscrepancyBE);
            }
            else
            {
                InsertHTML();
            }

        }
        //Redirecting to Previous Page
        RedirectToPage(GetQueryStringValue("PreviousPage"));
    }
    #endregion

    #region Methods

    private string SendMailAndGetMailLinks(int DiscrepancyLogID, string discrepancyType, string sCurrentCommunicationLevel, string sNextCommunicationLevel)
    {
        sendCommunicationAllLevel osendCommunicationAllLevel = new sendCommunicationAllLevel();
        return osendCommunicationAllLevel.SendMailAndGetMailLinks(DiscrepancyLogID, discrepancyType, sCurrentCommunicationLevel, sNextCommunicationLevel);

    }

    private void InsertHTML()
    {
        DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
        DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();

        WorkflowHTML oWorkflowHTML = new WorkflowHTML();
        oDiscrepancyBE.APHTML = oWorkflowHTML.function3("White", DateTime.Now, txtComment.Text.Trim());
        oDiscrepancyBE.APActionRequired = false;

        oDiscrepancyBE.Action = "InsertHTML";
        oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
        oDiscrepancyBE.LoggedDateTime = DateTime.Now;
        oDiscrepancyBE.LevelNumber = 1;

        int? iResult = oDiscrepancyBAL.addWorkFlowHTMLsBAL(oDiscrepancyBE);
    }

    private string GetDiscrepancyEmailListOfVendor()
    {
        if (GetQueryStringValue("disLogID") != null)
        {
            DiscrepancyBE oNewDiscrepancyBE = new DiscrepancyBE();
            DiscrepancyBAL oNewDiscrepancyBAL = new DiscrepancyBAL();
            oNewDiscrepancyBE.Action = "GetDiscrepancyLog";
            oNewDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
            List<DiscrepancyBE> lstDisLog = oNewDiscrepancyBAL.GetDiscrepancyLogDetailsBAL(oNewDiscrepancyBE);
            oNewDiscrepancyBAL = null;
            if (lstDisLog != null && lstDisLog.Count > 0)
            {
                Siteid = lstDisLog[0].Site.SiteID;
                Vendorid = Convert.ToInt32(lstDisLog[0].VendorID);
            }

            EmailList = GetDiscrepancyContactsOfVendor(Vendorid, Siteid);

        }
        return EmailList;

    }

    private int? sendAndSaveDiscrepancyCommunication(int iDiscrepancy, string LetterType)
    {

        GetDiscrepancyEmailListOfVendor();

        int? vendorID = Vendorid;

        sendCommunication communication = new sendCommunication();
        int? retVal = communication.sendCommunicationByEMail2(iDiscrepancy, "invoice", EmailList, vendorID, "", "", "", 0, LetterType);
        sSendMailLink = communication.sSendMailLink;
        return retVal;

    }


    #endregion
    protected void btnBack_Click(object sender, EventArgs e)
    {
        //Redirecting to Previous Page
        RedirectToPage(GetQueryStringValue("PreviousPage"));
    }
}
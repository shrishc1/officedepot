﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Web.UI;
using BusinessEntities.ModuleBE.Appointment.CountrySetting;
using BusinessEntities.ModuleBE.Discrepancy.LogDiscrepancy;
using BusinessEntities.ModuleBE.Security;
using BusinessLogicLayer.ModuleBAL.Appointment.CountrySetting;
using BusinessLogicLayer.ModuleBAL.Discrepancy.LogDiscrepancy;
using BusinessLogicLayer.ModuleBAL.Security;
using Utilities;
using WebUtilities;

public partial class GINAction_GIN003 : CommonPage
{


    #region Events

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //******************
            DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
            DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();
            oDiscrepancyBE.Action = "GetEsclationType";
            oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
            string EsclationType = oDiscrepancyBAL.GetEsclationTypeBAL(oDiscrepancyBE);

            ViewState["EsclationType"] = EsclationType;
            //*******************  

            DiscrepancyBE oDiscrepancyBE1 = new DiscrepancyBE();
            oDiscrepancyBE1.Action = "GetVendorSubscriptionStatus";
            oDiscrepancyBE1.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
            string VendorStatus = oDiscrepancyBAL.GetVendorSubscriptionStatusBAL(oDiscrepancyBE1);
            if (EsclationType == "EscalationType3" && VendorStatus == "False")
            {
                trAction.Visible = true;
                pnlGIN003Desc.Text = WebCommon.getGlobalResourceValue("VendorNotRespondedToArrangeCollection");  // "Vendor has not responded to requests to arrange collection. Please contact vendor directly to request what we should do with the goods";
            }
            //if (VendorStatus == "False" && EsclationType != "EscalationType3")
            //{
            //    txtCarriageCharge.Visible = false;
            //    lblCarriagechargetobeapplied.Visible = false;
            //    rfvCarriageChargeRequired.Visible = false;
            //    trCarrier.Visible = false;
            //}
            //if (EsclationType == "EscalationType3")
            //{
            //    pnlGIN003Desc.Text = WebCommon.getGlobalResourceValue("VendorNotRespondedToArrangeCollection");  // "Vendor has not responded to requests to arrange collection. Please contact vendor directly to request what we should do with the goods";
            //}
            //*******************
            BindCarrier();
        }
    }

    private void BindCarrier()
    {
        MASCNT_CarrierBE oMASCNT_CarrierBE = new MASCNT_CarrierBE();
        APPCNT_CarrierBAL oMASCNT_CarrierBAL = new APPCNT_CarrierBAL();

        oMASCNT_CarrierBE.Action = "ShowAllByVDRNumber";
        oMASCNT_CarrierBE.Discrepancy = new BusinessEntities.ModuleBE.Discrepancy.LogDiscrepancy.DiscrepancyBE();
        oMASCNT_CarrierBE.Discrepancy.VDRNo = GetQueryStringValue("VDRNo").ToString();

        List<MASCNT_CarrierBE> lstCarrier = oMASCNT_CarrierBAL.GetCarrierDetailsBAL(oMASCNT_CarrierBE);

        if (lstCarrier.Count > 0)
        {
            FillControls.FillDropDown(ref ddlCarrierSendingGoodsBack, lstCarrier, "CarrierName", "CarrierID", "--Select--");
            FillControls.FillDropDown(ref ucTxtCarrierWhoCollectGoods, lstCarrier, "CarrierName", "CarrierID", "--Select--");
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        /*
         Vendor asks for office depot to return goods. Go to step-4b
         */
        btnSave.Enabled = false;
        DISLog_WorkFlowBE oDISLog_WorkFlowBE = new DISLog_WorkFlowBE();
        DISLog_WorkFlowBAL oDISLog_WorkFlowBAL = new DISLog_WorkFlowBAL();
        string GINReturnType = string.Empty;
        string strcarrierSendingGoods = string.Empty;
        string strCarriageCharge = string.Empty;
        string strNumPalletsExchange = string.Empty;
        string strtCarrierTrackingNum = string.Empty;
        string GoodsCollectedOn = null;
        if (GetQueryStringValue("disLogID") != null && GetQueryStringValue("disLogID").ToString() != "")
        {
            //**********************************************************
            string Location = hdfLocation.Value;
            DiscrepancyBE oDiscrepancyBE1 = new DiscrepancyBE();
            DiscrepancyBAL oDiscrepancyBAL1 = new DiscrepancyBAL();
            oDiscrepancyBE1.Action = "UpdateLocation";
            oDiscrepancyBE1.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
            oDiscrepancyBE1.Location = Location;
            oDiscrepancyBE1.UserID = Convert.ToInt32(Session["UserID"]);
            string sResult1 = oDiscrepancyBAL1.updateDiscrepancyLocationBAL(oDiscrepancyBE1);
            //**********************************************************
            string EsclationType = ViewState["EsclationType"].ToString();
            // Print Return note from here.
            int? ReturnNoteID = null;
            if (EsclationType == "EscalationType3" && (rdoDisposeOfGood.Checked || rdoVendorToCollect.Checked))
            {
            }
            else
            {
                ReturnNoteID = PrintAndSaveReturnNote();
            }

            oDISLog_WorkFlowBE.Action = "InsertUpdateWorkflowActions";
            oDISLog_WorkFlowBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
            oDISLog_WorkFlowBE.GIN003_Carrier = ddlCarrierSendingGoodsBack.SelectedIndex > 0 ? Convert.ToInt32(ddlCarrierSendingGoodsBack.SelectedItem.Value) : (int?)null;
            oDISLog_WorkFlowBE.GIN003_CarrierTrackingNumber = txtCarrierTrackingNum.Text.Trim();
            oDISLog_WorkFlowBE.GIN003_CarriageCharge = txtCarriageCharge.Text.Trim() != "" ? Convert.ToDecimal(txtCarriageCharge.Text.Trim()) : (decimal?)null;
            oDISLog_WorkFlowBE.GIN003_PalletsExchanged = txtNumPalletsExchange.Text.Trim() != "" ? Convert.ToInt32(txtNumPalletsExchange.Text.Trim()) : (int?)null;
            oDISLog_WorkFlowBE.GIN003_CartonsExchanged = txtNumCartonsExchange.Text.Trim() != "" ? Convert.ToInt32(txtNumCartonsExchange.Text.Trim()) : (int?)null;
            oDISLog_WorkFlowBE.StageCompleted = chkGIN003StageCompleted.Checked ? true : false;
            oDISLog_WorkFlowBE.StageCompletedForWhom = "GIN003_StageCompleted";
            oDISLog_WorkFlowBE.Comment = txtGIN003Comment.Text;
            oDISLog_WorkFlowBE.CostCenter = txtCostCenter.Text;

            //*********************
            if (EsclationType == "EscalationType3")
            {
                if (rdoVendorToCollect.Checked)
                {
                    oDISLog_WorkFlowBE.GIN_HowGoodsReturned = "V";
                    GINReturnType = "V";
                    GoodsCollectedOn = txtCollectionDate.Text;
                    oDISLog_WorkFlowBE.GIN002_ArrangedCollectionDate = Convert.ToDateTime(Common.GetMM_DD_YYYY(txtCollectionDate.Text.Trim()));
                    oDISLog_WorkFlowBE.GIN002_CollectionAuthorisationNumber = txtCollectionAuthNum.Text.Trim();
                    oDISLog_WorkFlowBE.CarrierWhoWillCollectGoods = Convert.ToInt32(ucTxtCarrierWhoCollectGoods.SelectedItem.Value);
                    oDISLog_WorkFlowBE.Decision = "Vendor To Collect";
                }
                else if (rdoOfficeDepotToReturn.Checked)
                {
                    oDISLog_WorkFlowBE.GIN_HowGoodsReturned = "O";
                    GINReturnType = "O";
                    strcarrierSendingGoods = ddlCarrierSendingGoodsBack.SelectedItem.Text.ToString();
                    strCarriageCharge = txtCarriageCharge.Text.ToString();
                    oDISLog_WorkFlowBE.Decision = "Office Depot To Return";
                }
                else if (rdoDisposeOfGood.Checked)
                {
                    oDISLog_WorkFlowBE.GIN_HowGoodsReturned = "D";
                    GINReturnType = "D";
                    ReturnNoteID = null;
                    oDISLog_WorkFlowBE.Decision = "Dispose of Goods";
                }
            }
            //************************
            //update the work flow action table each time for the comments and other fields( if stage is completed)
            oDISLog_WorkFlowBE.DiscrepancyWorkFlowID = Convert.ToInt32(GetQueryStringValue("DisWFlowID").ToString());

            //insert or update the record according to the stage completed status
            int? iResult = oDISLog_WorkFlowBAL.addWorkFlowActionsBAL(oDISLog_WorkFlowBE);

            var carrierSendingGoods = string.Empty;
            if (ddlCarrierSendingGoodsBack.SelectedItem != null)
                carrierSendingGoods = ddlCarrierSendingGoodsBack.SelectedItem.Text;

            //now insert the work flow HTML according to the action taken only if the stage is completed
            DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
            DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();
            WorkflowHTML oWorkflowHTML = new WorkflowHTML();
            string VendorAccountPayablesEmail = string.Empty;
            string sAction = "Goods returned to vendor.";
            if (GetQueryStringValue("FromPage") != null && GetQueryStringValue("FromPage").ToString().Trim() == "WrongPackSize"
                || GetQueryStringValue("FromPage") != null && GetQueryStringValue("FromPage").ToString().Trim() == "IncorrectProduct"
                || GetQueryStringValue("FromPage") != null && GetQueryStringValue("FromPage").ToString().Trim() == "GoodsReceivedDamaged"
                || GetQueryStringValue("FromPage") != null && GetQueryStringValue("FromPage").ToString().Trim() == "NoPO"
                || GetQueryStringValue("FromPage") != null && GetQueryStringValue("FromPage").ToString().Trim() == "QualityIssue"
                || GetQueryStringValue("FromPage") != null && GetQueryStringValue("FromPage").ToString().Trim() == "NoPaperwork"
                || GetQueryStringValue("FromPage") != null && GetQueryStringValue("FromPage").ToString().Trim() == "ItemNotOnPo")
            {
                //Step- 7: Office Depot to enter the return details and 
                //the carriage costs. Accounts Payable to 
                //credit/debit for carriage costs
                //Go to step 8.
                if (chkGIN003StageCompleted.Checked)
                {
                    if (!string.IsNullOrEmpty(GINReturnType))
                    {
                        if (GINReturnType != "V")
                        {
                            oDiscrepancyBE.GINHTML = oWorkflowHTML.function11(GINReturnType, DateTime.Now,
                                null,
                                null,
                                "Yellow",
                                string.Empty,
                                string.Empty,
                                UIUtility.GetODVendorUserName(Convert.ToString(Session["Role"]), Convert.ToString(Session["UserName"]), Convert.ToString(Session["LoginID"])),
                                txtGIN003Comment.Text,
                                 string.Empty,
                                string.Empty,
                                strcarrierSendingGoods,
                                 string.Empty,
                                strCarriageCharge,
                                string.Empty,
                                string.Empty,
                                sAction,
                                string.Empty,
                                string.Empty,
                                string.Empty,
                                string.Empty,
                                string.Empty,
                                string.Empty,
                                string.Empty,
                                string.Empty,
                                string.Empty,
                               ReturnNoteID,
                                string.Empty,
                                string.Empty,
                                string.Empty,
                                string.Empty
                                );

                            oDiscrepancyBE.GINActionRequired = false;

                            oDiscrepancyBE.INVHTML = oWorkflowHTML.function3("white", null, "", "");
                            oDiscrepancyBE.INVActionRequired = false;
                            if (GINReturnType == "D")
                            {
                                oDiscrepancyBE.APHTML = oWorkflowHTML.function3("white", null, "", "");
                                oDiscrepancyBE.APActionRequired = false;
                            }
                            if (GINReturnType == "O")
                            {
                                if (Convert.ToDecimal(txtCarriageCharge.Text) > 0)
                                {
                                    oDiscrepancyBE.APHTML = oWorkflowHTML.function2(DateTime.Now, null, "Blue", VendorAccountPayablesEmail,
                                                                              "Build on to Accounts Payable Work List on " + DateTime.Now.ToString("dd/MM/yyyy") + " with the following request",
                                                                              "Please credit/debit the account for the amount to cover carriage costs - " + txtCarriageCharge.Text.Trim() + "");
                                    oDiscrepancyBE.APActionRequired = true;
                                    oDiscrepancyBE.APUserControl = "ACP002";
                                    oDiscrepancyBE.APActionCategory = "AR";  //Action Required
                                }
                                else
                                {
                                    oDiscrepancyBE.APHTML = oWorkflowHTML.function3("white", null, "", "");
                                    oDiscrepancyBE.APActionRequired = false;
                                }
                            }
                            oDiscrepancyBE.VENHTML = oWorkflowHTML.function3("white", null, "", "");
                            oDiscrepancyBE.VenActionRequired = false;

                            oDiscrepancyBE.Action = "InsertHTML";
                            oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
                            oDiscrepancyBE.LoggedDateTime = DateTime.Now;
                            oDiscrepancyBE.LevelNumber = 1;
                            oDiscrepancyBE.ActionTakenBy = "GIN";                            
                            int? iResult1 = oDiscrepancyBAL.addWorkFlowHTMLsBAL(oDiscrepancyBE);
                        }
                        else
                        {
                            string color = string.Empty;
                            string strCollectionAuthNo = string.Empty;
                            string sentTo = string.Empty;
                            string strBottomComment = string.Empty;
                            for (int i = 1; i <= 2; i++)
                            {
                                if (i == 1)
                                {
                                    color = "Yellow";
                                    txtGIN003Comment.Text = "";
                                    ReturnNoteID = null;
                                    GINReturnType = "";
                                    strCollectionAuthNo = txtCollectionAuthNum.Text;
                                    sentTo = "Goods In";
                                    strcarrierSendingGoods = ucTxtCarrierWhoCollectGoods.SelectedItem.Text;
                                }
                                else
                                {
                                    color = "Blue";
                                    sentTo = "Goods In";
                                    strBottomComment = "Goods In to confirm goods collection";
                                    txtGIN003Comment.Text = "";
                                    ReturnNoteID = null;
                                    GINReturnType = "";
                                    strCollectionAuthNo = "";
                                    strcarrierSendingGoods = "";
                                    GoodsCollectedOn = "";
                                }
                                oDiscrepancyBE.GINHTML = oWorkflowHTML.function12(GINReturnType, DateTime.Now,
                                 null,
                                 GoodsCollectedOn,
                                 color,
                                 strCollectionAuthNo,
                                 txtGIN003Comment.Text,
                                 Convert.ToString(Session["UserName"]),
                                 ReturnNoteID,
                                 sentTo,
                                 strcarrierSendingGoods,
                                 strBottomComment
                                 );
                                if (i == 1)
                                {
                                    oDiscrepancyBE.GINActionRequired = false;
                                }
                                else
                                {
                                    oDiscrepancyBE.GINActionRequired = true;
                                    oDiscrepancyBE.GINUserControl = "GIN001";                                   
                                    oDiscrepancyBE.GINActionCategory = "RA";  //Remedial Action
                                }

                                oDiscrepancyBE.INVHTML = oWorkflowHTML.function3("white", null, "", "");
                                oDiscrepancyBE.INVActionRequired = false;
                                oDiscrepancyBE.APHTML = oWorkflowHTML.function3("white", null, "", "");
                                oDiscrepancyBE.APActionRequired = false;
                                oDiscrepancyBE.VENHTML = oWorkflowHTML.function3("white", null, "", "");
                                oDiscrepancyBE.VenActionRequired = false;

                                oDiscrepancyBE.Action = "InsertHTML";
                                oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
                                oDiscrepancyBE.LoggedDateTime = DateTime.Now;
                                oDiscrepancyBE.LevelNumber = 5;
                                oDiscrepancyBE.ActionTakenBy = "GIN";
                                int? iResult1 = oDiscrepancyBAL.addWorkFlowHTMLsBAL(oDiscrepancyBE);
                            }
                        }
                    }
                    else
                    {
                        ////**********************************************
                        //DiscrepancyBE oDiscrepancyBE2 = new DiscrepancyBE();
                        //oDiscrepancyBE2.Action = "GetVendorSubscriptionStatus";
                        //oDiscrepancyBE2.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
                        //string VendorStatus = oDiscrepancyBAL.GetVendorSubscriptionStatusBAL(oDiscrepancyBE2);
                        ////*************************************************
                        //if (VendorStatus == "True")
                        //{
                        oDiscrepancyBE.GINHTML = oWorkflowHTML.function1(DateTime.Now,
                            null,
                            null,
                            "Yellow",
                            string.Empty,
                            string.Empty,
                            UIUtility.GetODVendorUserName(Convert.ToString(Session["Role"]), Convert.ToString(Session["UserName"]), Convert.ToString(Session["LoginID"])),
                            txtGIN003Comment.Text,
                            string.Empty,
                            string.Empty,
                            carrierSendingGoods,
                            string.Empty,
                            txtCarriageCharge.Text,
                            string.Empty,
                            string.Empty,
                            sAction,
                            string.Empty,
                            string.Empty,
                            string.Empty,
                            txtNumPalletsExchange.Text,
                            string.Empty,
                            string.Empty,
                            string.Empty,
                            string.Empty,
                            string.Empty,
                            ReturnNoteID,
                            string.Empty,
                            string.Empty,
                            txtCarrierTrackingNum.Text,
                            txtNumCartonsExchange.Text,
                            pCostCenter: txtCostCenter.Text
                            );

                        oDiscrepancyBE.GINActionRequired = false;

                        oDiscrepancyBE.INVHTML = oWorkflowHTML.function3("white", null, "", "");
                        oDiscrepancyBE.INVActionRequired = false;

                        oDISLog_WorkFlowBE.Action = "GetAccountPayable";
                        VendorAccountPayablesEmail = oDISLog_WorkFlowBAL.GetVendorAccountPayablesEmail(oDISLog_WorkFlowBE);

                        if (Convert.ToDecimal(txtCarriageCharge.Text) > 0)
                        {
                            oDiscrepancyBE.APHTML = oWorkflowHTML.function2(DateTime.Now, null, "Blue", VendorAccountPayablesEmail,
                                                                            "Build on to Accounts Payable Work List on " + DateTime.Now.ToString("dd/MM/yyyy") + " with the following request",
                                                                            "Please credit/debit the account for the amount to cover carriage costs - " + txtCarriageCharge.Text.Trim() + "");
                            oDiscrepancyBE.APActionRequired = true;
                            oDiscrepancyBE.APUserControl = "ACP002";
                            oDiscrepancyBE.APActionCategory = "AR";  //Action Required
                        }
                        else
                        {
                            oDiscrepancyBE.APHTML = oWorkflowHTML.function3("white", null, "", "");
                            oDiscrepancyBE.APActionRequired = false;
                        }

                        oDiscrepancyBE.VENHTML = oWorkflowHTML.function3("white", null, "", "");
                        oDiscrepancyBE.VenActionRequired = false;

                        oDiscrepancyBE.Action = "InsertHTML";
                        oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
                        oDiscrepancyBE.LoggedDateTime = DateTime.Now;
                        oDiscrepancyBE.LevelNumber = 1;
                        oDiscrepancyBE.ActionTakenBy = "GIN";                        
                        int? iResult1 = oDiscrepancyBAL.addWorkFlowHTMLsBAL(oDiscrepancyBE);
                        //}
                        //else
                        //{
                        //    oDiscrepancyBE.GINHTML = oWorkflowHTML.function1(DateTime.Now,
                        //      null,
                        //      null,
                        //      "Yellow",
                        //      string.Empty,
                        //      string.Empty,
                        //      UIUtility.GetODVendorUserName(Convert.ToString(Session["Role"]), Convert.ToString(Session["UserName"]), Convert.ToString(Session["LoginID"])),
                        //      txtGIN003Comment.Text,
                        //      string.Empty,
                        //      string.Empty,
                        //      carrierSendingGoods,
                        //      string.Empty,
                        //      txtCarriageCharge.Text,
                        //      string.Empty,
                        //      string.Empty,
                        //      sAction,
                        //      string.Empty,
                        //      string.Empty,
                        //      string.Empty,
                        //      txtNumPalletsExchange.Text,
                        //      string.Empty,
                        //      string.Empty,
                        //      string.Empty,
                        //      string.Empty,
                        //      string.Empty,
                        //      ReturnNoteID,
                        //      string.Empty,
                        //      string.Empty,
                        //      txtCarrierTrackingNum.Text,
                        //      txtNumCartonsExchange.Text
                        //      );

                        //    oDiscrepancyBE.GINActionRequired = false;

                        //    oDiscrepancyBE.INVHTML = oWorkflowHTML.function3("white", null, "", "");
                        //    oDiscrepancyBE.INVActionRequired = false;

                        //    oDISLog_WorkFlowBE.Action = "GetAccountPayable";
                        //    VendorAccountPayablesEmail = oDISLog_WorkFlowBAL.GetVendorAccountPayablesEmail(oDISLog_WorkFlowBE);
                        //    oDiscrepancyBE.APHTML = oWorkflowHTML.function3("white", null, "", "");
                        //    oDiscrepancyBE.APActionRequired = false;

                        //    oDiscrepancyBE.VENHTML = oWorkflowHTML.function3("white", null, "", "");
                        //    oDiscrepancyBE.VenActionRequired = false;

                        //    oDiscrepancyBE.Action = "InsertHTML";
                        //    oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
                        //    oDiscrepancyBE.LoggedDateTime = DateTime.Now;
                        //    oDiscrepancyBE.LevelNumber = 1;
                        //    int? iResult1 = oDiscrepancyBAL.addWorkFlowHTMLsBAL(oDiscrepancyBE);
                        //}
                    }
                }
                else
                {
                    InsertHTML();
                }
            }
            else
            {
                //if stage is going to complete.
                if (chkGIN003StageCompleted.Checked)
                {

                    if (!string.IsNullOrEmpty(GINReturnType))
                    {
                        if (GINReturnType != "V")
                        {
                            oDiscrepancyBE.GINHTML = oWorkflowHTML.function11(GINReturnType, DateTime.Now,
                                null,
                                null,
                                "Yellow",
                                string.Empty,
                                string.Empty,
                                UIUtility.GetODVendorUserName(Convert.ToString(Session["Role"]), Convert.ToString(Session["UserName"]), Convert.ToString(Session["LoginID"])),
                                txtGIN003Comment.Text,
                                 string.Empty,
                                string.Empty,
                                strcarrierSendingGoods,
                                string.Empty,
                                strCarriageCharge,
                                string.Empty,
                                string.Empty,
                                sAction,
                                string.Empty,
                                string.Empty,
                                string.Empty,
                                string.Empty,
                                string.Empty,
                                string.Empty,
                                string.Empty,
                                string.Empty,
                                string.Empty,
                                ReturnNoteID,
                                string.Empty,
                                string.Empty,
                                string.Empty,
                                string.Empty
                                );

                            oDiscrepancyBE.GINActionRequired = false;

                            oDiscrepancyBE.INVHTML = oWorkflowHTML.function3("white", null, "", "");
                            oDiscrepancyBE.INVActionRequired = false;
                            if (GINReturnType == "D")
                            {
                                oDiscrepancyBE.APHTML = oWorkflowHTML.function3("white", null, "", "");
                                oDiscrepancyBE.APActionRequired = false;
                            }
                            if (GINReturnType == "O")
                            {
                                if (Convert.ToDecimal(txtCarriageCharge.Text) > 0)
                                {
                                    oDiscrepancyBE.APHTML = oWorkflowHTML.function2(DateTime.Now, null, "Blue", VendorAccountPayablesEmail,
                                                                       "Build on to Accounts Payable Work List on " + DateTime.Now.ToString("dd/MM/yyyy") + " with the following request",
                                                                       "Please credit/debit the account for the amount to cover carriage costs - " + txtCarriageCharge.Text.Trim() + "");
                                    oDiscrepancyBE.APActionRequired = true;
                                    oDiscrepancyBE.APUserControl = "ACP002";
                                    oDiscrepancyBE.APActionCategory = "AR";  //Action Required
                                }
                                else
                                {
                                    oDiscrepancyBE.APHTML = oWorkflowHTML.function3("white", null, "", "");
                                    oDiscrepancyBE.APActionRequired = false;
                                }
                            
                            }
                            oDiscrepancyBE.VENHTML = oWorkflowHTML.function3("white", null, "", "");
                            oDiscrepancyBE.VenActionRequired = false;

                            oDiscrepancyBE.Action = "InsertHTML";
                            oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
                            oDiscrepancyBE.LoggedDateTime = DateTime.Now;
                            oDiscrepancyBE.LevelNumber = 1;
                            oDiscrepancyBE.ActionTakenBy = "GIN";                            
                            int? iResult1 = oDiscrepancyBAL.addWorkFlowHTMLsBAL(oDiscrepancyBE);
                        }
                        else
                        {
                            string color = string.Empty;
                            string strCollectionAuthNo = string.Empty;
                            string sentTo = string.Empty;
                            string strBottomComment = string.Empty;
                            for (int i = 1; i <= 2; i++)
                            {
                                if (i == 1)
                                {
                                    color = "Yellow";
                                    txtGIN003Comment.Text = "";
                                    ReturnNoteID = null;
                                    GINReturnType = "";
                                    strCollectionAuthNo = txtCollectionAuthNum.Text;
                                    sentTo = "Goods In";
                                    strcarrierSendingGoods = ucTxtCarrierWhoCollectGoods.SelectedItem.Text;
                                }
                                else
                                {
                                    color = "Blue";
                                    sentTo = "Goods In";
                                    strBottomComment = "Goods In to confirm goods collection";
                                    txtGIN003Comment.Text = "";
                                    ReturnNoteID = null;
                                    GINReturnType = "";
                                    strCollectionAuthNo = "";
                                    strcarrierSendingGoods = "";
                                    GoodsCollectedOn = "";
                                }
                                oDiscrepancyBE.GINHTML = oWorkflowHTML.function12(GINReturnType, DateTime.Now,
                                 null,
                                 GoodsCollectedOn,
                                 color,
                                 strCollectionAuthNo,
                                 txtGIN003Comment.Text,
                                 Convert.ToString(Session["UserName"]),
                                 ReturnNoteID,
                                 sentTo,
                                 strcarrierSendingGoods,
                                 strBottomComment
                                 );
                                if (i == 1)
                                {
                                    oDiscrepancyBE.GINActionRequired = false;
                                }
                                else
                                {
                                    oDiscrepancyBE.GINActionRequired = true;
                                    oDiscrepancyBE.GINUserControl = "GIN001";
                                    oDiscrepancyBE.GINActionCategory = "RA";  //Remedial Action
                                }

                                oDiscrepancyBE.INVHTML = oWorkflowHTML.function3("white", null, "", "");
                                oDiscrepancyBE.INVActionRequired = false;
                                oDiscrepancyBE.APHTML = oWorkflowHTML.function3("white", null, "", "");
                                oDiscrepancyBE.APActionRequired = false;
                                oDiscrepancyBE.VENHTML = oWorkflowHTML.function3("white", null, "", "");
                                oDiscrepancyBE.VenActionRequired = false;

                                oDiscrepancyBE.Action = "InsertHTML";
                                oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
                                oDiscrepancyBE.LoggedDateTime = DateTime.Now;
                                oDiscrepancyBE.LevelNumber = 5;
                                oDiscrepancyBE.ActionTakenBy = "GIN";
                                int? iResult1 = oDiscrepancyBAL.addWorkFlowHTMLsBAL(oDiscrepancyBE);
                            }
                        }

                    }
                    else
                    {
                        //**********************************************
                        //DiscrepancyBE oDiscrepancyBE2 = new DiscrepancyBE();
                        //oDiscrepancyBE2.Action = "GetVendorSubscriptionStatus";
                        //oDiscrepancyBE2.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
                        //string VendorStatus = oDiscrepancyBAL.GetVendorSubscriptionStatusBAL(oDiscrepancyBE2);
                        //*************************************************
                        //if (VendorStatus == "True")
                        //{
                        oDiscrepancyBE.GINHTML = oWorkflowHTML.function1(DateTime.Now,
                            null,
                            null,
                            "Yellow",
                            string.Empty,
                            sAction,
                            UIUtility.GetODVendorUserName(Convert.ToString(Session["Role"]), Convert.ToString(Session["UserName"]), Convert.ToString(Session["LoginID"])),
                            txtGIN003Comment.Text,
                            string.Empty,
                            string.Empty,
                            carrierSendingGoods,
                            string.Empty,
                            txtCarriageCharge.Text,
                            string.Empty,
                            string.Empty,
                            string.Empty,
                            string.Empty,
                            string.Empty,
                            string.Empty,
                            txtNumPalletsExchange.Text,
                            string.Empty,
                            string.Empty,
                            string.Empty,
                            string.Empty,
                            string.Empty,
                            ReturnNoteID,
                            string.Empty,
                            string.Empty,
                            txtCarrierTrackingNum.Text,
                            txtNumCartonsExchange.Text,
                            pCostCenter: txtCostCenter.Text
                            );

                        oDiscrepancyBE.GINActionRequired = false;

                        oDiscrepancyBE.INVHTML = oWorkflowHTML.function3("white", null, "", "");
                        oDiscrepancyBE.INVActionRequired = false;


                        if (Convert.ToDecimal(txtCarriageCharge.Text) > 0)
                        {
                            oDiscrepancyBE.APHTML = oWorkflowHTML.function2(DateTime.Now, null, "Blue", VendorAccountPayablesEmail,
                                                                                                   "Build on to Accounts Payable Work List on " + DateTime.Now.ToString("dd/MM/yyyy") + " with the following request",
                                                                                                   "Please credit/debit the account for the amount to cover carriage costs.");
                            oDiscrepancyBE.APActionRequired = true;
                            oDiscrepancyBE.APUserControl = "ACP002";
                            oDiscrepancyBE.APActionCategory = "AR";  //Action Required
                        }
                        else
                        {
                            oDiscrepancyBE.APHTML = oWorkflowHTML.function3("white", null, "", "");
                            oDiscrepancyBE.APActionRequired = false;
                        }
                       

                        oDiscrepancyBE.VENHTML = oWorkflowHTML.function3("white", null, "", "");
                        oDiscrepancyBE.VenActionRequired = false;

                        oDiscrepancyBE.Action = "InsertHTML";
                        oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
                        oDiscrepancyBE.LoggedDateTime = DateTime.Now;
                        oDiscrepancyBE.LevelNumber = 1;
                        oDiscrepancyBE.ActionTakenBy = "GIN";
                        int? iResult1 = oDiscrepancyBAL.addWorkFlowHTMLsBAL(oDiscrepancyBE);
                        // }
                        //else
                        //{
                        //    oDiscrepancyBE.GINHTML = oWorkflowHTML.function1(DateTime.Now,
                        //                                  null,
                        //                                  null,
                        //                                  "Yellow",
                        //                                  string.Empty,
                        //                                  sAction,
                        //                                  UIUtility.GetODVendorUserName(Convert.ToString(Session["Role"]), Convert.ToString(Session["UserName"]), Convert.ToString(Session["LoginID"])),
                        //                                  txtGIN003Comment.Text,
                        //                                  string.Empty,
                        //                                  string.Empty,
                        //                                  carrierSendingGoods,
                        //                                  string.Empty,
                        //                                  txtCarriageCharge.Text,
                        //                                  string.Empty,
                        //                                  string.Empty,
                        //                                  string.Empty,
                        //                                  string.Empty,
                        //                                  string.Empty,
                        //                                  string.Empty,
                        //                                  txtNumPalletsExchange.Text,
                        //                                  string.Empty,
                        //                                  string.Empty,
                        //                                  string.Empty,
                        //                                  string.Empty,
                        //                                  string.Empty,
                        //                                  ReturnNoteID,
                        //                                  string.Empty,
                        //                                  string.Empty,
                        //                                  txtCarrierTrackingNum.Text,
                        //                                  txtNumCartonsExchange.Text
                        //                                  );

                        //    oDiscrepancyBE.GINActionRequired = false;

                        //    oDiscrepancyBE.INVHTML = oWorkflowHTML.function3("white", null, "", "");
                        //    oDiscrepancyBE.INVActionRequired = false;

                        //    oDiscrepancyBE.APHTML = oWorkflowHTML.function3("white", null, "", "");
                        //    oDiscrepancyBE.APActionRequired = false;
                        //    oDiscrepancyBE.VENHTML = oWorkflowHTML.function3("white", null, "", "");
                        //    oDiscrepancyBE.VenActionRequired = false;

                        //    oDiscrepancyBE.Action = "InsertHTML";
                        //    oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
                        //    oDiscrepancyBE.LoggedDateTime = DateTime.Now;
                        //    oDiscrepancyBE.LevelNumber = 1;
                        //    int? iResult1 = oDiscrepancyBAL.addWorkFlowHTMLsBAL(oDiscrepancyBE);
                        //}
                    }
                }
                else
                {
                    InsertHTML();
                }
            }
        }


        //Redirecting to Previous Page
        RedirectToPage(GetQueryStringValue("PreviousPage"));
    }

    private int? PrintAndSaveReturnNote()
    {
        decimal TotalCost = 0;
        decimal NetTotalCost = 0;
        decimal FrieghtCharges = 0;
        string htmlBody = string.Empty;
        string DiscrepancyType = string.Empty;
        // vendor return address
        DiscrepancyBE oNewDiscrepancyBE = new DiscrepancyBE();
        DiscrepancyBAL oNewDiscrepancyBAL = new DiscrepancyBAL();
        oNewDiscrepancyBE.Action = "GetDiscrepancySiteVendorDetail";
        oNewDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID"));

        List<DiscrepancyBE> lstVendor = oNewDiscrepancyBAL.GetDiscrepancyLogSiteVendorDetailsBAL(oNewDiscrepancyBE);
        if (lstVendor.Count > 0)
            DiscrepancyType = lstVendor[0].DiscrepancyType;
        string VendorAddress = string.Empty;
        if (string.IsNullOrEmpty(lstVendor[0].Vendor.ReturnAddress))
        {
            VendorAddress = lstVendor[0].Vendor.address1 + "<br />" +
                lstVendor[0].Vendor.address2 + "<br />" +
                lstVendor[0].Vendor.city + "<br />" +
                lstVendor[0].Vendor.VMPPIN + " " + lstVendor[0].Vendor.VMPPOU;

        }
        else
        {
            VendorAddress = lstVendor[0].Vendor.ReturnAddress.Replace("\r\n", "<br />");
        }
        // site address
        string SiteAddress = lstVendor[0].Site.SiteAddressLine1 + "<br />" +
           lstVendor[0].Site.SiteAddressLine2 + "<br />" +
           lstVendor[0].Site.SiteAddressLine3 + "<br />" +
           lstVendor[0].Site.SiteAddressLine4 + "<br />" +
           lstVendor[0].Site.SiteAddressLine5 + "<br />" +
           lstVendor[0].Site.SiteAddressLine6 + "<br />" +
           lstVendor[0].Site.SitePincode;

        oNewDiscrepancyBE.Action = "GetDiscrepancyLogITemsForReturnNote";
        List<DiscrepancyBE> lstItems = oNewDiscrepancyBAL.GetDiscrepancyItemsForReturnNote(oNewDiscrepancyBE);
        string DiscrepancyItems = string.Empty;
        string InCorrectDiscrepancyItems = string.Empty;
        int count = lstItems.Count / 2;
        int tempCount = 0;
        if (lstItems != null && lstItems.Count > 0)
        {
            foreach (DiscrepancyBE item in lstItems)
            {
                if (count == 0 || tempCount < count)
                {
                    if (string.IsNullOrEmpty(item.ODSKUCode) && !string.IsNullOrEmpty(item.ProductDescription))
                    {
                        DiscrepancyItems += "<tr><td>" + item.DeliveredQuantity
                                                  + "</td><td>" + item.UOM
                                                  + "</td><td>" + item.ODSKUCode
                                                  + "</td><td>" + item.DirectCode
                                                  + "</td><td>" + item.ProductDescription;
                        if (lstItems[0].DiscrepancyTypeID == 12)
                        {
                            DiscrepancyItems += "</td><td>" + item.OriginalQuantity;
                            DiscrepancyItems += "</td><td>" + item.OutstandingQuantity;

                            DiscrepancyItems += "</td><td style='text-align:left;'>" + item.ItemVal
                                         + "</td><td style='text-align:right;'>";
                        }
                        else
                        {
                            DiscrepancyItems += "</td><td style='text-align:right;'>" + item.ItemVal
                                             + "</td><td style='text-align:right;'>";
                        }

                        if (item.DeliveredQuantity.HasValue && !string.IsNullOrEmpty(item.ItemVal))
                        {
                            TotalCost += Convert.ToDecimal(item.ItemVal) * item.DeliveredQuantity.Value;
                            DiscrepancyItems += Convert.ToDecimal(item.ItemVal) * item.DeliveredQuantity.Value;
                        }
                        else
                        {
                            DiscrepancyItems += string.Empty;
                        }
                        DiscrepancyItems += "</td></tr>";
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(item.ODSKUCode))
                        {
                            DiscrepancyItems += "<tr><td>" + item.DeliveredQuantity
                                + "</td><td>" + item.UOM
                                + "</td><td>" + item.ODSKUCode
                                + "</td><td>" + item.DirectCode
                                + "</td><td>" + item.ProductDescription;

                            if (lstItems[0].DiscrepancyTypeID == 12)
                            {
                                DiscrepancyItems += "</td><td>" + item.OriginalQuantity;
                                DiscrepancyItems += "</td><td>" + item.OutstandingQuantity;

                                DiscrepancyItems += "</td><td style='text-align:left;'>" + item.ItemVal
                            + "</td><td style='text-align:right;'>";
                            }
                            else
                            {
                                DiscrepancyItems += "</td><td style='text-align:right;'>" + item.ItemVal
                         + "</td><td style='text-align:right;'>";
                            }


                            if (item.DeliveredQuantity.HasValue && !string.IsNullOrEmpty(item.ItemVal))
                            {
                                TotalCost += Convert.ToDecimal(item.ItemVal) * item.DeliveredQuantity.Value;
                                DiscrepancyItems += Convert.ToDecimal(item.ItemVal) * item.DeliveredQuantity.Value;
                            }
                            else
                            {
                                DiscrepancyItems += string.Empty;
                            }
                            DiscrepancyItems += "</td></tr>";
                        }
                    }
                }
                else
                {
                    InCorrectDiscrepancyItems += "<tr><td>" + item.DeliveredQuantity
                    + "</td><td>" + item.UOM
                    + "</td><td>" + item.ODSKUCode
                    + "</td><td>" + item.DirectCode
                    + "</td><td>" + item.ProductDescription;
                    DiscrepancyItems += "</td></tr>";
                }
                tempCount++;
            }
        }
        if (!string.IsNullOrEmpty(txtCarriageCharge.Text))
        {
            FrieghtCharges = Convert.ToDecimal(txtCarriageCharge.Text);
        }
        NetTotalCost = TotalCost + FrieghtCharges;
        string VendorLanguage = lstVendor[0].Vendor.Language;
        //string templateFile = "~/EmailTemplates/ReturnNote/ReturnNote." + VendorLanguage + ".htm";
        string templateFile = string.Empty;
        if (lstItems[0].DiscrepancyTypeID == 12)
        {
            templateFile = "~/EmailTemplates/ReturnNote/ReturnNoteQuality.english.htm";
        }
        else
        {
            templateFile = "~/EmailTemplates/ReturnNote/ReturnNote.english.htm";
        }



        #region Setting reason as per the language ...
        switch (VendorLanguage)
        {
            case clsConstants.English:
                Page.UICulture = clsConstants.EnglishISO; // "en-US"; // English
                break;
            case clsConstants.French:
                Page.UICulture = clsConstants.FranceISO; // // France
                break;
            case clsConstants.German:
                Page.UICulture = clsConstants.GermanyISO; //"de"; // German
                break;
            case clsConstants.Dutch:
                Page.UICulture = clsConstants.NederlandISO; //"nl"; // Dutch
                break;
            case clsConstants.Spanish:
                Page.UICulture = clsConstants.SpainISO; //"es"; // Spanish
                break;
            case clsConstants.Italian:
                Page.UICulture = clsConstants.ItalyISO; //"it"; // Italy
                break;
            case clsConstants.Czech:
                Page.UICulture = clsConstants.CzechISO; //"cz"; // Czech
                break;
            default:
                Page.UICulture = clsConstants.EnglishISO; //"en-US"; // English
                break;
        }

        #endregion

        string contactPerson = string.Empty;

        sendCommunicationCommon osendCommunicationCommon = new sendCommunicationCommon();
        if (System.IO.File.Exists(System.Web.HttpContext.Current.Server.MapPath(templateFile.ToLower())))
        {
            using (StreamReader sReader = new StreamReader(System.Web.HttpContext.Current.Server.MapPath(templateFile.ToLower())))
            {
                htmlBody = sReader.ReadToEnd();
                if (DiscrepancyType != "Incorrect Product Code")
                    htmlBody = htmlBody.Replace("block", "none");

                htmlBody = htmlBody.Replace("{VendorDiscrepancyReturnNumber}", WebCommon.getGlobalResourceValue("VendorDiscrepancyReturnNumber"));
                htmlBody = htmlBody.Replace("{VDRNoValue}", lstVendor[0].VDRNo);

                htmlBody = htmlBody.Replace("{logoInnerPath}", osendCommunicationCommon.getAbsolutePath());

                htmlBody = htmlBody.Replace("{From}", WebCommon.getGlobalResourceValue("From"));
                htmlBody = htmlBody.Replace("{SiteAddressValue}", SiteAddress);

                htmlBody = htmlBody.Replace("{VendorDiscrepancyReturnNote}", WebCommon.getGlobalResourceValue("VendorDiscrepancyReturnNote"));

                htmlBody = htmlBody.Replace("{VendorNumberSDR}", WebCommon.getGlobalResourceValue("VendorNumberSDR"));
                htmlBody = htmlBody.Replace("{VendorNumberValue}", lstVendor[0].Vendor.Vendor_No);

                htmlBody = htmlBody.Replace("{VendorName}", WebCommon.getGlobalResourceValue("VendorName"));
                htmlBody = htmlBody.Replace("{VendorNameValue}", string.IsNullOrEmpty(lstVendor[0].Vendor.VendorContactName) ? lstVendor[0].Vendor.VendorName : lstVendor[0].Vendor.VendorContactName);

                htmlBody = htmlBody.Replace("{Returnto}", WebCommon.getGlobalResourceValue("Returnto"));
                htmlBody = htmlBody.Replace("{VendorAddressValue}", VendorAddress);

                htmlBody = htmlBody.Replace("{ReturnNoteText1}", WebCommon.getGlobalResourceValue("ReturnNoteText1"));
                htmlBody = htmlBody.Replace("{ReturnNoteText2}", WebCommon.getGlobalResourceValue("ReturnNoteText2"));

                htmlBody = htmlBody.Replace("{AuthorisedBy}", WebCommon.getGlobalResourceValue("AuthorisedBy"));
                htmlBody = htmlBody.Replace("{VendorAuthorisationReference}", WebCommon.getGlobalResourceValue("VendorAuthorisationReference"));
                htmlBody = htmlBody.Replace("{ReasonforReturn}", WebCommon.getGlobalResourceValue("ReasonforReturn"));


                htmlBody = htmlBody.Replace("{VendorContactValue}", GetVendorContactName());
                if (lstItems != null && lstItems.Count > 0)
                    htmlBody = htmlBody.Replace("{VendorAuthorisationReferenceValue}", lstItems[0].CollectionAuthNumber);
                else
                    htmlBody = htmlBody.Replace("{VendorAuthorisationReferenceValue}", "-");

                htmlBody = htmlBody.Replace("{VENDORDISREPACNYTYPEValue}", lstVendor[0].DiscrepancyType);

                htmlBody = htmlBody.Replace("{QTY}", WebCommon.getGlobalResourceValue("QTY"));
                htmlBody = htmlBody.Replace("{UOM}", WebCommon.getGlobalResourceValue("UOM"));
                htmlBody = htmlBody.Replace("{OURPRODUCTCODE}", WebCommon.getGlobalResourceValue("OURPRODUCTCODE"));
                htmlBody = htmlBody.Replace("{VENDORREFERENCE}", WebCommon.getGlobalResourceValue("VENDORREFERENCE"));
                htmlBody = htmlBody.Replace("{DESCRIPTION}", WebCommon.getGlobalResourceValue("DESCRIPTION"));
                htmlBody = htmlBody.Replace("{COST}", WebCommon.getGlobalResourceValue("COST"));
                htmlBody = htmlBody.Replace("{NET}", WebCommon.getGlobalResourceValue("NET"));

                // quatity return notes changes//
                if (lstItems[0].DiscrepancyTypeID == 12)
                {
                    htmlBody = htmlBody.Replace("{POQuantity}", WebCommon.getGlobalResourceValue("OriginalPOQuantity"));
                    htmlBody = htmlBody.Replace("{OUTSTANDING}", WebCommon.getGlobalResourceValue("OutstandingPOQuantity"));
                }

                htmlBody = htmlBody.Replace("{DiscrepancyItemsValue}", DiscrepancyItems);

                htmlBody = htmlBody.Replace("{ReturnTotal}", WebCommon.getGlobalResourceValue("ReturnTotal"));
                htmlBody = htmlBody.Replace("{TotalCostValue}", TotalCost.ToString());

                htmlBody = htmlBody.Replace("{FreightCharge}", WebCommon.getGlobalResourceValue("FreightCharge"));
                htmlBody = htmlBody.Replace("{FreightChargeValue}", FrieghtCharges.ToString());
                // *******  New changes
                htmlBody = htmlBody.Replace("{ReturnNoteIncorrectProductDec}", WebCommon.getGlobalResourceValue("ReturnNoteIncorrectProductDec"));
                htmlBody = htmlBody.Replace("{DiscrepancyItemsValue1}", InCorrectDiscrepancyItems);
                //******************************
                htmlBody = htmlBody.Replace("{Total}", WebCommon.getGlobalResourceValue("Total"));
                htmlBody = htmlBody.Replace("{NetTotalCostValue}", NetTotalCost.ToString());

                htmlBody = htmlBody.Replace("{PreparedBy}", WebCommon.getGlobalResourceValue("PreparedBy"));
                htmlBody = htmlBody.Replace("{OFFICE_DEPOT_DC_CONTACTValue}", Session["UserName"].ToString());

                htmlBody = htmlBody.Replace("{DatePrepared}", WebCommon.getGlobalResourceValue("DatePrepared"));
                htmlBody = htmlBody.Replace("{DATEPREPAREDValue}", DateTime.Now.ToString("dd/MM/yyyy"));

                htmlBody = htmlBody.Replace("{NumberofPallets}", WebCommon.getGlobalResourceValue("NumberofPallets"));
                htmlBody = htmlBody.Replace("{NumberofPalletsValue}", txtNumPalletsExchange.Text.Trim());

                htmlBody = htmlBody.Replace("{NumberofCartons}", WebCommon.getGlobalResourceValue("NumberofCartons"));
                htmlBody = htmlBody.Replace("{NumberofCartonsValue}", txtNumCartonsExchange.Text.Trim());

                htmlBody = htmlBody.Replace("{CollectionReturnDate}", WebCommon.getGlobalResourceValue("CollectionReturnDate"));
                if (!string.IsNullOrEmpty(txtCollectionDate.Text.ToString()))
                {
                    htmlBody = htmlBody.Replace("{ColletionDateValue}", txtCollectionDate.Text.ToString());
                }
                else
                {
                    htmlBody = htmlBody.Replace("{ColletionDateValue}", DateTime.Now.ToString("dd/MM/yyyy"));
                }

                htmlBody = htmlBody.Replace("{Signed}", WebCommon.getGlobalResourceValue("Signed"));

                htmlBody = htmlBody.Replace("{Carrier}", WebCommon.getGlobalResourceValue("Carrier"));
                if (ucTxtCarrierWhoCollectGoods.SelectedItem.Text != null && ucTxtCarrierWhoCollectGoods.SelectedItem.Text != "----Select----")
                {
                    htmlBody = htmlBody.Replace("{CarrierValue}", ucTxtCarrierWhoCollectGoods.SelectedItem.Text);
                }
                else if (ddlCarrierSendingGoodsBack.SelectedItem != null)
                    htmlBody = htmlBody.Replace("{CarrierValue}", ddlCarrierSendingGoodsBack.SelectedItem.Text);
                else
                    htmlBody = htmlBody.Replace("{CarrierValue}", "");

                htmlBody = htmlBody.Replace("{VEHICLEREG}", WebCommon.getGlobalResourceValue("VEHICLEREG"));
                htmlBody = htmlBody.Replace("{DRIVER}", WebCommon.getGlobalResourceValue("DRIVER"));
                htmlBody = htmlBody.Replace("{SIGN}", WebCommon.getGlobalResourceValue("SIGN"));
                htmlBody = htmlBody.Replace("{Print}", WebCommon.getGlobalResourceValue("Print"));


                // changes for alternate Return note 
                htmlBody = htmlBody.Replace("{PalletExchangeInfo}", WebCommon.getGlobalResourceValue("PalletExchangeInfo"));
                htmlBody = htmlBody.Replace("{OnewayPallets}", WebCommon.getGlobalResourceValue("OneWayPallets"));
                htmlBody = htmlBody.Replace("{EuroPallets}", WebCommon.getGlobalResourceValue("NumberEuroPallets"));
                htmlBody = htmlBody.Replace("{EuroPalletsChanged}", WebCommon.getGlobalResourceValue("EuroPalletsChanged"));
                // changes end

                htmlBody = htmlBody.Replace("{ImportantNotice}", WebCommon.getGlobalResourceValue("ImportantNotice"));
                htmlBody = htmlBody.Replace("{ReturnNoteText3}", WebCommon.getGlobalResourceValue("ReturnNoteText3"));
                htmlBody = htmlBody.Replace("{ReturnNoteText4}", WebCommon.getGlobalResourceValue("ReturnNoteText4"));

                contactPerson = GetAPContact(lstVendor[0].VendorID);
                htmlBody = htmlBody.Replace("{ACCOUNTS PAYABLE CONTACT}", string.IsNullOrEmpty(contactPerson) ? "<Not Available>" : contactPerson);
                if (lstVendor[0].Site.SiteMangerUserID != null)
                    contactPerson = GetDCSiteContact(lstVendor[0].Site.SiteMangerUserID);

                htmlBody = htmlBody.Replace("{OFFICE DEPOT DC SITE CONTACT}", string.IsNullOrEmpty(contactPerson) ? "<Not Available>" : contactPerson);

                htmlBody = htmlBody.Replace("{POHash}", WebCommon.getGlobalResourceValue("POHash"));
                htmlBody = htmlBody.Replace("{POValue}", lstItems[0].PurchaseOrderNumber);
            }
        }
        Page.UICulture = Convert.ToString(Session["CultureInfo"]);

        // Save into the database
        DiscrepancyReturnNoteBE oDiscrepancyReturnNoteBE = new DiscrepancyReturnNoteBE();
        oDiscrepancyReturnNoteBE.Action = "AddDiscrepancyReturnNote";
        oDiscrepancyReturnNoteBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID"));
        oDiscrepancyReturnNoteBE.LoggedDate = DateTime.Now;
        oDiscrepancyReturnNoteBE.ReturnNoteBody = htmlBody.ToString();

        int? retval = oNewDiscrepancyBAL.AddDiscrepancyReturnNote(oDiscrepancyReturnNoteBE);


        // Print Return note
        Session["LabelHTML"] = htmlBody.ToString();

        string a = "window.open('../../LogDiscrepancy/DIS_PrintLetter.aspx', '', 'left =' + screen.width + ',top=' + screen.height + ',width=0,height=0,toolbar=0,scrollbars=0,status=0')";
        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "pp", a, true);

        return retval;
    }
    #endregion

    #region Methods
    private string GetVendorContactName()
    {
        string DiscrepancyVendorContact = string.Empty;
        DiscrepancyBE oNewDiscrepancyBE = new DiscrepancyBE();
        DiscrepancyBAL oNewDiscrepancyBAL = new DiscrepancyBAL();
        oNewDiscrepancyBE.Action = "GetDiscrepancyVendorContact";
        oNewDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID"));
        oNewDiscrepancyBE = oNewDiscrepancyBAL.GetDiscrepancyVendorContact(oNewDiscrepancyBE);
        if (!string.IsNullOrEmpty(oNewDiscrepancyBE.Username))
            DiscrepancyVendorContact = oNewDiscrepancyBE.Username;
        return DiscrepancyVendorContact;


    }

    private string GetDCSiteContact(int? UserID)
    {
        string DCSiteContact = string.Empty;
        SCT_UserBE oSCT_UserBE = new SCT_UserBE();
        oSCT_UserBE.UserID = UserID.Value;
        oSCT_UserBE.Action = "ShowAll";
        SCT_UserBAL oSCT_UserBAL = new SCT_UserBAL();
        List<SCT_UserBE> lstUser = oSCT_UserBAL.GetUserDetailsBAL(oSCT_UserBE);
        if (lstUser != null && lstUser.Count > 0)
        {
            DCSiteContact = lstUser[0].FirstName + " " + lstUser[0].Lastname + " " + lstUser[0].PhoneNumber;
        }
        return DCSiteContact;
    }

    private string GetAPContact(int? VendorID)
    {
        string DCSiteContact = string.Empty;
        SCT_UserBE oSCT_UserBE = new SCT_UserBE();
        oSCT_UserBE.VendorID = VendorID.Value;
        oSCT_UserBE.Action = "GetAccountPayableDetail";
        SCT_UserBAL oSCT_UserBAL = new SCT_UserBAL();
        List<SCT_UserBE> lstUser = oSCT_UserBAL.GetUserDetailsBAL(oSCT_UserBE);
        if (lstUser != null && lstUser.Count > 0)
        {
            DCSiteContact = lstUser[0].FirstName + " " + lstUser[0].Lastname + " " + lstUser[0].PhoneNumber;
        }
        return DCSiteContact;
    }

    private void InsertHTML()
    {

        DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
        DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();

        WorkflowHTML oWorkflowHTML = new WorkflowHTML();


        oDiscrepancyBE.GINHTML = oWorkflowHTML.function3("White", DateTime.Now, txtGIN003Comment.Text.Trim(), "");

        oDiscrepancyBE.Action = "InsertHTML";
        oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
        oDiscrepancyBE.LoggedDateTime = DateTime.Now;
        oDiscrepancyBE.LevelNumber = 1;

        int? iResult = oDiscrepancyBAL.addWorkFlowHTMLsBAL(oDiscrepancyBE);
    }

    #endregion

    protected void btnBack_Click(object sender, EventArgs e)
    {
        //Redirecting to Previous Page
        RedirectToPage(GetQueryStringValue("PreviousPage"));
    }
}
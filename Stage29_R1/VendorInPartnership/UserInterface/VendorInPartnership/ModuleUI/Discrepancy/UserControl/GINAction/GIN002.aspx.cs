﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Web.UI;
using BusinessEntities.ModuleBE.Appointment.CountrySetting;
using BusinessEntities.ModuleBE.Discrepancy.LogDiscrepancy;
using BusinessEntities.ModuleBE.Security;
using BusinessLogicLayer.ModuleBAL.Appointment.CountrySetting;
using BusinessLogicLayer.ModuleBAL.Discrepancy.LogDiscrepancy;
using BusinessLogicLayer.ModuleBAL.Security;
using Utilities; using WebUtilities;



public partial class GINAction_GIN002 : CommonPage
{

    #region Global variables

    string sAction = string.Empty;
    string sGoodsNotCollectedWarning = WebCommon.getGlobalResourceValue("GoodsNotCollectedWarning");

    #endregion

    #region Events

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            chkGIN002StageCompleted.Checked = true;
            //chkGIN002StageCompleted.Enabled = false;

            BindCarrier();
            GetVendorCollectionData();
            txtCollectionDate.Attributes.Add("readonly", "readonly");
        }
        //rbnGoodsCollected.Checked = true;
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        //if (rbnGoodsNotCollected.Checked && chkGIN002StageCompleted.Checked) {
        //    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('" + sGoodsNotCollectedWarning + "')", true);
        //    return;
        //}
       // btnSave.Enabled = false;
        DISLog_WorkFlowBE oDISLog_WorkFlowBE = new DISLog_WorkFlowBE();
        DISLog_WorkFlowBAL oDISLog_WorkFlowBAL = new DISLog_WorkFlowBAL();
        if (GetQueryStringValue("disLogID") != null && GetQueryStringValue("disLogID").ToString() != "")
        {
            //**********************************************************
            string Location = hdfLocation.Value;
            DiscrepancyBE oDiscrepancyBE1 = new DiscrepancyBE();
            DiscrepancyBAL oDiscrepancyBAL1 = new DiscrepancyBAL();
            oDiscrepancyBE1.Action = "UpdateLocation";
            oDiscrepancyBE1.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
            oDiscrepancyBE1.Location = Location;
            oDiscrepancyBE1.UserID = Convert.ToInt32(Session["UserID"]);
            string sResult1 = oDiscrepancyBAL1.updateDiscrepancyLocationBAL(oDiscrepancyBE1);
            //**********************************************************
            // Print Return note from here.
            int? ReturnNoteID = PrintAndSaveReturnNote();

            oDISLog_WorkFlowBE.Action = "InsertUpdateWorkflowActions";
            oDISLog_WorkFlowBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
            oDISLog_WorkFlowBE.GIN002_IsGoodsCollected = rbnGoodsCollected.Checked ? true : false;
            oDISLog_WorkFlowBE.GIN002_ArrangedCollectionDate = Convert.ToDateTime(Common.GetMM_DD_YYYY(txtCollectionDate.Text.Trim()));
            oDISLog_WorkFlowBE.GIN002_CollectionAuthorisationNumber = txtCollectionAuthNum.Text.Trim();
            oDISLog_WorkFlowBE.Comment = txtGIN002Comment.Text;
            oDISLog_WorkFlowBE.CarrierWhoWillCollectGoods = Convert.ToInt32(ucTxtCarrierWhoCollectGoods.SelectedItem.Value);
            oDISLog_WorkFlowBE.GIN003_PalletsExchanged = txtNumPalletsExchange.Text.Trim() != "" ? Convert.ToInt32(txtNumPalletsExchange.Text.Trim()) : (int?)null;
            oDISLog_WorkFlowBE.GIN003_CartonsExchanged = txtNumCartonsExchange.Text.Trim() != "" ? Convert.ToInt32(txtNumCartonsExchange.Text.Trim()) : (int?)null;
            oDISLog_WorkFlowBE.StageCompletedForWhom = "GIN002_StageCompleted";
            oDISLog_WorkFlowBE.StageCompleted = chkGIN002StageCompleted.Checked ? true : false;

            //update the work flow action table each time for the comments and other fields( if stage is completed)
            oDISLog_WorkFlowBE.DiscrepancyWorkFlowID = Convert.ToInt32(GetQueryStringValue("DisWFlowID").ToString());
            oDISLog_WorkFlowBE.Decision = rbnGoodsCollected.Checked ? "Goods collected" : "Goods not collected";

            oDISLog_WorkFlowBE.CostCenter = txtCostCenter.Text.Trim();

            //insert or update the record according to the stage completed status
            int? iResult = oDISLog_WorkFlowBAL.addWorkFlowActionsBAL(oDISLog_WorkFlowBE);

            //now insert the work flow HTML according to the action taken only if the stage is completed
            DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
            DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();
            WorkflowHTML oWorkflowHTML = new WorkflowHTML();

            sAction = rbnGoodsCollected.Checked ? "Goods collected" : "Goods not collected";

            if (GetQueryStringValue("FromPage") != null && GetQueryStringValue("FromPage").ToString().Trim() == "IncorrectProduct"
                || GetQueryStringValue("FromPage") != null && GetQueryStringValue("FromPage").ToString().Trim() == "NoPO"
                || GetQueryStringValue("FromPage") != null && GetQueryStringValue("FromPage").ToString().Trim() == "NoPaperwork"
                || GetQueryStringValue("FromPage") != null && GetQueryStringValue("FromPage").ToString().Trim() == "overs"
                || GetQueryStringValue("FromPage") != null && GetQueryStringValue("FromPage").ToString().Trim() == "ItemNotOnPo"
               )
            {
                //Goods In confirm the collection is completed on 
                //the arranged date. No further actions so discrepancy is closed. 
                //If collection is not completed then go to step 6c
                if (chkGIN002StageCompleted.Checked)
                {
                    if (rbnGoodsCollected.Checked)
                    {
                        //step - 4a
                        oDiscrepancyBE.GINHTML = oWorkflowHTML.function1(DateTime.Now,
                            null,
                            Convert.ToDateTime(Common.GetMM_DD_YYYY(txtCollectionDate.Text.Trim())),
                            "yellow",
                            string.Empty,
                            sAction,
                            UIUtility.GetODVendorUserName(Convert.ToString(Session["Role"]), Convert.ToString(Session["UserName"]), Convert.ToString(Session["LoginID"])),
                            txtGIN002Comment.Text,
                            string.Empty,
                            string.Empty,
                            string.Empty,
                            string.Empty,
                            string.Empty,
                            string.Empty,
                            string.Empty,
                            string.Empty,
                            string.Empty,
                            string.Empty,
                            string.Empty,
                            txtNumPalletsExchange.Text,
                            string.Empty,
                            string.Empty,
                            string.Empty,
                            string.Empty,
                            string.Empty,
                            ReturnNoteID,
                            string.Empty,
                            string.Empty,
                            string.Empty,
                            txtNumCartonsExchange.Text,
                            pCostCenter:txtCostCenter.Text
                        );

                        oDiscrepancyBE.GINActionRequired = false;

                        oDiscrepancyBE.INVHTML = oWorkflowHTML.function3("white", null);
                        oDiscrepancyBE.INVActionRequired = false;

                        oDiscrepancyBE.APHTML = oWorkflowHTML.function3("white", null);
                        oDiscrepancyBE.APActionRequired = false;

                        oDiscrepancyBE.VENHTML = oWorkflowHTML.function3("white", null);
                        oDiscrepancyBE.VenActionRequired = false;

                        oDiscrepancyBE.Action = "InsertHTML";
                        oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
                        oDiscrepancyBE.LoggedDateTime = DateTime.Now;
                        oDiscrepancyBE.LevelNumber = 1;

                        oDiscrepancyBE.CloseDiscrepancy = true;
                        int? iResult1 = oDiscrepancyBAL.addWorkFlowHTMLsBAL(oDiscrepancyBE);
                    }
                    else
                    {
                        // The code spinnet got never executed.
                        // If gets executed, modify function4 for returnnoteID.
                        //step - 6c
                        oDiscrepancyBE.GINHTML = oWorkflowHTML.function4("Blue", "", null, "Goods-In", "", "", "Please arrange for goods to be returned to the vendor and confirm the carriage charge", "", "", DateTime.Now);
                        oDiscrepancyBE.GINActionRequired = true;
                        oDiscrepancyBE.GINUserControl = "GIN003";

                        oDiscrepancyBE.INVHTML = oWorkflowHTML.function3("white", null);
                        oDiscrepancyBE.INVActionRequired = false;

                        oDiscrepancyBE.APHTML = oWorkflowHTML.function3("white", null);
                        oDiscrepancyBE.APActionRequired = false;

                        oDiscrepancyBE.VENHTML = oWorkflowHTML.function4("Blue", "", DateTime.Now, "ajayc@damcogroup.com", "Admin@OfficeDepot.com", "Escalation communication sent asking for a response to the discrepancy(Letter Type 4)");
                        oDiscrepancyBE.VenActionRequired = false;

                        oDiscrepancyBE.Action = "InsertHTML";
                        oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
                        oDiscrepancyBE.LoggedDateTime = DateTime.Now;
                        oDiscrepancyBE.LevelNumber = 1;
                        oDiscrepancyBE.ActionTakenBy = "GIN";
                        oDiscrepancyBE.GINActionCategory = "AR";  //Action Required
                        int? iResult1 = oDiscrepancyBAL.addWorkFlowHTMLsBAL(oDiscrepancyBE);
                    }
                }
                else
                {
                    /*Enter comments with date*/
                    InsertHTML();
                }

            }
            else if (GetQueryStringValue("FromPage") != null && GetQueryStringValue("FromPage").ToString().Trim() == "QualityIssue"
                  || GetQueryStringValue("FromPage") != null && GetQueryStringValue("FromPage").ToString().Trim() == "GoodsReceivedDamaged"
                  || GetQueryStringValue("FromPage") != null && GetQueryStringValue("FromPage").ToString().Trim() == "WrongPackSize")
            {
                if (chkGIN002StageCompleted.Checked)
                {
                    if (rbnGoodsCollected.Checked)
                    {
                        //step - 4a
                        oDiscrepancyBE.GINHTML = oWorkflowHTML.function1(DateTime.Now, 
                            null,
                            Convert.ToDateTime(Common.GetMM_DD_YYYY(txtCollectionDate.Text.Trim())),
                            "yellow",
                            string.Empty,
                            sAction,
                            UIUtility.GetODVendorUserName(Convert.ToString(Session["Role"]), Convert.ToString(Session["UserName"]), Convert.ToString(Session["LoginID"])),
                            txtGIN002Comment.Text,
                            string.Empty,
                            string.Empty,
                            string.Empty,
                            string.Empty,
                            string.Empty,
                            string.Empty,
                            string.Empty,
                            string.Empty,
                            string.Empty,
                            string.Empty,
                            string.Empty,
                            txtNumPalletsExchange.Text,
                            string.Empty,
                            string.Empty,
                            string.Empty,
                            string.Empty,
                            string.Empty,
                            ReturnNoteID,
                            string.Empty,
                            string.Empty,
                            string.Empty,
                            txtNumCartonsExchange.Text,
                            pCostCenter:txtCostCenter.Text
                        );

                        oDiscrepancyBE.GINActionRequired = false;

                        oDiscrepancyBE.INVHTML = oWorkflowHTML.function3("white", null);
                        oDiscrepancyBE.INVActionRequired = false;

                        oDiscrepancyBE.APHTML = oWorkflowHTML.function3("white", null);
                        oDiscrepancyBE.APActionRequired = false;

                        oDiscrepancyBE.VENHTML = oWorkflowHTML.function3("white", null);
                        oDiscrepancyBE.VenActionRequired = false;

                        oDiscrepancyBE.Action = "InsertHTML";
                        oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
                        oDiscrepancyBE.LoggedDateTime = DateTime.Now;
                        oDiscrepancyBE.LevelNumber = 1;

                        //Check whether ACP001 has completed its stage or not..If Yes then Close Discrepancy otherwise not.
                        oDISLog_WorkFlowBE.Action = "GetDataForSpecificAction";
                        oDISLog_WorkFlowBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
                        oDISLog_WorkFlowBE.StageCompletedForWhom = "ACP001_StageCompleted";
                        DataTable dtACO001 = oDISLog_WorkFlowBAL.GetDataForSpecificActionBAL(oDISLog_WorkFlowBE);
                        if (dtACO001 != null && dtACO001.Rows.Count > 0)
                        {
                            oDiscrepancyBE.CloseDiscrepancy = true;
                        }

                        int? iResult1 = oDiscrepancyBAL.addWorkFlowHTMLsBAL(oDiscrepancyBE);
                    }
                    else
                    {
                        // The code spinnet got never executed.
                        // If gets executed, modify function4 for returnnoteID.
                        //step - 6c
                        oDiscrepancyBE.GINHTML = oWorkflowHTML.function4("Blue", "", null, "Goods-In", "", "", "Please arrange for goods to be returned to the vendor and confirm the carriage charge", "", "", DateTime.Now);
                        oDiscrepancyBE.GINActionRequired = true;
                        oDiscrepancyBE.GINUserControl = "GIN003";

                        oDiscrepancyBE.INVHTML = oWorkflowHTML.function3("white", null);
                        oDiscrepancyBE.INVActionRequired = false;

                        oDiscrepancyBE.APHTML = oWorkflowHTML.function3("white", null);
                        oDiscrepancyBE.APActionRequired = false;

                        oDiscrepancyBE.VENHTML = oWorkflowHTML.function4("Blue", "", DateTime.Now, "ajayc@damcogroup.com", "Admin@OfficeDepot.com", "Escalation communication sent asking for a response to the discrepancy(Letter Type 4)");
                        oDiscrepancyBE.VenActionRequired = false;

                        oDiscrepancyBE.Action = "InsertHTML";
                        oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
                        oDiscrepancyBE.LoggedDateTime = DateTime.Now;
                        oDiscrepancyBE.LevelNumber = 1;
                        oDiscrepancyBE.ActionTakenBy = "GIN";
                        oDiscrepancyBE.GINActionCategory = "AR";  //Action Required
                        int? iResult1 = oDiscrepancyBAL.addWorkFlowHTMLsBAL(oDiscrepancyBE);
                    }
                }
                else
                {
                    /*Enter comments with date*/
                    InsertHTML();
                }
            }
        }

        //Redirecting to Previous Page
        RedirectToPage(GetQueryStringValue("PreviousPage"));
    }

    private int? PrintAndSaveReturnNote()
    {
        decimal TotalCost = 0;
        decimal NetTotalCost = 0;
        decimal FrieghtCharges = 0;
        string htmlBody = string.Empty;
        string DiscrepancyType = string.Empty;
        // vendor return address
        DiscrepancyBE oNewDiscrepancyBE = new DiscrepancyBE();
        DiscrepancyBAL oNewDiscrepancyBAL = new DiscrepancyBAL();
        oNewDiscrepancyBE.Action = "GetDiscrepancySiteVendorDetail";
        oNewDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID"));

        List<DiscrepancyBE> lstVendor = oNewDiscrepancyBAL.GetDiscrepancyLogSiteVendorDetailsBAL(oNewDiscrepancyBE);
        if (lstVendor.Count > 0)
            DiscrepancyType = lstVendor[0].DiscrepancyType;
        string VendorAddress = string.Empty;
        if (string.IsNullOrEmpty(lstVendor[0].Vendor.ReturnAddress))
        {
            VendorAddress = lstVendor[0].Vendor.address1 + "<br />" +
                lstVendor[0].Vendor.address2 + "<br />" +
                lstVendor[0].Vendor.city + "<br />" +
                lstVendor[0].Vendor.VMPPIN + " " + lstVendor[0].Vendor.VMPPOU;

        }
        else
        {
            VendorAddress = lstVendor[0].Vendor.ReturnAddress.Replace("\r\n", "<br />");
        }
        // site address
        string SiteAddress = lstVendor[0].Site.SiteAddressLine1 + "<br />" +
           lstVendor[0].Site.SiteAddressLine2 + "<br />" +
           lstVendor[0].Site.SiteAddressLine3 + "<br />" +
           lstVendor[0].Site.SiteAddressLine4 + "<br />" +
           lstVendor[0].Site.SiteAddressLine5 + "<br />" +
           lstVendor[0].Site.SiteAddressLine6 + "<br />" +
           lstVendor[0].Site.SitePincode;

        oNewDiscrepancyBE.Action = "GetDiscrepancyLogITemsForReturnNote";
        List<DiscrepancyBE> lstItems = oNewDiscrepancyBAL.GetDiscrepancyItemsForReturnNote(oNewDiscrepancyBE);
        string DiscrepancyItems = string.Empty;
        string InCorrectDiscrepancyItems = string.Empty;
        int count = lstItems.Count/2;
        int tempCount = 0;
        if (lstItems != null && lstItems.Count > 0)
        {
            foreach (DiscrepancyBE item in lstItems)
            {
                if (count == 0 || tempCount < count)
                {
                    if (string.IsNullOrEmpty(item.ODSKUCode) && !string.IsNullOrEmpty(item.ProductDescription))
                    {
                        DiscrepancyItems += "<tr><td>" + item.DeliveredQuantity
                               + "</td><td>" + item.UOM
                               + "</td><td>" + item.ODSKUCode
                               + "</td><td>" + item.DirectCode
                               + "</td><td>" + item.ProductDescription;                                           
                        if (lstItems[0].DiscrepancyTypeID == 12)
                        {
                            DiscrepancyItems += "</td><td>" + item.OriginalQuantity;
                            DiscrepancyItems += "</td><td>" + item.OutstandingQuantity;

                            DiscrepancyItems += "</td><td style='text-align:left;'>" + item.ItemVal
                                         + "</td><td style='text-align:right;'>";
                        }
                        else
                        {
                            DiscrepancyItems += "</td><td style='text-align:right;'>" + item.ItemVal
                                             + "</td><td style='text-align:right;'>";
                        }
                        if (item.DeliveredQuantity.HasValue && !string.IsNullOrEmpty(item.ItemVal))
                        {
                            TotalCost += Convert.ToDecimal(item.ItemVal) * item.DeliveredQuantity.Value;
                            DiscrepancyItems += Convert.ToDecimal(item.ItemVal) * item.DeliveredQuantity.Value;

                        }
                        else
                        {
                            DiscrepancyItems += string.Empty;
                        }
                        DiscrepancyItems += "</td></tr>";
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(item.ODSKUCode))
                        {
                            DiscrepancyItems += "<tr><td>" + item.DeliveredQuantity
                                + "</td><td>" + item.UOM
                                + "</td><td>" + item.ODSKUCode
                                + "</td><td>" + item.DirectCode
                                + "</td><td>" + item.ProductDescription;
                            if (lstItems[0].DiscrepancyTypeID == 12)
                            {
                                DiscrepancyItems += "</td><td>" + item.OriginalQuantity;
                                DiscrepancyItems += "</td><td>" + item.OutstandingQuantity;

                                DiscrepancyItems += "</td><td style='text-align:left;'>" + item.ItemVal
                            + "</td><td style='text-align:right;'>";
                            }
                            else
                            {
                                DiscrepancyItems += "</td><td style='text-align:right;'>" + item.ItemVal
                         + "</td><td style='text-align:right;'>";
                            }

                            if (item.DeliveredQuantity.HasValue && !string.IsNullOrEmpty(item.ItemVal))
                            {
                                TotalCost += Convert.ToDecimal(item.ItemVal) * item.DeliveredQuantity.Value;
                                DiscrepancyItems += Convert.ToDecimal(item.ItemVal) * item.DeliveredQuantity.Value;

                            }
                            else
                            {
                                DiscrepancyItems += string.Empty;
                            }
                            DiscrepancyItems += "</td></tr>";
                        }
                    }
                }
                else
                {
                    InCorrectDiscrepancyItems += "<tr><td>" + item.DeliveredQuantity
                    + "</td><td>" + item.UOM
                    + "</td><td>" + item.ODSKUCode
                    + "</td><td>" + item.DirectCode
                    + "</td><td>" + item.ProductDescription;
                    DiscrepancyItems += "</td></tr>";
                }
                tempCount++;
            }
        }
        NetTotalCost = TotalCost + FrieghtCharges;
        string VendorLanguage = lstVendor[0].Vendor.Language;
        //string templateFile = "~/EmailTemplates/ReturnNote/ReturnNote." + VendorLanguage + ".htm";
        string templateFile = string.Empty;
        if (lstItems[0].DiscrepancyTypeID == 12)
        {
            templateFile = "~/EmailTemplates/ReturnNote/ReturnNoteQuality.english.htm";
        }
        else
        {
            templateFile = "~/EmailTemplates/ReturnNote/ReturnNote.english.htm";
        }

        #region Setting reason as per the language ...
        switch (VendorLanguage)
        {
            case clsConstants.English:
                Page.UICulture = clsConstants.EnglishISO; // "en-US"; // English
                break;
            case clsConstants.French:
                Page.UICulture = clsConstants.FranceISO; // // France
                break;
            case clsConstants.German:
                Page.UICulture = clsConstants.GermanyISO; //"de"; // German
                break;
            case clsConstants.Dutch:
                Page.UICulture = clsConstants.NederlandISO; //"nl"; // Dutch
                break;
            case clsConstants.Spanish:
                Page.UICulture = clsConstants.SpainISO; //"es"; // Spanish
                break;
            case clsConstants.Italian:
                Page.UICulture = clsConstants.ItalyISO; //"it"; // Italy
                break;
            case clsConstants.Czech:
                Page.UICulture = clsConstants.CzechISO; //"cz"; // Czech
                break;
            default:
                Page.UICulture = clsConstants.EnglishISO; //"en-US"; // English
                break;
        }

        #endregion

        string contactPerson = string.Empty;
        sendCommunicationCommon osendCommunicationCommon = new sendCommunicationCommon();
        if (System.IO.File.Exists(System.Web.HttpContext.Current.Server.MapPath(templateFile.ToLower())))
        {
            using (StreamReader sReader = new StreamReader(System.Web.HttpContext.Current.Server.MapPath(templateFile.ToLower())))
            {
                htmlBody = sReader.ReadToEnd();
                if (DiscrepancyType != "Incorrect Product Code")
                    htmlBody = htmlBody.Replace("block","none");

                htmlBody = htmlBody.Replace("{VendorDiscrepancyReturnNumber}", WebCommon.getGlobalResourceValue("VendorDiscrepancyReturnNumber"));
                htmlBody = htmlBody.Replace("{VDRNoValue}", lstVendor[0].VDRNo);

                htmlBody = htmlBody.Replace("{logoInnerPath}", osendCommunicationCommon.getAbsolutePath());

                htmlBody = htmlBody.Replace("{From}", WebCommon.getGlobalResourceValue("From"));
                htmlBody = htmlBody.Replace("{SiteAddressValue}", SiteAddress);

                htmlBody = htmlBody.Replace("{VendorDiscrepancyReturnNote}", WebCommon.getGlobalResourceValue("VendorDiscrepancyReturnNote"));

                htmlBody = htmlBody.Replace("{VendorNumberSDR}", WebCommon.getGlobalResourceValue("VendorNumberSDR"));
                htmlBody = htmlBody.Replace("{VendorNumberValue}", lstVendor[0].Vendor.Vendor_No);

                htmlBody = htmlBody.Replace("{VendorName}", WebCommon.getGlobalResourceValue("VendorName"));
                htmlBody = htmlBody.Replace("{VendorNameValue}", string.IsNullOrEmpty(lstVendor[0].Vendor.VendorContactName) ? lstVendor[0].Vendor.VendorName : lstVendor[0].Vendor.VendorContactName);

                htmlBody = htmlBody.Replace("{Returnto}", WebCommon.getGlobalResourceValue("Returnto"));
                htmlBody = htmlBody.Replace("{VendorAddressValue}", VendorAddress);

                htmlBody = htmlBody.Replace("{ReturnNoteText1}", WebCommon.getGlobalResourceValue("ReturnNoteText1"));
                htmlBody = htmlBody.Replace("{ReturnNoteText2}", WebCommon.getGlobalResourceValue("ReturnNoteText2"));

                htmlBody = htmlBody.Replace("{AuthorisedBy}", WebCommon.getGlobalResourceValue("AuthorisedBy"));
                htmlBody = htmlBody.Replace("{VendorAuthorisationReference}", WebCommon.getGlobalResourceValue("VendorAuthorisationReference"));
                htmlBody = htmlBody.Replace("{ReasonforReturn}", WebCommon.getGlobalResourceValue("ReasonforReturn"));

                htmlBody = htmlBody.Replace("{VendorContactValue}", GetVendorContactName());
                htmlBody = htmlBody.Replace("{VendorAuthorisationReferenceValue}", txtCollectionAuthNum.Text.Trim());
                htmlBody = htmlBody.Replace("{VENDORDISREPACNYTYPEValue}", lstVendor[0].DiscrepancyType);

                htmlBody = htmlBody.Replace("{QTY}", WebCommon.getGlobalResourceValue("QTY"));
                htmlBody = htmlBody.Replace("{UOM}", WebCommon.getGlobalResourceValue("UOM"));
                htmlBody = htmlBody.Replace("{OURPRODUCTCODE}", WebCommon.getGlobalResourceValue("OURPRODUCTCODE"));
                htmlBody = htmlBody.Replace("{VENDORREFERENCE}", WebCommon.getGlobalResourceValue("VENDORREFERENCE"));
                htmlBody = htmlBody.Replace("{DESCRIPTION}", WebCommon.getGlobalResourceValue("DESCRIPTION"));
                htmlBody = htmlBody.Replace("{COST}", WebCommon.getGlobalResourceValue("COST"));
                htmlBody = htmlBody.Replace("{NET}", WebCommon.getGlobalResourceValue("NET"));

                // quatity return notes changes//
                if (lstItems[0].DiscrepancyTypeID == 12)
                {
                    htmlBody = htmlBody.Replace("{POQuantity}", WebCommon.getGlobalResourceValue("OriginalPOQuantity"));
                    htmlBody = htmlBody.Replace("{OUTSTANDING}", WebCommon.getGlobalResourceValue("OutstandingPOQuantity"));
                }

                htmlBody = htmlBody.Replace("{DiscrepancyItemsValue}", DiscrepancyItems);
                htmlBody = htmlBody.Replace("{DiscrepancyItemsValue1}", InCorrectDiscrepancyItems);
                htmlBody = htmlBody.Replace("{ReturnTotal}", WebCommon.getGlobalResourceValue("ReturnTotal"));
                htmlBody = htmlBody.Replace("{TotalCostValue}", TotalCost.ToString());

                htmlBody = htmlBody.Replace("{ReturnNoteIncorrectProductDec}", WebCommon.getGlobalResourceValue("ReturnNoteIncorrectProductDec"));

                htmlBody = htmlBody.Replace("{FreightCharge}", WebCommon.getGlobalResourceValue("FreightCharge"));
                htmlBody = htmlBody.Replace("{FreightChargeValue}", FrieghtCharges.ToString());

                htmlBody = htmlBody.Replace("{Total}", WebCommon.getGlobalResourceValue("Total"));
                htmlBody = htmlBody.Replace("{NetTotalCostValue}", NetTotalCost.ToString());

                htmlBody = htmlBody.Replace("{PreparedBy}", WebCommon.getGlobalResourceValue("PreparedBy"));
                htmlBody = htmlBody.Replace("{OFFICE_DEPOT_DC_CONTACTValue}", Session["UserName"].ToString());

                htmlBody = htmlBody.Replace("{DatePrepared}", WebCommon.getGlobalResourceValue("DatePrepared"));
                htmlBody = htmlBody.Replace("{DATEPREPAREDValue}", DateTime.Now.ToString("dd/MM/yyyy"));

                htmlBody = htmlBody.Replace("{NumberofPallets}", WebCommon.getGlobalResourceValue("NumberofPallets"));
                htmlBody = htmlBody.Replace("{NumberofPalletsValue}", txtNumPalletsExchange.Text);

                htmlBody = htmlBody.Replace("{NumberofCartons}", WebCommon.getGlobalResourceValue("NumberofCartons"));
                htmlBody = htmlBody.Replace("{NumberofCartonsValue}", txtNumCartonsExchange.Text);

                htmlBody = htmlBody.Replace("{CollectionReturnDate}", WebCommon.getGlobalResourceValue("CollectionReturnDate"));
                htmlBody = htmlBody.Replace("{ColletionDateValue}", txtCollectionDate.Text);

                htmlBody = htmlBody.Replace("{Signed}", WebCommon.getGlobalResourceValue("Signed"));

                htmlBody = htmlBody.Replace("{Carrier}", WebCommon.getGlobalResourceValue("Carrier"));
                if (!string.IsNullOrEmpty(ucTxtCarrierWhoCollectGoods.SelectedItem.Text) && !string.IsNullOrWhiteSpace(ucTxtCarrierWhoCollectGoods.SelectedItem.Text))
                    htmlBody = htmlBody.Replace("{CarrierValue}", ucTxtCarrierWhoCollectGoods.SelectedItem.Text);
                else
                    htmlBody = htmlBody.Replace("{CarrierValue}", "");

                htmlBody = htmlBody.Replace("{VEHICLEREG}", WebCommon.getGlobalResourceValue("VEHICLEREG"));
                htmlBody = htmlBody.Replace("{DRIVER}", WebCommon.getGlobalResourceValue("DRIVER"));
                htmlBody = htmlBody.Replace("{SIGN}", WebCommon.getGlobalResourceValue("SIGN"));
                htmlBody = htmlBody.Replace("{Print}", WebCommon.getGlobalResourceValue("Print"));


                // changes for alternate Return note 
                htmlBody = htmlBody.Replace("{PalletExchangeInfo}", WebCommon.getGlobalResourceValue("PalletExchangeInfo"));
                htmlBody = htmlBody.Replace("{OnewayPallets}", WebCommon.getGlobalResourceValue("OneWayPallets"));
                htmlBody = htmlBody.Replace("{EuroPallets}", WebCommon.getGlobalResourceValue("NumberEuroPallets"));
                htmlBody = htmlBody.Replace("{EuroPalletsChanged}", WebCommon.getGlobalResourceValue("EuroPalletsChanged"));
                // changes end

                htmlBody = htmlBody.Replace("{ImportantNotice}", WebCommon.getGlobalResourceValue("ImportantNotice"));
                htmlBody = htmlBody.Replace("{ReturnNoteText3}", WebCommon.getGlobalResourceValue("ReturnNoteText3"));
                htmlBody = htmlBody.Replace("{ReturnNoteText4}", WebCommon.getGlobalResourceValue("ReturnNoteText4"));
                
                contactPerson = GetAPContact(lstVendor[0].VendorID);
                htmlBody = htmlBody.Replace("{ACCOUNTS PAYABLE CONTACT}", string.IsNullOrEmpty(contactPerson) ? "<Not Available>" : contactPerson);
                if (lstVendor[0].Site.SiteMangerUserID != null)
                    contactPerson = GetDCSiteContact(lstVendor[0].Site.SiteMangerUserID);

                htmlBody = htmlBody.Replace("{OFFICE DEPOT DC SITE CONTACT}", string.IsNullOrEmpty(contactPerson) ? "<Not Available>" : contactPerson);

                htmlBody = htmlBody.Replace("{POHash}", WebCommon.getGlobalResourceValue("POHash"));
                htmlBody = htmlBody.Replace("{POValue}", lstItems[0].PurchaseOrderNumber);
            }
        }
        Page.UICulture = Convert.ToString(Session["CultureInfo"]);

        // Save into the database
        DiscrepancyReturnNoteBE oDiscrepancyReturnNoteBE = new DiscrepancyReturnNoteBE();
        oDiscrepancyReturnNoteBE.Action = "AddDiscrepancyReturnNote";
        oDiscrepancyReturnNoteBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID"));
        oDiscrepancyReturnNoteBE.LoggedDate = DateTime.Now;
        oDiscrepancyReturnNoteBE.ReturnNoteBody = htmlBody.ToString();

        int? retval = oNewDiscrepancyBAL.AddDiscrepancyReturnNote(oDiscrepancyReturnNoteBE);


        // Print Return note
        Session["LabelHTML"] = htmlBody.ToString();

        string a = "window.open('../../LogDiscrepancy/DIS_PrintLetter.aspx', '', 'left =' + screen.width + ',top=' + screen.height + ',width=0,height=0,toolbar=0,scrollbars=0,status=0')";
        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "pp", a, true);

        return retval;
    }

    #endregion

    #region Methods
    protected string CommentRequired = "-" + WebCommon.getGlobalResourceValue("CommentRequired");
    private string GetVendorContactName()
    {
        string DiscrepancyVendorContact = string.Empty;
        DiscrepancyBE oNewDiscrepancyBE = new DiscrepancyBE();
        DiscrepancyBAL oNewDiscrepancyBAL = new DiscrepancyBAL();
        oNewDiscrepancyBE.Action = "GetDiscrepancyVendorContact";
        oNewDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID"));
        oNewDiscrepancyBE = oNewDiscrepancyBAL.GetDiscrepancyVendorContact(oNewDiscrepancyBE);
        if (!string.IsNullOrEmpty(oNewDiscrepancyBE.Username))
            DiscrepancyVendorContact = oNewDiscrepancyBE.Username;
        return DiscrepancyVendorContact;
    }

    private string GetDCSiteContact(int? UserID)
    {
        string DCSiteContact = string.Empty;
        SCT_UserBE oSCT_UserBE = new SCT_UserBE();
        oSCT_UserBE.UserID = UserID.Value;
        oSCT_UserBE.Action = "ShowAll";
        SCT_UserBAL oSCT_UserBAL = new SCT_UserBAL();
        List<SCT_UserBE> lstUser = oSCT_UserBAL.GetAPContactBAL(oSCT_UserBE);
        if (lstUser != null && lstUser.Count > 0)
        {

            DCSiteContact = lstUser[0].FirstName + " " + lstUser[0].Lastname + " " + lstUser[0].PhoneNumber;
        }
        return DCSiteContact;
    }

    private string GetAPContact(int? VendorID)
    {
        string DCSiteContact = string.Empty;
        SCT_UserBE oSCT_UserBE = new SCT_UserBE();
        oSCT_UserBE.VendorID = VendorID.Value;
        oSCT_UserBE.Action = "GetAccountPayableDetail";
        SCT_UserBAL oSCT_UserBAL = new SCT_UserBAL();
        List<SCT_UserBE> lstUser = oSCT_UserBAL.GetAPContactBAL(oSCT_UserBE);
        if (lstUser != null && lstUser.Count > 0)
        {

            DCSiteContact = lstUser[0].FirstName + " " + lstUser[0].Lastname + " " + lstUser[0].PhoneNumber;

        }
        return DCSiteContact;
    }

    private void GetVendorCollectionData()
    {
        string sStageName = string.Empty;
        DISLog_WorkFlowBE oDISLog_WorkFlowBE = new DISLog_WorkFlowBE();
        DISLog_WorkFlowBAL oDISLog_WorkFlowBAL = new DISLog_WorkFlowBAL();
        if (GetQueryStringValue("disLogID") != null && GetQueryStringValue("disLogID").ToString() != "")
        {

            oDISLog_WorkFlowBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID"));
            oDISLog_WorkFlowBE.StageCompletedForWhom = "VEN001_StageCompleted";

            if (GetQueryStringValue("FromPage") != null && GetQueryStringValue("FromPage").ToString().Trim() == "WrongPackSize")
            {
                oDISLog_WorkFlowBE.StageCompletedForWhom = "VEN017_StageCompleted";
            }
            else if (GetQueryStringValue("FromPage") != null &&
                (GetQueryStringValue("FromPage").ToString().Trim() == "IncorrectProduct") || GetQueryStringValue("FromPage").ToString().Trim() == "QualityIssue" || GetQueryStringValue("FromPage").ToString().Trim() == "NoPaperwork" || GetQueryStringValue("FromPage").ToString().Trim() == "ItemNotOnPo")
            {
                oDISLog_WorkFlowBE.StageCompletedForWhom = "VEN011_StageCompleted";
            }
            else if (GetQueryStringValue("FromPage") != null && GetQueryStringValue("FromPage").ToString().Trim() == "GoodsReceivedDamaged")
            {
                //oDISLog_WorkFlowBE.StageCompletedForWhom = "VEN005_StageCompleted";
                oDISLog_WorkFlowBE.Action = "GetAll";
                oDISLog_WorkFlowBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
                DataTable dt = oDISLog_WorkFlowBAL.GetDataForSpecificActionBAL(oDISLog_WorkFlowBE);

                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        if (dr["VENUserControl"] != DBNull.Value && dr["VENActionRequired"] != DBNull.Value && Convert.ToBoolean(dr["VENActionRequired"]) == false)
                        {
                            if (Convert.ToString(dr["VENUserControl"]) == "VEN005")
                                sStageName = "VEN005_StageCompleted";
                            else if (Convert.ToString(dr["VENUserControl"]) == "VEN006")
                                sStageName = "VEN006_StageCompleted";
                            break;
                        }
                    }
                    oDISLog_WorkFlowBE.StageCompletedForWhom = sStageName;
                }
            }
            else if (GetQueryStringValue("FromPage") != null && GetQueryStringValue("FromPage").ToString().Trim() == "NoPO")
            {
                //Check ven011 instead of ven009.
                oDISLog_WorkFlowBE.StageCompletedForWhom = "VEN011_StageCompleted";
            }

            oDISLog_WorkFlowBE.Action = "GetVendorCollectionData";
            DataTable dtVendorCollData = oDISLog_WorkFlowBAL.GetVendorCollectionDataBAL(oDISLog_WorkFlowBE);
            if (dtVendorCollData != null && dtVendorCollData.Rows.Count > 0)
            {

                ucTxtCarrierWhoCollectGoods.SelectedIndex = dtVendorCollData.Rows[0]["CarrierWhoWillCollectGoods"] != DBNull.Value ?
                                                                ucTxtCarrierWhoCollectGoods.Items.IndexOf(ucTxtCarrierWhoCollectGoods.Items.FindByValue(dtVendorCollData.Rows[0]["CarrierWhoWillCollectGoods"].ToString()))
                                                                 : 0;
                ucTxtCarrierWhoCollectGoods.Enabled = false;
                if (GetQueryStringValue("FromPage") != null && GetQueryStringValue("FromPage").ToString().Trim() == "WrongPackSize")
                {
                    txtCollectionDate.Text = dtVendorCollData.Rows[0]["VEN017_DateGoodsAreToBeCollected"] != DBNull.Value ? Convert.ToDateTime(dtVendorCollData.Rows[0]["VEN017_DateGoodsAreToBeCollected"]).ToString("dd/MM/yyyy") : "";
                    txtCollectionAuthNum.Text = dtVendorCollData.Rows[0]["VEN017_CollectionAuthorisationNumber"] != DBNull.Value ? dtVendorCollData.Rows[0]["VEN017_CollectionAuthorisationNumber"].ToString() : "";
                }
                else if (GetQueryStringValue("FromPage") != null &&
                        (GetQueryStringValue("FromPage").ToString().Trim() == "IncorrectProduct"
                        || GetQueryStringValue("FromPage").ToString().Trim() == "NoPO"
                        || GetQueryStringValue("FromPage").ToString().Trim() == "QualityIssue"
                        || GetQueryStringValue("FromPage").ToString().Trim() == "NoPaperwork"
                        || GetQueryStringValue("FromPage").ToString().Trim() == "ItemNotOnPo")
                        )
                {
                    txtCollectionDate.Text = dtVendorCollData.Rows[0]["VEN011_DateGoodsAreToBeCollected"] != DBNull.Value ? Convert.ToDateTime(dtVendorCollData.Rows[0]["VEN011_DateGoodsAreToBeCollected"]).ToString("dd/MM/yyyy") : "";
                    txtCollectionAuthNum.Text = dtVendorCollData.Rows[0]["VEN011_CollectionAuthorisationNumber"] != DBNull.Value ? dtVendorCollData.Rows[0]["VEN011_CollectionAuthorisationNumber"].ToString() : "";
                }
                else if (GetQueryStringValue("FromPage") != null && GetQueryStringValue("FromPage").ToString().Trim() == "GoodsReceivedDamaged")
                {
                    if (sStageName == "VEN005_StageCompleted")
                    {
                        txtCollectionDate.Text = dtVendorCollData.Rows[0]["VEN005_DateGoodsAreToBeCollected"] != DBNull.Value ? Convert.ToDateTime(dtVendorCollData.Rows[0]["VEN005_DateGoodsAreToBeCollected"]).ToString("dd/MM/yyyy") : "";
                        txtCollectionAuthNum.Text = dtVendorCollData.Rows[0]["VEN005_CollectionAuthorisationNumber"] != DBNull.Value ? dtVendorCollData.Rows[0]["VEN005_CollectionAuthorisationNumber"].ToString() : "";
                    }
                    else if (sStageName == "VEN006_StageCompleted")
                    {
                        txtCollectionDate.Text = dtVendorCollData.Rows[0]["VEN006_DateGoodsAreToBeCollected"] != DBNull.Value ? Convert.ToDateTime(dtVendorCollData.Rows[0]["VEN006_DateGoodsAreToBeCollected"]).ToString("dd/MM/yyyy") : "";
                        txtCollectionAuthNum.Text = dtVendorCollData.Rows[0]["VEN006_CollectionAuthorisationNumber"] != DBNull.Value ? dtVendorCollData.Rows[0]["VEN006_CollectionAuthorisationNumber"].ToString() : "";
                    }
                }
                else if (GetQueryStringValue("FromPage") != null && GetQueryStringValue("FromPage").ToString().Trim() == "overs")
                {
                    txtCollectionDate.Text = dtVendorCollData.Rows[0]["VEN001_DateGoodsAreToBeCollected"] != DBNull.Value ? Convert.ToDateTime(dtVendorCollData.Rows[0]["VEN001_DateGoodsAreToBeCollected"]).ToString("dd/MM/yyyy") : "";
                    txtCollectionAuthNum.Text = dtVendorCollData.Rows[0]["VEN001_CollectionAuthorisationNumber"] != DBNull.Value ? dtVendorCollData.Rows[0]["VEN001_CollectionAuthorisationNumber"].ToString() : "";
                }
                else
                {
                    txtCollectionDate.Text = dtVendorCollData.Rows[0]["GIN002_ArrangedCollectionDate"] != DBNull.Value ? Convert.ToDateTime(dtVendorCollData.Rows[0]["GIN002_ArrangedCollectionDate"]).ToString("dd/MM/yyyy") : "";
                    txtCollectionAuthNum.Text = dtVendorCollData.Rows[0]["GIN002_CollectionAuthorisationNumber"] != DBNull.Value ? dtVendorCollData.Rows[0]["GIN002_CollectionAuthorisationNumber"].ToString() : "";
                }
            }
        }
    }

    private void BindCarrier()
    {
        MASCNT_CarrierBE oMASCNT_CarrierBE = new MASCNT_CarrierBE();
        APPCNT_CarrierBAL oMASCNT_CarrierBAL = new APPCNT_CarrierBAL();

        oMASCNT_CarrierBE.Action = "ShowAllByVDRNumber";
        oMASCNT_CarrierBE.Discrepancy = new BusinessEntities.ModuleBE.Discrepancy.LogDiscrepancy.DiscrepancyBE();
        oMASCNT_CarrierBE.Discrepancy.VDRNo = GetQueryStringValue("VDRNo").ToString();

        List<MASCNT_CarrierBE> lstCarrier = oMASCNT_CarrierBAL.GetCarrierDetailsBAL(oMASCNT_CarrierBE);

        if (lstCarrier.Count > 0)
        {
            FillControls.FillDropDown(ref ucTxtCarrierWhoCollectGoods, lstCarrier, "CarrierName", "CarrierID", "--Select--");
        }

    }

    private void InsertHTML()
    {
        DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
        DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();

        WorkflowHTML oWorkflowHTML = new WorkflowHTML();
        oDiscrepancyBE.GINHTML = oWorkflowHTML.function3("White", DateTime.Now, txtGIN002Comment.Text.Trim(), "");
        oDiscrepancyBE.Action = "InsertHTML";
        oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
        oDiscrepancyBE.LoggedDateTime = DateTime.Now;
        oDiscrepancyBE.LevelNumber = 1;

        int? iResult = oDiscrepancyBAL.addWorkFlowHTMLsBAL(oDiscrepancyBE);
    }

    #endregion

    protected void btnBack_Click(object sender, EventArgs e)
    {
        //Redirecting to Previous Page
        RedirectToPage(GetQueryStringValue("PreviousPage"));
    }
}
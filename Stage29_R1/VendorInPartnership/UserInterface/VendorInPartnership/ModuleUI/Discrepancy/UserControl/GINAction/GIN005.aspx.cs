﻿using System;
using System.Data;

using BusinessEntities.ModuleBE.Discrepancy.LogDiscrepancy;
using BusinessLogicLayer.ModuleBAL.Discrepancy.LogDiscrepancy;

public partial class GINAction_GIN005 : CommonPage
{
    #region Global variables

    string NAXPO = string.Empty;

    #endregion

    #region Events

    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);
        GetAdvisedPurchaseOrder();
        if (NAXPO != string.Empty && NAXPO != "")
        {
            lblGIN005Desc.Text = "Please confirm receipt of the delivered item against Purchase order " + NAXPO;
        }
       
    }

    private void GetAdvisedPurchaseOrder()
    {
        DISLog_WorkFlowBE oDISLog_WorkFlowBE = new DISLog_WorkFlowBE();
        DISLog_WorkFlowBAL oDISLog_WorkFlowBAL = new DISLog_WorkFlowBAL();
        oDISLog_WorkFlowBE.Action = "GetDataForSpecificAction";
        oDISLog_WorkFlowBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
        if (GetQueryStringValue("disLogID") != null && GetQueryStringValue("disLogID").ToString() != "")
        {
            oDISLog_WorkFlowBE.StageCompletedForWhom = "INV001_StageCompleted";//checking VEN012 instead of VEN010 in NoPO and VEN008 in Nopaperwork
        }

        DataTable dtVEN = oDISLog_WorkFlowBAL.GetDataForSpecificActionBAL(oDISLog_WorkFlowBE);
        if (dtVEN != null && dtVEN.Rows.Count > 0)
        {
             NAXPO = dtVEN.Rows[0]["INV001_NAX_PO"].ToString();
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            chkGIN005StageCompleted.Checked = true;
            
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        btnSave.Enabled = false;

        //**********************************************************
        string Location = hdfLocation.Value;
        DiscrepancyBE oDiscrepancyBE1 = new DiscrepancyBE();
        DiscrepancyBAL oDiscrepancyBAL1 = new DiscrepancyBAL();
        oDiscrepancyBE1.Action = "UpdateLocation";
        oDiscrepancyBE1.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
        oDiscrepancyBE1.Location = Location;
        oDiscrepancyBE1.UserID = Convert.ToInt32(Session["UserID"]);
        string sResult1 = oDiscrepancyBAL1.updateDiscrepancyLocationBAL(oDiscrepancyBE1);
        //**********************************************************
        DISLog_WorkFlowBE oDISLog_WorkFlowBE = new DISLog_WorkFlowBE();
        DISLog_WorkFlowBAL oDISLog_WorkFlowBAL = new DISLog_WorkFlowBAL();


        oDISLog_WorkFlowBE.Action = "InsertUpdateWorkflowActions";
        oDISLog_WorkFlowBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
        oDISLog_WorkFlowBE.StageCompleted = chkGIN005StageCompleted.Checked ? true : false;

        oDISLog_WorkFlowBE.StageCompletedForWhom = "GIN005_StageCompleted";
        oDISLog_WorkFlowBE.Comment = txtComments.Text;

        //update the work flow action table each time for the comments and other fields( if stage is completed)
        oDISLog_WorkFlowBE.DiscrepancyWorkFlowID = Convert.ToInt32(GetQueryStringValue("DisWFlowID").ToString());

        //insert or update the record according to the stage completed status
        int? iResult = oDISLog_WorkFlowBAL.addWorkFlowActionsBAL(oDISLog_WorkFlowBE);

        //now insert the work flow HTML according to the action taken only if the stage is completed
        DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
        DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();
        WorkflowHTML oWorkflowHTML = new WorkflowHTML();


        if (GetQueryStringValue("FromPage") != null && GetQueryStringValue("FromPage").ToString().Trim() == "NoPO"
            || GetQueryStringValue("FromPage") != null && GetQueryStringValue("FromPage").ToString().Trim() == "NoPaperwork")
        {
            oDISLog_WorkFlowBE.Action = "GetDataForSpecificAction";
            oDISLog_WorkFlowBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
            if (GetQueryStringValue("disLogID") != null && GetQueryStringValue("disLogID").ToString() != "")
            {
                oDISLog_WorkFlowBE.StageCompletedForWhom = "VEN012_StageCompleted";//checking VEN012 instead of VEN010 in NoPO and VEN008 in Nopaperwork
            }
            
            DataTable dtVEN = oDISLog_WorkFlowBAL.GetDataForSpecificActionBAL(oDISLog_WorkFlowBE);

            if (chkGIN005StageCompleted.Checked)
            {
                oDiscrepancyBE.GINHTML = oWorkflowHTML.function1(DateTime.Now, null, null, "Yellow", "", "", 
                    //Session["UserName"].ToString(), 
                    UIUtility.GetODVendorUserName(Convert.ToString(Session["Role"]), Convert.ToString(Session["UserName"]), Convert.ToString(Session["LoginID"])),
                    txtComments.Text);
                oDiscrepancyBE.GINActionRequired = false;

                oDiscrepancyBE.INVHTML = oWorkflowHTML.function3("white", null);
                oDiscrepancyBE.INVActionRequired = false;

                oDiscrepancyBE.APHTML = oWorkflowHTML.function3("white", null);
                oDiscrepancyBE.APActionRequired = false;

                oDiscrepancyBE.VENHTML = oWorkflowHTML.function3("white", null);
                oDiscrepancyBE.VenActionRequired = false;

                oDiscrepancyBE.Action = "InsertHTML";

                if (dtVEN != null && dtVEN.Rows.Count > 0)
                {
                    oDiscrepancyBE.CloseDiscrepancy = true;
                }
                oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
                oDiscrepancyBE.LoggedDateTime = DateTime.Now;
                oDiscrepancyBE.LevelNumber = 1;
                int? iResult1 = oDiscrepancyBAL.addWorkFlowHTMLsBAL(oDiscrepancyBE);
            }
            else
            {
                InsertHTML();
            }
        }


        #region ItemNotOnPO

        if (GetQueryStringValue("FromPage") != null && GetQueryStringValue("FromPage").ToString().Trim() == "ItemNotOnPo")
        {
            string GAction = "Goods Receipted";
            oDISLog_WorkFlowBE.Action = "GetDataForSpecificAction";
            oDISLog_WorkFlowBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
            if (GetQueryStringValue("disLogID") != null && GetQueryStringValue("disLogID").ToString() != "")
            {
                oDISLog_WorkFlowBE.StageCompletedForWhom = "INV001_StageCompleted";//checking INV001 stage is completed or not.
            }

            DataTable dtVEN = oDISLog_WorkFlowBAL.GetDataForSpecificActionBAL(oDISLog_WorkFlowBE);

            if (chkGIN005StageCompleted.Checked)
            {
                oDiscrepancyBE.GINHTML = oWorkflowHTML.function1(DateTime.Now, null, null, "Yellow", "", GAction,
                    //Session["UserName"].ToString(), 
                    UIUtility.GetODVendorUserName(Convert.ToString(Session["Role"]), Convert.ToString(Session["UserName"]), Convert.ToString(Session["LoginID"])),
                    txtComments.Text);
                oDiscrepancyBE.GINActionRequired = false;

                oDiscrepancyBE.INVHTML = oWorkflowHTML.function3("white", null);
                oDiscrepancyBE.INVActionRequired = false;

                oDiscrepancyBE.APHTML = oWorkflowHTML.function3("white", null);
                oDiscrepancyBE.APActionRequired = false;

                oDiscrepancyBE.VENHTML = oWorkflowHTML.function3("white", null);
                oDiscrepancyBE.VenActionRequired = false;

                oDiscrepancyBE.Action = "InsertHTML";

                if (dtVEN != null && dtVEN.Rows.Count > 0)
                {
                    oDiscrepancyBE.CloseDiscrepancy = true;
                }
                oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
                oDiscrepancyBE.LoggedDateTime = DateTime.Now;
                oDiscrepancyBE.LevelNumber = 1;
                int? iResult1 = oDiscrepancyBAL.addWorkFlowHTMLsBAL(oDiscrepancyBE);
            }
            else
            {
                InsertHTML();
            }
        }

        #endregion

        //Redirecting to Previous Page
        RedirectToPage(GetQueryStringValue("PreviousPage"));
    }
    #endregion

    #region Methods
    private void InsertHTML()
    {
        DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
        DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();

        WorkflowHTML oWorkflowHTML = new WorkflowHTML();
        oDiscrepancyBE.GINHTML = oWorkflowHTML.function3("White", DateTime.Now, txtComments.Text.Trim());
        oDiscrepancyBE.GINActionRequired = false;

        oDiscrepancyBE.Action = "InsertHTML";
        oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
        oDiscrepancyBE.LoggedDateTime = DateTime.Now;
        oDiscrepancyBE.LevelNumber = 1;

        int? iResult = oDiscrepancyBAL.addWorkFlowHTMLsBAL(oDiscrepancyBE);
    }
    #endregion

    protected void btnBack_Click(object sender, EventArgs e)
    {
        //Redirecting to Previous Page
        RedirectToPage(GetQueryStringValue("PreviousPage"));
    }
}
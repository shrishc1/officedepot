﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using BusinessEntities.ModuleBE.Discrepancy.CountrySetting;
using BusinessEntities.ModuleBE.Security;
using BusinessLogicLayer.ModuleBAL.Appointment.SiteSettings;
using BusinessLogicLayer.ModuleBAL.Discrepancy.CountrySetting;
using BusinessEntities.ModuleBE.AdminFunctions;
using BusinessLogicLayer.ModuleBAL.AdminFunctions;
using System.Configuration;
using System.Data;
using System.Web.UI.WebControls;
using Utilities;
using WebUtilities;
using System.Linq;


public partial class ucSDRCommunicationAPListing : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e) { }

    public void FillAPContacts(int vendorID)
    {
        var vendorPointsContactBAL = new MAS_MaintainVendorPointsContactBAL();
        var VendorPointsContact = new MAS_MaintainVendorPointsContactBE();

        VendorPointsContact.Action = "GetMaintainVendorPointsContact";
        VendorPointsContact.Vendor = new MAS_VendorBE();
        VendorPointsContact.Vendor.VendorID = vendorID;
        
        /*Here filtering to get only Account payable contacts for the vendor. */ 
        var lstAPContacts = vendorPointsContactBAL.GetVendorPointsContactsBAL(VendorPointsContact).FindAll(x => x.MerchandisingPOC.Equals("Y"));
        if (lstAPContacts != null && lstAPContacts.Count > 0)
        {
            txtVendorEmailList.Text = string.Empty;
            foreach (var contact in lstAPContacts.Select(x=>x.EmailAddress.ToLower()).Distinct().ToList())
            {
                if (string.IsNullOrEmpty(txtVendorEmailList.Text) && string.IsNullOrWhiteSpace(txtVendorEmailList.Text))
                    txtVendorEmailList.Text = contact;
                else
                    txtVendorEmailList.Text = string.Format("{0},{1}", txtVendorEmailList.Text, contact);
            }
            txtVendorEmailList.Text = txtVendorEmailList.Text.TrimEnd(',');
        }
        else
        {
            txtVendorEmailList.Text = Convert.ToString(ConfigurationManager.AppSettings["DefaultEmailAddress"]);
        }
        vendorPointsContactBAL = null;
    }
}
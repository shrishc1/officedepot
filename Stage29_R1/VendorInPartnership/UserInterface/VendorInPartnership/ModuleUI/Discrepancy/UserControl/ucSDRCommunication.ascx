﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucSDRCommunication.ascx.cs"
    Inherits="ucSDRCommunication" %>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>

<table width="100%" cellspacing="0" cellpadding="0" class="form-table">
    <tr id="trSOV1" runat="server" visible="false">
        <td style="font-weight: bold;" colspan="3">
            <cc1:ucLabel ID="lblSendOutputVia" runat="server" Text="Send Output Via:"></cc1:ucLabel>
            <asp:HiddenField ID="hdnEmail" runat="server" Value="" />
            <asp:HiddenField ID="hdnFax" runat="server" Value="" />
        </td>
    </tr>
    <tr id="trSOV2" runat="server" visible="false">
        <td style="width: 33%">
            <cc1:ucRadioButton ID="rdoLetter" runat="server" GroupName="Communication" OnCheckedChanged="rdoLetter_CheckedChanged"
                AutoPostBack="true" />
        </td>     
        <td style="width: 67%">
            <cc1:ucRadioButton ID="rdoEmailComm" runat="server" GroupName="Communication" OnCheckedChanged="rdoEmailComm_CheckedChanged"
                AutoPostBack="true" />
        </td>
    </tr>
    <tr>
        <td style="font-weight: bold;" colspan="2">
            <cc1:ucTextbox ID="ucVendorEmailList" runat="server" ReadOnly="True" onkeyup="checkTextLengthOnKeyUp(this,1000);"
                TextMode="MultiLine" Width="95%" Height="50px"></cc1:ucTextbox>
        </td>
    </tr>
    <tr>
        <td style="font-weight: bold;" colspan="2">
            <cc1:ucLabel ID="lblAdditionalEmailAddressTo" runat="server"></cc1:ucLabel>
        </td>
    </tr>
    <tr>
        <td style="font-weight: bold;" colspan="2">
            <cc1:ucTextbox ID="txtAltEmail" runat="server" Width="95%" />
        </td>
    </tr>
</table>

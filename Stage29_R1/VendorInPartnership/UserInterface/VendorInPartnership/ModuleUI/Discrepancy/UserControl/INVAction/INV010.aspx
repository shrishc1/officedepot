﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="INV010.aspx.cs" Inherits="INVAction_INV010" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Office Depot</title>
    <link rel="stylesheet" type="text/css" href="../../../../Css/style.css" />
    <link type="text/css" href="../../../../Css/ui-lightness/jquery-ui-1.8.5.custom.css"
        rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="../../../../Css/superfish.css" media="screen" />
    <script type="text/javascript" src="<%= ResolveClientUrl("../../../../Scripts/jquery-1.2.6.min.js")%>"></script>
    <script type="text/javascript" src="<%= ResolveClientUrl("../../../../Scripts/jquery-1.4.2.min.js")%>"></script>
    <script type="text/javascript" src="<%= ResolveClientUrl("../../../../Scripts/jquery-ui-1.8.5.custom.min.js")%>"></script>
    <script type="text/javascript" src="<%= ResolveClientUrl("../../../../Scripts/hoverIntent.js")%>"></script>
    <script type="text/javascript" src="<%= ResolveClientUrl("../../../../Scripts/superfish.js")%>"></script>
    <script type="text/javascript" src="<%= ResolveClientUrl("../../../../Scripts/ValidateInputs.js")%>"></script>
    <script language="javascript" type="text/javascript">
        function LabourCostRequired(sender, args) {
            var objrdo1 = document.getElementById('<%=rdoINV010Charge.ClientID %>');
            var objrdo2 = document.getElementById('<%=rdoINV010NoCharge.ClientID %>');
            if (objrdo1.checked == false && objrdo2.checked == false) {
                args.IsValid = false;
            }
        }
        function HideButton() {
            document.getElementById('btnSave').style.visibility = 'hidden';
        }
        function GetLocation() {
            if (checkValidationGroup("a")) {
                document.getElementById('<%=btnSave.ClientID%>').style.visibility = "hidden";
            }
            else {
                document.getElementById('<%=btnSave.ClientID%>').style.visibility = "visible";
                return false;
            }
            var Location = parent.document.getElementById('ctl00_ContentPlaceHolder1_txtLocation').value;
            document.getElementById('<%=hdfLocation.ClientID%>').value = Location;


        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <cc1:ucPanel ID="pnlActionRequired" runat="server" Width="100%" CssClass="fieldset-form"
            GroupingText="Action Required" BorderColor="gray" BorderStyle="Solid" BorderWidth="0">
            <table class="form-table" cellpadding="0" cellspacing="0" width="98%" style="margin: 2px;">
                <tr>
                    <td style="font-weight: bold;"><asp:HiddenField ID="hdfLocation" runat="server" />
                        <cc1:ucLabel ID="lblINV010Desc" runat="server" CssClass="action-required-heading"
                            Text="There has been a premature invoice before goods have been delivered. There has been an administration cost for this. Please confirm if the vendor should be charged for this"></cc1:ucLabel>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table class="form-table" cellpadding="0" cellspacing="0" width="98%" style="margin: 2px;">
                            <tr>
                                <td width="19%">
                                    <cc1:ucRadioButton ID="rdoINV010Charge" runat="server" GroupName="GoodsOption" Text="Charge"/>
                                </td>
                                <td width="1%">
                                </td>
                                <td width="80%">
                                    <cc1:ucRadioButton ID="rdoINV010NoCharge" runat="server" GroupName="GoodsOption"
                                        Text="No Charge" />
                                     <asp:CustomValidator ID="cusvLabourCostRequired" runat="server" Text="Please select the Labour Cost"
                                        ClientValidationFunction="LabourCostRequired" Display="None" ValidationGroup="a">
                                    </asp:CustomValidator>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 19%; font-weight: bold;">
                                    <cc1:ucLabel ID="lblLabourCost" runat="server" Text="Labour Cost"></cc1:ucLabel>
                                </td>
                                <td width="1%">
                                    :
                                </td>
                                <td width="80%">
                                    <cc1:ucTextbox ID="txtINV010LabourCost" runat="server" ReadOnly="true"></cc1:ucTextbox>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table class="form-table" cellpadding="0" cellspacing="0" width="98%" style="margin: 2px;">
                            <tr>
                                <td width="19%">
                                    <cc1:ucLabel ID="lblCommentLabel" runat="server" Text="Comment"></cc1:ucLabel>
                                </td>
                                <td width="81%">
                                    :
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <cc1:ucTextbox ID="txtComments" runat="server" onkeyup="checkTextLengthOnKeyUp(this,1000);"
                                        TextMode="MultiLine" CssClass="textarea" Width="98%"></cc1:ucTextbox>
                                    <asp:RequiredFieldValidator ID="rfvCommentRequired" runat="server" Text="Please enter comments."
                                        ControlToValidate="txtComments" Display="None" ValidationGroup="a">
                                    </asp:RequiredFieldValidator>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table class="form-table" cellpadding="0" cellspacing="0" width="98%" style="margin: 2px;">
                            <tr>
                                <td width="19%" style="display:none;">
                                    <cc1:ucLabel ID="lblStageCompleted" runat="server" Text="Stage Completed"></cc1:ucLabel>
                                    <asp:ValidationSummary ID="vs1" runat="server" ValidationGroup="a" ShowSummary="false"
                                        ShowMessageBox="true" />
                                </td>
                                <td width="1%" style="display:none;">
                                    :
                                </td>
                                <td width="70%" align="left" style="display:none;">
                                    <cc1:ucCheckbox ID="chkINV010StageCompleted" runat="server" Checked="true" />
                                </td>
                                <td width="10%" style="display:none;">
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3" style="display:none;">
                                    <cc1:ucLabel ID="lblDiscrepancyNote" runat="server" Text="If  you required to tag a comment without closing the discrepancy<br/>1 -    Deselect Stage Complete<br/>2 -   Enter  the comment<br/>3 -   Save"></cc1:ucLabel>
                                </td>
                                <td align="right" valign="bottom">
                                    <cc1:ucButton ID="btnSave" runat="server" Text="Save" class="button" ValidationGroup="a"
                                        OnClick="btnSave_Click" OnClientClick="javascript:GetLocation()" />
                                    <cc1:ucButton ID="btnBack" runat="server" Text="Back" class="button" OnClick="btnBack_Click" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </cc1:ucPanel>
    </div>
    </form>
</body>
</html>

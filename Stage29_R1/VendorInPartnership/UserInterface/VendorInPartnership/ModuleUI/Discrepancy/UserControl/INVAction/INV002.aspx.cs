﻿using System;
using System.Web.UI;
using BusinessEntities.ModuleBE.Discrepancy.LogDiscrepancy;
using BusinessLogicLayer.ModuleBAL.Discrepancy.LogDiscrepancy;
using WebUtilities;


public partial class INVAction_INV002 : CommonPage {
    #region Events

    protected void Page_Load(object sender, EventArgs e)
    {
        if (GetQueryStringValue("FromPage") != null && GetQueryStringValue("FromPage").ToString().Trim() == "shortage")
        {
            trInv003.Style.Add("Display", "none");
            trInv002.Style.Add("Display", "block");
        }
        else if (GetQueryStringValue("FromPage") != null && GetQueryStringValue("FromPage").ToString().Trim() == "GoodsReceivedDamaged")
        {
            trInv003.Style.Add("Display", "block");
            trInv002.Style.Add("Display", "none");
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {

        bool IsINV002AppOptionChecked = clsDiscrepancy.ISRadioButtonChecked(rdoINV002ReduceOrder, rdoINV002Resentbyvendor);
        if (IsINV002AppOptionChecked == false)
        {
            string INV002AppOptionMessage = WebCommon.getGlobalResourceValue("INV002AppOption");
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('" + INV002AppOptionMessage + "')", true);
            return;
        }


        ScriptManager.RegisterStartupScript(this, this.GetType(), "HideButton", "HideButton();", true);
        btnSave.Enabled = false;
        string sentToLinks = string.Empty;

        DISLog_WorkFlowBE oDISLog_WorkFlowBE = new DISLog_WorkFlowBE();
        DISLog_WorkFlowBAL oDISLog_WorkFlowBAL = new DISLog_WorkFlowBAL();
        if (GetQueryStringValue("disLogID") != null && GetQueryStringValue("disLogID").ToString() != "")
        {
            //**********************************************************
            string Location = hdfLocation.Value;
            DiscrepancyBE oDiscrepancyBE1 = new DiscrepancyBE();
            DiscrepancyBAL oDiscrepancyBAL1 = new DiscrepancyBAL();
            oDiscrepancyBE1.Action = "UpdateLocation";
            oDiscrepancyBE1.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
            oDiscrepancyBE1.Location = Location;
            oDiscrepancyBE1.UserID = Convert.ToInt32(Session["UserID"]);
            string sResult1 = oDiscrepancyBAL1.updateDiscrepancyLocationBAL(oDiscrepancyBE1);
            //**********************************************************

            oDISLog_WorkFlowBE.Action = "InsertUpdateWorkflowActions";
            oDISLog_WorkFlowBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
            oDISLog_WorkFlowBE.INV002_GoodsDecision = rdoINV002ReduceOrder.Checked ? 'P' : 'R';

            oDISLog_WorkFlowBE.Decision = rdoINV002ReduceOrder.Checked ? "Reduce Purchase Order" : "Goods to be Resent by the Vendor";

            oDISLog_WorkFlowBE.StageCompleted = chkINV002StageCompleted.Checked ? true : false;
            oDISLog_WorkFlowBE.Comment = txtINV002Comment.Text.Trim();
            oDISLog_WorkFlowBE.StageCompletedForWhom = "INV002_StageCompleted";
            if (GetQueryStringValue("FromPage") != null && GetQueryStringValue("FromPage").ToString().Trim() == "shortage") {
                oDISLog_WorkFlowBE.VenCommunicationType = "Kept";
                oDISLog_WorkFlowBE.Decision = "Goods to be kept";
            }
            if (GetQueryStringValue("FromPage") != null && GetQueryStringValue("FromPage").ToString().Trim() == "GoodsReceivedDamaged") {
                oDISLog_WorkFlowBE.VenCommunicationType = "return";
                oDISLog_WorkFlowBE.Decision = "Goods to be returned";
            }
            //else {
                // Does not Exist                
            //}

            //update the work flow action table each time for the comments and other fields( if stage is completed)
            oDISLog_WorkFlowBE.DiscrepancyWorkFlowID = Convert.ToInt32(GetQueryStringValue("DisWFlowID").ToString());
            //insert or update the record according to the stage completed status
            int? iResult = oDISLog_WorkFlowBAL.addWorkFlowActionsBAL(oDISLog_WorkFlowBE);

            //now insert the work flow HTML according to the action taken only if the stage is completed
            DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
            DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();
            WorkflowHTML oWorkflowHTML = new WorkflowHTML();

            if (GetQueryStringValue("FromPage") != null && GetQueryStringValue("FromPage").ToString().Trim() == "shortage")
            {
                if (chkINV002StageCompleted.Checked)
                {

                    string sAction = rdoINV002ReduceOrder.Checked ? "Reduce Purchase Order" : "Goods to be re-sent";

                    sentToLinks = SendMailAndGetMailLinks(Convert.ToInt32(GetQueryStringValue("disLogID")), "qtydifferenceshortage", "Communication1", "Communication2",sAction);
                   
                   

                    oDiscrepancyBE.GINHTML = oWorkflowHTML.function3("White", null, "", "");
                    oDiscrepancyBE.GINActionRequired = false;

                    oDiscrepancyBE.INVHTML = oWorkflowHTML.function1(DateTime.Now, null, null, "Yellow",
                                                                          "",
                                                                          sAction,
                                                                          //Session["UserName"].ToString(), 
                                                                          UIUtility.GetODVendorUserName(Convert.ToString(Session["Role"]), Convert.ToString(Session["UserName"]), Convert.ToString(Session["LoginID"])),
                                                                          txtINV002Comment.Text, "", "", "", "", "", "");
                    oDiscrepancyBE.INVActionRequired = false;

                    oDISLog_WorkFlowBE.Action = "GetAccountPayable";
                    string VendorAccountPayablesEmail = oDISLog_WorkFlowBAL.GetVendorAccountPayablesEmail(oDISLog_WorkFlowBE);
//                    oDiscrepancyBE.APHTML = oWorkflowHTML.function2(DateTime.Now, null, "Blue", VendorAccountPayablesEmail,
//                                                                            @"Build on to Accounts Payable Work list with following request",
//                                                                            @"A Discrepancy has been raised which requires
//                                                                        for debit to be raised or credit to be issued.
//                                                                         Please action accordingly upon receipt of invoice.",
//                                                                            "", "", "", "", "", "");
                    oDiscrepancyBE.APHTML = oWorkflowHTML.function8("green",
                                                                       @"A Discrepancy has been raised which may require a debit to be raised or credit to be issued. Please actions accordingly upon receipt of invoice.");
                    oDiscrepancyBE.APActionRequired = false; //true;
                    //oDiscrepancyBE.APUserControl = "ACP001";

                    if (sAction == "Reduce Purchase Order")
                    {

                        oDiscrepancyBE.VENHTML = oWorkflowHTML.function4("Blue", "", null, sentToLinks, "Admin@OfficeDepot.com",
                                                                         "", @"Short delivered, do not resend goods, credit/debit 
                                                                            to be issued.Please advise of remedial actions.", "", "", DateTime.Now);
                        oDiscrepancyBE.VENUserControl = "VEN003";
                    }
                    else
                    {
                        oDiscrepancyBE.VENHTML = oWorkflowHTML.function4("Blue", "", null, sentToLinks, "Admin@OfficeDepot.com",
                                                                          "", @"Short delivered, Credit/Debit will be issued.
                                                                            Please resend items short delivered and advise of remedial actions.", "", "", DateTime.Now);
                        oDiscrepancyBE.VENUserControl = "VEN004";
                    }

                    oDiscrepancyBE.VenActionRequired = true;

                    oDiscrepancyBE.Action = "InsertHTML";
                    oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
                    oDiscrepancyBE.LoggedDateTime = DateTime.Now;
                    oDiscrepancyBE.LevelNumber = 1;
                    oDiscrepancyBE.ACP001Control = "ACP001";
                    oDiscrepancyBE.IsACP001Done = false;
                    oDiscrepancyBE.ActionTakenBy = "INV";
                    oDiscrepancyBE.VendorActionCategory = "RA";  //Remedial Action
                    int? iResult1 = oDiscrepancyBAL.addWorkFlowHTMLsBAL(oDiscrepancyBE);
                }
                else
                {
                    /*Enter comments with date*/
                    InsertHTML();
                }
            }

            if (GetQueryStringValue("FromPage") != null && GetQueryStringValue("FromPage").ToString().Trim() == "GoodsReceivedDamaged")
            {
                if (chkINV002StageCompleted.Checked)
                {
                    string sAction = rdoINV002ReduceOrder.Checked ? "Reduce Purchase Order" : "Goods to be re-sent";

                    sentToLinks = SendMailAndGetMailLinks(Convert.ToInt32(GetQueryStringValue("disLogID")), "goodsreceiveddamaged", "Communication1", "Communication2",sAction);

                   

                    oDiscrepancyBE.GINHTML = oWorkflowHTML.function3("White", null, "", "");
                    oDiscrepancyBE.GINActionRequired = false;

                    oDiscrepancyBE.INVHTML = oWorkflowHTML.function1(DateTime.Now, null, null, "Yellow",
                                                                          "",
                                                                          sAction,
                                                                          //Session["UserName"].ToString(), 
                                                                          UIUtility.GetODVendorUserName(Convert.ToString(Session["Role"]), Convert.ToString(Session["UserName"]), Convert.ToString(Session["LoginID"])),
                                                                          txtINV002Comment.Text, "", "", "", "", "", "");
                    oDiscrepancyBE.INVActionRequired = false;

                    oDISLog_WorkFlowBE.Action = "GetAccountPayable";
                    string VendorAccountPayablesEmail = oDISLog_WorkFlowBAL.GetVendorAccountPayablesEmail(oDISLog_WorkFlowBE);
                    
//                    oDiscrepancyBE.APHTML = oWorkflowHTML.function2(DateTime.Now, null, "Blue", VendorAccountPayablesEmail,
//                                                                            @"Build on to Accounts Payable Work list with following request",
//                                                                            @"A Discrepancy has been raised which requires
//                                                                        for debit to be raised or credit to be issued.
//                                                                         Please action accordingly upon receipt of invoice.",
//                                                                            "", "", "", "", "", "");

                    oDiscrepancyBE.APHTML = oWorkflowHTML.function8("green",
                                                                      @"A Discrepancy has been raised which may require a debit to be raised or credit to be issued. Please actions accordingly upon receipt of invoice.");
                    oDiscrepancyBE.APActionRequired = false; // true;
                    //oDiscrepancyBE.APUserControl = "ACP001";

                    if (sAction == "Reduce Purchase Order")
                    {

                        oDiscrepancyBE.VENHTML = oWorkflowHTML.function4("Blue", "", null, sentToLinks, "Admin@OfficeDepot.com",
                                                                         "", @"Goods received damaged, do not resend damaged goods. Please arrange collection", "", "", DateTime.Now);
                        if (Session["GRD"] != null && Convert.ToBoolean(Session["GRD"]) == true)
                        {
                            oDiscrepancyBE.VENUserControl = "VEN027";
                            oDiscrepancyBE.VendorActionCategory = "RA";
                        }
                        else
                        {
                            oDiscrepancyBE.VENUserControl = "VEN005";
                            oDiscrepancyBE.VendorActionCategory = "AR";
                        }
                        
                    }
                    else
                    {
                        oDiscrepancyBE.VENHTML = oWorkflowHTML.function4("Blue", "", null, sentToLinks, "Admin@OfficeDepot.com",
                                                                          "", @"Items have been delivered damaged.Please arrange collection of damaged items and send replacements  ", "", "", DateTime.Now);
                        if (Session["GRD"] != null && Convert.ToBoolean(Session["GRD"]) == true)
                        {
                            oDiscrepancyBE.VENUserControl = "VEN027";
                            oDiscrepancyBE.VendorActionCategory = "RA";
                        }
                        else
                        {
                            oDiscrepancyBE.VENUserControl = "VEN006";
                            oDiscrepancyBE.VendorActionCategory = "AR";
                        }                        
                    }

                    oDiscrepancyBE.VenActionRequired = true;

                    oDiscrepancyBE.Action = "InsertHTML";
                    oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
                    oDiscrepancyBE.LoggedDateTime = DateTime.Now;
                    oDiscrepancyBE.LevelNumber = 1;
                    oDiscrepancyBE.ACP001Control = "ACP001";
                    oDiscrepancyBE.IsACP001Done = false;
                    oDiscrepancyBE.ActionTakenBy = "INV";
                    int? iResult1 = oDiscrepancyBAL.addWorkFlowHTMLsBAL(oDiscrepancyBE);

                }
                else
                {
                    /*Enter comments with date*/
                    InsertHTML();
                }
            }


            //Redirecting to Previous Page
            RedirectToPage(GetQueryStringValue("PreviousPage"));
        }
    }

    #endregion

    #region Methods

    private string SendMailAndGetMailLinks(int DiscrepancyLogID, string discrepancyType, string sCurrentCommunicationLevel, string sNextCommunicationLevel, string INVAction )
    {
        sendCommunicationAllLevel osendCommunicationAllLevel = new sendCommunicationAllLevel();
        return osendCommunicationAllLevel.SendMailAndGetMailLinks(DiscrepancyLogID, discrepancyType, sCurrentCommunicationLevel, sNextCommunicationLevel, INVAction);
    }

    private void InsertHTML()
    {

        DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
        DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();

        WorkflowHTML oWorkflowHTML = new WorkflowHTML();
        //oDiscrepancyBE.INVHTML = oWorkflowHTML.function3("Yellow", DateTime.Now, txtINV002Comment.Text, "");
        oDiscrepancyBE.VENHTML = oWorkflowHTML.function1(DateTime.Now, null,
            null, "Yellow", "", "",
            //Session["UserName"].ToString(),
            UIUtility.GetODVendorUserName(Convert.ToString(Session["Role"]), Convert.ToString(Session["UserName"]), Convert.ToString(Session["LoginID"])),
            txtINV002Comment.Text);


        oDiscrepancyBE.Action = "InsertHTML";
        oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
        oDiscrepancyBE.LoggedDateTime = DateTime.Now;
        oDiscrepancyBE.LevelNumber = 1;
        int? iResult = oDiscrepancyBAL.addWorkFlowHTMLsBAL(oDiscrepancyBE);
    }

    #endregion

    protected void btnBack_Click(object sender, EventArgs e)
    {
        //Redirecting to Previous Page
        RedirectToPage(GetQueryStringValue("PreviousPage"));
    }
}
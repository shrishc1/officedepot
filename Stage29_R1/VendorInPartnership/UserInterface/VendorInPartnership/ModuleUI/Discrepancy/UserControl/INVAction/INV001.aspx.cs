﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.Discrepancy.LogDiscrepancy;
using BusinessLogicLayer.ModuleBAL.Discrepancy.LogDiscrepancy;
using BaseControlLibrary;
using WebUtilities;
using BusinessEntities.ModuleBE.Security;
using BusinessLogicLayer.ModuleBAL.Security;

public partial class INVAction_INV001 : CommonPage
{

    #region Events

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
          //  ScriptManager.GetCurrent(this).RegisterPostBackControl(btnSave);
            chkINV001aStageCompleted.Checked = true;
            txtINV001aPurchaseOrder.Text = GetQueryStringValue("PONo").ToString();
            //GetWorkFlowDetails();
            if (GetQueryStringValue("FromPage") != null && GetQueryStringValue("FromPage").ToString().Trim() == "NoPO")
            {
                trNoPODesc.Style["display"] = "";
                trOversDesc.Style["display"] = "none";
                trNoPaperworkDesc.Style["display"] = "none";
                trItemNotOnPoDesc.Style["display"] = "none";
            }
            if (GetQueryStringValue("FromPage") != null && GetQueryStringValue("FromPage").ToString().Trim() == "NoPaperwork")
            {
                trNoPODesc.Style["display"] = "none";
                trOversDesc.Style["display"] = "none";
                trNoPaperworkDesc.Style["display"] = "";
                trItemNotOnPoDesc.Style["display"] = "none";
            }
            if (GetQueryStringValue("FromPage") != null && GetQueryStringValue("FromPage").ToString().Trim() == "ItemNotOnPo")
            {
                trNoPODesc.Style["display"] = "none";
                trOversDesc.Style["display"] = "none";
                trNoPaperworkDesc.Style["display"] = "none";
                trItemNotOnPoDesc.Style["display"] = "";                
            }
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        bool IsINV001ActionRequiredChecked = clsDiscrepancy.ISRadioButtonChecked(rdoINV001aGoodsKept, rdoINV001aGoodsNAXOrder, rdoINV001aGoodsReturned);
        if (IsINV001ActionRequiredChecked == false)
        {
            string INV001ActionRequiredMessage = WebCommon.getGlobalResourceValue("INV001ActionRequired");
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('" + INV001ActionRequiredMessage + "')", true);
            return;
        }

        /*
         * if goods to be kept or goods to be kept NAX order then go to 2a
         * else if goods to be returned go to 2b
         */
        ScriptManager.RegisterStartupScript(this, this.GetType(), "HideButton", "HideButton();", true);
        btnSave.Enabled = false;
        DISLog_WorkFlowBE oDISLog_WorkFlowBE = new DISLog_WorkFlowBE();
        DISLog_WorkFlowBAL oDISLog_WorkFlowBAL = new DISLog_WorkFlowBAL();
        string sentToLinks = string.Empty;
        if (GetQueryStringValue("disLogID") != null && GetQueryStringValue("disLogID").ToString() != "")
        {
            //********************************************************** For Update Location
            string Location = hdfLocation.Value;
            DiscrepancyBE oDiscrepancyBE1 = new DiscrepancyBE();
            DiscrepancyBAL oDiscrepancyBAL1 = new DiscrepancyBAL();
            oDiscrepancyBE1.Action = "UpdateLocation";
            oDiscrepancyBE1.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
            oDiscrepancyBE1.Location = Location;
            oDiscrepancyBE1.UserID = Convert.ToInt32(Session["UserID"]);
            string sResult1 = oDiscrepancyBAL1.updateDiscrepancyLocationBAL(oDiscrepancyBE1);
            //**********************************************************

            oDISLog_WorkFlowBE.Action = "InsertUpdateWorkflowActions";
            oDISLog_WorkFlowBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());

            if (rdoINV001aGoodsKept.Checked)
            {
                oDISLog_WorkFlowBE.INV001_GoodsKeptDecision = "K"; //Goods to be kept
                oDISLog_WorkFlowBE.VenCommunicationType = "Kept";
                oDISLog_WorkFlowBE.Decision = "Goods to be kept";
            }
            else if (rdoINV001aGoodsNAXOrder.Checked)
            {
                oDISLog_WorkFlowBE.INV001_GoodsKeptDecision = "N"; //Goods to be kept NAX order
                oDISLog_WorkFlowBE.VenCommunicationType = "Kept";
                oDISLog_WorkFlowBE.Decision = "Goods to be kept NAX order";
            }
            else if (rdoINV001aGoodsReturned.Checked)
            {
                oDISLog_WorkFlowBE.INV001_GoodsKeptDecision = "R"; //Goods to be returned
                oDISLog_WorkFlowBE.VenCommunicationType = "Return";
                oDISLog_WorkFlowBE.Decision = "Goods to be returned";
            }
            oDISLog_WorkFlowBE.INV001_NAX_PO = txtINV001aPurchaseOrder.Text;
            oDISLog_WorkFlowBE.StageCompletedForWhom = "INV001_StageCompleted";
            if (chkINV001aStageCompleted.Checked)
            {
                oDISLog_WorkFlowBE.StageCompleted = true;
            }
            else
            {
                oDISLog_WorkFlowBE.StageCompleted = false;
            }
            oDISLog_WorkFlowBE.Comment = txtINV001aComment.Text;

            //update the work flow action table each time for the comments and other fields( if stage is completed)
            oDISLog_WorkFlowBE.DiscrepancyWorkFlowID = Convert.ToInt32(GetQueryStringValue("DisWFlowID").ToString());

            //insert or update the record according to the stage completed status
            int? iResult = oDISLog_WorkFlowBAL.addWorkFlowActionsBAL(oDISLog_WorkFlowBE);

            //now insert the work flow HTML according to the action taken only if the stage is completed
            DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
            DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();

            #region Overs

            if (GetQueryStringValue("FromPage") != null && GetQueryStringValue("FromPage").ToString().Trim() == "overs")
            {

                if (chkINV001aStageCompleted.Checked)
                {

                    WorkflowHTML oWorkflowHTML = new WorkflowHTML();
                    string sAction = oDISLog_WorkFlowBE.INV001_GoodsKeptDecision == "K" ? "Goods to be kept"
                                    : (oDISLog_WorkFlowBE.INV001_GoodsKeptDecision == "N" ? "Goods to be kept NAX order"
                                    : "Goods to be returned");

                    string sPO = oDISLog_WorkFlowBE.INV001_GoodsKeptDecision == "K" || oDISLog_WorkFlowBE.INV001_GoodsKeptDecision == "N" ? txtINV001aPurchaseOrder.Text : GetQueryStringValue("PONo").ToString();

                    //if goods to be kept or goods to be kept NAX order then go to 2b
                    if (rdoINV001aGoodsKept.Checked || rdoINV001aGoodsNAXOrder.Checked)
                    {
                        // send mail of communication3              
                        sentToLinks = SendMailAndGetMailLinks(Convert.ToInt32(GetQueryStringValue("disLogID")), "qtydifferenceovers", "Communication1", "Communication3");

                        oDiscrepancyBE.GINHTML = oWorkflowHTML.function2(DateTime.Now, null, "Blue", "Goods-In",
                                                                            "",
                                                                            @"Please confirm receipt of the over delivered
                                                                            item against Purchase order: " + sPO,
                                                                            "Action Required", "", "", "", "", "");

                        oDiscrepancyBE.GINActionRequired = true;
                        oDiscrepancyBE.GINUserControl = "GIN001";
                        oDiscrepancyBE.INVHTML = oWorkflowHTML.function1(DateTime.Now, null, null, "Yellow",
                                                                            "",
                                                                            sAction,
                                                                            //Session["UserName"].ToString(),
                                                                            UIUtility.GetODVendorUserName(Convert.ToString(Session["Role"]), Convert.ToString(Session["UserName"]), Convert.ToString(Session["LoginID"])),
                                                                            txtINV001aComment.Text,
                                                                            sPO, "", "", "", "", "");
                        oDiscrepancyBE.INVActionRequired = false;

                        oDiscrepancyBE.APHTML = oWorkflowHTML.function3("White", null, "", "");
                        oDiscrepancyBE.APActionRequired = false;
                        oDiscrepancyBE.VENHTML = oWorkflowHTML.function2(DateTime.Now, null, "Blue", sentToLinks,
                                                                            @"Follow up communication sent and discrepancy builds
                                                                            onto vendor worklist with following request",
                                                                            @"Please select the reason the goods were over delivered
                                                                            and confirm of remedial action.",
                                                                            "", "", "A030000492", "", "", "");
                        oDiscrepancyBE.VenActionRequired = true;
                        oDiscrepancyBE.VENUserControl = "VEN002";

                        oDiscrepancyBE.Action = "InsertHTML";
                        oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
                        oDiscrepancyBE.LoggedDateTime = DateTime.Now;
                        oDiscrepancyBE.LevelNumber = 1;
                        oDiscrepancyBE.ActionTakenBy = "INV";
                        oDiscrepancyBE.VendorActionCategory = "RA";  //Remedial Action
                        oDiscrepancyBE.GINActionCategory = "RA";  //Action Required
                        int? iResult1 = oDiscrepancyBAL.addWorkFlowHTMLsBAL(oDiscrepancyBE);
                    }
                    else
                    {
                        //if goods to be returned go to 2a
                        // send mail of communication2
                        sentToLinks = SendMailAndGetMailLinks(Convert.ToInt32(GetQueryStringValue("disLogID")), "qtydifferenceovers", "Communication1", "Communication2");

                        oDiscrepancyBE.GINHTML = oWorkflowHTML.function3("White", null, "", "");
                        oDiscrepancyBE.GINActionRequired = false;
                        oDiscrepancyBE.INVHTML = oWorkflowHTML.function1(DateTime.Now, null, null, "Yellow",
                                                                            "", sAction, 
                                                                            //Session["UserName"].ToString(), 
                                                                            UIUtility.GetODVendorUserName(Convert.ToString(Session["Role"]), Convert.ToString(Session["UserName"]), Convert.ToString(Session["LoginID"])),
                                                                            txtINV001aComment.Text.Trim(), "", "", "", "", "", "");
                        oDiscrepancyBE.INVActionRequired = false;

                        oDISLog_WorkFlowBE.Action = "GetAccountPayable";
                        string VendorAccountPayablesEmail = oDISLog_WorkFlowBAL.GetVendorAccountPayablesEmail(oDISLog_WorkFlowBE);
                        oDiscrepancyBE.APHTML = oWorkflowHTML.function8("green",
                                                                        @"A Discrepancy has been raised which may require a debit to be raised or credit to be issued. Please actions accordingly upon receipt of invoice." );
                        oDiscrepancyBE.APActionRequired = false; //true;
                        //oDiscrepancyBE.APUserControl = "ACP001";

                        oDiscrepancyBE.VENHTML = oWorkflowHTML.function2(DateTime.Now, null, "Blue", sentToLinks,
                                                                            @"Follow up communication sent and discrepancy builds
                                                                            onto vendor worklist with following request
                                                                            Please select the reason the goods were over delivered
                                                                            and select method of return.", "",
                                                                            "", "", "A030000492", "", "", "");
                        oDiscrepancyBE.VenActionRequired = true;
                        oDiscrepancyBE.VENUserControl = "VEN001";
                        oDiscrepancyBE.Action = "InsertHTML";
                        oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
                        oDiscrepancyBE.LoggedDateTime = DateTime.Now;
                        oDiscrepancyBE.LevelNumber = 1;
                        oDiscrepancyBE.ACP001Control = "ACP001";
                        oDiscrepancyBE.IsACP001Done = false;
                        oDiscrepancyBE.ActionTakenBy = "INV";
                        oDiscrepancyBE.VendorActionCategory = "AR";  //Action Required
                        int? iResult1 = oDiscrepancyBAL.addWorkFlowHTMLsBAL(oDiscrepancyBE);
                    }
                }
                else
                {
                    /*Enter comments with date*/
                    InsertHTML();
                }
            }
            #endregion

            #region NoPO+NoPaperwork

            if (GetQueryStringValue("FromPage") != null && GetQueryStringValue("FromPage").ToString().Trim() == "NoPO"
               || GetQueryStringValue("FromPage") != null && GetQueryStringValue("FromPage").ToString().Trim() == "NoPaperwork"
                )
            {

                if (chkINV001aStageCompleted.Checked)
                {

                    WorkflowHTML oWorkflowHTML = new WorkflowHTML();
                    string sAction = oDISLog_WorkFlowBE.INV001_GoodsKeptDecision == "K" ? "Goods to be kept"
                                    : (oDISLog_WorkFlowBE.INV001_GoodsKeptDecision == "N" ? "Goods to be kept NAX order"
                                    : "Goods to be returned");
                    string sPO = oDISLog_WorkFlowBE.INV001_GoodsKeptDecision == "K" || oDISLog_WorkFlowBE.INV001_GoodsKeptDecision == "N" ? txtINV001aPurchaseOrder.Text : GetQueryStringValue("PONo").ToString();

                    //if goods to be kept or goods to be kept NAX order then go to 2a
                    if (rdoINV001aGoodsKept.Checked || rdoINV001aGoodsNAXOrder.Checked)
                    {
                        if (GetQueryStringValue("FromPage").ToString().Trim() == "NoPO")
                            sentToLinks = SendMailAndGetMailLinks(Convert.ToInt32(GetQueryStringValue("disLogID")), "nopurchaseordernumber", "Communication1", "Communication2");
                        else if (GetQueryStringValue("FromPage").ToString().Trim() == "NoPaperwork")
                            sentToLinks = SendMailAndGetMailLinks(Convert.ToInt32(GetQueryStringValue("disLogID")), "nopaperwork", "Communication1", "Communication3");

                        oDiscrepancyBE.GINHTML = oWorkflowHTML.function2(DateTime.Now, null, "Blue", "",
                                                                             @"Build onto Goods in Work list on " + DateTime.Now.ToString("dd/MM/yyyy") + " with following request",
                                                                             @"Please confirm receipt of the delivered
                                                                            item against Purchase order: " + sPO);

                        oDiscrepancyBE.GINActionRequired = true;
                        oDiscrepancyBE.GINUserControl = "GIN005";

                        oDiscrepancyBE.INVHTML = oWorkflowHTML.function1(DateTime.Now, null, null, "Yellow",
                                                                            "",
                                                                            sAction,
                                                                            //Session["UserName"].ToString(),
                                                                            UIUtility.GetODVendorUserName(Convert.ToString(Session["Role"]), Convert.ToString(Session["UserName"]), Convert.ToString(Session["LoginID"])),
                                                                            txtINV001aComment.Text,
                                                                            sPO, "", "", "", "", "");
                        oDiscrepancyBE.INVActionRequired = false;

                        oDiscrepancyBE.APHTML = oWorkflowHTML.function3("White", null, "", "");
                        oDiscrepancyBE.APActionRequired = false;

                        oDiscrepancyBE.VENHTML = oWorkflowHTML.function4("Blue", "", DateTime.Now, sentToLinks, "amitc@damcogroup.com,"
                                                                           , @"Goods were delivered without any paperwork. 
                                                                            On this occassion it was accepted but may not 
                                                                            be in the future. Please advise of remedial actions to ensure this does not occur in future", ""
                                                                            , "", "", null, "");
                        oDiscrepancyBE.VenActionRequired = true;
                        oDiscrepancyBE.VENUserControl = "VEN012";// calling VEN012 instead of VEN010 in case of PO and VEN008 in case of paperwork

                        oDiscrepancyBE.Action = "InsertHTML";
                        oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
                        oDiscrepancyBE.LoggedDateTime = DateTime.Now;
                        oDiscrepancyBE.LevelNumber = 1;
                        oDiscrepancyBE.ActionTakenBy = "INV";
                        oDiscrepancyBE.VendorActionCategory = "RA";
                        oDiscrepancyBE.GINActionCategory = "RA";  //Action Required
                        int? iResult1 = oDiscrepancyBAL.addWorkFlowHTMLsBAL(oDiscrepancyBE);
                    }
                    else
                    {
                        // if goods to be returned go to 2b
                        if (GetQueryStringValue("FromPage").ToString().Trim() == "NoPO")
                            sentToLinks = SendMailAndGetMailLinks(Convert.ToInt32(GetQueryStringValue("disLogID")), "nopurchaseordernumber", "Communication1", "Communication3");
                        else if (GetQueryStringValue("FromPage").ToString().Trim() == "NoPaperwork")
                            sentToLinks = SendMailAndGetMailLinks(Convert.ToInt32(GetQueryStringValue("disLogID")), "nopaperwork", "Communication1", "Communication2");

                        oDiscrepancyBE.GINHTML = oWorkflowHTML.function3("White", null, "", "");
                        oDiscrepancyBE.GINActionRequired = false;

                        oDiscrepancyBE.INVHTML = oWorkflowHTML.function1(DateTime.Now, null, null, "Yellow",
                                                                            "", sAction, 
                                                                            //Session["UserName"].ToString(), 
                                                                            UIUtility.GetODVendorUserName(Convert.ToString(Session["Role"]), Convert.ToString(Session["UserName"]), Convert.ToString(Session["LoginID"])),
                                                                            txtINV001aComment.Text.Trim());
                        oDiscrepancyBE.INVActionRequired = false;

                        oDiscrepancyBE.APHTML = oWorkflowHTML.function3("White", null, "", "");
                        oDiscrepancyBE.APActionRequired = false;

                        if (GetQueryStringValue("FromPage").ToString().Trim() == "NoPaperwork")
                        {
                            oDiscrepancyBE.VENHTML = oWorkflowHTML.function4("Blue", "", DateTime.Now, sentToLinks, "amitc@damcogroup.com,"
                                                                            , @"Follow up communication sent and discrepancy builds
                                                                            onto vendor worklist with following request - No Paperwork has been supplied with this delivery 
                                                                            so we are unable to accept. Please decide method of return "
                                                                            , ""
                                                                            , "", "", null, "");

                        }
                        else
                        {
                            oDiscrepancyBE.VENHTML = oWorkflowHTML.function4("Blue", "", DateTime.Now, sentToLinks, "amitc@damcogroup.com,"
                                                                            , @"Follow up communication sent and discrepancy builds
                                                                            onto vendor worklist with following request"
                                                                            , @"No PO has been supplied with this delivery so 
                                                                            we are unable to accept. Please decide method if return"
                                                                            , "", "", null, "");

                        }

                        oDiscrepancyBE.VenActionRequired = true;
                        oDiscrepancyBE.VENUserControl = "VEN011"; //calling VEN011 instead of VEN009 in case of PO and VEN007 in case of Nopaperwork.
                        oDiscrepancyBE.Action = "InsertHTML";
                        oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
                        oDiscrepancyBE.LoggedDateTime = DateTime.Now;
                        oDiscrepancyBE.LevelNumber = 1;
                        oDiscrepancyBE.ActionTakenBy = "INV";
                        oDiscrepancyBE.VendorActionCategory = "AR";  //Action Required
                        int? iResult1 = oDiscrepancyBAL.addWorkFlowHTMLsBAL(oDiscrepancyBE);
                    }
                }
                else
                {
                    /*Enter comments with date*/
                    InsertHTML();
                }
            }
            #endregion


            #region ItemNotOnPO

            if (GetQueryStringValue("FromPage") != null && GetQueryStringValue("FromPage").ToString().Trim() == "ItemNotOnPo")
            {

                if (chkINV001aStageCompleted.Checked)
                {

                    WorkflowHTML oWorkflowHTML = new WorkflowHTML();
                    string sAction = oDISLog_WorkFlowBE.INV001_GoodsKeptDecision == "K" ? "Keep Goods"
                                    : (oDISLog_WorkFlowBE.INV001_GoodsKeptDecision == "N" ? "Keep Goods NAX"
                                    : "Goods to be returned");
                    string sPO = oDISLog_WorkFlowBE.INV001_GoodsKeptDecision == "K" || oDISLog_WorkFlowBE.INV001_GoodsKeptDecision == "N" ? txtINV001aPurchaseOrder.Text : GetQueryStringValue("PONo").ToString();

                    //if goods to be kept or goods to be kept NAX order then go to 2a
                    if (rdoINV001aGoodsKept.Checked || rdoINV001aGoodsNAXOrder.Checked)
                    {                      
                       
                        oDiscrepancyBE.GINHTML = oWorkflowHTML.function2(DateTime.Now, null, "Blue", "",
                                                                             @"Build onto Goods in Work list on " + DateTime.Now.ToString("dd/MM/yyyy") + " with following request",
                                                                             @"Please confirm receipt of the delivered
                                                                            item against Purchase order: " + sPO);

                        oDiscrepancyBE.GINActionRequired = true;
                        oDiscrepancyBE.GINUserControl = "GIN005";

                        oDiscrepancyBE.INVHTML = oWorkflowHTML.function1(DateTime.Now, null, null, "Yellow",
                                                                            "",
                                                                            sAction,
                                                                            //Session["UserName"].ToString(),
                                                                            UIUtility.GetODVendorUserName(Convert.ToString(Session["Role"]), Convert.ToString(Session["UserName"]), Convert.ToString(Session["LoginID"])),
                                                                            txtINV001aComment.Text,
                                                                            sPO, "", "", "", "", "");
                        oDiscrepancyBE.INVActionRequired = false;

                        oDiscrepancyBE.APHTML = oWorkflowHTML.function3("White", null, "", "");
                        oDiscrepancyBE.APActionRequired = false;

                        oDiscrepancyBE.VENHTML = oWorkflowHTML.function3("White", null, "", "");
                        oDiscrepancyBE.VenActionRequired = false;

                       

                        oDiscrepancyBE.Action = "InsertHTML";
                        oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
                        oDiscrepancyBE.LoggedDateTime = DateTime.Now;
                        oDiscrepancyBE.LevelNumber = 1;
                        oDiscrepancyBE.ActionTakenBy = "INV";
                        oDiscrepancyBE.VendorActionCategory = "RA";
                        oDiscrepancyBE.GINActionCategory = "RA";  //Action Required

                        int? iResult1 = oDiscrepancyBAL.addWorkFlowHTMLsBAL(oDiscrepancyBE);
                    }
                    else
                    {
                        // if goods to be returned go to 2b
                        if (GetQueryStringValue("FromPage").ToString().Trim() == "ItemNotOnPo")
                        {
                            //HiddenField hdnEmail = new HiddenField();
                            //HiddenField hdnFax = new HiddenField();
                            //SCT_UserBE oSCT_UserBE = new SCT_UserBE();
                            //SCT_UserBAL oSCT_UserBAL = new SCT_UserBAL();
                            //int? vendorID = 0;
                            //int? Siteid = 0;

                            //oSCT_UserBE.Action = "GetContactDetailsDiscrepancy";

                            //if (GetQueryStringValue("VendorID") != null)
                            //{
                            //    vendorID = Convert.ToInt32(GetQueryStringValue("VendorID"));

                            //    oSCT_UserBE.VendorID = vendorID;
                            //}
                            //if (GetQueryStringValue("Siteid") != null)
                            //{
                            //    Siteid = Convert.ToInt32(GetQueryStringValue("Siteid"));
                            //    oSCT_UserBE.SiteId = Siteid;
                            //}

                            //List<SCT_UserBE> lstVendorDetails1 = oSCT_UserBAL.GetUserDetailsBAL(oSCT_UserBE);

                            //hdnEmail.Value = "";
                            //hdnFax.Value = "";
                            //if (lstVendorDetails1 != null && lstVendorDetails1.Count > 0)
                            //{
                            //    foreach (SCT_UserBE item in lstVendorDetails1)
                            //    {
                            //        hdnEmail.Value += item.EmailId.ToString() + ", ";
                            //        hdnFax.Value += item.FaxNumber.ToString() + ", ";
                            //    }
                            //    hdnEmail.Value = hdnEmail.Value.Trim(new char[] { ',', ' ' });
                            //    hdnFax.Value = hdnFax.Value.Trim(new char[] { ',', ' ' });
                            //}

                            //txtVendorCommList.Text = hdnEmail.Value;

                            int? vendorID = 0;

                            if (GetQueryStringValue("VendorID") != null)
                            {
                                vendorID = Convert.ToInt32(GetQueryStringValue("VendorID"));                                
                            }

                            // Check for @nd Communication
                            //sendCommunication communication = new sendCommunication();
                            //int? retVal = communication.sendCommunicationByEMail(Convert.ToInt32(GetQueryStringValue("disLogID")), "itemnotonpo",
                            //     txtVendorAltCommList,txtVendorCommList, vendorID, string.Empty, string.Empty, string.Empty, 0, true);
                            //sentToLinks = communication.sSendMailLink.ToString();


                             sentToLinks = SendMailAndGetMailLinks(Convert.ToInt32(GetQueryStringValue("disLogID")), "itemnotonpo", "Communication1", "Communication2");
                        }

                        oDiscrepancyBE.GINHTML = oWorkflowHTML.function3("White", null, "", "");
                        oDiscrepancyBE.GINActionRequired = false;

                        oDiscrepancyBE.INVHTML = oWorkflowHTML.function1(DateTime.Now, null, null, "Yellow",
                                                                            "", sAction,
                                                                            //Session["UserName"].ToString(), 
                                                                            UIUtility.GetODVendorUserName(Convert.ToString(Session["Role"]), Convert.ToString(Session["UserName"]), Convert.ToString(Session["LoginID"])),
                                                                            txtINV001aComment.Text.Trim(),
                                                                            sPO, "", "", "", "", "");
                        oDiscrepancyBE.INVActionRequired = false;

                        oDiscrepancyBE.APHTML = oWorkflowHTML.function3("White", null, "", "");
                        oDiscrepancyBE.APActionRequired = false;

                        if (GetQueryStringValue("FromPage").ToString().Trim() == "ItemNotOnPo")
                        {
                            oDiscrepancyBE.VENHTML = oWorkflowHTML.function2(DateTime.Now, null, "Blue", sentToLinks,
                                                                             @"Build onto Work list on " + DateTime.Now.ToString("dd/MM/yyyy") + " with following request",
                                                                             @"An item that has been closed or was not on your PO has
                                                                            been received and is not required. Please select method
                                                                            of return");

                        }                       

                        oDiscrepancyBE.VenActionRequired = true;
                        oDiscrepancyBE.VENUserControl = "VEN011"; 
                        oDiscrepancyBE.Action = "InsertHTML";
                        oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
                        oDiscrepancyBE.LoggedDateTime = DateTime.Now;
                        oDiscrepancyBE.LevelNumber = 1;
                        oDiscrepancyBE.ActionTakenBy = "INV";
                        oDiscrepancyBE.VendorActionCategory = "AR";
                        int? iResult1 = oDiscrepancyBAL.addWorkFlowHTMLsBAL(oDiscrepancyBE);
                    }
                }
                else
                {
                    /*Enter comments with date*/
                    InsertHTML();
                }
            }
            #endregion


            //Redirecting to Previous Page
            RedirectToPage(GetQueryStringValue("PreviousPage"));

        }
    }

    #endregion

    #region Methods

    private string SendMailAndGetMailLinks(int DiscrepancyLogID, string discrepancyType, string sCurrentCommunicationLevel, string sNextCommunicationLevel)
    {
        sendCommunicationAllLevel osendCommunicationAllLevel = new sendCommunicationAllLevel();
        return osendCommunicationAllLevel.SendMailAndGetMailLinks(DiscrepancyLogID, discrepancyType, sCurrentCommunicationLevel, sNextCommunicationLevel);
        //return osendCommunicationAllLevel.sendCommunicationByEMail(DiscrepancyLogID, discrepancyType, sCurrentCommunicationLevel, sNextCommunicationLevel);
    }

    public void GetWorkFlowDetails()
    {
        if (GetQueryStringValue("disLogID") != null && GetQueryStringValue("disLogID").ToString() != "")
        {
            DISLog_WorkFlowBE oDISLog_WorkFlowBE = new DISLog_WorkFlowBE();
            DISLog_WorkFlowBAL oDISLog_WorkFlowBAL = new DISLog_WorkFlowBAL();
            oDISLog_WorkFlowBE.Action = "GetWorkFlowActionDetails";
            oDISLog_WorkFlowBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
            List<DISLog_WorkFlowBE> lstWorkFlowActionDetails = oDISLog_WorkFlowBAL.GetWorkFlowActionsBAL(oDISLog_WorkFlowBE);
            if (lstWorkFlowActionDetails != null && lstWorkFlowActionDetails.Count > 0)
            {
                if (lstWorkFlowActionDetails[0].INV001_GoodsKeptDecision.ToString().Trim() == "K")
                {
                    rdoINV001aGoodsKept.Checked = true;
                }
                else if (lstWorkFlowActionDetails[0].INV001_GoodsKeptDecision.ToString().Trim() == "N")
                {
                    rdoINV001aGoodsNAXOrder.Checked = true;
                }
                else if (lstWorkFlowActionDetails[0].INV001_GoodsKeptDecision.ToString().Trim() == "R")
                {
                    rdoINV001aGoodsReturned.Checked = true;
                }
                txtINV001aPurchaseOrder.Text = lstWorkFlowActionDetails[0].INV001_NAX_PO.ToString().Trim();

                if (lstWorkFlowActionDetails[0].StageCompleted == true)
                {
                    chkINV001aStageCompleted.Checked = true;
                }
                else
                {
                    chkINV001aStageCompleted.Checked = false;
                    txtINV001aComment.Style["display"] = "";
                    // txtINV001aComment.Text = lstWorkFlowActionDetails[0].INV001_Comments.ToString().Trim();
                }
            }
        }
    }

    private void InsertHTML()
    {

        DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
        DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();

        WorkflowHTML oWorkflowHTML = new WorkflowHTML();
        oDiscrepancyBE.INVHTML = oWorkflowHTML.function3("White", DateTime.Now, txtINV001aComment.Text, "");


        oDiscrepancyBE.Action = "InsertHTML";
        oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
        oDiscrepancyBE.LoggedDateTime = DateTime.Now;
        oDiscrepancyBE.LevelNumber = 1;
        int? iResult = oDiscrepancyBAL.addWorkFlowHTMLsBAL(oDiscrepancyBE);
    }

    #endregion

    protected void btnBack_Click(object sender, EventArgs e)
    {
        //Redirecting to Previous Page
        RedirectToPage(GetQueryStringValue("PreviousPage"));
    }
}
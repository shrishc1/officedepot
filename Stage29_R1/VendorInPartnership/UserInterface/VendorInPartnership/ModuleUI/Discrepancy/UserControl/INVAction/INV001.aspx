﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="INV001.aspx.cs" Inherits="INVAction_INV001" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Office Depot</title>
    <link rel="stylesheet" type="text/css" href="../../../../Css/style.css" />
    <link type="text/css" href="../../../../Css/ui-lightness/jquery-ui-1.8.5.custom.css"
        rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="../../../../Css/superfish.css" media="screen" />
    <script type="text/javascript" src="<%= ResolveClientUrl("../../../../Scripts/jquery-1.2.6.min.js")%>"></script>
    <script type="text/javascript" src="<%= ResolveClientUrl("../../../../Scripts/jquery-1.4.2.min.js")%>"></script>
    <script type="text/javascript" src="<%= ResolveClientUrl("../../../../Scripts/jquery-ui-1.8.5.custom.min.js")%>"></script>
    <script type="text/javascript" src="<%= ResolveClientUrl("../../../../Scripts/hoverIntent.js")%>"></script>
    <script type="text/javascript" src="<%= ResolveClientUrl("../../../../Scripts/superfish.js")%>"></script>
    <script type="text/javascript" src="<%= ResolveClientUrl("../../../../Scripts/ValidateInputs.js")%>"></script>
    <script type="text/javascript">

        function showHide(showORhide) {
            var objtr1 = document.getElementById('<%=trAdvisePurchaseOrder.ClientID %>');
            var objtr2 = document.getElementById('<%=trComments.ClientID %>');
            var objtr3 = document.getElementById('<%=trGoodsReturnReason.ClientID %>');
            var objINV001aPurchaseOrderRequired = document.getElementById('<%=rfvINV001aPurchaseOrderRequired.ClientID %>');

            if (showORhide == '1') {
                objtr1.style.display = '';
                objtr2.style.display = '';
                objtr3.style.display = 'none';

                ValidatorEnable(objINV001aPurchaseOrderRequired, true);
            }

            if (showORhide == '2') {
                objtr1.style.display = 'none';
                objtr2.style.display = 'none';
                objtr3.style.display = '';
                ValidatorEnable(objINV001aPurchaseOrderRequired, false);
            }


        }

        function INV001ActionRequired(sender, args) {
            var objrdo1 = document.getElementById('<%=rdoINV001aGoodsKept.ClientID %>');
            var objrdo2 = document.getElementById('<%=rdoINV001aGoodsNAXOrder.ClientID %>');
            var objrdo3 = document.getElementById('<%=rdoINV001aGoodsReturned.ClientID %>');
           
            if (objrdo1.checked == false && objrdo2.checked == false && objrdo3.checked == false) {
                args.IsValid = false;
            }
        }
        function StageCompleted() {
            var objStageCompleted = document.getElementById('<%=chkINV001aStageCompleted.ClientID %>');
            var objtxtINV001aComment = document.getElementById('<%=txtINV001aComment.ClientID %>');

            var objINV001ActionRequired = document.getElementById('<%=cusvINV001ActionRequired.ClientID %>');
            var objINV001aPurchaseOrderRequired = document.getElementById('<%=rfvINV001aPurchaseOrderRequired.ClientID %>');
            var objINV001aCommentRequired = document.getElementById('<%=rfvINV001aCommentRequired.ClientID %>');

            if (document.getElementById('<%=hdnValue.ClientID %>').value == '1') {

                ValidatorEnable(objINV001ActionRequired, false);
                ValidatorEnable(objINV001aPurchaseOrderRequired, false);
                objStageCompleted.checked = false;
                document.getElementById('<%=hdnValue.ClientID %>').value = '0';
                //                if (document.getElementById('<%=rdoINV001aGoodsReturned.ClientID %>').checked == true) {
                //                    ValidatorEnable(objINV001aPurchaseOrderRequired, false);
                //                }

            }
            else if (document.getElementById('<%=hdnValue.ClientID %>').value == '0') {

                ValidatorEnable(objINV001ActionRequired, true);
                if (document.getElementById('<%=rdoINV001aGoodsKept.ClientID %>').checked || document.getElementById('<%=rdoINV001aGoodsNAXOrder.ClientID %>').checked) {
                    ValidatorEnable(objINV001aPurchaseOrderRequired, true);
                }
                else
                    ValidatorEnable(objINV001aPurchaseOrderRequired, false);

                //                if (document.getElementById('<%=rdoINV001aGoodsReturned.ClientID %>').checked == true) {
                //                    ValidatorEnable(objINV001aPurchaseOrderRequired, false);
                //                }


                objStageCompleted.checked = true;
                document.getElementById('<%=hdnValue.ClientID %>').value = '1';
               
            }
        }
        function EmptyTextbox() {
            document.getElementById('<%=txtINV001aPurchaseOrder.ClientID%>').value = "";

        }
        function GetLocation()
        {
            var Location = parent.document.getElementById('ctl00_ContentPlaceHolder1_txtLocation').value;
            document.getElementById('<%=hdfLocation.ClientID%>').value = Location;
            //  var updatePanel = parent.document.getElementById('ctl00_ContentPlaceHolder1_up1');
            if (checkValidationGroup("a")) {
                document.getElementById('<%=btnSave.ClientID%>').style.visibility = "hidden";
            }
            else {
                document.getElementById('<%=btnSave.ClientID%>').style.visibility = "visible";
                return false;
            }

        }
        function HideButton() {
            document.getElementById('btnSave').style.visibility = 'hidden';
        }

       function AssignVendorCommEmailList() {            
            var VendorCommList = parent.document.getElementById('ctl00_ContentPlaceHolder1_hdnVendorCommEmailList');
            document.getElementById('<%=txtVendorCommList.ClientID%>').value = VendorCommList.value;
          
           var VendorAltCommList = parent.document.getElementById('ctl00_ContentPlaceHolder1_hdnVendorAltCommEmailList');
           document.getElementById('<%=txtVendorAltCommList.ClientID%>').value = VendorAltCommList.value;
        }
    </script>
</head>
<body onload="AssignVendorCommEmailList()">
    <form id="form2" runat="server" method="post">
    <div>
        <cc1:ucTextbox ID ="txtVendorCommList" runat="server" Style="display:none" ></cc1:ucTextbox>
        <cc1:ucTextbox ID ="txtVendorAltCommList" runat="server"  Style="display:none"></cc1:ucTextbox>
        <%--<asp:HiddenField ID="hdnVendorCommList" runat="server" />
          <asp:HiddenField ID="hdnVendorAltCommList" runat="server" />--%>
        <asp:HiddenField ID="hdfLocation" runat="server" />
        <cc1:ucPanel ID="pnlActionRequired" runat="server" GroupingText="Action Required"
            Width="100%" CssClass="fieldset-form" BorderColor="gray" BorderStyle="Solid"
            BorderWidth="0">
            <table class="form-table" cellpadding="0" cellspacing="0" width="98%" style="margin: 2px;">
                <tr runat="server" id="trOversDesc">
                    <td style="font-weight: bold;">
                        <cc1:ucLabel CssClass="action-required-heading" ID="lblINV001aDesc" runat="server"
                            Style="text-align: center;" Text="Over's have been received please confirm if the over delivered goods are to be kept or returned to the vendor"></cc1:ucLabel>
                    </td>
                </tr>
                <tr runat="server" style="display: none" id="trNoPODesc">
                    <td style="font-weight: bold;">
                        <cc1:ucLabel ID="lblNoPODesc" CssClass="action-required-heading" runat="server" Style="text-align: center;"
                            Text="No Purchase Order has been quoted by the vendor. Please confirm the next action for this delivery?"></cc1:ucLabel>
                    </td>
                </tr>
                <tr runat="server" style="display: none" id="trNoPaperworkDesc">
                    <td style="font-weight: bold;">
                        <cc1:ucLabel ID="lblNoPaperworkDesc" CssClass="action-required-heading" runat="server"
                            Style="text-align: center;" Text="No Paperwork has been received from the vendor. Please confirm the next action for this delivery?"></cc1:ucLabel>
                    </td>
                </tr>
                 <tr runat="server" style="display: none" id="trItemNotOnPoDesc">
                    <td style="font-weight: bold;">
                        <cc1:ucLabel ID="ItemNotOnPODescMsg1" CssClass="action-required-heading" runat="server"
                            Style="text-align: center;" ></cc1:ucLabel>                                          
                    </td>
                </tr>
                <tr>
                    <td>
                        <table class="form-table" cellpadding="0" cellspacing="0" width="98%" style="margin: 2px;">
                            <tr>
                                <td width="21%">
                                    <cc1:ucLabel ID="lblINV001aAppOption" runat="server" Text="Please select Appropriate Option"></cc1:ucLabel>
                                </td>
                                <td width="1%">
                                    :
                                </td>
                                <td width="20%">
                                    <cc1:ucRadioButton ID="rdoINV001aGoodsKept" runat="server" GroupName="GoodsOption"
                                        onclick="javascript:showHide('1')" Text="Goods are to be Kept" />
                                </td>
                                <td width="24%">
                                    <cc1:ucRadioButton ID="rdoINV001aGoodsNAXOrder" runat="server" GroupName="GoodsOption"
                                        onclick="javascript:showHide('1');EmptyTextbox();" Text="Goods to be Kept NAX Order" />
                                </td>
                                <td width="34%">
                                    <cc1:ucRadioButton ID="rdoINV001aGoodsReturned" runat="server" GroupName="GoodsOption"
                                        onclick="javascript:showHide('2')" Text="Goods to be returned to Vendor" />
                                </td>
                            </tr>
                            <asp:CustomValidator ID="cusvINV001ActionRequired" runat="server" Text="Please take an action."
                                ClientValidationFunction="INV001ActionRequired" Display="None" ValidationGroup="a">
                            </asp:CustomValidator>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table class="form-table" cellpadding="0" cellspacing="0" width="100%" style="margin: 2px;">
                            <tr id="trAdvisePurchaseOrder" runat="server">
                                <td width="52%">
                                    <cc1:ucLabel ID="lblINV001aPurchaseOrder" runat="server" Text="Please advise of the Purchase Order which the Goods are to be booked in against"></cc1:ucLabel>
                                </td>
                                <td width="1%">
                                    :
                                </td>
                                <td style="text-align: left" width="47%">
                                    &nbsp;<cc1:ucTextbox ID="txtINV001aPurchaseOrder" runat="server" Width="100px" MaxLength="100"></cc1:ucTextbox>
                                    <asp:RequiredFieldValidator ID="rfvINV001aPurchaseOrderRequired" runat="server" ErrorMessage="Please enter your advise"
                                        Display="None" ValidationGroup="a" ControlToValidate="txtINV001aPurchaseOrder"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr id="trComments" runat="server">
                    <td>
                        <table class="form-table" cellpadding="0" cellspacing="0" width="100%" style="margin: 2px;">
                            <tr>
                                <td width="25%">
                                    <cc1:ucLabel ID="lblINV001aComment" runat="server" Text="Please enter any additional comments"></cc1:ucLabel>
                                </td>
                                <td width="75%">
                                    :
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr id="trGoodsReturnReason" runat="server" style="display: none">
                    <td>
                        <table class="form-table" cellpadding="0" cellspacing="0" width="100%" style="margin: 2px;">
                            <tr>
                                <td width="35%">
                                    <cc1:ucLabel ID="lblGoodsReturnReason" runat="server" Text="Please state why goods are to be returned to vendors"></cc1:ucLabel>
                                </td>
                                <td width="65%">
                                    :
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <cc1:ucTextbox ID="txtINV001aComment" CssClass="inputbox textarea" runat="server"
                            onkeyup="checkTextLengthOnKeyUp(this,1000);" TextMode="MultiLine" Width="98%"></cc1:ucTextbox>
                        <asp:RequiredFieldValidator ID="rfvINV001aCommentRequired" runat="server" Text="Please enter comments."
                            ControlToValidate="txtINV001aComment" Display="None" ValidationGroup="a"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table class="form-table" cellpadding="0" cellspacing="0" width="100%" style="margin: 2px;">
                            <tr>
                                <td width="14%" style="display:none;">
                                    <cc1:ucLabel ID="lblStageCompleted" runat="server" Text="Stage Completed"></cc1:ucLabel>
                                    <asp:ValidationSummary ID="vs1" runat="server" ValidationGroup="a" ShowSummary="false"
                                        ShowMessageBox="true" />
                                </td>
                                <td width="1%" style="display:none;">
                                    :
                                </td>
                                <td width="75%" align="left" style="display:none;">
                                    <cc1:ucCheckbox ID="chkINV001aStageCompleted" runat="server" Checked="true" />
                                </td>
                                <td width="10%" style="display:none;">
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3" style="display:none;">
                                    <cc1:ucLabel ID="lblDiscrepancyNote" runat="server" Text="If  you required to tag a comment without closing the discrepancy<br/>1 -    Deselect Stage Complete<br/>2 -   Enter  the comment<br/>3 -   Save"></cc1:ucLabel>
                                </td>
                                <td align="right" valign="bottom">
                                    <cc1:ucButton ID="btnSave" runat="server" Text="Save" class="button" ValidationGroup="a"
                                      OnClientClick="javascript:GetLocation()"   OnClick="btnSave_Click" />
                                    <input type="hidden" runat="server" id="hdnValue" value="1" />
                                    <cc1:ucButton ID="btnBack" runat="server" Text="Back" class="button" OnClick="btnBack_Click" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            
        </cc1:ucPanel>
    </div>
           
    </form>
</body>
</html>

﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="INV008.aspx.cs" Inherits="INVAction_INV008" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Office Depot</title>
    <link rel="stylesheet" type="text/css" href="../../../../Css/style.css" />
    <link type="text/css" href="../../../../Css/ui-lightness/jquery-ui-1.8.5.custom.css"
        rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="../../../../Css/superfish.css" media="screen" />
    <script type="text/javascript" src="<%= ResolveClientUrl("../../../../Scripts/jquery-1.2.6.min.js")%>"></script>
    <script type="text/javascript" src="<%= ResolveClientUrl("../../../../Scripts/jquery-1.4.2.min.js")%>"></script>
    <script type="text/javascript" src="<%= ResolveClientUrl("../../../../Scripts/jquery-ui-1.8.5.custom.min.js")%>"></script>
    <script type="text/javascript" src="<%= ResolveClientUrl("../../../../Scripts/hoverIntent.js")%>"></script>
    <script type="text/javascript" src="<%= ResolveClientUrl("../../../../Scripts/superfish.js")%>"></script>
    <script type="text/javascript" src="<%= ResolveClientUrl("../../../../Scripts/ValidateInputs.js")%>"></script>
     <script language="javascript" type="text/javascript">
         function PleaseTakeAnAction(sender, args) {
             var txtQualityIssueCharges = parent.document.getElementById('ctl00_ContentPlaceHolder1_hdnQualityIssueCharges').value;

             var objrdo1 = document.getElementById('<%=rdoINV008GoodsReturnToVendor.ClientID %>');
             var objrdo2 = document.getElementById('<%=rdoINV008ReworknotRequired.ClientID %>');
             var objrdo3 = document.getElementById('<%=rdoINV008ReworkNoCharge.ClientID %>');
             var objrdo4 = document.getElementById('<%=rdoINV008RewordAndCharge.ClientID %>');
             if (txtQualityIssueCharges != '') {
                 if (objrdo1.checked == false && objrdo2.checked == false && objrdo3.checked == false && objrdo4.checked == false) {
                     args.IsValid = false;
                 }
             }
             else {
                 if (objrdo1.checked == false && objrdo2.checked == false && objrdo3.checked == false) {
                     args.IsValid = false;
                 }
             }
         }
         function HideButton() {
             document.getElementById('btnSave').style.visibility = 'hidden';
         }
         function GetLocation() {
             var Location = parent.document.getElementById('ctl00_ContentPlaceHolder1_txtLocation').value;
             document.getElementById('<%=hdfLocation.ClientID%>').value = Location;
             if (checkValidationGroup("a")) {
                 document.getElementById('<%=btnSave.ClientID%>').style.visibility = "hidden";
             }
             else {
                 document.getElementById('<%=btnSave.ClientID%>').style.visibility = "visible";
                 return false;
             }
         }

         function GetChargeValue() {             
             var txtQualityIssueCharges = parent.document.getElementById('ctl00_ContentPlaceHolder1_hdnQualityIssueCharges').value;            
             document.getElementById("hdnChargeValue").value = txtQualityIssueCharges;

             if (txtQualityIssueCharges == '')
             {
                 $('#rdoINV008RewordAndCharge').css('display', 'none');
                 $('label[For= "rdoINV008RewordAndCharge"]').css('display', 'none');
             }
         }
    </script>
</head>
<body onload="GetChargeValue();">
    <form id="form1" runat="server">
         <asp:HiddenField ID="hdnChargeValue" runat="server" />
    <div>
        <cc1:ucPanel ID="pnlActionRequired" runat="server" Width="100%" GroupingText="Action Required"
            CssClass="fieldset-form" BorderColor="gray" BorderStyle="Solid" BorderWidth="0">
            <table class="form-table" cellpadding="0" cellspacing="0" width="98%">
                <tr id="trWrongPackSize" runat="server">
                    <td><asp:HiddenField ID="hdfLocation" runat="server" />
                        <cc1:ucLabel ID="lblINV008Desc" CssClass="action-required-heading" runat="server"
                            Style="text-align: center;" Text="Product has been delivered in a pack size different to the stated pack quantity on the Purchase Order.<br/>Please confirm if goods are to be kept or returned to the vendor "></cc1:ucLabel>
                    </td>
                </tr>
                <tr id="trQualityIssue" runat="server" style="display: none">
                    <td>
                        <cc1:ucLabel ID="lblQualityIssue" CssClass="action-required-heading" runat="server"
                            Style="text-align: center;" Text="A Quality Issue has been identified. Please confirm action required."></cc1:ucLabel>
                    </td>
                </tr>
                <tr id="trQualityIssueOnVendorRefuseCharge" runat="server" style="display: none">
                    <td>                        
                        <cc1:ucLabel ID="lblQualityIssueOnVendorRefuseCharge" CssClass="action-required-heading" runat="server"
                            Style="text-align: center;" Text="Vendor has disagreed with the charge for this issue. Please review once again."></cc1:ucLabel>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table class="form-table" cellpadding="0" cellspacing="0" width="98%" style="margin: 2px;">
                            <tr>
                                <td width="20%">
                                    <cc1:ucRadioButton ID="rdoINV008GoodsReturnToVendor" runat="server" GroupName="GoodsOption"
                                        Text="Return Goods to Vendor" />
                                </td>
                                <td width="20%">
                                    <cc1:ucRadioButton ID="rdoINV008ReworknotRequired" runat="server" GroupName="GoodsOption"
                                        Text="Rework not required" />
                                </td>
                                <td width="20%">
                                    <cc1:ucRadioButton ID="rdoINV008ReworkNoCharge" runat="server" GroupName="GoodsOption"
                                        Text="Rework,no charge" />
                                </td>
                                <td width="40%">
                                    <cc1:ucRadioButton ID="rdoINV008RewordAndCharge" runat="server" GroupName="GoodsOption"
                                        Text="Rework and charge" />
                                    <asp:CustomValidator ID="cusvPleaseTakeAnAction" runat="server" Text="Please take an action."
                                        ClientValidationFunction="PleaseTakeAnAction" Display="None" ValidationGroup="a">
                                    </asp:CustomValidator>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table class="form-table" cellpadding="0" cellspacing="0" width="98%" style="margin: 2px;">
                            <tr>
                                <td width="7">
                                    <cc1:ucLabel ID="lblCommentLabel" runat="server" Text="Comment"></cc1:ucLabel>
                                </td>
                                <td width="93%">
                                    :
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <cc1:ucTextbox ID="txtComments" runat="server" onkeyup="checkTextLengthOnKeyUp(this,1000);"
                                        TextMode="MultiLine" CssClass="textarea" Width="98%"></cc1:ucTextbox>
                                    <asp:RequiredFieldValidator ID="rfvCommentRequired" runat="server" Text="Please enter comments."
                                        ControlToValidate="txtComments" Display="None" ValidationGroup="a">
                                    </asp:RequiredFieldValidator>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table class="form-table" cellpadding="0" cellspacing="0" width="98%" style="margin: 2px;">
                            <tr>
                                <td width="14%" style="display:none;">
                                    <cc1:ucLabel ID="lblStageCompleted" runat="server" Text="Stage Completed"></cc1:ucLabel>
                                    <asp:ValidationSummary ID="vs1" runat="server" ValidationGroup="a" ShowSummary="false"
                                        ShowMessageBox="true" />
                                </td>
                                <td width="1%" style="display:none;">
                                    :
                                </td>
                                <td width="75%" align="left" style="display:none;">
                                    <cc1:ucCheckbox ID="chkINV008StageCompleted" runat="server" Checked="true" />
                                </td>
                                <td width="10%" style="display:none;">
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3" style="display:none;">
                                    <cc1:ucLabel ID="lblDiscrepancyNote" runat="server" Text="If  you required to tag a comment without closing the discrepancy<br/>1 -    Deselect Stage Complete<br/>2 -   Enter  the comment<br/>3 -   Save"></cc1:ucLabel>
                                </td>
                                <td align="right" valign="bottom">
                                    <cc1:ucButton ID="btnSave" runat="server" Text="Save" class="button" ValidationGroup="a"
                                        OnClick="btnSave_Click"  OnClientClick="javascript:GetLocation()" />
                                    <cc1:ucButton ID="btnBack" runat="server" Text="Back" class="button" OnClick="btnBack_Click" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </cc1:ucPanel>
    </div>
    </form>
</body>
</html>

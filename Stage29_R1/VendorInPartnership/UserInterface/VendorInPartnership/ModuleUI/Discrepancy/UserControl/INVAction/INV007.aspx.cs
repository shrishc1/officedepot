﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using BusinessEntities.ModuleBE.Discrepancy.LogDiscrepancy;
using BusinessLogicLayer.ModuleBAL.Discrepancy.LogDiscrepancy;
using WebUtilities;

public partial class INVAction_INV007 : CommonPage {
    #region Global variables
    #endregion

    #region Events

    protected void Page_Load(object sender, EventArgs e) {
        if (!IsPostBack) {
            GetCarriageCost();
        }
    }
    protected void btnSave_Click(object sender, EventArgs e) {

        bool IsCarriageCostRequiredChecked = clsDiscrepancy.ISRadioButtonChecked(rdoINV007Charge, rdoINV007NoCharge);
        if (IsCarriageCostRequiredChecked == false)
        {
            string CarriageCostRequiredMessage = WebCommon.getGlobalResourceValue("CarriageCostRequired");
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('" + CarriageCostRequiredMessage + "')", true);
            return;
        }


        ScriptManager.RegisterStartupScript(this, this.GetType(), "HideButton", "HideButton();", true);
        btnSave.Enabled = false;
        string sentToLinks = string.Empty;
        DISLog_WorkFlowBE oDISLog_WorkFlowBE = new DISLog_WorkFlowBE();
        DISLog_WorkFlowBAL oDISLog_WorkFlowBAL = new DISLog_WorkFlowBAL();
        
        if (GetQueryStringValue("disLogID") != null && GetQueryStringValue("disLogID").ToString() != "") 
        {
            //**********************************************************
            string Location = hdfLocation.Value;
            DiscrepancyBE oDiscrepancyBE1 = new DiscrepancyBE();
            DiscrepancyBAL oDiscrepancyBAL1 = new DiscrepancyBAL();
            oDiscrepancyBE1.Action = "UpdateLocation";
            oDiscrepancyBE1.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
            oDiscrepancyBE1.Location = Location;
            oDiscrepancyBE1.UserID = Convert.ToInt32(Session["UserID"]);
            string sResult1 = oDiscrepancyBAL1.updateDiscrepancyLocationBAL(oDiscrepancyBE1);
            //**********************************************************
            oDISLog_WorkFlowBE.Action = "InsertUpdateWorkflowActions";
            oDISLog_WorkFlowBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
            oDISLog_WorkFlowBE.StageCompletedForWhom = "INV007_StageCompleted";
            oDISLog_WorkFlowBE.Comment = txtComments.Text.Trim();
            
            if (rdoINV007Charge.Checked) {
                oDISLog_WorkFlowBE.INV007_CarriageCost = Convert.ToDecimal(txtINV007CarriageCost.Text.Trim());
                oDISLog_WorkFlowBE.INV007_IsCharged = "C"; // is being charged.
                oDISLog_WorkFlowBE.Decision = "Charged";
            }
            else {
                oDISLog_WorkFlowBE.INV007_CarriageCost = Convert.ToDecimal("0.00");
                oDISLog_WorkFlowBE.INV007_IsCharged = "N"; // not charged.
                oDISLog_WorkFlowBE.Decision = "Not charged";
            }
            oDISLog_WorkFlowBE.StageCompleted = chkINV007StageCompleted.Checked ? true : false;
            //update the work flow action table each time for the comments and other fields( if stage is completed)
            oDISLog_WorkFlowBE.DiscrepancyWorkFlowID = Convert.ToInt32(GetQueryStringValue("DisWFlowID").ToString());

            //insert or update the record according to the stage completed status
            int? iResult = oDISLog_WorkFlowBAL.addWorkFlowActionsBAL(oDISLog_WorkFlowBE);

            //now insert the work flow HTML according to the action taken only if the stage is completed
            DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
            DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();
            WorkflowHTML oWorkflowHTML = new WorkflowHTML();

            if (chkINV007StageCompleted.Checked) {
                
                if (rdoINV007Charge.Checked) {

                    sentToLinks = SendMailAndGetMailLinks(Convert.ToInt32(GetQueryStringValue("disLogID")), "incorrectdeliveryaddress", "Communication1", "Communication2");

                    oDiscrepancyBE.GINHTML = oWorkflowHTML.function3("White", null, "", "");
                    oDiscrepancyBE.GINActionRequired = false;

                    oDiscrepancyBE.INVHTML = oWorkflowHTML.function1(DateTime.Now, null, null, "Yellow", "", "Charge", 
                        //Session["UserName"].ToString(), 
                        UIUtility.GetODVendorUserName(Convert.ToString(Session["Role"]), Convert.ToString(Session["UserName"]), Convert.ToString(Session["LoginID"])),
                        txtComments.Text.Trim(), "", "", "", "", txtINV007CarriageCost.Text);
                    oDiscrepancyBE.INVActionRequired = false;

                    oDiscrepancyBE.APHTML = oWorkflowHTML.function4("Blue", "", null, "", "", "Build on to Accounts Payable worklist on " + DateTime.Now.ToString("dd/MM/yyyy") + " with the following request",
                                                                     @"Please credit/debit the account for the amount to cover carriage costs");
                    oDiscrepancyBE.APActionRequired = true;
                    oDiscrepancyBE.APUserControl = "ACP002";

                    oDiscrepancyBE.VENHTML = oWorkflowHTML.function4("Blue", "", DateTime.Now, sentToLinks,
                                                                    "Admin@OfficeDepot.com",
                                                                    @"Goods have been delivered into a  wrong site, 
                                                                    Office Depot have shipped these on to the correct 
                                                                    location. Please advise of remedial action");
                    oDiscrepancyBE.VenActionRequired = true;
                    oDiscrepancyBE.VENUserControl = "VEN015";

                    oDiscrepancyBE.Action = "InsertHTML";
                    oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString()); ;
                    oDiscrepancyBE.LoggedDateTime = DateTime.Now;
                    oDiscrepancyBE.LevelNumber = 1;
                    oDiscrepancyBE.VendorActionCategory = "RA";
                    oDiscrepancyBE.ActionTakenBy = "INV";
                    oDiscrepancyBE.APActionCategory = "AR";  //Action Required
                    int? iResult1 = oDiscrepancyBAL.addWorkFlowHTMLsBAL(oDiscrepancyBE);
                }
                else if (rdoINV007NoCharge.Checked) {
                    
                    sentToLinks = SendMailAndGetMailLinks(Convert.ToInt32(GetQueryStringValue("disLogID")), "incorrectdeliveryaddress", "Communication1", "Communication3");

                    oDiscrepancyBE.GINHTML = oWorkflowHTML.function3("White", null, "", "");
                    oDiscrepancyBE.GINActionRequired = false;

                    oDiscrepancyBE.INVHTML = oWorkflowHTML.function1(DateTime.Now, null, null, "Yellow", "", "No Charge", 
                        //Session["UserName"].ToString(), 
                        UIUtility.GetODVendorUserName(Convert.ToString(Session["Role"]), Convert.ToString(Session["UserName"]), Convert.ToString(Session["LoginID"])),
                        txtComments.Text.Trim());
                    oDiscrepancyBE.INVActionRequired = false;

                    oDiscrepancyBE.APHTML = oWorkflowHTML.function3("White", null, "", "");
                    oDiscrepancyBE.APActionRequired = false;

                    oDiscrepancyBE.VENHTML = oWorkflowHTML.function4("Blue", "", DateTime.Now, sentToLinks,
                                                                    "Admin@OfficeDepot.com",
                                                                    @"Goods have been delivered into a  wrong site, 
                                                                    Office Depot have shipped these on to the correct 
                                                                    location. Please advise of remedial action");
                    oDiscrepancyBE.VenActionRequired = true;
                    oDiscrepancyBE.VENUserControl = "VEN015";

                    oDiscrepancyBE.Action = "InsertHTML";
                    oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString()); ;
                    oDiscrepancyBE.LoggedDateTime = DateTime.Now;
                    oDiscrepancyBE.LevelNumber = 1;
                    oDiscrepancyBE.VendorActionCategory = "RA";
                    oDiscrepancyBE.ActionTakenBy = "INV";
                    int? iResult1 = oDiscrepancyBAL.addWorkFlowHTMLsBAL(oDiscrepancyBE);
                }
            }
            else {
                InsertHTML();
            }
        }

        //Redirecting to Previous Page
        RedirectToPage(GetQueryStringValue("PreviousPage"));
    }

    #endregion

    #region Methods

    private string SendMailAndGetMailLinks(int DiscrepancyLogID, string discrepancyType, string sCurrentCommunicationLevel, string sNextCommunicationLevel)
    {
        sendCommunicationAllLevel osendCommunicationAllLevel = new sendCommunicationAllLevel();
        return osendCommunicationAllLevel.SendMailAndGetMailLinks(DiscrepancyLogID, discrepancyType, sCurrentCommunicationLevel, sNextCommunicationLevel);
    }

    private void GetCarriageCost() {
        if (GetQueryStringValue("disLogID") != null) {

            DiscrepancyBE oNewDiscrepancyBE = new DiscrepancyBE();
            DiscrepancyBAL oNewDiscrepancyBAL = new DiscrepancyBAL();
            oNewDiscrepancyBE.Action = "GetDiscrepancyLog";
            oNewDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
            List<DiscrepancyBE> lstDisLog = oNewDiscrepancyBAL.GetDiscrepancyLogDetailsBAL(oNewDiscrepancyBE);
            if (lstDisLog != null && lstDisLog.Count > 0) {
                txtINV007CarriageCost.Text = lstDisLog[0].Freight_Charges.ToString();
                txtINV007CarriageCost.ReadOnly = true;
            }

        }
    }

    private void InsertHTML() {

        DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
        DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();

        WorkflowHTML oWorkflowHTML = new WorkflowHTML();
        oDiscrepancyBE.INVHTML = oWorkflowHTML.function3("White", DateTime.Now, txtComments.Text, "");
        oDiscrepancyBE.INVActionRequired = false;
        
        oDiscrepancyBE.Action = "InsertHTML";
        oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
        oDiscrepancyBE.LoggedDateTime = DateTime.Now;
        oDiscrepancyBE.LevelNumber = 1;
        int? iResult = oDiscrepancyBAL.addWorkFlowHTMLsBAL(oDiscrepancyBE);
    }

    #endregion    

    protected void btnBack_Click(object sender, EventArgs e)
    {
        //Redirecting to Previous Page
        RedirectToPage(GetQueryStringValue("PreviousPage"));
    }
}
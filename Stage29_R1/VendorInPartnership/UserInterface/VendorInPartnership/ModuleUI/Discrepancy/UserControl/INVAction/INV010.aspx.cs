﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using BusinessEntities.ModuleBE.Discrepancy.LogDiscrepancy;
using BusinessLogicLayer.ModuleBAL.Discrepancy.LogDiscrepancy;
using WebUtilities;

public partial class INVAction_INV010 : CommonPage {
    #region Global variables
    #endregion

    #region Events
    protected void Page_Load(object sender, EventArgs e) {
        if (!IsPostBack) {
            GetLabourCost();
        }
    }
    
    protected void btnSave_Click(object sender, EventArgs e) {

        bool IsLabourCostRequiredChecked = clsDiscrepancy.ISRadioButtonChecked(rdoINV010Charge, rdoINV010NoCharge);
        if (IsLabourCostRequiredChecked == false)
        {
            string LabourCostRequiredMessage = WebCommon.getGlobalResourceValue("LabourCostRequired");
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('" + LabourCostRequiredMessage + "')", true);
            return;
        }


        ScriptManager.RegisterStartupScript(this, this.GetType(), "HideButton", "HideButton();", true);
        btnSave.Enabled = false;
        sendCommunicationAllLevel osendCommunicationAllLevel = new sendCommunicationAllLevel();
        string sentToLinks = string.Empty;
        DISLog_WorkFlowBE oDISLog_WorkFlowBE = new DISLog_WorkFlowBE();
        DISLog_WorkFlowBAL oDISLog_WorkFlowBAL = new DISLog_WorkFlowBAL();

        if (GetQueryStringValue("disLogID") != null && GetQueryStringValue("disLogID").ToString() != "")
        {
            //**********************************************************
            string Location = hdfLocation.Value;
            DiscrepancyBE oDiscrepancyBE1 = new DiscrepancyBE();
            DiscrepancyBAL oDiscrepancyBAL1 = new DiscrepancyBAL();
            oDiscrepancyBE1.Action = "UpdateLocation";
            oDiscrepancyBE1.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
            oDiscrepancyBE1.Location = Location;
            oDiscrepancyBE1.UserID = Convert.ToInt32(Session["UserID"]);
            string sResult1 = oDiscrepancyBAL1.updateDiscrepancyLocationBAL(oDiscrepancyBE1);
            //**********************************************************
            oDISLog_WorkFlowBE.Action = "InsertUpdateWorkflowActions";
            oDISLog_WorkFlowBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
            oDISLog_WorkFlowBE.StageCompletedForWhom = "INV010_StageCompleted";
            oDISLog_WorkFlowBE.Comment = txtComments.Text.Trim();

            if (rdoINV010Charge.Checked) {
                oDISLog_WorkFlowBE.INV010_LabourCost = Convert.ToDecimal(txtINV010LabourCost.Text.Trim());
                oDISLog_WorkFlowBE.INV010_IsCharged = "C"; // is being charged.
                oDISLog_WorkFlowBE.Decision = "Charged";
            }
            else {
                oDISLog_WorkFlowBE.INV010_LabourCost = Convert.ToDecimal("0.00");
                oDISLog_WorkFlowBE.INV010_IsCharged = "N"; // not charged.
                oDISLog_WorkFlowBE.Decision = "Not charged";
            }
            oDISLog_WorkFlowBE.StageCompleted = chkINV010StageCompleted.Checked ? true : false;

            //update the work flow action table each time for the comments and other fields( if stage is completed)
            oDISLog_WorkFlowBE.DiscrepancyWorkFlowID = Convert.ToInt32(GetQueryStringValue("DisWFlowID").ToString());

            //insert or update the record according to the stage completed status
            int? iResult = oDISLog_WorkFlowBAL.addWorkFlowActionsBAL(oDISLog_WorkFlowBE);

            //now insert the work flow HTML according to the action taken only if the stage is completed
            DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
            DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();
            WorkflowHTML oWorkflowHTML = new WorkflowHTML();

            if (chkINV010StageCompleted.Checked) {

                if (rdoINV010Charge.Checked) {

                    sentToLinks = osendCommunicationAllLevel.SendMailAndGetMailLinks(Convert.ToInt32(GetQueryStringValue("disLogID")), "prematureinvoicereceipt", "Communication1", "Communication2");

                    oDiscrepancyBE.GINHTML = oWorkflowHTML.function3("White", null, "", "");
                    oDiscrepancyBE.GINActionRequired = false;

                    oDiscrepancyBE.INVHTML = oWorkflowHTML.function1(DateTime.Now, null, null, "Yellow", "", "Charge created", 
                        //Session["UserName"].ToString(),
                        UIUtility.GetODVendorUserName(Convert.ToString(Session["Role"]), Convert.ToString(Session["UserName"]), Convert.ToString(Session["LoginID"])),
                        txtComments.Text.Trim());
                    oDiscrepancyBE.INVActionRequired = false;

                    oDISLog_WorkFlowBE.Action = "GetAccountPayable";
                    string VendorAccountPayablesEmail = oDISLog_WorkFlowBAL.GetVendorAccountPayablesEmail(oDISLog_WorkFlowBE);

                    oDiscrepancyBE.APHTML = oWorkflowHTML.function4("Blue", "", null,VendorAccountPayablesEmail, "", "Build on to Accounts Payable worklist on " + DateTime.Now.ToString("dd/MM/yyyy") + " with the following request",
                                                                     @"Please credit/debit the account for the amount to cover labour costs", "", "", DateTime.Now,txtINV010LabourCost.Text);
                    oDiscrepancyBE.APActionRequired = true;
                    oDiscrepancyBE.APUserControl = "ACP003";

                    oDiscrepancyBE.VENHTML = oWorkflowHTML.function4("Blue", "", DateTime.Now, sentToLinks,
                                                                    "Admin@OfficeDepot.com",
                                                                    @"Follow up communication sent and discrepancy builds onto Vendor Work List with following request :-
                                                                    There has been a premature invoice before goods have been delivered. Please advise of remedial action");
                    oDiscrepancyBE.VenActionRequired = true;
                    oDiscrepancyBE.VENUserControl = "VEN022";

                    oDiscrepancyBE.Action = "InsertHTML";
                    oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString()); ;
                    oDiscrepancyBE.LoggedDateTime = DateTime.Now;
                    oDiscrepancyBE.LevelNumber = 1;
                    oDiscrepancyBE.VendorActionCategory = "RA";
                    oDiscrepancyBE.ActionTakenBy = "INV";
                    oDiscrepancyBE.APActionCategory = "AR";  //Action Required
                    int? iResult1 = oDiscrepancyBAL.addWorkFlowHTMLsBAL(oDiscrepancyBE);

                }
                else{
                    
                    sentToLinks = osendCommunicationAllLevel.SendMailAndGetMailLinks(Convert.ToInt32(GetQueryStringValue("disLogID")), "prematureinvoicereceipt", "Communication1", "Communication3");

                    oDiscrepancyBE.GINHTML = oWorkflowHTML.function3("White", null, "", "");
                    oDiscrepancyBE.GINActionRequired = false;

                    oDiscrepancyBE.INVHTML = oWorkflowHTML.function1(DateTime.Now, null, null, "Yellow", "", "No charge", 
                        //Session["UserName"].ToString(), 
                        UIUtility.GetODVendorUserName(Convert.ToString(Session["Role"]), Convert.ToString(Session["UserName"]), Convert.ToString(Session["LoginID"])),
                        txtComments.Text.Trim());
                    oDiscrepancyBE.INVActionRequired = false;

                    oDiscrepancyBE.APHTML = oWorkflowHTML.function3("White", null, "", "");
                    oDiscrepancyBE.APActionRequired = false;

                    oDiscrepancyBE.VENHTML = oWorkflowHTML.function4("Blue", "", DateTime.Now, sentToLinks,
                                                                    "Admin@OfficeDepot.com",
                                                                    @"Follow up communication sent and discrepancy builds onto Vendor Work List with following request :-
                                                                    There has been a premature invoice before goods have been delivered. Please advise of remedial action");
                    oDiscrepancyBE.VenActionRequired = true;
                    oDiscrepancyBE.VENUserControl = "VEN022";

                    oDiscrepancyBE.Action = "InsertHTML";
                    oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString()); ;
                    oDiscrepancyBE.LoggedDateTime = DateTime.Now;
                    oDiscrepancyBE.LevelNumber = 1;
                    oDiscrepancyBE.VendorActionCategory = "RA";
                    oDiscrepancyBE.ActionTakenBy = "INV";
                    int? iResult1 = oDiscrepancyBAL.addWorkFlowHTMLsBAL(oDiscrepancyBE);
                }                
            }
            else {
                InsertHTML();
            }
        }


        //Redirecting to Previous Page
        RedirectToPage(GetQueryStringValue("PreviousPage"));
    }

    #endregion

    #region Methods

    private void GetLabourCost() {
        if (GetQueryStringValue("disLogID") != null) {

            DiscrepancyBE oNewDiscrepancyBE = new DiscrepancyBE();
            DiscrepancyBAL oNewDiscrepancyBAL = new DiscrepancyBAL();
            oNewDiscrepancyBE.Action = "GetDiscrepancyLog";
            oNewDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
            List<DiscrepancyBE> lstDisLog = oNewDiscrepancyBAL.GetDiscrepancyLogDetailsBAL(oNewDiscrepancyBE);
            if (lstDisLog != null && lstDisLog.Count > 0) {
                txtINV010LabourCost.Text = lstDisLog[0].Freight_Charges.ToString();
                txtINV010LabourCost.ReadOnly = true;
            }
        }
    }
    private void InsertHTML() {

        DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
        DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();

        WorkflowHTML oWorkflowHTML = new WorkflowHTML();
        oDiscrepancyBE.INVHTML = oWorkflowHTML.function3("White", DateTime.Now, txtComments.Text, "");
        oDiscrepancyBE.INVActionRequired = false;

        oDiscrepancyBE.Action = "InsertHTML";
        oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
        oDiscrepancyBE.LoggedDateTime = DateTime.Now;
        oDiscrepancyBE.LevelNumber = 1;
        int? iResult = oDiscrepancyBAL.addWorkFlowHTMLsBAL(oDiscrepancyBE);

    }
    #endregion

    protected void btnBack_Click(object sender, EventArgs e)
    {
        //Redirecting to Previous Page
        RedirectToPage(GetQueryStringValue("PreviousPage"));
    }
}
﻿using System;
using System.Web.UI;
using BusinessEntities.ModuleBE.Discrepancy.LogDiscrepancy;
using BusinessLogicLayer.ModuleBAL.Discrepancy.LogDiscrepancy;
using WebUtilities;


public partial class INVAction_INV006 : CommonPage {

    #region Events

    protected void Page_Load(object sender, EventArgs e) {
        if (!IsPostBack) {
            chkINV006aStageCompleted.Checked = true;
            rfvINV006aPurchaseOrderRequired.Enabled = false;
            //rdoINV006aGoodsKept.Checked = true;
        }
    }

    protected void btnSave_Click(object sender, EventArgs e) {

        bool IsINV006ActionRequiredChecked = clsDiscrepancy.ISRadioButtonChecked(rdoINV006aGoodsKept, rdoINV006aGoodsKeptSub, rdoINV006aGoodsReturned, rdoINV006aUnexpectedItem);
        if (IsINV006ActionRequiredChecked == false)
        {
            string INV006ActionRequiredMessage = WebCommon.getGlobalResourceValue("INV006ActionRequired");
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('" + INV006ActionRequiredMessage + "')", true);
            return;
        }
                
        /*
         * if goods to be kept or goods to be kept NAX order then go to 2a
         * else if goods to be returned go to 2b
         */
        ScriptManager.RegisterStartupScript(this, this.GetType(), "HideButton", "HideButton();", true);
        btnSave.Enabled = false;
        string sentToLinks = string.Empty;
        DISLog_WorkFlowBE oDISLog_WorkFlowBE = new DISLog_WorkFlowBE();
        DISLog_WorkFlowBAL oDISLog_WorkFlowBAL = new DISLog_WorkFlowBAL();

        if (GetQueryStringValue("disLogID") != null && GetQueryStringValue("disLogID").ToString() != "")
        {
            //**********************************************************
            string Location = hdfLocation.Value;
            DiscrepancyBE oDiscrepancyBE1 = new DiscrepancyBE();
            DiscrepancyBAL oDiscrepancyBAL1 = new DiscrepancyBAL();
            oDiscrepancyBE1.Action = "UpdateLocation";
            oDiscrepancyBE1.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
            oDiscrepancyBE1.Location = Location;
            oDiscrepancyBE1.UserID = Convert.ToInt32(Session["UserID"]);
            string sResult1 = oDiscrepancyBAL1.updateDiscrepancyLocationBAL(oDiscrepancyBE1);
            //**********************************************************
            oDISLog_WorkFlowBE.Action = "InsertUpdateWorkflowActions";
            oDISLog_WorkFlowBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());

            if (rdoINV006aGoodsKept.Checked) {
                oDISLog_WorkFlowBE.INV006_GoodsDecision = 'C'; //Goods to be kept
                oDISLog_WorkFlowBE.VenCommunicationType = "Kept";
                oDISLog_WorkFlowBE.Decision = "Goods to be kept";
            }
            else if (rdoINV006aGoodsKeptSub.Checked)
            {
                oDISLog_WorkFlowBE.INV006_GoodsDecision = 'S'; //Goods to be kept rder
                oDISLog_WorkFlowBE.VenCommunicationType = "Kept";
                oDISLog_WorkFlowBE.Decision = "Goods to be kept order";
            }
            else if (rdoINV006aGoodsReturned.Checked) {
                oDISLog_WorkFlowBE.INV006_GoodsDecision = 'R'; //Goods to be returned
                oDISLog_WorkFlowBE.VenCommunicationType = "Return";
                oDISLog_WorkFlowBE.Decision = "Goods to be returned";
            }
            else if (rdoINV006aUnexpectedItem.Checked)
            {
                oDISLog_WorkFlowBE.INV006_GoodsDecision = 'U'; //Unexpected item
                oDISLog_WorkFlowBE.VenCommunicationType = "Kept";
                oDISLog_WorkFlowBE.PurchaseOrderNumber = txtINV006aPurchaseOrder.Text.Trim();
                oDISLog_WorkFlowBE.Decision = "Unexpected item";
            }
            oDISLog_WorkFlowBE.StageCompletedForWhom = "INV006_StageCompleted";

            if (chkINV006aStageCompleted.Checked) {
                oDISLog_WorkFlowBE.StageCompleted = true;
            }
            else {
                oDISLog_WorkFlowBE.StageCompleted = false;
            }
            oDISLog_WorkFlowBE.Comment = txtINV006aComment.Text;

            //update the work flow action table each time for the comments and other fields( if stage is completed)
            oDISLog_WorkFlowBE.DiscrepancyWorkFlowID = Convert.ToInt32(GetQueryStringValue("DisWFlowID").ToString());

            //insert or update the record according to the stage completed status
            int? iResult = oDISLog_WorkFlowBAL.addWorkFlowActionsBAL(oDISLog_WorkFlowBE);

            //now insert the work flow HTML according to the action taken only if the stage is completed
            DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
            DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();

            if (GetQueryStringValue("FromPage") != null && GetQueryStringValue("FromPage").ToString().Trim() == "IncorrectProduct") {

                if (chkINV006aStageCompleted.Checked) {

                    WorkflowHTML oWorkflowHTML = new WorkflowHTML();
                    string sAction = oDISLog_WorkFlowBE.INV006_GoodsDecision == 'C' ? "Goods correct, Book In"
                                    : (oDISLog_WorkFlowBE.INV006_GoodsDecision == 'S' ? "Goods correct Sub, Book In"
                                    : (oDISLog_WorkFlowBE.INV006_GoodsDecision == 'R' ? "Goods to be returned to Vendor"
                                    : "Unexpected Item, Book in NAX"));

                    //if goods to be kept or goods to be kept NAX order then go to 2b
                    if (rdoINV006aGoodsKept.Checked || rdoINV006aGoodsKeptSub.Checked) {

                        sentToLinks = SendMailAndGetMailLinks(Convert.ToInt32(GetQueryStringValue("disLogID")), "incorrectproductcode", "Communication1", "Communication3");

                        oDiscrepancyBE.GINHTML = oWorkflowHTML.function2(DateTime.Now, null, "Blue", "Goods-In",
                                                                             "Build on to Goods In Worklist on "
                                                                                + DateTime.Now.ToString("dd/MM/yyyy") + " with the following request",
                                                                             @"Items are correct, please confirm the 
                                                                         goods are booked in against original Purchase Order");
                        oDiscrepancyBE.GINActionRequired = true;
                        oDiscrepancyBE.GINUserControl = "GIN006";


                        /* Changed as per the Issue raised by client for point 132 on [Issues Log _20130710] issue log on Abhinav's systems. */
                        //oDiscrepancyBE.INVHTML = oWorkflowHTML.function1(DateTime.Now, null, null, "Yellow", "", "Incorrect Delivery Address Discrepancy", Session["UserName"].ToString(), txtINV006aComment.Text);
                        oDiscrepancyBE.INVHTML = oWorkflowHTML.function1(DateTime.Now, null, null, "Yellow", "", "Correct Item/Correct Item Sub", 
                            //Session["UserName"].ToString(), 
                            UIUtility.GetODVendorUserName(Convert.ToString(Session["Role"]), Convert.ToString(Session["UserName"]), Convert.ToString(Session["LoginID"])),
                            txtINV006aComment.Text);
                        oDiscrepancyBE.INVActionRequired = false;

                        oDiscrepancyBE.APHTML = oWorkflowHTML.function3("White", null, "", "");
                        oDiscrepancyBE.APActionRequired = false;

                        oDiscrepancyBE.VENHTML = oWorkflowHTML.function4("Blue", "", null, sentToLinks, "Admin@OfficeDepot.com",
                                                                         "", @"As per the original discrepancy note, products received did 
                                                                           not match the Purchase Order detail. Please advise of remedial action"
                                                                         , "", "", DateTime.Now);
                        oDiscrepancyBE.VenActionRequired = true;
                        oDiscrepancyBE.VENUserControl = "VEN013";

                        oDiscrepancyBE.Action = "InsertHTML";
                        oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
                        oDiscrepancyBE.LoggedDateTime = DateTime.Now;
                        oDiscrepancyBE.LevelNumber = 1;
                        oDiscrepancyBE.VendorActionCategory = "RA";
                        oDiscrepancyBE.ActionTakenBy = "INV";
                        oDiscrepancyBE.GINActionCategory = "RA";  //Action Required
                        int? iResult1 = oDiscrepancyBAL.addWorkFlowHTMLsBAL(oDiscrepancyBE);
                    }
                    else if (rdoINV006aGoodsReturned.Checked) {
                        //if goods to be returned go to 2b
                        sentToLinks = SendMailAndGetMailLinks(Convert.ToInt32(GetQueryStringValue("disLogID")), "incorrectproductcode", "Communication1", "Communication2");
                        
                        oDiscrepancyBE.GINHTML = oWorkflowHTML.function3("White", null, "", "");
                        oDiscrepancyBE.GINActionRequired = false;
                        oDiscrepancyBE.INVHTML = oWorkflowHTML.function1(DateTime.Now, null, null, "Yellow", "", sAction, 
                            //Session["UserName"].ToString(), 
                            UIUtility.GetODVendorUserName(Convert.ToString(Session["Role"]), Convert.ToString(Session["UserName"]), Convert.ToString(Session["LoginID"])),
                            txtINV006aComment.Text.Trim());
                        oDiscrepancyBE.INVActionRequired = false;

                        oDISLog_WorkFlowBE.Action = "GetAccountPayable";
                        string VendorAccountPayablesEmail = oDISLog_WorkFlowBAL.GetVendorAccountPayablesEmail(oDISLog_WorkFlowBE);
//                        oDiscrepancyBE.APHTML = oWorkflowHTML.function2(DateTime.Now, null, "Blue", VendorAccountPayablesEmail,
//                                                                            @"Build on to Accounts Payable Work list with following request",
//                                                                            @"A discrepancy has been raised which requires
//                                                                        for debit to be raised or credit to be issued.
//                                                                         Please action accordingly upon receipt of invoice.");

                        oDiscrepancyBE.APHTML = oWorkflowHTML.function8("green",
                                                                      @"A Discrepancy has been raised which may require a debit to be raised or credit to be issued. Please actions accordingly upon receipt of invoice.");
                        oDiscrepancyBE.APActionRequired = false; //true;
                        //oDiscrepancyBE.APUserControl = "ACP001";

                        oDiscrepancyBE.VENHTML = oWorkflowHTML.function4("Blue", "", null, sentToLinks, "Admin@OfficeDepot.com",
                                                                         "", @"An incorrect product has been delivered and is to be returned. 
                                                                     Please select method of return and advise of remedial action",
                                                                         "", "", DateTime.Now);
                        oDiscrepancyBE.VenActionRequired = true;
                        oDiscrepancyBE.VENUserControl = "VEN011";
                        oDiscrepancyBE.Action = "InsertHTML";
                        oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
                        oDiscrepancyBE.LoggedDateTime = DateTime.Now;
                        oDiscrepancyBE.LevelNumber = 1;
                        oDiscrepancyBE.ACP001Control = "ACP001";
                        oDiscrepancyBE.IsACP001Done = false;
                        oDiscrepancyBE.VendorActionCategory = "AR";
                        oDiscrepancyBE.ActionTakenBy = "INV";
                        int? iResult1 = oDiscrepancyBAL.addWorkFlowHTMLsBAL(oDiscrepancyBE);
                    }
                    else if (rdoINV006aUnexpectedItem.Checked) {
                        // unexpected item 
                        sentToLinks = SendMailAndGetMailLinks(Convert.ToInt32(GetQueryStringValue("disLogID")), "incorrectproductcode", "Communication1", "Communication3");

                        oDiscrepancyBE.GINHTML = oWorkflowHTML.function2(DateTime.Now, null, "Blue", "Goods-In",
                                                                             "Build on to Goods In Worklist on "
                                                                                + DateTime.Now.ToString("dd/MM/yyyy") + " with the following request",
                                                                             @"Unexpected item has been delivered. Please confirm booking in against Purchase Order " + txtINV006aPurchaseOrder.Text.Trim());
                        oDiscrepancyBE.GINActionRequired = true;
                        oDiscrepancyBE.GINUserControl = "GIN007";

                        oDiscrepancyBE.INVHTML = oWorkflowHTML.function1(DateTime.Now, null, null, "Yellow", "", sAction,
                                                            //Session["UserName"].ToString(), 
                                                            UIUtility.GetODVendorUserName(Convert.ToString(Session["Role"]), Convert.ToString(Session["UserName"]), Convert.ToString(Session["LoginID"])),
                                                            txtINV006aComment.Text,
                                                            txtINV006aPurchaseOrder.Text.Trim());
                        oDiscrepancyBE.INVActionRequired = false;
                        
                        oDiscrepancyBE.APHTML = oWorkflowHTML.function3("White", null, "", "");
                        oDiscrepancyBE.APActionRequired = false;

                        oDiscrepancyBE.VENHTML = oWorkflowHTML.function4("Blue", "", null, sentToLinks, "Admin@OfficeDepot.com",
                                                                         "", @"An incorrect product has been received in by Office Depot and kept on this 
                                                                            occasion. Please advise of remedial action to ensure this does not happen in the future"
                                                                         , "", "", DateTime.Now);
                        oDiscrepancyBE.VenActionRequired = true;
                        oDiscrepancyBE.VENUserControl = "VEN012";

                        oDiscrepancyBE.Action = "InsertHTML";
                        oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
                        oDiscrepancyBE.LoggedDateTime = DateTime.Now;
                        oDiscrepancyBE.LevelNumber = 1;
                        oDiscrepancyBE.VendorActionCategory = "RA";
                        oDiscrepancyBE.ActionTakenBy = "INV";
                        oDiscrepancyBE.GINActionCategory = "RA";  //Action Required
                        int? iResult1 = oDiscrepancyBAL.addWorkFlowHTMLsBAL(oDiscrepancyBE);
                    }
                }
                else {
                    /*Enter comments with date*/
                    InsertHTML();
                }
            }


            //Redirecting to Previous Page
            RedirectToPage(GetQueryStringValue("PreviousPage"));
        }
    }
    
    #endregion
    
    #region Methods
    private string SendMailAndGetMailLinks(int DiscrepancyLogID, string discrepancyType, string sCurrentCommunicationLevel, string sNextCommunicationLevel)
    {
        sendCommunicationAllLevel osendCommunicationAllLevel = new sendCommunicationAllLevel();
        return osendCommunicationAllLevel.SendMailAndGetMailLinks(DiscrepancyLogID, discrepancyType, sCurrentCommunicationLevel, sNextCommunicationLevel);
    }
    private void InsertHTML() {

        DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
        DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();

        WorkflowHTML oWorkflowHTML = new WorkflowHTML();
        oDiscrepancyBE.INVHTML = oWorkflowHTML.function3("White", DateTime.Now, txtINV006aComment.Text, "");


        oDiscrepancyBE.Action = "InsertHTML";
        oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
        oDiscrepancyBE.LoggedDateTime = DateTime.Now;
        oDiscrepancyBE.LevelNumber = 1;
        int? iResult = oDiscrepancyBAL.addWorkFlowHTMLsBAL(oDiscrepancyBE);
    }

    #endregion    

    protected void btnBack_Click(object sender, EventArgs e)
    {
        //Redirecting to Previous Page
        RedirectToPage(GetQueryStringValue("PreviousPage"));
    }
}
﻿using System;
using System.Web.UI;
using System.Collections.Generic;
using BusinessEntities.ModuleBE.Discrepancy.LogDiscrepancy;
using BusinessLogicLayer.ModuleBAL.Discrepancy.LogDiscrepancy;
using Utilities;
using WebUtilities;
using BaseControlLibrary;

public partial class INVAction_INV012 : CommonPage
{
    //ReservationGoodsInDesc
    string ReservationGoodsInDesc = WebCommon.getGlobalResourceValue("ReservationGoodsInDesc");

    #region Events

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        #region Validation to select Yes or No ..
        ScriptManager.RegisterStartupScript(this, this.GetType(), "HideButton", "HideButton();", true);
        bool blnCheck = false;
        for (int index = 0; index < rblYesNo.Items.Count; index++) 
        {
            if (rblYesNo.Items[index].Selected)
                blnCheck = true;
        }
        if (!blnCheck)
        {
            string passBackToGoodsInRequired = WebCommon.getGlobalResourceValue("PassBackToGoodsInRequired");
            ScriptManager.RegisterStartupScript(this, this.GetType(), "popup", "<script>alert('" + passBackToGoodsInRequired + "');</script>", false);
            return;
        }
        #endregion 

        #region Logic to get the EmailId of Stock planner.
        //string strStockPlannerEmailId = string.Empty;
        //DiscrepancyBE oNewDiscrepancyBE = new DiscrepancyBE();
        //DiscrepancyBAL oNewDiscrepancyBAL = new DiscrepancyBAL();
        //oNewDiscrepancyBE.Action = "GetDiscrepancyLog";
        //oNewDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
        //List<DiscrepancyBE> lstDisLog = oNewDiscrepancyBAL.GetDiscrepancyLogDetailsBAL(oNewDiscrepancyBE);
        //oNewDiscrepancyBAL = null;
        //if (lstDisLog != null && lstDisLog.Count > 0)
        //{
        //    strStockPlannerEmailId = lstDisLog[0].StockPlannerEmailID;
        //}
        #endregion

        btnSave.Enabled = false;
        DISLog_WorkFlowBE oDISLog_WorkFlowBE = new DISLog_WorkFlowBE();
        DISLog_WorkFlowBAL oDISLog_WorkFlowBAL = new DISLog_WorkFlowBAL();

        if (GetQueryStringValue("disLogID") != null && GetQueryStringValue("disLogID").ToString() != "")
        {
            //**********************************************************
            string Location = hdfLocation.Value;
            DiscrepancyBE oDiscrepancyBE1 = new DiscrepancyBE();
            DiscrepancyBAL oDiscrepancyBAL1 = new DiscrepancyBAL();
            oDiscrepancyBE1.Action = "UpdateLocation";
            oDiscrepancyBE1.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
            oDiscrepancyBE1.Location = Location;
            oDiscrepancyBE1.UserID = Convert.ToInt32(Session["UserID"]);
            string sResult1 = oDiscrepancyBAL1.updateDiscrepancyLocationBAL(oDiscrepancyBE1);
            //**********************************************************
            oDISLog_WorkFlowBE.Action = "InsertUpdateWorkflowActions";
            oDISLog_WorkFlowBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
            oDISLog_WorkFlowBE.Comment = txtComments.Text.Trim();
            oDISLog_WorkFlowBE.StageCompleted = true;
            oDISLog_WorkFlowBE.StageCompletedForWhom = "INV012_StageCompleted";
            if (rblYesNo.SelectedValue.Equals("YES"))
            {
                oDISLog_WorkFlowBE.INV012_PassBackToGoodsIn = true;
                oDISLog_WorkFlowBE.Decision = "Pass back to Goods In Yes";
            }
            else
            {
                oDISLog_WorkFlowBE.INV012_PassBackToGoodsIn = false;
                oDISLog_WorkFlowBE.Decision = "Pass back to Goods In No ";
            }

            //update the work flow action table each time for the comments and other fields( if stage is completed)
            oDISLog_WorkFlowBE.DiscrepancyWorkFlowID = Convert.ToInt32(GetQueryStringValue("DisWFlowID").ToString());
            
            //insert or update the record according to the stage completed status
            int? iResult = oDISLog_WorkFlowBAL.addWorkFlowActionsBAL(oDISLog_WorkFlowBE);

            //now insert the work flow HTML according to the action taken only if the stage is completed
            DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
            DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();
            WorkflowHTML oWorkflowHTML = new WorkflowHTML();

            if (rblYesNo.SelectedValue.Equals("YES"))
            {
                oDiscrepancyBE.GINHTML = oWorkflowHTML.function4("Blue", "", DateTime.Now, Convert.ToString(Session["LoginID"]), "", string.Empty, ReservationGoodsInDesc);
                oDiscrepancyBE.GINActionRequired = true;
                oDiscrepancyBE.GINUserControl = "GIN013";

                oDiscrepancyBE.INVHTML = oWorkflowHTML.function1(DateTime.Now, null, null, "Yellow", string.Empty,
                    "Pass to Goods In", 
                    //Session["UserName"].ToString(), 
                    UIUtility.GetODVendorUserName(Convert.ToString(Session["Role"]), Convert.ToString(Session["UserName"]), Convert.ToString(Session["LoginID"])),
                    txtComments.Text.Trim());
                oDiscrepancyBE.INVActionRequired = false;

                oDiscrepancyBE.APHTML = oWorkflowHTML.function3("White", null, "", "");
                oDiscrepancyBE.APActionRequired = false;

                oDiscrepancyBE.VENHTML = oWorkflowHTML.function3("White", null, "", "");
                oDiscrepancyBE.VenActionRequired = false;

                oDiscrepancyBE.Action = "InsertHTML";
                oDiscrepancyBE.CloseDiscrepancy = false;
                oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
                oDiscrepancyBE.LoggedDateTime = DateTime.Now;
                oDiscrepancyBE.LevelNumber = 1;
                oDiscrepancyBE.ActionTakenBy = "INV";
                oDiscrepancyBE.GINActionCategory = "RA";  //Remedial Action
                int? iResult1 = oDiscrepancyBAL.addWorkFlowHTMLsBAL(oDiscrepancyBE);
            }
            else
            {
                oDiscrepancyBE.GINHTML = oWorkflowHTML.function3("White", null, "", "");
                oDiscrepancyBE.GINActionRequired = false;

                oDiscrepancyBE.INVHTML = oWorkflowHTML.function1(DateTime.Now, null, null, "Yellow", string.Empty,
                    "Closing discrepancy", 
                    //Session["UserName"].ToString(), 
                    UIUtility.GetODVendorUserName(Convert.ToString(Session["Role"]), Convert.ToString(Session["UserName"]), Convert.ToString(Session["LoginID"])),
                    txtComments.Text.Trim());
                oDiscrepancyBE.INVActionRequired = false;

                oDiscrepancyBE.APHTML = oWorkflowHTML.function3("White", null, "", "");
                oDiscrepancyBE.APActionRequired = false;

                oDiscrepancyBE.VENHTML = oWorkflowHTML.function3("White", null, "", "");
                oDiscrepancyBE.VenActionRequired = false;

                oDiscrepancyBE.Action = "InsertHTML";
                oDiscrepancyBE.CloseDiscrepancy = true;
                oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
                oDiscrepancyBE.LoggedDateTime = DateTime.Now;
                oDiscrepancyBE.LevelNumber = 1;
                int? iResult1 = oDiscrepancyBAL.addWorkFlowHTMLsBAL(oDiscrepancyBE);
            }
        }

        //Redirecting to Previous Page
        RedirectToPage(GetQueryStringValue("PreviousPage"));
    }

    #endregion

    protected void btnBack_Click(object sender, EventArgs e)
    {
        //Redirecting to Previous Page
        RedirectToPage(GetQueryStringValue("PreviousPage"));
    }
}
﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="INV002.aspx.cs" Inherits="INVAction_INV002" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Office Depot</title>
    <link rel="stylesheet" type="text/css" href="../../../../Css/style.css" />
    <link type="text/css" href="../../../../Css/ui-lightness/jquery-ui-1.8.5.custom.css"
        rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="../../../../Css/superfish.css" media="screen" />
    <script type="text/javascript" src="<%= ResolveClientUrl("../../../../Scripts/jquery-1.2.6.min.js")%>"></script>
    <script type="text/javascript" src="<%= ResolveClientUrl("../../../../Scripts/jquery-1.4.2.min.js")%>"></script>
    <script type="text/javascript" src="<%= ResolveClientUrl("../../../../Scripts/jquery-ui-1.8.5.custom.min.js")%>"></script>
    <script type="text/javascript" src="<%= ResolveClientUrl("../../../../Scripts/hoverIntent.js")%>"></script>
    <script type="text/javascript" src="<%= ResolveClientUrl("../../../../Scripts/superfish.js")%>"></script>
    <script type="text/javascript" src="<%= ResolveClientUrl("../../../../Scripts/ValidateInputs.js")%>"></script>
    <script language="javascript" type="text/javascript">
        function INV002AppOption(sender, args) {            
            var objrdo1 = document.getElementById('<%=rdoINV002ReduceOrder.ClientID %>');
            var objrdo2 = document.getElementById('<%=rdoINV002Resentbyvendor.ClientID %>');
            if (objrdo1.checked == false && objrdo2.checked == false) {
                args.IsValid = false;
            }
        }

        function GetLocation() {
            var Location = parent.document.getElementById('ctl00_ContentPlaceHolder1_txtLocation').value;
            document.getElementById('<%=hdfLocation.ClientID%>').value = Location;
            if (checkValidationGroup("a")) {
                document.getElementById('<%=btnSave.ClientID%>').style.visibility = "hidden";
            }
            else {
                document.getElementById('<%=btnSave.ClientID%>').style.visibility = "visible";
                return false;
            }

        }
        function HideButton() {
            document.getElementById('btnSave').style.visibility = 'hidden';
        }
    </script>
</head>
<body>
    <form id="form2" runat="server" method="post">
    <div>
        <cc1:ucPanel ID="pnlAtionRequired" runat="server" GroupingText="Action Required"
            Width="100%" CssClass="fieldset-form" BorderColor="gray" BorderStyle="Solid"
            BorderWidth="0">
            <table class="form-table" cellpadding="0" cellspacing="0" width="99%">
                <tr id="trInv002" runat="server">
                    <td style="font-weight: bold;"><asp:HiddenField ID="hdfLocation" runat="server" />
                        <cc1:ucLabel ID="lblINV002Desc" runat="server" Style="text-align: center;" CssClass="action-required-heading"
                            Text="Item have been short delivered. Please reduce Purchase Order or request for short delivered items to be re-delivered"></cc1:ucLabel>
                    </td>
                </tr>
                <tr id="trInv003" runat="server" style="display: none">
                    <td style="font-weight: bold;">
                        <cc1:ucLabel ID="lblINV003Desc" Style="text-align: center;" CssClass="action-required-heading"
                            runat="server" Text="Damaged Goods have been delivered. Please advise if these goods are to be resent or the PO is to be adjusted"></cc1:ucLabel>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table class="form-table" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td width="22%">
                                    <cc1:ucLabel ID="lblINV002AppOption" runat="server" Text="Please select Appropriate Option"></cc1:ucLabel>
                                </td>
                                <td width="3%">
                                    :
                                </td>
                                <td width="22%">
                                    <cc1:ucRadioButton ID="rdoINV002ReduceOrder" runat="server" GroupName="GoodsOption"
                                        Text="Reduce Purchase Order" />
                                </td>
                                <td width="57%">
                                    <cc1:ucRadioButton ID="rdoINV002Resentbyvendor" runat="server" GroupName="GoodsOption"
                                        Text="Goods to be Resent by the Vendor" />
                                </td>
                            </tr>
                            <asp:CustomValidator ID="cusvINV002AppOption" runat="server" Text="Please select Appropriate Option."
                                ClientValidationFunction="INV002AppOption" Display="None" ValidationGroup="a">
                            </asp:CustomValidator>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table class="form-table" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td width="18%">
                                    <cc1:ucLabel ID="lblINV002Comment" runat="server" Text="Please confirm action taken "></cc1:ucLabel>
                                </td>
                                <td width="82%">
                                    :
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <cc1:ucTextbox ID="txtINV002Comment" runat="server" onkeyup="checkTextLengthOnKeyUp(this,1000);"
                                        TextMode="MultiLine" Width="98%"></cc1:ucTextbox>
                                    <asp:RequiredFieldValidator ID="rfvCommentRequired" runat="server" Text="Please enter comments."
                                        ControlToValidate="txtINV002Comment" Display="None" ValidationGroup="a"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table class="form-table" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td width="14%" style="display:none;">
                                    <cc1:ucLabel ID="lblStageCompleted" runat="server" Text="Stage Completed"></cc1:ucLabel>
                                    <asp:ValidationSummary ID="vs1" runat="server" ValidationGroup="a" ShowSummary="false"
                                        ShowMessageBox="true" />
                                </td>
                                <td width="1%" style="display:none;">
                                    :
                                </td>
                                <td width="75%" align="left" style="display:none;">
                                    <cc1:ucCheckbox ID="chkINV002StageCompleted" runat="server" Checked="true" />
                                </td>
                                <td width="10%" style="display:none;">
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3" style="display:none;">
                                    <cc1:ucLabel ID="lblDiscrepancyNote" runat="server" Text="If  you required to tag a comment without closing the discrepancy<br/>1 -    Deselect Stage Complete<br/>2 -   Enter  the comment<br/>3 -   Save"></cc1:ucLabel>
                                </td>
                                <td align="right" valign="bottom">
                                    <cc1:ucButton ID="btnSave" runat="server" Text="Save" class="button" ValidationGroup="a"
                                        OnClick="btnSave_Click" OnClientClick="javascript:GetLocation()"  />
                                    <cc1:ucButton ID="btnBack" runat="server" Text="Back" class="button" OnClick="btnBack_Click" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </cc1:ucPanel>
    </div>
    </form>
</body>
</html>

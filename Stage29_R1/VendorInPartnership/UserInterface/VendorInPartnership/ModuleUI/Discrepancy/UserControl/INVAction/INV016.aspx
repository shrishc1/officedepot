﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="INV016.aspx.cs" Inherits="ModuleUI_Discrepancy_UserControl_INVAction_INV016" %>


<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Office Depot</title>
    <link rel="stylesheet" type="text/css" href="../../../../Css/style.css" />
    <link type="text/css" href="../../../../Css/ui-lightness/jquery-ui-1.8.5.custom.css"
        rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="../../../../Css/superfish.css" media="screen" />
    <script type="text/javascript" src="<%= ResolveClientUrl("../../../../Scripts/jquery-1.2.6.min.js")%>"></script>
    <script type="text/javascript" src="<%= ResolveClientUrl("../../../../Scripts/jquery-1.4.2.min.js")%>"></script>
    <script type="text/javascript" src="<%= ResolveClientUrl("../../../../Scripts/jquery-ui-1.8.5.custom.min.js")%>"></script>
    <script type="text/javascript" src="<%= ResolveClientUrl("../../../../Scripts/hoverIntent.js")%>"></script>
    <script type="text/javascript" src="<%= ResolveClientUrl("../../../../Scripts/superfish.js")%>"></script>
    <script type="text/javascript" src="<%= ResolveClientUrl("../../../../Scripts/ValidateInputs.js")%>"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#<%=trfileupload.ClientID %>').hide();
            if ($('#<%=rdoEscalateIssue.ClientID %>').is(":checked")) {
                $('#<%=trfileupload.ClientID %>').show();
            }
            else {
                $('#<%=trfileupload.ClientID %>').hide();
            }


            $('#<%= rdoAcceptGoods.ClientID%>').click(function () {
                $('#<%=trfileupload.ClientID %>').hide();
            });
            $('#<%= rdoEscalateIssue.ClientID%>').click(function () {
                $('#<%=trfileupload.ClientID %>').show();
            });
            $('#<%=btnSave.ClientID%>').click(function () {
                if ($('#<%=txtComment.ClientID%>').val() == '') {
                    alert('<%=CommentRequired %>');
                    return false;
                }
            });
        });
           
    </script>
</head>
<body>
     <form id="form2" runat="server" method="post">
    <cc1:ucPanel ID="pnlActionRequired" runat="server" Width="100%" CssClass="fieldset-form"
        BorderColor="gray" BorderStyle="Solid" BorderWidth="0" GroupingText="Action Required">
        <table class="form-table" cellpadding="3" cellspacing="4" width="96%" style="margin: 2px;">
            <tr style="padding: 15px; margin: 15px">
                <td style="font-weight: bold;" colspan="4">
                    <cc1:ucLabel ID="lblGIN0017Text1" runat="server"  CssClass="action-required-heading" 
                        Style="text-align: center;"></cc1:ucLabel>
                </td>
            </tr>
            <tr>
                <td colspan="4">
                    <cc1:ucRadioButton ID="rdoAcceptGoods"  runat="server" Checked="true" GroupName="Figure" Text="Accept Goods In's" />
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <cc1:ucRadioButton ID="rdoEscalateIssue"  runat="server" GroupName="Figure" Text="Escalate issue and attach" />
                </td>
                </tr>
                   <tr>
                            <td width="14%" style="display:none;" >
                                <cc1:ucLabel ID="lblStageCompleted" runat="server" Text="Stage Completed"></cc1:ucLabel>
                                <asp:ValidationSummary ID="vs1" runat="server" ValidationGroup="a" ShowSummary="false"
                                    ShowMessageBox="true" />
                            </td>
                            <td width="1%" style="display:none;">
                                :
                            </td>
                            <td width="65%" align="left" style="display:none;">
                                <cc1:ucCheckbox ID="chkStageCompleted" runat="server" Checked="true" />
                            </td>
                            <td width="20%" style="display:none;">
                            </td>
                        </tr>
                    <tr id="trfileupload" runat="server">
                     <td colspan="4">
                     <cc1:ucLabel ID="lblAddNewPOD" runat="server"  Text="ADD NEW POD" isRequired="true"></cc1:ucLabel>                    
                    
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;                
                    
                    <asp:FileUpload ID="fileupload" runat="server"/>
                    </td>
                    </tr>
                 <tr id="trCommentLable">
                       <td colspan="4">
                           <cc1:ucLabel ID="lblComments" Text="Comment" runat="server"></cc1:ucLabel>
                       </td>
                 </tr>
                 <tr id="trCommentText">
                       <td colspan="4">
                          <cc1:ucTextbox ID="txtComment" runat="server" 
                            TextMode="MultiLine" CssClass="textarea" Width="100%"></cc1:ucTextbox>
                         </td>
                  </tr>
           
            <tr>
           <td align="right" valign="bottom" colspan="4">
             <cc1:ucButton ID="btnSave" runat="server" Text="Save" class="button" ValidationGroup="a"
                                    OnClientClick="javascript:HideShowButton()" OnClick="btnSave_Click" />
                                <cc1:ucButton ID="btnBack" runat="server" Text="Back" class="button" OnClick="btnBack_Click" />
            </td>
            </tr>
            </table>
            </cc1:ucPanel>
            </form>
            
</body>
</html>
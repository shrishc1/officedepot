﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="INV006.aspx.cs" Inherits="INVAction_INV006" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Office Depot</title>
    <link rel="stylesheet" type="text/css" href="../../../../Css/style.css" />
    <link type="text/css" href="../../../../Css/ui-lightness/jquery-ui-1.8.5.custom.css"
        rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="../../../../Css/superfish.css" media="screen" />
    <script type="text/javascript" src="<%= ResolveClientUrl("../../../../Scripts/jquery-1.2.6.min.js")%>"></script>
    <script type="text/javascript" src="<%= ResolveClientUrl("../../../../Scripts/jquery-1.4.2.min.js")%>"></script>
    <script type="text/javascript" src="<%= ResolveClientUrl("../../../../Scripts/jquery-ui-1.8.5.custom.min.js")%>"></script>
    <script type="text/javascript" src="<%= ResolveClientUrl("../../../../Scripts/hoverIntent.js")%>"></script>
    <script type="text/javascript" src="<%= ResolveClientUrl("../../../../Scripts/superfish.js")%>"></script>
    <script type="text/javascript" src="<%= ResolveClientUrl("../../../../Scripts/ValidateInputs.js")%>"></script>
    <script type="text/javascript">
        function INV006ActionRequired(sender, args) {
            var objrdo1 = document.getElementById('<%=rdoINV006aGoodsKept.ClientID %>');
            var objrdo2 = document.getElementById('<%=rdoINV006aGoodsKeptSub.ClientID %>');
            var objrdo3 = document.getElementById('<%=rdoINV006aGoodsReturned.ClientID %>');
            var objrdo4 = document.getElementById('<%=rdoINV006aUnexpectedItem.ClientID %>');
            if (objrdo1.checked == false && objrdo2.checked == false && objrdo3.checked == false && objrdo4.checked == false) {
                args.IsValid = false;
            }
        }
        function StageCompleted() {
            var objStageCompleted = document.getElementById('<%=chkINV006aStageCompleted.ClientID %>');
            var objtxtINV001aComment = document.getElementById('<%=txtINV006aComment.ClientID %>');

            var objINV006ActionRequired = document.getElementById('<%=cusvINV006ActionRequired.ClientID %>');
            var objINV006aPurchaseOrderRequired = document.getElementById('<%=rfvINV006aPurchaseOrderRequired.ClientID %>');
            var objINV006aCommentRequired = document.getElementById('<%=rfvINV006aCommentRequired.ClientID %>');

            if (document.getElementById('<%=hdnValue.ClientID %>').value == '1') {

                ValidatorEnable(objINV006ActionRequired, false);
                ValidatorEnable(objINV006aPurchaseOrderRequired, false);
                objStageCompleted.checked = false;
                document.getElementById('<%=hdnValue.ClientID %>').value = '0';

            }
            else if (document.getElementById('<%=hdnValue.ClientID %>').value == '0') {

                ValidatorEnable(objINV006ActionRequired, true);
                if (document.getElementById('<%=rdoINV006aUnexpectedItem.ClientID %>').checked) {
                    ValidatorEnable(objINV006aPurchaseOrderRequired, true);
                }
                objStageCompleted.checked = true;
                document.getElementById('<%=hdnValue.ClientID %>').value = '1';
            }
        }
        function showHide(arg, rdo) {
            var objtdPurchaseOrder = document.getElementById('<%=tdPurchaseOrder.ClientID %>');
            var objlblINV006aComment = document.getElementById('<%=lblINV006aComment.ClientID %>');

            var objGoodsKept = document.getElementById('<%=rdoINV006aGoodsKept.ClientID %>');
            var objGoodsKeptSub = document.getElementById('<%=rdoINV006aGoodsKeptSub.ClientID %>');
            var objGoodsReturned = document.getElementById('<%=rdoINV006aGoodsReturned.ClientID %>');
            var objUnexpectedItem = document.getElementById('<%=rdoINV006aUnexpectedItem.ClientID %>');

            var objPurchaseOrderRequired = document.getElementById('<%=rfvINV006aPurchaseOrderRequired.ClientID %>');
            if (arg == '1') {
                objlblINV006aComment.innerText = 'Please enter any additional comments.';
                objtdPurchaseOrder.style.display = 'none';
                ValidatorEnable(objPurchaseOrderRequired, false);
            }
            if (arg == '2') {
                objlblINV006aComment.innerText = 'Please state why goods are to be returned to vendors.';
                objtdPurchaseOrder.style.display = 'none';
                ValidatorEnable(objPurchaseOrderRequired, false);
            }
            if (arg == '3') {
                objlblINV006aComment.innerText = 'Comment';
                objtdPurchaseOrder.style.display = '';
                ValidatorEnable(objPurchaseOrderRequired, true);
            }
        }

        function HideButton() {
            document.getElementById('btnSave').style.visibility = 'hidden';
        }
        function GetLocation() {
            var Location = parent.document.getElementById('ctl00_ContentPlaceHolder1_txtLocation').value;
            document.getElementById('<%=hdfLocation.ClientID%>').value = Location;
            if (checkValidationGroup("a")) {
                document.getElementById('<%=btnSave.ClientID%>').style.visibility = "hidden";
            }
            else {
                document.getElementById('<%=btnSave.ClientID%>').style.visibility = "visible";
                return false;
            }
        }
    </script>
</head>
<body>
    <form id="form2" runat="server" method="post">
    <div>
        <cc1:ucPanel ID="pnlActionRequired" runat="server" Width="99%" Height="100%" CssClass="fieldset-form"
            BorderColor="gray" BorderStyle="Solid" BorderWidth="0" GroupingText="Action Required">
            <table class="form-table" cellpadding="0" cellspacing="0" width="98%" style="margin: 2px;">
                <tr>
                    <td style="font-weight: bold;"><asp:HiddenField ID="hdfLocation" runat="server" />
                        <cc1:ucLabel ID="lblINV006aDesc" runat="server" CssClass="action-required-heading"
                            Style="text-align: center;" Text="Incorrect Product has been delivered - please confirm if stock to be kept or returned"></cc1:ucLabel>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table class="form-table" cellpadding="0" cellspacing="0" width="98%" style="margin: 2px;">
                            <tr>
                                <td width="22%">
                                    <cc1:ucLabel ID="lblINV006aAppOption" runat="server" Text="Please select Appropriate Option"></cc1:ucLabel>
                                </td>
                                <td colspan="3">
                                    :
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <cc1:ucRadioButton ID="rdoINV006aGoodsKept" runat="server" GroupName="GoodsOption"
                                        Text="Goods correct, Book In" onclick='javascript:return showHide(1,this);' />
                                </td>
                                <td>
                                    <cc1:ucRadioButton ID="rdoINV006aGoodsKeptSub" runat="server" GroupName="GoodsOption"
                                        Text="Goods correct Sub, Book In" onclick='javascript:return showHide(1,this);' />
                                </td>
                                <td>
                                    <cc1:ucRadioButton ID="rdoINV006aGoodsReturned" runat="server" GroupName="GoodsOption"
                                        Text="Goods to be returned to Vendor" onclick='javascript:return showHide(2,this);' />
                                </td>
                                <td>
                                    <cc1:ucRadioButton ID="rdoINV006aUnexpectedItem" runat="server" GroupName="GoodsOption"
                                        Text="Unexpected Item, Book in NAX" onclick='javascript:return showHide(3,this);' />
                                </td>
                            </tr>
                            <asp:CustomValidator ID="cusvINV006ActionRequired" runat="server" Text="Please take an action."
                                ClientValidationFunction="INV006ActionRequired" Display="None" ValidationGroup="a">
                            </asp:CustomValidator>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td id="tdPurchaseOrder" runat="server" style="display: none">
                        <table class="form-table" cellpadding="0" cellspacing="0" width="98%" style="margin: 2px;">
                            <tr>
                                <td style="width: 13%">
                                    <cc1:ucLabel ID="lblPurchaseOrder" runat="server" Text="Purchase Order Number"></cc1:ucLabel>
                                </td>
                                <td width="1%">
                                    :
                                </td>
                                <td style="width: 86%" align="left">
                                    &nbsp;<cc1:ucTextbox ID="txtINV006aPurchaseOrder" runat="server" Width="100px" MaxLength="100"></cc1:ucTextbox>
                                    <asp:RequiredFieldValidator ID="rfvINV006aPurchaseOrderRequired" runat="server" ErrorMessage="Please enter purchase order number."
                                        Display="None" ValidationGroup="a" ControlToValidate="txtINV006aPurchaseOrder"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table class="form-table" cellpadding="0" cellspacing="0" width="98%" style="margin: 2px;">
                            <tr>
                                <td width="25%">
                                    <cc1:ucLabel ID="lblINV006aComment" runat="server" Text="Please enter any additional comments "></cc1:ucLabel>
                                </td>
                                <td width="75%">
                                    :
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <cc1:ucTextbox ID="txtINV006aComment" runat="server" onkeyup="checkTextLengthOnKeyUp(this,1000);"
                                        TextMode="MultiLine" CssClass="textarea" Width="98%"></cc1:ucTextbox>
                                    <asp:RequiredFieldValidator ID="rfvINV006aCommentRequired" runat="server" Text="Please enter comments."
                                        ControlToValidate="txtINV006aComment" Display="None" ValidationGroup="a"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table class="form-table" cellpadding="0" cellspacing="0" width="98%" style="margin: 2px;">
                            <tr>
                                <td width="14%" style="display:none;">
                                    <cc1:ucLabel ID="lblStageCompleted" runat="server" Text="Stage Completed"></cc1:ucLabel>
                                    <asp:ValidationSummary ID="vs1" runat="server" ValidationGroup="a" ShowSummary="false"
                                        ShowMessageBox="true" />
                                </td>
                                <td width="1%" style="display:none;">
                                    :
                                </td>
                                <td width="75%" align="left" style="display:none;">
                                    <input type="checkbox" runat="server" id="chkINV006aStageCompleted" onclick="return StageCompleted();"
                                        checked="checked" /><input type="hidden" runat="server" id="hdnValue" value="1" />
                                </td>
                                <td width="10%" style="display:none;">
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3" style="display:none;">
                                    <cc1:ucLabel ID="lblDiscrepancyNote" runat="server" Text="If  you required to tag a comment without closing the discrepancy<br/>1 -    Deselect Stage Complete<br/>2 -   Enter  the comment<br/>3 -   Save"></cc1:ucLabel>
                                </td>
                                <td align="right" valign="bottom">
                                    <cc1:ucButton ID="btnSave" runat="server" Text="Save" class="button" ValidationGroup="a"
                                        OnClick="btnSave_Click" OnClientClick="javascript:GetLocation()" />
                                    <cc1:ucButton ID="btnBack" runat="server" Text="Back" class="button" OnClick="btnBack_Click" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </cc1:ucPanel>
    </div>
    </form>
</body>
</html>

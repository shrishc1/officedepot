﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Utilities;
using WebUtilities;
using BusinessEntities.ModuleBE.Discrepancy.LogDiscrepancy;
using BusinessLogicLayer.ModuleBAL.Discrepancy.LogDiscrepancy;
using System.Data;

public partial class INVAction_INV008 : CommonPage {

    #region Global variables
    #endregion

    #region Events

    protected void Page_Load(object sender, EventArgs e) {
        if (GetQueryStringValue("FromPage") != null && GetQueryStringValue("FromPage").ToString().Trim() == "QualityIssue")
        {
            trQualityIssue.Style.Add("display", "block");
            trWrongPackSize.Style.Add("display", "none");
            trQualityIssueOnVendorRefuseCharge.Style.Add("display", "none");
        }
        if (GetQueryStringValue("disLogID") != null && GetQueryStringValue("disLogID").ToString() != "")
        {
            DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
            DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();
            oDiscrepancyBE.Action = "CheckVendorChargeStatus";
            oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID"));
            List<DiscrepancyBE> lstDetails = oDiscrepancyBAL.CheckVendorChargeStatusBAL(oDiscrepancyBE);
            if (lstDetails.Count > 0 && lstDetails != null)
            {
                if (lstDetails[0].DiscrepancyTypeID==10 && lstDetails[0].IncorrectPackLabour == "False")
                {
                    rdoINV008RewordAndCharge.Visible = false;
                }
                else if (lstDetails[0].DiscrepancyTypeID == 12 && lstDetails[0].QualityIssueLabour == "False")
                {
                    rdoINV008RewordAndCharge.Visible = false;
                }
            }

            oDiscrepancyBE.Action = "CheckIsVendorAcceptRefuseCharge";
            oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID")); // checking on previous DiscrepancyWorkflowID
            DataTable dt = oDiscrepancyBAL.GetIsVendorAcceptRefuseChargeBAL(oDiscrepancyBE);

            if (dt != null && dt.Rows.Count > 0)
            {
                if (dt.Rows[0]["VENUserControl"].ToString().Equals("VEN030")) // means user refuses the cahrge
                {
                    trQualityIssueOnVendorRefuseCharge.Style.Add("display", "block");
                    trQualityIssue.Style.Add("display", "none");
                    trWrongPackSize.Style.Add("display", "none");
                }
            }
        }
    }

    protected void btnSave_Click(object sender, EventArgs e) {

        if (hdnChargeValue.Value == "")
        {
            rdoINV008RewordAndCharge.Visible = false; // not to be visible, even after when user takes action of save and page is getting loaded.
        }

        bool IsPleaseTakeAnActionChecked = clsDiscrepancy.ISRadioButtonChecked(rdoINV008GoodsReturnToVendor, rdoINV008ReworknotRequired, rdoINV008ReworkNoCharge, rdoINV008RewordAndCharge);
        if (IsPleaseTakeAnActionChecked == false)
        {
            string PleaseTakeAnActionMessage = WebCommon.getGlobalResourceValue("PleaseTakeAnAction");
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('" + PleaseTakeAnActionMessage + "')", true);
            return;
        }


        ScriptManager.RegisterStartupScript(this, this.GetType(), "HideButton", "HideButton();", true);
        btnSave.Enabled = false;
        string sentToLinks = string.Empty;

        DISLog_WorkFlowBE oDISLog_WorkFlowBE = new DISLog_WorkFlowBE();
        DISLog_WorkFlowBAL oDISLog_WorkFlowBAL = new DISLog_WorkFlowBAL();

        if (GetQueryStringValue("disLogID") != null && GetQueryStringValue("disLogID").ToString() != "") 
        {

            //**********************************************************
            string Location = hdfLocation.Value;
            DiscrepancyBE oDiscrepancyBE1 = new DiscrepancyBE();
            DiscrepancyBAL oDiscrepancyBAL1 = new DiscrepancyBAL();
            oDiscrepancyBE1.Action = "UpdateLocation";
            oDiscrepancyBE1.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
            oDiscrepancyBE1.Location = Location;
            oDiscrepancyBE1.UserID = Convert.ToInt32(Session["UserID"]);
            string sResult1 = oDiscrepancyBAL1.updateDiscrepancyLocationBAL(oDiscrepancyBE1);
            //**********************************************************
            oDISLog_WorkFlowBE.Action = "InsertUpdateWorkflowActions";
            oDISLog_WorkFlowBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());            
            oDISLog_WorkFlowBE.Comment = txtComments.Text.Trim();
            oDISLog_WorkFlowBE.StageCompletedForWhom = "INV008_StageCompleted";
            oDISLog_WorkFlowBE.StageCompleted = chkINV008StageCompleted.Checked ? true : false;

            if (rdoINV008GoodsReturnToVendor.Checked) {
                oDISLog_WorkFlowBE.INV008_ReworkType = "GV"; //goods return to vendor
                oDISLog_WorkFlowBE.VenCommunicationType = "Return";
                oDISLog_WorkFlowBE.Decision = "Goods return to vendor";
            }
            else if (rdoINV008ReworknotRequired.Checked) {
                oDISLog_WorkFlowBE.INV008_ReworkType = "NR"; // rework not required
                oDISLog_WorkFlowBE.VenCommunicationType = "Kept";
                oDISLog_WorkFlowBE.Decision = "Rework not required";
            }
            else if (rdoINV008ReworkNoCharge.Checked) {
                oDISLog_WorkFlowBE.INV008_ReworkType = "NC"; // rework no charge
                oDISLog_WorkFlowBE.VenCommunicationType = "Kept";
                oDISLog_WorkFlowBE.Decision = "Rework no charge";
            }
            else if (rdoINV008RewordAndCharge.Checked) {
                oDISLog_WorkFlowBE.INV008_ReworkType = "RC"; // rework and charge
                oDISLog_WorkFlowBE.VenCommunicationType = "Kept";
                oDISLog_WorkFlowBE.Decision = "Rework and charge";
            }
            
            //update the work flow action table each time for the comments and other fields( if stage is completed)
            oDISLog_WorkFlowBE.DiscrepancyWorkFlowID = Convert.ToInt32(GetQueryStringValue("DisWFlowID").ToString());

            //insert or update the record according to the stage completed status
            int? iResult = oDISLog_WorkFlowBAL.addWorkFlowActionsBAL(oDISLog_WorkFlowBE);

            //now insert the work flow HTML according to the action taken only if the stage is completed
            DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
            DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();
            WorkflowHTML oWorkflowHTML = new WorkflowHTML();

            #region WrongPackSize
            if (GetQueryStringValue("FromPage") != null && GetQueryStringValue("FromPage").ToString().Trim() == "WrongPackSize")
            {

                if (chkINV008StageCompleted.Checked)
                {
                    if (rdoINV008GoodsReturnToVendor.Checked)
                    {
                        //step - 2a
                        sentToLinks = SendMailAndGetMailLinks(Convert.ToInt32(GetQueryStringValue("disLogID")), "incorrectpacksize", "Communication1", "Communication3");

                        oDiscrepancyBE.GINHTML = oWorkflowHTML.function3("White", null, "", "");
                        oDiscrepancyBE.GINActionRequired = false;

                        oDiscrepancyBE.INVHTML = oWorkflowHTML.function1(DateTime.Now, null, null, "Yellow", "", "Return Goods", 
                            //Session["UserName"].ToString(), 
                            UIUtility.GetODVendorUserName(Convert.ToString(Session["Role"]), Convert.ToString(Session["UserName"]), Convert.ToString(Session["LoginID"])),
                            txtComments.Text.Trim(), GetQueryStringValue("PONo").ToString());
                        oDiscrepancyBE.INVActionRequired = false;

                        oDISLog_WorkFlowBE.Action = "GetAccountPayable";
                        string VendorAccountPayablesEmail = oDISLog_WorkFlowBAL.GetVendorAccountPayablesEmail(oDISLog_WorkFlowBE);
                    
                        //oDiscrepancyBE.APHTML = oWorkflowHTML.function4("Blue", "", null, VendorAccountPayablesEmail, "", "Build on to Accounts Payable worklist on " + DateTime.Now.ToString("dd/MM/yyyy") + " with the following request"
                        //                                                , @"A Discrepancy has been raised which requires for debit to be raised or credit to be issued. Please action accordingly upon receipt of invoice", "", "", DateTime.Now);
                        oDiscrepancyBE.APHTML = oWorkflowHTML.function8("green",
                                                                      @"A Discrepancy has been raised which may require a debit to be raised or credit to be issued. Please actions accordingly upon receipt of invoice.");
                        oDiscrepancyBE.APActionRequired = false; //true;
                        //oDiscrepancyBE.APUserControl = "ACP001";

                        oDiscrepancyBE.VENHTML = oWorkflowHTML.function4("Blue", "", DateTime.Now, sentToLinks,
                                                                        "Admin@OfficeDepot.com",
                                                                        @"Follow up communication sent and discrepancy builds onto Vendor Work List with following request :-
                                                                    Goods have been delivered in an incorrect Pack Quantity. Please arrange collection of goods");
                        oDiscrepancyBE.VenActionRequired = true;
                        oDiscrepancyBE.VENUserControl = "VEN017";

                        oDiscrepancyBE.Action = "InsertHTML";
                        oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString()); ;
                        oDiscrepancyBE.LoggedDateTime = DateTime.Now;
                        oDiscrepancyBE.LevelNumber = 1;
                        oDiscrepancyBE.ACP001Control = "ACP001";
                        oDiscrepancyBE.IsACP001Done = false;
                        oDiscrepancyBE.VendorActionCategory = "AR";
                        oDiscrepancyBE.ActionTakenBy = "INV";
                        int? iResult1 = oDiscrepancyBAL.addWorkFlowHTMLsBAL(oDiscrepancyBE);

                    }
                    else if (rdoINV008ReworknotRequired.Checked)
                    {
                        //step - 2b
                        sentToLinks = SendMailAndGetMailLinks(Convert.ToInt32(GetQueryStringValue("disLogID")), "incorrectpacksize", "Communication1", "Communication4");

                        oDiscrepancyBE.GINHTML = oWorkflowHTML.function4("Blue", "", null, "", "", "Build onto Goods In Work list on " + DateTime.Now.ToString("dd/MM/yyyy") + " with the following request", @"Please confirm the booking of goods in against Purchase Order " + GetQueryStringValue("PONo").ToString() + ", Do not rework.");
                        oDiscrepancyBE.GINUserControl = "GIN008";
                        oDiscrepancyBE.GINActionRequired = true;

                        oDiscrepancyBE.INVHTML = oWorkflowHTML.function1(DateTime.Now, null, null, "Yellow", "", "Keep Goods, do not rework", 
                            //Session["UserName"].ToString(), 
                            UIUtility.GetODVendorUserName(Convert.ToString(Session["Role"]), Convert.ToString(Session["UserName"]), Convert.ToString(Session["LoginID"])),
                            txtComments.Text.Trim());
                        oDiscrepancyBE.INVActionRequired = false;

                        oDiscrepancyBE.APHTML = oWorkflowHTML.function3("White", null, "", "");
                        oDiscrepancyBE.APActionRequired = false;

                        oDiscrepancyBE.VENHTML = oWorkflowHTML.function4("Blue", "", DateTime.Now, sentToLinks,
                                                                        "Admin@OfficeDepot.com",
                                                                        @"Follow up communication sent and discrepancy builds onto Vendor Work List with following request :-
                                                                    Goods have been delivered in an incorrect Pack Quantity. Please advise of remedial action");
                        oDiscrepancyBE.VenActionRequired = true;
                        oDiscrepancyBE.VENUserControl = "VEN025";

                        oDiscrepancyBE.Action = "InsertHTML";
                        oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString()); ;
                        oDiscrepancyBE.LoggedDateTime = DateTime.Now;
                        oDiscrepancyBE.LevelNumber = 1;
                        oDiscrepancyBE.VendorActionCategory = "RA";
                        oDiscrepancyBE.ActionTakenBy = "INV";
                        oDiscrepancyBE.GINActionCategory = "RA";  //Action Required
                        int? iResult1 = oDiscrepancyBAL.addWorkFlowHTMLsBAL(oDiscrepancyBE);

                    }
                    else if (rdoINV008ReworkNoCharge.Checked)
                    {
                        //step - 2c
                        sentToLinks = SendMailAndGetMailLinks(Convert.ToInt32(GetQueryStringValue("disLogID")), "incorrectpacksize", "Communication1", "Communication4");

                        oDiscrepancyBE.GINHTML = oWorkflowHTML.function4("Blue", "", null, "", "", "Build onto Goods In Work list on " + DateTime.Now.ToString("dd/MM/yyyy") + " with the following request", @"Please confirm the goods are booked in and rework to the correct pack size against Purchase Order ");
                        oDiscrepancyBE.GINUserControl = "GIN009";
                        oDiscrepancyBE.GINActionRequired = true;

                        oDiscrepancyBE.INVHTML = oWorkflowHTML.function1(DateTime.Now, null, null, "Yellow", "", "Keep Goods, Rework, No Charge", 
                            //Session["UserName"].ToString(), 
                            UIUtility.GetODVendorUserName(Convert.ToString(Session["Role"]), Convert.ToString(Session["UserName"]), Convert.ToString(Session["LoginID"])),
                            txtComments.Text.Trim());
                        oDiscrepancyBE.INVActionRequired = false;

                        oDiscrepancyBE.APHTML = oWorkflowHTML.function3("White", null, "", "");
                        oDiscrepancyBE.APActionRequired = false;

                        oDiscrepancyBE.VENHTML = oWorkflowHTML.function4("Blue", "", DateTime.Now, sentToLinks,
                                                                        "Admin@OfficeDepot.com", @"Goods with incorrect pack size have been accepted on this occasion. Please advise of remedial action for future deliveries.");
                        oDiscrepancyBE.VenActionRequired = true;
                        oDiscrepancyBE.VENUserControl = "VEN018";

                        oDiscrepancyBE.Action = "InsertHTML";
                        oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString()); ;
                        oDiscrepancyBE.LoggedDateTime = DateTime.Now;
                        oDiscrepancyBE.LevelNumber = 1;
                        oDiscrepancyBE.VendorActionCategory = "RA";
                        oDiscrepancyBE.ActionTakenBy = "INV";
                        oDiscrepancyBE.GINActionCategory = "RA";  //Action Required
                        int? iResult1 = oDiscrepancyBAL.addWorkFlowHTMLsBAL(oDiscrepancyBE);

                    }
                    else if (rdoINV008RewordAndCharge.Checked)
                    {
                        //step - 2d
                        sentToLinks = SendMailAndGetMailLinks(Convert.ToInt32(GetQueryStringValue("disLogID")), "incorrectpacksize", "Communication1", "Communication5");

                        oDiscrepancyBE.GINHTML = oWorkflowHTML.function4("Blue", "", null, "", "", "Build onto Goods In Work list on " + DateTime.Now.ToString("dd/MM/yyyy") + " with the following request", @"Please confirm the goods are booked in and rework to the correct pack size against Purchase Order. Add cost of this additional work.");
                        oDiscrepancyBE.GINUserControl = "GIN010";
                        oDiscrepancyBE.GINActionRequired = true;

                        oDiscrepancyBE.INVHTML = oWorkflowHTML.function1(DateTime.Now, null, null, "Yellow", "", "Keep Goods, Rework, Charge", 
                            //Session["UserName"].ToString(), 
                            UIUtility.GetODVendorUserName(Convert.ToString(Session["Role"]), Convert.ToString(Session["UserName"]), Convert.ToString(Session["LoginID"])),
                            txtComments.Text.Trim());
                        oDiscrepancyBE.INVActionRequired = false;

                        oDiscrepancyBE.APHTML = oWorkflowHTML.function3("White", null, "", "");
                        oDiscrepancyBE.APActionRequired = false;

                        oDiscrepancyBE.VENHTML = oWorkflowHTML.function4("Blue", "", DateTime.Now, sentToLinks,
                                                                        "Admin@OfficeDepot.com",
                                                                        @"Follow up communication sent and discrepancy builds onto Vendor Work List with following request :-
                                                                    Goods delivered in incorrect pack size. Office Depot have reworked them at a cost. Please advise of remedial action for future deliveries");
                        oDiscrepancyBE.VenActionRequired = true;
                        oDiscrepancyBE.VENUserControl = "VEN019";

                        oDiscrepancyBE.Action = "InsertHTML";
                        oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString()); ;
                        oDiscrepancyBE.LoggedDateTime = DateTime.Now;
                        oDiscrepancyBE.LevelNumber = 1;
                        oDiscrepancyBE.VendorActionCategory = "RA";
                        oDiscrepancyBE.ActionTakenBy = "INV";
                        oDiscrepancyBE.GINActionCategory = "AR";  //Action Required
                        int? iResult1 = oDiscrepancyBAL.addWorkFlowHTMLsBAL(oDiscrepancyBE);
                    }
                }
                else
                {
                    InsertHTML();
                }
            }
            #endregion

            #region Quality Issue
            
            if (GetQueryStringValue("FromPage") != null && GetQueryStringValue("FromPage").ToString().Trim() == "QualityIssue")
            {

                if (chkINV008StageCompleted.Checked)
                {
                    if (rdoINV008GoodsReturnToVendor.Checked)
                    {
                        //step - 2a
                        sentToLinks = SendMailAndGetMailLinks(Convert.ToInt32(GetQueryStringValue("disLogID")), "printqualityissue", "Communication1", "Communication2");
                        oDiscrepancyBE.GINHTML = oWorkflowHTML.function3("White", null, "", "");
                        oDiscrepancyBE.GINActionRequired = false;

                        oDiscrepancyBE.INVHTML = oWorkflowHTML.function1(DateTime.Now, null, null, "Yellow", "", "Return Goods", 
                            //Session["UserName"].ToString(), 
                            UIUtility.GetODVendorUserName(Convert.ToString(Session["Role"]), Convert.ToString(Session["UserName"]), Convert.ToString(Session["LoginID"])),
                            txtComments.Text.Trim(), GetQueryStringValue("PONo").ToString());
                        oDiscrepancyBE.INVActionRequired = false;

                        oDISLog_WorkFlowBE.Action = "GetAccountPayable";
                        string VendorAccountPayablesEmail = oDISLog_WorkFlowBAL.GetVendorAccountPayablesEmail(oDISLog_WorkFlowBE);
                    
                        //oDiscrepancyBE.APHTML = oWorkflowHTML.function4("Blue", "", null, VendorAccountPayablesEmail, "", "Build on to Accounts Payable worklist on " + DateTime.Now.ToString("dd/MM/yyyy") + " with the following request"
                        //                                                , @"A Discrepancy has been raised which requires for debit to be raised or credit to be issued. Please action accordingly upon receipt of invoice", "", "", DateTime.Now);
                        oDiscrepancyBE.APHTML = oWorkflowHTML.function8("green",
                                                                      @"A Discrepancy has been raised which may require a debit to be raised or credit to be issued. Please actions accordingly upon receipt of invoice.");
                        oDiscrepancyBE.APActionRequired = false; //true;
                        //oDiscrepancyBE.APUserControl = "ACP001";

                        oDiscrepancyBE.VENHTML = oWorkflowHTML.function4("Blue", "", DateTime.Now, sentToLinks,
                                                                        "Admin@OfficeDepot.com",
                                                                        @"Follow up communication sent and discrepancy builds onto Vendor Work List with following request :-
                                                                    There has been a quality issue with this delivery and goods are to be returned to vendor.Please arrange collection of goods");
                        oDiscrepancyBE.VenActionRequired = true;
                        oDiscrepancyBE.VENUserControl = "VEN011"; //calling VEN011 instead of VEN023

                        oDiscrepancyBE.Action = "InsertHTML";
                        oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString()); ;
                        oDiscrepancyBE.LoggedDateTime = DateTime.Now;
                        oDiscrepancyBE.LevelNumber = 1;
                        oDiscrepancyBE.ACP001Control = "ACP001";
                        oDiscrepancyBE.IsACP001Done = false;
                        oDiscrepancyBE.ActionTakenBy = "INV";
                        oDiscrepancyBE.VendorActionCategory = "AR";  //Action Required
                        int? iResult1 = oDiscrepancyBAL.addWorkFlowHTMLsBAL(oDiscrepancyBE);

                    }
                    else if (rdoINV008ReworknotRequired.Checked)
                    {
                        //step - 2b
                        sentToLinks = SendMailAndGetMailLinks(Convert.ToInt32(GetQueryStringValue("disLogID")), "printqualityissue", "Communication1", "Communication2");

                        oDiscrepancyBE.GINHTML = oWorkflowHTML.function4("Blue", "", null, "", "", "Build onto Goods In Work list on " + DateTime.Now.ToString("dd/MM/yyyy") + " with the following request", @"Please confirm the booking of goods in against Purchase Order " + GetQueryStringValue("PONo").ToString() + ", Do not rework.");
                        oDiscrepancyBE.GINUserControl = "GIN008";
                        oDiscrepancyBE.GINActionRequired = true;

                        oDiscrepancyBE.INVHTML = oWorkflowHTML.function1(DateTime.Now, null, null, "Yellow", "", "Keep Goods, do not rework", 
                            //Session["UserName"].ToString(), 
                            UIUtility.GetODVendorUserName(Convert.ToString(Session["Role"]), Convert.ToString(Session["UserName"]), Convert.ToString(Session["LoginID"])),
                            txtComments.Text.Trim());
                        oDiscrepancyBE.INVActionRequired = false;

                        oDiscrepancyBE.APHTML = oWorkflowHTML.function3("White", null, "", "");
                        oDiscrepancyBE.APActionRequired = false;

                        oDiscrepancyBE.VENHTML = oWorkflowHTML.function4("Blue", "", DateTime.Now, sentToLinks,
                                                                        "Admin@OfficeDepot.com",
                                                                        @"Follow up communication sent and discrepancy builds onto Vendor Work List with following request :-
                                                                   There has been a quality issue with this delivery.Please advise of remedial action");
                        oDiscrepancyBE.VenActionRequired = true;
                        oDiscrepancyBE.VENUserControl = "VEN025"; //calling VEN025 instead of VEN024

                        oDiscrepancyBE.Action = "InsertHTML";
                        oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString()); ;
                        oDiscrepancyBE.LoggedDateTime = DateTime.Now;
                        oDiscrepancyBE.LevelNumber = 1;
                        oDiscrepancyBE.ActionTakenBy = "INV";
                        oDiscrepancyBE.VendorActionCategory = "RA";  //Remedial Action
                        oDiscrepancyBE.GINActionCategory = "RA";  //Remedial Action
                        int? iResult1 = oDiscrepancyBAL.addWorkFlowHTMLsBAL(oDiscrepancyBE);

                    }
                    else if (rdoINV008ReworkNoCharge.Checked)
                    {
                        //step - 2c
                        sentToLinks = SendMailAndGetMailLinks(Convert.ToInt32(GetQueryStringValue("disLogID")), "printqualityissue", "Communication1", "Communication3");

                        oDiscrepancyBE.GINHTML = oWorkflowHTML.function4("Blue", "", null, "", "", "Build onto Goods In Work list on " + DateTime.Now.ToString("dd/MM/yyyy") + " with the following request", @"Please confirm the goods are booked in and rework to the correct pack size against Purchase Order ");
                        oDiscrepancyBE.GINUserControl = "GIN009";// calling GIN009 instead of GIN011
                        oDiscrepancyBE.GINActionRequired = true;

                        oDiscrepancyBE.INVHTML = oWorkflowHTML.function1(DateTime.Now, null, null, "Yellow", "", "Keep Goods, do not rework", 
                            //Session["UserName"].ToString(), 
                            UIUtility.GetODVendorUserName(Convert.ToString(Session["Role"]), Convert.ToString(Session["UserName"]), Convert.ToString(Session["LoginID"])),
                            txtComments.Text.Trim());
                        oDiscrepancyBE.INVActionRequired = false;

                        oDiscrepancyBE.APHTML = oWorkflowHTML.function3("White", null, "", "");
                        oDiscrepancyBE.APActionRequired = false;

                        oDiscrepancyBE.VENHTML = oWorkflowHTML.function4("Blue", "", DateTime.Now, sentToLinks,
                                                                        "Admin@OfficeDepot.com", @"Follow up communication sent and discrepancy builds onto Vendor Work List with following request :- There has been a quality issue with this delivery.Please advise of remedial action.");
                        oDiscrepancyBE.VenActionRequired = true;
                        oDiscrepancyBE.VENUserControl = "VEN025"; // calling VEN025 instead of VEN024

                        oDiscrepancyBE.Action = "InsertHTML";
                        oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString()); 
                        oDiscrepancyBE.LoggedDateTime = DateTime.Now;
                        oDiscrepancyBE.LevelNumber = 1;
                        oDiscrepancyBE.ActionTakenBy = "INV";
                        oDiscrepancyBE.VendorActionCategory = "RA";  //Remedial Action
                        oDiscrepancyBE.GINActionCategory = "RA";  //Remedial Action
                        int? iResult1 = oDiscrepancyBAL.addWorkFlowHTMLsBAL(oDiscrepancyBE);

                    }
                    else if (rdoINV008RewordAndCharge.Checked)
                    {
                        //step - 2d
                        sentToLinks = SendMailAndGetMailLinks(Convert.ToInt32(GetQueryStringValue("disLogID")), "printqualityissue", "Communication1", "Communication4");


                        oDISLog_WorkFlowBE.Action = "GetDataForQualityIssueWorkflowUpdate";
                        oDISLog_WorkFlowBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());

                        //DataTable dtQualityIssueWorkflowData = oDISLog_WorkFlowBAL.GetDataForQualityIssueWorkflowUpdateBAL(oDISLog_WorkFlowBE);


                        //oDiscrepancyBE.GINHTML = oWorkflowHTML.function4("Blue", "", null, "", "", "Build onto Goods In Work list on " + DateTime.Now.ToString("dd/MM/yyyy") + " with the following request", @"Please confirm the goods are booked in and reworked to the correct pack size against Purchase Order ");
                        //oDiscrepancyBE.GINUserControl = "GIN012";  // no goods in action will open at this stage (Phase 24 R2)
                        //oDiscrepancyBE.GINActionRequired = true;

                        oDiscrepancyBE.INVHTML = oWorkflowHTML.function1(DateTime.Now, null, null, "Yellow", "", "Keep Goods, Rework, Charge", 
                            //Session["UserName"].ToString(), 
                            UIUtility.GetODVendorUserName(Convert.ToString(Session["Role"]), Convert.ToString(Session["UserName"]), Convert.ToString(Session["LoginID"])),
                            txtComments.Text.Trim());
                        oDiscrepancyBE.INVActionRequired = false;

                        oDiscrepancyBE.APHTML = oWorkflowHTML.function3("White", null, "", "");
                        oDiscrepancyBE.APActionRequired = false;

                        oDiscrepancyBE.VENHTML = oWorkflowHTML.function4("Blue", "", DateTime.Now, sentToLinks,
                                                                        "Admin@OfficeDepot.com",
                                                                        @"Follow up communication sent and discrepancy builds onto Vendor Work List with following request :-
                                                                        There has been a quality issue with this delivery. Please review this charge and provide remedial action");
                        oDiscrepancyBE.VenActionRequired = true;
                        //oDiscrepancyBE.VENUserControl = "VEN025";// calling VEN025 instead of VEN024
                        oDiscrepancyBE.VENUserControl = "VEN030";// calling VEN030 instead of VEN025 Phase24 R2

                        oDiscrepancyBE.Action = "InsertHTML";
                        oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString()); ;
                        oDiscrepancyBE.LoggedDateTime = DateTime.Now;
                        oDiscrepancyBE.LevelNumber = 1;
                        oDiscrepancyBE.ActionTakenBy = "INV";
                        oDiscrepancyBE.VendorActionCategory = "AR";  //Action Required
                        int? iResult1 = oDiscrepancyBAL.addWorkFlowHTMLsBAL(oDiscrepancyBE);

                        //oDiscrepancyBE.DiscrepancyWorkFlowID = iResult1; //scope identity value for DiscrepancyWorkflowID


                        //if (dtQualityIssueWorkflowData != null && dtQualityIssueWorkflowData.Rows.Count > 0)
                        //{

                        //    oDiscrepancyBE.Action = "UpdateWorkflowForQualityIssueDisc";
                        //    oDiscrepancyBE.DiscrepancyWorkflowID = Convert.ToInt32(dtQualityIssueWorkflowData.Rows[0]["DiscrepancyWorkflowID"]); // previous DiscrepancyWorkflowID

                        //    oDiscrepancyBE.GINActionRequired = Convert.ToBoolean(dtQualityIssueWorkflowData.Rows[0]["GInActionRequired"]);
                        //    oDiscrepancyBE.GINUserControl = Convert.ToString(dtQualityIssueWorkflowData.Rows[0]["GInUserControl"]);

                        //    oDiscrepancyBE.INVActionRequired = Convert.ToBoolean(dtQualityIssueWorkflowData.Rows[0]["INVActionRequired"]);
                        //    oDiscrepancyBE.INVUserControl = Convert.ToString(dtQualityIssueWorkflowData.Rows[0]["INVUserControl"]);

                        //    oDiscrepancyBE.APActionRequired = Convert.ToBoolean(dtQualityIssueWorkflowData.Rows[0]["APActionRequired"]);
                        //    oDiscrepancyBE.APUserControl = Convert.ToString(dtQualityIssueWorkflowData.Rows[0]["APUserControl"]);

                        //    oDiscrepancyBE.VenActionRequired = Convert.ToBoolean(dtQualityIssueWorkflowData.Rows[0]["VENActionRequired"]);
                        //    oDiscrepancyBE.VENUserControl = Convert.ToString(dtQualityIssueWorkflowData.Rows[0]["VENUserControl"]);

                        //    oDiscrepancyBE.MEDActionRequired = Convert.ToBoolean(dtQualityIssueWorkflowData.Rows[0]["MEDActionRequired"]);
                        //    oDiscrepancyBE.MEDUserControl = Convert.ToString(dtQualityIssueWorkflowData.Rows[0]["MEDUserControl"]);

                        //    int? iResult2 = oDiscrepancyBAL.UpdateWorkflowForQualityIssueDiscBAL(oDiscrepancyBE);
                        //}
                    }
                }
                else
                {
                    InsertHTML();
                }
            }

            #endregion
        }

        //Redirecting to Previous Page
        RedirectToPage(GetQueryStringValue("PreviousPage"));
    }

    #endregion

    #region Methods

    private string SendMailAndGetMailLinks(int DiscrepancyLogID, string discrepancyType, string sCurrentCommunicationLevel, string sNextCommunicationLevel)
    {
        sendCommunicationAllLevel osendCommunicationAllLevel = new sendCommunicationAllLevel();
        return osendCommunicationAllLevel.SendMailAndGetMailLinks(DiscrepancyLogID, discrepancyType, sCurrentCommunicationLevel, sNextCommunicationLevel);
    }

    private void InsertHTML() {

        DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
        DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();

        WorkflowHTML oWorkflowHTML = new WorkflowHTML();
        oDiscrepancyBE.INVHTML = oWorkflowHTML.function3("White", DateTime.Now, txtComments.Text, "");
        oDiscrepancyBE.INVActionRequired = false;

        oDiscrepancyBE.Action = "InsertHTML";
        oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
        oDiscrepancyBE.LoggedDateTime = DateTime.Now;
        oDiscrepancyBE.LevelNumber = 1;
        int? iResult = oDiscrepancyBAL.addWorkFlowHTMLsBAL(oDiscrepancyBE);
    }

    #endregion

    protected void btnBack_Click(object sender, EventArgs e)
    {
        //Redirecting to Previous Page
        RedirectToPage(GetQueryStringValue("PreviousPage"));
    }
}
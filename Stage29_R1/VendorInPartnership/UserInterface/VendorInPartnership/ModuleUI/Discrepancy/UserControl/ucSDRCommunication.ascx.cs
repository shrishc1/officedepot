﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using BusinessEntities.ModuleBE.Security;
using BusinessLogicLayer.ModuleBAL.Security;
using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using BusinessLogicLayer.ModuleBAL.Appointment.SiteSettings;
using BaseControlLibrary;

public partial class ucSDRCommunication : System.Web.UI.UserControl
{
    public int VendorID
    {
        get;
        set;
    }

    public int SiteID
    {
        get;
        set;
    }    

    public string VendorEmailList
    {
        get
        {
            if (!string.IsNullOrWhiteSpace(txtAltEmail.Text))
            {
                return txtAltEmail.Text;
            }
            return ucVendorEmailList.Text;
        }
        set
        {
            ucVendorEmailList.Text = value;
            hdnEmail.Value = value;
        }
    }
    public char innerControlRdoCommunication
    {
        get
        {
            char selection;
            if (rdoEmailComm.Checked)
                selection = 'E';
            else if (rdoLetter.Checked)
                selection = 'L';
            else
                selection = 'F';

            return selection;
        }
        set
        {
            switch (value)
            {
                case 'E':
                    rdoEmailComm.Checked = true;
                    ucVendorEmailList.Text = hdnEmail.Value;
                    return;
                case 'L':
                    rdoLetter.Checked = true;
                    ucVendorEmailList.Text = "";
                    return;
                default:
                    return;

            }
        }
    }
    public ucTextbox innerControlEmailList
    {
        get
        {
            return this.ucVendorEmailList;
        }
    }
    public ucTextbox innerControlAltEmailList
    {
        get
        {
            return this.txtAltEmail;
        }
    } 
    public ucRadioButton innerControlRdoLetter
    {
        get
        {   
            return rdoLetter;
        }
    }
    public ucRadioButton innerControlRdoEmail
    {
        get
        {
            return rdoEmailComm;
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

        }
    }
    protected void rdoEmailComm_CheckedChanged(object sender, EventArgs e)
    {
        if (rdoEmailComm.Checked)
            ucVendorEmailList.Text = hdnEmail.Value;
    }
    protected void rdoFax_CheckedChanged(object sender, EventArgs e)
    {
      
    }
    protected void rdoLetter_CheckedChanged(object sender, EventArgs e)
    {
        if (rdoLetter.Checked)
            ucVendorEmailList.Text = "";
    }
    public void GetDetails()
    {
        if (VendorID > 0)
        {
            SCT_UserBE oSCT_UserBE = new SCT_UserBE();
            SCT_UserBAL oSCT_UserBAL = new SCT_UserBAL();
            oSCT_UserBE.Action = "GetContactDetailsDiscrepancy";
            oSCT_UserBE.VendorID = VendorID;
            oSCT_UserBE.SiteId = this.SiteID;
            List<SCT_UserBE> lstVendorDetails1 = oSCT_UserBAL.GetUserDetailsBAL(oSCT_UserBE);
            hdnEmail.Value = "";
            hdnFax.Value = "";
            if (lstVendorDetails1 != null && lstVendorDetails1.Count > 0)
            {
                foreach (SCT_UserBE item in lstVendorDetails1)
                {
                    hdnEmail.Value += item.EmailId.ToString() + ", ";
                    hdnFax.Value += item.FaxNumber.ToString() + ", ";
                }
                hdnEmail.Value = hdnEmail.Value.Trim(new char[] { ',',' ' });
                hdnFax.Value = hdnFax.Value.Trim(new char[] { ',', ' ' });
            }
            else
            {
                MASSIT_VendorBE oMASSIT_VendorBE = new MASSIT_VendorBE();
                APPSIT_VendorBAL oAPPSIT_VendorBAL = new APPSIT_VendorBAL();
                oMASSIT_VendorBE.Action = "GetContactDetails";
                oSCT_UserBE.SiteId = this.SiteID;
                oMASSIT_VendorBE.SiteVendorID = VendorID;

                List<MASSIT_VendorBE> lstVendorDetails2 = oAPPSIT_VendorBAL.GetVendorContactDetailsBAL(oMASSIT_VendorBE);
                if (lstVendorDetails2 != null && lstVendorDetails2.Count > 0)
                {
                    if (!string.IsNullOrEmpty(lstVendorDetails2[0].Vendor.VendorContactEmail.ToString())) {
                        hdnEmail.Value = lstVendorDetails2[0].Vendor.VendorContactEmail.ToString();
                        hdnFax.Value = lstVendorDetails2[0].Vendor.VendorContactFax.ToString();
                    }
                }
            }
            if (string.IsNullOrEmpty(hdnEmail.Value)) {
                hdnEmail.Value = System.Configuration.ConfigurationManager.AppSettings["DefaultEmailAddress"];
                ucVendorEmailList.Text = System.Configuration.ConfigurationManager.AppSettings["DefaultEmailAddress"];
            }            
        }
    }
}
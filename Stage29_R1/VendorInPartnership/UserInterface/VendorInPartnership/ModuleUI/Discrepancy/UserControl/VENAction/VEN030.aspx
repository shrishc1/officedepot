﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="VEN030.aspx.cs" Inherits="ModuleUI_Discrepancy_UserControl_VENAction_VEN030" %>

<!DOCTYPE html>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Office Depot</title>
    <link rel="stylesheet" type="text/css" href="../../../../Css/style.css" />
    <link type="text/css" href="../../../../Css/ui-lightness/jquery-ui-1.8.5.custom.css"
        rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="../../../../Css/superfish.css" media="screen" />
    <script type="text/javascript" src="<%= ResolveClientUrl("../../../../Scripts/jquery-1.2.6.min.js")%>"></script>
    <script type="text/javascript" src="<%= ResolveClientUrl("../../../../Scripts/jquery-1.4.2.min.js")%>"></script>
    <script type="text/javascript" src="<%= ResolveClientUrl("../../../../Scripts/jquery-ui-1.8.5.custom.min.js")%>"></script>
    <script type="text/javascript" src="<%= ResolveClientUrl("../../../../Scripts/hoverIntent.js")%>"></script>
    <script type="text/javascript" src="<%= ResolveClientUrl("../../../../Scripts/superfish.js")%>"></script>
    <script type="text/javascript" src="<%= ResolveClientUrl("../../../../Scripts/ValidateInputs.js")%>"></script>
    <script type="text/javascript">
        function PleaseTakeAnAction(sender, args) {
             var objrdo1 = document.getElementById('<%=rdoAcceptCharge.ClientID %>');
             var objrdo2 = document.getElementById('<%=rdoRefuseCharge.ClientID %>');
             
             if (objrdo1.checked == false && objrdo2.checked == false ) {
                 args.IsValid = false;
             }
         }
        function HideShowButton() {
            if (checkValidationGroup("a")) {
                document.getElementById('<%=btnSave.ClientID%>').style.visibility = "hidden";
            }
            else {
                document.getElementById('<%=btnSave.ClientID%>').style.visibility = "visible";
                return false;
            }
        }
        function GetChargeValue() {       
            var txtQualityIssueCharges = parent.document.getElementById('ctl00_ContentPlaceHolder1_hdnQualityIssueCharges').value;
            document.getElementById("txtChargeAmount").value = txtQualityIssueCharges;
            document.getElementById("hdnChargeValue").value = txtQualityIssueCharges;
        }

    </script>
</head>
<body onload="GetChargeValue();">
   
       <form id="form2" runat="server" method="post">
            <asp:HiddenField ID="hdnChargeValue" runat="server" />
    <cc1:ucPanel GroupingText="Action Required" ID="pnlActionRequired" runat="server"
        Width="100%" CssClass="fieldset-form">
        <table class="form-table" cellpadding="0" cellspacing="0" width="98%" style="margin: 2px;">            
            <tr id="trQualityIssue" runat="server">
                <td style="font-weight: bold;" >
                    <cc1:ucLabel ID="lblVEN030Header" runat="server" CssClass="action-required-heading" Style="text-align: center;"></cc1:ucLabel><br />
                </td>
            </tr>
            <tr>
                <td style="width:10%">
                    <cc1:ucLabel ID="lblChargeAmount" runat="server" Text="Charge Amount" ></cc1:ucLabel>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                    <cc1:ucTextbox ID="txtChargeAmount" runat="server" Enabled="false"></cc1:ucTextbox><br /><br />  
                </td>  
                
            </tr>
            <tr>
                <td>
                    <cc1:ucRadioButton ID="rdoAcceptCharge" runat="server" GroupName="goodsReturnedOption"
                        Text="Accept the Charge"  />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                    <cc1:ucRadioButton ID="rdoRefuseCharge" runat="server" GroupName="goodsReturnedOption"
                        Text="Refuse the Charge"  /> <br />
                     <asp:CustomValidator ID="cusvPleaseTakeAnAction" runat="server" Text="Please take an action."
                                        ClientValidationFunction="PleaseTakeAnAction" Display="None" ValidationGroup="a">
                                    </asp:CustomValidator>
                    <br />
                </td>                                           
            </tr>
            <tr>
                <td style="width:10%">
                    <cc1:ucLabel ID="lblRemedialAction" runat="server" Text="Please supply a brief narrative of remedial action taken :"></cc1:ucLabel>
                </td>                
            </tr>
            <tr>
                <td>
                    <cc1:ucTextbox ID="txtComments" runat="server" onkeyup="checkTextLengthOnKeyUp(this,1000);"
                        TextMode="MultiLine" CssClass="textarea" Width="98%"></cc1:ucTextbox>
                    <asp:RequiredFieldValidator ID="rfvCommentRequired" runat="server" Text="Please enter comments."
                        ControlToValidate="txtComments" Display="None" ValidationGroup="a">
                    </asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <%--<td style="text-align:right">
                    <cc1:ucButton ID="btnSave" runat="server" Text="Save" class="button" ValidationGroup="a"
                    OnClientClick="javascript:HideShowButton()"
                        OnClick="btnSave_Click" />
                    <asp:ValidationSummary ID="vs1" runat="server" ValidationGroup="a" ShowSummary="false"
                        ShowMessageBox="true" />
                    <cc1:ucButton ID="btnBack" runat="server" Text="Back" class="button" OnClick="btnBack_Click" />
                </td>--%>
                 <td>
                    <table class="form-table" cellpadding="0" cellspacing="0" width="98%" style="margin: 2px;">
                        <tr>
                            <td style="width: 80%">
                                <asp:ValidationSummary ID="vs1" runat="server" ValidationGroup="a" ShowSummary="false"
                                    ShowMessageBox="true" />
                            </td>
                            <td style="text-align: right; vertical-align: top;">
                                <cc1:ucButton ID="btnSave" runat="server" Text="Save" class="button" ValidationGroup="a"
                                OnClientClick="javascript:HideShowButton()"
                                    OnClick="btnSave_Click"  />
                                <input type="hidden" runat="server" id="hdnValue" value="1" />
                                <cc1:ucButton ID="btnBack" runat="server" Text="Back" class="button" OnClick="btnBack_Click" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </cc1:ucPanel>
    </form>
</body>
</html>

﻿using BusinessEntities.ModuleBE.Discrepancy.LogDiscrepancy;
using BusinessLogicLayer.ModuleBAL.Discrepancy.LogDiscrepancy;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ModuleUI_Discrepancy_UserControl_VENAction_VEN030 : CommonPage
{
    #region Global variables
    #endregion

    #region Events

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
        }
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        btnSave.Enabled = false;
        DISLog_WorkFlowBE oDISLog_WorkFlowBE = new DISLog_WorkFlowBE();
        DISLog_WorkFlowBAL oDISLog_WorkFlowBAL = new DISLog_WorkFlowBAL();

        if (GetQueryStringValue("disLogID") != null && GetQueryStringValue("disLogID").ToString() != "")
        {
            oDISLog_WorkFlowBE.Action = "InsertUpdateWorkflowActions";
            oDISLog_WorkFlowBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
            oDISLog_WorkFlowBE.Comment = txtComments.Text.Trim();
            oDISLog_WorkFlowBE.StageCompleted = true;
            oDISLog_WorkFlowBE.StageCompletedForWhom = "VEN030_StageCompleted";
            if (rdoAcceptCharge.Checked)
            {
                oDISLog_WorkFlowBE.VEN030_IsChargeAcceptRefuse = "Accept";
                oDISLog_WorkFlowBE.Decision = "Accept the Charge";
            }
            else if (rdoRefuseCharge.Checked)
            {
                oDISLog_WorkFlowBE.VEN030_IsChargeAcceptRefuse = "Refuse";
                oDISLog_WorkFlowBE.Decision = "Refuse the Charge";
            }            

            if (hdnChargeValue.Value != "")
            {
                oDISLog_WorkFlowBE.VEN030_ChargeValue = Convert.ToDecimal(hdnChargeValue.Value);
            }
            else
            {
                oDISLog_WorkFlowBE.VEN030_ChargeValue = (decimal?)null;
            }

            //update the work flow action table each time for the comments and other fields( if stage is completed)
            oDISLog_WorkFlowBE.DiscrepancyWorkFlowID = Convert.ToInt32(GetQueryStringValue("DisWFlowID").ToString());

            //insert or update the record according to the stage completed status
            int? iResult = oDISLog_WorkFlowBAL.addWorkFlowActionsBAL(oDISLog_WorkFlowBE);

            DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
            DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();

            //For getting Inventory Action Log date and then update VendorActionElaspedTime in Trn_DiscrepancyLog Table
            oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID"));
            oDiscrepancyBE.Action = "GetInventoryLogDate";
            oDiscrepancyBE.DiscrepancyWorkFlowID = Convert.ToInt32(GetQueryStringValue("DisWFlowID"));
            DataTable dtAllDetails = oDiscrepancyBAL.GetAllDetailsOfWorkFlowBAL(oDiscrepancyBE);
            DateTime DtInventoryLogDate = DateTime.Now;

            if (dtAllDetails != null && dtAllDetails.Rows.Count > 0)
            {
                DtInventoryLogDate = Convert.ToDateTime(dtAllDetails.Rows[0]["LogDate"]);
            }
            oDiscrepancyBE.VendorActionElapsedTime = (decimal?)DateTime.Now.Subtract(DtInventoryLogDate).TotalDays;
            oDiscrepancyBE.Action = "UpdateVendorActionElapsedTime";
            oDiscrepancyBAL.UpdateVendorActionElapsedTimeBAL(oDiscrepancyBE);

            //now insert the work flow HTML according to the action taken only if the stage is completed
            WorkflowHTML oWorkflowHTML = new WorkflowHTML();


            if (GetQueryStringValue("FromPage") != null && GetQueryStringValue("FromPage").ToString().Trim() == "QualityIssue")
            {
                string pAction = string.Empty;

                if(rdoAcceptCharge.Checked)
                {
                    pAction = "Accept Charge";
                }
                else if (rdoRefuseCharge.Checked)
                {
                    pAction = "Refused Charge";
                }

                oDISLog_WorkFlowBE.Action = "GetDataForSpecificAction";
                oDISLog_WorkFlowBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
                oDISLog_WorkFlowBE.StageCompletedForWhom = "INV008_StageCompleted";
                DataTable dtINV001 = oDISLog_WorkFlowBAL.GetDataForSpecificActionBAL(oDISLog_WorkFlowBE);

                if (dtINV001 != null && dtINV001.Rows.Count > 0)
                {
                    //'GV' goods return to vendor, 'NR' rework not required, 'NC' rework no charge, 'RC' rework and charge                   
                    if (dtINV001.Rows[0]["INV008_ReworkType"].ToString() == "RC")
                        oDISLog_WorkFlowBE.StageCompletedForWhom = "GIN010_StageCompleted";
                }


                DataTable dtGIN = oDISLog_WorkFlowBAL.GetDataForSpecificActionBAL(oDISLog_WorkFlowBE);

                if (dtGIN != null && dtGIN.Rows.Count > 0)
                {
                    oDiscrepancyBE.CloseDiscrepancy = true;
                }

                oDISLog_WorkFlowBE.Action = "GetDataForQualityIssueWorkflowUpdate";
                oDISLog_WorkFlowBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());

                //DataTable dtQualityIssueWorkflowData = oDISLog_WorkFlowBAL.GetDataForQualityIssueWorkflowUpdateBAL(oDISLog_WorkFlowBE);
                                
                if (rdoAcceptCharge.Checked)
                {
                    oDiscrepancyBE.GINHTML = oWorkflowHTML.function4("Blue", "", null, "", "", "Build onto Goods In Work list on " + DateTime.Now.ToString("dd/MM/yyyy") + " with the following request", @"Please confirm the goods are booked in and reworked to the correct pack size against Purchase Order ");
                    oDiscrepancyBE.GINUserControl = "GIN012";  // no goods in action will open at this stage (Phase 24 R2)
                    oDiscrepancyBE.GINActionRequired = true;
                    oDiscrepancyBE.GINActionCategory = "AR";  //Action Required
                    oDiscrepancyBE.INVHTML = oWorkflowHTML.function3("white", null);
                    oDiscrepancyBE.INVActionRequired = false;
                }
                else if (rdoRefuseCharge.Checked)
                {
                    oDiscrepancyBE.GINHTML = oWorkflowHTML.function3("white", null);
                    oDiscrepancyBE.GINActionRequired = false;

                    oDiscrepancyBE.INVHTML  = oWorkflowHTML.function4("Blue", "", null, "", "", "Build onto Inventory Work list on " + DateTime.Now.ToString("dd/MM/yyyy") + " with the following request", @"Vendor has refused to accept the charge. Please review and state what should happen with the goods");
                    oDiscrepancyBE.INVActionRequired = true;                   
                    oDiscrepancyBE.InventoryActionCategory = "AR";  //Action Required
                    oDiscrepancyBE.INVUserControl = "INV008";
                }

                //oDiscrepancyBE.INVHTML = oWorkflowHTML.function3("white", null);
                //oDiscrepancyBE.INVActionRequired = false;

                oDiscrepancyBE.APHTML = oWorkflowHTML.function3("white", null);
                oDiscrepancyBE.APActionRequired = false;

                oDiscrepancyBE.VENHTML = oWorkflowHTML.function1(DateTime.Now, null,
                                                                    null, "Yellow", "", pAction,
                                                                    //Session["UserName"].ToString(),
                                                                    UIUtility.GetODVendorUserName(Convert.ToString(Session["Role"]), Convert.ToString(Session["UserName"]), Convert.ToString(Session["LoginID"])),
                                                                    txtComments.Text);
                oDiscrepancyBE.VenActionRequired = false;

                oDiscrepancyBE.Action = "InsertHTML";
                oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
                oDiscrepancyBE.LoggedDateTime = DateTime.Now;
                oDiscrepancyBE.LevelNumber = 1;
                oDiscrepancyBE.ActionTakenBy = "VEN";               
                int? iResult1 = oDiscrepancyBAL.addWorkFlowHTMLsBAL(oDiscrepancyBE);

                //oDiscrepancyBE.DiscrepancyWorkFlowID = iResult1; //scope identity value for DiscrepancyWorkflowID
                

                //if (dtQualityIssueWorkflowData != null && dtQualityIssueWorkflowData.Rows.Count > 0)
                //{
                    
                //    oDiscrepancyBE.Action = "UpdateWorkflowForQualityIssueDisc";
                //    oDiscrepancyBE.DiscrepancyWorkflowID = Convert.ToInt32(dtQualityIssueWorkflowData.Rows[0]["DiscrepancyWorkflowID"]); // previous DiscrepancyWorkflowID

                //    oDiscrepancyBE.GINActionRequired = Convert.ToBoolean(dtQualityIssueWorkflowData.Rows[0]["GInActionRequired"]);
                //    oDiscrepancyBE.GINUserControl = Convert.ToString(dtQualityIssueWorkflowData.Rows[0]["GInUserControl"])  ;

                //    oDiscrepancyBE.INVActionRequired = Convert.ToBoolean(dtQualityIssueWorkflowData.Rows[0]["INVActionRequired"]);
                //    oDiscrepancyBE.INVUserControl = Convert.ToString(dtQualityIssueWorkflowData.Rows[0]["INVUserControl"]);

                //    oDiscrepancyBE.APActionRequired = Convert.ToBoolean(dtQualityIssueWorkflowData.Rows[0]["APActionRequired"]);
                //    oDiscrepancyBE.APUserControl = Convert.ToString(dtQualityIssueWorkflowData.Rows[0]["APUserControl"]);

                //    oDiscrepancyBE.VenActionRequired = Convert.ToBoolean(dtQualityIssueWorkflowData.Rows[0]["VENActionRequired"]);
                //    oDiscrepancyBE.VENUserControl = Convert.ToString(dtQualityIssueWorkflowData.Rows[0]["VENUserControl"]);

                //    oDiscrepancyBE.MEDActionRequired = Convert.ToBoolean(dtQualityIssueWorkflowData.Rows[0]["MEDActionRequired"]);
                //    oDiscrepancyBE.MEDUserControl = Convert.ToString(dtQualityIssueWorkflowData.Rows[0]["MEDUserControl"]);

                //    int? iResult2 = oDiscrepancyBAL.UpdateWorkflowForQualityIssueDiscBAL(oDiscrepancyBE);
                //}
            }
        }
        //Redirecting to Previous Page
        RedirectToPage(GetQueryStringValue("PreviousPage"));
    }
    #endregion

    #region Methods    
    #endregion

    protected void btnBack_Click(object sender, EventArgs e)
    {
        //Redirecting to Previous Page
        RedirectToPage(GetQueryStringValue("PreviousPage"));
    }
}
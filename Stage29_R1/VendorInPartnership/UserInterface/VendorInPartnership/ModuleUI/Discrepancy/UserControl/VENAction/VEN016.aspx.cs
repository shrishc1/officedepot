﻿using System;

using BusinessEntities.ModuleBE.Discrepancy.LogDiscrepancy;
using BusinessLogicLayer.ModuleBAL.Discrepancy.LogDiscrepancy;
using System.Data;

public partial class VENAction_VEN016 : CommonPage {
    #region Global variables

    static int SiteID = 0;
    static string VDRNo = string.Empty;
    static int DiscrepancyLogID = 0;

    #endregion

    #region Events

    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btnSave_Click(object sender, EventArgs e) {

        /*
         * Step 2. Vendor supplies their remedial action
         * Vendor responds to the discrepancy being raised and adds what action they have taken. 
         * There are no further actions so discrepancy is closed. If they do no respond then go to step 3
         */
        btnSave.Enabled = false;
        DISLog_WorkFlowBE oDISLog_WorkFlowBE = new DISLog_WorkFlowBE();
        DISLog_WorkFlowBAL oDISLog_WorkFlowBAL = new DISLog_WorkFlowBAL();
        if (GetQueryStringValue("disLogID") != null && GetQueryStringValue("disLogID").ToString() != "") 
        {
            
            oDISLog_WorkFlowBE.Action = "InsertUpdateWorkflowActions";
            oDISLog_WorkFlowBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
            oDISLog_WorkFlowBE.Comment = txtComments.Text.Trim();
            oDISLog_WorkFlowBE.StageCompleted = true;
            oDISLog_WorkFlowBE.StageCompletedForWhom = "VEN016_StageCompleted";

            //update the work flow action table each time for the comments and other fields( if stage is completed)
            oDISLog_WorkFlowBE.DiscrepancyWorkFlowID = Convert.ToInt32(GetQueryStringValue("DisWFlowID").ToString());
            oDISLog_WorkFlowBE.Decision = "";
            //insert or update the record according to the stage completed status
            int? iResult = oDISLog_WorkFlowBAL.addWorkFlowActionsBAL(oDISLog_WorkFlowBE);

            DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
            DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();

            //For getting Inventory Action Log date and then update VendorActionElaspedTime in Trn_DiscrepancyLog Table
            oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID"));
            oDiscrepancyBE.Action = "GetInventoryLogDate";
            oDiscrepancyBE.DiscrepancyWorkFlowID = Convert.ToInt32(GetQueryStringValue("DisWFlowID"));
            DataTable dtAllDetails = oDiscrepancyBAL.GetAllDetailsOfWorkFlowBAL(oDiscrepancyBE);
            DateTime DtInventoryLogDate = DateTime.Now;

            if (dtAllDetails != null && dtAllDetails.Rows.Count > 0)
            {
                DtInventoryLogDate = Convert.ToDateTime(dtAllDetails.Rows[0]["LogDate"]);
            }
            oDiscrepancyBE.VendorActionElapsedTime = (decimal?)DateTime.Now.Subtract(DtInventoryLogDate).TotalDays;
            oDiscrepancyBE.Action = "UpdateVendorActionElapsedTime";
            oDiscrepancyBAL.UpdateVendorActionElapsedTimeBAL(oDiscrepancyBE);

            //now insert the work flow HTML according to the action taken only if the stage is completed
            WorkflowHTML oWorkflowHTML = new WorkflowHTML();

            oDiscrepancyBE.GINHTML = oWorkflowHTML.function3("White", null, "", "");
            oDiscrepancyBE.GINActionRequired = false;

            oDiscrepancyBE.INVHTML = oWorkflowHTML.function3("White", null, "", "");
            oDiscrepancyBE.INVActionRequired = false;

            oDiscrepancyBE.APHTML = oWorkflowHTML.function3("White", null, "", "");
            oDiscrepancyBE.APActionRequired = false;

            oDiscrepancyBE.VENHTML = oWorkflowHTML.function1(DateTime.Now, null, null, "Yellow", "", "",
            //Session["UserName"].ToString(), 
            UIUtility.GetODVendorUserName(Convert.ToString(Session["Role"]), Convert.ToString(Session["UserName"]), Convert.ToString(Session["LoginID"])),
            "", "", "", "", "", "", "", "", txtComments.Text);
            oDiscrepancyBE.VenActionRequired = false;

            oDiscrepancyBE.Action = "InsertHTML";
            oDiscrepancyBE.CloseDiscrepancy = true;
            oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
            oDiscrepancyBE.LoggedDateTime = DateTime.Now;
            oDiscrepancyBE.LevelNumber = 1;
            int? iResult1 = oDiscrepancyBAL.addWorkFlowHTMLsBAL(oDiscrepancyBE);
        }
        //Redirecting to Previous Page
        RedirectToPage(GetQueryStringValue("PreviousPage"));
    }

    #endregion

    #region Methods



    #endregion

    protected void btnBack_Click(object sender, EventArgs e)
    {
        //Redirecting to Previous Page
        RedirectToPage(GetQueryStringValue("PreviousPage"));
    }
}
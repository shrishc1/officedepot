﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucVENCommunication.ascx.cs"
    Inherits="ModuleUI_Discrepancy_UserControl_ucVENCommunication" %>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<table width="100%" cellspacing="5" cellpadding="0" class="form-table">
    <tr>
        <td>
            <cc1:ucPanel ID="pnlVen001a" runat="server" Width="70%" CssClass="fieldset-form">
                <table class="form-table" cellpadding="5" cellspacing="0">
                    <tr>
                        <td colspan="2" style="font-weight: bold; background-color: Yellow">
                            <cc1:ucLabel ID="lblVen001a" runat="server" Text="Please select the reason the goods were over delivered and select method of return "></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <cc1:ucLabel ID="lblSelectReason" runat="server" Text="Reason for goods being over delevered"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <cc1:ucRadioButtonList ID="rdoDebitCredit" runat="server" RepeatDirection="Horizontal"
                                Width="80%">
                                <asp:ListItem Text="Rounded Order Quantity"></asp:ListItem>
                                <asp:ListItem Text="Open Order Quantity"></asp:ListItem>
                                <asp:ListItem Text="Other"></asp:ListItem>
                            </cc1:ucRadioButtonList>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <hr />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <cc1:ucLabel ID="lblhow" runat="server" Text="How should goods be returned ?"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <cc1:ucRadioButtonList ID="UcRadioButtonList1" runat="server" RepeatDirection="Horizontal"
                                Width="80%">
                                <asp:ListItem Text="Vendor to Collect"></asp:ListItem>
                                <asp:ListItem Text="Office Depot to Return"></asp:ListItem>
                            </cc1:ucRadioButtonList>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="padding-left: 280px">
                            <cc1:ucLabel ID="UcLabel1" runat="server" ForeColor="Red" Font-Size="Smaller" Text="Carriage cahrge to be applied"></cc1:ucLabel>
                            <br />
                            <cc1:ucLabel ID="UcLabel35" runat="server" ForeColor="Red" Font-Size="Smaller" Text="(minimum charge per pallete $$,per cartoon $$)"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucLabel ID="lblGoodsCollectionDate" runat="server" Text="Date Goods are to be collected"></cc1:ucLabel>
                        </td>
                        <td>
                            <cc1:ucTextbox ID="txtGoodsCollectionDate" runat="server"></cc1:ucTextbox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucLabel ID="lblGoodsCarrier" runat="server" Text="Carrier Who will be collecting goods"></cc1:ucLabel>
                        </td>
                        <td>
                            <cc1:ucTextbox ID="txtGoodsCarrier" runat="server"></cc1:ucTextbox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucLabel ID="lblCollectionAuthorizationNo" runat="server" Text="Collection Authorization #"></cc1:ucLabel>
                        </td>
                        <td>
                            <cc1:ucTextbox ID="txtCollectionAuthorizationNo" runat="server"></cc1:ucTextbox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <cc1:ucLabel ID="lblActionTaken" runat="server" Text="Please supply a brief narrative of remedial action taken"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <cc1:ucTextbox ID="txtActionTaken" runat="server" onkeyup="checkTextLengthOnKeyUp(this,1000);" TextMode="MultiLine" Width="700px"
                                Height="100px"></cc1:ucTextbox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <cc1:ucButton ID="btnSave" runat="server" Text="Save" class="button" />
                        </td>
                    </tr>
                </table>
            </cc1:ucPanel>
            <cc1:ucPanel ID="pnlVEN001b" runat="server" Width="70%" CssClass="fieldset-form">
                <table class="form-table" cellpadding="5" cellspacing="0">
                    <tr>
                        <td colspan="2" style="font-weight: bold; background-color: Yellow">
                            <cc1:ucLabel ID="lblVEN001b" runat="server" Text="Please select the reason the goods were over delivered and select method of return "></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <cc1:ucLabel ID="UcLabel3" runat="server" Text="Reason for goods being over delevered"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <cc1:ucRadioButtonList ID="UcRadioButtonList2" runat="server" RepeatDirection="Horizontal"
                                Width="80%">
                                <asp:ListItem Text="Rounded Order Quantity"></asp:ListItem>
                                <asp:ListItem Text="Open Order Quantity"></asp:ListItem>
                                <asp:ListItem Text="Other" Selected="True"></asp:ListItem>
                            </cc1:ucRadioButtonList>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <hr />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <cc1:ucLabel ID="UcLabel4" runat="server" Text="How should goods be returned ?"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <cc1:ucRadioButtonList ID="UcRadioButtonList3" runat="server" RepeatDirection="Horizontal"
                                Width="80%">
                                <asp:ListItem Text="Vendor to Collect"></asp:ListItem>
                                <asp:ListItem Text="Office Depot to Return"></asp:ListItem>
                            </cc1:ucRadioButtonList>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="padding-left: 280px">
                            <cc1:ucLabel ID="UcLabel5" runat="server" ForeColor="Red" Font-Size="Smaller" Text="Carriage cahrge to be applied"></cc1:ucLabel>
                            <br />
                            <cc1:ucLabel ID="UcLabel10" runat="server" ForeColor="Red" Font-Size="Smaller" Text="(minimum charge per pallete $$,per cartoon $$)"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucLabel ID="UcLabel13" runat="server" Text="Collection Authorization #"></cc1:ucLabel>
                        </td>
                        <td>
                            <cc1:ucTextbox ID="UcTextbox3" runat="server"></cc1:ucTextbox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <cc1:ucLabel ID="UcLabel14" runat="server" Text="Please supply a brief narrative of remedial action taken"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <cc1:ucTextbox ID="UcTextbox5" runat="server" onkeyup="checkTextLengthOnKeyUp(this,1000);" TextMode="MultiLine" Width="700px"
                                Height="100px"></cc1:ucTextbox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <cc1:ucButton ID="UcButton1" runat="server" Text="Save" class="button" />
                        </td>
                    </tr>
                </table>
            </cc1:ucPanel>
            <cc1:ucPanel ID="pnlVEN002" runat="server" Width="70%" CssClass="fieldset-form">
                <table class="form-table" cellpadding="5" cellspacing="0">
                    <tr>
                        <td style="font-weight: bold; background-color: Yellow">
                            <cc1:ucLabel ID="lblVEN002" runat="server" Text="Please select the reason the goods were over delivered and confirm of remedial action "></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucLabel ID="UcLabel11" runat="server" Text="Reason for goods being over delevered"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucRadioButtonList ID="UcRadioButtonList4" runat="server" RepeatDirection="Horizontal"
                                Width="80%">
                                <asp:ListItem Text="Rounded Order Quantity"></asp:ListItem>
                                <asp:ListItem Text="Open Order Quantity"></asp:ListItem>
                                <asp:ListItem Text="Other"></asp:ListItem>
                            </cc1:ucRadioButtonList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucLabel ID="UcLabel18" runat="server" Text="Please supply a brief narrative of remedial action taken"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucTextbox ID="UcTextbox2" runat="server" onkeyup="checkTextLengthOnKeyUp(this,1000);" TextMode="MultiLine" Width="700px"
                                Height="100px"></cc1:ucTextbox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucButton ID="UcButton2" runat="server" Text="Save" class="button" />
                        </td>
                    </tr>
                </table>
            </cc1:ucPanel>
            <cc1:ucPanel ID="pnlVEN003" runat="server" Width="70%" CssClass="fieldset-form">
                <table class="form-table" cellpadding="5" cellspacing="0">
                    <tr>
                        <td style="font-weight: bold; background-color: Yellow">
                            <cc1:ucLabel ID="lblVEN003" runat="server" Text="Short delivered,do not resend goods, credit/debit is to issued.Pleased advise of remedial action "></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucLabel ID="UcLabel15" runat="server" Text="Please supply a brief narrative of remedial action taken"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucTextbox ID="UcTextbox1" runat="server" onkeyup="checkTextLengthOnKeyUp(this,1000);" TextMode="MultiLine" Width="700px"
                                Height="100px"></cc1:ucTextbox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucButton ID="UcButton3" runat="server" Text="Save" class="button" />
                        </td>
                    </tr>
                </table>
            </cc1:ucPanel>
            <cc1:ucPanel ID="pnlVEN004" runat="server" Width="70%" CssClass="fieldset-form">
                <table class="form-table" cellpadding="5" cellspacing="0">
                    <tr>
                        <td style="font-weight: bold; background-color: Yellow">
                            <cc1:ucLabel ID="lblVEN004" runat="server" Text="Short delivered.Credit/Debit will be issued, please resend items short delivered and advise of remedial action "></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucLabel ID="UcLabel12" runat="server" Text="Please supply a brief narrative of remedial action taken"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucTextbox ID="UcTextbox6" runat="server" onkeyup="checkTextLengthOnKeyUp(this,1000);" TextMode="MultiLine" Width="700px"
                                Height="100px"></cc1:ucTextbox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucButton ID="UcButton4" runat="server" Text="Save" class="button" />
                        </td>
                    </tr>
                </table>
            </cc1:ucPanel>
            <cc1:ucPanel ID="pnlVEN005" runat="server" Width="70%" CssClass="fieldset-form">
                <table class="form-table" cellpadding="5" cellspacing="0">
                    <tr>
                        <td colspan="2" style="font-weight: bold; background-color: Yellow">
                            <cc1:ucLabel ID="lblVEN005" runat="server" Text="Goods received damaged, do not resend damaged goods.Please arrange collection. "></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="padding-left: 50px">
                            <cc1:ucRadioButtonList ID="UcRadioButtonList7" runat="server" RepeatDirection="Horizontal"
                                Width="100%">
                                <asp:ListItem Text="Vendor to collect"></asp:ListItem>
                                <asp:ListItem Text="Office Depot to return"></asp:ListItem>
                                <asp:ListItem Text="Dispose of goods"></asp:ListItem>
                            </cc1:ucRadioButtonList>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="padding-left: 280px">
                            <cc1:ucLabel ID="UcLabel25" runat="server" ForeColor="Red" Font-Size="Smaller" Text="Carriage cahrge to be applied"></cc1:ucLabel>
                            <br />
                            <cc1:ucLabel ID="UcLabel28" runat="server" ForeColor="Red" Font-Size="Smaller" Text="(minimum charge per pallete $$,per cartoon $$)"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucLabel ID="UcLabel30" runat="server" Text="Date Goods are to be collected"></cc1:ucLabel>
                        </td>
                        <td>
                            <cc1:ucTextbox ID="UcTextbox11" runat="server"></cc1:ucTextbox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucLabel ID="UcLabel31" runat="server" Text="Carrier Who will be collecting goods"></cc1:ucLabel>
                        </td>
                        <td>
                            <cc1:ucTextbox ID="UcTextbox12" runat="server"></cc1:ucTextbox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucLabel ID="UcLabel32" runat="server" Text="Collection Authorization #"></cc1:ucLabel>
                        </td>
                        <td>
                            <cc1:ucTextbox ID="UcTextbox13" runat="server"></cc1:ucTextbox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <cc1:ucLabel ID="UcLabel33" runat="server" Text="Comment"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <cc1:ucTextbox ID="UcTextbox14" runat="server" onkeyup="checkTextLengthOnKeyUp(this,1000);" TextMode="MultiLine" Width="700px"
                                Height="100px"></cc1:ucTextbox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <cc1:ucButton ID="UcButton6" runat="server" Text="Save" class="button" />
                        </td>
                    </tr>
                </table>
            </cc1:ucPanel>
            <cc1:ucPanel ID="pnlVPN005b" runat="server" Width="70%" CssClass="fieldset-form">
                <table class="form-table" cellpadding="5" cellspacing="0">
                    <tr>
                        <td colspan="2" style="font-weight: bold; background-color: Yellow">
                            <cc1:ucLabel ID="lblVPN005b" runat="server" Text="Goods received damaged, do not resend damaged goods, debit is to issued.Please arrange collection. "></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="padding-left: 50px">
                            <cc1:ucRadioButtonList ID="UcRadioButtonList8" runat="server" RepeatDirection="Horizontal"
                                Width="100%">
                                <asp:ListItem Text="Vendor to collect"></asp:ListItem>
                                <asp:ListItem Text="Office Depot to return"></asp:ListItem>
                                <asp:ListItem Text="Dispose of goods"></asp:ListItem>
                            </cc1:ucRadioButtonList>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="padding-left: 270px">
                            <cc1:ucLabel ID="UcLabel26" runat="server" ForeColor="Red" Font-Size="Smaller" Text="Carriage cahrge to be applied"></cc1:ucLabel>
                            <br />
                            <cc1:ucLabel ID="UcLabel27" runat="server" ForeColor="Red" Font-Size="Smaller" Text="(minimum charge per pallete $$,per cartoon $$)"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucLabel ID="UcLabel36" runat="server" Text="Collection Authorization #"></cc1:ucLabel>
                        </td>
                        <td>
                            <cc1:ucTextbox ID="UcTextbox17" runat="server"></cc1:ucTextbox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <cc1:ucLabel ID="UcLabel37" runat="server" Text="Comment"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <cc1:ucTextbox ID="UcTextbox18" runat="server" onkeyup="checkTextLengthOnKeyUp(this,1000);" TextMode="MultiLine" Width="700px"
                                Height="100px"></cc1:ucTextbox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <cc1:ucButton ID="UcButton7" runat="server" Text="Save" class="button" />
                        </td>
                    </tr>
                </table>
            </cc1:ucPanel>
            <cc1:ucPanel ID="pnlVEN005c" runat="server" Width="70%" CssClass="fieldset-form">
                <table class="form-table" cellpadding="5" cellspacing="0">
                    <tr>
                        <td style="font-weight: bold; background-color: Yellow">
                            <cc1:ucLabel ID="lblVEN005c" runat="server" Text="Goods received damaged, do not resend damaged goods, debit is to issued.Please arrange collection. "></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-left: 50px">
                            <cc1:ucRadioButtonList ID="UcRadioButtonList9" runat="server" RepeatDirection="Horizontal"
                                Width="100%">
                                <asp:ListItem Text="Vendor to collect"></asp:ListItem>
                                <asp:ListItem Text="Office Depot to return"></asp:ListItem>
                                <asp:ListItem Text="Dispose of goods"></asp:ListItem>
                            </cc1:ucRadioButtonList>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-left: 280px">
                            <cc1:ucLabel ID="UcLabel6" runat="server" ForeColor="Red" Font-Size="Smaller" Text="Carriage cahrge to be applied"></cc1:ucLabel>
                            <br />
                            <cc1:ucLabel ID="UcLabel34" runat="server" ForeColor="Red" Font-Size="Smaller" Text="(minimum charge per pallete $$,per cartoon $$)"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucLabel ID="UcLabel39" runat="server" Text="Comment"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucTextbox ID="UcTextbox16" runat="server" onkeyup="checkTextLengthOnKeyUp(this,1000);" TextMode="MultiLine" Width="700px"
                                Height="100px"></cc1:ucTextbox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucButton ID="UcButton8" runat="server" Text="Save" class="button" />
                        </td>
                    </tr>
                </table>
            </cc1:ucPanel>
            <cc1:ucPanel ID="pnlVEN006a" runat="server" Width="70%" CssClass="fieldset-form">
                <table class="form-table" cellpadding="5" cellspacing="0">
                    <tr>
                        <td colspan="2" style="font-weight: bold; background-color: Yellow">
                            <cc1:ucLabel ID="lblVEN006a" runat="server" Text="Items have been delivered damaged.Please arrange collection of damaged items and send replacements. "></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-left: 50px" colspan="2">
                            <cc1:ucRadioButtonList ID="UcRadioButtonList10" runat="server" RepeatDirection="Horizontal"
                                Width="100%">
                                <asp:ListItem Text="Vendor to collect"></asp:ListItem>
                                <asp:ListItem Text="Office Depot to return"></asp:ListItem>
                                <asp:ListItem Text="Dispose of goods"></asp:ListItem>
                            </cc1:ucRadioButtonList>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="padding-left: 280px">
                            <cc1:ucLabel ID="UcLabel7" runat="server" ForeColor="Red" Font-Size="Smaller" Text="Carriage cahrge to be applied"></cc1:ucLabel>
                            <br />
                            <cc1:ucLabel ID="UcLabel8" runat="server" ForeColor="Red" Font-Size="Smaller" Text="(minimum charge per pallete $$,per cartoon $$)"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucLabel ID="UcLabel9" runat="server" Text="Date Goods are to be collected"></cc1:ucLabel>
                        </td>
                        <td>
                            <cc1:ucTextbox ID="UcTextbox4" runat="server"></cc1:ucTextbox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucLabel ID="UcLabel29" runat="server" Text="Carrier Who will be collecting goods"></cc1:ucLabel>
                        </td>
                        <td>
                            <cc1:ucTextbox ID="UcTextbox15" runat="server"></cc1:ucTextbox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucLabel ID="UcLabel38" runat="server" Text="Collection Authorization #"></cc1:ucLabel>
                        </td>
                        <td>
                            <cc1:ucTextbox ID="UcTextbox19" runat="server"></cc1:ucTextbox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <cc1:ucLabel ID="UcLabel40" runat="server" Text="Comment"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <cc1:ucTextbox ID="UcTextbox20" runat="server" onkeyup="checkTextLengthOnKeyUp(this,1000);" TextMode="MultiLine" Width="700px"
                                Height="100px"></cc1:ucTextbox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <cc1:ucButton ID="UcButton9" runat="server" Text="Save" class="button" />
                        </td>
                    </tr>
                </table>
            </cc1:ucPanel>
            <cc1:ucPanel ID="pnlVEN006b" runat="server" Width="70%" CssClass="fieldset-form">
                <table class="form-table" cellpadding="5" cellspacing="0">
                    <tr>
                        <td colspan="2" style="font-weight: bold; background-color: Yellow">
                            <cc1:ucLabel ID="lblVEN006b" runat="server" Text="Items have been delivered damaged.Please arrange collection of damaged items and send replacements. "></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="padding-left: 50px">
                            <cc1:ucRadioButtonList ID="UcRadioButtonList11" runat="server" RepeatDirection="Horizontal"
                                Width="100%">
                                <asp:ListItem Text="Vendor to collect"></asp:ListItem>
                                <asp:ListItem Text="Office Depot to return"></asp:ListItem>
                                <asp:ListItem Text="Dispose of goods"></asp:ListItem>
                            </cc1:ucRadioButtonList>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="padding-left: 270px">
                            <cc1:ucLabel ID="UcLabel42" runat="server" ForeColor="Red" Font-Size="Smaller" Text="Carriage cahrge to be applied"></cc1:ucLabel>
                            <br />
                            <cc1:ucLabel ID="UcLabel43" runat="server" ForeColor="Red" Font-Size="Smaller" Text="(minimum charge per pallete $$,per cartoon $$)"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucLabel ID="UcLabel44" runat="server" Text="Collection Authorization #"></cc1:ucLabel>
                        </td>
                        <td>
                            <cc1:ucTextbox ID="UcTextbox21" runat="server"></cc1:ucTextbox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <cc1:ucLabel ID="UcLabel45" runat="server" Text="Comment"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <cc1:ucTextbox ID="UcTextbox22" runat="server" onkeyup="checkTextLengthOnKeyUp(this,1000);" TextMode="MultiLine" Width="700px"
                                Height="100px"></cc1:ucTextbox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <cc1:ucButton ID="UcButton10" runat="server" Text="Save" class="button" />
                        </td>
                    </tr>
                </table>
            </cc1:ucPanel>
            <cc1:ucPanel ID="pnlVEN006c" runat="server" Width="70%" CssClass="fieldset-form">
                <table class="form-table" cellpadding="5" cellspacing="0">
                    <tr>
                        <td style="font-weight: bold; background-color: Yellow">
                            <cc1:ucLabel ID="lblVEN006c" runat="server" Text="Items have been delivered damaged.Please arrange collection of damaged items and send replacements. "></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-left: 50px">
                            <cc1:ucRadioButtonList ID="UcRadioButtonList12" runat="server" RepeatDirection="Horizontal"
                                Width="100%">
                                <asp:ListItem Text="Vendor to collect"></asp:ListItem>
                                <asp:ListItem Text="Office Depot to return"></asp:ListItem>
                                <asp:ListItem Text="Dispose of goods"></asp:ListItem>
                            </cc1:ucRadioButtonList>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-left: 270px">
                            <cc1:ucLabel ID="UcLabel46" runat="server" ForeColor="Red" Font-Size="Smaller" Text="Carriage cahrge to be applied"></cc1:ucLabel>
                            <br />
                            <cc1:ucLabel ID="UcLabel47" runat="server" ForeColor="Red" Font-Size="Smaller" Text="(minimum charge per pallete $$,per cartoon $$)"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucLabel ID="UcLabel49" runat="server" Text="Comment"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucTextbox ID="UcTextbox24" runat="server" onkeyup="checkTextLengthOnKeyUp(this,1000);" TextMode="MultiLine" Width="700px"
                                Height="100px"></cc1:ucTextbox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucButton ID="UcButton11" runat="server" Text="Save" class="button" />
                        </td>
                    </tr>
                </table>
            </cc1:ucPanel>
            <cc1:ucPanel ID="pnlVEN007a" runat="server" Width="70%" CssClass="fieldset-form">
                <table class="form-table" cellpadding="5" cellspacing="0">
                    <tr>
                        <td colspan="2" style="font-weight: bold; background-color: Yellow">
                            <cc1:ucLabel ID="lblVEN007a" runat="server" Text="No paperwork has been supplied with this delivery so we are unable to accept. Please decide method of return."></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <cc1:ucRadioButtonList ID="UcRadioButtonList13" runat="server" RepeatDirection="Horizontal"
                                Width="80%">
                                <asp:ListItem Text="Vendor to collect"></asp:ListItem>
                                <asp:ListItem Text="Office Depot to return"></asp:ListItem>
                            </cc1:ucRadioButtonList>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="padding-left: 270px">
                            <cc1:ucLabel ID="UcLabel48" runat="server" ForeColor="Red" Font-Size="Smaller" Text="Carriage cahrge to be applied"></cc1:ucLabel>
                            <br />
                            <cc1:ucLabel ID="UcLabel50" runat="server" ForeColor="Red" Font-Size="Smaller" Text="(minimum charge per pallete $$,per cartoon $$)"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucLabel ID="UcLabel51" runat="server" Text="Date Goods are to be collected"></cc1:ucLabel>
                        </td>
                        <td>
                            <cc1:ucTextbox ID="UcTextbox23" runat="server"></cc1:ucTextbox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucLabel ID="UcLabel52" runat="server" Text="Carrier Who will be collecting goods"></cc1:ucLabel>
                        </td>
                        <td>
                            <cc1:ucTextbox ID="UcTextbox25" runat="server"></cc1:ucTextbox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucLabel ID="UcLabel53" runat="server" Text="Collection Authorization #"></cc1:ucLabel>
                        </td>
                        <td>
                            <cc1:ucTextbox ID="UcTextbox26" runat="server"></cc1:ucTextbox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <cc1:ucLabel ID="UcLabel54" runat="server" Text="Please supply a brief narrative of remedial action taken"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <cc1:ucTextbox ID="UcTextbox27" runat="server" onkeyup="checkTextLengthOnKeyUp(this,1000);" TextMode="MultiLine" Width="700px"
                                Height="100px"></cc1:ucTextbox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <cc1:ucButton ID="UcButton12" runat="server" Text="Save" class="button" />
                        </td>
                    </tr>
                </table>
            </cc1:ucPanel>
            <cc1:ucPanel ID="pnlVEN007b" runat="server" Width="70%" CssClass="fieldset-form">
                <table class="form-table" cellpadding="5" cellspacing="0">
                    <tr>
                        <td colspan="2" style="font-weight: bold; background-color: Yellow">
                            <cc1:ucLabel ID="lblVEN007b" runat="server" Text="No paperwork has been supplied with this delivery so we are unable to accept. Please decide method of return."></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <cc1:ucRadioButtonList ID="UcRadioButtonList14" runat="server" RepeatDirection="Horizontal"
                                Width="80%">
                                <asp:ListItem Text="Vendor to collect"></asp:ListItem>
                                <asp:ListItem Text="Office Depot to return"></asp:ListItem>
                            </cc1:ucRadioButtonList>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="padding-left: 270px">
                            <cc1:ucLabel ID="UcLabel55" runat="server" ForeColor="Red" Font-Size="Smaller" Text="Carriage cahrge to be applied"></cc1:ucLabel>
                            <br />
                            <cc1:ucLabel ID="UcLabel56" runat="server" ForeColor="Red" Font-Size="Smaller" Text="(minimum charge per pallete $$,per cartoon $$)"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucLabel ID="UcLabel59" runat="server" Text="Collection Authorization #"></cc1:ucLabel>
                        </td>
                        <td>
                            <cc1:ucTextbox ID="UcTextbox30" runat="server"></cc1:ucTextbox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <cc1:ucLabel ID="UcLabel60" runat="server" Text="Please supply a brief narrative of remedial action taken"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <cc1:ucTextbox ID="UcTextbox31" runat="server" onkeyup="checkTextLengthOnKeyUp(this,1000);" TextMode="MultiLine" Width="700px"
                                Height="100px"></cc1:ucTextbox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <cc1:ucButton ID="UcButton13" runat="server" Text="Save" class="button" />
                        </td>
                    </tr>
                </table>
            </cc1:ucPanel>
            <cc1:ucPanel ID="pnlVEN008" runat="server" Width="70%" CssClass="fieldset-form">
                <table class="form-table" cellpadding="5" cellspacing="0">
                    <tr>
                        <td style="font-weight: bold; background-color: Yellow">
                            <cc1:ucLabel ID="lblVEN008" runat="server" Text="Goods were delivered without any paperwork. On this occasion it was accepted but may not be in the future. Please advise of remedial action to ensure this does not occur in future."></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucLabel ID="UcLabel62" runat="server" Text="Please supply a brief narrative of remedial action taken"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucTextbox ID="UcTextbox29" runat="server" onkeyup="checkTextLengthOnKeyUp(this,1000);" TextMode="MultiLine" Width="700px"
                                Height="100px"></cc1:ucTextbox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucButton ID="UcButton14" runat="server" Text="Save" class="button" />
                        </td>
                    </tr>
                </table>
            </cc1:ucPanel>
            <cc1:ucPanel ID="pnlVEN009a" runat="server" Width="70%" CssClass="fieldset-form">
                <table class="form-table" cellpadding="5" cellspacing="0">
                    <tr>
                        <td colspan="2" style="font-weight: bold; background-color: Yellow">
                            <cc1:ucLabel ID="lblVEN009a" runat="server" Text="No purchase order has been supplied with this delivery so we are unable to accept. Please decide method of return."></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <cc1:ucRadioButtonList ID="UcRadioButtonList15" runat="server" RepeatDirection="Horizontal"
                                Width="80%">
                                <asp:ListItem Text="Vendor to collect"></asp:ListItem>
                                <asp:ListItem Text="Office Depot to return"></asp:ListItem>
                            </cc1:ucRadioButtonList>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="padding-left: 270px">
                            <cc1:ucLabel ID="UcLabel57" runat="server" ForeColor="Red" Font-Size="Smaller" Text="Carriage cahrge to be applied"></cc1:ucLabel>
                            <br />
                            <cc1:ucLabel ID="UcLabel58" runat="server" ForeColor="Red" Font-Size="Smaller" Text="(minimum charge per pallete $$,per cartoon $$)"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucLabel ID="UcLabel61" runat="server" Text="Date Goods are to be collected"></cc1:ucLabel>
                        </td>
                        <td>
                            <cc1:ucTextbox ID="UcTextbox28" runat="server"></cc1:ucTextbox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucLabel ID="UcLabel63" runat="server" Text="Carrier Who will be collecting goods"></cc1:ucLabel>
                        </td>
                        <td>
                            <cc1:ucTextbox ID="UcTextbox32" runat="server"></cc1:ucTextbox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucLabel ID="UcLabel64" runat="server" Text="Collection Authorization #"></cc1:ucLabel>
                        </td>
                        <td>
                            <cc1:ucTextbox ID="UcTextbox33" runat="server"></cc1:ucTextbox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <cc1:ucLabel ID="UcLabel65" runat="server" Text="Please supply a brief narrative of remedial action taken"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <cc1:ucTextbox ID="UcTextbox34" runat="server" onkeyup="checkTextLengthOnKeyUp(this,1000);" TextMode="MultiLine" Width="700px"
                                Height="100px"></cc1:ucTextbox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <cc1:ucButton ID="UcButton15" runat="server" Text="Save" class="button" />
                        </td>
                    </tr>
                </table>
            </cc1:ucPanel>
            <cc1:ucPanel ID="pnlVEN009b" runat="server" Width="70%" CssClass="fieldset-form">
                <table class="form-table" cellpadding="5" cellspacing="0">
                    <tr>
                        <td colspan="2" style="font-weight: bold; background-color: Yellow">
                            <cc1:ucLabel ID="lblVEN009b" runat="server" Text="No purchase order has been supplied with this delivery so we are unable to accept. Please decide method of return."></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <cc1:ucRadioButtonList ID="UcRadioButtonList16" runat="server" RepeatDirection="Horizontal"
                                Width="80%">
                                <asp:ListItem Text="Vendor to collect"></asp:ListItem>
                                <asp:ListItem Text="Office Depot to return"></asp:ListItem>
                            </cc1:ucRadioButtonList>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="padding-left: 270px">
                            <cc1:ucLabel ID="UcLabel66" runat="server" ForeColor="Red" Font-Size="Smaller" Text="Carriage cahrge to be applied"></cc1:ucLabel>
                            <br />
                            <cc1:ucLabel ID="UcLabel67" runat="server" ForeColor="Red" Font-Size="Smaller" Text="(minimum charge per pallete $$,per cartoon $$)"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucLabel ID="UcLabel70" runat="server" Text="Collection Authorization #"></cc1:ucLabel>
                        </td>
                        <td>
                            <cc1:ucTextbox ID="UcTextbox37" runat="server"></cc1:ucTextbox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <cc1:ucLabel ID="UcLabel71" runat="server" Text="Please supply a brief narrative of remedial action taken"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <cc1:ucTextbox ID="UcTextbox38" runat="server" onkeyup="checkTextLengthOnKeyUp(this,1000);" TextMode="MultiLine" Width="700px"
                                Height="100px"></cc1:ucTextbox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <cc1:ucButton ID="UcButton16" runat="server" Text="Save" class="button" />
                        </td>
                    </tr>
                </table>
            </cc1:ucPanel>
            <cc1:ucPanel ID="pnlVEN010" runat="server" Width="70%" CssClass="fieldset-form">
                <table class="form-table" cellpadding="5" cellspacing="0">
                    <tr>
                        <td style="font-weight: bold; background-color: Yellow">
                            <cc1:ucLabel ID="lblVEN010" runat="server" Text="Goods were delivered without a Purchase Order reference. On this occasion it was accepted but may not be in the future. Please advise of remedial action to ensure this does not occur in future."></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucLabel ID="UcLabel68" runat="server" Text="Please supply a brief narrative of remedial action taken"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucTextbox ID="UcTextbox35" runat="server" onkeyup="checkTextLengthOnKeyUp(this,1000);" TextMode="MultiLine" Width="700px"
                                Height="100px"></cc1:ucTextbox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucButton ID="UcButton17" runat="server" Text="Save" class="button" />
                        </td>
                    </tr>
                </table>
            </cc1:ucPanel>
            <cc1:ucPanel ID="pnlVEN011a" runat="server" Width="70%" CssClass="fieldset-form">
                <table class="form-table" cellpadding="5" cellspacing="0">
                    <tr>
                        <td colspan="2" style="font-weight: bold; background-color: Yellow">
                            <cc1:ucLabel ID="lblVEN011a" runat="server" Text="An incorrect product has been delivered and is to be returned. Please select method of return and advise of remedial action."></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <cc1:ucRadioButtonList ID="UcRadioButtonList17" runat="server" RepeatDirection="Horizontal"
                                Width="80%">
                                <asp:ListItem Text="Vendor to collect"></asp:ListItem>
                                <asp:ListItem Text="Office Depot to return"></asp:ListItem>
                            </cc1:ucRadioButtonList>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="padding-left: 280px">
                            <cc1:ucLabel ID="UcLabel69" runat="server" ForeColor="Red" Font-Size="Smaller" Text="Carriage cahrge to be applied"></cc1:ucLabel>
                            <br />
                            <cc1:ucLabel ID="UcLabel72" runat="server" ForeColor="Red" Font-Size="Smaller" Text="(minimum charge per pallete $$,per cartoon $$)"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucLabel ID="UcLabel73" runat="server" Text="Date Goods are to be collected"></cc1:ucLabel>
                        </td>
                        <td>
                            <cc1:ucTextbox ID="UcTextbox36" runat="server"></cc1:ucTextbox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucLabel ID="UcLabel74" runat="server" Text="Carrier Who will be collecting goods"></cc1:ucLabel>
                        </td>
                        <td>
                            <cc1:ucTextbox ID="UcTextbox39" runat="server"></cc1:ucTextbox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucLabel ID="UcLabel75" runat="server" Text="Collection Authorization #"></cc1:ucLabel>
                        </td>
                        <td>
                            <cc1:ucTextbox ID="UcTextbox40" runat="server"></cc1:ucTextbox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <cc1:ucLabel ID="UcLabel76" runat="server" Text="Please supply a brief narrative of remedial action taken"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <cc1:ucTextbox ID="UcTextbox41" runat="server" onkeyup="checkTextLengthOnKeyUp(this,1000);" TextMode="MultiLine" Width="700px"
                                Height="100px"></cc1:ucTextbox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <cc1:ucButton ID="UcButton18" runat="server" Text="Save" class="button" />
                        </td>
                    </tr>
                </table>
            </cc1:ucPanel>
            <cc1:ucPanel ID="pnlVEN011b" runat="server" Width="70%" CssClass="fieldset-form">
                <table class="form-table" cellpadding="5" cellspacing="0">
                    <tr>
                        <td colspan="2" style="font-weight: bold; background-color: Yellow">
                            <cc1:ucLabel ID="lblVEN011b" runat="server" Text="No purchase order has been supplied with this delivery so we are unable to accept. Please decide method of return."></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <cc1:ucRadioButtonList ID="UcRadioButtonList18" runat="server" RepeatDirection="Horizontal"
                                Width="80%">
                                <asp:ListItem Text="Vendor to collect"></asp:ListItem>
                                <asp:ListItem Text="Office Depot to return"></asp:ListItem>
                            </cc1:ucRadioButtonList>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="padding-left: 280px">
                            <cc1:ucLabel ID="UcLabel77" runat="server" ForeColor="Red" Font-Size="Smaller" Text="Carriage cahrge to be applied"></cc1:ucLabel>
                            <br />
                            <cc1:ucLabel ID="UcLabel78" runat="server" ForeColor="Red" Font-Size="Smaller" Text="(minimum charge per pallete $$,per cartoon $$)"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucLabel ID="UcLabel81" runat="server" Text="Collection Authorization #"></cc1:ucLabel>
                        </td>
                        <td>
                            <cc1:ucTextbox ID="UcTextbox44" runat="server"></cc1:ucTextbox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <cc1:ucLabel ID="UcLabel82" runat="server" Text="Please supply a brief narrative of remedial action taken"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <cc1:ucTextbox ID="UcTextbox45" runat="server" onkeyup="checkTextLengthOnKeyUp(this,1000);" TextMode="MultiLine" Width="700px"
                                Height="100px"></cc1:ucTextbox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <cc1:ucButton ID="UcButton19" runat="server" Text="Save" class="button" />
                        </td>
                    </tr>
                </table>
            </cc1:ucPanel>
            <cc1:ucPanel ID="pnlVEN012" runat="server" Width="70%" CssClass="fieldset-form">
                <table class="form-table" cellpadding="5" cellspacing="0">
                    <tr>
                        <td style="font-weight: bold; background-color: Yellow">
                            <cc1:ucLabel ID="lblVEN012" runat="server" Text="An incorrect product has been received in by Office Depot and kept on this occasion. Please advise of remedial action to ensure this does not happen in the future."></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucLabel ID="UcLabel79" runat="server" Text="Please supply a brief narrative of remedial action taken"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucTextbox ID="UcTextbox42" runat="server" onkeyup="checkTextLengthOnKeyUp(this,1000);" TextMode="MultiLine" Width="700px"
                                Height="100px"></cc1:ucTextbox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucButton ID="UcButton20" runat="server" Text="Save" class="button" />
                        </td>
                    </tr>
                </table>
            </cc1:ucPanel>
            <cc1:ucPanel ID="pnlVEN013" runat="server" Width="70%" CssClass="fieldset-form">
                <table class="form-table" cellpadding="5" cellspacing="0">
                    <tr>
                        <td style="font-weight: bold; background-color: Yellow">
                            <cc1:ucLabel ID="lblVEN013" runat="server" Text="As per the original discrepancy note, products received did not match the Purchase Order detail. Please advise of remedial action."></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucLabel ID="UcLabel80" runat="server" Text="Please supply a brief narrative of remedial action taken"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucTextbox ID="UcTextbox43" runat="server" onkeyup="checkTextLengthOnKeyUp(this,1000);" TextMode="MultiLine" Width="700px"
                                Height="100px"></cc1:ucTextbox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucButton ID="UcButton21" runat="server" Text="Save" class="button" />
                        </td>
                    </tr>
                </table>
            </cc1:ucPanel>
            <cc1:ucPanel ID="pnlVEN014" runat="server" Width="70%" CssClass="fieldset-form">
                <table class="form-table" cellpadding="5" cellspacing="0">
                    <tr>
                        <td style="font-weight: bold; background-color: Yellow">
                            <cc1:ucLabel ID="lblVEN014" runat="server" Text="There has been a presentation issue with this delivery, please advise of remedial action to ensure this does not happen in future."></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucLabel ID="lblCommentsandAction" runat="server" Text="Please supply a brief narrative of remedial action taken"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucTextbox ID="UcTextbox46" runat="server" onkeyup="checkTextLengthOnKeyUp(this,1000);" TextMode="MultiLine" Width="700px"
                                Height="100px"></cc1:ucTextbox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucButton ID="UcButton22" runat="server" Text="Save" class="button" />
                        </td>
                    </tr>
                </table>
            </cc1:ucPanel>
            <cc1:ucPanel ID="pnlVEN015" runat="server" Width="70%" CssClass="fieldset-form">
                <table class="form-table" cellpadding="5" cellspacing="0">
                    <tr>
                        <td style="font-weight: bold; background-color: Yellow">
                            <cc1:ucLabel ID="lblVEN015" runat="server" Text="Goods have been delivered into a  wrong site, Office Depot have shipped these on to the correct location. Please advise of remedial action."></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucLabel ID="UcLabel83" runat="server" Text="Please supply a brief narrative of remedial action taken"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucTextbox ID="UcTextbox47" runat="server" onkeyup="checkTextLengthOnKeyUp(this,1000);" TextMode="MultiLine" Width="700px"
                                Height="100px"></cc1:ucTextbox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucButton ID="UcButton23" runat="server" Text="Save" class="button" />
                        </td>
                    </tr>
                </table>
            </cc1:ucPanel>
            <cc1:ucPanel ID="pnlVEN016" runat="server" Width="70%" CssClass="fieldset-form">
                <table class="form-table" cellpadding="5" cellspacing="0">
                    <tr>
                        <td style="font-weight: bold; background-color: Yellow">
                            <cc1:ucLabel ID="lblVEN016" runat="server" Text="Paperwork has been manually amended. Please ensure the invoice matches the adjustment. Please advise of remedial action to ensure this does not happen in the future."></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucLabel ID="UcLabel84" runat="server" Text="Please supply a brief narrative of remedial action taken"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucTextbox ID="UcTextbox48" runat="server" onkeyup="checkTextLengthOnKeyUp(this,1000);" TextMode="MultiLine" Width="700px"
                                Height="100px"></cc1:ucTextbox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucButton ID="UcButton24" runat="server" Text="Save" class="button" />
                        </td>
                    </tr>
                </table>
            </cc1:ucPanel>
            <cc1:ucPanel ID="pnlVEN017a" runat="server" Width="70%" CssClass="fieldset-form">
                <table class="form-table" cellpadding="5" cellspacing="0">
                    <tr>
                        <td colspan="2" style="font-weight: bold; background-color: Yellow">
                            <cc1:ucLabel ID="lblVEN017a" runat="server" Text="Goods have been delivered in an incorrect Pack Quantity. Please arrange collection of goods."></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <cc1:ucRadioButtonList ID="UcRadioButtonList19" runat="server" RepeatDirection="Horizontal"
                                Width="80%">
                                <asp:ListItem Text="Vendor to collect"></asp:ListItem>
                                <asp:ListItem Text="Office Depot to return"></asp:ListItem>
                            </cc1:ucRadioButtonList>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="padding-left: 280px">
                            <cc1:ucLabel ID="UcLabel85" runat="server" ForeColor="Red" Font-Size="Smaller" Text="Carriage cahrge to be applied"></cc1:ucLabel>
                            <br />
                            <cc1:ucLabel ID="UcLabel86" runat="server" ForeColor="Red" Font-Size="Smaller" Text="(minimum charge per pallete $$,per cartoon $$)"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucLabel ID="UcLabel87" runat="server" Text="Date Goods are to be collected"></cc1:ucLabel>
                        </td>
                        <td>
                            <cc1:ucTextbox ID="UcTextbox49" runat="server"></cc1:ucTextbox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucLabel ID="UcLabel88" runat="server" Text="Carrier Who will be collecting goods"></cc1:ucLabel>
                        </td>
                        <td>
                            <cc1:ucTextbox ID="UcTextbox50" runat="server"></cc1:ucTextbox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucLabel ID="UcLabel89" runat="server" Text="Collection Authorization #"></cc1:ucLabel>
                        </td>
                        <td>
                            <cc1:ucTextbox ID="UcTextbox51" runat="server"></cc1:ucTextbox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <cc1:ucLabel ID="UcLabel90" runat="server" Text="Please supply a brief narrative of remedial action taken"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <cc1:ucTextbox ID="UcTextbox52" runat="server" onkeyup="checkTextLengthOnKeyUp(this,1000);" TextMode="MultiLine" Width="700px"
                                Height="100px"></cc1:ucTextbox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <cc1:ucButton ID="UcButton25" runat="server" Text="Save" class="button" />
                        </td>
                    </tr>
                </table>
            </cc1:ucPanel>
            <cc1:ucPanel ID="pnlVEN017b" runat="server" Width="70%" CssClass="fieldset-form">
                <table class="form-table" cellpadding="5" cellspacing="0">
                    <tr>
                        <td colspan="2" style="font-weight: bold; background-color: Yellow">
                            <cc1:ucLabel ID="lblVEN017b" runat="server" Text="No purchase order has been supplied with this delivery so we are unable to accept. Please decide method of return."></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <cc1:ucRadioButtonList ID="UcRadioButtonList20" runat="server" RepeatDirection="Horizontal"
                                Width="80%">
                                <asp:ListItem Text="Vendor to collect"></asp:ListItem>
                                <asp:ListItem Text="Office Depot to return"></asp:ListItem>
                            </cc1:ucRadioButtonList>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="padding-left: 280px">
                            <cc1:ucLabel ID="UcLabel91" runat="server" ForeColor="Red" Font-Size="Smaller" Text="Carriage cahrge to be applied"></cc1:ucLabel>
                            <br />
                            <cc1:ucLabel ID="UcLabel92" runat="server" ForeColor="Red" Font-Size="Smaller" Text="(minimum charge per pallete $$,per cartoon $$)"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucLabel ID="UcLabel93" runat="server" Text="Collection Authorization #"></cc1:ucLabel>
                        </td>
                        <td>
                            <cc1:ucTextbox ID="UcTextbox53" runat="server"></cc1:ucTextbox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <cc1:ucLabel ID="UcLabel94" runat="server" Text="Please supply a brief narrative of remedial action taken"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <cc1:ucTextbox ID="UcTextbox54" runat="server" onkeyup="checkTextLengthOnKeyUp(this,1000);" TextMode="MultiLine" Width="700px"
                                Height="100px"></cc1:ucTextbox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <cc1:ucButton ID="UcButton26" runat="server" Text="Save" class="button" />
                        </td>
                    </tr>
                </table>
            </cc1:ucPanel>
            <cc1:ucPanel ID="pnlVEN018" runat="server" Width="70%" CssClass="fieldset-form">
                <table class="form-table" cellpadding="5" cellspacing="0">
                    <tr>
                        <td style="font-weight: bold; background-color: Yellow">
                            <cc1:ucLabel ID="lblVEN018" runat="server" Text="Goods with an incorrect pack size have been accepted on this occasion. Please advise of remedial action for future deliveries."></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucLabel ID="UcLabel95" runat="server" Text="Please supply a brief narrative of remedial action taken"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucTextbox ID="UcTextbox55" runat="server" onkeyup="checkTextLengthOnKeyUp(this,1000);" TextMode="MultiLine" Width="700px"
                                Height="100px"></cc1:ucTextbox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucButton ID="UcButton27" runat="server" Text="Save" class="button" />
                        </td>
                    </tr>
                </table>
            </cc1:ucPanel>
            <cc1:ucPanel ID="pnlVEN019" runat="server" Width="70%" CssClass="fieldset-form">
                <table class="form-table" cellpadding="5" cellspacing="0">
                    <tr>
                        <td style="font-weight: bold; background-color: Yellow">
                            <cc1:ucLabel ID="lblVEN019" runat="server" Text="Goods delivered in incorrect pack size. Office Depot have reworked them at a cost. Please advise of remedial action for future deliveries."></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucLabel ID="UcLabel96" runat="server" Text="Please supply a brief narrative of remedial action taken"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucTextbox ID="UcTextbox56" runat="server" onkeyup="checkTextLengthOnKeyUp(this,1000);" TextMode="MultiLine" Width="700px"
                                Height="100px"></cc1:ucTextbox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucButton ID="UcButton28" runat="server" Text="Save" class="button" />
                        </td>
                    </tr>
                </table>
            </cc1:ucPanel>
            <cc1:ucPanel ID="pnlVEN020" runat="server" Width="70%" CssClass="fieldset-form">
                <table class="form-table" cellpadding="5" cellspacing="0">
                    <tr>
                        <td style="font-weight: bold; background-color: Yellow">
                            <cc1:ucLabel ID="lblVEN020" runat="server" Text="There has been a Delivery Issue. Please Confirm Remedial Action."></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucLabel ID="UcLabel97" runat="server" Text="Please supply a brief narrative of remedial action taken"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucTextbox ID="UcTextbox57" runat="server" onkeyup="checkTextLengthOnKeyUp(this,1000);" TextMode="MultiLine" Width="700px"
                                Height="100px"></cc1:ucTextbox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucButton ID="UcButton29" runat="server" Text="Save" class="button" />
                        </td>
                    </tr>
                </table>
            </cc1:ucPanel>
            <cc1:ucPanel ID="pnlVEN021" runat="server" Width="70%" CssClass="fieldset-form">
                <table class="form-table" cellpadding="5" cellspacing="0">
                    <tr>
                        <td style="font-weight: bold; background-color: Yellow">
                            <cc1:ucLabel ID="lblVEN021" runat="server" Text="Goods failed required pallet specification and had to be reworked - Please confirm if vendor should be charged."></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucLabel ID="UcLabel98" runat="server" Text="Please supply a brief narrative of remedial action taken"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucTextbox ID="UcTextbox58" runat="server" onkeyup="checkTextLengthOnKeyUp(this,1000);" TextMode="MultiLine" Width="700px"
                                Height="100px"></cc1:ucTextbox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucButton ID="UcButton30" runat="server" Text="Save" class="button" />
                        </td>
                    </tr>
                </table>
            </cc1:ucPanel>
            <cc1:ucPanel ID="pnlVEN022" runat="server" Width="70%" CssClass="fieldset-form">
                <table class="form-table" cellpadding="5" cellspacing="0">
                    <tr>
                        <td style="font-weight: bold; background-color: Yellow">
                            <cc1:ucLabel ID="lblVEN022" runat="server" Text="There has been a premature invoice before goods have been delivered. Please advise of remedial action"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucLabel ID="UcLabel99" runat="server" Text="Please supply a brief narrative of remedial action taken"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucTextbox ID="UcTextbox59" runat="server" onkeyup="checkTextLengthOnKeyUp(this,1000);" TextMode="MultiLine" Width="700px"
                                Height="100px"></cc1:ucTextbox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucButton ID="UcButton31" runat="server" Text="Save" class="button" />
                        </td>
                    </tr>
                </table>
            </cc1:ucPanel>
            <cc1:ucPanel ID="pnlVEN023a" runat="server" Width="70%" CssClass="fieldset-form">
                <table class="form-table" cellpadding="5" cellspacing="0">
                    <tr>
                        <td colspan="2" style="font-weight: bold; background-color: Yellow">
                            <cc1:ucLabel ID="lblVEN023a" runat="server" Text="There has been a quality issue with this delivery and goods are to be returned to vendor. Please arrange collection of goods."></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <cc1:ucRadioButtonList ID="UcRadioButtonList21" runat="server" RepeatDirection="Horizontal"
                                Width="80%">
                                <asp:ListItem Text="Vendor to collect"></asp:ListItem>
                                <asp:ListItem Text="Office Depot to return"></asp:ListItem>
                            </cc1:ucRadioButtonList>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="padding-left: 280px">
                            <cc1:ucLabel ID="UcLabel100" runat="server" ForeColor="Red" Font-Size="Smaller" Text="Carriage cahrge to be applied"></cc1:ucLabel>
                            <br />
                            <cc1:ucLabel ID="UcLabel101" runat="server" ForeColor="Red" Font-Size="Smaller" Text="(minimum charge per pallete $$,per cartoon $$)"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucLabel ID="UcLabel102" runat="server" Text="Date Goods are to be collected"></cc1:ucLabel>
                        </td>
                        <td>
                            <cc1:ucTextbox ID="UcTextbox60" runat="server"></cc1:ucTextbox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucLabel ID="UcLabel103" runat="server" Text="Carrier Who will be collecting goods"></cc1:ucLabel>
                        </td>
                        <td>
                            <cc1:ucTextbox ID="UcTextbox61" runat="server"></cc1:ucTextbox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucLabel ID="UcLabel104" runat="server" Text="Collection Authorization #"></cc1:ucLabel>
                        </td>
                        <td>
                            <cc1:ucTextbox ID="UcTextbox62" runat="server"></cc1:ucTextbox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <cc1:ucLabel ID="UcLabel105" runat="server" Text="Please supply a brief narrative of remedial action taken"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <cc1:ucTextbox ID="UcTextbox63" runat="server" onkeyup="checkTextLengthOnKeyUp(this,1000);" TextMode="MultiLine" Width="700px"
                                Height="100px"></cc1:ucTextbox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <cc1:ucButton ID="UcButton32" runat="server" Text="Save" class="button" />
                        </td>
                    </tr>
                </table>
            </cc1:ucPanel>
            <cc1:ucPanel ID="pnlVEN023b" runat="server" Width="70%" CssClass="fieldset-form">
                <table class="form-table" cellpadding="5" cellspacing="0">
                    <tr>
                        <td colspan="2" style="font-weight: bold; background-color: Yellow">
                            <cc1:ucLabel ID="lblVEN023b" runat="server" Text="There has been a quality issue with this delivery and goods are to be returned to vendor. Please arrange collection of goods."></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <cc1:ucRadioButtonList ID="UcRadioButtonList22" runat="server" RepeatDirection="Horizontal"
                                Width="80%">
                                <asp:ListItem Text="Vendor to collect"></asp:ListItem>
                                <asp:ListItem Text="Office Depot to return"></asp:ListItem>
                            </cc1:ucRadioButtonList>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="padding-left: 280px">
                            <cc1:ucLabel ID="UcLabel106" runat="server" ForeColor="Red" Font-Size="Smaller" Text="Carriage cahrge to be applied"></cc1:ucLabel>
                            <br />
                            <cc1:ucLabel ID="UcLabel107" runat="server" ForeColor="Red" Font-Size="Smaller" Text="(minimum charge per pallete $$,per cartoon $$)"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucLabel ID="UcLabel110" runat="server" Text="Collection Authorization #"></cc1:ucLabel>
                        </td>
                        <td>
                            <cc1:ucTextbox ID="UcTextbox66" runat="server"></cc1:ucTextbox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <cc1:ucLabel ID="UcLabel111" runat="server" Text="Please supply a brief narrative of remedial action taken"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <cc1:ucTextbox ID="UcTextbox67" runat="server" onkeyup="checkTextLengthOnKeyUp(this,1000);" TextMode="MultiLine" Width="700px"
                                Height="100px"></cc1:ucTextbox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <cc1:ucButton ID="UcButton33" runat="server" Text="Save" class="button" />
                        </td>
                    </tr>
                </table>
            </cc1:ucPanel>
            <cc1:ucPanel ID="pnlVEN024" runat="server" Width="70%" CssClass="fieldset-form">
                <table class="form-table" cellpadding="5" cellspacing="0">
                    <tr>
                        <td style="font-weight: bold; background-color: Yellow">
                            <cc1:ucLabel ID="lblVEN024" runat="server" Text="There has been a quality issue with this delivery. Please advise of remedial action."></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucLabel ID="UcLabel108" runat="server" Text="Please supply a brief narrative of remedial action taken"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucTextbox ID="UcTextbox64" runat="server" onkeyup="checkTextLengthOnKeyUp(this,1000);" TextMode="MultiLine" Width="700px"
                                Height="100px"></cc1:ucTextbox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucButton ID="UcButton34" runat="server" Text="Save" class="button" />
                        </td>
                    </tr>
                </table>
            </cc1:ucPanel>
            <cc1:ucPanel ID="pnlVEN025" runat="server" Width="70%" CssClass="fieldset-form">
                <table class="form-table" cellpadding="5" cellspacing="0">
                    <tr>
                        <td style="font-weight: bold; background-color: Yellow">
                            <cc1:ucLabel ID="lblVEN025" runat="server" Text="Goods have been delivered in an incorrect Pack Quantity. Please advise of remedial action."></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucLabel ID="UcLabel109" runat="server" Text="Please supply a brief narrative of remedial action taken"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucTextbox ID="UcTextbox65" runat="server" onkeyup="checkTextLengthOnKeyUp(this,1000);" TextMode="MultiLine" Width="700px"
                                Height="100px"></cc1:ucTextbox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucButton ID="UcButton35" runat="server" Text="Save" class="button" />
                        </td>
                    </tr>
                </table>
            </cc1:ucPanel>
        </td>
    </tr>
</table>

﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="VEN011.aspx.cs" Inherits="VENAction_VEN011" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Office Depot</title>
    <script type="text/javascript">
        var baseUrl = "<%= ResolveUrl("~/") %>";
        function ResolveUrl(url) {
            if (url.indexOf("~/") == 0) {
                url = baseUrl + url.substring(2);
            }
            return url;
        }
    </script>
    <link rel="stylesheet" type="text/css" href="../../../../Css/style.css" />
    <link type="text/css" href="../../../../Css/ui-lightness/jquery-ui-1.8.5.custom.css"
        rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="../../../../Css/superfish.css" media="screen" />
    <script type="text/javascript" src="<%= ResolveClientUrl("../../../../Scripts/jquery-1.2.6.min.js")%>"></script>
    <script type="text/javascript" src="<%= ResolveClientUrl("../../../../Scripts/jquery-1.4.2.min.js")%>"></script>
    <script type="text/javascript" src="<%= ResolveClientUrl("../../../../Scripts/jquery-ui-1.8.5.custom.min.js")%>"></script>
    <script type="text/javascript" src="<%= ResolveClientUrl("../../../../Scripts/hoverIntent.js")%>"></script>
    <script type="text/javascript" src="<%= ResolveClientUrl("../../../../Scripts/superfish.js")%>"></script>
    <script type="text/javascript" src="<%= ResolveClientUrl("../../../../Scripts/ValidateInputs.js")%>"></script>
    <script type="text/javascript">

        // initialise plugins
        jQuery(function () {
            jQuery('ul.sf-menu').superfish();
        });
    </script>
    <script type="text/javascript">

        function showHide(showORhide) {
            var objtr1 = document.getElementById('<%=trDateGoodsAreToBeCollected.ClientID %>');
            var objtr2 = document.getElementById('<%=trCarrierWhoWillBeCollectingGoods.ClientID %>');
            var objtr3 = document.getElementById('<%=trCollectionAuthorisationNumber.ClientID %>');
            var objv2 = document.getElementById('<%=rfvCarrierWhoCollectGoodsRequired.ClientID %>');
            var OtherCarrier = document.getElementById('<%=rfvCarrierRequired.ClientID %>');

            if (showORhide == '1') {
                objtr1.style.display = '';
                objtr2.style.display = '';
                objtr3.style.display = '';
                ValidatorEnable(objv2, true);
                ValidatorEnable(document.getElementById('<%=rfvCollectionAuthorisationNumberRequired.ClientID%>'), true);
            }

            if (showORhide == '2') {
                objtr1.style.display = 'none';
                objtr2.style.display = 'none';
                objtr3.style.display = '';
                ValidatorEnable(objv2, false);
                ValidatorEnable(document.getElementById('<%=rfvCollectionAuthorisationNumberRequired.ClientID%>'), true);
            }

            if (showORhide == '3') {
                objtr1.style.display = 'none';
                objtr2.style.display = 'none';
                objtr3.style.display = 'none';
                ValidatorEnable(objv2, false);
                ValidatorEnable(document.getElementById('<%=rfvCollectionAuthorisationNumberRequired.ClientID%>'), false);
                ValidatorEnable(document.getElementById('<%=cusvPleaseSelectGoodsBeReturned.ClientID%>'), false);

            }


            //            objtr1.style.display = showORhide;
            //            objtr2.style.display = showORhide;
            //            if (objtr1.style.display == '' || objtr1.style.display == 'block') {
            //                ValidatorEnable(document.getElementById('<%=rfvCarrierWhoCollectGoodsRequired.ClientID%>'), true);
            //                ValidatorEnable(document.getElementById('<%=rfvCarrierWhoCollectGoodsRequired.ClientID%>'), true);
            //                ValidatorEnable(document.getElementById('<%=rfvCarrierRequired.ClientID%>'), true);
            //            }
            //            else {
            //                ValidatorEnable(document.getElementById('<%=rfvCarrierWhoCollectGoodsRequired.ClientID%>'), false);
            //                ValidatorEnable(document.getElementById('<%=rfvCarrierWhoCollectGoodsRequired.ClientID%>'), false);
            //                ValidatorEnable(document.getElementById('<%=rfvCarrierRequired.ClientID%>'), false);
            //            }
        }

        function PleaseSelectGoodsBeReturned(sender, args) {
            var objrdo1 = document.getElementById('<%=rdoVendorToCollect.ClientID %>');
            var objrdo2 = document.getElementById('<%=rdoOfficeDepotToReturn.ClientID %>');
            if (objrdo1.checked == false && objrdo2.checked == false) {
                args.IsValid = false;
            }
        }
        function GetLocation() {
            var Location = parent.document.getElementById('ctl00_ContentPlaceHolder1_txtLocation').value;
            document.getElementById('<%=hdfLocation.ClientID%>').value = Location;
        }
    </script>
    <script type="text/javascript">
        function HideShowButton() {
            if (checkValidationGroup("a")) {
                document.getElementById('<%=btnSave.ClientID%>').style.visibility = "hidden";
            }
            else {
                document.getElementById('<%=btnSave.ClientID%>').style.visibility = "visible";
                return false;
            }
        }
    </script>
    <script type="text/javascript" language="javascript">
        $(function () {
            $('#txtDateGoodsAreToBeCollected').datepicker({ showOtherMonths: true,
                selectOtherMonths: true, changeMonth: true, changeYear: true,
                minDate: new Date(2013, 0, 1),
                yearRange: '2013:+100',
                dateFormat: 'dd/mm/yy'
            });
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
    &nbsp;&nbsp;&nbsp;
    <cc1:ucPanel ID="pnlActionRequired" runat="server" Width="100%" CssClass="fieldset-form"
        GroupingText="Action Required">
        <table class="form-table" cellpadding="0" cellspacing="0" width="98%" style="margin: 2px;">
            <tr id="trIncorrectProduct" runat="server">
                <td style="font-weight: bold;"><asp:HiddenField ID="hdfLocation" runat="server" />
                    <cc1:ucLabel ID="lblVEN011a" runat="server" CssClass="action-required-heading" Style="text-align: center;"
                        Text="An incorrect product has been delivered and is to be returned. Please select method of return and advise of remedial action."></cc1:ucLabel>
                </td>
            </tr>
            <tr id="trQualityIssue" runat="server" style="display: none">
                <td style="font-weight: bold;">
                    <cc1:ucLabel ID="lblVEN011b" runat="server" CssClass="action-required-heading" Style="text-align: center;"
                        Text="There has been a quality issue with this delivery and goods are to be returned to vendor. Please arrange collection of goods."></cc1:ucLabel>
                </td>
            </tr>
            <tr id="trNoPaperwork" runat="server" style="display: none">
                <td style="font-weight: bold;">
                    <cc1:ucLabel ID="lblVEN011c" runat="server" CssClass="action-required-heading" Style="text-align: center;"
                        Text="No paperwork has been supplied with this delivery so we are unable to accept. Please decide method of return."></cc1:ucLabel>
                </td>
            </tr>
             <tr runat="server" id="trItemNotOnPo" style="display: none">
                    <td style="font-weight: bold;">
                        <cc1:ucLabel ID="lblItemNotOnPoVenMsg" runat="server" CssClass="action-required-heading" Style="text-align: center;width:946px;" ></cc1:ucLabel>
                    </td>
                </tr>
            <tr>
                <td>
                    <table class="form-table" cellpadding="0" cellspacing="0" width="98%" style="margin: 2px;">
                        <tr>
                            <td colspan="2">
                                <cc1:ucLabel ID="lblVEN001HowGoodsReturned" runat="server" Text="How should goods be returned?"></cc1:ucLabel>
                            </td>
                        </tr>
                        <tr>
                            <td width="15%">
                                <cc1:ucRadioButton ID="rdoVendorToCollect" runat="server" GroupName="goodsReturnedOption"
                                    Text="Vendor To Collect" onclick="javascript:showHide('1')" />
                            </td>
                            <td width="15%" id="tdOfficeDepotToReturn" runat="server">
                                <cc1:ucRadioButton ID="rdoOfficeDepotToReturn" runat="server" GroupName="goodsReturnedOption"
                                    Text="Office Depot To Return" onclick="javascript:showHide('2')" />
                                <asp:CustomValidator ID="cusvPleaseSelectGoodsBeReturned" runat="server" ErrorMessage="Please select goods be returned?"
                                    ClientValidationFunction="PleaseSelectGoodsBeReturned" Display="None" ValidationGroup="a">
                                </asp:CustomValidator>
                            </td>

                            <td width="60%">
                                <cc1:ucRadioButton ID="rdoDisposeOfGoods" runat="server" GroupName="goodsReturnedOption"
                                    Text="Dispose of goods" onclick="javascript:showHide('3')" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td colspan="2">
                                <cc1:ucLabel ID="lblCarriageChargeApplied" runat="server" Text="If you select Office Depot to return the goods, &lt;br/&gt;our local standard carton or pallet carriage costs will be applied and debited against your account."
                                    ForeColor="Red"></cc1:ucLabel>
                            </td>
                        </tr>
                    </table>
                    <table class="form-table" cellpadding="0" cellspacing="0" width="98%">
                        <tr runat="server" id="trDateGoodsAreToBeCollected">
                            <td width="29%">
                                <cc1:ucLabel ID="lblDateGoodsAreToBeCollected" runat="server" Text="Date Goods are to be collected"></cc1:ucLabel>
                            </td>
                            <td width="1%">
                                :
                            </td>
                            <td>
                                <asp:TextBox ID="txtDateGoodsAreToBeCollected" runat="server" ClientIDMode="Static"
                                    class="date" Width="100px" />
                            </td>
                        </tr>
                        <tr runat="server" id="trCarrierWhoWillBeCollectingGoods">
                            <td width="29%">
                                <cc1:ucLabel ID="lblCarrierWhoWillCollectingGoods" runat="server" Text="Carrier Who will be collecting Goods"></cc1:ucLabel>
                            </td>
                            <td width="1%">
                                :
                            </td>
                            <td width="70%">
                                <cc1:ucDropdownList ID="ddlCarrierWhoCollectGoods" runat="server" AutoPostBack="true"
                                    OnSelectedIndexChanged="ddlCarrierWhoCollectGoods_SelectedIndexChanged">
                                </cc1:ucDropdownList>
                                <asp:RequiredFieldValidator ID="rfvCarrierWhoCollectGoodsRequired" runat="server"
                                    InitialValue="0" ControlToValidate="ddlCarrierWhoCollectGoods" Display="None"
                                    ValidationGroup="a" ErrorMessage="Please enter the carrier who collects the goods.">
                                </asp:RequiredFieldValidator>
                                <cc1:ucTextbox runat="server" ID="txtOtherCarrier" Width="100px" />
                                <asp:RequiredFieldValidator ID="rfvCarrierRequired" runat="server" ControlToValidate="txtOtherCarrier"
                                    ValidationGroup="a" ErrorMessage="Carrier Required" Display="None"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr runat="server" id="trCollectionAuthorisationNumber">
                            <td width="29%">
                                <cc1:ucLabel ID="lblCollectionAuthorisationNumber" runat="server" Text="Collection Authorisation Number"></cc1:ucLabel>
                            </td>
                            <td width="1%">
                                :
                            </td>
                            <td>
                                <cc1:ucTextbox ID="txtCollectionAuthorisationNumber" runat="server" Width="100px"
                                    MaxLength="100"></cc1:ucTextbox>
                                <asp:RequiredFieldValidator ID="rfvCollectionAuthorisationNumberRequired" runat="server"
                                    ControlToValidate="txtCollectionAuthorisationNumber" ErrorMessage="Please enter the collection authorisation number."
                                    Display="None" ValidationGroup="a"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table cellpadding="0" cellspacing="0" class="form-table" style="margin: 2px;" width="98%">
                        <tr>
                            <td width="35%">
                                <cc1:ucLabel ID="lblVEN001Comment" runat="server" Text="Please supply a brief narrative of remedial action taken"></cc1:ucLabel>
                            </td>
                            <td width="65%">
                                :
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <cc1:ucTextbox ID="txtComment" runat="server" onkeyup="checkTextLengthOnKeyUp(this,1000);"
                                    TextMode="MultiLine" CssClass="textarea" Width="99%"></cc1:ucTextbox>
                                <asp:RequiredFieldValidator ID="rfvVEN001CommentRequired" runat="server" ErrorMessage="Please enter comments."
                                    ControlToValidate="txtComment" Display="None" ValidationGroup="a"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table class="form-table" cellpadding="0" cellspacing="0" width="98%" style="margin: 2px;">
                        <tr>
                            <td style="width: 80%">
                                <asp:ValidationSummary ID="vs1" runat="server" ValidationGroup="a" ShowSummary="false"
                                    ShowMessageBox="true" />
                            </td>
                            <td style="text-align: right; vertical-align: top;">
                                <cc1:ucButton ID="btnSave" runat="server" Text="Save" class="button" ValidationGroup="a"
                                OnClientClick="javascript:HideShowButton()"
                                    OnClick="btnSave_Click"  />
                                <input type="hidden" runat="server" id="hdnValue" value="1" />
                                <cc1:ucButton ID="btnBack" runat="server" Text="Back" class="button" OnClick="btnBack_Click" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </cc1:ucPanel>
    </form>
</body>
</html>

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.Discrepancy.LogDiscrepancy;
using BusinessLogicLayer.ModuleBAL.Discrepancy.LogDiscrepancy;
using System.Data;
using WebUtilities;
using System.IO;

public partial class ModuleUI_Discrepancy_UserControl_VENAction_VEN027 : CommonPage
{
    string fileUploadError1 = WebCommon.getGlobalResourceValue("fileUploadError1");
    string WrongFile = WebCommon.getGlobalResourceValue("WrongFile");
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
       // btnSave.Enabled = false;
        DISLog_WorkFlowBE oDISLog_WorkFlowBE = new DISLog_WorkFlowBE();
        DISLog_WorkFlowBAL oDISLog_WorkFlowBAL = new DISLog_WorkFlowBAL();


        oDISLog_WorkFlowBE.Action = "InsertUpdateWorkflowActions";
        oDISLog_WorkFlowBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
        oDISLog_WorkFlowBE.StageCompleted = chkStageComplete.Checked ? true : false;

        oDISLog_WorkFlowBE.StageCompletedForWhom = "VEN028_StageCompleted";
        oDISLog_WorkFlowBE.Comment = txtComments.Text;
        oDISLog_WorkFlowBE.Decision = "";
        //update the work flow action table each time for the comments and other fields( if stage is completed)
        oDISLog_WorkFlowBE.DiscrepancyWorkFlowID = Convert.ToInt32(GetQueryStringValue("DisWFlowID").ToString());

        //insert or update the record according to the stage completed status
        int? iResult = oDISLog_WorkFlowBAL.addWorkFlowActionsBAL(oDISLog_WorkFlowBE);

        DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
        DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();
        //For getting Inventory Action Log date and then update VendorActionElaspedTime in Trn_DiscrepancyLog Table
        oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID"));
        oDiscrepancyBE.Action = "GetInventoryLogDate";
        oDiscrepancyBE.DiscrepancyWorkFlowID = Convert.ToInt32(GetQueryStringValue("DisWFlowID"));
        DataTable dtAllDetails = oDiscrepancyBAL.GetAllDetailsOfWorkFlowBAL(oDiscrepancyBE);
        DateTime DtInventoryLogDate = DateTime.Now;

        if (dtAllDetails != null && dtAllDetails.Rows.Count > 0)
        {
            DtInventoryLogDate = Convert.ToDateTime(dtAllDetails.Rows[0]["LogDate"]);
        }
        oDiscrepancyBE.VendorActionElapsedTime = (decimal?)DateTime.Now.Subtract(DtInventoryLogDate).TotalDays;
        oDiscrepancyBE.Action = "UpdateVendorActionElapsedTime";
        oDiscrepancyBAL.UpdateVendorActionElapsedTimeBAL(oDiscrepancyBE);


        //now insert the work flow HTML according to the action taken only if the stage is completed
        //now insert the work flow HTML according to the action taken only if the stage is completed
        WorkflowHTML oWorkflowHTML = new WorkflowHTML();
        if (rdoVEN028Text1.Checked)
        {
            string fileName1 = string.Empty;
            if (fileupload.HasFile == false)
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('" + fileUploadError1 + "')", true);
                return;
            }
            
            if (fileupload.HasFile)
            {
                if (fileupload.FileName.Split('.').ElementAt(1).ToLower() == "jpg" || fileupload.FileName.Split('.').ElementAt(1).ToLower() == "bmp"  || fileupload.FileName.Split('.').ElementAt(1).ToLower() == "pdf")
                {
                    fileName1 = fileupload.FileName;
                    fileupload.SaveAs(Path.Combine(Server.MapPath("~/images/discrepancy"), fileName1));

                    oDISLog_WorkFlowBE.Action = "InvoiceInsertDocInformation";
                    oDISLog_WorkFlowBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
                    oDISLog_WorkFlowBE.DocPath = fileName1;
                    oDISLog_WorkFlowBE.Doctype = "Credit";
                    oDISLog_WorkFlowBAL.addWorkFlowInvoiceQueryActionsBAL(oDISLog_WorkFlowBE);

                    oDiscrepancyBE.GINHTML = oWorkflowHTML.function3("White", null, "", "");
                    oDiscrepancyBE.GINActionRequired = false;

                    oDiscrepancyBE.INVHTML = oWorkflowHTML.function3("White", null, "", "");
                    oDiscrepancyBE.INVActionRequired = false;

                    oDiscrepancyBE.APHTML = oWorkflowHTML.function13(pColor: "Blue", pDate: DateTime.Now, SentTo: WebCommon.getGlobalResourceValue("AccountsPayable"), DiscrePancyTextBottom: WebCommon.getGlobalResourceValue("VEN028WOrkflow"));

                    oDiscrepancyBE.APActionRequired = true;
                    oDiscrepancyBE.APUserControl = "ACP007";

                    oDiscrepancyBE.VENHTML = oWorkflowHTML.function13(pColor: "Yellow", pDate: DateTime.Now,
                        pUserName: Convert.ToString(Session["UserName"]), pAction: WebCommon.getGlobalResourceValue("VEN028WOrkflow1"), pComments: txtComments.Text);
                    oDiscrepancyBE.VenActionRequired = false;



                    oDiscrepancyBE.Action = "InsertHTML";

                    oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
                    oDiscrepancyBE.LoggedDateTime = DateTime.Now;
                    oDiscrepancyBE.LevelNumber = 1;
                    oDiscrepancyBE.ActionTakenBy = "VEN";
                    oDiscrepancyBE.APActionCategory = "AR";  //Action Required
                    int? iResult1 = oDiscrepancyBAL.addWorkFlowHTMLsBAL(oDiscrepancyBE);
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('" + WrongFile + "')", true);
                    return;
                }
            }
        }
        else
        {
            oDiscrepancyBE.GINHTML = oWorkflowHTML.function3("White", null, "", "");
            oDiscrepancyBE.GINActionRequired = false;

            oDiscrepancyBE.INVHTML = oWorkflowHTML.function3("White", null, "", "");
            oDiscrepancyBE.INVActionRequired = false;

            oDiscrepancyBE.APHTML = oWorkflowHTML.function3("White", null, "", "");
            oDiscrepancyBE.APActionRequired = false;


            oDiscrepancyBE.VENHTML = oWorkflowHTML.function13(pColor: "Yellow", pDate: DateTime.Now,
                   pUserName: Convert.ToString(Session["UserName"]), pAction: WebCommon.getGlobalResourceValue("VEN028WOrkflow2"), pComments: txtComments.Text);
            oDiscrepancyBE.VenActionRequired = false;

            oDiscrepancyBE.MEDHTML = oWorkflowHTML.function14(pColor: "blue", DiscrePancyText: WebCommon.getGlobalResourceValue("MED001Workflow"));
            oDiscrepancyBE.MEDActionRequired = true;
            oDiscrepancyBE.MEDUserControl = "MED001";

            oDiscrepancyBE.Action = "InsertHTML";

            oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
            oDiscrepancyBE.LoggedDateTime = DateTime.Now;
            oDiscrepancyBE.LevelNumber = 1;
            int? iResult1 = oDiscrepancyBAL.addWorkFlowHTMLsBAL(oDiscrepancyBE);
        }
     

        


        RedirectToPage(GetQueryStringValue("PreviousPage"));
    }
 


    private void InsertHTML()
    {
        DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
        DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();

        WorkflowHTML oWorkflowHTML = new WorkflowHTML();
        oDiscrepancyBE.GINHTML = oWorkflowHTML.function3("White", DateTime.Now, txtComments.Text.Trim());
        oDiscrepancyBE.GINActionRequired = false;

        oDiscrepancyBE.Action = "InsertHTML";
        oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
        oDiscrepancyBE.LoggedDateTime = DateTime.Now;
        oDiscrepancyBE.LevelNumber = 1;

        int? iResult = oDiscrepancyBAL.addWorkFlowHTMLsBAL(oDiscrepancyBE);
    }
    protected void btnBack_Click(object sender, EventArgs e)
    {
        //Redirecting to Previous Page
        RedirectToPage(GetQueryStringValue("PreviousPage"));
    }
}
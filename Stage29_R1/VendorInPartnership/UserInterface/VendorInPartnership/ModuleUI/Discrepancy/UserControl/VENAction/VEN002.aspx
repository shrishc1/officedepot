﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="VEN002.aspx.cs" Inherits="VENAction_VEN002"
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Office Depot</title>
    <link rel="stylesheet" type="text/css" href="../../../../Css/style.css" />
    <link type="text/css" href="../../../../Css/ui-lightness/jquery-ui-1.8.5.custom.css"
        rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="../../../../Css/superfish.css" media="screen" />
    <script type="text/javascript" src="<%= ResolveClientUrl("../../../../Scripts/jquery-1.2.6.min.js")%>"></script>
    <script type="text/javascript" src="<%= ResolveClientUrl("../../../../Scripts/jquery-1.4.2.min.js")%>"></script>
    <script type="text/javascript" src="<%= ResolveClientUrl("../../../../Scripts/jquery-ui-1.8.5.custom.min.js")%>"></script>
    <script type="text/javascript" src="<%= ResolveClientUrl("../../../../Scripts/hoverIntent.js")%>"></script>
    <script type="text/javascript" src="<%= ResolveClientUrl("../../../../Scripts/superfish.js")%>"></script>
    <script type="text/javascript" src="<%= ResolveClientUrl("../../../../Scripts/ValidateInputs.js")%>"></script>
    <script type="text/javascript">
        function VEN001ActionRequired(sender, args) {
            var objOverDeliveredQReason = document.getElementById('<%=rdoOverDeliveredQReason.ClientID %>');
            var selectedvalue = $("#" + objOverDeliveredQReason.id + " input:radio:checked").val();
            if (selectedvalue == null && selectedvalue == '') {
                args.IsValid = true;
            }
        }

         function HideShowButton() {
          if (checkValidationGroup("a")) {
                document.getElementById('<%=btnSave.ClientID%>').style.visibility = "hidden";
            }
            else {
                document.getElementById('<%=btnSave.ClientID%>').style.visibility = "visible";
                return false;
            }
        }
    </script>
</head>
<body>
    <form id="form2" runat="server">
    <div>
        <cc1:ucPanel ID="pnlActionRequired" runat="server" Width="100%" CssClass="fieldset-form"
            GroupingText="Action Required">
            <table class="form-table" cellpadding="0" cellspacing="0" width="98%" style="margin: 2px;">
                <tr>
                    <td style="font-weight: bold;">
                        <cc1:ucLabel ID="lblVEN002Desc" runat="server" CssClass="action-required-heading"
                            Style="text-align: center;" Text="Please select the reason the goods were over delivered and confirm of remedial action "></cc1:ucLabel>
                        <br />
                    </td>
                </tr>
                <tr>
                    <td>
                        <table class="form-table" cellpadding="0" cellspacing="0" width="98%" style="margin: 2px;">
                            <tr>
                                <td width="25%">
                                    <cc1:ucLabel ID="lblReasonGoodsOverDelivered" runat="server" Text="Reason for goods being over delivered"></cc1:ucLabel>
                                </td>
                                <td width="75%">
                                    :
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <cc1:ucRadioButtonList ID="rdoOverDeliveredQReason" runat="server" RepeatDirection="Horizontal"
                                        Width="45%">
                                        <asp:ListItem Text="Rounded Order Quantity" Value="RQ"></asp:ListItem>
                                        <asp:ListItem Text="Open Order Quantity" Value="OQ"></asp:ListItem>
                                        <asp:ListItem Text="Other" Value="OT"></asp:ListItem>
                                    </cc1:ucRadioButtonList>
                                    <asp:CustomValidator ID="cusvOverDeliveredQReasonRequired" runat="server" ErrorMessage="Please select a reason"
                                        ControlToValidate="rdoOverDeliveredQReason" ClientValidationFunction="VEN001ActionRequired"
                                        ValidationGroup="a" Display="None"></asp:CustomValidator>
                                    <asp:RequiredFieldValidator ID="rfvOverDeliveredQReasonRequired" runat="server" ErrorMessage="Please select a reason"
                                        ControlToValidate="rdoOverDeliveredQReason" ValidationGroup="a" Display="None"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table class="form-table" cellpadding="0" cellspacing="0" width="98%" style="margin: 2px;">
                            <tr>
                                <td width="35%">
                                    <cc1:ucLabel ID="lblVEN012RemadialAction" runat="server" Text="Please supply a brief narrative of remedial action taken"></cc1:ucLabel>
                                </td>
                                <td width="65%">
                                    :
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <cc1:ucTextbox ID="txtComments" runat="server" CssClass="textarea" TextMode="MultiLine"
                                        Width="99%"></cc1:ucTextbox>
                                    <asp:RequiredFieldValidator ID="rfvCommentRequired" runat="server" ErrorMessage="Please enter comments."
                                        ControlToValidate="txtComments" Display="None" ValidationGroup="a"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table class="form-table" cellpadding="0" cellspacing="0" width="98%">
                            <tr>
                                <td width="80%">
                                    <asp:ValidationSummary ID="vs1" runat="server" ValidationGroup="a" ShowSummary="false"
                                        ShowMessageBox="true" />
                                </td>
                                <td style="text-align: right; vertical-align: text-top;">
                                    <cc1:ucButton ID="btnSave" runat="server" Text="Save" class="button" ValidationGroup="a"
                                    OnClientClick="javascript:HideShowButton()"
                                        OnClick="btnSave_Click" />
                                    <cc1:ucButton ID="btnBack" runat="server" Text="Back" class="button" OnClick="btnBack_Click" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </cc1:ucPanel>
    </div>
    </form>
</body>
</html>

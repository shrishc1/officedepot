﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI;
using BusinessEntities.ModuleBE.Appointment.CountrySetting;
using BusinessEntities.ModuleBE.Discrepancy.LogDiscrepancy;
using BusinessLogicLayer.ModuleBAL.Appointment.CountrySetting;
using BusinessLogicLayer.ModuleBAL.Discrepancy.LogDiscrepancy;
using Utilities;
using WebUtilities;
using System.Web.UI.WebControls;


public partial class VENAction_VEN001 : CommonPage
{

    #region Events

    string strOpenOrderQuantity = WebCommon.getGlobalResourceValue("VEN001OpenOrderQuantity");
    string strRoundedOrderQuantity = WebCommon.getGlobalResourceValue("VEN001RoundedOrderQuantity");
    string strOther = WebCommon.getGlobalResourceValue("VEN001Other");

    protected void Page_Load(object sender, EventArgs e)
    {
        //if (rdoDisposeOfGoods.Checked)
        //{
        //    rfvCarrierWhoCollectGoodsRequired.Enabled = false;
        //    rfvCarrierRequired.Enabled = false;
        //    trCollectionAuthorisationNumber.Style.Add("display", "none");
        //    trDateGoodsAreToBeCollected.Style.Add("display", "none");
        //    trCarrierWhoWillBeCollectingGoods.Style.Add("display", "none");
        //}

        if (!IsPostBack)
        {
            GetIsODToReturn();

            txtOtherCarrier.Attributes.Add("style", "display:none;");
            rfvCarrierRequired.Enabled = false;
            BindCarrier();
            txtOtherCarrier.Text = string.Empty;
            txtOtherCarrier.Attributes.Add("style", "display:none;");
            rfvCarrierRequired.Enabled = false;
            rfvCarrierRequired.Visible = false;

            //rdoVendorToCollect.Checked = true;
            //if (rdoVendorToCollect.Checked == true)
            //{
            //    trDateGoodsAreToBeCollected.Style["display"] = "";
            //    trCarrierWhoWillBeCollectingGoods.Style["display"] = "";
            //}
            //else
            //{
            //    trDateGoodsAreToBeCollected.Style["display"] = "none";
            //    trCarrierWhoWillBeCollectingGoods.Style["display"] = "none";
            //}
            txtDateGoodsAreToBeCollected.Text = DateTime.Now.ToString("dd/MM/yyyy");
            txtDateGoodsAreToBeCollected.Attributes.Add("readonly", "readonly");
        }
    }

    private void GetIsODToReturn()
    {
        DiscrepancyBE oNewDiscrepancyBE = new DiscrepancyBE();
        DiscrepancyBAL oNewDiscrepancyBAL = new DiscrepancyBAL();
        oNewDiscrepancyBE.Action = "GetIsOdToReturn";
        oNewDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());

        DataSet ds = oNewDiscrepancyBAL.GetIsodToReturnBAL(oNewDiscrepancyBE);
        if (ds.Tables[0].Rows.Count > 0)
        {
            if (ds.Tables[0].Rows[0]["IsODToReturn"] != null)
            {
                if (Convert.ToBoolean(ds.Tables[0].Rows[0]["IsODToReturn"]) == true)
                {
                    tdOfficeDepotToReturn.Visible = true;
                    lblCarriageChargeApplied.Visible = true;
                }
                else
                {
                    tdOfficeDepotToReturn.Visible = false;
                    lblCarriageChargeApplied.Visible = false;
                }

            }
        }
    }
    

    protected void btnSave_Click(object sender, EventArgs e)
    {
        bool IsGoodsBeingOverDeliveredChecked = clsDiscrepancy.ISRadioButtonChecked(rdoVEN001RoundedOrderQuantity, rdoVEN001OpenOrderQuantity, rdoVEN001Other);
        if (IsGoodsBeingOverDeliveredChecked == false)
        {
            string PleaseSelectTheAnyReasonForGoodsMessage = WebCommon.getGlobalResourceValue("PleaseSelectTheAnyReasonForGoods");
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('" + PleaseSelectTheAnyReasonForGoodsMessage + "')", true);
            return;
        }

        bool IsHowGoodsBeReturnedChecked = clsDiscrepancy.ISRadioButtonChecked(rdoVendorToCollect, rdoOfficeDepotToReturn, rdoDisposeOfGoods);
        if (IsHowGoodsBeReturnedChecked == false)
        {
            string PleaseSelectGoodsBeReturnedMessage = WebCommon.getGlobalResourceValue("PleaseSelectGoodsBeReturned");
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('" + PleaseSelectGoodsBeReturnedMessage + "')", true);
            return;
        }
        
        /*
            if the vendor is comming to collect then goto step-3b
         *  if they want OD to return the goods then goto step-3a
         */
        //collection date must be future date
        btnSave.Enabled = false;
        if (rdoVendorToCollect.Checked)
        {
            DateTime dt = Common.GetMM_DD_YYYY(txtDateGoodsAreToBeCollected.Text);
            if (dt < DateTime.Now.Date)
            {
                string saveMessage = WebCommon.getGlobalResourceValue("NotDateValidMessage");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('" + saveMessage + "')", true);
                return;
            }
        }

        DISLog_WorkFlowBE oDISLog_WorkFlowBE = new DISLog_WorkFlowBE();
        DISLog_WorkFlowBAL oDISLog_WorkFlowBAL = new DISLog_WorkFlowBAL();
        if (GetQueryStringValue("disLogID") != null && Convert.ToString(GetQueryStringValue("disLogID")) != "")
        {


            // save in DIS_WorkflowActions table
            oDISLog_WorkFlowBE.Action = "InsertUpdateWorkflowActions";
            oDISLog_WorkFlowBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID"));

            oDISLog_WorkFlowBE.VEN001_ReasonForGoodsOverDelivered = rdoVEN001RoundedOrderQuantity.Checked == true ? "RQ" : (rdoVEN001OpenOrderQuantity.Checked == true ? "OQ" : (rdoVEN001Other.Checked == true ? "OT" : ""));

            if (rdoVendorToCollect.Checked)
            {
                oDISLog_WorkFlowBE.VEN001_HowGoodsReturned = "V";
                oDISLog_WorkFlowBE.VEN001_DateGoodsAreToBeCollected = Common.GetMM_DD_YYYY(txtDateGoodsAreToBeCollected.Text);
                if (ddlCarrierWhoCollectGoods.SelectedItem != null)
                    oDISLog_WorkFlowBE.CarrierWhoWillCollectGoods = Convert.ToInt32(ddlCarrierWhoCollectGoods.SelectedItem.Value);
                oDISLog_WorkFlowBE.CarrierOther = txtOtherCarrier.Text;
                oDISLog_WorkFlowBE.Decision = "Vendor To Collect";
            }
            else if (rdoOfficeDepotToReturn.Checked)
            {
                oDISLog_WorkFlowBE.VEN001_HowGoodsReturned = "O";
                oDISLog_WorkFlowBE.Decision = "Office Depot To Return";
            }
            else if (rdoDisposeOfGoods.Checked)
            {
                oDISLog_WorkFlowBE.Decision = "Dispose of goods";
            }

            oDISLog_WorkFlowBE.VEN001_CollectionAuthorisationNumber = txtCollectionAuthorisationNumber.Text.Trim();
            oDISLog_WorkFlowBE.Comment = txtVEN001Comment.Text.Trim();
            oDISLog_WorkFlowBE.StageCompleted = true;
            oDISLog_WorkFlowBE.StageCompletedForWhom = "VEN001_StageCompleted";

            //update the work flow action table each time for the comments and other fields( if stage is completed)
            oDISLog_WorkFlowBE.DiscrepancyWorkFlowID = Convert.ToInt32(GetQueryStringValue("DisWFlowID").ToString());

            int? result = oDISLog_WorkFlowBAL.addWorkFlowActionsBAL(oDISLog_WorkFlowBE);

            // save in RPT_DiscrepancyWorkflow table

            DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
            DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();
            WorkflowHTML oWorkflowHTML = new WorkflowHTML();
            oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID"));


            //For getting Inventory Action Log date and then update VendorActionElaspedTime in Trn_DiscrepancyLog Table
            oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID"));
            oDiscrepancyBE.Action = "GetInventoryLogDate";
            oDiscrepancyBE.DiscrepancyWorkFlowID = Convert.ToInt32(GetQueryStringValue("DisWFlowID"));
            DataTable dtAllDetails = oDiscrepancyBAL.GetAllDetailsOfWorkFlowBAL(oDiscrepancyBE);
            DateTime DtInventoryLogDate = DateTime.Now;

            if (dtAllDetails != null && dtAllDetails.Rows.Count > 0)
            {
                DtInventoryLogDate = Convert.ToDateTime(dtAllDetails.Rows[0]["LogDate"]);
            }
            oDiscrepancyBE.VendorActionElapsedTime = (decimal?)DateTime.Now.Subtract(DtInventoryLogDate).TotalDays;
            oDiscrepancyBE.Action = "UpdateVendorActionElapsedTime";
            oDiscrepancyBAL.UpdateVendorActionElapsedTimeBAL(oDiscrepancyBE);

            var varddlCarrierText = string.Empty;
            if (ddlCarrierWhoCollectGoods.SelectedItem != null)
                varddlCarrierText = ddlCarrierWhoCollectGoods.SelectedItem.Text;

            if (rdoVendorToCollect.Checked)
            {

                /*if AP has taken the action then print the following HTML*/
                //if (bAPActionRequired == false) {
                oDiscrepancyBE.GINHTML = oWorkflowHTML.function1(DateTime.Now.Date, null,
                                                                    Common.GetMM_DD_YYYY(txtDateGoodsAreToBeCollected.Text), "Blue", "", "", "",
                                                                    @"Collection has been organised by vandor.Please ensure 
                                                                        the goods are collected as per the vendor's instruction", "", "",
                                                                    varddlCarrierText,
                                                                    txtCollectionAuthorisationNumber.Text,
                                                                    "", "", "");
                oDiscrepancyBE.GINUserControl = "GIN002";
                oDiscrepancyBE.GINActionRequired = true;
                oDiscrepancyBE.GINActionCategory = "AR";  //Action Required

                oDiscrepancyBE.INVHTML = oWorkflowHTML.function3("White", null, "", "");
                oDiscrepancyBE.INVActionRequired = false;

                oDiscrepancyBE.APHTML = oWorkflowHTML.function3("White", null, "", "");
                oDiscrepancyBE.APActionRequired = false;

                string strOversReason = rdoVEN001RoundedOrderQuantity.Checked == true ? strRoundedOrderQuantity :
                    (rdoVEN001OpenOrderQuantity.Checked == true ? strOpenOrderQuantity : (rdoVEN001Other.Checked == true ? strOther : string.Empty));
                //overs - 3b
                oDiscrepancyBE.VENHTML = oWorkflowHTML.function1(DateTime.Now.Date,
                    Common.GetMM_DD_YYYY(txtDateGoodsAreToBeCollected.Text),
                    null,
                    "Yellow",
                    string.Empty,
                    "Goods to be collected",
                    UIUtility.GetODVendorUserName(Convert.ToString(Session["Role"]), Convert.ToString(Session["UserName"]), Convert.ToString(Session["LoginID"])),
                    txtVEN001Comment.Text,
                    string.Empty,
                    string.Empty,
                    varddlCarrierText,
                    txtCollectionAuthorisationNumber.Text,
                    string.Empty,
                    string.Empty,
                    string.Empty,
                    string.Empty,
                    string.Empty,
                    string.Empty,
                    string.Empty,
                    string.Empty,
                    string.Empty,
                    string.Empty,
                    string.Empty,
                    string.Empty,
                    string.Empty,
                    null,
                    string.Empty,
                    strOversReason
                    );

                oDiscrepancyBE.VenActionRequired = false;
                //}
                ////AP's action is pending the print only Vendor's HTML
                //else if (dtAllDetails.Rows[0]["APActionRequired"] != null && Convert.ToInt32(dtAllDetails.Rows[0]["APActionRequired"]) == 1) {

                //    oDiscrepancyBE.GINHTML = oWorkflowHTML.function3("White", null, "", "");
                //    oDiscrepancyBE.GINActionRequired = false;

                //    oDiscrepancyBE.INVHTML = oWorkflowHTML.function3("White", null, "", "");
                //    oDiscrepancyBE.INVActionRequired = false;

                //    oDiscrepancyBE.APHTML = oWorkflowHTML.function3("White", null, "", "");
                //    oDiscrepancyBE.APActionRequired = false;

                //    oDiscrepancyBE.VENHTML = oWorkflowHTML.function1(DateTime.Now.Date, DateTime.Now.Date,
                //                                                        Common.GetDD_MM_YYYYDatetime(txtDateGoodsAreToBeCollected.Text),
                //                                                        "Yellow", "", "Goods to be collected", Session["UserName"].ToString(),
                //                                                        txtVEN001Comment.Text, "", "",
                //                                                        ucTxtCarrierWhoCollectGoods.SelectedItem.Text,
                //                                                        txtCollectionAuthorisationNumber.Text, "", "", "");
                //    oDiscrepancyBE.VenActionRequired = false;
                //}
            }
            else if (rdoOfficeDepotToReturn.Checked)
            {

                /* if AP has taken the action then print the following HTML */
                //if (bAPActionRequired == false) 
                {
                    oDiscrepancyBE.GINHTML = oWorkflowHTML.function2(DateTime.Now.Date, null, "Blue", "Goods-In", ""
                                                                    , @"Please arrange for goods to be returned 
                                                                    to the vendor and confirm the carriage charge.");

                    oDiscrepancyBE.GINUserControl = "GIN003";
                    oDiscrepancyBE.GINActionRequired = true;
                    oDiscrepancyBE.GINActionCategory = "AR";  //Action Required

                    oDiscrepancyBE.INVHTML = oWorkflowHTML.function3("White", null, "", "");
                    oDiscrepancyBE.INVActionRequired = false;

                    oDiscrepancyBE.APHTML = oWorkflowHTML.function3("White", null, "", "");
                    oDiscrepancyBE.APActionRequired = false;

                    string strOversReason = rdoVEN001RoundedOrderQuantity.Checked == true ? strRoundedOrderQuantity :
                    (rdoVEN001OpenOrderQuantity.Checked == true ? strOpenOrderQuantity : (rdoVEN001Other.Checked == true ? strOther : string.Empty));

                    oDiscrepancyBE.VENHTML = oWorkflowHTML.function1(DateTime.Now.Date,
                        null,
                        null,
                        "Yellow",
                        string.Empty,
                        string.Empty,
                        UIUtility.GetODVendorUserName(Convert.ToString(Session["Role"]), Convert.ToString(Session["UserName"]), Convert.ToString(Session["LoginID"])),
                        txtVEN001Comment.Text,
                        string.Empty,
                        string.Empty,
                        string.Empty,
                        txtCollectionAuthorisationNumber.Text,
                        string.Empty,
                        string.Empty,
                        string.Empty,
                        "Office Depot to return",
                        string.Empty,
                        string.Empty,
                        string.Empty,
                        string.Empty,
                        string.Empty,
                        string.Empty,
                        string.Empty,
                        string.Empty,
                        string.Empty,
                        null,
                        string.Empty,
                        strOversReason
                        );
                    oDiscrepancyBE.VenActionRequired = false;
                }
                //AP's action is pending the print only Vendor's HTML
                //else if (dtAllDetails.Rows[0]["APActionRequired"] != null && Convert.ToInt32(dtAllDetails.Rows[0]["APActionRequired"]) == 1) {
                //else {
                //    oDiscrepancyBE.GINHTML = oWorkflowHTML.function3("White", null, "", "");
                //    oDiscrepancyBE.GINActionRequired = false;

                //    oDiscrepancyBE.INVHTML = oWorkflowHTML.function3("White", null, "", "");
                //    oDiscrepancyBE.INVActionRequired = false;

                //    oDiscrepancyBE.APHTML = oWorkflowHTML.function3("White", null, "", "");
                //    oDiscrepancyBE.APActionRequired = false;

                //    oDiscrepancyBE.VENHTML = oWorkflowHTML.function1(DateTime.Now.Date, null, null, "Yellow",
                //                                                        "", "Office Depot to return", Session["UserName"].ToString(),
                //                                                        txtVEN001Comment.Text, "", "",
                //                                                        "", txtCollectionAuthorisationNumber.Text);
                //    oDiscrepancyBE.VenActionRequired = false;
                //}
            }

            else if (rdoDisposeOfGoods.Checked)
            {
                oDiscrepancyBE.GINHTML = oWorkflowHTML.function2(DateTime.Now.Date, null, "Blue", "Goods-In", ""
                                                                    , @"Vendor has informed that damaged goods should be scrapped. Please confirm goods have been disposed off.");

                oDiscrepancyBE.GINUserControl = "GIN004";
                oDiscrepancyBE.GINActionRequired = true;
                oDiscrepancyBE.GINActionCategory = "RA";  //Remedial Action
                oDiscrepancyBE.INVHTML = oWorkflowHTML.function3("White", null, "", "");
                oDiscrepancyBE.INVActionRequired = false;

                oDiscrepancyBE.APHTML = oWorkflowHTML.function3("White", null, "", "");
                oDiscrepancyBE.APActionRequired = false;

                oDiscrepancyBE.VENHTML = oWorkflowHTML.function1(DateTime.Now.Date, null, null, "Yellow", "", "",
                    //Session["UserName"].ToString(), 
                    UIUtility.GetODVendorUserName(Convert.ToString(Session["Role"]), Convert.ToString(Session["UserName"]), Convert.ToString(Session["LoginID"])),
                    txtVEN001Comment.Text, "", "", "", txtCollectionAuthorisationNumber.Text, "", "", "", "Dispose of goods");
                oDiscrepancyBE.VenActionRequired = false;
            }
            oDiscrepancyBE.LevelNumber = 1;
            oDiscrepancyBE.Action = "InsertHTML";
            oDiscrepancyBE.ActionTakenBy = "VEN";
            oDiscrepancyBE.LoggedDateTime = DateTime.Now;
            oDiscrepancyBAL.addWorkFlowHTMLsBAL(oDiscrepancyBE);
        }
        //Redirecting to Previous Page
        RedirectToPage(GetQueryStringValue("PreviousPage"));
    }

    #endregion

    #region Methods

    private void BindCarrier()
    {
        MASCNT_CarrierBE oMASCNT_CarrierBE = new MASCNT_CarrierBE();
        APPCNT_CarrierBAL oMASCNT_CarrierBAL = new APPCNT_CarrierBAL();

        oMASCNT_CarrierBE.Action = "ShowAllByVDRNumber";
        oMASCNT_CarrierBE.Discrepancy = new BusinessEntities.ModuleBE.Discrepancy.LogDiscrepancy.DiscrepancyBE();
        oMASCNT_CarrierBE.Discrepancy.VDRNo = GetQueryStringValue("VDRNo") != null ? GetQueryStringValue("VDRNo").ToString() : null;

        List<MASCNT_CarrierBE> lstCarrier = oMASCNT_CarrierBAL.GetCarrierDetailsBAL(oMASCNT_CarrierBE);

        if (lstCarrier.Count > 0)
        {
            FillControls.FillDropDown(ref ddlCarrierWhoCollectGoods, lstCarrier, "CarrierName", "CarrierID", "--Select--");
        }
    }

    #endregion

    protected void ddlCarrierWhoCollectGoods_SelectedIndexChanged(object sender, EventArgs e)
    {
        MASCNT_CarrierBE oMASCNT_CarrierBE = new MASCNT_CarrierBE();
        APPCNT_CarrierBAL oMASCNT_CarrierBAL = new APPCNT_CarrierBAL();
        oMASCNT_CarrierBE.CarrierID = ddlCarrierWhoCollectGoods.SelectedValue != null ? Convert.ToInt32(ddlCarrierWhoCollectGoods.SelectedValue) : 0;
        oMASCNT_CarrierBE.Action = "ShowAll";

        List<MASCNT_CarrierBE> lstCarrier = oMASCNT_CarrierBAL.GetCarrierDetailsBAL(oMASCNT_CarrierBE);
        if (lstCarrier != null && lstCarrier.Count > 0 && lstCarrier[0].IsNarrativeRequired && (!ddlCarrierWhoCollectGoods.SelectedValue.Equals("0")))
        {
            txtOtherCarrier.Attributes.Remove("style");
            rfvCarrierRequired.Enabled = true;
        }
        else
        {
            txtOtherCarrier.Text = string.Empty;
            txtOtherCarrier.Attributes.Add("style", "display:none;");
            rfvCarrierRequired.Enabled = false;
        }
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        //Redirecting to Previous Page
        RedirectToPage(GetQueryStringValue("PreviousPage"));
    }
}
﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucInvCommunication.ascx.cs"
    Inherits="ModuleUI_Discrepancy_UserControl_ucInvCommunication" %>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<table width="100%" cellspacing="5" cellpadding="0" class="form-table">
    <tr>
        <td>
            
            <cc1:ucPanel ID="pnlINV002" runat="server"  Width="65%" CssClass="fieldset-form" BorderColor="gray" BorderStyle="Solid" BorderWidth="1">
                <table class="form-table" cellpadding="5" cellspacing="0"  width="600"  style="margin:10px;">
                    <tr>
                        <td colspan="2" style="font-weight: bold; background-color: Yellow">
                            <cc1:ucLabel ID="lblINV002Desc" runat="server" Text="Item have been short delivered. Please reduce Purchase Order or request for short delivered items to be re-delivered"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <table>
                                <tr>
                                    <td colspan="2">
                                        <cc1:ucLabel ID="lblINV002AppOption" runat="server" Text="Please select Appropriate Option"></cc1:ucLabel>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <cc1:ucRadioButton ID="rdoINV002ReduceOrder" runat="server" GroupName="GoodsOption"
                                            Text="Reduce Purchase Order" />
                                    </td>
                                    <td>
                                        <cc1:ucRadioButton ID="rdoINV002Resentbyvendor" runat="server" GroupName="GoodsOption"
                                            Text="Goods to be Resent by the Vendor" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <cc1:ucLabel ID="lblINV002Comment" runat="server" Text="Please confirm action taken "></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <cc1:ucTextbox ID="txtINV002Comment" runat="server" onkeyup="checkTextLengthOnKeyUp(this,1000);" TextMode="MultiLine" Width="575px"
                                Height="100px"></cc1:ucTextbox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucButton ID="btnINV002Save" runat="server" Text="Save" class="button" />
                        </td>
                        <td>
                            <cc1:ucCheckbox ID="chkINV002StageCompleted" runat="server" Checked="true"/><cc1:ucLabel ID="lblINV002StageCompleted"
                                runat="server" Text="Stage Completed"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                            <table>
                                <tr>
                                    <td>
                                        <cc1:ucLabel ID="lblINV002Note" runat="server" Text="If  you required to tag a comment without closing the discrepancy<br/>1 -    Deslect Stage Complete<br/>2 -   Enter  the comment<br/>3 -   Save"></cc1:ucLabel>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </cc1:ucPanel>
            <cc1:ucPanel ID="pnlINV003" runat="server"  Width="65%" CssClass="fieldset-form" BorderColor="gray" BorderStyle="Solid" BorderWidth="1">
                <table class="form-table" cellpadding="5" cellspacing="0"  width="600"  style="margin:10px;">
                    <tr>
                        <td colspan="2" style="font-weight: bold; background-color: Yellow">
                            <cc1:ucLabel ID="lblINV003Desc" runat="server" Text="Damaged Goods have been delivered. Please advise if these goods are to be resent or the PO is to be adjusted"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <table>
                                <tr>
                                    <td colspan="2">
                                        <cc1:ucLabel ID="lblINV003Option" runat="server" Text="Please select Appropriate Option"></cc1:ucLabel>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <cc1:ucRadioButton ID="rdoINV003ReducePO" runat="server" GroupName="GoodsOption"
                                            Text="Reduce Purchase Order" />
                                    </td>
                                    <td>
                                        <cc1:ucRadioButton ID="rdoINV003ResentbyVendor" runat="server" GroupName="GoodsOption"
                                            Text="Goods to be Resent by the Vendor" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <cc1:ucLabel ID="lblINV003Comment" runat="server" Text="Please confirm action taken "></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <cc1:ucTextbox ID="txtINV003Comment" runat="server" onkeyup="checkTextLengthOnKeyUp(this,1000);" TextMode="MultiLine" Width="575px"
                                Height="100px"></cc1:ucTextbox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucButton ID="btnINV003Save" runat="server" Text="Save" class="button" />
                        </td>
                        <td>
                            <cc1:ucCheckbox ID="chkINV003StageCompleted" runat="server" Checked="true"/><cc1:ucLabel ID="lblINV003StageCompleted"
                                runat="server" Text="Stage Completed"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                            <table>
                                <tr>
                                    <td>
                                        <cc1:ucLabel ID="lblINV003Note" runat="server" Text="If  you required to tag a comment without closing the discrepancy<br/>1 -    Deslect Stage Complete<br/>2 -   Enter  the comment<br/>3 -   Save"></cc1:ucLabel>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </cc1:ucPanel>
            <cc1:ucPanel ID="pnlINV004" runat="server" Width="65%" CssClass="fieldset-form" BorderColor="gray" BorderStyle="Solid" BorderWidth="1">
                <table class="form-table" cellpadding="5" cellspacing="0"  width="600"  style="margin:10px;">
                    <tr>
                        <td colspan="2" style="font-weight: bold; background-color: Yellow">
                            <cc1:ucLabel ID="lblINV004Desc" runat="server" Text="No Paperwork has been received from the vendor. Please confirm the next action for this delivery?"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <table>
                                <tr>
                                    <td colspan="3">
                                        <cc1:ucLabel ID="lblINV004AppOption" runat="server" Text="Please select Appropriate Option"></cc1:ucLabel>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <cc1:ucRadioButton ID="rdoINV004GoodsKept" runat="server" GroupName="GoodsOption" Text="Goods are to be Kept" />
                                    </td>
                                    <td>
                                        <cc1:ucRadioButton ID="rdoINV004GoodsNaxOrder" runat="server" GroupName="GoodsOption" Text="Goods to be Kept NAX Order" />
                                    </td>
                                    <td>
                                        <cc1:ucRadioButton ID="rdoINV004GoodsReturn" runat="server" GroupName="GoodsOption" Text="Goods to be returned to Vendor" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <table>
                                <tr>
                                    <td style="width: 85%">
                                        <cc1:ucLabel ID="lblINV004PurchaseOrder" runat="server" Text="Please advise of the Purchase Order which the Goods are to be booked in against"></cc1:ucLabel>
                                    </td>
                                    <td style="width: 5%" align="right">
                                        <cc1:ucTextbox ID="txtINV004PurchaseOrder" runat="server" Width="100px"></cc1:ucTextbox>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <cc1:ucLabel ID="lblINV004Comments" runat="server" Text="Please enter any additional comments "></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <cc1:ucTextbox ID="txtINV004Comments" runat="server" onkeyup="checkTextLengthOnKeyUp(this,1000);" TextMode="MultiLine" Width="575px"
                                Height="100px"></cc1:ucTextbox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucButton ID="btnINV004Save" runat="server" Text="Save" class="button" />
                        </td>
                        <td>
                            <cc1:ucCheckbox ID="chkINV004StageCompleted" runat="server" Checked="true"/><cc1:ucLabel ID="lblINV004StageCompleted"
                                runat="server" Text="Stage Completed"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                            <table>
                                <tr>
                                    <td>
                                        <cc1:ucLabel ID="lblINV004Note" runat="server" Text="If  you required to tag a comment without closing the discrepancy<br/>1 -    Deslect Stage Complete<br/>2 -   Enter  the comment<br/>3 -   Save"></cc1:ucLabel>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </cc1:ucPanel>
            <cc1:ucPanel ID="pnlINV005" runat="server"  Width="65%" CssClass="fieldset-form" BorderColor="gray" BorderStyle="Solid" BorderWidth="1">
                <table class="form-table" cellpadding="5" cellspacing="0"  width="600"  style="margin:10px;">
                    <tr>
                        <td colspan="2" style="font-weight: bold; background-color: Yellow">
                            <cc1:ucLabel ID="lblINV005Desc" runat="server" Text="No Purchase Order has been quoted by the vendor. Please confirm the next action for this delivery?"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <table>
                                <tr>
                                    <td colspan="3">
                                        <cc1:ucLabel ID="lblINV005AppOption" runat="server" Text="Please select Appropriate Option"></cc1:ucLabel>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <cc1:ucRadioButton ID="rdoINV005GoodsKept" runat="server" GroupName="GoodsOption"
                                            Text="Goods are to be Kept" />
                                    </td>
                                    <td>
                                        <cc1:ucRadioButton ID="rdoINV005GoodsNAxOrder" runat="server" GroupName="GoodsOption"
                                            Text="Goods to be Kept NAX Order" />
                                    </td>
                                    <td>
                                        <cc1:ucRadioButton ID="rdoINV005GoodsReturnedtoVendor" runat="server" GroupName="GoodsOption"
                                            Text="Goods to be returned to Vendor" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <table>
                                <tr>
                                    <td style="width: 85%">
                                        <cc1:ucLabel ID="lblINV005PO" runat="server" Text="Please advise of the Purchase Order which the Goods are to be booked in against"></cc1:ucLabel>
                                    </td>
                                    <td style="width: 5%" align="right">
                                        <cc1:ucTextbox ID="txtINV005PurchaseOrder" runat="server" Width="100px"></cc1:ucTextbox>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <cc1:ucLabel ID="lblINV005Comment" runat="server" Text="Please enter any additional comments "></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <cc1:ucTextbox ID="txtINV005Commnent" runat="server" onkeyup="checkTextLengthOnKeyUp(this,1000);" TextMode="MultiLine" Width="575px"
                                Height="100px"></cc1:ucTextbox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucButton ID="btnINV005Save" runat="server" Text="Save" class="button" />
                        </td>
                        <td>
                            <cc1:ucCheckbox ID="chkINV005StageCompleted" runat="server" Checked="true"/><cc1:ucLabel ID="lblINV005StageCompleted"
                                runat="server" Text="Stage Completed"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                            <table>
                                <tr>
                                    <td>
                                        <cc1:ucLabel ID="lblINV005Note" runat="server" Text="If  you required to tag a comment without closing the discrepancy<br/>1 -    Deslect Stage Complete<br/>2 -   Enter  the comment<br/>3 -   Save"></cc1:ucLabel>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </cc1:ucPanel>
            <cc1:ucPanel ID="pnlINV006" runat="server"  Width="65%" CssClass="fieldset-form" BorderColor="gray" BorderStyle="Solid" BorderWidth="1">
                <table class="form-table" cellpadding="5" cellspacing="0"  width="600"  style="margin:10px;">
                    <tr>
                        <td colspan="2" style="font-weight: bold; background-color: Yellow">
                            <cc1:ucLabel ID="lblINV006Desc" runat="server" Text="No Purchase Order has been quoted by the vendor. Please confirm the next action for this delivery?"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <table>
                                <tr>
                                    <td colspan="3">
                                        <cc1:ucLabel ID="lblINV006AppOption" runat="server" Text="Please select Appropriate Option"></cc1:ucLabel>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <cc1:ucRadioButton ID="rdoINV006GoodsCorrect" runat="server" GroupName="GoodsOption"
                                            Text="Goods correct,Book In" />
                                    </td>
                                    <td>
                                        <cc1:ucRadioButton ID="rdoINV006GoodsCorrectSub" runat="server" GroupName="GoodsOption"
                                            Text="Goods correct Sub,Book In" />
                                    </td>
                                    <td>
                                        <cc1:ucRadioButton ID="rdoINV006ReturnToVendor" runat="server" GroupName="GoodsOption"
                                            Text="Goods to be returned to Vendor" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <cc1:ucLabel ID="lblINV006Comment" runat="server" Text="Please enter any additional comments "></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <cc1:ucTextbox ID="txtINV006Comment" runat="server" onkeyup="checkTextLengthOnKeyUp(this,1000);" TextMode="MultiLine" Width="575px"
                                Height="100px"></cc1:ucTextbox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucButton ID="btnINV006Save" runat="server" Text="Save" class="button" />
                        </td>
                        <td>
                            <cc1:ucCheckbox ID="chkINV006StageCompleted" runat="server" Checked="true"/><cc1:ucLabel ID="lblINV006StageCompleted"
                                runat="server" Text="Stage Completed"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                            <table>
                                <tr>
                                    <td>
                                        <cc1:ucLabel ID="lblINV006Note" runat="server" Text="If  you required to tag a comment without closing the discrepancy<br/>1 -    Deslect Stage Complete<br/>2 -   Enter  the comment<br/>3 -   Save"></cc1:ucLabel>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </cc1:ucPanel>
            <cc1:ucPanel ID="pnlINV007" runat="server"  Width="65%" CssClass="fieldset-form" BorderColor="gray" BorderStyle="Solid" BorderWidth="1">
                <table class="form-table" cellpadding="5" cellspacing="0"  width="600"  style="margin:10px;">
                    <tr>
                        <td colspan="2" style="font-weight: bold; background-color: Yellow">
                            <cc1:ucLabel ID="lblINV007Desc" runat="server" Text="No Purchase Order has been quoted by the vendor. Please confirm the next action for this delivery?"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <table>
                                <tr>
                                    <td style="width:60%; font-weight: bold;">
                                        <cc1:ucLabel ID="lblINV007CarriageCost" runat="server" Text="Carriage Cost"></cc1:ucLabel>
                                    </td>
                                    <td>
                                        <cc1:ucTextbox ID="txtINV007CarriageCost" runat="server"></cc1:ucTextbox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <cc1:ucRadioButton ID="rdoINV007Charge" runat="server" GroupName="GoodsOption" Text="Charge" />
                                    </td>
                                    <td>
                                        <cc1:ucRadioButton ID="rdoINV007NoCharge" runat="server" GroupName="GoodsOption"
                                            Text="No Charge" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <cc1:ucLabel ID="lblINV007Comment" runat="server" Text="Comment"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <cc1:ucTextbox ID="txtINV007Comment" runat="server" onkeyup="checkTextLengthOnKeyUp(this,1000);" TextMode="MultiLine" Width="575px"
                                Height="100px"></cc1:ucTextbox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucButton ID="btnINV007Save" runat="server" Text="Save" class="button" />
                        </td>
                        <td>
                            <cc1:ucCheckbox ID="chkINV007StageCompleted" runat="server" Checked="true"/><cc1:ucLabel ID="lblINV007StageCompleted"
                                runat="server" Text="Stage Completed"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                            <table>
                                <tr>
                                    <td>
                                        <cc1:ucLabel ID="UcLabel6" runat="server" Text="If  you required to tag a comment without closing the discrepancy<br/>1 -    Deslect Stage Complete<br/>2 -   Enter  the comment<br/>3 -   Save"></cc1:ucLabel>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </cc1:ucPanel>
            <cc1:ucPanel ID="pnlINV008" runat="server"  Width="65%" CssClass="fieldset-form" BorderColor="gray" BorderStyle="Solid" BorderWidth="1">
                <table class="form-table" cellpadding="5" cellspacing="0"  width="600"  style="margin:10px;">
                    <tr>
                        <td colspan="2" style="font-weight: bold; background-color: Yellow">
                            <cc1:ucLabel ID="lblINV008Desc" runat="server" Text="Product has been delivered in a pack size different to the stated pack quantity on the Purchase Order.<br/>Please confirm if goods are to be kept or returned to the vendor "></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <table>
                                
                                <tr>
                                    <td>
                                        <cc1:ucRadioButton ID="rdoINV008GoodsReturnToVendor" runat="server" GroupName="GoodsOption" Text="Return Goods to Vendor" />
                                    </td>
                                    <td>
                                        <cc1:ucRadioButton ID="rdoINV008ReworknotRequired" runat="server" GroupName="GoodsOption"
                                            Text="Rework not required" />
                                    </td>
                                    <td>
                                        <cc1:ucRadioButton ID="rdoINV008ReworkNoCharge" runat="server" GroupName="GoodsOption" Text="Rework,no charge" />
                                    </td>
                                    <td>
                                        <cc1:ucRadioButton ID="rdoINV008RewordAndCharge" runat="server" GroupName="GoodsOption"
                                            Text="Rework and charge" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <cc1:ucLabel ID="lblINV008Comment" runat="server" Text="Comment"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <cc1:ucTextbox ID="txtINV008Comment" runat="server" onkeyup="checkTextLengthOnKeyUp(this,1000);" TextMode="MultiLine" Width="575px"
                                Height="100px"></cc1:ucTextbox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucButton ID="btnINV008Save" runat="server" Text="Save" class="button" />
                        </td>
                        <td>
                            <cc1:ucCheckbox ID="chkINV008StageCompleted" runat="server" Checked="true"/><cc1:ucLabel ID="lblINV008StageCompleted"
                                runat="server" Text="Stage Completed"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                            <table>
                                <tr>
                                    <td>
                                        <cc1:ucLabel ID="lblINV008Note" runat="server" Text="If  you required to tag a comment without closing the discrepancy<br/>1 -    Deslect Stage Complete<br/>2 -   Enter  the comment<br/>3 -   Save"></cc1:ucLabel>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </cc1:ucPanel>
            <cc1:ucPanel ID="pnlINV009" runat="server"  Width="65%" CssClass="fieldset-form" BorderColor="gray" BorderStyle="Solid" BorderWidth="1">
                <table class="form-table" cellpadding="5" cellspacing="0"  width="600"  style="margin:10px;">
                    <tr>
                        <td colspan="2" style="font-weight: bold; background-color: Yellow">
                            <cc1:ucLabel ID="lblINV009Desc" runat="server" Text="Goods failed required pallet specification and had to be reworked - Please confirm if vendor should be charged"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <table>
                                <tr>
                                    <td style="width:60%; font-weight: bold;">
                                        <cc1:ucLabel ID="lblINV009NoofPallets" runat="server" Text="# of Pallets"></cc1:ucLabel>
                                    </td>
                                    <td>
                                        <cc1:ucTextbox ID="txtINV009NoofPallets" runat="server"></cc1:ucTextbox>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width:60%; font-weight: bold;">
                                        <cc1:ucLabel ID="lblINV009LabourCost" runat="server" Text="Labour Cost"></cc1:ucLabel>
                                    </td>
                                    <td>
                                        <cc1:ucTextbox ID="txtINV009LabourCost" runat="server"></cc1:ucTextbox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <cc1:ucRadioButton ID="rdoINV009Charge" runat="server" GroupName="GoodsOption" Text="Charge" />
                                    </td>
                                    <td>
                                        <cc1:ucRadioButton ID="rdoINV009NoCharge" runat="server" GroupName="GoodsOption"
                                            Text="No Charge" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <cc1:ucLabel ID="lblINV009Comment" runat="server" Text="Comment"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <cc1:ucTextbox ID="txtINV009Comment" runat="server" onkeyup="checkTextLengthOnKeyUp(this,1000);" TextMode="MultiLine" Width="575px"
                                Height="100px"></cc1:ucTextbox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucButton ID="btnINV009Save" runat="server" Text="Save" class="button" />
                        </td>
                        <td>
                            <cc1:ucCheckbox ID="chkINV009StageCompleted" runat="server" Checked="true"/><cc1:ucLabel ID="UcLabel4"
                                runat="server" Text="lblINV009StageCompleted"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                            <table>
                                <tr>
                                    <td>
                                        <cc1:ucLabel ID="lblINV009Note" runat="server" Text="If  you required to tag a comment without closing the discrepancy<br/>1 -    Deslect Stage Complete<br/>2 -   Enter  the comment<br/>3 -   Save"></cc1:ucLabel>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </cc1:ucPanel>
            <cc1:ucPanel ID="pnlINV010" runat="server"  Width="65%" CssClass="fieldset-form" BorderColor="gray" BorderStyle="Solid" BorderWidth="1">
                <table class="form-table" cellpadding="5" cellspacing="0"  width="600"  style="margin:10px;">
                    <tr>
                        <td colspan="2" style="font-weight: bold; background-color: Yellow">
                            <cc1:ucLabel ID="lblINV010Desc" runat="server" Text="There has been a premature invoice before goods have been delivered. There has been an <br/>administration cost for this. Please confirm if the vendor should be charged for this"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <table>
                               
                                <tr>
                                    <td style="width:60%; font-weight: bold;">
                                        <cc1:ucLabel ID="lblINV010LabourCost" runat="server" Text="Labour Cost"></cc1:ucLabel>
                                    </td>
                                    <td>
                                        <cc1:ucTextbox ID="txtINV010LabourCost" runat="server"></cc1:ucTextbox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <cc1:ucRadioButton ID="rdoINV010Charge" runat="server" GroupName="GoodsOption" Text="Charge" />
                                    </td>
                                    <td>
                                        <cc1:ucRadioButton ID="rdoINV010NoCharge" runat="server" GroupName="GoodsOption"
                                            Text="No Charge" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <cc1:ucLabel ID="lblINV010Comment" runat="server" Text="Comment"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <cc1:ucTextbox ID="txtINV010Comment" runat="server" onkeyup="checkTextLengthOnKeyUp(this,1000);" TextMode="MultiLine" Width="575px"
                                Height="100px"></cc1:ucTextbox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucButton ID="btnINV010Save" runat="server" Text="Save" class="button" />
                        </td>
                        <td>
                            <cc1:ucCheckbox ID="chkINV010StageCompleted" runat="server" Checked="true"/><cc1:ucLabel ID="lblINV010StageCompleted"
                                runat="server" Text="lblINV009StageCompleted"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                            <table>
                                <tr>
                                    <td>
                                        <cc1:ucLabel ID="lblINV010Note" runat="server" Text="If  you required to tag a comment without closing the discrepancy<br/>1 -    Deslect Stage Complete<br/>2 -   Enter  the comment<br/>3 -   Save"></cc1:ucLabel>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </cc1:ucPanel>
            <cc1:ucPanel ID="pnlINV011" runat="server"  Width="65%" CssClass="fieldset-form" BorderColor="gray" BorderStyle="Solid" BorderWidth="1">
                <table class="form-table" cellpadding="5" cellspacing="0"  width="600"  style="margin:10px;">
                    <tr>
                        <td style="font-weight: bold; background-color: Yellow">
                            <cc1:ucLabel ID="lblINV011Desc" runat="server" Text="Generic Discrepancy has been created, please advise of remedial action"></cc1:ucLabel>
                        </td>
                    </tr>
                    
                    <tr>
                        <td>
                            <cc1:ucLabel ID="lblINV011Comment" runat="server" Text="Please supply a brief narrative of remedial action taken"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucTextbox ID="txtINV011Comment" runat="server" onkeyup="checkTextLengthOnKeyUp(this,1000);" TextMode="MultiLine" Width="575px"
                                Height="100px"></cc1:ucTextbox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucButton ID="btnINV011Save" runat="server" Text="Save" class="button" />
                        </td>
                       
                    </tr>
                    
                </table>
            </cc1:ucPanel>
            <cc1:ucPanel ID="pnlINV012" runat="server"  Width="65%" CssClass="fieldset-form" BorderColor="gray" BorderStyle="Solid" BorderWidth="1">
                <table class="form-table" cellpadding="5" cellspacing="0"  width="600"  style="margin:10px;">
                    <tr>
                        <td colspan="2" style="font-weight: bold; background-color: Yellow">
                            <cc1:ucLabel ID="lblINV012Desc" runat="server" Text="A Quality Issue has been identified. Please confirm action required"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <table>
                                
                                <tr>
                                    <td>
                                        <cc1:ucRadioButton ID="rdoINV012ReturnGoodstoVendor" runat="server" GroupName="GoodsOption" Text="Return Goods to Vendor" />
                                    </td>
                                    <td>
                                        <cc1:ucRadioButton ID="rdoINV012ReworkNotRequired" runat="server" GroupName="GoodsOption"
                                            Text="Rework not required" />
                                    </td>
                                    <td>
                                        <cc1:ucRadioButton ID="rdoINV012ReworkNoCharge" runat="server" GroupName="GoodsOption" Text="Rework,no charge" />
                                    </td>
                                    <td>
                                        <cc1:ucRadioButton ID="rdoINV012ReworkAndCharge" runat="server" GroupName="GoodsOption"
                                            Text="Rework and charge" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <cc1:ucLabel ID="lblINV012Comment" runat="server" Text="Comment"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <cc1:ucTextbox ID="txtINV012Comment" runat="server" onkeyup="checkTextLengthOnKeyUp(this,1000);" TextMode="MultiLine" Width="575px"
                                Height="100px"></cc1:ucTextbox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucButton ID="btnINV012Save" runat="server" Text="Save" class="button" />
                        </td>
                        <td>
                            <cc1:ucCheckbox ID="chkINV012StageCompleted" runat="server" Checked="true"/><cc1:ucLabel ID="lblINV012StageCompleted"
                                runat="server" Text="Stage Completed"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                            <table>
                                <tr>
                                    <td>
                                        <cc1:ucLabel ID="lblINV012Note" runat="server" Text="If  you required to tag a comment without closing the discrepancy<br/>1 -    Deslect Stage Complete<br/>2 -   Enter  the comment<br/>3 -   Save"></cc1:ucLabel>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </cc1:ucPanel>
        </td>
    </tr>
</table>

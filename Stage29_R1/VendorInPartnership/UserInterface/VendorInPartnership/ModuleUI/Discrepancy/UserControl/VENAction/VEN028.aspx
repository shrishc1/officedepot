﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="VEN028.aspx.cs" Inherits="ModuleUI_Discrepancy_UserControl_VENAction_VEN027" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Office Depot</title>
    <link rel="stylesheet" type="text/css" href="../../../../Css/style.css" />
    <link type="text/css" href="../../../../Css/ui-lightness/jquery-ui-1.8.5.custom.css"
        rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="../../../../Css/superfish.css" media="screen" />
    <script type="text/javascript" src="<%= ResolveClientUrl("../../../../Scripts/jquery-1.2.6.min.js")%>"></script>
    <script type="text/javascript" src="<%= ResolveClientUrl("../../../../Scripts/jquery-1.4.2.min.js")%>"></script>
    <script type="text/javascript" src="<%= ResolveClientUrl("../../../../Scripts/jquery-ui-1.8.5.custom.min.js")%>"></script>
    <script type="text/javascript" src="<%= ResolveClientUrl("../../../../Scripts/hoverIntent.js")%>"></script>
    <script type="text/javascript" src="<%= ResolveClientUrl("../../../../Scripts/superfish.js")%>"></script>
    <script type="text/javascript" src="<%= ResolveClientUrl("../../../../Scripts/ValidateInputs.js")%>"></script>
    <script type="text/javascript">

        $(document).ready(function () {
//            $('#<%=trAccept.ClientID %>').show();
//            $('#<%=trReject.ClientID %>').hide();

           // $('#<%=txtComments.ClientID %>').val("");
            if ($('#<%=rdoVEN028Text1.ClientID %>').is(":checked")) {
                $('#<%=trAccept.ClientID %>').show();
                $('#<%=trReject.ClientID %>').hide();
            }
            else {
                $('#<%=trAccept.ClientID %>').hide();
                $('#<%=trReject.ClientID %>').show();
            }
            $('#<%=rdoVEN028Text1.ClientID %>').click(function () {
                $('#<%=trAccept.ClientID %>').show();
                $('#<%=trReject.ClientID %>').hide();
            });

            $('#<%=rdoVEN028Text2.ClientID %>').click(function () {
                $('#<%=trAccept.ClientID %>').hide();
                $('#<%=trReject.ClientID %>').show();
            });           

        });


        function HideShowButton() {
            if (checkValidationGroup("a")) {
                document.getElementById('<%=btnSave.ClientID%>').style.visibility = "hidden";
            }
            else {
                document.getElementById('<%=btnSave.ClientID%>').style.visibility = "visible";
                return false;
            }
        } 
    </script>
</head>
<body>
    <form id="form2" runat="server" method="post">
    <cc1:ucPanel ID="pnlActionRequired" runat="server" Width="99%" GroupingText="Action Required"
        CssClass="fieldset-form" BorderColor="gray" BorderStyle="Solid" BorderWidth="0">
        <table class="form-table" cellpadding="2" cellspacing="4" width="98%">
            <tr>
                <td style="font-weight: bold;">
                    <cc1:ucLabel ID="lblVEN028Text" runat="server" CssClass="action-required-heading"
                        Text="Our warehouse team have carried out a detailed investigation for the missing disputed quantities. They were unable to find these items so we are unable to pay for the requested Invoice Quantity. Please state below if you accept this or require this to be further escalated."
                        Style="text-align: center;"></cc1:ucLabel>
                </td>
            </tr>
            <tr>
                <td>
                    <table class="form-table" cellpadding="2" cellspacing="5" width="98%" >
                        <tr>
                            <td>
                               <b> <cc1:ucRadioButton ID="rdoVEN028Text1" runat="server" GroupName="Figure" Text="Accept the results of the investigation" /></b>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <b>  <cc1:ucRadioButton ID="rdoVEN028Text2" runat="server" GroupName="Figure" Text="Escalate to senior manager"
                                    Checked="true" /></b>
                            </td>
                        </tr>
                        <tr id="trReject" runat="server">
                            <td>
                                 <b> <cc1:ucLabel ID="lblVEN028Text3" Text="Please note, if you have further documentation to support this dispute, please attach using the Add Images button at the top of this page" runat="server"></cc1:ucLabel>
                                </b>
                            </td>
                        </tr>
                        <tr id="trAccept" runat="server"><td>
                          <b><cc1:ucLabel ID="lblVEN028Text5" Text="In order for us to post payment, please attach a Credit Note using the link below and add a comment to confirm the quantities being credited.Please note, the attachments must be in either jpg, bmp or pdf format." runat="server"></cc1:ucLabel></b>
                        <br />
                        <br />
                        
                         <b> <cc1:ucLabel ID="lblVEN028Text4" Text="UPLOAD CREDIT NOTE" runat="server"></cc1:ucLabel></b>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:FileUpload ID="fileupload" runat="server" />
                        </td></tr>
                        <tr>
                            <td width="100%">
                                  <b><cc1:ucLabel ID="lblComment" runat="server"></cc1:ucLabel></b>
                                :
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <cc1:ucTextbox ID="txtComments" runat="server" onkeyup="checkTextLengthOnKeyUp(this,1000);"
                                    TextMode="MultiLine" CssClass="textarea" Width="99%" Height="99%"></cc1:ucTextbox>
                                <asp:RequiredFieldValidator ID="rfvCommentRequired" runat="server" Text="Please enter comments."
                                    ControlToValidate="txtComments" Display="None" ValidationGroup="a">
                                </asp:RequiredFieldValidator>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table class="form-table" cellpadding="0" cellspacing="0" width="98%" style="margin: 2px;">
                        <tr>
                            <td width="14%" style="display: none;">
                                  <b><cc1:ucLabel ID="lblStageCompleted" runat="server" Text="Stage Completed"></cc1:ucLabel></b>
                                <asp:ValidationSummary ID="vs1" runat="server" ValidationGroup="a" ShowSummary="false"
                                    ShowMessageBox="true" />
                            </td>
                            <td width="1%" style="display: none;">
                                :
                            </td>
                            <td width="75%" align="left" style="display: none;">
                                <cc1:ucCheckbox ID="chkStageComplete" runat="server" Checked="true" />
                            </td>
                            <td width="10%" style="display: none;">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" style="display: none;">
                                 <b> <cc1:ucLabel ID="lblDiscrepancyNote" runat="server" Text="If  you required to tag a comment without closing the discrepancy<br/>1 -    Deselect Stage Complete<br/>2 -   Enter  the comment<br/>3 -   Save"></cc1:ucLabel></b>
                            </td>
                            <td align="right" valign="bottom">
                                <cc1:ucButton ID="btnSave" runat="server" Text="Save" class="button" ValidationGroup="a"
                                    OnClientClick="javascript:HideShowButton()" OnClick="btnSave_Click" />
                                <cc1:ucButton ID="btnBack" runat="server" Text="Back" class="button" OnClick="btnBack_Click" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </cc1:ucPanel>
    </form>
</body>
</html>

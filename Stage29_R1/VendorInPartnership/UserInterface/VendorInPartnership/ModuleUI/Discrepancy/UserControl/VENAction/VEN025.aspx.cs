﻿using System;
using System.Data;

using BusinessEntities.ModuleBE.Discrepancy.LogDiscrepancy;
using BusinessLogicLayer.ModuleBAL.Discrepancy.LogDiscrepancy;

public partial class VENAction_VIN025 : CommonPage {
    #region Global variables
    #endregion

    #region Events

    protected void Page_Load(object sender, EventArgs e) {
        if (!IsPostBack) {

            if (GetQueryStringValue("FromPage") != null && GetQueryStringValue("FromPage").ToString().Trim() == "QualityIssue") {

                trQualityIssue.Visible = true;
                trWrongPackSize.Visible = false;
            }
            else {
                trQualityIssue.Visible = false;
                trWrongPackSize.Visible = true;
            }
        }
    }
    protected void btnSave_Click(object sender, EventArgs e) {
        btnSave.Enabled = false;
        DISLog_WorkFlowBE oDISLog_WorkFlowBE = new DISLog_WorkFlowBE();
        DISLog_WorkFlowBAL oDISLog_WorkFlowBAL = new DISLog_WorkFlowBAL();

        if (GetQueryStringValue("disLogID") != null && GetQueryStringValue("disLogID").ToString() != "") {
            oDISLog_WorkFlowBE.Action = "InsertUpdateWorkflowActions";
            oDISLog_WorkFlowBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
            oDISLog_WorkFlowBE.Comment = txtComments.Text.Trim();
            oDISLog_WorkFlowBE.StageCompleted = true;
            oDISLog_WorkFlowBE.StageCompletedForWhom = "VEN025_StageCompleted";
            oDISLog_WorkFlowBE.Decision = "";
            //update the work flow action table each time for the comments and other fields( if stage is completed)
            oDISLog_WorkFlowBE.DiscrepancyWorkFlowID = Convert.ToInt32(GetQueryStringValue("DisWFlowID").ToString());

            //insert or update the record according to the stage completed status
            int? iResult = oDISLog_WorkFlowBAL.addWorkFlowActionsBAL(oDISLog_WorkFlowBE);

            DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
            DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();

            //For getting Inventory Action Log date and then update VendorActionElaspedTime in Trn_DiscrepancyLog Table
            oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID"));
            oDiscrepancyBE.Action = "GetInventoryLogDate";
            oDiscrepancyBE.DiscrepancyWorkFlowID = Convert.ToInt32(GetQueryStringValue("DisWFlowID"));
            DataTable dtAllDetails = oDiscrepancyBAL.GetAllDetailsOfWorkFlowBAL(oDiscrepancyBE);
            DateTime DtInventoryLogDate = DateTime.Now;

            if (dtAllDetails != null && dtAllDetails.Rows.Count > 0)
            {
                DtInventoryLogDate = Convert.ToDateTime(dtAllDetails.Rows[0]["LogDate"]);
            }
            oDiscrepancyBE.VendorActionElapsedTime = (decimal?)DateTime.Now.Subtract(DtInventoryLogDate).TotalDays;
            oDiscrepancyBE.Action = "UpdateVendorActionElapsedTime";
            oDiscrepancyBAL.UpdateVendorActionElapsedTimeBAL(oDiscrepancyBE);

            //now insert the work flow HTML according to the action taken only if the stage is completed
            WorkflowHTML oWorkflowHTML = new WorkflowHTML();

            if (GetQueryStringValue("FromPage") != null && GetQueryStringValue("FromPage").ToString().Trim() == "WrongPackSize") {

                oDISLog_WorkFlowBE.Action = "GetDataForSpecificAction";
                oDISLog_WorkFlowBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());

                oDISLog_WorkFlowBE.StageCompletedForWhom = "GIN008_StageCompleted";
                DataTable dtGIN008 = oDISLog_WorkFlowBAL.GetDataForSpecificActionBAL(oDISLog_WorkFlowBE);

                if (dtGIN008 != null && dtGIN008.Rows.Count > 0) {
                    oDiscrepancyBE.CloseDiscrepancy = true;
                }
                oDiscrepancyBE.GINHTML = oWorkflowHTML.function3("white", null);
                oDiscrepancyBE.GINActionRequired = false;

                oDiscrepancyBE.INVHTML = oWorkflowHTML.function3("white", null);
                oDiscrepancyBE.INVActionRequired = false;

                oDiscrepancyBE.APHTML = oWorkflowHTML.function3("white", null);
                oDiscrepancyBE.APActionRequired = false;

                oDiscrepancyBE.VENHTML = oWorkflowHTML.function1(DateTime.Now, null,
                                                                    null, "Yellow", "", "",
                                                                    //Session["UserName"].ToString(),
                                                                    UIUtility.GetODVendorUserName(Convert.ToString(Session["Role"]), Convert.ToString(Session["UserName"]), Convert.ToString(Session["LoginID"])),
                                                                    txtComments.Text);
                oDiscrepancyBE.VenActionRequired = false;

                oDiscrepancyBE.Action = "InsertHTML";
                oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
                oDiscrepancyBE.LoggedDateTime = DateTime.Now;
                oDiscrepancyBE.LevelNumber = 1;
                int? iResult1 = oDiscrepancyBAL.addWorkFlowHTMLsBAL(oDiscrepancyBE);
            }

            if (GetQueryStringValue("FromPage") != null && GetQueryStringValue("FromPage").ToString().Trim() == "QualityIssue")
            {

                oDISLog_WorkFlowBE.Action = "GetDataForSpecificAction";
                oDISLog_WorkFlowBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
                oDISLog_WorkFlowBE.StageCompletedForWhom = "INV008_StageCompleted";
                DataTable dtINV001 = oDISLog_WorkFlowBAL.GetDataForSpecificActionBAL(oDISLog_WorkFlowBE);

                if (dtINV001 != null && dtINV001.Rows.Count > 0)
                {
                    //'GV' goods return to vendor, 'NR' rework not required, 'NC' rework no charge, 'RC' rework and charge
                    if (dtINV001.Rows[0]["INV008_ReworkType"].ToString() == "NR")                        
                        oDISLog_WorkFlowBE.StageCompletedForWhom = "GIN008_StageCompleted";

                    else if (dtINV001.Rows[0]["INV008_ReworkType"].ToString() == "NC")
                        oDISLog_WorkFlowBE.StageCompletedForWhom = "GIN009_StageCompleted";

                    else if (dtINV001.Rows[0]["INV008_ReworkType"].ToString() == "RC")
                        oDISLog_WorkFlowBE.StageCompletedForWhom = "GIN010_StageCompleted";
                }


                DataTable dtGIN = oDISLog_WorkFlowBAL.GetDataForSpecificActionBAL(oDISLog_WorkFlowBE);

                if (dtGIN != null && dtGIN.Rows.Count > 0)
                {
                    oDiscrepancyBE.CloseDiscrepancy = true;
                }
                oDiscrepancyBE.GINHTML = oWorkflowHTML.function3("white", null);
                oDiscrepancyBE.GINActionRequired = false;

                oDiscrepancyBE.INVHTML = oWorkflowHTML.function3("white", null);
                oDiscrepancyBE.INVActionRequired = false;

                oDiscrepancyBE.APHTML = oWorkflowHTML.function3("white", null);
                oDiscrepancyBE.APActionRequired = false;

                oDiscrepancyBE.VENHTML = oWorkflowHTML.function1(DateTime.Now, null,
                                                                    null, "Yellow", "", "",
                                                                    //Session["UserName"].ToString(),
                                                                    UIUtility.GetODVendorUserName(Convert.ToString(Session["Role"]), Convert.ToString(Session["UserName"]), Convert.ToString(Session["LoginID"])),
                                                                    txtComments.Text);
                oDiscrepancyBE.VenActionRequired = false;

                oDiscrepancyBE.Action = "InsertHTML";
                oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
                oDiscrepancyBE.LoggedDateTime = DateTime.Now;
                oDiscrepancyBE.LevelNumber = 1;
                int? iResult1 = oDiscrepancyBAL.addWorkFlowHTMLsBAL(oDiscrepancyBE);
            }
        }
        //Redirecting to Previous Page
        RedirectToPage(GetQueryStringValue("PreviousPage"));
    }
    #endregion

    #region Methods    
    #endregion

    protected void btnBack_Click(object sender, EventArgs e)
    {
        //Redirecting to Previous Page
        RedirectToPage(GetQueryStringValue("PreviousPage"));
    }
}

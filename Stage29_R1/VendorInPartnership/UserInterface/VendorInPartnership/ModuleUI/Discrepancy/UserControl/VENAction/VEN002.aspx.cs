﻿using System;

using BusinessEntities.ModuleBE.Discrepancy.LogDiscrepancy;
using BusinessLogicLayer.ModuleBAL.Discrepancy.LogDiscrepancy;
using System.Data;
using WebUtilities;
using System.Web.UI;
public partial class VENAction_VEN002 : CommonPage {

    #region Events

    protected void Page_Load(object sender, EventArgs e) {
        if (!IsPostBack) {

        }
    }

    protected void btnSave_Click(object sender, EventArgs e) {

        bool IsGoodsBeingOverDeliveredChecked = clsDiscrepancy.ISRadioButtonListChecked(rdoOverDeliveredQReason);
        if (IsGoodsBeingOverDeliveredChecked == false)
        {
            string OverDeliveredQReasonRequiredMessage = WebCommon.getGlobalResourceValue("OverDeliveredQReasonRequired");
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('" + OverDeliveredQReasonRequiredMessage + "')", true);
            return;
        }
        
        //if vendor select a reason for over delivered with comments then go to step 3a, complete the stage
        btnSave.Enabled = false;
        DISLog_WorkFlowBE oDISLog_WorkFlowBE = new DISLog_WorkFlowBE();
        DISLog_WorkFlowBAL oDISLog_WorkFlowBAL = new DISLog_WorkFlowBAL();

        if (GetQueryStringValue("disLogID") != null && GetQueryStringValue("disLogID").ToString() != "")
        {

            oDISLog_WorkFlowBE.Action = "InsertUpdateWorkflowActions";
            oDISLog_WorkFlowBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());

            if (rdoOverDeliveredQReason.SelectedItem.Value == "RQ")
            {
                oDISLog_WorkFlowBE.VEN002_ReasonForGoodsOverDelivered = "RQ";
                oDISLog_WorkFlowBE.Decision = "Rounded Order Quantity";
            }
            else if (rdoOverDeliveredQReason.SelectedItem.Value == "OQ")
            {
                oDISLog_WorkFlowBE.VEN002_ReasonForGoodsOverDelivered = "OQ";
                oDISLog_WorkFlowBE.Decision = "Open Order Quantity";
            }
            else if (rdoOverDeliveredQReason.SelectedItem.Value == "OT")
            {
                oDISLog_WorkFlowBE.VEN002_ReasonForGoodsOverDelivered = "OT";
                oDISLog_WorkFlowBE.Decision = "Other";
            }

            oDISLog_WorkFlowBE.StageCompletedForWhom = "VEN002_StageCompleted";
            oDISLog_WorkFlowBE.Comment = txtComments.Text;
            if (rdoOverDeliveredQReason.SelectedItem.Value != "" && txtComments.Text.Trim() != "")
                oDISLog_WorkFlowBE.StageCompleted = true;
            else
                oDISLog_WorkFlowBE.StageCompleted = false;

            oDISLog_WorkFlowBE.DiscrepancyWorkFlowID = Convert.ToInt32(GetQueryStringValue("DisWFlowID").ToString());

            //insert or update the record according to the stage completed status
            int? iResult = oDISLog_WorkFlowBAL.addWorkFlowActionsBAL(oDISLog_WorkFlowBE);

            if (GetQueryStringValue("FromPage") != null && GetQueryStringValue("FromPage").ToString().Trim() == "overs")
            {

                DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
                DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();

                //For getting Inventory Action Log date and then update VendorActionElaspedTime in Trn_DiscrepancyLog Table
                oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID"));
                oDiscrepancyBE.Action = "GetInventoryLogDate";
                oDiscrepancyBE.DiscrepancyWorkFlowID = Convert.ToInt32(GetQueryStringValue("DisWFlowID"));
                DataTable dtAllDetails = oDiscrepancyBAL.GetAllDetailsOfWorkFlowBAL(oDiscrepancyBE);
                DateTime DtInventoryLogDate = DateTime.Now;

                if (dtAllDetails != null && dtAllDetails.Rows.Count > 0)
                {
                    DtInventoryLogDate = Convert.ToDateTime(dtAllDetails.Rows[0]["LogDate"]);
                }
                oDiscrepancyBE.VendorActionElapsedTime = (decimal?)DateTime.Now.Subtract(DtInventoryLogDate).TotalDays;
                oDiscrepancyBE.Action = "UpdateVendorActionElapsedTime";
                oDiscrepancyBAL.UpdateVendorActionElapsedTimeBAL(oDiscrepancyBE);


                //now insert the work flow HTML according to the action taken only if the stage is completed


              
                //insert HTML for the stage completed i.e. step 3a
                WorkflowHTML oWorkflowHTML = new WorkflowHTML();

                oDISLog_WorkFlowBE.Action = "GetDataForSpecificAction";
                oDISLog_WorkFlowBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());

                oDISLog_WorkFlowBE.StageCompletedForWhom = "GIN001_StageCompleted";
                DataTable dtGIN001 = oDISLog_WorkFlowBAL.GetDataForSpecificActionBAL(oDISLog_WorkFlowBE);

                if (dtGIN001 != null && dtGIN001.Rows.Count > 0)
                {
                    oDiscrepancyBE.CloseDiscrepancy = true;
                }


                oDiscrepancyBE.GINHTML = oWorkflowHTML.function3("White", null, "", "");
                oDiscrepancyBE.GINActionRequired = false;

                oDiscrepancyBE.INVHTML = oWorkflowHTML.function3("White", null, "", "");
                oDiscrepancyBE.INVActionRequired = false;

                oDiscrepancyBE.APHTML = oWorkflowHTML.function3("White", null, "", "");
                oDiscrepancyBE.APActionRequired = false;

                oDiscrepancyBE.VENHTML = oWorkflowHTML.function1(DateTime.Now, null, null, "Yellow", "", "",
                    //Session["UserName"].ToString(), 
                    UIUtility.GetODVendorUserName(Convert.ToString(Session["Role"]), Convert.ToString(Session["UserName"]), Convert.ToString(Session["LoginID"])),
                    txtComments.Text, "", "", "", "", "", "", rdoOverDeliveredQReason.SelectedItem.Text);
                oDiscrepancyBE.VenActionRequired = false;

                oDiscrepancyBE.Action = "InsertHTML";
                oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
                oDiscrepancyBE.LoggedDateTime = DateTime.Now;
                oDiscrepancyBE.LevelNumber = 3;
                int? iResult1 = oDiscrepancyBAL.addWorkFlowHTMLsBAL(oDiscrepancyBE);
               
            }
        }
        //Redirecting to Previous Page
        RedirectToPage(GetQueryStringValue("PreviousPage"));
    }
    
    #endregion

    #region Methods

    private void InsertHTML() {

        DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
        DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();

        WorkflowHTML oWorkflowHTML = new WorkflowHTML();
        oDiscrepancyBE.VENHTML = oWorkflowHTML.function3("White", DateTime.Now, txtComments.Text, "");


        oDiscrepancyBE.Action = "InsertHTML";
        oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
        oDiscrepancyBE.LoggedDateTime = DateTime.Now;
        oDiscrepancyBE.LevelNumber = 1;
        int? iResult = oDiscrepancyBAL.addWorkFlowHTMLsBAL(oDiscrepancyBE);
    }

    #endregion

    protected void btnBack_Click(object sender, EventArgs e)
    {
        //Redirecting to Previous Page
        RedirectToPage(GetQueryStringValue("PreviousPage"));
    }

}
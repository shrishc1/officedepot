﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="VEN026.aspx.cs" Inherits="VENAction_VEN026" %>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Office Depot</title>
    <link rel="stylesheet" type="text/css" href="../../../../Css/style.css" />
    <link type="text/css" href="../../../../Css/ui-lightness/jquery-ui-1.8.5.custom.css"
        rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="../../../../Css/superfish.css" media="screen" />
    <script type="text/javascript" src="<%= ResolveClientUrl("../../../../Scripts/jquery-1.2.6.min.js")%>"></script>
    <script type="text/javascript" src="<%= ResolveClientUrl("../../../../Scripts/jquery-1.4.2.min.js")%>"></script>
    <script type="text/javascript" src="<%= ResolveClientUrl("../../../../Scripts/jquery-ui-1.8.5.custom.min.js")%>"></script>
    <script type="text/javascript" src="<%= ResolveClientUrl("../../../../Scripts/hoverIntent.js")%>"></script>
    <script type="text/javascript" src="<%= ResolveClientUrl("../../../../Scripts/superfish.js")%>"></script>
    <script type="text/javascript" src="<%= ResolveClientUrl("../../../../Scripts/ValidateInputs.js")%>"></script>    
    <script type="text/javascript">
        function HideShowButton() {
            if (checkValidationGroup("a")) {
                document.getElementById('<%=btnSave.ClientID%>').style.visibility = "hidden";
            }
            else {
                document.getElementById('<%=btnSave.ClientID%>').style.visibility = "visible";
                return false;
            }
        }

    </script>
</head>
<body>
   <form id="form1" runat="server">
    <div>
        <cc1:ucPanel ID="pnlActionRequired" runat="server" GroupingText="Action Required"
            Width="100%" CssClass="fieldset-form">
            <table class="form-table" cellpadding="5" cellspacing="0" width="96%">
                <tr>
                    <td style="font-weight: bold;">
                        <cc1:ucLabel ID="lblVEN26Action" runat="server" CssClass="action-required-heading"
                            Style="text-align: left;" Text="Enter with you agree with the discrepancy that has been created"></cc1:ucLabel>
                    </td>
                </tr>
                <tr>
                    <td>
                        <cc1:ucRadioButtonList ID="rblYesNo" runat="server" RepeatColumns="2" RepeatDirection="Horizontal">
                            <asp:ListItem Text="YES" Value="YES"></asp:ListItem>
                            <asp:ListItem Text="NO" Value="NO"></asp:ListItem>
                        </cc1:ucRadioButtonList>
                    </td>                    
                </tr>
                <tr>
                    <td>
                        <cc1:ucTextbox ID="txtComments" runat="server" onkeyup="checkTextLengthOnKeyUp(this,1000);"
                            TextMode="MultiLine" CssClass="textarea" Width="98%"></cc1:ucTextbox>
                        <asp:RequiredFieldValidator ID="rfvCommentRequired" runat="server" Text="Please enter comments."
                            ControlToValidate="txtComments" Display="None" ValidationGroup="a">
                        </asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table class="form-table" cellpadding="0" cellspacing="0" width="98%" style="margin: 2px;">
                            <tr>
                                <td width="80%">
                                    <asp:ValidationSummary ID="vs1" runat="server" ValidationGroup="a" ShowSummary="false"
                                        ShowMessageBox="true" />
                                </td>
                                <td style="text-align: right;">
                                    <cc1:ucButton ID="btnSave" runat="server" Text="Save" class="button" ValidationGroup="a"                                    
                                     OnClientClick="javascript:HideShowButton()"
                                     OnClick="btnSave_Click" />
                                    <cc1:ucButton ID="btnBack" runat="server" Text="Back" class="button" OnClick="btnBack_Click" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </cc1:ucPanel>
    </div>
    </form>
</body>
</html>

﻿using System;
using System.Data;

using BusinessEntities.ModuleBE.Discrepancy.LogDiscrepancy;
using BusinessLogicLayer.ModuleBAL.Discrepancy.LogDiscrepancy;


public partial class VENAction_VEN004 : CommonPage {
    #region Events

    protected void Page_Load(object sender, EventArgs e) {

    }

    protected void btnSave_Click(object sender, EventArgs e) {
        btnSave.Enabled = false;
        DISLog_WorkFlowBE oDISLog_WorkFlowBE = new DISLog_WorkFlowBE();
        DISLog_WorkFlowBAL oDISLog_WorkFlowBAL = new DISLog_WorkFlowBAL();

        oDISLog_WorkFlowBE.Action = "GetDataForSpecificAction";
        oDISLog_WorkFlowBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
        oDISLog_WorkFlowBE.StageCompletedForWhom = "ACP001_StageCompleted";
        DataTable dtACP001 = oDISLog_WorkFlowBAL.GetDataForSpecificActionBAL(oDISLog_WorkFlowBE);

        if (GetQueryStringValue("disLogID") != null && GetQueryStringValue("disLogID").ToString() != "") {

           
            oDISLog_WorkFlowBE.Action = "InsertUpdateWorkflowActions";
            oDISLog_WorkFlowBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
            oDISLog_WorkFlowBE.Comment = txtVEN004Comment.Text.Trim();
            oDISLog_WorkFlowBE.StageCompletedForWhom = "VEN004_StageCompleted";

            //update the work flow action table each time for the comments and other fields( if stage is completed)
            oDISLog_WorkFlowBE.DiscrepancyWorkFlowID = Convert.ToInt32(GetQueryStringValue("DisWFlowID").ToString());

            oDISLog_WorkFlowBE.Decision = "";

            //insert or update the record according to the stage completed status
            int ? iResult = oDISLog_WorkFlowBAL.addWorkFlowActionsBAL(oDISLog_WorkFlowBE);

           
            DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
            DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();

            //For getting Inventory Action Log date and then update VendorActionElaspedTime in Trn_DiscrepancyLog Table
            oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID"));
            oDiscrepancyBE.Action = "GetInventoryLogDate";
            oDiscrepancyBE.DiscrepancyWorkFlowID = Convert.ToInt32(GetQueryStringValue("DisWFlowID"));
            DataTable dtAllDetails = oDiscrepancyBAL.GetAllDetailsOfWorkFlowBAL(oDiscrepancyBE);
            DateTime DtInventoryLogDate = DateTime.Now;

            if (dtAllDetails != null && dtAllDetails.Rows.Count > 0)
            {
                DtInventoryLogDate = Convert.ToDateTime(dtAllDetails.Rows[0]["LogDate"]);
            }
            oDiscrepancyBE.VendorActionElapsedTime = (decimal?)DateTime.Now.Subtract(DtInventoryLogDate).TotalDays;
            oDiscrepancyBE.Action = "UpdateVendorActionElapsedTime";
            oDiscrepancyBAL.UpdateVendorActionElapsedTimeBAL(oDiscrepancyBE);

            //now insert the work flow HTML according to the action taken only if the stage is completed
            WorkflowHTML oWorkflowHTML = new WorkflowHTML();

            oDiscrepancyBE.GINHTML = oWorkflowHTML.function3("White", null, "", "");
            oDiscrepancyBE.GINActionRequired = false;

            oDiscrepancyBE.INVHTML = oWorkflowHTML.function3("White", null, "", "");
            oDiscrepancyBE.INVActionRequired = false;

            oDiscrepancyBE.APHTML = oWorkflowHTML.function3("White", null, "", "");
            oDiscrepancyBE.APActionRequired = false;

            oDiscrepancyBE.VENHTML = oWorkflowHTML.function1(DateTime.Now, null, null, "Yellow",
                                                                        "",
                                                                        "",
                                                                        //Session["UserName"].ToString(), 
                                                                        UIUtility.GetODVendorUserName(Convert.ToString(Session["Role"]), Convert.ToString(Session["UserName"]), Convert.ToString(Session["LoginID"])),
                                                                        "", "", "", "", "", "", "", "", txtVEN004Comment.Text);
            oDiscrepancyBE.VenActionRequired = false;

            oDiscrepancyBE.Action = "InsertHTML";
            oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
            oDiscrepancyBE.LoggedDateTime = DateTime.Now;

            if (dtACP001 != null && dtACP001.Rows.Count > 0)
                oDiscrepancyBE.CloseDiscrepancy = true;

            oDiscrepancyBE.LevelNumber = 1;
            int? iResult1 = oDiscrepancyBAL.addWorkFlowHTMLsBAL(oDiscrepancyBE);


        }
        //Redirecting to Previous Page
        RedirectToPage(GetQueryStringValue("PreviousPage"));
    }

    #endregion

    protected void btnBack_Click(object sender, EventArgs e)
    {
        //Redirecting to Previous Page
        RedirectToPage(GetQueryStringValue("PreviousPage"));
    }
}
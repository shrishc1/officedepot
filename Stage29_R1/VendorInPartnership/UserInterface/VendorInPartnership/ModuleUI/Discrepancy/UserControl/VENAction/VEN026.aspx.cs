﻿using System;
using System.Web.UI;
using System.Collections.Generic;
using BusinessEntities.ModuleBE.Discrepancy.LogDiscrepancy;
using BusinessLogicLayer.ModuleBAL.Discrepancy.LogDiscrepancy;
using WebUtilities;

public partial class VENAction_VEN026 : CommonPage
{
    string ReservationGoodsInDesc = WebCommon.getGlobalResourceValue("ReservationGoodsInDesc");
    string strYes = WebCommon.getGlobalResourceValue("Yes").ToUpper();
    string strNo = WebCommon.getGlobalResourceValue("No").ToUpper();

    #region Events

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        bool IsrblYesNoChecked = clsDiscrepancy.ISRadioButtonListChecked(rblYesNo);
        if (IsrblYesNoChecked == false)
        {
            string rblYesNoRequiredMessage = WebCommon.getGlobalResourceValue("VEN26Action");
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('" + rblYesNoRequiredMessage + "')", true);
            return;
        }



        #region Validation to select Yes or No ..
        bool blnCheck = false;
        for (int index = 0; index < rblYesNo.Items.Count; index++)
        {
            if (rblYesNo.Items[index].Selected)
                blnCheck = true;
        }
        if (!blnCheck)
        {
            string ven26Action = WebCommon.getGlobalResourceValue("VEN26Action");
            ScriptManager.RegisterStartupScript(this, this.GetType(), "popup", "<script>alert('" + ven26Action + "');</script>", false);
            return;
        }
        #endregion 

        #region Logic to get the Picker name.
        string PickerName = string.Empty;
        if (GetQueryStringValue("disLogID") != null && GetQueryStringValue("disLogID").ToString() != string.Empty)
        {
            string strStockPlannerEmailId = string.Empty;
            DiscrepancyBE oNewDiscrepancyBE = new DiscrepancyBE();
            DiscrepancyBAL oNewDiscrepancyBAL = new DiscrepancyBAL();
            oNewDiscrepancyBE.Action = "GetDiscrepancyLog";
            oNewDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
            List<DiscrepancyBE> lstDisLog = oNewDiscrepancyBAL.GetDiscrepancyLogDetailsBAL(oNewDiscrepancyBE);
            oNewDiscrepancyBAL = null;
            if (lstDisLog != null && lstDisLog.Count > 0)
            {
                PickerName = lstDisLog[0].PickersName;
            }
        }
        #endregion

        btnSave.Enabled = false;
        DISLog_WorkFlowBE oDISLog_WorkFlowBE = new DISLog_WorkFlowBE();
        DISLog_WorkFlowBAL oDISLog_WorkFlowBAL = new DISLog_WorkFlowBAL();

        if (GetQueryStringValue("disLogID") != null && GetQueryStringValue("disLogID").ToString() != "")
        {
            string AgreeWithDiscrepancy = string.Empty;            
            oDISLog_WorkFlowBE.Action = "InsertUpdateWorkflowActions";
            oDISLog_WorkFlowBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
            oDISLog_WorkFlowBE.Comment = txtComments.Text.Trim();
            oDISLog_WorkFlowBE.StageCompleted = true;
            oDISLog_WorkFlowBE.StageCompletedForWhom = "VEN26_StageCompleted";
            if (rblYesNo.SelectedValue.Equals("YES"))
            {
                oDISLog_WorkFlowBE.Ven26_YesNo = true;
                AgreeWithDiscrepancy = strYes;
                oDISLog_WorkFlowBE.Decision = "YES";
            }
            else
            {
                oDISLog_WorkFlowBE.Ven26_YesNo = false;
                AgreeWithDiscrepancy = strNo;
                oDISLog_WorkFlowBE.Decision = "NO";
            }

            //update the work flow action table each time for the comments and other fields( if stage is completed)
            oDISLog_WorkFlowBE.DiscrepancyWorkFlowID = Convert.ToInt32(GetQueryStringValue("DisWFlowID").ToString());

            //insert or update the record according to the stage completed status
            int? iResult = oDISLog_WorkFlowBAL.addWorkFlowActionsBAL(oDISLog_WorkFlowBE);

            //now insert the work flow HTML according to the action taken only if the stage is completed
            DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
            DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();
            WorkflowHTML oWorkflowHTML = new WorkflowHTML();

            oDiscrepancyBE.GINHTML = oWorkflowHTML.function3("White", null, string.Empty, string.Empty);
            oDiscrepancyBE.GINActionRequired = false;

            oDiscrepancyBE.INVHTML = oWorkflowHTML.function3("White", null, string.Empty, string.Empty);
            oDiscrepancyBE.INVActionRequired = false;

            oDiscrepancyBE.APHTML = oWorkflowHTML.function3("White", null, string.Empty, string.Empty);
            oDiscrepancyBE.APActionRequired = false;

            oDiscrepancyBE.VENHTML = oWorkflowHTML.function5(DateTime.Now, "Yellow", 
                //Session["UserName"].ToString(), 
                UIUtility.GetODVendorUserName(Convert.ToString(Session["Role"]), Convert.ToString(Session["UserName"]), Convert.ToString(Session["LoginID"])),
                AgreeWithDiscrepancy, PickerName, txtComments.Text);
            oDiscrepancyBE.VenActionRequired = false;

            oDiscrepancyBE.Action = "InsertHTML";
            oDiscrepancyBE.CloseDiscrepancy = false;
            oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
            oDiscrepancyBE.LoggedDateTime = DateTime.Now;
            oDiscrepancyBE.LevelNumber = 1;
            int? iResult1 = oDiscrepancyBAL.addWorkFlowHTMLsBAL(oDiscrepancyBE);
        }

        //Redirecting to Previous Page
        RedirectToPage(GetQueryStringValue("PreviousPage"));
    }

    #endregion

    protected void btnBack_Click(object sender, EventArgs e)
    {
        //Redirecting to Previous Page
        RedirectToPage(GetQueryStringValue("PreviousPage"));
    }
}
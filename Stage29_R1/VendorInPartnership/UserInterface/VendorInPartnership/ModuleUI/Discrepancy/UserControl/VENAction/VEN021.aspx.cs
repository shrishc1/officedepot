﻿using System;
using System.Data;

using BusinessEntities.ModuleBE.Discrepancy.LogDiscrepancy;
using BusinessLogicLayer.ModuleBAL.Discrepancy.LogDiscrepancy;


public partial class VENAction_VEN021 : CommonPage {

    #region Events

    protected void Page_Load(object sender, EventArgs e) {

    }

    protected void btnSave_Click(object sender, EventArgs e) {
        btnSave.Enabled = false;
        DISLog_WorkFlowBE oDISLog_WorkFlowBE = new DISLog_WorkFlowBE();
        DISLog_WorkFlowBAL oDISLog_WorkFlowBAL = new DISLog_WorkFlowBAL();

        if (GetQueryStringValue("disLogID") != null && GetQueryStringValue("disLogID").ToString() != "")
        {
           
            oDISLog_WorkFlowBE.Action = "InsertUpdateWorkflowActions";
            oDISLog_WorkFlowBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
            oDISLog_WorkFlowBE.Comment = txtComments.Text.Trim();
            oDISLog_WorkFlowBE.StageCompleted = true;
            oDISLog_WorkFlowBE.StageCompletedForWhom = "VEN021_StageCompleted";
            oDISLog_WorkFlowBE.Decision = "";
            //update the work flow action table each time for the comments and other fields( if stage is completed)
            oDISLog_WorkFlowBE.DiscrepancyWorkFlowID = Convert.ToInt32(GetQueryStringValue("DisWFlowID").ToString());

            //insert or update the record according to the stage completed status
            int? iResult = oDISLog_WorkFlowBAL.addWorkFlowActionsBAL(oDISLog_WorkFlowBE);

            DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
            DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();

            //For getting Inventory Action Log date and then update VendorActionElaspedTime in Trn_DiscrepancyLog Table
            oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID"));
            oDiscrepancyBE.Action = "GetInventoryLogDate";
            oDiscrepancyBE.DiscrepancyWorkFlowID = Convert.ToInt32(GetQueryStringValue("DisWFlowID"));
            DataTable dtAllDetails = oDiscrepancyBAL.GetAllDetailsOfWorkFlowBAL(oDiscrepancyBE);
            DateTime DtInventoryLogDate = DateTime.Now;

            if (dtAllDetails != null && dtAllDetails.Rows.Count > 0)
            {
                DtInventoryLogDate = Convert.ToDateTime(dtAllDetails.Rows[0]["LogDate"]);
            }
            oDiscrepancyBE.VendorActionElapsedTime = (decimal?)DateTime.Now.Subtract(DtInventoryLogDate).TotalDays;
            oDiscrepancyBE.Action = "UpdateVendorActionElapsedTime";
            oDiscrepancyBAL.UpdateVendorActionElapsedTimeBAL(oDiscrepancyBE);

            //now insert the work flow HTML according to the action taken only if the stage is completed
            WorkflowHTML oWorkflowHTML = new WorkflowHTML();

            oDiscrepancyBE.GINHTML = oWorkflowHTML.function3("White", null, "", "");
            oDiscrepancyBE.GINActionRequired = false;

            oDiscrepancyBE.INVHTML = oWorkflowHTML.function3("White", null, "", "");
            oDiscrepancyBE.INVActionRequired = false;

            oDiscrepancyBE.APHTML = oWorkflowHTML.function3("White", null, "", "");
            oDiscrepancyBE.APActionRequired = false;

            oDiscrepancyBE.VENHTML = oWorkflowHTML.function1(DateTime.Now, null, null, "Yellow", "", "",
                                                            //Session["UserName"].ToString(), 
                                                            UIUtility.GetODVendorUserName(Convert.ToString(Session["Role"]), Convert.ToString(Session["UserName"]), Convert.ToString(Session["LoginID"])),
                                                            txtComments.Text.Trim());
            oDiscrepancyBE.VenActionRequired = false;

            oDISLog_WorkFlowBE.Action = "GetAll";
            oDISLog_WorkFlowBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());            
            DataTable dt1 = oDISLog_WorkFlowBAL.GetDataForSpecificActionBAL(oDISLog_WorkFlowBE);

            //step - 2b
            if (dt1 != null && dt1.Rows.Count > 0) {
                foreach (DataRow dr in dt1.Rows) {
                    if (dr["APUserControl"] != DBNull.Value && Convert.ToString(dr["APUserControl"]) == "ACP003" && dr["APActionRequired"] != DBNull.Value && Convert.ToBoolean(dr["APActionRequired"]) == false) {
                        oDiscrepancyBE.CloseDiscrepancy = true;
                        break;
                    }                   
                }
            }

            //step- 2a
            int count1 = 0;
            if (dt1 != null && dt1.Rows.Count > 0) {
                foreach (DataRow dr in dt1.Rows) {
                    if (dr["APUserControl"] == DBNull.Value) {
                        count1++;
                    }
                }
            }

           
            bool invTookAction = false;
            if (dt1 != null && dt1.Rows.Count > 0) {
                foreach (DataRow dr1 in dt1.Rows) {
                    if (dr1["INVUserControl"] != DBNull.Value && Convert.ToString(dr1["INVUserControl"]) == "INV009" && dr1["INVActionRequired"] != DBNull.Value && Convert.ToBoolean(dr1["INVActionRequired"]) == false) {
                        invTookAction = true;
                        break;
                    }
                }                
            }
            if (count1 == dt1.Rows.Count && invTookAction == true) { oDiscrepancyBE.CloseDiscrepancy = true; }

            //step - 2a.1
            int count2 = 0;
            if (dt1 != null && dt1.Rows.Count > 0) {
                foreach (DataRow dr in dt1.Rows) {
                    if (dr["APUserControl"] == DBNull.Value && dr["INVUserControl"] == DBNull.Value) {
                        count2++;
                    }
                }
                if (count2 == dt1.Rows.Count) { oDiscrepancyBE.CloseDiscrepancy = true; }
            }
            

            oDiscrepancyBE.Action = "InsertHTML";
            oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
            oDiscrepancyBE.LoggedDateTime = DateTime.Now;
            oDiscrepancyBE.LevelNumber = 1;
            int? iResult1 = oDiscrepancyBAL.addWorkFlowHTMLsBAL(oDiscrepancyBE);
        }
        //Redirecting to Previous Page
        RedirectToPage(GetQueryStringValue("PreviousPage"));
    }

    #endregion

    protected void btnBack_Click(object sender, EventArgs e)
    {
        //Redirecting to Previous Page
        RedirectToPage(GetQueryStringValue("PreviousPage"));
    }
}
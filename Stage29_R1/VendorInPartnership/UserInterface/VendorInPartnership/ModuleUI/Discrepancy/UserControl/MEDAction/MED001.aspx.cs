﻿using System;
using System.Collections.Generic;
using System.Data;
using BusinessEntities.ModuleBE.AdminFunctions;
using BusinessEntities.ModuleBE.Discrepancy.LogDiscrepancy;
using BusinessLogicLayer.ModuleBAL.AdminFunctions;
using BusinessLogicLayer.ModuleBAL.Discrepancy.LogDiscrepancy;
using WebUtilities;


public partial class APAction_ACP005 : CommonPage
{

    int Siteid = 0;
    int Vendorid = 0;
    string EmailList = null;
    System.Text.StringBuilder sSendMailLink = new System.Text.StringBuilder();
    string sentToLinks = string.Empty;
    protected string CommentRequired = WebCommon.getGlobalResourceValue("CommentRequired");
    string GetDebitCreditStatus = string.Empty;
    protected void Page_Load(object sender, EventArgs e)
    {

    }


    #region Events
    protected void btnBack_Click(object sender, EventArgs e)
    {
        //Redirecting to Previous Page
        RedirectToPage(GetQueryStringValue("PreviousPage"));
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        btnSave.Enabled = false;
        Save();

        //Redirecting to Previous Page
        RedirectToPage(GetQueryStringValue("PreviousPage"));
    }

   

    #endregion

    #region

    private void Save()
    {

        DISLog_WorkFlowBE oDISLog_WorkFlowBE = new DISLog_WorkFlowBE();
        DISLog_WorkFlowBAL oDISLog_WorkFlowBAL = new DISLog_WorkFlowBAL();
        if (GetQueryStringValue("disLogID") != null && GetQueryStringValue("disLogID").ToString() != "")
        {
            //**********************************************************
            string Location = hdfLocation.Value;
            DiscrepancyBE oDiscrepancyBE1 = new DiscrepancyBE();
            DiscrepancyBAL oDiscrepancyBAL1 = new DiscrepancyBAL();
            oDiscrepancyBE1.Action = "UpdateLocation";
            oDiscrepancyBE1.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
            oDiscrepancyBE1.Location = Location;
            oDiscrepancyBE1.UserID = Convert.ToInt32(Session["UserID"]);
            string sResult1 = oDiscrepancyBAL1.updateDiscrepancyLocationBAL(oDiscrepancyBE1);
            //**********************************************************
            oDISLog_WorkFlowBE.Action = "InsertUpdateWorkflowActions";
            oDISLog_WorkFlowBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
            oDISLog_WorkFlowBE.StageCompletedForWhom = "MED001_StageCompleted";
            if (chkStageComplete.Checked)
            {
                oDISLog_WorkFlowBE.StageCompleted = true;
            }
            else
            {
                oDISLog_WorkFlowBE.StageCompleted = false;
            }
            oDISLog_WorkFlowBE.Comment = txtComment.Text;

            //update the work flow action table each time for the comments and other fields( if stage is completed)
            oDISLog_WorkFlowBE.DiscrepancyWorkFlowID = Convert.ToInt32(GetQueryStringValue("DisWFlowID").ToString());

            //insert or update the record according to the stage completed status
            int? iResult = oDISLog_WorkFlowBAL.addWorkFlowActionsBAL(oDISLog_WorkFlowBE);

            InquiryHTML();


        }
    }

    private void InquiryHTML()
    {

        GetDebitCreditStatus = GetCreditDebitStatus();

        DISLog_WorkFlowBE oDISLog_WorkFlowBE = new DISLog_WorkFlowBE();
        DISLog_WorkFlowBAL oDISLog_WorkFlowBAL = new DISLog_WorkFlowBAL();
        //now insert the work flow HTML according to the action taken only if the stage is completed
        DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
        DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();


        if (chkStageComplete.Checked)
        {
            WorkflowHTML oWorkflowHTML = new WorkflowHTML();
            if (rdoMED001Text1.Checked)
            {
                sentToLinks = SendMailAndGetMailLinks(Convert.ToInt32(GetQueryStringValue("disLogID")), "invoice query", "Communication2", "Communication5");
            }
            //else
            //{
            //    sentToLinks = SendMailAndGetMailLinks(Convert.ToInt32(GetQueryStringValue("disLogID")), "invoice query", "Communication2", "Communication4");
            //}
           

            oDiscrepancyBE.GINHTML = oWorkflowHTML.function3("white", null, "", "");
            oDiscrepancyBE.GINActionRequired = false;
            oDiscrepancyBE.INVHTML = oWorkflowHTML.function3("white", null, "", "");
            oDiscrepancyBE.INVActionRequired = false;
           
            if (rdoMED001Text1.Checked)
            {
                oDiscrepancyBE.VENHTML = oWorkflowHTML.function2(DateTime.Now, null, "Blue", Convert.ToString(sentToLinks), "", WebCommon.getGlobalResourceValue("Communicationsent"));               

                oDiscrepancyBE.MEDHTML = oWorkflowHTML.function14(pColor: "blue", DiscrePancyText: WebCommon.getGlobalResourceValue("MED001Workflow3"));

                oDiscrepancyBE.APHTML = oWorkflowHTML.function13(pColor: "Blue", pDate: DateTime.Now, SentTo: WebCommon.getGlobalResourceValue("AccountsPayable"), DiscrePancyTextBottom: WebCommon.getGlobalResourceValue("MED001Workflow31"));
                oDiscrepancyBE.APActionRequired = true;
                oDiscrepancyBE.APUserControl = "ACP013";
            }
            else
            {
                //oDiscrepancyBE.VENHTML = oWorkflowHTML.function2(DateTime.Now, null, "Blue", Convert.ToString(sentToLinks), "", WebCommon.getGlobalResourceValue("MED001Workflow2"));              
                oDiscrepancyBE.VENHTML = oWorkflowHTML.function3("white", null, "", "");
                oDiscrepancyBE.MEDHTML = oWorkflowHTML.function14(pColor: "yellow", DiscrePancyText: WebCommon.getGlobalResourceValue("MED001Workflow4"));

                oDiscrepancyBE.APHTML = oWorkflowHTML.function13(pColor: "Blue", pDate: DateTime.Now, SentTo: WebCommon.getGlobalResourceValue("AccountsPayable"), DiscrePancyTextBottom: WebCommon.getGlobalResourceValue("MED001Workflow32"));
                oDiscrepancyBE.APActionRequired = true;

              
              
                if (GetDebitCreditStatus == "D")
                {
                    oDiscrepancyBE.APUserControl = "ACP011";
                }
                else if (GetDebitCreditStatus == "C")           
                {
                    oDiscrepancyBE.APUserControl = "ACP012";
                }               
            }

            oDiscrepancyBE.VenActionRequired = false;

           
            oDiscrepancyBE.MEDActionRequired = false;

            oDiscrepancyBE.Action = "InsertHTML";
            if (GetDebitCreditStatus == "N")
            {
                oDiscrepancyBE.CloseDiscrepancy = true;
                oDiscrepancyBE.APActionRequired = false;
                oDiscrepancyBE.APUserControl = "";
            }
            else
            {
                oDiscrepancyBE.CloseDiscrepancy = false;
            }
            oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
            oDiscrepancyBE.LoggedDateTime = DateTime.Now;
            oDiscrepancyBE.LevelNumber = 1;
            oDiscrepancyBE.ActionTakenBy = "MED";
            oDiscrepancyBE.APActionCategory = "AR";  //Action Required
            int? iResult1 = oDiscrepancyBAL.addWorkFlowHTMLsBAL(oDiscrepancyBE);
          
        }
        else
        {
            /*Enter comments with date*/
            InsertHTML();
        }
    }
    private string SendMailAndGetMailLinks(int DiscrepancyLogID, string discrepancyType, string sCurrentCommunicationLevel, string sNextCommunicationLevel)
    {
        sendCommunicationAllLevel osendCommunicationAllLevel = new sendCommunicationAllLevel();
        return osendCommunicationAllLevel.SendMailAndGetMailLinks(DiscrepancyLogID, discrepancyType, sCurrentCommunicationLevel, sNextCommunicationLevel);
        //return osendCommunicationAllLevel.sendCommunicationByEMail(DiscrepancyLogID, discrepancyType, sCurrentCommunicationLevel, sNextCommunicationLevel);
    }

    private void InsertHTML()
    {
        DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
        DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();

        WorkflowHTML oWorkflowHTML = new WorkflowHTML();
        oDiscrepancyBE.GINHTML = oWorkflowHTML.function3("White", DateTime.Now, txtComment.Text.Trim(), "");
        oDiscrepancyBE.Action = "InsertHTML";
        oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
        oDiscrepancyBE.LoggedDateTime = DateTime.Now;
        oDiscrepancyBE.LevelNumber = 1;

        int? iResult = oDiscrepancyBAL.addWorkFlowHTMLsBAL(oDiscrepancyBE);
    }

    private int? sendAndSaveDiscrepancyCommunication(int iDiscrepancy, string LetterType)
    {

        GetDiscrepancyEmailListOfVendor();

        int? vendorID = Vendorid;

        sendCommunication communication = new sendCommunication();
        int? retVal = communication.sendCommunicationByEMail2(iDiscrepancy, "invoice", EmailList, vendorID, "", "", "", 0, LetterType);
        sSendMailLink = communication.sSendMailLink;
        return retVal;

    }


    private string GetDiscrepancyEmailListOfVendor()
    {
        if (GetQueryStringValue("disLogID") != null)
        {
            DiscrepancyBE oNewDiscrepancyBE = new DiscrepancyBE();
            DiscrepancyBAL oNewDiscrepancyBAL = new DiscrepancyBAL();
            oNewDiscrepancyBE.Action = "GetDiscrepancyLog";
            oNewDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
            List<DiscrepancyBE> lstDisLog = oNewDiscrepancyBAL.GetDiscrepancyLogDetailsBAL(oNewDiscrepancyBE);
            oNewDiscrepancyBAL = null;
            if (lstDisLog != null && lstDisLog.Count > 0)
            {
                Siteid = lstDisLog[0].Site.SiteID;
                Vendorid = Convert.ToInt32(lstDisLog[0].VendorID);
            }

            EmailList = GetDiscrepancyContactsOfVendor(Vendorid, Siteid);

        }
        return EmailList;

    }

    public string GetCreditDebitStatus()
    {
        DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
        DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();
        oDiscrepancyBE.Action = "GetDebitCreditStatus";
        oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString()); ;
        GetDebitCreditStatus = oDiscrepancyBAL.GetDebitCreditStatusBAL(oDiscrepancyBE);
        return GetDebitCreditStatus;
    }
    #endregion
}
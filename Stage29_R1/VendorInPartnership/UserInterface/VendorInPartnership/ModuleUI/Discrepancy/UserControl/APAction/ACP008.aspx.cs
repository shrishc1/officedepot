﻿using System;
using System.Collections.Generic;
using System.Data;
using BusinessEntities.ModuleBE.AdminFunctions;
using BusinessEntities.ModuleBE.Discrepancy.LogDiscrepancy;
using BusinessLogicLayer.ModuleBAL.AdminFunctions;
using BusinessLogicLayer.ModuleBAL.Discrepancy.LogDiscrepancy;
using WebUtilities;
using System.Web.UI;
using System.IO;
using System.Linq;


public partial class APAction_ACP005 : CommonPage
{

    int Siteid = 0;
    int Vendorid = 0;
    string EmailList = null;
    System.Text.StringBuilder sSendMailLink = new System.Text.StringBuilder();

    protected string CommentRequired = "-" + WebCommon.getGlobalResourceValue("CommentRequired");
    string fileUploadError = WebCommon.getGlobalResourceValue("fileUploadError");
    string fileUploadError1 = WebCommon.getGlobalResourceValue("fileUploadError1");
    string WrongFile = WebCommon.getGlobalResourceValue("WrongFile");
    protected string APDocumentErrorMessage = "-" + WebCommon.getGlobalResourceValue("APDocumentErrorMessage");
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnSave_Click(object sender, EventArgs e)
    {

        DISLog_WorkFlowBE oDISLog_WorkFlowBE = new DISLog_WorkFlowBE();
        DISLog_WorkFlowBAL oDISLog_WorkFlowBAL = new DISLog_WorkFlowBAL();
        string fileName1 = string.Empty;
        string fileName2 = string.Empty;
        if (rdoVEN029Text1.Checked)
        {
            if (fileupload1.HasFile == false || fileupload2.HasFile == false)
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('" + fileUploadError + "')", true);
                return;
            }

            if (fileupload1.HasFile)
            {
                if (fileupload1.FileName.Split('.').ElementAt(1).ToLower() == "jpg" || fileupload1.FileName.Split('.').ElementAt(1).ToLower() == "bmp" || fileupload1.FileName.Split('.').ElementAt(1).ToLower() == "pdf")
                {
                    fileName1 = fileupload1.FileName;
                    fileupload1.SaveAs(Path.Combine(Server.MapPath("~/images/discrepancy"), fileName1));
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('" + WrongFile + "')", true);
                    return;
                }
            }

            if (fileupload2.HasFile)
            {
                if (fileupload2.FileName.Split('.').ElementAt(1).ToLower() == "jpg" || fileupload2.FileName.Split('.').ElementAt(1).ToLower() == "bmp" || fileupload2.FileName.Split('.').ElementAt(1).ToLower() == "pdf")
                {
                    fileName2 = fileupload2.FileName;
                    fileupload2.SaveAs(Path.Combine(Server.MapPath("~/images/discrepancy"), fileName2));
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('" + WrongFile + "')", true);
                    return;
                }
            }
            oDISLog_WorkFlowBE.InvoiceResolution = "Receipt Qty Accepted";
        }
        else if (rdoDeliveredQty.Checked)
        {
            if (fileupload.HasFile == false)
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('" + fileUploadError1 + "')", true);
                return;
            }

            if (fileupload.HasFile)
            {
                if (fileupload.FileName.Split('.').ElementAt(1).ToLower() == "jpg" || fileupload.FileName.Split('.').ElementAt(1).ToLower() == "bmp"  || fileupload.FileName.Split('.').ElementAt(1).ToLower() == "pdf")
                {
                    fileName1 = fileupload.FileName;
                    fileupload.SaveAs(Path.Combine(Server.MapPath("~/images/discrepancy"), fileName1));
                    fileName2 = string.Empty;
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('" + WrongFile + "')", true);
                    return;
                }
            }
            oDISLog_WorkFlowBE.InvoiceResolution = "Invoice Qty Accepted ";

        }
        if (!string.IsNullOrEmpty(fileName1) && !string.IsNullOrEmpty(fileName2))
        {
            for (int i = 0; i < 2; i++)
            {
                oDISLog_WorkFlowBE.Action = "InvoiceInsertDocInformation";
                oDISLog_WorkFlowBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
                if (i == 0)
                {
                    oDISLog_WorkFlowBE.DocPath = fileName1;
                    oDISLog_WorkFlowBE.Doctype = "POD";
                }
                else
                {
                    oDISLog_WorkFlowBE.DocPath = fileName2;
                    oDISLog_WorkFlowBE.Doctype = "DelieveryNotes";
                }
                oDISLog_WorkFlowBAL.addWorkFlowInvoiceQueryActionsBAL(oDISLog_WorkFlowBE);
            }

        }
        else
        {
            oDISLog_WorkFlowBE.Action = "InvoiceInsertDocInformation";
            oDISLog_WorkFlowBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
            oDISLog_WorkFlowBE.DocPath = fileName1;
            oDISLog_WorkFlowBE.Doctype = "Credit";
            oDISLog_WorkFlowBAL.addWorkFlowInvoiceQueryActionsBAL(oDISLog_WorkFlowBE);
        }



        oDISLog_WorkFlowBE.Action = "InsertUpdateWorkflowActions";
        oDISLog_WorkFlowBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
        oDISLog_WorkFlowBE.StageCompleted = chkStageComplete.Checked ? true : false;

        oDISLog_WorkFlowBE.StageCompletedForWhom = "ACP008_StageCompleted";
        oDISLog_WorkFlowBE.Comment = txtComments.Text;

        //update the work flow action table each time for the comments and other fields( if stage is completed)
        oDISLog_WorkFlowBE.DiscrepancyWorkFlowID = Convert.ToInt32(GetQueryStringValue("DisWFlowID").ToString());
        oDISLog_WorkFlowBE.ApDocumentNumber = txtAPDocumentNumber.Text;
        //insert or update the record according to the stage completed status
        int? iResult = oDISLog_WorkFlowBAL.addWorkFlowActionsBAL(oDISLog_WorkFlowBE);

        DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
        DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();
        //For getting Inventory Action Log date and then update VendorActionElaspedTime in Trn_DiscrepancyLog Table
        oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID"));
        oDiscrepancyBE.Action = "GetInventoryLogDate";
        oDiscrepancyBE.DiscrepancyWorkFlowID = Convert.ToInt32(GetQueryStringValue("DisWFlowID"));
        DataTable dtAllDetails = oDiscrepancyBAL.GetAllDetailsOfWorkFlowBAL(oDiscrepancyBE);
        DateTime DtInventoryLogDate = DateTime.Now;

        if (dtAllDetails != null && dtAllDetails.Rows.Count > 0)
        {
            DtInventoryLogDate = Convert.ToDateTime(dtAllDetails.Rows[0]["LogDate"]);
        }
        oDiscrepancyBE.VendorActionElapsedTime = (decimal?)DateTime.Now.Subtract(DtInventoryLogDate).TotalDays;
        oDiscrepancyBE.Action = "UpdateVendorActionElapsedTime";
        oDiscrepancyBAL.UpdateVendorActionElapsedTimeBAL(oDiscrepancyBE);


        //now insert the work flow HTML according to the action taken only if the stage is completed
        //now insert the work flow HTML according to the action taken only if the stage is completed
        WorkflowHTML oWorkflowHTML = new WorkflowHTML();

        if (rdoVEN029Text1.Checked)
        {
            oDiscrepancyBE.GINHTML = oWorkflowHTML.function13(pColor: "Blue", pDate: DateTime.Now, SentTo: WebCommon.getGlobalResourceValue("GoodsIn"), DiscrePancyTextBottom: WebCommon.getGlobalResourceValue("ACP0008Workflow"));
            oDiscrepancyBE.GINActionRequired = true;
            oDiscrepancyBE.GINUserControl = "GIN015";
            oDiscrepancyBE.GINActionCategory = "AR";  //Action Required
        }
        else
        {
            oDiscrepancyBE.GINHTML = oWorkflowHTML.function3("White", null, "", "");
            oDiscrepancyBE.GINActionRequired = false;
        }

        oDiscrepancyBE.INVHTML = oWorkflowHTML.function3("White", null, "", "");
        oDiscrepancyBE.INVActionRequired = false;



        if (rdoDeliveredQty.Checked)
        {
            oDiscrepancyBE.CloseDiscrepancy = true;
            oDiscrepancyBE.APHTML = oWorkflowHTML.function13(pColor: "Yellow", pDate: DateTime.Now,
                    pUserName: Convert.ToString(Session["UserName"]), pAction: WebCommon.getGlobalResourceValue("ACP006WOrkflow"), aPDocument: txtAPDocumentNumber.Text, pComments: txtComments.Text);
            oDiscrepancyBE.APActionRequired = false;
        }
        else
        {
            oDiscrepancyBE.CloseDiscrepancy = false;
            oDiscrepancyBE.APHTML = oWorkflowHTML.function13(pColor: "Yellow", pDate: DateTime.Now,
                     pUserName: Convert.ToString(Session["UserName"]), pAction: WebCommon.getGlobalResourceValue("ACP010Workflow1"), pComments: txtComments.Text);
            oDiscrepancyBE.APActionRequired = false;
        }


        oDiscrepancyBE.VENHTML = oWorkflowHTML.function3("White", null, "", "");
        oDiscrepancyBE.VenActionRequired = false;

        oDiscrepancyBE.Action = "InsertHTML";

        oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
        oDiscrepancyBE.LoggedDateTime = DateTime.Now;
        oDiscrepancyBE.LevelNumber = 1;
        oDiscrepancyBE.ActionTakenBy = "ACP";
        
        int? iResult1 = oDiscrepancyBAL.addWorkFlowHTMLsBAL(oDiscrepancyBE);


        RedirectToPage(GetQueryStringValue("PreviousPage"));
    }
    private void InsertHTML()
    {
        DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
        DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();

        WorkflowHTML oWorkflowHTML = new WorkflowHTML();
        oDiscrepancyBE.GINHTML = oWorkflowHTML.function3("White", DateTime.Now, txtComments.Text.Trim());
        oDiscrepancyBE.GINActionRequired = false;

        oDiscrepancyBE.Action = "InsertHTML";
        oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
        oDiscrepancyBE.LoggedDateTime = DateTime.Now;
        oDiscrepancyBE.LevelNumber = 1;

        int? iResult = oDiscrepancyBAL.addWorkFlowHTMLsBAL(oDiscrepancyBE);
    }
    protected void btnBack_Click(object sender, EventArgs e)
    {
        //Redirecting to Previous Page
        RedirectToPage(GetQueryStringValue("PreviousPage"));
    }
}
﻿using System;
using System.Collections.Generic;
using System.Data;
using BaseControlLibrary;
using BusinessEntities.ModuleBE.AdminFunctions;
using BusinessEntities.ModuleBE.Discrepancy.APAction;
using BusinessEntities.ModuleBE.Discrepancy.LogDiscrepancy;
using BusinessEntities.ModuleBE.StockOverview;
using BusinessLogicLayer.ModuleBAL.AdminFunctions;
using BusinessLogicLayer.ModuleBAL.Discrepancy.CountrySetting;
using BusinessLogicLayer.ModuleBAL.Discrepancy.LogDiscrepancy;
using BusinessLogicLayer.ModuleBAL.StockOverview;
using WebUtilities;
using System.Configuration;
using Utilities;
using BusinessLogicLayer.ModuleBAL.Appointment.Booking;
using BusinessEntities.ModuleBE.Languages.Languages;
using System.IO;

public partial class APAction_ACP003 : CommonPage
{

    #region Global variables
    protected string DebitRaisedSuccessfully = WebCommon.getGlobalResourceValue("DebitRaisedSuccessfully");
    string DebitNoteAgainstDiscEmailSubject = WebCommon.getGlobalResourceValue("DebitNoteAgainstDiscEmailSubject");
    string PleaseentertheVendorVATReference = WebCommon.getGlobalResourceValue("PleaseentertheVendorVATReference");
    string PleaseselecttheOfficeDepotVATReference = WebCommon.getGlobalResourceValue("PleaseselecttheOfficeDepotVATReference");
    #endregion

    #region Events
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            this.BindCurrency();
            DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
            DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();
            oDiscrepancyBE.Action = "GetCurrencyByVendorID";
            if (GetQueryStringValue("disLogID") != null)
            {
                oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID"));
            }
            ddlCurrencyDebit.SelectedValue = oDiscrepancyBAL.GetCurrencyBAL(oDiscrepancyBE); 

            this.BindDebitReason();
            this.CheckDebitOrCreditNote();
            this.GetLabourCost();

            if (GetQueryStringValue("FromPage") != null && (GetQueryStringValue("FromPage").ToString().Trim() == "WrongPackSize"))
            {
                GetLabourCostForWrongPackSize();
            }

            if (GetQueryStringValue("FromPage") != null && (GetQueryStringValue("FromPage").ToString().Trim() == "FailPalletSpecification"))
            {
                GetLabourCostForFailPalletSpecification();
            }

            if (ViewState["VendorID"] != null)
                this.SetPOVendorVATReference(Convert.ToInt32(ViewState["VendorID"]));

            if (ViewState["SiteID"] != null)
                this.SetVendODVATReference(Convert.ToInt32(ViewState["SiteID"]));
        }
    }

    protected void btnCreditSave_Click(object sender, EventArgs e)
    {
        btnCreditSave.Enabled = false;
        DISLog_WorkFlowBE oDISLog_WorkFlowBE = new DISLog_WorkFlowBE();
        DISLog_WorkFlowBAL oDISLog_WorkFlowBAL = new DISLog_WorkFlowBAL();
        if (GetQueryStringValue("disLogID") != null && GetQueryStringValue("disLogID").ToString() != "")
        {
            //**********************************************************
            string Location = hdfLocation.Value;
            DiscrepancyBE oDiscrepancyBE1 = new DiscrepancyBE();
            DiscrepancyBAL oDiscrepancyBAL1 = new DiscrepancyBAL();
            oDiscrepancyBE1.Action = "UpdateLocation";
            oDiscrepancyBE1.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
            oDiscrepancyBE1.Location = Location;
            oDiscrepancyBE1.UserID = Convert.ToInt32(Session["UserID"]);
            string sResult1 = oDiscrepancyBAL1.updateDiscrepancyLocationBAL(oDiscrepancyBE1);
            //**********************************************************
            oDISLog_WorkFlowBE.Action = "InsertUpdateWorkflowActions";
            oDISLog_WorkFlowBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
            oDISLog_WorkFlowBE.ACP003_NoteType = 'C';
            oDISLog_WorkFlowBE.ACP003_NoteNumber = txtCreditNoteNumber.Text;
            oDISLog_WorkFlowBE.ACP003_LabourCost = txtCreditLabourCost.Text.Trim() != "" ? Convert.ToDecimal(txtCreditLabourCost.Text) : (decimal?)null;
            oDISLog_WorkFlowBE.ACP003_ActualValue = txtCreditValueActual.Text != "" ? Convert.ToDecimal(txtCreditValueActual.Text) : (decimal?)null;
            oDISLog_WorkFlowBE.StageCompletedForWhom = "ACP003_StageCompleted";
            if (chkCreditStageComplete.Checked)
            {
                oDISLog_WorkFlowBE.StageCompleted = true;
            }
            else
            {
                oDISLog_WorkFlowBE.StageCompleted = false;
            }
            oDISLog_WorkFlowBE.Comment = txtCreditComment.Text;

            oDISLog_WorkFlowBE.CostCenter = txtCostCenter.Text.Trim(); 

            //update the work flow action table each time for the comments and other fields( if stage is completed)
            oDISLog_WorkFlowBE.DiscrepancyWorkFlowID = Convert.ToInt32(GetQueryStringValue("DisWFlowID").ToString());

            //insert or update the record according to the stage completed status
            int? iResult = oDISLog_WorkFlowBAL.addWorkFlowActionsBAL(oDISLog_WorkFlowBE);

            if (GetQueryStringValue("FromPage") != null && GetQueryStringValue("FromPage").ToString().Trim() == "PrematureInvoice")
            {
                CreditSaveForPrematureInvoice();
            }
            else if (GetQueryStringValue("FromPage") != null && GetQueryStringValue("FromPage").ToString().Trim() == "FailPalletSpecification")
            {
                CreditSaveForFailPallets();
            }
            else if (GetQueryStringValue("FromPage") != null && GetQueryStringValue("FromPage").ToString().Trim() == "WrongPackSize")
            {
                CreditSaveForWrongPackSize();
            }
            else if (GetQueryStringValue("FromPage") != null && GetQueryStringValue("FromPage").ToString().Trim() == "QualityIssue")
            {
                CreditSaveForWrongPackSize();
            }
        }

        //Redirecting to Previous Page
        RedirectToPage(GetQueryStringValue("PreviousPage"));
    }

    protected void btnDebitSave_Click(object sender, EventArgs e)
    {
        btnDebitSave.Enabled = false;
        DISLog_WorkFlowBE oDISLog_WorkFlowBE = new DISLog_WorkFlowBE();
        DISLog_WorkFlowBAL oDISLog_WorkFlowBAL = new DISLog_WorkFlowBAL();
        if (GetQueryStringValue("disLogID") != null && GetQueryStringValue("disLogID").ToString() != "")
        {
            //**********************************************************
            string Location = hdfLocation.Value;
            DiscrepancyBE oDiscrepancyBE1 = new DiscrepancyBE();
            DiscrepancyBAL oDiscrepancyBAL1 = new DiscrepancyBAL();
            oDiscrepancyBE1.Action = "UpdateLocation";
            oDiscrepancyBE1.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
            oDiscrepancyBE1.Location = Location;
            oDiscrepancyBE1.UserID = Convert.ToInt32(Session["UserID"]);
            string sResult1 = oDiscrepancyBAL1.updateDiscrepancyLocationBAL(oDiscrepancyBE1);
            //**********************************************************
            var discripancyLogId = Convert.ToInt32(GetQueryStringValue("disLogID"));

            #region Inserting data Debit raise tables ...
            var discrepancyBE = new DiscrepancyBE();
            var discrepancyBAL = new DiscrepancyBAL();
            discrepancyBE.Action = "AddDebitRaise";
            discrepancyBE.DebitRaseType = "D";

            if (ViewState["PONumber"] != null)
                discrepancyBE.PurchaseOrderNumber = Convert.ToString(ViewState["PONumber"]);

            if (ViewState["SiteID"] != null)
                discrepancyBE.SiteID = Convert.ToInt32(ViewState["SiteID"]);

            if (ViewState["VendorID"] != null)
                discrepancyBE.VendorID = Convert.ToInt32(ViewState["VendorID"]);

            discrepancyBE.InvoiceNo = string.Empty;
            discrepancyBE.ReferenceNo = string.Empty;

            discrepancyBE.VendorVatReference = txtVendVendorVATReference.Text;
            discrepancyBE.DebitTotalValue = Convert.ToDecimal(txtDebitValueActual.Text);
            if (ViewState["ODVatCodeID"] != null)
                discrepancyBE.ODVatReferenceID = Convert.ToInt32(ViewState["ODVatCodeID"]);

            var txtCommEmailList = (ucTextbox)ucSDRCommunicationDebit1.FindControl("txtVendorEmailList");
            var txtAdditionalEmailList = (ucTextbox)ucSDRCommunicationDebit1.FindControl("txtAdditionalEmailList");
            var emailIds = string.Empty;
            if (txtCommEmailList != null && txtAdditionalEmailList != null)
            {
                if ((!string.IsNullOrEmpty(txtCommEmailList.Text) && !string.IsNullOrWhiteSpace(txtCommEmailList.Text)) &&
                    (!string.IsNullOrEmpty(txtAdditionalEmailList.Text) && !string.IsNullOrWhiteSpace(txtAdditionalEmailList.Text)))
                {
                    emailIds = string.Format("{0},{1}", txtCommEmailList.Text, txtAdditionalEmailList.Text);
                }
                else if ((!string.IsNullOrEmpty(txtCommEmailList.Text) && !string.IsNullOrWhiteSpace(txtCommEmailList.Text)))
                {
                    emailIds = txtCommEmailList.Text;
                }
                else if ((!string.IsNullOrEmpty(txtAdditionalEmailList.Text) && !string.IsNullOrWhiteSpace(txtAdditionalEmailList.Text)))
                {
                    emailIds = txtAdditionalEmailList.Text;
                }
            }

            discrepancyBE.CommunicationEmails = emailIds;
            discrepancyBE.InternalComments = txtDebitComment.Text;

            //if (ddlCurrencyDebit.SelectedIndex > 0)
            //{
                discrepancyBE.Currency = new CurrencyBE();
                discrepancyBE.Currency.CurrencyId = Convert.ToInt32(ddlCurrencyDebit.SelectedValue);
          //  }

            discrepancyBE.DiscrepancyLogID = discripancyLogId;
            discrepancyBE.Status = "A"; // A is an Active status of Debit Raise.
            discrepancyBE.UserID = Convert.ToInt32(Session["UserID"]);
            discrepancyBE.Country = new MAS_CountryBE();

            //if (ucCountry.innerControlddlCountry.Items.Count > 0)
            //    discrepancyBE.Country.CountryID = Convert.ToInt32(ucCountry.innerControlddlCountry.SelectedValue);

            discrepancyBE.APActionRef = "ACP003";

            var debitRaiseId = discrepancyBAL.addDebitRaiseBAL(discrepancyBE);
            if (debitRaiseId > 0)
            {
                var discrepancy = new DiscrepancyBE();
                discrepancy.Action = "AddDebitRaiseItem";
                discrepancy.DebitRaiseId = debitRaiseId;
                discrepancy.StateValueDebited = Convert.ToDecimal(txtDebitValueActual.Text);

                if (ddlReasonDebit.SelectedIndex > 0)
                    discrepancy.DebitReasonId = Convert.ToInt32(ddlReasonDebit.SelectedValue);

                discrepancyBAL.addDebitRaiseItemBAL(discrepancy);
            }

            #endregion

            #region Getting here Saved Debit Raise data & Workflow entry ...
            if (debitRaiseId > 0)
            {
                discrepancyBE = new DiscrepancyBE();
                //var discrepancyBAL = new DiscrepancyBAL();
                discrepancyBE.Action = "GetAllDebitRaise";
                discrepancyBE.DebitRaiseId = debitRaiseId;
                var findDiscrepancy = discrepancyBAL.GetAllDebitRaiseBAL(discrepancyBE).Find(x => x.APActionRef.Equals("ACP003"));
                if (findDiscrepancy != null)
                {
                    ViewState["DebitNoteNo"] = findDiscrepancy.DebitNoteNo;
                    DebitRaisedSuccessfully = DebitRaisedSuccessfully.Replace("##RaisedDebitNumber##", findDiscrepancy.DebitNoteNo);
                    DebitNoteAgainstDiscEmailSubject = DebitNoteAgainstDiscEmailSubject.Replace("##DebitNoteNumber##", findDiscrepancy.DebitNoteNo);

                    #region Logic to Save & Send email ...
                    var oAPPBOK_CommunicationBAL = new APPBOK_CommunicationBAL();
                    var oLanguages = oAPPBOK_CommunicationBAL.GetLanguages();
                    foreach (MAS_LanguageBE objLanguage in oLanguages)
                    {
                        bool MailSentInLanguage = false;
                        if (objLanguage.LanguageID.Equals(findDiscrepancy.Vendor.LanguageID))
                            MailSentInLanguage = true;

                        var path = AppDomain.CurrentDomain.BaseDirectory.ToLower();
                        //var templatesPath = @"emailtemplates/communication1";
                        var templateFile = string.Format(@"{0}emailtemplates/communication1/DebitNoteAgainstDiscrepancy.english.htm", path);

                        #region Setting reason as per the language ...
                        switch (objLanguage.LanguageID)
                        {
                            case 1:
                                Page.UICulture = clsConstants.EnglishISO; //"en-US"; // English
                                break;
                            case 2:
                                Page.UICulture = clsConstants.FranceISO; //"fr"; // France
                                break;
                            case 3:
                                Page.UICulture = clsConstants.GermanyISO; //"de"; // German
                                break;
                            case 4:
                                Page.UICulture = clsConstants.NederlandISO; //"nl"; // Dutch
                                break;
                            case 5:
                                Page.UICulture = clsConstants.SpainISO; //"es"; // Spanish
                                break;
                            case 6:
                                Page.UICulture = clsConstants.ItalyISO; //"it"; // Italy
                                break;
                            case 7:
                                Page.UICulture = clsConstants.CzechISO; //"cz"; // Czech
                                break;
                            default:
                                Page.UICulture = clsConstants.EnglishISO; // "en-US"; // English
                                break;
                        }

                        #endregion

                        #region  Prepairing html body format ...
                        string htmlBody = null;
                        using (StreamReader sReader = new StreamReader(templateFile.ToLower()))
                        {
                            htmlBody = sReader.ReadToEnd();
                            //htmlBody = htmlBody.Replace("{logoInnerPath}", getAbsolutePath());

                            htmlBody = htmlBody.Replace("{VendorNumber}", findDiscrepancy.Vendor.Vendor_No);
                            htmlBody = htmlBody.Replace("{DebitNoteNumber}", WebCommon.getGlobalResourceValue("DebitNoteNumber"));
                            htmlBody = htmlBody.Replace("{DebitNoteNumberValue}", findDiscrepancy.DebitNoteNo);
                            htmlBody = htmlBody.Replace("{VendorName}", findDiscrepancy.Vendor.VendorName);
                            htmlBody = htmlBody.Replace("{OurVATReference}", WebCommon.getGlobalResourceValue("OurVATReference"));

                            if (!string.IsNullOrEmpty(findDiscrepancy.OurVATReference) && !string.IsNullOrWhiteSpace(findDiscrepancy.OurVATReference))
                                htmlBody = htmlBody.Replace("{OurVATReferenceValue}", findDiscrepancy.OurVATReference);
                            else
                                htmlBody = htmlBody.Replace("{OurVATReferenceValue}", string.Empty);

                            htmlBody = htmlBody.Replace("{VendorAddress1}", findDiscrepancy.Vendor.address1);
                            htmlBody = htmlBody.Replace("{YourVATReference}", WebCommon.getGlobalResourceValue("YourVATReference"));

                            if (!string.IsNullOrEmpty(findDiscrepancy.YourVATReference) && !string.IsNullOrWhiteSpace(findDiscrepancy.YourVATReference))
                                htmlBody = htmlBody.Replace("{YourVATReferenceValue}", findDiscrepancy.YourVATReference);
                            else
                                htmlBody = htmlBody.Replace("{YourVATReferenceValue}", string.Empty);

                            htmlBody = htmlBody.Replace("{VendorAddress2}", findDiscrepancy.Vendor.address2);
                            htmlBody = htmlBody.Replace("{DebitReason}", WebCommon.getGlobalResourceValue("DebitReason"));
                            htmlBody = htmlBody.Replace("{DebitReasonValue}", findDiscrepancy.Reason);

                            if (!string.IsNullOrEmpty(findDiscrepancy.Vendor.VMPPIN) && !string.IsNullOrWhiteSpace(findDiscrepancy.Vendor.VMPPIN))
                                htmlBody = htmlBody.Replace("{VMPPIN}", findDiscrepancy.Vendor.VMPPIN);
                            else
                                htmlBody = htmlBody.Replace("{VMPPIN}", string.Empty);

                            if (!string.IsNullOrEmpty(findDiscrepancy.Vendor.VMPPOU) && !string.IsNullOrWhiteSpace(findDiscrepancy.Vendor.VMPPOU))
                                htmlBody = htmlBody.Replace("{VMPPOU}", findDiscrepancy.Vendor.VMPPOU);
                            else
                                htmlBody = htmlBody.Replace("{VMPPOU}", string.Empty);

                            if (!string.IsNullOrEmpty(findDiscrepancy.Vendor.city) && !string.IsNullOrWhiteSpace(findDiscrepancy.Vendor.city))
                                htmlBody = htmlBody.Replace("{VendorCity}", findDiscrepancy.Vendor.city);
                            else
                                htmlBody = htmlBody.Replace("{VendorCity}", string.Empty);

                            htmlBody = htmlBody.Replace("{DearSirMadam}", WebCommon.getGlobalResourceValue("DearSirMadam"));
                            htmlBody = htmlBody.Replace("{Date}", WebCommon.getGlobalResourceValue("Date"));

                            var debitRaiseDate = Convert.ToDateTime(findDiscrepancy.DebitRaiseDate);
                            htmlBody = htmlBody.Replace("{DateValue}", debitRaiseDate.ToString("dd/MM/yyyy"));

                            htmlBody = htmlBody.Replace("{DebitLetterMessage1}", WebCommon.getGlobalResourceValue("DebitLetterMessage1"));
                            htmlBody = htmlBody.Replace("{VDRNo}", WebCommon.getGlobalResourceValue("VDRNoKey"));
                            htmlBody = htmlBody.Replace("{VDRNoValue}", findDiscrepancy.VDRNo);
                            htmlBody = htmlBody.Replace("{DebitLetterMessage2}", WebCommon.getGlobalResourceValue("DebitLetterMessage2"));

                            var vdrDateValue = Convert.ToDateTime(findDiscrepancy.DiscrepancyLogDate);
                            htmlBody = htmlBody.Replace("{VDRDateValue}", vdrDateValue.ToString("dd/MM/yyyy"));

                            htmlBody = htmlBody.Replace("{DebitLetterMessage3}", WebCommon.getGlobalResourceValue("DebitLetterMessage3"));
                            htmlBody = htmlBody.Replace("{DebitValue}", Convert.ToString(findDiscrepancy.StateValueDebited));
                            htmlBody = htmlBody.Replace("{DebitCurrencyValue}", Convert.ToString(findDiscrepancy.Currency.CurrencyName));
                            htmlBody = htmlBody.Replace("{DebitLetterMessage4}", WebCommon.getGlobalResourceValue("DebitLetterMessage4"));
                            htmlBody = htmlBody.Replace("{DebiNoteComments}", findDiscrepancy.Comment);
                            htmlBody = htmlBody.Replace("{DebitLetterMessage5}", WebCommon.getGlobalResourceValue("DebitLetterMessage5"));
                            htmlBody = htmlBody.Replace("{Yoursfaithfully}", WebCommon.getGlobalResourceValue("Yoursfaithfully"));
                            htmlBody = htmlBody.Replace("{APClerkName}", findDiscrepancy.CreatedBy);

                            if (!string.IsNullOrEmpty(findDiscrepancy.CreatedByContactNumber) && !string.IsNullOrWhiteSpace(findDiscrepancy.CreatedByContactNumber))
                                htmlBody = htmlBody.Replace("{APClerkContactNo}", findDiscrepancy.CreatedByContactNumber);
                            else
                                htmlBody = htmlBody.Replace("{APClerkContactNo}", string.Empty);

                            var apAddress = string.Empty;
                            if (!string.IsNullOrEmpty(findDiscrepancy.APAddress1) && !string.IsNullOrEmpty(findDiscrepancy.APAddress2)
                                && !string.IsNullOrEmpty(findDiscrepancy.APAddress3) && !string.IsNullOrEmpty(findDiscrepancy.APAddress4)
                                && !string.IsNullOrEmpty(findDiscrepancy.APAddress5) && !string.IsNullOrEmpty(findDiscrepancy.APAddress6))
                            {
                                apAddress = string.Format("{0},&nbsp;{1},&nbsp;{2},&nbsp;{3},&nbsp;{4},&nbsp;{5}", findDiscrepancy.APAddress1, findDiscrepancy.APAddress2,
                                    findDiscrepancy.APAddress3, findDiscrepancy.APAddress4, findDiscrepancy.APAddress5, findDiscrepancy.APAddress6);
                            }
                            else if (!string.IsNullOrEmpty(findDiscrepancy.APAddress1) && !string.IsNullOrEmpty(findDiscrepancy.APAddress2)
                                && !string.IsNullOrEmpty(findDiscrepancy.APAddress3) && !string.IsNullOrEmpty(findDiscrepancy.APAddress4)
                                && !string.IsNullOrEmpty(findDiscrepancy.APAddress5))
                            {
                                apAddress = string.Format("{0},&nbsp;{1},&nbsp;{2},&nbsp;{3},&nbsp;{4}", findDiscrepancy.APAddress1, findDiscrepancy.APAddress2,
                                    findDiscrepancy.APAddress3, findDiscrepancy.APAddress4, findDiscrepancy.APAddress5);
                            }
                            else if (!string.IsNullOrEmpty(findDiscrepancy.APAddress1) && !string.IsNullOrEmpty(findDiscrepancy.APAddress2)
                               && !string.IsNullOrEmpty(findDiscrepancy.APAddress3) && !string.IsNullOrEmpty(findDiscrepancy.APAddress4))
                            {
                                apAddress = string.Format("{0},&nbsp;{1},&nbsp;{2},&nbsp;{3}", findDiscrepancy.APAddress1, findDiscrepancy.APAddress2,
                                    findDiscrepancy.APAddress3, findDiscrepancy.APAddress4);
                            }
                            else if (!string.IsNullOrEmpty(findDiscrepancy.APAddress1) && !string.IsNullOrEmpty(findDiscrepancy.APAddress2)
                               && !string.IsNullOrEmpty(findDiscrepancy.APAddress3))
                            {
                                apAddress = string.Format("{0},&nbsp;{1},&nbsp;{2}", findDiscrepancy.APAddress1, findDiscrepancy.APAddress2,
                                    findDiscrepancy.APAddress3);
                            }
                            else if (!string.IsNullOrEmpty(findDiscrepancy.APAddress1) && !string.IsNullOrEmpty(findDiscrepancy.APAddress2))
                            {
                                apAddress = string.Format("{0},&nbsp;{1}", findDiscrepancy.APAddress1, findDiscrepancy.APAddress2);
                            }
                            else
                            {
                                apAddress = findDiscrepancy.APAddress1;
                            }

                            if (!string.IsNullOrEmpty(findDiscrepancy.APPincode))
                                apAddress = string.Format("{0},&nbsp;{1}", apAddress, findDiscrepancy.APPincode);

                            htmlBody = htmlBody.Replace("{APClerkAddress}", apAddress);

                        }
                        #endregion

                        #region Sending and saving email details ...
                        string[] sMailAddress = emailIds.Split(',');
                        var sentToWithLink = new System.Text.StringBuilder();
                        for (int index = 0; index < sMailAddress.Length; index++)
                            sentToWithLink.Append("<a target='_blank' href='" + Common.EncryptQuery("../LogDiscrepancy/DISLog_ReSendCommunication.aspx?CommTitle=DebitRaised&DebitRaiseId=" + debitRaiseId + "&CommunicationType=DebitRaised") + "' onclick='window.open(this.href, this.target,\"scrollbars=1,left=100px,top=100px,width=1000px,height=800px\"); return false;'>" + sMailAddress[index] + "</a>" + System.Environment.NewLine);

                        var discrepancyMailBE = new DiscrepancyMailBE();
                        discrepancyMailBE.Action = "AddDebitRaiseCommunication";
                        discrepancyMailBE.DebitRaiseId = debitRaiseId;
                        discrepancyMailBE.mailSubject = DebitNoteAgainstDiscEmailSubject;
                        discrepancyMailBE.sentTo = emailIds;
                        discrepancyMailBE.mailBody = htmlBody;
                        discrepancyMailBE.SendByID = Convert.ToInt32(Session["UserID"]);
                        discrepancyMailBE.IsMailSent = MailSentInLanguage;
                        discrepancyMailBE.languageID = objLanguage.LanguageID;
                        discrepancyMailBE.MailSentInLanguage = MailSentInLanguage;
                        discrepancyMailBE.CommTitle = "DebitRaised";
                        discrepancyMailBE.SentToWithLink = sentToWithLink.ToString();
                        var debitRaiseCommId = discrepancyBAL.addDebitRaiseCommunicationBAL(discrepancyMailBE);
                        if (MailSentInLanguage)
                        {
                            var emailToAddress = emailIds;
                            var emailFromAddress = Convert.ToString(ConfigurationManager.AppSettings["fromAddress"]);
                            var emailToSubject = DebitNoteAgainstDiscEmailSubject;
                            var emailBody = htmlBody;
                            Utilities.clsEmail oclsEmail = new Utilities.clsEmail();
                            oclsEmail.sendMail(emailToAddress, emailBody, emailToSubject, emailFromAddress, true);
                        }
                        #endregion
                    }
                    #endregion
                    Page.UICulture = Convert.ToString(Session["CultureInfo"]);
                }

                #region Data is being Insert in Discrepancy Workflow tables ...

                oDISLog_WorkFlowBE.Action = "InsertUpdateWorkflowActions";
                oDISLog_WorkFlowBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
                oDISLog_WorkFlowBE.ACP003_NoteType = 'D';

                if (ViewState["DebitNoteNo"] != null)
                    oDISLog_WorkFlowBE.ACP003_NoteNumber = Convert.ToString(ViewState["DebitNoteNo"]); //txtDebitNoteNumber.Text;

                oDISLog_WorkFlowBE.ACP003_LabourCost = txtDebitLabourCost.Text.Trim() != "" ? Convert.ToDecimal(txtDebitLabourCost.Text) : (decimal?)null;
                oDISLog_WorkFlowBE.ACP003_ActualValue = txtDebitValueActual.Text.Trim() != "" ? Convert.ToDecimal(txtDebitValueActual.Text) : (decimal?)null;
                oDISLog_WorkFlowBE.StageCompletedForWhom = "ACP003_StageCompleted";
                if (chkDebitStageComplete.Checked)
                {
                    oDISLog_WorkFlowBE.StageCompleted = true;
                }
                else
                {
                    oDISLog_WorkFlowBE.StageCompleted = false;
                }
                oDISLog_WorkFlowBE.Comment = txtDebitComment.Text;

                oDISLog_WorkFlowBE.CostCenter = txtCostCenter1.Text.Trim(); // Debit Cost Center

                //update the work flow action table each time for the comments and other fields( if stage is completed)
                oDISLog_WorkFlowBE.DiscrepancyWorkFlowID = Convert.ToInt32(GetQueryStringValue("DisWFlowID").ToString());

                //insert or update the record according to the stage completed status
                int? iResult = oDISLog_WorkFlowBAL.addWorkFlowActionsBAL(oDISLog_WorkFlowBE);
                if (GetQueryStringValue("FromPage") != null && GetQueryStringValue("FromPage").ToString().Trim() == "PrematureInvoice")
                {
                    DebitSaveForPrematureInvoice();
                }
                else if (GetQueryStringValue("FromPage") != null && GetQueryStringValue("FromPage").ToString().Trim() == "FailPalletSpecification")
                {
                    DebitSaveForFailPallets();
                }
                else if (GetQueryStringValue("FromPage") != null && GetQueryStringValue("FromPage").ToString().Trim() == "WrongPackSize")
                {
                    DebitSaveForWrongPackSize();
                }
                else if (GetQueryStringValue("FromPage") != null && GetQueryStringValue("FromPage").ToString().Trim() == "QualityIssue")
                {
                    DebitSaveForWrongPackSize();
                }

                #endregion
            }
            #endregion
        }

        //System.Web.UI.ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + DebitRaisedSuccessfully + "')", true);

        //Redirecting to Previous Page
        RedirectToPage(GetQueryStringValue("PreviousPage"));
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        //Redirecting to Previous Page
        RedirectToPage(GetQueryStringValue("PreviousPage"));
    }

    #endregion

    #region Methods

    private void CheckDebitOrCreditNote()
    {
        if (GetQueryStringValue("SiteID") != null)
        {
            /* Check for the site CommunicationWithEscalationType (whether Credit or Debit) */
            MAS_SiteBE oMAS_SiteBE = new MAS_SiteBE();
            MAS_SiteBAL oMAS_SiteBAL = new MAS_SiteBAL();
            oMAS_SiteBE.Action = "GetSiteDisSettings";
            oMAS_SiteBE.SiteID = Convert.ToInt32(GetQueryStringValue("SiteID").ToString());

            oMAS_SiteBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();

            List<MAS_SiteBE> lstSiteDetails = oMAS_SiteBAL.GetSiteMisSettingBAL(oMAS_SiteBE);
            if (lstSiteDetails != null && lstSiteDetails.Count > 0)
            {
                if (lstSiteDetails[0].DIS_CommunicationWithEscalationType.Trim() != "D")
                {
                    pnlACP002d.Style.Add("display", "block");
                    pnlACP002c.Style.Add("display", "none");
                    rdoACP003bDebit.Checked = true;
                }
                else if (lstSiteDetails[0].DIS_CommunicationWithEscalationType.Trim() != "C")
                {
                    pnlACP002d.Style.Add("display", "none");
                    pnlACP002c.Style.Add("display", "block");
                    rdoACP003aCredit.Checked = true;
                }
            }
        }
    }

    private void GetLabourCost()
    {
        if (GetQueryStringValue("disLogID") != null)
        {
            DiscrepancyBE oNewDiscrepancyBE = new DiscrepancyBE();
            DiscrepancyBAL oNewDiscrepancyBAL = new DiscrepancyBAL();
            oNewDiscrepancyBE.Action = "GetDiscrepancyLog";
            oNewDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
            List<DiscrepancyBE> lstDisLog = oNewDiscrepancyBAL.GetDiscrepancyLogDetailsBAL(oNewDiscrepancyBE);
            if (lstDisLog != null && lstDisLog.Count > 0)
            {
                txtCreditLabourCost.Text = lstDisLog[0].Freight_Charges.ToString();
                txtCreditLabourCost.ReadOnly = true;
                txtDebitLabourCost.Text = lstDisLog[0].Freight_Charges.ToString();
                txtDebitLabourCost.ReadOnly = true;

                if (lstDisLog[0].VendorID != null)
                    ucSDRCommunicationDebit1.FillAPContacts(Convert.ToInt32(lstDisLog[0].VendorID));

                ViewState["PONumber"] = lstDisLog[0].PurchaseOrderNumber;
                ViewState["SiteID"] = lstDisLog[0].Site.SiteID;
                ViewState["VendorID"] = lstDisLog[0].VendorID;
                txtCostCenter.Text = lstDisLog[0].Site.CostCenter; // Credit Cost Centre
                txtCostCenter1.Text = lstDisLog[0].Site.CostCenter; // Debit Cost Centre
            }
        }
    }

    private void GetLabourCostForFailPalletSpecification()
    {
        if (GetQueryStringValue("disLogID") != null)
        {

            DiscrepancyBE oNewDiscrepancyBE = new DiscrepancyBE();
            DiscrepancyBAL oNewDiscrepancyBAL = new DiscrepancyBAL();
            oNewDiscrepancyBE.Action = "GetDiscrepancyLog";
            oNewDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
            List<DiscrepancyBE> lstDisLog = oNewDiscrepancyBAL.GetDiscrepancyLogDetailsBAL(oNewDiscrepancyBE);
            if (lstDisLog != null && lstDisLog.Count > 0)
            {
                txtCreditLabourCost.Text = lstDisLog[0].TotalLabourCost.ToString();
                txtCreditLabourCost.ReadOnly = true;
                txtDebitLabourCost.Text = lstDisLog[0].TotalLabourCost.ToString();
                txtDebitLabourCost.ReadOnly = true;
                txtCostCenter.Text = lstDisLog[0].Site.CostCenter; // Credit Cost Centre
                txtCostCenter1.Text = lstDisLog[0].Site.CostCenter; // Debit Cost Centre
            }
        }
    }

    private void GetLabourCostForWrongPackSize()
    {
        DISLog_WorkFlowBE oDISLog_WorkFlowBE = new DISLog_WorkFlowBE();
        DISLog_WorkFlowBAL oDISLog_WorkFlowBAL = new DISLog_WorkFlowBAL();

        oDISLog_WorkFlowBE.Action = "GetDataForSpecificAction";
        oDISLog_WorkFlowBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());

        oDISLog_WorkFlowBE.StageCompletedForWhom = "GIN010_StageCompleted";
        DataTable dtGIN010 = oDISLog_WorkFlowBAL.GetDataForSpecificActionBAL(oDISLog_WorkFlowBE);
        if (dtGIN010 != null && dtGIN010.Rows.Count > 0)
        {
            txtCreditLabourCost.Text = dtGIN010.Rows[0]["GIN010_LabourCost"].ToString();
            txtDebitLabourCost.Text = dtGIN010.Rows[0]["GIN010_LabourCost"].ToString();
            txtCreditLabourCost.ReadOnly = true;
            txtDebitLabourCost.ReadOnly = true;
            txtCostCenter.Text = dtGIN010.Rows[0]["CostCenter"].ToString();
            txtCostCenter1.Text = dtGIN010.Rows[0]["CostCenter"].ToString();
        }
    }

    public void CreditSaveForPrematureInvoice()
    {
        DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
        DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();
        WorkflowHTML oWorkflowHTML = new WorkflowHTML();

        DISLog_WorkFlowBE oDISLog_WorkFlowBE = new DISLog_WorkFlowBE();
        DISLog_WorkFlowBAL oDISLog_WorkFlowBAL = new DISLog_WorkFlowBAL();

        oDISLog_WorkFlowBE.Action = "GetDataForSpecificAction";
        oDISLog_WorkFlowBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
        oDISLog_WorkFlowBE.StageCompletedForWhom = "VEN022_StageCompleted";
        DataTable dt = oDISLog_WorkFlowBAL.GetDataForSpecificActionBAL(oDISLog_WorkFlowBE);

        if (chkCreditStageComplete.Checked)
        {

            if (dt != null && dt.Rows.Count > 0)
            {
                //Vendor has taken the action, close the discrepancy
                oDiscrepancyBE.CloseDiscrepancy = true;
            }
            oDiscrepancyBE.GINHTML = oWorkflowHTML.function3("White", null, "", "");
            oDiscrepancyBE.GINActionRequired = false;

            oDiscrepancyBE.INVHTML = oWorkflowHTML.function3("White", null, "", "");
            oDiscrepancyBE.INVActionRequired = false;

            oDiscrepancyBE.APHTML = oWorkflowHTML.function1(DateTime.Now, null, null, "Yellow", "", "Credit note raised",
                //Session["UserName"].ToString(), 
                UIUtility.GetODVendorUserName(Convert.ToString(Session["Role"]), Convert.ToString(Session["UserName"]), Convert.ToString(Session["LoginID"])),
                txtCreditComment.Text.Trim(), "", "", "", "", "", txtCreditNoteNumber.Text.Trim()
                , "", "", "", "", "", "", "", "", "", "", txtCreditValueActual.Text, pCostCenter: txtCostCenter.Text);
            oDiscrepancyBE.APActionRequired = false;

            oDiscrepancyBE.VENHTML = oWorkflowHTML.function3("White", null, "", "");
            oDiscrepancyBE.VenActionRequired = false;

            oDiscrepancyBE.Action = "InsertHTML";
            oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
            oDiscrepancyBE.LoggedDateTime = DateTime.Now;
            oDiscrepancyBE.LevelNumber = 1;
            int? iResult1 = oDiscrepancyBAL.addWorkFlowHTMLsBAL(oDiscrepancyBE);
        }
        else
        {
            oDiscrepancyBE.APHTML = oWorkflowHTML.function3("White", DateTime.Now, txtDebitComment.Text.Trim());
            oDiscrepancyBE.APActionRequired = false;
            oDiscrepancyBE.Action = "InsertHTML";
            oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
            oDiscrepancyBE.LoggedDateTime = DateTime.Now;
            oDiscrepancyBE.LevelNumber = 1;
            int? iResult = oDiscrepancyBAL.addWorkFlowHTMLsBAL(oDiscrepancyBE);
        }
    }

    public void CreditSaveForFailPallets()
    {
        DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
        DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();
        WorkflowHTML oWorkflowHTML = new WorkflowHTML();

        DISLog_WorkFlowBE oDISLog_WorkFlowBE = new DISLog_WorkFlowBE();
        DISLog_WorkFlowBAL oDISLog_WorkFlowBAL = new DISLog_WorkFlowBAL();

        oDISLog_WorkFlowBE.Action = "GetDataForSpecificAction";
        oDISLog_WorkFlowBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
        oDISLog_WorkFlowBE.StageCompletedForWhom = "VEN021_StageCompleted";
        DataTable dt = oDISLog_WorkFlowBAL.GetDataForSpecificActionBAL(oDISLog_WorkFlowBE);

        if (chkCreditStageComplete.Checked)
        {

            if (dt != null && dt.Rows.Count > 0)
            {
                //Vendor has taken the action, close the discrepancy
                oDiscrepancyBE.CloseDiscrepancy = true;
            }
            oDiscrepancyBE.GINHTML = oWorkflowHTML.function3("White", null, "", "");
            oDiscrepancyBE.GINActionRequired = false;

            oDiscrepancyBE.INVHTML = oWorkflowHTML.function3("White", null, "", "");
            oDiscrepancyBE.INVActionRequired = false;

            oDiscrepancyBE.APHTML = oWorkflowHTML.function1(DateTime.Now, null, null, "Yellow", "", "Credit note raised",
                //Session["UserName"].ToString(), 
                UIUtility.GetODVendorUserName(Convert.ToString(Session["Role"]), Convert.ToString(Session["UserName"]), Convert.ToString(Session["LoginID"])),
                txtCreditComment.Text.Trim(), "", "", "", "", "", txtCreditNoteNumber.Text.Trim()
                , "", "", "", "", "", "", "", "", "", "", txtCreditValueActual.Text, pCostCenter: txtCostCenter.Text);
            oDiscrepancyBE.APActionRequired = false;

            oDiscrepancyBE.VENHTML = oWorkflowHTML.function3("White", null, "", "");
            oDiscrepancyBE.VenActionRequired = false;

            oDiscrepancyBE.Action = "InsertHTML";
            oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
            oDiscrepancyBE.LoggedDateTime = DateTime.Now;
            oDiscrepancyBE.LevelNumber = 1;
            int? iResult1 = oDiscrepancyBAL.addWorkFlowHTMLsBAL(oDiscrepancyBE);
        }
        else
        {
            oDiscrepancyBE.APHTML = oWorkflowHTML.function3("White", DateTime.Now, txtDebitComment.Text.Trim());
            oDiscrepancyBE.APActionRequired = false;
            oDiscrepancyBE.Action = "InsertHTML";
            oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
            oDiscrepancyBE.LoggedDateTime = DateTime.Now;
            oDiscrepancyBE.LevelNumber = 1;
            int? iResult = oDiscrepancyBAL.addWorkFlowHTMLsBAL(oDiscrepancyBE);
        }
    }

    public void CreditSaveForWrongPackSize()
    {
        DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
        DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();
        WorkflowHTML oWorkflowHTML = new WorkflowHTML();

        DISLog_WorkFlowBE oDISLog_WorkFlowBE = new DISLog_WorkFlowBE();
        DISLog_WorkFlowBAL oDISLog_WorkFlowBAL = new DISLog_WorkFlowBAL();

        oDISLog_WorkFlowBE.Action = "GetDataForSpecificAction";
        oDISLog_WorkFlowBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
        oDISLog_WorkFlowBE.StageCompletedForWhom = "VEN019_StageCompleted";
        DataTable dtVEN019 = oDISLog_WorkFlowBAL.GetDataForSpecificActionBAL(oDISLog_WorkFlowBE);
        if (dtVEN019 != null && dtVEN019.Rows.Count > 0)
        {
            oDiscrepancyBE.CloseDiscrepancy = true;
        }

        if (chkCreditStageComplete.Checked)
        {

            oDiscrepancyBE.GINHTML = oWorkflowHTML.function3("White", null, "", "");
            oDiscrepancyBE.GINActionRequired = false;

            oDiscrepancyBE.INVHTML = oWorkflowHTML.function3("White", null, "", "");
            oDiscrepancyBE.INVActionRequired = false;

            oDiscrepancyBE.APHTML = oWorkflowHTML.function1(DateTime.Now, null, null, "Yellow", "", "Credit note raised",
                //Session["UserName"].ToString(), 
                UIUtility.GetODVendorUserName(Convert.ToString(Session["Role"]), Convert.ToString(Session["UserName"]), Convert.ToString(Session["LoginID"])),
                txtCreditComment.Text.Trim(), "", "", "", "", "", txtCreditNoteNumber.Text.Trim()
                , "", "", "", "", "", "", "", "", "", "", txtCreditValueActual.Text, pCostCenter:txtCostCenter.Text);
            oDiscrepancyBE.APActionRequired = false;

            oDiscrepancyBE.VENHTML = oWorkflowHTML.function3("White", null, "", "");
            oDiscrepancyBE.VenActionRequired = false;

            oDiscrepancyBE.Action = "InsertHTML";

            oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
            oDiscrepancyBE.LoggedDateTime = DateTime.Now;
            oDiscrepancyBE.LevelNumber = 1;
            int? iResult1 = oDiscrepancyBAL.addWorkFlowHTMLsBAL(oDiscrepancyBE);

        }
        else
        {
            oDiscrepancyBE.APHTML = oWorkflowHTML.function3("White", DateTime.Now, txtDebitComment.Text.Trim());
            oDiscrepancyBE.APActionRequired = false;
            oDiscrepancyBE.Action = "InsertHTML";
            oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
            oDiscrepancyBE.LoggedDateTime = DateTime.Now;
            oDiscrepancyBE.LevelNumber = 1;
            int? iResult = oDiscrepancyBAL.addWorkFlowHTMLsBAL(oDiscrepancyBE);
        }
    }

    public void DebitSaveForPrematureInvoice()
    {
        DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
        DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();
        WorkflowHTML oWorkflowHTML = new WorkflowHTML();

        DISLog_WorkFlowBE oDISLog_WorkFlowBE = new DISLog_WorkFlowBE();
        DISLog_WorkFlowBAL oDISLog_WorkFlowBAL = new DISLog_WorkFlowBAL();

        oDISLog_WorkFlowBE.Action = "GetDataForSpecificAction";
        oDISLog_WorkFlowBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
        oDISLog_WorkFlowBE.StageCompletedForWhom = "VEN022_StageCompleted";
        DataTable dt = oDISLog_WorkFlowBAL.GetDataForSpecificActionBAL(oDISLog_WorkFlowBE);


        if (chkDebitStageComplete.Checked)
        {

            if (dt != null && dt.Rows.Count > 0)
            {
                //Vendor has taken the action, close the discrepancy
                oDiscrepancyBE.CloseDiscrepancy = true;
            }
            oDiscrepancyBE.GINHTML = oWorkflowHTML.function3("White", null, "", "");
            oDiscrepancyBE.GINActionRequired = false;

            oDiscrepancyBE.INVHTML = oWorkflowHTML.function3("White", null, "", "");
            oDiscrepancyBE.INVActionRequired = false;

            string debitNoteNumber = string.Empty;
            if (ViewState["DebitNoteNo"] != null)
                debitNoteNumber = Convert.ToString(ViewState["DebitNoteNo"]);

            oDiscrepancyBE.APHTML = oWorkflowHTML.function1(DateTime.Now, null, null, "Yellow", "", "Debit note raised",
                //Session["UserName"].ToString(), 
                UIUtility.GetODVendorUserName(Convert.ToString(Session["Role"]), Convert.ToString(Session["UserName"]), Convert.ToString(Session["LoginID"])),
                txtDebitComment.Text.Trim(), "", debitNoteNumber
                , "", "", "", "", "", "", "", "", "", "", "", "", "", "", txtDebitValueActual.Text, pCostCenter:txtCostCenter1.Text);
            oDiscrepancyBE.APActionRequired = false;

            oDiscrepancyBE.VENHTML = oWorkflowHTML.function3("White", null, "", "");
            oDiscrepancyBE.VenActionRequired = false;

            oDiscrepancyBE.Action = "InsertHTML";
            oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
            oDiscrepancyBE.LoggedDateTime = DateTime.Now;
            oDiscrepancyBE.LevelNumber = 1;
            int? iResult1 = oDiscrepancyBAL.addWorkFlowHTMLsBAL(oDiscrepancyBE);
        }
        else
        {
            oDiscrepancyBE.APHTML = oWorkflowHTML.function3("White", DateTime.Now, txtDebitComment.Text.Trim());
            oDiscrepancyBE.APActionRequired = false;
            oDiscrepancyBE.Action = "InsertHTML";
            oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
            oDiscrepancyBE.LoggedDateTime = DateTime.Now;
            oDiscrepancyBE.LevelNumber = 1;
            int? iResult = oDiscrepancyBAL.addWorkFlowHTMLsBAL(oDiscrepancyBE);
        }
    }

    public void DebitSaveForFailPallets()
    {
        DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
        DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();
        WorkflowHTML oWorkflowHTML = new WorkflowHTML();

        DISLog_WorkFlowBE oDISLog_WorkFlowBE = new DISLog_WorkFlowBE();
        DISLog_WorkFlowBAL oDISLog_WorkFlowBAL = new DISLog_WorkFlowBAL();

        oDISLog_WorkFlowBE.Action = "GetDataForSpecificAction";
        oDISLog_WorkFlowBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
        oDISLog_WorkFlowBE.StageCompletedForWhom = "VEN021_StageCompleted";
        DataTable dt = oDISLog_WorkFlowBAL.GetDataForSpecificActionBAL(oDISLog_WorkFlowBE);
        if (chkDebitStageComplete.Checked)
        {

            if (dt != null && dt.Rows.Count > 0)
            {
                //Vendor has taken the action, close the discrepancy
                oDiscrepancyBE.CloseDiscrepancy = true;
            }
            oDiscrepancyBE.GINHTML = oWorkflowHTML.function3("White", null, "", "");
            oDiscrepancyBE.GINActionRequired = false;

            oDiscrepancyBE.INVHTML = oWorkflowHTML.function3("White", null, "", "");
            oDiscrepancyBE.INVActionRequired = false;

            string debitNoteNumber = string.Empty;
            if (ViewState["DebitNoteNo"] != null)
                debitNoteNumber = Convert.ToString(ViewState["DebitNoteNo"]);

            oDiscrepancyBE.APHTML = oWorkflowHTML.function1(DateTime.Now, null, null, "Yellow", "", "Debit note raised",
                //Session["UserName"].ToString(), 
                UIUtility.GetODVendorUserName(Convert.ToString(Session["Role"]), Convert.ToString(Session["UserName"]), Convert.ToString(Session["LoginID"])),
                txtDebitComment.Text.Trim(), "", debitNoteNumber
                , "", "", "", "", "", "", "", "", "", "", "", "", "", "", txtDebitValueActual.Text, pCostCenter:txtCostCenter1.Text);
            oDiscrepancyBE.APActionRequired = false;

            oDiscrepancyBE.VENHTML = oWorkflowHTML.function3("White", null, "", "");
            oDiscrepancyBE.VenActionRequired = false;

            oDiscrepancyBE.Action = "InsertHTML";
            oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
            oDiscrepancyBE.LoggedDateTime = DateTime.Now;
            oDiscrepancyBE.LevelNumber = 1;
            int? iResult1 = oDiscrepancyBAL.addWorkFlowHTMLsBAL(oDiscrepancyBE);
        }
        else
        {
            oDiscrepancyBE.APHTML = oWorkflowHTML.function3("White", DateTime.Now, txtDebitComment.Text.Trim());
            oDiscrepancyBE.APActionRequired = false;
            oDiscrepancyBE.Action = "InsertHTML";
            oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
            oDiscrepancyBE.LoggedDateTime = DateTime.Now;
            oDiscrepancyBE.LevelNumber = 1;
            int? iResult = oDiscrepancyBAL.addWorkFlowHTMLsBAL(oDiscrepancyBE);
        }
    }

    public void DebitSaveForWrongPackSize()
    {
        DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
        DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();
        WorkflowHTML oWorkflowHTML = new WorkflowHTML();

        DISLog_WorkFlowBE oDISLog_WorkFlowBE = new DISLog_WorkFlowBE();
        DISLog_WorkFlowBAL oDISLog_WorkFlowBAL = new DISLog_WorkFlowBAL();

        oDISLog_WorkFlowBE.Action = "GetDataForSpecificAction";
        oDISLog_WorkFlowBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
        oDISLog_WorkFlowBE.StageCompletedForWhom = "VEN019_StageCompleted";
        DataTable dtVEN019 = oDISLog_WorkFlowBAL.GetDataForSpecificActionBAL(oDISLog_WorkFlowBE);
        if (dtVEN019 != null && dtVEN019.Rows.Count > 0)
        {
            oDiscrepancyBE.CloseDiscrepancy = true;
        }


        if (chkDebitStageComplete.Checked)
        {

            oDiscrepancyBE.GINHTML = oWorkflowHTML.function3("White", null, "", "");
            oDiscrepancyBE.GINActionRequired = false;

            oDiscrepancyBE.INVHTML = oWorkflowHTML.function3("White", null, "", "");
            oDiscrepancyBE.INVActionRequired = false;

            string debitNoteNumber = string.Empty;
            if (ViewState["DebitNoteNo"] != null)
                debitNoteNumber = Convert.ToString(ViewState["DebitNoteNo"]);

            oDiscrepancyBE.APHTML = oWorkflowHTML.function1(DateTime.Now, null, null, "Yellow", "", "Debit note raised",
                //Session["UserName"].ToString(), 
                UIUtility.GetODVendorUserName(Convert.ToString(Session["Role"]), Convert.ToString(Session["UserName"]), Convert.ToString(Session["LoginID"])),
                txtDebitComment.Text.Trim(), "", debitNoteNumber
                , "", "", "", "", "", "", "", "", "", "", "", "", "", "", txtDebitValueActual.Text, pCostCenter: txtCostCenter1.Text);
            oDiscrepancyBE.APActionRequired = false;

            oDiscrepancyBE.VENHTML = oWorkflowHTML.function3("White", null, "", "");
            oDiscrepancyBE.VenActionRequired = false;

            oDiscrepancyBE.Action = "InsertHTML";

            oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
            oDiscrepancyBE.LoggedDateTime = DateTime.Now;
            oDiscrepancyBE.LevelNumber = 1;
            int? iResult1 = oDiscrepancyBAL.addWorkFlowHTMLsBAL(oDiscrepancyBE);
        }
        else
        {
            oDiscrepancyBE.APHTML = oWorkflowHTML.function3("White", DateTime.Now, txtDebitComment.Text.Trim());
            oDiscrepancyBE.APActionRequired = false;
            oDiscrepancyBE.Action = "InsertHTML";
            oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
            oDiscrepancyBE.LoggedDateTime = DateTime.Now;
            oDiscrepancyBE.LevelNumber = 1;
            int? iResult = oDiscrepancyBAL.addWorkFlowHTMLsBAL(oDiscrepancyBE);
        }
    }

    private void BindCurrency()
    {
        var currencyBAL = new MAS_CurrencyBAL();
        var currencyBE = new CurrencyBE();
        currencyBE.Action = "ShowAll";
        var lstCurrency = currencyBAL.GetCurrenyBAL(currencyBE);
        if (lstCurrency != null && lstCurrency.Count > 0)
        {
            FillControls.FillDropDown(ref ddlCurrencyDebit, lstCurrency, "CurrencyName", "CurrencyID");

        }
    }

    private void BindDebitReason()
    {
        var debitReasonTypeBAL = new DIS_DebitReasonTypeBAL();
        var apActionBE = new APActionBE();
        apActionBE.Action = "GetReasonTypeSetUp";
        apActionBE.ReasonType = "DR"; /* Debit Reason */
        var lstAPActionBE = debitReasonTypeBAL.GetReasonTypeBAL(apActionBE);
        if (lstAPActionBE != null && lstAPActionBE.Count > 0)
        {
            FillControls.FillDropDown(ref ddlReasonDebit, lstAPActionBE, "Reason", "ReasonTypeID", "Select");
        }
    }

    private void SetPOVendorVATReference(int vendorId)
    {
        var masVatCodeBAL = new MAS_VatCodeBAL();
        var masVatCodeBE = new MAS_VatCodeBE();
        masVatCodeBE.Action = "GetVendorVatCodeByVendorId";
        masVatCodeBE.Vendor = new MAS_VendorBE();
        masVatCodeBE.Vendor.VendorID = vendorId;
        var vendorVatCodeDetails = masVatCodeBAL.GetVendorVatCodeByVendorIdBAL(masVatCodeBE);
        if (vendorVatCodeDetails.Vendor != null)
            txtVendVendorVATReference.Text = vendorVatCodeDetails.Vendor.VatCode;
        else
            txtVendVendorVATReference.Text = string.Empty;
    }

    private void SetVendODVATReference(int siteId)
    {
        var masVatCodeBAL = new MAS_VatCodeBAL();
        var masVatCodeBE = new MAS_VatCodeBE();
        masVatCodeBE.Action = "ShowAllODVatCode";
        masVatCodeBE.SiteId = siteId;

        var lstGetODVatCode = masVatCodeBAL.GetODVatCodeBAL(masVatCodeBE);
        if (lstGetODVatCode != null && lstGetODVatCode.Count > 0)
        {
            lblOfficeDepotVATReferenceValue.Text = lstGetODVatCode[0].VatCode;
            ViewState["ODVatCodeID"] = lstGetODVatCode[0].ODVatCodeID;
            //FillControls.FillDropDown(ref ddlVendOfficeDepotVATReference, lstGetODVatCode, "VatCode", "ODVatCodeID", "Select");
        }
    }

    #endregion


}
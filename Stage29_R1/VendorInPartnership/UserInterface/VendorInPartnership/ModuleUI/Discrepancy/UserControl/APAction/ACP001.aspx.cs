﻿using System;
using System.Collections.Generic;
using System.Data;
using BusinessEntities.ModuleBE.AdminFunctions;
using BusinessEntities.ModuleBE.Discrepancy.LogDiscrepancy;
using BusinessLogicLayer.ModuleBAL.AdminFunctions;
using BusinessLogicLayer.ModuleBAL.Discrepancy.LogDiscrepancy;

public partial class APAction_ACP001 : CommonPage {

    #region Events
    
    protected void Page_Load(object sender, EventArgs e) {
        if (!IsPostBack) {
            CheckDebitOrCreditNote();
            GetValueExpected();
            if (GetQueryStringValue("FromPage") != null && GetQueryStringValue("FromPage").ToString().Trim() == "QualityIssue")
            {
                lblValueActual1.Visible = false;
                lblValueDebited.Visible = true;
                trValueExpectedDebit.Visible = false;
                txtDebitValueExpected.Text = string.Empty;
                txtDebitValueActual.Text = string.Empty;

                lblValueActual.Visible = false;
                lblValueCredited.Visible = true;
                trValueExpectedCredit.Visible = false;
                txtCreditValueExpected.Text = string.Empty;
                txtCreditValueActual.Text = string.Empty;
            }
        }
       
    }
    
    protected void btnCreditSave_Click(object sender, EventArgs e) {
        btnCreditSave.Enabled = false;
        CreditSave();

        //Redirecting to Previous Page
        RedirectToPage(GetQueryStringValue("PreviousPage"));
    }

    protected void btnDebitSave1_Click(object sender, EventArgs e) {
        btnSave_1.Enabled = false;
        DebitSave();

        //Redirecting to Previous Page
        RedirectToPage(GetQueryStringValue("PreviousPage"));
    }
    protected void btnOtherSave_Click(object sender, EventArgs e)
    {
        btnOtherSave.Enabled = false;
        OtherSave();
        RedirectToPage(GetQueryStringValue("PreviousPage"));
    }

    #endregion

    #region Methods
    
    private void CreditSave() {

        DISLog_WorkFlowBE oDISLog_WorkFlowBE = new DISLog_WorkFlowBE();
        DISLog_WorkFlowBAL oDISLog_WorkFlowBAL = new DISLog_WorkFlowBAL();
        if (GetQueryStringValue("disLogID") != null && GetQueryStringValue("disLogID").ToString() != "") {
            oDISLog_WorkFlowBE.Action = "InsertUpdateWorkflowActions";
            oDISLog_WorkFlowBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
            oDISLog_WorkFlowBE.ACP001_NoteType = 'C';
            oDISLog_WorkFlowBE.ACP001_NoteNumber = txtCreditNoteNumber.Text;
            oDISLog_WorkFlowBE.ACP001_ValueExpected = txtCreditValueExpected.Text != string.Empty ? Convert.ToDecimal(txtCreditValueExpected.Text) : (decimal?)null;
            oDISLog_WorkFlowBE.ACP001_ActualValue = txtCreditValueActual.Text != string.Empty ? Convert.ToDecimal(txtCreditValueActual.Text) : (decimal?)null;
            oDISLog_WorkFlowBE.StageCompletedForWhom = "ACP001_StageCompleted";
            if (chkCreditStageComplete.Checked) {
                oDISLog_WorkFlowBE.StageCompleted = true;
            }
            else {
                oDISLog_WorkFlowBE.StageCompleted = false;
            }
            oDISLog_WorkFlowBE.Comment = txtCreditComment.Text;

            //update the work flow action table each time for the comments and other fields( if stage is completed)
            oDISLog_WorkFlowBE.DiscrepancyWorkFlowID = Convert.ToInt32(GetQueryStringValue("DisWFlowID").ToString());

            //insert or update the record according to the stage completed status
            int? iResult = oDISLog_WorkFlowBAL.addWorkFlowActionsBAL(oDISLog_WorkFlowBE);
            if (GetQueryStringValue("FromPage") != null && GetQueryStringValue("FromPage").ToString().Trim() == "overs") {
                OversCreditHTML();
            }
            if (GetQueryStringValue("FromPage") != null && GetQueryStringValue("FromPage").ToString().Trim() == "shortage") {
                ShortageCreditHTML();
            }
            if (GetQueryStringValue("FromPage") != null && GetQueryStringValue("FromPage").ToString().Trim() == "WrongPackSize") {
                IncorrectProductCreditHTML();
            }
            if (GetQueryStringValue("FromPage") != null && GetQueryStringValue("FromPage").ToString().Trim() == "IncorrectProduct") {
                IncorrectProductCreditHTML();
            }
            if (GetQueryStringValue("FromPage") != null && GetQueryStringValue("FromPage").ToString().Trim() == "GoodsReceivedDamaged")
            {
                GoodsReceivedDamagedCreditHTML();
            }
            if (GetQueryStringValue("FromPage") != null && GetQueryStringValue("FromPage").ToString().Trim() == "QualityIssue")
            {
                QualityIssueCreditHTML();
            }
        }
    }

    private void DebitSave() {
        DISLog_WorkFlowBE oDISLog_WorkFlowBE = new DISLog_WorkFlowBE();
        DISLog_WorkFlowBAL oDISLog_WorkFlowBAL = new DISLog_WorkFlowBAL();
        if (GetQueryStringValue("disLogID") != null && GetQueryStringValue("disLogID").ToString() != "") {
            oDISLog_WorkFlowBE.Action = "InsertUpdateWorkflowActions";
            oDISLog_WorkFlowBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
            oDISLog_WorkFlowBE.ACP001_NoteType = 'D';
            oDISLog_WorkFlowBE.ACP001_NoteNumber = txtDebitNoteNumber.Text;
            oDISLog_WorkFlowBE.ACP001_ValueExpected = txtDebitValueExpected.Text != string.Empty ? Convert.ToDecimal(txtDebitValueExpected.Text) : (decimal?)null;
            oDISLog_WorkFlowBE.ACP001_ActualValue = txtDebitValueActual.Text != string.Empty ? Convert.ToDecimal(txtDebitValueActual.Text) : (decimal?)null;
            oDISLog_WorkFlowBE.StageCompletedForWhom = "ACP001_StageCompleted";
            if (chkDebitStageComplete.Checked) {
                oDISLog_WorkFlowBE.StageCompleted = true;
            }
            else {
                oDISLog_WorkFlowBE.StageCompleted = false;
            }
            oDISLog_WorkFlowBE.Comment = txtDebitComment.Text;

            //update the work flow action table each time for the comments and other fields( if stage is completed)
            oDISLog_WorkFlowBE.DiscrepancyWorkFlowID = Convert.ToInt32(GetQueryStringValue("DisWFlowID").ToString());

            //insert or update the record according to the stage completed status
            int? iResult = oDISLog_WorkFlowBAL.addWorkFlowActionsBAL(oDISLog_WorkFlowBE);

            if (GetQueryStringValue("FromPage") != null && GetQueryStringValue("FromPage").ToString().Trim() == "overs") {
                OversDebitHTML();
            }
            if (GetQueryStringValue("FromPage") != null && GetQueryStringValue("FromPage").ToString().Trim() == "shortage") {
                ShortageDebitHTML();
            }
            if (GetQueryStringValue("FromPage") != null && GetQueryStringValue("FromPage").ToString().Trim() == "WrongPackSize") {
                IncorrectProductDebitHTML();
            }
            if (GetQueryStringValue("FromPage") != null && GetQueryStringValue("FromPage").ToString().Trim() == "IncorrectProduct") {
                IncorrectProductDebitHTML();
            }
            if (GetQueryStringValue("FromPage") != null && GetQueryStringValue("FromPage").ToString().Trim() == "GoodsReceivedDamaged")
            {
                GoodsReceivedDamagedDebitHTML();
            }
            if (GetQueryStringValue("FromPage") != null && GetQueryStringValue("FromPage").ToString().Trim() == "QualityIssue")
            {
                QualityIssueDebitHTML();
            }
        }
    }

    private void OtherSave() {
        DISLog_WorkFlowBE oDISLog_WorkFlowBE = new DISLog_WorkFlowBE();
        DISLog_WorkFlowBAL oDISLog_WorkFlowBAL = new DISLog_WorkFlowBAL();
        if (GetQueryStringValue("disLogID") != null && GetQueryStringValue("disLogID").ToString() != "") {
            oDISLog_WorkFlowBE.Action = "InsertUpdateWorkflowActions";
            oDISLog_WorkFlowBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
            oDISLog_WorkFlowBE.ACP001_NoteType = 'O';
            
            oDISLog_WorkFlowBE.StageCompletedForWhom = "ACP001_StageCompleted";
            if (chkOtherStageComplete.Checked) {
                oDISLog_WorkFlowBE.StageCompleted = true;
            }
            else {
                oDISLog_WorkFlowBE.StageCompleted = false;
            }
            oDISLog_WorkFlowBE.Comment = txtOtherComments.Text;

            //update the work flow action table each time for the comments and other fields( if stage is completed)
            oDISLog_WorkFlowBE.DiscrepancyWorkFlowID = Convert.ToInt32(GetQueryStringValue("DisWFlowID").ToString());

            //insert or update the record according to the stage completed status
            int? iResult = oDISLog_WorkFlowBAL.addWorkFlowActionsBAL(oDISLog_WorkFlowBE);

            if (GetQueryStringValue("FromPage") != null && GetQueryStringValue("FromPage").ToString().Trim() == "overs") {
                OversOtherHTML();
            }
            if (GetQueryStringValue("FromPage") != null && GetQueryStringValue("FromPage").ToString().Trim() == "shortage") {
                ShortageOtherHTML();
            }
            if (GetQueryStringValue("FromPage") != null && GetQueryStringValue("FromPage").ToString().Trim() == "WrongPackSize") {
                IncorrectProductOtherHTML();
            }
            if (GetQueryStringValue("FromPage") != null && GetQueryStringValue("FromPage").ToString().Trim() == "IncorrectProduct") {
                IncorrectProductOtherHTML();
            }
            if (GetQueryStringValue("FromPage") != null && GetQueryStringValue("FromPage").ToString().Trim() == "GoodsReceivedDamaged") {
                GoodsReceivedDamagedOtherHTML();
            }
            if (GetQueryStringValue("FromPage") != null && GetQueryStringValue("FromPage").ToString().Trim() == "QualityIssue") {
                QualityIssueOtherHTML();
            }
        }
    }

    private void CheckDebitOrCreditNote() {
        if (GetQueryStringValue("SiteID") != null) {
            /* Check for the site CommunicationWithEscalationType (whether Credit or Debit) */
            MAS_SiteBE oMAS_SiteBE = new MAS_SiteBE();
            MAS_SiteBAL oMAS_SiteBAL = new MAS_SiteBAL();
            oMAS_SiteBE.Action = "GetSiteDisSettings";
            oMAS_SiteBE.SiteID = Convert.ToInt32(GetQueryStringValue("SiteID").ToString());

            oMAS_SiteBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();

            List<MAS_SiteBE> lstSiteDetails = oMAS_SiteBAL.GetSiteMisSettingBAL(oMAS_SiteBE);
            if (lstSiteDetails != null && lstSiteDetails.Count > 0) {
                if (lstSiteDetails[0].DIS_CommunicationWithEscalationType.Trim() != "D") {
                    pnlACP001d.Style.Add("display", "block");
                    pnlACP001c.Style.Add("display", "none");
                    rdoACP001dDebit.Checked = true;
                }
                else if (lstSiteDetails[0].DIS_CommunicationWithEscalationType.Trim() != "C") {
                    pnlACP001d.Style.Add("display", "none");
                    pnlACP001c.Style.Add("display", "block");
                    rdoACP001cCredit.Checked = true;
                }
            }


        }
    }

    private void GetValueExpected() {
        DISLog_WorkFlowBE oDISLog_WorkFlowBE = new DISLog_WorkFlowBE();
        DISLog_WorkFlowBAL oDISLog_WorkFlowBAL = new DISLog_WorkFlowBAL();
        if (GetQueryStringValue("disLogID") != null && GetQueryStringValue("disLogID").ToString() != "") {
            oDISLog_WorkFlowBE.Action = "GetPOValueExpected";
            if (GetQueryStringValue("FromPage") != null && GetQueryStringValue("FromPage").ToString().Trim() == "QualityIssue") {
                oDISLog_WorkFlowBE.Action = "GetPOValueExpectedQualityIssue";
            }
            oDISLog_WorkFlowBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID"));

            List<DISLog_WorkFlowBE> lstActualValue = oDISLog_WorkFlowBAL.GetPOValueExpectedBAL(oDISLog_WorkFlowBE);
            if (lstActualValue.Count > 0 && lstActualValue != null) {
                txtCreditValueExpected.Text = lstActualValue[0].ACP001_ValueExpected.ToString();
                txtCreditValueActual.Text = lstActualValue[0].ACP001_ValueExpected.ToString();
                txtDebitValueExpected.Text = lstActualValue[0].ACP001_ValueExpected.ToString();
                txtDebitValueActual.Text = lstActualValue[0].ACP001_ValueExpected.ToString();

                if (GetQueryStringValue("FromPage") != null && GetQueryStringValue("FromPage").ToString().Trim() == "shortage")
                {
                    txtCreditValueExpected.Text = lstActualValue[0].ACP001_ShortageValueExpected.ToString();
                    txtCreditValueActual.Text = lstActualValue[0].ACP001_ShortageValueExpected.ToString();
                    txtDebitValueExpected.Text = lstActualValue[0].ACP001_ShortageValueExpected.ToString();
                    txtDebitValueActual.Text = lstActualValue[0].ACP001_ShortageValueExpected.ToString();
                }

                if (GetQueryStringValue("FromPage") != null && GetQueryStringValue("FromPage").ToString().Trim() == "WrongPackSize") {
                    txtCreditValueExpected.Text = lstActualValue[0].ACP001_WrongPackValueExpected.ToString();
                    txtCreditValueActual.Text = lstActualValue[0].ACP001_WrongPackValueExpected.ToString();
                    txtDebitValueExpected.Text = lstActualValue[0].ACP001_WrongPackValueExpected.ToString();
                    txtDebitValueActual.Text = lstActualValue[0].ACP001_WrongPackValueExpected.ToString();
                }
                else if (GetQueryStringValue("FromPage") != null && GetQueryStringValue("FromPage").ToString().Trim() == "GoodsReceivedDamaged") {
                    txtCreditValueExpected.Text = lstActualValue[0].ACP001_DamageValueExpected.ToString();
                    txtCreditValueActual.Text = lstActualValue[0].ACP001_DamageValueExpected.ToString();
                    txtDebitValueExpected.Text = lstActualValue[0].ACP001_DamageValueExpected.ToString();
                    txtDebitValueActual.Text = lstActualValue[0].ACP001_DamageValueExpected.ToString();
                }
                else if (GetQueryStringValue("FromPage") != null && GetQueryStringValue("FromPage").ToString().Trim() == "QualityIssue")
                {
                    txtCreditValueExpected.Text = lstActualValue[0].ACP001_QualityValueExpected.ToString();
                    txtCreditValueActual.Text = lstActualValue[0].ACP001_QualityValueExpected.ToString();
                    txtDebitValueExpected.Text = lstActualValue[0].ACP001_QualityValueExpected.ToString();
                    txtDebitValueActual.Text = lstActualValue[0].ACP001_QualityValueExpected.ToString();
                }
                else if (GetQueryStringValue("FromPage") != null && GetQueryStringValue("FromPage").ToString().Trim() == "IncorrectProduct") {
                    txtCreditValueExpected.Text = lstActualValue[0].ACP001_IncorrectProductValueExpected.ToString();
                    txtCreditValueActual.Text = lstActualValue[0].ACP001_IncorrectProductValueExpected.ToString();
                    txtDebitValueExpected.Text = lstActualValue[0].ACP001_IncorrectProductValueExpected.ToString();
                    txtDebitValueActual.Text = lstActualValue[0].ACP001_IncorrectProductValueExpected.ToString();
                } 
            }
        }
    }

    private void OversDebitHTML() {

        DISLog_WorkFlowBE oDISLog_WorkFlowBE = new DISLog_WorkFlowBE();
        DISLog_WorkFlowBAL oDISLog_WorkFlowBAL = new DISLog_WorkFlowBAL();
        //now insert the work flow HTML according to the action taken only if the stage is completed
        DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
        DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();

        if (chkDebitStageComplete.Checked) {
            WorkflowHTML oWorkflowHTML = new WorkflowHTML();

            //Vendor's action is pending then print only Vendor's HTML

            
          //  if(bVenActionRequired == true){

                //Check whether ACP002 has completed its stage or not..If Yes then Close Discrepancy otherwise not.
                oDISLog_WorkFlowBE.Action = "GetDataForSpecificAction";
                oDISLog_WorkFlowBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
                oDISLog_WorkFlowBE.StageCompletedForWhom = "ACP002_StageCompleted";
                DataTable dtACO002 = oDISLog_WorkFlowBAL.GetDataForSpecificActionBAL(oDISLog_WorkFlowBE);
                if (dtACO002 != null && dtACO002.Rows.Count > 0)           {
              
                    oDiscrepancyBE.CloseDiscrepancy = true;
                }                
                oDiscrepancyBE.GINHTML = oWorkflowHTML.function3("white", null, "", "");
                oDiscrepancyBE.INVHTML = oWorkflowHTML.function3("white", null, "", "");
                oDiscrepancyBE.APHTML = oWorkflowHTML.function1(DateTime.Now, null, null, "Yellow", "", "Debit Note Raised",
                                                             //Session["UserName"].ToString(), 
                                                             UIUtility.GetODVendorUserName(Convert.ToString(Session["Role"]), Convert.ToString(Session["UserName"]), Convert.ToString(Session["LoginID"])),
                                                             txtDebitComment.Text,
                                                             "", txtDebitNoteNumber.Text, "", "", "", "","","","","","","","","","","",txtDebitValueActual.Text);

                oDiscrepancyBE.VENHTML = oWorkflowHTML.function3("white", null, "", "");
          //  }
            /*if Vendor has taken the action then print the following HTML*/

           
//            else if (bVenActionRequired == false) {
//                if (sHowGoodsReturned == "V") {
//                    //i.e vendor is comming to coolect goods

//                    if (dt != null && dt.Rows.Count > 0) {

//                        oDiscrepancyBE.GINHTML = oWorkflowHTML.function1(Convert.ToDateTime(dt.Rows[0]["LogDate"]), null,
//                                                                            Convert.ToDateTime(dt.Rows[0]["VEN001_DateGoodsAreToBeCollected"]), "Blue", "", "", Session["UserName"].ToString(),
//                                                                            @"Collection has been organised by vendor.Please ensure 
//                                                                                the goods are collected as per the vendor's instruction", "", "",
//                                                                            dt.Rows[0]["CarrierName"].ToString(),
//                                                                            dt.Rows[0]["VEN001_CollectionAuthorisationNumber"].ToString());
//                        oDiscrepancyBE.GINUserControl = "GIN002";
//                        oDiscrepancyBE.GINActionRequired = true;
//                    }

//                    oDiscrepancyBE.INVHTML = oWorkflowHTML.function3("White", null, "", "");
//                    oDiscrepancyBE.INVActionRequired = false;

//                    oDiscrepancyBE.APHTML = oWorkflowHTML.function1(DateTime.Now, null, null, "yellow", "", "Debit Note Raised",
//                                                                 Session["UserName"].ToString(), txtDebitComment.Text,
//                                                                 "", txtDebitNoteNumber.Text);
//                    oDiscrepancyBE.APActionRequired = false;

//                    oDiscrepancyBE.VENHTML = oWorkflowHTML.function3("White", null, "", "");
//                    oDiscrepancyBE.VenActionRequired = false;

//                }
//                else if (sHowGoodsReturned == "O") {
//                    //i.e OD will return the goods to vendor                        
//                    if (dt != null && dt.Rows.Count > 0) {
//                        oDiscrepancyBE.GINHTML = oWorkflowHTML.function2(DateTime.Now, null, "Blue", Convert.ToString(sSendMailLink), "",
//                                                                            @"Please arrange for goods to be returned to the vendor
//                                                                                and confirm the carriage charge");
//                        oDiscrepancyBE.GINUserControl = "GIN003";
//                        oDiscrepancyBE.GINActionRequired = true;
//                    }
//                    oDiscrepancyBE.INVHTML = oWorkflowHTML.function3("White", null, "", "");
//                    oDiscrepancyBE.INVActionRequired = false;

//                    oDiscrepancyBE.APHTML = oWorkflowHTML.function1(DateTime.Now, null, null, "yellow", "", "Credit Note Raised",
//                                                                 Session["UserName"].ToString(), txtDebitComment.Text,
//                                                                 "", "", "", "", "", txtDebitNoteNumber.Text);
//                    oDiscrepancyBE.APActionRequired = false;

//                    oDiscrepancyBE.VENHTML = oWorkflowHTML.function3("White", null, "", "");
//                    oDiscrepancyBE.VenActionRequired = false;
//                }
//            }
            oDiscrepancyBE.Action = "InsertHTML";
            oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
            oDiscrepancyBE.LoggedDateTime = DateTime.Now;
            oDiscrepancyBE.LevelNumber = 1;
            int? iResult1 = oDiscrepancyBAL.addWorkFlowHTMLsBAL(oDiscrepancyBE);

        }
        else {
            /*Enter comments with date*/
            InsertHTML(txtDebitComment.Text, "Debit Note Raised", txtDebitNoteNumber.Text, "D");
        }
    }

    private void OversCreditHTML() {
        DISLog_WorkFlowBE oDISLog_WorkFlowBE = new DISLog_WorkFlowBE();
        DISLog_WorkFlowBAL oDISLog_WorkFlowBAL = new DISLog_WorkFlowBAL();
        //now insert the work flow HTML according to the action taken only if the stage is completed
        DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
        DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();

      


        if (chkCreditStageComplete.Checked) {
            WorkflowHTML oWorkflowHTML = new WorkflowHTML();

      
                //Check whether ACP002 has completed its stage or not..If Yes then Close Discrepancy otherwise not.
                oDISLog_WorkFlowBE.Action = "GetDataForSpecificAction";
                oDISLog_WorkFlowBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
                oDISLog_WorkFlowBE.StageCompletedForWhom = "ACP002_StageCompleted";
                DataTable dtACO002 = oDISLog_WorkFlowBAL.GetDataForSpecificActionBAL(oDISLog_WorkFlowBE);
                if (dtACO002 != null && dtACO002.Rows.Count > 0)
                {
                    oDiscrepancyBE.CloseDiscrepancy = true;
                }    
                
                oDiscrepancyBE.GINHTML = oWorkflowHTML.function3("white", null, "", "");
                oDiscrepancyBE.GINActionRequired = false;

                oDiscrepancyBE.INVHTML = oWorkflowHTML.function3("white", null, "", "");
                oDiscrepancyBE.INVActionRequired = false;

                oDiscrepancyBE.APHTML = oWorkflowHTML.function1(DateTime.Now, null, null, "yellow", "", "Credit Note Raised",
                                                             //Session["UserName"].ToString(), 
                                                             UIUtility.GetODVendorUserName(Convert.ToString(Session["Role"]), Convert.ToString(Session["UserName"]), Convert.ToString(Session["LoginID"])),
                                                             txtCreditComment.Text,
                                                             "", "", "", "", "", txtCreditNoteNumber.Text,"","","","","","","","","","",txtCreditValueActual.Text);
                oDiscrepancyBE.APActionRequired = false;

                oDiscrepancyBE.VENHTML = oWorkflowHTML.function3("white", null, "", "");
                oDiscrepancyBE.VenActionRequired = false;
           // }
            /*if Vendor has taken the action then print the following HTML*/
            //else if (dtAllDetails.Rows[0]["VenActionRequired"] != null && Convert.ToInt32(dtAllDetails.Rows[0]["VenActionRequired"]) == 0) {
//            else if (bVenActionRequired == false) {
//                if (sHowGoodsReturned == "V") {
//                    //i.e vendor is comming to coolect goods

//                    if (dt != null && dt.Rows.Count > 0) {

//                        oDiscrepancyBE.GINHTML = oWorkflowHTML.function1(Convert.ToDateTime(dt.Rows[0]["LogDate"]), null,
//                                                                            Convert.ToDateTime(dt.Rows[0]["VEN001_DateGoodsAreToBeCollected"]), "Blue", "", "", Session["UserName"].ToString(),
//                                                                            @"Collection has been organised by vendor.Please ensure 
//                                                                                the goods are collected as per the vendor's instruction", "", "",
//                                                                            dt.Rows[0]["CarrierName"].ToString(),
//                                                                            dt.Rows[0]["VEN001_CollectionAuthorisationNumber"].ToString());
//                        oDiscrepancyBE.GINUserControl = "GIN002";
//                        oDiscrepancyBE.GINActionRequired = true;
//                    }

//                    oDiscrepancyBE.INVHTML = oWorkflowHTML.function3("White", null, "", "");
//                    oDiscrepancyBE.INVActionRequired = false;

//                    oDiscrepancyBE.APHTML = oWorkflowHTML.function1(DateTime.Now, null, null, "yellow", "", "Credit Note Raised",
//                                                                 Session["UserName"].ToString(), txtCreditComment.Text,
//                                                                 "", "", "", "", "", txtCreditNoteNumber.Text);
//                    oDiscrepancyBE.APActionRequired = false;

//                    oDiscrepancyBE.VENHTML = oWorkflowHTML.function3("White", null, "", "");
//                    oDiscrepancyBE.VenActionRequired = false;
//                }
//                else if (sHowGoodsReturned == "O") {
//                    //i.e OD will return the goods to vendor
//                    if (dt != null && dt.Rows.Count > 0) {
//                        oDiscrepancyBE.GINHTML = oWorkflowHTML.function2(DateTime.Now, null, "Blue", Convert.ToString(sSendMailLink), "",
//                                                                            @"Please arrange for goods to be returned to the vendor
//                                                                                and confirm the carriage charge");
//                        oDiscrepancyBE.GINUserControl = "GIN003";
//                        oDiscrepancyBE.GINActionRequired = true;
//                    }
//                    oDiscrepancyBE.INVHTML = oWorkflowHTML.function3("White", null, "", "");
//                    oDiscrepancyBE.INVActionRequired = false;

//                    oDiscrepancyBE.APHTML = oWorkflowHTML.function1(DateTime.Now, null, null, "Yellow", "", "Credit Note Raised",
//                                                                 Session["UserName"].ToString(), txtCreditComment.Text,
//                                                                 "", "", "", "", "", txtCreditNoteNumber.Text);
//                    oDiscrepancyBE.APActionRequired = false;

//                    oDiscrepancyBE.VENHTML = oWorkflowHTML.function3("White", null, "", "");
//                    oDiscrepancyBE.VenActionRequired = false;
//                }
//            }
            oDiscrepancyBE.Action = "InsertHTML";
            oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
            oDiscrepancyBE.LoggedDateTime = DateTime.Now;
            oDiscrepancyBE.LevelNumber = 1;
            int? iResult1 = oDiscrepancyBAL.addWorkFlowHTMLsBAL(oDiscrepancyBE);
        }
        else {
            /*Enter comments with date*/
            InsertHTML(txtCreditComment.Text, "Credit Note Raised", txtCreditNoteNumber.Text, "C");
        }
    }

    private void ShortageDebitHTML() {
        DISLog_WorkFlowBE oDISLog_WorkFlowBE = new DISLog_WorkFlowBE();
        DISLog_WorkFlowBAL oDISLog_WorkFlowBAL = new DISLog_WorkFlowBAL();
        //now insert the work flow HTML according to the action taken only if the stage is completed
        DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
        DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();

        oDISLog_WorkFlowBE.Action = "GetDataForSpecificAction";
        oDISLog_WorkFlowBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
        oDISLog_WorkFlowBE.StageCompletedForWhom = "VEN003_StageCompleted";
        DataTable dtVEN003 = oDISLog_WorkFlowBAL.GetDataForSpecificActionBAL(oDISLog_WorkFlowBE);

        oDISLog_WorkFlowBE.StageCompletedForWhom = "VEN004_StageCompleted";
        DataTable dtVEN004 = oDISLog_WorkFlowBAL.GetDataForSpecificActionBAL(oDISLog_WorkFlowBE);

        if (chkDebitStageComplete.Checked) {
            WorkflowHTML oWorkflowHTML = new WorkflowHTML();

            oDiscrepancyBE.GINHTML = oWorkflowHTML.function3("white", null, "", "");
            oDiscrepancyBE.GINActionRequired = false;

            oDiscrepancyBE.INVHTML = oWorkflowHTML.function3("white", null, "", "");
            oDiscrepancyBE.INVActionRequired = false;

            oDiscrepancyBE.APHTML = oWorkflowHTML.function1(DateTime.Now, null, null, "Yellow", "", "Debit Note Raised",
                                                         //Session["UserName"].ToString(), 
                                                         UIUtility.GetODVendorUserName(Convert.ToString(Session["Role"]), Convert.ToString(Session["UserName"]), Convert.ToString(Session["LoginID"])),
                                                         txtDebitComment.Text,
                                                         "", txtDebitNoteNumber.Text, "", "", "", "","","","","","","","","","","",txtDebitValueActual.Text);
            oDiscrepancyBE.APActionRequired = false;

            oDiscrepancyBE.VENHTML = oWorkflowHTML.function3("white", null, "", "");
            oDiscrepancyBE.VenActionRequired = false;


            oDiscrepancyBE.Action = "InsertHTML";
            oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
            oDiscrepancyBE.LoggedDateTime = DateTime.Now;
            oDiscrepancyBE.LevelNumber = 1;

            if ((dtVEN003 != null && dtVEN003.Rows.Count > 0) || dtVEN004 != null && dtVEN004.Rows.Count > 0)
                oDiscrepancyBE.CloseDiscrepancy = true;

            int? iResult1 = oDiscrepancyBAL.addWorkFlowHTMLsBAL(oDiscrepancyBE);

        }
        else {
            /*Enter comments with date*/
            InsertHTML(txtDebitComment.Text, "Debit Note Raised", txtDebitNoteNumber.Text, "D");
        }
    }

    private void ShortageCreditHTML() {

        DISLog_WorkFlowBE oDISLog_WorkFlowBE = new DISLog_WorkFlowBE();
        DISLog_WorkFlowBAL oDISLog_WorkFlowBAL = new DISLog_WorkFlowBAL();
        //now insert the work flow HTML according to the action taken only if the stage is completed
        DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
        DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();

        oDISLog_WorkFlowBE.Action = "GetDataForSpecificAction";
        oDISLog_WorkFlowBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
        oDISLog_WorkFlowBE.StageCompletedForWhom = "VEN003_StageCompleted";
        DataTable dtVEN003 = oDISLog_WorkFlowBAL.GetDataForSpecificActionBAL(oDISLog_WorkFlowBE);

        oDISLog_WorkFlowBE.StageCompletedForWhom = "VEN004_StageCompleted";
        DataTable dtVEN004 = oDISLog_WorkFlowBAL.GetDataForSpecificActionBAL(oDISLog_WorkFlowBE);

        if (chkCreditStageComplete.Checked) {
            WorkflowHTML oWorkflowHTML = new WorkflowHTML();

            oDiscrepancyBE.GINHTML = oWorkflowHTML.function3("white", null, "", "");
            oDiscrepancyBE.GINActionRequired = false;

            oDiscrepancyBE.INVHTML = oWorkflowHTML.function3("white", null, "", "");
            oDiscrepancyBE.INVActionRequired = false;

            oDiscrepancyBE.APHTML = oWorkflowHTML.function1(DateTime.Now, null, null, "Yellow", "", "Credit Note Raised",
                                                         //Session["UserName"].ToString(), 
                                                         UIUtility.GetODVendorUserName(Convert.ToString(Session["Role"]), Convert.ToString(Session["UserName"]), Convert.ToString(Session["LoginID"])),
                                                         txtCreditComment.Text,
                                                         "", "", "", "", "", txtCreditNoteNumber.Text, "","","","","","","","","","",txtCreditValueActual.Text);
            oDiscrepancyBE.APActionRequired = false;

            oDiscrepancyBE.VENHTML = oWorkflowHTML.function3("white", null, "", "");
            oDiscrepancyBE.VenActionRequired = false;

            if ((dtVEN003 != null && dtVEN003.Rows.Count > 0) || dtVEN004 != null && dtVEN004.Rows.Count > 0)
                oDiscrepancyBE.CloseDiscrepancy = true;

            oDiscrepancyBE.Action = "InsertHTML";
            oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
            oDiscrepancyBE.LoggedDateTime = DateTime.Now;
            oDiscrepancyBE.LevelNumber = 1;

            int? iResult1 = oDiscrepancyBAL.addWorkFlowHTMLsBAL(oDiscrepancyBE);

        }
        else {
            /*Enter comments with date*/
            InsertHTML(txtDebitComment.Text, "Credit Note Raised", txtDebitNoteNumber.Text, "D");
        }
    }

    private void QualityIssueDebitHTML()
    {
        DISLog_WorkFlowBE oDISLog_WorkFlowBE = new DISLog_WorkFlowBE();
        DISLog_WorkFlowBAL oDISLog_WorkFlowBAL = new DISLog_WorkFlowBAL();
        //now insert the work flow HTML according to the action taken only if the stage is completed
        DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
        DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();

        WorkflowHTML oWorkflowHTML = new WorkflowHTML();

        if (chkDebitStageComplete.Checked)
        {
            oDISLog_WorkFlowBE.Action = "GetDataForSpecificAction";
            oDISLog_WorkFlowBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
            oDISLog_WorkFlowBE.StageCompletedForWhom = "GIN002_StageCompleted";
            DataTable dtGIN = oDISLog_WorkFlowBAL.GetDataForSpecificActionBAL(oDISLog_WorkFlowBE);
            if (dtGIN != null && dtGIN.Rows.Count > 0)
            {
                oDiscrepancyBE.CloseDiscrepancy = true;
            }

            oDiscrepancyBE.GINHTML = oWorkflowHTML.function3("white", null, "", "");
            oDiscrepancyBE.GINActionRequired = false;

            oDiscrepancyBE.INVHTML = oWorkflowHTML.function3("white", null, "", "");
            oDiscrepancyBE.INVActionRequired = false;

            oDiscrepancyBE.APHTML = oWorkflowHTML.function1(DateTime.Now, null, null, "Yellow", "", "Debit Note Raised",
                                                         //Session["UserName"].ToString(),
                                                         UIUtility.GetODVendorUserName(Convert.ToString(Session["Role"]), Convert.ToString(Session["UserName"]), Convert.ToString(Session["LoginID"])),
                                                         txtDebitComment.Text,
                                                         "", txtDebitNoteNumber.Text,"","","","","","","","","","","","","","",txtDebitValueActual.Text);
            oDiscrepancyBE.APActionRequired = false;

            oDiscrepancyBE.VENHTML = oWorkflowHTML.function3("white", null, "", "");
            oDiscrepancyBE.VenActionRequired = false;

            oDiscrepancyBE.Action = "InsertHTML";
            oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
            oDiscrepancyBE.LoggedDateTime = DateTime.Now;
            oDiscrepancyBE.LevelNumber = 1;
            int? iResult = oDiscrepancyBAL.addWorkFlowHTMLsBAL(oDiscrepancyBE);
        }
        else
        {
            /*Enter comments with date*/
            InsertHTML(txtDebitComment.Text, "Debit Note Raised", txtDebitNoteNumber.Text, "D");
        }
    }

    private void QualityIssueCreditHTML()
    {
        DISLog_WorkFlowBE oDISLog_WorkFlowBE = new DISLog_WorkFlowBE();
        DISLog_WorkFlowBAL oDISLog_WorkFlowBAL = new DISLog_WorkFlowBAL();
        //now insert the work flow HTML according to the action taken only if the stage is completed
        DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
        DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();

        WorkflowHTML oWorkflowHTML = new WorkflowHTML();

        if (chkDebitStageComplete.Checked)
        {
            oDISLog_WorkFlowBE.Action = "GetDataForSpecificAction";
            oDISLog_WorkFlowBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
            oDISLog_WorkFlowBE.StageCompletedForWhom = "GIN002_StageCompleted";
            DataTable dtGIN = oDISLog_WorkFlowBAL.GetDataForSpecificActionBAL(oDISLog_WorkFlowBE);
            if (dtGIN != null && dtGIN.Rows.Count > 0)
            {
                oDiscrepancyBE.CloseDiscrepancy = true;
            }

            oDiscrepancyBE.GINHTML = oWorkflowHTML.function3("white", null, "", "");
            oDiscrepancyBE.GINActionRequired = false;

            oDiscrepancyBE.INVHTML = oWorkflowHTML.function3("white", null, "", "");
            oDiscrepancyBE.INVActionRequired = false;

            oDiscrepancyBE.APHTML = oWorkflowHTML.function1(DateTime.Now, null, null, "Yellow", "", "Credit Note Raised",
                                                         //Session["UserName"].ToString(),
                                                         UIUtility.GetODVendorUserName(Convert.ToString(Session["Role"]), Convert.ToString(Session["UserName"]), Convert.ToString(Session["LoginID"])),
                                                         txtCreditNoteNumber.Text,
                                                         "", txtCreditNoteNumber.Text,"","","","","","","","","","","","","","",txtCreditValueActual.Text);
            oDiscrepancyBE.APActionRequired = false;

            oDiscrepancyBE.VENHTML = oWorkflowHTML.function3("white", null, "", "");
            oDiscrepancyBE.VenActionRequired = false;

            oDiscrepancyBE.Action = "InsertHTML";
            oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
            oDiscrepancyBE.LoggedDateTime = DateTime.Now;
            oDiscrepancyBE.LevelNumber = 1;
            int? iResult = oDiscrepancyBAL.addWorkFlowHTMLsBAL(oDiscrepancyBE);
        }
        else
        {
            /*Enter comments with date*/
            InsertHTML(txtDebitComment.Text, "Credit Note Raised", txtDebitNoteNumber.Text, "D");
        }
    }

    private void IncorrectProductDebitHTML() {
        DISLog_WorkFlowBE oDISLog_WorkFlowBE = new DISLog_WorkFlowBE();
        DISLog_WorkFlowBAL oDISLog_WorkFlowBAL = new DISLog_WorkFlowBAL();
        //now insert the work flow HTML according to the action taken only if the stage is completed
        DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
        DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();

       

        WorkflowHTML oWorkflowHTML = new WorkflowHTML();

        if (chkDebitStageComplete.Checked) 
        {
            

                //Check whether ACP002 has completed its stage or not..If Yes then Close Discrepancy otherwise not.
                oDISLog_WorkFlowBE.Action = "GetDataForSpecificAction";
                oDISLog_WorkFlowBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
                oDISLog_WorkFlowBE.StageCompletedForWhom = "ACP002_StageCompleted";
                DataTable dtACO002 = oDISLog_WorkFlowBAL.GetDataForSpecificActionBAL(oDISLog_WorkFlowBE);
                if (dtACO002 != null && dtACO002.Rows.Count > 0)
                {
                    oDiscrepancyBE.CloseDiscrepancy = true;
                }  

                oDiscrepancyBE.GINHTML = oWorkflowHTML.function3("white", null, "", "");
                oDiscrepancyBE.GINActionRequired = false;

                oDiscrepancyBE.INVHTML = oWorkflowHTML.function3("white", null, "", "");
                oDiscrepancyBE.INVActionRequired = false;

                oDiscrepancyBE.APHTML = oWorkflowHTML.function1(DateTime.Now, null, null, "Yellow", "", "Debit Note Raised",
                                                             //Session["UserName"].ToString(), 
                                                             UIUtility.GetODVendorUserName(Convert.ToString(Session["Role"]), Convert.ToString(Session["UserName"]), Convert.ToString(Session["LoginID"])),
                                                             txtDebitComment.Text,
                                                             "", txtDebitNoteNumber.Text,"","","","","","","","","","","","","","",txtDebitValueActual.Text);
                oDiscrepancyBE.APActionRequired = false;

                oDiscrepancyBE.VENHTML = oWorkflowHTML.function3("white", null, "", "");
                oDiscrepancyBE.VenActionRequired = false;

                oDiscrepancyBE.Action = "InsertHTML";
                oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
                oDiscrepancyBE.LoggedDateTime = DateTime.Now;
                oDiscrepancyBE.LevelNumber = 1;
                int? iResult = oDiscrepancyBAL.addWorkFlowHTMLsBAL(oDiscrepancyBE);
           // }
        }
        else {
            /*Enter comments with date*/
            InsertHTML(txtDebitComment.Text, "Debit Note Raised", txtDebitNoteNumber.Text, "D");
        }
    }

    private void IncorrectProductCreditHTML() {
        DISLog_WorkFlowBE oDISLog_WorkFlowBE = new DISLog_WorkFlowBE();
        DISLog_WorkFlowBAL oDISLog_WorkFlowBAL = new DISLog_WorkFlowBAL();
        //now insert the work flow HTML according to the action taken only if the stage is completed
        DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
        DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();

        

        WorkflowHTML oWorkflowHTML = new WorkflowHTML();

        if (chkCreditStageComplete.Checked)
        {
         

                //Check whether ACP002 has completed its stage or not..If Yes then Close Discrepancy otherwise not.
                oDISLog_WorkFlowBE.Action = "GetDataForSpecificAction";
                oDISLog_WorkFlowBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
                oDISLog_WorkFlowBE.StageCompletedForWhom = "ACP002_StageCompleted";
                DataTable dtACO002 = oDISLog_WorkFlowBAL.GetDataForSpecificActionBAL(oDISLog_WorkFlowBE);
                if (dtACO002 != null && dtACO002.Rows.Count > 0)
                {
                    oDiscrepancyBE.CloseDiscrepancy = true;
                }   

                oDiscrepancyBE.GINHTML = oWorkflowHTML.function3("white", null, "", "");
                oDiscrepancyBE.GINActionRequired = false;

                oDiscrepancyBE.INVHTML = oWorkflowHTML.function3("white", null, "", "");
                oDiscrepancyBE.INVActionRequired = false;

                oDiscrepancyBE.APHTML = oWorkflowHTML.function1(DateTime.Now, null, null, "Yellow", "", "Credit Note Raised",
                                                             //Session["UserName"].ToString(), 
                                                             UIUtility.GetODVendorUserName(Convert.ToString(Session["Role"]), Convert.ToString(Session["UserName"]), Convert.ToString(Session["LoginID"])),
                                                             txtCreditComment.Text,
                                                             "", "", "", "", "", txtCreditNoteNumber.Text, "","","","","","","","","","",txtCreditValueActual.Text);
                oDiscrepancyBE.APActionRequired = false;

                oDiscrepancyBE.VENHTML = oWorkflowHTML.function3("white", null, "", "");
                oDiscrepancyBE.VenActionRequired = false;

                oDiscrepancyBE.Action = "InsertHTML";
                oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
                oDiscrepancyBE.LoggedDateTime = DateTime.Now;
                oDiscrepancyBE.LevelNumber = 1;
                int? iResult = oDiscrepancyBAL.addWorkFlowHTMLsBAL(oDiscrepancyBE);
           // }
        }
        else {
            /*Enter comments with date*/
            InsertHTML(txtCreditComment.Text, "Credit Note Raised", txtCreditNoteNumber.Text, "C");
        }
    }

    private void GoodsReceivedDamagedDebitHTML()
    {
        DISLog_WorkFlowBE oDISLog_WorkFlowBE = new DISLog_WorkFlowBE();
        DISLog_WorkFlowBAL oDISLog_WorkFlowBAL = new DISLog_WorkFlowBAL();
        //now insert the work flow HTML according to the action taken only if the stage is completed
        DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
        DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();

       

        WorkflowHTML oWorkflowHTML = new WorkflowHTML();

        if (chkDebitStageComplete.Checked)
        {
          
                oDISLog_WorkFlowBE.Action = "GetDataForSpecificAction";
                oDISLog_WorkFlowBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
                oDISLog_WorkFlowBE.StageCompletedForWhom = "GIN002_StageCompleted";
                DataTable dtGIN = oDISLog_WorkFlowBAL.GetDataForSpecificActionBAL(oDISLog_WorkFlowBE);
                if (dtGIN != null && dtGIN.Rows.Count > 0)
                {
                    oDiscrepancyBE.CloseDiscrepancy = true;
                }
                oDISLog_WorkFlowBE.Action = "GetDataForSpecificAction";
                oDISLog_WorkFlowBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
                oDISLog_WorkFlowBE.StageCompletedForWhom = "ACP002_StageCompleted";
                DataTable dtAP = oDISLog_WorkFlowBAL.GetDataForSpecificActionBAL(oDISLog_WorkFlowBE);
                if (dtAP != null && dtAP.Rows.Count > 0)
                {
                    oDiscrepancyBE.CloseDiscrepancy = true;
                }

                oDISLog_WorkFlowBE.Action = "GetDataForSpecificAction";
                oDISLog_WorkFlowBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
                oDISLog_WorkFlowBE.StageCompletedForWhom = "GIN004_StageCompleted";
                DataTable dtGIN4 = oDISLog_WorkFlowBAL.GetDataForSpecificActionBAL(oDISLog_WorkFlowBE);
                if (dtGIN4 != null && dtGIN4.Rows.Count > 0)
                {
                    oDiscrepancyBE.CloseDiscrepancy = true;
                }

                oDiscrepancyBE.GINHTML = oWorkflowHTML.function3("white", null, "", "");
                oDiscrepancyBE.GINActionRequired = false;

                oDiscrepancyBE.INVHTML = oWorkflowHTML.function3("white", null, "", "");
                oDiscrepancyBE.INVActionRequired = false;

                oDiscrepancyBE.APHTML = oWorkflowHTML.function1(DateTime.Now, null, null, "Yellow", "", "Debit Note Raised",
                                                             //Session["UserName"].ToString(), 
                                                             UIUtility.GetODVendorUserName(Convert.ToString(Session["Role"]), Convert.ToString(Session["UserName"]), Convert.ToString(Session["LoginID"])),
                                                             txtDebitComment.Text,
                                                             "", txtDebitNoteNumber.Text,"","","","","","","","","","","","","","",txtDebitValueActual.Text);
                oDiscrepancyBE.APActionRequired = false;

                oDiscrepancyBE.VENHTML = oWorkflowHTML.function3("white", null, "", "");
                oDiscrepancyBE.VenActionRequired = false;

                oDiscrepancyBE.Action = "InsertHTML";
                oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
                oDiscrepancyBE.LoggedDateTime = DateTime.Now;
                oDiscrepancyBE.LevelNumber = 1;
                int? iResult = oDiscrepancyBAL.addWorkFlowHTMLsBAL(oDiscrepancyBE);
            //}
        }
        else
        {
            /*Enter comments with date*/
            InsertHTML(txtDebitComment.Text, "Debit Note Raised", txtDebitNoteNumber.Text, "D");
        }
    }

    private void GoodsReceivedDamagedCreditHTML()
    {
        DISLog_WorkFlowBE oDISLog_WorkFlowBE = new DISLog_WorkFlowBE();
        DISLog_WorkFlowBAL oDISLog_WorkFlowBAL = new DISLog_WorkFlowBAL();
        //now insert the work flow HTML according to the action taken only if the stage is completed
        DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
        DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();


        WorkflowHTML oWorkflowHTML = new WorkflowHTML();

        if (chkCreditStageComplete.Checked)
        {
           
          
                // Checking GIN002 stage complete 3d,4a
                oDISLog_WorkFlowBE.Action = "GetDataForSpecificAction";
                oDISLog_WorkFlowBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
                oDISLog_WorkFlowBE.StageCompletedForWhom = "GIN002_StageCompleted";
                DataTable dtGIN = oDISLog_WorkFlowBAL.GetDataForSpecificActionBAL(oDISLog_WorkFlowBE);
                if (dtGIN != null && dtGIN.Rows.Count > 0)
                {
                    oDiscrepancyBE.CloseDiscrepancy = true;
                }

                oDISLog_WorkFlowBE.Action = "GetDataForSpecificAction";
                oDISLog_WorkFlowBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
                oDISLog_WorkFlowBE.StageCompletedForWhom = "ACP002_StageCompleted";
                DataTable dtAP= oDISLog_WorkFlowBAL.GetDataForSpecificActionBAL(oDISLog_WorkFlowBE);
                if (dtAP != null && dtAP.Rows.Count > 0)
                {
                    oDiscrepancyBE.CloseDiscrepancy = true;
                }

                oDISLog_WorkFlowBE.Action = "GetDataForSpecificAction";
                oDISLog_WorkFlowBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
                oDISLog_WorkFlowBE.StageCompletedForWhom = "GIN004_StageCompleted";
                DataTable dtGIN4 = oDISLog_WorkFlowBAL.GetDataForSpecificActionBAL(oDISLog_WorkFlowBE);
                if (dtGIN4 != null && dtGIN4.Rows.Count > 0)
                {
                    oDiscrepancyBE.CloseDiscrepancy = true;
                }

                oDiscrepancyBE.GINHTML = oWorkflowHTML.function3("white", null, "", "");
                oDiscrepancyBE.GINActionRequired = false;

                oDiscrepancyBE.INVHTML = oWorkflowHTML.function3("white", null, "", "");
                oDiscrepancyBE.INVActionRequired = false;

                oDiscrepancyBE.APHTML = oWorkflowHTML.function1(DateTime.Now, null, null, "Yellow", "", "Credit Note Raised",
                                                             //Session["UserName"].ToString(), 
                                                             UIUtility.GetODVendorUserName(Convert.ToString(Session["Role"]), Convert.ToString(Session["UserName"]), Convert.ToString(Session["LoginID"])),
                                                             txtCreditComment.Text,
                                                             "", "", "", "", "", txtCreditNoteNumber.Text, "","","","","","","","","","",txtCreditValueActual.Text);
                oDiscrepancyBE.APActionRequired = false;

                oDiscrepancyBE.VENHTML = oWorkflowHTML.function3("white", null, "", "");
                oDiscrepancyBE.VenActionRequired = false;

                oDiscrepancyBE.Action = "InsertHTML";
                oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
                oDiscrepancyBE.LoggedDateTime = DateTime.Now;
                oDiscrepancyBE.LevelNumber = 1;
                int? iResult = oDiscrepancyBAL.addWorkFlowHTMLsBAL(oDiscrepancyBE);
            //}
        }
        else
        {
            /*Enter comments with date*/
            InsertHTML(txtCreditComment.Text, "Credit Note Raised", txtCreditNoteNumber.Text, "C");
        }

    }

    private void OversOtherHTML() {

        DISLog_WorkFlowBE oDISLog_WorkFlowBE = new DISLog_WorkFlowBE();
        DISLog_WorkFlowBAL oDISLog_WorkFlowBAL = new DISLog_WorkFlowBAL();
        //now insert the work flow HTML according to the action taken only if the stage is completed
        DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
        DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();

        if (chkOtherStageComplete.Checked) {
            WorkflowHTML oWorkflowHTML = new WorkflowHTML();

            //Vendor's action is pending then print only Vendor's HTML


            //  if(bVenActionRequired == true){

            //Check whether ACP002 has completed its stage or not..If Yes then Close Discrepancy otherwise not.
            oDISLog_WorkFlowBE.Action = "GetDataForSpecificAction";
            oDISLog_WorkFlowBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
            oDISLog_WorkFlowBE.StageCompletedForWhom = "ACP002_StageCompleted";
            DataTable dtACO002 = oDISLog_WorkFlowBAL.GetDataForSpecificActionBAL(oDISLog_WorkFlowBE);
            if (dtACO002 != null && dtACO002.Rows.Count > 0) {

                oDiscrepancyBE.CloseDiscrepancy = true;
            }
            oDiscrepancyBE.GINHTML = oWorkflowHTML.function3("white", null, "", "");
            oDiscrepancyBE.INVHTML = oWorkflowHTML.function3("white", null, "", "");
            oDiscrepancyBE.APHTML = oWorkflowHTML.function1(DateTime.Now, null, null, "Yellow", "", "Other", UIUtility.GetODVendorUserName(Convert.ToString(Session["Role"]), Convert.ToString(Session["UserName"]), Convert.ToString(Session["LoginID"])),
                                                         txtOtherComments.Text,
                                                         "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "");

            oDiscrepancyBE.VENHTML = oWorkflowHTML.function3("white", null, "", "");
            //  }
          
            oDiscrepancyBE.Action = "InsertHTML";
            oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
            oDiscrepancyBE.LoggedDateTime = DateTime.Now;
            oDiscrepancyBE.LevelNumber = 1;
            int? iResult1 = oDiscrepancyBAL.addWorkFlowHTMLsBAL(oDiscrepancyBE);

        }
        else {
            /*Enter comments with date*/
            InsertHTML(txtDebitComment.Text, "Other Reason", "", "O");
        }
    }

    private void ShortageOtherHTML() {
        DISLog_WorkFlowBE oDISLog_WorkFlowBE = new DISLog_WorkFlowBE();
        DISLog_WorkFlowBAL oDISLog_WorkFlowBAL = new DISLog_WorkFlowBAL();
        //now insert the work flow HTML according to the action taken only if the stage is completed
        DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
        DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();

        oDISLog_WorkFlowBE.Action = "GetDataForSpecificAction";
        oDISLog_WorkFlowBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
        oDISLog_WorkFlowBE.StageCompletedForWhom = "VEN003_StageCompleted";
        DataTable dtVEN003 = oDISLog_WorkFlowBAL.GetDataForSpecificActionBAL(oDISLog_WorkFlowBE);

        oDISLog_WorkFlowBE.StageCompletedForWhom = "VEN004_StageCompleted";
        DataTable dtVEN004 = oDISLog_WorkFlowBAL.GetDataForSpecificActionBAL(oDISLog_WorkFlowBE);

        if (chkOtherStageComplete.Checked) {
            WorkflowHTML oWorkflowHTML = new WorkflowHTML();

            oDiscrepancyBE.GINHTML = oWorkflowHTML.function3("white", null, "", "");
            oDiscrepancyBE.GINActionRequired = false;

            oDiscrepancyBE.INVHTML = oWorkflowHTML.function3("white", null, "", "");
            oDiscrepancyBE.INVActionRequired = false;

            oDiscrepancyBE.APHTML = oWorkflowHTML.function1(DateTime.Now, null, null, "Yellow", "", "Other", UIUtility.GetODVendorUserName(Convert.ToString(Session["Role"]), Convert.ToString(Session["UserName"]), Convert.ToString(Session["LoginID"])),
                                                       txtOtherComments.Text,
                                                       "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "");
            oDiscrepancyBE.APActionRequired = false;

            oDiscrepancyBE.VENHTML = oWorkflowHTML.function3("white", null, "", "");
            oDiscrepancyBE.VenActionRequired = false;


            oDiscrepancyBE.Action = "InsertHTML";
            oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
            oDiscrepancyBE.LoggedDateTime = DateTime.Now;
            oDiscrepancyBE.LevelNumber = 1;

            if ((dtVEN003 != null && dtVEN003.Rows.Count > 0) || dtVEN004 != null && dtVEN004.Rows.Count > 0)
                oDiscrepancyBE.CloseDiscrepancy = true;

            int? iResult1 = oDiscrepancyBAL.addWorkFlowHTMLsBAL(oDiscrepancyBE);

        }
        else {
            /*Enter comments with date*/
            InsertHTML(txtDebitComment.Text, "Other Reason", "", "O");
        }
    }

    private void QualityIssueOtherHTML() {
        DISLog_WorkFlowBE oDISLog_WorkFlowBE = new DISLog_WorkFlowBE();
        DISLog_WorkFlowBAL oDISLog_WorkFlowBAL = new DISLog_WorkFlowBAL();
        //now insert the work flow HTML according to the action taken only if the stage is completed
        DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
        DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();

        WorkflowHTML oWorkflowHTML = new WorkflowHTML();

        if (chkOtherStageComplete.Checked) {
            oDISLog_WorkFlowBE.Action = "GetDataForSpecificAction";
            oDISLog_WorkFlowBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
            oDISLog_WorkFlowBE.StageCompletedForWhom = "GIN002_StageCompleted";
            DataTable dtGIN = oDISLog_WorkFlowBAL.GetDataForSpecificActionBAL(oDISLog_WorkFlowBE);
            if (dtGIN != null && dtGIN.Rows.Count > 0) {
                oDiscrepancyBE.CloseDiscrepancy = true;
            }

            oDiscrepancyBE.GINHTML = oWorkflowHTML.function3("white", null, "", "");
            oDiscrepancyBE.GINActionRequired = false;

            oDiscrepancyBE.INVHTML = oWorkflowHTML.function3("white", null, "", "");
            oDiscrepancyBE.INVActionRequired = false;

            oDiscrepancyBE.APHTML = oWorkflowHTML.function1(DateTime.Now, null, null, "Yellow", "", "Other", UIUtility.GetODVendorUserName(Convert.ToString(Session["Role"]), Convert.ToString(Session["UserName"]), Convert.ToString(Session["LoginID"])),
                                                        txtOtherComments.Text,
                                                        "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "");
            oDiscrepancyBE.APActionRequired = false;

            oDiscrepancyBE.VENHTML = oWorkflowHTML.function3("white", null, "", "");
            oDiscrepancyBE.VenActionRequired = false;

            oDiscrepancyBE.Action = "InsertHTML";
            oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
            oDiscrepancyBE.LoggedDateTime = DateTime.Now;
            oDiscrepancyBE.LevelNumber = 1;
            int? iResult = oDiscrepancyBAL.addWorkFlowHTMLsBAL(oDiscrepancyBE);
        }
        else {
            /*Enter comments with date*/
            InsertHTML(txtDebitComment.Text, "Other Reason", "", "O");
        }
    }

    private void IncorrectProductOtherHTML() {
        DISLog_WorkFlowBE oDISLog_WorkFlowBE = new DISLog_WorkFlowBE();
        DISLog_WorkFlowBAL oDISLog_WorkFlowBAL = new DISLog_WorkFlowBAL();
        //now insert the work flow HTML according to the action taken only if the stage is completed
        DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
        DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();



        WorkflowHTML oWorkflowHTML = new WorkflowHTML();

        if (chkOtherStageComplete.Checked) {


            //Check whether ACP002 has completed its stage or not..If Yes then Close Discrepancy otherwise not.
            oDISLog_WorkFlowBE.Action = "GetDataForSpecificAction";
            oDISLog_WorkFlowBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
            oDISLog_WorkFlowBE.StageCompletedForWhom = "ACP002_StageCompleted";
            DataTable dtACO002 = oDISLog_WorkFlowBAL.GetDataForSpecificActionBAL(oDISLog_WorkFlowBE);
            if (dtACO002 != null && dtACO002.Rows.Count > 0) {
                oDiscrepancyBE.CloseDiscrepancy = true;
            }

            oDiscrepancyBE.GINHTML = oWorkflowHTML.function3("white", null, "", "");
            oDiscrepancyBE.GINActionRequired = false;

            oDiscrepancyBE.INVHTML = oWorkflowHTML.function3("white", null, "", "");
            oDiscrepancyBE.INVActionRequired = false;

            oDiscrepancyBE.APHTML = oWorkflowHTML.function1(DateTime.Now, null, null, "Yellow", "", "Other", UIUtility.GetODVendorUserName(Convert.ToString(Session["Role"]), Convert.ToString(Session["UserName"]), Convert.ToString(Session["LoginID"])),
                                                         txtOtherComments.Text,
                                                         "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "");


            oDiscrepancyBE.APActionRequired = false;

            oDiscrepancyBE.VENHTML = oWorkflowHTML.function3("white", null, "", "");
            oDiscrepancyBE.VenActionRequired = false;

            oDiscrepancyBE.Action = "InsertHTML";
            oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
            oDiscrepancyBE.LoggedDateTime = DateTime.Now;
            oDiscrepancyBE.LevelNumber = 1;
            int? iResult = oDiscrepancyBAL.addWorkFlowHTMLsBAL(oDiscrepancyBE);
            // }
        }
        else {
            /*Enter comments with date*/

            InsertHTML(txtDebitComment.Text, "Other Reason", "", "O");
        }
    }

    private void GoodsReceivedDamagedOtherHTML() {
        DISLog_WorkFlowBE oDISLog_WorkFlowBE = new DISLog_WorkFlowBE();
        DISLog_WorkFlowBAL oDISLog_WorkFlowBAL = new DISLog_WorkFlowBAL();
        //now insert the work flow HTML according to the action taken only if the stage is completed
        DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
        DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();



        WorkflowHTML oWorkflowHTML = new WorkflowHTML();

        if (chkOtherStageComplete.Checked) {

            oDISLog_WorkFlowBE.Action = "GetDataForSpecificAction";
            oDISLog_WorkFlowBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
            oDISLog_WorkFlowBE.StageCompletedForWhom = "GIN002_StageCompleted";
            DataTable dtGIN = oDISLog_WorkFlowBAL.GetDataForSpecificActionBAL(oDISLog_WorkFlowBE);
            if (dtGIN != null && dtGIN.Rows.Count > 0) {
                oDiscrepancyBE.CloseDiscrepancy = true;
            }
            oDISLog_WorkFlowBE.Action = "GetDataForSpecificAction";
            oDISLog_WorkFlowBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
            oDISLog_WorkFlowBE.StageCompletedForWhom = "ACP002_StageCompleted";
            DataTable dtAP = oDISLog_WorkFlowBAL.GetDataForSpecificActionBAL(oDISLog_WorkFlowBE);
            if (dtAP != null && dtAP.Rows.Count > 0) {
                oDiscrepancyBE.CloseDiscrepancy = true;
            }

            oDISLog_WorkFlowBE.Action = "GetDataForSpecificAction";
            oDISLog_WorkFlowBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
            oDISLog_WorkFlowBE.StageCompletedForWhom = "GIN004_StageCompleted";
            DataTable dtGIN4 = oDISLog_WorkFlowBAL.GetDataForSpecificActionBAL(oDISLog_WorkFlowBE);
            if (dtGIN4 != null && dtGIN4.Rows.Count > 0) {
                oDiscrepancyBE.CloseDiscrepancy = true;
            }

            oDiscrepancyBE.GINHTML = oWorkflowHTML.function3("white", null, "", "");
            oDiscrepancyBE.GINActionRequired = false;

            oDiscrepancyBE.INVHTML = oWorkflowHTML.function3("white", null, "", "");
            oDiscrepancyBE.INVActionRequired = false;

            oDiscrepancyBE.APHTML = oWorkflowHTML.function1(DateTime.Now, null, null, "Yellow", "", "Other", UIUtility.GetODVendorUserName(Convert.ToString(Session["Role"]), Convert.ToString(Session["UserName"]), Convert.ToString(Session["LoginID"])),
                                                      txtOtherComments.Text,
                                                      "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "");
            oDiscrepancyBE.APActionRequired = false;

            oDiscrepancyBE.VENHTML = oWorkflowHTML.function3("white", null, "", "");
            oDiscrepancyBE.VenActionRequired = false;

            oDiscrepancyBE.Action = "InsertHTML";
            oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
            oDiscrepancyBE.LoggedDateTime = DateTime.Now;
            oDiscrepancyBE.LevelNumber = 1;
            int? iResult = oDiscrepancyBAL.addWorkFlowHTMLsBAL(oDiscrepancyBE);
            //}
        }
        else {
            /*Enter comments with date*/
            InsertHTML(txtDebitComment.Text, "Other Reason", "", "O");
        }
    }
   
    
    private void InsertHTML(string ptxtComment, string pNoteTypeValue, string pNoteNumber, string pNoteType) {

        DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
        DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();

        WorkflowHTML oWorkflowHTML = new WorkflowHTML();

        oDiscrepancyBE.GINHTML = oWorkflowHTML.function3("white", null, "", "");
        oDiscrepancyBE.GINActionRequired = false;

        oDiscrepancyBE.INVHTML = oWorkflowHTML.function3("white", null, "", "");
        oDiscrepancyBE.INVActionRequired = false;

        if (pNoteType == "D") {
            oDiscrepancyBE.APHTML = oWorkflowHTML.function1(DateTime.Now, null, null, "White", "", pNoteTypeValue,
                                                                  Session["UserName"].ToString(), ptxtComment,
                                                                  "", pNoteNumber, "", "", "", "","","","","","","","","","","",txtDebitValueActual.Text);

        }
        else if (pNoteType == "C") {
            oDiscrepancyBE.APHTML = oWorkflowHTML.function1(DateTime.Now, null, null, "White", "", pNoteTypeValue,
                                                                  Session["UserName"].ToString(), ptxtComment,
                                                                  "", "", "", "", "", pNoteNumber,"","","","","","","","","","",txtCreditValueActual.Text);
        }
        oDiscrepancyBE.APActionRequired = false;

        oDiscrepancyBE.VENHTML = oWorkflowHTML.function3("white", null, "", "");
        oDiscrepancyBE.VenActionRequired = false;

        oDiscrepancyBE.Action = "InsertHTML";
        oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
        oDiscrepancyBE.LoggedDateTime = DateTime.Now;
        oDiscrepancyBE.LevelNumber = 1;
        int? iResult = oDiscrepancyBAL.addWorkFlowHTMLsBAL(oDiscrepancyBE);
    }

    #endregion
    protected void btnBack_Click(object sender, EventArgs e)
    {
        //Redirecting to Previous Page
        RedirectToPage(GetQueryStringValue("PreviousPage"));
    }
}
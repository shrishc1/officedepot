﻿using System;
using System.Collections.Generic;
using System.Data;
using BusinessEntities.ModuleBE.AdminFunctions;
using BusinessEntities.ModuleBE.Discrepancy.LogDiscrepancy;
using BusinessLogicLayer.ModuleBAL.AdminFunctions;
using BusinessLogicLayer.ModuleBAL.Discrepancy.LogDiscrepancy;

public partial class APAction_ACP005 : CommonPage
{
    int Siteid = 0;
    int Vendorid = 0;
    string EmailList = null;
    System.Text.StringBuilder sSendMailLink = new System.Text.StringBuilder();

    #region Events

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            CheckDebitOrCreditNote();
            GetValueExpected();


        }

    }

    protected void btnCreditSave_Click(object sender, EventArgs e)
    {
        btnCreditSave.Enabled = false;
        CreditSave();

        //Redirecting to Previous Page
        RedirectToPage(GetQueryStringValue("PreviousPage"));
    }

    protected void btnDebitSave1_Click(object sender, EventArgs e)
    {
        btnSave_1.Enabled = false;
        DebitSave();

        //Redirecting to Previous Page
        RedirectToPage(GetQueryStringValue("PreviousPage"));
    }

    protected void btnResendSave_Click(object sender, EventArgs e)
    {
        btnSave_2.Enabled = false;
        ResendSave();

        //Redirecting to Previous Page
        RedirectToPage(GetQueryStringValue("PreviousPage"));
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        //Redirecting to Previous Page
        RedirectToPage(GetQueryStringValue("PreviousPage"));
    }


    #endregion

    #region Methods

    private void CheckDebitOrCreditNote()
    {
        if (GetQueryStringValue("SiteID") != null)
        {
            /* Check for the site CommunicationWithEscalationType (whether Credit or Debit) */
            MAS_SiteBE oMAS_SiteBE = new MAS_SiteBE();
            MAS_SiteBAL oMAS_SiteBAL = new MAS_SiteBAL();
            oMAS_SiteBE.Action = "GetSiteDisSettings";
            oMAS_SiteBE.SiteID = Convert.ToInt32(GetQueryStringValue("SiteID").ToString());

            oMAS_SiteBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();

            List<MAS_SiteBE> lstSiteDetails = oMAS_SiteBAL.GetSiteMisSettingBAL(oMAS_SiteBE);
            if (lstSiteDetails != null && lstSiteDetails.Count > 0)
            {
                if (lstSiteDetails[0].DIS_CommunicationWithEscalationType.Trim() != "D")
                {
                    pnlACP005d.Style.Add("display", "block");
                    pnlACP005c.Style.Add("display", "none");
                    pnlACP005r.Style.Add("display", "none");
                    rdoACP005dDebit.Checked = true;
                }
                else if (lstSiteDetails[0].DIS_CommunicationWithEscalationType.Trim() != "C")
                {
                    pnlACP005d.Style.Add("display", "none");
                    pnlACP005c.Style.Add("display", "block");
                    pnlACP005r.Style.Add("display", "none");
                    rdoACP005cCredit.Checked = true;
                }
                else
                {
                    pnlACP005d.Style.Add("display", "none");
                    pnlACP005c.Style.Add("display", "none");
                    pnlACP005r.Style.Add("display", "block");
                    rdoR_RequestNewPOD.Checked = true;
                }
            }


        }
    }

    private void GetValueExpected()
    {
        DISLog_WorkFlowBE oDISLog_WorkFlowBE = new DISLog_WorkFlowBE();
        DISLog_WorkFlowBAL oDISLog_WorkFlowBAL = new DISLog_WorkFlowBAL();
        if (GetQueryStringValue("disLogID") != null && GetQueryStringValue("disLogID").ToString() != "")
        {
            oDISLog_WorkFlowBE.Action = "GetPOValueExpected";

            oDISLog_WorkFlowBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID"));

            List<DISLog_WorkFlowBE> lstActualValue = oDISLog_WorkFlowBAL.GetPOValueExpectedBAL(oDISLog_WorkFlowBE);
            if (lstActualValue.Count > 0 && lstActualValue != null)
            {
                txtCreditValueExpected.Text = lstActualValue[0].ACP001_ChaseValueExpected.ToString();
                txtCreditValueActual.Text = lstActualValue[0].ACP001_ChaseValueExpected.ToString();
                txtDebitValueExpected.Text = lstActualValue[0].ACP001_ChaseValueExpected.ToString();
                txtDebitValueActual.Text = lstActualValue[0].ACP001_ChaseValueExpected.ToString();
            }
        }
    }

    private void CreditSave()
    {

        DISLog_WorkFlowBE oDISLog_WorkFlowBE = new DISLog_WorkFlowBE();
        DISLog_WorkFlowBAL oDISLog_WorkFlowBAL = new DISLog_WorkFlowBAL();
        if (GetQueryStringValue("disLogID") != null && GetQueryStringValue("disLogID").ToString() != "")
        {
            oDISLog_WorkFlowBE.Action = "InsertUpdateWorkflowActions";
            oDISLog_WorkFlowBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
            oDISLog_WorkFlowBE.ACP001_NoteType = 'C';
            oDISLog_WorkFlowBE.ACP001_NoteNumber = txtCreditNoteNumber.Text;
            oDISLog_WorkFlowBE.ACP001_ValueExpected = txtCreditValueExpected.Text != string.Empty ? Convert.ToDecimal(txtCreditValueExpected.Text) : (decimal?)null;
            oDISLog_WorkFlowBE.ACP001_ActualValue = txtCreditValueActual.Text != string.Empty ? Convert.ToDecimal(txtCreditValueActual.Text) : (decimal?)null;
            oDISLog_WorkFlowBE.StageCompletedForWhom = "ACP005_StageCompleted";
            if (chkCreditStageComplete.Checked)
            {
                oDISLog_WorkFlowBE.StageCompleted = true;
            }
            else
            {
                oDISLog_WorkFlowBE.StageCompleted = false;
            }
            oDISLog_WorkFlowBE.Comment = txtCreditComment.Text;

            //update the work flow action table each time for the comments and other fields( if stage is completed)
            oDISLog_WorkFlowBE.DiscrepancyWorkFlowID = Convert.ToInt32(GetQueryStringValue("DisWFlowID").ToString());

            //insert or update the record according to the stage completed status
            int? iResult = oDISLog_WorkFlowBAL.addWorkFlowActionsBAL(oDISLog_WorkFlowBE);
            
                InquiryCreditHTML();
            

        }
    }

    private void DebitSave()
    {
        DISLog_WorkFlowBE oDISLog_WorkFlowBE = new DISLog_WorkFlowBE();
        DISLog_WorkFlowBAL oDISLog_WorkFlowBAL = new DISLog_WorkFlowBAL();
        if (GetQueryStringValue("disLogID") != null && GetQueryStringValue("disLogID").ToString() != "")
        {
            oDISLog_WorkFlowBE.Action = "InsertUpdateWorkflowActions";
            oDISLog_WorkFlowBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
            oDISLog_WorkFlowBE.ACP001_NoteType = 'D';
            oDISLog_WorkFlowBE.ACP001_NoteNumber = txtDebitNoteNumber.Text;
            oDISLog_WorkFlowBE.ACP001_ValueExpected = txtDebitValueExpected.Text != string.Empty ? Convert.ToDecimal(txtDebitValueExpected.Text) : (decimal?)null;
            oDISLog_WorkFlowBE.ACP001_ActualValue = txtDebitValueActual.Text != string.Empty ? Convert.ToDecimal(txtDebitValueActual.Text) : (decimal?)null;
            oDISLog_WorkFlowBE.StageCompletedForWhom = "ACP005_StageCompleted";
            if (chkDebitStageComplete.Checked)
            {
                oDISLog_WorkFlowBE.StageCompleted = true;
            }
            else
            {
                oDISLog_WorkFlowBE.StageCompleted = false;
            }
            oDISLog_WorkFlowBE.Comment = txtDebitComment.Text;

            //update the work flow action table each time for the comments and other fields( if stage is completed)
            oDISLog_WorkFlowBE.DiscrepancyWorkFlowID = Convert.ToInt32(GetQueryStringValue("DisWFlowID").ToString());

            //insert or update the record according to the stage completed status
            int? iResult = oDISLog_WorkFlowBAL.addWorkFlowActionsBAL(oDISLog_WorkFlowBE);

            
                InquiryDebitHTML();
            

        }
    }

    private void ResendSave()
    {
        DISLog_WorkFlowBE oDISLog_WorkFlowBE = new DISLog_WorkFlowBE();
        DISLog_WorkFlowBAL oDISLog_WorkFlowBAL = new DISLog_WorkFlowBAL();
        if (GetQueryStringValue("disLogID") != null && GetQueryStringValue("disLogID").ToString() != "")
        {
            oDISLog_WorkFlowBE.Action = "InsertUpdateWorkflowActions";
            oDISLog_WorkFlowBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
            oDISLog_WorkFlowBE.ACP001_NoteType = 'R';

            oDISLog_WorkFlowBE.StageCompletedForWhom = "ACP005_StageCompleted";
            if (chkDebitStageComplete.Checked)
            {
                oDISLog_WorkFlowBE.StageCompleted = true;
            }
            else
            {
                oDISLog_WorkFlowBE.StageCompleted = false;
            }
            oDISLog_WorkFlowBE.Comment = txtResendComment.Text;

            //update the work flow action table each time for the comments and other fields( if stage is completed)
            oDISLog_WorkFlowBE.DiscrepancyWorkFlowID = Convert.ToInt32(GetQueryStringValue("DisWFlowID").ToString());

            //insert or update the record according to the stage completed status
            int? iResult = oDISLog_WorkFlowBAL.addWorkFlowActionsBAL(oDISLog_WorkFlowBE);

            
                InquiryResendHTML();
           

        }
    }

    private void InquiryDebitHTML()
    {

        DISLog_WorkFlowBE oDISLog_WorkFlowBE = new DISLog_WorkFlowBE();
        DISLog_WorkFlowBAL oDISLog_WorkFlowBAL = new DISLog_WorkFlowBAL();
        //now insert the work flow HTML according to the action taken only if the stage is completed
        DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
        DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();

        if (chkDebitStageComplete.Checked)
        {
            WorkflowHTML oWorkflowHTML = new WorkflowHTML();


            oDiscrepancyBE.GINHTML = oWorkflowHTML.function3("white", null, "", "");
            oDiscrepancyBE.INVHTML = oWorkflowHTML.function3("white", null, "", "");
            oDiscrepancyBE.APHTML = oWorkflowHTML.function1(DateTime.Now, null, null, "Yellow", "", "Debit Note Raised",
                                                            UIUtility.GetODVendorUserName(Convert.ToString(Session["Role"]), Convert.ToString(Session["UserName"]), Convert.ToString(Session["LoginID"])),
                                                            txtDebitComment.Text,
                                                             "", txtDebitNoteNumber.Text, "", "", "", "", "", "", "", "", "", "", "", "", "", "", txtDebitValueActual.Text);

            sendAndSaveDiscrepancyCommunication(Convert.ToInt32(GetQueryStringValue("disLogID").ToString()), "lettertype2");

            oDiscrepancyBE.VENHTML = oWorkflowHTML.function2(DateTime.Now, null, "Blue", sSendMailLink.ToString(),
                                                                    @"Communication sent to vendor", "", "", "", "", "", "", "");
            oDiscrepancyBE.VenActionRequired = false;

            oDiscrepancyBE.Action = "InsertHTML";
            oDiscrepancyBE.CloseDiscrepancy = true;
            oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
            oDiscrepancyBE.LoggedDateTime = DateTime.Now;
            oDiscrepancyBE.LevelNumber = 1;
            int? iResult1 = oDiscrepancyBAL.addWorkFlowHTMLsBAL(oDiscrepancyBE);

        }
        else
        {
            /*Enter comments with date*/
            InsertHTML(txtDebitComment.Text, "Debit Note Raised", txtDebitNoteNumber.Text, "D");
        }
    }

    private void InquiryCreditHTML()
    {
        DISLog_WorkFlowBE oDISLog_WorkFlowBE = new DISLog_WorkFlowBE();
        DISLog_WorkFlowBAL oDISLog_WorkFlowBAL = new DISLog_WorkFlowBAL();
        //now insert the work flow HTML according to the action taken only if the stage is completed
        DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
        DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();


        if (chkCreditStageComplete.Checked)
        {
            WorkflowHTML oWorkflowHTML = new WorkflowHTML();


            oDiscrepancyBE.GINHTML = oWorkflowHTML.function3("white", null, "", "");
            oDiscrepancyBE.INVHTML = oWorkflowHTML.function3("white", null, "", "");
            oDiscrepancyBE.APHTML = oWorkflowHTML.function1(DateTime.Now, null, null, "Yellow", "", "Credit Note Raised",
                                                            UIUtility.GetODVendorUserName(Convert.ToString(Session["Role"]), Convert.ToString(Session["UserName"]), Convert.ToString(Session["LoginID"])),
                                                            txtCreditComment.Text,
                                                             "", txtCreditNoteNumber.Text, "", "", "", "", "", "", "", "", "", "", "", "", "", "", txtCreditValueActual.Text);

            sendAndSaveDiscrepancyCommunication(Convert.ToInt32(GetQueryStringValue("disLogID").ToString()), "lettertype2");

            oDiscrepancyBE.VENHTML = oWorkflowHTML.function2(DateTime.Now, null, "Blue", sSendMailLink.ToString(),
                                                                    @"Communication sent to vendor", "", "", "", "", "", "", "");
            oDiscrepancyBE.VenActionRequired = false;

            oDiscrepancyBE.Action = "InsertHTML";
            oDiscrepancyBE.CloseDiscrepancy = true;
            oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
            oDiscrepancyBE.LoggedDateTime = DateTime.Now;
            oDiscrepancyBE.LevelNumber = 1;
            int? iResult1 = oDiscrepancyBAL.addWorkFlowHTMLsBAL(oDiscrepancyBE);
        }
        else
        {
            /*Enter comments with date*/
            InsertHTML(txtCreditComment.Text, "Credit Note Raised", txtCreditNoteNumber.Text, "C");
        }
    }

    private void InquiryResendHTML()
    {
        DISLog_WorkFlowBE oDISLog_WorkFlowBE = new DISLog_WorkFlowBE();
        DISLog_WorkFlowBAL oDISLog_WorkFlowBAL = new DISLog_WorkFlowBAL();
        //now insert the work flow HTML according to the action taken only if the stage is completed
        DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
        DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();


        if (chkCreditStageComplete.Checked)
        {
            WorkflowHTML oWorkflowHTML = new WorkflowHTML();


            oDiscrepancyBE.GINHTML = oWorkflowHTML.function3("white", null, "", "");
            oDiscrepancyBE.INVHTML = oWorkflowHTML.function3("white", null, "", "");
            oDiscrepancyBE.APHTML = oWorkflowHTML.function1(DateTime.Now, null, null, "Yellow", "", "New POD Requested",
                                                            UIUtility.GetODVendorUserName(Convert.ToString(Session["Role"]), Convert.ToString(Session["UserName"]), Convert.ToString(Session["LoginID"])),
                                                            txtResendComment.Text,
                                                             "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "");

            sendAndSaveDiscrepancyCommunication(Convert.ToInt32(GetQueryStringValue("disLogID").ToString()), "lettertype3");

            oDiscrepancyBE.VENHTML = oWorkflowHTML.function2(DateTime.Now, null, "Blue", sSendMailLink.ToString(),
                                                                    @"Communication sent to vendor", "", "", "", "", "", "", "");

            oDiscrepancyBE.VenActionRequired = false;

            oDiscrepancyBE.Action = "InsertHTML";
            oDiscrepancyBE.CloseDiscrepancy = true;
            oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
            oDiscrepancyBE.LoggedDateTime = DateTime.Now;
            oDiscrepancyBE.LevelNumber = 1;
            int? iResult1 = oDiscrepancyBAL.addWorkFlowHTMLsBAL(oDiscrepancyBE);
        }
        else
        {
            /*Enter comments with date*/
            InsertHTML(txtCreditComment.Text, "Credit Note Raised", txtCreditNoteNumber.Text, "C");
        }
    }

    private void InsertHTML(string ptxtComment, string pNoteTypeValue, string pNoteNumber, string pNoteType)
    {

        DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
        DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();

        WorkflowHTML oWorkflowHTML = new WorkflowHTML();

        oDiscrepancyBE.GINHTML = oWorkflowHTML.function3("white", null, "", "");
        oDiscrepancyBE.GINActionRequired = false;

        oDiscrepancyBE.INVHTML = oWorkflowHTML.function3("white", null, "", "");
        oDiscrepancyBE.INVActionRequired = false;

        if (pNoteType == "D")
        {
            oDiscrepancyBE.APHTML = oWorkflowHTML.function1(DateTime.Now, null, null, "White", "", pNoteTypeValue,
                                                                  Session["UserName"].ToString(), ptxtComment,
                                                                  "", pNoteNumber, "", "", "", "", "", "", "", "", "", "", "", "", "", "", txtDebitValueActual.Text);

        }
        else if (pNoteType == "C")
        {
            oDiscrepancyBE.APHTML = oWorkflowHTML.function1(DateTime.Now, null, null, "White", "", pNoteTypeValue,
                                                                  Session["UserName"].ToString(), ptxtComment,
                                                                  "", "", "", "", "", pNoteNumber, "", "", "", "", "", "", "", "", "", "", txtCreditValueActual.Text);
        }
        oDiscrepancyBE.APActionRequired = false;

        oDiscrepancyBE.VENHTML = oWorkflowHTML.function3("white", null, "", "");
        oDiscrepancyBE.VenActionRequired = false;

        oDiscrepancyBE.Action = "InsertHTML";
        oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
        oDiscrepancyBE.LoggedDateTime = DateTime.Now;
        oDiscrepancyBE.LevelNumber = 1;
        int? iResult = oDiscrepancyBAL.addWorkFlowHTMLsBAL(oDiscrepancyBE);
    }

    private string GetDiscrepancyEmailListOfVendor()
    {
        if (GetQueryStringValue("disLogID") != null)
        {
            DiscrepancyBE oNewDiscrepancyBE = new DiscrepancyBE();
            DiscrepancyBAL oNewDiscrepancyBAL = new DiscrepancyBAL();
            oNewDiscrepancyBE.Action = "GetDiscrepancyLog";
            oNewDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
            List<DiscrepancyBE> lstDisLog = oNewDiscrepancyBAL.GetDiscrepancyLogDetailsBAL(oNewDiscrepancyBE);
            oNewDiscrepancyBAL = null;
            if (lstDisLog != null && lstDisLog.Count > 0)
            {
                Siteid = lstDisLog[0].Site.SiteID;
                Vendorid = Convert.ToInt32(lstDisLog[0].VendorID);
            }

            EmailList = GetDiscrepancyContactsOfVendor(Vendorid, Siteid);

        }
        return EmailList;

    }

    private int? sendAndSaveDiscrepancyCommunication(int iDiscrepancy, string LetterType)
    {

        GetDiscrepancyEmailListOfVendor();

        int? vendorID = Vendorid;

        sendCommunication communication = new sendCommunication();
        int? retVal = communication.sendCommunicationByEMail2(iDiscrepancy, "invoice", EmailList, vendorID, "", "", "", 0, LetterType);
        sSendMailLink = communication.sSendMailLink;
        return retVal;

    }
    #endregion

}
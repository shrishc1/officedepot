﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.Discrepancy.APAction;
using BusinessLogicLayer.ModuleBAL.Discrepancy.CountrySetting;
using BusinessEntities.ModuleBE.StockOverview;
using BusinessLogicLayer.ModuleBAL.StockOverview;
using BaseControlLibrary;
using BusinessEntities.ModuleBE.AdminFunctions;
using BusinessEntities.ModuleBE.Discrepancy.LogDiscrepancy;
using BusinessLogicLayer.ModuleBAL.AdminFunctions;
using BusinessLogicLayer.ModuleBAL.Discrepancy.LogDiscrepancy;
using Utilities;
using WebUtilities;
using BusinessLogicLayer.ModuleBAL.Appointment.Booking;
using BusinessEntities.ModuleBE.Languages.Languages;
using System.IO;
using System.Configuration;


public partial class APAction_APAction : System.Web.UI.UserControl
{
    #region Local Variables ...

    string AdditionEmailValidation = WebCommon.getGlobalResourceValue("AdditionEmailValidation");
    string ValueInDisputeMandatry = WebCommon.getGlobalResourceValue("ValueInDisputeMandatry");
    string ValuetobeDebitedMandatry = WebCommon.getGlobalResourceValue("ValuetobeDebitedMandatry");
    string CurrencyMandatry = WebCommon.getGlobalResourceValue("CurrencyMandatry");
    string ReasonMandatry = WebCommon.getGlobalResourceValue("ReasonMandatry");
    protected string DebitRaisedSuccessfully = WebCommon.getGlobalResourceValue("DebitRaisedSuccessfully");
    protected string DebitNoteAgainstDiscEmailSubject = WebCommon.getGlobalResourceValue("DebitNoteAgainstDiscEmailSubject");
    string PleaseentertheVendorVATReference = WebCommon.getGlobalResourceValue("PleaseentertheVendorVATReference");
    string PleaseselecttheOfficeDepotVATReference = WebCommon.getGlobalResourceValue("PleaseselecttheOfficeDepotVATReference");

    #endregion

    #region Event Handllers ...

    public event EventHandler SaveEventValidation;

    #endregion

    #region Properties ...

    public bool IsModelOpen { get; set; }
    public CommonPage CurrentPage { get; set; }

    public int DisLogID { get; set; }
    public int DisWFlowID { get; set; }
    public string PONumber { get; set; }
    public int SiteID { get; set; }
    public int VendorID { get; set; }
    public string VDRNo { get; set; }
    public string FromPage { get; set; }
    public string PreviousPage { get; set; }

    //+ ".aspx?disLogID=" + GetQueryStringValue("disLogID").ToString()
    //+ "&DisWFlowID=" + dtActionDetails.Rows[0]["DiscrepancyWorkflowID"].ToString()
    //+ "&PONo=" + dtActionDetails.Rows[0]["PurchaseOrderNumber"].ToString()
    //+ "&SiteID=" + SiteID.ToString()
    //+ "&VDRNo=" + VDRNo
    //+ "&FromPage=overs"
    //+ "&PreviousPage=" + G

    #endregion

    #region Events ...

    protected void Page_Load(object sender, EventArgs e)
    {
        ucCountry.CurrentPage = CurrentPage;

        if (!IsPostBack)
        {
            if (!string.IsNullOrEmpty(Convert.ToString(VendorID)))
                this.SetPOVendorVATReference(VendorID);

            if (!string.IsNullOrEmpty(Convert.ToString(SiteID)))
                this.SetVendODVATReference(SiteID);

            this.BindCurrency();
            this.BindDebitReason();
            //this.GetDiscripancyInfo();
            this.SetDiscripancyInfo();
            this.GetValueExpected();
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        this.IsModelOpen = false;

        if (string.IsNullOrEmpty(txtVendVendorVATReference.Text) && string.IsNullOrWhiteSpace(txtVendVendorVATReference.Text))
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + PleaseentertheVendorVATReference + "')", true);
            txtVendVendorVATReference.Focus();
            this.IsModelOpen = true;
            this.SaveEventValidation(sender, e);
            return;
        }

        //if (ddlVendOfficeDepotVATReference.Items.Count.Equals(0))
        //{
        //    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + PleaseselecttheOfficeDepotVATReference + "')", true);
        //    ddlVendOfficeDepotVATReference.Focus();
        //    this.IsModelOpen = true;
        //    this.SaveEventValidation(sender, e);
        //    return;
        //}

        //if (ddlVendOfficeDepotVATReference.SelectedIndex.Equals(0))
        //{
        //    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + PleaseselecttheOfficeDepotVATReference + "')", true);
        //    ddlVendOfficeDepotVATReference.Focus();
        //    this.IsModelOpen = true;
        //    this.SaveEventValidation(sender, e);
        //    return;
        //}

        //if (string.IsNullOrEmpty(txtValueInDispute.Text) && string.IsNullOrWhiteSpace(txtValueInDispute.Text))
        //{
        //    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + ValueInDisputeMandatry + "')", true);
        //    txtValueInDispute.Focus();
        //    this.IsModelOpen = true;
        //    this.SaveEventValidation(sender, e);
        //    return;
        //}

        if (string.IsNullOrEmpty(txtValueDebited.Text) && string.IsNullOrWhiteSpace(txtValueDebited.Text))
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + ValuetobeDebitedMandatry + "')", true);
            txtValueDebited.Focus();
            this.IsModelOpen = true;
            this.SaveEventValidation(sender, e);
            return;
        }

        if (ddlCurrency.Items.Count.Equals(0))
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + CurrencyMandatry + "')", true);
            ddlCurrency.Focus();
            this.IsModelOpen = true;
            this.SaveEventValidation(sender, e);
            return;
        }

        //if (ddlCurrency.SelectedIndex.Equals(0))
        //{
        //    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + CurrencyMandatry + "')", true);
        //    ddlCurrency.Focus();
        //    this.IsModelOpen = true;
        //    this.SaveEventValidation(sender, e);
        //    return;
        //}

        if (ddlReason.Items.Count.Equals(0))
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + ReasonMandatry + "')", true);
            ddlReason.Focus();
            this.IsModelOpen = true;
            this.SaveEventValidation(sender, e);
            return;
        }

        if (ddlReason.SelectedIndex.Equals(0))
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + ReasonMandatry + "')", true);
            ddlReason.Focus();
            this.IsModelOpen = true;
            this.SaveEventValidation(sender, e);
            return;
        }


        var txtAdditionalEmailList = (ucTextbox)ucSDRCommunication1.FindControl("txtAdditionalEmailList");
        if (!string.IsNullOrEmpty(txtAdditionalEmailList.Text) && !string.IsNullOrWhiteSpace(txtAdditionalEmailList.Text))
        {
            string[] sMailAddress;
            if (txtAdditionalEmailList.Text.Contains(','))
            {
                sMailAddress = txtAdditionalEmailList.Text.Trim().Split(',');

                for (int index = 0; index < sMailAddress.Length; index++)
                {
                    if (!RegexUtilities.IsValidEmail(sMailAddress[index].Trim()))
                    {
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + AdditionEmailValidation + "')", true);
                        txtAdditionalEmailList.Focus();
                        this.IsModelOpen = true;
                        this.SaveEventValidation(sender, e);
                        return;
                    }
                }
            }
            else
            {
                if (!RegexUtilities.IsValidEmail(txtAdditionalEmailList.Text.Trim()))
                {
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + AdditionEmailValidation + "')", true);
                    txtAdditionalEmailList.Focus();
                    this.IsModelOpen = true;
                    this.SaveEventValidation(sender, e);

                    return;
                }
            }
        }

        this.SaveDabit(sender, e);
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {

    }

    #endregion

    #region Methods ...

    private void BindCurrency()
    {
        var currencyBAL = new MAS_CurrencyBAL();
        var currencyBE = new CurrencyBE();
        currencyBE.Action = "ShowAll";
        var lstCurrency = currencyBAL.GetCurrenyBAL(currencyBE);
        if (lstCurrency != null && lstCurrency.Count > 0)
        {
            FillControls.FillDropDown(ref ddlCurrency, lstCurrency, "CurrencyName", "CurrencyID");

        }
    }

    private void BindDebitReason()
    {
        var debitReasonTypeBAL = new DIS_DebitReasonTypeBAL();
        var apActionBE = new APActionBE();
        apActionBE.Action = "GetReasonTypeSetUp";
        apActionBE.ReasonType = "DR"; /* Debit Reason */
        var lstAPActionBE = debitReasonTypeBAL.GetReasonTypeBAL(apActionBE);
        if (lstAPActionBE != null && lstAPActionBE.Count > 0)
        {
            FillControls.FillDropDown(ref ddlReason, lstAPActionBE, "Reason", "ReasonTypeID", "Select");
        }
    }

    private void GetDiscripancyInfo()
    {
        if (CurrentPage.GetQueryStringValue("disLogID") != null)
        {
            DiscrepancyBE oNewDiscrepancyBE = new DiscrepancyBE();
            DiscrepancyBAL oNewDiscrepancyBAL = new DiscrepancyBAL();
            oNewDiscrepancyBE.Action = "GetDiscrepancyLog";
            oNewDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(CurrentPage.GetQueryStringValue("disLogID").ToString());
            oNewDiscrepancyBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
            List<DiscrepancyBE> lstDisLog = oNewDiscrepancyBAL.GetDiscrepancyLogDetailsBAL(oNewDiscrepancyBE);
            if (lstDisLog != null && lstDisLog.Count > 0)
            {
                ucSDRCommunication1.FillAPContacts(Convert.ToInt32(lstDisLog[0].VendorID));
                ViewState["VendorID"] = lstDisLog[0].VendorID;
                ViewState["SiteID"] = lstDisLog[0].Site.SiteID;
                ViewState["PONumber"] = lstDisLog[0].PurchaseOrderNumber;
                ViewState["DiscWorkFlowID"] = lstDisLog[0].DiscrepancyWorkFlowID;
            }
            oNewDiscrepancyBAL = null;
        }
    }

    private void SetDiscripancyInfo()
    {
        ucSDRCommunication1.FillAPContacts(VendorID);
        //ViewState["DisLogID"] = DisLogID;
        ViewState["VendorID"] = VendorID;
        ViewState["SiteID"] = SiteID;
        ViewState["PONumber"] = PONumber;
        ViewState["DiscWorkFlowID"] = DisWFlowID;
        ViewState["FromPage"] = FromPage;
        ViewState["PreviousPage"] = PreviousPage;
    }

    private void GetValueExpected()
    {
        DISLog_WorkFlowBE oDISLog_WorkFlowBE = new DISLog_WorkFlowBE();
        DISLog_WorkFlowBAL oDISLog_WorkFlowBAL = new DISLog_WorkFlowBAL();
        if (!string.IsNullOrEmpty(CurrentPage.GetQueryStringValue("disLogID")))
        {
            oDISLog_WorkFlowBE.Action = "GetPOValueExpected";
            if (Convert.ToString(ViewState["FromPage"]).ToLower().Equals("qualityissue"))
            {
                oDISLog_WorkFlowBE.Action = "GetPOValueExpectedQualityIssue";
            }
            oDISLog_WorkFlowBE.DiscrepancyLogID = Convert.ToInt32(CurrentPage.GetQueryStringValue("disLogID"));

            List<DISLog_WorkFlowBE> lstActualValue = oDISLog_WorkFlowBAL.GetPOValueExpectedBAL(oDISLog_WorkFlowBE);
            if (lstActualValue.Count > 0 && lstActualValue != null)
            {
                txtValueInDispute.Text = txtValueDebited.Text = lstActualValue[0].ACP001_ValueExpected.ToString();

                if (ViewState["FromPage"] != null)
                {
                    var option = Convert.ToString(ViewState["FromPage"]).ToLower();
                    switch (option)
                    {
                        case "overs":
                            txtValueInDispute.Text = txtValueDebited.Text = lstActualValue[0].ACP001_ValueExpected.ToString();
                            break;
                        case "shortage":
                            txtValueInDispute.Text = txtValueDebited.Text = lstActualValue[0].ACP001_ShortageValueExpected.ToString();
                            break;
                        case "wrongpacksize":
                            txtValueInDispute.Text = txtValueDebited.Text = lstActualValue[0].ACP001_WrongPackValueExpected.ToString();
                            break;
                        case "goodsreceiveddamaged":
                            txtValueInDispute.Text = txtValueDebited.Text = lstActualValue[0].ACP001_DamageValueExpected.ToString();
                            break;
                        case "qualityissue":
                            txtValueInDispute.Text = txtValueDebited.Text = lstActualValue[0].ACP001_QualityValueExpected.ToString();
                            break;
                        case "incorrectproduct":
                            txtValueInDispute.Text = txtValueDebited.Text = lstActualValue[0].ACP001_IncorrectProductValueExpected.ToString();
                            break;
                        case "itemnotonpo":
                            txtValueInDispute.Text = txtValueDebited.Text = lstActualValue[0].ACP001_ValueExpected.ToString();
                            break;
                        default:
                            break;
                    }
                }
            }
        }
    }

    private void SaveDabit(object sender, EventArgs e)
    {
        if (CurrentPage.GetQueryStringValue("disLogID") != null)
        {
            var discripancyLogId = Convert.ToInt32(CurrentPage.GetQueryStringValue("disLogID"));
            var discrepancyBE = new DiscrepancyBE();
            var discrepancyBAL = new DiscrepancyBAL();

            #region Inserting data Debit raise tables ...
            discrepancyBE.Action = "AddDebitRaise";
            discrepancyBE.DebitRaseType = "D";

            if (ViewState["PONumber"] != null)
                discrepancyBE.PurchaseOrderNumber = Convert.ToString(ViewState["PONumber"]);

            if (ViewState["SiteID"] != null)
                discrepancyBE.SiteID = Convert.ToInt32(ViewState["SiteID"]);

            if (ViewState["VendorID"] != null)
                discrepancyBE.VendorID = Convert.ToInt32(ViewState["VendorID"]);

            discrepancyBE.InvoiceNo = string.Empty;
            discrepancyBE.ReferenceNo = string.Empty;

            discrepancyBE.VendorVatReference = txtVendVendorVATReference.Text;
            discrepancyBE.DebitTotalValue = Convert.ToDecimal(txtValueDebited.Text);
            if (ViewState["ODVatCodeID"] != null)
                discrepancyBE.ODVatReferenceID = Convert.ToInt32(ViewState["ODVatCodeID"]);
            
            var txtCommEmailList = (ucTextbox)ucSDRCommunication1.FindControl("txtVendorEmailList");
            var txtAdditionalEmailList = (ucTextbox)ucSDRCommunication1.FindControl("txtAdditionalEmailList");
            var emailIds = string.Empty;
            if (txtCommEmailList != null && txtAdditionalEmailList != null)
            {
                if ((!string.IsNullOrEmpty(txtCommEmailList.Text) && !string.IsNullOrWhiteSpace(txtCommEmailList.Text)) &&
                    (!string.IsNullOrEmpty(txtAdditionalEmailList.Text) && !string.IsNullOrWhiteSpace(txtAdditionalEmailList.Text)))
                {
                    emailIds = string.Format("{0},{1}", txtCommEmailList.Text, txtAdditionalEmailList.Text);
                }
                else if ((!string.IsNullOrEmpty(txtCommEmailList.Text) && !string.IsNullOrWhiteSpace(txtCommEmailList.Text)))
                {
                    emailIds = txtCommEmailList.Text;
                }
                else if ((!string.IsNullOrEmpty(txtAdditionalEmailList.Text) && !string.IsNullOrWhiteSpace(txtAdditionalEmailList.Text)))
                {
                    emailIds = txtAdditionalEmailList.Text;
                }
            }

            discrepancyBE.CommunicationEmails = emailIds;
            discrepancyBE.InternalComments = txtCommentToVendor.Text;

            //if (ddlCurrency.SelectedIndex > 0)
            //{
                discrepancyBE.Currency = new CurrencyBE();
                discrepancyBE.Currency.CurrencyId = Convert.ToInt32(ddlCurrency.SelectedValue);
           // }

            discrepancyBE.DiscrepancyLogID = discripancyLogId;
            discrepancyBE.Status = "A"; // A is an Active status of Debit Raise.
            discrepancyBE.UserID = Convert.ToInt32(Session["UserID"]);
            discrepancyBE.Country = new MAS_CountryBE();

            if (ucCountry.innerControlddlCountry.Items.Count > 0)
                discrepancyBE.Country.CountryID = Convert.ToInt32(ucCountry.innerControlddlCountry.SelectedValue);

            discrepancyBE.APActionRef = "ACP001";

            var debitRaiseId = discrepancyBAL.addDebitRaiseBAL(discrepancyBE);
            if (debitRaiseId > 0)
            {
                var discrepancy = new DiscrepancyBE();
                discrepancy.Action = "AddDebitRaiseItem";
                discrepancy.DebitRaiseId = debitRaiseId;
                discrepancy.StateValueDebited = Convert.ToDecimal(txtValueDebited.Text);

                if (ddlReason.SelectedIndex > 0)
                    discrepancy.DebitReasonId = Convert.ToInt32(ddlReason.SelectedValue);

                discrepancyBAL.addDebitRaiseItemBAL(discrepancy);
            }

            #endregion

            #region Get Debit Raise data And Logic to Save & Send email & WorkFlow entry...
            if (debitRaiseId > 0)
            {
                discrepancyBE = new DiscrepancyBE();
                discrepancyBE.Action = "GetAllDebitRaise";
                discrepancyBE.DebitRaiseId = debitRaiseId;
                var lstDiscrepancyBE = discrepancyBAL.GetAllDebitRaiseBAL(discrepancyBE);
                if (lstDiscrepancyBE != null && lstDiscrepancyBE.Count > 0)
                {
                    ViewState["DebitNoteNo"] = lstDiscrepancyBE[0].DebitNoteNo;
                    DebitRaisedSuccessfully = DebitRaisedSuccessfully.Replace("##RaisedDebitNumber##", lstDiscrepancyBE[0].DebitNoteNo);
                    DebitNoteAgainstDiscEmailSubject = DebitNoteAgainstDiscEmailSubject.Replace("##DebitNoteNumber##", lstDiscrepancyBE[0].DebitNoteNo);
                    #region Logic to Save & Send email ...
                    var oAPPBOK_CommunicationBAL = new APPBOK_CommunicationBAL();
                    var oLanguages = oAPPBOK_CommunicationBAL.GetLanguages();
                    foreach (MAS_LanguageBE objLanguage in oLanguages)
                    {
                        bool MailSentInLanguage = false;
                        if (objLanguage.LanguageID.Equals(lstDiscrepancyBE[0].Vendor.LanguageID))
                            MailSentInLanguage = true;

                        var path = AppDomain.CurrentDomain.BaseDirectory.ToLower();
                        //var templatesPath = @"emailtemplates/communication1";
                        var templateFile = string.Format(@"{0}emailtemplates/communication1/DebitNoteAgainstDiscrepancy.english.htm", path);

                        #region Setting reason as per the language ...
                        switch (objLanguage.LanguageID)
                        {
                            case 1:
                                Page.UICulture = clsConstants.EnglishISO; //"en-US"; // English
                                break;
                            case 2:
                                Page.UICulture = clsConstants.FranceISO; //"fr"; // France
                                break;
                            case 3:
                                Page.UICulture = clsConstants.GermanyISO; //"de"; // German
                                break;
                            case 4:
                                Page.UICulture = clsConstants.NederlandISO; //"nl"; // Dutch
                                break;
                            case 5:
                                Page.UICulture = clsConstants.SpainISO; //"es"; // Spanish
                                break;
                            case 6:
                                Page.UICulture = clsConstants.ItalyISO; //"it"; // Italy
                                break;
                            case 7:
                                Page.UICulture = clsConstants.CzechISO; //"cz"; // Czech
                                break;
                            default:
                                Page.UICulture = clsConstants.EnglishISO; // "en-US"; // English
                                break;
                        }

                        #endregion

                        #region  Prepairing html body format ...
                        string htmlBody = null;
                        using (StreamReader sReader = new StreamReader(templateFile.ToLower()))
                        {
                            htmlBody = sReader.ReadToEnd();
                            //htmlBody = htmlBody.Replace("{logoInnerPath}", getAbsolutePath());

                            htmlBody = htmlBody.Replace("{VendorNumber}", lstDiscrepancyBE[0].Vendor.Vendor_No);
                            htmlBody = htmlBody.Replace("{DebitNoteNumber}", WebCommon.getGlobalResourceValue("DebitNoteNumber"));
                            htmlBody = htmlBody.Replace("{DebitNoteNumberValue}", lstDiscrepancyBE[0].DebitNoteNo);
                            htmlBody = htmlBody.Replace("{VendorName}", lstDiscrepancyBE[0].Vendor.VendorName);
                            htmlBody = htmlBody.Replace("{OurVATReference}", WebCommon.getGlobalResourceValue("OurVATReference"));

                            if (!string.IsNullOrEmpty(lstDiscrepancyBE[0].OurVATReference) && !string.IsNullOrWhiteSpace(lstDiscrepancyBE[0].OurVATReference))
                                htmlBody = htmlBody.Replace("{OurVATReferenceValue}", lstDiscrepancyBE[0].OurVATReference);
                            else
                                htmlBody = htmlBody.Replace("{OurVATReferenceValue}", string.Empty);

                            htmlBody = htmlBody.Replace("{VendorAddress1}", lstDiscrepancyBE[0].Vendor.address1);
                            htmlBody = htmlBody.Replace("{YourVATReference}", WebCommon.getGlobalResourceValue("YourVATReference"));

                            if (!string.IsNullOrEmpty(lstDiscrepancyBE[0].YourVATReference) && !string.IsNullOrWhiteSpace(lstDiscrepancyBE[0].YourVATReference))
                                htmlBody = htmlBody.Replace("{YourVATReferenceValue}", lstDiscrepancyBE[0].YourVATReference);
                            else
                                htmlBody = htmlBody.Replace("{YourVATReferenceValue}", string.Empty);

                            htmlBody = htmlBody.Replace("{VendorAddress2}", lstDiscrepancyBE[0].Vendor.address2);
                            htmlBody = htmlBody.Replace("{DebitReason}", WebCommon.getGlobalResourceValue("DebitReason"));
                            htmlBody = htmlBody.Replace("{DebitReasonValue}", lstDiscrepancyBE[0].Reason);

                            if (!string.IsNullOrEmpty(lstDiscrepancyBE[0].Vendor.VMPPIN) && !string.IsNullOrWhiteSpace(lstDiscrepancyBE[0].Vendor.VMPPIN))
                                htmlBody = htmlBody.Replace("{VMPPIN}", lstDiscrepancyBE[0].Vendor.VMPPIN);
                            else
                                htmlBody = htmlBody.Replace("{VMPPIN}", string.Empty);

                            if (!string.IsNullOrEmpty(lstDiscrepancyBE[0].Vendor.VMPPOU) && !string.IsNullOrWhiteSpace(lstDiscrepancyBE[0].Vendor.VMPPOU))
                                htmlBody = htmlBody.Replace("{VMPPOU}", lstDiscrepancyBE[0].Vendor.VMPPOU);
                            else
                                htmlBody = htmlBody.Replace("{VMPPOU}", string.Empty);

                            if (!string.IsNullOrEmpty(lstDiscrepancyBE[0].Vendor.city) && !string.IsNullOrWhiteSpace(lstDiscrepancyBE[0].Vendor.city))
                                htmlBody = htmlBody.Replace("{VendorCity}", lstDiscrepancyBE[0].Vendor.city);
                            else
                                htmlBody = htmlBody.Replace("{VendorCity}", string.Empty);

                            htmlBody = htmlBody.Replace("{DearSirMadam}", WebCommon.getGlobalResourceValue("DearSirMadam"));
                            htmlBody = htmlBody.Replace("{Date}", WebCommon.getGlobalResourceValue("Date"));

                            var debitRaiseDate = Convert.ToDateTime(lstDiscrepancyBE[0].DebitRaiseDate);
                            htmlBody = htmlBody.Replace("{DateValue}", debitRaiseDate.ToString("dd/MM/yyyy"));

                            htmlBody = htmlBody.Replace("{DebitLetterMessage1}", WebCommon.getGlobalResourceValue("DebitLetterMessage1"));
                            htmlBody = htmlBody.Replace("{VDRNo}", WebCommon.getGlobalResourceValue("VDRNoKey"));
                            htmlBody = htmlBody.Replace("{VDRNoValue}", lstDiscrepancyBE[0].VDRNo);
                            htmlBody = htmlBody.Replace("{DebitLetterMessage2}", WebCommon.getGlobalResourceValue("DebitLetterMessage2"));

                            var vdrDateValue = Convert.ToDateTime(lstDiscrepancyBE[0].DiscrepancyLogDate);
                            htmlBody = htmlBody.Replace("{VDRDateValue}", vdrDateValue.ToString("dd/MM/yyyy"));

                            htmlBody = htmlBody.Replace("{DebitLetterMessage3}", WebCommon.getGlobalResourceValue("DebitLetterMessage3"));
                            htmlBody = htmlBody.Replace("{DebitValue}", Convert.ToString(lstDiscrepancyBE[0].StateValueDebited));
                            htmlBody = htmlBody.Replace("{DebitCurrencyValue}", Convert.ToString(lstDiscrepancyBE[0].Currency.CurrencyName));
                            htmlBody = htmlBody.Replace("{DebitLetterMessage4}", WebCommon.getGlobalResourceValue("DebitLetterMessage4"));
                            htmlBody = htmlBody.Replace("{DebiNoteComments}", lstDiscrepancyBE[0].Comment);
                            htmlBody = htmlBody.Replace("{DebitLetterMessage5}", WebCommon.getGlobalResourceValue("DebitLetterMessage5"));
                            htmlBody = htmlBody.Replace("{Yoursfaithfully}", WebCommon.getGlobalResourceValue("Yoursfaithfully"));
                            htmlBody = htmlBody.Replace("{APClerkName}", lstDiscrepancyBE[0].CreatedBy);

                            if (!string.IsNullOrEmpty(lstDiscrepancyBE[0].CreatedByContactNumber) && !string.IsNullOrWhiteSpace(lstDiscrepancyBE[0].CreatedByContactNumber))
                                htmlBody = htmlBody.Replace("{APClerkContactNo}", lstDiscrepancyBE[0].CreatedByContactNumber);
                            else
                                htmlBody = htmlBody.Replace("{APClerkContactNo}", string.Empty);

                            var apAddress = string.Empty;
                            if (!string.IsNullOrEmpty(lstDiscrepancyBE[0].APAddress1) && !string.IsNullOrEmpty(lstDiscrepancyBE[0].APAddress2)
                                && !string.IsNullOrEmpty(lstDiscrepancyBE[0].APAddress3) && !string.IsNullOrEmpty(lstDiscrepancyBE[0].APAddress4)
                                && !string.IsNullOrEmpty(lstDiscrepancyBE[0].APAddress5) && !string.IsNullOrEmpty(lstDiscrepancyBE[0].APAddress6))
                            {
                                apAddress = string.Format("{0},&nbsp;{1},&nbsp;{2},&nbsp;{3},&nbsp;{4},&nbsp;{5}", lstDiscrepancyBE[0].APAddress1, lstDiscrepancyBE[0].APAddress2,
                                    lstDiscrepancyBE[0].APAddress3, lstDiscrepancyBE[0].APAddress4, lstDiscrepancyBE[0].APAddress5, lstDiscrepancyBE[0].APAddress6);
                            }
                            else if (!string.IsNullOrEmpty(lstDiscrepancyBE[0].APAddress1) && !string.IsNullOrEmpty(lstDiscrepancyBE[0].APAddress2)
                                && !string.IsNullOrEmpty(lstDiscrepancyBE[0].APAddress3) && !string.IsNullOrEmpty(lstDiscrepancyBE[0].APAddress4)
                                && !string.IsNullOrEmpty(lstDiscrepancyBE[0].APAddress5))
                            {
                                apAddress = string.Format("{0},&nbsp;{1},&nbsp;{2},&nbsp;{3},&nbsp;{4}", lstDiscrepancyBE[0].APAddress1, lstDiscrepancyBE[0].APAddress2,
                                    lstDiscrepancyBE[0].APAddress3, lstDiscrepancyBE[0].APAddress4, lstDiscrepancyBE[0].APAddress5);
                            }
                            else if (!string.IsNullOrEmpty(lstDiscrepancyBE[0].APAddress1) && !string.IsNullOrEmpty(lstDiscrepancyBE[0].APAddress2)
                               && !string.IsNullOrEmpty(lstDiscrepancyBE[0].APAddress3) && !string.IsNullOrEmpty(lstDiscrepancyBE[0].APAddress4))
                            {
                                apAddress = string.Format("{0},&nbsp;{1},&nbsp;{2},&nbsp;{3}", lstDiscrepancyBE[0].APAddress1, lstDiscrepancyBE[0].APAddress2,
                                    lstDiscrepancyBE[0].APAddress3, lstDiscrepancyBE[0].APAddress4);
                            }
                            else if (!string.IsNullOrEmpty(lstDiscrepancyBE[0].APAddress1) && !string.IsNullOrEmpty(lstDiscrepancyBE[0].APAddress2)
                               && !string.IsNullOrEmpty(lstDiscrepancyBE[0].APAddress3))
                            {
                                apAddress = string.Format("{0},&nbsp;{1},&nbsp;{2}", lstDiscrepancyBE[0].APAddress1, lstDiscrepancyBE[0].APAddress2,
                                    lstDiscrepancyBE[0].APAddress3);
                            }
                            else if (!string.IsNullOrEmpty(lstDiscrepancyBE[0].APAddress1) && !string.IsNullOrEmpty(lstDiscrepancyBE[0].APAddress2))
                            {
                                apAddress = string.Format("{0},&nbsp;{1}", lstDiscrepancyBE[0].APAddress1, lstDiscrepancyBE[0].APAddress2);
                            }
                            else
                            {
                                apAddress = lstDiscrepancyBE[0].APAddress1;
                            }

                            if (!string.IsNullOrEmpty(lstDiscrepancyBE[0].APPincode))
                                apAddress = string.Format("{0},&nbsp;{1}", apAddress, lstDiscrepancyBE[0].APPincode);

                            htmlBody = htmlBody.Replace("{APClerkAddress}", apAddress);

                        }
                        #endregion

                        #region Sending and saving email details ...
                        string[] sMailAddress = emailIds.Split(',');
                        var sentToWithLink = new System.Text.StringBuilder();
                        for (int index = 0; index < sMailAddress.Length; index++)
                            sentToWithLink.Append("<a target='_blank' href='" + Common.EncryptQuery("../LogDiscrepancy/DISLog_ReSendCommunication.aspx?CommTitle=DebitRaised&DebitRaiseId=" + debitRaiseId + "&CommunicationType=DebitRaised") + "' onclick='window.open(this.href, this.target,\"scrollbars=1,left=100px,top=100px,width=1000px,height=800px\"); return false;'>" + sMailAddress[index] + "</a>" + System.Environment.NewLine);

                        var discrepancyMailBE = new DiscrepancyMailBE();
                        discrepancyMailBE.Action = "AddDebitRaiseCommunication";
                        discrepancyMailBE.DebitRaiseId = debitRaiseId;
                        discrepancyMailBE.mailSubject = DebitNoteAgainstDiscEmailSubject;
                        discrepancyMailBE.sentTo = emailIds;
                        discrepancyMailBE.mailBody = htmlBody;
                        discrepancyMailBE.SendByID = Convert.ToInt32(Session["UserID"]);
                        discrepancyMailBE.IsMailSent = MailSentInLanguage;
                        discrepancyMailBE.languageID = objLanguage.LanguageID;
                        discrepancyMailBE.MailSentInLanguage = MailSentInLanguage;
                        discrepancyMailBE.CommTitle = "DebitRaised";
                        discrepancyMailBE.SentToWithLink = sentToWithLink.ToString();
                        var debitRaiseCommId = discrepancyBAL.addDebitRaiseCommunicationBAL(discrepancyMailBE);
                        if (MailSentInLanguage)
                        {
                            var emailToAddress = emailIds;
                            var emailFromAddress = Convert.ToString(ConfigurationManager.AppSettings["fromAddress"]);
                            var emailToSubject = DebitNoteAgainstDiscEmailSubject;
                            var emailBody = htmlBody;
                            Utilities.clsEmail oclsEmail = new Utilities.clsEmail();
                            oclsEmail.sendMail(emailToAddress, emailBody, emailToSubject, emailFromAddress, true);
                        }
                        #endregion

                        Page.UICulture = Convert.ToString(Session["CultureInfo"]);
                    }
                    #endregion
                }

                #region Inserting data in Discrepancy Workflow tables ...
                /*
                DISLog_WorkFlowBE oDISLog_WorkFlowBE = new DISLog_WorkFlowBE();
                DISLog_WorkFlowBAL oDISLog_WorkFlowBAL = new DISLog_WorkFlowBAL();
                oDISLog_WorkFlowBE.Action = "InsertUpdateWorkflowActions";
                oDISLog_WorkFlowBE.DiscrepancyLogID = discripancyLogId;
                oDISLog_WorkFlowBE.ACP001_NoteType = 'D';

                if (ViewState["DebitNoteNo"] != null)
                    oDISLog_WorkFlowBE.ACP001_NoteNumber = Convert.ToString(ViewState["DebitNoteNo"]);

                oDISLog_WorkFlowBE.ACP001_ValueExpected = txtValueInDispute.Text != string.Empty ? Convert.ToDecimal(txtValueInDispute.Text) : (decimal?)null;
                oDISLog_WorkFlowBE.ACP001_ActualValue = txtValueDebited.Text != string.Empty ? Convert.ToDecimal(txtValueDebited.Text) : (decimal?)null;
                oDISLog_WorkFlowBE.StageCompletedForWhom = "ACP001_StageCompleted";
                oDISLog_WorkFlowBE.StageCompleted = true;
                oDISLog_WorkFlowBE.Comment = txtCommentToVendor.Text;

                //update the work flow action table each time for the comments and other fields( if stage is completed)
                if (ViewState["DiscWorkFlowID"] != null)
                    oDISLog_WorkFlowBE.DiscrepancyWorkFlowID = Convert.ToInt32(ViewState["DiscWorkFlowID"]);

                //insert or update the record according to the stage completed status
                int? iResult = oDISLog_WorkFlowBAL.addWorkFlowActionsBAL(oDISLog_WorkFlowBE);

                if (ViewState["FromPage"] != null)
                {
                    var option = Convert.ToString(ViewState["FromPage"]).ToLower();
                    switch (option)
                    {
                        case "overs":
                            this.OversDebitHTML();
                            break;
                        case "shortage":
                            this.ShortageDebitHTML();
                            break;
                        case "wrongpacksize":
                            this.IncorrectProductDebitHTML();
                            break;
                        case "goodsreceiveddamaged":
                            GoodsReceivedDamagedDebitHTML();
                            break;
                        case "qualityissue":
                            QualityIssueDebitHTML();
                            break;
                        case "incorrectproduct":
                            IncorrectProductDebitHTML();
                            break;
                        default:
                            break;
                    }
                }

                */

                #endregion
            }

            #endregion            

            //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + DebitRaisedSuccessfully + "')", true);
            //this.IsModelOpen = true;
            //this.SaveEventValidation(sender, e);

            CurrentPage.EncryptQueryString("~/ModuleUI/Discrepancy/DIS_SearchResult.aspx?PreviousPage=usercontrol");
        }
    }

    private void OversDebitHTML()
    {
        DISLog_WorkFlowBE oDISLog_WorkFlowBE = new DISLog_WorkFlowBE();
        DISLog_WorkFlowBAL oDISLog_WorkFlowBAL = new DISLog_WorkFlowBAL();
        DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
        DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();

        //if (chkDebitStageComplete.Checked)
        //{
        WorkflowHTML oWorkflowHTML = new WorkflowHTML();
        //Vendor's action is pending then print only Vendor's HTML
        //  if(bVenActionRequired == true){

        //Check whether ACP002 has completed its stage or not..If Yes then Close Discrepancy otherwise not.
        oDISLog_WorkFlowBE.Action = "GetDataForSpecificAction";
        oDISLog_WorkFlowBE.DiscrepancyLogID = Convert.ToInt32(CurrentPage.GetQueryStringValue("disLogID").ToString());
        oDISLog_WorkFlowBE.StageCompletedForWhom = "ACP002_StageCompleted";
        DataTable dtACO002 = oDISLog_WorkFlowBAL.GetDataForSpecificActionBAL(oDISLog_WorkFlowBE);
        if (dtACO002 != null && dtACO002.Rows.Count > 0)
        {

            oDiscrepancyBE.CloseDiscrepancy = true;
        }
        oDiscrepancyBE.GINHTML = oWorkflowHTML.function3("white", null, "", "");
        oDiscrepancyBE.INVHTML = oWorkflowHTML.function3("white", null, "", "");

        string debitNoteNumber = string.Empty;
        if (ViewState["DebitNoteNo"] != null)
            debitNoteNumber = Convert.ToString(ViewState["DebitNoteNo"]);

        oDiscrepancyBE.APHTML = oWorkflowHTML.function1(DateTime.Now, null, null, "Yellow", "", "Debit Note Raised",
            UIUtility.GetODVendorUserName(Convert.ToString(Session["Role"]), Convert.ToString(Session["UserName"]), Convert.ToString(Session["LoginID"])),
            txtCommentToVendor.Text, "", debitNoteNumber, "", "", "", "", "", "", "", "", "", "", "", "", "", "", txtValueDebited.Text);

        oDiscrepancyBE.VENHTML = oWorkflowHTML.function3("white", null, "", "");
        oDiscrepancyBE.Action = "InsertHTML";
        oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(CurrentPage.GetQueryStringValue("disLogID").ToString());
        oDiscrepancyBE.LoggedDateTime = DateTime.Now;
        oDiscrepancyBE.LevelNumber = 1;
        int? iResult1 = oDiscrepancyBAL.addWorkFlowHTMLsBAL(oDiscrepancyBE);

        //}
        //else
        //{        
        //    InsertHTML(txtDebitComment.Text, "Debit Note Raised", txtDebitNoteNumber.Text, "D");
        //}
    }

    private void ShortageDebitHTML()
    {
        DISLog_WorkFlowBE oDISLog_WorkFlowBE = new DISLog_WorkFlowBE();
        DISLog_WorkFlowBAL oDISLog_WorkFlowBAL = new DISLog_WorkFlowBAL();
        //now insert the work flow HTML according to the action taken only if the stage is completed
        DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
        DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();

        oDISLog_WorkFlowBE.Action = "GetDataForSpecificAction";
        oDISLog_WorkFlowBE.DiscrepancyLogID = Convert.ToInt32(CurrentPage.GetQueryStringValue("disLogID").ToString());
        oDISLog_WorkFlowBE.StageCompletedForWhom = "VEN003_StageCompleted";
        DataTable dtVEN003 = oDISLog_WorkFlowBAL.GetDataForSpecificActionBAL(oDISLog_WorkFlowBE);

        oDISLog_WorkFlowBE.StageCompletedForWhom = "VEN004_StageCompleted";
        DataTable dtVEN004 = oDISLog_WorkFlowBAL.GetDataForSpecificActionBAL(oDISLog_WorkFlowBE);

        //if (chkDebitStageComplete.Checked)
        //{
        WorkflowHTML oWorkflowHTML = new WorkflowHTML();
        oDiscrepancyBE.GINHTML = oWorkflowHTML.function3("white", null, "", "");
        oDiscrepancyBE.GINActionRequired = false;

        oDiscrepancyBE.INVHTML = oWorkflowHTML.function3("white", null, "", "");
        oDiscrepancyBE.INVActionRequired = false;

        string debitNoteNumber = string.Empty;
        if (ViewState["DebitNoteNo"] != null)
            debitNoteNumber = Convert.ToString(ViewState["DebitNoteNo"]);

        oDiscrepancyBE.APHTML = oWorkflowHTML.function1(DateTime.Now, null, null, "Yellow", "", "Debit Note Raised",
            UIUtility.GetODVendorUserName(Convert.ToString(Session["Role"]), Convert.ToString(Session["UserName"]), Convert.ToString(Session["LoginID"])),
            txtCommentToVendor.Text, "", debitNoteNumber, "", "", "", "", "", "", "", "", "", "", "", "", "", "", txtValueDebited.Text);

        oDiscrepancyBE.APActionRequired = false;

        oDiscrepancyBE.VENHTML = oWorkflowHTML.function3("white", null, "", "");
        oDiscrepancyBE.VenActionRequired = false;


        oDiscrepancyBE.Action = "InsertHTML";
        oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(CurrentPage.GetQueryStringValue("disLogID").ToString());
        oDiscrepancyBE.LoggedDateTime = DateTime.Now;
        oDiscrepancyBE.LevelNumber = 1;

        if ((dtVEN003 != null && dtVEN003.Rows.Count > 0) || dtVEN004 != null && dtVEN004.Rows.Count > 0)
            oDiscrepancyBE.CloseDiscrepancy = true;

        int? iResult1 = oDiscrepancyBAL.addWorkFlowHTMLsBAL(oDiscrepancyBE);

        //}
        //else
        //{       
        //    InsertHTML(txtCommentToVendor.Text, "Debit Note Raised", txtDebitNoteNumber.Text, "D");
        //}
    }

    private void IncorrectProductDebitHTML()
    {
        DISLog_WorkFlowBE oDISLog_WorkFlowBE = new DISLog_WorkFlowBE();
        DISLog_WorkFlowBAL oDISLog_WorkFlowBAL = new DISLog_WorkFlowBAL();
        //now insert the work flow HTML according to the action taken only if the stage is completed
        DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
        DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();
        WorkflowHTML oWorkflowHTML = new WorkflowHTML();

        //if (chkDebitStageComplete.Checked)
        //{
        //Check whether ACP002 has completed its stage or not..If Yes then Close Discrepancy otherwise not.
        oDISLog_WorkFlowBE.Action = "GetDataForSpecificAction";
        oDISLog_WorkFlowBE.DiscrepancyLogID = Convert.ToInt32(CurrentPage.GetQueryStringValue("disLogID").ToString());
        oDISLog_WorkFlowBE.StageCompletedForWhom = "ACP002_StageCompleted";
        DataTable dtACO002 = oDISLog_WorkFlowBAL.GetDataForSpecificActionBAL(oDISLog_WorkFlowBE);
        if (dtACO002 != null && dtACO002.Rows.Count > 0)
        {
            oDiscrepancyBE.CloseDiscrepancy = true;
        }

        oDiscrepancyBE.GINHTML = oWorkflowHTML.function3("white", null, "", "");
        oDiscrepancyBE.GINActionRequired = false;

        oDiscrepancyBE.INVHTML = oWorkflowHTML.function3("white", null, "", "");
        oDiscrepancyBE.INVActionRequired = false;

        string debitNoteNumber = string.Empty;
        if (ViewState["DebitNoteNo"] != null)
            debitNoteNumber = Convert.ToString(ViewState["DebitNoteNo"]);

        oDiscrepancyBE.APHTML = oWorkflowHTML.function1(DateTime.Now, null, null, "Yellow", "", "Debit Note Raised",
            UIUtility.GetODVendorUserName(Convert.ToString(Session["Role"]), Convert.ToString(Session["UserName"]), Convert.ToString(Session["LoginID"])),
            txtCommentToVendor.Text, "", debitNoteNumber, "", "", "", "", "", "", "", "", "", "", "", "", "", "", txtValueDebited.Text);

        oDiscrepancyBE.APActionRequired = false;

        oDiscrepancyBE.VENHTML = oWorkflowHTML.function3("white", null, "", "");
        oDiscrepancyBE.VenActionRequired = false;

        oDiscrepancyBE.Action = "InsertHTML";
        oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(CurrentPage.GetQueryStringValue("disLogID").ToString());
        oDiscrepancyBE.LoggedDateTime = DateTime.Now;
        oDiscrepancyBE.LevelNumber = 1;
        int? iResult = oDiscrepancyBAL.addWorkFlowHTMLsBAL(oDiscrepancyBE);
        // }
        //}
        //else
        //{
        //    InsertHTML(txtCommentToVendor.Text, "Debit Note Raised", txtDebitNoteNumber.Text, "D");
        //}
    }

    private void GoodsReceivedDamagedDebitHTML()
    {
        DISLog_WorkFlowBE oDISLog_WorkFlowBE = new DISLog_WorkFlowBE();
        DISLog_WorkFlowBAL oDISLog_WorkFlowBAL = new DISLog_WorkFlowBAL();
        //now insert the work flow HTML according to the action taken only if the stage is completed
        DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
        DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();
        WorkflowHTML oWorkflowHTML = new WorkflowHTML();

        //if (chkDebitStageComplete.Checked)
        //{
        oDISLog_WorkFlowBE.Action = "GetDataForSpecificAction";
        oDISLog_WorkFlowBE.DiscrepancyLogID = Convert.ToInt32(CurrentPage.GetQueryStringValue("disLogID").ToString());
        oDISLog_WorkFlowBE.StageCompletedForWhom = "GIN002_StageCompleted";
        DataTable dtGIN = oDISLog_WorkFlowBAL.GetDataForSpecificActionBAL(oDISLog_WorkFlowBE);
        if (dtGIN != null && dtGIN.Rows.Count > 0)
        {
            oDiscrepancyBE.CloseDiscrepancy = true;
        }
        oDISLog_WorkFlowBE.Action = "GetDataForSpecificAction";
        oDISLog_WorkFlowBE.DiscrepancyLogID = Convert.ToInt32(CurrentPage.GetQueryStringValue("disLogID").ToString());
        oDISLog_WorkFlowBE.StageCompletedForWhom = "ACP002_StageCompleted";
        DataTable dtAP = oDISLog_WorkFlowBAL.GetDataForSpecificActionBAL(oDISLog_WorkFlowBE);
        if (dtAP != null && dtAP.Rows.Count > 0)
        {
            oDiscrepancyBE.CloseDiscrepancy = true;
        }

        oDISLog_WorkFlowBE.Action = "GetDataForSpecificAction";
        oDISLog_WorkFlowBE.DiscrepancyLogID = Convert.ToInt32(CurrentPage.GetQueryStringValue("disLogID").ToString());
        oDISLog_WorkFlowBE.StageCompletedForWhom = "GIN004_StageCompleted";
        DataTable dtGIN4 = oDISLog_WorkFlowBAL.GetDataForSpecificActionBAL(oDISLog_WorkFlowBE);
        if (dtGIN4 != null && dtGIN4.Rows.Count > 0)
        {
            oDiscrepancyBE.CloseDiscrepancy = true;
        }

        oDiscrepancyBE.GINHTML = oWorkflowHTML.function3("white", null, "", "");
        oDiscrepancyBE.GINActionRequired = false;

        oDiscrepancyBE.INVHTML = oWorkflowHTML.function3("white", null, "", "");
        oDiscrepancyBE.INVActionRequired = false;

        string debitNoteNumber = string.Empty;
        if (ViewState["DebitNoteNo"] != null)
            debitNoteNumber = Convert.ToString(ViewState["DebitNoteNo"]);

        oDiscrepancyBE.APHTML = oWorkflowHTML.function1(DateTime.Now, null, null, "Yellow", "", "Debit Note Raised",
            UIUtility.GetODVendorUserName(Convert.ToString(Session["Role"]), Convert.ToString(Session["UserName"]), Convert.ToString(Session["LoginID"])),
            txtCommentToVendor.Text, "", debitNoteNumber, "", "", "", "", "", "", "", "", "", "", "", "", "", "", txtValueDebited.Text);

        oDiscrepancyBE.APActionRequired = false;

        oDiscrepancyBE.VENHTML = oWorkflowHTML.function3("white", null, "", "");
        oDiscrepancyBE.VenActionRequired = false;

        oDiscrepancyBE.Action = "InsertHTML";
        oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(CurrentPage.GetQueryStringValue("disLogID").ToString());
        oDiscrepancyBE.LoggedDateTime = DateTime.Now;
        oDiscrepancyBE.LevelNumber = 1;
        int? iResult = oDiscrepancyBAL.addWorkFlowHTMLsBAL(oDiscrepancyBE);
        //}
        //}
        //else
        //{
        //    InsertHTML(txtCommentToVendor.Text, "Debit Note Raised", txtDebitNoteNumber.Text, "D");
        //}
    }

    private void QualityIssueDebitHTML()
    {
        DISLog_WorkFlowBE oDISLog_WorkFlowBE = new DISLog_WorkFlowBE();
        DISLog_WorkFlowBAL oDISLog_WorkFlowBAL = new DISLog_WorkFlowBAL();
        //now insert the work flow HTML according to the action taken only if the stage is completed
        DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
        DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();
        WorkflowHTML oWorkflowHTML = new WorkflowHTML();

        //if (chkDebitStageComplete.Checked)
        //{
        oDISLog_WorkFlowBE.Action = "GetDataForSpecificAction";
        oDISLog_WorkFlowBE.DiscrepancyLogID = Convert.ToInt32(CurrentPage.GetQueryStringValue("disLogID").ToString());
        oDISLog_WorkFlowBE.StageCompletedForWhom = "GIN002_StageCompleted";
        DataTable dtGIN = oDISLog_WorkFlowBAL.GetDataForSpecificActionBAL(oDISLog_WorkFlowBE);
        if (dtGIN != null && dtGIN.Rows.Count > 0)
        {
            oDiscrepancyBE.CloseDiscrepancy = true;
        }

        oDiscrepancyBE.GINHTML = oWorkflowHTML.function3("white", null, "", "");
        oDiscrepancyBE.GINActionRequired = false;

        oDiscrepancyBE.INVHTML = oWorkflowHTML.function3("white", null, "", "");
        oDiscrepancyBE.INVActionRequired = false;

        string debitNoteNumber = string.Empty;
        if (ViewState["DebitNoteNo"] != null)
            debitNoteNumber = Convert.ToString(ViewState["DebitNoteNo"]);

        oDiscrepancyBE.APHTML = oWorkflowHTML.function1(DateTime.Now, null, null, "Yellow", "", "Debit Note Raised",
            UIUtility.GetODVendorUserName(Convert.ToString(Session["Role"]), Convert.ToString(Session["UserName"]), Convert.ToString(Session["LoginID"])),
            txtCommentToVendor.Text, "", debitNoteNumber, "", "", "", "", "", "", "", "", "", "", "", "", "", "", txtValueDebited.Text);

        oDiscrepancyBE.APActionRequired = false;

        oDiscrepancyBE.VENHTML = oWorkflowHTML.function3("white", null, "", "");
        oDiscrepancyBE.VenActionRequired = false;

        oDiscrepancyBE.Action = "InsertHTML";
        oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(CurrentPage.GetQueryStringValue("disLogID").ToString());
        oDiscrepancyBE.LoggedDateTime = DateTime.Now;
        oDiscrepancyBE.LevelNumber = 1;
        int? iResult = oDiscrepancyBAL.addWorkFlowHTMLsBAL(oDiscrepancyBE);
        //}
        //else
        //{ 
        //    InsertHTML(txtCommentToVendor.Text, "Debit Note Raised", txtDebitNoteNumber.Text, "D");
        //}
    }

    private void SetPOVendorVATReference(int vendorId)
    {
        var masVatCodeBAL = new MAS_VatCodeBAL();
        var masVatCodeBE = new MAS_VatCodeBE();
        masVatCodeBE.Action = "GetVendorVatCodeByVendorId";
        masVatCodeBE.Vendor = new MAS_VendorBE();
        masVatCodeBE.Vendor.VendorID = vendorId;
        var vendorVatCodeDetails = masVatCodeBAL.GetVendorVatCodeByVendorIdBAL(masVatCodeBE);
        if (vendorVatCodeDetails.Vendor != null)
            txtVendVendorVATReference.Text = vendorVatCodeDetails.Vendor.VatCode;
        else
            txtVendVendorVATReference.Text = string.Empty;
    }

    private void SetVendODVATReference(int siteId)
    {
        var masVatCodeBAL = new MAS_VatCodeBAL();
        var masVatCodeBE = new MAS_VatCodeBE();
        masVatCodeBE.Action = "ShowAllODVatCode";
        masVatCodeBE.SiteId = siteId;

        var lstGetODVatCode = masVatCodeBAL.GetODVatCodeBAL(masVatCodeBE);
        if (lstGetODVatCode != null && lstGetODVatCode.Count > 0)
        {
            lblOfficeDepotVATReferenceValue.Text = lstGetODVatCode[0].VatCode;
            ViewState["ODVatCodeID"] = lstGetODVatCode[0].ODVatCodeID;
            //FillControls.FillDropDown(ref ddlVendOfficeDepotVATReference, lstGetODVatCode, "VatCode", "ODVatCodeID", "Select");
        }
    }


    #endregion
}
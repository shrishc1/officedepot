﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucAPCommunication.ascx.cs"
    Inherits="ModuleUI_Discrepancy_UserControl_ucAPCommunication" %>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<table width="100%" cellspacing="5" cellpadding="0" class="form-table">
    <tr>
        <td>
            <cc1:ucPanel ID="pnlGIN001" runat="server" Width="70%" CssClass="fieldset-form">
                <table class="form-table" cellpadding="5" cellspacing="0">
                    <tr>
                        <td colspan="2" style="font-weight: bold; background-color: Yellow">
                            <cc1:ucLabel ID="lblACP001a" runat="server" Text="A Discrepancy has been raised which requires for debit to be raised OR credit to be issued.Please action accordingly upon receipt of invoice. "></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <cc1:ucRadioButtonList ID="rdoDebitCredit" runat="server" RepeatDirection="Horizontal"
                                Width="60%">
                                <asp:ListItem Text="Credit"></asp:ListItem>
                                <asp:ListItem Text="Debit" Selected="True"></asp:ListItem>
                            </cc1:ucRadioButtonList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucLabel ID="lblCreditNoteNumber" runat="server" Text="Please Enter Credit Note Number"></cc1:ucLabel>
                        </td>
                        <td>
                            <cc1:ucTextbox ID="txtCreditNoteNumber" runat="server"></cc1:ucTextbox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucLabel ID="lblValueexpected" runat="server" Text="Value expected to be credited"></cc1:ucLabel>
                        </td>
                        <td>
                            <cc1:ucTextbox ID="txtValueexpected" runat="server" Text="xxxxxxx" ReadOnly="true"
                                Enabled="false"></cc1:ucTextbox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucLabel ID="lblValueActual" runat="server" Text="Actual Value to be credited"></cc1:ucLabel>
                        </td>
                        <td>
                            <cc1:ucTextbox ID="txtValueActual" runat="server"></cc1:ucTextbox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <cc1:ucLabel ID="lblComment" runat="server" Text="Please Enter Comments"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <cc1:ucTextbox ID="txtComment" runat="server" onkeyup="checkTextLengthOnKeyUp(this,1000);" TextMode="MultiLine" Width="700px"
                                Height="100px"></cc1:ucTextbox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucButton ID="btnSave" runat="server" Text="Save" class="button" />
                        </td>
                        <td>
                            <cc1:ucCheckbox ID="chkStageComplete" runat="server" /><cc1:ucLabel ID="lblStageComplete"
                                runat="server" Text="Stage Complete"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            If you required to tag a comment without choosing the discrepancy<br />
                            1- Deselect "Stage Complete"<br />
                            2- Enter the Comment<br />
                            3- Save
                        </td>
                    </tr>
                </table>
            </cc1:ucPanel>
            <cc1:ucPanel ID="pnlACP001b" runat="server" Width="70%" CssClass="fieldset-form">
                <table class="form-table" cellpadding="5" cellspacing="0">
                    <tr>
                        <td colspan="2" style="font-weight: bold; background-color: Yellow">
                            <cc1:ucLabel ID="lblACP001b" runat="server" Text="A Quality Variance exists,vendor "></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <cc1:ucRadioButtonList ID="rdoDebitCredit1" runat="server" RepeatDirection="Horizontal"
                                Width="60%">
                                <asp:ListItem Text="Credit"></asp:ListItem>
                                <asp:ListItem Text="Debit"></asp:ListItem>
                            </cc1:ucRadioButtonList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucLabel ID="lblDebitNote1" runat="server" Text="Please Enter Debit Note Number"></cc1:ucLabel>
                        </td>
                        <td>
                            <cc1:ucTextbox ID="txtDebitNote1" runat="server"></cc1:ucTextbox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucLabel ID="lblValueexpected1" runat="server" Text="Value expected to be Debited"></cc1:ucLabel>
                        </td>
                        <td>
                            <cc1:ucTextbox ID="txtValueexpected1" runat="server" Text="xxxxxxx" ReadOnly="true"
                                Enabled="false"></cc1:ucTextbox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucLabel ID="lblValueActual1" runat="server" Text="Actual Value to be Debited"></cc1:ucLabel>
                        </td>
                        <td>
                            <cc1:ucTextbox ID="txtValueActual1" runat="server"></cc1:ucTextbox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <cc1:ucLabel ID="lblComments1" runat="server" Text="Please Enter Comments"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <cc1:ucTextbox ID="UcTextbox4" runat="server" onkeyup="checkTextLengthOnKeyUp(this,1000);" TextMode="MultiLine" Width="700px"
                                Height="100px"></cc1:ucTextbox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucButton ID="btnSave1" runat="server" Text="Save" class="button" />
                        </td>
                        <td>
                            <cc1:ucCheckbox ID="UcCheckbox1" runat="server" /><cc1:ucLabel ID="lblStageComplete1"
                                runat="server" Text="Stage Complete"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            If you required to tag a comment without choosing the discrepancy<br />
                            1- Deselect "Stage Complete"<br />
                            2- Enter the Comment<br />
                            3- Save
                        </td>
                    </tr>
                </table>
            </cc1:ucPanel>
            <cc1:ucPanel ID="UcPanel1" runat="server" Width="70%" CssClass="fieldset-form">
                <table class="form-table" cellpadding="5" cellspacing="0">
                    <tr>
                        <td colspan="2" style="font-weight: bold; background-color: Yellow">
                            <cc1:ucLabel ID="lblACP002a" runat="server" Text="Please credit/debit the account for the amount to cover carriage costs "></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <cc1:ucRadioButtonList ID="rdoCreditDebit2" runat="server" RepeatDirection="Horizontal"
                                Width="60%">
                                <asp:ListItem Text="Credit"></asp:ListItem>
                                <asp:ListItem Text="Debit"></asp:ListItem>
                            </cc1:ucRadioButtonList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucLabel ID="lblCreditNote2" runat="server" Text="Please Enter Credit Note Number"></cc1:ucLabel>
                        </td>
                        <td>
                            <cc1:ucTextbox ID="txtCreditNote2" runat="server"></cc1:ucTextbox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucLabel ID="lblCarriageCost" runat="server" Text="Carriage Cost"></cc1:ucLabel>
                        </td>
                        <td>
                            <cc1:ucTextbox ID="txtCarriageCost" runat="server" Text="xxxxxxx" ReadOnly="true"
                                Enabled="false"></cc1:ucTextbox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucLabel ID="lblValueActual2" runat="server" Text="Actual Value to be Credited"></cc1:ucLabel>
                        </td>
                        <td>
                            <cc1:ucTextbox ID="txtValueActual2" runat="server"></cc1:ucTextbox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <cc1:ucLabel ID="lblComments2" runat="server" Text="Please Enter Comments"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <cc1:ucTextbox ID="txtComments2" runat="server" onkeyup="checkTextLengthOnKeyUp(this,1000);" TextMode="MultiLine" Width="700px"
                                Height="100px"></cc1:ucTextbox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucButton ID="btnSave2" runat="server" Text="Save" class="button" />
                        </td>
                        <td>
                            <cc1:ucCheckbox ID="chkStageComplete2" runat="server" /><cc1:ucLabel ID="UcLabel6"
                                runat="server" Text="Stage Complete"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            If you required to tag a comment without choosing the discrepancy<br />
                            1- Deselect "Stage Complete"<br />
                            2- Enter the Comment<br />
                            3- Save
                        </td>
                    </tr>
                </table>
            </cc1:ucPanel>
            <cc1:ucPanel ID="pnlACP002b" runat="server" Width="70%" CssClass="fieldset-form">
                <table class="form-table" cellpadding="5" cellspacing="0">
                    <tr>
                        <td colspan="2" style="font-weight: bold; background-color: Yellow">
                            <cc1:ucLabel ID="lblACP002b" runat="server" Text="Please credit/debit the account for the amount to cover carriage costs "></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <cc1:ucRadioButtonList ID="rdoCreditDebit3" runat="server" RepeatDirection="Horizontal"
                                Width="60%">
                                <asp:ListItem Text="Credit"></asp:ListItem>
                                <asp:ListItem Text="Debit"></asp:ListItem>
                            </cc1:ucRadioButtonList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucLabel ID="lblDebitNote3" runat="server" Text="Please Enter Debit Note Number"></cc1:ucLabel>
                        </td>
                        <td>
                            <cc1:ucTextbox ID="txtDebitNote3" runat="server"></cc1:ucTextbox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucLabel ID="lblCarriageCost2" runat="server" Text="Carriage Cost"></cc1:ucLabel>
                        </td>
                        <td>
                            <cc1:ucTextbox ID="txtCarriageCost2" runat="server" Text="xxxxxxx" ReadOnly="true"
                                Enabled="false"></cc1:ucTextbox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucLabel ID="lblValueSActual3" runat="server" Text="Actual Value to be Debited"></cc1:ucLabel>
                        </td>
                        <td>
                            <cc1:ucTextbox ID="txtValueSActual3" runat="server"></cc1:ucTextbox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <cc1:ucLabel ID="lblComments3" runat="server" Text="Please Enter Comments"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <cc1:ucTextbox ID="txtComments3" runat="server" onkeyup="checkTextLengthOnKeyUp(this,1000);" TextMode="MultiLine" Width="700px"
                                Height="100px"></cc1:ucTextbox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucButton ID="btnSave3" runat="server" Text="Save" class="button" />
                        </td>
                        <td>
                            <cc1:ucCheckbox ID="chkStageComplete3" runat="server" /><cc1:ucLabel ID="UcLabel7"
                                runat="server" Text="Stage Complete"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            If you required to tag a comment without choosing the discrepancy<br />
                            1- Deselect "Stage Complete"<br />
                            2- Enter the Comment<br />
                            3- Save
                        </td>
                    </tr>
                </table>
            </cc1:ucPanel>
            <cc1:ucPanel ID="pnlACP003a" runat="server" Width="70%" CssClass="fieldset-form">
                <table class="form-table" cellpadding="5" cellspacing="0">
                    <tr>
                        <td colspan="2" style="font-weight: bold; background-color: Yellow">
                            <cc1:ucLabel ID="lblACP003a" runat="server" Text="Please credit/debit the account for the amount to cover labour costs "></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <cc1:ucRadioButtonList ID="rdoCreditDebit4" runat="server" RepeatDirection="Horizontal"
                                Width="60%">
                                <asp:ListItem Text="Credit"></asp:ListItem>
                                <asp:ListItem Text="Debit"></asp:ListItem>
                            </cc1:ucRadioButtonList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucLabel ID="lblCrerditNote4" runat="server" Text="Please Enter Crerdit Note Number"></cc1:ucLabel>
                        </td>
                        <td>
                            <cc1:ucTextbox ID="txtCrerditNote4" runat="server"></cc1:ucTextbox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucLabel ID="lblLabourCost" runat="server" Text="Labour Cost"></cc1:ucLabel>
                        </td>
                        <td>
                            <cc1:ucTextbox ID="txtLabourCost" runat="server" Text="xxxxxxx" ReadOnly="true" Enabled="false"></cc1:ucTextbox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucLabel ID="lblValueActual4" runat="server" Text="Actual Value to be Credited"></cc1:ucLabel>
                        </td>
                        <td>
                            <cc1:ucTextbox ID="txtValueActual4" runat="server"></cc1:ucTextbox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <cc1:ucLabel ID="lblComments4" runat="server" Text="Please Enter Comments"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <cc1:ucTextbox ID="txtComments4" runat="server" onkeyup="checkTextLengthOnKeyUp(this,1000);" TextMode="MultiLine" Width="700px"
                                Height="100px"></cc1:ucTextbox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucButton ID="btnSave4" runat="server" Text="Save" class="button" />
                        </td>
                        <td>
                            <cc1:ucCheckbox ID="chkStageComplete4" runat="server" /><cc1:ucLabel ID="UcLabel8"
                                runat="server" Text="Stage Complete"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            If you required to tag a comment without choosing the discrepancy<br />
                            1- Deselect "Stage Complete"<br />
                            2- Enter the Comment<br />
                            3- Save
                        </td>
                    </tr>
                </table>
            </cc1:ucPanel>
            <cc1:ucPanel ID="pnlACP003b" runat="server" Width="70%" CssClass="fieldset-form">
                <table class="form-table" cellpadding="5" cellspacing="0">
                    <tr>
                        <td colspan="2" style="font-weight: bold; background-color: Yellow">
                            <cc1:ucLabel ID="lblACP003b" runat="server" Text="Please credit/debit the account for the amount to cover labour costs "></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <cc1:ucRadioButtonList ID="rdoCreditDebit5" runat="server" RepeatDirection="Horizontal"
                                Width="60%">
                                <asp:ListItem Text="Credit"></asp:ListItem>
                                <asp:ListItem Text="Debit"></asp:ListItem>
                            </cc1:ucRadioButtonList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucLabel ID="lblDebitNote5" runat="server" Text="Please Enter Debit Note Number"></cc1:ucLabel>
                        </td>
                        <td>
                            <cc1:ucTextbox ID="txtDebitNote5" runat="server"></cc1:ucTextbox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucLabel ID="lblLabourCost1" runat="server" Text="Labour Cost"></cc1:ucLabel>
                        </td>
                        <td>
                            <cc1:ucTextbox ID="txtLabourCost1" runat="server" Text="xxxxxxx" ReadOnly="true"
                                Enabled="false"></cc1:ucTextbox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucLabel ID="lblValueActual5" runat="server" Text="Actual Value to be Debited"></cc1:ucLabel>
                        </td>
                        <td>
                            <cc1:ucTextbox ID="txtValueActual5" runat="server"></cc1:ucTextbox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <cc1:ucLabel ID="lblComments5" runat="server" Text="Please Enter Comments"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <cc1:ucTextbox ID="txtComments5" runat="server" onkeyup="checkTextLengthOnKeyUp(this,1000);" TextMode="MultiLine" Width="700px"
                                Height="100px"></cc1:ucTextbox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucButton ID="btnSave5" runat="server" Text="Save" class="button" />
                        </td>
                        <td>
                            <cc1:ucCheckbox ID="chkStageComplete5" runat="server" /><cc1:ucLabel ID="UcLabel9"
                                runat="server" Text="Stage Complete"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            If you required to tag a comment without choosing the discrepancy<br />
                            1- Deselect "Stage Complete"<br />
                            2- Enter the Comment<br />
                            3- Save
                        </td>
                    </tr>
                </table>
            </cc1:ucPanel>
        </td>
    </tr>
</table>

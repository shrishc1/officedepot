﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ACP008.aspx.cs" Inherits="APAction_ACP005" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Office Depot</title>
    <link rel="stylesheet" type="text/css" href="../../../../Css/style.css" />
    <link type="text/css" href="../../../../Css/ui-lightness/jquery-ui-1.8.5.custom.css"
        rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="../../../../Css/superfish.css" media="screen" />
    <script type="text/javascript" src="<%= ResolveClientUrl("../../../../Scripts/jquery-1.2.6.min.js")%>"></script>
    <script type="text/javascript" src="<%= ResolveClientUrl("../../../../Scripts/jquery-1.4.2.min.js")%>"></script>
    <script type="text/javascript" src="<%= ResolveClientUrl("../../../../Scripts/jquery-ui-1.8.5.custom.min.js")%>"></script>
    <script type="text/javascript" src="<%= ResolveClientUrl("../../../../Scripts/hoverIntent.js")%>"></script>
    <script type="text/javascript" src="<%= ResolveClientUrl("../../../../Scripts/superfish.js")%>"></script>
    <script type="text/javascript" src="<%= ResolveClientUrl("../../../../Scripts/ValidateInputs.js")%>"></script>
    <script type="text/javascript">

        $(document).ready(function () {
            //            $('#<%=trAccept.ClientID %>').show();
            //            $('#<%=trReject.ClientID %>').hide();
            $('#<%=btnSave.ClientID%>').click(function () {
               
                if ($('#<%=trAccept.ClientID %>').is(':visible')) {
                   
                    if ($('#<%=txtAPDocumentNumber.ClientID%>').val() == '' && $('#<%=txtComments.ClientID%>').val() == '') {
                        alert('<%=APDocumentErrorMessage %>\n<%=CommentRequired %>');
                        return false;
                    }

                    else if ($('#<%=txtAPDocumentNumber.ClientID%>').val() == '') {
                        alert('<%=APDocumentErrorMessage %>');
                        return false;
                    }
                }

                if ($('#<%=txtComments.ClientID%>').val() == '') {
                    alert('<%=CommentRequired %>');
                    return false;
                }
            });
            // $('#<%=txtComments.ClientID %>').val("");
            if ($('#<%=rdoDeliveredQty.ClientID %>').is(":checked")) {
                $('#<%=trAccept.ClientID %>').show();
                $('#<%=trReject.ClientID %>').hide();
            }
            else {
                $('#<%=trAccept.ClientID %>').hide();
                $('#<%=trReject.ClientID %>').show();
            }
            $('#<%=rdoDeliveredQty.ClientID %>').click(function () {
                $('#<%=trAccept.ClientID %>').show();
                $('#<%=trReject.ClientID %>').hide();
            });

            $('#<%=rdoVEN029Text1.ClientID %>').click(function () {
                $('#<%=trAccept.ClientID %>').hide();
                $('#<%=trReject.ClientID %>').show();
            });

        });
        function GetLocation() {
            var Location = parent.document.getElementById('ctl00_ContentPlaceHolder1_txtLocation').value;
            document.getElementById('<%=hdfLocation.ClientID%>').value = Location;
            if (checkValidationGroup("Save")) {
                document.getElementById('<%=btnSave.ClientID%>').style.visibility = "hidden";
            }
            else {
                document.getElementById('<%=btnSave.ClientID%>').style.visibility = "visible";
                return false;
            }
        }
    </script>
</head>
<body>
    <form id="form2" runat="server" method="post">
    <cc1:ucPanel ID="pnlACP007c" runat="server" Width="100%" CssClass="fieldset-form"
        GroupingText="Action Required">
        <table class="form-table" cellpadding="4" cellspacing="5" width="100%">
            <tr>
                <td style="font-weight: bold;">
                    <asp:HiddenField ID="hdfLocation" runat="server" />
                    <cc1:ucLabel ID="lblACP008Text" runat="server" CssClass="action-required-heading"
                        Text="The vendor has not responded to this discrepancy or the escalations. Please contact the vendor manually to either get a credit note for the required amount or to close the discrepancy"
                        Style="text-align: center;"></cc1:ucLabel>
                </td>
            </tr>
            <tr>
                <td>
                    <table class="form-table" cellpadding="5" cellspacing="3" width="98%">
                        <tr>
                            <td colspan="2">
                                <cc1:ucRadioButton ID="rdoVEN029Text1" runat="server" GroupName="Figure" Text="Invoice Quantity" />
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <cc1:ucRadioButton ID="rdoDeliveredQty" runat="server" GroupName="Figure" Text="Delivered Qty"
                                    Checked="true" />
                            </td>
                        </tr>
                        <tr id="trReject" runat="server">
                            <td colspan="2">
                                <cc1:ucLabel ID="lblVEN029Text3" runat="server"></cc1:ucLabel>
                                <br />
                                <br />
                                <b>
                                    <cc1:ucLabel ID="lblVEN029Text6" Text="ADD POD" runat="server"></cc1:ucLabel></b>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <asp:FileUpload ID="fileupload1" runat="server" />
                                &nbsp;&nbsp;&nbsp;<cc1:ucLabel ID="lblVEN029Text7" runat="server" Text="(An Office Depot signature with date of delivery)"></cc1:ucLabel>
                                <br />
                                <br />
                                <b>
                                    <cc1:ucLabel ID="lblVEN029Text8" runat="server" Text="ADD DELIVERY NOTES"></cc1:ucLabel></b>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <asp:FileUpload ID="fileupload2" runat="server" />
                                &nbsp;&nbsp;<cc1:ucLabel ID="lblVEN029Text9" runat="server" Text="(Advises line level detail, ie product details and quantities delivered)"></cc1:ucLabel>
                            </td>
                        </tr>
                        <tr id="trAccept" runat="server">
                            <td colspan="2">
                                <cc1:ucLabel ID="lblVEN028Text5" Text="In order for us to post payment, please attach a Credit Note using the link below and add a comment to confirm the quantities being credited. 
 Please note, the attachments must be in either jpg, bmp or pdf format." runat="server"></cc1:ucLabel>
                                <br />
                                <br />
                                <cc1:ucLabel ID="lblVEN028Text4" Text="UPLOAD CREDIT NOTE" runat="server"></cc1:ucLabel>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <asp:FileUpload ID="fileupload" runat="server" />
                                <br /><br />
                                <cc1:ucLabel ID="lblAPDocumentNumber" runat="server" Text="AP Document Number"></cc1:ucLabel>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;
                                <cc1:ucTextbox ID="txtAPDocumentNumber" runat="server" onkeyup="checkTextLengthOnKeyUp(this,1000);"
                                    Width="241px"></cc1:ucTextbox>
                            </td>
                        </tr>                        
                        <tr>
                            <td width="15%">
                                <cc1:ucLabel ID="lblComment" runat="server" Text="Please Enter Comments"></cc1:ucLabel>
                            </td>
                            <td width="85%">
                                :
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <cc1:ucTextbox ID="txtComments" runat="server" onkeyup="checkTextLengthOnKeyUp(this,1000);"
                                    TextMode="MultiLine" CssClass="textarea" Width="98%"></cc1:ucTextbox>
                             <%--   <asp:RequiredFieldValidator ID="rfvCommentRequired" runat="server" Text="Please enter comments."
                                    ControlToValidate="txtComments" Display="None" ValidationGroup="Save"></asp:RequiredFieldValidator>--%>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table cellpadding="0" cellspacing="0" class="form-table" width="98%">
                        <tr>
                            <td width="14%" style="display: none;">
                                <cc1:ucLabel ID="lblStageComplete" runat="server" Text="Stage Complete"></cc1:ucLabel>
                                <input type="hidden" runat="server" id="hdnValueCredit" value="1" />
                                <asp:ValidationSummary ID="VSSave" runat="server" ValidationGroup="Save" ShowSummary="false"
                                    ShowMessageBox="true" />
                            </td>
                            <td width="1%" style="display: none;">
                                :
                            </td>
                            <td align="left" width="65%" style="display: none;">
                                <cc1:ucCheckbox ID="chkStageComplete" runat="server" Checked="true" onclick="return StageCompleted();" />
                            </td>
                            <td width="20%" style="display: none;">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" style="display: none;">
                                <cc1:ucLabel ID="lblDiscrepancyNote" runat="server" Text="If  you required to tag a comment without closing the discrepancy&lt;br/&gt;1 -    Deselect Stage Complete&lt;br/&gt;2 -   Enter  the comment&lt;br/&gt;3 -   Save"></cc1:ucLabel>
                            </td>
                            <td align="right" valign="bottom">
                                <cc1:ucButton ID="btnSave" runat="server" Text="Save" class="button" ValidationGroup="Save"
                                    OnClick="btnSave_Click" OnClientClick="javascript:GetLocation()" />
                                <cc1:ucButton ID="btnBack" runat="server" Text="Back" class="button" OnClick="btnBack_Click" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </cc1:ucPanel>
    </form>
</body>
</html>

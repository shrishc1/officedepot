﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ACP001.aspx.cs" Inherits="APAction_ACP001" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Office Depot</title>
    <link rel="stylesheet" type="text/css" href="../../../../Css/style.css" />
    <link type="text/css" href="../../../../Css/ui-lightness/jquery-ui-1.8.5.custom.css"
        rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="../../../../Css/superfish.css" media="screen" />
    <script type="text/javascript" src="<%= ResolveClientUrl("../../../../Scripts/jquery-1.2.6.min.js")%>"></script>
    <script type="text/javascript" src="<%= ResolveClientUrl("../../../../Scripts/jquery-1.4.2.min.js")%>"></script>
    <script type="text/javascript" src="<%= ResolveClientUrl("../../../../Scripts/jquery-ui-1.8.5.custom.min.js")%>"></script>
    <script type="text/javascript" src="<%= ResolveClientUrl("../../../../Scripts/hoverIntent.js")%>"></script>
    <script type="text/javascript" src="<%= ResolveClientUrl("../../../../Scripts/superfish.js")%>"></script>
    <script type="text/javascript" src="<%= ResolveClientUrl("../../../../Scripts/ValidateInputs.js")%>"></script>
    <script type="text/javascript">

        function TogglePanelControl() {
            //Panel Controls

            var pnlCredit = document.getElementById('<%=pnlACP001c.ClientID %>');
            var pnlDebit = document.getElementById('<%=pnlACP001d.ClientID %>');
            var pnlOther = document.getElementById('<%=pnlACP001O.ClientID %>');
            //Debit Panel Controls
            var objrdoDebitCreditNote = document.getElementById('<%=rdoACP001dCredit.ClientID %>');
            var objrdoDebitDebitNote = document.getElementById('<%=rdoACP001dDebit.ClientID %>');
            var objrdoDebitOther = document.getElementById('<%=rdoOther_1.ClientID %>');

            //Credit Panel Controls
            var objrdoCreditCreditNote = document.getElementById('<%=rdoACP001cCredit.ClientID %>');
            var objrdoCreditDebitNote = document.getElementById('<%=rdoACP001cDebit.ClientID %>');
            var objrdoCreditOther = document.getElementById('<%=rdoOther_2.ClientID %>');

            //Other Panel Controls
            var objrdoOtherCreditNote = document.getElementById('<%=rdoACP001OCredit.ClientID %>');
            var objrdoOtherDebitNote = document.getElementById('<%=rdoACP001ODebit.ClientID %>');
            var objrdoOtherOther = document.getElementById('<%=rdoOther_3.ClientID %>');

            // Debit Note
            if (objrdoDebitCreditNote.checked) {
                pnlCredit.style.display = "block";
                pnlDebit.style.display = "none";
                pnlOther.style.display = "none";

                objrdoCreditCreditNote.checked = true;
            }

            else if (objrdoDebitOther.checked) {

                pnlCredit.style.display = "none";
                pnlDebit.style.display = "none";
                pnlOther.style.display = "block";

                objrdoOtherOther.checked = true;
            }
            // Credit Note

            else if (objrdoCreditDebitNote.checked) {
                pnlCredit.style.display = "none";
                pnlDebit.style.display = "block";
                pnlOther.style.display = "none";

                objrdoDebitDebitNote.checked = true;
            }
            else if (objrdoCreditOther.checked) {

                pnlCredit.style.display = "none";
                pnlDebit.style.display = "none";
                pnlOther.style.display = "block";

                objrdoOtherOther.checked = true;

            }
            // Other Note
            if (objrdoOtherCreditNote.checked) {

                pnlCredit.style.display = "block";
                pnlDebit.style.display = "none";
                pnlOther.style.display = "none";
                objrdoCreditCreditNote.checked = true;
            }
            else if (objrdoOtherDebitNote.checked) {
                pnlCredit.style.display = "none";
                pnlDebit.style.display = "block";
                pnlOther.style.display = "none";

                objrdoDebitDebitNote.checked = true;
            }

        }

        function StageCompleted() {
            var objStageCompletedDebit = document.getElementById('<%=chkDebitStageComplete.ClientID %>');
            var objDebitNoteNumberRequired = document.getElementById('<%=rfvDebitNoteNumberReq.ClientID %>');
            var objDebitActualValueRequired = document.getElementById('<%=rfvDebitActualValueReq.ClientID %>');

            var objStageCompletedCredit = document.getElementById('<%=chkCreditStageComplete.ClientID %>');
            var objCreditNoteNumberRequired = document.getElementById('<%=rfvCreditNoteNumberReq.ClientID %>');
            var objCreditActualValueRequired = document.getElementById('<%=rfvCreditActualValueReq.ClientID %>');

            if (document.getElementById('<%=hdnValueDebit.ClientID %>').value == '1') {

                ValidatorEnable(objDebitNoteNumberRequired, false);
                ValidatorEnable(objDebitActualValueRequired, false);
                objStageCompletedDebit.checked = false;
                document.getElementById('<%=hdnValueDebit.ClientID %>').value = '0';

            }
            else if (document.getElementById('<%=hdnValueDebit.ClientID %>').value == '0') {

                ValidatorEnable(objDebitNoteNumberRequired, true);
                ValidatorEnable(objDebitActualValueRequired, true);
                objStageCompletedDebit.checked = true;
                document.getElementById('<%=hdnValueDebit.ClientID %>').value = '1';
            }

            if (document.getElementById('<%=hdnValueDebit.ClientID %>').value == '1') {

                ValidatorEnable(objCreditNoteNumberRequired, false);
                ValidatorEnable(objCreditActualValueRequired, false);
                objStageCompletedCredit.checked = false;
                document.getElementById('<%=hdnValueCredit.ClientID %>').value = '0';

            }
            else if (document.getElementById('<%=hdnValueDebit.ClientID %>').value == '0') {

                ValidatorEnable(objCreditNoteNumberRequired, true);
                ValidatorEnable(objCreditActualValueRequired, true);
                objStageCompletedCredit.checked = true;
                document.getElementById('<%=hdnValueCredit.ClientID %>').value = '1';
            }


        }

    </script>
    <script type="text/javascript">
        function HideShowButton(id) {
            if (checkValidationGroup("CreditSave") || checkValidationGroup("DebitSave") {
                id.style.visibility = "hidden";
            }
            else {
                id.style.visibility = "visible";
                return false;
            }
        }
    </script>
</head>
<body>
    <form id="form2" runat="server" method="post">
    <cc1:ucPanel ID="pnlACP001c" runat="server" Width="100%" CssClass="fieldset-form"
        GroupingText="Action Required">
        <table class="form-table" cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td style="font-weight: bold;">
                    <cc1:ucLabel ID="lblACP001a" runat="server" CssClass="action-required-heading" Style="text-align: center;"
                        Text="A Discrepancy has been raised which requires for debit to be raised OR credit to be issued.Please action accordingly upon receipt of invoice. "></cc1:ucLabel>
                </td>
            </tr>
            <tr>
                <td>
                    <table class="form-table" cellpadding="0" cellspacing="0" width="98%">
                        <tr>
                            <td width="25%">
                                <cc1:ucRadioButton ID="rdoACP001cCredit" runat="server" GroupName="NoteName" OnClick="TogglePanelControl()"
                                    Text="Credit" />
                            </td>
                            <td width="25%">
                                <cc1:ucRadioButton ID="rdoACP001cDebit" runat="server" GroupName="NoteName" OnClick="TogglePanelControl()"
                                    Text="Debit" />
                            </td>
                            <td width="50%">
                                <cc1:ucRadioButton ID="rdoOther_1" runat="server" GroupName="NoteName" OnClick="TogglePanelControl()"
                                    Text="Other" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table class="form-table" cellpadding="0" cellspacing="0" width="98%">
                        <tr>
                            <td width="25%">
                                <cc1:ucLabel ID="lblCreditNoteNumberD" runat="server" Text="Please Enter Credit Note Number"></cc1:ucLabel>
                            </td>
                            <td width="1%">
                                :
                            </td>
                            <td width="74%" style="text-align: left">
                                <cc1:ucTextbox ID="txtCreditNoteNumber" runat="server" MaxLength="10"></cc1:ucTextbox>
                                <asp:RequiredFieldValidator ID="rfvCreditNoteNumberReq" runat="server" Text="Please enter Credit Note Number."
                                    ControlToValidate="txtCreditNoteNumber" Display="None" ValidationGroup="CreditSave"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr runat="server" id="trValueExpectedCredit">
                            <td>
                                <cc1:ucLabel ID="lblValueexpected" runat="server" Text="Value expected to be credited"></cc1:ucLabel>
                            </td>
                            <td>
                                :
                            </td>
                            <td>
                                <cc1:ucTextbox ID="txtCreditValueExpected" runat="server" ReadOnly="true" Enabled="false"></cc1:ucTextbox>
                                <%--<cc1:ucDropdownList ID="drpRate" runat="server" Width="60px">
                                        <asp:ListItem Text="EURO" Value="1"></asp:ListItem>
                                        <asp:ListItem Text="GBP" Value="2"></asp:ListItem>
                                    </cc1:ucDropdownList>--%>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <cc1:ucLabel ID="lblValueActual" runat="server" Text="Actual Value to be credited"></cc1:ucLabel>
                                <cc1:ucLabel ID="lblValueCredited" runat="server" Text="Value to be credited" Visible="false"></cc1:ucLabel>
                            </td>
                            <td>
                                :
                            </td>
                            <td>
                                <cc1:ucTextbox ID="txtCreditValueActual" runat="server" MaxLength="7"></cc1:ucTextbox>
                                
                                <asp:RequiredFieldValidator ID="rfvCreditActualValueReq" runat="server" Text="Please enter Actual Value to be Credited."
                                    ControlToValidate="txtCreditValueActual" Display="None" ValidationGroup="CreditSave"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="revCreditActualValueRE" ValidationExpression="^\d{0,8}(\.\d{0,2})?$"
                                    runat="server" ControlToValidate="txtCreditValueActual" Display="None" ValidationGroup="CreditSave"></asp:RegularExpressionValidator>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table class="form-table" cellpadding="0" cellspacing="0" width="98%">
                        <tr>
                            <td width="15%">
                                <cc1:ucLabel ID="lblComment" runat="server" Text="Please Enter Comments"></cc1:ucLabel>
                            </td>
                            <td width="85%">
                                :
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <cc1:ucTextbox ID="txtCreditComment" runat="server" onkeyup="checkTextLengthOnKeyUp(this,1000);"
                                    TextMode="MultiLine" CssClass="textarea" Width="98%"></cc1:ucTextbox>
                                <asp:RequiredFieldValidator ID="rfvCreditCommentRequired" runat="server" Text="Please enter comments."
                                    ControlToValidate="txtCreditComment" Display="None" ValidationGroup="CreditSave"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table cellpadding="0" cellspacing="0" class="form-table" width="98%">
                        <tr>
                            <td width="25%" style="display:none;">
                                <cc1:ucLabel ID="lblStageComplete" runat="server" Text="Stage Complete"></cc1:ucLabel>
                                <input type="hidden" runat="server" id="hdnValueCredit" value="1" />
                                <asp:ValidationSummary ID="VSCreditSave" runat="server" ValidationGroup="CreditSave"
                                    ShowSummary="false" ShowMessageBox="true" />
                            </td>
                            <td width="1%" style="display:none;">
                                :
                            </td>
                            <td align="left" width="54%" style="display:none;">
                                <cc1:ucCheckbox ID="chkCreditStageComplete" runat="server" Checked="true" onclick="return StageCompleted();" />
                            </td>
                            <td width="20%" style="display:none;">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" style="display:none;">
                                <cc1:ucLabel ID="lblDiscrepancyNote" runat="server" Text="If  you required to tag a comment without closing the discrepancy&lt;br/&gt;1 -    Deselect Stage Complete&lt;br/&gt;2 -   Enter  the comment&lt;br/&gt;3 -   Save"></cc1:ucLabel>
                            </td>
                            <td align="right" valign="bottom">
                                <cc1:ucButton ID="btnCreditSave" runat="server" Text="Save" class="button" ValidationGroup="CreditSave"
                                OnClientClick="javascript:HideShowButton(this)"
                                    OnClick="btnCreditSave_Click" /> 
                                <cc1:ucButton ID="btnBack" runat="server" Text="Back" class="button" OnClick="btnBack_Click" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </cc1:ucPanel>
    <cc1:ucPanel ID="pnlACP001d" runat="server" Width="100%" CssClass="fieldset-form"
        Style="display: none" GroupingText="Action Required">
        <table class="form-table" cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td style="font-weight: bold;">
                    <cc1:ucLabel ID="lblACP001b" runat="server" CssClass="action-required-heading" Style="text-align: center;"
                        Text="A Discrepancy has been raised which requires for debit to be raised OR credit to be issued.Please action accordingly upon receipt of invoice."></cc1:ucLabel>
                </td>
            </tr>
            <tr>
                <td>
                    <table class="form-table" cellpadding="0" cellspacing="0" width="98%">
                        <tr>
                            <td style="width: 25%">
                                <cc1:ucRadioButton ID="rdoACP001dCredit" runat="server" GroupName="NoteName" OnClick="TogglePanelControl()"
                                    Text="Credit" />
                            </td>
                            <td style="width: 25%">
                                <cc1:ucRadioButton ID="rdoACP001dDebit" runat="server" GroupName="NoteName" OnClick="TogglePanelControl()"
                                    Text="Debit" />
                            </td>
                             <td width="50%">
                                <cc1:ucRadioButton ID="rdoOther_2" runat="server" GroupName="NoteName" OnClick="TogglePanelControl()"
                                    Text="Other" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table class="form-table" cellpadding="0" cellspacing="0" width="98%">
                        <tr>
                            <td width="25%">
                                <cc1:ucLabel ID="lblDebitNote" runat="server" Text="Please Enter Debit Note Number"></cc1:ucLabel>
                            </td>
                            <td width="1%">
                                :
                            </td>
                            <td width="74%">
                                <cc1:ucTextbox ID="txtDebitNoteNumber" runat="server" MaxLength="10"></cc1:ucTextbox>
                                <asp:RequiredFieldValidator ID="rfvDebitNoteNumberReq" runat="server" Text="Please enter Debit Note Number."
                                    ControlToValidate="txtDebitNoteNumber" Display="None" ValidationGroup="DebitSave"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr runat="server" id="trValueExpectedDebit">
                            <td>
                                <cc1:ucLabel ID="lblValueexpected1" runat="server" Text="Value expected to be Debited"></cc1:ucLabel>
                            </td>
                            <td>
                                :
                            </td>
                            <td>
                                <cc1:ucTextbox ID="txtDebitValueExpected" runat="server" ReadOnly="true" Enabled="false"></cc1:ucTextbox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <cc1:ucLabel ID="lblValueActual1" runat="server" Text="Actual Value to be Debited"></cc1:ucLabel>
                                <cc1:ucLabel ID="lblValueDebited" runat="server" Text="Value to be debited" Visible="false"></cc1:ucLabel>
                            </td>
                            <td>
                                :
                            </td>
                            <td>
                                <cc1:ucTextbox ID="txtDebitValueActual" runat="server" MaxLength="7"></cc1:ucTextbox>
                                <asp:RequiredFieldValidator ID="rfvDebitActualValueReq" runat="server" Text="Please enter actual value to be debited."
                                    ControlToValidate="txtDebitValueActual" Display="None" ValidationGroup="DebitSave"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="revDebitActualValueRE" ValidationExpression="^\d{0,8}(\.\d{0,2})?$"
                                    runat="server" ControlToValidate="txtDebitValueActual" Display="None" ValidationGroup="DebitSave"></asp:RegularExpressionValidator>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table class="form-table" cellpadding="0" cellspacing="0" width="98%">
                        <tr>
                            <td width="15%">
                                <cc1:ucLabel ID="lblComments1" runat="server" Text="Please Enter Comments"></cc1:ucLabel>
                            </td>
                            <td width="85%">
                                :
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <cc1:ucTextbox ID="txtDebitComment" runat="server" onkeyup="checkTextLengthOnKeyUp(this,1000);"
                                    TextMode="MultiLine" CssClass="textarea" Width="98%"></cc1:ucTextbox>
                                <asp:RequiredFieldValidator ID="rfvDebitCommentReq" runat="server" Text="Please enter Comments."
                                    ControlToValidate="txtDebitComment" Display="None" ValidationGroup="DebitSave"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table class="form-table" cellpadding="0" cellspacing="0" width="98%">
                        <tr>
                            <td width="25%" style="display:none;">
                                <cc1:ucLabel ID="lblStageComplete1" runat="server" Text="Stage Completed"></cc1:ucLabel>
                                <asp:ValidationSummary ID="VSDebitSave" runat="server" ValidationGroup="DebitSave"
                                    ShowSummary="false" ShowMessageBox="true" />
                            </td>
                            <td width="1%" style="display:none;">
                                :
                            </td>
                            <td width="54%" align="left" style="display:none;">
                                <cc1:ucCheckbox ID="chkDebitStageComplete" runat="server" Checked="true" onclick="return StageCompleted();" />
                            </td>
                            <td width="20%" style="display:none;">
                                <input type="hidden" runat="server" id="hdnValueDebit" value="1" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" style="display:none;">
                                <cc1:ucLabel ID="lblDiscrepancyNote_1" runat="server" Text="If  you required to tag a comment without closing the discrepancy<br/>1 -    Deselect Stage Complete<br/>2 -   Enter  the comment<br/>3 -   Save"></cc1:ucLabel>
                            </td>
                            <td align="right" valign="bottom">
                                <cc1:ucButton ID="btnSave_1" runat="server" Text="Save" class="button" ValidationGroup="DebitSave"
                                  OnClientClick="javascript:HideShowButton(this)"
                                    OnClick="btnDebitSave1_Click" />
                                     <cc1:ucButton ID="btnback_2" runat="server" Text="Back" class="button" OnClick="btnBack_Click" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </cc1:ucPanel>
    <cc1:ucPanel ID="pnlACP001O" runat="server" Width="100%" CssClass="fieldset-form"
        Style="display: none" GroupingText="Action Required">
        <table class="form-table" cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td style="font-weight: bold;">
                    <cc1:ucLabel ID="lblACP001a_2" runat="server" CssClass="action-required-heading"
                        Style="text-align: center;" Text="A Discrepancy has been raised which requires for debit to be raised OR credit to be issued.Please action accordingly upon receipt of invoice. "></cc1:ucLabel>
                </td>
            </tr>
            <tr>
                <td>
                    <table class="form-table" cellpadding="0" cellspacing="0" width="98%">
                        <tr>
                            <td width="25%">
                                <cc1:ucRadioButton ID="rdoACP001OCredit" runat="server" GroupName="NoteName" OnClick="TogglePanelControl()"
                                    Text="Credit" />
                            </td>
                            <td width="25%">
                                <cc1:ucRadioButton ID="rdoACP001ODebit" runat="server" GroupName="NoteName" OnClick="TogglePanelControl()"
                                    Text="Debit" />
                            </td>
                            <td width="50%">
                                <cc1:ucRadioButton ID="rdoOther_3" runat="server" GroupName="NoteName" OnClick="TogglePanelControl()"
                                    Text="Other" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table class="form-table" cellpadding="0" cellspacing="0" width="95%">
                        <tr>
                            <td width="45%">
                                <cc1:ucLabel ID="lblCommentsforNodebitCredit" runat="server"></cc1:ucLabel>
                            </td>
                            <td width="55%">
                                :
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <cc1:ucTextbox ID="txtOtherComments" runat="server" onkeyup="checkTextLengthOnKeyUp(this,1000);"
                                    TextMode="MultiLine" CssClass="textarea" Width="98%"></cc1:ucTextbox>
                                <asp:RequiredFieldValidator ID="rfvComment" runat="server" Text="Please enter comments."
                                    ControlToValidate="txtOtherComments" Display="None" ValidationGroup="OtherSave"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table cellpadding="0" cellspacing="0" class="form-table" width="98%">
                        <tr>
                            <td width="25%" style="display:none;">
                                <cc1:ucLabel ID="lblStageComplete_1" runat="server" Text="Stage Complete"></cc1:ucLabel>
                                <input type="hidden" runat="server" id="Hidden1" value="1" />
                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="OtherSave"
                                    ShowSummary="false" ShowMessageBox="true" />
                            </td>
                            <td width="1%" style="display:none;">
                                :
                            </td>
                            <td align="left" width="54%" style="display:none;">
                                <cc1:ucCheckbox ID="chkOtherStageComplete" runat="server" Checked="true" />
                            </td>
                            <td width="20%" style="display:none;">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" style="display:none;">
                                <cc1:ucLabel ID="lblDiscrepancyNote_O" runat="server" Text="If  you required to tag a comment without closing the discrepancy&lt;br/&gt;1 -    Deselect Stage Complete&lt;br/&gt;2 -   Enter  the comment&lt;br/&gt;3 -   Save"></cc1:ucLabel>
                            </td>
                            <td align="right" valign="bottom">
                                <cc1:ucButton ID="btnOtherSave" runat="server" Text="Save" class="button" ValidationGroup="OtherSave"
                                    OnClick="btnOtherSave_Click" />
                                <cc1:ucButton ID="btnBack_O" runat="server" Text="Back" class="button" OnClick="btnBack_Click" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </cc1:ucPanel>
    </form>
</body>
</html>

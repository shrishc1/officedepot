﻿using System;
using System.Collections.Generic;
using System.Data;
using BaseControlLibrary;
using BusinessEntities.ModuleBE.AdminFunctions;
using BusinessEntities.ModuleBE.Discrepancy.APAction;
using BusinessEntities.ModuleBE.Discrepancy.LogDiscrepancy;
using BusinessEntities.ModuleBE.StockOverview;
using BusinessLogicLayer.ModuleBAL.AdminFunctions;
using BusinessLogicLayer.ModuleBAL.Discrepancy.CountrySetting;
using BusinessLogicLayer.ModuleBAL.Discrepancy.LogDiscrepancy;
using BusinessLogicLayer.ModuleBAL.StockOverview;
using WebUtilities;
using BusinessLogicLayer.ModuleBAL.Appointment.Booking;
using BusinessEntities.ModuleBE.Languages.Languages;
using System.IO;
using Utilities;
using System.Configuration;


public partial class APAction_ACP005 : CommonPage
{

    int Siteid = 0;
    int Vendorid = 0;
    string EmailList = null;
    System.Text.StringBuilder sSendMailLink = new System.Text.StringBuilder();
    string sentToLinks = string.Empty;
    protected string CommentRequired = "-" + WebCommon.getGlobalResourceValue("CommentRequired");
    protected string APDocumentErrorMessage = "-" + WebCommon.getGlobalResourceValue("APDocumentErrorMessage");
    protected string AmountDebitErrorMessage = "-" + WebCommon.getGlobalResourceValue("AmountDebitErrorMessage");

    protected string PleaseentertheVendorVATReference = "-" + WebCommon.getGlobalResourceValue("PleaseentertheVendorVATReference");
    protected string ReasonMandatry = "-" + WebCommon.getGlobalResourceValue("ReasonMandatry");
    protected string CurrencyMandatry = "-" + WebCommon.getGlobalResourceValue("CurrencyMandatry");
    protected string DebitRaisedSuccessfully = WebCommon.getGlobalResourceValue("DebitRaisedSuccessfully");
    string DebitNoteAgainstDiscEmailSubject = WebCommon.getGlobalResourceValue("DebitNoteAgainstDiscEmailSubject");
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindCurrency();          
            DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
            DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();
            oDiscrepancyBE.Action = "GetCurrencyByVendorID";
            if (GetQueryStringValue("disLogID") != null)
            {
                oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID"));
            }
            ddlCurrencyDebit.SelectedValue = oDiscrepancyBAL.GetCurrencyBAL(oDiscrepancyBE);   
            //  BindDebitReason();
            GetCarriageCostForIncorrectAddress();
            DiscrepancyBE oNewDiscrepancyBE = new DiscrepancyBE();
            DiscrepancyBAL oNewDiscrepancyBAL = new DiscrepancyBAL();
            oNewDiscrepancyBE.Action = "GetExpectedDebitedAmount";
            oNewDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
            DataSet ds = oNewDiscrepancyBAL.GetInvoiceDiscrepancyCommentsBAL(oNewDiscrepancyBE);
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (!string.IsNullOrEmpty(ds.Tables[0].Rows[0]["Amount"].ToString()))
                {
                    txtExpectedDebit.Text = ds.Tables[0].Rows[0]["Amount"].ToString();
                }
            }
        }

    }


    private void BindCurrency()
    {
        var currencyBAL = new MAS_CurrencyBAL();
        var currencyBE = new CurrencyBE();
        currencyBE.Action = "ShowAll";
        var lstCurrency = currencyBAL.GetCurrenyBAL(currencyBE);
        if (lstCurrency != null && lstCurrency.Count > 0)
        {
            FillControls.FillDropDown(ref ddlCurrencyDebit, lstCurrency, "CurrencyName", "CurrencyID");

        }
    }

    private void GetCarriageCostForIncorrectAddress()
    {
        if (GetQueryStringValue("disLogID") != null)
        {

            DiscrepancyBE oNewDiscrepancyBE = new DiscrepancyBE();
            DiscrepancyBAL oNewDiscrepancyBAL = new DiscrepancyBAL();
            oNewDiscrepancyBE.Action = "GetDiscrepancyLog";
            oNewDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
            List<DiscrepancyBE> lstDisLog = oNewDiscrepancyBAL.GetDiscrepancyLogDetailsBAL(oNewDiscrepancyBE);
            if (lstDisLog != null && lstDisLog.Count > 0)
            {
                //if (GetQueryStringValue("FromPage") != null && GetQueryStringValue("FromPage").Trim().ToLower().Equals("incorrectaddress"))
                //{
                //    txtCreditCarriageCost.Text = lstDisLog[0].Freight_Charges.ToString();
                //    txtDebitCarriageCost.Text = lstDisLog[0].Freight_Charges.ToString();
                //}

                if (lstDisLog[0].VendorID != null)
                {
                    ucTextbox ucVendorEmailList = (ucTextbox)ucSDRCommunication1.FindControl("ucVendorEmailList");
                    ucVendorEmailList.Text = ucSDRCommunication1.FillAPContacts(Convert.ToInt32(lstDisLog[0].VendorID));
                }
                ViewState["PONumber"] = lstDisLog[0].PurchaseOrderNumber;
                ViewState["SiteID"] = lstDisLog[0].Site.SiteID;
                ViewState["VendorID"] = lstDisLog[0].VendorID;
            }
        }
    }
    //private void BindDebitReason()
    //{
    //    var debitReasonTypeBAL = new DIS_DebitReasonTypeBAL();
    //    var apActionBE = new APActionBE();
    //    apActionBE.Action = "GetReasonTypeSetUp";
    //    apActionBE.ReasonType = "DR"; /* Debit Reason */
    //    var lstAPActionBE = debitReasonTypeBAL.GetReasonTypeBAL(apActionBE);
    //    if (lstAPActionBE != null && lstAPActionBE.Count > 0)
    //    {
    //        FillControls.FillDropDown(ref ddlReasonDebit, lstAPActionBE, "Reason", "ReasonTypeID", "Select");
    //    }
    //}


    #region Events
    protected void btnBack_Click(object sender, EventArgs e)
    {
        //Redirecting to Previous Page
        RedirectToPage(GetQueryStringValue("PreviousPage"));
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        Save();

        //Redirecting to Previous Page
        RedirectToPage(GetQueryStringValue("PreviousPage"));
    }



    #endregion

    #region

    private void Save()
    {

        DISLog_WorkFlowBE oDISLog_WorkFlowBE = new DISLog_WorkFlowBE();
        DISLog_WorkFlowBAL oDISLog_WorkFlowBAL = new DISLog_WorkFlowBAL();
        if (GetQueryStringValue("disLogID") != null && GetQueryStringValue("disLogID").ToString() != "")
        {
            //**********************************************************
            string Location = hdfLocation.Value;
            DiscrepancyBE oDiscrepancyBE1 = new DiscrepancyBE();
            DiscrepancyBAL oDiscrepancyBAL1 = new DiscrepancyBAL();
            oDiscrepancyBE1.Action = "UpdateLocation";
            oDiscrepancyBE1.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
            oDiscrepancyBE1.Location = Location;
            oDiscrepancyBE1.UserID = Convert.ToInt32(Session["UserID"]);
            string sResult1 = oDiscrepancyBAL1.updateDiscrepancyLocationBAL(oDiscrepancyBE1);


            var discripancyLogId = Convert.ToInt32(GetQueryStringValue("disLogID"));
            var discrepancyBE = new DiscrepancyBE();
            var discrepancyBAL = new DiscrepancyBAL();

            #region Inserting data Debit raise tables ...
            discrepancyBE.Action = "AddDebitRaise";
            discrepancyBE.DebitRaseType = "D";

            if (ViewState["PONumber"] != null)
                discrepancyBE.PurchaseOrderNumber = Convert.ToString(ViewState["PONumber"]);

            if (ViewState["SiteID"] != null)
                discrepancyBE.SiteID = Convert.ToInt32(ViewState["SiteID"]);

            if (ViewState["VendorID"] != null)
                discrepancyBE.VendorID = Convert.ToInt32(ViewState["VendorID"]);

            discrepancyBE.InvoiceNo = string.Empty;
            discrepancyBE.ReferenceNo = string.Empty;

            discrepancyBE.VendorVatReference = txtVendVendorVATReference.Text;
            discrepancyBE.DebitTotalValue = Convert.ToDecimal(txtAmountDebit.Text);
            if (ViewState["ODVatCodeID"] != null)
                discrepancyBE.ODVatReferenceID = Convert.ToInt32(ViewState["ODVatCodeID"]);

            var emailIds = string.Empty;
            var txtCommEmailList = (ucTextbox)ucSDRCommunication1.FindControl("ucVendorEmailList");
            var txtAdditionalEmailList = (ucTextbox)ucSDRCommunication1.FindControl("txtAltEmail");
            //  var emailIds = string.Empty;
            if (txtCommEmailList != null && txtAdditionalEmailList != null)
            {
                if ((!string.IsNullOrEmpty(txtCommEmailList.Text) && !string.IsNullOrWhiteSpace(txtCommEmailList.Text)) &&
                    (!string.IsNullOrEmpty(txtAdditionalEmailList.Text) && !string.IsNullOrWhiteSpace(txtAdditionalEmailList.Text)))
                {
                    emailIds = string.Format("{0},{1}", txtCommEmailList.Text, txtAdditionalEmailList.Text);
                }
                else if ((!string.IsNullOrEmpty(txtCommEmailList.Text) && !string.IsNullOrWhiteSpace(txtCommEmailList.Text)))
                {
                    emailIds = txtCommEmailList.Text;
                }
                else if ((!string.IsNullOrEmpty(txtAdditionalEmailList.Text) && !string.IsNullOrWhiteSpace(txtAdditionalEmailList.Text)))
                {
                    emailIds = txtAdditionalEmailList.Text;
                }
            }

            discrepancyBE.CommunicationEmails = emailIds;
            discrepancyBE.InternalComments = txtComment.Text;

            //if (ddlCurrencyDebit.SelectedIndex > 0)
            //{
                discrepancyBE.Currency = new CurrencyBE();
                discrepancyBE.Currency.CurrencyId = Convert.ToInt32(ddlCurrencyDebit.SelectedValue);
           // }

            discrepancyBE.DiscrepancyLogID = discripancyLogId;
            discrepancyBE.Status = "A"; // A is an Active status of Debit Raise.
            discrepancyBE.UserID = Convert.ToInt32(Session["UserID"]);
            discrepancyBE.Country = new MAS_CountryBE();

            //if (ucCountry.innerControlddlCountry.Items.Count > 0)
            //    discrepancyBE.Country.CountryID = Convert.ToInt32(ucCountry.innerControlddlCountry.SelectedValue);

            discrepancyBE.APActionRef = "ACP002";

            var debitRaiseId = discrepancyBAL.addDebitRaiseBAL(discrepancyBE);
            if (debitRaiseId > 0)
            {
                var discrepancy = new DiscrepancyBE();
                discrepancy.Action = "AddDebitRaiseItem";
                discrepancy.DebitRaiseId = debitRaiseId;
                discrepancy.StateValueDebited = Convert.ToDecimal(txtAmountDebit.Text);

                //if (ddlReasonDebit.SelectedIndex > 0)
                //    discrepancy.DebitReasonId = Convert.ToInt32(ddlReasonDebit.SelectedValue);

                discrepancyBAL.addDebitRaiseItemBAL(discrepancy);
            }

            #endregion


            #region Getting here Saved Debit Raise data & Workflow entry ...
            if (debitRaiseId > 0)
            {
                discrepancyBE = new DiscrepancyBE();
                //var discrepancyBAL = new DiscrepancyBAL();
                discrepancyBE.Action = "GetAllDebitRaise";
                discrepancyBE.DebitRaiseId = debitRaiseId;
                var findDiscrepancy = discrepancyBAL.GetAllDebitRaiseBAL(discrepancyBE).Find(x => x.APActionRef.Equals("ACP002"));
                if (findDiscrepancy != null)
                {
                    ViewState["DebitNoteNo"] = findDiscrepancy.DebitNoteNo;
                    DebitRaisedSuccessfully = DebitRaisedSuccessfully.Replace("##RaisedDebitNumber##", findDiscrepancy.DebitNoteNo);
                    DebitNoteAgainstDiscEmailSubject = DebitNoteAgainstDiscEmailSubject.Replace("##DebitNoteNumber##", findDiscrepancy.DebitNoteNo);
                    #region Logic to Save & Send email ...
                    var oAPPBOK_CommunicationBAL = new APPBOK_CommunicationBAL();
                    var oLanguages = oAPPBOK_CommunicationBAL.GetLanguages();
                    foreach (MAS_LanguageBE objLanguage in oLanguages)
                    {
                        bool MailSentInLanguage = false;
                        if (objLanguage.LanguageID.Equals(findDiscrepancy.Vendor.LanguageID))
                            MailSentInLanguage = true;

                        var path = AppDomain.CurrentDomain.BaseDirectory.ToLower();
                        //var templatesPath = @"emailtemplates/communication1";
                        var templateFile = string.Format(@"{0}emailtemplates/communication1/DebitNoteAgainstDiscrepancy.english.htm", path);

                        #region Setting reason as per the language ...
                        switch (objLanguage.LanguageID)
                        {
                            case 1:
                                Page.UICulture = clsConstants.EnglishISO; //"en-US"; // English
                                break;
                            case 2:
                                Page.UICulture = clsConstants.FranceISO; //"fr"; // France
                                break;
                            case 3:
                                Page.UICulture = clsConstants.GermanyISO; //"de"; // German
                                break;
                            case 4:
                                Page.UICulture = clsConstants.NederlandISO; //"nl"; // Dutch
                                break;
                            case 5:
                                Page.UICulture = clsConstants.SpainISO; //"es"; // Spanish
                                break;
                            case 6:
                                Page.UICulture = clsConstants.ItalyISO; //"it"; // Italy
                                break;
                            case 7:
                                Page.UICulture = clsConstants.CzechISO; //"cz"; // Czech
                                break;
                            default:
                                Page.UICulture = clsConstants.EnglishISO; // "en-US"; // English
                                break;
                        }

                        #endregion

                        #region  Prepairing html body format ...
                        string htmlBody = null;
                        using (StreamReader sReader = new StreamReader(templateFile.ToLower()))
                        {
                            htmlBody = sReader.ReadToEnd();
                            //htmlBody = htmlBody.Replace("{logoInnerPath}", getAbsolutePath());

                            htmlBody = htmlBody.Replace("{VendorNumber}", findDiscrepancy.Vendor.Vendor_No);
                            htmlBody = htmlBody.Replace("{DebitNoteNumber}", WebCommon.getGlobalResourceValue("DebitNoteNumber"));
                            htmlBody = htmlBody.Replace("{DebitNoteNumberValue}", findDiscrepancy.DebitNoteNo);
                            htmlBody = htmlBody.Replace("{VendorName}", findDiscrepancy.Vendor.VendorName);
                            htmlBody = htmlBody.Replace("{OurVATReference}", WebCommon.getGlobalResourceValue("OurVATReference"));

                            if (!string.IsNullOrEmpty(findDiscrepancy.OurVATReference) && !string.IsNullOrWhiteSpace(findDiscrepancy.OurVATReference))
                                htmlBody = htmlBody.Replace("{OurVATReferenceValue}", findDiscrepancy.OurVATReference);
                            else
                                htmlBody = htmlBody.Replace("{OurVATReferenceValue}", string.Empty);

                            htmlBody = htmlBody.Replace("{VendorAddress1}", findDiscrepancy.Vendor.address1);
                            htmlBody = htmlBody.Replace("{YourVATReference}", WebCommon.getGlobalResourceValue("YourVATReference"));

                            if (!string.IsNullOrEmpty(findDiscrepancy.YourVATReference) && !string.IsNullOrWhiteSpace(findDiscrepancy.YourVATReference))
                                htmlBody = htmlBody.Replace("{YourVATReferenceValue}", findDiscrepancy.YourVATReference);
                            else
                                htmlBody = htmlBody.Replace("{YourVATReferenceValue}", string.Empty);

                            htmlBody = htmlBody.Replace("{VendorAddress2}", findDiscrepancy.Vendor.address2);
                            htmlBody = htmlBody.Replace("{DebitReason}", WebCommon.getGlobalResourceValue("DebitReason"));
                            htmlBody = htmlBody.Replace("{DebitReasonValue}", findDiscrepancy.Reason);

                            if (!string.IsNullOrEmpty(findDiscrepancy.Vendor.VMPPIN) && !string.IsNullOrWhiteSpace(findDiscrepancy.Vendor.VMPPIN))
                                htmlBody = htmlBody.Replace("{VMPPIN}", findDiscrepancy.Vendor.VMPPIN);
                            else
                                htmlBody = htmlBody.Replace("{VMPPIN}", string.Empty);

                            if (!string.IsNullOrEmpty(findDiscrepancy.Vendor.VMPPOU) && !string.IsNullOrWhiteSpace(findDiscrepancy.Vendor.VMPPOU))
                                htmlBody = htmlBody.Replace("{VMPPOU}", findDiscrepancy.Vendor.VMPPOU);
                            else
                                htmlBody = htmlBody.Replace("{VMPPOU}", string.Empty);

                            if (!string.IsNullOrEmpty(findDiscrepancy.Vendor.city) && !string.IsNullOrWhiteSpace(findDiscrepancy.Vendor.city))
                                htmlBody = htmlBody.Replace("{VendorCity}", findDiscrepancy.Vendor.city);
                            else
                                htmlBody = htmlBody.Replace("{VendorCity}", string.Empty);

                            htmlBody = htmlBody.Replace("{DearSirMadam}", WebCommon.getGlobalResourceValue("DearSirMadam"));
                            htmlBody = htmlBody.Replace("{Date}", WebCommon.getGlobalResourceValue("Date"));

                            var debitRaiseDate = Convert.ToDateTime(findDiscrepancy.DebitRaiseDate);
                            htmlBody = htmlBody.Replace("{DateValue}", debitRaiseDate.ToString("dd/MM/yyyy"));

                            htmlBody = htmlBody.Replace("{DebitLetterMessage1}", WebCommon.getGlobalResourceValue("DebitLetterMessage1"));
                            htmlBody = htmlBody.Replace("{VDRNo}", WebCommon.getGlobalResourceValue("VDRNoKey"));
                            htmlBody = htmlBody.Replace("{VDRNoValue}", findDiscrepancy.VDRNo);
                            htmlBody = htmlBody.Replace("{DebitLetterMessage2}", WebCommon.getGlobalResourceValue("DebitLetterMessage2"));

                            var vdrDateValue = Convert.ToDateTime(findDiscrepancy.DiscrepancyLogDate);
                            htmlBody = htmlBody.Replace("{VDRDateValue}", vdrDateValue.ToString("dd/MM/yyyy"));

                            htmlBody = htmlBody.Replace("{DebitLetterMessage3}", WebCommon.getGlobalResourceValue("DebitLetterMessage3"));
                            htmlBody = htmlBody.Replace("{DebitValue}", Convert.ToString(findDiscrepancy.StateValueDebited));
                            htmlBody = htmlBody.Replace("{DebitCurrencyValue}", Convert.ToString(findDiscrepancy.Currency.CurrencyName));
                            htmlBody = htmlBody.Replace("{DebitLetterMessage4}", WebCommon.getGlobalResourceValue("DebitLetterMessage4"));
                            htmlBody = htmlBody.Replace("{DebiNoteComments}", findDiscrepancy.Comment);
                            htmlBody = htmlBody.Replace("{DebitLetterMessage5}", WebCommon.getGlobalResourceValue("DebitLetterMessage5"));
                            htmlBody = htmlBody.Replace("{Yoursfaithfully}", WebCommon.getGlobalResourceValue("Yoursfaithfully"));
                            htmlBody = htmlBody.Replace("{APClerkName}", findDiscrepancy.CreatedBy);

                            if (!string.IsNullOrEmpty(findDiscrepancy.CreatedByContactNumber) && !string.IsNullOrWhiteSpace(findDiscrepancy.CreatedByContactNumber))
                                htmlBody = htmlBody.Replace("{APClerkContactNo}", findDiscrepancy.CreatedByContactNumber);
                            else
                                htmlBody = htmlBody.Replace("{APClerkContactNo}", string.Empty);

                            var apAddress = string.Empty;
                            if (!string.IsNullOrEmpty(findDiscrepancy.APAddress1) && !string.IsNullOrEmpty(findDiscrepancy.APAddress2)
                                && !string.IsNullOrEmpty(findDiscrepancy.APAddress3) && !string.IsNullOrEmpty(findDiscrepancy.APAddress4)
                                && !string.IsNullOrEmpty(findDiscrepancy.APAddress5) && !string.IsNullOrEmpty(findDiscrepancy.APAddress6))
                            {
                                apAddress = string.Format("{0},&nbsp;{1},&nbsp;{2},&nbsp;{3},&nbsp;{4},&nbsp;{5}", findDiscrepancy.APAddress1, findDiscrepancy.APAddress2,
                                    findDiscrepancy.APAddress3, findDiscrepancy.APAddress4, findDiscrepancy.APAddress5, findDiscrepancy.APAddress6);
                            }
                            else if (!string.IsNullOrEmpty(findDiscrepancy.APAddress1) && !string.IsNullOrEmpty(findDiscrepancy.APAddress2)
                                && !string.IsNullOrEmpty(findDiscrepancy.APAddress3) && !string.IsNullOrEmpty(findDiscrepancy.APAddress4)
                                && !string.IsNullOrEmpty(findDiscrepancy.APAddress5))
                            {
                                apAddress = string.Format("{0},&nbsp;{1},&nbsp;{2},&nbsp;{3},&nbsp;{4}", findDiscrepancy.APAddress1, findDiscrepancy.APAddress2,
                                    findDiscrepancy.APAddress3, findDiscrepancy.APAddress4, findDiscrepancy.APAddress5);
                            }
                            else if (!string.IsNullOrEmpty(findDiscrepancy.APAddress1) && !string.IsNullOrEmpty(findDiscrepancy.APAddress2)
                               && !string.IsNullOrEmpty(findDiscrepancy.APAddress3) && !string.IsNullOrEmpty(findDiscrepancy.APAddress4))
                            {
                                apAddress = string.Format("{0},&nbsp;{1},&nbsp;{2},&nbsp;{3}", findDiscrepancy.APAddress1, findDiscrepancy.APAddress2,
                                    findDiscrepancy.APAddress3, findDiscrepancy.APAddress4);
                            }
                            else if (!string.IsNullOrEmpty(findDiscrepancy.APAddress1) && !string.IsNullOrEmpty(findDiscrepancy.APAddress2)
                               && !string.IsNullOrEmpty(findDiscrepancy.APAddress3))
                            {
                                apAddress = string.Format("{0},&nbsp;{1},&nbsp;{2}", findDiscrepancy.APAddress1, findDiscrepancy.APAddress2,
                                    findDiscrepancy.APAddress3);
                            }
                            else if (!string.IsNullOrEmpty(findDiscrepancy.APAddress1) && !string.IsNullOrEmpty(findDiscrepancy.APAddress2))
                            {
                                apAddress = string.Format("{0},&nbsp;{1}", findDiscrepancy.APAddress1, findDiscrepancy.APAddress2);
                            }
                            else
                            {
                                apAddress = findDiscrepancy.APAddress1;
                            }

                            if (!string.IsNullOrEmpty(findDiscrepancy.APPincode))
                                apAddress = string.Format("{0},&nbsp;{1}", apAddress, findDiscrepancy.APPincode);

                            htmlBody = htmlBody.Replace("{APClerkAddress}", apAddress);

                        }
                        #endregion

                        #region Sending and saving email details ...
                        string[] sMailAddress = emailIds.Split(',');
                        var sentToWithLink = new System.Text.StringBuilder();
                        for (int index = 0; index < sMailAddress.Length; index++)
                            sentToWithLink.Append("<a target='_blank' href='" + Common.EncryptQuery("../LogDiscrepancy/DISLog_ReSendCommunication.aspx?CommTitle=DebitRaised&DebitRaiseId=" + debitRaiseId + "&CommunicationType=DebitRaised") + "' onclick='window.open(this.href, this.target,\"scrollbars=1,left=100px,top=100px,width=1000px,height=800px\"); return false;'>" + sMailAddress[index] + "</a>" + System.Environment.NewLine);


                        var discrepancyMailBE = new DiscrepancyMailBE();
                        discrepancyMailBE.Action = "AddDebitRaiseCommunication";
                        discrepancyMailBE.DebitRaiseId = debitRaiseId;
                        discrepancyMailBE.mailSubject = DebitNoteAgainstDiscEmailSubject;
                        discrepancyMailBE.sentTo = emailIds;
                        discrepancyMailBE.mailBody = htmlBody;
                        discrepancyMailBE.SendByID = Convert.ToInt32(Session["UserID"]);
                        discrepancyMailBE.IsMailSent = MailSentInLanguage;
                        discrepancyMailBE.languageID = objLanguage.LanguageID;
                        discrepancyMailBE.MailSentInLanguage = MailSentInLanguage;
                        discrepancyMailBE.CommTitle = "DebitRaised";
                        discrepancyMailBE.SentToWithLink = sentToWithLink.ToString();
                        var debitRaiseCommId = discrepancyBAL.addDebitRaiseCommunicationBAL(discrepancyMailBE);
                        if (MailSentInLanguage)
                        {
                            var emailToAddress = emailIds;
                            var emailFromAddress = Convert.ToString(ConfigurationManager.AppSettings["fromAddress"]);
                            var emailToSubject = DebitNoteAgainstDiscEmailSubject;
                            var emailBody = htmlBody;
                            Utilities.clsEmail oclsEmail = new Utilities.clsEmail();
                            oclsEmail.sendMail(emailToAddress, emailBody, emailToSubject, emailFromAddress, true);
                        }
                        #endregion
                    }
                    #endregion
                    Page.UICulture = Convert.ToString(Session["CultureInfo"]);
                }

                //#region Getting here Saved Debit Raise data & Workflow entry ...
                //if (debitRaiseId > 0)
                //{
                //    discrepancyBE = new DiscrepancyBE();
                //    //var discrepancyBAL = new DiscrepancyBAL();
                //    discrepancyBE.Action = "GetAllDebitRaise";
                //    discrepancyBE.DebitRaiseId = debitRaiseId;
                //    var findDiscrepancy = discrepancyBAL.GetAllDebitRaiseBAL(discrepancyBE).Find(x => x.APActionRef.Equals("ACP002"));
                //    if (findDiscrepancy != null)
                //    {
                //        ViewState["DebitNoteNo"] = findDiscrepancy.DebitNoteNo;
                //        DebitRaisedSuccessfully = DebitRaisedSuccessfully.Replace("##RaisedDebitNumber##", findDiscrepancy.DebitNoteNo);
                //        DebitNoteAgainstDiscEmailSubject = DebitNoteAgainstDiscEmailSubject.Replace("##DebitNoteNumber##", findDiscrepancy.DebitNoteNo);
                //        #region Logic to Save & Send email ...
                //        var oAPPBOK_CommunicationBAL = new APPBOK_CommunicationBAL();
                //        var oLanguages = oAPPBOK_CommunicationBAL.GetLanguages();
                //        foreach (MAS_LanguageBE objLanguage in oLanguages)
                //        {
                //            bool MailSentInLanguage = false;
                //            if (objLanguage.LanguageID.Equals(findDiscrepancy.Vendor.LanguageID))
                //                MailSentInLanguage = true;

                //            var path = AppDomain.CurrentDomain.BaseDirectory.ToLower();
                //            //var templatesPath = @"emailtemplates/communication1";
                //            var templateFile = string.Format(@"{0}emailtemplates/communication1/DebitNoteAgainstDiscrepancy.english.htm", path);

                //            #region Setting reason as per the language ...
                //            switch (objLanguage.LanguageID)
                //            {
                //                case 1:
                //                    Page.UICulture = clsConstants.EnglishISO; //"en-US"; // English
                //                    break;
                //                case 2:
                //                    Page.UICulture = clsConstants.FranceISO; //"fr"; // France
                //                    break;
                //                case 3:
                //                    Page.UICulture = clsConstants.GermanyISO; //"de"; // German
                //                    break;
                //                case 4:
                //                    Page.UICulture = clsConstants.NederlandISO; //"nl"; // Dutch
                //                    break;
                //                case 5:
                //                    Page.UICulture = clsConstants.SpainISO; //"es"; // Spanish
                //                    break;
                //                case 6:
                //                    Page.UICulture = clsConstants.ItalyISO; //"it"; // Italy
                //                    break;
                //                case 7:
                //                    Page.UICulture = clsConstants.CzechISO; //"cz"; // Czech
                //                    break;
                //                default:
                //                    Page.UICulture = clsConstants.EnglishISO; // "en-US"; // English
                //                    break;
                //            }

                //            #endregion

                //            #region  Prepairing html body format ...
                //            string htmlBody = null;
                //            using (StreamReader sReader = new StreamReader(templateFile.ToLower()))
                //            {
                //                htmlBody = sReader.ReadToEnd();
                //                //htmlBody = htmlBody.Replace("{logoInnerPath}", getAbsolutePath());

                //                htmlBody = htmlBody.Replace("{VendorNumber}", findDiscrepancy.Vendor.Vendor_No);
                //                htmlBody = htmlBody.Replace("{DebitNoteNumber}", WebCommon.getGlobalResourceValue("DebitNoteNumber"));
                //                htmlBody = htmlBody.Replace("{DebitNoteNumberValue}", findDiscrepancy.DebitNoteNo);
                //                htmlBody = htmlBody.Replace("{VendorName}", findDiscrepancy.Vendor.VendorName);
                //                htmlBody = htmlBody.Replace("{OurVATReference}", WebCommon.getGlobalResourceValue("OurVATReference"));

                //                if (!string.IsNullOrEmpty(findDiscrepancy.OurVATReference) && !string.IsNullOrWhiteSpace(findDiscrepancy.OurVATReference))
                //                    htmlBody = htmlBody.Replace("{OurVATReferenceValue}", findDiscrepancy.OurVATReference);
                //                else
                //                    htmlBody = htmlBody.Replace("{OurVATReferenceValue}", string.Empty);

                //                htmlBody = htmlBody.Replace("{VendorAddress1}", findDiscrepancy.Vendor.address1);
                //                htmlBody = htmlBody.Replace("{YourVATReference}", WebCommon.getGlobalResourceValue("YourVATReference"));

                //                if (!string.IsNullOrEmpty(findDiscrepancy.YourVATReference) && !string.IsNullOrWhiteSpace(findDiscrepancy.YourVATReference))
                //                    htmlBody = htmlBody.Replace("{YourVATReferenceValue}", findDiscrepancy.YourVATReference);
                //                else
                //                    htmlBody = htmlBody.Replace("{YourVATReferenceValue}", string.Empty);

                //                htmlBody = htmlBody.Replace("{VendorAddress2}", findDiscrepancy.Vendor.address2);
                //                htmlBody = htmlBody.Replace("{DebitReason}", WebCommon.getGlobalResourceValue("DebitReason"));
                //                htmlBody = htmlBody.Replace("{DebitReasonValue}", findDiscrepancy.Reason);

                //                if (!string.IsNullOrEmpty(findDiscrepancy.Vendor.VMPPIN) && !string.IsNullOrWhiteSpace(findDiscrepancy.Vendor.VMPPIN))
                //                    htmlBody = htmlBody.Replace("{VMPPIN}", findDiscrepancy.Vendor.VMPPIN);
                //                else
                //                    htmlBody = htmlBody.Replace("{VMPPIN}", string.Empty);

                //                if (!string.IsNullOrEmpty(findDiscrepancy.Vendor.VMPPOU) && !string.IsNullOrWhiteSpace(findDiscrepancy.Vendor.VMPPOU))
                //                    htmlBody = htmlBody.Replace("{VMPPOU}", findDiscrepancy.Vendor.VMPPOU);
                //                else
                //                    htmlBody = htmlBody.Replace("{VMPPOU}", string.Empty);

                //                if (!string.IsNullOrEmpty(findDiscrepancy.Vendor.city) && !string.IsNullOrWhiteSpace(findDiscrepancy.Vendor.city))
                //                    htmlBody = htmlBody.Replace("{VendorCity}", findDiscrepancy.Vendor.city);
                //                else
                //                    htmlBody = htmlBody.Replace("{VendorCity}", string.Empty);

                //                htmlBody = htmlBody.Replace("{DearSirMadam}", WebCommon.getGlobalResourceValue("DearSirMadam"));
                //                htmlBody = htmlBody.Replace("{Date}", WebCommon.getGlobalResourceValue("Date"));

                //                var debitRaiseDate = Convert.ToDateTime(findDiscrepancy.DebitRaiseDate);
                //                htmlBody = htmlBody.Replace("{DateValue}", debitRaiseDate.ToString("dd/MM/yyyy"));

                //                htmlBody = htmlBody.Replace("{DebitLetterMessage1}", WebCommon.getGlobalResourceValue("DebitLetterMessage1"));
                //                htmlBody = htmlBody.Replace("{VDRNo}", WebCommon.getGlobalResourceValue("VDRNoKey"));
                //                htmlBody = htmlBody.Replace("{VDRNoValue}", findDiscrepancy.VDRNo);
                //                htmlBody = htmlBody.Replace("{DebitLetterMessage2}", WebCommon.getGlobalResourceValue("DebitLetterMessage2"));

                //                var vdrDateValue = Convert.ToDateTime(findDiscrepancy.DiscrepancyLogDate);
                //                htmlBody = htmlBody.Replace("{VDRDateValue}", vdrDateValue.ToString("dd/MM/yyyy"));

                //                htmlBody = htmlBody.Replace("{DebitLetterMessage3}", WebCommon.getGlobalResourceValue("DebitLetterMessage3"));
                //                htmlBody = htmlBody.Replace("{DebitValue}", Convert.ToString(findDiscrepancy.StateValueDebited));
                //                htmlBody = htmlBody.Replace("{DebitCurrencyValue}", Convert.ToString(findDiscrepancy.Currency.CurrencyName));
                //                htmlBody = htmlBody.Replace("{DebitLetterMessage4}", WebCommon.getGlobalResourceValue("DebitLetterMessage4"));
                //                htmlBody = htmlBody.Replace("{DebiNoteComments}", findDiscrepancy.Comment);
                //                htmlBody = htmlBody.Replace("{DebitLetterMessage5}", WebCommon.getGlobalResourceValue("DebitLetterMessage5"));
                //                htmlBody = htmlBody.Replace("{Yoursfaithfully}", WebCommon.getGlobalResourceValue("Yoursfaithfully"));
                //                htmlBody = htmlBody.Replace("{APClerkName}", findDiscrepancy.CreatedBy);

                //                if (!string.IsNullOrEmpty(findDiscrepancy.CreatedByContactNumber) && !string.IsNullOrWhiteSpace(findDiscrepancy.CreatedByContactNumber))
                //                    htmlBody = htmlBody.Replace("{APClerkContactNo}", findDiscrepancy.CreatedByContactNumber);
                //                else
                //                    htmlBody = htmlBody.Replace("{APClerkContactNo}", string.Empty);

                //                var apAddress = string.Empty;
                //                if (!string.IsNullOrEmpty(findDiscrepancy.APAddress1) && !string.IsNullOrEmpty(findDiscrepancy.APAddress2)
                //                    && !string.IsNullOrEmpty(findDiscrepancy.APAddress3) && !string.IsNullOrEmpty(findDiscrepancy.APAddress4)
                //                    && !string.IsNullOrEmpty(findDiscrepancy.APAddress5) && !string.IsNullOrEmpty(findDiscrepancy.APAddress6))
                //                {
                //                    apAddress = string.Format("{0},&nbsp;{1},&nbsp;{2},&nbsp;{3},&nbsp;{4},&nbsp;{5}", findDiscrepancy.APAddress1, findDiscrepancy.APAddress2,
                //                        findDiscrepancy.APAddress3, findDiscrepancy.APAddress4, findDiscrepancy.APAddress5, findDiscrepancy.APAddress6);
                //                }
                //                else if (!string.IsNullOrEmpty(findDiscrepancy.APAddress1) && !string.IsNullOrEmpty(findDiscrepancy.APAddress2)
                //                    && !string.IsNullOrEmpty(findDiscrepancy.APAddress3) && !string.IsNullOrEmpty(findDiscrepancy.APAddress4)
                //                    && !string.IsNullOrEmpty(findDiscrepancy.APAddress5))
                //                {
                //                    apAddress = string.Format("{0},&nbsp;{1},&nbsp;{2},&nbsp;{3},&nbsp;{4}", findDiscrepancy.APAddress1, findDiscrepancy.APAddress2,
                //                        findDiscrepancy.APAddress3, findDiscrepancy.APAddress4, findDiscrepancy.APAddress5);
                //                }
                //                else if (!string.IsNullOrEmpty(findDiscrepancy.APAddress1) && !string.IsNullOrEmpty(findDiscrepancy.APAddress2)
                //                   && !string.IsNullOrEmpty(findDiscrepancy.APAddress3) && !string.IsNullOrEmpty(findDiscrepancy.APAddress4))
                //                {
                //                    apAddress = string.Format("{0},&nbsp;{1},&nbsp;{2},&nbsp;{3}", findDiscrepancy.APAddress1, findDiscrepancy.APAddress2,
                //                        findDiscrepancy.APAddress3, findDiscrepancy.APAddress4);
                //                }
                //                else if (!string.IsNullOrEmpty(findDiscrepancy.APAddress1) && !string.IsNullOrEmpty(findDiscrepancy.APAddress2)
                //                   && !string.IsNullOrEmpty(findDiscrepancy.APAddress3))
                //                {
                //                    apAddress = string.Format("{0},&nbsp;{1},&nbsp;{2}", findDiscrepancy.APAddress1, findDiscrepancy.APAddress2,
                //                        findDiscrepancy.APAddress3);
                //                }
                //                else if (!string.IsNullOrEmpty(findDiscrepancy.APAddress1) && !string.IsNullOrEmpty(findDiscrepancy.APAddress2))
                //                {
                //                    apAddress = string.Format("{0},&nbsp;{1}", findDiscrepancy.APAddress1, findDiscrepancy.APAddress2);
                //                }
                //                else
                //                {
                //                    apAddress = findDiscrepancy.APAddress1;
                //                }

                //                if (!string.IsNullOrEmpty(findDiscrepancy.APPincode))
                //                    apAddress = string.Format("{0},&nbsp;{1}", apAddress, findDiscrepancy.APPincode);

                //                htmlBody = htmlBody.Replace("{APClerkAddress}", apAddress);

                //            }
                //            #endregion

                //            #region Sending and saving email details ...
                //            string[] sMailAddress = emailIds.Split(',');
                //            var sentToWithLink = new System.Text.StringBuilder();
                //            for (int index = 0; index < sMailAddress.Length; index++)
                //                sentToWithLink.Append("<a target='_blank' href='" + Common.EncryptQuery("../LogDiscrepancy/DISLog_ReSendCommunication.aspx?CommTitle=DebitRaised&DebitRaiseId=" + debitRaiseId + "&CommunicationType=DebitRaised") + "' onclick='window.open(this.href, this.target,\"scrollbars=1,left=100px,top=100px,width=1000px,height=800px\"); return false;'>" + sMailAddress[index] + "</a>" + System.Environment.NewLine);


                //            var discrepancyMailBE = new DiscrepancyMailBE();
                //            discrepancyMailBE.Action = "AddDebitRaiseCommunication";
                //            discrepancyMailBE.DebitRaiseId = debitRaiseId;
                //            discrepancyMailBE.mailSubject = DebitNoteAgainstDiscEmailSubject;
                //            discrepancyMailBE.sentTo = emailIds;
                //            discrepancyMailBE.mailBody = htmlBody;
                //            discrepancyMailBE.SendByID = Convert.ToInt32(Session["UserID"]);
                //            discrepancyMailBE.IsMailSent = MailSentInLanguage;
                //            discrepancyMailBE.languageID = objLanguage.LanguageID;
                //            discrepancyMailBE.MailSentInLanguage = MailSentInLanguage;
                //            discrepancyMailBE.CommTitle = "DebitRaised";
                //            discrepancyMailBE.SentToWithLink = sentToWithLink.ToString();
                //            var debitRaiseCommId = discrepancyBAL.addDebitRaiseCommunicationBAL(discrepancyMailBE);
                //            if (MailSentInLanguage)
                //            {
                //                var emailToAddress = emailIds;
                //                var emailFromAddress = Convert.ToString(ConfigurationManager.AppSettings["fromAddress"]);
                //                var emailToSubject = DebitNoteAgainstDiscEmailSubject;
                //                var emailBody = htmlBody;
                //                Utilities.clsEmail oclsEmail = new Utilities.clsEmail();
                //                oclsEmail.sendMail(emailToAddress, emailBody, emailToSubject, emailFromAddress, true);
                //            }
                //            #endregion
                //        }
                //        #endregion
                //        Page.UICulture = Convert.ToString(Session["CultureInfo"]);
                //    }

                //    #region Data is being Insert in Discrepancy Workflow tables ...

                //    oDISLog_WorkFlowBE.Action = "InsertUpdateWorkflowActions";
                //    oDISLog_WorkFlowBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
                //    oDISLog_WorkFlowBE.ACP002_NoteType = 'D';

                //    if (ViewState["DebitNoteNo"] != null)
                //        oDISLog_WorkFlowBE.ACP002_NoteNumber = Convert.ToString(ViewState["DebitNoteNo"]); //txtDebitNoteNumber.Text;

                //    oDISLog_WorkFlowBE.ACP002_CarriageCost = txtDebitCarriageCost.Text.Trim() != "" ? Convert.ToDecimal(txtDebitCarriageCost.Text) : (decimal?)null;
                //    oDISLog_WorkFlowBE.ACP002_ActualValue = txtDebitValueActual.Text.Trim() != "" ? Convert.ToDecimal(txtDebitValueActual.Text) : (decimal?)null;
                //    oDISLog_WorkFlowBE.StageCompletedForWhom = "ACP002_StageCompleted";
                //    if (chkDebitStageComplete.Checked)
                //    {
                //        oDISLog_WorkFlowBE.StageCompleted = true;
                //    }
                //    else
                //    {
                //        oDISLog_WorkFlowBE.StageCompleted = false;
                //    }
                //    oDISLog_WorkFlowBE.Comment = txtDebitComment.Text;

                //    //update the work flow action table each time for the comments and other fields( if stage is completed)
                //    oDISLog_WorkFlowBE.DiscrepancyWorkFlowID = Convert.ToInt32(GetQueryStringValue("DisWFlowID").ToString());

                //    //insert or update the record according to the stage completed status
                //    int? iResult = oDISLog_WorkFlowBAL.addWorkFlowActionsBAL(oDISLog_WorkFlowBE);
                //    if (GetQueryStringValue("FromPage") != null && GetQueryStringValue("FromPage").ToString().Trim() == "IncorrectAddress")
                //    {
                //        DebitSaveIncorrectAddress();
                //    }
                //    else if (GetQueryStringValue("FromPage") != null && GetQueryStringValue("FromPage").ToString().Trim() == "WrongPackSize")
                //    {
                //        DebitSaveWrongPackSize();
                //    }

                //    else if (GetQueryStringValue("FromPage") != null && GetQueryStringValue("FromPage").ToString().Trim() == "NoPO")
                //    {
                //        DebitSave();
                //    }

                //    else if (GetQueryStringValue("FromPage") != null && GetQueryStringValue("FromPage").ToString().Trim() == "NoPaperwork")
                //    {
                //        DebitSave();
                //    }
                //    else if (GetQueryStringValue("FromPage") != null && GetQueryStringValue("FromPage").ToString().Trim() == "overs"
                //          || GetQueryStringValue("FromPage") != null && GetQueryStringValue("FromPage").ToString().Trim() == "IncorrectProduct"
                //          || GetQueryStringValue("FromPage") != null && GetQueryStringValue("FromPage").ToString().Trim() == "GoodsReceivedDamaged"
                //          || GetQueryStringValue("FromPage") != null && GetQueryStringValue("FromPage").ToString().Trim() == "QualityIssue")
                //    {
                //        DebitSaveOvers();
                //    }
                //    else
                //    {
                //        DebitSave();
                //    }

                //    #endregion
                //}
                //#endregion
            }
            #endregion



            //**********************************************************
            oDISLog_WorkFlowBE.Action = "InsertUpdateWorkflowActions";
            oDISLog_WorkFlowBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
            oDISLog_WorkFlowBE.StageCompletedForWhom = "ACP011_StageCompleted";
            if (chkStageComplete.Checked)
            {
                oDISLog_WorkFlowBE.StageCompleted = true;
            }
            else
            {
                oDISLog_WorkFlowBE.StageCompleted = false;
            }
            oDISLog_WorkFlowBE.Comment = txtComment.Text;
            oDISLog_WorkFlowBE.ApDocumentNumber = txtAPDocumentNumber.Text;
            oDISLog_WorkFlowBE.InvoiceResolution = "Vendor Debited";
            //update the work flow action table each time for the comments and other fields( if stage is completed)
            oDISLog_WorkFlowBE.DiscrepancyWorkFlowID = Convert.ToInt32(GetQueryStringValue("DisWFlowID").ToString());

            //insert or update the record according to the stage completed status
            int? iResult = oDISLog_WorkFlowBAL.addWorkFlowActionsBAL(oDISLog_WorkFlowBE);

            InquiryHTML();


        }
    }
    private string SendMailAndGetMailLinks(int DiscrepancyLogID, string discrepancyType, string sCurrentCommunicationLevel, string sNextCommunicationLevel)
    {
        sendCommunicationAllLevel osendCommunicationAllLevel = new sendCommunicationAllLevel();
        return osendCommunicationAllLevel.SendMailAndGetMailLinks(DiscrepancyLogID, discrepancyType, sCurrentCommunicationLevel, sNextCommunicationLevel);
        //return osendCommunicationAllLevel.sendCommunicationByEMail(DiscrepancyLogID, discrepancyType, sCurrentCommunicationLevel, sNextCommunicationLevel);
    }
    private void InquiryHTML()
    {
        DISLog_WorkFlowBE oDISLog_WorkFlowBE = new DISLog_WorkFlowBE();
        DISLog_WorkFlowBAL oDISLog_WorkFlowBAL = new DISLog_WorkFlowBAL();
        //now insert the work flow HTML according to the action taken only if the stage is completed
        DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
        DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();


        if (chkStageComplete.Checked)
        {
            WorkflowHTML oWorkflowHTML = new WorkflowHTML();

            sentToLinks = SendMailAndGetMailLinks(Convert.ToInt32(GetQueryStringValue("disLogID")), "invoice query", "Communication2", "Communication6");

            oDiscrepancyBE.GINHTML = oWorkflowHTML.function3("white", null, "", "");
            oDiscrepancyBE.GINActionRequired = false;
            oDiscrepancyBE.INVHTML = oWorkflowHTML.function3("white", null, "", "");
            oDiscrepancyBE.INVActionRequired = false;
            oDiscrepancyBE.APHTML = oWorkflowHTML.function13(pColor: "Yellow", pDate: DateTime.Now,
            pUserName: Convert.ToString(Session["UserName"]), pAction: WebCommon.getGlobalResourceValue("ACP009Workflow1"),
            aPDocument: txtAPDocumentNumber.Text, pComments: txtComment.Text, DebitAmount: txtAmountDebit.Text, currency: ddlCurrencyDebit.SelectedItem.Text, vendorVATRef: txtVendVendorVATReference.Text, debitNote: Convert.ToString(ViewState["DebitNoteNo"]));

            oDiscrepancyBE.APActionRequired = false;

            oDiscrepancyBE.VENHTML = oWorkflowHTML.function2(DateTime.Now, null, "Blue", Convert.ToString(sentToLinks), "", WebCommon.getGlobalResourceValue("Communicationsent"));
            oDiscrepancyBE.VenActionRequired = false;

            oDiscrepancyBE.Action = "InsertHTML";
            oDiscrepancyBE.CloseDiscrepancy = true;
            oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
            oDiscrepancyBE.LoggedDateTime = DateTime.Now;
            oDiscrepancyBE.LevelNumber = 1;
            int? iResult1 = oDiscrepancyBAL.addWorkFlowHTMLsBAL(oDiscrepancyBE);
        }
        else
        {
            /*Enter comments with date*/
            InsertHTML();
        }
    }


    private void InsertHTML()
    {
        DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
        DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();

        WorkflowHTML oWorkflowHTML = new WorkflowHTML();
        oDiscrepancyBE.GINHTML = oWorkflowHTML.function3("White", DateTime.Now, txtComment.Text.Trim(), "");
        oDiscrepancyBE.Action = "InsertHTML";
        oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
        oDiscrepancyBE.LoggedDateTime = DateTime.Now;
        oDiscrepancyBE.LevelNumber = 1;

        int? iResult = oDiscrepancyBAL.addWorkFlowHTMLsBAL(oDiscrepancyBE);
    }

    private int? sendAndSaveDiscrepancyCommunication(int iDiscrepancy, string LetterType)
    {

        GetDiscrepancyEmailListOfVendor();

        int? vendorID = Vendorid;

        sendCommunication communication = new sendCommunication();
        int? retVal = communication.sendCommunicationByEMail2(iDiscrepancy, "invoice", EmailList, vendorID, "", "", "", 0, LetterType);
        sSendMailLink = communication.sSendMailLink;
        return retVal;

    }


    private string GetDiscrepancyEmailListOfVendor()
    {
        if (GetQueryStringValue("disLogID") != null)
        {
            DiscrepancyBE oNewDiscrepancyBE = new DiscrepancyBE();
            DiscrepancyBAL oNewDiscrepancyBAL = new DiscrepancyBAL();
            oNewDiscrepancyBE.Action = "GetDiscrepancyLog";
            oNewDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
            List<DiscrepancyBE> lstDisLog = oNewDiscrepancyBAL.GetDiscrepancyLogDetailsBAL(oNewDiscrepancyBE);
            oNewDiscrepancyBAL = null;
            if (lstDisLog != null && lstDisLog.Count > 0)
            {
                Siteid = lstDisLog[0].Site.SiteID;
                Vendorid = Convert.ToInt32(lstDisLog[0].VendorID);
            }

            EmailList = GetDiscrepancyContactsOfVendor(Vendorid, Siteid);

        }
        return EmailList;

    }

    #endregion
}
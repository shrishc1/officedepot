﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="APAction.ascx.cs" Inherits="APAction_APAction" %>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/ucCountry.ascx" TagName="ucCountry" TagPrefix="uc1" %>
<%@ Register Src="~/ModuleUI/Discrepancy/UserControl/ucSDRCommunicationAPListing.ascx"
    TagName="ucSDRCommunication" TagPrefix="uc2" %>
<table width="100%" border="0">
    <tr>
        <td width="90%">
            <cc1:ucPanel ID="pnlApAction" runat="server" Width="100%" CssClass="fieldset-form"
                GroupingText="AP Action">
                <table class="form-table" cellpadding="5" cellspacing="0" width="96%">
                    <tr align="center">
                        <td>
                            <table class="form-table" cellpadding="0" cellspacing="0" width="70%" border="0">
                                <tr>
                                    <td width="30%">
                                        <cc1:ucLabel ID="lblVendorVATReference" isRequired="true" Text="Vendor VAT Reference"
                                            runat="server"></cc1:ucLabel>
                                    </td>
                                    <td width="5%">
                                        :
                                    </td>
                                    <td width="65%" style="text-align: left">
                                        <cc1:ucTextbox ID="txtVendVendorVATReference" runat="server" Width="143px"></cc1:ucTextbox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <cc1:ucLabel ID="lblOfficeDepotVATReference" isRequired="true" Text="Office Depot VAT Reference"
                                            runat="server"></cc1:ucLabel>
                                    </td>
                                    <td>
                                        :
                                    </td>
                                    <td style="text-align: left">
                                        <%--<cc1:ucDropdownList ID="ddlVendOfficeDepotVATReference" runat="server" Width="150px">
                                        </cc1:ucDropdownList>--%>
                                        <cc1:ucLabel ID="lblOfficeDepotVATReferenceValue" runat="server"></cc1:ucLabel>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <cc1:ucLabel ID="lblValueInDispute" runat="server" isRequired="true" Text="Value In Dispute"></cc1:ucLabel>
                                    </td>
                                    <td>
                                        :
                                    </td>
                                    <td style="text-align: left">
                                        <cc1:ucTextbox ID="txtValueInDispute" runat="server" ReadOnly="true" Enabled="false"
                                            Width="150px" Text="0"></cc1:ucTextbox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <cc1:ucLabel ID="lblValuetobeDebited" runat="server" isRequired="true" Text="Value to be Debited"></cc1:ucLabel>
                                    </td>
                                    <td>
                                        :
                                    </td>
                                    <td style="text-align: left">
                                        <cc1:ucTextbox ID="txtValueDebited" runat="server" MaxLength="7" Width="150px"></cc1:ucTextbox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <cc1:ucLabel ID="lblCurrency" runat="server" isRequired="true" Text="Currency"></cc1:ucLabel>
                                    </td>
                                    <td>
                                        :
                                    </td>
                                    <td style="text-align: left">
                                        <cc1:ucDropdownList ID="ddlCurrency" runat="server" Width="150px">
                                        </cc1:ucDropdownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <cc1:ucLabel ID="lblReason" runat="server" isRequired="true" Text="Reason"></cc1:ucLabel>
                                    </td>
                                    <td>
                                        :
                                    </td>
                                    <td style="text-align: left">
                                        <cc1:ucDropdownList ID="ddlReason" runat="server" Width="150px">
                                        </cc1:ucDropdownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <cc1:ucLabel ID="lblDebitTemplate" isRequired="true" runat="server"></cc1:ucLabel>
                                    </td>
                                    <td>
                                        :
                                    </td>
                                    <td style="text-align: left">
                                        <uc1:ucCountry ID="ucCountry" runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucPanel ID="pnlCommunicationDetail" runat="server" GroupingText="Communication Detail"
                                CssClass="fieldset-form">
                                <table width="100%" cellspacing="0" cellpadding="0" class="form-table">
                                    <tr>
                                        <td>
                                            <uc2:ucSDRCommunication ID="ucSDRCommunication1" runat="server" />
                                        </td>
                                    </tr>
                                </table>
                            </cc1:ucPanel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucLabel ID="lblCommentSentToVendor" runat="server" Text="Please enter comment to be sent to Vendor"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucTextbox ID="txtCommentToVendor" runat="server" onkeyup="checkTextLengthOnKeyUp(this,1000);"
                                TextMode="MultiLine" CssClass="textarea" Width="98%" Height="50px"></cc1:ucTextbox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td align="right" valign="bottom">
                            <cc1:ucButton ID="btnSave" runat="server" Text="Save" class="button" OnClick="btnSave_Click" />
                            <cc1:ucButton ID="btnBack" runat="server" Text="Back" class="button" OnClick="btnBack_Click" />
                        </td>
                    </tr>
                </table>
            </cc1:ucPanel>
        </td>
    </tr>
</table>

﻿using System;
using System.Collections.Generic;
using System.Data;
using BusinessEntities.ModuleBE.AdminFunctions;
using BusinessEntities.ModuleBE.Discrepancy.LogDiscrepancy;
using BusinessLogicLayer.ModuleBAL.AdminFunctions;
using BusinessLogicLayer.ModuleBAL.Discrepancy.LogDiscrepancy;
using WebUtilities;
using System.Web.UI;
using System.IO;
using System.Linq;


public partial class APAction_ACP005 : CommonPage
{
    System.Text.StringBuilder sSendMailLink = new System.Text.StringBuilder();

    protected string CommentRequired = "-"+WebCommon.getGlobalResourceValue("CommentRequired");
    protected string AmountDebitERROR = WebCommon.getGlobalResourceValue("AmountDebitERROR");    
    string fileUploadError1 = WebCommon.getGlobalResourceValue("fileUploadError1");
    string WrongFile = WebCommon.getGlobalResourceValue("WrongFile");
    protected string CreditNoteNumberERROR = "-" + WebCommon.getGlobalResourceValue("CreditNoteNumberERROR");
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //DiscrepancyBE oNewDiscrepancyBE = new DiscrepancyBE();
            //DiscrepancyBAL oNewDiscrepancyBAL = new DiscrepancyBAL();
            //oNewDiscrepancyBE.Action = "GetExpectedDebitedAmount";
            //oNewDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
            //DataSet ds = oNewDiscrepancyBAL.GetInvoiceDiscrepancyCommentsBAL(oNewDiscrepancyBE);
            //if (ds.Tables[0].Rows.Count > 0)
            //{
            //    if (!string.IsNullOrEmpty(ds.Tables[0].Rows[0]["Amount"].ToString()))
            //    {
            //        txtExpectedDebit.Text = ds.Tables[0].Rows[0]["Amount"].ToString();
            //    }
            //}
        }
    }


    #region Events
    protected void btnBack_Click(object sender, EventArgs e)
    {
        //Redirecting to Previous Page
        RedirectToPage(GetQueryStringValue("PreviousPage"));
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {       
        Save();

        //Redirecting to Previous Page
       //
    }

   

    #endregion

    #region

    private void Save()
    {
        DISLog_WorkFlowBE oDISLog_WorkFlowBE = new DISLog_WorkFlowBE();
        DISLog_WorkFlowBAL oDISLog_WorkFlowBAL = new DISLog_WorkFlowBAL();
        string fileName1 = string.Empty;
        if (rdoACP008Text1.Checked)
        {
            if (fileupload.HasFile == false)
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('" + fileUploadError1 + "')", true);
                return;
            }

            if (fileupload.HasFile)
            {
                if (fileupload.FileName.Split('.').ElementAt(1).ToLower() == "jpg"  || fileupload.FileName.Split('.').ElementAt(1).ToLower() == "bmp"  || fileupload.FileName.Split('.').ElementAt(1).ToLower() == "pdf")
                {
                    fileName1 = fileupload.FileName;
                    fileupload.SaveAs(Path.Combine(Server.MapPath("~/images/discrepancy"), fileName1));
                    oDISLog_WorkFlowBE.Action = "InvoiceInsertDocInformation";
                    oDISLog_WorkFlowBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
                    oDISLog_WorkFlowBE.DocPath = fileName1;
                    oDISLog_WorkFlowBE.Doctype = "Credit";
                    oDISLog_WorkFlowBAL.addWorkFlowInvoiceQueryActionsBAL(oDISLog_WorkFlowBE);
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('" + WrongFile + "')", true);
                    return;
                }
            }
        }

     
        if (GetQueryStringValue("disLogID") != null && GetQueryStringValue("disLogID").ToString() != "")
        {
            //**********************************************************
            string Location = hdfLocation.Value;
            DiscrepancyBE oDiscrepancyBE1 = new DiscrepancyBE();
            DiscrepancyBAL oDiscrepancyBAL1 = new DiscrepancyBAL();
            oDiscrepancyBE1.Action = "UpdateLocation";
            oDiscrepancyBE1.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
            oDiscrepancyBE1.Location = Location;
            oDiscrepancyBE1.UserID = Convert.ToInt32(Session["UserID"]);
            string sResult1 = oDiscrepancyBAL1.updateDiscrepancyLocationBAL(oDiscrepancyBE1);
            //**********************************************************
            oDISLog_WorkFlowBE.Action = "InsertUpdateWorkflowActions";
            oDISLog_WorkFlowBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
            oDISLog_WorkFlowBE.StageCompletedForWhom = "ACP010_StageCompleted";
            if (chkStageComplete.Checked)
            {
                oDISLog_WorkFlowBE.StageCompleted = true;
            }
            else
            {
                oDISLog_WorkFlowBE.StageCompleted = false;
            }
            oDISLog_WorkFlowBE.Comment = txtComments.Text;
            if (rdoACP008Text1.Checked)
            {
                oDISLog_WorkFlowBE.InvoiceResolution = "Vendor Credited";
                oDISLog_WorkFlowBE.CreditNoteNumber = txtCreditNoteNumber.Text;
            }
            else
            {
                oDISLog_WorkFlowBE.InvoiceResolution = "Vendor Refused";                
            }
            //update the work flow action table each time for the comments and other fields( if stage is completed)
            oDISLog_WorkFlowBE.DiscrepancyWorkFlowID = Convert.ToInt32(GetQueryStringValue("DisWFlowID").ToString());

            //insert or update the record according to the stage completed status
            int? iResult = oDISLog_WorkFlowBAL.addWorkFlowActionsBAL(oDISLog_WorkFlowBE);

            InquiryHTML();


        }
    }

    private void InquiryHTML()
    {
        DISLog_WorkFlowBE oDISLog_WorkFlowBE = new DISLog_WorkFlowBE();
        DISLog_WorkFlowBAL oDISLog_WorkFlowBAL = new DISLog_WorkFlowBAL();
        //now insert the work flow HTML according to the action taken only if the stage is completed
        DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
        DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();


        if (chkStageComplete.Checked)
        {
            WorkflowHTML oWorkflowHTML = new WorkflowHTML();


            oDiscrepancyBE.GINHTML = oWorkflowHTML.function3("white", null, "", "");
            oDiscrepancyBE.GINActionRequired=false;
            oDiscrepancyBE.INVHTML = oWorkflowHTML.function3("white", null, "", "");
            oDiscrepancyBE.INVActionRequired = false;
            if (rdoACP008Text1.Checked)
            {
                oDiscrepancyBE.APHTML = oWorkflowHTML.function13(pColor: "Yellow", pDate: DateTime.Now,
                pUserName: Convert.ToString(Session["UserName"]), pAction: WebCommon.getGlobalResourceValue("ACP009Workflow"), creditNote: txtCreditNoteNumber.Text, pComments: txtComments.Text);
            }
            else
            {
                oDiscrepancyBE.APHTML = oWorkflowHTML.function13(pColor: "Yellow", pDate: DateTime.Now,
                     pUserName: Convert.ToString(Session["UserName"]), pAction: WebCommon.getGlobalResourceValue("ACP010Workflow"), pComments: txtComments.Text);
            }

            oDiscrepancyBE.APActionRequired = false;
            oDiscrepancyBE.VENHTML = oWorkflowHTML.function3("white", null, "", "");

            oDiscrepancyBE.VenActionRequired = false;

            oDiscrepancyBE.Action = "InsertHTML";
            oDiscrepancyBE.CloseDiscrepancy = true;
            oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
            oDiscrepancyBE.LoggedDateTime = DateTime.Now;
            oDiscrepancyBE.LevelNumber = 1;
            int? iResult1 = oDiscrepancyBAL.addWorkFlowHTMLsBAL(oDiscrepancyBE);
            RedirectToPage(GetQueryStringValue("PreviousPage"));
        }
        else
        {
            /*Enter comments with date*/
            InsertHTML();
        }
    }


    private void InsertHTML()
    {
        DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
        DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();

        WorkflowHTML oWorkflowHTML = new WorkflowHTML();
        oDiscrepancyBE.GINHTML = oWorkflowHTML.function3("White", DateTime.Now, txtComments.Text.Trim(), "");
        oDiscrepancyBE.Action = "InsertHTML";
        oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
        oDiscrepancyBE.LoggedDateTime = DateTime.Now;
        oDiscrepancyBE.LevelNumber = 1;

        int? iResult = oDiscrepancyBAL.addWorkFlowHTMLsBAL(oDiscrepancyBE);
    }   

    #endregion
}
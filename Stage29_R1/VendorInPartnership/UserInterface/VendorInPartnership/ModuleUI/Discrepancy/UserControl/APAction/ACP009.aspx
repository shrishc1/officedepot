﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ACP009.aspx.cs" Inherits="APAction_ACP005" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="~/ModuleUI/Discrepancy/UserControl/InvoiceQueryucSDRCommunication.ascx" TagName="ucSDRCommunication"
    TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Office Depot</title>
    <link rel="stylesheet" type="text/css" href="../../../../Css/style.css" />
    <link type="text/css" href="../../../../Css/ui-lightness/jquery-ui-1.8.5.custom.css"
        rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="../../../../Css/superfish.css" media="screen" />
    <script type="text/javascript" src="<%= ResolveClientUrl("../../../../Scripts/jquery-1.2.6.min.js")%>"></script>
    <script type="text/javascript" src="<%= ResolveClientUrl("../../../../Scripts/jquery-1.4.2.min.js")%>"></script>
    <script type="text/javascript" src="<%= ResolveClientUrl("../../../../Scripts/jquery-ui-1.8.5.custom.min.js")%>"></script>
    <script type="text/javascript" src="<%= ResolveClientUrl("../../../../Scripts/hoverIntent.js")%>"></script>
    <script type="text/javascript" src="<%= ResolveClientUrl("../../../../Scripts/superfish.js")%>"></script>
    <script type="text/javascript" src="<%= ResolveClientUrl("../../../../Scripts/ValidateInputs.js")%>"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#<%=btnSave.ClientID%>').click(function () {
                if ($('#<%=txtAPDocumentNumber.ClientID%>').val() == '' && $('#<%=txtComment.ClientID%>').val() == '' && $('#<%=txtAmountDebit.ClientID%>').val() == ''
                && $('#<%=ddlCurrencyDebit.ClientID%>').val() == '0' && $('#<%=txtVendVendorVATReference.ClientID%>').val() == ''
                ) {
                    alert('<%=CurrencyMandatry %>\n<%=PleaseentertheVendorVATReference %>\n<%=AmountDebitErrorMessage %>\n<%=APDocumentErrorMessage %>\n<%=AmountDebitErrorMessage %>\n<%=CommentRequired %>');
                    return false;
                }


                else if ($('#<%=ddlCurrencyDebit.ClientID%>').val() == '0') {
                    alert('<%=CurrencyMandatry %>');
                    return false;
                }
                else if ($('#<%=txtAmountDebit.ClientID%>').val() == '') {
                    alert('<%=AmountDebitErrorMessage %>');
                    return false;
                }

                else if ($('#<%=txtVendVendorVATReference.ClientID%>').val() == '') {
                    alert('<%=PleaseentertheVendorVATReference %>');
                    return false;
                }

                //                else if ($('#<%=txtAPDocumentNumber.ClientID%>').val() == '') {
                //                    alert('<%=APDocumentErrorMessage %>');
                //                    return false;
                //                }

                else if ($('#<%=txtAPDocumentNumber.ClientID%>').val() == '') {
                    alert('<%=APDocumentErrorMessage %>');
                    return false;
                }

                else if ($('#<%=txtComment.ClientID%>').val() == '') {
                    alert('<%=CommentRequired %>');
                    return false;
                }
            });
            $("#<%=txtAmountDebit.ClientID%>").keypress(function (event) {
                //this.value = this.value.replace(/[^0-9\.]/g,'');
                $(this).val($(this).val().replace(/[^0-9\.]/g, ''));
                if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
                    event.preventDefault();
                }
            });
        });




        function GetLocation() {
            var Location = parent.document.getElementById('ctl00_ContentPlaceHolder1_txtLocation').value;
            document.getElementById('<%=hdfLocation.ClientID%>').value = Location;
            if (checkValidationGroup("Save")) {
                document.getElementById('<%=btnSave.ClientID%>').style.visibility = "hidden";
            }
            else {
                document.getElementById('<%=btnSave.ClientID%>').style.visibility = "visible";
                return false;
            }
        }
    </script>
</head>
<body>
    <form id="form2" runat="server" method="post">
    <cc1:ucPanel ID="pnlACP007c" runat="server" Width="100%" CssClass="fieldset-form"
        GroupingText="Action Required">
        <table class="form-table" cellpadding="2" cellspacing="2" width="100%">
            <tr>
                <td style="font-weight: bold;">
                    <asp:HiddenField ID="hdfLocation" runat="server" />
                   <cc1:ucLabel ID="lblACP011Text" runat="server" CssClass="action-required-heading"
                        Text="The mediator has disagreed with the vendor's query. Please create a debit for the required amount and enter a comment to close this issue"
                        Style="text-align: center;"></cc1:ucLabel>
                </td>
            </tr>
            <tr>
                <td>
                    <table class="form-table" cellpadding="0" cellspacing="0" width="98%">
                        <tr>
                            <td>
                                <cc1:ucLabel ID="lblACP009Text1" runat="server" Text="Expected Debit Amount"></cc1:ucLabel>
                            </td>
                            <td>
                                <cc1:ucTextbox ID="txtExpectedDebit" runat="server" Enabled="false"></cc1:ucTextbox>
                            </td>
                        </tr>
            
                            
                      
                        <tr>
                            <td>
                                <cc1:ucLabel ID="lblCurrencyDebit" runat="server"  Text="Currency"></cc1:ucLabel>
                            </td>
                            <td style="text-align: left">
                                <cc1:ucDropdownList ID="ddlCurrencyDebit" runat="server" Width="150px">
                                </cc1:ucDropdownList>                               
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <cc1:ucLabel ID="lblACP009Text2" runat="server" Text="Amount to be debited"></cc1:ucLabel>
                            </td>
                            <td>
                                <cc1:ucTextbox ID="txtAmountDebit" runat="server" Enabled="true" Width="144px"></cc1:ucTextbox>
                            </td>
                        </tr>
                                    <tr>
                             <td width="19%">
                                <cc1:ucLabel ID="lblVendorVATReference"  Text="Vendor VAT Reference"
                                    runat="server"></cc1:ucLabel>
                            </td>                           
                            <td width="30%" style="text-align: left">
                                <cc1:ucTextbox ID="txtVendVendorVATReference" runat="server" Width="143px"></cc1:ucTextbox>                                 
                            </td>                                         
                        </tr>
                        <tr>
                            <td colspan="2">
                                <cc1:ucLabel ID="lblAPDocumentNumber" runat="server" Text="AP Document Number"></cc1:ucLabel>
                                &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;<cc1:ucTextbox ID="txtAPDocumentNumber" runat="server" onkeyup="checkTextLengthOnKeyUp(this,1000);"
                                    Width="241px"></cc1:ucTextbox>
                            </td>
                        </tr>
                        <tr>
                            <td width="15%">
                                <cc1:ucLabel ID="lblComment" runat="server" Text="Please Enter Comments"></cc1:ucLabel>
                            </td>
                            <td width="85%">
                                :
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <cc1:ucTextbox ID="txtComment" runat="server" onkeyup="checkTextLengthOnKeyUp(this,1000);"
                                    TextMode="MultiLine" CssClass="textarea" Width="98%"></cc1:ucTextbox>
                                <asp:RequiredFieldValidator ID="rfvCommentRequired" runat="server" Text="Please enter comments."
                                    ControlToValidate="txtComment" Display="None" ValidationGroup="Save"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>           <td>
                                        <cc1:ucPanel ID="pnlCommunicationDetail" runat="server" GroupingText="Communication Detail"
                                            CssClass="fieldset-form" Visible="false">
                                            <table width="100%" cellspacing="0" cellpadding="0" class="form-table" height="140px">
                                                <tr>
                                                    <td>
                                                        <uc2:ucSDRCommunication ID="ucSDRCommunication1" runat="server" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </cc1:ucPanel>
                                    </td>
                                </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table cellpadding="0" cellspacing="0" class="form-table" width="98%">
                        <tr>
                            <td width="14%" style="display: none;">
                                <cc1:ucLabel ID="lblStageComplete" runat="server" Text="Stage Complete"></cc1:ucLabel>
                                <input type="hidden" runat="server" id="hdnValueCredit" value="1" />
                                <asp:ValidationSummary ID="VSSave" runat="server" ValidationGroup="Save" ShowSummary="false"
                                    ShowMessageBox="true" />
                            </td>
                            <td width="1%" style="display: none;">
                                :
                            </td>
                            <td align="left" width="65%" style="display: none;">
                                <cc1:ucCheckbox ID="chkStageComplete" runat="server" Checked="true" onclick="return StageCompleted();" />
                            </td>
                            <td width="20%" style="display: none;">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" style="display: none;">
                                <cc1:ucLabel ID="lblDiscrepancyNote" runat="server" Text="If  you required to tag a comment without closing the discrepancy&lt;br/&gt;1 -    Deselect Stage Complete&lt;br/&gt;2 -   Enter  the comment&lt;br/&gt;3 -   Save"></cc1:ucLabel>
                            </td>
                            <td align="right" valign="bottom">
                                <cc1:ucButton ID="btnSave" runat="server" Text="Save" class="button" ValidationGroup="Save"
                                    OnClick="btnSave_Click" OnClientClick="javascript:GetLocation()" />
                                <cc1:ucButton ID="btnBack" runat="server" Text="Back" class="button" OnClick="btnBack_Click" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </cc1:ucPanel>
    </form>
</body>
</html>

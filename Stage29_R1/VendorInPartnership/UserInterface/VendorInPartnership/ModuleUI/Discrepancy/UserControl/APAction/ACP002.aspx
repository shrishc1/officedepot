﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ACP002.aspx.cs" Inherits="APAction_ACP002" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="../ucSDRCommunicationAPListing.ascx" TagName="ucSDRCommunication"
    TagPrefix="uc2" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Office Depot</title>
    <link rel="stylesheet" type="text/css" href="../../../../Css/style.css" />
    <link type="text/css" href="../../../../Css/ui-lightness/jquery-ui-1.8.5.custom.css"
        rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="../../../../Css/superfish.css" media="screen" />
    <script type="text/javascript" src="<%= ResolveClientUrl("../../../../Scripts/jquery-1.2.6.min.js")%>"></script>
    <script type="text/javascript" src="<%= ResolveClientUrl("../../../../Scripts/jquery-1.4.2.min.js")%>"></script>
    <script type="text/javascript" src="<%= ResolveClientUrl("../../../../Scripts/jquery-ui-1.8.5.custom.min.js")%>"></script>
    <script type="text/javascript" src="<%= ResolveClientUrl("../../../../Scripts/hoverIntent.js")%>"></script>
    <script type="text/javascript" src="<%= ResolveClientUrl("../../../../Scripts/superfish.js")%>"></script>
    <script type="text/javascript" src="<%= ResolveClientUrl("../../../../Scripts/ValidateInputs.js")%>"></script>
    <script type="text/javascript">

        function TogglePanelControl() {
            //Panel Controls
            var pnlCredit = document.getElementById('<%=pnlACP002c.ClientID %>');
            var pnlDebit = document.getElementById('<%=pnlACP002d.ClientID %>');
            //Debit Panel Controls
            var objrdoDebitCreditNote = document.getElementById('<%=rdoACP002dCredit.ClientID %>');
            var objrdoDebitDebitNote = document.getElementById('<%=rdoACP002dDebit.ClientID %>');
            //Credit Panel Controls
            var objrdoCreditCreditNote = document.getElementById('<%=rdoACP002cCredit.ClientID %>');
            var objrdoCreditDebitNote = document.getElementById('<%=rdoACP002cDebit.ClientID %>');


            if (objrdoDebitCreditNote.checked) {

                pnlCredit.style.display = "block";
                pnlDebit.style.display = "none";
                document.getElementById('<%=rdoACP002cCredit.ClientID %>').checked = true;
            }
            else if (objrdoDebitDebitNote.checked) {

                pnlCredit.style.display = "none";
                pnlDebit.style.display = "block";
                document.getElementById('<%=rdoACP002dDebit.ClientID %>').checked = true;
            }

            if (objrdoCreditCreditNote.checked) {

                pnlCredit.style.display = "block";
                pnlDebit.style.display = "none";
                document.getElementById('<%=rdoACP002cCredit.ClientID %>').checked = true;

            }
            else if (objrdoCreditDebitNote.checked) {

                pnlCredit.style.display = "none";
                pnlDebit.style.display = "block";
                document.getElementById('<%=rdoACP002dDebit.ClientID %>').checked = true;

            }
        }

        function StageCompleted() {
            var objStageCompletedDebit = document.getElementById('<%=chkDebitStageComplete.ClientID %>');
            var objDebitActualValueRequired = document.getElementById('<%=rfvDebitActualValueReq.ClientID %>');

            var objStageCompletedCredit = document.getElementById('<%=chkCreditStageComplete.ClientID %>');
            var objCreditNoteNumberRequired = document.getElementById('<%=rfvCreditNoteNumberReq.ClientID %>');
            var objCreditActualValueRequired = document.getElementById('<%=rfvCreditActualValueReq.ClientID %>');

            if (document.getElementById('<%=hdnValueDebit.ClientID %>').value == '1') {
                ValidatorEnable(objDebitActualValueRequired, false);
                objStageCompletedDebit.checked = false;
                document.getElementById('<%=hdnValueDebit.ClientID %>').value = '0';

            }
            else if (document.getElementById('<%=hdnValueDebit.ClientID %>').value == '0') {
                ValidatorEnable(objDebitActualValueRequired, true);
                objStageCompletedDebit.checked = true;
                document.getElementById('<%=hdnValueDebit.ClientID %>').value = '1';
            }

            if (document.getElementById('<%=hdnValueCredit.ClientID %>').value == '1') {

                ValidatorEnable(objCreditNoteNumberRequired, false);
                ValidatorEnable(objCreditActualValueRequired, false);
                objStageCompletedCredit.checked = false;
                document.getElementById('<%=hdnValueCredit.ClientID %>').value = '0';

            }
            else if (document.getElementById('<%=hdnValueCredit.ClientID %>').value == '0') {

                ValidatorEnable(objCreditNoteNumberRequired, true);
                ValidatorEnable(objCreditActualValueRequired, true);
                objStageCompletedCredit.checked = true;
                document.getElementById('<%=hdnValueCredit.ClientID %>').value = '1';
            }
        }
        function GetLocation(id) {
            var Location = parent.document.getElementById('ctl00_ContentPlaceHolder1_txtLocation').value;
            document.getElementById('<%=hdfLocation.ClientID%>').value = Location;
            if (checkValidationGroup("DebitSave") || checkValidationGroup("CreditSave")) {
                id.style.visibility = "hidden";
            }
            else {
                id.style.visibility = "visible";
                return false;
            }

        }

//        function AssignCostCenterDebitAndCredit() {
//            var CostCenter = parent.document.getElementById('ctl00_ContentPlaceHolder1_hdnCostCenter');
//            document.getElementById('<%=txtCostCenter.ClientID%>').value = CostCenter.value; // Credit section
//            document.getElementById('<%=txtCostCenter1.ClientID%>').value = CostCenter.value; // Debit section

//        }
    </script>
</head>
<body>
    <form id="form2" runat="server" method="post">
    <cc1:ucPanel ID="pnlACP002c" runat="server" Width="100%" CssClass="fieldset-form"
        GroupingText="Action Required">
        <table class="form-table" cellpadding="5" cellspacing="0" width="96%">
            <tr>
                <td style="font-weight: bold;"><asp:HiddenField ID="hdfLocation" runat="server" />
                    <cc1:ucLabel ID="lblACP002a" runat="server" CssClass="action-required-heading" Style="text-align: center;"
                        Text="Please credit the account for the amount to cover carriage costs "></cc1:ucLabel>
                </td>
            </tr>
            <tr>
                <td>
                    <table class="form-table" cellpadding="0" cellspacing="0" width="98%" style="margin: 2px;">
                        <tr>
                            <td width="10%">
                                <cc1:ucRadioButton ID="rdoACP002cCredit" runat="server" GroupName="NoteName" OnClick="TogglePanelControl()"
                                    Text="Credit" />
                            </td>
                            <td>
                                <cc1:ucRadioButton ID="rdoACP002cDebit" runat="server" GroupName="NoteName" OnClick="TogglePanelControl()"
                                    Text="Debit" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table class="form-table" cellpadding="0" cellspacing="0" width="98%" style="margin: 2px;">
                        <tr>
                            <td width="30%">
                                <cc1:ucLabel ID="lblCreditNoteNumberD" isRequired="true" runat="server" Text="Please Enter Credit Note #"></cc1:ucLabel>
                            </td>
                            <td width="1%">
                                :
                            </td>
                            <td style="text-align: left">
                                <cc1:ucTextbox ID="txtCreditNoteNumber" runat="server" MaxLength="10"></cc1:ucTextbox>
                                <asp:RequiredFieldValidator ID="rfvCreditNoteNumberReq" runat="server" Text="Please enter Credit Note Number."
                                    ControlToValidate="txtCreditNoteNumber" Display="None" ValidationGroup="CreditSave"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <cc1:ucLabel ID="lblCarriageCost" runat="server" Text="Carriage Cost"></cc1:ucLabel>
                            </td>
                            <td>
                                :
                            </td>
                            <td>
                                <cc1:ucTextbox ID="txtCreditCarriageCost" runat="server" ReadOnly="true" Enabled="false"></cc1:ucTextbox>
                                <%--<cc1:ucDropdownList ID="drpRate" runat="server" Width="60px">
                        <asp:ListItem Text="EURO" Value="1"></asp:ListItem>
                        <asp:ListItem Text="GBP" Value="2"></asp:ListItem>
                    </cc1:ucDropdownList>--%>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <cc1:ucLabel ID="lblValueActual" runat="server" isRequired="true" Text="Actual Value to be credited"></cc1:ucLabel>
                            </td>
                            <td>
                                :
                            </td>
                            <td>
                                <cc1:ucTextbox ID="txtCreditValueActual" runat="server" MaxLength="7"></cc1:ucTextbox>
                                <%--<cc1:ucDropdownList ID="UcDropdownList1" runat="server" Width="60px">
                                                       <asp:ListItem Text="EURO" Value="1"></asp:ListItem>
                                                       <asp:ListItem Text="GBP" Value="2"></asp:ListItem>
                                                        </cc1:ucDropdownList>--%>
                                <asp:RequiredFieldValidator ID="rfvCreditActualValueReq" runat="server" Text="Please enter Actual Value to be Credited."
                                    ControlToValidate="txtCreditValueActual" Display="None" ValidationGroup="CreditSave"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="revCreditActualValueRE" ValidationExpression="^\d{0,8}(\.\d{0,2})?$"
                                    runat="server" ControlToValidate="txtCreditValueActual" Display="None" ValidationGroup="CreditSave"></asp:RegularExpressionValidator>
                            </td>
                        </tr>
                         <tr>
                            <td>
                                <cc1:ucLabel ID="lblCostCenter" runat="server" Text="Cost Centre" isRequired="true"></cc1:ucLabel>
                            </td>
                            <td width="1%">
                                :
                            </td>
                            <td>
                                <cc1:ucTextbox ID="txtCostCenter" runat="server"></cc1:ucTextbox>
                                <asp:RequiredFieldValidator ID="rfvPleaseEnterCostCenterCredit" runat="server"
                                    Text="Please enter Cost Center" ControlToValidate="txtCostCenter"
                                    Display="None" ValidationGroup="CreditSave"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <cc1:ucLabel ID="lblComment" runat="server" isRequired="true" Text="Please Enter Comments"></cc1:ucLabel>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <cc1:ucTextbox ID="txtCreditComment" runat="server" onkeyup="checkTextLengthOnKeyUp(this,1000);"
                                    TextMode="MultiLine" CssClass="textarea" Width="98%" Height="50px"></cc1:ucTextbox>
                                <asp:RequiredFieldValidator ID="rfvCreditCommentRequired" runat="server" Text="Please enter comments."
                                    ControlToValidate="txtCreditComment" Display="None" ValidationGroup="CreditSave"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table cellpadding="0" cellspacing="0" class="form-table" style="margin: 2px;" width="98%">
                        <tr>
                            <td width="25%" style="display:none;">
                                <cc1:ucLabel ID="lblStageComplete" runat="server" Text="Stage Complete"></cc1:ucLabel>
                                <input type="hidden" runat="server" id="hdnValueCredit" value="1" />
                                <asp:ValidationSummary ID="vs1" runat="server" ShowMessageBox="true" ShowSummary="false"
                                    ValidationGroup="CreditSave" />
                            </td>
                            <td width="1%" style="display:none;">
                                :
                            </td>
                            <td align="left" width="54%" style="display:none;">
                                <cc1:ucCheckbox ID="chkCreditStageComplete" runat="server" Checked="true" onclick="return StageCompleted();" />
                            </td>
                            <td width="20%" style="display:none;">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" style="display:none;">
                                <cc1:ucLabel ID="lblDiscrepancyNote" runat="server" Text="If  you required to tag a comment without closing the discrepancy&lt;br/&gt;1 -    Deselect Stage Complete&lt;br/&gt;2 -   Enter  the comment&lt;br/&gt;3 -   Save"></cc1:ucLabel>
                            </td>
                            <td align="right" valign="bottom">
                                <cc1:ucButton ID="btnCreditSave" runat="server" Text="Save" class="button" ValidationGroup="CreditSave"
                                    OnClick="btnCreditSave_Click" OnClientClick="javascript:GetLocation(this)" />
                                <cc1:ucButton ID="btnBack" runat="server" Text="Back" class="button" OnClick="btnBack_Click" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </cc1:ucPanel>
    <cc1:ucPanel ID="pnlACP002d" runat="server" Width="100%" CssClass="fieldset-form"
        Style="display: none" GroupingText="Action Required">
        <table class="form-table" cellpadding="5" cellspacing="0" width="96%">
            <tr>
                <td style="font-weight: bold;">
                    <cc1:ucLabel ID="lblACP002b" runat="server" CssClass="action-required-heading" Style="text-align: center;"
                        Text="Please debit the account for the amount to cover carriage costs "></cc1:ucLabel>
                </td>
            </tr>
            <tr>
                <td>
                    <table class="form-table" cellpadding="0" cellspacing="0" width="98%" style="margin: 2px;">
                        <tr>
                            <td width="10%">
                                <cc1:ucRadioButton ID="rdoACP002dCredit" runat="server" GroupName="NoteName" OnClick="TogglePanelControl()"
                                    Text="Credit" />
                            </td>
                            <td>
                                <cc1:ucRadioButton ID="rdoACP002dDebit" runat="server" GroupName="NoteName" OnClick="TogglePanelControl()"
                                    Text="Debit" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table class="form-table" cellpadding="0" cellspacing="0" width="98%" style="margin: 2px;">
                        <%--<tr>
                            <td width="25%">
                                <cc1:ucLabel ID="lblDebitNote" runat="server" Text="Please Enter Debit Note Number"></cc1:ucLabel>
                            </td>
                            <td width="1%">
                                :
                            </td>
                            <td width="74%">
                                <cc1:ucTextbox ID="txtDebitNoteNumber" runat="server" MaxLength="10"></cc1:ucTextbox>
                                <asp:RequiredFieldValidator ID="rfvDebitNoteNumberReq" runat="server" Text="Please enter Debit Note Number."
                                    ControlToValidate="txtDebitNoteNumber" Display="None" ValidationGroup="DebitSave"></asp:RequiredFieldValidator>
                            </td>
                        </tr>--%>
                        <tr>
                             <td width="19%">
                                <cc1:ucLabel ID="lblVendorVATReference" isRequired="true" Text="Vendor VAT Reference"
                                    runat="server"></cc1:ucLabel>
                            </td>
                           <td width="1%">
                                :
                            </td>
                            <td width="30%" style="text-align: left">
                                <cc1:ucTextbox ID="txtVendVendorVATReference" runat="server" Width="143px"></cc1:ucTextbox>
                                 <asp:RequiredFieldValidator ID="rfvPleaseentertheVendorVATReference" runat="server"
                                    Text="Please enter the Vendor VAT Reference" ControlToValidate="txtVendVendorVATReference"
                                    Display="None" ValidationGroup="DebitSave"></asp:RequiredFieldValidator>
                            </td>                       
                             <td width="19%">
                                <cc1:ucLabel ID="lblOfficeDepotVATReference" isRequired="true" Text="Office Depot VAT Reference"
                                    runat="server"></cc1:ucLabel>
                            </td>
                           <td width="1%">
                                :
                            </td>
                            <td width="30%" style="text-align: left">
                                <%--<cc1:ucDropdownList ID="ddlVendOfficeDepotVATReference" runat="server" Width="150px">
                                </cc1:ucDropdownList>--%>
                                <cc1:ucLabel ID="lblOfficeDepotVATReferenceValue" runat="server"></cc1:ucLabel>
                                <%--<asp:RequiredFieldValidator ID="rfvPleaseselecttheOfficeDepotVATReference" runat="server" Text="Please select the Office Depot VAT Reference"
                                    ControlToValidate="ddlVendOfficeDepotVATReference" InitialValue="0" Display="None" ValidationGroup="DebitSave"></asp:RequiredFieldValidator>--%>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <cc1:ucLabel ID="lblReasonForDebit" runat="server" isRequired="true" Text="Reason For Debit"></cc1:ucLabel>
                            </td>
                            <td>
                                :
                            </td>
                            <td style="text-align: left">
                                <cc1:ucDropdownList ID="ddlReasonDebit" runat="server" Width="150px">
                                </cc1:ucDropdownList>
                                <asp:RequiredFieldValidator ID="rfvReasonMandatry" runat="server" Text="Please select the Reason."
                                    ControlToValidate="ddlReasonDebit" InitialValue="0" Display="None" ValidationGroup="DebitSave"></asp:RequiredFieldValidator>
                            </td>                        
                            <td>
                                <cc1:ucLabel ID="lblCarriageCost2" runat="server" Text="Carriage Cost"></cc1:ucLabel>
                            </td>
                            <td>
                                :
                            </td>
                            <td>
                                <cc1:ucTextbox ID="txtDebitCarriageCost" runat="server" ReadOnly="true" Enabled="false"></cc1:ucTextbox>
                                <%--<cc1:ucDropdownList ID="UcDropdownList2" runat="server" Width="60px">
                                                       <asp:ListItem Text="EURO" Value="1"></asp:ListItem>
                                                       <asp:ListItem Text="GBP" Value="2"></asp:ListItem>
                                                        </cc1:ucDropdownList>--%>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <cc1:ucLabel ID="lblValueActual1" runat="server" isRequired="true" Text="Actual Value to be Debited"></cc1:ucLabel>
                            </td>
                            <td>
                                :
                            </td>
                            <td>
                                <cc1:ucTextbox ID="txtDebitValueActual" runat="server" MaxLength="7"></cc1:ucTextbox>
                                <asp:RequiredFieldValidator ID="rfvDebitActualValueReq" runat="server" Text="Please enter actual value to be debited."
                                    ControlToValidate="txtDebitValueActual" Display="None" ValidationGroup="DebitSave"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="revDebitActualValueRE" ValidationExpression="^\d{0,8}(\.\d{0,2})?$"
                                    runat="server" ControlToValidate="txtDebitValueActual" Display="None" ValidationGroup="DebitSave"></asp:RegularExpressionValidator>
                            </td>                       
                            <td>
                                <cc1:ucLabel ID="lblCurrencyDebit" runat="server" isRequired="true" Text="Currency"></cc1:ucLabel>
                            </td>
                            <td>
                                :
                            </td>
                            <td style="text-align: left">
                                <cc1:ucDropdownList ID="ddlCurrencyDebit" runat="server" Width="150px">
                                </cc1:ucDropdownList>
                                <%--<asp:RequiredFieldValidator ID="rfvCurrencyMandatry" runat="server" Text="Please select the currency"
                                    ControlToValidate="ddlCurrencyDebit" InitialValue="0" Display="None" ValidationGroup="DebitSave"></asp:RequiredFieldValidator>--%>
                            </td>
                        </tr>
                        <tr>
                         <td>
                                <cc1:ucLabel ID="lblCostCenter1" runat="server" Text="Cost Centre" isRequired="true"></cc1:ucLabel>
                            </td>
                            <td width="1%">
                                :
                            </td>
                            <td>
                                <cc1:ucTextbox ID="txtCostCenter1" runat="server"></cc1:ucTextbox>
                                 <asp:RequiredFieldValidator ID="rfvPleaseEnterCostCenterDebit" runat="server"
                                    Text="Please enter the Cost Center" ControlToValidate="txtCostCenter1"
                                    Display="None" ValidationGroup="DebitSave"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6">
                                <cc1:ucPanel ID="pnlCommunicationDetail" runat="server" GroupingText="Communication Detail"
                                    CssClass="fieldset-form" Visible="false">
                                    <table width="100%" cellspacing="0" cellpadding="0" class="form-table">
                                        <tr>
                                            <td>
                                                <uc2:ucSDRCommunication ID="ucSDRCommunicationDebit1" runat="server" />
                                            </td>
                                        </tr>
                                    </table>
                                </cc1:ucPanel>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6">
                                <cc1:ucLabel ID="lblComments1" runat="server" isRequired="true" Text="Please Enter Comments"></cc1:ucLabel>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6">
                                <cc1:ucTextbox ID="txtDebitComment" runat="server" onkeyup="checkTextLengthOnKeyUp(this,1000);"
                                    TextMode="MultiLine" CssClass="textarea" Width="98%" Height="50px"></cc1:ucTextbox>
                                <asp:RequiredFieldValidator ID="rfvDebitCommentReq" runat="server" Text="Please enter Comments."
                                    ControlToValidate="txtDebitComment" Display="None" ValidationGroup="DebitSave"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table cellpadding="0" cellspacing="0" class="form-table" style="margin: 2px;" width="98%">
                        <tr>
                            <td width="25%" style="display:none;">
                                <cc1:ucLabel ID="lblStageComplete1" runat="server" Text="Stage Complete"></cc1:ucLabel>
                                <input type="hidden" runat="server" id="hdnValueDebit" value="1" />
                                <asp:ValidationSummary ID="VSDebitSave" runat="server" ShowMessageBox="true" ShowSummary="false"
                                    ValidationGroup="DebitSave" />
                            </td>
                            <td width="1%" style="display:none;">
                                :
                            </td>
                            <td width="54%" align="left" style="display:none;">
                                <cc1:ucCheckbox ID="chkDebitStageComplete" runat="server" Checked="true" onclick="return StageCompleted();" />
                            </td>
                            <td width="20%" style="display:none;">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" style="display:none;">
                                <cc1:ucLabel ID="lblDiscrepancyNote_1" runat="server" Text="If  you required to tag a comment without closing the discrepancy&lt;br/&gt;1 -    Deselect Stage Complete&lt;br/&gt;2 -   Enter  the comment&lt;br/&gt;3 -   Save"></cc1:ucLabel>
                            </td>
                            <td align="right" valign="bottom">
                                <cc1:ucButton ID="btnDebitSave" runat="server" Text="Save" class="button" ValidationGroup="DebitSave"
                                    OnClick="btnDebitSave_Click" OnClientClick="javascript:GetLocation(this)"  />
                                <cc1:ucButton ID="btnBack_1" runat="server" Text="Back" class="button" OnClick="btnBack_Click" />
                            </td>
                        </tr>                       
                    </table>
                </td>
            </tr>
        </table>
    </cc1:ucPanel>
    </form>
</body>
</html>

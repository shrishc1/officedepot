﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using BusinessEntities.ModuleBE.Security;
using BusinessLogicLayer.ModuleBAL.Security;
using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using BusinessLogicLayer.ModuleBAL.Appointment.SiteSettings;
using BaseControlLibrary;
using BusinessLogicLayer.ModuleBAL.AdminFunctions;
using BusinessEntities.ModuleBE.AdminFunctions;
using System.Linq;

public partial class InvoiceQueryucSDRCommunication : System.Web.UI.UserControl
{
    public int VendorID
    {
        get;
        set;
    }

    public int SiteID
    {
        get;
        set;
    }

    public string VendorEmailList
    {
        get
        {
            if (!string.IsNullOrWhiteSpace(txtAltEmail.Text))
            {
                return txtAltEmail.Text;
            }
            return ucVendorEmailList.Text;
        }
        set
        {
            ucVendorEmailList.Text = value;
            hdnEmail.Value = value;
        }
    }

    public char innerControlRdoCommunication
    {
        get
        {
            char selection;
            if (rdoEmailComm.Checked)
                selection = 'E';
            else if (rdoLetter.Checked)
                selection = 'L';
            else
                selection = 'F';

            return selection;
        }
        set
        {
            switch (value)
            {
                case 'E':
                    rdoEmailComm.Checked = true;
                    ucVendorEmailList.Text = hdnEmail.Value;
                    return;
                case 'L':
                    rdoLetter.Checked = true;
                    ucVendorEmailList.Text = "";
                    return;
                //case 'F':
                //    rdoFax.Checked = true;
                //    return;
                default:
                    return;

            }
        }
    }

    public ucTextbox innerControlEmailList
    {
        get
        {
            return this.ucVendorEmailList;
        }
    }

    public ucTextbox innerControlAltEmailList
    {
        get
        {
            return this.txtAltEmail;
        }
    }

    public ucRadioButton innerControlRdoLetter
    {
        get
        {
            return rdoLetter;
        }
    }

    public ucRadioButton innerControlRdoEmail
    {
        get
        {
            return rdoEmailComm;
        }
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

        }
    }

    protected void rdoEmailComm_CheckedChanged(object sender, EventArgs e)
    {
        if (rdoEmailComm.Checked)
            ucVendorEmailList.Text = hdnEmail.Value;

    }
    protected void rdoFax_CheckedChanged(object sender, EventArgs e)
    {
        //if (rdoFax.Checked)
        //{
        //    ucVendorEmailList.Text = hdnFax.Value;
        //}
    }
    protected void rdoLetter_CheckedChanged(object sender, EventArgs e)
    {
        if (rdoLetter.Checked)
            ucVendorEmailList.Text = "";
    }



    public string FillAPContacts(int vendorID)
    {
        if (vendorID > 0)
        {

            List<SCT_UserBE> lstAPContacts = new List<SCT_UserBE>();
            UP_VendorBAL oUP_VendorBAL = new UP_VendorBAL();

            UP_VendorBE oUP_VendorBE = new UP_VendorBE();
            oUP_VendorBE.Action = "GetVendorContact";
            oUP_VendorBE.VendorID = vendorID;
            oUP_VendorBE.User = new SCT_UserBE();

            oUP_VendorBE.User.SchedulingContact = null;
            oUP_VendorBE.User.DiscrepancyContact = null;
            oUP_VendorBE.User.OTIFContact = null;
            oUP_VendorBE.User.ScorecardContact = null;
            oUP_VendorBE.User.InvoiceIssueContact = 'Y';

            lstAPContacts = oUP_VendorBAL.GetVendorContact(oUP_VendorBE);

            //MAS_MaintainVendorPointsContactBAL vendorPointsContactBAL = new MAS_MaintainVendorPointsContactBAL();
            //MAS_MaintainVendorPointsContactBE VendorPointsContact = new MAS_MaintainVendorPointsContactBE();
            //VendorPointsContact.Action = "GetMaintainVendorPointsContact";
            //VendorPointsContact.Vendor = new MAS_VendorBE();
            //VendorPointsContact.Vendor.VendorID = VendorID;
            //var lstAPContacts = vendorPointsContactBAL.GetVendorPointsContactsBAL(VendorPointsContact);
            hdnEmail.Value = "";
            hdnFax.Value = "";
            if (lstAPContacts != null && lstAPContacts.Count > 0)
            {
                foreach (var item in lstAPContacts.Select(x => x.EmailId.ToLower()).Distinct().ToList())
                {
                    hdnEmail.Value += item + ", ";
                }
                hdnEmail.Value = hdnEmail.Value.Trim(new char[] { ',', ' ' });
            }
            else
            {
                if (string.IsNullOrEmpty(hdnEmail.Value))
                {
                    hdnEmail.Value = System.Configuration.ConfigurationManager.AppSettings["DefaultEmailAddress"];
                    ucVendorEmailList.Text = System.Configuration.ConfigurationManager.AppSettings["DefaultEmailAddress"];
                }
            }
        }
        return hdnEmail.Value;
    }
    public void GetDetails()
    {
        if (VendorID > 0)
        {

            List<SCT_UserBE> lstAPContacts = new List<SCT_UserBE>();
            UP_VendorBAL oUP_VendorBAL = new UP_VendorBAL();

            UP_VendorBE oUP_VendorBE = new UP_VendorBE();
            oUP_VendorBE.Action = "GetVendorContact";
            oUP_VendorBE.VendorID = VendorID;
            oUP_VendorBE.User = new SCT_UserBE();

            oUP_VendorBE.User.SchedulingContact = null;
            oUP_VendorBE.User.DiscrepancyContact = null;
            oUP_VendorBE.User.OTIFContact = null;
            oUP_VendorBE.User.ScorecardContact = null;
            oUP_VendorBE.User.InvoiceIssueContact = 'Y';

            lstAPContacts = oUP_VendorBAL.GetVendorContact(oUP_VendorBE);

            //MAS_MaintainVendorPointsContactBAL vendorPointsContactBAL = new MAS_MaintainVendorPointsContactBAL();
            //MAS_MaintainVendorPointsContactBE VendorPointsContact = new MAS_MaintainVendorPointsContactBE();
            //VendorPointsContact.Action = "GetMaintainVendorPointsContact";
            //VendorPointsContact.Vendor = new MAS_VendorBE();
            //VendorPointsContact.Vendor.VendorID = VendorID;
            //var lstAPContacts = vendorPointsContactBAL.GetVendorPointsContactsBAL(VendorPointsContact);
            hdnEmail.Value = "";
            hdnFax.Value = "";
            if (lstAPContacts != null && lstAPContacts.Count > 0)
            {
                foreach (var item in lstAPContacts.Select(x => x.EmailId.ToLower()).Distinct().ToList())
                {
                    hdnEmail.Value += item + ", ";
                }
                hdnEmail.Value = hdnEmail.Value.Trim(new char[] { ',', ' ' });
            }
            else
            {
                if (string.IsNullOrEmpty(hdnEmail.Value))
                {
                    hdnEmail.Value = System.Configuration.ConfigurationManager.AppSettings["DefaultEmailAddress"];
                    ucVendorEmailList.Text = System.Configuration.ConfigurationManager.AppSettings["DefaultEmailAddress"];
                }
            }
        }
    }
}
﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucSDRCommunicationAPListing.ascx.cs"
    Inherits="ucSDRCommunicationAPListing" %>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<table width="100%" cellspacing="0" cellpadding="0" class="form-table">
    <tr>
        <td>
            <%--<asp:HiddenField ID="hdnEmailIds" runat="server" />--%>
        </td>
        <td>
            <cc1:ucLabel ID="lblAddtoCirculation" runat="server" Text="Add to Circulation"></cc1:ucLabel>
        </td>
    </tr>
    <tr>
        <td>
            <cc1:ucTextbox ID="txtVendorEmailList" runat="server" ReadOnly="True" TextMode="MultiLine"                
                Width="95%" Height="50px"></cc1:ucTextbox>
        </td>
        <td>
            <cc1:ucTextbox ID="txtAdditionalEmailList" runat="server" TextMode="MultiLine" Width="95%"
                Height="50px"></cc1:ucTextbox>
        </td>
    </tr>  
</table>

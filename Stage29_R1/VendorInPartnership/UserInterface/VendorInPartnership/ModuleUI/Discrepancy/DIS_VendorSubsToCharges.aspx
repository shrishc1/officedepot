﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="DIS_VendorSubsToCharges.aspx.cs"
    MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master" Inherits="VendorSubsToCharges"
    MaintainScrollPositionOnPostback="true" EnableEventValidation="false" %>

<%@ Register Src="~/CommonUI/UserControls/MultiSelectCountry.ascx" TagName="MultiSelectCountry"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectVendor.ascx" TagName="MultiSelectVendor"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/ucVendorSelectionTemplate.ascx" TagName="VendorSelectTemplate"
    TagPrefix="cc1" %>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/ucExportToExcel.ascx" TagName="ucExportToExcel"
    TagPrefix="cc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript">
        $(document).ready(function () {
            $("body").keypress(function (e) {
                if (e.which == 13 || e.keyCode == 13) {
                    $('#<%=btnGenerateReport.ClientID %>').click();
                    return false;
                }
            });
        });
    </script>
    <h2>
        <cc1:ucLabel ID="lblVendorSubsToCharges" runat="server"></cc1:ucLabel>
    </h2>
    <asp:ScriptManager ID="SM1" runat="server" AsyncPostBackTimeout="2400">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="upd" runat="server">
        <ContentTemplate>
            <div class="right-shadow">
                <div class="formbox">
                    <cc1:ucPanel ID="UcInputPanel" runat="server">
                        <table width="95%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                            <tr>
                                <td align="right">
                                    <cc1:VendorSelectTemplate runat="server" ID="ucVendorTemplateSelect" />
                                </td>
                            </tr>
                        </table>
                        <table width="95%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                            <tr>
                                <td style="font-weight: bold; width: 10%">
                                    <cc1:ucLabel ID="lblCountry" runat="server" Text="Country"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold; width: 1%">
                                    :
                                </td>
                                <td style="font-weight: bold; width: 89%">
                                    <cc1:MultiSelectCountry runat="server" ID="msCountry" />
                                </td>
                            </tr>
                            <tr>
                                <td style="font-weight: bold;">
                                    <cc1:ucLabel ID="lblVendor" runat="server" Text="Vendor"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold;">
                                    :
                                </td>
                                <td>
                                    <cc1:MultiSelectVendor runat="server" ID="msVendor" />
                                </td>
                            </tr>
                            <tr>
                                <td style="font-weight: bold;">
                                    <cc1:ucLabel ID="lblChargeType" runat="server" Text="Charge Type"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold;">
                                    :
                                </td>
                                <td>
                                    <cc1:ucDropdownList ID="ddlChargeType" runat="server">
                                        <asp:ListItem Text="--All--" Value="0"></asp:ListItem>
                                        <asp:ListItem Text="Overs" Value="Overs"></asp:ListItem>
                                        <asp:ListItem Text="Damage" Value="Damage"></asp:ListItem>
                                        <asp:ListItem Text="No PO" Value="No PO"></asp:ListItem>
                                        <asp:ListItem Text="No Paperwork" Value="No Paperwork"></asp:ListItem>
                                        <asp:ListItem Text="Incorrect Product" Value="Incorrect Product"></asp:ListItem>
                                        <asp:ListItem Text="Incorrect Address" Value="Incorrect Address"></asp:ListItem>
                                        <asp:ListItem Text="Failed Pallet Spec" Value="Failed Pallet Spec"></asp:ListItem>
                                        <asp:ListItem Text="Incorrect Pack Carriage" Value="Incorrect Pack Carriage"></asp:ListItem>
                                        <asp:ListItem Text="Incorrect Pack Labour" Value="Incorrect Pack Labour"></asp:ListItem>
                                        <asp:ListItem Text="Premature Invoice" Value="Premature Invoice"></asp:ListItem>
                                        <asp:ListItem Text="Quality Issue Carriage" Value="Quality Issue Carriage"></asp:ListItem>
                                        <asp:ListItem Text="Quality Issue Labour" Value="Quality Issue Labour"></asp:ListItem>
                                    </cc1:ucDropdownList>
                                </td>
                            </tr>
                            <tr>
                                <td style="font-weight: bold;">
                                    <cc1:ucLabel ID="lblSubscribed" runat="server" Text="Subscribed"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold;">
                                    :
                                </td>
                                <td>
                                    <cc1:ucDropdownList ID="ddlSubscribed" runat="server">
                                        <asp:ListItem Text="--Both--" Value="-1"></asp:ListItem>
                                        <asp:ListItem Text="Yes" Value="1"></asp:ListItem>
                                        <asp:ListItem Text="No" Value="0"></asp:ListItem>
                                    </cc1:ucDropdownList>
                                </td>
                            </tr>
                            <tr>
                                <td align="right" colspan="3">
                                    <div class="button-row">
                                        <cc1:ucButton ID="btnGenerateReport" runat="server" Text="Generate Report" CssClass="button"
                                            OnClick="btnGenerateReport_Click" />
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </cc1:ucPanel>
                    <cc1:ucPanel ID="UcResultPanel" runat="server" Visible="false">
                        <table width="95%" cellspacing="5" cellpadding="0" border="0" align="left" class="top-settingsnoborder">
                            <tr>
                                <td align="left" width="70%">
                                    <cc1:ucLabel ID="lblVendorSubsInfo" runat="server" Font-Bold="true"></cc1:ucLabel>
                                </td>
                                <td align="right">
                                    <cc1:ucButton ID="btnExportToExcel" runat="server" OnClick="btnExport_Click" CssClass="exporttoexcel"
                                        Width="109px" Height="20px" />
                                </td>
                            </tr>
                        </table>
                        
                            <cc1:ucGridView ID="grdSubs" Width="100%" runat="server" CssClass="grid" OnRowCommand="grdSubs_RowCommand"
                                PageSize="50" AllowPaging="true" OnPageIndexChanging="grdSubs_PageIndexChanging" style="display:block;">
                                <Columns>
                                    <asp:BoundField HeaderText="Vendor" DataField="Vendor_Name">
                                        <HeaderStyle HorizontalAlign="Left" Width="10%" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="Country" DataField="Vendor_Country">
                                        <HeaderStyle HorizontalAlign="Left" Width="5%" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:BoundField>
                                    <asp:TemplateField HeaderText="Overs">
                                        <HeaderStyle HorizontalAlign="Center" Width="3%" />
                                        <ItemStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <cc1:ucCheckbox ID="Overs" runat="server" Checked='<%# Eval("Overs")%>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Damage">
                                        <HeaderStyle HorizontalAlign="Center" Width="3%" />
                                        <ItemStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <cc1:ucCheckbox ID="Damage" runat="server" Checked='<%# Eval("Damage")%>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="No PO">
                                        <HeaderStyle HorizontalAlign="Center" Width="3%" />
                                        <ItemStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <cc1:ucCheckbox ID="NoPO" runat="server" Checked='<%# Eval("NoPO")%>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="No Paperwork">
                                        <HeaderStyle HorizontalAlign="Center" Width="3%" />
                                        <ItemStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <cc1:ucCheckbox ID="NoPaperwork" runat="server" Checked='<%# Eval("NoPaperwork")%>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Incorrect Product">
                                        <HeaderStyle HorizontalAlign="Center" Width="3%" />
                                        <ItemStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <cc1:ucCheckbox ID="IncorrectProduct" runat="server" Checked='<%# Eval("IncorrectProduct")%>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Incorrect Address">
                                        <HeaderStyle HorizontalAlign="Center" Width="3%" />
                                        <ItemStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <cc1:ucCheckbox ID="IncorrectAddress" runat="server" Checked='<%# Eval("IncorrectAddress")%>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Failed Pallet Spec">
                                        <HeaderStyle HorizontalAlign="Center" Width="3%" />
                                        <ItemStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <cc1:ucCheckbox ID="FailedPalletSpec" runat="server" Checked='<%# Eval("FailedPalletSpec")%>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Incorrect Pack Carriage">
                                        <HeaderStyle HorizontalAlign="Center" Width="3%" />
                                        <ItemStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <cc1:ucCheckbox ID="IncorrectPackCarriage" runat="server" Checked='<%# Eval("IncorrectPackCarriage")%>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Incorrect Pack Labour">
                                        <HeaderStyle HorizontalAlign="Center" Width="3%" />
                                        <ItemStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <cc1:ucCheckbox ID="IncorrectPackLabour" runat="server" Checked='<%# Eval("IncorrectPackLabour")%>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Premature Invoice">
                                        <HeaderStyle HorizontalAlign="Center" Width="3%" />
                                        <ItemStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <cc1:ucCheckbox ID="PrematureInvoice" runat="server" Checked='<%# Eval("PrematureInvoice")%>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Quality Issue Carriage">
                                        <HeaderStyle HorizontalAlign="Center" Width="3%" />
                                        <ItemStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <cc1:ucCheckbox ID="QualityIssueCarriage" runat="server" Checked='<%# Eval("QualityIssueCarriage")%>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Quality Issue Labour">
                                        <HeaderStyle HorizontalAlign="Center" Width="3%" />
                                        <ItemStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <cc1:ucCheckbox ID="QualityIssueLabour" runat="server" Checked='<%# Eval("QualityIssueLabour")%>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Action" ItemStyle-Width="4%" ItemStyle-HorizontalAlign="Left"
                                        HeaderStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <table cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td>
                                                        <cc1:ucButton runat="server" CssClass="button" ID="btnUpdate" Text="Update" CommandArgument='<%#Eval("VendorID")%>'
                                                            CommandName="SAVE" />
                                                    </td>
                                                    <td>
                                                        <asp:Image ID="imgUpdate" runat="server" Width="9" ImageUrl="~/Images/tick.png" Visible="false" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </cc1:ucGridView>
                        
                        <table border="1" cellspacing="0" cellpadding="0" class="form-table" id="tblExport"
                            runat="server" style="display: none;">
                            <tr>
                                <td>
                                    <cc1:ucGridView ID="grdSubsExport" Width="100%" runat="server">
                                        <Columns>
                                            <asp:BoundField HeaderText="Vendor" DataField="Vendor_Name">
                                                <HeaderStyle HorizontalAlign="Left" Width="10%" />
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:BoundField>
                                            <asp:BoundField HeaderText="Country" DataField="Vendor_Country">
                                                <HeaderStyle HorizontalAlign="Left" Width="5%" />
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:BoundField>
                                            <asp:TemplateField HeaderText="Overs">
                                                <HeaderStyle HorizontalAlign="Center" Width="3%" />
                                                <ItemStyle HorizontalAlign="Center" />
                                                <ItemTemplate>
                                                    <cc1:ucCheckbox ID="Overs" runat="server" Checked='<%# Eval("Overs")%>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Damage">
                                                <HeaderStyle HorizontalAlign="Center" Width="3%" />
                                                <ItemStyle HorizontalAlign="Center" />
                                                <ItemTemplate>
                                                    <cc1:ucCheckbox ID="Damage" runat="server" Checked='<%# Eval("Damage")%>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="No PO">
                                                <HeaderStyle HorizontalAlign="Center" Width="3%" />
                                                <ItemStyle HorizontalAlign="Center" />
                                                <ItemTemplate>
                                                    <cc1:ucCheckbox ID="NoPO" runat="server" Checked='<%# Eval("NoPO")%>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="No Paperwork">
                                                <HeaderStyle HorizontalAlign="Center" Width="3%" />
                                                <ItemStyle HorizontalAlign="Center" />
                                                <ItemTemplate>
                                                    <cc1:ucCheckbox ID="NoPaperwork" runat="server" Checked='<%# Eval("NoPaperwork")%>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Incorrect Product">
                                                <HeaderStyle HorizontalAlign="Center" Width="3%" />
                                                <ItemStyle HorizontalAlign="Center" />
                                                <ItemTemplate>
                                                    <cc1:ucCheckbox ID="IncorrectProduct" runat="server" Checked='<%# Eval("IncorrectProduct")%>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Incorrect Address">
                                                <HeaderStyle HorizontalAlign="Center" Width="3%" />
                                                <ItemStyle HorizontalAlign="Center" />
                                                <ItemTemplate>
                                                    <cc1:ucCheckbox ID="IncorrectAddress" runat="server" Checked='<%# Eval("IncorrectAddress")%>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Failed Pallet Spec">
                                                <HeaderStyle HorizontalAlign="Center" Width="3%" />
                                                <ItemStyle HorizontalAlign="Center" />
                                                <ItemTemplate>
                                                    <cc1:ucCheckbox ID="FailedPalletSpec" runat="server" Checked='<%# Eval("FailedPalletSpec")%>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Incorrect Pack Carriage">
                                                <HeaderStyle HorizontalAlign="Center" Width="3%" />
                                                <ItemStyle HorizontalAlign="Center" />
                                                <ItemTemplate>
                                                    <cc1:ucCheckbox ID="IncorrectPackCarriage" runat="server" Checked='<%# Eval("IncorrectPackCarriage")%>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Incorrect Pack Labour">
                                                <HeaderStyle HorizontalAlign="Center" Width="3%" />
                                                <ItemStyle HorizontalAlign="Center" />
                                                <ItemTemplate>
                                                    <cc1:ucCheckbox ID="IncorrectPackLabour" runat="server" Checked='<%# Eval("IncorrectPackLabour")%>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Premature Invoice">
                                                <HeaderStyle HorizontalAlign="Center" Width="3%" />
                                                <ItemStyle HorizontalAlign="Center" />
                                                <ItemTemplate>
                                                    <cc1:ucCheckbox ID="PrematureInvoice" runat="server" Checked='<%# Eval("PrematureInvoice")%>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Quality Issue Carriage">
                                                <HeaderStyle HorizontalAlign="Center" Width="3%" />
                                                <ItemStyle HorizontalAlign="Center" />
                                                <ItemTemplate>
                                                    <cc1:ucCheckbox ID="QualityIssueCarriage" runat="server" Checked='<%# Eval("QualityIssueCarriage")%>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Quality Issue Labour">
                                                <HeaderStyle HorizontalAlign="Center" Width="3%" />
                                                <ItemStyle HorizontalAlign="Center" />
                                                <ItemTemplate>
                                                    <cc1:ucCheckbox ID="QualityIssueLabour" runat="server" Checked='<%# Eval("QualityIssueLabour")%>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </cc1:ucGridView>
                                </td>
                            </tr>
                        </table>
                        <div class="button-row">
                            <cc1:ucButton ID="btnBack" runat="server" Text="Back" CssClass="button" OnClick="btnBack_Click" />
                        </div>
                    </cc1:ucPanel>
                </div>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnGenerateReport" />
            <asp:PostBackTrigger ControlID="btnExportToExcel" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>

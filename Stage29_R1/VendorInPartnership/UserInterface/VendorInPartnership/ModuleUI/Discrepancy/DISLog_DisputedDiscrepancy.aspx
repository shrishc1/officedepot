﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    AutoEventWireup="true" CodeFile="DISLog_DisputedDiscrepancy.aspx.cs" Inherits="DISLog_DisputedDiscrepancy" %>

<%@ Register Src="~/CommonUI/UserControls/MultiSelectVendor.ascx" TagName="MultiSelectVendor"
    TagPrefix="cc2" %>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="../../CommonUI/UserControls/ucExportToExcel.ascx" TagName="ucExportToExcel"
    TagPrefix="cc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2>
        <cc1:ucLabel ID="lblDisputedDiscrepancy" runat="server"></cc1:ucLabel>
    </h2>
    <asp:ScriptManager ID="sm1" runat="server"></asp:ScriptManager>
    <div class="button-row">
        <cc2:ucExportToExcel ID="ucExportToExcel" runat="server" Visible="false" />
    </div>
    <div class="right-shadow">
        <div class="formbox">
            <div class="button-row">
                <cc1:ucMultiView ID="mvDiscrepancyList" runat="server" ActiveViewIndex="0">
                    <cc1:ucView ID="vwSearchListing" runat="server">
                        <cc1:ucPanel ID="pnlGrid" runat="server" ScrollBars="Both" CssClass="fieldset-form">
                            <cc1:ucGridView ID="gvDisputedDiscrepancy" Width="100%" runat="server" CssClass="grid" OnRowDataBound="gvDisputedDiscrepancy_RowDataBound"
                                OnSorting="SortGrid" AllowSorting="true" Style="overflow: auto;">
                                <Columns>
                                    <asp:TemplateField HeaderText="Site" SortExpression="Site.SiteName">
                                        <HeaderStyle HorizontalAlign="Left" Width="300px" />
                                        <ItemStyle HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLiteral ID="ltSiteName" runat="server" Text='<%#Eval("Site.SiteName") %>'></cc1:ucLiteral>
                                            <asp:HiddenField ID="hdnDisType" runat="server" Value='<%#Eval("DiscrepancyTypeID") %>' />
                                            <asp:HiddenField ID="hdnDisLogID" runat="server" Value='<%#Eval("DiscrepancyLogID") %>' />
                                            <asp:HiddenField ID="hdnUserID" runat="server" Value='<%#Eval("UserID") %>' />
                                            <asp:HiddenField ID="hdnQueryDisId" runat="server" Value='<%#Eval("QueryDiscrepancy.QueryDiscrepancyID") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField HeaderText="Status" DataField="DiscrepancyStatusDiscreption" AccessibleHeaderText="false"
                                        SortExpression="DiscrepancyStatusDiscreption">
                                        <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:BoundField>
                                    <asp:TemplateField HeaderText="VDR Number" SortExpression="VDRNo">
                                        <HeaderStyle Width="80px" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hypLinkToDisDetails" runat="server" Text='<%#Eval("VDRNo") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField HeaderText="VDR Discription" DataField="ProductDescription" SortExpression="ProductDescription">
                                        <HeaderStyle HorizontalAlign="Left" Width="300px" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:BoundField>
                                    <%--BEGIN -- Need to replace column for Query GI,INV,AP,VEN--%>
                                    <asp:TemplateField HeaderText="Query" SortExpression="QueryDiscrepancy.Query">
                                        <HeaderStyle HorizontalAlign="Left" Width="200px" />
                                        <ItemStyle HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLiteral ID="ltQuery" runat="server" Text='<%#Eval("QueryDiscrepancy.Query") %>'></cc1:ucLiteral></ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField HeaderText="GI" DataField="GI" SortExpression="GI">
                                        <HeaderStyle HorizontalAlign="Left" Width="300px" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="INV" DataField="INV" SortExpression="INV">
                                        <HeaderStyle HorizontalAlign="Left" Width="300px" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="AP" DataField="AP" SortExpression="AP">
                                        <HeaderStyle HorizontalAlign="Left" Width="300px" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="VEN" DataField="VEN" SortExpression="VEN">
                                        <HeaderStyle HorizontalAlign="Left" Width="300px" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:BoundField>
                                    <%--END -- Need to replace column for GI,INV,AP,VEN--%>
                                    <asp:TemplateField HeaderText="CreateDate" SortExpression="DiscrepancyLogDate">
                                        <HeaderStyle HorizontalAlign="Left" Width="200px" />
                                        <ItemStyle HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLiteral ID="ltDiscrepancyLogDate" runat="server" Text='<%#Eval("DiscrepancyLogDate", "{0:dd/MM/yyyy}") %>'></cc1:ucLiteral></ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField HeaderText="Delivery Note" DataField="DeliveryNoteNumber" SortExpression="DeliveryNoteNumber">
                                        <HeaderStyle HorizontalAlign="Left" Width="100px" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="Vendor" DataField="VendorNoName" SortExpression="VendorNoName">
                                        <HeaderStyle HorizontalAlign="Left" Width="400px" />
                                        <ItemStyle HorizontalAlign="Left" Wrap="true" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="PO Number" DataField="PurchaseOrderNumber" SortExpression="PurchaseOrderNumber">
                                        <HeaderStyle HorizontalAlign="Left" Width="100px" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:BoundField>
                                    <asp:TemplateField HeaderText="CreatedBy" SortExpression="User.FirstName">
                                        <HeaderStyle HorizontalAlign="Left" Width="400px" />
                                        <ItemStyle HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLiteral ID="ltUserName" runat="server" Text='<%#Eval("User.FirstName") %>'></cc1:ucLiteral></ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField HeaderText="Stock Planner" DataField="StockPlannerNO" SortExpression="StockPlannerNO">
                                        <HeaderStyle HorizontalAlign="Left" Width="400px" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:BoundField>
                                </Columns>
                            </cc1:ucGridView>
                        </cc1:ucPanel>
                    </cc1:ucView>
                </cc1:ucMultiView>
            </div>
        </div>
    </div>
</asp:Content>

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;

public partial class ModuleUI_Discrepancy_DISLog_Test : CommonPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        BindDiscrepancyList();       
        
    }

    protected void BindDiscrepancyList()
    {
        SqlConnection con = new SqlConnection();       
        con.ConnectionString = ConfigurationManager.AppSettings["sConn"];
        con.Open();
        SqlCommand cmd = new SqlCommand("Deleteddiscrepacnytestsp", con);
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        DataSet ds = new DataSet();
        da.Fill(ds);
        GridView1.DataSource = ds.Tables[0];
        GridView1.DataBind();
        con.Close();
        
    }


}
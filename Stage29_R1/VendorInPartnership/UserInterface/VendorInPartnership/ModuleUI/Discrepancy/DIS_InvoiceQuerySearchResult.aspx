﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    AutoEventWireup="true" CodeFile="DIS_InvoiceQuerySearchResult.aspx.cs" Inherits="ModuleUI_Discrepancy_DIS_InvoiceQuerySearchResult" %>

<%@ Register Src="~/CommonUI/UserControls/MultiSelectVendor.ascx" TagName="MultiSelectVendor"
    TagPrefix="cc2" %>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="../../CommonUI/UserControls/ucExportToExcel.ascx" TagName="ucExportToExcel"
    TagPrefix="cc2" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectAP.ascx" TagName="MultiSelectAP"
    TagPrefix="cc1" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script language="javascript" type="text/javascript">
        $(document).ready(function () {

            var lstitem = document.getElementById('<%= UclstSiteSelected.ClientID %>');
            if (lstitem != null) {
                if (lstitem.options.length == 0) {
                    document.getElementById('<%= hiddenSelectedIDs.ClientID %>').value = "";
                    document.getElementById('<%= hiddenSelectedName.ClientID %>').value = "";
                }
            }
        });

        $(document).ready(function () {
            $("body").keypress(function (e) {
                if (e.which == 13 || e.keyCode == 13) {
                    $('#<%=btnSearch.ClientID %>').click();
                    return false;
                }
            });
        });
    </script>
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
     <h2>
    <cc1:ucLabel ID="lblInvoiceQueryDiscrepancySearch" runat="server"></cc1:ucLabel>
    </h2>
    <div class="button-row">
        <cc2:ucExportToExcel ID="ucExportToExcel1" runat="server" Visible="false" />
    </div>
    <div class="right-shadow">
        <div class="formbox">
            <div class="button-row">
<table width="70%" id="tblSearch" runat="server" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
        <tr>
            <td>
            </td>
        </tr>
        <tr>
            <td style="font-weight: bold;">
                &nbsp;&nbsp;<cc1:ucLabel ID="lblPurchaseOrderNo" runat="server" Text="Purchase Order #"></cc1:ucLabel>
            </td>
            <td style="font-weight: bold;">
                <cc1:ucLabel ID="UcLabel7" runat="server">:</cc1:ucLabel>
            </td>
            <td style="font-weight: bold;">
                <cc1:ucTextbox ID="txtPurchaseOrder" runat="server" Width="145px"></cc1:ucTextbox>
            </td>
            <td style="font-weight: bold;">
                <cc1:ucLabel ID="lblDiscrepancyNo" runat="server" Text="Discrepancy #" Width="100%"></cc1:ucLabel>
            </td>
            <td style="font-weight: bold;">
                &nbsp;<cc1:ucLabel ID="UcLabel5" runat="server">:</cc1:ucLabel>
            </td>
            <td style="font-weight: bold;">
                <cc1:ucTextbox ID="txtDiscrepancyNo" runat="server" Width="145px"></cc1:ucTextbox>
            </td>
        </tr>
        <tr>
            <td style="font-weight: bold;">
                &nbsp;&nbsp;<cc1:ucLabel ID="lblDiscrepancyStatus" runat="server" Text="Discrepancy Status"></cc1:ucLabel>
            </td>
            <td style="font-weight: bold;">
                <cc1:ucLabel ID="UcLabel6" runat="server">:</cc1:ucLabel>
            </td>
            <td style="font-weight: bold;">
                <asp:DropDownList ID="ddlDiscrepancyStatus" runat="server" Width="150px">
                    <asp:ListItem Text="--Select--" Value="" />
                    <asp:ListItem Text="Open" Value="O" />
                    <asp:ListItem Text="Forced Closed" Value="F" />
                    <asp:ListItem Text="Closed" Value="C" />
                </asp:DropDownList>
            </td>
            <td style="font-weight: bold;">
                &nbsp;&nbsp;<cc1:ucLabel ID="lblActionPendingWith" runat="server" Text="Action Pending With"></cc1:ucLabel>
            </td>
            <td style="font-weight: bold;">
                <cc1:ucLabel ID="UcLabel9" runat="server">:</cc1:ucLabel>
            </td>
            <td style="font-weight: bold;">
                <asp:DropDownList ID="ddlActionPendingWithDropDown" runat="server" Width="150px">
                    <asp:ListItem Text="All" Value="A" />
                    <asp:ListItem Text="GoodsIn" Value="G" />
                    <asp:ListItem Text="Stock Planner" Value="S" />
                    <asp:ListItem Text="Account Payable" Value="P" />
                    <asp:ListItem Text="Vendor" Value="V" />
                    <asp:ListItem Text="Mediator" Value="M" />
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td style="font-weight: bold;">
                &nbsp;&nbsp;<cc1:ucLabel ID="lblDiscrepancyDateRange" runat="server" Text="Discrepancy Date Range"></cc1:ucLabel>
            </td>
            <td style="font-weight: bold;">
                <cc1:ucLabel ID="UcLabel8" runat="server">:</cc1:ucLabel>
            </td>
            <td style="font-weight: bold;" align="left">
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:TextBox ID="txtDiscrepancyDateRangeFrom" runat="server" ClientIDMode="Static"
                    class="date" Width="79px" />
            </td>
            <td style="font-weight: bold;" align="Left">
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <cc1:ucLabel ID="lblTo" runat="server" Text="To"></cc1:ucLabel>
            </td>
            <td style="font-weight: bold;">
                <cc1:ucLabel ID="UcLabel3" runat="server">:</cc1:ucLabel>
            </td>
            <td style="font-weight: bold;" align="left">
                &nbsp;&nbsp;&nbsp;&nbsp;
                <asp:TextBox ID="txtDiscrepancyDateRangeTo" runat="server" ClientIDMode="Static"
                    class="date" Width="79px" />
            </td>
        </tr>
        <tr>
            <td style="font-weight: bold;">
                &nbsp;&nbsp;<cc1:ucLabel ID="lblInvoice" runat="server" Text="Invoice #"></cc1:ucLabel>
            </td>
            <td style="font-weight: bold;">
                <cc1:ucLabel ID="UcLabel12" runat="server">:</cc1:ucLabel>
            </td>
            <td style="font-weight: bold; " colspan="4" align="left">
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <cc1:ucTextbox ID="txtDeliveryNote" runat="server" Width="145px"></cc1:ucTextbox>
            </td>
        </tr>
        <tr>
            <td style="font-weight: bold; width: 23%">
                &nbsp;&nbsp;<cc1:ucLabel ID="lblSite" runat="server" Text="Site"></cc1:ucLabel>
            </td>
            <td style="font-weight: bold; width: 1%">
                <cc1:ucLabel ID="UcLabel2" runat="server">:</cc1:ucLabel>
            </td>
            <td width="30%">
                <cc1:ucListBox ID="UclstSiteList" runat="server" Height="150px" Width="150px">
                </cc1:ucListBox>
            </td>
            <td valign="middle" align="center" width="20%" colspan="1">
                <div>
                    <input type="button" id="btnMoveRight" value=">" class="button" style="width: 35px"
                        onclick="Javascript:MoveItem('<%= UclstSiteList.ClientID %>', '<%= UclstSiteSelected.ClientID %>','<%= hiddenSelectedIDs.ClientID %>','<%= hiddenSelectedName.ClientID %>');" />
                </div>
                &nbsp;
                <div>
                    <input type="button" id="btnMoveLeft" value="<" class="button" style="width: 35px"
                        onclick="Javascript:MoveItem('<%= UclstSiteSelected.ClientID %>', '<%= UclstSiteList.ClientID %>','<%= hiddenSelectedIDs.ClientID %>','<%= hiddenSelectedName.ClientID %>');" />
                </div>
            </td>
            <td width="1%">
                <asp:HiddenField ID="hiddenSelectedIDs" runat="server" Value="" />
                <asp:HiddenField ID="hiddenSelectedName" runat="server" Value="" />
            </td>
            <td width="25%">
                <cc1:ucListBox ID="UclstSiteSelected" runat="server" Height="150px" Width="150px"
                    ViewStateMode="Enabled">
                </cc1:ucListBox>
            </td>
        </tr>
        <tr>
            <td style="font-weight: bold;">
                &nbsp;&nbsp;<cc1:ucLabel ID="lblSearchBySku" runat="server" Text="Search By SKU"></cc1:ucLabel>
            </td>
            <td style="font-weight: bold;">
                <cc1:ucLabel ID="UcLabel13" runat="server">:</cc1:ucLabel>
            </td>
            <td style="font-weight: bold;">
                <cc1:ucTextbox ID="txtSearchBySkuText" runat="server" Width="145px"></cc1:ucTextbox>
            </td>
            <td style="font-weight: bold;">
                &nbsp;&nbsp;<cc1:ucLabel ID="lblSearchByCatCode" runat="server" Text="Search By CAT Code"></cc1:ucLabel>
            </td>
            <td style="font-weight: bold;">
                <cc1:ucLabel ID="UcLabel15" runat="server">:</cc1:ucLabel>
            </td>
            <td style="font-weight: bold;">
                <cc1:ucTextbox ID="txtSearchByCatCodeText" runat="server" Width="145px"></cc1:ucTextbox>
            </td>
        </tr>
        <tr>
            <td style="font-weight: bold;" colspan="6">
                <cc1:MultiSelectAP runat="server" ID="msAP" />
            </td>
        </tr>
        <tr>
            <td style="font-weight: bold;" colspan="6">
                <cc2:MultiSelectVendor runat="server" ID="msVendor" />
            </td>
        </tr>
    </table>
    <div class="row">
        <cc1:ucPanel ID="pnlGrid" runat="server" ScrollBars="Both" CssClass="fieldset-form">
            <cc1:ucGridView ID="gvDisLog" Width="100%" runat="server" CssClass="grid" OnRowDataBound="gvDisLog_RowDataBound"
                OnSorting="SortGrid" AllowSorting="true" Style="overflow: auto;">
                <Columns>
                    <asp:TemplateField HeaderText="Site" SortExpression="Site.SiteName">
                        <HeaderStyle HorizontalAlign="Left" Width="300px" />
                        <ItemStyle HorizontalAlign="Left" />
                        <ItemTemplate>
                            <cc1:ucLiteral ID="ltSiteName" runat="server" Text='<%#Eval("POSiteName") %>'></cc1:ucLiteral><asp:HiddenField
                                ID="hdnDisType" runat="server" Value='<%#Eval("DiscrepancyTypeID") %>' />
                            <asp:HiddenField ID="hdnDisLogID" runat="server" Value='<%#Eval("DiscrepancyLogID") %>' />
                            <asp:HiddenField ID="hdnUserID" runat="server" Value='<%#Eval("UserID") %>' />
                            <asp:HiddenField ID="hdnWorkFlowID" runat="server" Value='<%#Eval("DiscrepancyWorkflowID") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>                    
                    <asp:TemplateField HeaderText="Discrepancy" SortExpression="VDRNo">
                        <HeaderStyle Width="80px" HorizontalAlign="Left" />
                        <ItemStyle HorizontalAlign="Left" />
                        <ItemTemplate>
                            <asp:HyperLink ID="hypLinkToDisDetails" runat="server" Text='<%#Eval("VDRNo") %>' Target="_blank" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField HeaderText="Status" DataField="DiscrepancyStatus" AccessibleHeaderText="false"
                        SortExpression="DiscrepancyStatus">
                        <HeaderStyle Width="100px" HorizontalAlign="Left" />
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:BoundField>
                    <asp:BoundField HeaderText="Open With" DataField="OpenWith" SortExpression="OpenWith">
                        <HeaderStyle HorizontalAlign="Center" Width="300px" />
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:BoundField>

                     <asp:BoundField HeaderText="Invoice #" DataField="DeliveryNoteNumber" SortExpression="DeliveryNoteNumber">
                        <HeaderStyle HorizontalAlign="Center" Width="300px" />
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:BoundField>

                     <asp:BoundField HeaderText="Vendor" DataField="VendorNoName" SortExpression="VendorNoName">
                        <HeaderStyle HorizontalAlign="Center" Width="300px" />
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:BoundField>

                     <asp:BoundField HeaderText="PO #" DataField="PurchaseOrderNumber" SortExpression="PurchaseOrderNumber">
                        <HeaderStyle HorizontalAlign="Center" Width="300px" />
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:BoundField>

                     <asp:BoundField HeaderText="AP Clerk" DataField="AP" SortExpression="AP">
                        <HeaderStyle HorizontalAlign="Left" Width="300px" />
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:BoundField>

                      <asp:BoundField HeaderText="Created By" DataField="CreatedBy" SortExpression="CreatedBy">
                        <HeaderStyle HorizontalAlign="Left" Width="300px" />
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:BoundField>

                    <asp:BoundField HeaderText="Date" DataField="CreatedDate" SortExpression="CreatedDate">
                        <HeaderStyle HorizontalAlign="Left" Width="300px" />
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:BoundField>

                     <asp:BoundField HeaderText="Resolution" DataField="Resolution" SortExpression="Resolution">
                        <HeaderStyle HorizontalAlign="Left" Width="300px" />
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:BoundField>                 
                </Columns>
            </cc1:ucGridView>
        </cc1:ucPanel>
    </div>
    </div>
    </div>
    </div>
    <div class="button-row">
        <%--<cc1:ucButton ID="btnSave" runat="server" Text="Save"
    class="button" />--%>
        <cc1:ucButton ID="btnSearch" runat="server" Text="Search" CssClass="button" 
            onclick="btnSearch_Click" />
        <cc1:ucButton ID="btnReturntoSearchCriteria" runat="server" Visible="false" Text="Return to Search Criteria"
            class="button" onclick="btnReturntoSearchCriteria_Click"  />
    </div>
</asp:Content>

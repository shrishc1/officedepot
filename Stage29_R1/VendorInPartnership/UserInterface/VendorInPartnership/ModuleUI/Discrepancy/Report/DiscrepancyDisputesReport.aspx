﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    AutoEventWireup="true" CodeFile="DiscrepancyDisputesReport.aspx.cs" Inherits="DiscrepancyDisputesReport" %>

<%@ Register Src="~/CommonUI/UserControls/MultiSelectSite.ascx" TagName="MultiSelectSite"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectVendor.ascx" TagName="MultiSelectVendor"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/ucSeacrhVendor.ascx" TagName="ucSeacrhVendor"
    TagPrefix="uc1" %>
<%@ Register Src="~/CommonUI/UserControls/ucVendorSelectionTemplate.ascx" TagName="VendorSelectTemplate"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/ucExportToExcel.ascx" TagName="ucExportToExcel"
    TagPrefix="cc2" %>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script type="text/javascript" language="javascript">
        function setValue1(target) {
            document.getElementById('<%=hdnJSFromDt.ClientID %>').value = target.value;
        }

        function setValue2(target) {
            document.getElementById('<%=hdnJSToDt.ClientID %>').value = target.value;
        }

        function getScroll1(ctrDiv) {
            document.getElementById('<%=scroll2.ClientID%>').value = ctrDiv.scrollLeft;
        }
        $(document).ready(function () {
            $("body").keypress(function (e) {
                if (e.which == 13 || e.keyCode == 13) {
                    $('#<%=btnSearch.ClientID %>').click();
                    return false;
                }
            });
        });

    </script>
    <h2>
        <cc1:ucLabel ID="lblDiscrepancyDisputes" runat="server"></cc1:ucLabel>
    </h2>
    <input type="hidden" id="scroll2" runat="server" />
    <asp:HiddenField ID="hdnJSFromDt" runat="server" />
    <asp:HiddenField ID="hdnJSToDt" runat="server" />
     
    <div class="right-shadow">
        <div class="formbox">
            <table width="100%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings" id="tblVendorTemplate" runat="server">
                <tr>
                    <td align="right">
                        <cc1:VendorSelectTemplate runat="server" ID="ucVendorTemplateSelect" />
                    </td>
                </tr>
            </table>
            <table width="100%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                <tr>
                    <td style="font-weight: bold; width: 90px;">
                        <cc1:ucLabel ID="lblSite" runat="server" Text="Site"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold;">
                        :
                    </td>
                    <td>
                        <cc1:MultiSelectSite runat="server" ID="msSite" />
                    </td>
                </tr>
                <tr id="trSingleVendorControl" runat="server">
                    <td style="font-weight: bold;">
                        <cc1:ucLabel ID="lblVendor_1" runat="server" Text="Vendor"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold;">
                        :
                    </td>
                    <td style="font-weight: bold;">
                        <table>
                            <tr>
                                <td>
                                    <span id="spVender" runat="server">
                                        <uc1:ucSeacrhVendor ID="ucSeacrhVendor1" runat="server" />
                                    </span>
                                    <cc1:ucLiteral ID="ltSearchVendorName" runat="server" Visible="false"></cc1:ucLiteral>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr id="trMultivendorControl" runat="server">
                    <td style="font-weight: bold;">
                        <cc1:ucLabel ID="lblVendor" runat="server"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold;">
                        :
                    </td>
                    <td>
                        <cc1:MultiSelectVendor runat="server" ID="msVendor" />
                    </td>
                </tr>              
                <tr>
                    <td colspan="3">
                        <table width="100%">
                            <tr style="height: 3em;">
                                <td style="font-weight: bold; width: 71px">
                                    <cc1:ucLabel ID="lblDateFrom" runat="server" Text="Date From"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold; width: 1%">
                                    <cc1:ucLabel ID="UcLabel1" runat="server">:</cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold; width: 89%">
                                    <div id="UcPanelDate" style="margin-left: 20px;">
                                        <table cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style="font-weight: bold; width: 6em;">
                                                    <cc1:ucTextbox ID="txtFromDate" runat="server" ClientIDMode="Static" CssClass="date"
                                                        onchange="setValue1(this)" ReadOnly="True" Width="70px" />
                                                </td>
                                                <td style="font-weight: bold; width: 4em; text-align: center">
                                                    <cc1:ucLabel ID="lblTo" runat="server" Text="To"></cc1:ucLabel>
                                                </td>
                                                <td style="font-weight: bold; width: 6em;">
                                                    <cc1:ucTextbox ID="txtToDate" runat="server" ClientIDMode="Static" CssClass="date"
                                                        onchange="setValue2(this)" ReadOnly="True" Width="70px" />
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="font-weight: bold;">
                        <cc1:ucLabel ID="lblQueryStatus" runat="server"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold;">
                        :
                    </td>
                    <td style="width: 800px;" class="nobold radiobuttonlist">
                        <cc1:ucRadioButtonList ID="rblQueryStatus" CssClass="radio-fix" runat="server" RepeatColumns="3"
                            RepeatDirection="Horizontal">
                            <asp:ListItem Text="Open" Value="O" Selected="True"></asp:ListItem>
                            <asp:ListItem Text="Closed" Value="C"></asp:ListItem>
                            <asp:ListItem Text="All" Value="A"></asp:ListItem>
                        </cc1:ucRadioButtonList>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <div class="button-row" style="text-align: center;">
                            <cc1:ucButton ID="btnSearch" runat="server" Text="Search" CssClass="button" OnClick="btnSearch_Click" />
                        </div>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div class="button-row">
        <cc2:ucExportToExcel ID="ucExportToExcel1" runat="server" />
    </div>
    <div style="width: 100%; height: 440px; overflow: scroll" id="divPrint" class="fixedTable"
        onscroll="getScroll1(this);">
        <table cellspacing="1" cellpadding="0" class="form-table">
            <tr>
                <td>
                    <cc1:ucGridView ID="gvDisDispute" runat="server" AutoGenerateColumns="false" CssClass="grid gvclass"
                        CellPadding="0" Width="1500px" AllowSorting="false" AllowPaging="true" PageSize="30"
                        OnPageIndexChanging="gvDisDispute_PageIndexChanging" OnRowDataBound="gvDisDispute_RowDataBound">
                        <AlternatingRowStyle BackColor="White" ForeColor="#284775"></AlternatingRowStyle>
                        <EmptyDataTemplate>
                            <div style="text-align: center">
                                <cc1:ucLabel ID="lblNoData" runat="server" Text="No Records found."></cc1:ucLabel>
                            </div>
                        </EmptyDataTemplate>
                        <Columns>
                            <asp:BoundField HeaderText="Vendor #" DataField="Vendor_No">
                                <HeaderStyle Width="120px" />
                                <ItemStyle HorizontalAlign="Center" Width="120px" />
                            </asp:BoundField>
                            <asp:BoundField HeaderText="Vendor Name" DataField="Vendor_Name">
                                <HeaderStyle Width="200px" />
                                <ItemStyle HorizontalAlign="Center" Width="400px" />
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="Date Query Rasied">
                                <HeaderStyle Width="100px" />
                                <ItemStyle HorizontalAlign="Center" Width="100px" />
                                <ItemTemplate>
                                    <cc1:ucLabel ID="lblVendorQueryDate" runat="server" Text='<%# Eval("VendorQueryDate", "{0:dd/MM/yyyy}") %>'></cc1:ucLabel>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Who raised the issue" DataField="VendorUserName">
                                <HeaderStyle Width="150px" />
                                <ItemStyle HorizontalAlign="Center" Width="200px" />
                            </asp:BoundField>
                            <asp:BoundField HeaderText="Query Status" DataField="Query">
                                <HeaderStyle Width="100px" />
                                <ItemStyle HorizontalAlign="Center" Width="100px" />
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="Date Discrepancy Raised">
                                <HeaderStyle Width="100px" />
                                <ItemStyle HorizontalAlign="Center" Width="100px" />
                                <ItemTemplate>
                                    <cc1:ucLabel ID="lblDiscrepancyLogDate" runat="server" Text='<%# Eval("Discrepancy.DiscrepancyLogDate", "{0:dd/MM/yyyy}") %>'></cc1:ucLabel>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Discrepancy Number">
                                <HeaderStyle Width="150px" />
                                <ItemStyle HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <asp:HiddenField ID="hdnDisLogID" runat="server" Value='<%#Eval("Discrepancy.DiscrepancyLogID") %>' />
                                    <asp:HiddenField ID="hdnQueryDisId" runat="server" Value='<%#Eval("QueryDiscrepancyID") %>' />
                                    <asp:HyperLink ID="hypLinkToDisDetails" runat="server" Text='<%#Eval("Discrepancy.VDRNo") %>'></asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Detail of Query" DataField="VendorComment">
                                <HeaderStyle Width="250px" />
                                <ItemStyle HorizontalAlign="Left" Width="500px" />
                            </asp:BoundField>
                            <asp:BoundField HeaderText="Office Depot Response" DataField="GoodsInComment">
                                <HeaderStyle Width="250" />
                                <ItemStyle HorizontalAlign="Left" Width="500px" />
                            </asp:BoundField>
                            <asp:BoundField HeaderText="Date of Response" DataField="GoodsInQueryDate" DataFormatString="{0:dd/MM/yyyy}">
                                <HeaderStyle Width="100px" />
                                <ItemStyle HorizontalAlign="Center" Width="100px" />
                            </asp:BoundField>
                            <asp:BoundField HeaderText="Responded by" DataField="GoodsInUserName">
                                <HeaderStyle Width="150px" />
                                <ItemStyle HorizontalAlign="Center" Width="100px" />
                            </asp:BoundField>
                            <asp:BoundField HeaderText="Elasped Time to Respond" DataField="ElaspedTime">
                                <HeaderStyle Width="100px" />
                                <ItemStyle HorizontalAlign="Center" Width="100px" />
                            </asp:BoundField>
                        </Columns>
                    </cc1:ucGridView>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>

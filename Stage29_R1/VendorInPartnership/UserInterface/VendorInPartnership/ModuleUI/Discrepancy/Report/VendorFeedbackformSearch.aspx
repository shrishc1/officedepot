﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    CodeFile="VendorFeedbackformSearch.aspx.cs" Inherits="VendorFeedbackformSearch" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectCountry.ascx" TagName="MultiSelectCountry"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectSite.ascx" TagName="MultiSelectSite"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectAP.ascx" TagName="MultiSelectAP"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectVendor.ascx" TagName="MultiSelectVendor"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectStockPlanner.ascx" TagName="MultiSelectStockPlanner"
    TagPrefix="cc1" %>
      <%@ Register Src="~/CommonUI/UserControls/ucVendorSelectionTemplate.ascx" TagName="VendorSelectTemplate"
    TagPrefix="cc1" %>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript" language="javascript">

        function ShowDiscrepancyType() {

            var objrdoDiscrepancy = document.getElementById('<%=rdoDiscrepancyType.ClientID %>');

            if (objrdoDiscrepancy.checked) {
                document.getElementById('tdDiscrepancyType').style.display = 'block';
            }
            else {
                document.getElementById('tdDiscrepancyType').style.display = 'none';
            }

        }

        $(document).ready(function () {
            $("body").keypress(function (e) {
                if (e.which == 13 || e.keyCode == 13) {
                    $('#<%=btnGenerateReport.ClientID %>').click();
                    return false;
                }
            });
        });
    </script>
    <h2>
        <cc1:ucLabel ID="lblVendorFeedbackReport" runat="server" Text="Vendor Feedback Report"></cc1:ucLabel>
    </h2>
    <asp:ScriptManager ID="SM1" runat="server">
    </asp:ScriptManager>
    <div class="right-shadow">
        <div class="formbox">
         <table width="90%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                <tr>
                    <td align="right">
                        <cc1:VendorSelectTemplate runat="server" ID="ucVendorTemplateSelect" />
                    </td>
                </tr>
            </table>
            <table width="90%" cellspacing="5" style="table-layout: fixed;" cellpadding="0" border="0"
                align="center" class="top-settings">
                <tr>
                    <td style="font-weight: bold; width: 7em;">
                        <cc1:ucLabel ID="lblCountry" runat="server" Text="Country"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold; width: 1em;">
                        :
                    </td>
                    <td colspan="3">
                        <cc1:MultiSelectCountry runat="server" ID="msCountry" />
                    </td>
                </tr>
                <tr>
                    <td style="font-weight: bold;">
                        <cc1:ucLabel ID="lblSite" runat="server" Text="Site"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold;">
                        :
                    </td>
                    <td colspan="3">
                        <cc1:MultiSelectSite runat="server" ID="msSite" />
                    </td>
                </tr>
                <tr>
                    <td style="font-weight: bold;">
                        <cc1:ucLabel ID="lblVendor" runat="server" Text="Vendor"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold;">
                        :
                    </td>
                    <td colspan="3">
                        <cc1:MultiSelectVendor runat="server" ID="msVendor" />
                    </td>
                </tr>
                <tr>
                    <td style="font-weight: bold;">
                        <cc1:ucLabel ID="lblDateFrom" runat="server" Text="Date From"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold;">
                        :
                    </td>
                    <td colspan="3">
                        <table>
                            <tr>
                                <td style="font-weight: bold; width: 6em;">
                                    <cc1:ucTextbox ID="txtFromDate" ClientIDMode="Static" runat="server" CssClass="date"
                                        Width="70px" />
                                </td>
                                <td style="font-weight: bold; text-align: center; width: 6em;">
                                    <cc1:ucLabel ID="lblTo" runat="server" Text="To"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold; text-align: left; width: 6em;">
                                    <cc1:ucTextbox ID="txtToDate" ClientIDMode="Static" runat="server" CssClass="date"
                                        Width="70px" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="font-weight: bold;">
                        <cc1:ucLabel ID="lblReportType" runat="server" Text="Report Type"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold;">
                        :
                    </td>
                    <td colspan="3">
                        <table>
                            <tr>
                                <td style="font-weight: bold; width: 10em;">
                                    <cc1:ucRadioButton ID="rdoCheckAll" runat="server" Text="All" GroupName="ReportType" onclick="ShowDiscrepancyType();"
                                        Checked="true" />
                                </td>
                                <td style="font-weight: bold; width: 15em;">
                                    <cc1:ucRadioButton ID="rdoDiscrepancyType" runat="server" Text="Discrepancy Type" GroupName="ReportType"
                                        onclick="ShowDiscrepancyType();" />
                                </td>
                                <td>
                                    <div style="display: none;" id="tdDiscrepancyType">
                                        <cc1:ucDropdownList ID="ddlDiscrepancyType" runat="server" Width="200px">
                                        </cc1:ucDropdownList>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div class="button-row">
        <cc1:ucButton ID="btnGenerateReport" runat="server" Text="Generate Report" CssClass="button"
            OnClick="btnGenerateReport_Click" />
    </div>
</asp:Content>

﻿using System;
using System.Data;
using BusinessEntities.ModuleBE.Discrepancy.Report;
using BusinessLogicLayer.ModuleBAL.Discrepancy.Report;
using Utilities;
using WebUtilities;
using Microsoft.Reporting.WebForms;

public partial class ShuttleDiscrepancyReport : CommonPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            txtFromDate.Text = DateTime.Now.AddDays(-DateTime.Now.Day + 1).ToString("dd/MM/yyyy");
            txtToDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
            txtFromDate.Attributes.Add("readonly", "readonly");
            txtToDate.Attributes.Add("readonly", "readonly");
        }
        txtPOText.Focus();
    }

    protected void btnGenerateReport_Click(object sender, EventArgs e)
    {
        DiscrepancyReportBE oDiscrepancyReportBE = new DiscrepancyReportBE();
        DiscrepancyReportBAL oDiscrepancyReportBAL = new DiscrepancyReportBAL();

        oDiscrepancyReportBE.Action = "ShuttleDiscrepancyReport";
        if (!string.IsNullOrEmpty(msReceivingSite.SelectedSiteIDs))
            oDiscrepancyReportBE.SelectedReceivingSiteIDs = msReceivingSite.SelectedSiteIDs;

        if (!string.IsNullOrEmpty(msSendingSite.SelectedSiteIDs))
            oDiscrepancyReportBE.SelectedSendingSiteIDs = msSendingSite.SelectedSiteIDs;

        if (!string.IsNullOrEmpty(msSelectDiscType.SelectedTypes))
            oDiscrepancyReportBE.SelectedTypes = msSelectDiscType.SelectedTypes.Replace(", ", ",");

        if (!string.IsNullOrEmpty(txtPOText.Text))
            oDiscrepancyReportBE.SelectedPO = txtPOText.Text;

        if (!string.IsNullOrEmpty(txtSkuWithHashText.Text))
            oDiscrepancyReportBE.SelectedSKU = txtSkuWithHashText.Text;

        oDiscrepancyReportBE.DateTo = string.IsNullOrEmpty(txtToDate.Text) ? (DateTime?)null : Common.GetMM_DD_YYYY(txtToDate.Text);
        oDiscrepancyReportBE.DateFrom = string.IsNullOrEmpty(txtFromDate.Text) ? (DateTime?)null : Common.GetMM_DD_YYYY(txtFromDate.Text);
        DataSet dsFunctionalVDRTracker = oDiscrepancyReportBAL.GetShuttleDiscrepancyReportBAL(oDiscrepancyReportBE);
        Session["DataSetObject"] = dsFunctionalVDRTracker.Tables[0];
        oDiscrepancyReportBAL = null;

        ReportParameter[] reportParameter = new ReportParameter[7];
        reportParameter[0] = new ReportParameter("ReceivingSite", msReceivingSite.SelectedSiteName);
        reportParameter[1] = new ReportParameter("SendingSite", msSendingSite.SelectedSiteName);
        reportParameter[2] = new ReportParameter("Types", msSelectDiscType.SelectedTypes);
        reportParameter[3] = new ReportParameter("PO", txtPOText.Text);
        reportParameter[4] = new ReportParameter("SKU", txtSkuWithHashText.Text);
        reportParameter[5] = new ReportParameter("DateFrom", txtFromDate.Text);
        reportParameter[6] = new ReportParameter("DateTo", txtToDate.Text);
        Session["ReportParameter"] = reportParameter;

        Session["ReportPath"] = "\\ModuleUI\\Discrepancy\\Report\\RDLC\\ShuttleDiscrepancyReport.rdlc";
        Session["DateTableName"] = "ShuttleDiscrepencyDataSet";
        EncryptQueryString("../../ReportView.aspx?PageName=" + "SDR");
    }
}
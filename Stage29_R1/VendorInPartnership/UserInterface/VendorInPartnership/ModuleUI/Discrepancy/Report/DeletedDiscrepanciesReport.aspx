﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    AutoEventWireup="true" CodeFile="DeletedDiscrepanciesReport.aspx.cs" Inherits="DeletedDiscrepanciesReport" %>

<%@ Register Src="~/CommonUI/UserControls/MultiSelectCountry.ascx" TagName="MultiSelectCountry"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectSite.ascx" TagName="MultiSelectSite"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectVendor.ascx" TagName="MultiSelectVendor"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectDiscrepancy.ascx" TagName="MultiSelectDiscrepancy"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectUser.ascx" TagName="MultiSelectUser"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/ucVendorSelectionTemplate.ascx" TagName="VendorSelectTemplate"
    TagPrefix="cc1" %>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script type="text/javascript">
        $(document).ready(function () {
            $("body").keypress(function (e) {
                if (e.which == 13 || e.keyCode == 13) {
                    $('#<%=btnGenerateReport.ClientID %>').click();
                    return false;
                }
            });
        });
    </script>
    <h2>
        <cc1:ucLabel ID="lblDeletedDiscrepanciesReport" runat="server" Text="Deleted Discrepancies Report"></cc1:ucLabel>
    </h2>
    <asp:ScriptManager ID="SM1" runat="server">
    </asp:ScriptManager>
    <div class="right-shadow">
        <div class="formbox">
            <table width="100%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                <tr>
                    <td align="right">
                        <cc1:VendorSelectTemplate runat="server" ID="ucVendorTemplateSelect" />
                    </td>
                </tr>
            </table>
            <table width="100%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                <tr>
                    <td style="font-weight: bold;width:90px;">
                        <cc1:ucLabel ID="lblDiscrepancy" runat="server" Text="Discrepancy"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold; ">
                        :
                    </td>
                    <td style="font-weight: bold; ">
                        <cc1:MultiSelectDiscrepancy runat="server" ID="msDiscrepancy" />
                    </td>
                </tr>
                <%-- <tr>
                    <td colspan="7">
                        <br />
                    </td>
                </tr>--%>
                <tr>
                    <td style="font-weight: bold;">
                        <cc1:ucLabel ID="lblCountry" runat="server" Text="Country"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold;">
                        :
                    </td>
                    <td>
                        <cc1:MultiSelectCountry runat="server" ID="msCountry" />
                    </td>
                </tr>
                <tr>
                    <td style="font-weight: bold;">
                        <cc1:ucLabel ID="lblSite" runat="server" Text="Site"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold;">
                        :
                    </td>
                    <td>
                        <cc1:MultiSelectSite runat="server" ID="msSite" />
                    </td>
                </tr>
                <tr>
                    <td style="font-weight: bold;">
                        <cc1:ucLabel ID="lblVendor" runat="server" Text="Vendor"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold;">
                        :
                    </td>
                    <td>
                        <cc1:MultiSelectVendor runat="server" ID="msVendor" />
                    </td>
                </tr>
                <tr>
                    <td style="font-weight: bold;">
                        <cc1:ucLabel ID="lblUser" runat="server" Text="User"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold;">
                        :
                    </td>
                    <td>
                        <cc1:MultiSelectUser runat="server" ID="msUser" />
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <table width="100%">
                            <tr style="height: 3em;">
                                <td style="font-weight: bold; width:71px">
                                    <cc1:ucLabel ID="lblDateFrom" runat="server" Text="Date From"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold; width: 1%">
                                    <cc1:ucLabel ID="UcLabel1" runat="server">:</cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold; width: 89%">
                                    <div id="UcPanelDate" style="margin-left: 8px;">
                                        <table cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style="font-weight: bold; width: 6em;">
                                                    <cc1:ucTextbox ID="txtFromDate" ClientIDMode="Static" runat="server" CssClass="date"
                                                        Width="70px" />
                                                </td>
                                                <td style="font-weight: bold; width: 4em; text-align: center">
                                                    <cc1:ucLabel ID="lblTo" runat="server" Text="To"></cc1:ucLabel>
                                                </td>
                                                <td style="font-weight: bold; width: 6em;">
                                                    <cc1:ucTextbox ID="txtToDate" ClientIDMode="Static" runat="server" CssClass="date"
                                                        Width="70px" />
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div class="button-row">
        <cc1:ucButton ID="btnGenerateReport" runat="server" Text="Generate Report" CssClass="button"
            OnClick="btnGenerateReport_Click" />
    </div>
</asp:Content>

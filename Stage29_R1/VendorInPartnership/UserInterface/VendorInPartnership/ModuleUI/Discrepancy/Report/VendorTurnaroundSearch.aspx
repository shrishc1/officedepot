﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="VendorTurnaroundSearch.aspx.cs"
    MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master" Inherits="VendorTurnaroundSearch" %>

<%@ Register Src="~/CommonUI/UserControls/MultiSelectCountry.ascx" TagName="MultiSelectCountry"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectSite.ascx" TagName="MultiSelectSite"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectAP.ascx" TagName="MultiSelectAP"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectVendor.ascx" TagName="MultiSelectVendor"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectStockPlanner.ascx" TagName="MultiSelectStockPlanner"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/ucVendorSelectionTemplate.ascx" TagName="VendorSelectTemplate"
    TagPrefix="cc1" %>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript">
        $(document).ready(function () {
            $("body").keypress(function (e) {
                if (e.which == 13 || e.keyCode == 13) {
                    $('#<%=btnGenerateReport.ClientID %>').click();
                    return false;
                }
            });
        });
    </script>
    <h2>
        <cc1:ucLabel ID="lblVendorTurnaroundReport" runat="server" Text="Vendor Turnaround Report"></cc1:ucLabel>
    </h2>
    <asp:ScriptManager ID="SM1" runat="server">
    </asp:ScriptManager>
    <div class="right-shadow">
        <div class="formbox">
         <table width="100%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                <tr>
                    <td align="right">
                        <cc1:VendorSelectTemplate runat="server" ID="ucVendorTemplateSelect" />
                    </td>
                </tr>
            </table>
            <table width="100%" cellspacing="0" cellpadding="0" border="0" align="center" class="top-settings">
                <tr>
                    <td style="font-weight: bold;width:100px">
                        <cc1:ucLabel ID="lblCountry" runat="server" Text="Country"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold;width:10px;">
                        :
                    </td>
                    <td>
                        <cc1:MultiSelectCountry runat="server" ID="msCountry" />
                    </td>
                </tr>
                <tr>
                    <td style="font-weight: bold;">
                        <cc1:ucLabel ID="lblSite" runat="server" Text="Site"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold;">
                        :
                    </td>
                    <td>
                        <cc1:MultiSelectSite runat="server" ID="msSite" />
                    </td>
                </tr>
                <tr>
                    <td style="font-weight: bold;">
                        <cc1:ucLabel ID="lblVendor" runat="server" Text="Vendor"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold;">
                        :
                    </td>
                    <td>
                        <cc1:MultiSelectVendor runat="server" ID="msVendor" />
                    </td>
                </tr>
                <tr>
                    <td style="font-weight: bold;">
                        <cc1:ucLabel ID="lblDateFrom" runat="server" Text="Date From"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold;">
                        :
                    </td>
                    <td>
                        <table cellpadding="0" cellspacing="0">
                            <tr>
                                <td style="font-weight: bold; width: 6em;">
                                    <cc1:ucTextbox ID="txtFromDate" ClientIDMode="Static" runat="server" CssClass="date"
                                        Width="70px" />
                                </td>
                                <td style="font-weight: bold; width: 6em; text-align: center">
                                    <cc1:ucLabel ID="lblTo" runat="server" Text="To"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold; width: 6em;">
                                    <cc1:ucTextbox ID="txtToDate" ClientIDMode="Static" runat="server" CssClass="date"
                                        Width="70px" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div class="button-row">
        <cc1:ucButton ID="btnGenerateReport" runat="server" Text="Generate Report" CssClass="button"
            OnClick="btnGenerateReport_Click" />
    </div>
</asp:Content>


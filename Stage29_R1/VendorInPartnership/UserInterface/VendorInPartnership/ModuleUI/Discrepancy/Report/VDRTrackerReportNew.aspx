﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    CodeFile="VDRTrackerReportNew.aspx.cs" Inherits="ModuleUI_Discrepancy_Report_VDRTrackerReportNew" %>

<%@ Register Src="~/CommonUI/UserControls/MultiSelectCountry.ascx" TagName="MultiSelectCountry"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectSite.ascx" TagName="MultiSelectSite"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectAP.ascx" TagName="MultiSelectAP"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectVendor.ascx" TagName="MultiSelectVendor"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectStockPlanner.ascx" TagName="MultiSelectStockPlanner"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/ucVendorSelectionTemplate.ascx" TagName="VendorSelectTemplate"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/ucExportToExcel.ascx" TagName="ucExportToExcel"
    TagPrefix="cc2" %>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2>
          <cc1:ucLabel ID="lblDiscrepancyTrackingReport" runat="server" ></cc1:ucLabel>
        <asp:Label ID="lblDiscrepancyTrackingRpt" Visible="false" runat="server" ></asp:Label>
    </h2>
    
    <style type="text/css">
     .wmd-view-topscroll, .wmd-view
        {
            overflow-x: auto;
            overflow-y: hidden;
            width: 960px;           
        }
        
        .wmd-view-topscroll
        {
            height: 16px;
        }
        
        .dynamic-div
        {
            display: inline-block;          
        }
        #content .grid.modify-grid th{width:120px;}
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            $("body").keypress(function (e) {
                if (e.which == 13 || e.keyCode == 13) {
                    $('#<%=btnGO.ClientID %>').click();
                    return false;
                }
            });
        });
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequest);
        function EndRequest(sender, args) {
            if (args.get_error() == undefined) {
                $('#txtToDate,#txtFromDate').datepicker({ showOtherMonths: true,
                    selectOtherMonths: true, changeMonth: true, changeYear: true,
                    minDate: new Date(2013, 0, 1),
                    yearRange: '2013:+100',
                    dateFormat: 'dd/mm/yy', maxDate: $('<%=txtDateMax.ClientID %>').val()
                });
                $("body").keypress(function (e) {
                    if (e.which == 13 || e.keyCode == 13) {
                        $('#<%=btnGO.ClientID %>').click();
                        return false;
                    }
                });
                function setValue1(target) {
                    document.getElementById('<%=hdnJSFromDt.ClientID %>').value = target.value;                    
                }

                function setValue2(target) {
                    document.getElementById('<%=hdnJSToDt.ClientID %>').value = target.value;
                }
                if ($('#<%=rdoSummary.ClientID %>').attr('checked') == true) {
                    $("#<%= ddlSubReports.ClientID%>").show();
                    $("#<%= ddlReport.ClientID%>").show();
                    $("#<%= tdReport.ClientID%>").show();
                    $("#<%= tdReport1.ClientID%>").show();
                }
                if ($('#<%=rdoDetail.ClientID %>').attr('checked') == true) {
                    $("#<%= ddlSubReports.ClientID%>").hide();
                    $("#<%= ddlReport.ClientID%>").hide();
                    $("#<%= tdReport1.ClientID%>").hide();
                    $("#<%= tdReport.ClientID%>").hide();
                }
                $('#<%=rdoSummary.ClientID %>').click(function () {
                    $("#<%= ddlSubReports.ClientID%>").show();
                    $("#<%= ddlReport.ClientID%>").show();
                    $("#<%= tdReport.ClientID%>").show();
                    $("#<%= tdReport1.ClientID%>").show();
                });
                $('#<%=rdoDetail.ClientID %>').click(function () {
                    $("#<%= ddlSubReports.ClientID%>").hide();
                    $("#<%= ddlReport.ClientID%>").hide();
                    $("#<%= tdReport.ClientID%>").hide();
                    $("#<%= tdReport1.ClientID%>").hide();
                });
                $('#<%=btnGO.ClientID %>').click(function (e) {                   
                    $('#<%=txtToDate.ClientID %>').trigger("change");
                    $('#<%=txtFromDate.ClientID %>').trigger("change");
//                    var datefrom = document.getElementById('<%=txtFromDate.ClientID %>').value;
//                    alert(dateto);
//                    alert(datefrom);
//                    document.getElementById('<%=hdnJSFromDt.ClientID %>').value = datefrom;
//                    document.getElementById('<%=hdnJSToDt.ClientID %>').value = dateto;
                });
//                $('#<%=btnBack.ClientID %>').click(function (e) {
//                    if ($("#<%= hdnDetailViewCheck.ClientID%>").val() == "Yes") {
//                        $("#<%= tdReport.ClientID%>").hide();
//                    }
//                    else {
//                        $("#<%= tdReport.ClientID%>").show();
//                    }
//                });
            }
        }

        function setValue1(target) {
            document.getElementById('<%=hdnJSFromDt.ClientID %>').value = target.value;
           // alert(1);
        }

        function setValue2(target) {
            document.getElementById('<%=hdnJSToDt.ClientID %>').value = target.value;
        }
        $(document).ready(function () {
            $('#txtToDate,#txtFromDate').datepicker({ showOtherMonths: true,
                selectOtherMonths: true, changeMonth: true, changeYear: true,
                minDate: new Date(2013, 0, 1),
                yearRange: '2013:+100',
                dateFormat: 'dd/mm/yy', maxDate: $('<%=txtDateMax.ClientID %>').val()
            });

            if ($('#<%=rdoSummary.ClientID %>').attr('checked') == true) {
                $("#<%= ddlSubReports.ClientID%>").show();
                $("#<%= ddlReport.ClientID%>").show();
                $("#<%= tdReport.ClientID%>").show();
                $("#<%= tdReport1.ClientID%>").show();

            }
            if ($('#<%=rdoDetail.ClientID %>').attr('checked') == true) {
                $("#<%= ddlSubReports.ClientID%>").hide();
                $("#<%= ddlReport.ClientID%>").hide();
                $("#<%= tdReport1.ClientID%>").hide();
            }
            $('#<%=rdoSummary.ClientID %>').click(function () {
                $("#<%= ddlSubReports.ClientID%>").show();
                $("#<%= ddlReport.ClientID%>").show();
                $("#<%= tdReport.ClientID%>").show();
                $("#<%= tdReport1.ClientID%>").show();
            });
            $('#<%=rdoDetail.ClientID %>').click(function () {
                $("#<%= ddlSubReports.ClientID%>").hide();
                $("#<%= ddlReport.ClientID%>").hide();
                $("#<%= tdReport.ClientID%>").hide();
                $("#<%= tdReport1.ClientID%>").hide();
            });
            $('#<%=btnGO.ClientID %>').click(function (e) {
               // debugger;
                $('#<%=txtToDate.ClientID %>').trigger("change");
                $('#<%=txtFromDate.ClientID %>').trigger("change");
                //                    var datefrom = document.getElementById('<%=txtFromDate.ClientID %>').value;
                //                    alert(dateto);
                //                    alert(datefrom);
                //                    document.getElementById('<%=hdnJSFromDt.ClientID %>').value = datefrom;
                //                    document.getElementById('<%=hdnJSToDt.ClientID %>').value = dateto;
            });
            //            $('#<%=btnBack.ClientID %>').click(function (e) {
            //                if ($("#<%= hdnDetailViewCheck.ClientID%>").val() == "Yes") {
            //                    $("#<%= tdReport.ClientID%>").hide();
            //                }
            //                else {
            //                    $("#<%= tdReport.ClientID%>").show();
            //                }
            //            });
            //            $("#<%= ddlReport.ClientID%>").change(function () {
            //                var newOption;
            //                var val = $("#<%= ddlReport.ClientID%>").val();
            //                $("#<%= ddlSubReports.ClientID%>").empty();
            //                if (val == "1") {
            //                    newOption = "<option value='" + "1" + "'>By Planner/Site</option> <option value='" + "2" + "'>By Planner/Type</option> <option value='" + "3" + "'>By Planner</option> <option value='" + "4" + "'>By Category/Site</option>";
            //                    $("#<%= ddlSubReports.ClientID%>").append(newOption);
            //                }
            //                else if (val == "2") {
            //                    newOption = "<option value='" + "1" + "'>By AP Clerk/Site</option> <option value='" + "2" + "'>By AP Clerk/Type</option> <option value='" + "3" + "'>By AP Clerk</option>";
            //                    $("#<%= ddlSubReports.ClientID%>").append(newOption);
            //                }
            //                else if (val == "3") {
            //                    newOption = "<option value='" + "1" + "'>By Site/Type</option> <option value='" + "2" + "'>By Site/Department</option> <option value='" + "3" + "'>By Site/Status</option>";
            //                    $("#<%= ddlSubReports.ClientID%>").append(newOption);
            //                }
            //                else if (val == "4") {
            //                    newOption = "<option value='" + "1" + "'>By Vendor/Type</option> <option value='" + "2" + "'>By Vendor/Site</option> <option value='" + "3" + "'>By Site/Status</option>";
            //                    $("#<%= ddlSubReports.ClientID%>").append(newOption);
            //                }
            //            });

            //            if ($('#<%= rdoSummary.ClientID %>').attr('checked') == true) {
            ////                var newOption = "<option value='" + "1" + "'>Inventory Summary</option> <option value='" + "2" + "'>Accounts Payable Summary</option> <option value='" + "3" + "'>Site Summary</option> <option value='" + "4" + "'>Vendor Summary</option>";
            ////                $("#<%= ddlReport.ClientID%>").append(newOption);
            //                newOption = '';
            //                newOption = "<option value='" + "1" + "'>By Planner/Site</option> <option value='" + "2" + "'>By Planner/Type</option> <option value='" + "3" + "'>By Planner</option> <option value='" + "4" + "'>By Category/Site</option>";
            //                $("#<%= ddlSubReports.ClientID%>").append(newOption);
            //            }
        })
    </script>
    <asp:ScriptManager ID="SM1" runat="server">
    </asp:ScriptManager>
    <asp:HiddenField ID="hdnJSFromDt" runat="server" />
    <asp:HiddenField ID="hdnJSToDt" runat="server" />
     <asp:HiddenField ID="hdnDetailViewCheck" runat="server" />
    <asp:TextBox ID="txtDateMax" Visible="false" runat="server" />
    <div class="right-shadow">
        <div class="formbox">
            <asp:UpdatePanel ID="pnlUP1" runat="server">
                <ContentTemplate>
                    <table width="100%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings"
                        id="tbl" runat="server">
                         <tr>
                    <td align="right" colspan="6">
                        <cc1:VendorSelectTemplate runat="server" ID="ucVendorTemplateSelect" />
                    </td>
                </tr>
                        <tr>
                            <td style="font-weight: bold;">
                                <cc1:ucLabel ID="lblCountry" runat="server" Text="Country"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold;">
                                :
                            </td>
                            <td>
                                <cc1:MultiSelectCountry runat="server" ID="msCountry" />
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold;">
                                <cc1:ucLabel ID="lblSite" runat="server" Text="Site"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold;">
                                :
                            </td>
                            <td>
                                <cc1:MultiSelectSite runat="server" ID="msSite" />
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold;">
                                <cc1:ucLabel ID="lblStockPlanner" runat="server" Text="Stock Planner"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold;">
                                :
                            </td>
                            <td>
                                <cc1:MultiSelectStockPlanner runat="server" ID="msStockPlanner" />
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold;">
                                <cc1:ucLabel ID="lblVendor" runat="server" Text="Vendor"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold;">
                                :
                            </td>
                            <td>
                                <cc1:MultiSelectVendor runat="server" ID="msVendor" />
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold;">
                                <cc1:ucLabel ID="lblAccountsPayable" runat="server" Text="Accounts Payable"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold;">
                                :
                            </td>
                            <td>
                                <cc1:MultiSelectAP runat="server" ID="msAP" />
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold;">
                                <cc1:ucLabel ID="lblReportType" runat="server" Text="Report Type"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold;">
                                :
                            </td>
                            <td>
                                <table>
                                    <tr>
                                        <td style="font-weight: bold; width: 8em;">
                                            <cc1:ucRadioButton ID="rdoSummary" runat="server" Text="Summary" GroupName="ReportType"
                                                Checked="true" AutoPostBack="true" OnCheckedChanged="rdoSummary_CheckedChanged" />
                                        </td>
                                        <td style="font-weight: bold; width: 8em;">
                                            <cc1:ucRadioButton ID="rdoDetail" runat="server" Text="Detailed" AutoPostBack="true"
                                                GroupName="ReportType" oncheckedchanged="rdoDetail_CheckedChanged" />
                                        </td>
                                    </tr>
                            </td>
                        </tr>
                        <%-- <tr id="trType1" runat="server" style="display:none;">
                                <td style="font-weight: bold; width: 12em;">
                                    <cc1:ucRadioButton ID="rdoInventorySummary" runat="server" Text="Inventory Summary" GroupName="ReportType2"
                                     Checked="true" />
                                </td>
                                <td style="font-weight: bold; width: 16em;">
                                    <cc1:ucRadioButton ID="rdoAPSummary" runat="server" Text="Accounts Payable Summary" GroupName="ReportType2" />
                                </td>      
                                 <td style="font-weight: bold; width: 11em;">
                                    <cc1:ucRadioButton ID="rdoSite" runat="server" Text="Site Summary" GroupName="ReportType2" />
                                 </td>     
                                 <td style="font-weight: bold; " class="style1">
                                    <cc1:ucRadioButton ID="rdoVendor" runat="server" Text="Vendor Summary" GroupName="ReportType2" />
                                 </td>                          
                            </tr>

                              <tr id="trType2" runat="server" style="display:none;">
                                <td style="font-weight: bold; width: 12em;">
                                    <cc1:ucRadioButton ID="rdoInventorySummary_1" runat="server" Text="Inventory Summary" GroupName="ReportType1"
                                     Checked="true" />
                                </td>
                                <td style="font-weight: bold; width: 6em;">
                                    <cc1:ucRadioButton ID="rdoSite_1" runat="server" Text="By Site " GroupName="ReportType1" />
                                </td>      
                                 <td style="font-weight: bold; width: 6em;">
                                    <cc1:ucRadioButton ID="rdoUser" runat="server" Text="By User" GroupName="ReportType1" />
                                 </td>     
                                 <td style="font-weight: bold; " class="style1">
                                    <cc1:ucRadioButton ID="rdoDepartment" runat="server" Text="By Department" GroupName="ReportType1" />
                                 </td>   
                                  <td style="font-weight: bold; width: 8em;">
                                    <cc1:ucRadioButton ID="rdoByVendor" runat="server" Text="By Vendor" GroupName="ReportType1" />
                                 </td>                          
                            </tr>

                             <tr id="trType3" runat="server" style="display:none;">
                                <td style="font-weight: bold; width: 10em;">
                                    <cc1:ucRadioButton ID="rdoPlannerbySite" runat="server" Text="By Planner/Site" GroupName="ReportType3"
                                     Checked="true" />
                                </td>
                                <td style="font-weight: bold; width: 10em;">
                                    <cc1:ucRadioButton ID="rdoPlannerType" runat="server" Text="By Planner/Type" GroupName="ReportType3" />
                                </td>      
                                 <td style="font-weight: bold; width: 8em;">
                                    <cc1:ucRadioButton ID="rdoPlanner" runat="server" Text="By Planner" GroupName="ReportType3" />
                                 </td>     
                                 <td style="font-weight: bold; " class="style1">
                                    <cc1:ucRadioButton ID="rdoCategoryBySite" runat="server" Text="By Category/Site" GroupName="ReportType3" />
                                 </td>   
                                  <td style="font-weight: bold; width: 14em;">
                                    <cc1:ucRadioButton ID="rdoCategoryByDisc" runat="server" Text="By Category/Disc Type" GroupName="ReportType3" />
                                 </td>                          
                            </tr>


                            <tr id="trType4" runat="server" style="display:none;">
                                <td style="font-weight: bold; width: 10em;">
                                    <cc1:ucRadioButton ID="rdoClerkBySite" runat="server" Text="By AP Clerk/Site" GroupName="ReportType4"
                                     Checked="true" />
                                </td>
                                <td style="font-weight: bold; width: 12em;">
                                    <cc1:ucRadioButton ID="rdoClerkByType" runat="server" Text="By AP Clerk/Type" GroupName="ReportType4" />
                                </td>      
                                 <td style="font-weight: bold; width: 8em;">
                                    <cc1:ucRadioButton ID="rdoByAPClerk" runat="server" Text="By AP Clerk " GroupName="ReportType4" />
                                 </td>                                                             
                            </tr>

                            <tr id="trType5" runat="server" style="display:none;">
                             <td style="font-weight: bold; width: 8em;">
                                    <cc1:ucRadioButton ID="rdoBySiteType" runat="server" Text="By Site/Type" GroupName="ReportType5"  Checked="true"/>
                                 </td>
                                <td style="font-weight: bold; width: 12em;">
                                    <cc1:ucRadioButton ID="rdoSiteByDepartment" runat="server" Text="By Site/Department" GroupName="ReportType5"
                                     />
                                </td>
                                <td style="font-weight: bold; width: 12em;">
                                    <cc1:ucRadioButton ID="rdoSiteByStatus" runat="server" Text="By Site/Status" GroupName="ReportType5" />
                             </td>      
                                                                                             
                            </tr>

                          <tr id="trType6" runat="server" style="display:none;">
                             <td style="font-weight: bold; width: 10em;">
                                    <cc1:ucRadioButton ID="rdoVendorByType" runat="server" Text="By Vendor/Type" GroupName="ReportType6"  Checked="true"/>
                                 </td>
                                <td style="font-weight: bold; width: 12em;">
                                    <cc1:ucRadioButton ID="rdoVendorBySite" runat="server" Text="By Vendor/Site" GroupName="ReportType6"
                                     />
                                </td>
                                <td style="font-weight: bold; width: 12em;">
                                    <cc1:ucRadioButton ID="rdoVendorSiteByStatus" runat="server" Text="By Site/Status" GroupName="ReportType6" />
                             </td>      
                                                                                             
                            </tr>        --%>
                        <tr>
                            <td style="width: 14em;">
                                <br />
                                <cc1:ucDropdownList ID="ddlReport" runat="server" Width="110%" AutoPostBack="true"
                                    OnSelectedIndexChanged="ddlReport_SelectedIndexChanged">
                                </cc1:ucDropdownList>
                            </td>
                        </tr>
                    </table>
                    <br />
                    <table id="tblsearch" runat="server">
                        <tr>
                            <td style="font-weight: bold; width: 6em;">
                                <cc1:ucLabel ID="lblDateFrom_1" runat="server" Text="Date From"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold; width: 1em;">
                                <cc1:ucTextbox ID="txtFromDate" ClientIDMode="Static" runat="server" onchange="setValue1(this)"
                                    ReadOnly="True" Width="70px" />
                            </td>
                            <td style="font-weight: bold; width: 5em; text-align: center">
                                <cc1:ucLabel ID="lblTo" runat="server" Text="To"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold; width: 8em;">
                                <cc1:ucTextbox ID="txtToDate" ClientIDMode="Static" runat="server" onchange="setValue2(this)"
                                    ReadOnly="True" Width="70px" />
                            </td>
                            <td style="font-weight: bold; text-align: center; width: 10em;">
                                 <cc1:ucLabel ID="lblActionStatus" runat="server" Text="Action Status"></cc1:ucLabel>                                
                            </td>
                            <td style="font-weight: bold; text-align: center; width: 8em;">
                                <cc1:ucDropdownList ID="ddlStatus" runat="server" Width="100 px">
                                  <%--  <asp:ListItem Text="All" Selected="True" Value="A" />
                                    <asp:ListItem Text="Open" Value="O" />
                                    <asp:ListItem Text="Closed" Value="C" />--%>
                                    <%--<asp:ListItem Text="Force Closed" Value="F" />--%>
                                </cc1:ucDropdownList>
                            </td>
                            <td style="font-weight: bold; text-align: center; width: 8em;" id="tdReport" runat="server">
                              <cc1:ucLabel ID="lblReportType_1" runat="server" Text="Report Type"></cc1:ucLabel>:
                            </td>
                            <td id="tdReport1" runat="server">
                                <cc1:ucDropdownList ID="ddlSubReports" runat="server" Width="100%" AutoPostBack="true" OnSelectedIndexChanged="ddlSubReports_OnSelectedIndexChanged">                                
                                </cc1:ucDropdownList>
                            </td>
                            
                            <td>&nbsp;&nbsp;<cc1:ucButton ID="btnGO" runat="server" CssClass="button"
                            OnClick="btnGenerateReport_Click" /></td>
                        </tr>                       
                    </table>
                    </table> 
                    <%--<div class="button-row">
                        
                    </div>--%>
                    <div id="divGrid" runat="server" visible="false">
                        <%--<div class="button-row">--%>
                        <br />
                             <cc2:ucExportToExcel ID="ucExportToExcel1" runat="server" Visible="false" />                             
                            <cc1:ucButton ID="btnExportToExcel" runat="server" Visible="false" CssClass="exporttoexcel" OnClick="btnExport_Click"
                             Width="109px" Height="20px" />
                           
                     <%-- </div>--%>
                     <%--   <div class="button-row">--%>
                            <table border="0" cellpadding="0" cellspacing="0" id="tblSummary" runat="server" visible="false" width="100%">
                            <tr>
                            <td colspan="7" align="left"><div id="divSummary" runat="server" visible="false">     
                            <br />                      
                            <cc1:ucRadioButton ID="rdoType" runat="server" Text="Type" GroupName="ReportType1"
                                Checked="true" AutoPostBack="true" OnCheckedChanged="rdoType_CheckedChanged" />
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <cc1:ucRadioButton ID="rdoStatus" runat="server" Text="Status" AutoPostBack="true"
                                GroupName="ReportType1" OnCheckedChanged="rdoStatus_CheckedChanged" />
                        </div>  <br /><br /> </td>
                            </tr>
                                <tr>                                
                                    <td style="width:15%" align="left"><b><cc1:ucLabel ID="lblName" runat="server" /></b></td>
                                    
                                    <td style="width:5px" align="left"><cc1:ucLabel ID="lblStckPlannerName" runat="server" /> </td>
                                    
                                    <td style="width:5px" align="left"><b><cc1:ucLabel ID="lblDateFrom" runat="server" /></b></td>
                                   
                                    <td style="width:3px" align="left"><cc1:ucLabel ID="lblDateFromValue" runat="server" /> </td>
                                    
                                    <td style="width:5px" align="left"><b><cc1:ucLabel ID="lblDateTo" runat="server" /></b></td>
                                    
                                    <td style="width:5px" align="left"><cc1:ucLabel ID="lblDateToValue" runat="server" /> </td>
                                </tr>                                                
                            </table>
                           
                       <%-- </div>--%>
                           <style type="text/css">
                                                                           
                             </style>
                        <div class="wmd-view-topscroll">
                            <div class="scroll-div">
                                &nbsp;
                            </div>
                        </div>
                        <div class="wmd-view">
                            <div class="dynamic-div">
                                <cc1:ucGridView ID="grdBind" ClientIDMode="Static" Width="100%" Height="80%" runat="server"
                                    AllowPaging="true" PageSize="200" CssClass="grid modify-grid" GridLines="Both" AutoGenerateColumns="true" EnableViewState="false"
                                     OnPageIndexChanging="OnPageIndexChanging" OnRowDataBound="grdBind_RowDataBound">
                                    <RowStyle HorizontalAlign="Center"></RowStyle>
                                </cc1:ucGridView>
                            </div>
                            <asp:Label ID="lblError" runat="server" Visible="false" />
                        </div>
                        <cc1:ucGridView ID="grdBindExport" Visible="false" ClientIDMode="Static" Width="100%"
                            Height="80%" runat="server" CssClass="grid" GridLines="Both" AutoGenerateColumns="true">                          
                            
                        </cc1:ucGridView>
                <div class="button-row" >
                    <cc1:PagerV2_8 ID="pager1" runat="server" OnCommand="pager_Command" GenerateGoToSection="false">
                    </cc1:PagerV2_8>
                </div>
                        <div class="button-row">
                            <br />
                            <br />
                            <cc1:ucButton ID="btnBack" runat="server" Text="Back" align="right" Visible="false"
                                CssClass="button" OnClick="btnBack_Click" />
                        </div>
                 </div>
                   
                </ContentTemplate>
                <Triggers>                
                    <asp:PostBackTrigger ControlID="btnGO" />
                      <asp:PostBackTrigger ControlID="ucExportToExcel1" />
                       <asp:PostBackTrigger ControlID="btnExportToExcel" />
                       <asp:PostBackTrigger ControlID="btnBack" />
                </Triggers>
            </asp:UpdatePanel>
     </div>
        </div>
</asp:Content>

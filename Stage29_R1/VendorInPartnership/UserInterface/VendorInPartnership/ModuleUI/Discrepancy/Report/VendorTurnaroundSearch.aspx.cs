﻿using System;
using System.Data;
using BusinessEntities.ModuleBE.Discrepancy.Report;
using BusinessLogicLayer.ModuleBAL.Discrepancy.Report;
using Utilities; using WebUtilities;
using Microsoft.Reporting.WebForms;
using BusinessEntities.ModuleBE.Security;
using BusinessLogicLayer.ModuleBAL.Security;
using System.Web.UI.WebControls;
using System.Collections.Generic;

public partial class VendorTurnaroundSearch : CommonPage
{
    protected void Page_Init(object sender, EventArgs e)
    {
        ucVendorTemplateSelect.CurrentPage = this;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
       if (!IsPostBack)
        {
        txtFromDate.Text = DateTime.Now.AddDays(-DateTime.Now.Day + 1).ToString("dd/MM/yyyy");
        txtToDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
        txtFromDate.Attributes.Add("readonly", "readonly");
        txtToDate.Attributes.Add("readonly", "readonly");
        }
    }
    /// <summary>
    /// Method to show Vendor on the basis of selected Template
    /// </summary>
    public override void TemplateSelectedIndexChanged()
    {
        base.TemplateSelectedIndexChanged();

        int VendorTemplateId = Convert.ToInt32(ucVendorTemplateSelect.innerControlddlTemplate.SelectedValue);

        ListBox lstSelectedVendor = (ListBox)msVendor.FindControl("lstRight");
        ListBox lstVendor = (ListBox)msVendor.FindControl("ucVendor").FindControl("lstLeft");

        if (VendorTemplateId > 0)
        {

            SCT_TemplateBAL oSCT_TemplateBAL = new SCT_TemplateBAL();
            SCT_TemplateBE oSCT_TemplateBE = new SCT_TemplateBE();

            oSCT_TemplateBE.VendorTemplateID = VendorTemplateId;
            oSCT_TemplateBE.Action = "GetVendorTemplateInfobyID";
            List<SCT_TemplateBE> lstVendorTemplate = oSCT_TemplateBAL.GetVendorTemplateById(oSCT_TemplateBE);

            if (lstVendorTemplate != null && lstVendorTemplate.Count > 0)
            {
                lstSelectedVendor.Items.Clear();
                msVendor.innerControlHiddenField.Value = string.Empty;
                msVendor.innerControlHiddenField2.Value = string.Empty;
                for (int iCount = 0; iCount < lstVendorTemplate.Count; iCount++)
                {
                    lstSelectedVendor.Items.Add(new ListItem(lstVendorTemplate[iCount].Vendor.Vendor_Name, lstVendorTemplate[iCount].Vendor.VendorID.ToString()));
                    msVendor.SelectedVendorIDs = lstVendorTemplate[iCount].Vendor.VendorID.ToString();
                    msVendor.SelectedVendorName = lstVendorTemplate[iCount].Vendor.Vendor_Name.ToString();
                }
            }
        }
        else
        {
            lstSelectedVendor.Items.Clear();
            msVendor.innerControlHiddenField.Value = string.Empty;
            msVendor.innerControlHiddenField2.Value = string.Empty;
        }
    }

    protected void btnGenerateReport_Click(object sender, EventArgs e)
    {
        DiscrepancyReportBE oDiscrepancyReportBE = new DiscrepancyReportBE();
        DiscrepancyReportBAL oDiscrepancyReportBAL = new DiscrepancyReportBAL();

        oDiscrepancyReportBE.SelectedCountryIDs = msCountry.SelectedCountryIDs;
        oDiscrepancyReportBE.SelectedSiteIDs = msSite.SelectedSiteIDs;
        oDiscrepancyReportBE.SelectedVendorIDs = msVendor.SelectedVendorIDs;

        oDiscrepancyReportBE.DateFrom = string.IsNullOrEmpty(txtFromDate.Text) ? (DateTime?)null : Common.GetMM_DD_YYYY(txtFromDate.Text);
        oDiscrepancyReportBE.DateTo = string.IsNullOrEmpty(txtToDate.Text) ? (DateTime?)null : Common.GetMM_DD_YYYY(txtToDate.Text);

        oDiscrepancyReportBE.Action = "VendorTurnaround";

        ReportParameter[] reportParameter = new ReportParameter[5];
        reportParameter[0] = new ReportParameter("Country", msCountry.SelectedCountryName);
        reportParameter[1] = new ReportParameter("Site", msSite.SelectedSiteName);
        reportParameter[2] = new ReportParameter("Vendor", msVendor.SelectedVendorName);  
        reportParameter[3] = new ReportParameter("DateFrom", txtFromDate.Text);
        reportParameter[4] = new ReportParameter("DateTo", txtToDate.Text);
        Session["ReportParameter"] = reportParameter;




        DataTable dtRptDIS_Report = null;

        dtRptDIS_Report = oDiscrepancyReportBAL.getDiscrepancyReportBAL(oDiscrepancyReportBE).Tables[0];
        oDiscrepancyReportBAL = null;
        Session["DataSetObject"] = dtRptDIS_Report;
        Session["DateTableName"] = "dtVendorTurnaround";
        Session["ReportPath"] = "\\ModuleUI\\Discrepancy\\Report\\RDLC\\VendorTurnaround.rdlc";

        EncryptQueryString("../../ReportView.aspx?PageName=" + "VTS");

    }
}
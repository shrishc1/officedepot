﻿using System;
using System.Data;
using BusinessEntities.ModuleBE.Discrepancy.Report;
using BusinessLogicLayer.ModuleBAL.Discrepancy.Report;
using Microsoft.Reporting.WebForms;
using System.Web.UI.WebControls;
using BusinessLogicLayer.ModuleBAL.Security;
using BusinessEntities.ModuleBE.Security;
using System.Collections.Generic;

public partial class DiscrepancyTrackingReportSearch : CommonPage
{
    protected void Page_Init(object sender, EventArgs e)
    {
        ucVendorTemplateSelect.CurrentPage = this;
    }

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    /// <summary>
    /// Method to show Vendor on the basis of selected Template
    /// </summary>
    public override void TemplateSelectedIndexChanged()
    {
        base.TemplateSelectedIndexChanged();

        int VendorTemplateId = Convert.ToInt32(ucVendorTemplateSelect.innerControlddlTemplate.SelectedValue);

        ListBox lstSelectedVendor = (ListBox)msVendor.FindControl("lstRight");
        ListBox lstVendor = (ListBox)msVendor.FindControl("ucVendor").FindControl("lstLeft");

        if (VendorTemplateId > 0)
        {

            SCT_TemplateBAL oSCT_TemplateBAL = new SCT_TemplateBAL();
            SCT_TemplateBE oSCT_TemplateBE = new SCT_TemplateBE();

            oSCT_TemplateBE.VendorTemplateID = VendorTemplateId;
            oSCT_TemplateBE.Action = "GetVendorTemplateInfobyID";
            List<SCT_TemplateBE> lstVendorTemplate = oSCT_TemplateBAL.GetVendorTemplateById(oSCT_TemplateBE);

            if (lstVendorTemplate != null && lstVendorTemplate.Count > 0)
            {
                lstSelectedVendor.Items.Clear();
                msVendor.innerControlHiddenField.Value = string.Empty;
                msVendor.innerControlHiddenField2.Value = string.Empty;
                for (int iCount = 0; iCount < lstVendorTemplate.Count; iCount++)
                {
                    lstSelectedVendor.Items.Add(new ListItem(lstVendorTemplate[iCount].Vendor.Vendor_Name, lstVendorTemplate[iCount].Vendor.VendorID.ToString()));
                    msVendor.SelectedVendorIDs = lstVendorTemplate[iCount].Vendor.VendorID.ToString();
                    msVendor.SelectedVendorName = lstVendorTemplate[iCount].Vendor.Vendor_Name.ToString();
                }
            }
        }
        else
        {
            lstSelectedVendor.Items.Clear();
            msVendor.innerControlHiddenField.Value = string.Empty;
            msVendor.innerControlHiddenField2.Value = string.Empty;
        }
    }

    protected void btnGenerateReport_Click(object sender, EventArgs e)
    {
        DiscrepancyReportBE oDiscrepancyReportBE = new DiscrepancyReportBE();
        DiscrepancyReportBAL oDiscrepancyReportBAL = new DiscrepancyReportBAL();

        oDiscrepancyReportBE.SelectedAPIDs = msAP.SelectedAPIDs;
        oDiscrepancyReportBE.SelectedCountryIDs = msCountry.SelectedCountryIDs;
        oDiscrepancyReportBE.SelectedSiteIDs = msSite.SelectedSiteIDs;
        oDiscrepancyReportBE.SelectedStockPlannerIDs = msStockPlanner.SelectedStockPlannerIDs;
        oDiscrepancyReportBE.SelectedVendorIDs = msVendor.SelectedVendorIDs;

        ReportParameter[] reportParameter = new ReportParameter[5];
        reportParameter[0] = new ReportParameter("AccountsPayable", msAP.SelectedAPName);
        reportParameter[1] = new ReportParameter("Country", msCountry.SelectedCountryName);
        reportParameter[2] = new ReportParameter("Site", msSite.SelectedSiteName);
        reportParameter[3] = new ReportParameter("StockPlanner", msStockPlanner.SelectedSPName);
        reportParameter[4] = new ReportParameter("Vendor", msVendor.SelectedVendorName); 

        Session["ReportParameter"] = reportParameter;

        if (rdoSummary.Checked)
        {
            oDiscrepancyReportBE.Action = "DiscrepancyTrackingReport_TotalsOverview";
            Session["ReportPath"] = "\\ModuleUI\\Discrepancy\\Report\\RDLC\\DiscrepancyTrackingReport_TotalsOverview.rdlc";
            Session["DateTableName"] = "dtFunctionalVDRTracker1";
        }
        else if (rdoDetail.Checked)
        {
            oDiscrepancyReportBE.Action = "DiscrepancyTrackingReport_DetailOverview_IndividualOverview";
            Session["ReportPath"] = "\\ModuleUI\\Discrepancy\\Report\\RDLC\\DiscrepancyTrackingReport_IndividualOverview.rdlc";
            Session["DateTableName"] = "dtVDRFunctionalReportDetail";
        }

        if (rdoListing.Checked) 
        {
            oDiscrepancyReportBE.Action = "DiscrepancyTrackingReport_ListOverview_IndividualOverview";
            Session["ReportPath"] = "\\ModuleUI\\Discrepancy\\Report\\RDLC\\DiscrepancyTrackingReport_ListOverview.rdlc";
            Session["DateTableName"] = "DataSet1";
        }


        DataSet dsFunctionalVDRTracker = null;
        dsFunctionalVDRTracker = oDiscrepancyReportBAL.getDiscrepancyReportBAL(oDiscrepancyReportBE);
        oDiscrepancyReportBAL = null;
        Session["DataSetObject"] = dsFunctionalVDRTracker.Tables[0];


        EncryptQueryString("../../ReportView.aspx?PageName=" + "DTRS");
       
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System;
using System.Data;
using BusinessEntities.ModuleBE.Discrepancy.Report;
using BusinessLogicLayer.ModuleBAL.Discrepancy.Report;
using Utilities;
using WebUtilities;
using Microsoft.Reporting.WebForms;
using BusinessLogicLayer.ModuleBAL.Security;
using BusinessEntities.ModuleBE.Security;

public partial class DeletedDiscrepanciesReport : CommonPage
{
    protected void Page_InIt(object sender, EventArgs e) 
    {
        msUser.SetActionText = "GetUsersForDeletedDiscripancy";
        ucVendorTemplateSelect.CurrentPage = this;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            txtFromDate.Text = DateTime.Now.AddDays(-DateTime.Now.Day + 1).ToString("dd/MM/yyyy");
            txtToDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
            txtFromDate.Attributes.Add("readonly", "readonly");
            txtToDate.Attributes.Add("readonly", "readonly");
        }

        TextBox txtDiscrepancyNo = Utilities.Common.FindControlRecursive(this.Page.Controls[0], "txtDiscrepancyNo") as TextBox;
        if (txtDiscrepancyNo != null)
            txtDiscrepancyNo.Focus();
    }

    /// <summary>
    /// Method to show Vendor on the basis of selected Template
    /// </summary>
    public override void TemplateSelectedIndexChanged()
    {
        base.TemplateSelectedIndexChanged();

        int VendorTemplateId = Convert.ToInt32(ucVendorTemplateSelect.innerControlddlTemplate.SelectedValue);

        ListBox lstSelectedVendor = (ListBox)msVendor.FindControl("lstRight");
        ListBox lstVendor = (ListBox)msVendor.FindControl("ucVendor").FindControl("lstLeft");

        if (VendorTemplateId > 0)
        {

            SCT_TemplateBAL oSCT_TemplateBAL = new SCT_TemplateBAL();
            SCT_TemplateBE oSCT_TemplateBE = new SCT_TemplateBE();

            oSCT_TemplateBE.VendorTemplateID = VendorTemplateId;
            oSCT_TemplateBE.Action = "GetVendorTemplateInfobyID";
            List<SCT_TemplateBE> lstVendorTemplate = oSCT_TemplateBAL.GetVendorTemplateById(oSCT_TemplateBE);

            if (lstVendorTemplate != null && lstVendorTemplate.Count > 0)
            {
                lstSelectedVendor.Items.Clear();
                msVendor.innerControlHiddenField.Value = string.Empty;
                msVendor.innerControlHiddenField2.Value = string.Empty;
                for (int iCount = 0; iCount < lstVendorTemplate.Count; iCount++)
                {
                    lstSelectedVendor.Items.Add(new ListItem(lstVendorTemplate[iCount].Vendor.Vendor_Name, lstVendorTemplate[iCount].Vendor.VendorID.ToString()));
                    msVendor.SelectedVendorIDs = lstVendorTemplate[iCount].Vendor.VendorID.ToString();
                    msVendor.SelectedVendorName = lstVendorTemplate[iCount].Vendor.Vendor_Name.ToString();
                }
            }
        }
        else
        {
            lstSelectedVendor.Items.Clear();
            msVendor.innerControlHiddenField.Value = string.Empty;
            msVendor.innerControlHiddenField2.Value = string.Empty;
        }
    }

    protected void btnGenerateReport_Click(object sender, EventArgs e)
    {
        var oDiscrepancyReportBE = new DiscrepancyReportBE();
        var oDiscrepancyReportBAL = new DiscrepancyReportBAL();

        oDiscrepancyReportBE.Action = "DeletedDiscrepanciesReport";
        if (!string.IsNullOrEmpty(msDiscrepancy.SelectedDiscrepancy))
            oDiscrepancyReportBE.SelectedDiscrepancyNos = msDiscrepancy.SelectedDiscrepancy;

        if (!string.IsNullOrEmpty(msCountry.SelectedCountryIDs))
            oDiscrepancyReportBE.SelectedCountryIDs = msCountry.SelectedCountryIDs;

        if (!string.IsNullOrEmpty(msSite.SelectedSiteIDs))
            oDiscrepancyReportBE.SelectedSiteIDs = msSite.SelectedSiteIDs;

        if (!string.IsNullOrEmpty(msVendor.SelectedVendorIDs))
            oDiscrepancyReportBE.SelectedVendorIDs = msVendor.SelectedVendorIDs;

        if (!string.IsNullOrEmpty(msUser.SelectedUserIDs))
            oDiscrepancyReportBE.SelectedUserIDs = msUser.SelectedUserIDs;

        oDiscrepancyReportBE.DateTo = string.IsNullOrEmpty(txtToDate.Text) ? (DateTime?)null : Common.GetMM_DD_YYYY(txtToDate.Text);
        oDiscrepancyReportBE.DateFrom = string.IsNullOrEmpty(txtFromDate.Text) ? (DateTime?)null : Common.GetMM_DD_YYYY(txtFromDate.Text);
        DataSet dsFunctionalVDRTracker = oDiscrepancyReportBAL.GetDeletedDiscrepanciesReportBAL(oDiscrepancyReportBE);
        Session["DataSetObject"] = dsFunctionalVDRTracker.Tables[0];
        oDiscrepancyReportBAL = null;

        ReportParameter[] reportParameter = new ReportParameter[7];
        reportParameter[0] = new ReportParameter("Discrepancy", msDiscrepancy.SelectedDiscrepancy);
        reportParameter[1] = new ReportParameter("Country", msCountry.SelectedCountryName);
        reportParameter[2] = new ReportParameter("Site", msSite.SelectedSiteName);
        reportParameter[3] = new ReportParameter("Vendor", msVendor.SelectedVendorName);
        reportParameter[4] = new ReportParameter("User", msUser.SelectedUserName);
        reportParameter[5] = new ReportParameter("DateFrom", txtFromDate.Text);
        reportParameter[6] = new ReportParameter("DateTo", txtToDate.Text);
        Session["ReportParameter"] = reportParameter;

        Session["ReportPath"] = "\\ModuleUI\\Discrepancy\\Report\\RDLC\\DeletedDiscrepancyReport.rdlc";
        Session["DateTableName"] = "dsDeletedDiscrepencyDataSet";
        EncryptQueryString("../../ReportView.aspx?PageName=" + "DDR");
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLogicLayer.ModuleBAL.Security;
using BusinessEntities.ModuleBE.Security;
using BusinessEntities.ModuleBE.Discrepancy.Report;
using BusinessLogicLayer.ModuleBAL.Discrepancy.Report;
using System.Data;
using Utilities;
using WebUtilities;
public partial class ModuleUI_Discrepancy_Report_VDRTrackerReportNew : CommonPage
{
    protected string RecordNotFound = WebCommon.getGlobalResourceValue("RecordNotFound");
    bool IsExportClicked;
    int RecordCountforPaging;
    protected void Page_Init(object sender, EventArgs e)
    {
        ucVendorTemplateSelect.CurrentPage = this;
        msVendor.VendorFillOnSearch = true;
    }
    /// <summary>
    /// Method to show Vendor on the basis of selected Template
    /// </summary>
    public override void TemplateSelectedIndexChanged()
    {
        base.TemplateSelectedIndexChanged();

        int VendorTemplateId = Convert.ToInt32(ucVendorTemplateSelect.innerControlddlTemplate.SelectedValue);

        ListBox lstSelectedVendor = (ListBox)msVendor.FindControl("lstRight");
        ListBox lstVendor = (ListBox)msVendor.FindControl("ucVendor").FindControl("lstLeft");

        if (VendorTemplateId > 0)
        {

            SCT_TemplateBAL oSCT_TemplateBAL = new SCT_TemplateBAL();
            SCT_TemplateBE oSCT_TemplateBE = new SCT_TemplateBE();

            oSCT_TemplateBE.VendorTemplateID = VendorTemplateId;
            oSCT_TemplateBE.Action = "GetVendorTemplateInfobyID";
            List<SCT_TemplateBE> lstVendorTemplate = oSCT_TemplateBAL.GetVendorTemplateById(oSCT_TemplateBE);

            if (lstVendorTemplate != null && lstVendorTemplate.Count > 0)
            {
                lstSelectedVendor.Items.Clear();
                msVendor.innerControlHiddenField.Value = string.Empty;
                msVendor.innerControlHiddenField2.Value = string.Empty;
                for (int iCount = 0; iCount < lstVendorTemplate.Count; iCount++)
                {
                    lstSelectedVendor.Items.Add(new ListItem(lstVendorTemplate[iCount].Vendor.Vendor_Name, lstVendorTemplate[iCount].Vendor.VendorID.ToString()));
                    msVendor.SelectedVendorIDs = lstVendorTemplate[iCount].Vendor.VendorID.ToString();
                    msVendor.SelectedVendorName = lstVendorTemplate[iCount].Vendor.Vendor_Name.ToString();
                }
            }
        }
        else
        {
            lstSelectedVendor.Items.Clear();
            msVendor.innerControlHiddenField.Value = string.Empty;
            msVendor.innerControlHiddenField2.Value = string.Empty;
        }
    }



    protected void Page_Load(object sender, EventArgs e)
    {
        ucExportToExcel1.CurrentPage = this;
        ucExportToExcel1.GridViewControl = grdBindExport;
        ucExportToExcel1.IsAutoGenratedGridview = true;
        // ucExportToExcel1.FileName = "Discrepancy Reports-Summary";
        if (!IsPostBack)
        {
            lblDiscrepancyTrackingReport.Visible = true;
            lblDiscrepancyTrackingRpt.Visible = false;
            pager1.PageSize = 200;
            pager1.GenerateGoToSection = false;
            pager1.GeneratePagerInfoSection = false;
            pager1.Visible = false;

            txtFromDate.Text = DateTime.Now.AddDays(-DateTime.Now.Day + 1).ToString("dd/MM/yyyy");
            txtToDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
            txtDateMax.Text = txtToDate.Text;
            hdnJSFromDt.Value = txtFromDate.Text;
            hdnJSToDt.Value = txtToDate.Text;
            if (ddlReport.Items.Count == 0)
            {
                ddlReport.Items.Add(new ListItem("Inventory Summary", "1"));
                ddlReport.Items.Add(new ListItem("Accounts Payable Summary", "2"));
                ddlReport.Items.Add(new ListItem("Site Summary", "3"));
                ddlReport.Items.Add(new ListItem("Vendor Summary", "4"));

                ddlSubReports.Items.Add(new ListItem("By Planner/Site", "1"));
                ddlSubReports.Items.Add(new ListItem("By Planner/Type", "2"));
                ddlSubReports.Items.Add(new ListItem("By Planner/Status", "3"));
                ddlSubReports.Items.Add(new ListItem("By Category/Site", "4"));
                ddlSubReports.Items.Add(new ListItem("By Category/Type", "5"));
                ddlSubReports.Items.Add(new ListItem("By Category/Status", "6"));
            }
            if (ddlStatus.Items.Count == 0)
            {
                BindDDLreports();
            }
            if (GetQueryStringValue("SP") != null || GetQueryStringValue("AP") != null)
            {
                if (GetQueryStringValue("SP") != null)
                {
                    BindOnQueryString("SiteWiseDiscINV");
                }
                else if (GetQueryStringValue("AP") != null)
                {
                    BindOnQueryString("SiteWiseDiscAP");
                }
                tblsearch.Visible = false;
                btnGO.Visible = false;
            }
            else
            {
                tblsearch.Visible = true;
                btnGO.Visible = true;
                btnGO.Text = "Generate Report";
            }
            ddlStatus.Enabled = true;

        }
        else
        {
            txtFromDate.Text = hdnJSFromDt.Value;
            txtToDate.Text = hdnJSToDt.Value;
            lblDiscrepancyTrackingRpt.Text = "Discrepancy Tracking Report";
            msStockPlanner.setStockPlannerOnPostBack();
            msSite.setSitesOnPostBack();
            msVendor.setVendorsOnPostBack();
            msCountry.SetCountryOnPostBack();
            msAP.SetAPOnPostBack();
        }
        
        if (rdoSummary.Checked == true)
        {
            tdReport.Visible = true;
            tdReport1.Visible = true;
            if (ddlReport.SelectedItem.Text == "Inventory Summary")
            {
              //  lblDiscrepancyTrackingRpt.Text = "Inventory Summary";
                if (ddlSubReports.SelectedItem.Text == "By Planner/Site")
                {
                    ucExportToExcel1.FileName = "By Planner/Site";
                    
                }
                else if (ddlSubReports.SelectedItem.Text == "By Planner/Type")
                {
                    ucExportToExcel1.FileName = "By Planner/Type";
                }
                else if (ddlSubReports.SelectedItem.Text == "By Planner/Status")
                {
                    ucExportToExcel1.FileName = "By Planner/Status";
                }
                else if (ddlSubReports.SelectedItem.Text == "By Category/Site")
                {
                    ucExportToExcel1.FileName = "By Category/Site";
                }
                else if (ddlSubReports.SelectedItem.Text == "By Category/Type")
                {
                    ucExportToExcel1.FileName = "By Category/Type";
                }
                else if (ddlSubReports.SelectedItem.Text == "By Category/Status")
                {
                    ucExportToExcel1.FileName = "By Category/Status";
                }

            }
            else if (ddlReport.SelectedItem.Text == "Accounts Payable Summary")
            {
              //  lblDiscrepancyTrackingRpt.Text = "Accounts Payable Summary";
                if (ddlSubReports.SelectedItem.Text == "By AP Clerk/Site")
                {
                    ucExportToExcel1.FileName = "By AP Clerk/Site";
                }
                else if (ddlSubReports.SelectedItem.Text == "By AP Clerk/Type")
                {
                    ucExportToExcel1.FileName = "By AP Clerk/Type";
                }
                else if (ddlSubReports.SelectedItem.Text == "By AP Clerk/Status")
                {
                    ucExportToExcel1.FileName = "By AP Clerk/Status";
                }
            }
            else if (ddlReport.SelectedItem.Text == "Site Summary")
            {
               // lblDiscrepancyTrackingRpt.Text = "Site Summary";
                if (ddlSubReports.SelectedItem.Text == "By Site/Type")
                {
                    ucExportToExcel1.FileName = "By Site/Type";
                }
                else if (ddlSubReports.SelectedItem.Text == "By Site/Department")
                {
                    ucExportToExcel1.FileName = "By Site/Department";
                }
                else if (ddlSubReports.SelectedItem.Text == "By Site/Status")
                {
                    ucExportToExcel1.FileName = "By Site/Status";
                }
            }
            else if (ddlReport.SelectedItem.Text == "Vendor Summary")
            {
                //lblDiscrepancyTrackingRpt.Text = "Vendor Summary";
                if (ddlSubReports.SelectedItem.Text == "By Vendor/Type")
                {
                    ucExportToExcel1.FileName = "By Vendor/Type";
                }
                else if (ddlSubReports.SelectedItem.Text == "By Vendor/Site")
                {
                    ucExportToExcel1.FileName = "By Vendor/Site";
                }
                else if (ddlSubReports.SelectedItem.Text == "By Vendor/Status")
                {
                    ucExportToExcel1.FileName = "By Vendor/Status";
                }
            }

        }
        //if (IsPostBack)
        //{
        //    lblDiscrepancyTrackingRpt.Text = "";
        //}

    }


    //private string ConvertSortDirectionToSql(SortDirection sortDirection)
    //{
    //    string newSortDirection = String.Empty;

    //    switch (sortDirection)
    //    {
    //        case SortDirection.Ascending:
    //            newSortDirection = "ASC";
    //            break;

    //        case SortDirection.Descending:
    //            newSortDirection = "DESC";
    //            break;
    //    }

    //    return newSortDirection;
    //}

    //protected void gridView_Sorting(object sender, GridViewSortEventArgs e)
    //{
    //    DataTable dataTable = grdBind.DataSource as DataTable;

    //    if (dataTable != null)
    //    {
    //        DataView dataView = new DataView(dataTable);
    //        dataView.Sort = e.SortExpression + " " + ConvertSortDirectionToSql(e.SortDirection);

    //        grdBind.DataSource = dataView;
    //        grdBind.DataBind();
    //    }
    //}   
    
    public void BindOnQueryString(string action)
    {
        tblSummary.Visible = true;
        divSummary.Visible = true;
        divGrid.Visible = true;
        btnBack.Visible = true;
        lblDiscrepancyTrackingReport.Visible = false;
        lblDiscrepancyTrackingRpt.Visible = true;
        if (Session["InputFields"] != null)
        {
            DiscrepancyReportBE oDiscrepancyReportBE = (DiscrepancyReportBE)Session["InputFields"];
            DiscrepancyReportBAL oDiscrepancyReportBAL = new DiscrepancyReportBAL();

            oDiscrepancyReportBE.Action = action;
            oDiscrepancyReportBE.DateFrom = Common.GetMM_DD_YYYY(GetQueryStringValue("DateFrom"));
            oDiscrepancyReportBE.DateTo = Common.GetMM_DD_YYYY(GetQueryStringValue("DateTo"));
            lblDateFromValue.Text = GetQueryStringValue("DateFrom");
            lblDateToValue.Text = GetQueryStringValue("DateTo");
            if (GetQueryStringValue("SP") != null)
            {
                lblDiscrepancyTrackingRpt.Text = "Planner Summaries";
                oDiscrepancyReportBE.StockPlannerName = GetQueryStringValue("SP");
                oDiscrepancyReportBE.APName = string.Empty;
                lblStckPlannerName.Text = GetQueryStringValue("SP");
            }
            if (GetQueryStringValue("AP") != null)
            {
                lblDiscrepancyTrackingRpt.Text = "Account Payable Summaries";
                oDiscrepancyReportBE.APName = GetQueryStringValue("AP");
                oDiscrepancyReportBE.StockPlannerName = string.Empty;
                lblStckPlannerName.Text = GetQueryStringValue("AP");
            }
            DataSet dt = oDiscrepancyReportBAL.GetVDRDiscrepanciesReportBAL(oDiscrepancyReportBE, out RecordCountforPaging);
            if (dt != null)
            {
                if (dt.Tables[0].Rows.Count > 0)
                {
                    grdBind.DataSource = dt.Tables[0];
                    grdBind.DataBind();
                    grdBindExport.DataSource = dt.Tables[0];
                    grdBindExport.DataBind();
                    lblError.Text = "";
                    lblError.Visible = false;
                    ucExportToExcel1.Visible = false;
                    btnExportToExcel.Visible = false;
                    tbl.Visible = false;
                    //  btnGO.Visible = false;
                }
                else
                {
                    grdBind.DataSource = null;
                    grdBind.DataBind();
                    lblError.Visible = true;
                    lblError.Text = RecordNotFound;
                    ucExportToExcel1.Visible = false;
                    btnExportToExcel.Visible = false;
                    tbl.Visible = false;
                    // btnGO.Visible = false;
                }
            }
            else
            {
                grdBind.DataSource = null;
                grdBind.DataBind();
                lblError.Visible = true;
                lblError.Text = RecordNotFound;
                tbl.Visible = false;
                // btnGO.Visible = false;
            }
        }
    }

    //protected void lnkName_Click(object sender, EventArgs e)
    //{
    //    try
    //    {
    //        LinkButton btn = (LinkButton)sender;
    //       // string[] CommandArgument = btn.CommandArgument.Split(',');
    //        var script="";
    //        if (ddlReport.SelectedItem.Text == "Inventory Summary")
    //        {
    //            script = "window.open('" + EncryptQuery("VDRTrackerReportNew.aspx?SP=" + btn.CommandArgument + "") + "')";
    //        }
    //        else if (ddlReport.SelectedItem.Text == "Accounts Payable Summary")
    //        {
    //          //  script = "window.open('" + EncryptQuery("VDRTrackerReportNew.aspx?AP=" + btn.CommandArgument + "")+'"");  
    //            script = "window.open('" + EncryptQuery("VDRTrackerReportNew.aspx?AP=" + btn.CommandArgument + "") + "')";

    //        }
    //        ScriptManager.RegisterStartupScript(pnlUP1, pnlUP1.GetType(), "script", script, true);
    //    }
    //    catch (Exception ex)
    //    {
    //        LogUtility.SaveErrorLogEntry(ex); ;
    //    }
    //}
    protected void btnGenerateReport_Click(object sender, EventArgs e)
    {

        BindGridview();
        lblDiscrepancyTrackingReport.Visible = false;
        lblDiscrepancyTrackingRpt.Visible = true;
    }

    public void BindGridview(int page = 1)
    {
        divGrid.Visible = true;
        btnBack.Visible = true;
        DiscrepancyReportBE oDiscrepancyReportBE = new DiscrepancyReportBE();
        DiscrepancyReportBAL oDiscrepancyReportBAL = new DiscrepancyReportBAL();
        oDiscrepancyReportBE.DateFrom = string.IsNullOrEmpty(hdnJSFromDt.Value) ? (DateTime?)null : Common.GetMM_DD_YYYY(hdnJSFromDt.Value);
        oDiscrepancyReportBE.DateTo = string.IsNullOrEmpty(hdnJSToDt.Value) ? (DateTime?)null : Common.GetMM_DD_YYYY(hdnJSToDt.Value);

        if (msSite.SelectedSiteIDs == null)
        {
            oDiscrepancyReportBE.SelectedSiteIDs = string.Empty;
        }
        else
        {
            oDiscrepancyReportBE.SelectedSiteIDs = msSite.SelectedSiteIDs;
        }

        if (msCountry.SelectedCountryIDs == null)
        {
            oDiscrepancyReportBE.SelectedCountryIDs = string.Empty;
        }
        else
        {
            oDiscrepancyReportBE.SelectedCountryIDs = msCountry.SelectedCountryIDs;
        }
        if (msStockPlanner.SelectedStockPlannerIDs == null)
        {
            oDiscrepancyReportBE.SelectedStockPlannerIDs = string.Empty;
        }
        else
        {
            oDiscrepancyReportBE.SelectedStockPlannerIDs = msStockPlanner.SelectedStockPlannerIDs;
        }
        if (msVendor.SelectedVendorIDs == null)
        {
            oDiscrepancyReportBE.SelectedVendorIDs = string.Empty;
        }
        else
        {
            oDiscrepancyReportBE.SelectedVendorIDs = msVendor.SelectedVendorIDs;
        }
        if (msAP.SelectedAPIDs == null)
        {
            oDiscrepancyReportBE.SelectedAPIDs = string.Empty;
        }
        else
        {
            oDiscrepancyReportBE.SelectedAPIDs = msAP.SelectedAPIDs;
        }

        if (divSummary.Visible == true)
        {
            if (GetQueryStringValue("SP") != null)
            {
                if (rdoStatus.Checked)
                {
                    oDiscrepancyReportBE.StockPlannerName = GetQueryStringValue("SP");
                    oDiscrepancyReportBE.Action = "SiteWiseOpenStatusINV";
                }
                else if (rdoType.Checked == true)
                {
                    oDiscrepancyReportBE.StockPlannerName = GetQueryStringValue("SP");
                    oDiscrepancyReportBE.Action = "SiteWiseDiscINV";
                }
            }

            if (GetQueryStringValue("AP") != null)
            {
                if (rdoStatus.Checked)
                {
                    oDiscrepancyReportBE.StockPlannerName = GetQueryStringValue("AP");
                    oDiscrepancyReportBE.Action = "SiteWiseOpenStatusAP";
                }
                else if (rdoType.Checked == true)
                {
                    oDiscrepancyReportBE.StockPlannerName = GetQueryStringValue("AP");
                    oDiscrepancyReportBE.Action = "SiteWiseDiscAP";
                }
            }
        }
        else
        {
            if (rdoSummary.Checked == true)
            {
                tdReport.Visible = true;
                tdReport1.Visible = true;
                if (ddlReport.SelectedItem.Text == "Inventory Summary")
                {
                    lblDiscrepancyTrackingRpt.Text = "Inventory Summary";
                    if (ddlSubReports.SelectedItem.Text == "By Planner/Site")
                    {
                        oDiscrepancyReportBE.Action = "PlannerSiteWise";
                        oDiscrepancyReportBE.DiscrepancyStatus = ddlStatus.SelectedValue;
                    }
                    else if (ddlSubReports.SelectedItem.Text == "By Planner/Type")
                    {
                        oDiscrepancyReportBE.Action = "PlannerDiscrepancyWise";
                        oDiscrepancyReportBE.DiscrepancyStatus = ddlStatus.SelectedValue;
                    }
                    else if (ddlSubReports.SelectedItem.Text == "By Planner/Status")
                    {
                        oDiscrepancyReportBE.Action = "PlannerWise";
                        oDiscrepancyReportBE.DiscrepancyStatus = ddlStatus.SelectedValue;
                    }
                    else if (ddlSubReports.SelectedItem.Text == "By Category/Site")
                    {
                        oDiscrepancyReportBE.Action = "CategorySitewise";
                        oDiscrepancyReportBE.DiscrepancyStatus = ddlStatus.SelectedValue;
                    }
                    else if (ddlSubReports.SelectedItem.Text == "By Category/Type")
                    {
                        oDiscrepancyReportBE.Action = "CategoryDiscwise";
                        oDiscrepancyReportBE.DiscrepancyStatus = ddlStatus.SelectedValue;
                    }
                    else if (ddlSubReports.SelectedItem.Text == "By Category/Status")
                    {
                        oDiscrepancyReportBE.Action = "CategoryOpenWise";
                        oDiscrepancyReportBE.DiscrepancyStatus = ddlStatus.SelectedValue;
                    }

                }
                else if (ddlReport.SelectedItem.Text == "Accounts Payable Summary")
                {
                    lblDiscrepancyTrackingRpt.Text = "Accounts Payable Summary";
                    if (ddlSubReports.SelectedItem.Text == "By AP Clerk/Site")
                    {
                        oDiscrepancyReportBE.Action = "AccountPayableSite";
                        oDiscrepancyReportBE.DiscrepancyStatus = ddlStatus.SelectedValue;
                    }
                    else if (ddlSubReports.SelectedItem.Text == "By AP Clerk/Type")
                    {
                        oDiscrepancyReportBE.Action = "AccountPayabledisc";
                        oDiscrepancyReportBE.DiscrepancyStatus = ddlStatus.SelectedValue;
                    }
                    else if (ddlSubReports.SelectedItem.Text == "By AP Clerk/Status")
                    {
                        oDiscrepancyReportBE.Action = "AccountPayableWiseOpen";
                        oDiscrepancyReportBE.DiscrepancyStatus = ddlStatus.SelectedValue;
                    }
                }
                else if (ddlReport.SelectedItem.Text == "Site Summary")
                {
                    lblDiscrepancyTrackingRpt.Text = "Site Summary";
                    if (ddlSubReports.SelectedItem.Text == "By Site/Type")
                    {
                        oDiscrepancyReportBE.Action = "SiteWiseDisc";
                        oDiscrepancyReportBE.DiscrepancyStatus = ddlStatus.SelectedValue;
                    }
                    else if (ddlSubReports.SelectedItem.Text == "By Site/Department")
                    {
                        oDiscrepancyReportBE.Action = "SiteDepartmentWise";
                        oDiscrepancyReportBE.DiscrepancyStatus = ddlStatus.SelectedValue;
                    }
                    else if (ddlSubReports.SelectedItem.Text == "By Site/Status")
                    {
                        oDiscrepancyReportBE.Action = "SiteWiseOpen";
                        oDiscrepancyReportBE.DiscrepancyStatus = ddlStatus.SelectedValue;
                    }
                }
                else if (ddlReport.SelectedItem.Text == "Vendor Summary")
                {
                    lblDiscrepancyTrackingRpt.Text = "Vendor Summary";
                    if (ddlSubReports.SelectedItem.Text == "By Vendor/Type")
                    {
                        oDiscrepancyReportBE.Action = "VendorWiseDisc";
                        oDiscrepancyReportBE.DiscrepancyStatus = ddlStatus.SelectedValue;
                    }
                    else if (ddlSubReports.SelectedItem.Text == "By Vendor/Site")
                    {
                        oDiscrepancyReportBE.Action = "VendorWiseSite";
                        oDiscrepancyReportBE.DiscrepancyStatus = ddlStatus.SelectedValue;
                    }
                    else if (ddlSubReports.SelectedItem.Text == "By Vendor/Status")
                    {
                        oDiscrepancyReportBE.Action = "VendorWiseOpen";
                        oDiscrepancyReportBE.DiscrepancyStatus = ddlStatus.SelectedValue;
                    }
                }

            }
            else if (rdoDetail.Checked == true)
            {
                lblDiscrepancyTrackingRpt.Text = "Detailed Reports";
                oDiscrepancyReportBE.Action = "DetailedReport";
                oDiscrepancyReportBE.DiscrepancyStatus = ddlStatus.SelectedValue;
                tdReport.Visible = false;
                tdReport1.Visible = false;
                oDiscrepancyReportBE.PageCount = page;
            }
        }

        DataSet dt = oDiscrepancyReportBAL.GetVDRDiscrepanciesReportBAL(oDiscrepancyReportBE, out RecordCountforPaging);
        if (dt != null)
        {
            if (rdoDetail.Checked != true)
            {
                grdBind.AllowPaging = true;
                //grdBind.AllowSorting = false;
            }
            else
            {
                grdBind.AllowPaging = false;
               // grdBind.AllowSorting = true;
                // grdBind.PageSize = 2;
            }
            pager1.ItemCount = RecordCountforPaging;
            if (dt.Tables[0].Rows.Count > 0)
            {
                ViewState["DataSource"] = dt.Tables[0];
                Session["InputFields"] = oDiscrepancyReportBE;
                grdBind.DataSource = dt.Tables[0];
                grdBind.DataBind();
                //if ((rdoSummary.Checked == true && ddlReport.SelectedItem.Text != "Vendor Summary") || rdoDetail.Checked != true)
                if (rdoDetail.Checked != true)
                {
                    grdBindExport.DataSource = dt.Tables[0];
                    grdBindExport.DataBind();
                    ucExportToExcel1.Visible = true;
                    btnExportToExcel.Visible = false;
                    pager1.Visible = false;

                }
                else
                {
                    ucExportToExcel1.Visible = false;
                    btnExportToExcel.Visible = true;
                    pager1.Visible = true;

                }
                lblError.Text = "";
                lblError.Visible = false;
                tbl.Visible = false;
                tblsearch.Visible = true;
                btnGO.Visible = true;
                btnGO.Text = "Generate Report";
                //  btnGO.Visible = false;
            }
            else
            {
                grdBind.DataSource = null;
                grdBind.DataBind();
                grdBindExport.DataSource = null;
                grdBindExport.DataBind();
                lblError.Visible = true;
                lblError.Text = RecordNotFound;
                ucExportToExcel1.Visible = false;
                tbl.Visible = false;
                tblsearch.Visible = true;
                btnGO.Visible = true;
                btnGO.Text = "Generate Report";
                btnExportToExcel.Visible = false;
                pager1.Visible = false;
                // btnGO.Visible = false;
            }
        }
        else
        {
            grdBind.DataSource = null;
            grdBind.DataBind();
            grdBindExport.DataSource = null;
            grdBindExport.DataBind();
            lblError.Visible = true;
            lblError.Text = RecordNotFound;
            tbl.Visible = false;
            tblsearch.Visible = true;
            btnGO.Visible = true;
            btnGO.Text = "Generate Report";
            btnExportToExcel.Visible = false;
            ucExportToExcel1.Visible = false;
            pager1.Visible = false;
            // btnGO.Visible = false;
        }
        //BindDDLreports();
        //ddlStatus.SelectedItem.Value = oDiscrepancyReportBE.DiscrepancyStatus;
    }
    protected void rdoSummary_CheckedChanged(object sender, EventArgs e)
    {
        if (ddlReport.Items.Count == 0)
        {
            ddlReport.Items.Add(new ListItem("Inventory Summary", "Inventory Summary"));
            ddlReport.Items.Add(new ListItem("Accounts Payable Summary", "Accounts Payable Summary"));
            ddlReport.Items.Add(new ListItem("Site Summary", "Site Summary"));
            ddlReport.Items.Add(new ListItem("Vendor Summary", "Vendor Summary"));
        }
        tdReport.Visible = true;
        tdReport1.Visible = true;
        ddlStatus.Enabled = true;
        BindDDLreports();

    }

    public void BindDDLreports()
    {
        ddlStatus.Items.Clear();
        ddlStatus.Items.Add(new ListItem("All", "A"));
        ddlStatus.Items.Add(new ListItem("Open", "O"));
        if (rdoDetail.Checked == true)
        {
            ddlStatus.Items.Add(new ListItem("Closed", "C"));
            ddlStatus.Items.Add(new ListItem("Force Closed", "F"));
        }
        else
        {
            ddlStatus.Items.Add(new ListItem("Closed", "C,F"));
        }
    }
    protected void btnBack_Click(object sender, EventArgs e)
    {
        divGrid.Visible = false;
        btnBack.Visible = false;
        tbl.Visible = true;
        tblsearch.Visible = true;
        btnGO.Visible = true;
        btnGO.Text = "GO";
        pager1.Visible = false;
        //  btnGO.Visible = true;
        msStockPlanner.setStockPlannerOnPostBack();
        msSite.setSitesOnPostBack();
        msVendor.setVendorsOnPostBack();
        msCountry.SetCountryOnPostBack();
        msAP.SetAPOnPostBack();
        lblDiscrepancyTrackingReport.Visible = true;
        lblDiscrepancyTrackingRpt.Visible = false;
        if (Request.RawUrl.Split('?').Length > 1)
        {
            //Response.Redirect("VDRTrackerReportNew.aspx");
            Page.ClientScript.RegisterStartupScript(this.GetType(), "script", "window.close()", true);
        }
       // Session["InputFields"] = null;
    }
    protected void ddlReport_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddlSubReports.Items.Clear();
        if (ddlReport.SelectedValue == "1")
        {
            ddlSubReports.Items.Add(new ListItem("By Planner/Site", "1"));
            ddlSubReports.Items.Add(new ListItem("By Planner/Type", "2"));
            ddlSubReports.Items.Add(new ListItem("By Planner/Status", "3"));
            ddlSubReports.Items.Add(new ListItem("By Category/Site", "4"));
            ddlSubReports.Items.Add(new ListItem("By Category/Type", "5"));
            ddlSubReports.Items.Add(new ListItem("By Category/Status", "6"));
        }
        else if (ddlReport.SelectedValue == "2")
        {
            ddlSubReports.Items.Add(new ListItem("By AP Clerk/Site", "1"));
            ddlSubReports.Items.Add(new ListItem("By AP Clerk/Type", "2"));
            ddlSubReports.Items.Add(new ListItem("By AP Clerk/Status", "3"));
        }
        else if (ddlReport.SelectedValue == "3")
        {
            ddlSubReports.Items.Add(new ListItem("By Site/Type", "1"));
            ddlSubReports.Items.Add(new ListItem("By Site/Department", "2"));
            ddlSubReports.Items.Add(new ListItem("By Site/Status", "3"));
        }
        else if (ddlReport.SelectedValue == "4")
        {
            ddlSubReports.Items.Add(new ListItem("By Vendor/Type", "1"));
            ddlSubReports.Items.Add(new ListItem("By Vendor/Site", "2"));
            ddlSubReports.Items.Add(new ListItem("By Vendor/Status", "3"));
        }
        ddlStatus.Items.Clear();
        BindDDLreports();
        ddlStatus.Enabled = true;
    }
    protected void OnPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        divGrid.Visible = true;
        btnBack.Visible = true;
        lblError.Text = "";
        lblError.Visible = false;
        ucExportToExcel1.Visible = true;
        tbl.Visible = false;
        grdBind.PageIndex = e.NewPageIndex;
        grdBind.DataSource = (DataTable)ViewState["DataSource"];
        grdBind.DataBind();

    }
    protected void grdBind_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            if (divSummary.Visible == false && rdoSummary.Checked == true)
            {
                if (ddlReport.SelectedItem.Text == "Inventory Summary" || ddlReport.SelectedItem.Text == "Accounts Payable Summary")
                {
                    var URL = "";
                    dynamic firstCell = e.Row.Cells[0];
                    firstCell.Controls.Clear();
                    if (ddlReport.SelectedItem.Text == "Inventory Summary" && GetQueryStringValue("SP") == null && (ddlSubReports.SelectedItem.Text == "By Planner/Site" || ddlSubReports.SelectedItem.Text == "By Planner/Type" || ddlSubReports.SelectedItem.Text == "By Planner/Status"))
                    {
                        URL = EncryptQuery("VDRTrackerReportNew.aspx?SP=" + Convert.ToString(firstCell.Text) + "&DateTo=" + hdnJSToDt.Value + "&DateFrom=" + hdnJSFromDt.Value + "");
                        firstCell.Controls.Add(new HyperLink
                        {
                            NavigateUrl = URL,
                            Target = "_blank",
                            Text = firstCell.Text
                        });
                    }
                    else if (ddlReport.SelectedItem.Text == "Accounts Payable Summary" && GetQueryStringValue("AP") == null)
                    {
                        URL = EncryptQuery("VDRTrackerReportNew.aspx?AP=" + Convert.ToString(firstCell.Text) + "&DateTo=" + hdnJSToDt.Value + "&DateFrom=" + hdnJSFromDt.Value + "");
                        firstCell.Controls.Add(new HyperLink
                        {
                            NavigateUrl = URL,
                            Target = "_blank",
                            Text = firstCell.Text
                        });
                    }

                }
            }

        }
    }
    protected void rdoType_CheckedChanged(object sender, EventArgs e)
    {
        if (GetQueryStringValue("SP") != null)
        {
            BindOnQueryString("SiteWiseDiscINV");
        }
        else if (GetQueryStringValue("AP") != null)
        {
            BindOnQueryString("SiteWiseDiscAP");
        }
    }
    protected void rdoStatus_CheckedChanged(object sender, EventArgs e)
    {
        if (GetQueryStringValue("SP") != null)
        {
            BindOnQueryString("SiteWiseOpenStatusINV");
        }
        else if (GetQueryStringValue("AP") != null)
        {
            BindOnQueryString("SiteWiseOpenStatusAP");
        }
    }
    public void pager_Command(object sender, CommandEventArgs e)
    {
        int currnetPageIndx = Convert.ToInt32(e.CommandArgument);
        pager1.CurrentIndex = currnetPageIndx;
        BindGridview(currnetPageIndx);
    }
    public void BindGridviewExcel()
    {

        if (Session["InputFields"] != null)
        {
            DiscrepancyReportBE oDiscrepancyReportBE = (DiscrepancyReportBE)Session["InputFields"];
            DiscrepancyReportBAL oDiscrepancyReportBAL = new DiscrepancyReportBAL();
            // if ((rdoSummary.Checked == true && ddlReport.SelectedItem.Text == "Vendor Summary") || rdoDetail.Checked == true)
            if (rdoDetail.Checked == true)
            {
                lblDateFromValue.Text = hdnJSFromDt.Value;
                lblDateToValue.Text = hdnJSToDt.Value;
                oDiscrepancyReportBE.PageCount = 0;
                oDiscrepancyReportBE.APName = string.Empty;
                oDiscrepancyReportBE.StockPlannerName = string.Empty;
                DataSet dt = oDiscrepancyReportBAL.GetVDRDiscrepanciesReportBAL(oDiscrepancyReportBE, out RecordCountforPaging);
                ExportToExcelDetailedTracker(dt.Tables[0], "Discrepancy Reports-Detailed");
            }
        }
    }
    protected void btnExport_Click(object sender, EventArgs e)
    {
        BindGridviewExcel();
    }
    public void ExportToExcelDetailedTracker(DataTable dtExport, string fileName)
    {
        HttpContext context = HttpContext.Current;
        string date = DateTime.Now.ToString("ddMMyyyy");
        string time = DateTime.Now.ToString("HH:mm");
        context.Response.ContentType = "text/vnd.ms-excel";
        context.Response.AddHeader("Content-Disposition", string.Format("attachment; filename={0}_{1}_{2}", fileName, date, time + ".xls"));
        if (dtExport.Rows.Count > 0)
        {

            for (int i = 0; i < dtExport.Columns.Count; i++)
            {
                context.Response.Write(dtExport.Columns[i].ColumnName);
                context.Response.Write("\t");
            }
        }

        context.Response.Write(Environment.NewLine);

        //Write data
        foreach (DataRow row in dtExport.Rows)
        {

            for (int i = 0; i < dtExport.Columns.Count; i++)
            {
                string rowdata = string.Empty;
                rowdata = Convert.ToString(row.ItemArray[i]) == "" ? "" : Convert.ToString(row.ItemArray[i]);
                context.Response.Write(rowdata);
                context.Response.Write("\t");
            }
            context.Response.Write(Environment.NewLine);
        }
        context.Response.End();
    }
    protected void ddlSubReports_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        ucExportToExcel1.Visible = false;
        if (ddlSubReports.SelectedItem.Text == "By Planner/Status")
        {
            ddlStatus.Enabled = false;
            ddlStatus.SelectedIndex = 0;
            //ddlStatus.SelectedItem.Value = "A";
        }
        else if (ddlSubReports.SelectedItem.Text == "By Category/Status")
        {
            ddlStatus.Enabled = false;
            ddlStatus.SelectedIndex = 0;
        }
        else if (ddlSubReports.SelectedItem.Text == "By AP Clerk/Status")
        {
            ddlStatus.Enabled = false;
            ddlStatus.SelectedIndex = 0;
        }
        else if (ddlSubReports.SelectedItem.Text == "By Site/Status")
        {
            ddlStatus.Enabled = false;
            ddlStatus.SelectedIndex = 0;
        }
        else if (ddlSubReports.SelectedItem.Text == "By Vendor/Status")
        {
            ddlStatus.Enabled = false;
            ddlStatus.SelectedIndex = 0;
        }
        else if (ddlSubReports.SelectedItem.Text == "By Site/Department")
        {
            ddlStatus.SelectedIndex = 1;
            ddlStatus.Enabled = false;
            // ddlStatus.SelectedItem.Value = "O";
        }
        else
        {
            ddlStatus.Enabled = true;
            //ddlStatus.SelectedItem.Text = "All";
            //ddlStatus.SelectedItem.Value = "A";
        }
    }
    protected void rdoDetail_CheckedChanged(object sender, EventArgs e)
    {
        ddlStatus.Enabled = true;
        BindDDLreports();
    }
}
﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="DISCnt_AssignedUnAssignedVendorsReport.aspx.cs"
    MasterPageFile="../../../CommonUI/CMN_MasterPages/CRUD_MasterPage.Master" Inherits="DISCnt_AssignedUnAssignedVendorsReport" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectCountry.ascx" TagName="MultiSelectCountry"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectSite.ascx" TagName="MultiSelectSite"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectAP.ascx" TagName="MultiSelectAP"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectVendor.ascx" TagName="MultiSelectVendor"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectUserDisReport.ascx" TagName="MultiSelectUser"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/ucVendorSelectionTemplate.ascx" TagName="VendorSelectTemplate"
    TagPrefix="cc1" %>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript">
        function DiscrepancyTypeRequired(oSource, args) {
            var $days = $('#rdoDiscrepancy').val();
            if ($('#rdoDiscrepancy:checked').val() && $('#ddlDiscrepancyType').val() == 0)
                args.IsValid = false;
            else
                args.IsValid = true;
        }

        $(document).ready(function () {
            $("body").keypress(function (e) {
                if (e.which == 13 || e.keyCode == 13) {
                    $('#<%=btnGenerateReport.ClientID %>').click();
                    return false;
                }
            });
        });
    </script>
    <h2>
        <cc1:ucLabel ID="lblVendorReportSearch" runat="server" Text="Assigned/Unassigned Vendors Report"></cc1:ucLabel>
    </h2>
    <asp:ScriptManager ID="SM1" runat="server">
    </asp:ScriptManager>
    <asp:HiddenField ID="hdnAction" runat="server" />
    <asp:HiddenField ID="hdnSelectedCountryIDs" runat="server" />
    <asp:HiddenField ID="hdnSelectedVendorIDs" runat="server" />
    <asp:HiddenField ID="hdnSelectedAPIDs" runat="server" />
    <div class="right-shadow">
        <div class="formbox">
            <cc1:ucPanel ID="UcCarrierVendorPanel" runat="server">
                <div class="right-shadow">
                    <div class="formbox">
                        <table width="100%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                            <tr>
                                <td align="right">
                                    <cc1:VendorSelectTemplate runat="server" ID="ucVendorTemplateSelect" />
                                </td>
                            </tr>
                        </table>
                        <table width="100%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                            <tr>
                                <td style="font-weight: bold; visibility: hidden">
                                    <cc1:ucLabel ID="UcLabel1" runat="server" Text="User"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold; visibility: hidden">
                                    :
                                </td>
                                <td style="font-weight: bold;">
                                    <asp:RadioButtonList ID="rbList" CellPadding="10" AutoPostBack="true" OnSelectedIndexChanged="rbList_OnSelectedIndexChanged"
                                        RepeatLayout="Table" TextAlign="Right" RepeatDirection="Horizontal" runat="server">
                                        <asp:ListItem Text="Assigned Vendor" Selected="True" />
                                        <asp:ListItem Text="Unassigned Vendor" />
                                    </asp:RadioButtonList>
                                </td>
                            </tr>
                            <tr>
                                <td style="font-weight: bold;width:81px;">
                                    <cc1:ucLabel ID="lblUser" runat="server" Text="User"></cc1:ucLabel>
                                </td>
                                <td id="tdColon" runat="server" style="font-weight: bold;width:10px;">
                                    :
                                </td>
                                <td>
                                    <cc1:MultiSelectUser runat="server" ID="msUser" />
                                </td>
                            </tr>
                            <tr>
                                <td style="font-weight: bold;">
                                    <cc1:ucLabel ID="lblCountry" runat="server" Text="Country"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold; ">
                                    :
                                </td>
                                <td>
                                    <cc1:MultiSelectCountry runat="server" ID="msCountry" />
                                </td>
                            </tr>
                            <tr>
                                <td style="font-weight: bold;">
                                    <cc1:ucLabel ID="lblVendor" runat="server" Text="Vendor"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold;">
                                    :
                                </td>
                                <td>
                                    <cc1:MultiSelectVendor runat="server" ID="msVendor" />
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="button-row">
                    <cc1:ucButton ID="btnGenerateReport" runat="server" Text="Generate Report" CssClass="button"
                        OnClick="btnGenerateReport_Click" ValidationGroup="Save" />
                </div>
            </cc1:ucPanel>
            <cc1:ucPanel ID="UcCarrierVendorReportViewPanel" runat="server" Visible="false">
                <table style="width: 100%;">
                    <tr>
                        <td>
                            <rsweb:ReportViewer ID="VendorReportViewer" runat="server" Width="950px" DocumentMapCollapsed="True"
                                Font-Names="Verdana" Font-Size="8pt" InteractiveDeviceInfos="(Collection)" WaitMessageFont-Names="Verdana"
                                WaitMessageFont-Size="14pt" Height="416px">
                                <LocalReport ReportPath="ModuleUI\\Discrepancy\\Report\\RDLC\\AssignedUnAssignedVendorsReport.rdlc">
                                    <DataSources>
                                        <rsweb:ReportDataSource DataSourceId="SqlDataSource1" Name="Table" />
                                    </DataSources>
                                </LocalReport>
                            </rsweb:ReportViewer>
                            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:sConn %>"
                                SelectCommand="spRPTDIS_Reports" SelectCommandType="StoredProcedure">
                                <SelectParameters>
                                    <asp:ControlParameter ControlID="hdnAction" DefaultValue="-1" Name="Action" PropertyName="Value"
                                        Type="String" />
                                    <asp:ControlParameter ControlID="hdnSelectedCountryIDs" DefaultValue="-1" Name="SelectedCountryIDs"
                                        PropertyName="Value" Type="String" />
                                    <asp:ControlParameter ControlID="hdnSelectedVendorIDs" DefaultValue="" Name="SelectedVendorIDs"
                                        PropertyName="Value" Type="String" />
                                    <asp:ControlParameter ControlID="hdnSelectedAPIDs" Name="SelectedAPIDs" PropertyName="Value"
                                        Type="String" />
                                </SelectParameters>
                            </asp:SqlDataSource>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <cc1:ucButton ID="btnBackSearch" runat="server" Text="Back To Search Page" CssClass="button"
                                OnClick="btnBack_Click" />
                        </td>
                    </tr>
                </table>
            </cc1:ucPanel>
        </div>
    </div>
</asp:Content>

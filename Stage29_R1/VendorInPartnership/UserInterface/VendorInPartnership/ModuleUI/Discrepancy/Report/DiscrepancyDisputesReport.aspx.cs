﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using BaseControlLibrary;
using BusinessEntities.ModuleBE.Discrepancy.LogDiscrepancy;
using BusinessEntities.ModuleBE.Security;
using BusinessLogicLayer.ModuleBAL.Discrepancy.LogDiscrepancy;
using BusinessLogicLayer.ModuleBAL.Security;
using BusinessLogicLayer.ModuleBAL.Appointment.SiteSettings;
using BusinessEntities.ModuleBE.Appointment.SiteSettings;

public partial class DiscrepancyDisputesReport : CommonPage
{
    public int MasterParentID
    {
        get
        {
            return Convert.ToInt32(ViewState["MasterParentID"]);
        }
        set { ViewState["MasterParentID"] = value; }
    }
    public SortDirection GridViewSortDirection
    {
        get
        {
            if (ViewState["sortDirection"] == null)
                ViewState["sortDirection"] = SortDirection.Ascending;
            return (SortDirection)ViewState["sortDirection"];
        }
        set { ViewState["sortDirection"] = value; }
    }

    #region Events

    protected void Page_Init(object sender, EventArgs e)
    {
        ucSeacrhVendor1.CurrentPage = this;
        ucVendorTemplateSelect.CurrentPage = this;
        if (Session["Role"].ToString().ToLower() == "vendor")
        {
            msVendor.FunctionCalled = "BindVendorByVendorId";
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {

        if (Session["UserID"] != null)
        {
            if (Session["Role"].ToString().ToLower() == "vendor")
            {
                trSingleVendorControl.Visible = false;
                trMultivendorControl.Visible = true;
                tblVendorTemplate.Visible = false;
            }

        }

        if (!IsPostBack)
        {
            txtFromDate.Text = DateTime.Now.AddDays(-DateTime.Now.Day + 1).ToString("dd/MM/yyyy");
            txtToDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
            hdnJSFromDt.Value = txtFromDate.Text;
            hdnJSToDt.Value = txtToDate.Text;

          
        }

        if (IsPostBack)
        {
            txtFromDate.Text = hdnJSFromDt.Value;
            txtToDate.Text = hdnJSToDt.Value;
            
            msVendor.setVendorsOnPostBack();
            msSite.setSitesOnPostBack();
        }

        ucExportToExcel1.CurrentPage = this;
        ucExportToExcel1.GridViewControl = gvDisDispute;
        ucExportToExcel1.FileName = "DiscrepancyDispute";

    }



    protected void gvDisDispute_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvDisDispute.PageIndex = e.NewPageIndex;
        BindDisputedDis();
    }

    protected void gvDisDispute_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType != DataControlRowType.Header && e.Row.RowType != DataControlRowType.Footer)
        {
            var hypLinkToDisDetails = (HyperLink)e.Row.FindControl("hypLinkToDisDetails");
            var hdnDisLogID = (HiddenField)e.Row.FindControl("hdnDisLogID");
            var hdnQueryDisId = (HiddenField)e.Row.FindControl("hdnQueryDisId");
            if (hypLinkToDisDetails != null)
            {
                hypLinkToDisDetails.NavigateUrl = EncryptQuery("../DISLog_QueryDiscrepancy.aspx?disLogID=" + hdnDisLogID.Value
                    + "&VDRNo=" + hypLinkToDisDetails.Text + "&QueryDisID=" + hdnQueryDisId.Value + "&Mode=Readonly");
            }
        }
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        gvDisDispute.PageIndex = 0;
        BindDisputedDis();

    }

    #endregion

    #region Methods

    protected void BindDisputedDis()
    {
        DISLog_QueryDiscrepancyBAL objqueryDiscrepancyBAL = new DISLog_QueryDiscrepancyBAL();
        DISLog_QueryDiscrepancyBE objqueryDiscrepancyBE = new DISLog_QueryDiscrepancyBE();

        objqueryDiscrepancyBE.Action = "GetDiscrepancyDisputes";
        objqueryDiscrepancyBE.SCDiscrepancyDateFrom = Utilities.Common.TextToDateFormat(hdnJSFromDt.Value);
        objqueryDiscrepancyBE.SCDiscrepancyDateTo = Utilities.Common.TextToDateFormat(hdnJSToDt.Value);

        objqueryDiscrepancyBE.SCSelectedSiteIDs = msSite.SelectedSiteIDs;


        if (rblQueryStatus.SelectedValue.ToString() == "A")
            objqueryDiscrepancyBE.Query = null;
        else
            objqueryDiscrepancyBE.Query = rblQueryStatus.SelectedValue;


        if (Session["UserID"] != null)
        {
            if (Session["Role"].ToString().ToLower() == "vendor")
            {
                    objqueryDiscrepancyBE.UserIdForConsVendors = Convert.ToInt32(Session["UserID"]); //Loggedin Vendor
            }
            else
            {
                if (ucSeacrhVendor1.VendorNo.Trim() != string.Empty)
                    objqueryDiscrepancyBE.VendorID = Convert.ToInt32(ucSeacrhVendor1.VendorNo);
            }
            objqueryDiscrepancyBE.SCSelectedVendorIDs = msVendor.SelectedVendorIDs;
        }

        List<DISLog_QueryDiscrepancyBE> lstDisputedDis = new List<DISLog_QueryDiscrepancyBE>();

        lstDisputedDis = objqueryDiscrepancyBAL.GetDiscrepancyDisputesBAL(objqueryDiscrepancyBE);

        gvDisDispute.DataSource = lstDisputedDis;
        gvDisDispute.DataBind();

        ucExportToExcel1.Visible = true;

    }

    



    /// <summary>
    /// Method to show Vendor on the basis of selected Template
    /// </summary>
    public override void TemplateSelectedIndexChanged()
    {
        base.TemplateSelectedIndexChanged();

        int VendorTemplateId = Convert.ToInt32(ucVendorTemplateSelect.innerControlddlTemplate.SelectedValue);

        ListBox lstSelectedVendor = (ListBox)msVendor.FindControl("lstRight");
        ListBox lstVendor = (ListBox)msVendor.FindControl("ucVendor").FindControl("lstLeft");

        if (VendorTemplateId > 0)
        {

            SCT_TemplateBAL oSCT_TemplateBAL = new SCT_TemplateBAL();
            SCT_TemplateBE oSCT_TemplateBE = new SCT_TemplateBE();

            oSCT_TemplateBE.VendorTemplateID = VendorTemplateId;
            oSCT_TemplateBE.Action = "GetVendorTemplateInfobyID";
            List<SCT_TemplateBE> lstVendorTemplate = oSCT_TemplateBAL.GetVendorTemplateById(oSCT_TemplateBE);

            if (lstVendorTemplate != null && lstVendorTemplate.Count > 0)
            {
                lstSelectedVendor.Items.Clear();
                msVendor.innerControlHiddenField.Value = string.Empty;
                msVendor.innerControlHiddenField2.Value = string.Empty;
                for (int iCount = 0; iCount < lstVendorTemplate.Count; iCount++)
                {
                    lstSelectedVendor.Items.Add(new ListItem(lstVendorTemplate[iCount].Vendor.Vendor_Name, lstVendorTemplate[iCount].Vendor.VendorID.ToString()));
                    msVendor.SelectedVendorIDs = lstVendorTemplate[iCount].Vendor.VendorID.ToString();
                    msVendor.SelectedVendorName = lstVendorTemplate[iCount].Vendor.Vendor_Name.ToString();
                }
            }
        }
        else
        {
            lstSelectedVendor.Items.Clear();
            msVendor.innerControlHiddenField.Value = string.Empty;
            msVendor.innerControlHiddenField2.Value = string.Empty;
        }
    }

    #endregion
}
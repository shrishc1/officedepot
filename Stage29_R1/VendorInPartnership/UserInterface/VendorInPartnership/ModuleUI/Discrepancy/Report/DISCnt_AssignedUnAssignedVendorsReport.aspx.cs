﻿using System;
using System.Data;
using BusinessEntities.ModuleBE.AdminFunctions;
using BusinessLogicLayer.ModuleBAL.AdminFunctions;
using System.Collections.Generic;
using BusinessEntities.ModuleBE.Discrepancy.Report;
using BusinessLogicLayer.ModuleBAL.Discrepancy.Report;
using System.Collections;
using BusinessEntities.ModuleBE.Discrepancy.LogDiscrepancy;
using BusinessLogicLayer.ModuleBAL.Discrepancy.LogDiscrepancy;
using Utilities;
using WebUtilities;
using Microsoft.Reporting.WebForms;
using System.Web.UI.WebControls;
using BusinessLogicLayer.ModuleBAL.Security;
using BusinessEntities.ModuleBE.Security;

public partial class DISCnt_AssignedUnAssignedVendorsReport : CommonPage
{
    protected void rbList_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        if (rbList.SelectedIndex == 1)
        {
            //msUser.EnableDisable(false);
            msUser.Visible = false;
            lblUser.Visible = false;
            tdColon.Visible = false;
        }
        else
        {
            msUser.Visible = true;
            lblUser.Visible = true;
            tdColon.Visible = true;
            //msUser.EnableDisable(true);
        }
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        ucVendorTemplateSelect.CurrentPage = this;
    }
    protected void Page_Load(object sender, EventArgs e)
    {
    }
    /// <summary>
    /// Method to show Vendor on the basis of selected Template
    /// </summary>
    public override void TemplateSelectedIndexChanged()
    {
        base.TemplateSelectedIndexChanged();

        int VendorTemplateId = Convert.ToInt32(ucVendorTemplateSelect.innerControlddlTemplate.SelectedValue);

        ListBox lstSelectedVendor = (ListBox)msVendor.FindControl("lstRight");
        ListBox lstVendor = (ListBox)msVendor.FindControl("ucVendor").FindControl("lstLeft");

        if (VendorTemplateId > 0)
        {

            SCT_TemplateBAL oSCT_TemplateBAL = new SCT_TemplateBAL();
            SCT_TemplateBE oSCT_TemplateBE = new SCT_TemplateBE();

            oSCT_TemplateBE.VendorTemplateID = VendorTemplateId;
            oSCT_TemplateBE.Action = "GetVendorTemplateInfobyID";
            List<SCT_TemplateBE> lstVendorTemplate = oSCT_TemplateBAL.GetVendorTemplateById(oSCT_TemplateBE);

            if (lstVendorTemplate != null && lstVendorTemplate.Count > 0)
            {
                lstSelectedVendor.Items.Clear();
                msVendor.innerControlHiddenField.Value = string.Empty;
                msVendor.innerControlHiddenField2.Value = string.Empty;
                for (int iCount = 0; iCount < lstVendorTemplate.Count; iCount++)
                {
                    lstSelectedVendor.Items.Add(new ListItem(lstVendorTemplate[iCount].Vendor.Vendor_Name, lstVendorTemplate[iCount].Vendor.VendorID.ToString()));
                    msVendor.SelectedVendorIDs = lstVendorTemplate[iCount].Vendor.VendorID.ToString();
                    msVendor.SelectedVendorName = lstVendorTemplate[iCount].Vendor.Vendor_Name.ToString();
                }
            }
        }
        else
        {
            lstSelectedVendor.Items.Clear();
            msVendor.innerControlHiddenField.Value = string.Empty;
            msVendor.innerControlHiddenField2.Value = string.Empty;
        }
    }

    protected void btnGenerateReport_Click(object sender, EventArgs e)
    {
        try
        {
            if (string.IsNullOrEmpty(msUser.SelectedUserIDs))
                msUser.SelectedUserIDs = "-1";                
            
            hdnSelectedAPIDs.Value = msUser.SelectedUserIDs;

            if (string.IsNullOrEmpty(msVendor.SelectedVendorIDs))
                msVendor.SelectedVendorIDs = "-1";
            
            hdnSelectedVendorIDs.Value = msVendor.SelectedVendorIDs;

            if (string.IsNullOrEmpty(msCountry.SelectedCountryIDs))
                hdnSelectedCountryIDs.Value = "-1";
            else
                hdnSelectedCountryIDs.Value = msCountry.SelectedCountryIDs;

            if (rbList.SelectedIndex == 0)
                hdnAction.Value = "AssignedVendorsReport";
            else
                hdnAction.Value = "UnAssignedVendorsReport";

            ReportParameter[] reportParameter = new ReportParameter[4];
            reportParameter[0] = new ReportParameter("Action", hdnAction.Value);
            reportParameter[1] = new ReportParameter("SelectedCountryIDs", hdnSelectedCountryIDs.Value);
            reportParameter[2] = new ReportParameter("SelectedVendorIDs", hdnSelectedVendorIDs.Value);
            reportParameter[3] = new ReportParameter("SelectedAPIDs", hdnSelectedAPIDs.Value);
            this.VendorReportViewer.LocalReport.SetParameters(reportParameter);

            UcCarrierVendorPanel.Visible = false;
            UcCarrierVendorReportViewPanel.Visible = true;
        }
        catch (Exception ex)
        {
        }
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        EncryptQueryString("DISCnt_AssignedUnAssignedVendorsReport.aspx");
    }
}
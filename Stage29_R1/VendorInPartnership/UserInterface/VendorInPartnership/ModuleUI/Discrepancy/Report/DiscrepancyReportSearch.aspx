﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="DiscrepancyReportSearch.aspx.cs"
    MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master" Inherits="DiscrepancyReportSearch" %>

<%@ Register Src="~/CommonUI/UserControls/MultiSelectCountry.ascx" TagName="MultiSelectCountry"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectSite.ascx" TagName="MultiSelectSite"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectAP.ascx" TagName="MultiSelectAP"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectVendor.ascx" TagName="MultiSelectVendor"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectStockPlanner.ascx" TagName="MultiSelectStockPlanner"
    TagPrefix="cc1" %>
    <%@ Register Src="~/CommonUI/UserControls/ucVendorSelectionTemplate.ascx" TagName="VendorSelectTemplate"
    TagPrefix="cc1" %>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript" language="javascript">

        function ShowDiscrepancyType() {

            var objrdoDiscrepancy = document.getElementById('<%=rdoDiscrepancy.ClientID %>');

            if (objrdoDiscrepancy.checked) {
                document.getElementById('tdDiscrepancyType').style.display = 'block';
            }
            else {
                document.getElementById('tdDiscrepancyType').style.display = 'none';
            }

        }

        $(document).ready(function () {
            $("body").keypress(function (e) {
                if (e.which == 13 || e.keyCode == 13) {
                    $('#<%=btnGenerateReport.ClientID %>').click();
                    return false;
                }
            });
        });
    </script>
    <script type="text/javascript">
        function DiscrepancyTypeRequired(oSource, args) {
            var $days = $('#rdoDiscrepancy').val();
            if ($('#rdoDiscrepancy:checked').val() && $('#ddlDiscrepancyType').val() == 0)
                args.IsValid = false;
            else
                args.IsValid = true;
        }
    </script>
    <h2>
        <cc1:ucLabel ID="lblDiscrepancyReportSearch" runat="server" Text="Discrepancy Report Search"></cc1:ucLabel>
    </h2>
    <asp:ScriptManager ID="SM1" runat="server">
    </asp:ScriptManager>
    <div class="right-shadow">
        <div class="formbox">
            <table width="100%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                <tr>
                    <td align="right">
                        <cc1:VendorSelectTemplate runat="server" ID="ucVendorTemplateSelect" />
                    </td>
                </tr>
            </table>
            <table width="100%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                <tr>
                    <td style="font-weight: bold;">
                        <cc1:ucLabel ID="lblCountry" runat="server" Text="Country"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold; ">
                        :
                    </td>
                    <td>
                        <cc1:MultiSelectCountry runat="server" ID="msCountry" />
                    </td>
                </tr>
                <tr>
                    <td style="font-weight: bold;">
                        <cc1:ucLabel ID="lblSite" runat="server" Text="Site"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold;">
                        :
                    </td>
                    <td>
                        <cc1:MultiSelectSite runat="server" ID="msSite" />
                    </td>
                </tr>
                <tr>
                    <td style="font-weight: bold;">
                        <cc1:ucLabel ID="lblStockPlanner" runat="server" Text="Stock Planner"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold;">
                        :
                    </td>
                    <td>
                        <cc1:MultiSelectStockPlanner runat="server" ID="msStockPlanner" />
                    </td>
                </tr>
                <tr>
                    <td style="font-weight: bold;">
                        <cc1:ucLabel ID="lblVendor" runat="server" Text="Vendor"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold;">
                        :
                    </td>
                    <td>
                        <cc1:MultiSelectVendor runat="server" ID="msVendor" />
                    </td>
                </tr>
                <tr>
                    <td style="font-weight: bold;">
                        <cc1:ucLabel ID="lblDateFrom" runat="server" Text="Date From"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold;">
                        :
                    </td>
                    <td>
                        <table cellpadding="0" cellspacing="0">
                            <tr>
                                <td style="font-weight: bold; width: 6em;">
                                    <cc1:ucTextbox ID="txtFromDate" ClientIDMode="Static" runat="server" CssClass="date"
                                        Width="70px" />
                                </td>
                                <td style="font-weight: bold; width: 4em; text-align: center">
                                    <cc1:ucLabel ID="lblTo" runat="server" Text="To"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold; width: 6em;">
                                    <cc1:ucTextbox ID="txtToDate" ClientIDMode="Static" runat="server" CssClass="date"
                                        Width="70px" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="font-weight: bold;">
                        <cc1:ucLabel ID="lblReportType" runat="server" Text="Report Type"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold;">
                        :
                    </td>
                    <td>
                        <table cellpadding="0" cellspacing="0" style="table-layout: fixed;">
                            <tr>
                                <td style="font-weight: bold; width: 9em;">
                                    <cc1:ucRadioButton ID="rdoSummary" runat="server" Text="Summary" GroupName="ReportType"
                                        Checked="true" onclick="ShowDiscrepancyType();" />
                                </td>
                                <td style="font-weight: bold; width: 9em;">
                                    <cc1:ucRadioButton ID="rdoDetail" runat="server" Text="Detail" GroupName="ReportType"
                                        onclick="ShowDiscrepancyType();" />
                                </td>
                                <td style="font-weight: bold; width: 9em;">
                                    <cc1:ucRadioButton ID="rdoDiscrepancy" ClientIDMode="Static" runat="server" Text="Discrepancy"
                                        GroupName="ReportType" onclick="ShowDiscrepancyType();" />
                                </td>
                                <td style="font-weight: bold; width: 15em;">
                                    <div style="display: none;" id="tdDiscrepancyType">
                                        <cc1:ucDropdownList ID="ddlDiscrepancyType" runat="server" ClientIDMode="Static"
                                            Width="170px">
                                        </cc1:ucDropdownList>
                                        <asp:CustomValidator ID="cusvDiscrepancyTypeRequired" ClientValidationFunction="DiscrepancyTypeRequired"
                                            runat="server" ValidationGroup="Save" />
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div class="button-row">
        <asp:ValidationSummary ID="vSummary" runat="server" ShowMessageBox="true" ShowSummary="false"
            Style="color: Red" ValidationGroup="Save" />
        <cc1:ucButton ID="btnGenerateReport" runat="server" Text="Generate Report" CssClass="button"
            OnClick="btnGenerateReport_Click" ValidationGroup="Save" />
    </div>
</asp:Content>

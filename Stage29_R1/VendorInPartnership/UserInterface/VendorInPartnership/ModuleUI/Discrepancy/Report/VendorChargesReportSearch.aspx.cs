﻿using System;
using System.Data;
using BusinessEntities.ModuleBE.AdminFunctions;
using BusinessLogicLayer.ModuleBAL.AdminFunctions;
using System.Collections.Generic;
using BusinessEntities.ModuleBE.Discrepancy.Report;
using BusinessLogicLayer.ModuleBAL.Discrepancy.Report;
using System.Collections;
using BusinessEntities.ModuleBE.Discrepancy.LogDiscrepancy;
using BusinessLogicLayer.ModuleBAL.Discrepancy.LogDiscrepancy;
using Utilities;
using WebUtilities;
using Microsoft.Reporting.WebForms;
using BusinessEntities.ModuleBE.Security;
using BusinessLogicLayer.ModuleBAL.Security;
using System.Web.UI.WebControls;
using System.Linq;

public partial class DiscrepancyReportSearch : CommonPage
{
    protected void Page_Init(object sender, EventArgs e)
    {
        ucVendorTemplateSelect.CurrentPage = this;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            txtFromDate.Text = DateTime.Now.AddDays(-DateTime.Now.Day + 1).ToString("dd/MM/yyyy");
            txtToDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
            txtFromDate.Attributes.Add("readonly", "readonly");
            txtToDate.Attributes.Add("readonly", "readonly");
        }
    }


    /// <summary>
    /// Method to show Vendor on the basis of selected Template
    /// </summary>
    public override void TemplateSelectedIndexChanged()
    {
        base.TemplateSelectedIndexChanged();

        int VendorTemplateId = Convert.ToInt32(ucVendorTemplateSelect.innerControlddlTemplate.SelectedValue);

        ListBox lstSelectedVendor = (ListBox)msVendor.FindControl("lstRight");
        ListBox lstVendor = (ListBox)msVendor.FindControl("ucVendor").FindControl("lstLeft");

        if (VendorTemplateId > 0)
        {

            SCT_TemplateBAL oSCT_TemplateBAL = new SCT_TemplateBAL();
            SCT_TemplateBE oSCT_TemplateBE = new SCT_TemplateBE();

            oSCT_TemplateBE.VendorTemplateID = VendorTemplateId;
            oSCT_TemplateBE.Action = "GetVendorTemplateInfobyID";
            List<SCT_TemplateBE> lstVendorTemplate = oSCT_TemplateBAL.GetVendorTemplateById(oSCT_TemplateBE);

            if (lstVendorTemplate != null && lstVendorTemplate.Count > 0)
            {
                lstSelectedVendor.Items.Clear();
                msVendor.innerControlHiddenField.Value = string.Empty;
                msVendor.innerControlHiddenField2.Value = string.Empty;
                for (int iCount = 0; iCount < lstVendorTemplate.Count; iCount++)
                {
                    lstSelectedVendor.Items.Add(new ListItem(lstVendorTemplate[iCount].Vendor.Vendor_Name, lstVendorTemplate[iCount].Vendor.VendorID.ToString()));
                    msVendor.SelectedVendorIDs = lstVendorTemplate[iCount].Vendor.VendorID.ToString();
                    msVendor.SelectedVendorName = lstVendorTemplate[iCount].Vendor.Vendor_Name.ToString();
                }
            }
        }
        else
        {
            lstSelectedVendor.Items.Clear();
            msVendor.innerControlHiddenField.Value = string.Empty;
            msVendor.innerControlHiddenField2.Value = string.Empty;
        }
    }

    protected void btnGenerateReport_Click(object sender, EventArgs e)
    {
        string reportView = String.Empty;

        if (rdoAllOnly.Checked)
            reportView = "All";
        else if (rdoPending.Checked)
            reportView = "Pending";
        else if (rdoPosted.Checked)
            reportView = "Posted";

        DiscrepancyReportBE oDiscrepancyReportBE = new DiscrepancyReportBE();
        DiscrepancyReportBAL oDiscrepancyReportBAL = new DiscrepancyReportBAL();


        oDiscrepancyReportBE.Action = rdoSummary.Checked ? "VendorChargesSummaryReport" : "VendorChargesDetailedReport";
        //oDiscrepancyReportBE.SelectedCountryIDs = msCountry.SelectedCountryIDs;
        oDiscrepancyReportBE.SelectedSiteIDs = msSite.SelectedSiteIDs;
        //  oDiscrepancyReportBE.SelectedStockPlannerIDs = msStockPlanner.SelectedStockPlannerIDs;
        oDiscrepancyReportBE.SelectedVendorIDs = msVendor.SelectedVendorIDs;
        oDiscrepancyReportBE.DateFrom = string.IsNullOrEmpty(txtFromDate.Text) ? (DateTime?)null : Common.GetMM_DD_YYYY(txtFromDate.Text);
        oDiscrepancyReportBE.DateTo = string.IsNullOrEmpty(txtToDate.Text) ? (DateTime?)null : Common.GetMM_DD_YYYY(txtToDate.Text);
        oDiscrepancyReportBE.SelectedTypes = reportView;

        if (reportView == "All")
        {
            oDiscrepancyReportBE.SelectedTypes = "Pending";
        }
        // oDiscrepancyReportBE.DiscrepancyTypeId = rdoDiscrepancy.Checked ? Convert.ToInt32(ddlDiscrepancyType.SelectedValue) : (int?)null;

        DataSet dsFunctionalVendorChargeRpt = oDiscrepancyReportBAL.getDiscrepancyReportBAL(oDiscrepancyReportBE);

        if (reportView == "All")
        {
            oDiscrepancyReportBE.SelectedTypes = "Posted";
            dsFunctionalVendorChargeRpt.Merge(oDiscrepancyReportBAL.getDiscrepancyReportBAL(oDiscrepancyReportBE), true, MissingSchemaAction.Ignore);
        }

        oDiscrepancyReportBAL = null;
        Session["DataSetObject"] = dsFunctionalVendorChargeRpt.Tables[0];

        if (oDiscrepancyReportBE.Action == "VendorChargesSummaryReport")
        {
            var lstFunctionalVendorChargeRpt =
                                dsFunctionalVendorChargeRpt.Tables[0].AsEnumerable()
                                .Select(x =>
                                    new
                                    {
                                        SiteName = x["SiteName"],
                                        Vendor_Name = x["Vendor_Name"],
                                        RLabour = x["RLabour"],
                                        RCarriage = x["RCarriage"],
                                        RTotalCharges = x["RTotal"],
                                        PLabour = x["PLabour"],
                                        PCarriage = x["PCarriage"],
                                        PTotalCharges = x["PTotal"]
                                    }
                                 )
                                 .GroupBy(s => new { s.SiteName, s.Vendor_Name })
                                 .Select(g =>
                                        new
                                        {
                                            SiteName = g.Key.SiteName,
                                            Vendor_Name = g.Key.Vendor_Name,
                                            RLabour = g.Sum(x => Math.Round(Convert.ToDecimal(x.RLabour), 2)),
                                            RCarriage = g.Sum(x => Math.Round(Convert.ToDecimal(x.RCarriage), 2)),
                                            RTotalCharges = g.Sum(x => Math.Round(Convert.ToDecimal(x.RTotalCharges), 2)),
                                            PLabour = g.Sum(x => Math.Round(Convert.ToDecimal(x.PLabour), 2)),
                                            PCarriage = g.Sum(x => Math.Round(Convert.ToDecimal(x.PCarriage), 2)),
                                            PTotalCharges = g.Sum(x => Math.Round(Convert.ToDecimal(x.PTotalCharges), 2)),
                                        }
                                 ).ToList();


            DataTable dt = new DataTable();

            dt.Columns.Add("SiteName");
            dt.Columns.Add("Vendor_Name");
            dt.Columns.Add("RLabour");
            dt.Columns.Add("RCarriage");
            dt.Columns.Add("RTotalCharges");
            dt.Columns.Add("PLabour");
            dt.Columns.Add("PCarriage");
            dt.Columns.Add("PTotalCharges");

            dt.Columns.Add("DATE");
            dt.Columns.Add("Discrepancy");
            dt.Columns.Add("DiscrepancyType");
            dt.Columns.Add("UserName");
            dt.Columns.Add("StageCompletedForWhom");
            if (lstFunctionalVendorChargeRpt.Count > 0)
            {
                foreach (var item in lstFunctionalVendorChargeRpt)
                {
                    dt.Rows.Add(item.SiteName, item.Vendor_Name, Convert.ToString(item.RLabour), Convert.ToString(item.RCarriage), Convert.ToString(item.RTotalCharges), Convert.ToString(item.PLabour), Convert.ToString(item.PCarriage), Convert.ToString(item.PTotalCharges));
                }
            }
            Session["DataSetObject"] = dt;
        }






        if (oDiscrepancyReportBE.Action == "VendorChargesSummaryReport")
        {

            ReportParameter[] reportParameter = new ReportParameter[5];
            reportParameter[0] = new ReportParameter("Site", msSite.SelectedSiteName);
            reportParameter[1] = new ReportParameter("Vendor", msVendor.SelectedVendorName);
            reportParameter[2] = new ReportParameter("DateFrom", txtFromDate.Text);
            reportParameter[3] = new ReportParameter("DateTo", txtToDate.Text);
            reportParameter[4] = new ReportParameter("ReportView", reportView);
            Session["ReportParameter"] = reportParameter;

            Session["ReportPath"] = "\\ModuleUI\\Discrepancy\\Report\\RDLC\\VendorChargesSummaryReport.rdlc";
            Session["DateTableName"] = "dtDiscrepancySummaryReport";
        }


        else if (oDiscrepancyReportBE.Action == "VendorChargesDetailedReport")
        {
            ReportParameter[] reportParameter = new ReportParameter[5];

            reportParameter[0] = new ReportParameter("Site", msSite.SelectedSiteName);
            reportParameter[1] = new ReportParameter("Vendor", msVendor.SelectedVendorName);
            reportParameter[2] = new ReportParameter("DateFrom", txtFromDate.Text);
            reportParameter[3] = new ReportParameter("DateTo", txtToDate.Text);
            reportParameter[4] = new ReportParameter("ReportView", reportView);
            Session["ReportParameter"] = reportParameter;

            Session["ReportPath"] = "\\ModuleUI\\Discrepancy\\Report\\RDLC\\VendorChargesDetailedReport.rdlc";
            Session["DateTableName"] = "dtDiscrepancySummaryReport";
            //getSubReportData();
            //Session["DataSetObject"] = dsFunctionalVDRTracker;
            //Session["DateTableNameSubreport"] = "dtDiscrepancyDetailedReport_Sites";
        }


        EncryptQueryString("../../ReportView.aspx?PageName=" + "VRC");

    }
    private void getSubReportData()
    {
        DiscrepancyReportBE oDiscrepancyReportBE = new DiscrepancyReportBE();
        DiscrepancyReportBAL oDiscrepancyReportBAL = new DiscrepancyReportBAL();
        oDiscrepancyReportBE.Action = "DiscrepancyDetailedSubReport";
        Session["dsSubReortData"] = oDiscrepancyReportBAL.getDiscrepancyReportBAL(oDiscrepancyReportBE);
        oDiscrepancyReportBAL = null;
    }
}
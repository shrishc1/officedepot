﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    AutoEventWireup="true" CodeFile="ShuttleDiscrepancyReport.aspx.cs" Inherits="ShuttleDiscrepancyReport" %>

<%@ Register Src="~/CommonUI/UserControls/MultiSelectCountry.ascx" TagName="MultiSelectCountry"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectSite.ascx" TagName="MultiSelectSite"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectAP.ascx" TagName="MultiSelectAP"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectVendor.ascx" TagName="MultiSelectVendor"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectStockPlanner.ascx" TagName="MultiSelectStockPlanner"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectDiscType.ascx" TagName="MultiSelectDiscType"
    TagPrefix="cc1" %>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script type="text/javascript">
        $(document).ready(function () {
            $("body").keypress(function (e) {
                if (e.which == 13 || e.keyCode == 13) {
                    $('#<%=btnGenerateReport.ClientID %>').click();
                    return false;
                }
            });
        });
    </script>
    <h2>
        <cc1:ucLabel ID="lblShuttleDiscrepancyReport" runat="server" Text="Shuttle Discrepancy Report"></cc1:ucLabel>
    </h2>
    <asp:ScriptManager ID="SM1" runat="server"></asp:ScriptManager>
    <div class="right-shadow">
        <div class="formbox">
            <table width="100%" cellspacing="0" cellpadding="0" border="0" align="center" class="top-settings">
                <tr>
                    <td style="font-weight: bold;">
                        <cc1:ucLabel ID="lblReceivingSite" runat="server" Text="Receiving Site"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold;width:10px;">
                        :
                    </td>
                    <td>
                        <cc1:MultiSelectSite runat="server" ID="msReceivingSite" />
                    </td>
                </tr>                
                <tr>
                    <td colspan="3"><br /></td>
                </tr>
                <tr>
                    <td style="font-weight: bold;">
                        <cc1:ucLabel ID="lblSendingSite" runat="server" Text="Sending Site"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold;">
                        :
                    </td>
                    <td >
                        <cc1:MultiSelectSite runat="server" ID="msSendingSite" />
                    </td>
                </tr>
                <tr>
                    <td colspan="3"><br /></td>
                </tr>
                <tr>
                    <td style="font-weight: bold;">
                        <cc1:ucLabel ID="lblType" runat="server" Text="Type"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold;">
                        :
                    </td>
                    <td >
                        <cc1:MultiSelectDiscType runat="server" ID="msSelectDiscType" />
                    </td>
                </tr>
                <tr>
                    <td colspan="3"><br /></td>
                </tr>
                <tr>
                    <td style="font-weight: bold;">
                        <cc1:ucLabel ID="lblPO" runat="server" Text="PO #"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold;">
                        :
                    </td>
                    <td >
                        <cc1:ucTextbox ID="txtPOText" runat="server" Width="145px" />                        
                    </td>
                </tr>
                <tr>
                    <td colspan="3"><br /></td>
                </tr>
                <tr>
                    <td style="font-weight: bold;">
                        <cc1:ucLabel ID="lblSkuWithHash" runat="server" Text="PO #"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold;">
                        :
                    </td>
                    <td>
                        <cc1:ucTextbox ID="txtSkuWithHashText" runat="server" Width="145px" />
                    </td>
                </tr>
                <tr>
                    <td colspan="3" style="width: 100%;">
                        <br />
                        <table cellpadding="0" cellspacing="0">
                            <tr>
                                <td style="font-weight: bold; width: 94px">
                                    <cc1:ucLabel ID="lblDateFrom" runat="server" Text="Date From"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold;width:10px;">
                                    :
                                </td>
                                <td style="font-weight: bold;width:72px;">
                                    <cc1:ucTextbox ID="txtFromDate" ClientIDMode="Static" runat="server" CssClass="date"
                                        Width="70px" />
                                </td>
                                <td style="font-weight: bold;text-align: center;width:50px;">
                                    <cc1:ucLabel ID="lblTo" runat="server" Text="To"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold;">
                                    <cc1:ucTextbox ID="txtToDate" ClientIDMode="Static" runat="server" CssClass="date"
                                        Width="70px" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div class="button-row">
        <cc1:ucButton ID="btnGenerateReport" runat="server" Text="Generate Report" CssClass="button"
            OnClick="btnGenerateReport_Click" />
    </div>
</asp:Content>

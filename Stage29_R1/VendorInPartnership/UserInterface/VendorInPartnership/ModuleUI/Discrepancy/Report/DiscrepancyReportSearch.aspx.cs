﻿using System;
using System.Data;
using BusinessEntities.ModuleBE.AdminFunctions;
using BusinessLogicLayer.ModuleBAL.AdminFunctions;
using System.Collections.Generic;
using BusinessEntities.ModuleBE.Discrepancy.Report;
using BusinessLogicLayer.ModuleBAL.Discrepancy.Report;
using System.Collections;
using BusinessEntities.ModuleBE.Discrepancy.LogDiscrepancy;
using BusinessLogicLayer.ModuleBAL.Discrepancy.LogDiscrepancy;
using Utilities; using WebUtilities;
using Microsoft.Reporting.WebForms;
using BusinessEntities.ModuleBE.Security;
using BusinessLogicLayer.ModuleBAL.Security;
using System.Web.UI.WebControls;

public partial class DiscrepancyReportSearch : CommonPage
{
    protected void Page_Init(object sender, EventArgs e)
    {
        ucVendorTemplateSelect.CurrentPage = this;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindDiscrepancyType();
            txtFromDate.Text = DateTime.Now.AddDays(-DateTime.Now.Day + 1).ToString("dd/MM/yyyy");
            txtToDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
            txtFromDate.Attributes.Add("readonly", "readonly");
            txtToDate.Attributes.Add("readonly", "readonly");
        }
    }
    private void BindDiscrepancyType()
    {
        DiscrepancyBE oNewDiscrepancyBE = new DiscrepancyBE();
        DiscrepancyBAL oNewDiscrepancyBAL = new DiscrepancyBAL();

        oNewDiscrepancyBE.Action = "GetDiscrepancyType";


        List<DiscrepancyBE> lstDiscrepancyType = oNewDiscrepancyBAL.GetDiscrepancyTypeBAL(oNewDiscrepancyBE);

        if (lstDiscrepancyType != null)
        {

            FillControls.FillDropDown(ref ddlDiscrepancyType, lstDiscrepancyType, "DiscrepancyType", "DiscrepancyTypeID", "---Select---");
        }
    }

    /// <summary>
    /// Method to show Vendor on the basis of selected Template
    /// </summary>
    public override void TemplateSelectedIndexChanged()
    {
        base.TemplateSelectedIndexChanged();

        int VendorTemplateId = Convert.ToInt32(ucVendorTemplateSelect.innerControlddlTemplate.SelectedValue);

        ListBox lstSelectedVendor = (ListBox)msVendor.FindControl("lstRight");
        ListBox lstVendor = (ListBox)msVendor.FindControl("ucVendor").FindControl("lstLeft");

        if (VendorTemplateId > 0)
        {

            SCT_TemplateBAL oSCT_TemplateBAL = new SCT_TemplateBAL();
            SCT_TemplateBE oSCT_TemplateBE = new SCT_TemplateBE();

            oSCT_TemplateBE.VendorTemplateID = VendorTemplateId;
            oSCT_TemplateBE.Action = "GetVendorTemplateInfobyID";
            List<SCT_TemplateBE> lstVendorTemplate = oSCT_TemplateBAL.GetVendorTemplateById(oSCT_TemplateBE);

            if (lstVendorTemplate != null && lstVendorTemplate.Count > 0)
            {
                lstSelectedVendor.Items.Clear();
                msVendor.innerControlHiddenField.Value = string.Empty;
                msVendor.innerControlHiddenField2.Value = string.Empty;
                for (int iCount = 0; iCount < lstVendorTemplate.Count; iCount++)
                {
                    lstSelectedVendor.Items.Add(new ListItem(lstVendorTemplate[iCount].Vendor.Vendor_Name, lstVendorTemplate[iCount].Vendor.VendorID.ToString()));
                    msVendor.SelectedVendorIDs = lstVendorTemplate[iCount].Vendor.VendorID.ToString();
                    msVendor.SelectedVendorName = lstVendorTemplate[iCount].Vendor.Vendor_Name.ToString();
                }
            }
        }
        else
        {
            lstSelectedVendor.Items.Clear();
            msVendor.innerControlHiddenField.Value = string.Empty;
            msVendor.innerControlHiddenField2.Value = string.Empty;
        }
    }

    protected void btnGenerateReport_Click(object sender, EventArgs e)
    {
        DiscrepancyReportBE oDiscrepancyReportBE = new DiscrepancyReportBE();
        DiscrepancyReportBAL oDiscrepancyReportBAL = new DiscrepancyReportBAL();


        oDiscrepancyReportBE.Action = rdoSummary.Checked ? "DiscrepancySummaryReport" : (rdoDetail.Checked ? "DiscrepancyDetailedReport" : "DiscrepancyTypeReport");
        oDiscrepancyReportBE.SelectedCountryIDs = msCountry.SelectedCountryIDs;
        oDiscrepancyReportBE.SelectedSiteIDs = msSite.SelectedSiteIDs;
        oDiscrepancyReportBE.SelectedStockPlannerIDs = msStockPlanner.SelectedStockPlannerIDs;
        oDiscrepancyReportBE.SelectedVendorIDs = msVendor.SelectedVendorIDs;
        oDiscrepancyReportBE.DateFrom = string.IsNullOrEmpty(txtFromDate.Text) ? (DateTime?)null : Common.GetMM_DD_YYYY(txtFromDate.Text);
        oDiscrepancyReportBE.DateTo = string.IsNullOrEmpty(txtToDate.Text) ? (DateTime?)null : Common.GetMM_DD_YYYY(txtToDate.Text);
        oDiscrepancyReportBE.DiscrepancyTypeId = rdoDiscrepancy.Checked ? Convert.ToInt32(ddlDiscrepancyType.SelectedValue) : (int?)null;

        DataSet dsFunctionalVDRTracker = oDiscrepancyReportBAL.getDiscrepancyReportBAL(oDiscrepancyReportBE);
        oDiscrepancyReportBAL = null;
        Session["DataSetObject"] = dsFunctionalVDRTracker.Tables[0];


        if (oDiscrepancyReportBE.Action == "YTDReport")
        {
            // Session["ReportPath"] = "\\ModuleUI\\Discrepancy\\Report\\RDLC\\SummaryFunctionalVDRTracker.rdlc";
            //Session["DateTableName"] = "dtDiscrepancySummaryReport";
        }
        else if (oDiscrepancyReportBE.Action == "DiscrepancySummaryReport")
        {
            ReportParameter[] reportParameter = new ReportParameter[6];
            reportParameter[0] = new ReportParameter("Country", msCountry.SelectedCountryName);
            reportParameter[1] = new ReportParameter("Site", msSite.SelectedSiteName);
            reportParameter[2] = new ReportParameter("Vendor", msVendor.SelectedVendorName);
            reportParameter[3] = new ReportParameter("StockPlanner", msStockPlanner.SelectedSPName);
            reportParameter[4] = new ReportParameter("DateFrom", txtFromDate.Text);
            reportParameter[5] = new ReportParameter("DateTo", txtToDate.Text);
            Session["ReportParameter"] = reportParameter;

            Session["ReportPath"] = "\\ModuleUI\\Discrepancy\\Report\\RDLC\\DiscrepancySummaryReport.rdlc";
            Session["DateTableName"] = "dtDiscrepancySummaryReport";
        }
        else if (oDiscrepancyReportBE.Action == "DiscrepancyTypeReport")
        {
            ReportParameter[] reportParameter = new ReportParameter[7];
            reportParameter[0] = new ReportParameter("Country", msCountry.SelectedCountryName);
            reportParameter[1] = new ReportParameter("Site", msSite.SelectedSiteName);
            reportParameter[2] = new ReportParameter("Vendor", msVendor.SelectedVendorName);
            reportParameter[3] = new ReportParameter("StockPlanner", msStockPlanner.SelectedSPName);
            reportParameter[4] = new ReportParameter("DateFrom", txtFromDate.Text);
            reportParameter[5] = new ReportParameter("DateTo", txtToDate.Text);
            reportParameter[6] = new ReportParameter("DiscrepancyType", ddlDiscrepancyType.SelectedIndex > 0 ? ddlDiscrepancyType.SelectedItem.Text : null);
            Session["ReportParameter"] = reportParameter;
            
            Session["ReportPath"] = "\\ModuleUI\\Discrepancy\\Report\\RDLC\\DiscrepancyTypeReport.rdlc";
            Session["DateTableName"] = "dtDiscrepancyTypeReport";

        }
       
        else if (oDiscrepancyReportBE.Action == "DiscrepancyDetailedReport")
        {
            ReportParameter[] reportParameter = new ReportParameter[6];
            reportParameter[0] = new ReportParameter("Country", msCountry.SelectedCountryName);
            reportParameter[1] = new ReportParameter("Site", msSite.SelectedSiteName);
            reportParameter[2] = new ReportParameter("Vendor", msVendor.SelectedVendorName);
            reportParameter[3] = new ReportParameter("StockPlanner", msStockPlanner.SelectedSPName);
            reportParameter[4] = new ReportParameter("DateFrom", txtFromDate.Text);
            reportParameter[5] = new ReportParameter("DateTo", txtToDate.Text);
            Session["ReportParameter"] = reportParameter;

            Session["ReportPath"] = "\\ModuleUI\\Discrepancy\\Report\\RDLC\\DiscrepancyDetailedReport.rdlc";
            Session["DateTableName"] = "dtDiscrepancyDetailedReportNew";
            //getSubReportData();
            //Session["DataSetObject"] = dsFunctionalVDRTracker;
            //Session["DateTableNameSubreport"] = "dtDiscrepancyDetailedReport_Sites";
        }


        EncryptQueryString("../../ReportView.aspx?PageName=" + "DRS");

    }
    private void getSubReportData()
    {
        DiscrepancyReportBE oDiscrepancyReportBE = new DiscrepancyReportBE();
        DiscrepancyReportBAL oDiscrepancyReportBAL = new DiscrepancyReportBAL();
        oDiscrepancyReportBE.Action = "DiscrepancyDetailedSubReport";
        Session["dsSubReortData"] = oDiscrepancyReportBAL.getDiscrepancyReportBAL(oDiscrepancyReportBE);
        oDiscrepancyReportBAL = null;
    }
}
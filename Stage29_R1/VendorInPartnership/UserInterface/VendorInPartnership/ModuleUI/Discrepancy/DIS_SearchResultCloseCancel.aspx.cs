﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.AdminFunctions;
using BusinessEntities.ModuleBE.Discrepancy.LogDiscrepancy;
using BusinessLogicLayer.ModuleBAL.AdminFunctions;
using BusinessLogicLayer.ModuleBAL.Discrepancy.LogDiscrepancy;
using Utilities; using WebUtilities;
using BusinessEntities.ModuleBE.Security;
using BusinessLogicLayer.ModuleBAL.Security;
using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using BusinessLogicLayer.ModuleBAL.Appointment.SiteSettings;
using BaseControlLibrary;
public partial class DIS_SearchResultCloseCancel : CommonPage
{

    System.Text.StringBuilder sSendMailLink = new System.Text.StringBuilder();

    protected void Page_Prerender(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {            
           // ucSeacrhVendor1.IsParentRequired = true;
            //ucSeacrhVendor1.CountryID = -1;
           // ucSeacrhVendor1.FillVendor();
            txtDiscrepancyDateRangeFrom.Attributes.Add("readonly", "readonly");
            txtDiscrepancyDateRangeTo.Attributes.Add("readonly", "readonly");
        }
    }

    protected void RetainDiscrepancyData()
    {
        //*********** Site ***************
        string SiteId = ViewState["SiteId"] != null ? ViewState["SiteId"].ToString() : "";
        ucListBox lstRightSite = msSite.FindControl("lstRight") as ucListBox;
        ucListBox lstLeftSite = msSite.FindControl("lstLeft") as ucListBox;
        string msSiteid = string.Empty;
        lstRightSite.Items.Clear();
        lstRightSite.Items.Clear();
        msSite.SelectedSiteIDs = "";
        if (lstRightSite != null && lstLeftSite != null && !string.IsNullOrEmpty(SiteId))
        {
            //lstLeftSite.Items.Clear();
            lstRightSite.Items.Clear();
            msSite.BindSite();

            string[] strSiteIDs = SiteId.Split(',');
            for (int index = 0; index < strSiteIDs.Length; index++)
            {
                ListItem listItem = lstLeftSite.Items.FindByValue(strSiteIDs[index]);
                if (listItem != null)
                {
                    msSiteid = msSiteid + strSiteIDs[index].ToString() + ",";
                    lstRightSite.Items.Add(listItem);
                    lstLeftSite.Items.Remove(listItem);
                }

            }
            if (string.IsNullOrEmpty(msSite.SelectedSiteIDs))
                msSite.SelectedSiteIDs = msSiteid.Trim(',');
        }
    //****************** Vendor ***********************
        string IncludeVendorId = ViewState["VendorId"] != null ? ViewState["VendorId"].ToString() : "";
        string IncludeVendorText = ViewState["SearchedVendorText"] != null ? ViewState["SearchedVendorText"].ToString() : "";
        ucListBox lstRightIncludeVendor = ucSeacrhVendor1.FindControl("lstRight") as ucListBox;
        ucListBox lstLeftIncludeVendor = ucSeacrhVendor1.FindControl("ucVendor").FindControl("lstLeft") as ucListBox;
        lstLeftIncludeVendor.Items.Clear();
        if (lstRightIncludeVendor != null && lstLeftIncludeVendor != null && (!string.IsNullOrEmpty(IncludeVendorId) || !string.IsNullOrEmpty(IncludeVendorText)))
        {
            lstRightIncludeVendor.Items.Clear();
            lstLeftIncludeVendor.Items.Clear();
            ucSeacrhVendor1.SearchVendorClick(IncludeVendorText);
            string[] strIncludeVendorIDs = IncludeVendorId.Split(',');
            for (int index = 0; index < strIncludeVendorIDs.Length; index++)
            {
                ListItem listItem = lstLeftIncludeVendor.Items.FindByValue(strIncludeVendorIDs[index]);
                if (listItem != null)
                {
                    lstRightIncludeVendor.Items.Add(listItem);
                    lstLeftIncludeVendor.Items.Remove(listItem);
                }
            }
        }
    }
    protected void Page_InIt(object sender, EventArgs e)
    {
        msSite.isMoveAllRequired = true;

      //  ucSeacrhVendor1.IsParentRequired = true;
       // ucSeacrhVendor1.IsStandAloneRequired = true;
    }

    protected void Page_Load(object sender, EventArgs e)
    {

        Label lblModuleText = (Label)Page.Master.FindControl("lblModuleText");
        if (mvDiscrepancyList.ActiveViewIndex == 0)
        {
            lblModuleText.Text = "Discrepancies - Search Criteria";
            lblSearchResult.Text = "Search Criteria";
        }
        else
        {
            lblModuleText.Text = "Discrepancies - Search Result";
            lblSearchResult.Text = "Search Result";
        }
        if (!IsPostBack)
        {
            mvDiscrepancyList.ActiveViewIndex = 0;
            BindDiscrepancyType();
           // BindSiteList();
            BindStockPlanners();

          
        }
      
        //coming from the add image page
        if (GetQueryStringValue("FromPage") != null && GetQueryStringValue("FromPage").ToString().Trim() == "images"
          || GetQueryStringValue("PreviousPage") != null && GetQueryStringValue("PreviousPage").ToString() == "usercontrol")
        {
            mvDiscrepancyList.ActiveViewIndex = 1;
            btnSearch.Visible = false;
            BindGrid();
        }
        btnReturntoSearchCriteria.Visible = false;
    }

    protected void btnForcedClosed_Click(object sender, CommandEventArgs e)
    {
        try
        {
            DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
            DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();
            oDiscrepancyBE.Action = "UpdateLogDiscrepancyStatus";
            oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(e.CommandArgument.ToString().Trim());
            oDiscrepancyBE.DiscrepancyStatus = "F";
            oDiscrepancyBE.ForceClosedUserId = Convert.ToInt32(Session["UserID"]);
            oDiscrepancyBAL.UpdateDiscrepancyStatus(oDiscrepancyBE);
            BindGrid();
            oDiscrepancyBAL = null;
        }
        catch (Exception ex)
        {           
        }
       
    }

    #region Commented Code ...
    //protected void btnCancel_Click(object sender, CommandEventArgs e)
    //{
    //    //var oDiscrepancyBE = new DiscrepancyBE();
    //    //var oDiscrepancyBAL = new DiscrepancyBAL();
    //    //oDiscrepancyBE.Action = "UpdateLogDiscrepancyStatus";
    //    //oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(e.CommandArgument.ToString().Trim());
    //    //oDiscrepancyBE.DiscrepancyStatus = "D";
    //    //oDiscrepancyBAL.UpdateDiscrepancyStatus(oDiscrepancyBE);
    //    //oDiscrepancyBAL = null;

    //    //this.SaveAndSendDiscCommunication(0, 0, 0, 0);

    //    //this.BindGrid();
    //}
    #endregion

    public string GetUserEmailIdsOfVendor(int vendorId, int siteId)
    {
        var emailIds = string.Empty;
        if (vendorId > 0)
        {
            SCT_UserBE oSCT_UserBE = new SCT_UserBE();
            SCT_UserBAL oSCT_UserBAL = new SCT_UserBAL();
            oSCT_UserBE.Action = "GetContactDetailsDiscrepancy";
            oSCT_UserBE.VendorID = vendorId;
            oSCT_UserBE.SiteId = siteId;
            List<SCT_UserBE> lstVendorDetails1 = oSCT_UserBAL.GetUserDetailsBAL(oSCT_UserBE);
            if (lstVendorDetails1 != null && lstVendorDetails1.Count > 0)
            {
                foreach (SCT_UserBE item in lstVendorDetails1)
                    emailIds += item.EmailId.ToString() + ", ";                    

                emailIds = emailIds.Trim(new char[] { ',', ' ' });                
            }          
        }

        if (string.IsNullOrEmpty(emailIds))
            emailIds = System.Configuration.ConfigurationManager.AppSettings["DefaultEmailAddress"];

        return emailIds;
    }

    private int? SaveAndSendDiscCommunication(int discrepancyLogID, int queryDisId, int vendorId, int siteId)
    {
        var emailIds = this.GetUserEmailIdsOfVendor(vendorId, siteId);

        if (!string.IsNullOrEmpty(emailIds))
        {
            var discType = "deleteddiscrepancy";
            BaseControlLibrary.ucTextbox txtEmailIds = new BaseControlLibrary.ucTextbox();
            txtEmailIds.Text = emailIds;

            var sendComm = new sendCommunication();
            int? retVal = sendComm.sendCommunicationByEMail(discrepancyLogID, discType, txtEmailIds, txtEmailIds, vendorId);
            sSendMailLink = sendComm.sSendMailLink;
            return retVal;
        }
        
        return null;
    }

    #region SearchCriteria
    private void BindDiscrepancyType()
    {
        DiscrepancyBE oNewDiscrepancyBE = new DiscrepancyBE();
        DiscrepancyBAL oNewDiscrepancyBAL = new DiscrepancyBAL();

        oNewDiscrepancyBE.Action = "GetDiscrepancyType";


        List<DiscrepancyBE> lstDiscrepancyType = oNewDiscrepancyBAL.GetDiscrepancyTypeBAL(oNewDiscrepancyBE);
        oNewDiscrepancyBAL = null;
        if (lstDiscrepancyType != null)
        {

            FillControls.FillDropDown(ref ddlDiscrepancyType, lstDiscrepancyType, "DiscrepancyType", "DiscrepancyTypeID", "---Select---");
        }
    }

  
    private void BindStockPlanners()
    {
        DiscrepancyBE oNewDiscrepancyBE = new DiscrepancyBE();
        DiscrepancyBAL oNewDiscrepancyBAL = new DiscrepancyBAL();

        oNewDiscrepancyBE.Action = "GetStockPlanners";


        List<DiscrepancyBE> lstStockPlanners = oNewDiscrepancyBAL.GetStockPlannersBAL(oNewDiscrepancyBE);

        if (lstStockPlanners != null)
        {
            FillControls.FillDropDown(ref ddlStockPlanner, lstStockPlanners, "StockPlannerNo", "UserID", "---Select---");
        }
        oNewDiscrepancyBAL = null;
    }

  



    protected void btnSearch_Click(object sender, EventArgs e)
    {
        mvDiscrepancyList.ActiveViewIndex = 1;
        Label lblModuleText = (Label)Page.Master.FindControl("lblModuleText");
        lblModuleText.Text = "Discrepancies - Search Result";
        lblSearchResult.Text = "Search Result";
        btnSearch.Visible = false;
        btnReturntoSearchCriteria.Visible = true;
        BindGrid();
    }

   
    #endregion

    #region SearchListing
    private void BindGrid()
    {
        DiscrepancyBE oNewDiscrepancyBE = new DiscrepancyBE();
        DiscrepancyBAL oNewDiscrepancyBAL = new DiscrepancyBAL();
        oNewDiscrepancyBE.Action = "GetDiscrepancyLog";

        //Search Creteria
        oNewDiscrepancyBE.SCDiscrepancyTypeID = ddlDiscrepancyType.SelectedIndex != 0 ? Convert.ToInt32(ddlDiscrepancyType.SelectedItem.Value) : (int?)null;
        oNewDiscrepancyBE.SCDiscrepancyNo = txtDiscrepancyNo.Text != string.Empty ? txtDiscrepancyNo.Text.Trim() : null;
        oNewDiscrepancyBE.SCPurchaseOrderNo = txtPurchaseOrder.Text != string.Empty ? txtPurchaseOrder.Text.Trim() : null;
        oNewDiscrepancyBE.SCPurchaseOrderDate = string.IsNullOrEmpty(txtPurchaseOrderDate.Text) ? (DateTime?)null : Common.GetMM_DD_YYYY(txtPurchaseOrderDate.Text);
        oNewDiscrepancyBE.SCDeliveryNoteNumber = txtDeliveryNote.Text != string.Empty ? txtDeliveryNote.Text.Trim() : null;
        oNewDiscrepancyBE.SCDiscrepancyDateFrom = string.IsNullOrEmpty(txtDiscrepancyDateRangeFrom.Text) ? (DateTime?)null : Common.GetMM_DD_YYYY(txtDiscrepancyDateRangeFrom.Text);
        // to show today's record when discrepancy is logged and viewmode 1 is active
        if (GetQueryStringValue("FromPage") != null && GetQueryStringValue("FromPage").ToString().Trim() == "images")
        {
            oNewDiscrepancyBE.SCDiscrepancyDateFrom = DateTime.Now;
            oNewDiscrepancyBE.SCDiscrepancyDateTo = DateTime.Now;
        }
        oNewDiscrepancyBE.SCDiscrepancyDateTo = string.IsNullOrEmpty(txtDiscrepancyDateRangeTo.Text) ? (DateTime?)null : Common.GetMM_DD_YYYY(txtDiscrepancyDateRangeTo.Text);

        oNewDiscrepancyBE.UserID = Session["UserID"] != null ? Convert.ToInt32(Session["UserID"]) : (int?)null;
        oNewDiscrepancyBE.SCSelectedSiteIDs = msSite.SelectedSiteIDs;
        ViewState["SiteId"] = msSite.SelectedSiteIDs;
        //if (UclstSiteSelected.Items.Count > 0)
        //{
        //    for (int i = 0; i < UclstSiteSelected.Items.Count; i++)
        //    {
        //        oNewDiscrepancyBE.SCSelectedSiteIDs += UclstSiteSelected.Items[i].Value + ",";
        //    }
        //    oNewDiscrepancyBE.SCSelectedSiteIDs = oNewDiscrepancyBE.SCSelectedSiteIDs.Trim(',');
        //}
        //else
        //    oNewDiscrepancyBE.SCSelectedSiteIDs = null;

        //ListBox lstSelectedVendor = (ListBox)ucSeacrhVendor1.FindControl("lstRight");

        //if (lstSelectedVendor.Items.Count > 0)
        //{
        //    foreach (ListItem item in lstSelectedVendor.Items)
        //    {
        //        oNewDiscrepancyBE.SCSelectedVendorIDs += item.Value + ",";
        //    }
        //    oNewDiscrepancyBE.SCSelectedVendorIDs = oNewDiscrepancyBE.SCSelectedVendorIDs.Trim(',');
        //}
        //else
        //{
        //    oNewDiscrepancyBE.SCSelectedVendorIDs = null;
        //}
        TextBox txtSearchedVendor = ucSeacrhVendor1.FindControl("ucVendor").FindControl("txtVendorNo") as TextBox;
        ViewState["SearchedVendorText"] = txtSearchedVendor.Text;
        oNewDiscrepancyBE.SCSelectedVendorIDs = ucSeacrhVendor1.SelectedVendorIDs;
        ViewState["VendorId"] = ucSeacrhVendor1.SelectedVendorIDs;
        oNewDiscrepancyBE.SCSPUserID = ddlStockPlanner.SelectedIndex != 0 ? Convert.ToInt32(ddlStockPlanner.SelectedItem.Value) : (int?)null;
        oNewDiscrepancyBE.DiscrepancyStatus = string.IsNullOrEmpty(ddlDiscrepancyStatus.SelectedValue) ? null : ddlDiscrepancyStatus.SelectedValue;

        if (ddlDiscrepancyStatus.SelectedValue == "D")
        {
            oNewDiscrepancyBE.IsDelete = true;
        } 
        List<DiscrepancyBE> lstDisLog = oNewDiscrepancyBAL.GetDiscrepancyLogDetailsBAL(oNewDiscrepancyBE);
        if (lstDisLog != null && lstDisLog.Count > 0)
        {
            gvDisLog.DataSource = lstDisLog;
            gvDisLog.DataBind();
            ViewState["lstSites"] = lstDisLog;
        }
        else
        {
            gvDisLog.DataSource = lstDisLog;
            gvDisLog.DataBind();
            ViewState["lstSites"] = lstDisLog;
        }
        oNewDiscrepancyBAL = null;
        msSite.setSitesOnPostBack();
        ucSeacrhVendor1.setVendorsOnPostBack();
    }

    protected void gvDisLog_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType != DataControlRowType.Header
            && e.Row.RowType != DataControlRowType.Footer)
        {
            DateTime dDiscrepancyLogDate = Convert.ToDateTime(DataBinder.Eval(e.Row.DataItem, "DiscrepancyLogDate"));
            e.Row.Cells[4].Text = dDiscrepancyLogDate.ToString("dd/MM/yyyy");
            e.Row.Cells[4].HorizontalAlign = HorizontalAlign.Left;
            HyperLink hypLinkToDisDetails = (HyperLink)e.Row.FindControl("hypLinkToDisDetails");
            HiddenField hdnDisType = (HiddenField)e.Row.FindControl("hdnDisType");
            HiddenField hdnDisLogID = (HiddenField)e.Row.FindControl("hdnDisLogID");
            HiddenField hdnUserID = (HiddenField)e.Row.FindControl("hdnUserID");
            string VDRNo = hypLinkToDisDetails.Text;
            string status = e.Row.Cells[1].Text.ToString();
            switch (Convert.ToInt32(hdnDisType.Value.Trim()))
            {
                case 1:
                    hypLinkToDisDetails.NavigateUrl = EncryptQuery("LogDiscrepancy/DISLog_Overs.aspx?status=" + status + "&disLogID=" + hdnDisLogID.Value + "&VDRNo=" + VDRNo + "&PreviousPage=CloseCancel");
                    break;
                case 2:
                    hypLinkToDisDetails.NavigateUrl = EncryptQuery("LogDiscrepancy/DISLog_Shortage.aspx?status=" + status + "&disLogID=" + hdnDisLogID.Value + "&VDRNo=" + VDRNo + "&PreviousPage=CloseCancel");
                    break;
                case 3:
                    hypLinkToDisDetails.NavigateUrl = EncryptQuery("LogDiscrepancy/DISLog_GoodsReceivedDamaged.aspx?status=" + status + "&disLogID=" + hdnDisLogID.Value + "&VDRNo=" + VDRNo + "&PreviousPage=CloseCancel");
                    break;
                case 4:
                    hypLinkToDisDetails.NavigateUrl = EncryptQuery("LogDiscrepancy/DISLog_NoPurchaseOrder.aspx?status=" + status + "&disLogID=" + hdnDisLogID.Value + "&VDRNo=" + VDRNo + "&PreviousPage=CloseCancel");
                    break;
                case 5:
                    hypLinkToDisDetails.NavigateUrl = EncryptQuery("LogDiscrepancy/DISLog_NoPaperwork.aspx?status=" + status + "&disLogID=" + hdnDisLogID.Value + "&VDRNo=" + VDRNo + "&PreviousPage=CloseCancel");
                    break;
                case 6:
                    hypLinkToDisDetails.NavigateUrl = EncryptQuery("LogDiscrepancy/DISLog_IncorrectProduct.aspx?status=" + status + "&disLogID=" + hdnDisLogID.Value + "&VDRNo=" + VDRNo + "&PreviousPage=CloseCancel");
                    break;
                case 7:
                    hypLinkToDisDetails.NavigateUrl = EncryptQuery("LogDiscrepancy/DISLog_PresentationIssue.aspx?status=" + status + "&disLogID=" + hdnDisLogID.Value + "&VDRNo=" + VDRNo + "&PreviousPage=CloseCancel");
                    break;
                case 8:
                    hypLinkToDisDetails.NavigateUrl = EncryptQuery("LogDiscrepancy/DISLog_IncorrectAddress.aspx?status=" + status + "&disLogID=" + hdnDisLogID.Value + "&VDRNo=" + VDRNo + "&PreviousPage=CloseCancel");
                    break;
                case 9:
                    hypLinkToDisDetails.NavigateUrl = EncryptQuery("LogDiscrepancy/DISLog_PaperworkAmended.aspx?status=" + status + "&disLogID=" + hdnDisLogID.Value + "&VDRNo=" + VDRNo + "&PreviousPage=CloseCancel");
                    break;
                case 10:
                    hypLinkToDisDetails.NavigateUrl = EncryptQuery("LogDiscrepancy/DISLog_WrongPackSize.aspx?status=" + status + "&disLogID=" + hdnDisLogID.Value + "&VDRNo=" + VDRNo + "&PreviousPage=CloseCancel");
                    break;
                case 11:
                    hypLinkToDisDetails.NavigateUrl = EncryptQuery("LogDiscrepancy/DISLog_FailPalletSpecification.aspx?status=" + status + "&disLogID=" + hdnDisLogID.Value + "&VDRNo=" + VDRNo + "&PreviousPage=CloseCancel");
                    break;
                case 12:
                    hypLinkToDisDetails.NavigateUrl = EncryptQuery("LogDiscrepancy/DISLog_QualityIssue.aspx?status=" + status + "&disLogID=" + hdnDisLogID.Value + "&VDRNo=" + VDRNo + "&PreviousPage=CloseCancel");
                    break;
                case 13:
                    hypLinkToDisDetails.NavigateUrl = EncryptQuery("LogDiscrepancy/DISLog_PrematureInvoiceReceipt.aspx?status=" + status + "&disLogID=" + hdnDisLogID.Value + "&VDRNo=" + VDRNo + "&PreviousPage=CloseCancel");
                    break;
                case 14:
                    hypLinkToDisDetails.NavigateUrl = EncryptQuery("LogDiscrepancy/DISLog_GenericDescrepancy.aspx?status=" + status + "&disLogID=" + hdnDisLogID.Value + "&VDRNo=" + VDRNo + "&PreviousPage=CloseCancel");
                    break;
                case 15:
                    hypLinkToDisDetails.NavigateUrl = EncryptQuery("LogDiscrepancy/DISLog_Shuttle.aspx?status=" + status + "&disLogID=" + hdnDisLogID.Value + "&VDRNo=" + VDRNo + "&PreviousPage=CloseCancel");
                    break;
                case 16:
                    hypLinkToDisDetails.NavigateUrl = EncryptQuery("LogDiscrepancy/DISLog_Reservation.aspx?status=" + status + "&disLogID=" + hdnDisLogID.Value + "&VDRNo=" + VDRNo + "&PreviousPage=CloseCancel");
                    break;
                case 17:
                    hypLinkToDisDetails.NavigateUrl = EncryptQuery("LogDiscrepancy/DISLog_InvoiceDiscrepancy.aspx?status=" + status + "&disLogID=" + hdnDisLogID.Value + "&VDRNo=" + VDRNo + "&PreviousPage=CloseCancel");
                    break;
                case 18:
                    hypLinkToDisDetails.NavigateUrl = EncryptQuery("LogDiscrepancy/DISLog_StockPlannerInvoiceDiscrepancy.aspx?status=" + status + "&disLogID=" + hdnDisLogID.Value + "&VDRNo=" + VDRNo + "&PreviousPage=CloseCancel");
                    break;
                case 19:
                    hypLinkToDisDetails.NavigateUrl = EncryptQuery("LogDiscrepancy/DISLog_InvoiceQueryDiscrepancy.aspx?status=" + status + "&disLogID=" + hdnDisLogID.Value + "&VDRNo=" + VDRNo + "&PreviousPage=CloseCancel");
                    break;

            }
            e.Row.Cells[3].HorizontalAlign = HorizontalAlign.Left;

            ImageButton btnForcedClosed = (ImageButton)e.Row.FindControl("btnForcedClosed");
            string DiscrepancyStatus = DataBinder.Eval(e.Row.DataItem, "DiscrepancyStatus").ToString();
            if (DiscrepancyStatus == "C" || DiscrepancyStatus == "F")
                btnForcedClosed.Enabled = false;
            else
                btnForcedClosed.Enabled = true;

        }
    }

    public override BusinessEntities.BaseBe[] SubSort(GridViewSortEventArgs e)
    {
        return Utilities.GenericListHelper<DiscrepancyBE>.SortList((List<DiscrepancyBE>)ViewState["lstSites"], e.SortExpression, e.SortDirection).ToArray();
    }

    protected void btnReturntoSearchCriteria_Click(object sender, EventArgs e)
    {
        mvDiscrepancyList.ActiveViewIndex = 0;
        btnSearch.Visible = true;
        btnReturntoSearchCriteria.Visible = false;
        Label lblModuleText = (Label)Page.Master.FindControl("lblModuleText");
        lblModuleText.Text = "Discrepancies - Search Criteria";
        lblSearchResult.Text = "Search Criteria";
        RetainDiscrepancyData();
    }
    #endregion


    protected void gvDisLog_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName.Equals("DeleteDiscrepancy"))
        {
            GridViewRow oGridViewRow = (GridViewRow)(((ImageButton)e.CommandSource).NamingContainer);
            int index = oGridViewRow.RowIndex;            
            var hdnDisLogID = (HiddenField)gvDisLog.Rows[index].FindControl("hdnDisLogID");
            var hdnSiteId = (HiddenField)gvDisLog.Rows[index].FindControl("hdnSiteId");
            var hdnVendorID = (HiddenField)gvDisLog.Rows[index].FindControl("hdnVendorID");

            if (hdnDisLogID != null)
            {
                /* 
                 * ---->>>> NOTE - Commented by Jai, 13 December 6:15 PM
                 * Please don't change the sequence of SaveAndSendDiscCommunication function otherwise it will not fetch the email data, 
                 * because in [GetDiscrepancyLog] action filtered the "D" means Deleted record not coming.
                 */
                this.SaveAndSendDiscCommunication(Convert.ToInt32(hdnDisLogID.Value), 0, Convert.ToInt32(hdnVendorID.Value), Convert.ToInt32(hdnSiteId.Value));
                /*  
                    ---->>>> Regarding Deleted Discrepancy Workflow Entry
                    If required then we have to save the Discrepancy Deleted Workflow entry as per the Stage 7 Point 24B with sky blue background.
                    For reference see "$(document).ready(function ()" function on DISLog_Workflow.aspx page.
                */
                var oDiscrepancyBE = new DiscrepancyBE();
                var oDiscrepancyBAL = new DiscrepancyBAL();

                WorkflowHTML oWorkflowHTML = new WorkflowHTML();

                oDiscrepancyBE.Action = "InsertHTML";
                oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(hdnDisLogID.Value);
                oDiscrepancyBE.LoggedDateTime = DateTime.Now;
                oDiscrepancyBE.LevelNumber = 1;

                oDiscrepancyBE.DELHTML = oWorkflowHTML.function7(DateTime.Now, "aqua", "Discrepancy Deleted", Session["UserName"].ToString(), sSendMailLink.ToString(), "Discrepancy deleted and mail sent to vendor");

                int? iResult = oDiscrepancyBAL.addWorkFlowHTMLsBAL(oDiscrepancyBE);

                #region Update Log Discrepancy Status.
                
                oDiscrepancyBE.Action = "UpdateLogDiscrepancyStatus";
                oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(hdnDisLogID.Value);
                oDiscrepancyBE.DeletedUserId = Convert.ToInt32(Session["UserID"]);
                oDiscrepancyBE.DiscrepancyStatus = "D";
                oDiscrepancyBAL.UpdateDiscrepancyStatus(oDiscrepancyBE);
                oDiscrepancyBAL = null;
                #endregion
                
                this.BindGrid();
            }
        }
    }
}
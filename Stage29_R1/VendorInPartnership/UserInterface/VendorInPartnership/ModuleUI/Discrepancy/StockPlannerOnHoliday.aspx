﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    CodeFile="StockPlannerOnHoliday.aspx.cs" Inherits="ModuleUI_Discrepancy_StockPlannerOnHoliday" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectStockPlanner.ascx" TagName="MultiSelectStockPlanner"
    TagPrefix="cc1" %>
<%@ Register Src="../../CommonUI/UserControls/ucExportToExcel.ascx" TagName="ucExportToExcel"
    TagPrefix="cc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="button-row">
        <cc2:ucExportToExcel ID="ucExportToExcel" runat="server" />
    </div>
    <asp:ScriptManager ID="ScriptManager1" runat="server" AsyncPostBackTimeout="1200">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <h2>
                <cc1:ucLabel ID="lblStockPlannerHolidayCover" runat="server"></cc1:ucLabel>
            </h2>
           <table width="95%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                <tr>
                    <td align="center">
                        <cc1:ucGridView ID="grdStockPlannerHoliday" Width="100%" runat="server" CssClass="grid" OnSorting="SortGrid"
                            AllowSorting="true">
                            <Columns>
                                <asp:TemplateField HeaderText="Planner Name" SortExpression="StockPlanner">
                                    <HeaderStyle HorizontalAlign="Left" Width="10%" />
                                    <ItemStyle HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <asp:HyperLink ID="hpStockPlanner" runat="server" Text='<%# Eval("StockPlanner") %>'
                                            NavigateUrl='<%# EncryptQuery("StockPlannerOnHolidayEdit.aspx?StockPlannerID="+ Eval("StockPlannerID") + "&StockPlanner=" + Eval("StockPlanner") ) %>'></asp:HyperLink>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Absent" SortExpression="IsAbsent">
                                    <HeaderStyle Width="5%" HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <cc1:ucLiteral ID="ltIsAbsent" runat="server" Text='<%# (Eval("IsAbsent").ToString() =="True")? "Yes" : "No" %>'></cc1:ucLiteral>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Planner To Cover" SortExpression="StockPlannerToCover">
                                    <HeaderStyle Width="20%" HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <cc1:ucLiteral ID="ltStockPlannerToCover" runat="server" Text='<%# Eval("StockPlannerToCover") %>'></cc1:ucLiteral>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" />
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Date Of Return" SortExpression="DateOfReturn">
                                    <HeaderStyle Width="10%" HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <cc1:ucLiteral ID="ltDateOfReturn" runat="server" Text='<%#  Eval("DateOfReturn") %>'></cc1:ucLiteral>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                            </Columns>
                        </cc1:ucGridView>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

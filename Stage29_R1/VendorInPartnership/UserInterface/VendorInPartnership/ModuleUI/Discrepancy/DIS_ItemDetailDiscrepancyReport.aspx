﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="DIS_ItemDetailDiscrepancyReport.aspx.cs" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
 Inherits="ModuleUI_Discrepancy_DIS_ItemDetailDiscrepancyReport" %>


<%@ Register Src="~/CommonUI/UserControls/MultiSelectVendor.ascx" TagName="MultiSelectVendor"
    TagPrefix="cc2" %>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="../../CommonUI/UserControls/ucExportToExcel.ascx" TagName="ucExportToExcel"
    TagPrefix="cc2" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectStockPlanner.ascx" TagName="MultiSelectStockPlanner"
    TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script language="javascript" type="text/javascript">
        $(document).ready(function () {

            var lstitem = document.getElementById('<%= UclstSiteSelected.ClientID %>');
            if (lstitem != null) {
                if (lstitem.options.length == 0) {
                    document.getElementById('<%= hiddenSelectedIDs.ClientID %>').value = "";
                    document.getElementById('<%= hiddenSelectedName.ClientID %>').value = "";
                }
            }
        });

        $(document).ready(function () {
            $("body").keypress(function (e) {
                if (e.which == 13 || e.keyCode == 13) {
                    $('#<%=btnSearch.ClientID %>').click();
                    return false;
                }
            });
        });
    </script>
    <h2>
        <cc1:ucLabel ID="lblSearchResultT" runat="server"></cc1:ucLabel>
    </h2>
    <asp:ScriptManager ID="sm1" runat="server">
    </asp:ScriptManager>
    <div class="button-row">
        <cc2:ucExportToExcel ID="ucExportToExcel1" runat="server" Visible="false" />
    </div>
    <div class="right-shadow">
        <div class="formbox">
            <div class="button-row">
                <%--<cc1:ucButton ID="btnRunScheduler" runat="server" Text="Run Scheduler" CssClass="button"
                    onkeypress="disableEnterKey(this)" OnClick="btnScheduler_Click" />--%></div>
            <cc1:ucMultiView ID="mvDiscrepancyList" runat="server" ActiveViewIndex="0">
                <cc1:ucView ID="vwSearchCreteria" runat="server">
                    <table width="70%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                        <tr>
                            <td style="font-weight: bold; padding-left: 8px">
                                <cc1:ucLabel ID="lblDiscrepancyType" runat="server" Text="Discrepancy Type" Width="100%"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold;">
                                :
                            </td>
                            <td style="font-weight: bold;">
                                <cc1:ucDropdownList runat="server" ID="ddlDiscrepancyType" Width="150px" />
                            </td>
                            <td style="font-weight: bold;">
                                <cc1:ucLabel ID="lblDiscrepancyNo" runat="server" Text="Discrepancy #" Width="100%"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold;">
                                &nbsp;<cc1:ucLabel ID="UcLabel5" runat="server">:</cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold;">
                                <cc1:ucTextbox ID="txtDiscrepancyNo" runat="server" Width="145px"></cc1:ucTextbox>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold;">
                                &nbsp;&nbsp;<cc1:ucLabel ID="lblPurchaseOrderNo" runat="server" Text="Purchase Order #"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold;">
                                <cc1:ucLabel ID="UcLabel7" runat="server">:</cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold;">
                                <cc1:ucTextbox ID="txtPurchaseOrder" runat="server" Width="145px"></cc1:ucTextbox>
                            </td>
                            <td style="font-weight: bold; text-align: left;">
                                <cc1:ucLabel ID="lblPurchaseOrderDate" runat="server" Text="Purchase Order Date"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold;">
                                <cc1:ucLabel ID="UcLabel1" runat="server">:</cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold;">
                                <asp:TextBox ID="txtPurchaseOrderDate" runat="server" ReadOnly="true" ClientIDMode="Static" class="date"  Width="79px" />
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold;">
                                &nbsp;&nbsp;<cc1:ucLabel ID="lblDiscrepancyDateRange" runat="server" Text="Discrepancy Date Range"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold;">
                                <cc1:ucLabel ID="UcLabel8" runat="server">:</cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold;">
                                <asp:TextBox ID="txtDiscrepancyDateRangeFrom" runat="server" ClientIDMode="Static"
                                    class="date" Width="79px" />
                            </td>
                            <td style="font-weight: bold;" align="Left">
                                <cc1:ucLabel ID="lblTo" runat="server" Text="To"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold;">
                                <cc1:ucLabel ID="UcLabel3" runat="server">:</cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold;">
                                <asp:TextBox ID="txtDiscrepancyDateRangeTo" runat="server" ClientIDMode="Static"
                                    class="date" Width="79px"/>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold;">
                                &nbsp;&nbsp;<cc1:ucLabel ID="lblDeliveryNote" runat="server" Text="Delivery Note"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold;">
                                <cc1:ucLabel ID="UcLabel12" runat="server">:</cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold;" colspan="4">
                                <cc1:ucTextbox ID="txtDeliveryNote" runat="server" Width="145px"></cc1:ucTextbox>
                            </td>
                            <%--<td style="font-weight: bold;">
                                <cc1:ucLabel ID="lblStockPlanner" runat="server" Text="Stock Planner"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold;">
                                <cc1:ucLabel ID="Label3" runat="server">:</cc1:ucLabel>
                            </td>
                            <td>
                                <cc1:ucDropdownList runat="server" ID="ddlStockPlanner" Width="150px" />
                            </td>--%>
                        </tr>
                        <tr>
                            <td style="font-weight: bold; width: 23%">
                                &nbsp;&nbsp;<cc1:ucLabel ID="lblSite" runat="server" Text="Site"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold; width: 1%">
                                <cc1:ucLabel ID="UcLabel2" runat="server">:</cc1:ucLabel>
                            </td>
                            <td width="30%">
                                <cc1:ucListBox ID="UclstSiteList" runat="server" Height="150px" Width="150px">
                                </cc1:ucListBox>
                            </td>
                            <td valign="middle" align="center" width="20%" colspan="1">
                                <div>
                                    <input type="button" id="btnMoveRight" value=">" class="button" style="width: 35px"
                                        onclick="Javascript:MoveItem('<%= UclstSiteList.ClientID %>', '<%= UclstSiteSelected.ClientID %>','<%= hiddenSelectedIDs.ClientID %>','<%= hiddenSelectedName.ClientID %>');" />
                                </div>
                                &nbsp;
                                <div>
                                    <input type="button" id="btnMoveLeft" value="<" class="button" style="width: 35px"
                                        onclick="Javascript:MoveItem('<%= UclstSiteSelected.ClientID %>', '<%= UclstSiteList.ClientID %>','<%= hiddenSelectedIDs.ClientID %>','<%= hiddenSelectedName.ClientID %>');" />
                                </div>
                            </td>
                            <td width="1%">
                                <asp:HiddenField ID="hiddenSelectedIDs" runat="server" Value="" />
                                <asp:HiddenField ID="hiddenSelectedName" runat="server" Value="" />
                            </td>
                            <td width="25%">
                                <cc1:ucListBox ID="UclstSiteSelected" runat="server" Height="150px" Width="150px"
                                    ViewStateMode="Enabled">
                                </cc1:ucListBox>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold;">
                                &nbsp;&nbsp;<cc1:ucLabel ID="lblDiscrepancyStatus" runat="server" Text="Discrepancy Status"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold;">
                                <cc1:ucLabel ID="UcLabel6" runat="server">:</cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold;">
                                <asp:DropDownList ID="ddlDiscrepancyStatus" runat="server" Width="150px">
                                    <asp:ListItem Text="--Select--" Value="" />
                                    <asp:ListItem Text="Open" Value="O" />
                                    <asp:ListItem Text="Forced Closed" Value="F" />
                                    <asp:ListItem Text="Closed" Value="C" />
                                </asp:DropDownList>
                            </td>
                            <td style="font-weight: bold;">
                                &nbsp;&nbsp;<cc1:ucLabel ID="lblActionPendingWith" runat="server" Text="Action Pending With"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold;">
                                <cc1:ucLabel ID="UcLabel9" runat="server">:</cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold;">
                                <asp:DropDownList ID="ddlActionPendingWithDropDown" runat="server" Width="150px">
                                    <asp:ListItem Text="All" Value="A" />
                                    <asp:ListItem Text="GoodsIn" Value="G" />
                                    <asp:ListItem Text="Stock Planner" Value="S" />
                                    <asp:ListItem Text="Account Payable" Value="P" />
                                    <asp:ListItem Text="Vendor" Value="V" />
                                    <asp:ListItem Text="Mediator" Value="M" />
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold;">
                                &nbsp;&nbsp;<cc1:ucLabel ID="lblQueryStatus" runat="server" Text="Query Status"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold;">
                                <cc1:ucLabel ID="UcLabel10" runat="server">:</cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold;">
                                <asp:DropDownList ID="ddlQueryStatus" runat="server" Width="150px">
                                    <asp:ListItem Text="--Select--" Value="" />
                                    <asp:ListItem Text="Open" Value="O" />
                                    <asp:ListItem Text="Accepted" Value="A" />
                                    <asp:ListItem Text="Rejected" Value="R" />
                                    <asp:ListItem Text="Closed" Value="C" />
                                    <asp:ListItem Text="No Query" Value="NQ" />
                                </asp:DropDownList>
                            </td>
                            <td style="font-weight: bold;">
                                &nbsp;&nbsp;<cc1:ucLabel ID="lblEscalation" runat="server" Text="Escalation"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold;">
                                <cc1:ucLabel ID="UcLabel4" runat="server">:</cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold;">
                                <asp:DropDownList ID="ddlEscalation" runat="server" Width="150px">
                                    <asp:ListItem Text="--Select--" Value="" />
                                    <asp:ListItem Text="First Escalation" Value="EscalationType2" />
                                    <%--<asp:ListItem Text="Second Escalation" Value="EscalationType3" />
                                    <asp:ListItem Text="Third Escalation" Value="EscalationType4" />
                                    <asp:ListItem Text="Fourth Escalation" Value="EscalationType5" />  --%>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold;">
                                &nbsp;&nbsp;<cc1:ucLabel ID="lblSearchBySku" runat="server" Text="Search By SKU"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold;">
                                <cc1:ucLabel ID="UcLabel13" runat="server">:</cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold;">
                                <cc1:ucTextbox ID="txtSearchBySkuText" runat="server" Width="145px"></cc1:ucTextbox>
                            </td>
                            <td style="font-weight: bold;">
                                &nbsp;&nbsp;<cc1:ucLabel ID="lblSearchByCatCode" runat="server" Text="Search By CAT Code"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold;">
                                <cc1:ucLabel ID="UcLabel15" runat="server">:</cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold;">
                                <cc1:ucTextbox ID="txtSearchByCatCodeText" runat="server" Width="145px"></cc1:ucTextbox>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold;" colspan="6">
                                <cc1:MultiSelectStockPlanner runat="server" ID="msStockPlanner" />
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold;" colspan="6">
                                <cc2:MultiSelectVendor runat="server" ID="msVendor" />
                            </td>
                        </tr>
                    </table>
                </cc1:ucView>
                <cc1:ucView ID="vwSearchListing" runat="server">
                    <cc1:ucPanel ID="pnlGrid" runat="server" ScrollBars="Both" CssClass="fieldset-form">
                        <%--<table width="112%" class="grid" id="tblGrid" runat="server">
                            <tr>
                                <th align="left" style="padding-left: 340px;">
                                    <asp:Label ID="lblOpenWith" Text="Open With" runat="server"></asp:Label>
                                </th>
                            </tr>
                        </table>--%>
                        <cc1:ucGridView ID="gvDisLog" Width="100%" runat="server" CssClass="grid"  AutoGenerateColumns="true"  OnRowDataBound="gvDisLog_RowDataBound"
                            OnSorting="SortGrid" Style="overflow: auto;">
                            <RowStyle HorizontalAlign="Center"></RowStyle>
                        </cc1:ucGridView>
                    </cc1:ucPanel>
                </cc1:ucView>
            </cc1:ucMultiView>
        </div>
    </div>
    <div class="button-row">
        <%--<cc1:ucButton ID="btnSave" runat="server" Text="Save"
    class="button" />--%>
        <cc1:ucButton ID="btnSearch" runat="server" Text="Search" CssClass="button" OnClick="btnSearch_Click" />
        <cc1:ucButton ID="btnReturntoSearchCriteria" runat="server" Text="Return to Search Criteria"
            class="button" OnClick="btnReturntoSearchCriteria_Click" />
    </div>
</asp:Content>

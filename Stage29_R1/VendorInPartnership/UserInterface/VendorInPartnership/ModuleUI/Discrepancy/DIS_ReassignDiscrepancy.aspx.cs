﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.AdminFunctions;
using BusinessEntities.ModuleBE.Discrepancy.LogDiscrepancy;
using BusinessLogicLayer.ModuleBAL.AdminFunctions;
using BusinessLogicLayer.ModuleBAL.Discrepancy.LogDiscrepancy;
using Utilities;
using WebUtilities;
using BusinessEntities.ModuleBE.Security;
using BusinessLogicLayer.ModuleBAL.Security;
using System.Data;
using BaseControlLibrary;
public partial class DIS_ReassignDiscrepancy :CommonPage 
{
    int SelectedStatus = 0;
    protected string SelectOneDiscrepancy = WebCommon.getGlobalResourceValue("PlzSelectAtleastOneDiscrepancy");
   // protected string AssignedMessage = WebCommon.getGlobalResourceValue("NewStockPlannerAssigned");
    protected string StockPlannerAssignedMessage = WebCommon.getGlobalResourceValue("NewStockPlannerAssigned");
    protected string AccountPayableAssignedMessage = WebCommon.getGlobalResourceValue("NewAccountPayableAssigned");

    protected string AreuSureMessage = WebCommon.getGlobalResourceValue("AreYouSureReassignDiscrepancies");
    protected void Page_Init(object sender, EventArgs e) 
    {
        ddlCountry.CurrentPage = this;
        ddlCountry.IsAllRequired = true;
    }
    protected void Page_Load(object sender, EventArgs e)
    {
       
            msStockPlanner.SetSPOnPostBack();
            msAccountPayable.SetAPOnPostBack();
      
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        pnlSearchScreen.Visible = false;
        pnlPotentialOutput.Visible = true;
        lblmsg.Text = "";
        BindStockPlanner(Convert.ToInt32(ddlCountry.innerControlddlCountry.SelectedValue));
        BindDiscrepancy();
    }
    protected void BindDiscrepancy()
    {
        hdnFldSelectedValues.Value = "";
        DiscrepancyBE dicrepancyBE = new DiscrepancyBE();
        DiscrepancyBAL discrepancyBL = new DiscrepancyBAL();
        List<DiscrepancyBE> lstDisLog = null;
        dicrepancyBE.Action = "SerachDiscrepancy";

        dicrepancyBE.SCSelectedSPIDs = msStockPlanner.SelectedStockPlannerIDs;
        dicrepancyBE.SCSelectedVendorIDs = msVendor.SelectedVendorIDs;
        if (rdoAccountsPayable.Checked)
        {
            dicrepancyBE.IsAPFlag = true;
            dicrepancyBE.SCSelectedAPIDs = msAccountPayable.SelectedStockPlannerIDs;
            dicrepancyBE.SCSelectedSPIDs = null;
        }
        else 
        {
            dicrepancyBE.IsAPFlag = false;
            dicrepancyBE.SCSelectedSPIDs = msStockPlanner.SelectedStockPlannerIDs;
            dicrepancyBE.SCSelectedAPIDs = null;
        }
       
        dicrepancyBE.APCountryID = Convert.ToInt32(ddlCountry.innerControlddlCountry.SelectedValue);
        dicrepancyBE.VDRNo = txtDiscrepancy.Text.Trim();
        lstDisLog = discrepancyBL.GetDiscrepancyForReassignBAL(dicrepancyBE);
        gdvDiscrepancy.PageIndex = 0;
        gdvDiscrepancy.DataSource = lstDisLog;
        gdvDiscrepancy.DataBind();
        ViewState["DiscrepancyDetails"] = lstDisLog;
        if (lstDisLog.Count == 0)
        {
            trAssign.Visible = false;
            UcButton1.Visible = false;
        }
        else
        {
            trAssign.Visible = true;
            UcButton1.Visible = true;
        }
    }
    protected void gdvDiscrepancy_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridViewRow header = gdvDiscrepancy.HeaderRow;
        ucCheckbox ch = (ucCheckbox)header.FindControl("chkSelectAllText");
        if (ch.Checked)
            SelectedStatus = 1;
        else
            SelectedStatus = 0;
        GetCheckedRecords(gdvDiscrepancy);
        gdvDiscrepancy.PageIndex = e.NewPageIndex;
        gdvDiscrepancy.DataSource = (List<DiscrepancyBE>)ViewState["DiscrepancyDetails"];
        gdvDiscrepancy.DataBind();
        TicCheckedRecords(gdvDiscrepancy);
    }
    public override BusinessEntities.BaseBe[] SubSort(GridViewSortEventArgs e)
    {
        return Utilities.GenericListHelper<DiscrepancyBE>.SortList((List<DiscrepancyBE>)ViewState["DiscrepancyDetails"], e.SortExpression, e.SortDirection).ToArray();
    }
    protected void BindStockPlanner(int CountryId)
    {
        SCT_UserBE oSCT_UserBE = new SCT_UserBE();
        SCT_UserBAL oMAS_UserBAL = new SCT_UserBAL();
        if (rdoAccountsPayable.Checked == false)
        {
            oSCT_UserBE.Action = "GetStockPlannersByCountryIdnew";
            oSCT_UserBE.UserID = Convert.ToInt32(Session["UserID"]);
            oSCT_UserBE.CountryId = CountryId;
            DataTable dtStockPlanner = oMAS_UserBAL.GetStockPlannerByCountryIdBAL(oSCT_UserBE);
            ddlStockPlanner.DataSource = dtStockPlanner;
            ddlStockPlanner.DataTextField = "StockPlannerWithCountry";
            ddlStockPlanner.DataValueField = "StockPlannerID";
            ddlStockPlanner.DataBind();
        }
        else
        {
           
            oSCT_UserBE.Action = "ShowAll";
            oSCT_UserBE.RoleName = "OD - Accounts Payable";

            List<SCT_UserBE> lstUsers = oMAS_UserBAL.GetUserDetailsBAL(oSCT_UserBE);
            if (lstUsers != null && lstUsers.Count > 0)
            {
                ddlStockPlanner.DataSource = lstUsers;
                ddlStockPlanner.DataTextField = "APUserName";
                ddlStockPlanner.DataValueField = "UserID";
                ddlStockPlanner.DataBind();

            }
            //oSCT_UserBE.Action = "GetAPByCountryIdnew";
            //oSCT_UserBE.VendorIDs = msVendor.SelectedVendorIDs;
            //oSCT_UserBE.CountryId = CountryId;
            //DataTable dtStockPlanner = oMAS_UserBAL.GetStockPlannerByCountryIdBAL(oSCT_UserBE);
            //ddlStockPlanner.DataSource = dtStockPlanner;
            //ddlStockPlanner.DataTextField = "AccountPaybleName";
            //ddlStockPlanner.DataValueField = "APID";
            //ddlStockPlanner.DataBind();
        }
    }



    protected void chkSelectAllText_CheckedChanged(object sender, EventArgs e)
    {
        GridViewRow header = gdvDiscrepancy.HeaderRow;
        ucCheckbox ch = (ucCheckbox)header.FindControl("chkSelectAllText");
        if (ch.Checked)
        {
            SelectedStatus = 1;
            List<DiscrepancyBE> lstDetails = (List<DiscrepancyBE>)ViewState["DiscrepancyDetails"];
            foreach (var item in lstDetails)
            {
                string DiscrepancyLogid = item.DiscrepancyLogID.ToString();
                if (!hdnFldSelectedValues.Value.ToString().Contains(DiscrepancyLogid))
                {
                    hdnFldSelectedValues.Value += "," + DiscrepancyLogid;
                }
            }

        }
        else
        {
            SelectedStatus = 0;
            hdnFldSelectedValues.Value = "";
        }
        BindDiscrepancy();
    }
    protected void gdvDiscrepancy_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Literal ltstockPlanner = (Literal)e.Row.FindControl("ltstockPlanner");
            Literal ltAccountPaybleName = (Literal)e.Row.FindControl("ltAccountPaybleName");
            if (rdoAccountsPayable.Checked)
            {
                gdvDiscrepancy.Columns[4].HeaderText = "APClerk";
                gdvDiscrepancy.Columns[4].SortExpression = "AccountPaybleName";
                ltstockPlanner.Visible = false;
                ltAccountPaybleName.Visible = true;
            }
            else
            {
                gdvDiscrepancy.Columns[4].HeaderText = "StockPlanner";
                gdvDiscrepancy.Columns[4].SortExpression = "StockPlannerName";
                ltstockPlanner.Visible = true;
                ltAccountPaybleName.Visible = false;
            }
            if(SelectedStatus == 1)
            {
                CheckBox ucCheck = (CheckBox)e.Row.FindControl("chkSelect");
                ucCheck.Checked = true;
                GridViewRow header = gdvDiscrepancy.HeaderRow;
                ucCheckbox ch = (ucCheckbox)header.FindControl("chkSelectAllText");
                ch.Checked = true;
                HiddenField hdnValue = (HiddenField)e.Row.FindControl("hdndiscrepancyLogid");
                if (!hdnFldSelectedValues.Value.ToString().Contains(hdnValue.Value))
                {
                    hdnFldSelectedValues.Value += "," + hdnValue.Value;
                }
            }
        }
    }
    private void TicCheckedRecords(GridView grd)
    {
        string str = string.Empty;
        if (!string.IsNullOrEmpty(hdnFldSelectedValues.Value.ToString()))
        {
            var ids = hdnFldSelectedValues.Value.ToString().TrimEnd(',').Split(',');
            foreach (GridViewRow gvrow in grd.Rows)
            {
                CheckBox chk = (CheckBox)gvrow.FindControl("chkSelect");
                HiddenField hdnRowNum = (HiddenField)gvrow.FindControl("hdndiscrepancyLogid");
                if (chk != null)
                {
                    if (ids.Contains(hdnRowNum.Value))
                    {
                        chk.Checked = true;
                    }
                }
            }
        }
    }

    private void GetCheckedRecords(GridView grd)
    {
        GridViewRow header = grd.HeaderRow;
        ucCheckbox ch = (ucCheckbox)header.FindControl("chkSelectAllText");
        if (ch.Checked && string.IsNullOrEmpty(hdnFldSelectedValues.Value.ToString()))
        {
            var lstRecords = new List<DiscrepancyBE>();
            if (ViewState["SearchedVendor"] != null)
            {
                lstRecords = (List<DiscrepancyBE>)ViewState["DiscrepancyDetails"];
            }
           
            foreach (var item in lstRecords)
            {
                hdnFldSelectedValues.Value = hdnFldSelectedValues.Value + item.DiscrepancyLogID.ToString() + ",";
            }
        }
        else
        {
            foreach (GridViewRow gvrow in grd.Rows)
            {
                CheckBox chk = (CheckBox)gvrow.FindControl("chkSelect");
                if (chk != null & chk.Checked)
                {
                    HiddenField hdnRowNum = (HiddenField)gvrow.FindControl("hdndiscrepancyLogid");
                    if (!hdnFldSelectedValues.Value.ToString().Contains(hdnRowNum.Value.ToString()))
                        hdnFldSelectedValues.Value = hdnFldSelectedValues.Value + hdnRowNum.Value + ",";
                }
            }
        }
    }
    protected void UcButton1_Click(object sender, EventArgs e)
    {
            int ? result=0;
            lblmsg.Text = "";
            DiscrepancyBAL discrepancyBL = new DiscrepancyBAL();
            string strValue = hdnFldSelectedValues.Value;
            string[] ArrDiscrepancyLogId = strValue.Split(',');
            foreach (var item in ArrDiscrepancyLogId)
            {
                if (item != "" && item != null)
                {
                    DiscrepancyBE discrepancyBE = new DiscrepancyBE();
                    discrepancyBE.DiscrepancyLogID = Convert.ToInt32(item);
                    discrepancyBE.UserID = Convert.ToInt32(Session["UserID"]);
                    discrepancyBE.Action = "SaveReassignDiscrepancy";
                    if (rdoAccountsPayable.Checked == false)
                    {
                        discrepancyBE.StockPlannerID = Convert.ToInt32(ddlStockPlanner.SelectedValue);
                        discrepancyBE.IsAPFlag = false;
                    }
                    else
                    {
                        discrepancyBE.AccountPayableID = Convert.ToInt32(ddlStockPlanner.SelectedValue);
                        discrepancyBE.IsAPFlag = true;
                    }
                    result = discrepancyBL.saveReassignDiscrepancyBAL(discrepancyBE);
                }
            }
            if (result > 0)
            {
                BindDiscrepancy();
                if (rdoAccountsPayable.Checked == true)
                {
                    lblmsg.Text = AccountPayableAssignedMessage;
                }
                else 
                {
                    lblmsg.Text = StockPlannerAssignedMessage;
                }

               lblmsg.ForeColor = System.Drawing.Color.Green;
                //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('New stock planner has been assigned to discrepancies')", true);
            }
       
    }
    protected void btnBack_1_Click(object sender, EventArgs e)
    {
        pnlSearchScreen.Visible = true;
        pnlPotentialOutput.Visible = false;
        hdnFldSelectedValues.Value = "";
        lblmsg.Text = "";
    }

    protected void rdoInventory_CheckedChanged(object sender, EventArgs e)
    {
        if (rdoInventory.Checked == true)
        {
            trStockPlanner.Visible = true;
            trAccountPayable.Visible = false;
        }
        else 
        {
            trStockPlanner.Visible = false;
            trAccountPayable.Visible = true;
        }
    }
    protected void rdoAccountsPayable_CheckedChanged(object sender, EventArgs e)
    {
        if (rdoAccountsPayable.Checked)
        {
            trStockPlanner.Visible = false;
            trAccountPayable.Visible = true;
            msAccountPayable.BindApDetails();
        }
        else
        {
            trStockPlanner.Visible = true;
            trAccountPayable.Visible = false;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.Discrepancy.LogDiscrepancy;
using BusinessLogicLayer.ModuleBAL.Discrepancy.LogDiscrepancy;
using BusinessLogicLayer.ModuleBAL.AdminFunctions;
using BusinessEntities.ModuleBE.AdminFunctions;
using Utilities;

public partial class ModuleUI_Discrepancy_DIS_InvoiceQuerySearchResult : CommonPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        ucExportToExcel1.CurrentPage = this;
        ucExportToExcel1.GridViewControl = gvDisLog;
        ucExportToExcel1.FileName = "InvoiceQuerySearchDiscrepancies";
        if (!IsPostBack)
        {
            if (ViewState["lstSites"] != null)
            {               
                ucExportToExcel1.Visible = true;
                gvDisLog.DataSource = ViewState["lstSites"];
                gvDisLog.DataBind();
                tblSearch.Visible = false;
                btnSearch.Visible = false;
                btnReturntoSearchCriteria.Visible = true;              
                hiddenSelectedIDs.Value = string.Empty;
            }
            BindSiteList();
        }
    }

    private void BindGrid()
    {
        DiscrepancyBAL oNewDiscrepancyBAL = new DiscrepancyBAL();
        DiscrepancyBE oNewDiscrepancyBE = new DiscrepancyBE();
        List<DiscrepancyBE> lstDisLog = null;
        //Search Creteria
        oNewDiscrepancyBE.Action = "InvoiceQueryDiscrepancyReport";
        oNewDiscrepancyBE.SCDiscrepancyNo = txtDiscrepancyNo.Text != string.Empty ? txtDiscrepancyNo.Text.Trim() : null;
        oNewDiscrepancyBE.SCPurchaseOrderNo = txtPurchaseOrder.Text != string.Empty ? txtPurchaseOrder.Text.Trim() : null;
        oNewDiscrepancyBE.SCDeliveryNoteNumber = txtDeliveryNote.Text != string.Empty ? txtDeliveryNote.Text.Trim() : null;
        oNewDiscrepancyBE.SCDiscrepancyDateFrom = string.IsNullOrEmpty(txtDiscrepancyDateRangeFrom.Text) ? (DateTime?)null : Common.GetMM_DD_YYYY(txtDiscrepancyDateRangeFrom.Text);
        oNewDiscrepancyBE.SCDiscrepancyDateTo = string.IsNullOrEmpty(txtDiscrepancyDateRangeTo.Text) ? (DateTime?)null : Common.GetMM_DD_YYYY(txtDiscrepancyDateRangeTo.Text);
        oNewDiscrepancyBE.UserID = Session["UserID"] != null ? Convert.ToInt32(Session["UserID"]) : (int?)null;
        oNewDiscrepancyBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
        oNewDiscrepancyBE.User.RoleTypeFlag = ddlActionPendingWithDropDown.SelectedItem.Value.Trim();

        //Site
        if (!string.IsNullOrEmpty(hiddenSelectedIDs.Value))
        {
            oNewDiscrepancyBE.SCSelectedSiteIDs = hiddenSelectedIDs.Value.Trim(',');
        }
        else
            oNewDiscrepancyBE.SCSelectedSiteIDs = null;

        oNewDiscrepancyBE.SCSelectedVendorIDs = msVendor.SelectedVendorIDs;
        oNewDiscrepancyBE.APIds = msAP.SelectedAPIDs;
        oNewDiscrepancyBE.OpenWith = string.IsNullOrEmpty(ddlActionPendingWithDropDown.SelectedValue) ? null : ddlActionPendingWithDropDown.SelectedItem.Text == "All" ? null : ddlActionPendingWithDropDown.SelectedItem.Text;
        oNewDiscrepancyBE.DiscrepancyStatus = string.IsNullOrEmpty(ddlDiscrepancyStatus.SelectedValue) ? null : ddlDiscrepancyStatus.SelectedValue;
        oNewDiscrepancyBE.SearchByODSku = txtSearchBySkuText.Text;
        oNewDiscrepancyBE.SearchByCatCode = txtSearchByCatCodeText.Text;


        lstDisLog = oNewDiscrepancyBAL.GetInvoiceQueryDiscrepancyLogDetailsBAL(oNewDiscrepancyBE);

        if (lstDisLog.Count > 0)
            ucExportToExcel1.Visible = true;
        else
            ucExportToExcel1.Visible = false;

        //ViewState["lstSites"] = lstDisLog;
        gvDisLog.DataSource = lstDisLog;
        gvDisLog.DataBind();
        tblSearch.Visible=false;
        btnSearch.Visible=false;
        btnReturntoSearchCriteria.Visible=true;      
        ViewState["lstSites"] = lstDisLog;
        Session["SearchCriteria"] = oNewDiscrepancyBE;
        hiddenSelectedIDs.Value = string.Empty;
    }
    private void BindSiteList()
    {
        if (GetQueryStringValue("SiteId") == null)
        {
            MAS_SiteBE oMAS_SiteBE = new MAS_SiteBE();
            MAS_SiteBAL oMAS_SiteBAL = new MAS_SiteBAL();
            oMAS_SiteBE.Action = "ShowAll";

            oMAS_SiteBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
            oMAS_SiteBE.User.UserID = Convert.ToInt32(Session["UserID"]);
            oMAS_SiteBE.SiteCountryID = 0;
            List<MAS_SiteBE> lstSite = new List<MAS_SiteBE>();
            lstSite = oMAS_SiteBAL.GetSiteMisSettingBAL(oMAS_SiteBE);
            if (lstSite.Count > 0 && lstSite != null)
            {
                FillControls.FillListBox(ref UclstSiteList, lstSite, "SiteDescription", "SiteID");
            }
            oMAS_SiteBAL = null;
        }

    }
    protected void gvDisLog_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType != DataControlRowType.Header
            && e.Row.RowType != DataControlRowType.Footer)
        {
            //DateTime dDiscrepancyLogDate = Convert.ToDateTime(DataBinder.Eval(e.Row.DataItem, "DiscrepancyLogDate"));
            //e.Row.Cells[9].Text = dDiscrepancyLogDate.ToString("dd/MM/yyyy");
            //e.Row.Cells[9].HorizontalAlign = HorizontalAlign.Left;
            HyperLink hypLinkToDisDetails = (HyperLink)e.Row.FindControl("hypLinkToDisDetails");
            HiddenField hdnDisType = (HiddenField)e.Row.FindControl("hdnDisType");
            HiddenField hdnDisLogID = (HiddenField)e.Row.FindControl("hdnDisLogID");
            HiddenField hdnUserID = (HiddenField)e.Row.FindControl("hdnUserID");
            HiddenField hdnWorkFlowID = (HiddenField)e.Row.FindControl("hdnWorkFlowID");
            string status = e.Row.Cells[1].Text.ToString();
            string VDRNo = hypLinkToDisDetails.Text;

            string TodaysDis = Convert.ToString(ViewState["TodayDiscrepancy"]);
            if (TodaysDis == null || TodaysDis == "")
                TodaysDis = "SearchDiscrepancy";
            else
                TodaysDis = Convert.ToString(ViewState["TodayDiscrepancy"]);

            switch (Convert.ToInt32(hdnDisType.Value.Trim()))
            {
                case 1:
                    hypLinkToDisDetails.NavigateUrl = EncryptQuery("LogDiscrepancy/DISLog_Overs.aspx?status=" + status + "&disLogID=" + hdnDisLogID.Value + "&VDRNo=" + VDRNo + "&UserID=" + hdnUserID.Value + "&RoleTypeFlag=" + ddlActionPendingWithDropDown.SelectedItem.Value.Trim() + "&PreviousPage=InvoiceSearchResult&WorkFlowID=" + hdnWorkFlowID.Value + "&SiteID=" + GetQueryStringValue("SiteId") + "&DisDate=" + GetQueryStringValue("DisDate") + "&TodayDis=TodayDiscrepancy");
                    break;
                case 2:
                    hypLinkToDisDetails.NavigateUrl = EncryptQuery("LogDiscrepancy/DISLog_Shortage.aspx?status=" + status + "&disLogID=" + hdnDisLogID.Value + "&VDRNo=" + VDRNo + "&UserID=" + hdnUserID.Value + "&RoleTypeFlag=" + ddlActionPendingWithDropDown.SelectedItem.Value.Trim() + "&PreviousPage=InvoiceSearchResult&WorkFlowID=" + hdnWorkFlowID.Value + "&SiteID=" + GetQueryStringValue("SiteId") + "&DisDate=" + GetQueryStringValue("DisDate"));
                    break;
                case 3:
                    hypLinkToDisDetails.NavigateUrl = EncryptQuery("LogDiscrepancy/DISLog_GoodsReceivedDamaged.aspx?status=" + status + "&disLogID=" + hdnDisLogID.Value + "&VDRNo=" + VDRNo + "&UserID=" + hdnUserID.Value + "&RoleTypeFlag=" + ddlActionPendingWithDropDown.SelectedItem.Value.Trim() + "&PreviousPage=InvoiceSearchResult&WorkFlowID=" + hdnWorkFlowID.Value + "&SiteID=" + GetQueryStringValue("SiteId") + "&DisDate=" + GetQueryStringValue("DisDate"));
                    break;
                case 4:
                    hypLinkToDisDetails.NavigateUrl = EncryptQuery("LogDiscrepancy/DISLog_NoPurchaseOrder.aspx?status=" + status + "&disLogID=" + hdnDisLogID.Value + "&VDRNo=" + VDRNo + "&UserID=" + hdnUserID.Value + "&RoleTypeFlag=" + ddlActionPendingWithDropDown.SelectedItem.Value.Trim() + "&PreviousPage=InvoiceSearchResult&WorkFlowID=" + hdnWorkFlowID.Value + "&SiteID=" + GetQueryStringValue("SiteId") + "&DisDate=" + GetQueryStringValue("DisDate"));
                    break;
                case 5:
                    hypLinkToDisDetails.NavigateUrl = EncryptQuery("LogDiscrepancy/DISLog_NoPaperwork.aspx?status=" + status + "&disLogID=" + hdnDisLogID.Value + "&VDRNo=" + VDRNo + "&UserID=" + hdnUserID.Value + "&RoleTypeFlag=" + ddlActionPendingWithDropDown.SelectedItem.Value.Trim() + "&PreviousPage=InvoiceSearchResult&WorkFlowID=" + hdnWorkFlowID.Value + "&SiteID=" + GetQueryStringValue("SiteId") + "&DisDate=" + GetQueryStringValue("DisDate"));
                    break;
                case 6:
                    hypLinkToDisDetails.NavigateUrl = EncryptQuery("LogDiscrepancy/DISLog_IncorrectProduct.aspx?status=" + status + "&disLogID=" + hdnDisLogID.Value + "&VDRNo=" + VDRNo + "&UserID=" + hdnUserID.Value + "&RoleTypeFlag=" + ddlActionPendingWithDropDown.SelectedItem.Value.Trim() + "&PreviousPage=InvoiceSearchResult&WorkFlowID=" + hdnWorkFlowID.Value + "&SiteID=" + GetQueryStringValue("SiteId") + "&DisDate=" + GetQueryStringValue("DisDate"));
                    break;
                case 7:
                    hypLinkToDisDetails.NavigateUrl = EncryptQuery("LogDiscrepancy/DISLog_PresentationIssue.aspx?status=" + status + "&disLogID=" + hdnDisLogID.Value + "&VDRNo=" + VDRNo + "&UserID=" + hdnUserID.Value + "&RoleTypeFlag=" + ddlActionPendingWithDropDown.SelectedItem.Value.Trim() + "&PreviousPage=InvoiceSearchResult&WorkFlowID=" + hdnWorkFlowID.Value + "&SiteID=" + GetQueryStringValue("SiteId") + "&DisDate=" + GetQueryStringValue("DisDate"));
                    break;
                case 8:
                    hypLinkToDisDetails.NavigateUrl = EncryptQuery("LogDiscrepancy/DISLog_IncorrectAddress.aspx?status=" + status + "&disLogID=" + hdnDisLogID.Value + "&VDRNo=" + VDRNo + "&UserID=" + hdnUserID.Value + "&RoleTypeFlag=" + ddlActionPendingWithDropDown.SelectedItem.Value.Trim() + "&PreviousPage=InvoiceSearchResult&WorkFlowID=" + hdnWorkFlowID.Value + "&SiteID=" + GetQueryStringValue("SiteId") + "&DisDate=" + GetQueryStringValue("DisDate"));
                    break;
                case 9:
                    hypLinkToDisDetails.NavigateUrl = EncryptQuery("LogDiscrepancy/DISLog_PaperworkAmended.aspx?status=" + status + "&disLogID=" + hdnDisLogID.Value + "&VDRNo=" + VDRNo + "&UserID=" + hdnUserID.Value + "&RoleTypeFlag=" + ddlActionPendingWithDropDown.SelectedItem.Value.Trim() + "&PreviousPage=InvoiceSearchResult&WorkFlowID=" + hdnWorkFlowID.Value + "&SiteID=" + GetQueryStringValue("SiteId") + "&DisDate=" + GetQueryStringValue("DisDate"));
                    break;
                case 10:
                    hypLinkToDisDetails.NavigateUrl = EncryptQuery("LogDiscrepancy/DISLog_WrongPackSize.aspx?status=" + status + "&disLogID=" + hdnDisLogID.Value + "&VDRNo=" + VDRNo + "&UserID=" + hdnUserID.Value + "&RoleTypeFlag=" + ddlActionPendingWithDropDown.SelectedItem.Value.Trim() + "&PreviousPage=InvoiceSearchResult&WorkFlowID=" + hdnWorkFlowID.Value + "&SiteID=" + GetQueryStringValue("SiteId") + "&DisDate=" + GetQueryStringValue("DisDate"));
                    break;
                case 11:
                    hypLinkToDisDetails.NavigateUrl = EncryptQuery("LogDiscrepancy/DISLog_FailPalletSpecification.aspx?status=" + status + "&disLogID=" + hdnDisLogID.Value + "&VDRNo=" + VDRNo + "&UserID=" + hdnUserID.Value + "&RoleTypeFlag=" + ddlActionPendingWithDropDown.SelectedItem.Value.Trim() + "&PreviousPage=InvoiceSearchResult&WorkFlowID=" + hdnWorkFlowID.Value + "&SiteID=" + GetQueryStringValue("SiteId") + "&DisDate=" + GetQueryStringValue("DisDate"));
                    break;
                case 12:
                    hypLinkToDisDetails.NavigateUrl = EncryptQuery("LogDiscrepancy/DISLog_QualityIssue.aspx?status=" + status + "&disLogID=" + hdnDisLogID.Value + "&VDRNo=" + VDRNo + "&UserID=" + hdnUserID.Value + "&RoleTypeFlag=" + ddlActionPendingWithDropDown.SelectedItem.Value.Trim() + "&PreviousPage=InvoiceSearchResult&WorkFlowID=" + hdnWorkFlowID.Value + "&SiteID=" + GetQueryStringValue("SiteId") + "&DisDate=" + GetQueryStringValue("DisDate"));
                    break;
                case 13:
                    hypLinkToDisDetails.NavigateUrl = EncryptQuery("LogDiscrepancy/DISLog_PrematureInvoiceReceipt.aspx?status=" + status + "&disLogID=" + hdnDisLogID.Value + "&VDRNo=" + VDRNo + "&UserID=" + hdnUserID.Value + "&RoleTypeFlag=" + ddlActionPendingWithDropDown.SelectedItem.Value.Trim() + "&PreviousPage=InvoiceSearchResult&WorkFlowID=" + hdnWorkFlowID.Value + "&SiteID=" + GetQueryStringValue("SiteId") + "&DisDate=" + GetQueryStringValue("DisDate"));
                    break;
                case 14:
                    hypLinkToDisDetails.NavigateUrl = EncryptQuery("LogDiscrepancy/DISLog_GenericDescrepancy.aspx?status=" + status + "&disLogID=" + hdnDisLogID.Value + "&VDRNo=" + VDRNo + "&UserID=" + hdnUserID.Value + "&RoleTypeFlag=" + ddlActionPendingWithDropDown.SelectedItem.Value.Trim() + "&PreviousPage=InvoiceSearchResult&WorkFlowID=" + hdnWorkFlowID.Value + "&SiteID=" + GetQueryStringValue("SiteId") + "&DisDate=" + GetQueryStringValue("DisDate"));
                    break;
                case 15:
                    hypLinkToDisDetails.NavigateUrl = EncryptQuery("LogDiscrepancy/DISLog_Shuttle.aspx?status=" + status + "&disLogID=" + hdnDisLogID.Value + "&VDRNo=" + VDRNo + "&UserID=" + hdnUserID.Value + "&RoleTypeFlag=" + ddlActionPendingWithDropDown.SelectedItem.Value.Trim() + "&PreviousPage=InvoiceSearchResult&WorkFlowID=" + hdnWorkFlowID.Value + "&SiteID=" + GetQueryStringValue("SiteId") + "&DisDate=" + GetQueryStringValue("DisDate"));
                    break;
                case 16:
                    hypLinkToDisDetails.NavigateUrl = EncryptQuery("LogDiscrepancy/DISLog_Reservation.aspx?status=" + status + "&disLogID=" + hdnDisLogID.Value + "&VDRNo=" + VDRNo + "&UserID=" + hdnUserID.Value + "&RoleTypeFlag=" + ddlActionPendingWithDropDown.SelectedItem.Value.Trim() + "&PreviousPage=InvoiceSearchResult&WorkFlowID=" + hdnWorkFlowID.Value + "&SiteID=" + GetQueryStringValue("SiteId") + "&DisDate=" + GetQueryStringValue("DisDate"));
                    break;
                case 17:
                    hypLinkToDisDetails.NavigateUrl = EncryptQuery("LogDiscrepancy/DISLog_InvoiceDiscrepancy.aspx?status=" + status + "&disLogID=" + hdnDisLogID.Value + "&VDRNo=" + VDRNo + "&UserID=" + hdnUserID.Value + "&RoleTypeFlag=" + ddlActionPendingWithDropDown.SelectedItem.Value.Trim() + "&PreviousPage=InvoiceSearchResult&WorkFlowID=" + hdnWorkFlowID.Value + "&SiteID=" + GetQueryStringValue("SiteId") + "&DisDate=" + GetQueryStringValue("DisDate") + "&TodayDis=" + TodaysDis);
                    break;
                case 18:
                    hypLinkToDisDetails.NavigateUrl = EncryptQuery("LogDiscrepancy/DISLog_StockPlannerInvoiceDiscrepancy.aspx?status=" + status + "&disLogID=" + hdnDisLogID.Value + "&VDRNo=" + VDRNo + "&UserID=" + hdnUserID.Value + "&RoleTypeFlag=" + ddlActionPendingWithDropDown.SelectedItem.Value.Trim() + "&PreviousPage=InvoiceSearchResult&WorkFlowID=" + hdnWorkFlowID.Value + "&SiteID=" + GetQueryStringValue("SiteId") + "&DisDate=" + GetQueryStringValue("DisDate") + "&TodayDis=" + TodaysDis);
                    break;
                case 19:
                    hypLinkToDisDetails.NavigateUrl = EncryptQuery("LogDiscrepancy/DISLog_InvoiceQueryDiscrepancy.aspx?status=" + status + "&disLogID=" + hdnDisLogID.Value + "&VDRNo=" + VDRNo + "&UserID=" + hdnUserID.Value + "&RoleTypeFlag=" + ddlActionPendingWithDropDown.SelectedItem.Value.Trim() + "&PreviousPage=InvoiceSearchResult&WorkFlowID=" + hdnWorkFlowID.Value + "&SiteID=" + GetQueryStringValue("SiteId") + "&DisDate=" + GetQueryStringValue("DisDate") + "&TodayDis=" + TodaysDis);
                    break;

            }
            e.Row.Cells[3].HorizontalAlign = HorizontalAlign.Left;
            //if (ddlDiscrepancyType.SelectedValue == "19")
            //{
            //    gvDisLog.Columns[9].Visible = true;                
            //}
            //else
            //{
            //    gvDisLog.Columns[9].Visible = false;               
            //}
        }
    }

    public override BusinessEntities.BaseBe[] SubSort(GridViewSortEventArgs e)
    {
        return Utilities.GenericListHelper<DiscrepancyBE>.SortList((List<DiscrepancyBE>)ViewState["lstSites"], e.SortExpression, e.SortDirection).ToArray();
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        BindGrid();
    }
    protected void btnReturntoSearchCriteria_Click(object sender, EventArgs e)
    {
        tblSearch.Visible = true;
        btnSearch.Visible = true;
        btnReturntoSearchCriteria.Visible = false;
        gvDisLog.DataSource = null;
        gvDisLog.DataBind();
        ucExportToExcel1.Visible = false;
        ViewState["lstSites"] = null;
    }
}
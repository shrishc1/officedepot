﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BaseControlLibrary;
using BusinessEntities.ModuleBE.Discrepancy.LogDiscrepancy;
using BusinessLogicLayer.ModuleBAL.Discrepancy.LogDiscrepancy;
using WebUtilities;

public partial class ModuleUI_Discrepancy_DIS_UnclosedDiscrepancyList : CommonPage
{
    int SelectedStatus = 0;
    protected string SelectOneDiscrepancy = WebCommon.getGlobalResourceValue("PlzSelectAtleastOneDiscrepancy");
    protected string AreuSureMessage = WebCommon.getGlobalResourceValue("AreYouSureReassignDiscrepancies");

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindUnclosedDiscrepancy();
        }
        
    }

    //protected void chkSelectAllText_CheckedChanged(object sender, EventArgs e)
    //{
    //    GridViewRow header = gdvDiscrepancy.HeaderRow;
    //    ucCheckbox ch = (ucCheckbox)header.FindControl("chkSelectAllText");
    //    if (ch.Checked)
    //    {
    //        SelectedStatus = 1;
    //        List<DiscrepancyBE> lstDetails = (List<DiscrepancyBE>)ViewState["DiscrepancyDetails"];
    //        foreach (var item in lstDetails)
    //        {
    //            string DiscrepancyLogid = item.DiscrepancyLogID.ToString();
    //            if (!hdnFldSelectedValues.Value.ToString().Contains(DiscrepancyLogid))
    //            {
    //                hdnFldSelectedValues.Value += "," + DiscrepancyLogid;
    //            }
    //        }

    //    }
    //    else
    //    {
    //        SelectedStatus = 0;
    //        hdnFldSelectedValues.Value = "";
    //    }
    //    BindUnclosedDiscrepancy();
    //}


    protected void BindUnclosedDiscrepancy()
    {
        hdnFldSelectedValues.Value = "";
        DiscrepancyBE dicrepancyBE = new DiscrepancyBE();
        DiscrepancyBAL discrepancyBAL = new DiscrepancyBAL();
        List<DiscrepancyBE> lstDisLog = null;
        dicrepancyBE.Action = "GetUnclosedDiscrepancy";


        lstDisLog = discrepancyBAL.GetUnclosedDiscrepancyBAL(dicrepancyBE);
        gdvDiscrepancy.DataSource = lstDisLog;
        gdvDiscrepancy.DataBind();
        ViewState["DiscrepancyDetails"] = lstDisLog;
        
    }

    protected void UpdateUnclosedDis_Click(object sender, EventArgs e)
    {
        int? result = 0;
        //lblmsg.Text = "";
        DiscrepancyBAL discrepancyBL = new DiscrepancyBAL();
        string DiscrepancyLogID=string.Empty;
        foreach (GridViewRow item in gdvDiscrepancy.Rows)
        {
            HiddenField hdn = (HiddenField)item.FindControl("hdndiscrepancyLogid");
            CheckBox chk = (CheckBox)item.FindControl("chkSelect");
            if (chk.Checked)
            {
                DiscrepancyLogID += hdn.Value + " ,";
            }
        }

        string strValue  = DiscrepancyLogID.Remove(DiscrepancyLogID.LastIndexOf(','));        
        
        string[] ArrDiscrepancyLogId = strValue.Split(',');
        foreach (var item in ArrDiscrepancyLogId)
        {
            if (item != "" && item != null)
            {
                DiscrepancyBE discrepancyBE = new DiscrepancyBE();
                discrepancyBE.DiscrepancyLogID = Convert.ToInt32(item);
                discrepancyBE.UserID = Convert.ToInt32(Session["UserID"]);
                discrepancyBE.Action = "UpdateUnclosedDiscrepancy";

                result = discrepancyBL.UpdateUnclosedDiscrepancyBAL(discrepancyBE);
            }
        }       
       BindUnclosedDiscrepancy();  
    }
    public override BusinessEntities.BaseBe[] SubSort(GridViewSortEventArgs e)
    {
        return Utilities.GenericListHelper<DiscrepancyBE>.SortList((List<DiscrepancyBE>)ViewState["DiscrepancyDetails"], e.SortExpression, e.SortDirection).ToArray();
    }

    protected void btnExport_Click(object sender, EventArgs e)
    {
        BindUnclosedDiscrepancy();
        LocalizeGridHeader(gdvDiscrepancy);
        gdvDiscrepancy.Columns[0].Visible = false;
        WebCommon.ExportHideHidden("UnclosedDiscrepancyList", gdvDiscrepancy);
    }

}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

public partial class DIS_SearchWorkList : CommonPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
       

        if (!IsPostBack)
        {
            BindSites();            
        }
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        EncryptQueryString("DIS_WorklistEdit.aspx");
    }

    protected DataSet GetSites()
    {
        SqlConnection sqlCon = GetConnection();
        SqlDataAdapter sqlDa = new SqlDataAdapter(@"select ltrim(rtrim(SitePrefix)) + ' - '+ SiteName as SiteDescription,SiteID from site", sqlCon);
        DataSet ds = new DataSet();
        sqlDa.Fill(ds, "Sites");
        return ds;
    }

    protected void BindSites()
    {
        DataSet ds = GetSites();
        ddlSite.DataSource = ds.Tables[0];
        ddlSite.DataValueField = "SiteID";
        ddlSite.DataTextField = "SiteDescription";
        ddlSite.DataBind();
    }

    protected SqlConnection GetConnection()
    {
        return (new SqlConnection("Data Source=172.29.9.7;Initial Catalog=OfficeDepot;Persist Security Info=True;User ID=sa;Password=spice"));
    }
}
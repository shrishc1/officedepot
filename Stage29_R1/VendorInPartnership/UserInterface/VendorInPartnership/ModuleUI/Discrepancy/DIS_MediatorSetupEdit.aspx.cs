﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.Discrepancy;
using BusinessLogicLayer.ModuleBAL.Discrepancy;
using WebUtilities;

public partial class DIS_MediatorSetupEdit : CommonPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        ucuser.CountryID = Convert.ToInt32(GetQueryStringValue("CountryID"));

        if (!Page.IsPostBack) {
            ltCountry.Text = GetQueryStringValue("CountryName");
            ucuser.ClearAndBindODUser();
        }
    }

    protected void btnSave_Click(object sender, EventArgs e) {
        string strUserID = ucuser.SelectedUserID;
        if (string.IsNullOrEmpty(strUserID)) {
            string errorMessage = WebCommon.getGlobalResourceValue("SelectMediator");
            Page.ClientScript.RegisterStartupScript(this.GetType(), "Message", "alert('" + errorMessage + "')", true);
            return;
        }

        MediatorBE oMediatorBE = new MediatorBE();
        MediatorBAL oMediatorBAL = new MediatorBAL();

        oMediatorBE.Action = "SaveMediatorUser";
        oMediatorBE.Country = new BusinessEntities.ModuleBE.AdminFunctions.MAS_CountryBE();
        oMediatorBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
        oMediatorBE.Country.CountryID = Convert.ToInt32(GetQueryStringValue("CountryID"));
        oMediatorBE.User.UserID = Convert.ToInt32(strUserID);

        oMediatorBAL.addEditMediatorBAL(oMediatorBE);
        EncryptQueryString("DIS_MediatorSetup.aspx");

    }

    protected void btnBack_Click(object sender, EventArgs e) {
        EncryptQueryString("DIS_MediatorSetup.aspx");
    }


}
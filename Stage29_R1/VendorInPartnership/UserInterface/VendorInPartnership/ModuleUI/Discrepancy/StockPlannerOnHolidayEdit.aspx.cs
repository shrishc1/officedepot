﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLogicLayer.ModuleBAL.Discrepancy;
using BusinessEntities.ModuleBE.Discrepancy;
using BusinessEntities.ModuleBE.Security;
using BaseControlLibrary;
using WebUtilities;
using BusinessLogicLayer.ModuleBAL.Security;
using BusinessEntities.ModuleBE.AdminFunctions;
using BusinessLogicLayer.ModuleBAL.AdminFunctions;
using System.Data;

public partial class ModuleUI_Discrepancy_StockPlannerOnHolidayEdit : CommonPage
{

    public string DateOfReturn = WebCommon.getGlobalResourceValue("DateOfReturn");
    public string AbsentRequired = WebCommon.getGlobalResourceValue("AbsentRequired");
    public string StockPlannerRequired = WebCommon.getGlobalResourceValue("StockPlannerRequired");
    public string DateOfReturnRequired = WebCommon.getGlobalResourceValue("DateOfReturnRequired");

    string strStockPlonnerId = string.Empty;
    public string StockPlannerUserIds
    {
        get
        {
            return ViewState["StockPlannerUserIds"] != null ? ViewState["StockPlannerUserIds"].ToString() : string.Empty;
        }
        set
        {
            ViewState["StockPlannerUserIds"] = value;
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (GetQueryStringValue("StockPlannerID") != null)
            {
                btnSave.Visible = true;
                lblStockPlannerValue.Text = GetQueryStringValue("StockPlanner");
                BindSelectedStockPlanners();                
            }
        }
    }
    protected void btnMoveRightAllStockPlanner_Click(object sender, EventArgs e)
    {
        if (uclstStockPlannerLeft.Items.Count > 0)
            FillControls.MoveAllItems(uclstStockPlannerLeft, uclstStockPlannerRight);
    }
    protected void btnMoveRightStockPlanner_Click(object sender, EventArgs e)
    {
        if (uclstStockPlannerLeft.SelectedItem != null)
        {
            FillControls.MoveOneItem(uclstStockPlannerLeft, uclstStockPlannerRight);
        }
    }
    protected void btnMoveLeftStockPlanner_Click(object sender, EventArgs e)
    {
        if (uclstStockPlannerRight.SelectedItem != null)
        {
            FillControls.MoveOneItem(uclstStockPlannerRight, uclstStockPlannerLeft);
        }
    }
    protected void btnMoveLeftAllStockPlanner_Click(object sender, EventArgs e)
    {
        if (uclstStockPlannerRight.Items.Count > 0)
        {
            FillControls.MoveAllItems(uclstStockPlannerRight, uclstStockPlannerLeft);
        }
    }
    private void BindSelectedStockPlanners()
    {

        //Get stock Planners (Bind Left Stock Planners List)
        StockPlannerHolidayCoverBE oStockPlannerHolidayCoverBE = new StockPlannerHolidayCoverBE();
        StockPlannerHolidayCoverBAL oStockPlannerHolidayBAL = new StockPlannerHolidayCoverBAL();
        oStockPlannerHolidayCoverBE.SCT_UserBE = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
        oStockPlannerHolidayCoverBE.Action = "GetStockPlannersWithUserCountrys";
        if (!string.IsNullOrEmpty(GetQueryStringValue("StockPlannerID")))
        {
            oStockPlannerHolidayCoverBE.SCT_UserBE.UserID = Convert.ToInt32(GetQueryStringValue("StockPlannerID"));
        }
        DataTable dtStockPlanner = oStockPlannerHolidayBAL.GetStockPlannerWithWithUserCountrysBAL(oStockPlannerHolidayCoverBE);
        if (dtStockPlanner.Rows.Count > 0)
        {
            FillControls.FillListBox(ref uclstStockPlannerLeft, dtStockPlanner, "StockPlannerWithCountry", "StockPlannerID");
        }
        else
        {
            FillControls.FillListBox(ref uclstStockPlannerLeft, dtStockPlanner, "StockPlannerWithCountry", "StockPlannerID");
        }

        //Get stock Planners (Bind Right Stock Planners List)
               
        oStockPlannerHolidayCoverBE.Action = "EditBindings";
        oStockPlannerHolidayCoverBE.StockPlannerID = Convert.ToInt32(GetQueryStringValue("StockPlannerID"));
        var result = oStockPlannerHolidayBAL.GetAllStockPlannerHolidaysBAL(oStockPlannerHolidayCoverBE);

        //*********** StockPlanner ***************//
        string StockPlannerID = GetQueryStringValue("StockPlannerID");

        uclstStockPlannerLeft.Items.Remove(uclstStockPlannerLeft.Items.FindByValue(StockPlannerID));

        if (result != null)
        {
            string StockPlannerCoverIDs = result.SPToCoverIDs;

            chkYes.Checked = result.IsAbsent;
            if (!string.IsNullOrEmpty(result.DateOfReturn))
            {
                DateTime dt = Convert.ToDateTime(result.DateOfReturn);
                txtDateOfReturn.innerControltxtDate.Value = dt.ToString("dd/MM/yyyy");
            }
            uclstStockPlannerLeft.Items.Remove(uclstStockPlannerLeft.Items.FindByValue(StockPlannerID));
            if (uclstStockPlannerLeft != null && uclstStockPlannerLeft != null && !string.IsNullOrEmpty(StockPlannerCoverIDs))
            {
                uclstStockPlannerRight.Items.Clear();
                string[] strStockPlonnerIDs = StockPlannerCoverIDs.Split(',');
                for (int index = 0; index < strStockPlonnerIDs.Length; index++)
                {
                    ListItem listItem = uclstStockPlannerLeft.Items.FindByValue(strStockPlonnerIDs[index]);
                    if (listItem != null)
                    {
                        StockPlannerUserIds += strStockPlonnerIDs[index] + ",";
                        uclstStockPlannerRight.Items.Add(listItem);
                        uclstStockPlannerLeft.Items.Remove(listItem);
                    }
                }
            }
        }
    }
    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        if (txtDateOfReturn.GetDate.Date < DateTime.Now.Date)
        {
            string errMsg = WebCommon.getGlobalResourceValue("HolidayCoverLeaveCheck");             
            ScriptManager.RegisterStartupScript(this, this.GetType(), "popup", "<script>alert('" + errMsg + "');</script>", false);
            return;
        }
        if(uclstStockPlannerRight.Items.Count==0)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "popup", "<script>alert('" + StockPlannerRequired + "');</script>", false);
            return;
        }
        if (uclstStockPlannerRight.Items.Count > 0)
        {
            if (!string.IsNullOrEmpty(GetQueryStringValue("StockPlannerID")))
            {
                StockPlannerHolidayCoverBE oStockPlannerHolidayCoverBE = new StockPlannerHolidayCoverBE();
                StockPlannerHolidayCoverBAL oStockPlannerHolidayBAL = new StockPlannerHolidayCoverBAL();
                SCT_UserBE oSCT_UserBE = new SCT_UserBE();
                if (uclstStockPlannerRight.Items.Count > 0)
                {
                    for (int i = 0; i < uclstStockPlannerRight.Items.Count; i++)
                    {
                        oStockPlannerHolidayCoverBE.SPToCoverIDs += uclstStockPlannerRight.Items[i].Value + ",";
                    }
                }
                oStockPlannerHolidayCoverBE.SPToCoverIDs = oStockPlannerHolidayCoverBE.SPToCoverIDs.TrimEnd(',');
                oStockPlannerHolidayCoverBE.Action = "AddEdit";
                
                string date = txtDateOfReturn.innerControltxtDate.Value;
                
                oStockPlannerHolidayCoverBE.DateOfReturn = date;
                oStockPlannerHolidayCoverBE.IsAbsent = chkYes.Checked;
                oStockPlannerHolidayCoverBE.StockPlannerID = Convert.ToInt32(GetQueryStringValue("StockPlannerID"));
                int? Result = oStockPlannerHolidayBAL.addEditStockPlannerHolidayCoveBAL(oStockPlannerHolidayCoverBE);
                EncryptQueryString("StockPlannerOnHoliday.aspx");
            }
            else
            {
                EncryptQueryString("StockPlannerOnHoliday.aspx");
            }
        }       
    }
}
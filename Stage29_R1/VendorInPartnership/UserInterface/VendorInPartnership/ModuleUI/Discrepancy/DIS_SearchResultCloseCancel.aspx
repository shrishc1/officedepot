﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    CodeFile="DIS_SearchResultCloseCancel.aspx.cs" Inherits="DIS_SearchResultCloseCancel" %>

<%--<%@ Register Src="../../CommonUI/UserControls/ucSeacrhnSelectVendor.ascx" TagName="ucSeacrhnSelectVendor"
    TagPrefix="cc4" %>--%>

<%@ Register Src="../../CommonUI/UserControls/MultiSelectVendor.ascx" TagName="MultiSelectVendor"
    TagPrefix="cc1" %>
<%@ Register Src="../../CommonUI/UserControls/MultiSelectSite.ascx" TagName="MultiSelectSite"
    TagPrefix="cc1" %>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript">
        $(document).ready(function () {
            $("body").keypress(function (e) {
                if (e.which == 13 || e.keyCode == 13) {
                    $('#<%=btnSearch.ClientID %>').click();
                    return false;
                }
            });
        });
    </script>
      <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <h2>
        <cc1:ucLabel ID="lblSearchResult" runat="server"></cc1:ucLabel>
    </h2>
    <div class="right-shadow">
        <div class="formbox">
            <cc1:ucMultiView ID="mvDiscrepancyList" runat="server" ActiveViewIndex="0">
                <cc1:ucView ID="vwSearchCreteria" runat="server">
                    <table width="90%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                        <tr>
                            <td style="font-weight: bold; width:22%">
                                &nbsp;&nbsp;<cc1:ucLabel ID="lblDiscrepancyType" runat="server" Text="Discrepancy Type" Width="100%"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold;">
                                :
                            </td>
                            <td style="font-weight: bold;">
                                <cc1:ucDropdownList runat="server" ID="ddlDiscrepancyType" Width="150px" />
                            </td>
                            <td style="font-weight: bold;">
                                <cc1:ucLabel ID="lblDiscrepancyNo" runat="server" Text="Discrepancy #" Width="100%"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold;">
                                :
                            </td>
                            <td style="font-weight: bold;">
                                <cc1:ucTextbox ID="txtDiscrepancyNo" runat="server" Width="145px"></cc1:ucTextbox>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold;">
                                &nbsp;&nbsp;<cc1:ucLabel ID="lblPurchaseOrderNo" runat="server" Text="Purchase Order #"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold;">
                                :
                            </td>
                            <td style="font-weight: bold;">
                                <cc1:ucTextbox ID="txtPurchaseOrder" runat="server" Width="145px"></cc1:ucTextbox>
                            </td>
                            <td style="font-weight: bold;" align="Left">
                                <cc1:ucLabel ID="lblPurchaseOrderDate" runat="server" Text="Purchase Order Date"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold;">
                                :
                            </td>
                            <td style="font-weight: bold;">
                                <asp:TextBox ID="txtPurchaseOrderDate" runat="server" ClientIDMode="Static" class="date" />
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold;">
                                &nbsp;&nbsp;<cc1:ucLabel ID="lblDiscrepancyDateRange" runat="server" Text="Discrepancy Date Range"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold;">
                                :
                            </td>
                            <td style="font-weight: bold;">
                                <asp:TextBox ID="txtDiscrepancyDateRangeFrom" runat="server" ClientIDMode="Static"
                                    class="date" />
                            </td>
                            <td style="font-weight: bold;" align="Left">
                                <cc1:ucLabel ID="lblTo" runat="server" Text="To"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold;">
                                :
                            </td>
                            <td style="font-weight: bold;">
                                <asp:TextBox ID="txtDiscrepancyDateRangeTo" runat="server" ClientIDMode="Static"
                                    class="date" />
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold;">
                                &nbsp;&nbsp;<cc1:ucLabel ID="lblDeliveryNote" runat="server" Text="Delivery Note"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold;">
                                :
                            </td>
                            <td style="font-weight: bold;">
                                <cc1:ucTextbox ID="txtDeliveryNote" runat="server" Width="145px"></cc1:ucTextbox>
                            </td>
                            <td style="font-weight: bold;">
                                <cc1:ucLabel ID="lblStockPlanner" runat="server" Text="Stock Planner"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold;">
                                :
                            </td>
                            <td>
                                <cc1:ucDropdownList runat="server" ID="ddlStockPlanner" Width="150px" />
                            </td>
                        </tr>
                        <tr>
                           
                            <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblSite" runat="server" Text="Site"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold;">
                                <cc1:ucLabel ID="UcLabel4" runat="server">:</cc1:ucLabel>
                            </td>
                            <td colspan="4">
                                <cc1:MultiSelectSite runat="server" ID="msSite" />
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold;">
                                &nbsp;&nbsp;<cc1:ucLabel ID="lblDiscrepancyStatus" runat="server" Text="Discrepancy Status"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold;">
                               :
                            </td>
                            <td style="font-weight: bold;" colspan="4">
                                <asp:DropDownList ID="ddlDiscrepancyStatus" runat="server" Width="150px">
                                <asp:ListItem Text="--Select--" Value="" />
                                    <asp:ListItem Text="Open" Value="O" />
                                    <asp:ListItem Text="Closed" Value="C" />
                                    <asp:ListItem Text="Forced Closed" Value="F" />
                                    <asp:ListItem Text="Deleted" Value="D" />
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                             <td style="font-weight: bold;">
                                 <cc1:ucLabel ID="lblVendor" runat="server" />
                            </td>
                             <td style="font-weight: bold; width: 1%">
                                   :
                              </td>
                            <td style="font-weight: bold;" colspan="4">
                                <%--<cc4:ucSeacrhnSelectVendor ID="ucSeacrhVendor1" runat="server" />--%>
                                <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                  <ContentTemplate>
                                    <cc1:MultiSelectVendor runat="server" ID="ucSeacrhVendor1" />
                                  </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                    </table>
                </cc1:ucView>
                <cc1:ucView ID="vwSearchListing" runat="server">
                    <cc1:ucPanel ID="pnlGrid" runat="server" ScrollBars="Both" CssClass="fieldset-form">
                        <cc1:ucGridView ID="gvDisLog" Width="100%" runat="server" CssClass="grid" OnRowDataBound="gvDisLog_RowDataBound"
                            OnSorting="SortGrid" AllowSorting="true" Style="overflow: auto;" OnRowCommand="gvDisLog_RowCommand">
                            <Columns>
                                <asp:TemplateField HeaderText="Site" SortExpression="Site.SiteName">
                                    <HeaderStyle HorizontalAlign="Left" Width="300px" />
                                    <ItemStyle HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <cc1:ucLiteral ID="ltSiteName" runat="server" Text='<%#Eval("Site.SiteName") %>'></cc1:ucLiteral><asp:HiddenField
                                            ID="hdnDisType" runat="server" Value='<%#Eval("DiscrepancyTypeID") %>' />
                                        <asp:HiddenField ID="hdnDisLogID" runat="server" Value='<%#Eval("DiscrepancyLogID") %>' />
                                        <asp:HiddenField ID="hdnSiteId" Value='<%#Eval("SiteID") %>' runat="server" />
                                        <asp:HiddenField ID="hdnVendorID" Value='<%#Eval("VendorID") %>' runat="server" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField HeaderText="Status" DataField="DiscrepancyStatusDiscreption" AccessibleHeaderText="false"
                                    SortExpression="DiscrepancyStatusDiscreption">
                                    <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                    <ItemStyle HorizontalAlign="Left" />
                                </asp:BoundField>
                                <asp:TemplateField HeaderText="VDR Number" SortExpression="VDRNo">
                                    <HeaderStyle Width="80px" HorizontalAlign="Left" />
                                    <ItemStyle HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <asp:HyperLink ID="hypLinkToDisDetails" runat="server" Text='<%#Eval("VDRNo") %>'></asp:HyperLink>

                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField HeaderText="VDR Discription" DataField="ProductDescription" SortExpression="ProductDescription">
                                    <HeaderStyle HorizontalAlign="Left" Width="300px" />
                                    <ItemStyle HorizontalAlign="Left" />
                                </asp:BoundField>
                                <asp:BoundField HeaderText="VDR Date" DataField="DiscrepancyLogDate" SortExpression="DiscrepancyLogDate">
                                    <HeaderStyle HorizontalAlign="Left" Width="80px" />
                                    <ItemStyle HorizontalAlign="Left" />
                                </asp:BoundField>
                                <asp:BoundField HeaderText="Delivery Note" DataField="DeliveryNoteNumber" SortExpression="DeliveryNoteNumber">
                                    <HeaderStyle HorizontalAlign="Left" Width="100px" />
                                    <ItemStyle HorizontalAlign="Left" />
                                </asp:BoundField>
                                <asp:BoundField HeaderText="Vendor" DataField="VendorNoName" SortExpression="VendorNoName">
                                    <HeaderStyle HorizontalAlign="Left" Width="400px" />
                                    <ItemStyle HorizontalAlign="Left" Wrap="true" />
                                </asp:BoundField>
                                <asp:BoundField HeaderText="PO Number" DataField="PurchaseOrderNumber" SortExpression="PurchaseOrderNumber">
                                    <HeaderStyle HorizontalAlign="Left" Width="100px" />
                                    <ItemStyle HorizontalAlign="Left" />
                                </asp:BoundField>
                                <asp:TemplateField HeaderText="User Name" SortExpression="User.FirstName">
                                    <HeaderStyle HorizontalAlign="Left" Width="400px" />
                                    <ItemStyle HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <cc1:ucLiteral ID="ltUserName" runat="server" Text='<%#Eval("User.FirstName") %>'></cc1:ucLiteral></ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField HeaderText="Stock Planner" DataField="StockPlannerNO" SortExpression="StockPlannerNO">
                                    <HeaderStyle HorizontalAlign="Left" Width="400px" />
                                    <ItemStyle HorizontalAlign="Left" />
                                </asp:BoundField>
                                <asp:TemplateField HeaderText="Forced Closed">
                                    <HeaderStyle Width="50px" HorizontalAlign="center" />
                                    <ItemTemplate>
                                        <asp:ImageButton ID="btnForcedClosed" ImageUrl="~/Images/forced-delete.gif" runat="server"
                                            CommandArgument='<%#Eval("DiscrepancyLogID") %>' CommandName="Forced Closed"
                                            OnCommand="btnForcedClosed_Click" /></ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Delete">
                                    <HeaderStyle Width="50px" HorizontalAlign="center" />
                                    <ItemTemplate>
                                        <asp:ImageButton ID="btnCancel" runat="server" ImageUrl="~/Images/delete.gif" CommandName="DeleteDiscrepancy" />
                                        <%--OnCommand="btnCancel_Click" CommandArgument='<%#Eval("DiscrepancyLogID") %>' />--%>                                        
                                    </ItemTemplate>                
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                            </Columns>
                        </cc1:ucGridView>
                    </cc1:ucPanel>
                </cc1:ucView>
            </cc1:ucMultiView>
        </div>
    </div>
    <div class="button-row">
        <%--<cc1:ucButton ID="btnSave" runat="server" Text="Save" class="button" />--%>
        <cc1:ucButton ID="btnSearch" runat="server" Text="Search" CssClass="button" OnClick="btnSearch_Click" />
       <cc1:ucButton ID="btnReturntoSearchCriteria" runat="server" Text="Return to Search Criteria"
            class="button" OnClick="btnReturntoSearchCriteria_Click" />
    </div>
</asp:Content>

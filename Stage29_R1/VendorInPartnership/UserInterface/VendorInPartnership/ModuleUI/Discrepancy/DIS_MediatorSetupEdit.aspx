﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    CodeFile="DIS_MediatorSetupEdit.aspx.cs" Inherits="DIS_MediatorSetupEdit" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="../../CommonUI/UserControls/ucSelectODUser.ascx" TagName="ucODUser"
    TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2>
        <cc1:ucLabel ID="lblMediatorSetup" runat="server"></cc1:ucLabel>
    </h2>
    <asp:ScriptManager ID="sm" runat="server"></asp:ScriptManager>
    <div class="right-shadow">
        
        <div class="formbox">
            <table width="80%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                <tr>
                    
                    <td style="font-weight: bold;" width="25%">
                        <cc1:ucLabel ID="lblCountry" runat="server" Text="Country" isRequired="true"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold;" width="1%" align="center">
                        :
                    </td>
                    <td style="font-weight: bold;" width="75%">
                        <cc1:ucLabel ID="ltCountry" runat="server"></cc1:ucLabel>
                    </td>
                   
                </tr>
                <tr>
                   
                    <td style="font-weight: bold">
                        <cc1:ucLabel ID="lblSearchUser" runat="server" Text="Search User" isRequired="true"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold;" align="center">
                        :
                    </td>
                    <td>
                        <uc1:ucODUser ID="ucuser" runat="server">
                        </uc1:ucODUser>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div class="bottom-shadow">
    </div>
    <div class="button-row">
        <cc1:ucButton ID="btnSave" runat="server" Text="Save" CssClass="button" OnClick="btnSave_Click"
             />
        <cc1:ucButton ID="btnBack" runat="server" Text="Back" CssClass="button" OnClick="btnBack_Click" />
    </div>
</asp:Content>

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using BaseControlLibrary;
using BusinessEntities.ModuleBE.Discrepancy.LogDiscrepancy;
using BusinessLogicLayer.ModuleBAL.Discrepancy.LogDiscrepancy;
using Microsoft.Reporting.WebForms;
using Utilities;
using WebUtilities;
using System.Web;


public partial class DIS_InvoiceImagePopup : CommonPage
{
    DataSet dsDummy = null; //dummy dataset use for repeater
    DataTable tbDummy = null; //dummy table for dataset
    public int quote_no;
    string LogFilePath = string.Empty;
    string mimeType;
    string encoding;
    string fileNameExtension;
    string[] streams;
    string defaultFileName = string.Empty;
    Warning[] warnings;
    protected string EncURL = string.Empty;

    protected string ConfirmMessage = WebCommon.getGlobalResourceValue("ConfirmMessage");
    protected string PleaseattachPDF = WebCommon.getGlobalResourceValue("PleaseattachPDF");

    protected void Page_Load(object sender, EventArgs e)
    {


        if (!IsPostBack) //check if the webpage is loaded for the first time.
        {
            if (GetQueryStringValue("Mode") != null && GetQueryStringValue("Mode") == "Edit")
            {
                ViewState["PreviousPage"] = Request.UrlReferrer;//Saves the Previous page url in ViewState
                hdnApplicationPath.Value = Request.UrlReferrer.ToString();
            }
        }

        dsDummy = new DataSet();
        tbDummy = new DataTable();

        if (!IsPostBack)
        {

            EncURL = "../DIS_SearchResult.aspx?" + Encryption.Encrypt("FromPage=images");


            if (GetQueryStringValue("VDRNo") != null &&
                GetQueryStringValue("VDRNo").ToString() != "")
                ltVDRNumberCreated.Text = GetQueryStringValue("VDRNo").ToString();

            BindImages();


        }


    }



    private string GetType(int discrepancyTypeId)
    {
        string strType = string.Empty;
        if (discrepancyTypeId == null) { discrepancyTypeId = 0; }
        switch (discrepancyTypeId)
        {
            case 1:
                strType = WebCommon.getGlobalResourceValue("Overs");
                break;
            case 2:
                strType = WebCommon.getGlobalResourceValue("Shortage");
                break;
            case 3:
                strType = WebCommon.getGlobalResourceValue("GoodsReceivedDamaged");
                break;
            case 4:
                strType = WebCommon.getGlobalResourceValue("NoPurchaseorder");
                break;
            case 5:
                strType = WebCommon.getGlobalResourceValue("NoPaperwork");
                break;
            case 6:
                strType = WebCommon.getGlobalResourceValue("IncorrectProduct");
                break;
            case 7:
                strType = WebCommon.getGlobalResourceValue("PresentationIssue");
                break;
            case 8:
                strType = WebCommon.getGlobalResourceValue("IncorrectAddress");
                break;
            case 9:
                strType = WebCommon.getGlobalResourceValue("PaperworkAmended");
                break;
            case 10:
                strType = WebCommon.getGlobalResourceValue("WrongPackSize");
                break;
            case 11:
                strType = WebCommon.getGlobalResourceValue("FailPalletSpecification");
                break;
            case 12:
                strType = WebCommon.getGlobalResourceValue("QualityIssues");
                break;
            case 13:
                strType = WebCommon.getGlobalResourceValue("PrematureInvoiceReceipt");
                break;
            case 14:
                strType = WebCommon.getGlobalResourceValue("GenericDiscrepancy");
                break;
            case 15:
                strType = WebCommon.getGlobalResourceValue("ShuttleDiscrepancy");
                break;
            case 16:
                strType = WebCommon.getGlobalResourceValue("ReservationIssue");
                break;
        }
        return strType;
    }

    protected void btnOK_Click(object sender, EventArgs e)
    {
        DISLog_ImagesBE oDISLog_ImagesBE = new DISLog_ImagesBE();
        DISLog_ImagesBAL oDISLog_ImagesBAL = new DISLog_ImagesBAL();
        if (rpImages.Items.Count > 0)
        {
            //save the images
            string sImageNames = string.Empty;
            //oDISLog_ImagesBE.Discrepancy.DiscrepancyLogID = 6;

            foreach (RepeaterItem rptItem in rpImages.Items)
            {
                HiddenField hdDiscrepancyLogImageID = ((HiddenField)rptItem.FindControl("hdnDiscrepancyLogImageID"));
                string imageName = ((HtmlAnchor)rptItem.FindControl("hylImageName")).InnerText;
                //HiddenField hdImageFullPath = ((HiddenField)rptItem.FindControl("ImageFullPath"));
                if (hdDiscrepancyLogImageID == null
                    || (hdDiscrepancyLogImageID != null && hdDiscrepancyLogImageID.Value == ""))
                {
                    sImageNames += imageName.Trim(new char[] { ',', '\r', '\n', ' ' }) + ",";
                }
            }
            if (sImageNames.Trim() != "")
            {
                oDISLog_ImagesBE.ImageNames = sImageNames.Trim(new char[] { ',', '\r', '\n', ' ' });
            }
        }
        else
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + PleaseattachPDF + "')", true);
            return;
        }


        oDISLog_ImagesBE.Discrepancy = new DiscrepancyBE();
        oDISLog_ImagesBE.Action = "InsertImages";
        oDISLog_ImagesBE.Discrepancy.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("DiscrepancyLogID"));
        oDISLog_ImagesBAL.addEditDISLogImageDetailsBAL(oDISLog_ImagesBE);
        oDISLog_ImagesBAL = null;
        BindImages();

        if (GetQueryStringValue("Mode") != null && GetQueryStringValue("Mode") == "Edit")
        {
            if (ViewState["PreviousPage"] != null)
            { //Check if the ViewState contains Previous page URL
                Response.Redirect(ViewState["PreviousPage"].ToString());//Redirect to Previous page by retrieving the PreviousPage Url from ViewState.
            }
            else
            {
                if (GetQueryStringValue("SiteId") != null && GetQueryStringValue("DisDate") != null)
                    EncryptQueryString("../DIS_SearchResult.aspx?FromPage=images&SiteId=" + GetQueryStringValue("SiteId") + "&DisDate=" + GetQueryStringValue("DisDate"));
                else
                    EncryptQueryString("../DIS_SearchResult.aspx?FromPage=images");
            }
        }
        else
        {
            if (GetQueryStringValue("SiteId") != null && GetQueryStringValue("DisDate") != null)
                EncryptQueryString("../DIS_SearchResult.aspx?FromPage=images&SiteId=" + GetQueryStringValue("SiteId") + "&DisDate=" + GetQueryStringValue("DisDate"));
            else
                EncryptQueryString("../DIS_SearchResult.aspx?FromPage=images");
        }

    }

    protected void btnAddImage_Click(object sender, EventArgs e)
    {
        for (int i = 0; i < Request.Files.Count; i++) {
            HttpPostedFile fileImage = Request.Files[i];

            if (fileImage.FileName != null
                && fileImage.FileName != "") {
                if (!(Convert.ToString(fileImage.FileName.Split('.')[1]).Trim().ToLower() == "jpeg"
                            || Convert.ToString(fileImage.FileName.Split('.')[1]).Trim().ToLower() == "jpg"
                            || Convert.ToString(fileImage.FileName.Split('.')[1]).Trim().ToLower() == "png"
                            || Convert.ToString(fileImage.FileName.Split('.')[1]).Trim().ToLower() == "bmp"
                            || Convert.ToString(fileImage.FileName.Split('.')[1]).Trim().ToLower() == "gif")) {

                    string sWrongImage = WebUtilities.WebCommon.getGlobalResourceValue("WrongImage");
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Mess1", "alert('" + sWrongImage + "')", true);
                    return;
                }
            }
            else {
                string sSelectImage = WebUtilities.WebCommon.getGlobalResourceValue("SelectImage");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Mess1", "alert('" + sSelectImage + "')", true);
                return;
            }
        }

         for (int i = 0; i < Request.Files.Count; i++) {
            HttpPostedFile fileImage = Request.Files[i];
       
                //check for the duplicate image
                foreach (RepeaterItem item in rpImages.Items)
                {
                    HiddenField hdImageID = ((HiddenField)item.FindControl("hdnImageID"));
                    //HyperLink hylImageName = ((HyperLink)item.FindControl("hylImageName"));
                    HtmlAnchor hylImageName = ((HtmlAnchor)item.FindControl("hylImageName"));
                    ucButton btnAdd = ((ucButton)item.FindControl("RemoveImage"));

                    if (hylImageName.InnerText.Trim() == ltVDRNumberCreated.Text + "_" + fileImage.FileName.Trim())
                    {
                        string sImageAlreadyAdded = WebUtilities.WebCommon.getGlobalResourceValue("ImageAlreadyAdded");
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Mess1", "alert('" + sImageAlreadyAdded + "')", true);
                        return;
                    }
                }
                IncreaseRow(i, fileImage);
            }            
    }

    protected void RemoveImage_Click(object sender, CommandEventArgs e)
    {
        string path = AppDomain.CurrentDomain.BaseDirectory.ToLower();
        path = path.Replace(@"\bin\debug", "");

        if (e.CommandName == "Remove" && e.CommandArgument != null)
        {
            RepeaterItem rptSelectedItem = (RepeaterItem)((Control)sender).NamingContainer;
            HiddenField hdSelectedDisLogItemID = ((HiddenField)rptSelectedItem.FindControl("hdnDiscrepancyLogImageID"));
            HiddenField hdSelectedImageD = ((HiddenField)rptSelectedItem.FindControl("hdnImageID"));
            int count = 1;
            tbDummy = null;
            tbDummy = new DataTable();
            AddColumns();
            foreach (RepeaterItem item in rpImages.Items)
            {
                HiddenField hdImageID = ((HiddenField)item.FindControl("hdnImageID"));
                ucButton btnAdd = ((ucButton)item.FindControl("RemoveImage"));
                HiddenField hdDiscrepancyLogImageID = ((HiddenField)item.FindControl("hdnDiscrepancyLogImageID"));
                HtmlAnchor hylImageName = ((HtmlAnchor)item.FindControl("hylImageName"));

                if (hdDiscrepancyLogImageID != null && hdDiscrepancyLogImageID.Value.Trim() != ""
                    && hdDiscrepancyLogImageID.Value.Trim() == hdSelectedDisLogItemID.Value.Trim())
                {
                    DISLog_ImagesBE oDISLog_ImagesBE = new DISLog_ImagesBE();
                    DISLog_ImagesBAL oDISLog_ImagesBAL = new DISLog_ImagesBAL();
                    oDISLog_ImagesBE.Discrepancy = new DiscrepancyBE();
                    oDISLog_ImagesBE.Action = "DeleteImage";
                    oDISLog_ImagesBE.DiscrepancyLogImageID = Convert.ToInt32(hdDiscrepancyLogImageID.Value);
                    oDISLog_ImagesBAL.addEditDISLogImageDetailsBAL(oDISLog_ImagesBE);
                    oDISLog_ImagesBAL = null;

                    //delete also from the Images folder
                    if (File.Exists(path + @"Images/Discrepancy/" + hylImageName.InnerText.Trim()))
                    {
                        File.Delete(path + @"Images/Discrepancy/" + hylImageName.InnerText.Trim());
                    }
                }

                else if (e.CommandArgument.ToString().Trim() != hdImageID.Value.ToString().Trim())
                {
                    //now change button text to remove, and save user entered values in table
                    if (hdDiscrepancyLogImageID != null && hdDiscrepancyLogImageID.Value != "")
                        tbDummy.Rows.Add(hylImageName.InnerText, (count++).ToString(), hdDiscrepancyLogImageID.Value,
                            //hylImageName.NavigateUrl.ToString(), 
                            "Remove");
                    else
                        tbDummy.Rows.Add(hylImageName.InnerText, (count++).ToString(), "",
                            //hylImageName.NavigateUrl.ToString(), 
                            "Remove");
                }
                else if (e.CommandArgument.ToString().Trim() == hdImageID.Value.ToString().Trim())
                {
                    //delete also from the Images folder
                    if (File.Exists(path + @"Images/Discrepancy/" + hylImageName.InnerText.Trim()))
                    {
                        File.Delete(path + @"Images/Discrepancy/" + hylImageName.InnerText.Trim());
                    }
                }
            }
            BindWithRepeater();
        }
    }

    private void AddColumns()
    {
        //Add 3 dummy coloumn, this can be increase on our need basis
        tbDummy.Columns.Add("ImageName");
        tbDummy.Columns.Add("ImageID");
        tbDummy.Columns.Add("DiscrepancyLogImageID");
        tbDummy.Columns.Add("Button");
    }

    private void BindWithRepeater()
    {
        if (dsDummy.Tables.Count <= 0) {

            //add this table to dataset
            dsDummy.Tables.Add(tbDummy);
        }

        //bind this dataset to repeater
        rpImages.DataSource = dsDummy;
        rpImages.DataBind();
    }

    private void IncreaseRow(int Icount, HttpPostedFile fileImage)
    {
        int Total = rpImages.Items.Count + 1;
        int count = 1;

        string path = AppDomain.CurrentDomain.BaseDirectory.ToLower();
        path = path.Replace(@"\bin\debug", "");

        if (Icount == 0)
            AddColumns();

        foreach (RepeaterItem item in rpImages.Items)
        {
            //getting the values of user entered fields
            HiddenField hddiscrepancyLogImageID = ((HiddenField)item.FindControl("hdnDiscrepancyLogImageID"));

           
            HtmlAnchor hylImageName = ((HtmlAnchor)item.FindControl("hylImageName"));
            ucButton btnAdd = ((ucButton)item.FindControl("RemoveImage"));

            //now change button text to remove, and save user entered values in table
            if (hddiscrepancyLogImageID != null && hddiscrepancyLogImageID.Value != "")
            {
                tbDummy.Rows.Add(hylImageName.InnerText.Trim(), (count++).ToString(), hddiscrepancyLogImageID.Value, "Remove");
            }
            else
            {
                tbDummy.Rows.Add(hylImageName.InnerText.Trim(), (count++).ToString(), "", "Remove");
            }
        }

        //Add dummy row, because we need to increase
        if (!File.Exists(path + "Images/Discrepancy/" + ltVDRNumberCreated.Text + "_" + fileImage.FileName))
        {
            fileImage.SaveAs(path + "Images/Discrepancy/" + ltVDRNumberCreated.Text + "_" + fileImage.FileName);
        }
        tbDummy.Rows.Add(ltVDRNumberCreated.Text + "_" + fileImage.FileName, count++, "", "Remove");
        
        BindWithRepeater();

        tbDummy.Rows.Clear();
    }

    protected void BindImages()
    {
        AddColumns();
        if (GetQueryStringValue("DiscrepancyLogID") != null && GetQueryStringValue("DiscrepancyLogID").ToString() != "")
        {
            DISLog_ImagesBE oDISLog_ImagesBE = new DISLog_ImagesBE();
            DISLog_ImagesBAL oDISLog_ImagesBAL = new DISLog_ImagesBAL();
            oDISLog_ImagesBE.Discrepancy = new DiscrepancyBE();
            oDISLog_ImagesBE.Action = "ShowAll";
            oDISLog_ImagesBE.Discrepancy.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("DiscrepancyLogID"));
            
            List<DISLog_ImagesBE> lstImagelist = oDISLog_ImagesBAL.GetDISLogImageDetailsBAL(oDISLog_ImagesBE);
            oDISLog_ImagesBAL = null;
            int count = 1;
            if (lstImagelist != null && lstImagelist.Count > 0)
            {

                foreach (DISLog_ImagesBE item in lstImagelist)
                {
                    tbDummy.Rows.Add(item.ImageName.ToString().Trim(), count++, item.DiscrepancyLogImageID, "Remove");
                    
                }
                dsDummy.Tables.Add(tbDummy);
                //bind this dataset to repeater
                rpImages.DataSource = dsDummy;
                rpImages.DataBind();
            }
        }
    }
}
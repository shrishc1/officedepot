﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using BaseControlLibrary;
using BusinessEntities.ModuleBE.Discrepancy.LogDiscrepancy;
using BusinessLogicLayer.ModuleBAL.Discrepancy.LogDiscrepancy;
using Microsoft.Reporting.WebForms;
using WebUtilities;
using Utilities;
using BusinessEntities.ModuleBE.Security;
using BusinessLogicLayer.ModuleBAL.Security;
using System.Web;

public partial class DIS_OptionsPopup : CommonPage {
    DataSet dsDummy = null; //dummy dataset use for repeater
    DataTable tbDummy = null; //dummy table for dataset
    public int quote_no;
    string LogFilePath = string.Empty;
    string mimeType;
    string encoding;
    string fileNameExtension;
    string[] streams;
    string defaultFileName = string.Empty;
    Warning[] warnings;
    protected string EncURL = string.Empty;
    sendCommunicationCommon oSendCommunicationCommon = new sendCommunicationCommon();
    protected string ConfirmMessage = WebCommon.getGlobalResourceValue("ConfirmMessage");
    protected void Page_Load(object sender, EventArgs e) {
        

        if (!IsPostBack) //check if the webpage is loaded for the first time.
        {
            if (GetQueryStringValue("Mode") != null && GetQueryStringValue("Mode") == "Edit")
            {
                ViewState["PreviousPage"] = Request.UrlReferrer;//Saves the Previous page url in ViewState
                hdnApplicationPath.Value = Request.UrlReferrer.ToString();
            }
        }

        dsDummy = new DataSet();
        tbDummy = new DataTable();

        if (!IsPostBack) {

            EncURL = "../DIS_SearchResult.aspx?"+Encryption.Encrypt("FromPage=images");

            if (GetQueryStringValue("CommunicationType").ToString() == "L") {
                string a = "window.open('DIS_PrintLetter.aspx', '', 'left =' + screen.width + ',top=' + screen.height + ',width=0,height=0,toolbar=0,scrollbars=0,status=0')";
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "pp", a, true);
            }
            if (GetQueryStringValue("VDRNo") != null &&
                GetQueryStringValue("VDRNo").ToString() != "")
                ltVDRNumberCreated.Text = GetQueryStringValue("VDRNo").ToString();

            BindImages();

            rfvCarriageChargeRequired.Enabled = false;
            cmpvCarriageChargeGreaterThanZero.Enabled = false;
            txtFreightCharges.Enabled = false;
            //**************************************
            DiscrepancyBE oNewDiscrepancyBE = new DiscrepancyBE();
            DiscrepancyBAL oNewDiscrepancyBAL = new DiscrepancyBAL();
            oNewDiscrepancyBE.Action = "GetDiscrepancySiteVendorDetail";
            oNewDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("DiscrepancyLogID"));

            List<DiscrepancyBE> lstVendor = oNewDiscrepancyBAL.GetDiscrepancyLogSiteVendorDetailsBAL(oNewDiscrepancyBE);
            int DiscrepancyTypeId = Convert.ToInt32(lstVendor[0].DiscrepancyTypeID.ToString());
            //**************************************
            if (GetQueryStringValue("Mode") == null && GetQueryStringValue("Mode") != "Edit" && this.GetType(DiscrepancyTypeId) != "Shortage")
            {
                PrintLavel();
            }
           
        }

        if (GetQueryStringValue("Type") != null) {
            pnlDiscrepancyCharges.Visible = true;
        }
        else {
            pnlDiscrepancyCharges.Visible = false;
        }
    }

    protected string InvalidValuesEntered = WebCommon.getGlobalResourceValue("InvalidValuesEntered");

    private string GetType(int discrepancyTypeId)
    {
        string strType = string.Empty;
        if (discrepancyTypeId == null) { discrepancyTypeId = 0; }
        switch (discrepancyTypeId)
        {
            case 1:
                strType = WebCommon.getGlobalResourceValue("Overs");
                break;
            case 2:
                strType = WebCommon.getGlobalResourceValue("Shortage");
                break;
            case 3:
                strType = WebCommon.getGlobalResourceValue("GoodsReceivedDamaged");
                break;
            case 4:
                strType = WebCommon.getGlobalResourceValue("NoPurchaseorder");
                break;
            case 5:
                strType = WebCommon.getGlobalResourceValue("NoPaperwork");
                break;
            case 6:
                strType = WebCommon.getGlobalResourceValue("IncorrectProduct");
                break;
            case 7:
                strType = WebCommon.getGlobalResourceValue("PresentationIssue");
                break;
            case 8:
                strType = WebCommon.getGlobalResourceValue("IncorrectAddress");
                break;
            case 9:
                strType = WebCommon.getGlobalResourceValue("PaperworkAmended");
                break;
            case 10:
                strType = WebCommon.getGlobalResourceValue("WrongPackSize");
                break;
            case 11:
                strType = WebCommon.getGlobalResourceValue("FailPalletSpecification");
                break;
            case 12:
                strType = WebCommon.getGlobalResourceValue("QualityIssues");
                break;
            case 13:
                strType = WebCommon.getGlobalResourceValue("PrematureInvoiceReceipt");
                break;
            case 14:
                strType = WebCommon.getGlobalResourceValue("GenericDiscrepancy");
                break;
            case 15:
                strType = WebCommon.getGlobalResourceValue("ShuttleDiscrepancy");
                break;
            case 16:
                strType = WebCommon.getGlobalResourceValue("ReservationIssue");
                break;
            case 20:
                strType = WebCommon.getGlobalResourceValue("ItemNotOnPO");
                break; 
        }
        return strType;
    }

    protected void btnOK_Click(object sender, EventArgs e) {
        DISLog_ImagesBE oDISLog_ImagesBE = new DISLog_ImagesBE();
        DISLog_ImagesBAL oDISLog_ImagesBAL = new DISLog_ImagesBAL();
        if (rpImages.Items.Count > 0) {
            //save the images
            string sImageNames = string.Empty;
            //oDISLog_ImagesBE.Discrepancy.DiscrepancyLogID = 6;

            foreach (RepeaterItem rptItem in rpImages.Items) {
                HiddenField hdDiscrepancyLogImageID = ((HiddenField)rptItem.FindControl("hdnDiscrepancyLogImageID"));
                string imageName = ((HtmlAnchor)rptItem.FindControl("hylImageName")).InnerText;
                //HiddenField hdImageFullPath = ((HiddenField)rptItem.FindControl("ImageFullPath"));
                if (hdDiscrepancyLogImageID == null
                    || (hdDiscrepancyLogImageID != null && hdDiscrepancyLogImageID.Value == "")) {
                    sImageNames += imageName.Trim(new char[] { ',', '\r', '\n', ' ' }) + ",";
                }
            }
            if (sImageNames.Trim() != "") {
                oDISLog_ImagesBE.ImageNames = sImageNames.Trim(new char[] { ',', '\r', '\n', ' ' });
            }
        }
        if (pnlDiscrepancyCharges.Visible == true
                || pnlDiscrepancyCharges.Style["display"] == "" || pnlDiscrepancyCharges.Style["display"] == "block") {
            oDISLog_ImagesBE.Freight_Charges = 0;
            if (rdoOthers.Checked == true)
                oDISLog_ImagesBE.Freight_Charges = Convert.ToDecimal(txtFreightCharges.Text.Trim());

        }
        oDISLog_ImagesBE.Discrepancy = new DiscrepancyBE();
        oDISLog_ImagesBE.Action = "InsertImages";
        oDISLog_ImagesBE.Discrepancy.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("DiscrepancyLogID"));
        oDISLog_ImagesBAL.addEditDISLogImageDetailsBAL(oDISLog_ImagesBE);
        oDISLog_ImagesBAL = null;
        BindImages();

        if (txtLablesNumber.Text.Trim() != string.Empty) 
        {
            PrintLavel();  
        }
        else {
            if (GetQueryStringValue("Mode") != null && GetQueryStringValue("Mode") == "Edit") {
                if (ViewState["PreviousPage"] != null) { //Check if the ViewState contains Previous page URL
                    Response.Redirect(ViewState["PreviousPage"].ToString());//Redirect to Previous page by retrieving the PreviousPage Url from ViewState.
                }
                else {
                    if (GetQueryStringValue("SiteId") != null && GetQueryStringValue("DisDate") != null)
                        EncryptQueryString("../DIS_SearchResult.aspx?FromPage=images&SiteId=" + GetQueryStringValue("SiteId") + "&DisDate=" + GetQueryStringValue("DisDate"));
                    else
                        EncryptQueryString("../DIS_SearchResult.aspx?FromPage=images");
                }
            }
            else {
                if (GetQueryStringValue("SiteId") != null && GetQueryStringValue("DisDate") != null)
                    EncryptQueryString("../DIS_SearchResult.aspx?FromPage=images&SiteId=" + GetQueryStringValue("SiteId") + "&DisDate=" + GetQueryStringValue("DisDate"));
                else
                    EncryptQueryString("../DIS_SearchResult.aspx?FromPage=images");
            }
        }
    }
    protected void PrintLavel()
    {
        int iLabelQty;
        bool isiDeliveryQtyDecimal = int.TryParse(txtLablesNumber.Text.Trim(), out iLabelQty);
        if (!string.IsNullOrEmpty(txtLablesNumber.Text)&&!isiDeliveryQtyDecimal)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + InvalidValuesEntered + "')", true);
            return;
        }

        //get vendor details  
        DiscrepancyBE oNewDiscrepancyBE = new DiscrepancyBE();
        DiscrepancyBAL oNewDiscrepancyBAL = new DiscrepancyBAL();
        oNewDiscrepancyBE.Action = "GetDiscrepancySiteVendorDetail";
        oNewDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("DiscrepancyLogID"));

        List<DiscrepancyBE> lstVendor = oNewDiscrepancyBAL.GetDiscrepancyLogSiteVendorDetailsBAL(oNewDiscrepancyBE);
        string strCommunicationTo = string.Empty;
        //****************** Get vendor EmailId ***************************
        if (lstVendor != null && lstVendor.Count > 0)
        {
            strCommunicationTo = lstVendor[0].CommunicationTo;
            SCT_UserBE oSCT_UserBE = new SCT_UserBE();
            SCT_UserBAL oSCT_UserBAL = new SCT_UserBAL();
            oSCT_UserBE.Action = "GetContactDetailsDiscrepancy";
            oSCT_UserBE.VendorID = lstVendor[0].VendorID;
            oSCT_UserBE.SiteId = lstVendor[0].SiteID;
            List<SCT_UserBE> lstVendorDetails1 = oSCT_UserBAL.GetUserDetailsBAL(oSCT_UserBE);
            if (lstVendorDetails1 != null && lstVendorDetails1.Count > 0)
            {
                strCommunicationTo += ",";
                foreach (SCT_UserBE item in lstVendorDetails1)
                {
                    if (!strCommunicationTo.Contains(item.EmailId.ToString()))
                    {
                        strCommunicationTo += item.EmailId.ToString() + ", ";
                    }
                }
            }
        }
        //*****************************************************************

        StringBuilder sb = new StringBuilder();
        string ActualStockPlannerNoValue = string.Empty;
        string ActualPlannerContactNoValue = string.Empty;

        if (lstVendor != null && lstVendor.Count > 0)
        {
            if (string.IsNullOrEmpty(txtLablesNumber.Text))
            {

                string ReturnAddress = lstVendor[0].Vendor.ReturnAddress.Replace("\r\n", "</span></td></tr><tr><td valign='top'><span class='style10'>");
                int discrepancyTypeID = lstVendor[0].DiscrepancyTypeID != null ? Convert.ToInt32(lstVendor[0].DiscrepancyTypeID) : 0;

                List<DiscrepancyBE> lstDetails = oSendCommunicationCommon.getDiscrepancyItemDetail("GetDiscrepancyItem", Convert.ToInt32(GetQueryStringValue("DiscrepancyLogID")), this.GetType(discrepancyTypeID));


                DiscrepancyBE DiscrepancyBE = new DiscrepancyBE();
                DiscrepancyBAL DiscrepancyBAL = new DiscrepancyBAL();

                DiscrepancyBE.Action = "GetActualStockPlannerDetailsBySPId"; // getting details of SP while discrepancy is logged

                DiscrepancyBE.StockPlannerID = Convert.ToInt32(lstDetails[0].ActualStockPlannerID);

                List<DiscrepancyBE> lstActualStockPlannerDetailsBySPId = DiscrepancyBAL.GetActualStockPlannerDetailsBAL(DiscrepancyBE);

                if (lstActualStockPlannerDetailsBySPId.Count > 0)
                {
                    ActualStockPlannerNoValue = lstActualStockPlannerDetailsBySPId[0].StockPlannerNO.ToString();
                    ActualPlannerContactNoValue = lstActualStockPlannerDetailsBySPId[0].StockPlannerContact.ToString();
                }

                if (!string.IsNullOrEmpty(ActualStockPlannerNoValue))
                {
                    if (discrepancyTypeID == 12)
                    {
                        sb.Append("<table width='100%' border='1' style=' border-collapse: collapse;'  BORDERCOLOR=black  > " +
                    "<tr><td valign='top' ><div align='center' class='style1'>" + WebCommon.getGlobalResourceValue("Discrepancy") + " # - " + GetQueryStringValue("VDRNo") + " </div></td></tr> " +
                   // "<tr><td valign='top'><div align='left' class='style1'>" + WebCommon.getGlobalResourceValue("Type") + " - " + this.GetType(discrepancyTypeID) + " </div></td></tr> " +
                   "<tr><td valign='top'  align='center'><span align='center' class='style1'>" + WebCommon.getGlobalResourceValue("Location") + "- </span><span style='font-size:210%'>" + lstVendor[0].Location.ToString() + " </span></td></tr> " +
                   "<tr><td valign='top'  align='center'><span align='center' class='style1'>" + WebCommon.getGlobalResourceValue("Vendor") + " # - </span><span style='font-size:210%'>" + lstVendor[0].Vendor.Vendor_No + " - " + lstVendor[0].Vendor.VendorName + " </span></td></tr> " +
                    "<tr><td valign='top'  align='center'><span align='center' class='style1'>" + WebCommon.getGlobalResourceValue("PurchaseOrder") + " - </span><span style='font-size:210%'>" + Convert.ToString(lstVendor[0].PurchaseOrder.Purchase_order) + " </span></td></tr> " +
                    "<tr><td valign='top'  align='center'><span align='center' class='style1'>" + WebCommon.getGlobalResourceValue("DeliveryNote") + " - </span><span style='font-size:210%'>" + lstVendor[0].DeliveryNoteNumber + " </span></td></tr> " +
                     "<tr><td><table width='100%' border='0'  BORDERCOLOR=black  >" +
                      "<tr><td class='style10' width='25%'>" + WebCommon.getGlobalResourceValue("Site") + "</td><td td width='25%'>" + lstVendor[0].Site.SiteName + "</td>" +
                      "<td  width='25%' class='style10'>" + WebCommon.getGlobalResourceValue("DateCreated") + "</td><td  width='25%'>" + Convert.ToDateTime(lstVendor[0].DiscrepancyLogDate.ToString()).ToString("dd/MM/yyyy") + "</td></tr>" +
                      "<tr><td class='style10'>" + WebCommon.getGlobalResourceValue("Type") + "</td><td>" + this.GetType(discrepancyTypeID) + " </td>" +
                      "<td class='style10'>" + WebCommon.getGlobalResourceValue("CreatedBy") + "</td><td>" + lstVendor[0].Username.ToString().Replace("-", " ").ToString() + "</td></tr>" +
                      "<tr><td>&nbsp;</td><td>&nbsp; </td>" +
                      "<td class='style10'>" + WebCommon.getGlobalResourceValue("SentTo") + "</td><td>" + strCommunicationTo + "</td></tr>" +
                     "</table></td></tr> " +

                     "<tr><td><table width='100%' border='0'  BORDERCOLOR=black  >" +
                      "<tr><td width='25%' class='style10'>" + WebCommon.getGlobalResourceValue("Vendor") + " #</td><td width='25%'>" + lstVendor[0].Vendor.Vendor_No + " - " + lstVendor[0].Vendor.VendorName + "</td>" +
                      "<td width='25%' class='style10'>" + WebCommon.getGlobalResourceValue("StockPlanner") + "</td><td width='25%'>" + ActualStockPlannerNoValue + "</td></tr>" +
                      "<tr><td class='style10'>" + WebCommon.getGlobalResourceValue("ReturnsAddress") + "</td><td>" + lstVendor[0].Vendor.address1 + " " + lstVendor[0].Vendor.address2 + " " + lstVendor[0].Vendor.city + " " + lstVendor[0].Vendor.county + " </td>" +
                      "<td class='style10'>Tel #</td><td>" + ActualPlannerContactNoValue + "</td></tr>" +
                     "</table></td></tr> " + GetHtmlString(discrepancyTypeID, lstDetails) +
                    "<tr><td width ='100%' style='padding-top: 10px; padding-bottom: 10px'><b>Description Of Issue: </b>" + lstDetails[0].Comment + "</td></tr> " +
                    "</table>" );
                        sb.Append("<p style='page-break-before: always'></p>");
                    }
                    else
                    {
                        sb.Append("<table width='100%' border='1' style=' border-collapse: collapse;'  BORDERCOLOR=black  > " +
                   "<tr><td valign='top' ><div align='center' class='style1'>" + WebCommon.getGlobalResourceValue("Discrepancy") + " # - " + GetQueryStringValue("VDRNo") + " </div></td></tr> " +
                  // "<tr><td valign='top'><div align='left' class='style1'>" + WebCommon.getGlobalResourceValue("Type") + " - " + this.GetType(discrepancyTypeID) + " </div></td></tr> " +
                  "<tr><td valign='top'  align='center'><span align='center' class='style1'>" + WebCommon.getGlobalResourceValue("Location") + "- </span><span style='font-size:210%'>" + lstVendor[0].Location.ToString() + " </span></td></tr> " +
                  "<tr><td valign='top'  align='center'><span align='center' class='style1'>" + WebCommon.getGlobalResourceValue("Vendor") + " # - </span><span style='font-size:210%'>" + lstVendor[0].Vendor.Vendor_No + " - " + lstVendor[0].Vendor.VendorName + " </span></td></tr> " +
                   "<tr><td valign='top'  align='center'><span align='center' class='style1'>" + WebCommon.getGlobalResourceValue("PurchaseOrder") + " - </span><span style='font-size:210%'>" + Convert.ToString(lstVendor[0].PurchaseOrder.Purchase_order) + " </span></td></tr> " +
                   "<tr><td valign='top'  align='center'><span align='center' class='style1'>" + WebCommon.getGlobalResourceValue("DeliveryNote") + " - </span><span style='font-size:210%'>" + lstVendor[0].DeliveryNoteNumber + " </span></td></tr> " +
                    "<tr><td><table width='100%' border='0'  BORDERCOLOR=black  >" +
                     "<tr><td class='style10' width='25%'>" + WebCommon.getGlobalResourceValue("Site") + "</td><td td width='25%'>" + lstVendor[0].Site.SiteName + "</td>" +
                     "<td  width='25%' class='style10'>" + WebCommon.getGlobalResourceValue("DateCreated") + "</td><td  width='25%'>" + Convert.ToDateTime(lstVendor[0].DiscrepancyLogDate.ToString()).ToString("dd/MM/yyyy") + "</td></tr>" +
                     "<tr><td class='style10'>" + WebCommon.getGlobalResourceValue("Type") + "</td><td>" + this.GetType(discrepancyTypeID) + " </td>" +
                     "<td class='style10'>" + WebCommon.getGlobalResourceValue("CreatedBy") + "</td><td>" + lstVendor[0].Username.ToString().Replace("-", " ").ToString() + "</td></tr>" +
                     "<tr><td>&nbsp;</td><td>&nbsp; </td>" +
                     "<td class='style10'>" + WebCommon.getGlobalResourceValue("SentTo") + "</td><td>" + strCommunicationTo + "</td></tr>" +
                    "</table></td></tr> " +

                    "<tr><td><table width='100%' border='0'  BORDERCOLOR=black  >" +
                     "<tr><td width='25%' class='style10'>" + WebCommon.getGlobalResourceValue("Vendor") + " #</td><td width='25%'>" + lstVendor[0].Vendor.Vendor_No + " - " + lstVendor[0].Vendor.VendorName + "</td>" +
                     "<td width='25%' class='style10'>" + WebCommon.getGlobalResourceValue("StockPlanner") + "</td><td width='25%'>" + ActualStockPlannerNoValue + "</td></tr>" +
                     "<tr><td class='style10'>" + WebCommon.getGlobalResourceValue("ReturnsAddress") + "</td><td>" + lstVendor[0].Vendor.address1 + " " + lstVendor[0].Vendor.address2 + " " + lstVendor[0].Vendor.city + " " + lstVendor[0].Vendor.county + " </td>" +
                     "<td class='style10'>Tel #</td><td>" + ActualPlannerContactNoValue + "</td></tr>" +
                    "</table></td></tr> " + GetHtmlString(discrepancyTypeID, lstDetails) +
                     @"<tr><td> <br /><br /><br /></td></tr>	" +
                   "</table>");
                        sb.Append("<p style='page-break-before: always'></p>");
                    }
                }
                else
                {
                    if (discrepancyTypeID == 12)
                    {
                        sb.Append("<table width='100%' border='1' style=' border-collapse: collapse;'  BORDERCOLOR=black  > " +
                "<tr><td valign='top' ><div align='center' class='style1'>" + WebCommon.getGlobalResourceValue("Discrepancy") + " # - " + GetQueryStringValue("VDRNo") + " </div></td></tr> " +
               // "<tr><td valign='top'><div align='left' class='style1'>" + WebCommon.getGlobalResourceValue("Type") + " - " + this.GetType(discrepancyTypeID) + " </div></td></tr> " +
               "<tr><td valign='top'  align='center'><span align='center' class='style1'>" + WebCommon.getGlobalResourceValue("Location") + "- </span><span style='font-size:210%'>" + lstVendor[0].Location.ToString() + " </span></td></tr> " +
               "<tr><td valign='top'  align='center'><span align='center' class='style1'>" + WebCommon.getGlobalResourceValue("Vendor") + " # - </span><span style='font-size:210%'>" + lstVendor[0].Vendor.Vendor_No + " - " + lstVendor[0].Vendor.VendorName + " </span></td></tr> " +
                "<tr><td valign='top'  align='center'><span align='center' class='style1'>" + WebCommon.getGlobalResourceValue("PurchaseOrder") + " - </span><span style='font-size:210%'>" + Convert.ToString(lstVendor[0].PurchaseOrder.Purchase_order) + " </span></td></tr> " +
                "<tr><td valign='top'  align='center'><span align='center' class='style1'>" + WebCommon.getGlobalResourceValue("DeliveryNote") + " - </span><span style='font-size:210%'>" + lstVendor[0].DeliveryNoteNumber + " </span></td></tr> " +
                 "<tr><td><table width='100%' border='0'  BORDERCOLOR=black  >" +
                  "<tr><td class='style10' width='25%'>" + WebCommon.getGlobalResourceValue("Site") + "</td><td td width='25%'>" + lstVendor[0].Site.SiteName + "</td>" +
                  "<td  width='25%' class='style10'>" + WebCommon.getGlobalResourceValue("DateCreated") + "</td><td  width='25%'>" + Convert.ToDateTime(lstVendor[0].DiscrepancyLogDate.ToString()).ToString("dd/MM/yyyy") + "</td></tr>" +
                  "<tr><td class='style10'>" + WebCommon.getGlobalResourceValue("Type") + "</td><td>" + this.GetType(discrepancyTypeID) + " </td>" +
                  "<td class='style10'>" + WebCommon.getGlobalResourceValue("CreatedBy") + "</td><td>" + lstVendor[0].Username.ToString().Replace("-", " ").ToString() + "</td></tr>" +
                  "<tr><td>&nbsp;</td><td>&nbsp; </td>" +
                  "<td class='style10'>" + WebCommon.getGlobalResourceValue("SentTo") + "</td><td>" + strCommunicationTo + "</td></tr>" +
                 "</table></td></tr> " +

                 "<tr><td><table width='100%' border='0'  BORDERCOLOR=black  >" +
                  "<tr><td width='25%' class='style10'>" + WebCommon.getGlobalResourceValue("Vendor") + " #</td><td width='25%'>" + lstVendor[0].Vendor.Vendor_No + " - " + lstVendor[0].Vendor.VendorName + "</td>" +
                  "<td width='25%' class='style10'>" + WebCommon.getGlobalResourceValue("StockPlanner") + "</td><td width='25%'>" + lstVendor[0].StockPlannerName + "</td></tr>" +
                  "<tr><td class='style10'>" + WebCommon.getGlobalResourceValue("ReturnsAddress") + "</td><td>" + lstVendor[0].Vendor.address1 + " " + lstVendor[0].Vendor.address2 + " " + lstVendor[0].Vendor.city + " " + lstVendor[0].Vendor.county + " </td>" +
                  "<td class='style10'>Tel #</td><td>" + lstVendor[0].StockPlannerContact + "</td></tr>" +
                 "</table></td></tr> " + GetHtmlString(discrepancyTypeID, lstDetails) +
                 "<tr><td width ='100%' style='padding-top: 10px; padding-bottom: 10px'><b>Description Of Issue: </b>" + lstDetails[0].Comment + "</td></tr> " +                
                "</table>");
                        sb.Append("<p style='page-break-before: always'></p>");
                    }
                    else
                    {
                        sb.Append("<table width='100%' border='1' style=' border-collapse: collapse;'  BORDERCOLOR=black  > " +
               "<tr><td valign='top' ><div align='center' class='style1'>" + WebCommon.getGlobalResourceValue("Discrepancy") + " # - " + GetQueryStringValue("VDRNo") + " </div></td></tr> " +
              // "<tr><td valign='top'><div align='left' class='style1'>" + WebCommon.getGlobalResourceValue("Type") + " - " + this.GetType(discrepancyTypeID) + " </div></td></tr> " +
              "<tr><td valign='top'  align='center'><span align='center' class='style1'>" + WebCommon.getGlobalResourceValue("Location") + "- </span><span style='font-size:210%'>" + lstVendor[0].Location.ToString() + " </span></td></tr> " +
              "<tr><td valign='top'  align='center'><span align='center' class='style1'>" + WebCommon.getGlobalResourceValue("Vendor") + " # - </span><span style='font-size:210%'>" + lstVendor[0].Vendor.Vendor_No + " - " + lstVendor[0].Vendor.VendorName + " </span></td></tr> " +
               "<tr><td valign='top'  align='center'><span align='center' class='style1'>" + WebCommon.getGlobalResourceValue("PurchaseOrder") + " - </span><span style='font-size:210%'>" + Convert.ToString(lstVendor[0].PurchaseOrder.Purchase_order) + " </span></td></tr> " +
               "<tr><td valign='top'  align='center'><span align='center' class='style1'>" + WebCommon.getGlobalResourceValue("DeliveryNote") + " - </span><span style='font-size:210%'>" + lstVendor[0].DeliveryNoteNumber + " </span></td></tr> " +
                "<tr><td><table width='100%' border='0'  BORDERCOLOR=black  >" +
                 "<tr><td class='style10' width='25%'>" + WebCommon.getGlobalResourceValue("Site") + "</td><td td width='25%'>" + lstVendor[0].Site.SiteName + "</td>" +
                 "<td  width='25%' class='style10'>" + WebCommon.getGlobalResourceValue("DateCreated") + "</td><td  width='25%'>" + Convert.ToDateTime(lstVendor[0].DiscrepancyLogDate.ToString()).ToString("dd/MM/yyyy") + "</td></tr>" +
                 "<tr><td class='style10'>" + WebCommon.getGlobalResourceValue("Type") + "</td><td>" + this.GetType(discrepancyTypeID) + " </td>" +
                 "<td class='style10'>" + WebCommon.getGlobalResourceValue("CreatedBy") + "</td><td>" + lstVendor[0].Username.ToString().Replace("-", " ").ToString() + "</td></tr>" +
                 "<tr><td>&nbsp;</td><td>&nbsp; </td>" +
                 "<td class='style10'>" + WebCommon.getGlobalResourceValue("SentTo") + "</td><td>" + strCommunicationTo + "</td></tr>" +
                "</table></td></tr> " +

                "<tr><td><table width='100%' border='0'  BORDERCOLOR=black  >" +
                 "<tr><td width='25%' class='style10'>" + WebCommon.getGlobalResourceValue("Vendor") + " #</td><td width='25%'>" + lstVendor[0].Vendor.Vendor_No + " - " + lstVendor[0].Vendor.VendorName + "</td>" +
                 "<td width='25%' class='style10'>" + WebCommon.getGlobalResourceValue("StockPlanner") + "</td><td width='25%'>" + lstVendor[0].StockPlannerName + "</td></tr>" +
                 "<tr><td class='style10'>" + WebCommon.getGlobalResourceValue("ReturnsAddress") + "</td><td>" + lstVendor[0].Vendor.address1 + " " + lstVendor[0].Vendor.address2 + " " + lstVendor[0].Vendor.city + " " + lstVendor[0].Vendor.county + " </td>" +
                 "<td class='style10'>Tel #</td><td>" + lstVendor[0].StockPlannerContact + "</td></tr>" +
                "</table></td></tr> " + GetHtmlString(discrepancyTypeID, lstDetails) +              
                 @"<tr><td> <br /><br /><br /></td></tr>" +
               "</table>");
                        sb.Append("<p style='page-break-before: always'></p>");
                    }
                }
            }
            else
            {
                for (int iCount = 1; iCount <= Convert.ToInt32(txtLablesNumber.Text.Trim()); iCount++)
                {
                    string ReturnAddress = lstVendor[0].Vendor.ReturnAddress.Replace("\r\n", "</span></td></tr><tr><td valign='top'><span class='style10'>");
                    int discrepancyTypeID = lstVendor[0].DiscrepancyTypeID != null ? Convert.ToInt32(lstVendor[0].DiscrepancyTypeID) : 0;

                    List<DiscrepancyBE> lstDetails = oSendCommunicationCommon.getDiscrepancyItemDetail("GetDiscrepancyItem", Convert.ToInt32(GetQueryStringValue("DiscrepancyLogID")), this.GetType(discrepancyTypeID));

                    DiscrepancyBE DiscrepancyBE = new DiscrepancyBE();
                    DiscrepancyBAL DiscrepancyBAL = new DiscrepancyBAL();

                    DiscrepancyBE.Action = "GetActualStockPlannerDetailsBySPId"; // getting details of SP while discrepancy is logged

                    DiscrepancyBE.StockPlannerID = Convert.ToInt32(lstDetails[0].ActualStockPlannerID);

                    List<DiscrepancyBE> lstActualStockPlannerDetailsBySPId = DiscrepancyBAL.GetActualStockPlannerDetailsBAL(DiscrepancyBE);

                    if (lstActualStockPlannerDetailsBySPId.Count > 0)
                    {
                        ActualStockPlannerNoValue = lstActualStockPlannerDetailsBySPId[0].StockPlannerNO.ToString();
                        ActualPlannerContactNoValue = lstActualStockPlannerDetailsBySPId[0].StockPlannerContact.ToString();
                    }

                    if (!string.IsNullOrEmpty(ActualStockPlannerNoValue))
                    {
                        if (discrepancyTypeID == 12)
                        {

                            sb.Append("<table width='100%' border='1' style=' border-collapse: collapse;'  BORDERCOLOR=black  > " +
                    "<tr><td valign='top' ><div align='center' class='style1'>" + WebCommon.getGlobalResourceValue("Discrepancy") + " # - " + GetQueryStringValue("VDRNo") + " </div></td></tr> " +
                   // "<tr><td valign='top'><div align='left' class='style1'>" + WebCommon.getGlobalResourceValue("Type") + " - " + this.GetType(discrepancyTypeID) + " </div></td></tr> " +
                   "<tr><td valign='top' align='center' ><span class='style1'>" + WebCommon.getGlobalResourceValue("Location") + " - </span><span style='font-size:210%'>" + lstVendor[0].Location.ToString() + "</span></td></tr> " +
                   "<tr><td valign='top' align='center'><span align='center' class='style1'>" + WebCommon.getGlobalResourceValue("Vendor") + " # - </span><span style='font-size:210%'>" + lstVendor[0].Vendor.Vendor_No + " - " + lstVendor[0].Vendor.VendorName + " </span></td></tr> " +
                    "<tr><td valign='top' align='center'><span align='center' class='style1'>" + WebCommon.getGlobalResourceValue("PurchaseOrder") + " - </span><span style='font-size:210%'>" + Convert.ToString(lstVendor[0].PurchaseOrder.Purchase_order) + " </span></td></tr> " +
                    "<tr><td valign='top' align='center'><span align='center' class='style1'>" + WebCommon.getGlobalResourceValue("DeliveryNote") + " - </span><span style='font-size:210%'>" + lstVendor[0].DeliveryNoteNumber + " </span></td></tr> " +
                     "<tr><td><table width='100%' border='0'  BORDERCOLOR=black  >" +
                      "<tr><td class='style10' width='25%'>" + WebCommon.getGlobalResourceValue("Site") + "</td><td td width='25%'>" + lstVendor[0].Site.SiteName + "</td>" +
                      "<td  width='25%' class='style10'>" + WebCommon.getGlobalResourceValue("DateCreated") + "</td><td  width='25%'>" + Convert.ToDateTime(lstVendor[0].DiscrepancyLogDate.ToString()).ToString("dd/MM/yyyy") + "</td></tr>" +
                      "<tr><td class='style10'>" + WebCommon.getGlobalResourceValue("Type") + "</td><td>" + this.GetType(discrepancyTypeID) + " </td>" +
                      "<td class='style10'>" + WebCommon.getGlobalResourceValue("CreatedBy") + "</td><td>" + lstVendor[0].Username.ToString().Replace("-", " ").ToString() + "</td></tr>" +
                      "<tr><td>&nbsp;</td><td>&nbsp; </td>" +
                      "<td class='style10'>" + WebCommon.getGlobalResourceValue("SentTo") + "</td><td>" + strCommunicationTo + "</td></tr>" +
                     "</table></td></tr> " +

                     "<tr><td><table width='100%' border='0'  BORDERCOLOR=black  >" +
                      "<tr><td width='25%' class='style10'>" + WebCommon.getGlobalResourceValue("Vendor") + " #</td><td width='25%'>" + lstVendor[0].Vendor.Vendor_No + " - " + lstVendor[0].Vendor.VendorName + "</td>" +
                      "<td width='25%' class='style10'>" + WebCommon.getGlobalResourceValue("StockPlanner") + "</td><td width='25%'>" + ActualStockPlannerNoValue + "</td></tr>" +
                      "<tr><td class='style10'>" + WebCommon.getGlobalResourceValue("ReturnsAddress") + "</td><td>" + lstVendor[0].Vendor.address1 + " " + lstVendor[0].Vendor.address2 + " " + lstVendor[0].Vendor.city + " " + lstVendor[0].Vendor.county + " </td>" +
                      "<td class='style10'>" + WebCommon.getGlobalResourceValue("Tel") + " #</td><td>" + ActualPlannerContactNoValue + "</td></tr>" +
                     "</table></td></tr> " + GetHtmlString(discrepancyTypeID, lstDetails) +
                    "<tr><td width ='100%' style='padding-top: 10px; padding-bottom: 10px'><b>Description Of Issue: </b>" + lstDetails[0].Comment + "</td></tr> " +

                    /* "<tr><td>&nbsp;</td></tr> " +
                     "<tr><td>" + WebCommon.getGlobalResourceValue("PONumber") + " &nbsp;" + Convert.ToString(lstVendor[0].PurchaseOrder.Purchase_order) + "</td></tr>	" +
                     "<tr><td>" + WebCommon.getGlobalResourceValue("DateDiscrepancyRaised") + " &nbsp;" + Convert.ToString(lstVendor[0].DiscrepancyLogDate) + "</td></tr>	" +
                     "<tr><td><span class='style10' style='text-align:right;'>Box Number &nbsp;" + iCount + " of " + txtLablesNumber.Text.Trim() + "</span></td></tr>" +
                     "<tr><td>&nbsp;</td></tr> " +
                     "<tr><td valign='top'><span class='style10'>" + lstVendor[0].Vendor.Vendor_No + " - " + lstVendor[0].Vendor.VendorName + "</span></td> " +
                     "</tr> " +
                     "<tr><td valign='top'><span class='style10'>" + lstVendor[0].Vendor.address1 + "</span></td> " +
                     "</tr> " +
                     " <tr><td valign='top'><span class='style10'>" + lstVendor[0].Vendor.address2 + "</span></td> " +
                     " </tr> " +
                     "  <tr><td valign='top'><span class='style10'>" + lstVendor[0].Vendor.city + "</span></td> " +
                     "  </tr> " +
                     " <tr><td valign='top'><span class='style10'>" + lstVendor[0].Vendor.county + "</span></td> " +
                     " </tr> " +
                     " <tr><td valign='top'><span class='style10'>" + lstVendor[0].Vendor.VMPPIN + " " + lstVendor[0].Vendor.VMPPOU + "</span></td> " +
                     " </tr> " +
                     " <tr><td valign='top'><span class='style10'>" + lstVendor[0].Vendor.VMTCC1 + "</span></td> " +
                     "  </tr> " +
                     " <tr><td valign='top'><span class='style10'>" + lstVendor[0].Vendor.VMTEL1 + "</span></td> " +
                     " </tr> " +
                     "<tr><td>&nbsp;</td></tr>" +
                     "<tr><td valign='top'><div align='left' class='style1'>" + WebCommon.getGlobalResourceValue("ReturnsAddress") + " </div></td></tr> " +
                     " <tr><td valign='top'><span class='style10'>" + ReturnAddress + "</span></td></tr> " +*/

                    "</table>" );

                            if (iCount < Convert.ToInt32(txtLablesNumber.Text.Trim()))
                                sb.Append("<p style='page-break-before: always'></p>");
                        }
                        else
                        {
                            sb.Append("<table width='100%' border='1' style=' border-collapse: collapse;'  BORDERCOLOR=black  > " +
                    "<tr><td valign='top' ><div align='center' class='style1'>" + WebCommon.getGlobalResourceValue("Discrepancy") + " # - " + GetQueryStringValue("VDRNo") + " </div></td></tr> " +
                   // "<tr><td valign='top'><div align='left' class='style1'>" + WebCommon.getGlobalResourceValue("Type") + " - " + this.GetType(discrepancyTypeID) + " </div></td></tr> " +
                   "<tr><td valign='top' align='center' ><span class='style1'>" + WebCommon.getGlobalResourceValue("Location") + " - </span><span style='font-size:210%'>" + lstVendor[0].Location.ToString() + "</span></td></tr> " +
                   "<tr><td valign='top' align='center'><span align='center' class='style1'>" + WebCommon.getGlobalResourceValue("Vendor") + " # - </span><span style='font-size:210%'>" + lstVendor[0].Vendor.Vendor_No + " - " + lstVendor[0].Vendor.VendorName + " </span></td></tr> " +
                    "<tr><td valign='top' align='center'><span align='center' class='style1'>" + WebCommon.getGlobalResourceValue("PurchaseOrder") + " - </span><span style='font-size:210%'>" + Convert.ToString(lstVendor[0].PurchaseOrder.Purchase_order) + " </span></td></tr> " +
                    "<tr><td valign='top' align='center'><span align='center' class='style1'>" + WebCommon.getGlobalResourceValue("DeliveryNote") + " - </span><span style='font-size:210%'>" + lstVendor[0].DeliveryNoteNumber + " </span></td></tr> " +
                     "<tr><td><table width='100%' border='0'  BORDERCOLOR=black  >" +
                      "<tr><td class='style10' width='25%'>" + WebCommon.getGlobalResourceValue("Site") + "</td><td td width='25%'>" + lstVendor[0].Site.SiteName + "</td>" +
                      "<td  width='25%' class='style10'>" + WebCommon.getGlobalResourceValue("DateCreated") + "</td><td  width='25%'>" + Convert.ToDateTime(lstVendor[0].DiscrepancyLogDate.ToString()).ToString("dd/MM/yyyy") + "</td></tr>" +
                      "<tr><td class='style10'>" + WebCommon.getGlobalResourceValue("Type") + "</td><td>" + this.GetType(discrepancyTypeID) + " </td>" +
                      "<td class='style10'>" + WebCommon.getGlobalResourceValue("CreatedBy") + "</td><td>" + lstVendor[0].Username.ToString().Replace("-", " ").ToString() + "</td></tr>" +
                      "<tr><td>&nbsp;</td><td>&nbsp; </td>" +
                      "<td class='style10'>" + WebCommon.getGlobalResourceValue("SentTo") + "</td><td>" + strCommunicationTo + "</td></tr>" +
                     "</table></td></tr> " +

                     "<tr><td><table width='100%' border='0'  BORDERCOLOR=black  >" +
                      "<tr><td width='25%' class='style10'>" + WebCommon.getGlobalResourceValue("Vendor") + " #</td><td width='25%'>" + lstVendor[0].Vendor.Vendor_No + " - " + lstVendor[0].Vendor.VendorName + "</td>" +
                      "<td width='25%' class='style10'>" + WebCommon.getGlobalResourceValue("StockPlanner") + "</td><td width='25%'>" + ActualStockPlannerNoValue + "</td></tr>" +
                      "<tr><td class='style10'>" + WebCommon.getGlobalResourceValue("ReturnsAddress") + "</td><td>" + lstVendor[0].Vendor.address1 + " " + lstVendor[0].Vendor.address2 + " " + lstVendor[0].Vendor.city + " " + lstVendor[0].Vendor.county + " </td>" +
                      "<td class='style10'>" + WebCommon.getGlobalResourceValue("Tel") + " #</td><td>" + ActualPlannerContactNoValue + "</td></tr>" +
                     "</table></td></tr> " + GetHtmlString(discrepancyTypeID, lstDetails) +
                     @"<tr><td> <br /><br /><br /></td></tr>	" +
                    "</table>");

                            if (iCount < Convert.ToInt32(txtLablesNumber.Text.Trim()))
                                sb.Append("<p style='page-break-before: always'></p>");
                        }

                    }
                    else
                    {
                        if (discrepancyTypeID == 12)
                        {
                            sb.Append("<table width='100%' border='1' style=' border-collapse: collapse;'  BORDERCOLOR=black  > " +
             "<tr><td valign='top' ><div align='center' class='style1'>" + WebCommon.getGlobalResourceValue("Discrepancy") + " # - " + GetQueryStringValue("VDRNo") + " </div></td></tr> " +
            // "<tr><td valign='top'><div align='left' class='style1'>" + WebCommon.getGlobalResourceValue("Type") + " - " + this.GetType(discrepancyTypeID) + " </div></td></tr> " +
            "<tr><td valign='top' align='center' ><span class='style1'>" + WebCommon.getGlobalResourceValue("Location") + " - </span><span style='font-size:210%'>" + lstVendor[0].Location.ToString() + "</span></td></tr> " +
            "<tr><td valign='top' align='center'><span align='center' class='style1'>" + WebCommon.getGlobalResourceValue("Vendor") + " # - </span><span style='font-size:210%'>" + lstVendor[0].Vendor.Vendor_No + " - " + lstVendor[0].Vendor.VendorName + " </span></td></tr> " +
             "<tr><td valign='top' align='center'><span align='center' class='style1'>" + WebCommon.getGlobalResourceValue("PurchaseOrder") + " - </span><span style='font-size:210%'>" + Convert.ToString(lstVendor[0].PurchaseOrder.Purchase_order) + " </span></td></tr> " +
             "<tr><td valign='top' align='center'><span align='center' class='style1'>" + WebCommon.getGlobalResourceValue("DeliveryNote") + " - </span><span style='font-size:210%'>" + lstVendor[0].DeliveryNoteNumber + " </span></td></tr> " +
              "<tr><td><table width='100%' border='0'  BORDERCOLOR=black  >" +
               "<tr><td class='style10' width='25%'>" + WebCommon.getGlobalResourceValue("Site") + "</td><td td width='25%'>" + lstVendor[0].Site.SiteName + "</td>" +
               "<td  width='25%' class='style10'>" + WebCommon.getGlobalResourceValue("DateCreated") + "</td><td  width='25%'>" + Convert.ToDateTime(lstVendor[0].DiscrepancyLogDate.ToString()).ToString("dd/MM/yyyy") + "</td></tr>" +
               "<tr><td class='style10'>" + WebCommon.getGlobalResourceValue("Type") + "</td><td>" + this.GetType(discrepancyTypeID) + " </td>" +
               "<td class='style10'>" + WebCommon.getGlobalResourceValue("CreatedBy") + "</td><td>" + lstVendor[0].Username.ToString().Replace("-", " ").ToString() + "</td></tr>" +
               "<tr><td>&nbsp;</td><td>&nbsp; </td>" +
               "<td class='style10'>" + WebCommon.getGlobalResourceValue("SentTo") + "</td><td>" + strCommunicationTo + "</td></tr>" +
              "</table></td></tr> " +

              "<tr><td><table width='100%' border='0'  BORDERCOLOR=black  >" +
               "<tr><td width='25%' class='style10'>" + WebCommon.getGlobalResourceValue("Vendor") + " #</td><td width='25%'>" + lstVendor[0].Vendor.Vendor_No + " - " + lstVendor[0].Vendor.VendorName + "</td>" +
               "<td width='25%' class='style10'>" + WebCommon.getGlobalResourceValue("StockPlanner") + "</td><td width='25%'>" + lstVendor[0].StockPlannerName + "</td></tr>" +
               "<tr><td class='style10'>" + WebCommon.getGlobalResourceValue("ReturnsAddress") + "</td><td>" + lstVendor[0].Vendor.address1 + " " + lstVendor[0].Vendor.address2 + " " + lstVendor[0].Vendor.city + " " + lstVendor[0].Vendor.county + " </td>" +
               "<td class='style10'>" + WebCommon.getGlobalResourceValue("Tel") + " #</td><td>" + lstVendor[0].StockPlannerContact + "</td></tr>" +
              "</table></td></tr> " + GetHtmlString(discrepancyTypeID, lstDetails) +
                 "<tr><td width ='100%' style='padding-top: 10px; padding-bottom: 10px'><b>Description Of Issue: </b>" + lstDetails[0].Comment + "</td></tr> " +
             /* "<tr><td>&nbsp;</td></tr> " +
              "<tr><td>" + WebCommon.getGlobalResourceValue("PONumber") + " &nbsp;" + Convert.ToString(lstVendor[0].PurchaseOrder.Purchase_order) + "</td></tr>	" +
              "<tr><td>" + WebCommon.getGlobalResourceValue("DateDiscrepancyRaised") + " &nbsp;" + Convert.ToString(lstVendor[0].DiscrepancyLogDate) + "</td></tr>	" +
              "<tr><td><span class='style10' style='text-align:right;'>Box Number &nbsp;" + iCount + " of " + txtLablesNumber.Text.Trim() + "</span></td></tr>" +
              "<tr><td>&nbsp;</td></tr> " +
              "<tr><td valign='top'><span class='style10'>" + lstVendor[0].Vendor.Vendor_No + " - " + lstVendor[0].Vendor.VendorName + "</span></td> " +
              "</tr> " +
              "<tr><td valign='top'><span class='style10'>" + lstVendor[0].Vendor.address1 + "</span></td> " +
              "</tr> " +
              " <tr><td valign='top'><span class='style10'>" + lstVendor[0].Vendor.address2 + "</span></td> " +
              " </tr> " +
              "  <tr><td valign='top'><span class='style10'>" + lstVendor[0].Vendor.city + "</span></td> " +
              "  </tr> " +
              " <tr><td valign='top'><span class='style10'>" + lstVendor[0].Vendor.county + "</span></td> " +
              " </tr> " +
              " <tr><td valign='top'><span class='style10'>" + lstVendor[0].Vendor.VMPPIN + " " + lstVendor[0].Vendor.VMPPOU + "</span></td> " +
              " </tr> " +
              " <tr><td valign='top'><span class='style10'>" + lstVendor[0].Vendor.VMTCC1 + "</span></td> " +
              "  </tr> " +
              " <tr><td valign='top'><span class='style10'>" + lstVendor[0].Vendor.VMTEL1 + "</span></td> " +
              " </tr> " +
              "<tr><td>&nbsp;</td></tr>" +
              "<tr><td valign='top'><div align='left' class='style1'>" + WebCommon.getGlobalResourceValue("ReturnsAddress") + " </div></td></tr> " +
              " <tr><td valign='top'><span class='style10'>" + ReturnAddress + "</span></td></tr> " +*/

             "</table>" );

                            if (iCount < Convert.ToInt32(txtLablesNumber.Text.Trim()))
                                sb.Append("<p style='page-break-before: always'></p>");
                        }

                        else
                        {
                            sb.Append("<table width='100%' border='1' style=' border-collapse: collapse;'  BORDERCOLOR=black  > " +
            "<tr><td valign='top' ><div align='center' class='style1'>" + WebCommon.getGlobalResourceValue("Discrepancy") + " # - " + GetQueryStringValue("VDRNo") + " </div></td></tr> " +
           // "<tr><td valign='top'><div align='left' class='style1'>" + WebCommon.getGlobalResourceValue("Type") + " - " + this.GetType(discrepancyTypeID) + " </div></td></tr> " +
           "<tr><td valign='top' align='center' ><span class='style1'>" + WebCommon.getGlobalResourceValue("Location") + " - </span><span style='font-size:210%'>" + lstVendor[0].Location.ToString() + "</span></td></tr> " +
           "<tr><td valign='top' align='center'><span align='center' class='style1'>" + WebCommon.getGlobalResourceValue("Vendor") + " # - </span><span style='font-size:210%'>" + lstVendor[0].Vendor.Vendor_No + " - " + lstVendor[0].Vendor.VendorName + " </span></td></tr> " +
            "<tr><td valign='top' align='center'><span align='center' class='style1'>" + WebCommon.getGlobalResourceValue("PurchaseOrder") + " - </span><span style='font-size:210%'>" + Convert.ToString(lstVendor[0].PurchaseOrder.Purchase_order) + " </span></td></tr> " +
            "<tr><td valign='top' align='center'><span align='center' class='style1'>" + WebCommon.getGlobalResourceValue("DeliveryNote") + " - </span><span style='font-size:210%'>" + lstVendor[0].DeliveryNoteNumber + " </span></td></tr> " +
             "<tr><td><table width='100%' border='0'  BORDERCOLOR=black  >" +
              "<tr><td class='style10' width='25%'>" + WebCommon.getGlobalResourceValue("Site") + "</td><td td width='25%'>" + lstVendor[0].Site.SiteName + "</td>" +
              "<td  width='25%' class='style10'>" + WebCommon.getGlobalResourceValue("DateCreated") + "</td><td  width='25%'>" + Convert.ToDateTime(lstVendor[0].DiscrepancyLogDate.ToString()).ToString("dd/MM/yyyy") + "</td></tr>" +
              "<tr><td class='style10'>" + WebCommon.getGlobalResourceValue("Type") + "</td><td>" + this.GetType(discrepancyTypeID) + " </td>" +
              "<td class='style10'>" + WebCommon.getGlobalResourceValue("CreatedBy") + "</td><td>" + lstVendor[0].Username.ToString().Replace("-", " ").ToString() + "</td></tr>" +
              "<tr><td>&nbsp;</td><td>&nbsp; </td>" +
              "<td class='style10'>" + WebCommon.getGlobalResourceValue("SentTo") + "</td><td>" + strCommunicationTo + "</td></tr>" +
             "</table></td></tr> " +

             "<tr><td><table width='100%' border='0'  BORDERCOLOR=black  >" +
              "<tr><td width='25%' class='style10'>" + WebCommon.getGlobalResourceValue("Vendor") + " #</td><td width='25%'>" + lstVendor[0].Vendor.Vendor_No + " - " + lstVendor[0].Vendor.VendorName + "</td>" +
              "<td width='25%' class='style10'>" + WebCommon.getGlobalResourceValue("StockPlanner") + "</td><td width='25%'>" + lstVendor[0].StockPlannerName + "</td></tr>" +
              "<tr><td class='style10'>" + WebCommon.getGlobalResourceValue("ReturnsAddress") + "</td><td>" + lstVendor[0].Vendor.address1 + " " + lstVendor[0].Vendor.address2 + " " + lstVendor[0].Vendor.city + " " + lstVendor[0].Vendor.county + " </td>" +
              "<td class='style10'>" + WebCommon.getGlobalResourceValue("Tel") + " #</td><td>" + lstVendor[0].StockPlannerContact + "</td></tr>" +
             "</table></td></tr> " + GetHtmlString(discrepancyTypeID, lstDetails) +
             @"<tr><td> <br /><br /><br /></td></tr>	" +
             "</table>");

                            if (iCount < Convert.ToInt32(txtLablesNumber.Text.Trim()))
                                sb.Append("<p style='page-break-before: always'></p>");
                        }
                    }
                }
            }
        }

        ltlable.Text = sb.ToString();
        Session["LabelHTML"] = sb.ToString();

        var winOpen = string.Empty;
        if (GetQueryStringValue("SiteId") != null && GetQueryStringValue("DisDate") != null)
        {
            winOpen = "window.open('DIS_PrintLabel.aspx?ltid=" + ltlable.ClientID
                + "&FromPage=images&SiteId=" + GetQueryStringValue("SiteId") + "&DisDate=" + GetQueryStringValue("DisDate")
                + "&AppPath=" + hdnApplicationPath.ClientID + "', '', 'left =' + screen.width + ',top=' + screen.height + ',width=0,height=0,toolbar=0,scrollbars=0,status=0')";
        }
        else
        {
            if (GetQueryStringValue("Mode") == "Edit")
            {
                winOpen = "window.open('DIS_PrintLabel.aspx?ltid=" + ltlable.ClientID
                    + "&redirect=true&AppPath=" + hdnApplicationPath.ClientID + "', '', 'left =' + screen.width + ',top=' + screen.height + ',width=0,height=0,toolbar=0,scrollbars=0,status=0')";
            }
            else
            {
                winOpen = "window.open('DIS_PrintLabel.aspx?ltid=" + ltlable.ClientID
                                   + "&redirect=false&AppPath=" + hdnApplicationPath.ClientID + "', '', 'left =' + screen.width + ',top=' + screen.height + ',width=0,height=0,toolbar=0,scrollbars=0,status=0')";
            }
        }

        //string a = "window.open('DIS_PrintLabel.aspx?ltid=" + ltlable.ClientID 
        //    + "&AppPath=" + hdnApplicationPath.ClientID + "', '', 'left =' + screen.width + ',top=' + screen.height + ',width=0,height=0,toolbar=0,scrollbars=0,status=0')";
        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "pp", winOpen, true);
    }
    protected string GetHtmlString(int discrepancyType,List<DiscrepancyBE> lstDetails)
    {
        string OurCode = WebCommon.getGlobalResourceValue("OurCode");
        string Description = WebCommon.getGlobalResourceValue("Description1");
        string VendorItemCode = WebCommon.getGlobalResourceValue("VendorItemCode");
        string  OutstandingPOQty = WebCommon.getGlobalResourceValue("OutstandingPOQty");
        //string OriginalPOQty = WebCommon.getGlobalResourceValue("OriginalPOQty");
        string DeliveryNoteQty = WebCommon.getGlobalResourceValue("DeliveryNoteQty");
        string DeliveredQty = WebCommon.getGlobalResourceValue("DeliveredQty");
        string QtyOverDelivered = WebCommon.getGlobalResourceValue("QtyOverDelivered");
        string ReceivedQuantity = WebCommon.getGlobalResourceValue("ReceivedQuantity1");
        string ShortageQuantity = WebCommon.getGlobalResourceValue("ShortageQuantity");
        string DamagedQuantity = WebCommon.getGlobalResourceValue("DamagedQuantity");
        string DamagedDescription = WebCommon.getGlobalResourceValue("DamagedDescription");
        string RequiredCode = WebCommon.getGlobalResourceValue("RequiredCode");
        string UOM = WebCommon.getGlobalResourceValue("UOM");
        string CodeReceived = WebCommon.getGlobalResourceValue("CodeReceived");
        string QuantityAdvisedOnDNote = WebCommon.getGlobalResourceValue("QuantityAdvisedOnDNote");
        string QuantityPaperworkAmendedto = WebCommon.getGlobalResourceValue("QuantityPaperworkAmendedto");
        string POPackSize = WebCommon.getGlobalResourceValue("POPackSize");
        string PackSizeofgooddelivered = WebCommon.getGlobalResourceValue("PackSizeofgooddelivered");
        string strLine = WebCommon.getGlobalResourceValue("Line");
        string strODCode = WebCommon.getGlobalResourceValue("ODCode");
        string strVikingCode = WebCommon.getGlobalResourceValue("VikingCode");
        string strOriginalPOQty = WebCommon.getGlobalResourceValue("OriginalPOQty");
        string strType = WebCommon.getGlobalResourceValue("Type");
        string  strDiscrepancyQty = WebCommon.getGlobalResourceValue("DiscrepancyQty");
        string strHi = WebCommon.getGlobalResourceValue("Hi");
        string strShuttleDiscMessage = WebCommon.getGlobalResourceValue("ShuttleDiscMessage");
        string strShortage = WebCommon.getGlobalResourceValue("Shortage");
        string strManyThanks = WebCommon.getGlobalResourceValue("ManyThanks");
        string ChaseQuantity = WebCommon.getGlobalResourceValue("ChaseQuantity");
        string Comments = WebCommon.getGlobalResourceValue("Comments");
        string NoofPacksReceived = WebCommon.getGlobalResourceValue("NoofPacksReceived");
        string Suppliercode = WebCommon.getGlobalResourceValue("Suppliercode");
        StringBuilder sProductDetail = new StringBuilder();
         if (lstDetails != null && lstDetails.Count > 0)
         {
             sProductDetail.Append("<tr><td ><table width='100%' border='1' style=' border-collapse: collapse;'  BORDERCOLOR=black >");
             switch (discrepancyType)
             {

                 case 1:
                     sProductDetail.Append("<tr><td width='15%' class='style10' style='font size:11px'>" + OurCode + "</td><td width='35%' class='style10' style='font size:11px'>" + Description + "</td><td width='10%' class='style10' style='font size:11px'>" + VendorItemCode + "</td><td  width='10%' class='style10' style='font size:11px'>" + OutstandingPOQty + "</td><td  width='10%' class='style10' style='font size:11px' >" + DeliveryNoteQty + "</td><td  width='10%' class='style10' style='font size:11px'>" + DeliveredQty + "</td><td  width='10%' class='style10' style='font size:11px'>" + QtyOverDelivered + "</td></tr>");
                     for (int i = 0; i < lstDetails.Count; i++)
                     {
                         sProductDetail.Append("<tr><td style='font size:9px'>" + lstDetails[i].ODSKUCode + "</td><td style='font size:9px'>" + lstDetails[i].ProductDescription + "</td><td style='font size:9px'>" + lstDetails[i].VendorCode);
                         sProductDetail.Append("</td><td style='font size:9px'>" + lstDetails[i].OutstandingQuantity + "</td><td style='font size:9px'>" + lstDetails[i].DeliveredNoteQuantity + "</td><td style='font size:9px'>" + lstDetails[i].DeliveredQuantity + "</td>");
                         sProductDetail.Append("<td style='font size:9px'>" + lstDetails[i].OversQuantity + "</td></tr>");
                     }
                     //sDiscrepancyRelatedtext = "Product received against the above delivery were over delivered against our Purchase Order as detailed below";
                     //sLine1 = "Please contact the undersigned urgently so that resolution on this issue can be reached.<br /><br />In all communication please ensure that you quote the Discrepancy Report Number.";                    
                     break;
                 case 2:
                     sProductDetail.Append("<tr><td width='15%' class='style10'>" + OurCode + "</td><td width='35%' class='style10'> " + Description + "</td><td width='10%' class='style10'>" + VendorItemCode + "</td><td  width='10%' class='style10'>" + OutstandingPOQty + "</td><td  width='10%' class='style10'>" + DeliveryNoteQty + "</td><td  width='10%' class='style10'>" + ReceivedQuantity + "</td><td  width='10%' class='style10'>" + ShortageQuantity + "</td></tr>");
                     for (int i = 0; i < lstDetails.Count; i++)
                     {
                         decimal dReceivedQuantity = (lstDetails[i].DeliveredNoteQuantity ?? 0) - (lstDetails[i].ShortageQuantity ?? 0);
                         sProductDetail.Append("<tr><td>" + lstDetails[i].ODSKUCode + "</td><td>" + lstDetails[i].ProductDescription + "</td><td>" + lstDetails[i].VendorCode);
                         sProductDetail.Append("</td><td>" + lstDetails[i].OutstandingQuantity + "</td><td>" + lstDetails[i].DeliveredNoteQuantity + "</td><td>" + dReceivedQuantity + "</td><td>" + lstDetails[i].ShortageQuantity + "</td></tr>");
                     }
                     //sDiscrepancyRelatedtext = "Product received against the above delivery note were short delivered as per the following details.";
                     //sLine1 = "In this first instance please contact the undersigned urgently so that they can advise as to whether the short delivered quantities are still required.<br /><br />Please ensure that you quote the Discrepancy Report Number in all communication relating to this issue.";
                     break;
                 case 3:
                     sProductDetail.Append("<tr><td width='15%' class='style10'>" + OurCode + "</td><td width='35%' class='style10'>" + Description + "</td><td width='10%' class='style10'>" + VendorItemCode + "</td><td  width='10%' class='style10'>" + OutstandingPOQty + "</td><td  width='10%' class='style10'>" + DamagedQuantity + "</td><td  width='10%' class='style10'>" + DamagedDescription + "</td></tr>");
                     for (int i = 0; i < lstDetails.Count; i++)
                     {
                         sProductDetail.Append("<tr><td>" + lstDetails[i].ODSKUCode + "</td><td>" + lstDetails[i].ProductDescription + "</td><td>" + lstDetails[i].VendorCode);
                         sProductDetail.Append("</td><td>" + lstDetails[i].OutstandingQuantity + "</td><td>" + lstDetails[i].DamageQuantity + "</td><td>" + lstDetails[i].DamageDescription + "</td></tr>");
                     }
                     //sDiscrepancyRelatedtext = "Product received against the above delivery note were damaged as detailed below.";
                     //sLine1 = "In this first instance please contact the undersigned urgently so that they can advise as to whether the damaged quantities are still required.<br /><br />Please ensure that you quote the Discrepancy Report Number in all communication relating to this issue.";                    
                     break;
                 case 4:
                     sProductDetail.Append("<tr><td width='15%' class='style10'>" + OurCode + "</td><td width='35%' class='style10'>" + Description + "</td><td width='10%' class='style10'>" + VendorItemCode + "</td><td  width='10%' class='style10'>" + ReceivedQuantity + "</td></tr>");
                     for (int i = 0; i < lstDetails.Count; i++)
                     {
                         sProductDetail.Append("<tr><td>" + lstDetails[i].ODSKUCode + "</td><td>" + lstDetails[i].ProductDescription + "</td><td>" + lstDetails[i].VendorCode);
                         sProductDetail.Append("</td><td>" + lstDetails[i].DeliveredQuantity + "</td></tr>");
                     }
                     //sDiscrepancyRelatedtext = "The above delivery did not quote the Office Depot Purchase Order on the documentation." + "<br /><br /> We are unable to receipt stock without a valid Purchase Order Number. Please contact the undersigned <u><b>urgently</b></u> so that we can confirm the correct Purchase Order Number.<br /><br />Please ensure that the invoice relating to this delivery quotes our Purchase Order Number and that when referencing this discrepancy please ensure that the Discrepancy number is quoted" + "<br /><br />Please find detail of what was received catalogued below<br /><br />";
                     break;
                 case 6:
                     sProductDetail.Append("<tr><td width='10%'  class='style10'>" + RequiredCode + "</td><td width='20%'  class='style10'>" + Description + "</td><td width='10%'  class='style10'>" + VendorItemCode + "</td><td  width='10%'  class='style10'>" + UOM + "</td><td  width='10%'  class='style10'>" + CodeReceived + "</td><td  width='10%'  class='style10'>" + Description + "</td><td width='10%'  class='style10'>" + VendorItemCode + "</td><td  width='10%'  class='style10'>" + ReceivedQuantity + "</td><td width='10%'  class='style10'>" + UOM + "</td></tr>");
                     for (int i = 0; i < lstDetails.Count; i++)
                     {
                         sProductDetail.Append("<tr><td>" + lstDetails[i].ODSKUCode + "</td><td>" + lstDetails[i].ProductDescription + "</td><td>" + lstDetails[i].VendorCode + "</td><td>" + lstDetails[i].UOM + "</td><td>" + lstDetails[i].CorrectODSKUCode + "</td><td>" + lstDetails[i].CorrectProductDescription + "</td><td>" + lstDetails[i].CorrectVendorCode + "</td><td>" + lstDetails[i].DeliveredQuantity + "</td><td>" + lstDetails[i].CorrectUOM + "</td></tr>");
                     }
                     // sDiscrepancyRelatedtext = "The above delivery contained incorrect products as per our Purchase Order Number, details as follows:";
                     //sLine1 = "Please contact the undersigned urgently so that resolution on this issue can be reached.<br /><br />In all communication please ensure that you quote the Discrepancy Report Number.";                    
                     break;
                 case 9:
                     //if (getCommunicationWithEscalation(lstDisLog[0].SiteID))
                     //{
                     //    // This is for escalation    
                     //    sProductDetail.Append("<tr style='color:#934500;border-bottom: 1px solid #E8E8E8;font-weight: bold;font-family:Arial ;font-size:12;vertical-align: top;'><td width='10%'>" + OurCode + "</td><td width='35%'>" + Description + "</td><td width='10%'>" + VendorItemCode + "</td><td  width='10%'>" + QuantityAdvisedOnDNote + "</td><td  width='10%'>" + QuantityPaperworkAmendedto + "</td></tr>");
                     //    for (int i = 0; i < lstDetails.Count; i++)
                     //    {
                     //        sProductDetail.Append("<tr style='background-color: #F7F6F3;font-size:12;font-family:Arial ;'><td>" + lstDetails[i].ODSKUCode + "</td><td>" + lstDetails[i].ProductDescription + "</td><td>" + lstDetails[i].VendorCode + "</td><td>" + lstDetails[i].DeliveredNoteQuantity + "</td><td>" + lstDetails[i].AmendedQuantity + "</td></tr>");
                     //    }
                     //}
                     //else
                     //{
                         // This is no escalation
                     sProductDetail.Append("<tr><td width='10%' class='style10'>" + OurCode + "</td><td width='35%' class='style10'>" + Description + "</td><td width='10%' class='style10'>" + VendorItemCode + "</td><td  width='10%' class='style10'>" + QuantityAdvisedOnDNote + "</td><td  width='10%' class='style10'>" + QuantityPaperworkAmendedto + "</td></tr>");
                         for (int i = 0; i < lstDetails.Count; i++)
                         {
                             sProductDetail.Append("<tr><td>" + lstDetails[i].ODSKUCode + "</td><td>" + lstDetails[i].ProductDescription + "</td><td>" + lstDetails[i].VendorCode + "</td><td>" + lstDetails[i].DeliveredNoteQuantity + "</td><td>" + lstDetails[i].AmendedQuantity + "</td></tr>");
                         }
                         //sDiscrepancyRelatedtext = "The above delivery paperwork was amended. Please find details catalogued below.";
                         //sLine1 = "Please ensure that the associated invoice is for the amended quantity, as this is what was physically received and receipted.";
                    // }
                     break;
                 case 10:
                     sProductDetail.Append("<tr><td width='15%' class='style10'>" + OurCode + "</td><td width='40%' class='style10'>" + Description + "</td><td width='15%' class='style10'>" + VendorItemCode + "</td><td  width='15%' class='style10'>" + POPackSize + "</td><td  width='15%' class='style10'>" + WebCommon.getGlobalResourceValue("PackSizeofgooddeliveredNew") + "</td><td  width='15%' class='style10'>" + NoofPacksReceived + "</td><td  width='15%' class='style10'>" + WebCommon.getGlobalResourceValue("#Units") + "</td></tr>");
                     for (int i = 0; i < lstDetails.Count; i++)
                     {
                         sProductDetail.Append("<tr><td>" + lstDetails[i].ODSKUCode + "</td><td>" + lstDetails[i].ProductDescription + "</td><td>" + lstDetails[i].VendorCode + "</td><td>" + lstDetails[i].PackSizeOrdered + "</td><td>" + lstDetails[i].PackSizeReceived + "</td><td>" + lstDetails[i].WrongPackReceived + "</td><td>" + Convert.ToInt32(lstDetails[i].PackSizeReceived) * lstDetails[i].WrongPackReceived + "</td></tr>");
                     }
                     //sDiscrepancyRelatedtext = "The above delivery contained different pack sizes from those shown on our purchase order as detailed below.";
                     //sLine1 = "Please contact the undersigned to discuss these differences, in order that we may amend our records if necessary.";
                     break;
                 case 5:
                     sProductDetail.Append("<tr><td width='15%' class='style10'>" + OurCode + "</td><td width='40%' class='style10'>" + Description + "</td><td width='15%' class='style10'>" + VendorItemCode + "</td><td  width='15%' class='style10'>" + ReceivedQuantity + "</td></tr>");
                     for (int i = 0; i < lstDetails.Count; i++)
                     {
                         sProductDetail.Append("<tr><td>" + lstDetails[i].ODSKUCode + "</td><td>" + lstDetails[i].ProductDescription + "</td><td>" + lstDetails[i].VendorCode + "</td><td>" + lstDetails[i].DeliveredQuantity + "</td></tr>");
                     }
                     break;
                 case 13: //invoice
                     sProductDetail.Append("<tr><td width='15%' class='style10'>" + Comments + "</td><td>" + lstDetails[0].Comment + "</td></tr>");
                     //sProductDetail.Append("<tr><td width='15%' class='style10'>" + OurCode + "</td><td width='40%' class='style10'>" + Description + "</td><td width='15%' class='style10'>" + VendorItemCode + "</td><td  width='15%' class='style10'>" + ChaseQuantity + "</td></tr>");
                     //for (int i = 0; i < lstDetails.Count; i++)
                     //{
                     //    sProductDetail.Append("<tr><td>" + lstDetails[i].ODSKUCode + "</td><td>" + lstDetails[i].ProductDescription + "</td><td>" + lstDetails[i].VendorCode + "</td><td>" + lstDetails[i].ChaseQuantity + "</td></tr>");
                     //}
                     break;
                 case 15:
                     sProductDetail.Append("<tr><td width='5%' class='style10'>"
                         + strLine + "</td><td width='10%' class='style10'>" + strODCode
                         + "</td><td width='10%' class='style10'>" + strVikingCode + "</td><td width='30%' class='style10'>" + Description + "</td><td width='10%' class='style10'>"
                         + strOriginalPOQty + "</td><td  width='10%' class='style10'>" + OutstandingPOQty + "</td><td  width='5%' class='style10'>" + UOM
                         + "</td><td  width='15%' class='style10'>" + strType + "</td><td  width='10%' class='style10'>" + strDiscrepancyQty + "</td></tr>");
                     for (int i = 0; i < lstDetails.Count; i++)
                     {
                         sProductDetail.Append("<tr><td>"
                             + lstDetails[i].Line_no + "</td><td>" + lstDetails[i].ODSKUCode + "</td><td>" + lstDetails[i].DirectCode);
                         sProductDetail.Append("</td><td>" + lstDetails[i].ProductDescription + "</td><td>" + lstDetails[i].OriginalQuantity + "</td><td>"
                             + lstDetails[i].OutstandingQuantity + "</td><td>" + lstDetails[i].UOM + "</td><td>" + lstDetails[i].ShuttleType
                             + "</td><td>" + lstDetails[i].DiscrepancyQty + "</td>");
                         sProductDetail.Append("</tr>");
                     }
                     break;
                 case 14:
                     sProductDetail.Append("<tr><td width='15%' class='style10'>"+Comments+"</td><td>"+lstDetails[0].Comment+"</td></tr>");
                     break;
                 case 7:
                     sProductDetail.Append("<tr><td width='15%' class='style10'>" + Comments + "</td><td>" + lstDetails[0].Comment + "</td></tr>");
                     break;
                 case 8:
                     sProductDetail.Append("<tr><td width='15%' class='style10'>" + Comments + "</td><td>" + lstDetails[0].Comment + "</td></tr>");
                     break;
                 case 11:
                     sProductDetail.Append("<tr><td width='15%' class='style10'>" + Comments + "</td><td>" + lstDetails[0].Comment + "</td></tr>");
                     break;
                 case 12:
                     if (string.IsNullOrEmpty(Convert.ToString(lstDetails[0].OriginalQuantity)))
                     {
                         sProductDetail.Append("<tr><td width='15%' class='style10'>" + Comments + "</td><td>" + lstDetails[0].Comment + "</td></tr>");
                     }
                     else
                     {
                         sProductDetail.Append("<tr><td width='15%' class='style10' style='font size:11px'>" + OurCode + "</td><td width='35%' class='style10' style='font size:11px'>" + Description + "</td><td width='10%' class='style10' style='font size:11px'>" + VendorItemCode + "</td><td  width='10%' class='style10' style='font size:11px'>" + OutstandingPOQty + "</td><td  width='10%' class='style10' style='font size:11px' >" + strOriginalPOQty + "</td><td  width='10%' class='style10' style='font size:11px'>" + UOM + "</td>"

                             + "</td><td  width='10%' class='style10' style='font size:11px'>" + DeliveredQty + "</td></tr>");
                         for (int i = 0; i < lstDetails.Count; i++)
                         {
                             sProductDetail.Append("<tr><td style='font size:9px'>" + lstDetails[i].ODSKUCode + "</td><td style='font size:9px'>" + lstDetails[i].ProductDescription + "</td><td style='font size:9px'>" + lstDetails[i].VendorCode);
                             sProductDetail.Append("</td><td style='font size:9px'>" + lstDetails[i].OutstandingQuantity + "</td><td style='font size:9px'>" + lstDetails[i].OriginalQuantity + "</td><td style='font size:9px'>" + lstDetails[i].UOM + "</td><td style='font size:9px'>" + lstDetails[i].DeliveredQuantity + "</td></tr>");
                         }
                     }
                     break;
                 case 16:
                     sProductDetail.Append("<tr><td width='15%' class='style10'>" + OurCode + "</td><td width='40%' class='style10'>" + Description + "</td><td width='15%' class='style10'>" + VendorItemCode + "</td><td  width='15%' class='style10'>" + ReceivedQuantity + "</td></tr>");
                     for (int i = 0; i < lstDetails.Count; i++)
                     {
                         sProductDetail.Append("<tr><td>" + lstDetails[i].ODSKUCode + "</td><td>" + lstDetails[i].ProductDescription + "</td><td>" + lstDetails[i].VendorCode + "</td><td>" + lstDetails[i].DeliveredQuantity + "</td></tr>");
                     }
                     break;
                 case 17:
                     sProductDetail.Append("<tr><td width='15%' class='style10'>" + OurCode + "</td><td width='40%' class='style10'>" + Description + "</td><td width='15%' class='style10'>" + VendorItemCode + "</td><td  width='10%' class='style10' style='font size:11px'>" + OutstandingPOQty + "</td><td  width='15%' class='style10'>" + ChaseQuantity + "</td></tr>");
                     for (int i = 0; i < lstDetails.Count; i++)
                     {
                         sProductDetail.Append("<tr><td>" + lstDetails[i].ODSKUCode + "</td><td>" + lstDetails[i].ProductDescription + "</td><td>" + lstDetails[i].VendorCode + "</td><td>" + lstDetails[i].OutstandingQuantity + "</td><td>" + lstDetails[i].ChaseQuantity + "</td></tr>");
                     }
                     break;

                 case 18:
                     sProductDetail.Append("<tr><td width='15%' class='style10'>" + OurCode + "</td><td width='40%' class='style10'>" + Description + "</td><td width='15%' class='style10'>" + VendorItemCode + "</td><td  width='10%' class='style10' style='font size:11px'>" + OutstandingPOQty + "</td><td  width='15%' class='style10'>" + ChaseQuantity + "</td></tr>");
                     for (int i = 0; i < lstDetails.Count; i++)
                     {
                         sProductDetail.Append("<tr><td>" + lstDetails[i].ODSKUCode + "</td><td>" + lstDetails[i].ProductDescription + "</td><td>" + lstDetails[i].VendorCode + "</td><td>" + lstDetails[i].OutstandingQuantity + "</td><td>" + lstDetails[i].ChaseQuantity + "</td></tr>");
                     }
                     break;

                 case 19:
                     sProductDetail.Append("<tr><td width='15%' class='style10'>" + OurCode + "</td><td width='40%' class='style10'>" + Description + "</td><td width='15%' class='style10'>" + VendorItemCode + "</td><td  width='10%' class='style10' style='font size:11px'>" + WebCommon.getGlobalResourceValue("ReceiptedQty") + "</td><td  width='15%' class='style10'>" + WebCommon.getGlobalResourceValue("InvoiceQTY") + "</td></tr>");
                     for (int i = 0; i < lstDetails.Count; i++)
                     {
                         sProductDetail.Append("<tr><td>" + lstDetails[i].ODSKUCode + "</td><td>" + lstDetails[i].ProductDescription + "</td><td>" + lstDetails[i].VendorCode + "</td><td>" + lstDetails[i].ReceiptedQty + "</td><td>" + lstDetails[i].InvoiceQty + "</td></tr>");
                     }
                     break;
                case 20:
                    sProductDetail.Append("<tr><td width='15%' class='style10'>" + OurCode + "</td><td width='10%' class='style10'>"
                        + strVikingCode + "</td><td width='15%' class='style10'>" + Suppliercode + "</td><td width='40%' class='style10'>"
                        + Description + "</td><td  width='15%' class='style10'>" + ReceivedQuantity + "</td><td  width='15%' class='style10'>"
                        + UOM + "</td></tr>");
                    for (int i = 0; i < lstDetails.Count; i++)
                    {
                        sProductDetail.Append("<tr><td>" + lstDetails[i].ODSKUCode + "</td><td>"
                            + lstDetails[i].DirectCode + "</td><td>" + lstDetails[i].VendorCode + "</td><td>"
                            + lstDetails[i].ProductDescription + "</td><td>" + lstDetails[i].DeliveredQuantity + "</td><td>" + lstDetails[i].UOM + "</td></tr>");
                    }
                    break;
            }
             sProductDetail.Append("</table></td></tr>");
         }
         return sProductDetail.ToString();
    }
    protected void btnAddImage_Click(object sender, EventArgs e) {

        for (int i = 0; i < Request.Files.Count; i++) {
            HttpPostedFile fileImage = Request.Files[i];
            
            if (fileImage.FileName != null
                && fileImage.FileName != "") {
                if (!(Convert.ToString(fileImage.FileName.Split('.')[1]).Trim().ToLower() == "jpeg"
                            || Convert.ToString(fileImage.FileName.Split('.')[1]).Trim().ToLower() == "jpg"
                            || Convert.ToString(fileImage.FileName.Split('.')[1]).Trim().ToLower() == "png"
                            || Convert.ToString(fileImage.FileName.Split('.')[1]).Trim().ToLower() == "bmp"
                            || Convert.ToString(fileImage.FileName.Split('.')[1]).Trim().ToLower() == "gif"
                            || Convert.ToString(fileImage.FileName.Split('.')[1]).Trim().ToLower() == "pdf"
                            || Convert.ToString(fileImage.FileName.Split('.')[1]).Trim().ToLower() == "tiff"
                            || Convert.ToString(fileImage.FileName.Split('.')[1]).Trim().ToLower() == "tif")) {                    
               
                    string sWrongImage = WebUtilities.WebCommon.getGlobalResourceValue("WrongImage");
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Mess1", "alert('" + sWrongImage + "')", true);
                    return;
                }
            }
            else {
                string sSelectImage = WebUtilities.WebCommon.getGlobalResourceValue("SelectImage");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Mess1", "alert('" + sSelectImage + "')", true);
                return;
            }
        }

        for (int i = 0; i < Request.Files.Count; i++) {
            HttpPostedFile fileImage = Request.Files[i];

            //check for the duplicate image
            foreach (RepeaterItem item in rpImages.Items)
            {
                HiddenField hdImageID = ((HiddenField)item.FindControl("hdnImageID"));
                //HyperLink hylImageName = ((HyperLink)item.FindControl("hylImageName"));
                HtmlAnchor hylImageName = ((HtmlAnchor)item.FindControl("hylImageName"));
                ucButton btnAdd = ((ucButton)item.FindControl("RemoveImage"));

                var FileImageName = fileImage.FileName.Trim().Replace('+', '-').Replace("&", "and"); //case handled if File name contains "&" character


                //if (hylImageName.InnerText.Trim() == ltVDRNumberCreated.Text + "_" + fileImage.FileName.Trim().Replace('+', '-')) {
                //    string sImageAlreadyAdded = WebUtilities.WebCommon.getGlobalResourceValue("ImageAlreadyAdded");
                //    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Mess1", "alert('" + sImageAlreadyAdded + "')", true);
                //    return;
                //}

                if (hylImageName.InnerText.Trim() == ltVDRNumberCreated.Text + "_" + FileImageName.Trim())
                {
                    string sImageAlreadyAdded = WebUtilities.WebCommon.getGlobalResourceValue("ImageAlreadyAdded");
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Mess1", "alert('" + sImageAlreadyAdded + "')", true);
                    return;
                }
            }
            IncreaseRow(i, fileImage);
        }

        
    }

    private void IncreaseRow(int Icount, HttpPostedFile fileImage) {
        int Total = rpImages.Items.Count + 1;
        int count = 1;

        string path = AppDomain.CurrentDomain.BaseDirectory.ToLower();
        path = path.Replace(@"\bin\debug", "");

        if (Icount == 0)
            AddColumns();

        foreach (RepeaterItem item in rpImages.Items) {
            //getting the values of user entered fields
            HiddenField hddiscrepancyLogImageID = ((HiddenField)item.FindControl("hdnDiscrepancyLogImageID"));

            //HyperLink hylImageName = ((HyperLink)item.FindControl("hylImageName"));
            HtmlAnchor hylImageName = ((HtmlAnchor)item.FindControl("hylImageName"));
            ucButton btnAdd = ((ucButton)item.FindControl("RemoveImage"));

            //now change button text to remove, and save user entered values in table
            if (hddiscrepancyLogImageID != null && hddiscrepancyLogImageID.Value != "") {
                tbDummy.Rows.Add(hylImageName.InnerText.Trim(), (count++).ToString(), hddiscrepancyLogImageID.Value, "Remove");
            }
            else {
                tbDummy.Rows.Add(hylImageName.InnerText.Trim(), (count++).ToString(), "", "Remove");
            }
        }
        var FileImageName = fileImage.FileName.Replace('+', '-').Replace("&", "and"); //case handled if File name contains "&" character
        
        //Add dummy row, because we need to increase
        //if (!File.Exists(path + "Images/Discrepancy/" + ltVDRNumberCreated.Text + "_" + fileImage.FileName.Replace('+', '-'))) {
        //    fileImage.SaveAs(path + "Images/Discrepancy/" + ltVDRNumberCreated.Text + "_" + fileImage.FileName.Replace('+', '-'));
        //}
        //tbDummy.Rows.Add(ltVDRNumberCreated.Text + "_" + fileImage.FileName.Replace('+', '-'), count++, "", "Remove");

        
            if (!File.Exists(path + "Images/Discrepancy/" + ltVDRNumberCreated.Text + "_" + FileImageName))
            {
                fileImage.SaveAs(path + "Images/Discrepancy/" + ltVDRNumberCreated.Text + "_" + FileImageName);
            }
            tbDummy.Rows.Add(ltVDRNumberCreated.Text + "_" + FileImageName, count++, "", "Remove");
        
      
            //(Request.ApplicationPath + "/Images/Discrepancy/" + ltVDRNumberCreated.Text + "_" + fileImage.PostedFile.FileName), 

            BindWithRepeater();

        tbDummy.Rows.Clear();

    }    

    private void AddColumns() {
        //Add 3 dummy coloumn, this can be increase on our need basis
        tbDummy.Columns.Add("ImageName");
        tbDummy.Columns.Add("ImageID");
        tbDummy.Columns.Add("DiscrepancyLogImageID");
        //tbDummy.Columns.Add("ImagePath");
        tbDummy.Columns.Add("Button");
    }

    private void BindWithRepeater() {

        if (dsDummy.Tables.Count <= 0) {

            //add this table to dataset
            dsDummy.Tables.Add(tbDummy);
        }       

        //bind this dataset to repeater
        rpImages.DataSource = dsDummy;
        rpImages.DataBind();
    }

    protected void RemoveImage_Click(object sender, CommandEventArgs e) {
        string path = AppDomain.CurrentDomain.BaseDirectory.ToLower();
        path = path.Replace(@"\bin\debug", "");

        if (e.CommandName == "Remove" && e.CommandArgument != null) {
            RepeaterItem rptSelectedItem = (RepeaterItem)((Control)sender).NamingContainer;
            HiddenField hdSelectedDisLogItemID = ((HiddenField)rptSelectedItem.FindControl("hdnDiscrepancyLogImageID"));
            HiddenField hdSelectedImageD = ((HiddenField)rptSelectedItem.FindControl("hdnImageID"));
            int count = 1;
            tbDummy = null;
            tbDummy = new DataTable();
            AddColumns();
            foreach (RepeaterItem item in rpImages.Items) {
                HiddenField hdImageID = ((HiddenField)item.FindControl("hdnImageID"));
                ucButton btnAdd = ((ucButton)item.FindControl("RemoveImage"));
                HiddenField hdDiscrepancyLogImageID = ((HiddenField)item.FindControl("hdnDiscrepancyLogImageID"));
                //HyperLink hylImageName = ((HyperLink)item.FindControl("hylImageName"));
                HtmlAnchor hylImageName = ((HtmlAnchor)item.FindControl("hylImageName"));

                if (hdDiscrepancyLogImageID != null && hdDiscrepancyLogImageID.Value.Trim() != ""
                    && hdDiscrepancyLogImageID.Value.Trim() == hdSelectedDisLogItemID.Value.Trim()) {
                    DISLog_ImagesBE oDISLog_ImagesBE = new DISLog_ImagesBE();
                    DISLog_ImagesBAL oDISLog_ImagesBAL = new DISLog_ImagesBAL();
                    oDISLog_ImagesBE.Discrepancy = new DiscrepancyBE();
                    oDISLog_ImagesBE.Action = "DeleteImage";
                    oDISLog_ImagesBE.DiscrepancyLogImageID = Convert.ToInt32(hdDiscrepancyLogImageID.Value);
                    oDISLog_ImagesBAL.addEditDISLogImageDetailsBAL(oDISLog_ImagesBE);
                    oDISLog_ImagesBAL = null;

                    //delete also from the Images folder
                    if (File.Exists(path + @"Images/Discrepancy/" + hylImageName.InnerText.Trim())) {
                        File.Delete(path + @"Images/Discrepancy/" + hylImageName.InnerText.Trim());
                    }
                }

                else if (e.CommandArgument.ToString().Trim() != hdImageID.Value.ToString().Trim()) {
                    //now change button text to remove, and save user entered values in table
                    if (hdDiscrepancyLogImageID != null && hdDiscrepancyLogImageID.Value != "")
                        tbDummy.Rows.Add(hylImageName.InnerText, (count++).ToString(), hdDiscrepancyLogImageID.Value,
                            //hylImageName.NavigateUrl.ToString(), 
                            "Remove");
                    else
                        tbDummy.Rows.Add(hylImageName.InnerText, (count++).ToString(), "",
                            //hylImageName.NavigateUrl.ToString(), 
                            "Remove");
                }
                else if (e.CommandArgument.ToString().Trim() == hdImageID.Value.ToString().Trim()) {
                    //delete also from the Images folder
                    if (File.Exists(path + @"Images/Discrepancy/" + hylImageName.InnerText.Trim())) {
                        File.Delete(path + @"Images/Discrepancy/" + hylImageName.InnerText.Trim());
                    }
                }
            }
            BindWithRepeater();
        }
    }

    protected void BindImages() {
        AddColumns();
        if (GetQueryStringValue("DiscrepancyLogID") != null && GetQueryStringValue("DiscrepancyLogID").ToString() != "") {
            DISLog_ImagesBE oDISLog_ImagesBE = new DISLog_ImagesBE();
            DISLog_ImagesBAL oDISLog_ImagesBAL = new DISLog_ImagesBAL();
            oDISLog_ImagesBE.Discrepancy = new DiscrepancyBE();
            oDISLog_ImagesBE.Action = "ShowAll";
            oDISLog_ImagesBE.Discrepancy.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("DiscrepancyLogID"));
            //oDISLog_ImagesBE.Discrepancy.DiscrepancyLogID = 6;
            List<DISLog_ImagesBE> lstImagelist = oDISLog_ImagesBAL.GetDISLogImageDetailsBAL(oDISLog_ImagesBE);
            oDISLog_ImagesBAL = null;
            int count = 1;
            if (lstImagelist != null && lstImagelist.Count > 0) {

                foreach (DISLog_ImagesBE item in lstImagelist) {
                    tbDummy.Rows.Add(item.ImageName.ToString().Trim(), count++, item.DiscrepancyLogImageID,"Remove");
                    //(Request.ApplicationPath + "/Images/Discrepancy/" + item.ImageName.ToString().Trim()), 
                }
                dsDummy.Tables.Add(tbDummy);
                //bind this dataset to repeater
                rpImages.DataSource = dsDummy;
                rpImages.DataBind();
            }
        }
    }
}
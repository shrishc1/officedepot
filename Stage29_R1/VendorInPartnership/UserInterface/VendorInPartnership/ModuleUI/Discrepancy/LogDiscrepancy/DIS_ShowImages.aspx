﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="DIS_ShowImages.aspx.cs"
    Inherits="DIS_ShowImages" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" type="text/css" href="../../../Css/style.css" />   
</head>
<body>
    <form id="form1" runat="server">
       <div style="width: 100%; padding-left: 10px">
            <table  align="center">
            <tr>
                <td>&nbsp;</td>
            </tr>            
                <tr id="rwButtons">
                    <td style="text-align: left;font-weight: bold;">
                     <asp:GridView ID="gvImages" runat="server" AutoGenerateColumns="false" onrowdatabound="gvImages_RowDataBound" ShowHeader="false" >
                        <Columns>                        
                        <asp:TemplateField ShowHeader="false">
                            <ItemTemplate>
                                <asp:Image ID="img" runat="server" ImageUrl='<%#DataBinder.Eval(Container.DataItem, "imagename")%>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                     </asp:GridView>  
                    </td>
                </tr>    
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>
                        <cc1:ucButton ID="btnClose" runat="server" Text="Close" OnClientClick="window.close();" class="button"  />
                    </td>
                </tr>        
            </table>       
        </div>
    </form>
</body>
</html>

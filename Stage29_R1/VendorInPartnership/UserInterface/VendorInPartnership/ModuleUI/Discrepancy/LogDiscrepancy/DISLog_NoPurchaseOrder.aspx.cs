﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using BaseControlLibrary;
using BusinessEntities.ModuleBE.AdminFunctions;
using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using BusinessEntities.ModuleBE.Discrepancy.LogDiscrepancy;
using BusinessLogicLayer.ModuleBAL.AdminFunctions;
using BusinessLogicLayer.ModuleBAL.Appointment.SiteSettings;
using BusinessLogicLayer.ModuleBAL.Discrepancy.LogDiscrepancy;
using Utilities;
using WebUtilities;

public partial class DISLog_NoPurchaseOrder : CommonPage
{
    #region Global variables

    //static int iVendorID = 0;
    DataSet dsDummy = null; //dummy dataset use for repeater
    DataTable tbDummy = null; //dummy table for dataset
    protected string InvalidValuesEntered = WebCommon.getGlobalResourceValue("InvalidValuesEntered");
    protected string AtLeastOne = WebCommon.getGlobalResourceValue("AtLeastOne");
    protected string ProductDescription = WebCommon.getGlobalResourceValue("ProductDescription");
    protected string ReceivedQuantity = WebCommon.getGlobalResourceValue("ReceivedQuantity");
    System.Text.StringBuilder sSendMailLink = new System.Text.StringBuilder();
    //int DiscrepancyLogID = 0;
    //static int SiteID = 0;

    public int iVendorID
    {
        get
        {
            return ViewState["iVendorID"] != null ? Convert.ToInt32(ViewState["iVendorID"].ToString()) : 0;
        }
        set
        {
            ViewState["iVendorID"] = value;
        }
    }

    public int DiscrepancyLogID
    {
        get
        {
            return ViewState["DiscrepancyLogID"] != null ? Convert.ToInt32(ViewState["DiscrepancyLogID"].ToString()) : 0;
        }
        set
        {
            ViewState["DiscrepancyLogID"] = value;
        }
    }

    public int SiteID
    {
        get
        {
            return ViewState["SiteID"] != null ? Convert.ToInt32(ViewState["SiteID"].ToString()) : 0;
        }
        set
        {
            ViewState["SiteID"] = value;
        }
    }
    static string VDRNo = string.Empty;
    static int iGridAlreadyContainsItems = 0;
    static BaseControlLibrary.ucTextbox currentTextBox;
    protected string ValidEmail = WebCommon.getGlobalResourceValue("ValidEmail");
    protected string NoEmailSpecifiedMessage = WebCommon.getGlobalResourceValue("NoEmailSpecifiedMessage");
    protected string ConfirmMessage = WebCommon.getGlobalResourceValue("ConfirmMessage");
    protected string DiscrepancyAlreadyCreated = WebCommon.getGlobalResourceValue("DiscrepancyAlreadyCreated");
    string InitialCommunicationSent = WebCommon.getGlobalResourceValue("InitialCommunicationSent");
    string FollowingRequestMessage = WebCommon.getGlobalResourceValue("FollowingRequestMessage");
    string NoPODesc = WebCommon.getGlobalResourceValue("NoPODesc");
    /*---------------SHOW ERROR MESSAGE WHEN QUERY IS ALREADY OPEN--------------*/
    string strThereIsCurrentlyAlreadyAnOpenQueryBeingInvestigated = WebCommon.getGlobalResourceValue("ThereIsCurrentlyAlreadyAnOpenQueryBeingInvestigated");
    string strPleaseWaitUntilYouHaveReceivedAReplyForThisBefore = WebCommon.getGlobalResourceValue("PleaseWaitUntilYouHaveReceivedAReplyForThisBefore");

    protected string DeleteBlankWorkflowEntriesText = WebCommon.getGlobalResourceValue("DeleteBlankWorkflowEntriesText");

    protected string ItemDetailsRequired = WebCommon.getGlobalResourceValue("ItemDetailsRequired");
    protected string VikingCodeDescription = WebCommon.getGlobalResourceValue("VikingCodeDescription");
    protected string SupplierCodeDescription = WebCommon.getGlobalResourceValue("SupplierCodeDescription");
    protected string ProductReceivedDescription = WebCommon.getGlobalResourceValue("ProductReceivedDescription");
   
    int? DeleteDiscrepancyTest;
    string sResult = string.Empty;
    bool bSendMail = false;
    int iResult;
    #endregion

    #region Events

    protected void Page_InIt(object sender, EventArgs e)
    {
        ucSite.CurrentPage = this;
        ucSeacrhVendor1.CurrentPage = this;
        ucSeacrhVendor1.IsStandAloneRequired = true;
        ucSeacrhVendor1.IsChildRequired = true;
        ucSeacrhVendor1.IsParentRequired = true;
        ucSeacrhVendor1.IsGrandParentRequired = false;
        ucApAction.CurrentPage = this;
    }
    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);
        if (!IsPostBack)
        {
            if (GetQueryStringValue("VDRNo") != null && !string.IsNullOrWhiteSpace(GetQueryStringValue("VDRNo").ToString()))
            {


                if (GetQueryStringValue("PN") != null && !string.IsNullOrWhiteSpace(GetQueryStringValue("PN")))
                {
                    if (GetQueryStringValue("PN").Trim().ToUpper().Equals("POINQ"))
                    {
                        btnBack_1.Visible = true;
                    }
                }
                else if (GetQueryStringValue("PreviousPage") != null)
                {
                    string iframeStyleStatus = ifUserControl.Style["display"];
                    if (GetQueryStringValue("PreviousPage").Trim().ToUpper().Equals("SearchResultVendor".ToUpper()) &&
                        iframeStyleStatus != null)
                        btnBack_1.Visible = true;
                    else if (GetQueryStringValue("PreviousPage").Trim().Equals("SearchResult") &&
                   iframeStyleStatus != null)
                    {
                        btnBack_1.Visible = true;
                    }
                    else
                    {
                        btnBack_1.Visible = false;
                        btnUpdateLocation.Visible = false;
                    }
                }
            }
            else
            {
                UIUtility.PageValidateForODUser();
            }
        }
        if (GetQueryStringValue("VDRNo") != null && !string.IsNullOrWhiteSpace(GetQueryStringValue("VDRNo").ToString()) && !lblNoPurchaseOrder.Text.ToString().Contains("-"))
        {
            lblNoPurchaseOrder.Text = lblNoPurchaseOrder.Text + " - " + GetQueryStringValue("VDRNo").ToString();
        }
    }
    protected void btnAccountsPayableAction_Click(object sender, EventArgs e)
    {

        mdlAPAction.Show();
        DropDownList ddl = ucApAction.FindControl("ddlCurrency") as DropDownList;
        DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
        DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();
        oDiscrepancyBE.Action = "GetCurrencyByVendorID";
        if (GetQueryStringValue("disLogID") != null)
        {
            oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID"));
        }
        ddl.SelectedValue = oDiscrepancyBAL.GetCurrencyBAL(oDiscrepancyBE);      
    }

    protected void btnBack_1_Click(object sender, EventArgs e)
    {
        if (GetQueryStringValue("PreviousPage") != null)
        {
            if (GetQueryStringValue("PreviousPage").Trim().ToUpper().Equals("SearchResultVendor".ToUpper()))
            {
                btnBack_1.Visible = false;
                EncryptQueryString("~/ModuleUI/Discrepancy/DIS_SearchResultVendor.aspx?disLogID=" + Convert.ToString(GetQueryStringValue("disLogID"))
                            + "&VDRNo=" + Convert.ToString(GetQueryStringValue("VDRNo"))
                            + "&UserID=" + Convert.ToString(GetQueryStringValue("UserID"))
                            + "&PreviousPage=SearchResultVendor&WorkFlowID=" + Convert.ToString(GetQueryStringValue("WorkFlowID").ToString()));
            }

            if (Session["PageMode"].ToString() == "Todays")
            {
                EncryptQueryString("../DIS_SearchResult.aspx?FromPage=images&SiteId=" + GetQueryStringValue("SiteId") + "&DisDate=" + GetQueryStringValue("DisDate"));
            }
            else if (Session["PageMode"].ToString() == "Search")
            {
                //For opening search discrepancies mode
                btnBack_1.Visible = false;
                EncryptQueryString("~/ModuleUI/Discrepancy/DIS_SearchResult.aspx?disLogID=" + Convert.ToString(GetQueryStringValue("disLogID"))
                            + "&VDRNo=" + Convert.ToString(GetQueryStringValue("VDRNo"))
                            + "&UserID=" + Convert.ToString(GetQueryStringValue("UserID"))
                            + "&PreviousPage=SearchResult&WorkFlowID=" + Convert.ToString(GetQueryStringValue("WorkFlowID").ToString()));

            }
        }
        else if (GetQueryStringValue("PN") != null)
        {
            if (GetQueryStringValue("PN").Trim().ToUpper().Equals("POINQ") || GetQueryStringValue("PN").Trim().ToUpper().Equals("OPENPL"))
            {
                btnBack_1.Visible = false;
                EncryptQueryString("~/ModuleUI/Appointment/PurchaseOrderInquiry.aspx?SId=" + GetQueryStringValue("SId").ToString()
                    + "&Status=" + GetQueryStringValue("Status").ToString()
                    + "&PN=DISLOG"
                    + "&PO=" + GetQueryStringValue("PO").ToString());
            }
            else if (GetQueryStringValue("PN").Trim().ToUpper().Equals("OPENPL"))
            {
                EncryptQueryString("~/ModuleUI/StockOverview/Expedite/OpenPurchaseListing.aspx?action=showlisting");
            }
        }
        else if (GetQueryStringValue("PageType") != null)
        {
            EncryptQueryString("~/ModuleUI/Discrepancy/DIS_LocationReport.aspx");
        }
    }
    void ucApAction_SaveEventValidation(object sender, EventArgs e)
    {
        if (ucApAction.IsModelOpen)
            mdlAPAction.Show();
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        dsDummy = new DataSet();
        tbDummy = new DataTable();
        ucApAction.SaveEventValidation += new EventHandler(ucApAction_SaveEventValidation);
        txtPurchaseOrder.Focus();
        btnPrintReturnsNote.Visible = false;
        if (GetQueryStringValue("disLogID") != null)
        {
            mvNoPurchaseOrder.ActiveViewIndex = 1;
        }
        if (mvNoPurchaseOrder.ActiveViewIndex == 1)
        {
            btnLog.Visible = false;
            btnBack.Visible = true;
            btnSave.Visible = true;
        }
        else
        {
            btnLog.Visible = true;
            btnBack.Visible = false;
            btnSave.Visible = false;
        }

        if (rdoPO1.Checked)
        {
            rfvPurchaseOrderNumberValidation.Enabled = true;
            cusvNoSelectedVendor.Enabled = false;
        }
        else if (rdoVendor11.Checked)
        {
            rfvPurchaseOrderNumberValidation.Enabled = false;
            cusvNoSelectedVendor.Enabled = true;
        }

        if (!IsPostBack)
        {
            txtDateDeliveryArrived.innerControltxtDate.Value = Convert.ToString(DateTime.Now.ToString("dd/MM/yyyy"));
            lblIssueRaisedDate.Text = Convert.ToString(DateTime.Now.ToString("dd/MM/yyyy hh:mm"));
            rfvPurchaseOrderNumberValidation.Enabled = true;
            cusvNoSelectedVendor.Enabled = false;
            txtPurchaseOrder.Enabled = true;
            ddlPurchaseOrderDate.Enabled = true;
            lblVendorValue.Text = "";
            hdnSelectOption.Value = "1";
            VendorsQueryOverview();
            if (GetQueryStringValue("disLogID") != null)
            {
                this.BindDiscrepancyLogComment();
                if (GetQueryStringValue("disLogID") != null && GetQueryStringValue("status").ToLower() == "closed")
                {
                    btnUpdateLocation.Visible = true;
                    //txtLocation.ReadOnly = true;
                }
                else
                {
                    btnUpdateLocation.Visible = true;
                }
                if (GetQueryStringValue("PageType") != null && GetQueryStringValue("PageType") == "Location")
                {
                    btnUpdateLocation.Visible = false;
                    txtLocation.ReadOnly = true;
                    btnBack_1.Visible = true;
                }
                mvNoPurchaseOrder.ActiveViewIndex = 1;
                btnAdd.Style["display"] = "none";
                GetDeliveryDetails();
                GetContactPurchaseOrderDetails();
                if (Session["Role"] != null)
                {
                    GetAccessRightRaiseQuery();
                    if (Session["Role"].ToString() == "Vendor")
                    {
                        pnlInternalComments.Visible = false;
                        btnAddComment.Visible = true;
                        btnAddImages.Visible = false;
                        btnRaiseQuery.Visible = true;
                        btnAccountsPayableAction.Visible = false;
                    }
                }
                AddUserControl();
                this.ShowACP001ActionButton();
                btnLog.Visible = false;
                btnBack.Visible = false;
                btnSave.Visible = false;
                if (Session["Role"] != null && (Session["Role"].ToString().ToLower() == "vendor" || Session["Role"].ToString().ToLower() == "carrier"))
                {
                    btnViewWF.Visible = false;
                }
                else
                {
                    btnViewWF.Visible = true;
                }
                // code for generating div for the imageviewer
                string DivGallery = CommonPage.GetImagesStringNewLayout(Convert.ToInt32(GetQueryStringValue("disLogID")));
                if (string.IsNullOrEmpty(DivGallery))
                {
                    btnViewImage.Visible = false;
                }
                else
                {
                    discrepancygallery.InnerHtml = DivGallery.ToString();
                    btnViewImage.Visible = true;
                }
            }
            else
            {
                btnAddComment.Visible = false;
                ifUserControl.Style["display"] = "none";
                btnViewWF.Visible = false;
                btnViewImage.Visible = false;
                btnAddImages.Visible = false;
                btnRaiseQuery.Visible = false;
                pnlQueryOverview_1.Visible = false;
                btnAccountsPayableAction.Visible = false;
            }
        }

        txtPurchaseOrder.Attributes.Add("autocomplete", "off");
        txtDeliveryNumber.Focus();
    }

    private void ShowACP001ActionButton()
    {
        DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
        DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();
        oDiscrepancyBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
        string CaseOf = String.Empty;

        oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID"));

        if (GetQueryStringValue("RoleTypeFlag") == "A" || GetQueryStringValue("RoleTypeFlag") == null)
        {
            if (Session["UserID"] == null || Session["UserID"].ToString() == "" || Session["Role"] == null || Session["Role"].ToString().Trim() == "")
                return;

            oDiscrepancyBE.User.UserID = Convert.ToInt32(Session["UserID"]);
            CaseOf = Session["Role"].ToString().Trim();
        }
        //else
        //{
        //    switch (GetQueryStringValue("RoleTypeFlag").ToString())
        //    {
        //        case "G":
        //            CaseOf = "OD - Goods In";
        //            break;
        //        case "S":
        //            CaseOf = "OD - Stock Planner";
        //            break;
        //        case "P":
        //            CaseOf = "OD - Accounts Payable";
        //            break;
        //        case "V":
        //            CaseOf = "Vendor";
        //            break;
        //    }
        //}

        /* Here P is an "OD - Accounts Payable" When from drop down selected the Account Payble option */
        /* Here A will come when actual Account Payble type user will loggedin. */
        if (GetQueryStringValue("RoleTypeFlag") == "P" || GetQueryStringValue("RoleTypeFlag") == "A")
        {
            oDiscrepancyBE.Action = "CheckForActionRequired";
            oDiscrepancyBE.DiscrepancyWorkFlowID = !string.IsNullOrEmpty(GetQueryStringValue("WorkFlowID")) ? Convert.ToInt32(GetQueryStringValue("WorkFlowID")) : (int?)null;
            DataTable dtActionDetails = oDiscrepancyBAL.GetActionRequiredDetailsForWorkFlowBAL(oDiscrepancyBE);
            if (dtActionDetails != null && dtActionDetails.Rows.Count > 0)
            {
                //if (dtActionDetails.Rows[0]["APUserControl"].ToString().Trim() == "ACP001")
               // if (Convert.ToString(dtActionDetails.Rows[0]["ACP001Control"]).Trim().Equals("ACP001")
               //     && !Convert.ToBoolean(dtActionDetails.Rows[0]["IsACP001Done"]))
                if (!string.IsNullOrEmpty(dtActionDetails.Rows[0]["IsACP001Done"].ToString()) && !Convert.ToBoolean(dtActionDetails.Rows[0]["IsACP001Done"]))
                {
                    if (GetQueryStringValue("disLogID") != null)
                        ucApAction.DisLogID = Convert.ToInt32(GetQueryStringValue("disLogID"));

                    ucApAction.DisWFlowID = Convert.ToInt32(dtActionDetails.Rows[0]["DiscrepancyWorkflowID"]);
                    ucApAction.PONumber = Convert.ToString(dtActionDetails.Rows[0]["PurchaseOrderNumber"]);
                    ucApAction.SiteID = SiteID;
                    ucApAction.VDRNo = VDRNo;
                    ucApAction.FromPage = "overs";
                    ucApAction.PreviousPage = GetQueryStringValue("PreviousPage").ToString();

                    if (ViewState["VendorID"] != null)
                        ucApAction.VendorID = Convert.ToInt32(ViewState["VendorID"]);

                    btnAccountsPayableAction.Visible = true;
                    //ifUserControl.Style["display"] = "none";
                }
               else
               {
                   btnAccountsPayableAction.Visible = false;
               }
            }
        }
    }

    public override void SitePost_Load()
    {
        base.SitePost_Load();
        if (!IsPostBack)
        {
            //ucSite.innerControlddlSite.SelectedIndex = 0;
            ucSite.innerControlddlSite.AutoPostBack = true;
            if (GetQueryStringValue("disLogID") == null)
            {
                ucSeacrhVendor1.SiteID = Convert.ToInt32(ucSite.innerControlddlSite.SelectedValue);
            }
        }
    }

    public override void SiteSelectedIndexChanged()
    {
        if (txtPurchaseOrder.Text != string.Empty)
        {
            GetPurchaseOrderList();
        }
        if (rdoVendor11.Checked)
        {
            TextBox txtVendor = (TextBox)ucSeacrhVendor1.FindControl("txtVendorNo");
            Label lblSelectedVendorName = (Label)ucSeacrhVendor1.FindControl("SelectedVendorName");
            txtVendor.Text = "";
            lblSelectedVendorName.Text = "";
        }
        if (GetQueryStringValue("disLogID") == null)
        {
            ucSeacrhVendor1.SiteID = Convert.ToInt32(ucSite.innerControlddlSite.SelectedValue);
        }
    }

    protected void RemoveImage_Click(object sender, CommandEventArgs e)
    {
        if (e.CommandName == "Remove" && e.CommandArgument != null)
        {
            int iCount = 1;
            tbDummy = null;
            tbDummy = new DataTable();
            AddColumns();
            foreach (GridViewRow item in gvNoPurchaseOrder.Rows)
            {

                ucTextbox txtLineNo = (ucTextbox)item.FindControl("txtLineNo");
                ucTextbox txtOfficeDepotCode = (ucTextbox)item.FindControl("txtOfficeDepotCode");
                ucTextbox txtVikingCode = (ucTextbox)item.FindControl("txtVikingCode");
                ucTextbox txtVendorItemCode = (ucTextbox)item.FindControl("txtVendorItemCode");
                ucTextbox txtDescription = (ucTextbox)item.FindControl("txtDescription");
                ucTextbox txtOriginalPOQuantity = (ucTextbox)item.FindControl("txtOriginalPOQuantity");
                ucTextbox txtOutstandingQty = (ucTextbox)item.FindControl("txtOutstandingQty");
                ucTextbox txtReceivedQty = (ucTextbox)item.FindControl("txtReceivedQty");
                ucTextbox txtUoM = (ucTextbox)item.FindControl("txtUoM");
                ucButton btnRemove = (ucButton)item.FindControl("btnRemove");


                HiddenField hdnRecordID = (HiddenField)item.FindControl("hdnRecordID");
                if (e.CommandArgument.ToString().Trim() != hdnRecordID.Value.ToString().Trim())
                {
                    tbDummy.Rows.Add(txtLineNo.Text.Trim(),
                                 txtOfficeDepotCode.Text.Trim(),
                                 txtVikingCode.Text.Trim(),
                                 txtVendorItemCode.Text.Trim(),
                                 txtDescription.Text.Trim(),
                                 txtOriginalPOQuantity.Text.Trim(),
                                 txtOutstandingQty.Text.Trim(),
                                 txtReceivedQty.Text.Trim(),
                                 txtUoM.Text.Trim(),
                                 "Remove",
                                 (iCount++).ToString());
                }
            }

            BindWithGrid();
            EnableDisableGridControls();
        }
    }

    protected void btnViewWF_Click(object sender, EventArgs e)
    {
        if (GetQueryStringValue("PN") != null)
        {
            if (GetQueryStringValue("PN").Trim().ToUpper().Equals("POINQ"))
            {
                string url = string.Format("DISLog_Workflow.aspx?disLogID={0}&PN={1}&SId={2}&Status={3}&PN={4}&PO={5}", GetQueryStringValue("disLogID").ToString(), "POINQ", GetQueryStringValue("SId").ToString(), GetQueryStringValue("Status").ToString(), "DISLOG", GetQueryStringValue("PO").ToString());
                EncryptQueryString(url);
            }

        }
        if (GetQueryStringValue("disLogID") != null)
        {
            string url = string.Format("DISLog_Workflow.aspx?disLogID={0}&VDRNo={1}&TodayDis={2}&SiteID={3}&DisDate={4}&UserID={5}", GetQueryStringValue("disLogID").ToString(), GetQueryStringValue("VDRNo").ToString(), GetQueryStringValue("TodayDis"), GetQueryStringValue("SiteId"), GetQueryStringValue("DisDate"), GetQueryStringValue("UserID"));
            EncryptQueryString(url);
        }
    }

    protected void btnAdd_Click(object sender, EventArgs e)
    {
        IncreaseRow();
    }

    protected void btnLog_Click(object sender, EventArgs e)
    {
        DateTime dt = txtDateDeliveryArrived.GetDate;
        if (dt > DateTime.Now.Date)
        {
            string saveMessage = WebCommon.getGlobalResourceValue("NotDateValidMessage");
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('" + saveMessage + "')", true);
            return;
        }
        GetDeliveryDetails();
        GetContactPurchaseOrderDetails();
        //if (ViewState["StockPlannerID"] == null)
        //{
        //    string NoStockPlannerExist = WebCommon.getGlobalResourceValue("NoStockPlannerExist");
        //    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + NoStockPlannerExist + "')", true);
        //    return;
        //}
        mvNoPurchaseOrder.ActiveViewIndex = 1;
        btnLog.Visible = false;
        btnBack.Visible = true;
        btnSave.Visible = true;
        btnSave.Enabled = true;      

        if (hdnSelectOption.Value == "1")
        {
            btnAdd.Style["display"] = "none";
        }
        else
        {
            btnAdd.Style["display"] = "";
            IncreaseRow();
        }
        if (gvNoPurchaseOrder.Rows.Count > 0)
            iGridAlreadyContainsItems = 1;
        ucSDRCommunication1.innerControlRdoCommunication = 'E';
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        var defaultEmailAddress = Convert.ToString(ConfigurationManager.AppSettings["DefaultEmailAddress"]);
        var txtAltEmail = (ucTextbox)ucSDRCommunication1.FindControl("txtAltEmail");
        if (ucSDRCommunication1.VendorEmailList.Trim().Equals(defaultEmailAddress) && (string.IsNullOrEmpty(txtAltEmail.Text) || string.IsNullOrWhiteSpace(txtAltEmail.Text)))
        {
            mdlAlternateEmail.Show();
            rfvEmailAddressCanNotBeEmpty.Enabled = true;
            txtAlternateEmailText.Focus();
        }
        else
        {
            foreach (GridViewRow item in gvNoPurchaseOrder.Rows)
            {
                ucTextbox txtOfficeDepotCode = (ucTextbox)item.FindControl("txtOfficeDepotCode");
                ucTextbox txtVikingCode = (ucTextbox)item.FindControl("txtVikingCode");
                ucTextbox txtVendorItemCode = (ucTextbox)item.FindControl("txtVendorItemCode");
                ucTextbox txtDescription = (ucTextbox)item.FindControl("txtDescription");
                if (txtOfficeDepotCode.Text.Equals("") || txtOfficeDepotCode.Text.Trim() == string.Empty)
                {
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + ItemDetailsRequired + "')", true);
                    return;
                }

                //if (txtVikingCode.Text.Trim() == "" || txtVikingCode.Text.Trim() == string.Empty)
                //{
                //    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + VikingCodeDescription + "')", true);
                //    return;
                //}

                //if (txtVendorItemCode.Text.Trim() == "" || txtVendorItemCode.Text.Trim() == string.Empty)
                //{
                //    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + SupplierCodeDescription + "')", true);
                //    return;
                //}
                //if (txtDescription.Text.Trim() == "" || txtDescription.Text.Trim() == string.Empty)
                //{
                //    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + ProductReceivedDescription + "')", true);
                //    return;
                //}
                  


            }

            if (Session["Role"].ToString().ToLower() == "vendor")
            {
                string LoginPath = ResolveClientUrl("../../../ModuleUI/Security/Login.aspx");
                Response.Redirect(LoginPath);
            }
            else
            {
                this.Save();
            }
            if (DeleteDiscrepancyTest == 0)
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + DeleteBlankWorkflowEntriesText + "')", true);
                iGridAlreadyContainsItems = 1;
                System.Threading.Thread.Sleep(30000);
            }
        }
    }

    private void Save()
    {
        try
        {
            int iAllEmpty = 0;
           
            if (gvNoPurchaseOrder != null && gvNoPurchaseOrder.Rows.Count > 0)
            {
                //////////////////////////////////////////////////
                foreach (GridViewRow item in gvNoPurchaseOrder.Rows)
                {
                    DiscrepancyBE oNewDiscrepancyBE = new DiscrepancyBE();
                    DiscrepancyBAL oNewDiscrepancyBAL = new DiscrepancyBAL();

                    ucTextbox txtLineNo = (ucTextbox)item.FindControl("txtLineNo");
                    ucTextbox txtOfficeDepotCode = (ucTextbox)item.FindControl("txtOfficeDepotCode");
                    ucTextbox txtVikingCode = (ucTextbox)item.FindControl("txtVikingCode");
                    ucTextbox txtVendorItemCode = (ucTextbox)item.FindControl("txtVendorItemCode");
                    ucTextbox txtDescription = (ucTextbox)item.FindControl("txtDescription");
                    ucTextbox txtReceivedQty = (ucTextbox)item.FindControl("txtReceivedQty");
                    ucTextbox txtUoM = (ucTextbox)item.FindControl("txtUoM");
                    ucTextbox txtOriginalPOQuantity = (ucTextbox)item.FindControl("txtOriginalPOQuantity");
                    ucTextbox txtOutstandingQty = (ucTextbox)item.FindControl("txtOutstandingQty");
                    if (txtOfficeDepotCode.Text.Trim() != ""
                        && txtOfficeDepotCode.Text.Trim() != string.Empty && iGridAlreadyContainsItems == 0)
                    {

                        int dReceivedQuantity;
                        bool isdReceivedQuantityDecimal = int.TryParse(txtReceivedQty.Text.Trim(), out dReceivedQuantity);
                        if (txtReceivedQty.Text.Trim() != "" && !isdReceivedQuantityDecimal)
                        {

                            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + InvalidValuesEntered + "')", true);
                            return;
                        }
                        if (txtReceivedQty.Text.Trim() != ""
                            && isdReceivedQuantityDecimal
                            && !txtReceivedQty.Text.IsNumeric())
                        {

                            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + InvalidValuesEntered + "')", true);
                            return;
                        }
                    }
                    else if (iGridAlreadyContainsItems == 1 && txtReceivedQty.Text.Trim() != "")
                    {
                        //means PO order already contains items, just check the Received quantity
                        int dReceivedQuantity;
                        bool isdReceivedQuantityDecimal = int.TryParse(txtReceivedQty.Text.Trim(), out dReceivedQuantity);
                        if (!isdReceivedQuantityDecimal)
                        {
                            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + InvalidValuesEntered + "')", true);
                            return;
                        }
                        if (isdReceivedQuantityDecimal
                            && !txtReceivedQty.Text.IsNumeric())
                        {
                            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + InvalidValuesEntered + "')", true);
                            return;
                        }
                    }
                    else
                    {
                        iAllEmpty++;
                    }
                }
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + AtLeastOne + "')", true);
                return;
            }
            if (iAllEmpty > 0 && iAllEmpty == gvNoPurchaseOrder.Rows.Count)
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + ReceivedQuantity + "')", true);
                return;
            }
            //////////////////////////////////////////////////

            //1= PO is selected and 2= vendor is selected
           // btnSave.Enabled = false;
            DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
            DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();

            oDiscrepancyBE.Action = "AddDiscrepancyLog";
            oDiscrepancyBE.SiteID = Convert.ToInt32(ucSite.innerControlddlSite.SelectedValue);
            oDiscrepancyBE.DiscrepancyTypeID = Convert.ToInt32(Common.DiscrepancyType.NoPurchaseOrder);
            oDiscrepancyBE.DiscrepancyLogDate = DateTime.Now;

            oDiscrepancyBE.InternalComments = txtInternalComments.Text;
            oDiscrepancyBE.DeliveryNoteNumber = txtDeliveryNumber.Text.Trim();
            oDiscrepancyBE.DeliveryArrivedDate = Common.GetMM_DD_YYYY(txtDateDeliveryArrived.innerControltxtDate.Value); //DateTime.Now;
            oDiscrepancyBE.CommunicationType = ucSDRCommunication1.innerControlRdoCommunication;
            oDiscrepancyBE.CommunicationTo = ucSDRCommunication1.VendorEmailList;

            if (rdoPO1.Checked)
            {
                oDiscrepancyBE.PurchaseOrderNumber = txtPurchaseOrder.Text.Trim();
                oDiscrepancyBE.PurchaseOrderID = ViewState["PurchaseOrderID"] != null ? Convert.ToInt32(ViewState["PurchaseOrderID"].ToString()) : (int?)null;
                oDiscrepancyBE.PurchaseOrderDate = Common.GetMM_DD_YYYY(lblPurchaseOrderDateValue.Text);
                oDiscrepancyBE.VendorID = iVendorID;
            }
            else
            {
                oDiscrepancyBE.PurchaseOrderID = (int?)null;
                oDiscrepancyBE.VendorID = Convert.ToInt32(ucSeacrhVendor1.VendorNo);
            }

            if (ViewState["SPToCoverStockPlannerID"] != null)
            {
                oDiscrepancyBE.StockPlannerID = Convert.ToInt32(ViewState["SPToCoverStockPlannerID"].ToString()); // case when actual Stock Planner is absent and SP to Cover is there
                oDiscrepancyBE.StockPlannerIDIfNotAbsent = ViewState["StockPlannerID"] != null ? Convert.ToInt32(ViewState["StockPlannerID"].ToString()) : (int?)null; // Actual Stock Palnner Id that should have been saved if not absent
            }
            else
            {
                if (ViewState["StockPlannerID"] != null)
                {
                    oDiscrepancyBE.StockPlannerID = Convert.ToInt32(ViewState["StockPlannerID"].ToString());
                    oDiscrepancyBE.StockPlannerIDIfNotAbsent = null;
                }
            }


            //oDiscrepancyBE.StockPlannerID = ViewState["StockPlannerID"] != null ? Convert.ToInt32(ViewState["StockPlannerID"].ToString()) : (int?)null;

            oDiscrepancyBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
            oDiscrepancyBE.User.Site = new BusinessEntities.ModuleBE.AdminFunctions.MAS_SiteBE();
            oDiscrepancyBE.User.UserID = Convert.ToInt32(Session["UserID"]);
            oDiscrepancyBE.Location = txtLocation.Text.Trim();
            sResult = oDiscrepancyBAL.addDiscrepancyLogBAL(oDiscrepancyBE);
            oDiscrepancyBAL = null;
            if (!sResult.Contains("Discrepancy Already Inserted"))
            {
                iResult = Convert.ToInt32(sResult.Split('-')[1].ToString());

                if (iResult > 0)
                {
                    foreach (GridViewRow item in gvNoPurchaseOrder.Rows)
                    {

                        ucTextbox txtOfficeDepotCode = (ucTextbox)item.FindControl("txtOfficeDepotCode");
                        ucTextbox txtDescription = (ucTextbox)item.FindControl("txtDescription");
                        ucTextbox txtReceivedQty = (ucTextbox)item.FindControl("txtReceivedQty");


                        if (txtOfficeDepotCode.Text.Trim() != ""
                          //&& txtDescription.Text.Trim() != ""
                          && txtReceivedQty.Text.Trim() != ""
                            )
                        {

                            DiscrepancyBE oNewDiscrepancyBE = new DiscrepancyBE();
                            DiscrepancyBAL oNewDiscrepancyBAL = new DiscrepancyBAL();

                            ucTextbox txtLineNo = (ucTextbox)item.FindControl("txtLineNo");
                            ucTextbox txtVikingCode = (ucTextbox)item.FindControl("txtVikingCode");
                            ucTextbox txtVendorItemCode = (ucTextbox)item.FindControl("txtVendorItemCode");
                            ucTextbox txtUoM = (ucTextbox)item.FindControl("txtUoM");
                            ucTextbox txtOriginalPOQuantity = (ucTextbox)item.FindControl("txtOriginalPOQuantity");
                            ucTextbox txtOutstandingQty = (ucTextbox)item.FindControl("txtOutstandingQty");


                            oNewDiscrepancyBE.Action = "AddDiscrepancyItem";
                          //  oNewDiscrepancyBE.Line_no = txtLineNo.Text.Trim() != "" ? Convert.ToInt32(txtLineNo.Text) : 0;
                            oNewDiscrepancyBE.Line_no = txtLineNo.Text.Trim() != "" ? Convert.ToInt32(txtLineNo.Text) : (int?)null;
                            oNewDiscrepancyBE.DirectCode = txtVikingCode.Text;
                            oNewDiscrepancyBE.ODSKUCode = txtOfficeDepotCode.Text;
                            oNewDiscrepancyBE.VendorCode = txtVendorItemCode.Text;
                            oNewDiscrepancyBE.ProductDescription = txtDescription.Text;
                            oNewDiscrepancyBE.PurchaseOrder = new BusinessEntities.ModuleBE.Upload.Up_PurchaseOrderDetailBE();
                            oNewDiscrepancyBE.OutstandingQuantity = string.IsNullOrEmpty(txtOutstandingQty.Text) ? (int?)null : Convert.ToInt32(txtOutstandingQty.Text);
                            oNewDiscrepancyBE.OriginalQuantity = string.IsNullOrEmpty(txtOriginalPOQuantity.Text) ? (int?)null : Convert.ToInt32(txtOriginalPOQuantity.Text);
                            oNewDiscrepancyBE.UOM = txtUoM.Text;
                            oNewDiscrepancyBE.DiscrepancyLogID = iResult;
                            oNewDiscrepancyBE.DeliveredQuantity = txtReceivedQty.Text.Trim() != "" ? Convert.ToInt32(txtReceivedQty.Text.Trim()) : 0;
                            //oNewDiscrepancyBE.PurchaseOrderID = Convert.ToInt32(hdnPurchaseOrderId.Value);


                            int? iResult2 = oNewDiscrepancyBAL.addDiscrepancyItemBAL(oNewDiscrepancyBE);
                            //bSendMail = true;
                        }
                    }
                    bSendMail = true;
                    DiscrepancyLogID = iResult;
                    VDRNo = sResult.Split('-')[0].ToString();
                    InsertHTML();
                    
                }
            }
            else if (sResult.Contains("Discrepancy Already Inserted"))
            {
                //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + DiscrepancyAlreadyCreated + "')", true);
                EncryptQueryString("DIS_OptionsPopup.aspx?VDRNo=" + sResult.Split('-')[0].ToString()
                       + "&DiscrepancyLogID=" + sResult.Split('-')[1].ToString()
                       + "&CommunicationType=" + ucSDRCommunication1.innerControlRdoCommunication
                       + "&SiteId=" + ucSite.innerControlddlSite.SelectedValue
                       + "&DisDate=" + DateTime.Now.ToString("dd/MM/yyyy"));
            }
        }
        catch (Exception ex)
        {
        }
        finally
        {
            if (DiscrepancyLogID > 0)
            {
                DiscrepancyBE oNewDiscrepancyBE = new DiscrepancyBE();
                DiscrepancyBAL oNewDiscrepancyBAL = new DiscrepancyBAL();

                WorkflowHTML oWorkflowHTML = new WorkflowHTML();

                oNewDiscrepancyBE.Action = "DeleteBlankWorkflowEntries";
                oNewDiscrepancyBE.DiscrepancyLogID = DiscrepancyLogID;
                DeleteDiscrepancyTest = oNewDiscrepancyBAL.DeleteBlankWorkflowEntriesBAL(oNewDiscrepancyBE);
                if (DeleteDiscrepancyTest != 0)
                {
                    if (((RadioButton)ucSDRCommunication1.FindControl("rdoEmailComm")) != null && ((RadioButton)ucSDRCommunication1.FindControl("rdoEmailComm")).Checked)
                    {
                        int? iDiscrepancyCommunicationID = sendAndSaveDiscrepancyCommunication(iResult);
                    }

                    if (ucSDRCommunication1.innerControlRdoCommunication == 'L')   // for letter                
                        oNewDiscrepancyBE.VENHTML = oWorkflowHTML.function4("Blue", "", DateTime.Now, "", "Admin@OfficeDepot.com", "", InitialCommunicationSent, "", Convert.ToString(sLetterLink));
                    else
                        oNewDiscrepancyBE.VENHTML = oWorkflowHTML.function4("Blue", "", DateTime.Now, Convert.ToString(sSendMailLink), "Admin@OfficeDepot.com", "", InitialCommunicationSent, "", "");

                    oNewDiscrepancyBE.Action = "UpdateVENHTML";
                    oNewDiscrepancyBE.DiscrepancyLogID = DiscrepancyLogID;

                    int? UpdateVENHTML = oNewDiscrepancyBAL.UpdateVENHtmlBAL(oNewDiscrepancyBE);


                    iGridAlreadyContainsItems = 0;
                    if (ucSDRCommunication1.innerControlRdoCommunication == 'L')
                    {
                        sendCommunication communication = new sendCommunication();
                        Session["LabelHTML"] = communication.sendAndSaveLetter(DiscrepancyLogID, "nopurchaseordernumber", ViewState["VendorID"] != null ? Convert.ToInt32(ViewState["VendorID"].ToString()) : iVendorID); ;
                        sLetterLink.Append(communication.sLetterLink);
                    }
                    if (!sResult.Contains("") || !sResult.ToString().Equals(""))
                    {
                        EncryptQueryString("DIS_OptionsPopup.aspx?VDRNo=" + sResult.Split('-')[0].ToString()
                        + "&DiscrepancyLogID=" + sResult.Split('-')[1].ToString()
                        + "&CommunicationType=" + ucSDRCommunication1.innerControlRdoCommunication
                        + "&SiteId=" + ucSite.innerControlddlSite.SelectedValue
                        + "&DisDate=" + DateTime.Now.ToString("dd/MM/yyyy"));
                    }
                }
            }
        }
    }

    protected void btnPrintReturnsNote_Click(object sender, EventArgs e)
    {
        btnSave.Visible = false;
        btnBack.Visible = false;

        PrintAndSaveReturnNote();
        btnPrintReturnsNote.Visible = true;
    }
    protected void btnBack_Click(object sender, EventArgs e)
    {
        mvNoPurchaseOrder.ActiveViewIndex = 0;
        btnLog.Visible = true;
        btnBack.Visible = false;
        btnSave.Visible = false;

        txtPurchaseOrder.Text = "";
        ddlPurchaseOrderDate.SelectedIndex = 0;
        lblVendorValue.Text = "";
        ((ucTextbox)(ucSeacrhVendor1.FindControl("txtVendorNo"))).Text = "";
        ((ucLabel)(ucSeacrhVendor1.FindControl("SelectedVendorName"))).Text = "";
        rdoPO1.Checked = true;
        rdoVendor11.Checked = false;
        iGridAlreadyContainsItems = 0;
    }

    protected void ddlPurchaseOrder_SelectedIndexChanged(object sender, EventArgs e)
    {
        //lblVendorValue.Text = ddlPurchaseOrderDate.SelectedItem.Value.ToString();
        if (ViewState["GetPODateList"] != null)
        {
            List<DiscrepancyBE> lstGetPODateList = (List<DiscrepancyBE>)ViewState["GetPODateList"];
            if (!string.IsNullOrEmpty(ddlPurchaseOrderDate.SelectedItem.Value) && lstGetPODateList.Count>0)
            {
                var PODateDetail = lstGetPODateList.Find(p => p.RowNumber == Convert.ToInt32(ddlPurchaseOrderDate.SelectedItem.Value));
                lblVendorValue.Text = PODateDetail.VendorNoName;

                if (PODateDetail.VendorID > 0)
                    ucSDRCommunication1.VendorID = Convert.ToInt32(PODateDetail.VendorID);
                else
                    ucSDRCommunication1.VendorID = 0;

                if (!string.IsNullOrEmpty(ucSite.innerControlddlSite.SelectedValue))
                    ucSDRCommunication1.SiteID = Convert.ToInt32(ucSite.innerControlddlSite.SelectedValue);
                else
                    ucSDRCommunication1.SiteID = 0;
                ucSDRCommunication1.GetDetails();
            }
        }
    }

    protected void txtPurchaseOrder_TextChanged(object sender, EventArgs e)
    {
        GetPurchaseOrderList();
    }

    //protected void TextBox_TextChanged_txtReceivedQuantity(object sender, EventArgs e)
    //{
    //    mvNoPurchaseOrder.ActiveViewIndex = 1;
    //    btnLog.Visible = false;
    //    btnBack.Visible = true;
    //    btnSave.Visible = true;

    //    BaseControlLibrary.ucTextbox currentTextBox = (BaseControlLibrary.ucTextbox)sender;

    //    int dReceivedQuantity;
    //    bool isdReceivedQuantityDecimal = int.TryParse(currentTextBox.Text.Trim(), out dReceivedQuantity);

    //    if (currentTextBox.Text.Trim() == "")
    //    {
    //        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + ReceivedQuantity + "')", true);
    //        return;
    //    }

    //    if (currentTextBox.Text.Trim() != "" && !isdReceivedQuantityDecimal)
    //    {

    //        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + InvalidValuesEntered + "')", true);
    //        return;
    //    }

    //    if (currentTextBox.Text.Trim() != ""
    //        && isdReceivedQuantityDecimal
    //        && !currentTextBox.Text.IsNumeric())
    //    {

    //        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + InvalidValuesEntered + "')", true);
    //        return;
    //    }
    //}

    protected void btnOk_Click(object sender, EventArgs e)
    {
        mvNoPurchaseOrder.ActiveViewIndex = 1;
        btnLog.Visible = false;
        btnBack.Visible = true;
        btnSave.Visible = true;

        //TextBox currentTextBox = (TextBox)(Session["currentTextBox"]);

        GridViewRow currentGridViewRow = (GridViewRow)currentTextBox.Parent.Parent;
        int ind = currentGridViewRow.RowIndex;
        GridViewRow row = gvNoPurchaseOrder.Rows[ind];

        ucTextbox txtLineNo = (ucTextbox)row.FindControl("txtLineNo");
        ucTextbox txtOfficeDepotCode = (ucTextbox)row.FindControl("txtOfficeDepotCode");
        ucTextbox txtVikingCode = (ucTextbox)row.FindControl("txtVikingCode");
        ucTextbox txtVendorItemCode = (ucTextbox)row.FindControl("txtVendorItemCode");
        ucTextbox txtDescription = (ucTextbox)row.FindControl("txtDescription");
        ucTextbox txtReceivedQty = (ucTextbox)row.FindControl("txtReceivedQty");
        ucTextbox txtUoM = (ucTextbox)row.FindControl("txtUoM");
        ucButton btnRemove = (ucButton)row.FindControl("btnRemove");
        ucTextbox txtOriginalPOQuantity = (ucTextbox)row.FindControl("txtOriginalPOQuantity");
        ucTextbox txtOutstandingQty = (ucTextbox)row.FindControl("txtOutstandingQty");

        txtLineNo.Text = "";
        txtVikingCode.Text = "";
        txtVendorItemCode.Text = "";
        txtDescription.Text = "";
        txtUoM.Text = "";
        txtOriginalPOQuantity.Text = "";
        txtOutstandingQty.Text = "";

        txtLineNo.Enabled = true;
        txtOfficeDepotCode.Enabled = true;
        txtVikingCode.Enabled = true;
        txtVendorItemCode.Enabled = true;
        txtDescription.Enabled = true;
        txtUoM.Enabled = true;
        txtOutstandingQty.Enabled = true;
        txtOriginalPOQuantity.Enabled = true;
        btnRemove.Style["display"] = "";

        currentTextBox = null;
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        mvNoPurchaseOrder.ActiveViewIndex = 1;
        btnLog.Visible = false;
        btnBack.Visible = true;
        btnSave.Visible = true;

        //TextBox currentTextBox = (TextBox)(Session["currentTextBox"]);

        GridViewRow currentGridViewRow = (GridViewRow)currentTextBox.Parent.Parent;
        int ind = currentGridViewRow.RowIndex;
        GridViewRow row = gvNoPurchaseOrder.Rows[ind];

        ucTextbox txtLineNo = (ucTextbox)row.FindControl("txtLineNo");
        ucTextbox txtOfficeDepotCode = (ucTextbox)row.FindControl("txtOfficeDepotCode");
        ucTextbox txtVikingCode = (ucTextbox)row.FindControl("txtVikingCode");
        ucTextbox txtVendorItemCode = (ucTextbox)row.FindControl("txtVendorItemCode");
        ucTextbox txtDescription = (ucTextbox)row.FindControl("txtDescription");
        ucTextbox txtReceivedQty = (ucTextbox)row.FindControl("txtReceivedQty");
        ucTextbox txtUoM = (ucTextbox)row.FindControl("txtUoM");
        ucButton btnRemove = (ucButton)row.FindControl("btnRemove");
        ucTextbox txtOriginalPOQuantity = (ucTextbox)row.FindControl("txtOriginalPOQuantity");
        ucTextbox txtOutstandingQty = (ucTextbox)row.FindControl("txtOutstandingQty");

        txtLineNo.Text = "";
        txtVikingCode.Text = "";
        txtVendorItemCode.Text = "";
        txtDescription.Text = "";
        txtUoM.Text = "";
        txtOriginalPOQuantity.Text = "";
        txtOutstandingQty.Text = "";
        txtOfficeDepotCode.Text = "";

        txtLineNo.Enabled = true;
        txtOfficeDepotCode.Enabled = true;
        //txtVikingCode.Enabled = false;
        //txtVendorItemCode.Enabled = false;
        //txtDescription.Enabled = false;
        //txtUoM.Enabled = false;
        //txtOutstandingQty.Enabled = false;
        //txtOriginalPOQuantity.Enabled = false;
        btnRemove.Style["display"] = "";

        currentTextBox = null;
    }

    protected void TextBox_TextChanged_txtOfficeDepotCode(object sender, EventArgs e)
    {
        mvNoPurchaseOrder.ActiveViewIndex = 1;
        btnLog.Visible = false;
        btnBack.Visible = true;
        btnSave.Visible = true;

        //TextBox currentTextBox = (TextBox)sender;
        //Session["currentTextBox"] = currentTextBox;
        currentTextBox = (BaseControlLibrary.ucTextbox)sender;

        GridViewRow currentGridViewRow = (GridViewRow)currentTextBox.Parent.Parent;
        int ind = currentGridViewRow.RowIndex;
        GridViewRow row = gvNoPurchaseOrder.Rows[ind];

        ucTextbox txtLineNo = (ucTextbox)row.FindControl("txtLineNo");
        ucTextbox txtOfficeDepotCode = (ucTextbox)row.FindControl("txtOfficeDepotCode");
        ucTextbox txtVikingCode = (ucTextbox)row.FindControl("txtVikingCode");
        ucTextbox txtVendorItemCode = (ucTextbox)row.FindControl("txtVendorItemCode");
        ucTextbox txtDescription = (ucTextbox)row.FindControl("txtDescription");
        ucTextbox txtReceivedQty = (ucTextbox)row.FindControl("txtReceivedQty");
        ucTextbox txtUoM = (ucTextbox)row.FindControl("txtUoM");
        ucButton btnRemove = (ucButton)row.FindControl("btnRemove");
        ucTextbox txtOriginalPOQuantity = (ucTextbox)row.FindControl("txtOriginalPOQuantity");
        ucTextbox txtOutstandingQty = (ucTextbox)row.FindControl("txtOutstandingQty");

        //if no product code exists in the textbox 
        //just reset the controls at initials
        if (currentTextBox.Text == "" || currentTextBox.Text == string.Empty)
        {
            txtLineNo.Text = "";
            txtOfficeDepotCode.Text = "";
            txtVikingCode.Text = "";
            txtVendorItemCode.Text = "";
            txtDescription.Text = "";
            txtUoM.Text = "";
            txtOriginalPOQuantity.Text = "";
            txtOutstandingQty.Text = "";

            //txtLineNo.Enabled = false;
            txtOfficeDepotCode.Enabled = true;
            //txtVikingCode.Enabled = false;
            //txtVendorItemCode.Enabled = false;
            //txtDescription.Enabled = false;
            //txtUoM.Enabled = false;
            //txtOutstandingQty.Enabled = false;
            //txtOriginalPOQuantity.Enabled = false;
            btnRemove.Style["display"] = "";
            return;
        }

        DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
        DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();

        oDiscrepancyBE.Action = "GetODProductBasedSiteVendorIds";
        oDiscrepancyBE.ODSKUCode = currentTextBox.Text;
        oDiscrepancyBE.SiteID = !string.IsNullOrWhiteSpace(ucSite.innerControlddlSite.SelectedValue) ? Convert.ToInt32(ucSite.innerControlddlSite.SelectedValue) : 0;
        oDiscrepancyBE.VendorID = !string.IsNullOrWhiteSpace(ucSeacrhVendor1.VendorNo) ? Convert.ToInt32(ucSeacrhVendor1.VendorNo) : 0;
        oDiscrepancyBE.PurchaseOrderNumber = lblPurchaseOrderValue.Text;

        List<DiscrepancyBE> lstODProductDetails = oDiscrepancyBAL.GetODProductDetailsBySiteVendorBAL(oDiscrepancyBE);
        if (lstODProductDetails == null || (lstODProductDetails != null && lstODProductDetails.Count == 0))
        {
            mdlNoPurchaseOrderViewer.Show();
        }
        else
        {
            //txtLineNo.Text = "";
            txtVikingCode.Text = lstODProductDetails[0].DirectCode;
            txtVendorItemCode.Text = lstODProductDetails[0].VendorCode;
            txtDescription.Text = lstODProductDetails[0].ProductDescription;
            txtUoM.Text = lstODProductDetails[0].UOM;
            txtOutstandingQty.Text = lstODProductDetails[0].OutstandingQuantity.ToString();
            txtOriginalPOQuantity.Text = lstODProductDetails[0].OriginalQuantity.ToString();

            txtLineNo.Enabled = true;
            //txtOfficeDepotCode.Enabled = false;
            //txtVikingCode.Enabled = false;
            //txtVendorItemCode.Enabled = false;
            //txtDescription.Enabled = false;
            //txtUoM.Enabled = false;
            btnRemove.Style["display"] = "";
        }
    }

    protected void gvNoPurchaseOrder_Sorting(object sender, GridViewSortEventArgs e)
    {
        if (ViewState["myDataTable"] != null)
        {
            DataTable myDataTable = (DataTable)ViewState["myDataTable"];
            ViewState["sortDirection"] = ViewState["sortDirection"] == null || Convert.ToString(ViewState["sortDirection"]) == "DESC" ? "ASC" : "DESC";
            DataView view = myDataTable.DefaultView;
            view.Sort = e.SortExpression + " " + Convert.ToString(ViewState["sortDirection"]);
            gvNoPurchaseOrder.DataSource = view;
            gvNoPurchaseOrder.DataBind();

        }
    }

    public override BusinessEntities.BaseBe[] SubSort(GridViewSortEventArgs e)
    {
        return Utilities.GenericListHelper<DiscrepancyBE>.SortList((List<DiscrepancyBE>)ViewState["lstSites"], e.SortExpression, e.SortDirection).ToArray();
    }

    protected void btnSave_1_Click(object sender, EventArgs e)
    {
        DiscrepancyBE discrepancyBE = null;
        DiscrepancyBAL discrepancyBAL = null;

        if (!string.IsNullOrEmpty(txtUserComments.Text) && !string.IsNullOrWhiteSpace(txtUserComments.Text))
        {
            lblErrorMsg.Text = string.Empty;
            discrepancyBE = new DiscrepancyBE();
            discrepancyBE.Action = "AddDiscrepancyLogComment";

            if (!string.IsNullOrEmpty(GetQueryStringValue("disLogID")))
                discrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID"));
            else
                discrepancyBE.DiscrepancyLogID = 0;

            discrepancyBE.UserID = Convert.ToInt32(Session["UserID"]);
            discrepancyBE.CommentDateTime = DateTime.Now;
            discrepancyBE.Comment = txtUserComments.Text;
            discrepancyBAL = new DiscrepancyBAL();
            discrepancyBAL.AddDiscrepancyLogCommentBAL(discrepancyBE);
            discrepancyBE.Action = "GetDiscrepancyLogComment";
            this.BindDiscrepancyLogComment(discrepancyBE);

            discrepancyBE.DiscrepancyType = "nopurchaseordernumber";
            int vendorID = 0;
            if (ViewState["VendorID"] != null)
                vendorID = Convert.ToInt32(ViewState["VendorID"]);
            new SendCommentMail().SendCommentsMail(discrepancyBE,
                (BaseControlLibrary.ucTextbox)ucSDRCommunication1.FindControl("txtAltEmail"),
                (BaseControlLibrary.ucTextbox)ucSDRCommunication1.FindControl("ucVendorEmailList"),
                vendorID, txtUserComments.Text, Session["Role"].ToString());

            mdlAddComment.Hide();
        }
        else
        {
            lblErrorMsg.Text = WebCommon.getGlobalResourceValue("MustEnterComment");
            mdlAddComment.Show();
        }

        btnSave.Visible = false;
        btnBack.Visible = false;
        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "Reload()", true);
    }

    protected void btnBack_2_Click(object sender, EventArgs e)
    {
        mdlAddComment.Hide();
        btnSave.Visible = false;
        btnBack.Visible = false;

        DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();
        DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
        oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID"));
        oDiscrepancyBE.Action = "GetDiscrepancyReturnNoteCount";
        DataTable dtReturnNoteCount = oDiscrepancyBAL.GetDiscrepancyReturnNoteCountBAL(oDiscrepancyBE);
        if (dtReturnNoteCount != null && dtReturnNoteCount.Rows.Count > 0)
        {
            if (Convert.ToInt32(dtReturnNoteCount.Rows[0].ItemArray[0].ToString()) > 0)
                btnPrintReturnsNote.Visible = true;
        }
        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "Reload()", true);
    }

    protected void btnAddComment_Click(object sender, EventArgs e)
    {
        txtUserComments.Text = string.Empty;
        lblErrorMsg.Text = string.Empty;
        lblUserT.Text = Convert.ToString(Session["UserName"]);
        lblDateT.Text = DateTime.Now.Date.ToString("dd/MM/yyyy");
        if (GetQueryStringValue("VDRNo") != null)
        {
            lblDiscrepancyNoT.Text = Convert.ToString(GetQueryStringValue("VDRNo"));
        }
        mdlAddComment.Show();
        txtUserComments.Focus();
        btnSave.Visible = false;
        btnBack.Visible = false;

    }

    protected void btnAddImages_Click(object sender, EventArgs e)
    {

        EncryptQueryString("DIS_OptionsPopup.aspx?Mode=Edit&VDRNo=" + GetQueryStringValue("VDRNo").ToString() + "&DiscrepancyLogID=" + GetQueryStringValue("disLogID").ToString() + "&CommunicationType=" + Common.DiscrepancyType.NoPurchaseOrder);
    }
    /* ENHANCEMENT FOR PHASE R3 */
    /*RAISE QUERY BUTTON*/
    protected void btnRaiseQuery_Click(object sender, EventArgs e)
    {
        try
        {
            var queryDiscrepancyBAL = new DISLog_QueryDiscrepancyBAL();
            var queryDiscrepancyBE = new DISLog_QueryDiscrepancyBE();
            var discrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID"));

            if (!this.IsAlreadyDisputeExist(discrepancyLogID) || !this.IsQueryClosed(discrepancyLogID))
            {
                btnSave.Visible = false;
                btnBack.Visible = false;
                mdlRaiseQueryConfirmMsg.Show();
            }
            else
            {

                lblMessageFirst.Text = strThereIsCurrentlyAlreadyAnOpenQueryBeingInvestigated;
                lblMessageSecond.Text = strPleaseWaitUntilYouHaveReceivedAReplyForThisBefore;
                btnSave.Visible = false;
                btnBack.Visible = false;
                mdlShowError.Show();
            }

        }

        catch (Exception ex)
        {
            LogUtility.SaveErrorLogEntry(ex);
        }
        
        
    }

    #region Alternate email code ...

    protected void btnSetAlternateEmail_Click(object sender, EventArgs e)
    {
        rfvEmailAddressCanNotBeEmpty.Enabled = false;
        var txtAltEmail = (ucTextbox)ucSDRCommunication1.FindControl("txtAltEmail");
        if (txtAltEmail != null)
        {
            txtAltEmail.Text = txtAlternateEmailText.Text;
            txtAlternateEmailText.Text = string.Empty;
        }

        if (Session["Role"].ToString().ToLower() == "vendor")
        {
            string LoginPath = ResolveClientUrl("../../../ModuleUI/Security/Login.aspx");
            Response.Redirect(LoginPath);
        }
        else
        {
            this.Save();
        }
        if (DeleteDiscrepancyTest == 0)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + DeleteBlankWorkflowEntriesText + "')", true);
            iGridAlreadyContainsItems = 1;
            System.Threading.Thread.Sleep(30000);
        }
    }

    protected void btnAddEmailId_Click(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(lblMultyAlternateEmail.Text) && !string.IsNullOrWhiteSpace(lblMultyAlternateEmail.Text))
        {
            if (!lblMultyAlternateEmail.Text.Contains(txtAlternateEmailText.Text))
            {
                lblMultyAlternateEmail.Text = string.Format("{0}, {1}", lblMultyAlternateEmail.Text, txtAlternateEmailText.Text);
            }
        }
        else
        {
            lblMultyAlternateEmail.Text = txtAlternateEmailText.Text;
        }

        lblMultyAlternateEmail.Visible = true;
        txtAlternateEmailText.Text = string.Empty;
        txtAlternateEmailText.Focus();
        mdlAlternateEmail.Show();
    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        txtAlternateEmailText.Text = string.Empty;
        lblMultyAlternateEmail.Text = string.Empty;
        txtAlternateEmailText.Focus();
        mdlAlternateEmail.Show();
    }

    #endregion

    #endregion

    #region Methods
    private void GetAccessRightRaiseQuery()
    {
        DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
        DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();
        oDiscrepancyBE.Action = "CheckUserRaiseQueryRight";
        oDiscrepancyBE.UserID = Convert.ToInt32(Session["UserID"]);
        oDiscrepancyBE.SiteID = Convert.ToInt32(Convert.ToString(hdnSiteID.Value));
        int? lstDetails = oDiscrepancyBAL.getAccessRightRaiseQueryBAL(oDiscrepancyBE);
        if (lstDetails > 0)
        {
            btnRaiseQuery.Visible = true;
        }
        else
        {
            btnRaiseQuery.Visible = false;
        }
    }

    private void AddColumns()
    {
        tbDummy.Columns.Add("Line_No");
        tbDummy.Columns.Add("OD_Code");
        tbDummy.Columns.Add("Direct_code");
        tbDummy.Columns.Add("Vendor_Code");
        tbDummy.Columns.Add("ProductDescription");
        tbDummy.Columns.Add("Original_quantity");
        tbDummy.Columns.Add("Outstanding_Qty");
        tbDummy.Columns.Add("ReceivedQty");
        tbDummy.Columns.Add("UOM");
        tbDummy.Columns.Add("Button");
        tbDummy.Columns.Add("RecordID");
    }

    private void BindWithGrid()
    {
        //add this table to dataset
        dsDummy.Tables.Add(tbDummy);

        //bind this dataset to grid
        gvNoPurchaseOrder.DataSource = dsDummy;
        gvNoPurchaseOrder.DataBind();
        ViewState["myDataTable"] = tbDummy;
    }

    private void GetDeliveryDetails()
    {
        //lblSiteValue.Text = ucSite.innerControlddlSite.SelectedItem.Text;
        //lblPurchaseOrderValue.Text = txtPurchaseOrder.Text;
        //lblDateReceivedValue.Text = lblIssueRaisedDate.Text;
        //lblDeliveryNoValue.Text = txtDeliveryNumber.Text;
        //lblDeliveryDateValue.Text = txtDateDeliveryArrived.Text;
        //lblPurchaseOrderDateValue.Text =txtPurchaseOrder.Text.Trim() != string.Empty ? ddlPurchaseOrderDate.SelectedItem.Text : "";

        if (GetQueryStringValue("disLogID") != null)
        {
            DiscrepancyBE oNewDiscrepancyBE = new DiscrepancyBE();
            DiscrepancyBAL oNewDiscrepancyBAL = new DiscrepancyBAL();
            oNewDiscrepancyBE.Action = "GetDiscrepancyLog";
            oNewDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
            if (GetQueryStringValue("status").ToLower() == "deleted")
            {
                oNewDiscrepancyBE.IsDelete = true;
            }
            List<DiscrepancyBE> lstDisLog = oNewDiscrepancyBAL.GetDiscrepancyLogDetailsBAL(oNewDiscrepancyBE);
            oNewDiscrepancyBAL = null;
            if (lstDisLog != null && lstDisLog.Count > 0)
            {
                hdnSiteID.Value = Convert.ToString(lstDisLog[0].Site.SiteID);
                lblSiteValue.Text = lstDisLog[0].Site.SiteName;
                lblPurchaseOrderValue.Text = lstDisLog[0].PurchaseOrderNumber;
                lblPurchaseOrderDateValue.Text = "- " + (Convert.ToDateTime(lstDisLog[0].PurchaseOrderDate)).ToString("dd/MM/yyyy");
                lblDeliveryNoValue.Text = lstDisLog[0].DeliveryNoteNumber.ToString();
                lblDeliveryDateValue.Text = "- " + (Convert.ToDateTime(lstDisLog[0].DeliveryArrivedDate)).ToString("dd/MM/yyyy");

                lblDateReceivedValue.Text = "- " + (Convert.ToDateTime(lstDisLog[0].DeliveryArrivedDate)).ToString("dd/MM/yyyy");

                txtInternalComments.Text = lstDisLog[0].InternalComments.ToString();
                txtInternalComments.ReadOnly = true;
                lblVendorNumberValue.Text = lstDisLog[0].VendorNoName.ToString();
                //lblContactValue.Text = lstDisLog[0].Vendor.VendorContactNumber.ToString();
                lblContactValue.Text = UIUtility.GetUserPhoneNoofVendor(Convert.ToInt32(lstDisLog[0].VendorID), Convert.ToInt32(lstDisLog[0].SiteID));
                txtLocation.Text = lstDisLog[0].Location.ToString();
                lblStockPlannerNoValue.Text = lstDisLog[0].StockPlannerNO.ToString();
                lblPlannerContactNoValue.Text = lstDisLog[0].StockPlannerContact.ToString();
                VDRNo = lstDisLog[0].VDRNo.ToString();
                SiteID = lstDisLog[0].Site.SiteID;
                ucSDRCommunication1.VendorEmailList = lstDisLog[0].CommunicationTo;
                ucSDRCommunication1.innerControlRdoCommunication = lstDisLog[0].CommunicationType.Value;
                ucSDRCommunication1.innerControlRdoLetter.Enabled = false;
                ucSDRCommunication1.innerControlRdoEmail.Enabled = false;
                ucSDRCommunication1.innerControlEmailList.Enabled = false;
                ucSDRCommunication1.innerControlAltEmailList.Enabled = false;

                ViewState["StockPlannerEmailID"] = lstDisLog[0].StockPlannerEmailID;
                hdnCostCenter.Value = lstDisLog[0].Site.CostCenter;

                lblSPActionComments.Text = "Comments : " + lstDisLog[0].ActionComments;

                lblDecision.Text = "Decision :  " + lstDisLog[0].Decision;

                if (!string.IsNullOrEmpty(lstDisLog[0].ActionComments) && !string.IsNullOrEmpty(lstDisLog[0].Decision))
                {
                    pnlDecisionComments.Visible = true;
                }
            }
        }
        else
        {
            hdnSiteID.Value = Convert.ToString(ucSite.innerControlddlSite.SelectedItem.Value);
            lblSiteValue.Text = ucSite.innerControlddlSite.SelectedItem.Text;
            lblPurchaseOrderValue.Text = txtPurchaseOrder.Text;
            lblDateReceivedValue.Text = lblIssueRaisedDate.Text;
            lblDeliveryNoValue.Text = txtDeliveryNumber.Text;
            lblDeliveryDateValue.Text = txtDateDeliveryArrived.innerControltxtDate.Value;
            lblPurchaseOrderDateValue.Text = txtPurchaseOrder.Text.Trim() != string.Empty ? ddlPurchaseOrderDate.SelectedItem.Text : "";
        }
    }

    private void GetContactPurchaseOrderDetails()
     {

        DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
        DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();
        if (GetQueryStringValue("disLogID") != null)
        {

            gvNoPurchaseOrderViewMode.Style["display"] = "";
            gvNoPurchaseOrder.Style["display"] = "none";

            oDiscrepancyBE.Action = "GetDiscrepancyItem";
            oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
            List<DiscrepancyBE> lstDetails = oDiscrepancyBAL.GetDiscrepancyItemDetailsBAL(oDiscrepancyBE);
            oDiscrepancyBAL = null;
            if (lstDetails.Count > 0 && lstDetails != null)
            {
                Session["PurchaseOrderID"] = lstDetails[0].PurchaseOrderID;
                gvNoPurchaseOrderViewMode.DataSource = lstDetails;
                gvNoPurchaseOrderViewMode.DataBind();
                ViewState["lstSites"] = lstDetails;
                ViewState["PurchaseOrderID"] = lstDetails[0].PurchaseOrderID != null ? Convert.ToInt32(lstDetails[0].PurchaseOrderID.ToString()) : (int?)null;
                ViewState["StockPlannerID"] = lstDetails[0].StockPlannerID != null ? Convert.ToInt32(lstDetails[0].StockPlannerID.ToString()) : (int?)null;
                ViewState["VendorID"] = lstDetails[0].VendorID != null ? Convert.ToInt32(lstDetails[0].VendorID.ToString()) : (int?)null;
                ViewState["ActualStockPlannerID"] = lstDetails[0].ActualStockPlannerID != null ?
                                                    Convert.ToInt32(lstDetails[0].ActualStockPlannerID.ToString()) : (int?)null; // Stock Planner ID while discrepancy is logged
            }

            DiscrepancyBE DiscrepancyBE = new DiscrepancyBE();
            DiscrepancyBAL DiscrepancyBAL = new DiscrepancyBAL();

            DiscrepancyBE.Action = "GetActualStockPlannerDetailsBySPId"; // getting details of SP while discrepancy is logged

            if (ViewState["ActualStockPlannerID"] != null)
                DiscrepancyBE.StockPlannerID = Convert.ToInt32(ViewState["ActualStockPlannerID"].ToString());
           
            List<DiscrepancyBE> lstActualStockPlannerDetailsBySPId = DiscrepancyBAL.GetActualStockPlannerDetailsBAL(DiscrepancyBE);

            if (lstActualStockPlannerDetailsBySPId.Count > 0)
            {
                lblStockPlannerNoValue.Text = lstActualStockPlannerDetailsBySPId[0].StockPlannerNO.ToString();
                lblPlannerContactNoValue.Text = lstActualStockPlannerDetailsBySPId[0].StockPlannerContact.ToString();
            }
            
        }

        else
        {

            gvNoPurchaseOrderViewMode.Style["display"] = "none";
            gvNoPurchaseOrder.Style["display"] = "";

            if (rdoPO1.Checked)
            {
                oDiscrepancyBE.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();

                oDiscrepancyBE.Action = "GetContactPurchaseOrderDetails";
                oDiscrepancyBE.PurchaseOrderNumber = txtPurchaseOrder.Text.Trim();
                oDiscrepancyBE.PurchaseOrder = new BusinessEntities.ModuleBE.Upload.Up_PurchaseOrderDetailBE();
                oDiscrepancyBE.PurchaseOrder.Order_raised = Common.GetMM_DD_YYYY(ddlPurchaseOrderDate.SelectedItem.Text);
                oDiscrepancyBE.SiteID = Convert.ToInt32(ucSite.innerControlddlSite.SelectedItem.Value);


                List<DiscrepancyBE> lstDetails = oDiscrepancyBAL.GetContactPurchaseOrderDetailsBAL(oDiscrepancyBE);
                oDiscrepancyBAL = null;
                if (lstDetails.Count > 0 && lstDetails != null)
                {
                    if (!string.IsNullOrEmpty(ddlPurchaseOrderDate.SelectedItem.Value))
                    {
                        List<DiscrepancyBE> lstGetPODateList = (List<DiscrepancyBE>)ViewState["GetPODateList"];
                        var PODateDetail = lstGetPODateList.Find(p => p.RowNumber == Convert.ToInt32(ddlPurchaseOrderDate.SelectedItem.Value));
                        lblVendorNumberValue.Text = PODateDetail.VendorNoName;
                        ucSDRCommunication1.VendorID = Convert.ToInt32(PODateDetail.VendorID);
                        if (!string.IsNullOrEmpty(ucSite.innerControlddlSite.SelectedValue))
                            ucSDRCommunication1.SiteID = Convert.ToInt32(ucSite.innerControlddlSite.SelectedValue);
                        else
                            ucSDRCommunication1.SiteID = 0;
                    }

                    //lblVendorNumberValue.Text = ddlPurchaseOrderDate.SelectedItem.Value.ToString();
                    //lblContactValue.Text = lstDetails[0].Vendor.VendorContactNumber.ToString();
                    lblContactValue.Text = UIUtility.GetUserPhoneNoofVendor(Convert.ToInt32(lstDetails[0].VendorID), Convert.ToInt32(ucSite.innerControlddlSite.SelectedItem.Value));

                    lblStockPlannerNoValue.Text = lstDetails[0].StockPlannerNO.ToString();
                    lblPlannerContactNoValue.Text = lstDetails[0].StockPlannerContact.ToString();

                    //ucSDRCommunication1.VendorID = Convert.ToInt32(lstDetails[ddlPurchaseOrderDate.SelectedIndex].VendorID);
                    //if (!string.IsNullOrEmpty(ucSite.innerControlddlSite.SelectedValue))
                    //    ucSDRCommunication1.SiteID = Convert.ToInt32(ucSite.innerControlddlSite.SelectedValue);
                    //else
                    //    ucSDRCommunication1.SiteID = 0;
                    ucSDRCommunication1.GetDetails();

                    ViewState["PurchaseOrderID"] = lstDetails[0].PurchaseOrderID != null ? Convert.ToInt32(lstDetails[0].PurchaseOrderID.ToString()) : (int?)null;
                    ViewState["StockPlannerID"] = lstDetails[0].StockPlannerID != null ? Convert.ToInt32(lstDetails[0].StockPlannerID.ToString()) : (int?)null;
                    ViewState["VendorID"] = lstDetails[0].VendorID != null ? Convert.ToInt32(lstDetails[0].VendorID.ToString()) : (int?)null;

                    AddColumns();
                    int iCount = 1;
                    foreach (DiscrepancyBE item in lstDetails)
                    {
                        tbDummy.Rows.Add(item.PurchaseOrder.Line_No,
                                         item.PurchaseOrder.OD_Code,
                                         item.PurchaseOrder.Direct_code,
                                         item.PurchaseOrder.Vendor_Code,
                                         item.PurchaseOrder.ProductDescription,
                                         item.PurchaseOrder.Original_quantity,
                                         item.PurchaseOrder.Outstanding_Qty,
                                         "",
                                         item.PurchaseOrder.UOM,
                                         "Remove",
                                         (iCount++).ToString());
                    }
                    dsDummy.Tables.Add(tbDummy);
                    gvNoPurchaseOrder.DataSource = dsDummy;
                    gvNoPurchaseOrder.DataBind();
                    ViewState["myDataTable"] = tbDummy;

                    EnableDisableGridControls();


                    DiscrepancyBE DiscrepancyBE = new DiscrepancyBE();
                    DiscrepancyBAL DiscrepancyBAL = new DiscrepancyBAL();

                    DiscrepancyBE.Action = "GetStockPlannerOnHolidayCover"; // getting details of SP on Holiday Cover if any

                    if (ViewState["StockPlannerID"] != null)
                        DiscrepancyBE.StockPlannerID = Convert.ToInt32(ViewState["StockPlannerID"].ToString());

                    DiscrepancyBE.SiteID = Convert.ToInt32(ucSite.innerControlddlSite.SelectedItem.Value);
                    List<DiscrepancyBE> lstStockPlannerOnHolidayCover = DiscrepancyBAL.GetStockPlannerOnHolidayCoverBAL(DiscrepancyBE);

                    if (lstStockPlannerOnHolidayCover.Count > 0)
                    {
                        ViewState["StockPlannerToCoverEmailID"] = lstStockPlannerOnHolidayCover[0].StockPlannerToCoverEmailID;
                        ViewState["SPToCoverStockPlannerID"] = lstStockPlannerOnHolidayCover[0].SPToCoverStockPlannerID;
                        //lblStockPlannerNoValue.Text = lstStockPlannerOnHolidayCover[0].StockPlannerNO.ToString();// lstDetails.Where(a => a.StockPlannerNO != "").FirstOrDefault().ToString();//
                        //lblPlannerContactNoValue.Text = lstStockPlannerOnHolidayCover[0].StockPlannerContact.ToString();
                    }

                }
            }
            else
            {
                UP_VendorBE oUP_VendorBE = new UP_VendorBE();
                UP_VendorBAL oUP_VendorBAL = new UP_VendorBAL();

                oUP_VendorBE.VendorID = Convert.ToInt32(ucSeacrhVendor1.VendorNo);
                oUP_VendorBE.Action = "ShowVendorByID";

                List<UP_VendorBE> lstDetails = new List<UP_VendorBE>();
                lstDetails = oUP_VendorBAL.GetVendorByIdBAL(oUP_VendorBE);
                oUP_VendorBAL = null;
                if (lstDetails.Count > 0 && lstDetails != null)
                {

                    lblVendorNumberValue.Text = lstDetails[0].VendorName.ToString();
                    //lblContactValue.Text = lstDetails[0].VendorContactNumber.ToString();
                    lblContactValue.Text = UIUtility.GetUserPhoneNoofVendor(Convert.ToInt32(lstDetails[0].VendorID), Convert.ToInt32(ucSite.innerControlddlSite.SelectedItem.Value));

                    //lblStockPlannerNoValue.Text = lstDetails[0].StockPlannerNumber.ToString();
                    lblStockPlannerNoValue.Text = string.Format("{0} - {1}", lstDetails[0].StockPlannerNumber, lstDetails[0].StockPlannerName);
                    lblPlannerContactNoValue.Text = lstDetails[0].StockPlannerContact.ToString();

                    ViewState["StockPlannerID"] = lstDetails[0].StockPlannerID != null ? Convert.ToInt32(lstDetails[0].StockPlannerID.ToString()) : (int?)null;
                    ViewState["VendorID"] = lstDetails[0].VendorID != null ? Convert.ToInt32(lstDetails[0].VendorID.ToString()) : (int?)null;
                    ucSDRCommunication1.VendorID = Convert.ToInt32(lstDetails[0].VendorID.ToString());
                    if (!string.IsNullOrEmpty(ucSite.innerControlddlSite.SelectedValue))
                        ucSDRCommunication1.SiteID = Convert.ToInt32(ucSite.innerControlddlSite.SelectedValue);
                    else
                        ucSDRCommunication1.SiteID = 0;
                    ucSDRCommunication1.GetDetails();

                    oDiscrepancyBE.Action = "GetStockPlannerOnHolidayCover"; // getting details of SP on Holiday Cover if any

                    if (ViewState["StockPlannerID"] != null)
                        oDiscrepancyBE.StockPlannerID = Convert.ToInt32(ViewState["StockPlannerID"].ToString());

                    oDiscrepancyBE.SiteID = Convert.ToInt32(ucSite.innerControlddlSite.SelectedItem.Value);
                    List<DiscrepancyBE> lstStockPlannerOnHolidayCover = oDiscrepancyBAL.GetStockPlannerOnHolidayCoverBAL(oDiscrepancyBE);

                    if (lstStockPlannerOnHolidayCover.Count > 0)
                    {
                        ViewState["StockPlannerToCoverEmailID"] = lstStockPlannerOnHolidayCover[0].StockPlannerToCoverEmailID;
                        ViewState["SPToCoverStockPlannerID"] = lstStockPlannerOnHolidayCover[0].SPToCoverStockPlannerID;
                        //lblStockPlannerNoValue.Text = lstStockPlannerOnHolidayCover[0].StockPlannerNO.ToString();// lstDetails.Where(a => a.StockPlannerNO != "").FirstOrDefault().ToString();//
                        //lblPlannerContactNoValue.Text = lstStockPlannerOnHolidayCover[0].StockPlannerContact.ToString();
                    }

                }
            }

            //foreach (GridViewRow row in gvNoPurchaseOrder.Rows) {
            //    TextBox txtbox = (TextBox)row.FindControl("txtDeliveryQty");

            //    sp1.RegisterPostBackControl(txtbox);
            //}
        }
    }

    private void GetPurchaseOrderList()
    {
        DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
        DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();

        oDiscrepancyBE.Action = "GetPurchaseOrderDate";
        oDiscrepancyBE.SiteID = Convert.ToInt32(ucSite.innerControlddlSite.SelectedValue);
        oDiscrepancyBE.PurchaseOrderNumber = txtPurchaseOrder.Text.Trim();

        List<DiscrepancyBE> lstDetails = oDiscrepancyBAL.GetPurchaseOrderDateListBAL(oDiscrepancyBE);
        ViewState["GetPODateList"] = lstDetails;
        oDiscrepancyBAL = null;
        if (lstDetails.Count > 0 && lstDetails != null)
        {
            FillControls.FillDropDown(ref ddlPurchaseOrderDate, lstDetails, "PurchaseOrder_Order_raised", "RowNumber");
            if (!string.IsNullOrEmpty(ddlPurchaseOrderDate.SelectedItem.Value))
            {
                var PODateDetail = lstDetails.Find(p => p.RowNumber == Convert.ToInt32(ddlPurchaseOrderDate.SelectedItem.Value));
                lblVendorValue.Text = PODateDetail.VendorNoName;
                iVendorID = Convert.ToInt32(PODateDetail.VendorID);
                ucSDRCommunication1.VendorID = Convert.ToInt32(PODateDetail.VendorID);
            }

            //lblVendorValue.Text = ddlPurchaseOrderDate.SelectedItem.Value.ToString();
            //iVendorID = Convert.ToInt32(lstDetails[ddlPurchaseOrderDate.SelectedIndex].VendorID);

        }
        else
        {
            if (rdoPO1.Checked)
            {
                string saveMessage = WebCommon.getGlobalResourceValue("InvalidPurchaseOrder");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('" + saveMessage + "')", true);
            }
        }
    }

    private void EnableDisableGridControls()
    {
        foreach (GridViewRow item in gvNoPurchaseOrder.Rows)
        {

            ucTextbox txtLineNo = (ucTextbox)item.FindControl("txtLineNo");
            ucTextbox txtOfficeDepotCode = (ucTextbox)item.FindControl("txtOfficeDepotCode");
            ucTextbox txtVikingCode = (ucTextbox)item.FindControl("txtVikingCode");
            ucTextbox txtVendorItemCode = (ucTextbox)item.FindControl("txtVendorItemCode");
            ucTextbox txtDescription = (ucTextbox)item.FindControl("txtDescription");
            ucTextbox txtReceivedQty = (ucTextbox)item.FindControl("txtReceivedQty");
            ucTextbox txtUoM = (ucTextbox)item.FindControl("txtUoM");
            ucButton btnRemove = (ucButton)item.FindControl("btnRemove");
            ucTextbox txtOriginalPOQuantity = (ucTextbox)item.FindControl("txtOriginalPOQuantity");
            ucTextbox txtOutstandingQty = (ucTextbox)item.FindControl("txtOutstandingQty");


            btnRemove.Attributes.Remove("style");

            if (hdnSelectOption.Value == "1")
            {
                //txtLineNo.Enabled = false;
                //txtOfficeDepotCode.Enabled = false;
                //txtVikingCode.Enabled = false;
                //txtVendorItemCode.Enabled = false;
                //txtDescription.Enabled = false;
                //if (txtOriginalPOQuantity != null)
                //    txtOriginalPOQuantity.Enabled = false;
                //if (txtOutstandingQty != null)
                //    txtOutstandingQty.Enabled = false;
                //txtUoM.Enabled = false;
                btnRemove.Style["display"] = "none";
            }
            else if (hdnSelectOption.Value == "2")
            {
                //check for the OD_Code if correct then disable the controls else enable them
                DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
                DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();

                oDiscrepancyBE.Action = "GetODProductDetails";
                oDiscrepancyBE.ODSKUCode = txtOfficeDepotCode.Text.Trim();
                oDiscrepancyBE.SiteID = Convert.ToInt32(ucSite.innerControlddlSite.SelectedValue);
                List<DiscrepancyBE> lstODProductDetails = oDiscrepancyBAL.GetODProductDetailsBAL(oDiscrepancyBE);
                if (lstODProductDetails != null
                    && lstODProductDetails.Count > 0
                    && txtOfficeDepotCode.Text.Trim() != "")
                {
                    //txtLineNo.Enabled = false;
                    //txtOfficeDepotCode.Enabled = false;
                    //txtVikingCode.Enabled = false;
                    //txtVendorItemCode.Enabled = false;
                    //txtDescription.Enabled = false;
                    //if (txtOriginalPOQuantity != null)
                    //    txtOriginalPOQuantity.Enabled = false;
                    //if (txtOutstandingQty != null)
                    //    txtOutstandingQty.Enabled = false;
                    //txtUoM.Enabled = false;
                }
                else
                {

                    txtLineNo.Enabled = true;
                    txtOfficeDepotCode.Enabled = true;
                    txtVikingCode.Enabled = true;
                    txtVendorItemCode.Enabled = true;
                    txtDescription.Enabled = true;
                    if (txtOriginalPOQuantity != null)
                        txtOriginalPOQuantity.Enabled = true;
                    if (txtOutstandingQty != null)
                        txtOutstandingQty.Enabled = true;
                    txtUoM.Enabled = true;
                }
                btnRemove.Style["display"] = "";
            }
        }
    }

    private void IncreaseRow()
    {
        int Total = gvNoPurchaseOrder.Rows.Count;
        int iCount = 1;
        Total = Total + 1; //increase 1 because of add
        AddColumns();
        foreach (GridViewRow item in gvNoPurchaseOrder.Rows)
        {


            ucTextbox txtLineNo = (ucTextbox)item.FindControl("txtLineNo");
            ucTextbox txtOfficeDepotCode = (ucTextbox)item.FindControl("txtOfficeDepotCode");
            ucTextbox txtVikingCode = (ucTextbox)item.FindControl("txtVikingCode");
            ucTextbox txtVendorItemCode = (ucTextbox)item.FindControl("txtVendorItemCode");
            ucTextbox txtDescription = (ucTextbox)item.FindControl("txtDescription");
            ucTextbox txtOriginalPOQuantity = (ucTextbox)item.FindControl("txtOriginalPOQuantity");
            ucTextbox txtOutstandingQty = (ucTextbox)item.FindControl("txtOutstandingQty");

            ucTextbox txtReceivedQty = (ucTextbox)item.FindControl("txtReceivedQty");
            ucTextbox txtUoM = (ucTextbox)item.FindControl("txtUoM");
            ucButton btnRemove = (ucButton)item.FindControl("btnRemove");


            tbDummy.Rows.Add(txtLineNo.Text.Trim(),
                                 txtOfficeDepotCode.Text.Trim(),
                                 txtVikingCode.Text.Trim(),
                                 txtVendorItemCode.Text.Trim(),
                                 txtDescription.Text.Trim(),
                                 txtOriginalPOQuantity.Text.Trim(),
                                 txtOutstandingQty.Text.Trim(),
                                 txtReceivedQty.Text.Trim(),
                                 txtUoM.Text.Trim(),
                                 "Remove",
                                 (iCount++).ToString());


        }

        tbDummy.Rows.Add(string.Empty,
                                 string.Empty,
                                 string.Empty,
                                 string.Empty,
                                 string.Empty,
                                 string.Empty,
                                 string.Empty,
                                 string.Empty,
                                 string.Empty,
                                 "Remove",
                                 (iCount++).ToString());


        BindWithGrid();

        EnableDisableGridControls();
    }
    private int? PrintAndSaveReturnNote()
    {
        //decimal TotalCost = 0;
        //decimal NetTotalCost = 0;
        //decimal FrieghtCharges = 0;
        //string htmlBody = string.Empty;
        //string DiscrepancyType = string.Empty;
        //// vendor return address
        //DiscrepancyBE oNewDiscrepancyBE = new DiscrepancyBE();
        DiscrepancyBAL oNewDiscrepancyBAL = new DiscrepancyBAL();
        //oNewDiscrepancyBE.Action = "GetDiscrepancySiteVendorDetail";
        //oNewDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID"));

        //List<DiscrepancyBE> lstVendor = oNewDiscrepancyBAL.GetDiscrepancyLogSiteVendorDetailsBAL(oNewDiscrepancyBE);
        //if (lstVendor.Count > 0)
        //    DiscrepancyType = lstVendor[0].DiscrepancyType;
        //string VendorAddress = string.Empty;
        //if (string.IsNullOrEmpty(lstVendor[0].Vendor.ReturnAddress))
        //{
        //    VendorAddress = lstVendor[0].Vendor.address1 + "<br />" +
        //        lstVendor[0].Vendor.address2 + "<br />" +
        //        lstVendor[0].Vendor.city + "<br />" +
        //        lstVendor[0].Vendor.VMPPIN + " " + lstVendor[0].Vendor.VMPPOU;

        //}
        //else
        //{
        //    VendorAddress = lstVendor[0].Vendor.ReturnAddress.Replace("\r\n", "<br />");
        //}
        //// site address
        //string SiteAddress = lstVendor[0].Site.SiteAddressLine1 + "<br />" +
        //   lstVendor[0].Site.SiteAddressLine2 + "<br />" +
        //   lstVendor[0].Site.SiteAddressLine3 + "<br />" +
        //   lstVendor[0].Site.SiteAddressLine4 + "<br />" +
        //   lstVendor[0].Site.SiteAddressLine5 + "<br />" +
        //   lstVendor[0].Site.SiteAddressLine6 + "<br />" +
        //   lstVendor[0].Site.SitePincode;

        //oNewDiscrepancyBE.Action = "GetDiscrepancyLogITemsForReturnNote";
        //List<DiscrepancyBE> lstItems = oNewDiscrepancyBAL.GetDiscrepancyItemsForReturnNote(oNewDiscrepancyBE);
        //string DiscrepancyItems = string.Empty;
        //string InCorrectDiscrepancyItems = string.Empty;
        //int count = lstItems.Count / 2;
        //int tempCount = 0;
        //if (lstItems != null && lstItems.Count > 0)
        //{
        //    foreach (DiscrepancyBE item in lstItems)
        //    {
        //        if (count == 0 || tempCount < count)
        //        {
        //            if (string.IsNullOrEmpty(item.ODSKUCode) && !string.IsNullOrEmpty(item.ProductDescription))
        //            {
        //                DiscrepancyItems += "<tr><td>" + item.DeliveredQuantity
        //                                          + "</td><td>" + item.UOM
        //                                          + "</td><td>" + item.ODSKUCode
        //                                          + "</td><td>" + item.DirectCode
        //                                          + "</td><td>" + item.ProductDescription;
        //                if (lstItems[0].DiscrepancyTypeID == 12)
        //                {
        //                    DiscrepancyItems += "</td><td>" + item.OriginalQuantity;
        //                    DiscrepancyItems += "</td><td>" + item.OutstandingQuantity;

        //                    DiscrepancyItems += "</td><td style='text-align:left;'>" + item.ItemVal
        //                                 + "</td><td style='text-align:right;'>";
        //                }
        //                else
        //                {
        //                    DiscrepancyItems += "</td><td style='text-align:right;'>" + item.ItemVal
        //                                     + "</td><td style='text-align:right;'>";
        //                }

        //                if (item.DeliveredQuantity.HasValue && !string.IsNullOrEmpty(item.ItemVal))
        //                {
        //                    TotalCost += Convert.ToDecimal(item.ItemVal) * item.DeliveredQuantity.Value;
        //                    DiscrepancyItems += Convert.ToDecimal(item.ItemVal) * item.DeliveredQuantity.Value;
        //                }
        //                else
        //                {
        //                    DiscrepancyItems += string.Empty;
        //                }
        //                DiscrepancyItems += "</td></tr>";
        //            }
        //            else
        //            {
        //                if (!string.IsNullOrEmpty(item.ODSKUCode))
        //                {
        //                    DiscrepancyItems += "<tr><td>" + item.DeliveredQuantity
        //                        + "</td><td>" + item.UOM
        //                        + "</td><td>" + item.ODSKUCode
        //                        + "</td><td>" + item.DirectCode
        //                        + "</td><td>" + item.ProductDescription;

        //                    if (lstItems[0].DiscrepancyTypeID == 12)
        //                    {
        //                        DiscrepancyItems += "</td><td>" + item.OriginalQuantity;
        //                        DiscrepancyItems += "</td><td>" + item.OutstandingQuantity;

        //                        DiscrepancyItems += "</td><td style='text-align:left;'>" + item.ItemVal
        //                    + "</td><td style='text-align:right;'>";
        //                    }
        //                    else
        //                    {
        //                        DiscrepancyItems += "</td><td style='text-align:right;'>" + item.ItemVal
        //                 + "</td><td style='text-align:right;'>";
        //                    }


        //                    if (item.DeliveredQuantity.HasValue && !string.IsNullOrEmpty(item.ItemVal))
        //                    {
        //                        TotalCost += Convert.ToDecimal(item.ItemVal) * item.DeliveredQuantity.Value;
        //                        DiscrepancyItems += Convert.ToDecimal(item.ItemVal) * item.DeliveredQuantity.Value;
        //                    }
        //                    else
        //                    {
        //                        DiscrepancyItems += string.Empty;
        //                    }
        //                    DiscrepancyItems += "</td></tr>";
        //                }
        //            }
        //        }
        //        else
        //        {
        //            InCorrectDiscrepancyItems += "<tr><td>" + item.DeliveredQuantity
        //            + "</td><td>" + item.UOM
        //            + "</td><td>" + item.ODSKUCode
        //            + "</td><td>" + item.DirectCode
        //            + "</td><td>" + item.ProductDescription;
        //            DiscrepancyItems += "</td></tr>";
        //        }
        //        tempCount++;
        //    }
        //}
        ////if (!string.IsNullOrEmpty(txtCarriageCharge.Text))
        ////{
        ////    FrieghtCharges = Convert.ToDecimal(txtCarriageCharge.Text);
        ////}
        //NetTotalCost = TotalCost + FrieghtCharges;
        //string VendorLanguage = lstVendor[0].Vendor.Language;
        ////string templateFile = "~/EmailTemplates/ReturnNote/ReturnNote." + VendorLanguage + ".htm";
        //string templateFile = string.Empty;
        //if (lstItems[0].DiscrepancyTypeID == 12)
        //{
        //    templateFile = "~/EmailTemplates/ReturnNote/ReturnNoteQuality.english.htm";
        //}
        //else
        //{
        //    templateFile = "~/EmailTemplates/ReturnNote/ReturnNote.english.htm";
        //}



        //#region Setting reason as per the language ...
        //switch (VendorLanguage)
        //{
        //    case clsConstants.English:
        //        Page.UICulture = clsConstants.EnglishISO; // "en-US"; // English
        //        break;
        //    case clsConstants.French:
        //        Page.UICulture = clsConstants.FranceISO; // // France
        //        break;
        //    case clsConstants.German:
        //        Page.UICulture = clsConstants.GermanyISO; //"de"; // German
        //        break;
        //    case clsConstants.Dutch:
        //        Page.UICulture = clsConstants.NederlandISO; //"nl"; // Dutch
        //        break;
        //    case clsConstants.Spanish:
        //        Page.UICulture = clsConstants.SpainISO; //"es"; // Spanish
        //        break;
        //    case clsConstants.Italian:
        //        Page.UICulture = clsConstants.ItalyISO; //"it"; // Italy
        //        break;
        //    case clsConstants.Czech:
        //        Page.UICulture = clsConstants.CzechISO; //"cz"; // Czech
        //        break;
        //    default:
        //        Page.UICulture = clsConstants.EnglishISO; //"en-US"; // English
        //        break;
        //}

        //#endregion

        //string contactPerson = string.Empty;

        //sendCommunicationCommon osendCommunicationCommon = new sendCommunicationCommon();
        //if (System.IO.File.Exists(System.Web.HttpContext.Current.Server.MapPath(templateFile.ToLower())))
        //{
        //    using (StreamReader sReader = new StreamReader(System.Web.HttpContext.Current.Server.MapPath(templateFile.ToLower())))
        //    {
        //        htmlBody = sReader.ReadToEnd();
        //        if (DiscrepancyType != "Incorrect Product Code")
        //            htmlBody = htmlBody.Replace("block", "none");

        //        htmlBody = htmlBody.Replace("{VendorDiscrepancyReturnNumber}", WebCommon.getGlobalResourceValue("VendorDiscrepancyReturnNumber"));
        //        htmlBody = htmlBody.Replace("{VDRNoValue}", lstVendor[0].VDRNo);

        //        htmlBody = htmlBody.Replace("{logoInnerPath}", osendCommunicationCommon.getAbsolutePath());

        //        htmlBody = htmlBody.Replace("{From}", WebCommon.getGlobalResourceValue("From"));
        //        htmlBody = htmlBody.Replace("{SiteAddressValue}", SiteAddress);

        //        htmlBody = htmlBody.Replace("{VendorDiscrepancyReturnNote}", WebCommon.getGlobalResourceValue("VendorDiscrepancyReturnNote"));

        //        htmlBody = htmlBody.Replace("{VendorNumberSDR}", WebCommon.getGlobalResourceValue("VendorNumberSDR"));
        //        htmlBody = htmlBody.Replace("{VendorNumberValue}", lstVendor[0].Vendor.Vendor_No);

        //        htmlBody = htmlBody.Replace("{VendorName}", WebCommon.getGlobalResourceValue("VendorName"));
        //        htmlBody = htmlBody.Replace("{VendorNameValue}", string.IsNullOrEmpty(lstVendor[0].Vendor.VendorContactName) ? lstVendor[0].Vendor.VendorName : lstVendor[0].Vendor.VendorContactName);

        //        htmlBody = htmlBody.Replace("{Returnto}", WebCommon.getGlobalResourceValue("Returnto"));
        //        htmlBody = htmlBody.Replace("{VendorAddressValue}", VendorAddress);

        //        htmlBody = htmlBody.Replace("{ReturnNoteText1}", WebCommon.getGlobalResourceValue("ReturnNoteText1"));
        //        htmlBody = htmlBody.Replace("{ReturnNoteText2}", WebCommon.getGlobalResourceValue("ReturnNoteText2"));

        //        htmlBody = htmlBody.Replace("{AuthorisedBy}", WebCommon.getGlobalResourceValue("AuthorisedBy"));
        //        htmlBody = htmlBody.Replace("{VendorAuthorisationReference}", WebCommon.getGlobalResourceValue("VendorAuthorisationReference"));
        //        htmlBody = htmlBody.Replace("{ReasonforReturn}", WebCommon.getGlobalResourceValue("ReasonforReturn"));


        //        htmlBody = htmlBody.Replace("{VendorContactValue}", GetVendorContactName());
        //        if (lstItems != null && lstItems.Count > 0)
        //            htmlBody = htmlBody.Replace("{VendorAuthorisationReferenceValue}", lstItems[0].CollectionAuthNumber);
        //        else
        //            htmlBody = htmlBody.Replace("{VendorAuthorisationReferenceValue}", "-");

        //        htmlBody = htmlBody.Replace("{VENDORDISREPACNYTYPEValue}", lstVendor[0].DiscrepancyType);

        //        htmlBody = htmlBody.Replace("{QTY}", WebCommon.getGlobalResourceValue("QTY"));
        //        htmlBody = htmlBody.Replace("{UOM}", WebCommon.getGlobalResourceValue("UOM"));
        //        htmlBody = htmlBody.Replace("{OURPRODUCTCODE}", WebCommon.getGlobalResourceValue("OURPRODUCTCODE"));
        //        htmlBody = htmlBody.Replace("{VENDORREFERENCE}", WebCommon.getGlobalResourceValue("VENDORREFERENCE"));
        //        htmlBody = htmlBody.Replace("{DESCRIPTION}", WebCommon.getGlobalResourceValue("DESCRIPTION"));
        //        htmlBody = htmlBody.Replace("{COST}", WebCommon.getGlobalResourceValue("COST"));
        //        htmlBody = htmlBody.Replace("{NET}", WebCommon.getGlobalResourceValue("NET"));

        //        // quatity return notes changes//
        //        if (lstItems[0].DiscrepancyTypeID == 12)
        //        {
        //            htmlBody = htmlBody.Replace("{POQuantity}", WebCommon.getGlobalResourceValue("OriginalPOQuantity"));
        //            htmlBody = htmlBody.Replace("{OUTSTANDING}", WebCommon.getGlobalResourceValue("OutstandingPOQuantity"));
        //        }

        //        htmlBody = htmlBody.Replace("{DiscrepancyItemsValue}", DiscrepancyItems);

        //        htmlBody = htmlBody.Replace("{ReturnTotal}", WebCommon.getGlobalResourceValue("ReturnTotal"));
        //        htmlBody = htmlBody.Replace("{TotalCostValue}", TotalCost.ToString());

        //        htmlBody = htmlBody.Replace("{FreightCharge}", WebCommon.getGlobalResourceValue("FreightCharge"));
        //        htmlBody = htmlBody.Replace("{FreightChargeValue}", FrieghtCharges.ToString());
        //        // *******  New changes
        //        htmlBody = htmlBody.Replace("{ReturnNoteIncorrectProductDec}", WebCommon.getGlobalResourceValue("ReturnNoteIncorrectProductDec"));
        //        htmlBody = htmlBody.Replace("{DiscrepancyItemsValue1}", InCorrectDiscrepancyItems);
        //        //******************************
        //        htmlBody = htmlBody.Replace("{Total}", WebCommon.getGlobalResourceValue("Total"));
        //        htmlBody = htmlBody.Replace("{NetTotalCostValue}", NetTotalCost.ToString());

        //        htmlBody = htmlBody.Replace("{PreparedBy}", WebCommon.getGlobalResourceValue("PreparedBy"));
        //        htmlBody = htmlBody.Replace("{OFFICE_DEPOT_DC_CONTACTValue}", Session["UserName"].ToString());

        //        htmlBody = htmlBody.Replace("{DatePrepared}", WebCommon.getGlobalResourceValue("DatePrepared"));
        //        htmlBody = htmlBody.Replace("{DATEPREPAREDValue}", DateTime.Now.ToString("dd/MM/yyyy"));

        //        htmlBody = htmlBody.Replace("{NumberofPallets}", WebCommon.getGlobalResourceValue("NumberofPallets"));

        //        htmlBody = htmlBody.Replace("{NumberofPalletsValue}", hdntxtNumPalletsExchange.Value.Trim());

        //        htmlBody = htmlBody.Replace("{NumberofCartons}", WebCommon.getGlobalResourceValue("NumberofCartons"));
        //        htmlBody = htmlBody.Replace("{NumberofCartonsValue}", hdntxtNumCartonsExchange.Value.Trim());

        //        htmlBody = htmlBody.Replace("{CollectionReturnDate}", WebCommon.getGlobalResourceValue("CollectionReturnDate"));
        //        if (!string.IsNullOrEmpty(hdntxtCollection.Value.ToString()))
        //        {
        //            htmlBody = htmlBody.Replace("{ColletionDateValue}", hdntxtCollection.Value.ToString());
        //        }
        //        else
        //        {
        //            htmlBody = htmlBody.Replace("{ColletionDateValue}", DateTime.Now.ToString("dd/MM/yyyy"));
        //        }

        //        htmlBody = htmlBody.Replace("{Signed}", WebCommon.getGlobalResourceValue("Signed"));

        //        htmlBody = htmlBody.Replace("{Carrier}", WebCommon.getGlobalResourceValue("Carrier"));
        //        if (hdnucTxtCarrierWhoCollectGoods.Value != null && hdnucTxtCarrierWhoCollectGoods.Value != "----Select----")
        //        {
        //            htmlBody = htmlBody.Replace("{CarrierValue}", hdnucTxtCarrierWhoCollectGoods.Value);
        //        }
        //        else if (hdnddlCarrierSendingGoodsBack.Value != null)
        //            htmlBody = htmlBody.Replace("{CarrierValue}", hdnddlCarrierSendingGoodsBack.Value);
        //        else
        //            htmlBody = htmlBody.Replace("{CarrierValue}", "");

        //        htmlBody = htmlBody.Replace("{VEHICLEREG}", WebCommon.getGlobalResourceValue("VEHICLEREG"));
        //        htmlBody = htmlBody.Replace("{DRIVER}", WebCommon.getGlobalResourceValue("DRIVER"));
        //        htmlBody = htmlBody.Replace("{SIGN}", WebCommon.getGlobalResourceValue("SIGN"));
        //        htmlBody = htmlBody.Replace("{Print}", WebCommon.getGlobalResourceValue("Print"));


        //        // changes for alternate Return note 
        //        htmlBody = htmlBody.Replace("{PalletExchangeInfo}", WebCommon.getGlobalResourceValue("PalletExchangeInfo"));
        //        htmlBody = htmlBody.Replace("{OnewayPallets}", WebCommon.getGlobalResourceValue("OneWayPallets"));
        //        htmlBody = htmlBody.Replace("{EuroPallets}", WebCommon.getGlobalResourceValue("NumberEuroPallets"));
        //        htmlBody = htmlBody.Replace("{EuroPalletsChanged}", WebCommon.getGlobalResourceValue("EuroPalletsChanged"));
        //        // changes end

        //        htmlBody = htmlBody.Replace("{ImportantNotice}", WebCommon.getGlobalResourceValue("ImportantNotice"));
        //        htmlBody = htmlBody.Replace("{ReturnNoteText3}", WebCommon.getGlobalResourceValue("ReturnNoteText3"));
        //        htmlBody = htmlBody.Replace("{ReturnNoteText4}", WebCommon.getGlobalResourceValue("ReturnNoteText4"));

        //        contactPerson = GetAPContact(lstVendor[0].VendorID);
        //        htmlBody = htmlBody.Replace("{ACCOUNTS PAYABLE CONTACT}", string.IsNullOrEmpty(contactPerson) ? "<Not Available>" : contactPerson);
        //        if (lstVendor[0].Site.SiteMangerUserID != null)
        //            contactPerson = GetDCSiteContact(lstVendor[0].Site.SiteMangerUserID);

        //        htmlBody = htmlBody.Replace("{OFFICE DEPOT DC SITE CONTACT}", string.IsNullOrEmpty(contactPerson) ? "<Not Available>" : contactPerson);
        //    }
        //}
        //Page.UICulture = Convert.ToString(Session["CultureInfo"]);

        //// Save into the database
        DiscrepancyReturnNoteBE oDiscrepancyReturnNoteBE = new DiscrepancyReturnNoteBE();
        //oDiscrepancyReturnNoteBE.Action = "AddDiscrepancyReturnNote";
        oDiscrepancyReturnNoteBE.Action = "GetDiscrepancyReturnNoteByLogID";
        oDiscrepancyReturnNoteBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID"));
        //oDiscrepancyReturnNoteBE.LoggedDate = DateTime.Now;
        //oDiscrepancyReturnNoteBE.ReturnNoteBody = htmlBody.ToString();

        //int? retval = oNewDiscrepancyBAL.AddDiscrepancyReturnNote(oDiscrepancyReturnNoteBE);
        oNewDiscrepancyBAL.GetDiscrepancyReturnNoteByLogID(oDiscrepancyReturnNoteBE);


        //// Print Return note
        Session["LabelHTML"] = oDiscrepancyReturnNoteBE.ReturnNoteBody;

        string a = "window.open('../LogDiscrepancy/DIS_PrintLetter.aspx', '', 'left =' + screen.width + ',top=' + screen.height + ',width=0,height=0,toolbar=0,scrollbars=1,status=0')";
        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "pp", a, true);

        return 0;
    }
    private void AddUserControl()
    {
        DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
        DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();
        oDiscrepancyBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
        string CaseOf = String.Empty;
        if (Session["Role"] != null && Session["Role"].ToString().Trim() == "Vendor")
        {
            btnUpdateLocation.Visible = false;
        }
        oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID"));

        if (GetQueryStringValue("RoleTypeFlag") == "A" || GetQueryStringValue("RoleTypeFlag") == null)
        {
            if (Session["UserID"] == null || Session["UserID"].ToString() == "" || Session["Role"] == null || Session["Role"].ToString().Trim() == "")
                return;

            oDiscrepancyBE.User.UserID = Convert.ToInt32(Session["UserID"]);
            CaseOf = Session["Role"].ToString().Trim();
        }
        else
        {
            switch (GetQueryStringValue("RoleTypeFlag").ToString())
            {
                case "G":
                    CaseOf = "OD - Goods In";
                    break;
                case "S":
                    CaseOf = "OD - Stock Planner";
                    break;
                case "P":
                    CaseOf = "OD - Accounts Payable";
                    break;
                case "V":
                    CaseOf = "Vendor";
                    break;
            }
        }

        if (CaseOf == "OD - Stock Planner")
        {
            oDiscrepancyBE.Action = "CheckForInventory";

            List<DiscrepancyBE> lstDetails = oDiscrepancyBAL.GetUserDetailsForWorkFlowBAL(oDiscrepancyBE);
            if (lstDetails != null && lstDetails.Count > 0)
            {
                //check for inventory user
                oDiscrepancyBE.Action = "CheckForActionRequired";
                oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID"));
                oDiscrepancyBE.INVActionRequired = true;
                DataTable dtActionDetails = oDiscrepancyBAL.GetActionRequiredDetailsForWorkFlowBAL(oDiscrepancyBE);
                if (dtActionDetails != null && dtActionDetails.Rows.Count > 0)
                {
                    string sUserControl = "../UserControl/INVAction/" + dtActionDetails.Rows[0]["INVUserControl"].ToString().Trim()
                                            + ".aspx?disLogID=" + GetQueryStringValue("disLogID").ToString()
                                            + "&DisWFlowID=" + dtActionDetails.Rows[0]["DiscrepancyWorkflowID"].ToString()
                                            + "&PONo=" + dtActionDetails.Rows[0]["PurchaseOrderNumber"].ToString()
                                            + "&SiteID=" + SiteID.ToString()
                                            + "&VDRNo=" + VDRNo
                                            + "&FromPage=NoPO"
                                        + "&PreviousPage=" + GetQueryStringValue("PreviousPage").ToString();
                    ifUserControl.Attributes.Add("src", EncryptURL(sUserControl));
                }
                else
                {
                    ifUserControl.Style["display"] = "none";
                }
            }
        }
        else if (CaseOf == "OD - Goods In")
        {
            //check for goods in
            oDiscrepancyBE.Action = "GetDiscrepancyReturnNoteCount";
            DataTable dtReturnNoteCount = oDiscrepancyBAL.GetDiscrepancyReturnNoteCountBAL(oDiscrepancyBE);
            if (dtReturnNoteCount != null && dtReturnNoteCount.Rows.Count > 0)
            {
                if (Convert.ToInt32(dtReturnNoteCount.Rows[0].ItemArray[0].ToString()) > 0)
                    btnPrintReturnsNote.Visible = true;
            }

            oDiscrepancyBE.Action = "CheckForActionRequired";
            oDiscrepancyBE.GINActionRequired = true;

            DataTable dtActionDetails = oDiscrepancyBAL.GetActionRequiredDetailsForWorkFlowBAL(oDiscrepancyBE);
            if (dtActionDetails != null && dtActionDetails.Rows.Count > 0)
            {

                string sUserControl = "../UserControl/GINAction/" + dtActionDetails.Rows[0]["GINUserControl"].ToString().Trim()
                                        + ".aspx?disLogID=" + GetQueryStringValue("disLogID").ToString()
                                        + "&DisWFlowID=" + dtActionDetails.Rows[0]["DiscrepancyWorkflowID"].ToString()
                                        + "&PONo=" + dtActionDetails.Rows[0]["PurchaseOrderNumber"].ToString()
                                        + "&SiteID=" + SiteID.ToString()
                                        + "&VDRNo=" + VDRNo + "&FromPage=NoPO"
                                        + "&PreviousPage=" + GetQueryStringValue("PreviousPage").ToString();
                ifUserControl.Attributes.Add("src", EncryptURL(sUserControl));


            }
            else
            {
                ifUserControl.Style["display"] = "none";
            }
        }
        else if (CaseOf == "OD - Accounts Payable")
        {
            //check for account payable in

            oDiscrepancyBE.Action = "CheckForActionRequired";
            oDiscrepancyBE.DiscrepancyWorkFlowID = !string.IsNullOrEmpty(GetQueryStringValue("WorkFlowID")) ? Convert.ToInt32(GetQueryStringValue("WorkFlowID")) : (int?)null;
            oDiscrepancyBE.APActionRequired = true;

            DataTable dtActionDetails = oDiscrepancyBAL.GetActionRequiredDetailsForWorkFlowBAL(oDiscrepancyBE);
            if (dtActionDetails != null && dtActionDetails.Rows.Count > 0)
            {
                string sUserControl = "../UserControl/APAction/" + dtActionDetails.Rows[0]["APUserControl"].ToString().Trim()
                                        + ".aspx?disLogID=" + GetQueryStringValue("disLogID").ToString()
                                        + "&DisWFlowID=" + dtActionDetails.Rows[0]["DiscrepancyWorkflowID"].ToString()
                                        + "&PONo=" + dtActionDetails.Rows[0]["PurchaseOrderNumber"].ToString()
                                        + "&SiteID=" + SiteID.ToString()
                                        + "&VDRNo=" + VDRNo
                                        + "&FromPage=NoPO"
                                        + "&PreviousPage=" + GetQueryStringValue("PreviousPage").ToString();
                ifUserControl.Attributes.Add("src", EncryptURL(sUserControl));
                btnAccountsPayableAction.Visible = true;
            }
            else
            {
                ifUserControl.Style["display"] = "none";
            }
        }
        else if (CaseOf == "Vendor")
        {
            //check for vendor in
            pnlLocation.Visible = false;
            oDiscrepancyBE.Action = "CheckForActionRequiredForVendor";
            oDiscrepancyBE.VenActionRequired = true;

            DataTable dtActionDetails = oDiscrepancyBAL.GetActionRequiredDetailsForWorkFlowBAL(oDiscrepancyBE);
            if (dtActionDetails != null && dtActionDetails.Rows.Count > 0)
            {

                string sUserControl = "../UserControl/VENAction/" + dtActionDetails.Rows[0]["VENUserControl"].ToString().Trim()
                                        + ".aspx?disLogID=" + GetQueryStringValue("disLogID").ToString()
                                        + "&DisWFlowID=" + dtActionDetails.Rows[0]["DiscrepancyWorkflowID"].ToString()
                                        + "&PONo=" + dtActionDetails.Rows[0]["PurchaseOrderNumber"].ToString()
                                        + "&SiteID=" + SiteID.ToString()
                                        + "&VDRNo=" + VDRNo + "&FromPage=NoPO"
                                        + "&PreviousPage=" + GetQueryStringValue("PreviousPage").ToString();
                ifUserControl.Attributes.Add("src", EncryptURL(sUserControl));
            }
            else
            {
                ifUserControl.Style["display"] = "none";
            }
        }
        else
        {
            ifUserControl.Style["display"] = "none";
            btnAccountsPayableAction.Visible = true;
        }
        oDiscrepancyBAL = null;
    }

    private void InsertHTML()
    {

        /* Check for the site "No Escalation" */
        MAS_SiteBE oMAS_SiteBE = new MAS_SiteBE();
        MAS_SiteBAL oMAS_SiteBAL = new MAS_SiteBAL();
        oMAS_SiteBE.Action = "GetSiteDisSettings";
        oMAS_SiteBE.SiteID = Convert.ToInt32(ucSite.innerControlddlSite.SelectedItem.Value);

        oMAS_SiteBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();

        List<MAS_SiteBE> lstSiteDetails = oMAS_SiteBAL.GetSiteMisSettingBAL(oMAS_SiteBE);
        oMAS_SiteBAL = null;
        if (lstSiteDetails != null && lstSiteDetails.Count > 0 && lstSiteDetails[0].DIS_CommunicationWithEscalationType.Trim() != "N")
        {
            //site "No Escalation" is not on 
            DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
            DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();

            WorkflowHTML oWorkflowHTML = new WorkflowHTML();
            oDiscrepancyBE.GINHTML = oWorkflowHTML.function1(DateTime.Now, null, null, "Yellow", "",
                                                                   "No PO Discrepancy Created",
                //Session["UserName"].ToString(),
                                                                   UIUtility.GetODVendorUserName(Convert.ToString(Session["Role"]), Convert.ToString(Session["UserName"]), Convert.ToString(Session["LoginID"])),
                                                                   txtInternalComments.Text, "", "", "", "", "", "", "", "", "", "", "", "", "", "", "NoPO", "Yes");
            oDiscrepancyBE.GINActionRequired = false;

            string StockPlannerEmailID = ViewState["StockPlannerEmailID"] == null ? "" : Convert.ToString(ViewState["StockPlannerEmailID"]);

            string StockPlannerToCoverEmailID = ViewState["StockPlannerToCoverEmailID"] == null ? "" : Convert.ToString(ViewState["StockPlannerToCoverEmailID"]);

            if (StockPlannerToCoverEmailID != "")
            {
                oDiscrepancyBE.INVHTML = oWorkflowHTML.function4("Blue", "", null, "", "",
                FollowingRequestMessage.Replace("{stockPlannerID}", StockPlannerToCoverEmailID).Replace("{Date}", DateTime.Now.ToString("dd/MM/yyyy")),//"Build on to" + StockPlannerEmailID + " Work list on " + DateTime.Now.ToString("dd/MM/yyyy") + " with following request",
                NoPODesc//"No PO has been received from the vendor. Please confirm the next action for this delivery?"
                );
            }
            else
            {
                oDiscrepancyBE.INVHTML = oWorkflowHTML.function4("Blue", "", null, "", "",
                FollowingRequestMessage.Replace("{stockPlannerID}", StockPlannerEmailID).Replace("{Date}", DateTime.Now.ToString("dd/MM/yyyy")),//"Build on to" + StockPlannerEmailID + " Work list on " + DateTime.Now.ToString("dd/MM/yyyy") + " with following request",
                NoPODesc//"No PO has been received from the vendor. Please confirm the next action for this delivery?"
                );
            }
                oDiscrepancyBE.INVActionRequired = true;
            oDiscrepancyBE.INVUserControl = "INV001"; // Calling INV001 instaed of INV005

            oDiscrepancyBE.APHTML = oWorkflowHTML.function3("White", null, "", "");
            oDiscrepancyBE.APActionRequired = false;

            if (ucSDRCommunication1.innerControlRdoCommunication == 'L')   // for letter           
                oDiscrepancyBE.VENHTML = oWorkflowHTML.function4("Blue", "", DateTime.Now, "", "Admin@OfficeDepot.com", "", InitialCommunicationSent, "", Convert.ToString(sLetterLink));
            else
                oDiscrepancyBE.VENHTML = oWorkflowHTML.function4("Blue", "", DateTime.Now, Convert.ToString(sSendMailLink), "Admin@OfficeDepot.com", "", InitialCommunicationSent, "", "");
            oDiscrepancyBE.VenActionRequired = false;

            oDiscrepancyBE.Action = "InsertHTML";
            oDiscrepancyBE.DiscrepancyLogID = DiscrepancyLogID;
            oDiscrepancyBE.LoggedDateTime = DateTime.Now;
            oDiscrepancyBE.LevelNumber = 1;
            oDiscrepancyBE.ActionTakenBy = "GIN";
            oDiscrepancyBE.InventoryActionCategory = "AR";  //Action Required
            int? iResult = oDiscrepancyBAL.addWorkFlowHTMLsBAL(oDiscrepancyBE);
        }
        else
        {
            //step 1b- close the discrepancy
            DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
            DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();

            WorkflowHTML oWorkflowHTML = new WorkflowHTML();
            oDiscrepancyBE.GINHTML = oWorkflowHTML.function1(DateTime.Now, null, null, "Yellow", "",
                                                                    "PO Discrepancy Created",
                //Session["UserName"].ToString(),
                                                                    UIUtility.GetODVendorUserName(Convert.ToString(Session["Role"]), Convert.ToString(Session["UserName"]), Convert.ToString(Session["LoginID"])),
                                                                    txtInternalComments.Text, "", "", "", "", "", "", "", "", "", "", "", "", "", "", "NoPO", "No");
            oDiscrepancyBE.GINActionRequired = false;

            oDiscrepancyBE.INVHTML = oWorkflowHTML.function3("White", null);
            oDiscrepancyBE.INVActionRequired = false;

            oDiscrepancyBE.APHTML = oWorkflowHTML.function3("White", null);
            oDiscrepancyBE.APActionRequired = false;

            if (ucSDRCommunication1.innerControlRdoCommunication == 'L')   // for letter  
                oDiscrepancyBE.VENHTML = oWorkflowHTML.function4("Blue", "", DateTime.Now, "", "Admin@OfficeDepot.com", InitialCommunicationSent, "", "", Convert.ToString(sLetterLink));
            else
                oDiscrepancyBE.VENHTML = oWorkflowHTML.function4("Blue", "", DateTime.Now, Convert.ToString(sSendMailLink), "Admin@OfficeDepot.com", InitialCommunicationSent);
            oDiscrepancyBE.VenActionRequired = false;

            oDiscrepancyBE.Action = "InsertHTML";
            oDiscrepancyBE.DiscrepancyLogID = DiscrepancyLogID;
            oDiscrepancyBE.LoggedDateTime = DateTime.Now;
            oDiscrepancyBE.LevelNumber = 1;
            oDiscrepancyBE.CloseDiscrepancy = true;
            int? iResult = oDiscrepancyBAL.addWorkFlowHTMLsBAL(oDiscrepancyBE);
            oDiscrepancyBAL = null;
        }
    }

    private int? sendAndSaveDiscrepancyCommunication(int iDiscrepancy)
    {
        //int? vendorID = ViewState["VendorID"] == null ? 0 : Convert.ToInt32(ViewState["VendorID"].ToString());
        int? vendorID = null;
        if (rdoPO1.Checked)
            vendorID = iVendorID;
        else
            vendorID = Convert.ToInt32(ucSeacrhVendor1.VendorNo);

        if (((RadioButton)ucSDRCommunication1.FindControl("rdoEmailComm")) != null && ((RadioButton)ucSDRCommunication1.FindControl("rdoEmailComm")).Checked)
        {
            if (((BaseControlLibrary.ucTextbox)ucSDRCommunication1.FindControl("txtAltEmail")) != null && ((BaseControlLibrary.ucTextbox)ucSDRCommunication1.FindControl("ucVendorEmailList")) != null)
            {
                sendCommunication communication = new sendCommunication();
                int? retVal = communication.sendCommunicationByEMail(iDiscrepancy, "nopurchaseordernumber", ((BaseControlLibrary.ucTextbox)ucSDRCommunication1.FindControl("txtAltEmail")), ((BaseControlLibrary.ucTextbox)ucSDRCommunication1.FindControl("ucVendorEmailList")), vendorID);
                sSendMailLink = communication.sSendMailLink;
                return retVal;
            }
        }
        return null;
    }

    /*----------------ADDING RAISE QUERY METHODS------------*/

    private bool IsAlreadyDisputeExist(int discrepancyLogID = 0)
    {
        var isExist = false;

        if (discrepancyLogID != 0)
        {
            var queryDiscrepancyBAL = new DISLog_QueryDiscrepancyBAL();
            var queryDiscrepancyBE = new DISLog_QueryDiscrepancyBE();
            if (discrepancyLogID > 0)
            {
                queryDiscrepancyBE.Action = "IsAlreadyDisputeExist";
                queryDiscrepancyBE.DiscrepancyLogID = discrepancyLogID;
                var result = queryDiscrepancyBAL.IsAlreadyDisputeExistBAL(queryDiscrepancyBE);
                if (result > 0)
                    isExist = true;
            }
        }

        return isExist;
    }
    /* CHECK QUERY IS CLOSED THEN VENDOR CAN RAISE QUERY*/
    private bool IsQueryClosed(int discrepancyLogID = 0)
    {
        var isExist = false;

        if (discrepancyLogID != 0)
        {
            var queryDiscrepancyBAL = new DISLog_QueryDiscrepancyBAL();
            var queryDiscrepancyBE = new DISLog_QueryDiscrepancyBE();
            if (discrepancyLogID > 0)
            {
                queryDiscrepancyBE.Action = "IsAlreadyDisputeExist";
                queryDiscrepancyBE.DiscrepancyLogID = discrepancyLogID;
                var result = queryDiscrepancyBAL.GetQueryClose(queryDiscrepancyBE);
                if (result != 0)
                    isExist = true;
            }
        }

        return isExist;
    }
    /* METHODS FOR VENDOR'S QUERY OVERVIEW*/
    private void VendorsQueryOverview()
    {

        var queryDiscrepancyBAL = new DISLog_QueryDiscrepancyBAL();
        var queryDiscrepancyBE = new DISLog_QueryDiscrepancyBE();
        var disLogID = GetQueryStringValue("disLogID") != null ? Convert.ToInt32(GetQueryStringValue("disLogID")) : 0;
        //var queryDisId = GetQueryStringValue("QueryDisID") != null ? Convert.ToInt32(GetQueryStringValue("QueryDisID")) : 0;
        if (disLogID > 0)
        {
            queryDiscrepancyBE.Action = "GetQueryDiscrepancy";
            // queryDiscrepancyBE.QueryDiscrepancyID = queryDisId;
            queryDiscrepancyBE.DiscrepancyLogID = disLogID;
            var getQueryDiscrepancy = queryDiscrepancyBAL.GetQueryDiscrepancyBAL(queryDiscrepancyBE);
            if (getQueryDiscrepancy.Count > 0)
            {
                pnlQueryOverview_1.Visible = true;
                rptQueryOverview.DataSource = getQueryDiscrepancy;
                rptQueryOverview.DataBind();
                
            }
        }
    }
    protected void rptQueryOverview_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            var hdnIsQueryClosedManually = (HiddenField)e.Item.FindControl("hdnIsQueryClosedManually");

            // GoodsIn declaration ...            
            var lblCommentLabelT_2 = (ucLabel)e.Item.FindControl("lblCommentLabelT_2");
            if (lblCommentLabelT_2 != null)
            {
                if (!string.IsNullOrEmpty(lblCommentLabelT_2.Text) ) { e.Item.FindControl("tblGoodsIn").Visible = true; }
                else { e.Item.FindControl("tblGoodsIn").Visible = false;  }

                if (!string.IsNullOrEmpty(lblCommentLabelT_2.Text) && hdnIsQueryClosedManually.Value == "True")
                {
                    e.Item.FindControl("tblGoodsIn").Visible = false;
                    e.Item.FindControl("tblQueryClosedManually").Visible = true;
                }

                var lnkView = (System.Web.UI.HtmlControls.HtmlAnchor)e.Item.FindControl("lnkView");
                var hdnQueryDiscrepancyID = (HiddenField)e.Item.FindControl("hdnQueryDiscrepancyID");
                var hdnDiscrepancyLogID = (HiddenField)e.Item.FindControl("hdnDiscrepancyLogID");
                lnkView.Attributes.Add("href", EncryptQuery("~/ModuleUI/Appointment/Receiving/APPRcv_Communication.aspx?DiscId=" + hdnDiscrepancyLogID.Value
                    + "&QueryDiscId=" + hdnQueryDiscrepancyID.Value));
            }
        }
    }

    #endregion

    protected void gvNoPurchaseOrder_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (rdoVendor11.Checked)
        {
            e.Row.Cells[5].Visible = false;
            e.Row.Cells[6].Visible = false;

        }
    }

    protected void gvNoPurchaseOrderViewMode_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (Session["PurchaseOrderID"] == null)
        {
            e.Row.Cells[5].Visible = false;
            e.Row.Cells[6].Visible = false;

        }
    }
    protected void btnUpdateLocation_Click(object sender, EventArgs e)
    {
        var disLogID = GetQueryStringValue("disLogID") != null ? Convert.ToInt32(GetQueryStringValue("disLogID")) : 0;
        if (disLogID != null)
        {
            DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
            DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();
            oDiscrepancyBE.Action = "UpdateLocation";
            oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(disLogID);
            oDiscrepancyBE.Location = txtLocation.Text.Trim();
            oDiscrepancyBE.UserID = Convert.ToInt32(Session["UserID"]);
            string sResult = oDiscrepancyBAL.updateDiscrepancyLocationBAL(oDiscrepancyBE);
            if (GetQueryStringValue("PreviousPage") != null)
            {
                if (GetQueryStringValue("PreviousPage").Trim().ToUpper().Equals("SearchResultVendor".ToUpper()))
                {
                    btnBack_1.Visible = false;
                    btnUpdateLocation.Visible = false;
                    EncryptQueryString("~/ModuleUI/Discrepancy/DIS_SearchResultVendor.aspx?disLogID=" + Convert.ToString(GetQueryStringValue("disLogID"))
                                + "&VDRNo=" + Convert.ToString(GetQueryStringValue("VDRNo"))
                                + "&UserID=" + Convert.ToString(GetQueryStringValue("UserID"))
                                + "&PreviousPage=SearchResultVendor&WorkFlowID=" + Convert.ToString(GetQueryStringValue("WorkFlowID").ToString()));
                }

                if (Session["PageMode"].ToString() == "Todays")
                {
                    EncryptQueryString("../DIS_SearchResult.aspx?FromPage=images&SiteId=" + GetQueryStringValue("SiteId") + "&DisDate=" + GetQueryStringValue("DisDate"));
                }
                else if (Session["PageMode"].ToString() == "Search")
                {
                    //For opening search discrepancies mode
                    btnBack_1.Visible = false;
                    btnUpdateLocation.Visible = false;
                    EncryptQueryString("~/ModuleUI/Discrepancy/DIS_SearchResult.aspx?disLogID=" + Convert.ToString(GetQueryStringValue("disLogID"))
                                + "&VDRNo=" + Convert.ToString(GetQueryStringValue("VDRNo"))
                                + "&UserID=" + Convert.ToString(GetQueryStringValue("UserID"))
                                + "&PreviousPage=SearchResult&WorkFlowID=" + Convert.ToString(GetQueryStringValue("WorkFlowID").ToString()));

                }
            }
            else if (GetQueryStringValue("PN") != null)
            {
                if (GetQueryStringValue("PN").Trim().ToUpper().Equals("POINQ"))
                {
                    btnBack_1.Visible = false;
                    btnUpdateLocation.Visible = false;
                    EncryptQueryString("~/ModuleUI/Appointment/PurchaseOrderInquiry.aspx?SId=" + GetQueryStringValue("SId").ToString()
                        + "&Status=" + GetQueryStringValue("Status").ToString()
                        + "&PN=DISLOG"
                        + "&PO=" + GetQueryStringValue("PO").ToString());
                }
                else if (GetQueryStringValue("PN").Trim().ToUpper().Equals("OPENPL"))
                {
                    EncryptQueryString("~/ModuleUI/StockOverview/Expedite/OpenPurchaseListing.aspx?action=showlisting");
                }


            }
        }
    }

    protected void btnContinue_Click(object sender, EventArgs e)
    {
        string EncryptcUrl = EncryptQuery("~/ModuleUI/Discrepancy/DISLog_QueryDiscrepancy.aspx?disLogID=" + Convert.ToString(GetQueryStringValue("disLogID"))
                                   + "&VDRNo=" + Convert.ToString(GetQueryStringValue("VDRNo")) + "&RaiseQueryVendor=Yes" + "&RedirectTo=DisLog_Overs.aspx");
        Response.Redirect(EncryptcUrl, false);
        Context.ApplicationInstance.CompleteRequest();
    }

    protected void btnRaiseQueryPopupBack_Click(object sender, EventArgs e)
    {
        btnBack.Visible = false;
        btnSave.Visible = false;
    }
    private void BindDiscrepancyLogComment(DiscrepancyBE discrepancyBE = null)
    {
        var discrepancyBAL = new DiscrepancyBAL();
        if (discrepancyBE != null)
        {
            rptDiscrepancyComments.DataSource = discrepancyBAL.GetDiscrepancyLogCommentsBAL(discrepancyBE);
        }
        else
        {
            discrepancyBE = new DiscrepancyBE();
            discrepancyBE.Action = "GetDiscrepancyLogComment";

            if (!string.IsNullOrEmpty(GetQueryStringValue("disLogID")))
                discrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID"));
            else
                discrepancyBE.DiscrepancyLogID = 0;

            rptDiscrepancyComments.DataSource = discrepancyBAL.GetDiscrepancyLogCommentsBAL(discrepancyBE);
        }

        rptDiscrepancyComments.DataBind();

        if (rptDiscrepancyComments.Items.Count > 0)
            lblComments.Visible = true;
        else
            lblComments.Visible = false;
        
    }
}


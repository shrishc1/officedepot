﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="DIS_PrintLabel.aspx.cs" Inherits="ModuleUI_Discrepancy_LogDiscrepancy_DIS_PrintLabel" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .style1
        {
            font-size: 36px;
            font-weight: bold;
            font-family: Arial, Helvetica, sans-serif;
        }
        .style10
        {
            font-size: 16px;
            font-weight: bold;
            font-family: Arial, Helvetica, sans-serif;
        }
    </style>
    <script language="javascript" type="text/javascript">

        function printScreen() {                      
            window.document.getElementById('<%=lblprint.ClientID %>').innerHTML = window.opener.document.getElementById('<%=ltlable%>').innerHTML;
            window.print();
            var reditect = '<%=redirect%>';
            if (ContainRedirection() && reditect=='true') {
                var AppURi = window.opener.document.getElementById('<%=AppPath%>').value;
                if (AppURi != "") {
                    window.opener.document.location.href = AppURi;
                }
                else {
                    var serachPageQString = '<%=strQueryString%>';
                    if (serachPageQString != null) {
                        if (serachPageQString != '') {
                            window.opener.document.location.href = "../DIS_SearchResult.aspx?" + serachPageQString;
                        }
                        else {
                            window.opener.document.location.href = "../DIS_SearchResult.aspx?W+Jt+US/7K/Hd2oqU6zrWw==";
                        }
                    }
                    else {
                        window.opener.document.location.href = "../DIS_SearchResult.aspx?W+Jt+US/7K/Hd2oqU6zrWw==";
                    }
                }
            }
            setTimeout('self.close()', 1000);
        }
        function ContainRedirection() {
            var qrStr = window.location.search;
            var spQrStr = qrStr.substring(1);
            var arrQrStr = new Array();
            var arr = spQrStr.split('&');

            for (var i = 0; i < arr.length; i++) {
                var queryvalue = arr[i].split('=');
                if (queryvalue[0] == 'RedirectionRequired') {
                    return false;
                }
            }
            return true;
       }        
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:Label ID="lblprint" runat="server"></asp:Label>
    </div>
    </form>
</body>
</html>

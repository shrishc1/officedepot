﻿using System;
using System.Web.UI;

public partial class ModuleUI_Discrepancy_LogDiscrepancy_DIS_PrintLetter : CommonPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string LabelHTML = Session["LabelHTML"].ToString();
        LabelHTML = LabelHTML.Replace("'", "");
        lblprint.Text = LabelHTML;
        Session.Remove("LabelHTML");
    }
}
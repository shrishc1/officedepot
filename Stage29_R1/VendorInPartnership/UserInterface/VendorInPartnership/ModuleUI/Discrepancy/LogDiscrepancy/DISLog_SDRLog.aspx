﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="DISLog_SDRLog.aspx.cs" Inherits="ModuleUI_Discrepancy_Log_Discrepancy_DISLog_SDRLog" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>SDR Log - SDR and Labels</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table>
            <tr class="fieldset-form">
                <td>
                    <cc1:ucLabel ID="lblSDRNumberCreated" runat="server" Text="SDR Number Created"></cc1:ucLabel>
                </td>
                <td style="font-weight: bold; width: 5%">
                    :
                </td>
                <td style="font-weight: bold">
                    <cc1:ucLabel ID="lblSDRNumberCreatedValue" runat="server" Text="G020000049"></cc1:ucLabel>
                </td>
            </tr>
            <tr class="fieldset-form">
                <td>
                    <cc1:ucLabel ID="lblNumberofLabels" runat="server" Text="Number of Labels"></cc1:ucLabel>
                </td>
                <td style="font-weight: bold; width: 5%">
                    :
                </td>
                <td style="font-weight: bold">
                    <cc1:ucTextbox ID="txtNumberofLabels" runat="server"></cc1:ucTextbox>
                </td>
            </tr>
            <tr class="fieldset-form">
                <td>
                    <cc1:ucLabel ID="lblDebitRaised" runat="server" Text="Debit to be Raised"></cc1:ucLabel>
                </td>
                <td style="font-weight: bold; width: 5%">
                    :
                </td>
                <td style="font-weight: bold">
                    <cc1:ucDropdownList runat="server" ID="ddlDebitRaised">
                        <asp:ListItem Text="Yes" Value="1"></asp:ListItem>
                        <asp:ListItem Text="No" Value="0"></asp:ListItem>
                    </cc1:ucDropdownList>
                </td>
            </tr>
            <tr>
                <td colspan="3" align="center">
                    <cc1:ucButton runat="server" Text="OK" />
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>

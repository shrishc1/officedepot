﻿using System;
using System.Web.UI;
using Utilities; 

public partial class ModuleUI_Discrepancy_LogDiscrepancy_DIS_PrintLabel : CommonPage
{
    protected string ltlable = string.Empty;
    protected string LabelHTML = string.Empty;
    protected string AppPath = string.Empty;
    protected string strQueryString = string.Empty;
    protected string redirect = string.Empty;
    protected void Page_Load(object sender, EventArgs e)
    {
        ltlable = Request.QueryString["ltid"].ToString();
        if (Request.QueryString["AppPath"] != null)
            AppPath = Request.QueryString["AppPath"].ToString();

        #region Logic to create the encrypted query string value ...
        var varFromDate = Convert.ToString(Request.QueryString["FromPage"]);
        if (!string.IsNullOrEmpty(varFromDate))
            strQueryString = string.Format("FromPage={0}", varFromDate);

        var varSiteId = Convert.ToString(Request.QueryString["SiteId"]);
        if (!string.IsNullOrEmpty(varSiteId))
            strQueryString = string.Format("{0}&SiteId={1}", strQueryString, varSiteId);

        var varDisDate = Convert.ToString(Request.QueryString["DisDate"]);
        if (!string.IsNullOrEmpty(varDisDate))
            strQueryString = string.Format("{0}&DisDate={1}", strQueryString, varDisDate);

        strQueryString = Encryption.Encrypt(strQueryString);
        redirect = Convert.ToString(Request.QueryString["redirect"]);
        #endregion

        Page.ClientScript.RegisterStartupScript(this.GetType(), "PrintJavaScript", "<script>printScreen();</script>");
    }
}
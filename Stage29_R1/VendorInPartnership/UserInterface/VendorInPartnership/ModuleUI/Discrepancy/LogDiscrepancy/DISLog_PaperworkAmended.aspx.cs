﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using BaseControlLibrary;
using BusinessEntities.ModuleBE.AdminFunctions;
using BusinessEntities.ModuleBE.Discrepancy.LogDiscrepancy;
using BusinessLogicLayer.ModuleBAL.AdminFunctions;
using BusinessLogicLayer.ModuleBAL.Discrepancy.LogDiscrepancy;
using Utilities;
using WebUtilities;

public partial class DISLog_PaperworkAmended : CommonPage
{
    #region Global variables

    protected string DeliveryNoteValidation = WebCommon.getGlobalResourceValue("DeliveryNoteValidation");
    protected string AmendedValidation = WebCommon.getGlobalResourceValue("AmendedValidation");
    protected string AtLeastOne = WebCommon.getGlobalResourceValue("AtLeastOne");
    protected string InvalidValuesEntered = WebCommon.getGlobalResourceValue("InvalidValuesEntered");
    protected string CannotEqualNoteValidation = WebCommon.getGlobalResourceValue("CannotEqualNoteValidation");
    protected string ValidEmail = WebCommon.getGlobalResourceValue("ValidEmail");
    protected string NoEmailSpecifiedMessage = WebCommon.getGlobalResourceValue("NoEmailSpecifiedMessage");
    protected string DiscrepancyAlreadyCreated = WebCommon.getGlobalResourceValue("DiscrepancyAlreadyCreated");
    string VEN016 = WebCommon.getGlobalResourceValue("VEN016");
    string InitialCommunicationSent = WebCommon.getGlobalResourceValue("InitialCommunicationSent");
    /*---------------SHOW ERROR MESSAGE WHEN QUERY IS ALREADY OPEN--------------*/
    string strThereIsCurrentlyAlreadyAnOpenQueryBeingInvestigated = WebCommon.getGlobalResourceValue("ThereIsCurrentlyAlreadyAnOpenQueryBeingInvestigated");
    string strPleaseWaitUntilYouHaveReceivedAReplyForThisBefore = WebCommon.getGlobalResourceValue("PleaseWaitUntilYouHaveReceivedAReplyForThisBefore");
    //static int SiteID = 0;
    static string VDRNo = string.Empty;
    //static int DiscrepancyLogID = 0;

    public int iVendorID
    {
        get
        {
            return ViewState["iVendorID"] != null ? Convert.ToInt32(ViewState["iVendorID"].ToString()) : 0;
        }
        set
        {
            ViewState["iVendorID"] = value;
        }
    }

    public int DiscrepancyLogID
    {
        get
        {
            return ViewState["DiscrepancyLogID"] != null ? Convert.ToInt32(ViewState["DiscrepancyLogID"].ToString()) : 0;
        }
        set
        {
            ViewState["DiscrepancyLogID"] = value;
        }
    }

    public int SiteID
    {
        get
        {
            return ViewState["SiteID"] != null ? Convert.ToInt32(ViewState["SiteID"].ToString()) : 0;
        }
        set
        {
            ViewState["SiteID"] = value;
        }
    }
    System.Text.StringBuilder sSendMailLink = new System.Text.StringBuilder();
    #endregion

    #region Events

    protected void Page_InIt(object sender, EventArgs e)
    {
        ucSite.CurrentPage = this;
        ucApAction.CurrentPage = this;
    }
    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);
        if (!IsPostBack)
        {
            if (GetQueryStringValue("VDRNo") != null && !string.IsNullOrWhiteSpace(GetQueryStringValue("VDRNo").ToString()))
            {
               

                if (GetQueryStringValue("PN") != null && !string.IsNullOrWhiteSpace(GetQueryStringValue("PN")))
                {
                    if (GetQueryStringValue("PN").Trim().ToUpper().Equals("POINQ"))
                    {
                        btnBack_1.Visible = true;
                    }
                }
                else if (GetQueryStringValue("PreviousPage") != null)
                {
                    string iframeStyleStatus = ifUserControl.Style["display"];
                    if (GetQueryStringValue("PreviousPage").Trim().ToUpper().Equals("SearchResultVendor".ToUpper()) &&
                        iframeStyleStatus != null)
                        btnBack_1.Visible = true;
                    else if (GetQueryStringValue("PreviousPage").Trim().Equals("SearchResult") &&
                        iframeStyleStatus != null)
                    {
                        btnBack_1.Visible = true;
                    }
                    else
                    {
                        btnBack_1.Visible = false;
                        btnUpdateLocation.Visible = false;
                    }
                }
            }
            else
            {
                UIUtility.PageValidateForODUser();
            }
        }

        if (GetQueryStringValue("VDRNo") != null && !string.IsNullOrWhiteSpace(GetQueryStringValue("VDRNo").ToString()) && !lblPaperworkAmended.Text.ToString().Contains("-"))
        {
            lblPaperworkAmended.Text = lblPaperworkAmended.Text + " - " + GetQueryStringValue("VDRNo").ToString();
        }
    }

    protected void btnBack_1_Click(object sender, EventArgs e)
    {
        if (GetQueryStringValue("PreviousPage") != null)
        {
            if (GetQueryStringValue("PreviousPage").Trim().ToUpper().Equals("SearchResultVendor".ToUpper()))
            {
                btnBack_1.Visible = false;
                EncryptQueryString("~/ModuleUI/Discrepancy/DIS_SearchResultVendor.aspx?disLogID=" + Convert.ToString(GetQueryStringValue("disLogID"))
                            + "&VDRNo=" + Convert.ToString(GetQueryStringValue("VDRNo"))
                            + "&UserID=" + Convert.ToString(GetQueryStringValue("UserID"))
                            + "&PreviousPage=SearchResultVendor&WorkFlowID=" + Convert.ToString(GetQueryStringValue("WorkFlowID").ToString()));
            }

            if (Session["PageMode"].ToString() == "Todays")
            {
                EncryptQueryString("../DIS_SearchResult.aspx?FromPage=images&SiteId=" + GetQueryStringValue("SiteId") + "&DisDate=" + GetQueryStringValue("DisDate"));
            }
            else if (Session["PageMode"].ToString() == "Search")
            {
                //For opening search discrepancies mode
                btnBack_1.Visible = false;
                EncryptQueryString("~/ModuleUI/Discrepancy/DIS_SearchResult.aspx?disLogID=" + Convert.ToString(GetQueryStringValue("disLogID"))
                            + "&VDRNo=" + Convert.ToString(GetQueryStringValue("VDRNo"))
                            + "&UserID=" + Convert.ToString(GetQueryStringValue("UserID"))
                            + "&PreviousPage=SearchResult&WorkFlowID=" + Convert.ToString(GetQueryStringValue("WorkFlowID").ToString()));

            }
        }
        else if (GetQueryStringValue("PN") != null)
        {
            if (GetQueryStringValue("PN").Trim().ToUpper().Equals("POINQ") || GetQueryStringValue("PN").Trim().ToUpper().Equals("OPENPL"))
            {
                btnBack_1.Visible = false;
                EncryptQueryString("~/ModuleUI/Appointment/PurchaseOrderInquiry.aspx?SId=" + GetQueryStringValue("SId").ToString()
                    + "&Status=" + GetQueryStringValue("Status").ToString()
                    + "&PN=DISLOG"
                    + "&PO=" + GetQueryStringValue("PO").ToString());
            }
            else if (GetQueryStringValue("PN").Trim().ToUpper().Equals("OPENPL"))
            {
                EncryptQueryString("~/ModuleUI/StockOverview/Expedite/OpenPurchaseListing.aspx?action=showlisting");
            }
        }
        else if (GetQueryStringValue("PageType") != null)
        {
            EncryptQueryString("~/ModuleUI/Discrepancy/DIS_LocationReport.aspx");
        }
    }
    void ucApAction_SaveEventValidation(object sender, EventArgs e)
    {
        if (ucApAction.IsModelOpen)
            mdlAPAction.Show();
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        ucApAction.SaveEventValidation += new EventHandler(ucApAction_SaveEventValidation);
        txtPurchaseOrder.Focus();
        if (GetQueryStringValue("disLogID") != null)
        {
            mvPaperAmended.ActiveViewIndex = 1;
        }
        else
        {
            ifUserControl.Style["display"] = "none";
        }

        if (mvPaperAmended.ActiveViewIndex == 1)
        {
            btnLogDiscrepancy.Visible = false;
            btnBack.Visible = true;
            btnSave.Visible = true;
        }
        else
        {
            btnLogDiscrepancy.Visible = true;
            btnBack.Visible = false;
            btnSave.Visible = false;
        }

        if (!IsPostBack)
        {
            //txtDateDeliveryArrived.SelectedDate = DateTime.Now;
            txtDateDeliveryArrived.innerControltxtDate.Value = Convert.ToString(DateTime.Now.ToString("dd/MM/yyyy"));
            lblIssueRaisedDate.Text = Convert.ToString(DateTime.Now.ToString("dd/MM/yyyy - HH:MM"));
            VendorsQueryOverview();
            if (GetQueryStringValue("disLogID") != null)
            {
                this.BindDiscrepancyLogComment();
                if (GetQueryStringValue("status").ToLower() == "closed")
                {
                    btnUpdateLocation.Visible = true;
                  //  txtLocation.ReadOnly = true;
                }
                else
                {
                    btnUpdateLocation.Visible = true;
                }
                if (GetQueryStringValue("PageType") != null && GetQueryStringValue("PageType") == "Location")
                {
                    btnUpdateLocation.Visible = false;
                    txtLocation.ReadOnly = true;
                    btnBack_1.Visible = true;
                }
                mvPaperAmended.ActiveViewIndex = 1;
                GetDeliveryDetails();
                GetContactPurchaseOrderDetails();
                if (Session["Role"] != null)
                {
                    GetAccessRightRaiseQuery();
                    if (Session["Role"].ToString() == "Vendor")
                    {
                        pnlInternalComments.Visible = false;
                        btnAddComment.Visible = true;
                        btnAddImages.Visible = false;
                        btnRaiseQuery.Visible = true;
                        btnAccountsPayableAction.Visible = false;
                    }
                }
                AddUserControl();
                this.ShowACP001ActionButton();
                btnLogDiscrepancy.Visible = false;
                btnBack.Visible = false;
                btnSave.Visible = false;
                if (Session["Role"] != null && (Session["Role"].ToString().ToLower() == "vendor" || Session["Role"].ToString().ToLower() == "carrier"))
                {
                    btnViewWF.Visible = false;
                }
                else
                {
                    btnViewWF.Visible = true;
                }
                // code for generating div for the imageviewer
                string DivGallery = CommonPage.GetImagesStringNewLayout(Convert.ToInt32(GetQueryStringValue("disLogID")));
                if (string.IsNullOrEmpty(DivGallery))
                {
                    btnViewImage.Visible = false;
                }
                else
                {
                    discrepancygallery.InnerHtml = DivGallery.ToString();
                    btnViewImage.Visible = true;
                }
            }
            else
            {
                btnAddComment.Visible = false;
                ifUserControl.Style["display"] = "none";
                btnViewWF.Visible = false;
                btnViewImage.Visible = false;
                btnAddImages.Visible = false;
                btnRaiseQuery.Visible = false;
                pnlQueryOverview_1.Visible = false;
                btnAccountsPayableAction.Visible = false;
            }
        }
        txtPurchaseOrder.Attributes.Add("autocomplete", "off");
        txtPurchaseOrder.Focus();
    }

    public override void SitePost_Load()
    {
        base.SitePost_Load();
        if (!IsPostBack)
        {
            //ucSite.innerControlddlSite.SelectedIndex = 0;
            ucSite.innerControlddlSite.AutoPostBack = true;

        }
    }
    protected void btnAccountsPayableAction_Click(object sender, EventArgs e)
    {
        mdlAPAction.Show();
        DropDownList ddl = ucApAction.FindControl("ddlCurrency") as DropDownList;
        DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
        DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();
        oDiscrepancyBE.Action = "GetCurrencyByVendorID";
        if (GetQueryStringValue("disLogID") != null)
        {
            oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID"));
        }
        ddl.SelectedValue = oDiscrepancyBAL.GetCurrencyBAL(oDiscrepancyBE);      
    }
    private void ShowACP001ActionButton()
    {
        DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
        DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();
        oDiscrepancyBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
        string CaseOf = String.Empty;

        oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID"));

        if (GetQueryStringValue("RoleTypeFlag") == "A" || GetQueryStringValue("RoleTypeFlag") == null)
        {
            if (Session["UserID"] == null || Session["UserID"].ToString() == "" || Session["Role"] == null || Session["Role"].ToString().Trim() == "")
                return;

            oDiscrepancyBE.User.UserID = Convert.ToInt32(Session["UserID"]);
            CaseOf = Session["Role"].ToString().Trim();
        }
        //else
        //{
        //    switch (GetQueryStringValue("RoleTypeFlag").ToString())
        //    {
        //        case "G":
        //            CaseOf = "OD - Goods In";
        //            break;
        //        case "S":
        //            CaseOf = "OD - Stock Planner";
        //            break;
        //        case "P":
        //            CaseOf = "OD - Accounts Payable";
        //            break;
        //        case "V":
        //            CaseOf = "Vendor";
        //            break;
        //    }
        //}

        /* Here P is an "OD - Accounts Payable" When from drop down selected the Account Payble option */
        /* Here A will come when actual Account Payble type user will loggedin. */
        if (GetQueryStringValue("RoleTypeFlag") == "P" || GetQueryStringValue("RoleTypeFlag") == "A")
        {
            oDiscrepancyBE.Action = "CheckForActionRequired";
            oDiscrepancyBE.DiscrepancyWorkFlowID = !string.IsNullOrEmpty(GetQueryStringValue("WorkFlowID")) ? Convert.ToInt32(GetQueryStringValue("WorkFlowID")) : (int?)null;
            DataTable dtActionDetails = oDiscrepancyBAL.GetActionRequiredDetailsForWorkFlowBAL(oDiscrepancyBE);
            if (dtActionDetails != null && dtActionDetails.Rows.Count > 0)
            {
                //if (dtActionDetails.Rows[0]["APUserControl"].ToString().Trim() == "ACP001")
                //if (Convert.ToString(dtActionDetails.Rows[0]["ACP001Control"]).Trim().Equals("ACP001")
                //    && !Convert.ToBoolean(dtActionDetails.Rows[0]["IsACP001Done"]))
                if (!string.IsNullOrEmpty(dtActionDetails.Rows[0]["IsACP001Done"].ToString()) && !Convert.ToBoolean(dtActionDetails.Rows[0]["IsACP001Done"]))
                {
                if (GetQueryStringValue("disLogID") != null)
                    ucApAction.DisLogID = Convert.ToInt32(GetQueryStringValue("disLogID"));

                ucApAction.DisWFlowID = Convert.ToInt32(dtActionDetails.Rows[0]["DiscrepancyWorkflowID"]);
                ucApAction.PONumber = Convert.ToString(dtActionDetails.Rows[0]["PurchaseOrderNumber"]);
                ucApAction.SiteID = SiteID;
                ucApAction.VDRNo = VDRNo;
                ucApAction.FromPage = "overs";
                ucApAction.PreviousPage = GetQueryStringValue("PreviousPage").ToString();

                if (ViewState["VendorID"] != null)
                    ucApAction.VendorID = Convert.ToInt32(ViewState["VendorID"]);

                btnAccountsPayableAction.Visible = true;
                //ifUserControl.Style["display"] = "none";
                 }
                else
                {
                    btnAccountsPayableAction.Visible = false;
                }
            }
        }
    }
    public override void SiteSelectedIndexChanged()
    {
        if (txtPurchaseOrder.Text != string.Empty)
        {
            GetPurchaseOrderList();
        }

    }

    protected void btnViewWF_Click(object sender, EventArgs e)
    {
        if (GetQueryStringValue("PN") != null)
        {
            if (GetQueryStringValue("PN").Trim().ToUpper().Equals("POINQ"))
            {
                string url = string.Format("DISLog_Workflow.aspx?disLogID={0}&PN={1}&SId={2}&Status={3}&PN={4}&PO={5}", GetQueryStringValue("disLogID").ToString(), "POINQ", GetQueryStringValue("SId").ToString(), GetQueryStringValue("Status").ToString(), "DISLOG", GetQueryStringValue("PO").ToString());
                EncryptQueryString(url);
            }

        }
        if (GetQueryStringValue("disLogID") != null)
        {
            string url = string.Format("DISLog_Workflow.aspx?disLogID={0}&VDRNo={1}&TodayDis={2}&SiteID={3}&DisDate={4}&UserID={5}", GetQueryStringValue("disLogID").ToString(), GetQueryStringValue("VDRNo").ToString(), GetQueryStringValue("TodayDis"), GetQueryStringValue("SiteId"), GetQueryStringValue("DisDate"), GetQueryStringValue("UserID"));
            EncryptQueryString(url);
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        var defaultEmailAddress = Convert.ToString(ConfigurationManager.AppSettings["DefaultEmailAddress"]);
        var txtAltEmail = (ucTextbox)ucSDRCommunication1.FindControl("txtAltEmail");
        if (ucSDRCommunication1.VendorEmailList.Trim().Equals(defaultEmailAddress) && (string.IsNullOrEmpty(txtAltEmail.Text) || string.IsNullOrWhiteSpace(txtAltEmail.Text)))
        {
            mdlAlternateEmail.Show();
            rfvEmailAddressCanNotBeEmpty.Enabled = true;
            txtAlternateEmailText.Focus();
        }
        else
        {
            if (Session["Role"].ToString().ToLower() == "vendor")
            {
                string LoginPath = ResolveClientUrl("../../../ModuleUI/Security/Login.aspx");
                Response.Redirect(LoginPath);
            }
            else
            {
                this.Save();
            }
        }
    }

    private void Save()
    {
        try
        {

            int iAllEmpty = 0;
            bool bSendMail = false;
            foreach (GridViewRow row in grdPaperWorkAmended.Rows)
            {

                TextBox txtDeliveryNoteQty = (TextBox)row.FindControl("txtDeliveryNoteQty");
                TextBox txtAmendedQty = (TextBox)row.FindControl("txtAmendedQty");

                if (txtDeliveryNoteQty.Text.Trim() == "" && txtAmendedQty.Text.Trim() == "")
                {
                    iAllEmpty++;
                    continue;
                }
                else
                {
                    int iDeliveryQty, iDeliveryNoteQty;
                    bool isiDeliveryQtyDecimal = int.TryParse(txtAmendedQty.Text.Trim(), out iDeliveryQty);
                    bool isDeliveryNoteQtyDecimal = int.TryParse(txtDeliveryNoteQty.Text.Trim(), out iDeliveryNoteQty);
                    if (txtAmendedQty.Text.Trim() != "" && !isiDeliveryQtyDecimal)
                    {
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + InvalidValuesEntered + "')", true);
                        return;
                    }
                    if (txtDeliveryNoteQty.Text.Trim() != "" && !isDeliveryNoteQtyDecimal)
                    {
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + InvalidValuesEntered + "')", true);
                        return;
                    }

                    if (txtAmendedQty.Text.Trim() != "" && isiDeliveryQtyDecimal &&
                       !txtAmendedQty.Text.IsNumeric())
                    {
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + InvalidValuesEntered + "')", true);
                        return;
                    }
                    if (txtDeliveryNoteQty.Text.Trim() != "" && isDeliveryNoteQtyDecimal &&
                        !txtDeliveryNoteQty.Text.IsNumeric())
                    {
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + InvalidValuesEntered + "')", true);
                        return;
                    }

                    if (txtDeliveryNoteQty.Text.Trim() == "" && txtAmendedQty.Text.Trim() != "")
                    {
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + DeliveryNoteValidation + "')", true);
                        return;
                    }

                    if (txtDeliveryNoteQty.Text.Trim() != "" && txtAmendedQty.Text.Trim() == "")
                    {
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + AmendedValidation + "')", true);
                        return;
                    }

                    if (txtDeliveryNoteQty.Text.Trim() != "" && txtAmendedQty.Text.Trim() != "" && (txtDeliveryNoteQty.Text.Trim() == txtAmendedQty.Text.Trim()))
                    {
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + CannotEqualNoteValidation + "')", true);
                        return;
                    }
                }
            }

            if (iAllEmpty > 0 && iAllEmpty == grdPaperWorkAmended.Rows.Count)
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + AtLeastOne + "')", true);
                return;
            }

            btnSave.Enabled = false;
             //to handle duplicating discrepancies in edit mode.

            if (ViewState["PurchaseOrderID"] != null && !(string.IsNullOrEmpty(lblPurchaseOrderDateValue.Text.Split('-')[1].ToString())))
            {
                DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
                DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();

                oDiscrepancyBE.Action = "AddDiscrepancyLog";
                oDiscrepancyBE.SiteID = Convert.ToInt32(ucSite.innerControlddlSite.SelectedValue);
                oDiscrepancyBE.DiscrepancyTypeID = Convert.ToInt32(Common.DiscrepancyType.PaperworkAmended);
                oDiscrepancyBE.DiscrepancyLogDate = DateTime.Now;
                oDiscrepancyBE.PurchaseOrderID = ViewState["PurchaseOrderID"] != null ? Convert.ToInt32(ViewState["PurchaseOrderID"].ToString()) : (int?)null;
                oDiscrepancyBE.PurchaseOrderDate = Common.GetMM_DD_YYYY(lblPurchaseOrderDateValue.Text.Split('-')[1].ToString());
                oDiscrepancyBE.PurchaseOrderNumber = txtPurchaseOrder.Text.Trim();
                oDiscrepancyBE.InternalComments = txtInternalComments.Text;
                oDiscrepancyBE.DeliveryNoteNumber = txtDeliveryNumber.Text.Trim();
                oDiscrepancyBE.DeliveryArrivedDate = Common.GetMM_DD_YYYY(txtDateDeliveryArrived.innerControltxtDate.Value); //DateTime.Now;
                oDiscrepancyBE.CommunicationType = ucSDRCommunication1.innerControlRdoCommunication;
                oDiscrepancyBE.CommunicationTo = ucSDRCommunication1.VendorEmailList;
                oDiscrepancyBE.VendorID = ViewState["VendorID"] != null ? Convert.ToInt32(ViewState["VendorID"].ToString()) : (int?)null;

                oDiscrepancyBE.StockPlannerID = ViewState["StockPlannerID"] != null ? Convert.ToInt32(ViewState["StockPlannerID"].ToString()) : (int?)null;

                oDiscrepancyBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
                oDiscrepancyBE.User.Site = new BusinessEntities.ModuleBE.AdminFunctions.MAS_SiteBE();
                oDiscrepancyBE.User.UserID = Convert.ToInt32(Session["UserID"]);
                oDiscrepancyBE.Location = txtLocation.Text.ToString();
                string sResult = oDiscrepancyBAL.addDiscrepancyLogBAL(oDiscrepancyBE);
                oDiscrepancyBAL = null;
                if (!sResult.Contains("Discrepancy Already Inserted"))
                {
                    int iResult = Convert.ToInt32(sResult.Split('-')[1].ToString());

                    if (iResult > 0)
                    {
                        DiscrepancyLogID = iResult;


                        foreach (GridViewRow gv in grdPaperWorkAmended.Rows)
                        {
                            DiscrepancyBE oNewDiscrepancyBE = new DiscrepancyBE();
                            DiscrepancyBAL oNewDiscrepancyBAL = new DiscrepancyBAL();

                            TextBox txtDeliveryNoteQty = (TextBox)gv.FindControl("txtDeliveryNoteQty");
                            TextBox txtAmendedQty = (TextBox)gv.FindControl("txtAmendedQty");

                            if (txtDeliveryNoteQty.Text.Trim() != "" || txtDeliveryNoteQty.Text.Trim() != string.Empty
                                 && txtAmendedQty.Text.Trim() != "" || txtAmendedQty.Text.Trim() != string.Empty)
                            {
                                Label lblLineNo = (Label)gv.FindControl("lblLineNo");
                                Label lblVikingCode = (Label)gv.FindControl("lblVikingCodeValue");
                                Label lblOfficeDepotCode = (Label)gv.FindControl("lblOfficeDepotCodeValue");
                                Label lblVendorItemCode = (Label)gv.FindControl("lblVendorItemCodeValue");
                                Label lblDescription = (Label)gv.FindControl("lblDescriptionValue");
                                Label lblOutstandingQty = (Label)gv.FindControl("lblOutstandingQty1");
                                Label lblUoM1 = (Label)gv.FindControl("lblUoM1");
                                HiddenField hdnPurchaseOrderId = (HiddenField)gv.FindControl("hdnPurchaseOrderID");
                                oNewDiscrepancyBE.Action = "AddDiscrepancyItem";
                                oNewDiscrepancyBE.Line_no = Convert.ToInt32(lblLineNo.Text);
                                oNewDiscrepancyBE.DirectCode = lblVikingCode.Text;
                                oNewDiscrepancyBE.ODSKUCode = lblOfficeDepotCode.Text;
                                oNewDiscrepancyBE.VendorCode = lblVendorItemCode.Text;
                                oNewDiscrepancyBE.ProductDescription = lblDescription.Text;
                                oNewDiscrepancyBE.OutstandingQuantity = Convert.ToInt32(lblOutstandingQty.Text);
                                oNewDiscrepancyBE.UOM = lblUoM1.Text;
                                oNewDiscrepancyBE.DeliveredNoteQuantity = Convert.ToInt32(txtDeliveryNoteQty.Text);
                                oNewDiscrepancyBE.AmendedQuantity = Convert.ToInt32(txtAmendedQty.Text);

                                oNewDiscrepancyBE.PurchaseOrderID = Convert.ToInt32(hdnPurchaseOrderId.Value);

                                oNewDiscrepancyBE.DiscrepancyLogID = iResult;

                                int? iResult2 = oNewDiscrepancyBAL.addDiscrepancyItemBAL(oNewDiscrepancyBE);
                                //bSendMail = true;
                            }
                        }
                        bSendMail = true;
                        if (bSendMail)
                        {
                            int? iDiscrepancyCommunicationID = sendAndSaveDiscrepancyCommunication(iResult);
                        }

                        if (ucSDRCommunication1.innerControlRdoCommunication == 'L')
                        {
                            sendCommunication communication = new sendCommunication();
                            int vendorId = ViewState["VendorID"] != null ? Convert.ToInt32(ViewState["VendorID"].ToString()) : 0;
                            Session["LabelHTML"] = communication.sendAndSaveLetter(DiscrepancyLogID, "paperworkamended", vendorId);
                            sLetterLink.Append(communication.sLetterLink);
                        }
                        InsertHTML();
                        EncryptQueryString("DIS_OptionsPopup.aspx?VDRNo=" + sResult.Split('-')[0].ToString()
                            + "&DiscrepancyLogID=" + sResult.Split('-')[1].ToString()
                            + "&CommunicationType=" + ucSDRCommunication1.innerControlRdoCommunication
                            + "&SiteId=" + ucSite.innerControlddlSite.SelectedValue
                            + "&DisDate=" + DateTime.Now.ToString("dd/MM/yyyy"));
                    }
                }
                else if (sResult.Contains("Discrepancy Already Inserted"))
                {
                    //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + DiscrepancyAlreadyCreated + "')", true);
                    EncryptQueryString("DIS_OptionsPopup.aspx?VDRNo=" + sResult.Split('-')[0].ToString()
                           + "&DiscrepancyLogID=" + sResult.Split('-')[1].ToString()
                           + "&CommunicationType=" + ucSDRCommunication1.innerControlRdoCommunication
                           + "&SiteId=" + ucSite.innerControlddlSite.SelectedValue
                           + "&DisDate=" + DateTime.Now.ToString("dd/MM/yyyy"));
                }
            }
            else
            {
                EncryptQueryString("~/ModuleUI/Discrepancy/DIS_SearchResult.aspx?disLogID=" + Convert.ToString(GetQueryStringValue("disLogID"))
                                + "&VDRNo=" + Convert.ToString(GetQueryStringValue("VDRNo"))
                                + "&UserID=" + Convert.ToString(GetQueryStringValue("UserID"))
                                + "&PreviousPage=SearchResult&WorkFlowID=" + Convert.ToString(GetQueryStringValue("WorkFlowID").ToString()));
            }
        }
        catch (Exception ex)
        {
            LogUtility.SaveErrorLogEntry(ex);
        }
    }

    protected void btnLogDiscrepancy_Click(object sender, EventArgs e)
    {
        DateTime dt = Common.GetMM_DD_YYYY(txtDateDeliveryArrived.innerControltxtDate.Value);
        if (dt > DateTime.Now.Date)
        {
            string saveMessage = WebCommon.getGlobalResourceValue("NotDateValidMessage");
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('" + saveMessage + "')", true);
            return;
        }
        mvPaperAmended.ActiveViewIndex = 1;
        btnLogDiscrepancy.Visible = false;
        btnBack.Visible = true;
        btnSave.Visible = true;
        btnSave.Enabled = true;      
        GetDeliveryDetails();
        GetContactPurchaseOrderDetails();
        ucSDRCommunication1.innerControlRdoCommunication = 'E';
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        mvPaperAmended.ActiveViewIndex = 0;
        btnLogDiscrepancy.Visible = true;
        btnBack.Visible = false;
        btnSave.Visible = false;
    }

    protected void txtPurchaseOrder_TextChanged(object sender, EventArgs e)
    {
        GetPurchaseOrderList();
        txtDeliveryNumber.Focus();
    }

    protected void ddlPurchaseOrderDate_SelectedIndexChanged(object sender, System.EventArgs e)
    {
        //lblVendorValue.Text = ddlPurchaseOrderDate.SelectedItem.Value.ToString();
        if (ViewState["GetPODateList"] != null)
        {
            List<DiscrepancyBE> lstGetPODateList = (List<DiscrepancyBE>)ViewState["GetPODateList"];
            if (!string.IsNullOrEmpty(ddlPurchaseOrderDate.SelectedItem.Value))
            {
                var PODateDetail = lstGetPODateList.Find(p => p.RowNumber == Convert.ToInt32(ddlPurchaseOrderDate.SelectedItem.Value));
                lblVendorValue.Text = PODateDetail.VendorNoName;

                if (PODateDetail.VendorID > 0)
                    ucSDRCommunication1.VendorID = Convert.ToInt32(PODateDetail.VendorID);
                else
                    ucSDRCommunication1.VendorID = 0;

                if (!string.IsNullOrEmpty(ucSite.innerControlddlSite.SelectedValue))
                    ucSDRCommunication1.SiteID = Convert.ToInt32(ucSite.innerControlddlSite.SelectedValue);
                else
                    ucSDRCommunication1.SiteID = 0;
                ucSDRCommunication1.GetDetails();
            }
        }
    }

    public override BusinessEntities.BaseBe[] SubSort(GridViewSortEventArgs e)
    {
        return Utilities.GenericListHelper<DiscrepancyBE>.SortList((List<DiscrepancyBE>)ViewState["lstSitesViewMode"], e.SortExpression, e.SortDirection).ToArray();
    }

    protected void btnSave_1_Click(object sender, EventArgs e)
    {
        DiscrepancyBE discrepancyBE = null;
        DiscrepancyBAL discrepancyBAL = null;

        if (!string.IsNullOrEmpty(txtUserComments.Text) && !string.IsNullOrWhiteSpace(txtUserComments.Text))
        {
            lblErrorMsg.Text = string.Empty;
            discrepancyBE = new DiscrepancyBE();
            discrepancyBE.Action = "AddDiscrepancyLogComment";

            if (!string.IsNullOrEmpty(GetQueryStringValue("disLogID")))
                discrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID"));
            else
                discrepancyBE.DiscrepancyLogID = 0;

            discrepancyBE.UserID = Convert.ToInt32(Session["UserID"]);
            discrepancyBE.CommentDateTime = DateTime.Now;
            discrepancyBE.Comment = txtUserComments.Text;
            discrepancyBAL = new DiscrepancyBAL();
            discrepancyBAL.AddDiscrepancyLogCommentBAL(discrepancyBE);
            discrepancyBE.Action = "GetDiscrepancyLogComment";
            this.BindDiscrepancyLogComment(discrepancyBE);

            discrepancyBE.DiscrepancyType = "paperworkamended";
            int vendorID = 0;
            if (ViewState["VendorID"] != null)
                vendorID = Convert.ToInt32(ViewState["VendorID"]);
            new SendCommentMail().SendCommentsMail(discrepancyBE,
                (BaseControlLibrary.ucTextbox)ucSDRCommunication1.FindControl("txtAltEmail"),
                (BaseControlLibrary.ucTextbox)ucSDRCommunication1.FindControl("ucVendorEmailList"),
                vendorID, txtUserComments.Text, Session["Role"].ToString());

            mdlAddComment.Hide();
        }
        else
        {
            lblErrorMsg.Text = WebCommon.getGlobalResourceValue("MustEnterComment");
            mdlAddComment.Show();
        }

        btnSave.Visible = false;
        btnBack.Visible = false;
        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "Reload()", true);
    }

    protected void btnBack_2_Click(object sender, EventArgs e)
    {
        mdlAddComment.Hide();
        btnSave.Visible = false;
        btnBack.Visible = false;
        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "Reload()", true);
    }

    protected void btnAddComment_Click(object sender, EventArgs e)
    {
        txtUserComments.Text = string.Empty;
        lblErrorMsg.Text = string.Empty;
        lblUserT.Text = Convert.ToString(Session["UserName"]);
        lblDateT.Text = DateTime.Now.Date.ToString("dd/MM/yyyy");
        if (GetQueryStringValue("VDRNo") != null)
            lblDiscrepancyNoT.Text = Convert.ToString(GetQueryStringValue("VDRNo"));
        mdlAddComment.Show();
        txtUserComments.Focus();
        btnSave.Visible = false;
        btnBack.Visible = false;

    }

    protected void btnAddImages_Click(object sender, EventArgs e) {

        EncryptQueryString("DIS_OptionsPopup.aspx?Mode=Edit&VDRNo=" + GetQueryStringValue("VDRNo").ToString() + "&DiscrepancyLogID=" + GetQueryStringValue("disLogID").ToString() + "&CommunicationType=" + Common.DiscrepancyType.PaperworkAmended);
    }
    /* ENHANCEMENT FOR PHASE R3 */
    /*RAISE QUERY BUTTON*/
    protected void btnRaiseQuery_Click(object sender, EventArgs e)
    {
        try
        {
            var queryDiscrepancyBAL = new DISLog_QueryDiscrepancyBAL();
            var queryDiscrepancyBE = new DISLog_QueryDiscrepancyBE();
            var discrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID"));

            if (!this.IsAlreadyDisputeExist(discrepancyLogID) || !this.IsQueryClosed(discrepancyLogID))
            {
                btnSave.Visible = false;
                btnBack.Visible = false;
                mdlRaiseQueryConfirmMsg.Show();              
            }
            else
            {

                lblMessageFirst.Text = strThereIsCurrentlyAlreadyAnOpenQueryBeingInvestigated;
                lblMessageSecond.Text = strPleaseWaitUntilYouHaveReceivedAReplyForThisBefore;
                btnSave.Visible = false;
                btnBack.Visible = false;
                mdlShowError.Show();
            }

        }

        catch (Exception ex)
        {
            LogUtility.SaveErrorLogEntry(ex);
        }


    }

    #region Alternate email code ...

    protected void btnSetAlternateEmail_Click(object sender, EventArgs e)
    {
        rfvEmailAddressCanNotBeEmpty.Enabled = false;
        var txtAltEmail = (ucTextbox)ucSDRCommunication1.FindControl("txtAltEmail");
        if (txtAltEmail != null)
        {
            txtAltEmail.Text = txtAlternateEmailText.Text;
            txtAlternateEmailText.Text = string.Empty;
        }

        if (Session["Role"].ToString().ToLower() == "vendor")
        {
            string LoginPath = ResolveClientUrl("../../../ModuleUI/Security/Login.aspx");
            Response.Redirect(LoginPath);
        }
        else
        {
            this.Save();
        }
    }

    protected void btnAddEmailId_Click(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(lblMultyAlternateEmail.Text) && !string.IsNullOrWhiteSpace(lblMultyAlternateEmail.Text))
        {
            if (!lblMultyAlternateEmail.Text.Contains(txtAlternateEmailText.Text))
            {
                lblMultyAlternateEmail.Text = string.Format("{0}, {1}", lblMultyAlternateEmail.Text, txtAlternateEmailText.Text);
            }
        }
        else
        {
            lblMultyAlternateEmail.Text = txtAlternateEmailText.Text;
        }

        lblMultyAlternateEmail.Visible = true;
        txtAlternateEmailText.Text = string.Empty;
        txtAlternateEmailText.Focus();
        mdlAlternateEmail.Show();
    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        txtAlternateEmailText.Text = string.Empty;
        lblMultyAlternateEmail.Text = string.Empty;
        txtAlternateEmailText.Focus();
        mdlAlternateEmail.Show();
    }

    #endregion

    #endregion

    #region Methods

    private void GetAccessRightRaiseQuery()
    {
        DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
        DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();
        oDiscrepancyBE.Action = "CheckUserRaiseQueryRight";
        oDiscrepancyBE.UserID = Convert.ToInt32(Session["UserID"]);
        oDiscrepancyBE.SiteID = Convert.ToInt32(Convert.ToString(hdnSiteID.Value));
        int? lstDetails = oDiscrepancyBAL.getAccessRightRaiseQueryBAL(oDiscrepancyBE);
        if (lstDetails > 0)
        {
            btnRaiseQuery.Visible = true;
        }
        else
        {
            btnRaiseQuery.Visible = false;
        }
    }

    private void GetDeliveryDetails()
    {
        if (GetQueryStringValue("disLogID") != null)
        {
            DiscrepancyBE oNewDiscrepancyBE = new DiscrepancyBE();
            DiscrepancyBAL oNewDiscrepancyBAL = new DiscrepancyBAL();
            oNewDiscrepancyBE.Action = "GetDiscrepancyLog";
            oNewDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
            if (GetQueryStringValue("status").ToLower() == "deleted")
            {
                oNewDiscrepancyBE.IsDelete = true;
            }
            List<DiscrepancyBE> lstDisLog = oNewDiscrepancyBAL.GetDiscrepancyLogDetailsBAL(oNewDiscrepancyBE);
            oNewDiscrepancyBAL = null;
            if (lstDisLog != null && lstDisLog.Count > 0)
            {
                hdnSiteID.Value = Convert.ToString(lstDisLog[0].Site.SiteID);
                lblSiteValue.Text = lstDisLog[0].Site.SiteName;
                lblPurchaseOrderValue.Text = lstDisLog[0].PurchaseOrderNumber;
                lblPurchaseOrderDateValue.Text = "- " + (Convert.ToDateTime(lstDisLog[0].PurchaseOrderDate)).ToString("dd/MM/yyyy");
                lblIssueRaisedDateValue.Text = (Convert.ToDateTime(lstDisLog[0].DiscrepancyLogDate)).ToString("dd/MM/yyyy");
                lblIssueRaisedTimeValue.Text = "- " + (Convert.ToDateTime(lstDisLog[0].DiscrepancyLogDate)).ToString("HH:MM");
                lblDeliveryNoValue.Text = lstDisLog[0].DeliveryNoteNumber.ToString();
                lblDeliveryDateValue.Text = "- " + (Convert.ToDateTime(lstDisLog[0].DeliveryArrivedDate)).ToString("dd/MM/yyyy");

                txtInternalComments.Text = lstDisLog[0].InternalComments.ToString();
                txtInternalComments.ReadOnly = true;

                lblVendorNumberValue.Text = lstDisLog[0].VendorNoName.ToString();
                //lblContactValue.Text = lstDisLog[0].Vendor.VendorContactNumber.ToString();
                lblContactValue.Text = UIUtility.GetUserPhoneNoofVendor(Convert.ToInt32(lstDisLog[0].VendorID), Convert.ToInt32(lstDisLog[0].SiteID));
                txtLocation.Text = lstDisLog[0].Location.ToString();
                lblStockPlannerNoValue.Text = lstDisLog[0].StockPlannerNO.ToString();
                lblPlannerContactNoValue.Text = lstDisLog[0].StockPlannerContact.ToString();
                SiteID = lstDisLog[0].Site.SiteID;
                VDRNo = lstDisLog[0].VDRNo.ToString();
                if (lstDisLog[0].DiscrepancyStatus == "CLOSED")
                    btnSave.Visible = false;
                ucSDRCommunication1.VendorEmailList = lstDisLog[0].CommunicationTo;
                ucSDRCommunication1.innerControlRdoCommunication = lstDisLog[0].CommunicationType.Value;
                ucSDRCommunication1.innerControlRdoLetter.Enabled = false;
                ucSDRCommunication1.innerControlRdoEmail.Enabled = false;
                ucSDRCommunication1.innerControlEmailList.Enabled = false;
                ucSDRCommunication1.innerControlAltEmailList.Enabled = false;

                ViewState["StockPlannerEmailID"] = lstDisLog[0].StockPlannerEmailID;
                lblSPActionComments.Text = "Comments : " + lstDisLog[0].ActionComments;

                lblDecision.Text = "Decision :  " + lstDisLog[0].Decision;

                if (!string.IsNullOrEmpty(lstDisLog[0].ActionComments) && !string.IsNullOrEmpty(lstDisLog[0].Decision))
                {
                    pnlDecisionComments.Visible = true;
                }
            }
        }
        else
        {
            hdnSiteID.Value = Convert.ToString(ucSite.innerControlddlSite.SelectedItem.Value);
            lblSiteValue.Text = ucSite.innerControlddlSite.SelectedItem.Text;
            lblPurchaseOrderValue.Text = txtPurchaseOrder.Text;
            lblPurchaseOrderDateValue.Text = "- " + ddlPurchaseOrderDate.SelectedItem.Text;
            lblIssueRaisedDateValue.Text = DateTime.Now.ToString("dd/MM/yyyy");
            lblIssueRaisedTimeValue.Text = DateTime.Now.ToString("- HH:MM");
            lblDeliveryNoValue.Text = txtDeliveryNumber.Text;
            lblDeliveryDateValue.Text = "- " + txtDateDeliveryArrived.innerControltxtDate.Value;
        }
    }

    private void GetContactPurchaseOrderDetails()
    {

        DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
        DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();
        if (GetQueryStringValue("disLogID") != null)
        {

            grdPaperWorkAmendedViewMode.Style["display"] = "";
            grdPaperWorkAmended.Style["display"] = "none";

            oDiscrepancyBE.Action = "GetDiscrepancyItem";
            oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
            List<DiscrepancyBE> lstDetails = oDiscrepancyBAL.GetDiscrepancyItemDetailsBAL(oDiscrepancyBE);
            oDiscrepancyBAL = null;
            if (lstDetails.Count > 0 && lstDetails != null)
            {
                grdPaperWorkAmendedViewMode.DataSource = lstDetails;
                grdPaperWorkAmendedViewMode.DataBind();
                ViewState["lstSitesViewMode"] = lstDetails;
                ViewState["VendorID"] = lstDetails[0].VendorID != null ? lstDetails[0].VendorID.ToString() : null;
            }
        }

        else
        {
            grdPaperWorkAmendedViewMode.Style["display"] = "none";
            grdPaperWorkAmended.Style["display"] = "";

            oDiscrepancyBE.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();

            oDiscrepancyBE.Action = "GetContactPurchaseOrderDetails";
            oDiscrepancyBE.PurchaseOrderNumber = txtPurchaseOrder.Text.Trim();
            oDiscrepancyBE.PurchaseOrder = new BusinessEntities.ModuleBE.Upload.Up_PurchaseOrderDetailBE();
            oDiscrepancyBE.PurchaseOrder.Order_raised = Common.GetMM_DD_YYYY(ddlPurchaseOrderDate.SelectedItem.Text);
            oDiscrepancyBE.SiteID = Convert.ToInt32(ucSite.innerControlddlSite.SelectedItem.Value);


            List<DiscrepancyBE> lstDetails = oDiscrepancyBAL.GetContactPurchaseOrderDetailsBAL(oDiscrepancyBE);
            oDiscrepancyBAL = null;
            if (lstDetails.Count > 0 && lstDetails != null)
            {
                if (!string.IsNullOrEmpty(ddlPurchaseOrderDate.SelectedItem.Value))
                {
                    List<DiscrepancyBE> lstGetPODateList = (List<DiscrepancyBE>)ViewState["GetPODateList"];
                    var PODateDetail = lstGetPODateList.Find(p => p.RowNumber == Convert.ToInt32(ddlPurchaseOrderDate.SelectedItem.Value));
                    lblVendorNumberValue.Text = PODateDetail.VendorNoName;
                } 

                //lblVendorNumberValue.Text = ddlPurchaseOrderDate.SelectedItem.Value.ToString();
                //lblContactValue.Text = lstDetails[0].Vendor.VendorContactNumber.ToString();
                lblContactValue.Text = UIUtility.GetUserPhoneNoofVendor(Convert.ToInt32(lstDetails[0].VendorID), Convert.ToInt32(ucSite.innerControlddlSite.SelectedItem.Value));

                lblStockPlannerNoValue.Text = lstDetails[0].StockPlannerNO.ToString();
                lblPlannerContactNoValue.Text = lstDetails[0].StockPlannerContact.ToString();

                //ucSDRCommunication1.innerControllblFaxNumber.Text = lstDetails[0].Vendor.VendorContactFax.ToString();
                //ucSDRCommunication1.innerControllblEmailID.Text = lstDetails[0].Vendor.VendorContactEmail.ToString();

                //ViewState["PurchaseOrderID"] = Convert.ToInt32(lstDetails[0].PurchaseOrderID.ToString());
                //ViewState["VendorID"] = Convert.ToInt32(lstDetails[0].VendorID.ToString());
                //ViewState["StockPlannerID"] = Convert.ToInt32(lstDetails[0].StockPlannerID.ToString());

                ViewState["PurchaseOrderID"] = lstDetails[0].PurchaseOrderID != null ? lstDetails[0].PurchaseOrderID.ToString() : null;
                ViewState["StockPlannerID"] = lstDetails[0].StockPlannerID != null ? lstDetails[0].StockPlannerID.ToString() : null;
                ViewState["VendorID"] = lstDetails[0].VendorID != null ? lstDetails[0].VendorID.ToString() : null;

                grdPaperWorkAmended.DataSource = lstDetails;
                grdPaperWorkAmended.DataBind();
                ViewState["lstSites"] = lstDetails;
            }

            //foreach (GridViewRow row in grdPaperWorkAmended.Rows)
            //{
            //    TextBox txtbox = (TextBox)row.FindControl("txtDeliveryQty");

            //    sp1.RegisterPostBackControl(txtbox);
            //}
        }
    }

    private void GetPurchaseOrderList()
    {
        DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
        DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();

        oDiscrepancyBE.Action = "GetPurchaseOrderDate";
        oDiscrepancyBE.SiteID = Convert.ToInt32(ucSite.innerControlddlSite.SelectedValue);
        oDiscrepancyBE.PurchaseOrderNumber = txtPurchaseOrder.Text.Trim();

        List<DiscrepancyBE> lstDetails = oDiscrepancyBAL.GetPurchaseOrderDateListBAL(oDiscrepancyBE);
        ViewState["GetPODateList"] = lstDetails;
        oDiscrepancyBAL = null;
        if (lstDetails.Count > 0 && lstDetails != null)
        {
            FillControls.FillDropDown(ref ddlPurchaseOrderDate, lstDetails, "PurchaseOrder_Order_raised", "RowNumber");
            if (!string.IsNullOrEmpty(ddlPurchaseOrderDate.SelectedItem.Value))
            {
                var PODateDetail = lstDetails.Find(p => p.RowNumber == Convert.ToInt32(ddlPurchaseOrderDate.SelectedItem.Value));
                lblVendorValue.Text = PODateDetail.VendorNoName;
                ucSDRCommunication1.VendorID = Convert.ToInt32(PODateDetail.VendorID);
                if (!string.IsNullOrEmpty(ucSite.innerControlddlSite.SelectedValue))
                    ucSDRCommunication1.SiteID = Convert.ToInt32(ucSite.innerControlddlSite.SelectedValue);
                else
                    ucSDRCommunication1.SiteID = 0;
            }

            //lblVendorValue.Text = ddlPurchaseOrderDate.SelectedItem.Value.ToString();
            //ucSDRCommunication1.VendorID = Convert.ToInt32(lstDetails[ddlPurchaseOrderDate.SelectedIndex].VendorID);
            //if (!string.IsNullOrEmpty(ucSite.innerControlddlSite.SelectedValue))
            //    ucSDRCommunication1.SiteID = Convert.ToInt32(ucSite.innerControlddlSite.SelectedValue);
            //else
            //    ucSDRCommunication1.SiteID = 0;
            ucSDRCommunication1.GetDetails();
        }
        else
        {
            string saveMessage = WebCommon.getGlobalResourceValue("InvalidPurchaseOrder");
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('" + saveMessage + "')", true);

            #region Clearing PO details
            txtPurchaseOrder.Text = string.Empty;
            ddlPurchaseOrderDate.Items.Clear();
            ListItem listItem = new ListItem();
            listItem.Text = "--Select--";
            listItem.Value = "0";
            ddlPurchaseOrderDate.Items.Add(listItem);
            lblVendorValue.Text = string.Empty;
            lblIssueRaisedDate.Text = string.Empty;
            txtPurchaseOrder.Focus();
            #endregion
        }
    }

    private int? sendAndSaveDiscrepancyCommunication(int iDiscrepancy)
    {
        int? vendorID = ViewState["VendorID"] == null ? 0 : Convert.ToInt32(ViewState["VendorID"].ToString());
        if (((RadioButton)ucSDRCommunication1.FindControl("rdoEmailComm")) != null && ((RadioButton)ucSDRCommunication1.FindControl("rdoEmailComm")).Checked)
        {
            if (((BaseControlLibrary.ucTextbox)ucSDRCommunication1.FindControl("txtAltEmail")) != null && ((BaseControlLibrary.ucTextbox)ucSDRCommunication1.FindControl("ucVendorEmailList")) != null)
            {
                sendCommunication communication = new sendCommunication();
                int? retVal = communication.sendCommunicationByEMail(iDiscrepancy, "PaperworkAmended", ((BaseControlLibrary.ucTextbox)ucSDRCommunication1.FindControl("txtAltEmail")), ((BaseControlLibrary.ucTextbox)ucSDRCommunication1.FindControl("ucVendorEmailList")), vendorID);
                sSendMailLink = communication.sSendMailLink;
                return retVal;
            }
        }
        return null;
    }

    private void InsertHTML()
    {

        /* Check for the site "No Escalation" */
        MAS_SiteBE oMAS_SiteBE = new MAS_SiteBE();
        MAS_SiteBAL oMAS_SiteBAL = new MAS_SiteBAL();
        oMAS_SiteBE.Action = "GetSiteDisSettings";
        oMAS_SiteBE.SiteID = Convert.ToInt32(ucSite.innerControlddlSite.SelectedItem.Value);

        oMAS_SiteBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();

        List<MAS_SiteBE> lstSiteDetails = oMAS_SiteBAL.GetSiteMisSettingBAL(oMAS_SiteBE);
        oMAS_SiteBAL = null;
        if (lstSiteDetails != null && lstSiteDetails.Count > 0 && lstSiteDetails[0].DIS_CommunicationWithEscalationType.Trim() != "N")
        {
            //site "No Escalation" is not on 
            DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
            DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();

            /*
                Step 1a. Paperwork Amended discrepancy raised, site not set to 'No Escalation'
             * Paperwork Amended discrepancy is created, Vendor to respond with remedial action.Go to 2
             */
            WorkflowHTML oWorkflowHTML = new WorkflowHTML();
            oDiscrepancyBE.GINHTML = oWorkflowHTML.function1(DateTime.Now, null, null, "Yellow", "", "Paperwork Amended Discrepancy Created", 
                //Session["UserName"].ToString(), 
                UIUtility.GetODVendorUserName(Convert.ToString(Session["Role"]), Convert.ToString(Session["UserName"]), Convert.ToString(Session["LoginID"])),
                txtInternalComments.Text);
            oDiscrepancyBE.GINActionRequired = false;

            oDiscrepancyBE.INVHTML = oWorkflowHTML.function3("White", null, "", "");
            oDiscrepancyBE.INVActionRequired = false;

            oDiscrepancyBE.APHTML = oWorkflowHTML.function3("White", null, "", "");
            oDiscrepancyBE.APActionRequired = false;

            if (ucSDRCommunication1.innerControlRdoCommunication == 'L')   // for letter
                oDiscrepancyBE.VENHTML = oWorkflowHTML.function4("Blue", "", DateTime.Now,
                                                            "",
                                                            "Admin@OfficeDepot.com",
                                                            InitialCommunicationSent,
                                                            VEN016, "", Convert.ToString(sLetterLink));
            else
                oDiscrepancyBE.VENHTML = oWorkflowHTML.function4("Blue", "", DateTime.Now,
                                                                Convert.ToString(sSendMailLink),
                                                                "Admin@OfficeDepot.com",
                                                                InitialCommunicationSent,
                                                                VEN016);
            oDiscrepancyBE.VenActionRequired = true;
            oDiscrepancyBE.VENUserControl = "VEN016";

            oDiscrepancyBE.Action = "InsertHTML";
            oDiscrepancyBE.DiscrepancyLogID = DiscrepancyLogID;
            oDiscrepancyBE.LoggedDateTime = DateTime.Now;
            oDiscrepancyBE.LevelNumber = 1;
            oDiscrepancyBE.ActionTakenBy = "GIN";
            oDiscrepancyBE.VendorActionCategory = "RA";  //Remedial Action
            int? iResult = oDiscrepancyBAL.addWorkFlowHTMLsBAL(oDiscrepancyBE);
            oDiscrepancyBAL = null;

        }
        else
        {
            /*
             * Step 1b.  Paperwork Amended discrepancy raised, site set to 'No Escalation'
             * Paperwork Amended discrepancy is created, no escalation is set for the site. 
             * No further actions required so 'Discrepancy Closed'
             */

            DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
            DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();

            WorkflowHTML oWorkflowHTML = new WorkflowHTML();

            oDiscrepancyBE.GINHTML = oWorkflowHTML.function1(DateTime.Now, null, null, "Yellow", "", "Paperwork Amended Discrepancy Created", 
                //Session["UserName"].ToString(), 
                UIUtility.GetODVendorUserName(Convert.ToString(Session["Role"]), Convert.ToString(Session["UserName"]), Convert.ToString(Session["LoginID"])),
                txtInternalComments.Text);
            oDiscrepancyBE.GINActionRequired = false;

            oDiscrepancyBE.INVHTML = oWorkflowHTML.function3("White", null, "", "");
            oDiscrepancyBE.INVActionRequired = false;

            oDiscrepancyBE.APHTML = oWorkflowHTML.function3("White", null, "", "");
            oDiscrepancyBE.APActionRequired = false;

            if (ucSDRCommunication1.innerControlRdoCommunication == 'L')   // for letter
                oDiscrepancyBE.VENHTML = oWorkflowHTML.function4("Blue", "", DateTime.Now,
                                                            "",
                                                            "Admin@OfficeDepot.com", "",
                                                            InitialCommunicationSent, "", Convert.ToString(sLetterLink));
            else
                oDiscrepancyBE.VENHTML = oWorkflowHTML.function4("Blue", "", DateTime.Now,
                                                            Convert.ToString(sSendMailLink),
                                                            "Admin@OfficeDepot.com", "",
                                                            InitialCommunicationSent);
            oDiscrepancyBE.VenActionRequired = false;

            oDiscrepancyBE.Action = "InsertHTML";
            oDiscrepancyBE.DiscrepancyLogID = DiscrepancyLogID;
            oDiscrepancyBE.LoggedDateTime = DateTime.Now;
            oDiscrepancyBE.LevelNumber = 1;
            oDiscrepancyBE.CloseDiscrepancy = true;
            int? iResult = oDiscrepancyBAL.addWorkFlowHTMLsBAL(oDiscrepancyBE);
            oDiscrepancyBAL = null;
        }
    }

    private void AddUserControl()
    {
        DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
        DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();
        oDiscrepancyBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
        string CaseOf = String.Empty;
        if (Session["Role"] != null && Session["Role"].ToString().Trim() == "Vendor")
        {
            btnUpdateLocation.Visible = false;
        }
        oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID"));

        if (GetQueryStringValue("RoleTypeFlag") == "A" || GetQueryStringValue("RoleTypeFlag") == null)
        {
            if (Session["UserID"] == null || Session["UserID"].ToString() == "" || Session["Role"] == null || Session["Role"].ToString().Trim() == "")
                return;

            oDiscrepancyBE.User.UserID = Convert.ToInt32(Session["UserID"]);
            CaseOf = Session["Role"].ToString().Trim();
        }
        else
        {
            switch (GetQueryStringValue("RoleTypeFlag").ToString())
            {
                case "G":
                    CaseOf = "OD - Goods In";
                    break;
                case "S":
                    CaseOf = "OD - Stock Planner";
                    break;
                case "P":
                    CaseOf = "OD - Accounts Payable";
                    break;
                case "V":
                    CaseOf = "Vendor";
                    break;
            }
        }


        if (CaseOf == "Vendor")
        {
            //check for vendor in
            pnlLocation.Visible = false;
            oDiscrepancyBE.Action = "CheckForActionRequiredForVendor";
            oDiscrepancyBE.VenActionRequired = true;

            DataTable dtActionDetails = oDiscrepancyBAL.GetActionRequiredDetailsForWorkFlowBAL(oDiscrepancyBE);
            if (dtActionDetails != null && dtActionDetails.Rows.Count > 0)
            {

                string sUserControl = "../UserControl/VENAction/" + dtActionDetails.Rows[0]["VENUserControl"].ToString().Trim()
                                        + ".aspx?disLogID=" + GetQueryStringValue("disLogID").ToString()
                                        + "&DisWFlowID=" + dtActionDetails.Rows[0]["DiscrepancyWorkflowID"].ToString()
                                        + "&PONo=" + dtActionDetails.Rows[0]["PurchaseOrderNumber"].ToString()
                                        + "&SiteID=" + SiteID.ToString()
                                        + "&VDRNo=" + VDRNo + "&FromPage=PaperworkAmended"
                                        + "&PreviousPage=" + GetQueryStringValue("PreviousPage").ToString();
                ifUserControl.Attributes.Add("src", EncryptURL(sUserControl));
            }
            else
            {
                ifUserControl.Style["display"] = "none";
            }
        }
        else if (CaseOf == "OD - Accounts Payable")
        {
            //check for account payable in

            oDiscrepancyBE.Action = "CheckForActionRequired";
            oDiscrepancyBE.DiscrepancyWorkFlowID = !string.IsNullOrEmpty(GetQueryStringValue("WorkFlowID")) ? Convert.ToInt32(GetQueryStringValue("WorkFlowID")) : (int?)null;
            oDiscrepancyBE.APActionRequired = true;

            DataTable dtActionDetails = oDiscrepancyBAL.GetActionRequiredDetailsForWorkFlowBAL(oDiscrepancyBE);
            if (dtActionDetails != null && dtActionDetails.Rows.Count > 0)
            {
                //if (dtActionDetails.Rows[0]["APUserControl"].ToString().Trim() == "ACP001")
                if (Convert.ToString(dtActionDetails.Rows[0]["ACP001Control"]).Trim().Equals("ACP001")
                    && Convert.ToString(dtActionDetails.Rows[0]["IsACP001Done"]).Trim().Equals("0"))
                {
                    if (GetQueryStringValue("disLogID") != null)
                        ucApAction.DisLogID = Convert.ToInt32(GetQueryStringValue("disLogID"));

                    ucApAction.DisWFlowID = Convert.ToInt32(dtActionDetails.Rows[0]["DiscrepancyWorkflowID"]);
                    ucApAction.PONumber = Convert.ToString(dtActionDetails.Rows[0]["PurchaseOrderNumber"]);
                    ucApAction.SiteID = SiteID;
                    ucApAction.VDRNo = VDRNo;
                    ucApAction.FromPage = "overs";
                    ucApAction.PreviousPage = GetQueryStringValue("PreviousPage").ToString();

                    if (ViewState["VendorID"] != null)
                        ucApAction.VendorID = Convert.ToInt32(ViewState["VendorID"]);

                    btnAccountsPayableAction.Visible = true;
                    ifUserControl.Style["display"] = "none";
                }
                else
                {

                    string sUserControl = string.Empty;
                    sUserControl = "../UserControl/APAction/" + dtActionDetails.Rows[0]["APUserControl"].ToString().Trim()
                                           + ".aspx?disLogID=" + GetQueryStringValue("disLogID").ToString()
                                           + "&DisWFlowID=" + dtActionDetails.Rows[0]["DiscrepancyWorkflowID"].ToString()
                                           + "&PONo=" + dtActionDetails.Rows[0]["PurchaseOrderNumber"].ToString()
                                           + "&SiteID=" + SiteID.ToString()
                                           + "&VDRNo=" + VDRNo
                                           + "&FromPage=overs"
                                           + "&PreviousPage=" + GetQueryStringValue("PreviousPage").ToString();

                    ifUserControl.Attributes.Add("src", EncryptURL(sUserControl));
                }
            }
            else
            {

                ifUserControl.Style["display"] = "none";
            }
        }
        else
        {
            if (CaseOf != "OD - Goods In" && CaseOf != "OD - Stock Planner")
            {
                btnAccountsPayableAction.Visible = true;
            }
            ifUserControl.Style["display"] = "none";
        }
        oDiscrepancyBAL = null;
    }

    /*----------------ADDING RAISE QUERY METHODS------------*/

    private bool IsAlreadyDisputeExist(int discrepancyLogID = 0)
    {
        var isExist = false;

        if (discrepancyLogID != 0)
        {
            var queryDiscrepancyBAL = new DISLog_QueryDiscrepancyBAL();
            var queryDiscrepancyBE = new DISLog_QueryDiscrepancyBE();
            if (discrepancyLogID > 0)
            {
                queryDiscrepancyBE.Action = "IsAlreadyDisputeExist";
                queryDiscrepancyBE.DiscrepancyLogID = discrepancyLogID;
                var result = queryDiscrepancyBAL.IsAlreadyDisputeExistBAL(queryDiscrepancyBE);
                if (result > 0)
                    isExist = true;
            }
        }

        return isExist;
    }
    /* CHECK QUERY IS CLOSED THEN VENDOR CAN RAISE QUERY*/
    private bool IsQueryClosed(int discrepancyLogID = 0)
    {
        var isExist = false;

        if (discrepancyLogID != 0)
        {
            var queryDiscrepancyBAL = new DISLog_QueryDiscrepancyBAL();
            var queryDiscrepancyBE = new DISLog_QueryDiscrepancyBE();
            if (discrepancyLogID > 0)
            {
                queryDiscrepancyBE.Action = "IsAlreadyDisputeExist";
                queryDiscrepancyBE.DiscrepancyLogID = discrepancyLogID;
                var result = queryDiscrepancyBAL.GetQueryClose(queryDiscrepancyBE);
                if (result != 0)
                    isExist = true;
            }
        }

        return isExist;
    }
    /* METHODS FOR VENDOR'S QUERY OVERVIEW*/
    private void VendorsQueryOverview()
    {

        var queryDiscrepancyBAL = new DISLog_QueryDiscrepancyBAL();
        var queryDiscrepancyBE = new DISLog_QueryDiscrepancyBE();
        var disLogID = GetQueryStringValue("disLogID") != null ? Convert.ToInt32(GetQueryStringValue("disLogID")) : 0;
        //var queryDisId = GetQueryStringValue("QueryDisID") != null ? Convert.ToInt32(GetQueryStringValue("QueryDisID")) : 0;
        if (disLogID > 0)
        {
            queryDiscrepancyBE.Action = "GetQueryDiscrepancy";
            // queryDiscrepancyBE.QueryDiscrepancyID = queryDisId;
            queryDiscrepancyBE.DiscrepancyLogID = disLogID;
            var getQueryDiscrepancy = queryDiscrepancyBAL.GetQueryDiscrepancyBAL(queryDiscrepancyBE);
            if (getQueryDiscrepancy.Count > 0)
            {
                pnlQueryOverview_1.Visible = true;
                rptQueryOverview.DataSource = getQueryDiscrepancy;
                rptQueryOverview.DataBind();

            }
        }
    }
    protected void rptQueryOverview_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            var hdnIsQueryClosedManually = (HiddenField)e.Item.FindControl("hdnIsQueryClosedManually");

            // GoodsIn declaration ...            
            var lblCommentLabelT_2 = (ucLabel)e.Item.FindControl("lblCommentLabelT_2");
            if (lblCommentLabelT_2 != null)
            {
                if (!string.IsNullOrEmpty(lblCommentLabelT_2.Text) ) { e.Item.FindControl("tblGoodsIn").Visible = true; }
                else { e.Item.FindControl("tblGoodsIn").Visible = false;  }

                if (!string.IsNullOrEmpty(lblCommentLabelT_2.Text) && hdnIsQueryClosedManually.Value == "True")
                {
                    e.Item.FindControl("tblGoodsIn").Visible = false;
                    e.Item.FindControl("tblQueryClosedManually").Visible = true;
                }

                var lnkView = (System.Web.UI.HtmlControls.HtmlAnchor)e.Item.FindControl("lnkView");
                var hdnQueryDiscrepancyID = (HiddenField)e.Item.FindControl("hdnQueryDiscrepancyID");
                var hdnDiscrepancyLogID = (HiddenField)e.Item.FindControl("hdnDiscrepancyLogID");
                lnkView.Attributes.Add("href", EncryptQuery("~/ModuleUI/Appointment/Receiving/APPRcv_Communication.aspx?DiscId=" + hdnDiscrepancyLogID.Value
                    + "&QueryDiscId=" + hdnQueryDiscrepancyID.Value));
            }
        }
    }

    #endregion
    protected void btnUpdateLocation_Click(object sender, EventArgs e)
    {
        var disLogID = GetQueryStringValue("disLogID") != null ? Convert.ToInt32(GetQueryStringValue("disLogID")) : 0;
        if (disLogID != null)
        {
            DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
            DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();
            oDiscrepancyBE.Action = "UpdateLocation";
            oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(disLogID);
            oDiscrepancyBE.Location = txtLocation.Text.Trim();
            oDiscrepancyBE.UserID = Convert.ToInt32(Session["UserID"]);
            string sResult = oDiscrepancyBAL.updateDiscrepancyLocationBAL(oDiscrepancyBE);
            if (GetQueryStringValue("PreviousPage") != null)
            {
                if (GetQueryStringValue("PreviousPage").Trim().ToUpper().Equals("SearchResultVendor".ToUpper()))
                {
                    btnBack_1.Visible = false;
                    btnUpdateLocation.Visible = false;
                    EncryptQueryString("~/ModuleUI/Discrepancy/DIS_SearchResultVendor.aspx?disLogID=" + Convert.ToString(GetQueryStringValue("disLogID"))
                                + "&VDRNo=" + Convert.ToString(GetQueryStringValue("VDRNo"))
                                + "&UserID=" + Convert.ToString(GetQueryStringValue("UserID"))
                                + "&PreviousPage=SearchResultVendor&WorkFlowID=" + Convert.ToString(GetQueryStringValue("WorkFlowID").ToString()));
                }

                if (Session["PageMode"].ToString() == "Todays")
                {
                    EncryptQueryString("../DIS_SearchResult.aspx?FromPage=images&SiteId=" + GetQueryStringValue("SiteId") + "&DisDate=" + GetQueryStringValue("DisDate"));
                }
                else if (Session["PageMode"].ToString() == "Search")
                {
                    //For opening search discrepancies mode
                    btnBack_1.Visible = false;
                    btnUpdateLocation.Visible = false;
                    EncryptQueryString("~/ModuleUI/Discrepancy/DIS_SearchResult.aspx?disLogID=" + Convert.ToString(GetQueryStringValue("disLogID"))
                                + "&VDRNo=" + Convert.ToString(GetQueryStringValue("VDRNo"))
                                + "&UserID=" + Convert.ToString(GetQueryStringValue("UserID"))
                                + "&PreviousPage=SearchResult&WorkFlowID=" + Convert.ToString(GetQueryStringValue("WorkFlowID").ToString()));

                }
            }
            else if (GetQueryStringValue("PN") != null)
            {
                if (GetQueryStringValue("PN").Trim().ToUpper().Equals("POINQ"))
                {
                    btnBack_1.Visible = false;
                    btnUpdateLocation.Visible = false;
                    EncryptQueryString("~/ModuleUI/Appointment/PurchaseOrderInquiry.aspx?SId=" + GetQueryStringValue("SId").ToString()
                        + "&Status=" + GetQueryStringValue("Status").ToString()
                        + "&PN=DISLOG"
                        + "&PO=" + GetQueryStringValue("PO").ToString());
                }
                else if (GetQueryStringValue("PN").Trim().ToUpper().Equals("OPENPL"))
                {
                    EncryptQueryString("~/ModuleUI/StockOverview/Expedite/OpenPurchaseListing.aspx?action=showlisting");
                }


            }
        }
    }


    protected void btnContinue_Click(object sender, EventArgs e)
    {
        string EncryptcUrl = EncryptQuery("~/ModuleUI/Discrepancy/DISLog_QueryDiscrepancy.aspx?disLogID=" + Convert.ToString(GetQueryStringValue("disLogID"))
                                + "&VDRNo=" + Convert.ToString(GetQueryStringValue("VDRNo")) + "&RaiseQueryVendor=Yes" + "&RedirectTo=DisLog_Overs.aspx");
        Response.Redirect(EncryptcUrl, false);
        Context.ApplicationInstance.CompleteRequest();
    }

    protected void btnRaiseQueryPopupBack_Click(object sender, EventArgs e)
    {
        btnBack.Visible = false;
        btnSave.Visible = false;
    }

    private void BindDiscrepancyLogComment(DiscrepancyBE discrepancyBE = null)
    {
        var discrepancyBAL = new DiscrepancyBAL();
        if (discrepancyBE != null)
        {
            rptDiscrepancyComments.DataSource = discrepancyBAL.GetDiscrepancyLogCommentsBAL(discrepancyBE);
        }
        else
        {
            discrepancyBE = new DiscrepancyBE();
            discrepancyBE.Action = "GetDiscrepancyLogComment";

            if (!string.IsNullOrEmpty(GetQueryStringValue("disLogID")))
                discrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID"));
            else
                discrepancyBE.DiscrepancyLogID = 0;

            rptDiscrepancyComments.DataSource = discrepancyBAL.GetDiscrepancyLogCommentsBAL(discrepancyBE);
        }

        rptDiscrepancyComments.DataBind();

        if (rptDiscrepancyComments.Items.Count > 0)
            lblComments.Visible = true;
        else
            lblComments.Visible = false;
        
    }
}
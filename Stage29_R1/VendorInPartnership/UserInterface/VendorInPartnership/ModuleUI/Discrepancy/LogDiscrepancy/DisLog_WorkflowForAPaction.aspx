﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    AutoEventWireup="true" CodeFile="DisLog_WorkflowForAPaction.aspx.cs" Inherits="ModuleUI_Discrepancy_LogDiscrepancy_DisLog_WorkflowForAPaction" %>

<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%	  
        Response.WriteFile("DiscrepancyImages.txt");
    %>
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePageMethods="true">
    </asp:ScriptManager>
    <h2>
        Work Flow
        <asp:Label runat="server" ID="lblVDRNo" /></h2>
    <style type="text/css">
        .table-border-color
        {
            border: solid 1px red;
            margin-bottom: 7px;
        }
        
        .table-border-color td table
        {
            border: solid 1px red;
        }
        .table-border-color td
        {
            padding: 0px 2px;
        }
        .table-border-color td table td
        {
            padding: 0px !important;
        }
    </style>
    <script type="text/javascript">


        $(document).ready(function () {
            $('td>table').each(function () {
                var $class = $(this).attr('class');
                if ($class != null) {
                    $(this).parent('td').addClass($class);
                }
            });
        });

        $(document).ready(function () {
            var $DiscrepancyStatus = $('#hdDiscrepancyStatus').val();
            var $UserData = $('#hdUserData').val();
            var $DelDisData = $('#hdnDelDisData').val();
            var $ForceClosedString = '<tr><td colspan="4" style="background-color:red;height:2em;text-align:center;font-weight:bold;">' + $UserData + '</td></tr>';
            var $DeletedDisString = '<tr><td colspan="4" style="background-color:#00ffff;height:2em;">' + $DelDisData + '</td></tr>';
            if ($DiscrepancyStatus == "C") {
                $('#tblWorkflow>tbody').append('<tr><td colspan="4" style="background-color:green;height:2em;text-align:center;font-weight:bold;">Discrepancy Closed</td></tr>');
            }
            if ($DiscrepancyStatus == "D") {
                $('#tblWorkflow>tbody').append($DeletedDisString);
            }
            else if ($DiscrepancyStatus == "F") {
                $('#tblWorkflow>tbody').append($ForceClosedString);
            }
        });
    </script>
    <asp:UpdatePanel ID="updpnlMain" runat="server">
        <ContentTemplate>
            <div class="button-row">
                <span>
                    <cc1:ucButton ID="btnAddComment" Text="Add Comment" runat="server" class="button"
                        OnClick="btnAddComment_Click" />
                    <cc1:ucButton ID="btnAddImages" Text="Add Images" runat="server" class="button" OnClick="btnAddImages_Click" />
                    <a href="#" rel="shadowbox[gal]" target="_blank" style="padding-left: 0px;">
                        <input type="button" id="btnViewImage" value="View Images" runat="server" title="View Images"
                            class="button" clientidmode="Static" /></a>
                &nbsp;&nbsp; &nbsp;
                    <asp:TextBox ID="txtNoofLabel" runat="server" Width="2em" onkeyup="AllowNumbersOnly(this);"
                        Style="vertical-align: bottom" />
                    <cc1:ucButton ID="btnGenerateLabels" Text="Generate Labels" runat="server" class="button"
                        OnClick="btnGenerateLabels_Click" />
                </span>
            </div>
            <div id="discrepancygallery" style="display: none" runat="server" clientidmode="Static">
            </div>
            <asp:Repeater ID="rpWorkflow" runat="server">
                <HeaderTemplate>
                    <table border="0" id="tblWorkflow" cellpadding="0" cellspacing="0" width="1000px"
                        class="workflow-grid">
                        <thead>
                            <tr style="height: 30px">
                                <th style="width: 250px;">
                                    Goods In
                                </th>
                                <th style="width: 250px;">
                                    Inventory
                                </th>
                                <th style="width: 250px;">
                                    Accounts Payable
                                </th>
                                <th style="width: 250px;">
                                    Vendor
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td valign="top" style="width: 250px;">
                            <%#DataBinder.Eval(Container.DataItem, "GINHTML")%>
                        </td>
                        <td valign="top" style="width: 250px;">
                            <%#DataBinder.Eval(Container.DataItem, "INVHTML")%>
                        </td>
                        <td valign="top" style="width: 250px;">
                            <%#DataBinder.Eval(Container.DataItem, "APHTML")%>
                        </td>
                        <td valign="top" style="width: 250px;">
                            <%#DataBinder.Eval(Container.DataItem, "VENHTML")%>
                        </td>
                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                    </tbody> </table>
                </FooterTemplate>
            </asp:Repeater>
            <div class="button-row">
                <span style="height: 15px;">
                    <cc1:ucButton ID="btnBacktoPrevious" runat="server" Text="Back" CssClass="button"
                        OnCommand="btnBackToPreviousPage_Click" />
                </span>
            </div>
            
            <br />
            <cc1:ucLabel ID="lblComments" Text="Comments" Font-Bold="true" Visible="false" runat="server"></cc1:ucLabel>
            <div style="border-bottom: 1px solid black;">
                &nbsp;</div>
            <asp:Repeater ID="rptDiscrepancyComments" runat="server">
                <ItemTemplate>
                    <table cellspacing="5" cellpadding="0" align="center" style="border: 1px solid black;
                        border-top: none; width: 100%">
                        <tr>
                            <td style="font-weight: bold; width: 10%;">
                                <cc1:ucLabel ID="lblDate_1" Text="Date" runat="server"></cc1:ucLabel>/<cc1:ucLabel
                                    ID="lblActualTime" Text="Time" runat="server"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold; width: 1%;">
                                :
                            </td>
                            <td style="width: 22%;">
                                <cc1:ucLabel ID="lblDateT_1" Text='<%#Eval("CommentDateTime", "{0:dd/MM/yyyy}") + "  " + Eval("CommentDateTime",@"{0:HH:mm}")%>'
                                    runat="server"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold; width: 10%;">
                                <cc1:ucLabel ID="lblUser_1" Text="User" runat="server"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold; width: 1%;">
                                :
                            </td>
                            <td style="width: 56%;">
                                <cc1:ucLabel ID="lblUserT_1" Text='<%#DataBinder.Eval(Container.DataItem, "Username")%>'
                                    runat="server"></cc1:ucLabel>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold; width: 100%;" colspan="6">
                                <cc1:ucLabel ID="lblCommentLabel_1" Text="Comment" runat="server"></cc1:ucLabel>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 100%;" colspan="6">
                                <cc1:ucLabel Style="width: 970px; word-wrap: break-word; display: block;" ID="lblCommentLabelT"
                                    Text='<%#DataBinder.Eval(Container.DataItem, "Comment")%>' runat="server"></cc1:ucLabel>
                            </td>
                        </tr>
                    </table>
                </ItemTemplate>
            </asp:Repeater>
            <div id="innerData" style="display: none;">
                <asp:Label ID="ltlable" runat="server" />
            </div>
            <asp:HiddenField ID="hdDiscrepancyStatus" runat="server" ClientIDMode="Static" />
            <asp:HiddenField ID="hdUserData" runat="server" ClientIDMode="Static" />
            <asp:HiddenField ID="hdnDelDisData" runat="server" ClientIDMode="Static" />
            <br />
            <cc1:ucLabel ID="lblDiscrepancyQuery" Text="Discrepancy Query" Visible="false" Font-Bold="true"
                runat="server"></cc1:ucLabel>
            <asp:Repeater ID="rptDiscrepancyQuery" runat="server" OnItemDataBound="rptDiscrepancyQuery_ItemDataBound">
                <ItemTemplate>
                    <table cellspacing="0" cellpadding="0" align="center" class="table-border-color">
                        <tr>
                            <td style="padding-top: 2px;">
                                <table cellspacing="5" cellpadding="0" align="center" style="width: 100%; border-bottom: none;">
                                    <tr>
                                        <td colspan="6">
                                            <asp:HiddenField ID="hdnQueryDiscrepancyID" Value='<%#DataBinder.Eval(Container.DataItem, "QueryDiscrepancyID")%>'
                                                runat="server" />
                                            <asp:HiddenField ID="hdnDiscrepancyLogID" Value='<%#DataBinder.Eval(Container.DataItem, "DiscrepancyLogID")%>'
                                                runat="server" />
                                            <cc1:ucLabel ID="lblQueryRaisedByVendor" Text="QUERY RAISED BY VENDOR" isRequired="true"
                                                Font-Bold="true" runat="server"></cc1:ucLabel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: bold; width: 10%;">
                                            <cc1:ucLabel ID="lblDate_1" Text="Date" runat="server" isRequired="true"></cc1:ucLabel><b>/</b><cc1:ucLabel
                                                ID="lblActualTime_1" Text="Time" isRequired="true" runat="server"></cc1:ucLabel>
                                        </td>
                                        <td style="font-weight: bold; width: 1%;">
                                            <cc1:ucLabel ID="lblCollon1" Text=":" runat="server" isRequired="true" Font-Bold="true"></cc1:ucLabel>
                                        </td>
                                        <td style="width: 22%;">
                                            <cc1:ucLabel ID="lblDateT_1" Text='<%#Eval("VendorQueryDate", "{0:dd/MM/yyyy}") + "  " + Eval("VendorQueryDate",@"{0:HH:mm}")%>'
                                                runat="server" ForeColor="#800000"></cc1:ucLabel>
                                        </td>
                                        <td style="font-weight: bold; width: 10%;">
                                            <cc1:ucLabel ID="lblUser_1" Text="User" isRequired="true" runat="server"></cc1:ucLabel>
                                        </td>
                                        <td style="font-weight: bold; width: 1%;">
                                            <cc1:ucLabel ID="lblCollon2" Text=":" runat="server" isRequired="true" Font-Bold="true"></cc1:ucLabel>
                                        </td>
                                        <td style="width: 56%;">
                                            <cc1:ucLabel ID="lblUserT_1" Text='<%#DataBinder.Eval(Container.DataItem, "VendorUserName")%>'
                                                runat="server" ForeColor="#800000"></cc1:ucLabel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: bold; width: 100%;" colspan="6">
                                            <cc1:ucLabel ID="lblCommentLabel_1" Text="Comment" isRequired="true" runat="server"></cc1:ucLabel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 100%;" colspan="6">
                                            <cc1:ucLabel Style="width: 970px; word-wrap: break-word; display: block;" ID="lblCommentLabelT_1"
                                                Text='<%#DataBinder.Eval(Container.DataItem, "VendorComment")%>' runat="server"
                                                ForeColor="#800000"></cc1:ucLabel>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding-bottom: 2px;">
                                <table cellspacing="5" cellpadding="0" align="center" style="width: 100%" id="tblGoodsIn"
                                    runat="server">
                                    <tr>
                                        <td colspan="9">
                                            <cc1:ucLabel ID="lblGoodsInFeedback" Text="GOODS IN FEEDBACK" isRequired="true" Font-Bold="true"
                                                runat="server"></cc1:ucLabel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: bold; width: 10%;">
                                            <cc1:ucLabel ID="lblDate_2" Text="Date" runat="server" isRequired="true"></cc1:ucLabel><b>/</b><cc1:ucLabel
                                                ID="lblActualTime_2" Text="Time" isRequired="true" runat="server"></cc1:ucLabel>
                                        </td>
                                        <td style="font-weight: bold; width: 1%;">
                                            <cc1:ucLabel ID="lblCollon3" Text=":" runat="server" isRequired="true" Font-Bold="true"></cc1:ucLabel>
                                        </td>
                                        <td style="width: 22%;">
                                            <cc1:ucLabel ID="lblDateT_2" Text='<%#Eval("GoodsInQueryDate", "{0:dd/MM/yyyy}") + "  " + Eval("GoodsInQueryDate",@"{0:HH:mm}")%>'
                                                runat="server" ForeColor="#800000"></cc1:ucLabel>
                                        </td>
                                        <td style="font-weight: bold; width: 10%;">
                                            <cc1:ucLabel ID="lblUser_2" Text="User" isRequired="true" runat="server"></cc1:ucLabel>
                                        </td>
                                        <td style="font-weight: bold; width: 1%;">
                                            <cc1:ucLabel ID="lblCollon4" Text=":" runat="server" isRequired="true" Font-Bold="true"></cc1:ucLabel>
                                        </td>
                                        <td style="width: 22%;">
                                            <cc1:ucLabel ID="lblUserT_2" Text='<%#DataBinder.Eval(Container.DataItem, "GoodsInUserName")%>'
                                                runat="server" ForeColor="#800000"></cc1:ucLabel>
                                        </td>
                                        <td style="font-weight: bold; width: 10%;">
                                            <cc1:ucLabel ID="lblCommunitcation" Text="Communitcation" isRequired="true" runat="server"></cc1:ucLabel>
                                        </td>
                                        <td style="font-weight: bold; width: 1%;">
                                            <cc1:ucLabel ID="lblCollon6" Text=":" runat="server" isRequired="true" Font-Bold="true"></cc1:ucLabel>
                                        </td>
                                        <td style="width: 23%">
                                            <a id="lnkView" runat="server" target='_blank' onclick='window.open(this.href, this.target,"scrollbars=1,left=100px,top=20px,width=1000px,height=620px,toolbar=0,scrollbars=0,status=0"); return false;'>
                                                View</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: bold;">
                                            <cc1:ucLabel ID="lblAction" Text="Action" runat="server" isRequired="true"></cc1:ucLabel>
                                        </td>
                                        <td style="font-weight: bold;">
                                            <cc1:ucLabel ID="lblCollon5" Text=":" runat="server" isRequired="true" Font-Bold="true"></cc1:ucLabel>
                                        </td>
                                        <td colspan="7">
                                            <cc1:ucLabel ID="lblActionT" Text='<%#DataBinder.Eval(Container.DataItem, "QueryAction")%>'
                                                runat="server" ForeColor="#800000"></cc1:ucLabel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: bold;" colspan="9">
                                            <cc1:ucLabel ID="lblCommentLabel_2" Text="Comment" isRequired="true" runat="server"></cc1:ucLabel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="9">
                                            <cc1:ucLabel Style="width: 970px; word-wrap: break-word; display: block;" ID="lblCommentLabelT_2"
                                                Text='<%#DataBinder.Eval(Container.DataItem, "GoodsInComment")%>' runat="server"
                                                ForeColor="#800000"></cc1:ucLabel>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </ItemTemplate>
            </asp:Repeater>
            <br />
            <cc1:ucLabel ID="lblDebitCreditPosting" Text="Debit Credit Posting" Visible="true"
                Font-Bold="true" runat="server"></cc1:ucLabel>
            <asp:Panel ID="pnlDebitCreditPosting" runat="server" Visible="false">
                <table cellspacing="0" cellpadding="0" width="100%">
                    <tr>
                        <td style="padding-top: 2px;">
                            <table cellspacing="5" cellpadding="0" align="center" style="border: 1px solid black;
                                width: 100%; background-color: #ffff99">
                                <tr>
                                    <td style="font-weight: bold; width: 10%;">
                                        <cc1:ucLabel ID="lblDate_1" Text="Date" runat="server"></cc1:ucLabel><b>/</b><cc1:ucLabel
                                            ID="lblActualTime_1" Text="Time" runat="server"></cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold; width: 1%;">
                                        :
                                    </td>
                                    <td style="width: 22%;">
                                        <cc1:ucLabel ID="lblDateTimeValue" Text="" runat="server"></cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold; width: 15%;">
                                        <cc1:ucLabel ID="lblCreatedBy" runat="server"></cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold; width: 1%;">
                                        :
                                    </td>
                                    <td style="width: 51%;">
                                        <cc1:ucLabel ID="lblCreatedByValue" Text="" runat="server"></cc1:ucLabel>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="font-weight: bold;">
                                        <cc1:ucLabel ID="lblActionTaken" runat="server"></cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold;">
                                        :
                                    </td>
                                    <td>
                                        <cc1:ucLabel ID="lblActionTakenValue" runat="server" Text=""></cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold;">
                                        <cc1:ucLabel ID="lblDebitNoteNumber" runat="server"></cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold;">
                                        :
                                    </td>
                                    <td>
                                        <cc1:ucLabel ID="lblDNNValue" runat="server" Text=""></cc1:ucLabel>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="font-weight: bold;">
                                        <cc1:ucLabel ID="lblReason" runat="server"></cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold; width: 1%;">
                                        :
                                    </td>
                                    <td>
                                        <cc1:ucLabel ID="lblReasonValue" runat="server" Text=""></cc1:ucLabel>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="font-weight: bold;">
                                        <cc1:ucLabel ID="lblValuePosted" runat="server"></cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold; width: 1%;">
                                        :
                                    </td>
                                    <td>
                                        <cc1:ucLabel ID="lblValuePostedValue" runat="server" Text=""></cc1:ucLabel>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="font-weight: bold;">
                                        <cc1:ucLabel ID="lblcurrency" runat="server"></cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold; width: 1%;">
                                        :
                                    </td>
                                    <td>
                                        <cc1:ucLabel ID="lblCurrencyValue" runat="server" Text=""></cc1:ucLabel>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="font-weight: bold;">
                                        <cc1:ucLabel ID="lblSentTo" runat="server"></cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold; width: 1%;">
                                        :
                                    </td>
                                    <td>
                                        <cc1:ucLabel ID="lblSentToValue" runat="server"></cc1:ucLabel>
                                        <%--<a id="A1" runat="server" target='_blank' href="~/ModuleUI/Discrepancy/AccountsPayable/Communication.aspx?CommunicationType=DebitCreate"
                                        onclick='window.open(this.href, this.target,"scrollbars=1,left=100px,top=20px,width=1000px,height=620px,toolbar=0,scrollbars=0,status=0"); return false;'>
                                        accotest@btinternet.com</a>--%>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <table cellspacing="0" cellpadding="0" width="100%">
                    <tr>
                        <td style="padding-top: 2px;">
                            <table cellspacing="5" cellpadding="0" align="center" style="border: 1px solid black;
                                width: 100%; background-color: #ffe699">
                                <tr>
                                    <td style="font-weight: bold; width: 14%;">
                                        <cc1:ucLabel ID="lblDebitNoteReference" runat="server"></cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold; width: 1%;">
                                        :
                                    </td>
                                    <td style="width: 85%;">
                                        <cc1:ucLabel ID="lblDebitNoteReferenceValue" Text="" runat="server"></cc1:ucLabel>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="font-weight: bold;">
                                        <cc1:ucLabel ID="lblResentBy" runat="server"></cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold;">
                                        :
                                    </td>
                                    <td>
                                        <cc1:ucLabel ID="lblResentByValue" runat="server" Text=""></cc1:ucLabel>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="font-weight: bold;">
                                        <cc1:ucLabel ID="lblResentOn" runat="server"></cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold; width: 1%;">
                                        :
                                    </td>
                                    <td>
                                        <cc1:ucLabel ID="lblResentOnValue" runat="server" Text=""></cc1:ucLabel>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="font-weight: bold;">
                                        <cc1:ucLabel ID="lblResentTo" runat="server"></cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold; width: 1%;">
                                        :
                                    </td>
                                    <td>
                                        <cc1:ucLabel ID="lblResentToValue" runat="server"></cc1:ucLabel>
                                        <%--<asp:HyperLink ID="HyperLink1" runat="server" Text="accotest@btinternet.com"
                                      NavigateUrl="~/ModuleUI/Discrepancy/AccountsPayable/Communication.aspx?CommunicationType=DebitCreate" Target='_blank' onclick='window.open(this.href, this.target,"scrollbars=1,left=100px,top=20px,width=1000px,height=620px,toolbar=0,scrollbars=0,status=0"); return false;'></asp:HyperLink>--%>
                                        <%--<asp:HyperLink ID="hypLinkToDisDetails" runat="server" Text=" mrvendor@gmail.com"
                                        NavigateUrl='<%# EncryptQuery("~/ModuleUI/Discrepancy/AccountsPayable/Communication.aspx?CommunicationType=DebitCancel" ) %>'
                                        Target='_blank' onclick='window.open(this.href, this.target,"scrollbars=1,left=100px,top=20px,width=1000px,height=620px,toolbar=0,scrollbars=0,status=0"); return false;'>
                                    </asp:HyperLink>--%>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <table cellspacing="0" cellpadding="0" width="100%">
                    <tr>
                        <td style="padding-top: 2px;">
                            <table cellspacing="5" cellpadding="0" align="center" style="border: 1px solid black;
                                width: 100%; background-color: #b4c7e7">
                                <tr>
                                    <td style="font-weight: bold; width: 14%;">
                                        <cc1:ucLabel ID="lblDebitNoteReference_1" runat="server"></cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold; width: 1%;">
                                        :
                                    </td>
                                    <td style="width: 85%;">
                                        <cc1:ucLabel ID="lblCancelledDebitNoteReferenceValue" Text="" runat="server"></cc1:ucLabel>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="font-weight: bold;">
                                        <cc1:ucLabel ID="lblCancelledBy" runat="server"></cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold; width: 1%;">
                                        :
                                    </td>
                                    <td>
                                        <cc1:ucLabel ID="lblCancelledByValue" runat="server" Text=""></cc1:ucLabel>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="font-weight: bold;">
                                        <cc1:ucLabel ID="lblCancelledOn" runat="server"></cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold; width: 1%;">
                                        :
                                    </td>
                                    <td>
                                        <cc1:ucLabel ID="lblCancelledOnValue" runat="server" Text=""></cc1:ucLabel>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="font-weight: bold;">
                                        <cc1:ucLabel ID="lblSentTo_1" runat="server"></cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold; width: 1%;">
                                        :
                                    </td>
                                    <td>
                                        <cc1:ucLabel ID="lblCancelledSentToValue" runat="server"></cc1:ucLabel>
                                        <%--<asp:HyperLink ID="HyperLink2" runat="server" Text="accotest@btinternet.com"
                                      NavigateUrl="~/ModuleUI/Discrepancy/AccountsPayable/Communication.aspx?CommunicationType=DebitCancel" Target='_blank' 
                                      onclick='window.open(this.href, this.target,"scrollbars=1,left=100px,top=20px,width=1000px,height=620px,toolbar=0,scrollbars=0,status=0"); return false;'></asp:HyperLink>--%>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnGenerateLabels" />
            <asp:AsyncPostBackTrigger ControlID="btnAddComment" />
        </Triggers>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="updpnlAddComment" runat="server">
        <ContentTemplate>
            <asp:Button ID="btnAddCommentMPE" runat="Server" Style="display: none" />
            <ajaxToolkit:ModalPopupExtender ID="mdlAddComment" runat="server" TargetControlID="btnAddCommentMPE"
                PopupControlID="pnlAddComment" BackgroundCssClass="modalBackground" BehaviorID="AddComment"
                DropShadow="false" />
            <asp:Panel ID="pnlAddComment" runat="server" Style="display: none; width: 630px">
                <div style="overflow-y: hidden; overflow-x: hidden; background-color: #fff; padding: 5px;
                    border: 2px solid #ccc; text-align: left;">
                    <table cellspacing="5" cellpadding="0" border="0" align="center" class="top-settingsNoBorder-popup">
                        <tr>
                            <td style="font-weight: bold; width: 15%;">
                                <cc1:ucLabel ID="lblUser" Text="User" runat="server"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold; width: 1%;">
                                :
                            </td>
                            <td style="width: 30%;">
                                <cc1:ucLabel ID="lblUserT" runat="server"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold; width: 10%;">
                                <cc1:ucLabel ID="lblDate" Text="Date" runat="server"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold; width: 1%;">
                                :
                            </td>
                            <td style="width: 30%;">
                                <cc1:ucLabel ID="lblDateT" runat="server"></cc1:ucLabel>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold; width: 15%;">
                                <cc1:ucLabel ID="lblDiscrepancyNo" Text="Discrepancy #" runat="server"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold; width: 1%;">
                                :
                            </td>
                            <td style="width: 80%;" colspan="4">
                                <cc1:ucLabel ID="lblDiscrepancyNoT" runat="server"></cc1:ucLabel>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold; width: 100%;" colspan="6">
                                <cc1:ucLabel ID="lblCommentLabel" Text="Comment" runat="server"></cc1:ucLabel>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold; width: 100%;" colspan="6">
                                <cc1:ucTextarea ID="txtUserComments" Height="150px" Width="600px" runat="server"></cc1:ucTextarea>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" style="width: 80%;" colspan="4">
                                <cc1:ucLabel ID="lblErrorMsg" Font-Bold="true" ForeColor="Red" runat="server"></cc1:ucLabel>
                            </td>
                            <td align="right" style="width: 20%;" colspan="2">
                                <cc1:ucButton ID="btnSave" runat="server" Text="Save" CssClass="button" OnCommand="btnSave_Click" />
                                &nbsp;
                                <cc1:ucButton ID="btnBack" runat="server" Text="Back" CssClass="button" OnCommand="btnBack_Click" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnSave" />
            <asp:AsyncPostBackTrigger ControlID="btnBack" />
            <asp:AsyncPostBackTrigger ControlID="btnAddComment" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>

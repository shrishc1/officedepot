﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="DISLog_PrematureInvoiceReceipt.aspx.cs"
    MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master" Inherits="DISLog_PrematureInvoiceReceipt" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register TagName="ucDate" TagPrefix="cc2" Src="~/CommonUI/UserControls/ucDate.ascx" %>
<%@ Register Src="../UserControl/ucSDRCommunication.ascx" TagName="ucSDRCommunication"
    TagPrefix="uc1" %>
<%@ Register Src="../../../CommonUI/UserControls/ucSite.ascx" TagName="ucSite" TagPrefix="cc2" %>
<%@ Register Src="../../../CommonUI/UserControls/ucSeacrhVendorForDisc.ascx" TagName="ucSeacrhVendor"
    TagPrefix="cc3" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<%@ Register Src="../UserControl/APAction/APAction.ascx" TagName="APAction" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
     <%	  
        Response.WriteFile("DiscrepancyImages.txt");
    %>
     <script src="../../../Scripts/iFrameHeight.js" type="text/javascript"></script>
    <script type="text/javascript">

        $(document).ready(function () {
            $("body").keypress(function (e) {
                if (e.which == 13 || e.keyCode == 13) {
                    return false;
                }
            });
        });

        $(document).ready(function () {
            if (document.getElementById('<%=pnlPotentialPurchaseDetails.ClientID %>') != null) {
                var objvVNo = document.getElementById('<%=ucSeacrhVendor1.FindControl("txtVendorNo").ClientID%>');
                var objv1 = document.getElementById('<%=rfvPurchaseOrderNumberValidation.ClientID %>');
                var objv2 = document.getElementById('<%=cusvNoSelectedVendor.ClientID %>');
                if (objvVNo != null && objvVNo.value == "") {

                    var objPO = document.getElementById('<%=txtPurchaseOrder.ClientID %>');
                    var objvPOD = document.getElementById('<%=ddlPurchaseOrderDate.ClientID %>');
                    var objvVName = document.getElementById('<%=lblVendorValue.ClientID %>');
                    var objvVNo = document.getElementById('<%=ucSeacrhVendor1.FindControl("txtVendorNo").ClientID%>');
                    var elem = document.getElementById('imgVendor');

                    if (!IsUrlContainDiscrepancyLogID()) {
                        ValidatorEnable(objv1, true);
                        ValidatorEnable(objv2, false);

                        objPO.disabled = false;
                        objPO.value = '';
                        objvPOD.disabled = false;
                        objvVName.innerText = '';
                        objvVNo.disabled = true;
                        elem.disabled = true;
                    }
                }
                if (objvVNo != null && objvVNo.value != "") {
                    ValidatorEnable(objv1, false);
                    ValidatorEnable(objv2, true);
                }
            }


            <%--$("#<%=ifUserControl.ClientID %>").load(function () {
                var iFrame = parent.document.getElementById("<%=ifUserControl.ClientID %>");
                 newHeight = parseInt(iFrame.offsetHeight) + 20;
                $("#" + iFrame.id).height(newHeight);
            });--%>

        });

        function DisablePageValidators(checkSelection) {
            if (document.getElementById('<%=pnlPotentialPurchaseDetails.ClientID %>') != null) {
                var objv1 = document.getElementById('<%=rfvPurchaseOrderNumberValidation.ClientID %>');
                var objv2 = document.getElementById('<%=cusvNoSelectedVendor.ClientID %>');

                var objPO = document.getElementById('<%=txtPurchaseOrder.ClientID %>');
                var objvPOD = document.getElementById('<%=ddlPurchaseOrderDate.ClientID %>');
                var objvVName = document.getElementById('<%=lblVendorValue.ClientID %>');
                var objvVNo = document.getElementById('<%=ucSeacrhVendor1.FindControl("txtVendorNo").ClientID%>');
                var objSelectedVendorNo = document.getElementById('<%=ucSeacrhVendor1.FindControl("SelectedVendorName").ClientID%>');
                var elem = document.getElementById('imgVendor');
                var len = objvPOD.options.length;

                var objSelectOption = document.getElementById('<%=hdnSelectOption.ClientID %>');

                if (checkSelection == '1') {
                    ValidatorEnable(objv1, true);
                    ValidatorEnable(objv2, false);

                    objPO.disabled = false;
                    objPO.value = '';
                    objvPOD.disabled = false;
                    objvVName.innerText = '';
                    objvVNo.value = '';
                    objvVNo.disabled = true;
                    elem.disabled = true;
                    for (i = 0; i < len; i++) {
                        objvPOD.remove(0);
                    }
                    var opt = document.createElement("OPTION");
                    opt.text = '--Select--';
                    opt.value = '0';
                    objvPOD.options.add(opt);
                    objSelectedVendorNo.innerText = '';
                    objSelectOption.value = '1';
                }
                if (checkSelection == '2') {
                    ValidatorEnable(objv1, false);
                    ValidatorEnable(objv2, true);

                    objPO.disabled = true;
                    objvPOD.disabled = false;
                    objPO.value = '';

                    for (j = 0; j < len; j++) {
                        objvPOD.remove(0);
                    }
                    var opt = document.createElement("OPTION");
                    opt.text = '--Select--';
                    opt.value = '0';
                    objvPOD.options.add(opt);
                    objvPOD.disabled = true;

                    objvVName.innerText = '';

                    objvVNo.value = '';
                    objvVNo.disabled = false;

                    elem.disabled = false;

                    objSelectedVendorNo.innerText = '';
                    objSelectOption.value = '2';

                }
            }
            setReadonlyClass();
        }

        function checkVendorSelected(source, args) {
            if (document.getElementById('<%=ucSeacrhVendor1.FindControl("txtVendorNo").ClientID%>') != null) {
                var obj = document.getElementById('<%=ucSeacrhVendor1.FindControl("txtVendorNo").ClientID%>');
                if (obj.value == '') {
                    args.IsValid = false;
                }
            }
        }
        function checkBlankEmailAddress() {

            if (Page_ClientValidate("a")) {
                var rbEmailComm = document.getElementById('<%=ucSDRCommunication1.FindControl("rdoEmailComm").ClientID %>');
                var objTxtAltEmail = document.getElementById('<%=ucSDRCommunication1.FindControl("txtAltEmail").ClientID %>');
                var objucTextBox1 = document.getElementById('<%=ucSDRCommunication1.FindControl("ucVendorEmailList").ClientID %>');
                if (checkEmailAddress(rbEmailComm, objTxtAltEmail, objucTextBox1, '<%=ValidEmail %>', '<%=NoEmailSpecifiedMessage%>') == false) {
                    return false;
                }


            }

        }

        function IsAmountRequired(arg) {
            if (arg == '1') {
                document.getElementById('<%=txtLabourCost.ClientID %>').disabled = true;
                document.getElementById('<%=txtLabourCost.ClientID %>').value = '';
                ValidatorEnable(document.getElementById('<%=rfvLabourCostRequired.ClientID %>'), false);
                ValidatorEnable(document.getElementById('<%=cmpvLabourCostGreaterThanZero.ClientID %>'), false);
            }
            else if (arg == '2') {
                document.getElementById('<%=txtLabourCost.ClientID %>').disabled = false;
                ValidatorEnable(document.getElementById('<%=rfvLabourCostRequired.ClientID %>'), true);
                ValidatorEnable(document.getElementById('<%=cmpvLabourCostGreaterThanZero.ClientID %>'), true);
            }
            setReadonlyClass();
        }
        function disableButton(sender, group) {
            Page_ClientValidate(group);
            if (Page_IsValid) {
                sender.disabled = "disabled";
                sender.value = 'Saving...'
                __doPostBack(sender.name, '');
            }
        } 
    </script>
    <asp:HiddenField ID="hdnSiteID" runat="server" />
    <asp:ScriptManager ID="sp1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="up1" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <h2>
                <cc1:ucLabel ID="lblPrematureInvoiceReceipt" runat="server" Text="Premature Invoice Receipt"></cc1:ucLabel>
                <asp:HiddenField ID="hdnSelectOption" runat="server" Value="" />
            </h2>
            <div class="button-row">
               <cc1:ucButton ID="btnAccountsPayableAction" runat="server" class="button" OnClick="btnAccountsPayableAction_Click"
                    Visible="false" />
              <cc1:ucButton ID="btnRaiseQuery" runat="server" class="button" 
                    onclick="btnRaiseQuery_Click" />
                <cc1:ucButton ID="btnAddComment" Text="Add Comment" runat="server" class="button" OnClick="btnAddComment_Click" />
                 <cc1:ucButton ID="btnAddImages" Text="Add Images" runat="server" class="button" OnClick="btnAddImages_Click" />
                <a href="#" rel="shadowbox[gal]" target="_blank" style="padding-left: 0px;">
                    <input type="button" id="btnViewImage" value="View Images" runat="server" title="View Images"
                        class="button" clientidmode="Static" /></a>
                <cc1:ucButton ID="btnViewWF" runat="server" Text="Workflow" class="button" OnClick="btnViewWF_Click" />
            </div>
            <div id="discrepancygallery" style="display: none" runat="server" clientidmode="Static">
            </div>
            <div class="right-shadow">
                <div class="formbox">
                    <cc1:ucMultiView ID="mvPrematureInvoiceReceipt" runat="server" ActiveViewIndex="0">
                        <cc1:ucView ID="vwPrematureInvoiceReceipt" runat="server">
                            <table width="45%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                                <tr>
                                    <td style="width: 5%">
                                        &nbsp;
                                    </td>
                                    <td style="font-weight: bold; width: 35%">
                                        <cc1:ucLabel ID="lblSitePrefixName" runat="server" Text="Site"></cc1:ucLabel>
                                    </td>
                                    <td style="width: 5%">
                                        :
                                    </td>
                                    <td style="width: 50%">
                                        <cc2:ucSite ID="ucSite" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td style="font-weight: bold;">
                                        <cc1:ucLabel ID="lblIssueRaisedDate1" runat="server" Text="Issue Raised Date"></cc1:ucLabel>
                                    </td>
                                    <td>
                                        :
                                    </td>
                                    <td>
                                        <cc1:ucLabel ID="lblIssueRaisedDate" runat="server"></cc1:ucLabel>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td style="font-weight: bold;">
                                        <cc1:ucLabel ID="lblInvoiceNumber" isRequired="true" runat="server" Text="Invoice Number"></cc1:ucLabel>
                                    </td>
                                    <td>
                                        :
                                    </td>
                                    <td>
                                        <cc1:ucTextbox ID="txtDeliveryNumber" runat="server" Width="120px" MaxLength="15"></cc1:ucTextbox>
                                        <asp:RequiredFieldValidator ID="rfvInvoiceNumberRequired" runat="server" ControlToValidate="txtDeliveryNumber"
                                            Display="None" ValidationGroup="LogDiscrepancy">
                                        </asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td style="font-weight: bold;">
                                        <cc1:ucLabel ID="lblDateDeliveryArrived" runat="server" Text="Date Delivery Arrived"></cc1:ucLabel>
                                    </td>
                                    <td>
                                        :
                                    </td>
                                    <td>
                                        <cc2:ucDate ID="txtDateDeliveryArrived" runat="server" AutoPostBack="false" />
                                        <asp:RequiredFieldValidator ID="rfvDateRequired" runat="server" Display="None" ControlToValidate="txtDateDeliveryArrived$txtUCDate"
                                            ValidateEmptyText="true" ValidationGroup="LogDiscrepancy" SetFocusOnError="true" />
                                    </td>
                                </tr>
                                <tr style="display:none;">
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td style="font-weight: bold;">
                                        <cc1:ucLabel ID="lblCarrier" runat="server" Text="Carrier"></cc1:ucLabel>
                                    </td>
                                    <td>
                                        :
                                    </td>
                                    <td>
                                        <cc1:ucDropdownList ID="ddlCarrier" runat="server" Width="150px">
                                        </cc1:ucDropdownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        <asp:ValidationSummary ID="vSummary" runat="server" ShowMessageBox="true" ShowSummary="false"
                                            Style="color: Red" ValidationGroup="LogDiscrepancy" />
                                    </td>
                                </tr>
                            </table>
                            <table width="100%" cellspacing="1" cellpadding="0" class="form-table">
                                <tr>
                                    <td>
                                        <cc1:ucLabel ID="lblEitherPOorVendor" runat="server" Text="Please enter either the Potential Purchase Order Details or Vendor Name"
                                            isRequired="False"></cc1:ucLabel>
                                    </td>
                                </tr>
                            </table>
                            <cc1:ucPanel ID="pnlPotentialPurchaseDetails" runat="server" GroupingText="Potential Purchase Details"
                                CssClass="fieldset-form">
                                <table width="100%" cellspacing="1" cellpadding="0" class="form-table">
                                    <tr>
                                        <td style="width: 2%">
                                            <cc1:ucRadioButton ID="rdoPO1" runat="server" GroupName="rdo" onclick="DisablePageValidators(1)"
                                                Checked="true" />
                                        </td>
                                        <td style="font-weight: bold; width: 18%">
                                            <cc1:ucLabel ID="lblPurchaseOrder" runat="server" Text="Purchase Order"></cc1:ucLabel>
                                        </td>
                                        <td style="font-weight: bold; width: 1%">
                                            :
                                        </td>
                                        <td style="width: 79%">
                                            <cc1:ucTextbox ID="txtPurchaseOrder" runat="server" Width="120px" MaxLength="11"
                                                AutoPostBack="true" OnTextChanged="txtPurchaseOrder_TextChanged"></cc1:ucTextbox>
                                            <asp:RequiredFieldValidator ID="rfvPurchaseOrderNumberValidation" runat="server"
                                                ControlToValidate="txtPurchaseOrder" Display="None" ValidationGroup="LogDiscrepancy">
                                            </asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td style="font-weight: bold;">
                                            <cc1:ucLabel ID="lblPurchaseOrderDate" runat="server" Text="Purchase Order Date"></cc1:ucLabel>
                                        </td>
                                        <td>
                                            :
                                        </td>
                                        <td>
                                            <cc1:ucDropdownList ID="ddlPurchaseOrderDate" runat="server" Width="150px" AutoPostBack="True"
                                                OnSelectedIndexChanged="ddlPurchaseOrderDate_SelectedIndexChanged">
                                                <asp:ListItem Text="--Select--" Value="0"></asp:ListItem>
                                            </cc1:ucDropdownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td style="font-weight: bold;">
                                            <cc1:ucLabel ID="lblVendor" runat="server" Text="Vendor"></cc1:ucLabel>
                                        </td>
                                        <td>
                                            :
                                        </td>
                                        <td>
                                            <cc1:ucLabel ID="lblVendorValue" runat="server"></cc1:ucLabel>
                                        </td>
                                    </tr>
                                </table>
                            </cc1:ucPanel>
                            <cc1:ucPanel ID="pnlLogBasisOfVendor" runat="server" GroupingText="Vendor" CssClass="fieldset-form">
                                <table width="100%" cellspacing="1" cellpadding="0" class="form-table">
                                    <tr>
                                        <td style="width: 2%">
                                            <cc1:ucRadioButton ID="rdoVendor11" runat="server" GroupName="rdo" onclick="DisablePageValidators(2)" />
                                        </td>
                                        <td style="font-weight: bold; width: 18%">
                                            <cc1:ucLabel ID="lblVendor1" runat="server" Text="Vendor"></cc1:ucLabel>
                                        </td>
                                        <td style="font-weight: bold; width: 1%">
                                            :
                                        </td>
                                        <td style="width: 79%">
                                            <cc3:ucSeacrhVendor ID="ucSeacrhVendor1" runat="server" />
                                            <asp:CustomValidator ID="cusvNoSelectedVendor" runat="server" ClientValidationFunction="checkVendorSelected"
                                                ValidationGroup="LogDiscrepancy" Display="None"></asp:CustomValidator>
                                        </td>
                                    </tr>
                                </table>
                            </cc1:ucPanel>
                        </cc1:ucView>
                        <cc1:ucView ID="vwLogSDR" runat="server">
                            <table width="100%" cellspacing="0" cellpadding="0" class="form-table">
                                <tr valign="top">
                                    <td width="30%">
                                        <cc1:ucPanel ID="pnlDeliveryDetail" runat="server" GroupingText="Delivery Detail"
                                            CssClass="fieldset-form">
                                            <table width="100%" cellspacing="1" cellpadding="0" class="form-table" height="140px">
                                                <tr>
                                                    <td style="font-weight: bold; width: 42%">
                                                        <cc1:ucLabel ID="lblSiteDetail" runat="server" Text="Site"></cc1:ucLabel>
                                                    </td>
                                                    <td style="font-weight: bold; width: 2%">
                                                        :
                                                    </td>
                                                    <td class="nobold" colspan="2">
                                                        <cc1:ucLabel ID="lblSiteValue" runat="server"></cc1:ucLabel>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="font-weight: bold;">
                                                        <cc1:ucLabel ID="lblPurchaseOrderNo" runat="server" Text="Purchase Order #"></cc1:ucLabel>
                                                    </td>
                                                    <td style="font-weight: bold;">
                                                        :
                                                    </td>
                                                    <td class="nobold" style="width: 22%">
                                                        <cc1:ucLabel ID="lblPurchaseOrderValue" runat="server"></cc1:ucLabel>
                                                    </td>
                                                    <td class="nobold" style="width: 34%">
                                                        <cc1:ucLabel ID="lblPurchaseOrderDateValue" runat="server"></cc1:ucLabel>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="font-weight: bold;">
                                                        <cc1:ucLabel ID="lblIssueRaisedDate2" runat="server" Text="Issue Raised Date"></cc1:ucLabel>
                                                    </td>
                                                    <td style="font-weight: bold;">
                                                        :
                                                    </td>
                                                    <td class="nobold">
                                                        <cc1:ucLabel ID="lblIssueRaisedDateValue" runat="server"></cc1:ucLabel>
                                                    </td>
                                                    <td class="nobold">
                                                        <cc1:ucLabel ID="lblIssueRaisedTimeValue" runat="server"></cc1:ucLabel>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="font-weight: bold;">
                                                        <cc1:ucLabel ID="lblDeliveryNo" runat="server" Text="Delivery #"></cc1:ucLabel>
                                                    </td>
                                                    <td style="font-weight: bold;">
                                                        :
                                                    </td>
                                                    <td class="nobold">
                                                        <cc1:ucLabel ID="lblDeliveryNoValue" runat="server"></cc1:ucLabel>
                                                    </td>
                                                    <td class="nobold">
                                                        <cc1:ucLabel ID="lblDeliveryDateValue" runat="server"></cc1:ucLabel>
                                                    </td>
                                                </tr>
                                                 <tr style="display:none;">
                                                    <td style="font-weight: bold;">
                                                        <cc1:ucLabel ID="lblCarrier_2" runat="server" Text="Carrier"></cc1:ucLabel>
                                                    </td>
                                                    <td style="font-weight: bold;">
                                                        :
                                                    </td>
                                                    <td class="nobold" colspan="2">
                                                        <cc1:ucLabel ID="lblCarrierValue" runat="server"></cc1:ucLabel>
                                                    </td>
                                                    <td>
                                                    </td>
                                                </tr>
                                            </table>
                                        </cc1:ucPanel>
                                    </td>
                                    <td width="2%">
                                        &nbsp;
                                    </td>
                                    <td width="35%">
                                        <cc1:ucPanel ID="pnlContactDetail" runat="server" GroupingText="Contact Detail" CssClass="fieldset-form">
                                            <table width="100%" height="140px">
                                                <tr>
                                                    <td>
                                                        <table width="100%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings"
                                                            height="62px">
                                                            <tr>
                                                                <td style="font-weight: bold; width: 30%">
                                                                    <cc1:ucLabel ID="lblVendor_1" runat="server" Text="Vendor "></cc1:ucLabel>
                                                                </td>
                                                                <td style="font-weight: bold; width: 2%">
                                                                    :
                                                                </td>
                                                                <td class="nobold" style="width: 68%">
                                                                    <cc1:ucLabel ID="lblVendorNumberValue" runat="server"></cc1:ucLabel>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="font-weight: bold;">
                                                                    <cc1:ucLabel ID="lblContactNo" runat="server" Text="Contact #"></cc1:ucLabel>
                                                                </td>
                                                                <td style="font-weight: bold;">
                                                                    :
                                                                </td>
                                                                <td class="nobold">
                                                                    <cc1:ucLabel ID="lblContactValue" runat="server"></cc1:ucLabel>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <table width="100%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings"
                                                            height="61px">
                                                            <tr>
                                                                <td style="font-weight: bold; width: 30%">
                                                                    <cc1:ucLabel ID="lblStockPlanner" runat="server" Text="Stock Planner"></cc1:ucLabel>
                                                                </td>
                                                                <td style="font-weight: bold; width: 2%">
                                                                    :
                                                                </td>
                                                                <td class="nobold" style="width: 68%">
                                                                    <cc1:ucLabel ID="lblStockPlannerNoValue" runat="server"></cc1:ucLabel>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="font-weight: bold;">
                                                                    <cc1:ucLabel ID="lblContactNo_1" runat="server" Text="Contact #"></cc1:ucLabel>
                                                                </td>
                                                                <td style="font-weight: bold;">
                                                                    :
                                                                </td>
                                                                <td class="nobold">
                                                                    <cc1:ucLabel ID="lblPlannerContactNoValue" runat="server"></cc1:ucLabel>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </cc1:ucPanel>
                                    </td>
                                    <td width="2%">
                                        &nbsp;
                                    </td>
                                    <td width="31%" rowspan="7" valign="top">
                                        <cc1:ucPanel ID="pnlCommunicationDetail" runat="server" GroupingText="Communication Detail"
                                            CssClass="fieldset-form">
                                            <table width="100%" cellspacing="0" cellpadding="0" class="form-table" height="140px">
                                                <tr>
                                                    <td>
                                                        <uc1:ucSDRCommunication ID="ucSDRCommunication1" runat="server" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </cc1:ucPanel>
                                    </td>
                                </tr>
                            </table>
                            <cc1:ucPanel ID="pnlPrematureInvoiceReceiptIssue" runat="server" GroupingText="Premature Invoice Receipt Issue"
                                CssClass="fieldset-form-mandatory">
                                <table width="100%" cellspacing="5" cellpadding="0" border="0" class="form-table">
                                    <tr>
                                        <td style="font-weight: bold;">
                                            <cc1:ucTextbox ID="txtPrematureReceiptInvoiceIssue" runat="server" onkeyup="checkTextLengthOnKeyUp(this,1000);"
                                                TextMode="MultiLine" Width="98%"></cc1:ucTextbox>
                                            <asp:RequiredFieldValidator ID="rfvPrematureReceiptRequired" runat="server" ControlToValidate="txtPrematureReceiptInvoiceIssue"
                                                Display="None" ValidationGroup="a" ErrorMessage="Please enter Premature Invoice Receipt Issue.">
                                            </asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                </table>
                            </cc1:ucPanel>
                            <cc1:ucPanel ID="pnlAdministrationFee" runat="server" CssClass="fieldset-form" GroupingText="Administration Fee">
                                <table width="100%" cellspacing="5" cellpadding="0" border="0" class="form-table">
                                    <tr id="trDiscrepancyCharges" runat="server">
                                        <td style="font-weight: bold; width: 20%">
                                            <cc1:ucRadioButton ID="rdoFreeOfCharge" runat="server" GroupName="DiscrepancyCharger"
                                                onclick="javascript:return IsAmountRequired(1);" />
                                        </td>
                                        <td style="font-weight: bold; width: 80%" align="left">
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr id="trAmount" runat="server">
                                        <td style="font-weight: bold;">
                                            <cc1:ucRadioButton ID="rdoOthers" runat="server" onclick="javascript:return IsAmountRequired(2);"
                                                GroupName="DiscrepancyCharger" />
                                        </td>
                                        <td style="font-weight: bold;">
                                            <cc1:ucLabel ID="UcLabel7" runat="server" Text="Amount"></cc1:ucLabel>
                                            &nbsp; : &nbsp;
                                            <cc1:ucTextbox ID="txtLabourCost" onkeyup="AllowDecimalOnly(this)" runat="server"
                                                Width="30px"></cc1:ucTextbox>
                                            <asp:RequiredFieldValidator ID="rfvLabourCostRequired" runat="server" ControlToValidate="txtLabourCost"
                                                Display="None" ValidationGroup="a" ErrorMessage="Please enter the labour cost.">
                                            </asp:RequiredFieldValidator>
                                            <asp:CompareValidator ID="cmpvLabourCostGreaterThanZero" runat="server" ControlToValidate="txtLabourCost"
                                                Operator="GreaterThan" ValueToCompare="0" Type="Double" Display="None" ErrorMessage="Labour cost must be greater than zero."
                                                ValidationGroup="a">
                                            </asp:CompareValidator>
                                            <asp:RegularExpressionValidator ID="revLabourCostCorrectFormatRequired" runat="server"
                                                ControlToValidate="txtLabourCost" Display="None" ValidationGroup="a" SetFocusOnError="true"
                                                ErrorMessage="Please enter labour cost upto 2 decimal digits." ValidationExpression="^\d{0,8}(\.\d{0,2})?$">
                                            </asp:RegularExpressionValidator>
                                        </td>
                                    </tr>
                                </table>
                            </cc1:ucPanel>
                            <cc1:ucPanel ID="pnlInternalComments" GroupingText="Internal Comments" runat="server"
                                CssClass="fieldset-form">
                                <table width="100%" cellspacing="5" cellpadding="0" border="0" class="form-table">
                                    <tr>
                                        <td style="font-weight: bold;" colspan="2">
                                            <cc1:ucTextbox ID="txtInternalComments" runat="server" onkeyup="checkTextLengthOnKeyUp(this,1000);"
                                                TextMode="MultiLine" Width="98%"></cc1:ucTextbox>
                                        </td>
                                    </tr>
                                </table>
                            </cc1:ucPanel>
                            <cc1:ucPanel ID="pnlDecisionComments" Width="100%" runat="server" Visible="false"
                                GroupingText="Decision & Comments"
                                CssClass="fieldset-form">
                                <table width="95%" cellspacing="5" cellpadding="0" border="0" class="form-table">
                                    <tr>
                                        <td style="font-weight: bold;">
                                            <cc1:ucLabel ID="lblDecision" runat="server"> </cc1:ucLabel>
                                            <br />
                                            <br />
                                            <cc1:ucLabel ID="lblSPActionComments" runat="server"></cc1:ucLabel>
                                        </td>
                                    </tr>
                                </table>
                            </cc1:ucPanel>
                            <cc1:ucPanel ID="pnlLocation" Width="25%" GroupingText="Location" runat="server"
                                CssClass="fieldset-form">
                                <table width="95%" cellspacing="5" cellpadding="0" border="0" class="form-table">
                                    <tr>
                                        <td style="font-weight: bold;" colspan="2">
                                            <cc1:ucTextbox ID="txtlocation" runat="server" onkeyup="checkTextLengthOnKeyUp(this,100);"
                                                TextMode="MultiLine" Width="98%"></cc1:ucTextbox>
                                        </td>
                                    </tr>
                                </table>
                            </cc1:ucPanel>
                            <table width="100%" cellspacing="0" cellpadding="0" class="form-table">
                                <tr>
                                    <td>
                                        <div class="button-row" style="text-align: right">
                                             <cc1:ucButton ID="btnUpdateLocation" runat="server" Text="Update Location"   OnClick="btnUpdateLocation_Click" class="button" Visible="false"  /> &nbsp;
                                            <cc1:ucButton ID="btnBack_1" runat="server" Visible="false" CssClass="button" OnClick="btnBack_1_Click" />
                                        </div>
                                    </td>
                                </tr>
                            </table>
                            <iframe id="ifUserControl" runat="server" frameborder="0" scrolling="no" 
                                width="954" style="margin: 0px 0px 0px 0px; background-image: url('../Images/conteint-mainbg.jpg') no-repeat scroll right bottom transparent;">
                            </iframe>
                        </cc1:ucView>
                    </cc1:ucMultiView>
                </div>
                  <br />
            <cc1:ucLabel ID="lblComments" Text="Comments" Font-Bold="true" Visible="false" runat="server"></cc1:ucLabel>
            <div style="border-bottom: 1px solid black;">
                &nbsp;</div>
            <asp:Repeater ID="rptDiscrepancyComments" runat="server">
                <ItemTemplate>
                    <table cellspacing="5" cellpadding="0" align="center" style="border: 1px solid black;
                        border-top: none; width: 100%">
                        <tr>
                            <td style="font-weight: bold; width: 10%;">
                                <cc1:ucLabel ID="lblDate_1" Text="Date" runat="server"></cc1:ucLabel>/<cc1:ucLabel
                                    ID="lblActualTime" Text="Time" runat="server"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold; width: 1%;">
                                :
                            </td>
                            <td style="width: 22%;">
                                <cc1:ucLabel ID="lblDateT_1" Text='<%#Eval("CommentDateTime", "{0:dd/MM/yyyy}") + "  " + Eval("CommentDateTime",@"{0:HH:mm}")%>'
                                    runat="server"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold; width: 10%;">
                                <cc1:ucLabel ID="lblUser_1" Text="User" runat="server"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold; width: 1%;">
                                :
                            </td>
                            <td style="width: 56%;">
                                <cc1:ucLabel ID="lblUserT_1" Text='<%#DataBinder.Eval(Container.DataItem, "Username")%>'
                                    runat="server"></cc1:ucLabel>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold; width: 100%;" colspan="6">
                                <cc1:ucLabel ID="lblCommentLabel_1" Text="Comment" runat="server"></cc1:ucLabel>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 100%;" colspan="6">
                                <cc1:ucLabel Style="width: 970px; word-wrap: break-word; display: block;" ID="lblCommentLabelT"
                                    Text='<%#DataBinder.Eval(Container.DataItem, "Comment")%>' runat="server"></cc1:ucLabel>
                            </td>
                        </tr>
                    </table>
                </ItemTemplate>
            </asp:Repeater>
            </div>
            <div class="button-row">
                <cc1:ucButton ID="btnLogDiscrepancy" runat="server" Text="Log Discrepancy" class="button"
                    ValidationGroup="LogDiscrepancy" OnClick="btnLogDiscrepancy_Click" />
                <cc1:ucButton ID="btnSave" runat="server" Text="Save" class="button" OnClientClick="disableButton(this,'a')"
                    UseSubmitBehavior="false" OnClick="btnSave_Click"  />
                <cc1:ucButton ID="btnBack" runat="server" Text="Back" class="button" OnClick="btnBack_Click1" />
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="true"
                    ShowSummary="false" Style="color: Red" ValidationGroup="a" />
            </div>
            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td align="center">
                        <asp:UpdateProgress runat="server" ID="PageUpdateProgress" AssociatedUpdatePanelID="up1">
                            <ProgressTemplate>
                                <div align="center" style="background-color: #000; top: 0px; left: 0px; bottom: 0px;
                                    right: 0px; padding-top: 20%; margin: 0; width: 100%; height: 130%; overflow: hidden;
                                    position: absolute; z-index: 1000; filter: alpha(opacity=50); opacity: 0.5;">
                                    <asp:Image ID="imgWait" runat="server" ImageUrl="~/Images/Ajaxloading.gif" ImageAlign="Middle" />
                                </div>
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    <triggers>
            <asp:AsyncPostBackTrigger ControlID="btnViewWF" />
            <asp:AsyncPostBackTrigger ControlID="btnLogDiscrepancy" />
            <asp:AsyncPostBackTrigger ControlID="btnSave" />
            <asp:AsyncPostBackTrigger ControlID="btnBack" />
            <asp:AsyncPostBackTrigger ControlID="btnBack_1" />
            <asp:AsyncPostBackTrigger ControlID="btnAddComment" />
            <%--<asp:AsyncPostBackTrigger ControlID="btnYes" />
            <asp:AsyncPostBackTrigger ControlID="btnNo" />--%>
            <%--<asp:AsyncPostBackTrigger ControlID="btnAdd" />--%>
        </triggers>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="updpnlAddComment" runat="server">
        <contenttemplate>
            <asp:Button ID="btnAddCommentMPE" runat="Server" Style="display: none" />
            <ajaxToolkit:ModalPopupExtender ID="mdlAddComment" runat="server" TargetControlID="btnAddCommentMPE"
                PopupControlID="pnlAddComment" BackgroundCssClass="modalBackground" BehaviorID="AddComment"
                DropShadow="false" />
            <%--<asp:Panel ID="pnlAddComment" runat="server" Style="display: none; width:50%" >--%>
            <asp:Panel ID="pnlAddComment" runat="server" Style="display: none; width:630px" >
                <div style="overflow-y: hidden; overflow-x: hidden; background-color: #fff; padding: 5px; border: 2px solid #ccc; text-align: left;">
                    <table cellspacing="5" cellpadding="0" border="0" align="center" class="top-settingsNoBorder-popup">
                        <tr>                            
                            <td style="font-weight: bold; width:15%;">                                
                                <cc1:ucLabel ID="lblUser" Text="User" runat="server"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold; width:1%;">
                                :
                            </td>
                            <td style="width:30%;">
                                <cc1:ucLabel ID="lblUserT" runat="server"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold; width:10%;">                                
                                <cc1:ucLabel ID="lblDate" Text="Date" runat="server"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold; width:1%;">
                                :
                            </td>
                            <td style="width:30%;">
                                <cc1:ucLabel ID="lblDateT" runat="server"></cc1:ucLabel>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold; width:15%;">                                
                                <cc1:ucLabel ID="lblDiscrepancyNo" Text="Discrepancy #" runat="server"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold; width:1%;">
                                :
                            </td>
                            <td style="width:80%;" colspan="4">
                                <cc1:ucLabel ID="lblDiscrepancyNoT" runat="server"></cc1:ucLabel>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold; width:100%;" colspan="6">                                
                                <cc1:ucLabel ID="lblCommentLabel" Text="Comment" runat="server"></cc1:ucLabel>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold; width:100%;" colspan="6">
                                <cc1:ucTextarea ID="txtUserComments" Height="150px" Width="600px" runat="server"></cc1:ucTextarea>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" style="width:80%;" colspan="4">
                                <cc1:ucLabel ID="lblErrorMsg" Font-Bold="true" ForeColor="Red" runat="server"></cc1:ucLabel>
                            </td>
                            <td align="right" style="width:20%;" colspan="2">
                                <cc1:ucButton ID="btnSave_1" runat="server" Text="Save" CssClass="button"  
                                    OnCommand="btnSave_1_Click" />
                                &nbsp;
                                <cc1:ucButton ID="btnBack_2" runat="server" Text="Back" CssClass="button" 
                                    OnCommand="btnBack_2_Click" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </contenttemplate>
        <triggers>
            <asp:AsyncPostBackTrigger ControlID="btnSave_1" />
            <asp:AsyncPostBackTrigger ControlID="btnBack_2" />
            <asp:AsyncPostBackTrigger ControlID="btnAddComment" />
        </triggers>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="updAlternateEmail" runat="server">
        <ContentTemplate>
            <asp:Button ID="btnAlternateEmailMPE" runat="Server" Style="display: none" />
            <ajaxToolkit:ModalPopupExtender ID="mdlAlternateEmail" runat="server" TargetControlID="btnAlternateEmailMPE"
                PopupControlID="pnlAlternateEmail" BackgroundCssClass="modalBackground" BehaviorID="btnSave"
                DropShadow="false"/>
             <asp:Panel ID="pnlAlternateEmail" runat="server" Style="display: none; width: 450px">
                 <div style="overflow-y: hidden; overflow-x: hidden; background-color: #fff; padding: 5px;
                    border: 2px solid #ccc; text-align: left;">
                    <table cellspacing="5" cellpadding="0" border="0" align="center" class="top-settingsNoBorder-popup">
                        <tr>
                            <td style="font-weight: bold; width:100%;" colspan="3" align="left">
                                <cc1:ucLabel ID="lblDiscAlternateEmailMsg" Text="No vendor's email available for communication, please enter alternative email address :" runat="server"></cc1:ucLabel>
                            </td>
                        </tr>
                         <tr>
                            <td style="font-weight: bold; width:100%;" colspan="3" align="left">
                                <br />
                            </td>
                        </tr>
                         <tr>
                            <td style="width: 100%;" align="left" valign="top">
                                <table>
                                    <tr>
                                        <td>
                                            <b>
                                                <cc1:ucTextbox ID="txtAlternateEmailText" Width="420px" runat="server"></cc1:ucTextbox></b>                                               
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:RegularExpressionValidator ID="revValidEmail" ControlToValidate="txtAlternateEmailText"
                                                runat="server" ValidationGroup="AltEmail" ErrorMessage="Please enter valid email address"
                                                ValidationExpression="^[\w-\.\']{1,}\@([\da-zA-Z-]{1,}\.){1,3}[\da-zA-Z-]{2,6}$" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:RequiredFieldValidator ID="rfvEmailAddressCanNotBeEmpty" Enabled="false" ControlToValidate="txtAlternateEmailText"
                                                runat="server" ValidationGroup="AltEmail" ErrorMessage="Please enter valid email address" />
                                        </td>
                                    </tr>
                                </table>
                                <%--<cc1:ucButton ID="btnAddMoreEmail" runat="server" Text="Add More Email"
                                        CssClass="button" OnCommand="btnAddEmailId_Click" ValidationGroup="AltEmail" /> --%>                               
                            </td>                           
                        </tr>
                        <tr>
                            <td style="width: 100%;">
                                <b><cc1:ucLabel ID="lblMultyAlternateEmail" Width="420px" Visible="false" runat="server"></cc1:ucLabel></b>                                 
                            </td>                           
                        </tr>
                        <tr>
                            <td colspan="3" align="right" style="width:100%;">
                                <div style="margin-right:5px;">
                                    <cc1:ucButton ID="btnSetAlternateEmail" runat="server" Text="Set Alternate Email"
                                        CssClass="button" OnCommand="btnSetAlternateEmail_Click" ValidationGroup="AltEmail" />
                                    <%--<cc1:ucButton ID="btnClear" runat="server" Text="Clear"
                                        CssClass="button" OnCommand="btnClear_Click" ValidationGroup="AltEmail" />--%>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
             </asp:Panel>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnSave" />
            <asp:AsyncPostBackTrigger ControlID="btnSetAlternateEmail" />            
        </Triggers>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="updAPAction" runat="server">
        <ContentTemplate>
            <asp:Button ID="btnAccountsPayableActionMPE" runat="Server" Style="display: none" />
            <ajaxToolkit:ModalPopupExtender ID="mdlAPAction" runat="server" TargetControlID="btnAccountsPayableActionMPE"
                PopupControlID="pnlAPAction" BackgroundCssClass="modalBackground" BehaviorID="ShowAPAction"
                DropShadow="false" />
            <asp:Panel ID="pnlAPAction" runat="server" Style="display: none; width: 630px">
                <div style="overflow-y: hidden; overflow-x: hidden; background-color: #fff; padding: 5px;
                    border: 2px solid #ccc; text-align: left;">
                    <table cellspacing="5" cellpadding="0" align="center" width="100%" class="top-settingsNoBorder-popup">
                        <tr>
                            <td>
                                <uc2:APAction ID="ucApAction" runat="server" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
     <%--POPUP ERROR MESSAGE FOR RAISE QUERY--%>
     <!-- Error Message Section -->
    <asp:UpdatePanel ID="updpnlShowError" runat="server">
        <ContentTemplate>
            <asp:Button ID="btnShowErrorMPE" runat="Server" Style="display: none" />
            <ajaxToolkit:ModalPopupExtender ID="mdlShowError" runat="server" TargetControlID="btnShowErrorMPE"
                PopupControlID="pnlShowError" BackgroundCssClass="modalBackground" BehaviorID="ShowError"
                CancelControlID="btnOk" DropShadow="false" />
            <asp:Panel ID="pnlShowError" runat="server" Style="display: none; width: 630px">
                <div style="overflow-y: hidden; overflow-x: hidden; background-color: #fff; padding: 5px;
                    border: 2px solid #ccc; text-align: left;">
                    <table cellspacing="5" cellpadding="0" border="0" align="center" width="100%" class="top-settingsNoBorder-popup">
                        <tr>
                            <td style="font-weight: bold; text-align: center; width: 100%;">
                                <cc1:ucLabel ID="lblError" Font-Bold="true" Font-Size="Medium" ForeColor="Black"
                                    Text="Error" runat="server"></cc1:ucLabel>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: center; width: 100%;">
                                <cc1:ucLabel ID="lblMessageFirst" Text="" ForeColor="Black" runat="server"></cc1:ucLabel>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: center; width: 100%;">
                                <cc1:ucLabel ID="lblMessageSecond" Text="" ForeColor="Black" runat="server"></cc1:ucLabel>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: center; width: 100%;">
                                <cc1:ucButton ID="btnOk" Text="OK" CssClass="button" Width="80px" runat="server">
                                </cc1:ucButton>
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnShowErrorMPE" />
        </Triggers>
    </asp:UpdatePanel>


     <%--POP MESSAGE FOR RAISE QUERY--%>
        <asp:UpdatePanel ID="updpnlWarning" runat="server">
        <ContentTemplate>
            <asp:Button ID="btnConfirmMsg" runat="Server" Style="display: none" />
            <ajaxToolkit:ModalPopupExtender ID="mdlRaiseQueryConfirmMsg" runat="server" TargetControlID="btnConfirmMsg"
                PopupControlID="pnlConfirmMsg" BackgroundCssClass="modalBackground" BehaviorID="ConfirmMsg"
                DropShadow="false" />
            <asp:Panel ID="pnlConfirmMsg" runat="server" Style="display: none;">
                <div class="popup-maincontainer" style="overflow-y: hidden; overflow-x: hidden;">
                    <h3><cc1:ucLabel ID="lblWarningInfo_3" runat="server"></cc1:ucLabel></h3>
                    <table cellspacing="5" cellpadding="0" border="0" align="center" class="popup-maincontainer" >
                        <tr>
                            <td>
                            <div  class="popup-innercontainer top-setting-Popup">
                                 <div class="row" style="width:580px">
                                     <cc1:ucLabel ID="lblRaiseQueryMsg1"  runat="server"></cc1:ucLabel>
                                     <br />
                                     <br />
                                     <cc1:ucLabel ID="lblRaiseQueryMsg2"  runat="server"></cc1:ucLabel>
                                 </div>
                           </div>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <div class="row">
                                <cc1:ucButton ID="btnRaiseQueryPopupContinue" runat="server" Text="CONTINUE" CssClass="button"
                                    OnCommand="btnContinue_Click" />
                                &nbsp;
                                <cc1:ucButton ID="btnRaiseQueryPopupBack" runat="server" Text="BACK" CssClass="button"  OnCommand="btnRaiseQueryPopupBack_Click"  />
                            </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnRaiseQueryPopupContinue" />    
             <asp:AsyncPostBackTrigger ControlID="btnRaiseQueryPopupBack" />             
        </Triggers>
    </asp:UpdatePanel>

    <cc1:ucPanel ID="pnlQueryOverview_1" Visible="false" runat="server" CssClass="fieldset-form" GroupingText="Query Overview">                         
    <asp:Repeater ID="rptQueryOverview" runat="server" onitemdatabound="rptQueryOverview_ItemDataBound" >
                <ItemTemplate>
                    <table cellspacing="0" cellpadding="0" align="center" class="table-border-color">
                        <tr>
                            <td style="padding-top: 2px;">
                                <table cellspacing="5" cellpadding="0" align="center" style="width: 100%; border-bottom: none;">
                                    <tr>
                                        <td colspan="6">
                                            <asp:HiddenField ID="hdnQueryDiscrepancyID" Value='<%#DataBinder.Eval(Container.DataItem, "QueryDiscrepancyID")%>'
                                                runat="server" />
                                            <asp:HiddenField ID="hdnDiscrepancyLogID" Value='<%#DataBinder.Eval(Container.DataItem, "DiscrepancyLogID")%>'
                                                runat="server" />
                                            <asp:HiddenField ID="hdnIsQueryClosedManually" Value='<%#DataBinder.Eval(Container.DataItem, "IsQueryClosedManually")%>'
                                                runat="server" />
                                            <cc1:ucLabel ID="lblQueryRaisedByVendor" Text="QUERY RAISED BY VENDOR" isRequired="true"
                                                Font-Bold="true" runat="server"></cc1:ucLabel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: bold; width: 10%;">
                                            <cc1:ucLabel ID="lblDate_1" Text="Date" runat="server" isRequired="true"></cc1:ucLabel><b>/</b><cc1:ucLabel
                                                ID="lblActualTime_1" Text="Time" isRequired="true" runat="server"></cc1:ucLabel>
                                        </td>
                                        <td style="font-weight: bold; width: 1%;">
                                            <cc1:ucLabel ID="lblCollon1" Text=":" runat="server" isRequired="true" Font-Bold="true"></cc1:ucLabel>
                                        </td>
                                        <td style="width: 22%;">
                                            <cc1:ucLabel ID="lblDateT_1" Text='<%#Eval("VendorQueryDate", "{0:dd/MM/yyyy}") + "  " + Eval("VendorQueryDate",@"{0:HH:mm}")%>'
                                                runat="server" ForeColor="#800000"></cc1:ucLabel>
                                        </td>
                                        <td style="font-weight: bold; width: 10%;">
                                            <cc1:ucLabel ID="lblUser_1" Text="User" isRequired="true" runat="server"></cc1:ucLabel>
                                        </td>
                                        <td style="font-weight: bold; width: 1%;">
                                            <cc1:ucLabel ID="lblCollon2" Text=":" runat="server" isRequired="true" Font-Bold="true"></cc1:ucLabel>
                                        </td>
                                        <td style="width: 56%;">
                                            <cc1:ucLabel ID="lblUserT_1" Text='<%#DataBinder.Eval(Container.DataItem, "VendorUserName")%>'
                                                runat="server" ForeColor="#800000"></cc1:ucLabel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: bold; width: 100%;" colspan="6">
                                            <cc1:ucLabel ID="lblCommentLabel_1" Text="Comment" isRequired="true" runat="server"></cc1:ucLabel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 100%;" colspan="6">
                                            <cc1:ucLabel Style="width: 970px; word-wrap: break-word; display: block;" ID="lblCommentLabelT_1"
                                                Text='<%#DataBinder.Eval(Container.DataItem, "VendorComment")%>' runat="server"
                                                ForeColor="#800000"></cc1:ucLabel>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding-bottom: 2px;">
                                <table cellspacing="5" cellpadding="0" align="center" style="width: 100%" id="tblGoodsIn"
                                    runat="server">
                                    <tr>
                                        <td colspan="9">
                                            <cc1:ucLabel ID="lblGoodsInFeedback" Text="GOODS IN FEEDBACK" isRequired="true" Font-Bold="true"
                                                runat="server"></cc1:ucLabel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: bold; width: 10%;">
                                            <cc1:ucLabel ID="lblDate_2" Text="Date" runat="server" isRequired="true"></cc1:ucLabel><b>/</b><cc1:ucLabel
                                                ID="lblActualTime_2" Text="Time" isRequired="true" runat="server"></cc1:ucLabel>
                                        </td>
                                        <td style="font-weight: bold; width: 1%;">
                                            <cc1:ucLabel ID="lblCollon3" Text=":" runat="server" isRequired="true" Font-Bold="true"></cc1:ucLabel>
                                        </td>
                                        <td style="width: 22%;">
                                            <cc1:ucLabel ID="lblDateT_2" Text='<%#Eval("GoodsInQueryDate", "{0:dd/MM/yyyy}") + "  " + Eval("GoodsInQueryDate",@"{0:HH:mm}")%>'
                                                runat="server" ForeColor="#800000"></cc1:ucLabel>
                                        </td>
                                        <td style="font-weight: bold; width: 10%;">
                                            <cc1:ucLabel ID="lblUser_2" Text="User" isRequired="true" runat="server"></cc1:ucLabel>
                                        </td>
                                        <td style="font-weight: bold; width: 1%;">
                                            <cc1:ucLabel ID="lblCollon4" Text=":" runat="server" isRequired="true" Font-Bold="true"></cc1:ucLabel>
                                        </td>
                                        <td style="width: 22%;">
                                            <cc1:ucLabel ID="lblUserT_2" Text='<%#DataBinder.Eval(Container.DataItem, "GoodsInUserName")%>'
                                                runat="server" ForeColor="#800000"></cc1:ucLabel>
                                        </td>
                                        <td style="font-weight: bold; width: 10%;">
                                            <cc1:ucLabel ID="lblCommunitcation" Text="Communitcation" isRequired="true" runat="server"></cc1:ucLabel>
                                        </td>
                                        <td style="font-weight: bold; width: 1%;">
                                            <cc1:ucLabel ID="lblCollon6" Text=":" runat="server" isRequired="true" Font-Bold="true"></cc1:ucLabel>
                                        </td>
                                        <td style="width: 23%">
                                            <a id="lnkView" runat="server" target='_blank' onclick='window.open(this.href, this.target,"scrollbars=1,left=100px,top=20px,width=1000px,height=620px,toolbar=0,scrollbars=0,status=0"); return false;'>
                                                View</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: bold;">
                                            <cc1:ucLabel ID="lblAction" Text="Action" runat="server" isRequired="true"></cc1:ucLabel>
                                        </td>
                                        <td style="font-weight: bold;">
                                            <cc1:ucLabel ID="lblCollon5" Text=":" runat="server" isRequired="true" Font-Bold="true"></cc1:ucLabel>
                                        </td>
                                        <td colspan="7">
                                            <cc1:ucLabel ID="lblActionT" Text='<%#DataBinder.Eval(Container.DataItem, "QueryAction")%>'
                                                runat="server" ForeColor="#800000"></cc1:ucLabel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: bold;" colspan="9">
                                            <cc1:ucLabel ID="lblCommentLabel_2" Text="Comment" isRequired="true" runat="server"></cc1:ucLabel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="9">
                                            <cc1:ucLabel Style="width: 970px; word-wrap: break-word; display: block;" ID="lblCommentLabelT_2"
                                                Text='<%#DataBinder.Eval(Container.DataItem, "GoodsInComment")%>' runat="server"
                                                ForeColor="#800000"></cc1:ucLabel>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding-bottom: 2px;">
                                <table cellspacing="5" cellpadding="0" align="center" style="width: 100%;" visible="false" id="tblQueryClosedManually"
                                    runat="server">
                                    <tr>
                                        <td colspan="9">
                                            <cc1:ucLabel ID="lblQueryClosedManually" Text="Query Closed Manually" isRequired="true" Font-Bold="true"
                                                runat="server"></cc1:ucLabel>
                                        </td>
                                    </tr>
                                
                                </table>
                            </td>
                        </tr>
                    </table>
                </ItemTemplate>
            </asp:Repeater>
            </cc1:ucPanel>  
</asp:Content>

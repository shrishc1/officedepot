﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.Discrepancy.LogDiscrepancy;
using BusinessLogicLayer.ModuleBAL.Discrepancy.LogDiscrepancy;
using System.Text;
using Utilities;
using WebUtilities;
using BaseControlLibrary;
using BusinessEntities.ModuleBE.Security;
using BusinessLogicLayer.ModuleBAL.Security;
using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using BusinessLogicLayer.ModuleBAL.Appointment.SiteSettings;

public partial class ModuleUI_Discrepancy_LogDiscrepancy_DISLog_Workflow : CommonPage
{
    string DebitRaisedText = WebCommon.getGlobalResourceValue("DebitRaisedText");
    sendCommunicationCommon oSendCommunicationCommon = new sendCommunicationCommon();
    int count = 1;
    protected void Page_InIt(object sender, EventArgs e)
    {
        ucApAction.CurrentPage = this;
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            lblErrorMsg.Text = string.Empty;
            this.BindDiscrepancyLogComment();
            this.BindDiscrepancyQuery();
            this.SetDebitCreditPosting();
        }

        // get vdrno for discrepancy
        if (GetQueryStringValue("VDRNo") != null && !string.IsNullOrEmpty(GetQueryStringValue("VDRNo").ToString()))
        {
            lblVDRNo.Text = " - " + GetQueryStringValue("VDRNo").ToString();
        }
        if (Session["Role"] != null)
        {
            if (Session["Role"].ToString() == "Vendor")
            {
                btnAddComment.Visible = true;
                btnAddImages.Visible = false;
            }
        }

        // get html of discrepancy
        if (GetQueryStringValue("disLogID") != null && !string.IsNullOrEmpty(GetQueryStringValue("disLogID").ToString()))
        {
            int? DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
            if (DiscrepancyLogID.HasValue)
            {
                this.ShowACP001ActionButton(Convert.ToInt32(DiscrepancyLogID));
                DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
                DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();
                List<DiscrepancyBE> lstWorkFlow = new List<DiscrepancyBE>();
                oDiscrepancyBE.Action = "GetHtml";
                oDiscrepancyBE.DiscrepancyLogID = DiscrepancyLogID;
                lstWorkFlow = oDiscrepancyBAL.GetWorkFlowHTMLsBAL(oDiscrepancyBE);
                oDiscrepancyBAL = null;
                if (lstWorkFlow.Count > 0)
                {
                    var lstRPWorkFlowData = lstWorkFlow.Where(x => x.APHTML != "&nbsp;" || x.INVHTML != "&nbsp;" || x.GINHTML != "&nbsp;" || x.VENHTML != "&nbsp;" || x.MEDHTML != "&nbsp;");
                    rpWorkflow.DataSource = lstRPWorkFlowData;
                    rpWorkflow.DataBind();
                    hdDiscrepancyStatus.Value = lstWorkFlow[0].DiscrepancyStatus;
                    VendorID = lstWorkFlow[0].VendorID;
                    SiteID = lstWorkFlow[0].SiteID;
                    DiscrepancyTypeID = lstWorkFlow[0].DiscrepancyTypeID;
                    //load vendors email and address
                    GetDetails();

                    if (hdDiscrepancyStatus.Value == "D")
                    {
                        var delDiscrepancyBE = lstWorkFlow.Find(x => x.DELHTML != String.Empty && x.APHTML != null && x.INVHTML != null && x.GINHTML != null && x.VENHTML != null);
                        if (delDiscrepancyBE != null)
                            hdnDelDisData.Value = delDiscrepancyBE.DELHTML;
                    }
                    if (hdDiscrepancyStatus.Value == "F")
                        hdUserData.Value = " Discrepancy Forced Closed By " + lstWorkFlow[0].ForceClosedUserName + " on " + lstWorkFlow[0].ForceClosedDate;
                }

                string DivGallery = CommonPage.GetImagesStringNewLayout(DiscrepancyLogID.Value);
                if (string.IsNullOrEmpty(DivGallery))
                {
                    btnViewImage.Visible = false;
                }
                else
                {
                    discrepancygallery.InnerHtml = DivGallery.ToString();
                    btnViewImage.Visible = true;
                }
            }
        }
        else
        {
            UIUtility.PageValidateForODUser();
        }
    }
    public string GetType(int discrepancyTypeId)
    {
        string strType = string.Empty;
        if (discrepancyTypeId == null) { discrepancyTypeId = 0; }
        switch (discrepancyTypeId)
        {
            case 1:
                strType = WebCommon.getGlobalResourceValue("Overs");
                break;
            case 2:
                strType = WebCommon.getGlobalResourceValue("Shortage");
                break;
            case 3:
                strType = WebCommon.getGlobalResourceValue("GoodsReceivedDamaged");
                break;
            case 4:
                strType = WebCommon.getGlobalResourceValue("NoPurchaseorder");
                break;
            case 5:
                strType = WebCommon.getGlobalResourceValue("NoPaperwork");
                break;
            case 6:
                strType = WebCommon.getGlobalResourceValue("IncorrectProduct");
                break;
            case 7:
                strType = WebCommon.getGlobalResourceValue("PresentationIssue");
                break;
            case 8:
                strType = WebCommon.getGlobalResourceValue("IncorrectAddress");
                break;
            case 9:
                strType = WebCommon.getGlobalResourceValue("PaperworkAmended");
                break;
            case 10:
                strType = WebCommon.getGlobalResourceValue("WrongPackSize");
                break;
            case 11:
                strType = WebCommon.getGlobalResourceValue("FailPalletSpecification");
                break;
            case 12:
                strType = WebCommon.getGlobalResourceValue("QualityIssues");
                break;
            case 13:
                strType = WebCommon.getGlobalResourceValue("PrematureInvoiceReceipt");
                break;
            case 14:
                strType = WebCommon.getGlobalResourceValue("GenericDiscrepancy");
                break;
            case 15:
                strType = WebCommon.getGlobalResourceValue("ShuttleDiscrepancy");
                break;
            case 16:
                strType = WebCommon.getGlobalResourceValue("ReservationIssue");
                break;
            case 20:
                strType = WebCommon.getGlobalResourceValue("ItemNotOnPO");
                break;
        }
        return strType;
    }
    protected void BtnGenerateLabels_Click(object sender, EventArgs e)
    {
        int No;
        if (txtNoofLabel.Text.Trim() != string.Empty && int.TryParse(txtNoofLabel.Text, out No))
        {
            //get vendor details  
            DiscrepancyBE oNewDiscrepancyBE = new DiscrepancyBE();
            DiscrepancyBAL oNewDiscrepancyBAL = new DiscrepancyBAL();
            oNewDiscrepancyBE.Action = "GetDiscrepancySiteVendorDetail";
            oNewDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID"));

            List<DiscrepancyBE> lstVendor = oNewDiscrepancyBAL.GetDiscrepancyLogSiteVendorDetailsBAL(oNewDiscrepancyBE);
            oNewDiscrepancyBAL = null;
            string strCommunicationTo = string.Empty;
            //****************** Get vendor EmailId ***************************
            if (lstVendor != null && lstVendor.Count > 0)
            {
                strCommunicationTo = lstVendor[0].CommunicationTo;
                SCT_UserBE oSCT_UserBE = new SCT_UserBE();
                SCT_UserBAL oSCT_UserBAL = new SCT_UserBAL();
                oSCT_UserBE.Action = "GetContactDetailsDiscrepancy";
                oSCT_UserBE.VendorID = lstVendor[0].VendorID;
                oSCT_UserBE.SiteId = lstVendor[0].SiteID;
                List<SCT_UserBE> lstVendorDetails1 = oSCT_UserBAL.GetUserDetailsBAL(oSCT_UserBE);
                if (lstVendorDetails1 != null && lstVendorDetails1.Count > 0)
                {
                    strCommunicationTo += ",";
                    foreach (SCT_UserBE item in lstVendorDetails1)
                    {
                        if (!strCommunicationTo.Contains(item.EmailId.ToString()))
                        {
                            strCommunicationTo += item.EmailId.ToString() + ", ";
                        }
                    }
                }
            }
            //*****************************************************************
            StringBuilder sb = new StringBuilder();


            for (int iCount = 1; iCount <= Convert.ToInt32(txtNoofLabel.Text.Trim()); iCount++)
            {
                //*************
                string ReturnAddress = lstVendor[0].Vendor.ReturnAddress.Replace("\r\n", "</span></td></tr><tr><td valign='top'><span class='style10'>");
                int discrepancyTypeID = lstVendor[0].DiscrepancyTypeID != null ? Convert.ToInt32(lstVendor[0].DiscrepancyTypeID) : 0;

                List<DiscrepancyBE> lstDetails = oSendCommunicationCommon.getDiscrepancyItemDetail("GetDiscrepancyItem", Convert.ToInt32(GetQueryStringValue("disLogID").ToString()), this.GetType(discrepancyTypeID));
                if (lstVendor[0].DiscrepancyTypeID != 19)
                {
                    sb.Append("<table width='100%' border='1' style=' border-collapse: collapse;'  BORDERCOLOR=black  > " +
                "<tr><td valign='top' ><div align='center' class='style1'>" + WebCommon.getGlobalResourceValue("Discrepancy") + " # - " + GetQueryStringValue("VDRNo") + " </div></td></tr> " +

               "<tr><td valign='top' align='center' ><span class='style1'>" + WebCommon.getGlobalResourceValue("Location") + " - </span><span style='font-size:210%'>" + lstVendor[0].Location.ToString() + "</span></td></tr> " +
               "<tr><td valign='top' align='center'><span align='center' class='style1'>" + WebCommon.getGlobalResourceValue("Vendor") + " # - </span><span style='font-size:210%'>" + lstVendor[0].Vendor.Vendor_No + " - " + lstVendor[0].Vendor.VendorName + " </span></td></tr> " +
                "<tr><td valign='top' align='center'><span align='center' class='style1'>" + WebCommon.getGlobalResourceValue("PurchaseOrder") + " - </span><span style='font-size:210%'>" + Convert.ToString(lstVendor[0].PurchaseOrder.Purchase_order) + " </span></td></tr> " +
                "<tr><td valign='top' align='center'><span align='center' class='style1'>" + WebCommon.getGlobalResourceValue("DeliveryNote") + " - </span><span style='font-size:210%'>" + lstVendor[0].DeliveryNoteNumber + " </span></td></tr> " +
                 "<tr><td><table width='100%' border='0'  BORDERCOLOR=black  >" +
                  "<tr><td class='style10' width='25%'>" + WebCommon.getGlobalResourceValue("Site") + "</td><td td width='25%'>" + lstVendor[0].Site.SiteName + "</td>" +
                  "<td  width='25%' class='style10'>" + WebCommon.getGlobalResourceValue("DateCreated") + "</td><td  width='25%'>" + Convert.ToDateTime(lstVendor[0].DiscrepancyLogDate.ToString()).ToString("dd/MM/yyyy") + "</td></tr>" +
                  "<tr><td class='style10'>" + WebCommon.getGlobalResourceValue("Type") + "</td><td>" + this.GetType(discrepancyTypeID) + " </td>" +
                  "<td class='style10'>" + WebCommon.getGlobalResourceValue("CreatedBy") + "</td><td>" + lstVendor[0].Username.ToString().Replace("-", " ").ToString() + "</td></tr>" +
                  "<tr><td>&nbsp;</td><td>&nbsp; </td>" +
                  "<td class='style10'>" + WebCommon.getGlobalResourceValue("SentTo") + "</td><td>" + strCommunicationTo + "</td></tr>" +
                 "</table></td></tr> " +

                 "<tr><td><table width='100%' border='0'  BORDERCOLOR=black  >" +
                  "<tr><td width='25%' class='style10'>" + WebCommon.getGlobalResourceValue("Vendor") + " #</td><td width='25%'>" + lstVendor[0].Vendor.Vendor_No + " - " + lstVendor[0].Vendor.VendorName + "</td>" +
                  "<td width='25%' class='style10'>" + WebCommon.getGlobalResourceValue("StockPlanner") + "</td><td width='25%'>" + lstVendor[0].StockPlannerName + "</td></tr>" +
                  "<tr><td class='style10'>" + WebCommon.getGlobalResourceValue("ReturnsAddress") + "</td><td>" + lstVendor[0].Vendor.address1 + " " + lstVendor[0].Vendor.address2 + " " + lstVendor[0].Vendor.city + " " + lstVendor[0].Vendor.county + " </td>" +
                  "<td class='style10'>" + WebCommon.getGlobalResourceValue("Tel") + " #</td><td>" + lstVendor[0].StockPlannerContact + "</td></tr>" +
                 "</table></td></tr> " + GetHtmlString(discrepancyTypeID, lstDetails) +


                @"<tr><td> <br /><br /><br /><br /><br /></td></tr>	" +
                "</table>");
                }
                else
                {
                    sb.Append("<table width='100%' border='1' style=' border-collapse: collapse;'  BORDERCOLOR=black  > " +
                  "<tr><td valign='top' ><div align='center' class='style1'>" + WebCommon.getGlobalResourceValue("Discrepancy") + " # - " + GetQueryStringValue("VDRNo") + " </div></td></tr> " +

                 "<tr><td valign='top' align='center' ><span class='style1'>" + WebCommon.getGlobalResourceValue("Location") + " - </span><span style='font-size:210%'>" + lstVendor[0].Location.ToString() + "</span></td></tr> " +
                 "<tr><td valign='top' align='center'><span align='center' class='style1'>" + WebCommon.getGlobalResourceValue("Vendor") + " # - </span><span style='font-size:210%'>" + lstVendor[0].Vendor.Vendor_No + " - " + lstVendor[0].Vendor.VendorName + " </span></td></tr> " +
                  "<tr><td valign='top' align='center'><span align='center' class='style1'>" + WebCommon.getGlobalResourceValue("PurchaseOrder") + " - </span><span style='font-size:210%'>" + Convert.ToString(lstVendor[0].PurchaseOrder.Purchase_order) + " </span></td></tr> " +
                  "<tr><td valign='top' align='center'><span align='center' class='style1'>" + WebCommon.getGlobalResourceValue("DeliveryNote") + " - </span><span style='font-size:210%'>" + lstVendor[0].DeliveryNoteNumber + " </span></td></tr> " +
                   "<tr><td><table width='100%' border='0'  BORDERCOLOR=black  >" +
                    "<tr><td class='style10' width='25%'>" + WebCommon.getGlobalResourceValue("Site") + "</td><td td width='25%'>" + lstVendor[0].Site.SiteName + "</td>" +
                    "<td  width='25%' class='style10'>" + WebCommon.getGlobalResourceValue("DateCreated") + "</td><td  width='25%'>" + Convert.ToDateTime(lstVendor[0].DiscrepancyLogDate.ToString()).ToString("dd/MM/yyyy") + "</td></tr>" +
                    "<tr><td class='style10'>" + WebCommon.getGlobalResourceValue("Type") + "</td><td>" + this.GetType(discrepancyTypeID) + " </td>" +
                    "<td class='style10'>" + WebCommon.getGlobalResourceValue("CreatedBy") + "</td><td>" + lstVendor[0].Username.ToString().Replace("-", " ").ToString() + "</td></tr>" +
                    "<tr><td>&nbsp;</td><td>&nbsp; </td>" +
                    "<td class='style10'>" + WebCommon.getGlobalResourceValue("SentTo") + "</td><td>" + strCommunicationTo + "</td></tr>" +
                   "</table></td></tr> " +

                   "<tr><td><table width='100%' border='0'  BORDERCOLOR=black  >" +
                    "<tr><td width='25%' class='style10'>" + WebCommon.getGlobalResourceValue("Vendor") + " #</td><td width='25%'>" + lstVendor[0].Vendor.Vendor_No + " - " + lstVendor[0].Vendor.VendorName + "</td>" +
                    "<td width='25%' class='style10'>" + WebCommon.getGlobalResourceValue("APClerk") + "</td><td width='25%'>" + lstVendor[0].AccountPaybleName + "</td></tr>" +
                    "<tr><td class='style10'>" + WebCommon.getGlobalResourceValue("ReturnsAddress") + "</td><td>" + lstVendor[0].Vendor.address1 + " " + lstVendor[0].Vendor.address2 + " " + lstVendor[0].Vendor.city + " " + lstVendor[0].Vendor.county + " </td>" +
                    "<td class='style10'>" + WebCommon.getGlobalResourceValue("Tel") + " #</td><td>" + lstVendor[0].AccountPayblePhoneNo + "</td></tr>" +
                   "</table></td></tr> " + GetHtmlString(discrepancyTypeID, lstDetails) +


                  @"<tr><td> <br /><br /><br /><br /><br /></td></tr>	" +
                  "</table>");
                }
                //*************

                if (iCount < Convert.ToInt32(txtNoofLabel.Text.Trim()))
                    sb.Append("<p style='page-break-before: always'></p>");

                ltlable.Text = sb.ToString();

                string a = "window.open('DIS_PrintLabel.aspx?RedirectionRequired=no&ltid=" + ltlable.ClientID + "', '', 'left =' + screen.width + ',top=' + screen.height + ',width=0,height=0,toolbar=0,scrollbars=0,status=0')";
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "pp", a, true);
            }
        }
    }
    protected string GetHtmlString(int discrepancyType, List<DiscrepancyBE> lstDetails)
    {
        string OurCode = WebCommon.getGlobalResourceValue("OurCode");
        string Description = WebCommon.getGlobalResourceValue("Description1");
        string VendorItemCode = WebCommon.getGlobalResourceValue("VendorItemCode");
        string OutstandingPOQty = WebCommon.getGlobalResourceValue("OutstandingPOQty");
        string DeliveryNoteQty = WebCommon.getGlobalResourceValue("DeliveryNoteQty");
        string DeliveredQty = WebCommon.getGlobalResourceValue("DeliveredQty");
        string QtyOverDelivered = WebCommon.getGlobalResourceValue("QtyOverDelivered");
        string ReceivedQuantity = WebCommon.getGlobalResourceValue("ReceivedQuantity1");
        string ShortageQuantity = WebCommon.getGlobalResourceValue("ShortageQuantity");
        string DamagedQuantity = WebCommon.getGlobalResourceValue("DamagedQuantity");
        string DamagedDescription = WebCommon.getGlobalResourceValue("DamagedDescription");
        string RequiredCode = WebCommon.getGlobalResourceValue("RequiredCode");
        string UOM = WebCommon.getGlobalResourceValue("UOM");
        string CodeReceived = WebCommon.getGlobalResourceValue("CodeReceived");
        string QuantityAdvisedOnDNote = WebCommon.getGlobalResourceValue("QuantityAdvisedOnDNote");
        string QuantityPaperworkAmendedto = WebCommon.getGlobalResourceValue("QuantityPaperworkAmendedto");
        string POPackSize = WebCommon.getGlobalResourceValue("POPackSize");
        string PackSizeofgooddelivered = WebCommon.getGlobalResourceValue("PackSizeofgooddelivered");
        string strLine = WebCommon.getGlobalResourceValue("Line");
        string strODCode = WebCommon.getGlobalResourceValue("ODCode");
        string strVikingCode = WebCommon.getGlobalResourceValue("VikingCode");
        string strOriginalPOQty = WebCommon.getGlobalResourceValue("OriginalPOQty");
        string strType = WebCommon.getGlobalResourceValue("Type");
        string strDiscrepancyQty = WebCommon.getGlobalResourceValue("DiscrepancyQty");
        string strHi = WebCommon.getGlobalResourceValue("Hi");
        string strShuttleDiscMessage = WebCommon.getGlobalResourceValue("ShuttleDiscMessage");
        string strShortage = WebCommon.getGlobalResourceValue("Shortage");
        string strManyThanks = WebCommon.getGlobalResourceValue("ManyThanks");
        string ChaseQuantity = WebCommon.getGlobalResourceValue("ChaseQuantity");
        string Comments = WebCommon.getGlobalResourceValue("Comments");
        string NoofPacksReceived = WebCommon.getGlobalResourceValue("NoofPacksReceived");
        string Suppliercode = WebCommon.getGlobalResourceValue("Suppliercode");
        StringBuilder sProductDetail = new StringBuilder();
        if (lstDetails != null && lstDetails.Count > 0)
        {
            sProductDetail.Append("<tr><td ><table width='100%' border='1' style=' border-collapse: collapse;'  BORDERCOLOR=black >");
            switch (discrepancyType)
            {

                case 1:
                    sProductDetail.Append("<tr><td width='15%' class='style10' style='font size:11px'>" + OurCode + "</td><td width='35%' class='style10' style='font size:11px'>" + Description + "</td><td width='10%' class='style10' style='font size:11px'>" + VendorItemCode + "</td><td  width='10%' class='style10' style='font size:11px'>" + OutstandingPOQty + "</td><td  width='10%' class='style10' style='font size:11px' >" + DeliveryNoteQty + "</td><td  width='10%' class='style10' style='font size:11px'>" + DeliveredQty + "</td><td  width='10%' class='style10' style='font size:11px'>" + QtyOverDelivered + "</td></tr>");
                    for (int i = 0; i < lstDetails.Count; i++)
                    {
                        sProductDetail.Append("<tr><td style='font size:9px'>" + lstDetails[i].ODSKUCode + "</td><td style='font size:9px'>" + lstDetails[i].ProductDescription + "</td><td style='font size:9px'>" + lstDetails[i].VendorCode);
                        sProductDetail.Append("</td><td style='font size:9px'>" + lstDetails[i].OutstandingQuantity + "</td><td style='font size:9px'>" + lstDetails[i].DeliveredNoteQuantity + "</td><td style='font size:9px'>" + lstDetails[i].DeliveredQuantity + "</td>");
                        sProductDetail.Append("<td style='font size:9px'>" + lstDetails[i].OversQuantity + "</td></tr>");
                    }
                    //sDiscrepancyRelatedtext = "Product received against the above delivery were over delivered against our Purchase Order as detailed below";
                    //sLine1 = "Please contact the undersigned urgently so that resolution on this issue can be reached.<br /><br />In all communication please ensure that you quote the Discrepancy Report Number.";                    
                    break;
                case 2:
                    sProductDetail.Append("<tr><td width='15%' class='style10'>" + OurCode + "</td><td width='35%' class='style10'> " + Description + "</td><td width='10%' class='style10'>" + VendorItemCode + "</td><td  width='10%' class='style10'>" + OutstandingPOQty + "</td><td  width='10%' class='style10'>" + DeliveryNoteQty + "</td><td  width='10%' class='style10'>" + ReceivedQuantity + "</td><td  width='10%' class='style10'>" + ShortageQuantity + "</td></tr>");
                    for (int i = 0; i < lstDetails.Count; i++)
                    {
                        decimal dReceivedQuantity = (lstDetails[i].DeliveredNoteQuantity ?? 0) - (lstDetails[i].ShortageQuantity ?? 0);
                        sProductDetail.Append("<tr><td>" + lstDetails[i].ODSKUCode + "</td><td>" + lstDetails[i].ProductDescription + "</td><td>" + lstDetails[i].VendorCode);
                        sProductDetail.Append("</td><td>" + lstDetails[i].OutstandingQuantity + "</td><td>" + lstDetails[i].DeliveredNoteQuantity + "</td><td>" + dReceivedQuantity + "</td><td>" + lstDetails[i].ShortageQuantity + "</td></tr>");
                    }
                    //sDiscrepancyRelatedtext = "Product received against the above delivery note were short delivered as per the following details.";
                    //sLine1 = "In this first instance please contact the undersigned urgently so that they can advise as to whether the short delivered quantities are still required.<br /><br />Please ensure that you quote the Discrepancy Report Number in all communication relating to this issue.";
                    break;
                case 3:
                    sProductDetail.Append("<tr><td width='15%' class='style10'>" + OurCode + "</td><td width='35%' class='style10'>" + Description + "</td><td width='10%' class='style10'>" + VendorItemCode + "</td><td  width='10%' class='style10'>" + OutstandingPOQty + "</td><td  width='10%' class='style10'>" + DamagedQuantity + "</td><td  width='10%' class='style10'>" + DamagedDescription + "</td></tr>");
                    for (int i = 0; i < lstDetails.Count; i++)
                    {
                        sProductDetail.Append("<tr><td>" + lstDetails[i].ODSKUCode + "</td><td>" + lstDetails[i].ProductDescription + "</td><td>" + lstDetails[i].VendorCode);
                        sProductDetail.Append("</td><td>" + lstDetails[i].OutstandingQuantity + "</td><td>" + lstDetails[i].DamageQuantity + "</td><td>" + lstDetails[i].DamageDescription + "</td></tr>");
                    }
                    //sDiscrepancyRelatedtext = "Product received against the above delivery note were damaged as detailed below.";
                    //sLine1 = "In this first instance please contact the undersigned urgently so that they can advise as to whether the damaged quantities are still required.<br /><br />Please ensure that you quote the Discrepancy Report Number in all communication relating to this issue.";                    
                    break;
                case 4:
                    sProductDetail.Append("<tr><td width='15%' class='style10'>" + OurCode + "</td><td width='35%' class='style10'>" + Description + "</td><td width='10%' class='style10'>" + VendorItemCode + "</td><td  width='10%' class='style10'>" + ReceivedQuantity + "</td></tr>");
                    for (int i = 0; i < lstDetails.Count; i++)
                    {
                        sProductDetail.Append("<tr><td>" + lstDetails[i].ODSKUCode + "</td><td>" + lstDetails[i].ProductDescription + "</td><td>" + lstDetails[i].VendorCode);
                        sProductDetail.Append("</td><td>" + lstDetails[i].DeliveredQuantity + "</td></tr>");
                    }
                    //sDiscrepancyRelatedtext = "The above delivery did not quote the Office Depot Purchase Order on the documentation." + "<br /><br /> We are unable to receipt stock without a valid Purchase Order Number. Please contact the undersigned <u><b>urgently</b></u> so that we can confirm the correct Purchase Order Number.<br /><br />Please ensure that the invoice relating to this delivery quotes our Purchase Order Number and that when referencing this discrepancy please ensure that the Discrepancy number is quoted" + "<br /><br />Please find detail of what was received catalogued below<br /><br />";
                    break;
                case 6:
                    sProductDetail.Append("<tr><td width='10%'  class='style10'>" + RequiredCode + "</td><td width='20%'  class='style10'>" + Description + "</td><td width='10%'  class='style10'>" + VendorItemCode + "</td><td  width='10%'  class='style10'>" + UOM + "</td><td  width='10%'  class='style10'>" + CodeReceived + "</td><td  width='10%'  class='style10'>" + Description + "</td><td width='10%'  class='style10'>" + VendorItemCode + "</td><td  width='10%'  class='style10'>" + ReceivedQuantity + "</td><td width='10%'  class='style10'>" + UOM + "</td></tr>");
                    for (int i = 0; i < lstDetails.Count; i++)
                    {
                        sProductDetail.Append("<tr><td>" + lstDetails[i].ODSKUCode + "</td><td>" + lstDetails[i].ProductDescription + "</td><td>" + lstDetails[i].VendorCode + "</td><td>" + lstDetails[i].UOM + "</td><td>" + lstDetails[i].CorrectODSKUCode + "</td><td>" + lstDetails[i].CorrectProductDescription + "</td><td>" + lstDetails[i].CorrectVendorCode + "</td><td>" + lstDetails[i].DeliveredQuantity + "</td><td>" + lstDetails[i].CorrectUOM + "</td></tr>");
                    }
                    // sDiscrepancyRelatedtext = "The above delivery contained incorrect products as per our Purchase Order Number, details as follows:";
                    //sLine1 = "Please contact the undersigned urgently so that resolution on this issue can be reached.<br /><br />In all communication please ensure that you quote the Discrepancy Report Number.";                    
                    break;
                case 9:
                    //if (getCommunicationWithEscalation(lstDisLog[0].SiteID))
                    //{
                    //    // This is for escalation    
                    //    sProductDetail.Append("<tr style='color:#934500;border-bottom: 1px solid #E8E8E8;font-weight: bold;font-family:Arial ;font-size:12;vertical-align: top;'><td width='10%'>" + OurCode + "</td><td width='35%'>" + Description + "</td><td width='10%'>" + VendorItemCode + "</td><td  width='10%'>" + QuantityAdvisedOnDNote + "</td><td  width='10%'>" + QuantityPaperworkAmendedto + "</td></tr>");
                    //    for (int i = 0; i < lstDetails.Count; i++)
                    //    {
                    //        sProductDetail.Append("<tr style='background-color: #F7F6F3;font-size:12;font-family:Arial ;'><td>" + lstDetails[i].ODSKUCode + "</td><td>" + lstDetails[i].ProductDescription + "</td><td>" + lstDetails[i].VendorCode + "</td><td>" + lstDetails[i].DeliveredNoteQuantity + "</td><td>" + lstDetails[i].AmendedQuantity + "</td></tr>");
                    //    }
                    //}
                    //else
                    //{
                    // This is no escalation
                    sProductDetail.Append("<tr><td width='10%' class='style10'>" + OurCode + "</td><td width='35%' class='style10'>" + Description + "</td><td width='10%' class='style10'>" + VendorItemCode + "</td><td  width='10%' class='style10'>" + QuantityAdvisedOnDNote + "</td><td  width='10%' class='style10'>" + QuantityPaperworkAmendedto + "</td></tr>");
                    for (int i = 0; i < lstDetails.Count; i++)
                    {
                        sProductDetail.Append("<tr><td>" + lstDetails[i].ODSKUCode + "</td><td>" + lstDetails[i].ProductDescription + "</td><td>" + lstDetails[i].VendorCode + "</td><td>" + lstDetails[i].DeliveredNoteQuantity + "</td><td>" + lstDetails[i].AmendedQuantity + "</td></tr>");
                    }
                    //sDiscrepancyRelatedtext = "The above delivery paperwork was amended. Please find details catalogued below.";
                    //sLine1 = "Please ensure that the associated invoice is for the amended quantity, as this is what was physically received and receipted.";
                    // }
                    break;
                case 10:
                    sProductDetail.Append("<tr><td width='15%' class='style10'>" + OurCode + "</td><td width='40%' class='style10'>" + Description + "</td><td width='15%' class='style10'>" + VendorItemCode + "</td><td  width='15%' class='style10'>" + POPackSize + "</td><td  width='15%' class='style10'>" + WebCommon.getGlobalResourceValue("PackSizeofgooddeliveredNew") + "</td><td  width='15%' class='style10'>" + NoofPacksReceived + "</td><td  width='15%' class='style10'>" + WebCommon.getGlobalResourceValue("#Units") + "</td></tr>");
                    for (int i = 0; i < lstDetails.Count; i++)
                    {
                        sProductDetail.Append("<tr><td>" + lstDetails[i].ODSKUCode + "</td><td>" + lstDetails[i].ProductDescription + "</td><td>" + lstDetails[i].VendorCode + "</td><td>" + lstDetails[i].PackSizeOrdered + "</td><td>" + lstDetails[i].PackSizeReceived + "</td><td>" + lstDetails[i].WrongPackReceived + "</td><td>" + Convert.ToInt32(lstDetails[i].PackSizeReceived) * lstDetails[i].WrongPackReceived + "</td></tr>");
                    }
                    //sDiscrepancyRelatedtext = "The above delivery contained different pack sizes from those shown on our purchase order as detailed below.";
                    //sLine1 = "Please contact the undersigned to discuss these differences, in order that we may amend our records if necessary.";
                    break;
                case 5:
                    sProductDetail.Append("<tr><td width='15%' class='style10'>" + OurCode + "</td><td width='40%' class='style10'>" + Description + "</td><td width='15%' class='style10'>" + VendorItemCode + "</td><td  width='15%' class='style10'>" + ReceivedQuantity + "</td></tr>");
                    for (int i = 0; i < lstDetails.Count; i++)
                    {
                        sProductDetail.Append("<tr><td>" + lstDetails[i].ODSKUCode + "</td><td>" + lstDetails[i].ProductDescription + "</td><td>" + lstDetails[i].VendorCode + "</td><td>" + lstDetails[i].DeliveredQuantity + "</td></tr>");
                    }
                    break;
                case 13: //invoice
                    sProductDetail.Append("<tr><td width='15%' class='style10'>" + Comments + "</td><td>" + lstDetails[0].Comment + "</td></tr>");
                    //sProductDetail.Append("<tr><td width='15%' class='style10'>" + OurCode + "</td><td width='40%' class='style10'>" + Description + "</td><td width='15%' class='style10'>" + VendorItemCode + "</td><td  width='15%' class='style10'>" + ChaseQuantity + "</td></tr>");
                    //for (int i = 0; i < lstDetails.Count; i++)
                    //{
                    //    sProductDetail.Append("<tr><td>" + lstDetails[i].ODSKUCode + "</td><td>" + lstDetails[i].ProductDescription + "</td><td>" + lstDetails[i].VendorCode + "</td><td>" + lstDetails[i].ChaseQuantity + "</td></tr>");
                    //}
                    break;
                case 15:
                    sProductDetail.Append("<tr><td width='5%' class='style10'>"
                        + strLine + "</td><td width='10%' class='style10'>" + strODCode
                        + "</td><td width='10%' class='style10'>" + strVikingCode + "</td><td width='30%' class='style10'>" + Description + "</td><td width='10%' class='style10'>"
                        + strOriginalPOQty + "</td><td  width='10%' class='style10'>" + OutstandingPOQty + "</td><td  width='5%' class='style10'>" + UOM
                        + "</td><td  width='15%' class='style10'>" + strType + "</td><td  width='10%' class='style10'>" + strDiscrepancyQty + "</td></tr>");
                    for (int i = 0; i < lstDetails.Count; i++)
                    {
                        sProductDetail.Append("<tr><td>"
                            + lstDetails[i].Line_no + "</td><td>" + lstDetails[i].ODSKUCode + "</td><td>" + lstDetails[i].DirectCode);
                        sProductDetail.Append("</td><td>" + lstDetails[i].ProductDescription + "</td><td>" + lstDetails[i].OriginalQuantity + "</td><td>"
                            + lstDetails[i].OutstandingQuantity + "</td><td>" + lstDetails[i].UOM + "</td><td>" + lstDetails[i].ShuttleType
                            + "</td><td>" + lstDetails[i].DiscrepancyQty + "</td>");
                        sProductDetail.Append("</tr>");
                    }
                    break;
                case 14:
                    sProductDetail.Append("<tr><td width='15%' class='style10'>" + Comments + "</td><td>" + lstDetails[0].Comment + "</td></tr>");
                    break;
                case 7:
                    sProductDetail.Append("<tr><td width='15%' class='style10'>" + Comments + "</td><td>" + lstDetails[0].Comment + "</td></tr>");
                    break;
                case 8:
                    sProductDetail.Append("<tr><td width='15%' class='style10'>" + Comments + "</td><td>" + lstDetails[0].Comment + "</td></tr>");
                    break;
                case 11:
                    sProductDetail.Append("<tr><td width='15%' class='style10'>" + Comments + "</td><td>" + lstDetails[0].Comment + "</td></tr>");
                    break;
                case 12:
                    if (string.IsNullOrEmpty(Convert.ToString(lstDetails[0].OriginalQuantity)))
                    {
                        sProductDetail.Append("<tr><td width='15%' class='style10'>" + Comments + "</td><td>" + lstDetails[0].Comment + "</td></tr>");
                    }
                    else
                    {
                        sProductDetail.Append("<tr><td width='15%' class='style10' style='font size:11px'>" + OurCode + "</td><td width='35%' class='style10' style='font size:11px'>" + Description + "</td><td width='10%' class='style10' style='font size:11px'>" + VendorItemCode + "</td><td  width='10%' class='style10' style='font size:11px'>" + OutstandingPOQty + "</td><td  width='10%' class='style10' style='font size:11px' >" + strOriginalPOQty + "</td><td  width='10%' class='style10' style='font size:11px'>" + UOM + "</td>"

                            + "</td><td  width='10%' class='style10' style='font size:11px'>" + DeliveredQty + "</td></tr>");
                        for (int i = 0; i < lstDetails.Count; i++)
                        {
                            sProductDetail.Append("<tr><td style='font size:9px'>" + lstDetails[i].ODSKUCode + "</td><td style='font size:9px'>" + lstDetails[i].ProductDescription + "</td><td style='font size:9px'>" + lstDetails[i].VendorCode);
                            sProductDetail.Append("</td><td style='font size:9px'>" + lstDetails[i].OutstandingQuantity + "</td><td style='font size:9px'>" + lstDetails[i].OriginalQuantity + "</td><td style='font size:9px'>" + lstDetails[i].UOM + "</td><td style='font size:9px'>" + lstDetails[i].DeliveredQuantity + "</td></tr>");
                        }
                    }
                    // break;
                    break;
                case 16:
                    sProductDetail.Append("<tr><td width='15%' class='style10'>" + OurCode + "</td><td width='40%' class='style10'>" + Description + "</td><td width='15%' class='style10'>" + VendorItemCode + "</td><td  width='15%' class='style10'>" + ReceivedQuantity + "</td></tr>");
                    for (int i = 0; i < lstDetails.Count; i++)
                    {
                        sProductDetail.Append("<tr><td>" + lstDetails[i].ODSKUCode + "</td><td>" + lstDetails[i].ProductDescription + "</td><td>" + lstDetails[i].VendorCode + "</td><td>" + lstDetails[i].DeliveredQuantity + "</td></tr>");
                    }
                    break;
                case 17:
                    sProductDetail.Append("<tr><td width='15%' class='style10'>" + OurCode + "</td><td width='40%' class='style10'>" + Description + "</td><td width='15%' class='style10'>" + VendorItemCode + "</td><td  width='10%' class='style10' style='font size:11px'>" + OutstandingPOQty + "</td><td  width='15%' class='style10'>" + ChaseQuantity + "</td></tr>");
                    for (int i = 0; i < lstDetails.Count; i++)
                    {
                        sProductDetail.Append("<tr><td>" + lstDetails[i].ODSKUCode + "</td><td>" + lstDetails[i].ProductDescription + "</td><td>" + lstDetails[i].VendorCode + "</td><td>" + lstDetails[i].OutstandingQuantity + "</td><td>" + lstDetails[i].ChaseQuantity + "</td></tr>");
                    }
                    break;

                case 18:
                    sProductDetail.Append("<tr><td width='15%' class='style10'>" + OurCode + "</td><td width='40%' class='style10'>" + Description + "</td><td width='15%' class='style10'>" + VendorItemCode + "</td><td  width='10%' class='style10' style='font size:11px'>" + OutstandingPOQty + "</td><td  width='15%' class='style10'>" + ChaseQuantity + "</td></tr>");
                    for (int i = 0; i < lstDetails.Count; i++)
                    {
                        sProductDetail.Append("<tr><td>" + lstDetails[i].ODSKUCode + "</td><td>" + lstDetails[i].ProductDescription + "</td><td>" + lstDetails[i].VendorCode + "</td><td>" + lstDetails[i].OutstandingQuantity + "</td><td>" + lstDetails[i].ChaseQuantity + "</td></tr>");
                    }
                    break;

                case 19:
                    sProductDetail.Append("<tr><td width='15%' class='style10'>" + OurCode + "</td><td width='40%' class='style10'>" + Description + "</td><td width='15%' class='style10'>" + VendorItemCode + "</td><td  width='10%' class='style10' style='font size:11px'>" + WebCommon.getGlobalResourceValue("ReceiptedQty") + "</td><td  width='15%' class='style10'>" + WebCommon.getGlobalResourceValue("InvoiceQTY") + "</td></tr>");
                    for (int i = 0; i < lstDetails.Count; i++)
                    {
                        sProductDetail.Append("<tr><td>" + lstDetails[i].ODSKUCode + "</td><td>" + lstDetails[i].ProductDescription + "</td><td>" + lstDetails[i].VendorCode + "</td><td>" + lstDetails[i].ReceiptedQty + "</td><td>" + lstDetails[i].InvoiceQty + "</td></tr>");
                    }
                    break;
                case 20:
                    sProductDetail.Append("<tr><td width='15%' class='style10'>" + OurCode + "</td><td width='10%' class='style10'>"
                        + strVikingCode + "</td><td width='15%' class='style10'>" + Suppliercode + "</td><td width='40%' class='style10'>"
                        + Description + "</td><td  width='15%' class='style10'>" + ReceivedQuantity + "</td><td  width='15%' class='style10'>"
                        + UOM + "</td></tr>");
                    for (int i = 0; i < lstDetails.Count; i++)
                    {
                        sProductDetail.Append("<tr><td>" + lstDetails[i].ODSKUCode + "</td><td>"
                            + lstDetails[i].DirectCode + "</td><td>" + lstDetails[i].VendorCode + "</td><td>"
                            + lstDetails[i].ProductDescription + "</td><td>" + lstDetails[i].DeliveredQuantity + "</td><td>" + lstDetails[i].UOM + "</td></tr>");
                    }
                    break;
            }
            sProductDetail.Append("</table></td></tr>");
        }
        return sProductDetail.ToString();
    }
    protected void BtnAddComment_Click(object sender, EventArgs e)
    {
        txtUserComments.Text = string.Empty;
        lblErrorMsg.Text = string.Empty;
        lblUserT.Text = Convert.ToString(Session["UserName"]);
        lblDateT.Text = DateTime.Now.Date.ToString("dd/MM/yyyy");
        lblDiscrepancyNoT.Text = lblVDRNo.Text.Replace("-", string.Empty);
        MdlAddComment.Show();
        txtUserComments.Focus();
    }
    protected void BtnAddImages_Click(object sender, EventArgs e)
    {
        DiscrepancyBE oNewDiscrepancyBE = new DiscrepancyBE();
        DiscrepancyBAL oNewDiscrepancyBAL = new DiscrepancyBAL();
        oNewDiscrepancyBE.Action = "GetDiscrepancySiteVendorDetail";
        oNewDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID"));

        List<DiscrepancyBE> lstVendor = oNewDiscrepancyBAL.GetDiscrepancyLogSiteVendorDetailsBAL(oNewDiscrepancyBE);
        int discrepancyTypeID = lstVendor[0].DiscrepancyTypeID != null ? Convert.ToInt32(lstVendor[0].DiscrepancyTypeID) : 0;

        EncryptQueryString("DIS_OptionsPopup.aspx?Mode=Edit&VDRNo=" + GetQueryStringValue("VDRNo").ToString() + "&DiscrepancyLogID=" + GetQueryStringValue("disLogID").ToString() + "&CommunicationType=" + GetType(discrepancyTypeID));

    }
    protected void BtnSave_Click(object sender, EventArgs e)
    {
        DiscrepancyBE discrepancyBE = null;
        DiscrepancyBAL discrepancyBAL = null;
        if (!string.IsNullOrEmpty(txtUserComments.Text) && !string.IsNullOrWhiteSpace(txtUserComments.Text))
        {
            lblErrorMsg.Text = string.Empty;
            discrepancyBE = new DiscrepancyBE
            {
                Action = "AddDiscrepancyLogComment",
                DiscrepancyLogID = !string.IsNullOrEmpty(GetQueryStringValue("disLogID")) ? Convert.ToInt32(GetQueryStringValue("disLogID")) : 0,
                UserID = Convert.ToInt32(Session["UserID"]),
                CommentDateTime = DateTime.Now,
                Comment = txtUserComments.Text
            };

            discrepancyBAL = new DiscrepancyBAL();
            discrepancyBAL.AddDiscrepancyLogCommentBAL(discrepancyBE);

            discrepancyBE.Action = "GetDiscrepancyLogComment";
            this.BindDiscrepancyLogComment(discrepancyBE);

            if (DiscrepancyTypeID != null && DiscrepancyTypeID > 0)
                discrepancyBE.DiscrepancyType = GetDiscrepancyType(DiscrepancyTypeID.Value);

            if (VendorID != null && VendorID > 0)
                new SendCommentMail().SendCommentsMailWorkFlow(discrepancyBE, VendorEmailIds,VendorID.Value,txtUserComments.Text, Session["Role"].ToString());

            MdlAddComment.Hide();
        }
        else
        {
            lblErrorMsg.Text = WebCommon.getGlobalResourceValue("MustEnterComment");
            MdlAddComment.Show();
        }
    }
    protected void BtnBack_Click(object sender, EventArgs e)
    {
        MdlAddComment.Hide();
    }
    protected void BtnBackToPreviousPage_Click(object sender, EventArgs e)
    {
        if (GetQueryStringValue("PN") != null)
        {
            if (GetQueryStringValue("PN").Trim().ToUpper().Equals("POINQ"))
            {
                EncryptQueryString("~/ModuleUI/Appointment/PurchaseOrderInquiry.aspx?SId=" + GetQueryStringValue("SId").ToString()
                 + "&Status=" + GetQueryStringValue("Status").ToString()
                 + "&PN=DISLOG"
                 + "&PO=" + GetQueryStringValue("PO").ToString());
            }
        }
        if (GetQueryStringValue("INVoiceQ") != null && GetQueryStringValue("INVoiceQ") == "InvoiceSearchResult")
        {
            //For opening search discrepancies mode

            ScriptManager.RegisterClientScriptBlock((sender as Control), this.GetType(), "script", "window.close()", true);
            return;

        }

        if (Session["PageMode"] != null && Session["PageMode"].ToString() == "Search")
        {
            //For opening search discrepancies mode

            EncryptQueryString("~/ModuleUI/Discrepancy/DIS_SearchResult.aspx?disLogID=" + Convert.ToString(GetQueryStringValue("disLogID"))
                          + "&VDRNo=" + Convert.ToString(GetQueryStringValue("VDRNo"))
                          + "&UserID=" + Convert.ToString(GetQueryStringValue("UserID"))
                          + "&PreviousPage=SearchResult");

        }

        else if (Session["PageMode"] != null && Session["PageMode"].ToString() == "Todays")
        {      //For opening Today's Discrepancy Mode
            if (GetQueryStringValue("SiteId") != null && GetQueryStringValue("DisDate") != null)
            {
                EncryptQueryString("../DIS_SearchResult.aspx?FromPage=images&SiteId=" + GetQueryStringValue("SiteId") + "&DisDate=" + GetQueryStringValue("DisDate"));
            }

        }
        else
        {
            EncryptQueryString("~/ModuleUI/Discrepancy/DISLog_QueryDiscrepancy.aspx?disLogID=" + Convert.ToString(GetQueryStringValue("disLogID"))
                            + "&VDRNo=" + Convert.ToString(GetQueryStringValue("VDRNo")) + "&Mode=Readonly");
        }
    }
    private void BindDiscrepancyLogComment(DiscrepancyBE discrepancyBE = null)
    {
        var discrepancyBAL = new DiscrepancyBAL();
        if (discrepancyBE != null)
        {
            rptDiscrepancyComments.DataSource = discrepancyBAL.GetDiscrepancyLogCommentsBAL(discrepancyBE);
        }
        else
        {
            discrepancyBE = new DiscrepancyBE();
            discrepancyBE.Action = "GetDiscrepancyLogComment";

            if (!string.IsNullOrEmpty(GetQueryStringValue("disLogID")))
                discrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID"));
            else
                discrepancyBE.DiscrepancyLogID = 0;

            rptDiscrepancyComments.DataSource = discrepancyBAL.GetDiscrepancyLogCommentsBAL(discrepancyBE);
        }

        rptDiscrepancyComments.DataBind();

        if (rptDiscrepancyComments.Items.Count > 0)
            lblComments.Visible = true;
        else
            lblComments.Visible = false;
    }
    private void BindDiscrepancyQuery()
    {
        var queryDiscrepancyBAL = new DISLog_QueryDiscrepancyBAL();
        var queryDiscrepancyBE = new DISLog_QueryDiscrepancyBE();
        var disLogID = GetQueryStringValue("disLogID") != null ? Convert.ToInt32(GetQueryStringValue("disLogID")) : 0;
        queryDiscrepancyBE.Action = "GetQueryDiscrepancy";
        queryDiscrepancyBE.DiscrepancyLogID = disLogID;
        rptDiscrepancyQuery.DataSource = queryDiscrepancyBAL.GetQueryDiscrepancyBAL(queryDiscrepancyBE);
        rptDiscrepancyQuery.DataBind();

        if (rptDiscrepancyQuery.Items.Count > 0)
            lblDiscrepancyQuery.Visible = true;
        else
            lblDiscrepancyQuery.Visible = false;
    }
    protected void RptDiscrepancyQuery_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            var hdnIsQueryClosedManually = (HiddenField)e.Item.FindControl("hdnIsQueryClosedManually");

            // GoodsIn declaration ...            
            var lblCommentLabelT_2 = (ucLabel)e.Item.FindControl("lblCommentLabelT_2");
            if (lblCommentLabelT_2 != null)
            {
                if (!string.IsNullOrEmpty(lblCommentLabelT_2.Text)) { e.Item.FindControl("tblGoodsIn").Visible = true; }
                else { e.Item.FindControl("tblGoodsIn").Visible = false; }

                if (!string.IsNullOrEmpty(lblCommentLabelT_2.Text) && hdnIsQueryClosedManually.Value == "True")
                {
                    e.Item.FindControl("tblGoodsIn").Visible = false;
                    e.Item.FindControl("tblQueryClosedManually").Visible = true;
                }

                var lnkView = (System.Web.UI.HtmlControls.HtmlAnchor)e.Item.FindControl("lnkView");
                var hdnQueryDiscrepancyID = (HiddenField)e.Item.FindControl("hdnQueryDiscrepancyID");
                var hdnDiscrepancyLogID = (HiddenField)e.Item.FindControl("hdnDiscrepancyLogID");
                var hdndiscrepancyCommunicationID = (HiddenField)e.Item.FindControl("hdnCommunicationId");
                var hdncommunicationLevel = (HiddenField)e.Item.FindControl("hdnCommunicationLevel");
                //lnkView.Attributes.Add("href", EncryptQuery("~/ModuleUI/Appointment/Receiving/APPRcv_Communication.aspx?DiscId=" + hdnDiscrepancyLogID.Value
                //    + "&QueryDiscId=" + hdnQueryDiscrepancyID.Value));
                lnkView.Attributes.Add("href", EncryptQuery("~/ModuleUI/Discrepancy/LogDiscrepancy/DISLog_ReSendCommunication.aspx?communicationid=" + hdndiscrepancyCommunicationID.Value
                   + "&CommunicationLevel=" + hdncommunicationLevel.Value + "&IsFromQuery=true"));
            }
        }
    }
    protected void RpWorkflow_OnItemDataBound(object sender, RepeaterItemEventArgs e)
    {

        if (e.Item.ItemType == ListItemType.Header)
        {
            Label lblHeader = e.Item.FindControl("lblGoodsIn") as Label;
            if (lblHeader != null)
            {
                lblHeader.Text = WebCommon.getGlobalResourceValue("GoodsIn");
            }
            Label lblInventory = e.Item.FindControl("lblInventory") as Label;
            if (lblInventory != null)
            {
                lblInventory.Text = WebCommon.getGlobalResourceValue("Inventory");
            }
            Label lblAccountPayable = e.Item.FindControl("lblAccountPayable") as Label;
            if (lblAccountPayable != null)
            {
                lblAccountPayable.Text = WebCommon.getGlobalResourceValue("AccountPayable");
            }
            Label lblVendor = e.Item.FindControl("lblVendor") as Label;
            if (lblVendor != null)
            {
                lblVendor.Text = WebCommon.getGlobalResourceValue("Vendor");
            }
        }


        if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
        {
            Label lblMEDHTML1 = e.Item.FindControl("lblMEDHTML1") as Label;
            Label lblMEDHTML2 = e.Item.FindControl("lblMEDHTML2") as Label;

            if (Convert.ToString(DataBinder.Eval(e.Item.DataItem, "MEDHTML")) != "&nbsp;")
            {
                if (count == 1)
                {
                    lblMEDHTML1.Visible = false;
                    lblMEDHTML2.Visible = true;
                }
                else if (count == 2)
                {
                    lblMEDHTML1.Visible = true;
                    lblMEDHTML2.Visible = false;
                }
                count = count + 1;
            }
        }
    }
    private void SetDebitCreditPosting()
    {
        var discrepancyBE = new DiscrepancyBE();
        var discrepancyBAL = new DiscrepancyBAL();

        #region Being set the Raised & Cancelled Information ...
        discrepancyBE.Action = "GetAllDebitRaise";
        if (!string.IsNullOrEmpty(GetQueryStringValue("disLogID")))
            discrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID"));
        else
            discrepancyBE.DiscrepancyLogID = 0;

        var findDiscrepancy = discrepancyBAL.GetAllDebitRaiseBAL(discrepancyBE).Find(x => x.APActionRef.Equals("ACP001"));
        if (findDiscrepancy != null)
        {
            pnlDebitCreditPosting.Visible = true;
            lblDebitCreditPosting.Visible = true;
            if (findDiscrepancy.DebitRaiseDate != null)
            {
                DateTime actualDate = Convert.ToDateTime(findDiscrepancy.DebitRaiseDate);
                lblDateTimeValue.Text = string.Format("{0} {1}", Common.ToDateTimeInMMDDYYYY(actualDate), actualDate.ToString("hh:mm"));
            }

            /* Section-1 : Debit / Credit Posting */
            if (!string.IsNullOrEmpty(findDiscrepancy.CreatedBy) && !string.IsNullOrWhiteSpace(findDiscrepancy.CreatedBy))
            {
                pnlRaisedDebit.Visible = true;
                lblCreatedByValue.Text = findDiscrepancy.CreatedBy;
                lblActionTakenValue.Text = DebitRaisedText;
                lblDNNValue.Text = findDiscrepancy.DebitNoteNo;
                lblReasonValue.Text = findDiscrepancy.Reason;
                lblValuePostedValue.Text = Convert.ToString(findDiscrepancy.StateValueDebited);
                lblCurrencyValue.Text = findDiscrepancy.Currency.CurrencyName;
                lblSentToValue.Text = findDiscrepancy.CommEmailsWithLink;
            }

            /* Section-3 : Debit / Credit Posting */
            if (!string.IsNullOrEmpty(findDiscrepancy.CancelBy) && !string.IsNullOrWhiteSpace(findDiscrepancy.CancelBy))
            {
                pnlCancelDebit.Visible = true;
                lblCancelledDebitNoteReferenceValue.Text = findDiscrepancy.DebitNoteNo;
                lblCancelledByValue.Text = findDiscrepancy.CancelBy;
                if (findDiscrepancy.CanceledDate != null)
                {
                    DateTime actualCancelDate = Convert.ToDateTime(findDiscrepancy.CanceledDate);
                    lblCancelledOnValue.Text = string.Format("{0} {1}", Common.ToDateTimeInMMDDYYYY(actualCancelDate), actualCancelDate.ToString("hh:mm"));
                }
                lblCancelledSentToValue.Text = findDiscrepancy.CancelCommEmailsWithLink;
            }

            ///* Section-2 : Debit / Credit Posting */
            #region Being set the Debit Re-Sent Information

            discrepancyBE = new DiscrepancyBE();
            discrepancyBE.Action = "GetDebitResentCommunication";
            discrepancyBE.DebitRaiseId = findDiscrepancy.DebitRaiseId;
            var lstReSentDebits = discrepancyBAL.GetDebitResentCommunicationBAL(discrepancyBE);
            if (lstReSentDebits != null && lstReSentDebits.Count > 0)
            {
                rptDebitResent.DataSource = lstReSentDebits;
                rptDebitResent.DataBind();
            }
            #endregion
        }
        else
        {
            pnlDebitCreditPosting.Visible = false;
            lblDebitCreditPosting.Visible = false;
            lblDateTimeValue.Text = string.Empty;
            lblCreatedByValue.Text = string.Empty;
            lblActionTakenValue.Text = string.Empty;
            lblDNNValue.Text = string.Empty;
            lblReasonValue.Text = string.Empty;
            lblValuePostedValue.Text = string.Empty;
            lblCurrencyValue.Text = string.Empty;
            lblSentToValue.Text = string.Empty;
            lblCancelledDebitNoteReferenceValue.Text = string.Empty;
            lblCancelledByValue.Text = string.Empty;
            lblCancelledOnValue.Text = string.Empty;
            lblCancelledSentToValue.Text = string.Empty;

            pnlRaisedDebit.Visible = false;
            pnlCancelDebit.Visible = false;
            rptDebitResent.DataSource = null;
            rptDebitResent.DataBind();
        }
        #endregion
    }
    private void ShowACP001ActionButton(int disLogID)
    {
        var oDiscrepancyBE = new DiscrepancyBE();
        var oDiscrepancyBAL = new DiscrepancyBAL();
        oDiscrepancyBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
        oDiscrepancyBE.DiscrepancyLogID = disLogID;

        #region Commented code ...
        //string CaseOf = String.Empty;
        //if (GetQueryStringValue("RoleTypeFlag") == "A" || GetQueryStringValue("RoleTypeFlag") == null)
        //{
        //    if (Session["UserID"] == null || Session["UserID"].ToString() == "" || Session["Role"] == null || Session["Role"].ToString().Trim() == "")
        //        return;

        //    oDiscrepancyBE.User.UserID = Convert.ToInt32(Session["UserID"]);
        //    CaseOf = Session["Role"].ToString().Trim();
        //}
        ////else
        ////{
        ////    switch (GetQueryStringValue("RoleTypeFlag").ToString())
        ////    {
        ////        case "G":
        ////            CaseOf = "OD - Goods In";
        ////            break;
        ////        case "S":
        ////            CaseOf = "OD - Stock Planner";
        ////            break;
        ////        case "P":
        ////            CaseOf = "OD - Accounts Payable";
        ////            break;
        ////        case "V":
        ////            CaseOf = "Vendor";
        ////            break;
        ////    }
        ////}

        ///* Here P is an "OD - Accounts Payable" When from drop down selected the Account Payble option */
        ///* Here A will come when actual Account Payble type user will loggedin. */
        //if (GetQueryStringValue("RoleTypeFlag") == "P" || GetQueryStringValue("RoleTypeFlag") == "A")
        #endregion

        if (Session["Role"].ToString().ToLower() != "vendor" && Session["Role"].ToString().ToLower() != "carrier")
        {
            if (GetQueryStringValue("RoleType") == "P" || GetQueryStringValue("RoleType") == "A")
            {
                oDiscrepancyBE.Action = "CheckForActionRequired";
                oDiscrepancyBE.DiscrepancyWorkFlowID = !string.IsNullOrEmpty(GetQueryStringValue("WorkFlowID")) ? Convert.ToInt32(GetQueryStringValue("WorkFlowID")) : (int?)null;
                DataTable dtActionDetails = oDiscrepancyBAL.GetActionRequiredDetailsForWorkFlowBAL(oDiscrepancyBE);
                if (dtActionDetails != null && dtActionDetails.Rows.Count > 0)
                {
                    if (Convert.ToString(dtActionDetails.Rows[0]["ACP001Control"]).Trim().Equals("ACP001") && !Convert.ToBoolean(dtActionDetails.Rows[0]["IsACP001Done"]))
                    {
                        oDiscrepancyBE = new DiscrepancyBE();
                        oDiscrepancyBE.Action = "GetDiscrepancySiteVendorDetail";
                        oDiscrepancyBE.DiscrepancyLogID = disLogID;
                        List<DiscrepancyBE> lstDetails = oDiscrepancyBAL.GetDiscrepancyLogSiteVendorDetailsBAL(oDiscrepancyBE);
                        if (lstDetails != null && lstDetails.Count > 0)
                        {
                            ucApAction.DisLogID = disLogID;
                            ucApAction.DisWFlowID = 0; // Not required workflowid from this page.
                            ucApAction.PONumber = lstDetails[0].PurchaseOrder.Purchase_order;
                            ucApAction.SiteID = lstDetails[0].Site.SiteID;
                            ucApAction.VDRNo = lstDetails[0].VDRNo;
                            ucApAction.FromPage = this.GetFromPage(lstDetails[0].VDRNo);
                            ucApAction.PreviousPage = Convert.ToString(GetQueryStringValue("PreviousPage"));
                            ucApAction.VendorID = lstDetails[0].VendorID != null ? Convert.ToInt32(lstDetails[0].VendorID) : 0;
                            btnAccountsPayableAction.Visible = true;
                        }
                    }
                    else { btnAccountsPayableAction.Visible = false; }
                }
                else { btnAccountsPayableAction.Visible = false; }
            }
            //else { btnAccountsPayableAction.Visible = true; }
        }
    }
    private string GetFromPage(string vdrNo)
    {
        var fromPage = string.Empty;
        if (!string.IsNullOrEmpty(vdrNo))
        {
            var vdrFirstText = vdrNo.Substring(0, 1);
            switch (vdrFirstText)
            {
                case clsConstants.OversPrefix:
                    fromPage = "overs";
                    break;
                case clsConstants.ShortagePrefix:
                    fromPage = "shortage";
                    break;
                case clsConstants.WrongPackSizePrefix:
                    fromPage = "wrongpacksize";
                    break;
                case clsConstants.GoodReceivedDamagedPrefix:
                    fromPage = "goodsreceiveddamaged";
                    break;
                case clsConstants.QualityIssuePrefix:
                    fromPage = "qualityissue";
                    break;
                case clsConstants.IncorrectProductPrefix:
                    fromPage = "incorrectproduct";
                    break;
            }
        }

        return fromPage;
    }
    protected void BtnAccountsPayableAction_Click(object sender, EventArgs e)
    {
        mdlAPAction.Show();
        DropDownList ddl = ucApAction.FindControl("ddlCurrency") as DropDownList;
        DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
        DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();
        oDiscrepancyBE.Action = "GetCurrencyByVendorID";
        if (GetQueryStringValue("disLogID") != null)
        {
            oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID"));
        }
        ddl.SelectedValue = oDiscrepancyBAL.GetCurrencyBAL(oDiscrepancyBE);
    }
    public int? VendorID
    {
        get
        {
            return ViewState["VendorID"] != null ? Convert.ToInt32(ViewState["VendorID"]) : 0;
        }
        set
        {
            ViewState["VendorID"] = value;
        }
    }
    public int? SiteID
    {
        get
        {
            return ViewState["SiteID"] != null ? Convert.ToInt32(ViewState["SiteID"]) : 0;
        }
        set
        {
            ViewState["SiteID"] = value;
        }
    }

    public int? DiscrepancyTypeID
    {
        get
        {
            return ViewState["DiscrepancyTypeID"] != null ? Convert.ToInt32(ViewState["DiscrepancyTypeID"]) : 0;
        }
        set
        {
            ViewState["DiscrepancyTypeID"] = value;
        }
    }

    public string VendorEmailIds
    {
        get
        {
            return ViewState["VendorEmailIds"] != null ? ViewState["VendorEmailIds"].ToString() : string.Empty;
        }
        set
        {
            ViewState["VendorEmailIds"] = value;
        }
    }
    public string VendorFax
    {
        get
        {
            return ViewState["VendorFax"] != null ? ViewState["VendorFax"].ToString() : string.Empty;
        }
        set
        {
            ViewState["VendorFax"] = value;
        }
    }
    public void GetDetails()
    {
        if (VendorID != null && VendorID > 0)
        {
            SCT_UserBE oSCT_UserBE = new SCT_UserBE();
            SCT_UserBAL oSCT_UserBAL = new SCT_UserBAL();
            oSCT_UserBE.Action = "GetContactDetailsDiscrepancy";
            oSCT_UserBE.VendorID = VendorID;
            oSCT_UserBE.SiteId = this.SiteID;
            List<SCT_UserBE> lstVendorDetails1 = oSCT_UserBAL.GetUserDetailsBAL(oSCT_UserBE);
            VendorEmailIds = "";
            VendorFax = "";
            if (lstVendorDetails1 != null && lstVendorDetails1.Count > 0)
            {
                foreach (SCT_UserBE item in lstVendorDetails1)
                {
                    VendorEmailIds += item.EmailId.ToString() + ", ";
                    VendorFax += item.FaxNumber.ToString() + ", ";
                }
                VendorEmailIds = VendorEmailIds.Trim(new char[] { ',', ' ' });
                VendorFax = VendorFax.Trim(new char[] { ',', ' ' });
            }
            else
            {
                MASSIT_VendorBE oMASSIT_VendorBE = new MASSIT_VendorBE();
                APPSIT_VendorBAL oAPPSIT_VendorBAL = new APPSIT_VendorBAL();
                oMASSIT_VendorBE.Action = "GetContactDetails";
                oSCT_UserBE.SiteId = this.SiteID;
                oMASSIT_VendorBE.SiteVendorID = VendorID.Value;

                List<MASSIT_VendorBE> lstVendorDetails2 = oAPPSIT_VendorBAL.GetVendorContactDetailsBAL(oMASSIT_VendorBE);
                if (lstVendorDetails2 != null && lstVendorDetails2.Count > 0)
                {
                    if (!string.IsNullOrEmpty(lstVendorDetails2[0].Vendor.VendorContactEmail.ToString()))
                    {
                        VendorEmailIds = lstVendorDetails2[0].Vendor.VendorContactEmail.ToString();
                        VendorFax = lstVendorDetails2[0].Vendor.VendorContactFax.ToString();
                    }
                }
            }
            if (string.IsNullOrEmpty(VendorEmailIds))
            {
                VendorEmailIds = System.Configuration.ConfigurationManager.AppSettings["DefaultEmailAddress"];
                VendorFax = System.Configuration.ConfigurationManager.AppSettings["DefaultEmailAddress"];
            }
        }
    }

    public string GetDiscrepancyType(int distID)
    {
        string distType = string.Empty;
        switch (distID)
        {
            case 1: 
                distType = "over";
                break;
            case 2: 
                distType = "shortage";
                break;
            case 3: 
                distType = "goodsreceiveddamaged";
                break;
            case 4: 
                distType = "nopurchaseordernumber";
                break;
            case 5: 
                distType = "nopaperwork";
                break;
            case 6: 
                distType = "incorrectproduct";
                break;
            case 7: 
                distType = "presentationissue";
                break;
            case 8:  
                distType = "IncorrectAddress";
                break;
            case 9:  
                distType = "paperworkamended";
                break;
            case 10: 
                distType = "WrongPackSize";
                break;
            case 11:  
                distType = "failpalletspecification";
                break;
            case 12:  
                distType = "qualityissue";
                break;
            case 13: 
                distType = "prematureinvoice";
                break;
            case 14: 
                distType = "generic";
                break;
            case 15:  
                distType = "shuttle";
                break;
            case 16: 
                distType = "reservation";
                break;
            case 17:
                distType = "Invoice";
                break;
            case 18: 
                distType = "Invoice";
                break;
            case 19: 
                distType = "Invoice Query";
                break;
            case 20: 
                distType = "itemnotonpo";
                break;             
        }

        return distType;
    }
}
﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using BaseControlLibrary;
using BusinessEntities.ModuleBE.AdminFunctions;
using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using BusinessEntities.ModuleBE.Discrepancy.LogDiscrepancy;
using BusinessLogicLayer.ModuleBAL.AdminFunctions;
using BusinessLogicLayer.ModuleBAL.Appointment.SiteSettings;
using BusinessLogicLayer.ModuleBAL.Discrepancy.LogDiscrepancy;
using Utilities;
using WebUtilities;

public partial class DISLog_Reservation : CommonPage
{
    #region Global variables

    //static int iVendorID = 0;
    //static int DiscrepancyLogID = 0;
    //static int SiteID = 0;

    public int iVendorID
    {
        get
        {
            return ViewState["iVendorID"] != null ? Convert.ToInt32(ViewState["iVendorID"].ToString()) : 0;
        }
        set
        {
            ViewState["iVendorID"] = value;
        }
    }

    public int DiscrepancyLogID
    {
        get
        {
            return ViewState["DiscrepancyLogID"] != null ? Convert.ToInt32(ViewState["DiscrepancyLogID"].ToString()) : 0;
        }
        set
        {
            ViewState["DiscrepancyLogID"] = value;
        }
    }

    public int SiteID
    {
        get
        {
            return ViewState["SiteID"] != null ? Convert.ToInt32(ViewState["SiteID"].ToString()) : 0;
        }
        set
        {
            ViewState["SiteID"] = value;
        }
    }
    static int iGridAlreadyContainsItems = 0;
    DataSet dsDummy = null; //dummy dataset use for repeater
    DataTable tbDummy = null; //dummy table for dataset
    static string VDRNo = string.Empty;
    System.Text.StringBuilder sSendMailLink = new System.Text.StringBuilder();
    static BaseControlLibrary.ucTextbox currentTextBox;
    protected string InvalidValuesEntered = WebCommon.getGlobalResourceValue("InvalidValuesEntered");
    protected string AtLeastOne = WebCommon.getGlobalResourceValue("AtLeastOne");
    protected string ProductDescription = WebCommon.getGlobalResourceValue("ProductDescription");
    protected string ReceivedQuantity = WebCommon.getGlobalResourceValue("ReceivedQuantity");
    protected string ValidEmail = WebCommon.getGlobalResourceValue("ValidEmail");
    protected string NoEmailSpecifiedMessage = WebCommon.getGlobalResourceValue("NoEmailSpecifiedMessage");
    protected string ConfirmMessage = WebCommon.getGlobalResourceValue("ConfirmMessage");
    protected string DiscrepancyAlreadyCreated = WebCommon.getGlobalResourceValue("DiscrepancyAlreadyCreated");
    string InitialCommunicationSent = WebCommon.getGlobalResourceValue("InitialCommunicationSent");
    string FollowingRequestMessage = WebCommon.getGlobalResourceValue("FollowingRequestMessage");
    string ReservationInvDesc = WebCommon.getGlobalResourceValue("ReservationInvDesc");
    string IssueRaisedDateMandatory = WebCommon.getGlobalResourceValue("IssueRaisedDateMandatory");
    string EnterDetailsofTheIssue = WebCommon.getGlobalResourceValue("EnterDetailsofTheIssue");
    /*---------------SHOW ERROR MESSAGE WHEN QUERY IS ALREADY OPEN--------------*/
    string strThereIsCurrentlyAlreadyAnOpenQueryBeingInvestigated = WebCommon.getGlobalResourceValue("ThereIsCurrentlyAlreadyAnOpenQueryBeingInvestigated");
    string strPleaseWaitUntilYouHaveReceivedAReplyForThisBefore = WebCommon.getGlobalResourceValue("PleaseWaitUntilYouHaveReceivedAReplyForThisBefore");


    protected string ItemDetailsRequired = WebCommon.getGlobalResourceValue("ItemDetailsRequired");

    #endregion

    #region Events

    protected void Page_InIt(object sender, EventArgs e)
    {
        ucSite.CurrentPage = this;
        ucSeacrhVendor1.CurrentPage = this;
        ucSeacrhVendor1.IsStandAloneRequired = true;
        ucSeacrhVendor1.IsChildRequired = true;
        ucSeacrhVendor1.IsParentRequired = true;
        ucSeacrhVendor1.IsGrandParentRequired = false;
        ucApAction.CurrentPage = this;
    }

    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);
        if (!IsPostBack)
        {
            if (GetQueryStringValue("VDRNo") != null && !string.IsNullOrWhiteSpace(GetQueryStringValue("VDRNo").ToString()))
            {

                if (GetQueryStringValue("PN") != null && !string.IsNullOrWhiteSpace(GetQueryStringValue("PN")))
                {
                    if (GetQueryStringValue("PN").Trim().ToUpper().Equals("POINQ") || GetQueryStringValue("PN").Trim().ToUpper().Equals("OPENPL"))
                    {
                        btnBack_1.Visible = true;
                    }
                }
                else if (GetQueryStringValue("PreviousPage") != null)
                {
                    string iframeStyleStatus = ifUserControl.Style["display"];
                    if (GetQueryStringValue("PreviousPage").Trim().ToUpper().Equals("SearchResultVendor".ToUpper()) &&
                        iframeStyleStatus != null)
                        btnBack_1.Visible = true;
                    else if (GetQueryStringValue("PreviousPage").Trim().Equals("SearchResult") &&
                        iframeStyleStatus != null)
                    {
                        btnBack_1.Visible = true;
                    }
                    else
                    {
                        btnBack_1.Visible = false;
                        btnUpdateLocation.Visible = false;
                    }
                }
            }
            else
            {
                UIUtility.PageValidateForODUser();
            }
        }
        if (GetQueryStringValue("VDRNo") != null && !string.IsNullOrWhiteSpace(GetQueryStringValue("VDRNo").ToString()) && !lblReservationIssue.Text.ToString().Contains("-"))
        {
            lblReservationIssue.Text = string.Format("{0} - {1}", lblReservationIssue.Text, GetQueryStringValue("VDRNo").ToString());
        }
    }

    protected void btnBack_1_Click(object sender, EventArgs e)
    {
        if (GetQueryStringValue("PreviousPage") != null)
        {
            if (GetQueryStringValue("PreviousPage").Trim().ToUpper().Equals("SearchResultVendor".ToUpper()))
            {
                btnBack_1.Visible = false;
                EncryptQueryString("~/ModuleUI/Discrepancy/DIS_SearchResultVendor.aspx?disLogID=" + Convert.ToString(GetQueryStringValue("disLogID"))
                            + "&VDRNo=" + Convert.ToString(GetQueryStringValue("VDRNo"))
                            + "&UserID=" + Convert.ToString(GetQueryStringValue("UserID"))
                            + "&PreviousPage=SearchResultVendor&WorkFlowID=" + Convert.ToString(GetQueryStringValue("WorkFlowID").ToString()));
            }

            if (Session["PageMode"].ToString() == "Todays")
            {
                EncryptQueryString("../DIS_SearchResult.aspx?FromPage=images&SiteId=" + GetQueryStringValue("SiteId") + "&DisDate=" + GetQueryStringValue("DisDate"));
            }
            else if (Session["PageMode"].ToString() == "Search")
            {
                //For opening search discrepancies mode
                btnBack_1.Visible = false;
                EncryptQueryString("~/ModuleUI/Discrepancy/DIS_SearchResult.aspx?disLogID=" + Convert.ToString(GetQueryStringValue("disLogID"))
                            + "&VDRNo=" + Convert.ToString(GetQueryStringValue("VDRNo"))
                            + "&UserID=" + Convert.ToString(GetQueryStringValue("UserID"))
                            + "&PreviousPage=SearchResult&WorkFlowID=" + Convert.ToString(GetQueryStringValue("WorkFlowID").ToString()));

            }
        }
        else if (GetQueryStringValue("PN") != null)
        {
            if (GetQueryStringValue("PN").Trim().ToUpper().Equals("POINQ"))
            {
                btnBack_1.Visible = false;
                EncryptQueryString("~/ModuleUI/Appointment/PurchaseOrderInquiry.aspx?SId=" + GetQueryStringValue("SId").ToString()
                    + "&Status=" + GetQueryStringValue("Status").ToString()
                    + "&PN=DISLOG"
                    + "&PO=" + GetQueryStringValue("PO").ToString());
            }
            else if (GetQueryStringValue("PN").Trim().ToUpper().Equals("OPENPL"))
            {
                EncryptQueryString("~/ModuleUI/StockOverview/Expedite/OpenPurchaseListing.aspx?action=showlisting");
            }
        }
        else if (GetQueryStringValue("PageType") != null)
        {
            EncryptQueryString("~/ModuleUI/Discrepancy/DIS_LocationReport.aspx");
        }
    }
    void ucApAction_SaveEventValidation(object sender, EventArgs e)
    {
        if (ucApAction.IsModelOpen)
            mdlAPAction.Show();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        ucApAction.SaveEventValidation += new EventHandler(ucApAction_SaveEventValidation);
        dsDummy = new DataSet();
        tbDummy = new DataTable();
        txtPurchaseOrder.Focus();

        if (GetQueryStringValue("disLogID") != null)
        {
            mvReservationIssue.ActiveViewIndex = 1;
        }

        if (mvReservationIssue.ActiveViewIndex == 1)
        {
            btnLog.Visible = false;
            btnBack.Visible = true;
            btnSave.Visible = true;
            lblEnterDetailsofTheIssue.Visible = false;
            pnlAction.ID = string.Format("pnl{0}", WebCommon.getGlobalResourceValue("Comments"));
            pnlAction.GroupingText = WebCommon.getGlobalResourceValue("Comments");
        }
        else
        {
            btnLog.Visible = true;
            btnBack.Visible = false;
            btnSave.Visible = false;
            lblEnterDetailsofTheIssue.Visible = true;
            pnlAction.ID = string.Format("pnl{0}", WebCommon.getGlobalResourceValue("Action"));
            pnlAction.GroupingText = WebCommon.getGlobalResourceValue("Action");
        }

        if (rdoPO1.Checked)
        {
            rfvPurchaseOrderNumberValidation.Enabled = true;
            cusvNoSelectedVendor.Enabled = false;
        }
        else if (rdoVendor11.Checked)
        {
            rfvPurchaseOrderNumberValidation.Enabled = false;
            cusvNoSelectedVendor.Enabled = true;
        }

        if (!IsPostBack)
        {
            txtDateDeliveryArrived.innerControltxtDate.Value = Convert.ToString(DateTime.Now.ToString("dd/MM/yyyy"));
            lblIssueRaisedDate.Text = Convert.ToString(DateTime.Now.ToString("dd/MM/yyyy hh:mm"));
            rfvPurchaseOrderNumberValidation.Enabled = true;
            cusvNoSelectedVendor.Enabled = false;
            txtPurchaseOrder.Enabled = true;
            ddlPurchaseOrderDate.Enabled = true;
            lblVendorValue.Text = "";
            hdnSelectOption.Value = "1";
            VendorsQueryOverview();
            if (GetQueryStringValue("disLogID") != null)
            {
                this.BindDiscrepancyLogComment();
                if (GetQueryStringValue("status").ToLower() == "closed")
                {
                    btnUpdateLocation.Visible = true;
                    // txtLocation.ReadOnly = true;
                }
                else
                {
                    btnUpdateLocation.Visible = true;
                }
                if (GetQueryStringValue("PageType") != null && GetQueryStringValue("PageType") == "Location")
                {
                    btnUpdateLocation.Visible = false;
                    txtLocation.ReadOnly = true;
                    btnBack_1.Visible = true;
                }
                mvReservationIssue.ActiveViewIndex = 1;
                btnAdd.Style["display"] = "none";
                GetDeliveryDetails();
                GetContactPurchaseOrderDetails();
                if (Session["Role"] != null)
                {
                    GetAccessRightRaiseQuery();
                    if (Session["Role"].ToString() == "Vendor")
                    {
                        pnlAction.Visible = false;
                        btnAddComment.Visible = true;
                        btnAddImages.Visible = false;
                        btnRaiseQuery.Visible = true;
                        btnAccountsPayableAction.Visible = false;
                    }
                }
                AddUserControl();
                this.ShowACP001ActionButton();
                btnLog.Visible = false;
                btnBack.Visible = false;
                btnSave.Visible = false;
                if (Session["Role"] != null && (Session["Role"].ToString().ToLower() == "vendor" || Session["Role"].ToString().ToLower() == "carrier"))
                {
                    btnViewWF.Visible = false;
                }
                else
                {
                    btnViewWF.Visible = true;
                }
                // code for generating div for the imageviewer
                string DivGallery = CommonPage.GetImagesStringNewLayout(Convert.ToInt32(GetQueryStringValue("disLogID")));
                if (string.IsNullOrEmpty(DivGallery))
                {
                    btnViewImage.Visible = false;
                }
                else
                {
                    discrepancygallery.InnerHtml = DivGallery.ToString();
                    btnViewImage.Visible = true;
                }
            }
            else
            {
                btnAddComment.Visible = false;
                ifUserControl.Style["display"] = "none";
                btnViewWF.Visible = false;
                btnViewImage.Visible = false;
                btnAddImages.Visible = false;
                btnRaiseQuery.Visible = false;
                pnlQueryOverview_1.Visible = false;
                btnAccountsPayableAction.Visible = false;
            }
        }

        txtPurchaseOrder.Attributes.Add("autocomplete", "off");
        txtDeliveryNumber.Focus();
    }

    public override void SitePost_Load()
    {
        base.SitePost_Load();
        if (!IsPostBack)
        {
            ucSite.innerControlddlSite.AutoPostBack = true;
            if (GetQueryStringValue("disLogID") == null)
            {
                ucSeacrhVendor1.SiteID = Convert.ToInt32(ucSite.innerControlddlSite.SelectedValue);
            }
        }
    }
    protected void btnAccountsPayableAction_Click(object sender, EventArgs e)
    {
        mdlAPAction.Show();
        DropDownList ddl = ucApAction.FindControl("ddlCurrency") as DropDownList;
        DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
        DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();
        oDiscrepancyBE.Action = "GetCurrencyByVendorID";
        if (GetQueryStringValue("disLogID") != null)
        {
            oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID"));
        }
        ddl.SelectedValue = oDiscrepancyBAL.GetCurrencyBAL(oDiscrepancyBE);
    }
    private void ShowACP001ActionButton()
    {
        DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
        DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();
        oDiscrepancyBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
        string CaseOf = String.Empty;

        oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID"));

        if (GetQueryStringValue("RoleTypeFlag") == "A" || GetQueryStringValue("RoleTypeFlag") == null)
        {
            if (Session["UserID"] == null || Session["UserID"].ToString() == "" || Session["Role"] == null || Session["Role"].ToString().Trim() == "")
                return;

            oDiscrepancyBE.User.UserID = Convert.ToInt32(Session["UserID"]);
            CaseOf = Session["Role"].ToString().Trim();
        }


        /* Here P is an "OD - Accounts Payable" When from drop down selected the Account Payble option */
        /* Here A will come when actual Account Payble type user will loggedin. */
        if (GetQueryStringValue("RoleTypeFlag") == "P" || GetQueryStringValue("RoleTypeFlag") == "A")
        {
            oDiscrepancyBE.Action = "CheckForActionRequired";
            oDiscrepancyBE.DiscrepancyWorkFlowID = !string.IsNullOrEmpty(GetQueryStringValue("WorkFlowID")) ? Convert.ToInt32(GetQueryStringValue("WorkFlowID")) : (int?)null;
            DataTable dtActionDetails = oDiscrepancyBAL.GetActionRequiredDetailsForWorkFlowBAL(oDiscrepancyBE);
            if (dtActionDetails != null && dtActionDetails.Rows.Count > 0)
            {
                if (!string.IsNullOrEmpty(dtActionDetails.Rows[0]["IsACP001Done"].ToString()) && !Convert.ToBoolean(dtActionDetails.Rows[0]["IsACP001Done"]))
                {
                    if (GetQueryStringValue("disLogID") != null)
                        ucApAction.DisLogID = Convert.ToInt32(GetQueryStringValue("disLogID"));

                    ucApAction.DisWFlowID = Convert.ToInt32(dtActionDetails.Rows[0]["DiscrepancyWorkflowID"]);
                    ucApAction.PONumber = Convert.ToString(dtActionDetails.Rows[0]["PurchaseOrderNumber"]);
                    ucApAction.SiteID = SiteID;
                    ucApAction.VDRNo = VDRNo;
                    ucApAction.FromPage = "overs";
                    ucApAction.PreviousPage = GetQueryStringValue("PreviousPage").ToString();

                    if (ViewState["VendorID"] != null)
                        ucApAction.VendorID = Convert.ToInt32(ViewState["VendorID"]);

                    btnAccountsPayableAction.Visible = true;

                }
                else
                {
                    btnAccountsPayableAction.Visible = false;
                }
            }
        }
    }
    public override void SiteSelectedIndexChanged()
    {
        if (rdoPO1.Checked && !string.IsNullOrEmpty(txtPurchaseOrder.Text) && !string.IsNullOrWhiteSpace(txtPurchaseOrder.Text))
        {
            GetPurchaseOrderList();
            ScriptManager.RegisterStartupScript(Page, GetType(), "DisableValidators", "DisablePageValidators(1);", true);
        }
        else if (rdoVendor11.Checked)
        {
            TextBox txtVendor = (TextBox)ucSeacrhVendor1.FindControl("txtVendorNo");
            Label lblSelectedVendorName = (Label)ucSeacrhVendor1.FindControl("SelectedVendorName");
            txtVendor.Text = string.Empty;
            txtVendor.Enabled = false;
            lblSelectedVendorName.Text = string.Empty;
            ScriptManager.RegisterStartupScript(Page, GetType(), "DisableValidators", "DisablePageValidators(2)", true);
        }
        else
        {
            ScriptManager.RegisterStartupScript(Page, GetType(), "DisableValidators", "DisablePageValidators(1);", true);
        }

        if (GetQueryStringValue("disLogID") == null)
            ucSeacrhVendor1.SiteID = Convert.ToInt32(ucSite.innerControlddlSite.SelectedValue);
    }

    protected void RemoveImage_Click(object sender, CommandEventArgs e)
    {
        if (e.CommandName == "Remove" && e.CommandArgument != null)
        {
            int iCount = 1;
            tbDummy = null;
            tbDummy = new DataTable();
            AddColumns();
            foreach (GridViewRow item in gvReservation.Rows)
            {

                ucTextbox txtLineNo = (ucTextbox)item.FindControl("txtLineNo");
                ucTextbox txtOfficeDepotCode = (ucTextbox)item.FindControl("txtOfficeDepotCode");
                ucTextbox txtVikingCode = (ucTextbox)item.FindControl("txtVikingCode");
                ucTextbox txtVendorItemCode = (ucTextbox)item.FindControl("txtVendorItemCode");
                ucTextbox txtDescription = (ucTextbox)item.FindControl("txtDescription");
                ucTextbox txtOriginalPOQuantity = (ucTextbox)item.FindControl("txtOriginalPOQuantity");
                ucTextbox txtOutstandingQty = (ucTextbox)item.FindControl("txtOutstandingQty");
                ucTextbox txtReceivedQty = (ucTextbox)item.FindControl("txtReceivedQty");
                ucTextbox txtUoM = (ucTextbox)item.FindControl("txtUoM");
                ucButton btnRemove = (ucButton)item.FindControl("btnRemove");


                HiddenField hdnRecordID = (HiddenField)item.FindControl("hdnRecordID");
                if (e.CommandArgument.ToString().Trim() != hdnRecordID.Value.ToString().Trim())
                {
                    tbDummy.Rows.Add(txtLineNo.Text.Trim(),
                                 txtOfficeDepotCode.Text.Trim(),
                                 txtVikingCode.Text.Trim(),
                                 txtVendorItemCode.Text.Trim(),
                                 txtDescription.Text.Trim(),
                                 txtOriginalPOQuantity.Text.Trim(),
                                 txtOutstandingQty.Text.Trim(),
                                 txtReceivedQty.Text.Trim(),
                                 txtUoM.Text.Trim(),
                                 "Remove",
                                 (iCount++).ToString());
                }
            }

            BindWithGrid();
            EnableDisableGridControls();
        }
    }

    protected void btnViewWF_Click(object sender, EventArgs e)
    {
        if (GetQueryStringValue("PN") != null)
        {
            if (GetQueryStringValue("PN").Trim().ToUpper().Equals("POINQ"))
            {
                string url = string.Format("DISLog_Workflow.aspx?disLogID={0}&PN={1}&SId={2}&Status={3}&PN={4}&PO={5}", GetQueryStringValue("disLogID").ToString(), "POINQ", GetQueryStringValue("SId").ToString(), GetQueryStringValue("Status").ToString(), "DISLOG", GetQueryStringValue("PO").ToString());
                EncryptQueryString(url);
            }

        }
        if (GetQueryStringValue("disLogID") != null)
        {
            string url = string.Format("DISLog_Workflow.aspx?disLogID={0}&VDRNo={1}&TodayDis={2}&SiteID={3}&DisDate={4}&UserID={5}", GetQueryStringValue("disLogID").ToString(), GetQueryStringValue("VDRNo").ToString(), GetQueryStringValue("TodayDis"), GetQueryStringValue("SiteId"), GetQueryStringValue("DisDate"), GetQueryStringValue("UserID"));
            EncryptQueryString(url);
        }
    }

    protected void btnAdd_Click(object sender, EventArgs e)
    {
        IncreaseRow();
    }

    protected void btnLog_Click(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(lblIssueRaisedDate.Text) || string.IsNullOrWhiteSpace(lblIssueRaisedDate.Text))
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('" + IssueRaisedDateMandatory + "')", true);
            return;
        }

        DateTime dt = txtDateDeliveryArrived.GetDate;
        if (dt > DateTime.Now.Date)
        {
            string saveMessage = WebCommon.getGlobalResourceValue("NotDateValidMessage");
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('" + saveMessage + "')", true);
            return;
        }
        GetDeliveryDetails();
        GetContactPurchaseOrderDetails();

        mvReservationIssue.ActiveViewIndex = 1;
        btnLog.Visible = false;
        btnBack.Visible = true;
        btnSave.Visible = true;
        btnSave.Enabled = true;

        if (hdnSelectOption.Value == "1")
        {
            btnAdd.Style["display"] = "none";
        }
        else
        {
            btnAdd.Style["display"] = "";
            IncreaseRow();
        }
        if (gvReservation.Rows.Count > 0)
            iGridAlreadyContainsItems = 1;
        ucSDRCommunication1.innerControlRdoCommunication = 'E';
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        var defaultEmailAddress = Convert.ToString(ConfigurationManager.AppSettings["DefaultEmailAddress"]);
        var txtAltEmail = (ucTextbox)ucSDRCommunication1.FindControl("txtAltEmail");
        if (ucSDRCommunication1.VendorEmailList.Trim().Equals(defaultEmailAddress) && (string.IsNullOrEmpty(txtAltEmail.Text) || string.IsNullOrWhiteSpace(txtAltEmail.Text)))
        {
            mdlAlternateEmail.Show();
            rfvEmailAddressCanNotBeEmpty.Enabled = true;
            txtAlternateEmailText.Focus();
        }
        else
        {
            foreach (GridViewRow item in gvReservation.Rows)
            {
                ucTextbox txtOfficeDepotCode = (ucTextbox)item.FindControl("txtOfficeDepotCode");

                if (txtOfficeDepotCode.Text.Equals(""))
                {
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + ItemDetailsRequired + "')", true);
                    return;
                }
            }
            if (Session["Role"].ToString().ToLower() == "vendor")
            {
                string LoginPath = ResolveClientUrl("../../../ModuleUI/Security/Login.aspx");
                Response.Redirect(LoginPath);
            }
            else
            {
                this.Save();
            }
        }
    }

    private void Save()
    {
        try
        {
            bool bSendMail = false;

            if (string.IsNullOrEmpty(txtInternalComments.Text) || string.IsNullOrWhiteSpace(txtInternalComments.Text))
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + EnterDetailsofTheIssue + "')", true);
                return;
            }

            //1= PO is selected and 2= vendor is selected
            btnSave.Enabled = false;
            DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
            DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();

            oDiscrepancyBE.Action = "AddDiscrepancyLog";
            oDiscrepancyBE.SiteID = Convert.ToInt32(ucSite.innerControlddlSite.SelectedValue);
            oDiscrepancyBE.DiscrepancyTypeID = Convert.ToInt32(Common.DiscrepancyType.ReservationDiscrepancy);
            oDiscrepancyBE.DiscrepancyLogDate = DateTime.Now;
            oDiscrepancyBE.InternalComments = txtInternalComments.Text;
            oDiscrepancyBE.DeliveryNoteNumber = txtDeliveryNumber.Text.Trim();
            oDiscrepancyBE.DeliveryArrivedDate = Common.GetMM_DD_YYYY(txtDateDeliveryArrived.innerControltxtDate.Value);// DateTime.Now;
            oDiscrepancyBE.CommunicationType = ucSDRCommunication1.innerControlRdoCommunication;
            oDiscrepancyBE.CommunicationTo = ucSDRCommunication1.VendorEmailList;

            if (rdoPO1.Checked)
            {
                oDiscrepancyBE.PurchaseOrderNumber = txtPurchaseOrder.Text.Trim();
                oDiscrepancyBE.PurchaseOrderID = ViewState["PurchaseOrderID"] != null ? Convert.ToInt32(ViewState["PurchaseOrderID"].ToString()) : (int?)null;
                oDiscrepancyBE.PurchaseOrderDate = Common.GetMM_DD_YYYY(lblPurchaseOrderDateValue.Text);
                oDiscrepancyBE.VendorID = iVendorID;
            }
            else
            {
                oDiscrepancyBE.PurchaseOrderID = (int?)null;
                oDiscrepancyBE.VendorID = Convert.ToInt32(ucSeacrhVendor1.VendorNo);
            }


            if (ViewState["SPToCoverStockPlannerID"] != null)
            {
                oDiscrepancyBE.StockPlannerID = Convert.ToInt32(ViewState["SPToCoverStockPlannerID"].ToString()); // case when actual Stock Planner is absent and SP to Cover is there
                oDiscrepancyBE.StockPlannerIDIfNotAbsent = ViewState["StockPlannerID"] != null ? Convert.ToInt32(ViewState["StockPlannerID"].ToString()) : (int?)null; // Actual Stock Palnner Id that should have been saved if not absent
            }
            else
            {
                if (ViewState["StockPlannerID"] != null)
                {
                    oDiscrepancyBE.StockPlannerID = Convert.ToInt32(ViewState["StockPlannerID"].ToString());
                    oDiscrepancyBE.StockPlannerIDIfNotAbsent = null;
                }
            }

            oDiscrepancyBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
            oDiscrepancyBE.User.Site = new BusinessEntities.ModuleBE.AdminFunctions.MAS_SiteBE();
            oDiscrepancyBE.User.UserID = Convert.ToInt32(Session["UserID"]);
            oDiscrepancyBE.Location = txtLocation.Text.Trim();
            string sResult = oDiscrepancyBAL.addDiscrepancyLogBAL(oDiscrepancyBE);
            oDiscrepancyBAL = null;
            if (!sResult.Contains("Discrepancy Already Inserted"))
            {
                int iResult = Convert.ToInt32(sResult.Split('-')[1].ToString());

                if (iResult > 0)
                {
                    foreach (GridViewRow item in gvReservation.Rows)
                    {
                        ucTextbox txtOfficeDepotCode = (ucTextbox)item.FindControl("txtOfficeDepotCode");
                        ucTextbox txtDescription = (ucTextbox)item.FindControl("txtDescription");
                        ucTextbox txtReceivedQty = (ucTextbox)item.FindControl("txtReceivedQty");
                        ucTextbox txtDeliveredQty = (ucTextbox)item.FindControl("txtDeliveredQty");
                        ucTextbox txtIssue = (ucTextbox)item.FindControl("txtIssue");

                        if (!string.IsNullOrEmpty(txtDeliveredQty.Text) || !string.IsNullOrWhiteSpace(txtIssue.Text))
                        {
                            DiscrepancyBE oNewDiscrepancyBE = new DiscrepancyBE();
                            DiscrepancyBAL oNewDiscrepancyBAL = new DiscrepancyBAL();
                            ucTextbox txtLineNo = (ucTextbox)item.FindControl("txtLineNo");
                            ucTextbox txtVikingCode = (ucTextbox)item.FindControl("txtVikingCode");
                            ucTextbox txtVendorItemCode = (ucTextbox)item.FindControl("txtVendorItemCode");
                            ucTextbox txtUoM = (ucTextbox)item.FindControl("txtUoM");
                            ucTextbox txtOriginalPOQuantity = (ucTextbox)item.FindControl("txtOriginalPOQuantity");
                            ucTextbox txtOutstandingQty = (ucTextbox)item.FindControl("txtOutstandingQty");

                            oNewDiscrepancyBE.Action = "AddDiscrepancyItem";
                            oNewDiscrepancyBE.Line_no = txtLineNo.Text.Trim() != "" ? Convert.ToInt32(txtLineNo.Text) : 0;
                            oNewDiscrepancyBE.DirectCode = txtVikingCode.Text;
                            oNewDiscrepancyBE.ODSKUCode = txtOfficeDepotCode.Text;
                            oNewDiscrepancyBE.VendorCode = txtVendorItemCode.Text;
                            oNewDiscrepancyBE.ProductDescription = txtDescription.Text;
                            oNewDiscrepancyBE.PurchaseOrder = new BusinessEntities.ModuleBE.Upload.Up_PurchaseOrderDetailBE();
                            oNewDiscrepancyBE.OutstandingQuantity = string.IsNullOrEmpty(txtOutstandingQty.Text) ? (int?)null : Convert.ToInt32(txtOutstandingQty.Text);
                            oNewDiscrepancyBE.OriginalQuantity = string.IsNullOrEmpty(txtOriginalPOQuantity.Text) ? (int?)null : Convert.ToInt32(txtOriginalPOQuantity.Text);
                            oNewDiscrepancyBE.UOM = txtUoM.Text;
                            oNewDiscrepancyBE.DiscrepancyLogID = iResult;
                            oNewDiscrepancyBE.DeliveredQuantity = txtReceivedQty.Text.Trim() != "" ? Convert.ToInt32(txtReceivedQty.Text.Trim()) : 0;

                            oNewDiscrepancyBE.DeliveredQuantity = string.IsNullOrEmpty(txtDeliveredQty.Text) ? (int?)null : Convert.ToInt32(txtDeliveredQty.Text);
                            oNewDiscrepancyBE.DamageDescription = txtIssue.Text;
                            int? iResult2 = oNewDiscrepancyBAL.addDiscrepancyItemBAL(oNewDiscrepancyBE);

                        }
                    }
                    bSendMail = true;
                    if (bSendMail && ((RadioButton)ucSDRCommunication1.FindControl("rdoEmailComm")) != null && ((RadioButton)ucSDRCommunication1.FindControl("rdoEmailComm")).Checked)
                    {
                        int? iDiscrepancyCommunicationID = sendAndSaveDiscrepancyCommunication(iResult); // only for sending initial communication to Stock Planner (Phase 25R3)
                    }

                    DiscrepancyLogID = iResult;
                    VDRNo = sResult.Split('-')[0].ToString();

                    iGridAlreadyContainsItems = 0;
                    if (ucSDRCommunication1.innerControlRdoCommunication == 'L')
                    {
                        sendCommunication communication = new sendCommunication();
                        Session["LabelHTML"] = communication.sendAndSaveLetter(DiscrepancyLogID, "nopurchaseordernumber", ViewState["VendorID"] != null ? Convert.ToInt32(ViewState["VendorID"].ToString()) : iVendorID); ;
                        sLetterLink.Append(communication.sLetterLink);
                    }
                    InsertHTML();
                    EncryptQueryString("DIS_OptionsPopup.aspx?VDRNo=" + sResult.Split('-')[0].ToString()
                        + "&DiscrepancyLogID=" + sResult.Split('-')[1].ToString()
                        + "&CommunicationType=" + ucSDRCommunication1.innerControlRdoCommunication
                        + "&SiteId=" + ucSite.innerControlddlSite.SelectedValue
                        + "&DisDate=" + DateTime.Now.ToString("dd/MM/yyyy"));
                }
            }
            else if (sResult.Contains("Discrepancy Already Inserted"))
            {
                EncryptQueryString("DIS_OptionsPopup.aspx?VDRNo=" + sResult.Split('-')[0].ToString()
                       + "&DiscrepancyLogID=" + sResult.Split('-')[1].ToString()
                       + "&CommunicationType=" + ucSDRCommunication1.innerControlRdoCommunication
                       + "&SiteId=" + ucSite.innerControlddlSite.SelectedValue
                       + "&DisDate=" + DateTime.Now.ToString("dd/MM/yyyy"));
            }
        }
        catch (Exception ex)
        {
        }
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        mvReservationIssue.ActiveViewIndex = 0;
        btnLog.Visible = true;
        btnBack.Visible = false;
        btnSave.Visible = false;

        txtPurchaseOrder.Text = "";
        ddlPurchaseOrderDate.SelectedIndex = 0;
        lblVendorValue.Text = "";
        ((ucTextbox)(ucSeacrhVendor1.FindControl("txtVendorNo"))).Text = "";
        ((ucLabel)(ucSeacrhVendor1.FindControl("SelectedVendorName"))).Text = "";
        rdoPO1.Checked = true;
        rdoVendor11.Checked = false;
        iGridAlreadyContainsItems = 0;
    }

    protected void ddlPurchaseOrder_SelectedIndexChanged(object sender, EventArgs e)
    {

        if (ViewState["GetPODateList"] != null)
        {
            List<DiscrepancyBE> lstGetPODateList = (List<DiscrepancyBE>)ViewState["GetPODateList"];
            if (!string.IsNullOrEmpty(ddlPurchaseOrderDate.SelectedItem.Value) && lstGetPODateList.Count > 0)
            {
                var PODateDetail = lstGetPODateList.Find(p => p.RowNumber == Convert.ToInt32(ddlPurchaseOrderDate.SelectedItem.Value));
                lblVendorValue.Text = PODateDetail.VendorNoName;

                if (PODateDetail.VendorID > 0)
                    ucSDRCommunication1.VendorID = Convert.ToInt32(PODateDetail.VendorID);
                else
                    ucSDRCommunication1.VendorID = 0;

                if (!string.IsNullOrEmpty(ucSite.innerControlddlSite.SelectedValue))
                    ucSDRCommunication1.SiteID = Convert.ToInt32(ucSite.innerControlddlSite.SelectedValue);
                else
                    ucSDRCommunication1.SiteID = 0;
                ucSDRCommunication1.GetDetails();
            }
        }
    }

    protected void txtPurchaseOrder_TextChanged(object sender, EventArgs e)
    {
        GetPurchaseOrderList();
    }

    protected void btnOk_Click(object sender, EventArgs e)
    {
        mvReservationIssue.ActiveViewIndex = 1;
        btnLog.Visible = false;
        btnBack.Visible = true;
        btnSave.Visible = true;

        GridViewRow currentGridViewRow = (GridViewRow)currentTextBox.Parent.Parent;
        int ind = currentGridViewRow.RowIndex;
        GridViewRow row = gvReservation.Rows[ind];

        ucTextbox txtLineNo = (ucTextbox)row.FindControl("txtLineNo");
        ucTextbox txtOfficeDepotCode = (ucTextbox)row.FindControl("txtOfficeDepotCode");
        ucTextbox txtVikingCode = (ucTextbox)row.FindControl("txtVikingCode");
        ucTextbox txtVendorItemCode = (ucTextbox)row.FindControl("txtVendorItemCode");
        ucTextbox txtDescription = (ucTextbox)row.FindControl("txtDescription");
        ucTextbox txtReceivedQty = (ucTextbox)row.FindControl("txtReceivedQty");
        ucTextbox txtUoM = (ucTextbox)row.FindControl("txtUoM");
        ucButton btnRemove = (ucButton)row.FindControl("btnRemove");
        ucTextbox txtOriginalPOQuantity = (ucTextbox)row.FindControl("txtOriginalPOQuantity");
        ucTextbox txtOutstandingQty = (ucTextbox)row.FindControl("txtOutstandingQty");

        txtLineNo.Text = "";
        txtVikingCode.Text = "";
        txtVendorItemCode.Text = "";
        txtDescription.Text = "";
        txtUoM.Text = "";
        txtOriginalPOQuantity.Text = "";
        txtOutstandingQty.Text = "";

        txtLineNo.Enabled = true;
        txtOfficeDepotCode.Enabled = true;
        txtVikingCode.Enabled = true;
        txtVendorItemCode.Enabled = true;
        txtDescription.Enabled = true;
        txtUoM.Enabled = true;
        txtOutstandingQty.Enabled = true;
        txtOriginalPOQuantity.Enabled = true;
        btnRemove.Style["display"] = "";

        currentTextBox = null;
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        mvReservationIssue.ActiveViewIndex = 1;
        btnLog.Visible = false;
        btnBack.Visible = true;
        btnSave.Visible = true;

        GridViewRow currentGridViewRow = (GridViewRow)currentTextBox.Parent.Parent;
        int ind = currentGridViewRow.RowIndex;
        GridViewRow row = gvReservation.Rows[ind];

        ucTextbox txtLineNo = (ucTextbox)row.FindControl("txtLineNo");
        ucTextbox txtOfficeDepotCode = (ucTextbox)row.FindControl("txtOfficeDepotCode");
        ucTextbox txtVikingCode = (ucTextbox)row.FindControl("txtVikingCode");
        ucTextbox txtVendorItemCode = (ucTextbox)row.FindControl("txtVendorItemCode");
        ucTextbox txtDescription = (ucTextbox)row.FindControl("txtDescription");
        ucTextbox txtReceivedQty = (ucTextbox)row.FindControl("txtReceivedQty");
        ucTextbox txtUoM = (ucTextbox)row.FindControl("txtUoM");
        ucButton btnRemove = (ucButton)row.FindControl("btnRemove");
        ucTextbox txtOriginalPOQuantity = (ucTextbox)row.FindControl("txtOriginalPOQuantity");
        ucTextbox txtOutstandingQty = (ucTextbox)row.FindControl("txtOutstandingQty");

        txtLineNo.Text = "";
        txtVikingCode.Text = "";
        txtVendorItemCode.Text = "";
        txtDescription.Text = "";
        txtUoM.Text = "";
        txtOriginalPOQuantity.Text = "";
        txtOutstandingQty.Text = "";
        txtOfficeDepotCode.Text = "";

        txtLineNo.Enabled = true;
        txtOfficeDepotCode.Enabled = true;
        txtVikingCode.Enabled = false;
        txtVendorItemCode.Enabled = false;
        txtDescription.Enabled = false;
        txtUoM.Enabled = false;
        txtOutstandingQty.Enabled = false;
        txtOriginalPOQuantity.Enabled = false;
        btnRemove.Style["display"] = "";

        currentTextBox = null;
    }

    protected void TextBox_TextChanged_txtOfficeDepotCode(object sender, EventArgs e)
    {
        mvReservationIssue.ActiveViewIndex = 1;
        btnLog.Visible = false;
        btnBack.Visible = true;
        btnSave.Visible = true;


        currentTextBox = (BaseControlLibrary.ucTextbox)sender;

        GridViewRow currentGridViewRow = (GridViewRow)currentTextBox.Parent.Parent;
        int ind = currentGridViewRow.RowIndex;
        GridViewRow row = gvReservation.Rows[ind];

        ucTextbox txtLineNo = (ucTextbox)row.FindControl("txtLineNo");
        ucTextbox txtOfficeDepotCode = (ucTextbox)row.FindControl("txtOfficeDepotCode");
        ucTextbox txtVikingCode = (ucTextbox)row.FindControl("txtVikingCode");
        ucTextbox txtVendorItemCode = (ucTextbox)row.FindControl("txtVendorItemCode");
        ucTextbox txtDescription = (ucTextbox)row.FindControl("txtDescription");
        ucTextbox txtReceivedQty = (ucTextbox)row.FindControl("txtReceivedQty");
        ucTextbox txtUoM = (ucTextbox)row.FindControl("txtUoM");
        ucButton btnRemove = (ucButton)row.FindControl("btnRemove");
        ucTextbox txtOriginalPOQuantity = (ucTextbox)row.FindControl("txtOriginalPOQuantity");
        ucTextbox txtOutstandingQty = (ucTextbox)row.FindControl("txtOutstandingQty");

        //if no product code exists in the textbox 
        //just reset the controls at initials
        if (currentTextBox.Text == "" || currentTextBox.Text == string.Empty)
        {
            txtLineNo.Text = "";
            txtOfficeDepotCode.Text = "";
            txtVikingCode.Text = "";
            txtVendorItemCode.Text = "";
            txtDescription.Text = "";
            txtUoM.Text = "";
            txtOriginalPOQuantity.Text = "";
            txtOutstandingQty.Text = "";

            txtLineNo.Enabled = false;
            txtOfficeDepotCode.Enabled = true;
            txtVikingCode.Enabled = false;
            txtVendorItemCode.Enabled = false;
            txtDescription.Enabled = false;
            txtUoM.Enabled = false;
            txtOutstandingQty.Enabled = false;
            txtOriginalPOQuantity.Enabled = false;
            btnRemove.Style["display"] = "";
            return;
        }

        DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
        DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();

        oDiscrepancyBE.Action = "GetODProductBasedSiteVendorIds";
        oDiscrepancyBE.ODSKUCode = currentTextBox.Text;
        oDiscrepancyBE.SiteID = !string.IsNullOrWhiteSpace(ucSite.innerControlddlSite.SelectedValue) ? Convert.ToInt32(ucSite.innerControlddlSite.SelectedValue) : 0;
        oDiscrepancyBE.VendorID = !string.IsNullOrWhiteSpace(ucSeacrhVendor1.VendorNo) ? Convert.ToInt32(ucSeacrhVendor1.VendorNo) : 0;

        List<DiscrepancyBE> lstODProductDetails = oDiscrepancyBAL.GetODProductDetailsBySiteVendorBAL(oDiscrepancyBE);
        if (lstODProductDetails == null || (lstODProductDetails != null && lstODProductDetails.Count == 0))
        {
            mdlNoPurchaseOrderViewer.Show();
        }
        else
        {
            txtVikingCode.Text = lstODProductDetails[0].DirectCode;
            txtVendorItemCode.Text = lstODProductDetails[0].VendorCode;
            txtDescription.Text = lstODProductDetails[0].ProductDescription;
            txtUoM.Text = lstODProductDetails[0].UOM;

            txtLineNo.Enabled = true;
            txtOfficeDepotCode.Enabled = false;
            txtVikingCode.Enabled = false;
            txtVendorItemCode.Enabled = false;
            txtDescription.Enabled = false;
            txtUoM.Enabled = false;
            btnRemove.Style["display"] = "";
        }
    }

    protected void gvNoPurchaseOrder_Sorting(object sender, GridViewSortEventArgs e)
    {
        if (ViewState["myDataTable"] != null)
        {
            DataTable myDataTable = (DataTable)ViewState["myDataTable"];
            ViewState["sortDirection"] = ViewState["sortDirection"] == null || Convert.ToString(ViewState["sortDirection"]) == "DESC" ? "ASC" : "DESC";
            DataView view = myDataTable.DefaultView;
            view.Sort = e.SortExpression + " " + Convert.ToString(ViewState["sortDirection"]);
            gvReservation.DataSource = view;
            gvReservation.DataBind();

        }
    }

    public override BusinessEntities.BaseBe[] SubSort(GridViewSortEventArgs e)
    {
        return Utilities.GenericListHelper<DiscrepancyBE>.SortList((List<DiscrepancyBE>)ViewState["lstSites"], e.SortExpression, e.SortDirection).ToArray();
    }

    protected void btnSave_1_Click(object sender, EventArgs e)
    {
        DiscrepancyBE discrepancyBE = null;
        DiscrepancyBAL discrepancyBAL = null;

        if (!string.IsNullOrEmpty(txtUserComments.Text) && !string.IsNullOrWhiteSpace(txtUserComments.Text))
        {
            lblErrorMsg.Text = string.Empty;
            discrepancyBE = new DiscrepancyBE();
            discrepancyBE.Action = "AddDiscrepancyLogComment";

            if (!string.IsNullOrEmpty(GetQueryStringValue("disLogID")))
                discrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID"));
            else
                discrepancyBE.DiscrepancyLogID = 0;

            discrepancyBE.UserID = Convert.ToInt32(Session["UserID"]);
            discrepancyBE.CommentDateTime = DateTime.Now;
            discrepancyBE.Comment = txtUserComments.Text;
            discrepancyBAL = new DiscrepancyBAL();
            discrepancyBAL.AddDiscrepancyLogCommentBAL(discrepancyBE);
            discrepancyBE.Action = "GetDiscrepancyLogComment";
            this.BindDiscrepancyLogComment(discrepancyBE);

            discrepancyBE.DiscrepancyType = "reservation";
            int vendorID = 0;
            if (ViewState["VendorID"] != null)
                vendorID = Convert.ToInt32(ViewState["VendorID"]);
            new SendCommentMail().SendCommentsMail(discrepancyBE,
                (BaseControlLibrary.ucTextbox)ucSDRCommunication1.FindControl("txtAltEmail"),
                (BaseControlLibrary.ucTextbox)ucSDRCommunication1.FindControl("ucVendorEmailList"),
                vendorID, txtUserComments.Text, Session["Role"].ToString());

            mdlAddComment.Hide();
        }
        else
        {
            lblErrorMsg.Text = WebCommon.getGlobalResourceValue("MustEnterComment");
            mdlAddComment.Show();
        }

        btnSave.Visible = false;
        btnBack.Visible = false;
        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "Reload()", true);
    }

    protected void btnBack_2_Click(object sender, EventArgs e)
    {
        mdlAddComment.Hide();

        btnSave.Visible = false;
        btnBack.Visible = false;
        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "Reload()", true);
    }

    protected void btnAddComment_Click(object sender, EventArgs e)
    {
        txtUserComments.Text = string.Empty;
        lblErrorMsg.Text = string.Empty;
        lblUserT.Text = Convert.ToString(Session["UserName"]);
        lblDateT.Text = DateTime.Now.Date.ToString("dd/MM/yyyy");
        if (GetQueryStringValue("VDRNo") != null)
            lblDiscrepancyNoT.Text = Convert.ToString(GetQueryStringValue("VDRNo"));
        mdlAddComment.Show();
        txtUserComments.Focus();

        btnSave.Visible = false;
        btnBack.Visible = false;
    }

    protected void btnAddImages_Click(object sender, EventArgs e)
    {

        EncryptQueryString("DIS_OptionsPopup.aspx?Mode=Edit&VDRNo=" + GetQueryStringValue("VDRNo").ToString() + "&DiscrepancyLogID=" + GetQueryStringValue("disLogID").ToString() + "&CommunicationType=" + Common.DiscrepancyType.ReservationDiscrepancy);
    }

    protected void gvReservation_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (rdoVendor11.Checked)
        {
            e.Row.Cells[5].Visible = false;
            e.Row.Cells[6].Visible = false;

        }
    }

    protected void gvReservationViewMode_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (Session["PurchaseOrderID"] == null)
        {
            e.Row.Cells[5].Visible = false;
            e.Row.Cells[6].Visible = false;
        }
    }
    /* ENHANCEMENT FOR PHASE R3 */
    /*RAISE QUERY BUTTON*/
    protected void btnRaiseQuery_Click(object sender, EventArgs e)
    {
        try
        {
            var queryDiscrepancyBAL = new DISLog_QueryDiscrepancyBAL();
            var queryDiscrepancyBE = new DISLog_QueryDiscrepancyBE();
            var discrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID"));

            if (!this.IsAlreadyDisputeExist(discrepancyLogID) || !this.IsQueryClosed(discrepancyLogID))
            {
                btnSave.Visible = false;
                btnBack.Visible = false;
                mdlRaiseQueryConfirmMsg.Show();
            }
            else
            {

                lblMessageFirst.Text = strThereIsCurrentlyAlreadyAnOpenQueryBeingInvestigated;
                lblMessageSecond.Text = strPleaseWaitUntilYouHaveReceivedAReplyForThisBefore;
                btnSave.Visible = false;
                btnBack.Visible = false;
                mdlShowError.Show();
            }

        }

        catch (Exception ex)
        {
            LogUtility.SaveErrorLogEntry(ex);
        }


    }

    #region Alternate email code ...

    protected void btnSetAlternateEmail_Click(object sender, EventArgs e)
    {
        rfvEmailAddressCanNotBeEmpty.Enabled = false;
        var txtAltEmail = (ucTextbox)ucSDRCommunication1.FindControl("txtAltEmail");
        if (txtAltEmail != null)
        {
            txtAltEmail.Text = txtAlternateEmailText.Text;
            txtAlternateEmailText.Text = string.Empty;
        }

        if (Session["Role"].ToString().ToLower() == "vendor")
        {
            string LoginPath = ResolveClientUrl("../../../ModuleUI/Security/Login.aspx");
            Response.Redirect(LoginPath);
        }
        else
        {
            this.Save();
        }
    }

    protected void btnAddEmailId_Click(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(lblMultyAlternateEmail.Text) && !string.IsNullOrWhiteSpace(lblMultyAlternateEmail.Text))
        {
            if (!lblMultyAlternateEmail.Text.Contains(txtAlternateEmailText.Text))
            {
                lblMultyAlternateEmail.Text = string.Format("{0}, {1}", lblMultyAlternateEmail.Text, txtAlternateEmailText.Text);
            }
        }
        else
        {
            lblMultyAlternateEmail.Text = txtAlternateEmailText.Text;
        }

        lblMultyAlternateEmail.Visible = true;
        txtAlternateEmailText.Text = string.Empty;
        txtAlternateEmailText.Focus();
        mdlAlternateEmail.Show();
    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        txtAlternateEmailText.Text = string.Empty;
        lblMultyAlternateEmail.Text = string.Empty;
        txtAlternateEmailText.Focus();
        mdlAlternateEmail.Show();
    }

    #endregion

    #endregion

    #region Methods

    private void GetAccessRightRaiseQuery()
    {
        DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
        DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();
        oDiscrepancyBE.Action = "CheckUserRaiseQueryRight";
        oDiscrepancyBE.UserID = Convert.ToInt32(Session["UserID"]);
        oDiscrepancyBE.SiteID = Convert.ToInt32(Convert.ToString(hdnSiteID.Value));
        int? lstDetails = oDiscrepancyBAL.getAccessRightRaiseQueryBAL(oDiscrepancyBE);
        if (lstDetails > 0)
        {
            btnRaiseQuery.Visible = true;
        }
        else
        {
            btnRaiseQuery.Visible = false;
        }
    }

    private void AddColumns()
    {
        tbDummy.Columns.Add("Line_No");
        tbDummy.Columns.Add("OD_Code");
        tbDummy.Columns.Add("Direct_code");
        tbDummy.Columns.Add("Vendor_Code");
        tbDummy.Columns.Add("ProductDescription");
        tbDummy.Columns.Add("Original_quantity");
        tbDummy.Columns.Add("Outstanding_Qty");
        tbDummy.Columns.Add("ReceivedQty");
        tbDummy.Columns.Add("UOM");
        tbDummy.Columns.Add("Button");
        tbDummy.Columns.Add("RecordID");
    }

    private void BindWithGrid()
    {
        //add this table to dataset
        dsDummy.Tables.Add(tbDummy);

        //bind this dataset to grid
        gvReservation.DataSource = dsDummy;
        gvReservation.DataBind();
        ViewState["myDataTable"] = tbDummy;
    }

    private void GetDeliveryDetails()
    {
        if (GetQueryStringValue("disLogID") != null)
        {
            DiscrepancyBE oNewDiscrepancyBE = new DiscrepancyBE();
            DiscrepancyBAL oNewDiscrepancyBAL = new DiscrepancyBAL();
            oNewDiscrepancyBE.Action = "GetDiscrepancyLog";
            oNewDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
            if (GetQueryStringValue("status").ToLower() == "deleted")
            {
                oNewDiscrepancyBE.IsDelete = true;
            }
            List<DiscrepancyBE> lstDisLog = oNewDiscrepancyBAL.GetDiscrepancyLogDetailsBAL(oNewDiscrepancyBE);
            oNewDiscrepancyBAL = null;
            if (lstDisLog != null && lstDisLog.Count > 0)
            {
                hdnSiteID.Value = Convert.ToString(lstDisLog[0].Site.SiteID);
                lblSiteValue.Text = lstDisLog[0].Site.SiteName;
                lblPurchaseOrderValue.Text = lstDisLog[0].PurchaseOrderNumber;
                lblPurchaseOrderDateValue.Text = "- " + (Convert.ToDateTime(lstDisLog[0].PurchaseOrderDate)).ToString("dd/MM/yyyy");
                lblDeliveryNoValue.Text = lstDisLog[0].DeliveryNoteNumber.ToString();
                lblDeliveryDateValue.Text = "- " + (Convert.ToDateTime(lstDisLog[0].DeliveryArrivedDate)).ToString("dd/MM/yyyy");

                lblDateReceivedValue.Text = "- " + (Convert.ToDateTime(lstDisLog[0].DeliveryArrivedDate)).ToString("dd/MM/yyyy");

                txtInternalComments.Text = lstDisLog[0].InternalComments.ToString();
                txtInternalComments.ReadOnly = true;
                lblVendorNumberValue.Text = lstDisLog[0].VendorNoName.ToString();
                lblContactValue.Text = UIUtility.GetUserPhoneNoofVendor(Convert.ToInt32(lstDisLog[0].VendorID), Convert.ToInt32(lstDisLog[0].SiteID));
                txtLocation.Text = lstDisLog[0].Location.ToString();
                lblStockPlannerNoValue.Text = lstDisLog[0].StockPlannerNO.ToString();
                lblPlannerContactNoValue.Text = lstDisLog[0].StockPlannerContact.ToString();
                VDRNo = lstDisLog[0].VDRNo.ToString();
                SiteID = lstDisLog[0].Site.SiteID;
                ucSDRCommunication1.VendorEmailList = lstDisLog[0].CommunicationTo;
                ucSDRCommunication1.innerControlRdoCommunication = lstDisLog[0].CommunicationType.Value;
                ucSDRCommunication1.innerControlRdoLetter.Enabled = false;
                ucSDRCommunication1.innerControlRdoEmail.Enabled = false;
                ucSDRCommunication1.innerControlEmailList.Enabled = false;
                ucSDRCommunication1.innerControlAltEmailList.Enabled = false;

                ViewState["StockPlannerEmailID"] = lstDisLog[0].StockPlannerEmailID;
            }
        }
        else
        {
            hdnSiteID.Value = Convert.ToString(ucSite.innerControlddlSite.SelectedItem.Value);
            lblSiteValue.Text = ucSite.innerControlddlSite.SelectedItem.Text;
            lblPurchaseOrderValue.Text = txtPurchaseOrder.Text;
            lblDateReceivedValue.Text = lblIssueRaisedDate.Text;
            lblDeliveryNoValue.Text = txtDeliveryNumber.Text;
            lblDeliveryDateValue.Text = txtDateDeliveryArrived.innerControltxtDate.Value;
            lblPurchaseOrderDateValue.Text = txtPurchaseOrder.Text.Trim() != string.Empty ? ddlPurchaseOrderDate.SelectedItem.Text : "";
        }
    }

    private void GetContactPurchaseOrderDetails()
    {

        DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
        DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();
        if (GetQueryStringValue("disLogID") != null)
        {

            gvReservationViewMode.Style["display"] = "";
            gvReservation.Style["display"] = "none";

            oDiscrepancyBE.Action = "GetDiscrepancyItem";
            oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
            List<DiscrepancyBE> lstDetails = oDiscrepancyBAL.GetDiscrepancyItemDetailsBAL(oDiscrepancyBE);
            oDiscrepancyBAL = null;
            if (lstDetails.Count > 0 && lstDetails != null)
            {
                Session["PurchaseOrderID"] = lstDetails[0].PurchaseOrderID;
                gvReservationViewMode.DataSource = lstDetails;
                gvReservationViewMode.DataBind();
                ViewState["lstSites"] = lstDetails;
                ViewState["PurchaseOrderID"] = lstDetails[0].PurchaseOrderID != null ? Convert.ToInt32(lstDetails[0].PurchaseOrderID.ToString()) : (int?)null;
                ViewState["StockPlannerID"] = lstDetails[0].StockPlannerID != null ? Convert.ToInt32(lstDetails[0].StockPlannerID.ToString()) : (int?)null;
                ViewState["VendorID"] = lstDetails[0].VendorID != null ? Convert.ToInt32(lstDetails[0].VendorID.ToString()) : (int?)null;
                ViewState["ActualStockPlannerID"] = lstDetails[0].ActualStockPlannerID != null ?
                                                Convert.ToInt32(lstDetails[0].ActualStockPlannerID.ToString()) : (int?)null; // Stock Planner ID while discrepancy is logged
            }

            DiscrepancyBE DiscrepancyBE = new DiscrepancyBE();
            DiscrepancyBAL DiscrepancyBAL = new DiscrepancyBAL();

            DiscrepancyBE.Action = "GetActualStockPlannerDetailsBySPId"; // getting details of SP while discrepancy is logged

            if (ViewState["ActualStockPlannerID"] != null)
                DiscrepancyBE.StockPlannerID = Convert.ToInt32(ViewState["ActualStockPlannerID"].ToString());

            List<DiscrepancyBE> lstActualStockPlannerDetailsBySPId = DiscrepancyBAL.GetActualStockPlannerDetailsBAL(DiscrepancyBE);

            if (lstActualStockPlannerDetailsBySPId.Count > 0)
            {
                lblStockPlannerNoValue.Text = lstActualStockPlannerDetailsBySPId[0].StockPlannerNO.ToString();
                lblPlannerContactNoValue.Text = lstActualStockPlannerDetailsBySPId[0].StockPlannerContact.ToString();
            }
        }

        else
        {

            gvReservationViewMode.Style["display"] = "none";
            gvReservation.Style["display"] = "";

            if (rdoPO1.Checked)
            {
                oDiscrepancyBE.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();

                oDiscrepancyBE.Action = "GetContactPurchaseOrderDetails";
                oDiscrepancyBE.PurchaseOrderNumber = txtPurchaseOrder.Text.Trim();
                oDiscrepancyBE.PurchaseOrder = new BusinessEntities.ModuleBE.Upload.Up_PurchaseOrderDetailBE();
                oDiscrepancyBE.PurchaseOrder.Order_raised = Common.GetMM_DD_YYYY(ddlPurchaseOrderDate.SelectedItem.Text);
                oDiscrepancyBE.SiteID = Convert.ToInt32(ucSite.innerControlddlSite.SelectedItem.Value);


                List<DiscrepancyBE> lstDetails = oDiscrepancyBAL.GetContactPurchaseOrderDetailsBAL(oDiscrepancyBE);
                oDiscrepancyBAL = null;
                if (lstDetails.Count > 0 && lstDetails != null)
                {
                    if (!string.IsNullOrEmpty(ddlPurchaseOrderDate.SelectedItem.Value))
                    {
                        List<DiscrepancyBE> lstGetPODateList = (List<DiscrepancyBE>)ViewState["GetPODateList"];
                        var PODateDetail = lstGetPODateList.Find(p => p.RowNumber == Convert.ToInt32(ddlPurchaseOrderDate.SelectedItem.Value));
                        lblVendorNumberValue.Text = PODateDetail.VendorNoName;
                        ucSDRCommunication1.VendorID = Convert.ToInt32(PODateDetail.VendorID);
                        if (!string.IsNullOrEmpty(ucSite.innerControlddlSite.SelectedValue))
                            ucSDRCommunication1.SiteID = Convert.ToInt32(ucSite.innerControlddlSite.SelectedValue);
                        else
                            ucSDRCommunication1.SiteID = 0;
                    }

                    lblContactValue.Text = UIUtility.GetUserPhoneNoofVendor(Convert.ToInt32(lstDetails[0].VendorID), Convert.ToInt32(ucSite.innerControlddlSite.SelectedItem.Value));

                    lblStockPlannerNoValue.Text = lstDetails[0].StockPlannerNO.ToString();
                    lblPlannerContactNoValue.Text = lstDetails[0].StockPlannerContact.ToString();


                    ucSDRCommunication1.GetDetails();

                    ViewState["PurchaseOrderID"] = lstDetails[0].PurchaseOrderID != null ? Convert.ToInt32(lstDetails[0].PurchaseOrderID.ToString()) : (int?)null;
                    ViewState["StockPlannerID"] = lstDetails[0].StockPlannerID != null ? Convert.ToInt32(lstDetails[0].StockPlannerID.ToString()) : (int?)null;
                    ViewState["StockPlannerEmailID"] = lstDetails[0].StockPlannerEmailID != null ? lstDetails[0].StockPlannerEmailID : string.Empty;
                    ViewState["VendorID"] = lstDetails[0].VendorID != null ? Convert.ToInt32(lstDetails[0].VendorID.ToString()) : (int?)null;

                    AddColumns();
                    int iCount = 1;
                    foreach (DiscrepancyBE item in lstDetails)
                    {
                        tbDummy.Rows.Add(item.PurchaseOrder.Line_No,
                                         item.PurchaseOrder.OD_Code,
                                         item.PurchaseOrder.Direct_code,
                                         item.PurchaseOrder.Vendor_Code,
                                         item.PurchaseOrder.ProductDescription,
                                         item.PurchaseOrder.Original_quantity,
                                         item.PurchaseOrder.Outstanding_Qty,
                                         "",
                                         item.PurchaseOrder.UOM,
                                         "Remove",
                                         (iCount++).ToString());
                    }
                    dsDummy.Tables.Add(tbDummy);
                    gvReservation.DataSource = dsDummy;
                    gvReservation.DataBind();
                    ViewState["myDataTable"] = tbDummy;

                    EnableDisableGridControls();

                    DiscrepancyBE DiscrepancyBE = new DiscrepancyBE();
                    DiscrepancyBAL DiscrepancyBAL = new DiscrepancyBAL();

                    DiscrepancyBE.Action = "GetStockPlannerOnHolidayCover"; // getting details of SP on Holiday Cover if any

                    if (ViewState["StockPlannerID"] != null)
                        DiscrepancyBE.StockPlannerID = Convert.ToInt32(ViewState["StockPlannerID"].ToString());

                    DiscrepancyBE.SiteID = Convert.ToInt32(ucSite.innerControlddlSite.SelectedItem.Value);
                    List<DiscrepancyBE> lstStockPlannerOnHolidayCover = DiscrepancyBAL.GetStockPlannerOnHolidayCoverBAL(DiscrepancyBE);

                    if (lstStockPlannerOnHolidayCover.Count > 0)
                    {
                        ViewState["StockPlannerToCoverEmailID"] = lstStockPlannerOnHolidayCover[0].StockPlannerToCoverEmailID;
                        ViewState["SPToCoverStockPlannerID"] = lstStockPlannerOnHolidayCover[0].SPToCoverStockPlannerID;
                    }
                }
            }
            else
            {
                UP_VendorBE oUP_VendorBE = new UP_VendorBE();
                UP_VendorBAL oUP_VendorBAL = new UP_VendorBAL();

                oUP_VendorBE.VendorID = Convert.ToInt32(ucSeacrhVendor1.VendorNo);
                oUP_VendorBE.Action = "ShowVendorByID";

                List<UP_VendorBE> lstDetails = new List<UP_VendorBE>();
                lstDetails = oUP_VendorBAL.GetVendorByIdBAL(oUP_VendorBE);
                oUP_VendorBAL = null;
                if (lstDetails.Count > 0 && lstDetails != null)
                {

                    lblVendorNumberValue.Text = lstDetails[0].VendorName.ToString();
                    lblContactValue.Text = UIUtility.GetUserPhoneNoofVendor(Convert.ToInt32(lstDetails[0].VendorID), Convert.ToInt32(ucSite.innerControlddlSite.SelectedItem.Value));

                    lblStockPlannerNoValue.Text = string.Format("{0} - {1}", lstDetails[0].StockPlannerNumber, lstDetails[0].StockPlannerName);
                    lblPlannerContactNoValue.Text = lstDetails[0].StockPlannerContact.ToString();

                    ViewState["StockPlannerID"] = lstDetails[0].StockPlannerID != null ? Convert.ToInt32(lstDetails[0].StockPlannerID.ToString()) : (int?)null;
                    ViewState["VendorID"] = lstDetails[0].VendorID != null ? Convert.ToInt32(lstDetails[0].VendorID.ToString()) : (int?)null;
                    ucSDRCommunication1.VendorID = Convert.ToInt32(lstDetails[0].VendorID.ToString());
                    if (!string.IsNullOrEmpty(ucSite.innerControlddlSite.SelectedValue))
                        ucSDRCommunication1.SiteID = Convert.ToInt32(ucSite.innerControlddlSite.SelectedValue);
                    else
                        ucSDRCommunication1.SiteID = 0;
                    ucSDRCommunication1.GetDetails();

                    oDiscrepancyBE.Action = "GetStockPlannerOnHolidayCover"; // getting details of SP on Holiday Cover if any

                    if (ViewState["StockPlannerID"] != null)
                        oDiscrepancyBE.StockPlannerID = Convert.ToInt32(ViewState["StockPlannerID"].ToString());

                    oDiscrepancyBE.SiteID = Convert.ToInt32(ucSite.innerControlddlSite.SelectedItem.Value);
                    List<DiscrepancyBE> lstStockPlannerOnHolidayCover = oDiscrepancyBAL.GetStockPlannerOnHolidayCoverBAL(oDiscrepancyBE);

                    if (lstStockPlannerOnHolidayCover.Count > 0)
                    {
                        ViewState["StockPlannerToCoverEmailID"] = lstStockPlannerOnHolidayCover[0].StockPlannerToCoverEmailID;
                        ViewState["SPToCoverStockPlannerID"] = lstStockPlannerOnHolidayCover[0].SPToCoverStockPlannerID;
                    }

                }
            }
        }
    }

    private void GetPurchaseOrderList()
    {
        DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
        DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();

        oDiscrepancyBE.Action = "GetPurchaseOrderDate";
        oDiscrepancyBE.SiteID = Convert.ToInt32(ucSite.innerControlddlSite.SelectedValue);
        oDiscrepancyBE.PurchaseOrderNumber = txtPurchaseOrder.Text.Trim();

        List<DiscrepancyBE> lstDetails = oDiscrepancyBAL.GetPurchaseOrderDateListBAL(oDiscrepancyBE);
        ViewState["GetPODateList"] = lstDetails;
        oDiscrepancyBAL = null;
        if (lstDetails.Count > 0 && lstDetails != null)
        {
            FillControls.FillDropDown(ref ddlPurchaseOrderDate, lstDetails, "PurchaseOrder_Order_raised", "RowNumber");
            if (!string.IsNullOrEmpty(ddlPurchaseOrderDate.SelectedItem.Value))
            {
                var PODateDetail = lstDetails.Find(p => p.RowNumber == Convert.ToInt32(ddlPurchaseOrderDate.SelectedItem.Value));
                lblVendorValue.Text = PODateDetail.VendorNoName;
                iVendorID = Convert.ToInt32(PODateDetail.VendorID);
                ucSDRCommunication1.VendorID = Convert.ToInt32(PODateDetail.VendorID);
            }
        }
        else
        {
            if (rdoPO1.Checked)
            {
                string saveMessage = WebCommon.getGlobalResourceValue("InvalidPurchaseOrder");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('" + saveMessage + "')", true);
            }
        }
    }

    private void EnableDisableGridControls()
    {
        foreach (GridViewRow item in gvReservation.Rows)
        {

            ucTextbox txtLineNo = (ucTextbox)item.FindControl("txtLineNo");
            ucTextbox txtOfficeDepotCode = (ucTextbox)item.FindControl("txtOfficeDepotCode");
            ucTextbox txtVikingCode = (ucTextbox)item.FindControl("txtVikingCode");
            ucTextbox txtVendorItemCode = (ucTextbox)item.FindControl("txtVendorItemCode");
            ucTextbox txtDescription = (ucTextbox)item.FindControl("txtDescription");
            ucTextbox txtReceivedQty = (ucTextbox)item.FindControl("txtReceivedQty");
            ucTextbox txtUoM = (ucTextbox)item.FindControl("txtUoM");
            ucButton btnRemove = (ucButton)item.FindControl("btnRemove");
            ucTextbox txtOriginalPOQuantity = (ucTextbox)item.FindControl("txtOriginalPOQuantity");
            ucTextbox txtOutstandingQty = (ucTextbox)item.FindControl("txtOutstandingQty");


            btnRemove.Attributes.Remove("style");

            if (hdnSelectOption.Value == "1")
            {
                txtLineNo.Enabled = false;
                txtOfficeDepotCode.Enabled = false;
                txtVikingCode.Enabled = false;
                txtVendorItemCode.Enabled = false;
                txtDescription.Enabled = false;
                if (txtOriginalPOQuantity != null)
                    txtOriginalPOQuantity.Enabled = false;
                if (txtOutstandingQty != null)
                    txtOutstandingQty.Enabled = false;
                txtUoM.Enabled = false;
                btnRemove.Style["display"] = "none";
            }
            else if (hdnSelectOption.Value == "2")
            {
                //check for the OD_Code if correct then disable the controls else enable them
                DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
                DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();

                oDiscrepancyBE.Action = "GetODProductDetails";
                oDiscrepancyBE.ODSKUCode = txtOfficeDepotCode.Text.Trim();
                oDiscrepancyBE.SiteID = Convert.ToInt32(ucSite.innerControlddlSite.SelectedValue);
                List<DiscrepancyBE> lstODProductDetails = oDiscrepancyBAL.GetODProductDetailsBAL(oDiscrepancyBE);
                if (lstODProductDetails != null
                    && lstODProductDetails.Count > 0
                    && txtOfficeDepotCode.Text.Trim() != "")
                {
                    txtLineNo.Enabled = false;
                    txtOfficeDepotCode.Enabled = false;
                    txtVikingCode.Enabled = false;
                    txtVendorItemCode.Enabled = false;
                    txtDescription.Enabled = false;
                    if (txtOriginalPOQuantity != null)
                        txtOriginalPOQuantity.Enabled = false;
                    if (txtOutstandingQty != null)
                        txtOutstandingQty.Enabled = false;
                    txtUoM.Enabled = false;
                }
                else
                {

                    txtLineNo.Enabled = true;
                    txtOfficeDepotCode.Enabled = true;
                    txtVikingCode.Enabled = true;
                    txtVendorItemCode.Enabled = true;
                    txtDescription.Enabled = true;
                    if (txtOriginalPOQuantity != null)
                        txtOriginalPOQuantity.Enabled = true;
                    if (txtOutstandingQty != null)
                        txtOutstandingQty.Enabled = true;
                    txtUoM.Enabled = true;
                }
                btnRemove.Style["display"] = "";
            }
        }
    }

    private void IncreaseRow()
    {
        int Total = gvReservation.Rows.Count;
        int iCount = 1;
        Total = Total + 1; //increase 1 because of add
        AddColumns();
        foreach (GridViewRow item in gvReservation.Rows)
        {


            ucTextbox txtLineNo = (ucTextbox)item.FindControl("txtLineNo");
            ucTextbox txtOfficeDepotCode = (ucTextbox)item.FindControl("txtOfficeDepotCode");
            ucTextbox txtVikingCode = (ucTextbox)item.FindControl("txtVikingCode");
            ucTextbox txtVendorItemCode = (ucTextbox)item.FindControl("txtVendorItemCode");
            ucTextbox txtDescription = (ucTextbox)item.FindControl("txtDescription");
            ucTextbox txtOriginalPOQuantity = (ucTextbox)item.FindControl("txtOriginalPOQuantity");
            ucTextbox txtOutstandingQty = (ucTextbox)item.FindControl("txtOutstandingQty");

            ucTextbox txtReceivedQty = (ucTextbox)item.FindControl("txtReceivedQty");
            ucTextbox txtUoM = (ucTextbox)item.FindControl("txtUoM");
            ucButton btnRemove = (ucButton)item.FindControl("btnRemove");


            tbDummy.Rows.Add(txtLineNo.Text.Trim(),
                                 txtOfficeDepotCode.Text.Trim(),
                                 txtVikingCode.Text.Trim(),
                                 txtVendorItemCode.Text.Trim(),
                                 txtDescription.Text.Trim(),
                                 txtOriginalPOQuantity.Text.Trim(),
                                 txtOutstandingQty.Text.Trim(),
                                 txtReceivedQty.Text.Trim(),
                                 txtUoM.Text.Trim(),
                                 "Remove",
                                 (iCount++).ToString());


        }

        tbDummy.Rows.Add(string.Empty,
                                 string.Empty,
                                 string.Empty,
                                 string.Empty,
                                 string.Empty,
                                 string.Empty,
                                 string.Empty,
                                 string.Empty,
                                 string.Empty,
                                 "Remove",
                                 (iCount++).ToString());


        BindWithGrid();

        EnableDisableGridControls();
    }

    private void AddUserControl()
    {
        DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
        DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();
        oDiscrepancyBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
        string CaseOf = String.Empty;
        if (Session["Role"] != null && Session["Role"].ToString().Trim() == "Vendor")
        {
            btnUpdateLocation.Visible = false;
        }
        oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID"));

        if (GetQueryStringValue("RoleTypeFlag") == "A" || GetQueryStringValue("RoleTypeFlag") == null)
        {
            if (Session["UserID"] == null || Session["UserID"].ToString() == "" || Session["Role"] == null || Session["Role"].ToString().Trim() == "")
                return;

            oDiscrepancyBE.User.UserID = Convert.ToInt32(Session["UserID"]);
            CaseOf = Session["Role"].ToString().Trim();
        }
        else
        {
            switch (GetQueryStringValue("RoleTypeFlag").ToString())
            {
                case "G":
                    CaseOf = "OD - Goods In";
                    break;
                case "S":
                    CaseOf = "OD - Stock Planner";
                    break;
                case "P":
                    CaseOf = "OD - Accounts Payable";
                    break;
                case "V":
                    CaseOf = "Vendor";
                    break;
            }
        }

        if (CaseOf == "OD - Stock Planner")
        {
            oDiscrepancyBE.Action = "CheckForInventory";

            List<DiscrepancyBE> lstDetails = oDiscrepancyBAL.GetUserDetailsForWorkFlowBAL(oDiscrepancyBE);
            if (lstDetails != null && lstDetails.Count > 0)
            {
                //check for inventory user
                oDiscrepancyBE.Action = "CheckForActionRequired";
                oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID"));
                oDiscrepancyBE.INVActionRequired = true;
                DataTable dtActionDetails = oDiscrepancyBAL.GetActionRequiredDetailsForWorkFlowBAL(oDiscrepancyBE);
                if (dtActionDetails != null && dtActionDetails.Rows.Count > 0)
                {
                    string sUserControl = "../UserControl/INVAction/" + dtActionDetails.Rows[0]["INVUserControl"].ToString().Trim()
                                            + ".aspx?disLogID=" + GetQueryStringValue("disLogID").ToString()
                                            + "&DisWFlowID=" + dtActionDetails.Rows[0]["DiscrepancyWorkflowID"].ToString()
                                            + "&PONo=" + dtActionDetails.Rows[0]["PurchaseOrderNumber"].ToString()
                                            + "&SiteID=" + SiteID.ToString()
                                            + "&VDRNo=" + VDRNo
                                            + "&FromPage=NoPO"
                                        + "&PreviousPage=" + GetQueryStringValue("PreviousPage").ToString();
                    ifUserControl.Attributes.Add("src", EncryptURL(sUserControl));
                }
                else
                {
                    ifUserControl.Style["display"] = "none";
                }
            }
        }
        else if (CaseOf == "OD - Goods In")
        {
            //check for goods in

            oDiscrepancyBE.Action = "CheckForActionRequired";
            oDiscrepancyBE.GINActionRequired = true;

            DataTable dtActionDetails = oDiscrepancyBAL.GetActionRequiredDetailsForWorkFlowBAL(oDiscrepancyBE);
            if (dtActionDetails != null && dtActionDetails.Rows.Count > 0)
            {

                string sUserControl = "../UserControl/GINAction/" + dtActionDetails.Rows[0]["GINUserControl"].ToString().Trim()
                                        + ".aspx?disLogID=" + GetQueryStringValue("disLogID").ToString()
                                        + "&DisWFlowID=" + dtActionDetails.Rows[0]["DiscrepancyWorkflowID"].ToString()
                                        + "&PONo=" + dtActionDetails.Rows[0]["PurchaseOrderNumber"].ToString()
                                        + "&SiteID=" + SiteID.ToString()
                                        + "&VDRNo=" + VDRNo + "&FromPage=NoPO"
                                        + "&PreviousPage=" + GetQueryStringValue("PreviousPage").ToString();
                ifUserControl.Attributes.Add("src", EncryptURL(sUserControl));


            }
            else
            {
                ifUserControl.Style["display"] = "none";
            }
        }
        else if (CaseOf == "OD - Accounts Payable")
        {
            //check for account payable in

            oDiscrepancyBE.Action = "CheckForActionRequired";
            oDiscrepancyBE.DiscrepancyWorkFlowID = !string.IsNullOrEmpty(GetQueryStringValue("WorkFlowID")) ? Convert.ToInt32(GetQueryStringValue("WorkFlowID")) : (int?)null;
            oDiscrepancyBE.APActionRequired = true;

            DataTable dtActionDetails = oDiscrepancyBAL.GetActionRequiredDetailsForWorkFlowBAL(oDiscrepancyBE);
            if (dtActionDetails != null && dtActionDetails.Rows.Count > 0)
            {
                string sUserControl = "../UserControl/APAction/" + dtActionDetails.Rows[0]["APUserControl"].ToString().Trim()
                                        + ".aspx?disLogID=" + GetQueryStringValue("disLogID").ToString()
                                        + "&DisWFlowID=" + dtActionDetails.Rows[0]["DiscrepancyWorkflowID"].ToString()
                                        + "&PONo=" + dtActionDetails.Rows[0]["PurchaseOrderNumber"].ToString()
                                        + "&SiteID=" + SiteID.ToString()
                                        + "&VDRNo=" + VDRNo
                                        + "&FromPage=NoPO"
                                        + "&PreviousPage=" + GetQueryStringValue("PreviousPage").ToString();
                ifUserControl.Attributes.Add("src", EncryptURL(sUserControl));
                btnAccountsPayableAction.Visible = true;
            }
            else
            {
                ifUserControl.Style["display"] = "none";
            }
        }
        else if (CaseOf == "Vendor")
        {
            //check for vendor in
            pnlLocation.Visible = false;
            oDiscrepancyBE.Action = "CheckForActionRequiredForVendor";
            oDiscrepancyBE.VenActionRequired = true;

            DataTable dtActionDetails = oDiscrepancyBAL.GetActionRequiredDetailsForWorkFlowBAL(oDiscrepancyBE);
            if (dtActionDetails != null && dtActionDetails.Rows.Count > 0)
            {

                string sUserControl = "../UserControl/VENAction/" + dtActionDetails.Rows[0]["VENUserControl"].ToString().Trim()
                                        + ".aspx?disLogID=" + GetQueryStringValue("disLogID").ToString()
                                        + "&DisWFlowID=" + dtActionDetails.Rows[0]["DiscrepancyWorkflowID"].ToString()
                                        + "&PONo=" + dtActionDetails.Rows[0]["PurchaseOrderNumber"].ToString()
                                        + "&SiteID=" + SiteID.ToString()
                                        + "&VDRNo=" + VDRNo + "&FromPage=NoPO"
                                        + "&PreviousPage=" + GetQueryStringValue("PreviousPage").ToString();
                ifUserControl.Attributes.Add("src", EncryptURL(sUserControl));
            }
            else
            {
                ifUserControl.Style["display"] = "none";

            }
        }
        else
        {
            ifUserControl.Style["display"] = "none";
            btnAccountsPayableAction.Visible = true;
        }
        oDiscrepancyBAL = null;
    }

    private void InsertHTML()
    {
        /* Check for the site "No Escalation" */
        MAS_SiteBE oMAS_SiteBE = new MAS_SiteBE();
        MAS_SiteBAL oMAS_SiteBAL = new MAS_SiteBAL();
        oMAS_SiteBE.Action = "GetSiteDisSettings";
        oMAS_SiteBE.SiteID = Convert.ToInt32(ucSite.innerControlddlSite.SelectedItem.Value);
        oMAS_SiteBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();

        List<MAS_SiteBE> lstSiteDetails = oMAS_SiteBAL.GetSiteMisSettingBAL(oMAS_SiteBE);
        oMAS_SiteBAL = null;
        if (lstSiteDetails != null && lstSiteDetails.Count > 0 && lstSiteDetails[0].DIS_CommunicationWithEscalationType.Trim() != "N")
        {
            //site "No Escalation" is not on 
            DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
            DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();
            WorkflowHTML oWorkflowHTML = new WorkflowHTML();

            oDiscrepancyBE.GINHTML = oWorkflowHTML.function1(DateTime.Now, null, null, "Yellow", string.Empty,
                "Reservation Discrepancy created",

                UIUtility.GetODVendorUserName(Convert.ToString(Session["Role"]), Convert.ToString(Session["UserName"]), Convert.ToString(Session["LoginID"])),
                txtInternalComments.Text);
            oDiscrepancyBE.GINActionRequired = false;

            string StockPlannerEmailID = ViewState["StockPlannerEmailID"] == null ? "" : Convert.ToString(ViewState["StockPlannerEmailID"]);

            string StockPlannerToCoverEmailID = ViewState["StockPlannerToCoverEmailID"] == null ? "" : Convert.ToString(ViewState["StockPlannerToCoverEmailID"]);

            if (StockPlannerToCoverEmailID != "")
            {
                oDiscrepancyBE.INVHTML = oWorkflowHTML.function4("Blue", "", DateTime.Now, StockPlannerToCoverEmailID, "", string.Empty, ReservationInvDesc);
            }
            else
            {
                oDiscrepancyBE.INVHTML = oWorkflowHTML.function4("Blue", "", DateTime.Now, StockPlannerEmailID, "", string.Empty, ReservationInvDesc);
            }

            oDiscrepancyBE.INVActionRequired = true;
            oDiscrepancyBE.INVUserControl = "INV012";

            oDiscrepancyBE.APHTML = oWorkflowHTML.function3("White", null, "", "");
            oDiscrepancyBE.APActionRequired = false;

            oDiscrepancyBE.VENHTML = oWorkflowHTML.function3("White", null, "", "");
            oDiscrepancyBE.VenActionRequired = false;

            oDiscrepancyBE.Action = "InsertHTML";
            oDiscrepancyBE.DiscrepancyLogID = DiscrepancyLogID;
            oDiscrepancyBE.LoggedDateTime = DateTime.Now;
            oDiscrepancyBE.LevelNumber = 1;
            oDiscrepancyBE.ActionTakenBy = "GIN";
            oDiscrepancyBE.InventoryActionCategory = "AR";  //Action Required
            int? iResult = oDiscrepancyBAL.addWorkFlowHTMLsBAL(oDiscrepancyBE);
        }
        else
        {
            //step 1b- close the discrepancy
            DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
            DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();
            WorkflowHTML oWorkflowHTML = new WorkflowHTML();
            oDiscrepancyBE.GINHTML = oWorkflowHTML.function1(DateTime.Now, null, null, "Yellow", string.Empty,
                "Reservation Discrepancy created",
                UIUtility.GetODVendorUserName(Convert.ToString(Session["Role"]), Convert.ToString(Session["UserName"]), Convert.ToString(Session["LoginID"])),
                txtInternalComments.Text);
            oDiscrepancyBE.GINActionRequired = false;

            oDiscrepancyBE.INVHTML = oWorkflowHTML.function3("White", null);
            oDiscrepancyBE.INVActionRequired = false;

            oDiscrepancyBE.APHTML = oWorkflowHTML.function3("White", null);
            oDiscrepancyBE.APActionRequired = false;

            oDiscrepancyBE.VENHTML = oWorkflowHTML.function3("White", null);
            oDiscrepancyBE.VenActionRequired = false;

            oDiscrepancyBE.Action = "InsertHTML";
            oDiscrepancyBE.DiscrepancyLogID = DiscrepancyLogID;
            oDiscrepancyBE.LoggedDateTime = DateTime.Now;
            oDiscrepancyBE.LevelNumber = 1;
            oDiscrepancyBE.CloseDiscrepancy = true;
            int? iResult = oDiscrepancyBAL.addWorkFlowHTMLsBAL(oDiscrepancyBE);
            oDiscrepancyBAL = null;
        }
    }

    private int? sendAndSaveDiscrepancyCommunication(int iDiscrepancy)
    {
        int? vendorID = null;
        if (rdoPO1.Checked)
            vendorID = iVendorID;
        else
            vendorID = Convert.ToInt32(ucSeacrhVendor1.VendorNo);

        if (((RadioButton)ucSDRCommunication1.FindControl("rdoEmailComm")) != null && ((RadioButton)ucSDRCommunication1.FindControl("rdoEmailComm")).Checked)
        {
            if (((BaseControlLibrary.ucTextbox)ucSDRCommunication1.FindControl("txtAltEmail")) != null && ((BaseControlLibrary.ucTextbox)ucSDRCommunication1.FindControl("ucVendorEmailList")) != null)
            {
                sendCommunication communication = new sendCommunication();
                int? retVal = communication.sendCommunicationByEMail(iDiscrepancy, "reservation", ((BaseControlLibrary.ucTextbox)ucSDRCommunication1.FindControl("txtAltEmail")), ((BaseControlLibrary.ucTextbox)ucSDRCommunication1.FindControl("ucVendorEmailList")), vendorID); // edited discrepancy type argument to [reservation] for sending initial communication mail to SP.
                sSendMailLink = communication.sSendMailLink;
                return retVal;
            }
        }
        return null;
    }

    /*----------------ADDING RAISE QUERY METHODS------------*/

    private bool IsAlreadyDisputeExist(int discrepancyLogID = 0)
    {
        var isExist = false;

        if (discrepancyLogID != 0)
        {
            var queryDiscrepancyBAL = new DISLog_QueryDiscrepancyBAL();
            var queryDiscrepancyBE = new DISLog_QueryDiscrepancyBE();
            if (discrepancyLogID > 0)
            {
                queryDiscrepancyBE.Action = "IsAlreadyDisputeExist";
                queryDiscrepancyBE.DiscrepancyLogID = discrepancyLogID;
                var result = queryDiscrepancyBAL.IsAlreadyDisputeExistBAL(queryDiscrepancyBE);
                if (result > 0)
                    isExist = true;
            }
        }

        return isExist;
    }
    /* CHECK QUERY IS CLOSED THEN VENDOR CAN RAISE QUERY*/
    private bool IsQueryClosed(int discrepancyLogID = 0)
    {
        var isExist = false;

        if (discrepancyLogID != 0)
        {
            var queryDiscrepancyBAL = new DISLog_QueryDiscrepancyBAL();
            var queryDiscrepancyBE = new DISLog_QueryDiscrepancyBE();
            if (discrepancyLogID > 0)
            {
                queryDiscrepancyBE.Action = "IsAlreadyDisputeExist";
                queryDiscrepancyBE.DiscrepancyLogID = discrepancyLogID;
                var result = queryDiscrepancyBAL.GetQueryClose(queryDiscrepancyBE);
                if (result != 0)
                    isExist = true;
            }
        }

        return isExist;
    }
    /* METHODS FOR VENDOR'S QUERY OVERVIEW*/
    private void VendorsQueryOverview()
    {

        var queryDiscrepancyBAL = new DISLog_QueryDiscrepancyBAL();
        var queryDiscrepancyBE = new DISLog_QueryDiscrepancyBE();
        var disLogID = GetQueryStringValue("disLogID") != null ? Convert.ToInt32(GetQueryStringValue("disLogID")) : 0;

        if (disLogID > 0)
        {
            queryDiscrepancyBE.Action = "GetQueryDiscrepancy";

            queryDiscrepancyBE.DiscrepancyLogID = disLogID;
            var getQueryDiscrepancy = queryDiscrepancyBAL.GetQueryDiscrepancyBAL(queryDiscrepancyBE);
            if (getQueryDiscrepancy.Count > 0)
            {
                pnlQueryOverview_1.Visible = true;
                rptQueryOverview.DataSource = getQueryDiscrepancy;
                rptQueryOverview.DataBind();

            }
        }
    }
    protected void rptQueryOverview_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            var hdnIsQueryClosedManually = (HiddenField)e.Item.FindControl("hdnIsQueryClosedManually");

            // GoodsIn declaration ...            
            var lblCommentLabelT_2 = (ucLabel)e.Item.FindControl("lblCommentLabelT_2");
            if (lblCommentLabelT_2 != null)
            {
                if (!string.IsNullOrEmpty(lblCommentLabelT_2.Text)) { e.Item.FindControl("tblGoodsIn").Visible = true; }
                else { e.Item.FindControl("tblGoodsIn").Visible = false; }

                if (!string.IsNullOrEmpty(lblCommentLabelT_2.Text) && hdnIsQueryClosedManually.Value == "True")
                {
                    e.Item.FindControl("tblGoodsIn").Visible = false;
                    e.Item.FindControl("tblQueryClosedManually").Visible = true;
                }

                var lnkView = (System.Web.UI.HtmlControls.HtmlAnchor)e.Item.FindControl("lnkView");
                var hdnQueryDiscrepancyID = (HiddenField)e.Item.FindControl("hdnQueryDiscrepancyID");
                var hdnDiscrepancyLogID = (HiddenField)e.Item.FindControl("hdnDiscrepancyLogID");
                lnkView.Attributes.Add("href", EncryptQuery("~/ModuleUI/Appointment/Receiving/APPRcv_Communication.aspx?DiscId=" + hdnDiscrepancyLogID.Value
                    + "&QueryDiscId=" + hdnQueryDiscrepancyID.Value));
            }
        }
    }

    #endregion
    protected void btnUpdateLocation_Click(object sender, EventArgs e)
    {
        var disLogID = GetQueryStringValue("disLogID") != null ? Convert.ToInt32(GetQueryStringValue("disLogID")) : 0;
        if (disLogID >0 )
        {
            DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
            DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();
            oDiscrepancyBE.Action = "UpdateLocation";
            oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(disLogID);
            oDiscrepancyBE.Location = txtLocation.Text.Trim();
            oDiscrepancyBE.UserID = Convert.ToInt32(Session["UserID"]);
            string sResult = oDiscrepancyBAL.updateDiscrepancyLocationBAL(oDiscrepancyBE);
            if (GetQueryStringValue("PreviousPage") != null)
            {
                if (GetQueryStringValue("PreviousPage").Trim().ToUpper().Equals("SearchResultVendor".ToUpper()))
                {
                    btnBack_1.Visible = false;
                    btnUpdateLocation.Visible = false;
                    EncryptQueryString("~/ModuleUI/Discrepancy/DIS_SearchResultVendor.aspx?disLogID=" + Convert.ToString(GetQueryStringValue("disLogID"))
                                + "&VDRNo=" + Convert.ToString(GetQueryStringValue("VDRNo"))
                                + "&UserID=" + Convert.ToString(GetQueryStringValue("UserID"))
                                + "&PreviousPage=SearchResultVendor&WorkFlowID=" + Convert.ToString(GetQueryStringValue("WorkFlowID").ToString()));
                }

                if (Session["PageMode"].ToString() == "Todays")
                {
                    EncryptQueryString("../DIS_SearchResult.aspx?FromPage=images&SiteId=" + GetQueryStringValue("SiteId") + "&DisDate=" + GetQueryStringValue("DisDate"));
                }
                else if (Session["PageMode"].ToString() == "Search")
                {
                    //For opening search discrepancies mode
                    btnBack_1.Visible = false;
                    btnUpdateLocation.Visible = false;
                    EncryptQueryString("~/ModuleUI/Discrepancy/DIS_SearchResult.aspx?disLogID=" + Convert.ToString(GetQueryStringValue("disLogID"))
                                + "&VDRNo=" + Convert.ToString(GetQueryStringValue("VDRNo"))
                                + "&UserID=" + Convert.ToString(GetQueryStringValue("UserID"))
                                + "&PreviousPage=SearchResult&WorkFlowID=" + Convert.ToString(GetQueryStringValue("WorkFlowID").ToString()));

                }
            }
            else if (GetQueryStringValue("PN") != null)
            {
                if (GetQueryStringValue("PN").Trim().ToUpper().Equals("POINQ"))
                {
                    btnBack_1.Visible = false;
                    btnUpdateLocation.Visible = false;
                    EncryptQueryString("~/ModuleUI/Appointment/PurchaseOrderInquiry.aspx?SId=" + GetQueryStringValue("SId").ToString()
                        + "&Status=" + GetQueryStringValue("Status").ToString()
                        + "&PN=DISLOG"
                        + "&PO=" + GetQueryStringValue("PO").ToString());
                }
                else if (GetQueryStringValue("PN").Trim().ToUpper().Equals("OPENPL"))
                {
                    EncryptQueryString("~/ModuleUI/StockOverview/Expedite/OpenPurchaseListing.aspx?action=showlisting");
                }


            }
        }
    }

    protected void btnContinue_Click(object sender, EventArgs e)
    {
        string EncryptcUrl = EncryptQuery("~/ModuleUI/Discrepancy/DISLog_QueryDiscrepancy.aspx?disLogID=" + Convert.ToString(GetQueryStringValue("disLogID"))
                                 + "&VDRNo=" + Convert.ToString(GetQueryStringValue("VDRNo")) + "&RaiseQueryVendor=Yes" + "&RedirectTo=DisLog_Overs.aspx");
        Response.Redirect(EncryptcUrl, false);
        Context.ApplicationInstance.CompleteRequest();
    }

    protected void btnRaiseQueryPopupBack_Click(object sender, EventArgs e)
    {
        btnBack.Visible = false;
        btnSave.Visible = false;
    }
    private void BindDiscrepancyLogComment(DiscrepancyBE discrepancyBE = null)
    {
        var discrepancyBAL = new DiscrepancyBAL();
        if (discrepancyBE != null)
        {
            rptDiscrepancyComments.DataSource = discrepancyBAL.GetDiscrepancyLogCommentsBAL(discrepancyBE);
        }
        else
        {
            discrepancyBE = new DiscrepancyBE();
            discrepancyBE.Action = "GetDiscrepancyLogComment";

            if (!string.IsNullOrEmpty(GetQueryStringValue("disLogID")))
                discrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID"));
            else
                discrepancyBE.DiscrepancyLogID = 0;

            rptDiscrepancyComments.DataSource = discrepancyBAL.GetDiscrepancyLogCommentsBAL(discrepancyBE);
        }

        rptDiscrepancyComments.DataBind();

        if (rptDiscrepancyComments.Items.Count > 0)
            lblComments.Visible = true;
        else
            lblComments.Visible = false;
    }
}
﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using BaseControlLibrary;
using BusinessEntities.ModuleBE.AdminFunctions;
using BusinessEntities.ModuleBE.Discrepancy.LogDiscrepancy;
using BusinessLogicLayer.ModuleBAL.AdminFunctions;
using BusinessLogicLayer.ModuleBAL.Discrepancy.LogDiscrepancy;
using Utilities;
using WebUtilities;
using System.Web;
public partial class DISLog_GoodsReceivedDamaged : CommonPage
{

    #region Global variables

    protected string DamagedDescriptionValidation = WebCommon.getGlobalResourceValue("DamagedDescriptionValidation");
    protected string DamagedQtyValidation = WebCommon.getGlobalResourceValue("DamagedQtyValidation");
    protected string InvalidShortageValidation = WebCommon.getGlobalResourceValue("InvalidShortageValidation");
    protected string InvalidValuesEntered = WebCommon.getGlobalResourceValue("InvalidValuesEntered");
    protected string AtLeastOne = WebCommon.getGlobalResourceValue("AtLeastOne");

    //int DiscrepancyLogID = 0;
    //static int SiteID = 0;

    public int iVendorID
    {
        get
        {
            return ViewState["iVendorID"] != null ? Convert.ToInt32(ViewState["iVendorID"].ToString()) : 0;
        }
        set
        {
            ViewState["iVendorID"] = value;
        }
    }

    public int DiscrepancyLogID
    {
        get
        {
            return ViewState["DiscrepancyLogID"] != null ? Convert.ToInt32(ViewState["DiscrepancyLogID"].ToString()) : 0;
        }
        set
        {
            ViewState["DiscrepancyLogID"] = value;
        }
    }

    public int SiteID
    {
        get
        {
            return ViewState["SiteID"] != null ? Convert.ToInt32(ViewState["SiteID"].ToString()) : 0;
        }
        set
        {
            ViewState["SiteID"] = value;
        }
    }


    static string VDRNo = string.Empty;
    System.Text.StringBuilder sSendMailLink = new System.Text.StringBuilder();
    protected string ValidEmail = WebCommon.getGlobalResourceValue("ValidEmail");
    protected string NoEmailSpecifiedMessage = WebCommon.getGlobalResourceValue("NoEmailSpecifiedMessage");
    protected string DiscrepancyAlreadyCreated = WebCommon.getGlobalResourceValue("DiscrepancyAlreadyCreated");
    string InitialCommunicationSent = WebCommon.getGlobalResourceValue("InitialCommunicationSent");
    string INV003Desc = WebCommon.getGlobalResourceValue("INV003Desc");
    string FollowingRequestMessage = WebCommon.getGlobalResourceValue("FollowingRequestMessage");
    /*---------------SHOW ERROR MESSAGE WHEN QUERY IS ALREADY OPEN--------------*/
    string strThereIsCurrentlyAlreadyAnOpenQueryBeingInvestigated = WebCommon.getGlobalResourceValue("ThereIsCurrentlyAlreadyAnOpenQueryBeingInvestigated");
    string strPleaseWaitUntilYouHaveReceivedAReplyForThisBefore = WebCommon.getGlobalResourceValue("PleaseWaitUntilYouHaveReceivedAReplyForThisBefore");

    protected string DeleteBlankWorkflowEntriesText = WebCommon.getGlobalResourceValue("DeleteBlankWorkflowEntriesText");
    int? DeleteDiscrepancyTest;
    string sResult = string.Empty;
    bool bSendMail = false;
    int iResult;
    #endregion

    #region Events

    protected void Page_InIt(object sender, EventArgs e)
    {
        ucSite.CurrentPage = this;
        ucApAction.CurrentPage = this;
    }
    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);
        if (!IsPostBack)
        {
            if (GetQueryStringValue("VDRNo") != null && !string.IsNullOrWhiteSpace(GetQueryStringValue("VDRNo").ToString()))
            {
                if (GetQueryStringValue("PN") != null && !string.IsNullOrWhiteSpace(GetQueryStringValue("PN")))
                {
                    if (GetQueryStringValue("PN").Trim().ToUpper().Equals("POINQ") || GetQueryStringValue("PN").Trim().ToUpper().Equals("OPENPL"))
                    {
                        btnBack_1.Visible = true;
                    }
                    else if (GetQueryStringValue("PN").Trim().ToUpper().Equals("SEARCHDEBIT") || (GetQueryStringValue("PN").Trim().ToUpper().Equals("CANCELDEBIT")))
                    {
                        btnBack_AP.Visible = true;
                    }
                }
                else if (GetQueryStringValue("PreviousPage") != null)
                {
                    string iframeStyleStatus = ifUserControl.Style["display"];
                    if (GetQueryStringValue("PreviousPage").Trim().ToUpper().Equals("SearchResultVendor".ToUpper()) &&
                        iframeStyleStatus != null)
                        btnBack_1.Visible = true;
                    else if (GetQueryStringValue("PreviousPage").Trim().Equals("SearchResult") &&
                       iframeStyleStatus != null)
                    {
                        btnBack_1.Visible = true;
                    }
                    else
                    {
                        btnUpdateLocation.Visible = false;
                        btnBack_1.Visible = false;                       
                    }
                       
                }
                chkGoodReturnedDriver.Enabled = false;
            }
            else
            {
                UIUtility.PageValidateForODUser();
            }
        }

        if (GetQueryStringValue("VDRNo") != null && !string.IsNullOrWhiteSpace(GetQueryStringValue("VDRNo").ToString()) && !lblGoodsReceivedDamaged.Text.ToString().Contains("-"))
        {
            lblGoodsReceivedDamaged.Text = lblGoodsReceivedDamaged.Text + " - " + GetQueryStringValue("VDRNo").ToString();
        }
    }

    protected void btnBackToAP_Click(object sender, EventArgs e)
    {
        if (GetQueryStringValue("PN").Trim().ToUpper().Equals("SEARCHDEBIT") || (GetQueryStringValue("PN").Trim().ToUpper().Equals("CANCELDEBIT")))
        {
            Response.Write("<script language='javascript'>window.open('', '_self', '');window.close();</script>");
        }
    }

    protected void btnBack_1_Click(object sender, EventArgs e)
    {
        if (GetQueryStringValue("PreviousPage") != null)
        {
            if (GetQueryStringValue("PreviousPage").Trim().ToUpper().Equals("SearchResultVendor".ToUpper()))
            {
                btnBack_1.Visible = false;
                EncryptQueryString("~/ModuleUI/Discrepancy/DIS_SearchResultVendor.aspx?disLogID=" + Convert.ToString(GetQueryStringValue("disLogID"))
                            + "&VDRNo=" + Convert.ToString(GetQueryStringValue("VDRNo"))
                            + "&UserID=" + Convert.ToString(GetQueryStringValue("UserID"))
                            + "&PreviousPage=SearchResultVendor&WorkFlowID=" + Convert.ToString(GetQueryStringValue("WorkFlowID").ToString()));
            }

            if (Session["PageMode"].ToString() == "Todays")
            {
                EncryptQueryString("../DIS_SearchResult.aspx?FromPage=images&SiteId=" + GetQueryStringValue("SiteId") + "&DisDate=" + GetQueryStringValue("DisDate"));
            }
            else if (Session["PageMode"].ToString() == "Search")
            {
                //For opening search discrepancies mode
                btnBack_1.Visible = false;
                EncryptQueryString("~/ModuleUI/Discrepancy/DIS_SearchResult.aspx?disLogID=" + Convert.ToString(GetQueryStringValue("disLogID"))
                            + "&VDRNo=" + Convert.ToString(GetQueryStringValue("VDRNo"))
                            + "&UserID=" + Convert.ToString(GetQueryStringValue("UserID"))
                            + "&PreviousPage=SearchResult&WorkFlowID=" + Convert.ToString(GetQueryStringValue("WorkFlowID").ToString()));

            }
        }
        else if (GetQueryStringValue("PN") != null)
        {
            if (GetQueryStringValue("PN").Trim().ToUpper().Equals("POINQ"))
            {
                btnBack_1.Visible = false;
                EncryptQueryString("~/ModuleUI/Appointment/PurchaseOrderInquiry.aspx?SId=" + GetQueryStringValue("SId").ToString()
                    + "&Status=" + GetQueryStringValue("Status").ToString()
                    + "&PN=DISLOG"
                    + "&PO=" + GetQueryStringValue("PO").ToString());
            }
            else if (GetQueryStringValue("PN").Trim().ToUpper().Equals("OPENPL"))
            {
                EncryptQueryString("~/ModuleUI/StockOverview/Expedite/OpenPurchaseListing.aspx?action=showlisting");
            }
        }
        else if (GetQueryStringValue("PageType") != null)
        {
            EncryptQueryString("~/ModuleUI/Discrepancy/DIS_LocationReport.aspx");
        }

    }

    protected void Page_Load(object sender, EventArgs e)
    {
        ucApAction.SaveEventValidation += new EventHandler(ucApAction_SaveEventValidation);
        txtPurchaseOrder.Focus();
        btnPrintReturnsNote.Visible = false;

        if (GetQueryStringValue("disLogID") != null)
        {
            mvGoodsReceivedDamaged.ActiveViewIndex = 1;
        }
        if (mvGoodsReceivedDamaged.ActiveViewIndex == 1)
        {
            btnLogDiscrepancy.Visible = false;
            btnBack.Visible = true;
            btnSave.Visible = true;
        }
        else
        {
            btnLogDiscrepancy.Visible = true;
            btnBack.Visible = false;
            btnSave.Visible = false;
        }
        if (!IsPostBack)
        {

            txtDateDeliveryArrived.innerControltxtDate.Value = Convert.ToString(DateTime.Now.ToString("dd/MM/yyyy"));
            lblIssueRaisedDate.Text = Convert.ToString(DateTime.Now.ToString("dd/MM/yyyy HH:MM"));
            VendorsQueryOverview();
            if (GetQueryStringValue("disLogID") != null)
            {
                this.BindDiscrepancyLogComment();
                if (GetQueryStringValue("disLogID") != null && GetQueryStringValue("status").ToLower() == "closed")
                {
                    btnUpdateLocation.Visible = true; 
                }
                else
                {
                    btnUpdateLocation.Visible = true;
                }
                if (GetQueryStringValue("PageType") != null && GetQueryStringValue("PageType") == "Location")
                {
                    btnUpdateLocation.Visible = false;
                    txtLocation.ReadOnly = true;
                    btnBack_1.Visible = true;
                }
                mvGoodsReceivedDamaged.ActiveViewIndex = 1;
                GetDeliveryDetails();
                GetContactPurchaseOrderDetails();
                if (Session["Role"] != null)
                {
                    GetAccessRightRaiseQuery();
                    if (Session["Role"].ToString() == "Vendor")
                    {
                        pnlInternalComments.Visible = false;
                        btnAddComment.Visible = true;
                        btnAddImages.Visible = false;
                        btnAccountsPayableAction.Visible = false;
                        btnRaiseQuery.Visible = true;

                    }
                }
                this.AddUserControl();
                this.ShowACP001ActionButton();
                btnLogDiscrepancy.Visible = false;
                btnBack.Visible = false;
                btnSave.Visible = false;
                if (Session["Role"] != null && (Session["Role"].ToString().ToLower() == "vendor" || Session["Role"].ToString().ToLower() == "carrier"))
                {
                    btnViewWF.Visible = false;
                }
                else
                {
                    btnViewWF.Visible = true;
                }
                // code for generating div for the imageviewer
                string DivGallery = CommonPage.GetImagesStringNewLayout(Convert.ToInt32(GetQueryStringValue("disLogID")));
                if (string.IsNullOrEmpty(DivGallery))
                {
                    btnViewImage.Visible = false;
                }
                else
                {
                    discrepancygallery.InnerHtml = DivGallery.ToString();
                    btnViewImage.Visible = true;
                }
            }
            else
            {
                btnAddComment.Visible = false;
                ifUserControl.Style["display"] = "none";
                btnViewWF.Visible = false;
                btnViewImage.Visible = false;
                btnAddImages.Visible = false;
                btnAccountsPayableAction.Visible = false;
                btnRaiseQuery.Visible = false;
                pnlQueryOverview_1.Visible = false;
            }
        }

        txtPurchaseOrder.Attributes.Add("autocomplete", "off");
        txtPurchaseOrder.Focus();
    }

    void ucApAction_SaveEventValidation(object sender, EventArgs e)
    {
        if (ucApAction.IsModelOpen)
            mdlAPAction.Show();
    }

    public override void SitePost_Load()
    {
        base.SitePost_Load();
        if (!IsPostBack)
        {            
            ucSite.innerControlddlSite.AutoPostBack = true;
        }
    }

    public override void SiteSelectedIndexChanged()
    {
        if (txtPurchaseOrder.Text != string.Empty)
        {
            GetPurchaseOrderList();
        }
    }

    protected void btnLogDiscrepancy_Click(object sender, EventArgs e)
    {
        DateTime dt = Common.GetMM_DD_YYYY(txtDateDeliveryArrived.innerControltxtDate.Value);
        if (dt > DateTime.Now.Date)
        {
            string saveMessage = WebCommon.getGlobalResourceValue("NotDateValidMessage");
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('" + saveMessage + "')", true);
            return;
        }
        mvGoodsReceivedDamaged.ActiveViewIndex = 1;
        btnLogDiscrepancy.Visible = false;
        btnBack.Visible = true;
        btnSave.Visible = true;
        btnSave.Enabled = true;
        GetDeliveryDetails();
        GetContactPurchaseOrderDetails();
        ucSDRCommunication1.innerControlRdoCommunication = 'E';
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        mvGoodsReceivedDamaged.ActiveViewIndex = 0;
        btnLogDiscrepancy.Visible = true;
        btnBack.Visible = false;
        btnSave.Visible = false;
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        var defaultEmailAddress = Convert.ToString(ConfigurationManager.AppSettings["DefaultEmailAddress"]);
        var txtAltEmail = (ucTextbox)ucSDRCommunication1.FindControl("txtAltEmail");
        if (ucSDRCommunication1.VendorEmailList.Trim().Equals(defaultEmailAddress) && (string.IsNullOrEmpty(txtAltEmail.Text) || string.IsNullOrWhiteSpace(txtAltEmail.Text)))
        {
            mdlAlternateEmail.Show();
            rfvEmailAddressCanNotBeEmpty.Enabled = true;
            txtAlternateEmailText.Focus();
        }
        else
        {
            if (Session["Role"].ToString().ToLower() == "vendor")
            {
                string LoginPath = ResolveClientUrl("../../../ModuleUI/Security/Login.aspx");
                Response.Redirect(LoginPath);
            }
            else
            {
                this.Save();
            }
            if (DeleteDiscrepancyTest == 0)
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + DeleteBlankWorkflowEntriesText + "')", true);
                System.Threading.Thread.Sleep(30000);
            }
        }
    }

    private void Save()
    {
        try
        {
            int iAllEmpty = 0;
         
            foreach (GridViewRow row in gvPurchaseOrderDetails.Rows)
            {

                TextBox txtDamagedQty = (TextBox)row.FindControl("txtDamagedQty");
                TextBox txtDamagedDescription = (TextBox)row.FindControl("txtDamagedDescription");
                Label lblOvers = (Label)row.FindControl("lblOvers");

                if (txtDamagedQty.Text.Trim() == "" && txtDamagedDescription.Text.Trim() == "")
                {
                    iAllEmpty++;
                    continue;
                }
                else
                {
                    if (txtDamagedQty.Text.Trim() == "" && txtDamagedDescription.Text.Trim() != "")
                    {
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + DamagedQtyValidation + "')", true);
                        return;
                    }

                    if (txtDamagedQty.Text.Trim() != "" && txtDamagedDescription.Text.Trim() == "")
                    {
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + DamagedDescriptionValidation + "')", true);
                        return;
                    }


                    int iDamagedQty;
                    bool isDamageQty = int.TryParse(txtDamagedQty.Text.Trim(), out iDamagedQty);

                    if (txtDamagedQty.Text.Trim() != "" && !isDamageQty)
                    {
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + InvalidValuesEntered + "')", true);
                        return;
                    }
                    if (txtDamagedQty.Text.Trim() != "" && isDamageQty && !txtDamagedQty.Text.IsNumeric())
                    {
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + InvalidValuesEntered + "')", true);
                        return;
                    }
                    if (txtDamagedQty.Text.Trim() != ""
                        && Convert.ToInt32(txtDamagedQty.Text.Trim()) <= 0)
                    {
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + InvalidValuesEntered + "')", true);
                        return;
                    }
                }
            }

            if (iAllEmpty > 0 && iAllEmpty == gvPurchaseOrderDetails.Rows.Count)
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + AtLeastOne + "')", true);
                return;
            }

            if (ViewState["PurchaseOrderID"] != null && !(string.IsNullOrEmpty(lblPurchaseOrderDateValue.Text.Split('-')[1].ToString())))
            {
                DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
                DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();

                oDiscrepancyBE.Action = "AddDiscrepancyLog";
                oDiscrepancyBE.VDRNo = "A000000005";
                oDiscrepancyBE.SiteID = Convert.ToInt32(ucSite.innerControlddlSite.SelectedValue);
                oDiscrepancyBE.DiscrepancyTypeID = Convert.ToInt32(Common.DiscrepancyType.GoodReceivedDamaged);
                oDiscrepancyBE.DiscrepancyLogDate = DateTime.Now;
                oDiscrepancyBE.PurchaseOrderID = ViewState["PurchaseOrderID"] != null ? Convert.ToInt32(ViewState["PurchaseOrderID"].ToString()) : (int?)null;
                oDiscrepancyBE.PurchaseOrderDate = Common.GetMM_DD_YYYY(lblPurchaseOrderDateValue.Text.Split('-')[1].ToString());
                oDiscrepancyBE.PurchaseOrderNumber = txtPurchaseOrder.Text.Trim();
                oDiscrepancyBE.InternalComments = txtInternalComments.Text;
                oDiscrepancyBE.DeliveryNoteNumber = txtDeliveryNumber.Text.Trim();
                oDiscrepancyBE.DeliveryArrivedDate = Common.GetMM_DD_YYYY(txtDateDeliveryArrived.innerControltxtDate.Value); //DateTime.Now;
                oDiscrepancyBE.CommunicationType = ucSDRCommunication1.innerControlRdoCommunication;
                oDiscrepancyBE.CommunicationTo = ucSDRCommunication1.VendorEmailList;
                oDiscrepancyBE.VendorID = ViewState["VendorID"] != null ? Convert.ToInt32(ViewState["VendorID"].ToString()) : (int?)null;

               
                if (ViewState["SPToCoverStockPlannerID"] != null)
                {
                    oDiscrepancyBE.StockPlannerID = Convert.ToInt32(ViewState["SPToCoverStockPlannerID"].ToString()); // case when actual Stock Planner is absent and SP to Cover is there
                    oDiscrepancyBE.StockPlannerIDIfNotAbsent = ViewState["StockPlannerID"] != null ? Convert.ToInt32(ViewState["StockPlannerID"].ToString()) : (int?)null; // Actual Stock Palnner Id that should have been saved if not absent
                }
                else
                {
                    if (ViewState["StockPlannerID"] != null)
                    {
                        oDiscrepancyBE.StockPlannerID = Convert.ToInt32(ViewState["StockPlannerID"].ToString());
                        oDiscrepancyBE.StockPlannerIDIfNotAbsent = null;
                    }
                }

                oDiscrepancyBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
                oDiscrepancyBE.User.Site = new BusinessEntities.ModuleBE.AdminFunctions.MAS_SiteBE();
                oDiscrepancyBE.User.UserID = Convert.ToInt32(Session["UserID"]);
                oDiscrepancyBE.Location = txtLocation.Text.Trim();
                oDiscrepancyBE.GoodsReturnedDriver = chkGoodReturnedDriver.Checked == true ? true : false;
                sResult = oDiscrepancyBAL.addDiscrepancyLogBAL(oDiscrepancyBE);
                oDiscrepancyBAL = null;
                if (!sResult.Contains("Discrepancy Already Inserted"))
                {
                    iResult = Convert.ToInt32(sResult.Split('-')[1].ToString());

                    if (iResult > 0)
                    {

                        foreach (GridViewRow gv in gvPurchaseOrderDetails.Rows)
                        {
                            DiscrepancyBE oNewDiscrepancyBE = new DiscrepancyBE();
                            DiscrepancyBAL oNewDiscrepancyBAL = new DiscrepancyBAL();

                            TextBox txtDamagedDescription = (TextBox)gv.FindControl("txtDamagedDescription");
                            TextBox txtDamagedQty = (TextBox)gv.FindControl("txtDamagedQty");

                            if (txtDamagedDescription.Text.Trim() != "" || txtDamagedDescription.Text.Trim() != string.Empty
                                && txtDamagedQty.Text.Trim() != "" || txtDamagedQty.Text.Trim() != string.Empty)
                            {
                                Label lblLineNo = (Label)gv.FindControl("lblLineNo");
                                Label lblVikingCode = (Label)gv.FindControl("lblVikingCodeValue");
                                Label lblOfficeDepotCode = (Label)gv.FindControl("lblOfficeDepotCodeValue");
                                Label lblVendorItemCode = (Label)gv.FindControl("lblVendorItemCodeValue");
                                Label lblDescription = (Label)gv.FindControl("lblDescriptionValue");
                                Label lblOutstandingQty = (Label)gv.FindControl("lblOutstandingQtyValue");
                                Label lblUoM = (Label)gv.FindControl("lblUoMValue");
                                 HiddenField hdnPurchaseOrderId = (HiddenField)gv.FindControl("hdnPurchaseOrderID");

                                oNewDiscrepancyBE.Action = "AddDiscrepancyItem";
                                oNewDiscrepancyBE.Line_no = Convert.ToInt32(lblLineNo.Text);
                                oNewDiscrepancyBE.DirectCode = lblVikingCode.Text;
                                oNewDiscrepancyBE.ODSKUCode = lblOfficeDepotCode.Text;
                                oNewDiscrepancyBE.VendorCode = lblVendorItemCode.Text;
                                oNewDiscrepancyBE.ProductDescription = lblDescription.Text;
                                oNewDiscrepancyBE.OutstandingQuantity = Convert.ToInt32(lblOutstandingQty.Text);
                                oNewDiscrepancyBE.UOM = lblUoM.Text;
                                oNewDiscrepancyBE.DamageDescription = txtDamagedDescription.Text.Trim();
                                oNewDiscrepancyBE.DamageQuantity = Convert.ToInt32(txtDamagedQty.Text);
                                 oNewDiscrepancyBE.PurchaseOrderID = Convert.ToInt32(hdnPurchaseOrderId.Value);

                                oNewDiscrepancyBE.DiscrepancyLogID = iResult;

                                int? iResult2 = oNewDiscrepancyBAL.addDiscrepancyItemBAL(oNewDiscrepancyBE);
                                oNewDiscrepancyBAL = null;
                               
                            }
                        }
                        bSendMail = true;
                        DiscrepancyLogID = iResult;
                        VDRNo = sResult.Split('-')[0].ToString();
                        InsertHTML();                       
                    }
                }
                else if (sResult.Contains("Discrepancy Already Inserted"))
                {
                     EncryptQueryString("DIS_OptionsPopup.aspx?VDRNo=" + sResult.Split('-')[0].ToString()
                        + "&DiscrepancyLogID=" + sResult.Split('-')[1].ToString()
                        + "&CommunicationType=" + ucSDRCommunication1.innerControlRdoCommunication
                        + "&SiteId=" + ucSite.innerControlddlSite.SelectedValue
                        + "&DisDate=" + DateTime.Now.ToString("dd/MM/yyyy"));

                }
            }
            else
            {
                EncryptQueryString("~/ModuleUI/Discrepancy/DIS_SearchResult.aspx?disLogID=" + Convert.ToString(GetQueryStringValue("disLogID"))
                                + "&VDRNo=" + Convert.ToString(GetQueryStringValue("VDRNo"))
                                + "&UserID=" + Convert.ToString(GetQueryStringValue("UserID"))
                                + "&PreviousPage=SearchResult&WorkFlowID=" + Convert.ToString(GetQueryStringValue("WorkFlowID").ToString()));
            }
        }
        catch (Exception ex)
        {
            LogUtility.SaveErrorLogEntry(ex);
        }
        finally
        {
            if (DiscrepancyLogID > 0)
            {
                DiscrepancyBE oNewDiscrepancyBE = new DiscrepancyBE();
                DiscrepancyBAL oNewDiscrepancyBAL = new DiscrepancyBAL();

                WorkflowHTML oWorkflowHTML = new WorkflowHTML();

                oNewDiscrepancyBE.Action = "DeleteBlankWorkflowEntries";
                oNewDiscrepancyBE.DiscrepancyLogID = DiscrepancyLogID;
                DeleteDiscrepancyTest = oNewDiscrepancyBAL.DeleteBlankWorkflowEntriesBAL(oNewDiscrepancyBE);
                if (DeleteDiscrepancyTest != 0)
                {
                    
                    int? iDiscrepancyCommunicationID = sendAndSaveDiscrepancyCommunication(iResult);
                   

                    if (ucSDRCommunication1.innerControlRdoCommunication == 'L')   // for letter                
                        oNewDiscrepancyBE.VENHTML = oWorkflowHTML.function4("Blue", "", DateTime.Now, "", "Admin@OfficeDepot.com", "", InitialCommunicationSent, "", Convert.ToString(sLetterLink));
                    else
                        oNewDiscrepancyBE.VENHTML = oWorkflowHTML.function4("Blue", "", DateTime.Now, Convert.ToString(sSendMailLink), "Admin@OfficeDepot.com", "", InitialCommunicationSent, "", "");

                    oNewDiscrepancyBE.Action = "UpdateVENHTML";
                    oNewDiscrepancyBE.DiscrepancyLogID = DiscrepancyLogID;

                    int? UpdateVENHTML = oNewDiscrepancyBAL.UpdateVENHtmlBAL(oNewDiscrepancyBE);


                    if (ucSDRCommunication1.innerControlRdoCommunication == 'L')
                    {
                        sendCommunication communication = new sendCommunication();
                        Session["LabelHTML"] = communication.sendAndSaveLetter(DiscrepancyLogID, "goodsreceiveddamaged", Convert.ToInt32(ViewState["VendorID"].ToString())); ;
                        sLetterLink.Append(communication.sLetterLink);
                    }

                    EncryptQueryString("DIS_OptionsPopup.aspx?VDRNo=" + sResult.Split('-')[0].ToString()
                    + "&DiscrepancyLogID=" + sResult.Split('-')[1].ToString()
                    + "&CommunicationType=" + ucSDRCommunication1.innerControlRdoCommunication
                    + "&SiteId=" + ucSite.innerControlddlSite.SelectedValue
                    + "&DisDate=" + DateTime.Now.ToString("dd/MM/yyyy"));
                }
            }
        }
    }

    protected void btnViewWF_Click(object sender, EventArgs e)
    {
        if (GetQueryStringValue("PN") != null)
        {
            if (GetQueryStringValue("PN").Trim().ToUpper().Equals("POINQ"))
            {
                string url = string.Format("DISLog_Workflow.aspx?disLogID={0}&PN={1}&SId={2}&Status={3}&PN={4}&PO={5}&RoleType={6}", GetQueryStringValue("disLogID").ToString(), "POINQ", GetQueryStringValue("SId").ToString(), GetQueryStringValue("Status").ToString(), "DISLOG", GetQueryStringValue("PO").ToString(), GetQueryStringValue("RoleTypeFlag"));
                EncryptQueryString(url);
            }

        }
        if (GetQueryStringValue("disLogID") != null)
        {
            string url = string.Format("DISLog_Workflow.aspx?disLogID={0}&VDRNo={1}&TodayDis={2}&SiteID={3}&DisDate={4}&UserID={5}&RoleType={6}", GetQueryStringValue("disLogID").ToString(), GetQueryStringValue("VDRNo").ToString(), GetQueryStringValue("TodayDis"), GetQueryStringValue("SiteId"), GetQueryStringValue("DisDate"), GetQueryStringValue("UserID"), GetQueryStringValue("RoleTypeFlag"));
            EncryptQueryString(url);
        }
    }

    private int? sendAndSaveDiscrepancyCommunication(int iDiscrepancy)
    {
        int? vendorID = ViewState["VendorID"] == null ? 0 : Convert.ToInt32(ViewState["VendorID"].ToString());
        if (((RadioButton)ucSDRCommunication1.FindControl("rdoEmailComm")) != null && ((RadioButton)ucSDRCommunication1.FindControl("rdoEmailComm")).Checked)
        {
            if (((BaseControlLibrary.ucTextbox)ucSDRCommunication1.FindControl("txtAltEmail")) != null && ((BaseControlLibrary.ucTextbox)ucSDRCommunication1.FindControl("ucVendorEmailList")) != null)
            {
                sendCommunication communication = new sendCommunication();
                int? retVal = communication.sendCommunicationByEMail(iDiscrepancy, "goodsreceiveddamaged", ((BaseControlLibrary.ucTextbox)ucSDRCommunication1.FindControl("txtAltEmail")), ((BaseControlLibrary.ucTextbox)ucSDRCommunication1.FindControl("ucVendorEmailList")), vendorID);
                sSendMailLink = communication.sSendMailLink;
                return retVal;
            }
        }
        return null;
    }

    protected void ddlPurchaseOrderDate_SelectedIndexChanged(object sender, EventArgs e)
    {
       
        if (ViewState["GetPODateList"] != null)
        {
            List<DiscrepancyBE> lstGetPODateList = (List<DiscrepancyBE>)ViewState["GetPODateList"];
            if (!string.IsNullOrEmpty(ddlPurchaseOrderDate.SelectedItem.Value))
            {
                var PODateDetail = lstGetPODateList.Find(p => p.RowNumber == Convert.ToInt32(ddlPurchaseOrderDate.SelectedItem.Value));
                lblVendorValue.Text = PODateDetail.VendorNoName;

                if (PODateDetail.VendorID > 0)
                    ucSDRCommunication1.VendorID = Convert.ToInt32(PODateDetail.VendorID);
                else
                    ucSDRCommunication1.VendorID = 0;

                if (!string.IsNullOrEmpty(ucSite.innerControlddlSite.SelectedValue))
                    ucSDRCommunication1.SiteID = Convert.ToInt32(ucSite.innerControlddlSite.SelectedValue);
                else
                    ucSDRCommunication1.SiteID = 0;
                ucSDRCommunication1.GetDetails();
            }
        }
    }

    protected void txtPurchaseOrder_TextChanged(object sender, EventArgs e)
    {
        GetPurchaseOrderList();
        txtDeliveryNumber.Focus();
    }

    public override BusinessEntities.BaseBe[] SubSort(GridViewSortEventArgs e)
    {
        return Utilities.GenericListHelper<DiscrepancyBE>.SortList((List<DiscrepancyBE>)ViewState["lstSitesViewMode"], e.SortExpression, e.SortDirection).ToArray();
    }

    protected void btnSave_1_Click(object sender, EventArgs e)
    {
        DiscrepancyBE discrepancyBE = null;
        DiscrepancyBAL discrepancyBAL = null;

        if (!string.IsNullOrEmpty(txtUserComments.Text) && !string.IsNullOrWhiteSpace(txtUserComments.Text))
        {
            lblErrorMsg.Text = string.Empty;
            discrepancyBE = new DiscrepancyBE();
            discrepancyBE.Action = "AddDiscrepancyLogComment";

            if (!string.IsNullOrEmpty(GetQueryStringValue("disLogID")))
                discrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID"));
            else
                discrepancyBE.DiscrepancyLogID = 0;

            discrepancyBE.UserID = Convert.ToInt32(Session["UserID"]);
            discrepancyBE.CommentDateTime = DateTime.Now;
            discrepancyBE.Comment = txtUserComments.Text;
            discrepancyBAL = new DiscrepancyBAL();
            discrepancyBAL.AddDiscrepancyLogCommentBAL(discrepancyBE);
            discrepancyBE.Action = "GetDiscrepancyLogComment";
            this.BindDiscrepancyLogComment(discrepancyBE);

            discrepancyBE.DiscrepancyType = "goodsreceiveddamaged";
            int vendorID = 0;
            if (ViewState["VendorID"] != null)
                vendorID = Convert.ToInt32(ViewState["VendorID"]);
            new SendCommentMail().SendCommentsMail(discrepancyBE,
                (BaseControlLibrary.ucTextbox)ucSDRCommunication1.FindControl("txtAltEmail"),
                (BaseControlLibrary.ucTextbox)ucSDRCommunication1.FindControl("ucVendorEmailList"),
                vendorID, txtUserComments.Text, Session["Role"].ToString());

            mdlAddComment.Hide();
        }
        else
        {
            lblErrorMsg.Text = WebCommon.getGlobalResourceValue("MustEnterComment");
            mdlAddComment.Show();
        }

        btnSave.Visible = false;
        btnBack.Visible = false;
        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "Reload()", true);
    }

    protected void btnBack_2_Click(object sender, EventArgs e)
    {
        mdlAddComment.Hide();
        btnSave.Visible = false;
        btnBack.Visible = false;

        DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();
        DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
        oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID"));
        oDiscrepancyBE.Action = "GetDiscrepancyReturnNoteCount";
        DataTable dtReturnNoteCount = oDiscrepancyBAL.GetDiscrepancyReturnNoteCountBAL(oDiscrepancyBE);
        if (dtReturnNoteCount != null && dtReturnNoteCount.Rows.Count > 0)
        {
            if (Convert.ToInt32(dtReturnNoteCount.Rows[0].ItemArray[0].ToString()) > 0)
                btnPrintReturnsNote.Visible = true;
        }
        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "Reload()", true);
    }

    protected void btnAddComment_Click(object sender, EventArgs e)
    {

        txtUserComments.Text = string.Empty;
        lblErrorMsg.Text = string.Empty;
        lblUserT.Text = Convert.ToString(Session["UserName"]);
        lblDateT.Text = DateTime.Now.Date.ToString("dd/MM/yyyy");
        if (GetQueryStringValue("VDRNo") != null)
            lblDiscrepancyNoT.Text = Convert.ToString(GetQueryStringValue("VDRNo"));
        mdlAddComment.Show();
        txtUserComments.Focus();

        btnSave.Visible = false;
        btnBack.Visible = false;
    }
    protected void btnAccountsPayableAction_Click(object sender, EventArgs e)
    {

        mdlAPAction.Show();
        DropDownList ddl = ucApAction.FindControl("ddlCurrency") as DropDownList;
        DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
        DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();
        oDiscrepancyBE.Action = "GetCurrencyByVendorID";
        if (GetQueryStringValue("disLogID") != null)
        {
            oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID"));
        }
        ddl.SelectedValue = oDiscrepancyBAL.GetCurrencyBAL(oDiscrepancyBE);      
    }

    protected void btnAddImages_Click(object sender, EventArgs e)
    {

        EncryptQueryString("DIS_OptionsPopup.aspx?Mode=Edit&VDRNo=" + GetQueryStringValue("VDRNo").ToString() + "&DiscrepancyLogID=" + GetQueryStringValue("disLogID").ToString() + "&CommunicationType=" + Common.DiscrepancyType.GoodReceivedDamaged);
    }
    /* ENHANCEMENT FOR PHASE R3 */
    /*RAISE QUERY BUTTON*/
    protected void btnRaiseQuery_Click(object sender, EventArgs e)
    {
        try
        {
            var queryDiscrepancyBAL = new DISLog_QueryDiscrepancyBAL();
            var queryDiscrepancyBE = new DISLog_QueryDiscrepancyBE();
            var discrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID"));

            if (!this.IsAlreadyDisputeExist(discrepancyLogID) || !this.IsQueryClosed(discrepancyLogID))
            {
                btnBack.Visible = false;
                btnSave.Visible = false;
                mdlRaiseQueryConfirmMsg.Show();   
            }
            else
            {
                lblMessageFirst.Text = strThereIsCurrentlyAlreadyAnOpenQueryBeingInvestigated;
                lblMessageSecond.Text = strPleaseWaitUntilYouHaveReceivedAReplyForThisBefore;
                btnSave.Visible = false;
                btnBack.Visible = false;
                mdlShowError.Show();
            }

        }

        catch (Exception ex)
        {
            LogUtility.SaveErrorLogEntry(ex);
        }
       
    }

    #region Alternate email code ...

    protected void btnSetAlternateEmail_Click(object sender, EventArgs e)
    {
        rfvEmailAddressCanNotBeEmpty.Enabled = false;
        var txtAltEmail = (ucTextbox)ucSDRCommunication1.FindControl("txtAltEmail");
        if (txtAltEmail != null)
        {
            txtAltEmail.Text = txtAlternateEmailText.Text;
            txtAlternateEmailText.Text = string.Empty;
        }

        if (Session["Role"].ToString().ToLower() == "vendor")
        {
            string LoginPath = ResolveClientUrl("../../../ModuleUI/Security/Login.aspx");
            Response.Redirect(LoginPath);
        }
        else
        {
            this.Save();
        }
        if (DeleteDiscrepancyTest == 0)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + DeleteBlankWorkflowEntriesText + "')", true);
            System.Threading.Thread.Sleep(30000);
        }
    }

    protected void btnAddEmailId_Click(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(lblMultyAlternateEmail.Text) && !string.IsNullOrWhiteSpace(lblMultyAlternateEmail.Text))
        {
            if (!lblMultyAlternateEmail.Text.Contains(txtAlternateEmailText.Text))
            {
                lblMultyAlternateEmail.Text = string.Format("{0}, {1}", lblMultyAlternateEmail.Text, txtAlternateEmailText.Text);
            }
        }
        else
        {
            lblMultyAlternateEmail.Text = txtAlternateEmailText.Text;
        }

        lblMultyAlternateEmail.Visible = true;
        txtAlternateEmailText.Text = string.Empty;
        txtAlternateEmailText.Focus();
        mdlAlternateEmail.Show();
    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        txtAlternateEmailText.Text = string.Empty;
        lblMultyAlternateEmail.Text = string.Empty;
        txtAlternateEmailText.Focus();
        mdlAlternateEmail.Show();
    }

    #endregion

    #endregion

    #region Methods

    private int? PrintAndSaveReturnNote()
    {
        //decimal TotalCost = 0;
        //decimal NetTotalCost = 0;
        //decimal FrieghtCharges = 0;
        //string htmlBody = string.Empty;
        //string DiscrepancyType = string.Empty;
        //// vendor return address
        //DiscrepancyBE oNewDiscrepancyBE = new DiscrepancyBE();
        DiscrepancyBAL oNewDiscrepancyBAL = new DiscrepancyBAL();
        //oNewDiscrepancyBE.Action = "GetDiscrepancySiteVendorDetail";
        //oNewDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID"));

        //List<DiscrepancyBE> lstVendor = oNewDiscrepancyBAL.GetDiscrepancyLogSiteVendorDetailsBAL(oNewDiscrepancyBE);
        //if (lstVendor.Count > 0)
        //    DiscrepancyType = lstVendor[0].DiscrepancyType;
        //string VendorAddress = string.Empty;
        //if (string.IsNullOrEmpty(lstVendor[0].Vendor.ReturnAddress))
        //{
        //    VendorAddress = lstVendor[0].Vendor.address1 + "<br />" +
        //        lstVendor[0].Vendor.address2 + "<br />" +
        //        lstVendor[0].Vendor.city + "<br />" +
        //        lstVendor[0].Vendor.VMPPIN + " " + lstVendor[0].Vendor.VMPPOU;

        //}
        //else
        //{
        //    VendorAddress = lstVendor[0].Vendor.ReturnAddress.Replace("\r\n", "<br />");
        //}
        //// site address
        //string SiteAddress = lstVendor[0].Site.SiteAddressLine1 + "<br />" +
        //   lstVendor[0].Site.SiteAddressLine2 + "<br />" +
        //   lstVendor[0].Site.SiteAddressLine3 + "<br />" +
        //   lstVendor[0].Site.SiteAddressLine4 + "<br />" +
        //   lstVendor[0].Site.SiteAddressLine5 + "<br />" +
        //   lstVendor[0].Site.SiteAddressLine6 + "<br />" +
        //   lstVendor[0].Site.SitePincode;

        //oNewDiscrepancyBE.Action = "GetDiscrepancyLogITemsForReturnNote";
        //List<DiscrepancyBE> lstItems = oNewDiscrepancyBAL.GetDiscrepancyItemsForReturnNote(oNewDiscrepancyBE);
        //string DiscrepancyItems = string.Empty;
        //string InCorrectDiscrepancyItems = string.Empty;
        //int count = lstItems.Count / 2;
        //int tempCount = 0;
        //if (lstItems != null && lstItems.Count > 0)
        //{
        //    foreach (DiscrepancyBE item in lstItems)
        //    {
        //        if (count == 0 || tempCount < count)
        //        {
        //            if (string.IsNullOrEmpty(item.ODSKUCode) && !string.IsNullOrEmpty(item.ProductDescription))
        //            {
        //                DiscrepancyItems += "<tr><td>" + item.DeliveredQuantity
        //                                          + "</td><td>" + item.UOM
        //                                          + "</td><td>" + item.ODSKUCode
        //                                          + "</td><td>" + item.DirectCode
        //                                          + "</td><td>" + item.ProductDescription;
        //                if (lstItems[0].DiscrepancyTypeID == 12)
        //                {
        //                    DiscrepancyItems += "</td><td>" + item.OriginalQuantity;
        //                    DiscrepancyItems += "</td><td>" + item.OutstandingQuantity;

        //                    DiscrepancyItems += "</td><td style='text-align:left;'>" + item.ItemVal
        //                                 + "</td><td style='text-align:right;'>";
        //                }
        //                else
        //                {
        //                    DiscrepancyItems += "</td><td style='text-align:right;'>" + item.ItemVal
        //                                     + "</td><td style='text-align:right;'>";
        //                }

        //                if (item.DeliveredQuantity.HasValue && !string.IsNullOrEmpty(item.ItemVal))
        //                {
        //                    TotalCost += Convert.ToDecimal(item.ItemVal) * item.DeliveredQuantity.Value;
        //                    DiscrepancyItems += Convert.ToDecimal(item.ItemVal) * item.DeliveredQuantity.Value;
        //                }
        //                else
        //                {
        //                    DiscrepancyItems += string.Empty;
        //                }
        //                DiscrepancyItems += "</td></tr>";
        //            }
        //            else
        //            {
        //                if (!string.IsNullOrEmpty(item.ODSKUCode))
        //                {
        //                    DiscrepancyItems += "<tr><td>" + item.DeliveredQuantity
        //                        + "</td><td>" + item.UOM
        //                        + "</td><td>" + item.ODSKUCode
        //                        + "</td><td>" + item.DirectCode
        //                        + "</td><td>" + item.ProductDescription;

        //                    if (lstItems[0].DiscrepancyTypeID == 12)
        //                    {
        //                        DiscrepancyItems += "</td><td>" + item.OriginalQuantity;
        //                        DiscrepancyItems += "</td><td>" + item.OutstandingQuantity;

        //                        DiscrepancyItems += "</td><td style='text-align:left;'>" + item.ItemVal
        //                    + "</td><td style='text-align:right;'>";
        //                    }
        //                    else
        //                    {
        //                        DiscrepancyItems += "</td><td style='text-align:right;'>" + item.ItemVal
        //                 + "</td><td style='text-align:right;'>";
        //                    }


        //                    if (item.DeliveredQuantity.HasValue && !string.IsNullOrEmpty(item.ItemVal))
        //                    {
        //                        TotalCost += Convert.ToDecimal(item.ItemVal) * item.DeliveredQuantity.Value;
        //                        DiscrepancyItems += Convert.ToDecimal(item.ItemVal) * item.DeliveredQuantity.Value;
        //                    }
        //                    else
        //                    {
        //                        DiscrepancyItems += string.Empty;
        //                    }
        //                    DiscrepancyItems += "</td></tr>";
        //                }
        //            }
        //        }
        //        else
        //        {
        //            InCorrectDiscrepancyItems += "<tr><td>" + item.DeliveredQuantity
        //            + "</td><td>" + item.UOM
        //            + "</td><td>" + item.ODSKUCode
        //            + "</td><td>" + item.DirectCode
        //            + "</td><td>" + item.ProductDescription;
        //            DiscrepancyItems += "</td></tr>";
        //        }
        //        tempCount++;
        //    }
        //}
        ////if (!string.IsNullOrEmpty(txtCarriageCharge.Text))
        ////{
        ////    FrieghtCharges = Convert.ToDecimal(txtCarriageCharge.Text);
        ////}
        //NetTotalCost = TotalCost + FrieghtCharges;
        //string VendorLanguage = lstVendor[0].Vendor.Language;
        ////string templateFile = "~/EmailTemplates/ReturnNote/ReturnNote." + VendorLanguage + ".htm";
        //string templateFile = string.Empty;
        //if (lstItems[0].DiscrepancyTypeID == 12)
        //{
        //    templateFile = "~/EmailTemplates/ReturnNote/ReturnNoteQuality.english.htm";
        //}
        //else
        //{
        //    templateFile = "~/EmailTemplates/ReturnNote/ReturnNote.english.htm";
        //}



        //#region Setting reason as per the language ...
        //switch (VendorLanguage)
        //{
        //    case clsConstants.English:
        //        Page.UICulture = clsConstants.EnglishISO; // "en-US"; // English
        //        break;
        //    case clsConstants.French:
        //        Page.UICulture = clsConstants.FranceISO; // // France
        //        break;
        //    case clsConstants.German:
        //        Page.UICulture = clsConstants.GermanyISO; //"de"; // German
        //        break;
        //    case clsConstants.Dutch:
        //        Page.UICulture = clsConstants.NederlandISO; //"nl"; // Dutch
        //        break;
        //    case clsConstants.Spanish:
        //        Page.UICulture = clsConstants.SpainISO; //"es"; // Spanish
        //        break;
        //    case clsConstants.Italian:
        //        Page.UICulture = clsConstants.ItalyISO; //"it"; // Italy
        //        break;
        //    case clsConstants.Czech:
        //        Page.UICulture = clsConstants.CzechISO; //"cz"; // Czech
        //        break;
        //    default:
        //        Page.UICulture = clsConstants.EnglishISO; //"en-US"; // English
        //        break;
        //}

        //#endregion

        //string contactPerson = string.Empty;

        //sendCommunicationCommon osendCommunicationCommon = new sendCommunicationCommon();
        //if (System.IO.File.Exists(System.Web.HttpContext.Current.Server.MapPath(templateFile.ToLower())))
        //{
        //    using (StreamReader sReader = new StreamReader(System.Web.HttpContext.Current.Server.MapPath(templateFile.ToLower())))
        //    {
        //        htmlBody = sReader.ReadToEnd();
        //        if (DiscrepancyType != "Incorrect Product Code")
        //            htmlBody = htmlBody.Replace("block", "none");

        //        htmlBody = htmlBody.Replace("{VendorDiscrepancyReturnNumber}", WebCommon.getGlobalResourceValue("VendorDiscrepancyReturnNumber"));
        //        htmlBody = htmlBody.Replace("{VDRNoValue}", lstVendor[0].VDRNo);

        //        htmlBody = htmlBody.Replace("{logoInnerPath}", osendCommunicationCommon.getAbsolutePath());

        //        htmlBody = htmlBody.Replace("{From}", WebCommon.getGlobalResourceValue("From"));
        //        htmlBody = htmlBody.Replace("{SiteAddressValue}", SiteAddress);

        //        htmlBody = htmlBody.Replace("{VendorDiscrepancyReturnNote}", WebCommon.getGlobalResourceValue("VendorDiscrepancyReturnNote"));

        //        htmlBody = htmlBody.Replace("{VendorNumberSDR}", WebCommon.getGlobalResourceValue("VendorNumberSDR"));
        //        htmlBody = htmlBody.Replace("{VendorNumberValue}", lstVendor[0].Vendor.Vendor_No);

        //        htmlBody = htmlBody.Replace("{VendorName}", WebCommon.getGlobalResourceValue("VendorName"));
        //        htmlBody = htmlBody.Replace("{VendorNameValue}", string.IsNullOrEmpty(lstVendor[0].Vendor.VendorContactName) ? lstVendor[0].Vendor.VendorName : lstVendor[0].Vendor.VendorContactName);

        //        htmlBody = htmlBody.Replace("{Returnto}", WebCommon.getGlobalResourceValue("Returnto"));
        //        htmlBody = htmlBody.Replace("{VendorAddressValue}", VendorAddress);

        //        htmlBody = htmlBody.Replace("{ReturnNoteText1}", WebCommon.getGlobalResourceValue("ReturnNoteText1"));
        //        htmlBody = htmlBody.Replace("{ReturnNoteText2}", WebCommon.getGlobalResourceValue("ReturnNoteText2"));

        //        htmlBody = htmlBody.Replace("{AuthorisedBy}", WebCommon.getGlobalResourceValue("AuthorisedBy"));
        //        htmlBody = htmlBody.Replace("{VendorAuthorisationReference}", WebCommon.getGlobalResourceValue("VendorAuthorisationReference"));
        //        htmlBody = htmlBody.Replace("{ReasonforReturn}", WebCommon.getGlobalResourceValue("ReasonforReturn"));


        //        htmlBody = htmlBody.Replace("{VendorContactValue}", GetVendorContactName());
        //        if (lstItems != null && lstItems.Count > 0)
        //            htmlBody = htmlBody.Replace("{VendorAuthorisationReferenceValue}", lstItems[0].CollectionAuthNumber);
        //        else
        //            htmlBody = htmlBody.Replace("{VendorAuthorisationReferenceValue}", "-");

        //        htmlBody = htmlBody.Replace("{VENDORDISREPACNYTYPEValue}", lstVendor[0].DiscrepancyType);

        //        htmlBody = htmlBody.Replace("{QTY}", WebCommon.getGlobalResourceValue("QTY"));
        //        htmlBody = htmlBody.Replace("{UOM}", WebCommon.getGlobalResourceValue("UOM"));
        //        htmlBody = htmlBody.Replace("{OURPRODUCTCODE}", WebCommon.getGlobalResourceValue("OURPRODUCTCODE"));
        //        htmlBody = htmlBody.Replace("{VENDORREFERENCE}", WebCommon.getGlobalResourceValue("VENDORREFERENCE"));
        //        htmlBody = htmlBody.Replace("{DESCRIPTION}", WebCommon.getGlobalResourceValue("DESCRIPTION"));
        //        htmlBody = htmlBody.Replace("{COST}", WebCommon.getGlobalResourceValue("COST"));
        //        htmlBody = htmlBody.Replace("{NET}", WebCommon.getGlobalResourceValue("NET"));

        //        // quatity return notes changes//
        //        if (lstItems[0].DiscrepancyTypeID == 12)
        //        {
        //            htmlBody = htmlBody.Replace("{POQuantity}", WebCommon.getGlobalResourceValue("OriginalPOQuantity"));
        //            htmlBody = htmlBody.Replace("{OUTSTANDING}", WebCommon.getGlobalResourceValue("OutstandingPOQuantity"));
        //        }

        //        htmlBody = htmlBody.Replace("{DiscrepancyItemsValue}", DiscrepancyItems);

        //        htmlBody = htmlBody.Replace("{ReturnTotal}", WebCommon.getGlobalResourceValue("ReturnTotal"));
        //        htmlBody = htmlBody.Replace("{TotalCostValue}", TotalCost.ToString());

        //        htmlBody = htmlBody.Replace("{FreightCharge}", WebCommon.getGlobalResourceValue("FreightCharge"));
        //        htmlBody = htmlBody.Replace("{FreightChargeValue}", FrieghtCharges.ToString());
        //        // *******  New changes
        //        htmlBody = htmlBody.Replace("{ReturnNoteIncorrectProductDec}", WebCommon.getGlobalResourceValue("ReturnNoteIncorrectProductDec"));
        //        htmlBody = htmlBody.Replace("{DiscrepancyItemsValue1}", InCorrectDiscrepancyItems);
        //        //******************************
        //        htmlBody = htmlBody.Replace("{Total}", WebCommon.getGlobalResourceValue("Total"));
        //        htmlBody = htmlBody.Replace("{NetTotalCostValue}", NetTotalCost.ToString());

        //        htmlBody = htmlBody.Replace("{PreparedBy}", WebCommon.getGlobalResourceValue("PreparedBy"));
        //        htmlBody = htmlBody.Replace("{OFFICE_DEPOT_DC_CONTACTValue}", Session["UserName"].ToString());

        //        htmlBody = htmlBody.Replace("{DatePrepared}", WebCommon.getGlobalResourceValue("DatePrepared"));
        //        htmlBody = htmlBody.Replace("{DATEPREPAREDValue}", DateTime.Now.ToString("dd/MM/yyyy"));

        //        htmlBody = htmlBody.Replace("{NumberofPallets}", WebCommon.getGlobalResourceValue("NumberofPallets"));

        //        htmlBody = htmlBody.Replace("{NumberofPalletsValue}", hdntxtNumPalletsExchange.Value.Trim());

        //        htmlBody = htmlBody.Replace("{NumberofCartons}", WebCommon.getGlobalResourceValue("NumberofCartons"));
        //        htmlBody = htmlBody.Replace("{NumberofCartonsValue}", hdntxtNumCartonsExchange.Value.Trim());

        //        htmlBody = htmlBody.Replace("{CollectionReturnDate}", WebCommon.getGlobalResourceValue("CollectionReturnDate"));
        //        if (!string.IsNullOrEmpty(hdntxtCollection.Value.ToString()))
        //        {
        //            htmlBody = htmlBody.Replace("{ColletionDateValue}", hdntxtCollection.Value.ToString());
        //        }
        //        else
        //        {
        //            htmlBody = htmlBody.Replace("{ColletionDateValue}", DateTime.Now.ToString("dd/MM/yyyy"));
        //        }

        //        htmlBody = htmlBody.Replace("{Signed}", WebCommon.getGlobalResourceValue("Signed"));

        //        htmlBody = htmlBody.Replace("{Carrier}", WebCommon.getGlobalResourceValue("Carrier"));
        //        if (hdnucTxtCarrierWhoCollectGoods.Value != null && hdnucTxtCarrierWhoCollectGoods.Value != "----Select----")
        //        {
        //            htmlBody = htmlBody.Replace("{CarrierValue}", hdnucTxtCarrierWhoCollectGoods.Value);
        //        }
        //        else if (hdnddlCarrierSendingGoodsBack.Value != null)
        //            htmlBody = htmlBody.Replace("{CarrierValue}", hdnddlCarrierSendingGoodsBack.Value);
        //        else
        //            htmlBody = htmlBody.Replace("{CarrierValue}", "");

        //        htmlBody = htmlBody.Replace("{VEHICLEREG}", WebCommon.getGlobalResourceValue("VEHICLEREG"));
        //        htmlBody = htmlBody.Replace("{DRIVER}", WebCommon.getGlobalResourceValue("DRIVER"));
        //        htmlBody = htmlBody.Replace("{SIGN}", WebCommon.getGlobalResourceValue("SIGN"));
        //        htmlBody = htmlBody.Replace("{Print}", WebCommon.getGlobalResourceValue("Print"));


        //        // changes for alternate Return note 
        //        htmlBody = htmlBody.Replace("{PalletExchangeInfo}", WebCommon.getGlobalResourceValue("PalletExchangeInfo"));
        //        htmlBody = htmlBody.Replace("{OnewayPallets}", WebCommon.getGlobalResourceValue("OneWayPallets"));
        //        htmlBody = htmlBody.Replace("{EuroPallets}", WebCommon.getGlobalResourceValue("NumberEuroPallets"));
        //        htmlBody = htmlBody.Replace("{EuroPalletsChanged}", WebCommon.getGlobalResourceValue("EuroPalletsChanged"));
        //        // changes end

        //        htmlBody = htmlBody.Replace("{ImportantNotice}", WebCommon.getGlobalResourceValue("ImportantNotice"));
        //        htmlBody = htmlBody.Replace("{ReturnNoteText3}", WebCommon.getGlobalResourceValue("ReturnNoteText3"));
        //        htmlBody = htmlBody.Replace("{ReturnNoteText4}", WebCommon.getGlobalResourceValue("ReturnNoteText4"));

        //        contactPerson = GetAPContact(lstVendor[0].VendorID);
        //        htmlBody = htmlBody.Replace("{ACCOUNTS PAYABLE CONTACT}", string.IsNullOrEmpty(contactPerson) ? "<Not Available>" : contactPerson);
        //        if (lstVendor[0].Site.SiteMangerUserID != null)
        //            contactPerson = GetDCSiteContact(lstVendor[0].Site.SiteMangerUserID);

        //        htmlBody = htmlBody.Replace("{OFFICE DEPOT DC SITE CONTACT}", string.IsNullOrEmpty(contactPerson) ? "<Not Available>" : contactPerson);
        //    }
        //}
        //Page.UICulture = Convert.ToString(Session["CultureInfo"]);

        //// Save into the database
        DiscrepancyReturnNoteBE oDiscrepancyReturnNoteBE = new DiscrepancyReturnNoteBE();
        //oDiscrepancyReturnNoteBE.Action = "AddDiscrepancyReturnNote";
        oDiscrepancyReturnNoteBE.Action = "GetDiscrepancyReturnNoteByLogID";
        oDiscrepancyReturnNoteBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID"));
        //oDiscrepancyReturnNoteBE.LoggedDate = DateTime.Now;
        //oDiscrepancyReturnNoteBE.ReturnNoteBody = htmlBody.ToString();

        //int? retval = oNewDiscrepancyBAL.AddDiscrepancyReturnNote(oDiscrepancyReturnNoteBE);
        oNewDiscrepancyBAL.GetDiscrepancyReturnNoteByLogID(oDiscrepancyReturnNoteBE);


        //// Print Return note
        Session["LabelHTML"] = oDiscrepancyReturnNoteBE.ReturnNoteBody;

        string a = "window.open('../LogDiscrepancy/DIS_PrintLetter.aspx', '', 'left =' + screen.width + ',top=' + screen.height + ',width=0,height=0,toolbar=0,scrollbars=1,status=0')";
        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "pp", a, true);

        return 0;
    }
    private void GetAccessRightRaiseQuery()
    {
        DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
        DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();
        oDiscrepancyBE.Action = "CheckUserRaiseQueryRight";
        oDiscrepancyBE.UserID = Convert.ToInt32(Session["UserID"]);
        oDiscrepancyBE.SiteID = Convert.ToInt32(Convert.ToString(hdnSiteID.Value));
        int? lstDetails = oDiscrepancyBAL.getAccessRightRaiseQueryBAL(oDiscrepancyBE);
        if (lstDetails > 0)
        {
            btnRaiseQuery.Visible = true;
        }
        else
        {
            btnRaiseQuery.Visible = false;
        }
    }

    private void GetDeliveryDetails()
    {
        if (GetQueryStringValue("disLogID") != null)
        {
            DiscrepancyBE oNewDiscrepancyBE = new DiscrepancyBE();
            DiscrepancyBAL oNewDiscrepancyBAL = new DiscrepancyBAL();
            oNewDiscrepancyBE.Action = "GetDiscrepancyLog";
            oNewDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
            if (GetQueryStringValue("status").ToLower() == "deleted")
            {
                oNewDiscrepancyBE.IsDelete = true;
            }
            List<DiscrepancyBE> lstDisLog = oNewDiscrepancyBAL.GetDiscrepancyLogDetailsBAL(oNewDiscrepancyBE);
            oNewDiscrepancyBAL = null;
            if (lstDisLog != null && lstDisLog.Count > 0)
            {
                hdnSiteID.Value = Convert.ToString(lstDisLog[0].Site.SiteID);
                lblSiteValue.Text = lstDisLog[0].Site.SiteName;
                lblPurchaseOrderValue.Text = lstDisLog[0].PurchaseOrderNumber;
                lblPurchaseOrderDateValue.Text = "- " + (Convert.ToDateTime(lstDisLog[0].PurchaseOrderDate)).ToString("dd/MM/yyyy");
                lblIssueRaisedDateValue.Text = (Convert.ToDateTime(lstDisLog[0].DiscrepancyLogDate)).ToString("dd/MM/yyyy");
                lblIssueRaisedTimeValue.Text = "- " + (Convert.ToDateTime(lstDisLog[0].DiscrepancyLogDate)).ToString("HH:MM");
                lblDeliveryNoValue.Text = lstDisLog[0].DeliveryNoteNumber.ToString();
                lblDeliveryDateValue.Text = "- " + (Convert.ToDateTime(lstDisLog[0].DeliveryArrivedDate)).ToString("dd/MM/yyyy");
                txtInternalComments.Text = lstDisLog[0].InternalComments.ToString();
                txtInternalComments.ReadOnly = true;
                lblVendorNumberValue.Text = lstDisLog[0].VendorNoName.ToString();
                chkGoodReturnedDriver.Checked = lstDisLog[0].GoodsReturnedDriver == true ? true : false;
                //lblContactValue.Text = lstDisLog[0].Vendor.VendorContactNumber.ToString();
                lblContactValue.Text = UIUtility.GetUserPhoneNoofVendor(Convert.ToInt32(lstDisLog[0].VendorID), Convert.ToInt32(lstDisLog[0].SiteID));

                lblStockPlannerNoValue.Text = lstDisLog[0].StockPlannerNO.ToString();
                lblPlannerContactNoValue.Text = lstDisLog[0].StockPlannerContact.ToString();
                VDRNo = lstDisLog[0].VDRNo.ToString();
                SiteID = lstDisLog[0].Site.SiteID;
                ucSDRCommunication1.VendorEmailList = lstDisLog[0].CommunicationTo;
                ucSDRCommunication1.innerControlRdoCommunication = lstDisLog[0].CommunicationType.Value;
                ucSDRCommunication1.innerControlRdoLetter.Enabled = false;
                ucSDRCommunication1.innerControlRdoEmail.Enabled = false;
                ucSDRCommunication1.innerControlEmailList.Enabled = false;
                ucSDRCommunication1.innerControlAltEmailList.Enabled = false;
                txtLocation.Text = lstDisLog[0].Location.ToString();
                ViewState["StockPlannerEmailID"] = lstDisLog[0].StockPlannerEmailID;
                hdnCostCenter.Value = lstDisLog[0].Site.CostCenter;


                lblSPActionComments.Text = "Comments :  " + lstDisLog[0].ActionComments;
                lblDecision.Text         = "Decision   :  " + lstDisLog[0].Decision;

                if (!string.IsNullOrEmpty(lstDisLog[0].ActionComments) && !string.IsNullOrEmpty(lstDisLog[0].Decision))
                {
                    pnlDecisionComments.Visible = true;
                }
            }
        }
        else
        {
            hdnSiteID.Value = Convert.ToString(ucSite.innerControlddlSite.SelectedItem.Value);
            lblSiteValue.Text = ucSite.innerControlddlSite.SelectedItem.Text;
            lblPurchaseOrderValue.Text = txtPurchaseOrder.Text;
            lblPurchaseOrderDateValue.Text = "- " + ddlPurchaseOrderDate.SelectedItem.Text;
            lblIssueRaisedDateValue.Text = DateTime.Now.ToString("dd/MM/yyyy");
            lblIssueRaisedTimeValue.Text = DateTime.Now.ToString("- HH:MM");
            lblDeliveryNoValue.Text = txtDeliveryNumber.Text;
            lblDeliveryDateValue.Text = "- " + txtDateDeliveryArrived.innerControltxtDate.Value;
        }
    }

    private void GetContactPurchaseOrderDetails()
    {

        DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
        DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();
        if (GetQueryStringValue("disLogID") != null)
        {

            gvPurchaseOrderDetailsViewMode.Style["display"] = "";
            gvPurchaseOrderDetails.Style["display"] = "none";

            oDiscrepancyBE.Action = "GetDiscrepancyItem";
            oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
            List<DiscrepancyBE> lstDetails = oDiscrepancyBAL.GetDiscrepancyItemDetailsBAL(oDiscrepancyBE);
            if (lstDetails.Count > 0 && lstDetails != null)
            {
                gvPurchaseOrderDetailsViewMode.DataSource = lstDetails;
                gvPurchaseOrderDetailsViewMode.DataBind();
                ViewState["lstSitesViewMode"] = lstDetails;
                ViewState["VendorID"] = lstDetails[0].VendorID != null ? lstDetails[0].VendorID.ToString() : null;
                ViewState["PurchaseOrderID"] = lstDetails[0].PurchaseOrderID != null ? lstDetails[0].PurchaseOrderID.ToString() : null;
                ViewState["ActualStockPlannerID"] = lstDetails[0].ActualStockPlannerID != null ?
                                                    Convert.ToInt32(lstDetails[0].ActualStockPlannerID.ToString()) : (int?)null; // Stock Planner ID while discrepancy is logged
            }

            DiscrepancyBE DiscrepancyBE = new DiscrepancyBE();
            DiscrepancyBAL DiscrepancyBAL = new DiscrepancyBAL();

            DiscrepancyBE.Action = "GetActualStockPlannerDetailsBySPId"; // getting details of SP while discrepancy is logged

            if (ViewState["ActualStockPlannerID"] != null)
                DiscrepancyBE.StockPlannerID = Convert.ToInt32(ViewState["ActualStockPlannerID"].ToString());

            List<DiscrepancyBE> lstActualStockPlannerDetailsBySPId = DiscrepancyBAL.GetActualStockPlannerDetailsBAL(DiscrepancyBE);

            if (lstActualStockPlannerDetailsBySPId.Count > 0)
            {
                lblStockPlannerNoValue.Text = lstActualStockPlannerDetailsBySPId[0].StockPlannerNO.ToString();
                lblPlannerContactNoValue.Text = lstActualStockPlannerDetailsBySPId[0].StockPlannerContact.ToString();
            }
        }

        else
        {
            gvPurchaseOrderDetailsViewMode.Style["display"] = "none";
            gvPurchaseOrderDetails.Style["display"] = "";

            oDiscrepancyBE.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();

            oDiscrepancyBE.Action = "GetContactPurchaseOrderDetails";

            if (string.IsNullOrEmpty(txtPurchaseOrder.Text.Trim()))
            {
                oDiscrepancyBE.Action = "GetPurchaseOrderByPOId";
                oDiscrepancyBE.PurchaseOrderID = Convert.ToInt32(ViewState["PurchaseOrderID"]);
                string PurchaseOrderNumber = oDiscrepancyBAL.GetPurchaseOrderByPOIdBAL(oDiscrepancyBE);

                if (!string.IsNullOrEmpty(PurchaseOrderNumber))
                {
                    oDiscrepancyBE.PurchaseOrderNumber = PurchaseOrderNumber;
                    txtPurchaseOrder.Text = PurchaseOrderNumber;
                    lblPurchaseOrderValue.Text = PurchaseOrderNumber;
                }
            }
            else
            {
                oDiscrepancyBE.PurchaseOrderNumber = txtPurchaseOrder.Text.Trim();
            }
           
            
            oDiscrepancyBE.PurchaseOrder = new BusinessEntities.ModuleBE.Upload.Up_PurchaseOrderDetailBE();
            oDiscrepancyBE.PurchaseOrder.Order_raised = Common.GetMM_DD_YYYY(ddlPurchaseOrderDate.SelectedItem.Text);
            oDiscrepancyBE.SiteID = Convert.ToInt32(ucSite.innerControlddlSite.SelectedItem.Value);

            List<DiscrepancyBE> lstDetails = oDiscrepancyBAL.GetContactPurchaseOrderDetailsBAL(oDiscrepancyBE);
            if (lstDetails.Count > 0 && lstDetails != null)
            {
                if (!string.IsNullOrEmpty(ddlPurchaseOrderDate.SelectedItem.Value))
                {
                    List<DiscrepancyBE> lstGetPODateList = (List<DiscrepancyBE>)ViewState["GetPODateList"];
                    var PODateDetail = lstGetPODateList.Find(p => p.RowNumber == Convert.ToInt32(ddlPurchaseOrderDate.SelectedItem.Value));
                    lblVendorNumberValue.Text = PODateDetail.VendorNoName;
                }

                //lblVendorNumberValue.Text = ddlPurchaseOrderDate.SelectedItem.Value.ToString();

                //lblContactValue.Text = lstDetails[0].Vendor.VendorContactNumber.ToString();
                lblContactValue.Text = UIUtility.GetUserPhoneNoofVendor(Convert.ToInt32(lstDetails[0].VendorID), Convert.ToInt32(ucSite.innerControlddlSite.SelectedItem.Value));

                lblStockPlannerNoValue.Text = lstDetails[0].StockPlannerNO.ToString();
                lblPlannerContactNoValue.Text = lstDetails[0].StockPlannerContact.ToString();

                //ucSDRCommunication1.innerControllblFaxNumber.Text = lstDetails[0].Vendor.VendorContactFax.ToString();
                //ucSDRCommunication1.innerControllblEmailID.Text = lstDetails[0].Vendor.VendorContactEmail.ToString();

                ViewState["PurchaseOrderID"] = lstDetails[0].PurchaseOrderID != null ? lstDetails[0].PurchaseOrderID.ToString() : null;
                ViewState["VendorID"] = lstDetails[0].VendorID != null ? lstDetails[0].VendorID.ToString() : null;
                ViewState["StockPlannerID"] = lstDetails[0].StockPlannerID != null ? lstDetails[0].StockPlannerID.ToString() : null;


                gvPurchaseOrderDetails.DataSource = lstDetails;
                gvPurchaseOrderDetails.DataBind();
                ViewState["lstSites"] = lstDetails;
            }

            oDiscrepancyBE.Action = "GetStockPlannerOnHolidayCover"; // getting details of SP on Holiday Cover if any

            if (ViewState["StockPlannerID"] != null)
                oDiscrepancyBE.StockPlannerID = Convert.ToInt32(ViewState["StockPlannerID"].ToString());

            oDiscrepancyBE.SiteID = Convert.ToInt32(ucSite.innerControlddlSite.SelectedItem.Value);
            List<DiscrepancyBE> lstStockPlannerOnHolidayCover = oDiscrepancyBAL.GetStockPlannerOnHolidayCoverBAL(oDiscrepancyBE);

            if (lstStockPlannerOnHolidayCover.Count > 0)
            {
                ViewState["StockPlannerToCoverEmailID"] = lstStockPlannerOnHolidayCover[0].StockPlannerToCoverEmailID;
                ViewState["SPToCoverStockPlannerID"] = lstStockPlannerOnHolidayCover[0].SPToCoverStockPlannerID;
                //lblStockPlannerNoValue.Text = lstStockPlannerOnHolidayCover[0].StockPlannerNO.ToString();// 
                //lblPlannerContactNoValue.Text = lstStockPlannerOnHolidayCover[0].StockPlannerContact.ToString();
            }

            //foreach (GridViewRow row in gvPurchaseOrderDetails.Rows) {
            //    TextBox txtbox = (TextBox)row.FindControl("txtShortage");

            //    sp1.RegisterPostBackControl(txtbox);
            //}
            oDiscrepancyBAL = null;
        }
    }

    private void GetPurchaseOrderList()
    {
        DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
        DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();

        oDiscrepancyBE.Action = "GetPurchaseOrderDate";
        oDiscrepancyBE.SiteID = Convert.ToInt32(ucSite.innerControlddlSite.SelectedValue);
        oDiscrepancyBE.PurchaseOrderNumber = txtPurchaseOrder.Text.Trim();

        List<DiscrepancyBE> lstDetails = oDiscrepancyBAL.GetPurchaseOrderDateListBAL(oDiscrepancyBE);
        ViewState["GetPODateList"] = lstDetails;
        oDiscrepancyBAL = null;
        if (lstDetails.Count > 0 && lstDetails != null)
        {
            FillControls.FillDropDown(ref ddlPurchaseOrderDate, lstDetails, "PurchaseOrder_Order_raised", "RowNumber");
            if (!string.IsNullOrEmpty(ddlPurchaseOrderDate.SelectedItem.Value))
            {
                var PODateDetail = lstDetails.Find(p => p.RowNumber == Convert.ToInt32(ddlPurchaseOrderDate.SelectedItem.Value));
                lblVendorValue.Text = PODateDetail.VendorNoName;
                ucSDRCommunication1.VendorID = Convert.ToInt32(PODateDetail.VendorID);
            }
            //lblVendorValue.Text = ddlPurchaseOrderDate.SelectedItem.Value.ToString();
            //ucSDRCommunication1.VendorID = Convert.ToInt32(lstDetails[ddlPurchaseOrderDate.SelectedIndex].VendorID);
            if (!string.IsNullOrEmpty(ucSite.innerControlddlSite.SelectedValue))
                ucSDRCommunication1.SiteID = Convert.ToInt32(ucSite.innerControlddlSite.SelectedValue);
            else
                ucSDRCommunication1.SiteID = 0;
            ucSDRCommunication1.GetDetails();
        }
        else
        {
            string saveMessage = WebCommon.getGlobalResourceValue("InvalidPurchaseOrder");
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('" + saveMessage + "')", true);

            #region Clearing PO details
            txtPurchaseOrder.Text = string.Empty;
            ddlPurchaseOrderDate.Items.Clear();
            ListItem listItem = new ListItem();
            listItem.Text = "--Select--";
            listItem.Value = "0";
            ddlPurchaseOrderDate.Items.Add(listItem);
            lblVendorValue.Text = string.Empty;
            lblIssueRaisedDate.Text = string.Empty;
            txtPurchaseOrder.Focus();
            #endregion
        }
    }

    private void AddUserControl()
    {
        DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
        DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();
        oDiscrepancyBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
        string CaseOf = String.Empty;
        if (Session["Role"] != null && Session["Role"].ToString().Trim() == "Vendor")
        {
            btnUpdateLocation.Visible = false;
        }
        oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID"));

        if (GetQueryStringValue("RoleTypeFlag") == "A" || GetQueryStringValue("RoleTypeFlag") == null)
        {
            if (Session["UserID"] == null || Session["UserID"].ToString() == "" || Session["Role"] == null || Session["Role"].ToString().Trim() == "")
                return;

            oDiscrepancyBE.User.UserID = Convert.ToInt32(Session["UserID"]);
            CaseOf = Session["Role"].ToString().Trim();
        }
        else
        {
            switch (GetQueryStringValue("RoleTypeFlag").ToString())
            {
                case "G":
                    CaseOf = "OD - Goods In";
                    break;
                case "S":
                    CaseOf = "OD - Stock Planner";
                    break;
                case "P":
                    CaseOf = "OD - Accounts Payable";
                    break;
                case "V":
                    CaseOf = "Vendor";
                    break;
            }
        }

        if (CaseOf == "OD - Stock Planner")
        {
            oDiscrepancyBE.Action = "CheckForInventory";

            List<DiscrepancyBE> lstDetails = oDiscrepancyBAL.GetUserDetailsForWorkFlowBAL(oDiscrepancyBE);
            if (lstDetails != null && lstDetails.Count > 0)
            {
                //check for inventory user
                oDiscrepancyBE.Action = "CheckForActionRequired";
                oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID"));
                oDiscrepancyBE.INVActionRequired = true;
                DataTable dtActionDetails = oDiscrepancyBAL.GetActionRequiredDetailsForWorkFlowBAL(oDiscrepancyBE);
                if (dtActionDetails != null && dtActionDetails.Rows.Count > 0)
                {
                    string sUserControl = "../UserControl/INVAction/" + dtActionDetails.Rows[0]["INVUserControl"].ToString().Trim()
                                            + ".aspx?disLogID=" + GetQueryStringValue("disLogID").ToString()
                                            + "&DisWFlowID=" + dtActionDetails.Rows[0]["DiscrepancyWorkflowID"].ToString()
                                            + "&PONo=" + dtActionDetails.Rows[0]["PurchaseOrderNumber"].ToString()
                                            + "&SiteID=" + SiteID.ToString()
                                            + "&VDRNo=" + VDRNo
                                            + "&FromPage=GoodsReceivedDamaged"
                                            + "&PreviousPage=" + GetQueryStringValue("PreviousPage").ToString();
                    if (chkGoodReturnedDriver.Checked)
                    {
                        Session["GRD"] = true;
                    }
                    else
                    {
                        Session["GRD"] = null;
                    }
                    ifUserControl.Attributes.Add("src", EncryptURL(sUserControl));
                }
                else
                {
                    ifUserControl.Style["display"] = "none";
                }
            }
        }
        else if (CaseOf == "OD - Goods In")
        {
            //check for goods in
            oDiscrepancyBE.Action = "GetDiscrepancyReturnNoteCount";
            DataTable dtReturnNoteCount = oDiscrepancyBAL.GetDiscrepancyReturnNoteCountBAL(oDiscrepancyBE);
            if (dtReturnNoteCount != null && dtReturnNoteCount.Rows.Count > 0)
            {
                if (Convert.ToInt32(dtReturnNoteCount.Rows[0].ItemArray[0].ToString()) > 0)
                    btnPrintReturnsNote.Visible = true;
            }

            oDiscrepancyBE.Action = "CheckForActionRequired";
            oDiscrepancyBE.GINActionRequired = true;

            DataTable dtActionDetails = oDiscrepancyBAL.GetActionRequiredDetailsForWorkFlowBAL(oDiscrepancyBE);
            if (dtActionDetails != null && dtActionDetails.Rows.Count > 0)
            {

                string sUserControl = "../UserControl/GINAction/" + dtActionDetails.Rows[0]["GINUserControl"].ToString().Trim()
                                        + ".aspx?disLogID=" + GetQueryStringValue("disLogID").ToString()
                                        + "&DisWFlowID=" + dtActionDetails.Rows[0]["DiscrepancyWorkflowID"].ToString()
                                        + "&PONo=" + dtActionDetails.Rows[0]["PurchaseOrderNumber"].ToString()
                                        + "&SiteID=" + SiteID.ToString()
                                        + "&VDRNo=" + VDRNo + "&FromPage=GoodsReceivedDamaged"
                                            + "&PreviousPage=" + GetQueryStringValue("PreviousPage").ToString();
                ifUserControl.Attributes.Add("src", EncryptURL(sUserControl));


            }
            else
            {
                ifUserControl.Style["display"] = "none";
            }
        }
        else if (CaseOf == "OD - Accounts Payable")
        {
            //check for account payable in

            oDiscrepancyBE.Action = "CheckForActionRequired";
            oDiscrepancyBE.DiscrepancyWorkFlowID = !string.IsNullOrEmpty(GetQueryStringValue("WorkFlowID")) ? Convert.ToInt32(GetQueryStringValue("WorkFlowID")) : (int?)null;
            oDiscrepancyBE.APActionRequired = true;

            DataTable dtActionDetails = oDiscrepancyBAL.GetActionRequiredDetailsForWorkFlowBAL(oDiscrepancyBE);
            if (dtActionDetails != null && dtActionDetails.Rows.Count > 0)
            {
                //if (dtActionDetails.Rows[0]["APUserControl"].ToString().Trim() == "ACP001")
                if (Convert.ToString(dtActionDetails.Rows[0]["ACP001Control"]).Trim().Equals("ACP001")
                    && Convert.ToString(dtActionDetails.Rows[0]["IsACP001Done"]).Trim().Equals("0"))
                {
                    if (GetQueryStringValue("disLogID") != null)
                        ucApAction.DisLogID = Convert.ToInt32(GetQueryStringValue("disLogID"));

                    ucApAction.DisWFlowID = Convert.ToInt32(dtActionDetails.Rows[0]["DiscrepancyWorkflowID"]);
                    ucApAction.PONumber = Convert.ToString(dtActionDetails.Rows[0]["PurchaseOrderNumber"]);
                    ucApAction.SiteID = SiteID;
                    ucApAction.VDRNo = VDRNo;
                    ucApAction.FromPage = "goodsreceiveddamaged";
                    ucApAction.PreviousPage = GetQueryStringValue("PreviousPage").ToString();

                    if (ViewState["VendorID"] != null)
                        ucApAction.VendorID = Convert.ToInt32(ViewState["VendorID"]);

                    btnAccountsPayableAction.Visible = true;
                    ifUserControl.Style["display"] = "none";
                }
                else
                {
                    string sUserControl = "../UserControl/APAction/" + dtActionDetails.Rows[0]["APUserControl"].ToString().Trim()
                                            + ".aspx?disLogID=" + GetQueryStringValue("disLogID").ToString()
                                            + "&DisWFlowID=" + dtActionDetails.Rows[0]["DiscrepancyWorkflowID"].ToString()
                                            + "&PONo=" + dtActionDetails.Rows[0]["PurchaseOrderNumber"].ToString()
                                            + "&SiteID=" + SiteID.ToString()
                                            + "&VDRNo=" + VDRNo
                                            + "&FromPage=GoodsReceivedDamaged "
                                                + "&PreviousPage=" + GetQueryStringValue("PreviousPage").ToString();
                    ifUserControl.Attributes.Add("src", EncryptURL(sUserControl));
                }
            }
            else
            {
                ifUserControl.Style["display"] = "none";
            }
        }
        else if (CaseOf == "Vendor")
        {
            //check for vendor in
            pnlLocation.Visible = false;
            oDiscrepancyBE.Action = "CheckForActionRequiredForVendor";
            oDiscrepancyBE.VenActionRequired = true;

            DataTable dtActionDetails = oDiscrepancyBAL.GetActionRequiredDetailsForWorkFlowBAL(oDiscrepancyBE);
            if (dtActionDetails != null && dtActionDetails.Rows.Count > 0)
            {

                string sUserControl = "../UserControl/VENAction/" + dtActionDetails.Rows[0]["VENUserControl"].ToString().Trim()
                                        + ".aspx?disLogID=" + GetQueryStringValue("disLogID").ToString()
                                        + "&DisWFlowID=" + dtActionDetails.Rows[0]["DiscrepancyWorkflowID"].ToString()
                                        + "&PONo=" + dtActionDetails.Rows[0]["PurchaseOrderNumber"].ToString()
                                        + "&SiteID=" + SiteID.ToString()
                                        + "&VDRNo=" + VDRNo
                                        + "&FromPage=GoodsReceivedDamaged "
                                        + "&PreviousPage=" + GetQueryStringValue("PreviousPage").ToString();
                ifUserControl.Attributes.Add("src", EncryptURL(sUserControl));
            }
            else
            {
                ifUserControl.Style["display"] = "none";
            }
        }
        else
        {
            ifUserControl.Style["display"] = "none";
            //btnAccountsPayableAction.Visible = true;
        }
        oDiscrepancyBAL = null;
    }

    private void InsertHTML()
    {

        /* Check for the site "No Escalation" */
        MAS_SiteBE oMAS_SiteBE = new MAS_SiteBE();
        MAS_SiteBAL oMAS_SiteBAL = new MAS_SiteBAL();
        oMAS_SiteBE.Action = "GetSiteDisSettings";
        oMAS_SiteBE.SiteID = Convert.ToInt32(ucSite.innerControlddlSite.SelectedItem.Value);

        oMAS_SiteBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();

        List<MAS_SiteBE> lstSiteDetails = oMAS_SiteBAL.GetSiteMisSettingBAL(oMAS_SiteBE);
        oMAS_SiteBAL = null;
        if (lstSiteDetails != null && lstSiteDetails.Count > 0 && lstSiteDetails[0].DIS_CommunicationWithEscalationType.Trim() != "N")
        {
            //site "No Escalation" is not on 
            DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
            DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();

            WorkflowHTML oWorkflowHTML = new WorkflowHTML();
            oDiscrepancyBE.GINHTML = oWorkflowHTML.function1(DateTime.Now, null, null, "Yellow", "",
                                                                   "Damage Discrepancy Created",
                //Session["UserName"].ToString(),
                                                                   UIUtility.GetODVendorUserName(Convert.ToString(Session["Role"]), Convert.ToString(Session["UserName"]), Convert.ToString(Session["LoginID"])),
                                                                   txtInternalComments.Text);
            oDiscrepancyBE.GINActionRequired = false;

            string StockPlannerEmailID = ViewState["StockPlannerEmailID"] == null ? "" : Convert.ToString(ViewState["StockPlannerEmailID"]);

            string StockPlannerToCoverEmailID = ViewState["StockPlannerToCoverEmailID"] == null ? "" : Convert.ToString(ViewState["StockPlannerToCoverEmailID"]);

            if (StockPlannerToCoverEmailID != "")
            {
                oDiscrepancyBE.INVHTML = oWorkflowHTML.function4("Blue", "", null, "", "", FollowingRequestMessage.Replace("{stockPlannerID}", StockPlannerToCoverEmailID).Replace("{Date}", DateTime.Now.ToString("dd/MM/yyyy")), INV003Desc);
            }
            else
            {
                oDiscrepancyBE.INVHTML = oWorkflowHTML.function4("Blue", "", null, "", "", FollowingRequestMessage.Replace("{stockPlannerID}", StockPlannerEmailID).Replace("{Date}", DateTime.Now.ToString("dd/MM/yyyy")), INV003Desc);
            }
           
            oDiscrepancyBE.INVActionRequired = true;
            oDiscrepancyBE.INVUserControl = "INV002";// calling INV002 instead of INV003 as they are same

            oDiscrepancyBE.APHTML = oWorkflowHTML.function3("White", null, "", "");
            oDiscrepancyBE.APActionRequired = false;

            if (ucSDRCommunication1.innerControlRdoCommunication == 'L')   // for letter                
                oDiscrepancyBE.VENHTML = oWorkflowHTML.function4("Blue", "", DateTime.Now, "", "Admin@OfficeDepot.com", "", InitialCommunicationSent, "", Convert.ToString(sLetterLink));
            else
                oDiscrepancyBE.VENHTML = oWorkflowHTML.function4("Blue", "", DateTime.Now, Convert.ToString(sSendMailLink), "Admin@OfficeDepot.com", "", InitialCommunicationSent, "", "");

            oDiscrepancyBE.VenActionRequired = false;

            oDiscrepancyBE.Action = "InsertHTML";
            oDiscrepancyBE.DiscrepancyLogID = DiscrepancyLogID;
            oDiscrepancyBE.LoggedDateTime = DateTime.Now;
            oDiscrepancyBE.LevelNumber = 1;
            oDiscrepancyBE.ActionTakenBy = "GIN";
            oDiscrepancyBE.InventoryActionCategory = "AR";  //Action Required
            int? iResult = oDiscrepancyBAL.addWorkFlowHTMLsBAL(oDiscrepancyBE);
            oDiscrepancyBAL = null;
        }
        else
        {
            //step 1b- close the discrepancy
            DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
            DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();

            WorkflowHTML oWorkflowHTML = new WorkflowHTML();
            oDiscrepancyBE.GINHTML = oWorkflowHTML.function1(DateTime.Now, null, null, "Yellow", "",
                                                                    "Damage Discrepancy Created",
                //Session["UserName"].ToString(),
                                                                    UIUtility.GetODVendorUserName(Convert.ToString(Session["Role"]), Convert.ToString(Session["UserName"]), Convert.ToString(Session["LoginID"])),
                                                                    txtInternalComments.Text);
            oDiscrepancyBE.GINActionRequired = false;

            oDiscrepancyBE.INVHTML = oWorkflowHTML.function3("White", null);
            oDiscrepancyBE.INVActionRequired = false;

            oDiscrepancyBE.APHTML = oWorkflowHTML.function3("White", null);
            oDiscrepancyBE.APActionRequired = false;

            if (ucSDRCommunication1.innerControlRdoCommunication == 'L')   // for letter           
                oDiscrepancyBE.VENHTML = oWorkflowHTML.function4("Blue", "", DateTime.Now, "", "Admin@OfficeDepot.com", InitialCommunicationSent, "", "", Convert.ToString(sLetterLink));
            else
                oDiscrepancyBE.VENHTML = oWorkflowHTML.function4("Blue", "", DateTime.Now, Convert.ToString(sSendMailLink), "Admin@OfficeDepot.com", InitialCommunicationSent);
            oDiscrepancyBE.VenActionRequired = false;

            oDiscrepancyBE.Action = "InsertHTML";
            oDiscrepancyBE.DiscrepancyLogID = DiscrepancyLogID;
            oDiscrepancyBE.LoggedDateTime = DateTime.Now;
            oDiscrepancyBE.LevelNumber = 1;
            oDiscrepancyBE.CloseDiscrepancy = true;
            int? iResult = oDiscrepancyBAL.addWorkFlowHTMLsBAL(oDiscrepancyBE);
            oDiscrepancyBAL = null;
        }
    }

    private void ShowACP001ActionButton()
    {
        DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
        DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();
        oDiscrepancyBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
        string CaseOf = String.Empty;

        oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID"));

        if (GetQueryStringValue("RoleTypeFlag") == "A" || GetQueryStringValue("RoleTypeFlag") == null)
        {
            if (Session["UserID"] == null || Session["UserID"].ToString() == "" || Session["Role"] == null || Session["Role"].ToString().Trim() == "")
                return;

            oDiscrepancyBE.User.UserID = Convert.ToInt32(Session["UserID"]);
            CaseOf = Session["Role"].ToString().Trim();
        }
        //else
        //{
        //    switch (GetQueryStringValue("RoleTypeFlag").ToString())
        //    {
        //        case "G":
        //            CaseOf = "OD - Goods In";
        //            break;
        //        case "S":
        //            CaseOf = "OD - Stock Planner";
        //            break;
        //        case "P":
        //            CaseOf = "OD - Accounts Payable";
        //            break;
        //        case "V":
        //            CaseOf = "Vendor";
        //            break;
        //    }
        //}

        /* Here P is an "OD - Accounts Payable" When from drop down selected the Account Payble option */
        /* Here A will come when actual Account Payble type user will loggedin. */
        if (GetQueryStringValue("RoleTypeFlag") == "P" || GetQueryStringValue("RoleTypeFlag") == "A")
        {
            oDiscrepancyBE.Action = "CheckForActionRequired";
            oDiscrepancyBE.DiscrepancyWorkFlowID = !string.IsNullOrEmpty(GetQueryStringValue("WorkFlowID")) ? Convert.ToInt32(GetQueryStringValue("WorkFlowID")) : (int?)null;
            DataTable dtActionDetails = oDiscrepancyBAL.GetActionRequiredDetailsForWorkFlowBAL(oDiscrepancyBE);
            if (dtActionDetails != null && dtActionDetails.Rows.Count > 0)
            {
                //if (dtActionDetails.Rows[0]["APUserControl"].ToString().Trim() == "ACP001")
                if (Convert.ToString(dtActionDetails.Rows[0]["ACP001Control"]).Trim().Equals("ACP001")
                    && !Convert.ToBoolean(dtActionDetails.Rows[0]["IsACP001Done"]))
                {
                    if (GetQueryStringValue("disLogID") != null)
                        ucApAction.DisLogID = Convert.ToInt32(GetQueryStringValue("disLogID"));

                    ucApAction.DisWFlowID = Convert.ToInt32(dtActionDetails.Rows[0]["DiscrepancyWorkflowID"]);
                    ucApAction.PONumber = Convert.ToString(dtActionDetails.Rows[0]["PurchaseOrderNumber"]);
                    ucApAction.SiteID = SiteID;
                    ucApAction.VDRNo = VDRNo;
                    ucApAction.FromPage = "goodsreceiveddamaged";
                    ucApAction.PreviousPage = GetQueryStringValue("PreviousPage").ToString();

                    if (ViewState["VendorID"] != null)
                        ucApAction.VendorID = Convert.ToInt32(ViewState["VendorID"]);

                    btnAccountsPayableAction.Visible = true;
                    //ifUserControl.Style["display"] = "none";
                }
            }
        }
    }

    /*----------------ADDING RAISE QUERY METHODS------------*/

    private bool IsAlreadyDisputeExist(int discrepancyLogID = 0)
    {
        var isExist = false;

        if (discrepancyLogID != 0)
        {
            var queryDiscrepancyBAL = new DISLog_QueryDiscrepancyBAL();
            var queryDiscrepancyBE = new DISLog_QueryDiscrepancyBE();
            if (discrepancyLogID > 0)
            {
                queryDiscrepancyBE.Action = "IsAlreadyDisputeExist";
                queryDiscrepancyBE.DiscrepancyLogID = discrepancyLogID;
                var result = queryDiscrepancyBAL.IsAlreadyDisputeExistBAL(queryDiscrepancyBE);
                if (result > 0)
                    isExist = true;
            }
        }

        return isExist;
    }
    /* CHECK QUERY IS CLOSED THEN VENDOR CAN RAISE QUERY*/
    private bool IsQueryClosed(int discrepancyLogID = 0)
    {
        var isExist = false;

        if (discrepancyLogID != 0)
        {
            var queryDiscrepancyBAL = new DISLog_QueryDiscrepancyBAL();
            var queryDiscrepancyBE = new DISLog_QueryDiscrepancyBE();
            if (discrepancyLogID > 0)
            {
                queryDiscrepancyBE.Action = "IsAlreadyDisputeExist";
                queryDiscrepancyBE.DiscrepancyLogID = discrepancyLogID;
                var result = queryDiscrepancyBAL.GetQueryClose(queryDiscrepancyBE);
                if (result != 0)
                    isExist = true;
            }
        }

        return isExist;
    }
    /* METHODS FOR VENDOR'S QUERY OVERVIEW*/
    private void VendorsQueryOverview()
    {

        var queryDiscrepancyBAL = new DISLog_QueryDiscrepancyBAL();
        var queryDiscrepancyBE = new DISLog_QueryDiscrepancyBE();
        var disLogID = GetQueryStringValue("disLogID") != null ? Convert.ToInt32(GetQueryStringValue("disLogID")) : 0;
        //var queryDisId = GetQueryStringValue("QueryDisID") != null ? Convert.ToInt32(GetQueryStringValue("QueryDisID")) : 0;
        if (disLogID > 0)
        {
            queryDiscrepancyBE.Action = "GetQueryDiscrepancy";
            // queryDiscrepancyBE.QueryDiscrepancyID = queryDisId;
            queryDiscrepancyBE.DiscrepancyLogID = disLogID;
            var getQueryDiscrepancy = queryDiscrepancyBAL.GetQueryDiscrepancyBAL(queryDiscrepancyBE);
            if (getQueryDiscrepancy.Count > 0)
            {
                pnlQueryOverview_1.Visible = true;
                rptQueryOverview.DataSource = getQueryDiscrepancy;
                rptQueryOverview.DataBind();

            }
        }
    }
    protected void rptQueryOverview_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            var hdnIsQueryClosedManually = (HiddenField)e.Item.FindControl("hdnIsQueryClosedManually");

            // GoodsIn declaration ...            
            var lblCommentLabelT_2 = (ucLabel)e.Item.FindControl("lblCommentLabelT_2");
            if (lblCommentLabelT_2 != null)
            {
                if (!string.IsNullOrEmpty(lblCommentLabelT_2.Text) ) { e.Item.FindControl("tblGoodsIn").Visible = true; }
                else { e.Item.FindControl("tblGoodsIn").Visible = false;  }

                if (!string.IsNullOrEmpty(lblCommentLabelT_2.Text) && hdnIsQueryClosedManually.Value == "True")
                {
                    e.Item.FindControl("tblGoodsIn").Visible = false;
                    e.Item.FindControl("tblQueryClosedManually").Visible = true;
                }

                var lnkView = (System.Web.UI.HtmlControls.HtmlAnchor)e.Item.FindControl("lnkView");
                var hdnQueryDiscrepancyID = (HiddenField)e.Item.FindControl("hdnQueryDiscrepancyID");
                var hdnDiscrepancyLogID = (HiddenField)e.Item.FindControl("hdnDiscrepancyLogID");
                lnkView.Attributes.Add("href", EncryptQuery("~/ModuleUI/Appointment/Receiving/APPRcv_Communication.aspx?DiscId=" + hdnDiscrepancyLogID.Value
                    + "&QueryDiscId=" + hdnQueryDiscrepancyID.Value));
            }
        }
    }

    #endregion

    protected void btnPrintReturnsNote_Click(object sender, EventArgs e)
    {
        btnSave.Visible = false;
        btnBack.Visible = false;

        PrintAndSaveReturnNote();
        btnPrintReturnsNote.Visible = true;
    }

    protected void btnUpdateLocation_Click(object sender, EventArgs e)
    {
        var disLogID = GetQueryStringValue("disLogID") != null ? Convert.ToInt32(GetQueryStringValue("disLogID")) : 0;
        if (disLogID != null)
        {
            DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
            DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();
            oDiscrepancyBE.Action = "UpdateLocation";
            oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(disLogID);
            oDiscrepancyBE.Location = txtLocation.Text.Trim();
            oDiscrepancyBE.UserID = Convert.ToInt32(Session["UserID"]);
            string sResult = oDiscrepancyBAL.updateDiscrepancyLocationBAL(oDiscrepancyBE);
            if (GetQueryStringValue("PreviousPage") != null)
            {
                if (GetQueryStringValue("PreviousPage").Trim().ToUpper().Equals("SearchResultVendor".ToUpper()))
                {
                    btnBack_1.Visible = false;
                    btnUpdateLocation.Visible = false;
                    EncryptQueryString("~/ModuleUI/Discrepancy/DIS_SearchResultVendor.aspx?disLogID=" + Convert.ToString(GetQueryStringValue("disLogID"))
                                + "&VDRNo=" + Convert.ToString(GetQueryStringValue("VDRNo"))
                                + "&UserID=" + Convert.ToString(GetQueryStringValue("UserID"))
                                + "&PreviousPage=SearchResultVendor&WorkFlowID=" + Convert.ToString(GetQueryStringValue("WorkFlowID").ToString()));
                }

                if (Session["PageMode"].ToString() == "Todays")
                {
                    EncryptQueryString("../DIS_SearchResult.aspx?FromPage=images&SiteId=" + GetQueryStringValue("SiteId") + "&DisDate=" + GetQueryStringValue("DisDate"));
                }
                else if (Session["PageMode"].ToString() == "Search")
                {
                    //For opening search discrepancies mode
                    btnBack_1.Visible = false;
                    btnUpdateLocation.Visible = false;
                    EncryptQueryString("~/ModuleUI/Discrepancy/DIS_SearchResult.aspx?disLogID=" + Convert.ToString(GetQueryStringValue("disLogID"))
                                + "&VDRNo=" + Convert.ToString(GetQueryStringValue("VDRNo"))
                                + "&UserID=" + Convert.ToString(GetQueryStringValue("UserID"))
                                + "&PreviousPage=SearchResult&WorkFlowID=" + Convert.ToString(GetQueryStringValue("WorkFlowID").ToString()));

                }
            }
            else if (GetQueryStringValue("PN") != null)
            {
                if (GetQueryStringValue("PN").Trim().ToUpper().Equals("POINQ"))
                {
                    btnBack_1.Visible = false;
                    btnUpdateLocation.Visible = false;
                    EncryptQueryString("~/ModuleUI/Appointment/PurchaseOrderInquiry.aspx?SId=" + GetQueryStringValue("SId").ToString()
                        + "&Status=" + GetQueryStringValue("Status").ToString()
                        + "&PN=DISLOG"
                        + "&PO=" + GetQueryStringValue("PO").ToString());
                }
                else if (GetQueryStringValue("PN").Trim().ToUpper().Equals("OPENPL"))
                {
                    EncryptQueryString("~/ModuleUI/StockOverview/Expedite/OpenPurchaseListing.aspx?action=showlisting");
                }


            }
        }
    }

    protected void btnContinue_Click(object sender, EventArgs e)
    {
        string EncryptcUrl = EncryptQuery("~/ModuleUI/Discrepancy/DISLog_QueryDiscrepancy.aspx?disLogID=" + Convert.ToString(GetQueryStringValue("disLogID"))
                        + "&VDRNo=" + Convert.ToString(GetQueryStringValue("VDRNo")) + "&RaiseQueryVendor=Yes" + "&RedirectTo=DisLog_Overs.aspx");
        //Response.Redirect(EncryptcUrl);
        Response.Redirect(EncryptcUrl, false);
        Context.ApplicationInstance.CompleteRequest();
    }

    protected void btnRaiseQueryPopupBack_Click(object sender, EventArgs e)
    {
        btnBack.Visible = false;
        btnSave.Visible = false;
    }
    private void BindDiscrepancyLogComment(DiscrepancyBE discrepancyBE = null)
    {
        var discrepancyBAL = new DiscrepancyBAL();
        if (discrepancyBE != null)
        {
            rptDiscrepancyComments.DataSource = discrepancyBAL.GetDiscrepancyLogCommentsBAL(discrepancyBE);
        }
        else
        {
            discrepancyBE = new DiscrepancyBE();
            discrepancyBE.Action = "GetDiscrepancyLogComment";

            if (!string.IsNullOrEmpty(GetQueryStringValue("disLogID")))
                discrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID"));
            else
                discrepancyBE.DiscrepancyLogID = 0;

            rptDiscrepancyComments.DataSource = discrepancyBAL.GetDiscrepancyLogCommentsBAL(discrepancyBE);
        }

        rptDiscrepancyComments.DataBind();

        if (rptDiscrepancyComments.Items.Count > 0)
            lblComments.Visible = true;
        else
            lblComments.Visible = false;
        
    }
}
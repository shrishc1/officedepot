﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    AutoEventWireup="true" CodeFile="DISLog_InvoiceQueryDiscrepancy.aspx.cs" Inherits="DISLog_InvoiceDiscrepancy" %>

<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register TagName="ucDate" TagPrefix="cc2" Src="~/CommonUI/UserControls/ucDate.ascx" %>
<%@ Register Src="../UserControl/InvoiceQueryucSDRCommunication.ascx" TagName="ucSDRCommunication"
    TagPrefix="uc2" %>
<%@ Register Src="../../../CommonUI/UserControls/ucSite.ascx" TagName="ucSite" TagPrefix="cc2" %>
<%@ Register Src="../UserControl/APAction/APAction.ascx" TagName="APAction" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%	  
        Response.WriteFile("DiscrepancyImages.txt");
    %>
     <script src="../../../Scripts/iFrameHeight.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">

        $(document).ready(function () {
            $("body").keypress(function (e) {
                if (e.which == 13 || e.keyCode == 13) {
                    return false;
                }
            });
        });

      Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequest);
      function EndRequest(sender, args) {          
          if (args.get_error() == undefined) {
              $('#<%=btnContinue.ClientID %>').addClass('button');
              $('#<%=btnCancel.ClientID %>').addClass('button');
          } 
      }
        function checkBlankEmailAddress() {
            var rbEmailComm = document.getElementById('<%=ucSDRCommunication1.FindControl("rdoEmailComm").ClientID %>');
            var objTxtAltEmail = document.getElementById('<%=ucSDRCommunication1.FindControl("txtAltEmail").ClientID %>');
            var objucTextBox1 = document.getElementById('<%=ucSDRCommunication1.FindControl("ucVendorEmailList").ClientID %>');
            if (checkEmailAddress(rbEmailComm, objTxtAltEmail, objucTextBox1, '<%=ValidEmail %>', '<%=NoEmailSpecifiedMessage%>') == false) {

                return false;
            }
        }

      <%--  $(document).ready(function () {
            $("#<%=ifUserControl.ClientID %>").load(function () {
                var iFrame = parent.document.getElementById("<%=ifUserControl.ClientID %>");
                newHeight = parseInt(iFrame.contentWindow.document.body.offsetHeight) + 140;
                $("#" + iFrame.id).height(newHeight);
            });
        });--%>
    </script>
    <asp:ScriptManager ID="sp1" runat="server">
    </asp:ScriptManager>
    <asp:HiddenField ID="hdnSiteID" runat="server" />
    <asp:UpdatePanel ID="up1" runat="server">
        <ContentTemplate>
            <h2>
                <cc1:ucLabel ID="lblInvoiceQueryDiscrepancy" runat="server" Text="Invoice"></cc1:ucLabel>
            </h2>
            <div class="button-row">
                <cc1:ucButton ID="btnAccountsPayableAction" runat="server" class="button" OnClick="btnAccountsPayableAction_Click"
                    Visible="false" />
                <cc1:ucButton ID="btnRaiseQuery" runat="server" class="button" OnClick="btnRaiseQuery_Click" />
                <cc1:ucButton ID="btnAddComment" Text="Add Comment" runat="server" class="button"
                    OnClick="btnAddComment_Click" />
                <cc1:ucButton ID="btnAddImages" Text="Add Images" runat="server" class="button" OnClick="btnAddImages_Click" />
                <a href="#" rel="shadowbox[gal]" target="_blank" style="padding-left: 0px;">
                    <input type="button" id="btnViewImage" value="View Images" runat="server" title="View Images"
                        class="button" clientidmode="Static" /></a>
                <cc1:ucButton ID="btnViewWF" runat="server" Text="Workflow" class="button" OnClick="btnViewWF_Click" />

                 <a href="#" rel="shadowbox[gal123]" target="_blank" style="padding-left: 0px;">
                    <input type="button" id="btnViewPOD" value="View Uploaded DOCs" runat="server" title="View Uploaded DOCs"
                        class="button" clientidmode="Static" visible="false" /></a>            
            </div>
            <div id="discrepancygallery" style="display: none" runat="server" clientidmode="Static">
            </div>
            <div id="discrepancygallerydocs" style="display: none" runat="server" clientidmode="Static">
            </div>
            <div class="right-shadow">
                <div class="formbox">
                    <cc1:ucMultiView ID="mvDiscrepancyInvoiceCriteria" runat="server" ActiveViewIndex="0">
                        <cc1:ucView ID="vwInvoiceCriteria" runat="server">
                            <table width="40%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                                <tr>
                                    <td style="font-weight: bold; width: 40%">
                                        <cc1:ucLabel ID="lblSitePrefixName" runat="server" Text="Site"></cc1:ucLabel>
                                    </td>
                                    <td style="width: 5%">
                                        :
                                    </td>
                                    <td style="width: 55%">
                                        <cc2:ucSite ID="ucSite" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="font-weight: bold;">
                                        <cc1:ucLabel ID="lblPurchaseOrder" runat="server" Text="Purchase Order" isRequired="true"></cc1:ucLabel>
                                    </td>
                                    <td>
                                        :
                                    </td>
                                    <td>
                                        <cc1:ucTextbox ID="txtPurchaseOrder" runat="server" Width="120px" MaxLength="11"
                                            AutoPostBack="true" OnTextChanged="txtPurchaseOrder_TextChanged"></cc1:ucTextbox>
                                        <asp:RequiredFieldValidator ID="rfvPurchaseOrderNumberValidation" runat="server"
                                            ControlToValidate="txtPurchaseOrder" Display="None" ValidationGroup="LogDiscrepancy">
                                        </asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="font-weight: bold;">
                                        <cc1:ucLabel ID="lblPurchaseOrderDate" runat="server" Text="Purchase Order Date"></cc1:ucLabel>
                                    </td>
                                    <td>
                                        :
                                    </td>
                                    <td>
                                        <cc1:ucDropdownList ID="ddlPurchaseOrderDate" runat="server" Width="150px" AutoPostBack="True"
                                            OnSelectedIndexChanged="ddlPurchaseOrderDate_SelectedIndexChanged">
                                            <asp:ListItem Text="--Select--" Value="0"></asp:ListItem>
                                        </cc1:ucDropdownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="font-weight: bold;">
                                        <cc1:ucLabel ID="lblVendor" runat="server" Text="Vendor"></cc1:ucLabel>
                                    </td>
                                    <td>
                                        :
                                    </td>
                                    <td>
                                        <cc1:ucLabel ID="lblVendorValue" runat="server"></cc1:ucLabel>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="font-weight: bold;">
                                        <cc1:ucLabel ID="lblInvoiceNo" isRequired="true" runat="server" Text="Invoice #"></cc1:ucLabel>
                                    </td>
                                    <td>
                                        :
                                    </td>
                                    <td>
                                        <cc1:ucTextbox ID="txtInvoiceNumber" runat="server" Width="120px" MaxLength="15"></cc1:ucTextbox>
                                        <asp:RequiredFieldValidator ID="rfvInvoiceNumberRequired" runat="server" ControlToValidate="txtInvoiceNumber"
                                            Display="None" ValidationGroup="LogDiscrepancy">
                                        </asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <asp:ValidationSummary ID="vSummary" runat="server" ShowMessageBox="true" ShowSummary="false"
                                            Style="color: Red" ValidationGroup="LogDiscrepancy" />
                                    </td>
                                </tr>
                            </table>
                        </cc1:ucView>
                        <cc1:ucView ID="vwLogSDR" runat="server">
                            <table width="100%" cellspacing="0" cellpadding="0" class="form-table">
                                <tr valign="top">
                                    <td width="30%">
                                        <cc1:ucPanel ID="pnlDeliveryDetail" runat="server" GroupingText="Delivery Detail"
                                            CssClass="fieldset-form">
                                            <table width="100%" cellspacing="1" cellpadding="0" class="form-table" height="140px">
                                                <tr>
                                                    <td style="font-weight: bold; width: 42%">
                                                        <cc1:ucLabel ID="lblSiteDetail" runat="server" Text="Site"></cc1:ucLabel>
                                                    </td>
                                                    <td style="font-weight: bold; width: 2%">
                                                        :
                                                    </td>
                                                    <td class="nobold" colspan="2">
                                                        <cc1:ucLabel ID="lblSiteValue" runat="server"></cc1:ucLabel>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="font-weight: bold;">
                                                        <cc1:ucLabel ID="lblPurchaseOrderNo" runat="server" Text="Purchase Order #"></cc1:ucLabel>
                                                    </td>
                                                    <td style="font-weight: bold;">
                                                        :
                                                    </td>
                                                    <td class="nobold" style="width: 22%">
                                                        <cc1:ucLabel ID="lblPurchaseOrderValue" runat="server"></cc1:ucLabel>
                                                    </td>
                                                    <td class="nobold" style="width: 34%">
                                                        <cc1:ucLabel ID="lblPurchaseOrderDateValue" runat="server"></cc1:ucLabel>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="font-weight: bold;">
                                                        <cc1:ucLabel ID="lbl2_InvoiceNo" runat="server" Text="Invoice #"></cc1:ucLabel>
                                                    </td>
                                                    <td style="font-weight: bold;">
                                                        :
                                                    </td>
                                                    <td class="nobold">
                                                        <cc1:ucLabel ID="lblInvoiceyNoValue" runat="server"></cc1:ucLabel>
                                                    </td>
                                                    <td class="nobold">
                                                    </td>
                                                </tr>
                                            </table>
                                        </cc1:ucPanel>
                                    </td>
                                    <td width="2%">
                                        &nbsp;
                                    </td>
                                    <td width="35%">
                                        <cc1:ucPanel ID="pnlContactDetail" runat="server" GroupingText="Contact Detail" CssClass="fieldset-form">
                                            <table width="100%" height="140px">
                                                <tr>
                                                    <td>
                                                        <table width="100%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings"
                                                            height="62px">
                                                            <tr>
                                                                <td style="font-weight: bold; width: 30%">
                                                                    <cc1:ucLabel ID="lblVendor_1" runat="server" Text="Vendor "></cc1:ucLabel>
                                                                </td>
                                                                <td style="font-weight: bold; width: 2%">
                                                                    :
                                                                </td>
                                                                <td class="nobold" style="width: 68%">
                                                                    <cc1:ucLabel ID="lblVendorNumberValue" runat="server"></cc1:ucLabel>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="font-weight: bold;">
                                                                    <cc1:ucLabel ID="lblContactNo" runat="server" Text="Contact #"></cc1:ucLabel>
                                                                </td>
                                                                <td style="font-weight: bold;">
                                                                    :
                                                                </td>
                                                                <td class="nobold">
                                                                    <cc1:ucLabel ID="lblContactValue" runat="server"></cc1:ucLabel>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <table width="100%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings"
                                                            height="61px">
                                                            <tr>
                                                                <td style="font-weight: bold; width: 30%">
                                                                    <cc1:ucLabel ID="lblAccountPayableName" runat="server" Text="AP Clerk"></cc1:ucLabel>
                                                                </td>
                                                                <td style="font-weight: bold; width: 2%">
                                                                    :
                                                                </td>
                                                                <td class="nobold" style="width: 68%">
                                                                    <cc1:ucLabel ID="lblAccountPayableNameValue" runat="server"></cc1:ucLabel>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="font-weight: bold;">
                                                                    <cc1:ucLabel ID="lblContactNo_1" runat="server" Text="Contact #"></cc1:ucLabel>
                                                                </td>
                                                                <td style="font-weight: bold;">
                                                                    :
                                                                </td>
                                                                <td class="nobold">
                                                                    <cc1:ucLabel ID="lblAPContactValue" runat="server"></cc1:ucLabel>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </cc1:ucPanel>
                                    </td>
                                    <td width="2%">
                                        &nbsp;
                                    </td>
                                    <td width="31%" rowspan="7" valign="top">
                                        <cc1:ucPanel ID="pnlCommunicationDetail" runat="server" GroupingText="Communication Detail"
                                            CssClass="fieldset-form">
                                            <table width="100%" cellspacing="0" cellpadding="0" class="form-table" height="140px">
                                                <tr>
                                                    <td>
                                                        <uc2:ucSDRCommunication ID="ucSDRCommunication1" runat="server" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </cc1:ucPanel>
                                    </td>
                                </tr>
                            </table>
                            <cc1:ucGridView ID="gvLogInvoice" Width="100%" runat="server" CssClass="grid">
                                <Columns>
                                    <asp:TemplateField HeaderText="Line" SortExpression="PurchaseOrder.Line_No">
                                        <HeaderStyle Width="6%" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="lblLineNo" Text='<%# Eval("PurchaseOrder.Line_No") %>'></cc1:ucLabel>
                                            <asp:HiddenField ID="hdnPurchaseOrderID" runat="server" Value='<%#Eval("PurchaseOrder.PurchaseOrderID") %>' />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Office Depot Code" SortExpression="PurchaseOrder.OD_Code">
                                        <HeaderStyle Width="10%" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="lblOfficeDepotCodeValue" Text='<%# Eval("PurchaseOrder.OD_Code") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Viking Code" SortExpression="PurchaseOrder.Direct_code">
                                        <HeaderStyle Width="10%" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="lblVikingCodeValue" Text='<%# Eval("PurchaseOrder.Direct_code") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Vendor Item Code" SortExpression="PurchaseOrder.Vendor_Code">
                                        <HeaderStyle Width="10%" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="lblVendorItemCodeValue" Text='<%# Eval("PurchaseOrder.Vendor_Code") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Description" SortExpression="PurchaseOrder.ProductDescription">
                                        <HeaderStyle Width="30%" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="lblDescriptionValue" Text='<%# Eval("PurchaseOrder.ProductDescription") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="UOM" SortExpression="PurchaseOrder.UOM">
                                        <HeaderStyle Width="5%" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="lblUoM1" Text='<%# Eval("PurchaseOrder.UOM") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Original PO Quantity" SortExpression="PurchaseOrder.Original_quantity">
                                        <HeaderStyle Width="7%" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="lblOriginalPOQuantity1" Text='<%# Eval("PurchaseOrder.Original_quantity") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Qty Receipted by OD" >
                                        <HeaderStyle Width="7%" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                           <asp:TextBox ID="txtReceiptedQty" runat="server" Width="80px" MaxLength="6" onkeyup="AllowNumbersOnly(this);"></asp:TextBox>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Invoice Quantity">
                                        <HeaderStyle Width="20%" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtInvoiceQty" runat="server" Width="80px" MaxLength="6" onkeyup="AllowNumbersOnly(this);"></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </cc1:ucGridView>
                            <table width="100%">
                                <tr style="height: 20px">
                                    <td align="right">
                                        <cc1:ucButton ID="btnAddLine" runat="server" Text="Add" CssClass="button" OnClick="btnAdd_Click"
                                            Visible="false" />
                                    </td>
                                </tr>
                            </table>
                            <cc1:ucGridView ID="gvLogInvoiceAddLine" runat="server" AutoGenerateColumns="False"
                                CellPadding="0" CssClass="grid" GridLines="None" Width="100%">
                                <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                                <Columns>
                                    <asp:TemplateField HeaderText="Line">
                                        <HeaderStyle Width="6%" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucTextbox runat="server" ID="txtLineNo" Text='<%# Eval("Line_No") %>' Width="95%"
                                                EnableViewState="true" MaxLength="3" onkeyup="AllowNumbersOnly(this);" AutoPostBack="true"
                                                OnTextChanged="TextBox_TextChanged_txtLineNo"></cc1:ucTextbox>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Office Depot Code">
                                        <HeaderStyle Width="10%" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucTextbox runat="server" ID="txtOfficeDepotCode" Text='<%# Eval("OD_Code") %>'
                                                Width="95%" AutoPostBack="true" MaxLength="10" OnTextChanged="TextBox_TextChanged_txtOfficeDepotCode">
                                            </cc1:ucTextbox>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Viking Code">
                                        <HeaderStyle Width="10%" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucTextbox runat="server" ID="txtVikingCode" Text='<%# Eval("Direct_code") %>'
                                                MaxLength="10" Width="95%"></cc1:ucTextbox>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Vendor Item Code">
                                        <HeaderStyle Width="10%" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucTextbox runat="server" ID="txtVendorItemCode" Text='<%# Eval("Vendor_Code") %>'
                                                MaxLength="10" Width="95%"></cc1:ucTextbox>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Description">
                                        <HeaderStyle Width="35%" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucTextbox runat="server" ID="txtDescription" Text='<%# Eval("ProductDescription") %>'
                                                MaxLength="250" Width="98%"></cc1:ucTextbox>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="UOM">
                                        <HeaderStyle Width="5%" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucTextbox runat="server" ID="txtUoM" Text='<%# Eval("UOM") %>' Width="92%" MaxLength="5"></cc1:ucTextbox>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Original PO Quantity">
                                        <HeaderStyle Width="7%" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucTextbox runat="server" ID="txtOriginalPOQuantity" Text='<%# Eval("Original_quantity") %>'
                                                Width="5em" />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Qty Receipted by OD">
                                        <HeaderStyle Width="7%" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucTextbox runat="server" ID="txtReceiptedQty" Text='<%# Eval("Receipted_Qty") %>'
                                                Width="5em" />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Invoice Quantity">
                                        <HeaderStyle Width="10%" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucTextbox runat="server" ID="txtInvoiceQty" MaxLength="6" onkeyup="AllowNumbersOnly(this);"
                                                Width="95%" Text='<%#Eval("InvoiceQuantity") %>'></cc1:ucTextbox>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderStyle Width="10%" HorizontalAlign="right" />
                                        <ItemTemplate>
                                            <cc1:ucButton ID="btnAddLine" runat="server" Text="Add Line" CommandArgument='<%#Eval("RecordID") %>'
                                                CommandName="AddLine" OnCommand="AddLine_Click" CssClass="button" />
                                            <cc1:ucButton ID="btnRemove" runat="server" Text="Remove" CommandArgument='<%#Eval("RecordID") %>'
                                                CommandName="Remove" OnCommand="RemoveImage_Click" CssClass="button" OnClientClick="return ConfirmMess();"
                                                Style="display: none" />
                                            <asp:HiddenField ID="hdnRecordID" runat="server" Value='<%#Eval("RecordID") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                            </cc1:ucGridView>
                            <cc1:ucGridView ID="gvLogInvoiceViewMode" Width="100%" runat="server" CssClass="grid"
                                OnSorting="SortGrid" Style="display: none" AllowSorting="true">
                                <Columns>
                                    <asp:TemplateField HeaderText="Line" SortExpression="Line_no">
                                        <HeaderStyle Width="8%" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="lblLineNo2" Text='<%# Eval("Line_no") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Office Depot Code" SortExpression="ODSKUCode">
                                        <HeaderStyle Width="8%" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="lblOfficeDepotCode2" Text='<%# Eval("ODSKUCode") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Viking Code" SortExpression="DirectCode">
                                        <HeaderStyle Width="8%" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="lblVikingCode2" Text='<%# Eval("DirectCode") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Vendor Item Code" SortExpression="VendorCode">
                                        <HeaderStyle Width="10%" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel ID="lblVendorItemCode1" runat="server" Text='<%# Eval("VendorCode") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Description" SortExpression="ProductDescription">
                                        <HeaderStyle Width="35%" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="lblDescription2" Text='<%# Eval("ProductDescription") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="UOM" SortExpression="UOM">
                                        <HeaderStyle Width="5%" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="lblUoM2" Text='<%# Eval("UOM") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Original PO Quantity" SortExpression="OriginalQuantity">
                                        <HeaderStyle Width="5%" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="lblOriginalPOQuantity2" Text='<%# Eval("OriginalQuantity") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Qty Receipted by OD" SortExpression="ReceiptedQty">
                                        <HeaderStyle Width="5%" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="lblReceiptedQty2" Text='<%# Eval("ReceiptedQty") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Invoice Quantity" SortExpression="InvoiceQty">
                                        <HeaderStyle Width="5%" HorizontalAlign="Right" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="lbl_InvoiceQty" Text='<%# Eval("InvoiceQty") %>'
                                                Width="50px"></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                </Columns>
                            </cc1:ucGridView>
                            <br />
                            <cc1:ucPanel ID="pnlInternalComments" GroupingText="Internal Comments" runat="server"
                                CssClass="fieldset-form">
                                <table width="100%" cellspacing="5" cellpadding="0" border="0" class="form-table">
                                    <tr>
                                        <td style="font-weight: bold;">
                                            <cc1:ucTextbox ID="txtInternalComments" runat="server" CssClass="inputbox" onkeyup="checkTextLengthOnKeyUp(this,1000);"
                                                TextMode="MultiLine" Width="98%"></cc1:ucTextbox>
                                        </td>
                                    </tr>
                                </table>
                            </cc1:ucPanel>
                            <cc1:ucPanel ID="pnlDecisionComments" Width="100%" runat="server" Visible="false"
                                GroupingText="Decision & Comments"
                                CssClass="fieldset-form">
                                <table width="95%" cellspacing="5" cellpadding="0" border="0" class="form-table">
                                    <tr>
                                        <td style="font-weight: bold;">
                                            <cc1:ucLabel ID="lblDecision" runat="server"> </cc1:ucLabel>
                                            <br />
                                            <br />
                                            <cc1:ucLabel ID="lblSPActionComments" runat="server"></cc1:ucLabel>
                                        </td>
                                    </tr>
                                </table>
                            </cc1:ucPanel>
                           <cc1:ucPanel ID="pnlAPComments" GroupingText="AP Comments" runat="server" Visible="false"
                                CssClass="fieldset-form">
                                <table width="100%" cellspacing="5" cellpadding="0" border="0" class="form-table">
                                    <tr>
                                        <td style="font-weight: bold;">
                                            <cc1:ucTextbox ID="txtAPComments" runat="server" CssClass="inputbox" onkeyup="checkTextLengthOnKeyUp(this,1000);"
                                                TextMode="MultiLine" Width="98%"></cc1:ucTextbox>
                                        </td>
                                    </tr>
                                </table>
                            </cc1:ucPanel>

                         <%--   <cc1:ucPanel ID="pnlGIComments" GroupingText="AP Comments" runat="server" Visible="false"
                                CssClass="fieldset-form">
                                <table width="100%" cellspacing="5" cellpadding="0" border="0" class="form-table">
                                    <tr>
                                        <td style="font-weight: bold;">
                                            <cc1:ucTextbox ID="txtGIComments" runat="server" CssClass="inputbox" onkeyup="checkTextLengthOnKeyUp(this,1000);"
                                                TextMode="MultiLine" Width="98%"></cc1:ucTextbox>
                                        </td>
                                    </tr>
                                </table>
                            </cc1:ucPanel>
                         --%>
                            <cc1:ucPanel ID="pnlVENComments" GroupingText="Vendor Comments" runat="server" Visible="false"
                                CssClass="fieldset-form">
                                <table width="100%" cellspacing="5" cellpadding="0" border="0" class="form-table">
                                    <tr>
                                        <td style="font-weight: bold;">
                                            <cc1:ucTextbox ID="txtVENComments" runat="server" CssClass="inputbox" onkeyup="checkTextLengthOnKeyUp(this,1000);"
                                                TextMode="MultiLine" Width="98%"></cc1:ucTextbox>
                                        </td>
                                    </tr>
                                </table>
                            </cc1:ucPanel>

                            <cc1:ucPanel ID="pnlLocation" Width="25%" GroupingText="Location" runat="server"
                                Visible="false" CssClass="fieldset-form">
                                <table width="95%" cellspacing="5" cellpadding="0" border="0" class="form-table">
                                    <tr>
                                        <td style="font-weight: bold;">
                                            <cc1:ucTextbox ID="txtLocation" runat="server" CssClass="inputbox" onkeyup="checkTextLengthOnKeyUp(this,100);"
                                                TextMode="MultiLine" Width="98%"></cc1:ucTextbox>
                                        </td>
                                    </tr>
                                </table>
                            </cc1:ucPanel>
                            <table width="100%" cellspacing="0" cellpadding="0" class="form-table">
                                <tr>
                                    <td>
                                        <div class="button-row" style="text-align: right">
                                            <cc1:ucButton ID="btnUpdateLocation" runat="server" Text="Update Location" OnClick="btnUpdateLocation_Click"
                                                class="button" Visible="false" />
                                            &nbsp;
                                            <cc1:ucButton ID="btnBack_1" runat="server" Visible="false" CssClass="button" OnClick="btnBack_1_Click" />
                                                <cc1:ucButton ID="btnBack_AP" runat="server" CssClass="button" OnClick="btnBackToAP_Click"
                                                Visible="false" />
                                        </div>
                                    </td>
                                </tr>
                            </table>
                            <iframe id="ifUserControl" runat="server" frameborder="0" scrolling="no"
                                width="954" style="margin: 0px 0px 0px 0px; background-image: url('../Images/conteint-mainbg.jpg') no-repeat scroll right bottom transparent;">
                            </iframe>
                        </cc1:ucView>
                    </cc1:ucMultiView>
                </div>
                  <br />
            <cc1:ucLabel ID="lblComments" Text="Comments" Font-Bold="true" Visible="false" runat="server"></cc1:ucLabel>
            <div style="border-bottom: 1px solid black;">
                &nbsp;</div>
            <asp:Repeater ID="rptDiscrepancyComments" runat="server">
                <ItemTemplate>
                    <table cellspacing="5" cellpadding="0" align="center" style="border: 1px solid black;
                        border-top: none; width: 100%">
                        <tr>
                            <td style="font-weight: bold; width: 10%;">
                                <cc1:ucLabel ID="lblDate_1" Text="Date" runat="server"></cc1:ucLabel>/<cc1:ucLabel
                                    ID="lblActualTime" Text="Time" runat="server"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold; width: 1%;">
                                :
                            </td>
                            <td style="width: 22%;">
                                <cc1:ucLabel ID="lblDateT_1" Text='<%#Eval("CommentDateTime", "{0:dd/MM/yyyy}") + "  " + Eval("CommentDateTime",@"{0:HH:mm}")%>'
                                    runat="server"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold; width: 10%;">
                                <cc1:ucLabel ID="lblUser_1" Text="User" runat="server"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold; width: 1%;">
                                :
                            </td>
                            <td style="width: 56%;">
                                <cc1:ucLabel ID="lblUserT_1" Text='<%#DataBinder.Eval(Container.DataItem, "Username")%>'
                                    runat="server"></cc1:ucLabel>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold; width: 100%;" colspan="6">
                                <cc1:ucLabel ID="lblCommentLabel_1" Text="Comment" runat="server"></cc1:ucLabel>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 100%;" colspan="6">
                                <cc1:ucLabel Style="width: 970px; word-wrap: break-word; display: block;" ID="lblCommentLabelT"
                                    Text='<%#DataBinder.Eval(Container.DataItem, "Comment")%>' runat="server"></cc1:ucLabel>
                            </td>
                        </tr>
                    </table>
                </ItemTemplate>
            </asp:Repeater>
            </div>
            <asp:Button ID="btnViewNoPurchaseOrder" runat="Server" Style="display: none" />
            <ajaxToolkit:ModalPopupExtender ID="mdlInvoiceViewer" runat="server" TargetControlID="btnViewNoPurchaseOrder"
                PopupControlID="pnlbtnViewNoPurchaseOrderViewer" BackgroundCssClass="modalBackground"
                BehaviorID="NoPurchaseOrderViewer" DropShadow="false" />
            <asp:Panel ID="pnlbtnViewNoPurchaseOrderViewer" runat="server" Style="display: none;
                width: 40%; height: 10%;">
                <div style="width: 100%; height: 100%; overflow-y: hidden; overflow-x: hidden; background-color: #fff;
                    padding: 5px; border: 2px solid #ccc; text-align: left;">
                    <table width="100%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                        <tr>
                            <td style="font-weight: bold;" width="100%">
                                <cc1:ucLabel ID="lblInvalidProductCode" runat="server"></cc1:ucLabel>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <cc1:ucButton ID="btnYes" Width="60px" class="button" runat="server" OnClientClick="javascript:$find('NoPaperWorkViewer').hide();return false;"
                                    OnClick="btnOk_Click" />
                                &nbsp;
                                <cc1:ucButton ID="btnNo" Width="60px" class="button" runat="server" OnClick="btnCancel_Click" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
            <div class="button-row">
                <cc1:ucButton ID="btnLog" runat="server" Text="Log Discrepancy" class="button" OnClick="btnLogDiscrepancy_Click"
                    ValidationGroup="LogDiscrepancy" />
                <cc1:ucButton ID="btnSave" runat="server" Text="Save" class="button" OnClientClick="if (!Page_ClientValidate()){ return false; } this.disabled = true; this.value = 'Saving...';"
                    UseSubmitBehavior="false" OnClick="btnSave_Click" />
                <cc1:ucButton ID="btnBack" runat="server" Text="Back" class="button" OnClick="btnBack_Click" />
            </div>
            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td align="center">
                        <asp:UpdateProgress runat="server" ID="PageUpdateProgress" AssociatedUpdatePanelID="up1">
                            <ProgressTemplate>
                                <div align="center" style="background-color: #000; top: 0px; left: 0px; bottom: 0px;
                                    right: 0px; padding-top: 20%; margin: 0; width: 100%; height: 120%; overflow: hidden;
                                    position: absolute; z-index: 1000; filter: alpha(opacity=50); opacity: 0.5;">
                                    <asp:Image ID="imgWait" runat="server" ImageUrl="~/Images/Ajaxloading.gif" ImageAlign="Middle" />
                                </div>
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnViewWF" />
            <asp:AsyncPostBackTrigger ControlID="btnLog" />
            <asp:AsyncPostBackTrigger ControlID="btnSave" />
            <asp:AsyncPostBackTrigger ControlID="btnBack" />
            <asp:AsyncPostBackTrigger ControlID="btnBack_1" />
            <asp:AsyncPostBackTrigger ControlID="btnAddComment" />
            <asp:PostBackTrigger ControlID="btnBack_AP" />
        </Triggers>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="updpnlAddComment" runat="server">
        <ContentTemplate>
            <asp:Button ID="btnAddCommentMPE" runat="Server" Style="display: none" />
            <ajaxToolkit:ModalPopupExtender ID="mdlAddComment" runat="server" TargetControlID="btnAddCommentMPE"
                PopupControlID="pnlAddComment" BackgroundCssClass="modalBackground" BehaviorID="AddComment"
                DropShadow="false" />
            <%--<asp:Panel ID="pnlAddComment" runat="server" Style="display: none; width:50%" >--%>
            <asp:Panel ID="pnlAddComment" runat="server" Style="display: none; width: 630px">
                <div style="overflow-y: hidden; overflow-x: hidden; background-color: #fff; padding: 5px;
                    border: 2px solid #ccc; text-align: left;">
                    <table cellspacing="5" cellpadding="0" border="0" align="center" class="top-settingsNoBorder-popup">
                        <tr>
                            <td style="font-weight: bold; width: 15%;">
                                <cc1:ucLabel ID="lblUser" Text="User" runat="server"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold; width: 1%;">
                                :
                            </td>
                            <td style="width: 30%;">
                                <cc1:ucLabel ID="lblUserT" runat="server"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold; width: 10%;">
                                <cc1:ucLabel ID="lblDate" Text="Date" runat="server"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold; width: 1%;">
                                :
                            </td>
                            <td style="width: 30%;">
                                <cc1:ucLabel ID="lblDateT" runat="server"></cc1:ucLabel>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold; width: 15%;">
                                <cc1:ucLabel ID="lblDiscrepancyNo" Text="Discrepancy #" runat="server"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold; width: 1%;">
                                :
                            </td>
                            <td style="width: 80%;" colspan="4">
                                <cc1:ucLabel ID="lblDiscrepancyNoT" runat="server"></cc1:ucLabel>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold; width: 100%;" colspan="6">
                                <cc1:ucLabel ID="lblCommentLabel" Text="Comment" runat="server"></cc1:ucLabel>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold; width: 100%;" colspan="6">
                                <cc1:ucTextarea ID="txtUserComments" Height="150px" Width="600px" runat="server"></cc1:ucTextarea>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" style="width: 80%;" colspan="4">
                                <cc1:ucLabel ID="lblErrorMsg" Font-Bold="true" ForeColor="Red" runat="server"></cc1:ucLabel>
                            </td>
                            <td align="right" style="width: 20%;" colspan="2">
                                <cc1:ucButton ID="btnSave_1" runat="server" Text="Save" CssClass="button" OnCommand="btnSave_1_Click"
                                    UseSubmitBehavior="false" OnClientClick="if (!Page_ClientValidate()){ return false; } this.disabled = true; this.value = 'Saving...';" />
                                &nbsp;
                                <cc1:ucButton ID="btnBack_2" runat="server" Text="Back" CssClass="button" OnCommand="btnBack_2_Click" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnSave_1" />
            <asp:AsyncPostBackTrigger ControlID="btnBack_2" />
            <asp:AsyncPostBackTrigger ControlID="btnAddComment" />
        </Triggers>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="updAlternateEmail" runat="server">
        <ContentTemplate>
            <asp:Button ID="btnAlternateEmailMPE" runat="Server" Style="display: none" />
            <ajaxToolkit:ModalPopupExtender ID="mdlAlternateEmail" runat="server" TargetControlID="btnAlternateEmailMPE"
                PopupControlID="pnlAlternateEmail" BackgroundCssClass="modalBackground" BehaviorID="btnSave"
                DropShadow="false" />
            <asp:Panel ID="pnlAlternateEmail" runat="server" Style="display: none; width: 450px">
                <div style="overflow-y: hidden; overflow-x: hidden; background-color: #fff; padding: 5px;
                    border: 2px solid #ccc; text-align: left;">
                    <table cellspacing="5" cellpadding="0" border="0" align="center" class="top-settingsNoBorder-popup">
                        <tr>
                            <td style="font-weight: bold; width: 100%;" colspan="3" align="left">
                                <cc1:ucLabel ID="lblDiscAlternateEmailMsg" Text="No vendor's email available for communication, please enter alternative email address :"
                                    runat="server"></cc1:ucLabel>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold; width: 100%;" colspan="3" align="left">
                                <br />
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 100%;" align="left" valign="top">
                                <table>
                                    <tr>
                                        <td>
                                            <b>
                                                <cc1:ucTextbox ID="txtAlternateEmailText" Width="420px" runat="server"></cc1:ucTextbox></b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:RegularExpressionValidator ID="revValidEmail" ControlToValidate="txtAlternateEmailText"
                                                runat="server" ValidationGroup="AltEmail" ErrorMessage="Please enter valid email address"
                                                ValidationExpression="^[\w-\.\']{1,}\@([\da-zA-Z-]{1,}\.){1,3}[\da-zA-Z-]{2,6}$" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:RequiredFieldValidator ID="rfvEmailAddressCanNotBeEmpty" Enabled="false" ControlToValidate="txtAlternateEmailText"
                                                runat="server" ValidationGroup="AltEmail" ErrorMessage="Please enter valid email address" />
                                        </td>
                                    </tr>
                                </table>
                                <%--<cc1:ucButton ID="btnAddMoreEmail" runat="server" Text="Add More Email"
                                        CssClass="button" OnCommand="btnAddEmailId_Click" ValidationGroup="AltEmail" /> --%>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 100%;">
                                <b>
                                    <cc1:ucLabel ID="lblMultyAlternateEmail" Width="420px" Visible="false" runat="server"></cc1:ucLabel></b>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" align="right" style="width: 100%;">
                                <div style="margin-right: 5px;">
                                    <cc1:ucButton ID="btnSetAlternateEmail" runat="server" Text="Set Alternate Email"
                                        CssClass="button" OnCommand="btnSetAlternateEmail_Click" ValidationGroup="AltEmail" />
                                    <%--<cc1:ucButton ID="btnClear" runat="server" Text="Clear"
                                        CssClass="button" OnCommand="btnClear_Click" ValidationGroup="AltEmail" />--%>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnSave" />
            <asp:AsyncPostBackTrigger ControlID="btnSetAlternateEmail" />
        </Triggers>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="updAPAction" runat="server">
        <ContentTemplate>
            <asp:Button ID="btnAccountsPayableActionMPE" runat="Server" Style="display: none" />
            <ajaxToolkit:ModalPopupExtender ID="mdlAPAction" runat="server" TargetControlID="btnAccountsPayableActionMPE"
                PopupControlID="pnlAPAction" BackgroundCssClass="modalBackground" BehaviorID="ShowAPAction"
                DropShadow="false" />
            <asp:Panel ID="pnlAPAction" runat="server" Style="display: none; width: 630px">
                <div style="overflow-y: hidden; overflow-x: hidden; background-color: #fff; padding: 5px;
                    border: 2px solid #ccc; text-align: left;">
                    <table cellspacing="5" cellpadding="0" align="center" width="100%" class="top-settingsNoBorder-popup">
                        <tr>
                            <td>
                                <uc2:APAction ID="ucApAction" runat="server" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    <%--POPUP ERROR MESSAGE FOR RAISE QUERY--%>
    <!-- Error Message Section -->
    <asp:UpdatePanel ID="updpnlShowError" runat="server">
        <ContentTemplate>
            <asp:Button ID="btnShowErrorMPE" runat="Server" Style="display: none" />
            <ajaxToolkit:ModalPopupExtender ID="mdlShowError" runat="server" TargetControlID="btnShowErrorMPE"
                PopupControlID="pnlShowError" BackgroundCssClass="modalBackground" BehaviorID="ShowError"
                CancelControlID="btnOk" DropShadow="false" />
            <asp:Panel ID="pnlShowError" runat="server" Style="display: none; width: 630px">
                <div style="overflow-y: hidden; overflow-x: hidden; background-color: #fff; padding: 5px;
                    border: 2px solid #ccc; text-align: left;">
                    <table cellspacing="5" cellpadding="0" border="0" align="center" width="100%" class="top-settingsNoBorder-popup">
                        <tr>
                            <td style="font-weight: bold; text-align: center; width: 100%;">
                                <cc1:ucLabel ID="lblError" Font-Bold="true" Font-Size="Medium" ForeColor="Black"
                                    Text="Error" runat="server"></cc1:ucLabel>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: center; width: 100%;">
                                <cc1:ucLabel ID="lblMessageFirst" Text="" ForeColor="Black" runat="server"></cc1:ucLabel>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: center; width: 100%;">
                                <cc1:ucLabel ID="lblMessageSecond" Text="" ForeColor="Black" runat="server"></cc1:ucLabel>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: center; width: 100%;">
                                <cc1:ucButton ID="btnOk" Text="OK" CssClass="button" Width="80px" runat="server">
                                </cc1:ucButton>
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnShowErrorMPE" />
        </Triggers>
    </asp:UpdatePanel>

     <%--POP MESSAGE FOR RAISE QUERY--%>
        <asp:UpdatePanel ID="updpnlWarning" runat="server">
        <ContentTemplate>
            <asp:Button ID="btnConfirmMsg" runat="Server" Style="display: none" />
            <ajaxToolkit:ModalPopupExtender ID="mdlRaiseQueryConfirmMsg" runat="server" TargetControlID="btnConfirmMsg"
                PopupControlID="pnlConfirmMsg" BackgroundCssClass="modalBackground" BehaviorID="ConfirmMsg"
                DropShadow="false" />
            <asp:Panel ID="pnlConfirmMsg" runat="server" Style="display: none;">
                <div class="popup-maincontainer" style="overflow-y: hidden; overflow-x: hidden;">
                    <h3><cc1:ucLabel ID="lblWarningInfo_3" runat="server"></cc1:ucLabel></h3>
                    <table cellspacing="5" cellpadding="0" border="0" align="center" class="popup-maincontainer" >
                        <tr>
                            <td>
                            <div  class="popup-innercontainer top-setting-Popup">
                                 <div class="row" style="width:580px">
                                     <cc1:ucLabel ID="lblRaiseQueryMsg1"  runat="server"></cc1:ucLabel>
                                     <br />
                                     <br />
                                     <cc1:ucLabel ID="lblRaiseQueryMsg2"  runat="server"></cc1:ucLabel>
                                 </div>
                           </div>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <div class="row">
                                <cc1:ucButton ID="btnRaiseQueryPopupContinue" runat="server" Text="CONTINUE" CssClass="button"
                                    OnCommand="btnRaiseQueryContinue_Click" />
                                &nbsp;
                                <cc1:ucButton ID="btnRaiseQueryPopupBack" runat="server" Text="BACK" CssClass="button"  OnCommand="btnRaiseQueryPopupBack_Click"  />
                            </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnRaiseQueryPopupContinue" />    
             <asp:AsyncPostBackTrigger ControlID="btnRaiseQueryPopupBack" />             
        </Triggers>
    </asp:UpdatePanel>

    <cc1:ucPanel ID="pnlQueryOverview_1" Visible="false" runat="server" CssClass="fieldset-form"
        GroupingText="Query Overview">
        <asp:Repeater ID="rptQueryOverview" runat="server" OnItemDataBound="rptQueryOverview_ItemDataBound">
            <ItemTemplate>
                <table cellspacing="0" cellpadding="0" align="center" class="table-border-color">
                    <tr>
                        <td style="padding-top: 2px;">
                            <table cellspacing="5" cellpadding="0" align="center" style="width: 100%; border-bottom: none;">
                                <tr>
                                    <td colspan="6">
                                        <asp:HiddenField ID="hdnQueryDiscrepancyID" Value='<%#DataBinder.Eval(Container.DataItem, "QueryDiscrepancyID")%>'
                                            runat="server" />
                                        <asp:HiddenField ID="hdnDiscrepancyLogID" Value='<%#DataBinder.Eval(Container.DataItem, "DiscrepancyLogID")%>'
                                            runat="server" />
                                        <asp:HiddenField ID="hdnIsQueryClosedManually" Value='<%#DataBinder.Eval(Container.DataItem, "IsQueryClosedManually")%>'
                                                runat="server" />
                                        <cc1:ucLabel ID="lblQueryRaisedByVendor" Text="QUERY RAISED BY VENDOR" isRequired="true"
                                            Font-Bold="true" runat="server"></cc1:ucLabel>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="font-weight: bold; width: 10%;">
                                        <cc1:ucLabel ID="lblDate_1" Text="Date" runat="server" isRequired="true"></cc1:ucLabel><b>/</b><cc1:ucLabel
                                            ID="lblActualTime_1" Text="Time" isRequired="true" runat="server"></cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold; width: 1%;">
                                        <cc1:ucLabel ID="lblCollon1" Text=":" runat="server" isRequired="true" Font-Bold="true"></cc1:ucLabel>
                                    </td>
                                    <td style="width: 22%;">
                                        <cc1:ucLabel ID="lblDateT_1" Text='<%#Eval("VendorQueryDate", "{0:dd/MM/yyyy}") + "  " + Eval("VendorQueryDate",@"{0:HH:mm}")%>'
                                            runat="server" ForeColor="#800000"></cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold; width: 10%;">
                                        <cc1:ucLabel ID="lblUser_1" Text="User" isRequired="true" runat="server"></cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold; width: 1%;">
                                        <cc1:ucLabel ID="lblCollon2" Text=":" runat="server" isRequired="true" Font-Bold="true"></cc1:ucLabel>
                                    </td>
                                    <td style="width: 56%;">
                                        <cc1:ucLabel ID="lblUserT_1" Text='<%#DataBinder.Eval(Container.DataItem, "VendorUserName")%>'
                                            runat="server" ForeColor="#800000"></cc1:ucLabel>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="font-weight: bold; width: 100%;" colspan="6">
                                        <cc1:ucLabel ID="lblCommentLabel_1" Text="Comment" isRequired="true" runat="server"></cc1:ucLabel>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 100%;" colspan="6">
                                        <cc1:ucLabel Style="width: 970px; word-wrap: break-word; display: block;" ID="lblCommentLabelT_1"
                                            Text='<%#DataBinder.Eval(Container.DataItem, "VendorComment")%>' runat="server"
                                            ForeColor="#800000"></cc1:ucLabel>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-bottom: 2px;">
                            <table cellspacing="5" cellpadding="0" align="center" style="width: 100%" id="tblGoodsIn"
                                runat="server">
                                <tr>
                                    <td colspan="9">
                                        <cc1:ucLabel ID="lblGoodsInFeedback" Text="GOODS IN FEEDBACK" isRequired="true" Font-Bold="true"
                                            runat="server"></cc1:ucLabel>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="font-weight: bold; width: 10%;">
                                        <cc1:ucLabel ID="lblDate_2" Text="Date" runat="server" isRequired="true"></cc1:ucLabel><b>/</b><cc1:ucLabel
                                            ID="lblActualTime_2" Text="Time" isRequired="true" runat="server"></cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold; width: 1%;">
                                        <cc1:ucLabel ID="lblCollon3" Text=":" runat="server" isRequired="true" Font-Bold="true"></cc1:ucLabel>
                                    </td>
                                    <td style="width: 22%;">
                                        <cc1:ucLabel ID="lblDateT_2" Text='<%#Eval("GoodsInQueryDate", "{0:dd/MM/yyyy}") + "  " + Eval("GoodsInQueryDate",@"{0:HH:mm}")%>'
                                            runat="server" ForeColor="#800000"></cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold; width: 10%;">
                                        <cc1:ucLabel ID="lblUser_2" Text="User" isRequired="true" runat="server"></cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold; width: 1%;">
                                        <cc1:ucLabel ID="lblCollon4" Text=":" runat="server" isRequired="true" Font-Bold="true"></cc1:ucLabel>
                                    </td>
                                    <td style="width: 22%;">
                                        <cc1:ucLabel ID="lblUserT_2" Text='<%#DataBinder.Eval(Container.DataItem, "GoodsInUserName")%>'
                                            runat="server" ForeColor="#800000"></cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold; width: 10%;">
                                        <cc1:ucLabel ID="lblCommunitcation" Text="Communitcation" isRequired="true" runat="server"></cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold; width: 1%;">
                                        <cc1:ucLabel ID="lblCollon6" Text=":" runat="server" isRequired="true" Font-Bold="true"></cc1:ucLabel>
                                    </td>
                                    <td style="width: 23%">
                                        <a id="lnkView" runat="server" target='_blank' onclick='window.open(this.href, this.target,"scrollbars=1,left=100px,top=20px,width=1000px,height=620px,toolbar=0,scrollbars=0,status=0"); return false;'>
                                            View</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="font-weight: bold;">
                                        <cc1:ucLabel ID="lblAction" Text="Action" runat="server" isRequired="true"></cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold;">
                                        <cc1:ucLabel ID="lblCollon5" Text=":" runat="server" isRequired="true" Font-Bold="true"></cc1:ucLabel>
                                    </td>
                                    <td colspan="7">
                                        <cc1:ucLabel ID="lblActionT" Text='<%#DataBinder.Eval(Container.DataItem, "QueryAction")%>'
                                            runat="server" ForeColor="#800000"></cc1:ucLabel>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="font-weight: bold;" colspan="9">
                                        <cc1:ucLabel ID="lblCommentLabel_2" Text="Comment" isRequired="true" runat="server"></cc1:ucLabel>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="9">
                                        <cc1:ucLabel Style="width: 970px; word-wrap: break-word; display: block;" ID="lblCommentLabelT_2"
                                            Text='<%#DataBinder.Eval(Container.DataItem, "GoodsInComment")%>' runat="server"
                                            ForeColor="#800000"></cc1:ucLabel>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                     <tr>
                            <td style="padding-bottom: 2px;">
                                <table cellspacing="5" cellpadding="0" align="center" style="width: 100%;" visible="false" id="tblQueryClosedManually"
                                    runat="server">
                                    <tr>
                                        <td colspan="9">
                                            <cc1:ucLabel ID="lblQueryClosedManually" Text="Query Closed Manually" isRequired="true" Font-Bold="true"
                                                runat="server"></cc1:ucLabel>
                                        </td>
                                    </tr>
                                
                                </table>
                            </td>
                        </tr>
                </table>
            </ItemTemplate>
        </asp:Repeater>
    </cc1:ucPanel>
    <asp:UpdatePanel ID="UpdatePanel6" runat="server">
        <ContentTemplate>
            <asp:Button ID="btnInvoiceQDisc" runat="Server" Style="display: none" />
            <ajaxToolkit:ModalPopupExtender ID="mdlInvoiceQDisc" runat="server" TargetControlID="btnInvoiceQDisc"
                PopupControlID="pnlInvoiceQDisc" BackgroundCssClass="modalBackground" BehaviorID="InvoiceQDisc"
                DropShadow="false" />
            <asp:Panel ID="pnlInvoiceQDisc" runat="server" Style="display: none;">
                <div class="popup-maincontainer" style="overflow-y: scroll;  overflow-x: scroll; width: 800px; height:300px" >
                    <h3>
                        <cc1:ucLabel ID="lblPOWarning" runat="server"></cc1:ucLabel></h3>
                    <table cellspacing="5" cellpadding="0" border="0" align="center" class="popup-maincontainer" style="min-height:240px;">
                        <tr>
                            <td>
                                <div class="popup-innercontainer top-setting-Popup">
                                    <div class="row1">
                                        <cc1:ucLabel ID="lblInvoiceDiscrepancyMsg" runat="server"></cc1:ucLabel></div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold;" colspan="2">
                                <cc1:ucGridView ID="gvDisLog" Width="100%" runat="server" CssClass="grid" OnRowDataBound="gvDisLog_RowDataBound"
                                    OnSorting="SortGrid" AllowSorting="true" Style="overflow: auto;">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Site">
                                            <HeaderStyle HorizontalAlign="Left" Width="100px" />
                                            <ItemStyle HorizontalAlign="Left" />
                                            <ItemTemplate>
                                                <cc1:ucLiteral ID="ltSiteName" runat="server" Text='<%#Eval("SiteName") %>'></cc1:ucLiteral><asp:HiddenField
                                                    ID="hdnDisType" runat="server" Value='<%#Eval("DiscrepancyTypeID") %>' />
                                                <asp:HiddenField ID="hdnDisLogID" runat="server" Value='<%#Eval("DiscrepancyLogID") %>' />
                                                <asp:HiddenField ID="hdnUserID" runat="server" Value='<%#Eval("UserID") %>' />                                              
                                                <asp:HiddenField ID="hdnRoleTypeFlag" runat="server" Value='<%#Eval("Status") %>' />
                                                <asp:HiddenField ID="hdnWorkFlowID" runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="VDR Number">
                                            <HeaderStyle Width="80px" HorizontalAlign="Left" />
                                            <ItemStyle HorizontalAlign="Left" />
                                            <ItemTemplate>
                                                <asp:HyperLink ID="hypLinkToDisDetails" runat="server" Text='<%#Eval("VDRNo") %>' Target="_blank" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField HeaderText="VDR Discription" DataField="ProductDescription">
                                            <HeaderStyle HorizontalAlign="Left" Width="100px" />
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:TemplateField HeaderText="CreateDate">
                                            <HeaderStyle HorizontalAlign="Left" Width="100px" />
                                            <ItemStyle HorizontalAlign="Left" />
                                            <ItemTemplate>
                                                <cc1:ucLiteral ID="ltDiscrepancyLogDate" runat="server" Text='<%#Eval("DiscrepancyLogDate", "{0:dd/MM/yyyy}") %>'></cc1:ucLiteral></ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField HeaderText="Status" DataField="Status" AccessibleHeaderText="false">
                                            <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:BoundField>
                                    </Columns>
                                </cc1:ucGridView>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <cc1:ucButton ID="btnCancel" runat="server" onclick="btnCancel_Click1" />
                                <cc1:ucButton ID="btnContinue" runat="server" onclick="btnContinue_Click" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnContinue" />
            <asp:PostBackTrigger ControlID="btnCancel" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>

﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using BaseControlLibrary;
using BusinessEntities.ModuleBE.AdminFunctions;
using BusinessEntities.ModuleBE.Discrepancy.LogDiscrepancy;
using BusinessLogicLayer.ModuleBAL.AdminFunctions;
using BusinessLogicLayer.ModuleBAL.Discrepancy.LogDiscrepancy;
using Utilities;
using WebUtilities;

public partial class DISLog_Shuttle : CommonPage
{
    #region Global Variables
    DataSet dsDummy = null; //dummy dataset use for repeater
    DataTable tbDummy = null; //dummy table for dataset
    static BaseControlLibrary.ucTextbox currentTextBox;
    protected string OversValidation = WebCommon.getGlobalResourceValue("OversValidation");
    protected string DeliveryNoteValidation = WebCommon.getGlobalResourceValue("DeliveryNoteValidation");
    protected string DeliveryQtyValidation = WebCommon.getGlobalResourceValue("DeliveryQtyValidation");
    protected string AtLeastOne = WebCommon.getGlobalResourceValue("AtLeastOne");
    protected string InvalidValuesEntered = WebCommon.getGlobalResourceValue("InvalidValuesEntered");
    //static int DiscrepancyLogID = 0;
    //static int SiteID = 0;

    public int iVendorID
    {
        get
        {
            return ViewState["iVendorID"] != null ? Convert.ToInt32(ViewState["iVendorID"].ToString()) : 0;
        }
        set
        {
            ViewState["iVendorID"] = value;
        }
    }

    public int DiscrepancyLogID
    {
        get
        {
            return ViewState["DiscrepancyLogID"] != null ? Convert.ToInt32(ViewState["DiscrepancyLogID"].ToString()) : 0;
        }
        set
        {
            ViewState["DiscrepancyLogID"] = value;
        }
    }

    public int SiteID
    {
        get
        {
            return ViewState["SiteID"] != null ? Convert.ToInt32(ViewState["SiteID"].ToString()) : 0;
        }
        set
        {
            ViewState["SiteID"] = value;
        }
    }

    static string VDRNo = string.Empty;
    System.Text.StringBuilder sSendMailLink = new System.Text.StringBuilder();
    protected string ValidEmail = WebCommon.getGlobalResourceValue("ValidEmail");
    protected string NoEmailSpecifiedMessage = WebCommon.getGlobalResourceValue("NoEmailSpecifiedMessage");
    protected string DiscrepancyAlreadyCreated = WebCommon.getGlobalResourceValue("DiscrepancyAlreadyCreated");
    string ShuttleINVDesc = WebCommon.getGlobalResourceValue("ShuttleINVDesc");
    string InitialCommunicationSent = WebCommon.getGlobalResourceValue("InitialCommunicationSent");
    string FollowingRequestMessage = WebCommon.getGlobalResourceValue("FollowingRequestMessage");
    string NumericDiscrepancyQty = WebCommon.getGlobalResourceValue("NumericDiscrepancyQty");
    string DiscrepancyQtyCantBeZero = WebCommon.getGlobalResourceValue("DiscrepancyQtyCantBeZero");
    string NothingEnteredForShuttleDiscrepancy = WebCommon.getGlobalResourceValue("NothingEnteredForShuttleDiscrepancy");
    string RequiredDiscQtyForType = WebCommon.getGlobalResourceValue("RequiredDiscQtyForType");
    string RequiredTypeForDiscQty = WebCommon.getGlobalResourceValue("RequiredTypeForDiscQty");
    string ShuttleVenDesc = WebCommon.getGlobalResourceValue("ShuttleVenDesc");
    string EnterDiscrepancyQuantity = WebCommon.getGlobalResourceValue("EnterDiscrepancyQuantity");
    protected string ProductDescription = WebCommon.getGlobalResourceValue("ProductDescription");
    protected string AlreadyExistLineNumber = WebCommon.getGlobalResourceValue("AlreadyExistLineNumber");
    protected string LineNoReq = WebCommon.getGlobalResourceValue("LineNoReq");
    protected string LineInformation = WebCommon.getGlobalResourceValue("LineInformation");
    /*---------------SHOW ERROR MESSAGE WHEN QUERY IS ALREADY OPEN--------------*/
    string strThereIsCurrentlyAlreadyAnOpenQueryBeingInvestigated = WebCommon.getGlobalResourceValue("ThereIsCurrentlyAlreadyAnOpenQueryBeingInvestigated");
    string strPleaseWaitUntilYouHaveReceivedAReplyForThisBefore = WebCommon.getGlobalResourceValue("PleaseWaitUntilYouHaveReceivedAReplyForThisBefore");
            
    #endregion

    #region Events

    protected void Page_InIt(object sender, EventArgs e)
    {
        ucSite.CurrentPage = this;
        ucSentFromSite.CurrentPage = this;
        //ucSentFromSite.IsAllRequired = true;
        ucSentFromSite.IsAllDefault = true;
        ucSentFromSite.IsAllowAllSiteWithoutUserId = true;
        ucApAction.CurrentPage = this;
    }

    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);
        if (!IsPostBack)
        {
            if (GetQueryStringValue("VDRNo") != null && !string.IsNullOrWhiteSpace(GetQueryStringValue("VDRNo").ToString()))
            {
               
                if (GetQueryStringValue("PN") != null && !string.IsNullOrWhiteSpace(GetQueryStringValue("PN")))
                {
                    if (GetQueryStringValue("PN").Trim().ToUpper().Equals("POINQ") || GetQueryStringValue("PN").Trim().ToUpper().Equals("OPENPL"))
                        btnBack_1.Visible = true;
                }
                else if (GetQueryStringValue("PreviousPage") != null)
                {
                    string iframeStyleStatus = ifUserControl.Style["display"];
                    if (GetQueryStringValue("PreviousPage").Trim().ToUpper().Equals("SearchResultVendor".ToUpper()) &&
                        iframeStyleStatus != null)
                        btnBack_1.Visible = true;
                    else if (GetQueryStringValue("PreviousPage").Trim().Equals("SearchResult") &&
                         iframeStyleStatus != null)
                    {
                        btnBack_1.Visible = true;
                    }
                    else
                    {
                        btnBack_1.Visible = false;
                        btnUpdateLocation.Visible = false;
                    }
                }
            }
            else
            {
                UIUtility.PageValidateForODUser();
            }

        }
        if (GetQueryStringValue("VDRNo") != null && !string.IsNullOrWhiteSpace(GetQueryStringValue("VDRNo").ToString()) && !lblShuttleDiscrepancy.Text.ToString().Contains("-"))
        {
            lblShuttleDiscrepancy.Text = lblShuttleDiscrepancy.Text + " - " + GetQueryStringValue("VDRNo").ToString();
        }
    }

    protected void btnBack_1_Click(object sender, EventArgs e)
    {
        if (GetQueryStringValue("PreviousPage") != null)
        {
            if (GetQueryStringValue("PreviousPage").Trim().ToUpper().Equals("SearchResultVendor".ToUpper()))
            {
                btnBack_1.Visible = false;
                EncryptQueryString("~/ModuleUI/Discrepancy/DIS_SearchResultVendor.aspx?disLogID=" + Convert.ToString(GetQueryStringValue("disLogID"))
                            + "&VDRNo=" + Convert.ToString(GetQueryStringValue("VDRNo"))
                            + "&UserID=" + Convert.ToString(GetQueryStringValue("UserID"))
                            + "&PreviousPage=SearchResultVendor&WorkFlowID=" + Convert.ToString(GetQueryStringValue("WorkFlowID").ToString()));
            }

            if (Session["PageMode"].ToString() == "Todays")
            {
                EncryptQueryString("../DIS_SearchResult.aspx?FromPage=images&SiteId=" + GetQueryStringValue("SiteId") + "&DisDate=" + GetQueryStringValue("DisDate"));
            }
            else if (Session["PageMode"].ToString() == "Search")
            {
                //For opening search discrepancies mode
                btnBack_1.Visible = false;
                EncryptQueryString("~/ModuleUI/Discrepancy/DIS_SearchResult.aspx?disLogID=" + Convert.ToString(GetQueryStringValue("disLogID"))
                            + "&VDRNo=" + Convert.ToString(GetQueryStringValue("VDRNo"))
                            + "&UserID=" + Convert.ToString(GetQueryStringValue("UserID"))
                            + "&PreviousPage=SearchResult&WorkFlowID=" + Convert.ToString(GetQueryStringValue("WorkFlowID").ToString()));

            }
        }
        else if (GetQueryStringValue("PN") != null)
        {
            if (GetQueryStringValue("PN").Trim().ToUpper().Equals("POINQ"))
            {
                btnBack_1.Visible = false;
                EncryptQueryString("~/ModuleUI/Appointment/PurchaseOrderInquiry.aspx?SId=" + GetQueryStringValue("SId").ToString()
                    + "&Status=" + GetQueryStringValue("Status").ToString()
                    + "&PN=DISLOG"
                    + "&PO=" + GetQueryStringValue("PO").ToString());
            }
            else if (GetQueryStringValue("PN").Trim().ToUpper().Equals("OPENPL"))
            {
                EncryptQueryString("~/ModuleUI/StockOverview/Expedite/OpenPurchaseListing.aspx?action=showlisting");
            }
        }
        else if (GetQueryStringValue("PageType") != null)
        {
            EncryptQueryString("~/ModuleUI/Discrepancy/DIS_LocationReport.aspx");
        }
    }
    void ucApAction_SaveEventValidation(object sender, EventArgs e)
    {
        if (ucApAction.IsModelOpen)
            mdlAPAction.Show();
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        ucApAction.SaveEventValidation += new EventHandler(ucApAction_SaveEventValidation);
        txtPurchaseOrder.Focus();
        dsDummy = new DataSet();
        tbDummy = new DataTable();

        if (mvOvers.ActiveViewIndex == 1)
        {
            btnLog.Visible = false;
            btnBack.Visible = true;
            btnSave.Visible = true;
        }
        else
        {
            btnLog.Visible = true;
            btnBack.Visible = false;
            btnSave.Visible = false;
        }
        if (!IsPostBack)
        {
            if (GetQueryStringValue("disLogID") != null)
            {
                this.BindDiscrepancyLogComment();
                if (GetQueryStringValue("status").ToLower() == "closed")
                {
                    btnUpdateLocation.Visible = true;
                    //txtLocation.ReadOnly = true;
                }
                else
                {
                    btnUpdateLocation.Visible = true;
                }

                if (GetQueryStringValue("PageType") != null && GetQueryStringValue("PageType") == "Location")
                {
                    btnUpdateLocation.Visible = false;
                    txtLocation.ReadOnly = true;
                    btnBack_1.Visible = true;
                }
                mvOvers.ActiveViewIndex = 1;                
                GetDeliveryDetails();
                GetContactPurchaseOrderDetails();
                VendorsQueryOverview();
                if (Session["Role"] != null)
                {
                    GetAccessRightRaiseQuery();
                    if (Session["Role"].ToString() == "Vendor")
                    {
                        pnlInternalComments.Visible = false;
                        btnAddComment.Visible = true;
                        btnAddImages.Visible = false;
                        btnRaiseQuery.Visible = true;
                        btnAccountsPayableAction.Visible = false;
                    }
                }
                AddUserControl();
                this.ShowACP001ActionButton();
                btnLog.Visible = false;
                btnBack.Visible = false;
                btnSave.Visible = false;
                if (Session["Role"] != null && (Session["Role"].ToString().ToLower() == "vendor" || Session["Role"].ToString().ToLower() == "carrier"))
                {
                    btnViewWF.Visible = false;
                }
                else
                {
                    btnViewWF.Visible = true;
                }
                // code for generating div for the imageviewer
                string DivGallery = CommonPage.GetImagesStringNewLayout(Convert.ToInt32(GetQueryStringValue("disLogID")));
                if (string.IsNullOrEmpty(DivGallery))
                {
                    btnViewImage.Visible = false;
                }
                else
                {
                    discrepancygallery.InnerHtml = DivGallery.ToString();
                    btnViewImage.Visible = true;
                }
                GetVendorEmailIds();
            }
            else
            {
                btnViewWF.Visible = false;
                btnViewImage.Visible = false;
                btnAddComment.Visible = false;
                btnAddImages.Visible = false;
                btnRaiseQuery.Visible = false;
                pnlQueryOverview_1.Visible = false;
                btnAccountsPayableAction.Visible = false;
            }

            txtDateDeliveryArrived.innerControltxtDate.Value = Convert.ToString(DateTime.Now.ToString("dd/MM/yyyy"));
            lblIssueRaisedDate.Text = Convert.ToString(DateTime.Now.ToString("dd/MM/yyyy - HH:MM"));
        }
        txtPurchaseOrder.Attributes.Add("autocomplete", "off");
        txtPurchaseOrder.Focus();

    }

    private void GetVendorEmailIds()
    {
        ucTextbox ucVendorEmailList = (ucTextbox)ucSDRCommunication1.FindControl("ucVendorEmailList");
        if (ucVendorEmailList != null)
            ViewState["ucVendorEmailList"] = ucVendorEmailList.Text;
    }

    public override void SitePost_Load()
    {
        base.SitePost_Load();
        if (!IsPostBack)
        {
            //ucSite.innerControlddlSite.SelectedIndex = 0;
            ucSite.innerControlddlSite.AutoPostBack = true;

        }
    }
    protected void btnAccountsPayableAction_Click(object sender, EventArgs e)
    {

        mdlAPAction.Show();
        DropDownList ddl = ucApAction.FindControl("ddlCurrency") as DropDownList;
        DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
        DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();
        oDiscrepancyBE.Action = "GetCurrencyByVendorID";
        if (GetQueryStringValue("disLogID") != null)
        {
            oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID"));
        }
        ddl.SelectedValue = oDiscrepancyBAL.GetCurrencyBAL(oDiscrepancyBE);      
    }
    private void ShowACP001ActionButton()
    {
        DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
        DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();
        oDiscrepancyBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
        string CaseOf = String.Empty;

        oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID"));

        if (GetQueryStringValue("RoleTypeFlag") == "A" || GetQueryStringValue("RoleTypeFlag") == null)
        {
            if (Session["UserID"] == null || Session["UserID"].ToString() == "" || Session["Role"] == null || Session["Role"].ToString().Trim() == "")
                return;

            oDiscrepancyBE.User.UserID = Convert.ToInt32(Session["UserID"]);
            CaseOf = Session["Role"].ToString().Trim();
        }
        //else
        //{
        //    switch (GetQueryStringValue("RoleTypeFlag").ToString())
        //    {
        //        case "G":
        //            CaseOf = "OD - Goods In";
        //            break;
        //        case "S":
        //            CaseOf = "OD - Stock Planner";
        //            break;
        //        case "P":
        //            CaseOf = "OD - Accounts Payable";
        //            break;
        //        case "V":
        //            CaseOf = "Vendor";
        //            break;
        //    }
        //}

        /* Here P is an "OD - Accounts Payable" When from drop down selected the Account Payble option */
        /* Here A will come when actual Account Payble type user will loggedin. */
        if (GetQueryStringValue("RoleTypeFlag") == "P" || GetQueryStringValue("RoleTypeFlag") == "A")
        {
            oDiscrepancyBE.Action = "CheckForActionRequired";
            oDiscrepancyBE.DiscrepancyWorkFlowID = !string.IsNullOrEmpty(GetQueryStringValue("WorkFlowID")) ? Convert.ToInt32(GetQueryStringValue("WorkFlowID")) : (int?)null;
            DataTable dtActionDetails = oDiscrepancyBAL.GetActionRequiredDetailsForWorkFlowBAL(oDiscrepancyBE);
            if (dtActionDetails != null && dtActionDetails.Rows.Count > 0)
            {
                //if (dtActionDetails.Rows[0]["APUserControl"].ToString().Trim() == "ACP001")
                //if (Convert.ToString(dtActionDetails.Rows[0]["ACP001Control"]).Trim().Equals("ACP001")
                //    && !Convert.ToBoolean(dtActionDetails.Rows[0]["IsACP001Done"]))
                if (!string.IsNullOrEmpty(dtActionDetails.Rows[0]["IsACP001Done"].ToString()) && !Convert.ToBoolean(dtActionDetails.Rows[0]["IsACP001Done"]))
                {
                if (GetQueryStringValue("disLogID") != null)
                    ucApAction.DisLogID = Convert.ToInt32(GetQueryStringValue("disLogID"));

                ucApAction.DisWFlowID = Convert.ToInt32(dtActionDetails.Rows[0]["DiscrepancyWorkflowID"]);
                ucApAction.PONumber = Convert.ToString(dtActionDetails.Rows[0]["PurchaseOrderNumber"]);
                ucApAction.SiteID = SiteID;
                ucApAction.VDRNo = VDRNo;
                ucApAction.FromPage = "overs";
                ucApAction.PreviousPage = GetQueryStringValue("PreviousPage").ToString();

                if (ViewState["VendorID"] != null)
                    ucApAction.VendorID = Convert.ToInt32(ViewState["VendorID"]);

                btnAccountsPayableAction.Visible = true;
                //ifUserControl.Style["display"] = "none";
                }
                else
                {
                    btnAccountsPayableAction.Visible = false;
                }
            }
        }
    }
    public override void SiteSelectedIndexChanged()
    {
        if (txtPurchaseOrder.Text != string.Empty)
        {
            GetPurchaseOrderList();
        }
    }

    protected void btnLog_Click(object sender, EventArgs e)
    {
        // This check is invalid because AShton has index zero and site is already selected.
        //if (ucSentFromSite.innerControlddlSite.SelectedIndex.Equals(0)) 
        //{
        //    string sentFromSiteRequired = WebCommon.getGlobalResourceValue("SentFromSiteRequired");
        //    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('" + sentFromSiteRequired + "')", true);
        //    return;
        //}

        if (ucSentFromSite.innerControlddlSite.SelectedValue.Equals(ucSite.innerControlddlSite.SelectedValue))
        {
            string receivingAndSentFromSiteCantSame = WebCommon.getGlobalResourceValue("ReceivingAndSentFromSiteCantSame");
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('" + receivingAndSentFromSiteCantSame + "')", true);
            return;
        }

        DateTime dt = txtDateDeliveryArrived.GetDate;
        if (dt > DateTime.Now.Date)
        {
            string saveMessage = WebCommon.getGlobalResourceValue("NotDateValidMessage");
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('" + saveMessage + "')", true);
            return;
        }
        mvOvers.ActiveViewIndex = 1;
        btnLog.Visible = false;
        btnBack.Visible = true;
        btnSave.Visible = true;
        btnSave.Enabled = true;
        GetDeliveryDetails();
        GetContactPurchaseOrderDetails();
        ifUserControl.Style["display"] = "none";
        ucSDRCommunication1.innerControlRdoCommunication = 'E';
        GetVendorEmailIds();
        IncreaseRow();
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        var defaultEmailAddress = Convert.ToString(ConfigurationManager.AppSettings["DefaultEmailAddress"]);
        var txtAltEmail = (ucTextbox)ucSDRCommunication1.FindControl("txtAltEmail");
        if (ucSDRCommunication1.VendorEmailList.Trim().Equals(defaultEmailAddress) && (string.IsNullOrEmpty(txtAltEmail.Text) || string.IsNullOrWhiteSpace(txtAltEmail.Text)))
        {
            mdlAlternateEmail.Show();
            rfvEmailAddressCanNotBeEmpty.Enabled = true;
            txtAlternateEmailText.Focus();
        }
        else
        {
            if (Session["Role"].ToString().ToLower() == "vendor")
            {
                string LoginPath = ResolveClientUrl("../../../ModuleUI/Security/Login.aspx");
                Response.Redirect(LoginPath);
            }
            else
            {
                this.Save();
            }
        }    
    }

    private void Save()
    {
        try
        {
            bool bSendMail = false;
            bool blnIsDiscSave = false;
            int iAllEmpty = 0;

            #region Validations ...
            foreach (GridViewRow row in grdPurchaseOrderDetail.Rows)
            {
                DropDownList ddlType = (DropDownList)row.FindControl("ddlType");
                TextBox txtDiscrepancyQty = (TextBox)row.FindControl("txtDiscrepancyQty");
                if (!string.IsNullOrEmpty(txtDiscrepancyQty.Text))
                {
                    
                    if (Convert.ToInt32(txtDiscrepancyQty.Text).Equals(0))
                    {
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + DiscrepancyQtyCantBeZero + "')", true);
                        return;
                    }
                    else if (Convert.ToInt32(txtDiscrepancyQty.Text) > 0 && ddlType.SelectedItem.Text.Equals("Select"))
                    {
                        RequiredTypeForDiscQty = RequiredTypeForDiscQty.Replace("##DiscrepancyQty##", txtDiscrepancyQty.Text);
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + RequiredTypeForDiscQty + "')", true);
                        return;
                    }

                    if (Convert.ToInt32(txtDiscrepancyQty.Text) > 0 && !ddlType.SelectedItem.Text.Equals("Select"))
                        blnIsDiscSave = true;
                }
                else if (!ddlType.SelectedItem.Text.Equals("Select") && string.IsNullOrEmpty(txtDiscrepancyQty.Text))
                {
                    RequiredDiscQtyForType = RequiredDiscQtyForType.Replace("##Type##", ddlType.SelectedItem.Text);
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + RequiredDiscQtyForType + "')", true);
                    return;
                }

                if (txtDiscrepancyQty.Text.Trim() == "")
                {
                    iAllEmpty++;
                    continue;
                }

            }



            //if (blnIsDiscSave.Equals(false))
            //{
            //    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + NothingEnteredForShuttleDiscrepancy + "')", true);
            //    return;
            //}


            //check validations for below grid
            foreach (GridViewRow row in grdPODetailAddLine.Rows)
            {

                DropDownList drpDiscrepancyType = (DropDownList)row.FindControl("ddlType");
                TextBox txtDiscrepancyQty = (TextBox)row.FindControl("txtDiscrepancyQty");
                ucTextbox txtOfficeDepotCode = (ucTextbox)row.FindControl("txtOfficeDepotCode");
                ucTextbox txtDescription = (ucTextbox)row.FindControl("txtDescription");
                ucTextbox txtLineNo = (ucTextbox)row.FindControl("txtLineno");

                if (txtOfficeDepotCode.Text.Trim() != "")
                {
                    if (txtLineNo.Text.Trim() == "")
                    {
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + LineNoReq + "')", true);
                        return;
                    }

                    if (txtDescription.Text.Trim() == "")
                    {
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + ProductDescription + "')", true);
                        return;
                    }

                    if (txtDiscrepancyQty.Text.Trim() == "")
                    {
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + EnterDiscrepancyQuantity + "')", true);
                        return;
                    }


                    if (!drpDiscrepancyType.SelectedItem.Text.Equals("Select") && string.IsNullOrEmpty(txtDiscrepancyQty.Text))
                    {
                        RequiredDiscQtyForType = RequiredDiscQtyForType.Replace("##Type##", drpDiscrepancyType.SelectedItem.Text);
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + RequiredDiscQtyForType + "')", true);
                        return;
                    }


                    if (Convert.ToInt32(txtDiscrepancyQty.Text) > 0 && drpDiscrepancyType.SelectedItem.Text.Equals("Select"))
                    {
                        RequiredTypeForDiscQty = RequiredTypeForDiscQty.Replace("##DiscrepancyQty##", txtDiscrepancyQty.Text);
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + RequiredTypeForDiscQty + "')", true);
                        return;
                    }

                    if (Convert.ToInt32(txtDiscrepancyQty.Text).Equals(0))
                    {
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + DiscrepancyQtyCantBeZero + "')", true);
                        return;
                    }

                }
                else if (!drpDiscrepancyType.SelectedItem.Text.Equals("Select") && string.IsNullOrEmpty(txtDiscrepancyQty.Text))
                {
                    RequiredDiscQtyForType = RequiredDiscQtyForType.Replace("##Type##", drpDiscrepancyType.SelectedItem.Text);
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + RequiredDiscQtyForType + "')", true);
                    return;
                }
                //else
                //{
                //    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + AtLeastOne + "')", true);
                //    return;
                //}
            }

            if (iAllEmpty > 0 && iAllEmpty == grdPurchaseOrderDetail.Rows.Count)
            {
                //Check Add Line grid -- Abhinav
                iAllEmpty = 0;
                int iRowCount = 0;
                foreach (GridViewRow row in grdPODetailAddLine.Rows)
                {

                    iRowCount++;
                    ucTextbox txtDiscrepancyQty = (ucTextbox)row.FindControl("txtDiscrepancyQty");
                    ucTextbox txtOfficeDepotCode = (ucTextbox)row.FindControl("txtOfficeDepotCode");


                    if ( txtDiscrepancyQty.Text.Trim() == "")
                    {
                        iAllEmpty++;
                        continue;
                    }


                }

                if (iAllEmpty > 0 || iRowCount == 0)
                {
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + EnterDiscrepancyQuantity + "')", true);
                    return;
                }
            }




            #endregion

            btnSave.Enabled = false;
              //to handle duplicating discrepancies in edit mode.

            if (ViewState["PurchaseOrderID"] != null && !(string.IsNullOrEmpty(lblPurchaseOrderDateValue.Text.Split('-')[1].ToString())))
            {
                DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
                DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();

                oDiscrepancyBE.Action = "AddDiscrepancyLog";
                oDiscrepancyBE.SiteID = Convert.ToInt32(ucSite.innerControlddlSite.SelectedValue);
                // ucSentFromSite is being saved in POSiteId column in database..
                oDiscrepancyBE.POSiteID = Convert.ToInt32(ucSentFromSite.innerControlddlSite.SelectedValue);
                oDiscrepancyBE.DiscrepancyTypeID = Convert.ToInt32(Common.DiscrepancyType.ShuttleDiscrepancy);
                oDiscrepancyBE.DiscrepancyLogDate = DateTime.Now;
                if (ViewState["PurchaseOrderID"] != null)
                    oDiscrepancyBE.PurchaseOrderID = Convert.ToInt32(ViewState["PurchaseOrderID"].ToString());
                oDiscrepancyBE.PurchaseOrderDate = Common.GetMM_DD_YYYY(lblPurchaseOrderDateValue.Text.Split('-')[1].ToString());
                oDiscrepancyBE.PurchaseOrderNumber = txtPurchaseOrder.Text.Trim();
                oDiscrepancyBE.InternalComments = txtInternalComments.Text;
                oDiscrepancyBE.DeliveryNoteNumber = txtDeliveryNumber.Text.Trim();
                oDiscrepancyBE.DeliveryArrivedDate = Common.GetMM_DD_YYYY(txtDateDeliveryArrived.innerControltxtDate.Value);// DateTime.Now;
                oDiscrepancyBE.CommunicationType = ucSDRCommunication1.innerControlRdoCommunication;
                oDiscrepancyBE.CommunicationTo = ucSDRCommunication1.VendorEmailList;
                if (ViewState["VendorID"] != null)
                    oDiscrepancyBE.VendorID = Convert.ToInt32(ViewState["VendorID"].ToString());

                //if (ViewState["StockPlannerID"] != null)
                //    oDiscrepancyBE.StockPlannerID = Convert.ToInt32(ViewState["StockPlannerID"].ToString());

                if (ViewState["SPToCoverStockPlannerID"] != null)
                {
                    oDiscrepancyBE.StockPlannerID = Convert.ToInt32(ViewState["SPToCoverStockPlannerID"].ToString()); // case when actual Stock Planner is absent and SP to Cover is there
                    oDiscrepancyBE.StockPlannerIDIfNotAbsent = ViewState["StockPlannerID"] != null ? Convert.ToInt32(ViewState["StockPlannerID"].ToString()) : (int?)null; // Actual Stock Palnner Id that should have been saved if not absent
                }
                else
                {
                    if (ViewState["StockPlannerID"] != null)
                    {
                        oDiscrepancyBE.StockPlannerID = Convert.ToInt32(ViewState["StockPlannerID"].ToString());
                        oDiscrepancyBE.StockPlannerIDIfNotAbsent = null;
                    }
                }

                oDiscrepancyBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
                oDiscrepancyBE.User.Site = new BusinessEntities.ModuleBE.AdminFunctions.MAS_SiteBE();
                oDiscrepancyBE.User.UserID = Convert.ToInt32(Session["UserID"]);
                oDiscrepancyBE.PickersName = lblPickerNameValue.Text;
                oDiscrepancyBE.Location = txtLocation.Text.Trim();
                string sResult = oDiscrepancyBAL.addDiscrepancyLogBAL(oDiscrepancyBE);
                oDiscrepancyBAL = null;

                if (!sResult.Contains("Discrepancy Already Inserted"))
                {
                    int iResult = Convert.ToInt32(sResult.Split('-')[1].ToString());

                    if (iResult > 0)
                    {
                        ViewState["ShuttleTypes"] = null;
                        DiscrepancyLogID = iResult;
                        foreach (GridViewRow gv in grdPurchaseOrderDetail.Rows)
                        {
                            DiscrepancyBE oNewDiscrepancyBE = new DiscrepancyBE();
                            DiscrepancyBAL oNewDiscrepancyBAL = new DiscrepancyBAL();

                            DropDownList ddlType = (DropDownList)gv.FindControl("ddlType");
                            TextBox txtDiscrepancyQty = (TextBox)gv.FindControl("txtDiscrepancyQty");
                            if (!string.IsNullOrEmpty(txtDiscrepancyQty.Text))
                            {
                                if (Convert.ToInt32(txtDiscrepancyQty.Text) > 0 && !ddlType.SelectedItem.Text.Equals("Select"))
                                {
                                    Label lblLineNo = (Label)gv.FindControl("lblLineNo");
                                    Label lblVikingCode = (Label)gv.FindControl("lblVikingCodeValue");
                                    Label lblOfficeDepotCode = (Label)gv.FindControl("lblOfficeDepotCodeValue");
                                    Label lblVendorItemCode = (Label)gv.FindControl("lblVendorItemCodeValue");
                                    Label lblDescription = (Label)gv.FindControl("lblDescriptionValue");
                                    Label lblOutstandingQty = (Label)gv.FindControl("lblOutstandingQty1");
                                    Label lblUoM1 = (Label)gv.FindControl("lblUoM1");
                                    Label lblOvers = (Label)gv.FindControl("lblOvers");
                                    Label lblOriginalPOQty = (Label)gv.FindControl("lblOriginalPOQuantity1");
                                    HiddenField hdnPurchaseOrderId = (HiddenField)gv.FindControl("hdnPurchaseOrderID");

                                    oNewDiscrepancyBE.Action = "AddDiscrepancyItem";
                                    oNewDiscrepancyBE.Line_no = Convert.ToInt32(lblLineNo.Text);
                                    oNewDiscrepancyBE.DirectCode = lblVikingCode.Text;
                                    oNewDiscrepancyBE.ODSKUCode = lblOfficeDepotCode.Text;
                                    oNewDiscrepancyBE.VendorCode = lblVendorItemCode.Text;
                                    oNewDiscrepancyBE.ProductDescription = lblDescription.Text;
                                    oNewDiscrepancyBE.OutstandingQuantity = Convert.ToInt32(lblOutstandingQty.Text);
                                    oNewDiscrepancyBE.UOM = lblUoM1.Text;
                                    oNewDiscrepancyBE.OriginalQuantity = Convert.ToInt32(lblOriginalPOQty.Text);
                                    oNewDiscrepancyBE.PurchaseOrderID = Convert.ToInt32(hdnPurchaseOrderId.Value);
                                    oNewDiscrepancyBE.DiscrepancyLogID = iResult;
                                    oNewDiscrepancyBE.ShuttleType = ddlType.SelectedItem.Text;
                                    if (!string.IsNullOrEmpty(ddlType.SelectedItem.Text))
                                    {
                                        if (ViewState["ShuttleTypes"] != null)
                                            ViewState["ShuttleTypes"] = string.Format("{0}, {1}", ViewState["ShuttleTypes"], ddlType.SelectedItem.Text);
                                        else
                                            ViewState["ShuttleTypes"] = ddlType.SelectedItem.Text;
                                    }
                                    oNewDiscrepancyBE.DiscrepancyQty = Convert.ToInt32(txtDiscrepancyQty.Text);
                                    int? iResult2 = oNewDiscrepancyBAL.addDiscrepancyItemBAL(oNewDiscrepancyBE);
                                    //bSendMail = true;
                                }
                            }
                        }
                        bSendMail = true;
                        //adding items in trn_discrepancyitems table from below grid
                        foreach (GridViewRow item in grdPODetailAddLine.Rows)
                        {

                            ucTextbox txtLineNo = (ucTextbox)item.FindControl("txtLineNo");
                            ucTextbox txtVikingCode = (ucTextbox)item.FindControl("txtVikingCode");
                            ucTextbox txtVendorItemCode = (ucTextbox)item.FindControl("txtVendorItemCode");
                            ucTextbox txtOfficeDepotCode = (ucTextbox)item.FindControl("txtOfficeDepotCode");
                            ucTextbox txtDescription = (ucTextbox)item.FindControl("txtDescription");
                            ucTextbox txtUoM = (ucTextbox)item.FindControl("txtUoM");
                            ucTextbox txtOriginalPOQuantity = (ucTextbox)item.FindControl("txtOriginalPOQuantity");
                            ucTextbox txtOutstandingQty = (ucTextbox)item.FindControl("txtOutstandingQty");
                            ucDropdownList drpDisType = (ucDropdownList)item.FindControl("ddlType");
                            ucTextbox txtDiscrepancyQty = (ucTextbox)item.FindControl("txtDiscrepancyQty");
                            HiddenField hdnPurchaseOrderId = (HiddenField)item.FindControl("hdnPurchaseOrderID");
                            if (txtOfficeDepotCode.Text.Trim() != "")
                            {

                                DiscrepancyBE oNewDiscrepancyBE = new DiscrepancyBE();
                                DiscrepancyBAL oNewDiscrepancyBAL = new DiscrepancyBAL();


                                oNewDiscrepancyBE.Action = "AddDiscrepancyItem";
                                oNewDiscrepancyBE.Line_no = txtLineNo.Text.Trim() != "" ? Convert.ToInt32(txtLineNo.Text) : 0;
                                oNewDiscrepancyBE.DirectCode = txtVikingCode.Text;
                                oNewDiscrepancyBE.ODSKUCode = txtOfficeDepotCode.Text;
                                oNewDiscrepancyBE.VendorCode = txtVendorItemCode.Text;
                                oNewDiscrepancyBE.ProductDescription = txtDescription.Text;
                                oNewDiscrepancyBE.OutstandingQuantity = string.IsNullOrEmpty(txtOutstandingQty.Text) ? (int?)null : Convert.ToInt32(txtOutstandingQty.Text);
                                oNewDiscrepancyBE.UOM = txtUoM.Text;
                                oNewDiscrepancyBE.OriginalQuantity = string.IsNullOrEmpty(txtOriginalPOQuantity.Text) ? (int?)null : Convert.ToInt32(txtOriginalPOQuantity.Text);
                                //if (ViewState["PurchaseOrderID"] != null)
                                //    oDiscrepancyBE.PurchaseOrderID = Convert.ToInt32(ViewState["PurchaseOrderID"].ToString());
                                oNewDiscrepancyBE.DiscrepancyLogID = iResult;
                                oNewDiscrepancyBE.ShuttleType = drpDisType.SelectedItem.Text;
                                oNewDiscrepancyBE.DiscrepancyQty = Convert.ToInt32(txtDiscrepancyQty.Text);

                                oNewDiscrepancyBE.DiscrepancyLogID = iResult;
                                int? iResult3 = oNewDiscrepancyBAL.addDiscrepancyItemBAL(oNewDiscrepancyBE);
                                oNewDiscrepancyBAL = null;
                            }
                        }

                        if (bSendMail)
                        {
                            int? iDiscrepancyCommunicationID = sendAndSaveDiscrepancyCommunication(iResult);
                        }

                        if (ucSDRCommunication1.innerControlRdoCommunication == 'L')
                        {
                            sendCommunication communication = new sendCommunication();
                            int vendorId = ViewState["VendorID"] != null ? Convert.ToInt32(ViewState["VendorID"].ToString()) : 0;
                            Session["LabelHTML"] = communication.sendAndSaveLetter(DiscrepancyLogID, "shuttle", vendorId);
                            sLetterLink.Append(communication.sLetterLink);
                        }
                        InsertHTML();
                        EncryptQueryString("DIS_OptionsPopup.aspx?VDRNo=" + sResult.Split('-')[0].ToString()
                            + "&DiscrepancyLogID=" + sResult.Split('-')[1].ToString()
                            + "&CommunicationType=" + ucSDRCommunication1.innerControlRdoCommunication
                            + "&SiteId=" + ucSite.innerControlddlSite.SelectedValue
                            + "&DisDate=" + DateTime.Now.ToString("dd/MM/yyyy"));
                    }

                }
                else if (sResult.Contains("Discrepancy Already Inserted"))
                {
                    //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + DiscrepancyAlreadyCreated + "')", true);
                    EncryptQueryString("DIS_OptionsPopup.aspx?VDRNo=" + sResult.Split('-')[0].ToString()
                        + "&DiscrepancyLogID=" + sResult.Split('-')[1].ToString()
                        + "&CommunicationType=" + ucSDRCommunication1.innerControlRdoCommunication
                        + "&SiteId=" + ucSite.innerControlddlSite.SelectedValue
                        + "&DisDate=" + DateTime.Now.ToString("dd/MM/yyyy"));

                }
            }
            else
            {
                EncryptQueryString("~/ModuleUI/Discrepancy/DIS_SearchResult.aspx?disLogID=" + Convert.ToString(GetQueryStringValue("disLogID"))
                                + "&VDRNo=" + Convert.ToString(GetQueryStringValue("VDRNo"))
                                + "&UserID=" + Convert.ToString(GetQueryStringValue("UserID"))
                                + "&PreviousPage=SearchResult&WorkFlowID=" + Convert.ToString(GetQueryStringValue("WorkFlowID").ToString()));
            }
        }

        catch (Exception ex)
        {
            string str = ex.Message;
            LogUtility.SaveErrorLogEntry(ex);
        }
    }

    private int? sendAndSaveDiscrepancyCommunication(int iDiscrepancy)
    {
        int? vendorID = ViewState["VendorID"] == null ? 0 : Convert.ToInt32(ViewState["VendorID"].ToString());
        if (((RadioButton)ucSDRCommunication1.FindControl("rdoEmailComm")) != null && ((RadioButton)ucSDRCommunication1.FindControl("rdoEmailComm")).Checked)
        {
            if (((BaseControlLibrary.ucTextbox)ucSDRCommunication1.FindControl("txtAltEmail")) != null && ((BaseControlLibrary.ucTextbox)ucSDRCommunication1.FindControl("ucVendorEmailList")) != null)
            {
                sendCommunication communication = new sendCommunication();
                int? retVal = communication.sendCommunicationByEMail(iDiscrepancy, "shuttle", ((BaseControlLibrary.ucTextbox)ucSDRCommunication1.FindControl("txtAltEmail")), ((BaseControlLibrary.ucTextbox)ucSDRCommunication1.FindControl("ucVendorEmailList")), vendorID);
                sSendMailLink = communication.sSendMailLink;
                return retVal;
            }
        }
        return null;
    }

    protected void btnViewWF_Click(object sender, EventArgs e)
    {
        if (GetQueryStringValue("PN") != null)
        {
            if (GetQueryStringValue("PN").Trim().ToUpper().Equals("POINQ"))
            {
                string url = string.Format("DISLog_Workflow.aspx?disLogID={0}&PN={1}&SId={2}&Status={3}&PN={4}&PO={5}", GetQueryStringValue("disLogID").ToString(), "POINQ", GetQueryStringValue("SId").ToString(), GetQueryStringValue("Status").ToString(), "DISLOG", GetQueryStringValue("PO").ToString());
                EncryptQueryString(url);
            }

        }
        if (GetQueryStringValue("disLogID") != null)
        {
            string url = string.Format("DISLog_Workflow.aspx?disLogID={0}&VDRNo={1}&TodayDis={2}&SiteID={3}&DisDate={4}&UserID={5}", GetQueryStringValue("disLogID").ToString(), GetQueryStringValue("VDRNo").ToString(), GetQueryStringValue("TodayDis"), GetQueryStringValue("SiteId"), GetQueryStringValue("DisDate"), GetQueryStringValue("UserID"));
            EncryptQueryString(url);
        }
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        mvOvers.ActiveViewIndex = 0;
        btnLog.Visible = true;
        btnBack.Visible = false;
        btnSave.Visible = false;
    }

    protected void ddlPurchaseOrderDate_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ViewState["GetPODateList"] != null)
        {
            List<DiscrepancyBE> lstGetPODateList = (List<DiscrepancyBE>)ViewState["GetPODateList"];
            if (!string.IsNullOrEmpty(ddlPurchaseOrderDate.SelectedItem.Value))
            {
                var PODateDetail = lstGetPODateList.Find(p => p.RowNumber == Convert.ToInt32(ddlPurchaseOrderDate.SelectedItem.Value));
                if (PODateDetail != null)
                {
                    lblVendorValue.Text = PODateDetail.VendorNoName;

                    if (PODateDetail.VendorID > 0)
                        ucSDRCommunication1.VendorID = Convert.ToInt32(PODateDetail.VendorID);
                    else
                        ucSDRCommunication1.VendorID = 0;

                    if (!string.IsNullOrEmpty(ucSite.innerControlddlSite.SelectedValue))
                        ucSDRCommunication1.SiteID = Convert.ToInt32(ucSite.innerControlddlSite.SelectedValue);
                    else
                        ucSDRCommunication1.SiteID = 0;
                    ucSDRCommunication1.GetDetails();
                }
            }
        }

        //lblVendorValue.Text = ddlPurchaseOrderDate.SelectedItem.Value.ToString();
    }

    protected void txtPurchaseOrder_TextChanged(object sender, EventArgs e)
    {
        GetPurchaseOrderList();
        txtDeliveryNumber.Focus();
    }

    protected void grdPurchaseOrderDetail_RowDataBound(Object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Label lblOutstandingQty = (Label)e.Row.FindControl("lblOutstandingQty1");
            TextBox txtDeliveryNoteQty = (TextBox)e.Row.FindControl("txtDeliveryNoteQty");
            TextBox txtDeliveryQty = (TextBox)e.Row.FindControl("txtDeliveryQty");
            Label lblOvers = (Label)e.Row.FindControl("lblOvers");
            HiddenField hdnOvers = (HiddenField)e.Row.FindControl("hdnOvers");

            txtDeliveryNoteQty.Attributes.Add("onchange", "SetOvers('" + lblOutstandingQty.ClientID + "','" + txtDeliveryNoteQty.ClientID + "','" + txtDeliveryQty.ClientID + "','" + lblOvers.ClientID + "','" + hdnOvers.ClientID + "')");
            txtDeliveryQty.Attributes.Add("onchange", "SetOvers('" + lblOutstandingQty.ClientID + "','" + txtDeliveryNoteQty.ClientID + "','" + txtDeliveryQty.ClientID + "','" + lblOvers.ClientID + "','" + hdnOvers.ClientID + "')");
        }
    }

    public static string InvalidValuesEnteredLocal = WebCommon.getGlobalResourceValue("InvalidValuesEntered");

    [WebMethod]
    public static string CalculateOvers(string OutstandingPOQuantity, string DeliveryNoteQuantity, string DeliveredQuantity)
    {

        if (DeliveredQuantity.Trim().Length > 0 && DeliveryNoteQuantity.Trim().Length > 0)
        {
            int iDeliveryQty, iDeliveryNoteQty;
            bool isiDeliveryQtyDecimal = int.TryParse(DeliveredQuantity.Trim(), out iDeliveryQty);
            bool isDeliveryNoteQtyDecimal = int.TryParse(DeliveryNoteQuantity.Trim(), out iDeliveryNoteQty);

            if (DeliveredQuantity.Trim() != "" && !isiDeliveryQtyDecimal)
            {
                return InvalidValuesEnteredLocal;
            }

            if (DeliveryNoteQuantity.Trim() != "" && !isDeliveryNoteQtyDecimal)
            {
                return InvalidValuesEnteredLocal;
            }

            if (DeliveredQuantity.Trim() != "" && isiDeliveryQtyDecimal && !DeliveredQuantity.IsNumeric())
            {
                return InvalidValuesEnteredLocal;
            }

            if (DeliveryNoteQuantity.Trim() != "" && isDeliveryNoteQtyDecimal && !DeliveryNoteQuantity.IsNumeric())
            {
                return InvalidValuesEnteredLocal;
            }

            if (DeliveredQuantity.Trim().Length == 0)
            {
                return "0";
            }
            else
            {
                if (DeliveryNoteQuantity.Trim().Length == 0)
                {
                    DeliveryNoteQuantity = "0";
                    return (Convert.ToInt32(DeliveredQuantity) - Convert.ToInt32(OutstandingPOQuantity)).ToString();
                }
                if ((Convert.ToInt32(DeliveredQuantity) - Convert.ToInt32(OutstandingPOQuantity)) > (Convert.ToInt32(DeliveredQuantity) - Convert.ToInt32(DeliveryNoteQuantity)))
                {
                    return (Convert.ToInt32(DeliveredQuantity) - Convert.ToInt32(OutstandingPOQuantity)).ToString();
                }
                else
                {
                    return (Convert.ToInt32(DeliveredQuantity) - Convert.ToInt32(DeliveryNoteQuantity)).ToString();
                }
            }
        }
        return "";

    }

    protected void TextBox_TextChanged_txtDeliveryQty(object sender, EventArgs e)
    {

        mvOvers.ActiveViewIndex = 1;
        btnLog.Visible = false;
        btnBack.Visible = true;
        btnSave.Visible = true;


        TextBox thisTextBox = (TextBox)sender;
        GridViewRow thisGridViewRow = (GridViewRow)thisTextBox.Parent.Parent;
        int ind = thisGridViewRow.RowIndex;
        GridViewRow row = grdPurchaseOrderDetail.Rows[ind];

        Label lblOutstandingQty = (Label)row.FindControl("lblOutstandingQty1");
        TextBox txtDeliveryNoteQty = (TextBox)row.FindControl("txtDeliveryNoteQty");
        TextBox txtDeliveryQty = (TextBox)row.FindControl("txtDeliveryQty");
        Label lblOvers = (Label)row.FindControl("lblOvers");



        int iDeliveryQty, iDeliveryNoteQty;
        bool isiDeliveryQtyDecimal = int.TryParse(txtDeliveryQty.Text.Trim(), out iDeliveryQty);
        bool isDeliveryNoteQtyDecimal = int.TryParse(txtDeliveryNoteQty.Text.Trim(), out iDeliveryNoteQty);
        if (txtDeliveryQty.Text.Trim() != "" && !isiDeliveryQtyDecimal)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + InvalidValuesEntered + "')", true);
            return;
        }
        if (txtDeliveryNoteQty.Text.Trim() != "" && !isDeliveryNoteQtyDecimal)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + InvalidValuesEntered + "')", true);
            return;
        }

        if (txtDeliveryQty.Text.Trim() != "" && isiDeliveryQtyDecimal && !txtDeliveryQty.Text.IsNumeric())
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + InvalidValuesEntered + "')", true);
            return;
        }
        if (txtDeliveryNoteQty.Text.Trim() != "" && isDeliveryNoteQtyDecimal && !txtDeliveryNoteQty.Text.IsNumeric())
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + InvalidValuesEntered + "')", true);
            return;
        }

        if (txtDeliveryQty.Text.Trim().Length == 0)
        {
            lblOvers.Text = "0";

        }
        else
        {
            if (txtDeliveryNoteQty.Text.Trim().Length == 0)
            {
                txtDeliveryNoteQty.Text = "0";
                lblOvers.Text = (Convert.ToInt32(txtDeliveryQty.Text) - Convert.ToInt32(lblOutstandingQty.Text)).ToString();
                return;
            }


            if ((Convert.ToInt32(txtDeliveryQty.Text) - Convert.ToInt32(lblOutstandingQty.Text)) > (Convert.ToInt32(txtDeliveryQty.Text) - Convert.ToInt32(txtDeliveryNoteQty.Text)))
            {
                lblOvers.Text = (Convert.ToInt32(txtDeliveryQty.Text) - Convert.ToInt32(lblOutstandingQty.Text)).ToString();
            }
            else
            {
                lblOvers.Text = (Convert.ToInt32(txtDeliveryQty.Text) - Convert.ToInt32(txtDeliveryNoteQty.Text)).ToString();
            }

        }

        if (txtDeliveryQty.Text.Trim().Length == 0 && txtDeliveryNoteQty.Text.Trim().Length == 0)
        {
            lblOvers.Text = "";
        }


    }

    public override BusinessEntities.BaseBe[] SubSort(GridViewSortEventArgs e)
    {

        return Utilities.GenericListHelper<DiscrepancyBE>.SortList((List<DiscrepancyBE>)ViewState["lstSitesViewMode"], e.SortExpression, e.SortDirection).ToArray();
    }

    protected void btnSave_1_Click(object sender, EventArgs e)
    {
        DiscrepancyBE discrepancyBE = null;
        DiscrepancyBAL discrepancyBAL = null;

        if (!string.IsNullOrEmpty(txtUserComments.Text) && !string.IsNullOrWhiteSpace(txtUserComments.Text))
        {
            lblErrorMsg.Text = string.Empty;
            discrepancyBE = new DiscrepancyBE();
            discrepancyBE.Action = "AddDiscrepancyLogComment";

            if (!string.IsNullOrEmpty(GetQueryStringValue("disLogID")))
                discrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID"));
            else
                discrepancyBE.DiscrepancyLogID = 0;

            discrepancyBE.UserID = Convert.ToInt32(Session["UserID"]);
            discrepancyBE.CommentDateTime = DateTime.Now;
            discrepancyBE.Comment = txtUserComments.Text;
            discrepancyBAL = new DiscrepancyBAL();
            discrepancyBAL.AddDiscrepancyLogCommentBAL(discrepancyBE);
            discrepancyBE.Action = "GetDiscrepancyLogComment";
            this.BindDiscrepancyLogComment(discrepancyBE);

            discrepancyBE.DiscrepancyType = "shuttle";
            int vendorID = 0;
            if (ViewState["VendorID"] != null)
                vendorID = Convert.ToInt32(ViewState["VendorID"]);
            new SendCommentMail().SendCommentsMail(discrepancyBE,
                (BaseControlLibrary.ucTextbox)ucSDRCommunication1.FindControl("txtAltEmail"),
                (BaseControlLibrary.ucTextbox)ucSDRCommunication1.FindControl("ucVendorEmailList"),
                vendorID, txtUserComments.Text, Session["Role"].ToString());
            mdlAddComment.Hide();
        }
        else
        {
            lblErrorMsg.Text = WebCommon.getGlobalResourceValue("MustEnterComment");
            mdlAddComment.Show();
        }
        btnSave.Visible = false;
        btnBack.Visible = false;
        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "Reload()", true);
    }

    protected void btnBack_2_Click(object sender, EventArgs e)
    {
        mdlAddComment.Hide();
        btnSave.Visible = false;
        btnBack.Visible = false;
        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "Reload()", true);
    }

    protected void btnAddComment_Click(object sender, EventArgs e)
    {
        txtUserComments.Text = string.Empty;
        lblErrorMsg.Text = string.Empty;
        lblUserT.Text = Convert.ToString(Session["UserName"]);
        lblDateT.Text = DateTime.Now.Date.ToString("dd/MM/yyyy");
        if (GetQueryStringValue("VDRNo") != null)
            lblDiscrepancyNoT.Text = Convert.ToString(GetQueryStringValue("VDRNo"));
        mdlAddComment.Show();
        txtUserComments.Focus();
        btnSave.Visible = false;
        btnBack.Visible = false;
    }

    protected void btnAddImages_Click(object sender, EventArgs e) {

        EncryptQueryString("DIS_OptionsPopup.aspx?Mode=Edit&VDRNo=" + GetQueryStringValue("VDRNo").ToString() + "&DiscrepancyLogID=" + GetQueryStringValue("disLogID").ToString() + "&CommunicationType=" + Common.DiscrepancyType.ShuttleDiscrepancy);
    }

    //protected void btnAccountsPayableAction_Click(object sender, EventArgs e) { }

    /* ENHANCEMENT FOR PHASE R3 */
    /*RAISE QUERY BUTTON*/
    protected void btnRaiseQuery_Click(object sender, EventArgs e)
    {
        try
        {
            var queryDiscrepancyBAL = new DISLog_QueryDiscrepancyBAL();
            var queryDiscrepancyBE = new DISLog_QueryDiscrepancyBE();
            var discrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID"));

            if (!this.IsAlreadyDisputeExist(discrepancyLogID) || !this.IsQueryClosed(discrepancyLogID))
            {
                btnSave.Visible = false;
                btnBack.Visible = false;
                mdlRaiseQueryConfirmMsg.Show();
            }
            else
            {

                lblMessageFirst.Text = strThereIsCurrentlyAlreadyAnOpenQueryBeingInvestigated;
                lblMessageSecond.Text = strPleaseWaitUntilYouHaveReceivedAReplyForThisBefore;
                btnSave.Visible = false;
                btnBack.Visible = false;
                mdlShowError.Show();
            }

        }

        catch (Exception ex)
        {
            LogUtility.SaveErrorLogEntry(ex);
        }


    }

    #region Alternate email code ...

    protected void btnSetAlternateEmail_Click(object sender, EventArgs e)
    {
        rfvEmailAddressCanNotBeEmpty.Enabled = false;
        var txtAltEmail = (ucTextbox)ucSDRCommunication1.FindControl("txtAltEmail");
        if (txtAltEmail != null)
        {
            txtAltEmail.Text = txtAlternateEmailText.Text;
            txtAlternateEmailText.Text = string.Empty;
        }

        if (Session["Role"].ToString().ToLower() == "vendor")
        {
            string LoginPath = ResolveClientUrl("../../../ModuleUI/Security/Login.aspx");
            Response.Redirect(LoginPath);
        }
        else
        {
            this.Save();
        }
    }

    protected void btnAddEmailId_Click(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(lblMultyAlternateEmail.Text) && !string.IsNullOrWhiteSpace(lblMultyAlternateEmail.Text))
        {
            if (!lblMultyAlternateEmail.Text.Contains(txtAlternateEmailText.Text))
            {
                lblMultyAlternateEmail.Text = string.Format("{0}, {1}", lblMultyAlternateEmail.Text, txtAlternateEmailText.Text);
            }
        }
        else
        {
            lblMultyAlternateEmail.Text = txtAlternateEmailText.Text;
        }

        lblMultyAlternateEmail.Visible = true;
        txtAlternateEmailText.Text = string.Empty;
        txtAlternateEmailText.Focus();
        mdlAlternateEmail.Show();
    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        txtAlternateEmailText.Text = string.Empty;
        lblMultyAlternateEmail.Text = string.Empty;
        txtAlternateEmailText.Focus();
        mdlAlternateEmail.Show();
    }

    #endregion

    #endregion

    #region Methods

    private void GetAccessRightRaiseQuery()
    {
        DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
        DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();
        oDiscrepancyBE.Action = "CheckUserRaiseQueryRight";
        oDiscrepancyBE.UserID = Convert.ToInt32(Session["UserID"]);
        oDiscrepancyBE.SiteID = Convert.ToInt32(Convert.ToString(hdnSiteID.Value));
        int? lstDetails = oDiscrepancyBAL.getAccessRightRaiseQueryBAL(oDiscrepancyBE);
        if (lstDetails > 0)
        {
            btnRaiseQuery.Visible = true;
        }
        else
        {
            btnRaiseQuery.Visible = false;
        }
    }

    private void GetDeliveryDetails()
    {
        if (GetQueryStringValue("disLogID") != null)
        {
            DiscrepancyBE oNewDiscrepancyBE = new DiscrepancyBE();
            DiscrepancyBAL oNewDiscrepancyBAL = new DiscrepancyBAL();
            oNewDiscrepancyBE.Action = "GetDiscrepancyLog";
            oNewDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
            if (GetQueryStringValue("status").ToLower() == "deleted")
            {
                oNewDiscrepancyBE.IsDelete = true;
            }
            oNewDiscrepancyBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
            //oNewDiscrepancyBE.User.RoleTypeFlag = null;
            List<DiscrepancyBE> lstDisLog = oNewDiscrepancyBAL.GetDiscrepancyLogDetailsBAL(oNewDiscrepancyBE);
            oNewDiscrepancyBAL = null;
            if (lstDisLog != null && lstDisLog.Count > 0)
            {
                hdnSiteID.Value = Convert.ToString(lstDisLog[0].Site.SiteID);
                lblSiteValue.Text = lstDisLog[0].Site.SiteName;
                lblPurchaseOrderValue.Text = lstDisLog[0].PurchaseOrderNumber;
                lblPurchaseOrderDateValue.Text = "- " + (Convert.ToDateTime(lstDisLog[0].PurchaseOrderDate)).ToString("dd/MM/yyyy");
                lblIssueRaisedDateValue.Text = (Convert.ToDateTime(lstDisLog[0].DiscrepancyLogDate)).ToString("dd/MM/yyyy");
                lblIssueRaisedTimeValue.Text = "- " + (Convert.ToDateTime(lstDisLog[0].DiscrepancyLogDate)).ToString("HH:MM");
                lblDeliveryNoValue.Text = lstDisLog[0].DeliveryNoteNumber.ToString();
                lblDeliveryDateValue.Text = "- " + (Convert.ToDateTime(lstDisLog[0].DeliveryArrivedDate)).ToString("dd/MM/yyyy");
                txtInternalComments.Text = lstDisLog[0].InternalComments.ToString();
                txtInternalComments.ReadOnly = true;
                lblVendorNumberValue.Text = lstDisLog[0].VendorNoName.ToString();
                //lblContactValue.Text = lstDisLog[0].Vendor.VendorContactNumber.ToString();
                lblContactValue.Text = UIUtility.GetUserPhoneNoofVendor(Convert.ToInt32(lstDisLog[0].VendorID), Convert.ToInt32(lstDisLog[0].SiteID));
                txtLocation.Text = lstDisLog[0].Location.ToString();
                lblStockPlannerNoValue.Text = lstDisLog[0].StockPlannerNO.ToString();
                lblPlannerContactNoValue.Text = lstDisLog[0].StockPlannerContact.ToString();
                SiteID = lstDisLog[0].Site.SiteID;
                VDRNo = lstDisLog[0].VDRNo.ToString();
                ucSDRCommunication1.innerControlRdoCommunication = lstDisLog[0].CommunicationType.Value;
                ucSDRCommunication1.VendorEmailList = lstDisLog[0].CommunicationTo;
                ucSDRCommunication1.innerControlRdoLetter.Enabled = false;
                ucSDRCommunication1.innerControlRdoEmail.Enabled = false;
                ucSDRCommunication1.innerControlEmailList.Enabled = false;
                ucSDRCommunication1.innerControlAltEmailList.Enabled = false;
                lblSentFromValue.Text = lstDisLog[0].POSiteName;
                lblPickerNameValue.Text = lstDisLog[0].PickersName;
                ViewState["StockPlannerEmailID"] = lstDisLog[0].StockPlannerEmailID;
                lblSPActionComments.Text = "Comments : " + lstDisLog[0].ActionComments;

                lblDecision.Text = "Decision :  " + lstDisLog[0].Decision;

                if (!string.IsNullOrEmpty(lstDisLog[0].ActionComments) && !string.IsNullOrEmpty(lstDisLog[0].Decision))
                {
                    pnlDecisionComments.Visible = true;
                }
            }
        }
        else
        {
            hdnSiteID.Value = Convert.ToString(ucSite.innerControlddlSite.SelectedItem.Value);
            lblSiteValue.Text = ucSite.innerControlddlSite.SelectedItem.Text;
            lblPurchaseOrderValue.Text = txtPurchaseOrder.Text;
            lblPurchaseOrderDateValue.Text = "- " + ddlPurchaseOrderDate.SelectedItem.Text;
            lblIssueRaisedDateValue.Text = DateTime.Now.ToString("dd/MM/yyyy");
            lblIssueRaisedTimeValue.Text = DateTime.Now.ToString("- HH:MM");
            lblDeliveryNoValue.Text = txtDeliveryNumber.Text;
            lblDeliveryDateValue.Text = "- " + txtDateDeliveryArrived.innerControltxtDate.Value;
            lblSentFromValue.Text = ucSentFromSite.innerControlddlSite.SelectedItem.Text;
            lblPickerNameValue.Text = txtPickerNameValue.Text;
        }
    }

    private void GetContactPurchaseOrderDetails()
    {

        DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
        DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();

        if (GetQueryStringValue("disLogID") != null)
        {
            grdPOViewMode.Style["display"] = "";
            grdPurchaseOrderDetail.Style["display"] = "none";
            oDiscrepancyBE.Action = "GetDiscrepancyItem";
            oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());
            List<DiscrepancyBE> lstDetails = oDiscrepancyBAL.GetDiscrepancyItemDetailsBAL(oDiscrepancyBE);
            if (lstDetails.Count > 0 && lstDetails != null)
            {
                grdPOViewMode.DataSource = lstDetails;
                grdPOViewMode.DataBind();
                ViewState["lstSitesViewMode"] = lstDetails;
                ViewState["VendorID"] = Convert.ToInt32(lstDetails[0].VendorID.ToString());
                ViewState["ActualStockPlannerID"] = lstDetails[0].ActualStockPlannerID != null ?
                                                Convert.ToInt32(lstDetails[0].ActualStockPlannerID.ToString()) : (int?)null; // Stock Planner ID while discrepancy is logged
            }

            DiscrepancyBE DiscrepancyBE = new DiscrepancyBE();
            DiscrepancyBAL DiscrepancyBAL = new DiscrepancyBAL();

            DiscrepancyBE.Action = "GetActualStockPlannerDetailsBySPId"; // getting details of SP while discrepancy is logged

            if (ViewState["ActualStockPlannerID"] != null)
                DiscrepancyBE.StockPlannerID = Convert.ToInt32(ViewState["ActualStockPlannerID"].ToString());

            List<DiscrepancyBE> lstActualStockPlannerDetailsBySPId = DiscrepancyBAL.GetActualStockPlannerDetailsBAL(DiscrepancyBE);

            if (lstActualStockPlannerDetailsBySPId.Count > 0)
            {
                lblStockPlannerNoValue.Text = lstActualStockPlannerDetailsBySPId[0].StockPlannerNO.ToString();
                lblPlannerContactNoValue.Text = lstActualStockPlannerDetailsBySPId[0].StockPlannerContact.ToString();
            }
        }

        else
        {
            grdPOViewMode.Style["display"] = "none";
            grdPurchaseOrderDetail.Style["display"] = "";


            oDiscrepancyBE.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();
            oDiscrepancyBE.Action = "GetContactPurchaseOrderDetails";
            oDiscrepancyBE.PurchaseOrderNumber = txtPurchaseOrder.Text.Trim();
            oDiscrepancyBE.PurchaseOrder = new BusinessEntities.ModuleBE.Upload.Up_PurchaseOrderDetailBE();
            oDiscrepancyBE.PurchaseOrder.Order_raised = Common.GetMM_DD_YYYY(ddlPurchaseOrderDate.SelectedItem.Text);
            oDiscrepancyBE.SiteID = Convert.ToInt32(ucSite.innerControlddlSite.SelectedItem.Value);

            List<DiscrepancyBE> lstDetails = oDiscrepancyBAL.GetContactPurchaseOrderDetailsBAL(oDiscrepancyBE);
            if (lstDetails.Count > 0 && lstDetails != null)
            {
                if (!string.IsNullOrEmpty(ddlPurchaseOrderDate.SelectedItem.Value))
                {
                    List<DiscrepancyBE> lstGetPODateList = (List<DiscrepancyBE>)ViewState["GetPODateList"];
                    var PODateDetail = lstGetPODateList.Find(p => p.RowNumber == Convert.ToInt32(ddlPurchaseOrderDate.SelectedItem.Value));
                    lblVendorNumberValue.Text = PODateDetail.VendorNoName;
                }

                //lblVendorNumberValue.Text = ddlPurchaseOrderDate.SelectedItem.Value.ToString();
                //lblContactValue.Text = lstDetails[0].Vendor.VendorContactNumber.ToString();
                lblContactValue.Text = UIUtility.GetUserPhoneNoofVendor(Convert.ToInt32(lstDetails[0].VendorID), Convert.ToInt32(ucSite.innerControlddlSite.SelectedItem.Value));

                lblStockPlannerNoValue.Text = lstDetails[0].StockPlannerNO.ToString();
                lblPlannerContactNoValue.Text = lstDetails[0].StockPlannerContact.ToString();

                //ucSDRCommunication1.innerControllblFaxNumber.Text = lstDetails[0].Vendor.VendorContactFax.ToString();
                //ucSDRCommunication1.innerControllblEmailID.Text = lstDetails[0].Vendor.VendorContactEmail.ToString();

                ViewState["PurchaseOrderID"] = lstDetails[0].PurchaseOrderID != null ? Convert.ToInt32(lstDetails[0].PurchaseOrderID.ToString()) : (int?)null;
                ViewState["VendorID"] = lstDetails[0].VendorID != null ? Convert.ToInt32(lstDetails[0].VendorID.ToString()) : (int?)null; ;
                ViewState["StockPlannerID"] = lstDetails[0].StockPlannerID != null ? Convert.ToInt32(lstDetails[0].StockPlannerID.ToString()) : (int?)null; ;
                ViewState["StockPlannerEmailID"] = lstDetails[0].StockPlannerEmailID != null ? lstDetails[0].StockPlannerEmailID : string.Empty;

                grdPurchaseOrderDetail.DataSource = lstDetails;
                grdPurchaseOrderDetail.DataBind();
                ViewState["lstSites"] = lstDetails;

                DiscrepancyBE DiscrepancyBE = new DiscrepancyBE();
                DiscrepancyBAL DiscrepancyBAL = new DiscrepancyBAL();

                DiscrepancyBE.Action = "GetStockPlannerOnHolidayCover"; // getting details of SP on Holiday Cover if any

                if (ViewState["StockPlannerID"] != null)
                    DiscrepancyBE.StockPlannerID = Convert.ToInt32(ViewState["StockPlannerID"].ToString());

                DiscrepancyBE.SiteID = Convert.ToInt32(ucSite.innerControlddlSite.SelectedItem.Value);
                List<DiscrepancyBE> lstStockPlannerOnHolidayCover = DiscrepancyBAL.GetStockPlannerOnHolidayCoverBAL(DiscrepancyBE);

                if (lstStockPlannerOnHolidayCover.Count > 0)
                {
                    ViewState["StockPlannerToCoverEmailID"] = lstStockPlannerOnHolidayCover[0].StockPlannerToCoverEmailID;
                    ViewState["SPToCoverStockPlannerID"] = lstStockPlannerOnHolidayCover[0].SPToCoverStockPlannerID;
                    //lblStockPlannerNoValue.Text = lstStockPlannerOnHolidayCover[0].StockPlannerNO.ToString();
                    //lblPlannerContactNoValue.Text = lstStockPlannerOnHolidayCover[0].StockPlannerContact.ToString();
                }

            }

            foreach (GridViewRow row in grdPurchaseOrderDetail.Rows)
            {
                TextBox txtbox = (TextBox)row.FindControl("txtDeliveryQty");

                sp1.RegisterPostBackControl(txtbox);
            }
        }
        oDiscrepancyBAL = null;

    }

    private void GetPurchaseOrderList()
    {
        DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
        DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();

        oDiscrepancyBE.Action = "GetPurchaseOrderDate";
        oDiscrepancyBE.SiteID = Convert.ToInt32(ucSite.innerControlddlSite.SelectedValue);
        oDiscrepancyBE.PurchaseOrderNumber = txtPurchaseOrder.Text.Trim();

        List<DiscrepancyBE> lstDetails = oDiscrepancyBAL.GetPurchaseOrderDateListBAL(oDiscrepancyBE);
        ViewState["GetPODateList"] = lstDetails;
        oDiscrepancyBAL = null;
        if (lstDetails.Count > 0 && lstDetails != null)
        {
            FillControls.FillDropDown(ref ddlPurchaseOrderDate, lstDetails, "PurchaseOrder_Order_raised", "RowNumber");
            if (!string.IsNullOrEmpty(ddlPurchaseOrderDate.SelectedItem.Value))
            {
                var PODateDetail = lstDetails.Find(p => p.RowNumber == Convert.ToInt32(ddlPurchaseOrderDate.SelectedItem.Value));
                lblVendorValue.Text = PODateDetail.VendorNoName;
                ucSDRCommunication1.VendorID = Convert.ToInt32(PODateDetail.VendorID); //Convert.ToInt32(lstDetails[ddlPurchaseOrderDate.SelectedIndex].VendorID);
            }

            if (!string.IsNullOrEmpty(ucSite.innerControlddlSite.SelectedValue))
                ucSDRCommunication1.SiteID = Convert.ToInt32(ucSite.innerControlddlSite.SelectedValue);
            else
                ucSDRCommunication1.SiteID = 0;
            ucSDRCommunication1.GetDetails();
        }
        else
        {
            string saveMessage = WebCommon.getGlobalResourceValue("InvalidPurchaseOrder");
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('" + saveMessage + "')", true);

            #region Clearing PO details
            txtPurchaseOrder.Text = string.Empty;
            ddlPurchaseOrderDate.Items.Clear();
            ListItem listItem = new ListItem();
            listItem.Text = "--Select--";
            listItem.Value = "0";
            ddlPurchaseOrderDate.Items.Add(listItem);
            lblVendorValue.Text = string.Empty;
            lblIssueRaisedDate.Text = string.Empty;
            txtPurchaseOrder.Focus();
            #endregion
        }
    }

    private void InsertHTML()
    {

        /* Check for the site "No Escalation" */
        MAS_SiteBE oMAS_SiteBE = new MAS_SiteBE();
        MAS_SiteBAL oMAS_SiteBAL = new MAS_SiteBAL();
        oMAS_SiteBE.Action = "GetSiteDisSettings";
        oMAS_SiteBE.SiteID = Convert.ToInt32(ucSite.innerControlddlSite.SelectedItem.Value);

        oMAS_SiteBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();

        List<MAS_SiteBE> lstSiteDetails = oMAS_SiteBAL.GetSiteMisSettingBAL(oMAS_SiteBE);
        oMAS_SiteBAL = null;
        if (lstSiteDetails != null && lstSiteDetails.Count > 0 && lstSiteDetails[0].DIS_CommunicationWithEscalationType.Trim() != "N")
        {
            DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
            DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();
            WorkflowHTML oWorkflowHTML = new WorkflowHTML();

            string strType = Convert.ToString(ViewState["ShuttleTypes"]); //WebCommon.getGlobalResourceValue("Shortage");
            oDiscrepancyBE.GINHTML = oWorkflowHTML.function1(DateTime.Now, null, null, "Yellow", string.Empty, "Shuttle Discrpancy Created",
                //Session["UserName"].ToString(), 
                UIUtility.GetODVendorUserName(Convert.ToString(Session["Role"]), Convert.ToString(Session["UserName"]), Convert.ToString(Session["LoginID"])),
                txtInternalComments.Text, string.Empty, string.Empty, string.Empty, string.Empty,
                string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty,
                string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, null, strType);
            oDiscrepancyBE.GINActionRequired = false;

            string StockPlannerEmailID = ViewState["StockPlannerEmailID"] == null ? string.Empty : Convert.ToString(ViewState["StockPlannerEmailID"]);

            string StockPlannerToCoverEmailID = ViewState["StockPlannerToCoverEmailID"] == null ? "" : Convert.ToString(ViewState["StockPlannerToCoverEmailID"]);

            if (StockPlannerToCoverEmailID != "")
            {
                oDiscrepancyBE.INVHTML = oWorkflowHTML.function4("Blue", string.Empty, DateTime.Now, StockPlannerToCoverEmailID, string.Empty, string.Empty, ShuttleINVDesc);
            }
            else
            {
                oDiscrepancyBE.INVHTML = oWorkflowHTML.function4("Blue", string.Empty, DateTime.Now, StockPlannerEmailID, string.Empty, string.Empty, ShuttleINVDesc);
            }
            oDiscrepancyBE.INVActionRequired = true;
            oDiscrepancyBE.INVUserControl = "INV013";

            oDiscrepancyBE.APHTML = oWorkflowHTML.function3("White", null, string.Empty, string.Empty);
            oDiscrepancyBE.APActionRequired = false;

            string VendorEmailID = ViewState["ucVendorEmailList"] == null ? string.Empty : Convert.ToString(ViewState["ucVendorEmailList"]);

            //As there is no link error on vendor email -- Add by Abhinav
            //oDiscrepancyBE.VENHTML = oWorkflowHTML.function4("Blue", string.Empty, DateTime.Now, VendorEmailID, string.Empty, string.Empty, ShuttleVenDesc);
            oDiscrepancyBE.VENHTML = oWorkflowHTML.function4("Blue", string.Empty, DateTime.Now, Convert.ToString(sSendMailLink), string.Empty, string.Empty, ShuttleVenDesc);


            oDiscrepancyBE.VenActionRequired = true;
            oDiscrepancyBE.VENUserControl = "VEN026";

            oDiscrepancyBE.Action = "InsertHTML";
            oDiscrepancyBE.DiscrepancyLogID = DiscrepancyLogID;
            oDiscrepancyBE.LoggedDateTime = DateTime.Now;
            oDiscrepancyBE.LevelNumber = 1;
            oDiscrepancyBE.ActionTakenBy = "GIN";
            oDiscrepancyBE.VendorActionCategory = "RA";  //Remedial Action      
            oDiscrepancyBE.InventoryActionCategory = "AR";  //Action Required
            int? iResult = oDiscrepancyBAL.addWorkFlowHTMLsBAL(oDiscrepancyBE);
            oDiscrepancyBAL = null;
        }
        else
        {
            //Insert the HTML entries and close the discrepancy
            DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
            DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();
            WorkflowHTML oWorkflowHTML = new WorkflowHTML();

            oDiscrepancyBE.GINHTML = oWorkflowHTML.function1(DateTime.Now, null, null, "Yellow", string.Empty, "Shuttle Discrepancy Created",
                //Session["UserName"].ToString());
            UIUtility.GetODVendorUserName(Convert.ToString(Session["Role"]), Convert.ToString(Session["UserName"]), Convert.ToString(Session["LoginID"])));
            oDiscrepancyBE.GINActionRequired = false;
            oDiscrepancyBE.INVHTML = oWorkflowHTML.function3("White", null, string.Empty, "Discrepancy Closed");
            oDiscrepancyBE.INVActionRequired = false;

            oDiscrepancyBE.APHTML = oWorkflowHTML.function3("White", null, string.Empty, "Discrepancy Closed");
            oDiscrepancyBE.APActionRequired = false;
            if (ucSDRCommunication1.innerControlRdoCommunication == 'L')   // for letter                                
                oDiscrepancyBE.VENHTML = oWorkflowHTML.function2(DateTime.Now, null, "Blue", string.Empty, string.Empty, InitialCommunicationSent, "Discrepancy closed", string.Empty, string.Empty, Convert.ToString(sLetterLink));
            else
                oDiscrepancyBE.VENHTML = oWorkflowHTML.function2(DateTime.Now, null, "Blue", Convert.ToString(sSendMailLink), "", InitialCommunicationSent, "Discrepancy closed");

            oDiscrepancyBE.VenActionRequired = false;
            oDiscrepancyBE.Action = "InsertHTML";
            oDiscrepancyBE.DiscrepancyLogID = DiscrepancyLogID;
            oDiscrepancyBE.LoggedDateTime = DateTime.Now;
            oDiscrepancyBE.LevelNumber = 1;
            oDiscrepancyBE.CloseDiscrepancy = true;
            int? iResult = oDiscrepancyBAL.addWorkFlowHTMLsBAL(oDiscrepancyBE);
            oDiscrepancyBAL = null;
        }
    }

    private void AddUserControl()
    {
        DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
        DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();
        oDiscrepancyBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
        string CaseOf = String.Empty;
        if (Session["Role"] != null && Session["Role"].ToString().Trim() == "Vendor")
        {
            btnUpdateLocation.Visible = false;
        }
        oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID"));

        if (GetQueryStringValue("RoleTypeFlag") == "A" || GetQueryStringValue("RoleTypeFlag") == null)
        {
            if (Session["UserID"] == null || Session["UserID"].ToString() == "" || Session["Role"] == null || Session["Role"].ToString().Trim() == "")
                return;

            oDiscrepancyBE.User.UserID = Convert.ToInt32(Session["UserID"]);
            CaseOf = Session["Role"].ToString().Trim();
        }
        else
        {
            switch (GetQueryStringValue("RoleTypeFlag").ToString())
            {
                case "G":
                    CaseOf = "OD - Goods In";
                    break;
                case "S":
                    CaseOf = "OD - Stock Planner";
                    break;
                case "P":
                    CaseOf = "OD - Accounts Payable";
                    break;
                case "V":
                    CaseOf = "Vendor";
                    break;
            }
        }

        if (CaseOf == "OD - Stock Planner")
        {
            //oDiscrepancyBE.Action = "CheckForInventory";

            //List<DiscrepancyBE> lstDetails = oDiscrepancyBAL.GetUserDetailsForWorkFlowBAL(oDiscrepancyBE);
            //if (lstDetails != null && lstDetails.Count > 0)
            //{
            //check for inventory user
            oDiscrepancyBE.Action = "CheckForActionRequired";
            oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID"));
            oDiscrepancyBE.INVActionRequired = true;
            DataTable dtActionDetails = oDiscrepancyBAL.GetActionRequiredDetailsForWorkFlowBAL(oDiscrepancyBE);
            if (dtActionDetails != null && dtActionDetails.Rows.Count > 0)
            {
                string sUserControl = "../UserControl/INVAction/" + dtActionDetails.Rows[0]["INVUserControl"].ToString().Trim()
                                        + ".aspx?disLogID=" + GetQueryStringValue("disLogID").ToString()
                                        + "&DisWFlowID=" + dtActionDetails.Rows[0]["DiscrepancyWorkflowID"].ToString()
                                        + "&PONo=" + dtActionDetails.Rows[0]["PurchaseOrderNumber"].ToString()
                                        + "&SiteID=" + SiteID.ToString()
                                        + "&VDRNo=" + VDRNo
                                        + "&FromPage=overs"
                                    + "&PreviousPage=" + GetQueryStringValue("PreviousPage").ToString();
                ifUserControl.Attributes.Add("src", EncryptURL(sUserControl));
            }
            else
            {
                ifUserControl.Style["display"] = "none";
            }
            //}
        }
        else if (CaseOf == "OD - Goods In")
        {
            //check for goods in

            oDiscrepancyBE.Action = "CheckForActionRequired";
            oDiscrepancyBE.DiscrepancyWorkFlowID = !string.IsNullOrEmpty(GetQueryStringValue("WorkFlowID")) ? Convert.ToInt32(GetQueryStringValue("WorkFlowID")) : (int?)null;
            oDiscrepancyBE.GINActionRequired = true;

            DataTable dtActionDetails = oDiscrepancyBAL.GetActionRequiredDetailsForWorkFlowBAL(oDiscrepancyBE);
            if (dtActionDetails != null && dtActionDetails.Rows.Count > 0)
            {

                string sUserControl = "../UserControl/GINAction/" + dtActionDetails.Rows[0]["GINUserControl"].ToString().Trim()
                                        + ".aspx?disLogID=" + GetQueryStringValue("disLogID").ToString()
                                        + "&DisWFlowID=" + dtActionDetails.Rows[0]["DiscrepancyWorkflowID"].ToString()
                                        + "&PONo=" + dtActionDetails.Rows[0]["PurchaseOrderNumber"].ToString()
                                        + "&SiteID=" + SiteID.ToString()
                                        + "&VDRNo=" + VDRNo + "&FromPage=overs"
                                        + "&PreviousPage=" + GetQueryStringValue("PreviousPage").ToString();
                ifUserControl.Attributes.Add("src", EncryptURL(sUserControl));

                //if (dtActionDetails.Rows[0]["GINUserControl"].ToString() == "GIN003")
                //{
                //    /*
                //     * User will able to see GIN003 only 
                //     * when "ACP001" and "VEN001" both have completed their actions.
                //     */
                //    oDiscrepancyBE.Action = "CheckForGIN003Visibility";
                //    DataTable dtCheckForGIN003Visibility = oDiscrepancyBAL.GetCheckForGIN003VisibilityBAL(oDiscrepancyBE);
                //    if (dtCheckForGIN003Visibility != null && dtCheckForGIN003Visibility.Rows.Count > 0)
                //    {
                //        ifUserControl.Attributes.Remove("src");
                //        ifUserControl.Style["display"] = "none";
                //    }
                //}

            }
            else
            {
                ifUserControl.Style["display"] = "none";
            }
        }
        else if (CaseOf == "OD - Accounts Payable")
        {
            //check for account payable in

            oDiscrepancyBE.Action = "CheckForActionRequired";
            oDiscrepancyBE.DiscrepancyWorkFlowID = !string.IsNullOrEmpty(GetQueryStringValue("WorkFlowID")) ? Convert.ToInt32(GetQueryStringValue("WorkFlowID")) : (int?)null;
            oDiscrepancyBE.APActionRequired = true;

            DataTable dtActionDetails = oDiscrepancyBAL.GetActionRequiredDetailsForWorkFlowBAL(oDiscrepancyBE);
            if (dtActionDetails != null && dtActionDetails.Rows.Count > 0)
            {
                string sUserControl = string.Empty;

                sUserControl = "../UserControl/APAction/" + dtActionDetails.Rows[0]["APUserControl"].ToString().Trim()
                                       + ".aspx?disLogID=" + GetQueryStringValue("disLogID").ToString()
                                       + "&DisWFlowID=" + dtActionDetails.Rows[0]["DiscrepancyWorkflowID"].ToString()
                                       + "&PONo=" + dtActionDetails.Rows[0]["PurchaseOrderNumber"].ToString()
                                       + "&SiteID=" + SiteID.ToString()
                                       + "&VDRNo=" + VDRNo
                                       + "&FromPage=overs"
                                       + "&PreviousPage=" + GetQueryStringValue("PreviousPage").ToString();

                ifUserControl.Attributes.Add("src", EncryptURL(sUserControl));
                btnAccountsPayableAction.Visible = true;
            }
            else
            {
                ifUserControl.Style["display"] = "none";
            }
        }
        else if (CaseOf == "Vendor")
        {
            //check for vendor in
            pnlLocation.Visible = false;
            oDiscrepancyBE.Action = "CheckForActionRequired";
            oDiscrepancyBE.DiscrepancyWorkFlowID = !string.IsNullOrEmpty(GetQueryStringValue("WorkFlowID")) ? Convert.ToInt32(GetQueryStringValue("WorkFlowID")) : (int?)null;
            oDiscrepancyBE.VenActionRequired = true;

            DataTable dtActionDetails = oDiscrepancyBAL.GetActionRequiredDetailsForWorkFlowBAL(oDiscrepancyBE);
            if (dtActionDetails != null && dtActionDetails.Rows.Count > 0)
            {

                string sUserControl = "../UserControl/VENAction/" + dtActionDetails.Rows[0]["VENUserControl"].ToString().Trim()
                                        + ".aspx?disLogID=" + GetQueryStringValue("disLogID").ToString()
                                        + "&DisWFlowID=" + dtActionDetails.Rows[0]["DiscrepancyWorkflowID"].ToString()
                                        + "&PONo=" + dtActionDetails.Rows[0]["PurchaseOrderNumber"].ToString()
                                        + "&SiteID=" + SiteID.ToString()
                                        + "&VDRNo=" + VDRNo + "&FromPage=overs"
                                        + "&PreviousPage=" + GetQueryStringValue("PreviousPage").ToString();
                ifUserControl.Attributes.Add("src", EncryptURL(sUserControl));
            }
            else
            {
                ifUserControl.Style["display"] = "none";
            }
        }
        else
        {
            ifUserControl.Style["display"] = "none";
            btnAccountsPayableAction.Visible = true;
        }

        oDiscrepancyBAL = null;
    }


    protected void btnAdd_Click(object sender, EventArgs e)
    {
        IncreaseRow();
    }

    private void AddColumns()
    {
        tbDummy.Columns.Add("Line_No");
        tbDummy.Columns.Add("OD_Code");
        tbDummy.Columns.Add("Direct_code");
        tbDummy.Columns.Add("Vendor_Code");
        tbDummy.Columns.Add("ProductDescription");
        tbDummy.Columns.Add("UOM");
        tbDummy.Columns.Add("Original_quantity");
        tbDummy.Columns.Add("Outstanding_Qty");
        tbDummy.Columns.Add("Type");
        tbDummy.Columns.Add("DiscrepancyQty");   
        tbDummy.Columns.Add("Button");
        tbDummy.Columns.Add("RecordID");
    }

    private void BindWithGrid()
    {
        //add this table to dataset
        try
        {
            dsDummy.Tables.Add(tbDummy);

            //bind this dataset to grid
            grdPODetailAddLine.DataSource = dsDummy;
            grdPODetailAddLine.DataBind();
            ViewState["myDataTable"] = tbDummy;
        }
        catch (Exception ex)
        {
        }
    }

    private void EnableDisableGridControls()
    {
        foreach (GridViewRow item in grdPODetailAddLine.Rows)
        {

            ucTextbox txtLineNo = (ucTextbox)item.FindControl("txtLineNo");
            ucTextbox txtOfficeDepotCode = (ucTextbox)item.FindControl("txtOfficeDepotCode");
            ucTextbox txtVikingCode = (ucTextbox)item.FindControl("txtVikingCode");
            ucTextbox txtVendorItemCode = (ucTextbox)item.FindControl("txtVendorItemCode");
            ucTextbox txtDescription = (ucTextbox)item.FindControl("txtDescription");
            ucTextbox txtOriginalPOQuantity = (ucTextbox)item.FindControl("txtOriginalPOQuantity");
            ucTextbox txtOutstandingQty = (ucTextbox)item.FindControl("txtOutstandingQty");
            ucTextbox txtUoM = (ucTextbox)item.FindControl("txtUoM");
            ucDropdownList drpDiscrepancyType = (ucDropdownList)item.FindControl("ddlType");
            ucTextbox txtDiscrepancyQty = (ucTextbox)item.FindControl("txtDiscrepancyQty");
            ucButton btnAddLine = (ucButton)item.FindControl("btnAddLine");
            ucButton btnRemove = (ucButton)item.FindControl("btnRemove");


            btnRemove.Attributes.Remove("style");


            DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
            DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();

            oDiscrepancyBE.Action = "GetODProductDetails";
            oDiscrepancyBE.ODSKUCode = txtOfficeDepotCode.Text.Trim();
            oDiscrepancyBE.SiteID = Convert.ToInt32(ucSite.innerControlddlSite.SelectedValue);
            List<DiscrepancyBE> lstODProductDetails = oDiscrepancyBAL.GetODProductDetailsBAL(oDiscrepancyBE);
            if (lstODProductDetails != null
                && lstODProductDetails.Count > 0
                && txtOfficeDepotCode.Text.Trim() != "")
            {
                txtLineNo.Enabled = true;
                txtOfficeDepotCode.Enabled = false;
                txtVikingCode.Enabled = false;
                txtVendorItemCode.Enabled = false;                
                txtDescription.Enabled = false;
                txtOriginalPOQuantity.Enabled = true;
                txtOutstandingQty.Enabled = true;
                txtUoM.Enabled = false;
                drpDiscrepancyType.Enabled = true;
                txtDiscrepancyQty.Enabled = true;

            }
            else
            {

                txtLineNo.Enabled = true;
                txtOfficeDepotCode.Enabled = true;
                txtVikingCode.Enabled = true;
                txtVendorItemCode.Enabled = true;               
                txtDescription.Enabled = true;                
                if (txtOriginalPOQuantity != null)
                    txtOriginalPOQuantity.Enabled = true;
                if (txtOutstandingQty != null)
                    txtOutstandingQty.Enabled = true;
                txtUoM.Enabled = true;
                drpDiscrepancyType.Enabled = true;
                txtDiscrepancyQty.Enabled = true;
            }

            GridViewRow currentGridViewRow = (GridViewRow)txtLineNo.Parent.Parent;
            int ind = currentGridViewRow.RowIndex;
            ind = ind + 1;
            if (ind == grdPODetailAddLine.Rows.Count)
            {
                btnAddLine.Style["display"] = "block";
                btnRemove.Style["display"] = "none";
            }
            else
            {
                btnAddLine.Style["display"] = "none";
                btnRemove.Style["display"] = "block";
            }

        }
    }

    private void IncreaseRow()
    {
        int Total = grdPODetailAddLine.Rows.Count;
        int iCount = 1;
        Total = Total + 1; //increase 1 because of add
        AddColumns();
        foreach (GridViewRow item in grdPODetailAddLine.Rows)
        {
            ucTextbox txtLineNo = (ucTextbox)item.FindControl("txtLineNo");
            ucTextbox txtOfficeDepotCode = (ucTextbox)item.FindControl("txtOfficeDepotCode");
            ucTextbox txtVikingCode = (ucTextbox)item.FindControl("txtVikingCode");
            ucTextbox txtVendorItemCode = (ucTextbox)item.FindControl("txtVendorItemCode");
            ucTextbox txtDescription = (ucTextbox)item.FindControl("txtDescription");
            ucTextbox txtOriginalPOQuantity = (ucTextbox)item.FindControl("txtOriginalPOQuantity");
            ucTextbox txtOutstandingQty = (ucTextbox)item.FindControl("txtOutstandingQty");
            ucTextbox txtUoM = (ucTextbox)item.FindControl("txtUoM");
            ucDropdownList drpDiscrepancyType = (ucDropdownList)item.FindControl("ddlType");
            ucTextbox txtDiscrepancyQty = (ucTextbox)item.FindControl("txtDiscrepancyQty");
            ucButton btnAddLine = (ucButton)item.FindControl("btnAddLine");
            ucButton btnRemove = (ucButton)item.FindControl("btnRemove");

           

            tbDummy.Rows.Add(txtLineNo.Text.Trim(),
                                 txtOfficeDepotCode.Text.Trim(),
                                 txtVikingCode.Text.Trim(),
                                 txtVendorItemCode.Text.Trim(),
                                 txtDescription.Text.Trim(),
                                  txtUoM.Text.Trim(),
                                 txtOriginalPOQuantity.Text.Trim(),
                                 txtOutstandingQty.Text.Trim(),                                 
                                 drpDiscrepancyType.SelectedItem.Text,
                                 txtDiscrepancyQty.Text.Trim(),                                
                                 "Remove",
                                 (iCount++).ToString());

        }

        tbDummy.Rows.Add(string.Empty,
                                 string.Empty,
                                 string.Empty,
                                 string.Empty,
                                 string.Empty,
                                 string.Empty,
                                 string.Empty,
                                 string.Empty,
                                 string.Empty,
                                 string.Empty,
                                 "Remove",
                                 (iCount++).ToString());


        BindWithGrid();

        EnableDisableGridControls();
    }

    protected void btnOk_Click(object sender, EventArgs e)
    {
        mvOvers.ActiveViewIndex = 1;
        btnLog.Visible = false;
        btnBack.Visible = true;
        btnSave.Visible = true;

        GridViewRow currentGridViewRow = (GridViewRow)currentTextBox.Parent.Parent;
        int ind = currentGridViewRow.RowIndex;
        GridViewRow row = grdPODetailAddLine.Rows[ind];   

        ucTextbox txtLineNo = (ucTextbox)row.FindControl("txtLineNo");
        ucTextbox txtOfficeDepotCode = (ucTextbox)row.FindControl("txtOfficeDepotCode");
        ucTextbox txtVikingCode = (ucTextbox)row.FindControl("txtVikingCode");
        ucTextbox txtVendorItemCode = (ucTextbox)row.FindControl("txtVendorItemCode");
        ucTextbox txtDescription = (ucTextbox)row.FindControl("txtDescription");
        ucTextbox txtOriginalPOQuantity = (ucTextbox)row.FindControl("txtOriginalPOQuantity");
        ucTextbox txtOutstandingQty = (ucTextbox)row.FindControl("txtOutstandingQty");
        ucTextbox txtUoM = (ucTextbox)row.FindControl("txtUoM");
        ucDropdownList drpDiscrepancyType = (ucDropdownList)row.FindControl("ddlType");
        ucTextbox txtDiscrepancyQty = (ucTextbox)row.FindControl("txtDiscrepancyQty");
        ucButton btnAddLine = (ucButton)row.FindControl("btnAddLine");
        ucButton btnRemove = (ucButton)row.FindControl("btnRemove");

        txtOfficeDepotCode.Text = "";
        txtVikingCode.Text = "";
        txtVendorItemCode.Text = "";
        txtDescription.Text = "";
        txtUoM.Text = "";
        txtOriginalPOQuantity.Text = "";
        txtOutstandingQty.Text = "";
        txtDiscrepancyQty.Text = "";

        txtLineNo.Enabled = true;
        txtOfficeDepotCode.Enabled = true;
        txtVikingCode.Enabled = true;
        txtVendorItemCode.Enabled = true;
        txtDiscrepancyQty.Enabled = true;
        txtDescription.Enabled = true;
        txtUoM.Enabled = true;
        txtOutstandingQty.Enabled = true;
        txtOriginalPOQuantity.Enabled = true;
        btnAddLine.Style["display"] = "block";
        btnRemove.Style["display"] = "none";

        currentTextBox = null;
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        mvOvers.ActiveViewIndex = 1;
        btnLog.Visible = false;
        btnBack.Visible = true;
        btnSave.Visible = true;

        GridViewRow currentGridViewRow = (GridViewRow)currentTextBox.Parent.Parent;
        int ind = currentGridViewRow.RowIndex;
        GridViewRow row = grdPODetailAddLine.Rows[ind];

        ucTextbox txtLineNo = (ucTextbox)row.FindControl("txtLineNo");
        ucTextbox txtOfficeDepotCode = (ucTextbox)row.FindControl("txtOfficeDepotCode");
        ucTextbox txtVikingCode = (ucTextbox)row.FindControl("txtVikingCode");
        ucTextbox txtVendorItemCode = (ucTextbox)row.FindControl("txtVendorItemCode");
        ucTextbox txtDescription = (ucTextbox)row.FindControl("txtDescription");
        ucTextbox txtOriginalPOQuantity = (ucTextbox)row.FindControl("txtOriginalPOQuantity");
        ucTextbox txtOutstandingQty = (ucTextbox)row.FindControl("txtOutstandingQty");
        ucTextbox txtUoM = (ucTextbox)row.FindControl("txtUoM");
        ucDropdownList drpDiscrepancyType = (ucDropdownList)row.FindControl("ddlType");
        ucTextbox txtDiscrepancyQty = (ucTextbox)row.FindControl("txtDiscrepancyQty");
        ucButton btnAddLine = (ucButton)row.FindControl("btnAddLine");
        ucButton btnRemove = (ucButton)row.FindControl("btnRemove");


        txtOfficeDepotCode.Text = "";
        txtVikingCode.Text = "";
        txtVendorItemCode.Text = "";
        txtDescription.Text = "";
        txtUoM.Text = "";
        txtOriginalPOQuantity.Text = "";
        txtOutstandingQty.Text = "";
        txtDiscrepancyQty.Text = "";

        txtLineNo.Enabled = true;
        txtOfficeDepotCode.Enabled = true;
        txtVikingCode.Enabled = false;
        txtVendorItemCode.Enabled = false;
        txtDescription.Enabled = false;
        txtUoM.Enabled = false;
        txtOriginalPOQuantity.Enabled = false;
        txtOutstandingQty.Enabled = false;
        btnAddLine.Style["display"] = "block";
        btnRemove.Style["display"] = "none";
        currentTextBox = null;
    }

    protected void TextBox_TextChanged_txtLineNo(object sender, EventArgs e)
    {
        mvOvers.ActiveViewIndex = 1;
        btnLog.Visible = false;
        btnBack.Visible = true;
        btnSave.Visible = true;

        currentTextBox = (BaseControlLibrary.ucTextbox)sender;

        GridViewRow currentGridViewRow = (GridViewRow)currentTextBox.Parent.Parent;
        int ind = currentGridViewRow.RowIndex;


        if (currentTextBox.Text != "" || currentTextBox.Text != string.Empty)
        {
            DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
            DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();

            oDiscrepancyBE.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();

            oDiscrepancyBE.Action = "GetContactPurchaseOrderDetails";
            oDiscrepancyBE.PurchaseOrderNumber = txtPurchaseOrder.Text.Trim();
            oDiscrepancyBE.PurchaseOrder = new BusinessEntities.ModuleBE.Upload.Up_PurchaseOrderDetailBE();
            oDiscrepancyBE.PurchaseOrder.Order_raised = Common.GetMM_DD_YYYY(ddlPurchaseOrderDate.SelectedItem.Text);
            oDiscrepancyBE.SiteID = Convert.ToInt32(ucSite.innerControlddlSite.SelectedItem.Value);

            List<DiscrepancyBE> lstDetails = oDiscrepancyBAL.GetContactPurchaseOrderDetailsBAL(oDiscrepancyBE);
            if (lstDetails.Count > 0 && lstDetails != null)
            {
                var LineNO = lstDetails.Find(dbe => dbe.PurchaseOrder.Line_No.ToString() == currentTextBox.Text);
                if (LineNO != null)
                {
                   
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + AlreadyExistLineNumber + "')", true);
                    currentTextBox.Text = string.Empty;
                    return;
                }
            }

            // check that line no is already used in previous rows.


            for (int item = 0; item < grdPODetailAddLine.Rows.Count - 1; item++)
            {

                ucTextbox txtLineNo = (ucTextbox)grdPODetailAddLine.Rows[item].FindControl("txtLineNo");
                HiddenField hdnRecordID = (HiddenField)grdPODetailAddLine.Rows[item].FindControl("hdnRecordID");

                if (ind > 0) // so that it checks from 2 row.
                {
                    if (currentTextBox.Text == txtLineNo.Text)
                    {
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + AlreadyExistLineNumber + "')", true);
                        currentTextBox.Text = string.Empty;
                        return;
                    }
                }
            }


        }
    }

    protected void TextBox_TextChanged_txtOfficeDepotCode(object sender, EventArgs e)
    {
        mvOvers.ActiveViewIndex = 1;
        btnLog.Visible = false;
        btnBack.Visible = true;
        btnSave.Visible = true;


        currentTextBox = (BaseControlLibrary.ucTextbox)sender;

        GridViewRow currentGridViewRow = (GridViewRow)currentTextBox.Parent.Parent;
        int ind = currentGridViewRow.RowIndex;
        GridViewRow row = grdPODetailAddLine.Rows[ind];

        ucTextbox txtLineNo = (ucTextbox)row.FindControl("txtLineNo");
        ucTextbox txtOfficeDepotCode = (ucTextbox)row.FindControl("txtOfficeDepotCode");
        ucTextbox txtVikingCode = (ucTextbox)row.FindControl("txtVikingCode");
        ucTextbox txtVendorItemCode = (ucTextbox)row.FindControl("txtVendorItemCode");
        ucTextbox txtDescription = (ucTextbox)row.FindControl("txtDescription");
        ucTextbox txtOriginalPOQuantity = (ucTextbox)row.FindControl("txtOriginalPOQuantity");
        ucTextbox txtOutstandingQty = (ucTextbox)row.FindControl("txtOutstandingQty");
        ucTextbox txtUoM = (ucTextbox)row.FindControl("txtUoM");
        ucDropdownList drpDiscrepancyType = (ucDropdownList)row.FindControl("ddlType");
        ucTextbox txtDiscrepancyQty = (ucTextbox)row.FindControl("txtDiscrepancyQty");
        ucButton btnAddLine = (ucButton)row.FindControl("btnAddLine");
        ucButton btnRemove = (ucButton)row.FindControl("btnRemove");


        //if no product code exists in the textbox 
        //just reset the controls at initials
        if (currentTextBox.Text == "" || currentTextBox.Text == string.Empty)
        {
            txtLineNo.Text = "";
            txtVikingCode.Text = "";
            txtVendorItemCode.Text = "";
            txtOfficeDepotCode.Text = "";
            txtDescription.Text = "";
            txtOriginalPOQuantity.Text = "";
            txtOutstandingQty.Text = "";
            txtUoM.Text = "";
           

            txtLineNo.Enabled = false;
            txtVikingCode.Enabled = false;
            txtVendorItemCode.Enabled = false;
            txtOfficeDepotCode.Enabled = true;
            txtDescription.Enabled = false;
            txtUoM.Enabled = false;
            txtOutstandingQty.Enabled = false;
            txtOriginalPOQuantity.Enabled = false;
            btnRemove.Style["display"] = "";
            return;
        }

        DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
        DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();

        oDiscrepancyBE.Action = "GetODProductBasedSiteVendorIds";
        oDiscrepancyBE.ODSKUCode = currentTextBox.Text;
        oDiscrepancyBE.SiteID = !string.IsNullOrWhiteSpace(ucSite.innerControlddlSite.SelectedValue) ? Convert.ToInt32(ucSite.innerControlddlSite.SelectedValue) : 0;
        oDiscrepancyBE.VendorID = ViewState["VendorID"] != null ? Convert.ToInt32(ViewState["VendorID"].ToString()) : (int?)null;

        List<DiscrepancyBE> lstODProductDetails = oDiscrepancyBAL.GetODProductDetailsBySiteVendorBAL(oDiscrepancyBE);
        if (lstODProductDetails == null || (lstODProductDetails != null && lstODProductDetails.Count == 0))
        {
            mdlShuttleViewer.Show();

        }
        else
        {
            //txtLineNo.Text = "";
            txtVikingCode.Text = lstODProductDetails[0].DirectCode;
            txtVendorItemCode.Text = lstODProductDetails[0].VendorCode;
            txtDescription.Text = lstODProductDetails[0].ProductDescription;
            txtUoM.Text = lstODProductDetails[0].UOM;

            txtLineNo.Enabled = true;
            txtOfficeDepotCode.Enabled = false;
            txtVikingCode.Enabled = false;
            txtVendorItemCode.Enabled = false;
            txtDescription.Enabled = false;
            txtUoM.Enabled = false;
            btnAddLine.Style["display"] = "block";
            btnRemove.Style["display"] = "none";
        }
    }

    protected void AddLine_Click(object sender, CommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "AddLine" && e.CommandArgument != null)
            {
                ucButton btn = (BaseControlLibrary.ucButton)sender;

                GridViewRow currentGridViewRow = (GridViewRow)btn.Parent.Parent;
                int ind = currentGridViewRow.RowIndex;
                GridViewRow row = grdPODetailAddLine.Rows[ind];



                DropDownList drpDiscrepancyType = (DropDownList)row.FindControl("ddlType");
                TextBox txtDiscrepancyQty = (TextBox)row.FindControl("txtDiscrepancyQty");
                ucTextbox txtOfficeDepotCode = (ucTextbox)row.FindControl("txtOfficeDepotCode");
                ucTextbox txtDescription = (ucTextbox)row.FindControl("txtDescription");
                ucTextbox txtLineNo = (ucTextbox)row.FindControl("txtLineno");

                if (txtOfficeDepotCode.Text.Trim() != "")
                {
                    if (txtLineNo.Text.Trim() == "")
                    {
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + LineNoReq + "')", true);
                        return;
                    }

                    if (txtDescription.Text.Trim() == "")
                    {
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + ProductDescription + "')", true);
                        return;
                    }

                    if (txtDiscrepancyQty.Text.Trim() == "")
                    {
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + EnterDiscrepancyQuantity + "')", true);
                        return;
                    }


                    if (!drpDiscrepancyType.SelectedItem.Text.Equals("Select") && string.IsNullOrEmpty(txtDiscrepancyQty.Text))
                    {
                        RequiredDiscQtyForType = RequiredDiscQtyForType.Replace("##Type##", drpDiscrepancyType.SelectedItem.Text);
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + RequiredDiscQtyForType + "')", true);
                        return;
                    }


                     if (Convert.ToInt32(txtDiscrepancyQty.Text) > 0 && drpDiscrepancyType.SelectedItem.Text.Equals("Select"))
                    {
                        RequiredTypeForDiscQty = RequiredTypeForDiscQty.Replace("##DiscrepancyQty##", txtDiscrepancyQty.Text);
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + RequiredTypeForDiscQty + "')", true);
                        return;
                    }

                     if (Convert.ToInt32(txtDiscrepancyQty.Text).Equals(0))
                    {
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + DiscrepancyQtyCantBeZero + "')", true);
                        return;
                    }
                   

                    btnAdd_Click(null, null);


                }
                else if (!drpDiscrepancyType.SelectedItem.Text.Equals("Select") && string.IsNullOrEmpty(txtDiscrepancyQty.Text))
                {
                    RequiredDiscQtyForType = RequiredDiscQtyForType.Replace("##Type##", drpDiscrepancyType.SelectedItem.Text);
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + RequiredDiscQtyForType + "')", true);
                    return;
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + AtLeastOne + "')", true);
                    return;
                }



            }
        }
        catch (Exception ex)
        {

        }
    }

    protected void RemoveImage_Click(object sender, CommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "Remove" && e.CommandArgument != null)
            {
                int iCount = 1;
                tbDummy = null;
                tbDummy = new DataTable();
                AddColumns();
                foreach (GridViewRow row in grdPODetailAddLine.Rows)
                {

                    ucTextbox txtLineNo = (ucTextbox)row.FindControl("txtLineNo");
                    ucTextbox txtOfficeDepotCode = (ucTextbox)row.FindControl("txtOfficeDepotCode");
                    ucTextbox txtVikingCode = (ucTextbox)row.FindControl("txtVikingCode");
                    ucTextbox txtVendorItemCode = (ucTextbox)row.FindControl("txtVendorItemCode");
                    ucTextbox txtDescription = (ucTextbox)row.FindControl("txtDescription");
                    ucTextbox txtOriginalPOQuantity = (ucTextbox)row.FindControl("txtOriginalPOQuantity");
                    ucTextbox txtOutstandingQty = (ucTextbox)row.FindControl("txtOutstandingQty");
                    ucTextbox txtUoM = (ucTextbox)row.FindControl("txtUoM");
                    ucDropdownList drpDiscrepancyType = (ucDropdownList)row.FindControl("ddlType");
                    ucTextbox txtDiscrepancyQty = (ucTextbox)row.FindControl("txtDiscrepancyQty");
                    ucButton btnAddLine = (ucButton)row.FindControl("btnAddLine");
                    ucButton btnRemove = (ucButton)row.FindControl("btnRemove");

                    HiddenField hdnRecordID = (HiddenField)row.FindControl("hdnRecordID");
                    if (e.CommandArgument.ToString().Trim() != hdnRecordID.Value.ToString().Trim())
                    {
                        tbDummy.Rows.Add(txtLineNo.Text.Trim(),
                                     txtOfficeDepotCode.Text.Trim(),
                                     txtVikingCode.Text.Trim(),
                                     txtVendorItemCode.Text.Trim(),
                                     txtDescription.Text.Trim(),
                                     txtOriginalPOQuantity.Text.Trim(),
                                     txtOutstandingQty.Text.Trim(),
                                     txtUoM.Text.Trim(),
                                     drpDiscrepancyType.SelectedItem.Text,
                                     txtDiscrepancyQty.Text.Trim(),                                    
                                     "Remove",
                                     (iCount++).ToString());
                    }
                }

                BindWithGrid();
                EnableDisableGridControls();
            }
        }
        catch (Exception ex)
        {

        }
    }

    protected void grdPODetailAddLine_RowDataBound(Object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            ucDropdownList drpDisType = (ucDropdownList)e.Row.FindControl("ddlType");

            if (!string.IsNullOrEmpty(dsDummy.Tables[0].Rows[e.Row.RowIndex]["Type"].ToString()))
            drpDisType.SelectedItem.Text = dsDummy.Tables[0].Rows[e.Row.RowIndex]["Type"].ToString();
        }
    }
    /*----------------ADDING RAISE QUERY METHODS------------*/

    private bool IsAlreadyDisputeExist(int discrepancyLogID = 0)
    {
        var isExist = false;

        if (discrepancyLogID != 0)
        {
            var queryDiscrepancyBAL = new DISLog_QueryDiscrepancyBAL();
            var queryDiscrepancyBE = new DISLog_QueryDiscrepancyBE();
            if (discrepancyLogID > 0)
            {
                queryDiscrepancyBE.Action = "IsAlreadyDisputeExist";
                queryDiscrepancyBE.DiscrepancyLogID = discrepancyLogID;
                var result = queryDiscrepancyBAL.IsAlreadyDisputeExistBAL(queryDiscrepancyBE);
                if (result > 0)
                    isExist = true;
            }
        }

        return isExist;
    }
    /* CHECK QUERY IS CLOSED THEN VENDOR CAN RAISE QUERY*/
    private bool IsQueryClosed(int discrepancyLogID = 0)
    {
        var isExist = false;

        if (discrepancyLogID != 0)
        {
            var queryDiscrepancyBAL = new DISLog_QueryDiscrepancyBAL();
            var queryDiscrepancyBE = new DISLog_QueryDiscrepancyBE();
            if (discrepancyLogID > 0)
            {
                queryDiscrepancyBE.Action = "IsAlreadyDisputeExist";
                queryDiscrepancyBE.DiscrepancyLogID = discrepancyLogID;
                var result = queryDiscrepancyBAL.GetQueryClose(queryDiscrepancyBE);
                if (result != 0)
                    isExist = true;
            }
        }

        return isExist;
    }
    /* METHODS FOR VENDOR'S QUERY OVERVIEW*/
    private void VendorsQueryOverview()
    {

        var queryDiscrepancyBAL = new DISLog_QueryDiscrepancyBAL();
        var queryDiscrepancyBE = new DISLog_QueryDiscrepancyBE();
        var disLogID = GetQueryStringValue("disLogID") != null ? Convert.ToInt32(GetQueryStringValue("disLogID")) : 0;
        //var queryDisId = GetQueryStringValue("QueryDisID") != null ? Convert.ToInt32(GetQueryStringValue("QueryDisID")) : 0;
        if (disLogID > 0)
        {
            queryDiscrepancyBE.Action = "GetQueryDiscrepancy";
            // queryDiscrepancyBE.QueryDiscrepancyID = queryDisId;
            queryDiscrepancyBE.DiscrepancyLogID = disLogID;
            var getQueryDiscrepancy = queryDiscrepancyBAL.GetQueryDiscrepancyBAL(queryDiscrepancyBE);
            if (getQueryDiscrepancy.Count > 0)
            {
                pnlQueryOverview_1.Visible = true;
                rptQueryOverview.DataSource = getQueryDiscrepancy;
                rptQueryOverview.DataBind();

            }
        }
    }
    protected void rptQueryOverview_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            var hdnIsQueryClosedManually = (HiddenField)e.Item.FindControl("hdnIsQueryClosedManually");

            // GoodsIn declaration ...            
            var lblCommentLabelT_2 = (ucLabel)e.Item.FindControl("lblCommentLabelT_2");
            if (lblCommentLabelT_2 != null)
            {
                if (!string.IsNullOrEmpty(lblCommentLabelT_2.Text) ) { e.Item.FindControl("tblGoodsIn").Visible = true; }
                else { e.Item.FindControl("tblGoodsIn").Visible = false;  }

                if (!string.IsNullOrEmpty(lblCommentLabelT_2.Text) && hdnIsQueryClosedManually.Value == "True")
                {
                    e.Item.FindControl("tblGoodsIn").Visible = false;
                    e.Item.FindControl("tblQueryClosedManually").Visible = true;
                }

                var lnkView = (System.Web.UI.HtmlControls.HtmlAnchor)e.Item.FindControl("lnkView");
                var hdnQueryDiscrepancyID = (HiddenField)e.Item.FindControl("hdnQueryDiscrepancyID");
                var hdnDiscrepancyLogID = (HiddenField)e.Item.FindControl("hdnDiscrepancyLogID");
                lnkView.Attributes.Add("href", EncryptQuery("~/ModuleUI/Appointment/Receiving/APPRcv_Communication.aspx?DiscId=" + hdnDiscrepancyLogID.Value
                    + "&QueryDiscId=" + hdnQueryDiscrepancyID.Value));
            }
        }
    }
    #endregion
    protected void btnUpdateLocation_Click(object sender, EventArgs e)
    {
        var disLogID = GetQueryStringValue("disLogID") != null ? Convert.ToInt32(GetQueryStringValue("disLogID")) : 0;
        if (disLogID != null)
        {
            DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
            DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();
            oDiscrepancyBE.Action = "UpdateLocation";
            oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(disLogID);
            oDiscrepancyBE.Location = txtLocation.Text.Trim();
            oDiscrepancyBE.UserID = Convert.ToInt32(Session["UserID"]);
            string sResult = oDiscrepancyBAL.updateDiscrepancyLocationBAL(oDiscrepancyBE);
            if (GetQueryStringValue("PreviousPage") != null)
            {
                if (GetQueryStringValue("PreviousPage").Trim().ToUpper().Equals("SearchResultVendor".ToUpper()))
                {
                    btnBack_1.Visible = false;
                    btnUpdateLocation.Visible = false;
                    EncryptQueryString("~/ModuleUI/Discrepancy/DIS_SearchResultVendor.aspx?disLogID=" + Convert.ToString(GetQueryStringValue("disLogID"))
                                + "&VDRNo=" + Convert.ToString(GetQueryStringValue("VDRNo"))
                                + "&UserID=" + Convert.ToString(GetQueryStringValue("UserID"))
                                + "&PreviousPage=SearchResultVendor&WorkFlowID=" + Convert.ToString(GetQueryStringValue("WorkFlowID").ToString()));
                }

                if (Session["PageMode"].ToString() == "Todays")
                {
                    EncryptQueryString("../DIS_SearchResult.aspx?FromPage=images&SiteId=" + GetQueryStringValue("SiteId") + "&DisDate=" + GetQueryStringValue("DisDate"));
                }
                else if (Session["PageMode"].ToString() == "Search")
                {
                    //For opening search discrepancies mode
                    btnBack_1.Visible = false;
                    btnUpdateLocation.Visible = false;
                    EncryptQueryString("~/ModuleUI/Discrepancy/DIS_SearchResult.aspx?disLogID=" + Convert.ToString(GetQueryStringValue("disLogID"))
                                + "&VDRNo=" + Convert.ToString(GetQueryStringValue("VDRNo"))
                                + "&UserID=" + Convert.ToString(GetQueryStringValue("UserID"))
                                + "&PreviousPage=SearchResult&WorkFlowID=" + Convert.ToString(GetQueryStringValue("WorkFlowID").ToString()));

                }
            }
            else if (GetQueryStringValue("PN") != null)
            {
                if (GetQueryStringValue("PN").Trim().ToUpper().Equals("POINQ"))
                {
                    btnBack_1.Visible = false;
                    btnUpdateLocation.Visible = false;
                    EncryptQueryString("~/ModuleUI/Appointment/PurchaseOrderInquiry.aspx?SId=" + GetQueryStringValue("SId").ToString()
                        + "&Status=" + GetQueryStringValue("Status").ToString()
                        + "&PN=DISLOG"
                        + "&PO=" + GetQueryStringValue("PO").ToString());
                }
                else if (GetQueryStringValue("PN").Trim().ToUpper().Equals("OPENPL"))
                {
                    EncryptQueryString("~/ModuleUI/StockOverview/Expedite/OpenPurchaseListing.aspx?action=showlisting");
                }


            }
        }
    }

    protected void btnContinue_Click(object sender, EventArgs e)
    {
        string EncryptcUrl = EncryptQuery("~/ModuleUI/Discrepancy/DISLog_QueryDiscrepancy.aspx?disLogID=" + Convert.ToString(GetQueryStringValue("disLogID"))
                                + "&VDRNo=" + Convert.ToString(GetQueryStringValue("VDRNo")) + "&RaiseQueryVendor=Yes" + "&RedirectTo=DisLog_Overs.aspx");
        Response.Redirect(EncryptcUrl, false);
        Context.ApplicationInstance.CompleteRequest();
    }

    protected void btnRaiseQueryPopupBack_Click(object sender, EventArgs e)
    {
        btnBack.Visible = false;
        btnSave.Visible = false;
    }
    private void BindDiscrepancyLogComment(DiscrepancyBE discrepancyBE = null)
    {
        var discrepancyBAL = new DiscrepancyBAL();
        if (discrepancyBE != null)
        {
            rptDiscrepancyComments.DataSource = discrepancyBAL.GetDiscrepancyLogCommentsBAL(discrepancyBE);
        }
        else
        {
            discrepancyBE = new DiscrepancyBE();
            discrepancyBE.Action = "GetDiscrepancyLogComment";

            if (!string.IsNullOrEmpty(GetQueryStringValue("disLogID")))
                discrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID"));
            else
                discrepancyBE.DiscrepancyLogID = 0;

            rptDiscrepancyComments.DataSource = discrepancyBAL.GetDiscrepancyLogCommentsBAL(discrepancyBE);
        }

        rptDiscrepancyComments.DataBind();

        if (rptDiscrepancyComments.Items.Count > 0)
            lblComments.Visible = true;
        else
            lblComments.Visible = false;
    }
}
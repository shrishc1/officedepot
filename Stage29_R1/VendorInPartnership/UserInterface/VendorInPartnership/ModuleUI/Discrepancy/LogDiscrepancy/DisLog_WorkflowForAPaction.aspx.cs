﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.Discrepancy.LogDiscrepancy;
using BusinessLogicLayer.ModuleBAL.Discrepancy.LogDiscrepancy;
using System.Text;
using Utilities;
using WebUtilities;
using BaseControlLibrary;

public partial class ModuleUI_Discrepancy_LogDiscrepancy_DisLog_WorkflowForAPaction : CommonPage
{
    string DebitRaisedText = WebCommon.getGlobalResourceValue("DebitRaisedText");

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            lblErrorMsg.Text = string.Empty;
            this.BindDiscrepancyLogComment();
            this.BindDiscrepancyQuery();
            this.SetDebitCreditPosting();
        }

       
        // get vdrno for discrepancy
        if (GetQueryStringValue("VDRNo") != null && !string.IsNullOrEmpty(GetQueryStringValue("VDRNo").ToString()))
        {
            lblVDRNo.Text = " - " + GetQueryStringValue("VDRNo").ToString();
        }
        if (Session["Role"] != null)
        {
            if (Session["Role"].ToString() == "Vendor")
            {
                btnAddComment.Visible = true;
                btnAddImages.Visible = false;
            }
        }

        // get html of discrepancy
        if (GetQueryStringValue("disLogID") != null && !string.IsNullOrEmpty(GetQueryStringValue("disLogID").ToString()))
        {
            int? DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID").ToString());

            if (DiscrepancyLogID.HasValue)
            {
                DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
                DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();
                List<DiscrepancyBE> lstWorkFlow = new List<DiscrepancyBE>();


                oDiscrepancyBE.Action = "GetHtml";
                //oDiscrepancyBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
                //oDiscrepancyBE.User.UserID = Convert.ToInt32(Session["UserID"]);
                oDiscrepancyBE.DiscrepancyLogID = DiscrepancyLogID;

                lstWorkFlow = oDiscrepancyBAL.GetWorkFlowHTMLsBAL(oDiscrepancyBE);
                oDiscrepancyBAL = null;
                if (lstWorkFlow.Count > 0)
                {
                    var lstRPWorkFlowData = lstWorkFlow.Where(x => x.APHTML != "&nbsp;" || x.INVHTML != "&nbsp;" || x.GINHTML != "&nbsp;" || x.VENHTML != "&nbsp;");
                    rpWorkflow.DataSource = lstRPWorkFlowData;
                    rpWorkflow.DataBind();
                    hdDiscrepancyStatus.Value = lstWorkFlow[0].DiscrepancyStatus;

                    if (hdDiscrepancyStatus.Value == "D")
                    {
                        var delDiscrepancyBE = lstWorkFlow.Find(x => x.DELHTML != String.Empty && x.APHTML != null && x.INVHTML != null && x.GINHTML != null && x.VENHTML != null);
                        if (delDiscrepancyBE != null)
                            hdnDelDisData.Value = delDiscrepancyBE.DELHTML;
                    }
                    if (hdDiscrepancyStatus.Value == "F")
                        hdUserData.Value = " Discrepancy Forced Closed By " + lstWorkFlow[0].ForceClosedUserName + " on " + lstWorkFlow[0].ForceClosedDate;
                }

                string DivGallery = CommonPage.GetImagesStringNewLayout(DiscrepancyLogID.Value);
                if (string.IsNullOrEmpty(DivGallery))
                {
                    btnViewImage.Visible = false;
                }
                else
                {
                    discrepancygallery.InnerHtml = DivGallery.ToString();
                    btnViewImage.Visible = true;
                }
            }
        }
        else
        {
            UIUtility.PageValidateForODUser();
        }
    }

    public string GetType(int discrepancyTypeId)
    {
        string strType = string.Empty;
        if (discrepancyTypeId == null) { discrepancyTypeId = 0; }
        switch (discrepancyTypeId)
        {
            case 1:
                strType = WebCommon.getGlobalResourceValue("Overs");
                break;
            case 2:
                strType = WebCommon.getGlobalResourceValue("Shortage");
                break;
            case 3:
                strType = WebCommon.getGlobalResourceValue("GoodsReceivedDamaged");
                break;
            case 4:
                strType = WebCommon.getGlobalResourceValue("NoPurchaseorder");
                break;
            case 5:
                strType = WebCommon.getGlobalResourceValue("NoPaperwork");
                break;
            case 6:
                strType = WebCommon.getGlobalResourceValue("IncorrectProduct");
                break;
            case 7:
                strType = WebCommon.getGlobalResourceValue("PresentationIssue");
                break;
            case 8:
                strType = WebCommon.getGlobalResourceValue("IncorrectAddress");
                break;
            case 9:
                strType = WebCommon.getGlobalResourceValue("PaperworkAmended");
                break;
            case 10:
                strType = WebCommon.getGlobalResourceValue("WrongPackSize");
                break;
            case 11:
                strType = WebCommon.getGlobalResourceValue("FailPalletSpecification");
                break;
            case 12:
                strType = WebCommon.getGlobalResourceValue("QualityIssues");
                break;
            case 13:
                strType = WebCommon.getGlobalResourceValue("PrematureInvoiceReceipt");
                break;
            case 14:
                strType = WebCommon.getGlobalResourceValue("GenericDiscrepancy");
                break;
            case 15:
                strType = WebCommon.getGlobalResourceValue("ShuttleDiscrepancy");
                break;
            case 16:
                strType = WebCommon.getGlobalResourceValue("ReservationIssue");
                break;
        }
        return strType;
    }

    protected void btnGenerateLabels_Click(object sender, EventArgs e)
    {
        int No;
        if (txtNoofLabel.Text.Trim() != string.Empty && int.TryParse(txtNoofLabel.Text, out No))
        {
            //get vendor details  
            DiscrepancyBE oNewDiscrepancyBE = new DiscrepancyBE();
            DiscrepancyBAL oNewDiscrepancyBAL = new DiscrepancyBAL();
            oNewDiscrepancyBE.Action = "GetDiscrepancySiteVendorDetail";
            oNewDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID"));

            List<DiscrepancyBE> lstVendor = oNewDiscrepancyBAL.GetDiscrepancyLogSiteVendorDetailsBAL(oNewDiscrepancyBE);
            oNewDiscrepancyBAL = null;

            StringBuilder sb = new StringBuilder();


            for (int iCount = 1; iCount <= Convert.ToInt32(txtNoofLabel.Text.Trim()); iCount++)
            {
                //if (string.IsNullOrEmpty(lstVendor[0].Vendor.ReturnAddress))
                //{
                string ReturnAddress = lstVendor[0].Vendor.ReturnAddress.Replace("\r\n", "</span></td></tr><tr><td valign='top'><span class='style10'>");
                int discrepancyTypeID = lstVendor[0].DiscrepancyTypeID != null ? Convert.ToInt32(lstVendor[0].DiscrepancyTypeID) : 0;

                sb.Append("<table width='100%' border='0' cellpadding='3' cellspacing='3' > " +
                "<tr><td valign='top'><div align='left' class='style1'>" + WebCommon.getGlobalResourceValue("DiscrepancyNumber") + " - " + GetQueryStringValue("VDRNo") + " </div></td></tr> " +
                "<tr><td valign='top'><div align='left' class='style1'>" + WebCommon.getGlobalResourceValue("Type") + " - " + this.GetType(discrepancyTypeID) + " </div></td></tr> " +
                "<tr><td>&nbsp;</td></tr> " +
                "<tr><td>" + WebCommon.getGlobalResourceValue("PONumber") + " &nbsp;" + Convert.ToString(lstVendor[0].PurchaseOrder.Purchase_order) + "</td></tr>	" +
                "<tr><td>" + WebCommon.getGlobalResourceValue("DateDiscrepancyRaised") + " &nbsp;" + Convert.ToString(lstVendor[0].DiscrepancyLogDate) + "</td></tr>	" +
                "<tr><td><span class='style10' style='text-align:right;'>Box Number &nbsp;" + iCount + " of " + txtNoofLabel.Text.Trim() + "</span></td></tr>" +
                "<tr><td>&nbsp;</td></tr> " +
                "<tr><td valign='top'><span class='style10'>" + lstVendor[0].Vendor.Vendor_No + " - " + lstVendor[0].Vendor.VendorName + "</span></td> " +
                "</tr> " +
                "<tr><td valign='top'><span class='style10'>" + lstVendor[0].Vendor.address1 + "</span></td> " +
                "</tr> " +
                " <tr><td valign='top'><span class='style10'>" + lstVendor[0].Vendor.address2 + "</span></td> " +
                " </tr> " +
                "  <tr><td valign='top'><span class='style10'>" + lstVendor[0].Vendor.city + "</span></td> " +
                "  </tr> " +
                " <tr><td valign='top'><span class='style10'>" + lstVendor[0].Vendor.county + "</span></td> " +
                " </tr> " +
                " <tr><td valign='top'><span class='style10'>" + lstVendor[0].Vendor.VMPPIN + " " + lstVendor[0].Vendor.VMPPOU + "</span></td> " +
                " </tr> " +
                " <tr><td valign='top'><span class='style10'>" + lstVendor[0].Vendor.VMTCC1 + "</span></td> " +
                "  </tr> " +
                " <tr><td valign='top'><span class='style10'>" + lstVendor[0].Vendor.VMTEL1 + "</span></td> " +
                " </tr> " +
                "<tr><td>&nbsp;</td></tr>" +
                "<tr><td valign='top'><div align='left' class='style1'>" + WebCommon.getGlobalResourceValue("ReturnsAddress") + " </div></td></tr> " +
                " <tr><td valign='top'><span class='style10'>" + ReturnAddress + "</span></td></tr> " +
                @"<tr><td> <br /><br /><br /><br /><br /></td></tr>	" +
                "</table>");

                if (iCount < Convert.ToInt32(txtNoofLabel.Text.Trim()))
                    sb.Append("<p style='page-break-before: always'></p>");
                #region Commented code ...
                //                }
                //                else
                //                {
                //                    // return address
                //                    string ReturnAddress = lstVendor[0].Vendor.ReturnAddress.Replace("\r\n", "</span></td></tr><tr><td valign='top'><span class='style10'>");
                //                    sb.Append("<table width='100%' border='0' cellpadding='3' cellspacing='3' > " +
                //                       "<tr><td valign='top'><div align='center' class='style1'>" + WebCommon.getGlobalResourceValue("DiscrepancyNumber") + " - " + GetQueryStringValue("VDRNo") + " </div></td> " +
                //                       "</tr> " +
                //                       "<tr><td>&nbsp;</td></tr> " +
                //                        "<tr><td valign='top'><span class='style10'>" + lstVendor[0].Vendor.Vendor_No + " - " + lstVendor[0].Vendor.VendorName + "</span></td> " +
                //                     "</tr> " +

                //                        "<tr><td valign='top'><span class='style10'>" + ReturnAddress + "</span></td> " +
                //                        "</tr> " +
                //                        @"<tr><td> <br /><br /><br /><br /><br />
                //                                <br /><br /><br /><br /><br />
                //                                <br /><br /><br /><br /><br />
                //                                <br /><br /><br /><br /><br />
                //                                <br /><br /><br /><br /><br />
                //                                <br /><br /></td></tr>	" +
                //                              "<tr><td><span class='style10' style='text-align:right;'>Box Number &nbsp;" + iCount + " of " + txtNoofLabel.Text.Trim() + "</span></td></tr>	" +
                //                       "</table>");
                //                    if (iCount < Convert.ToInt32(txtNoofLabel.Text.Trim()))
                //                        sb.Append("<p style='page-break-before: always'></p>");
                //                }
                #endregion
                ltlable.Text = sb.ToString();
                //Session["LabelHTML"] = sb.ToString();
                string a = "window.open('DIS_PrintLabel.aspx?RedirectionRequired=no&ltid=" + ltlable.ClientID + "', '', 'left =' + screen.width + ',top=' + screen.height + ',width=0,height=0,toolbar=0,scrollbars=0,status=0')";
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "pp", a, true);
            }
        }
    }

    protected void btnAddComment_Click(object sender, EventArgs e)
    {
        txtUserComments.Text = string.Empty;
        lblErrorMsg.Text = string.Empty;
        lblUserT.Text = Convert.ToString(Session["UserName"]);
        lblDateT.Text = DateTime.Now.Date.ToString("dd/MM/yyyy");
        lblDiscrepancyNoT.Text = lblVDRNo.Text.Replace("-", string.Empty);
        mdlAddComment.Show();
        txtUserComments.Focus();
    }

    protected void btnAddImages_Click(object sender, EventArgs e)
    {
        DiscrepancyBE oNewDiscrepancyBE = new DiscrepancyBE();
        DiscrepancyBAL oNewDiscrepancyBAL = new DiscrepancyBAL();
        oNewDiscrepancyBE.Action = "GetDiscrepancySiteVendorDetail";
        oNewDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID"));

        List<DiscrepancyBE> lstVendor = oNewDiscrepancyBAL.GetDiscrepancyLogSiteVendorDetailsBAL(oNewDiscrepancyBE);
        int discrepancyTypeID = lstVendor[0].DiscrepancyTypeID != null ? Convert.ToInt32(lstVendor[0].DiscrepancyTypeID) : 0;

        EncryptQueryString("DIS_OptionsPopup.aspx?Mode=Edit&VDRNo=" + GetQueryStringValue("VDRNo").ToString() + "&DiscrepancyLogID=" + GetQueryStringValue("disLogID").ToString() + "&CommunicationType=" + GetType(discrepancyTypeID));

    }


    protected void btnSave_Click(object sender, EventArgs e)
    {
        DiscrepancyBE discrepancyBE = null;
        DiscrepancyBAL discrepancyBAL = null;

        if (!string.IsNullOrEmpty(txtUserComments.Text) && !string.IsNullOrWhiteSpace(txtUserComments.Text))
        {
            lblErrorMsg.Text = string.Empty;
            discrepancyBE = new DiscrepancyBE();
            discrepancyBE.Action = "AddDiscrepancyLogComment";

            if (!string.IsNullOrEmpty(GetQueryStringValue("disLogID")))
                discrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID"));
            else
                discrepancyBE.DiscrepancyLogID = 0;

            discrepancyBE.UserID = Convert.ToInt32(Session["UserID"]);
            discrepancyBE.CommentDateTime = DateTime.Now;
            discrepancyBE.Comment = txtUserComments.Text;
            discrepancyBAL = new DiscrepancyBAL();
            discrepancyBAL.AddDiscrepancyLogCommentBAL(discrepancyBE);

            discrepancyBE.Action = "GetDiscrepancyLogComment";
            this.BindDiscrepancyLogComment(discrepancyBE);

            //discrepancyBE.DiscrepancyType = "QualityIssue";
            //int vendorID = 0;
            //if (ViewState["VendorID"] != null)
            //    vendorID = Convert.ToInt32(ViewState["VendorID"]);
            //new SendCommentMail().SendCommentsMail(discrepancyBE, (BaseControlLibrary.ucTextbox)ucSDRCommunication1.FindControl("txtAltEmail"),
            //    (BaseControlLibrary.ucTextbox)ucSDRCommunication1.FindControl("ucVendorEmailList"), vendorID);

            mdlAddComment.Hide();
        }
        else
        {
            lblErrorMsg.Text = WebCommon.getGlobalResourceValue("MustEnterComment");
            mdlAddComment.Show();
        }
        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "Reload()", true);
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {

        mdlAddComment.Hide();
        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "Reload()", true);
    }

    protected void btnBackToPreviousPage_Click(object sender, EventArgs e)
    {
        if (GetQueryStringValue("PN") != null)
        {
            if (GetQueryStringValue("PN").Trim().ToUpper().Equals("POINQ"))
            {
                EncryptQueryString("~/ModuleUI/Appointment/PurchaseOrderInquiry.aspx?SId=" + GetQueryStringValue("SId").ToString()
                 + "&Status=" + GetQueryStringValue("Status").ToString()
                 + "&PN=DISLOG"
                 + "&PO=" + GetQueryStringValue("PO").ToString());
            }
        }
        //else if (GetQueryStringValue("disLogID") != null)
        //{
        //    //For opening Today's Discrepancy Mode
        //    if (GetQueryStringValue("SiteId") != null && GetQueryStringValue("DisDate") != null && GetQueryStringValue("TodayDis") == "TodayDiscrepancy")
        //    {
        //        EncryptQueryString("../DIS_SearchResult.aspx?FromPage=images&SiteId=" + GetQueryStringValue("SiteId") + "&DisDate=" + GetQueryStringValue("DisDate"));
        //    }
        //    else
        //    {
        //        //For opening search discrepancies mode

        //        EncryptQueryString("~/ModuleUI/Discrepancy/DIS_SearchResult.aspx?disLogID=" + Convert.ToString(GetQueryStringValue("disLogID"))
        //                  + "&VDRNo=" + Convert.ToString(GetQueryStringValue("VDRNo"))
        //                  + "&UserID=" + Convert.ToString(GetQueryStringValue("UserID"))
        //                  + "&PreviousPage=SearchResult");
        //    }
        //}

        if (Session["PageMode"].ToString() == "Search")
        {
            //For opening search discrepancies mode

            EncryptQueryString("~/ModuleUI/Discrepancy/DIS_SearchResult.aspx?disLogID=" + Convert.ToString(GetQueryStringValue("disLogID"))
                          + "&VDRNo=" + Convert.ToString(GetQueryStringValue("VDRNo"))
                          + "&UserID=" + Convert.ToString(GetQueryStringValue("UserID"))
                          + "&PreviousPage=SearchResult");

        }
        if (Session["PageMode"].ToString() == "Todays")
        {      //For opening Today's Discrepancy Mode
            if (GetQueryStringValue("SiteId") != null && GetQueryStringValue("DisDate") != null)
            {
                EncryptQueryString("../DIS_SearchResult.aspx?FromPage=images&SiteId=" + GetQueryStringValue("SiteId") + "&DisDate=" + GetQueryStringValue("DisDate"));
            }

        }
    }

    private void BindDiscrepancyLogComment(DiscrepancyBE discrepancyBE = null)
    {
        var discrepancyBAL = new DiscrepancyBAL();
        if (discrepancyBE != null)
        {
            rptDiscrepancyComments.DataSource = discrepancyBAL.GetDiscrepancyLogCommentsBAL(discrepancyBE);
        }
        else
        {
            discrepancyBE = new DiscrepancyBE();
            discrepancyBE.Action = "GetDiscrepancyLogComment";

            if (!string.IsNullOrEmpty(GetQueryStringValue("disLogID")))
                discrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID"));
            else
                discrepancyBE.DiscrepancyLogID = 0;

            rptDiscrepancyComments.DataSource = discrepancyBAL.GetDiscrepancyLogCommentsBAL(discrepancyBE);
        }

        rptDiscrepancyComments.DataBind();

        if (rptDiscrepancyComments.Items.Count > 0)
            lblComments.Visible = true;
        else
            lblComments.Visible = false;       
    }

    private void BindDiscrepancyQuery()
    {
        var queryDiscrepancyBAL = new DISLog_QueryDiscrepancyBAL();
        var queryDiscrepancyBE = new DISLog_QueryDiscrepancyBE();
        var disLogID = GetQueryStringValue("disLogID") != null ? Convert.ToInt32(GetQueryStringValue("disLogID")) : 0;
        queryDiscrepancyBE.Action = "GetQueryDiscrepancy";
        queryDiscrepancyBE.DiscrepancyLogID = disLogID;
        rptDiscrepancyQuery.DataSource = queryDiscrepancyBAL.GetQueryDiscrepancyBAL(queryDiscrepancyBE);
        rptDiscrepancyQuery.DataBind();

        if (rptDiscrepancyQuery.Items.Count > 0)
            lblDiscrepancyQuery.Visible = true;
        else
            lblDiscrepancyQuery.Visible = false;
    }

    protected void rptDiscrepancyQuery_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            // GoodsIn declaration ...            
            var lblCommentLabelT_2 = (ucLabel)e.Item.FindControl("lblCommentLabelT_2");
            if (lblCommentLabelT_2 != null)
            {
                if (!string.IsNullOrEmpty(lblCommentLabelT_2.Text)) { e.Item.FindControl("tblGoodsIn").Visible = true; }
                else { e.Item.FindControl("tblGoodsIn").Visible = false; }

                var lnkView = (System.Web.UI.HtmlControls.HtmlAnchor)e.Item.FindControl("lnkView");
                var hdnQueryDiscrepancyID = (HiddenField)e.Item.FindControl("hdnQueryDiscrepancyID");
                var hdnDiscrepancyLogID = (HiddenField)e.Item.FindControl("hdnDiscrepancyLogID");
                lnkView.Attributes.Add("href", EncryptQuery("~/ModuleUI/Appointment/Receiving/APPRcv_Communication.aspx?DiscId=" + hdnDiscrepancyLogID.Value
                    + "&QueryDiscId=" + hdnQueryDiscrepancyID.Value));
            }
        }
    }

    //private void BindCancelaDebits()
    //{
    //    var discrepancyBE = new DiscrepancyBE();
    //    var discrepancyBAL = new DiscrepancyBAL();

    //    discrepancyBE.Action = "GetAllDebitRaise";
    //    if (!string.IsNullOrEmpty(msDebitType.SelectedDebitTypeIDs))
    //        discrepancyBE.DebitReasonIds = msDebitType.SelectedDebitTypeIDs;

    //    if (!string.IsNullOrEmpty(msVendor.SelectedVendorIDs))
    //        discrepancyBE.VendorIds = msVendor.SelectedVendorIDs;

    //    if (!string.IsNullOrEmpty(msAP.SelectedStockPlannerIDs))
    //        discrepancyBE.CreatedByIds = msAP.SelectedStockPlannerIDs;

    //    discrepancyBE.DebitRaiseFromDate = string.IsNullOrEmpty(txtFromDate.Text) ? (DateTime?)null : Common.GetMM_DD_YYYY(txtFromDate.Text);
    //    discrepancyBE.DebitRaiseToDate = string.IsNullOrEmpty(txtToDate.Text) ? (DateTime?)null : Common.GetMM_DD_YYYY(txtToDate.Text);

    //    if (ddlDebitStatus.SelectedValue.Equals("A") || ddlDebitStatus.SelectedValue.Equals("C"))
    //        discrepancyBE.Status = ddlDebitStatus.SelectedValue;

    //    var lstDiscrepancyBE = discrepancyBAL.GetAllDebitRaiseBAL(discrepancyBE);
    //    gvSearchDebit.DataSource = null;
    //    if (lstDiscrepancyBE != null && lstDiscrepancyBE.Count > 0)
    //    {
    //        gvSearchDebit.DataSource = lstDiscrepancyBE;
    //        ViewState["SearchDebitResult"] = lstDiscrepancyBE;
    //        ucExportToExcel1.Visible = true;
    //    }
    //    gvSearchDebit.DataBind();


    //    pnlDebitLibrary1.Visible = true;
    //    pnlSearchDebit1.Visible = false;
    //}

    private void SetDebitCreditPosting()
    {
        var discrepancyBE = new DiscrepancyBE();
        var discrepancyBAL = new DiscrepancyBAL();
        discrepancyBE.Action = "GetAllDebitRaise";
        if (!string.IsNullOrEmpty(GetQueryStringValue("disLogID")))
            discrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("disLogID"));
        else
            discrepancyBE.DiscrepancyLogID = 0;

        var lstDiscrepancyBE = discrepancyBAL.GetAllDebitRaiseBAL(discrepancyBE);
        if (lstDiscrepancyBE != null && lstDiscrepancyBE.Count > 0)
        {
            pnlDebitCreditPosting.Visible = true;
            if (lstDiscrepancyBE[0].DebitRaiseDate != null)
            {
                DateTime actualDate = Convert.ToDateTime(lstDiscrepancyBE[0].DebitRaiseDate);
                lblDateTimeValue.Text = string.Format("{0} {1}", Common.ToDateTimeInMMDDYYYY(actualDate), actualDate.ToString("hh:mm"));
            }

            /* Section-1 : Debit / Credit Posting */
            lblCreatedByValue.Text = lstDiscrepancyBE[0].CreatedBy;
            lblActionTakenValue.Text = DebitRaisedText;
            lblDNNValue.Text = lstDiscrepancyBE[0].DebitNoteNo;
            lblReasonValue.Text = lstDiscrepancyBE[0].Reason;
            lblValuePostedValue.Text = Convert.ToString(lstDiscrepancyBE[0].StateValueDebited);
            lblCurrencyValue.Text = lstDiscrepancyBE[0].Currency.CurrencyName;
            lblSentToValue.Text = lstDiscrepancyBE[0].CommunicationEmails;

            /* Section-2 : Debit / Credit Posting */
            if (!string.IsNullOrEmpty(lstDiscrepancyBE[0].CancelBy) && !string.IsNullOrWhiteSpace(lstDiscrepancyBE[0].CancelBy))
            {
                lblDebitNoteReferenceValue.Text = ""; //lstDiscrepancyBE[0].DebitNoteNo;
                lblResentByValue.Text = "";
                lblResentOnValue.Text = "";
                lblResentToValue.Text = "";
            }

            /* Section-3 : Debit / Credit Posting */
            if (!string.IsNullOrEmpty(lstDiscrepancyBE[0].CancelBy) && !string.IsNullOrWhiteSpace(lstDiscrepancyBE[0].CancelBy))
            {
                lblCancelledDebitNoteReferenceValue.Text = lstDiscrepancyBE[0].DebitNoteNo;
                lblCancelledByValue.Text = lstDiscrepancyBE[0].CancelBy;
                if (lstDiscrepancyBE[0].CanceledDate != null)
                {
                    DateTime actualCancelDate = Convert.ToDateTime(lstDiscrepancyBE[0].CanceledDate);
                    lblCancelledOnValue.Text = string.Format("{0} {1}", Common.ToDateTimeInMMDDYYYY(actualCancelDate), actualCancelDate.ToString("hh:mm"));
                }
                lblCancelledSentToValue.Text = lstDiscrepancyBE[0].CancelCommunicationEmails;
            }
        }
        else
        {
            pnlDebitCreditPosting.Visible = false;
            lblDateTimeValue.Text = string.Empty;
            lblCreatedByValue.Text = string.Empty;
            lblActionTakenValue.Text = string.Empty;
            lblDNNValue.Text = string.Empty;
            lblReasonValue.Text = string.Empty;
            lblValuePostedValue.Text = string.Empty;
            lblCurrencyValue.Text = string.Empty;
            lblSentToValue.Text = string.Empty;

            lblDebitNoteReferenceValue.Text = string.Empty;
            lblResentByValue.Text = string.Empty;
            lblResentOnValue.Text = string.Empty;
            lblResentToValue.Text = string.Empty;

            lblCancelledDebitNoteReferenceValue.Text = string.Empty;
            lblCancelledByValue.Text = string.Empty;
            lblCancelledOnValue.Text = string.Empty;
            lblCancelledSentToValue.Text = string.Empty;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.Discrepancy.LogDiscrepancy;
using BusinessLogicLayer.ModuleBAL.Discrepancy.LogDiscrepancy;

public partial class ModuleUI_Discrepancy_LogDiscrepancy_Dis_ReturnNote : CommonPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (GetQueryStringValue("ReturnnoteID") != null)
        {
            DiscrepancyReturnNoteBE oDiscrepancyReturnNoteBE = new DiscrepancyReturnNoteBE();
            DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();
            oDiscrepancyReturnNoteBE.Action = "GetDiscrepancyReturnNote";
            oDiscrepancyReturnNoteBE.ReturnNoteID = Convert.ToInt32(GetQueryStringValue("ReturnnoteID"));

            oDiscrepancyReturnNoteBE = oDiscrepancyBAL.GetDiscrepancyReturnNote(oDiscrepancyReturnNoteBE);
            oDiscrepancyBAL = null;
            lblprint.Text = oDiscrepancyReturnNoteBE.ReturnNoteBody;
        }
    }
}
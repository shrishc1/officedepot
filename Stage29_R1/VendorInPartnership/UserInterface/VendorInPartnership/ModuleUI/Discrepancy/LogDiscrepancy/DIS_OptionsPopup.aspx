﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="DIS_OptionsPopup.aspx.cs"
    MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master" Inherits="DIS_OptionsPopup" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
   
    <script type="text/javascript">
        function ConfirmMess() {
            var mess = window.confirm('<%=ConfirmMessage %>');
            if (mess == true) {
            }
            else {
                return false;
            }
        }
        function previewImage(arg) {
            //var path = document.getElementById('<%=hdnApplicationPath.ClientID %>').value + "/Images/Discrepancy/" + arg;
            var path = "../../../Images/Discrepancy/" + arg;
            var myArguments = new Object();
            var answer = window.showModalDialog(path, myArguments, "dialogWidth:700px; dialogHeight:600px; center:yes");
            return false;
        }

        function PrintLabel() {
            //window.open('DIS_PrintLabel.aspx');
            //Creating new page

            //open new window set the height and width =0,set windows position at bottom
            var a = window.open('', '', 'left =' + screen.width + ',top=' + screen.height + ',width=0,height=0,toolbar=0,scrollbars=0,status=0');
            //write gridview data into newly open window

            //major change here get innerHTML Of the Div

            //a.document.write(document.getElementById('innerData').innerHTML);
            a.document.close();
            a.focus();
            //call print
            a.print();
            //                var xcopy = 5;
            //                for (i = 1; i <= xcopy.value; i++) {
            //                    a.print({ bUI: false, bSilent: true, bSchrinkToFit: true });
            //                } 
            a.close();
            return false;
        }
        function IsAmountRequired(arg) {
            if (arg == '1') {
                document.getElementById('<%=txtFreightCharges.ClientID %>').disabled = true;
                document.getElementById('<%=txtFreightCharges.ClientID %>').value = '';
                ValidatorEnable(document.getElementById('<%=rfvCarriageChargeRequired.ClientID %>'), false);
                ValidatorEnable(document.getElementById('<%=cmpvCarriageChargeGreaterThanZero.ClientID %>'), false);
            }
            else if (arg == '2') {
                document.getElementById('<%=txtFreightCharges.ClientID %>').disabled = false;
                ValidatorEnable(document.getElementById('<%=rfvCarriageChargeRequired.ClientID %>'), true);
                ValidatorEnable(document.getElementById('<%=cmpvCarriageChargeGreaterThanZero.ClientID %>'), true);
            }
            setReadonlyClass();
        }
        function RedirectToSearchPage() {
            if (document.getElementById('<%=txtLablesNumber.ClientID %>').value != '') {
                window.location.href = '<%=EncURL%>';
            }
        }

        function ClickRedirectButton() {
            document.getElementById('<%=btnRedirect.ClientID %>').click();
        }

        function CheckImageAdded() {
            var val = document.getElementById('<%=fileImage.ClientID%>').value;
            if (val != "") {
            }
            else {
                alert('Please select an image.');
                return false;
            }
        }      
        
    </script>
    <asp:ScriptManager ID="sp1" runat="server">
    </asp:ScriptManager>
    <h2>
        <cc1:ucLabel ID="lblVDRLabelandImage" runat="server" Text="VDR Label and Image"></cc1:ucLabel>
    </h2>
    <asp:HiddenField ID="hdnApplicationPath" runat="server" />
  
    <div class="right-shadow">
        <div class="formbox">
            <table width="50%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                <tr>
                    <td style="font-weight: bold;  width: 28%">
                        <cc1:ucLabel ID="lblVDRNumberCreated" runat="server" Text="VDR Number Created"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold; width: 2%">
                        :
                    </td>
                    <td style="font-weight: bold; width: 60%">
                        <cc1:ucLiteral ID="ltVDRNumberCreated" runat="server"></cc1:ucLiteral>
                    </td>
                </tr>
                <tr>
                    <td style="font-weight: bold;">
                        <cc1:ucLabel ID="lblNumberofLabels" runat="server" Text="Number of Labels"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold;">
                        :
                    </td>
                    <td style="font-weight: bold;">
                        <cc1:ucTextbox ID="txtLablesNumber" runat="server" Width="30px"></cc1:ucTextbox>
                    </td>
                </tr>
                <tr>
                    <td style="font-weight: bold;">
                        <cc1:ucLabel ID="lblImagetobeattached" runat="server" Text="Image to be attached"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold;">
                        :
                    </td>
                    <td style="font-weight: bold;">                      
                        <input id="fileImage" runat="server" type="file" multiple="multiple" />
                        &nbsp;
                        <cc1:ucButton ID="btnAdd" runat="server" Text="Add" CssClass="button" OnClientClick="return CheckImageAdded();"
                            OnClick="btnAddImage_Click" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        &nbsp;
                    </td>
                    <td align="right">
                        <table width="100%" cellpadding="5" cellspacing="0" class="form-tableGrid">
                            <tr>
                                <th>
                                    <cc1:ucLabel runat="server" ID="lblTaggedImages" Text="Tagged Images" />
                                    <%--Tagged Images--%>
                                </th>
                                <th>
                                    <cc1:ucLabel runat="server" ID="lblRemove" Text="Remove" />
                                    <%--Remove--%>
                                </th>
                            </tr>
                            <asp:Repeater ID="rpImages" runat="server">
                                <ItemTemplate>
                                    <tr>
                                        <td>
                                            <a onclick='<%# string.Format("previewImage(\"{0}\");", Eval("ImageName"))%>' id="hylImageName"
                                                runat="server" style="cursor: pointer; text-decoration: underline; color: Blue">
                                                <%#Eval("ImageName") %></a>
                                            <asp:HiddenField ID="hdnImageID" runat="server" Value='<%#Eval("ImageID") %>' />
                                            <asp:HiddenField ID="hdnDiscrepancyLogImageID" runat="server" Value='<%#Eval("DiscrepancyLogImageID") %>' />
                                        </td>
                                        <td>
                                            <cc1:ucButton ID="RemoveImage" runat="server" Text='<%#Eval("Button") %>' CommandArgument='<%#Eval("ImageID") %>'
                                                CommandName="Remove" OnCommand="RemoveImage_Click" CssClass="button" OnClientClick="return ConfirmMess();" />
                                        </td>
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        &nbsp;
                    </td>
                </tr>
            </table>
            <br />
            <br />
            <cc1:ucPanel ID="pnlDiscrepancyCharges" GroupingText="Discrepancy Charges" runat="server"
                CssClass="fieldset-form" Width="100%">
                <table width="100%" cellspacing="5" cellpadding="0" border="0" class="form-table"
                    align="left">
                    <tr id="trDiscrepancyCharges" runat="server">
                        <td style="font-weight: bold; width: 20%">
                            <cc1:ucRadioButton ID="rdoFreeOfCharge" runat="server" GroupName="DiscrepancyCharger"
                                onclick="javascript:return IsAmountRequired(1);" />
                            &nbsp;<cc1:ucLabel ID="UcLabel1" runat="server" Text="Free of Charge"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold; width: 20%">
                            &nbsp;
                        </td>
                    </tr>
                    <tr id="trAmount" runat="server">
                        <td style="font-weight: bold; width: 20%">
                            <cc1:ucRadioButton ID="rdoOthers" runat="server" onclick="javascript:return IsAmountRequired(2);"
                                GroupName="DiscrepancyCharger" />
                            &nbsp;<cc1:ucLabel ID="lblOthers" runat="server" Text="Others"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;" colspan="5" valign="top">
                            <cc1:ucLabel ID="lblAmount" runat="server" Text="Amount"></cc1:ucLabel>
                            &nbsp; : &nbsp;
                            <cc1:ucTextbox ID="txtFreightCharges" onkeyup="AllowDecimalOnly(this)" runat="server"
                                Width="30px"></cc1:ucTextbox>
                            <asp:RequiredFieldValidator ID="rfvCarriageChargeRequired" runat="server" ControlToValidate="txtFreightCharges"
                                Display="None" ValidationGroup="a" ErrorMessage="Please enter the carrier charge.">
                            </asp:RequiredFieldValidator>
                            <asp:CompareValidator ID="cmpvCarriageChargeGreaterThanZero" runat="server" ControlToValidate="txtFreightCharges"
                                Operator="GreaterThan" Type="Double" Display="None" ValidationGroup="a">
                            </asp:CompareValidator>
                            <%--<asp:RegularExpressionValidator ID="revCarriageChargeCorrectFormatRequired" runat="server"
                                ControlToValidate="txtFreightCharges" Display="None" ValidationGroup="a" SetFocusOnError="true"
                                ErrorMessage="Please enter carriage charge upto 2 decimal digits." ValidationExpression="^\d+(\.\d{0,2})?$">
                            </asp:RegularExpressionValidator>--%>
                            <asp:RegularExpressionValidator ID="revCarriageChargeCorrectFormatRequired" runat="server"
                                ControlToValidate="txtFreightCharges" Display="None" ValidationGroup="a" SetFocusOnError="true"
                                ErrorMessage="Please enter quality issue charges upto 2 decimal digits." ValidationExpression="^\d{0,8}(\.\d{0,2})?$" />
                        </td>
                    </tr>
                </table>
            </cc1:ucPanel>
        </div>
    </div>
    <div class="button-row">
        <cc1:ucButton ID="btnOK" runat="server" Text="OK" ValidationGroup="a" CssClass="button"
            OnClick="btnOK_Click" />
        <cc1:ucButton ID="btnRedirect" runat="server" OnClientClick="RedirectToSearchPage();"
            Style="display: none;" />
        <asp:ValidationSummary ID="vs1" runat="server" ValidationGroup="a" ShowMessageBox="true"
            ShowSummary="false" />
    </div>
    <div id="innerData" style="display: none;">
        <cc1:ucLabel ID="ltlable" runat="server"></cc1:ucLabel>
    </div>
    <div style="display: none;">
        <rsweb:ReportViewer ID="ReportViewer1" runat="server" Width="990px" DocumentMapCollapsed="True">
        </rsweb:ReportViewer>
    </div>
</asp:Content>

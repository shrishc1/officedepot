﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    CodeFile="DIS_InvoiceImagePopup.aspx.cs" Inherits="DIS_InvoiceImagePopup" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript">
        function ConfirmMess() {
            var mess = window.confirm('<%=ConfirmMessage %>');
            if (mess == true) {
            }
            else {
                return false;
            }
        }
        function previewImage(arg) {
            //var path = document.getElementById('<%=hdnApplicationPath.ClientID %>').value + "/Images/Discrepancy/" + arg;
            var path = "../../../Images/Discrepancy/" + arg;
            var myArguments = new Object();
            var answer = window.showModalDialog(path, myArguments, "dialogWidth:700px; dialogHeight:600px; center:yes");
            return false;
        }
       
        

        function ClickRedirectButton() {
            document.getElementById('<%=btnRedirect.ClientID %>').click();
        }

        function CheckImageAdded() {
            var val = document.getElementById('<%=fileImage.ClientID%>').value;
            if (val != "") {
            }
            else {
                alert('<%=PleaseattachPDF %>');
                return false;
            }
        }      
        
    </script>
    <asp:ScriptManager ID="sp1" runat="server">
    </asp:ScriptManager>
    <h2>
        <cc1:ucLabel ID="lblPODAttachment" runat="server" Text="POD Attachment"></cc1:ucLabel>
    </h2>
    <asp:HiddenField ID="hdnApplicationPath" runat="server" />
    <div class="right-shadow">
        <div class="formbox">
            <table width="60%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                <tr>
                    <td style="font-weight: bold; width: 28%">
                        <cc1:ucLabel ID="lblVDRNumberCreated" runat="server" Text="VDR Number Created"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold; width: 2%">
                        :
                    </td>
                    <td style="font-weight: bold; width: 60%">
                        <cc1:ucLiteral ID="ltVDRNumberCreated" runat="server"></cc1:ucLiteral>
                    </td>
                </tr>
                <tr>
                    <td style="font-weight: bold;">
                        <cc1:ucLabel ID="lblDocumenttobeattached" runat="server" Text="Document to be attached"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold;">
                        :
                    </td>
                    <td style="font-weight: bold;">
                         <input id="fileImage" runat="server" type="file" multiple="multiple" />
                        &nbsp;
                        <cc1:ucButton ID="btnAdd" runat="server" Text="Add" CssClass="button" OnClientClick="return CheckImageAdded();"
                            OnClick="btnAddImage_Click" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        &nbsp;
                    </td>
                    <td align="right">
                        <table width="100%" cellpadding="5" cellspacing="0" class="form-tableGrid">
                            <tr>
                                <th align="center">
                                    <cc1:ucLabel runat="server" ID="lblAttachedFiles" Text="Attached Files" />
                                    <%--Tagged Images--%>
                                </th>
                                <th align="center">
                                    <cc1:ucLabel runat="server" ID="lblRemove" Text="Remove" />
                                    <%--Remove--%>
                                </th>
                            </tr>
                            <asp:Repeater ID="rpImages" runat="server">
                                <ItemTemplate>
                                    <tr>
                                        <td>
                                            <a onclick='<%# string.Format("previewImage(\"{0}\");", Eval("ImageName"))%>' id="hylImageName"
                                                runat="server" style="cursor: pointer; text-decoration: underline; color: Blue">
                                                <%#Eval("ImageName") %></a>
                                            <asp:HiddenField ID="hdnImageID" runat="server" Value='<%#Eval("ImageID") %>' />
                                            <asp:HiddenField ID="hdnDiscrepancyLogImageID" runat="server" Value='<%#Eval("DiscrepancyLogImageID") %>' />
                                        </td>
                                        <td>
                                            <cc1:ucButton ID="RemoveImage" runat="server" Text='<%#Eval("Button") %>' CommandArgument='<%#Eval("ImageID") %>'
                                                CommandName="Remove" OnCommand="RemoveImage_Click" CssClass="button" OnClientClick="return ConfirmMess();" />
                                        </td>
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        &nbsp;
                    </td>
                </tr>
            </table>
            <br />
            <br />
        </div>
    </div>
    <div class="button-row">
        <cc1:ucButton ID="btnOK" runat="server" Text="OK" ValidationGroup="a" CssClass="button"
            OnClick="btnOK_Click" />
        <cc1:ucButton ID="btnRedirect" runat="server"
            Style="display: none;" />
    </div>
</asp:Content>

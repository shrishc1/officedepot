﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.AdminFunctions;
using BusinessLogicLayer.ModuleBAL.AdminFunctions;
using BusinessEntities.ModuleBE.Discrepancy.LogDiscrepancy;
using BusinessLogicLayer.ModuleBAL.Discrepancy.LogDiscrepancy;
using System.Data;
using System.Configuration;

public partial class DIS_ShowImages : CommonPage
{    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (GetQueryStringValue("DiscrepancyLogID") != null)
            {
                DataSet dsImages = getDiscrepancyImages();
                if (dsImages != null && dsImages.Tables.Count > 0 && dsImages.Tables[0].Rows.Count > 0)
                {
                    gvImages.DataSource = dsImages.Tables[0];
                    gvImages.DataBind();
                }
                else
                    ShowErrorMessage();
            }
            else
                ShowErrorMessage();
        }
    }
    private void ShowErrorMessage()
    {
        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ShowNoImage", "alert('" + WebUtilities.WebCommon.getGlobalResourceValue("NoImagesToShow") + "');", true);
    }
    public DataSet getDiscrepancyImages()
    {
        DiscrepancyBE oNewDiscrepancyBE = new DiscrepancyBE();
        DiscrepancyBAL oNewDiscrepancyBAL = new DiscrepancyBAL();
        oNewDiscrepancyBE.Action = "GetDiscrepancyImages";
        oNewDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(GetQueryStringValue("DiscrepancyLogID"));
        DataSet ds= oNewDiscrepancyBAL.getDiscrepancyImagesBAL(oNewDiscrepancyBE);
        oNewDiscrepancyBAL = null;
        return ds;
    }


    protected void gvImages_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType != DataControlRowType.Header && e.Row.RowType != DataControlRowType.Footer)
        {
            System.Web.UI.WebControls.Image img = (Image)e.Row.FindControl("img");
            img.ImageUrl = "~/images/discrepancy/" + img.ImageUrl;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.Discrepancy.LogDiscrepancy;
using BusinessLogicLayer.ModuleBAL.Discrepancy.LogDiscrepancy;
using System.Data;
using Utilities; using WebUtilities;


public partial class ModuleUI_Discrepancy_LogDiscrepancy_DISLog_Letter : CommonPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (GetQueryStringValue("DiscrepancyLetterID") != null)
        {
            string letterBody=getDiscrepancyLetterDetail();
            if (!string.IsNullOrEmpty(letterBody))
            {
                sendCommunicationCommon oSendCommunicationCommon = new sendCommunicationCommon();
                letterBody = letterBody.Replace("{logoInnerPath}", oSendCommunicationCommon.getAbsolutePath());
                divDiscrepancyLetter.InnerHtml = letterBody;
            }
            else
            {
                showErrorMessage();
            }
        }
    }
    public void showErrorMessage()
    {
        string errorMessage = WebCommon.getGlobalResourceValue("NoLetterFound");
        if (string.IsNullOrEmpty(errorMessage))
            errorMessage = "No letter found";
        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('" + errorMessage + "')", true);        
    }

    private string getDiscrepancyLetterDetail()
    { 
        DiscrepancyMailBE oNewDiscrepancyBE = new DiscrepancyMailBE();
        DiscrepancyBAL oNewDiscrepancyBAL = new DiscrepancyBAL();
        oNewDiscrepancyBE.Action = "GetDiscrepancyLetter";
        oNewDiscrepancyBE.DiscrepancyLetterID=GetQueryStringValue("DiscrepancyLetterID");
        DataSet dsLetter =oNewDiscrepancyBAL.getDiscrepancyLetterBAL(oNewDiscrepancyBE);
        oNewDiscrepancyBAL = null;
        if (dsLetter != null && dsLetter.Tables.Count > 0 && dsLetter.Tables[0].Rows.Count > 0)
        {
            return Convert.ToString(dsLetter.Tables[0].Rows[0]["LetterBody"]);
        }
        else
            return null;
    }
}
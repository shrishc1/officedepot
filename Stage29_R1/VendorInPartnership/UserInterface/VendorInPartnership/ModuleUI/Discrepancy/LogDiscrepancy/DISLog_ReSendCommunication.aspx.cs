﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.AdminFunctions;
using BusinessLogicLayer.ModuleBAL.AdminFunctions;
using BusinessEntities.ModuleBE.Discrepancy.LogDiscrepancy;
using BusinessLogicLayer.ModuleBAL.Discrepancy.LogDiscrepancy;
using System.Data;
using System.Configuration;
using Utilities;
using BusinessLogicLayer.ModuleBAL.Appointment.Booking;
using BusinessEntities.ModuleBE.Languages.Languages;
using System.Text;

public partial class DISLog_ReSendCommunication : CommonPage
{
    #region Declarations ...

    string sFromAddress = Convert.ToString(ConfigurationManager.AppSettings["fromAddress"]);
    sendCommunicationCommon oSendCommunicationCommon = new sendCommunicationCommon();
    StringBuilder sSendMailLink = new StringBuilder();

    #endregion

    #region Events ...

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            InitLanguage();
            btnReSendCommunication.Visible = false;
            lblEmail.Visible = false;
            txtEmail.Visible = false;

            if (GetQueryStringValue("CommTitle") != null && GetQueryStringValue("DebitRaiseId") != null)
            {
                //DebitCancel, DebitRaised
                var CommTitle = GetQueryStringValue("CommTitle");
                ViewState["CommunicationLevel"] = GetQueryStringValue("CommTitle");
                var DebitRaiseId = GetQueryStringValue("DebitRaiseId");
                var discrepancyBAL = new DiscrepancyBAL();
                var discrepancyMailBE = new DiscrepancyMailBE();
                discrepancyMailBE.Action = "GetDebitCommunication";
                discrepancyMailBE.DebitRaiseId = Convert.ToInt32(DebitRaiseId);
                discrepancyMailBE.CommTitle = CommTitle;

                if (GetQueryStringValue("LanguageID") != null)
                    discrepancyMailBE.LanguageId = Convert.ToInt32(GetQueryStringValue("LanguageID"));

                var lstDebitCommunication = discrepancyBAL.GetDebitCommunicationBAL(discrepancyMailBE);
                if (lstDebitCommunication != null && lstDebitCommunication.Count > 0)
                {
                    if (drpLanguageId.Items.FindByValue(Convert.ToString(lstDebitCommunication[0].LanguageId)) != null)
                        drpLanguageId.Items.FindByValue(Convert.ToString(lstDebitCommunication[0].LanguageId)).Selected = true;

                    divReSendCommunication.InnerHtml = lstDebitCommunication[0].mailBody.Replace("{logoInnerPath}", oSendCommunicationCommon.getAbsolutePath());

                    ViewState["EmailSubject"] = lstDebitCommunication[0].mailSubject;
                    hdSubject.Value = lstDebitCommunication[0].mailSubject;
                    txtEmail.Text = lstDebitCommunication[0].sentTo;
                    btnReSendCommunication.Visible = true;
                    lblEmail.Visible = true;
                    txtEmail.Visible = true;
                }
                else { this.ShowErrorMessage(); }
            }
            else
            {
                if (GetQueryStringValue("CommunicationLevel") != null)
                    ViewState["CommunicationLevel"] = GetQueryStringValue("CommunicationLevel");
                else if (Request.QueryString["CommunicationLevel"] != null)
                    ViewState["CommunicationLevel"] = GetQueryStringValue(Request.QueryString["CommunicationLevel"]);

                int iCommId = 0;
                if (GetQueryStringValue("communicationid") != null)
                {
                    iCommId = Convert.ToInt32(GetQueryStringValue("communicationid"));
                }
                else if (Request.QueryString["communicationid"] != null)
                {
                    iCommId = Convert.ToInt32(Request.QueryString["communicationid"]);
                }

                if (iCommId != 0)
                {
                    DataSet dsCommunication = getCommunicationDetails(iCommId);

                    if (dsCommunication != null && dsCommunication.Tables.Count > 0 && dsCommunication.Tables[0].Rows.Count > 0)
                    {
                        try
                        {
                            if (drpLanguageId.Items.FindByValue(dsCommunication.Tables[0].Rows[0]["LanguageID"].ToString()) != null)
                                drpLanguageId.Items.FindByValue(dsCommunication.Tables[0].Rows[0]["LanguageID"].ToString()).Selected = true;
                        }
                        catch
                        {
                            //ShowErrorMessage();
                        }
                        divReSendCommunication.InnerHtml = Convert.ToString(dsCommunication.Tables[0].Rows[0]["body"]);
                        hdSubject.Value = Convert.ToString(dsCommunication.Tables[0].Rows[0]["Subject"]);
                        txtEmail.Text = distictEmails(Convert.ToString(dsCommunication.Tables[0].Rows[0]["SentTo"]));
                        btnReSendCommunication.Visible = true;
                        lblEmail.Visible = true;
                        txtEmail.Visible = true;
                        Session["dsCommunication"] = dsCommunication;
                    }
                    else { ShowErrorMessage(); }
                }
                else { ShowErrorMessage(); }
            }
        }
    }

    protected void btnReSendCommunication_Click(object sender, EventArgs e)
    {
        if (GetQueryStringValue("CommTitle") != null && GetQueryStringValue("DebitRaiseId") != null)
        {

            var CommTitle = GetQueryStringValue("CommTitle");
            //ViewState["CommunicationLevel"] = GetQueryStringValue("CommTitle");
            var DebitRaiseId = Convert.ToInt32(GetQueryStringValue("DebitRaiseId"));
            var discrepancyBAL = new DiscrepancyBAL();
            var discrepancyMailBE = new DiscrepancyMailBE();
            discrepancyMailBE.Action = "AddDebitRaiseCommunication";
            discrepancyMailBE.DebitRaiseId = DebitRaiseId;
            discrepancyMailBE.mailSubject = Convert.ToString(ViewState["EmailSubject"]); //hdSubject.Value;
            if (discrepancyMailBE.mailSubject.Contains("Cancel"))
                discrepancyMailBE.CommTitle = "ReSentCancelDebit";
            else
                discrepancyMailBE.CommTitle = "ReSentDebit";

            discrepancyMailBE.sentTo = txtEmail.Text;
            discrepancyMailBE.mailBody = divReSendCommunication.InnerHtml; //.Replace("{logoInnerPath}", oSendCommunicationCommon.getAbsolutePath());      
            discrepancyMailBE.SendByID = Convert.ToInt32(Session["UserID"]);
            discrepancyMailBE.IsMailSent = true;
            discrepancyMailBE.languageID = Convert.ToInt32(drpLanguageId.SelectedValue);
            discrepancyMailBE.MailSentInLanguage = true;

            var emailIds = txtEmail.Text.Replace(';', ',').TrimEnd(',').Trim();
            string[] sMailAddress = emailIds.Split(',');
            var sentToWithLink = new System.Text.StringBuilder();
            for (int index = 0; index < sMailAddress.Length; index++)
            {
                sentToWithLink.Append("<a target='_blank' href='" + Common.EncryptQuery("../LogDiscrepancy/DISLog_ReSendCommunication.aspx?CommTitle=" + discrepancyMailBE.CommTitle 
                    + "&DebitRaiseId=" + DebitRaiseId + "&LanguageID=" + Convert.ToInt32(drpLanguageId.SelectedValue) 
                    + "&CommunicationType=ReSentDebit") + "' onclick='window.open(this.href, this.target,\"scrollbars=1,left=100px,top=100px,width=1000px,height=800px\"); return false;'>" + sMailAddress[index] + "</a>" + System.Environment.NewLine);
            }
            discrepancyMailBE.SentToWithLink = sentToWithLink.ToString();
            var debitReSentCommId = discrepancyBAL.addDebitRaiseCommunicationBAL(discrepancyMailBE);
            if (debitReSentCommId > 0)
            {
                var emailToAddress = txtEmail.Text;
                var emailFromAddress = Convert.ToString(ConfigurationManager.AppSettings["fromAddress"]);
                var emailToSubject = hdSubject.Value;
                var emailBody = divReSendCommunication.InnerHtml;
                Utilities.clsEmail oclsEmail = new Utilities.clsEmail();
                oclsEmail.sendMail(emailToAddress, emailBody, emailToSubject, emailFromAddress, true);

                Response.Write("<script language='javascript'>window.open('', '_self', '');window.close();</script>");
                //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "MailToReSend", "window.opener.location.reload();window.close();", true);
            }
        }
        else
        {
            if (Session["dsCommunication"] != null)
            {
                int? retValue = null;
                DataSet dsCommunication = (DataSet)Session["dsCommunication"];
                sendCommunicationCommon oSendCommunicationCommon = new sendCommunicationCommon();
                string sendTo = txtEmail.Text;
                if (string.IsNullOrEmpty(sendTo))
                    sendTo = Convert.ToString(GetQueryStringValue("emailaddress"));

                clsEmail oclsEmail = new clsEmail();
                oclsEmail.sendMail(sendTo, divReSendCommunication.InnerHtml, Convert.ToString(dsCommunication.Tables[0].Rows[0]["subject"]), sFromAddress, true);
                bool IsFromQuery = false;
                if (!string.IsNullOrEmpty(GetQueryStringValue("IsFromQuery")) && GetQueryStringValue("IsFromQuery")=="true")
                {
                    IsFromQuery = true;
                }
                retValue = oSendCommunicationCommon.saveMail(Convert.ToInt32(dsCommunication.Tables[0].Rows[0]["DiscrepancyLogID"]), txtEmail.Text,
                    Convert.ToString(dsCommunication.Tables[0].Rows[0]["Body"]),
                    Convert.ToString(dsCommunication.Tables[0].Rows[0]["Subject"]),
                    Convert.ToInt32(dsCommunication.Tables[0].Rows[0]["languageid"]),
                    String.Format("{0}Resent", Convert.ToString(dsCommunication.Tables[0].Rows[0]["CommunicationLevel"])), true, IsFromQuery:IsFromQuery);
                if (retValue != null)
                {
                    sSendMailLink.Append("<a target='_blank' href='" + Common.EncryptQuery("DISLog_ReSendCommunication.aspx?communicationid=" + retValue.ToString()
                        + "&CommunicationLevel=" + String.Format("{0}Resent", Convert.ToString(dsCommunication.Tables[0].Rows[0]["CommunicationLevel"])))
                        + "' onclick='window.open(this.href, this.target,\"scrollbars=1,left=100px,top=100px,width=1000px,height=800px\"); return false;'>"
                        + sendTo + "</a>" + System.Environment.NewLine);
                }
                else
                {
                    sSendMailLink.Append("<a target='_blank' href='" + Common.EncryptQuery("DISLog_ReSendCommunication.aspx?communicationid=") + "' onclick='window.open(this.href, this.target,\"scrollbars=1,left=100px,top=100px,width=1000px,height=800px\"); return false;'>" + sendTo + "</a>" + System.Environment.NewLine);
                    oSendCommunicationCommon.errorLog(Convert.ToInt32(dsCommunication.Tables[0].Rows[0]["DiscrepancyLogID"]), Convert.ToString(dsCommunication.Tables[0].Rows[0]["Subject"]), txtEmail.Text, Convert.ToString(dsCommunication.Tables[0].Rows[0]["Body"]), "Error in save mail to database");
                }

                insertHtml(Convert.ToInt32(dsCommunication.Tables[0].Rows[0]["DiscrepancyLogID"]), sSendMailLink.ToString());
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "MailToReSend", "window.opener.location.reload();window.close();", true);
            }
            else { ShowErrorMessage(); }
        }
    }

    protected void drpLanguageId_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (GetQueryStringValue("CommTitle") != null && GetQueryStringValue("DebitRaiseId") != null)
        {
            var CommTitle = GetQueryStringValue("CommTitle");
            ViewState["CommunicationLevel"] = GetQueryStringValue("CommTitle");
            var DebitRaiseId = GetQueryStringValue("DebitRaiseId");
            var discrepancyBAL = new DiscrepancyBAL();
            var discrepancyMailBE = new DiscrepancyMailBE();
            discrepancyMailBE.Action = "GetDebitCommunicationWithLanguageId";
            discrepancyMailBE.DebitRaiseId = Convert.ToInt32(DebitRaiseId);

            if (CommTitle.Equals("DebitCancel"))
                discrepancyMailBE.CommTitle = "DebitCancel";
            else
                discrepancyMailBE.CommTitle = "DebitRaised";

            discrepancyMailBE.LanguageId = Convert.ToInt32(drpLanguageId.SelectedValue);
            var lstDebitCommunication = discrepancyBAL.GetDebitCommunicationBAL(discrepancyMailBE);
            if (lstDebitCommunication != null && lstDebitCommunication.Count > 0)
            {
                divReSendCommunication.InnerHtml = lstDebitCommunication[0].mailBody.Replace("{logoInnerPath}", oSendCommunicationCommon.getAbsolutePath());
                hdSubject.Value = Convert.ToString(ViewState["EmailSubject"]); //lstDebitCommunication[0].mailSubject;
                txtEmail.Text = lstDebitCommunication[0].sentTo;
            }
            else
            {
                this.ShowErrorMessage();
            }
        }
        else
        {
            int iCommId = 0;
            string CommunicationLevel = "communication1";//by defalut
            if (GetQueryStringValue("communicationid") != null)
            {
                iCommId = Convert.ToInt32(GetQueryStringValue("communicationid"));
            }
            else if (Request.QueryString["communicationid"] != null)
            {
                iCommId = Convert.ToInt32(Request.QueryString["communicationid"]);
            }

            if (ViewState["CommunicationLevel"] != null)
                CommunicationLevel = Convert.ToString(ViewState["CommunicationLevel"]).Replace("Resent", string.Empty);
            else if (GetQueryStringValue("CommunicationLevel") != null)
                CommunicationLevel = Convert.ToString(GetQueryStringValue("CommunicationLevel")).Replace("Resent", string.Empty);

            if (iCommId != 0)
            {
                try
                {
                    DataSet dsCommunication = getCommunicationDetails(iCommId, Convert.ToInt32(drpLanguageId.SelectedValue), CommunicationLevel, txtEmail.Text);
                    Session["dsCommunication"] = dsCommunication;
                    hdSubject.Value = Convert.ToString(dsCommunication.Tables[0].Rows[0]["Subject"]);
                    divReSendCommunication.InnerHtml = Convert.ToString(dsCommunication.Tables[0].Rows[0]["body"]);
                }
                catch
                {

                    ShowErrorMessage();
                }
            }
        }
    }

    #endregion

    #region Methods ...

    public string distictEmails(string Emails)
    {
        string[] arrEMails = Emails.Split(new char[] { ',' });

        List<string> lstEmails = new List<string>();
        foreach (string email in arrEMails)
        {
            if (!string.IsNullOrEmpty(email) && !string.IsNullOrWhiteSpace(email) && !lstEmails.Contains(email.Trim()))
                lstEmails.Add(email.Trim());
        }
        return string.Join(",", lstEmails.ToArray());
    }

    private void ShowErrorMessage()
    {
        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "showNoMailToReSend", "alert('" + WebUtilities.WebCommon.getGlobalResourceValue("NoMailToResend") + "');", true);
    }

    public DataSet getCommunicationDetails(int iDiscrepancyCommunicaitonID)
    {
        DiscrepancyMailBE oDiscrepancyMailBE = new DiscrepancyMailBE();
        oDiscrepancyMailBE.Action = "getDiscrepancyCommunciationDetail"; // returns one record by communicationid
        oDiscrepancyMailBE.discrepancyCommunicationID = iDiscrepancyCommunicaitonID;
        DiscrepancyBAL oNewDiscrepancyBAL = new DiscrepancyBAL();
        DataSet ds = oNewDiscrepancyBAL.getDiscrepancyCommunicationBAL(oDiscrepancyMailBE);
        oNewDiscrepancyBAL = null;
        return ds;
    }

    public DataSet getCommunicationDetails(int iDiscrepancyCommunicaitonID, int LanguageID, string CommunicationLevel, string SentTo)
    {
        DiscrepancyMailBE oDiscrepancyMailBE = new DiscrepancyMailBE();
        oDiscrepancyMailBE.Action = "getDiscrepancyCommunciationDetailWithLanguageID";
        oDiscrepancyMailBE.discrepancyCommunicationID = iDiscrepancyCommunicaitonID;
        oDiscrepancyMailBE.languageID = LanguageID;
        oDiscrepancyMailBE.communicationLevel = CommunicationLevel;
        oDiscrepancyMailBE.sentTo = SentTo;

        DiscrepancyBAL oNewDiscrepancyBAL = new DiscrepancyBAL();
        DataSet ds = oNewDiscrepancyBAL.getDiscrepancyCommunicationItemBAL(oDiscrepancyMailBE);
        oNewDiscrepancyBAL = null;
        return ds;
    }

    public void insertHtml(int DiscrepancyLogID, string EmailLink)
    {
        DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
        DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();
        WorkflowHTML oWorkflowHTML = new WorkflowHTML();

        oDiscrepancyBE.GINHTML = oWorkflowHTML.function3("white", null, "", "");
        oDiscrepancyBE.GINActionRequired = false;

        oDiscrepancyBE.INVHTML = oWorkflowHTML.function3("white", null, "", "");
        oDiscrepancyBE.INVActionRequired = false;

        oDiscrepancyBE.APHTML = oWorkflowHTML.function3("white", null, "", "");
        oDiscrepancyBE.APActionRequired = false;

        oDiscrepancyBE.VENHTML = oWorkflowHTML.function4("Blue", null, null, "", null, "Letter resent by " + Session["UserName"].ToString() + " to " + EmailLink + " on " + System.DateTime.Now.ToString("dd/MM/yyyy"), "", "", "", null, "");
        oDiscrepancyBE.VenActionRequired = false;

        oDiscrepancyBE.Action = "InsertHTML";
        oDiscrepancyBE.DiscrepancyLogID = DiscrepancyLogID;
        oDiscrepancyBE.LoggedDateTime = DateTime.Now;
        oDiscrepancyBE.LevelNumber = 1;
        int? iResult1 = oDiscrepancyBAL.addWorkFlowHTMLsBAL(oDiscrepancyBE);
        oDiscrepancyBAL = null;
    }

    private void InitLanguage()
    {
        APPBOK_CommunicationBAL oAPPBOK_CommunicationBAL = new APPBOK_CommunicationBAL();
        List<MAS_LanguageBE> oLanguages = oAPPBOK_CommunicationBAL.GetLanguages();
        drpLanguageId.DataSource = oLanguages;
        drpLanguageId.DataValueField = "LanguageID";
        drpLanguageId.DataTextField = "Language";
        drpLanguageId.DataBind();
    }

    #endregion
}
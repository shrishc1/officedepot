﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebUtilities;
using BusinessEntities.ModuleBE.Discrepancy.LogDiscrepancy;
using BusinessLogicLayer.ModuleBAL.Discrepancy.LogDiscrepancy;

public partial class DIS_WorklistEdit : CommonPage 
{
    protected void Page_Load(object sender, EventArgs e)
    {       
        if (!IsPostBack)
        {
            if (Session["Role"] != null)
                lblDepartmentT.Text = Convert.ToString(Session["Role"]);

            BindGrid();
        }
        ucExportToExcel1.CurrentPage = this;
        ucExportToExcel1.GridViewControl = gvDisLog;
        ucExportToExcel1.FileName = "Worklist";
    }

    #region Methods

    private void BindGrid()
    {
        DiscrepancyBE oNewDiscrepancyBE = new DiscrepancyBE();
        DiscrepancyBAL oNewDiscrepancyBAL = new DiscrepancyBAL();   
        oNewDiscrepancyBE.Action = "GetDiscrepancyWorkListForCurrentUser";
        oNewDiscrepancyBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
        if (GetQueryStringValue("GreaterThan3DaysFlag") != null )
        {
            oNewDiscrepancyBE.GreaterThan3DaysFlag = Convert.ToChar(GetQueryStringValue("GreaterThan3DaysFlag"));
        }
        
        oNewDiscrepancyBE.User.UserID = Convert.ToInt32(Session["UserID"]);
        oNewDiscrepancyBE.User.RoleName = Session["Role"].ToString();

        List<DiscrepancyBE> lstDisLog = oNewDiscrepancyBAL.GetDiscrepancyWorkListForCurrentUserBAL(oNewDiscrepancyBE);
        if (lstDisLog != null && lstDisLog.Count > 0)
        {
            gvDisLog.Visible = true;
            lblRecordNotFoundErrMess.Text = string.Empty;
            tblDisLogHeader.Visible = true;
            gvDisLog.DataSource = lstDisLog;
            gvDisLog.DataBind();
            ViewState["lstSites"] = lstDisLog;
            ucExportToExcel1.Visible = true;
        }
        else
        {
            ucExportToExcel1.Visible = false;
            gvDisLog.Visible = false;
            lblRecordNotFoundErrMess.Text = WebCommon.getGlobalResourceValue("RecordNotFound");
            tblDisLogHeader.Visible = false;
        }
        oNewDiscrepancyBAL = null;
    }

    #endregion

    protected void gvDisLog_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType != DataControlRowType.Header
            && e.Row.RowType != DataControlRowType.Footer)
        {
            //DateTime dDiscrepancyLogDate = Convert.ToDateTime(DataBinder.Eval(e.Row.DataItem, "DiscrepancyLogDate"));
            //e.Row.Cells[9].Text = dDiscrepancyLogDate.ToString("dd/MM/yyyy");
            //e.Row.Cells[9].HorizontalAlign = HorizontalAlign.Left;
            HyperLink hypLinkToDisDetails = (HyperLink)e.Row.FindControl("hypLinkToDisDetails");
            HiddenField hdnDisType = (HiddenField)e.Row.FindControl("hdnDisType");
            HiddenField hdnDisLogID = (HiddenField)e.Row.FindControl("hdnDisLogID");
            HiddenField hdnWorkFlowID = (HiddenField)e.Row.FindControl("hdnWorkFlowID");
            string status = e.Row.Cells[1].Text.ToString();
            string VDRNo=hypLinkToDisDetails.Text;
            #region Switch Case
            switch (Convert.ToInt32(hdnDisType.Value.Trim()))
            {
                case 1:
                    hypLinkToDisDetails.NavigateUrl = EncryptQuery("LogDiscrepancy/DISLog_Overs.aspx?status=" + status + "&disLogID=" + hdnDisLogID.Value + "&VDRNo=" + VDRNo + "&PreviousPage=WorkList&WorkFlowID=" + hdnWorkFlowID.Value);
                    break;
                case 2:
                    hypLinkToDisDetails.NavigateUrl = EncryptQuery("LogDiscrepancy/DISLog_Shortage.aspx?status=" + status + "&disLogID=" + hdnDisLogID.Value + "&VDRNo=" + VDRNo + "&PreviousPage=WorkList&WorkFlowID=" + hdnWorkFlowID.Value);
                    break;
                case 3:
                    hypLinkToDisDetails.NavigateUrl = EncryptQuery("LogDiscrepancy/DISLog_GoodsReceivedDamaged.aspx?status=" + status + "&disLogID=" + hdnDisLogID.Value + "&VDRNo=" + VDRNo + "&PreviousPage=WorkList&WorkFlowID=" + hdnWorkFlowID.Value);
                    break;
                case 4:
                    hypLinkToDisDetails.NavigateUrl = EncryptQuery("LogDiscrepancy/DISLog_NoPurchaseOrder.aspx?status=" + status + "&disLogID=" + hdnDisLogID.Value + "&VDRNo=" + VDRNo + "&PreviousPage=WorkList&WorkFlowID=" + hdnWorkFlowID.Value);
                    break;
                case 5:
                    hypLinkToDisDetails.NavigateUrl = EncryptQuery("LogDiscrepancy/DISLog_NoPaperwork.aspx?status=" + status + "&disLogID=" + hdnDisLogID.Value + "&VDRNo=" + VDRNo + "&PreviousPage=WorkList&WorkFlowID=" + hdnWorkFlowID.Value);
                    break;
                case 6:
                    hypLinkToDisDetails.NavigateUrl = EncryptQuery("LogDiscrepancy/DISLog_IncorrectProduct.aspx?status=" + status + "&disLogID=" + hdnDisLogID.Value + "&VDRNo=" + VDRNo + "&PreviousPage=WorkList&WorkFlowID=" + hdnWorkFlowID.Value);
                    break;
                case 7:
                    hypLinkToDisDetails.NavigateUrl = EncryptQuery("LogDiscrepancy/DISLog_PresentationIssue.aspx?status=" + status + "&disLogID=" + hdnDisLogID.Value + "&VDRNo=" + VDRNo + "&PreviousPage=WorkList&WorkFlowID=" + hdnWorkFlowID.Value);
                    break;
                case 8:
                    hypLinkToDisDetails.NavigateUrl = EncryptQuery("LogDiscrepancy/DISLog_IncorrectAddress.aspx?status=" + status + "&disLogID=" + hdnDisLogID.Value + "&VDRNo=" + VDRNo + "&PreviousPage=WorkList&WorkFlowID=" + hdnWorkFlowID.Value);
                    break;
                case 9:
                    hypLinkToDisDetails.NavigateUrl = EncryptQuery("LogDiscrepancy/DISLog_PaperworkAmended.aspx?status=" + status + "&disLogID=" + hdnDisLogID.Value + "&VDRNo=" + VDRNo + "&PreviousPage=WorkList&WorkFlowID=" + hdnWorkFlowID.Value);
                    break;
                case 10:
                    hypLinkToDisDetails.NavigateUrl = EncryptQuery("LogDiscrepancy/DISLog_WrongPackSize.aspx?status=" + status + "&disLogID=" + hdnDisLogID.Value + "&VDRNo=" + VDRNo + "&PreviousPage=WorkList&WorkFlowID=" + hdnWorkFlowID.Value);
                    break;
                case 11:
                    hypLinkToDisDetails.NavigateUrl = EncryptQuery("LogDiscrepancy/DISLog_FailPalletSpecification.aspx?status=" + status + "&disLogID=" + hdnDisLogID.Value + "&VDRNo=" + VDRNo + "&PreviousPage=WorkList&WorkFlowID=" + hdnWorkFlowID.Value);
                    break;
                case 12:
                    hypLinkToDisDetails.NavigateUrl = EncryptQuery("LogDiscrepancy/DISLog_QualityIssue.aspx?status=" + status + "&disLogID=" + hdnDisLogID.Value + "&VDRNo=" + VDRNo + "&PreviousPage=WorkList&WorkFlowID=" + hdnWorkFlowID.Value);
                    break;
                case 13:
                    hypLinkToDisDetails.NavigateUrl = EncryptQuery("LogDiscrepancy/DISLog_PrematureInvoiceReceipt.aspx?status=" + status + "&disLogID=" + hdnDisLogID.Value + "&VDRNo=" + VDRNo + "&PreviousPage=WorkList&WorkFlowID=" + hdnWorkFlowID.Value);
                    break;
                case 14:
                    hypLinkToDisDetails.NavigateUrl = EncryptQuery("LogDiscrepancy/DISLog_GenericDescrepancy.aspx?status=" + status + "&disLogID=" + hdnDisLogID.Value + "&VDRNo=" + VDRNo + "&PreviousPage=WorkList&WorkFlowID=" + hdnWorkFlowID.Value);
                    break;
                case 15:
                    hypLinkToDisDetails.NavigateUrl = EncryptQuery("LogDiscrepancy/DISLog_Shuttle.aspx?status=" + status + "&disLogID=" + hdnDisLogID.Value + "&VDRNo=" + VDRNo + "&PreviousPage=WorkList&WorkFlowID=" + hdnWorkFlowID.Value);
                    break;
                case 16:
                    hypLinkToDisDetails.NavigateUrl = EncryptQuery("LogDiscrepancy/DISLog_Reservation.aspx?status=" + status + "&disLogID=" + hdnDisLogID.Value + "&VDRNo=" + VDRNo + "&PreviousPage=WorkList&WorkFlowID=" + hdnWorkFlowID.Value);
                    break;
                case 17:
                    hypLinkToDisDetails.NavigateUrl = EncryptQuery("LogDiscrepancy/DISLog_InvoiceDiscrepancy.aspx?status=" + status + "&disLogID=" + hdnDisLogID.Value + "&VDRNo=" + VDRNo + "&PreviousPage=WorkList&WorkFlowID=" + hdnWorkFlowID.Value);
                    break;

                case 18:
                    hypLinkToDisDetails.NavigateUrl = EncryptQuery("LogDiscrepancy/DISLog_StockPlannerInvoiceDiscrepancy.aspx?status=" + status + "&disLogID=" + hdnDisLogID.Value + "&VDRNo=" + VDRNo + "&PreviousPage=WorkList&WorkFlowID=" + hdnWorkFlowID.Value);
                    break;
                case 19:
                    hypLinkToDisDetails.NavigateUrl = EncryptQuery("LogDiscrepancy/DISLog_InvoiceQueryDiscrepancy.aspx?status=" + status + "&disLogID=" + hdnDisLogID.Value + "&VDRNo=" + VDRNo + "&PreviousPage=WorkList&WorkFlowID=" + hdnWorkFlowID.Value);
                    break;
                case 20:
                    hypLinkToDisDetails.NavigateUrl = EncryptQuery("LogDiscrepancy/DISLog_ItemNotOnPO.aspx?status=" + status + "&disLogID=" + hdnDisLogID.Value + "&VDRNo=" + VDRNo + "&PreviousPage=WorkList&WorkFlowID=" + hdnWorkFlowID.Value);
                    break;
            }
            #endregion
            e.Row.Cells[3].HorizontalAlign = HorizontalAlign.Left;
            if (hdnDisType.Value == "19")
            {
                gvDisLog.Columns[9].Visible = true;        
            }
            else
            {
                gvDisLog.Columns[9].Visible = true;        
            }
        }
    }

    public override BusinessEntities.BaseBe[] SubSort(GridViewSortEventArgs e)
    {
        return Utilities.GenericListHelper<DiscrepancyBE>.SortList((List<DiscrepancyBE>)ViewState["lstSites"], e.SortExpression, e.SortDirection).ToArray();
    }

    protected void ddlDepartmentStatus_SelectedIndexChanged(object sender, EventArgs e)
    {
        var lstDisLog = (List<DiscrepancyBE>)ViewState["lstSites"];
        if (ViewState["lstSites"] != null && Session["Role"] != null)
        {
            if (!ddlDepartmentStatus.SelectedValue.Equals("All"))
            {
                switch (Session["Role"].ToString())
                {
                    case "OD - Accounts Payable":
                        var lstAPFilteredDisLog = lstDisLog.FindAll(x => x.AP.Equals(ddlDepartmentStatus.SelectedValue));
                        if (lstAPFilteredDisLog.Count > 0)
                            gvDisLog.DataSource = lstAPFilteredDisLog;
                        break;
                    case "OD - Goods In":
                        var lstGIFilteredDisLog = lstDisLog.FindAll(x => x.GI.Equals(ddlDepartmentStatus.SelectedValue));
                        if (lstGIFilteredDisLog.Count > 0)
                            gvDisLog.DataSource = lstGIFilteredDisLog;
                        break;
                    case "OD - Stock Planner":
                        var lstSPFilteredDisLog = lstDisLog.FindAll(x => x.INV.Equals(ddlDepartmentStatus.SelectedValue));
                        if (lstSPFilteredDisLog.Count > 0)
                            gvDisLog.DataSource = lstSPFilteredDisLog;
                        break;
                    case "Vendor":
                        var lstVENFilteredDisLog = lstDisLog.FindAll(x => x.VEN.Equals(ddlDepartmentStatus.SelectedValue));
                        if (lstVENFilteredDisLog.Count > 0)
                            gvDisLog.DataSource = lstVENFilteredDisLog;
                        break;
                    default:
                        break;

                }
                gvDisLog.DataBind();
            }
            else
            {
                gvDisLog.DataSource = lstDisLog;
                gvDisLog.DataBind();
            }
        }
    }
}
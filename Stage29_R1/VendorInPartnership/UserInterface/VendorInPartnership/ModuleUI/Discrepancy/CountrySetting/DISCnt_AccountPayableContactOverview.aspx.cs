﻿using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using Utilities;
using WebUtilities;
using BusinessEntities.ModuleBE.Discrepancy.CountrySetting;
using BusinessLogicLayer.ModuleBAL.Discrepancy.CountrySetting;
using System;
using System.Data;

public partial class DISCnt_AccountPayableContactOverview : CommonPage
{
   #region Methods

    private void BindAccountPayableContacts(string Action) {
        DISCnt_AccountPayableContactBE oDISCnt_AccountPayableContactBE = new DISCnt_AccountPayableContactBE();
        DISCNT_AccountPayableContactBAL oDISCnt_AccountPayableContactBAL = new DISCNT_AccountPayableContactBAL();

        ListBox lstLeft = msVendor.FindControl("ucVendor").FindControl("lstleft") as ListBox;

        oDISCnt_AccountPayableContactBE.Action = Action;


        if (string.IsNullOrEmpty(msVendor.SelectedVendorIDs))
        {
            oDISCnt_AccountPayableContactBE.Action = "ShowAllAPUsers";
        }
        else
        {
            oDISCnt_AccountPayableContactBE.VendorIDs = msVendor.SelectedVendorIDs;
        }

        if (GetQueryStringValue("CountryID") != null)
        {
            oDISCnt_AccountPayableContactBE.CountryID = Convert.ToInt32(GetQueryStringValue("CountryID"));
            btnBack.Visible = true;
        }
        else
        {
            btnBack.Visible = false;
        }

       
        msVendor.setVendorsOnPostBack();
        

        DataTable dt = oDISCnt_AccountPayableContactBAL.GetAccountPayableContactDetailsBAL(oDISCnt_AccountPayableContactBE,"");
        oDISCnt_AccountPayableContactBAL = null;
        if (dt != null ) {
            dt.Columns.Add("CountryId", typeof(int));
            if (dt.Rows.Count > 0)
            {
                if (GetQueryStringValue("CountryID") != null)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        dt.Rows[i]["CountryId"] = Convert.ToInt32(GetQueryStringValue("CountryID"));
                    }
                    dt.AcceptChanges();
                }
            }
            grdAccountPayable.DataSource = dt;
            grdAccountPayable.DataBind();
            ViewState["myDataTable"] = dt;
        }
        
        
    }

    private void GetUnAssignedVendorsCount()
    {
        DISCnt_AccountPayableContactBE oDISCnt_AccountPayableContactBE = new DISCnt_AccountPayableContactBE();
        DISCNT_AccountPayableContactBAL oDISCnt_AccountPayableContactBAL = new DISCNT_AccountPayableContactBAL();

        oDISCnt_AccountPayableContactBE.Action = "UnAssignedVendorCount";
        if (GetQueryStringValue("CountryID") != null)
        {
            oDISCnt_AccountPayableContactBE.CountryID = Convert.ToInt32(GetQueryStringValue("CountryID"));
        }

        int vendorCount  = oDISCnt_AccountPayableContactBAL.GetUnAssignedVendorsCountBAL(oDISCnt_AccountPayableContactBE, "");
        oDISCnt_AccountPayableContactBAL = null;
        APLink.Text = vendorCount.ToString();
    }

   #endregion

   #region Events

    protected void Page_Load(object sender, EventArgs e)
    {
        GetUnAssignedVendorsCount();
        if (!IsPostBack)
        {
            BindAccountPayableContacts("ShowAllAPUsers");
        }
        ucExportToExcel.GridViewControl = grdAccountPayable;
        ucExportToExcel.FileName = "Dis-AccountPayableOverview";

        TextBox txtVendor = Utilities.Common.FindControlRecursive(this.Page.Controls[0], "txtVendorNo") as TextBox;
        if (txtVendor != null)
            txtVendor.Focus();
    }

    protected void grdAccountPayable_Sorting(object sender, GridViewSortEventArgs e)
    {
        if (ViewState["myDataTable"] != null)
        {
            DataTable myDataTable = (DataTable)ViewState["myDataTable"];
            ViewState["sortDirection"] = ViewState["sortDirection"] == null || Convert.ToString(ViewState["sortDirection"]) == "DESC" ? "ASC" : "DESC";
            DataView view = myDataTable.DefaultView;
            view.Sort = e.SortExpression + " " + Convert.ToString(ViewState["sortDirection"]);
            grdAccountPayable.DataSource = view;
            grdAccountPayable.DataBind();

        }
    }   

    protected void btnMoveRightAll_Click(object sender, EventArgs e)
    {
    }
    protected void btnRefresh_Click(object sender, EventArgs e)
    {
        BindAccountPayableContacts("ShowAll");
    }
    protected void btnMoveLeft_Click(object sender, EventArgs e)
    {
    }
    protected void btnMoveLeftAll_Click(object sender, EventArgs e)
    {
    }
    #endregion
    protected void btnBack_Click(object sender, EventArgs e)
    {
        EncryptQueryString("../../GlobalSettings/VendorAPContactSummary.aspx");
    }
}
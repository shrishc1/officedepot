﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    CodeFile="DISCnt_AccountPayableContactOverview.aspx.cs" Inherits="DISCnt_AccountPayableContactOverview" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="../../../CommonUI/UserControls/ucAddButton.ascx" TagName="ucAddButton"
    TagPrefix="cc2" %>
<%@ Register Src="../../../CommonUI/UserControls/ucExportToExcel.ascx" TagName="ucExportToExcel"
    TagPrefix="cc3" %>
<%@ Register Src="~/CommonUI/UserControls/ucSeacrhVendor.ascx" TagName="ucSeacrhVendor"
    TagPrefix="cc4" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectVendor.ascx" TagName="MultiSelectVendor"
    TagPrefix="cc5" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript">
        $(document).ready(function () {
            $("body").keypress(function (e) {
                if (e.which == 13 || e.keyCode == 13) {
                    $('#<%=btnRefresh.ClientID %>').click();
                    return false;
                }
            });
        });
    </script>
    <h2>
        <cc1:ucLabel ID="lblAccountPayableContact" runat="server" Text="Account Payable Contact"></cc1:ucLabel>
    </h2>
    <div>
        <table width="90%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
            <tr align="left">
                <td style="font-weight: bold;">
                    <cc1:ucLabel ID="lblUVendor" runat="server" Text="Unassigned Vendors"></cc1:ucLabel>
                </td>
                <td style="font-weight: bold;">
                    :
                </td>
                <td>
                    <asp:HyperLink ID="APLink" runat="server" NavigateUrl="~/ModuleUI/Discrepancy/CountrySetting/DISCnt_AccountPayableUnAssignedVendors.aspx"></asp:HyperLink>
                </td>
            </tr>
            <tr align="left">
                <td style="font-weight: bold;">
                    <cc1:ucLabel ID="lblVendor" runat="server" Text="Vendor"></cc1:ucLabel>
                </td>
                <td style="font-weight: bold;">
                    :
                </td>
                <td>
                    <cc4:ucSeacrhVendor runat="server" ID="ddlSeacrhVendor" Visible="false" />
                    <cc5:MultiSelectVendor runat="server" ID="msVendor" EnableViewState="true" />
                </td>
            </tr>
        </table>
    </div>
    <div class="button-row">                    
        <cc1:ucButton ID="btnBack" runat="server" Text="Back" CssClass="button" Visible="false"
            onclick="btnBack_Click" />        
        <cc1:ucButton ID="btnRefresh" runat="server" Text="Refresh" CssClass="button" OnClick="btnRefresh_Click" />
        <cc3:ucExportToExcel ID="ucExportToExcel" runat="server" />
    </div>
    <table width="100%">
        <tr>
            <td align="center" colspan="9">
                <cc1:ucGridView ID="grdAccountPayable" Width="100%" runat="server" CssClass="grid"
                    OnSorting="grdAccountPayable_Sorting" AllowSorting="true">
                    <Columns>
                        <asp:TemplateField HeaderText="Clerk Name" SortExpression="UserName">
                            <HeaderStyle Width="175px" HorizontalAlign="Left" />
                            <ItemTemplate>
                                <asp:HiddenField ID="hdnUserId" Value='<%# Eval("UserID") %>' runat="server" />
                                <asp:HyperLink ID="APLink" runat="server" Text='<%#Eval("UserName") %>' NavigateUrl='<%# EncryptQuery("DISCnt_AccountPayableContactDetail.aspx?UserName="+Eval("UserName")+ "&UserID="+Eval("UserID")+"&EmailId="+Eval("EmailId")+"&PhoneNumber="+Eval("PhoneNumber")+"&FaxNumber="+Eval("FaxNumber")+"&VendorCount="+Eval("VendorCount")+"&CountryID="+Eval("CountryId"))%>'></asp:HyperLink>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:TemplateField>
                        <asp:BoundField HeaderText="Phone Number" DataField="PhoneNumber" SortExpression="PhoneNumber">
                            <HeaderStyle Width="175px" HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                        <asp:BoundField HeaderText="Fax Number" DataField="FaxNumber" SortExpression="FaxNumber">
                            <HeaderStyle Width="175px" HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                        <asp:BoundField HeaderText="Email Address" DataField="EmailId" SortExpression="EmailId">
                            <HeaderStyle Width="175px" HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                        <asp:BoundField HeaderText="# of Vendors" DataField="VendorCount" SortExpression="VendorCount">
                            <HeaderStyle Width="175px" HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                    </Columns>
                </cc1:ucGridView>
            </td>
        </tr>
    </table>
</asp:Content>

﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Utilities;
using WebUtilities;
using BusinessEntities.ModuleBE.Discrepancy.CountrySetting;
using BusinessLogicLayer.ModuleBAL.Discrepancy.CountrySetting;
using BusinessEntities.ModuleBE.Security;
using BusinessLogicLayer.ModuleBAL.Security;
using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using BusinessLogicLayer.ModuleBAL.Appointment.SiteSettings;

public partial class DISCnt_AccountPayableContactDetail : CommonPage
{

    public SortDirection GridViewSortDirection {
        get {
            if (ViewState["GridViewSortDirection"] == null)
                ViewState["GridViewSortDirection"] = SortDirection.Descending;
            return (SortDirection)ViewState["GridViewSortDirection"];
        }
        set { ViewState["GridViewSortDirection"] = value; }
    }

    public string SortDir {
        get {
            if (GridViewSortDirection == SortDirection.Ascending) {
                return "DESC";
            }
            else {
                return "ASC";
            }
        }
    }


    public string GridViewSortExp {
        get {
            if (ViewState["GridViewSortExp"] == null)
                ViewState["GridViewSortExp"] = "Vendor_Name";
            return (string)ViewState["GridViewSortExp"];
        }
        set { ViewState["GridViewSortExp"] = value; }
    }

    protected string deleteMessage = WebCommon.getGlobalResourceValue("DeleteMessage");
    static int userId = 0;
   
    #region Methods

    public void ShowAccountPayableInfo()
    {
        if (GetQueryStringValue("UserName") != null)
        {           
            NameValue.Text = GetQueryStringValue("UserName").ToString();
            EmailAddressvalue.Text = GetQueryStringValue("EmailId").ToString();
            PhoneNumbervalue.Text  = GetQueryStringValue("PhoneNumber").ToString();
            FaxNumbervalue.Text = GetQueryStringValue("FaxNumber").ToString();
            lblVendorsCount.Text  = GetQueryStringValue("VendorCount").ToString();

            userId = Convert.ToInt32(GetQueryStringValue("UserID").ToString());

            DISCnt_AccountPayableContactBE oDISCnt_AccountPayableContactBE = new DISCnt_AccountPayableContactBE();
            DISCNT_AccountPayableContactBAL oDISCnt_AccountPayableContactBAL = new DISCNT_AccountPayableContactBAL();

            oDISCnt_AccountPayableContactBE.UserID = userId;
            if (GetQueryStringValue("CountryID") != null && GetQueryStringValue("CountryID")!="")
            {
                oDISCnt_AccountPayableContactBE.CountryID = Convert.ToInt32(GetQueryStringValue("CountryID"));
                ddlCountry.innerControlddlCountry.SelectedValue = GetQueryStringValue("CountryID");
            }

            oDISCnt_AccountPayableContactBE.Action = "VendorDetailsByUserID";

            DataTable dt = oDISCnt_AccountPayableContactBAL.GetAccountPayableVendorDetailsBAL(oDISCnt_AccountPayableContactBE, "");
            oDISCnt_AccountPayableContactBAL = null;
            if (dt != null)
            {
                //grdAccountPayable.DataSource = dt;
                //grdAccountPayable.DataBind();
                ViewState["VendorDetailTable"] = dt;
                SortGridView(GridViewSortExp, GridViewSortDirection);
            }
        }
        else if (Session["VendorDetails"] != null)
        {
            userId = Convert.ToInt32(Session["ClerkID"].ToString());

            DISCnt_AccountPayableContactBE oDISCnt_AccountPayableContactBE = new DISCnt_AccountPayableContactBE();
            DISCNT_AccountPayableContactBAL oDISCnt_AccountPayableContactBAL = new DISCNT_AccountPayableContactBAL();

            oDISCnt_AccountPayableContactBE.UserID = Convert.ToInt32(Session["ClerkID"].ToString());
            oDISCnt_AccountPayableContactBE.Action = "VendorDetailsByUserID";

            DataTable dt = oDISCnt_AccountPayableContactBAL.GetAccountPayableVendorDetailsBAL(oDISCnt_AccountPayableContactBE, "");
            oDISCnt_AccountPayableContactBAL = null;

            if (dt != null)
            {
                //grdAccountPayable.DataSource = dt;
                //grdAccountPayable.DataBind();
                ViewState["VendorDetailTable"] = dt;
                SortGridView(GridViewSortExp, GridViewSortDirection);
            }

            NameValue.Text = Session["NameValue"].ToString();
            EmailAddressvalue.Text = Session["EmailAddressvalue"].ToString();
            PhoneNumbervalue.Text = Session["PhoneNumbervalue"].ToString();
            FaxNumbervalue.Text = Session["FaxNumbervalue"].ToString();
            lblVendorsCount.Text = Session["VendorCount"].ToString();            
        }
    }

    
    #endregion

    #region Events

    protected void Page_Init(object sender, EventArgs e)
    {
        ScriptManager scriptManager = ScriptManager.GetCurrent(this.Page);
        scriptManager.RegisterPostBackControl(this.ucExportToExcel);

        ddlCountry.CurrentPage = this;
        ucExportToExcel.CurrentPage = this;

        ddlCountry.CurrentPage = this;
        ddlCountry.IsAllRequired = true;
        ddlCountry.IsAllDefault = true;
    }

    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            ShowAccountPayableInfo();

            if (SortDir == "ASC") {
                GridViewSortDirection = SortDirection.Descending;
            }
            else {
                GridViewSortDirection = SortDirection.Ascending;
            }
        }
        ucExportToExcel.GridViewControl = grdAccountPayableExcel;
        ucExportToExcel.FileName = "AccountPayableOverview";
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {

    }

    protected void grdAccountPayable_Sorting(object sender, GridViewSortEventArgs e)
    {
        GridViewSortExp = e.SortExpression;
        if (SortDir == "ASC") {
            GridViewSortDirection = SortDirection.Ascending;
        }
        else {
            GridViewSortDirection = SortDirection.Descending;
        }

        SortGridView(GridViewSortExp, GridViewSortDirection);

        //if (ViewState["VendorDetailTable"] != null)
        //{
        //    DataTable myDataTable = (DataTable)ViewState["VendorDetailTable"];
        //    ViewState["sortDirection"] = ViewState["sortDirection"] == null || Convert.ToString(ViewState["sortDirection"]) == "DESC" ? "ASC" : "DESC";
        //    DataView view = myDataTable.DefaultView;
        //    view.Sort = e.SortExpression + " " + Convert.ToString(ViewState["sortDirection"]);
        //    grdAccountPayable.DataSource = view;
        //    grdAccountPayable.DataBind();

        //}
    }

    protected void grdAccountPayable_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        grdAccountPayable.PageIndex = e.NewPageIndex;

        SortGridView(GridViewSortExp, GridViewSortDirection);
    }

    private void SortGridView(string sortExpression, SortDirection direction) {

        try {

            if (ViewState["VendorDetailTable"] != null) {
                DataTable myDataTable = (DataTable)ViewState["VendorDetailTable"];
                DataView view = myDataTable.DefaultView;
                view.Sort = sortExpression + " " + SortDir;
                grdAccountPayable.DataSource = view;
                grdAccountPayable.DataBind();
                grdAccountPayableExcel.DataSource = view;
                grdAccountPayableExcel.DataBind();
                CommonPage c = new CommonPage();
                c.LocalizeGridHeader(grdAccountPayable);
                c.LocalizeGridHeader(grdAccountPayableExcel);
            }            
        }
        catch (Exception ex) {
            LogUtility.SaveErrorLogEntry(ex);
        }
    }

    public override void CountrySelectedIndexChanged()
    {
        base.CountrySelectedIndexChanged();        
    }

    protected void btnAddVendor_Click(object sender, EventArgs e)
    {
        SaveSession();
        
        if (GetQueryStringValue("UserName") != null)
            EncryptQueryString("DISCnt_AccountPayableAddVendor.aspx?UserName=" + GetQueryStringValue("UserName").ToString() + "&UserID=" + GetQueryStringValue("UserID").ToString());
        else
            EncryptQueryString("DISCnt_AccountPayableAddVendor.aspx?UserName=" + Session["NameValue"].ToString() + "&UserID=" + Session["ClerkID"].ToString());
    }

    private void SaveSession()
    {
        //if (ViewState["VendorDetailTable"] != null)
        //{
            Session["VendorDetails"] = ViewState["VendorDetailTable"];
            Session["NameValue"] = NameValue.Text;
            Session["EmailAddressvalue"] = EmailAddressvalue.Text;
            Session["PhoneNumbervalue"] = PhoneNumbervalue.Text;
            Session["FaxNumbervalue"] = FaxNumbervalue.Text;
            Session["VendorCount"] = lblVendorsCount.Text;
            Session["ClerkID"] = userId.ToString();
          

        //}
    }
    protected void btnReAssign_Click(object sender, EventArgs e)
    {
        SaveSession();

        if (GetQueryStringValue("UserName") != null)
            EncryptQueryString("DISCnt_AccountPayableReassignVendors.aspx?ClerkName=" + GetQueryStringValue("UserName").ToString() + "&UserID=" + GetQueryStringValue("UserID").ToString());
        else
            EncryptQueryString("DISCnt_AccountPayableReassignVendors.aspx?ClerkName=" + Session["NameValue"].ToString() + "&UserID=" + Session["ClerkID"].ToString());

        //EncryptQueryString("DISCnt_AccountPayableReassignVendors.aspx?ClerkName=" + GetQueryStringValue("UserName").ToString() + "&UserID=" + GetQueryStringValue("UserID").ToString());
    }
    protected void btnRemove_Click(object sender, EventArgs e)
    {
        SaveSession();

        if (GetQueryStringValue("UserName") != null)
            EncryptQueryString("DISCnt_AccountPayableRemoveVendors.aspx?ClerkName=" + GetQueryStringValue("UserName").ToString() + "&UserID=" + GetQueryStringValue("UserID").ToString());
        else
            EncryptQueryString("DISCnt_AccountPayableRemoveVendors.aspx?ClerkName=" + Session["NameValue"].ToString() + "&UserID=" + Session["ClerkID"].ToString());

        //EncryptQueryString("DISCnt_AccountPayableRemoveVendors.aspx?ClerkName=" + GetQueryStringValue("UserName").ToString() + "&UserID=" + GetQueryStringValue("UserID").ToString());
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        grdAccountPayable.PageIndex = 0;
        GridViewSortExp = "Vendor_Name";
        if (GetQueryStringValue("UserName") != null)
        {
            NameValue.Text = GetQueryStringValue("UserName").ToString();
            EmailAddressvalue.Text = GetQueryStringValue("EmailId").ToString();
            PhoneNumbervalue.Text = GetQueryStringValue("PhoneNumber").ToString();
            FaxNumbervalue.Text = GetQueryStringValue("FaxNumber").ToString();
            lblVendorsCount.Text = GetQueryStringValue("VendorCount").ToString();

            userId = Convert.ToInt32(GetQueryStringValue("UserID").ToString());

            DISCnt_AccountPayableContactBE oDISCnt_AccountPayableContactBE = new DISCnt_AccountPayableContactBE();
            DISCNT_AccountPayableContactBAL oDISCnt_AccountPayableContactBAL = new DISCNT_AccountPayableContactBAL();

            oDISCnt_AccountPayableContactBE.UserID = userId;
            oDISCnt_AccountPayableContactBE.Action = "VendorDetailsByUserID";

            DataTable dt = oDISCnt_AccountPayableContactBAL.GetAccountPayableVendorDetailsBAL(oDISCnt_AccountPayableContactBE, "");
            oDISCnt_AccountPayableContactBAL = null;
            if (dt != null)
            {                
                DataView dv1 = dt.DefaultView;

                if (ddlCountry.innerControlddlCountry.SelectedItem.Text.Trim().Equals("----All----") && string.IsNullOrEmpty(txtVendor.Text))
                {
                    ViewState["VendorDetailTable"] = dt;
                    SortGridView(GridViewSortExp, GridViewSortDirection);
                    //grdAccountPayable.DataSource = dt;
                    //grdAccountPayable.DataBind();
                    return;
                }
                else if (ddlCountry.innerControlddlCountry.SelectedItem.Text.Trim().Equals("----All----") && !string.IsNullOrEmpty(txtVendor.Text))
                {
                    dv1.RowFilter = "Vendor_Name like '%" + txtVendor.Text + "%'";
                }
                else
                    dv1.RowFilter = " CountryName = '" + ddlCountry.innerControlddlCountry.SelectedItem.Text + "'  and Vendor_Name like '%" + txtVendor.Text + "%'";

                DataTable dtNew = dv1.ToTable();
                ViewState["VendorDetailTable"] = dtNew;
                SortGridView(GridViewSortExp, GridViewSortDirection);
                //grdAccountPayable.DataSource = dtNew;
                //grdAccountPayable.DataBind();
            }
        }
        else if (Session["VendorDetails"] != null)
        {
            NameValue.Text = Session["NameValue"].ToString();
            EmailAddressvalue.Text = Session["EmailAddressvalue"].ToString();
            PhoneNumbervalue.Text = Session["PhoneNumbervalue"].ToString();
            FaxNumbervalue.Text = Session["FaxNumbervalue"].ToString();
            lblVendorsCount.Text = Session["VendorCount"].ToString();
            
            userId = Convert.ToInt32(Session["ClerkID"].ToString());            

            DISCnt_AccountPayableContactBE oDISCnt_AccountPayableContactBE = new DISCnt_AccountPayableContactBE();
            DISCNT_AccountPayableContactBAL oDISCnt_AccountPayableContactBAL = new DISCNT_AccountPayableContactBAL();

            oDISCnt_AccountPayableContactBE.UserID = userId;
            oDISCnt_AccountPayableContactBE.Action = "VendorDetailsByUserID";

            DataTable dt = oDISCnt_AccountPayableContactBAL.GetAccountPayableVendorDetailsBAL(oDISCnt_AccountPayableContactBE, "");
            oDISCnt_AccountPayableContactBAL = null;
            if (dt != null)
            {
                DataView dv1 = dt.DefaultView;

                if (ddlCountry.innerControlddlCountry.SelectedItem.Text.Trim().Equals("----All----") && string.IsNullOrEmpty(txtVendor.Text))
                {
                    ViewState["VendorDetailTable"] = dt;
                    //grdAccountPayable.DataSource = dt;
                    //grdAccountPayable.DataBind();
                    SortGridView(GridViewSortExp, GridViewSortDirection);
                    return;
                }
                else if (ddlCountry.innerControlddlCountry.SelectedItem.Text.Trim().Equals("----All----") && !string.IsNullOrEmpty(txtVendor.Text))
                {
                    dv1.RowFilter = "Vendor_Name like '%" + txtVendor.Text + "%'";
                }
                else
                    dv1.RowFilter = " CountryName = '" + ddlCountry.innerControlddlCountry.SelectedItem.Text + "'  and Vendor_Name like '%" + txtVendor.Text + "%'";

                DataTable dtNew = dv1.ToTable();
                ViewState["VendorDetailTable"] = dtNew;
                SortGridView(GridViewSortExp, GridViewSortDirection);
                //grdAccountPayable.DataSource = dtNew;
                //grdAccountPayable.DataBind();
            }
        }
    }    

    protected void btnBack_Click(object sender, EventArgs e)
    {
        SaveSession();
        if (GetQueryStringValue("CountryID") != null && GetQueryStringValue("CountryID") != "")
        {
            int CountryID = Convert.ToInt32(GetQueryStringValue("CountryID"));
            EncryptQueryString("DISCnt_AccountPayableContactOverview.aspx?&CountryID="+CountryID);
        }
        else
        {
            EncryptQueryString("DISCnt_AccountPayableContactOverview.aspx");
        }
           
    }



    #endregion

}
﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="../../../CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    CodeFile="DISCnt_AccountPayableContactDetail.aspx.cs" Inherits="DISCnt_AccountPayableContactDetail" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register TagName="ucVendorSearch" TagPrefix="cc2" Src="~/CommonUI/UserControls/ucSeacrhVendor.ascx" %>
<%@ Register Src="../../../CommonUI/UserControls/ucExportToExcel.ascx" TagName="ucExportToExcel"
    TagPrefix="cc4" %>
<%@ Register TagName="ucCountry" TagPrefix="cc3" Src="~/CommonUI/UserControls/ucCountry.ascx" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script language="javascript" type="text/javascript">
        function AlertForExistance(arg) {
            var objExists = window.confirm(arg);
            if (objExists == true) {
                return true;
            }
            else {
                return false;
            }
        }

        function confirmDelete() {
            return confirm('<%=deleteMessage%>');
        }
    </script>
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="up1" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <h2>
                <cc1:ucLabel ID="lblAccountPayableContact" runat="server" Text="Account Payable Contact"></cc1:ucLabel>
            </h2>
            <div class="right-shadow">
                <div class="formbox">
                    <br />
                    <cc1:ucPanel ID="pnlAccountPayable" runat="server" CssClass="fieldset-form">
                        <table width="80%" cellspacing="5" cellpadding="0" border="0" class="form-table">
                            <tr>
                                <td width="15%" style="font-weight: bold;">
                                    <cc1:ucLabel ID="lblName" runat="server" Text="Name"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold; width: 5px">
                                    :
                                </td>
                                <td width="35%" class="nobold">
                                    <cc1:ucLabel ID="NameValue" runat="server"></cc1:ucLabel>
                                </td>
                                <td width="15%" style="font-weight: bold;">
                                    <cc1:ucLabel ID="lblEmailAddress" runat="server" Text="Email Address"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold; width: 5px">
                                    :
                                </td>
                                <td width="35%" class="nobold">
                                    <cc1:ucLabel ID="EmailAddressvalue" runat="server"></cc1:ucLabel>
                                </td>
                            </tr>
                            <tr>
                                <td style="font-weight: bold;">
                                    <cc1:ucLabel ID="lblPhoneNumber" runat="server" Text="Phone Number"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold;">
                                    :
                                </td>
                                <td class="nobold">
                                    <cc1:ucLabel ID="PhoneNumbervalue" runat="server"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold;">
                                    <cc1:ucLabel ID="lblFaxNumber" runat="server" Text="Fax Number"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold;">
                                    :
                                </td>
                                <td class="nobold">
                                    <cc1:ucLabel ID="FaxNumbervalue" runat="server"></cc1:ucLabel>
                                </td>
                            </tr>
                            <tr>
                                <td  style="font-weight: bold;">
                                    <cc1:ucLabel ID="lblVendors" runat="server" Text="# Vendors"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold;">
                                    :
                                </td>
                                <td class="nobold">
                                    <cc1:ucLabel ID="lblVendorsCount" runat="server"></cc1:ucLabel>
                                </td>
                            </tr>
                        </table>
                    </cc1:ucPanel>
                </div>
            </div>
            <div class="bottom-shadow">
            </div>
            <div class="button-row">
                <cc1:ucButton ID="btnAddVendor" runat="server" Text="Add Vendors" OnClick="btnAddVendor_Click"
                    CssClass="button" />
                <cc1:ucButton ID="btnReAssign" runat="server" Text="Reassign Vendors" OnClick="btnReAssign_Click"
                    CssClass="button" />
                <cc1:ucButton ID="btnRemove" runat="server" Text="Remove Vendors" OnClick="btnRemove_Click"
                    CssClass="button" />
                <cc4:ucExportToExcel ID="ucExportToExcel" runat="server" />
            </div>
            <cc1:ucPanel ID="pnlCurrentVendor" runat="server" GroupingText="Current Vendor" CssClass="fieldset-form">
                <table width="60%" cellspacing="5" cellpadding="0" border="0" class="form-table">
                    <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblCountry" runat="server"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold; width: 5px">
                            :
                        </td>
                        <td >
                            <cc3:ucCountry ID="ddlCountry" runat="server" />
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblVendor" runat="server"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold; width: 5px">
                            :
                        </td>
                        <td>
                            <cc1:ucTextbox ID="txtVendor" runat="server" />
                        </td>
                        <td>
                            <cc1:ucButton ID="btnSearch" runat="server" OnClick="btnSearch_Click" CssClass="button" />
                        </td>
                    </tr>
                </table>
            </cc1:ucPanel>
            <table width="100%">
                <tr>
                    <td style="width: 100%">
                        <cc1:ucGridView ID="grdAccountPayable" Width="100%" runat="server" CssClass="grid"
                            OnSorting="grdAccountPayable_Sorting" OnPageIndexChanging="grdAccountPayable_PageIndexChanging"
                            AllowPaging="true" PageSize="30" AllowSorting="true">
                            <Columns>
                                <asp:TemplateField HeaderText="Country" SortExpression="CountryName">
                                    <HeaderStyle HorizontalAlign="Left" />
                                    <ItemStyle HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <asp:HiddenField ID="hdnVendorID" Value='<%# Eval("VendorID") %>' runat="server" />
                                        <asp:HiddenField ID="hdnEmailId" Value='<%# Eval("EmailId") %>' runat="server" />
                                        <asp:HiddenField ID="hdnFaxNumber" Value='<%# Eval("FaxNumber") %>' runat="server" />
                                        <asp:HiddenField ID="hdnPhoneNumber" Value='<%# Eval("PhoneNumber") %>' runat="server" />
                                        <asp:HiddenField ID="hdnUserName" Value='<%# Eval("UserName") %>' runat="server" />
                                        <asp:Label ID="lblCountry" runat="server" Text='<%#Eval("CountryName") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField HeaderText="Vendor #" DataField="Vendor_No" SortExpression="Vendor_No">
                                    <HeaderStyle HorizontalAlign="Left" />
                                    <ItemStyle HorizontalAlign="Left" />
                                </asp:BoundField>
                                <asp:BoundField HeaderText="Vendor Name" SortExpression="Vendor_Name" DataField="Vendor_Name">
                                    <HeaderStyle HorizontalAlign="Left" />
                                </asp:BoundField>
                            </Columns>
                        </cc1:ucGridView>
                        <cc1:ucGridView ID="grdAccountPayableExcel" Width="100%" runat="server" CssClass="grid" Visible="false">
                            <Columns>
                                <asp:TemplateField HeaderText="Country" SortExpression="CountryName">
                                    <HeaderStyle HorizontalAlign="Left" />
                                    <ItemStyle HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <asp:HiddenField ID="hdnVendorID" Value='<%# Eval("VendorID") %>' runat="server" />
                                        <asp:HiddenField ID="hdnEmailId" Value='<%# Eval("EmailId") %>' runat="server" />
                                        <asp:HiddenField ID="hdnFaxNumber" Value='<%# Eval("FaxNumber") %>' runat="server" />
                                        <asp:HiddenField ID="hdnPhoneNumber" Value='<%# Eval("PhoneNumber") %>' runat="server" />
                                        <asp:HiddenField ID="hdnUserName" Value='<%# Eval("UserName") %>' runat="server" />
                                        <asp:Label ID="lblCountry" runat="server" Text='<%#Eval("CountryName") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField HeaderText="Vendor #" DataField="Vendor_No" SortExpression="Vendor_No">
                                    <HeaderStyle HorizontalAlign="Left" />
                                    <ItemStyle HorizontalAlign="Left" />
                                </asp:BoundField>
                                <asp:BoundField HeaderText="Vendor Name" SortExpression="Vendor_Name" DataField="Vendor_Name">
                                    <HeaderStyle HorizontalAlign="Left" />
                                </asp:BoundField>
                            </Columns>
                        </cc1:ucGridView>
                    </td>
                    <tr>
                        <td colspan="6">
                            <div class="button-row" style="text-align: right">
                                <cc1:ucButton ID="btnBack" runat="server" Text="Back" CssClass="button" OnClick="btnBack_Click" />
                            </div>
                        </td>
                    </tr>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

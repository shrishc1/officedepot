﻿<%@ Page Language="C#" 
AutoEventWireup="true" 
MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master" 
CodeFile="DISCNT_AccountPayableDetailsOverview.aspx.cs" 
Inherits="DISCNT_AccountPayableDetailsOverview" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register src="../../../CommonUI/UserControls/ucAddButton.ascx" tagname="ucAddButton" tagprefix="cc2" %>
<%@ Register src="../../../CommonUI/UserControls/ucExportToExcel.ascx" tagname="ucExportToExcel" tagprefix="cc3" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<h2>
   <cc1:ucLabel ID="lblAccountPayableContact" runat="server" Text="Account Payable Contact"></cc1:ucLabel>
</h2>
<div class="button-row">    
     <cc2:ucAddButton ID="btnAdd" runat="server" NavigateUrl="DISCnt_AccountPayableDetailsEdit.aspx" />
     <cc3:ucExportToExcel ID="btnExportToExcel1" runat="server" />
</div>
<table width="100%">              
        <tr>
            <td align="center" colspan="9">
                <cc1:ucGridView ID="grdAccountPayable" Width="100%" runat="server" 
                    CssClass="grid" onsorting="grdAccountPayable_Sorting"   AllowSorting="true">
                    <Columns>                        
                        <asp:BoundField HeaderText="Country" DataField="CountryName" SortExpression="CountryName">
                            <HeaderStyle Width="100px"  HorizontalAlign="Left"/>
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField>                                                
                         <asp:TemplateField HeaderText="Vendor Name" SortExpression="VendorName">
                            <HeaderStyle Width="150px" HorizontalAlign="Left" />                            
                            <ItemTemplate>
                                <asp:HyperLink ID="VendorLink" runat="server" NavigateUrl='<%# EncryptQuery("DISCnt_AccountPayableDetailsEdit.aspx?VendorAccountPayableID="+Eval("VendorAccountPayableID")) %>' Text='<%#Eval("VendorName") %>'></asp:HyperLink>                                
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:TemplateField>                                                                                                                                                                                                                                                               
                       
                        <asp:BoundField HeaderText="Account Payable clerk Name" DataField="UserName" SortExpression="UserName">
                            <HeaderStyle Width="175px"  HorizontalAlign="Left"/>
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField> 

                         <asp:BoundField HeaderText="Account Payable clerk Phone Number" DataField="PhoneNumber" SortExpression="PhoneNumber">
                            <HeaderStyle Width="175px"  HorizontalAlign="Left"/>
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField> 

                         <asp:BoundField HeaderText="Account Payable clerk Fax Number" DataField="FaxNumber" SortExpression="FaxNumber">
                            <HeaderStyle Width="175px"  HorizontalAlign="Left"/>
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField> 

                        <asp:BoundField HeaderText="Account Payable clerk Email Address" DataField="EmailId" SortExpression="EmailId">
                            <HeaderStyle Width="175px"  HorizontalAlign="Left"/>
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField> 

                    </Columns>                                       
                </cc1:ucGridView>               
            </td>
        </tr>
</table>  
</asp:Content>
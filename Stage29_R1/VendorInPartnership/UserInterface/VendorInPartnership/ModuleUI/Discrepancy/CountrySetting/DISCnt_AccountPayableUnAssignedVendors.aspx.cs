﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Utilities;
using WebUtilities;
using BusinessEntities.ModuleBE.Discrepancy.CountrySetting;
using BusinessLogicLayer.ModuleBAL.Discrepancy.CountrySetting;
using BusinessEntities.ModuleBE.Security;
using BusinessLogicLayer.ModuleBAL.Security;
using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using BusinessLogicLayer.ModuleBAL.Appointment.SiteSettings;


public partial class DISCnt_AccountPayableUnAssignedVendors : CommonPage
{
    public SortDirection GridViewSortDirection {
        get {
            if (ViewState["GridViewSortDirection"] == null)
                ViewState["GridViewSortDirection"] = SortDirection.Ascending;
            return (SortDirection)ViewState["GridViewSortDirection"];
        }
        set { ViewState["GridViewSortDirection"] = value; }
    }

    public string SortDir {
        get {
            if (GridViewSortDirection == SortDirection.Ascending) {
                return "DESC";
            }
            else {
                return "ASC";
            }
        }
    }


    public string GridViewSortExp {
        get {
            if (ViewState["GridViewSortExp"] == null)
                ViewState["GridViewSortExp"] = "VendorID";
            return (string)ViewState["GridViewSortExp"];
        }
        set { ViewState["GridViewSortExp"] = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack) {
            GetUnAssignedVendors();

            if (SortDir == "ASC") {
                GridViewSortDirection = SortDirection.Ascending;
            }
            else {
                GridViewSortDirection = SortDirection.Descending;
            }
        }
    }
    private void GetUnAssignedVendors()
    {
        DataTable myDataTable = null;
        if (ViewState["UnAssignedVendor"] != null) {
            myDataTable = (DataTable)ViewState["UnAssignedVendor"];
        }
        else {
            DISCnt_AccountPayableContactBE oDISCnt_AccountPayableContactBE = new DISCnt_AccountPayableContactBE();
            DISCNT_AccountPayableContactBAL oDISCnt_AccountPayableContactBAL = new DISCNT_AccountPayableContactBAL();

            oDISCnt_AccountPayableContactBE.Action = "UnAssignedVendors";
            myDataTable = oDISCnt_AccountPayableContactBAL.GetUnAssignedVendorsBAL(oDISCnt_AccountPayableContactBE, "");
            oDISCnt_AccountPayableContactBAL = null;
        }
        if (myDataTable != null)
        {
            ViewState["UnAssignedVendor"] = myDataTable;
            grdAccountPayable.DataSource = myDataTable;
            grdAccountPayable.DataBind();
        }
    }
    protected void grdAccountPayable_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        //if (ViewState["UnAssignedVendor"] != null)
        //{
        //    DataTable myDataTable = (DataTable)ViewState["UnAssignedVendor"];
        //    grdAccountPayable.PageIndex = e.NewPageIndex;
        //    grdAccountPayable.DataSource = myDataTable;
        //    grdAccountPayable.DataBind();
        //}

        grdAccountPayable.PageIndex = e.NewPageIndex;     

        SortGridView(GridViewSortExp, GridViewSortDirection);
    }

    private void SortGridView(string sortExpression, SortDirection direction) {
      
        try {
            if (ViewState["UnAssignedVendor"] != null) {
                DataTable myDataTable = (DataTable)ViewState["UnAssignedVendor"];
                DataView view = myDataTable.DefaultView;
                view.Sort = sortExpression + " " + SortDir;
                grdAccountPayable.DataSource = view;
                grdAccountPayable.DataBind();
                CommonPage c = new CommonPage();
                c.LocalizeGridHeader(grdAccountPayable);
            }
        }
        catch (Exception ex) {
            LogUtility.SaveErrorLogEntry(ex);
        }
    }

    protected void grdAccountPayable_Sorting(object sender, GridViewSortEventArgs e)
    {
        GridViewSortExp = e.SortExpression;
        if (SortDir == "ASC") {
            GridViewSortDirection = SortDirection.Ascending;
        }
        else {
            GridViewSortDirection = SortDirection.Descending;
        }

        SortGridView(GridViewSortExp, GridViewSortDirection);

        //if (ViewState["UnAssignedVendor"] != null)
        //{
        //    DataTable myDataTable = (DataTable)ViewState["UnAssignedVendor"];
        //    ViewState["sortDirection"] = ViewState["sortDirection"] == null || Convert.ToString(ViewState["sortDirection"]) == "DESC" ? "ASC" : "DESC";
        //    DataView view = myDataTable.DefaultView;
        //    view.Sort = e.SortExpression + " " + Convert.ToString(ViewState["sortDirection"]);
        //    grdAccountPayable.DataSource = view;
        //    grdAccountPayable.DataBind();

        //}
    }
}
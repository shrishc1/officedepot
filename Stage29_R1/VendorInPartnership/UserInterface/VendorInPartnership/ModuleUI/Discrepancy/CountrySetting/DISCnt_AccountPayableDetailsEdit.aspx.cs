﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using Utilities; using WebUtilities;
using BusinessEntities.ModuleBE.Discrepancy.CountrySetting;
using BusinessLogicLayer.ModuleBAL.Discrepancy.CountrySetting;
using BusinessEntities.ModuleBE.Security;
using BusinessLogicLayer.ModuleBAL.Security;
using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using BusinessLogicLayer.ModuleBAL.Appointment.SiteSettings;

public partial class DISCNT_AccountPayableDetailsEdit : CommonPage {

    protected string deleteMessage = WebCommon.getGlobalResourceValue("DeleteMessage");
    protected void Page_Init(object sender, EventArgs e) {
        ddlCountry.CurrentPage = this;
       
        SearchVendor1.CurrentPage = this;
        SearchVendor1.IsStandAloneRequired = true;
        SearchVendor1.IsChildRequired = true;
        SearchVendor1.IsParentRequired = false;
        SearchVendor1.IsGrandParentRequired = false;
    }

    protected void Page_Load(object sender, EventArgs e) {


        if (!IsPostBack) {
            ddlCountry.innerControlddlCountry.AutoPostBack = true;
        }
    }

    protected void Page_PreRender(object sender, EventArgs e) {
        if (!IsPostBack) {
            BindAccountPayableClearks();
            SearchVendor1.CountryID = Convert.ToInt32(ddlCountry.innerControlddlCountry.SelectedItem.Value);
            GetAccountPayable();
        }
    }

    #region Methods

    public void GetAccountPayable() {
        DISCnt_AccountPayableContactBE oDISCnt_AccountPayableContactBE = new DISCnt_AccountPayableContactBE();
        DISCNT_AccountPayableContactBAL oDISCnt_AccountPayableContactBAL = new DISCNT_AccountPayableContactBAL();
        if (GetQueryStringValue("VendorAccountPayableID") != null && GetQueryStringValue("VendorAccountPayableID").ToString() != "") {
            btnDelete.Style["display"] = "";
            btnSave.Style["display"] = "none";
            oDISCnt_AccountPayableContactBE.Action = "ShowAll";
            oDISCnt_AccountPayableContactBE.VendorAccountPayableID = Convert.ToInt32(GetQueryStringValue("VendorAccountPayableID"));
            List<DISCnt_AccountPayableContactBE> lstResult = oDISCnt_AccountPayableContactBAL.GetAccountPayableContactDetailsBAL(oDISCnt_AccountPayableContactBE);
            oDISCnt_AccountPayableContactBAL = null;
            if (lstResult != null && lstResult.Count > 0) {
                ddlCountry.innerControlddlCountry.SelectedValue = lstResult[0].CountryID.ToString();
                ddlCountry.innerControlddlCountry.Enabled = false;
                chkCheckAll.Style["display"] = "none";
                SearchVendor1.Visible = false;
                VendorNumberValue.Visible = true;
                VendorNumberValue.Text = lstResult[0].Vendor.VendorName.ToString();
                hidVendorID.Value = lstResult[0].VendorID.ToString();
                ddlAccountPayableclerk.SelectedValue = lstResult[0].UserID.ToString();
                ddlAccountPayableclerk.Enabled = false;
                GetAccountPayableClerkInfo(Convert.ToInt32(lstResult[0].UserID));
            }
            else {
                ClearControls();
                btnDelete.Style["display"] = "none";
                btnSave.Style["display"] = "";
                GetAccountPayableClerkInfo(Convert.ToInt32(ddlAccountPayableclerk.SelectedItem.Value));
            }
        }
        else {
            ClearControls();
            btnDelete.Style["display"] = "none";
            btnSave.Style["display"] = "";
            if (Convert.ToInt32(ddlAccountPayableclerk.SelectedItem.Value) > 0)
                GetAccountPayableClerkInfo(Convert.ToInt32(ddlAccountPayableclerk.SelectedItem.Value));
        }
    }

    public void GetAccountPayableClerkInfo(int pUserID) {
        SCT_UserBE oSCT_UserBE = new SCT_UserBE();
        SCT_UserBAL oSCT_UserBAL = new SCT_UserBAL();
        oSCT_UserBE.Action = "ShowAll";
        oSCT_UserBE.UserID = pUserID;
        List<SCT_UserBE> lstUser = oSCT_UserBAL.GetUserDetailsBAL(oSCT_UserBE);
        oSCT_UserBAL = null;
        if (lstUser != null && lstUser.Count > 0) {
            NameValue.Text = lstUser[0].FullName.ToString();
            EmailAddressvalue.Text = lstUser[0].EmailId.ToString();
            PhoneNumbervalue.Text = lstUser[0].PhoneNumber.ToString();
            FaxNumbervalue.Text = lstUser[0].FaxNumber.ToString();
        }
    }

    public void BindAccountPayableClearks() {
        SCT_UserBE oSCT_UserBE = new SCT_UserBE();
        SCT_UserBAL oSCT_UserBAL = new SCT_UserBAL();
        oSCT_UserBE.Action = "ShowAll";
        oSCT_UserBE.RoleName = "OD - Accounts Payable";
        List<SCT_UserBE> lstUsers = oSCT_UserBAL.GetUserDetailsBAL(oSCT_UserBE);
        oSCT_UserBAL = null;
        if (lstUsers != null && lstUsers.Count > 0) {
            FillControls.FillDropDown(ref ddlAccountPayableclerk, lstUsers, "FullName", "UserID");
        }
    }

    public void ClearControls() {
        ddlCountry.innerControlddlCountry.Enabled = true;
        // ddlCountry.innerControlddlCountry.SelectedIndex = 0;
        SearchVendor1.Visible = true;
        VendorNumberValue.Visible = false;
        VendorNumberValue.Text = string.Empty;
        ddlAccountPayableclerk.Enabled = true;
        ddlAccountPayableclerk.SelectedIndex = 0;
        NameValue.Text = string.Empty;
        EmailAddressvalue.Text = string.Empty;
        PhoneNumbervalue.Text = string.Empty;
        FaxNumbervalue.Text = string.Empty;
    }

    public void Save() {
        DISCnt_AccountPayableContactBE oDISCnt_AccountPayableContactBE = new DISCnt_AccountPayableContactBE();
        DISCNT_AccountPayableContactBAL oDISCnt_AccountPayableContactBAL = new DISCNT_AccountPayableContactBAL();

        oDISCnt_AccountPayableContactBE.UserID = Convert.ToInt32(ddlAccountPayableclerk.SelectedItem.Value);
        if (chkCheckAll.Checked) {
            oDISCnt_AccountPayableContactBE.IsAllVendors = 1;
            UP_VendorBE oUP_VendorBE = new UP_VendorBE();
            UP_VendorBAL oUP_VendorBAL = new UP_VendorBAL();
            oUP_VendorBE.Action = "SearchVendor";
            oUP_VendorBE.VendorName = "";
            oUP_VendorBE.Site = new BusinessEntities.ModuleBE.AdminFunctions.MAS_SiteBE();
            oUP_VendorBE.Site.SiteCountryID = Convert.ToInt32(ddlCountry.innerControlddlCountry.SelectedItem.Value);
            List<UP_VendorBE> lstUPVendor = new List<UP_VendorBE>();
            lstUPVendor = oUP_VendorBAL.GetCarrierDetailsBAL(oUP_VendorBE);
            if (lstUPVendor != null && lstUPVendor.Count > 0) {
                if (lstUPVendor.Count == 1) {
                    oDISCnt_AccountPayableContactBE.VendorIDs = lstUPVendor[0].VendorID.ToString();
                }
                else if (lstUPVendor.Count > 1) {
                    oDISCnt_AccountPayableContactBE.VendorIDs = lstUPVendor[0].VendorID.ToString();
                    for (int i = 1; i < lstUPVendor.Count - 1; i++) {
                        oDISCnt_AccountPayableContactBE.VendorIDs += "," + lstUPVendor[i].VendorID.ToString();
                    }
                    oDISCnt_AccountPayableContactBE.VendorIDs += "," + lstUPVendor[lstUPVendor.Count - 1].VendorID.ToString();
                }
            }
        }
        else {

            if (SearchVendor1.VendorNo != null && SearchVendor1.VendorNo != "") {
                oDISCnt_AccountPayableContactBE.IsAllVendors = 0;
                oDISCnt_AccountPayableContactBE.VendorIDs = SearchVendor1.VendorNo;
            }
        }
        oDISCnt_AccountPayableContactBE.Action = "Edit";
        int? iResult = oDISCnt_AccountPayableContactBAL.addEditAccountPayableContactDetailsBAL(oDISCnt_AccountPayableContactBE);
        EncryptQueryString("DISCnt_AccountPayableDetailsOverview.aspx");
    }

    #endregion

    #region Events

    public override void CountrySelectedIndexChanged() {
        base.CountrySelectedIndexChanged();
        SearchVendor1.CountryID = Convert.ToInt32(ddlCountry.innerControlddlCountry.SelectedItem.Value);
        SearchVendor1.ClearSearch();
    }

    protected void btnSave_Click(object sender, EventArgs e) {
        DISCnt_AccountPayableContactBE oDISCnt_AccountPayableContactBE = new DISCnt_AccountPayableContactBE();
        DISCNT_AccountPayableContactBAL oDISCnt_AccountPayableContactBAL = new DISCNT_AccountPayableContactBAL();


        if (chkCheckAll.Checked) {
            oDISCnt_AccountPayableContactBE.IsAllVendors = 1;
            UP_VendorBE oUP_VendorBE = new UP_VendorBE();
            UP_VendorBAL oUP_VendorBAL = new UP_VendorBAL();
            oUP_VendorBE.Action = "SearchVendor";
            oUP_VendorBE.VendorName = "";
            oUP_VendorBE.Site = new BusinessEntities.ModuleBE.AdminFunctions.MAS_SiteBE();
            oUP_VendorBE.Site.SiteCountryID = Convert.ToInt32(ddlCountry.innerControlddlCountry.SelectedItem.Value);
            List<UP_VendorBE> lstUPVendor = new List<UP_VendorBE>();
            lstUPVendor = oUP_VendorBAL.GetCarrierDetailsBAL(oUP_VendorBE);
            if (lstUPVendor != null && lstUPVendor.Count > 0) {
                if (lstUPVendor.Count == 1) {
                    oDISCnt_AccountPayableContactBE.VendorIDs = lstUPVendor[0].VendorID.ToString();
                }
                else if (lstUPVendor.Count > 1) {
                    oDISCnt_AccountPayableContactBE.VendorIDs = lstUPVendor[0].VendorID.ToString();
                    for (int i = 1; i < lstUPVendor.Count - 1; i++) {
                        oDISCnt_AccountPayableContactBE.VendorIDs += "," + lstUPVendor[i].VendorID.ToString();
                    }
                    oDISCnt_AccountPayableContactBE.VendorIDs += "," + lstUPVendor[lstUPVendor.Count - 1].VendorID.ToString();
                }
            }
            oDISCnt_AccountPayableContactBE.Action = "ShowAll";
            List<DISCnt_AccountPayableContactBE> lstExistance = oDISCnt_AccountPayableContactBAL.GetAccountPayableContactDetailsBAL(oDISCnt_AccountPayableContactBE);
            if (lstExistance != null && lstExistance.Count > 0) {
                if (lstExistance.Count > 0) {
                    AlertMessage.Text = WebCommon.getGlobalResourceValue("VendorUserCombinationSAlreadyExists");
                    mdlAccountPayableViewer.Show();
                    return;
                }

            }
        }
        else {
            if (SearchVendor1.VendorNo != null && SearchVendor1.VendorNo != "") {
                oDISCnt_AccountPayableContactBE.IsAllVendors = 0;
                oDISCnt_AccountPayableContactBE.VendorIDs = SearchVendor1.VendorNo;

                oDISCnt_AccountPayableContactBE.Action = "ShowAll";
                List<DISCnt_AccountPayableContactBE> lstExistance = oDISCnt_AccountPayableContactBAL.GetAccountPayableContactDetailsBAL(oDISCnt_AccountPayableContactBE);
                if (lstExistance != null && lstExistance.Count > 0) {
                    if (lstExistance.Count > 0) {
                        AlertMessage.Text = WebCommon.getGlobalResourceValue("VendorUserCombinationSAlreadyExists");
                        mdlAccountPayableViewer.Show();
                        return;
                            }



                    //if (lstExistance != null && lstExistance.Count > 0) {
                    //    string strVendorUserCombinationAlreadyExists = WebCommon.getGlobalResourceValue("VendorUserCombinationAlreadyExists");
                    //    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert2", "alert('" + strVendorUserCombinationAlreadyExists + "')", true);
                    //    if (SearchVendor1.FindControl("SelectedVendorName") != null)
                    //        ((Label)SearchVendor1.FindControl("SelectedVendorName")).Text = hidVendorName.Value;
                    //    return;
                    //}
                }
            }
            oDISCnt_AccountPayableContactBE.UserID = Convert.ToInt32(ddlAccountPayableclerk.SelectedItem.Value);
            oDISCnt_AccountPayableContactBE.Action = "Edit";
            oDISCnt_AccountPayableContactBE.CountryID = Convert.ToInt32(ddlCountry.innerControlddlCountry.SelectedItem.Value);
            int? iResult = oDISCnt_AccountPayableContactBAL.addEditAccountPayableContactDetailsBAL(oDISCnt_AccountPayableContactBE);
            oDISCnt_AccountPayableContactBAL = null;
            EncryptQueryString("DISCnt_AccountPayableDetailsOverview.aspx");
        }
    }

    protected void btnDelete_Click(object sender, EventArgs e) {

        DISCnt_AccountPayableContactBE oDISCnt_AccountPayableContactBE = new DISCnt_AccountPayableContactBE();
        DISCNT_AccountPayableContactBAL oDISCnt_AccountPayableContactBAL = new DISCNT_AccountPayableContactBAL();
        oDISCnt_AccountPayableContactBE.Action = "Delete";
        oDISCnt_AccountPayableContactBE.VendorAccountPayableID = Convert.ToInt32(GetQueryStringValue("VendorAccountPayableID"));
        oDISCnt_AccountPayableContactBAL.addEditAccountPayableContactDetailsBAL(oDISCnt_AccountPayableContactBE);
        oDISCnt_AccountPayableContactBAL = null;
        EncryptQueryString("DISCnt_AccountPayableDetailsOverview.aspx");
    }

    protected void btnBack_Click(object sender, EventArgs e) {

        EncryptQueryString("DISCnt_AccountPayableDetailsOverview.aspx");
    }

    protected void ddlAccountPayableclerk_SelectedIndexChanged(object sender, EventArgs e) {
        GetAccountPayableClerkInfo(Convert.ToInt32(ddlAccountPayableclerk.SelectedItem.Value));
        if (SearchVendor1.FindControl("SelectedVendorName") != null)
            ((Label)SearchVendor1.FindControl("SelectedVendorName")).Text = hidVendorName.Value;
    }

    protected void btnOk_Click(object sender, EventArgs e) {
        Save();
    }

    protected void btnCancel_Click(object sender, EventArgs e) {

    }




    #endregion
}
﻿<%@ Page Language="C#" AutoEventWireup="true" 
MasterPageFile="../../../CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    CodeFile="DISCNT_AccountPayableDetailsEdit.aspx.cs" Inherits="DISCNT_AccountPayableDetailsEdit" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register TagName="ucVendorSearch" TagPrefix="cc2" Src="~/CommonUI/UserControls/ucSeacrhVendor.ascx" %>
<%@ Register TagName="ucCountry" TagPrefix="cc3" Src="~/CommonUI/UserControls/ucCountry.ascx" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">   
<script language="javascript" type="text/javascript">
    function AlertForExistance(arg) {
        var objExists = window.confirm(arg);
        if (objExists == true) {
            return true;
        }
        else {
            return false;
        }
    }

    function checkVendorSelected(source, args) {
        
        var selectAll = document.getElementById('<%=chkCheckAll.ClientID%>');
        if (selectAll != null && selectAll.checked == false)
        {
            if (document.getElementById('<%=SearchVendor1.FindControl("txtVendorNo").ClientID%>') != null) 
            {
                var obj = document.getElementById('<%=SearchVendor1.FindControl("txtVendorNo").ClientID%>');
                if (obj.value == '') {
                    args.IsValid = false;
                }
            }
        }
    }


    function confirmDelete() {
        return confirm('<%=deleteMessage%>');
    }
    </script>
    <asp:UpdatePanel ID="up1" runat="server" UpdateMode="Always">
        <ContentTemplate>               
            <h2>                
                <cc1:ucLabel ID="lblAccountPayableContact" runat="server" Text="Account Payable Contact"></cc1:ucLabel>
            </h2>            
            <div class="right-shadow">
                <div class="formbox">
                    <table width="60%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                        <tr>
                            <td style="font-weight: bold; width: 30%">
                                <cc1:ucLabel ID="lblCountry" runat="server" Text="Country"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold; width: 1%">:</td>
                            <td style="width: 60%">                                                                                                      
                                <cc3:ucCountry ID="ddlCountry" runat="server" />                                   
                            </td>
                            <td style="font-weight: bold; width: 9%"></td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold;">
                                <cc1:ucLabel ID="lblVendorNo" runat="server" Text="Vendor No"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold;">:</td>
                            <td>                               
                                <cc2:ucVendorSearch ID="SearchVendor1" runat="server" />
                                <asp:CustomValidator ID="cusvNoSelectedVendor" runat="server" ClientValidationFunction="checkVendorSelected"
                                 ValidationGroup="Save" Display="None"></asp:CustomValidator>
                                <cc1:ucLabel ID="VendorNumberValue" runat="server" Visible="false"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold;"><cc1:ucCheckbox ID="chkCheckAll" runat="server" Text="All" /> </td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold;">
                                <cc1:ucLabel ID="lblAccountPayableclerk" runat="server" Text="Account Payable clerk"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold;">:</td>
                            <td style="font-weight: bold;">                               
                                <cc1:ucDropdownList ID="ddlAccountPayableclerk" runat="server" Width="150px" 
                                    AutoPostBack="true" 
                                    onselectedindexchanged="ddlAccountPayableclerk_SelectedIndexChanged">
                                </cc1:ucDropdownList>                                                             
                            </td>
                             <td></td>
                        </tr>
                    </table>
                    <br />
                    <cc1:ucPanel ID="pnlAccountPayable" runat="server" CssClass="fieldset-form">
                        <table width="80%" cellspacing="5" cellpadding="0" border="0"  class="form-table">
                            <tr>
                                <%--<td width="25%" style="font-weight: bold;">
                                    Number
                                </td>
                                <td style="font-weight: bold; width: 5px">:</td>
                                <td width="25%" class="nobold">
                                    <cc1:ucLabel ID="Number" runat="server"></cc1:ucLabel>
                                </td>--%>
                                <td width="25%" style="font-weight: bold;">
                                  <cc1:ucLabel ID="lblName" runat="server" Text="Name"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold; width: 5px">:</td>
                                <td width="25%" class="nobold">
                                    <cc1:ucLabel ID="NameValue" runat="server"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold;">
                                    <cc1:ucLabel ID="lblEmailAddress" runat="server" Text="Email Address"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold; width: 5px">:</td>
                                <td colspan="3" class="nobold">
                                    <cc1:ucLabel ID="EmailAddressvalue" runat="server"></cc1:ucLabel>
                                </td>
                            </tr>
                            <tr>
                                <td width="25%" style="font-weight: bold;">
                                    <cc1:ucLabel ID="lblPhoneNumber" runat="server" Text="Phone Number"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold; width: 5px">:</td>
                                <td class="nobold">
                                    <cc1:ucLabel ID="PhoneNumbervalue" runat="server"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold;">
                                    <cc1:ucLabel ID="lblFaxNumber" runat="server" Text="Fax Number"></cc1:ucLabel> 
                                </td>
                                <td style="font-weight: bold; width: 5px">:</td>
                                <td class="nobold">
                                    <cc1:ucLabel ID="FaxNumbervalue" runat="server"></cc1:ucLabel>
                                </td>
                            </tr>
                        </table>
                    </cc1:ucPanel>
                </div>
            </div>    
            <div class="bottom-shadow"></div>
            <div class="button-row">
             <asp:ValidationSummary ID="vSummary" runat="server" ShowMessageBox="true" ShowSummary="false"
                Style="color: Red" ValidationGroup="Save" />
                <cc1:ucButton ID="btnSave" runat="server" Text="Save" CssClass="button" ValidationGroup="Save" 
                    onclick="btnSave_Click" />
                <cc1:ucButton ID="btnDelete" runat="server" Text="Delete" CssClass="button" 
                    OnClientClick="return confirmDelete();"
                    onclick="btnDelete_Click"  />
                <cc1:ucButton ID="btnBack" runat="server" Text="Back" CssClass="button" 
                    onclick="btnBack_Click" />
                <input type="hidden" id="hidVendorID" runat="server" />    
                <input type="hidden" id="hidVendorName" runat="server" />                
            </div>
            <asp:Button ID="btnAccountPayableViewer" runat="Server" Style="display: none" />
            <ajaxToolkit:ModalPopupExtender ID="mdlAccountPayableViewer" runat="server" TargetControlID="btnAccountPayableViewer"
                PopupControlID="pnlbtnAccountPayableViewer" BackgroundCssClass="modalBackground"
                BehaviorID="AccountPayableViewer" DropShadow="false" />
            <asp:Panel ID="pnlbtnAccountPayableViewer" runat="server" Style="display: none; width: 45%;
                height: 45%;">
                    <table width="100%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                        <tr>
                            <td  align="center">
                                <cc1:ucLabel ID="AlertMessage" runat="server"></cc1:ucLabel>
                            </td>
                        </tr>
                        <tr>
                            <td  align="center">
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <cc1:ucButton ID="btnOk" runat="server" OnClick="btnOk_Click" CssClass="button" />
                                &nbsp;&nbsp;&nbsp;
                                <cc1:ucButton ID="btnCancel" runat="server" OnClick="btnCancel_Click" CssClass="button"/>
                            </td>                            
                        </tr>
                    </table>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    
</asp:Content>

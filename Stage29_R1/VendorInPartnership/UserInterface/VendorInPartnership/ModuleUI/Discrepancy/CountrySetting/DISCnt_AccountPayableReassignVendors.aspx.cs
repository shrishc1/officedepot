﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.AdminFunctions;
using BusinessEntities.ModuleBE.Appointment.CountrySetting;
using BusinessEntities.ModuleBE.Security;
using BusinessLogicLayer.ModuleBAL.AdminFunctions;
using BusinessLogicLayer.ModuleBAL.Appointment.CountrySetting;
using BusinessLogicLayer.ModuleBAL.Security;
using Utilities;
using WebUtilities;
using System.Configuration;
using System.Threading;
using System.Globalization;
using BusinessLogicLayer.ModuleBAL.Appointment.SiteSettings;
using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using BaseControlLibrary;
using BusinessEntities.ModuleBE.Discrepancy.CountrySetting;
using BusinessLogicLayer.ModuleBAL.Discrepancy.CountrySetting;
using System.Data;
using System.Linq.Expressions;

public partial class DISCnt_AccountPayableReassignVendors : CommonPage
{
    #region Declarations ...

    SCT_UserBAL oSCT_UserBAL = null;
    SCT_UserBE oSCT_UserBE = null;
    DISCnt_AccountPayableContactBE oDISCnt_AccountPayableContactBE = null;
    DISCNT_AccountPayableContactBAL oDISCnt_AccountPayableContactBAL = null;

    protected string selectMoveTo = WebCommon.getGlobalResourceValue("SelectMoveTo");
    string vendorMovedConfirmation = WebCommon.getGlobalResourceValue("VendorMovedConfirmation");
    protected string vendorReassignConfirmation = WebCommon.getGlobalResourceValue("VendorReassignConfirmation");
    protected string selectCheckboxToReassignVendor = WebCommon.getGlobalResourceValue("SelectCheckboxToReassignVendor");

    public SortDirection GridViewSortDirection
    {
        get
        {
            if (ViewState["GridViewSortDirection"] == null)
                ViewState["GridViewSortDirection"] = SortDirection.Ascending;
            return (SortDirection)ViewState["GridViewSortDirection"];
        }
        set { ViewState["GridViewSortDirection"] = value; }
    }

    public string SortDir
    {
        get
        {
            if (GridViewSortDirection == SortDirection.Ascending)
            {
                return "DESC";
            }
            else
            {
                return "ASC";
            }
        }
    }

    public string GridViewSortExp
    {
        get
        {
            if (ViewState["GridViewSortExp"] == null)
                ViewState["GridViewSortExp"] = "Vendor.VendorName";
            return (string)ViewState["GridViewSortExp"];
        }
        set { ViewState["GridViewSortExp"] = value; }
    }
    
    #endregion

    #region Events ...

    protected void Page_InIt(object sender, EventArgs e)
    {
        ddlCountry.CurrentPage = this;
        ddlCountry.IsAllRequired = true;
        ddlCountry.IsAllDefault = true;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            lblAPClerkNameT.Text = Convert.ToString(GetQueryStringValue("ClerkName"));
            this.BindMoveTo();
            if (ddlMoveToClerkName.Items.Count > 0)
            {
                int index = ddlMoveToClerkName.Items.IndexOf(ddlMoveToClerkName.Items.FindByText(lblAPClerkNameT.Text));
                ddlMoveToClerkName.Items.RemoveAt(index);
            }
            this.GetAllAssignVendors();
        }
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        this.Search();
    }    

    protected void grdAccountPayable_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        if (ViewState["VendorDetailTable"] != null)
        {
            //List<DISCnt_AccountPayableContactBE> GetAllAssignedVendors = (List<DISCnt_AccountPayableContactBE>)ViewState["VendorDetailTable"];
            //grdAccountPayable.PageIndex = e.NewPageIndex;
            //grdAccountPayable.DataSource = GetAllAssignedVendors;
            //grdAccountPayable.DataBind();

            grdAccountPayable.PageIndex = e.NewPageIndex;
            SortGridView(GridViewSortExp, GridViewSortDirection);
        }
    }

   

    protected void btnBack_Click(object sender, EventArgs e)
    {
        EncryptQueryString("DISCnt_AccountPayableContactDetail.aspx");
        //EncryptQueryString("DISCnt_AccountPayableContactDetail.aspx?UserName=" + GetQueryStringValue("ClerkName").ToString() + "&UserID=" + GetQueryStringValue("UserID").ToString());
    }

    protected void btnReassign_Click(object sender, EventArgs e)
    {
        if (ddlMoveToClerkName.Items.Count > 0)
        {
            if (ddlMoveToClerkName.SelectedIndex > 0)
            {
                oDISCnt_AccountPayableContactBE = new DISCnt_AccountPayableContactBE();
                oDISCnt_AccountPayableContactBAL = new DISCNT_AccountPayableContactBAL();
                oDISCnt_AccountPayableContactBE.Action = "UpdateMultipleChecked";
                oDISCnt_AccountPayableContactBE.IsAllVendors = 0;
                oDISCnt_AccountPayableContactBE.UserID = Convert.ToInt32(GetQueryStringValue("UserID"));
                oDISCnt_AccountPayableContactBE.MoveTo = Convert.ToInt32(ddlMoveToClerkName.SelectedValue);

                foreach (GridViewRow row in grdAccountPayable.Rows)
                {
                    CheckBox chkSelect = row.FindControl("chkSelect") as CheckBox;
                    HiddenField hdnVendorId = row.FindControl("hdnVendorId") as HiddenField;
                    if (chkSelect != null && hdnVendorId != null)
                    {
                        if (chkSelect.Checked)
                            oDISCnt_AccountPayableContactBE.VendorIDs += string.Format(",{0}", hdnVendorId.Value);
                    }
                }

                string vendorIds = oDISCnt_AccountPayableContactBE.VendorIDs.Substring(0, 1);
                if (vendorIds.Trim().Equals(","))
                    oDISCnt_AccountPayableContactBE.VendorIDs = oDISCnt_AccountPayableContactBE.VendorIDs.Remove(0, 1);

                int? iResult = oDISCnt_AccountPayableContactBAL.addEditAccountPayableContactDetailsBAL(oDISCnt_AccountPayableContactBE);
                oDISCnt_AccountPayableContactBAL = null;
                
                if (ViewState["GridViewSortDirection"] != null)
                    this.GetAllAssignVendors(Convert.ToString(ViewState["GridViewSortExp"]), (SortDirection)ViewState["GridViewSortDirection"]);
                else
                    this.GetAllAssignVendors(Convert.ToString(ViewState["GridViewSortExp"]), SortDirection.Ascending);
                
                SortGridView(GridViewSortExp, GridViewSortDirection);

                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert1", "alert('" + vendorMovedConfirmation + "')", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert1", "alert('" + selectMoveTo + "')", true);
                return;
            }
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "alert1", "alert('" + selectMoveTo + "')", true);
            return;
        }
    }    

    #endregion

    #region Methods ...

    private void BindMoveTo()
    {
        oSCT_UserBAL = new SCT_UserBAL();
        oSCT_UserBE = new SCT_UserBE();
        oSCT_UserBE.Action = "GetUserOverview";
        /* This Role Id of [OD - Accounts Payable] */
        /* As suggested by Abhinav passing hardcode this Role Id. */
        oSCT_UserBE.UserRoleID = 5;
        List<SCT_UserBE> lstMoveTo = oSCT_UserBAL.GetUsers(oSCT_UserBE).OrderBy(u => u.FullName).ToList();
        if (lstMoveTo.Count > 0 && lstMoveTo != null)
        {
            FillControls.FillDropDown(ref ddlMoveToClerkName, lstMoveTo, "FullName", "UserID", " Select ");
        }
    }

    private void GetAllAssignVendors(string gridViewSortExp = "", SortDirection gridViewSortDirection = SortDirection.Ascending)
    {
        if (GetQueryStringValue("UserID") != null)
        {
            oDISCnt_AccountPayableContactBE = new DISCnt_AccountPayableContactBE();
            oDISCnt_AccountPayableContactBAL = new DISCNT_AccountPayableContactBAL();
            oDISCnt_AccountPayableContactBE.Action = "GetAllAssignedVendorsByUserID";
            if (GetQueryStringValue("UserID") != null)
                oDISCnt_AccountPayableContactBE.UserID = Convert.ToInt32(GetQueryStringValue("UserID"));

            List<DISCnt_AccountPayableContactBE> GetAllAssignedVendors = 
                oDISCnt_AccountPayableContactBAL.GetAllAssignedVendorsByUserIDBAL(oDISCnt_AccountPayableContactBE);

            if (GetAllAssignedVendors.Count > 0)
            {
                ViewState["VendorDetailTable"] = GetAllAssignedVendors;
                grdAccountPayable.DataSource = GetAllAssignedVendors;
                grdAccountPayable.DataBind();

                if (string.IsNullOrEmpty(gridViewSortExp))
                    GridViewSortExp = "Vendor.VendorName";
                else
                    GridViewSortExp = gridViewSortExp;

                if (gridViewSortDirection == null)
                    GridViewSortDirection = SortDirection.Ascending;
                else
                    GridViewSortDirection = gridViewSortDirection;  
            }
            else 
            {
                ViewState["VendorDetailTable"] = null;
                grdAccountPayable.DataSource = null;
                grdAccountPayable.DataBind();
            }

            oDISCnt_AccountPayableContactBAL = null;
        }
    }

    private void Search()
    {
        if (ViewState["VendorDetailTable"] != null)
        {
            List<DISCnt_AccountPayableContactBE> GetAllAssignedVendors = (List<DISCnt_AccountPayableContactBE>)ViewState["VendorDetailTable"];

            if (!string.IsNullOrEmpty(txtVendor.Text) && ddlCountry.innerControlddlCountry.SelectedIndex > 0)
                grdAccountPayable.DataSource = GetAllAssignedVendors.Where(av => av.CountryID == Convert.ToInt32(ddlCountry.innerControlddlCountry.SelectedValue)
                    && av.Vendor.VendorName.StartsWith(txtVendor.Text.Trim(), StringComparison.OrdinalIgnoreCase)).ToList();
            else if (!string.IsNullOrEmpty(txtVendor.Text))
                grdAccountPayable.DataSource = GetAllAssignedVendors.Where(av => av.Vendor.VendorName.StartsWith(txtVendor.Text.Trim(), StringComparison.OrdinalIgnoreCase)).ToList();
            else if (ddlCountry.innerControlddlCountry.SelectedIndex > 0)
                grdAccountPayable.DataSource = GetAllAssignedVendors.Where(av => av.CountryID == Convert.ToInt32(ddlCountry.innerControlddlCountry.SelectedValue)).ToList();
            else
                grdAccountPayable.DataSource = GetAllAssignedVendors;

            grdAccountPayable.DataBind();
        }
    }

    private void SortGridView(string sortExpression, SortDirection direction)
    {
        List<DISCnt_AccountPayableContactBE> lstVendor = new List<DISCnt_AccountPayableContactBE>();
        try
        {
            if (ViewState["VendorDetailTable"] != null)
            {
                //lstVendor = Utilities.GenericListHelper<DISCnt_AccountPayableContactBE>.SortList((List<DISCnt_AccountPayableContactBE>)ViewState["VendorDetailTable"], sortExpression, direction);
                //grdAccountPayable.DataSource = lstVendor;
                //grdAccountPayable.DataBind();
                //CommonPage c = new CommonPage();
                //c.LocalizeGridHeader(grdAccountPayable);

                var GetAllAssignedVendors = (List<DISCnt_AccountPayableContactBE>)ViewState["VendorDetailTable"];
                var ShotredData = new List<DISCnt_AccountPayableContactBE>();
                if (!string.IsNullOrEmpty(txtVendor.Text) && ddlCountry.innerControlddlCountry.SelectedIndex > 0)
                    ShotredData = GetAllAssignedVendors.Where(av => av.CountryID == Convert.ToInt32(ddlCountry.innerControlddlCountry.SelectedValue)
                        && av.Vendor.VendorName.StartsWith(txtVendor.Text.Trim(), StringComparison.OrdinalIgnoreCase)).ToList();
                else if (!string.IsNullOrEmpty(txtVendor.Text))
                    ShotredData = GetAllAssignedVendors.Where(av => av.Vendor.VendorName.StartsWith(txtVendor.Text.Trim(), StringComparison.OrdinalIgnoreCase)).ToList();
                else if (ddlCountry.innerControlddlCountry.SelectedIndex > 0)
                    ShotredData = GetAllAssignedVendors.Where(av => av.CountryID == Convert.ToInt32(ddlCountry.innerControlddlCountry.SelectedValue)).ToList();
                else
                    ShotredData = GetAllAssignedVendors;

                lstVendor = Utilities.GenericListHelper<DISCnt_AccountPayableContactBE>.SortList(ShotredData, sortExpression, direction);
                grdAccountPayable.DataSource = lstVendor;
                grdAccountPayable.DataBind();
                CommonPage c = new CommonPage();
                c.LocalizeGridHeader(grdAccountPayable); 
            }
        }
        catch (Exception ex)
        {
            LogUtility.SaveErrorLogEntry(ex);
        }

    }

    public override BusinessEntities.BaseBe[] SubSort(GridViewSortEventArgs e)
    {
        ViewState["GridViewSortExp"] = e.SortExpression;
        ViewState["GridViewSortDirection"] = e.SortDirection;

        if (SortDir == "ASC")
            GridViewSortDirection = SortDirection.Ascending;
        else
            GridViewSortDirection = SortDirection.Descending;

        var GetAllAssignedVendors = (List<DISCnt_AccountPayableContactBE>)ViewState["VendorDetailTable"];
        var ShotredData = new List<DISCnt_AccountPayableContactBE>();
        if (!string.IsNullOrEmpty(txtVendor.Text) && ddlCountry.innerControlddlCountry.SelectedIndex > 0)
            ShotredData = GetAllAssignedVendors.Where(av => av.CountryID == Convert.ToInt32(ddlCountry.innerControlddlCountry.SelectedValue)
                && av.Vendor.VendorName.StartsWith(txtVendor.Text.Trim(), StringComparison.OrdinalIgnoreCase)).ToList();
        else if (!string.IsNullOrEmpty(txtVendor.Text))
            ShotredData = GetAllAssignedVendors.Where(av => av.Vendor.VendorName.StartsWith(txtVendor.Text.Trim(), StringComparison.OrdinalIgnoreCase)).ToList();
        else if (ddlCountry.innerControlddlCountry.SelectedIndex > 0)
            ShotredData = GetAllAssignedVendors.Where(av => av.CountryID == Convert.ToInt32(ddlCountry.innerControlddlCountry.SelectedValue)).ToList();
        else
            ShotredData = GetAllAssignedVendors;

        return Utilities.GenericListHelper<DISCnt_AccountPayableContactBE>.SortList(ShotredData, e.SortExpression, GridViewSortDirection).ToArray();
        //return Utilities.GenericListHelper<DISCnt_AccountPayableContactBE>.SortList((List<DISCnt_AccountPayableContactBE>)ViewState["VendorDetailTable"], e.SortExpression, GridViewSortDirection).ToArray();
    }

    #endregion    
}
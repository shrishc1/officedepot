﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="../../../CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    CodeFile="DISCnt_AccountPayableAddVendor.aspx.cs" Inherits="DISCnt_AccountPayableAddVendor" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register TagName="ucVendorSearch" TagPrefix="cc2" Src="~/CommonUI/UserControls/ucSeacrhVendor.ascx" %>
<%@ Register TagName="ucCountry" TagPrefix="cc3" Src="~/CommonUI/UserControls/ucCountry.ascx" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<%@ Register Src="../../../CommonUI/UserControls/ucExportToExcel.ascx" TagName="ucExportToExcel"
    TagPrefix="cc3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script language="javascript" type="text/javascript">
        var gridCheckedCount = 0;


        $(document).ready(function () {
            SelectAllGrid($("#chkSelectAllText")[0]);
        });



        function SelectAllGrid(chkBoxAllObj) {
            if (chkBoxAllObj.checked) {
                $("input[type='checkbox'][name$='chkSelect']").attr('checked', true);
                gridCheckedCount = $("input[type='checkbox'][name$='chkSelect']:checked").size();
            }
            else {
                $("input[type='checkbox'][name$='chkSelect']").attr('checked', false);
                gridCheckedCount = 0;
            }
        }

        function CheckUncheckAllCheckBoxAsNeeded() {

            if ($("input[type='checkbox'][name$='chkSelect']:checked").size() == $("input[type='checkbox'][name$='chkSelect']").length) {
                $("#chkSelectAllText").attr('checked', true);
                gridCheckedCount = $("#<%=grdAccountPayable.ClientID%> input[id*='chkSelect']:checkbox:checked").size();
            }
            else {
                $("#chkSelectAllText").attr('checked', false);
                gridCheckedCount = $("#<%=grdAccountPayable.ClientID%> input[id*='chkSelect']:checkbox:checked").size();
            }
        }


        function ReassignConfirmation() {
            var ReassignConfMessage = '<%=vendorAddConfirmation%>';
            var SelectCheckboxToReassignVendor = '<%=selectCheckboxToAddVendor%>';

            if (gridCheckedCount > 30) {
                gridCheckedCount = 30;
            }

            if (gridCheckedCount > 0) {
                ReassignConfMessage = ReassignConfMessage.replace('##VendorCount##', gridCheckedCount);

                if (confirm(ReassignConfMessage)) {
                    return true;
                } else {
                    return false;
                }
            }
            else {
                alert(SelectCheckboxToReassignVendor);
                return false
            }
        }
    </script>
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="up1" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <h2>
                <cc1:ucLabel ID="lblAPContact" runat="server" Text="Accounts Payable Contact - Add Vendors"></cc1:ucLabel>
            </h2>
            <div class="right-shadow">
                <div class="formbox">
                    <br />
                    <cc1:ucPanel ID="pnlAccountPayable" runat="server" CssClass="fieldset-form">
                        <table width="80%" cellspacing="5" cellpadding="0" border="0" class="form-table">
                            <tr>
                                <td width="50%" style="font-weight: bold;">
                                    <cc1:ucLabel ID="lblName1" runat="server">AP Clerk Name</cc1:ucLabel>
                                </td>
                                <td width="50%" class="nobold">
                                    <cc1:ucLabel ID="NameValue" runat="server"></cc1:ucLabel>
                                </td>
                            </tr>
                        </table>
                    </cc1:ucPanel>
                    <table width="50%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                        <tr>
                            <td style="font-weight: bold;">
                                <cc1:ucLabel ID="lblCountry" runat="server"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold; width: 5px">
                                :
                            </td>
                            <td>
                                <cc3:ucCountry ID="ddlCountry" runat="server" />
                            </td>
                            <td style="font-weight: bold;">
                                <cc1:ucLabel ID="lblVendor" runat="server"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold; width: 5px">
                                :
                            </td>
                            <td>
                                <cc1:ucTextbox ID="txtVendor" runat="server" />
                            </td>
                            <td>
                                <cc1:ucButton ID="btnSearch" runat="server" OnClick="btnSearch_Click" CssClass="button" />
                            </td>
                        </tr>
                    </table>
                    <table width="100%">
                        <tr>
                            <td width="100%">
                                <cc1:ucGridView ID="grdAccountPayable" ClientIDMode="Static" Width="99%" runat="server"
                                    AutoGenerateColumns="False" CssClass="grid" OnSorting="SortGridView" OnPageIndexChanging="grdAccountPayable_PageIndexChanging"
                                    AllowPaging="true" PageSize="30" AllowSorting="true">
                                    <Columns>
                                        <asp:TemplateField HeaderText="SelectAllCheckBox">
                                            <HeaderStyle HorizontalAlign="Left" Width="10%" />
                                            <ItemStyle Width="10%" HorizontalAlign="Left" VerticalAlign="Middle" />
                                            <HeaderTemplate>
                                                <cc1:ucCheckbox runat="server" ID="chkSelectAllText" CssClass="checkbox-input" onclick="SelectAllGrid(this);" />
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:HiddenField ID="hdnVendorID" Value='<%# Eval("VendorID") %>' runat="server" />
                                                <asp:CheckBox runat="server" ViewStateMode="Enabled" ID="chkSelect" onclick="CheckUncheckAllCheckBoxAsNeeded();" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField SortExpression="Vendor.CountryName" HeaderText="Country">
                                            <HeaderStyle Width="15%" HorizontalAlign="Left" />
                                            <ItemStyle HorizontalAlign="Left" Width="15%" />
                                            <ItemTemplate>
                                                <asp:Literal ID="ltrCountryNameT" Text='<%# Eval("Vendor.CountryName") %>' runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField SortExpression="Vendor.Vendor_No" HeaderText="VendorNumber">
                                            <HeaderStyle Width="15%" HorizontalAlign="Left" />
                                            <ItemStyle HorizontalAlign="Left" Width="15%" />
                                            <ItemTemplate>
                                                <asp:Literal ID="ltVendorNoT" Text='<%# Eval("Vendor.Vendor_No") %>' runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField SortExpression="Vendor.VendorName" HeaderText="VendorName">
                                            <HeaderStyle Width="15%" HorizontalAlign="Left" />
                                            <ItemStyle HorizontalAlign="Left" Width="15%" />
                                            <ItemTemplate>
                                                <asp:Literal ID="ltVendorNameT" Text='<%# Eval("Vendor.VendorName") %>' runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </cc1:ucGridView>
                            </td>
                        </tr>
                    </table>
                    <div class="button-row">
                        <cc1:ucButton ID="btnAdd" runat="server" OnClientClick="return ReassignConfirmation();"
                            ClientIDMode="Static" Text="Add" CssClass="button" OnClick="btnAdd_Click" />
                        <cc1:ucButton ID="btnBack" runat="server" Text="Back" CssClass="button" OnClick="btnBack_Click" />
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

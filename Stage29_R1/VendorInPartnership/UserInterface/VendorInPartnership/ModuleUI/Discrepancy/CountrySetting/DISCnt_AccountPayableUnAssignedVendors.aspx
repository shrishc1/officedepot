﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="DISCnt_AccountPayableUnAssignedVendors.aspx.cs"
    MasterPageFile="../../../CommonUI/CMN_MasterPages/CRUD_MasterPage.Master" Inherits="DISCnt_AccountPayableUnAssignedVendors" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register TagName="ucVendorSearch" TagPrefix="cc2" Src="~/CommonUI/UserControls/ucSeacrhVendor.ascx" %>
<%@ Register TagName="ucCountry" TagPrefix="cc3" Src="~/CommonUI/UserControls/ucCountry.ascx" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<%@ Register Src="../../../CommonUI/UserControls/ucExportToExcel.ascx" TagName="ucExportToExcel"
    TagPrefix="cc3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script language="javascript" type="text/javascript">
        function AlertForExistance(arg) {
            var objExists = window.confirm(arg);
            if (objExists == true) {
                return true;
            }
            else {
                return false;
            }
        }
    </script>
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="up1" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <h2>
                <cc1:ucLabel ID="lblUnAssignedVendor" runat="server" Text="Unassigned Vendors"></cc1:ucLabel>
            </h2>
            <table width="100%">
                <tr>
                    <td align="center">
                        <%-- <asp:GridView ID="grdAccountPayable" ClientIDMode="Static" Width="100%" runat="server"
                            AutoGenerateColumns="False" CssClass="grid" OnSorting="grdAccountPayable_Sorting"
                            OnPageIndexChanging="grdAccountPayable_PageIndexChanging" AllowPaging="true"
                            PageSize="30" AllowSorting="true">--%>
                        <cc1:ucGridView ID="grdAccountPayable" Width="100%" runat="server" CssClass="grid"
                            OnSorting="grdAccountPayable_Sorting" ClientIDMode="Static" 
                            OnPageIndexChanging="grdAccountPayable_PageIndexChanging" AllowPaging="true"
                            PageSize="30" AllowSorting="true">
                            <Columns>
                                <asp:BoundField HeaderText="Country" DataField="Vendor_Country" SortExpression="Vendor_Country">
                                    <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                    <ItemStyle HorizontalAlign="Left" />
                                </asp:BoundField>
                                <asp:BoundField HeaderText="Vendor Name" SortExpression="Vendor_Name" DataField="Vendor_Name">
                                    <HeaderStyle Width="250px" HorizontalAlign="Left" />
                                </asp:BoundField>
                                <asp:BoundField HeaderText="Vendor #" DataField="VendorID" SortExpression="VendorID">
                                    <HeaderStyle Width="175px" HorizontalAlign="Left" />
                                    <ItemStyle HorizontalAlign="Left" />
                                </asp:BoundField>
                            </Columns>
                        </cc1:ucGridView>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

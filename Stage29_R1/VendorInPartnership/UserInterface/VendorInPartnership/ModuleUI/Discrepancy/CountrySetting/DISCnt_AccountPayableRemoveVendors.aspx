﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    CodeFile="DISCnt_AccountPayableRemoveVendors.aspx.cs" Inherits="DISCnt_AccountPayableRemoveVendors" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register TagName="ucVendorSearch" TagPrefix="cc2" Src="~/CommonUI/UserControls/ucSeacrhVendor.ascx" %>
<%@ Register TagName="ucCountry" TagPrefix="cc3" Src="~/CommonUI/UserControls/ucCountry.ascx" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<%@ Register Src="../../../CommonUI/UserControls/ucExportToExcel.ascx" TagName="ucExportToExcel"
    TagPrefix="cc3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script language="javascript" type="text/javascript">
        var gridCheckedCount = 0;

        $(document).ready(function () {
            SelectAllGrid($("#chkSelectAllText")[0]);
        });

        function SelectAllGrid(chkBoxAllObj) {
            if (chkBoxAllObj.checked) {
                $("input[type='checkbox'][name$='chkSelect']").attr('checked', true);
                gridCheckedCount = $("input[type='checkbox'][name$='chkSelect']:checked").size();
            }
            else {
                $("input[type='checkbox'][name$='chkSelect']").attr('checked', false);
                gridCheckedCount = 0;
            }
        }

        function CheckUncheckAllCheckBoxAsNeeded() {

            if ($("input[type='checkbox'][name$='chkSelect']:checked").size() == $("input[type='checkbox'][name$='chkSelect']").length) {
                $("#chkSelectAllText").attr('checked', true);
                gridCheckedCount = $("#<%=grdAccountPayable.ClientID%> input[id*='chkSelect']:checkbox:checked").size();
            }
            else {
                $("#chkSelectAllText").attr('checked', false);
                gridCheckedCount = $("#<%=grdAccountPayable.ClientID%> input[id*='chkSelect']:checkbox:checked").size();
            }
        }

        function RemoveConfirmation() {
            var RemoveConfirmation = '<%=vendorRemoveConfirmation%>';
            var SelectCheckboxToRemoveVendor = '<%=selectCheckboxToRemoveVendor%>';

            if (gridCheckedCount > 30) {
                gridCheckedCount = 30;
            }

            if (gridCheckedCount > 0) {
                RemoveConfirmation = RemoveConfirmation.replace('##VendorCount##', gridCheckedCount);
                RemoveConfirmation = RemoveConfirmation.replace('##CLERKSNAME##', $('#<%=lblAPClerkNameT.ClientID%>').text());

                if (confirm(RemoveConfirmation)) {
                    return true;
                } else {
                    return false;
                }
            }
            else {
                alert(SelectCheckboxToRemoveVendor);
                return false
            }
        }
    </script>
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="up1" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <h2>
                <cc1:ucLabel ID="lblAPCRemoveVendors" runat="server" Text="Accounts Payable Contact - Remove Vendors"></cc1:ucLabel>
            </h2>
            <div class="right-shadow">
                <div class="formbox">
                    <table width="70%" cellspacing="5" cellpadding="0" border="0" class="top-settings">
                        <tr>
                            <td style="font-weight: bold; width: 20%">
                                <cc1:ucLabel ID="lblAPClerkName" runat="server" Text="AP Clerk Name"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold; width: 1%">
                                :
                            </td>
                            <td style="width: 29%">
                                <cc1:ucLabel ID="lblAPClerkNameT" ClientIDMode="Static" runat="server" Text="OD AP Clerk Name"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold; width: 20%">
                            </td>
                            <td style="font-weight: bold; width: 1%">
                            </td>
                            <td style="width: 29%">
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold; width: 20%">
                                <cc1:ucLabel ID="lblCountry" runat="server" Text="Country"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold; width: 1%">
                                :
                            </td>
                            <td style="width: 29%">
                                <cc3:ucCountry ID="ddlCountry" runat="server" Width="206px" />
                            </td>
                            <td style="font-weight: bold; width: 20%">
                                <cc1:ucLabel ID="lblVendor" runat="server"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold; width: 1%">
                                :
                            </td>
                            <td style="width: 29%">
                                <cc1:ucTextbox ID="txtVendor" runat="server" Width="200px" />
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold; width: 20%">
                            </td>
                            <td style="font-weight: bold; width: 1%">
                            </td>
                            <td style="width: 29%">
                            </td>
                            <td style="font-weight: bold; width: 20%">
                            </td>
                            <td style="font-weight: bold; width: 1%">
                            </td>
                            <td style="width: 29%">
                                <cc1:ucButton ID="btnSearch" runat="server" OnClick="btnSearch_Click" Text="Search"
                                    CssClass="button" />
                            </td>
                        </tr>
                    </table>
                    <table width="100%" cellspacing="1" cellpadding="0" border="0" align="center">
                        <tr>
                            <td colspan="6">
                                <br />
                            </td>
                        </tr>
                    </table>
                    <table width="100%" cellspacing="1" cellpadding="0" border="0" align="center">
                        <tr>
                            <td colspan="6" align="center" valign="top">
                                <cc1:ucGridView ID="grdAccountPayable" ClientIDMode="Static" Width="100%" runat="server"
                                    AutoGenerateColumns="false" CssClass="grid" OnSorting="SortGridView" OnPageIndexChanging="grdAccountPayable_PageIndexChanging"
                                    AllowPaging="true" PageSize="30" AllowSorting="true">
                                    <Columns>
                                        <asp:TemplateField HeaderText="SelectAllCheckBox">
                                            <HeaderStyle HorizontalAlign="Left" Width="10%" />
                                            <ItemStyle Width="10%" HorizontalAlign="Left" VerticalAlign="Middle" />
                                            <HeaderTemplate>
                                             <cc1:ucCheckbox runat="server" ID="chkSelectAllText" 
                                            CssClass="checkbox-input" onclick="SelectAllGrid(this);" />   
                                               <%-- <asp:CheckBox runat="server" ID="chkSelectAll" Text="<%$Resources:OfficeDepot, SelectAllText%>"
                                                    CssClass="checkbox-input" onclick="SelectAllGrid(this);" />--%>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:CheckBox runat="server" ViewStateMode="Enabled" ID="chkSelect" onclick="CheckUncheckAllCheckBoxAsNeeded();" />
                                                <asp:HiddenField ID="hdnVendorId" Value='<%# Eval("VendorID") %>' runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField SortExpression="Country.CountryName" HeaderText="Country">
                                            <HeaderStyle Width="15%" HorizontalAlign="Left" />
                                            <ItemStyle HorizontalAlign="Left" Width="15%" />
                                            <ItemTemplate>
                                                <asp:Literal ID="ltrCountryNameT" Text='<%# Eval("Country.CountryName") %>' runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField SortExpression="Vendor.Vendor_No" HeaderText="VendorNumber">
                                            <HeaderStyle Width="15%" HorizontalAlign="Left" />
                                            <ItemStyle HorizontalAlign="Left" Width="15%" />
                                            <ItemTemplate>
                                                <asp:Literal ID="ltVendorNoT" Text='<%# Eval("Vendor.Vendor_No") %>' runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField SortExpression="Vendor.VendorName" HeaderText="VendorName">
                                            <HeaderStyle Width="15%" HorizontalAlign="Left" />
                                            <ItemStyle HorizontalAlign="Left" Width="15%" />
                                            <ItemTemplate>
                                                <asp:Literal ID="ltVendorNameT" Text='<%# Eval("Vendor.VendorName") %>' runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </cc1:ucGridView>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6" align="right" valign="top">
                                <br />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6">
                                <div class="button-row" style="text-align: right">
                                    <cc1:ucButton ID="btnRemove" runat="server" Text="Remove" CssClass="button" OnClick="btnRemove_Click"
                                        OnClientClick="return RemoveConfirmation();" />&nbsp;&nbsp;
                                    <cc1:ucButton ID="btnBack" runat="server" Text="Back" CssClass="button" OnClick="btnBack_Click" />
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

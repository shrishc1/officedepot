﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using Utilities;
using WebUtilities;
using BusinessEntities.ModuleBE.Discrepancy.CountrySetting;
using BusinessLogicLayer.ModuleBAL.Discrepancy.CountrySetting;
using System.Linq;


public partial class DISCnt_AccountPayableAddVendor : CommonPage
{
    #region Declarations ...
    string warningMessage = WebCommon.getGlobalResourceValue("AssignVendorConfirmation");
    string vendorAddedConfirmation = WebCommon.getGlobalResourceValue("VendorAddedConfirmation");
    protected string vendorAddConfirmation = WebCommon.getGlobalResourceValue("VendorAddConfirmation");
    protected string selectCheckboxToAddVendor = WebCommon.getGlobalResourceValue("SelectCheckboxToAddVendor");

    public SortDirection GridViewSortDirection
    {
        get
        {
            if (ViewState["GridViewSortDirection"] == null)
                ViewState["GridViewSortDirection"] = SortDirection.Ascending;
            return (SortDirection)ViewState["GridViewSortDirection"];
        }
        set { ViewState["GridViewSortDirection"] = value; }
    }

    public string SortDir
    {
        get
        {
            if (GridViewSortDirection == SortDirection.Ascending)
            {
                return "DESC";
            }
            else
            {
                return "ASC";
            }
        }
    }

    public string GridViewSortExp
    {
        get
        {
            if (ViewState["GridViewSortExp"] == null)
                ViewState["GridViewSortExp"] = "Vendor.VendorName";
            return (string)ViewState["GridViewSortExp"];
        }
        set { ViewState["GridViewSortExp"] = value; }
    }

    #endregion

    #region Events ...
    protected void Page_Load(object sender, EventArgs e)
    {
        ddlCountry.CurrentPage = this;
        ddlCountry.IsAllRequired = true;
        ddlCountry.IsAllDefault = true;
        
        if (!Page.IsPostBack)
        {            
            if (GetQueryStringValue("UserName") != null)
            {
                NameValue.Text = GetQueryStringValue("UserName").ToString();
                GetUnAssignedVendors();
            }
        }
    }

    protected void grdAccountPayable_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        if (ViewState["VendorDetailTable"] != null)
        {
            grdAccountPayable.PageIndex = e.NewPageIndex;
            SortGridView(GridViewSortExp, GridViewSortDirection);
        }
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        EncryptQueryString("DISCnt_AccountPayableContactDetail.aspx");
    }

    protected void btnAdd_Click(object sender, EventArgs e)
    {
        DISCnt_AccountPayableContactBE oDISCnt_AccountPayableContactBE = new DISCnt_AccountPayableContactBE();
        DISCNT_AccountPayableContactBAL oDISCnt_AccountPayableContactBAL = new DISCNT_AccountPayableContactBAL();

        oDISCnt_AccountPayableContactBE.UserID = Convert.ToInt32(GetQueryStringValue("UserID"));
        oDISCnt_AccountPayableContactBE.Action = "Edit";       
        oDISCnt_AccountPayableContactBE.IsAllVendors = 0;

        foreach (GridViewRow row in grdAccountPayable.Rows)
        {
            CheckBox cbox = row.FindControl("chkSelect") as CheckBox;
            if (cbox != null)
            {
                if (cbox.Checked)
                {
                    HiddenField field = row.FindControl("hdnVendorID") as HiddenField;
                    oDISCnt_AccountPayableContactBE.VendorIDs += field.Value + ",";
                }                
            }
        }

        if (!string.IsNullOrEmpty(oDISCnt_AccountPayableContactBE.VendorIDs))
        {
            if(oDISCnt_AccountPayableContactBE.VendorIDs.Substring(oDISCnt_AccountPayableContactBE.VendorIDs.Length -1,1).Equals(","))
                oDISCnt_AccountPayableContactBE.VendorIDs = oDISCnt_AccountPayableContactBE.VendorIDs.Remove(oDISCnt_AccountPayableContactBE.VendorIDs.Length - 1, 1);

        }
        
        int? iResult = oDISCnt_AccountPayableContactBAL.addEditAccountPayableContactDetailsBAL(oDISCnt_AccountPayableContactBE);
        oDISCnt_AccountPayableContactBAL = null;
        
        if (ViewState["GridViewSortDirection"] != null)
            this.GetUnAssignedVendors(Convert.ToString(ViewState["GridViewSortExp"]), (SortDirection)ViewState["GridViewSortDirection"]);
        else
            this.GetUnAssignedVendors(Convert.ToString(ViewState["GridViewSortExp"]), SortDirection.Ascending);
        
        SortGridView(GridViewSortExp, GridViewSortDirection);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert1", "alert('" + vendorAddedConfirmation + "')", true);
        //Again bind the grid to refresh the records       

    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        this.Search();
    }
    #endregion

    #region Methods ...
    private void GetUnAssignedVendors(string gridViewSortExp = "", SortDirection gridViewSortDirection = SortDirection.Ascending)
    {
        DISCnt_AccountPayableContactBE oDISCnt_AccountPayableContactBE = new DISCnt_AccountPayableContactBE();
        DISCNT_AccountPayableContactBAL oDISCnt_AccountPayableContactBAL = new DISCNT_AccountPayableContactBAL();
 

        oDISCnt_AccountPayableContactBE.Action = "UnAssignedVendors";
        var GetAllAssignedVendors = oDISCnt_AccountPayableContactBAL.GetAllUnAssignedVendorsBAL(oDISCnt_AccountPayableContactBE);

        if (GetAllAssignedVendors.Count > 0)
        {
            ViewState["VendorDetailTable"] = GetAllAssignedVendors;
            grdAccountPayable.DataSource = GetAllAssignedVendors;
            grdAccountPayable.DataBind();

            if (string.IsNullOrEmpty(gridViewSortExp))
                GridViewSortExp = "Vendor.VendorName";
            else
                GridViewSortExp = gridViewSortExp;

            if (gridViewSortDirection == null)
                GridViewSortDirection = SortDirection.Ascending;
            else
                GridViewSortDirection = gridViewSortDirection;  
        }
        else
        {
            ViewState["VendorDetailTable"] = null;
            grdAccountPayable.DataSource = null;
            grdAccountPayable.DataBind();
        }

        oDISCnt_AccountPayableContactBAL = null;
    }

    private void Search()
    { 

        var GetAllAssignedVendors = (List<DISCnt_AccountPayableContactBE>)ViewState["VendorDetailTable"];

        if (!string.IsNullOrEmpty(txtVendor.Text) && ddlCountry.innerControlddlCountry.SelectedIndex > 0)
            grdAccountPayable.DataSource = GetAllAssignedVendors.Where(av => av.Vendor.CountryName == ddlCountry.innerControlddlCountry.SelectedItem.Text
                && av.Vendor.VendorName.StartsWith(txtVendor.Text.Trim(), StringComparison.OrdinalIgnoreCase)).ToList();
        else if (!string.IsNullOrEmpty(txtVendor.Text))
            grdAccountPayable.DataSource = GetAllAssignedVendors.Where(av => av.Vendor.VendorName.StartsWith(txtVendor.Text.Trim(), StringComparison.OrdinalIgnoreCase)).ToList();
        else if (ddlCountry.innerControlddlCountry.SelectedIndex > 0)
            grdAccountPayable.DataSource = GetAllAssignedVendors.Where(av => av.Vendor.CountryName == ddlCountry.innerControlddlCountry.SelectedItem.Text).ToList();
        else
            grdAccountPayable.DataSource = GetAllAssignedVendors;

        grdAccountPayable.DataBind();
    }

    private void SortGridView(string sortExpression, SortDirection direction)
    {
        List<DISCnt_AccountPayableContactBE> lstVendor = new List<DISCnt_AccountPayableContactBE>();
        try
        {
            if (ViewState["VendorDetailTable"] != null)
            {
              

                var GetAllAssignedVendors = (List<DISCnt_AccountPayableContactBE>)ViewState["VendorDetailTable"];
                var ShotredData = new List<DISCnt_AccountPayableContactBE>();
                if (!string.IsNullOrEmpty(txtVendor.Text) && ddlCountry.innerControlddlCountry.SelectedIndex > 0)
                    ShotredData = GetAllAssignedVendors.Where(av => av.Vendor.CountryName == ddlCountry.innerControlddlCountry.SelectedItem.Text
                        && av.Vendor.VendorName.StartsWith(txtVendor.Text.Trim(), StringComparison.OrdinalIgnoreCase)).ToList();
                else if (!string.IsNullOrEmpty(txtVendor.Text))
                    ShotredData = GetAllAssignedVendors.Where(av => av.Vendor.VendorName.StartsWith(txtVendor.Text.Trim(), StringComparison.OrdinalIgnoreCase)).ToList();
                else if (ddlCountry.innerControlddlCountry.SelectedIndex > 0)
                    ShotredData = GetAllAssignedVendors.Where(av => av.Vendor.CountryName == ddlCountry.innerControlddlCountry.SelectedItem.Text).ToList();
                else
                    ShotredData = GetAllAssignedVendors;

                lstVendor = Utilities.GenericListHelper<DISCnt_AccountPayableContactBE>.SortList(ShotredData, sortExpression, direction);
                grdAccountPayable.DataSource = lstVendor;
                grdAccountPayable.DataBind();
                CommonPage c = new CommonPage();
                c.LocalizeGridHeader(grdAccountPayable);                
            }
        }
        catch (Exception ex)
        {
            LogUtility.SaveErrorLogEntry(ex);
        }
    }

    public override BusinessEntities.BaseBe[] SubSort(GridViewSortEventArgs e)
    {
        ViewState["GridViewSortExp"] = e.SortExpression;
        ViewState["GridViewSortExp"] = e.SortExpression;
        ViewState["GridViewSortDirection"] = e.SortDirection;

        if (SortDir == "ASC")
            GridViewSortDirection = SortDirection.Ascending;
        else
            GridViewSortDirection = SortDirection.Descending;

        var GetAllAssignedVendors = (List<DISCnt_AccountPayableContactBE>)ViewState["VendorDetailTable"];
        var ShotredData = new List<DISCnt_AccountPayableContactBE>();
        if (!string.IsNullOrEmpty(txtVendor.Text) && ddlCountry.innerControlddlCountry.SelectedIndex > 0)
            ShotredData = GetAllAssignedVendors.Where(av => av.Vendor.CountryName == ddlCountry.innerControlddlCountry.SelectedItem.Text
                && av.Vendor.VendorName.StartsWith(txtVendor.Text.Trim(), StringComparison.OrdinalIgnoreCase)).ToList();
        else if (!string.IsNullOrEmpty(txtVendor.Text))
            ShotredData = GetAllAssignedVendors.Where(av => av.Vendor.VendorName.StartsWith(txtVendor.Text.Trim(), StringComparison.OrdinalIgnoreCase)).ToList();
        else if (ddlCountry.innerControlddlCountry.SelectedIndex > 0)
            ShotredData = GetAllAssignedVendors.Where(av => av.Vendor.CountryName == ddlCountry.innerControlddlCountry.SelectedItem.Text).ToList();
        else
            ShotredData = GetAllAssignedVendors;

        return Utilities.GenericListHelper<DISCnt_AccountPayableContactBE>.SortList(ShotredData, e.SortExpression, GridViewSortDirection).ToArray();
    }

    #endregion
}
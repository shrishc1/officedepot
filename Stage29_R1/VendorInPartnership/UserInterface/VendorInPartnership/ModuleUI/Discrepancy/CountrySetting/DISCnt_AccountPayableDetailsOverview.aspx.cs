﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using Utilities; using WebUtilities;
using BusinessEntities.ModuleBE.Discrepancy.CountrySetting;
using BusinessLogicLayer.ModuleBAL.Discrepancy.CountrySetting;

public partial class DISCNT_AccountPayableDetailsOverview : CommonPage {
    protected void Page_Load(object sender, EventArgs e) {
       
        if (!IsPostBack) {
            BindAccountPayableContacts();
        }
        btnExportToExcel1.GridViewControl = grdAccountPayable;
        btnExportToExcel1.FileName = "Dis-AccountPayableDetailsOverview";
    }

    #region Methods

    private void BindAccountPayableContacts() {
        DISCnt_AccountPayableContactBE oDISCnt_AccountPayableContactBE = new DISCnt_AccountPayableContactBE();
        DISCNT_AccountPayableContactBAL oDISCnt_AccountPayableContactBAL = new DISCNT_AccountPayableContactBAL();

        oDISCnt_AccountPayableContactBE.Action = "ShowAll";
        DataTable dt = oDISCnt_AccountPayableContactBAL.GetAccountPayableContactDetailsBAL(oDISCnt_AccountPayableContactBE,"");
        oDISCnt_AccountPayableContactBAL = null;
        if (dt != null && dt.Rows.Count > 0) {
            grdAccountPayable.DataSource = dt;
            grdAccountPayable.DataBind();
            ViewState["myDataTable"] = dt;
        }
    }

   




    #endregion

    #region Events
    protected void grdAccountPayable_Sorting(object sender, GridViewSortEventArgs e)
    {
        if (ViewState["myDataTable"] != null)
        {
            DataTable myDataTable = (DataTable)ViewState["myDataTable"];
            ViewState["sortDirection"] = ViewState["sortDirection"] == null || Convert.ToString(ViewState["sortDirection"]) == "DESC" ? "ASC" : "DESC";
            DataView view = myDataTable.DefaultView;
            view.Sort = e.SortExpression + " " + Convert.ToString(ViewState["sortDirection"]);
            grdAccountPayable.DataSource = view;
            grdAccountPayable.DataBind();

        }
    }
    #endregion
}

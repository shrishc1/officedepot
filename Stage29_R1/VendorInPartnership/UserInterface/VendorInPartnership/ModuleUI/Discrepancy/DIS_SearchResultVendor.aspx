﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    AutoEventWireup="true" CodeFile="DIS_SearchResultVendor.aspx.cs" Inherits="DIS_SearchResultVendor" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectVendor.ascx" TagName="MultiSelectVendor" TagPrefix="cc2" %>
<%@ Register Src="../../CommonUI/UserControls/ucExportToExcel.ascx" TagName="ucExportToExcel" TagPrefix="cc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script language="javascript" type="text/javascript">
        $(document).ready(function () {
            var lstitem = document.getElementById('<%= UclstSiteSelected.ClientID %>');
            if (lstitem != null) {
                if (lstitem.options.length == 0) {
                    document.getElementById('<%= hiddenSelectedIDs.ClientID %>').value = "";
                    document.getElementById('<%= hiddenSelectedName.ClientID %>').value = "";
                }
            }
        });

        function ClearHiddenFiels(ctrlHiddenSelectedId, ctrlHiddenSelectedName) {
            var HiddenSelectedId = document.getElementById(ctrlHiddenSelectedId);
            var HiddenSelectedName = document.getElementById(ctrlHiddenSelectedName);
            var lstitem = document.getElementById('<%= UclstSiteSelected.ClientID %>');
            if (lstitem != null) {
                HiddenSelectedId.value = '';
                HiddenSelectedName.value = '';
                for (var index = 0; index < lstitem.options.length; index++) {
                    var theOption = new Option;
                    if (index == 0) {
                        HiddenSelectedId.value = lstitem.options[index].value;
                        HiddenSelectedName.value = lstitem.options[index].text;
                    }
                    else {
                        HiddenSelectedId.value = HiddenSelectedId.value + ',' + lstitem.options[index].value;
                        HiddenSelectedName.value = HiddenSelectedName.value + ',' + lstitem.options[index].text;
                    }                    
                }
            }            
        }

    </script>
    <h2>
        <cc1:ucLabel ID="lblSearchResult" runat="server"></cc1:ucLabel>
    </h2>
    <asp:ScriptManager ID="sm1" runat="server"></asp:ScriptManager>
    <div class="button-row">        
        <cc2:ucExportToExcel ID="ucExportToExcel1" runat="server" Visible="false" />
    </div>
    <div class="right-shadow">
        <div class="formbox">
            <div class="button-row">
                <%--<cc1:ucButton ID="btnRunScheduler" runat="server" Text="Run Scheduler" CssClass="button"
                    onkeypress="disableEnterKey(this)" OnClick="btnScheduler_Click" />--%></div>
            <cc1:ucMultiView ID="mvDiscrepancyList" runat="server" ActiveViewIndex="0">
                <cc1:ucView ID="vwSearchCreteria" runat="server">
                    <table width="70%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                        <tr>
                            <td style="font-weight: bold;width:14%">
                               <cc1:ucLabel ID="lblVendor" runat="server" Text="Vendor"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold;width:1%">
                                :
                            </td>
                            <td style="font-weight: bold;width:19%">
                                <cc1:ucDropdownList runat="server" ID="ddlVendor" Width="150px" />
                            </td>
                            <td style="width:1%"></td>
                            <td style="font-weight: bold;width:19%">
                               <cc1:ucLabel ID="lblStatus" runat="server" Text="Status"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold;width:1%">
                                <cc1:ucLabel ID="UcLabel11" runat="server">:</cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold;width:44%">
                                <asp:DropDownList ID="ddlStatus" runat="server" Width="150px">
                                    <asp:ListItem Text="--Select--" Value="" />
                                    <asp:ListItem Text="Open" Value="O" />
                                    <asp:ListItem Text="Forced Closed" Value="F" />
                                    <asp:ListItem Text="Closed" Value="C" />
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold;">
                               <cc1:ucLabel ID="lblDiscrepancyType" runat="server" Text="Discrepancy Type"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold;">
                                :
                            </td>
                            <td style="font-weight: bold;">
                                <cc1:ucDropdownList runat="server" ID="ddlDiscrepancyType" Width="150px" />
                            </td>
                             <td style="width:1%"></td>
                            <td style="font-weight: bold;">
                                <cc1:ucLabel ID="lblDiscrepancyNo" runat="server" Text="Discrepancy #"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold;">
                                &nbsp;<cc1:ucLabel ID="UcLabel5" runat="server">:</cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold;">
                                <cc1:ucTextbox ID="txtDiscrepancyNo" runat="server" Width="145px"></cc1:ucTextbox>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold;">
                              <cc1:ucLabel ID="lblDiscrepancyDateRange" runat="server" Text="Discrepancy Date Range"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold;">
                                <cc1:ucLabel ID="UcLabel8" runat="server">:</cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold;">
                                <asp:TextBox ID="txtDiscrepancyDateRangeFrom" runat="server" ClientIDMode="Static"
                                    class="date" />
                            </td>
                            <td style="width:1%"></td>
                            <td style="font-weight: bold;" align="Left">
                                <cc1:ucLabel ID="lblTo" runat="server" Text="To"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold;">
                                <cc1:ucLabel ID="UcLabel3" runat="server">:</cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold;">
                                <asp:TextBox ID="txtDiscrepancyDateRangeTo" runat="server" ClientIDMode="Static"
                                    class="date" />
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold;">
                              <cc1:ucLabel ID="lblPurchaseOrderNo" runat="server" Text="Purchase Order #"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold;">
                                <cc1:ucLabel ID="UcLabel7" runat="server">:</cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold;">
                                <cc1:ucTextbox ID="txtPurchaseOrder" runat="server" Width="145px"></cc1:ucTextbox>
                            </td>
                            <td style="width:1%"></td>
                            <td style="font-weight: bold; text-align: left;">
                                <%--<cc1:ucLabel ID="lblPurchaseOrderDate" runat="server" Text="Purchase Order Date"></cc1:ucLabel>--%>
                            </td>
                            <td style="font-weight: bold;">
                                <%--<cc1:ucLabel ID="UcLabel1" runat="server">:</cc1:ucLabel>--%>
                            </td>
                            <td style="font-weight: bold;">
                                <%--<asp:TextBox ID="txtPurchaseOrderDate" runat="server" ClientIDMode="Static" class="date" />--%>
                            </td>
                        </tr>
                   
                        <tr>
                            <td style="font-weight: bold; width: 23%;vertical-align:top">
                               <cc1:ucLabel ID="lblSite" runat="server" Text="Site"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold; width: 1%">
                                <cc1:ucLabel ID="UcLabel2" runat="server">:</cc1:ucLabel>
                            </td>
                            <td width="30%">
                                <cc1:ucListBox ID="UclstSiteList" runat="server" Height="150px" Width="150px">
                                </cc1:ucListBox>
                            </td>
                            <td style="width:1%"></td>
                            <td valign="middle" align="left" width="20%" colspan="1">
                                <div>
                                    <input type="button" id="btnMoveRight" value=">" class="button" style="width: 35px"
                                        onclick="Javascript:MoveItem('<%= UclstSiteList.ClientID %>', '<%= UclstSiteSelected.ClientID %>','<%= hiddenSelectedIDs.ClientID %>','<%= hiddenSelectedName.ClientID %>');ClearHiddenFiels('<%= hiddenSelectedIDs.ClientID %>','<%= hiddenSelectedName.ClientID %>');" />
                                </div>
                                &nbsp;
                                <div>
                                    <input type="button" id="btnMoveLeft" value="<" class="button" style="width: 35px"
                                        onclick="Javascript:MoveItem('<%= UclstSiteSelected.ClientID %>', '<%= UclstSiteList.ClientID %>','<%= hiddenSelectedIDs.ClientID %>','<%= hiddenSelectedName.ClientID %>');ClearHiddenFiels('<%= hiddenSelectedIDs.ClientID %>','<%= hiddenSelectedName.ClientID %>');" />
                                </div>
                            </td>
                            <td width="1%">
                                <asp:HiddenField ID="hiddenSelectedIDs" runat="server" Value="" />
                                <asp:HiddenField ID="hiddenSelectedName" runat="server" Value="" />
                            </td>
                            <td width="25%">
                                <cc1:ucListBox ID="UclstSiteSelected" runat="server" Height="150px" Width="150px"
                                    ViewStateMode="Enabled">
                                </cc1:ucListBox>
                            </td>
                        </tr>
                      
                    </table>
                </cc1:ucView>
                <cc1:ucView ID="vwSearchListing" runat="server">
                    <cc1:ucPanel ID="pnlGrid" runat="server" ScrollBars="Both" CssClass="fieldset-form">
                        <cc1:ucGridView ID="gvDisLog" Width="100%" runat="server" CssClass="grid" OnRowDataBound="gvDisLog_RowDataBound"
                            OnSorting="SortGrid" AllowSorting="true" Style="overflow: auto;">
                            <Columns>
                                <asp:TemplateField HeaderText="Site" SortExpression="Site.SiteName">
                                    <HeaderStyle HorizontalAlign="Left" Width="300px" />
                                    <ItemStyle HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <cc1:ucLiteral ID="ltSiteName" runat="server" Text='<%#Eval("Site.SiteName") %>'></cc1:ucLiteral><asp:HiddenField
                                            ID="hdnDisType" runat="server" Value='<%#Eval("DiscrepancyTypeID") %>' />
                                        <asp:HiddenField ID="hdnDisLogID" runat="server" Value='<%#Eval("DiscrepancyLogID") %>' />
                                        <asp:HiddenField ID="hdnUserID" runat="server" Value='<%#Eval("UserID") %>' />
                                        <asp:HiddenField ID="hdnWorkFlowID" runat="server" Value='<%#Eval("DiscrepancyWorkflowID") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField HeaderText="Status" DataField="DiscrepancyStatusDiscreption" AccessibleHeaderText="false"
                                    SortExpression="DiscrepancyStatusDiscreption">
                                    <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                    <ItemStyle HorizontalAlign="Left" />
                                </asp:BoundField>
                                <asp:TemplateField HeaderText="VDR Number" SortExpression="VDRNo">
                                    <HeaderStyle Width="80px" HorizontalAlign="Left" />
                                    <ItemStyle HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <asp:HyperLink ID="hypLinkToDisDetails" runat="server" Text='<%#Eval("VDRNo") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField HeaderText="VDR Discription" DataField="ProductDescription" SortExpression="ProductDescription">
                                    <HeaderStyle HorizontalAlign="Left" Width="300px" />
                                    <ItemStyle HorizontalAlign="Left" />
                                </asp:BoundField>
                                <asp:BoundField HeaderText="CreateDate" DataField="DiscrepancyLogDate" SortExpression="DiscrepancyLogDate">
                                    <HeaderStyle HorizontalAlign="Left" Width="80px" />
                                    <ItemStyle HorizontalAlign="Left" />
                                </asp:BoundField>
                                <asp:BoundField HeaderText="Delivery Note" DataField="DeliveryNoteNumber" SortExpression="DeliveryNoteNumber">
                                    <HeaderStyle HorizontalAlign="Left" Width="100px" />
                                    <ItemStyle HorizontalAlign="Left" />
                                </asp:BoundField>
                                <asp:BoundField HeaderText="Vendor" DataField="VendorNoName" SortExpression="VendorNoName">
                                    <HeaderStyle HorizontalAlign="Left" Width="400px" />
                                    <ItemStyle HorizontalAlign="Left" Wrap="true" />
                                </asp:BoundField>
                                <asp:BoundField HeaderText="PO Number" DataField="PurchaseOrderNumber" SortExpression="PurchaseOrderNumber">
                                    <HeaderStyle HorizontalAlign="Left" Width="100px" />
                                    <ItemStyle HorizontalAlign="Left" />
                                </asp:BoundField>
                                <asp:TemplateField HeaderText="CreatedBy" SortExpression="User.FirstName">
                                    <HeaderStyle HorizontalAlign="Left" Width="400px" />
                                    <ItemStyle HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <cc1:ucLiteral ID="ltUserName" runat="server" Text='<%#Eval("User.FirstName") %>'></cc1:ucLiteral></ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField HeaderText="Stock Planner" DataField="StockPlannerNO" SortExpression="StockPlannerNO">
                                    <HeaderStyle HorizontalAlign="Left" Width="400px" />
                                    <ItemStyle HorizontalAlign="Left" />
                                </asp:BoundField>
                            </Columns>
                        </cc1:ucGridView>
                    </cc1:ucPanel>
                </cc1:ucView>
            </cc1:ucMultiView>
        </div>
    </div>
    <div class="button-row">
        <%--<cc1:ucButton ID="btnSave" runat="server" Text="Save"
    class="button" />--%>
        <cc1:ucButton ID="btnSearch" runat="server" Text="Search" CssClass="button" OnClick="btnSearch_Click" />
        <cc1:ucButton ID="btnReturntoSearchCriteria" runat="server" Text="Return to Search Criteria"
            class="button" OnClick="btnReturntoSearchCriteria_Click" />
    </div>
</asp:Content>

﻿<%@ Page Language="C#" AutoEventWireup="true" 
CodeFile="DIS_VendorSelectionCriteria.aspx.cs"  MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master" Inherits="DIS_CarrierSelectionCriteria" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2>
        <cc1:ucLabel ID="lblVendorSelectionCriteria" runat="server" Text="Vendor Selection Criteria"></cc1:ucLabel>
    </h2>
    <script language="javascript" type="text/javascript">
        function showHideDiv() {
            var divstyle = new String();
            divstyle = document.getElementById("divVendor").style.visibility;

            if (divstyle.toLowerCase() == "visible") {
                document.getElementById("divVendor").style.visibility = "hidden";
            }
            else {
                document.getElementById("divVendor").style.visibility = "visible";
            }
            return false;
        }
    </script>
    <div class="right-shadow">
        <div class="formbox">
                <table width="70%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                    <tr>
                        <td style="font-weight: bold" >
                            <cc1:ucLabel ID="lblSDRNo" runat="server" Text="SDR #" Width ="100%"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold; width: 5%">
                            :
                        </td>
                        <td style="font-weight: bold" colspan="4">
                            <cc1:ucTextbox ID="txtSDRNo" runat="server" Width="120px"></cc1:ucTextbox>
                              </td>                   
                    </tr>
                    <tr>
                        <td style="font-weight: bold" >
                            <cc1:ucLabel ID="lblSDRtype" runat="server" Text="SDR Type" Width ="100%"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold; width: 5%">
                            :
                        </td>
                        <td style="font-weight: bold" colspan="4">
                            <cc1:ucDropdownList ID="ddlSDRType" runat="server" Width="150px"></cc1:ucDropdownList>                              
                              </td>                   
                    </tr>
                        <tr>
                        <td style="font-weight: bold;" width="100px">
                            <cc1:ucLabel ID="lblSite" runat="server" Text="Site"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;" width="10px">
                            :
                        </td>
                        <td width="160px">
                            <cc1:ucListBox ID="lstSite1" runat="server" Height="200px" Width="160px">
                            </cc1:ucListBox>
                        </td>
                        <td valign="middle" align="center" width="60px">
                            <div>
                                <cc1:ucButton ID="btnMoveRightAll" runat="server" Text=">>" CssClass="button" Width="35px" /></div>
                            &nbsp;
                            <div>
                                <cc1:ucButton ID="btnMoveRight" runat="server" Text=">" CssClass="button" Width="35px" /></div>
                            &nbsp;
                            <div>
                                <cc1:ucButton ID="btnMoveLeft" runat="server" Text="<" CssClass="button" Width="35px" /></div>
                            &nbsp;
                            <div>
                                <cc1:ucButton ID="btnMoveLeftAll" runat="server" Text="<<" CssClass="button" Width="35px" /></div>
                        </td>
                        <td width="160px" colspan ="2">
                            <cc1:ucListBox ID="lstSite2" runat="server" Height="200px" Width="160px">
                            </cc1:ucListBox>
                        </td>
                    </tr>
                                 
                     <tr>
                        <td style="font-weight: bold;" width="100px">
                            <cc1:ucLabel ID="lblPurchaseOrder" runat="server" Text="Purchase Order"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;" width="10px">
                            :
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucTextbox ID="txtPurchaseOrder" runat="server" Width="120px"></cc1:ucTextbox>
                            
                        </td>
                        <td style="font-weight: bold;" width="60px" align="center" >
                            <cc1:ucLabel ID="lblPurchaseOrderTo" runat="server" Text="To"></cc1:ucLabel>
                        </td>
                       
                        <td style="font-weight: bold;" colspan="2">
                            <cc1:ucTextbox ID="txtPurchaseOrderTo" runat="server" Width="120px"></cc1:ucTextbox>
                            
                        </td>
                    </tr>
                     <tr>
                        <td style="font-weight: bold;" width="100px">
                            <cc1:ucLabel ID="lblSDRDateRange" runat="server" Text="SDR Date Range"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;" width="10px">
                            :
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucTextbox ID="txtSDRDateRange" runat="server" Width="80px"></cc1:ucTextbox>
                            
                        </td>
                        <td style="font-weight: bold;" width="60px" align="center" >
                            <cc1:ucLabel ID="lblSDRDateRangeTo" runat="server" Text="To"></cc1:ucLabel>
                        </td>
                       
                        <td style="font-weight: bold;" colspan="2">
                            <cc1:ucTextbox ID="txtSDRDateRangeTo" runat="server" Width="80px"></cc1:ucTextbox>
                            
                        </td>
                    </tr>
                   

                        <tr>
                        <td style="font-weight: bold;" width="100px">
                            <cc1:ucLabel ID="lblDeliveryNote" runat="server" Text="Delivery Note"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;" width="10px">
                            :
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucTextbox ID="txtDeliveryNote" runat="server" Width="120px"></cc1:ucTextbox>
                            
                        </td>
                        <td style="font-weight: bold;" width="60px" align="center"  >
                            <cc1:ucLabel ID="lblDeliveryNoteTo" runat="server" Text="To"></cc1:ucLabel>
                        </td>
                       
                        <td style="font-weight: bold;" colspan="2">
                            <cc1:ucTextbox ID="txtDeliveryNoteTo" runat="server" Width="120px"></cc1:ucTextbox>
                            
                        </td>
                    </tr>
                </table>
                                 
        </div>
    </div>
   
    
    <div class="button-row">
        <cc1:ucButton ID="btnSearch" runat="server" Text="Search" CssClass="button" 
            onclick="btnSearch_Click" />
       
    </div>
</asp:Content>

﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    CodeFile="DIS_WorklistEdit.aspx.cs" Inherits="DIS_WorklistEdit" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="../../CommonUI/UserControls/ucExportToExcel.ascx" TagName="ucExportToExcel" TagPrefix="cc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2>
        <cc1:ucLabel ID="lblWorklist" runat="server" Text="Worklist"></cc1:ucLabel>
    </h2>
     <div class="button-row">        
        <cc2:ucExportToExcel ID="ucExportToExcel1" runat="server" Visible="false" />
    </div>
    <div class="right-shadow">
        <div class="formbox">
            <table width="100%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settingsNoBorder">
                <tr>
                    <td align="center">
                        <table width="100%;">
                            <tr>
                                <td style="font-weight:bold; width:15%"></td>
                                <td style="font-weight:bold; width:15%; text-align:right;">
                                    <cc1:ucLabel ID="lblDepartmentStatus" Text="Department Status" runat="server"></cc1:ucLabel>
                                </td>
                                <td style="font-weight:bold; width:1%">
                                    &nbsp;:
                                </td>
                                <td style="width:19%">
                                    <cc1:ucDropdownList ID="ddlDepartmentStatus" runat="server" Width="150px" 
                                        onselectedindexchanged="ddlDepartmentStatus_SelectedIndexChanged" AutoPostBack="true">
                                        <asp:ListItem Text="All" Value="All"></asp:ListItem>
                                        <asp:ListItem Text="Open" Value="Open"></asp:ListItem>
                                        <asp:ListItem Text="Pending" Value="Pending"></asp:ListItem>
                                    </cc1:ucDropdownList>
                                </td>
                                <td style="font-weight:bold; width:15%; text-align:right;">
                                    <cc1:ucLabel ID="lblDepartment" Text="Department" runat="server"></cc1:ucLabel>
                                </td>
                                <td style="font-weight:bold; width:1%">
                                    &nbsp;:
                                </td>
                                <td style="width:19%">
                                    <cc1:ucLabel ID="lblDepartmentT" runat="server"></cc1:ucLabel>
                                </td>
                                <td style="font-weight:bold; width:15%"></td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <cc1:ucPanel ID="pnlGrid" runat="server" ScrollBars="Both" CssClass="fieldset-form">
                <table id="tblDisLogHeader" runat="server" width="100%" class="grid">
                    <tr>
                        <th align="left" style="padding-left:340px;"><asp:Label ID="lblOpenWith" Text="Open With" runat="server"></asp:Label></th>
                    </tr>
                </table>
                <cc1:ucGridView ID="gvDisLog" Width="100%" runat="server" CssClass="grid" OnRowDataBound="gvDisLog_RowDataBound"
                    OnSorting="SortGrid" AllowSorting="true" Style="overflow: auto; nowrap">
                    <Columns>
                        <asp:TemplateField HeaderText="Site" SortExpression="Site.SiteName">
                            <HeaderStyle HorizontalAlign="Left" Width="300px" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <cc1:ucLiteral ID="ltSiteName" runat="server" Text='<%#Eval("Site.SiteName") %>'></cc1:ucLiteral>
                                <asp:HiddenField ID="hdnDisType" runat="server" Value='<%#Eval("DiscrepancyTypeID") %>' />
                                <asp:HiddenField ID="hdnDisLogID" runat="server" Value='<%#Eval("DiscrepancyLogID") %>' />
                                <asp:HiddenField ID="hdnWorkFlowID" runat="server" Value='<%#Eval("DiscrepancyWorkflowID") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField HeaderText="Status" DataField="DiscrepancyStatusDiscreption" AccessibleHeaderText="false"
                            SortExpression="DiscrepancyStatusDiscreption">
                            <HeaderStyle Width="100px" HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                        <asp:TemplateField HeaderText="VDR Number" SortExpression="VDRNo">
                            <HeaderStyle Width="80px" HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <asp:HyperLink ID="hypLinkToDisDetails" runat="server" Text='<%#Eval("VDRNo") %>'></asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField HeaderText="VDR Discription" DataField="ProductDescription" SortExpression="ProductDescription">
                            <HeaderStyle HorizontalAlign="Left" Width="300px" />
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                        <%--BEGIN -- Need to replace column for GI,INV,AP,VEN--%>
                        <asp:TemplateField HeaderText="Query" SortExpression="QueryDiscrepancy.Query">
                            <HeaderStyle HorizontalAlign="Left" Width="200px" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <cc1:ucLiteral ID="ltQuery" runat="server" Text='<%#Eval("QueryDiscrepancy.Query") %>'></cc1:ucLiteral></ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField HeaderText="GI" DataField="GI" SortExpression="GI">
                            <HeaderStyle HorizontalAlign="Left" Width="300px" />
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                        <asp:BoundField HeaderText="INV" DataField="INV" SortExpression="INV">
                            <HeaderStyle HorizontalAlign="Left" Width="300px" />
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                        <asp:BoundField HeaderText="AP" DataField="AP" SortExpression="AP">
                            <HeaderStyle HorizontalAlign="Left" Width="300px" />
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                        <asp:BoundField HeaderText="VEN" DataField="VEN" SortExpression="VEN">
                            <HeaderStyle HorizontalAlign="Left" Width="300px" />
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                        <asp:BoundField HeaderText="MED" DataField="MED" SortExpression="VEN">
                            <HeaderStyle HorizontalAlign="Left" Width="300px" />
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                        <%--END -- Need to replace column for GI,INV,AP,VEN--%>
                        <asp:TemplateField HeaderText="CreateDate" SortExpression="DiscrepancyLogDate">
                            <HeaderStyle HorizontalAlign="Left" Width="200px" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <cc1:ucLiteral ID="ltDiscrepancyLogDate" runat="server" Text='<%#Eval("DiscrepancyLogDate", "{0:dd/MM/yyyy}") %>'></cc1:ucLiteral></ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField HeaderText="Delivery Note" DataField="DeliveryNoteNumber" SortExpression="DeliveryNoteNumber">
                            <HeaderStyle HorizontalAlign="Left" Width="100px" />
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                        <asp:BoundField HeaderText="Vendor" DataField="VendorNoName" SortExpression="VendorNoName">
                            <HeaderStyle HorizontalAlign="Left" Width="400px" />
                            <ItemStyle HorizontalAlign="Left" Wrap="true" />
                        </asp:BoundField>
                        <asp:BoundField HeaderText="PO Number" DataField="PurchaseOrderNumber" SortExpression="PurchaseOrderNumber">
                            <HeaderStyle HorizontalAlign="Left" Width="100px" />
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                        <asp:TemplateField HeaderText="CreatedBy" SortExpression="User.FirstName">
                            <HeaderStyle HorizontalAlign="Left" Width="400px" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <cc1:ucLiteral ID="ltUserName" runat="server" Text='<%#Eval("User.FirstName") %>'></cc1:ucLiteral>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField HeaderText="Stock Planner" DataField="StockPlannerNO" SortExpression="StockPlannerNO">
                            <HeaderStyle HorizontalAlign="Left" Width="400px" />
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                    </Columns>
                </cc1:ucGridView>
                <cc1:ucLabel ID="lblRecordNotFoundErrMess" ForeColor="Red" Font-Bold="true" runat="server"></cc1:ucLabel>
            </cc1:ucPanel>
        </div>
    </div>
    <div class="button-row">
        <%--<cc1:ucButton ID="btnSave" runat="server" Text="Save" class="button" />--%>
        <cc1:ucButton ID="btnBack" runat="server" Text="Back" class="button" Visible="false" />
    </div>
    
    

</asp:Content>

﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    CodeFile="DIS_SearchResult.aspx.cs" Inherits="DIS_SearchResult" %>

<%@ Register Src="~/CommonUI/UserControls/MultiSelectVendor.ascx" TagName="MultiSelectVendor"
    TagPrefix="cc2" %>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="../../CommonUI/UserControls/ucExportToExcel.ascx" TagName="ucExportToExcel"
    TagPrefix="cc2" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectStockPlanner.ascx" TagName="MultiSelectStockPlanner"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/ucVendorSelectionTemplate.ascx" TagName="VendorSelectTemplate"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/ucExportExpediteDiscrepancy.ascx" TagPrefix="cc1" TagName="ucExportExpediteDiscrepancy" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script language="javascript" type="text/javascript">
        $(document).ready(function () {

            var lstitem = document.getElementById('<%= UclstSiteSelected.ClientID %>');
            if (lstitem != null) {
                if (lstitem.options.length == 0) {
                    document.getElementById('<%= hiddenSelectedIDs.ClientID %>').value = "";
                    document.getElementById('<%= hiddenSelectedName.ClientID %>').value = "";
                }
            }
        });

        $(document).ready(function () {
            $("body").keypress(function (e) {
                if (e.which == 13 || e.keyCode == 13) {
                    $('#<%=btnSearch.ClientID %>').click();
                    return false;
                }
            });
        });
    </script>
    <h2>
        <cc1:ucLabel ID="lblSearchResultT" runat="server"></cc1:ucLabel>
    </h2>
    <asp:ScriptManager ID="sm1" runat="server">
    </asp:ScriptManager>
    <div class="button-row">
        <cc1:ucExportExpediteDiscrepancy runat="server" ID="ucExportExpediteDiscrepancy" />
    </div>
    <div class="right-shadow">
        <div class="formbox">

            <cc1:ucMultiView ID="mvDiscrepancyList" runat="server" ActiveViewIndex="0">

                <cc1:ucView ID="vwSearchCreteria" runat="server">
                    <table width="80%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                        <tr>
                            <td align="right">
                                <cc1:VendorSelectTemplate runat="server" ID="ucVendorTemplateSelect" />
                            </td>
                        </tr>
                    </table>
                    <table width="80%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                        <tr>
                            <td style="font-weight: bold; padding-left: 8px">
                                <cc1:ucLabel ID="lblDiscrepancyType" runat="server" Text="Discrepancy Type" Width="100%"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold;">:
                            </td>
                            <td style="font-weight: bold;">
                                <cc1:ucDropdownList runat="server" ID="ddlDiscrepancyType" Width="150px" />
                            </td>
                            <td style="font-weight: bold;">
                                <cc1:ucLabel ID="lblDiscrepancyNo" runat="server" Text="Discrepancy #" Width="100%"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold;">&nbsp;<cc1:ucLabel ID="UcLabel5" runat="server">:</cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold;">
                                <cc1:ucTextbox ID="txtDiscrepancyNo" runat="server" Width="145px"></cc1:ucTextbox>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold;">&nbsp;&nbsp;<cc1:ucLabel ID="lblPurchaseOrderNo" runat="server" Text="Purchase Order #"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold;">
                                <cc1:ucLabel ID="UcLabel7" runat="server">:</cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold;">
                                <cc1:ucTextbox ID="txtPurchaseOrder" runat="server" Width="145px"></cc1:ucTextbox>
                            </td>
                            <td style="font-weight: bold; text-align: left;">
                                <cc1:ucLabel ID="lblPurchaseOrderDate" runat="server" Text="Purchase Order Date"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold;">
                                <cc1:ucLabel ID="UcLabel1" runat="server">:</cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold;">
                                <asp:TextBox ID="txtPurchaseOrderDate" runat="server" ReadOnly="true" ClientIDMode="Static"
                                    class="date" Width="79px" />
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold;">&nbsp;&nbsp;<cc1:ucLabel ID="lblDiscrepancyDateRange" runat="server" Text="Discrepancy Date Range"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold;">
                                <cc1:ucLabel ID="UcLabel8" runat="server">:</cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold;">
                                <asp:TextBox ID="txtDiscrepancyDateRangeFrom" runat="server" ClientIDMode="Static"
                                    class="date" Width="79px" />
                            </td>
                            <td style="font-weight: bold;" align="Left">
                                <cc1:ucLabel ID="lblTo" runat="server" Text="To"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold;">
                                <cc1:ucLabel ID="UcLabel3" runat="server">:</cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold;">
                                <asp:TextBox ID="txtDiscrepancyDateRangeTo" runat="server" ClientIDMode="Static"
                                    class="date" Width="79px" />
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold;">&nbsp;&nbsp;<cc1:ucLabel ID="lblDeliveryNote" runat="server" Text="Delivery Note"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold;">
                                <cc1:ucLabel ID="UcLabel12" runat="server">:</cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold;" colspan="4">
                                <cc1:ucTextbox ID="txtDeliveryNote" runat="server" Width="145px"></cc1:ucTextbox>
                            </td>
                            <%--<td style="font-weight: bold;">
                                <cc1:ucLabel ID="lblStockPlanner" runat="server" Text="Stock Planner"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold;">
                                <cc1:ucLabel ID="Label3" runat="server">:</cc1:ucLabel>
                            </td>
                            <td>
                                <cc1:ucDropdownList runat="server" ID="ddlStockPlanner" Width="150px" />
                            </td>--%>
                        </tr>
                        <tr>
                            <td style="font-weight: bold; width: 23%">&nbsp;&nbsp;<cc1:ucLabel ID="lblSite" runat="server" Text="Site"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold; width: 1%">
                                <cc1:ucLabel ID="UcLabel2" runat="server">:</cc1:ucLabel>
                            </td>
                            <td width="30%">
                                <cc1:ucListBox ID="UclstSiteList" runat="server" Height="150px" Width="150px">
                                </cc1:ucListBox>
                            </td>
                            <td valign="middle" align="center" width="20%" colspan="1">
                                <div>
                                    <input type="button" id="btnMoveRight" value=">" class="button" style="width: 35px"
                                        onclick="Javascript: MoveItem('<%= UclstSiteList.ClientID %>', '<%= UclstSiteSelected.ClientID %>','<%= hiddenSelectedIDs.ClientID %>','<%= hiddenSelectedName.ClientID %>');" />
                                </div>
                                &nbsp;
                                <div>
                                    <input type="button" id="btnMoveLeft" value="<" class="button" style="width: 35px"
                                        onclick="Javascript: MoveItem('<%= UclstSiteSelected.ClientID %>', '<%= UclstSiteList.ClientID %>','<%= hiddenSelectedIDs.ClientID %>','<%= hiddenSelectedName.ClientID %>');" />
                                </div>
                            </td>
                            <td width="1%">
                                <asp:HiddenField ID="hiddenSelectedIDs" runat="server" Value="" />
                                <asp:HiddenField ID="hiddenSelectedName" runat="server" Value="" />
                            </td>
                            <td width="25%">
                                <cc1:ucListBox ID="UclstSiteSelected" runat="server" Height="150px" Width="150px"
                                    ViewStateMode="Enabled">
                                </cc1:ucListBox>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold;">&nbsp;&nbsp;<cc1:ucLabel ID="lblDiscrepancyStatus" runat="server" Text="Discrepancy Status"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold;">
                                <cc1:ucLabel ID="UcLabel6" runat="server">:</cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold;">
                                <asp:DropDownList ID="ddlDiscrepancyStatus" runat="server" Width="150px">
                                    <asp:ListItem Text="--Select--" Value="" />
                                    <asp:ListItem Text="Open" Value="O" />
                                    <asp:ListItem Text="Forced Closed" Value="F" />
                                    <asp:ListItem Text="Closed" Value="C" />
                                </asp:DropDownList>
                            </td>
                            <td style="font-weight: bold;">&nbsp;&nbsp;<cc1:ucLabel ID="lblActionPendingWith" runat="server" Text="Action Pending With"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold;">
                                <cc1:ucLabel ID="UcLabel9" runat="server">:</cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold;">
                                <asp:DropDownList ID="ddlActionPendingWithDropDown" runat="server" Width="150px">
                                    <asp:ListItem Text="All" Value="A" />
                                    <asp:ListItem Text="GoodsIn" Value="G" />
                                    <asp:ListItem Text="Stock Planner" Value="S" />
                                    <asp:ListItem Text="Account Payable" Value="P" />
                                    <asp:ListItem Text="Vendor" Value="V" />
                                    <asp:ListItem Text="Mediator" Value="M" />
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold;">&nbsp;&nbsp;<cc1:ucLabel ID="lblQueryStatus" runat="server" Text="Query Status"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold;">
                                <cc1:ucLabel ID="UcLabel10" runat="server">:</cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold;">
                                <asp:DropDownList ID="ddlQueryStatus" runat="server" Width="150px">
                                    <asp:ListItem Text="--Select--" Value="" />
                                    <asp:ListItem Text="Open" Value="O" />
                                    <asp:ListItem Text="Accepted" Value="A" />
                                    <asp:ListItem Text="Rejected" Value="R" />
                                    <asp:ListItem Text="Closed" Value="C" />
                                    <asp:ListItem Text="No Query" Value="NQ" />
                                </asp:DropDownList>
                            </td>
                            <td style="font-weight: bold;">&nbsp;&nbsp;<cc1:ucLabel ID="lblEscalation" runat="server" Text="Escalation"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold;">
                                <cc1:ucLabel ID="UcLabel4" runat="server">:</cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold;">
                                <asp:DropDownList ID="ddlEscalation" runat="server" Width="150px">
                                    <asp:ListItem Text="--Select--" Value="" />
                                    <asp:ListItem Text="First Escalation" Value="EscalationType2" />
                                    <%--<asp:ListItem Text="Second Escalation" Value="EscalationType3" />
                                    <asp:ListItem Text="Third Escalation" Value="EscalationType4" />
                                    <asp:ListItem Text="Fourth Escalation" Value="EscalationType5" />  --%>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold;">&nbsp;&nbsp;<cc1:ucLabel ID="lblSearchBySku" runat="server" Text="Search By SKU"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold;">
                                <cc1:ucLabel ID="UcLabel13" runat="server">:</cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold;">
                                <cc1:ucTextbox ID="txtSearchBySkuText" runat="server" Width="145px"></cc1:ucTextbox>
                            </td>
                            <td style="font-weight: bold;">&nbsp;&nbsp;<cc1:ucLabel ID="lblSearchByCatCode" runat="server" Text="Search By CAT Code"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold;">
                                <cc1:ucLabel ID="UcLabel15" runat="server">:</cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold;">
                                <cc1:ucTextbox ID="txtSearchByCatCodeText" runat="server" Width="145px"></cc1:ucTextbox>
                            </td>
                        </tr>

                        <tr>
                            <td colspan="3">&nbsp;
                            </td>
                            <td style="font-weight: bold;">&nbsp;&nbsp;<cc1:ucLabel ID="lblActionRequired" runat="server" Text="Action Required"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold;">
                                <cc1:ucLabel ID="UcLabel17" runat="server">:</cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold;">
                                <asp:DropDownList ID="ddlActionRequired" runat="server" Width="150px">
                                    <asp:ListItem Text="--Select--" Value="" />
                                    <asp:ListItem Text="Action Required" Value="Action Required" />
                                    <asp:ListItem Text="Remedial Action" Value="Remedial Action" />
                                </asp:DropDownList>
                            </td>
                        </tr>

                        <tr>
                            <td style="font-weight: bold;" colspan="6">
                                <cc1:MultiSelectStockPlanner runat="server" ID="msStockPlanner" />
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold;" colspan="6">
                                <cc2:MultiSelectVendor runat="server" ID="msVendor" />
                            </td>
                        </tr>
                    </table>
                </cc1:ucView>

                <cc1:ucView ID="vwSearchListing" runat="server">

                    <cc1:ucPanel ID="pnlGrid" runat="server" ScrollBars="Both" CssClass="fieldset-form">
                        <table width="110%" class="grid" id="tblGrid" runat="server">
                            <tr>
                                <th align="left" style="padding-left: 340px;">
                                    <asp:Label ID="lblOpenWith" Text="Open With" runat="server"></asp:Label>
                                </th>
                            </tr>
                        </table>

                        <table>
                            <tr>
                                <%--OnSorting="SortGrid"  AllowSorting="true"  --%>
                                <td>
                                    <cc1:ucGridView ID="gvDisLog" Width="110%" runat="server" CssClass="grid" OnRowDataBound="gvDisLog_RowDataBound"
                                       Style="overflow: auto;"
                                        AllowPaging="false" PageSize="200">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Site" SortExpression="Site.SiteName">
                                                <HeaderStyle HorizontalAlign="Left" Width="300px" />
                                                <ItemStyle HorizontalAlign="Left" />
                                                <ItemTemplate>
                                                    <cc1:ucLiteral ID="ltSiteName" runat="server" Text='<%#Eval("Site.SiteName") %>'></cc1:ucLiteral><asp:HiddenField
                                                        ID="hdnDisType" runat="server" Value='<%#Eval("DiscrepancyTypeID") %>' />
                                                    <asp:HiddenField ID="hdnDisLogID" runat="server" Value='<%#Eval("DiscrepancyLogID") %>' />
                                                    <asp:HiddenField ID="hdnUserID" runat="server" Value='<%#Eval("UserID") %>' />
                                                    <asp:HiddenField ID="hdnWorkFlowID" runat="server" Value='<%#Eval("DiscrepancyWorkflowID") %>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField HeaderText="Status" DataField="DiscrepancyStatusDiscreption" AccessibleHeaderText="false"
                                                SortExpression="DiscrepancyStatusDiscreption">
                                                <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:BoundField>
                                            <asp:TemplateField HeaderText="VDR Number" SortExpression="VDRNo">
                                                <HeaderStyle Width="80px" HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Left" />
                                                <ItemTemplate>
                                                    <asp:HyperLink ID="hypLinkToDisDetails" runat="server" Text='<%#Eval("VDRNo") %>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField HeaderText="VDR Discription" DataField="ProductDescription" SortExpression="ProductDescription">
                                                <HeaderStyle HorizontalAlign="Left" Width="300px" />
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:BoundField>
                                            <%--BEGIN -- Need to replace column for GI,INV,AP,VEN--%>
                                            <asp:TemplateField HeaderText="Query" SortExpression="QueryDiscrepancy.Query">
                                                <HeaderStyle HorizontalAlign="Left" Width="200px" />
                                                <ItemStyle HorizontalAlign="Left" />
                                                <ItemTemplate>
                                                    <cc1:ucLiteral ID="ltQuery" runat="server" Text='<%#Eval("QueryDiscrepancy.Query") %>'></cc1:ucLiteral>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField HeaderText="GI" DataField="GI" SortExpression="GI">
                                                <HeaderStyle HorizontalAlign="Left" Width="300px" />
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:BoundField>
                                            <asp:BoundField HeaderText="INV" DataField="INV" SortExpression="INV">
                                                <HeaderStyle HorizontalAlign="Left" Width="300px" />
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:BoundField>
                                            <asp:BoundField HeaderText="AP" DataField="AP" SortExpression="AP">
                                                <HeaderStyle HorizontalAlign="Left" Width="300px" />
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:BoundField>
                                            <asp:BoundField HeaderText="VEN" DataField="VEN" SortExpression="VEN">
                                                <HeaderStyle HorizontalAlign="Left" Width="300px" />
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:BoundField>
                                            <%--<asp:BoundField HeaderText="MED" DataField="MED" SortExpression="MED">
                                    <HeaderStyle HorizontalAlign="Left" Width="300px" />
                                    <ItemStyle HorizontalAlign="Left" />
                                </asp:BoundField>--%>
                                            <%--END -- Need to replace column for GI,INV,AP,VEN--%>
                                            <asp:TemplateField HeaderText="CreateDate" SortExpression="DiscrepancyLogDate">
                                                <HeaderStyle HorizontalAlign="Left" Width="200px" />
                                                <ItemStyle HorizontalAlign="Left" />
                                                <ItemTemplate>
                                                    <cc1:ucLiteral ID="ltDiscrepancyLogDate" runat="server" Text='<%#Eval("DiscrepancyLogDate", "{0:dd/MM/yyyy}") %>'></cc1:ucLiteral>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField HeaderText="Delivery Note" DataField="DeliveryNoteNumber" SortExpression="DeliveryNoteNumber">
                                                <HeaderStyle HorizontalAlign="Left" Width="100px" />
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:BoundField>
                                            <asp:BoundField HeaderText="Vendor" DataField="VendorNoName" SortExpression="VendorNoName">
                                                <HeaderStyle HorizontalAlign="Left" Width="400px" />
                                                <ItemStyle HorizontalAlign="Left" Wrap="true" />
                                            </asp:BoundField>
                                            <asp:BoundField HeaderText="PO Number" DataField="PurchaseOrderNumber" SortExpression="PurchaseOrderNumber">
                                                <HeaderStyle HorizontalAlign="Left" Width="100px" />
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:BoundField>
                                            <asp:TemplateField HeaderText="CreatedBy" SortExpression="User.FirstName">
                                                <HeaderStyle HorizontalAlign="Left" Width="400px" />
                                                <ItemStyle HorizontalAlign="Left" />
                                                <ItemTemplate>
                                                    <cc1:ucLiteral ID="ltUserName" runat="server" Text='<%#Eval("User.FirstName") %>'></cc1:ucLiteral>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField HeaderText="Stock Planner" DataField="StockPlannerNO" SortExpression="StockPlannerNO">
                                                <HeaderStyle HorizontalAlign="Left" Width="400px" />
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:BoundField>
                                            <asp:BoundField HeaderText="Requested Discrepancy Charge" DataField="DiscrepancyCharge"
                                                SortExpression="DiscrepancyCharge">
                                                <HeaderStyle HorizontalAlign="Left" Width="100px" />
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:BoundField>
                                            <asp:BoundField HeaderText="Action Required" DataField="ActionRequired"
                                                SortExpression="ActionRequired">
                                                <HeaderStyle HorizontalAlign="Left" Width="100px" />
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:BoundField>
                                        </Columns>
                                    </cc1:ucGridView>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <cc1:PagerV2_8 ID="Pager1" runat="server" OnCommand="pager_Command" GenerateGoToSection="false"></cc1:PagerV2_8>
                                </td>
                            </tr>
                        </table>                     

                    </cc1:ucPanel>
                </cc1:ucView>
            </cc1:ucMultiView>
        </div>
    </div>

    <div class="button-row">
        <cc1:ucButton ID="btnSearch" runat="server" Text="Search" CssClass="button" OnClick="btnSearch_Click" />
        <cc1:ucButton ID="btnReturntoSearchCriteria" runat="server" Text="Return to Search Criteria"
            class="button" OnClick="btnReturntoSearchCriteria_Click" />
    </div>
</asp:Content>

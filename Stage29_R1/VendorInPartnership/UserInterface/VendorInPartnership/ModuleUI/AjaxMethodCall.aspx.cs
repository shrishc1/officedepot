﻿using BusinessEntities.ModuleBE.AdminFunctions;
using BusinessEntities.ModuleBE.Appointment.Booking;
using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using BusinessLogicLayer.ModuleBAL.Appointment.Booking;
using BusinessLogicLayer.ModuleBAL.Appointment.SiteSettings;
using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;

public partial class AjaxMethodCall : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    public static int iSlotTimeLength = 15;

    [WebMethod]
    public static string GetGlobalConsolidateVendorIds(int vendorId)
    {
        string strVendorIds = string.Empty;
        UP_VendorBAL oUP_VendorBAL = new UP_VendorBAL();
        UP_VendorBE oUP_VendorBE = new UP_VendorBE();
        oUP_VendorBE.Action = "GetGlobalConsolidateVendorIds";
        oUP_VendorBE.VendorID = vendorId;
        strVendorIds = oUP_VendorBAL.GetGlobalConsolidateVendorIdsBAL(oUP_VendorBE);
        return strVendorIds;
    }

    [WebMethod]
    public static BookingInfo GetData(string name)
    {
        BookingInfo bookingInfo = new BookingInfo();
        APPBOK_BookingBAL objBooking = new APPBOK_BookingBAL();
        DataSet ds = objBooking.GetCurrentBookingInformationBAL(name);
        if (ds.Tables[0].Rows.Count > 0)
        {

            bookingInfo.Vendor = ds.Tables[0].Rows[0]["Vendor"].ToString();
            bookingInfo.CarrierName = ds.Tables[0].Rows[0]["CarrierName"].ToString();
            bookingInfo.VehicleType = ds.Tables[0].Rows[0]["VehicleType"].ToString();
            bookingInfo.VehicleTypeOther = ds.Tables[0].Rows[0]["VehicleTypeOther"].ToString();
            bookingInfo.Door = ds.Tables[0].Rows[0]["Door"].ToString();
            bookingInfo.Lifts = ds.Tables[0].Rows[0]["Lifts"].ToString();
            bookingInfo.Lines = ds.Tables[0].Rows[0]["Lines"].ToString();
            bookingInfo.Pallets = ds.Tables[0].Rows[0]["Pallets"] != null ? Convert.ToInt32(ds.Tables[0].Rows[0]["Pallets"]) : 0;
            bookingInfo.Catrons = ds.Tables[0].Rows[0]["Catrons"] != null ? Convert.ToInt32(ds.Tables[0].Rows[0]["Catrons"]) : 0;
            bookingInfo.StartTime = ds.Tables[0].Rows[0]["StartTime"].ToString();
            bookingInfo.SiteID = ds.Tables[0].Rows[0]["SiteID"] != null ? Convert.ToInt32(ds.Tables[0].Rows[0]["SiteID"]) : 0;

            bookingInfo.VendorID = ds.Tables[0].Rows[0]["VendorID"] != null ? Convert.ToInt32(ds.Tables[0].Rows[0]["VendorID"]) : 0;
            bookingInfo.CarrierID = ds.Tables[0].Rows[0]["CarrierID"] != null ? Convert.ToInt32(ds.Tables[0].Rows[0]["CarrierID"]) : 0;
            bookingInfo.VehicleTypeID = ds.Tables[0].Rows[0]["VehicleTypeID"] != null ? Convert.ToInt32(ds.Tables[0].Rows[0]["VehicleTypeID"]) : 0;
            bookingInfo.SupplierType = ds.Tables[0].Rows[0]["SupplierType"].ToString();
        }
        return bookingInfo;
    }

    [WebMethod]
    public static List<DashboardBooking> BindOneDashboardData(string date,string SiteID)
    {
        APPBOK_BookingBAL oAPPBOK_BookingBAL = new APPBOK_BookingBAL();
        DateTime selectedDate = Convert.ToDateTime(date);
        string action = "OneDayBookingData";
        var site = Convert.ToInt32(SiteID);
        return oAPPBOK_BookingBAL.GetDashboardOneBookingsBAL(action, site, selectedDate);
    }


    [WebMethod]
    public static List<DashboardBooking> SearchDashboardList(string date, string SiteID)
    {
        APPBOK_BookingBAL oAPPBOK_BookingBAL = new APPBOK_BookingBAL();
        DateTime selectedDateNew = !string.IsNullOrEmpty(date) ? Utilities.Common.TextToDateFormat(date) : DateTime.Now;         
        string action = "AllDashboard";
        var site = Convert.ToInt32(SiteID);
        List<DashboardBooking> dtBookings = oAPPBOK_BookingBAL.GetDashboardBookingsBAL(action, site, selectedDateNew);
        return  dtBookings.Count > 0 ? dtBookings : null;       
    }

    private static int GetTimeSlotLength(BookingInfo bookingInfo)
    {

        //--------start Calculate Unload Time-------------------//
        decimal TotalCalculatedTime = 0;  //In Min
        bool isTimeCalculatedForPallets = false;
        bool isTimeCalculatedForCartons = false;
        int VehicleTypeID = -1;

        //Step1 >Check Vendor Processing Window 
        if (isTimeCalculatedForPallets == false || isTimeCalculatedForCartons == false)
        {

            MASSIT_VendorProcessingWindowBE oMASSIT_VendorProcessingWindowBE = new MASSIT_VendorProcessingWindowBE();
            APPSIT_VendorProcessingWindowBAL oMASSIT_VendorProcessingWindowBAL = new APPSIT_VendorProcessingWindowBAL();
            oMASSIT_VendorProcessingWindowBE.Action = "ShowAllConsolidate";
            oMASSIT_VendorProcessingWindowBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
            oMASSIT_VendorProcessingWindowBE.User.UserID = Convert.ToInt32(HttpContext.Current.Session["UserID"]);
            // oMASSIT_VendorProcessingWindowBE.VendorID = Convert.ToInt32(ucSeacrhVendorID); // Convert.ToInt32(ucSeacrhVendor1.VendorNo);
            oMASSIT_VendorProcessingWindowBE.SiteID = Convert.ToInt32(bookingInfo.SiteID);
            MASSIT_VendorProcessingWindowBE lstVendor = new MASSIT_VendorProcessingWindowBE();
            lstVendor = oMASSIT_VendorProcessingWindowBAL.GetVendorDetailsByIdBAL(oMASSIT_VendorProcessingWindowBE);
            if (lstVendor != null)
            {  //Vendor Processing Window Not found
                if (lstVendor.APP_PalletsUnloadedPerHour > 0 && bookingInfo.Pallets != 0 && isTimeCalculatedForPallets == false)
                {
                    TotalCalculatedTime += Convert.ToDecimal(Convert.ToInt32(bookingInfo.Pallets) * (60.0 / lstVendor.APP_PalletsUnloadedPerHour));
                    isTimeCalculatedForPallets = true;
                }
                if (lstVendor.APP_CartonsUnloadedPerHour > 0 && bookingInfo.Catrons != 0 && isTimeCalculatedForCartons == false)
                {
                    TotalCalculatedTime += Convert.ToDecimal(Convert.ToInt32(bookingInfo.Catrons) * (60.0 / lstVendor.APP_CartonsUnloadedPerHour));
                    isTimeCalculatedForCartons = true;
                }
            }
        }
        //---------------------------------// 

        //Step2 > Check Vehicle Type Setup
        if (isTimeCalculatedForPallets == false || isTimeCalculatedForCartons == false)
        {
            MASSIT_VehicleTypeBE oMASSIT_VehicleTypeBE = new MASSIT_VehicleTypeBE();
            APPSIT_VehicleTypeBAL oMASSIT_VehicleTypeBAL = new APPSIT_VehicleTypeBAL();

            oMASSIT_VehicleTypeBE.Action = "ShowById";
            oMASSIT_VehicleTypeBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
            oMASSIT_VehicleTypeBE.User.UserID = Convert.ToInt32(HttpContext.Current.Session["UserID"]);

            oMASSIT_VehicleTypeBE.VehicleTypeID = VehicleTypeID;

            MASSIT_VehicleTypeBE lstVehicleType = oMASSIT_VehicleTypeBAL.GetVehicleTypeDetailsByIdBAL(oMASSIT_VehicleTypeBE);
            if (lstVehicleType != null)
            {
                //Volume Capacity 
                if (lstVehicleType.APP_PalletsUnloadedPerHour > 0 && bookingInfo.Pallets != 0 && isTimeCalculatedForPallets == false)
                {
                    TotalCalculatedTime += Convert.ToDecimal(Convert.ToInt32(bookingInfo.Pallets) * (60.0 / lstVehicleType.APP_PalletsUnloadedPerHour));
                    isTimeCalculatedForPallets = true;
                }
                if (lstVehicleType.APP_CartonsUnloadedPerHour > 0 && bookingInfo.Catrons != 0 && isTimeCalculatedForCartons == false)
                {
                    TotalCalculatedTime += Convert.ToDecimal(Convert.ToInt32(bookingInfo.Catrons) * (60.0 / lstVehicleType.APP_CartonsUnloadedPerHour));
                    isTimeCalculatedForCartons = true;
                }

                if (isTimeCalculatedForPallets == false && isTimeCalculatedForCartons == false)
                { //Time Based 
                    if (lstVehicleType.TotalUnloadTimeInMinute > 0)
                    {
                        TotalCalculatedTime += Convert.ToDecimal(Convert.ToDecimal(lstVehicleType.TotalUnloadTimeInMinute));
                        isTimeCalculatedForPallets = true;
                        isTimeCalculatedForCartons = true;
                    }
                }
                //---------------Stage 14 Point 15-------------//
                if (lstVehicleType.TotalUnloadTimeInMinute <= 0 && TotalCalculatedTime < lstVehicleType.MinimumTimePerDeliveryInMinute)
                {
                    TotalCalculatedTime = lstVehicleType.MinimumTimePerDeliveryInMinute;
                }
                //-------------------------------------------//
            }
        }

        //Calculate Slot Length
        int intResult = (int)TotalCalculatedTime % iSlotTimeLength;
        int intr = (int)TotalCalculatedTime / iSlotTimeLength;
        if (intResult > 0)
            intr++;

        if (TotalCalculatedTime > 0 && TotalCalculatedTime < 1)
            intr = 1;

        if (TotalCalculatedTime == 0)
            intr++;
        return intr;
    }

    private static string GetFormatedTime(string TimeToFormat)
    {
        if (TimeToFormat != string.Empty)
        {
            string[] arrDate = TimeToFormat.Split(':');
            string HH, MM;
            if (arrDate[0].Length < 2)
                HH = "0" + arrDate[0].ToString();
            else
                HH = arrDate[0];

            if (arrDate[1].Length < 2)
                MM = "0" + arrDate[1].ToString();
            else
                MM = arrDate[1];


            return HH + ":" + MM;
        }
        else
        {
            return TimeToFormat;
        }
    }


    private static int GetRequiredTimeSlotLengthForBookedSlotOfOthers(BookingInfo bookingInfo)
    {
        //--------start Calculate Unload Time-------------------//
        decimal TotalCalculatedTime = 0;  //In Min
        bool isTimeCalculatedForPallets = false;
        bool isTimeCalculatedForCartons = false;

        int? TotalPallets;
        int? TotalCartons;

        TotalPallets = bookingInfo.Pallets;
        TotalCartons = bookingInfo.Catrons;

        string SupplierType = bookingInfo.SupplierType;

        if (SupplierType == "V")
        {
            //Step1 >Check Vendor Processing Window 
            if (isTimeCalculatedForPallets == false || isTimeCalculatedForCartons == false)
            {
                MASSIT_VendorProcessingWindowBE oMASSIT_VendorProcessingWindowBE = new MASSIT_VendorProcessingWindowBE();
                APPSIT_VendorProcessingWindowBAL oMASSIT_VendorProcessingWindowBAL = new APPSIT_VendorProcessingWindowBAL();
                oMASSIT_VendorProcessingWindowBE.Action = "ShowAllConsolidate";
                oMASSIT_VendorProcessingWindowBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
                oMASSIT_VendorProcessingWindowBE.User.UserID = Convert.ToInt32(HttpContext.Current.Session["UserID"]);
                oMASSIT_VendorProcessingWindowBE.VendorID = bookingInfo.VendorID;
                oMASSIT_VendorProcessingWindowBE.SiteID = Convert.ToInt32(bookingInfo.SiteID);
                MASSIT_VendorProcessingWindowBE lstVendor = new MASSIT_VendorProcessingWindowBE();
                lstVendor = oMASSIT_VendorProcessingWindowBAL.GetVendorDetailsByIdBAL(oMASSIT_VendorProcessingWindowBE);
                if (lstVendor != null)
                {  //Vendor Processing Window Not found

                    if (lstVendor.APP_PalletsUnloadedPerHour > 0 && TotalPallets != 0 && isTimeCalculatedForPallets == false)
                    {
                        TotalCalculatedTime += Convert.ToDecimal(Convert.ToInt32(TotalPallets) * (60.0 / lstVendor.APP_PalletsUnloadedPerHour));
                        isTimeCalculatedForPallets = true;
                    }
                    if (lstVendor.APP_CartonsUnloadedPerHour > 0 && TotalCartons != 0 && isTimeCalculatedForCartons == false)
                    {
                        TotalCalculatedTime += Convert.ToDecimal(Convert.ToInt32(TotalCartons) * (60.0 / lstVendor.APP_CartonsUnloadedPerHour));
                        isTimeCalculatedForCartons = true;
                    }
                }
            }
            //---------------------------------// 
        }

        if (SupplierType == "C")
        {
            //Step1 >Check Carrier Processing Window 
            if (isTimeCalculatedForPallets == false || isTimeCalculatedForCartons == false)
            {
                MASSIT_CarrierProcessingWindowBE oMASSIT_CarrerProcessingWindowBE = new MASSIT_CarrierProcessingWindowBE();
                APPSIT_CarrierProcessingWindowBAL oMASSIT_CarrerProcessingWindowBAL = new APPSIT_CarrierProcessingWindowBAL();
                oMASSIT_CarrerProcessingWindowBE.Action = "GetConstraints";
                oMASSIT_CarrerProcessingWindowBE.CarrierID = bookingInfo.CarrierID;
                oMASSIT_CarrerProcessingWindowBE.SiteID = Convert.ToInt32(bookingInfo.SiteID);
                MASSIT_CarrierProcessingWindowBE lstCarrier = oMASSIT_CarrerProcessingWindowBAL.GetConstraintsBAL(oMASSIT_CarrerProcessingWindowBE);
                if (lstCarrier != null)
                {  //Carrier Processing Window Not found

                    if (lstCarrier.APP_PalletsUnloadedPerHour > 0 && TotalPallets != 0 && isTimeCalculatedForPallets == false)
                    {
                        TotalCalculatedTime += Convert.ToDecimal(Convert.ToInt32(TotalPallets) * (60.0 / lstCarrier.APP_PalletsUnloadedPerHour));
                        isTimeCalculatedForPallets = true;
                    }
                    if (lstCarrier.APP_CartonsUnloadedPerHour > 0 && TotalCartons != 0 && isTimeCalculatedForCartons == false)
                    {
                        TotalCalculatedTime += Convert.ToDecimal(Convert.ToInt32(TotalCartons) * (60.0 / lstCarrier.APP_CartonsUnloadedPerHour));
                        isTimeCalculatedForCartons = true;
                    }
                }
            }
            //---------------------------------// 
        }


        //Step2 > Check Vehicle Type Setup
        if (isTimeCalculatedForPallets == false || isTimeCalculatedForCartons == false)
        {
            MASSIT_VehicleTypeBE oMASSIT_VehicleTypeBE = new MASSIT_VehicleTypeBE();
            APPSIT_VehicleTypeBAL oMASSIT_VehicleTypeBAL = new APPSIT_VehicleTypeBAL();

            oMASSIT_VehicleTypeBE.Action = "ShowById";
            oMASSIT_VehicleTypeBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
            oMASSIT_VehicleTypeBE.User.UserID = Convert.ToInt32(HttpContext.Current.Session["UserID"]);

            oMASSIT_VehicleTypeBE.VehicleTypeID = bookingInfo.VehicleTypeID;

            MASSIT_VehicleTypeBE lstVehicleType = oMASSIT_VehicleTypeBAL.GetVehicleTypeDetailsByIdBAL(oMASSIT_VehicleTypeBE);
            if (lstVehicleType != null)
            {
                //Volume Capacity 
                if (lstVehicleType.APP_PalletsUnloadedPerHour > 0 && TotalPallets != 0 && isTimeCalculatedForPallets == false)
                {
                    TotalCalculatedTime += Convert.ToDecimal(Convert.ToInt32(TotalPallets) * (60.0 / lstVehicleType.APP_PalletsUnloadedPerHour));
                    isTimeCalculatedForPallets = true;
                }
                if (lstVehicleType.APP_CartonsUnloadedPerHour > 0 && TotalCartons != 0 && isTimeCalculatedForCartons == false)
                {
                    TotalCalculatedTime += Convert.ToDecimal(Convert.ToInt32(TotalCartons) * (60.0 / lstVehicleType.APP_CartonsUnloadedPerHour));
                    isTimeCalculatedForCartons = true;
                }

                if (isTimeCalculatedForPallets == false && isTimeCalculatedForCartons == false)
                { //Time Based 
                    if (lstVehicleType.TotalUnloadTimeInMinute > 0)
                    {
                        TotalCalculatedTime += Convert.ToDecimal(Convert.ToDecimal(lstVehicleType.TotalUnloadTimeInMinute));
                        isTimeCalculatedForPallets = true;
                        isTimeCalculatedForCartons = true;
                    }
                }

                //---------------Stage 14 Point 15-------------//
                if (lstVehicleType.TotalUnloadTimeInMinute <= 0 && TotalCalculatedTime < lstVehicleType.MinimumTimePerDeliveryInMinute)
                {
                    TotalCalculatedTime = lstVehicleType.MinimumTimePerDeliveryInMinute;
                }
                //-------------------------------------------//

            }
        }
        //---------------------------------// 
        //Step3 >Check Site Miscellaneous Settings
        if (isTimeCalculatedForPallets == false || isTimeCalculatedForCartons == false)
        {

            MAS_SiteBE oMAS_SiteBE = new MAS_SiteBE();
            BusinessLogicLayer.ModuleBAL.AdminFunctions.MAS_SiteBAL oMAS_SiteBAL = new BusinessLogicLayer.ModuleBAL.AdminFunctions.MAS_SiteBAL();

            if (HttpContext.Current.Session["Role"].ToString() == "Vendor")
                oMAS_SiteBE.Action = "ShowAllForVendor";
            else
                oMAS_SiteBE.Action = "ShowAll";

            oMAS_SiteBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
            oMAS_SiteBE.User.UserID = Convert.ToInt32(HttpContext.Current.Session["UserID"]);
            oMAS_SiteBE.SiteID = Convert.ToInt32(bookingInfo.SiteID);

            List<MAS_SiteBE> lstSiteMisSettings = new List<MAS_SiteBE>();
            lstSiteMisSettings = oMAS_SiteBAL.GetSiteMisSettingBAL(oMAS_SiteBE);

            if (lstSiteMisSettings != null && lstSiteMisSettings.Count > 0)
            {
                if (lstSiteMisSettings[0].AveragePalletUnloadedPerManHour > 0 && TotalPallets != 0 && isTimeCalculatedForPallets == false)
                {
                    TotalCalculatedTime += Convert.ToDecimal(Convert.ToInt32(TotalPallets) * (60.0 / lstSiteMisSettings[0].AveragePalletUnloadedPerManHour));      // Convert.ToDecimal((Convert.ToInt32(TotalPallets) / lstSiteMisSettings[0].AveragePalletUnloadedPerManHour) * 60);
                    isTimeCalculatedForPallets = true;
                }
                if (lstSiteMisSettings[0].AverageCartonUnloadedPerManHour > 0 && TotalCartons != 0 && isTimeCalculatedForCartons == false)
                {
                    TotalCalculatedTime += Convert.ToDecimal(Convert.ToInt32(TotalCartons) * (60.0 / lstSiteMisSettings[0].AverageCartonUnloadedPerManHour));      //Convert.ToDecimal((Convert.ToInt32(TotalCartons) / lstSiteMisSettings[0].AverageCartonUnloadedPerManHour) * 60);
                    isTimeCalculatedForCartons = true;
                }
            }
        }
        //---------------------------------//  
        //Calculate Slot Length
        int intResult = (int)TotalCalculatedTime % iSlotTimeLength;
        int intr = (int)TotalCalculatedTime / iSlotTimeLength;
        if (intResult > 0)
            intr++;
        if (TotalCalculatedTime > 0 && TotalCalculatedTime < 1)
            intr = 1;
        if (TotalCalculatedTime == 0)
            intr++;

        return intr;
    }


    /// <summary>
    /// class for booking information details on button click events
    /// </summary>
    public class BookingInfo
    {
        public string Vendor { get; set; }
        public string CarrierName { get; set; }
        public string VehicleType { get; set; }
        public string VehicleTypeOther { get; set; }
        public string Door { get; set; }
        public string Lifts { get; set; }
        public string Lines { get; set; }
        public int Pallets { get; set; }
        public int Catrons { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public int SiteID { get; set; }
        public string SupplierType { get; set; }
        public int BookingID { get; set; }
        public int VehicleTypeID { get; set; }

        public int VendorID { get; set; }

        public int CarrierID { get; set; }
    }
}
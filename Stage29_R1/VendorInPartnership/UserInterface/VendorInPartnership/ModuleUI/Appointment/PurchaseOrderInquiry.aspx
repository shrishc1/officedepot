﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    AutoEventWireup="true" CodeFile="PurchaseOrderInquiry.aspx.cs" Inherits="PurchaseOrderInquiry" %>

<%@ Register TagName="ucDate" TagPrefix="cc2" Src="~/CommonUI/UserControls/ucDate.ascx" %>
<%@ Register TagName="ucDate2" TagPrefix="cc3" Src="~/CommonUI/UserControls/ucDate2.ascx" %>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<%@ Register Src="../../CommonUI/UserControls/ucSeacrhVendor.ascx" TagName="ucSeacrhVendor"
    TagPrefix="uc1" %>
<%@ Register Src="../../CommonUI/UserControls/ucSite.ascx" TagName="ucSite" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script type="text/javascript">
        $(document).ready(function () {
            $("body").keypress(function (e) {
                if (e.which == 13 || e.keyCode == 13) {
                    $('#<%=btnSearch.ClientID %>').click();
                    return false;
                }
            });
        });
    </script>
    <h2>
        <cc1:ucLabel ID="lblPurchaseOrderInquiry" runat="server"></cc1:ucLabel>
        <cc1:ucTextbox ID="hdnMailHTML" runat="server" Visible="false" />
    </h2>
    <div class="right-shadow">
        <div class="formbox">
            <asp:Panel ID="pnlPurchaseOrderInqSearch" runat="server">

                <asp:UpdatePanel ID="UpdatePanel1" runat="server" ChildrenAsTriggers="true">
                    <ContentTemplate>
                        <table width="100%" cellspacing="5" cellpadding="0" border="0" class="top-settingsNoBorder">
                            <tr>
                                <td style="font-weight: bold; width: 15%;">
                                    <cc1:ucLabel ID="lblPurchaseOrderNo" runat="server"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold; width: 1%;">:
                                </td>
                                <td style="font-weight: bold; width: 34%;">
                                    <cc1:ucTextbox ID="txtPurchaseOrderNo" Width="150px" runat="server"></cc1:ucTextbox>
                                </td>
                                <td style="font-weight: bold;" width="15%">
                                    <cc1:ucLabel ID="lblVendorNumber" runat="server"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold; width: 1%">:
                                </td>
                                <td style="font-weight: bold; width: 34%">
                                    <span id="spVender" runat="server">
                                        <uc1:ucSeacrhVendor ID="ucSeacrhVendor1" runat="server" />
                                    </span>
                                    <cc1:ucLiteral ID="ltSearchVendorName" runat="server" Visible="false"></cc1:ucLiteral>
                                </td>
                                <%--<td style="font-weight: bold;" width="10%">
                                        <cc1:ucLabel ID="lblVendorNumberSDR" runat="server"></cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold; width: 3%">
                                        :
                                    </td>
                                    <td style="font-weight: bold; width: 41%">
                                        <cc1:ucDropdownList ID="ddlVendorNumberSDR" runat="server">
                                        </cc1:ucDropdownList>
                                    </td>--%>
                            </tr>
                            <tr>
                                <td style="font-weight: bold; width: 15%;">
                                    <cc1:ucLabel ID="lblSkuWithHash" runat="server"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold; width: 1%;">:
                                </td>
                                <td style="font-weight: bold; width: 34%;">
                                    <cc1:ucTextbox ID="txtSku" Width="150px" runat="server"></cc1:ucTextbox>
                                </td>
                                <td style="font-weight: bold; width: 15%;">
                                    <cc1:ucLabel ID="lblSitePrefixName" runat="server"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold; width: 1%;">:
                                </td>
                                <td style="font-weight: bold; width: 34%;">
                                    <span id="spSite" runat="server">
                                        <uc2:ucSite ID="ddlSite" runat="server" />
                                    </span>
                                    <cc1:ucLiteral ID="ltSelectedSite" runat="server" Visible="false"></cc1:ucLiteral>
                                    <asp:HiddenField ID="hdnBaseSiteId" runat="server" />
                                </td>
                            </tr>

                            <tr>
                                <td style="font-weight: bold; width: 15%;">
                                    <cc1:ucLabel ID="lblCATCode" runat="server"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold; width: 1%;">:
                                </td>
                                <td style="font-weight: bold; width: 34%;">
                                    <cc1:ucTextbox ID="txtCATCode" Width="150px" runat="server"></cc1:ucTextbox>
                                </td>
                                <td style="font-weight: bold; width: 15%;">
                                    <cc1:ucLabel ID="lblReceiptStatus" runat="server"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold; width: 1%;">:
                                </td>
                                <td style="font-weight: bold; width: 34%;">
                                    <asp:DropDownList ID="ddlReceipt" runat="server" Width="40%">
                                        <asp:ListItem Text="All" Value="0" />
                                        <asp:ListItem Text="No Receipts" Value="1" />
                                        <asp:ListItem Text="Patially Received" Value="2" />
                                        <asp:ListItem Text="Received" Value="3" />
                                    </asp:DropDownList>
                                </td>
                            </tr>

                            <tr>
                                <td style="font-weight: bold; width: 15%;">
                                    <cc1:ucLabel ID="lblDateCreatedFrom" runat="server"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold; width: 1%;">:
                                </td>
                                <td style="font-weight: bold; width: 34%;">
                                    <cc3:ucDate2 ID="txtDateFrom" runat="server" ClientIDMode="Static" />
                                </td>
                                <td style="font-weight: bold; width: 15%;">
                                    <cc1:ucLabel ID="lblDateCreatedTo" runat="server"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold; width: 1%;">:
                                </td>
                                <td style="font-weight: bold; width: 34%;">
                                    <cc2:ucDate ID="txtDateTo" runat="server" ClientIDMode="Static" />
                                </td>
                            </tr>

                            <tr>
                                <td style="font-weight: bold;" width="15%">
                                    <cc1:ucLabel ID="lblStatus" runat="server"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold; width: 1%">:
                                </td>
                                <td style="font-weight: bold; width: 34%">
                                    <cc1:ucDropdownList ID="ddlStatus" Width="157px" runat="server">
                                        <asp:ListItem Text="All" Value="1"></asp:ListItem>
                                        <asp:ListItem Text="Open" Value="2"></asp:ListItem>
                                        <asp:ListItem Text="Closed" Value="3"></asp:ListItem>
                                    </cc1:ucDropdownList>
                                </td>
                                <td style="font-weight: bold;" width="15%"></td>
                                <td style="font-weight: bold; width: 1%"></td>
                                <td style="font-weight: bold; width: 34%;">
                                    <div class="button-row" style="text-align: left">
                                        <cc1:ucButton ID="btnSearch" runat="server" CssClass="button" OnClick="btnSearch_Click" />
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                    <Triggers>
                        <asp:PostBackTrigger ControlID="btnSearch" />
                        <asp:AsyncPostBackTrigger ControlID="ddlSite" />
                    </Triggers>
                </asp:UpdatePanel>

                <table width="100%" cellspacing="5" cellpadding="0" border="0" class="top-settingsNoBorder">
                    <tr>
                        <td style="font-weight: bold; width: 100%;" colspan="6" align="center">
                            <cc1:ucGridView ID="gvPurchaseOrderInquiry" runat="server" AutoGenerateColumns="false"
                                CssClass="grid" Width="100%"
                                OnRowCommand="gvPurchaseOrderInquiry_RowCommand">
                                <AlternatingRowStyle BackColor="White" ForeColor="#284775"></AlternatingRowStyle>
                                <Columns>
                                    <asp:TemplateField HeaderText="PO" SortExpression="Purchase_order">
                                        <HeaderStyle Width="10%" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkPO" Text='<%# Eval("Purchase_order") %>' CommandName="SendOnPOHistory"
                                                runat="server" CommandArgument='<%# Eval("Purchase_order") %>'></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Vendor" SortExpression="Vendor_No">
                                        <ItemStyle HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <asp:HiddenField ID="hdnVendorId" runat="server" Value='<%# Eval("Vendor.VendorID") %>' />
                                            <cc1:ucLabel ID="lblVendorPOInq" runat="server" Text='<%# Eval("Vendor_No") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Status" SortExpression="Status">
                                        <ItemStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <cc1:ucLabel ID="lblStatusPOInq" runat="server" Text='<%# Eval("Status") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="OpenLinesWithPreHash" SortExpression="Line_No">
                                        <ItemStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <cc1:ucLabel ID="lblLineNoPOInq" runat="server" Text='<%# Eval("Line_No") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="OrderRaisedDate" SortExpression="Order_raised">
                                        <ItemStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <cc1:ucLabel ID="lblOrderRaisedDatePOInq" runat="server" Text='<%# Eval("Order_raised","{0:dd-MM-yyyy}") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="ExpectDuedate" SortExpression="Expected_date">
                                        <ItemStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <cc1:ucLabel ID="lblExpecteddatePOInq" runat="server" Text='<%# Eval("Expected_date","{0:dd-MM-yyyy}") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Site" SortExpression="Site.SiteName">
                                        <ItemStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <asp:HiddenField ID="hdnSiteId" runat="server" Value='<%# Eval("Site.SiteID") %>' />
                                            <cc1:ucLabel ID="lblSiteNamePOInq" runat="server" Text='<%# Eval("Site.SiteName") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                            </cc1:ucGridView>
                            <cc1:ucLabel ID="lblNoRecordsFound" Font-Bold="true" ForeColor="Red" Visible="false"
                                runat="server"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:PagerV2_8 ID="Pager1" runat="server" OnCommand="pager_Command" GenerateGoToSection="false"
                                GenerateFirstLastSection="False" enerateGoToSection="False" MaxSmartShortCutCount="5"
                                GenerateHiddenHyperlinks="False" GeneratePagerInfoSection="False" GenerateSmartShortCuts="True" GenerateToolTips="True"
                                SmartShortCutThreshold="10" PageSize="200"></cc1:PagerV2_8>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <asp:Panel ID="pnlPurchaseOrderInqDetail" runat="server">
                <div id="divSearchButtons">
                    <table width="100%" cellspacing="1" cellpadding="0" border="0" align="center" class="top-settings">
                        <tr>
                            <td style="font-weight: bold; width: 15%">
                                <cc1:ucLabel ID="lblPurchaseOrder" runat="server"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold; width: 1%">:
                            </td>
                            <td style="font-weight: bold; width: 34%">
                                <cc1:ucLabel ID="lblPurchaseOrderV" runat="server"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold; width: 15%">
                                <cc1:ucLabel ID="lblVendor" runat="server"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold; width: 1%">:
                            </td>
                            <td style="width: 34%">
                                <cc1:ucLabel ID="lblVendorV" runat="server"></cc1:ucLabel>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold; width: 15%">
                                <cc1:ucLabel ID="lblSiteDiscShortage" runat="server"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold; width: 1%">:
                            </td>
                            <td style="font-weight: bold; width: 34%">
                                <cc1:ucLabel ID="lblSiteV" runat="server"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold; width: 15%">
                                <cc1:ucLabel ID="lblStatus_1" runat="server"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold; width: 1%">:
                            </td>
                            <td style="width: 34%">
                                <cc1:ucLabel ID="lblStatusV" runat="server"></cc1:ucLabel>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold; width: 15%">
                                <cc1:ucLabel ID="lblOriginalDueDate" runat="server"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold; width: 1%">:
                            </td>
                            <td style="font-weight: bold; width: 34%">
                                <cc1:ucLabel ID="lblOriginalDueDateV" runat="server"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold; width: 15%">
                                <cc1:ucLabel ID="lblBuyer" runat="server"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold; width: 1%">:
                            </td>
                            <td style="width: 34%">
                                <cc1:ucLabel ID="lblBuyerV" runat="server"></cc1:ucLabel>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold; width: 15%">
                                <cc1:ucLabel ID="lblOrderCreated" runat="server"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold; width: 1%">:
                            </td>
                            <td style="font-weight: bold; width: 34%">
                                <cc1:ucLabel ID="lblOrderCreatedV" runat="server"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold; width: 50%" colspan="3"></td>
                        </tr>
                    </table>
                </div>
                <div>
                    <br />
                </div>
                <cc1:ucPanel ID="pnlPODetails" runat="server" CssClass="fieldset-form" Style="display: block">
                    <table width="100%" cellspacing="5" cellpadding="0" class="form-table" id="tabblocks">
                        <tr>
                            <td style="font-weight: bold; width: 100%;" colspan="6" align="center">
                                <cc1:ucGridView ID="gvPODetails" runat="server" AutoGenerateColumns="false" CssClass="grid"
                                    Width="100%">
                                    <AlternatingRowStyle BackColor="White" ForeColor="#284775"></AlternatingRowStyle>
                                    <Columns>
                                        <asp:TemplateField HeaderText="LineNbr" SortExpression="Line_No" HeaderStyle-HorizontalAlign="Left">
                                            <ItemStyle HorizontalAlign="Left" />
                                            <ItemTemplate>
                                                <cc1:ucLabel ID="lblLineNbrPOInq" runat="server" Text='<%# Eval("Line_No") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="ODSKUWithHash" SortExpression="OD_Code">
                                            <ItemStyle HorizontalAlign="Left" />
                                            <ItemTemplate>
                                                <cc1:ucLabel ID="lblODSKUPOInq" runat="server" Text='<%# Eval("OD_Code") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="VikingSKUWithHash" SortExpression="Direct_code">
                                            <ItemStyle HorizontalAlign="Left" />
                                            <ItemTemplate>
                                                <cc1:ucLabel ID="lblVikingSKUPOInq" runat="server" Text='<%# Eval("Direct_code") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="VendorCode" SortExpression="Vendor_Code">
                                            <ItemStyle HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <cc1:ucLabel ID="lblVendorCodePOInq" runat="server" Text='<%# Eval("Vendor_Code") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Description1" SortExpression="Product_description">
                                            <ItemStyle HorizontalAlign="Left" />
                                            <ItemTemplate>
                                                <cc1:ucLabel ID="lblDescriptionPOInq" runat="server" Text='<%# Eval("Product_description") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="OriginalQty" SortExpression="Original_quantity">
                                            <ItemStyle HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <cc1:ucLabel ID="lblOriginalQtyPOInq" runat="server" Text='<%# Eval("Original_quantity") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="OutstandingQty" SortExpression="Outstanding_Qty">
                                            <ItemStyle HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <cc1:ucLabel ID="lblOutstandingQtyPOInq" runat="server" Text='<%# Eval("Outstanding_Qty") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="ExpectedDate" SortExpression="Expected_date">
                                            <ItemStyle HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <cc1:ucLabel ID="lblExpectedDatePOInq" runat="server" Text='<%# Eval("Expected_date","{0:dd-MM-yyyy}") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="LastReceipt" SortExpression="LastReceiptDate">
                                            <ItemStyle HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <cc1:ucLabel ID="lblLastReceiptPOInq" runat="server" Text='<%# Eval("LastReceiptDate","{0:dd-MM-yyyy}") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                                </cc1:ucGridView>
                            </td>
                        </tr>
                    </table>
                </cc1:ucPanel>
                <cc1:ucPanel ID="pnlBookingDetails" runat="server" CssClass="fieldset-form" Style="display: block">
                    <table width="100%" cellspacing="5" cellpadding="0" class="form-table" id="Table1">
                        <tr>
                            <td style="font-weight: bold; width: 100%;" colspan="6" align="center">
                                <cc1:ucGridView ID="gvBookingDetails" runat="server" AutoGenerateColumns="false"
                                    CssClass="grid" Width="100%" OnRowDataBound="gvBookingDetails_OnRowDataBound">
                                    <AlternatingRowStyle BackColor="White" ForeColor="#284775"></AlternatingRowStyle>
                                    <Columns>
                                        <asp:TemplateField HeaderText="Date" SortExpression="ScheduleDate">
                                            <ItemStyle HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <cc1:ucLabel ID="lblDatePOInq" runat="server" Text='<%# Eval("ScheduleDate","{0:dd-MM-yyyy}") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="BookingTime" SortExpression="SlotTime.SlotTime">
                                            <ItemStyle HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <cc1:ucLabel ID="lblTimePOInq" runat="server" Text='<%# Eval("SlotTime.SlotTime") %>'></cc1:ucLabel>&nbsp;
                                                    <cc1:ucLabel ID="ltdayname" runat="server" Text='<%# Eval("ScheduleDate", "{0:dddd}")%>'></cc1:ucLabel>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Status" SortExpression="BookingStatus">
                                            <ItemStyle HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <cc1:ucLabel ID="lblStatusPOInq" runat="server" Text='<%# Eval("BookingStatus") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="BookingRefNo" SortExpression="BookingRef">
                                            <ItemStyle HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <asp:HiddenField ID="hdnBookingID" Value='<%# Eval("BookingID") %>' runat="server" />
                                                <asp:HyperLink ID="hpBookingRefPOInq" runat="server" Text='<%# Eval("BookingRef") %>'
                                                    NavigateUrl='<%# EncryptQuery("~/ModuleUI/Appointment/Receiving/APPRcv_BookingHistory.aspx?Scheduledate=" + Utilities.Common.GetDD_MM_YYYY(Eval("ScheduleDate").ToString()) + "&ID=BK-" + Eval("SupplierType") + "-" + Eval("BookingID") + "-" + Eval("SiteId") + "&PN=POInq&Status=" + lblStatusV.Text + "&PO=" + lblPurchaseOrderV.Text)  %>'></asp:HyperLink>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="ActualPallets" SortExpression="NumberOfPallet">
                                            <ItemStyle HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <cc1:ucLabel ID="lblPalletsPOInq" runat="server" Text='<%# Eval("NumberOfPallet") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="ActualCartons" SortExpression="NumberOfCartons">
                                            <ItemStyle HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <cc1:ucLabel ID="lblCartonsPOInq" runat="server" Text='<%# Eval("NumberOfCartons") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="ActualLines" SortExpression="NumberOfLines">
                                            <ItemStyle HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <cc1:ucLabel ID="lblLinesPOInq" runat="server" Text='<%# Eval("NumberOfLines") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="CarrierActual" SortExpression="Carrier.CarrierName">
                                            <ItemStyle HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <cc1:ucLabel ID="lblCarrierPOInq" runat="server" Text='<%# Eval("Carrier.CarrierName") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                                </cc1:ucGridView>
                            </td>
                        </tr>
                    </table>
                </cc1:ucPanel>
                <cc1:ucPanel ID="pnlDiscrepancies" runat="server" CssClass="fieldset-form" Style="display: block">
                    <table width="100%" cellspacing="5" cellpadding="0" class="form-table" id="Table2">
                        <tr>
                            <td style="font-weight: bold; width: 100%;" colspan="6" align="center">
                                <cc1:ucGridView ID="gvDiscrepancies" runat="server" AutoGenerateColumns="false" CssClass="grid"
                                    Width="100%" OnRowDataBound="gvDiscrepancies_RowDataBound">
                                    <AlternatingRowStyle BackColor="White" ForeColor="#284775"></AlternatingRowStyle>
                                    <Columns>
                                        <asp:TemplateField HeaderText="Status" SortExpression="DiscrepancyStatus">
                                            <ItemStyle HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <asp:HiddenField ID="hdnDiscrepancyTypeID" Value='<%# Eval("DiscrepancyTypeID") %>' runat="server" />
                                                <asp:HiddenField ID="hdnDiscrepancyLogID" Value='<%# Eval("DiscrepancyLogID") %>' runat="server" />
                                                <asp:HiddenField ID="hdnUserID" Value='<%# Eval("UserID") %>' runat="server" />

                                                <cc1:ucLabel ID="lblStatusPOInq" runat="server" Text='<%# Eval("DiscrepancyStatus") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="VDRNumber" SortExpression="VDRNo">
                                            <ItemStyle HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <asp:HyperLink ID="hpBookingRefPOInq" runat="server" Text='<%# Eval("VDRNo") %>'></asp:HyperLink>
                                                <%--NavigateUrl='<%# EncryptQuery("~/ModuleUI/Discrepancy/LogDiscrepancy/DISLog_PresentationIssue.aspx?disLogID=" + Eval("DiscrepancyLogID") + "&VDRNo=" + Eval("VDRNo") + "&UserID=" + Eval("UserID")+ "&PN=POINQ&Status=" + lblStatusV.Text + "&PO=" + lblPurchaseOrderV.Text + "&SId=" + hdnBaseSiteId.Value)  %>'></asp:HyperLink>--%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="VDRDiscription" SortExpression="DiscrepancyType">
                                            <ItemStyle HorizontalAlign="Left" />
                                            <ItemTemplate>
                                                <cc1:ucLabel ID="lblVDRDescription" runat="server" Text='<%# Eval("DiscrepancyType") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="CreatedBy" SortExpression="Username">
                                            <ItemStyle HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <cc1:ucLabel ID="lblCreatedByPOInq" runat="server" Text='<%# Eval("Username") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="VDRDate" SortExpression="DiscrepancyLogDate">
                                            <ItemStyle HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <cc1:ucLabel ID="lblVDRDatePOInq" runat="server" Text='<%# Eval("DiscrepancyLogDate","{0:dd-MM-yyyy}") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                                </cc1:ucGridView>
                            </td>
                        </tr>
                    </table>
                </cc1:ucPanel>
                <cc1:ucPanel ID="pnlReceipts" runat="server" CssClass="fieldset-form" Visible="true">
                    <table width="100%" cellspacing="5" cellpadding="0" class="form-table" id="Table5">
                        <tr>
                            <td style="font-weight: bold; width: 100%;" colspan="6" align="left">
                                <cc1:ucLinkButton ID="lnkClickToView" Text="Click to view" Enabled="false" runat="server" ForeColor="#026DCF"
                                    OnClick="lnkClickToView_Click" Font-Bold="true"></cc1:ucLinkButton>
                            </td>
                        </tr>
                    </table>
                </cc1:ucPanel>
                <cc1:ucPanel ID="pnlPOReceipts" runat="server" CssClass="fieldset-form" Visible="false">
                    <table width="100%" cellspacing="5" cellpadding="0" class="form-table" id="Table4">
                        <tr>
                            <td style="font-weight: bold; width: 70%;" colspan="6" align="center">
                                <%--<cc1:ucGridView ID="gvPOReceipts" runat="server" AutoGenerateColumns="false" CssClass="grid"
                                    Width="100%" OnRowDataBound="gvPOReceipts_RowDataBound">
                                    <AlternatingRowStyle BackColor="White" ForeColor="#284775"></AlternatingRowStyle>
                                    <Columns>
                                    </Columns>
                                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                                </cc1:ucGridView>--%>
                                <asp:Repeater ID="rptPOReceipts" runat="server">
                                    <ItemTemplate>
                                        <table width="100%" cellspacing="2" cellpadding="0" class="form-table">
                                            <tr>
                                                <td style="width: 100%;" align="left" valign="top">
                                                    <cc1:ucLabel ID="lblDate" Text="Date" runat="server"></cc1:ucLabel>&nbsp;-
                                                    <cc1:ucLabel ID="lblDateText" Text='<%# Eval("ReceiptDate", "{0:dd/MM/yyyy}") %>' runat="server"></cc1:ucLabel>
                                                    <asp:HiddenField ID="hdnDateText" Value='<%# Bind("ReceiptDate") %>' runat="server" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 100%;" align="left" valign="top">
                                                    <cc1:ucGridView ID="gvPOReceiptsDetails" runat="server" AutoGenerateColumns="false" CssClass="grid" Width="100%">
                                                        <AlternatingRowStyle BackColor="White" ForeColor="#284775"></AlternatingRowStyle>
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="Line" SortExpression="Line_No" HeaderStyle-HorizontalAlign="Left">
                                                                <ItemStyle HorizontalAlign="Center" />
                                                                <ItemTemplate>
                                                                    <cc1:ucLabel ID="lblLineText" Text='<%# Bind("Line_No") %>' runat="server"></cc1:ucLabel>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="SKU" SortExpression="ODSKU_No" HeaderStyle-HorizontalAlign="Left">
                                                                <ItemStyle HorizontalAlign="Center" />
                                                                <ItemTemplate>
                                                                    <cc1:ucLabel ID="lblSKUText" Text='<%# Bind("ODSKU_No") %>' runat="server"></cc1:ucLabel>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="VendorCode" SortExpression="Vendor_Code" HeaderStyle-HorizontalAlign="Left">
                                                                <ItemStyle HorizontalAlign="Center" />
                                                                <ItemTemplate>
                                                                    <cc1:ucLabel ID="lblVendorCodeText" Text='<%# Bind("Vendor_Code") %>' runat="server"></cc1:ucLabel>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Description1" SortExpression="ProductDescription" HeaderStyle-HorizontalAlign="Left">
                                                                <ItemStyle HorizontalAlign="Center" />
                                                                <ItemTemplate>
                                                                    <cc1:ucLabel ID="lblDescriptionText" Text='<%# Bind("ProductDescription") %>' runat="server"></cc1:ucLabel>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="QtyReceipted" SortExpression="Qty_receipted" HeaderStyle-HorizontalAlign="Left">
                                                                <ItemStyle HorizontalAlign="Center" />
                                                                <ItemTemplate>
                                                                    <cc1:ucLabel ID="lblQtyReceiptedText" Text='<%# Bind("Qty_receipted") %>' runat="server"></cc1:ucLabel>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                        <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                                                    </cc1:ucGridView>
                                                </td>
                                            </tr>
                                        </table>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </td>
                        </tr>
                    </table>
                </cc1:ucPanel>

                <cc1:ucPanel ID="pnlScheduledPOReconcilliation" runat="server" CssClass="fieldset-form" Visible="true">
                    <table width="100%" cellspacing="5" cellpadding="0" class="form-table" id="Table6">
                        <tr>
                            <td style="font-weight: bold; width: 100%;" colspan="6" align="left">
                                <asp:HyperLink ID="lnkClickToView_1" Text="Click to view" runat="server" ForeColor="#026DCF"
                                    Font-Bold="true" Target="_blank"></asp:HyperLink>
                            </td>
                        </tr>
                    </table>
                </cc1:ucPanel>

                <cc1:ucPanel ID="pnlPenaltyManagement" runat="server" CssClass="fieldset-form" Visible="true">
                    <table width="100%" cellspacing="5" cellpadding="0" class="form-table" id="Table7">
                        <tr>
                            <td style="font-weight: bold; width: 100%;" colspan="6" align="left">
                                <asp:HyperLink ID="lnkClickToView_2" Text="Click to view" runat="server" ForeColor="#026DCF"
                                    Font-Bold="true" Target="_blank"></asp:HyperLink>
                            </td>
                        </tr>
                    </table>
                </cc1:ucPanel>

                <cc1:ucPanel ID="pnlViewDebits" runat="server" CssClass="fieldset-form" Visible="true">
                    <table width="100%" cellspacing="5" cellpadding="0" class="form-table" id="Table8">
                        <tr>
                            <td style="font-weight: bold; width: 100%;" colspan="6" align="left">
                                <asp:HyperLink ID="lnkClickToView_3" Text="Click to view" runat="server" ForeColor="#026DCF"
                                    Font-Bold="true" Target="_blank"></asp:HyperLink>
                            </td>
                        </tr>
                    </table>
                </cc1:ucPanel>

                <table width="100%" cellspacing="5" cellpadding="0" class="form-table" id="Table3">
                    <tr>
                        <td style="font-weight: bold; width: 100%;">
                            <asp:UpdatePanel ID="updPannelBack" runat="server">
                                <ContentTemplate>
                                    <%--<a id="btnSendMail" runat="server" target='_blank' onclick='window.open(this.href, this.target,"scrollbars=1,left=100px,top=20px,width=800px,height=600px,toolbar=0,scrollbars=1,status=0"); return false;'> EMail PO To Vendor </a>--%>
                                    <div class="button-row" style="text-align: right">
                                        <cc1:ucButton ID="btnSendMail" Text="EMail PO To Vendor" UseSubmitBehavior="false" runat="server" 
                                            CssClass="button" />
                                        <cc1:ucButton ID="btnBack" runat="server" CssClass="button" OnClick="btnBack_Click" />
                                    </div>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="btnBack" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        </div>
    </div>
    <div class="bottom-shadow">
    </div>
</asp:Content>

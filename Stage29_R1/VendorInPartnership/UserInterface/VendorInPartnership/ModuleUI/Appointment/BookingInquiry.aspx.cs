﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.AdminFunctions;
using BusinessEntities.ModuleBE.Appointment.CountrySetting;
using BusinessLogicLayer.ModuleBAL.Appointment.CountrySetting;
using Utilities;
using System.Data;
using BusinessEntities.ModuleBE.Appointment.Booking;
using BusinessLogicLayer.ModuleBAL.Appointment.Booking;
using BaseControlLibrary;
using System.Collections;

public partial class BookingInquiry : CommonPage
{
    public int? PreSiteCountryID
    {
        get
        {
            return ViewState["PreSiteCountryID"] != null ? Convert.ToInt32(ViewState["PreSiteCountryID"].ToString()) : (int?)null;
        }
        set
        {
            ViewState["PreSiteCountryID"] = value;
        }
    }

    public SortDirection GridViewSortDirection
    {
        get
        {
            if (ViewState["sortDirection"] == null)
                ViewState["sortDirection"] = SortDirection.Ascending;
            return (SortDirection)ViewState["sortDirection"];
        }
        set { ViewState["sortDirection"] = value; }
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        ddlSite.CurrentPage = this;
        ddlSite.innerControlddlSite.AutoPostBack = true;
        ddlSite.IsAllRequired = true;
        ucSeacrhVendor1.CurrentPage = this;


    }

    protected void Page_Load(object sender, EventArgs e)
    {


        if (!IsPostBack)
        {
            txtFromDate.Text = DateTime.Now.AddDays(-DateTime.Now.Day + 1).ToString("dd/MM/yyyy");
            txtToDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
            hdnJSFromDt.Value = txtFromDate.Text;
            hdnJSToDt.Value = txtToDate.Text;
        }

        if (IsPostBack)
        {
            txtFromDate.Text = hdnJSFromDt.Value;
            txtToDate.Text = hdnJSToDt.Value;
            msVendor.setVendorsOnPostBack();
        }

        ucExportToExcel1.CurrentPage = this;
        ucExportToExcel1.GridViewControl = gvExport;
        ucExportToExcel1.FileName = "BookingInquiry";         
    
        txtBookingRef.Focus();
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {

        if (!Page.IsPostBack)
        {
            if (Session["BookingInquery"] != null)
            {
                GetSession();
                BindBookings();
            }
            else if (Session["SiteID"] != null)
            {
                ddlSite.innerControlddlSite.SelectedIndex = ddlSite.innerControlddlSite.Items.IndexOf(
                    ddlSite.innerControlddlSite.Items.FindByValue(Convert.ToString(Session["SiteID"])));
                BindCarrier();
            }
        }
    }

    public override void SitePost_Load()
    {
        base.SitePost_Load();
        MAS_SiteBE oMAS_SiteBE = ddlSite.oSiteBE;
        if (oMAS_SiteBE != null)
        {
            PreSiteCountryID = Convert.ToInt32(oMAS_SiteBE.SiteCountryID);
        }
    }
    public override void SiteSelectedIndexChanged()
    {
        base.SiteSelectedIndexChanged();

        txtFromDate.Text = hdnJSFromDt.Value;
        txtToDate.Text = hdnJSToDt.Value;
        msVendor.setVendorsOnPostBack();
        ddlSite.SelectedIndexChanged();

        MAS_SiteBE oMAS_SiteBE = ddlSite.oSiteBE;

        if (oMAS_SiteBE != null)
        {

            int CurrentSiteCountryID = Convert.ToInt32(oMAS_SiteBE.SiteCountryID);
            if (PreSiteCountryID != CurrentSiteCountryID)
            {
                ucSeacrhVendor1.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
                ucSeacrhVendor1.ClearSearch();
                BindCarrier();
            }
            else
            {
                if (!Page.IsPostBack)
                {
                    ucSeacrhVendor1.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
                    ucSeacrhVendor1.ClearSearch();
                    BindCarrier();
                }
            }
            PreSiteCountryID = CurrentSiteCountryID;
        }
        else
        {
            PreSiteCountryID = 0;
            BindCarrier();
        }
    }

    private void BindCarrier()
    {
        MASCNT_CarrierBE oMASCNT_CarrierBE = new MASCNT_CarrierBE();
        APPCNT_CarrierBAL oMASCNT_CarrierBAL = new APPCNT_CarrierBAL();

        oMASCNT_CarrierBE.Action = "ShowAll";

        oMASCNT_CarrierBE.CountryID = ddlSite.CountryID;
        oMASCNT_CarrierBE.CountryIDs = ddlSite.CountryIDs;

        List<MASCNT_CarrierBE> lstCarrier = oMASCNT_CarrierBAL.GetCarrierDetailsBAL(oMASCNT_CarrierBE);

        if (lstCarrier.Count > 0)
        {
            FillControls.FillDropDown(ref ddlCarrier, lstCarrier, "CarrierName", "CarrierID", "--Select--");
        }
    }

    protected void gridView_Sorting(Object sender, GridViewSortEventArgs e)
    {
        try
        {
            string sortExpression = e.SortExpression;
            if (GridViewSortDirection == SortDirection.Ascending)
            {
                GridViewSortDirection = SortDirection.Descending;
                SortGridView(sortExpression, " ASC");
            }
            else
            {
                GridViewSortDirection = SortDirection.Ascending;
                SortGridView(sortExpression, " DESC");
            }
        }
        catch (Exception ex)
        {
            LogUtility.SaveErrorLogEntry(ex);
        }
    }

    private void SortGridView(string sortExpression, string direction)
    {
        DataTable dtSortingTbl;
        DataView dtView;
        try
        {
            if (ViewState["lstSites"] != null)
            {
                dtSortingTbl = (DataTable)ViewState["lstSites"];
                dtView = dtSortingTbl.DefaultView;
                if (sortExpression != "Weekday")
                {
                    int sortExpIndx = sortExpression.IndexOf(",");
                    if (sortExpIndx == -1)
                        dtView.Sort = sortExpression + direction;
                    else
                    {
                        dtView.Sort = sortExpression.Substring(0, sortExpIndx) + direction + sortExpression.Substring(sortExpIndx);
                    }
                }
                else
                {
                    if (direction.Trim() == "ASC")
                        dtView.Sort = "Weekday desc,OrderBY";
                    else
                        dtView.Sort = "Weekday asc,OrderBY";
                }
                if (sortExpression != "AutoID")
                    dtSortingTbl = GetSortByBooking(dtView.ToTable());
                else
                    dtSortingTbl = dtView.ToTable();

                string Role = Session["Role"].ToString().Trim().ToLower();
                GridView gridToParse = new GridView();

                gridToParse = gvVendor;

                gridToParse.DataSource = dtSortingTbl;
                gridToParse.DataBind();
                ViewState["lstSites"] = dtSortingTbl;
                CommonPage c = new CommonPage();
                c.LocalizeGridHeader(gridToParse);
            }
        }
        catch (Exception ex)
        {
            LogUtility.SaveErrorLogEntry(ex);
        }

    }

    DataTable dtSortingTblByBooking = null;
    private DataTable GetSortByBooking(DataTable dtSortingTbl)
    {
        if (dtSortingTbl != null && dtSortingTbl.Rows.Count > 0)
        {
            if (dtSortingTblByBooking == null)
                dtSortingTblByBooking = dtSortingTbl.Clone();

            string bookingRef = dtSortingTbl.Rows[0]["BookingRef"].ToString();

            DataRow[] rows;
            rows = dtSortingTbl.Select("BookingRef = '" + bookingRef + "'");
            foreach (DataRow r in rows)
            {
                dtSortingTblByBooking.ImportRow(r);
                r.Delete();
            }
            dtSortingTblByBooking.AcceptChanges();         
            GetSortByBooking(dtSortingTbl);
        }
        return dtSortingTblByBooking;
    }

    protected void gvVendor_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvVendor.PageIndex = e.NewPageIndex;
        if (Session["BookingInquery"] != null)
        {
            Hashtable htBookingCriteria = (Hashtable)Session["BookingInquery"];

            htBookingCriteria.Remove("PageIndex");
            htBookingCriteria.Add("PageIndex", e.NewPageIndex);

            Session["BookingInquery"] = htBookingCriteria;
        }
        BindBookingPageWise();
    }

    protected void gvVendor_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        GridViewRow gvr = e.Row;
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            #region Code to set visibility of [Time / Day] based on [Time Window Id] ..
            ucLiteral UcLiteral27 = (ucLiteral)e.Row.FindControl("UcLiteral27");
            ucLiteral UcLiteral27_1 = (ucLiteral)e.Row.FindControl("UcLiteral27_1");
            ucLiteral UcLiteral27_2 = (ucLiteral)e.Row.FindControl("UcLiteral27_2");

            if (!string.IsNullOrEmpty(UcLiteral27_1.Text) && !string.IsNullOrWhiteSpace(UcLiteral27_1.Text))
            {
                UcLiteral27_2.Visible = false;
                UcLiteral27_1.Visible = true;
                UcLiteral27.Visible = false;
            }
            else if (!string.IsNullOrEmpty(UcLiteral27_2.Text) && !string.IsNullOrWhiteSpace(UcLiteral27_2.Text))
            {
                UcLiteral27_2.Visible = true;
                UcLiteral27_1.Visible = false;
                UcLiteral27.Visible = false;
            }
            else
            {
                UcLiteral27_2.Visible = false;
                UcLiteral27_1.Visible = false;
                UcLiteral27.Visible = true;
            }
            #endregion

            ucLiteral ltHistory = (ucLiteral)e.Row.FindControl("ltHistory");
            HyperLink hypHistory = (HyperLink)e.Row.FindControl("hypHistory");

            if (ltHistory.Text == "Fixed booking")
            {
                ltHistory.Visible = true;
                hypHistory.Visible = false;
            }
            else
            {
                ltHistory.Visible = false;
                hypHistory.Visible = true;
            }

        }
    }

    private void BindBookingPageWise()
    {
        if (ViewState["lstSites"] != null)
        {
            DataTable lstBookings = new DataTable();
            lstBookings = (DataTable)ViewState["lstSites"];
            lstBookings.DefaultView.Sort = "DeliveryDate,OrderBY";
            gvVendor.DataSource = lstBookings;
            gvVendor.DataBind();
        }
        else
        {
            BindBookings();
        }
    }

    protected void BindBookings()
    {
        APPBOK_BookingBE oAPPBOK_BookingBE = new APPBOK_BookingBE();
        APPBOK_BookingBAL oAPPBOK_BookingBAL = new APPBOK_BookingBAL();


        oAPPBOK_BookingBE.FixedSlot = new BusinessEntities.ModuleBE.Appointment.SiteSettings.MASSIT_FixedSlotBE();
        oAPPBOK_BookingBE.Carrier = new BusinessEntities.ModuleBE.Appointment.CountrySetting.MASCNT_CarrierBE();
        oAPPBOK_BookingBE.Delivery = new BusinessEntities.ModuleBE.Appointment.CountrySetting.MASCNT_DeliveryTypeBE();
        oAPPBOK_BookingBE.FixedSlot.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();

        if (txtBookingRef.Text.Trim() != string.Empty)
            oAPPBOK_BookingBE.BookingRef = txtBookingRef.Text.Trim();

        if (ddlSite.innerControlddlSite.SelectedIndex > 0)
            oAPPBOK_BookingBE.FixedSlot.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
        else
            oAPPBOK_BookingBE.FixedSlot.SiteIDs = ddlSite.UserSiteIDs;

        oAPPBOK_BookingBE.FromDate = Utilities.Common.TextToDateFormat(hdnJSFromDt.Value);
        oAPPBOK_BookingBE.ToDate = Utilities.Common.TextToDateFormat(hdnJSToDt.Value);

        if (ddlCarrier.SelectedIndex > 0)
            oAPPBOK_BookingBE.Carrier.CarrierID = Convert.ToInt32(ddlCarrier.SelectedItem.Value);

        if (ucSeacrhVendor1.VendorNo.Trim() != string.Empty)
            oAPPBOK_BookingBE.VendorID = Convert.ToInt32(ucSeacrhVendor1.VendorNo);

        if (!string.IsNullOrEmpty(msVendor.SelectedVendorIDs))
            oAPPBOK_BookingBE.VendorIDs = msVendor.SelectedVendorIDs;

        DataTable lstBookings = new DataTable();

        lstBookings = oAPPBOK_BookingBAL.GetBookingInqueryBAL(oAPPBOK_BookingBE);

        if (lstBookings != null && lstBookings.Rows.Count > 0)
        {
            lstBookings = lstBookings.AsEnumerable()
                       .OrderBy(r => r.Field<DateTime>("DeliveryDate"))
                       .ThenBy(r => r.Field<int>("OrderBY"))
                       .CopyToDataTable();
        }

        DataView lstBooking = lstBookings.DefaultView;

        if (ddlStatus.SelectedIndex > 0)
        {

            switch (ddlStatus.SelectedItem.Value)
            {
                case "1":
                    lstBooking.RowFilter = "BookingStatus = 'Confirmed Slot' OR " +
                                       "BookingStatus = 'Confirm Window'";

                    break;

                case "2":
                    lstBooking.RowFilter = "BookingStatus = 'Delivery Arrived'";
                    break;

                case "3":
                    lstBooking.RowFilter = "BookingStatus = 'Delivery Unloaded' OR " +
                                    "BookingStatus = 'Partially Refused'";
                    break;

                case "4":
                    lstBooking.RowFilter = "BookingStatus = 'Partially Refused' OR " +
                                    "BookingStatus = 'Refused Delivery' OR " +
                                    "BookingStatus = 'QC for PR' ";
                    break;

                case "5":
                    lstBooking.RowFilter = "BookingStatus = 'QC for PR' OR " +
                                    "BookingStatus = 'Quality Checked'";
                    break;

                case "6":
                    lstBooking.RowFilter = "BookingStatus = 'Delivery Arrived' OR " +
                                    "BookingStatus = 'Delivery Unloaded' OR " +
                                    "BookingStatus = 'QC for PR' OR " +
                                    "BookingStatus = 'Quality Checked' OR " +
                                    "BookingStatus = 'Partially Refused'";
                    break;

                case "7":
                    lstBooking.RowFilter = "BookingStatus = 'No Show' OR " +
                                    "BookingStatus = 'Refused Delivery' OR " +
                                     "BookingStatus = 'QC for PR' OR " +
                                    "BookingStatus = 'Partially Refused'";
                    break;

                case "8":
                    lstBooking.RowFilter = "BookingType = 'Provisional'";
                    break;
                case "9":
                    lstBooking.RowFilter = "BookingStatus = 'Deleted'";
                    break;
                case "10":
                    lstBooking.RowFilter = "BookingStatus = 'Unconfirmed Fixed' OR " +
                                       "BookingStatus = 'Reserved Window'  OR " +
                                       "BookingStatus = 'Open Fixed'";
                    break;
                default:
                    break;
            }
        }

        lstBookings = lstBooking.ToTable();

        gvVendor.DataSource = lstBookings;
        gvVendor.DataBind();
        gvExport.DataSource = lstBookings;
        gvExport.DataBind();
        ViewState["lstSites"] = lstBookings;
        ucExportToExcel1.Visible = true;
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        gvVendor.PageIndex = 0;
        BindBookings();
        SetSession();
    }

    private void SetSession()
    {
        Session["BookingInquery"] = null;
        Hashtable htBookingCritreria = new Hashtable();
        htBookingCritreria.Add("BookingRef", txtBookingRef.Text.Trim());

        if (ddlSite.innerControlddlSite.SelectedIndex >= 0)
            htBookingCritreria.Add("site", ddlSite.innerControlddlSite.SelectedItem.Value);

        htBookingCritreria.Add("FromDate", hdnJSFromDt.Value);
        htBookingCritreria.Add("ToDate", hdnJSToDt.Value);

        if (ddlCarrier.SelectedIndex >= 0)
            htBookingCritreria.Add("CarrierID", ddlCarrier.SelectedItem.Value);

        if (ucSeacrhVendor1.VendorNo != string.Empty)
        {
            htBookingCritreria.Add("VendorNo", ucSeacrhVendor1.VendorNo);
            ucLabel SelectedVendorName = (ucLabel)ucSeacrhVendor1.FindControl("SelectedVendorName");
            htBookingCritreria.Add("VendorName", SelectedVendorName.Text);
        }

        htBookingCritreria.Add("VendorIDs", msVendor.SelectedVendorIDs);
        htBookingCritreria.Add("SelectedVendorName", msVendor.SelectedVendorName);

        htBookingCritreria.Add("PageIndex", 0);
        htBookingCritreria.Add("PreSiteCountryID", PreSiteCountryID);

        Session["BookingInquery"] = htBookingCritreria;
    }

    private void GetSession()
    {
        if (Session["BookingInquery"] != null)
        {
            Hashtable htBookingCriteria = (Hashtable)Session["BookingInquery"];

            txtBookingRef.Text = htBookingCriteria["BookingRef"].ToString();

            if (htBookingCriteria.ContainsKey("site"))
            {
                PreSiteCountryID = Convert.ToInt32(htBookingCriteria["PreSiteCountryID"]);
                ddlSite.innerControlddlSite.SelectedValue = htBookingCriteria["site"].ToString();
                ddlSite.PreCountryID = PreSiteCountryID;
                ddlSite.CountryID = Convert.ToInt32(PreSiteCountryID);
                SiteSelectedIndexChanged();
            }

            txtFromDate.Text = htBookingCriteria.ContainsKey("FromDate") ? htBookingCriteria["FromDate"].ToString() : "";
            txtToDate.Text = htBookingCriteria.ContainsKey("ToDate") ? htBookingCriteria["ToDate"].ToString() : "";
            hdnJSFromDt.Value = txtFromDate.Text;
            hdnJSToDt.Value = txtToDate.Text;

            if (htBookingCriteria.ContainsKey("CarrierID"))
                ddlCarrier.SelectedValue = htBookingCriteria["CarrierID"].ToString();

            if (htBookingCriteria.ContainsKey("VendorNo"))
            {
                ucLabel SelectedVendorName = (ucLabel)ucSeacrhVendor1.FindControl("SelectedVendorName");
                SelectedVendorName.Text = htBookingCriteria["VendorName"].ToString();
                ucSeacrhVendor1.VendorNo = htBookingCriteria["VendorNo"].ToString();
            }

            if (htBookingCriteria["VendorIDs"] != null)
            {
                msVendor.SelectedVendorIDs = htBookingCriteria["VendorIDs"].ToString();
                msVendor.SelectedVendorName = htBookingCriteria["SelectedVendorName"].ToString();
                msVendor.setVendorsOnPostBack();
            }
            gvVendor.PageIndex = Convert.ToInt32(htBookingCriteria["PageIndex"].ToString());
        }
    }
}
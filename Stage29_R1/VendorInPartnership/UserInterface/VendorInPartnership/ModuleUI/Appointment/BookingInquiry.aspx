﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    AutoEventWireup="true" CodeFile="BookingInquiry.aspx.cs" Inherits="BookingInquiry"
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<%@ Register Src="../../CommonUI/UserControls/ucSeacrhVendor.ascx" TagName="ucSeacrhVendor"
    TagPrefix="uc1" %>
<%@ Register Src="../../CommonUI/UserControls/ucSite.ascx" TagName="ucSite" TagPrefix="uc2" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectVendor.ascx" TagName="MultiSelectVendor"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/ucExportToExcel.ascx" TagName="ucExportToExcel"
    TagPrefix="cc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script type="text/javascript" language="javascript">
        function setValue1(target) {
            document.getElementById('<%=hdnJSFromDt.ClientID %>').value = target.value;
        }

        function setValue2(target) {
            document.getElementById('<%=hdnJSToDt.ClientID %>').value = target.value;
        }
        $(document).ready(function () {
            $("body").keypress(function (e) {
                if (e.which == 13 || e.keyCode == 13) {
                    $('#<%=btnSearch.ClientID %>').click();
                    return false;
                }
            });
        });
    </script>
    <h2>
        <cc1:ucLabel ID="lblBookingInquiry" runat="server"></cc1:ucLabel>
    </h2>
    <asp:HiddenField ID="hdnJSFromDt" runat="server" />
    <asp:HiddenField ID="hdnJSToDt" runat="server" />
    <div class="right-shadow">
        <div class="formbox">
            <cc1:ucPanel ID="pnlBookingInquiry" runat="server" CssClass="fieldset-form">
                <table width="95%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settingsNoBorder">
                    <tr>
                        <td style="font-weight: bold; width: 13%">
                            <cc1:ucLabel ID="lblBookingRef" runat="server" Text="Booking Reference"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold; width: 1%">:
                        </td>
                        <td style="width: 36%">
                            <cc1:ucTextbox ID="txtBookingRef" runat="server" MaxLength="20" Width="180px"></cc1:ucTextbox>
                        </td>
                        <td style="font-weight: bold; width: 5%">
                            <cc1:ucLabel ID="lblSite" runat="server" Text="Site"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold; width: 1%">:
                        </td>
                        <td style="width: 44%">
                            <uc2:ucSite ID="ddlSite" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblDateFrom" runat="server" Text="Date From"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">:
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucTextbox ID="txtFromDate" runat="server" ClientIDMode="Static" CssClass="date"
                                ReadOnly="True" Width="70px" onchange="setValue1(this)" />
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblDateTo" runat="server" Text="Date To"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">:
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucTextbox ID="txtToDate" runat="server" ClientIDMode="Static" CssClass="date"
                                ReadOnly="True" Width="70px" onchange="setValue2(this)" />
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblCarrier" runat="server" Text="Carrier"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">:
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucDropdownList ID="ddlCarrier" runat="server" Width="150px">
                            </cc1:ucDropdownList>
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblStatus" runat="server" Text="Status"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">:
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucDropdownList ID="ddlStatus" runat="server" Width="130px">
                                <asp:ListItem Text="--All--" Value="0"></asp:ListItem>
                                <asp:ListItem Text="Confirmed, not arrived" Value="1"></asp:ListItem>
                                <asp:ListItem Text="Unconfirmed" Value="10"></asp:ListItem>
                                <asp:ListItem Text="Delivery Arrived" Value="2"></asp:ListItem>
                                <asp:ListItem Text="Delivery Unloaded" Value="3"></asp:ListItem>
                                <asp:ListItem Text="Refused" Value="4"></asp:ListItem>
                                <asp:ListItem Text="Quality Checked" Value="5"></asp:ListItem>
                                <asp:ListItem Text="On site" Value="6"></asp:ListItem>
                                <asp:ListItem Text="Failed" Value="7"></asp:ListItem>
                                <asp:ListItem Text="Pending" Value="8"></asp:ListItem>
                                <asp:ListItem Text="Deleted" Value="9"></asp:ListItem>
                            </cc1:ucDropdownList>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblVendorNo" runat="server" Text="Vendor No"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">:
                        </td>
                        <td style="font-weight: bold;" colspan="4">
                            <span id="spVender" runat="server">
                                <uc1:ucSeacrhVendor ID="ucSeacrhVendor1" runat="server" />
                            </span>
                            <cc1:ucLiteral ID="ltSearchVendorName" runat="server" Visible="false"></cc1:ucLiteral>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="6">
                            <table>
                                <tr>
                                    <td style="font-weight: bold; width: 13%">
                                        <cc1:ucLabel ID="lblVendor" runat="server" Text="Vendor"></cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold; width: 1%">
                                        <cc1:ucLabel ID="lblVendorCollon" runat="server">:</cc1:ucLabel>
                                    </td>
                                    <td>
                                        <cc1:MultiSelectVendor runat="server" ID="msVendor" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="6">
                            <div class="button-row" style="text-align: center;">
                                <cc1:ucButton ID="btnSearch" runat="server" Text="Search" CssClass="button" OnClick="btnSearch_Click" />
                            </div>
                        </td>
                    </tr>
                </table>
            </cc1:ucPanel>
        </div>
    </div>
    <div class="button-row">
        <cc2:ucExportToExcel ID="ucExportToExcel1" runat="server"  Visible="false"/>
    </div>
    <div style="width: 99%; overflow: auto" id="divPrint" class="fixedTable" onscroll="getScroll1(this);">
        <table cellspacing="1" cellpadding="0" class="form-table">
            <tr>
                <td>
                    <cc1:ucGridView ID="gvVendor" runat="server" AutoGenerateColumns="false" CssClass="grid gvclass"
                        CellPadding="0" Width="980px" DataKeyNames="PKID" OnSorting="gridView_Sorting"
                        AllowSorting="false" AllowPaging="true" PageSize="30" OnPageIndexChanging="gvVendor_PageIndexChanging"
                        OnRowDataBound="gvVendor_RowDataBound">
                        <AlternatingRowStyle BackColor="White" ForeColor="#284775"></AlternatingRowStyle>
                        <EmptyDataTemplate>
                            <div style="text-align: center">
                                <cc1:ucLabel ID="lblNoData" runat="server" Text="No Records found."></cc1:ucLabel>
                            </div>
                        </EmptyDataTemplate>
                        <Columns>
                            <asp:TemplateField HeaderText="Date">
                                <HeaderStyle Width="100px" />
                                <ItemStyle HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <asp:HiddenField ID="hdnExpectedDeliveryDate" runat="server" Value='<%# Eval("DeliveryDate") %>' />
                                    <cc1:ucLabel ID="lblExpectedDeliveryDate" runat="server" Text='<%# Eval("DeliveryDate", "{0:dd/MM/yyyy}") %>'></cc1:ucLabel>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Time / Day">
                                <HeaderStyle Width="150px" />
                                <ItemStyle HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <asp:HiddenField ID="hdnTimeWindowID" runat="server" Value='<%# Eval("TimeWindowID") %>' />
                                    <cc1:ucLiteral ID="UcLiteral27_2" runat="server" Text='<%#Eval("NonWindowFromToTime") %>'
                                        Visible="false"></cc1:ucLiteral>
                                    <cc1:ucLiteral ID="UcLiteral27_1" runat="server" Text='<%#Eval("WindowStartEndTime") %>'
                                        Visible="false"></cc1:ucLiteral>
                                    <cc1:ucLiteral ID="UcLiteral27" runat="server" Text='<%#Eval("ExpectedDeliveryTime") %>'></cc1:ucLiteral>&nbsp;
                                    <cc1:ucLiteral ID="UcLiteral28" runat="server" Text='<%# Eval("DeliveryDate", "{0:dddd}")%>'></cc1:ucLiteral>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Site">
                                <HeaderStyle Width="120px" />
                                <ItemStyle HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <cc1:ucLabel ID="LBLSiteName" runat="server" Text='<%# Eval("SiteName") %>'></cc1:ucLabel>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Booking Ref #">
                                <HeaderStyle Width="150px" />
                                <ItemStyle HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <cc1:ucLiteral ID="ltHistory" runat="server" Text='<%#Eval("BookingRef") %>'></cc1:ucLiteral>
                                    <asp:HyperLink ID="hypHistory" runat="server" Text='<%#Eval("BookingRef") %>' NavigateUrl='<%# EncryptQuery("~/ModuleUI/Appointment/Receiving/APPRcv_BookingHistory.aspx?PN=BOINQ&Scheduledate=" + Eval("DeliveryDate", "{0:dd/MM/yyyy}") + "&ID=" + Eval("PKID") + "-V-" + Eval("PKID") + "-" +  Eval("PKID")) %>'></asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Vendor Name">
                                <HeaderStyle Width="200px" />
                                <ItemStyle HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <cc1:ucLiteral ID="UcLiteral2" runat="server" Text='<%#Eval("VendorName") %>'></cc1:ucLiteral>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Carrier Name">
                                <HeaderStyle Width="150px" />
                                <ItemStyle HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <cc1:ucLiteral ID="UcLiteral3" runat="server" Text='<%#Eval("CarrierName") %>'></cc1:ucLiteral>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Status" DataField="BookingStatus">
                                <HeaderStyle Width="120px" />
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:BoundField HeaderText="ActualPallets" DataField="Pallets" SortExpression="Pallets">
                                <HeaderStyle Width="50px" HorizontalAlign="Left" />
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:BoundField HeaderText="ActualCartons" DataField="Cartons" SortExpression="Cartons">
                                <HeaderStyle Width="50px" HorizontalAlign="Left" />
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:BoundField HeaderText="ActualLines" DataField="Lines" SortExpression="Lines">
                                <HeaderStyle Width="50px" HorizontalAlign="Left" />
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="VehicleType">
                                <HeaderStyle Width="120px" />
                                <ItemStyle HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <cc1:ucLabel ID="LBLVehicleType" runat="server" Text='<%# Eval("VehicleType") %>'></cc1:ucLabel>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="BookingComments">
                                <HeaderStyle Width="120px" />
                                <ItemStyle HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <cc1:ucLabel ID="LBLBookingComments" runat="server" Text='<%# Eval("BookingComments") %>'></cc1:ucLabel>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </cc1:ucGridView>
                    <cc1:ucGridView ID="gvExport" runat="server" AutoGenerateColumns="false" OnRowDataBound="gvVendor_RowDataBound" Style="display: none;">
                        <Columns>
                            <asp:TemplateField HeaderText="Date">
                                <HeaderStyle Width="100px" />
                                <ItemStyle HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <asp:HiddenField ID="hdnExpectedDeliveryDate" runat="server" Value='<%# Eval("DeliveryDate") %>' />
                                    <cc1:ucLabel ID="lblExpectedDeliveryDate" runat="server" Text='<%# Eval("DeliveryDate", "{0:dd/MM/yyyy}") %>'></cc1:ucLabel>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Time / Day" SortExpression="Weekday">
                                <HeaderStyle Width="150px" HorizontalAlign="Left" />
                                <ItemStyle HorizontalAlign="Left" />
                                <ItemTemplate>
                                    <asp:HiddenField ID="hdnTimeWindowID" runat="server" Value='<%# Eval("TimeWindowID") %>' />
                                    <cc1:ucLiteral ID="UcLiteral27_2" runat="server" Text='<%#Eval("NonWindowFromToTime") %>'
                                        Visible="false"></cc1:ucLiteral>
                                    <cc1:ucLiteral ID="UcLiteral27_1" runat="server" Text='<%#Eval("WindowStartEndTime") %>'
                                        Visible="false"></cc1:ucLiteral>
                                    <cc1:ucLiteral ID="UcLiteral27" runat="server" Text='<%#Eval("ExpectedDeliveryTime") %>'></cc1:ucLiteral>&nbsp;
                                    <cc1:ucLiteral ID="UcLiteral28" runat="server" Text='<%# Eval("DeliveryDate", "{0:dddd}")%>'></cc1:ucLiteral>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Site">
                                <HeaderStyle Width="120px" HorizontalAlign="Left" />
                                <ItemStyle HorizontalAlign="Left" />
                                <ItemTemplate>
                                    <cc1:ucLabel ID="LBLSiteName" runat="server" Text='<%# Eval("SiteName") %>'></cc1:ucLabel>
                                    <cc1:ucLiteral ID="ltHistory" Visible="false" runat="server"></cc1:ucLiteral>
                                    <asp:HyperLink ID="hypHistory" Visible="false" runat="server"></asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Booking Ref #" DataField="BookingRef" SortExpression="BookingRef">
                                <HeaderStyle Width="150px" HorizontalAlign="Left" />
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="Vendor Name">
                                <HeaderStyle Width="200px" HorizontalAlign="Left" />
                                <ItemStyle HorizontalAlign="Left" />
                                <ItemTemplate>
                                    <cc1:ucLiteral ID="UcLiteral2" runat="server" Text='<%#Eval("VendorName") %>'></cc1:ucLiteral>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Carrier Name">
                                <HeaderStyle Width="150px" HorizontalAlign="Left" />
                                <ItemStyle HorizontalAlign="Left" />
                                <ItemTemplate>
                                    <cc1:ucLiteral ID="UcLiteral3" runat="server" Text='<%#Eval("CarrierName") %>'></cc1:ucLiteral>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Status" DataField="BookingStatus" SortExpression="BookingStatus">
                                <HeaderStyle Width="120px" HorizontalAlign="Left" />
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField HeaderText="ActualPallets" DataField="Pallets" SortExpression="Pallets">
                                <HeaderStyle Width="50px" HorizontalAlign="Left" />
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:BoundField HeaderText="ActualCartons" DataField="Cartons" SortExpression="Cartons">
                                <HeaderStyle Width="50px" HorizontalAlign="Left" />
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:BoundField HeaderText="ActualLines" DataField="Lines" SortExpression="Lines">
                                <HeaderStyle Width="50px" HorizontalAlign="Left" />
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="VehicleType">
                                <HeaderStyle Width="120px" />
                                <ItemStyle HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <cc1:ucLabel ID="LBLVehicleType" runat="server" Text='<%# Eval("VehicleType") %>'></cc1:ucLabel>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Additional Info">
                                <HeaderStyle Width="120px" />
                                <ItemStyle HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <cc1:ucLabel ID="LBLAdditionalInfo" runat="server" Text='<%# Eval("BookingComments") %>'></cc1:ucLabel>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </cc1:ucGridView>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>

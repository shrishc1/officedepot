﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="APPCNT_AutoEmailPOToVendorEdit.aspx.cs" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
 Inherits="ModuleUI_Appointment_CountrySetting_APPCNT_AutoEmailPOToVendorEdit" %>

 
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2>
        <cc1:ucLabel ID="lblAutoEmailNewPOToVendor" runat="server"></cc1:ucLabel>
    </h2>
     <div class="right-shadow">
        
        <div class="formbox">
            <table width="60%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                <tr>
                    
                    <td style="font-weight: bold;" width="42%">
                        <cc1:ucLabel ID="lblCountry" runat="server" Text="Country" isRequired="true"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold;" width="1%" align="center">
                        :
                    </td>
                    <td style="font-weight: bold;" width="55%">
                        <cc1:ucLabel ID="ltCountry" runat="server"></cc1:ucLabel>
                    </td>
                   
                </tr>
                <tr>
                   
                    <td style="font-weight: bold">
                        <cc1:ucLabel ID="lblAutomaticallyMailNewPOToVendor" runat="server"  isRequired="true"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold;" align="center">
                        :
                    </td>
                    <td id="CheckStatus" runat="server">
                       <cc1:ucRadioButton ID="rdoYes" runat="server" GroupName="StatusSelect"  /> &nbsp;&nbsp;&nbsp;
                       <cc1:ucRadioButton ID="rdoNo" runat="server" Checked="true" GroupName="StatusSelect" />
                    </td>
                   
                </tr>
            </table>
        </div>
    </div>
     <div class="bottom-shadow">
    </div>
    <div class="button-row">
        <cc1:ucButton ID="btnSave" runat="server" Text="Save" CssClass="button" OnClick="btnSave_Click" />
        <cc1:ucButton ID="btnBack" runat="server" Text="Back" CssClass="button" OnClick="btnBack_Click" />
    </div>
    </asp:Content>

﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using BaseControlLibrary;

using Utilities;
using WebUtilities;
using BusinessEntities.ModuleBE.Appointment.CountrySetting;
using BusinessLogicLayer.ModuleBAL.Appointment.CountrySetting;
using BusinessEntities.ModuleBE.AdminFunctions;
using BusinessLogicLayer.ModuleBAL.AdminFunctions;

public partial class APPCNT_ISPM15CountrySetupEdit : CommonPage
{

    protected string deleteMessage = WebCommon.getGlobalResourceValue("DeleteMessage");

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            editPanel.Visible = true;
            MAS_CountryBE oMAS_CountryBE = new MAS_CountryBE();
            MAS_CountryBAL oMAS_CountryBAL = new MAS_CountryBAL();

            if (GetQueryStringValue("CountryID") != null)
            {
                txtCountryName.Text = GetQueryStringValue("CountryName");
            }
        }
    }

    #region Methods



    #endregion
    #region Events
    protected void btnSave_Click(object sender, EventArgs e)
    {
        MAS_CountryBE oMAS_CountryBE = new MAS_CountryBE();
        MAS_CountryBAL oMAS_CountryBAL = new MAS_CountryBAL();

        if (GetQueryStringValue("CountryID") == null)
        {
            oMAS_CountryBE.Action = "GetISPM15Country";
            oMAS_CountryBE.CountryName = txtCountryName.Text.Trim();
            oMAS_CountryBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
            oMAS_CountryBE.User.UserID = Convert.ToInt32(Session["UserID"].ToString());
            oMAS_CountryBE.IsActive = true;
            var lstCarriers = oMAS_CountryBAL.GetCountryISPM15BAL(oMAS_CountryBE);

            if (lstCarriers.Count > 0)
            {
                string errorMessage = WebCommon.getGlobalResourceValue("CountrysExists");
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Message", "alert('" + errorMessage + "')", true);
            }
            else
            {
                if (!string.IsNullOrEmpty(txtCountryName.Text))
                {

                    oMAS_CountryBE.Action = "InsertISPM15Country";
                    oMAS_CountryBE.CountryName = txtCountryName.Text.Trim();
                    oMAS_CountryBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
                    oMAS_CountryBE.User.UserID = Convert.ToInt32(Session["UserID"].ToString());
                    int result = oMAS_CountryBAL.InsertCountryISPM15BAL(oMAS_CountryBE);

                }
                EncryptQueryString("APPCNT_ISPM15CountrySetup.aspx");
            }
        }
        else
        {
            if (GetQueryStringValue("CountryID") != null)
            {
                if (!string.IsNullOrEmpty(txtCountryName.Text))
                {
                    oMAS_CountryBE.CountryName = txtCountryName.Text.Trim();
                    oMAS_CountryBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
                    oMAS_CountryBE.User.UserID = Convert.ToInt32(Session["UserID"].ToString());
                    oMAS_CountryBE.Action = "EditISPM15Country";
                    oMAS_CountryBE.CountryName = txtCountryName.Text.Trim();
                    oMAS_CountryBE.IsActive = true;
                    oMAS_CountryBE.CountryID = Convert.ToInt32(GetQueryStringValue("CountryID"));
                    int result = oMAS_CountryBAL.InsertCountryISPM15BAL(oMAS_CountryBE);
                }
                EncryptQueryString("APPCNT_ISPM15CountrySetup.aspx");
            }
        }
    }

    protected void btnDelete_Click(object sender, EventArgs e)
    {
        if (GetQueryStringValue("CountryID") != null)
        {
            MAS_CountryBE oMAS_CountryBE = new MAS_CountryBE();
            MAS_CountryBAL oMAS_CountryBAL = new MAS_CountryBAL();
            oMAS_CountryBE.Action = "EditISPM15Country"; 
            oMAS_CountryBE.IsActive = false;
            oMAS_CountryBE.CountryID = Convert.ToInt32(GetQueryStringValue("CountryID"));
            oMAS_CountryBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
            oMAS_CountryBE.User.UserID = Convert.ToInt32(Session["UserID"].ToString());
            var result = oMAS_CountryBAL.InsertCountryISPM15BAL(oMAS_CountryBE);

            if(result==2)
            {
                string errorMessage = WebCommon.getGlobalResourceValue("CountryDeleted");
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Message", "alert('" + errorMessage + "')", true);
            }
            else
            {

            }
        }

        EncryptQueryString("APPCNT_ISPM15CountrySetup.aspx");
    }
    protected void btnBack_Click(object sender, EventArgs e)
    {
        EncryptQueryString("APPCNT_ISPM15CountrySetup.aspx");
    }
    #endregion
}



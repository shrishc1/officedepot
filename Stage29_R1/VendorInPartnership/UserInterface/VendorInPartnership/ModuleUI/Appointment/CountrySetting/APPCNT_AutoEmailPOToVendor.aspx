﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="APPCNT_AutoEmailPOToVendor.aspx.cs"
    MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master" Inherits="ModuleUI_Appointment_CountrySetting_APPCNT_AutoEmailPOToVendor" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="../../../CommonUI/UserControls/ucExportToExcel.ascx" TagName="ucExportToExcel"
    TagPrefix="cc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
  
    <h2>
        <cc1:ucLabel ID="lblAutoEmailNewPOToVendor" runat="server"></cc1:ucLabel>
    </h2>
      <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
   <Style type="text/css">
   .row1{
   color:#333333;background-color:#F7F6F3;
   }
   .row2
   {color:#284775;background-color:#FFFFFF;      
   }
   </Style>
   
    <div class="formbox">
        <table width="79%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
            <tr>
                <td style="width: 5%">
                </td>
                <td style="font-weight: bold;" width="28%">
                    <cc1:ucLabel ID="lblCountry" runat="server" Text="Country" isRequired="true"></cc1:ucLabel>
                </td>
                <td style="font-weight: bold;" width="5%" align="center">
                    :
                </td>
                <td style="font-weight: bold;" width="57%">
                    <cc1:ucDropdownList ID="ddlCountry" runat="server" Width="150px" AutoPostBack="true"
                        OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged">
                    </cc1:ucDropdownList>
                    <cc1:ucLabel ID="lblEditCountry" runat="server" Text=""></cc1:ucLabel>
                </td>
                <td style="width: 5%">
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td style="font-weight: bold">
                    <cc1:ucLabel ID="lblVendor" runat="server" Text="Vendor" isRequired="true"></cc1:ucLabel>
                </td>
                <td style="font-weight: bold;" align="center">
                    :
                </td>
                <td>                    
                    <cc1:ucTextbox ID="txtVendorNo" runat="server" MaxLength="15" Width="90px"></cc1:ucTextbox>&nbsp;
                    <cc1:ucButton CssClass="button" runat="server" ID="btnSearchVendor" OnClick="btnSearch_Click" />&nbsp;
                    &nbsp;<cc1:ucButton CssClass="button" runat="server" ID="btnSearchVendorNumber" OnClick="btnSearchByVendorNo_Click" />
                    <div id="pnlVendor" runat="server">
                        <table>
                            <tr>
                                <td>
                                    <cc1:ucListBox ID="lstLeft" runat="server" Height="150px" Width="320px">
                                    </cc1:ucListBox>
                                </td>
                                <td align="center">
                                <div>
                                        <input type="button" id="Button1" value=">>" class="button" style="width: 35px"
                                            onclick="Javascript:MoveAll('<%= lstLeft.ClientID %>', '<%=lstRight.ClientID %>','<%= hiddenSelectedIDs.ClientID %>','<%= hiddenSelectedName.ClientID %>');" />
                                    </div> &nbsp;
                                    <div>
                                        <input type="button" id="btnMoveRight" value=">" class="button" style="width: 35px"
                                            onclick="Javascript:MoveItem('<%=lstLeft.ClientID %>', '<%= lstRight.ClientID %>','<%= hiddenSelectedIDs.ClientID %>','<%= hiddenSelectedName.ClientID %>');" />
                                    </div>
                                    &nbsp;
                                    <div>
                                        <input type="button" id="btnMoveLeft" value="<" class="button" style="width: 35px"
                                            onclick="Javascript:MoveItem('<%= lstRight.ClientID %>', '<%=lstLeft.ClientID %>','<%= hiddenSelectedIDs.ClientID %>','<%= hiddenSelectedName.ClientID %>');" />
                                    </div>
                                    &nbsp;
                                    <div>
                                        <input type="button" id="btnMoveLeftAll" value="<<" class="button" style="width: 35px"
                                            onclick="Javascript:MoveAll('<%= lstRight.ClientID %>', '<%=lstLeft.ClientID %>','<%= hiddenSelectedIDs.ClientID %>','<%= hiddenSelectedName.ClientID %>');" />
                                    </div>
                                </td>
                                <td>
                                    <cc1:ucListBox ID="lstRight" runat="server" Height="150px" Width="320px">
                                    </cc1:ucListBox>
                                    <asp:HiddenField ID="hiddenSelectedIDs" runat="server" />
                                    <asp:HiddenField ID="hiddenSelectedName" runat="server" />
                                </td>
                            </tr>
                        </table>
                    </div>
                    <%--  <cc1:MultiSelectVendor runat="server" ID="msVendor" />--%>
                </td>
            </tr>
              <tr>
                <td colspan="8" align="center">
                    <cc1:ucRadioButtonList ID="rblStatus" runat="server" RepeatDirection="Horizontal">
                        <asp:ListItem Selected="True" Text="AllOnly" Value="0"></asp:ListItem>
                        <asp:ListItem Text=" Immediate" Value="1"></asp:ListItem>
                        <asp:ListItem Text=" Evening" Value="2"></asp:ListItem>
                        <asp:ListItem Text=" Do not mail" Value="3"></asp:ListItem>
                    </cc1:ucRadioButtonList>
                </td>
            </tr>
            <tr>               
                <td colspan="8">
                 <div class="button-row">
                    <cc1:ucButton ID="btnShow" runat="server" CssClass="button" 
                         onclick="btnShow_Click"/>
                    </div>
                </td>
            </tr>
        </table>
    </div>
     <asp:UpdatePanel ID="UpdatePanel1" runat="server"  EnablePartialRendering="false">
        <ContentTemplate>
          <script type="text/javascript">
              var gridCheckedCount = 0;
              $(document).ready(function () {
                  $('[id$=chkSelectAllText]').click(function () {
                      $("[id$='chkSelect']").attr('checked', this.checked);
                      gridCheckedCount = $('[id$=chkSelect]:checked').length;
                  });
              });
              function SelectAllGrid(id) {
                  // var ID=(id.id);
                  if ($(id).attr('checked', this.checked)) {
                      //debugger;
                      $("[id$='chkSelect']").attr('checked', true);
                      gridCheckedCount = $('[id$=chkSelect]:checked').length;
                  }
                  else {
                      $("[id$='chkSelect']").attr('checked', false);
                      gridCheckedCount = $('[id$=chkSelect]:checked').length;
                  }
              }
              function CheckUncheckAllCheckBoxAsNeeded(input) {
                  if ($('[id$=chkSelect]:checked').length == $('[id$=chkSelect]').length) {
                      $("[id$=chkSelectAllText]").attr('checked', true);
                      gridCheckedCount = $('[id$=chkSelect]:checked').length;
                  }
                  if (!$(input).is(":checked")) {
                      $("[id$=chkSelectAllText]").attr('checked', false);
                      gridCheckedCount = $('[id$=chkSelect]:checked').length;
                  }

                  if ($(input).is(":checked")) {
                      gridCheckedCount = $('[id$=chkSelect]:checked').length;
                  }
              }
              function CheckAtleastOneCheckBox() {
                  //debugger;
                  var selectCheckboxToAccept = '<%=selectCheckboxToAccept%>';
                  if (gridCheckedCount == 0) {
                      alert(selectCheckboxToAccept);
                      return false
                  }
                  else {
                      gridCheckedCount = 0;
                  }
              }
          </script>
    <div class="button-row">
        <cc1:ucButton ID="ucExportToExcel1" runat="server" CssClass="exporttoexcel" Text="Export To Excel" OnClick="btnExport_Click"
                        Width="109px" Height="20px" Visible="false" />       
    </div> 
    <asp:HiddenField ID="hdnGridCurrentPageNo" runat="server" />
    <asp:HiddenField ID="hdnGridPageSize" runat="server" />
    <table width="100%">
        <tr>
            <td align="center">
                <asp:GridView ID="grdAutoEmailPODetails" Width="100%" runat="server" AutoGenerateColumns="false" CssClass="grid" 
                    OnRowDataBound="grdAutoEmailPODetails_RowDataBound" >
                     <RowStyle CssClass="row1" />
                    <AlternatingRowStyle CssClass="row2" />
                    <Columns>
                        <asp:TemplateField HeaderText="SelectAllText">
                            <HeaderStyle HorizontalAlign="Left" Width="5%" />
                            <ItemStyle Width="20px" HorizontalAlign="Left" VerticalAlign="Middle" />
                            <HeaderTemplate>
                                <asp:CheckBox  runat="server" ID="chkSelectAllText" CssClass="checkbox-input" onclick="SelectAllGrid(this);"/>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:CheckBox runat="server" ViewStateMode="Enabled" ID="chkSelect" onclick="CheckUncheckAllCheckBoxAsNeeded(this);" />
                                <cc1:ucLiteral ID="ltVendorID" Visible="false" runat="server" Text='<%# Eval("UP_VendorBE.VendorID") %>'></cc1:ucLiteral>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Country" >
                            <HeaderStyle HorizontalAlign="Left" Width="10%" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <%-- <asp:HyperLink ID="hpcountry" runat="server" Text='<%# Eval("Country.CountryName") %>'
                                NavigateUrl='<%# EncryptQuery("APPCNT_AutoEmailPOToVendorEdit.aspx?CountryID="+ Eval("Country.CountryID") + "&CountryName=" + Eval("Country.CountryName") + "&IsAutoEmailNewPO=" + Eval("IsAutoEmailNewPO")) %>'></asp:HyperLink>--%>
                                 <asp:HiddenField ID="hdnCountryID" runat="server" Value='<%# Eval("Country.CountryID") %>'></asp:HiddenField>
                                <cc1:ucLiteral ID="ltCountry" runat="server" Text='<%# Eval("Country.CountryName") %>'></cc1:ucLiteral>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Vendor Number" >
                            <HeaderStyle Width="10%" HorizontalAlign="Left" />
                            <ItemTemplate>
                                <cc1:ucLiteral ID="ltVendorNo" runat="server" Text='<%# Eval("UP_VendorBE.Vendor_No") %>'></cc1:ucLiteral>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Vendor Name" >
                            <HeaderStyle Width="10%" HorizontalAlign="Left" />
                            <ItemTemplate>
                                <cc1:ucLiteral ID="ltVendorName" runat="server" Text='<%# Eval("UP_VendorBE.VendorName") %>'></cc1:ucLiteral>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Email Status?">
                            <HeaderStyle Width="10%" HorizontalAlign="Left" />
                            <ItemTemplate>
                                <cc1:ucLiteral ID="ltAutoEmailNewPOs" runat="server" Text='<%# Eval("IsAutoEmailNewPO") %>'></cc1:ucLiteral>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>                
            </td>
        </tr>
        <tr><td colspan="4"><cc1:PagerV2_8 ID="pager1" runat="server" OnCommand="pager_Command" GenerateGoToSection="true">
                            </cc1:PagerV2_8></td></tr>
    </table>
     <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td align="center">
                        <asp:UpdateProgress runat="server" ID="PageUpdateProgress" AssociatedUpdatePanelID="UpdatePanel1">
                            <ProgressTemplate>
                                <div align="center" style="background-color: #000; top: 0px; left: 0px; bottom: 0px;
                                    right: 0px; padding-top: 20%; margin: 0; width: 100%; height: 50%; overflow: hidden;
                                    position: absolute; z-index: 1000; filter: alpha(opacity=50); opacity: 0.5;">
                                    <asp:Image ID="imgWait" runat="server" ImageUrl="~/Images/Ajaxloading.gif" ImageAlign="Middle" />
                                </div>
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                    </td>
                </tr>
            </table>
             <div class="button-row">
                 <cc1:ucButton ID="btnDoNotMail" runat="server" CssClass="button" 
                        onclick="btnDoNotAutoEmail_Click"   OnClientClick="return CheckAtleastOneCheckBox();"  />
                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                 <cc1:ucButton ID="btnSendImmediatelyNew" runat="server" CssClass="button" 
                        onclick="btnSendImmediately_Click"   OnClientClick="return CheckAtleastOneCheckBox();"  />
                 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                 <cc1:ucButton ID="btnEndofDay" runat="server" CssClass="button" 
                        onclick="btnAutoEmail_Click"   OnClientClick="return CheckAtleastOneCheckBox();"  />
             </div>
     </ContentTemplate>
     <Triggers>
     <asp:PostBackTrigger ControlID="btnShow" />
     <asp:PostBackTrigger ControlID="ucExportToExcel1" />     
     <asp:AsyncPostBackTrigger ControlID="btnEndofDay" />   
     <asp:AsyncPostBackTrigger ControlID="btnDoNotMail" />  
     <asp:AsyncPostBackTrigger ControlID="btnSendImmediatelyNew" />          
     <asp:AsyncPostBackTrigger ControlID="pager1" />   
     </Triggers>
    </asp:UpdatePanel>
</asp:Content>

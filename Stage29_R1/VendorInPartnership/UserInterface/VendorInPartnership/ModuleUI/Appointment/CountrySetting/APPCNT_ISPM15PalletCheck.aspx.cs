﻿using BusinessEntities.ModuleBE.Appointment.CountrySetting;
using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using BusinessEntities.ModuleBE.VendorScorecard;
using BusinessLogicLayer.ModuleBAL.Appointment.CountrySetting;
using BusinessLogicLayer.ModuleBAL.Appointment.SiteSettings;
using BusinessLogicLayer.ModuleBAL.VendorScorecard;
using System;
using System.Collections.Generic;
using System.Data;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebUtilities;
public partial class ModuleUI_Appointment_CountrySetting_APPCNT_ISPM15PalletCheck : CommonPage
{
    protected bool IsVendorByNameClicked = false;
    protected bool IsVendorByNumberClicked = false;
    bool IsExportClicked = false;
    protected string VendorPalletCheckingSuccessMesg = WebCommon.getGlobalResourceValue("VendorPalletCheckingSuccessMesg");
    protected string VendorPalletCheckingSuccessMesg2 = WebCommon.getGlobalResourceValue("VendorPalletCheckingSuccessMesg2");
    protected string VendorPalletCheckingAlreadyExists = WebCommon.getGlobalResourceValue("VendorPalletCheckingAlreadyExists");
    protected string DeleteMessage = WebCommon.getGlobalResourceValue("DeleteMessage");
    protected string ISPM15CountriesAlreadyExists = WebCommon.getGlobalResourceValue("ISPM15CountriesAlreadyExists");
    protected string SuccessMesg = WebCommon.getGlobalResourceValue("SuccessMesg");
    protected string DestinationAndSourceNotSame = WebCommon.getGlobalResourceValue("DestinationAndSourceNotSame");
    
    public List<ISPM15PalletCheckingBE> ListISPM15PalletCheckingBE
    {
        get
        {
            return (List<ISPM15PalletCheckingBE>)ViewState["ListCountries"];
        }
        set
        {
            ViewState["ListCountries"] = value;
        }
    }
    public int? ID
    {
        get
        {
            return (int)ViewState["ID"];
        }
        set
        {
            ViewState["ID"] = value;
        }
    }
    protected void Page_Init(object sender, EventArgs e)
    {
        ddlSourceCountry.CurrentPage = this;
        ddlSourceCountry.IsAllRequired = false;
        ddlDestinationCountry.CurrentPage = this;
        ddlDestinationCountry.IsAllRequired = false;
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            pager1.PageSize = 200;
            pager1.GenerateGoToSection = false;
            pager1.GeneratePagerInfoSection = false;
            pager1.Visible = false;
            BindGridView();

            if (!string.IsNullOrEmpty(GetQueryStringValue("value")))
            {
                trVendorPalletChecking.Visible = true;
                trVendorPalletCheckingButton.Visible = true;
                btnAdd.Visible = false;
                btnExportToExcel.Visible = false;
                grdVendorData.Visible = false;
                pager.Visible = false;
                btnDelete.Visible = false;
            }
            else
            {
                trVendorPalletChecking.Visible = false;
                trVendorPalletCheckingButton.Visible = false;
                btnAdd.Visible = true;
                btnExportToExcel.Visible = true;
                grdVendorData.Visible = true;
                pager.Visible = true;
                btnDelete.Visible = true;
            }
        }
    }
    protected void BtnSave_Click(object sender, EventArgs e)
    {
        if (ddlSourceCountry.innerControlddlCountry.SelectedValue != ddlDestinationCountry.innerControlddlCountry.SelectedValue)
        {
            ISPM15PalletCheckingBE objISPM15 = new ISPM15PalletCheckingBE();
            ISPM15PalletCheckingBAL objISPM15BAL = new ISPM15PalletCheckingBAL();
            objISPM15.Action = btnSave.Text != WebCommon.getGlobalResourceValue("Update") ? "InsertISPM15" : "UpdateISPM15";
            objISPM15.SourceCountryID = Convert.ToInt32(ddlSourceCountry.innerControlddlCountry.SelectedValue);
            objISPM15.DestinationCountryID = Convert.ToInt32(ddlDestinationCountry.innerControlddlCountry.SelectedValue);
            objISPM15.ISPM15Check = chkPalletChecking.SelectedValue == "1" ? true : false;
            objISPM15.UserID = Convert.ToInt32(Session["UserID"]);

            if (btnSave.Text == WebCommon.getGlobalResourceValue("Update"))
                objISPM15.ID = ID.Value;

            var result = objISPM15BAL.SaveISPM15PalletCheckingBAL(objISPM15);

            if (result == 2 || result == 1)
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('" + SuccessMesg + "')", true);

                EncryptQueryString("APPCNT_ISPM15PalletCheck.aspx");
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('" + ISPM15CountriesAlreadyExists + "')", true);
            }
        }
        else
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('" + DestinationAndSourceNotSame + "')", true);
        }
    }
    protected void btnRemove_Click(object sender, EventArgs e)
    {
        UP_VendorBE oUP_VendorBE = new UP_VendorBE();
        UP_VendorBAL oUP_VendorBAL = new UP_VendorBAL();

        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('" + VendorPalletCheckingSuccessMesg + "')", true);
        BindGridView();
    }
    protected void grdVendorPalletChecking_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if ((e.Row.RowType == DataControlRowType.Header || e.Row.RowType == DataControlRowType.DataRow) && IsExportClicked)
        {
            e.Row.Cells[6].Visible = false;
        }
    }
    protected void btnBack_Click(object sender, EventArgs e)
    {
        EncryptQueryString("APPCNT_ISPM15PalletCheck.aspx");
    }
    public void BindGridView(int Page = 1)
    {
        ISPM15PalletCheckingBE objISPM15 = new ISPM15PalletCheckingBE();
        ISPM15PalletCheckingBAL objISPM15BAL = new ISPM15PalletCheckingBAL();
        objISPM15.Action = "GetAll";

        if (IsExportClicked)
        {
            objISPM15.GridCurrentPageNo = 0;
            objISPM15.GridPageSize = 0;
        }
        else
        {
            objISPM15.GridCurrentPageNo = Page;
            objISPM15.GridPageSize = 200;
        }

        List<ISPM15PalletCheckingBE> lstISPM15PalletCheckingBE = objISPM15BAL.GetISPM15PalletCheckingDetailsBAL(objISPM15);
        ListISPM15PalletCheckingBE = lstISPM15PalletCheckingBE;

        if (ListISPM15PalletCheckingBE != null && ListISPM15PalletCheckingBE.Count > 0)
        {
            pager1.Visible = true;
            pager1.ItemCount = ListISPM15PalletCheckingBE[0].TotalRecords;
            grdVendorPalletChecking.DataSource = ListISPM15PalletCheckingBE;
            grdVendorPalletChecking.DataBind();
            btnExportToExcel.Visible = true;
        }
        else
        {
            grdVendorPalletChecking.DataSource = null;
            grdVendorPalletChecking.DataBind();
            btnExportToExcel.Visible = false;
        }
    }
    public void pager_Command(object sender, CommandEventArgs e)
    {
        int currnetPageIndx = Convert.ToInt32(e.CommandArgument);
        pager1.CurrentIndex = currnetPageIndx;
        BindGridView(currnetPageIndx);
    }
    protected void btnExport_Click(object sender, EventArgs e)
    {
        IsExportClicked = true;
        BindGridView();
        WebCommon.ExportHideHidden("CountryPalletChecking", grdVendorPalletChecking);
    }
    protected void lnkRemove_Click(object sender, EventArgs e)
    {
        LinkButton btn = (LinkButton)(sender);
        ID = Convert.ToInt32(btn.CommandArgument);
        hdnSeletedVendorID.Value = ID.ToString();
        trVendorPalletChecking.Visible = true;
        trVendorPalletCheckingButton.Visible = true;
        btnAdd.Visible = false;
        btnExportToExcel.Visible = false;
        grdVendorData.Visible = false;
        pager.Visible = false;
        btnSave.CausesValidation = false;
        GridViewRow grdrow = (GridViewRow)((LinkButton)sender).NamingContainer;

        var chkVendorPallet = (CheckBox)grdrow.FindControl("chkVendorPallet");

        HiddenField hdnSourceCountryID = (HiddenField)grdrow.FindControl("hdnSourceCountryID");
        HiddenField hdnDestinationCountryID = (HiddenField)grdrow.FindControl("hdnDestinationCountryID");

        ddlSourceCountry.innerControlddlCountry.SelectedValue = hdnSourceCountryID.Value;
        ddlDestinationCountry.innerControlddlCountry.SelectedValue = hdnDestinationCountryID.Value;
        chkPalletChecking.SelectedValue = chkVendorPallet.Checked ? "1" : "2";
        btnSave.Text = WebCommon.getGlobalResourceValue("Update");
    }
    protected void btnDelete_Click(object sender, EventArgs e)
    {
        ISPM15PalletCheckingBE objISPM15 = new ISPM15PalletCheckingBE();
        ISPM15PalletCheckingBAL objISPM15BAL = new ISPM15PalletCheckingBAL();
        objISPM15.Action = "DeleteISPM15";
        objISPM15.UserID = Convert.ToInt32(Session["UserID"]);
        objISPM15.ID = ID.Value;
        var result = objISPM15BAL.SaveISPM15PalletCheckingBAL(objISPM15);
        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('" + VendorPalletCheckingSuccessMesg + "')", true);
        EncryptQueryString("APPCNT_ISPM15PalletCheck.aspx");
    }
}
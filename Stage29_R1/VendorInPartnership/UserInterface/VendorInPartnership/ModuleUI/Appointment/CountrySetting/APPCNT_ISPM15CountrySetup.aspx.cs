﻿using BusinessEntities.ModuleBE.AdminFunctions;
using BusinessLogicLayer.ModuleBAL.AdminFunctions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebUtilities;

public partial class ModuleUI_Appointment_CountrySetting_APPCNT_ISPM15CountrySetup : CommonPage
{
    bool IsExportClicked = false;
    protected void Page_Load(object sender, EventArgs e)
    {
        GetISPM15Country();         
    }
    protected void btnExport_Click(object sender, EventArgs e)
    {
        IsExportClicked = true;
        GetISPM15Country();
        WebCommon.ExportHideHidden("ISPM15Countries", grdCountry);
    }
    private void GetISPM15Country()
    {
        MAS_CountryBE oMAS_CountryBE = new MAS_CountryBE();
        MAS_CountryBAL oMAS_CountryBAL = new MAS_CountryBAL();
        oMAS_CountryBE.Action = "GetISPM15Country";
        oMAS_CountryBE.CountryName = "";
        oMAS_CountryBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
        oMAS_CountryBE.User.UserID = Convert.ToInt32(Session["UserID"].ToString());
        var lstCarriers = oMAS_CountryBAL.GetCountryISPM15BAL(oMAS_CountryBE);
        grdCountry.DataSource = lstCarriers;
        grdCountry.DataBind();

        btnExportToExcel.Visible = lstCarriers.Count > 0;
    }
}
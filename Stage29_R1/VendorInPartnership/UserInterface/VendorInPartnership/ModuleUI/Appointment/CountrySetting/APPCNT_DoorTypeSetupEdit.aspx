﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    CodeFile="APPCNT_DoorTypeSetupEdit.aspx.cs" Inherits="APPCNT_DoorTypeSetupEdit" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/ucCountry.ascx" TagName="ucCountry" TagPrefix="cc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<script language="javascript" type="text/javascript">
    function confirmDelete() {
        return confirm('<%=deleteMessage%>');
    }
</script>
    <h2>
        <cc1:ucLabel ID="lblDoorTypeSetup" runat="server" Text="Door Type Setup"></cc1:ucLabel>
    </h2>
    <div class="right-shadow">
        <div>
            <asp:RequiredFieldValidator ID="rfvDoorTypeRequired" runat="server" ControlToValidate="txtDoorType"
                Display="None" ValidationGroup="a" SetFocusOnError="true">
            </asp:RequiredFieldValidator>
            <asp:ValidationSummary ID="vSummary" runat="server" ShowMessageBox="true" ShowSummary="false"
                Style="color: Red" ValidationGroup="a" />
        </div>
        <div class="formbox" >
            <table width="40%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                <tr>
                    <td style="width:5%"></td>
                    <td style="font-weight: bold; width: 20%">
                        <cc1:ucLabel ID="lblCountry" runat="server" Text="Country" isRequired="true"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold; width: 5%" align="center">
                        :
                    </td>
                    <td style="font-weight: bold; width: 70%">
                        <cc2:ucCountry ID="ddlCountry" runat="server" />
                    </td>
                    
                </tr>
                <tr>
                    <td></td>
                    <td style="font-weight: bold">
                        <cc1:ucLabel ID="lblDoorType" runat="server" Text="Door Type" isRequired="true"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold" align="center">
                        :
                    </td>
                    <td>
                        <cc1:ucTextbox ID="txtDoorType" runat="server" Width="220px" MaxLength="30"></cc1:ucTextbox>
                    </td>
                    
                </tr>
            </table>
        </div>
    </div>
    <div class="bottom-shadow">
    </div>
    <div class="button-row">
        <cc1:ucButton ID="btnSave" runat="server" Text="Save" CssClass="button" OnClick="btnSave_Click"
            ValidationGroup="a" />
        <cc1:ucButton ID="btnDelete" runat="server" Text="Delete" CssClass="button" OnClick="btnDelete_Click" OnClientClick="return confirmDelete();"  />
        <cc1:ucButton ID="btnBack" runat="server" Text="Back" CssClass="button" OnClick="btnBack_Click" />
    </div>
</asp:Content>

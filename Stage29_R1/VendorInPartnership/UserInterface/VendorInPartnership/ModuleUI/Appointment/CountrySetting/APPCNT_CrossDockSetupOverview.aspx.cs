﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using BusinessEntities.ModuleBE.Appointment.CountrySetting;
using BusinessLogicLayer.ModuleBAL.Appointment.CountrySetting;
using System.Data;
using BaseControlLibrary;

public partial class APPCNT_CrossDockSetupOverview : CommonPage 
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
            BindGrid();

        ucExportToExcel1.GridViewControl = UcGridView1;
        ucExportToExcel1.FileName = "CrossDockSetupOverview";
    }

    #region Methods

    protected void BindGrid()
    {
        MASCNT_CrossDockBE oMASCNT_CrossDockBE = new MASCNT_CrossDockBE();
        APPCNT_CrossDockBAL oMASCNT_CrossDockBAL = new APPCNT_CrossDockBAL();

        oMASCNT_CrossDockBE.Action = "ShowAll";
        oMASCNT_CrossDockBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
        oMASCNT_CrossDockBE.User.UserID = Convert.ToInt32(Session["UserID"]);
        List<MASCNT_CrossDockBE> lstCrossDock = oMASCNT_CrossDockBAL.GetCrossDockDetailsBAL(oMASCNT_CrossDockBE);

        if (lstCrossDock.Count > 0) {
            UcGridView1.DataSource = lstCrossDock;
            UcGridView1.DataBind();
            ViewState["lstSites"] = lstCrossDock;
        }
    }

    public override BusinessEntities.BaseBe[] SubSort(GridViewSortEventArgs e)
    {
        return  Utilities.GenericListHelper<MASCNT_CrossDockBE>.SortList((List<MASCNT_CrossDockBE>)ViewState["lstSites"], e.SortExpression, e.SortDirection).ToArray();
    }

    #endregion
}
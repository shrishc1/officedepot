﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    CodeFile="APPCNT_CrossDockSetupEdit.aspx.cs" Inherits="APPCNT_CrossDockSetupEdit" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="../../../CommonUI/UserControls/ucCountry.ascx" TagName="ucCountry"
    TagPrefix="uc1" %>
<%@ Register Src="../../../CommonUI/UserControls/ucSite.ascx" TagName="ucSite"
    TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<script language="javascript" type="text/javascript">
    function confirmDelete() {
        return confirm('<%=deleteMessage%>');
    }
</script>
    <h2 style="font-size: 15px;">
        <cc1:ucLabel ID="lblCrossDock" runat="server" Text="Cross Docking Setup"></cc1:ucLabel>
    </h2>
    <div class="right-shadow">
        <div class="formbox">
            <table width="35%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                <tr>
                    <td style="width:5%"></td>
                    <td style="font-weight: bold; width: 35%">
                        <cc1:ucLabel ID="lblCountry" runat="server" Text="Country" isRequired="true"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold; width: 5%">:</td>
                    <td style="font-weight: bold; width: 50%">
                        <uc1:ucCountry ID="ucCountry" runat="server" />
                    </td>
                    <td style="width:5%"></td>
                </tr>
                <tr>
                    <td></td>
                    <td style="font-weight: bold;">
                        <cc1:ucLabel ID="lblSitePrefixName" runat="server" Text="Site" isRequired="true"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold;">:</td>
                    <td style="font-weight: bold;">
                        <uc2:ucSite ID="ucSite" runat="server" />
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td></td>
                    <td style="font-weight: bold;">
                        <cc1:ucLabel ID="lblDestinationSitePrefixName" runat="server" Text="Destination Site" isRequired="true"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold;">:</td>
                    <td colspan="2">
                        <uc2:ucSite ID="ucDestinationSite" runat="server" />
                    </td>
                    <td></td>
                </tr>
            </table>
        </div>
    </div>
    <div class="bottom-shadow">
    </div>
    <div class="button-row">
        <cc1:ucButton ID="btnSave" runat="server" Text="Save" CssClass="button" OnClick="btnSave_Click" />
        <cc1:ucButton ID="btnDelete" runat="server" Text="Delete" CssClass="button" OnClick="btnDelete_Click" OnClientClick="return confirmDelete();"  />
        <cc1:ucButton ID="btnBack" runat="server" Text="Back" CssClass="button" OnClick="btnBack_Click" />
    </div>
</asp:Content>

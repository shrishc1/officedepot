﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.Appointment.CountrySetting;
using BusinessLogicLayer.ModuleBAL.Appointment.CountrySetting;
using BusinessEntities.ModuleBE.VendorScorecard;
using BusinessEntities.ModuleBE.AdminFunctions;
using BusinessLogicLayer.ModuleBAL.VendorScorecard;
using BusinessLogicLayer.ModuleBAL.AdminFunctions;
using WebUtilities;
using BaseControlLibrary;
using System.Configuration;

public partial class ModuleUI_Appointment_CountrySetting_APPCNT_AutoEmailPOToVendor : CommonPage
{
    protected bool IsVendorByNameClicked = false;
    protected bool IsVendorByNumberClicked = false;
    protected string AutoEmailPoError = WebCommon.getGlobalResourceValue("AutoEmailPoError");
    protected string selectCheckboxToAccept = WebCommon.getGlobalResourceValue("SelectCheckboxToAction");
    bool IsExportClicked = false;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            pager1.PageSize = 200;
            ucExportToExcel1.Visible = false;
            pager1.GenerateGoToSection = false;
            pager1.GeneratePagerInfoSection = false;
            pager1.Visible = false;
            btnDoNotMail.Visible = false;
            btnEndofDay.Visible = false;
            btnSendImmediatelyNew.Visible = false;
            BindCountry();
            //BindAutoEmailNewPoDetails();
        }

    }

    protected void BindAutoEmailNewPoDetails(int Page = 1)
    {
        MASCNT_AutoEmailBE AutoEmailBE = new MASCNT_AutoEmailBE();
        APPCNT_AutoEmailBAL AutoEmailBAL = new APPCNT_AutoEmailBAL();
        AutoEmailBE.UP_VendorBE = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();
        AutoEmailBE.Action = "ShowAll";
        AutoEmailBE.UP_VendorBE.CountryID = Convert.ToInt32(ddlCountry.SelectedValue);
        AutoEmailBE.UP_VendorBE.VendorIDs = SelectedVendorIDs;
        AutoEmailBE.EmailStatus = Convert.ToInt32(rblStatus.SelectedValue);
        if (IsExportClicked)
        {
            AutoEmailBE.GridCurrentPageNo = 0;
            AutoEmailBE.GridPageSize = 0;
        }
        else
        {
            AutoEmailBE.GridCurrentPageNo = Page;
            AutoEmailBE.GridPageSize = 200;
        }
        List<MASCNT_AutoEmailBE> lstAutoEmailBE = AutoEmailBAL.GetAutoEmailPODetailsBAL(AutoEmailBE);

        if (lstAutoEmailBE != null && lstAutoEmailBE.Count > 0)
        {
            pager1.Visible = true;
            btnEndofDay.Visible = true;
            btnSendImmediatelyNew.Visible = true;
            btnDoNotMail.Visible = true;
            ucExportToExcel1.Visible = true;
            pager1.ItemCount = lstAutoEmailBE[0].TotalRecord;
            ViewState["TotalRecord"] = lstAutoEmailBE[0].TotalRecord;
            pager1.CurrentIndex = Page;
            grdAutoEmailPODetails.DataSource = lstAutoEmailBE;
            grdAutoEmailPODetails.DataBind();
            ViewState["lstAutoEmailBE"] = lstAutoEmailBE;
        }
        else
        {
            grdAutoEmailPODetails.DataSource = null;
            grdAutoEmailPODetails.DataBind();
            pager1.Visible = false;
            btnEndofDay.Visible = false;
            btnDoNotMail.Visible = false;
            btnSendImmediatelyNew.Visible = false;
            ucExportToExcel1.Visible = false;
        }
        ViewState["hiddenSelectedIDs"] = hiddenSelectedIDs.Value;
        ViewState["hiddenSelectedName"] = hiddenSelectedName.Value;

        if (lstAutoEmailBE.Count > 0)
        {
            if (ConfigurationManager.AppSettings.AllKeys.Any(key => key == "EmailSendVendorAutoPO"))
            {
                var countries = (ConfigurationManager.AppSettings["EmailSendVendorAutoPO"]);
                var countryArray = countries.Split(',');

                bool isVisible = (countryArray.Contains(ddlCountry.SelectedValue) && countryArray.Contains(lstAutoEmailBE[0].Country.CountryID.ToString()));

                btnSendImmediatelyNew.Visible = !isVisible;
                btnEndofDay.Visible = !isVisible;
                btnDoNotMail.Visible = !isVisible;
            }
        }
    }

    //public override BusinessEntities.BaseBe[] SubSort(GridViewSortEventArgs e)
    //{
    //    return Utilities.GenericListHelper<MASCNT_AutoEmailBE>.SortList((List<MASCNT_AutoEmailBE>)ViewState["lstAutoEmailBE"], e.SortExpression, e.SortDirection).ToArray();
    //}

    protected void ddlCountry_SelectedIndexChanged(object sender, EventArgs e)
    {
        // BindVendor();
        txtVendorNo.Text = "";
        pager1.ItemCount = Convert.ToInt32(ViewState["TotalRecord"]);
    }

    private string _selectedVendorIDs = string.Empty;
    public string SelectedVendorIDs
    {
        get
        {
            if (!string.IsNullOrEmpty(hiddenSelectedIDs.Value))
            {
                return hiddenSelectedIDs.Value.Substring(0, hiddenSelectedIDs.Value.Length - 1);
            }
            return null;
        }
        set
        {
            hiddenSelectedIDs.Value += value + ",";
        }
    }

    protected void BindCountry()
    {
        MAS_CountryBE oMAS_CountryBE = new MAS_CountryBE();
        MAS_CountryBAL oMAS_CountryBAL = new MAS_CountryBAL();

        oMAS_CountryBE.Action = "ShowAll";
        oMAS_CountryBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
        oMAS_CountryBE.User.UserID = Convert.ToInt32(Session["UserID"]);

        List<MAS_CountryBE> lstCountry = new List<MAS_CountryBE>();
        lstCountry = oMAS_CountryBAL.GetCountryBAL(oMAS_CountryBE);
        oMAS_CountryBAL = null;
        if (lstCountry.Count > 0)
        {
            FillControls.FillDropDown(ref ddlCountry, lstCountry.ToList(), "CountryName", "CountryID", "All");
        }
        if (Session["SiteCountryID"] != null)
        {
            ddlCountry.SelectedIndex = ddlCountry.Items.IndexOf(ddlCountry.Items.FindByValue(Session["SiteCountryID"].ToString()));
            btnSendImmediatelyNew.Visible = false;
            btnEndofDay.Visible = false;
            btnDoNotMail.Visible = false;
        }
    }

    protected void BindVendor()
    {
        MAS_VendorScoreCardBE objVendorScoreCardBE = new MAS_VendorScoreCardBE();
        MAS_VendorScoreCardBAL objVendorScoreCardBAL = new MAS_VendorScoreCardBAL();

        objVendorScoreCardBE.Action = "GetVendorByCountryAutoEmailPO";
        objVendorScoreCardBE.CountryID = Convert.ToInt32(ddlCountry.SelectedValue);

        if (IsVendorByNameClicked == true)
        {
            objVendorScoreCardBE.VendorName = txtVendorNo.Text.Trim();
        }
        else if (IsVendorByNumberClicked == true)
        {
            objVendorScoreCardBE.VendorNo = txtVendorNo.Text.Trim();
        }

        List<MAS_VendorScoreCardBE> lstUPVendor = new List<MAS_VendorScoreCardBE>();
        lstUPVendor = objVendorScoreCardBAL.GetVendorByCountryBAL(objVendorScoreCardBE);
        objVendorScoreCardBAL = null;
        if (lstUPVendor.Count > 0)
        {
            FillControls.FillListBox(ref lstLeft, lstUPVendor, "VendorName", "VendorID");
        }
        else
        {
            lstLeft.Items.Clear();
            lstRight.Items.Clear();
        }

    }


    #region Events

    protected virtual void btnMoveRightAll_Click(object sender, EventArgs e)
    {
        FillControls.MoveAllItems(lstLeft, lstRight);
    }

    protected virtual void btnMoveRight_Click(object sender, EventArgs e)
    {
        if (lstLeft.SelectedItem != null)
        {
            FillControls.MoveOneItem(lstLeft, lstRight);
        }
    }

    protected virtual void btnMoveLeft_Click(object sender, EventArgs e)
    {
        if (lstRight.SelectedItem != null)
        {
            FillControls.MoveOneItem(lstRight, lstLeft);
        }
    }

    protected virtual void btnMoveLeftAll_Click(object sender, EventArgs e)
    {
        FillControls.MoveAllItems(lstRight, lstLeft);
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        IsVendorByNameClicked = true;
        IsVendorByNumberClicked = false;
        lstRight.Items.Clear();
        hiddenSelectedIDs.Value = string.Empty;
        hiddenSelectedName.Value = string.Empty;
        BindVendor();
        pager1.ItemCount = Convert.ToInt32(ViewState["TotalRecord"]);

    }

    protected void btnSearchByVendorNo_Click(object sender, EventArgs e)
    {
        IsVendorByNameClicked = false;
        IsVendorByNumberClicked = true;
        lstRight.Items.Clear();
        hiddenSelectedIDs.Value = string.Empty;
        hiddenSelectedName.Value = string.Empty;
        BindVendor();
        pager1.ItemCount = Convert.ToInt32(ViewState["TotalRecord"]);
    }
    #endregion
    protected void btnShow_Click(object sender, EventArgs e)
    {
        BindAutoEmailNewPoDetails();
        SetVendorOnPostBack();
    }
    protected void btnAutoEmail_Click(object sender, EventArgs e)
    {
        string VendorIds = string.Empty;
        foreach (GridViewRow row in grdAutoEmailPODetails.Rows)
        {
            CheckBox chkSelect = (CheckBox)row.FindControl("chkSelect");
            Literal ltVendorID = (Literal)row.FindControl("ltVendorID");
            HiddenField hdnCountryId = (HiddenField)row.FindControl("hdnCountryID");

            if (chkSelect.Checked)
            {
                // VendorIds+=ltVendorID.Text+ ",";
                //}
                MASCNT_AutoEmailBE AutoEmailBE = new MASCNT_AutoEmailBE();
                APPCNT_AutoEmailBAL AutoEmailBAL = new APPCNT_AutoEmailBAL();
                AutoEmailBE.UP_VendorBE = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();
                AutoEmailBE.ModifiedByID = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
                DateTime myDateTime = DateTime.Now;
                string sqlFormattedDate = myDateTime.Date.ToString("yyyy-MM-dd HH:mm:ss.fff");
                AutoEmailBE.Action = "SaveAutoEmailStatus";
                AutoEmailBE.UP_VendorBE.CountryID = Convert.ToInt32(ddlCountry.SelectedValue);
                AutoEmailBE.UP_VendorBE.VendorIDs = ltVendorID.Text;
                AutoEmailBE.ModifiedByID.UserID = Convert.ToInt32(Session["UserID"]);
                AutoEmailBE.IsAutoEmailNewPO = 2;
                AutoEmailBE.ModifiedOn = sqlFormattedDate;
                if (hdnCountryId.Value != "3")
                {
                    AutoEmailBAL.SaveAutoEmailPOToVendorStatusBAL(AutoEmailBE);
                }
            }
        }

        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + AutoEmailPoError + "')", true);
        BindAutoEmailNewPoDetails();
        pager1.ItemCount = Convert.ToInt32(ViewState["TotalRecord"]);
    }
    protected void btnDoNotAutoEmail_Click(object sender, EventArgs e)
    {
        string VendorIds = string.Empty;
        foreach (GridViewRow row in grdAutoEmailPODetails.Rows)
        {
            CheckBox chkSelect = (CheckBox)row.FindControl("chkSelect");
            Literal ltVendorID = (Literal)row.FindControl("ltVendorID");
            HiddenField hdnCountryId = (HiddenField)row.FindControl("hdnCountryID");
            if (chkSelect.Checked)
            {
                MASCNT_AutoEmailBE AutoEmailBE = new MASCNT_AutoEmailBE();
                APPCNT_AutoEmailBAL AutoEmailBAL = new APPCNT_AutoEmailBAL();
                AutoEmailBE.UP_VendorBE = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();
                AutoEmailBE.ModifiedByID = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
                DateTime myDateTime = DateTime.Now;
                string sqlFormattedDate = myDateTime.Date.ToString("yyyy-MM-dd HH:mm:ss.fff");
                AutoEmailBE.Action = "SaveAutoEmailStatus";
                AutoEmailBE.UP_VendorBE.CountryID = Convert.ToInt32(ddlCountry.SelectedValue);
                AutoEmailBE.UP_VendorBE.VendorIDs = ltVendorID.Text;
                AutoEmailBE.ModifiedByID.UserID = Convert.ToInt32(Session["UserID"]);
                AutoEmailBE.IsAutoEmailNewPO = 0;
                AutoEmailBE.ModifiedOn = sqlFormattedDate;
                if (hdnCountryId.Value != "3")
                {
                    AutoEmailBAL.SaveAutoEmailPOToVendorStatusBAL(AutoEmailBE);
                }
            }
        }

        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + AutoEmailPoError + "')", true);
        BindAutoEmailNewPoDetails();
        pager1.ItemCount = Convert.ToInt32(ViewState["TotalRecord"]);
    }
    protected void btnSendImmediately_Click(object sender, EventArgs e)
    {
        string VendorIds = string.Empty;
        foreach (GridViewRow row in grdAutoEmailPODetails.Rows)
        {
            CheckBox chkSelect = (CheckBox)row.FindControl("chkSelect");
            Literal ltVendorID = (Literal)row.FindControl("ltVendorID");
            HiddenField hdnCountryId = (HiddenField)row.FindControl("hdnCountryID");

            if (chkSelect.Checked)
            {
                MASCNT_AutoEmailBE AutoEmailBE = new MASCNT_AutoEmailBE();
                APPCNT_AutoEmailBAL AutoEmailBAL = new APPCNT_AutoEmailBAL();
                AutoEmailBE.UP_VendorBE = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();
                AutoEmailBE.ModifiedByID = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
                DateTime myDateTime = DateTime.Now;
                string sqlFormattedDate = myDateTime.Date.ToString("yyyy-MM-dd HH:mm:ss.fff");
                AutoEmailBE.Action = "SaveAutoEmailStatus";
                AutoEmailBE.UP_VendorBE.CountryID = Convert.ToInt32(ddlCountry.SelectedValue);
                AutoEmailBE.UP_VendorBE.VendorIDs = ltVendorID.Text;
                AutoEmailBE.ModifiedByID.UserID = Convert.ToInt32(Session["UserID"]);
                AutoEmailBE.IsAutoEmailNewPO = 1;
                AutoEmailBE.ModifiedOn = sqlFormattedDate;
                if (hdnCountryId.Value != "3")
                {
                    AutoEmailBAL.SaveAutoEmailPOToVendorStatusBAL(AutoEmailBE);
                }
            }
        }

        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + AutoEmailPoError + "')", true);
        BindAutoEmailNewPoDetails();
        pager1.ItemCount = Convert.ToInt32(ViewState["TotalRecord"]);
    }
    public void pager_Command(object sender, CommandEventArgs e)
    {
        int currnetPageIndx = Convert.ToInt32(e.CommandArgument);
        pager1.CurrentIndex = currnetPageIndx;
        BindAutoEmailNewPoDetails(currnetPageIndx);
    }

    protected void btnExport_Click(object sender, EventArgs e)
    {
        IsExportClicked = true;
        BindAutoEmailNewPoDetails();
        grdAutoEmailPODetails.Columns[0].Visible = false;
        WebCommon.ExportHideHidden("AutoEmailNewPOToVendor", grdAutoEmailPODetails);
    }

    public void SetVendorOnPostBack()
    {
        string[] strSelectedName = null;
        string[] strSelectedIDs = null;
        lstRight.Items.Clear();
        if (!string.IsNullOrEmpty(Convert.ToString(ViewState["hiddenSelectedName"])))
        {
            strSelectedName = Convert.ToString(ViewState["hiddenSelectedName"]).Split(',');
        }
        if (!string.IsNullOrEmpty(Convert.ToString(ViewState["hiddenSelectedIDs"])))
        {
            strSelectedIDs = Convert.ToString(ViewState["hiddenSelectedIDs"]).Split(',');
        }
        //string[] strSelectedName = hiddenSelectedName.Value.TrimEnd(',').TrimEnd(' ').Split(',');
        //string[] strSelectedIDs = hiddenSelectedIDs.Value.TrimEnd(',').Split(',');
        if (strSelectedName != null && strSelectedIDs != null)
        {
            for (int i = 0; i < strSelectedName.Length; i++)
            {
                lstRight.Items.Add(new ListItem(strSelectedName[i].ToString(), strSelectedIDs[i].ToString()));
                lstLeft.Items.Remove(new ListItem(strSelectedName[i].ToString(), strSelectedIDs[i].ToString()));
                lstLeft.Items.Remove(new ListItem(null, null));
                lstRight.Items.Remove(new ListItem(null, null));
            }
        }
    }
    public void RemoveVendor(string VendorIdID)
    {
        if (lstLeft.Items.FindByValue(VendorIdID.Trim()) != null)
            lstLeft.Items.RemoveAt(lstLeft.Items.IndexOf(lstLeft.Items.FindByValue(VendorIdID.Trim())));
    }

    protected void grdAutoEmailPODetails_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        GridViewRow gvr = e.Row;
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            #region 
            ucLiteral ltAutoEmailNewPOs = (ucLiteral)e.Row.FindControl("ltAutoEmailNewPOs");

            if (ltAutoEmailNewPOs.Text == "2")
            {
                ltAutoEmailNewPOs.Text = "Evening";
            }
            else if (ltAutoEmailNewPOs.Text == "1")
            {
                ltAutoEmailNewPOs.Text = "Immediate";
            }
            else
            {
                ltAutoEmailNewPOs.Text = "Do not mail";
            }
            #endregion      
        }
    }
}
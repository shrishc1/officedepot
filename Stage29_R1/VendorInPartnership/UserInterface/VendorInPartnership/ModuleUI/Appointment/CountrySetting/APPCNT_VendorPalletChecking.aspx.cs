﻿using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using BusinessEntities.ModuleBE.VendorScorecard;
using BusinessLogicLayer.ModuleBAL.Appointment.SiteSettings;
using BusinessLogicLayer.ModuleBAL.VendorScorecard;
using System;
using System.Collections.Generic;
using System.Data;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebUtilities;

public partial class ModuleUI_Appointment_CountrySetting_APPCNT_VendorPalletChecking : CommonPage
{
    protected bool IsVendorByNameClicked = false;
    protected bool IsVendorByNumberClicked = false;
    bool IsExportClicked = false;
    protected string VendorPalletCheckingSuccessMesg = WebCommon.getGlobalResourceValue("VendorPalletCheckingSuccessMesg");
    protected string VendorPalletCheckingSuccessMesg2 = WebCommon.getGlobalResourceValue("VendorPalletCheckingSuccessMesg2");
    protected string VendorPalletCheckingAlreadyExists = WebCommon.getGlobalResourceValue("VendorPalletCheckingAlreadyExists");
    protected string DeleteMessage = WebCommon.getGlobalResourceValue("DeleteMessage");
    public bool IsVenodrPalletChecking
    {
        get
        {
            if (ViewState["IsVenodrPalletChecking"] == null)
                ViewState["IsVenodrPalletChecking"] = false;
            return (bool)ViewState["IsVenodrPalletChecking"];
        }
        set
        {
            ViewState["IsVenodrPalletChecking"] = value;
        }
    }
    public bool IsStandardPalletChecking
    {
        get
        {
            if (ViewState["IsStandardPalletChecking"] == null)
                ViewState["IsStandardPalletChecking"] = false;
            return (bool)ViewState["IsStandardPalletChecking"];
        }
        set
        {
            ViewState["IsStandardPalletChecking"] = value;
        }
    }
    public int PalletChecking
    {
        get
        {
            if (ViewState["PalletChecking"] == null)
                ViewState["PalletChecking"] = 1;
            return Convert.ToInt32(ViewState["PalletChecking"]);
        }
        set
        {
            ViewState["PalletChecking"] = value;
        }
    }
    public int SiteId
    {
        get
        {
            return Convert.ToInt32(ViewState["SiteId"]);
        }
        set
        {
            ViewState["SiteId"] = value;
        }
    }
    public string VendorName
    {
        get
        {
            return Convert.ToString(ViewState["VendorName"]);
        }
        set
        {
            ViewState["VendorName"] = value;
        }
    }
    public DataTable PalletTable
    {
        get
        {
            return ViewState["PalletTable"] as DataTable;
        }
        set
        {
            ViewState["PalletTable"] = value;
        }
    }
    public int SiteCountryId
    {
        get
        {
            return Convert.ToInt32(ViewState["SiteCountryId"]);
        }
        set
        {
            ViewState["SiteCountryId"] = value;
        }
    }
    public List<MAS_VendorScoreCardBE> ListVendor
    {
        get
        {
            return (List<MAS_VendorScoreCardBE>)ViewState["ListVendor"];
        }
        set
        {
            ViewState["ListVendor"] = value;
        }
    }
    protected void Page_Init(object sender, EventArgs e)
    {
        ddlSite.CurrentPage = this;
        ddlSite.innerControlddlSite.AutoPostBack = true;
        ddlSite.IsAllRequired = false;
    }
    protected void btnHidden_Click(object sender, EventArgs e)
    {
        int vendorid = Convert.ToInt32(hdnSeletedVendorID.Value);
        var countryid = ListVendor.Find(x => x.VendorID == vendorid).CountryID;
        ddlSite.CurrentPage = this;
        ddlSite.CountryID = countryid.Value;
        ddlSite.innerControlddlSite.AutoPostBack = true;
        ddlSite.IsAllRequired = false;

        VendorName = hdnVendor.Value;

        txtSelectedVendorValue.Text = hdnVendor.Value;

        if (ListVendor != null && ListVendor.Count > 0 && Convert.ToInt32(hdnSeletedVendorID.Value) > 0)
        {
            SiteCountryId = ListVendor.Find(x => x.VendorID == Convert.ToInt32(hdnSeletedVendorID.Value)).CountryID.Value;
            ddlSite.CountryID = SiteCountryId;
            txtSelectedVendorValue.Text = hdnVendor.Value;
            ddlSite.BindSites();
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            sitetable.Visible = chkPalletChecking.SelectedValue == "2";

            pager1.PageSize = 200;
            pager1.GenerateGoToSection = false;
            pager1.GeneratePagerInfoSection = false;
            pager1.Visible = false;
            BindGridView();

            if (!string.IsNullOrEmpty(GetQueryStringValue("value")))
            {
                trVendorPalletChecking.Visible = true;
                trVendorPalletCheckingButton.Visible = true;
                btnAdd.Visible = false;
                btnExportToExcel.Visible = false;
                grdVendorData.Visible = false;
                pager.Visible = false;
                btnDelete.Visible = false;
            }
            else
            {
                trVendorPalletChecking.Visible = false;
                trVendorPalletCheckingButton.Visible = false;
                btnAdd.Visible = true;
                btnExportToExcel.Visible = true;
                grdVendorData.Visible = true;
                pager.Visible = true;
                btnDelete.Visible = true;
            }
        }
    }
    protected void BtnSave_Click(object sender, EventArgs e)
    {
        UP_VendorBE oUP_VendorBE = new UP_VendorBE();
        UP_VendorBAL oUP_VendorBAL = new UP_VendorBAL();
        oUP_VendorBE.Action = "UpdateVendorPallet";
        oUP_VendorBE.VendorID = Convert.ToInt32(hdnSeletedVendorID.Value);
        oUP_VendorBE.PalletChecking = Convert.ToInt32(chkPalletChecking.SelectedValue);
        oUP_VendorBE.Site = new BusinessEntities.ModuleBE.AdminFunctions.MAS_SiteBE();
        oUP_VendorBE.Site.SiteID = oUP_VendorBE.PalletChecking == 2 ? Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value) : 0;
        int result = oUP_VendorBAL.UpdateVendorPalletBAL(oUP_VendorBE);
        if (result == 2 || result == 1)
        {
            btnMoveRight.Disabled = true;
            btnMoveLeft.Disabled = true;
            EncryptQueryString("APPCNT_VendorPalletChecking.aspx");
        }
    }
    protected void btnRemove_Click(object sender, EventArgs e)
    {
        UP_VendorBE oUP_VendorBE = new UP_VendorBE();
        UP_VendorBAL oUP_VendorBAL = new UP_VendorBAL();
        oUP_VendorBE.Action = "RemoveVendorPallet";
        oUP_VendorBE.VendorID = Convert.ToInt32(ViewState["SelectedVendorID"]);
        oUP_VendorBE.PalletChecking = PalletChecking;
        oUP_VendorBE.Site = new BusinessEntities.ModuleBE.AdminFunctions.MAS_SiteBE();
        oUP_VendorBE.Site.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
        oUP_VendorBAL.UpdateVendorPalletBAL(oUP_VendorBE);
        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('" + VendorPalletCheckingSuccessMesg + "')", true);
        mdlQueryMsg.Hide();
        BindGridView();
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        mdlQueryMsg.Hide();
    }
    protected void grdVendorPalletChecking_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if ((e.Row.RowType == DataControlRowType.Header || e.Row.RowType == DataControlRowType.DataRow) && IsExportClicked)
        {
            e.Row.Cells[6].Visible = false;
        }
    }
    protected void btnBack_Click(object sender, EventArgs e)
    {
        btnMoveRight.Disabled = true;
        btnMoveLeft.Disabled = true;
        EncryptQueryString("APPCNT_VendorPalletChecking.aspx");
    }
    public void BindGridView(int Page = 1)
    {
        UP_VendorBE oUP_VendorBE = new UP_VendorBE();
        UP_VendorBAL oUP_VendorBAL = new UP_VendorBAL();

        oUP_VendorBE.Action = "BindVendorPallet";
        if (IsExportClicked)
        {
            oUP_VendorBE.GridCurrentPageNo = 0;
            oUP_VendorBE.GridPageSize = 0;
        }
        else
        {
            oUP_VendorBE.GridCurrentPageNo = Page;
            oUP_VendorBE.GridPageSize = 200;
        }

        PalletTable = oUP_VendorBAL.BindVendorPalletBAL(oUP_VendorBE);

        if (PalletTable.Rows.Count > 0)
        {
            pager1.Visible = true;
            pager1.ItemCount = Convert.ToInt32(PalletTable.Rows[0]["TotalRecords"]);
            grdVendorPalletChecking.DataSource = PalletTable;
            grdVendorPalletChecking.DataBind();
            btnExportToExcel.Visible = true;
        }
        else
        {
            grdVendorPalletChecking.DataSource = null;
            grdVendorPalletChecking.DataBind();
            btnExportToExcel.Visible = false;
        }
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        IsVendorByNameClicked = true;
        IsVendorByNumberClicked = false;
        BindVendor();
        txtSelectedVendorValue.Text = string.Empty;
    }
    public void pager_Command(object sender, CommandEventArgs e)
    {
        int currnetPageIndx = Convert.ToInt32(e.CommandArgument);
        pager1.CurrentIndex = currnetPageIndx;
        BindGridView(currnetPageIndx);
    }
    protected void btnSearchByVendorNo_Click(object sender, EventArgs e)
    {
        IsVendorByNameClicked = false;
        IsVendorByNumberClicked = true;
        BindVendor();
        txtSelectedVendorValue.Text = string.Empty;
    }
    protected void BindVendor()
    {
        MAS_VendorScoreCardBE objVendorScoreCardBE = new MAS_VendorScoreCardBE();
        MAS_VendorScoreCardBAL objVendorScoreCardBAL = new MAS_VendorScoreCardBAL();

        objVendorScoreCardBE.Action = "GetVendorPalletChecking";

        switch (IsVendorByNameClicked)
        {
            case true:
                objVendorScoreCardBE.VendorName = txtVendorNo.Text.Trim();
                break;
            default:
                if (IsVendorByNumberClicked)
                    objVendorScoreCardBE.VendorNo = txtVendorNo.Text.Trim();
                break;
        }
        List<MAS_VendorScoreCardBE> lstUPVendor = objVendorScoreCardBAL.GetVendorByCountryBAL(objVendorScoreCardBE);

        ListVendor = lstUPVendor;

        if (lstUPVendor.Count > 0)
        {
            FillControls.FillListBox(ref lstLeft, lstUPVendor, "VendorName", "VendorID");
        }
        else
        {
            lstLeft.Items.Clear();
            txtSelectedVendorValue.Text = string.Empty;
        }
    }
    protected void btnExport_Click(object sender, EventArgs e)
    {
        IsExportClicked = true;
        BindGridView();
        WebCommon.ExportHideHidden("VendorPalletChecking", grdVendorPalletChecking);
    }
    protected void lnkRemove_Click(object sender, EventArgs e)
    {
        LinkButton btn = (LinkButton)(sender);
        int Vendorid = Convert.ToInt32(btn.CommandArgument);
        ViewState["SelectedVendorID"] = Vendorid;
        hdnSeletedVendorID.Value = Vendorid.ToString();
        trVendorPalletChecking.Visible = true;
        trVendorPalletCheckingButton.Visible = true;
        btnAdd.Visible = false;
        btnExportToExcel.Visible = false;
        grdVendorData.Visible = false;
        pager.Visible = false;
        txtSelectedVendorValue.Text = btn.Text;
        btnSave.CausesValidation = false;
        GridViewRow grdrow = (GridViewRow)((LinkButton)sender).NamingContainer;

        var chkVendorPallet = (CheckBox)grdrow.FindControl("chkVendorPallet");
        var chkStandard = (CheckBox)grdrow.FindControl("chkStandard");

        IsVenodrPalletChecking = chkVendorPallet.Checked;
        IsStandardPalletChecking = chkStandard.Checked;
        chkPalletChecking.SelectedValue = IsVenodrPalletChecking ? "1" : "2";
        PalletChecking = chkVendorPallet.Checked ? 1 : 2;
        btnMoveRight.Disabled = true;
        btnMoveLeft.Disabled = true;

        Label lblVendor_1 = (Label)grdrow.FindControl("lblVendor");
        HiddenField hdnSiteID = (HiddenField)grdrow.FindControl("hdnSiteID");
        HiddenField hdnSiteCountryID = (HiddenField)grdrow.FindControl("hdnSiteCountryID");

        SiteCountryId = !string.IsNullOrEmpty(hdnSiteCountryID.Value) ? Convert.ToInt32(hdnSiteCountryID.Value) : 0;
        SiteId = !string.IsNullOrEmpty(hdnSiteID.Value) ? Convert.ToInt32(hdnSiteID.Value) : 0;
        hdnVendor.Value = "(" + grdrow.Cells[0].Text + ")" + lblVendor_1.Text;
        txtSelectedVendorValue.Text = hdnVendor.Value;
        VendorName = hdnVendor.Value;
        sitetable.Visible = PalletChecking == 2;

        btnSave.Text = WebCommon.getGlobalResourceValue("Update");

        if (SiteCountryId > 0)
        {
            ddlSite.CountryID = SiteCountryId;
            ddlSite.BindSites();
            if (SiteId > 0)
                ddlSite.innerControlddlSite.SelectedValue = SiteId.ToString();
        }
    }
    protected void lnkEdit_Click(object sender, EventArgs e)
    {
        LinkButton btn = (LinkButton)(sender);
        int Vendorid = Convert.ToInt32(btn.CommandArgument);
        ViewState["SelectedVendorID"] = Vendorid;
        hdnSeletedVendorID.Value = Vendorid.ToString();
        trVendorPalletChecking.Visible = true;
        trVendorPalletCheckingButton.Visible = true;
        btnAdd.Visible = false;
        btnExportToExcel.Visible = false;
        grdVendorData.Visible = false;
        pager.Visible = false;
        txtSelectedVendorValue.Text = btn.Text;
        btnSave.CausesValidation = false;
        GridViewRow grdrow = (GridViewRow)((LinkButton)sender).NamingContainer;

        var chkVendorPallet = (CheckBox)grdrow.FindControl("chkVendorPallet");
        var chkStandard = (CheckBox)grdrow.FindControl("chkStandard");

        IsVenodrPalletChecking = chkVendorPallet.Checked;
        IsStandardPalletChecking = chkStandard.Checked;
        chkPalletChecking.SelectedValue = IsVenodrPalletChecking ? "1" : "2";
        PalletChecking = chkVendorPallet.Checked ? 1 : 2;
        btnMoveRight.Disabled = true;
        btnMoveLeft.Disabled = true;

        sitetable.Visible = PalletChecking == 2;
    }
    public override void SiteSelectedIndexChanged()
    {
        txtSelectedVendorValue.Text = VendorName;
    }
    protected void btnDelete_Click(object sender, EventArgs e)
    {
        UP_VendorBE oUP_VendorBE = new UP_VendorBE();
        UP_VendorBAL oUP_VendorBAL = new UP_VendorBAL();
        oUP_VendorBE.Action = "RemoveVendorPallet";
        oUP_VendorBE.VendorID = Convert.ToInt32(ViewState["SelectedVendorID"]);
        oUP_VendorBE.PalletChecking = PalletChecking;
        oUP_VendorBE.Site = new BusinessEntities.ModuleBE.AdminFunctions.MAS_SiteBE();
        oUP_VendorBE.Site.SiteID = SiteId;
        oUP_VendorBAL.UpdateVendorPalletBAL(oUP_VendorBE);

        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('" + VendorPalletCheckingSuccessMesg + "')", true);

        EncryptQueryString("APPCNT_VendorPalletChecking.aspx");
    }
    protected void chkPalletChecking_SelectedIndexChanged(object sender, EventArgs e)
    {
        txtSelectedVendorValue.Text = VendorName;
        sitetable.Visible = chkPalletChecking.SelectedValue == "2";
        ddlSite.CountryID = SiteCountryId;
        ddlSite.BindSites();
    }
    protected void btnContinue_Click(object sender, CommandEventArgs e)
    {
        UP_VendorBE oUP_VendorBE = new UP_VendorBE();
        UP_VendorBAL oUP_VendorBAL = new UP_VendorBAL();
        oUP_VendorBE.Action = "UpdateVendorPallet";
        oUP_VendorBE.VendorID = Convert.ToInt32(hdnSeletedVendorID.Value);
        oUP_VendorBE.PalletChecking = Convert.ToInt32(chkPalletChecking.SelectedValue);
        oUP_VendorBE.Site = new BusinessEntities.ModuleBE.AdminFunctions.MAS_SiteBE();
        oUP_VendorBE.Site.SiteID = oUP_VendorBE.PalletChecking == 2  ? Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value) : 0;
        int result = oUP_VendorBAL.UpdateVendorPalletBAL(oUP_VendorBE);

        if (result == 2 || result == 1)
        {
            btnMoveRight.Disabled = true;
            btnMoveLeft.Disabled = true;
            EncryptQueryString("APPCNT_VendorPalletChecking.aspx");
        }

        mdlQueryMsg.Hide();
    }

    [WebMethod]
    public static int GetData(int VendorID)
    {
        UP_VendorBE oUP_VendorBE = new UP_VendorBE();
        UP_VendorBAL oUP_VendorBAL = new UP_VendorBAL();
        oUP_VendorBE.Action = "BindVendorPallet";
        DataTable dataTable= oUP_VendorBAL.BindVendorPalletBAL(oUP_VendorBE);
        DataRow[] dr = dataTable.Select("VendorID = " + Convert.ToString(VendorID));
        return dr.Length > 0 ? 1 : 0;
    }
}
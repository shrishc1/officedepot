﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="APPCNT_ISPM15CountrySetup.aspx.cs"
    Inherits="ModuleUI_Appointment_CountrySetting_APPCNT_ISPM15CountrySetup" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master" %>
 


<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="../../../CommonUI/UserControls/ucCountry.ascx" TagName="ucCountry"
    TagPrefix="uc1" %>
<%@ Register Src="../../../CommonUI/UserControls/ucAddButton.ascx" TagName="ucAddButton"
    TagPrefix="cc2" %>
<%@ Register Src="../../../CommonUI/UserControls/ucExportToExcel.ascx" TagName="ucExportToExcel"
    TagPrefix="cc2" %>
 

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
    <h2>
        <cc1:ucLabel ID="lblISPM15Countrys" runat="server" Text="ISPM15 Countrys"></cc1:ucLabel>
    </h2>

    <asp:Panel ID="pnlAddISPM15Country" runat="server">
        <div class="button-row">
            <cc2:ucAddButton ID="btnAdd" runat="server" NavigateUrl="APPCNT_ISPM15CountrySetupEdit.aspx?A=add" />
            <cc1:ucButton ID="btnExportToExcel" runat="server" CssClass="exporttoexcel" Text="Export To Excel" 
                OnClick="btnExport_Click"
                        Width="109px" Height="20px" Visible="false" />
        </div>
        <table width="100%">
            <tr>
                <td align="left">
                    <cc1:ucGridView ID="grdCountry" Width="50%" runat="server" CssClass="grid" OnSorting="SortGrid" AllowSorting="true">
                        <Columns>

                            <asp:TemplateField HeaderText="Country" SortExpression="CountryName">
                                <HeaderStyle Width="10%" HorizontalAlign="Left" />
                                <ItemTemplate>
                                    <asp:HyperLink ID="hpCarrier"
                                        runat="server"
                                        Text='<%# Eval("CountryName") %>'
                                        NavigateUrl='<%# EncryptQuery("APPCNT_ISPM15CountrySetupEdit.aspx?CountryID="+ Eval("CountryID")+"&CountryName="+Eval("CountryName")) %>'></asp:HyperLink>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:TemplateField>
                        </Columns>
                    </cc1:ucGridView>
                </td>
            </tr>
        </table>

    </asp:Panel>
     
</asp:Content>
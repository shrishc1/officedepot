﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using Utilities; using WebUtilities;
using BusinessEntities.ModuleBE.Appointment.CountrySetting;
using BusinessLogicLayer.ModuleBAL.Appointment.CountrySetting;
using BaseControlLibrary;



public partial class APPCNT_DeliveryTypeSetupEdit : CommonPage {
    protected string deleteMessage = WebCommon.getGlobalResourceValue("DeleteMessage");
    protected void Page_Init(object sender, EventArgs e) {
        ddlCountry.CurrentPage = this;
    }

    protected void Page_Load(object sender, EventArgs e) {
        
    }
    #region Methods

    public override void CountryPost_Load() {
        if (!IsPostBack) {
            GetDeliveryType();
        }
    }

    private void GetDeliveryType() {

        MASCNT_DeliveryTypeBE oMASCNT_DeliveryTypeBE = new MASCNT_DeliveryTypeBE();
        APPCNT_DeliveryTypeBAL oMASCNT_DeliveryTypeBAL = new APPCNT_DeliveryTypeBAL();

        if (GetQueryStringValue("CountryDeliveryTypeID") != null) {
           // btnSave.Visible = false;
            oMASCNT_DeliveryTypeBE.Action = "ShowById";
            oMASCNT_DeliveryTypeBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
            oMASCNT_DeliveryTypeBE.User.UserID = Convert.ToInt32(Session["UserID"]);
            oMASCNT_DeliveryTypeBE.DeliveryTypeID = Convert.ToInt32(GetQueryStringValue("CountryDeliveryTypeID").ToString());
            MASCNT_DeliveryTypeBE lstDeliveryType = oMASCNT_DeliveryTypeBAL.GetDeliveryTypeDetailsByIdBAL(oMASCNT_DeliveryTypeBE);

            ddlCountry.innerControlddlCountry.SelectedValue = Convert.ToString(lstDeliveryType.CountryID);
            txtDeliveryType.Text = lstDeliveryType.DeliveryType;
            if (lstDeliveryType.IsEnabledAsVendorOption == true) {
                chkEnableAsVendorCarrier.Checked = true;
            }
            else {
                chkEnableAsVendorCarrier.Checked = false;
            }

            ddlCountry.innerControlddlCountry.Enabled = false;
            txtDeliveryType.Enabled = false;
           // chkEnableAsVendorCarrier.Enabled = false;
        }
        else {
            btnDelete.Visible = false;
        }
    }
    
    #endregion

    #region Events

    #endregion
    protected void btnSave_Click(object sender, EventArgs e) {
        MASCNT_DeliveryTypeBE oMASCNT_DeliveryTypeBE = new MASCNT_DeliveryTypeBE();
        APPCNT_DeliveryTypeBAL oMASCNT_DeliveryTypeBAL = new APPCNT_DeliveryTypeBAL();
        oMASCNT_DeliveryTypeBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();

        if (GetQueryStringValue("CountryDeliveryTypeID") == null)//add mode 
        {
            oMASCNT_DeliveryTypeBE.DeliveryType = txtDeliveryType.Text.Trim();
            oMASCNT_DeliveryTypeBE.CountryID = Convert.ToInt32(ddlCountry.innerControlddlCountry.SelectedItem.Value);
            oMASCNT_DeliveryTypeBE.Action = "CheckExistance";
            List<MASCNT_DeliveryTypeBE> lstDeliveryTypes = oMASCNT_DeliveryTypeBAL.GetDeliveryTypeDetailsBAL(oMASCNT_DeliveryTypeBE);
            if (lstDeliveryTypes != null && lstDeliveryTypes.Count > 0) {
                string errorMessage = WebCommon.getGlobalResourceValue("DeliveryTypeExists");
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Message", "alert('" + errorMessage + "')", true);
            }
            else {
                oMASCNT_DeliveryTypeBE.Action = "Add";
                oMASCNT_DeliveryTypeBE.EnabledAsVendorOption = chkEnableAsVendorCarrier.Checked == true ? "Yes" : "No";
                oMASCNT_DeliveryTypeBAL.addEditDeliveryTypeDetailsBAL(oMASCNT_DeliveryTypeBE);
                EncryptQueryString("APPCNT_DeliveryTypeSetupOverview.aspx");
            }
        }
        else //Edit Mode
        {
            oMASCNT_DeliveryTypeBE.Action = "UpdateEnableVendorCarrier";
            oMASCNT_DeliveryTypeBE.EnabledAsVendorOption = chkEnableAsVendorCarrier.Checked == true ? "Yes" : "No";
            oMASCNT_DeliveryTypeBE.DeliveryTypeID = Convert.ToInt32( GetQueryStringValue("CountryDeliveryTypeID"));
            oMASCNT_DeliveryTypeBAL.addEditDeliveryTypeDetailsBAL(oMASCNT_DeliveryTypeBE);
            EncryptQueryString("APPCNT_DeliveryTypeSetupOverview.aspx");
        }
    }
    
    protected void btnDelete_Click(object sender, EventArgs e) {
        MASCNT_DeliveryTypeBE oMASCNT_DeliveryTypeBE = new MASCNT_DeliveryTypeBE();
        APPCNT_DeliveryTypeBAL oMASCNT_DeliveryTypeBAL = new APPCNT_DeliveryTypeBAL();

        oMASCNT_DeliveryTypeBE.Action = "Edit";

        if (GetQueryStringValue("CountryDeliveryTypeID") != null) {
            oMASCNT_DeliveryTypeBE.DeliveryTypeID = Convert.ToInt32(GetQueryStringValue("CountryDeliveryTypeID"));

            oMASCNT_DeliveryTypeBAL.addEditDeliveryTypeDetailsBAL(oMASCNT_DeliveryTypeBE);
        }

        EncryptQueryString("APPCNT_DeliveryTypeSetupOverview.aspx");
    }
    protected void btnBack_Click(object sender, EventArgs e) {
        EncryptQueryString("APPCNT_DeliveryTypeSetupOverview.aspx");
    }
}
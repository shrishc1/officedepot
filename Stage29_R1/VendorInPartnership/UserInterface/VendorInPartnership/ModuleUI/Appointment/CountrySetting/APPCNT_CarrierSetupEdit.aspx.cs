﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using BaseControlLibrary;

using Utilities; using WebUtilities;
using BusinessEntities.ModuleBE.Appointment.CountrySetting;
using BusinessLogicLayer.ModuleBAL.Appointment.CountrySetting;




public partial class APPCNT_CarrierSetupEdit : CommonPage {

    protected string deleteMessage = WebCommon.getGlobalResourceValue("DeleteMessage");
    protected void Page_Init(object sender, EventArgs e) {
        ucCountry.CurrentPage = this;
       
    }

    protected void Page_Load(object sender, EventArgs e) {
    }
    #region Methods

    private void GetCarrier() {
        MASCNT_CarrierBE oMASCNT_CarrierBE = new MASCNT_CarrierBE();
        APPCNT_CarrierBAL oMASCNT_CarrierBAL = new APPCNT_CarrierBAL();


        if (GetQueryStringValue("CountryCarrierID") != null) {
          
            oMASCNT_CarrierBE.Action = "ShowAll";
            oMASCNT_CarrierBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
            oMASCNT_CarrierBE.User.UserID = Convert.ToInt32(Session["UserID"]);
            oMASCNT_CarrierBE.CarrierID = Convert.ToInt32(GetQueryStringValue("CountryCarrierID"));

            MASCNT_CarrierBE lstCarrier = oMASCNT_CarrierBAL.GetCarrierDetailsByIdBAL(oMASCNT_CarrierBE);

            //ucCountry.innerControlddlCountry.SelectedValue = Convert.ToString(lstCarrier.CountryID);
            if (lstCarrier != null) {
                ucCountry.innerControlddlCountry.SelectedIndex = ucCountry.innerControlddlCountry.Items.IndexOf(ucCountry.innerControlddlCountry.Items.FindByValue(lstCarrier.CountryID.ToString()));

                txtCarrier.Text = Server.HtmlDecode(lstCarrier.CarrierName);
                if (lstCarrier.IsNarrativeRequired)
                    cbIsNarrativeRequired.Checked = true;
                else
                    cbIsNarrativeRequired.Checked = false;
            }

            ucCountry.innerControlddlCountry.Enabled = false;
            txtCarrier.Enabled = false;
        }
        else {
            
            btnDelete.Visible = false;           
        }
    }

    #endregion
    #region Events

    
    protected void btnSave_Click(object sender, EventArgs e) {
        MASCNT_CarrierBE oMASCNT_CarrierBE = new MASCNT_CarrierBE();
        APPCNT_CarrierBAL oMASCNT_CarrierBAL = new APPCNT_CarrierBAL();

        oMASCNT_CarrierBE.CarrierName = Server.HtmlEncode( txtCarrier.Text.Trim());
        oMASCNT_CarrierBE.CountryID = Convert.ToInt32(ucCountry.innerControlddlCountry.SelectedItem.Value);
        oMASCNT_CarrierBE.IsNarrativeRequired = cbIsNarrativeRequired.Checked == true ? true : false;
        oMASCNT_CarrierBE.Action = "CheckExistance";
        List<MASCNT_CarrierBE> lstCarriers = oMASCNT_CarrierBAL.GetCarrierDetailsBAL(oMASCNT_CarrierBE);
        if (lstCarriers.Count > 0) {
            string errorMessage = WebCommon.getGlobalResourceValue("CarrierExists");
            Page.ClientScript.RegisterStartupScript(this.GetType(), "Message", "alert('" + errorMessage + "')", true);
        }
        else {
            if (GetQueryStringValue("CountryCarrierID") != null)
            {
                oMASCNT_CarrierBE.Action = "EditNarrative";
                oMASCNT_CarrierBE.CarrierID = Convert.ToInt32(GetQueryStringValue("CountryCarrierID"));
            }
            else

                oMASCNT_CarrierBE.Action = "Add";
            oMASCNT_CarrierBE.CountryID = Convert.ToInt32(ucCountry.innerControlddlCountry.SelectedItem.Value);
            oMASCNT_CarrierBAL.addEditCarrierDetailsBAL(oMASCNT_CarrierBE);
            EncryptQueryString("APPCNT_CarrierSetupOverview.aspx");
        }        
    }
    protected void btnDelete_Click(object sender, EventArgs e) {
        MASCNT_CarrierBE oMASCNT_CarrierBE = new MASCNT_CarrierBE();
        APPCNT_CarrierBAL oAPPCNT_CarrierBAL = new APPCNT_CarrierBAL();

        oMASCNT_CarrierBE.Action = "Edit";

        if (GetQueryStringValue("CountryCarrierID") != null)
        {
            oMASCNT_CarrierBE.CarrierID = Convert.ToInt32(GetQueryStringValue("CountryCarrierID"));

            oAPPCNT_CarrierBAL.addEditCarrierDetailsBAL(oMASCNT_CarrierBE);
        }

        EncryptQueryString("APPCNT_CarrierSetupOverview.aspx");
    }
    protected void btnBack_Click(object sender, EventArgs e) {
        EncryptQueryString("APPCNT_CarrierSetupOverview.aspx");
    }
    public override void CountrySelectedIndexChanged() {

    }

    public override void CountryPost_Load() {
        if (!IsPostBack) {           
            GetCarrier();
        }
    }

    #endregion
}



﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="APPCNT_ISPM15PalletCheck.aspx.cs" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master" Inherits="ModuleUI_Appointment_CountrySetting_APPCNT_ISPM15PalletCheck" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="../../../CommonUI/UserControls/ucAddButton.ascx" TagName="ucAddButton" TagPrefix="cc2" %>
<%@ Register Src="../../../CommonUI/UserControls/ucExportToExcel.ascx" TagName="ucExportToExcel" TagPrefix="cc2" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>

<%@ Register Src="~/CommonUI/UserControls/ucCountryISPM15.ascx" TagName="ucCountryISPM15" TagPrefix="cc2" %>
<%@ Register Src="~/CommonUI/UserControls/ucCountry.ascx" TagName="ucCountry" TagPrefix="cc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <style type="text/css">
        .row1 {
            color: #333333;
            background-color: #F7F6F3;
        }

        .row2 {
            color: #284775;
            background-color: #FFFFFF;
        }
    </style>

    <h2>
        <cc1:ucLabel ID="lblISPM15ForCountrys" runat="server"></cc1:ucLabel>
    </h2>

    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>

    <script type="text/javascript">
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequest);
        function EndRequest(sender, args) {
            if (args.get_error() == undefined) {
                $('input:submit[id$="btnAdd"]').addClass("add button");
            }
        }
        function myDelete() {
            return confirm('<%= DeleteMessage %>');
        }
    </script>

    <asp:HiddenField ID="hdnVendor" runat="server" />

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="formbox">

                <div class="button-row">
                    <cc2:ucAddButton ID="btnAdd" runat="server" NavigateUrl="APPCNT_ISPM15PalletCheck.aspx?value=1" CssClass="add button" />
                    <cc1:ucButton ID="btnExportToExcel" runat="server" CssClass="exporttoexcel" Text="Export To Excel" OnClick="btnExport_Click"
                        Width="109px" Height="20px" Visible="false" />
                </div>

                <br />
                <table width="100%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">

                    <tr id="grdVendorData" runat="server">
                        <td align="left">
                            <asp:GridView ID="grdVendorPalletChecking" Width="100%" runat="server" AutoGenerateColumns="false"
                                CssClass="grid" OnRowDataBound="grdVendorPalletChecking_RowDataBound">
                                <RowStyle CssClass="row1" />
                                <AlternatingRowStyle CssClass="row2" />
                                <EmptyDataTemplate>
                                    No Records Found
                                </EmptyDataTemplate>

                                <Columns>

                                    <asp:BoundField DataField="SourceCountryName" HeaderText="Country of Origin">
                                        <ItemStyle HorizontalAlign="left" />
                                        <HeaderStyle HorizontalAlign="left" />
                                    </asp:BoundField>

                                    <asp:BoundField DataField="DestinationCountryName" HeaderText="To Country">
                                        <ItemStyle HorizontalAlign="left" />
                                        <HeaderStyle HorizontalAlign="left" />
                                    </asp:BoundField>

                                    <asp:TemplateField HeaderText="ISPM15Check">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkVendorPallet" runat="server"
                                                Checked='<%# Convert.ToBoolean(Eval("ISPM15Check ") ) ? true:false %>'
                                                Enabled="false" />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>

                                    <asp:BoundField DataField="AddedBy" HeaderText="CreatedBy">
                                        <ItemStyle HorizontalAlign="left" />
                                        <HeaderStyle HorizontalAlign="left" />
                                    </asp:BoundField>

                                    <asp:BoundField DataField="ModifiedBy" HeaderText="ModifiedBy">
                                        <ItemStyle HorizontalAlign="left" />
                                        <HeaderStyle HorizontalAlign="left" />
                                    </asp:BoundField>

                                    <asp:BoundField DataField="ModifiedDate" HeaderText="ModifiedDate">
                                        <ItemStyle HorizontalAlign="left" />
                                        <HeaderStyle HorizontalAlign="left" />
                                    </asp:BoundField>

                                    <asp:TemplateField HeaderText="Action">
                                        <ItemTemplate>
                                            <asp:LinkButton Text="Edit" ID="lnkEdit" runat="server" CommandArgument='<%# Eval("ID") %>' OnClick="lnkRemove_Click" />
                                            <asp:HiddenField ID="hdnID" runat="server" Value='<%# Eval("ID") %>' />
                                            <asp:HiddenField ID="hdnSourceCountryID" runat="server" Value='<%# Eval("SourceCountryID") %>' />
                                            <asp:HiddenField ID="hdnDestinationCountryID" runat="server" Value='<%# Eval("DestinationCountryID") %>' />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="left" />
                                        <HeaderStyle HorizontalAlign="left" />
                                    </asp:TemplateField>

                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>

                    <tr id="pager" runat="server">
                        <td colspan="4">
                            <cc1:PagerV2_8 ID="pager1" runat="server" OnCommand="pager_Command" GenerateGoToSection="true"></cc1:PagerV2_8>
                        </td>
                    </tr>

                    <tr id="trVendorPalletChecking" runat="server" visible="false">
                        <td>
                            <cc1:ucPanel ID="pnlCountryPalletChecking" runat="server" GroupingText="Country Pallet Checking"
                                CssClass="fieldset-form">
                                <table width="95%" cellspacing="5" cellpadding="0" border="0"
                                    align="center" class="top-settingsNoBorder">
                                    <tr>
                                        <td colspan="5">
                                            <asp:HiddenField ID="hdnSeletedVendorID" runat="server" Value="0" />
                                        </td>
                                    </tr>
                                    <tr>

                                        <td>
                                            <cc1:ucLabel ID="lblSourceCountry" runat="server" Text="Country of Origin" isRequired="true"></cc1:ucLabel>
                                        </td>
                                        <td>:
                                        </td>
                                        <td>
                                            <cc2:ucCountryISPM15 runat="server" ID="ddlSourceCountry" />
                                        </td>
                                        <td>
                                            <cc1:ucLabel ID="lblDestinationCountry" runat="server" Text="Destination Country" isRequired="true"></cc1:ucLabel>
                                        </td>
                                        <td>:
                                        </td>
                                        <td>
                                            <cc2:ucCountry runat="server" ID="ddlDestinationCountry" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <cc1:ucLabel ID="lblISPM15" runat="server" Text="ISPM15Check" isRequired="true"></cc1:ucLabel>
                                        </td>
                                        <td>:
                                        </td>
                                        <td>
                                            <cc1:ucRadioButtonList ID="chkPalletChecking" runat="server" AutoPostBack="true" Width="300px"
                                                RepeatDirection="Horizontal">
                                                <asp:ListItem Value="1" Selected="True"> YES</asp:ListItem>
                                                <asp:ListItem Value="2"> NO </asp:ListItem>
                                            </cc1:ucRadioButtonList>
                                        </td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                </table>
                            </cc1:ucPanel>
                        </td>
                    </tr>

                    <tr id="trVendorPalletCheckingButton" runat="server" visible="false">
                        <td colspan="3" align="right">
                            <cc1:ucButton ID="btnBack" runat="server" CssClass="button"
                                OnClick="btnBack_Click" />
                            <cc1:ucButton ID="btnSave" runat="server" CssClass="button" ValidationGroup="CheckVendor"
                                OnClientClick="return mySave();"
                                OnClick="BtnSave_Click" />
                            <cc1:ucButton ID="btnDelete" runat="server" CssClass="button" OnClientClick="return myDelete();"
                                OnClick="btnDelete_Click" />
                            <asp:ValidationSummary ID="vSummary" runat="server" ShowMessageBox="true" ShowSummary="false"
                                Style="color: Red" ValidationGroup="CheckVendor" />
                        </td>
                    </tr>
                </table>
            </div>

        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnBack" />
            <asp:PostBackTrigger ControlID="btnSave" />
            <asp:PostBackTrigger ControlID="btnExportToExcel" />
        </Triggers>
    </asp:UpdatePanel>

</asp:Content>

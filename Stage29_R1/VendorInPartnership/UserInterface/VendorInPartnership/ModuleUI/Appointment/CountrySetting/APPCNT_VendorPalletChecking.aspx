﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="APPCNT_VendorPalletChecking.aspx.cs"
    MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master" Inherits="ModuleUI_Appointment_CountrySetting_APPCNT_VendorPalletChecking" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="../../../CommonUI/UserControls/ucAddButton.ascx" TagName="ucAddButton"
    TagPrefix="cc2" %>
<%@ Register Src="../../../CommonUI/UserControls/ucExportToExcel.ascx" TagName="ucExportToExcel"
    TagPrefix="cc2" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<%@ Register Src="~/CommonUI/UserControls/ucSite.ascx" TagPrefix="cc2" TagName="ucSite" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <style type="text/css">
        .row1 {
            color: #333333;
            background-color: #F7F6F3;
        }

        .row2 {
            color: #284775;
            background-color: #FFFFFF;
        }
    </style>
    <h2>
        <cc1:ucLabel ID="lblVendorPalletChecking" runat="server"></cc1:ucLabel>
    </h2>
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>

    <script type="text/javascript">
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequest);
        function EndRequest(sender, args) {
            if (args.get_error() == undefined) {
                $('input:submit[id$="btnAdd"]').addClass("add button");
            }
        }

        function checkVendorSelected(source, args) {
            if (document.getElementById('<%=lstLeft.ClientID%>') != null) {
                var obj = document.getElementById('<%=lstLeft.ClientID%>');
                if (obj.value == '') {
                    args.IsValid = false;
                }
            }
        }

        function ShowSelectedVendorName(type) {
            var txtObj = $('#<%=txtSelectedVendorValue.ClientID%>');
            var lstObj = $('#<%=lstLeft.ClientID%> option:selected');
            var hdnID = $('#<%=hdnSeletedVendorID.ClientID%>');
            var hdnVendor = $('#<%=hdnVendor.ClientID%>');
            if (lstObj.text() != '') {
                $(txtObj).val(lstObj.text());
                $(hdnVendor).val(lstObj.text());
                $(hdnID).val(lstObj.val());
                $('#<%=btnHidden.ClientID %>').click();
            }
        }

        function RemoveSelectedVendorName(type) {
            var txtObj = $('#<%=txtSelectedVendorValue.ClientID%>');
            $('#<%=lstLeft.ClientID%>').find("option").attr("selected", false);
            var hdnID = $('#<%=hdnSeletedVendorID.ClientID%>');
            $(txtObj).val('');
            $(hdnID).val('0');
        }

        function myDelete() {
            return confirm('<%= DeleteMessage %>');            
        }
        
        function mySave() {
            if ($(this).val() == "Save") {
                $.ajax({
                    type: "POST",
                    url: "APPCNT_VendorPalletChecking.aspx/GetData",
                    data: '{VendorID:' + $('#ctl00_ContentPlaceHolder1_hdnSeletedVendorID').val() + '}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        if (data.d == "1") {
                            debugger;
                            var x = confirm('<%= VendorPalletCheckingAlreadyExists %>');
                            if (x) {
                                $('#<%=btnErrContinue.ClientID %>').click();
                                window.location.reload();
                                return true;
                            }
                            else {
                                return false;
                            }
                        }
                    },
                    error: function (data) {
                        console.log(data);
                    }
                });
            }
            return true;
        }        
    </script>

    <asp:HiddenField ID="hdnVendor" runat="server" />

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="formbox">
                <div class="button-row">
                    <cc2:ucAddButton ID="btnAdd" runat="server" NavigateUrl="APPCNT_VendorPalletChecking.aspx?value=1" CssClass="add button" />
                    <cc1:ucButton ID="btnExportToExcel" runat="server" CssClass="exporttoexcel" Text="Export To Excel" OnClick="btnExport_Click"
                        Width="109px" Height="20px" Visible="false" />

                </div>
                <br />
                <table width="100%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                    <tr id="grdVendorData" runat="server">
                        <td align="left">
                            <asp:GridView ID="grdVendorPalletChecking" Width="100%" runat="server" AutoGenerateColumns="false"
                                CssClass="grid" OnRowDataBound="grdVendorPalletChecking_RowDataBound">
                                <RowStyle CssClass="row1" />
                                <AlternatingRowStyle CssClass="row2" />
                                <EmptyDataTemplate>
                                    No Records Found
                                </EmptyDataTemplate>
                                <Columns>
                                    <asp:BoundField DataField="Country" HeaderText="Country">
                                        <ItemStyle HorizontalAlign="left" />
                                        <HeaderStyle HorizontalAlign="left" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="SiteName" HeaderText="Site">
                                        <ItemStyle HorizontalAlign="left" />
                                        <HeaderStyle HorizontalAlign="left" />
                                    </asp:BoundField>

                                    <asp:TemplateField HeaderText="Vendor">
                                        <ItemTemplate>
                                            <asp:Label ID="lblVendor" runat="server" Text='<%# Eval("Vendor") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="left" />
                                        <HeaderStyle HorizontalAlign="left" />
                                    </asp:TemplateField>

                                    <asp:BoundField DataField="Created On" HeaderText="Created On">
                                        <ItemStyle HorizontalAlign="left" />
                                        <HeaderStyle HorizontalAlign="left" />
                                    </asp:BoundField>

                                    <asp:TemplateField HeaderText="Pallet Check for ISPM15">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkVendorPallet" runat="server"
                                                Checked='<%# Eval("PalletChecking ").ToString() == "1" ? true:false %>'
                                                Enabled="false" />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Standard Pallet Check">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkStandard" runat="server"
                                                Checked='<%# Eval("PalletChecking ").ToString() == "2" ? true:false %>'
                                                Enabled="false" />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Action">
                                        <ItemTemplate>
                                            <asp:LinkButton Text="Edit" ID="lnkEdit" runat="server"
                                                CommandArgument='<%# Eval("Vendorid") %>' OnClick="lnkRemove_Click" />
                                            <asp:HiddenField ID="hdnSiteID" runat="server" Value='<%# Eval("SiteID") %>' />
                                            <asp:HiddenField ID="hdnSiteCountryID" runat="server" Value='<%# Eval("SiteCountryID") %>' />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="left" />
                                        <HeaderStyle HorizontalAlign="left" />
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr id="pager" runat="server">
                        <td colspan="4">
                            <cc1:PagerV2_8 ID="pager1" runat="server" OnCommand="pager_Command" GenerateGoToSection="true"></cc1:PagerV2_8>
                        </td>
                    </tr>
                    <tr id="trVendorPalletChecking" runat="server" visible="false">
                        <td>
                            <cc1:ucPanel ID="pnlVendorPalletChecking" runat="server" GroupingText="Vendor Pallet Checking"
                                CssClass="fieldset-form">
                                <table width="95%" cellspacing="5" cellpadding="0" border="0"
                                    align="center" class="top-settingsNoBorder">
                                    <tr>
                                        <td colspan="5">
                                            <asp:HiddenField ID="hdnSeletedVendorID" runat="server" Value="0" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 2%"></td>
                                        <td>
                                            <cc1:ucLabel ID="lblVendor" runat="server" Text="Vendor" isRequired="true"></cc1:ucLabel>
                                        </td>
                                        <td>:
                                        </td>
                                        <td>
                                            <table border="0">
                                                <tr>
                                                    <td style="width: 40%">
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <cc1:ucTextbox ID="txtVendorNo" runat="server" MaxLength="15" Width="77px"></cc1:ucTextbox>
                                                                </td>
                                                                <td>
                                                                    <cc1:ucButton CssClass="button" runat="server" ID="btnSearchVendor" OnClick="btnSearch_Click" />&nbsp;&nbsp;
                                                                </td>
                                                                <td>
                                                                    <cc1:ucButton CssClass="button" runat="server" ID="btnSearchVendorNumber" OnClick="btnSearchByVendorNo_Click" />
                                                                </td>

                                                            </tr>
                                                        </table>
                                                    </td>
                                                    <td style="width: 30%;" align="center"></td>
                                                    <td style="width: 30%"></td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <cc1:ucListBox ID="lstLeft" runat="server" Height="150px" Width="370px">
                                                        </cc1:ucListBox>
                                                    </td>
                                                    <td align="center">
                                                        <div>
                                                            <input type="button" id="btnMoveRight" value=">" class="button" style="width: 35px"
                                                                onclick="Javascript: ShowSelectedVendorName('Local');"
                                                                runat="server" />
                                                            <asp:Button ID="btnHidden" runat="server" OnClick="btnHidden_Click" Style="display: none;" />
                                                        </div>
                                                        &nbsp;
                                                            <div>
                                                                <input type="button" id="btnMoveLeft" value="<" class="button" style="width: 35px"
                                                                    onclick="Javascript: RemoveSelectedVendorName('Local');"
                                                                    runat="server" />
                                                            </div>
                                                    </td>
                                                    <td>&nbsp;
                                                    </td>
                                                </tr>
                                            </table>
                                            <asp:CustomValidator ID="cusvNoSelectedVendor" runat="server" ClientValidationFunction="checkVendorSelected"
                                                ValidationGroup="CheckVendor" Display="None"></asp:CustomValidator>
                                        </td>
                                        <td>
                                            <table>
                                                <tr>
                                                    <td style="height: 48px">
                                                        <cc1:ucTextbox ID="txtSelectedVendorValue" runat="server" Font-Bold="true" ReadOnly="true"></cc1:ucTextbox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <cc1:ucRadioButtonList ID="chkPalletChecking" runat="server" AutoPostBack="true" Width="300px"
                                                            RepeatDirection="Horizontal" OnSelectedIndexChanged="chkPalletChecking_SelectedIndexChanged">
                                                            <asp:ListItem Value="1" Selected="True"> Pallet Check for ISPM15</asp:ListItem>
                                                            <asp:ListItem Value="2"> Standard Pallet Checking</asp:ListItem>
                                                        </cc1:ucRadioButtonList>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <br />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <table id="sitetable" runat="server">
                                                            <tr>
                                                                <td style="font-weight: bold; width: 5%">
                                                                    <cc1:ucLabel ID="lblSite" runat="server" Text="Site"></cc1:ucLabel>
                                                                </td>
                                                                <td>:</td>
                                                                <td>
                                                                    <cc2:ucSite ID="ddlSite" runat="server" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </cc1:ucPanel>

                        </td>
                    </tr>
                    <tr id="trVendorPalletCheckingButton" runat="server" visible="false">
                        <td colspan="3" align="right">
                            <cc1:ucButton ID="btnBack" runat="server" CssClass="button"
                                OnClick="btnBack_Click" />
                            <cc1:ucButton ID="btnSave" runat="server" CssClass="button" ValidationGroup="CheckVendor"
                                OnClientClick="return mySave();"
                                OnClick="BtnSave_Click" />
                            <cc1:ucButton ID="btnDelete" runat="server" CssClass="button" OnClientClick="return myDelete();"
                                OnClick="btnDelete_Click" />
                            <asp:ValidationSummary ID="vSummary" runat="server" ShowMessageBox="true" ShowSummary="false"
                                Style="color: Red" ValidationGroup="CheckVendor" />
                        </td>
                    </tr>
                </table>
            </div>

        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnBack" />
            <asp:PostBackTrigger ControlID="btnSave" />
            <asp:PostBackTrigger ControlID="btnExportToExcel" />
        </Triggers>
    </asp:UpdatePanel>

    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
        <ContentTemplate>
            <asp:Button ID="btnQueryMsg" runat="Server" Style="display: none" />
            <ajaxToolkit:ModalPopupExtender ID="mdlQueryMsg" runat="server" TargetControlID="btnQueryMsg"
                PopupControlID="pnlQueryMsg" BackgroundCssClass="modalBackground" BehaviorID="QueryMsg"
                DropShadow="false" />
            <asp:Panel ID="pnlQueryMsg" runat="server" Style="display: none;">
                <div style="overflow-y: hidden; overflow-x: hidden; background-color: #fff; padding: 5px; border: 2px solid #ccc; text-align: left;">
                    <table cellspacing="5" cellpadding="0" border="0" align="center" class="top-setting-Popup">
                        <tr>
                            <td style="font-weight: bold;">
                                <cc1:ucLiteral ID="ltConfirmMsg" runat="server" Text=""></cc1:ucLiteral>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <cc1:ucButton ID="btnErrContinue" runat="server" Text="CONTINUE" CssClass="button"
                                    OnCommand="btnContinue_Click" />
                                &nbsp;
                                    <cc1:ucButton ID="btnErrBack" runat="server" Text="BACK" CssClass="button"
                                        OnCommand="btnBack_Click" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnErrContinue" />
            <asp:AsyncPostBackTrigger ControlID="btnErrBack" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>

﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using Utilities; using WebUtilities;
using BusinessEntities.ModuleBE.Appointment.CountrySetting;
using BusinessLogicLayer.ModuleBAL.Appointment.CountrySetting;
using BaseControlLibrary;


public partial class APPCNT_DoorTypeSetupEdit : CommonPage {
    protected string deleteMessage = WebCommon.getGlobalResourceValue("DeleteMessage");
    protected void Page_Init(object sender, EventArgs e) {
        ddlCountry.CurrentPage = this;
    }

    protected void Page_Load(object sender, EventArgs e) {
    }

    #region Methods
    public override void CountryPost_Load() {
        if (!IsPostBack) {
            GetDoorType();
        }
    }

    private void GetDoorType() {

        MASCNT_DoorTypeBE oMASCNT_DoorTypeBE = new MASCNT_DoorTypeBE();
        APPCNT_DoorTypeBAL oMASCNT_DoorTypeBAL = new APPCNT_DoorTypeBAL();

        if (GetQueryStringValue("CountryDoorTypeID") != null) {
            btnSave.Visible = false;
            oMASCNT_DoorTypeBE.Action = "ShowById";
            oMASCNT_DoorTypeBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
            oMASCNT_DoorTypeBE.User.UserID = Convert.ToInt32(Session["UserID"]);
            oMASCNT_DoorTypeBE.DoorTypeID = Convert.ToInt32(GetQueryStringValue("CountryDoorTypeID").ToString());
            MASCNT_DoorTypeBE lstDoorType = oMASCNT_DoorTypeBAL.GetDoorTypeDetailsByIdBAL(oMASCNT_DoorTypeBE);

            ddlCountry.innerControlddlCountry.SelectedValue = Convert.ToString(lstDoorType.CountryID);
            txtDoorType.Text = lstDoorType.DoorType;

            ddlCountry.innerControlddlCountry.Enabled = false;
            txtDoorType.Enabled = false;
        }
        else {
            btnDelete.Visible = false;
        }
    }
    #endregion

    #region Events


    protected void btnSave_Click(object sender, EventArgs e) {
        MASCNT_DoorTypeBE oMASCNT_DoorTypeBE = new MASCNT_DoorTypeBE();
        APPCNT_DoorTypeBAL oMASCNT_DoorTypeBAL = new APPCNT_DoorTypeBAL();

        oMASCNT_DoorTypeBE.DoorType = txtDoorType.Text.Trim();
        oMASCNT_DoorTypeBE.CountryID = Convert.ToInt32(ddlCountry.innerControlddlCountry.SelectedItem.Value);

        oMASCNT_DoorTypeBE.Action = "CheckExistance";
        List<MASCNT_DoorTypeBE> lstDoorTypes = oMASCNT_DoorTypeBAL.GetDoorTypeDetailsBAL(oMASCNT_DoorTypeBE);
        if (lstDoorTypes != null && lstDoorTypes.Count > 0) {
            string errorMessage = WebCommon.getGlobalResourceValue("DoorTypeExists");
            Page.ClientScript.RegisterStartupScript(this.GetType(), "Message", "alert('" + errorMessage + "')", true);
        }
        else {
            oMASCNT_DoorTypeBE.Action = "Add";
            oMASCNT_DoorTypeBAL.addEditDoorTypeDetailsBAL(oMASCNT_DoorTypeBE);
            EncryptQueryString("APPCNT_DoorTypeSetupOverview.aspx");
        }
    }
    protected void btnDelete_Click(object sender, EventArgs e) {
        MASCNT_DoorTypeBE oMASCNT_DoorTypeBE = new MASCNT_DoorTypeBE();
        APPCNT_DoorTypeBAL oMASCNT_DoorTypeBAL = new APPCNT_DoorTypeBAL();

        oMASCNT_DoorTypeBE.Action = "Edit";

        if (GetQueryStringValue("CountryDoorTypeID") != null) {
            oMASCNT_DoorTypeBE.DoorTypeID = Convert.ToInt32(GetQueryStringValue("CountryDoorTypeID"));

            oMASCNT_DoorTypeBAL.addEditDoorTypeDetailsBAL(oMASCNT_DoorTypeBE);
        }

        EncryptQueryString("APPCNT_DoorTypeSetupOverview.aspx");
    }
    protected void btnBack_Click(object sender, EventArgs e) {
        EncryptQueryString("APPCNT_DoorTypeSetupOverview.aspx");
    }
    #endregion
}
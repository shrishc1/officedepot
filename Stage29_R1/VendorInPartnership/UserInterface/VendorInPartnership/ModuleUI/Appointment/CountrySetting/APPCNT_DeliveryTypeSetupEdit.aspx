﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master" CodeFile="APPCNT_DeliveryTypeSetupEdit.aspx.cs" Inherits="APPCNT_DeliveryTypeSetupEdit" %>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register src="~/CommonUI/UserControls/ucCountry.ascx" tagname="ucCountry" tagprefix="cc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<script language="javascript" type="text/javascript">
    function confirmDelete() {
        return confirm('<%=deleteMessage%>');
    }
</script>
    <h2>
        <cc1:ucLabel ID="lblDeliveryTypeSetup" runat="server"  Text="Delivery Type Setup"></cc1:ucLabel>
    </h2>
    <div class="right-shadow">
        <div>            
            <asp:RequiredFieldValidator ID="rfvDeliveryTypeRequired" 
            runat="server" 
            ControlToValidate="txtDeliveryType" 
            Display="None" ValidationGroup="a">
            </asp:RequiredFieldValidator>
            <asp:ValidationSummary ID="vSummary" runat="server" ShowMessageBox="true" ShowSummary="false"
            style="color:Red" ValidationGroup="a"/>
        </div>
        <div class="formbox">
            <table width="45%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">                     
                <tr>            
                    <td style="width:5%"></td>
                    <td style="font-weight: bold; width:25%">
                        <cc1:ucLabel ID="lblCountry" runat="server" Text="Country" isRequired="true"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold;width:5%" align="center">
                        :
                    </td>
                    <td style="width:65%">
                        <cc2:ucCountry ID="ddlCountry" runat="server" />
                    </td>                     
                </tr>
                <tr>            
                    <td></td>
                    <td style="font-weight: bold;">
                        <cc1:ucLabel ID="lblDeliveryType" runat="server" Text="Delivery Type" isRequired="true"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold;" align="center">
                        :
                    </td>
                    <td>
                         <cc1:ucTextbox ID="txtDeliveryType" runat="server" Width="220px" MaxLength="30"></cc1:ucTextbox>
                    </td>                    
                </tr>      
                 <tr> 
                    <td></td>
                     <td style="font-weight: bold">
                        <cc1:ucLabel ID="lblEnableAsVendor" runat="server"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold" align="center">
                        :
                    </td>
                    <td>
                        <cc1:ucCheckbox ID="chkEnableAsVendorCarrier" runat="server" />
                    </td>                                  
                  
                                      
                </tr>                                             
            </table>   
        </div>
    </div>
    <div class="bottom-shadow"></div>   
    <div class="button-row">
        <cc1:ucButton ID="btnSave" runat="server" Text="Save"  CssClass="button" 
            onclick="btnSave_Click" ValidationGroup="a"/>
        <cc1:ucButton ID="btnDelete" runat="server" Text="Delete" 
           OnClientClick="return confirmDelete();" CssClass="button" onclick="btnDelete_Click"/>        
        <cc1:ucButton ID="btnBack" runat="server" Text="Back" CssClass="button" 
            onclick="btnBack_Click"/> 
    </div>
</asp:Content>
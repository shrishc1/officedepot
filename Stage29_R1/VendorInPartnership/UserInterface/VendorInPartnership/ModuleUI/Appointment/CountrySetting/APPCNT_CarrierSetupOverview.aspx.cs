﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using System.Data;

using BusinessEntities.ModuleBE.Appointment.CountrySetting;
using BusinessLogicLayer.ModuleBAL.Appointment.CountrySetting;
using Utilities; using WebUtilities;
using BaseControlLibrary;

public partial class APPCNT_CarrierSetupOverview : CommonPage {

    protected void Page_Prerender(object sender, EventArgs e) {        
                 
    }

    protected void Page_Load(object sender, EventArgs e) {

        if (!IsPostBack) {
            BindRegionCarrier();
        }
        ucExportToExcel1.GridViewControl = UcGridView1;
        ucExportToExcel1.FileName = "CarriersOverview";
    }
        
    #region Methods

    protected void BindRegionCarrier() {
        MASCNT_CarrierBE oMASCNT_CarrierBE = new MASCNT_CarrierBE();
        APPCNT_CarrierBAL oMASCNT_CarrierBAL = new APPCNT_CarrierBAL();

        oMASCNT_CarrierBE.Action = "ShowAll";
        oMASCNT_CarrierBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
        oMASCNT_CarrierBE.User.UserID = Convert.ToInt32(Session["UserID"]);

        List<MASCNT_CarrierBE> lstCarrier = oMASCNT_CarrierBAL.GetCarrierDetailsBAL(oMASCNT_CarrierBE);

        if (lstCarrier.Count > 0)
        {            
            UcGridView1.DataSource = lstCarrier;
            UcGridView1.DataBind();
            ViewState["lstSites"] = lstCarrier;
        }        
    }

    public override BusinessEntities.BaseBe[] SubSort(GridViewSortEventArgs e)
    {
        return Utilities.GenericListHelper<MASCNT_CarrierBE>.SortList((List<MASCNT_CarrierBE>)ViewState["lstSites"], e.SortExpression, e.SortDirection).ToArray();
    }

    #endregion    
}
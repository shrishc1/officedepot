﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using BusinessEntities.ModuleBE.Appointment.CountrySetting;
using BusinessLogicLayer.ModuleBAL.Appointment.CountrySetting;
using BaseControlLibrary;

public partial class APPCNT_DoorTypeSetupOverview : CommonPage {
    protected void Page_Load(object sender, EventArgs e) {
        if (!IsPostBack) {
            BindCountryDoorSetup();
        }
        btnExportToExcel1.GridViewControl = UcGridView1;
        btnExportToExcel1.FileName = "DoorTypeSetupOverview";
    }

    #region Methods

    protected void BindCountryDoorSetup()
    {
        MASCNT_DoorTypeBE oMASCNT_DoorTypeBE = new MASCNT_DoorTypeBE();
        APPCNT_DoorTypeBAL oMASCNT_DoorTypeBAL = new APPCNT_DoorTypeBAL();

        oMASCNT_DoorTypeBE.Action = "ShowAll";
        oMASCNT_DoorTypeBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
        oMASCNT_DoorTypeBE.User.UserID = Convert.ToInt32(Session["UserID"]);

        List<MASCNT_DoorTypeBE> lstDoorType = oMASCNT_DoorTypeBAL.GetDoorTypeDetailsBAL(oMASCNT_DoorTypeBE);

        if (lstDoorType.Count > 0) {
            UcGridView1.DataSource = lstDoorType;
            UcGridView1.DataBind();
            ViewState["lstSites"] = lstDoorType;
        }
    }

    public override BusinessEntities.BaseBe[] SubSort(GridViewSortEventArgs e)
    {
        return Utilities.GenericListHelper<MASCNT_DoorTypeBE>.SortList((List<MASCNT_DoorTypeBE>)ViewState["lstSites"], e.SortExpression, e.SortDirection).ToArray();
    }
  
    #endregion
}
﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    CodeFile="APPCNT_DeliveryTypeSetupOverview.aspx.cs" Inherits="APPCNT_DeliveryTypeSetupOverview" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register src="../../../CommonUI/UserControls/ucAddButton.ascx" tagname="ucAddButton" tagprefix="cc2" %>
<%@ Register src="../../../CommonUI/UserControls/ucExportToExcel.ascx" tagname="ucExportToExcel" tagprefix="cc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2>
        <cc1:ucLabel ID="lblDeliveryTypeSetup" runat="server" Text="Delivery Type Setup"></cc1:ucLabel>
    </h2>
    <div class="button-row">
        <cc2:ucAddButton ID="btnAdd" runat="server" NavigateUrl="APPCNT_DeliveryTypeSetupEdit.aspx" />
        <cc2:ucExportToExcel ID="ucExportToExcel1" runat="server" />
    </div>
    <table width="100%">
        <tr>
            <td align="center" >
                <cc1:ucGridView ID="UcGridView1" Width="100%" runat="server" CssClass="grid" onsorting="SortGrid" AllowSorting="true">
                    <Columns>                        
                        <asp:TemplateField HeaderText="Country" SortExpression="Country.CountryName">
                            <HeaderStyle Width="15%" HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <asp:Label ID="lblCountryName" runat="server" Text='<%#Eval("Country.CountryName") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                      

                          <asp:TemplateField HeaderText="Delivery Type" SortExpression="DeliveryType">
                            <HeaderStyle Width="15%" HorizontalAlign="Left" />
                            <ItemTemplate>
                                <asp:HyperLink ID="hpDelivery" runat="server" Text='<%# Eval("DeliveryType") %>'  NavigateUrl='<%# EncryptQuery("APPCNT_DeliveryTypeSetupEdit.aspx?CountryDeliveryTypeID="+ Eval("DeliveryTypeID")) %>'></asp:HyperLink>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:TemplateField> 
                           
                         <asp:TemplateField HeaderText="Enable" SortExpression="IsEnabledAsVendorOption">
                            <HeaderStyle Width="70%" HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <asp:Label ID="lblEnableAsVendor" runat="server" Text='<%# Eval("IsEnabledAsVendorOption").ToString() == "True" ?"Yes":"No" %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>                                          
                        
                       
                    </Columns>
                </cc1:ucGridView>
            </td>
        </tr>        
    </table>
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
</asp:Content>

﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using BusinessEntities.ModuleBE.Appointment.CountrySetting;
using BusinessLogicLayer.ModuleBAL.Appointment.CountrySetting;
using BaseControlLibrary;

public partial class APPCNT_DeliveryTypeSetupOverview : CommonPage {
    protected void Page_Load(object sender, EventArgs e) {
        if (!IsPostBack) {
            BindRegionDeliveryTypeSetup();
        }
        ucExportToExcel1.GridViewControl = UcGridView1;
        ucExportToExcel1.FileName = "DeliveryTypeSetupOverview";
    }

    #region Methods

    protected void BindRegionDeliveryTypeSetup() {
        MASCNT_DeliveryTypeBE oMASCNT_DeliveryTypeBE = new MASCNT_DeliveryTypeBE();
        APPCNT_DeliveryTypeBAL oMASCNT_DeliveryTypeBAL = new APPCNT_DeliveryTypeBAL();

        oMASCNT_DeliveryTypeBE.Action = "ShowAll";
        oMASCNT_DeliveryTypeBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
        oMASCNT_DeliveryTypeBE.User.UserID = Convert.ToInt32(Session["UserID"]);

        List<MASCNT_DeliveryTypeBE> lstDeliveryType = oMASCNT_DeliveryTypeBAL.GetDeliveryTypeDetailsBAL(oMASCNT_DeliveryTypeBE);

        if (lstDeliveryType.Count > 0) {
            UcGridView1.DataSource = lstDeliveryType;
            UcGridView1.DataBind();
            ViewState["lstSites"] = lstDeliveryType;
        }
    }

    public override BusinessEntities.BaseBe[] SubSort(GridViewSortEventArgs e)
    {
        return Utilities.GenericListHelper<MASCNT_DeliveryTypeBE>.SortList((List<MASCNT_DeliveryTypeBE>)ViewState["lstSites"], e.SortExpression, e.SortDirection).ToArray();
    }

    #endregion
}
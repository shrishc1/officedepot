﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebUtilities;
using BusinessEntities.ModuleBE.Appointment.CountrySetting;
using BusinessLogicLayer.ModuleBAL.Appointment.CountrySetting;

public partial class ModuleUI_Appointment_CountrySetting_APPCNT_AutoEmailPOToVendorEdit :  CommonPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
         if (!Page.IsPostBack)
        {
            ltCountry.Text = GetQueryStringValue("CountryName");
            string Status=GetQueryStringValue("IsAutoEmailNewPO");
            if (Status.Equals("True"))
            {
                rdoYes.Checked = true;
                rdoNo.Checked = false;
            }
            else
            {
                rdoNo.Checked = true;
                rdoYes.Checked = false;
            }
        }

    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        MASCNT_AutoEmailBE AutoEmailBE = new MASCNT_AutoEmailBE();
        APPCNT_AutoEmailBAL AutoEmailBAL = new APPCNT_AutoEmailBAL();
        DateTime myDateTime = DateTime.Now;
        string sqlFormattedDate = myDateTime.Date.ToString("yyyy-MM-dd HH:mm:ss.fff");
        AutoEmailBE.Action = "SaveAutoEmailStatus";
        AutoEmailBE.Country = new BusinessEntities.ModuleBE.AdminFunctions.MAS_CountryBE();
        AutoEmailBE.ModifiedByID = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
        AutoEmailBE.Country.CountryID = Convert.ToInt32(GetQueryStringValue("CountryID"));
        AutoEmailBE.ModifiedByID.UserID = Convert.ToInt32(Session["UserID"]);
        if (rdoNo.Checked)
        {
            AutoEmailBE.IsAutoEmailNewPO = 0;
        }
        else
        {
            AutoEmailBE.IsAutoEmailNewPO = 1 ;
        }
            AutoEmailBE.ModifiedOn = sqlFormattedDate;
        AutoEmailBAL.SaveAutoEmailPOToVendorStatusBAL(AutoEmailBE);
        EncryptQueryString("APPCNT_AutoEmailPOToVendor.aspx");

    }
    protected void btnBack_Click(object sender, EventArgs e)
    {
        EncryptQueryString("APPCNT_AutoEmailPOToVendor.aspx");
    }
}
﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    CodeFile="APPCNT_ISPM15CountrySetupEdit.aspx.cs" Inherits="APPCNT_ISPM15CountrySetupEdit" ValidateRequest="false" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="../../../CommonUI/UserControls/ucCountry.ascx" TagName="ucCountry"
    TagPrefix="uc1" %>
<%@ Register Src="../../../CommonUI/UserControls/ucAddButton.ascx" TagName="ucAddButton"
    TagPrefix="cc2" %>
<%@ Register Src="../../../CommonUI/UserControls/ucExportToExcel.ascx" TagName="ucExportToExcel"
    TagPrefix="cc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script language="javascript" type="text/javascript">
        function confirmDelete() {
            return confirm('<%=deleteMessage%>');
        }
    </script>
    <h2>
        <cc1:ucLabel ID="lblISPM15Countrys" runat="server" Text="ISPM15 Countrys"></cc1:ucLabel>
    </h2>
     
    <asp:Panel ID="editPanel" runat="server">

        <div class="right-shadow">
            <div>
                <asp:RequiredFieldValidator ID="rfvCarrierRequired" runat="server" ControlToValidate="txtCountryName"
                    Display="None" ValidationGroup="a">
                </asp:RequiredFieldValidator>
                <asp:ValidationSummary ID="vSummary" runat="server" ShowMessageBox="true" ShowSummary="false"
                    Style="color: Red" ValidationGroup="a" />
            </div>
            <div class="formbox">
                <table width="100%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                    <tr>
                        <td></td>
                        <td style="font-weight: bold">
                            <cc1:ucLabel ID="lblCountryPalletCheck" runat="server" Text="Please provide a new country " isRequired="true"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;" align="center">:
                        </td>
                        <td>
                            <cc1:ucTextbox ID="txtCountryName" runat="server" Width="340px" MaxLength="50"></cc1:ucTextbox>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="bottom-shadow">
        </div>
        <div class="button-row">
            <cc1:ucButton ID="btnSave" runat="server" Text="Save" CssClass="button" OnClick="btnSave_Click"
                ValidationGroup="a" />
            <cc1:ucButton ID="btnDelete" runat="server" Text="Delete" CssClass="button" OnClick="btnDelete_Click"
                OnClientClick="return confirmDelete();" />
            <cc1:ucButton ID="btnBack" runat="server" Text="Back" CssClass="button" OnClick="btnBack_Click" />
        </div>

    </asp:Panel>
</asp:Content>

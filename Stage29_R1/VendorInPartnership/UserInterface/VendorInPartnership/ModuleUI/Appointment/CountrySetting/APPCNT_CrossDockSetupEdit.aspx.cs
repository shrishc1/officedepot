﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Linq;

using Utilities; using WebUtilities;
using BusinessEntities.ModuleBE.Appointment.CountrySetting;
using BusinessLogicLayer.ModuleBAL.Appointment.CountrySetting;
using System.Collections.Generic;
using BusinessEntities.ModuleBE.AdminFunctions;
using BusinessLogicLayer.ModuleBAL.AdminFunctions;
using BaseControlLibrary;


public partial class APPCNT_CrossDockSetupEdit : CommonPage {
    protected string deleteMessage = WebCommon.getGlobalResourceValue("DeleteMessage");
    protected void Page_Init(object sender, EventArgs e) {
        ucCountry.CurrentPage = this;
        ucSite.CurrentPage = this;
        ucDestinationSite.CurrentPage = this;
    }

    protected void Page_Load(object sender, EventArgs e) { }

    #region Methods

    private void GetCrossDock() {
        MASCNT_CrossDockBE oMASCNT_CrossDockBE = new MASCNT_CrossDockBE();
        APPCNT_CrossDockBAL oMASCNT_CrossDockBAL = new APPCNT_CrossDockBAL();

        if (GetQueryStringValue("CrossDockingID") != null) {
            btnSave.Visible = false;
            oMASCNT_CrossDockBE.Action = "ShowById";
            oMASCNT_CrossDockBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
            oMASCNT_CrossDockBE.User.UserID = Convert.ToInt32(Session["UserID"]);
            oMASCNT_CrossDockBE.CrossDockingID = Convert.ToInt32(GetQueryStringValue("CrossDockingID").ToString());

            MASCNT_CrossDockBE lstCrossDock = oMASCNT_CrossDockBAL.GetCrossDockDetailsByIdBAL(oMASCNT_CrossDockBE);
            
            ucCountry.innerControlddlCountry.SelectedValue = Convert.ToString(lstCrossDock.CountryID);
            ucSite.innerControlddlSite.SelectedValue = Convert.ToString(lstCrossDock.SiteID);
            ucDestinationSite.innerControlddlSite.SelectedValue = Convert.ToString(lstCrossDock.DestinationSiteID);

            ucCountry.innerControlddlCountry.Enabled = false;
            ucSite.innerControlddlSite.Enabled = false;
            ucDestinationSite.innerControlddlSite.Enabled = false;
        }
        else {
            btnDelete.Visible = false;
        }
    }


    public override bool SitePrePage_Load() {
        return false;
    }

    public override void CountryPost_Load() {
        if (!IsPostBack) {
            ucCountry.innerControlddlCountry.AutoPostBack = true;

            ucSite.CountryID = Convert.ToInt32(ucCountry.innerControlddlCountry.SelectedItem.Value);
            ucDestinationSite.CountryID = Convert.ToInt32(ucCountry.innerControlddlCountry.SelectedItem.Value);            
            
            if (GetQueryStringValue("CrossDockingID") != null && GetQueryStringValue("CountryID") != null)
            {
                ucSite.CountryID = Convert.ToInt32(GetQueryStringValue("CountryID"));
                ucDestinationSite.CountryID = Convert.ToInt32(GetQueryStringValue("CountryID"));            
            }            

            ucSite.innerControlddlSite.AutoPostBack = true;            
            ucSite.BindSites();
            ucDestinationSite.innerControlddlSite.AutoPostBack = true;            
            ucDestinationSite.BindSites();
            GetCrossDock();
        }
    }

    #endregion


    #region Events

    protected void btnSave_Click(object sender, EventArgs e) {
        MASCNT_CrossDockBE oMASCNT_CrossDockBE = new MASCNT_CrossDockBE();
        APPCNT_CrossDockBAL oMASCNT_CrossDockBAL = new APPCNT_CrossDockBAL();
        oMASCNT_CrossDockBE.Action = "CheckExistance";
        oMASCNT_CrossDockBE.CountryID = Convert.ToInt32(ucCountry.innerControlddlCountry.SelectedItem.Value);
        oMASCNT_CrossDockBE.SiteID = Convert.ToInt32(ucSite.innerControlddlSite.SelectedItem.Value);
        oMASCNT_CrossDockBE.DestinationSiteID = Convert.ToInt32(ucDestinationSite.innerControlddlSite.SelectedItem.Value);
        oMASCNT_CrossDockBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
        oMASCNT_CrossDockBE.User.UserID = Convert.ToInt32(Session["UserID"]);

        List<MASCNT_CrossDockBE> lstCrossDock = oMASCNT_CrossDockBAL.GetCrossDockDetailsBAL(oMASCNT_CrossDockBE);
        if (lstCrossDock != null && lstCrossDock.Count > 0) {
            string errorMessage = WebCommon.getGlobalResourceValue("SiteDestinationsiteCombinationExists");
            Page.ClientScript.RegisterStartupScript(this.GetType(), "Message", "alert('" + errorMessage + "')", true);
        }
        else {
            oMASCNT_CrossDockBE.Action = "Add";
            oMASCNT_CrossDockBAL.addEditCrossDockDetailsBAL(oMASCNT_CrossDockBE);
            EncryptQueryString("APPCNT_CrossDockSetupOverview.aspx");
        }        
    }
    protected void btnDelete_Click(object sender, EventArgs e) {
        MASCNT_CrossDockBE oMASCNT_CrossDockBE = new MASCNT_CrossDockBE();
        APPCNT_CrossDockBAL oMASCNT_CrossDockBAL = new APPCNT_CrossDockBAL();

        oMASCNT_CrossDockBE.Action = "Edit";

        if (GetQueryStringValue("CrossDockingID") != null) {
            oMASCNT_CrossDockBE.CrossDockingID = Convert.ToInt32(GetQueryStringValue("CrossDockingID"));

            oMASCNT_CrossDockBAL.addEditCrossDockDetailsBAL(oMASCNT_CrossDockBE);
        }
        EncryptQueryString("APPCNT_CrossDockSetupOverview.aspx");
    }
    protected void btnBack_Click(object sender, EventArgs e) {
        EncryptQueryString("APPCNT_CrossDockSetupOverview.aspx");
    }

    public override void CountrySelectedIndexChanged() {
        //BindSites(Convert.ToInt32(ucCountry.innerControlddlCountry.SelectedItem.Value));
        ucSite.CountryID = Convert.ToInt32(ucCountry.innerControlddlCountry.SelectedItem.Value);
        ucDestinationSite.CountryID = Convert.ToInt32(ucCountry.innerControlddlCountry.SelectedItem.Value);
        ucSite.BindSites();
        ucDestinationSite.BindSites();
    }

    //public void BindSites(int CountryID) {
    //    MAS_SiteBE oMAS_SiteBE = new MAS_SiteBE();
    //    MAS_SiteBAL oMAS_SiteBAL = new MAS_SiteBAL();

    //    oMAS_SiteBE.Action = "ShowAll";

    //    oMAS_SiteBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
    //    oMAS_SiteBE.User.UserID = Convert.ToInt32(Session["UserID"]);
    //    oMAS_SiteBE.CountryID = CountryID;

    //    List<MAS_SiteBE> lstSite = new List<MAS_SiteBE>();
    //    lstSite = oMAS_SiteBAL.GetSiteBAL(oMAS_SiteBE);

    //    if (lstSite.Count > 0) {
    //        ucSite.innerControlddlSite.DataSource = lstSite.ToList();
    //        ucSite.innerControlddlSite.DataTextField = "SiteDescription";
    //        ucSite.innerControlddlSite.DataValueField = "SiteID";
    //        ucSite.innerControlddlSite.DataBind();

    //        ucDestinationSite.innerControlddlSite.DataSource = lstSite.ToList();
    //        ucDestinationSite.innerControlddlSite.DataTextField = "SiteDescription";
    //        ucDestinationSite.innerControlddlSite.DataValueField = "SiteID";
    //        ucDestinationSite.innerControlddlSite.DataBind();   
    //    }
    //}


    #endregion        
}
﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    CodeFile="APPCNT_DoorTypeSetupOverview.aspx.cs" Inherits="APPCNT_DoorTypeSetupOverview" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="../../../CommonUI/UserControls/ucAddButton.ascx" TagName="ucAddButton"
    TagPrefix="cc2" %>
<%@ Register Src="../../../CommonUI/UserControls/ucExportToExcel.ascx" TagName="ucExportToExcel"
    TagPrefix="cc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h2>
        <cc1:ucLabel ID="lblDoorTypeSetup" runat="server" Text="Door Type Setup"></cc1:ucLabel>
    </h2>
    <div class="button-row">
        <cc2:ucAddButton ID="btnAdd" runat="server" NavigateUrl="APPCNT_DoorTypeSetupEdit.aspx" />
        <cc2:ucExportToExcel ID="btnExportToExcel1" runat="server" />
    </div>
    <table width="100%">
        <tr>
            <td align="center" colspan="9">
                <cc1:ucGridView ID="UcGridView1" Width="100%" runat="server" CssClass="grid"  onsorting="SortGrid" AllowSorting="true">
                    <Columns>
                        <asp:TemplateField HeaderText="Country Name" SortExpression="Country.CountryName">
                            <HeaderStyle Width="15%" HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <asp:Label ID="lblCountryName" runat="server" Text='<%#Eval("Country.CountryName") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                       
                          <asp:TemplateField HeaderText="Door Type" SortExpression="DoorType">
                            <HeaderStyle Width="10%" HorizontalAlign="Left" />
                            <ItemTemplate>
                                <asp:HyperLink ID="hpDelivery" runat="server" Text='<%# Eval("DoorType") %>'  NavigateUrl='<%# EncryptQuery("APPCNT_DoorTypeSetupEdit.aspx?CountryDoorTypeID="+ Eval("DoorTypeID")) %>'></asp:HyperLink>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:TemplateField> 
                       
                    </Columns>
                </cc1:ucGridView>
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
            </td>
            <td>
            </td>
            <td>
            </td>
            <td>
            </td>
            <td>
            </td>
            <td>
            </td>
            <td>
            </td>
            <td>
            </td>
        </tr>
    </table>
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
</asp:Content>

﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    AutoEventWireup="true" CodeFile="OverduePOReport.aspx.cs" Inherits="ModuleUI_Appointment_OverduePOReport"
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<%@ Register Src="../../CommonUI/UserControls/ucSite.ascx" TagName="ucSite" TagPrefix="uc2" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectSite.ascx" TagName="MultiSelectSite"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/ucExportToExcel.ascx" TagName="ucExportToExcel"
    TagPrefix="cc2" %>
<%@ Register Src="~/CommonUI/UserControls/ucVendorSelectionTemplate.ascx" TagName="VendorSelectTemplate"
    TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script type="text/javascript" language="javascript">
        function setValue1(target) {
            document.getElementById('<%=hdnJSFromDt.ClientID %>').value = target.value;
        }

        function setValue2(target) {
            document.getElementById('<%=hdnJSToDt.ClientID %>').value = target.value;
        }

    </script>
   <asp:HiddenField ID="hdnJSFromDt" runat="server" />
    <asp:HiddenField ID="hdnJSToDt" runat="server" />
    <asp:HiddenField ID="hdnSiteIDs" runat="server" />
    <asp:HiddenField ID="hdnCountryIDs" runat="server" />
    <asp:HiddenField ID="hdnVendorIDs" runat="server" />
    <asp:HiddenField ID="hdnCarrierIDs" runat="server" />
    <asp:HiddenField ID="hdnSupplierType" runat="server" />
    <asp:HiddenField ID="hdnFromDt" runat="server" />
    <asp:HiddenField ID="hdnToDt" runat="server" />
    <asp:ScriptManager ID="SM1" runat="server">
    </asp:ScriptManager>
    <h2>
        <cc1:ucLabel ID="lblOverduePOReport" runat="server" Text="Overdue PO Report"></cc1:ucLabel>
    </h2>
    <div class="right-shadow">
        <div class="formbox">
            <div class="right-shadow">
                <div class="formbox">
                    <cc1:ucPanel ID="pnlOverduePOReport" runat="server">
                        <table width="100%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                            <tr>
                                <td style="font-weight: bold; width: 81px">
                                    <cc1:ucLabel ID="lblDateFrom" runat="server" Text="Date From"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold; width: 1%">
                                    <cc1:ucLabel ID="UcLabel6" runat="server">:</cc1:ucLabel>
                                </td>
                                <td align="left" colspan="4">
                                    <table width="25%" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td style="font-weight: bold; width: 6em;">
                                                <cc1:ucTextbox ID="txtFromDate" runat="server" ClientIDMode="Static" CssClass="date"
                                                    onchange="setValue1(this)" ReadOnly="True" Width="70px" />
                                            </td>
                                            <td style="font-weight: bold; width: 4em; text-align: center">
                                                <cc1:ucLabel ID="lblTo" runat="server" Text="To"></cc1:ucLabel>
                                            </td>
                                            <td style="font-weight: bold; width: 6em;">
                                                <cc1:ucTextbox ID="txtToDate" runat="server" ClientIDMode="Static" CssClass="date"
                                                    onchange="setValue2(this)" ReadOnly="True" Width="70px" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="justify" style="font-weight: bold;">
                                    <cc1:ucLabel ID="lblSite" runat="server" Text="Site"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold;">
                                    <cc1:ucLabel ID="UcLabel4" runat="server">:</cc1:ucLabel>
                                </td>
                                <td align="left">
                                    <cc1:MultiSelectSite runat="server" ID="msSite" />
                                </td>
                            </tr>
                            <tr>
                                <td align="center" colspan="4">
                                    <div class="button-row">
                                        <cc1:ucButton ID="UcButton1" runat="server" Text="Generate Report" CssClass="button"
                                            OnClick="btnGenerateReport_Click" />
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </cc1:ucPanel>
                </div>
            </div>
        </div>
    </div>
    <div class="button-row">
        <cc2:ucExportToExcel ID="ucExportToExcel1" runat="server" />
    </div>
    <div style="width: 99%; overflow: auto" id="divPrint" class="fixedTable" onscroll="getScroll1(this);">
        <table cellspacing="1" cellpadding="0" class="form-table">
            <tr>
                <td>
                    <cc1:ucGridView ID="gvVendor" runat="server" AutoGenerateColumns="false" CssClass="grid gvclass"
                        CellPadding="0" Width="980px" DataKeyNames="PKID" OnSorting="gridView_Sorting"
                        AllowSorting="false" AllowPaging="true" PageSize="30" OnPageIndexChanging="gvVendor_PageIndexChanging"
                        >
                        <AlternatingRowStyle BackColor="White" ForeColor="#284775"></AlternatingRowStyle>
                        <EmptyDataTemplate>
                            <div style="text-align: center">
                                <cc1:ucLabel ID="lblNoData" runat="server" Text="No Records found."></cc1:ucLabel>
                            </div>
                        </EmptyDataTemplate>
                        <Columns>
                            <asp:TemplateField HeaderText="Date">
                                <HeaderStyle Width="200px" />
                                <ItemStyle HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <asp:HiddenField ID="hdnExpectedDeliveryDate" runat="server" Value='<%# Eval("DeliveryDate") %>' />
                                    <cc1:ucLabel ID="lblExpectedDeliveryDate" runat="server" Text='<%# Eval("DeliveryDate", "{0:dd/MM/yyyy}") %>'></cc1:ucLabel>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Time / Day">
                                <HeaderStyle Width="150px" />
                                <ItemStyle HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <asp:HiddenField ID="hdnTimeWindowID" runat="server" Value='<%# Eval("TimeWindowID") %>' />
                                    <cc1:ucLiteral ID="UcLiteral27_2" runat="server" Text='<%#Eval("NonWindowFromToTime") %>'
                                        Visible="false"></cc1:ucLiteral>
                                    <cc1:ucLiteral ID="UcLiteral27_1" runat="server" Text='<%#Eval("WindowStartEndTime") %>'
                                        Visible="false"></cc1:ucLiteral>
                                    <cc1:ucLiteral ID="UcLiteral27" runat="server" Text='<%#Eval("ExpectedDeliveryTime") %>'></cc1:ucLiteral>&nbsp;
                                    <cc1:ucLiteral ID="UcLiteral28" runat="server" Text='<%# Eval("DeliveryDate", "{0:dddd}")%>'></cc1:ucLiteral>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Site">
                                <HeaderStyle Width="120px" />
                                <ItemStyle HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <cc1:ucLabel ID="LBLSiteName" runat="server" Text='<%# Eval("SiteName") %>'></cc1:ucLabel>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Booking Ref #">
                                <HeaderStyle Width="170px" />
                                <ItemStyle HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <cc1:ucLiteral ID="ltHistory" runat="server" Text='<%#Eval("BookingRef") %>'></cc1:ucLiteral>
                                    <asp:HyperLink ID="hypHistory" runat="server" Text='<%#Eval("BookingRef") %>' NavigateUrl='<%# EncryptQuery("~/ModuleUI/Appointment/Receiving/APPRcv_BookingHistory.aspx?PN=BOINQ&Scheduledate=" + Eval("DeliveryDate", "{0:dd/MM/yyyy}") + "&ID=" + Eval("PKID") + "-V-" + Eval("PKID") + "-" +  Eval("PKID")) %>'></asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Vendor Name">
                                <HeaderStyle Width="200px" />
                                <ItemStyle HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <cc1:ucLiteral ID="UcLiteral2" runat="server" Text='<%#Eval("VendorName") %>'></cc1:ucLiteral>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Carrier Name">
                                <HeaderStyle Width="180px" />
                                <ItemStyle HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <cc1:ucLiteral ID="UcLiteral3" runat="server" Text='<%#Eval("CarrierName") %>'></cc1:ucLiteral>
                                </ItemTemplate>
                            </asp:TemplateField>
                          <%--  <asp:BoundField HeaderText="Status" Visible="false" DataField="BookingStatus">
                                <HeaderStyle Width="120px" />
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>--%>
                        </Columns>
                    </cc1:ucGridView>
                    <cc1:ucGridView ID="gvExport" runat="server" AutoGenerateColumns="false" Style="display: none;">
                        <Columns>
                            <asp:TemplateField HeaderText="Date">
                                <HeaderStyle Width="200px" />
                                <ItemStyle HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <asp:HiddenField ID="hdnExpectedDeliveryDate" runat="server" Value='<%# Eval("DeliveryDate") %>' />
                                    <cc1:ucLabel ID="lblExpectedDeliveryDate" runat="server" Text='<%# Eval("DeliveryDate", "{0:dd/MM/yyyy}") %>'></cc1:ucLabel>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Time / Day" SortExpression="Weekday">
                                <HeaderStyle Width="150px" HorizontalAlign="Left" />
                                <ItemStyle HorizontalAlign="Left" />
                                <ItemTemplate>
                                    <asp:HiddenField ID="hdnTimeWindowID" runat="server" Value='<%# Eval("TimeWindowID") %>' />
                                    <cc1:ucLiteral ID="UcLiteral27_2" runat="server" Text='<%#Eval("NonWindowFromToTime") %>'
                                        Visible="false"></cc1:ucLiteral>
                                    <cc1:ucLiteral ID="UcLiteral27_1" runat="server" Text='<%#Eval("WindowStartEndTime") %>'
                                        Visible="false"></cc1:ucLiteral>
                                    <cc1:ucLiteral ID="UcLiteral27" runat="server" Text='<%#Eval("ExpectedDeliveryTime") %>'></cc1:ucLiteral>&nbsp;
                                    <cc1:ucLiteral ID="UcLiteral28" runat="server" Text='<%# Eval("DeliveryDate", "{0:dddd}")%>'></cc1:ucLiteral>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Site">
                                <HeaderStyle Width="120px" HorizontalAlign="Left" />
                                <ItemStyle HorizontalAlign="Left" />
                                <ItemTemplate>
                                    <cc1:ucLabel ID="LBLSiteName" runat="server" Text='<%# Eval("SiteName") %>'></cc1:ucLabel>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Booking Ref #" DataField="BookingRef" SortExpression="BookingRef">
                                <HeaderStyle Width="170px" HorizontalAlign="Left" />
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="Vendor Name">
                                <HeaderStyle Width="200px" HorizontalAlign="Left" />
                                <ItemStyle HorizontalAlign="Left" />
                                <ItemTemplate>
                                    <cc1:ucLiteral ID="UcLiteral2" runat="server" Text='<%#Eval("VendorName") %>'></cc1:ucLiteral>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Carrier Name">
                                <HeaderStyle Width="180px" HorizontalAlign="Left" />
                                <ItemStyle HorizontalAlign="Left" />
                                <ItemTemplate>
                                    <cc1:ucLiteral ID="UcLiteral3" runat="server" Text='<%#Eval("CarrierName") %>'></cc1:ucLiteral>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Status" Visible="false" DataField="BookingStatus" SortExpression="BookingStatus">
                                <HeaderStyle Width="120px" HorizontalAlign="Left" />
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                        </Columns>
                    </cc1:ucGridView>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>

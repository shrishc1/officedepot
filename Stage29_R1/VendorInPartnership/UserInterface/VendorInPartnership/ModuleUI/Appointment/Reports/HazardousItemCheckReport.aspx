﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="HazardousItemCheckReport.aspx.cs"
    MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    Inherits="ModuleUI_Appointment_Reports_HazardousItemCheckReport" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectSite.ascx" TagName="MultiSelectSite"
    TagPrefix="cc1" %>
<%@ Register Src="../../../CommonUI/UserControls/ucExportToExcel.ascx" TagName="ucExportToExcel"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectVendor.ascx" TagName="MultiSelectVendor"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectCarrier.ascx" TagName="MultiSelectCarrier"
    TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2>
        <cc1:ucLabel ID="lblHazardousItemCheckReport" runat="server"></cc1:ucLabel>
    </h2>
    <style type="text/css">
        .row1 {
            color: #333333;
            background-color: #F7F6F3;
        }

        .row2 {
            color: #284775;
            background-color: #FFFFFF;
        }
    </style>
    <script type="text/javascript">

        function setValue1(target) {
            document.getElementById('<%=hdnJSFromDt.ClientID %>').value = target.value;
        }

        function setValue2(target) {
            document.getElementById('<%=hdnJSToDt.ClientID %>').value = target.value;
        }

        $(document).ready(function () {
            $('#txtToDate,#txtFromDate').datepicker({
                showOtherMonths: true,
                selectOtherMonths: true, changeMonth: true, changeYear: true,
                minDate: new Date(2013, 0, 1),
                yearRange: '2013:+100',
                dateFormat: 'dd/mm/yy'
            });
        });

    </script>
    <asp:ScriptManager runat="server" />
    <div class="formbox">
        <asp:HiddenField ID="hdnJSFromDt" runat="server" />
        <asp:HiddenField ID="hdnJSToDt" runat="server" />
        <table width="95%" cellspacing="5" cellpadding="2" border="0" align="center" class="top-settings" id="tblsearch" runat="server">
            <tr>
                <td style="font-weight: bold;">
                    <cc1:ucLabel ID="lblVendor" runat="server" Text="Vendor"></cc1:ucLabel>
                </td>
                <td style="font-weight: bold;">
                    <cc1:ucLabel ID="UcLabel2" runat="server">:</cc1:ucLabel>
                </td>
                <td>
                    <cc1:MultiSelectVendor runat="server" ID="msVendor" />
                </td>
            </tr>
            <tr>
                <td style="font-weight: bold;">
                    <cc1:ucLabel ID="lblSite" runat="server" Text="Site"></cc1:ucLabel>
                </td>
                <td style="font-weight: bold;">
                    <cc1:ucLabel ID="UcLabel4" runat="server">:</cc1:ucLabel>
                </td>
                <td>
                    <cc1:MultiSelectSite runat="server" ID="msSite" />
                </td>
            </tr>
            <tr>
                <td style="font-weight: bold;">
                    <cc1:ucLabel ID="UcLabel1" runat="server" Text="Carrier"></cc1:ucLabel>
                </td>
                <td style="font-weight: bold;">
                    <cc1:ucLabel ID="UcLabel3" runat="server">:</cc1:ucLabel>
                </td>
                <td style="width: 83%;">
                    <uc1:MultiSelectCarrier ID="msCarrier" runat="server" />
                </td>
            </tr>
            <tr>
                <td></td>
            </tr>
            <tr>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td style="font-weight: bold;">
                    <cc1:ucRadioButton ID="rdoSummary" runat="server" Text="Summary" GroupName="ReportType"
                        Checked="true" />
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <cc1:ucRadioButton ID="rdoDetailed" runat="server" Text="Detailed" GroupName="ReportType" />
                </td>
            </tr>
            <tr>
                <td></td>
            </tr>
            <tr>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td colspan="4" style="font-weight: bold;">
                    <cc1:ucLabel ID="lblDateFrom_1" runat="server" Text="Date From"></cc1:ucLabel>
                    <cc1:ucTextbox ID="txtFromDate" ClientIDMode="Static" runat="server" onchange="setValue1(this)"
                        ReadOnly="True" Width="70px" />
                    <cc1:ucLabel ID="lblTo" runat="server" Text="To"></cc1:ucLabel>
                    <cc1:ucTextbox ID="txtToDate" ClientIDMode="Static" runat="server" onchange="setValue2(this)"
                        ReadOnly="True" Width="70px" />
                </td>
                <td>&nbsp;
                </td>
            </tr>
            <tr>
                <td colspan="8" align="right">
                    <cc1:ucButton ID="btnSearch" runat="server" Text="Search"
                        CssClass="button" OnClick="btnSearch_Click" /></td>
            </tr>
        </table>
        <br />
        <table width="100%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings" id="tblgrid" runat="server" visible="false">
            <tr>
                <td align="right">
                    <cc1:ucButton ID="btnExportToExcel" runat="server" CssClass="exporttoexcel" Text="Export To Excel"
                        OnClick="btnExportToExcel_Click" Width="109px" Height="20px" Visible="false" /></td>
            </tr>
            <tr>
                <td align="left">
                    <asp:GridView ID="grdHazardous" Width="100%" runat="server" AutoGenerateColumns="false"
                        CssClass="grid">
                        <EmptyDataTemplate>
                            No Records Found
                        </EmptyDataTemplate>
                        <Columns>
                            <asp:BoundField DataField="SiteName" HeaderText="Site">
                                <ItemStyle HorizontalAlign="left" />
                                <HeaderStyle HorizontalAlign="left" />
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="Vendor">
                                <ItemTemplate>
                                    <asp:LinkButton Text='<%# Eval("Vendor.VendorName") %>' ID="lnkVendor" runat="server"
                                        CommandArgument='<%# Eval("Vendor.Vendorid") + "," +Eval("SiteID") %>' OnClick="lnkVendor_Click" />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="left" />
                                <HeaderStyle HorizontalAlign="left" />
                            </asp:TemplateField>
                            <asp:BoundField DataField="NumberOfDeliveries" HeaderText="# Deliveries">
                                <ItemStyle HorizontalAlign="left" />
                                <HeaderStyle HorizontalAlign="left" />
                            </asp:BoundField>
                        </Columns>
                    </asp:GridView>
                    <asp:GridView ID="grdHazardousByVendorID" Width="100%" runat="server" AutoGenerateColumns="false"
                        CssClass="grid">
                        <EmptyDataTemplate>
                            No Records Found
                        </EmptyDataTemplate>
                        <Columns>
                            <asp:BoundField DataField="SiteName" HeaderText="Site">
                                <ItemStyle HorizontalAlign="left" />
                                <HeaderStyle HorizontalAlign="left" />
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="Vendor">
                                <ItemTemplate>
                                    <asp:Literal ID="ltVendor" Text='<%# Eval("Vendor.VendorName") %>' runat="server"></asp:Literal>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="left" />
                                <HeaderStyle HorizontalAlign="left" />
                            </asp:TemplateField>
                            <asp:BoundField DataField="BookingRef" HeaderText="BookingRef">
                                <ItemStyle HorizontalAlign="left" />
                                <HeaderStyle HorizontalAlign="left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="ScheduleDate" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Date">
                                <ItemStyle HorizontalAlign="left" />
                                <HeaderStyle HorizontalAlign="left" />
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="Hazardous Items Declared">
                                <ItemTemplate>
                                    <asp:Literal ID="ltHazardous" Text='<%# Convert.ToBoolean(Eval("IsEnableHazardouesItemPrompt")) == true ? "Y" :"N" %>' runat="server"></asp:Literal>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="left" />
                                <HeaderStyle HorizontalAlign="left" />
                            </asp:TemplateField>
                            <asp:BoundField DataField="BookingComments" HeaderText="Comments">
                                <ItemStyle HorizontalAlign="left" />
                                <HeaderStyle HorizontalAlign="left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="UserName" HeaderText="Unloaded by">
                                <ItemStyle HorizontalAlign="left" />
                                <HeaderStyle HorizontalAlign="left" />
                            </asp:BoundField>
                        </Columns>
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td colspan="4">
                    <cc1:PagerV2_8 ID="pager1" runat="server" OnCommand="pager_Command" GenerateGoToSection="true"></cc1:PagerV2_8>
                </td>
            </tr>
            <tr>
                <td colspan="5" align="right">
                    <cc1:ucButton ID="btnBack" CssClass="button" runat="server"
                        OnClick="btnBack_Click"></cc1:ucButton>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>

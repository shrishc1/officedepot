﻿using System;
using Microsoft.Reporting.WebForms;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using Utilities;
using WebUtilities;
using System.Web.UI;
using BusinessEntities.ModuleBE.Upload;
using System.Collections.Generic;
using BusinessLogicLayer.ModuleBAL.Upload;
using System.Drawing;
using BusinessLogicLayer.ModuleBAL.Security;
using BusinessEntities.ModuleBE.Security;
using BaseControlLibrary;

public partial class ModuleUI_Appointment_Reports_OpenPurchaseOrderOverview : CommonPage
{
    #region Declarations ...
    protected string SelectDueByDate = WebCommon.getGlobalResourceValue("SelectDueByDate");
    protected string Pleaseselectatleastonesite = WebCommon.getGlobalResourceValue("Pleaseselectatleastonesite");
    protected string Maximumfoursitesareallowed = WebCommon.getGlobalResourceValue("Maximumfoursitesareallowed");
    protected string BookedPODate = WebCommon.getGlobalResourceValue("BookedPODate");
    protected string Pleaseaddsearchcriteria = WebCommon.getGlobalResourceValue("Pleaseaddsearchcriteria");
    protected string NewMaximumfoursitesareallowed = WebCommon.getGlobalResourceValue("NewMaximumfoursitesareallowed");

    #endregion

    protected void Page_Init(object sender, EventArgs e)
    {
        ucVendorTemplateSelect.CurrentPage = this;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        BtnExportToExcel.GridViewControl = TempGridView;
        BtnExportToExcel.Page = this;
        BtnExportToExcel.FileName = "OpenPOReport";
        BtnExportToExcel.TextColNumber = new int[3]; // How may column is required to be in text      
        BtnExportToExcel.TextColNumber[0] = 15; //column number on grid view 
        BtnExportToExcel.TextColNumber[1] = 14; //column number on grid view 
        BtnExportToExcel.TextColNumber[2] = 16; //column number on grid view 

        TextBox TxtPO = msPO.Controls[0] as TextBox;
        lblSearchResult.Text = "Open Purchase Order Report";

        if (!IsPostBack)
        {
            BtnBackSearch.Visible = false;
            mvDiscrepancyList.ActiveViewIndex = 0;

            if (TxtPO != null)
                TxtPO.Width = 313;

            rbAll.Checked = true;
            if (!string.IsNullOrEmpty(GetQueryStringValue("SVT")))
            {
                BindGridview();
            }
            if (!string.IsNullOrEmpty(GetQueryStringValue("Report")))
            {
                BindGridViewForOpenSummaryDrill();
            }
        }

        TextBox TxtPurchaseOrder = Utilities.Common.FindControlRecursive(this.Page.Controls[0], "txtPurchaseOrder") as TextBox;
        if (TxtPurchaseOrder != null)
            TxtPurchaseOrder.Focus();

    }

    /// <summary>
    /// Method to show Vendor on the basis of selected Template
    /// </summary>
    public override void TemplateSelectedIndexChanged()
    {
        base.TemplateSelectedIndexChanged();

        int VendorTemplateId = Convert.ToInt32(ucVendorTemplateSelect.innerControlddlTemplate.SelectedValue);

        ListBox LstSelectedVendor = (ListBox)msVendor.FindControl("lstRight");
        ListBox LstVendor = (ListBox)msVendor.FindControl("ucVendor").FindControl("lstLeft");

        if (VendorTemplateId > 0)
        {
            SCT_TemplateBAL oSCT_TemplateBAL = new SCT_TemplateBAL();
            SCT_TemplateBE oSCT_TemplateBE = new SCT_TemplateBE();
            oSCT_TemplateBE.VendorTemplateID = VendorTemplateId;
            oSCT_TemplateBE.Action = "GetVendorTemplateInfobyID";
            List<SCT_TemplateBE> LstVendorTemplate = oSCT_TemplateBAL.GetVendorTemplateById(oSCT_TemplateBE);

            if (LstVendorTemplate != null && LstVendorTemplate.Count > 0)
            {
                LstSelectedVendor.Items.Clear();
                msVendor.innerControlHiddenField.Value = string.Empty;
                msVendor.innerControlHiddenField2.Value = string.Empty;
                for (int iCount = 0; iCount < LstVendorTemplate.Count; iCount++)
                {
                    LstSelectedVendor.Items.Add(new ListItem(LstVendorTemplate[iCount].Vendor.Vendor_Name, LstVendorTemplate[iCount].Vendor.VendorID.ToString()));
                    msVendor.SelectedVendorIDs = LstVendorTemplate[iCount].Vendor.VendorID.ToString();
                    msVendor.SelectedVendorName = LstVendorTemplate[iCount].Vendor.Vendor_Name.ToString();
                }
            }
        }
        else
        {
            LstSelectedVendor.Items.Clear();
            msVendor.innerControlHiddenField.Value = string.Empty;
            msVendor.innerControlHiddenField2.Value = string.Empty;
        }
    }
    public void BindGridViewForOpenSummaryDrill()
    {
        List<Up_PurchaseOrderDetailBE> lsOpenPO = null;
        UP_PurchaseOrderDetailBAL objPOBal = new UP_PurchaseOrderDetailBAL();
        Up_PurchaseOrderDetailBE oNewPOBE = new Up_PurchaseOrderDetailBE();
        if (GetQueryStringValue("VendorID") != null && GetQueryStringValue("VendorID") != "")
        {
            oNewPOBE.SelectedVendorIDs = GetQueryStringValue("VendorID").Trim();
        }
        if (GetQueryStringValue("SPID") != null && GetQueryStringValue("SPID") != "")
        {
            oNewPOBE.SelectedInventoryMgrIDs = GetQueryStringValue("SPID").Trim();
        }
        if (GetQueryStringValue("SPGID") != null && GetQueryStringValue("SPGID") != "" && GetQueryStringValue("SPGID") != "0")
        {
            oNewPOBE.StockPlannerGroupingID = GetQueryStringValue("SPGID").Trim();
        }
        if (GetQueryStringValue("SiteID") != null && GetQueryStringValue("SiteID") != "" && GetQueryStringValue("SiteID") != "0")
        {
            oNewPOBE.SelectedSiteIDs = GetQueryStringValue("SiteID").Trim();
        }

        if (GetQueryStringValue("CountryID") != null && GetQueryStringValue("CountryID") != "" && GetQueryStringValue("CountryID") != "0")
        {
            oNewPOBE.SelectedCountryIDs = GetQueryStringValue("CountryID").Trim();
        }
        if (GetQueryStringValue("Report") != null && GetQueryStringValue("Report") != "")
        {
            if (GetQueryStringValue("Report") == "Open")
            {
                oNewPOBE.Action = "ALL";
            }
            else
            {
                oNewPOBE.Action = "OOD";
            }
        }

        if (GetQueryStringValue("FilterDays") != null && !string.IsNullOrEmpty(GetQueryStringValue("FilterDays").Trim()))
        {
            oNewPOBE.Daysfilter = GetQueryStringValue("FilterDays");
        }


        oNewPOBE.IsFromSummary = true;
        lsOpenPO = objPOBal.GetOpenPODetailsBAL(oNewPOBE);
        GvDisLog.DataSource = lsOpenPO;
        GvDisLog.DataBind();

        //for export to excel
        TempGridView.DataSource = lsOpenPO;
        TempGridView.DataBind();

        Cache["lstOpenPO"] = lsOpenPO;
        mvDiscrepancyList.ActiveViewIndex = 1;
        BtnBackSearch.Visible = true;
        BtnGenerateReport.Visible = false;
        rbAll.Checked = true;
        txtDate.innerControltxtDate.Value = string.Empty;

        if (lsOpenPO != null && lsOpenPO.Count > 0)
        {
            BtnExportToExcel.Visible = true;
        }
        else
        {
            BtnExportToExcel.Visible = false;
        }
    }
    public void BindGridview()
    {
        List<Up_PurchaseOrderDetailBE> lsOpenPO = null;
        UP_PurchaseOrderDetailBAL objPOBal = new UP_PurchaseOrderDetailBAL();
        Up_PurchaseOrderDetailBE oNewPOBE = new Up_PurchaseOrderDetailBE();

        oNewPOBE.SelectedSiteIDs = GetQueryStringValue("SiteID");
        oNewPOBE.SelectedDate = GetQueryStringValue("ReportDate");
        GvDisLog.HeaderStyle.Wrap = false;
        GvDisLog.HeaderStyle.HorizontalAlign = HorizontalAlign.Justify;
        if (GetQueryStringValue("SVT").Trim() == "ConfirmedLines")
        {
            oNewPOBE.OnTime = string.Empty;
            oNewPOBE.Action = "Booked";
        }
        else if (GetQueryStringValue("SVT").Trim() == "Early")
        {
            oNewPOBE.OnTime = "Early";
            oNewPOBE.Action = "Booked";
        }
        else if (GetQueryStringValue("SVT").Trim() == "Late")
        {
            oNewPOBE.OnTime = "Late";
            oNewPOBE.Action = "Booked";
        }
        else if (GetQueryStringValue("SVT").Trim() == "OnDueDate")
        {
            oNewPOBE.OnTime = "On Time";
            oNewPOBE.Action = "Booked";
        }
        if (GetQueryStringValue("SVT").Trim() == "LinesDue")
        {
            oNewPOBE.OnTime = "ALL";
            oNewPOBE.Action = "DUEBY";
        }
        else if (GetQueryStringValue("SVT").Trim() == "BookedEarly")
        {
            oNewPOBE.OnTime = "Early";
            oNewPOBE.Action = "DUEBY";
        }
        else if (GetQueryStringValue("SVT").Trim() == "BookedLate")
        {
            oNewPOBE.OnTime = "Late";
            oNewPOBE.Action = "DUEBY";
        }
        else if (GetQueryStringValue("SVT").Trim() == "BookedDueDate")
        {
            oNewPOBE.OnTime = "On Time";
            oNewPOBE.Action = "DUEBY";
        }
        else if (GetQueryStringValue("SVT").Trim() == "NotBooked")
        {
            oNewPOBE.OnTime = string.Empty;
            oNewPOBE.Action = "DUEBY";
        }
        else if (GetQueryStringValue("SVT").Trim() == "Overdue")
        {
            oNewPOBE.OnTime = string.Empty;
            oNewPOBE.Action = "OOD";
        }

        if (GetQueryStringValue("FilterDays") != null && !string.IsNullOrEmpty(GetQueryStringValue("FilterDays").Trim()))
        {
            oNewPOBE.Daysfilter = GetQueryStringValue("FilterDays");
        }

        lsOpenPO = objPOBal.GetOpenPODetailsBAL(oNewPOBE);
        GvDisLog.DataSource = lsOpenPO;
        GvDisLog.DataBind();

        //for export to excel
        TempGridView.DataSource = lsOpenPO;
        TempGridView.DataBind();

        Cache["lstOpenPO"] = lsOpenPO;
        mvDiscrepancyList.ActiveViewIndex = 1;
        BtnBackSearch.Visible = true;
        BtnGenerateReport.Visible = false;
        rbAll.Checked = true;
        txtDate.innerControltxtDate.Value = string.Empty;
    }

    protected void BtnGenerateReport_Click(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(msPO.SelectedPO))
        {
            if (string.IsNullOrEmpty(msStockPlanner.SelectedStockPlannerIDs) || string.IsNullOrWhiteSpace(msStockPlanner.SelectedStockPlannerIDs))
            {
                if (string.IsNullOrEmpty(msVendor.SelectedVendorIDs) || string.IsNullOrWhiteSpace(msVendor.SelectedVendorIDs))
                {
                    if (string.IsNullOrEmpty(UcVikingSku.SelectedSKUName))
                    {
                        if (string.IsNullOrEmpty(msSKU.SelectedSKUName))
                        {
                            if (string.IsNullOrEmpty(multiSelectStockPlannerGrouping.SelectedStockPlannerGroupingIDs) ||
                                string.IsNullOrWhiteSpace(multiSelectStockPlannerGrouping.SelectedStockPlannerGroupingIDs))
                            {
                                if (string.IsNullOrEmpty(multiSelectSKUGrouping.SelectedSkuGroupingIDs) ||
                                    string.IsNullOrWhiteSpace(multiSelectSKUGrouping.SelectedSkuGroupingIDs))
                                {
                                    if (string.IsNullOrEmpty(msSite.SelectedSiteIDs) || string.IsNullOrWhiteSpace(msSite.SelectedSiteIDs))
                                    {
                                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + Pleaseaddsearchcriteria + "')", true);
                                        return;
                                    }
                                    else
                                    {
                                        string[] SelectedSite = msSite.SelectedSiteIDs.Split(',');
                                        if (SelectedSite.Length > 4)
                                        {
                                            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + NewMaximumfoursitesareallowed + "')", true);
                                            return;
                                        }

                                    }
                                }

                            }
                        }
                    }
                }
            }
        }

        List<Up_PurchaseOrderDetailBE> lsOpenPO = null;
        UP_PurchaseOrderDetailBAL objPOBal = new UP_PurchaseOrderDetailBAL();
        Up_PurchaseOrderDetailBE oNewPOBE = new Up_PurchaseOrderDetailBE();

        if (!string.IsNullOrEmpty(msSite.SelectedSiteIDs))
            oNewPOBE.SelectedSiteIDs = msSite.SelectedSiteIDs;

        if (!string.IsNullOrEmpty(msVendor.SelectedVendorIDs))
            oNewPOBE.SelectedVendorIDs = msVendor.SelectedVendorIDs;

        if (!string.IsNullOrEmpty(msStockPlanner.SelectedStockPlannerIDs))
            oNewPOBE.SelectedInventoryMgrIDs = msStockPlanner.SelectedStockPlannerIDs;

        if (!string.IsNullOrEmpty(msPO.SelectedPO))
            oNewPOBE.PurchaseOrderIds = msPO.SelectedPO;

        if (!string.IsNullOrEmpty(UcVikingSku.SelectedSKUName))
            oNewPOBE.Direct_code = UcVikingSku.SelectedSKUName;
        if (!string.IsNullOrEmpty(msSKU.SelectedSKUName))
            oNewPOBE.OD_Code = msSKU.SelectedSKUName;
        if (!string.IsNullOrEmpty(multiSelectStockPlannerGrouping.SelectedStockPlannerGroupingIDs))
            oNewPOBE.StockPlannerGroupingID = multiSelectStockPlannerGrouping.SelectedStockPlannerGroupingIDs;
        oNewPOBE.SkugroupingID = multiSelectSKUGrouping.SelectedSkuGroupingIDs;


        if (!string.IsNullOrEmpty(MultiSelectSubvdr.SelectedSubVendorIDs))
            oNewPOBE.SubVendorIDs = MultiSelectSubvdr.SelectedSubVendorIDs;

        if (rbAll.Checked)
        {
            oNewPOBE.Action = "ALL";
            oNewPOBE.SelectedDate = null;
        }
        else if (rbOOD.Checked)
        {
            oNewPOBE.Action = "OOD";
            oNewPOBE.SelectedDate = DateTime.Now.ToString("dd/MM/yyyy");
        }
        else if (rbDueBy.Checked)
        {
            oNewPOBE.Action = "DUEBY";
            oNewPOBE.SelectedDate = txtDate.GetDate.ToString("dd/MM/yyyy");
        }
        else if (rbBookedPO.Checked)
        {
            oNewPOBE.Action = "Booked";
            oNewPOBE.SelectedDate = txtBookingDate.GetDate.ToString("dd/MM/yyyy");
        }
        GvDisLog.HeaderStyle.Wrap = false;
        GvDisLog.HeaderStyle.HorizontalAlign = HorizontalAlign.Justify;


        if (GetQueryStringValue("FilterDays") != null && !string.IsNullOrEmpty(GetQueryStringValue("FilterDays").Trim()))
        {
            oNewPOBE.Daysfilter = GetQueryStringValue("FilterDays");
        }


        lsOpenPO = objPOBal.GetOpenPODetailsBAL(oNewPOBE);
        GvDisLog.DataSource = lsOpenPO;
        GvDisLog.DataBind();

        //for export to excel
        TempGridView.DataSource = lsOpenPO;
        TempGridView.DataBind();

        Cache["lstOpenPO"] = lsOpenPO;
        mvDiscrepancyList.ActiveViewIndex = 1;
        BtnBackSearch.Visible = true;
        BtnGenerateReport.Visible = false;
        rbAll.Checked = true;
        txtDate.innerControltxtDate.Value = string.Empty;

    }

    protected void BtnBack_Click(object sender, EventArgs e)
    {
        mvDiscrepancyList.ActiveViewIndex = 0;
        BtnBackSearch.Visible = false;
        BtnGenerateReport.Visible = true;
        RetainPurchaseOrderData();

        if (!string.IsNullOrEmpty(GetQueryStringValue("Report")))
        {
            Page.ClientScript.RegisterStartupScript(this.GetType(), "script", "window.close()", true);
        }
    }
    public void RetainPurchaseOrderData()
    {
        multiSelectStockPlannerGrouping.setStockPlannerGroupingsOnPostBack();
        msStockPlanner.setStockPlannerOnPostBack();
        msSite.setSitesOnPostBack();
        msVendor.setVendorsOnPostBack();


        TextBox txtSearchedStockPlanner = msStockPlanner.FindControl("txtUserName") as TextBox;
        string StockPlannerText = txtSearchedStockPlanner.Text;
        string StockPlannerID = msStockPlanner.SelectedStockPlannerIDs;
        ucListBox lstRight = msStockPlanner.FindControl("ucLBRight") as ucListBox;
        ucListBox lstLeft = msStockPlanner.FindControl("ucLBLeft") as ucListBox;
        string strDtockPlonnerId = string.Empty;
        if (lstLeft != null && lstRight != null && !string.IsNullOrEmpty(StockPlannerID))
        {
            lstLeft.Items.Clear();
            msStockPlanner.SearchStockPlannerClick(StockPlannerText);
            string[] strStockPlonnerIDs = StockPlannerID.Split(',');
            for (int index = 0; index < strStockPlonnerIDs.Length; index++)
            {
                ListItem listItem = lstLeft.Items.FindByValue(strStockPlonnerIDs[index]);
                if (listItem != null)
                {
                    strDtockPlonnerId += strStockPlonnerIDs[index] + ",";
                    lstLeft.Items.Remove(listItem);
                }

            }
            HiddenField hdnSelectedStockPlonner = msStockPlanner.FindControl("hiddenSelectedIDs") as HiddenField;
            hdnSelectedStockPlonner.Value = strDtockPlonnerId;
        }

        ////************************* StockPlannerGrouping ***************************
        string StockPlannerGroupingID = multiSelectStockPlannerGrouping.SelectedStockPlannerGroupingIDs;
        ucListBox lstRightGrouping = multiSelectStockPlannerGrouping.FindControl("lstRight") as ucListBox;
        ucListBox lstLeftGrouping = multiSelectStockPlannerGrouping.FindControl("lstLeft") as ucListBox;

        if (lstLeftGrouping != null && lstRightGrouping != null)
        {
            lstLeftGrouping.Items.Clear();
            multiSelectStockPlannerGrouping.BindStockPlannerGrouping();
            if (!string.IsNullOrEmpty(StockPlannerGroupingID))
            {
                string[] strStockPlannerGroupingIDs = StockPlannerGroupingID.Split(',');
                for (int index = 0; index < strStockPlannerGroupingIDs.Length; index++)
                {
                    ListItem listItem = lstLeftGrouping.Items.FindByValue(strStockPlannerGroupingIDs[index]);
                    if (listItem != null)
                    {
                        lstLeftGrouping.Items.Remove(listItem);
                    }
                }
            }
        }
        //*********** Site ***************
        string SiteId = msSite.SelectedSiteIDs;
        ucListBox lstRightSite = msSite.FindControl("lstRight") as ucListBox;
        ucListBox lstLeftSite = msSite.FindControl("lstLeft") as ucListBox;
        string msSiteid = string.Empty;
        lstRightSite.Items.Clear();
        lstRightSite.Items.Clear();
        msSite.SelectedSiteIDs = "";
        if (lstLeftSite != null && lstRightSite != null && !string.IsNullOrEmpty(SiteId))
        {
            lstRightSite.Items.Clear();
            msSite.BindSite();

            string[] strSiteIDs = SiteId.Split(',');
            for (int index = 0; index < strSiteIDs.Length; index++)
            {
                ListItem listItem = lstLeftSite.Items.FindByValue(strSiteIDs[index]);
                if (listItem != null)
                {
                    msSiteid = msSiteid + strSiteIDs[index].ToString() + ",";
                    lstRightSite.Items.Add(listItem);
                    lstLeftSite.Items.Remove(listItem);
                }

            }
            if (string.IsNullOrEmpty(msSite.SelectedSiteIDs))
                msSite.SelectedSiteIDs = msSiteid.Trim(',');
        }

        ////************************* Vendor ***************************
        string IncludeVendorId = msVendor.SelectedVendorIDs;
        TextBox txtVendorId = msVendor.FindControl("ucVendor").FindControl("txtVendorNo") as TextBox;
        string txtVendorIdText = txtVendorId.Text;
        ucListBox lstRightVendor = msVendor.FindControl("lstRight") as ucListBox;
        ucListBox lstLeftVendor = msVendor.FindControl("ucVendor").FindControl("lstLeft") as ucListBox;

        if (lstLeftVendor != null && lstRightVendor != null && (!string.IsNullOrEmpty(IncludeVendorId) || !string.IsNullOrEmpty(txtVendorIdText)))
        {
            lstLeftVendor.Items.Clear();

            msVendor.SearchVendorClick(txtVendorIdText);
            if (!string.IsNullOrEmpty(IncludeVendorId))
            {
                string[] strIncludeVendorIDs = IncludeVendorId.Split(',');
                for (int index = 0; index < strIncludeVendorIDs.Length; index++)
                {
                    ListItem listItem = lstLeftVendor.Items.FindByValue(strIncludeVendorIDs[index]);
                    if (listItem != null)
                    {
                        lstRightVendor.Items.Add(listItem);
                        lstLeftVendor.Items.Remove(listItem);
                    }
                }
            }
        }

        //*********************** Viking Sku
        string vikingSkuId = UcVikingSku.SelectedSKUName;
        ucListBox lstRightViking = UcVikingSku.FindControl("lstRight") as ucListBox;
        string hdnSkuid = string.Empty;
        lstRightViking.Items.Clear();
        if (lstRightViking != null && !string.IsNullOrEmpty(vikingSkuId))
        {
            HiddenField hdfSelectedId = UcVikingSku.FindControl("hiddenSelectedId") as HiddenField;
            lstRightViking.Items.Clear();
            string[] strVikingIDs = vikingSkuId.Split(',');
            for (int index = 0; index < strVikingIDs.Length; index++)
            {
                string Item = strVikingIDs[index];
                if (!string.IsNullOrEmpty(Item))
                {
                    ListItem listItem = new ListItem();
                    listItem.Text = Item;
                    hdnSkuid += Item + ",";
                    lstRightViking.Items.Add(listItem);
                }
            }
            hdfSelectedId.Value = hdnSkuid;
        }
        //************** Od sku
        string OdSkuId = msSKU.SelectedSKUName;
        ucListBox lstRightOD = msSKU.FindControl("lstRight") as ucListBox;
        string hdnValue = string.Empty;
        lstRightOD.Items.Clear();
        if (lstRightOD != null && !string.IsNullOrEmpty(OdSkuId))
        {
            HiddenField hdfSelectedId = msSKU.FindControl("hiddenSelectedId") as HiddenField;
            lstRightOD.Items.Clear();
            string[] strODIDs = OdSkuId.Split(',');
            for (int index = 0; index < strODIDs.Length; index++)
            {
                string Item = strODIDs[index];
                if (!string.IsNullOrEmpty(Item))
                {
                    ListItem listItem = new ListItem();
                    listItem.Text = Item;
                    lstRightOD.Items.Add(listItem);
                    hdnValue += Item + ",";
                }
            }
            hdfSelectedId.Value = hdnValue;
        }

        //**************** SKUGrouping Group
        string SKUGroupId = multiSelectSKUGrouping.SelectedSkuGroupingIDs;
        ucListBox lstRightSKU = multiSelectSKUGrouping.FindControl("lstRight") as ucListBox;
        ucListBox lstLeftSKU = multiSelectSKUGrouping.FindControl("lstLeft") as ucListBox;
        string msSKUgrouping = string.Empty;
        lstRightSKU.Items.Clear();
        multiSelectSKUGrouping.SelectedSkuGroupingIDs = "";
        if (lstLeftSKU != null && lstRightSKU != null && !string.IsNullOrEmpty(SKUGroupId))
        {

            lstRightSKU.Items.Clear();
            multiSelectSKUGrouping.BindSkuGrouping();

            string[] strSKUs = SKUGroupId.Split(',');
            for (int index = 0; index < strSKUs.Length; index++)
            {
                ListItem listItem = lstLeftSKU.Items.FindByValue(strSKUs[index]);
                if (listItem != null)
                {
                    msSKUgrouping = msSKUgrouping + strSKUs[index].ToString() + ",";
                    lstRightSKU.Items.Add(listItem);
                    lstLeftSKU.Items.Remove(listItem);
                }

            }
            if (string.IsNullOrEmpty(multiSelectSKUGrouping.SelectedSkuGroupingIDs))
                multiSelectSKUGrouping.SelectedSkuGroupingIDs = msSKUgrouping.Trim(',');
        }

        //************** Purchase Order No
        string PONo = msPO.SelectedPO;
        ucListBox lstRightPO = msPO.FindControl("lstRight") as ucListBox;
        string hiddenValue = string.Empty;
        lstRightPO.Items.Clear();
        if (lstRightPO != null && !string.IsNullOrEmpty(PONo))
        {
            HiddenField hdfSelectedId = msPO.FindControl("hiddenSelectedId") as HiddenField;
            lstRightPO.Items.Clear();
            string[] strPOs = PONo.Split(',');
            for (int index = 0; index < strPOs.Length; index++)
            {
                string Item = strPOs[index];
                if (!string.IsNullOrEmpty(Item))
                {
                    ListItem listItem = new ListItem();
                    listItem.Text = Item;
                    lstRightPO.Items.Add(listItem);
                    hiddenValue += Item + ",";
                }
            }
            hdfSelectedId.Value = hiddenValue;
        }

        if (rbDueBy.Checked)
        {

        }

        if (rbBookedPO.Checked)
        {

        }

    }

    protected void GvDisLog_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType != DataControlRowType.DataRow)
            return;
        if (e.Row.Cells[8].Text == "Early")
        {
            e.Row.Cells[8].ForeColor = Color.Green;
        }
        else if (e.Row.Cells[8].Text == "Late")
        {
            e.Row.Cells[8].ForeColor = Color.Red;
        }

        for (int i = 0; i < e.Row.Cells.Count; i++)
        {
            if (i == 5)
            {
                TableCell Cell = e.Row.Cells[i];
                // if both row and column are odd, color then black
                // if both row and column are even, color then yellow
                if (((e.Row.RowIndex % 2 == 1) && (i % 2 == 1)) ||
                    ((e.Row.RowIndex % 2 == 0) && (i % 2 == 0)))
                    Cell.BackColor = Color.LightGreen;
                else
                    Cell.BackColor = Color.White;
                break;
            }
        }
    }

    protected void GvDisLog_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        if (Cache["lstOpenPO"] != null)
        {
            List<Up_PurchaseOrderDetailBE> LsOpenPO = Cache["lstOpenPO"] as List<Up_PurchaseOrderDetailBE>;
            GvDisLog.PageIndex = e.NewPageIndex;
            GvDisLog.DataSource = LsOpenPO;
            GvDisLog.DataBind();
        }
    }

    public override BusinessEntities.BaseBe[] SubSort(GridViewSortEventArgs e)
    {
        return Utilities.GenericListHelper<Up_PurchaseOrderDetailBE>.SortList((List<Up_PurchaseOrderDetailBE>)Cache["lstOpenPO"],
            e.SortExpression, e.SortDirection).ToArray();
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebUtilities;
using BusinessEntities.ModuleBE.Appointment.Reports;
using BusinessLogicLayer.ModuleBAL.Appointment.Reports;
using Utilities;

public partial class ModuleUI_Appointment_Reports_ScheduledReceiptedReportBySiteAndVendor :CommonPage
{
    protected string siteRequired = WebCommon.getGlobalResourceValue("SiteRequired");
    protected string NoRecordsFound = WebCommon.getGlobalResourceValue("NoRecordsFound");
    protected string VendorReq = WebCommon.getGlobalResourceValue("VendorReq");
    protected void Page_Init(object sender, EventArgs e)
    {
        ucSeacrhVendor1.CurrentPage = this;
        ddlSite.CurrentPage = this;
        ddlSite.IsAllRequired = true;
        ddlSite.SchedulingContact = true;
        ddlSite.CurrentPage = this;
        ddlSite.IsAllRequired = true;
        ucSeacrhVendor1.IsParentRequired = true;
        ucSeacrhVendor1.IsStandAloneRequired = true;
        ucSeacrhVendor1.IsChildRequired = true;
        ucSeacrhVendor1.IsGrandParentRequired = true;
    }

    //public override void SitePost_Load()
    //{
    //    base.SitePost_Load();

    //    if (!Page.IsPostBack)
    //    {
    //        ddlSite.innerControlddlSite.AutoPostBack = true;           
    //    }
    //}

    protected void Page_Load(object sender, EventArgs e)
    {
        
        ddlSite.innerControlddlSite.AutoPostBack = true; 
        if (!IsPostBack)
        {
            try
            {
                if (GetQueryStringValue("Date") != null)
                {
                    txtDate.innerControltxtDate.Value = GetQueryStringValue("Date");
                }
                else
                {
                    txtDate.innerControltxtDate.Value = Convert.ToString(DateTime.Now.ToString("dd/MM/yyyy"));
                }
                if (GetQueryStringValue("SiteID") != null && GetQueryStringValue("VendorID") != null)
                {
                    BindGridView(GetQueryStringValue("SiteID"), GetQueryStringValue("VendorID"));
                    ucSeacrhVendor1.IsLoadBindVendor = "true";
                    ucSeacrhVendor1.txtVendorNoVal = GetQueryStringValue("VendorName").Split('-').ElementAtOrDefault(0);
                    //ucSeacrhVendor1.innerControlVendorName.Text = GetQueryStringValue("VendorName").Replace('|','&');
                    //TextBox txtVendorNo = (TextBox)ucSeacrhVendor1.FindControl("txtVendorNo");
                    //txtVendorNo.Text = GetQueryStringValue("VendorName").Split('-').ElementAtOrDefault(0);
                    ucSeacrhVendor1.innerControlVendorNo.Text = GetQueryStringValue("VendorID");
                    ucSeacrhVendor1.VendorNo = GetQueryStringValue("VendorID");
                    ucSeacrhVendor1.SiteID = Convert.ToInt32(GetQueryStringValue("SiteID"));
                }

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex); ;
            }           
        }
        btnExportToExcel1.GridViewControl = UcGridView1;
        btnExportToExcel1.FileName = "ScheduledReceiptedReportBySiteAndVendor";
    }

    public override void SiteSelectedIndexChanged()
    {
        ucSeacrhVendor1.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
    }
    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ddlSite.innerControlddlSite.SelectedIndex = ddlSite.innerControlddlSite.Items.IndexOf(ddlSite.innerControlddlSite.Items.FindByValue(GetQueryStringValue("SiteID")));
            ucSeacrhVendor1.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
        }
    }

    public void BindGridView(string siteId=null ,string vendorid=null)
    {
        try
        {
            if (siteId == null && vendorid ==null)
            {
            siteId = ddlSite.innerControlddlSite.SelectedValue.ToString();
            vendorid = ucSeacrhVendor1.VendorNo;                 
            }
            if (!string.IsNullOrEmpty(vendorid) && (!string.IsNullOrEmpty(siteId) || siteId != "0"))
            {
                ScheduledReceiptedReportDetailsBE oScheduledReceiptedReportBE = new ScheduledReceiptedReportDetailsBE();
                ScheduledReceiptedReportBAL oScheduledReceiptedReportBAL = new ScheduledReceiptedReportBAL();
                List<ScheduledReceiptedReportDetailsBE> lstScheduledReceiptedReportBE = new List<ScheduledReceiptedReportDetailsBE>();
                oScheduledReceiptedReportBE.Action = "ScheduledReceiptedReportBySiteAndVendor";
                oScheduledReceiptedReportBE.Date = Common.GetYYYY_MM_DD(txtDate.innerControltxtDate.Value);
                oScheduledReceiptedReportBE.SiteId = Convert.ToInt32(siteId);
                oScheduledReceiptedReportBE.VendorID = Convert.ToInt32(vendorid);
                lstScheduledReceiptedReportBE = oScheduledReceiptedReportBAL.ScheduledReceiptedReportDetailsVendorBySiteBAL(oScheduledReceiptedReportBE);
                if (lstScheduledReceiptedReportBE.Count > 0)
                {
                    UcGridView1.DataSource = lstScheduledReceiptedReportBE;
                    UcGridView1.DataBind();
                    lblError.Text = string.Empty;
                    //ViewState["lstSites"] = lstScheduledReceiptedReportBE;
                }
                else
                {
                    UcGridView1.DataSource = null;
                    UcGridView1.DataBind();
                    lblError.Text = NoRecordsFound;
                }
            }

        }
        catch (Exception ex)
        {
            LogUtility.SaveErrorLogEntry(ex); ;
        }
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        ViewState["BookingRef"] = null;
        BindGridView();
    }
    //public override BusinessEntities.BaseBe[] SubSort(GridViewSortEventArgs e)
    //{
    //    return Utilities.GenericListHelper<ScheduledReceiptedReportDetailsBE>.SortList((List<ScheduledReceiptedReportDetailsBE>)ViewState["lstSites"], e.SortExpression, e.SortDirection).ToArray();
    //}
    protected void UcGridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lblBookingReferenceID = (Label)e.Row.FindControl("lblBookingReferenceID");
                Label lblLinesBooked = (Label)e.Row.FindControl("lblLinesBooked");
                Label lblTotalReceipt = (Label)e.Row.FindControl("lblTotalReceipt");
                if (ViewState["BookingRef"] == null)
                {
                    ViewState["BookingRef"] = lblBookingReferenceID.Text;
                    return;
                }
                else
                {
                    if (Convert.ToString(ViewState["BookingRef"]) != lblBookingReferenceID.Text)
                    {
                        ViewState["BookingRef"] = null;
                        ViewState["BookingRef"] = lblBookingReferenceID.Text;
                        return;
                    }
                }
                if (Convert.ToString(ViewState["BookingRef"]) == lblBookingReferenceID.Text)
                {
                    lblBookingReferenceID.Text = string.Empty;
                    lblLinesBooked.Text = string.Empty;
                    lblTotalReceipt.Text = string.Empty;
                }
                else
                {
                    ViewState["BookingRef"] = null;
                }
            }
        }
        catch (Exception ex)
        {
            LogUtility.SaveErrorLogEntry(ex); ;
        }
    }
}
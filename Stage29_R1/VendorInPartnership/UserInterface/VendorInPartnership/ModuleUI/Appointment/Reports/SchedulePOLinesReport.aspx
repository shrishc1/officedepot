﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master" AutoEventWireup="true" CodeFile="SchedulePOLinesReport.aspx.cs" Inherits="SchedulePOLinesReport" %>

<%@ Register Src="~/CommonUI/UserControls/ucSite.ascx" TagName="ucSite" TagPrefix="cc2" %>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register TagName="ucDate" TagPrefix="cc2" Src="~/CommonUI/UserControls/ucDate.ascx" %>
<%@ Register TagName="ucExportButton" TagPrefix="uc1" Src="~/CommonUI/UserControls/ucExportToExcel.ascx" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="SM1" runat="server">
    </asp:ScriptManager>
    <h2>
        <cc1:ucLabel ID="lblScheduledPOLines" runat="server" Text="Scheduled PO Lines Report"></cc1:ucLabel>
    </h2>
    <div class="right-shadow">
        <div class="formbox">
            <div id="divSearchButtons" runat="server">
                <table width="100%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                    <tr>
                        <td style="font-weight: bold; width: 12%">
                            <cc1:ucLabel ID="lblSitePrefixName" runat="server"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold; width: 1%">:
                        </td>
                        <td style="width: 14%">
                            <cc2:ucSite ID="ddlSite" runat="server" />
                        </td>
                        <td style="font-weight: bold; width: 12%">
                            <cc1:ucLabel ID="lblSchedulingdate" runat="server" Text="Scheduling Date"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold; width: 1%">:
                        </td>
                        <td style="font-weight: bold; width: 10%">
                            <cc2:ucDate ClientIDMode="Static" ID="txtDate" runat="server" />
                            <asp:RequiredFieldValidator ID="rfvDateRequired" runat="server" Display="None" ControlToValidate="txtDate$txtUCDate"
                                ValidateEmptyText="true" ValidationGroup="a" SetFocusOnError="true"></asp:RequiredFieldValidator>
                        </td>
                        <td style="font-weight: bold; width: 10%">
                            <cc1:ucLabel ID="lblLine" runat="server"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold; width: 1%">:
                        </td>
                        <td style="font-weight: bold; width: 17%" colspan="2">
                            <cc1:ucDropdownList ID="ddlLine" runat="server" Width="130px">
                                <asp:ListItem Text="--All--" Value="0"></asp:ListItem>
                                <asp:ListItem Text="Backorder" Value="1"></asp:ListItem>
                                <asp:ListItem Text="Stockouts" Value="2"></asp:ListItem>

                            </cc1:ucDropdownList>
                        </td>
                        <td style="width: 15%">
                            <cc1:ucButton ID="UcButton1" runat="server" Text="Search" CssClass="button"
                                ValidationGroup="a" OnClick="UcButton1_Click" />&nbsp;&nbsp;
                                    <uc1:ucExportButton ID="btnExportToExcel1" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="11">&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="11">
                            <cc1:ucGridView ID="ucGridSchedulePOLineReport" 
                                OnPageIndexChanging="UcGridSchedulePOLineReport_PageIndexChanging"
                                runat="server" 
                                AutoGenerateColumns="false" 
                                CssClass="grid gvclass"
                                CellPadding="0" 
                                Width="100%"
                                AllowPaging="true" 
                                PageSize="50" 
                                EmptyDataText="No Record Found" 
                                AllowSorting="true" 
                                OnSorting="GridView_Sorting"
                                EmptyDataRowStyle-HorizontalAlign="Center">
                                <AlternatingRowStyle BackColor="White" ForeColor="#284775"></AlternatingRowStyle>
                                <Columns>
                                    <asp:TemplateField>
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <ItemTemplate>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField HeaderText="PO" DataField="Purchase_order">
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" Width="150px" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="SKU" DataField="OD_SKU_NO">
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="CatCode" DataField="Category">
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="Item" DataField="Product_description">
                                        <HeaderStyle Width="15%" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="BackOrders" DataField="BackOrder">
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="BackOrder Qty" DataField="BackorderQty">
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="StockOut" DataField="StockOut">
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:BoundField>

                                    <asp:TemplateField HeaderText="Booking Ref" SortExpression="BookingReferenceID">
                                        <HeaderStyle Width="15%" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hpBookingRefPOInq" runat="server" Text='<%# Eval("BookingReferenceID") %>'
                                                NavigateUrl='<%# EncryptQuery("~/ModuleUI/Appointment/Receiving/APPRcv_BookingHistory.aspx?PN=SPOLR&Scheduledate=" + Utilities.Common.GetDD_MM_YYYY(Eval("ScheduleDate").ToString()) + "&ID=BK-" + Eval("SupplierType") + "-" + Eval("BookingID") +"&siteid=" +Eval("Siteid") )  %>'></asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="TimeDay" SortExpression="AutoID">
                                        <HeaderStyle Width="110px" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" />
                                        <ItemTemplate>                                           
                                            <cc1:ucLiteral ID="UcLiteral17_2" runat="server" Text='<%#Eval("NonWindowFromToTime") %>'
                                                Visible="false"></cc1:ucLiteral>
                                            <cc1:ucLiteral ID="UcLiteral17_1" runat="server" Text='<%#Eval("WindowStartEndTime") %>'
                                                Visible="false"></cc1:ucLiteral>
                                            <cc1:ucLiteral ID="UcLiteral17" runat="server" Text='<%#Eval("ExpectedDeliveryTime") %>'></cc1:ucLiteral>&nbsp;

                                            <cc1:ucLiteral ID="UcLiteral18" runat="server" Text='<%# Eval("DeliveryDate", "{0:dddd}")%>'></cc1:ucLiteral>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:BoundField HeaderText="Vendor" DataField="Vendor_Code">
                                        <HeaderStyle Width="15%" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="Carrier" DataField="CarrierName">
                                        <HeaderStyle Width="15%" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="Stock Planner" DataField="StockPlannerName">
                                        <HeaderStyle Width="15%" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="Status" DataField="Status">
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:BoundField>
                                    <%-- Sprint 3a End --%>
                                </Columns>
                            </cc1:ucGridView>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="11">
                            <cc1:ucGridView ID="UcGridView1" Visible="false" runat="server" AutoGenerateColumns="false" CssClass="grid gvclass"
                                CellPadding="0" Width="100%"
                                EmptyDataText="No Record Found" EmptyDataRowStyle-HorizontalAlign="Center">
                                <AlternatingRowStyle BackColor="White" ForeColor="#284775"></AlternatingRowStyle>
                                <Columns>
                                    <asp:TemplateField HeaderText="S.N.">
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <%#Container.DataItemIndex+1 %>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField HeaderText="PO" DataField="Purchase_order">
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" Width="150px" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="SKU" DataField="OD_SKU_NO">
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="CatCode" DataField="Category">
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="Item" DataField="Product_description">
                                        <HeaderStyle Width="15%" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="BackOrders" DataField="BackOrder">
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="BackOrder Qty" DataField="BackorderQty">
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="StockOut" DataField="StockOut">
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="Booking Ref" DataField="BookingReferenceID">
                                        <HeaderStyle Width="15%" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:BoundField>
                                     <asp:TemplateField HeaderText="TimeDay">
                                        <HeaderStyle Width="110px" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" />
                                        <ItemTemplate>                                           
                                            <cc1:ucLiteral ID="UcLiteral17_2" runat="server" Text='<%#Eval("NonWindowFromToTime") %>'
                                                Visible="false"></cc1:ucLiteral>
                                            <cc1:ucLiteral ID="UcLiteral17_1" runat="server" Text='<%#Eval("WindowStartEndTime") %>'
                                                Visible="false"></cc1:ucLiteral>
                                            <cc1:ucLiteral ID="UcLiteral17" runat="server" Text='<%#Eval("ExpectedDeliveryTime") %>'></cc1:ucLiteral>&nbsp;
                                            <cc1:ucLiteral ID="UcLiteral18" runat="server" Text='<%# Eval("DeliveryDate", "{0:dddd}")%>'></cc1:ucLiteral>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField HeaderText="Vendor" DataField="Vendor_Code">
                                        <HeaderStyle Width="15%" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="Carrier" DataField="CarrierName">
                                        <HeaderStyle Width="15%" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="Stock Planner" DataField="StockPlannerName">
                                        <HeaderStyle Width="15%" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="Status" DataField="Status">
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:BoundField>
                                    <%-- Sprint 3a End --%>
                                </Columns>
                            </cc1:ucGridView>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>


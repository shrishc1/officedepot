﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    AutoEventWireup="true" CodeFile="OpenPurchaseOrderOverview.aspx.cs" Inherits="ModuleUI_Appointment_Reports_OpenPurchaseOrderOverview"
    MaintainScrollPositionOnPostback="true" %>

<%@ Register TagName="ucDate2" TagPrefix="cc3" Src="~/CommonUI/UserControls/ucDate2.ascx" %>
<%@ Register TagName="ucDate" TagPrefix="cc2" Src="~/CommonUI/UserControls/ucDate.ascx" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectPO.ascx" TagName="MultiSelectPO"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectSite.ascx" TagName="MultiSelectSite"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectVendor.ascx" TagName="MultiSelectVendor"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectStockPlanner.ascx" TagName="MultiSelectStockPlanner"
    TagPrefix="cc1" %>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectCarrier.ascx" TagName="MultiSelectCarrier"
    TagPrefix="uc1" %>
<%@ Register Src="~/CommonUI/UserControls/ucVendorSelectionTemplate.ascx" TagName="VendorSelectTemplate"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectSKU.ascx" TagName="MultiSelectSKU"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/ucVikingSku.ascx" TagName="ucVikingSku"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectStockPlannerGrouping.ascx" TagName="MultiSelectStockPlannerGrouping"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectSKUGrouping.ascx" TagName="MultiSelectSKUGrouping"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectSubvdr.ascx" TagName="MultiSelectSubvdr"
    TagPrefix="cc1" %>
<%@ Register TagName="ucExportButton" TagPrefix="uc1" Src="~/CommonUI/UserControls/ucExportToExcel.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript" language="javascript">
            function ValidatePage() {
            if ($("[id$='rbDueBy']").is(":checked")) {
                if ($("[id$='txtUCDate']").val() == '') {
                    alert('<%=SelectDueByDate%>');
                    return false;
                }
            }

            if ($("[id$='rbBookedPO']").is(":checked")) {
                if ($("[id$='txtUCDateBooked']").val() == '') {
                    alert('<%=BookedPODate%>');
                    return false;
                }
            }

            var POCount = $("#ctl00_ContentPlaceHolder1_msPO_lstRight option").length;
            var SiteCount = $("#ctl00_ContentPlaceHolder1_msSite_lstRight option").length;
            var StockPlannerCount = $("#ctl00_ContentPlaceHolder1_msStockPlanner_ucLBRight option").length;
            var VendorCount = $("#ctl00_ContentPlaceHolder1_msVendor_lstRight option").length;
            var VikingSkuCount = $("#ctl00_ContentPlaceHolder1_UcVikingSku_lstRight option").length;
            var ODSkuCount = $("#ctl00_ContentPlaceHolder1_msSKU_lstRight option").length;
            var StockPlannerGroupingCount = $("#ctl00_ContentPlaceHolder1_multiSelectStockPlannerGrouping_lstRight option").length;
            var SKUGroupingCount = $("#ctl00_ContentPlaceHolder1_multiSelectSKUGrouping_lstRight option").length;

            if (POCount == 0) {
                if (StockPlannerCount == 0) {
                    if (VendorCount == 0) {
                        if (VikingSkuCount == 0) {
                            if (ODSkuCount == 0) {
                                if (StockPlannerGroupingCount == 0) {
                                    if (SKUGroupingCount == 0) {
                                        if (SiteCount == 0) {
                                            alert('<%=Pleaseaddsearchcriteria%>');
                                            return false;
                                        }
                                        else if (SiteCount > 4) {
                                            alert('<%=NewMaximumfoursitesareallowed%>');
                                            return false;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        $(document).ready(function () {
            $("body").keypress(function (e) {
                if (e.which == 13 || e.keyCode == 13) {
                    $('#<%=BtnGenerateReport.ClientID %>').click();
                    return false;
                }
            });
        });
    </script>
    <h2>
        <cc1:ucLabel ID="UcLabel5" runat="server"></cc1:ucLabel>
        <cc1:ucLabel ID="lblSearchResult" runat="server"></cc1:ucLabel>
    </h2>
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div class="right-shadow">
        <div class="formbox">
            <div class="button-row">
                <cc1:ucMultiView ID="mvDiscrepancyList" runat="server" ActiveViewIndex="0">
                    <cc1:ucView ID="vwSearchCreteria" runat="server">
                        <cc1:ucPanel ID="UcCarrierVendorPanel" runat="server">
                            <table align="center" border="0" cellpadding="0" cellspacing="5"
                                class="top-settings" width="100%">
                                <tr>
                                    <td align="right">
                                        <cc1:VendorSelectTemplate ID="ucVendorTemplateSelect" runat="server" />
                                    </td>
                                </tr>
                            </table>
                            <table align="center" border="0" cellpadding="0" cellspacing="5"
                                class="top-settings" width="100%">
                                <tr>
                                    <td colspan="3">
                                        <table>
                                            <tr>
                                                <td style="font-weight: bold;">
                                                    <cc1:ucLabel ID="lblPurchaseOrderNo1" runat="server"
                                                        Text="Purchase Order Number">
                                                    </cc1:ucLabel>
                                                </td>
                                                <td style="font-weight: bold;">
                                                    <cc1:ucLabel ID="UcLabel3" runat="server">:</cc1:ucLabel>
                                                </td>
                                                <td>
                                                    <cc1:MultiSelectPO ID="msPO" runat="server" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="font-weight: bold;">
                                                    <cc1:ucLabel ID="lblSite1" runat="server" Text="Sites">
                                                    </cc1:ucLabel>
                                                </td>
                                                <td style="font-weight: bold;">
                                                    <cc1:ucLabel ID="UcLabel4" runat="server">:</cc1:ucLabel>
                                                </td>
                                                <td>
                                                    <cc1:MultiSelectSite ID="msSite" runat="server" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="font-weight: bold;">
                                                    <cc1:ucLabel ID="lblInventoryManager1" runat="server" Text="Stock Planner">
                                                    </cc1:ucLabel>
                                                </td>
                                                <td style="font-weight: bold;">
                                                    <cc1:ucLabel ID="UcLabel2" runat="server">:</cc1:ucLabel>
                                                </td>
                                                <td>
                                                    <cc1:MultiSelectStockPlanner ID="msStockPlanner" runat="server" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="font-weight: bold; width: 10%">
                                                    <cc1:ucLabel ID="lblVendor" runat="server" Text="Vendor Name">
                                                    </cc1:ucLabel>
                                                </td>
                                                <td style="font-weight: bold; width: 1%">
                                                    <cc1:ucLabel ID="lblVendorCollon" runat="server">:</cc1:ucLabel>
                                                </td>
                                                <td>
                                                    <cc1:MultiSelectVendor ID="msVendor" runat="server" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="font-weight: bold;">
                                                    <cc1:ucLabel ID="UclblVikingSku" runat="server" Text="Viking Sku">
                                                    </cc1:ucLabel>
                                                </td>
                                                <td style="font-weight: bold;">
                                                    <cc1:ucLabel ID="UcLabel12" runat="server">:</cc1:ucLabel>
                                                </td>
                                                <td>
                                                    <cc1:ucVikingSku ID="UcVikingSku" runat="server" />
                                                </td>
                                            </tr>
                                            <%-----------------End-------------------------------%>
                                            <tr>
                                                <td style="font-weight: bold;">
                                                    <cc1:ucLabel ID="lblODSKUCode" runat="server" Text="OD SKU Code">
                                                    </cc1:ucLabel>
                                                </td>
                                                <td style="font-weight: bold;">
                                                    <cc1:ucLabel ID="UcLabel8" runat="server">:</cc1:ucLabel>
                                                </td>
                                                <td>
                                                    <cc1:MultiSelectSKU ID="msSKU" runat="server" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="font-weight: bold;">
                                                    <cc1:ucLabel ID="lblStockPlannerGroupings" runat="server">
                                                    </cc1:ucLabel>
                                                </td>
                                                <td style="font-weight: bold;">
                                                    <cc1:ucLabel ID="UcLabel55" runat="server">:</cc1:ucLabel>
                                                </td>
                                                <td>
                                                    <cc1:MultiSelectStockPlannerGrouping ID="multiSelectStockPlannerGrouping"
                                                        runat="server" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="font-weight: bold;">
                                                    <cc1:ucLabel ID="lblSKUgrouping" runat="server" Text="Sku Grouping">
                                                    </cc1:ucLabel>
                                                </td>
                                                <td style="font-weight: bold;">
                                                    <cc1:ucLabel ID="UcLabel81" runat="server">:</cc1:ucLabel>
                                                </td>
                                                <td>
                                                    <cc1:MultiSelectSKUGrouping runat="server" ID="multiSelectSKUGrouping" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="font-weight: bold;">
                                                    <cc1:ucLabel ID="lblSubVendor" runat="server" Text="Sub Vendor">
                                                    </cc1:ucLabel>
                                                </td>
                                                <td style="font-weight: bold;">
                                                    <cc1:ucLabel ID="UcLabel7" runat="server">:</cc1:ucLabel>
                                                </td>
                                                <td> 
                                                    <cc1:MultiSelectSubvdr runat="server" id="MultiSelectSubvdr" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="3">&#160;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="font-weight: bold; width: 10%">
                                                    <cc1:ucLabel ID="UcLabel1" runat="server" Text="Display">
                                                    </cc1:ucLabel>
                                                </td>
                                                <td style="font-weight: bold; width: 1%">
                                                    <cc1:ucLabel ID="lblVendorCollon2" runat="server">:</cc1:ucLabel>
                                                </td>
                                                <td align="left" style="font-weight: bold; width: 89%">
                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                        <tr>
                                                            <td width="50%">&#160;
                                                                <cc1:ucRadioButton ID="rbAll" runat="server" GroupName="Display" Text=" All" />&#160;
                                                                &#160;
                                                                <cc1:ucRadioButton ID="rbOOD" runat="server" GroupName="Display"
                                                                    Text=" Only Over Due" Width="100" />
                                                                &#160;
                                                                <cc1:ucRadioButton ID="rbDueBy" runat="server" GroupName="Display"
                                                                    Text=" Due By" />
                                                                <cc2:ucDate ID="txtDate" runat="server" ClientIDMode="Static" />
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server"
                                                                    ControlToValidate="txtDate$txtUCDate" Display="None" SetFocusOnError="true"
                                                                    ValidateEmptyText="true" ValidationGroup="a"></asp:RequiredFieldValidator>
                                                            </td>
                                                            <td align="right" width="44%">
                                                                <cc1:ucRadioButton ID="rbBookedPO" runat="server" GroupName="Display"
                                                                    Text=" Booked " />
                                                                &#160;
                                                                <cc3:ucDate2 ID="txtBookingDate" runat="server" ClientIDMode="Static" />
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server"
                                                                    ControlToValidate="txtBookingDate$txtUCDate2" Display="None"
                                                                    SetFocusOnError="true" ValidateEmptyText="true" ValidationGroup="a"></asp:RequiredFieldValidator>
                                                            </td>
                                                            <td width="6%">&#160;
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </cc1:ucPanel>
                    </cc1:ucView>
                    <cc1:ucView ID="vwSearchListing" runat="server">
                        <div class="button-row">
                            <uc1:ucExportButton ID="BtnExportToExcel" runat="server" />
                        </div>
                        <cc1:ucPanel
                            ID="pnlGrid" runat="server" CssClass="fieldset-form" ScrollBars="Both">
                            <%--                        <table width="100%" class="grid">
                            <tr>
                                <th align="left" style="padding-left:340px;"></th>
                            </tr>
                        </table>--%>
                            <cc1:ucGridView ID="GvDisLog" runat="server" AllowPaging="true"
                                AllowSorting="true" CssClass="grid"
                                OnPageIndexChanging="GvDisLog_PageIndexChanging"
                                OnRowDataBound="GvDisLog_RowDataBound" OnSorting="SortGrid"
                                PagerStyle-HorizontalAlign="Left" PageSize="100" Style="overflow: auto;"
                                Width="100%">
                                <Columns>
                                    <asp:BoundField AccessibleHeaderText="false" DataField="Vendor_No"
                                        HeaderText="Vendor No" ItemStyle-Wrap="false" SortExpression="Vendor_No">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField AccessibleHeaderText="false" DataField="Vendor_Name"
                                        HeaderText="Vendor Name" ItemStyle-Wrap="false" SortExpression="Vendor_Name">
                                        <HeaderStyle HorizontalAlign="Center" Width="100px" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:BoundField>
                                    <asp:BoundField AccessibleHeaderText="false" DataField="CreatedBy"
                                        HeaderText="Created By" ItemStyle-Wrap="false" SortExpression="CreatedBy">
                                        <HeaderStyle HorizontalAlign="Center" Width="100px" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField AccessibleHeaderText="false"
                                        DataField="StockPlannerGroupingName" HeaderText="Stock Planner Group"
                                        ItemStyle-Wrap="false" SortExpression="CreatedBy">
                                        <HeaderStyle HorizontalAlign="Center" Width="100px" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField AccessibleHeaderText="false" DataField="StockPlannerNo"
                                        HeaderText="Stock Planner No" ItemStyle-Wrap="false"
                                        SortExpression="StockPlannerNo">
                                        <HeaderStyle HorizontalAlign="Center" Width="100px" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="SiteName" HeaderText="Site" ItemStyle-Wrap="false"
                                        SortExpression="SiteName">
                                        <HeaderStyle HorizontalAlign="Center" Width="300px" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Purchase_order" HeaderText="PO Number"
                                        ItemStyle-Wrap="false" SortExpression="Purchase_order">
                                        <HeaderStyle HorizontalAlign="Center" Width="300px" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="ScheduleDateString"
                                        DataFormatString="{0:dd-MM-yyyy}" HeaderText="Earliest Advise Date"
                                        ItemStyle-Wrap="false" SortExpression="ScheduleDateString">
                                        <HeaderStyle HorizontalAlign="Center" Width="300px" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <%--Added for the Open PO Report filter and new field--%>
                                    <asp:BoundField DataField="OnTime" HeaderText="On Time" ItemStyle-Wrap="false"
                                        SortExpression="OnTime">
                                        <HeaderStyle HorizontalAlign="Center" Width="300px" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:BoundField>

                                    <asp:BoundField DataField="DeliveryStatus" HeaderText="DeliveryStatus"
                                        ItemStyle-Wrap="false" SortExpression="DeliveryStatus">
                                        <HeaderStyle HorizontalAlign="Center" Width="300px" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Order_raisedString"
                                        DataFormatString="{0:dd-MM-yyyy}" HeaderText="Order Date"
                                        ItemStyle-Wrap="false" SortExpression="Order_raisedString">
                                        <HeaderStyle HorizontalAlign="Center" Width="300px" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Original_due_dateString"
                                        DataFormatString="{0:dd-MM-yyyy}" HeaderText="Orignal Due Date"
                                        ItemStyle-Wrap="false" SortExpression="Original_due_dateString">
                                        <HeaderStyle HorizontalAlign="Center" Width="300px" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Expected_dateString"
                                        DataFormatString="{0:dd-MM-yyyy}" HeaderText="Expected Due Date"
                                        ItemStyle-Wrap="false" SortExpression="Expected_dateString">
                                        <HeaderStyle HorizontalAlign="Center" Width="80px" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="OD_Code" HeaderText="OD Code" ItemStyle-Wrap="false"
                                        SortExpression="OD_Code">
                                        <HeaderStyle HorizontalAlign="Center" Width="100px" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Direct_code" HeaderText="Viking Catalogue"
                                        ItemStyle-Wrap="false" SortExpression="Direct_code">
                                        <HeaderStyle HorizontalAlign="Center" Width="400px" />
                                        <ItemStyle HorizontalAlign="Center" Wrap="false" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Vendor_code" HeaderText="Vendor Catalogue Code"
                                        ItemStyle-Wrap="false" SortExpression="Vendor_code">
                                        <HeaderStyle HorizontalAlign="Center" Width="400px" />
                                        <ItemStyle HorizontalAlign="Center" Wrap="false" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Product_description" HeaderText="Description"
                                        ItemStyle-Wrap="false" SortExpression="Product_description">
                                        <HeaderStyle HorizontalAlign="Center" Width="100px" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:BoundField>
                                    <asp:BoundField AccessibleHeaderText="false" DataField="UOM" HeaderText="UOM"
                                        ItemStyle-Wrap="false" SortExpression="UOM">
                                        <HeaderStyle HorizontalAlign="Center" Width="100px" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Original_quantity" HeaderText="Order Qty"
                                        ItemStyle-Wrap="false" SortExpression="Original_quantity">
                                        <HeaderStyle HorizontalAlign="Center" Width="400px" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Outstanding_Qty" HeaderText="Remain Qty"
                                        ItemStyle-Wrap="false" SortExpression="Outstanding_Qty">
                                        <HeaderStyle HorizontalAlign="Center" Width="400px" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Qty_On_Hand" HeaderText="O/H Qty"
                                        ItemStyle-Wrap="false" SortExpression="Qty_On_Hand">
                                        <HeaderStyle HorizontalAlign="Center" Width="400px" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Total_Qty_BO" HeaderText="Total Qty BO" ItemStyle-Wrap="false"
                                        SortExpression="Total_Qty_BO">
                                        <HeaderStyle HorizontalAlign="Center" Width="400px" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Other" HeaderText="Remarks" ItemStyle-Wrap="false"
                                        SortExpression="Other">
                                        <HeaderStyle HorizontalAlign="Center" Width="400px" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:BoundField>
                                </Columns>
                            </cc1:ucGridView>
                            <cc1:ucGridView ID="TempGridView" runat="server"
                                OnRowDataBound="GvDisLog_RowDataBound" OnSorting="SortGrid"
                                Visible="false" Width="100%">
                                <Columns>
                                    <asp:BoundField AccessibleHeaderText="false" DataField="Vendor_No"
                                        HeaderText="Vendor No" ItemStyle-Wrap="false" SortExpression="Vendor_No">
                                        <HeaderStyle HorizontalAlign="Right" />
                                        <ItemStyle HorizontalAlign="Right" />
                                    </asp:BoundField>
                                    <asp:BoundField AccessibleHeaderText="false" DataField="Vendor_Name"
                                        HeaderText="Vendor Name" ItemStyle-Wrap="false" SortExpression="Vendor_Name">
                                        <HeaderStyle HorizontalAlign="Right" Width="100px" />
                                        <ItemStyle HorizontalAlign="Right" />
                                    </asp:BoundField>
                                    <asp:BoundField AccessibleHeaderText="false" DataField="CreatedBy"
                                        HeaderText="Created By" ItemStyle-Wrap="false" SortExpression="CreatedBy">
                                        <HeaderStyle HorizontalAlign="Right" Width="100px" />
                                        <ItemStyle HorizontalAlign="Right" />
                                    </asp:BoundField>
                                    <asp:BoundField AccessibleHeaderText="false"
                                        DataField="StockPlannerGroupingName" HeaderText="Stock Planner Group"
                                        ItemStyle-Wrap="false" SortExpression="CreatedBy">
                                        <HeaderStyle HorizontalAlign="Center" Width="100px" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField AccessibleHeaderText="false" DataField="StockPlannerNo"
                                        HeaderText="Stock Planner No" ItemStyle-Wrap="false"
                                        SortExpression="StockPlannerNo">
                                        <HeaderStyle HorizontalAlign="Right" Width="100px" />
                                        <ItemStyle HorizontalAlign="Right" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="SiteName" HeaderText="Site" ItemStyle-Wrap="false"
                                        SortExpression="SiteName">
                                        <HeaderStyle HorizontalAlign="Right" Width="300px" />
                                        <ItemStyle HorizontalAlign="Right" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Purchase_order" HeaderText="PO Number"
                                        ItemStyle-Wrap="false" SortExpression="Purchase_order">
                                        <HeaderStyle HorizontalAlign="Right" Width="300px" />
                                        <ItemStyle HorizontalAlign="Right" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="ScheduleDateString"
                                        DataFormatString="{0:dd-MM-yyyy}" HeaderText="Earliest Advise Date"
                                        ItemStyle-Wrap="false" SortExpression="ScheduleDateString">
                                        <HeaderStyle HorizontalAlign="Right" Width="300px" />
                                        <ItemStyle HorizontalAlign="Right" />
                                    </asp:BoundField>
                                    <%--Added for the Open PO Report filter and new field--%>
                                    <asp:BoundField DataField="OnTime" HeaderText="On Time" ItemStyle-Wrap="false"
                                        SortExpression="OnTime">
                                        <HeaderStyle HorizontalAlign="Center" Width="300px" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="DeliveryStatus" HeaderText="DeliveryStatus"
                                        ItemStyle-Wrap="false" SortExpression="DeliveryStatus">
                                        <HeaderStyle HorizontalAlign="Center" Width="300px" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Order_raisedString"
                                        DataFormatString="{0:dd-MM-yyyy}" HeaderText="Order Date"
                                        ItemStyle-Wrap="false" SortExpression="Order_raisedString">
                                        <HeaderStyle HorizontalAlign="Right" Width="300px" />
                                        <ItemStyle HorizontalAlign="Right" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Original_due_dateString"
                                        DataFormatString="{0:dd-MM-yyyy}" HeaderText="Orignal Due Date"
                                        ItemStyle-Wrap="false" SortExpression="Original_due_dateString">
                                        <HeaderStyle HorizontalAlign="Right" Width="300px" />
                                        <ItemStyle HorizontalAlign="Right" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Expected_dateString"
                                        DataFormatString="{0:dd-MM-yyyy}" HeaderText="Expected Due Date"
                                        ItemStyle-Wrap="false" SortExpression="Expected_dateString">
                                        <HeaderStyle HorizontalAlign="Right" Width="80px" />
                                        <ItemStyle HorizontalAlign="Right" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="OD_Code" HeaderText="OD Code" ItemStyle-Wrap="false"
                                        SortExpression="OD_Code">
                                        <HeaderStyle HorizontalAlign="Right" Width="100px" />
                                        <ItemStyle HorizontalAlign="Right" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Direct_code" HeaderText="Viking Catalogue"
                                        ItemStyle-Wrap="false" SortExpression="Direct_code">
                                        <HeaderStyle HorizontalAlign="Right" Width="400px" />
                                        <ItemStyle HorizontalAlign="Right" Wrap="false" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Vendor_code" HeaderText="Vendor Catalogue Code"
                                        ItemStyle-Wrap="false" SortExpression="Vendor_code">
                                        <HeaderStyle HorizontalAlign="Right" Width="400px" />
                                        <ItemStyle HorizontalAlign="Right" Wrap="false" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Product_description" HeaderText="Description"
                                        ItemStyle-Wrap="false" SortExpression="Product_description">
                                        <HeaderStyle HorizontalAlign="Right" Width="100px" />
                                        <ItemStyle HorizontalAlign="Right" />
                                    </asp:BoundField>
                                    <asp:BoundField AccessibleHeaderText="false" DataField="UOM" HeaderText="UOM"
                                        ItemStyle-Wrap="false" SortExpression="UOM">
                                        <HeaderStyle HorizontalAlign="Right" Width="100px" />
                                        <ItemStyle HorizontalAlign="Right" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Original_quantity" HeaderText="Order Qty"
                                        ItemStyle-Wrap="false" SortExpression="Original_quantity">
                                        <HeaderStyle HorizontalAlign="Right" Width="400px" />
                                        <ItemStyle HorizontalAlign="Right" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Outstanding_Qty" HeaderText="Remain Qty"
                                        ItemStyle-Wrap="false" SortExpression="Outstanding_Qty">
                                        <HeaderStyle HorizontalAlign="Right" Width="400px" />
                                        <ItemStyle HorizontalAlign="Right" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Qty_On_Hand" HeaderText="O/H Qty"
                                        ItemStyle-Wrap="false" SortExpression="Qty_On_Hand">
                                        <HeaderStyle HorizontalAlign="Right" Width="400px" />
                                        <ItemStyle HorizontalAlign="Right" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Total_Qty_BO" HeaderText="Total Qty BO" ItemStyle-Wrap="false"
                                        SortExpression="Total_Qty_BO">
                                        <HeaderStyle HorizontalAlign="Right" Width="400px" />
                                        <ItemStyle HorizontalAlign="Right" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Other" HeaderText="Remarks" ItemStyle-Wrap="false"
                                        SortExpression="Other">
                                        <HeaderStyle HorizontalAlign="Right" Width="400px" />
                                        <ItemStyle HorizontalAlign="Right" />
                                    </asp:BoundField>
                                </Columns>
                            </cc1:ucGridView>
                        </cc1:ucPanel>
                    </cc1:ucView>
                </cc1:ucMultiView>
            </div>
        </div>
        <div class="button-row">
            <cc1:ucButton ID="BtnGenerateReport" runat="server" Text="Search" CssClass="button"
                OnClientClick="return ValidatePage();" OnClick="BtnGenerateReport_Click" />
            <cc1:ucButton ID="BtnBackSearch" runat="server" Text="Back To Search Criteria" CssClass="button"
                OnClick="BtnBack_Click" />
        </div>
    </div>
</asp:Content>
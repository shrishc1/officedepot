﻿<%@ Page  Language="C#" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master" AutoEventWireup="true" CodeFile="ScheduledReceiptedReportByVendor.aspx.cs" Inherits="ModuleUI_Appointment_Reports_ScheduledReceiptedReportByVendor" %>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/ucSite.ascx" TagName="ucSite" TagPrefix="cc2" %>
<%@ Register Src="~/CommonUI/UserControls/ucDate.ascx" TagName="ucDate" TagPrefix="cc5" %>
<%@ Register TagName="ucExportButton" TagPrefix="uc1" Src="~/CommonUI/UserControls/ucExportToExcel.ascx" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
 <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <h2>
       
        <cc1:ucLabel ID="lblScheduledReceiptedReportVendor" runat="server"></cc1:ucLabel></h2>
        <script type="text/javascript">
            function ValidateVolumeDetails() {
                if ($("select[id$='ddlSite'] option:selected").val() > 0) {
                    return true;
                }
                else {
                    alert('<%=siteRequired%>');
                    $("select[id$='ddlSite']").focus();
                    return false;
                }
            } 
        </script>

        <div class="right-shadow">
            <div class="formbox">
                <asp:UpdatePanel ID="pnlUP1" runat="server">
                    <ContentTemplate>
                        <table width="90%" cellspacing="5" cellpadding="0" class="form-table">
                            <tr>
                                <td style="font-weight: bold; width: 10%;">
                                    <cc1:ucLabel ID="lblSite" runat="server"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold; width: 1%;">
                                    <cc1:ucLabel ID="UcLabel1" runat="server">:</cc1:ucLabel>
                                </td>
                                <td style="width: 21%;">
                                    <cc2:ucSite ID="ddlSite" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td style="font-weight: bold; width: 10%;">
                                    <cc1:ucLabel ID="lblDate" runat="server"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold; width: 1%;">
                                    <cc1:ucLabel ID="UcLabel3" runat="server">:</cc1:ucLabel>
                                </td>
                                <td style="width: 21%;">
                                    <cc5:ucDate ClientIDMode="Static" ID="txtDate" runat="server" />
                                    <asp:RequiredFieldValidator ID="rfvDateRequired" runat="server" Display="None" ControlToValidate="txtDate$txtUCDate"
                                        ValidateEmptyText="true" ValidationGroup="a" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;    <cc1:ucButton ID="btnSearch" runat="server" Text="Search" CssClass="button" OnClick="btnSearch_Click"
                                      OnClientClick="return ValidateVolumeDetails();" />
                                </td>
                                 <td colspan="4"> 
                                     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                                     &nbsp;&nbsp; 
                                   <uc1:ucexportbutton ID="btnExportToExcel1" runat="server" />    
                                 </td>
                            </tr>
                                 <tr>
                            <td colspan="4">
                            <asp:Label ID="lblError" runat="server"></asp:Label>
                                <cc1:ucGridView ID="UcGridView1" Width="100%" runat="server" CssClass="grid" OnSorting="SortGrid"
                                    AllowSorting="true">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Vendor" SortExpression="VendorName">
                                            <HeaderStyle Width="10%" HorizontalAlign="Left" />
                                            <ItemStyle HorizontalAlign="center" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblVendor" runat="server" Text='<%#Eval("VendorName") %>'></asp:Label>                                             
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="# Lines Due">
                                            <HeaderStyle Width="10%" HorizontalAlign="Left" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblLinesDue" runat="server" Text='<%#Eval("LinesDue") %>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="center" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="# Lines Booked">
                                            <HeaderStyle Width="10%" HorizontalAlign="Left" />
                                            <ItemStyle HorizontalAlign="center" />
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkLinesBooked" runat="server" Text='<%#Eval("LinesBooked") %>' CommandArgument='<%#Eval("SiteId") + "," + Eval("VendorID")+"," + Eval("VendorName")%>'
                                                 OnClick="lnkLinesBooked_Click" ></asp:LinkButton>
                                                   <%--NavigateUrl='<%# EncryptQuery("ScheduledReceiptedReportBySiteAndVendor.aspx?SiteID="+Eval("SiteId")+"&VendorID="+ Eval("VendorID")+"") %>'--%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="# Lines Receipted">
                                            <HeaderStyle Width="10%" HorizontalAlign="Left" />
                                            <ItemStyle HorizontalAlign="center" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblLinesReceipted" runat="server" Text='<%#Eval("LinesReceipted") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </cc1:ucGridView>                               
                                 <div class="button-row">
                                 <cc1:PagerV2_8 ID="pager1" runat="server"  OnCommand="pager_Command" GenerateGoToSection="false">
                                  </cc1:PagerV2_8>
                                 </div>  
                            </td>
                        </tr>
                        </table>
                    </ContentTemplate>
                     <Triggers>           
                 <asp:PostBackTrigger ControlID="btnExportToExcel1" />        
                    </Triggers>
                </asp:UpdatePanel>
            </div>
        </div>  
</asp:Content>

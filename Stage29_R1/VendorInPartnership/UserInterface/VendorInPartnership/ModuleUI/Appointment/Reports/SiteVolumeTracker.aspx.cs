﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.Appointment.Reports;
using BusinessLogicLayer.ModuleBAL.Appointment.Reports;
using WebUtilities;
using System.IO;

public partial class SiteVolumeTracker : CommonPage
{
    bool IsExportClicked = false;

    public int WeekCounter
    {
        get
        {
            return ViewState["WeekCounter"] != null ? Convert.ToInt32(ViewState["WeekCounter"]) : 0;
        }
        set
        {
            ViewState["WeekCounter"] = value;
        }
    }

    protected void Page_InIt(object sender, EventArgs e)
    {
        ddlSite.CurrentPage = this;

    }

    //public override void SitePost_Load() {
    //    base.SitePost_Load();

    //    if (!Page.IsPostBack) {
    //        //ddlSite.innerControlddlSite.AutoPostBack = true;
    //        //BindReport();
    //    }
    //}

    protected void Page_Load(object sender, EventArgs e)
    {


    }

    protected void btnGo_Click(object sender, EventArgs e)
    {
        WeekCounter = 0;
        BindReport();
    }

    public override void SiteSelectedIndexChanged()
    {
        //BindReport(); 
    }

    List<SiteVolumeTrackerBE> oSiteVolumeTrackerBEList = new List<SiteVolumeTrackerBE>();
    private void BindReport(bool isExport = false)
    {


        SiteVolumeTrackerBE oSiteVolumeTrackerBE = new SiteVolumeTrackerBE();
        SiteVolumeTrackerBAL oSiteVolumeTrackerBAL = new SiteVolumeTrackerBAL();

        oSiteVolumeTrackerBE.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
        if (rblReportType.SelectedIndex == 0)
        {
            divcapacity.Visible = true;
            UcGridView1.Visible = false;
            btnExportToExcel.Visible = true;

            oSiteVolumeTrackerBE.Action = "CapacityReport";
            oSiteVolumeTrackerBE.WeekCounter = WeekCounter;

            oSiteVolumeTrackerBEList = oSiteVolumeTrackerBAL.GetSiteVolumeReportDetailsBAL(oSiteVolumeTrackerBE);

            var oSiteVolumeTracker = oSiteVolumeTrackerBEList.OrderBy(o => o.CalculationDate).Select(c => new { c.CalculationDate }).Distinct().ToList();

            rptDate.DataSource = oSiteVolumeTracker;
            rptDate.DataBind();

            if (WeekCounter < 0)
            {
                oSiteVolumeTrackerBEList = oSiteVolumeTrackerBEList.Where(c => c.Area != "Due Lines").ToList();
            }

            var oSiteVolumeTrackerArea = oSiteVolumeTrackerBEList.OrderBy(o => o.CalculationDate).Select(c => new { c.Area }).Distinct().ToList();

            if (isExport == false)
            {
                rptSiteVolTrackerReports.DataSource = oSiteVolumeTrackerArea;
                rptSiteVolTrackerReports.DataBind();
            }
            else
            {
                rptSiteVolTrackerReportsExport.DataSource = oSiteVolumeTrackerArea;
                rptSiteVolTrackerReportsExport.DataBind();
                rptDateExport.DataSource = oSiteVolumeTracker;
                rptDateExport.DataBind();                
            }

            //rptSiteVolTrackerReports.DataSource = oSiteVolumeTrackerArea;
            //rptSiteVolTrackerReports.DataBind();

            oSiteVolumeTrackerBEList = null;

            lnkLeft.Visible = lnkRight.Visible = true;
        }
        else
        {
            divcapacity.Visible = false;
            UcGridView1.Visible = true;

            oSiteVolumeTrackerBE.Action = "BacklogProjection";
            if (rdoDateType.SelectedValue == "1")
            {
                oSiteVolumeTrackerBE.DateType = 1;
            }
            else if (rdoDateType.SelectedValue == "2")
            {
                oSiteVolumeTrackerBE.DateType = 2;
            }

            oSiteVolumeTrackerBEList = oSiteVolumeTrackerBAL.GetSiteVolumeReportDetailsBAL(oSiteVolumeTrackerBE);

            //var oSiteVolumeTracker = oSiteVolumeTrackerBEList.OrderBy(o => o.CalculationDate).ToList();

            UcGridView1.DataSource = oSiteVolumeTrackerBEList;
            UcGridView1.DataBind();

            if (oSiteVolumeTrackerBEList.Count > 0)
            {
                btnExportToExcel.Visible = true;
            }
            else
            {
                btnExportToExcel.Visible = false;
            }

            oSiteVolumeTrackerBEList = null;
        }


    }

    protected void rptSiteVolTrackerReports_OnItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {

            Literal ltArea = (Literal)e.Item.FindControl("ltArea");
            if ((Literal)e.Item.FindControl("ltInfo") != null)
            {
                Literal ltInfo = (Literal)e.Item.FindControl("ltInfo");
                SetInfoText(ref ltInfo, ltArea.Text);
            }


            //List<SiteVolumeTrackerBE> oSiteVolumeTrackerBEList = (List<SiteVolumeTrackerBE>)ViewState["oSiteVolumeTrackerBEList"];


            //var oVIPManagementReport = oSiteVolumeTrackerBEList.Where(c => c.Area == ltArea.Text).ToList();


            Repeater rptkeyDetails = (Repeater)e.Item.FindControl("rptkeyDetails");

            var uniqueAreaKeys = oSiteVolumeTrackerBEList.Where(c => c.Area == ltArea.Text).Select(c => new { c.AreaKey, c.KeyOrder }).Distinct().ToList(); // .OrderBy(o => o.KeyOrder).ToList();
            rptkeyDetails.DataSource = uniqueAreaKeys;
            rptkeyDetails.DataBind();
        }
    }

    protected void rptkeyDetails_OnItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {

            //List<SiteVolumeTrackerBE> oSiteVolumeTrackerBEList = (List<SiteVolumeTrackerBE>)ViewState["oSiteVolumeTrackerBEList"];

            Repeater rptKeyvalue = (Repeater)e.Item.FindControl("rptKeyvalue");
            Literal ltAreaKey = (Literal)e.Item.FindControl("ltAreaKey");


            var oSiteVolumeTrackerBE = oSiteVolumeTrackerBEList.Where(c => c.AreaKey == ltAreaKey.Text).OrderBy(o => o.CalculationDate).ToList();

            rptKeyvalue.DataSource = oSiteVolumeTrackerBE;
            rptKeyvalue.DataBind();


        }
    }

    protected void rptKeyvalue_OnItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        //return;
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Literal hdnAreaKey = (Literal)e.Item.FindControl("hdnAreaKey");
            Literal ltDate = (Literal)e.Item.FindControl("ltDate");
            Literal ltKeyvalue = (Literal)e.Item.FindControl("ltKeyvalue");

            DateTime dt = Utilities.Common.TextToDateFormat(ltDate.Text);

            if (IsExportClicked == false)
            {

                switch (hdnAreaKey.Text)
                {
                    case "Lines Due":
                    case "Booked on Due Date":
                    case "Booked Late":
                    case "Booked Early":
                    case "Not Booked":
                    case "Overdue":

                    case "Confirmed Lines":
                    case "Early":
                    case "Late":
                    case "Max Plts":
                    // case "Confirmed Lines":
                    case "Total Bookings":
                    case "Unconfirmed Fixed Slots":
                    case "Unconfirmed":
                    case "On Due Date":
                        HyperLink hpLink = (HyperLink)e.Item.FindControl("hpLink");


                        ltKeyvalue.Visible = false;
                        hpLink.Visible = true;

                        if (hdnAreaKey.Text == "Total Bookings")
                            hpLink.NavigateUrl = Utilities.Common.EncryptQuery("../Booking/APPBok_BookingOverview.aspx?SVT=true&SiteID=" + ddlSite.innerControlddlSite.SelectedItem.Value + "&ReportDate=" + ltDate.Text);


                        if (hdnAreaKey.Text == "Max Plts")
                            hpLink.NavigateUrl = Utilities.Common.EncryptQuery("../Booking/APPBok_BookingVolumeDetail.aspx?SVT=true&SiteID=" + ddlSite.innerControlddlSite.SelectedItem.Value + "&ReportDate=" + ltDate.Text);

                        if (hdnAreaKey.Text == "Confirmed Lines")
                            hpLink.NavigateUrl = Utilities.Common.EncryptQuery("../Reports/OpenPurchaseOrderOverview.aspx?SVT=ConfirmedLines&SiteID=" + ddlSite.innerControlddlSite.SelectedItem.Value + "&ReportDate=" + ltDate.Text);

                        if (hdnAreaKey.Text == "Early")
                            hpLink.NavigateUrl = Utilities.Common.EncryptQuery("../Reports/OpenPurchaseOrderOverview.aspx?SVT=Early&SiteID=" + ddlSite.innerControlddlSite.SelectedItem.Value + "&ReportDate=" + ltDate.Text);

                        if (hdnAreaKey.Text == "Late")
                            hpLink.NavigateUrl = Utilities.Common.EncryptQuery("../Reports/OpenPurchaseOrderOverview.aspx?SVT=Late&SiteID=" + ddlSite.innerControlddlSite.SelectedItem.Value + "&ReportDate=" + ltDate.Text);

                        if (hdnAreaKey.Text == "Lines Due")
                            hpLink.NavigateUrl = Utilities.Common.EncryptQuery("../Reports/OpenPurchaseOrderOverview.aspx?SVT=LinesDue &SiteID=" + ddlSite.innerControlddlSite.SelectedItem.Value + "&ReportDate=" + ltDate.Text);


                        if (hdnAreaKey.Text == "Booked on Due Date")
                            hpLink.NavigateUrl = Utilities.Common.EncryptQuery("../Reports/OpenPurchaseOrderOverview.aspx?SVT=BookedDueDate&SiteID=" + ddlSite.innerControlddlSite.SelectedItem.Value + "&ReportDate=" + ltDate.Text);


                        if (hdnAreaKey.Text == "Booked Late")
                            hpLink.NavigateUrl = Utilities.Common.EncryptQuery("../Reports/OpenPurchaseOrderOverview.aspx?SVT=BookedLate &SiteID=" + ddlSite.innerControlddlSite.SelectedItem.Value + "&ReportDate=" + ltDate.Text);

                        if (hdnAreaKey.Text == "Booked Early")
                            hpLink.NavigateUrl = Utilities.Common.EncryptQuery("../Reports/OpenPurchaseOrderOverview.aspx?SVT=BookedEarly &SiteID=" + ddlSite.innerControlddlSite.SelectedItem.Value + "&ReportDate=" + ltDate.Text);


                        if (hdnAreaKey.Text == "Not Booked")
                            hpLink.NavigateUrl = Utilities.Common.EncryptQuery("../Reports/OpenPurchaseOrderOverview.aspx?SVT=NotBooked &SiteID=" + ddlSite.innerControlddlSite.SelectedItem.Value + "&ReportDate=" + ltDate.Text);

                        if (hdnAreaKey.Text == "Overdue")
                            hpLink.NavigateUrl = Utilities.Common.EncryptQuery("../Reports/OpenPurchaseOrderOverview.aspx?SVT=Overdue&SiteID=" + ddlSite.innerControlddlSite.SelectedItem.Value + "&ReportDate=" + ltDate.Text);


                        else if (hdnAreaKey.Text == "Unconfirmed Fixed Slots")
                            hpLink.NavigateUrl = Utilities.Common.EncryptQuery("../Booking/APPBok_BookingOverview.aspx?SVT=true&BookingStatus=unconfirmed&SiteID=" + ddlSite.innerControlddlSite.SelectedItem.Value + "&ReportDate=" + ltDate.Text);

                        else if (hdnAreaKey.Text == "Unconfirmed")
                            hpLink.NavigateUrl = Utilities.Common.EncryptQuery("../Booking/APPBok_BookingOverview.aspx?SVT=true&BookingStatus=unconfirmed&SiteID=" + ddlSite.innerControlddlSite.SelectedItem.Value + "&ReportDate=" + ltDate.Text);
                        else if (hdnAreaKey.Text == "On Due Date")
                            hpLink.NavigateUrl = Utilities.Common.EncryptQuery("../Reports/OpenPurchaseOrderOverview.aspx?SVT=OnDueDate&SiteID=" + ddlSite.innerControlddlSite.SelectedItem.Value + "&ReportDate=" + ltDate.Text);

                        break;


                    case "Total Plts":
                    case "Confirmed Plts":

                        var oKey1 = oSiteVolumeTrackerBEList.Where(c => c.CalculationDate == dt && c.AreaKey == "Max Plts").ToList();

                        if (oKey1[0].KeyValue < Convert.ToDouble(ltKeyvalue.Text))
                        {
                            ltKeyvalue.Text = "<font style='color : red;font-weight: bold;font-size: larger;'>" + ltKeyvalue.Text + "</font>";
                        }
                        break;

                    case "Total Lines":
                        // case "Confirmed Lines":
                        oKey1 = oSiteVolumeTrackerBEList.Where(c => c.CalculationDate == dt && c.AreaKey == "Max Lines").ToList();

                        if (oKey1[0].KeyValue < Convert.ToDouble(ltKeyvalue.Text))
                        {
                            ltKeyvalue.Text = "<font style='color : red;font-weight: bold;font-size: larger;'>" + ltKeyvalue.Text + "</font>";
                        }
                        break;
                }
            }
            else
            {
                switch (hdnAreaKey.Text)
                {
                    case "Lines Due":
                    case "Booked on Due Date":
                    case "Booked Late":
                    case "Booked Early":
                    case "Not Booked":
                    case "Overdue":

                    case "Confirmed Lines":
                    case "Early":
                    case "Late":
                    case "Max Plts":
                    // case "Confirmed Lines":
                    case "Total Bookings":
                    case "Unconfirmed Fixed Slots":
                    case "Unconfirmed":
                    case "On Due Date":                       

                        Literal ltLink = (Literal)e.Item.FindControl("ltLink");

                        ltKeyvalue.Visible = false;
                        ltLink.Visible = true;

                        break;

                    case "Total Plts":
                    case "Confirmed Plts":

                        var oKey1 = oSiteVolumeTrackerBEList.Where(c => c.CalculationDate == dt && c.AreaKey == "Max Plts").ToList();

                        if (oKey1[0].KeyValue < Convert.ToDouble(ltKeyvalue.Text))
                        {
                            ltKeyvalue.Text = "<font style='color : red;font-weight: bold;font-size: larger;'>" + ltKeyvalue.Text + "</font>";
                        }
                        break;

                    case "Total Lines":
                        // case "Confirmed Lines":
                        oKey1 = oSiteVolumeTrackerBEList.Where(c => c.CalculationDate == dt && c.AreaKey == "Max Lines").ToList();

                        if (oKey1[0].KeyValue < Convert.ToDouble(ltKeyvalue.Text))
                        {
                            ltKeyvalue.Text = "<font style='color : red;font-weight: bold;font-size: larger;'>" + ltKeyvalue.Text + "</font>";
                        }
                        break;

                }
            }

            //if (hdnAreaKey.Text == "Total Bookings" || hdnAreaKey.Text == "Unconfirmed Booking" || hdnAreaKey.Text == "On Due Date") {

            //    }

            ////List<SiteVolumeTrackerBE> oSiteVolumeTrackerBEList = (List<SiteVolumeTrackerBE>)ViewState["oSiteVolumeTrackerBEList"];

            //if (hdnAreaKey.Text == "Total Plts" || hdnAreaKey.Text == "Confirmed Plts") {


            //}


            //if (hdnAreaKey.Text == "Total Lines" || hdnAreaKey.Text == "Confirmed Lines") {


            //}


        }
    }

    private void SetInfoText(ref Literal ltInfo, string strArea)
    {
        switch (strArea)
        {
            case "Capacity Settings":
                ltInfo.Text = "This is the max capacities set by the site. <br><br>When the total expected volume is > than either of these two value Vendors will not be able to make a booking on the day in question.";
                break;
            case "Bookings":
                ltInfo.Text = "This is a count of the number of deliveries due in for the day in question.<br><br>Please click on hyperlink to view either the bookings or the unconfirmed bookings.";
                break;
            case "Pallets/Cartons":
                ltInfo.Text = "This supplies an overview on the number of pallets due to come in on a given day.<br><br>If the Total Pallets count is > than the max pallets defined by the site Vendor will be unable to make booking for the day in question.";
                break;
            case "Due Lines":
                ltInfo.Text = "This supplies an overview on the number of line due to come in on a given day.<br><br>If the Total Lines count is > than the max lines defined by the site Vendor will be unable to make booking for the day in question.<br><br>To view the scheduled PO please click on the hyperlink.";
                break;
            case "Hours":
                ltInfo.Text = "Based on the transaction time assigned by the site for each various activities this is the minimum required hours to process the planned volume.";
                break;
        }
    }

    protected void lnkLeft_Click(object sender, EventArgs e)
    {
        WeekCounter--;
        BindReport();
    }

    protected void lnkRight_Click(object sender, EventArgs e)
    {
        WeekCounter++;
        BindReport();
    }

    protected void btnExport_Click(object sender, EventArgs e)
    {
        IsExportClicked = true;
        BindReport(isExport: true);

        if (rblReportType.SelectedValue == "2")
        {
            if (rdoDateType.SelectedValue == "1")
            {
                WebCommon.ExportHideHidden("SiteVolumeTracker_BackLogProj_RevisedDate_Report", UcGridView1, null, false, true);
            }
            else
            {
                WebCommon.ExportHideHidden("SiteVolumeTracker_BackLogProj_OriginalDueDate_Report", UcGridView1, null, false, true);
            }
        }
        else
        {
            //WebCommon.ExportHideHidden("ISPM15_Detailed_Report", UcGridView1, null, false, true);
            Response.ClearContent();
            Response.Buffer = true;
            Response.ContentType = "application/force-download";
            Response.AddHeader("content-disposition", "attachment; filename=SiteVolumeTrackerReport.xls");
            Response.Write("<html xmlns:x=\"urn:schemas-microsoft-com:office:excel\">");
            Response.Write("<head>");
            Response.Write("<META http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">");
            Response.Write("<!--[if gte mso 9]><xml>");
            Response.Write("<x:ExcelWorkbook>");
            Response.Write("<x:ExcelWorksheets>");
            Response.Write("<x:ExcelWorksheet>");
            Response.Write("<x:Name>Site Volume Tracker Capacity Report</x:Name>");
            Response.Write("<x:WorksheetOptions>");
            Response.Write("<x:Print>");
            Response.Write("<x:ValidPrinterInfo/>");
            Response.Write("</x:Print>");
            Response.Write("</x:WorksheetOptions>");
            Response.Write("</x:ExcelWorksheet>");
            Response.Write("</x:ExcelWorksheets>");
            Response.Write("</x:ExcelWorkbook>");
            Response.Write("</xml>");
            Response.Write("<![endif]--> ");
            StringWriter tw = new StringWriter(); HtmlTextWriter hw = new HtmlTextWriter(tw); divExport.RenderControl(hw);
            Response.Write(tw.ToString());
            Response.Write("</head>");
            Response.End();
        }
    }
}
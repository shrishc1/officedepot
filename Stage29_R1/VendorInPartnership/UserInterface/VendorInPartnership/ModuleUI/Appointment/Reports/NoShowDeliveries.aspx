﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    AutoEventWireup="true" CodeFile="NoShowDeliveries.aspx.cs" Inherits="NoShowDeliveries" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register TagName="ucDate" TagPrefix="cc2" Src="~/CommonUI/UserControls/ucDate.ascx" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectSite.ascx" TagName="MultiSelectSite"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectVendor.ascx" TagName="MultiSelectVendor"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectCountry.ascx" TagName="MultiSelectCountry"
    TagPrefix="cc1" %>
<%@ Register TagName="ucExportButton" TagPrefix="uc1" Src="~/CommonUI/UserControls/ucExportToExcel.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript" language="javascript">
        $(document).ready(function () {
            $("body").keypress(function (e) {
                if (e.which == 13 || e.keyCode == 13) {
                    $('#<%=btnSearch.ClientID %>').click();
                    return false;
                }
            });
        });
        function setValue1(target) {
            document.getElementById('<%=hdnJSFromDt.ClientID %>').value = target.value;
        }

        function setValue2(target) {
            document.getElementById('<%=hdnJSToDt.ClientID %>').value = target.value;
        }
        $(document).ready(function () {
            var gridlength = $(".grid tr").length;
            //alert(gridlength);
            if (gridlength < 2) {
                $(".gridouter").css('width', 'auto')
            } else {
                $(".gridouter").css('width', '1200px')
            }
        });
    </script>
    <h2>
        <cc1:ucLabel ID="lblNoShowDeliveries" Text="No Show Deliveries" runat="server"></cc1:ucLabel>
    </h2>
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:HiddenField ID="hdnJSFromDt" runat="server" />
    <asp:HiddenField ID="hdnJSToDt" runat="server" />
    <div class="right-shadow">
        <div class="formbox">
            <cc1:ucMultiView ID="mvDiscrepancyList" runat="server" ActiveViewIndex="0">
                <cc1:ucView ID="vwSearchCreteria" runat="server">
                    <cc1:ucPanel ID="UcSearchPanel" runat="server"><table width="100%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings"><tr><td style="font-weight: bold; width: 10%;"><cc1:ucLabel ID="lblCountry" runat="server" Text="Country"></cc1:ucLabel></td><td style="font-weight: bold; width: 2%;"><cc1:ucLabel ID="UcLabel3" runat="server">:</cc1:ucLabel></td><td style="font-weight: bold; width: 88%;"><cc1:MultiSelectCountry runat="server" ID="msCountry" /></td></tr><tr><td style="font-weight: bold;"><cc1:ucLabel ID="lblSite" runat="server" Text="Site"></cc1:ucLabel></td><td style="font-weight: bold;"><cc1:ucLabel ID="UcLabel4" runat="server">:</cc1:ucLabel></td><td><cc1:MultiSelectSite runat="server" ID="msSite" /></td></tr><tr><td style="font-weight: bold;"><cc1:ucLabel ID="lblVendor" runat="server" Text="Vendor"></cc1:ucLabel></td><td style="font-weight: bold;"><cc1:ucLabel ID="lblVendorCollon" runat="server">:</cc1:ucLabel></td><td><cc1:MultiSelectVendor runat="server" ID="msVendor" /></td></tr><tr><td style="font-weight: bold;"><cc1:ucLabel ID="lblFrom" runat="server" Text="From"></cc1:ucLabel></td><td style="font-weight: bold;"><cc1:ucLabel ID="UcLabel1" runat="server">:</cc1:ucLabel></td><td align="left"><span style="font-weight: bold; width: 6em; margin-left: 4px;"><cc1:ucTextbox ID="txtFromDate" runat="server" ClientIDMode="Static" CssClass="date"
                                            onchange="setValue1(this)" ReadOnly="True" Width="70px" /></span><span style="font-weight: bold; width: 4em; text-align: center"><cc1:ucLabel ID="lblTo" runat="server" Text="To"></cc1:ucLabel></span><span style="font-weight: bold;
                                            width: 6em;"><cc1:ucTextbox ID="txtToDate" runat="server" ClientIDMode="Static" CssClass="date"
                                                onchange="setValue2(this)" ReadOnly="True" Width="70px" /></span></td></tr><tr><td align="right" colspan="3"><div class="button-row"><cc1:ucButton ID="btnSearch" runat="server" Text="Search" CssClass="button" OnClick="btnSearch_Click" /></div></td></tr></table></cc1:ucPanel>
                </cc1:ucView>
                <cc1:ucView ID="vwSummaryView" runat="server">
                    <div class="button-row">
                        <uc1:ucExportButton ID="btnExportToExcelSummary" runat="server" Visible="false" />
                    </div>
                    <div style="overflow-x: auto;">
                        <cc1:ucPanel ID="pnlSummary" runat="server" CssClass="gridouter fieldset-form"><cc1:ucGridView ID="gvNoShowDeliveries" runat="server" CssClass="grid" OnSorting="SortGrid"
                                PageSize="50" AllowPaging="true" PagerStyle-HorizontalAlign="Left" AllowSorting="true"
                                Style="overflow: auto;" OnPageIndexChanging="gvNoShowDeliveries_PageIndexChanging" Width="100%"><EmptyDataTemplate><div style="text-align:center"><cc1:ucLabel ID="lblRecordNotFound" isRequired="true" runat="server" Text="No record found"></cc1:ucLabel></div></EmptyDataTemplate><Columns><asp:TemplateField HeaderText="Site"><HeaderStyle Width="100px" HorizontalAlign="Left" /><ItemStyle HorizontalAlign="Left" /><ItemTemplate><cc1:ucLiteral ID="lblSiteNameValue" runat="server" Text='<%#Eval("SiteName") %>'></cc1:ucLiteral></ItemTemplate></asp:TemplateField><asp:TemplateField HeaderText="Vendor"><HeaderStyle Width="150px" HorizontalAlign="Left" /><ItemStyle HorizontalAlign="Left" /><ItemTemplate><cc1:ucLiteral ID="lblVendorNameValue" runat="server" Text='<%#Eval("Vendor.VendorName") %>'></cc1:ucLiteral></ItemTemplate></asp:TemplateField><asp:TemplateField HeaderText="Carrier"><HeaderStyle Width="150px" HorizontalAlign="Left" /><ItemStyle HorizontalAlign="Left" /><ItemTemplate><cc1:ucLiteral ID="lblCarrierNameValue" runat="server" Text='<%#Eval("Carrier.CarrierName") %>'></cc1:ucLiteral></ItemTemplate></asp:TemplateField><asp:TemplateField HeaderText="BookingReference"><HeaderStyle Width="150px" HorizontalAlign="Center" /><ItemStyle HorizontalAlign="Center" /><ItemTemplate><cc1:ucLiteral ID="lblBookingRefValue" runat="server" Text='<%#Eval("BookingRef") %>'></cc1:ucLiteral></ItemTemplate></asp:TemplateField><asp:TemplateField HeaderText="Date"><HeaderStyle Width="100px" HorizontalAlign="Center" /><ItemStyle HorizontalAlign="Center" /><ItemTemplate><cc1:ucLiteral ID="lblScheduleDateValue" runat="server" Text='<%#Eval("ScheduleDate","{0:dd/MM/yyyy}") %>'></cc1:ucLiteral></ItemTemplate></asp:TemplateField><asp:TemplateField HeaderText="DayTime"><HeaderStyle Width="100px" HorizontalAlign="Center" /><ItemStyle HorizontalAlign="Center" /><ItemTemplate><cc1:ucLiteral ID="lblSlotTimeValue" runat="server" Text='<%#Eval("SlotTime.SlotTime") %>'></cc1:ucLiteral></ItemTemplate></asp:TemplateField><asp:TemplateField HeaderText="BookedBy"><HeaderStyle Width="150px" HorizontalAlign="Left" /><ItemStyle HorizontalAlign="Left" /><ItemTemplate><cc1:ucLiteral ID="lblProvBookedByValue" runat="server" Text='<%#Eval("ProvBookedBy") %>'></cc1:ucLiteral></ItemTemplate></asp:TemplateField><asp:TemplateField HeaderText="NoShowBy"><HeaderStyle Width="150px" HorizontalAlign="Left" /><ItemStyle HorizontalAlign="Left" /><ItemTemplate><cc1:ucLiteral ID="lblNoShowByUserValue" runat="server" Text='<%#Eval("NoShowByUser") %>'></cc1:ucLiteral></ItemTemplate></asp:TemplateField><asp:TemplateField HeaderText="NoShowTime"><HeaderStyle Width="150px" HorizontalAlign="Center" /><ItemStyle HorizontalAlign="Center" /><ItemTemplate><cc1:ucLiteral ID="lblNoShowTimeValue" runat="server" Text='<%#Eval("NoShowDate", "{0:dd/MM/yyyy}") + "  " + Eval("NoShowDate",@"{0:HH:mm}")%>'></cc1:ucLiteral></ItemTemplate></asp:TemplateField></Columns></cc1:ucGridView><cc1:ucGridView ID="gvNoShowDeliveriesExcel" runat="server" CssClass="grid" Style="overflow: auto;"
                                Visible="false"><EmptyDataTemplate><div style="text-align: center"><cc1:ucLabel ID="lblRecordNotFound" isRequired="true" runat="server" Text="No record found"></cc1:ucLabel></div></EmptyDataTemplate><Columns><asp:TemplateField HeaderText="Site"><HeaderStyle Width="100px" HorizontalAlign="Left" /><ItemStyle HorizontalAlign="Left" /><ItemTemplate><cc1:ucLiteral ID="lblSiteNameValue" runat="server" Text='<%#Eval("SiteName") %>'></cc1:ucLiteral></ItemTemplate></asp:TemplateField><asp:TemplateField HeaderText="Vendor"><HeaderStyle Width="150px" HorizontalAlign="Left" /><ItemStyle HorizontalAlign="Left" /><ItemTemplate><cc1:ucLiteral ID="lblVendorNameValue" runat="server" Text='<%#Eval("Vendor.VendorName") %>'></cc1:ucLiteral></ItemTemplate></asp:TemplateField><asp:TemplateField HeaderText="Carrier"><HeaderStyle Width="150px" HorizontalAlign="Left" /><ItemStyle HorizontalAlign="Left" /><ItemTemplate><cc1:ucLiteral ID="lblCarrierNameValue" runat="server" Text='<%#Eval("Carrier.CarrierName") %>'></cc1:ucLiteral></ItemTemplate></asp:TemplateField><asp:TemplateField HeaderText="BookingReference"><HeaderStyle Width="150px" HorizontalAlign="Center" /><ItemStyle HorizontalAlign="Center" /><ItemTemplate><cc1:ucLiteral ID="lblBookingRefValue" runat="server" Text='<%#Eval("BookingRef") %>'></cc1:ucLiteral></ItemTemplate></asp:TemplateField><asp:TemplateField HeaderText="Date"><HeaderStyle Width="100px" HorizontalAlign="Center" /><ItemStyle HorizontalAlign="Center" /><ItemTemplate><cc1:ucLiteral ID="lblScheduleDateValue" runat="server" Text='<%#Eval("ScheduleDate","{0:dd/MM/yyyy}") %>'></cc1:ucLiteral></ItemTemplate></asp:TemplateField><asp:TemplateField HeaderText="DayTime"><HeaderStyle Width="100px" HorizontalAlign="Center" /><ItemStyle HorizontalAlign="Center" /><ItemTemplate><cc1:ucLiteral ID="lblSlotTimeValue" runat="server" Text='<%#Eval("SlotTime.SlotTime") %>'></cc1:ucLiteral></ItemTemplate></asp:TemplateField><asp:TemplateField HeaderText="BookedBy"><HeaderStyle Width="150px" HorizontalAlign="Left" /><ItemStyle HorizontalAlign="Left" /><ItemTemplate><cc1:ucLiteral ID="lblProvBookedByValue" runat="server" Text='<%#Eval("ProvBookedBy") %>'></cc1:ucLiteral></ItemTemplate></asp:TemplateField><asp:TemplateField HeaderText="NoShowBy"><HeaderStyle Width="150px" HorizontalAlign="Left" /><ItemStyle HorizontalAlign="Left" /><ItemTemplate><cc1:ucLiteral ID="lblNoShowByUserValue" runat="server" Text='<%#Eval("NoShowByUser") %>'></cc1:ucLiteral></ItemTemplate></asp:TemplateField><asp:TemplateField HeaderText="NoShowTime"><HeaderStyle Width="150px" HorizontalAlign="Center" /><ItemStyle HorizontalAlign="Center" /><ItemTemplate><cc1:ucLiteral ID="lblNoShowTimeValue" runat="server" Text='<%#Eval("NoShowDate", "{0:dd/MM/yyyy}") + "  " + Eval("NoShowDate",@"{0:HH:mm}")%>'></cc1:ucLiteral></ItemTemplate></asp:TemplateField></Columns></cc1:ucGridView></cc1:ucPanel>
                    </div>
                </cc1:ucView>
            </cc1:ucMultiView>
            <table style="width: 100%;">
                <tr>
                    <td align="right">
                        <div class="button-row">
                            <cc1:ucButton ID="btnBackSearch" runat="server" Text="Back To Search Page" CssClass="button"
                                OnClick="btnBackSearch_Click" Visible="false" />
                        </div>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>

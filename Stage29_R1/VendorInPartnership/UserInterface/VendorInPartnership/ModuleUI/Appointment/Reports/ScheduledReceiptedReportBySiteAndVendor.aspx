﻿<%@ Page Language="C#" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master" AutoEventWireup="true" CodeFile="ScheduledReceiptedReportBySiteAndVendor.aspx.cs" Inherits="ModuleUI_Appointment_Reports_ScheduledReceiptedReportBySiteAndVendor" %>


<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/ucSite.ascx" TagName="ucSite" TagPrefix="cc2" %>
<%@ Register Src="~/CommonUI/UserControls/ucDate.ascx" TagName="ucDate" TagPrefix="cc5" %>
<%@ Register Src="~/CommonUI/UserControls/ucSeacrhVendor.ascx" TagName="ucSeacrhVendor"
    TagPrefix="cc3" %>
<%@ Register TagName="ucExportButton" TagPrefix="uc1" Src="~/CommonUI/UserControls/ucExportToExcel.ascx" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h2>
        <cc1:ucLabel ID="lblScheduledReceiptedReportVendorSite" runat="server"></cc1:ucLabel>
    </h2>
        <script type="text/javascript">
            function Validate() {             
              if ($("select[id$='ddlSite'] option:selected").val() == 0) {
                    alert('<%=siteRequired%>');
                    $("select[id$='ddlSite']").focus();
                    return false;
                }
                if ($("#<%=ucSeacrhVendor1.FindControl("txtVendorNo").ClientID%>").val()=="") {
                    alert('<%=VendorReq%>');                   
                    return false;
                }               
            } 
        </script>
        <div class="right-shadow">
            <div class="formbox">
                <asp:UpdatePanel ID="pnlUP1" runat="server">
                    <ContentTemplate>
                        <table width="90%" cellspacing="5" cellpadding="0" class="form-table">
                            <tr>
                                <td style="font-weight: bold;">
                                    <cc1:ucLabel ID="lblSite" runat="server"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold;">
                                    <cc1:ucLabel ID="UcLabel1" runat="server">:</cc1:ucLabel>
                                </td>
                                <td style>
                                    <cc2:ucSite ID="ddlSite" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td style="font-weight: bold;">
                                    <cc1:ucLabel ID="lblDate" runat="server"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold;">
                                    <cc1:ucLabel ID="UcLabel3" runat="server">:</cc1:ucLabel>
                                </td>
                                <td style>
                                    <cc5:ucDate ClientIDMode="Static" ID="txtDate" runat="server" />
                                    <asp:RequiredFieldValidator ID="rfvDateRequired" runat="server" Display="None" ControlToValidate="txtDate$txtUCDate"
                                        ValidateEmptyText="true" ValidationGroup="a" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td style="font-weight: bold;">
                                    <cc1:ucLabel ID="lblVendorNo" runat="server" Text="Vendor No"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold;" >
                                    :
                                </td>
                                <td style="font-weight: bold;">
                                    <span id="spVender" runat="server">
                                        <cc3:ucSeacrhVendor ID="ucSeacrhVendor1" runat="server" />
                                    </span>
                                    
                                    <cc1:ucLiteral ID="ltSearchVendorName"  runat="server" Visible="false"></cc1:ucLiteral>
                                    
                                
                                </td>
                                <td colspan="4"> 
                                <span><cc1:ucButton ID="btnSearch" runat="server" Text="Search" CssClass="button"
                                      OnClientClick="return Validate();" OnClick="btnSearch_Click"/></span>
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; 
                                   <uc1:ucexportbutton ID="btnExportToExcel1" runat="server" />    
                                 </td>
                            </tr>
                        </tr>
                            <tr>
                                <td colspan="4">
                                    <asp:Label ID="lblError" runat="server"></asp:Label>
                                    <cc1:ucGridView ID="UcGridView1" runat="server" AllowSorting="true" 
                                        CssClass="grid" OnSorting="SortGrid" Width="100%" 
                                        onrowdatabound="UcGridView1_RowDataBound">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Booking Ref #">
                                                <HeaderStyle HorizontalAlign="Left" Width="10%" />
                                                <ItemStyle HorizontalAlign="center" />
                                                <ItemTemplate>
                                                    <asp:Label ID="lblBookingReferenceID" runat="server" Text='<%#Eval("BookingReferenceID") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Lines Booked">
                                                <HeaderStyle HorizontalAlign="Left" Width="10%" />
                                                <ItemTemplate>
                                                    <asp:Label ID="lblLinesBooked" runat="server" Text='<%#Eval("LinesBooked") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="center" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Total Receipted from PO linked to booking">
                                                <HeaderStyle HorizontalAlign="Left" Width="10%" />
                                                <ItemStyle HorizontalAlign="center" />
                                                <ItemTemplate>
                                                 <asp:Label ID="lblTotalReceipt" runat="server" Text='<%#Eval("TotalReceipt") %>'></asp:Label>                                                  
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="PO #">
                                                <HeaderStyle HorizontalAlign="Left" Width="10%" />
                                                <ItemStyle HorizontalAlign="center" />
                                                <ItemTemplate>
                                                    <asp:Label ID="lblPO_NO" runat="server" Text='<%#Eval("PO_NO") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Lines Receipted">
                                                <HeaderStyle HorizontalAlign="Left" Width="10%" />
                                                <ItemStyle HorizontalAlign="center" />
                                                <ItemTemplate>
                                                     <asp:Label ID="lblLinesReceipted" runat="server" Text='<%#Eval("LinesReceipted") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </cc1:ucGridView>                                   
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                     <Triggers>           
                 <asp:PostBackTrigger ControlID="btnExportToExcel1" />        
                    </Triggers>
                </asp:UpdatePanel>
            </div>
        </div>  
</asp:Content>

﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    AutoEventWireup="true" CodeFile="ScheduledPOReconciliation.aspx.cs" Inherits="ScheduledPOReconciliation"
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectSite.ascx" TagName="MultiSelectSite"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectVendor.ascx" TagName="MultiSelectVendor"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/ucVendorSelectionTemplate.ascx" TagName="VendorSelectTemplate"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectPO.ascx" TagName="MultiSelectPO"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectSKU.ascx" TagName="MultiSelectSKU"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/ucVikingSku.ascx" TagName="ucVikingSku"
    TagPrefix="cc1" %>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register TagName="ucDate" TagPrefix="cc2" Src="~/CommonUI/UserControls/ucBackDate.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript" language="javascript">
        $(document).ready(function () {
            $("body").keypress(function (e) {
                if (e.which == 13 || e.keyCode == 13) {
                    $('#<%=btnGenerateReport.ClientID %>').click();
                    return false;
                }
            });
        });
        var gridCheckedCount = 0;

        $(document).ready(function () {
            SelectAllGrid($("#chkInitiateChase")[0]);
        });


        function setValue1(target) {
            document.getElementById('<%=hdnJSFromDt.ClientID %>').value = target.value;
        }

        function setValue2(target) {
            document.getElementById('<%=hdnJSToDt.ClientID %>').value = target.value;
        }

        function SelectAllGrid(chkBoxAllObj) {
            if (chkBoxAllObj.checked) {
                $("input[type='checkbox'][name$='chkSelect']").attr('checked', true);
                gridCheckedCount = $("input[type='checkbox'][name$='chkSelect']:checked").size();
            }
            else {
                $("input[type='checkbox'][name$='chkSelect']").attr('checked', false);
                gridCheckedCount = 0;
            }
        }

        function CheckUncheckAllCheckBoxAsNeeded() {

            if ($("input[type='checkbox'][name$='chkSelect']:checked").size() == $("input[type='checkbox'][name$='chkSelect']").length) {
                $("#chkInitiateChase").attr('checked', true);
                gridCheckedCount = $("#<%=gvPOReCon.ClientID%> input[id*='chkSelect']:checkbox:checked").size();
            }
            else {
                $("#chkInitiateChase").attr('checked', false);
                gridCheckedCount = $("#<%=gvPOReCon.ClientID%> input[id*='chkSelect']:checkbox:checked").size();
            }
        }

        function CheckAtleastOneCheckBox() {
            var selectCheckboxToAccept = '<%=selectCheckboxToAccept%>';

            if (gridCheckedCount == 0) {
                alert(selectCheckboxToAccept);
                return false
            }
        }




    </script>
    <asp:ScriptManager ID="SM1" runat="server">
    </asp:ScriptManager>
    <asp:HiddenField ID="hdnJSFromDt" runat="server" />
    <asp:HiddenField ID="hdnJSToDt" runat="server" />
    <asp:HiddenField ID="hdnSiteIDs" runat="server" />
    <asp:HiddenField ID="hdnCountryIDs" runat="server" />
    <asp:HiddenField ID="hdnVendorIDs" runat="server" />
    <asp:HiddenField ID="hdnCarrierIDs" runat="server" />
    <asp:HiddenField ID="hdnSupplierType" runat="server" />
    <asp:HiddenField ID="hdnFromDt" runat="server" />
    <asp:HiddenField ID="hdnToDt" runat="server" />
    <h2>
        <cc1:ucLabel ID="lblScheduledPOReconciliationReport" runat="server" Text="Scheduled Purchase Order Reconciliation Report"></cc1:ucLabel>
    </h2>
    <div class="right-shadow">
        <div class="formbox">
            <cc1:ucPanel ID="ucpnlsearch" runat="server">
                <table width="100%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                    <tr>
                        <td align="right">
                            <cc1:VendorSelectTemplate runat="server" ID="ucVendorTemplateSelect" />
                        </td>
                    </tr>
                </table>
                <table width="100%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                    <tr>
                        <td>
                            <table>
                                <tr>
                                    <td style="font-weight: bold; width: 10%">
                                        <cc1:ucLabel ID="lblVendor" runat="server" Text="Vendor">
                                        </cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold; width: 1%">
                                        :
                                    </td>
                                    <td>
                                        <cc1:MultiSelectVendor runat="server" ID="msVendor" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="font-weight: bold;">
                                        <cc1:ucLabel ID="lblSite" runat="server" Text="Site">
                                        </cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold;">
                                        :
                                    </td>
                                    <td>
                                        <cc1:MultiSelectSite runat="server" ID="msSite" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="font-weight: bold;">
                                        <cc1:ucLabel ID="lblVikingSku" runat="server">
                                        </cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold;">
                                        :
                                    </td>
                                    <td>
                                        <cc1:ucVikingSku ID="UcVikingSku" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="font-weight: bold;">
                                        <cc1:ucLabel ID="lblODSKUCode" runat="server">
                                        </cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold;">
                                       :
                                    </td>
                                    <td>
                                        <cc1:MultiSelectSKU ID="msSKU" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="font-weight: bold;">
                                        <cc1:ucLabel ID="lblPurchaseOrderNo" runat="server">
                                        </cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold;">
                                        :
                                    </td>
                                    <td>
                                        <cc1:MultiSelectPO ID="msPO" runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table width="100%">
                                <tr>
                                    <td style="font-weight: bold; width: 81px">
                                        <cc1:ucLabel ID="lblDateFrom" runat="server" Text="Date From">
                                        </cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold; width: 1%">
                                        :
                                    </td>
                                    <td align="left">
                                        <table width="25%" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style="font-weight: bold; width: 6em;">
                                                    <cc1:ucTextbox ID="txtFromDate" runat="server" ClientIDMode="Static" CssClass="date"
                                                        onchange="setValue1(this)" ReadOnly="True" Width="70px" />
                                                    <%-- <cc2:ucDate ClientIDMode="Static" ID="txtFromDate" runat="server" onchange="setValue1(this)"
                                                        ReadOnly="True" Width="70px" />--%>
                                                </td>
                                                <td style="font-weight: bold; width: 4em; text-align: center">
                                                    <cc1:ucLabel ID="lblTo" runat="server" Text="To">
                                                    </cc1:ucLabel>
                                                </td>
                                                <td style="font-weight: bold; width: 6em;">
                                                    <cc1:ucTextbox ID="txtToDate" runat="server" ClientIDMode="Static" CssClass="date"
                                                        onchange="setValue2(this)" ReadOnly="True" Width="70px" />
                                                    <%--<cc2:ucDate ClientIDMode="Static" ID="txtToDate" runat="server" onchange="setValue2(this)"  Class=" readonly="readonly" id="Text1" class="date hasDatepicker inputbox-readonly"
                                                        ReadOnly="True" Width="70px" />--%>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table width="100%">
                                <tr>
                                    <td style="font-weight: bold; width: 81px">
                                        <cc1:ucLabel runat="server" ID="lblType" />
                                    </td>
                                    <td style="font-weight: bold; width: 1%">
                                        :
                                    </td>
                                    <td style="width: 800px;" class="nobold radiobuttonlist">
                                        <cc1:ucRadioButtonList ID="rblType" CssClass="radio-fix" runat="server" RepeatColumns="3"
                                            RepeatDirection="Horizontal">
                                        </cc1:ucRadioButtonList>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table width="100%">
                                <tr>
                                    <td style="font-weight: bold; width: 81px">
                                        <cc1:ucLabel runat="server" ID="lblChaseStatus" Text="Chase Status" />
                                    </td>
                                    <td style="font-weight: bold; width: 1%">
                                        :
                                    </td>
                                    <td style="width: 800px;" class="nobold radiobuttonlist">
                                        <cc1:ucRadioButtonList ID="rblChaseStatus" CssClass="radio-fix" runat="server" RepeatColumns="3"
                                            RepeatDirection="Horizontal">
                                        </cc1:ucRadioButtonList>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <div class="button-row">
                                <cc1:ucButton ID="btnGenerateReport" runat="server" Text="Generate Report" CssClass="button"
                                    OnClick="btnGenerateReport_Click" />
                            </div>
                        </td>
                    </tr>
                </table>
            </cc1:ucPanel>
            <cc1:ucPanel ID="ucpnlResult" runat="server" Visible="false">
                <div class="button-row">
                    <cc1:ucButton ID="btnExportToExcel" runat="server" CssClass="exporttoexcel" OnClick="btnExport_Click"
                        Width="109px" Height="20px" />
                </div>
                <table width="100%">
                    <tr>
                        <td align="center" colspan="9">
                            <cc1:ucGridView ID="gvPOReCon" Width="100%" runat="server" CssClass="grid gvclass searchgrid-1" 
                                OnPageIndexChanging="gvPOReCon_PageIndexChanging" OnRowDataBound="gvPOReCon_RowDataBound">
                                <emptydatatemplate>
                                    <div style="text-align: center">
                                        <cc1:ucLabel ID="lblNoData" runat="server" Text="No Records found."></cc1:ucLabel>
                                    </div>
                                </emptydatatemplate>
                                <columns>
                                    <asp:TemplateField HeaderText="SelectAllCheckBox">
                                        <HeaderStyle HorizontalAlign="Left" Width="30px" Font-Size="11px" />
                                        <ItemStyle Width="30px" HorizontalAlign="Left" VerticalAlign="Middle" />
                                        <HeaderTemplate>
                                            <cc1:ucCheckbox runat="server" ID="chkInitiateChase" CssClass="checkbox-input" onclick="SelectAllGrid(this);"
                                                TextAlign="Left" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:HiddenField ID="hdnPOId" Value='<%# Eval("PurchaseOrder.PurchaseOrderID") %>'
                                                runat="server" />
                                            <asp:HiddenField ID="hdnBookingId" Value='<%# Eval("BookingID") %>' runat="server" />
                                            <asp:CheckBox runat="server" ViewStateMode="Enabled" ID="chkSelect" onclick="CheckUncheckAllCheckBoxAsNeeded();"
                                                Visible='<%# (Convert.ToString( Eval("PurchaseOrder.ChaseStatus")) == "0") ? true: false %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Site">
                                        <HeaderStyle Width="50px" HorizontalAlign="Left" Font-Size="11px" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="ltSite" Text='<%#Eval("SiteName") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" Width="50px" Font-Size="11px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="BookingReference">
                                        <HeaderStyle Width="90px" HorizontalAlign="Left" Font-Size="11px" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="ltBookingRef" Text='<%#Eval("BookingRef") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" Width="90px" Font-Size="11px" />
                                    </asp:TemplateField>

                                     <asp:TemplateField HeaderText="BookingTime">
                                            <HeaderStyle Width="40px" HorizontalAlign="Center" Font-Size="11px" />
                                            <ItemStyle HorizontalAlign="Center" Width="40px" Font-Size="11px"  />
                                            <ItemTemplate>                                              
                                               <cc1:ucLabel ID="lblTimePOInq" runat="server" Text='<%# Eval("SlotTime.SlotTime") %>'></cc1:ucLabel>&nbsp;
                                                    <cc1:ucLabel ID="ltdayname" runat="server" Text='<%# Eval("ScheduleDate", "{0:dddd}")%>'></cc1:ucLabel>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                    <asp:TemplateField HeaderText="DeliveryStatus">
                                        <HeaderStyle Width="50px" HorizontalAlign="Left" Font-Size="11px" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="ltDeliveryStatus" Text='<%#Eval("BookingStatus") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" Width="50px" Font-Size="11px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="VendorName">
                                        <HeaderStyle Width="70px" HorizontalAlign="Left" Font-Size="11px" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="ltVendorName" Text='<%#Eval("Vendor.VendorName") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" Width="70px" Font-Size="11px" />
                                    </asp:TemplateField>

                                      <asp:TemplateField HeaderText="Carrier Name">
                                        <HeaderStyle Width="70px" HorizontalAlign="Left" Font-Size="11px" />
                                        <ItemTemplate>
                                           <cc1:ucLabel runat="server" ID="lt1Carrier" Text='<%#Eval("Carrier.CarrierName") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" Width="70px" Font-Size="11px" />
                                    </asp:TemplateField>

                                     <asp:TemplateField HeaderText="Pallets">
                                        <HeaderStyle Width="40px" HorizontalAlign="Left" Font-Size="11px" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="ltNumberOfPallet" Text='<%#Eval("NoOfPallets") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" Width="40px" Font-Size="11px" />
                                    </asp:TemplateField>

                                     <asp:TemplateField HeaderText="Cartons">
                                        <HeaderStyle Width="40px" HorizontalAlign="Left" Font-Size="11px" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="NumberOfCartons" Text='<%#Eval("NoOfCartoons") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" Width="40px" Font-Size="11px" />
                                    </asp:TemplateField>
                                    
                                    <asp:TemplateField HeaderText="PurchaseOrderNumber">
                                        <HeaderStyle Width="40px" HorizontalAlign="Left" Font-Size="11px" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="ltPoNumber" Text='<%#Eval("PurchaseOrder.Purchase_order") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" Width="40px" Font-Size="11px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Schedulingdate">
                                        <HeaderStyle Width="40px" HorizontalAlign="Left" Font-Size="11px" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="ltScheduleDate" Text='<%#Eval("ScheduleDate","{0:dd/MM/yyyy}") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" Width="40px" Font-Size="11px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="DueDate">
                                        <HeaderStyle Width="40px" HorizontalAlign="Left" Font-Size="11px" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="ltDueDate" Text='<%#Eval("DueDate","{0:dd/MM/yyyy}") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" Width="40px" Font-Size="11px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="DeliveryDate">
                                        <HeaderStyle Width="40px" HorizontalAlign="Left" Font-Size="11px" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="ltDeliveryDate" Text='<%#Eval("DeliveryDate","{0:dd/MM/yyyy}") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" Width="40px" Font-Size="11px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="ChaseStatus">
                                        <HeaderStyle Width="40px" HorizontalAlign="Left" Font-Size="11px" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="ltPOStatus" Text='<%#Eval("PurchaseOrder.Status") %>'
                                                Visible="false"></cc1:ucLabel>
                                            <asp:HyperLink ID="hlPOStatus" runat="server" Text='<%# Eval("PurchaseOrder.Status") %>'
                                                Visible="false"></asp:HyperLink>
                                            <asp:HiddenField ID="hdnChaseStatus" Value='<%# Eval("PurchaseOrder.ChaseStatus") %>'
                                                runat="server" />
                                            <asp:HiddenField ID="hdnOrderRaised" Value='<%# Eval("PurchaseOrder.Order_raised") %>'
                                                runat="server" />
                                            <asp:HiddenField ID="hdnSiteId" Value='<%# Eval("SiteId") %>' runat="server" />
                                            <asp:HiddenField ID="hdnVendorId" Value='<%# Eval("VendorID") %>' runat="server" />
                                            <cc1:ucLabel runat="server" ID="ltCarrier" Text='<%#Eval("Carrier.CarrierName") %>'
                                                Visible="false"></cc1:ucLabel>
                                            <asp:HiddenField ID="hdnMultiPONo" Value='<%# Eval("MultiPONO") %>' runat="server" />
                                            <asp:HiddenField ID="hdnMultiPOIDs" Value='<%# Eval("MultiPOIDs") %>' runat="server" />
                                            <asp:HiddenField ID="hdnPOChaseCommunicationId" Value='<%# Eval("POChaseCommunicationId") %>'
                                                runat="server" />
                                            <asp:HiddenField ID="hdnCountry" Value='<%# Eval("Country") %>' runat="server" />
                                            <asp:HiddenField ID="hdnIsPOManuallyAdded" Value='<%# Eval("PurchaseOrder.IsPOManuallyAdded") %>'
                                                runat="server" />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" Width="40px" Font-Size="11px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="ScheduledNotBookedInComma">
                                        <HeaderStyle Width="40px" HorizontalAlign="Left" Font-Size="11px" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="ltSchuduleNotBookedIn" Text='<%#Eval("ScheduledNotBookedIn") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" Width="40px" Font-Size="11px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="NotScheduledBookedIn">
                                        <HeaderStyle Width="40px" HorizontalAlign="Left" Font-Size="11px" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="ltNotScheduledBookedIn" Text='<%#Eval("NotScheduledBookedIn") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" Width="40px" Font-Size="11px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="ApplyHit">
                                        <HeaderStyle Width="50px" HorizontalAlign="Left" Font-Size="11px" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="ltApplyHit" Visible="false"></cc1:ucLabel>
                                            <asp:HyperLink ID="hlApplyHit" runat="server" Visible="false"></asp:HyperLink>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" Width="50px" Font-Size="11px" />
                                    </asp:TemplateField>
                                </columns>
                            </cc1:ucGridView>

                            <div class="button-row">
                            <cc1:PagerV2_8 ID="pager1" runat="server"  OnCommand="pager_Command" GenerateGoToSection="false">
                            </cc1:PagerV2_8>
                            </div>  

                            <cc1:ucGridView ID="gvPoReConExcel" Visible="false" Width="100%" runat="server" CssClass="grid"
                                OnRowDataBound="gvPOReCon_RowDataBound">
                                <emptydatatemplate>
                                    <div style="text-align: center">
                                        <cc1:ucLabel ID="lblNoData" runat="server" Text="No Records found."></cc1:ucLabel>
                                    </div>
                                </emptydatatemplate>
                                <columns>
                                    <asp:TemplateField HeaderText="SelectAllCheckBox">
                                        <HeaderStyle HorizontalAlign="Left" Width="30px" Font-Size="11px" />
                                        <ItemStyle Width="30px" HorizontalAlign="Left" VerticalAlign="Middle" />
                                        <HeaderTemplate>
                                            <cc1:ucCheckbox runat="server" ID="chkInitiateChase" CssClass="checkbox-input" onclick="SelectAllGrid(this);"
                                                TextAlign="Left" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:HiddenField ID="hdnPOId" Value='<%# Eval("PurchaseOrder.PurchaseOrderID") %>'
                                                runat="server" />
                                            <asp:HiddenField ID="hdnBookingId" Value='<%# Eval("BookingID") %>' runat="server" />
                                            <asp:CheckBox runat="server" ViewStateMode="Enabled" ID="chkSelect" onclick="CheckUncheckAllCheckBoxAsNeeded();"
                                                Visible='<%# (Convert.ToString( Eval("PurchaseOrder.ChaseStatus")) == "0") ? true: false %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Site">
                                        <HeaderStyle Width="50px" HorizontalAlign="Left" Font-Size="11px" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="ltSite" Text='<%#Eval("SiteName") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" Width="50px" Font-Size="11px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="BookingReference">
                                        <HeaderStyle Width="90px" HorizontalAlign="Left" Font-Size="11px" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="ltBookingRef" Text='<%#Eval("BookingRef") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" Width="90px" Font-Size="11px" />
                                    </asp:TemplateField>

                                     <asp:TemplateField HeaderText="BookingTime">
                                            <HeaderStyle Width="40px" HorizontalAlign="Center" Font-Size="11px" />
                                            <ItemStyle HorizontalAlign="Center" Width="40px" Font-Size="11px"  />
                                            <ItemTemplate>                                              
                                               <cc1:ucLabel ID="lblTimePOInq" runat="server" Text='<%# Eval("SlotTime.SlotTime") %>'></cc1:ucLabel>&nbsp;
                                                    <cc1:ucLabel ID="ltdayname" runat="server" Text='<%# Eval("ScheduleDate", "{0:dddd}")%>'></cc1:ucLabel>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                    <asp:TemplateField HeaderText="DeliveryStatus">
                                        <HeaderStyle Width="50px" HorizontalAlign="Left" Font-Size="11px" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="ltDeliveryStatus" Text='<%#Eval("BookingStatus") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" Width="50px" Font-Size="11px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="VendorName">
                                        <HeaderStyle Width="70px" HorizontalAlign="Left" Font-Size="11px" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="ltVendorName" Text='<%#Eval("Vendor.VendorName") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" Width="70px" Font-Size="11px" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Carrier Name">
                                        <HeaderStyle Width="70px" HorizontalAlign="Left" Font-Size="11px" />
                                        <ItemTemplate>
                                           <cc1:ucLabel runat="server" ID="lt1Carrier" Text='<%#Eval("Carrier.CarrierName") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" Width="70px" Font-Size="11px" />
                                    </asp:TemplateField>

                                     <asp:TemplateField HeaderText="Pallets">
                                        <HeaderStyle Width="40px" HorizontalAlign="Left" Font-Size="11px" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="ltNumberOfPallet" Text='<%#Eval("NoOfPallets") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" Width="40px" Font-Size="11px" />
                                    </asp:TemplateField>

                                     <asp:TemplateField HeaderText="Cartons">
                                        <HeaderStyle Width="40px" HorizontalAlign="Left" Font-Size="11px" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="NumberOfCartons" Text='<%#Eval("NoOfCartoons") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" Width="40px" Font-Size="11px" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="PurchaseOrderNumber">
                                        <HeaderStyle Width="40px" HorizontalAlign="Left" Font-Size="11px" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="ltPoNumber" Text='<%#Eval("PurchaseOrder.Purchase_order") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" Width="40px" Font-Size="11px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Schedulingdate">
                                        <HeaderStyle Width="40px" HorizontalAlign="Left" Font-Size="11px" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="ltScheduleDate" Text='<%#Eval("ScheduleDate","{0:dd/MM/yyyy}") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" Width="40px" Font-Size="11px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="DueDate">
                                        <HeaderStyle Width="40px" HorizontalAlign="Left" Font-Size="11px" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="ltDueDate" Text='<%#Eval("DueDate","{0:dd/MM/yyyy}") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" Width="40px" Font-Size="11px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="DeliveryDate">
                                        <HeaderStyle Width="40px" HorizontalAlign="Left" Font-Size="11px" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="ltDeliveryDate" Text='<%#Eval("DeliveryDate","{0:dd/MM/yyyy}") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" Width="40px" Font-Size="11px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="ChaseStatus">
                                        <HeaderStyle Width="40px" HorizontalAlign="Left" Font-Size="11px" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="ltPOStatus" Text='<%#Eval("PurchaseOrder.Status") %>'
                                                Visible="false"></cc1:ucLabel>
                                            <asp:HyperLink ID="hlPOStatus" runat="server" 
                                                Visible="false" ></asp:HyperLink>
                                            <asp:HiddenField ID="hdnChaseStatus" Value='<%# Eval("PurchaseOrder.ChaseStatus") %>'
                                                runat="server" />
                                            <asp:HiddenField ID="hdnOrderRaised" Value='<%# Eval("PurchaseOrder.Order_raised") %>'
                                                runat="server" />
                                            <asp:HiddenField ID="hdnSiteId" Value='<%# Eval("SiteId") %>' runat="server" />
                                            <asp:HiddenField ID="hdnVendorId" Value='<%# Eval("VendorID") %>' runat="server" />
                                            <cc1:ucLabel runat="server" ID="ltCarrier" 
                                                Visible="false" ></cc1:ucLabel>
                                            <asp:HiddenField ID="hdnMultiPONo" Value='<%# Eval("MultiPONO") %>' runat="server" />
                                            <asp:HiddenField ID="hdnMultiPOIDs" Value='<%# Eval("MultiPOIDs") %>' runat="server" />
                                            <asp:HiddenField ID="hdnPOChaseCommunicationId" Value='<%# Eval("POChaseCommunicationId") %>'
                                                runat="server" />
                                            <asp:HiddenField ID="hdnCountry" Value='<%# Eval("Country") %>' runat="server" />
                                            <asp:HiddenField ID="hdnIsPOManuallyAdded" Value='<%# Eval("PurchaseOrder.IsPOManuallyAdded") %>'
                                                runat="server" />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" Width="40px" Font-Size="11px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="ScheduledNotBookedInComma">
                                        <HeaderStyle Width="40px" HorizontalAlign="Left" Font-Size="11px" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="ltSchuduleNotBookedIn" Text='<%#Eval("ScheduledNotBookedIn") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" Width="40px" Font-Size="11px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="NotScheduledBookedIn">
                                        <HeaderStyle Width="40px" HorizontalAlign="Left" Font-Size="11px" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="ltNotScheduledBookedIn" Text='<%#Eval("NotScheduledBookedIn") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" Width="40px" Font-Size="11px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="ApplyHit" Visible="false">
                                        <HeaderStyle Width="50px" HorizontalAlign="Left" Font-Size="11px" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="ltApplyHit" Visible="false"></cc1:ucLabel>
                                            <asp:HyperLink ID="hlApplyHit" runat="server" Visible="false"></asp:HyperLink>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" Width="50px" Font-Size="11px" />
                                    </asp:TemplateField>
                                </columns>
                            </cc1:ucGridView>
                        </td>
                    </tr>
                </table>
                <table width="100%" cellspacing="1" cellpadding="0" border="0" align="center" class="top-settingsnoborder">
                    <tr>
                        <td align="right">
                            <div class="button-row">
                                <cc1:ucButton ID="btnChase" runat="server" Text="Chase" CssClass="button" OnClick="btnChase_Click"
                                    OnClientClick="return CheckAtleastOneCheckBox();" />
                                
                                <cc1:ucButton ID="btnNoAction" runat="server" Text="No Action" CssClass="button" OnClick="btnNoAction_Click" OnClientClick="return CheckAtleastOneCheckBox();"
                                     />

                                <cc1:ucButton ID="btnBack" runat="server" Text="Back" CssClass="button" OnClick="btnBack_Click" />
                            </div>
                        </td>
                    </tr>
                </table>
            </cc1:ucPanel>
        </div>
    </div>
</asp:Content>

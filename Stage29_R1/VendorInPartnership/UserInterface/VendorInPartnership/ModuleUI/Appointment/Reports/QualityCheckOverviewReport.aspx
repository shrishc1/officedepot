﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    AutoEventWireup="true" CodeFile="QualityCheckOverviewReport.aspx.cs" Inherits="ModuleUI_Appointment_Reports_QualityCheckOverviewReport" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectCountry.ascx" TagName="MultiSelectCountry"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectSite.ascx" TagName="MultiSelectSite"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectAP.ascx" TagName="MultiSelectAP"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectVendor.ascx" TagName="MultiSelectVendor"
    TagPrefix="cc1" %>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectCarrier.ascx" TagName="MultiSelectCarrier"
    TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript" language="javascript">
        $(document).ready(function () {
            $("body").keypress(function (e) {
                if (e.which == 13 || e.keyCode == 13) {
                    $('#<%=UcButton1.ClientID %>').click();
                    return false;
                }
            });
        });
        function setValue1(target) {
            document.getElementById('<%=hdnJSFromDt.ClientID %>').value = target.value;
        }

        function setValue2(target) {
            document.getElementById('<%=hdnJSToDt.ClientID %>').value = target.value;
        }

    </script>
    <asp:ScriptManager ID="SM1" runat="server">
    </asp:ScriptManager>
    <asp:HiddenField ID="hdnJSFromDt" runat="server" />
    <asp:HiddenField ID="hdnJSToDt" runat="server" />
    <asp:HiddenField ID="hdnSiteIDs" runat="server" />
    <asp:HiddenField ID="hdnBookingReference" runat="server" />
    <asp:HiddenField ID="hdnFromDt" runat="server" />
    <asp:HiddenField ID="hdnToDt" runat="server" />
    <h2>
        <cc1:ucLabel ID="lblQualityCheckOverviewReport" runat="server" Text="Quality Check Overview Report"></cc1:ucLabel>
    </h2>
    <div class="right-shadow">
        <div class="formbox">
            <cc1:ucPanel ID="UcCarrierVendorPanel" runat="server">
                <table width="100%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                    <tr>
                        <td>
                            <table>
                                <tr>
                                    <td style="font-weight: bold;">
                                        <cc1:ucLabel ID="lblSite" runat="server" Text="Site"></cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold;">
                                        <cc1:ucLabel ID="UcLabel4" runat="server">:</cc1:ucLabel>
                                    </td>
                                    <td>
                                        <cc1:MultiSelectSite runat="server" ID="msSite" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="font-weight: bold;">
                                        <asp:Label ID="lblBookingReference" runat="server" Text="Booking Reference"></asp:Label>
                                    </td>
                                    <td style="font-weight: bold;">
                                        <asp:Label ID="Label1" runat="server" Text=":"></asp:Label>
                                    </td>
                                    <td>
                                        <cc1:ucTextbox ID="txtBookingReference" runat="server" MaxLength="20"></cc1:ucTextbox>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table width="100%">
                                <tr>
                                    <td style="font-weight: bold; width: 105px">
                                        <cc1:ucLabel ID="lblDateFrom" runat="server" Text="Date From"></cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold; width: 5px">
                                        <cc1:ucLabel ID="UcLabel6" runat="server">:</cc1:ucLabel>
                                    </td>
                                    <td align="left">
                                        <table width="25%" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style="font-weight: bold; width: 6em;">
                                                    <cc1:ucTextbox ID="txtFromDate" runat="server" ClientIDMode="Static" CssClass="date"
                                                        onchange="setValue1(this)" ReadOnly="True" Width="70px" />
                                                </td>
                                                <td style="font-weight: bold; width: 4em; text-align: center">
                                                    <cc1:ucLabel ID="lblTo" runat="server" Text="To"></cc1:ucLabel>
                                                </td>
                                                <td style="font-weight: bold; width: 6em;">
                                                    <cc1:ucTextbox ID="txtToDate" runat="server" ClientIDMode="Static" CssClass="date"
                                                        onchange="setValue2(this)" ReadOnly="True" Width="70px" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <div class="button-row">
                                <cc1:ucButton ID="UcButton1" runat="server" Text="Generate Report" CssClass="button"
                                    OnClick="btnGenerateReport_Click" />
                            </div>
                        </td>
                    </tr>
                </table>
            </cc1:ucPanel>
            <cc1:ucPanel ID="UcCarrierVendorReportViewPanel" runat="server" Visible="false">
                <table style="width: 100%;">
                    <tr>
                        <td>
                            <rsweb:ReportViewer ID="QualityCheckOverviewReportReportViewer" runat="server" Width="950px"
                                DocumentMapCollapsed="True" Font-Names="Verdana" Font-Size="8pt" InteractiveDeviceInfos="(Collection)"
                                WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt" Height="416px">
                                <LocalReport ReportPath="ModuleUI\Appointment\Reports\RDLC\QualityCheckOverviewReport.rdlc">
                                    <DataSources>
                                        <rsweb:ReportDataSource DataSourceId="SqlDataSource1" Name="dtRPTBOK_QualityCheckOverview" />
                                    </DataSources>
                                </LocalReport>
                            </rsweb:ReportViewer>
                            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:sConn %>"
                                SelectCommand="spRPTBOK_QualityCheckOverview" SelectCommandType="StoredProcedure">
                                <SelectParameters>
                                    <asp:ControlParameter ControlID="hdnSiteIDs" DefaultValue="-1" Name="SelectedSiteIDs"
                                        PropertyName="Value" Type="String" />
                                    <asp:ControlParameter ControlID="hdnToDt" DefaultValue="" Name="DateTo" PropertyName="Value"
                                        Type="DateTime" />
                                    <asp:ControlParameter ControlID="hdnFromDt" Name="DateFrom" PropertyName="Value"
                                        DefaultValue="" Type="DateTime" />
                                    <asp:ControlParameter ControlID="hdnBookingReference" DefaultValue="-1" Name="BookingReferenceID"
                                        PropertyName="Value" Type="String" />
                                </SelectParameters>
                            </asp:SqlDataSource>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <cc1:ucButton ID="btnBackSearch" runat="server" Text="Back To Search Page" CssClass="button"
                                OnClick="btnBack_Click" />
                        </td>
                    </tr>
                </table>
            </cc1:ucPanel>
        </div>
    </div>
</asp:Content>

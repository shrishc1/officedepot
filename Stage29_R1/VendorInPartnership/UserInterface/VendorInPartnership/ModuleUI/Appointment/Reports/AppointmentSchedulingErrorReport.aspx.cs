﻿using System;
using Microsoft.Reporting.WebForms;
using System.Data.SqlClient;
using System.Data;
using BusinessLogicLayer.ModuleBAL.Security;
using BusinessEntities.ModuleBE.Security;
using System.Web.UI.WebControls;
using System.Collections.Generic;
public partial class ModuleUI_Appointment_Reports_AppointmentSchedulingErrorReport : CommonPage
{
    protected void Page_Init(object sender, EventArgs e)
    {
        ucVendorTemplateSelect.CurrentPage = this;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            txtFromDate.Text = DateTime.Now.AddDays(-DateTime.Now.Day + 1).ToString("dd/MM/yyyy");
            txtToDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
            hdnJSFromDt.Value = txtFromDate.Text;
            hdnJSToDt.Value = txtToDate.Text;
        }
        TextBox txtVendor = Utilities.Common.FindControlRecursive(this.Page.Controls[0], "txtVendorNo") as TextBox;
        if (txtVendor != null)
            txtVendor.Focus();
    }

    /// <summary>
    /// Method to show Vendor on the basis of selected Template
    /// </summary>
    public override void TemplateSelectedIndexChanged()
    {
        base.TemplateSelectedIndexChanged();

        int VendorTemplateId = Convert.ToInt32(ucVendorTemplateSelect.innerControlddlTemplate.SelectedValue);

        ListBox lstSelectedVendor = (ListBox)msVendor.FindControl("lstRight");
        ListBox lstVendor = (ListBox)msVendor.FindControl("ucVendor").FindControl("lstLeft");

        if (VendorTemplateId > 0)
        {

            SCT_TemplateBAL oSCT_TemplateBAL = new SCT_TemplateBAL();
            SCT_TemplateBE oSCT_TemplateBE = new SCT_TemplateBE();

            oSCT_TemplateBE.VendorTemplateID = VendorTemplateId;
            oSCT_TemplateBE.Action = "GetVendorTemplateInfobyID";
            List<SCT_TemplateBE> lstVendorTemplate = oSCT_TemplateBAL.GetVendorTemplateById(oSCT_TemplateBE);

            if (lstVendorTemplate != null && lstVendorTemplate.Count > 0)
            {
                lstSelectedVendor.Items.Clear();
                msVendor.innerControlHiddenField.Value = string.Empty;
                msVendor.innerControlHiddenField2.Value = string.Empty;
                for (int iCount = 0; iCount < lstVendorTemplate.Count; iCount++)
                {
                    lstSelectedVendor.Items.Add(new ListItem(lstVendorTemplate[iCount].Vendor.Vendor_Name, lstVendorTemplate[iCount].Vendor.VendorID.ToString()));
                    msVendor.SelectedVendorIDs = lstVendorTemplate[iCount].Vendor.VendorID.ToString();
                    msVendor.SelectedVendorName = lstVendorTemplate[iCount].Vendor.Vendor_Name.ToString();
                }
            }
        }
        else
        {
            lstSelectedVendor.Items.Clear();
            msVendor.innerControlHiddenField.Value = string.Empty;
            msVendor.innerControlHiddenField2.Value = string.Empty;
        }
    }

    protected void btnGenerateReport_Click(object sender, EventArgs e)
    {

        if (!string.IsNullOrEmpty(msSite.SelectedSiteIDs))
            hdnSiteIDs.Value = msSite.SelectedSiteIDs;
        else
            hdnSiteIDs.Value = "-1";
        if (!string.IsNullOrEmpty(msCountry.SelectedCountryIDs))
            hdnCountryIDs.Value = msCountry.SelectedCountryIDs;
        else
            hdnCountryIDs.Value = "-1";
        if (!string.IsNullOrEmpty(msVendor.SelectedVendorIDs))
            hdnVendorIDs.Value = msVendor.SelectedVendorIDs;
        else
            hdnVendorIDs.Value = "-1";
        if (!string.IsNullOrEmpty(msCarrier.SelectedCarrierIDs))
            hdnCarrierIDs.Value = msCarrier.SelectedCarrierIDs;
        else
            hdnCarrierIDs.Value = "-1";

        if (!string.IsNullOrEmpty(txtFromDate.Text))
            hdnFromDt.Value = Utilities.Common.TextToDateFormat(hdnJSFromDt.Value).Year + "-" + Utilities.Common.TextToDateFormat(hdnJSFromDt.Value).Month + "-" + Utilities.Common.TextToDateFormat(hdnJSFromDt.Value).Day;
        if (!string.IsNullOrEmpty(txtToDate.Text))
            hdnToDt.Value = Utilities.Common.TextToDateFormat(hdnJSToDt.Value).Year + "-" + Utilities.Common.TextToDateFormat(hdnJSToDt.Value).Month + "-" + Utilities.Common.TextToDateFormat(hdnJSToDt.Value).Day;

        DataSet objDataSet = new DataSet();
        SqlConnection objSqlConnection = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["sConn"].ToString());
        objSqlConnection.Open();
        SqlCommand objSqlCommand = new SqlCommand("spRPTBOK_AppointmentSchedulingError", objSqlConnection);
        objSqlCommand.CommandType = CommandType.StoredProcedure;
        objSqlCommand.Parameters.Add("@SelectedCountryIDs", SqlDbType.VarChar).Value = hdnCountryIDs.Value;
        objSqlCommand.Parameters.Add("@SelectedSiteIDs", SqlDbType.VarChar).Value = hdnSiteIDs.Value;
        objSqlCommand.Parameters.Add("@SelectedVendorIDs", SqlDbType.VarChar).Value = hdnVendorIDs.Value;
        objSqlCommand.Parameters.Add("@DateTo", SqlDbType.DateTime).Value = hdnToDt.Value;
        objSqlCommand.Parameters.Add("@DateFrom", SqlDbType.DateTime).Value = hdnFromDt.Value;
        objSqlCommand.Parameters.Add("@SelectedCarrierIDs", SqlDbType.VarChar).Value = hdnCarrierIDs.Value;
        SqlDataAdapter objSqlDataAdapter = new SqlDataAdapter(objSqlCommand);
        objSqlDataAdapter.Fill(objDataSet);
        ReportDataSource rdsRPTBOK_AppointmentSchedulingError = new ReportDataSource();
        rdsRPTBOK_AppointmentSchedulingError = new ReportDataSource("dtRPTBOK_AppointmentSchedulingError", objDataSet.Tables[0]);
        AppointmentSchedulingErrorReportViewer.LocalReport.DataSources.Clear();
        AppointmentSchedulingErrorReportViewer.LocalReport.DataSources.Add(rdsRPTBOK_AppointmentSchedulingError);

        ReportParameter[] reportParameter = new ReportParameter[6];
        reportParameter[0] = new ReportParameter("Country", msCountry.SelectedCountryName);
        reportParameter[1] = new ReportParameter("Site", msSite.SelectedSiteName);
        reportParameter[2] = new ReportParameter("Vendor", msVendor.SelectedVendorName);
        reportParameter[3] = new ReportParameter("DateFrom", hdnJSFromDt.Value);
        reportParameter[4] = new ReportParameter("DateTo", hdnJSToDt.Value);
        reportParameter[5] = new ReportParameter("Carrier", msCarrier.SelectedCarrierName);
        this.AppointmentSchedulingErrorReportViewer.LocalReport.SetParameters(reportParameter);

        UcTimingOfplacingOrdersPanel.Visible = false;
        UcTimingOfplacingOrdersReportViewPanel.Visible = true;
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        EncryptQueryString("AppointmentSchedulingErrorReport.aspx");
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.Appointment.Booking;
using BusinessLogicLayer.ModuleBAL.Appointment.Booking;
using WebUtilities;
using System.Linq;
using Utilities;

public partial class NoShowDeliveries : CommonPage
{
    protected void Page_Init(object sender, EventArgs e)
    {        
        msSite.isMoveAllRequired = true;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            btnBackSearch.Visible = false;
            txtFromDate.Text = DateTime.Now.AddDays(-DateTime.Now.Day + 1).ToString("dd/MM/yyyy");
            txtToDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
            hdnJSFromDt.Value = txtFromDate.Text;
            hdnJSToDt.Value = txtToDate.Text;
            btnBackSearch.Visible = false;
        }

        msCountry.SetCountryOnPostBack();
        msSite.setSitesOnPostBack();
        msVendor.setVendorsOnPostBack();

        btnExportToExcelSummary.CurrentPage = this;
        btnExportToExcelSummary.FileName = "NoShowDeliveries";
        btnExportToExcelSummary.GridViewControl = gvNoShowDeliveriesExcel;
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        mvDiscrepancyList.ActiveViewIndex = 1;
        btnBackSearch.Visible = true;
        this.BindGrids();
    }

    protected void btnBackSearch_Click(object sender, EventArgs e)
    {
        mvDiscrepancyList.ActiveViewIndex = 0;        
        btnBackSearch.Visible = false;
        txtFromDate.Text = DateTime.Now.AddDays(-DateTime.Now.Day + 1).ToString("dd/MM/yyyy");
        txtToDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
        hdnJSFromDt.Value = txtFromDate.Text;
        hdnJSToDt.Value = txtToDate.Text;

        gvNoShowDeliveries.DataSource = null;
        gvNoShowDeliveries.DataBind();
        gvNoShowDeliveriesExcel.DataSource = null;
        gvNoShowDeliveriesExcel.DataBind();
        ViewState["NoShowDeliveries"] = null;
    }

    protected void gvNoShowDeliveries_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        if (ViewState["NoShowDeliveries"] != null)
        {
            gvNoShowDeliveries.PageIndex = e.NewPageIndex;
            gvNoShowDeliveries.DataSource = (List<APPBOK_BookingBE>)ViewState["NoShowDeliveries"];
            gvNoShowDeliveries.DataBind();
        }
    }

    private void BindGrids()
    {
        var bookingBE = new APPBOK_BookingBE();
        bookingBE.Action ="GetNoShowReport";
        if (!string.IsNullOrEmpty(msCountry.SelectedCountryIDs) && !string.IsNullOrWhiteSpace(msCountry.SelectedCountryIDs))
            bookingBE.SelectedCountryIDs = msCountry.SelectedCountryIDs;

        if (!string.IsNullOrEmpty(msSite.SelectedSiteIDs) && !string.IsNullOrWhiteSpace(msSite.SelectedSiteIDs))
            bookingBE.SelectedSiteIds = msSite.SelectedSiteIDs;

        if (!string.IsNullOrEmpty(msVendor.SelectedVendorIDs) && !string.IsNullOrWhiteSpace(msVendor.SelectedVendorIDs))
            bookingBE.SelectedVendorIDs = msVendor.SelectedVendorIDs;

        bookingBE.FromDate = string.IsNullOrEmpty(txtFromDate.Text) ? (DateTime?)null : Common.GetMM_DD_YYYY(hdnJSFromDt.Value);
        bookingBE.ToDate = string.IsNullOrEmpty(txtToDate.Text) ? (DateTime?)null : Common.GetMM_DD_YYYY(hdnJSToDt.Value);
        var bookingBAL = new APPBOK_BookingBAL();
        var lstNoShowBookings = bookingBAL.GetNoShowReportBAL(bookingBE);        
        if (lstNoShowBookings != null && lstNoShowBookings.Count > 0)
        {
            gvNoShowDeliveries.DataSource = lstNoShowBookings;
            gvNoShowDeliveriesExcel.DataSource = lstNoShowBookings;
            ViewState["NoShowDeliveries"] = lstNoShowBookings;
            btnExportToExcelSummary.Visible = true;
            gvNoShowDeliveries.PageIndex = 0;
        }
        else
        {
            gvNoShowDeliveries.DataSource = null;
            gvNoShowDeliveriesExcel.DataSource = null;
            btnExportToExcelSummary.Visible = false;
        }
        gvNoShowDeliveries.DataBind();
        gvNoShowDeliveriesExcel.DataBind();        
    }

    public override BusinessEntities.BaseBe[] SubSort(GridViewSortEventArgs e)
    {
        return Utilities.GenericListHelper<APPBOK_BookingBE>.SortList((List<APPBOK_BookingBE>)ViewState["NoShowDeliveries"], e.SortExpression, e.SortDirection).ToArray();
    }
}
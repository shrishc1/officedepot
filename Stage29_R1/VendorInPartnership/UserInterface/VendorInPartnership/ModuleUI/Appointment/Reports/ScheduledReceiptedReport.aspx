﻿<%@ Page Language="C#" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    AutoEventWireup="true" CodeFile="ScheduledReceiptedReport.aspx.cs" Inherits="ModuleUI_Appointment_Reports_ScheduledReceiptedReport" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/ucDate.ascx" TagName="ucDate" TagPrefix="cc5" %>
<%@ Register TagName="ucExportButton" TagPrefix="uc1" Src="~/CommonUI/UserControls/ucExportToExcel.ascx" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script type="text/javascript">
        $(document).ready(function () {
            $("body").keypress(function (e) {
                if (e.which == 13 || e.keyCode == 13) {
                    $('#<%=btnSearch.ClientID %>').click();
                    return false;
                }
            });
        });
    </script>
    <h2>
        <cc1:ucLabel ID="lblScheduledReceiptedReport" runat="server" Text="Scheduled and Receipted Report"></cc1:ucLabel>
    </h2>   
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div class="right-shadow">
        <div class="formbox">
            <asp:UpdatePanel ID="pnlUP1" runat="server">
                <ContentTemplate>
                    <table width="90%" cellspacing="5" cellpadding="0" class="form-table">
                        <tr>
                            <td style="font-weight: bold; width: 10%;">
                                <cc1:ucLabel ID="lblDate" runat="server"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold; width: 1%;">
                                <cc1:ucLabel ID="UcLabel3" runat="server">:</cc1:ucLabel>
                            </td>
                            <td style="width: 21%;">
                                <cc5:ucDate ClientIDMode="Static" ID="txtDate" runat="server" />
                                <asp:RequiredFieldValidator ID="rfvDateRequired" runat="server" Display="None" ControlToValidate="txtDate$txtUCDate"
                                    ValidateEmptyText="true" ValidationGroup="a" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;   <cc1:ucButton ID="btnSearch" runat="server" Text="Search" CssClass="button" 
                                    onclick="btnSearch_Click" />
                            </td>
                            <td colspan="4">
                                
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <uc1:ucExportButton ID="btnExportToExcel1" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4">
                              <asp:Label ID="lblError" runat="server"></asp:Label>
                                <cc1:ucGridView ID="UcGridView1" Width="100%" runat="server" CssClass="grid" onsorting="SortGrid"
                                    AllowSorting="true">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Site" SortExpression="SiteId">
                                            <HeaderStyle Width="10%" HorizontalAlign="Left" />
                                            <ItemStyle HorizontalAlign="center" />
                                            <ItemTemplate>
                                              <asp:LinkButton ID="lnkSite" runat="server" Text='<%#Eval("SiteName") %>'  CommandArgument='<%#Eval("SiteId") %>'  OnClick="lnkSite_Onclick"></asp:LinkButton>                                                
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="# Lines Due">
                                            <HeaderStyle Width="10%" HorizontalAlign="Left" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblLinesDue" runat="server" Text='<%#Eval("LinesDue") %>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="center" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="# Lines Booked">
                                            <HeaderStyle Width="10%" HorizontalAlign="Left" />
                                            <ItemStyle HorizontalAlign="center" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblLinesBooked" runat="server" Text='<%#Eval("LinesBooked") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="# Lines Receipted">
                                            <HeaderStyle Width="10%" HorizontalAlign="Left" />
                                            <ItemStyle HorizontalAlign="center" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblLinesReceipted" runat="server" Text='<%#Eval("LinesReceipted") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </cc1:ucGridView>                               
                            </td>
                        </tr>
                    </table>
                </ContentTemplate>
                <Triggers>           
                 <asp:PostBackTrigger ControlID="btnExportToExcel1" />        
                    </Triggers>
            </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>

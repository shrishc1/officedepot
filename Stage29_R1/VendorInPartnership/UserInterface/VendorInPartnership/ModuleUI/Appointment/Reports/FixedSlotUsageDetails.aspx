﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    MaintainScrollPositionOnPostback="true" AutoEventWireup="true" CodeFile="FixedSlotUsageDetails.aspx.cs"
    Inherits="FixedSlotUsageDetails" %>

<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/ucSite.ascx" TagName="ucSite" TagPrefix="cc2" %>
<%@ Register Src="~/CommonUI/UserControls/ucExportToExcel.ascx" TagName="ucExportToExcel"
    TagPrefix="cc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h2>
        <cc1:ucLabel ID="lblFixedSlotUsageReport" runat="server"></cc1:ucLabel>
    </h2>
    <div class="button-row">
        <cc2:ucExportToExcel ID="ucExportToExcel1" runat="server" />
    </div>
    <div class="right-shadow">
        <div class="formbox">
            <table cellspacing="1" cellpadding="0">
                <tr>
                    <td>
                        <cc1:ucGridView ID="ucGridView1" runat="server" AutoGenerateColumns="false" CssClass="grid gvclass"
                            CellPadding="0" Width="960px" OnSorting="SortGrid" AllowSorting="true" OnRowDataBound="GridView1_DataBound">
                            <AlternatingRowStyle BackColor="White" ForeColor="#284775"></AlternatingRowStyle>
                            <Columns>                                
                                <asp:BoundField HeaderText="Vendor/Carrier" DataField="VendorName" SortExpression="VendorName">
                                    <HeaderStyle Width="150px" HorizontalAlign="Left" />
                                    <ItemStyle HorizontalAlign="Left" Width="150px" />
                                </asp:BoundField>
                                <asp:BoundField HeaderText="SlotTime" DataField="SlotTime" SortExpression="SlotTime">
                                    <HeaderStyle Width="40px" HorizontalAlign="Left" />
                                    <ItemStyle HorizontalAlign="Left" Width="40px" />
                                </asp:BoundField>
                                <asp:BoundField HeaderText="DoorNumber" DataField="DoorNumber" SortExpression="DoorNumber">
                                    <HeaderStyle Width="50px" HorizontalAlign="Left" />
                                    <ItemStyle HorizontalAlign="Left" Width="50px" />
                                </asp:BoundField>
                                <asp:BoundField HeaderText="Rsvd Plt" DataField="MaximumPallets" SortExpression="MaximumPallets">
                                    <HeaderStyle Width="40px" HorizontalAlign="Left" />
                                    <ItemStyle HorizontalAlign="Left" Width="40px" />
                                </asp:BoundField>
                                <asp:BoundField HeaderText="Rsvd Ctn" DataField="MaximumCatrons" SortExpression="MaximumCatrons">
                                    <HeaderStyle Width="40px" HorizontalAlign="Left" />
                                    <ItemStyle HorizontalAlign="Left" Width="40px" />
                                </asp:BoundField>
                                <asp:BoundField HeaderText="Rsvd Ln" DataField="MaximumLines" SortExpression="MaximumLines">
                                    <HeaderStyle Width="40px" HorizontalAlign="Left" />
                                    <ItemStyle HorizontalAlign="Left" Width="40px" />
                                </asp:BoundField>
                                <asp:BoundField HeaderText="Day" DataField="Day" SortExpression="Day">
                                    <HeaderStyle Width="50px" HorizontalAlign="Left" />
                                    <ItemStyle HorizontalAlign="Left" Width="50px" />
                                </asp:BoundField>
                                <asp:BoundField HeaderText="Total Slots" DataField="TOTALCOUNT" SortExpression="TOTALCOUNT">
                                    <HeaderStyle Width="30px" HorizontalAlign="Left" />
                                    <ItemStyle HorizontalAlign="Left" Width="30px" />
                                </asp:BoundField>
                                <asp:BoundField HeaderText="Used" DataField="USED" SortExpression="USED">
                                    <HeaderStyle Width="40px" HorizontalAlign="Left" />
                                    <ItemStyle HorizontalAlign="Left" Width="40px" />
                                </asp:BoundField>
                                <asp:BoundField HeaderText="UsedPerc" DataField="PercUsed" SortExpression="PercUsed">
                                    <HeaderStyle Width="40px" HorizontalAlign="Left" />
                                    <ItemStyle HorizontalAlign="Left" Width="40px" />
                                </asp:BoundField>
                                <asp:BoundField HeaderText="Ave Plts" DataField="UsedPallets" SortExpression="UsedPallets">
                                    <HeaderStyle Width="40px" HorizontalAlign="Left" />
                                    <ItemStyle HorizontalAlign="Left" Width="40px" />
                                </asp:BoundField>
                                <asp:BoundField HeaderText="Ave Ctns" DataField="UsedCartons" SortExpression="UsedCartons">
                                    <HeaderStyle Width="40px" HorizontalAlign="Left" />
                                    <ItemStyle HorizontalAlign="Left" Width="40px" />
                                </asp:BoundField>
                                <asp:BoundField HeaderText="Ave Lns" DataField="UsedLines" SortExpression="UsedLines">
                                    <HeaderStyle Width="40px" HorizontalAlign="Left" />
                                    <ItemStyle HorizontalAlign="Left" Width="40px" />
                                </asp:BoundField>
                            </Columns>
                        </cc1:ucGridView>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div class="button-row">
        <cc1:ucButton ID="btnBack" runat="server" Text="Back" CssClass="button" OnClientClick="javascript:window.open('', '_self', '');window.close();" />
    </div>
</asp:Content>

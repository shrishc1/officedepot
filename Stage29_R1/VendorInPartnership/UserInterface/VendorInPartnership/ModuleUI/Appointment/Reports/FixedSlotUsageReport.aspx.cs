﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.Appointment.Reports;
using BusinessLogicLayer.ModuleBAL.Appointment.Reports;
using System.Data;
using Utilities;

public partial class FixedSlotUsageReport : CommonPage
{
   
    protected void Page_Init(object sender, EventArgs e) {
        ddlSite.CurrentPage = this;
        ddlSite.IsAllRequired = false;
        ddlSite.TimeSlotWindow = "S";
    }

    protected void Page_Load(object sender, EventArgs e) {
        ucExportToExcel1.CurrentPage = this;
        ucExportToExcel1.FileName = "FixedSlotUsageReport";
        if (ddlReportType.SelectedItem.Value == "ps") {
            ucExportToExcel1.GridViewControl = ucGridView1;
        }
        else {
            ucExportToExcel1.GridViewControl = ucGridView2;
        }
    }

    protected void btnGenerateReport_Click(object sender, EventArgs e) {        
        BindReport();
    }

    private void BindReport() {
        FixedSlotUsageReportBAL oFixedSlotUsageReportBAL = new FixedSlotUsageReportBAL();
        FixedSlotUsageReportBE oFixedSlotUsageReportBE = new FixedSlotUsageReportBE();

        oFixedSlotUsageReportBE.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
        oFixedSlotUsageReportBE.Period = Convert.ToInt32(ddlPeriod.SelectedItem.Value);

        List<FixedSlotUsageReportBE> lstFixedSlotUsageReport = new List<FixedSlotUsageReportBE>();


        if (ddlReportType.SelectedItem.Value == "ps") {
            oFixedSlotUsageReportBE.Action = "UsagePerSlot";
            tblps.Visible = true;
            tblpv.Visible = false;
            lstFixedSlotUsageReport = oFixedSlotUsageReportBAL.GetFixedSlotUsageReportBAL(oFixedSlotUsageReportBE);
        }
        else {
            oFixedSlotUsageReportBE.Action = "UsagePerVendor";
            tblps.Visible = false;
            tblpv.Visible = true;
            lstFixedSlotUsageReport = oFixedSlotUsageReportBAL.GetFixedSlotUsageVendorBAL(oFixedSlotUsageReportBE);
        }

        if (lstFixedSlotUsageReport != null && lstFixedSlotUsageReport.Count > 0) {
            ViewState["lstFixedSlotUsageReport"] = lstFixedSlotUsageReport;
        
            if (ddlReportType.SelectedItem.Value == "ps") {
                ucGridView1.DataSource = lstFixedSlotUsageReport;
                ucGridView1.DataBind();               
            }
            else {
                ucGridView2.DataSource = lstFixedSlotUsageReport;
                ucGridView2.DataBind();               
            }
        }
        else {
            ViewState["lstFixedSlotUsageReport"] = null;
            ucGridView1.DataSource = null;
            ucGridView1.DataBind();
            ucGridView2.DataSource = null;
            ucGridView2.DataBind();
        }
    }

    public override BusinessEntities.BaseBe[] SubSort(GridViewSortEventArgs e){    
        return Utilities.GenericListHelper<FixedSlotUsageReportBE>.SortList((List<FixedSlotUsageReportBE>)ViewState["lstFixedSlotUsageReport"], e.SortExpression, e.SortDirection).ToArray();      
    }    
}
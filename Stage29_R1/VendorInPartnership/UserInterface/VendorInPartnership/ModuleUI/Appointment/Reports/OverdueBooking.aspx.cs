﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.AdminFunctions;
using BusinessEntities.ModuleBE.Appointment.CountrySetting;
using BusinessLogicLayer.ModuleBAL.Appointment.CountrySetting;
using Utilities;
using System.Data;
using BusinessEntities.ModuleBE.Appointment.Booking;
using BusinessLogicLayer.ModuleBAL.Appointment.Booking;
using BaseControlLibrary;
using System.Collections;
using BusinessLogicLayer.ModuleBAL.Security;
using BusinessEntities.ModuleBE.Security;
using WebUtilities;
using System.Drawing;


public partial class ModuleUI_Appointment_Reports_OverdueBooking : CommonPage
{
    #region Declarations ...
    protected string UpdatedSuccessfully = WebCommon.getGlobalResourceValue("Reviewed");
    #endregion

    public SortDirection GridViewSortDirection
    {
        get
        {
            if (ViewState["sortDirection"] == null)
                ViewState["sortDirection"] = SortDirection.Ascending;
            return (SortDirection)ViewState["sortDirection"];
        }
        set { ViewState["sortDirection"] = value; }
    }
    protected void Page_Init(object sender, EventArgs e)
    {
        ucExportToExcel1.Visible = false;
        ucVendorTemplateSelect.CurrentPage = this;
        msSite.isMoveAllRequired = true;



    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (GetQueryStringValue("PF") != null && GetQueryStringValue("PF").ToString() != "")
            {
                string pageFrom = GetQueryStringValue("PF").ToString();
                if (Session["OverdueBooking"] != null && pageFrom == "BH")
                {
                    GetSession();
                    BindBookings();
                    mvDiscrepancyList.ActiveViewIndex = 1;
                    lblSearchResult.Visible = true;
                    lblOverdueBooking.Visible = false;
                }
            }
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            txtFromDate.Text = DateTime.Now.AddDays(-DateTime.Now.Day + 1).ToString("dd/MM/yyyy");
            txtToDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
            hdnJSFromDt.Value = txtFromDate.Text;
            hdnJSToDt.Value = txtToDate.Text;
        }

        if (IsPostBack)
        {
            txtFromDate.Text = hdnJSFromDt.Value;
            txtToDate.Text = hdnJSToDt.Value;
        }

        ucExportToExcel1.CurrentPage = this;
        ucExportToExcel1.GridViewControl = gvExport;
        ucExportToExcel1.FileName = "OverdueBooking";


        msSite.setSitesOnPostBack();
        msVendor.setVendorsOnPostBack();
        msStockPlanner.SetSPOnPostBack();
    }

    protected void btnGenerateReport_Click(object sender, EventArgs e)
    {
        gvVendor.PageIndex = 0;
        BindBookings();
        mvDiscrepancyList.ActiveViewIndex = 1;
        lblSearchResult.Visible = true;
        lblOverdueBooking.Visible = false;
    }

    protected void gvVendor_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvVendor.PageIndex = e.NewPageIndex;
        BindBookings();



        if (Session["OverdueBooking"] != null)
        {
            Hashtable htBookingCriteria = (Hashtable)Session["OverdueBooking"];

            htBookingCriteria.Remove("PageIndex");
            htBookingCriteria.Add("PageIndex", e.NewPageIndex);

            Session["OverdueBooking"] = htBookingCriteria;
        }
    }

    protected void gvVendor_PreRender(object sender, EventArgs e)
    {
        MergeRows(gvVendor);
    }

    protected void BindBookings(List<APPBOK_BookingBE> lstBookings = null)
    {
        if (lstBookings == null)
        {
            APPBOK_BookingBE oAPPBOK_BookingBE = new APPBOK_BookingBE();
            APPBOK_BookingBAL oAPPBOK_BookingBAL = new APPBOK_BookingBAL();


            oAPPBOK_BookingBE.FromDate = Utilities.Common.TextToDateFormat(hdnJSFromDt.Value);
            oAPPBOK_BookingBE.ToDate = Utilities.Common.TextToDateFormat(hdnJSToDt.Value);

            oAPPBOK_BookingBE.Action = "GetOverdueBooking";

            if (!string.IsNullOrEmpty(msStockPlanner.SelectedStockPlannerIDs))
                oAPPBOK_BookingBE.SelectedStockPlannerIDs = msStockPlanner.SelectedStockPlannerIDs;

            if (!string.IsNullOrEmpty(msVendor.SelectedVendorIDs))
                oAPPBOK_BookingBE.SelectedVendorIDs = msVendor.SelectedVendorIDs;

            if (!string.IsNullOrEmpty(msSite.SelectedSiteIDs))
                oAPPBOK_BookingBE.SelectedSiteIds = msSite.SelectedSiteIDs;

            if (rbAll.Checked)
            {
                oAPPBOK_BookingBE.DisplayType = "ALL";
            }
            else if (rbReviewed.Checked)
            {
                oAPPBOK_BookingBE.DisplayType = "Reviewed";
            }
            else if (rbToBeReviewed.Checked)
            {
                oAPPBOK_BookingBE.DisplayType = "ToBeReviewed";
            }

            SetSession(oAPPBOK_BookingBE.DisplayType);
            //DataTable lstBookings = new DataTable();
            //List<APPBOK_BookingBE> lstBookings = new List<APPBOK_BookingBE>();


            lstBookings = oAPPBOK_BookingBAL.GetOverdueBookingBAL(oAPPBOK_BookingBE);
            lstBookings.Where(x => x.IsReviewed == null).ToList().ForEach(x => x.IsReviewed = false);
        }
        gvVendor.DataSource = lstBookings;
        gvVendor.DataBind();

        gvExport.DataSource = lstBookings;
        gvExport.DataBind();

        if (gvExport.Rows.Count > 0)
            ucExportToExcel1.Visible = true;
        else
            ucExportToExcel1.Visible = false;


        ViewState["lstSites"] = lstBookings;
        Cache["lstGridResult"] = lstBookings;
    }

    private void SetSession(string selectedRbtn)
    {
        Session["OverdueBooking"] = null;
        Hashtable htOverdueBooking = new Hashtable();
        htOverdueBooking.Add("FromDate", hdnJSFromDt.Value);
        htOverdueBooking.Add("ToDate", hdnJSToDt.Value);

        htOverdueBooking.Add("SiteIDs", msSite.SelectedSiteIDsForOB);
        htOverdueBooking.Add("SelectedSiteName", msSite.SelectedSiteName);

        htOverdueBooking.Add("SpIDs", msStockPlanner.SelectedSPIDsForOB);
        htOverdueBooking.Add("SelectedSpName", msStockPlanner.SelectedSPNameForOB);

        htOverdueBooking.Add("VendorIDs", msVendor.SelectedVendorIDs);
        htOverdueBooking.Add("SelectedVendorName", msVendor.SelectedVendorName);
        htOverdueBooking.Add("Display", selectedRbtn);

        htOverdueBooking.Add("PageIndex", 0);


        Session["OverdueBooking"] = htOverdueBooking;
    }

    private void GetSession()
    {
        if (Session["OverdueBooking"] != null)
        {
            Hashtable htOverdueBooking = (Hashtable)Session["OverdueBooking"];

            txtFromDate.Text = htOverdueBooking.ContainsKey("FromDate") ? htOverdueBooking["FromDate"].ToString() : "";
            txtToDate.Text = htOverdueBooking.ContainsKey("ToDate") ? htOverdueBooking["ToDate"].ToString() : "";
            hdnJSFromDt.Value = txtFromDate.Text;
            hdnJSToDt.Value = txtToDate.Text;

            if (htOverdueBooking["SiteIDs"] != null)
            {
                msSite.innerControlHiddenSelectedID.Value = htOverdueBooking["SiteIDs"].ToString();
                msSite.innerControlHiddenSelectedName.Value = htOverdueBooking["SelectedSiteName"].ToString();
                msSite.setSitesOnPostBack();
            }
            if (htOverdueBooking["SpIDs"] != null)
            {
                ((HiddenField)msStockPlanner.FindControl("hiddenSelectedIDs")).Value = htOverdueBooking["SpIDs"].ToString();
                ((HiddenField)msStockPlanner.FindControl("hiddenSelectedName")).Value = htOverdueBooking["SelectedSpName"].ToString();
                msStockPlanner.SetSPOnPostBack();
            }

            if (htOverdueBooking["VendorIDs"] != null)
            {
                ((HiddenField)msVendor.FindControl("hiddenSelectedIDs")).Value = htOverdueBooking["VendorIDs"].ToString();
                ((HiddenField)msVendor.FindControl("hiddenSelectedName")).Value = htOverdueBooking["SelectedVendorName"].ToString();
                msVendor.setVendorsOnPostBack();
            }

            if (htOverdueBooking["Display"] != null)
            {
                rbToBeReviewed.Checked = false;
                rbReviewed.Checked = false;
                rbAll.Checked = false;
                string selectedRbtn = htOverdueBooking["Display"].ToString();
                if (selectedRbtn.Equals("ToBeReviewed"))
                    rbToBeReviewed.Checked = true;
                else if (selectedRbtn.Equals("Reviewed"))
                    rbReviewed.Checked = true;
                else
                    rbAll.Checked = true;
            }

            gvVendor.PageIndex = Convert.ToInt32(htOverdueBooking["PageIndex"].ToString());
        }
    }

    protected void gvVendor_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        GridViewRow gvr = e.Row;
        var preRowNum = e.Row.RowIndex - 1;
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            LinkButton lnkStats = (LinkButton)e.Row.FindControl("lnkStats");
            ucLiteral ltHistory = (ucLiteral)e.Row.FindControl("ltHistory");
            HyperLink hypHistory = (HyperLink)e.Row.FindControl("hypHistory");

            #region Set Row color
            string refId = hypHistory.Text;
            string preRefId = string.Empty;
            Color color2 = Color.FromArgb(247, 246, 243);
            Color color1 = Color.FromArgb(255, 255, 255);

            if (preRowNum >= 0)
            {
                var preRow = gvVendor.Rows[e.Row.RowIndex - 1];

                HyperLink preHypHistory = (HyperLink)preRow.FindControl("hypHistory");
                preRefId = preHypHistory.Text;


                var rowColor = preRow.BackColor;
                if (refId == preRefId)
                {
                    e.Row.BackColor = rowColor;
                }
                else
                {
                    e.Row.BackColor = rowColor.Equals(color1) ? color2 : color1;
                }
            }
            #endregion Set Row color

            #region Code to set visibility of [Time / Day] based on [Time Window Id] ..
            ucLiteral UcLiteral27 = (ucLiteral)e.Row.FindControl("UcLiteral27");
            ucLiteral UcLiteral27_1 = (ucLiteral)e.Row.FindControl("UcLiteral27_1");
            ucLiteral UcLiteral27_2 = (ucLiteral)e.Row.FindControl("UcLiteral27_2");


            if (!string.IsNullOrEmpty(UcLiteral27_1.Text) && !string.IsNullOrWhiteSpace(UcLiteral27_1.Text))
            {
                UcLiteral27_2.Visible = false;
                UcLiteral27_1.Visible = true;
                UcLiteral27.Visible = false;
            }
            else if (!string.IsNullOrEmpty(UcLiteral27_2.Text) && !string.IsNullOrWhiteSpace(UcLiteral27_2.Text))
            {
                UcLiteral27_2.Visible = true;
                UcLiteral27_1.Visible = false;
                UcLiteral27.Visible = false;
            }
            else
            {
                UcLiteral27_2.Visible = false;
                UcLiteral27_1.Visible = false;
                UcLiteral27.Visible = true;
            }
            #endregion

            if (lnkStats != null)
            {
                if (lnkStats.Text == "Reviewed")
                {
                    lnkStats.Enabled = false;
                    lnkStats.Style.Add("text-decoration", "none");
                    lnkStats.Style.Add("color", "#6b6b6b");
                }
            }

            if (ltHistory.Text == "Fixed booking")
            {
                ltHistory.Visible = true;
                hypHistory.Visible = false;
            }
            else
            {
                ltHistory.Visible = false;
                hypHistory.Visible = true;
            }
        }
    }

    public override void TemplateSelectedIndexChanged()
    {
        base.TemplateSelectedIndexChanged();

        int VendorTemplateId = Convert.ToInt32(ucVendorTemplateSelect.innerControlddlTemplate.SelectedValue);

        ListBox lstSelectedVendor = (ListBox)msVendor.FindControl("lstRight");
        ListBox lstVendor = (ListBox)msVendor.FindControl("ucVendor").FindControl("lstLeft");

        if (VendorTemplateId > 0)
        {

            SCT_TemplateBAL oSCT_TemplateBAL = new SCT_TemplateBAL();
            SCT_TemplateBE oSCT_TemplateBE = new SCT_TemplateBE();

            oSCT_TemplateBE.VendorTemplateID = VendorTemplateId;
            oSCT_TemplateBE.Action = "GetVendorTemplateInfobyID";
            List<SCT_TemplateBE> lstVendorTemplate = oSCT_TemplateBAL.GetVendorTemplateById(oSCT_TemplateBE);

            if (lstVendorTemplate != null && lstVendorTemplate.Count > 0)
            {
                lstSelectedVendor.Items.Clear();
                msVendor.innerControlHiddenField.Value = string.Empty;
                msVendor.innerControlHiddenField2.Value = string.Empty;
                for (int iCount = 0; iCount < lstVendorTemplate.Count; iCount++)
                {
                    lstSelectedVendor.Items.Add(new ListItem(lstVendorTemplate[iCount].Vendor.Vendor_Name, lstVendorTemplate[iCount].Vendor.VendorID.ToString()));
                    msVendor.SelectedVendorIDs = lstVendorTemplate[iCount].Vendor.VendorID.ToString();
                    msVendor.SelectedVendorName = lstVendorTemplate[iCount].Vendor.Vendor_Name.ToString();
                }
            }
        }
        else
        {
            lstSelectedVendor.Items.Clear();
            msVendor.innerControlHiddenField.Value = string.Empty;
            msVendor.innerControlHiddenField2.Value = string.Empty;
        }
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        mvDiscrepancyList.ActiveViewIndex = 0;
        lblSearchResult.Visible = false;
        lblOverdueBooking.Visible = true;
    }

    protected void lnkStatus_Click(object sender, EventArgs e)
    {
        LinkButton lnk = (LinkButton)sender;
        var val = (lnk.CommandArgument);
        int bookingId = 0;
        bookingId = Convert.ToInt32(val);

        try
        {
            APPBOK_BookingBE oAPPBOK_BookingBE = new APPBOK_BookingBE();
            APPBOK_BookingBAL oAPPBOK_BookingBAL = new APPBOK_BookingBAL();

            oAPPBOK_BookingBE.Action = "UpdateReviewStatus";
            oAPPBOK_BookingBE.BookingID = bookingId;

            oAPPBOK_BookingBAL.UpdateBookingReviewedStatusBAL(oAPPBOK_BookingBE);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "popup", "<script>alert('" + UpdatedSuccessfully + "');</script>", false);

            List<APPBOK_BookingBE> lstBookings = new List<APPBOK_BookingBE>();
            lstBookings = (List<APPBOK_BookingBE>)Cache["lstGridResult"];

            lstBookings.Where(x => x.PKID == val).ToList().ForEach(x => x.IsReviewed = true);

            BindBookings(lstBookings);
        }
        catch (Exception)
        {
            throw;
        }
    }

    private void MergeRows(GridView gridView)
    {
        for (int rowIndex = gridView.Rows.Count - 2; rowIndex >= 0; rowIndex--)
        {
            GridViewRow row = gridView.Rows[rowIndex];
            GridViewRow previousRow = gridView.Rows[rowIndex + 1];

            Literal currBookingRef = (Literal)(row.FindControl("ltHistory"));
            Literal prevBookingRef = (Literal)(previousRow.FindControl("ltHistory"));

            if (currBookingRef.Text == prevBookingRef.Text)
            {
                //Booking Ref #
                row.Cells[0].RowSpan = previousRow.Cells[0].RowSpan < 2 ? 2 : previousRow.Cells[0].RowSpan + 1;
                previousRow.Cells[0].Visible = false;

                //Site
                row.Cells[1].RowSpan = previousRow.Cells[1].RowSpan < 2 ? 2 : previousRow.Cells[1].RowSpan + 1;
                previousRow.Cells[1].Visible = false;

                //Time / Day
                row.Cells[2].RowSpan = previousRow.Cells[2].RowSpan < 2 ? 2 : previousRow.Cells[2].RowSpan + 1;
                previousRow.Cells[2].Visible = false;

                //Date Booking was made
                row.Cells[5].RowSpan = previousRow.Cells[5].RowSpan < 2 ? 2 : previousRow.Cells[5].RowSpan + 1;
                previousRow.Cells[5].Visible = false;

                //Date Booking was made for
                row.Cells[6].RowSpan = previousRow.Cells[6].RowSpan < 2 ? 2 : previousRow.Cells[6].RowSpan + 1;
                previousRow.Cells[6].Visible = false;


                //Carrier name
                row.Cells[7].RowSpan = previousRow.Cells[7].RowSpan < 2 ? 2 : previousRow.Cells[7].RowSpan + 1;
                previousRow.Cells[7].Visible = false;

                //Booking was made by
                row.Cells[10].RowSpan = previousRow.Cells[10].RowSpan < 2 ? 2 : previousRow.Cells[10].RowSpan + 1;
                previousRow.Cells[10].Visible = false;

                //Status
                row.Cells[13].RowSpan = previousRow.Cells[13].RowSpan < 2 ? 2 : previousRow.Cells[13].RowSpan + 1;
                previousRow.Cells[13].Visible = false;

            }
        }
    }

    public override BusinessEntities.BaseBe[] SubSort(GridViewSortEventArgs e)
    {
        return Utilities.GenericListHelper<APPBOK_BookingBE>.SortList((List<APPBOK_BookingBE>)Cache["lstGridResult"], e.SortExpression, e.SortDirection).ToArray();
    }
}
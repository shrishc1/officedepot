﻿using System;
using Microsoft.Reporting.WebForms;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using Utilities;
using WebUtilities;
using System.Web.UI;
using BusinessEntities.ModuleBE.Upload;
using System.Collections.Generic;
using BusinessLogicLayer.ModuleBAL.Upload;
using System.Drawing;
using BusinessLogicLayer.ModuleBAL.Security;
using BusinessEntities.ModuleBE.Security;
using BaseControlLibrary;
using System.Linq;
using System.Collections;

public partial class ModuleUI_Appointment_Reports_OpenPurchaseOrderSummaryOverview : CommonPage
{
    private const int gridPageSize = 200;
    bool IsExportClicked = false;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindPlanner();
            BindStockPlannerGrouping();
            pager1.Visible = false;
            pager1.PageSize = gridPageSize;
            pager1.GenerateGoToSection = false;
            pager1.GeneratePagerInfoSection = false;

            if (!string.IsNullOrEmpty(GetQueryStringValue("Report")))
            {
                BindSUBReport();
            }
        }
    }
    protected void Page_PreRender(object sender, EventArgs e)
    {


    }
    public void pager_Command(object sender, CommandEventArgs e)
    {
        int currnetPageIndx = Convert.ToInt32(e.CommandArgument);
        pager1.CurrentIndex = currnetPageIndx;
        if (!string.IsNullOrEmpty(GetQueryStringValue("Report")))
        {
            BindSUBReport(currnetPageIndx);
        }
        else
        {
            BindReport(currnetPageIndx);
        }
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        BindReport();
    }
    private void BindPlanner()
    {
        SCT_UserBE oNewSCT_UserBE = new SCT_UserBE();
        SCT_UserBAL oSCT_UserBAL = new SCT_UserBAL();
        List<SCT_UserBE> lstUser = new List<SCT_UserBE>();
        oNewSCT_UserBE.Action = "GetStockPalnnerByUserId";
        oNewSCT_UserBE.UserID = Convert.ToInt32(Session["UserID"]);
        lstUser = oSCT_UserBAL.GetPlannerBAL(oNewSCT_UserBE);
        if (lstUser.Count > 0 && lstUser != null)
        {
            FillControls.FillDropDown(ref ddlStockPlanner, lstUser, "FullName", "UserID", "All");
        }
    }
    public void BindStockPlannerGrouping()
    {
        StockPlannerGroupingsBE oStockPlannerGroupingsBE = new StockPlannerGroupingsBE();
        StockPlannerGroupingsBAL oStockPlannerGroupingsBAL = new StockPlannerGroupingsBAL();

        oStockPlannerGroupingsBE.Action = "ShowAll";
        List<StockPlannerGroupingsBE> lstStockPlannerGrouping = new List<StockPlannerGroupingsBE>();
        lstStockPlannerGrouping = oStockPlannerGroupingsBAL.GetStockPlannerGroupingsBAL(oStockPlannerGroupingsBE);
        // lstStockPlannerGrouping.RemoveAll(x => x.StockPlannerGroupingID == Convert.ToInt32(strStockPlannerGroupingid));
        if (lstStockPlannerGrouping.Count > 0)
        {
            FillControls.FillDropDown(ref ddlStockPlannerGroupings, lstStockPlannerGrouping, "StockPlannerGroupings", "StockPlannerGroupingsID", "All");
        }
    }
    protected void btnExport_Click(object sender, EventArgs e)
    {
        IsExportClicked = true;
        if (!string.IsNullOrEmpty(GetQueryStringValue("Report")))
        {
            BindSUBReport();
        }
        else
        {
            BindReport();
        }
        if (string.IsNullOrEmpty(GetQueryStringValue("Report")))
        {
            if (rdoGroupBy.Checked)
            {
                WebCommon.ExportHideHidden("OpenPOSummary-ByGroup", gvOpenPurchaseOrder, null, false, true);
            }
            else
            {
                WebCommon.ExportHideHidden("OpenPOSummary-ByStockPlanner", gvOpenPurchaseOrder, null, false, true);
            }
        }
        else if (!string.IsNullOrEmpty(GetQueryStringValue("Report")) && GetQueryStringValue("Report") == "BySite")
        {
            WebCommon.ExportHideHidden("OpenPOSummary-BySite", gvOpenPurchaseOrder, null, false, true);
        }
        else if (!string.IsNullOrEmpty(GetQueryStringValue("Report")) && GetQueryStringValue("Report") == "ByStockPlanner")
        {
            WebCommon.ExportHideHidden("OpenPOSummary-ByStockPlanner", gvOpenPurchaseOrder, null, false, true);
        }
        else if (!string.IsNullOrEmpty(GetQueryStringValue("Report")) && GetQueryStringValue("Report") == "BySiteFromPlanner")
        {
            WebCommon.ExportHideHidden("OpenPOSummary-BySiteFromPlanner", gvOpenPurchaseOrder, null, false, true);
        }

    }
    protected void btnBack_Click(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(GetQueryStringValue("Report")))
        {
            Page.ClientScript.RegisterStartupScript(this.GetType(), "script", "window.close()", true);
        }
        else
        {
            tblGrid.Visible = false;
            tblSearch.Visible = true;
            if (Session["OpenPOReportNew"] != null)
            {
                RetainSearchData();
            }

        }
    }
    private void BindSUBReport(int Page = 1)
    {
        tblSearch.Visible = false;
        Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE = new Up_PurchaseOrderDetailBE();
        UP_PurchaseOrderDetailBAL oUP_PurchaseOrderDetailBAL = new UP_PurchaseOrderDetailBAL();
        if ((!string.IsNullOrEmpty(GetQueryStringValue("Report")) && (GetQueryStringValue("Report") == "BySite")))
        {
            if (GetQueryStringValue("SPGID") != null && !string.IsNullOrEmpty(GetQueryStringValue("SPGID").Trim()))
            {
                oUp_PurchaseOrderDetailBE.StockPlannerGroupingID = GetQueryStringValue("SPGID");
            }
            //if (!string.IsNullOrEmpty(GetQueryStringValue("SPID")))
            //{
            //    oUp_PurchaseOrderDetailBE.StockPlannerID = Convert.ToInt32(GetQueryStringValue("SPID"));
            //}

            //if (GetQueryStringValue("FilterDays") != null && !string.IsNullOrEmpty(GetQueryStringValue("FilterDays").Trim()))
            //{
            //    oUp_PurchaseOrderDetailBE.Daysfilter = GetQueryStringValue("FilterDays");
            //}

            oUp_PurchaseOrderDetailBE.Action = "BySite";
            // oUp_PurchaseOrderDetailBE.StockPlannerGroupingID = GetQueryStringValue("SPGID");
            lblInfo.Text = "Stock Planner Group: " + GetQueryStringValue("SPG");
        }


        if ((!string.IsNullOrEmpty(GetQueryStringValue("Report")) && (GetQueryStringValue("Report") == "ByStockPlanner")))
        {
            if (GetQueryStringValue("SPGID") != null && !string.IsNullOrEmpty(GetQueryStringValue("SPGID").Trim()))
            {
                oUp_PurchaseOrderDetailBE.StockPlannerGroupingID = GetQueryStringValue("SPGID").Trim();
            }
            if (GetQueryStringValue("SiteID") != null && !string.IsNullOrEmpty(GetQueryStringValue("SiteID").Trim()))
            {
                oUp_PurchaseOrderDetailBE.SiteID = Convert.ToInt32(GetQueryStringValue("SiteID").Trim());
            }
            if (GetQueryStringValue("SPID") != null && !string.IsNullOrEmpty(GetQueryStringValue("SPID").Trim()))
            {
                oUp_PurchaseOrderDetailBE.StockPlannerID = Convert.ToInt32(GetQueryStringValue("SPID").Trim());
            }
            lblInfo.Text = "Stock Planner Group: " + GetQueryStringValue("SPG") + "  , Site: " + GetQueryStringValue("Site");
            oUp_PurchaseOrderDetailBE.Action = "ByStockPlanner";
        }

        if ((!string.IsNullOrEmpty(GetQueryStringValue("Report")) && (GetQueryStringValue("Report") == "BySiteFromPlanner")))
        {
            if (GetQueryStringValue("SPGID") != null && !string.IsNullOrEmpty(GetQueryStringValue("SPGID").Trim()))
            {
                oUp_PurchaseOrderDetailBE.StockPlannerGroupingID = GetQueryStringValue("SPGID").Trim();
            }
            if (GetQueryStringValue("SPID") != null && !string.IsNullOrEmpty(GetQueryStringValue("SPID").Trim()))
            {
                oUp_PurchaseOrderDetailBE.StockPlannerID = Convert.ToInt32(GetQueryStringValue("SPID").Trim());
            }
            lblInfo.Text = "Stock Planner Group: " + GetQueryStringValue("SPG") + " ,  Stock Planner: " + GetQueryStringValue("SPN");
            oUp_PurchaseOrderDetailBE.Action = "BySite";
        }

        //if ((!string.IsNullOrEmpty(GetQueryStringValue("Report")) && (GetQueryStringValue("Report") == "BySite")))
        //{
        //    if (!string.IsNullOrEmpty(GetQueryStringValue("SPGID")))
        //    {
        //        oUp_PurchaseOrderDetailBE.StockPlannerGroupingID = GetQueryStringValue("SPGID");
        //    }
        //    if (!string.IsNullOrEmpty(GetQueryStringValue("SPID")))
        //    {
        //        oUp_PurchaseOrderDetailBE.StockPlannerID = Convert.ToInt32(GetQueryStringValue("SPID"));
        //    }

        //    oUp_PurchaseOrderDetailBE.Action = "BySite";
        //    oUp_PurchaseOrderDetailBE.StockPlannerGroupingID = GetQueryStringValue("SPGID");
        //}



        Hashtable htBOPenaltyOverview = (Hashtable)Session["OpenPOReportNew"];

        if ((htBOPenaltyOverview.ContainsKey("SelectedCountryIDs") && htBOPenaltyOverview["SelectedCountryIDs"] != null && htBOPenaltyOverview["SelectedCountryIDs"] != ""))
        {
            oUp_PurchaseOrderDetailBE.SelectedCountryIDs = Convert.ToString(htBOPenaltyOverview["SelectedCountryIDs"]);
        }
        if ((htBOPenaltyOverview.ContainsKey("SelectedVendorIDs") && htBOPenaltyOverview["SelectedVendorIDs"] != null && htBOPenaltyOverview["SelectedVendorIDs"] != ""))
        {
            oUp_PurchaseOrderDetailBE.SelectedVendorIDs = Convert.ToString(htBOPenaltyOverview["SelectedVendorIDs"]);
        }

        if (IsExportClicked)
        {
            hdnGridCurrentPageNo.Value = "0";
            hdnGridPageSize.Value = "0";
        }
        else
        {
            hdnGridCurrentPageNo.Value = Page.ToString();
            hdnGridPageSize.Value = gridPageSize.ToString();
        }

        DataTable dt = oUP_PurchaseOrderDetailBAL.GetOpenPOReportBAL(oUp_PurchaseOrderDetailBE);
        if (dt.Rows.Count == 1)
        {
            dt = null;
        }

        int pageSize = 200;
        int startRow = (Convert.ToInt32(hdnGridCurrentPageNo.Value) - 1) * pageSize;
        if (dt != null && dt.Rows.Count > 0)
        {
            int totalRecords = dt.Rows.Count;
            pager1.ItemCount = Convert.ToDouble(dt.Rows.Count.ToString());
            pager1.CurrentIndex = Page;
            btnExportToExcel.Visible = true;
            pager1.Visible = true;

            if (!IsExportClicked)
            {
                dt = dt.AsEnumerable().Skip(startRow).Take(pageSize).CopyToDataTable();
            }

            gvOpenPurchaseOrder.DataSource = dt;
            gvOpenPurchaseOrder.DataBind();
            gvOpenPurchaseOrder.Visible = true;
            tblGrid.Visible = true;
            tblSearch.Visible = false;
        }
        else
        {
            pager1.Visible = false;
            btnExportToExcel.Visible = false;
            gvOpenPurchaseOrder.DataSource = null;
            gvOpenPurchaseOrder.DataBind();
            gvOpenPurchaseOrder.Visible = true;
            tblGrid.Visible = false;
            tblSearch.Visible = true;
        }
    }
    private void BindReport(int Page = 1)
    {
        Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE = new Up_PurchaseOrderDetailBE();
        UP_PurchaseOrderDetailBAL oUP_PurchaseOrderDetailBAL = new UP_PurchaseOrderDetailBAL();
        if (rdoGroupBy.Checked == true)
        {
            oUp_PurchaseOrderDetailBE.StockPlannerGroupingID = ddlStockPlannerGroupings.SelectedValue;
            oUp_PurchaseOrderDetailBE.StockPlannerGroupingName = ddlStockPlannerGroupings.SelectedItem.Text;
            oUp_PurchaseOrderDetailBE.Action = "ByStockPlannerGroupings";
            oUp_PurchaseOrderDetailBE.Item_type = "ByGroup";
        }
        else
        {
            oUp_PurchaseOrderDetailBE.StockPlannerID = Convert.ToInt32(ddlStockPlanner.SelectedValue);
            oUp_PurchaseOrderDetailBE.StockPlannerName = ddlStockPlanner.SelectedItem.Text;
            oUp_PurchaseOrderDetailBE.Action = "ByStockPlanner";
            oUp_PurchaseOrderDetailBE.Item_type = "BySP";
        }

        oUp_PurchaseOrderDetailBE.SelectedVendorIDs = msVendor.SelectedVendorIDs;
        oUp_PurchaseOrderDetailBE.SelectedCountryIDs = msCountry.SelectedCountryIDs;

        if (IsExportClicked)
        {
            hdnGridCurrentPageNo.Value = "0";
            hdnGridPageSize.Value = "0";
        }
        else
        {
            hdnGridCurrentPageNo.Value = Page.ToString();
            hdnGridPageSize.Value = gridPageSize.ToString();
        }

        SetSession(oUp_PurchaseOrderDetailBE);
        DataTable dt = oUP_PurchaseOrderDetailBAL.GetOpenPOReportBAL(oUp_PurchaseOrderDetailBE);
        if (dt.Rows.Count == 1)
        {
            dt = null;
        }
        int pageSize = 200;
        int startRow = (Convert.ToInt32(hdnGridCurrentPageNo.Value) - 1) * pageSize;
        if (dt != null && dt.Rows.Count > 0)
        {
            int totalRecords = dt.Rows.Count;
            pager1.ItemCount = Convert.ToDouble(dt.Rows.Count.ToString());
            pager1.CurrentIndex = Page;
            btnExportToExcel.Visible = true;
            pager1.Visible = true;

            if (!IsExportClicked)
            {
                dt = dt.AsEnumerable().Skip(startRow).Take(pageSize).CopyToDataTable();
            }


            gvOpenPurchaseOrder.DataSource = dt;
            gvOpenPurchaseOrder.DataBind();
            gvOpenPurchaseOrder.Visible = true;
            tblGrid.Visible = true;
            tblSearch.Visible = false;
        }
        else
        {
            tblGrid.Visible = true;
            tblSearch.Visible = false;
            pager1.Visible = false;
            btnExportToExcel.Visible = false;
            gvOpenPurchaseOrder.Visible = true;
            gvOpenPurchaseOrder.DataSource = null;
            gvOpenPurchaseOrder.DataBind();
        }
    }
    protected void gvOpenPurchaseOrder_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Header)
        {
            if (string.IsNullOrEmpty(GetQueryStringValue("Report")))
            {
                if (rdoGroupBy.Checked)
                {
                    e.Row.Cells[0].HorizontalAlign = HorizontalAlign.Left;
                    e.Row.Cells[1].Visible = false;
                }
                else
                {
                    e.Row.Cells[2].Visible = false;
                    e.Row.Cells[3].Visible = false;
                    e.Row.Cells[0].HorizontalAlign = HorizontalAlign.Left;
                }
            }
            else if (!string.IsNullOrEmpty(GetQueryStringValue("Report")) && GetQueryStringValue("Report") == "BySite")
            {
                e.Row.Cells[0].HorizontalAlign = HorizontalAlign.Left;
                e.Row.Cells[1].Visible = false;
            }
            else if (!string.IsNullOrEmpty(GetQueryStringValue("Report")) && GetQueryStringValue("Report") == "ByStockPlanner")
            {
                e.Row.Cells[2].Visible = false;
                e.Row.Cells[3].Visible = false;
                e.Row.Cells[0].HorizontalAlign = HorizontalAlign.Left;
            }
            else if (!string.IsNullOrEmpty(GetQueryStringValue("Report")) && GetQueryStringValue("Report") == "BySiteFromPlanner")
            {
                e.Row.Cells[0].HorizontalAlign = HorizontalAlign.Left;
                e.Row.Cells[1].Visible = false;
            }

        }
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            if (e.Row.Cells[0].Text == "ZZZZZBlank")
            {
                e.Row.Cells[0].Text = "Blank";
            }
            if (e.Row.Cells[0].Text == "ZZZZZTotal")
            {
                e.Row.Cells[0].Text = "Total";
            }

            var URL = "";
            var URL2 = "";
            var URL3 = "";
            var URL6 = "";
            var URL7 = "";
            var URL8 = "";
            var URL9 = "";
            var URL10 = "";
            var URL11 = "";
            dynamic firstCell = e.Row.Cells[0];
            firstCell.Controls.Clear();
            if (string.IsNullOrEmpty(GetQueryStringValue("Report")))
            {
                if (rdoGroupBy.Checked)
                {
                    e.Row.Cells[0].HorizontalAlign = HorizontalAlign.Left;
                    e.Row.Cells[1].Visible = false;
                }
                else
                {
                    e.Row.Cells[2].Visible = false;
                    e.Row.Cells[3].Visible = false;
                    e.Row.Cells[0].HorizontalAlign = HorizontalAlign.Left;
                }
                if (rdoGroupBy.Checked)
                {
                    if (Convert.ToString(e.Row.Cells[0].Text) != "Total")
                    {
                        URL = EncryptQuery("OpenPurchaseOrderSummaryOverview.aspx?SPGID=" + Convert.ToString(e.Row.Cells[1].Text) + "&SPG=" + Convert.ToString(e.Row.Cells[0].Text) + "&VendorID=" + msVendor.SelectedVendorIDs + "&CountryID=" + msCountry.SelectedCountryIDs + "&Report=BySite");
                        firstCell.Controls.Add(new HyperLink
                        {
                            NavigateUrl = URL,
                            Target = "_blank",
                            Text = firstCell.Text
                        });



                        if (e.Row.Cells[2].Text != "0")
                        {
                            URL2 = EncryptQuery("~/ModuleUI/Appointment/Reports/OpenPurchaseOrderOverview.aspx?SPGID=" + Convert.ToString(e.Row.Cells[1].Text) + "&VendorID=" + msVendor.SelectedVendorIDs + "&CountryID=" + msCountry.SelectedCountryIDs + "&Report=Open");
                            e.Row.Cells[2].Controls.Add(new HyperLink
                            {
                                NavigateUrl = URL2,
                                Target = "_blank",
                                Text = e.Row.Cells[2].Text
                            });
                        }

                        if (e.Row.Cells[4].Text != "0")
                        {
                            URL3 = EncryptQuery("~/ModuleUI/Appointment/Reports/OpenPurchaseOrderOverview.aspx?SPGID=" + Convert.ToString(e.Row.Cells[1].Text) + "&VendorID=" + msVendor.SelectedVendorIDs + "&CountryID=" + msCountry.SelectedCountryIDs + "&Report=OverDue");
                            e.Row.Cells[4].Controls.Add(new HyperLink
                            {
                                NavigateUrl = URL3,
                                Target = "_blank",
                                Text = e.Row.Cells[4].Text
                            });
                        }
                        if (e.Row.Cells[6].Text != "0")
                        {
                            URL6 = EncryptQuery("~/ModuleUI/Appointment/Reports/OpenPurchaseOrderOverview.aspx?SPGID=" + Convert.ToString(e.Row.Cells[1].Text) + "&VendorID=" + msVendor.SelectedVendorIDs + "&CountryID=" + msCountry.SelectedCountryIDs + "&Report=OverDue" + "&FilterDays=LessThan3Days");
                            e.Row.Cells[6].Controls.Add(new HyperLink
                            {
                                NavigateUrl = URL6,
                                Target = "_blank",
                                Text = e.Row.Cells[6].Text
                            });
                        }
                        if (e.Row.Cells[7].Text != "0")
                        {
                            URL7 = EncryptQuery("~/ModuleUI/Appointment/Reports/OpenPurchaseOrderOverview.aspx?SPGID=" + Convert.ToString(e.Row.Cells[1].Text) + "&VendorID=" + msVendor.SelectedVendorIDs + "&CountryID=" + msCountry.SelectedCountryIDs + "&Report=OverDue" + "&FilterDays=LessThan1Week");
                            e.Row.Cells[7].Controls.Add(new HyperLink
                            {
                                NavigateUrl = URL7,
                                Target = "_blank",
                                Text = e.Row.Cells[7].Text
                            });
                        }

                        if (e.Row.Cells[8].Text != "0")
                        {
                            URL8 = EncryptQuery("~/ModuleUI/Appointment/Reports/OpenPurchaseOrderOverview.aspx?SPGID=" + Convert.ToString(e.Row.Cells[1].Text) + "&VendorID=" + msVendor.SelectedVendorIDs + "&CountryID=" + msCountry.SelectedCountryIDs + "&Report=OverDue" + "&FilterDays=LessThan2Week");
                            e.Row.Cells[8].Controls.Add(new HyperLink
                            {
                                NavigateUrl = URL8,
                                Target = "_blank",
                                Text = e.Row.Cells[8].Text
                            });
                        }
                        if (e.Row.Cells[9].Text != "0")
                        {
                            URL9 = EncryptQuery("~/ModuleUI/Appointment/Reports/OpenPurchaseOrderOverview.aspx?SPGID=" + Convert.ToString(e.Row.Cells[1].Text) + "&VendorID=" + msVendor.SelectedVendorIDs + "&CountryID=" + msCountry.SelectedCountryIDs + "&Report=OverDue" + "&FilterDays=LessThan1Month");

                            e.Row.Cells[9].Controls.Add(new HyperLink
                            {
                                NavigateUrl = URL9,
                                Target = "_blank",
                                Text = e.Row.Cells[9].Text
                            });
                        }
                        if (e.Row.Cells[10].Text != "0")
                        {
                            URL10 = EncryptQuery("~/ModuleUI/Appointment/Reports/OpenPurchaseOrderOverview.aspx?SPGID=" + Convert.ToString(e.Row.Cells[1].Text) + "&VendorID=" + msVendor.SelectedVendorIDs + "&CountryID=" + msCountry.SelectedCountryIDs + "&Report=OverDue" + "&FilterDays=LessThan3Month");
                            e.Row.Cells[10].Controls.Add(new HyperLink
                            {
                                NavigateUrl = URL10,
                                Target = "_blank",
                                Text = e.Row.Cells[10].Text
                            });
                        }
                        if (e.Row.Cells[11].Text != "0")
                        {
                            URL11 = EncryptQuery("~/ModuleUI/Appointment/Reports/OpenPurchaseOrderOverview.aspx?SPGID=" + Convert.ToString(e.Row.Cells[1].Text) + "&VendorID=" + msVendor.SelectedVendorIDs + "&CountryID=" + msCountry.SelectedCountryIDs + "&Report=OverDue" + "&FilterDays=GreaterThan3Month");
                            e.Row.Cells[11].Controls.Add(new HyperLink
                            {
                                NavigateUrl = URL11,
                                Target = "_blank",
                                Text = e.Row.Cells[11].Text
                            });
                        }
                    }
                    else
                    {
                        string GROUPID = ddlStockPlannerGroupings.SelectedValue != "0" ? ddlStockPlannerGroupings.SelectedValue : null;

                        URL = EncryptQuery("OpenPurchaseOrderSummaryOverview.aspx?SPGID=" + GROUPID + "&SPG=" + Convert.ToString(e.Row.Cells[0].Text) + "&VendorID=" + msVendor.SelectedVendorIDs + "&CountryID=" + msCountry.SelectedCountryIDs + "&Report=BySite");
                        firstCell.Controls.Add(new HyperLink
                        {
                            NavigateUrl = URL,
                            Target = "_blank",
                            Text = firstCell.Text
                        });



                        if (e.Row.Cells[2].Text != "0")
                        {
                            URL2 = EncryptQuery("~/ModuleUI/Appointment/Reports/OpenPurchaseOrderOverview.aspx?SPGID=" + GROUPID + "&VendorID=" + msVendor.SelectedVendorIDs + "&CountryID=" + msCountry.SelectedCountryIDs + "&Report=Open");
                            e.Row.Cells[2].Controls.Add(new HyperLink
                            {
                                NavigateUrl = URL2,
                                Target = "_blank",
                                Text = e.Row.Cells[2].Text
                            });
                        }

                        if (e.Row.Cells[4].Text != "0")
                        {
                            URL3 = EncryptQuery("~/ModuleUI/Appointment/Reports/OpenPurchaseOrderOverview.aspx?SPGID=" + GROUPID + "&VendorID=" + msVendor.SelectedVendorIDs + "&CountryID=" + msCountry.SelectedCountryIDs + "&Report=OverDue");
                            e.Row.Cells[4].Controls.Add(new HyperLink
                            {
                                NavigateUrl = URL3,
                                Target = "_blank",
                                Text = e.Row.Cells[4].Text
                            });
                        }
                        if (e.Row.Cells[6].Text != "0")
                        {
                            URL6 = EncryptQuery("~/ModuleUI/Appointment/Reports/OpenPurchaseOrderOverview.aspx?SPGID=" + GROUPID + "&VendorID=" + msVendor.SelectedVendorIDs + "&CountryID=" + msCountry.SelectedCountryIDs + "&Report=OverDue" + "&FilterDays=LessThan3Days");
                            e.Row.Cells[6].Controls.Add(new HyperLink
                            {
                                NavigateUrl = URL6,
                                Target = "_blank",
                                Text = e.Row.Cells[6].Text
                            });
                        }

                        if (e.Row.Cells[7].Text != "0")
                        {
                            URL7 = EncryptQuery("~/ModuleUI/Appointment/Reports/OpenPurchaseOrderOverview.aspx?SPGID=" + GROUPID + "&VendorID=" + msVendor.SelectedVendorIDs + "&CountryID=" + msCountry.SelectedCountryIDs + "&Report=OverDue" + "&FilterDays=LessThan1Week");
                            e.Row.Cells[7].Controls.Add(new HyperLink
                            {
                                NavigateUrl = URL7,
                                Target = "_blank",
                                Text = e.Row.Cells[7].Text
                            });
                        }

                        if (e.Row.Cells[8].Text != "0")
                        {
                            URL8 = EncryptQuery("~/ModuleUI/Appointment/Reports/OpenPurchaseOrderOverview.aspx?SPGID=" + GROUPID + "&VendorID=" + msVendor.SelectedVendorIDs + "&CountryID=" + msCountry.SelectedCountryIDs + "&Report=OverDue" + "&FilterDays=LessThan2Week");
                            e.Row.Cells[8].Controls.Add(new HyperLink
                            {
                                NavigateUrl = URL8,
                                Target = "_blank",
                                Text = e.Row.Cells[8].Text
                            });
                        }

                        if (e.Row.Cells[9].Text != "0")
                        {
                            URL9 = EncryptQuery("~/ModuleUI/Appointment/Reports/OpenPurchaseOrderOverview.aspx?SPGID=" + GROUPID + "&VendorID=" + msVendor.SelectedVendorIDs + "&CountryID=" + msCountry.SelectedCountryIDs + "&Report=OverDue" + "&FilterDays=LessThan1Month");
                            e.Row.Cells[9].Controls.Add(new HyperLink
                            {
                                NavigateUrl = URL9,
                                Target = "_blank",
                                Text = e.Row.Cells[9].Text
                            });
                        }

                        if (e.Row.Cells[10].Text != "0")
                        {
                            URL10 = EncryptQuery("~/ModuleUI/Appointment/Reports/OpenPurchaseOrderOverview.aspx?SPGID=" + GROUPID + "&VendorID=" + msVendor.SelectedVendorIDs + "&CountryID=" + msCountry.SelectedCountryIDs + "&Report=OverDue" + "&FilterDays=LessThan3Month");
                            e.Row.Cells[10].Controls.Add(new HyperLink
                            {
                                NavigateUrl = URL10,
                                Target = "_blank",
                                Text = e.Row.Cells[10].Text
                            });
                        }

                        if (e.Row.Cells[11].Text != "0")
                        {
                            URL11 = EncryptQuery("~/ModuleUI/Appointment/Reports/OpenPurchaseOrderOverview.aspx?SPGID=" + GROUPID + "&VendorID=" + msVendor.SelectedVendorIDs + "&CountryID=" + msCountry.SelectedCountryIDs + "&Report=OverDue" + "&FilterDays=GreaterThan3Month");
                            e.Row.Cells[11].Controls.Add(new HyperLink
                            {
                                NavigateUrl = URL11,
                                Target = "_blank",
                                Text = e.Row.Cells[11].Text
                            });
                        }
                    }
                }
                if (rdoByPlanner.Checked)
                {
                    if (Convert.ToString(e.Row.Cells[0].Text) != "Total")
                    {
                        URL = EncryptQuery("OpenPurchaseOrderSummaryOverview.aspx?SPGID=" + Convert.ToString(e.Row.Cells[2].Text) + "&SPG=" + Convert.ToString(e.Row.Cells[1].Text) + "&SPN=" + Convert.ToString(e.Row.Cells[0].Text) + "&SPID= " + Convert.ToString(e.Row.Cells[3].Text) + "&VendorID=" + msVendor.SelectedVendorIDs + "&CountryID=" + msCountry.SelectedCountryIDs + "&Report=BySiteFromPlanner");
                        firstCell.Controls.Add(new HyperLink
                        {
                            NavigateUrl = URL,
                            Target = "_blank",
                            Text = firstCell.Text
                        });


                        if (e.Row.Cells[4].Text != "0")
                        {
                            URL2 = EncryptQuery("~/ModuleUI/Appointment/Reports/OpenPurchaseOrderOverview.aspx?SPGID=" + Convert.ToString(e.Row.Cells[2].Text) + "&SPID= " + Convert.ToString(e.Row.Cells[3].Text) + "&VendorID=" + msVendor.SelectedVendorIDs + "&CountryID=" + msCountry.SelectedCountryIDs + "&Report=Open");
                            e.Row.Cells[4].Controls.Add(new HyperLink
                            {
                                NavigateUrl = URL2,
                                Target = "_blank",
                                Text = e.Row.Cells[4].Text
                            });
                        }

                        if (e.Row.Cells[6].Text != "0")
                        {

                            URL3 = EncryptQuery("~/ModuleUI/Appointment/Reports/OpenPurchaseOrderOverview.aspx?SPGID=" + Convert.ToString(e.Row.Cells[2].Text) + "&SPID= " + Convert.ToString(e.Row.Cells[3].Text) + "&VendorID=" + msVendor.SelectedVendorIDs + "&CountryID=" + msCountry.SelectedCountryIDs + "&Report=OverDue");
                            e.Row.Cells[6].Controls.Add(new HyperLink
                            {
                                NavigateUrl = URL3,
                                Target = "_blank",
                                Text = e.Row.Cells[6].Text
                            });
                        }
                        if (e.Row.Cells[8].Text != "0")
                        {
                            URL6 = EncryptQuery("~/ModuleUI/Appointment/Reports/OpenPurchaseOrderOverview.aspx?SPGID=" + Convert.ToString(e.Row.Cells[2].Text) + "&SPID= " + Convert.ToString(e.Row.Cells[3].Text) + "&VendorID=" + msVendor.SelectedVendorIDs + "&CountryID=" + msCountry.SelectedCountryIDs + "&Report=OverDue" + "&FilterDays=LessThan3Days");
                            e.Row.Cells[8].Controls.Add(new HyperLink
                            {
                                NavigateUrl = URL6,
                                Target = "_blank",
                                Text = e.Row.Cells[8].Text
                            });
                        }

                        if (e.Row.Cells[9].Text != "0")
                        {
                            URL7 = EncryptQuery("~/ModuleUI/Appointment/Reports/OpenPurchaseOrderOverview.aspx?SPGID=" + Convert.ToString(e.Row.Cells[2].Text) + "&SPID= " + Convert.ToString(e.Row.Cells[3].Text) + "&VendorID=" + msVendor.SelectedVendorIDs + "&CountryID=" + msCountry.SelectedCountryIDs + "&Report=OverDue" + "&FilterDays=LessThan1Week");
                            e.Row.Cells[9].Controls.Add(new HyperLink
                            {
                                NavigateUrl = URL7,
                                Target = "_blank",
                                Text = e.Row.Cells[9].Text
                            });
                        }

                        if (e.Row.Cells[10].Text != "0")
                        {
                            URL8 = EncryptQuery("~/ModuleUI/Appointment/Reports/OpenPurchaseOrderOverview.aspx?SPGID=" + Convert.ToString(e.Row.Cells[2].Text) + "&SPID= " + Convert.ToString(e.Row.Cells[3].Text) + "&VendorID=" + msVendor.SelectedVendorIDs + "&CountryID=" + msCountry.SelectedCountryIDs + "&Report=OverDue" + "&FilterDays=LessThan2Week");
                            e.Row.Cells[10].Controls.Add(new HyperLink
                            {
                                NavigateUrl = URL8,
                                Target = "_blank",
                                Text = e.Row.Cells[10].Text
                            });
                        }

                        if (e.Row.Cells[11].Text != "0")
                        {
                            URL9 = EncryptQuery("~/ModuleUI/Appointment/Reports/OpenPurchaseOrderOverview.aspx?SPGID=" + Convert.ToString(e.Row.Cells[2].Text) + "&SPID= " + Convert.ToString(e.Row.Cells[3].Text) + "&VendorID=" + msVendor.SelectedVendorIDs + "&CountryID=" + msCountry.SelectedCountryIDs + "&Report=OverDue" + "&FilterDays=LessThan1Month");
                            e.Row.Cells[11].Controls.Add(new HyperLink
                            {
                                NavigateUrl = URL9,
                                Target = "_blank",
                                Text = e.Row.Cells[11].Text
                            });
                        }

                        if (e.Row.Cells[12].Text != "0")
                        {
                            URL10 = EncryptQuery("~/ModuleUI/Appointment/Reports/OpenPurchaseOrderOverview.aspx?SPGID=" + Convert.ToString(e.Row.Cells[2].Text) + "&SPID= " + Convert.ToString(e.Row.Cells[3].Text) + "&VendorID=" + msVendor.SelectedVendorIDs + "&CountryID=" + msCountry.SelectedCountryIDs + "&Report=OverDue" + "&FilterDays=LessThan3Month");
                            e.Row.Cells[12].Controls.Add(new HyperLink
                            {
                                NavigateUrl = URL10,
                                Target = "_blank",
                                Text = e.Row.Cells[12].Text
                            });
                        }

                        if (e.Row.Cells[13].Text != "0")
                        {
                            URL11 = EncryptQuery("~/ModuleUI/Appointment/Reports/OpenPurchaseOrderOverview.aspx?SPGID=" + Convert.ToString(e.Row.Cells[2].Text) + "&SPID= " + Convert.ToString(e.Row.Cells[3].Text) + "&VendorID=" + msVendor.SelectedVendorIDs + "&CountryID=" + msCountry.SelectedCountryIDs + "&Report=OverDue" + "&FilterDays=GreaterThan3Month");
                            e.Row.Cells[13].Controls.Add(new HyperLink
                            {
                                NavigateUrl = URL11,
                                Target = "_blank",
                                Text = e.Row.Cells[13].Text
                            });
                        }

                    }
                    else
                    {
                        string UserID = ddlStockPlanner.SelectedValue != "0" ? ddlStockPlanner.SelectedValue : null;

                        URL = EncryptQuery("OpenPurchaseOrderSummaryOverview.aspx?SPGID=" + Convert.ToString(e.Row.Cells[2].Text) + "&SPG=" + Convert.ToString(e.Row.Cells[1].Text) + "&SPN=" + Convert.ToString(e.Row.Cells[0].Text) + "&SPID= " + UserID + "&VendorID=" + msVendor.SelectedVendorIDs + "&CountryID=" + msCountry.SelectedCountryIDs + "&Report=BySiteFromPlanner");
                        firstCell.Controls.Add(new HyperLink
                        {
                            NavigateUrl = URL,
                            Target = "_blank",
                            Text = firstCell.Text
                        });


                        if (e.Row.Cells[4].Text != "0")
                        {
                            URL2 = EncryptQuery("~/ModuleUI/Appointment/Reports/OpenPurchaseOrderOverview.aspx?SPGID=" + Convert.ToString(e.Row.Cells[2].Text) + "&SPID= " + UserID + "&VendorID=" + msVendor.SelectedVendorIDs + "&CountryID=" + msCountry.SelectedCountryIDs + "&Report=Open");
                            e.Row.Cells[4].Controls.Add(new HyperLink
                            {
                                NavigateUrl = URL2,
                                Target = "_blank",
                                Text = e.Row.Cells[4].Text
                            });
                        }

                        if (e.Row.Cells[6].Text != "0")
                        {
                            URL3 = EncryptQuery("~/ModuleUI/Appointment/Reports/OpenPurchaseOrderOverview.aspx?SPGID=" + Convert.ToString(e.Row.Cells[2].Text) + "&SPID= " + UserID + "&VendorID=" + msVendor.SelectedVendorIDs + "&CountryID=" + msCountry.SelectedCountryIDs + "&Report=OverDue");
                            e.Row.Cells[6].Controls.Add(new HyperLink
                            {
                                NavigateUrl = URL3,
                                Target = "_blank",
                                Text = e.Row.Cells[6].Text
                            });
                        }
                        if (e.Row.Cells[8].Text != "0")
                        {
                            URL6 = EncryptQuery("~/ModuleUI/Appointment/Reports/OpenPurchaseOrderOverview.aspx?SPGID=" + Convert.ToString(e.Row.Cells[2].Text) + "&SPID= " + UserID + "&VendorID=" + msVendor.SelectedVendorIDs + "&CountryID=" + msCountry.SelectedCountryIDs + "&Report=OverDue" + "&FilterDays=LessThan3Days");
                            e.Row.Cells[8].Controls.Add(new HyperLink
                            {
                                NavigateUrl = URL6,
                                Target = "_blank",
                                Text = e.Row.Cells[8].Text
                            });
                        }

                        if (e.Row.Cells[9].Text != "0")
                        {
                            URL7 = EncryptQuery("~/ModuleUI/Appointment/Reports/OpenPurchaseOrderOverview.aspx?SPGID=" + Convert.ToString(e.Row.Cells[2].Text) + "&SPID= " + UserID + "&VendorID=" + msVendor.SelectedVendorIDs + "&CountryID=" + msCountry.SelectedCountryIDs + "&Report=OverDue" + "&FilterDays=LessThan1Week");
                            e.Row.Cells[9].Controls.Add(new HyperLink
                            {
                                NavigateUrl = URL7,
                                Target = "_blank",
                                Text = e.Row.Cells[9].Text
                            });
                        }

                        if (e.Row.Cells[10].Text != "0")
                        {
                            URL8 = EncryptQuery("~/ModuleUI/Appointment/Reports/OpenPurchaseOrderOverview.aspx?SPGID=" + Convert.ToString(e.Row.Cells[2].Text) + "&SPID= " + UserID + "&VendorID=" + msVendor.SelectedVendorIDs + "&CountryID=" + msCountry.SelectedCountryIDs + "&Report=OverDue" + "&FilterDays=LessThan2Week");
                            e.Row.Cells[10].Controls.Add(new HyperLink
                            {
                                NavigateUrl = URL8,
                                Target = "_blank",
                                Text = e.Row.Cells[10].Text
                            });
                        }

                        if (e.Row.Cells[11].Text != "0")
                        {

                            URL9 = EncryptQuery("~/ModuleUI/Appointment/Reports/OpenPurchaseOrderOverview.aspx?SPGID=" + Convert.ToString(e.Row.Cells[2].Text) + "&SPID= " + UserID + "&VendorID=" + msVendor.SelectedVendorIDs + "&CountryID=" + msCountry.SelectedCountryIDs + "&Report=OverDue" + "&FilterDays=LessThan1Month");
                            e.Row.Cells[11].Controls.Add(new HyperLink
                            {
                                NavigateUrl = URL9,
                                Target = "_blank",
                                Text = e.Row.Cells[11].Text
                            });
                        }

                        if (e.Row.Cells[12].Text != "0")
                        {
                            URL10 = EncryptQuery("~/ModuleUI/Appointment/Reports/OpenPurchaseOrderOverview.aspx?SPGID=" + Convert.ToString(e.Row.Cells[2].Text) + "&SPID= " + UserID + "&VendorID=" + msVendor.SelectedVendorIDs + "&CountryID=" + msCountry.SelectedCountryIDs + "&Report=OverDue" + "&FilterDays=LessThan3Month");
                            e.Row.Cells[12].Controls.Add(new HyperLink
                            {
                                NavigateUrl = URL10,
                                Target = "_blank",
                                Text = e.Row.Cells[12].Text
                            });
                        }

                        if (e.Row.Cells[13].Text != "0")
                        {
                            URL11 = EncryptQuery("~/ModuleUI/Appointment/Reports/OpenPurchaseOrderOverview.aspx?SPGID=" + Convert.ToString(e.Row.Cells[2].Text) + "&SPID= " + UserID + "&VendorID=" + msVendor.SelectedVendorIDs + "&CountryID=" + msCountry.SelectedCountryIDs + "&Report=OverDue" + "&FilterDays=GreaterThan3Month");
                            e.Row.Cells[13].Controls.Add(new HyperLink
                            {
                                NavigateUrl = URL11,
                                Target = "_blank",
                                Text = e.Row.Cells[13].Text
                            });
                        }
                    }
                }
            }
            else
            {
                Hashtable htBOPenaltyOverview = (Hashtable)Session["OpenPOReportNew"];
                string countryIds = string.Empty;
                string vendorids = string.Empty;
                if ((htBOPenaltyOverview.ContainsKey("SelectedCountryIDs") && htBOPenaltyOverview["SelectedCountryIDs"] != null && htBOPenaltyOverview["SelectedCountryIDs"] != ""))
                {
                    countryIds = Convert.ToString(htBOPenaltyOverview["SelectedCountryIDs"]);
                }
                if ((htBOPenaltyOverview.ContainsKey("SelectedVendorIDs") && htBOPenaltyOverview["SelectedVendorIDs"] != null && htBOPenaltyOverview["SelectedVendorIDs"] != ""))
                {
                    vendorids = Convert.ToString(htBOPenaltyOverview["SelectedVendorIDs"]);
                }
                if (!string.IsNullOrEmpty(GetQueryStringValue("Report")) && GetQueryStringValue("Report") == "ByStockPlannerGroupings")
                {

                    URL = EncryptQuery("OpenPurchaseOrderSummaryOverview.aspx?SITEID=" + Convert.ToString(firstCell.Text) + "&Report=BySite");
                    firstCell.Controls.Add(new HyperLink
                    {
                        NavigateUrl = URL,
                        Target = "_blank",
                        Text = firstCell.Text
                    });
                }
                if (!string.IsNullOrEmpty(GetQueryStringValue("Report")) && GetQueryStringValue("Report") == "BySite")
                {

                    e.Row.Cells[0].HorizontalAlign = HorizontalAlign.Left;
                    e.Row.Cells[1].Visible = false;

                    string site = string.Empty;
                    if (Convert.ToString(e.Row.Cells[0].Text) == "Gro&#223;ostheim")
                    {
                        site = "GroBostheim";
                    }
                    else
                    {
                        site = e.Row.Cells[0].Text;
                    }

                    URL = EncryptQuery("OpenPurchaseOrderSummaryOverview.aspx?SPGID=" + GetQueryStringValue("SPGID") + "&SPG=" + GetQueryStringValue("SPG") + "&Site=" + site + "&SiteID=" + Convert.ToString(e.Row.Cells[1].Text) + "&Report=ByStockPlanner");
                    firstCell.Controls.Add(new HyperLink
                    {
                        NavigateUrl = URL,
                        Target = "_blank",
                        Text = firstCell.Text
                    });



                    if (e.Row.Cells[2].Text != "0")
                    {
                        URL2 = EncryptQuery("~/ModuleUI/Appointment/Reports/OpenPurchaseOrderOverview.aspx?SPGID=" + GetQueryStringValue("SPGID") + "&SiteID=" + Convert.ToString(e.Row.Cells[1].Text) + "&VendorID=" + vendorids + "&CountryID=" + countryIds + "&Report=Open");
                        e.Row.Cells[2].Controls.Add(new HyperLink
                        {
                            NavigateUrl = URL2,
                            Target = "_blank",
                            Text = e.Row.Cells[2].Text
                        });
                    }

                    if (e.Row.Cells[4].Text != "0")
                    {
                        URL3 = EncryptQuery("~/ModuleUI/Appointment/Reports/OpenPurchaseOrderOverview.aspx?SPGID=" + GetQueryStringValue("SPGID") + "&SiteID=" + Convert.ToString(e.Row.Cells[1].Text) + "&VendorID=" + vendorids + "&CountryID=" + countryIds + "&Report=OverDue");
                        e.Row.Cells[4].Controls.Add(new HyperLink
                        {
                            NavigateUrl = URL3,
                            Target = "_blank",
                            Text = e.Row.Cells[4].Text
                        });

                    }

                    if (e.Row.Cells[6].Text != "0")
                    {
                        URL6 = EncryptQuery("~/ModuleUI/Appointment/Reports/OpenPurchaseOrderOverview.aspx?SPGID=" + GetQueryStringValue("SPGID") + "&SiteID=" + Convert.ToString(e.Row.Cells[1].Text) + "&VendorID=" + vendorids + "&CountryID=" + countryIds + "&Report=OverDue" + "&FilterDays=LessThan3Days");
                        e.Row.Cells[6].Controls.Add(new HyperLink
                        {
                            NavigateUrl = URL6,
                            Target = "_blank",
                            Text = e.Row.Cells[6].Text
                        });
                    }

                    if (e.Row.Cells[7].Text != "0")
                    {
                        URL7 = EncryptQuery("~/ModuleUI/Appointment/Reports/OpenPurchaseOrderOverview.aspx?SPGID=" + GetQueryStringValue("SPGID") + "&SiteID=" + Convert.ToString(e.Row.Cells[1].Text) + "&VendorID=" + vendorids + "&CountryID=" + countryIds + "&Report=OverDue" + "&FilterDays=LessThan1Week");
                        e.Row.Cells[7].Controls.Add(new HyperLink
                        {
                            NavigateUrl = URL7,
                            Target = "_blank",
                            Text = e.Row.Cells[7].Text
                        });
                    }

                    if (e.Row.Cells[8].Text != "0")
                    {

                        URL8 = EncryptQuery("~/ModuleUI/Appointment/Reports/OpenPurchaseOrderOverview.aspx?SPGID=" + GetQueryStringValue("SPGID") + "&SiteID=" + Convert.ToString(e.Row.Cells[1].Text) + "&VendorID=" + vendorids + "&CountryID=" + countryIds + "&Report=OverDue" + "&FilterDays=LessThan2Week");
                        e.Row.Cells[8].Controls.Add(new HyperLink
                        {
                            NavigateUrl = URL8,
                            Target = "_blank",
                            Text = e.Row.Cells[8].Text
                        });
                    }

                    if (e.Row.Cells[9].Text != "0")
                    {

                        URL9 = EncryptQuery("~/ModuleUI/Appointment/Reports/OpenPurchaseOrderOverview.aspx?SPGID=" + GetQueryStringValue("SPGID") + "&SiteID=" + Convert.ToString(e.Row.Cells[1].Text) + "&VendorID=" + vendorids + "&CountryID=" + countryIds + "&Report=OverDue" + "&FilterDays=LessThan1Month");
                        e.Row.Cells[9].Controls.Add(new HyperLink
                        {
                            NavigateUrl = URL9,
                            Target = "_blank",
                            Text = e.Row.Cells[9].Text
                        });

                    }

                    if (e.Row.Cells[10].Text != "0")
                    {
                        URL10 = EncryptQuery("~/ModuleUI/Appointment/Reports/OpenPurchaseOrderOverview.aspx?SPGID=" + GetQueryStringValue("SPGID") + "&SiteID=" + Convert.ToString(e.Row.Cells[1].Text) + "&VendorID=" + vendorids + "&CountryID=" + countryIds + "&Report=OverDue" + "&FilterDays=LessThan3Month");
                        e.Row.Cells[10].Controls.Add(new HyperLink
                        {
                            NavigateUrl = URL10,
                            Target = "_blank",
                            Text = e.Row.Cells[10].Text
                        });

                    }

                    if (e.Row.Cells[11].Text != "0")
                    {
                        URL11 = EncryptQuery("~/ModuleUI/Appointment/Reports/OpenPurchaseOrderOverview.aspx?SPGID=" + GetQueryStringValue("SPGID") + "&SiteID=" + Convert.ToString(e.Row.Cells[1].Text) + "&VendorID=" + vendorids + "&CountryID=" + countryIds + "&Report=OverDue" + "&FilterDays=GreaterThan3Month");
                        e.Row.Cells[11].Controls.Add(new HyperLink
                        {
                            NavigateUrl = URL11,
                            Target = "_blank",
                            Text = e.Row.Cells[11].Text
                        });
                    }
                }
                else if (!string.IsNullOrEmpty(GetQueryStringValue("Report")) && GetQueryStringValue("Report") == "ByStockPlanner")
                {
                    e.Row.Cells[2].Visible = false;
                    e.Row.Cells[3].Visible = false;
                    e.Row.Cells[0].HorizontalAlign = HorizontalAlign.Left;

                    if (Convert.ToString(e.Row.Cells[0].Text) != "Total")
                    {

                        URL = EncryptQuery("OpenPurchaseOrderSummaryOverview.aspx?SPGID=" + Convert.ToString(e.Row.Cells[2].Text) + "&SPG=" + Convert.ToString(e.Row.Cells[1].Text) + "&SPN=" + Convert.ToString(e.Row.Cells[0].Text) + "&SPID=" + Convert.ToString(e.Row.Cells[3].Text) + "&Report=BySiteFromPlanner");
                        firstCell.Controls.Add(new HyperLink
                        {
                            NavigateUrl = URL,
                            Target = "_blank",
                            Text = firstCell.Text
                        });

                        if (e.Row.Cells[4].Text != "0")
                        {

                            URL2 = EncryptQuery("~/ModuleUI/Appointment/Reports/OpenPurchaseOrderOverview.aspx?SPGID=" + Convert.ToString(e.Row.Cells[2].Text) + "&SiteID=" + GetQueryStringValue("SiteID") + "&SPID=" + Convert.ToString(e.Row.Cells[3].Text) + "&VendorID=" + vendorids + "&CountryID=" + countryIds + "&Report=Open");
                            e.Row.Cells[4].Controls.Add(new HyperLink
                            {
                                NavigateUrl = URL2,
                                Target = "_blank",
                                Text = e.Row.Cells[4].Text
                            });
                        }

                        if (e.Row.Cells[6].Text != "0")
                        {
                            URL3 = EncryptQuery("~/ModuleUI/Appointment/Reports/OpenPurchaseOrderOverview.aspx?SPGID=" + Convert.ToString(e.Row.Cells[2].Text) + "&SiteID=" + GetQueryStringValue("SiteID") + "&SPID=" + Convert.ToString(e.Row.Cells[3].Text) + "&VendorID=" + vendorids + "&CountryID=" + countryIds + "&Report=OverDue");
                            e.Row.Cells[6].Controls.Add(new HyperLink
                            {
                                NavigateUrl = URL3,
                                Target = "_blank",
                                Text = e.Row.Cells[6].Text
                            });
                        }
                        if (e.Row.Cells[8].Text != "0")
                        {

                            URL6 = EncryptQuery("~/ModuleUI/Appointment/Reports/OpenPurchaseOrderOverview.aspx?SPGID=" + Convert.ToString(e.Row.Cells[2].Text) + "&SiteID=" + GetQueryStringValue("SiteID") + "&SPID=" + Convert.ToString(e.Row.Cells[3].Text) + "&VendorID=" + vendorids + "&CountryID=" + countryIds + "&Report=OverDue" + "&FilterDays=LessThan3Days");
                            e.Row.Cells[8].Controls.Add(new HyperLink
                            {
                                NavigateUrl = URL6,
                                Target = "_blank",
                                Text = e.Row.Cells[8].Text
                            });
                        }

                        if (e.Row.Cells[9].Text != "0")
                        {
                            URL7 = EncryptQuery("~/ModuleUI/Appointment/Reports/OpenPurchaseOrderOverview.aspx?SPGID=" + Convert.ToString(e.Row.Cells[2].Text) + "&SiteID=" + GetQueryStringValue("SiteID") + "&SPID=" + Convert.ToString(e.Row.Cells[3].Text) + "&VendorID=" + vendorids + "&CountryID=" + countryIds + "&Report=OverDue" + "&FilterDays=LessThan1Week");
                            e.Row.Cells[9].Controls.Add(new HyperLink
                            {
                                NavigateUrl = URL7,
                                Target = "_blank",
                                Text = e.Row.Cells[9].Text
                            });
                        }

                        if (e.Row.Cells[10].Text != "0")
                        {
                            URL8 = EncryptQuery("~/ModuleUI/Appointment/Reports/OpenPurchaseOrderOverview.aspx?SPGID=" + Convert.ToString(e.Row.Cells[2].Text) + "&SiteID=" + GetQueryStringValue("SiteID") + "&SPID=" + Convert.ToString(e.Row.Cells[3].Text) + "&VendorID=" + vendorids + "&CountryID=" + countryIds + "&Report=OverDue" + "&FilterDays=LessThan2Week");
                            e.Row.Cells[10].Controls.Add(new HyperLink
                            {
                                NavigateUrl = URL8,
                                Target = "_blank",
                                Text = e.Row.Cells[10].Text
                            });
                        }

                        if (e.Row.Cells[11].Text != "0")
                        {
                            URL9 = EncryptQuery("~/ModuleUI/Appointment/Reports/OpenPurchaseOrderOverview.aspx?SPGID=" + Convert.ToString(e.Row.Cells[2].Text) + "&SiteID=" + GetQueryStringValue("SiteID") + "&SPID=" + Convert.ToString(e.Row.Cells[3].Text) + "&VendorID=" + vendorids + "&CountryID=" + countryIds + "&Report=OverDue" + "&FilterDays=LessThan1Month");
                            e.Row.Cells[11].Controls.Add(new HyperLink
                            {
                                NavigateUrl = URL9,
                                Target = "_blank",
                                Text = e.Row.Cells[11].Text
                            });
                        }

                        if (e.Row.Cells[12].Text != "0")
                        {
                            URL10 = EncryptQuery("~/ModuleUI/Appointment/Reports/OpenPurchaseOrderOverview.aspx?SPGID=" + Convert.ToString(e.Row.Cells[2].Text) + "&SiteID=" + GetQueryStringValue("SiteID") + "&SPID=" + Convert.ToString(e.Row.Cells[3].Text) + "&VendorID=" + vendorids + "&CountryID=" + countryIds + "&Report=OverDue" + "&FilterDays=LessThan3Month");
                            e.Row.Cells[12].Controls.Add(new HyperLink
                            {
                                NavigateUrl = URL10,
                                Target = "_blank",
                                Text = e.Row.Cells[12].Text
                            });
                        }

                        if (e.Row.Cells[13].Text != "0")
                        {
                            URL11 = EncryptQuery("~/ModuleUI/Appointment/Reports/OpenPurchaseOrderOverview.aspx?SPGID=" + Convert.ToString(e.Row.Cells[2].Text) + "&SiteID=" + GetQueryStringValue("SiteID") + "&SPID=" + Convert.ToString(e.Row.Cells[3].Text) + "&VendorID=" + vendorids + "&CountryID=" + countryIds + "&Report=OverDue" + "&FilterDays=GreaterThan3Month");
                            e.Row.Cells[13].Controls.Add(new HyperLink
                            {
                                NavigateUrl = URL11,
                                Target = "_blank",
                                Text = e.Row.Cells[13].Text
                            });
                        }
                    }
                    else
                    {

                        URL = EncryptQuery("OpenPurchaseOrderSummaryOverview.aspx?SPGID=" + GetQueryStringValue("SPGID") + "&SPG=" + Convert.ToString(e.Row.Cells[1].Text) + "&SPN=" + GetQueryStringValue("SPN") + "&SiteID=" + GetQueryStringValue("SiteID") + "&SPID=" + GetQueryStringValue("SPID") + "&Report=BySiteFromPlanner");
                        firstCell.Controls.Add(new HyperLink
                        {
                            NavigateUrl = URL,
                            Target = "_blank",
                            Text = firstCell.Text
                        });


                        if (e.Row.Cells[4].Text != "0")
                        {
                            URL2 = EncryptQuery("~/ModuleUI/Appointment/Reports/OpenPurchaseOrderOverview.aspx?SPGID=" + GetQueryStringValue("SPGID") + "&SPID=" + GetQueryStringValue("SPID") + "&SiteID=" + GetQueryStringValue("SiteID") + "&SPID=" + Convert.ToString(e.Row.Cells[3].Text) + "&VendorID=" + vendorids + "&CountryID=" + countryIds + "&Report=Open");
                            e.Row.Cells[4].Controls.Add(new HyperLink
                            {
                                NavigateUrl = URL2,
                                Target = "_blank",
                                Text = e.Row.Cells[4].Text
                            });
                        }

                        if (e.Row.Cells[6].Text != "0")
                        {
                            URL3 = EncryptQuery("~/ModuleUI/Appointment/Reports/OpenPurchaseOrderOverview.aspx?SPGID=" + GetQueryStringValue("SPGID") + "&SPID=" + GetQueryStringValue("SPID") + "&SiteID=" + GetQueryStringValue("SiteID") + "&SPID=" + Convert.ToString(e.Row.Cells[3].Text) + "&VendorID=" + vendorids + "&CountryID=" + countryIds + "&Report=OverDue");
                            e.Row.Cells[6].Controls.Add(new HyperLink
                            {
                                NavigateUrl = URL3,
                                Target = "_blank",
                                Text = e.Row.Cells[6].Text
                            });
                        }


                        if (e.Row.Cells[8].Text != "0")
                        {
                            URL6 = EncryptQuery("~/ModuleUI/Appointment/Reports/OpenPurchaseOrderOverview.aspx?SPGID=" + GetQueryStringValue("SPGID") + "&SPID=" + GetQueryStringValue("SPID") + "&SiteID=" + GetQueryStringValue("SiteID") + "&SPID=" + Convert.ToString(e.Row.Cells[3].Text) + "&VendorID=" + vendorids + "&CountryID=" + countryIds + "&Report=OverDue" + "&FilterDays=LessThan3Days");
                            e.Row.Cells[8].Controls.Add(new HyperLink
                            {
                                NavigateUrl = URL6,
                                Target = "_blank",
                                Text = e.Row.Cells[8].Text
                            });
                        }

                        if (e.Row.Cells[9].Text != "0")
                        {
                            URL7 = EncryptQuery("~/ModuleUI/Appointment/Reports/OpenPurchaseOrderOverview.aspx?SPGID=" + GetQueryStringValue("SPGID") + "&SPID=" + GetQueryStringValue("SPID") + "&SiteID=" + GetQueryStringValue("SiteID") + "&SPID=" + Convert.ToString(e.Row.Cells[3].Text) + "&VendorID=" + vendorids + "&CountryID=" + countryIds + "&Report=OverDue" + "&FilterDays=LessThan1Week");
                            e.Row.Cells[9].Controls.Add(new HyperLink
                            {
                                NavigateUrl = URL7,
                                Target = "_blank",
                                Text = e.Row.Cells[9].Text
                            });
                        }

                        if (e.Row.Cells[10].Text != "0")
                        {
                            URL8 = EncryptQuery("~/ModuleUI/Appointment/Reports/OpenPurchaseOrderOverview.aspx?SPGID=" + GetQueryStringValue("SPGID") + "&SPID=" + GetQueryStringValue("SPID") + "&SiteID=" + GetQueryStringValue("SiteID") + "&SPID=" + Convert.ToString(e.Row.Cells[3].Text) + "&VendorID=" + vendorids + "&CountryID=" + countryIds + "&Report=OverDue" + "&FilterDays=LessThan2Week");
                            e.Row.Cells[10].Controls.Add(new HyperLink
                            {
                                NavigateUrl = URL8,
                                Target = "_blank",
                                Text = e.Row.Cells[10].Text
                            });
                        }

                        if (e.Row.Cells[11].Text != "0")
                        {
                            URL9 = EncryptQuery("~/ModuleUI/Appointment/Reports/OpenPurchaseOrderOverview.aspx?SPGID=" + GetQueryStringValue("SPGID") + "&SPID=" + GetQueryStringValue("SPID") + "&SiteID=" + GetQueryStringValue("SiteID") + "&SPID=" + Convert.ToString(e.Row.Cells[3].Text) + "&VendorID=" + vendorids + "&CountryID=" + countryIds + "&Report=OverDue" + "&FilterDays=LessThan1Month");
                            e.Row.Cells[11].Controls.Add(new HyperLink
                            {
                                NavigateUrl = URL9,
                                Target = "_blank",
                                Text = e.Row.Cells[11].Text
                            });
                        }

                        if (e.Row.Cells[12].Text != "0")
                        {
                            URL10 = EncryptQuery("~/ModuleUI/Appointment/Reports/OpenPurchaseOrderOverview.aspx?SPGID=" + GetQueryStringValue("SPGID") + "&SPID=" + GetQueryStringValue("SPID") + "&SiteID=" + GetQueryStringValue("SiteID") + "&SPID=" + Convert.ToString(e.Row.Cells[3].Text) + "&VendorID=" + vendorids + "&CountryID=" + countryIds + "&Report=OverDue" + "&FilterDays=LessThan3Month");
                            e.Row.Cells[12].Controls.Add(new HyperLink
                            {
                                NavigateUrl = URL10,
                                Target = "_blank",
                                Text = e.Row.Cells[12].Text
                            });
                        }

                        if (e.Row.Cells[13].Text != "0")
                        {
                            URL11 = EncryptQuery("~/ModuleUI/Appointment/Reports/OpenPurchaseOrderOverview.aspx?SPGID=" + GetQueryStringValue("SPGID") + "&SPID=" + GetQueryStringValue("SPID") + "&SiteID=" + GetQueryStringValue("SiteID") + "&SPID=" + Convert.ToString(e.Row.Cells[3].Text) + "&VendorID=" + vendorids + "&CountryID=" + countryIds + "&Report=OverDue" + "&FilterDays=GreaterThan3Month");
                            e.Row.Cells[13].Controls.Add(new HyperLink
                            {
                                NavigateUrl = URL11,
                                Target = "_blank",
                                Text = e.Row.Cells[13].Text
                            });
                        }
                    }
                }
                else if (!string.IsNullOrEmpty(GetQueryStringValue("Report")) && GetQueryStringValue("Report") == "BySiteFromPlanner")
                {
                    e.Row.Cells[0].HorizontalAlign = HorizontalAlign.Left;
                    e.Row.Cells[1].Visible = false;
                    URL = EncryptQuery("OpenPurchaseOrderSummaryOverview.aspx?SPGID=" + GetQueryStringValue("SPGID") + "&SPG=" + GetQueryStringValue("SPG") + "&Site=" + Convert.ToString(e.Row.Cells[0].Text) + "&SiteID=" + Convert.ToString(e.Row.Cells[1].Text) + "&SPID=" + GetQueryStringValue("SPID") + "&SPN=" + GetQueryStringValue("SPN") + "&Report=ByStockPlanner");
                    firstCell.Controls.Add(new HyperLink
                    {
                        NavigateUrl = URL,
                        Target = "_blank",
                        Text = firstCell.Text
                    });


                    if (e.Row.Cells[2].Text != "0")
                    {
                        URL2 = EncryptQuery("~/ModuleUI/Appointment/Reports/OpenPurchaseOrderOverview.aspx?SPGID=" + GetQueryStringValue("SPGID") + "&SiteID=" + Convert.ToString(e.Row.Cells[1].Text) + "&VendorID=" + vendorids + "&CountryID=" + countryIds + "&SPID=" + GetQueryStringValue("SPID") + "&Report=Open");
                        e.Row.Cells[2].Controls.Add(new HyperLink
                        {
                            NavigateUrl = URL2,
                            Target = "_blank",
                            Text = e.Row.Cells[2].Text
                        });
                    }

                    if (e.Row.Cells[4].Text != "0")
                    {
                        URL3 = EncryptQuery("~/ModuleUI/Appointment/Reports/OpenPurchaseOrderOverview.aspx?SPGID=" + GetQueryStringValue("SPGID") + "&SiteID=" + Convert.ToString(e.Row.Cells[1].Text) + "&VendorID=" + vendorids + "&CountryID=" + countryIds + "&SPID=" + GetQueryStringValue("SPID") + "&Report=OverDue");
                        e.Row.Cells[4].Controls.Add(new HyperLink
                        {
                            NavigateUrl = URL3,
                            Target = "_blank",
                            Text = e.Row.Cells[4].Text
                        });
                    }

                    if (e.Row.Cells[6].Text != "0")
                    {

                        URL6 = EncryptQuery("~/ModuleUI/Appointment/Reports/OpenPurchaseOrderOverview.aspx?SPGID=" + GetQueryStringValue("SPGID") + "&SiteID=" + Convert.ToString(e.Row.Cells[1].Text) + "&VendorID=" + vendorids + "&CountryID=" + countryIds + "&SPID=" + GetQueryStringValue("SPID") + "&Report=OverDue" + "&FilterDays=LessThan3Days");
                        e.Row.Cells[6].Controls.Add(new HyperLink
                        {
                            NavigateUrl = URL6,
                            Target = "_blank",
                            Text = e.Row.Cells[6].Text
                        });
                    }

                    if (e.Row.Cells[7].Text != "0")
                    {
                        URL7 = EncryptQuery("~/ModuleUI/Appointment/Reports/OpenPurchaseOrderOverview.aspx?SPGID=" + GetQueryStringValue("SPGID") + "&SiteID=" + Convert.ToString(e.Row.Cells[1].Text) + "&VendorID=" + vendorids + "&CountryID=" + countryIds + "&SPID=" + GetQueryStringValue("SPID") + "&Report=OverDue" + "&FilterDays=LessThan1Week");
                        e.Row.Cells[7].Controls.Add(new HyperLink
                        {
                            NavigateUrl = URL7,
                            Target = "_blank",
                            Text = e.Row.Cells[7].Text
                        });
                    }

                    if (e.Row.Cells[8].Text != "0")
                    {
                        URL8 = EncryptQuery("~/ModuleUI/Appointment/Reports/OpenPurchaseOrderOverview.aspx?SPGID=" + GetQueryStringValue("SPGID") + "&SiteID=" + Convert.ToString(e.Row.Cells[1].Text) + "&VendorID=" + vendorids + "&CountryID=" + countryIds + "&SPID=" + GetQueryStringValue("SPID") + "&Report=OverDue" + "&FilterDays=LessThan2Week");
                        e.Row.Cells[8].Controls.Add(new HyperLink
                        {
                            NavigateUrl = URL8,
                            Target = "_blank",
                            Text = e.Row.Cells[8].Text
                        });
                    }

                    if (e.Row.Cells[9].Text != "0")
                    {
                        URL9 = EncryptQuery("~/ModuleUI/Appointment/Reports/OpenPurchaseOrderOverview.aspx?SPGID=" + GetQueryStringValue("SPGID") + "&SiteID=" + Convert.ToString(e.Row.Cells[1].Text) + "&VendorID=" + vendorids + "&CountryID=" + countryIds + "&SPID=" + GetQueryStringValue("SPID") + "&Report=OverDue" + "&FilterDays=LessThan1Month");
                        e.Row.Cells[9].Controls.Add(new HyperLink
                        {
                            NavigateUrl = URL9,
                            Target = "_blank",
                            Text = e.Row.Cells[9].Text
                        });
                    }

                    if (e.Row.Cells[10].Text != "0")
                    {
                        URL10 = EncryptQuery("~/ModuleUI/Appointment/Reports/OpenPurchaseOrderOverview.aspx?SPGID=" + GetQueryStringValue("SPGID") + "&SiteID=" + Convert.ToString(e.Row.Cells[1].Text) + "&VendorID=" + vendorids + "&CountryID=" + countryIds + "&SPID=" + GetQueryStringValue("SPID") + "&Report=OverDue" + "&FilterDays=LessThan3Month");
                        e.Row.Cells[10].Controls.Add(new HyperLink
                        {
                            NavigateUrl = URL10,
                            Target = "_blank",
                            Text = e.Row.Cells[10].Text
                        });
                    }

                    if (e.Row.Cells[11].Text != "0")
                    {
                        URL11 = EncryptQuery("~/ModuleUI/Appointment/Reports/OpenPurchaseOrderOverview.aspx?SPGID=" + GetQueryStringValue("SPGID") + "&SiteID=" + Convert.ToString(e.Row.Cells[1].Text) + "&VendorID=" + vendorids + "&CountryID=" + countryIds + "&SPID=" + GetQueryStringValue("SPID") + "&Report=OverDue" + "&FilterDays=GreaterThan3Month");
                        e.Row.Cells[11].Controls.Add(new HyperLink
                        {
                            NavigateUrl = URL11,
                            Target = "_blank",
                            Text = e.Row.Cells[11].Text
                        });
                    }
                }
            }
        }
    }
    private void SetSession(Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE)
    {
        Session["OpenPOReportNew"] = null;
        TextBox txtVendor = (TextBox)msVendor.FindControl("ucVendor").FindControl("txtVendorNo");
        Hashtable htBOPenaltyOverview = new Hashtable();
        htBOPenaltyOverview.Add("ReportType", oUp_PurchaseOrderDetailBE.Item_type);
        htBOPenaltyOverview.Add("SelectedCountryIDs", oUp_PurchaseOrderDetailBE.SelectedCountryIDs);
        htBOPenaltyOverview.Add("SelectedStockPlannerGroupID", oUp_PurchaseOrderDetailBE.StockPlannerGroupingID);
        htBOPenaltyOverview.Add("SelectedStockPlannerGroupName", oUp_PurchaseOrderDetailBE.StockPlannerGroupingName);
        htBOPenaltyOverview.Add("SelectedVendorIDs", oUp_PurchaseOrderDetailBE.SelectedVendorIDs);
        htBOPenaltyOverview.Add("SelectedStockPlannerID", oUp_PurchaseOrderDetailBE.StockPlannerID);
        htBOPenaltyOverview.Add("SelectedStockPlannerName", oUp_PurchaseOrderDetailBE.StockPlannerName);
        htBOPenaltyOverview.Add("IsSearchedByVendorNo", msVendor.IsSearchedByVendorNo);
        htBOPenaltyOverview.Add("txtVendor", txtVendor.Text);

        Session["OpenPOReportNew"] = htBOPenaltyOverview;
    }
    public void RetainSearchData()
    {

        //msVendor.setVendorsOnPostBack();
        // multiSelectStockPlannerGrouping.setStockPlannerGroupingsOnPostBack();
        // msCountry.SetCountryOnPostBack();
        Hashtable htBOPenaltyOverview = (Hashtable)Session["OpenPOReportNew"];

        if ((htBOPenaltyOverview.ContainsKey("SelectedStockPlannerGroupName") && htBOPenaltyOverview["SelectedStockPlannerGroupName"] != null && htBOPenaltyOverview["SelectedStockPlannerGroupName"] != ""))
        {
            ddlStockPlannerGroupings.SelectedItem.Text = htBOPenaltyOverview["SelectedStockPlannerGroupName"].ToString();
        }

        if ((htBOPenaltyOverview.ContainsKey("StockPlannerName") && htBOPenaltyOverview["StockPlannerName"] != null && htBOPenaltyOverview["StockPlannerName"] != ""))
        {
            ddlStockPlanner.SelectedItem.Text = htBOPenaltyOverview["StockPlannerName"].ToString();
        }
        if ((htBOPenaltyOverview.ContainsKey("ReportType") && htBOPenaltyOverview["ReportType"] != null && htBOPenaltyOverview["ReportType"] != ""))
        {
            if (Convert.ToString(htBOPenaltyOverview["ReportType"]) == "ByGroup")
            {
                rdoGroupBy.Checked = true;
            }
            else
            {
                rdoByPlanner.Checked = true;
            }
        }

        //*********** Country ***************
        string CountryIDs = (htBOPenaltyOverview.ContainsKey("SelectedCountryIDs") && htBOPenaltyOverview["SelectedCountryIDs"] != null) ? htBOPenaltyOverview["SelectedCountryIDs"].ToString() : "";
        ListBox lstRightCountry = msCountry.FindControl("lstRight") as ListBox;
        ListBox lstLeftCountry = msCountry.FindControl("lstLeft") as ListBox;
        string msCountryid = string.Empty;
        lstRightCountry.Items.Clear();
        //lstRightSite.Items.Clear();
        msCountry.SelectedCountryIDs = "";
        msCountry.BindCountry();
        if (lstLeftCountry != null && lstRightCountry != null && !string.IsNullOrEmpty(CountryIDs))
        {
            //lstLeftSite.Items.Clear();
            lstRightCountry.Items.Clear();
            // msCountry.BindCountry();

            string[] strCountryIDs = CountryIDs.Split(',');
            for (int index = 0; index < strCountryIDs.Length; index++)
            {
                ListItem listItem = lstLeftCountry.Items.FindByValue(strCountryIDs[index]);
                if (listItem != null)
                {
                    msCountryid = msCountryid + strCountryIDs[index].ToString() + ",";
                    lstRightCountry.Items.Add(listItem);
                    lstLeftCountry.Items.Remove(listItem);
                }

            }
            if (string.IsNullOrEmpty(msCountry.SelectedCountryIDs))
                msCountry.SelectedCountryIDs = msCountryid.Trim(',');
        }

        //********** Vendor ***************
        string txtVendor = (htBOPenaltyOverview.ContainsKey("txtVendor") && htBOPenaltyOverview["txtVendor"] != null) ? htBOPenaltyOverview["txtVendor"].ToString() : "";
        string VendorId = (htBOPenaltyOverview.ContainsKey("SelectedVendorIDs") && htBOPenaltyOverview["SelectedVendorIDs"] != null) ? htBOPenaltyOverview["SelectedVendorIDs"].ToString() : "";
        TextBox txtVendorId = msVendor.FindControl("ucVendor").FindControl("txtVendorNo") as TextBox;

        bool IsSearchedByVendorNo = (htBOPenaltyOverview.ContainsKey("IsSearchedByVendorNo") && htBOPenaltyOverview["IsSearchedByVendorNo"] != null) ? Convert.ToBoolean(htBOPenaltyOverview["IsSearchedByVendorNo"]) : false;
        string txtVendorIdText = txtVendorId.Text;
        ucListBox lstRightVendor = msVendor.FindControl("lstRight") as ucListBox;
        ucListBox lstLeftVendor = msVendor.FindControl("ucVendor").FindControl("lstLeft") as ucListBox;
        string strVendorId = string.Empty;
        int value;
        lstLeftVendor.Items.Clear();
        lstRightVendor.Items.Clear();
        if (lstLeftVendor != null && lstRightVendor != null && (!string.IsNullOrEmpty(VendorId) || !string.IsNullOrEmpty(VendorId)))
        {
            lstLeftVendor.Items.Clear();
            //lstRightVendor.Items.Clear();

            if (IsSearchedByVendorNo == true)
            {
                msVendor.SearchVendorNumberClick(txtVendor);
                //parsing successful 
            }
            else
            {
                msVendor.SearchVendorClick(txtVendor);
                //parsing failed. 
            }

            if (!string.IsNullOrEmpty(VendorId))
            {
                string[] strIncludeVendorIDs = VendorId.Split(',');
                for (int index = 0; index < strIncludeVendorIDs.Length; index++)
                {
                    ListItem listItem = lstLeftVendor.Items.FindByValue(strIncludeVendorIDs[index]);
                    if (listItem != null)
                    {

                        strVendorId += strIncludeVendorIDs[index] + ",";
                        lstRightVendor.Items.Add(listItem);
                        lstLeftVendor.Items.Remove(listItem);
                    }
                }
            }

            HiddenField hdnSelectedVendor = msVendor.FindControl("hiddenSelectedIDs") as HiddenField;
            hdnSelectedVendor.Value = strVendorId;
        }

        Session["OpenPOReportNew"] = null;
    }

}
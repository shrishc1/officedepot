﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebUtilities;
using BusinessLogicLayer.ModuleBAL.Upload;
using BusinessEntities.ModuleBE.Upload;
using Utilities;
using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using BaseControlLibrary;
using BusinessEntities.ModuleBE.Appointment.Booking;
using BusinessEntities.ModuleBE.Discrepancy.LogDiscrepancy;
using BusinessEntities.ModuleBE.Upload;
using BusinessLogicLayer.ModuleBAL.Upload;
using System.Web.UI;
using WebUtilities;
using System.Linq;


public partial class DeliveriesPerPurchaseOrderReport : CommonPage
{
    string Summary = WebCommon.getGlobalResourceValue("Summary");
    string DetailbyReceipt = WebCommon.getGlobalResourceValue("DetailbyReceipt");
    string DetailbyBooking = WebCommon.getGlobalResourceValue("DetailbyBooking");

    protected void Page_InIt(object sender, EventArgs e)
    {
        //ucVendorTemplateSelect.CurrentPage = this;
        msSite.isMoveAllRequired = true;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            txtFromDate.Text = DateTime.Now.AddDays(-DateTime.Now.Day + 1).ToString("dd/MM/yyyy");
            txtToDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
            hdnJSFromDt.Value = txtFromDate.Text;
            hdnJSToDt.Value = txtToDate.Text;
            this.BindSearchOption();
            btnBackSearch.Visible = false;
        }

        msCountry.SetCountryOnPostBack();
        msSite.setSitesOnPostBack();
        msVendor.setVendorsOnPostBack();

        if (rblSearchOption.Items.Count > 0)
        {
            switch (rblSearchOption.SelectedIndex)
            {
                case 0: // Summary
                    btnExportToExcelSummary.CurrentPage = this;
                    btnExportToExcelSummary.FileName = "DeliveriesPerPOSummary";
                    btnExportToExcelSummary.GridViewControl = gvSummaryExcel;
                    break;
                case 1: // Detail by Receipt
                    btnExportToExcelDetailByReceipt.CurrentPage = this;
                    btnExportToExcelDetailByReceipt.FileName = "DeliveriesPerPODetailbyReceipt";
                    btnExportToExcelDetailByReceipt.GridViewControl = gvDetailByReceiptExcel;
                    break;
                case 2: // Detail by Booking
                    btnExportToExcelDetailByBooking.CurrentPage = this;
                    btnExportToExcelDetailByBooking.FileName = "DeliveriesPerPODetailbyBooking";
                    btnExportToExcelDetailByBooking.GridViewControl = gvDetailByBookingExcel;
                    break;
            }
        }
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        this.BindGrids();
    }

    protected void btnBackSearch_Click(object sender, EventArgs e)
    {
        mvDiscrepancyList.ActiveViewIndex = 0;
        btnBackSearch.Visible = false;
        txtFromDate.Text = DateTime.Now.AddDays(-DateTime.Now.Day + 1).ToString("dd/MM/yyyy");
        txtToDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
        hdnJSFromDt.Value = txtFromDate.Text;
        hdnJSToDt.Value = txtToDate.Text;

        switch (rblSearchOption.SelectedIndex)
        {
            case 0: // Summary               
                gvSummary.DataSource = null;
                gvSummary.DataBind();
                gvSummaryExcel.DataSource = null;
                gvSummaryExcel.DataBind();
                ViewState["lstPOSummary"] = null;
                break;
            case 1: // Detail by Receipt                
                gvDetailByReceipt.DataSource = null;
                gvDetailByReceipt.DataBind();
                gvDetailByReceiptExcel.DataSource = null;
                gvDetailByReceiptExcel.DataBind();
                ViewState["lstPODetailbyReceipt"] = null;
                break;
            case 2: // Detail by Booking                
                gvDetailByBooking.DataSource = null;
                gvDetailByBooking.DataBind();
                gvDetailByBookingExcel.DataSource = null;
                gvDetailByBookingExcel.DataBind();
                ViewState["lstPODetailbyBooking"] = null;
                break;
        }
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        btnBack.Visible = false;
        btnBackSearch.Visible = true;
        rptPOReceipts.Visible = false;
        gvDetailByReceipt.Visible = true;
        if (gvDetailByReceipt.Rows.Count > 0)
            btnExportToExcelDetailByReceipt.Visible = true;

        rptPOReceipts.DataSource = null;
        rptPOReceipts.DataBind();
    }

    private void BindSearchOption()
    {
        rblSearchOption.Items.Clear();
        ListItem lstItem = new ListItem(Summary, "1");
        rblSearchOption.Items.Add(lstItem);
        lstItem = new ListItem(DetailbyReceipt, "2");
        rblSearchOption.Items.Add(lstItem);
        lstItem = new ListItem(DetailbyBooking, "3");
        rblSearchOption.Items.Add(lstItem);
        rblSearchOption.SelectedIndex = 0;
    }

    public DateTime GetLastBusinessDay()
    {
        var LastBusinessDay = DateTime.Now.AddDays(-1);

        if (LastBusinessDay.DayOfWeek == DayOfWeek.Sunday)
            LastBusinessDay = LastBusinessDay.AddDays(-2);
        else if (LastBusinessDay.DayOfWeek == DayOfWeek.Saturday)
            LastBusinessDay = LastBusinessDay.AddDays(-1);

        return LastBusinessDay;
    }

    private void BindGrids()
    {
        if (rblSearchOption.Items.Count > 0)
        {
            var purchaseOrderDetailBE = new Up_PurchaseOrderDetailBE();
            if (!string.IsNullOrEmpty(msCountry.SelectedCountryIDs) && !string.IsNullOrWhiteSpace(msCountry.SelectedCountryIDs))
                purchaseOrderDetailBE.SelectedCountryIDs = msCountry.SelectedCountryIDs;

            if (!string.IsNullOrEmpty(msSite.SelectedSiteIDs) && !string.IsNullOrWhiteSpace(msSite.SelectedSiteIDs))
                purchaseOrderDetailBE.SelectedSiteIDs = msSite.SelectedSiteIDs;

            if (!string.IsNullOrEmpty(msVendor.SelectedVendorIDs) && !string.IsNullOrWhiteSpace(msVendor.SelectedVendorIDs))
                purchaseOrderDetailBE.SelectedVendorIDs = msVendor.SelectedVendorIDs;

            purchaseOrderDetailBE.FromDate = string.IsNullOrEmpty(txtFromDate.Text) ? (DateTime?)null : Common.GetMM_DD_YYYY(hdnJSFromDt.Value);
            purchaseOrderDetailBE.ToDate = string.IsNullOrEmpty(txtToDate.Text) ? (DateTime?)null : Common.GetMM_DD_YYYY(hdnJSToDt.Value);
            var purchaseOrderDetailBAL = new UP_PurchaseOrderDetailBAL();

            switch (rblSearchOption.SelectedIndex)
            {
                case 0: // Summary
                    mvDiscrepancyList.ActiveViewIndex = 1;
                    purchaseOrderDetailBE.Action = "GetDelPerPOSummaryReport";
                    var lstPOSummary = purchaseOrderDetailBAL.GetDelPerPOSummaryReportBAL(purchaseOrderDetailBE);
                    gvSummary.DataSource = null;
                    gvSummaryExcel.DataSource = null;
                    btnExportToExcelSummary.Visible = false;
                    if (lstPOSummary != null && lstPOSummary.Count > 0)
                    {
                        gvSummary.DataSource = lstPOSummary;
                        gvSummaryExcel.DataSource = lstPOSummary;
                        ViewState["lstPOSummary"] = lstPOSummary;
                        btnExportToExcelSummary.Visible = true;
                        gvSummary.PageIndex = 0;
                    }

                    gvSummary.DataBind();
                    gvSummaryExcel.DataBind();
                    mvDiscrepancyList.ActiveViewIndex = 1;
                    break;
                case 1: // Detail by Receipt
                    mvDiscrepancyList.ActiveViewIndex = 2;
                    purchaseOrderDetailBE.Action = "GetDelPerPODetailByReceiptReport";
                    var lstPODetailbyReceipt = purchaseOrderDetailBAL.GetDelPerPODetailByReceiptReportBAL(purchaseOrderDetailBE);
                    gvDetailByReceipt.DataSource = null;
                    gvDetailByReceiptExcel.DataSource = null;
                    btnExportToExcelDetailByReceipt.Visible = false;
                    if (lstPODetailbyReceipt != null && lstPODetailbyReceipt.Count > 0)
                    {
                        gvDetailByReceipt.DataSource = lstPODetailbyReceipt;
                        gvDetailByReceiptExcel.DataSource = lstPODetailbyReceipt;
                        ViewState["lstPODetailbyReceipt"] = lstPODetailbyReceipt;
                        btnExportToExcelDetailByReceipt.Visible = true;                        
                        gvDetailByReceipt.PageIndex = 0;
                    }
                    
                    gvDetailByReceipt.DataBind();
                    gvDetailByReceiptExcel.DataBind();

                    gvDetailByReceipt.Visible = true;
                    rptPOReceipts.Visible = false;
                    mvDiscrepancyList.ActiveViewIndex = 2;
                    break;
                case 2: // Detail by Booking
                    mvDiscrepancyList.ActiveViewIndex = 3;
                    purchaseOrderDetailBE.Action = "GetDelPerPODetailByBookingReport";
                    var lstPODetailbyBooking = purchaseOrderDetailBAL.GetDelPerPODetailByBookingReportBAL(purchaseOrderDetailBE);
                    gvDetailByBooking.DataSource = null;
                    gvDetailByBookingExcel.DataSource = null;
                    btnExportToExcelDetailByBooking.Visible = false;
                    if (lstPODetailbyBooking != null && lstPODetailbyBooking.Count > 0)
                    {
                        gvDetailByBooking.DataSource = lstPODetailbyBooking;
                        gvDetailByBookingExcel.DataSource = lstPODetailbyBooking;
                        ViewState["lstPODetailbyBooking"] = lstPODetailbyBooking;
                        btnExportToExcelDetailByBooking.Visible = true;
                        gvDetailByBooking.PageIndex = 0;
                    }

                    gvDetailByBooking.DataBind();                   
                    gvDetailByBookingExcel.DataBind();
                    mvDiscrepancyList.ActiveViewIndex = 3;
                    break;
            }

            btnBackSearch.Visible = true;
        }
    }

    protected void gvSummary_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        if (ViewState["lstPOSummary"] != null)
        {
            gvSummary.PageIndex = e.NewPageIndex;
            gvSummary.DataSource = (List<Up_PurchaseOrderDetailBE>)ViewState["lstPOSummary"];
            gvSummary.DataBind();
        }
    }

    protected void gvDetailByReceipt_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        if (ViewState["lstPODetailbyReceipt"] != null)
        {
            gvDetailByReceipt.PageIndex = e.NewPageIndex;
            gvDetailByReceipt.DataSource = (List<Up_PurchaseOrderDetailBE>)ViewState["lstPODetailbyReceipt"];
            gvDetailByReceipt.DataBind();
        }
    }

    protected void gvDetailByBooking_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        if (ViewState["lstPODetailbyBooking"] != null)
        {
            gvDetailByBooking.PageIndex = e.NewPageIndex;
            gvDetailByBooking.DataSource = (List<Up_PurchaseOrderDetailBE>)ViewState["lstPODetailbyBooking"];
            gvDetailByBooking.DataBind();
        }
    }

    public override BusinessEntities.BaseBe[] SubSort(GridViewSortEventArgs e)
    {
        if (ViewState["lstPOSummary"] != null)
            return Utilities.GenericListHelper<Up_PurchaseOrderDetailBE>.SortList((List<Up_PurchaseOrderDetailBE>)ViewState["lstPOSummary"], e.SortExpression, e.SortDirection).ToArray();
        else if (ViewState["lstPODetailbyReceipt"] != null)
            return Utilities.GenericListHelper<Up_PurchaseOrderDetailBE>.SortList((List<Up_PurchaseOrderDetailBE>)ViewState["lstPODetailbyReceipt"], e.SortExpression, e.SortDirection).ToArray();
        else if (ViewState["lstPODetailbyBooking"] != null)
            return Utilities.GenericListHelper<Up_PurchaseOrderDetailBE>.SortList((List<Up_PurchaseOrderDetailBE>)ViewState["lstPODetailbyBooking"], e.SortExpression, e.SortDirection).ToArray();
        else
            return null;
    }

    protected void gvDetailByReceipt_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType != DataControlRowType.Header && e.Row.RowType != DataControlRowType.Footer)
        {
            var lnkPOHashValue = (ucLinkButton)e.Row.FindControl("lnkPOHashValue");
            var lblPOHashValue = (ucLiteral)e.Row.FindControl("lblPOHashValue");
            var lblReceiptDateValue = (ucLiteral)e.Row.FindControl("lblReceiptDateValue");
            if (lblReceiptDateValue != null)
            {
                if (!string.IsNullOrEmpty(lblReceiptDateValue.Text))
                {
                    lnkPOHashValue.Visible = true;
                    lblPOHashValue.Visible = false;
                }
                else
                {
                    lnkPOHashValue.Visible = false;
                    lblPOHashValue.Visible = true;
                }
            }
        }
    }

    protected void gvDetailByReceipt_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName.Equals("ReceiptInfo"))
        {
            GridViewRow oGridViewRow = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
            int intRowIndex = oGridViewRow.RowIndex;

            ucLinkButton lnkPOHashValue = (ucLinkButton)gvDetailByReceipt.Rows[intRowIndex].FindControl("lnkPOHashValue");
            HiddenField hdnSiteIDValue = (HiddenField)gvDetailByReceipt.Rows[intRowIndex].FindControl("hdnSiteIDValue");
            HiddenField hdnVendorIDValue = (HiddenField)gvDetailByReceipt.Rows[intRowIndex].FindControl("hdnVendorIDValue");
            ucLiteral lblOriginalDueDateValue = (ucLiteral)gvDetailByReceipt.Rows[intRowIndex].FindControl("lblOriginalDueDateValue");
            ucLiteral lblReceiptDateValue = (ucLiteral)gvDetailByReceipt.Rows[intRowIndex].FindControl("lblReceiptDateValue");

            if (!string.IsNullOrEmpty(lnkPOHashValue.Text) && !string.IsNullOrEmpty(hdnSiteIDValue.Value)
                && !string.IsNullOrEmpty(hdnVendorIDValue.Value) && !string.IsNullOrEmpty(lblOriginalDueDateValue.Text)
                && !string.IsNullOrEmpty(lblReceiptDateValue.Text))
            {
                var purchaseOrderDetailBAL = new UP_PurchaseOrderDetailBAL();
                var purchaseOrderDetail = new Up_PurchaseOrderDetailBE();
                purchaseOrderDetail.Action = "GetDelPerPOReceiptData";
                purchaseOrderDetail.Purchase_order = lnkPOHashValue.Text;
                purchaseOrderDetail.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();
                purchaseOrderDetail.Vendor.VendorID = Convert.ToInt32(hdnVendorIDValue.Value);
                purchaseOrderDetail.Original_due_date = Convert.ToDateTime(Common.GetMM_DD_YYYY(lblOriginalDueDateValue.Text));
                purchaseOrderDetail.Site = new BusinessEntities.ModuleBE.AdminFunctions.MAS_SiteBE();
                purchaseOrderDetail.Site.SiteID = Convert.ToInt32(hdnSiteIDValue.Value);
                purchaseOrderDetail.ReceiptDate = Convert.ToDateTime(Common.GetMM_DD_YYYY(lblReceiptDateValue.Text));
                var lstReceiptData = purchaseOrderDetailBAL.GetGetDelPerPOReceiptDataBAL(purchaseOrderDetail);

                var distinctReceiptDateData = lstReceiptData.OrderBy(receipt => receipt.ReceiptDate)
                    //.ThenBy(receipt => receipt.ReceiptDate)
                    .GroupBy(receipt => receipt.ReceiptDate)
                    .SelectMany(receipt => receipt.Take(1));
                rptPOReceipts.DataSource = distinctReceiptDateData;
                rptPOReceipts.DataBind();

                for (int index = 0; index < rptPOReceipts.Items.Count; index++)
                {
                    ucGridView gvPOReceiptsDetails = (ucGridView)rptPOReceipts.Items[index].FindControl("gvPOReceiptsDetails");
                    HiddenField hdnDateText = (HiddenField)rptPOReceipts.Items[index].FindControl("hdnDateText");
                    if (gvPOReceiptsDetails != null)
                    {
                        gvPOReceiptsDetails.DataSource = lstReceiptData.FindAll(rd => rd.ReceiptDate.Equals(Convert.ToDateTime(hdnDateText.Value)));
                        gvPOReceiptsDetails.DataBind();
                    }
                }
            }

            rptPOReceipts.Visible = true;
            btnBack.Visible = true;
            gvDetailByReceipt.Visible = false;
            btnBackSearch.Visible = false;
            btnExportToExcelDetailByReceipt.Visible = false;
        }
    }
}
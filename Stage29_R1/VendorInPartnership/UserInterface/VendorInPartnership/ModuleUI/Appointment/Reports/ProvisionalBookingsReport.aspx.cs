﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.Appointment.Booking;
using BusinessLogicLayer.ModuleBAL.Appointment.Booking;
using Utilities;
using WebUtilities;

public partial class ProvisionalBookingsReport : CommonPage
{
    const int PageSize = 50;
    const int TotalSiteID = 9999;
    const int TotalSiteCountryID = 9999;
    string TotalSiteName = WebCommon.getGlobalResourceValue("Total");
    int? SumAcceptedValue = 0;
    int? SumRejectedValue = 0;

    protected void Page_Init(object sender, EventArgs e)
    {
        ddlCountry.CurrentPage = this;        
    }

    protected void Page_Load(object sender, EventArgs e)
    {        
        if (!IsPostBack)
        {
            btnBackSearch.Visible = false;
            txtFromDate.Text = DateTime.Now.AddDays(-DateTime.Now.Day + 1).ToString("dd/MM/yyyy");
            txtToDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
            hdnJSFromDt.Value = txtFromDate.Text;
            hdnJSToDt.Value = txtToDate.Text;
            pager1.PageSize = PageSize;
        }

        msSite.setSitesOnPostBack();
        msVendor.setVendorsOnPostBack();
        msCarrier.setCarriersOnPostBack();

        if (rdoDetailed.Checked)
        {
            btnExportToExcelProvBookingsDetail.CurrentPage = this;
            btnExportToExcelProvBookingsDetail.FileName = "ProvisionalBookingDetailReport";
            btnExportToExcelProvBookingsDetail.GridViewControl = gvProvisionalBookingDetailExcel;
        }
        else
        {
            if (ddlReportTypes.SelectedValue.Equals("1"))
            {
                btnExportToExcelProvBookingsSummaryByType.CurrentPage = this;
                btnExportToExcelProvBookingsSummaryByType.FileName = "ProvisionalBookingSummaryReportByType";
                btnExportToExcelProvBookingsSummaryByType.GridViewControl = gvProvisionalBookingByTypeExcel;
            }
            else
            {
                //btnExportToExcelProvBookingsSummaryBySite.CurrentPage = this;
                //btnExportToExcelProvBookingsSummaryBySite.FileName = "ProvisionalBookingSummaryReportBySite";
                //btnExportToExcelProvBookingsSummaryBySite.GridViewControl = gvProvisionalBookingBySiteExcel;
            }
        }
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        if (rdoDetailed.Checked)
        {
            mvDiscrepancyList.ActiveViewIndex = 2;
        }
        else
        {
            mvDiscrepancyList.ActiveViewIndex = 1;
        }

        this.BindGrids();
        btnBackSearch.Visible = true;
    }

    protected void btnBackSearch_Click(object sender, EventArgs e)
    {
        mvDiscrepancyList.ActiveViewIndex = 0;
        lblProvisionalBookingsReport.Text = "Provisional Bookings Report";
        btnBackSearch.Visible = false;
        txtFromDate.Text = DateTime.Now.AddDays(-DateTime.Now.Day + 1).ToString("dd/MM/yyyy");
        txtToDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
        hdnJSFromDt.Value = txtFromDate.Text;
        hdnJSToDt.Value = txtToDate.Text;
    }

    protected void gvProvisionalBookingByType_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        if (ViewState["lstProvBookings"] != null)
        {
            gvProvisionalBookingByType.PageIndex = e.NewPageIndex;
            gvProvisionalBookingByType.DataSource = (List<APPBOK_BookingBE>)ViewState["lstProvBookings"];
            gvProvisionalBookingByType.DataBind();
        }
    }

    protected void gvProvisionalBookingDetail_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        if (ViewState["lstProvBookings"] != null)
        {
            gvProvisionalBookingDetail.PageIndex = e.NewPageIndex;
            gvProvisionalBookingDetail.DataSource = (List<APPBOK_BookingBE>)ViewState["lstProvBookings"];
            gvProvisionalBookingDetail.DataBind();
        }
    }

    public override BusinessEntities.BaseBe[] SubSort(GridViewSortEventArgs e)
    {
        return Utilities.GenericListHelper<APPBOK_BookingBE>.SortList((List<APPBOK_BookingBE>)ViewState["lstProvBookings"], e.SortExpression, e.SortDirection).ToArray();
    }

    protected void ddlReportTypes_SelectedIndexChanged(object sender, System.EventArgs e)
    {
        if (ddlReportTypes.SelectedValue.Equals("1"))
        {
            pnlSummaryByType.Visible = true;
            pnlSummaryBySite.Visible = false;
        }
        else
        {
            pnlSummaryByType.Visible = false;
            pnlSummaryBySite.Visible = true;
        }

        this.BindGrids();
    }

    public void pager_Command(object sender, CommandEventArgs e)
    {
        int currnetPageIndx = Convert.ToInt32(e.CommandArgument);
        pager1.CurrentIndex = currnetPageIndx;
        BindGrids(currnetPageIndx);
    }

    protected void rptSite_OnItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {

            //if (Convert.ToBoolean(ViewState["isExchangeRateShown"]))
            //{
            //    Literal ltSitePreFix = (Literal)e.Item.FindControl("ltSitePreFix");
            //    if (strExchnageRateSite.Contains("," + ltSitePreFix.Text + ","))
            //        ltSitePreFix.Text = ltSitePreFix.Text + "*";
            //}

            List<APPBOK_BookingBE> oVIPManagementReportBEList = (List<APPBOK_BookingBE>)ViewState["lstProvBookings"];
            Repeater rptKeyHeader = (Repeater)e.Item.FindControl("rptKeyHeader");
            string[] oVIPManagementReportBE = oVIPManagementReportBEList.Select(c => c.KeyHeader).ToList()[0].Split('~');


            List<APPBOK_BookingBE> lst = new List<APPBOK_BookingBE>();
            for (int iHeaderCount = 0; iHeaderCount < oVIPManagementReportBE.Length; iHeaderCount++)
                lst.Add(new APPBOK_BookingBE() { KeyHeader = oVIPManagementReportBE[iHeaderCount] });

            rptKeyHeader.DataSource = lst;
            rptKeyHeader.DataBind();

            //var tblSite = (HtmlTable)e.Item.FindControl("tblSite");
            //tblMain.Attributes.Add("width", (lst.Count * 870).ToString() + "px");


            var tdSite = (HtmlTableCell)e.Item.FindControl("tdControlSite");
            tdSite.Attributes.Add("colspan", lst.Count.ToString());

            //int rowWidth = 100 / lst.Count;
            for (int iRowCount = 0; iRowCount < lst.Count; iRowCount++)
            {
                var td = (HtmlTableCell)rptKeyHeader.Items[iRowCount].FindControl("tdControl");
                td.Style.Add("width", "40px");
            }
        }
    }

    protected void rptkeyDetails_OnItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            List<APPBOK_BookingBE> oVIPManagementReportBEList = (List<APPBOK_BookingBE>)ViewState["lstProvBookings"];
            Repeater rptKeyvalue = (Repeater)e.Item.FindControl("rptKeyvalue");
            HyperLink hpLink = (HyperLink)e.Item.FindControl("hpLink");
            Literal ltVendorNameValue = (Literal)e.Item.FindControl("ltVendorNameValue");
            Literal ltVendorIDValue = (Literal)e.Item.FindControl("ltVendorIDValue");

            var lstVendorData = oVIPManagementReportBEList.FindAll(x => x.VendorID == Convert.ToInt32(ltVendorIDValue.Text));
            if (lstVendorData != null && lstVendorData.Count > 0)
            {
                var sumAccepted = lstVendorData.Sum(x => x.Accepted);
                SumAcceptedValue = sumAccepted;               

                var sumRejected = lstVendorData.Sum(x => x.Rejected);
                SumRejectedValue = sumRejected;
            }

            if (string.IsNullOrEmpty(ltVendorNameValue.Text))
                e.Item.Visible = false;
            else
            {
                var uniqueSites = oVIPManagementReportBEList.Select(c => new { c.SiteId, c.SiteName, c.SiteCountryID }).Distinct().OrderBy(o => o.SiteCountryID).ThenBy(o => o.SiteName).ToList();
                oVIPManagementReportBEList = oVIPManagementReportBEList.Where(c => c.Vendor.VendorName == ltVendorNameValue.Text).ToList();
                APPBOK_BookingBE oNewVIPManagementReportBE = new APPBOK_BookingBE();
                for (int iCount = 0; iCount < uniqueSites.Count; iCount++)
                {

                    if (!oVIPManagementReportBEList.Exists(x => x.SiteId == uniqueSites[iCount].SiteId))
                    {
                        oNewVIPManagementReportBE = new APPBOK_BookingBE();
                        oNewVIPManagementReportBE.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();
                        oNewVIPManagementReportBE.Vendor.VendorName = ltVendorNameValue.Text;
                        oNewVIPManagementReportBE.SiteName = uniqueSites[iCount].SiteName;
                        oNewVIPManagementReportBE.SiteId = uniqueSites[iCount].SiteId;
                        oNewVIPManagementReportBE.SiteCountryID = uniqueSites[iCount].SiteCountryID;
                        //oNewVIPManagementReportBE.Accepted = 0;
                        //oNewVIPManagementReportBE.Rejected = 0;
                        oVIPManagementReportBEList.Add(oNewVIPManagementReportBE);
                    }
                }

                oVIPManagementReportBEList = oVIPManagementReportBEList.OrderBy(o => o.SiteCountryID).ThenBy(o => o.SiteName).ToList();
                rptKeyvalue.DataSource = oVIPManagementReportBEList;
                rptKeyvalue.DataBind();
            }
        }
    }

    protected void rptKeyvalue_OnItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            APPBOK_BookingBE i = (APPBOK_BookingBE)e.Item.DataItem;
            Repeater rptKeyValueDetails = (Repeater)e.Item.FindControl("rptKeyValueDetails");
            var lst = new List<APPBOK_BookingBE>();

            if (ViewState["uniqueNo"] == null)
                ViewState["uniqueNo"] = 1;                
            else
                ViewState["uniqueNo"] = Convert.ToInt32(ViewState["uniqueNo"]) + 1;
                        
            if (Convert.ToInt32(ViewState["uniqueSitesCount"]) == Convert.ToInt32(ViewState["uniqueNo"]))
            {
                if (SumAcceptedValue != (int?)null)
                    lst.Add(new APPBOK_BookingBE() { KeyValue = Convert.ToInt32(SumAcceptedValue) });
                else
                    lst.Add(new APPBOK_BookingBE() { KeyValue = 0 });

                if (SumRejectedValue != (int?)null)
                    lst.Add(new APPBOK_BookingBE() { KeyValue = Convert.ToInt32(SumRejectedValue) });
                else
                    lst.Add(new APPBOK_BookingBE() { KeyValue = 0 });

                ViewState["uniqueNo"] = null;
            }
            else
            {
                if (i.Accepted != (int?)null)
                    lst.Add(new APPBOK_BookingBE() { KeyValue = Convert.ToInt32(i.Accepted) });
                else
                    lst.Add(new APPBOK_BookingBE() { KeyValue = 0 });

                if (i.Rejected != (int?)null)
                    lst.Add(new APPBOK_BookingBE() { KeyValue = Convert.ToInt32(i.Rejected) });
                else
                    lst.Add(new APPBOK_BookingBE() { KeyValue = 0 });
            }

            rptKeyValueDetails.DataSource = lst;
            rptKeyValueDetails.DataBind();

            for (int iRowCount = 0; iRowCount < lst.Count; iRowCount++)
            {
                var td = (HtmlTableCell)rptKeyValueDetails.Items[iRowCount].FindControl("tdControl");
                td.Style.Add("width", "40px");
            }

            /*
            Literal ltAcceptedTotal = (Literal)e.Item.FindControl("ltAcceptedTotal");
            if (SumAcceptedValue != null && SumAcceptedValue > 0)
                ltAcceptedTotal.Text = SumAcceptedValue.ToString(); //Math.Round(Convert.ToDouble(SumAcceptedValue), 2).ToString();                
            else
                ltAcceptedTotal.Text = "0";

            Literal ltRejectedTotal = (Literal)e.Item.FindControl("ltRejectedTotal");
            if (SumRejectedValue > 0)
                ltRejectedTotal.Text = SumRejectedValue.ToString(); //Math.Round(Convert.ToDouble(SumRejectedValue), 2).ToString();
            else
                ltRejectedTotal.Text = "0";
            */
        }
    }

    private void BindGrids(int Page = 1)
    {
        var bookingBE = new APPBOK_BookingBE();
        if (!string.IsNullOrEmpty(ddlCountry.innerControlddlCountry.SelectedValue) && !string.IsNullOrWhiteSpace(ddlCountry.innerControlddlCountry.SelectedValue))
            bookingBE.SelectedCountryID = Convert.ToInt32(ddlCountry.innerControlddlCountry.SelectedValue);

        if (!string.IsNullOrEmpty(msSite.SelectedSiteIDs) && !string.IsNullOrWhiteSpace(msSite.SelectedSiteIDs))
            bookingBE.SelectedSiteIds = msSite.SelectedSiteIDs;

        if (!string.IsNullOrEmpty(msVendor.SelectedVendorIDs) && !string.IsNullOrWhiteSpace(msVendor.SelectedVendorIDs))
            bookingBE.SelectedVendorIDs = msVendor.SelectedVendorIDs;

        if (!string.IsNullOrEmpty(msCarrier.SelectedCarrierIDs) && !string.IsNullOrWhiteSpace(msCarrier.SelectedCarrierIDs))
            bookingBE.SelectedCarrierIDs = msCarrier.SelectedCarrierIDs;

        bookingBE.FromDate = string.IsNullOrEmpty(txtFromDate.Text) ? (DateTime?)null : Common.GetMM_DD_YYYY(hdnJSFromDt.Value);
        bookingBE.ToDate = string.IsNullOrEmpty(txtToDate.Text) ? (DateTime?)null : Common.GetMM_DD_YYYY(hdnJSToDt.Value);
        bookingBE.PageNumber = Page;
        bookingBE.PageSize = PageSize;

        var bookingBAL = new APPBOK_BookingBAL();
        var lstProvBookings = new List<APPBOK_BookingBE>();
        if (rdoDetailed.Checked)
        {
            bookingBE.Action = "ProvBookDetailView";
            lstProvBookings = bookingBAL.GetProvisionalBookingReportDetailViewBAL(bookingBE);
            if (lstProvBookings != null && lstProvBookings.Count > 0)
            {
                gvProvisionalBookingDetail.DataSource = lstProvBookings;
                gvProvisionalBookingDetailExcel.DataSource = lstProvBookings;
                ViewState["lstProvBookings"] = lstProvBookings;
                btnExportToExcelProvBookingsDetail.Visible = true;
                gvProvisionalBookingDetail.PageIndex = 0;
            }
            else
            {
                gvProvisionalBookingDetail.DataSource = null;
                gvProvisionalBookingDetailExcel.DataSource = null;
                btnExportToExcelProvBookingsDetail.Visible = false;
            }
            gvProvisionalBookingDetail.DataBind();
            gvProvisionalBookingDetailExcel.DataBind();
        }
        else
        {
            if (ddlReportTypes.SelectedValue.Equals("1"))
            {
                bookingBE.Action = "ProvBookSummaryViewByType";
                lstProvBookings = bookingBAL.GetProvisionalBookingReportViewByTypeBAL(bookingBE);
                if (lstProvBookings != null && lstProvBookings.Count > 0)
                {
                    gvProvisionalBookingByType.DataSource = lstProvBookings;
                    gvProvisionalBookingByTypeExcel.DataSource = lstProvBookings;
                    ViewState["lstProvBookings"] = lstProvBookings;
                    btnExportToExcelProvBookingsSummaryByType.Visible = true;                    
                    gvProvisionalBookingByType.PageIndex = 0;
                }
                else
                {
                    gvProvisionalBookingByType.DataSource = null;
                    gvProvisionalBookingByTypeExcel.DataSource = null;
                    btnExportToExcelProvBookingsSummaryByType.Visible = false;
                }
                gvProvisionalBookingByType.DataBind();
                gvProvisionalBookingByTypeExcel.DataBind();
                btnExportToExcel.Visible = false;
            }
            else
            {
                bookingBE.Action = "ProvBookSummaryViewBySite";
                lstProvBookings = bookingBAL.GetProvisionalBookingReportSummaryViewBySiteBAL(bookingBE);
                if (lstProvBookings != null && lstProvBookings.Count > 0)
                {
                    /* Logic to adding Total Column for Grid Output */
                    var totalBookingBE = new APPBOK_BookingBE();
                    totalBookingBE.SiteId = TotalSiteID;
                    totalBookingBE.SiteName = TotalSiteName;
                    totalBookingBE.SiteCountryID = TotalSiteCountryID;
                    totalBookingBE.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();
                    lstProvBookings.Add(totalBookingBE);

                    ViewState["lstProvBookings"] = lstProvBookings;
                    var uniqueSites = lstProvBookings.Select(c => new { c.SiteId, c.SiteName, c.SiteCountryID }).Distinct().OrderBy(o => o.SiteCountryID).ThenBy(o => o.SiteName).ToList();
                    rptSite.DataSource = uniqueSites;
                    ViewState["uniqueSitesCount"] = Convert.ToInt32(uniqueSites.Count);

                    var uniqueAreaKeys = lstProvBookings.Select(c => new { c.Vendor.VendorName, c.VendorID }).Distinct().OrderByDescending(o => o.VendorName).ToList();
                    rptkeyDetails.DataSource = uniqueAreaKeys;
                                       
                    pager1.ItemCount = lstProvBookings[0].TotalRecords;
                    pager1.Visible = true;
                    ViewState["TotalRecords"] = lstProvBookings[0].TotalRecords;
                    btnExportToExcel.Visible = true;
                    lblVendorCarrierName.Visible = true;
                    thVendorCarrierName.Visible = true;
                    lblRecordNotFound.Visible = false;
                }
                else
                {
                    rptSite.DataSource = null;
                    rptkeyDetails.DataSource = null;
                    pager1.ItemCount = 0;
                    pager1.Visible = false;
                    btnExportToExcel.Visible = false;
                    lblVendorCarrierName.Visible = false;
                    thVendorCarrierName.Visible = false;
                    lblRecordNotFound.Visible = true;
                }
                rptSite.DataBind();
                rptkeyDetails.DataBind();
                pager1.CurrentIndex = Page;
                btnExportToExcelProvBookingsSummaryByType.Visible = false;
            }
        }
    }

    private void ProvBookingsSummaryBySiteExport(int Page = 1)
    {
        var bookingBE = new APPBOK_BookingBE();
        if (!string.IsNullOrEmpty(ddlCountry.innerControlddlCountry.SelectedValue) && !string.IsNullOrWhiteSpace(ddlCountry.innerControlddlCountry.SelectedValue))
            bookingBE.SelectedCountryID = Convert.ToInt32(ddlCountry.innerControlddlCountry.SelectedValue);

        if (!string.IsNullOrEmpty(msSite.SelectedSiteIDs) && !string.IsNullOrWhiteSpace(msSite.SelectedSiteIDs))
            bookingBE.SelectedSiteIds = msSite.SelectedSiteIDs;

        if (!string.IsNullOrEmpty(msVendor.SelectedVendorIDs) && !string.IsNullOrWhiteSpace(msVendor.SelectedVendorIDs))
            bookingBE.SelectedVendorIDs = msVendor.SelectedVendorIDs;

        if (!string.IsNullOrEmpty(msCarrier.SelectedCarrierIDs) && !string.IsNullOrWhiteSpace(msCarrier.SelectedCarrierIDs))
            bookingBE.SelectedCarrierIDs = msCarrier.SelectedCarrierIDs;

        bookingBE.FromDate = string.IsNullOrEmpty(txtFromDate.Text) ? (DateTime?)null : Common.GetMM_DD_YYYY(hdnJSFromDt.Value);
        bookingBE.ToDate = string.IsNullOrEmpty(txtToDate.Text) ? (DateTime?)null : Common.GetMM_DD_YYYY(hdnJSToDt.Value);
        bookingBE.PageNumber = Page;
        bookingBE.PageSize = Convert.ToInt32(ViewState["TotalRecords"]);

        var bookingBAL = new APPBOK_BookingBAL();
        var lstProvBookings = new List<APPBOK_BookingBE>();
        bookingBE.Action = "ProvBookSummaryViewBySite";
        lstProvBookings = bookingBAL.GetProvisionalBookingReportSummaryViewBySiteBAL(bookingBE);
        if (lstProvBookings != null && lstProvBookings.Count > 0)
        {
            /* Logic to adding Total Column for Grid Output */
            var totalBookingBE = new APPBOK_BookingBE();
            totalBookingBE.SiteId = TotalSiteID;
            totalBookingBE.SiteName = TotalSiteName;
            totalBookingBE.SiteCountryID = TotalSiteCountryID;
            totalBookingBE.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();
            lstProvBookings.Add(totalBookingBE);

            ViewState["lstProvBookings"] = lstProvBookings;
            var uniqueSites = lstProvBookings.Select(c => new { c.SiteId, c.SiteName, c.SiteCountryID }).Distinct().OrderBy(o => o.SiteCountryID).ThenBy(o => o.SiteName).ToList();
            rptSiteExport.DataSource = uniqueSites;
            //ViewState["uniqueSitesCount"] = Convert.ToInt32(uniqueSites.Count);

            var uniqueAreaKeys = lstProvBookings.Select(c => new { c.Vendor.VendorName, c.VendorID }).Distinct().OrderByDescending(o => o.VendorName).ToList();
            rptkeyDetailsExport.DataSource = uniqueAreaKeys;

            //pager1.ItemCount = lstProvBookings[0].TotalRecords;
            //pager1.Visible = true;
            //ViewState["TotalRecords"] = lstProvBookings[0].TotalRecords;
        }
        else
        {
            rptSiteExport.DataSource = null;
            rptkeyDetailsExport.DataSource = null;
            pager1.ItemCount = 0;
            pager1.Visible = false;
        }
        rptSiteExport.DataBind();
        rptkeyDetailsExport.DataBind();
        //pager1.CurrentIndex = Page;
    }

    protected void btnExportToExcelProvBookingsSummaryBySite_Click(object sender, EventArgs e)
    {
        ProvBookingsSummaryBySiteExport();

        Response.ContentType = "application/force-download";
        Response.AddHeader("content-disposition", "attachment; filename=ProvisionalBookingSummaryReportBySite.xls");
        Response.Write("<html xmlns:x=\"urn:schemas-microsoft-com:office:excel\">");
        Response.Write("<head>");
        Response.Write("<META http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">");
        Response.Write("<!--[if gte mso 9]><xml>");
        Response.Write("<x:ExcelWorkbook>");
        Response.Write("<x:ExcelWorksheets>");
        Response.Write("<x:ExcelWorksheet>");
        //Response.Write("<x:Name>" + ltOverViewType.Text + "</x:Name>");
        Response.Write("<x:Name>" + "BySite" + "</x:Name>");
        Response.Write("<x:WorksheetOptions>");
        Response.Write("<x:Print>");
        Response.Write("<x:ValidPrinterInfo/>");
        Response.Write("</x:Print>");
        Response.Write("</x:WorksheetOptions>");
        Response.Write("</x:ExcelWorksheet>");
        Response.Write("</x:ExcelWorksheets>");
        Response.Write("</x:ExcelWorkbook>");
        Response.Write("</xml>");
        Response.Write("<![endif]--> ");
        StringWriter tw = new StringWriter(); HtmlTextWriter hw = new HtmlTextWriter(tw); tblExport.RenderControl(hw);
        Response.Write(tw.ToString());
        Response.Write("</head>");
        Response.End();
    }
}

﻿<%@ Page Language="C#" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    AutoEventWireup="true" CodeFile="OverdueBooking.aspx.cs" Inherits="ModuleUI_Appointment_Reports_OverdueBooking" %>

<%@ Register Src="~/CommonUI/UserControls/MultiSelectSite.ascx" TagName="MultiSelectSite"
    TagPrefix="cc1" %>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/ucExportToExcel.ascx" TagName="ucExportToExcel"
    TagPrefix="cc2" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectStockPlanner.ascx" TagName="MultiSelectStockPlanner"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectVendor.ascx" TagName="MultiSelectVendor"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/ucVendorSelectionTemplate.ascx" TagName="VendorSelectTemplate"
    TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript" language="javascript">
        $(document).ready(function () {
            $("body").keypress(function (e) {
                if (e.which == 13 || e.keyCode == 13) {
                    $('#<%=UcButton1.ClientID %>').click();
                    return false;
                }
            });
        });
        function setValue1(target) {
            document.getElementById('<%=hdnJSFromDt.ClientID %>').value = target.value;
        }

        function setValue2(target) {
            document.getElementById('<%=hdnJSToDt.ClientID %>').value = target.value;
        }

    </script>
    <asp:ScriptManager ID="SM1" runat="server">
    </asp:ScriptManager>
    <asp:HiddenField ID="hdnJSFromDt" runat="server" />
    <asp:HiddenField ID="hdnJSToDt" runat="server" />
    <h2>
        <cc1:ucLabel ID="lblOverdueBooking" runat="server" Text="Overdue Booking"></cc1:ucLabel>
        <cc1:ucLabel ID="lblSearchResult" runat="server" Visible="false"></cc1:ucLabel>
    </h2>
    <cc1:ucMultiView ID="mvDiscrepancyList" runat="server" ActiveViewIndex="0">
        <cc1:ucView ID="vwSearchCreteria" runat="server">
            <div class="right-shadow">
                <div class="formbox">
                    <cc1:ucPanel ID="pnlSearchScreen" runat="server" CssClass="fieldset-form">
                        <table width="100%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                            <tr>
                                <td align="right">
                                    <cc1:VendorSelectTemplate runat="server" ID="ucVendorTemplateSelect" />
                                </td>
                            </tr>
                        </table>
                        <table width="100%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                            <tr>
                                <td>
                                    <table>
                                        <tr>
                                            <td style="font-weight: bold;">
                                                <cc1:ucLabel ID="lblStockPlanner" runat="server" Text="Stock Planner"></cc1:ucLabel>
                                            </td>
                                            <td style="font-weight: bold;">
                                                <cc1:ucLabel ID="Label17" runat="server">:</cc1:ucLabel>
                                            </td>
                                            <td>
                                                <cc1:MultiSelectStockPlanner runat="server" ID="msStockPlanner" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: bold;">
                                                <cc1:ucLabel ID="lblVendor" runat="server" Text="Vendor"></cc1:ucLabel>
                                            </td>
                                            <td style="font-weight: bold;">
                                                <cc1:ucLabel ID="UcLabel2" runat="server">:</cc1:ucLabel>
                                            </td>
                                            <td>
                                                <cc1:MultiSelectVendor runat="server" ID="msVendor" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: bold; width: 81px">
                                                <cc1:ucLabel ID="lblSite" runat="server" Text="Site"></cc1:ucLabel>
                                            </td>
                                            <td style="font-weight: bold;">
                                                <cc1:ucLabel ID="UcLabel4" runat="server">:</cc1:ucLabel>
                                            </td>
                                            <td>
                                                <cc1:MultiSelectSite runat="server" ID="msSite" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <%--<td>
                                        <table width="100%">
                                            <tr>--%>
                                            <td style="font-weight: bold; width: 81px">
                                                <cc1:ucLabel ID="lblDateFrom" runat="server" Text="Date From"></cc1:ucLabel>
                                            </td>
                                            <td style="font-weight: bold; width: 1%">
                                                <cc1:ucLabel ID="UcLabel6" runat="server">:</cc1:ucLabel>
                                            </td>
                                            <td>
                                                <table width="100%">
                                                    <tr>
                                                        <td align="left">
                                                            <table width="25%" cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td style="font-weight: bold; width: 6em;">
                                                                        <cc1:ucTextbox ID="txtFromDate" runat="server" ClientIDMode="Static" CssClass="date"
                                                                            onchange="setValue1(this)" ReadOnly="True" Width="70px" />
                                                                    </td>
                                                                    <td style="font-weight: bold; width: 4em; text-align: center">
                                                                        <cc1:ucLabel ID="lblTo" runat="server" Text="To"></cc1:ucLabel>
                                                                    </td>
                                                                    <td style="font-weight: bold; width: 6em;">
                                                                        <cc1:ucTextbox ID="txtToDate" runat="server" ClientIDMode="Static" CssClass="date"
                                                                            onchange="setValue2(this)" ReadOnly="True" Width="70px" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: bold; width: 10%">
                                                <cc1:ucLabel ID="lblDisplay" runat="server" Text="Display"></cc1:ucLabel>
                                            </td>
                                            <td style="font-weight: bold; width: 1%">
                                                <cc1:ucLabel ID="lblDisplayCollon" runat="server">:</cc1:ucLabel>
                                            </td>
                                            <td>
                                                <table width="100%">
                                                    <tr style="height: 3em;">
                                                        <td width="10%" style="font-weight: bold; text-align: left;">
                                                            <cc1:ucRadioButton Width="50" ID="rbAll" GroupName="Display" runat="server" Checked="true" Text=" All" />
                                                        </td>
                                                        <td width="10%" style="font-weight: bold; text-align: left;">
                                                            <cc1:ucRadioButton GroupName="Display" ID="rbReviewed" Width="120" runat="server"
                                                                Text="Reviewed" />
                                                        </td>
                                                        <td width="10%" style="font-weight: bold; text-align: left;">
                                                            <cc1:ucRadioButton GroupName="Display" ID="rbToBeReviewed" Width="120" runat="server"
                                                                Text="To be Reviewed" />
                                                        </td>
                                                        <td width="60%">
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="right">
                                    <div class="button-row">
                                        <cc1:ucButton ID="UcButton1" runat="server" Text="Generate Report" CssClass="button"
                                            OnClick="btnGenerateReport_Click" />
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </cc1:ucPanel>
                </div>
            </div>
        </cc1:ucView>
        <cc1:ucView ID="vwSearchListing" runat="server">
            <div class="button-row">
                <cc2:ucExportToExcel ID="ucExportToExcel1" runat="server" />
            </div>
            <%--<div style="width: 99%; overflow: auto" id="divPrint" class="fixedTable" onscroll="getScroll1(this);">--%>
                <table cellspacing="1" cellpadding="0" class="form-table">
                    <tr>
                        <td>
                        <%--<div style="width: 980px;overflow: scroll;">--%>
                            <cc1:ucGridView ID="gvVendor" runat="server" AutoGenerateColumns="false" CssClass="grid gvclass searchgrid-1"
                                CellPadding="0"  DataKeyNames="PKID" AllowSorting="true" AllowPaging="true"
                                OnSorting="SortGrid" PageSize="30" OnRowDataBound="gvVendor_RowDataBound" OnPreRender="gvVendor_PreRender"
                                OnPageIndexChanging="gvVendor_PageIndexChanging" >
                                <AlternatingRowStyle BackColor="#F7F6F3" ForeColor="#284775"></AlternatingRowStyle>
                                <EmptyDataTemplate>
                                    <div style="text-align: center">
                                        <cc1:ucLabel ID="lblNoData" runat="server" Text="No Records found."></cc1:ucLabel>
                                    </div>
                                </EmptyDataTemplate>
                                <Columns>
                                    <asp:TemplateField HeaderText="BookingRefNo" SortExpression="BookingRef">
                                        <HeaderStyle Width="200px" />
                                        <ItemStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <cc1:ucLiteral ID="ltHistory" runat="server" Text='<%#Eval("BookingRef") %>'></cc1:ucLiteral>
                                            <asp:HyperLink ID="hypHistory" runat="server" Text='<%#Eval("BookingRef") %>' NavigateUrl='<%# EncryptQuery("~/ModuleUI/Appointment/Receiving/APPRcv_BookingHistory.aspx?PN=OB&Scheduledate=" + Eval("DeliveryDate", "{0:dd/MM/yyyy}") + "&ID=" + Eval("PKID") + "-V-" + Eval("PKID") + "-" +  Eval("PKID")) %>'></asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Site" SortExpression="SiteName">
                                        <HeaderStyle Width="100px" />
                                        <ItemStyle HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel ID="LBLSiteName" runat="server" Text='<%# Eval("SiteName") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="DayTime">
                                        <HeaderStyle Width="90px" />
                                        <ItemStyle HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <asp:HiddenField ID="TimeWindowID" runat="server" Value='<%# Eval("TimeWindow.TimeWindowID") %>' />
                                            <cc1:ucLiteral ID="UcLiteral28" runat="server" Text='<%# Eval("DeliveryDate", "{0:dddd}")%>'></cc1:ucLiteral>&nbsp;
                                            <cc1:ucLiteral ID="UcLiteral27_2" runat="server" Text='<%#Eval("NonWindowFromToTime") %>'
                                                Visible="false"></cc1:ucLiteral>
                                            <cc1:ucLiteral ID="UcLiteral27_1" runat="server" Text='<%#Eval("WindowStartEndTime") %>'
                                                Visible="false"></cc1:ucLiteral>
                                            <cc1:ucLiteral ID="UcLiteral27" runat="server" Text='<%#Eval("ExpectedDeliveryTime") %>'></cc1:ucLiteral>
                                            
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="PurchaseOrders" SortExpression="PurchaseOrder.Purchase_order">
                                        <HeaderStyle Width="100px" />
                                        <ItemStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <cc1:ucLiteral ID="UcLiteralPO" runat="server" Text='<%#Eval("PurchaseOrder.Purchase_order") %>'></cc1:ucLiteral>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="VendorName" SortExpression="Vendor.VendorName">
                                        <HeaderStyle Width="220px" />
                                        <ItemStyle HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLiteral ID="UcLiteralVendor" runat="server" Text='<%#Eval("Vendor.VendorName") %>'></cc1:ucLiteral>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="DateBookingWasMade" SortExpression="BookingDat">
                                        <HeaderStyle Width="100px" />
                                        <ItemStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <asp:HiddenField ID="hdnExpectedDeliveryDate" runat="server" Value='<%# Eval("BookingDat") %>' />
                                            <cc1:ucLabel ID="lblExpectedDeliveryDate" runat="server" Text='<%# Eval("BookingDat", "{0:dd/MM/yyyy}") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="DateBookingWasMadeFor" SortExpression="DeliveryDate">
                                        <HeaderStyle Width="100px" />
                                        <ItemStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <asp:HiddenField ID="hdnBookingDate" runat="server" Value='<%# Eval("DeliveryDate") %>' />
                                            <cc1:ucLabel ID="lblDateBookingWasMade4" runat="server" Text='<%# Eval("DeliveryDate", "{0:dd/MM/yyyy}") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="CarrierName" SortExpression="Carrier.CarrierName">
                                        <HeaderStyle Width="180px" />
                                        <ItemStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <cc1:ucLiteral ID="UcLiteral3" runat="server" Text='<%#Eval("Carrier.CarrierName") %>'></cc1:ucLiteral>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="PODueDate" SortExpression="OriginalDueDate">
                                        <HeaderStyle Width="100px" />
                                        <ItemStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <cc1:ucLabel ID="lblPoDueDat" runat="server" Text='<%# Eval("OriginalDueDate", "{0:dd/MM/yyyy}") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Due Date Full" SortExpression="DueDateFull">
                                        <HeaderStyle Width="50px" />
                                        <ItemStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <cc1:ucLiteral ID="lblDueDateFull" runat="server" Text='<%#Eval("DueDateFull") %>'></cc1:ucLiteral>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Variance" SortExpression="Variance">
                                        <HeaderStyle Width="50px" />
                                        <ItemStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <cc1:ucLiteral ID="lblVarianc" runat="server" Text='<%#Eval("Variance") %>'></cc1:ucLiteral>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="BookingWasMadeBy" SortExpression="ProvBookedBy">
                                        <HeaderStyle Width="200px" />
                                        <ItemStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <cc1:ucLiteral ID="lblBookingWasMadeBys" runat="server" Text='<%#Eval("ProvBookedBy") %>'></cc1:ucLiteral>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="StockPlannerPointOfContactName" SortExpression="ContactName">
                                        <HeaderStyle Width="200px" />
                                        <ItemStyle HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLiteral ID="lblContactName" runat="server" Text='<%#Eval("ContactName") %>'></cc1:ucLiteral>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="StockPlannerPointOfContactNumber" SortExpression="ContactNumber">
                                        <HeaderStyle Width="200px" />
                                        <ItemStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <cc1:ucLiteral ID="lblContactNumber" runat="server" Text='<%#Eval("ContactNumber") %>'></cc1:ucLiteral>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Status" SortExpression="IsReviewed">
                                        <HeaderStyle Width="150px" />
                                        <ItemStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <cc1:ucLinkButton ID="lnkStats" runat="server" Text='<%# Eval("IsReviewed").ToString().ToUpper() .Equals ("TRUE") ? "Reviewed" : "Open" %>'
                                                OnClick="lnkStatus_Click" CommandArgument='<%# Eval("PKID") %>'></cc1:ucLinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </cc1:ucGridView>
                            <%--</div>--%>
                            <cc1:ucGridView ID="gvExport" runat="server" AutoGenerateColumns="false" Style="display: none;"
                                OnPreRender="gvVendor_PreRender">
                                <Columns>
                                    <asp:TemplateField HeaderText="BookingRefNo">
                                        <HeaderStyle Width="170px" />
                                        <ItemStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <cc1:ucLiteral ID="ltHistory" runat="server" Text='<%#Eval("BookingRef") %>'></cc1:ucLiteral>
                                            <%--<cc1:ucLabel ID="lblHistry" runat="server" Text='<%#Eval("BookingRef") %>'></cc1:ucLabel>--%>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Site">
                                        <HeaderStyle Width="120px" />
                                        <ItemStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <cc1:ucLabel ID="LBLSiteName" runat="server" Text='<%# Eval("SiteName") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="DayTime">
                                        <HeaderStyle Width="150px" />
                                        <ItemStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <asp:HiddenField ID="TimeWindowID" runat="server" Value='<%# Eval("TimeWindow.TimeWindowID") %>' />
                                            <cc1:ucLiteral ID="UcLiteral28" runat="server" Text='<%# Eval("DeliveryDate", "{0:dddd}")%>'></cc1:ucLiteral>&nbsp;
                                            <cc1:ucLiteral ID="UcLiteral27_2" runat="server" Text='<%#Eval("NonWindowFromToTime") %>'
                                                Visible="false"></cc1:ucLiteral>
                                            <cc1:ucLiteral ID="UcLiteral27_1" runat="server" Text='<%#Eval("WindowStartEndTime") %>'
                                                Visible="false"></cc1:ucLiteral>
                                            <cc1:ucLiteral ID="UcLiteral27" runat="server" Text='<%#Eval("ExpectedDeliveryTime") %>'></cc1:ucLiteral>
                                            
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="PurchaseOrders">
                                        <HeaderStyle Width="200px" />
                                        <ItemStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <cc1:ucLiteral ID="UcLiteralPO" runat="server" Text='<%#Eval("PurchaseOrder.Purchase_order") %>'></cc1:ucLiteral>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="VendorName">
                                        <HeaderStyle Width="200px" />
                                        <ItemStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <cc1:ucLiteral ID="UcLiteralVendor" runat="server" Text='<%#Eval("Vendor.VendorName") %>'></cc1:ucLiteral>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="DateBookingWasMade">
                                        <HeaderStyle Width="200px" />
                                        <ItemStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <asp:HiddenField ID="hdnExpectedDeliveryDate" runat="server" Value='<%# Eval("BookingDat") %>' />
                                            <cc1:ucLabel ID="lblExpectedDeliveryDate" runat="server" Text='<%# Eval("BookingDat", "{0:dd/MM/yyyy}") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="DateBookingWasMadeFor">
                                        <HeaderStyle Width="200px" />
                                        <ItemStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <asp:HiddenField ID="hdnBookingDate" runat="server" Value='<%# Eval("DeliveryDate") %>' />
                                            <cc1:ucLabel ID="lblDateBookingWasMade4" runat="server" Text='<%# Eval("DeliveryDate", "{0:dd/MM/yyyy}") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="CarrierName">
                                        <HeaderStyle Width="180px" />
                                        <ItemStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <cc1:ucLiteral ID="UcLiteral3" runat="server" Text='<%#Eval("Carrier.CarrierName") %>'></cc1:ucLiteral>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="PODueDate">
                                        <HeaderStyle Width="200px" />
                                        <ItemStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <cc1:ucLabel ID="lblPoDueDat" runat="server" Text='<%# Eval("OriginalDueDate", "{0:dd/MM/yyyy}") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                     <asp:TemplateField HeaderText="Due Date Full" SortExpression="DueDateFull">
                                        <HeaderStyle Width="50px" />
                                        <ItemStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <cc1:ucLiteral ID="lblDueDateFull" runat="server" Text='<%#Eval("DueDateFull") %>'></cc1:ucLiteral>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Variance">
                                        <HeaderStyle Width="200px" />
                                        <ItemStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <cc1:ucLiteral ID="lblVarianc" runat="server" Text='<%#Eval("Variance") %>'></cc1:ucLiteral>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="BookingWasMadeBy">
                                        <HeaderStyle Width="200px" />
                                        <ItemStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <cc1:ucLiteral ID="lblBookingWasMadeBys" runat="server" Text='<%#Eval("ProvBookedBy") %>'></cc1:ucLiteral>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="StockPlannerPointOfContactName">
                                        <HeaderStyle Width="200px" />
                                        <ItemStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <cc1:ucLiteral ID="lblContactName" runat="server" Text='<%#Eval("ContactName") %>'></cc1:ucLiteral>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="StockPlannerPointOfContactNumber">
                                        <HeaderStyle Width="200px" />
                                        <ItemStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <cc1:ucLiteral ID="lblContactNumber" runat="server" Text='<%#Eval("ContactNumber") %>'></cc1:ucLiteral>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Status">
                                        <HeaderStyle Width="200px" />
                                        <ItemStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <cc1:ucLabel ID="lblsatu" runat="server" Text='<%# Eval("IsReviewed").ToString().ToUpper() .Equals ("TRUE") ? "Reviewed" : "Open" %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </cc1:ucGridView>
                        </td>
                    </tr>
                </table>
           <%-- </div>--%>
            <div class="button-row">
                <cc1:ucButton ID="btnBackSearch" runat="server" Text="Back To Search Criteria" CssClass="button"
                    OnClick="btnBack_Click" />
            </div>
        </cc1:ucView>
    </cc1:ucMultiView>
</asp:Content>

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.AdminFunctions;
using BusinessLogicLayer.ModuleBAL.AdminFunctions;
using BusinessEntities.ModuleBE.Discrepancy.LogDiscrepancy;
using BusinessLogicLayer.ModuleBAL.Discrepancy.LogDiscrepancy;
using System.Data;
using System.Configuration;
using Utilities;
using BusinessLogicLayer.ModuleBAL.Appointment.Booking;
using BusinessEntities.ModuleBE.Languages.Languages;
using System.Text;
using BusinessEntities.ModuleBE.Upload;
using BusinessLogicLayer.ModuleBAL.Upload;

public partial class ModuleUI_Appointment_Reports_SentPO_ReSendCommunication : CommonPage
{
    #region Declarations ...

    string sFromAddress = Convert.ToString(ConfigurationManager.AppSettings["fromAddress"]);
    sendCommunicationCommon oSendCommunicationCommon = new sendCommunicationCommon();
    StringBuilder sSendMailLink = new StringBuilder();

    #endregion

    #region Events ...

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            btnReSendCommunication.Visible = false;
            lblEmail.Visible = false;
            txtEmail.Visible = false;
            InitLanguage();

            if (GetQueryStringValue("PO") != null && GetQueryStringValue("SiteID") != null && GetQueryStringValue("VendorID") != null)
            {                
                var PO = GetQueryStringValue("PO");
                var SiteID = GetQueryStringValue("SiteID");
                var VendorID = GetQueryStringValue("VendorID");
                var SentDate = GetQueryStringValue("SentDate");
                var CommunicationType = GetQueryStringValue("CommType");
                var languageId = Convert.ToInt32(GetQueryStringValue("LangId"));

                if (CommunicationType == "Resent-New PO" || CommunicationType == "Resent-Manually Sent PO Mail")
                {
                    btnReSendCommunication.Visible = false;
                    lblLanguage.Visible = false;
                    lblColons.Visible = false;
                    drpLanguageId.Visible = false;
                }
                else
                {
                    btnReSendCommunication.Visible = true;
                    lblLanguage.Visible = true;
                    lblColons.Visible = true;
                    drpLanguageId.Visible = true;
                }

                var objPurchaseOrderDetailBAL = new UP_PurchaseOrderDetailBAL();
                var objUp_PurchaseOrderDetailBE = new Up_PurchaseOrderDetailBE();
                objUp_PurchaseOrderDetailBE.Action = "GetSentPOEmailDetails";
                objUp_PurchaseOrderDetailBE.Purchase_order = Convert.ToString(PO);
                objUp_PurchaseOrderDetailBE.SiteId = Convert.ToInt32(Convert.ToString(SiteID));
                objUp_PurchaseOrderDetailBE.VendorId = Convert.ToInt32(Convert.ToString(VendorID));
                objUp_PurchaseOrderDetailBE.SentDate = Common.GetMM_DD_YYYY(SentDate);
                objUp_PurchaseOrderDetailBE.CommunicationType = Convert.ToString(CommunicationType);
                var lstPurchaseOrderDetailBE = objPurchaseOrderDetailBAL.GetSentPOEmailDetailsBAL(objUp_PurchaseOrderDetailBE);

                ViewState["lstPurchaseOrderDetailBE"] = lstPurchaseOrderDetailBE;

                var purchaseBE = lstPurchaseOrderDetailBE.Find(x => x.LanguageID == languageId);
                if (purchaseBE != null)
                {
                    //var result = lstPurchaseOrderDetailBE.FirstOrDefault(x => x.IsMailSentInLanguage == true);
                    if (drpLanguageId.Items.FindByValue(Convert.ToString(languageId)) != null)
                        drpLanguageId.Items.FindByValue(Convert.ToString(languageId)).Selected = true;

                    divReSendCommunication.InnerHtml = purchaseBE.Body.Replace("{logoInnerPath}", oSendCommunicationCommon.getAbsolutePath());

                    ViewState["EmailSubject"] = purchaseBE.Subject;
                    hdSubject.Value = purchaseBE.Subject;
                    txtEmail.Text = purchaseBE.SentTo;
                    //btnReSendCommunication.Visible = true;
                    lblEmail.Visible = true;
                    txtEmail.Visible = true;
                }
                else { this.ShowErrorMessage(); }

                //if (lstPurchaseOrderDetailBE != null && lstPurchaseOrderDetailBE.Count > 0)
                //{
                //    var result = lstPurchaseOrderDetailBE.FirstOrDefault(x => x.IsMailSentInLanguage == true);
                //    if (drpLanguageId.Items.FindByValue(Convert.ToString(languageId)) != null)
                //        drpLanguageId.Items.FindByValue(Convert.ToString(languageId)).Selected = true;

                //    divReSendCommunication.InnerHtml = result.Body.Replace("{logoInnerPath}", oSendCommunicationCommon.getAbsolutePath());

                //    ViewState["EmailSubject"] = result.Subject;
                //    hdSubject.Value = result.Subject;
                //    txtEmail.Text = result.SentTo;
                //    //btnReSendCommunication.Visible = true;
                //    lblEmail.Visible = true;
                //    txtEmail.Visible = true;
                //}
                //else { this.ShowErrorMessage(); }
            }
            else
            {
                if (GetQueryStringValue("CommunicationLevel") != null)
                    ViewState["CommunicationLevel"] = GetQueryStringValue("CommunicationLevel");
                else if (Request.QueryString["CommunicationLevel"] != null)
                    ViewState["CommunicationLevel"] = GetQueryStringValue(Request.QueryString["CommunicationLevel"]);

                int iCommId = 0;
                if (GetQueryStringValue("communicationid") != null)
                {
                    iCommId = Convert.ToInt32(GetQueryStringValue("communicationid"));
                }
                else if (Request.QueryString["communicationid"] != null)
                {
                    iCommId = Convert.ToInt32(Request.QueryString["communicationid"]);
                }

                if (iCommId != 0)
                {
                    DataSet dsCommunication = getCommunicationDetails(iCommId);

                    if (dsCommunication != null && dsCommunication.Tables.Count > 0 && dsCommunication.Tables[0].Rows.Count > 0)
                    {
                        try
                        {
                            if (drpLanguageId.Items.FindByValue(dsCommunication.Tables[0].Rows[0]["LanguageID"].ToString()) != null)
                                drpLanguageId.Items.FindByValue(dsCommunication.Tables[0].Rows[0]["LanguageID"].ToString()).Selected = true;
                        }
                        catch
                        {
                            //ShowErrorMessage();
                        }
                        divReSendCommunication.InnerHtml = Convert.ToString(dsCommunication.Tables[0].Rows[0]["body"]);
                        hdSubject.Value = Convert.ToString(dsCommunication.Tables[0].Rows[0]["Subject"]);
                        txtEmail.Text = distictEmails(Convert.ToString(dsCommunication.Tables[0].Rows[0]["SentTo"]));
                        //btnReSendCommunication.Visible = true;
                        lblEmail.Visible = true;
                        txtEmail.Visible = true;
                        Session["dsCommunication"] = dsCommunication;
                    }
                    else { ShowErrorMessage(); }
                }
                else { ShowErrorMessage(); }
            }
        }
    }

    protected void btnReSendCommunication_Click(object sender, EventArgs e)
    {
        var PO = GetQueryStringValue("PO");
        var SiteID = GetQueryStringValue("SiteID");
        var VendorID = GetQueryStringValue("VendorID");
        var LanguageId = Convert.ToInt32(drpLanguageId.SelectedValue);

        UP_PurchaseOrderDetailBAL objPurchaseOrderDetailBAL = new UP_PurchaseOrderDetailBAL();
        Up_PurchaseOrderDetailBE objUp_PurchaseOrderDetailBE = new Up_PurchaseOrderDetailBE();

        objUp_PurchaseOrderDetailBE.Action = "InsertReSentPOEmailData";
        objUp_PurchaseOrderDetailBE.Purchase_order = Convert.ToString(PO);
        objUp_PurchaseOrderDetailBE.SiteId = Convert.ToInt32(Convert.ToString(SiteID));
        objUp_PurchaseOrderDetailBE.VendorId = Convert.ToInt32(Convert.ToString(VendorID));
        objUp_PurchaseOrderDetailBE.LanguageID = Convert.ToInt32(LanguageId);
        objUp_PurchaseOrderDetailBE.SentTo = txtEmail.Text;
        objUp_PurchaseOrderDetailBE.FirstSentToEmail = txtEmail.Text.Split(',')[0];

        var reSentCommId = objPurchaseOrderDetailBAL.SaveImportNewSentPOEmailDataBAL(objUp_PurchaseOrderDetailBE);
        if (reSentCommId > 0)
        {
            var emailToAddress = txtEmail.Text;
            var emailFromAddress = Convert.ToString(ConfigurationManager.AppSettings["fromAddress"]);
            var emailToSubject = hdSubject.Value;
            var emailBody = divReSendCommunication.InnerHtml;
            Utilities.clsEmail oclsEmail = new Utilities.clsEmail();
            oclsEmail.sendMail(emailToAddress, emailBody, emailToSubject, emailFromAddress, true);

            Response.Write("<script language='javascript'>window.open('', '_self', '');window.close();</script>");
            //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "MailToReSend", "window.opener.location.reload();window.close();", true);
        }
    }

    protected void drpLanguageId_SelectedIndexChanged(object sender, EventArgs e)
    {
        var LanguageId = Convert.ToInt32(drpLanguageId.SelectedValue);
        var lstPurchaseOrderDetailBE = (List<Up_PurchaseOrderDetailBE>)ViewState["lstPurchaseOrderDetailBE"];
        if (lstPurchaseOrderDetailBE != null && lstPurchaseOrderDetailBE.Count > 0)
        {
            var result = lstPurchaseOrderDetailBE.FirstOrDefault(x => x.LanguageID == LanguageId);
            if (drpLanguageId.Items.FindByValue(Convert.ToString(result.LanguageID)) != null)
                drpLanguageId.Items.FindByValue(Convert.ToString(result.LanguageID)).Selected = true;

            divReSendCommunication.InnerHtml = result.Body.Replace("{logoInnerPath}", oSendCommunicationCommon.getAbsolutePath());

            ViewState["EmailSubject"] = result.Subject;
            hdSubject.Value = result.Subject;
            txtEmail.Text = result.SentTo;
            btnReSendCommunication.Visible = true;
            lblEmail.Visible = true;
            txtEmail.Visible = true;
        }

    }

    #endregion

    #region Methods ...

    public string distictEmails(string Emails)
    {
        string[] arrEMails = Emails.Split(new char[] { ',' });

        List<string> lstEmails = new List<string>();
        foreach (string email in arrEMails)
        {
            if (!string.IsNullOrEmpty(email) && !string.IsNullOrWhiteSpace(email) && !lstEmails.Contains(email.Trim()))
                lstEmails.Add(email.Trim());
        }
        return string.Join(",", lstEmails.ToArray());
    }

    private void ShowErrorMessage()
    {
        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "showNoMailToReSend", "alert('" + WebUtilities.WebCommon.getGlobalResourceValue("NoMailToResend") + "');", true);
    }

    public DataSet getCommunicationDetails(int iDiscrepancyCommunicaitonID)
    {
        DiscrepancyMailBE oDiscrepancyMailBE = new DiscrepancyMailBE();
        oDiscrepancyMailBE.Action = "getDiscrepancyCommunciationDetail"; // returns one record by communicationid
        oDiscrepancyMailBE.discrepancyCommunicationID = iDiscrepancyCommunicaitonID;
        DiscrepancyBAL oNewDiscrepancyBAL = new DiscrepancyBAL();
        DataSet ds = oNewDiscrepancyBAL.getDiscrepancyCommunicationBAL(oDiscrepancyMailBE);
        oNewDiscrepancyBAL = null;
        return ds;
    }

    public DataSet getCommunicationDetails(int iDiscrepancyCommunicaitonID, int LanguageID, string CommunicationLevel, string SentTo)
    {
        DiscrepancyMailBE oDiscrepancyMailBE = new DiscrepancyMailBE();
        oDiscrepancyMailBE.Action = "getDiscrepancyCommunciationDetailWithLanguageID";
        oDiscrepancyMailBE.discrepancyCommunicationID = iDiscrepancyCommunicaitonID;
        oDiscrepancyMailBE.languageID = LanguageID;
        oDiscrepancyMailBE.communicationLevel = CommunicationLevel;
        oDiscrepancyMailBE.sentTo = SentTo;

        DiscrepancyBAL oNewDiscrepancyBAL = new DiscrepancyBAL();
        DataSet ds = oNewDiscrepancyBAL.getDiscrepancyCommunicationItemBAL(oDiscrepancyMailBE);
        oNewDiscrepancyBAL = null;
        return ds;
    }

    public void insertHtml(int DiscrepancyLogID, string EmailLink)
    {
        DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
        DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();
        WorkflowHTML oWorkflowHTML = new WorkflowHTML();

        oDiscrepancyBE.GINHTML = oWorkflowHTML.function3("white", null, "", "");
        oDiscrepancyBE.GINActionRequired = false;

        oDiscrepancyBE.INVHTML = oWorkflowHTML.function3("white", null, "", "");
        oDiscrepancyBE.INVActionRequired = false;

        oDiscrepancyBE.APHTML = oWorkflowHTML.function3("white", null, "", "");
        oDiscrepancyBE.APActionRequired = false;

        oDiscrepancyBE.VENHTML = oWorkflowHTML.function4("Blue", null, null, "", null, "Letter resent by " + Session["UserName"].ToString() + " to " + EmailLink + " on " + System.DateTime.Now.ToString("dd/MM/yyyy"), "", "", "", null, "");
        oDiscrepancyBE.VenActionRequired = false;

        oDiscrepancyBE.Action = "InsertHTML";
        oDiscrepancyBE.DiscrepancyLogID = DiscrepancyLogID;
        oDiscrepancyBE.LoggedDateTime = DateTime.Now;
        oDiscrepancyBE.LevelNumber = 1;
        int? iResult1 = oDiscrepancyBAL.addWorkFlowHTMLsBAL(oDiscrepancyBE);
        oDiscrepancyBAL = null;
    }

    private void InitLanguage()
    {
        APPBOK_CommunicationBAL oAPPBOK_CommunicationBAL = new APPBOK_CommunicationBAL();
        List<MAS_LanguageBE> oLanguages = oAPPBOK_CommunicationBAL.GetLanguages();
        drpLanguageId.DataSource = oLanguages;
        drpLanguageId.DataValueField = "LanguageID";
        drpLanguageId.DataTextField = "Language";
        drpLanguageId.DataBind();
    }

    #endregion
}
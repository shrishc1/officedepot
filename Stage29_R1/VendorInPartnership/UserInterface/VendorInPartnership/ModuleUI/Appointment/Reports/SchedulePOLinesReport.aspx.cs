﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.Appointment.Booking;
using BusinessEntities.ModuleBE.Security;
using BusinessLogicLayer.ModuleBAL.Appointment.Booking;
using BusinessLogicLayer.ModuleBAL.Security;
using System.Data;
using System.Data.SqlClient;
using WebUtilities;
using Utilities;
using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using BusinessLogicLayer.ModuleBAL.Appointment.SiteSettings;

public partial class SchedulePOLinesReport : CommonPage
{
    protected void Page_Init(object sender, EventArgs e)
    {
        ddlSite.CurrentPage = this;
        ddlSite.IsAllRequired = true;
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            DateTime scheduleDate = Utilities.Common.TextToDateFormat(GetQueryStringValue("scheduledate"));
            string SiteId = GetQueryStringValue("siteid");
            txtDate.innerControltxtDate.Value = scheduleDate.ToString("dd/MM/yyyy");
            ddlSite.innerControlddlSite.SelectedValue = SiteId;
            APPBOK_BookingBE oAPPBOK_BookingBE = new APPBOK_BookingBE();
            oAPPBOK_BookingBE.FixedSlot = new BusinessEntities.ModuleBE.Appointment.SiteSettings.MASSIT_FixedSlotBE();
            oAPPBOK_BookingBE.ScheduleDate = Convert.ToDateTime(scheduleDate);
            oAPPBOK_BookingBE.FixedSlot.SiteID = Convert.ToInt32(SiteId);
            oAPPBOK_BookingBE.FixedSlot.FixedSlotID = 0;
            BingGrid(oAPPBOK_BookingBE);
        }
        btnExportToExcel1.CurrentPage = this;
        btnExportToExcel1.IsHideHidden = true;
        btnExportToExcel1.GridViewControl = UcGridView1;
        btnExportToExcel1.FileName = "SchedulePOLineReport";
    }
    protected void BingGrid(APPBOK_BookingBE oAPPBOK_BookingBE)
    {
        APPBOK_BookingBAL oAPPBOK_BookingBAL = new APPBOK_BookingBAL();
        Data = oAPPBOK_BookingBAL.GetSchedulePOLineReport(oAPPBOK_BookingBE);

        DataColumn dcIndex = new DataColumn
        {
            ColumnName = "AutoID",
            DataType = System.Type.GetType("System.Int32")
        };

        Data.Columns.Add(dcIndex);

        DataView lstBookings = new DataView(Data.DefaultView.Table);

        int iAutoId = 0;
        List<MASSIT_WeekSetupBE> unused = new List<MASSIT_WeekSetupBE>();

        string Role = Session["Role"].ToString().Trim().ToLower();

        if (Role != "vendor" && Role != "carrier")
        {
            DateTime dtSelectedDate = txtDate.GetDate;
            string WeekDay = dtSelectedDate.ToString("dddd");

            MASSIT_WeekSetupBE oMASSIT_WeekSetupBE = new MASSIT_WeekSetupBE();
            MASSIT_WeekSetupBAL oMASSIT_WeekSetupBAL = new MASSIT_WeekSetupBAL();
            oMASSIT_WeekSetupBE.Action = "GetSpecificWeekShowAll";
            oMASSIT_WeekSetupBE.SiteID = Convert.ToInt32(GetQueryStringValue("siteid"));
            oMASSIT_WeekSetupBE.EndWeekday = WeekDay;
            oMASSIT_WeekSetupBE.ScheduleDate = dtSelectedDate;
            List<MASSIT_WeekSetupBE> lstWeekSetup = oMASSIT_WeekSetupBAL.GetWeekSetupDetailsBAL(oMASSIT_WeekSetupBE);
           
            if (lstWeekSetup == null || lstWeekSetup.Count <= 0 || lstWeekSetup[0].StartTime == null)
            {
                if (lstBookings != null && lstBookings.Count > 0)
                {
                    lstBookings.RowFilter = "WeekDay <> '8'";
                    lstBookings = lstBookings.ToTable().DefaultView;
                }
            }

            if (lstWeekSetup.Count > 0 && lstWeekSetup[0].StartTime != null)
            {
                if (lstWeekSetup[0].StartWeekday != lstWeekSetup[0].EndWeekday)
                {                    
                    DateTime selectedDate = txtDate.GetDate; 

                    if (lstBookings != null && lstBookings.Count > 0)
                    {
                        lstBookings.RowFilter = "Weekday <> '10'";
                        lstBookings = lstBookings.ToTable().DefaultView;
                    }

                    if (lstBookings != null && lstBookings.Count > 0)
                    {                        
                        lstBookings.Sort = "EndOrderBY asc";
                    }

                    foreach (DataRowView rowView in lstBookings)
                    {
                        DataRow dr = rowView.Row;

                        if (lstWeekSetup.Count > 0 && lstWeekSetup[0].StartTime != null)
                        {
                            if (lstWeekSetup[0].StartWeekday == lstWeekSetup[0].EndWeekday)
                            {
                                dr["AutoID"] = dr["EndOrderBy"].ToString();
                            }
                            else
                            {
                                iAutoId++;
                                dr["AutoID"] = iAutoId.ToString();
                            }
                        }
                    }                   
                }
            }
        }
        ViewState["lstSites"] = lstBookings.ToTable();
        ucGridSchedulePOLineReport.DataSource = lstBookings;
        ucGridSchedulePOLineReport.DataBind();
        UcGridView1.DataSource = Data;
        UcGridView1.DataBind();
    }

    protected void UcButton1_Click(object sender, EventArgs e)
    {
        APPBOK_BookingBE oAPPBOK_BookingBE = new APPBOK_BookingBE();
        oAPPBOK_BookingBE.FixedSlot = new BusinessEntities.ModuleBE.Appointment.SiteSettings.MASSIT_FixedSlotBE();
        oAPPBOK_BookingBE.ScheduleDate = txtDate.GetDate;
        oAPPBOK_BookingBE.FixedSlot.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedValue.ToString());
        oAPPBOK_BookingBE.FixedSlot.FixedSlotID = Convert.ToInt32(ddlLine.SelectedValue.ToString());
        BingGrid(oAPPBOK_BookingBE);
    }
    protected void UcGridSchedulePOLineReport_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        ucGridSchedulePOLineReport.PageIndex = e.NewPageIndex;
        APPBOK_BookingBE oAPPBOK_BookingBE = new APPBOK_BookingBE();
        oAPPBOK_BookingBE.FixedSlot = new BusinessEntities.ModuleBE.Appointment.SiteSettings.MASSIT_FixedSlotBE();
        oAPPBOK_BookingBE.ScheduleDate = txtDate.GetDate;
        oAPPBOK_BookingBE.FixedSlot.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedValue.ToString());
        oAPPBOK_BookingBE.FixedSlot.FixedSlotID = Convert.ToInt32(ddlLine.SelectedValue.ToString());
        BingGrid(oAPPBOK_BookingBE);
    }
    bool IsGridSort = false;
    protected void GridView_Sorting(Object sender, GridViewSortEventArgs e)
    {
        try
        {
            IsGridSort = true;
            string sortExpression = e.SortExpression;
            GridViewSortExp = sortExpression;
            if (GridViewSortDirection == SortDirection.Ascending)
            {
                GridViewSortDirection = SortDirection.Descending;
                SortGridView(sortExpression, " ASC");
            }
            else
            {
                GridViewSortDirection = SortDirection.Ascending;
                SortGridView(sortExpression, " DESC");
            }
        }
        catch (Exception ex)
        {
            LogUtility.SaveErrorLogEntry(ex);
        }
    }
    public string GridViewSortExp
    {
        get
        {
            return Convert.ToString(ViewState["GridViewSortExp"]);
        }
        set { ViewState["GridViewSortExp"] = value; }
    }
    public DataTable Data
    {
        get
        {
            return ViewState["Data"] as DataTable;
        }
        set
        {
            ViewState["Data"] = value;
        }
    }
    public SortDirection GridViewSortDirection
    {
        get
        {
            if (ViewState["sortDirection"] == null)
                ViewState["sortDirection"] = SortDirection.Ascending;
            return (SortDirection)ViewState["sortDirection"];
        }
        set
        {
            ViewState["sortDirection"] = value;
        }
    }
    private void SortGridView(string sortExpression, string direction)
    {
        DataTable dtSortingTbl;
        DataView dtView;
        try
        {
            if (ViewState["lstSites"] != null)
            {
                dtSortingTbl = (DataTable)ViewState["lstSites"];
                dtView = dtSortingTbl.DefaultView;

                if (sortExpression != "Weekday")
                {
                    int sortExpIndx = sortExpression.IndexOf(",");
                    dtView.Sort = sortExpIndx == -1
                        ? sortExpression + direction
                        : sortExpression.Substring(0, sortExpIndx) + direction + sortExpression.Substring(sortExpIndx);
                }
                else
                {
                    dtView.Sort = direction.Trim() == "ASC" ? "Weekday desc,OrderBY,DoorNumber" : "Weekday asc,OrderBY,DoorNumber";
                }

                dtSortingTbl = sortExpression != "AutoID" && IsGridSort ? GetSortByBooking(dtView.ToTable()) : dtView.ToTable();

                string Role = Session["Role"].ToString().Trim().ToLower();
                GridView gridToParse = new GridView();
                gridToParse = ucGridSchedulePOLineReport;

                ViewState["lstSites"] = dtSortingTbl;
                gridToParse.DataSource = dtSortingTbl;
                gridToParse.DataBind();
            }
        }
        catch (Exception ex)
        {
            LogUtility.SaveErrorLogEntry(ex);
        }
    }

    private DataTable dtSortingTblByBooking = null;
    private DataTable GetSortByBooking(DataTable dtSortingTbl)
    {
        if (dtSortingTbl != null && dtSortingTbl.Rows.Count > 0)
        {
            if (dtSortingTblByBooking == null)
                dtSortingTblByBooking = dtSortingTbl.Clone();

            string bookingRef = dtSortingTbl.Rows[0]["BookingReferenceID"].ToString();

            DataRow[] rows = dtSortingTbl.Select("BookingReferenceID = '" + bookingRef + "'");

            foreach (DataRow r in rows)
            {
                dtSortingTblByBooking.ImportRow(r);
                r.Delete();
            }

            dtSortingTblByBooking.AcceptChanges();
            GetSortByBooking(dtSortingTbl);
        }

        return dtSortingTblByBooking;
    }
}
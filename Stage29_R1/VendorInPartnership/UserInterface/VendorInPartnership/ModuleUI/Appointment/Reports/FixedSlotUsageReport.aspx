﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    MaintainScrollPositionOnPostback="true" AutoEventWireup="true" CodeFile="FixedSlotUsageReport.aspx.cs"
    Inherits="FixedSlotUsageReport" %>

<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/ucSite.ascx" TagName="ucSite" TagPrefix="cc2" %>
<%@ Register Src="~/CommonUI/UserControls/ucExportToExcel.ascx" TagName="ucExportToExcel"
    TagPrefix="cc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h2>
        <cc1:ucLabel ID="lblFixedSlotUsageReport" runat="server"></cc1:ucLabel>
    </h2>
    <div class="right-shadow">
        <div class="formbox">
            <table width="100%" cellspacing="1" cellpadding="0" border="0" align="center" class="top-settings">
                <tr>
                    <td style="font-weight: bold; width: 3%">
                        <cc1:ucLabel ID="lblSitePrefixName" runat="server"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold; width: 1%">
                        :
                    </td>
                    <td style="font-weight: bold; width: 20%">
                        <cc2:ucSite ID="ddlSite" runat="server" />
                    </td>
                    <td style="font-weight: bold; width: 4%">
                        <cc1:ucLabel ID="lblPeriod" runat="server"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold; width: 1%">
                        :
                    </td>
                    <td style="font-weight: bold; width: 20%">
                        <cc1:ucDropdownList ID="ddlPeriod" runat="server" Width="130px">
                            <asp:ListItem Text="4 Week" Value="4"></asp:ListItem>
                            <asp:ListItem Text="8 Week" Value="8"></asp:ListItem>
                        </cc1:ucDropdownList>
                    </td>
                    <td style="font-weight: bold; width: 8%">
                        <cc1:ucLabel ID="lblReportType" runat="server"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold; width: 1%">
                        :
                    </td>
                    <td style="font-weight: bold; width: 20%">
                        <cc1:ucDropdownList ID="ddlReportType" runat="server" Width="130px">
                            <asp:ListItem Text="Per Slot" Value="ps"></asp:ListItem>
                            <asp:ListItem Text="Per Vendor" Value="pv"></asp:ListItem>
                        </cc1:ucDropdownList>
                    </td>
                    <td align="center" style="font-weight: bold; width: 22%">
                        <cc1:ucButton ID="btnGo" runat="server" CssClass="button" OnClick="btnGenerateReport_Click" />
                        &nbsp; &nbsp; &nbsp;
                        <cc2:ucExportToExcel ID="ucExportToExcel1" runat="server" />
                    </td>
                </tr>
            </table><br />
            <table cellspacing="1" cellpadding="0" id="tblps" runat="server">
                <tr>
                    <td>
                        <cc1:ucGridView ID="ucGridView1" runat="server" AutoGenerateColumns="false" CssClass="grid gvclass"
                            CellPadding="0" Width="960px" OnSorting="SortGrid" AllowSorting="true">
                            <AlternatingRowStyle BackColor="White" ForeColor="#284775"></AlternatingRowStyle>
                            <Columns>
                                <asp:TemplateField HeaderText="Vendor/Carrier" SortExpression="VendorName">
                                    <HeaderStyle Width="150px" HorizontalAlign="Left" />
                                    <ItemStyle HorizontalAlign="Left" Width="150px" />
                                    <ItemTemplate>
                                        <asp:HyperLink ID="hypLink" runat="server" Target="_blank" Text='<%#Eval("VendorName") %>' NavigateUrl='<%# EncryptQuery("FixedSlotUsageDetails.aspx?Period=" + ddlPeriod.SelectedItem.Value + "&FixedslotID="+ Eval("FixedSlotID")) %>'></asp:HyperLink>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField HeaderText="Allocation Type" DataField="AllocationType" SortExpression="AllocationType">
                                    <HeaderStyle Width="40px" HorizontalAlign="Left" />
                                    <ItemStyle HorizontalAlign="Left" Width="40px" />
                                </asp:BoundField>
                                <asp:BoundField HeaderText="SlotTime" DataField="SlotTime" SortExpression="SlotTime">
                                    <HeaderStyle Width="40px" HorizontalAlign="Left" />
                                    <ItemStyle HorizontalAlign="Left" Width="40px" />
                                </asp:BoundField>
                                <asp:BoundField HeaderText="DoorNumber" DataField="DoorNumber" SortExpression="DoorNumber">
                                    <HeaderStyle Width="30px" HorizontalAlign="Left" />
                                    <ItemStyle HorizontalAlign="Left" Width="30px" />
                                </asp:BoundField>
                                <asp:BoundField HeaderText="Rsvd Plt" DataField="MaximumPallets" SortExpression="MaximumPallets">
                                    <HeaderStyle Width="30px" HorizontalAlign="Left" />
                                    <ItemStyle HorizontalAlign="Left" Width="30px" />
                                </asp:BoundField>
                                <asp:BoundField HeaderText="Rsvd Ctn" DataField="MaximumCatrons" SortExpression="MaximumCatrons">
                                    <HeaderStyle Width="30px" HorizontalAlign="Left" />
                                    <ItemStyle HorizontalAlign="Left" Width="30px" />
                                </asp:BoundField>
                                <asp:BoundField HeaderText="Rsvd Ln" DataField="MaximumLines" SortExpression="MaximumLines">
                                    <HeaderStyle Width="30px" HorizontalAlign="Left" />
                                    <ItemStyle HorizontalAlign="Left" Width="30px" />
                                </asp:BoundField>
                                <asp:BoundField HeaderText="MonShort" DataField="Monday" SortExpression="Monday">
                                    <HeaderStyle Width="20px" HorizontalAlign="Left" />
                                    <ItemStyle HorizontalAlign="Left" Width="20px" />
                                </asp:BoundField>
                                <asp:BoundField HeaderText="TueShort" DataField="Tuesday" SortExpression="Tuesday">
                                    <HeaderStyle Width="20px" HorizontalAlign="Left" />
                                    <ItemStyle HorizontalAlign="Left" Width="20px" />
                                </asp:BoundField>
                                <asp:BoundField HeaderText="WedShort" DataField="Wednesday" SortExpression="Wednesday">
                                    <HeaderStyle Width="20px" HorizontalAlign="Left" />
                                    <ItemStyle HorizontalAlign="Left" Width="20px" />
                                </asp:BoundField>
                                <asp:BoundField HeaderText="ThuShort" DataField="Thursday" SortExpression="Thursday">
                                    <HeaderStyle Width="20px" HorizontalAlign="Left" />
                                    <ItemStyle HorizontalAlign="Left" Width="20px" />
                                </asp:BoundField>
                                <asp:BoundField HeaderText="FriShort" DataField="Friday" SortExpression="Friday">
                                    <HeaderStyle Width="20px" HorizontalAlign="Left" />
                                    <ItemStyle HorizontalAlign="Left" Width="20px" />
                                </asp:BoundField>
                                <asp:BoundField HeaderText="Total Slots" DataField="TOTALCOUNT" SortExpression="TOTALCOUNT">
                                    <HeaderStyle Width="30px" HorizontalAlign="Left" />
                                    <ItemStyle HorizontalAlign="Left" Width="30px" />
                                </asp:BoundField>
                                <asp:BoundField HeaderText="Used" DataField="USED" SortExpression="USED">
                                    <HeaderStyle Width="30px" HorizontalAlign="Left" />
                                    <ItemStyle HorizontalAlign="Left" Width="30px" />
                                </asp:BoundField>
                                <asp:BoundField HeaderText="UsedPerc" DataField="PercUsed" SortExpression="PercUsed">
                                    <HeaderStyle Width="30px" HorizontalAlign="Left" />
                                    <ItemStyle HorizontalAlign="Left" Width="30px" />
                                </asp:BoundField>
                                <asp:BoundField HeaderText="Ave Plts" DataField="UsedPallets" SortExpression="UsedPallets">
                                    <HeaderStyle Width="30px" HorizontalAlign="Left" />
                                    <ItemStyle HorizontalAlign="Left" Width="30px" />
                                </asp:BoundField>
                                <asp:BoundField HeaderText="Ave Ctns" DataField="UsedCartons" SortExpression="UsedCartons">
                                    <HeaderStyle Width="30px" HorizontalAlign="Left" />
                                    <ItemStyle HorizontalAlign="Left" Width="30px" />
                                </asp:BoundField>
                                <asp:BoundField HeaderText="Ave Lns" DataField="UsedLines" SortExpression="UsedLines">
                                    <HeaderStyle Width="30px" HorizontalAlign="Left" />
                                    <ItemStyle HorizontalAlign="Left" Width="30px" />
                                </asp:BoundField>
                            </Columns>
                        </cc1:ucGridView>
                    </td>
                </tr>
            </table>

            <table cellspacing="1" cellpadding="0" id="tblpv" runat="server">
                <tr>
                    <td>
                        <cc1:ucGridView ID="ucGridView2" runat="server" AutoGenerateColumns="false" CssClass="grid gvclass"
                            CellPadding="0" Width="960px" OnSorting="SortGrid" AllowSorting="true">
                            <AlternatingRowStyle BackColor="White" ForeColor="#284775"></AlternatingRowStyle>
                            <Columns>
                                <asp:TemplateField HeaderText="Vendor/Carrier" SortExpression="VendorName">
                                    <HeaderStyle Width="150px" HorizontalAlign="Left" />
                                    <ItemStyle HorizontalAlign="Left" Width="150px" />
                                    <ItemTemplate>
                                        <asp:HyperLink ID="hypLink" Target="_blank" runat="server" Text='<%#Eval("VendorName") %>' NavigateUrl='<%# EncryptQuery("FixedSlotUsageDetails.aspx?Period=" + ddlPeriod.SelectedItem.Value + "&VendorID="+ Eval("VendorID") + "&ScheduleType="+ Eval("ScheduleType") + "&SiteID="+ Eval("SiteID")) %>'></asp:HyperLink>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                
                                <asp:BoundField HeaderText="Total Slots" DataField="TOTALCOUNT" SortExpression="TOTALCOUNT">
                                    <HeaderStyle Width="30px" HorizontalAlign="Left" />
                                    <ItemStyle HorizontalAlign="Left" Width="30px" />
                                </asp:BoundField>
                                <asp:BoundField HeaderText="Used" DataField="USED" SortExpression="USED">
                                    <HeaderStyle Width="30px" HorizontalAlign="Left" />
                                    <ItemStyle HorizontalAlign="Left" Width="30px" />
                                </asp:BoundField>
                                <asp:BoundField HeaderText="UsedPerc" DataField="PercUsed" SortExpression="PercUsed">
                                    <HeaderStyle Width="30px" HorizontalAlign="Left" />
                                    <ItemStyle HorizontalAlign="Left" Width="30px" />
                                </asp:BoundField>
                                <asp:BoundField HeaderText="Ave Plts" DataField="UsedPallets" SortExpression="UsedPallets">
                                    <HeaderStyle Width="30px" HorizontalAlign="Left" />
                                    <ItemStyle HorizontalAlign="Left" Width="30px" />
                                </asp:BoundField>
                                <asp:BoundField HeaderText="Ave Ctns" DataField="UsedCartons" SortExpression="UsedCartons">
                                    <HeaderStyle Width="30px" HorizontalAlign="Left" />
                                    <ItemStyle HorizontalAlign="Left" Width="30px" />
                                </asp:BoundField>
                                <asp:BoundField HeaderText="Ave Lns" DataField="UsedLines" SortExpression="UsedLines">
                                    <HeaderStyle Width="30px" HorizontalAlign="Left" />
                                    <ItemStyle HorizontalAlign="Left" Width="30px" />
                                </asp:BoundField>
                            </Columns>
                        </cc1:ucGridView>
                    </td>
                </tr>
            </table>

        </div>
    </div>
</asp:Content>

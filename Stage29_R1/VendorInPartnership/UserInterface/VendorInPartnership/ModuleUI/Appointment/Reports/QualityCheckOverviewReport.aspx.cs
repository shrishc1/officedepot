﻿using System;
using Microsoft.Reporting.WebForms;

public partial class ModuleUI_Appointment_Reports_QualityCheckOverviewReport : CommonPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            txtFromDate.Text = DateTime.Now.AddDays(-DateTime.Now.Day + 1).ToString("dd/MM/yyyy");
            txtToDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
            hdnJSFromDt.Value = txtFromDate.Text;
            hdnJSToDt.Value = txtToDate.Text;
        }
    }

    protected void btnGenerateReport_Click(object sender, EventArgs e)
    {

        if (!string.IsNullOrEmpty(msSite.SelectedSiteIDs))
            hdnSiteIDs.Value = msSite.SelectedSiteIDs;
        if (!string.IsNullOrEmpty(txtFromDate.Text))
            hdnFromDt.Value = Utilities.Common.TextToDateFormat(hdnJSFromDt.Value).Year + "-" + Utilities.Common.TextToDateFormat(hdnJSFromDt.Value).Month + "-" + Utilities.Common.TextToDateFormat(hdnJSFromDt.Value).Day;
        if (!string.IsNullOrEmpty(txtToDate.Text))
            hdnToDt.Value = Utilities.Common.TextToDateFormat(hdnJSToDt.Value).Year + "-" + Utilities.Common.TextToDateFormat(hdnJSToDt.Value).Month + "-" + Utilities.Common.TextToDateFormat(hdnJSToDt.Value).Day;
        if (!string.IsNullOrEmpty(txtBookingReference.Text))
            hdnBookingReference.Value = txtBookingReference.Text;

        ReportParameter[] reportParameter = new ReportParameter[4];
        reportParameter[0] = new ReportParameter("BookingReference", txtBookingReference.Text);
        reportParameter[1] = new ReportParameter("Site", msSite.SelectedSiteName);
        reportParameter[2] = new ReportParameter("DateFrom", hdnJSFromDt.Value);
        reportParameter[3] = new ReportParameter("DateTo", hdnJSToDt.Value);
        this.QualityCheckOverviewReportReportViewer.LocalReport.SetParameters(reportParameter);
 
        UcCarrierVendorPanel.Visible = false;
        UcCarrierVendorReportViewPanel.Visible = true;
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        EncryptQueryString("QualityCheckOverviewReport.aspx");
    }
}
﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    AutoEventWireup="true" CodeFile="OpenPurchaseOrderSummaryOverview.aspx.cs" Inherits="ModuleUI_Appointment_Reports_OpenPurchaseOrderSummaryOverview"
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Src="~/CommonUI/UserControls/MultiSelectCountry.ascx" TagName="MultiSelectCountry"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectVendor.ascx" TagName="MultiSelectVendor"
    TagPrefix="cc1" %>
<%@ Register Src="../../../CommonUI/UserControls/ucExportToExcel.ascx" TagName="ucExportToExcel"
    TagPrefix="cc1" %>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register TagName="ucExportButton" TagPrefix="uc1" Src="~/CommonUI/UserControls/ucExportToExcel.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2>
        <cc1:ucLabel ID="lblOpenPOLinesSummary" runat="server"></cc1:ucLabel>
    </h2>
    <Style type="text/css">
   .row1{
   color:#333333;background-color:#F7F6F3;
   }
   .row2
   {color:#284775;background-color:#FFFFFF;      
   }
   </Style>
    <script type="text/javascript">
        $(document).ready(function () {
            if ($('#<%=rdoGroupBy.ClientID%>').is(':checked')) {                
                $('#<%=lblStockPlanner.ClientID%>').hide();
                $('#<%=ddlStockPlanner.ClientID%>').hide();
                $('#<%=lblStockPlannerGroupings.ClientID%>').show();
                $('#<%=ddlStockPlannerGroupings.ClientID%>').show();
            }

            if ($('#<%=rdoByPlanner.ClientID%>').is(':checked')) {                
                $('#<%=lblStockPlannerGroupings.ClientID%>').hide();
                $('#<%=ddlStockPlannerGroupings.ClientID%>').hide();
                $('#<%=lblStockPlanner.ClientID%>').show();
                $('#<%=ddlStockPlanner.ClientID%>').show();
            }

            $('#<%=rdoGroupBy.ClientID%>').change(function () {
                $('#<%=lblStockPlanner.ClientID%>').hide();
                $('#<%=ddlStockPlanner.ClientID%>').hide();
                $('#<%=lblStockPlannerGroupings.ClientID%>').show();
                $('#<%=ddlStockPlannerGroupings.ClientID%>').show();
            });
            $('#<%=rdoByPlanner.ClientID%>').change(function () {
                $('#<%=lblStockPlannerGroupings.ClientID%>').hide();
                $('#<%=ddlStockPlannerGroupings.ClientID%>').hide();
                $('#<%=lblStockPlanner.ClientID%>').show();
                $('#<%=ddlStockPlanner.ClientID%>').show();
            });

        });
    </script>
    <asp:ScriptManager runat="server" />
    <div class="right-shadow">
        <div class="formbox">
            <asp:HiddenField ID="hdnGridCurrentPageNo" runat="server" />
            <asp:HiddenField ID="hdnGridPageSize" runat="server" />
            <table width="95%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings"
                id="tblSearch" runat="server">
                <tr>
                    <td style="font-weight: bold; width: 2%">
                    </td>
                    <td style="font-weight: bold; width: 89%" colspan="2">
                        <asp:RadioButton ID="rdoGroupBy" runat="server" Text="By Group" GroupName="Group"
                            Checked="true" />
                        <asp:RadioButton ID="rdoByPlanner" runat="server" Text="By Planner" GroupName="Group" />
                        &nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;
                        <cc1:ucLabel ID="lblStockPlanner" ClientIDMode="Static" runat="server" Style="display: none;"></cc1:ucLabel>
                        &nbsp; &nbsp;
                        <cc1:ucDropdownList ID="ddlStockPlanner" runat="server" Width="150px" Style="display: none;">
                        </cc1:ucDropdownList>
                        <cc1:ucLabel ID="lblStockPlannerGroupings" ClientIDMode="Static" runat="server"></cc1:ucLabel>
                        &nbsp; &nbsp;
                        <cc1:ucDropdownList ID="ddlStockPlannerGroupings" runat="server" Width="150px">
                        </cc1:ucDropdownList>
                    </td>
                </tr>
                <tr>
                    <td style="font-weight: bold; width: 10%">
                        <cc1:ucLabel ID="lblCountry" runat="server" Text="Country"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold; width: 1%">
                        <cc1:ucLabel ID="UcLabel5" runat="server">:</cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold; width: 89%">
                        <cc1:MultiSelectCountry runat="server" ID="msCountry" />
                    </td>
                </tr>
                <tr>
                    <td style="font-weight: bold;">
                        <cc1:ucLabel ID="lblVendor" runat="server" Text="Vendor"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold;">
                        <cc1:ucLabel ID="UcLabel2" runat="server">:</cc1:ucLabel>
                    </td>
                    <td>
                        <cc1:MultiSelectVendor runat="server" ID="msVendor" />
                    </td>
                </tr>
                <tr>
                    <td colspan="4" align="right">
                        <cc1:ucButton ID="btnSearch" runat="server" Text="Search" CssClass="button" OnClick="btnSearch_Click" />
                    </td>
                </tr>
            </table>
            <table width="100%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings"
                id="tblGrid" runat="server" visible="false">
                <tr>
                <td>
                    <asp:Label ID="lblInfo" runat="server" Font-Bold="true" /></td>
                    <td align="right">
                        <cc1:ucButton ID="btnExportToExcel" runat="server" CssClass="exporttoexcel" OnClick="btnExport_Click"
                            Width="109px" Height="20px" Visible="false" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                       <%-- <cc1:ucGridView ID="gvOpenPurchaseOrder" Visible="false" runat="server" AutoGenerateColumns="true"
                            CssClass="grid gvclass searchgrid-1 " CellPadding="0" Width="100%" OnRowDataBound="gvOpenPurchaseOrder_RowDataBound">--%>
                           
                            <asp:GridView ID="gvOpenPurchaseOrder" Visible="false"  Width="100%" runat="server" AutoGenerateColumns="true" CssClass="grid" OnRowDataBound="gvOpenPurchaseOrder_RowDataBound">
                     <RowStyle CssClass="row1" />
                    <AlternatingRowStyle CssClass="row2" />
                     <RowStyle HorizontalAlign="Center" />
                            <EmptyDataTemplate>
                                <div style="text-align: left">
                                    <cc1:ucLabel ID="lblNoData" runat="server" Text="No Records found."></cc1:ucLabel>
                                </div>
                            </EmptyDataTemplate>
                        </asp:GridView>
                        <cc1:PagerV2_8 ID="pager1" runat="server" OnCommand="pager_Command" GenerateGoToSection="true">
                        </cc1:PagerV2_8>
                    </td>
                </tr>
                <tr>
                    <td align="right" colspan="2">
                        <cc1:ucButton ID="btnBack" runat="server" OnClick="btnBack_Click" />
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>

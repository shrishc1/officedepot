﻿using System;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.AdminFunctions;
using BusinessLogicLayer.ModuleBAL.AdminFunctions;
using BusinessEntities.ModuleBE.Appointment.CountrySetting;
using BusinessLogicLayer.ModuleBAL.Appointment.CountrySetting;
using System.Collections.Generic;
using Microsoft.Reporting.WebForms;

public partial class ModuleUI_Appointment_Reports_DeliveryType : CommonPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            txtFromDate.Text = DateTime.Now.AddDays(-DateTime.Now.Day + 1).ToString("dd/MM/yyyy");
            txtToDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
            hdnJSFromDt.Value = txtFromDate.Text;
            hdnJSToDt.Value = txtToDate.Text;

            APPCNT_DeliveryTypeBAL oAPPCNT_DeliveryTypeBAL = new APPCNT_DeliveryTypeBAL();
            MASCNT_DeliveryTypeBE oMASCNT_DeliveryTypeBE = new MASCNT_DeliveryTypeBE();

            oMASCNT_DeliveryTypeBE.Action = "ShowAll";
            oMASCNT_DeliveryTypeBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
            oMASCNT_DeliveryTypeBE.User.UserID = Convert.ToInt32(Session["UserID"]);

            List<MASCNT_DeliveryTypeBE> lstDeliveryType = new List<MASCNT_DeliveryTypeBE>();
            lstDeliveryType = oAPPCNT_DeliveryTypeBAL.GetDeliveryTypeDetailsBAL(oMASCNT_DeliveryTypeBE);
            if (lstDeliveryType.Count > 0)
            {
                FillControls.FillListBox(ref lstLeft, lstDeliveryType, "DeliveryType", "DeliveryTypeID");
            }
            
        }
    }

    private string _selectedDeliveryTypeIDs = string.Empty;
    public string SelectedDeliveryTypeIDs
    {
        get
        {
            _selectedDeliveryTypeIDs = string.Empty;
            if (lstRight.Items.Count > 0)
            {
                foreach (ListItem item in lstRight.Items)
                {
                    _selectedDeliveryTypeIDs += "," + item.Value;
                }
                return _selectedDeliveryTypeIDs.Trim(',');
            }
            return null;
        }
    }

    public string SelectedDeliveryType
    {
        get
        {
            string _SelectedDeliveryType = string.Empty;

            foreach (ListItem item in lstRight.Items)
                _SelectedDeliveryType += item.Text + ", ";

            if (_SelectedDeliveryType.Length > 0)
                _SelectedDeliveryType = _SelectedDeliveryType.Substring(0, _SelectedDeliveryType.Length - 2);

            return _SelectedDeliveryType;
        }
    }

    protected virtual void btnMoveRightAll_Click(object sender, EventArgs e)
    {
        FillControls.MoveAllItems(lstLeft, lstRight);
    }

    protected virtual void btnMoveRight_Click(object sender, EventArgs e)
    {
        if (lstLeft.SelectedItem != null)
        {
            FillControls.MoveOneItem(lstLeft, lstRight);
        }
    }

    protected virtual void btnMoveLeft_Click(object sender, EventArgs e)
    {
        if (lstRight.SelectedItem != null)
        {
            FillControls.MoveOneItem(lstRight, lstLeft);
        }
    }

    protected virtual void btnMoveLeftAll_Click(object sender, EventArgs e)
    {
        FillControls.MoveAllItems(lstRight, lstLeft);
    }

    protected void btnGenerateReport_Click(object sender, EventArgs e)
    {       
        if (!string.IsNullOrEmpty(msCountry.SelectedCountryIDs))
            hdnCountryIDs.Value = msCountry.SelectedCountryIDs;
        if (!string.IsNullOrEmpty(txtFromDate.Text))
            hdnFromDt.Value = Utilities.Common.TextToDateFormat(hdnJSFromDt.Value).Year + "-" + Utilities.Common.TextToDateFormat(hdnJSFromDt.Value).Month + "-" + Utilities.Common.TextToDateFormat(hdnJSFromDt.Value).Day;
        if (!string.IsNullOrEmpty(txtToDate.Text))
            hdnToDt.Value = Utilities.Common.TextToDateFormat(hdnJSToDt.Value).Year + "-" + Utilities.Common.TextToDateFormat(hdnJSToDt.Value).Month + "-" + Utilities.Common.TextToDateFormat(hdnJSToDt.Value).Day;
        if (!string.IsNullOrEmpty(SelectedDeliveryTypeIDs))
            hdnDeliveryTypeIDs.Value = SelectedDeliveryTypeIDs;

        ReportParameter[] reportParameter = new ReportParameter[4];
        reportParameter[0] = new ReportParameter("Country", msCountry.SelectedCountryName);
        reportParameter[1] = new ReportParameter("DeliveryType", SelectedDeliveryType);
        reportParameter[2] = new ReportParameter("DateFrom", hdnJSFromDt.Value);
        reportParameter[3] = new ReportParameter("DateTo", hdnJSToDt.Value);
        this.DeliveryTypeReportViewer.LocalReport.SetParameters(reportParameter);

        UcCarrierVendorPanel.Visible = false;
        UcCarrierVendorReportViewPanel.Visible = true;
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        EncryptQueryString("DeliveryType.aspx");
    }
}
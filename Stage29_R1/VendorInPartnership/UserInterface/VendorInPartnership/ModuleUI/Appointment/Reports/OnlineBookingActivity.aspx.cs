﻿using System;
using Microsoft.Reporting.WebForms;
using System.Web.UI;
using System.Data;
using BusinessLogicLayer.ModuleBAL.Security;
using BusinessEntities.ModuleBE.Security;
using System.Web.UI.WebControls;
using System.Collections.Generic;

public partial class ModuleUI_Appointment_Reports_OnlineBookingActivity : CommonPage
{
    DataTable dt = new DataTable();

    protected void Page_Init(object sender, EventArgs e)
    {
        ucVendorTemplateSelect.CurrentPage = this;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            txtFromDate.Text = DateTime.Now.AddDays(-DateTime.Now.Day + 1).ToString("dd/MM/yyyy");
            txtToDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
            hdnJSFromDt.Value = txtFromDate.Text;
            hdnJSToDt.Value = txtToDate.Text;
        }
        TextBox txtVendor = Utilities.Common.FindControlRecursive(this.Page.Controls[0], "txtVendorNo") as TextBox;
        if (txtVendor != null)
            txtVendor.Focus();
    }

    /// <summary>
    /// Method to show Vendor on the basis of selected Template
    /// </summary>
    public override void TemplateSelectedIndexChanged()
    {
        base.TemplateSelectedIndexChanged();

        int VendorTemplateId = Convert.ToInt32(ucVendorTemplateSelect.innerControlddlTemplate.SelectedValue);

        ListBox lstSelectedVendor = (ListBox)msVendor.FindControl("lstRight");
        ListBox lstVendor = (ListBox)msVendor.FindControl("ucVendor").FindControl("lstLeft");

        if (VendorTemplateId > 0)
        {

            SCT_TemplateBAL oSCT_TemplateBAL = new SCT_TemplateBAL();
            SCT_TemplateBE oSCT_TemplateBE = new SCT_TemplateBE();

            oSCT_TemplateBE.VendorTemplateID = VendorTemplateId;
            oSCT_TemplateBE.Action = "GetVendorTemplateInfobyID";
            List<SCT_TemplateBE> lstVendorTemplate = oSCT_TemplateBAL.GetVendorTemplateById(oSCT_TemplateBE);

            if (lstVendorTemplate != null && lstVendorTemplate.Count > 0)
            {
                lstSelectedVendor.Items.Clear();
                msVendor.innerControlHiddenField.Value = string.Empty;
                msVendor.innerControlHiddenField2.Value = string.Empty;
                for (int iCount = 0; iCount < lstVendorTemplate.Count; iCount++)
                {
                    lstSelectedVendor.Items.Add(new ListItem(lstVendorTemplate[iCount].Vendor.Vendor_Name, lstVendorTemplate[iCount].Vendor.VendorID.ToString()));
                    msVendor.SelectedVendorIDs = lstVendorTemplate[iCount].Vendor.VendorID.ToString();
                    msVendor.SelectedVendorName = lstVendorTemplate[iCount].Vendor.Vendor_Name.ToString();
                }
            }
        }
        else
        {
            lstSelectedVendor.Items.Clear();
            msVendor.innerControlHiddenField.Value = string.Empty;
            msVendor.innerControlHiddenField2.Value = string.Empty;
        }
    }

    protected void btnGenerateReport_Click(object sender, EventArgs e)
    {

        if (!string.IsNullOrEmpty(msSite.SelectedSiteIDs))
            hdnSiteIDs.Value = msSite.SelectedSiteIDs;
        if (!string.IsNullOrEmpty(msCountry.SelectedCountryIDs))
            hdnCountryIDs.Value = msCountry.SelectedCountryIDs;
        if (msVendor.Visible == true && !string.IsNullOrEmpty(msVendor.SelectedVendorIDs))
            hdnVendorIDs.Value = msVendor.SelectedVendorIDs;
        if (msStockPlanner.Visible == true && !string.IsNullOrEmpty(msStockPlanner.SelectedStockPlannerIDs))
            hdnInventoryMangerIDs.Value = msStockPlanner.SelectedStockPlannerIDs;
        if (!string.IsNullOrEmpty(txtFromDate.Text))
            hdnFromDt.Value = Utilities.Common.TextToDateFormat(hdnJSFromDt.Value).Year + "-" + Utilities.Common.TextToDateFormat(hdnJSFromDt.Value).Month + "-" + Utilities.Common.TextToDateFormat(hdnJSFromDt.Value).Day;
        if (!string.IsNullOrEmpty(txtToDate.Text))
            hdnToDt.Value = Utilities.Common.TextToDateFormat(hdnJSToDt.Value).Year + "-" + Utilities.Common.TextToDateFormat(hdnJSToDt.Value).Month + "-" + Utilities.Common.TextToDateFormat(hdnJSToDt.Value).Day;
        if (rdoDetail.Checked == true)
            hdnReportType.Value = "Detailed";
        if (rdoSummary.Checked == true)
            hdnReportType.Value = "Summary";
        if (rdbDailyView.Checked == true)
            hdnViewType.Value = "Daily";
        if (rdbMonthlyView.Checked == true)
            hdnViewType.Value = "Monthly";
        if (rdbWeeklyView.Checked == true)
            hdnViewType.Value = "Weekly";
        if (rdoBookingDatesearch.Checked == true)
            hdnSearchBy.Value = "BookingDate";
        if (rdoDeliveryDate.Checked == true)
            hdnSearchBy.Value = "DeliveryDate";

        //if (rdbDailyView.Checked == true || rdbWeeklyView.Checked == true || rdbMonthlyView.Checked == true)
        //{
        //    ReportParameter[] reportParameter1 = new ReportParameter[7];
        //    reportParameter1[0] = new ReportParameter("Country", msCountry.SelectedCountryName);
        //    reportParameter1[1] = new ReportParameter("Site", msSite.SelectedSiteName);
        //    reportParameter1[2] = new ReportParameter("Vendor", msVendor.SelectedVendorName);
        //    reportParameter1[3] = new ReportParameter("DateFrom", hdnJSFromDt.Value);
        //    reportParameter1[4] = new ReportParameter("DateTo", hdnJSToDt.Value);
        //    reportParameter1[5] = new ReportParameter("ReportType", hdnViewType.Value);
        //    reportParameter1[6] = new ReportParameter("InventoryManager", msStockPlanner.SelectedSPName);
        //    ViewTypeReportViewer.LocalReport.SetParameters(reportParameter1);
        //}

        //if (rdoSummaryReport.Checked == true || rdoDetailedReport.Checked == true)
        //{
        DataSourceSelectArguments arg = new DataSourceSelectArguments();
        DataView view = (DataView)SqlDataSource1.Select(arg);
        DataTable dtOnlineBookingActivity = view.ToTable();
        ReportDataSource rdsOnlineBookingActivity = new ReportDataSource();

        rdsOnlineBookingActivity = new ReportDataSource("dtOnlineBookingActivitySummary", dtOnlineBookingActivity);
        OnlineBookingActivityReportViewer.LocalReport.DataSources.Add(rdsOnlineBookingActivity);

        DataSourceSelectArguments arg1 = new DataSourceSelectArguments();
        DataView view1 = (DataView)SqlDataSource3.Select(arg1);
        DataTable dtOnlineBookingActivity1 = view1.ToTable();
        ReportDataSource rdsOnlineBookingActivity1 = new ReportDataSource();
        rdsOnlineBookingActivity1 = new ReportDataSource("dtOnlineBookingActivityDetailed", dtOnlineBookingActivity1);

        OnlineBookingActivityReportViewer.LocalReport.DataSources.Add(rdsOnlineBookingActivity1);

        // Subreport//
        DataSourceSelectArguments arg2 = new DataSourceSelectArguments();
        DataView view2 = (DataView)SqlDataSource2.Select(arg);
        dt= view2.ToTable();



        this.OnlineBookingActivityReportViewer.LocalReport.SubreportProcessing += new SubreportProcessingEventHandler(SetSubDataSource);
        ReportParameter[] reportParameter = new ReportParameter[8];
        reportParameter[0] = new ReportParameter("Country", msCountry.SelectedCountryName);
        reportParameter[1] = new ReportParameter("Site", msSite.SelectedSiteName);
        reportParameter[2] = new ReportParameter("Vendor", msVendor.SelectedVendorName);
        reportParameter[3] = new ReportParameter("DateFrom", hdnJSFromDt.Value);
        reportParameter[4] = new ReportParameter("DateTo", hdnJSToDt.Value);
        reportParameter[5] = new ReportParameter("ReportType", hdnReportType.Value);
        reportParameter[6] = new ReportParameter("ViewType", hdnViewType.Value);
        reportParameter[7] = new ReportParameter("InventoryManager", msStockPlanner.SelectedSPName);
        this.OnlineBookingActivityReportViewer.LocalReport.SetParameters(reportParameter);




        UcCarrierVendorPanel.Visible = false;
        UcCarrierVendorReportViewPanel.Visible = true;
    }

    public void SetSubDataSource(object sender, SubreportProcessingEventArgs e)
    {

        e.DataSources.Add(new ReportDataSource("dtOnlineBookingActivityViewType", dt));
        
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        EncryptQueryString("OnlineBookingActivity.aspx");
    }
}
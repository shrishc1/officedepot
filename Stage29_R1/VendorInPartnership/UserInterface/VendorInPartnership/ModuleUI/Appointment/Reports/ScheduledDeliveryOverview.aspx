﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    AutoEventWireup="true" CodeFile="ScheduledDeliveryOverview.aspx.cs" Inherits="ModuleUI_Appointment_Reports_ScheduledDeliveryOverview"
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectCountry.ascx" TagName="MultiSelectCountry"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectSite.ascx" TagName="MultiSelectSite"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectAP.ascx" TagName="MultiSelectAP"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectVendor.ascx" TagName="MultiSelectVendor"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectStockPlanner.ascx" TagName="MultiSelectStockPlanner"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/ucVendorSelectionTemplate.ascx" TagName="VendorSelectTemplate"
    TagPrefix="cc1" %>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectCarrier.ascx" TagName="MultiSelectCarrier"
    TagPrefix="uc1" %>
<%@ Register TagName="ucExportButton" TagPrefix="uc1" Src="~/CommonUI/UserControls/ucExportToExcel.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript" language="javascript">
        $(document).ready(function () {
            $("body").keypress(function (e) {
                if (e.which == 13 || e.keyCode == 13) {
                    $('#<%=UcButton1.ClientID %>').click();
                    return false;
                }
            });
        });
        function setValue1(target) {
            document.getElementById('<%=hdnJSFromDt.ClientID %>').value = target.value;
        }

        function setValue2(target) {
            document.getElementById('<%=hdnJSToDt.ClientID %>').value = target.value;
        }

    </script>
    <asp:ScriptManager ID="SM1" runat="server">
    </asp:ScriptManager>
    <asp:HiddenField ID="hdnJSFromDt" runat="server" />
    <asp:HiddenField ID="hdnJSToDt" runat="server" />
    <asp:HiddenField ID="hdnSiteIDs" runat="server" />
    <asp:HiddenField ID="hdnCountryIDs" runat="server" />
    <asp:HiddenField ID="hdnVendorIDs" runat="server" />
    <asp:HiddenField ID="hdnInventoryMnagerIDs" runat="server" />
    <asp:HiddenField ID="hdnBooking" runat="server" />
    <asp:HiddenField ID="hdnPurchaseOrder" runat="server" />
    <asp:HiddenField ID="hdnFromDt" runat="server" />
    <asp:HiddenField ID="hdnToDt" runat="server" />
    <h2>
        <cc1:ucLabel ID="lblScheduledDeliveryOverviewReport" runat="server" Text="Scheduled Delivery Overview Report"></cc1:ucLabel>
    </h2>
    <div class="right-shadow">
        <div class="formbox">
            <cc1:ucPanel ID="UcCarrierVendorPanel" runat="server">
             <table width="90%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                    <tr>
                        <td align="right">
                            <cc1:VendorSelectTemplate runat="server" ID="ucVendorTemplateSelect" />
                        </td>
                    </tr>
                </table>
                <table width="90%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                    <tr>
                        <td colspan="3">
                            <table>
                                <tr>
                                    <td style="font-weight: bold; width: 10%">
                                        <cc1:ucLabel ID="lblVendor" runat="server" Text="Vendor"></cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold; width: 1%">
                                        <cc1:ucLabel ID="lblVendorCollon" runat="server">:</cc1:ucLabel>
                                    </td>
                                    <td>
                                        <cc1:MultiSelectVendor runat="server" ID="msVendor" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="font-weight: bold; width: 10%">
                                        <cc1:ucLabel ID="lblCountry" runat="server" Text="Country"></cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold; width: 1%">
                                        <cc1:ucLabel ID="UcLabel5" runat="server">:</cc1:ucLabel>
                                    </td>
                                    <td>
                                        <cc1:MultiSelectCountry runat="server" ID="msCountry" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="font-weight: bold;">
                                        <cc1:ucLabel ID="lblSite" runat="server" Text="Site"></cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold;">
                                        <cc1:ucLabel ID="UcLabel4" runat="server">:</cc1:ucLabel>
                                    </td>
                                    <td>
                                        <cc1:MultiSelectSite runat="server" ID="msSite" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="font-weight: bold;">
                                        <cc1:ucLabel ID="lblInventoryManager" runat="server" Text="Inventory Manager"></cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold;">
                                        <cc1:ucLabel ID="UcLabel2" runat="server">:</cc1:ucLabel>
                                    </td>
                                    <td>
                                        <cc1:MultiSelectStockPlanner ID="msStockPlanner" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="font-weight: bold;">
                                        <cc1:ucLabel ID="lblPurchaseOrderNo" runat="server" Text="Purchase Order No"></cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold;">
                                        <cc1:ucLabel ID="UcLabel3" runat="server">:</cc1:ucLabel>
                                    </td>
                                    <td>
                                        <cc1:ucTextbox ID="txtPurchaseOrderNo" runat="server"></cc1:ucTextbox>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" align="center">
                                        <table width="60%" cellspacing="3" cellpadding="0" align="left" class="top-settings">
                                            <tr>
                                                <td style="width: 34%;" class="nobold">
                                                    <cc1:ucRadioButton ID="rdoConfirmedBooking" runat="server" GroupName="Booking" Text="Confirmed Booking"
                                                        Checked="true" />
                                                </td>
                                                <td style="width: 33%;" class="nobold">
                                                    <cc1:ucRadioButton ID="rdoUnconfirmedBooking" runat="server" GroupName="Booking"
                                                        Text="Unconfirmed Booking" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                            <%--<cc1:ucLabel runat="server" ID="UcLabel2" Text="Please select if you want to view report by Carrier or By Vendor" />--%>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <table width="100%">
                            </table>
                            <%--<cc1:ucLabel runat="server" ID="UcLabel3" Text="Please select if you want to view report by Carrier or By Vendor" />--%>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <div class="button-row">
                                <cc1:ucButton ID="UcButton1" runat="server" Text="Generate Report" CssClass="button"
                                    OnClick="btnGenerateReport_Click" />
                            </div>
                        </td>
                    </tr>
                </table>
            </cc1:ucPanel>
            <cc1:ucPanel ID="UcCarrierVendorReportViewPanel" runat="server" Visible="false">
                <table style="width: 100%;">
                    <tr>
                        <td>
                            <rsweb:ReportViewer ID="ScheduledDeliveryOverviewReportViewer" runat="server" Width="950px"
                                DocumentMapCollapsed="True" Font-Names="Verdana" Font-Size="8pt" InteractiveDeviceInfos="(Collection)"
                                WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt">
                                <LocalReport ReportPath="ModuleUI\Appointment\Reports\RDLC\ConfirmedPurchaseOrders.rdlc">
                                </LocalReport>
                            </rsweb:ReportViewer>
                            <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:sConn %>"
                                SelectCommand="spRPTBOK_ScheduledDeliveryOverview" SelectCommandType="StoredProcedure">
                                <SelectParameters>
                                    <asp:ControlParameter ControlID="hdnVendorIDs" Name="SelectedVendorIDs" PropertyName="Value"
                                        Type="String" DefaultValue="-1" />
                                    <asp:ControlParameter ControlID="hdnCountryIDs" DefaultValue="-1" Name="SelectedCountryIDs"
                                        PropertyName="Value" Type="String" />
                                    <asp:ControlParameter ControlID="hdnSiteIDs" Name="SelectedSiteIDs" PropertyName="Value"
                                        Type="String" DefaultValue="-1" />
                                    <asp:ControlParameter ControlID="hdnInventoryMnagerIDs" Name="SelectedInventoryMgrIDs"
                                        PropertyName="Value" Type="String" DefaultValue="-1" />
                                    <asp:ControlParameter ControlID="hdnPurchaseOrder" Name="PurchaseOrder" PropertyName="Value"
                                        Type="String" DefaultValue="-1" />
                                    <asp:Parameter DefaultValue="Confirmed" Name="BookinType" Type="String" />
                                </SelectParameters>
                            </asp:SqlDataSource>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <cc1:ucButton ID="btnBackSearch" runat="server" Text="Back To Search Page" CssClass="button"
                                OnClick="btnBack_Click" />
                        </td>
                    </tr>
                </table>
            </cc1:ucPanel>
            <cc1:ucPanel ID="ucpnlnotConfirmed" runat="server" Visible="false">
                <div class="button-row">
                    <uc1:ucExportButton ID="btnExportToExcel1" runat="server" />
                </div>
                <table style="width: 100%;">
                    <tr>
                        <td align="center">
                            <cc1:ucGridView ID="UcGridView1" Width="100%" runat="server" CssClass="grid" AllowSorting="false"
                                AllowPaging="false" OnRowDataBound="UcGridView1_RowDataBound">
                                <Columns>
                                    <asp:BoundField HeaderText="Vendor Name" DataField="VendorName">
                                        <HeaderStyle HorizontalAlign="Left" Width="20%" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="Site Name" DataField="SiteName">
                                        <HeaderStyle HorizontalAlign="Left" Width="15%" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="Purchase Order Number" DataField="Purchase_order">
                                        <HeaderStyle HorizontalAlign="Left" Width="10%" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="Purchase Order Due Date" DataField="Original_due_date">
                                        <HeaderStyle HorizontalAlign="Left" Width="15%" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="Number of Open Lines on Purchase Order" DataField="POLines">
                                        <HeaderStyle HorizontalAlign="Left" Width="10%" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="Number of Days till  Due Date  is met" DataField="PODueDays">
                                        <HeaderStyle HorizontalAlign="Left" Width="10%" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="Inventory Mgr" DataField="InventoryManager">
                                        <HeaderStyle HorizontalAlign="Left" Width="10%" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:BoundField>
                                    <asp:TemplateField HeaderText="Initiate chase">
                                        <HeaderStyle HorizontalAlign="Left" Width="10%" />
                                        <ItemStyle HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucCheckbox ID="chkChase" runat="server" />
                                            <asp:HiddenField ID="hdnVendorEmail" runat="server" Value='<%#Eval("VendorMail") %>' />
                                            <asp:HiddenField ID="hdnInventoryManagerMail" runat="server" Value='<%#Eval("InventoryManagerMail") %>' />
                                            <asp:HiddenField ID="VendorID" runat="server" Value='<%#Eval("VendorID") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </cc1:ucGridView>
                            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:sConn %>"
                                SelectCommand="spRPTBOK_ScheduledDeliveryOverview" SelectCommandType="StoredProcedure">
                                <SelectParameters>
                                    <asp:ControlParameter ControlID="hdnVendorIDs" DefaultValue="-1" Name="SelectedVendorIDs"
                                        PropertyName="Value" Type="String" />
                                    <asp:ControlParameter ControlID="hdnCountryIDs" DefaultValue="-1" Name="SelectedCountryIDs"
                                        PropertyName="Value" Type="String" />
                                    <asp:ControlParameter ControlID="hdnSiteIDs" DefaultValue="-1" Name="SelectedSiteIDs"
                                        PropertyName="Value" Type="String" />
                                    <asp:ControlParameter ControlID="hdnInventoryMnagerIDs" DefaultValue="-1" Name="SelectedInventoryMgrIDs"
                                        PropertyName="Value" Type="String" />
                                    <asp:ControlParameter ControlID="hdnPurchaseOrder" Name="PurchaseOrder" PropertyName="Value"
                                        Type="String" DefaultValue="-1" />
                                    <asp:ControlParameter ControlID="hdnBooking" Name="BookinType" PropertyName="Value"
                                        Type="String" DefaultValue="-1" />
                                </SelectParameters>
                            </asp:SqlDataSource>
                            <cc1:ucGridView ID="UcGridView2" Width="100%" runat="server" Style="display: none;">
                                <Columns>
                                    <asp:BoundField HeaderText="Vendor Name" DataField="VendorName">
                                        <HeaderStyle HorizontalAlign="Left" Width="20%" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="Site Name" DataField="SiteName">
                                        <HeaderStyle HorizontalAlign="Left" Width="15%" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="Purchase Order Number" DataField="Purchase_order">
                                        <HeaderStyle HorizontalAlign="Left" Width="10%" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="Purchase Order Due Date" DataField="Original_due_date">
                                        <HeaderStyle HorizontalAlign="Left" Width="15%" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="Number of Open Lines on Purchase Order" DataField="POLines">
                                        <HeaderStyle HorizontalAlign="Left" Width="10%" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="Number of Days till  Due Date  is met" DataField="PODueDays">
                                        <HeaderStyle HorizontalAlign="Left" Width="10%" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="Inventory Mgr" DataField="InventoryManager">
                                        <HeaderStyle HorizontalAlign="Left" Width="10%" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:BoundField>
                                </Columns>
                            </cc1:ucGridView>
                        </td>
                    </tr>
                    <tr>
                        <td align="right" style="padding-right: 80px;">
                            <cc1:ucButton ID="btnChase" runat="server" Text="Chase" CssClass="button" OnClick="btnChase_Click" />
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <cc1:ucButton ID="UcButton2" runat="server" Text="Back To Search Page" CssClass="button"
                                OnClick="btnBack_Click" />
                        </td>
                    </tr>
                </table>
            </cc1:ucPanel>
        </div>
    </div>
</asp:Content>

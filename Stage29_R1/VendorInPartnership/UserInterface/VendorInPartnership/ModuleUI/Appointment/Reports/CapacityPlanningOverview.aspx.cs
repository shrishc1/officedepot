﻿using System;
using Microsoft.Reporting.WebForms;

using System.Data.SqlClient;
using System.Data;
using BusinessEntities.ModuleBE.AdminFunctions;
using BusinessLogicLayer.ModuleBAL.AdminFunctions;
using System.Collections.Generic;

public partial class ModuleUI_Appointment_Reports_CapacityPlanningOverview : CommonPage
{
    DataSet objDataSet = new DataSet();
    DataSet objDataSet1 = new DataSet();
    DataSet objDataSet2 = new DataSet();
    protected void Page_Init(object sender, EventArgs e)
    {
        ddlSite.CurrentPage = this;
    }

    protected void Page_PreRender(object sender, EventArgs e) {
        if (!Page.IsPostBack) {
            ddlSite.innerControlddlSite.AutoPostBack = true;
            GetSite();
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        // CapacityPlanningOverviewReportViewer.Visible = false;
        // ReportViewTd.Visible = false;

        if (!Page.IsPostBack) {
            ddlSite.PreCountryID = Convert.ToInt32(Session["SiteCountryID"]);           
        }
    }



    public override void SiteSelectedIndexChanged() {
        GetSite();
    }
    private void GetSite() {
        try {
            MAS_SiteBE oMAS_SiteBE = new MAS_SiteBE();
            MAS_SiteBAL oMAS_SiteBAL = new MAS_SiteBAL();

            oMAS_SiteBE.Action = "GetSiteByID";

            if (ddlSite.innerControlddlSite.SelectedItem == null)
            {
                oMAS_SiteBE.SiteID = Convert.ToInt32(Session["SiteID"]);            
            }
            else
            {
            oMAS_SiteBE.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
            }
            oMAS_SiteBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();

            List<MAS_SiteBE> lstSite = oMAS_SiteBAL.GetSiteByIDBAL(oMAS_SiteBE);

            if (lstSite != null && lstSite.Count > 0) {
                UcLinePerHour.Text = lstSite[0].LinePerFTE.ToString();
            }
        }
        catch { }
    }

    protected void btnGenerateReport_Click(object sender, EventArgs e)
    {
        //  CapacityPlanningOverviewReportViewer.Visible = true;
        //ReportViewTd.Visible = true;
      
        SqlConnection objSqlConnection = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["sConn"].ToString());
        objSqlConnection.Open();
        SqlCommand objSqlCommand = new SqlCommand("spRPTBOK_CapacityPlanningSlotWiseDetails_WeekWise", objSqlConnection);
        objSqlCommand.CommandType = CommandType.StoredProcedure;
        objSqlCommand.Parameters.Add("@SiteID", SqlDbType.VarChar).Value = ddlSite.innerControlddlSite.SelectedItem.Value;
        if (rdoCurrentWeek.Checked)
            objSqlCommand.Parameters.Add("@WEEKNO", SqlDbType.Int).Value = 1;
        if (rdoNextWeek.Checked)
            objSqlCommand.Parameters.Add("@WEEKNO", SqlDbType.Int).Value = 2;

        SqlDataAdapter objSqlDataAdapter = new SqlDataAdapter(objSqlCommand);
        objSqlDataAdapter.Fill(objDataSet);

        SqlCommand objSqlCommand1 = new SqlCommand("spRPTBOK_CapacityPlanningSlotPloting_WeekWise", objSqlConnection);
        objSqlCommand1.CommandType = CommandType.StoredProcedure;
        objSqlCommand1.Parameters.Add("@SiteID", SqlDbType.VarChar).Value = ddlSite.innerControlddlSite.SelectedItem.Value;
        if (rdoCurrentWeek.Checked)
            objSqlCommand1.Parameters.Add("@WEEKNO", SqlDbType.Int).Value = 1;
        if (rdoNextWeek.Checked)
            objSqlCommand1.Parameters.Add("@WEEKNO", SqlDbType.Int).Value = 2;

        SqlDataAdapter objSqlDataAdapter1 = new SqlDataAdapter(objSqlCommand1);
        objSqlDataAdapter1.Fill(objDataSet1);


        SqlCommand objSqlCommand2 = new SqlCommand("spRPTBOK_CapacityPlanningSlotWiseNonTime_WeekWise", objSqlConnection);
        objSqlCommand2.CommandType = CommandType.StoredProcedure;
        objSqlCommand2.Parameters.Add("@SiteID", SqlDbType.VarChar).Value = ddlSite.innerControlddlSite.SelectedItem.Value;
        if (rdoCurrentWeek.Checked)
            objSqlCommand2.Parameters.Add("@WEEKNO", SqlDbType.Int).Value = 1;
        if (rdoNextWeek.Checked)
            objSqlCommand2.Parameters.Add("@WEEKNO", SqlDbType.Int).Value = 2;

        SqlDataAdapter objSqlDataAdapter2 = new SqlDataAdapter(objSqlCommand2);
        objSqlDataAdapter2.Fill(objDataSet2);



        CapacityPlanningOverviewReportViewer.LocalReport.ReportPath = Server.MapPath("~") + "\\ModuleUI\\Appointment\\Reports\\RDLC\\CapacityPlanningOverview_WeekWise.rdlc";
        this.CapacityPlanningOverviewReportViewer.LocalReport.SubreportProcessing += new SubreportProcessingEventHandler(SetSubDataSource);
        ReportDataSource rdsRPTBOK_AppointmentSchedulingError = new ReportDataSource();
       
        rdsRPTBOK_AppointmentSchedulingError = new ReportDataSource("dtCombineCapacityPlanning", objDataSet1.Tables[0]);
        CapacityPlanningOverviewReportViewer.LocalReport.DataSources.Clear();
        CapacityPlanningOverviewReportViewer.LocalReport.DataSources.Add(rdsRPTBOK_AppointmentSchedulingError);

        ReportParameter[] reportParameter = new ReportParameter[1];
        reportParameter[0] = new ReportParameter("Site", ddlSite.innerControlddlSite.SelectedItem.Text);
        this.CapacityPlanningOverviewReportViewer.LocalReport.SetParameters(reportParameter);
    }

    public void SetSubDataSource(object sender, SubreportProcessingEventArgs e)
    {
       
        e.DataSources.Add(new ReportDataSource("dtSubSlotimeDetails", objDataSet.Tables[0]));
        e.DataSources.Add(new ReportDataSource("dtRPTBOK_CapacityPlanningSlotWiseNonTime", objDataSet2.Tables[0]));
       
    }
}
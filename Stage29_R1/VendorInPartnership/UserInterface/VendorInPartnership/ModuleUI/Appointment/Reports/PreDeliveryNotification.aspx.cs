﻿using System;
using Microsoft.Reporting.WebForms;
using System.Web.UI.WebControls;
using Utilities;
using WebUtilities;
using System.Configuration;
using System.IO;

public partial class ModuleUI_Appointment_Reports_PreDeliveryNotification : CommonPage {
    protected void Page_Load(object sender, EventArgs e) {
        if (!IsPostBack) {
            txtFromDate.Text = DateTime.Now.AddDays(-DateTime.Now.Day + 1).ToString("dd/MM/yyyy");
            txtToDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
            hdnJSFromDt.Value = txtFromDate.Text;
            hdnJSToDt.Value = txtToDate.Text;
        }
        btnExportToExcel1.GridViewControl = UcGridView2;
        btnExportToExcel1.FileName = "PreDeliveryNotificationReport";
    }

    protected void btnGenerateReport_Click(object sender, EventArgs e) {

        if (!string.IsNullOrEmpty(msSite.SelectedSiteIDs))
            hdnSiteIDs.Value = msSite.SelectedSiteIDs;
        if (!string.IsNullOrEmpty(txtFromDate.Text))
            hdnFromDt.Value = Utilities.Common.TextToDateFormat(hdnJSFromDt.Value).Year + "-" + Utilities.Common.TextToDateFormat(hdnJSFromDt.Value).Month + "-" + Utilities.Common.TextToDateFormat(hdnJSFromDt.Value).Day;
        if (!string.IsNullOrEmpty(txtToDate.Text))
            hdnToDt.Value = Utilities.Common.TextToDateFormat(hdnJSToDt.Value).Year + "-" + Utilities.Common.TextToDateFormat(hdnJSToDt.Value).Month + "-" + Utilities.Common.TextToDateFormat(hdnJSToDt.Value).Day;


        //ReportParameter[] reportParameter = new ReportParameter[3];
        //reportParameter[0] = new ReportParameter("Site", msSite.SelectedSiteName);
        //reportParameter[1] = new ReportParameter("DateFrom", hdnJSFromDt.Value);
        //reportParameter[2] = new ReportParameter("DateTo", hdnJSToDt.Value);
        //this.PreDeliveryNotificationReportViewer.LocalReport.SetParameters(reportParameter);

        UcCarrierVendorPanel.Visible = false;
        UcCarrierVendorReportViewPanel.Visible = true;
    }

    protected void btnBack_Click(object sender, EventArgs e) {
        EncryptQueryString("PreDeliveryNotification.aspx");
    }

    protected void btnChase_Click(object sender, EventArgs e) {
        // Iterate through the Products.Rows property
        foreach (GridViewRow row in UcGridView1.Rows) {
            // Access the CheckBox
            CheckBox cb = (CheckBox)row.FindControl("chkChase");
            if (cb != null && cb.Checked) {
                HiddenField hdnVendorIDs = (HiddenField)row.FindControl("hdnVendorIDs");

                //Send Mail
                try {
                    string[] sentTo;
                    string[] language;
                    string[] VendorData = new string[2];


                    string[] VendorsIDs = hdnVendorIDs.Value.Split(',');

                    for (int iCount = 0; iCount < VendorsIDs.Length; iCount++) {
                        VendorData = CommonPage.GetVendorEmailsWithLanguage(Convert.ToInt32(VendorsIDs[iCount]));
                        if (VendorData.Length > 0 && !string.IsNullOrEmpty(VendorData[0])) {
                            sentTo = VendorData[0].Split(',');
                            language = VendorData[1].Split(',');
                            for (int j = 0; j < sentTo.Length; j++) {
                                if (sentTo[j].Trim() != "") {
                                    SendMailToVendor(sentTo[j], language[j]);
                                }
                            }
                        }
                    }
                }
                catch { }
            }
        }
    }

    private void SendMailToVendor(string toAddress, string language) {

        string path = AppDomain.CurrentDomain.BaseDirectory.ToLower();
        string templatePathName = @"/EmailTemplates/Appointment/";
        if (!string.IsNullOrEmpty(toAddress)) {
            string htmlBody = string.Empty;

            string templatePath = null;
            templatePath = path + templatePathName;
            string LanguageFile = "Pre-advise." + language + ".htm";
            if (!System.IO.File.Exists(templatePath + LanguageFile.ToLower())) {
                LanguageFile = "Pre-advise." + "english.htm";
            }

            using (StreamReader sReader = new StreamReader(templatePath + LanguageFile.ToLower())) {
                htmlBody = sReader.ReadToEnd();
            }
            string sFromAddress = Convert.ToString(ConfigurationManager.AppSettings["fromAddress"]);
            clsEmail oclsEmail = new clsEmail();
            oclsEmail.sendMail(toAddress, htmlBody, "Pre Advise Notification", sFromAddress, true);

        }


    }



    bool firstTime = true;
    System.Data.DataTable dt;

    protected void UcGridView1_RowDataBound(object sender, GridViewRowEventArgs e) {
        if (e.Row.RowType == DataControlRowType.DataRow)
            if (this.firstTime) {
                System.Data.DataView dv =
                     (e.Row.DataItem as System.Data.DataRowView).DataView;
                this.dt = dv.ToTable();

                UcGridView2.DataSource = dt;
                UcGridView2.DataBind();

                this.firstTime = false;
            }
    }
}
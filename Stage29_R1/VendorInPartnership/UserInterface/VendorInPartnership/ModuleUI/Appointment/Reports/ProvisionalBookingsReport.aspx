﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    AutoEventWireup="true" CodeFile="ProvisionalBookingsReport.aspx.cs" Inherits="ProvisionalBookingsReport" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register TagName="ucDate" TagPrefix="cc2" Src="~/CommonUI/UserControls/ucDate.ascx" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectSite.ascx" TagName="MultiSelectSite"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectVendor.ascx" TagName="MultiSelectVendor"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectCountry.ascx" TagName="MultiSelectCountry"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectCarriers.ascx" TagName="MultiSelectCarriers"
    TagPrefix="uc1" %>
<%@ Register TagName="ucExportButton" TagPrefix="uc1" Src="~/CommonUI/UserControls/ucExportToExcel.ascx" %>
<%@ Register Src="~/CommonUI/UserControls/ucCountry.ascx" TagName="ucCountry" TagPrefix="cc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript" language="javascript">
        $(document).ready(function () {
            $("body").keypress(function (e) {
                if (e.which == 13 || e.keyCode == 13) {
                    $('#<%=btnSearch.ClientID %>').click();
                    return false;
                }
            });
        });
        function setValue1(target) {
            document.getElementById('<%=hdnJSFromDt.ClientID %>').value = target.value;
        }

        function setValue2(target) {
            document.getElementById('<%=hdnJSToDt.ClientID %>').value = target.value;
        }
        $(document).ready(function () {           
            var html1 = "";
            html1 = "<tr><th></th><th colspan='2' style='text-align:center'>Ashton</th><th colspan='2' style='text-align:center'>Dublin</th><th colspan='2' style='text-align:center'>Leicester</th><th colspan='2' style='text-align:center'>Northampton</th><th colspan='2' style='text-align:center'>Total</th></tr>";
            $('#ctl00_ContentPlaceHolder1_gvSummary tr:first').before(html1);
            $('#ctl00_ContentPlaceHolder1_gvSummaryExcel tr:first').before(html1);
        }
    );
    </script>
    <style type="text/css">
        .customClass
        {
            table, td, th {border: 1px solid black !important;}
        }
    </style>
    <h2>
        <%--<cc1:ucLabel ID="UcLabel5" runat="server"></cc1:ucLabel>--%>
        <cc1:ucLabel ID="lblProvisionalBookingsReport" Text="Provisional Bookings Report"
            runat="server"></cc1:ucLabel>
    </h2>
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:HiddenField ID="hdnJSFromDt" runat="server" />
    <asp:HiddenField ID="hdnJSToDt" runat="server" />
    <div class="right-shadow">
        <div class="formbox">
            <cc1:ucMultiView ID="mvDiscrepancyList" runat="server" ActiveViewIndex="0">
                <cc1:ucView ID="vwSearchCreteria" runat="server">
                    <cc1:ucPanel ID="UcCarrierVendorPanel" runat="server">
                        <table width="100%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                            <tr>
                                <td style="font-weight: bold; width: 10%;">
                                    <cc1:ucLabel ID="lblCountry" runat="server" Text="Country"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold; width: 2%;">
                                    <cc1:ucLabel ID="UcLabel3" runat="server">:</cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold; width: 88%;">
                                    <cc2:ucCountry ID="ddlCountry" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td style="font-weight: bold;">
                                    <cc1:ucLabel ID="lblSite" runat="server" Text="Site"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold;">
                                    <cc1:ucLabel ID="UcLabel4" runat="server">:</cc1:ucLabel>
                                </td>
                                <td>
                                    <cc1:MultiSelectSite runat="server" ID="msSite" />
                                </td>
                            </tr>
                            <tr>
                                <td style="font-weight: bold;">
                                    <cc1:ucLabel ID="lblVendor" runat="server" Text="Vendor"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold;">
                                    <cc1:ucLabel ID="lblVendorCollon" runat="server">:</cc1:ucLabel>
                                </td>
                                <td>
                                    <cc1:MultiSelectVendor runat="server" ID="msVendor" />
                                </td>
                            </tr>
                            <tr>
                                <td style="font-weight: bold;">
                                    <cc1:ucLabel ID="lblCarrier" runat="server" Text="Carrier"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold;">
                                    <cc1:ucLabel ID="lblCarrierCollon" runat="server">:</cc1:ucLabel>
                                </td>
                                <td>
                                    <uc1:MultiSelectCarriers ID="msCarrier" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td style="font-weight: bold;">
                                    <cc1:ucLabel ID="lblFrom" runat="server" Text="From"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold;">
                                    <cc1:ucLabel ID="UcLabel1" runat="server">:</cc1:ucLabel>
                                </td>
                                <td align="left">
                                    <span style="font-weight: bold; width: 6em; margin-left: 4px;">
                                        <cc1:ucTextbox ID="txtFromDate" runat="server" ClientIDMode="Static" CssClass="date"
                                            onchange="setValue1(this)" ReadOnly="True" Width="70px" />
                                    </span><span style="font-weight: bold; width: 4em; text-align: center">
                                        <cc1:ucLabel ID="lblTo" runat="server" Text="To"></cc1:ucLabel></span> 
                                    <span style="font-weight: bold;
                                            width: 6em;">
                                            <cc1:ucTextbox ID="txtToDate" runat="server" ClientIDMode="Static" CssClass="date"
                                                onchange="setValue2(this)" ReadOnly="True" Width="70px" />
                                        </span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    <table width="100%">
                                        <tr style="height: 3em;">
                                            <td width="10%" style="font-weight: bold;">
                                            </td>
                                            <td width="1%" style="font-weight: bold;">
                                            </td>
                                            <td width="89%">
                                                <table cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td style="font-weight: bold;" class="checkbox-list">
                                                            <cc1:ucRadioButton ID="rdoSummary" runat="server" Text="Summary" GroupName="ReportType"
                                                                Checked="true" />
                                                        </td>
                                                        <td style="font-weight: bold;" class="checkbox-list">
                                                            <cc1:ucRadioButton ID="rdoDetailed" runat="server" Text="Detailed" GroupName="ReportType"
                                                                onClick="checkFlag()" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                            <td align="left" class="nobold radiobuttonlist">
                                                <cc1:ucRadioButtonList ID="rblSearchOption" CssClass="radio-fix" runat="server" RepeatColumns="3"
                                                    RepeatDirection="Horizontal">
                                                </cc1:ucRadioButtonList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" colspan="3">
                                                <div class="button-row">
                                                    <cc1:ucButton ID="btnSearch" runat="server" Text="Search" CssClass="button" OnClick="btnSearch_Click" />
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </cc1:ucPanel>
                </cc1:ucView>
                <cc1:ucView ID="vwSummaryView" runat="server">
                    <div class="button-row">
                        <uc1:ucExportButton ID="btnExportToExcelProvBookingsSummaryByType" runat="server"
                            Visible="false" />
                        <%--<uc1:ucExportButton ID="btnExportToExcelProvBookingsSummaryBySite" runat="server"
                            Visible="false" />--%>
                        <cc1:ucButton ID="btnExportToExcel" runat="server" 
                            OnClick="btnExportToExcelProvBookingsSummaryBySite_Click" CssClass="exporttoexcel" Width="109px" Height="20px" Visible="false"/>                        
                    </div>
                    <cc1:ucPanel ID="pnlSummary" runat="server" CssClass="fieldset-form">
                        <table>
                            <tr>
                                <td align="left">
                                    <cc1:ucLabel ID="lblReportType" runat="server" Text="Report Type"></cc1:ucLabel>&nbsp;&nbsp;
                                </td>
                                <td align="left">
                                    <cc1:ucDropdownList ID="ddlReportTypes" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlReportTypes_SelectedIndexChanged"
                                        Width="100px">
                                        <asp:ListItem Selected="True" Text="By Type" Value="1"></asp:ListItem>
                                        <asp:ListItem Text="By Site" Value="2"></asp:ListItem>
                                    </cc1:ucDropdownList>
                                </td>
                            </tr>
                        </table>
                        <cc1:ucPanel ID="pnlSummaryByType" runat="server">
                            <cc1:ucGridView ID="gvProvisionalBookingByType" Width="950" runat="server" CssClass="grid"
                                OnSorting="SortGrid" PageSize="50" AllowPaging="true" PagerStyle-HorizontalAlign="Left"
                                AllowSorting="true" Style="overflow: auto;" OnPageIndexChanging="gvProvisionalBookingByType_PageIndexChanging">
                                <EmptyDataTemplate>
                                    <div style="text-align: center">
                                        <cc1:ucLabel ID="lblRecordNotFound" isRequired="true" runat="server" Text="No record found"></cc1:ucLabel>
                                    </div>
                                </EmptyDataTemplate>
                                <Columns>
                                    <asp:TemplateField HeaderText="VendorCarrierName">
                                        <HeaderStyle Width="150px" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLiteral ID="lblVendorNameValue" runat="server" Text='<%#Eval("Vendor.VendorName") %>'></cc1:ucLiteral>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Accepted">
                                        <HeaderStyle Width="50px" HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <cc1:ucLiteral ID="lblAcceptedValue" runat="server" Text='<%#Eval("Accepted") %>'></cc1:ucLiteral>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Rejected">
                                        <HeaderStyle Width="50px" HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <cc1:ucLiteral ID="lblRejectedValue" runat="server" Text='<%#Eval("Rejected") %>'></cc1:ucLiteral>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Total">
                                        <HeaderStyle Width="50px" HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <cc1:ucLiteral ID="lblTotalValue" runat="server" Text='<%#Eval("Total") %>'></cc1:ucLiteral>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="NoSpace">
                                        <HeaderStyle Width="50px" HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <cc1:ucLiteral ID="lblProvNoSpaceValue" runat="server" Text='<%#Eval("ProvNoSpace") %>'></cc1:ucLiteral>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="WindowCapacity">
                                        <HeaderStyle Width="50px" HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <cc1:ucLiteral ID="lblProvWindowCapacityValue" runat="server" Text='<%#Eval("ProvWindowCapacity") %>'></cc1:ucLiteral>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="FixedSlot">
                                        <HeaderStyle Width="50px" HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <cc1:ucLiteral ID="lblProvFixedSlotValue" runat="server" Text='<%#Eval("ProvFixedSlot") %>'></cc1:ucLiteral>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </cc1:ucGridView>
                            <cc1:ucGridView ID="gvProvisionalBookingByTypeExcel" Width="100%" runat="server"
                                CssClass="grid" Style="overflow: auto;" Visible="false">
                                <EmptyDataTemplate>
                                    <div style="text-align: center">
                                        <cc1:ucLabel ID="lblRecordNotFound" isRequired="true" runat="server" Text="No record found"></cc1:ucLabel>
                                    </div>
                                </EmptyDataTemplate>
                                <Columns>
                                    <asp:TemplateField HeaderText="VendorCarrierName">
                                        <HeaderStyle Width="180px" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLiteral ID="lblVendorNameValue" runat="server" Text='<%#Eval("Vendor.VendorName") %>'></cc1:ucLiteral>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Accepted">
                                        <HeaderStyle Width="180px" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLiteral ID="lblAcceptedValue" runat="server" Text='<%#Eval("Accepted") %>'></cc1:ucLiteral>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Rejected">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <cc1:ucLiteral ID="lblRejectedValue" runat="server" Text='<%#Eval("Rejected") %>'></cc1:ucLiteral>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Total">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <cc1:ucLiteral ID="lblTotalValue" runat="server" Text='<%#Eval("Total") %>'></cc1:ucLiteral>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="NoSpace">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <cc1:ucLiteral ID="lblProvNoSpaceValue" runat="server" Text='<%#Eval("ProvNoSpace") %>'></cc1:ucLiteral>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="WindowCapacity">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <cc1:ucLiteral ID="lblProvWindowCapacityValue" runat="server" Text='<%#Eval("ProvWindowCapacity") %>'></cc1:ucLiteral>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="FixedSlot">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <cc1:ucLiteral ID="lblProvFixedSlotValue" runat="server" Text='<%#Eval("ProvFixedSlot") %>'></cc1:ucLiteral>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </cc1:ucGridView>
                        </cc1:ucPanel>
                        <cc1:ucPanel ID="pnlSummaryBySite" runat="server" Visible="false">
                            <div style="width: 950px; overflow-x: auto;">
                                <table style="width:100%">
                                    <tr>
                                        <td align="center"  style="width:100%" align="center" valign="middle">
                                            <cc1:ucLabel ID="lblRecordNotFound" isRequired="true" runat="server" Text="No record found"></cc1:ucLabel>                                            
                                        </td>
                                    </tr>
                                </table>
                                <table border="0" cellspacing="0" cellpadding="0" class="form-table" id="tblMain"
                                    runat="server">
                                    <tr>                                        
                                        <td align="left">
                                            <table border="0" cellspacing="0" cellpadding="0" class="grid" width="100%">
                                                <tr>
                                                    <th id="thVendorCarrierName" runat="server" style="width:300px; text-align: left; vertical-align: middle;">
                                                        <cc1:ucLabel ID="lblVendorCarrierName" runat="server" Text='Vendor/Carrier Name'></cc1:ucLabel>
                                                    </th>
                                                    <asp:Repeater ID="rptSite" runat="server" OnItemDataBound="rptSite_OnItemDataBound">
                                                        <ItemTemplate>
                                                            <th style="text-align: right;">
                                                                <table border="0">
                                                                    <tr>
                                                                        <td align="center" runat="server" id="tdControlSite">
                                                                            <asp:Literal ID="ltSitePreFix" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "SiteName") %>'></asp:Literal>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <asp:Repeater ID="rptKeyHeader" runat="server">
                                                                            <ItemTemplate>
                                                                                <td align="right" runat="server" id="tdControl">
                                                                                    <asp:Literal ID="ltKeyHeaderText" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "KeyHeader") %>'></asp:Literal>
                                                                                </td>
                                                                            </ItemTemplate>
                                                                        </asp:Repeater>
                                                                    </tr>
                                                                </table>
                                                            </th>
                                                        </ItemTemplate>
                                                    </asp:Repeater>
                                                </tr>
                                                <asp:Repeater ID="rptkeyDetails" runat="server" OnItemDataBound="rptkeyDetails_OnItemDataBound">
                                                    <ItemTemplate>
                                                        <tr>
                                                            <td style="height: 20px; width:5%;">
                                                                <asp:Literal ID="ltVendorNameValue" runat="server" Text='<%#  DataBinder.Eval(Container.DataItem, "VendorName") %>'></asp:Literal>
                                                                <asp:Literal ID="ltVendorIDValue" runat="server" Text='<%#  DataBinder.Eval(Container.DataItem, "VendorID") %>'
                                                                    Visible="false">
                                                                </asp:Literal>
                                                            </td>
                                                            <asp:Repeater ID="rptKeyvalue" runat="server" OnItemDataBound="rptKeyvalue_OnItemDataBound">
                                                                <ItemTemplate>
                                                                    <td style="text-align: right; width:5%;">
                                                                        <table border="0">
                                                                            <tr>
                                                                                <asp:Repeater ID="rptKeyValueDetails" runat="server">
                                                                                    <ItemTemplate>
                                                                                        <td align="right" runat="server" id="tdControl" style="height: 20px;">
                                                                                            <asp:Literal ID="ltKeyvalue" runat="server" Text='<%#  DataBinder.Eval(Container.DataItem, "Keyvalue") %>'></asp:Literal>
                                                                                        </td>
                                                                                    </ItemTemplate>
                                                                                </asp:Repeater>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </ItemTemplate>
                                                            </asp:Repeater>
                                                        </tr>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <table border="0" cellspacing="0" cellpadding="0" class="form-table">
                                <tr>
                                    <td>
                                        <cc1:PagerV2_8 ID="pager1" runat="server" OnCommand="pager_Command" GenerateGoToSection="false">
                                        </cc1:PagerV2_8>
                                    </td>
                                </tr>
                            </table>
                            <table cellspacing="0" cellpadding="0" class="form-table customClass" id="tblExport"
                                runat="server" style="display:none;">
                                <tr>
                                    <td>
                                        <table cellspacing="0" cellpadding="0" class="grid gvclass searchgrid-1 customClass">
                                            <tr>
                                                <th style="width: 50px; height: 36px; text-align: left; vertical-align: middle; background-color:#DBDBDB;">
                                                    <cc1:ucLabel ID="UcLabel2" runat="server" Text='Vendor/Carrier Name'></cc1:ucLabel>
                                                </th>
                                                <asp:Repeater ID="rptSiteExport" runat="server" OnItemDataBound="rptSite_OnItemDataBound">
                                                    <ItemTemplate>
                                                        <th style="text-align: right;">
                                                            <table border="0">
                                                                <tr>
                                                                    <td align="center" runat="server" id="tdControlSite" style="background-color:#DBDBDB; font-weight:bold;">
                                                                        <asp:Literal ID="ltSitePreFix" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "SiteName") %>'></asp:Literal>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <asp:Repeater ID="rptKeyHeader" runat="server">
                                                                        <ItemTemplate>
                                                                            <td align="right" runat="server" id="tdControl" style="background-color:#DBDBDB; font-weight:bold;">
                                                                                <asp:Literal ID="ltKeyHeaderText" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "KeyHeader") %>'></asp:Literal>
                                                                            </td>
                                                                        </ItemTemplate>
                                                                    </asp:Repeater>
                                                                </tr>
                                                            </table>
                                                        </th>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </tr>
                                            <asp:Repeater ID="rptkeyDetailsExport" runat="server" OnItemDataBound="rptkeyDetails_OnItemDataBound">
                                                <ItemTemplate>
                                                    <tr>
                                                        <td style="height: 20px; width: 150px;">
                                                            <asp:Literal ID="ltVendorNameValue" runat="server" Text='<%#  DataBinder.Eval(Container.DataItem, "VendorName") %>'></asp:Literal>
                                                            <asp:Literal ID="ltVendorIDValue" runat="server" Text='<%#  DataBinder.Eval(Container.DataItem, "VendorID") %>'
                                                                Visible="false">
                                                            </asp:Literal>
                                                        </td>
                                                        <asp:Repeater ID="rptKeyvalue" runat="server" OnItemDataBound="rptKeyvalue_OnItemDataBound">
                                                            <ItemTemplate>
                                                                <td style="text-align: right;">
                                                                    <table border="0">
                                                                        <tr>
                                                                            <asp:Repeater ID="rptKeyValueDetails" runat="server">
                                                                                <ItemTemplate>
                                                                                    <td align="right" runat="server" id="tdControl" style="height: 20px;">
                                                                                        <asp:Literal ID="ltKeyvalue" runat="server" Text='<%#  DataBinder.Eval(Container.DataItem, "Keyvalue") %>'></asp:Literal>
                                                                                    </td>
                                                                                </ItemTemplate>
                                                                            </asp:Repeater>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </ItemTemplate>
                                                        </asp:Repeater>
                                                    </tr>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                            <%--<div class="button-row"></div>--%>
                        </cc1:ucPanel>
                    </cc1:ucPanel>
                </cc1:ucView>
                <cc1:ucView ID="vwDetailView" runat="server">
                    <div class="button-row">
                        <%--<uc1:ucExportButton ID="btnExportToExcelDetailByReceipt" runat="server" />--%>
                        <uc1:ucExportButton ID="btnExportToExcelProvBookingsDetail" runat="server" Visible="false" />
                    </div>
                    <cc1:ucPanel ID="pnlDetailByReceipt" runat="server" CssClass="fieldset-form">
                        <cc1:ucGridView ID="gvProvisionalBookingDetail" Width="950" runat="server" CssClass="grid"
                            OnSorting="SortGrid" PageSize="50" AllowPaging="true" PagerStyle-HorizontalAlign="Left"
                            AllowSorting="true" Style="overflow: auto;" OnPageIndexChanging="gvProvisionalBookingDetail_PageIndexChanging">
                            <EmptyDataTemplate>
                                <div style="text-align: center">
                                    <cc1:ucLabel ID="lblRecordNotFound" isRequired="true" runat="server" Text="No record found"></cc1:ucLabel>
                                </div>
                            </EmptyDataTemplate>
                            <Columns>
                                <asp:TemplateField HeaderText="Vendor">
                                    <HeaderStyle Width="150px" HorizontalAlign="Left" />
                                    <ItemStyle HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <cc1:ucLiteral ID="lblVendorNameValue" runat="server" Text='<%#Eval("Vendor.VendorName") %>'></cc1:ucLiteral>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Carrier">
                                    <HeaderStyle HorizontalAlign="Left" Width="150px" />
                                    <ItemStyle HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <cc1:ucLiteral ID="lblCarrierNameValue" runat="server" Text='<%#Eval("Carrier.CarrierName") %>'></cc1:ucLiteral>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="BookingReference">
                                    <HeaderStyle HorizontalAlign="Left" Width="150px" />
                                    <ItemStyle HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <cc1:ucLiteral ID="lblBookingReferenceValue" runat="server" Text='<%#Eval("BookingRef") %>'></cc1:ucLiteral>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="BookedBy">
                                    <HeaderStyle HorizontalAlign="Left" Width="150px" />
                                    <ItemStyle HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <cc1:ucLiteral ID="lblProvBookedById" runat="server" Text='<%#Eval("ProvBookedById") %>'
                                            Visible="false"></cc1:ucLiteral>
                                        <cc1:ucLiteral ID="lblProvBookedByValue" runat="server" Text='<%#Eval("ProvBookedBy") %>'></cc1:ucLiteral>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Type">
                                    <HeaderStyle HorizontalAlign="Center" Width="100px" />
                                    <ItemStyle HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <cc1:ucLiteral ID="lblProvTypeValue" runat="server" Text='<%#Eval("ProvType") %>'></cc1:ucLiteral>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Action">
                                    <HeaderStyle HorizontalAlign="Center" Width="100px" />
                                    <ItemStyle HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <cc1:ucLiteral ID="lblProvActionValue" runat="server" Text='<%#Eval("ProvAction") %>'></cc1:ucLiteral>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="ODUser">
                                    <HeaderStyle HorizontalAlign="Left" Width="150px" />
                                    <ItemStyle HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <cc1:ucLiteral ID="lblProvODUserId" runat="server" Text='<%#Eval("ProvODUserId") %>'
                                            Visible="false"></cc1:ucLiteral>
                                        <cc1:ucLiteral ID="lblProvODUserValue" runat="server" Text='<%#Eval("ProvODUser") %>'></cc1:ucLiteral>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </cc1:ucGridView>
                        <cc1:ucGridView ID="gvProvisionalBookingDetailExcel" Width="100%" runat="server"
                            CssClass="grid" Visible="false">
                            <EmptyDataTemplate>
                                <div style="text-align: center">
                                    <cc1:ucLabel ID="lblRecordNotFound" isRequired="true" runat="server" Text="No record found"></cc1:ucLabel>
                                </div>
                            </EmptyDataTemplate>
                            <Columns>
                                <asp:TemplateField HeaderText="Vendor">
                                    <HeaderStyle Width="180px" HorizontalAlign="Left" />
                                    <ItemStyle HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <cc1:ucLiteral ID="lblVendorNameValue" runat="server" Text='<%#Eval("Vendor.VendorName") %>'></cc1:ucLiteral>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Carrier">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <cc1:ucLiteral ID="lblCarrierNameValue" runat="server" Text='<%#Eval("Carrier.CarrierName") %>'></cc1:ucLiteral>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="BookingReference">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <cc1:ucLiteral ID="lblBookingReferenceValue" runat="server" Text='<%#Eval("BookingRef") %>'></cc1:ucLiteral>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="BookedBy">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <cc1:ucLiteral ID="lblProvBookedByValue" runat="server" Text='<%#Eval("ProvBookedBy") %>'></cc1:ucLiteral>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Type">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <cc1:ucLiteral ID="lblProvTypeValue" runat="server" Text='<%#Eval("ProvType") %>'></cc1:ucLiteral>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Action">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <cc1:ucLiteral ID="lblProvActionValue" runat="server" Text='<%#Eval("ProvAction") %>'></cc1:ucLiteral>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="ODUser">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <cc1:ucLiteral ID="lblProvODUserValue" runat="server" Text='<%#Eval("ProvODUser") %>'></cc1:ucLiteral>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </cc1:ucGridView>
                        <%--<asp:Repeater ID="rptPOReceipts" runat="server">
                            <ItemTemplate>
                                <table width="100%" cellspacing="2" cellpadding="0" class="form-table">
                                    <tr>
                                        <td style="width: 100%;" align="left" valign="top">
                                            <cc1:ucLabel ID="lblDate" Text="Date" runat="server"></cc1:ucLabel>&nbsp;-
                                            <cc1:ucLabel ID="lblDateText" Text='<%# Eval("ReceiptDate", "{0:dd/MM/yyyy}") %>'
                                                runat="server"></cc1:ucLabel>
                                            <asp:HiddenField ID="hdnDateText" Value='<%# Bind("ReceiptDate") %>' runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 100%;" align="left" valign="top">
                                            <cc1:ucGridView ID="gvPOReceiptsDetails" runat="server" AutoGenerateColumns="false"
                                                CssClass="grid" Width="100%">
                                                <AlternatingRowStyle BackColor="White" ForeColor="#284775"></AlternatingRowStyle>
                                                <EmptyDataTemplate>
                                                    <div style="text-align: center">
                                                        <cc1:ucLabel ID="lblRecordNotFound" isRequired="true" runat="server" Text="No record found"></cc1:ucLabel>
                                                    </div>
                                                </EmptyDataTemplate>
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Line" SortExpression="Line_No">
                                                        <HeaderStyle Width="100px" HorizontalAlign="Center" />
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        <ItemTemplate>
                                                            <cc1:ucLabel ID="lblLineText" Text='<%# Bind("Line_No") %>' runat="server"></cc1:ucLabel>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="SKU" SortExpression="ODSKU_No">
                                                        <HeaderStyle Width="100px" HorizontalAlign="Center" />
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        <ItemTemplate>
                                                            <cc1:ucLabel ID="lblSKUText" Text='<%# Bind("ODSKU_No") %>' runat="server"></cc1:ucLabel>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="VendorCode" SortExpression="Vendor_Code">
                                                        <HeaderStyle Width="100px" HorizontalAlign="Center" />
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        <ItemTemplate>
                                                            <cc1:ucLabel ID="lblVendorCodeText" Text='<%# Bind("Vendor_Code") %>' runat="server"></cc1:ucLabel>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Description1" SortExpression="ProductDescription">
                                                        <HeaderStyle Width="200px" HorizontalAlign="Center" />
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        <ItemTemplate>
                                                            <cc1:ucLabel ID="lblDescriptionText" Text='<%# Bind("ProductDescription") %>' runat="server"></cc1:ucLabel>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="QtyReceipted" SortExpression="Qty_receipted">
                                                        <HeaderStyle Width="100px" HorizontalAlign="Center" />
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        <ItemTemplate>
                                                            <cc1:ucLabel ID="lblQtyReceiptedText" Text='<%# Bind("Qty_receipted") %>' runat="server"></cc1:ucLabel>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                                            </cc1:ucGridView>
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                        </asp:Repeater>--%>
                    </cc1:ucPanel>
                </cc1:ucView>
            </cc1:ucMultiView>
            <table style="width: 100%;">
                <tr>
                    <td align="right">
                        <div class="button-row">
                            <cc1:ucButton ID="btnBackSearch" runat="server" Text="Back To Search Page" CssClass="button"
                                OnClick="btnBackSearch_Click" Visible="false" />
                        </div>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>

﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    AutoEventWireup="true" CodeFile="DeliveriesPerPurchaseOrderReport.aspx.cs" Inherits="DeliveriesPerPurchaseOrderReport" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register TagName="ucDate" TagPrefix="cc2" Src="~/CommonUI/UserControls/ucDate.ascx" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectSite.ascx" TagName="MultiSelectSite"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectVendor.ascx" TagName="MultiSelectVendor"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectCountry.ascx" TagName="MultiSelectCountry"
    TagPrefix="cc1" %>
<%@ Register TagName="ucExportButton" TagPrefix="uc1" Src="~/CommonUI/UserControls/ucExportToExcel.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript" language="javascript">
        $(document).ready(function () {
            $("body").keypress(function (e) {
                if (e.which == 13 || e.keyCode == 13) {
                    $('#<%=btnSearch.ClientID %>').click();
                    return false;
                }
            });
        });
        function setValue1(target) {
            document.getElementById('<%=hdnJSFromDt.ClientID %>').value = target.value;
        }

        function setValue2(target) {
            document.getElementById('<%=hdnJSToDt.ClientID %>').value = target.value;
        }

    </script>
    <%-- <style type="text/css">
        .customlink
        {
            color: #333333 !important;
        }
        .customlink:hover
        {
            text-decoration: none !important;
        }
    </style>--%>
    <h2>
        <cc1:ucLabel ID="UcLabel5" runat="server"></cc1:ucLabel>
        <cc1:ucLabel ID="lblDeliveriesPerPurchaseOrderReport" Text="Deliveries Per Purchase Order Report"
            runat="server"></cc1:ucLabel>
    </h2>
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:HiddenField ID="hdnJSFromDt" runat="server" />
    <asp:HiddenField ID="hdnJSToDt" runat="server" />
    <div class="right-shadow">
        <div class="formbox">
            <cc1:ucMultiView ID="mvDiscrepancyList" runat="server" ActiveViewIndex="0">
                <cc1:ucView ID="vwSearchCreteria" runat="server">
                    <cc1:ucPanel ID="UcCarrierVendorPanel" runat="server">
                        <table width="100%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                            <tr>
                                <td style="font-weight: bold; width: 10%;">
                                    <cc1:ucLabel ID="lblCountry" runat="server" Text="Country"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold; width: 2%;">
                                    <cc1:ucLabel ID="UcLabel2" runat="server">:</cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold; width: 88%;">
                                    <cc1:MultiSelectCountry runat="server" ID="msCountry" />
                                </td>
                            </tr>
                            <tr>
                                <td style="font-weight: bold;">
                                    <cc1:ucLabel ID="lblSite" runat="server" Text="Site"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold;">
                                    <cc1:ucLabel ID="UcLabel4" runat="server">:</cc1:ucLabel>
                                </td>
                                <td>
                                    <cc1:MultiSelectSite runat="server" ID="msSite" />
                                </td>
                            </tr>
                            <tr>
                                <td style="font-weight: bold;">
                                    <cc1:ucLabel ID="lblVendor" runat="server" Text="Vendor"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold;">
                                    <cc1:ucLabel ID="lblVendorCollon" runat="server">:</cc1:ucLabel>
                                </td>
                                <td>
                                    <cc1:MultiSelectVendor runat="server" ID="msVendor" />
                                </td>
                            </tr>
                            <tr>
                                <td style="font-weight: bold;">
                                    <cc1:ucLabel ID="lblFrom" runat="server" Text="From"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold;">
                                    <cc1:ucLabel ID="UcLabel1" runat="server">:</cc1:ucLabel>
                                </td>
                                <td align="left">
                                    <span style="font-weight: bold; width: 6em; margin-left: 4px;">
                                        <cc1:ucTextbox ID="txtFromDate" runat="server" ClientIDMode="Static" CssClass="date"
                                            onchange="setValue1(this)" ReadOnly="True" Width="70px" /></span><span style="font-weight: bold;
                                                width: 4em; text-align: center"><cc1:ucLabel ID="lblTo" runat="server" Text="To"></cc1:ucLabel></span><span
                                                    style="font-weight: bold; width: 6em;"><cc1:ucTextbox ID="txtToDate" runat="server"
                                                        ClientIDMode="Static" CssClass="date" onchange="setValue2(this)" ReadOnly="True"
                                                        Width="70px" /></span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td>
                                </td>
                                <td align="left" class="nobold radiobuttonlist">
                                    <cc1:ucRadioButtonList ID="rblSearchOption" CssClass="radio-fix" runat="server" RepeatColumns="3"
                                        RepeatDirection="Horizontal">
                                    </cc1:ucRadioButtonList>
                                </td>
                            </tr>
                            <tr>
                                <td align="right" colspan="3">
                                    <div class="button-row">
                                        <cc1:ucButton ID="btnSearch" runat="server" Text="Search" CssClass="button" OnClick="btnSearch_Click" /></div>
                                </td>
                            </tr>
                        </table>
                    </cc1:ucPanel>
                </cc1:ucView>
                <cc1:ucView ID="vwSummary" runat="server">
                    <div class="button-row">
                        <uc1:ucExportButton ID="btnExportToExcelSummary" runat="server" />
                    </div>
                    <cc1:ucPanel ID="pnlSummary" runat="server" ScrollBars="Both" CssClass="fieldset-form">
                        <cc1:ucGridView ID="gvSummary" Width="100%" runat="server" CssClass="grid" OnSorting="SortGrid"
                            PageSize="50" AllowPaging="true" PagerStyle-HorizontalAlign="Left" AllowSorting="true"
                            Style="overflow: auto;" OnPageIndexChanging="gvSummary_PageIndexChanging">
                            <EmptyDataTemplate>
                                <div style="text-align: center">
                                    <cc1:ucLabel ID="lblRecordNotFound" isRequired="true" runat="server" Text="No record found"></cc1:ucLabel></div>
                            </EmptyDataTemplate>
                            <Columns>
                                <asp:TemplateField HeaderText="Site">
                                    <HeaderStyle Width="180px" HorizontalAlign="Left" />
                                    <ItemStyle HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <cc1:ucLiteral ID="lblSiteValue" runat="server" Text='<%#Eval("Site.SiteName") %>'></cc1:ucLiteral></ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Vendor">
                                    <HeaderStyle Width="180px" HorizontalAlign="Left" />
                                    <ItemStyle HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <cc1:ucLiteral ID="lblVendorValue" runat="server" Text='<%#Eval("Vendor.VendorName") %>'></cc1:ucLiteral></ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="HashPOsDue">
                                    <HeaderStyle Width="180px" HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <cc1:ucLiteral ID="lblHashPOsDueValue" runat="server" Text='<%#Eval("PODue") %>'></cc1:ucLiteral></ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="HashPOsDelivered">
                                    <HeaderStyle Width="180px" HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <cc1:ucLiteral ID="lblHashPOsDeliveredValue" runat="server" Text='<%#Eval("PODelivered") %>'></cc1:ucLiteral></ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="HashReceipts">
                                    <HeaderStyle Width="180px" HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <cc1:ucLiteral ID="lblHashReceiptsValue" runat="server" Text='<%#Eval("Receipts") %>'></cc1:ucLiteral></ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="ReceiptsPerDeliveredPO">
                                    <HeaderStyle Width="180px" HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <cc1:ucLiteral ID="lblReceiptsPerDeliveredPOValue" runat="server" Text='<%#Eval("ReceiptsPerDeliveredPO") %>'></cc1:ucLiteral></ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </cc1:ucGridView>
                        <cc1:ucGridView ID="gvSummaryExcel" Width="100%" runat="server" CssClass="grid" Visible="false">
                            <EmptyDataTemplate>
                                <div style="text-align: center">
                                    <cc1:ucLabel ID="lblRecordNotFound" isRequired="true" runat="server" Text="No record found"></cc1:ucLabel></div>
                            </EmptyDataTemplate>
                            <Columns>
                                <asp:TemplateField HeaderText="Site">
                                    <HeaderStyle Width="180px" HorizontalAlign="Left" />
                                    <ItemStyle HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <cc1:ucLiteral ID="lblSiteValue" runat="server" Text='<%#Eval("Site.SiteName") %>'></cc1:ucLiteral></ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Vendor">
                                    <HeaderStyle Width="180px" HorizontalAlign="Left" />
                                    <ItemStyle HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <cc1:ucLiteral ID="lblVendorValue" runat="server" Text='<%#Eval("Vendor.VendorName") %>'></cc1:ucLiteral></ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="HashPOsDue">
                                    <HeaderStyle Width="180px" HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <cc1:ucLiteral ID="lblHashPOsDueValue" runat="server" Text='<%#Eval("PODue") %>'></cc1:ucLiteral></ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="HashPOsDelivered">
                                    <HeaderStyle Width="180px" HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <cc1:ucLiteral ID="lblHashPOsDeliveredValue" runat="server" Text='<%#Eval("PODelivered") %>'></cc1:ucLiteral></ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="HashReceipts">
                                    <HeaderStyle Width="180px" HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <cc1:ucLiteral ID="lblHashReceiptsValue" runat="server" Text='<%#Eval("Receipts") %>'></cc1:ucLiteral></ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="ReceiptsPerDeliveredPO">
                                    <HeaderStyle Width="180px" HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <cc1:ucLiteral ID="lblReceiptsPerDeliveredPOValue" runat="server" Text='<%#Eval("ReceiptsPerDeliveredPO") %>'></cc1:ucLiteral></ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </cc1:ucGridView>
                    </cc1:ucPanel>
                </cc1:ucView>
                <cc1:ucView ID="vwDetailByReceipt" runat="server">
                    <div class="button-row">
                        <uc1:ucExportButton ID="btnExportToExcelDetailByReceipt" runat="server" />
                    </div>
                    <cc1:ucPanel ID="pnlDetailByReceipt" runat="server" ScrollBars="Both" CssClass="fieldset-form">
                        <cc1:ucGridView ID="gvDetailByReceipt" Width="100%" runat="server" CssClass="grid"
                            OnSorting="SortGrid" PageSize="50" AllowPaging="true" PagerStyle-HorizontalAlign="Left"
                            AllowSorting="true" Style="overflow: auto;" OnPageIndexChanging="gvDetailByReceipt_PageIndexChanging"
                            OnRowCommand="gvDetailByReceipt_RowCommand" OnRowDataBound="gvDetailByReceipt_RowDataBound">
                            <EmptyDataTemplate>
                                <div style="text-align: center">
                                    <cc1:ucLabel ID="lblRecordNotFound" isRequired="true" runat="server" Text="No record found"></cc1:ucLabel></div>
                            </EmptyDataTemplate>
                            <Columns>
                                <asp:TemplateField HeaderText="Site">
                                    <HeaderStyle Width="180px" HorizontalAlign="Left" />
                                    <ItemStyle HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <asp:HiddenField ID="hdnSiteIDValue" runat="server" Value='<%#Eval("Site.SiteID") %>'>
                                        </asp:HiddenField>
                                        <cc1:ucLiteral ID="lblSiteValue" runat="server" Text='<%#Eval("Site.SiteName") %>'></cc1:ucLiteral></ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Vendor">
                                    <HeaderStyle Width="180px" HorizontalAlign="Left" />
                                    <ItemStyle HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <asp:HiddenField ID="hdnVendorIDValue" runat="server" Value='<%#Eval("Vendor.VendorID") %>'>
                                        </asp:HiddenField>
                                        <cc1:ucLiteral ID="lblVendorValue" runat="server" Text='<%#Eval("Vendor.VendorName") %>'></cc1:ucLiteral></ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="POHash">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <cc1:ucLinkButton ID="lnkPOHashValue" Text='<%#Eval("Purchase_order") %>' runat="server"
                                            ForeColor="#026DCF" Font-Bold="true" CommandName="ReceiptInfo"></cc1:ucLinkButton><cc1:ucLiteral
                                                ID="lblPOHashValue" runat="server" Text='<%#Eval("Purchase_order") %>'></cc1:ucLiteral></ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="OriginalDueDate">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <cc1:ucLiteral ID="lblOriginalDueDateValue" runat="server" Text='<%#Eval("Original_due_date", "{0:dd/MM/yyyy}") %>'></cc1:ucLiteral></ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="ReceiptDate">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <cc1:ucLiteral ID="lblReceiptDateValue" runat="server" Text='<%#Eval("ReceiptDate", "{0:dd/MM/yyyy}") %>'></cc1:ucLiteral></ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </cc1:ucGridView>
                        <cc1:ucGridView ID="gvDetailByReceiptExcel" Width="100%" runat="server" CssClass="grid"
                            Visible="false">
                            <EmptyDataTemplate>
                                <div style="text-align: center">
                                    <cc1:ucLabel ID="lblRecordNotFound" isRequired="true" runat="server" Text="No record found"></cc1:ucLabel></div>
                            </EmptyDataTemplate>
                            <Columns>
                                <asp:TemplateField HeaderText="Site">
                                    <HeaderStyle Width="180px" HorizontalAlign="Left" />
                                    <ItemStyle HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <asp:HiddenField ID="hdnSiteIDValue" runat="server" Value='<%#Eval("Site.SiteID") %>'>
                                        </asp:HiddenField>
                                        <cc1:ucLiteral ID="lblSiteValue" runat="server" Text='<%#Eval("Site.SiteName") %>'></cc1:ucLiteral></ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Vendor">
                                    <HeaderStyle Width="180px" HorizontalAlign="Left" />
                                    <ItemStyle HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <asp:HiddenField ID="hdnVendorIDValue" runat="server" Value='<%#Eval("Vendor.VendorID") %>'>
                                        </asp:HiddenField>
                                        <cc1:ucLiteral ID="lblVendorValue" runat="server" Text='<%#Eval("Vendor.VendorName") %>'></cc1:ucLiteral></ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="POHash">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <cc1:ucLiteral ID="lblPOHashValue" runat="server" Text='<%#Eval("Purchase_order") %>'></cc1:ucLiteral></ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="OriginalDueDate">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <cc1:ucLiteral ID="lblOriginalDueDateValue" runat="server" Text='<%#Eval("Original_due_date", "{0:dd/MM/yyyy}") %>'></cc1:ucLiteral></ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="ReceiptDate">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <cc1:ucLiteral ID="lblReceiptDateValue" runat="server" Text='<%#Eval("ReceiptDate", "{0:dd/MM/yyyy}") %>'></cc1:ucLiteral></ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </cc1:ucGridView>
                        <asp:Repeater ID="rptPOReceipts" runat="server">
                            <ItemTemplate>
                                <table width="100%" cellspacing="2" cellpadding="0" class="form-table">
                                    <tr>
                                        <td style="width: 100%;" align="left" valign="top">
                                            <cc1:ucLabel ID="lblDate" Text="Date" runat="server"></cc1:ucLabel>&nbsp;-
                                            <cc1:ucLabel ID="lblDateText" Text='<%# Eval("ReceiptDate", "{0:dd/MM/yyyy}") %>'
                                                runat="server"></cc1:ucLabel><asp:HiddenField ID="hdnDateText" Value='<%# Bind("ReceiptDate") %>'
                                                    runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 100%;" align="left" valign="top">
                                            <cc1:ucGridView ID="gvPOReceiptsDetails" runat="server" AutoGenerateColumns="false"
                                                CssClass="grid" Width="100%">
                                                <AlternatingRowStyle BackColor="White" ForeColor="#284775"></AlternatingRowStyle>
                                                <EmptyDataTemplate>
                                                    <div style="text-align: center">
                                                        <cc1:ucLabel ID="lblRecordNotFound" isRequired="true" runat="server" Text="No record found"></cc1:ucLabel></div>
                                                </EmptyDataTemplate>
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Line" SortExpression="Line_No">
                                                        <HeaderStyle Width="100px" HorizontalAlign="Center" />
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        <ItemTemplate>
                                                            <cc1:ucLabel ID="lblLineText" Text='<%# Bind("Line_No") %>' runat="server"></cc1:ucLabel></ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="SKU" SortExpression="ODSKU_No">
                                                        <HeaderStyle Width="100px" HorizontalAlign="Center" />
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        <ItemTemplate>
                                                            <cc1:ucLabel ID="lblSKUText" Text='<%# Bind("ODSKU_No") %>' runat="server"></cc1:ucLabel></ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="VendorCode" SortExpression="Vendor_Code">
                                                        <HeaderStyle Width="100px" HorizontalAlign="Center" />
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        <ItemTemplate>
                                                            <cc1:ucLabel ID="lblVendorCodeText" Text='<%# Bind("Vendor_Code") %>' runat="server"></cc1:ucLabel></ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Description1" SortExpression="ProductDescription">
                                                        <HeaderStyle Width="200px" HorizontalAlign="Center" />
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        <ItemTemplate>
                                                            <cc1:ucLabel ID="lblDescriptionText" Text='<%# Bind("ProductDescription") %>' runat="server"></cc1:ucLabel></ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="QtyReceipted" SortExpression="Qty_receipted">
                                                        <HeaderStyle Width="100px" HorizontalAlign="Center" />
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        <ItemTemplate>
                                                            <cc1:ucLabel ID="lblQtyReceiptedText" Text='<%# Bind("Qty_receipted") %>' runat="server"></cc1:ucLabel></ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                                            </cc1:ucGridView>
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                        </asp:Repeater>
                    </cc1:ucPanel>
                </cc1:ucView>
                <cc1:ucView ID="vwDetailByBooking" runat="server">
                    <div class="button-row">
                        <uc1:ucExportButton ID="btnExportToExcelDetailByBooking" runat="server" />
                    </div>
                    <cc1:ucPanel ID="pnlDetailByBooking" runat="server" ScrollBars="Both" CssClass="fieldset-form">
                        <cc1:ucGridView ID="gvDetailByBooking" Width="100%" runat="server" CssClass="grid"
                            OnSorting="SortGrid" PageSize="50" AllowPaging="true" PagerStyle-HorizontalAlign="Left"
                            AllowSorting="true" Style="overflow: auto;" OnPageIndexChanging="gvDetailByBooking_PageIndexChanging">
                            <EmptyDataTemplate>
                                <div style="text-align: center">
                                    <cc1:ucLabel ID="lblRecordNotFound" isRequired="true" runat="server" Text="No record found"></cc1:ucLabel></div>
                            </EmptyDataTemplate>
                            <Columns>
                                <asp:TemplateField HeaderText="Site">
                                    <HeaderStyle Width="180px" HorizontalAlign="Left" />
                                    <ItemStyle HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <cc1:ucLiteral ID="lblSiteValue" runat="server" Text='<%#Eval("Site.SiteName") %>'></cc1:ucLiteral></ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Vendor">
                                    <HeaderStyle Width="180px" HorizontalAlign="Left" />
                                    <ItemStyle HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <cc1:ucLiteral ID="lblVendorValue" runat="server" Text='<%#Eval("Vendor.VendorName") %>'></cc1:ucLiteral></ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="POHash">
                                    <HeaderStyle Width="180px" HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <cc1:ucLiteral ID="lblPOHashValue" runat="server" Text='<%#Eval("Purchase_order") %>'></cc1:ucLiteral></ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="OriginalDueDate">
                                    <HeaderStyle Width="180px" HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <cc1:ucLiteral ID="lblDueDateValue" runat="server" Text='<%#Eval("Original_due_date", "{0:dd/MM/yyyy}") %>'></cc1:ucLiteral></ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="BookingReference">
                                    <HeaderStyle Width="180px" HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <asp:HiddenField ID="hdnBookingID" Value='<%# Eval("Booking.BookingID") %>' runat="server" />
                                        <asp:HyperLink ID="hpBookingRefPOInq" runat="server" Text='<%# Eval("Booking.BookingRef") %>'
                                            Target="_blank" NavigateUrl='<%# EncryptQuery("~/ModuleUI/Appointment/Receiving/APPRcv_BookingHistory.aspx?Scheduledate=" + Eval("Booking.ScheduleDate", "{0:dd/MM/yyyy}") + "&ID=BK-" + Eval("Booking.SupplierType") + "-" + Eval("Booking.BookingID") + "-" + Eval("Site.SiteID") + "&PN=DelPerPORpt&PO=" + Eval("Purchase_order"))  %>'></asp:HyperLink></ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Date">
                                    <HeaderStyle Width="180px" HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <cc1:ucLiteral ID="lblDateValue" runat="server" Text='<%#Eval("Booking.ScheduleDate", "{0:dd/MM/yyyy}") %>'></cc1:ucLiteral></ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="DayTime">
                                    <HeaderStyle Width="180px" HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <cc1:ucLiteral ID="lblTimeDayValue" runat="server" Text='<%#Eval("Booking.SlotTime.SlotTime") %>'></cc1:ucLiteral></ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </cc1:ucGridView>
                        <cc1:ucGridView ID="gvDetailByBookingExcel" Width="100%" runat="server" CssClass="grid"
                            Visible="false">
                            <EmptyDataTemplate>
                                <div style="text-align: center">
                                    <cc1:ucLabel ID="lblRecordNotFound" isRequired="true" runat="server" Text="No record found"></cc1:ucLabel></div>
                            </EmptyDataTemplate>
                            <Columns>
                                <asp:TemplateField HeaderText="Site">
                                    <HeaderStyle Width="180px" HorizontalAlign="Left" />
                                    <ItemStyle HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <cc1:ucLiteral ID="lblSiteValue" runat="server" Text='<%#Eval("Site.SiteName") %>'></cc1:ucLiteral></ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Vendor">
                                    <HeaderStyle Width="180px" HorizontalAlign="Left" />
                                    <ItemStyle HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <cc1:ucLiteral ID="lblVendorValue" runat="server" Text='<%#Eval("Vendor.VendorName") %>'></cc1:ucLiteral></ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="POHash">
                                    <HeaderStyle Width="180px" HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <cc1:ucLiteral ID="lblPOHashValue" runat="server" Text='<%#Eval("Purchase_order") %>'></cc1:ucLiteral></ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="OriginalDueDate">
                                    <HeaderStyle Width="180px" HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <cc1:ucLiteral ID="lblDueDateValue" runat="server" Text='<%#Eval("Original_due_date", "{0:dd/MM/yyyy}") %>'></cc1:ucLiteral></ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="BookingReference">
                                    <HeaderStyle Width="180px" HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <asp:HiddenField ID="hdnBookingID" Value='<%# Eval("Booking.BookingID") %>' runat="server" />
                                        <asp:HyperLink ID="hpBookingRefPOInq" runat="server" Text='<%# Eval("Booking.BookingRef") %>'
                                            Target="_blank" NavigateUrl='<%# EncryptQuery("~/ModuleUI/Appointment/Receiving/APPRcv_BookingHistory.aspx?Scheduledate=" + Eval("Booking.ScheduleDate") + "&ID=BK-" + Eval("Booking.SupplierType") + "-" + Eval("Booking.BookingID") + "-" + Eval("Site.SiteID") + "&PN=DelPerPORpt&PO=" + Eval("Purchase_order"))  %>'></asp:HyperLink></ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Date">
                                    <HeaderStyle Width="180px" HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <cc1:ucLiteral ID="lblDateValue" runat="server" Text='<%#Eval("Booking.ScheduleDate", "{0:dd/MM/yyyy}") %>'></cc1:ucLiteral></ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="DayTime">
                                    <HeaderStyle Width="180px" HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <cc1:ucLiteral ID="lblTimeDayValue" runat="server" Text='<%#Eval("Booking.SlotTime.SlotTime") %>'></cc1:ucLiteral></ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </cc1:ucGridView>
                    </cc1:ucPanel>
                </cc1:ucView>
            </cc1:ucMultiView>
            <table style="width: 100%;">
                <tr>
                    <td align="right">
                        <div class="button-row">
                            <cc1:ucButton ID="btnBackSearch" runat="server" Text="Back To Search Page" CssClass="button"
                                OnClick="btnBackSearch_Click" />
                            <cc1:ucButton ID="btnBack" runat="server" Text="Back" CssClass="button" OnClick="btnBack_Click"
                                Visible="false" />
                        </div>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>

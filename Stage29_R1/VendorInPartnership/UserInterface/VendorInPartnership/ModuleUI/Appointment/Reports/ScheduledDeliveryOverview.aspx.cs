﻿using System;
using Microsoft.Reporting.WebForms;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using Utilities; using WebUtilities;
using System.Web.UI;
using BusinessLogicLayer.ModuleBAL.Security;
using BusinessEntities.ModuleBE.Security;
using System.Collections.Generic;

public partial class ModuleUI_Appointment_Reports_ScheduledDeliveryOverview : CommonPage
{
    protected void Page_Init(object sender, EventArgs e)
    {
        ucVendorTemplateSelect.CurrentPage = this;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        btnExportToExcel1.GridViewControl = UcGridView2;
        btnExportToExcel1.FileName = "ScheduledDeliveryOverview";
        TextBox txtVendor = Utilities.Common.FindControlRecursive(this.Page.Controls[0], "txtVendorNo") as TextBox;
        if (txtVendor != null)
            txtVendor.Focus();
    }

    /// <summary>
    /// Method to show Vendor on the basis of selected Template
    /// </summary>
    public override void TemplateSelectedIndexChanged()
    {
        base.TemplateSelectedIndexChanged();

        int VendorTemplateId = Convert.ToInt32(ucVendorTemplateSelect.innerControlddlTemplate.SelectedValue);

        ListBox lstSelectedVendor = (ListBox)msVendor.FindControl("lstRight");
        ListBox lstVendor = (ListBox)msVendor.FindControl("ucVendor").FindControl("lstLeft");

        if (VendorTemplateId > 0)
        {

            SCT_TemplateBAL oSCT_TemplateBAL = new SCT_TemplateBAL();
            SCT_TemplateBE oSCT_TemplateBE = new SCT_TemplateBE();

            oSCT_TemplateBE.VendorTemplateID = VendorTemplateId;
            oSCT_TemplateBE.Action = "GetVendorTemplateInfobyID";
            List<SCT_TemplateBE> lstVendorTemplate = oSCT_TemplateBAL.GetVendorTemplateById(oSCT_TemplateBE);

            if (lstVendorTemplate != null && lstVendorTemplate.Count > 0)
            {
                lstSelectedVendor.Items.Clear();
                msVendor.innerControlHiddenField.Value = string.Empty;
                msVendor.innerControlHiddenField2.Value = string.Empty;
                for (int iCount = 0; iCount < lstVendorTemplate.Count; iCount++)
                {
                    lstSelectedVendor.Items.Add(new ListItem(lstVendorTemplate[iCount].Vendor.Vendor_Name, lstVendorTemplate[iCount].Vendor.VendorID.ToString()));
                    msVendor.SelectedVendorIDs = lstVendorTemplate[iCount].Vendor.VendorID.ToString();
                    msVendor.SelectedVendorName = lstVendorTemplate[iCount].Vendor.Vendor_Name.ToString();
                }
            }
        }
        else
        {
            lstSelectedVendor.Items.Clear();
            msVendor.innerControlHiddenField.Value = string.Empty;
            msVendor.innerControlHiddenField2.Value = string.Empty;
        }
    }

    protected void btnGenerateReport_Click(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(msSite.SelectedSiteIDs))
            hdnSiteIDs.Value = msSite.SelectedSiteIDs;
        if (!string.IsNullOrEmpty(msCountry.SelectedCountryIDs))
            hdnCountryIDs.Value = msCountry.SelectedCountryIDs;
        if (!string.IsNullOrEmpty(msVendor.SelectedVendorIDs))
            hdnVendorIDs.Value = msVendor.SelectedVendorIDs;
        if (!string.IsNullOrEmpty(msStockPlanner.SelectedStockPlannerIDs))
            hdnInventoryMnagerIDs.Value = msStockPlanner.SelectedStockPlannerIDs;
        if (rdoConfirmedBooking.Checked == true)
            hdnBooking.Value = "Confirmed";
        else if (rdoUnconfirmedBooking.Checked == true)
            hdnBooking.Value = "NotConfirmed";
        if (!string.IsNullOrEmpty(txtPurchaseOrderNo.Text))
            hdnPurchaseOrder.Value = txtPurchaseOrderNo.Text;

        // We donot know about what has to be done with the radio button information 

        ReportParameter[] reportParameter = new ReportParameter[5];
        reportParameter[0] = new ReportParameter("Country", msCountry.SelectedCountryName);
        reportParameter[1] = new ReportParameter("Site", msSite.SelectedSiteName);
        reportParameter[2] = new ReportParameter("Vendor", msVendor.SelectedVendorName);
        reportParameter[3] = new ReportParameter("InventoryManager", msStockPlanner.SelectedSPName);
        reportParameter[4] = new ReportParameter("PurchaseOrderNo", txtPurchaseOrderNo.Text);
        this.ScheduledDeliveryOverviewReportViewer.LocalReport.SetParameters(reportParameter);


       

        if (rdoConfirmedBooking.Checked) {
            UcCarrierVendorPanel.Visible = false;
            UcCarrierVendorReportViewPanel.Visible = true;
            ucpnlnotConfirmed.Visible = false;
            DataSourceSelectArguments arg = new DataSourceSelectArguments();
            DataView view = (DataView)SqlDataSource2.Select(arg);
            DataTable dtConfirmedPurchaseOrders = view.ToTable();
            ReportDataSource rdsConfirmedPurchaseOrders = new ReportDataSource();
            rdsConfirmedPurchaseOrders = new ReportDataSource("dtRPTBOK_ScheduledDeliveryOverview", dtConfirmedPurchaseOrders);
            ScheduledDeliveryOverviewReportViewer.LocalReport.DataSources.Add(rdsConfirmedPurchaseOrders);
        }
        if (rdoUnconfirmedBooking.Checked) {
            UcCarrierVendorPanel.Visible = false;
            UcCarrierVendorReportViewPanel.Visible = false;
            ucpnlnotConfirmed.Visible = true;
            UcGridView1.DataSourceID = "SqlDataSource1";
        }
    }

    DataTable dtOutstandingPO = null;

    protected void btnChase_Click(object sender, EventArgs e) {
         // Iterate through the Products.Rows property
        foreach (GridViewRow row in UcGridView1.Rows) {
            // Access the CheckBox
            CheckBox cb = (CheckBox)row.FindControl("chkChase");
            if (cb != null && cb.Checked) {

                if (dtOutstandingPO == null) {

                    dtOutstandingPO = new DataTable();


                    DataColumn myDataColumn = new DataColumn();
                    myDataColumn.ColumnName = "PurchaseNumber";
                    dtOutstandingPO.Columns.Add(myDataColumn);

                    myDataColumn = new DataColumn();
                    myDataColumn.ColumnName = "Original_due_date";
                    dtOutstandingPO.Columns.Add(myDataColumn);


                    myDataColumn = new DataColumn();
                    myDataColumn.ColumnName = "OutstandingLines";
                    dtOutstandingPO.Columns.Add(myDataColumn);

                    myDataColumn = new DataColumn();
                    myDataColumn.ColumnName = "SiteName";
                    dtOutstandingPO.Columns.Add(myDataColumn);

                    myDataColumn = new DataColumn();
                    myDataColumn.ColumnName = "contactMail";
                    dtOutstandingPO.Columns.Add(myDataColumn);

                    myDataColumn = new DataColumn();
                    myDataColumn.ColumnName = "InventoryManagerMail";
                    dtOutstandingPO.Columns.Add(myDataColumn);

                    myDataColumn = new DataColumn();
                    myDataColumn.ColumnName = "VendorID";
                    dtOutstandingPO.Columns.Add(myDataColumn);

                }   

                HiddenField hdnVendorEmail = (HiddenField)row.FindControl("hdnVendorEmail");
                HiddenField hdnInventoryManagerMail = (HiddenField)row.FindControl("hdnInventoryManagerMail");
                HiddenField VendorID = (HiddenField)row.FindControl("VendorID");

                DataRow dataRow = dtOutstandingPO.NewRow();

                dataRow["PurchaseNumber"] = row.Cells[2].Text;
                dataRow["Original_due_date"] = row.Cells[3].Text;
                dataRow["OutstandingLines"] = row.Cells[4].Text;
                dataRow["SiteName"] = row.Cells[1].Text;
                dataRow["contactMail"] = hdnVendorEmail.Value;
                dataRow["InventoryManagerMail"] = hdnInventoryManagerMail.Value;
                dataRow["VendorID"] = VendorID.Value;

                dtOutstandingPO.Rows.Add(dataRow);
                
            }
        }

        if (dtOutstandingPO != null && dtOutstandingPO.Rows.Count > 0) {
            SendMailToVendors();
        }
    }

    private void SendMailToVendors() {
        if (dtOutstandingPO.Rows.Count > 0) {
            DataView POView = dtOutstandingPO.DefaultView;
            POView.RowFilter = "VendorID='" + Convert.ToString(dtOutstandingPO.Rows[0]["VendorID"]) + "'";

            SendMail(POView.ToTable());
            Page.UICulture = Convert.ToString(Session["CultureInfo"]);

            DataView POViewFilter = dtOutstandingPO.DefaultView;
            POViewFilter.RowFilter = "VendorID <> '" + Convert.ToString(dtOutstandingPO.Rows[0]["VendorID"]) + "'";

            dtOutstandingPO = POViewFilter.ToTable();
            SendMailToVendors();
        }
    }

    private void SendMail(DataTable tab) {

        sendCommunicationCommon oSendCommunication = new sendCommunicationCommon();
        string toAddress = tab.Rows[0]["contactMail"].ToString();       
        string mailBoby = string.Empty;
        string bodyData = string.Empty;
        string dueDate = string.Empty;
        string language = string.Empty;
        //string logoimagepath = System.AppDomain.CurrentDomain.BaseDirectory + @"\..\..\Images\OfficeDepotLogo.jpg";
        string logoimagepath = oSendCommunication.getAbsolutePath();

        string templatesPath2 = "emailtemplates/Appointment";
        string templateFile2 = "~/" + templatesPath2 + "/";
        templateFile2 += "DeliveryChase.english.htm";

        #region Setting reason as per the language ...
        switch (language)
        {
            case clsConstants.English:
                Page.UICulture = clsConstants.EnglishISO; // "en-US"; // English
                break;
            case clsConstants.French:
                Page.UICulture = clsConstants.FranceISO; // // France
                break;
            case clsConstants.German:
                Page.UICulture = clsConstants.GermanyISO; //"de"; // German
                break;
            case clsConstants.Dutch:
                Page.UICulture = clsConstants.NederlandISO; //"nl"; // Dutch
                break;
            case clsConstants.Spanish:
                Page.UICulture = clsConstants.SpainISO; //"es"; // Spanish
                break;
            case clsConstants.Italian:
                Page.UICulture = clsConstants.ItalyISO; //"it"; // Italy
                break;
            case clsConstants.Czech:
                Page.UICulture = clsConstants.CzechISO; //"cz"; // Czech
                break;
            default:
                Page.UICulture = clsConstants.EnglishISO; //"en-US"; // English
                break;
        }

        #endregion

        foreach (DataRow dr in tab.Rows) {
            dueDate = Convert.ToString(dr["Original_due_date"]);  // == "" && dr["Original_due_date"] == null ? string.Empty : Convert.ToDateTime(dr["Original_due_date"]).ToString("dd/MM/yyyy");
            bodyData += "<tr><td style='text-align:left;'>" + dr["SiteName"] + "</td>" +
                "<td style='text-align:center;'>" + dr["PurchaseNumber"] + "</td>" +
                "<td style='text-align:center;'>" + dr["OutstandingLines"] + "</td>" +
                "<td style='text-align:center;'>" + dueDate + "</td></tr>";
        }

        if (System.IO.File.Exists(System.Web.HttpContext.Current.Server.MapPath(templateFile2.ToLower()))) {
            using (StreamReader sReader = new StreamReader(System.Web.HttpContext.Current.Server.MapPath(templateFile2.ToLower()))) {

                mailBoby = sReader.ReadToEnd();
                mailBoby = mailBoby.Replace("{DearSirMadam}", WebCommon.getGlobalResourceValue("DearSirMadam"));
                mailBoby = mailBoby.Replace("{DeliveryChaseText1}", WebCommon.getGlobalResourceValue("DeliveryChaseText1"));
                mailBoby = mailBoby.Replace("{DeliveryChaseText2}", WebCommon.getGlobalResourceValue("DeliveryChaseText2"));
                mailBoby = mailBoby.Replace("{SiteRequiredKey}", WebCommon.getGlobalResourceValue("SiteRequiredKey"));
                mailBoby = mailBoby.Replace("{PurchaseOrderNumber}", WebCommon.getGlobalResourceValue("PurchaseOrderNumber"));
                mailBoby = mailBoby.Replace("{NoOfLinesOutstanding}", WebCommon.getGlobalResourceValue("NoOfLinesOutstanding"));
                mailBoby = mailBoby.Replace("{PODueDate}", WebCommon.getGlobalResourceValue("PODueDate"));
                mailBoby = mailBoby.Replace("{DeliveryChaseText3}", WebCommon.getGlobalResourceValue("DeliveryChaseText3"));
                mailBoby = mailBoby.Replace("{DeliveryChaseText4}", WebCommon.getGlobalResourceValue("DeliveryChaseText4"));
                mailBoby = mailBoby.Replace("{Yoursfaithfully}", WebCommon.getGlobalResourceValue("Yoursfaithfully"));
                mailBoby = mailBoby.Replace("{OfficeDepots}", WebCommon.getGlobalResourceValue("OfficeDepots"));

                mailBoby = mailBoby.Replace("{datatable}", bodyData);
                //mailBoby = mailBoby.Replace("{logoInnerPath}", logoimagepath);
            }
        }

        string mailSubject = "URGENT -DO NOT IGNORE";
        string fromAddress = System.Configuration.ConfigurationManager.AppSettings["fromAddress"] as string;
        bool blsHTMLMail = true;
        clsEmail oclsEmail = new clsEmail();

        if (!string.IsNullOrEmpty(toAddress)) {
            oclsEmail.sendMail(toAddress, mailBoby, mailSubject, fromAddress, blsHTMLMail);
        }

        toAddress = tab.Rows[0]["InventoryManagerMail"].ToString();
        if (!string.IsNullOrEmpty(toAddress)) {
            oclsEmail.sendMail(toAddress, mailBoby, mailSubject, fromAddress, blsHTMLMail);
        }
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        EncryptQueryString("ScheduledDeliveryOverview.aspx");
    }

    bool firstTime = true;
    System.Data.DataTable dt;

    protected void UcGridView1_RowDataBound(object sender, GridViewRowEventArgs e) {
        if (e.Row.RowType == DataControlRowType.DataRow)
            if (this.firstTime) {
                System.Data.DataView dv =
                     (e.Row.DataItem as System.Data.DataRowView).DataView;
                this.dt = dv.ToTable();

                UcGridView2.DataSource = dt;
                UcGridView2.DataBind();

                this.firstTime = false;
            }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebUtilities;
using BusinessEntities.ModuleBE.Appointment.Reports;
using BusinessLogicLayer.ModuleBAL.Appointment.Reports;
using Utilities;

public partial class ModuleUI_Appointment_Reports_ScheduledReceiptedReportByVendor : CommonPage
{
    protected string siteRequired = WebCommon.getGlobalResourceValue("SiteRequired");
    protected string NoRecordsFound = WebCommon.getGlobalResourceValue("NoRecordsFound");
    protected void Page_Init(object sender, EventArgs e)
    {
        ddlSite.CurrentPage = this;
        ddlSite.IsAllRequired = true;
        ddlSite.SchedulingContact = true;
        ddlSite.CurrentPage = this;
        ddlSite.IsAllRequired = true;
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            try
            {
                pager1.PageSize = 200;
                pager1.GenerateGoToSection = false;
                pager1.GeneratePagerInfoSection = false;
                pager1.Visible = false;
                if (GetQueryStringValue("Date") != null)
                {
                    txtDate.innerControltxtDate.Value = GetQueryStringValue("Date");
                }
                else
                {
                    txtDate.innerControltxtDate.Value = Convert.ToString(DateTime.Now.ToString("dd/MM/yyyy"));
                }
                if (GetQueryStringValue("SiteID") != null)
                {
                    BindGridView(GetQueryStringValue("SiteID"));
                }
               

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex); ;
            }
        }

        btnExportToExcel1.GridViewControl = UcGridView1;
        btnExportToExcel1.FileName = "ScheduledReceiptedReportByVendor";
    }
    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (GetQueryStringValue("SiteID") != null)
            {
                ddlSite.innerControlddlSite.SelectedIndex = ddlSite.innerControlddlSite.Items.IndexOf(ddlSite.innerControlddlSite.Items.FindByValue(GetQueryStringValue("SiteID")));
            }
        }
    }


    public void BindGridView(string siteID = null, int Page = 1)
    {
        try
        {
            if (siteID == null)
            {
                siteID = Convert.ToString(ddlSite.innerControlddlSite.SelectedValue);
            }
            ScheduledReceiptedReportDetailsBE oScheduledReceiptedReportBE = new ScheduledReceiptedReportDetailsBE();
            ScheduledReceiptedReportBAL oScheduledReceiptedReportBAL = new ScheduledReceiptedReportBAL();
            List<ScheduledReceiptedReportDetailsBE> lstScheduledReceiptedReportBE = new List<ScheduledReceiptedReportDetailsBE>();
            oScheduledReceiptedReportBE.Action = "ScheduledReceiptedReportByVendor";
            oScheduledReceiptedReportBE.Date = Common.GetYYYY_MM_DD(txtDate.innerControltxtDate.Value);
            oScheduledReceiptedReportBE.SiteId =Convert.ToInt32(siteID);
            oScheduledReceiptedReportBE.PageCount = Page;
            int RecordCount = 0;
            oScheduledReceiptedReportBAL.ScheduledReceiptedReportDetailsVendorBAL(oScheduledReceiptedReportBE, out lstScheduledReceiptedReportBE, out RecordCount);
            if (lstScheduledReceiptedReportBE.Count > 0)
            {
                pager1.ItemCount = RecordCount;
                pager1.Visible = true;
                UcGridView1.DataSource = lstScheduledReceiptedReportBE;
                UcGridView1.DataBind();              
                lblError.Text = string.Empty;
                ViewState["lstSites"] = lstScheduledReceiptedReportBE;
            }
            else
            {
                UcGridView1.DataSource = null;
                UcGridView1.DataBind();                
                lblError.Text = NoRecordsFound;
            }

        }
        catch (Exception ex)
        {
            LogUtility.SaveErrorLogEntry(ex); ;
        }
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        BindGridView();
    }
    public override BusinessEntities.BaseBe[] SubSort(GridViewSortEventArgs e)
    {
        return Utilities.GenericListHelper<ScheduledReceiptedReportDetailsBE>.SortList((List<ScheduledReceiptedReportDetailsBE>)ViewState["lstSites"], e.SortExpression, e.SortDirection).ToArray();
    }
    public void pager_Command(object sender, CommandEventArgs e)
    {
        int currnetPageIndx = Convert.ToInt32(e.CommandArgument);
        pager1.CurrentIndex = currnetPageIndx;
        BindGridView(null, currnetPageIndx);
    }
    protected void lnkLinesBooked_Click(object sender, EventArgs e)
    {
        try
        {
            LinkButton btn = (LinkButton)sender;
            string[] CommandArgument = btn.CommandArgument.Split(',');
            var script = "window.open('" + EncryptQuery("ScheduledReceiptedReportBySiteAndVendor.aspx?SiteID=" + CommandArgument[0] + "&VendorID=" + CommandArgument[1] + "&Date=" + txtDate.innerControltxtDate.Value + "&VendorName=" + CommandArgument[2].Replace('&','|') + "") + "');";
            ScriptManager.RegisterStartupScript(pnlUP1, pnlUP1.GetType(), "script", script, true);                      
        }
        catch (Exception ex)
        {
            LogUtility.SaveErrorLogEntry(ex); ;
        }        
    }
}
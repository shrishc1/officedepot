﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.Appointment.CountrySetting;
using BusinessLogicLayer.ModuleBAL.Appointment.CountrySetting;
using BusinessEntities.ModuleBE.VendorScorecard;
using BusinessEntities.ModuleBE.AdminFunctions;
using BusinessLogicLayer.ModuleBAL.VendorScorecard;
using BusinessLogicLayer.ModuleBAL.AdminFunctions;
using WebUtilities;
using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using BusinessLogicLayer.ModuleBAL.Appointment.SiteSettings;
using System.Data;
using System.Collections;
using BaseControlLibrary;
using Utilities;

public partial class ModuleUI_Appointment_Reports_ISP15Report : CommonPage
{
    protected bool IsVendorByNameClicked = false;
    protected bool IsVendorByNumberClicked = false;
    bool IsExportClicked = false;
    bool IsButtonsearchedClicked = false;
    protected string VendorPalletCheckingSuccessMesg = WebCommon.getGlobalResourceValue("VendorPalletCheckingSuccessMesg");
    
    protected void Page_Load(object sender, EventArgs e)
    {      
        if (!IsPostBack)
        {
            pager1.PageSize = 200;           
            pager1.GenerateGoToSection = false;
            pager1.GeneratePagerInfoSection = false;
            pager1.Visible = false;
            txtFromDate.Text = DateTime.Now.AddDays(-DateTime.Now.Day + 1).ToString("dd/MM/yyyy");
            txtToDate.Text = DateTime.Now.ToString("dd/MM/yyyy");           
            hdnJSFromDt.Value = txtFromDate.Text;
            hdnJSToDt.Value = txtToDate.Text;
            if (!string.IsNullOrEmpty(GetQueryStringValue("VendorID")) && !string.IsNullOrEmpty(GetQueryStringValue("DateTo"))
                && !string.IsNullOrEmpty(GetQueryStringValue("DateFrom")) && !string.IsNullOrEmpty(GetQueryStringValue("SiteID")))
            {
                BindGridView(1);
            }
            else if (!string.IsNullOrEmpty(GetQueryStringValue("BindSummary")))
            {
                BindGridView(1);
            }
        }       
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (GetQueryStringValue("RetainData") != null && GetQueryStringValue("RetainData").ToString() == "true")
        {
            if (Session["ISP15"] != null && IsButtonsearchedClicked==false)
            {
                RetainSearchData();
            }
        }
    }

    protected void lnkVendor_Click(object sender, EventArgs e)
    {
        IsButtonsearchedClicked = true;
        LinkButton btn = (LinkButton)(sender);
        string[] argument = btn.CommandArgument.Split(',');
        EncryptQueryString("ISP15Report.aspx?VendorID=" + Convert.ToString(argument[0]) + "&DateTo=" + hdnJSToDt.Value + "&DateFrom=" + hdnJSFromDt.Value + "&SiteID=" + Convert.ToString(argument[1]) + "");
       
    }    
    
    protected void btnBack_Click(object sender, EventArgs e)
    {
        EncryptQueryString("APPCNT_VendorPalletChecking.aspx");
    }


    public void BindGridView(int Page=1)
    {
        UP_VendorBE oUP_VendorBE = new UP_VendorBE();
        UP_VendorBAL oUP_VendorBAL = new UP_VendorBAL();

        oUP_VendorBE.Action = !string.IsNullOrEmpty(GetQueryStringValue("VendorID"))
            ? "ISPM15Detailed"
            : !string.IsNullOrEmpty(GetQueryStringValue("BindSummary"))
                ? "ISPM15Summary"
                : rdoSummary.Checked == true ? "ISPM15Summary" : "ISPM15Detailed";

        oUP_VendorBE.IsVenodrPalletChecking = rdoIsVendorPalletCheck.Checked;
        oUP_VendorBE.IsStandardPalletChecking = rdoIsStandardPalletCheck.Checked;
        oUP_VendorBE.ISPM15CountryPalletChecking = rdoISPM15PalletCheck.Checked;


        if (Session["ISP15"] != null && !string.IsNullOrEmpty(GetQueryStringValue("BindSummary")))
        {
            Hashtable htInventoryReview1 = (Hashtable)Session["ISP15"];
            oUP_VendorBE.VendorIDs = (htInventoryReview1.ContainsKey("SelectedVendorIDs") && htInventoryReview1["SelectedVendorIDs"] != null) ? htInventoryReview1["SelectedVendorIDs"].ToString() : null;
            oUP_VendorBE.VendorActiveSiteIDs = (htInventoryReview1.ContainsKey("SelectedSiteId") && htInventoryReview1["SelectedSiteId"] != null) ? htInventoryReview1["SelectedSiteId"].ToString() : null;
            oUP_VendorBE.DateTo = (htInventoryReview1.ContainsKey("DateTo") && htInventoryReview1["DateTo"] != null) ? Common.GetYYYY_MM_DD(htInventoryReview1["DateTo"].ToString()) : "";
            oUP_VendorBE.DateFrom = (htInventoryReview1.ContainsKey("DateFrom") && htInventoryReview1["DateFrom"] != null) ? Common.GetYYYY_MM_DD(htInventoryReview1["DateFrom"].ToString()) : "";
            hdnJSToDt.Value = (htInventoryReview1.ContainsKey("DateTo") && htInventoryReview1["DateTo"] != null) ? (htInventoryReview1["DateTo"].ToString()) : "";
            hdnJSFromDt.Value = (htInventoryReview1.ContainsKey("DateFrom") && htInventoryReview1["DateFrom"] != null) ? (htInventoryReview1["DateFrom"].ToString()) : "";
        } 
        else
        {
            oUP_VendorBE.VendorActiveSiteIDs = !string.IsNullOrEmpty(GetQueryStringValue("SiteID")) ? GetQueryStringValue("SiteID") : msSite.SelectedSiteIDs;

            oUP_VendorBE.VendorIDs = !string.IsNullOrEmpty(GetQueryStringValue("VendorID")) ? GetQueryStringValue("VendorID") : msVendor.SelectedVendorIDs;

            if (!string.IsNullOrEmpty(GetQueryStringValue("DateTo")))
            {
                oUP_VendorBE.DateTo = Common.GetYYYY_MM_DD(GetQueryStringValue("DateTo"));
                hdnJSToDt.Value = GetQueryStringValue("DateTo");
            }
            else
            {
                oUP_VendorBE.DateTo = Common.GetYYYY_MM_DD(hdnJSToDt.Value);
            }
            if (!string.IsNullOrEmpty(GetQueryStringValue("DateFrom")))
            {
                oUP_VendorBE.DateFrom = Common.GetYYYY_MM_DD(GetQueryStringValue("DateFrom"));
                hdnJSFromDt.Value = GetQueryStringValue("DateFrom");
            }
            else
            {
                oUP_VendorBE.DateFrom = Common.GetYYYY_MM_DD(hdnJSFromDt.Value);
                if(string.IsNullOrEmpty(GetQueryStringValue("BindSummary")))
                this.SetSession(oUP_VendorBE);
            }
        }

        if (IsExportClicked)
        {
            oUP_VendorBE.GridCurrentPageNo = 0;
            oUP_VendorBE.GridPageSize = 0;
        }
        else
        {
            oUP_VendorBE.GridCurrentPageNo = Page;
            oUP_VendorBE.GridPageSize = 200;
        }

        
        DataTable dt = oUP_VendorBAL.BindVendorISPM15ReportBAL(oUP_VendorBE);
        if (dt.Rows.Count > 0)
        {
            pager1.Visible = true;
            pager1.ItemCount = Convert.ToInt32(dt.Rows[0]["TotalRecords"]);
            if (!string.IsNullOrEmpty(GetQueryStringValue("VendorID")))
            {
                grdISPDetails.DataSource = dt;
                grdISPDetails.DataBind();
                grdISP.Visible = false;
                grdISPDetails.Visible = true;
            }
            else
            {
                if (rdoSummary.Checked)
                {
                    grdISP.DataSource = dt;
                    grdISP.DataBind();
                    grdISP.Visible = true;
                    grdISPDetails.Visible = false;
                }
                else
                {
                    grdISPDetails.DataSource = dt;
                    grdISPDetails.DataBind();
                    grdISP.Visible = false;
                    grdISPDetails.Visible = true;
                }
            }

            tblsearch.Visible = false;
            btnExportToExcel.Visible = true;
            tblgrid.Visible = true;
        }
        else
        {
            if (!string.IsNullOrEmpty(GetQueryStringValue("VendorID")))
            {
                grdISPDetails.DataSource = null;
                grdISPDetails.DataBind();
            }
            else
            {
                if (rdoSummary.Checked )
                {
                    grdISP.DataSource = null;
                    grdISP.DataBind();
                }
                else
                {
                    grdISPDetails.DataSource = null;
                    grdISPDetails.DataBind();
                }
            }

            tblsearch.Visible = false;
            btnExportToExcel.Visible = false;
            tblgrid.Visible = true;
        }
    }

   
    public void pager_Command(object sender, CommandEventArgs e)
    {
        int currnetPageIndx = Convert.ToInt32(e.CommandArgument);
        pager1.CurrentIndex = currnetPageIndx;
        BindGridView(currnetPageIndx);
    }

    protected void btnExport_Click(object sender, EventArgs e)
    {
        IsExportClicked = true;
        BindGridView();
        if (!string.IsNullOrEmpty(GetQueryStringValue("VendorID")))
        {
            WebCommon.ExportHideHidden("ISPM15_Detailed_Report", grdISPDetails, null, false, true);
        }
        else
        {
            switch (rdoSummary.Checked)
            {
                case true:
                    WebCommon.ExportHideHidden("ISPM15_Summary_Report", grdISP, null, false, true);
                    break;
                default:
                    WebCommon.ExportHideHidden("ISPM15_Detailed_Report", grdISPDetails, null, false, true);
                    break;
            }
        }       
    }

    protected void BtnSearch_Click(object sender, EventArgs e)
    {
        BindGridView(1);
        tblsearch.Visible = false;
        btnExportToExcel.Visible = true;
        tblgrid.Visible = true;
        IsButtonsearchedClicked = true;       
    }
    protected void btnBack_Click1(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(GetQueryStringValue("VendorID")))
        {
            EncryptQueryString("ISP15Report.aspx?BindSummary=true");
        }
        else
        {
            EncryptQueryString("ISP15Report.aspx?RetainData=true");
        }
    }
    private void SetSession(UP_VendorBE oUP_VendorBE)
    {
        Session["ISP15"] = null;
        Session.Remove("ISP15");      
        TextBox txtVendor = (TextBox)msVendor.FindControl("ucVendor").FindControl("txtVendorNo");
        Hashtable htInventoryReview1 = new Hashtable();
        htInventoryReview1.Add("txtVendor", txtVendor.Text);
        htInventoryReview1.Add("SelectedSiteId", oUP_VendorBE.VendorActiveSiteIDs);
        htInventoryReview1.Add("SelectedVendorIDs", oUP_VendorBE.VendorIDs);
        htInventoryReview1.Add("IsSearchedByVendorNo", msVendor.IsSearchedByVendorNo);
        htInventoryReview1.Add("DateTo", hdnJSToDt.Value);
        htInventoryReview1.Add("DateFrom", hdnJSFromDt.Value);
        switch (rdoDetailed.Checked)
        {
            case true:
                htInventoryReview1.Add("rdoDetailedChecked", true);
                break;
            default:
                htInventoryReview1.Add("rdoDetailedChecked", false);
                break;
        }
        Session["ISP15"] = htInventoryReview1;
    }
    public void RetainSearchData()
    {     
        Hashtable htInventoryReview1 = (Hashtable)Session["ISP15"];

        bool rdoDetailedChecked = (htInventoryReview1.ContainsKey("rdoDetailedChecked") && htInventoryReview1["rdoDetailedChecked"] != null) ? Convert.ToBoolean(htInventoryReview1["rdoDetailedChecked"]) : false;
        if (rdoDetailedChecked)
        {
            rdoDetailed.Checked = true;
            rdoSummary.Checked = false;
        }
        else
        {
            rdoDetailed.Checked = false;
            rdoSummary.Checked = true;
        }
        //********** Vendor ***************
        string txtVendor = (htInventoryReview1.ContainsKey("txtVendor") && htInventoryReview1["txtVendor"] != null) ? htInventoryReview1["txtVendor"].ToString() : "";
        string VendorId = (htInventoryReview1.ContainsKey("SelectedVendorIDs") && htInventoryReview1["SelectedVendorIDs"] != null) ? htInventoryReview1["SelectedVendorIDs"].ToString() : "";
        TextBox txtVendorId = msVendor.FindControl("ucVendor").FindControl("txtVendorNo") as TextBox;

        bool IsSearchedByVendorNo = (htInventoryReview1.ContainsKey("IsSearchedByVendorNo") && htInventoryReview1["IsSearchedByVendorNo"] != null) ? Convert.ToBoolean(htInventoryReview1["IsSearchedByVendorNo"]) : false;
        string txtVendorIdText = txtVendorId.Text;
        ucListBox lstRightVendor = msVendor.FindControl("lstRight") as ucListBox;
        ucListBox lstLeftVendor = msVendor.FindControl("ucVendor").FindControl("lstLeft") as ucListBox;
        string strVendorId = string.Empty;
        lstLeftVendor.Items.Clear();
        if (lstLeftVendor != null && lstRightVendor != null && (!string.IsNullOrEmpty(VendorId) || !string.IsNullOrEmpty(VendorId)))
        {
            lstLeftVendor.Items.Clear(); 

            switch (IsSearchedByVendorNo)
            {
                case true:
                    msVendor.SearchVendorNumberClick(txtVendor);
                    break;
                default:
                    msVendor.SearchVendorClick(txtVendor);
                    break; 
            }

            if (!string.IsNullOrEmpty(VendorId))
            {
                string[] strIncludeVendorIDs = VendorId.Split(',');
                for (int index = 0; index < strIncludeVendorIDs.Length; index++)
                {
                    ListItem listItem = lstLeftVendor.Items.FindByValue(strIncludeVendorIDs[index]);
                    if (listItem != null)
                    {
                        strVendorId += strIncludeVendorIDs[index] + ",";
                        lstRightVendor.Items.Add(listItem);
                        lstLeftVendor.Items.Remove(listItem);
                    }
                }
            }

            HiddenField hdnSelectedVendor = msVendor.FindControl("hiddenSelectedIDs") as HiddenField;
            hdnSelectedVendor.Value = strVendorId;
        }

      
      //  *********** Site ***************
        string SiteId = (htInventoryReview1.ContainsKey("SelectedSiteId") && htInventoryReview1["SelectedSiteId"] != null) ? htInventoryReview1["SelectedSiteId"].ToString() : "";
        ucListBox lstRightSite = msSite.FindControl("lstRight") as ucListBox;
        ucListBox lstLeftSite = msSite.FindControl("lstLeft") as ucListBox;
        string msSiteid = string.Empty;
        lstRightSite.Items.Clear();
        lstRightSite.Items.Clear();
        msSite.SelectedSiteIDs = "";
        if (lstLeftSite != null && lstRightSite != null && !string.IsNullOrEmpty(SiteId))
        {
            lstLeftSite.Items.Clear();
            lstRightSite.Items.Clear();
            msSite.BindSite();

            string[] strSiteIDs = SiteId.Split(',');
            for (int index = 0; index < strSiteIDs.Length; index++)
            {
                ListItem listItem = lstLeftSite.Items.FindByValue(strSiteIDs[index]);
                if (listItem != null)
                {
                    msSiteid = msSiteid + strSiteIDs[index].ToString() + ",";
                    lstRightSite.Items.Add(listItem);
                    lstLeftSite.Items.Remove(listItem);
                }

            }
            if (string.IsNullOrEmpty(msSite.SelectedSiteIDs))
                msSite.SelectedSiteIDs = msSiteid.Trim(',');
        }
        txtFromDate.Text = (htInventoryReview1.ContainsKey("DateFrom") && htInventoryReview1["DateFrom"] != null) ? Convert.ToString(htInventoryReview1["DateFrom"]) : string.Empty;
        txtToDate.Text = (htInventoryReview1.ContainsKey("DateTo") && htInventoryReview1["DateTo"] != null) ? Convert.ToString(htInventoryReview1["DateTo"]) : string.Empty;
        Session["ISP15"] = null;
        Session.Remove("ISP15");  
    }
}
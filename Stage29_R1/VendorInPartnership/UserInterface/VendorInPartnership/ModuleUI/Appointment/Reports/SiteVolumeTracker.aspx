﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    AutoEventWireup="true" CodeFile="SiteVolumeTracker.aspx.cs" Inherits="SiteVolumeTracker" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="../../../CommonUI/UserControls/ucSite.ascx" TagName="ucSite" TagPrefix="cc2" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <style type="text/css">
        .radio-fix label {
            margin-right: 10px !important;
            display: inline-block;
        }

        .radiobuttonlist label {
            margin-right: 0px;
        }

        .radio-fix-last label {
            margin-right: 10px !important;
        }
    </style>

    <script language="javascript" type="text/javascript">
        $(document).ready(function () {
            $("body").keypress(function (e) {
                if (e.which == 13 || e.keyCode == 13) {
                    $('#<%=btnGo.ClientID %>').click();
                    return false;
                }
            });
            $("#<%=rblReportType.ClientID%>").change(function () {                
                $('#<%=btnExportToExcel.ClientID%>').hide();
                if ($("#<%=rblReportType.ClientID%>").find(":checked").val() == 1) {
                    $('#trbacklog').hide();
                    
                }
                else {
                    $('#trbacklog').show();
                }
            })

             $("#<%=rdoDateType.ClientID%>").change(function () {
                $('#<%=btnExportToExcel.ClientID%>').hide();                
            })

            if ($("#<%=rblReportType.ClientID%>").find(":checked").val() == 1) {
                $('#trbacklog').hide();
            }
            else {
                $('#trbacklog').show();
            }
            //            if $("#<%= rblReportType.ClientID%> :checked").val() == "1" {
            //                $('#trbacklog').show();
            //            }
            //            if $("#<%= rblReportType.ClientID%> :checked").val() == "2" {
            //                $('#trbacklog').hide();
            //            }            
        });
        document.onmousemove = positiontip;
    </script>
    <div id="dhtmltooltip" class="balloonstyle">
    </div>
    <iframe id="iframetop" scrolling="no" frameborder="0" class="iballoonstyle" width="0"
        height="0"></iframe>
    <h2>
        <cc1:ucLabel ID="lblSiteVolumeTracker" runat="server"></cc1:ucLabel>
    </h2>
    <div class="right-shadow">
        <div class="formbox">
            <cc1:ucPanel ID="UcCarrierVendorPanel" runat="server">
                <table width="100%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                    <tr>
                        <td style="font-weight: bold; width: 10%">
                            <cc1:ucLabel ID="lblSite" runat="server" Text="Site"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold; width: 1%">:
                        </td>
                        <td>
                            <cc2:ucSite ID="ddlSite" runat="server" />
                        </td>
                        <td style="font-weight: bold; width: 10%">
                            <cc1:ucLabel ID="lblReportType" runat="server" Text="Site"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold; width: 1%">:
                        </td>
                        <td>
                            <cc1:ucRadioButtonList ID="rblReportType" CssClass="radio-fix" runat="server" RepeatColumns="4"
                                RepeatDirection="Horizontal">
                                <asp:ListItem Text="lstCapacity" Value="1" Selected="True"></asp:ListItem>
                                <asp:ListItem Text="lstBacklog" Value="2"></asp:ListItem>
                            </cc1:ucRadioButtonList>
                        </td>
                        <td style="font-weight: bold; width: 10%">
                            <cc1:ucButton ID="btnGo" runat="server" OnClick="btnGo_Click" />
                        </td>
                        <td>
                            <cc1:ucButton ID="btnExportToExcel" runat="server" CssClass="exporttoexcel" Text="Export To Excel"
                                OnClick="btnExport_Click" Width="109px" Height="20px" Visible="false" />
                        </td>

                    </tr>
                    <tr id="trbacklog">
                        <td></td>
                        <td></td>
                        <td></td>
                        <td style="font-weight: bold; width: 10%">
                            <cc1:ucLabel ID="lblDateType" runat="server" Text="Date Type"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold; width: 1%">:
                        </td>
                        <td>
                            <cc1:ucRadioButtonList ID="rdoDateType" CssClass="radio-fix" runat="server" RepeatColumns="4"
                                RepeatDirection="Horizontal">
                                <asp:ListItem Text="lstOriginalDueDate" Value="2" Selected="True"></asp:ListItem>
                                <asp:ListItem Text="lstRevisedDate" Value="1"></asp:ListItem>
                            </cc1:ucRadioButtonList>
                        </td>
                    </tr>
                </table>
            </cc1:ucPanel>
            <div id="divcapacity" runat="server">
                <table style="min-width: 950px;" border="0" cellspacing="0" cellpadding="0" class="grid gvclass searchgrid-1">
                    <tr>
                        <th style="width: 180px; text-align: right;">
                            <cc1:ucLinkButton ID="lnkLeft" runat="server" Text="<<" OnClick="lnkLeft_Click" Visible="false" ToolTip="Move backwards"></cc1:ucLinkButton>
                        </th>

                        <asp:Repeater ID="rptDate" runat="server">
                            <ItemTemplate>
                                <th style="width: 85px; text-align: center;">
                                    <asp:Literal ID="ltDate" runat="server" Text='<%# Convert.ToDateTime(Eval("CalculationDate").ToString()).ToString("dd/MM/yyyy") %>'></asp:Literal>
                                </th>
                            </ItemTemplate>
                        </asp:Repeater>

                        <th style="width: 10px; text-align: right;">
                            <cc1:ucLinkButton ID="lnkRight" runat="server" Text=">>" Visible="false" ToolTip="Move forward"
                                OnClick="lnkRight_Click"></cc1:ucLinkButton>
                        </th>

                    </tr>
                    <tr>
                        <td colspan="11">
                            <asp:Repeater ID="rptSiteVolTrackerReports" runat="server" OnItemDataBound="rptSiteVolTrackerReports_OnItemDataBound">
                                <ItemTemplate>
                                    <table border="0" cellspacing="0" cellpadding="0" class="form-table" width="100%">
                                        <tr>
                                            <td>
                                                <asp:Literal ID="ltArea" Visible="false" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Area") %>'></asp:Literal>
                                                <cc1:ucPanel runat="server" ID="ucPnl" GroupingText='<%# DataBinder.Eval(Container.DataItem, "Area") %>'
                                                    CssClass="fieldset-form" Style="position: relative;">
                                                    <div style="position: absolute; top: 6px; left: 125px;">
                                                        <a href="javascript:void(0)" rel='<%# DataBinder.Eval(Container.DataItem, "Area") %>'
                                                            tipwidth="750" onmouseover="ddrivetip('<%# DataBinder.Eval(Container.DataItem, "Area") %>','#ededed','750');"
                                                            onmouseout="hideddrivetip();">
                                                            <img src="../../../Images/info_button1.gif" align="absMiddle" border="0" /></a>
                                                    </div>
                                                    <div class="balloonstyle" id='<%# DataBinder.Eval(Container.DataItem, "Area") %>'>
                                                        <span class="bla8blue">
                                                            <asp:Literal ID="ltInfo" runat="server"></asp:Literal>
                                                        </span>
                                                    </div>
                                                    <table border="0" cellspacing="0" cellpadding="0" class="form-table" width="100%">
                                                        <asp:Repeater ID="rptkeyDetails" runat="server" OnItemDataBound="rptkeyDetails_OnItemDataBound">
                                                            <ItemTemplate>
                                                                <tr>
                                                                    <td style="height: 25px; width: 150px; font-weight: bold;">
                                                                        <asp:Literal ID="ltAreaKey" runat="server" Text='<%#  DataBinder.Eval(Container.DataItem, "AreaKey") %>'></asp:Literal>
                                                                    </td>
                                                                    <asp:Repeater ID="rptKeyvalue" runat="server" OnItemDataBound="rptKeyvalue_OnItemDataBound">
                                                                        <ItemTemplate>
                                                                            <td style="color: #333333; background-color: #F7F6F3; text-align: center; width: 85px;">
                                                                                <asp:Literal ID="hdnAreaKey" runat="server" Text='<%#  DataBinder.Eval(Container.DataItem, "AreaKey") %>'
                                                                                    Visible="false" />
                                                                                <asp:Literal ID="ltKeyvalue" runat="server" Text='<%#  DataBinder.Eval(Container.DataItem, "Keyvalue") %>'></asp:Literal>
                                                                                <asp:HyperLink ID="hpLink" Target="_blank" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Keyvalue") %>'
                                                                                    NavigateUrl='#' Visible="false"></asp:HyperLink>
                                                                                <asp:Literal ID="ltDate" runat="server" Text='<%# Convert.ToDateTime(Eval("CalculationDate").ToString()).ToString("dd/MM/yyyy") %>' Visible="false"></asp:Literal>
                                                                            </td>
                                                                        </ItemTemplate>
                                                                        <AlternatingItemTemplate>
                                                                            <td style="text-align: center; width: 85px;">
                                                                                <asp:Literal ID="hdnAreaKey" runat="server" Text='<%#  DataBinder.Eval(Container.DataItem, "AreaKey") %>'
                                                                                    Visible="false" />
                                                                                <asp:Literal ID="ltKeyvalue" runat="server" Text='<%#  DataBinder.Eval(Container.DataItem, "Keyvalue") %>'></asp:Literal>
                                                                                <asp:HyperLink ID="hpLink" Target="_blank" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Keyvalue") %>'
                                                                                    NavigateUrl='#' Visible="false"></asp:HyperLink>
                                                                                <asp:Literal ID="ltDate" runat="server" Text='<%# Convert.ToDateTime(Eval("CalculationDate").ToString()).ToString("dd/MM/yyyy") %>' Visible="false"></asp:Literal>
                                                                            </td>
                                                                        </AlternatingItemTemplate>
                                                                    </asp:Repeater>
                                                                </tr>
                                                            </ItemTemplate>
                                                        </asp:Repeater>
                                                    </table>
                                                </cc1:ucPanel>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:Repeater>
                        </td>
                    </tr>
                </table>
            </div>

            <div id="divExport" runat="server" style="display:none">
                <table border="1" cellspacing="0" cellpadding="0" class="form-table" id="tblExport">
                    <tr>
                      <td style="height: 25px; min-width: 140px;max-width:140px; font-weight: bold;"></td>
                            <asp:Repeater ID="rptDateExport" runat="server">
                                <ItemTemplate>
                                    <td style="min-width: 75px;max-width: 75px; text-align: center;">
                                        <asp:Literal ID="ltDate" runat="server" Text='<%# Convert.ToDateTime(Eval("CalculationDate").ToString()).ToString("dd/MM/yyyy") %>'></asp:Literal>
                                    </td>
                                </ItemTemplate>
                            </asp:Repeater>
                      
                    </tr>
                    </table>
                <table cellspacing="0" cellpadding="0" class="form-table">
                    <tr>                           
                        <td>
                            <asp:Repeater ID="rptSiteVolTrackerReportsExport" runat="server" OnItemDataBound="rptSiteVolTrackerReports_OnItemDataBound">
                                <ItemTemplate>
                                    <table border="0" cellspacing="0" cellpadding="0" class="form-table" width="100%">
                                        <tr>
                                            <td style="font-weight: bold; text-align: center">
                                                <asp:Literal ID="ltArea" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Area") %>'></asp:Literal>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table border="0" cellspacing="0" cellpadding="0" class="form-table" width="100%">
                                                    <asp:Repeater ID="rptkeyDetails" runat="server" OnItemDataBound="rptkeyDetails_OnItemDataBound">
                                                        <ItemTemplate>
                                                            <tr>
                                                                <td style="height: 25px; min-width: 140px;max-width:140px; font-weight: bold;">
                                                                    <asp:Literal ID="ltAreaKey" runat="server" Text='<%#  DataBinder.Eval(Container.DataItem, "AreaKey") %>'></asp:Literal>
                                                                </td>
                                                                <asp:Repeater ID="rptKeyvalue" runat="server" OnItemDataBound="rptKeyvalue_OnItemDataBound">
                                                                    <ItemTemplate>
                                                                        <td style="color: #333333; background-color: #F7F6F3; text-align: center; min-width: 75px;max-width: 75px;">
                                                                            <asp:Literal ID="hdnAreaKey" runat="server" Text='<%#  DataBinder.Eval(Container.DataItem, "AreaKey") %>'
                                                                                Visible="false" />
                                                                            <asp:Literal ID="ltKeyvalue" runat="server" Text='<%#  DataBinder.Eval(Container.DataItem, "Keyvalue") %>'></asp:Literal>
                                                                            <asp:Literal ID="ltLink" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Keyvalue") %>' Visible="false" />
                                                                            <%--<asp:HyperLink ID="hpLink" Target="_blank" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Keyvalue") %>'
                                                                                NavigateUrl='#' Visible="false"></asp:HyperLink>--%>
                                                                            <asp:Literal ID="ltDate" runat="server" Text='<%# Convert.ToDateTime(Eval("CalculationDate").ToString()).ToString("dd/MM/yyyy") %>' Visible="false"></asp:Literal>
                                                                        </td>
                                                                    </ItemTemplate>
                                                                    <AlternatingItemTemplate>
                                                                        <td style="text-align: center; min-width: 75px;max-width:75px;">
                                                                            <asp:Literal ID="hdnAreaKey" runat="server" Text='<%#  DataBinder.Eval(Container.DataItem, "AreaKey") %>'
                                                                                Visible="false" />
                                                                            <asp:Literal ID="ltKeyvalue" runat="server" Text='<%#  DataBinder.Eval(Container.DataItem, "Keyvalue") %>'></asp:Literal>
                                                                            <asp:Literal ID="ltLink" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Keyvalue") %>' Visible="false" />
                                                                            <%--<asp:HyperLink ID="hpLink" Target="_blank" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Keyvalue") %>'
                                                                                NavigateUrl='#' Visible="false"></asp:HyperLink>--%>
                                                                            <asp:Literal ID="ltDate" runat="server" Text='<%# Convert.ToDateTime(Eval("CalculationDate").ToString()).ToString("dd/MM/yyyy") %>' Visible="false"></asp:Literal>
                                                                        </td>
                                                                    </AlternatingItemTemplate>
                                                                </asp:Repeater>
                                                            </tr>
                                                        </ItemTemplate>
                                                    </asp:Repeater>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="height: 35px;">&nbsp;
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:Repeater>
                        </td>
                       
                    </tr>
                </table>
            </div>

            <cc1:ucGridView ID="UcGridView1" Width="70%" runat="server" CssClass="grid" Visible="false">
                <Columns>
                    <asp:TemplateField HeaderText="Date">
                        <HeaderStyle Width="5%" HorizontalAlign="Left" />
                        <ItemStyle HorizontalAlign="Left" />
                        <ItemTemplate>
                            <asp:Label ID="lblDate" runat="server" Text='<%# Convert.ToDateTime(Eval("CalculationDate").ToString()).ToString("dd/MM/yyyy") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField HeaderText="Max Lines" DataField="MaxLines">
                        <HeaderStyle HorizontalAlign="Center" Width="7%" />
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:BoundField>
                    <asp:BoundField HeaderText="Overdue Line" DataField="OnBackorder">
                        <HeaderStyle HorizontalAlign="Center" Width="10%" />
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:BoundField>
                    <asp:BoundField HeaderText="Actual Lines Due" DataField="LinesDue">
                        <HeaderStyle HorizontalAlign="Center" Width="10%" />
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:BoundField>
                    <asp:BoundField HeaderText="Projected Backlog   (to clear)" DataField="ProjectedBacklog">
                        <HeaderStyle HorizontalAlign="Center" Width="15%" />
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:BoundField>
                    <asp:BoundField HeaderText="Avg Lines Due" DataField="AvgLinesDue">
                        <HeaderStyle HorizontalAlign="Center" Width="10%" />
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:BoundField>
                    <asp:BoundField HeaderText="Projected Backlog       (based on max and avg due)" DataField="AvgProjectedBacklog">
                        <HeaderStyle HorizontalAlign="Center" Width="15%" />
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:BoundField>
                </Columns>
            </cc1:ucGridView>
        </div>
</asp:Content>

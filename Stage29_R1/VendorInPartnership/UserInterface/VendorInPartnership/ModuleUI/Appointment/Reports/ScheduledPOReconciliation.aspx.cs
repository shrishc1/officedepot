﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebUtilities;
using BusinessEntities.ModuleBE.Appointment.Booking;
using BusinessLogicLayer.ModuleBAL.Appointment.Booking;
using BusinessLogicLayer.ModuleBAL.Security;
using BusinessEntities.ModuleBE.Security;
using Utilities;
using System.Globalization;
using System.Collections;
using BusinessEntities.ModuleBE.Languages.Languages;
using BusinessLogicLayer.ModuleBAL.StockOverview;
using BusinessEntities.ModuleBE.StockOverview;
using System.IO;
using System.Configuration;
using BusinessEntities.ModuleBE.Upload;
using BusinessLogicLayer.ModuleBAL.Upload;

public partial class ScheduledPOReconciliation : CommonPage
{
    string sPortalLink = Convert.ToString(ConfigurationManager.AppSettings["Portallink"]) + @"/ModuleUI/Security/Login.aspx";
    protected string selectCheckboxToAccept = WebCommon.getGlobalResourceValue("SelectCheckboxToAction");
   

    #region Events

    protected void Page_Init(object sender, EventArgs e)
    {
        ucVendorTemplateSelect.CurrentPage = this;
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {

        if (!Page.IsPostBack)
        {
            pager1.PageSize = 50;
            pager1.GenerateGoToSection = false;
            pager1.GeneratePagerInfoSection = false;
            pager1.Visible = false;      
            if (GetQueryStringValue("PreviousPage") != null && GetQueryStringValue("PreviousPage").ToString() == "OTIFHit")
            {
                if (Session["POReconciliation"] != null)
                {
                    GetSession();
                }
            }
            else if (GetQueryStringValue("PreviousPage") != null && GetQueryStringValue("PreviousPage").ToString() == "POInquiry") {  //----------phase 18 R2 point 10 -------------------------------//
                BindGridview();
            }
        }
    }
    public void pager_Command(object sender, CommandEventArgs e)
    {

        int currnetPageIndx = Convert.ToInt32(e.CommandArgument);
        pager1.CurrentIndex = currnetPageIndx;
        BindGridview(currnetPageIndx);
    }

    //private void BindReportByPO(int Page = 1)
    //{
    //    ucpnlsearch.Visible = false;
    //    ucpnlResult.Visible = true;

        
    //}


    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {

            txtFromDate.Text = DateTime.Now.AddDays(-DateTime.Now.Day + 1).ToString("dd/MM/yyyy");
            txtToDate.Text = DateTime.Now.AddDays(-1).ToString("dd/MM/yyyy");
            hdnJSFromDt.Value = txtFromDate.Text;
            hdnJSToDt.Value = txtToDate.Text;

            BindDropDowns();
        }
    }

    protected void btnGenerateReport_Click(object sender, EventArgs e)
    {
        BindGridview();
    }

    private void BindGridview(int Page=1)
    {
        ucpnlsearch.Visible = false;
        ucpnlResult.Visible = true;

        if (GetQueryStringValue("Params") != null)
        {

            APPBOK_BookingBE oAPPBOK_BookingBE = new APPBOK_BookingBE();
            APPBOK_BookingBAL oAPPBOK_BookingBAL = new APPBOK_BookingBAL();


            oAPPBOK_BookingBE.PurchaseOrder = new BusinessEntities.ModuleBE.Upload.Up_PurchaseOrderDetailBE();

            string[] strparams = new string[3];
            strparams = GetQueryStringValue("Params").Split('~');
            oAPPBOK_BookingBE.SelectedVendorIDs = strparams[0];
            oAPPBOK_BookingBE.SelectedSiteIds = strparams[1];
            oAPPBOK_BookingBE.PurchaseOrder.Purchase_order = strparams[2];
            oAPPBOK_BookingBE.PageCount = Page;
            List<APPBOK_BookingBE> lstPORec = oAPPBOK_BookingBAL.GetPOReconciliationBAL(oAPPBOK_BookingBE);
            if (lstPORec != null && lstPORec.Count > 0)
            {
                gvPOReCon.DataSource = null;
                gvPOReCon.DataSource = lstPORec;
                ViewState["PORec"] = lstPORec;
                btnExportToExcel.Visible = true;
            }
            else
            {
                btnChase.Visible = false;
                btnExportToExcel.Visible = false;
            }

            gvPOReCon.DataBind();
        }
        else
        {
            APPBOK_BookingBE oAPPBOK_BookingBE = new APPBOK_BookingBE();
            APPBOK_BookingBAL oAPPBOK_BookingBAL = new APPBOK_BookingBAL();

            oAPPBOK_BookingBE.SelectedVendorIDs = msVendor.SelectedVendorIDs;
            oAPPBOK_BookingBE.SelectedSiteIds = msSite.SelectedSiteIDs;
            oAPPBOK_BookingBE.FromDate = Utilities.Common.TextToDateFormat(hdnJSFromDt.Value);
            oAPPBOK_BookingBE.ToDate = Utilities.Common.TextToDateFormat(hdnJSToDt.Value);
            oAPPBOK_BookingBE.BookingType = rblType.SelectedValue == "2" ? "SNBI" : (rblType.SelectedValue == "3" ? "NSBI" : null);
            oAPPBOK_BookingBE.PurchaseOrder = new BusinessEntities.ModuleBE.Upload.Up_PurchaseOrderDetailBE();
            oAPPBOK_BookingBE.PurchaseOrder.ChaseStatus = rblChaseStatus.SelectedValue == "2" ? "Open" : (rblChaseStatus.SelectedValue == "3" ? "Close" : null);

            //-----------------Phase 20 R2 point 3---------------------------------------------//
            if (!string.IsNullOrEmpty(msPO.SelectedPO))
                oAPPBOK_BookingBE.PurchaseOrder.PurchaseOrderIds = msPO.SelectedPO;

            if (!string.IsNullOrEmpty(UcVikingSku.SelectedSKUName))
                oAPPBOK_BookingBE.PurchaseOrder.Direct_code = UcVikingSku.SelectedSKUName;

            if (!string.IsNullOrEmpty(msSKU.SelectedSKUName))
                oAPPBOK_BookingBE.PurchaseOrder.OD_Code = msSKU.SelectedSKUName;
            //------------------------------------------------------------------------//

            this.SetSession(oAPPBOK_BookingBE);
            oAPPBOK_BookingBE.PageCount = Page;
            List<APPBOK_BookingBE> lstPORec = oAPPBOK_BookingBAL.GetPOReconciliationBAL(oAPPBOK_BookingBE);
            if (lstPORec != null && lstPORec.Count > 0)
            {
                pager1.ItemCount = lstPORec[0].RecordCount;
                pager1.Visible = true;

                gvPOReCon.DataSource = null;
                gvPOReCon.DataSource = lstPORec;
                ViewState["PORec"] = lstPORec;
                btnExportToExcel.Visible = true;
            }
            else
            {
                btnChase.Visible = false;
                btnExportToExcel.Visible = false;
            }

            gvPOReCon.DataBind();
        }

    }

   

    protected void btnBack_Click(object sender, EventArgs e)
    {
        EncryptQueryString("ScheduledPOReconciliation.aspx");
    }

    protected void btnChase_Click(object sender, EventArgs e)
    {
        try
        {
            foreach (GridViewRow row in gvPOReCon.Rows)
            {
                CheckBox chkSelect = row.FindControl("chkSelect") as CheckBox;
                HiddenField hdnPOId = row.FindControl("hdnPOId") as HiddenField;
                HiddenField hdnSiteId = row.FindControl("hdnSiteId") as HiddenField;
                HiddenField hdnVendorId = row.FindControl("hdnVendorId") as HiddenField;
                Label ltSchuduleNotBookedIn = row.FindControl("ltSchuduleNotBookedIn") as Label;
                Label ltNotScheduledBookedIn = row.FindControl("ltNotScheduledBookedIn") as Label;
                Label ltBookingRef = row.FindControl("ltBookingRef") as Label;
                Label ltPoNumber = row.FindControl("ltPoNumber") as Label;
                Label ltDeliveryDate = row.FindControl("ltDeliveryDate") as Label;
                Label ltCarrier = row.FindControl("ltCarrier") as Label;
                Label ltScheduleDate = row.FindControl("ltScheduleDate") as Label;
                HiddenField hdnMultiPONo = row.FindControl("hdnMultiPONo") as HiddenField;
                HiddenField hdnMultiPOIDs = row.FindControl("hdnMultiPOIDs") as HiddenField;
                HiddenField hdnOrderRaised = row.FindControl("hdnOrderRaised") as HiddenField;
                HiddenField hdnCountry = row.FindControl("hdnCountry") as HiddenField;
                


                string BookingType = string.Empty;
                if (chkSelect != null && ltPoNumber != null)
                {
                    if (chkSelect.Checked)
                    {
                        ViewState["VendorID"] = Convert.ToInt32(hdnVendorId.Value);
                        ViewState["SiteId"] = Convert.ToInt32(hdnSiteId.Value);
                        ViewState["BookingRef"] = ltBookingRef.Text;
                        ViewState["PurchaseOrderNo"] = ltPoNumber.Text;
                        ViewState["DeliveryDate"] = ltDeliveryDate.Text;
                        ViewState["CarrierName"] = ltCarrier.Text;
                        ViewState["ScheduleDate"] = ltScheduleDate.Text;
                        ViewState["MultiPONo"] = hdnMultiPONo.Value.Trim(',');
                        ViewState["MultiPOIDs"] = hdnMultiPOIDs.Value.Trim(',');
                        ViewState["PoId"] = hdnPOId.Value;
                        ViewState["OrderRaised"] = hdnOrderRaised.Value;
                        ViewState["Country"] = hdnCountry.Value;

                        #region Setting Booking Type
                        if (!string.IsNullOrEmpty(ltSchuduleNotBookedIn.Text) && ltSchuduleNotBookedIn.Text.ToLower() == "yes")
                        {
                            BookingType = "SNBI";
                        }
                        else if (!string.IsNullOrEmpty(ltNotScheduledBookedIn.Text) && ltNotScheduledBookedIn.Text.ToLower() == "yes")
                        {
                            BookingType = "NSBI";
                        }
                        #endregion

                        #region Getting New Po Chase Communication ID and setting it in Viewstate
                        APPBOK_CommunicationBAL oAPPBOK_CommunicationBAL = new APPBOK_CommunicationBAL();
                        int? POChaseCommunicationId ;
                        POChaseCommunicationId = oAPPBOK_CommunicationBAL.GetPOChaseCommunicationIDBAL();
                        ViewState["POChaseCommunicationId"] = POChaseCommunicationId;
                        #endregion

                        ChaseMailToVendor(Convert.ToInt32(hdnVendorId.Value), Convert.ToInt32(hdnSiteId.Value), BookingType);
                    }
                }

            }
            GetSession();

        }
        catch (Exception ex)
        {
            LogUtility.SaveErrorLogEntry(ex);
        }

    }


    protected void btnNoAction_Click(object sender, EventArgs e) {
        try {

            string PoIds = string.Empty;
            foreach (GridViewRow row in gvPOReCon.Rows) {
                CheckBox chkSelect = row.FindControl("chkSelect") as CheckBox;
                Label ltPoNumber = row.FindControl("ltPoNumber") as Label;
                HiddenField hdnPOId = row.FindControl("hdnPOId") as HiddenField;

                if (chkSelect != null && ltPoNumber != null) {
                    if (chkSelect.Checked) {
                        if (PoIds == string.Empty) {
                            PoIds = hdnPOId.Value;
                        }
                        else {
                            PoIds += "," + hdnPOId.Value;
                        }
                    }
                }
            }

            UP_PurchaseOrderDetailBAL oUP_PurchaseOrderDetailBAL = new UP_PurchaseOrderDetailBAL();
            Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE = new Up_PurchaseOrderDetailBE();

            oUp_PurchaseOrderDetailBE.Action = "UpdatePOReconciliationStatus";
            oUp_PurchaseOrderDetailBE.PurchaseOrderIds = PoIds;
            oUP_PurchaseOrderDetailBAL.UpdatePOReconciliationBAL(oUp_PurchaseOrderDetailBE);

            GetSession();

        }
        catch (Exception ex) {
            LogUtility.SaveErrorLogEntry(ex);
        }
    }

    protected void gvPOReCon_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        if (ViewState["PORec"] != null)
        {
            gvPOReCon.PageIndex = e.NewPageIndex;
            gvPOReCon.DataSource = (List<APPBOK_BookingBE>)ViewState["PORec"];
            gvPOReCon.DataBind();
        }
    }

    protected void gvPOReCon_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType != DataControlRowType.Header
            && e.Row.RowType != DataControlRowType.Footer)
        {

            #region Showing single Checkbox for same Bookingid

            Label ltSchuduleNotBookedIn = (Label)e.Row.FindControl("ltSchuduleNotBookedIn") ;
            Label ltNotScheduledBookedIn = (Label)e.Row.FindControl("ltNotScheduledBookedIn");
            HiddenField hdnChaseStatus = (HiddenField)e.Row.FindControl("hdnChaseStatus");
            HiddenField hdnBookingId = (HiddenField)e.Row.FindControl("hdnBookingId");
            CheckBox chkSelect = (CheckBox)e.Row.FindControl("chkSelect");

            string BookingType = string.Empty;

            if (ltSchuduleNotBookedIn != null)
            {
                if (!string.IsNullOrEmpty(ltSchuduleNotBookedIn.Text) && ltSchuduleNotBookedIn.Text.ToLower() == "yes")
                {
                  
                    if (ViewState["BookingID"] != null && hdnBookingId != null)
                    {
                        if (hdnBookingId.Value.Equals(Convert.ToString(ViewState["BookingID"])))
                        {
                            chkSelect.Visible = false;
                            ViewState["BookingID"] = hdnBookingId.Value;
                        }
                        else
                        {
                            ViewState["BookingID"] = hdnBookingId.Value;
                        }
                    }
                    else if (hdnBookingId != null) { ViewState["BookingID"] = hdnBookingId.Value; }
                }
            }

            //If Hit is applied then mail cant be sent
            if (hdnChaseStatus != null && hdnChaseStatus.Value == "2" && hdnChaseStatus.Value == "3")
            {
                chkSelect.Visible = false;
            }


            #endregion

            #region Setting Chase Status
            HyperLink hlPOStatus = (HyperLink)e.Row.FindControl("hlPOStatus");
            Label ltPOStatus = (Label)e.Row.FindControl("ltPOStatus");            
            Label ltPoNumber = (Label)e.Row.FindControl("ltPoNumber");
            HiddenField hdnPOId = (HiddenField)e.Row.FindControl("hdnPOId");
            HiddenField hdnSiteId = (HiddenField)e.Row.FindControl("hdnSiteId");
            HiddenField hdnPOChaseCommunicationId = (HiddenField)e.Row.FindControl("hdnPOChaseCommunicationId");

            if (ltPOStatus != null)
            {
                if (ltPOStatus.Text.ToLower() == "open")
                {
                    ltPOStatus.Visible = true;
                    hlPOStatus.Visible = false;
                }
                else if (ltPOStatus.Text.ToLower() == "close")
                {
                    ltPOStatus.Visible = false;
                    hlPOStatus.Visible = true;
                    if (hdnChaseStatus != null && hdnChaseStatus.Value == "2")
                    {
                        hlPOStatus.NavigateUrl = EncryptQuery("../../OnTimeInFull/LeadTimeCalculationsSkuOverview.aspx?PurchaseOrderNo=" + ltPoNumber.Text + "&SiteId=" + hdnSiteId.Value);
                    }
                    else if (hdnChaseStatus != null && hdnChaseStatus.Value == "1")
                    {
                        hlPOStatus.NavigateUrl = EncryptQuery("POReconciliation_ReSendCommunication.aspx?POChaseCommunicationId="+hdnPOChaseCommunicationId.Value);
                        hlPOStatus.Attributes.Add("target","_blank");
                    }
                }
                else if (ltPOStatus.Text.ToLower() == "no action reqd") {
                    ltPOStatus.Visible = true;
                    hlPOStatus.Visible = false;
                    ltPOStatus.Text = "NO ACTION REQD";
                }
            }
            #endregion

            #region Apply Hit 
            Label ltScheduleDate = (Label)e.Row.FindControl("ltScheduleDate");
            Label ltDueDate = (Label)e.Row.FindControl("ltDueDate");
            HyperLink hlApplyHit = (HyperLink)e.Row.FindControl("hlApplyHit");
            Label ltApplyHit = (Label)e.Row.FindControl("ltApplyHit");
            HiddenField hdnIsPOManuallyAdded = (HiddenField)e.Row.FindControl("hdnIsPOManuallyAdded");
            HiddenField hdnOrderRaised = (HiddenField)e.Row.FindControl("hdnOrderRaised");
            Label ltDeliveryDate = (Label)e.Row.FindControl("ltDeliveryDate");
            if (ltScheduleDate != null)
            {
                if (!string.IsNullOrEmpty(ltScheduleDate.Text))
                {
                    DateTime dScheduleDate, dDueDate, dDeliveryDate;
                    dScheduleDate = DateTime.ParseExact(ltScheduleDate.Text.Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    dDueDate = DateTime.ParseExact(ltDueDate.Text.Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture);


                    if (dScheduleDate == dDueDate)
                    {
                        hlApplyHit.Visible = true;
                        ltApplyHit.Visible = false;
                        hlApplyHit.Text = "APPLY HIT";
                        hlApplyHit.NavigateUrl = EncryptQuery("../../OnTimeInFull/LeadTimeCalculationsSkuEdit.aspx?PreviousPage=PORec&PurchaseOrderNo=" + ltPoNumber.Text + "&SiteId=" + hdnSiteId.Value + "&PageMode=Add");

                        if (string.IsNullOrEmpty(ltPoNumber.Text) || hdnIsPOManuallyAdded.Value.ToLower() == "true")
                        {
                            hlApplyHit.Visible = false;
                        }
                        if (hdnChaseStatus != null && hdnChaseStatus.Value == "2")
                        {
                            hlApplyHit.Visible = false;
                            ltApplyHit.Visible = true;
                            ltApplyHit.Text = "APPLIED";
                        }
                        if (hdnChaseStatus != null && hdnChaseStatus.Value == "1")
                        {
                            
                            Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE = new Up_PurchaseOrderDetailBE();
                            UP_PurchaseOrderDetailBAL oUP_PurchaseOrderDetailBAL = new UP_PurchaseOrderDetailBAL();
                            oUp_PurchaseOrderDetailBE.Action = "CheckIfOTIFHIT";
                            oUp_PurchaseOrderDetailBE.Purchase_order = ltPoNumber.Text;
                            oUp_PurchaseOrderDetailBE.Site = new BusinessEntities.ModuleBE.AdminFunctions.MAS_SiteBE();
                            oUp_PurchaseOrderDetailBE.Site.SiteID = Convert.ToInt32(hdnSiteId.Value);
                            oUp_PurchaseOrderDetailBE.Order_raised = Convert.ToDateTime(hdnOrderRaised.Value);

                            bool IsOTIFHIt = false;
                            IsOTIFHIt = oUP_PurchaseOrderDetailBAL.CheckIfOTIFHitBAL(oUp_PurchaseOrderDetailBE);
                            if (IsOTIFHIt)
                            {
                                hlApplyHit.Visible = false;
                                ltApplyHit.Visible = true;
                                ltApplyHit.Text = "APPLIED";
                            }

                        }


                    }
                    else if (dScheduleDate < dDueDate)
                    {
                        hlApplyHit.Visible = false;
                        ltApplyHit.Visible = true;
                        ltApplyHit.Text = "EARLY";
                    }
                    else if (dScheduleDate > dDueDate)
                    {
                        hlApplyHit.Visible = false;
                        ltApplyHit.Visible = true;
                        ltApplyHit.Text = "LATE";
                    }

                    if (!string.IsNullOrEmpty(ltDeliveryDate.Text))
                    {
                        dDeliveryDate = DateTime.ParseExact(ltDeliveryDate.Text.Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture);

                        if (dDeliveryDate < dDueDate)
                        {
                            hlApplyHit.Visible = false;
                            ltApplyHit.Visible = true;
                            ltApplyHit.Text = "EARLY";
                        }
                    }
                }
            }

            #endregion

            #region set booking time
            int WeekDay = Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "WeekDay"));

            if (WeekDay > 0) {
                DateTime ScheduleDate = Convert.ToDateTime(DataBinder.Eval(e.Row.DataItem, "ScheduleDate"));
                Label ltdayname = (Label)e.Row.FindControl("ltdayname");

                string strWeekDay = ScheduleDate.DayOfWeek.ToString().ToUpper();
                if (strWeekDay == "SUNDAY") {
                    if (WeekDay != 1)
                        ltdayname.Text = ScheduleDate.AddDays(-1).DayOfWeek.ToString().ToUpper();
                }
                else if (strWeekDay == "MONDAY") {
                    if (WeekDay != 2)
                        ltdayname.Text = ScheduleDate.AddDays(-1).DayOfWeek.ToString().ToUpper();
                }
                else if (strWeekDay == "TUESDAY") {
                    if (WeekDay != 3)
                        ltdayname.Text = ScheduleDate.AddDays(-1).DayOfWeek.ToString().ToUpper();
                }
                else if (strWeekDay == "WEDNESDAY") {
                    if (WeekDay != 4)
                        ltdayname.Text = ScheduleDate.AddDays(-1).DayOfWeek.ToString().ToUpper();
                }
                else if (strWeekDay == "THURSDAY") {
                    if (WeekDay != 5)
                        ltdayname.Text = ScheduleDate.AddDays(-1).DayOfWeek.ToString().ToUpper();
                }
                else if (strWeekDay == "FRIDAY") {
                    if (WeekDay != 6)
                        ltdayname.Text = ScheduleDate.AddDays(-1).DayOfWeek.ToString().ToUpper();
                }
                else if (strWeekDay == "SATURDAY") {
                    if (WeekDay != 7)
                        ltdayname.Text = ScheduleDate.AddDays(-1).DayOfWeek.ToString().ToUpper();
                }
            }
            #endregion
        }
    }

    protected void btnExport_Click(object sender, EventArgs e)
    {
        APPBOK_BookingBE oAPPBOK_BookingBE = new APPBOK_BookingBE();
        APPBOK_BookingBAL oAPPBOK_BookingBAL = new APPBOK_BookingBAL();

        if (GetQueryStringValue("PreviousPage") != null && GetQueryStringValue("PreviousPage").ToString() == "POInquiry") {  //----------phase 18 R2 point 10 -------------------------------//
            oAPPBOK_BookingBE.PurchaseOrder = new BusinessEntities.ModuleBE.Upload.Up_PurchaseOrderDetailBE();
            oAPPBOK_BookingBE.PurchaseOrder.Purchase_order = GetQueryStringValue("Purchase_order");
        }
        else {
            oAPPBOK_BookingBE.SelectedVendorIDs = msVendor.SelectedVendorIDs;
            oAPPBOK_BookingBE.SelectedSiteIds = msSite.SelectedSiteIDs;
            oAPPBOK_BookingBE.FromDate = Utilities.Common.TextToDateFormat(hdnJSFromDt.Value);
            oAPPBOK_BookingBE.ToDate = Utilities.Common.TextToDateFormat(hdnJSToDt.Value);
            oAPPBOK_BookingBE.BookingType = rblType.SelectedValue == "2" ? "SNBI" : (rblType.SelectedValue == "3" ? "NSBI" : null);
            oAPPBOK_BookingBE.PurchaseOrder = new BusinessEntities.ModuleBE.Upload.Up_PurchaseOrderDetailBE();
            oAPPBOK_BookingBE.PurchaseOrder.ChaseStatus = rblChaseStatus.SelectedValue == "2" ? "Open" : (rblChaseStatus.SelectedValue == "3" ? "Close" : null);

            //-----------------Phase 20 R2 point 3---------------------------------------------//
            if (!string.IsNullOrEmpty(msPO.SelectedPO))
                oAPPBOK_BookingBE.PurchaseOrder.PurchaseOrderIds = msPO.SelectedPO;

            if (!string.IsNullOrEmpty(UcVikingSku.SelectedSKUName))
                oAPPBOK_BookingBE.PurchaseOrder.Direct_code = UcVikingSku.SelectedSKUName;

            if (!string.IsNullOrEmpty(msSKU.SelectedSKUName))
                oAPPBOK_BookingBE.PurchaseOrder.OD_Code = msSKU.SelectedSKUName;
            //------------------------------------------------------------------------//
        }
        oAPPBOK_BookingBE.PageCount = 0;
        this.SetSession(oAPPBOK_BookingBE);

        List<APPBOK_BookingBE> lstPORec = oAPPBOK_BookingBAL.GetPOReconciliationBAL(oAPPBOK_BookingBE);
        if (lstPORec != null && lstPORec.Count > 0)
        {
            gvPoReConExcel.DataSource = null;
            gvPoReConExcel.DataSource = lstPORec;
           // ViewState["PORec"] = lstPORec;
        }
        else
        {
            btnChase.Visible = false;
        }

        gvPoReConExcel.DataBind();
        LocalizeGridHeader(gvPoReConExcel);
        WebCommon.ExportHideHidden("ScheduledPOReconciliation", gvPoReConExcel,true);

    }


    #endregion

    #region Methods

    private void BindDropDowns()
    {
        /* Report Type Data binding */
        var listItem = new ListItem(string.Format("{0} ", WebCommon.getGlobalResourceValue("AllOnly")), "1");
        listItem.Selected = true;
        rblType.Items.Add(listItem);
        listItem = new ListItem(WebCommon.getGlobalResourceValue("ScheduledNotBookedIn"), "2");
        rblType.Items.Add(listItem);
        listItem = new ListItem(WebCommon.getGlobalResourceValue("BookedInNotScheduled"), "3");
        rblType.Items.Add(listItem);

        listItem = new ListItem(string.Format("{0} ", WebCommon.getGlobalResourceValue("AllOnly")), "1");
        listItem.Selected = true;
        rblChaseStatus.Items.Add(listItem);
        listItem = new ListItem(WebCommon.getGlobalResourceValue("Open"), "2");
        rblChaseStatus.Items.Add(listItem);
        listItem = new ListItem(WebCommon.getGlobalResourceValue("Closed"), "3");
        rblChaseStatus.Items.Add(listItem);
    }
   
    private void SetSession(APPBOK_BookingBE oAPPBOK_BookingBE)
    {
        Session["POReconciliation"] = null;
        Session.Remove("POReconciliation");

        Hashtable htPOReconciliation = new Hashtable();
        htPOReconciliation.Add("SelectedVendorIDs", oAPPBOK_BookingBE.SelectedVendorIDs);
        htPOReconciliation.Add("SelectedSiteIDs", oAPPBOK_BookingBE.SelectedSiteIds);
        htPOReconciliation.Add("FromDate", oAPPBOK_BookingBE.FromDate);
        htPOReconciliation.Add("ToDate", oAPPBOK_BookingBE.ToDate);
        htPOReconciliation.Add("BookingType", oAPPBOK_BookingBE.BookingType);
        htPOReconciliation.Add("ChaseStatus", oAPPBOK_BookingBE.PurchaseOrder.ChaseStatus);
        //-----------------Phase 20 R2 point 3---------------------------------------------//
        htPOReconciliation.Add("PurchaseOrderIds", oAPPBOK_BookingBE.PurchaseOrder.PurchaseOrderIds);
        htPOReconciliation.Add("Direct_code", oAPPBOK_BookingBE.PurchaseOrder.Direct_code);
        htPOReconciliation.Add("OD_Code", oAPPBOK_BookingBE.PurchaseOrder.OD_Code);
        //------------------------------------------------------------------------//
        Session["POReconciliation"] = htPOReconciliation;
    }

    private void GetSession()
    {

        APPBOK_BookingBE oAPPBOK_BookingBE = new APPBOK_BookingBE();
        APPBOK_BookingBAL oAPPBOK_BookingBAL = new APPBOK_BookingBAL();


        if (Session["POReconciliation"] != null)
        {
            Hashtable htPOReconciliation = (Hashtable)Session["POReconciliation"];

            oAPPBOK_BookingBE.SelectedVendorIDs = (htPOReconciliation.ContainsKey("SelectedVendorIDs") && htPOReconciliation["SelectedVendorIDs"] != null) ? htPOReconciliation["SelectedVendorIDs"].ToString() : null;
            oAPPBOK_BookingBE.SelectedSiteIds = (htPOReconciliation.ContainsKey("SelectedSiteIDs") && htPOReconciliation["SelectedSiteIDs"] != null) ? htPOReconciliation["SelectedSiteIDs"].ToString() : null;
            oAPPBOK_BookingBE.FromDate = (htPOReconciliation.ContainsKey("FromDate") && htPOReconciliation["FromDate"] != null) ? Convert.ToDateTime(htPOReconciliation["FromDate"]) : (DateTime?)null;
            oAPPBOK_BookingBE.ToDate = (htPOReconciliation.ContainsKey("ToDate") && htPOReconciliation["ToDate"] != null) ? Convert.ToDateTime(htPOReconciliation["ToDate"]) : (DateTime?)null;
            oAPPBOK_BookingBE.BookingType = (htPOReconciliation.ContainsKey("BookingType") && htPOReconciliation["BookingType"] != null) ? htPOReconciliation["BookingType"].ToString() : null;
            oAPPBOK_BookingBE.PurchaseOrder = new BusinessEntities.ModuleBE.Upload.Up_PurchaseOrderDetailBE();
            oAPPBOK_BookingBE.PurchaseOrder.ChaseStatus = (htPOReconciliation.ContainsKey("ChaseStatus") && htPOReconciliation["ChaseStatus"] != null) ? htPOReconciliation["ChaseStatus"].ToString() : null;
            //-----------------Phase 20 R2 point 3---------------------------------------------//      
            oAPPBOK_BookingBE.PurchaseOrder.PurchaseOrderIds = msPO.SelectedPO;    
            oAPPBOK_BookingBE.PurchaseOrder.Direct_code = UcVikingSku.SelectedSKUName;   
            oAPPBOK_BookingBE.PurchaseOrder.OD_Code = msSKU.SelectedSKUName;
            //------------------------------------------------------------------------//

            List<APPBOK_BookingBE> lstPORec = oAPPBOK_BookingBAL.GetPOReconciliationBAL(oAPPBOK_BookingBE);
            if (lstPORec != null && lstPORec.Count > 0)
            {
                gvPOReCon.DataSource = null;
                gvPOReCon.DataSource = lstPORec;
                ViewState["PORec"] = lstPORec;
            }

            gvPOReCon.DataBind();

            ucpnlsearch.Visible = false;
            ucpnlResult.Visible = true;

            //CommonPage c = new CommonPage();
            //c.LocalizeGridHeader(gvPOReCon);

        }


    }

    /// <summary>
    /// Method to show Vendor on the basis of selected Template
    /// </summary>
    public override void TemplateSelectedIndexChanged()
    {
        base.TemplateSelectedIndexChanged();

        int VendorTemplateId = Convert.ToInt32(ucVendorTemplateSelect.innerControlddlTemplate.SelectedValue);

        ListBox lstSelectedVendor = (ListBox)msVendor.FindControl("lstRight");
        ListBox lstVendor = (ListBox)msVendor.FindControl("ucVendor").FindControl("lstLeft");

        if (VendorTemplateId > 0)
        {

            SCT_TemplateBAL oSCT_TemplateBAL = new SCT_TemplateBAL();
            SCT_TemplateBE oSCT_TemplateBE = new SCT_TemplateBE();

            oSCT_TemplateBE.VendorTemplateID = VendorTemplateId;
            oSCT_TemplateBE.Action = "GetVendorTemplateInfobyID";
            List<SCT_TemplateBE> lstVendorTemplate = oSCT_TemplateBAL.GetVendorTemplateById(oSCT_TemplateBE);

            if (lstVendorTemplate != null && lstVendorTemplate.Count > 0)
            {
                lstSelectedVendor.Items.Clear();
                msVendor.innerControlHiddenField.Value = string.Empty;
                msVendor.innerControlHiddenField2.Value = string.Empty;
                for (int iCount = 0; iCount < lstVendorTemplate.Count; iCount++)
                {
                    lstSelectedVendor.Items.Add(new ListItem(lstVendorTemplate[iCount].Vendor.Vendor_Name, lstVendorTemplate[iCount].Vendor.VendorID.ToString()));
                    msVendor.SelectedVendorIDs = lstVendorTemplate[iCount].Vendor.VendorID.ToString();
                    msVendor.SelectedVendorName = lstVendorTemplate[iCount].Vendor.Vendor_Name.ToString();
                }
            }
        }
        else
        {
            lstSelectedVendor.Items.Clear();
            msVendor.innerControlHiddenField.Value = string.Empty;
            msVendor.innerControlHiddenField2.Value = string.Empty;
        }
    }
    #endregion


    #region Mail

    private void ChaseMailToVendor(int vendorid, int siteid, string BookingType)
    {
        string[] sentTo;
        string[] language;
        string[] VendorData = new string[2];

        VendorData = CommonPage.GetVendorEmailsWithLanguage(vendorid, siteid);
        ViewState["VendorEmailIds"] = VendorData[0];

        if (VendorData.Length > 0 && !string.IsNullOrEmpty(VendorData[0]))
        {
            sentTo = VendorData[0].Split(',');
            language = VendorData[1].Split(',');

            for (int j = 0; j < sentTo.Length; j++)
            {
                if (sentTo[j] != " ")
                {
                    SendMailToVendors(sentTo[j], language[j], BookingType);
                }
            }
        }

        Page.UICulture = Convert.ToString(Session["CultureInfo"]);
    }

    private void SendMailToVendors(string toAddress, string language, string BookingType)
    {
        if (!string.IsNullOrEmpty(toAddress))
        {
            BackOrderBAL oBackOrderBAL = new BackOrderBAL();
            BackOrderBE oBackOrderBE = new BackOrderBE();

            oBackOrderBE.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();
            oBackOrderBE.User = new SCT_UserBE();
            oBackOrderBE.Action = "GetVendorSiteAddress";
            oBackOrderBE.Vendor.VendorID = Convert.ToInt32(ViewState["VendorID"]);
            oBackOrderBE.SiteID = Convert.ToInt32(ViewState["SiteId"]);
            var lstBackOrderBE = oBackOrderBAL.GetVendorDetailBAL(oBackOrderBE);

            if (lstBackOrderBE != null && lstBackOrderBE.Count > 0)
            {

                #region Logic to Save & Send email ...
                var oAPPBOK_CommunicationBAL = new APPBOK_CommunicationBAL();
                var oLanguages = oAPPBOK_CommunicationBAL.GetLanguages();

                foreach (MAS_LanguageBE objLanguage in oLanguages)
                {
                    bool MailSentInLanguage = false;
                    if (objLanguage.Language.ToLower() == language.ToLower())
                        MailSentInLanguage = true;

                    var path = AppDomain.CurrentDomain.BaseDirectory.ToLower();
                    var templateFile = string.Empty;
                    if (BookingType == "SNBI")
                    {
                        templateFile = string.Format(@"{0}emailtemplates/Appointment/POReconciliationSNBI.english.htm", path);
                    }
                    else
                    {
                        templateFile = string.Format(@"{0}emailtemplates/Appointment/POReconciliationNSBI.english.htm", path);
                    }

                    #region Setting Region as per the language ...
                    switch (objLanguage.LanguageID)
                    {
                        case 1:
                            Page.UICulture = clsConstants.EnglishISO; //"en-US"; // English
                            break;
                        case 2:
                            Page.UICulture = clsConstants.FranceISO; //"fr"; // France
                            break;
                        case 3:
                            Page.UICulture = clsConstants.GermanyISO; //"de"; // German
                            break;
                        case 4:
                            Page.UICulture = clsConstants.NederlandISO; //"nl"; // Dutch
                            break;
                        case 5:
                            Page.UICulture = clsConstants.SpainISO; //"es"; // Spanish
                            break;
                        case 6:
                            Page.UICulture = clsConstants.ItalyISO; //"it"; // Italy
                            break;
                        case 7:
                            Page.UICulture = clsConstants.CzechISO; //"cz"; // Czech
                            break;
                        default:
                            Page.UICulture = clsConstants.EnglishISO; // "en-US"; // English
                            break;
                    }

                    #endregion

                    #region  Prepairing html body format ...
                    string htmlBody = null;
                    using (StreamReader sReader = new StreamReader(templateFile.ToLower()))
                    {
                        htmlBody = sReader.ReadToEnd();

                        htmlBody = htmlBody.Replace("{Date}", WebCommon.getGlobalResourceValue("Date"));
                        htmlBody = htmlBody.Replace("{DateValue}", DateTime.Now.ToString("dd/MM/yyyy"));
                        htmlBody = htmlBody.Replace("{VendorCodeValue}", lstBackOrderBE[0].Vendor.Vendor_No);
                        if (BookingType == "SNBI")
                        {
                            htmlBody = htmlBody.Replace("{BookingReference}", WebCommon.getGlobalResourceValue("BookingReference"));
                        }
                        htmlBody = htmlBody.Replace("{BookingRefValue}", Convert.ToString(ViewState["BookingRef"]));
                        htmlBody = htmlBody.Replace("{VendorNameValue}", lstBackOrderBE[0].Vendor.VendorName);
                        htmlBody = htmlBody.Replace("{PurchaseOrderNumber}", WebCommon.getGlobalResourceValue("PurchaseOrderNumber"));
                        if (BookingType == "SNBI")
                        {
                            htmlBody = htmlBody.Replace("{PurchaseOrderNumberValue}", Convert.ToString(ViewState["MultiPONo"]));
                        }
                        else
                        {
                            htmlBody = htmlBody.Replace("{PurchaseOrderNumberValue}", Convert.ToString(ViewState["PurchaseOrderNo"]));
                        }
                        htmlBody = htmlBody.Replace("{VendorAddress1Value}", lstBackOrderBE[0].Vendor.address1);
                        htmlBody = htmlBody.Replace("{DeliveryDate}", WebCommon.getGlobalResourceValue("DeliveryDate"));
                        if (BookingType == "SNBI")
                        {
                            htmlBody = htmlBody.Replace("{DeliveryDateValue}", Convert.ToString(ViewState["ScheduleDate"]));
                        }
                        else
                        {
                            htmlBody = htmlBody.Replace("{DeliveryDateValue}", Convert.ToString(ViewState["DeliveryDate"]));
                        }

                        htmlBody = htmlBody.Replace("{VendorAddress2Value}", lstBackOrderBE[0].Vendor.address2);

                        if (BookingType == "SNBI")
                        {
                            htmlBody = htmlBody.Replace("{Carrier}", WebCommon.getGlobalResourceValue("Carrier"));
                            htmlBody = htmlBody.Replace("{CarrierValue}", Convert.ToString(ViewState["CarrierName"]));
                        }
                        if (!string.IsNullOrEmpty(lstBackOrderBE[0].Vendor.VMPPIN) && !string.IsNullOrWhiteSpace(lstBackOrderBE[0].Vendor.VMPPIN))
                            htmlBody = htmlBody.Replace("{VMPPIN}", lstBackOrderBE[0].Vendor.VMPPIN);
                        else
                            htmlBody = htmlBody.Replace("{VMPPIN}", string.Empty);

                        if (!string.IsNullOrEmpty(lstBackOrderBE[0].Vendor.VMPPOU) && !string.IsNullOrWhiteSpace(lstBackOrderBE[0].Vendor.VMPPOU))
                            htmlBody = htmlBody.Replace("{VMPPOU}", lstBackOrderBE[0].Vendor.VMPPOU);
                        else
                            htmlBody = htmlBody.Replace("{VMPPOU}", string.Empty);

                        if (!string.IsNullOrEmpty(lstBackOrderBE[0].Vendor.city) && !string.IsNullOrWhiteSpace(lstBackOrderBE[0].Vendor.city))
                            htmlBody = htmlBody.Replace("{VendorCity}", lstBackOrderBE[0].Vendor.city);
                        else
                            htmlBody = htmlBody.Replace("{VendorCity}", string.Empty);

                        htmlBody = htmlBody.Replace("{DearSirMadam}", WebCommon.getGlobalResourceValue("DearSirMadam"));

                        if (BookingType == "SNBI")
                        {
                            htmlBody = htmlBody.Replace("{POReconciliationSNBIText1}", WebCommon.getGlobalResourceValue("POReconciliationSNBIText1"));
                            htmlBody = htmlBody.Replace("{DeliveryDateValue}", Convert.ToString(ViewState["ScheduleDate"]));
                        }
                        else
                        {
                            htmlBody = htmlBody.Replace("{POReconciliationNSBIText1}", WebCommon.getGlobalResourceValue("POReconciliationNSBIText1"));
                            htmlBody = htmlBody.Replace("{POReconciliationNSBIText2}", WebCommon.getGlobalResourceValue("POReconciliationNSBIText2"));
                            htmlBody = htmlBody.Replace("{DeliveryDateValue}", Convert.ToString(ViewState["DeliveryDate"]));
                        }


                        htmlBody = htmlBody.Replace("{PortalLink}", "<a href=" + sPortalLink + ">" + sPortalLink + " </a>");
                        htmlBody = htmlBody.Replace("{Yoursfaithfully}", WebCommon.getGlobalResourceValue("Yoursfaithfully"));


                        var apAddress = string.Empty;
                        if (!string.IsNullOrEmpty(lstBackOrderBE[0].APAddress1) && !string.IsNullOrEmpty(lstBackOrderBE[0].APAddress2)
                            && !string.IsNullOrEmpty(lstBackOrderBE[0].APAddress3) && !string.IsNullOrEmpty(lstBackOrderBE[0].APAddress4)
                            && !string.IsNullOrEmpty(lstBackOrderBE[0].APAddress5) && !string.IsNullOrEmpty(lstBackOrderBE[0].APAddress6))
                        {
                            apAddress = string.Format("{0},&nbsp;{1},&nbsp;{2},&nbsp;{3},&nbsp;{4},&nbsp;{5}", lstBackOrderBE[0].APAddress1, lstBackOrderBE[0].APAddress2,
                                lstBackOrderBE[0].APAddress3, lstBackOrderBE[0].APAddress4, lstBackOrderBE[0].APAddress5, lstBackOrderBE[0].APAddress6);
                        }
                        else if (!string.IsNullOrEmpty(lstBackOrderBE[0].APAddress1) && !string.IsNullOrEmpty(lstBackOrderBE[0].APAddress2)
                            && !string.IsNullOrEmpty(lstBackOrderBE[0].APAddress3) && !string.IsNullOrEmpty(lstBackOrderBE[0].APAddress4)
                            && !string.IsNullOrEmpty(lstBackOrderBE[0].APAddress5))
                        {
                            apAddress = string.Format("{0},&nbsp;{1},&nbsp;{2},&nbsp;{3},&nbsp;{4}", lstBackOrderBE[0].APAddress1, lstBackOrderBE[0].APAddress2,
                                lstBackOrderBE[0].APAddress3, lstBackOrderBE[0].APAddress4, lstBackOrderBE[0].APAddress5);
                        }
                        else if (!string.IsNullOrEmpty(lstBackOrderBE[0].APAddress1) && !string.IsNullOrEmpty(lstBackOrderBE[0].APAddress2)
                           && !string.IsNullOrEmpty(lstBackOrderBE[0].APAddress3) && !string.IsNullOrEmpty(lstBackOrderBE[0].APAddress4))
                        {
                            apAddress = string.Format("{0},&nbsp;{1},&nbsp;{2},&nbsp;{3}", lstBackOrderBE[0].APAddress1, lstBackOrderBE[0].APAddress2,
                                lstBackOrderBE[0].APAddress3, lstBackOrderBE[0].APAddress4);
                        }
                        else if (!string.IsNullOrEmpty(lstBackOrderBE[0].APAddress1) && !string.IsNullOrEmpty(lstBackOrderBE[0].APAddress2)
                           && !string.IsNullOrEmpty(lstBackOrderBE[0].APAddress3))
                        {
                            apAddress = string.Format("{0},&nbsp;{1},&nbsp;{2}", lstBackOrderBE[0].APAddress1, lstBackOrderBE[0].APAddress2,
                                lstBackOrderBE[0].APAddress3);
                        }
                        else if (!string.IsNullOrEmpty(lstBackOrderBE[0].APAddress1) && !string.IsNullOrEmpty(lstBackOrderBE[0].APAddress2))
                        {
                            apAddress = string.Format("{0},&nbsp;{1}", lstBackOrderBE[0].APAddress1, lstBackOrderBE[0].APAddress2);
                        }
                        else
                        {
                            apAddress = lstBackOrderBE[0].APAddress1;
                        }

                        if (!string.IsNullOrEmpty(lstBackOrderBE[0].APPincode))
                            apAddress = string.Format("{0},&nbsp;{1}", apAddress, lstBackOrderBE[0].APPincode);

                        htmlBody = htmlBody.Replace("{SiteAddress}", apAddress);



                    }
                    #endregion

                    #region Sending and saving email details and updating Chase Status ...

                    
                    var sentToWithLink = new System.Text.StringBuilder();
                    sentToWithLink.Append("<a target='_blank' href='" + Common.EncryptQuery("POReconciliation_ReSendCommunication.aspx.aspx?POChaseCommunicationId=" + ViewState["POChaseCommunicationId"]) + "' onclick='window.open(this.href, this.target,\"scrollbars=1,left=100px,top=100px,width=1000px,height=800px\"); return false;'>" + toAddress + "</a>" + System.Environment.NewLine);

                    var oAPPBOK_CommunicationBE = new APPBOK_CommunicationBE();
                    oAPPBOK_CommunicationBE.Action = "AddPOChaseCommunication";
                   
                     string SNBIEmailSubject = WebCommon.getGlobalResourceValue("SNBIEmailSubject");
                     string NSBIEmailSubject = WebCommon.getGlobalResourceValue("NSBIEmailSubject");

                    if (BookingType == "SNBI")
                    {
                        oAPPBOK_CommunicationBE.Subject = SNBIEmailSubject;
                    }
                    else
                    {
                        oAPPBOK_CommunicationBE.Subject = NSBIEmailSubject;
                    }
                    oAPPBOK_CommunicationBE.SentTo = toAddress;
                    oAPPBOK_CommunicationBE.POChaseSentDate = DateTime.Now;
                    oAPPBOK_CommunicationBE.Body = htmlBody;
                    oAPPBOK_CommunicationBE.SendByID = Convert.ToInt32(Session["UserID"]);
                    oAPPBOK_CommunicationBE.LanguageID = objLanguage.LanguageID;
                    oAPPBOK_CommunicationBE.MailSentInLanguage = MailSentInLanguage;
                    oAPPBOK_CommunicationBE.CommunicationStatus = "Initial";
                    oAPPBOK_CommunicationBE.SentToWithLink = sentToWithLink.ToString();
                    oAPPBOK_CommunicationBE.VendorEmailIds = Convert.ToString(ViewState["VendorEmailIds"]);
                    oAPPBOK_CommunicationBE.POChaseCommunicationId = Convert.ToInt32(ViewState["POChaseCommunicationId"]);                

                    APPBOK_CommunicationBAL oAPPBOK_CommunicationBAL1 = new APPBOK_CommunicationBAL();
                    var CommId = oAPPBOK_CommunicationBAL1.AddBookingCommunicationBAL(oAPPBOK_CommunicationBE);
                    if (MailSentInLanguage)
                    {
                        #region Update Chase Status and POChaseCommunicationId
                        //Update Chase Status and PO Chase CommunicationID
                        if (BookingType == "SNBI")
                        {
                            oAPPBOK_CommunicationBE.MultiPoIds = Convert.ToString(ViewState["MultiPOIDs"]);
                            oAPPBOK_CommunicationBE.Action = "UpdateChaseStatusSNBI";
                        }
                        else
                        {
                            oAPPBOK_CommunicationBE.Purchase_order = Convert.ToString(ViewState["PurchaseOrderNo"]);
                            oAPPBOK_CommunicationBE.SiteID = Convert.ToInt32( ViewState["SiteId"]);
                            oAPPBOK_CommunicationBE.OrderRaised = Convert.ToDateTime( ViewState["OrderRaised"]);
                            oAPPBOK_CommunicationBE.Country = Convert.ToString(ViewState["Country"]);
                            oAPPBOK_CommunicationBE.Action = "UpdateChaseStatusNSBI";
                        }
                        #endregion


                        #region Mail Send
                        APPBOK_CommunicationBAL oAPPBOK_CommunicationBAL2 = new APPBOK_CommunicationBAL();
                        var ChaseId = oAPPBOK_CommunicationBAL2.UpdateChaseStatusBAL(oAPPBOK_CommunicationBE);
                        //--------------------------------------------------------------------------

                        var emailToSubject = string.Empty;
                        var emailToAddress = toAddress;
                        var emailFromAddress = Convert.ToString(ConfigurationManager.AppSettings["fromAddress"]);
                        if (BookingType == "SNBI")
                        {
                            emailToSubject = SNBIEmailSubject;
                        }
                        else
                        {
                            emailToSubject = NSBIEmailSubject;
                        }
                        var emailBody = htmlBody;
                        Utilities.clsEmail oclsEmail = new Utilities.clsEmail();
                        oclsEmail.sendMail(emailToAddress, emailBody, emailToSubject, emailFromAddress, true);
                        #endregion
                    }
                    #endregion
                }


                #endregion
            }
        }
    }


    #endregion





}
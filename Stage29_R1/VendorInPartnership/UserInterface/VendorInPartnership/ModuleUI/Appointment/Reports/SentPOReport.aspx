﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    AutoEventWireup="true" CodeFile="SentPOReport.aspx.cs" Inherits="ModuleUI_Appointment_Reports_SentPOReport" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectStockPlanner.ascx" TagName="MultiSelectStockPlanner"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectPO.ascx" TagName="MultiSelectPO"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectVendor.ascx" TagName="MultiSelectVendor"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectSite.ascx" TagName="MultiSelectSite"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectSKU.ascx" TagName="MultiSelectSKU"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectItemClassification.ascx" TagName="MultiSelectItemClassification"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/ucVendorSelectionTemplate.ascx" TagName="VendorSelectTemplate"
    TagPrefix="cc1" %>
<%@ Register TagName="ucExportButton" TagPrefix="uc1" Src="~/CommonUI/UserControls/ucExportToExcel.ascx" %>
<%@ Register TagName="ucDate" TagPrefix="cc2" Src="~/CommonUI/UserControls/ucDate.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script src="../../../Scripts/jquery.tooltip.min.js" type="text/javascript"></script>
    <style type="text/css">
        .radio-fix label
        {
            margin-right: 10px !important;
            display: inline-block;
        }
        
        .radiobuttonlist label
        {
            margin-right: 0px;
        }
        .radio-fix-last label
        {
            margin-right: 10px !important;
        }
    </style>
    <script type="text/javascript" language="javascript">
        $(document).ready(function () {
            $("body").keypress(function (e) {
                if (e.which == 13 || e.keyCode == 13) {
                    $('#<%=btnGenerateReport.ClientID %>').click();
                    return false;
                }
            });
        });
        function setValue1(target) {
            document.getElementById('<%=hdnJSFromDt.ClientID %>').value = target.value;
        }

        function setValue2(target) {
            document.getElementById('<%=hdnJSToDt.ClientID %>').value = target.value;
        }
        function setValue3(target) {
            document.getElementById('<%=hdnJSDt.ClientID %>').value = target.value;
        }

    </script>
    <asp:ScriptManager ID="SM1" runat="server" AsyncPostBackTimeout="2400">
    </asp:ScriptManager>
    <asp:HiddenField ID="hdnJSFromDt" runat="server" />
    <asp:HiddenField ID="hdnJSToDt" runat="server" />
    <asp:HiddenField ID="hdnJSDt" runat="server" />
    <h2>
        <cc1:ucLabel ID="lblSentPurchaseOrderReport" runat="server" Text="Sent Purchase Order Report"></cc1:ucLabel>
    </h2>
    <div class="right-shadow">
        <div class="formbox">
            <%--<cc1:ucMultiView ID="mvDiscrepancyList" runat="server" ActiveViewIndex="0">--%>
            <cc1:ucPanel ID="pnlSearchScreen" runat="server">
                <table width="100%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                    <tr>
                        <td align="right">
                            <cc1:VendorSelectTemplate runat="server" ID="ucVendorTemplateSelect" />
                        </td>
                    </tr>
                </table>
                <table width="100%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                    <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblPurchaseOrderNo1" runat="server" Text="Purchase Order Number">
                            </cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="UcLabel3" runat="server">:</cc1:ucLabel>
                        </td>
                        <td>
                            <cc1:MultiSelectPO runat="server" ID="msPO" />
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblSite" runat="server" Text="Site">
                            </cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="UcLabel4" runat="server">:</cc1:ucLabel>
                        </td>
                        <td>
                            <cc1:MultiSelectSite runat="server" ID="msSite" />
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblStockPlanner" runat="server" Text="Stock Planner">
                            </cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="Label17" runat="server">:</cc1:ucLabel>
                        </td>
                        <td>
                            <cc1:MultiSelectStockPlanner runat="server" ID="msStockPlanner" />
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblVendor" runat="server" Text="Vendor">
                            </cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="UcLabel2" runat="server">:</cc1:ucLabel>
                        </td>
                        <td>
                            <cc1:MultiSelectVendor runat="server" ID="msVendor" />
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblDisplay" runat="server" Text="Display">
                            </cc1:ucLabel>
                        </td>
                        <td colspan="2">
                            <table style="width: 100%" class="radiobuttonlist radio-calendar" border="0">
                                <tr>
                                    <td style="width: 10%; font-weight: bold;">
                                        <cc1:ucRadioButton ID="rbAll" GroupName="Display" Checked="true" runat="server" Text=" All" />
                                    </td>
                                    <td style="font-weight: bold; width: 10%;">
                                        <cc1:ucRadioButton GroupName="Display" ID="rbDue" runat="server" Text="Due" />
                                    </td>
                                    <td style="font-weight: bold; width: 25%;">
                                        <cc1:ucRadioButton GroupName="Display" ID="rbDueBy" runat="server" Text=" Due By" />
                                        <cc1:ucTextbox ID="txtDate" runat="server" ClientIDMode="Static" CssClass="date"
                                            onchange="setValue3(this)" ReadOnly="True" Width="70px" />
                                        <%--<cc2:ucDate ClientIDMode="Static" ID="txtDate" runat="server" />--%>
                                    </td>
                                    <td style="font-weight: bold; width: 55%;">
                                        <cc1:ucRadioButton GroupName="Display" ID="rbRaisedBetween" runat="server" Text="Raised Between" />
                                        <cc1:ucTextbox ID="txtDateFrom" runat="server" ClientIDMode="Static" CssClass="date"
                                            onchange="setValue1(this)" ReadOnly="True" Width="70px" />
                                        <%--<cc2:ucDate ClientIDMode="Static" ID="txtDateFrom" runat="server" CssClass="date" />--%>
                                        <label>
                                            to</label>
                                        <cc1:ucTextbox ID="txtDateTo" runat="server" ClientIDMode="Static" CssClass="date"
                                            onchange="setValue2(this)" ReadOnly="True" Width="70px" />
                                        <%--<cc2:ucDate ClientIDMode="Static" ID="txtDateTo" runat="server" CssClass="date" />--%>
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </cc1:ucPanel>
            <cc1:ucPanel ID="pnlPotentialOutput" runat="server">
                <div class="button-row">
                                    <cc1:ucButton ID="btnExportToExcel" runat="server" CssClass="exporttoexcel" OnClick="btnExport_Click"
                                        Width="109px" Height="20px" />
                </div>
                <div id="ctl00_ContentPlaceHolder1_pnlGrid" class="fieldset-form" style="overflow-x:scroll;">
                <div id="divPrint" class="fixedTable">
                    <table cellspacing="1" cellpadding="0" border="0" align="center" width="100%">
                        <tr>
                            <td style="font-weight: bold;">
                                
                                <cc1:ucGridView ID="gvSentPoRequest" Width="130%" runat="server" CssClass="grid">
                                    <emptydatatemplate>
                                        <div style="text-align: center">
                                            <cc1:ucLabel ID="lblRecordNotFound" runat="server" Text="No record found"></cc1:ucLabel>
                                        </div>
                                    </emptydatatemplate>
                                    <columns>
                                        <asp:TemplateField HeaderText="PONumber" SortExpression="Purchase_order">
                                            <HeaderStyle Width="7%" HorizontalAlign="Left" />
                                            <ItemTemplate>                                                
                                                <asp:HyperLink ID="hplPONumber1" runat="server" Target="_blank" Text='<%# Eval("Purchase_order") %>'
                                                    NavigateUrl='<%# EncryptQuery("~/ModuleUI/StockOverview/Expedite/OpenPurchaseListing.aspx?po=" + Eval("Purchase_order") 
                                                    + "&status=" + Eval("Status")                                                   
                                                    + "&vendorId=" + Eval("Vendor.VendorID") 
                                                    + "&siteId=" + Eval("Site.SiteID") 
                                                    + "&page=SPOR&orderRaised=" + Eval("Order_raised","{0:dd/MM/yyyy}")                                                                                                        
                                                    )%>'></asp:HyperLink>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Status" SortExpression="Status">
                                            <HeaderStyle Width="5%" HorizontalAlign="Left" />
                                            <ItemTemplate>
                                                <cc1:ucLabel runat="server" ID="lblStatusValue" Font-Bold="true" Text='<%#Eval("Status") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Site" SortExpression="Site.SiteName">
                                            <HeaderStyle Width="10%" HorizontalAlign="Left" />
                                            <ItemTemplate>
                                                <cc1:ucLabel runat="server" ID="lblSiteNameValue" Text='<%#Eval("Site.SiteName") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" Width="30px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="VendorNumber" SortExpression="Vendor.Vendor_No">
                                            <HeaderStyle Width="10%" HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <cc1:ucLabel runat="server" ID="lblVendorNoValue" Text='<%#Eval("Vendor.Vendor_No") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="VendorName" SortExpression="Vendor.VendorName">
                                            <HeaderStyle Width="12%" HorizontalAlign="Left" />
                                            <ItemTemplate>
                                                <cc1:ucLabel runat="server" ID="lblVendorNameValue" Text='<%#Eval("Vendor.VendorName") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="StockPlannerNumber" SortExpression="Vendor.StockPlannerNumber">
                                            <HeaderStyle Width="8%" HorizontalAlign="Left" />
                                            <ItemTemplate>
                                                <cc1:ucLabel runat="server" ID="lblStockPlannerNumbers" Text='<%#Eval("Vendor.StockPlannerNumber") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" Width="30px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="StockPlannerName" SortExpression="Vendor.StockPlannerName">
                                            <HeaderStyle Width="10%" HorizontalAlign="Left" />
                                            <ItemTemplate>
                                                <cc1:ucLabel runat="server" ID="lblStockPlannerNames" Text='<%#Eval("Vendor.StockPlannerName") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" Width="30px" />
                                        </asp:TemplateField>
                                        <asp:BoundField HeaderText="RaisedDate" DataFormatString="{0:dd/MM/yyyy}" ItemStyle-Wrap="false"
                                            DataField="Order_raised" SortExpression="Order_raised">
                                            <HeaderStyle HorizontalAlign="Center" Width="7%" />
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:BoundField>
                                        <asp:BoundField HeaderText="DueDate" DataFormatString="{0:dd/MM/yyyy}" ItemStyle-Wrap="false"
                                            DataField="Original_due_date" SortExpression="Original_due_date">
                                            <HeaderStyle HorizontalAlign="Center" Width="7%" />
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:BoundField>
                                        <asp:TemplateField HeaderText="Send Method" SortExpression="SendMethod">
                                            <HeaderStyle Width="8%" HorizontalAlign="Left" />
                                            <ItemTemplate>
                                                <cc1:ucLabel runat="server" ID="lblSendMethod" Text='<%#Eval("SendMethod") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" Width="30px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Sent By" SortExpression="SentFrom">
                                            <HeaderStyle Width="8%" HorizontalAlign="Left" />
                                            <ItemTemplate>
                                                <cc1:ucLabel runat="server" ID="lblSentFrom" Text='<%#Eval("SentFrom") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" Width="30px" />
                                        </asp:TemplateField>
                                        <asp:BoundField HeaderText="SentDate" DataFormatString="{0:dd/MM/yyyy}" ItemStyle-Wrap="false"
                                            DataField="SentDate" SortExpression="SentDate">
                                            <HeaderStyle HorizontalAlign="Center" Width="10%" />
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:BoundField>
                                        <asp:TemplateField HeaderText="EmailSentTo" SortExpression="FirstSentToEmail">
                                            <HeaderStyle Width="12%" HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <asp:HyperLink ID="lnkFirstSentToEmail" runat="server" Target="_blank" Text='<%# Eval("SentTo") %>'
                                                    NavigateUrl='<%# EncryptQuery("SentPO_ReSendCommunication.aspx?PO=" + Eval("Purchase_order")
                                                    + "&SiteID=" + Eval("Site.SiteID")
                                                    + "&VendorID=" + Eval("Vendor.VendorID")
                                                    + "&SentDate=" + Eval("SentDate","{0:dd/MM/yyyy}")
                                                    + "&CommType=" + Eval("CommunicationType")
                                                    + "&LangId=" + Eval("LanguageID")
                                                    )%>'></asp:HyperLink>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="left" />
                                        </asp:TemplateField>
                                    </columns>
                                </cc1:ucGridView>
                                <div class="button-row">
                                    <cc1:PagerV2_8 ID="pager1" runat="server" OnCommand="pager_Command" GenerateGoToSection="false">
                                    </cc1:PagerV2_8>
                                </div>
                                <%--   <cc1:ucGridView ID="tempGridView" Visible="false" Width="100%" runat="server" CssClass="grid"
                                    AllowSorting="true" Style="overflow: auto;">
                                    <Columns>
                                        <asp:TemplateField HeaderText="PONumber" SortExpression="Purchase_order">
                                            <HeaderStyle Width="10px" HorizontalAlign="Left" />
                                            <ItemTemplate>
                                                <cc1:ucLinkButton ID="lnkPONumbr" runat="server" Text='<%# Eval("Purchase_order") %>'></cc1:ucLinkButton>
                                             
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Status" SortExpression="Status">
                                            <HeaderStyle Width="15px" HorizontalAlign="Left" />
                                            <ItemTemplate>
                                                <cc1:ucLabel runat="server" ID="lblStatusValue" Font-Bold="true" Text='<%#Eval("Status") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Site" SortExpression="Site.SiteName">
                                            <HeaderStyle Width="80px" HorizontalAlign="Left" />
                                            <ItemTemplate>
                                                <cc1:ucLabel runat="server" ID="lblSiteNameValue" Text='<%#Eval("Site.SiteName") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" Width="30px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="VendorNumber" SortExpression="Vendor.Vendor_No">
                                            <HeaderStyle Width="30px" HorizontalAlign="Left" />
                                            <ItemTemplate>
                                                <cc1:ucLabel runat="server" ID="lblVendorNoValue" Text='<%#Eval("Vendor.Vendor_No") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="VendorName" SortExpression="Vendor.VendorName">
                                            <HeaderStyle Width="150px" HorizontalAlign="Left" />
                                            <ItemTemplate>
                                                <cc1:ucLabel runat="server" ID="lblVendorNameValue" Text='<%#Eval("Vendor.VendorName") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="StockPlannerNumber" SortExpression="Vendor.StockPlannerNumber">
                                            <HeaderStyle Width="30px" HorizontalAlign="Left" />
                                            <ItemTemplate>
                                                <cc1:ucLabel runat="server" ID="lblStockPlannerNumbers" Text='<%#Eval("Vendor.StockPlannerNumber") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" Width="30px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="StockPlannerName" SortExpression="Vendor.StockPlannerName">
                                            <HeaderStyle Width="30px" HorizontalAlign="Left" />
                                            <ItemTemplate>
                                                <cc1:ucLabel runat="server" ID="lblStockPlannerNames" Text='<%#Eval("Vendor.StockPlannerName") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" Width="30px" />
                                        </asp:TemplateField>
                                        <asp:BoundField HeaderText="RaisedDate" DataFormatString="{0:dd-MM-yyyy}" ItemStyle-Wrap="false"
                                            DataField="Order_raised" SortExpression="Order_raised">
                                            <HeaderStyle HorizontalAlign="Center" Width="150px" />
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:BoundField>
                                        <asp:BoundField HeaderText="DueDate" DataFormatString="{0:dd-MM-yyyy}" ItemStyle-Wrap="false"
                                            DataField="Original_due_date" SortExpression="Original_due_date">
                                            <HeaderStyle HorizontalAlign="Center" Width="150px" />
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:BoundField>
                                        <asp:BoundField HeaderText="SentDate" DataFormatString="{0:dd/MM/yyyy}" ItemStyle-Wrap="false"
                                            DataField="SentDate" SortExpression="SentDate">
                                            <HeaderStyle HorizontalAlign="Center" Width="7%" />
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:BoundField>
                                        <asp:TemplateField HeaderText="EmailSentTo" SortExpression="FirstSentToEmail">
                                            <HeaderStyle Width="80px" HorizontalAlign="Left" />
                                            <ItemTemplate>
                                                <asp:HyperLink ID="lnkFirstSentToEmail" runat="server" Target="_blank" Text='<%# Eval("SentTo") %>'
                                                    NavigateUrl='<%# EncryptQuery("SentPO_ReSendCommunication.aspx?PO=" + Eval("Purchase_order")+"&SiteID=" + Eval("Site.SiteID")+"&VendorID=" + Eval("Vendor.VendorID"))%>'></asp:HyperLink>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                    </Columns>
                                </cc1:ucGridView>--%>
                            </td>
                        </tr>
                    </table>
                </div>
                    </div>
            </cc1:ucPanel>
            <%--</cc1:ucMultiView>--%>
            <div class="button-row">
                <cc1:ucButton ID="btnGenerateReport" runat="server" Text="Search" CssClass="button"
                    OnClick="btnGenerateReport_Click" />
                <cc1:ucButton ID="btnBackSearch" runat="server" Text="Back To Search Criteria" Visible="false"
                    CssClass="button" OnClick="btnBack_Click" />
            </div>
        </div>
    </div>
</asp:Content>

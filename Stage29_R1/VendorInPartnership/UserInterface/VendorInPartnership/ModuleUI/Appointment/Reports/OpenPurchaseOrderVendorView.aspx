﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    CodeFile="OpenPurchaseOrderVendorView.aspx.cs" Inherits="ModuleUI_Appointment_Reports_OpenPurchaseOrderVendorView"
    MaintainScrollPositionOnPostback="true" %>

<%@ Register TagName="ucDate" TagPrefix="cc2" Src="~/CommonUI/UserControls/ucDate.ascx" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectPO.ascx" TagName="MultiSelectPO"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectSite.ascx" TagName="MultiSelectSite"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectVendor.ascx" TagName="MultiSelectVendor"
    TagPrefix="cc1" %>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectCarrier.ascx" TagName="MultiSelectCarrier"
    TagPrefix="uc1" %>
<%@ Register Src="~/CommonUI/UserControls/ucVendorSelectionTemplate.ascx" TagName="VendorSelectTemplate"
    TagPrefix="cc1" %>
<%@ Register TagName="ucExportButton" TagPrefix="uc1" Src="~/CommonUI/UserControls/ucExportToExcel.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript" language="javascript">
        function ValidatePage() {
            if ($("[id$='rbDueBy']").is(":checked")) {
                if ($("[id$='txtUCDate']").val() == '') {
                    alert('<%=SelectDueByDate%>');
                    return false;
                }
            }
        }
        $(document).ready(function () {
            $("body").keypress(function (e) {
                if (e.which == 13 || e.keyCode == 13) {
                    $('#<%=btnGenerateReport.ClientID %>').click();
                    return false;
                }
            });
        });
    </script>
    <h2>
        <cc1:ucLabel ID="lblOpenPurchaseOrderVendorView" runat="server"></cc1:ucLabel>
        <cc1:ucLabel ID="lblSearchResult" runat="server" Visible="false"></cc1:ucLabel>
    </h2>
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div class="right-shadow">
        <div class="formbox">
            <div class="button-row">
                <cc1:ucMultiView ID="mvDiscrepancyList" runat="server" ActiveViewIndex="0">
                    <cc1:ucView ID="vwSearchCreteria" runat="server">
                        <cc1:ucPanel ID="UcCarrierVendorPanel" runat="server">
                            <table width="100%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                                <tr>
                                    <td align="right">
                                        <cc1:VendorSelectTemplate runat="server" ID="ucVendorTemplateSelect" />
                                    </td>
                                </tr>
                            </table>
                            <table width="100%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                                <tr>
                                    <td colspan="3">
                                        <table>
                                            <tr>
                                                <td style="font-weight: bold;">
                                                    <cc1:ucLabel ID="lblPurchaseOrderNo1" runat="server" Text="Purchase Order Number"></cc1:ucLabel>
                                                </td>
                                                <td style="font-weight: bold;">
                                                    <cc1:ucLabel ID="UcLabel3" runat="server">:</cc1:ucLabel>
                                                </td>
                                                <td>
                                                    <cc1:MultiSelectPO runat="server" ID="msPO" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="font-weight: bold;">
                                                    <cc1:ucLabel ID="lblSite1" runat="server" Text="Sites"></cc1:ucLabel>
                                                </td>
                                                <td style="font-weight: bold;">
                                                    <cc1:ucLabel ID="UcLabel4" runat="server">:</cc1:ucLabel>
                                                </td>
                                                <td>
                                                    <cc1:MultiSelectSite runat="server" ID="msSite" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="font-weight: bold; width: 10%">
                                                    <cc1:ucLabel ID="lblVendor" runat="server" Text="Vendor Name"></cc1:ucLabel>
                                                </td>
                                                <td style="font-weight: bold; width: 1%">
                                                    <cc1:ucLabel ID="lblVendorCollon" runat="server">:</cc1:ucLabel>
                                                </td>
                                                <td>
                                                    <cc1:MultiSelectVendor runat="server" ID="msVendor" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="font-weight: bold; width: 10%">
                                                    <cc1:ucLabel ID="lblDisplay" runat="server" Text="Display"></cc1:ucLabel>
                                                </td>
                                                <td style="font-weight: bold; width: 1%">
                                                    <cc1:ucLabel ID="lblDisplayCollon" runat="server">:</cc1:ucLabel>
                                                </td>
                                                <td>
                                                    <table width="100%">
                                                        <tr style="height: 3em;">
                                                            <td width="10%" style="font-weight: bold; text-align: left;">
                                                                <cc1:ucRadioButton ID="rbAll" GroupName="Display" runat="server" Text=" All" />
                                                            </td>
                                                            <td width="10%" style="font-weight: bold; text-align: left;">
                                                                <cc1:ucRadioButton GroupName="Display" ID="rbOOD" Width="120" runat="server" Text="Only Over Due" />
                                                            </td>
                                                            <td width="10%" style="font-weight: bold; text-align: left;">
                                                                <cc1:ucRadioButton GroupName="Display" ID="rbRaiseInLast48Hours" Width="120" runat="server"
                                                                    Text="Raise in Last 48hrs" />
                                                            </td>
                                                            <td width="73%" style="font-weight: bold;">
                                                                <cc1:ucRadioButton GroupName="Display" ID="rbDueBy" runat="server" Text=" Due By" />
                                                                <cc2:ucDate ClientIDMode="Static" ID="txtDate" runat="server" />
                                                                <asp:RequiredFieldValidator ID="rfvDateRequired" runat="server" Display="None" ControlToValidate="txtDate$txtUCDate"
                                                                    ValidateEmptyText="true" ValidationGroup="a" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </cc1:ucPanel>
                    </cc1:ucView>
                    <cc1:ucView ID="vwSearchListing" runat="server">
                        <div class="button-row">
                            <uc1:ucExportButton ID="btnExportToExcel" runat="server" />
                        </div>
                        <cc1:ucPanel ID="pnlGrid" runat="server" ScrollBars="Both" CssClass="fieldset-form">
                            <cc1:ucGridView ID="gvDisLog" Width="100%" runat="server" CssClass="grid" OnSorting="SortGrid"
                                PageSize="50" AllowPaging="true" PagerStyle-HorizontalAlign="left" AllowSorting="true"
                                Style="overflow: auto;" OnPageIndexChanging="gvDisLog_PageIndexChanging">
                                <EmptyDataTemplate>
                                    <div style="text-align: center">
                                        <cc1:ucLabel ID="lblRecordNotFound" isRequired="true" runat="server" Text="No record found"></cc1:ucLabel>
                                    </div>
                                </EmptyDataTemplate>
                                <Columns>
                                    <asp:BoundField ItemStyle-Wrap="false" HeaderText="VendorNo" DataField="Vendor_No"
                                        AccessibleHeaderText="false" SortExpression="Vendor_No">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField ItemStyle-Wrap="false" HeaderText="VendorName" DataField="Vendor_Name"
                                        AccessibleHeaderText="false" SortExpression="Vendor_Name">
                                        <HeaderStyle Width="100px" HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:BoundField>
                                    <asp:BoundField ItemStyle-Wrap="false" HeaderText="PurchaseOrder" DataField="Purchase_order"
                                        AccessibleHeaderText="false" SortExpression="Purchase_order">
                                        <HeaderStyle Width="100px" HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField ItemStyle-Wrap="false" HeaderText="Line" DataField="Line_No" AccessibleHeaderText="false"
                                        SortExpression="Line_No">
                                        <HeaderStyle Width="100px" HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="Order Date" DataFormatString="{0:dd-MM-yyyy}" ItemStyle-Wrap="false"
                                        DataField="Order_raisedString" SortExpression="Order_raisedString">
                                        <HeaderStyle HorizontalAlign="Center" Width="300px" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="Orignal Due Date" DataFormatString="{0:dd-MM-yyyy}" ItemStyle-Wrap="false"
                                        DataField="Original_due_dateString" SortExpression="Original_due_dateString">
                                        <HeaderStyle HorizontalAlign="Center" Width="300px" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="Warehouse" DataField="SiteName" SortExpression="SiteName"
                                        ItemStyle-Wrap="false">
                                        <HeaderStyle HorizontalAlign="Center" Width="400px" />
                                        <ItemStyle HorizontalAlign="Center" Wrap="false" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="Direct_code" DataField="Direct_code" SortExpression="Direct_code"
                                        ItemStyle-Wrap="false">
                                        <HeaderStyle HorizontalAlign="Center" Width="400px" />
                                        <ItemStyle HorizontalAlign="Center" Wrap="false" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="ODCode" DataField="OD_Code" SortExpression="OD_Code"
                                        ItemStyle-Wrap="false">
                                        <HeaderStyle HorizontalAlign="Center" Width="100px" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="VendorCode" DataField="Vendor_code" SortExpression="Vendor_code"
                                        ItemStyle-Wrap="false">
                                        <HeaderStyle HorizontalAlign="Center" Width="400px" />
                                        <ItemStyle HorizontalAlign="Center" Wrap="false" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="Description1" DataField="Product_description" SortExpression="Product_description"
                                        ItemStyle-Wrap="false">
                                        <HeaderStyle HorizontalAlign="Center" Width="100px" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="UOM" DataField="UOM" AccessibleHeaderText="false" ItemStyle-Wrap="false"
                                        SortExpression="UOM">
                                        <HeaderStyle Width="100px" HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="Original_quantity" DataField="Original_quantity" SortExpression="Original_quantity"
                                        ItemStyle-Wrap="false">
                                        <HeaderStyle HorizontalAlign="Center" Width="400px" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="Outstanding_Qty" DataField="Outstanding_Qty" SortExpression="Outstanding_Qty"
                                        ItemStyle-Wrap="false">
                                        <HeaderStyle HorizontalAlign="Center" Width="400px" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="UnitCost" DataField="PO_cost" ItemStyle-Wrap="false"
                                        SortExpression="PO_cost">
                                        <HeaderStyle HorizontalAlign="Center" Width="300px" />
                                        <ItemStyle HorizontalAlign="Right" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="TotalLineCost" DataField="PO_Totalcost" ItemStyle-Wrap="false"
                                        SortExpression="PO_Totalcost">
                                        <HeaderStyle HorizontalAlign="Center" Width="300px" />
                                        <ItemStyle HorizontalAlign="Right" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="Currency" DataField="Currency" ItemStyle-Wrap="false"
                                        SortExpression="Currency">
                                        <HeaderStyle HorizontalAlign="Center" Width="300px" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="Buyer_no" ItemStyle-Wrap="false" DataField="StockPlannerNo"
                                        AccessibleHeaderText="false" SortExpression="StockPlannerNo">
                                        <HeaderStyle Width="100px" HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="BuyerName" ItemStyle-Wrap="false" DataField="StockPlannerName"
                                        AccessibleHeaderText="false" SortExpression="StockPlannerName">
                                        <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:BoundField>
                                </Columns>
                            </cc1:ucGridView>
                            <cc1:ucGridView ID="tempGridView" Visible="false" Width="100%" runat="server" CssClass="grid"
                                OnSorting="SortGrid" PageSize="100" AllowSorting="true" Style="overflow: auto;"
                                OnPageIndexChanging="gvDisLog_PageIndexChanging">
                                <Columns>
                                    <asp:BoundField ItemStyle-Wrap="false" HeaderText="VendorNo" DataField="Vendor_No"
                                        AccessibleHeaderText="false" SortExpression="Vendor_No">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField ItemStyle-Wrap="false" HeaderText="VendorName" DataField="Vendor_Name"
                                        AccessibleHeaderText="false" SortExpression="Vendor_Name">
                                        <HeaderStyle Width="100px" HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:BoundField>
                                    <asp:BoundField ItemStyle-Wrap="false" HeaderText="PurchaseOrder" DataField="Purchase_order"
                                        AccessibleHeaderText="false" SortExpression="Purchase_order">
                                        <HeaderStyle Width="100px" HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:BoundField>
                                    <asp:BoundField ItemStyle-Wrap="false" HeaderText="Line" DataField="Line_No" AccessibleHeaderText="false"
                                        SortExpression="Line_No">
                                        <HeaderStyle Width="100px" HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="Order Date" DataFormatString="{0:dd-MM-yyyy}" ItemStyle-Wrap="false"
                                        DataField="Order_raisedString" SortExpression="Order_raisedString">
                                        <HeaderStyle HorizontalAlign="Center" Width="300px" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="Orignal Due Date" DataFormatString="{0:dd-MM-yyyy}" ItemStyle-Wrap="false"
                                        DataField="Original_due_dateString" SortExpression="Original_due_dateString">
                                        <HeaderStyle HorizontalAlign="Center" Width="300px" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="Warehouse" DataField="SiteName" SortExpression="SiteName"
                                        ItemStyle-Wrap="false">
                                        <HeaderStyle HorizontalAlign="Center" Width="400px" />
                                        <ItemStyle HorizontalAlign="Center" Wrap="false" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="Direct_code" DataField="Direct_code" SortExpression="Direct_code"
                                        ItemStyle-Wrap="false">
                                        <HeaderStyle HorizontalAlign="Center" Width="400px" />
                                        <ItemStyle HorizontalAlign="Center" Wrap="false" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="ODCode" DataField="OD_Code" SortExpression="OD_Code"
                                        ItemStyle-Wrap="false">
                                        <HeaderStyle HorizontalAlign="Center" Width="100px" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="VendorCode" DataField="Vendor_code" SortExpression="Vendor_code"
                                        ItemStyle-Wrap="false">
                                        <HeaderStyle HorizontalAlign="Center" Width="400px" />
                                        <ItemStyle HorizontalAlign="Center" Wrap="false" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="Description1" DataField="Product_description" SortExpression="Product_description"
                                        ItemStyle-Wrap="false">
                                        <HeaderStyle HorizontalAlign="Center" Width="100px" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="UOM" DataField="UOM" AccessibleHeaderText="false" ItemStyle-Wrap="false"
                                        SortExpression="UOM">
                                        <HeaderStyle Width="100px" HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="Original_quantity" DataField="Original_quantity" SortExpression="Original_quantity"
                                        ItemStyle-Wrap="false">
                                        <HeaderStyle HorizontalAlign="Center" Width="400px" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="Outstanding_Qty" DataField="Outstanding_Qty" SortExpression="Outstanding_Qty"
                                        ItemStyle-Wrap="false">
                                        <HeaderStyle HorizontalAlign="Center" Width="400px" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="UnitCost" DataField="PO_cost" ItemStyle-Wrap="false"
                                        SortExpression="PO_cost">
                                        <HeaderStyle HorizontalAlign="Center" Width="300px" />
                                        <ItemStyle HorizontalAlign="Right" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="TotalLineCost" DataField="PO_Totalcost" ItemStyle-Wrap="false"
                                        SortExpression="PO_Totalcost">
                                        <HeaderStyle HorizontalAlign="Center" Width="300px" />
                                        <ItemStyle HorizontalAlign="Right" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="Currency" DataField="Currency" ItemStyle-Wrap="false"
                                        SortExpression="Currency">
                                        <HeaderStyle HorizontalAlign="Center" Width="300px" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="Buyer_no" ItemStyle-Wrap="false" DataField="StockPlannerNo"
                                        AccessibleHeaderText="false" SortExpression="StockPlannerNo">
                                        <HeaderStyle Width="100px" HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="BuyerName" ItemStyle-Wrap="false" DataField="StockPlannerName"
                                        AccessibleHeaderText="false" SortExpression="StockPlannerName">
                                        <HeaderStyle Width="100px" HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:BoundField>
                                </Columns>
                            </cc1:ucGridView>
                        </cc1:ucPanel>
                    </cc1:ucView>
                </cc1:ucMultiView>
            </div>
        </div>
        <div class="button-row">
            <cc1:ucButton ID="btnGenerateReport" runat="server" Text="Search" OnClientClick="return ValidatePage();"
                CssClass="button" OnClick="btnGenerateReport_Click" />
            <cc1:ucButton ID="btnBackSearch" runat="server" Text="Back To Search Criteria" CssClass="button"
                OnClick="btnBack_Click" />
        </div>
    </div>
</asp:Content>

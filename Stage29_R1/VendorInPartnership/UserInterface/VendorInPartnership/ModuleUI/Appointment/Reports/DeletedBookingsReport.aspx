﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master" AutoEventWireup="true" 
CodeFile="DeletedBookingsReport.aspx.cs" Inherits="DeletedBookingsReport" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectCountry.ascx" TagName="MultiSelectCountry"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectSite.ascx" TagName="MultiSelectSite"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectVendor.ascx" TagName="MultiSelectVendor"
    TagPrefix="cc1" %>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectCarrier.ascx" TagName="MultiSelectCarrier"
    TagPrefix="uc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectUser.ascx" TagName="MultiSelectUser"
    TagPrefix="uc1" %>
<%@ Register Src="~/CommonUI/UserControls/ucVendorSelectionTemplate.ascx" TagName="VendorSelectTemplate"
    TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript" language="javascript">
        $(document).ready(function () {
            $("body").keypress(function (e) {
                if (e.which == 13 || e.keyCode == 13) {
                    $('#<%=UcButton1.ClientID %>').click();
                    return false;
                }
            });
        });
        function setValue1(target) {
            document.getElementById('<%=hdnJSFromDt.ClientID %>').value = target.value;
        }

        function setValue2(target) {
            document.getElementById('<%=hdnJSToDt.ClientID %>').value = target.value;
        }
    </script>
    <asp:ScriptManager ID="SM1" runat="server">
    </asp:ScriptManager>
    <asp:HiddenField ID="hdnJSFromDt" runat="server" />
    <asp:HiddenField ID="hdnJSToDt" runat="server" />
    <asp:HiddenField ID="hdnSiteIDs" runat="server" />
    <asp:HiddenField ID="hdnCountryIDs" runat="server" />
    <asp:HiddenField ID="hdnVendorIDs" runat="server" />
    <asp:HiddenField ID="hdnFromDt" runat="server" />
    <asp:HiddenField ID="hdnToDt" runat="server" />
    <asp:HiddenField ID="hdnCarrierIDs" runat="server" />
    <asp:HiddenField ID="hdnUserIDs" runat="server" />
    <h2>
        <cc1:ucLabel ID="lblDeletedBookings" runat="server"></cc1:ucLabel>
        
    </h2>
    <div class="right-shadow">
        <div class="formbox">
            <cc1:ucPanel ID="UcVehiclePanel" runat="server">
             <table width="100%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                    <tr>
                        <td align="right">
                            <cc1:VendorSelectTemplate runat="server" ID="ucVendorTemplateSelect" />
                        </td>
                    </tr>
                </table>
                <table width="100%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                    <tr>
                        <td>
                            <table>
                                <tr>
                                    <td style="font-weight: bold; width: 10%;">
                                        <cc1:ucLabel ID="lblCountry" runat="server" Text="Country"></cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold; width: 1%;">
                                        <cc1:ucLabel ID="UcLabel5" runat="server">:</cc1:ucLabel>
                                    </td>
                                    <td>
                                        <cc1:MultiSelectCountry ID="msCountry" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="font-weight: bold;">
                                        <cc1:ucLabel ID="lblSite" runat="server" Text="Site"></cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold;">
                                        <cc1:ucLabel ID="UcLabel4" runat="server">:</cc1:ucLabel>
                                    </td>
                                    <td>
                                        <cc1:MultiSelectSite ID="msSite" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="font-weight: bold; width: 10%">
                                        <cc1:ucLabel ID="lblVendor" runat="server" Text="Vendor"></cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold; width: 1%">
                                        <cc1:ucLabel ID="lblVendorCollon" runat="server">:</cc1:ucLabel>
                                    </td>
                                    <td>
                                        <cc1:MultiSelectVendor ID="msVendor" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="font-weight: bold; width: 10%">
                                        <cc1:ucLabel ID="lblCarrier" runat="server" Text="Carrier"></cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold; width: 1%">
                                        <cc1:ucLabel ID="lblCarrierCollon" runat="server">:</cc1:ucLabel>
                                    </td>
                                    <td>
                                        <uc1:MultiSelectCarrier ID="msCarrier" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="font-weight: bold; width: 10%">
                                        <cc1:ucLabel ID="lblUser" runat="server"></cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold; width: 1%">
                                        <cc1:ucLabel ID="lblUserCollon" runat="server">:</cc1:ucLabel>
                                    </td>
                                    <td>
                                        <uc1:MultiSelectUser ID="msUser" runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table width="100%">
                                <tr>
                                    <td style="font-weight: bold; width:81px">
                                        <cc1:ucLabel ID="lblDateFrom" runat="server" Text="Date From"></cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold; width: 1%">
                                        <cc1:ucLabel ID="UcLabel6" runat="server">:</cc1:ucLabel>
                                    </td>
                                    <td align="left">
                                        <table width="25%" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style="font-weight: bold; width: 6em;">
                                                    <cc1:ucTextbox ID="txtFromDate" runat="server" ClientIDMode="Static" CssClass="date"
                                                        onchange="setValue1(this)" ReadOnly="True" Width="70px" />
                                                </td>
                                                <td style="font-weight: bold; width: 4em; text-align: center">
                                                    <cc1:ucLabel ID="lblTo" runat="server" Text="To"></cc1:ucLabel>
                                                </td>
                                                <td style="font-weight: bold; width: 6em;">
                                                    <cc1:ucTextbox ID="txtToDate" runat="server" ClientIDMode="Static" CssClass="date"
                                                        onchange="setValue2(this)" ReadOnly="True" Width="70px" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <div class="button-row">
                                <cc1:ucButton ID="UcButton1" runat="server" Text="Generate Report" CssClass="button"
                                    OnClick="btnGenerateReport_Click" />
                            </div>
                        </td>
                    </tr>
                </table>
            </cc1:ucPanel>
            <cc1:ucPanel ID="UcVehicleReportViewPanel" runat="server" Visible="false">
                <table style="width: 100%;">
                    <tr>
                        <td>
                            <rsweb:ReportViewer ID="VehicleReportViewer" runat="server" Width="950px" DocumentMapCollapsed="True"
                                Font-Names="Verdana" Font-Size="8pt" InteractiveDeviceInfos="(Collection)" WaitMessageFont-Names="Verdana"
                                WaitMessageFont-Size="14pt">
                                <LocalReport ReportPath="ModuleUI\Appointment\Reports\RDLC\DeletedBookingsReport.rdlc">
                                    <DataSources>
                                        <rsweb:ReportDataSource DataSourceId="DeletedBookingsSource" Name="ODDeletedBookings" />
                                    </DataSources>
                                </LocalReport>
                            </rsweb:ReportViewer>
                            <asp:SqlDataSource ID="DeletedBookingsSource" runat="server" ConnectionString="<%$ ConnectionStrings:sConn %>"
                                SelectCommand="spRPTBOK_DeletedBookings" SelectCommandType="StoredProcedure">
                                <SelectParameters>
                                    <asp:ControlParameter ControlID="hdnCountryIDs" DefaultValue="-1" Name="SelectedCountryIDs"
                                        PropertyName="Value" Type="String" />
                                    <asp:ControlParameter ControlID="hdnSiteIDs" DefaultValue="-1" Name="SelectedSiteIDs"
                                        PropertyName="Value" Type="String" />
                                    <asp:ControlParameter ControlID="hdnVendorIDs" DefaultValue="-1" Name="SelectedVendorIDs"
                                        PropertyName="Value" Type="String" />
                                    <asp:ControlParameter ControlID="hdnCarrierIDs" DefaultValue="-1" Name="SelectedCarrierIDs"
                                        PropertyName="Value" Type="String" />
                                    <asp:ControlParameter ControlID="hdnUserIDs" DefaultValue="-1" Name="SelectedUserIDs"
                                        PropertyName="Value" Type="String" />
                                    <asp:ControlParameter ControlID="hdnToDt" DefaultValue="" Name="DateTo" PropertyName="Value"
                                        Type="DateTime" />
                                    <asp:ControlParameter ControlID="hdnFromDt" Name="DateFrom" PropertyName="Value"
                                        Type="DateTime" />
                                </SelectParameters>
                            </asp:SqlDataSource>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <cc1:ucButton ID="BackSearch" runat="server" CssClass="button"
                                OnClick="BackSearch_Click" />
                        </td>
                    </tr>
                </table>
            </cc1:ucPanel>
        </div>
    </div>
</asp:Content>





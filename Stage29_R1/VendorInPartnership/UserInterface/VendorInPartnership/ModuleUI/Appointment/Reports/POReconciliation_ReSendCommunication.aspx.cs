﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Text;
using System.Web.UI;
using BusinessEntities.ModuleBE.Discrepancy.LogDiscrepancy;
using BusinessEntities.ModuleBE.Languages.Languages;
using BusinessLogicLayer.ModuleBAL.Appointment.Booking;
using BusinessLogicLayer.ModuleBAL.Discrepancy.LogDiscrepancy;
using Utilities;
using BusinessLogicLayer.ModuleBAL.StockOverview;
using BusinessEntities.ModuleBE.StockOverview;
using System.Web.UI.WebControls;
using System.Linq;
using WebUtilities;
using BusinessEntities.ModuleBE.Appointment.Booking;

public partial class POReconciliation_ReSendCommunication : CommonPage
{

    #region Declarations ...

    string sFromAddress = Convert.ToString(ConfigurationManager.AppSettings["fromAddress"]);
    sendCommunicationCommon oSendCommunicationCommon = new sendCommunicationCommon();
    StringBuilder sSendMailLink = new StringBuilder();

    #endregion

    #region Events ...

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            this.BindLanguages();
            btnReSendCommunication.Visible = false;
            lblEmail.Visible = true;
            txtEmail.Visible = false;

            if (GetQueryStringValue("POChaseCommunicationId") != null )
            {
                var POChaseCommunicationId = Convert.ToInt32(GetQueryStringValue("POChaseCommunicationId"));


                var APPBOK_CommunicationBE = new APPBOK_CommunicationBE();
                APPBOK_CommunicationBE.Action = "GetChaseEmailData";
                APPBOK_CommunicationBE.POChaseCommunicationId = POChaseCommunicationId;
               
                var APPBOK_CommunicationBAL = new APPBOK_CommunicationBAL();
                var lstEmailEscalationData = APPBOK_CommunicationBAL.GetChaseEmailDataBAL(APPBOK_CommunicationBE);
                if (lstEmailEscalationData != null && lstEmailEscalationData.Count > 0)
                {
                    this.BindEmailIds(lstEmailEscalationData);
                    ViewState["lstEmailEscalationData"] = lstEmailEscalationData;

                    var filteredData = lstEmailEscalationData.Find(x => x.LanguageID == Convert.ToInt32(drpLanguageId.SelectedValue)
                        && x.SentTo == ddlSentEmailIds.SelectedItem.Text);

                    divReSendCommunication.InnerHtml = filteredData.Body.Replace("{logoInnerPath}", oSendCommunicationCommon.getAbsolutePath());
                    hdSubject.Value = filteredData.Subject;
                    btnReSendCommunication.Visible = true;
                    lblEmail.Visible = true;
                }
            }
            else
            {
                this.ShowErrorMessage();
            }

         
        }
    }

    protected void btnReSendCommunication_Click(object sender, EventArgs e)
    {
        // if (ViewState["lstEmailEscalationData"] != null)
        //{
        var lstEmailEscalationData = (List<APPBOK_CommunicationBE>)ViewState["lstEmailEscalationData"];
        var filteredData = lstEmailEscalationData.Find(x => x.LanguageID == Convert.ToInt32(drpLanguageId.SelectedValue)
            && x.SentTo == ddlSentEmailIds.SelectedItem.Text);

        var sentToWithLink = new System.Text.StringBuilder();

        sentToWithLink.Append("<a target='_blank' href='" + Common.EncryptQuery("POReconciliation_ReSendCommunication.aspx?POChaseCommunicationId=" + Convert.ToInt32(GetQueryStringValue("POChaseCommunicationId"))) + "' onclick='window.open(this.href, this.target,\"scrollbars=1,left=100px,top=100px,width=1000px,height=800px\"); return false;'>" + ddlSentEmailIds.SelectedItem.Text + "</a>" + System.Environment.NewLine);

        var oAPPBOK_CommunicationBE = new APPBOK_CommunicationBE();
        oAPPBOK_CommunicationBE.Action = "AddPOChaseCommunication";
        oAPPBOK_CommunicationBE.Subject = filteredData.Subject;
        oAPPBOK_CommunicationBE.SentTo = ddlSentEmailIds.SelectedItem.Text;
        oAPPBOK_CommunicationBE.POChaseSentDate = DateTime.Now;
        oAPPBOK_CommunicationBE.Body = filteredData.Body;
        oAPPBOK_CommunicationBE.SendByID = Convert.ToInt32(Session["UserID"]);
        oAPPBOK_CommunicationBE.LanguageID  = filteredData.LanguageID;
        oAPPBOK_CommunicationBE.MailSentInLanguage = true;
        oAPPBOK_CommunicationBE.CommunicationStatus = "ResentFromWebApp";
        oAPPBOK_CommunicationBE.SentToWithLink = sentToWithLink.ToString();
        oAPPBOK_CommunicationBE.VendorEmailIds = filteredData.VendorEmailIds;
        oAPPBOK_CommunicationBE.POChaseCommunicationId = Convert.ToInt32(GetQueryStringValue("POChaseCommunicationId"));

        APPBOK_CommunicationBAL oAPPBOK_CommunicationBAL1 = new APPBOK_CommunicationBAL();
        var CommId = oAPPBOK_CommunicationBAL1.AddBookingCommunicationBAL(oAPPBOK_CommunicationBE);

        var emailToAddress = ddlSentEmailIds.SelectedItem.Text;
        var emailFromAddress = Convert.ToString(ConfigurationManager.AppSettings["fromAddress"]);
        var emailToSubject = filteredData.Subject;
        var emailBody = filteredData.Body;
        Utilities.clsEmail oclsEmail = new Utilities.clsEmail();
        oclsEmail.sendMail(emailToAddress, emailBody, emailToSubject, emailFromAddress, true);

        Response.Write("<script language='javascript'>window.open('', '_self', '');window.close();</script>");
        //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "MailToReSend", "window.opener.location.reload();window.close();", true);       
    }

    protected void drpLanguageId_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ViewState["lstEmailEscalationData"] != null)
        {
            var lstEmailEscalationData = (List<APPBOK_CommunicationBE>)ViewState["lstEmailEscalationData"];
            var filteredData = lstEmailEscalationData.Find(x => x.LanguageID == Convert.ToInt32(drpLanguageId.SelectedValue)
                && x.SentTo == ddlSentEmailIds.SelectedItem.Text);

            divReSendCommunication.InnerHtml = filteredData.Body.Replace("{logoInnerPath}", oSendCommunicationCommon.getAbsolutePath());
            hdSubject.Value = filteredData.Subject;
            btnReSendCommunication.Visible = true;
            lblEmail.Visible = true;
        }
        else
        {
            this.ShowErrorMessage();
        }

      
    }

    protected void ddlSentEmailIds_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ViewState["lstEmailEscalationData"] != null)
        {
            var lstEmailEscalationData = (List<APPBOK_CommunicationBE>)ViewState["lstEmailEscalationData"];

            //Getting UserLanguage and bind language dropdown 
            var UserLanguage = (from x in lstEmailEscalationData
                                where x.MailSentInLanguage && x.CommunicationStatus == "Initial" &&  x.SentTo == ddlSentEmailIds.SelectedItem.Text
                                select new
                                {
                                    LanguageId = x.LanguageID
                                }).ToList();

            drpLanguageId.SelectedIndex = drpLanguageId.Items.IndexOf(drpLanguageId.Items.FindByValue(UserLanguage[0].LanguageId.ToString()));


            var filteredData = lstEmailEscalationData.Find(x => x.LanguageID == Convert.ToInt32(drpLanguageId.SelectedValue)
                && x.SentTo == ddlSentEmailIds.SelectedItem.Text );

            divReSendCommunication.InnerHtml = filteredData.Body.Replace("{logoInnerPath}", oSendCommunicationCommon.getAbsolutePath());
            hdSubject.Value = filteredData.Subject;
            btnReSendCommunication.Visible = true;
            lblEmail.Visible = true;
        }
        else
        {
            this.ShowErrorMessage();
        }
    }

    #endregion

    #region Methods ...

    private void BindEmailIds(List<APPBOK_CommunicationBE> lstAPPBOK_CommunicationBE)
    {
        var filteredComm = (from x in lstAPPBOK_CommunicationBE
                            where x.MailSentInLanguage && x.CommunicationStatus == "Initial"
                            select new
                            {
                                POChaseId = x.POChaseId,
                                SentTo = x.SentTo,
                                LanguageId = x.LanguageID
                            }).ToList();

        if (filteredComm.Count > 0 && filteredComm != null)
        {
            ddlSentEmailIds.DataSource = filteredComm;
            ddlSentEmailIds.DataValueField = "POChaseId";
            ddlSentEmailIds.DataTextField = "SentTo";
            ddlSentEmailIds.DataBind();

            ddlSentEmailIds.SelectedIndex = 0;
            var languageId = from x in filteredComm
                             where x.POChaseId == Convert.ToInt32(ddlSentEmailIds.SelectedValue)
                             select x.LanguageId;

            drpLanguageId.SelectedIndex = drpLanguageId.Items.IndexOf(drpLanguageId.Items.FindByValue(languageId.ToString()));
        }
    }

    public string distictEmails(string Emails)
    {
        string[] arrEMails = Emails.Split(new char[] { ',' });

        List<string> lstEmails = new List<string>();
        foreach (string email in arrEMails)
        {
            if (!string.IsNullOrEmpty(email) && !string.IsNullOrWhiteSpace(email) && !lstEmails.Contains(email.Trim()))
                lstEmails.Add(email.Trim());
        }
        return string.Join(",", lstEmails.ToArray());
    }

    private void ShowErrorMessage()
    {
        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "showNoMailToReSend", "alert('" + WebUtilities.WebCommon.getGlobalResourceValue("NoMailToResend") + "');", true);
    }

    private void BindLanguages()
    {
        APPBOK_CommunicationBAL oAPPBOK_CommunicationBAL = new APPBOK_CommunicationBAL();
        List<MAS_LanguageBE> oLanguages = oAPPBOK_CommunicationBAL.GetLanguages();
        drpLanguageId.DataSource = oLanguages;
        drpLanguageId.DataValueField = "LanguageID";
        drpLanguageId.DataTextField = "Language";
        drpLanguageId.DataBind();
    }

    #endregion
}
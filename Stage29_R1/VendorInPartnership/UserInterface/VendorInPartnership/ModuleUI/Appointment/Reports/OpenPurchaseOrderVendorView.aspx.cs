﻿using BusinessEntities.ModuleBE.Security;
using BusinessEntities.ModuleBE.Upload;
using BusinessLogicLayer.ModuleBAL.Security;
using BusinessLogicLayer.ModuleBAL.Upload;
using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebUtilities;

public partial class ModuleUI_Appointment_Reports_OpenPurchaseOrderVendorView : CommonPage
{
    #region Declarations ...
    protected string SelectDueByDate = WebCommon.getGlobalResourceValue("SelectDueByDate");
    protected string Pleaseselectatleastonesite = WebCommon.getGlobalResourceValue("Pleaseselectatleastonesite");
    protected string Maximumfoursitesareallowed = WebCommon.getGlobalResourceValue("Maximumfoursitesareallowed");
    #endregion
    protected void Page_InIt(object sender, EventArgs e)
    {
        ucVendorTemplateSelect.CurrentPage = this;
        msSite.isMoveAllRequired = true;

        ucVendorTemplateSelect.CurrentPage = this;
        if (Session["Role"].ToString().ToLower() == "vendor")
        {
            msVendor.FunctionCalled = "BindVendorByVendorId";
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    { 
        btnExportToExcel.GridViewControl = tempGridView;
        btnExportToExcel.Page = this;
        btnExportToExcel.FileName = "OpenPOVendorReport";
        TextBox txtPO = msPO.Controls[0] as TextBox;

        if (!IsPostBack)
        {
            btnBackSearch.Visible = false;
            mvDiscrepancyList.ActiveViewIndex = 0;

            if (txtPO != null)
                txtPO.Width = 313;

            rbAll.Checked = true;
        }

        TextBox txtPurchaseOrder = Utilities.Common.FindControlRecursive(this.Page.Controls[0], "txtPurchaseOrder") as TextBox;
        if (txtPurchaseOrder != null)
        {
            txtPurchaseOrder.Focus();
        }

        msSite.setSitesOnPostBack();
        msVendor.setVendorsOnPostBack();
    }
    public override void TemplateSelectedIndexChanged()
    {
        base.TemplateSelectedIndexChanged();

        int VendorTemplateId = Convert.ToInt32(ucVendorTemplateSelect.innerControlddlTemplate.SelectedValue);

        ListBox lstSelectedVendor = (ListBox)msVendor.FindControl("lstRight");

        ListBox lstVendor = (ListBox)msVendor.FindControl("ucVendor").FindControl("lstLeft");

        if (VendorTemplateId > 0)
        {
            SCT_TemplateBAL oSCT_TemplateBAL = new SCT_TemplateBAL();
            SCT_TemplateBE oSCT_TemplateBE = new SCT_TemplateBE
            {
                VendorTemplateID = VendorTemplateId,
                Action = "GetVendorTemplateInfobyID"
            };

            List<SCT_TemplateBE> lstVendorTemplate = oSCT_TemplateBAL.GetVendorTemplateById(oSCT_TemplateBE);

            if (lstVendorTemplate != null && lstVendorTemplate.Count > 0)
            {
                lstSelectedVendor.Items.Clear();
                msVendor.innerControlHiddenField.Value = string.Empty;
                msVendor.innerControlHiddenField2.Value = string.Empty;
                for (int iCount = 0; iCount < lstVendorTemplate.Count; iCount++)
                {
                    lstSelectedVendor.Items.Add(new ListItem(lstVendorTemplate[iCount].Vendor.Vendor_Name, lstVendorTemplate[iCount].Vendor.VendorID.ToString()));
                    msVendor.SelectedVendorIDs = lstVendorTemplate[iCount].Vendor.VendorID.ToString();
                    msVendor.SelectedVendorName = lstVendorTemplate[iCount].Vendor.Vendor_Name.ToString();
                }
            }
        }
        else
        {
            lstSelectedVendor.Items.Clear();
            msVendor.innerControlHiddenField.Value = string.Empty;
            msVendor.innerControlHiddenField2.Value = string.Empty;
        }
    }

    protected void btnGenerateReport_Click(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(msSite.SelectedSiteIDs) || string.IsNullOrWhiteSpace(msSite.SelectedSiteIDs))
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + Pleaseselectatleastonesite + "')", true);
            return;
        }

        string[] SelectedSite = msSite.SelectedSiteIDs.Split(',');

        if (!rbRaiseInLast48Hours.Checked && SelectedSite.Length > 4)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + Maximumfoursitesareallowed + "')", true);
            return;
        }

        lblOpenPurchaseOrderVendorView.Visible = false;
        lblSearchResult.Visible = true;
        UP_PurchaseOrderDetailBAL objPOBal = new UP_PurchaseOrderDetailBAL();
        Up_PurchaseOrderDetailBE oNewPOBE = new Up_PurchaseOrderDetailBE();

        if (!string.IsNullOrEmpty(msSite.SelectedSiteIDs))
        {
            oNewPOBE.SelectedSiteIDs = msSite.SelectedSiteIDs;
        }

        if (!string.IsNullOrEmpty(msVendor.SelectedVendorIDs))
        {
            oNewPOBE.SelectedVendorIDs = msVendor.SelectedVendorIDs;
        }

        if (!string.IsNullOrEmpty(msPO.SelectedPO))
        {
            oNewPOBE.PurchaseOrderIds = msPO.SelectedPO;
        }

        if (rbAll.Checked)
        {
            oNewPOBE.Action = "ALL";
            oNewPOBE.SelectedDate = null;
        }
        else if (rbOOD.Checked)
        {
            oNewPOBE.Action = "OOD";
            oNewPOBE.SelectedDate = DateTime.Now.ToString("dd/MM/yyyy");
        }
        else if (rbRaiseInLast48Hours.Checked)
        {
            oNewPOBE.Action = "RIL48H";
            oNewPOBE.SelectedDate = null;
        }
        else if (rbDueBy.Checked)
        {
            oNewPOBE.Action = "DUEBY";
            oNewPOBE.SelectedDate = txtDate.GetDate.ToString("dd/MM/yyyy");
        }

        oNewPOBE.UserID = Convert.ToInt32(Convert.ToSingle(Session["UserID"]));
        oNewPOBE.UserRoleID = Convert.ToInt32(Convert.ToSingle(Session["UserRoleId"]));

        gvDisLog.HeaderStyle.Wrap = false;
        gvDisLog.HeaderStyle.HorizontalAlign = HorizontalAlign.Justify;

        List<Up_PurchaseOrderDetailBE> lsOpenPO = objPOBal.GetOpenPOVendorDetailsBAL(oNewPOBE);

        gvDisLog.DataSource = lsOpenPO;
        gvDisLog.DataBind();

        if (gvDisLog.Rows.Count > 0)
        {
            btnExportToExcel.Visible = true;
            gvDisLog.PageIndex = 0;
            gvDisLog.DataBind();
        }
        else
        {
            btnExportToExcel.Visible = false;
        }

        //for export to excel
        tempGridView.DataSource = lsOpenPO;
        tempGridView.DataBind();

        Cache["lstOpenPO"] = lsOpenPO;
        mvDiscrepancyList.ActiveViewIndex = 1;
        btnBackSearch.Visible = true;
        btnGenerateReport.Visible = false;
        rbAll.Checked = true;
        txtDate.innerControltxtDate.Value = string.Empty;
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        mvDiscrepancyList.ActiveViewIndex = 0;
        btnBackSearch.Visible = false;
        btnGenerateReport.Visible = true;

        lblOpenPurchaseOrderVendorView.Visible = true;
        lblSearchResult.Visible = false;
    }

    protected void gvDisLog_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        if (Cache["lstOpenPO"] != null)
        {
            List<Up_PurchaseOrderDetailBE> lsOpenPO = Cache["lstOpenPO"] as List<Up_PurchaseOrderDetailBE>;
            gvDisLog.PageIndex = e.NewPageIndex;
            gvDisLog.DataSource = lsOpenPO;
            gvDisLog.DataBind();
        }
    }

    public override BusinessEntities.BaseBe[] SubSort(GridViewSortEventArgs e)
    {
        return Utilities.GenericListHelper<Up_PurchaseOrderDetailBE>.SortList((List<Up_PurchaseOrderDetailBE>)Cache["lstOpenPO"], e.SortExpression, e.SortDirection).ToArray();
    }
}
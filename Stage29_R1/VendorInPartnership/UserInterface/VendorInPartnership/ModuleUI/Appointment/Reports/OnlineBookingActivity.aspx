﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    AutoEventWireup="true" CodeFile="OnlineBookingActivity.aspx.cs" Inherits="ModuleUI_Appointment_Reports_OnlineBookingActivity" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectCountry.ascx" TagName="MultiSelectCountry"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectSite.ascx" TagName="MultiSelectSite"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectAP.ascx" TagName="MultiSelectAP"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectVendor.ascx" TagName="MultiSelectVendor"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectStockPlanner.ascx" TagName="MultiSelectStockPlanner"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/ucVendorSelectionTemplate.ascx" TagName="VendorSelectTemplate"
    TagPrefix="cc1" %>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectCarrier.ascx" TagName="MultiSelectCarrier"
    TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript" language="javascript">
        $(document).ready(function () {
            $("body").keypress(function (e) {
                if (e.which == 13 || e.keyCode == 13) {
                    $('#<%=UcButton1.ClientID %>').click();
                    return false;
                }
            });
        });
        function setValue1(target) {
            document.getElementById('<%=hdnJSFromDt.ClientID %>').value = target.value;
        }

        function setValue2(target) {
            document.getElementById('<%=hdnJSToDt.ClientID %>').value = target.value;
        }

    </script>
    <asp:ScriptManager ID="SM1" runat="server">
    </asp:ScriptManager>
    <asp:HiddenField ID="hdnJSFromDt" runat="server" />
    <asp:HiddenField ID="hdnJSToDt" runat="server" />
    <asp:HiddenField ID="hdnSiteIDs" runat="server" />
    <asp:HiddenField ID="hdnCountryIDs" runat="server" />
    <asp:HiddenField ID="hdnVendorIDs" runat="server" />
    <asp:HiddenField ID="hdnInventoryMangerIDs" runat="server" />
    <asp:HiddenField ID="hdnReportType" runat="server" />
    <asp:HiddenField ID="hdnFromDt" runat="server" />
    <asp:HiddenField ID="hdnToDt" runat="server" />
    <asp:HiddenField ID="hdnViewType" runat="server" />
    <asp:HiddenField ID="hdnSearchBy" runat="server" />
    <h2>
        <cc1:ucLabel ID="lblOnlineBookingActivity" runat="server" Text="Online Booking Activity"></cc1:ucLabel>
    </h2>
    <div class="right-shadow">
        <div class="formbox">
            <cc1:ucPanel ID="UcCarrierVendorPanel" runat="server">
                <table width="100%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                    <tr>
                        <td align="right">
                            <cc1:VendorSelectTemplate runat="server" ID="ucVendorTemplateSelect" />
                        </td>
                    </tr>
                </table>
                <table width="100%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                    <tr>
                        <td>
                            <table>
                                <tr>
                                    <td style="font-weight: bold; width: 10%">
                                        <cc1:ucLabel ID="lblVendor" runat="server" Text="Vendor"></cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold; width: 1%">
                                        <cc1:ucLabel ID="lblVendorCollon" runat="server">:</cc1:ucLabel>
                                    </td>
                                    <td>
                                        <cc1:MultiSelectVendor runat="server" ID="msVendor" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="font-weight: bold; width: 10%">
                                        <cc1:ucLabel ID="lblInventoryManager" runat="server" Text="Inventory Manager"></cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold; width: 1%">
                                        <cc1:ucLabel ID="lblCarrierCollon" runat="server">:</cc1:ucLabel>
                                    </td>
                                    <td>
                                        <cc1:MultiSelectStockPlanner ID="msStockPlanner" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="font-weight: bold; width: 10%">
                                        <cc1:ucLabel ID="lblCountry" runat="server" Text="Country"></cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold; width: 1%">
                                        <cc1:ucLabel ID="UcLabel5" runat="server">:</cc1:ucLabel>
                                    </td>
                                    <td>
                                        <cc1:MultiSelectCountry runat="server" ID="msCountry" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="font-weight: bold; width: 10%">
                                        <cc1:ucLabel ID="lblSite" runat="server" Text="Site"></cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold; width: 1%">
                                        <cc1:ucLabel ID="UcLabel4" runat="server">:</cc1:ucLabel>
                                    </td>
                                    <td>
                                        <cc1:MultiSelectSite runat="server" ID="msSite" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table width="100%">
                                <tr>
                                    <td style="font-weight: bold; width: 81px">
                                        <cc1:ucLabel ID="lblDateFrom" runat="server" Text="Date From"></cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold; width: 1%">
                                        <cc1:ucLabel ID="UcLabel6" runat="server">:</cc1:ucLabel>
                                    </td>
                                    <td align="left">
                                        <table width="25%" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style="font-weight: bold; width: 6em;">
                                                    <cc1:ucTextbox ID="txtFromDate" runat="server" ClientIDMode="Static" CssClass="date"
                                                        onchange="setValue1(this)" ReadOnly="True" Width="70px" />
                                                </td>
                                                <td style="font-weight: bold; width: 4em; text-align: center">
                                                    <cc1:ucLabel ID="lblTo" runat="server" Text="To"></cc1:ucLabel>
                                                </td>
                                                <td style="font-weight: bold; width: 6em;">
                                                    <cc1:ucTextbox ID="txtToDate" runat="server" ClientIDMode="Static" CssClass="date"
                                                        onchange="setValue2(this)" ReadOnly="True" Width="70px" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="font-weight: bold; width: 10%">
                                        <cc1:ucLabel ID="lblSearchBy" runat="server" Text="Search By"></cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold; width: 1%">
                                        :
                                    </td>
                                    <td align="center" class="radiobuttonlist">
                                        <table width="33%" cellspacing="3" cellpadding="0" align="left" class="top-settingsnoborder"
                                            style="margin-top: 10px;">
                                            <tr>
                                                <td style="width: 34%;" class="nobold">
                                                    <cc1:ucRadioButton ID="rdoBookingDatesearch" runat="server" GroupName="Date" Text="Booking Date"
                                                        Checked="true" />
                                                </td>
                                                <td style="width: 33%;" class="nobold">
                                                    <cc1:ucRadioButton ID="rdoDeliveryDate" runat="server" GroupName="Date" Text="Delivery Date" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="font-weight: bold; width: 10%">
                                        <cc1:ucLabel ID="lblReportType" runat="server" Text="Report Type"></cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold; width: 1%">
                                        <cc1:ucLabel ID="UcLabel2" runat="server">:</cc1:ucLabel>
                                    </td>
                                    <td align="center" class="radiobuttonlist">
                                        <table width="33%" cellspacing="3" cellpadding="0" align="left" class="top-settingsnoborder"
                                            style="margin-top: 10px;">
                                            <tr>
                                                <td style="width: 34%;" class="nobold">
                                                    <cc1:ucRadioButton ID="rdoSummary" runat="server" GroupName="Report" Text="Summary"
                                                        Checked="true" />
                                                </td>
                                                <td style="width: 33%;" class="nobold">
                                                    <cc1:ucRadioButton ID="rdoDetail" runat="server" GroupName="Report" Text="Detail" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="font-weight: bold; width: 10%">
                                        <cc1:ucLabel ID="lblViewType" runat="server" Text="View Type"></cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold; width: 1%">
                                        <cc1:ucLabel ID="UcLabel7" runat="server">:</cc1:ucLabel>
                                    </td>
                                    <td class="radiobuttonlist">
                                        <table width="50%" cellspacing="3" cellpadding="0" align="left" class="top-settingsnoborder"
                                            style="margin-top: 10px;">
                                            <tr>
                                                <td style="width: 20%;" class="nobold">
                                                    <cc1:ucRadioButton ID="rdbDailyView" runat="server" GroupName="View" Text="Daily View"
                                                        Checked="true" />
                                                </td>
                                                <td style="width: 20%;" class="nobold">
                                                    <cc1:ucRadioButton ID="rdbWeeklyView" runat="server" GroupName="View" Text="weekly View" />
                                                </td>
                                                <td style="width: 20%;" class="nobold">
                                                    <cc1:ucRadioButton ID="rdbMonthlyView" runat="server" GroupName="View" Text="Monthly View" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <div class="button-row">
                                <cc1:ucButton ID="UcButton1" runat="server" Text="Generate Report" CssClass="button"
                                    OnClick="btnGenerateReport_Click" />
                            </div>
                        </td>
                    </tr>
                </table>
            </cc1:ucPanel>
            <cc1:ucPanel ID="UcCarrierVendorReportViewPanel" runat="server" Visible="false">
                <table style="width: 100%;">
                    <tr>
                        <td>
                            <rsweb:ReportViewer ID="OnlineBookingActivityReportViewer" runat="server" Width="950px"
                                DocumentMapCollapsed="True" Font-Names="Verdana" Font-Size="8pt" InteractiveDeviceInfos="(Collection)"
                                WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt" Height="416px">
                                <LocalReport ReportPath="ModuleUI\Appointment\Reports\RDLC\OnlineBookingActivityReportTypes.rdlc">
                                </LocalReport>
                            </rsweb:ReportViewer>
                            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:sConn %>"
                                SelectCommand="spRPTBOK_OnlineBookingActivity" SelectCommandType="StoredProcedure">
                                <SelectParameters>
                                    <asp:ControlParameter ControlID="hdnVendorIDs" DefaultValue="-1" Name="SelectedVendorIDs"
                                        PropertyName="Value" Type="String" />
                                    <asp:ControlParameter ControlID="hdnSiteIDs" DefaultValue="-1" Name="SelectedSiteIDs"
                                        PropertyName="Value" Type="String" />
                                    <asp:ControlParameter ControlID="hdnInventoryMangerIDs" DefaultValue="-1" Name="SelectedInventoryMgrIDs"
                                        PropertyName="Value" Type="String" />
                                    <asp:ControlParameter ControlID="hdnCountryIDs" DefaultValue="-1" Name="SelectedCountryIDs"
                                        PropertyName="Value" Type="String" />
                                    <asp:ControlParameter ControlID="hdnToDt" DefaultValue="" Name="DateTo" PropertyName="Value"
                                        Type="DateTime" />
                                    <asp:ControlParameter ControlID="hdnFromDt" Name="DateFrom" PropertyName="Value"
                                        Type="DateTime" />
                                    <asp:ControlParameter ControlID="hdnSearchBy" DefaultValue="BookingDate" Name="SearchBy"
                                        PropertyName="Value" Type="String" />
                                    <asp:Parameter DefaultValue="Summary" Name="ViewType" Type="String" />
                                </SelectParameters>
                            </asp:SqlDataSource>
                            <asp:SqlDataSource ID="SqlDataSource3" runat="server" ConnectionString="<%$ ConnectionStrings:sConn %>"
                                SelectCommand="spRPTBOK_OnlineBookingActivity" SelectCommandType="StoredProcedure">
                                <SelectParameters>
                                    <asp:ControlParameter ControlID="hdnVendorIDs" DefaultValue="-1" Name="SelectedVendorIDs"
                                        PropertyName="Value" Type="String" />
                                    <asp:ControlParameter ControlID="hdnSiteIDs" DefaultValue="-1" Name="SelectedSiteIDs"
                                        PropertyName="Value" Type="String" />
                                    <asp:ControlParameter ControlID="hdnInventoryMangerIDs" DefaultValue="-1" Name="SelectedInventoryMgrIDs"
                                        PropertyName="Value" Type="String" />
                                    <asp:ControlParameter ControlID="hdnCountryIDs" DefaultValue="-1" Name="SelectedCountryIDs"
                                        PropertyName="Value" Type="String" />
                                    <asp:ControlParameter ControlID="hdnToDt" DefaultValue="" Name="DateTo" PropertyName="Value"
                                        Type="DateTime" />
                                    <asp:ControlParameter ControlID="hdnFromDt" Name="DateFrom" PropertyName="Value"
                                        Type="DateTime" />
                                    <asp:ControlParameter ControlID="hdnSearchBy" DefaultValue="BookingDate" Name="SearchBy"
                                        PropertyName="Value" Type="String" />
                                    <asp:Parameter DefaultValue="Detailed" Name="ViewType" Type="String" />
                                </SelectParameters>
                            </asp:SqlDataSource>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <%-- <rsweb:ReportViewer ID="ViewTypeReportViewer" runat="server" Width="950px" DocumentMapCollapsed="True"
                                Font-Names="Verdana" Font-Size="8pt" InteractiveDeviceInfos="(Collection)" WaitMessageFont-Names="Verdana"
                                WaitMessageFont-Size="14pt" Height="416px">
                                <LocalReport ReportPath="ModuleUI\Appointment\Reports\RDLC\OnlineBookingActivityViewTypes.rdlc">
                                    <DataSources>
                                        <rsweb:ReportDataSource DataSourceId="SqlDataSource2" Name="dtOnlineBookingActivityViewType" />
                                    </DataSources>
                                </LocalReport>
                            </rsweb:ReportViewer>--%>
                            <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:sConn %>"
                                SelectCommand="spRPTBOK_OnlineBookingActivity" SelectCommandType="StoredProcedure">
                                <SelectParameters>
                                    <asp:ControlParameter ControlID="hdnVendorIDs" DefaultValue="-1" Name="SelectedVendorIDs"
                                        PropertyName="Value" Type="String" />
                                    <asp:ControlParameter ControlID="hdnSiteIDs" DefaultValue="-1" Name="SelectedSiteIDs"
                                        PropertyName="Value" Type="String" />
                                    <asp:ControlParameter ControlID="hdnInventoryMangerIDs" DefaultValue="-1" Name="SelectedInventoryMgrIDs"
                                        PropertyName="Value" Type="String" />
                                    <asp:ControlParameter ControlID="hdnCountryIDs" DefaultValue="-1" Name="SelectedCountryIDs"
                                        PropertyName="Value" Type="String" />
                                    <asp:ControlParameter ControlID="hdnToDt" DefaultValue="" Name="DateTo" PropertyName="Value"
                                        Type="DateTime" />
                                    <asp:ControlParameter ControlID="hdnFromDt" Name="DateFrom" PropertyName="Value"
                                        Type="DateTime" />
                                    <asp:ControlParameter ControlID="hdnSearchBy" DefaultValue="BookingDate" Name="SearchBy"
                                        PropertyName="Value" Type="String" />
                                    <asp:ControlParameter ControlID="hdnViewType" DefaultValue="-1" Name="ViewType" PropertyName="Value"
                                        Type="String" />
                                </SelectParameters>
                            </asp:SqlDataSource>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <cc1:ucButton ID="btnBackSearch" runat="server" Text="Back To Search Page" CssClass="button"
                                OnClick="btnBack_Click" />
                        </td>
                    </tr>
                </table>
            </cc1:ucPanel>
        </div>
    </div>
</asp:Content>

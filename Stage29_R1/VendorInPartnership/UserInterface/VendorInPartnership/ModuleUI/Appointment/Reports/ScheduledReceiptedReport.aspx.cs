﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebUtilities;
using BusinessEntities.ModuleBE.Appointment.Booking;
using BusinessLogicLayer.ModuleBAL.Appointment.Booking;
using BusinessEntities.ModuleBE.Appointment.Reports;
using BusinessLogicLayer.ModuleBAL.Appointment.Reports;
using Utilities;

public partial class ModuleUI_Appointment_Reports_ScheduledReceiptedReport : CommonPage
{
    protected string siteRequired = WebCommon.getGlobalResourceValue("SiteRequired");
    protected string NoRecordsFound = WebCommon.getGlobalResourceValue("NoRecordsFound");    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            try
            {
                txtDate.innerControltxtDate.Value = Convert.ToString(DateTime.Now.ToString("dd/MM/yyyy"));
                BindGridView();
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex); ;
            }
        }
        btnExportToExcel1.GridViewControl = UcGridView1;
        btnExportToExcel1.FileName = "ScheduledReceiptedReport";
    }

    public void BindGridView()
    {
        try
        {
            ScheduledReceiptedReportDetailsBE oScheduledReceiptedReportBE = new ScheduledReceiptedReportDetailsBE();
            ScheduledReceiptedReportBAL oScheduledReceiptedReportBAL = new ScheduledReceiptedReportBAL();
            List<ScheduledReceiptedReportDetailsBE> lstScheduledReceiptedReportBE = new List<ScheduledReceiptedReportDetailsBE>();
            oScheduledReceiptedReportBE.Action = "ScheduledReceiptedReport";
            oScheduledReceiptedReportBE.Date = Common.GetYYYY_MM_DD(txtDate.innerControltxtDate.Value);
            lstScheduledReceiptedReportBE = oScheduledReceiptedReportBAL.ScheduledReceiptedReportDetailsBAL(oScheduledReceiptedReportBE);
            if (lstScheduledReceiptedReportBE.Count > 0)
            {
                UcGridView1.DataSource = lstScheduledReceiptedReportBE;
                UcGridView1.DataBind();                
                lblError.Text = string.Empty;
                UcGridView1.Visible = true;
                ViewState["lstSites"] = lstScheduledReceiptedReportBE;              
            }
            else
            {
                UcGridView1.DataSource = null;
                UcGridView1.DataBind();               
                lblError.Text = NoRecordsFound;
            }

        }
        catch (Exception ex)
        {
            LogUtility.SaveErrorLogEntry(ex); ;
        }
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        BindGridView();
    }
    public override BusinessEntities.BaseBe[] SubSort(GridViewSortEventArgs e)
    {
        return Utilities.GenericListHelper<ScheduledReceiptedReportDetailsBE>.SortList((List<ScheduledReceiptedReportDetailsBE>)ViewState["lstSites"], e.SortExpression, e.SortDirection).ToArray();
    }
    protected void lnkSite_Onclick(object sender, EventArgs e)
    {
        try
        {
            LinkButton btn = (LinkButton)sender;
            string CommandArgument = btn.CommandArgument;
            var script = "window.open('" + EncryptQuery("ScheduledReceiptedReportByVendor.aspx?SiteID=" + btn.CommandArgument + "&Date=" + txtDate.innerControltxtDate.Value) + "');";
            ScriptManager.RegisterStartupScript(pnlUP1, pnlUP1.GetType(), "script", script, true);            
        }
        catch (Exception ex)
        {
            LogUtility.SaveErrorLogEntry(ex);
        }
    }
}
﻿using BaseControlLibrary;
using BusinessEntities.ModuleBE.Appointment.Booking;
using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using BusinessLogicLayer.ModuleBAL.Appointment.Booking;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Utilities;
using WebUtilities;
public partial class ModuleUI_Appointment_Reports_HazardousItemCheckReport : CommonPage
{
    bool IsExportClicked = false;
    bool IsButtonsearchedClicked = false;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            pager1.PageSize = 200;
            pager1.GenerateGoToSection = false;
            pager1.GeneratePagerInfoSection = false;
            pager1.Visible = false;
            txtFromDate.Text = DateTime.Now.AddDays(-DateTime.Now.Day + 1).ToString("dd/MM/yyyy");
            txtToDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
            hdnJSFromDt.Value = txtFromDate.Text;
            hdnJSToDt.Value = txtToDate.Text;
            if (!string.IsNullOrEmpty(GetQueryStringValue("VendorID")) && !string.IsNullOrEmpty(GetQueryStringValue("DateTo"))
                && !string.IsNullOrEmpty(GetQueryStringValue("DateFrom")) && !string.IsNullOrEmpty(GetQueryStringValue("SiteID")))
            {
                BindGridView(1);
            }
            else if (!string.IsNullOrEmpty(GetQueryStringValue("BindSummary")))
            {
                BindGridView(1);
            }
        }
    }
    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (GetQueryStringValue("RetainData") != null && GetQueryStringValue("RetainData").ToString() == "true")
        {
            if (Session["Hazardous"] != null && IsButtonsearchedClicked == false)
            {
                RetainSearchData();
            }
        }
    }
    protected void btnBack_Click(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(GetQueryStringValue("VendorID")))
        {
            EncryptQueryString("HazardousItemCheckReport.aspx?BindSummary=true");
        }
        else
        {
            EncryptQueryString("HazardousItemCheckReport.aspx?RetainData=true");
        }
    }
    protected void btnExportToExcel_Click(object sender, EventArgs e)
    {
        IsExportClicked = true;
        BindGridView();
        if (!string.IsNullOrEmpty(GetQueryStringValue("VendorID")))
        {
            WebCommon.ExportHideHidden("Hazardous_Detailed_Report", grdHazardousByVendorID, null, false, true);
        }
        else
        {
            if (rdoSummary.Checked == true)
            {
                WebCommon.ExportHideHidden("Hazardous_Summary_Report", grdHazardous, null, false, true);
            }
            else
            {
                WebCommon.ExportHideHidden("Hazardous_Detailed_Report", grdHazardousByVendorID, null, false, true);
            }
        }
    }
    public void pager_Command(object sender, CommandEventArgs e)
    {
        int currnetPageIndx = Convert.ToInt32(e.CommandArgument);
        pager1.CurrentIndex = currnetPageIndx;
        BindGridView(currnetPageIndx);
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        if (msSite.SelectedSiteIDs != null && msSite.SelectedSiteIDs != "")
        {
            BindGridView(1);
            tblsearch.Visible = false;
            btnExportToExcel.Visible = true;
            tblgrid.Visible = true;
            IsButtonsearchedClicked = true;
        }
        else
        {
            var SiteSearchMessage = WebCommon.getGlobalResourceValue("Pleaseselectatleastonesite");
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + SiteSearchMessage + "');", true);
            return;
        }
    }
    protected void lnkVendor_Click(object sender, EventArgs e)
    {
        IsButtonsearchedClicked = true;
        LinkButton btn = (LinkButton)(sender);
        string[] argument = btn.CommandArgument.Split(',');
        EncryptQueryString("HazardousItemCheckReport.aspx?VendorID=" + Convert.ToString(argument[0]) + "&DateTo=" + hdnJSToDt.Value + "&DateFrom=" + hdnJSFromDt.Value + "&SiteID=" + Convert.ToString(argument[1]) + "");
    }
    private void SetSession(APPBOK_BookingBE aPPBOK)
    {
        Session["Hazardous"] = null;
        Session.Remove("Hazardous");
        TextBox txtVendor = (TextBox)msVendor.FindControl("ucVendor").FindControl("txtVendorNo");
        Hashtable htInventoryReview1 = new Hashtable();
        htInventoryReview1.Add("txtVendor", txtVendor.Text);
        htInventoryReview1.Add("SelectedSiteId", aPPBOK.SelectedSiteIds);
        htInventoryReview1.Add("SelectedCarrierID", aPPBOK.SelectedCarrierIDs);
        htInventoryReview1.Add("SelectedVendorIDs", aPPBOK.SelectedVendorIDs);
        htInventoryReview1.Add("IsSearchedByVendorNo", msVendor.IsSearchedByVendorNo);
        htInventoryReview1.Add("DateTo", hdnJSToDt.Value);
        htInventoryReview1.Add("DateFrom", hdnJSFromDt.Value);
        if (rdoDetailed.Checked == true)
        {
            htInventoryReview1.Add("rdoDetailedChecked", true);
        }
        else
        {
            htInventoryReview1.Add("rdoDetailedChecked", false);
        }
        Session["Hazardous"] = htInventoryReview1;
    }
    public void RetainSearchData()
    {
        Hashtable htInventoryReview1 = (Hashtable)Session["Hazardous"];
        bool rdoDetailedChecked = (htInventoryReview1.ContainsKey("rdoDetailedChecked") && htInventoryReview1["rdoDetailedChecked"] != null) ? Convert.ToBoolean(htInventoryReview1["rdoDetailedChecked"]) : false;
        if (rdoDetailedChecked == true)
        {
            rdoDetailed.Checked = true;
            rdoSummary.Checked = false;
        }
        else
        {
            rdoDetailed.Checked = false;
            rdoSummary.Checked = true;
        }
        //********** Vendor ***************
        string txtVendor = (htInventoryReview1.ContainsKey("txtVendor") && htInventoryReview1["txtVendor"] != null) ? htInventoryReview1["txtVendor"].ToString() : "";
        string VendorId = (htInventoryReview1.ContainsKey("SelectedVendorIDs") && htInventoryReview1["SelectedVendorIDs"] != null) ? htInventoryReview1["SelectedVendorIDs"].ToString() : "";
        TextBox txtVendorId = msVendor.FindControl("ucVendor").FindControl("txtVendorNo") as TextBox;

        bool IsSearchedByVendorNo = (htInventoryReview1.ContainsKey("IsSearchedByVendorNo") && htInventoryReview1["IsSearchedByVendorNo"] != null) ? Convert.ToBoolean(htInventoryReview1["IsSearchedByVendorNo"]) : false;
        string txtVendorIdText = txtVendorId.Text;
        ucListBox lstRightVendor = msVendor.FindControl("lstRight") as ucListBox;
        ucListBox lstLeftVendor = msVendor.FindControl("ucVendor").FindControl("lstLeft") as ucListBox;
        string strVendorId = string.Empty;
     
        lstLeftVendor.Items.Clear();
        if (lstLeftVendor != null && lstRightVendor != null && (!string.IsNullOrEmpty(VendorId) || !string.IsNullOrEmpty(VendorId)))
        {
            lstLeftVendor.Items.Clear();
            //lstRightVendor.Items.Clear();

            if (IsSearchedByVendorNo == true)
            {
                msVendor.SearchVendorNumberClick(txtVendor);
                //parsing successful 
            }
            else
            {
                msVendor.SearchVendorClick(txtVendor);
                //parsing failed. 
            }

            if (!string.IsNullOrEmpty(VendorId))
            {
                string[] strIncludeVendorIDs = VendorId.Split(',');
                for (int index = 0; index < strIncludeVendorIDs.Length; index++)
                {
                    ListItem listItem = lstLeftVendor.Items.FindByValue(strIncludeVendorIDs[index]);
                    if (listItem != null)
                    {

                        strVendorId += strIncludeVendorIDs[index] + ",";
                        lstRightVendor.Items.Add(listItem);
                        lstLeftVendor.Items.Remove(listItem);
                    }
                }
            }

            HiddenField hdnSelectedVendor = msVendor.FindControl("hiddenSelectedIDs") as HiddenField;
            hdnSelectedVendor.Value = strVendorId;
        }


        //  *********** Site ***************
        string SiteId = (htInventoryReview1.ContainsKey("SelectedSiteId") && htInventoryReview1["SelectedSiteId"] != null) ? htInventoryReview1["SelectedSiteId"].ToString() : "";
        ucListBox lstRightSite = msSite.FindControl("lstRight") as ucListBox;
        ucListBox lstLeftSite = msSite.FindControl("lstLeft") as ucListBox;
        string msSiteid = string.Empty;
        lstRightSite.Items.Clear();
        lstRightSite.Items.Clear();
        msSite.SelectedSiteIDs = "";
        if (lstLeftSite != null && lstRightSite != null && !string.IsNullOrEmpty(SiteId))
        {
            lstLeftSite.Items.Clear();
            lstRightSite.Items.Clear();
            msSite.BindSite();

            string[] strSiteIDs = SiteId.Split(',');
            for (int index = 0; index < strSiteIDs.Length; index++)
            {
                ListItem listItem = lstLeftSite.Items.FindByValue(strSiteIDs[index]);
                if (listItem != null)
                {
                    msSiteid = msSiteid + strSiteIDs[index].ToString() + ",";
                    lstRightSite.Items.Add(listItem);
                    lstLeftSite.Items.Remove(listItem);
                }

            }
            if (string.IsNullOrEmpty(msSite.SelectedSiteIDs))
                msSite.SelectedSiteIDs = msSiteid.Trim(',');
        }


        //  *********** carrier ***************
        string CarrierId = (htInventoryReview1.ContainsKey("SelectedCarrierID") && htInventoryReview1["SelectedCarrierID"] != null) ? htInventoryReview1["SelectedCarrierID"].ToString() : "";
        ucListBox lstRightCarrier = msCarrier.FindControl("lstRight") as ucListBox;
        ucListBox lstLeftCarrier = msCarrier.FindControl("lstLeft") as ucListBox;
        string msCarrierId = string.Empty;
        if (lstRightCarrier !=null && lstRightCarrier.Items.Count > 0)
        {
            lstRightCarrier.Items.Clear();
        }
        string strCarrerIDs = string.Empty;
        if (lstLeftCarrier != null && lstRightCarrier != null && !string.IsNullOrEmpty(CarrierId))
        {
            lstLeftCarrier.Items.Clear();

            if (!string.IsNullOrEmpty(CarrierId))
            {
                string[] strIncludeCarrierIDs = CarrierId.Split(',');
                for (int index = 0; index < strIncludeCarrierIDs.Length; index++)
                {
                    ListItem listItem = lstLeftCarrier.Items.FindByValue(strIncludeCarrierIDs[index]);
                    if (listItem != null)
                    {

                        strCarrerIDs += strIncludeCarrierIDs[index] + ",";
                        lstRightCarrier.Items.Add(listItem);
                        lstLeftCarrier.Items.Remove(listItem);
                    }
                }
            }
            HiddenField hdnSelectedCarreier = msCarrier.FindControl("hiddenSelectedIDs") as HiddenField;
            hdnSelectedCarreier.Value = strCarrerIDs;
        }

        txtFromDate.Text = (htInventoryReview1.ContainsKey("DateFrom") && htInventoryReview1["DateFrom"] != null) ? Convert.ToString(htInventoryReview1["DateFrom"]) : string.Empty;
        txtToDate.Text = (htInventoryReview1.ContainsKey("DateTo") && htInventoryReview1["DateTo"] != null) ? Convert.ToString(htInventoryReview1["DateTo"]) : string.Empty;
        Session["Hazardous"] = null;
        Session.Remove("Hazardous");
    }
    public void BindGridView(int Page = 1)
    {
        APPBOK_BookingBE oAPPBOK_BookingBE = new APPBOK_BookingBE();
        APPBOK_BookingBAL aPPBOK_BookingBAL = new APPBOK_BookingBAL();

        if (!string.IsNullOrEmpty(GetQueryStringValue("VendorID")))
        {
            oAPPBOK_BookingBE.Action = "GetHazrdousBookingByVendorid";
        }
        else if (!string.IsNullOrEmpty(GetQueryStringValue("BindSummary")))
        {
            oAPPBOK_BookingBE.Action = "GetHazrdousBooking";
        }
        else
        {
            if (rdoSummary.Checked == true)
            {
                oAPPBOK_BookingBE.Action = "GetHazrdousBooking";
            }
            else
            {
                oAPPBOK_BookingBE.Action = "GetHazrdousBookingByVendorid";
            }
        }
        if (Session["Hazardous"] != null && !string.IsNullOrEmpty(GetQueryStringValue("BindSummary")))
        {
            Hashtable htInventoryReview1 = (Hashtable)Session["Hazardous"];
            oAPPBOK_BookingBE.SelectedVendorIDs = (htInventoryReview1.ContainsKey("SelectedVendorIDs") && htInventoryReview1["SelectedVendorIDs"] != null) ? htInventoryReview1["SelectedVendorIDs"].ToString() : null;
            oAPPBOK_BookingBE.SelectedSiteIds = (htInventoryReview1.ContainsKey("SelectedSiteId") && htInventoryReview1["SelectedSiteId"] != null) ? htInventoryReview1["SelectedSiteId"].ToString() : null;
            oAPPBOK_BookingBE.SelectedCarrierIDs = (htInventoryReview1.ContainsKey("SelectedCarrierIDs") && htInventoryReview1["SelectedCarrierIDs"] != null) ? htInventoryReview1["SelectedCarrierIDs"].ToString() : null;
            oAPPBOK_BookingBE.ToDate = (htInventoryReview1.ContainsKey("DateTo") && htInventoryReview1["DateTo"] != null) ? Common.GetMM_DD_YYYY(htInventoryReview1["DateTo"].ToString()) : (DateTime?)null;
            oAPPBOK_BookingBE.FromDate = (htInventoryReview1.ContainsKey("DateFrom") && htInventoryReview1["DateFrom"] != null) ? Common.GetMM_DD_YYYY(htInventoryReview1["DateFrom"].ToString()) : (DateTime?)null;
            hdnJSToDt.Value = (htInventoryReview1.ContainsKey("DateTo") && htInventoryReview1["DateTo"] != null) ? (htInventoryReview1["DateTo"].ToString()) : "";
            hdnJSFromDt.Value = (htInventoryReview1.ContainsKey("DateFrom") && htInventoryReview1["DateFrom"] != null) ? (htInventoryReview1["DateFrom"].ToString()) : "";
        }
        else
        {
            if (!string.IsNullOrEmpty(GetQueryStringValue("SiteID")))
            {
                oAPPBOK_BookingBE.SelectedSiteIds = GetQueryStringValue("SiteID");
            }
            else
            {
                oAPPBOK_BookingBE.SelectedSiteIds = msSite.SelectedSiteIDs;
            }

            if (!string.IsNullOrEmpty(GetQueryStringValue("VendorID")))
            {
                oAPPBOK_BookingBE.SelectedVendorIDs = GetQueryStringValue("VendorID");
            }
            else
            {
                oAPPBOK_BookingBE.SelectedVendorIDs = msVendor.SelectedVendorIDs;
            }

            if (!string.IsNullOrEmpty(GetQueryStringValue("CarreirIDs")))
            {
                oAPPBOK_BookingBE.SelectedCarrierIDs = GetQueryStringValue("CarreirIDs");
            }
            else
            {
                oAPPBOK_BookingBE.SelectedCarrierIDs = msCarrier.SelectedCarrierIDs;
            }

            if (!string.IsNullOrEmpty(GetQueryStringValue("DateTo")))
            {
                oAPPBOK_BookingBE.ToDate = Common.GetMM_DD_YYYY(GetQueryStringValue("DateTo"));
                hdnJSToDt.Value = GetQueryStringValue("DateTo");
            }
            else
            {
                oAPPBOK_BookingBE.ToDate = Common.GetMM_DD_YYYY(hdnJSToDt.Value);
            }
            if (!string.IsNullOrEmpty(GetQueryStringValue("DateFrom")))
            {
                oAPPBOK_BookingBE.FromDate = Common.GetMM_DD_YYYY(GetQueryStringValue("DateFrom"));
                hdnJSFromDt.Value = GetQueryStringValue("DateFrom");
            }
            else
            {
                oAPPBOK_BookingBE.FromDate = Common.GetMM_DD_YYYY(hdnJSFromDt.Value);
                if (string.IsNullOrEmpty(GetQueryStringValue("BindSummary")))
                    this.SetSession(oAPPBOK_BookingBE);
            }
        }
        if (IsExportClicked)
        {
            oAPPBOK_BookingBE.GridCurrentPageNo = 0;
            oAPPBOK_BookingBE.GridPageSize = 0;
        }
        else
        {
            oAPPBOK_BookingBE.GridCurrentPageNo = Page;
            oAPPBOK_BookingBE.GridPageSize = 200;
        }
        var lst = aPPBOK_BookingBAL.GetHazardousBookingBAL(oAPPBOK_BookingBE);
        if (lst != null && lst.Count > 0)
        {
            pager1.Visible = true;
            pager1.ItemCount = Convert.ToInt32(lst[0].TotalRecords);
            if (!string.IsNullOrEmpty(GetQueryStringValue("VendorID")))
            {
                grdHazardousByVendorID.DataSource = lst;
                grdHazardousByVendorID.DataBind();
                grdHazardous.Visible = false;
                grdHazardousByVendorID.Visible = true;
            }
            else
            {
                if (rdoSummary.Checked == true)
                {
                    grdHazardous.DataSource = lst;
                    grdHazardous.DataBind();
                    grdHazardous.Visible = true;
                    grdHazardousByVendorID.Visible = false;
                }
                else
                {
                    grdHazardousByVendorID.DataSource = lst;
                    grdHazardousByVendorID.DataBind();
                    grdHazardous.Visible = false;
                    grdHazardousByVendorID.Visible = true;
                }
            }

            tblsearch.Visible = false;
            btnExportToExcel.Visible = true;
            tblgrid.Visible = true;
        }
        else
        {
            if (!string.IsNullOrEmpty(GetQueryStringValue("VendorID")))
            {
                grdHazardousByVendorID.DataSource = null;
                grdHazardousByVendorID.DataBind();
            }
            else
            {
                if (rdoSummary.Checked == true)
                {
                    grdHazardous.DataSource = null;
                    grdHazardous.DataBind();
                }
                else
                {
                    grdHazardousByVendorID.DataSource = null;
                    grdHazardousByVendorID.DataBind();
                }
            }

            tblsearch.Visible = false;
            btnExportToExcel.Visible = false;
            tblgrid.Visible = true;
        }
    }
}
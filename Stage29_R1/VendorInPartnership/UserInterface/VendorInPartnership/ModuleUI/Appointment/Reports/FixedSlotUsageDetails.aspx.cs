﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLogicLayer.ModuleBAL.Appointment.Reports;
using BusinessEntities.ModuleBE.Appointment.Reports;

public partial class FixedSlotUsageDetails : CommonPage
{    

    protected void Page_Load(object sender, EventArgs e) {
        ucExportToExcel1.CurrentPage = this;
        ucExportToExcel1.GridViewControl = ucGridView1;
        ucExportToExcel1.FileName = "FixedSlotUsageReport";
        if (!Page.IsPostBack) {
            BindReport();
        }
    }

    private void BindReport() {
        FixedSlotUsageReportBAL oFixedSlotUsageReportBAL = new FixedSlotUsageReportBAL();
        FixedSlotUsageReportBE oFixedSlotUsageReportBE = new FixedSlotUsageReportBE();

        oFixedSlotUsageReportBE.Action = "DetailedView";
        if (!string.IsNullOrEmpty(GetQueryStringValue("FixedSlotID"))) {
            oFixedSlotUsageReportBE.FixedSlotID = Convert.ToInt32(GetQueryStringValue("FixedSlotID"));
        }
        else {
            if (GetQueryStringValue("ScheduleType") == "V")
                oFixedSlotUsageReportBE.VendorID = Convert.ToInt32(GetQueryStringValue("VendorID"));
            else
                oFixedSlotUsageReportBE.CarrierID = Convert.ToInt32(GetQueryStringValue("VendorID"));

            oFixedSlotUsageReportBE.SiteID = Convert.ToInt32(GetQueryStringValue("SiteID"));
        }

        oFixedSlotUsageReportBE.Period = Convert.ToInt32(GetQueryStringValue("Period"));

        List<FixedSlotUsageReportBE> lstFixedSlotUsageReport = oFixedSlotUsageReportBAL.GetFixedSlotUsageDetailsBAL(oFixedSlotUsageReportBE);

        if (lstFixedSlotUsageReport != null && lstFixedSlotUsageReport.Count > 0) {
            ViewState["lstFixedSlotUsageReport"] = lstFixedSlotUsageReport;
            ucGridView1.DataSource = lstFixedSlotUsageReport;
            ucGridView1.DataBind();
        }
    }

    protected void GridView1_DataBound(object sender, GridViewRowEventArgs e) {
         GridViewRow gvr = e.Row;
         if (e.Row.RowType == DataControlRowType.DataRow) {
             if (!string.IsNullOrWhiteSpace(gvr.Cells[0].Text)  && gvr.Cells[0].Text != "&nbsp;") {
                 gvr.Style.Add(HtmlTextWriterStyle.FontWeight, "Bold");
                 gvr.Style.Add(HtmlTextWriterStyle.Color, "#333333");
             }
         }       
    }

    public override BusinessEntities.BaseBe[] SubSort(GridViewSortEventArgs e) {
        return Utilities.GenericListHelper<FixedSlotUsageReportBE>.SortList((List<FixedSlotUsageReportBE>)ViewState["lstFixedSlotUsageReport"], e.SortExpression, e.SortDirection).ToArray();
    }
}
﻿using BusinessEntities.ModuleBE.Security;
using BusinessLogicLayer.ModuleBAL.Security;
using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;

public partial class ModuleUI_Appointment_Reports_UnconfirmedUnusedStandardDcheduledSlots : CommonPage
{
    protected void Page_Init(object sender, EventArgs e)
    {
        ucVendorTemplateSelect.CurrentPage = this;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            txtFromDate.Text = DateTime.Now.AddDays(-DateTime.Now.Day + 1).ToString("dd/MM/yyyy");
            txtToDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
            hdnJSFromDt.Value = txtFromDate.Text;
            hdnJSToDt.Value = txtToDate.Text;
        }
        TextBox txtVendor = Utilities.Common.FindControlRecursive(this.Page.Controls[0], "txtVendorNo") as TextBox;
        if (txtVendor != null)
            txtVendor.Focus();
    }

    /// <summary>
    /// Method to show Vendor on the basis of selected Template
    /// </summary>
    public override void TemplateSelectedIndexChanged()
    {
        base.TemplateSelectedIndexChanged();

        int VendorTemplateId = Convert.ToInt32(ucVendorTemplateSelect.innerControlddlTemplate.SelectedValue);

        ListBox lstSelectedVendor = (ListBox)msVendor.FindControl("lstRight");
        ListBox lstVendor = (ListBox)msVendor.FindControl("ucVendor").FindControl("lstLeft");

        if (VendorTemplateId > 0)
        {
            SCT_TemplateBAL oSCT_TemplateBAL = new SCT_TemplateBAL();
            SCT_TemplateBE oSCT_TemplateBE = new SCT_TemplateBE();

            oSCT_TemplateBE.VendorTemplateID = VendorTemplateId;
            oSCT_TemplateBE.Action = "GetVendorTemplateInfobyID";
            List<SCT_TemplateBE> lstVendorTemplate = oSCT_TemplateBAL.GetVendorTemplateById(oSCT_TemplateBE);

            if (lstVendorTemplate != null && lstVendorTemplate.Count > 0)
            {
                lstSelectedVendor.Items.Clear();
                msVendor.innerControlHiddenField.Value = string.Empty;
                msVendor.innerControlHiddenField2.Value = string.Empty;
                for (int iCount = 0; iCount < lstVendorTemplate.Count; iCount++)
                {
                    lstSelectedVendor.Items.Add(new ListItem(lstVendorTemplate[iCount].Vendor.Vendor_Name, lstVendorTemplate[iCount].Vendor.VendorID.ToString()));
                    msVendor.SelectedVendorIDs = lstVendorTemplate[iCount].Vendor.VendorID.ToString();
                    msVendor.SelectedVendorName = lstVendorTemplate[iCount].Vendor.Vendor_Name.ToString();
                }
            }
        }
        else
        {
            lstSelectedVendor.Items.Clear();
            msVendor.innerControlHiddenField.Value = string.Empty;
            msVendor.innerControlHiddenField2.Value = string.Empty;
        }
    }

    protected void btnGenerateReport_Click(object sender, EventArgs e)
    {
        try
        {
            if (!string.IsNullOrEmpty(msSite.SelectedSiteIDs))
                hdnSiteIDs.Value = msSite.SelectedSiteIDs;
            if (!string.IsNullOrEmpty(msCountry.SelectedCountryIDs))
                hdnCountryIDs.Value = msCountry.SelectedCountryIDs;
            if (!string.IsNullOrEmpty(msVendor.SelectedVendorIDs))
                hdnVendorIDs.Value = msVendor.SelectedVendorIDs;
            if (!string.IsNullOrEmpty(txtFromDate.Text))
                hdnFromDt.Value = Utilities.Common.TextToDateFormat(hdnJSFromDt.Value).Year + "-" + Utilities.Common.TextToDateFormat(hdnJSFromDt.Value).Month + "-" + Utilities.Common.TextToDateFormat(hdnJSFromDt.Value).Day;
            if (!string.IsNullOrEmpty(txtToDate.Text))
                hdnToDt.Value = Utilities.Common.TextToDateFormat(hdnJSToDt.Value).Year + "-" + Utilities.Common.TextToDateFormat(hdnJSToDt.Value).Month + "-" + Utilities.Common.TextToDateFormat(hdnJSToDt.Value).Day;

            ReportParameter[] reportParameter = new ReportParameter[5];
            reportParameter[0] = new ReportParameter("Country", msCountry.SelectedCountryName);
            reportParameter[1] = new ReportParameter("Site", msSite.SelectedSiteName);
            reportParameter[2] = new ReportParameter("Vendor", msVendor.SelectedVendorName);
            reportParameter[3] = new ReportParameter("DateFrom", hdnJSFromDt.Value);
            reportParameter[4] = new ReportParameter("DateTo", hdnJSToDt.Value);
            this.UnconfirmedUnusedStandardDcheduledSlotsReportViewer.LocalReport.SetParameters(reportParameter);

            UcCarrierVendorPanel.Visible = false;
            UcCarrierVendorReportViewPanel.Visible = true;
        }
        catch (Exception ex)
        {
            Utilities.LogUtility.SaveErrorLogEntry(ex, "Report - UnconfirmedUnusedStandardDcheduledSlots");
        }
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        EncryptQueryString("UnconfirmedUnusedStandardScheduledSlots.aspx");
    }

    protected void SqlDataSourceCommon_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
    {
        e.Command.CommandTimeout = 0;
    }
}
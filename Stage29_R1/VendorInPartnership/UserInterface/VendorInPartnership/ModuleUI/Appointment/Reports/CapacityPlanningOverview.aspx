﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    AutoEventWireup="true" CodeFile="CapacityPlanningOverview.aspx.cs" Inherits="ModuleUI_Appointment_Reports_CapacityPlanningOverview" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Register Src="~/CommonUI/UserControls/ucSite.ascx" TagName="ucSite" TagPrefix="cc2" %>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <%--<script type="text/javascript" language="javascript">
        function setValue1(target) {
            document.getElementById('<%=hdnJSFromDt.ClientID %>').value = target.value;
        }

        function setValue2(target) {
            document.getElementById('<%=hdnJSToDt.ClientID %>').value = target.value;
        }

    </script>--%>
    <script type="text/javascript">
        $(document).ready(function () {
            $("body").keypress(function (e) {
                if (e.which == 13 || e.keyCode == 13) {
                    $('#<%=UcButton1.ClientID %>').click();
                    return false;
                }
            });
        });
    </script>
    <asp:ScriptManager ID="SM1" runat="server">
    </asp:ScriptManager>
    <asp:HiddenField ID="hdnJSFromDt" runat="server" />
    <h2>
        <cc1:ucLabel ID="lblCapacityPlanningOverview" runat="server" Text="Capacity Planning Overview"></cc1:ucLabel>
    </h2>
    <div class="right-shadow">
        <div class="formbox">
            <table width="100%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                <tr>
                    <td>
                        <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                            <ContentTemplate>
                                <table width="100%" cellspacing="5" cellpadding="0" border="0" >
                                    <tr>
                                        <td style="font-weight: bold; width: 10%">
                                            <cc1:ucLabel ID="lblSite" runat="server" Text="Site"></cc1:ucLabel>
                                        </td>
                                        <td style="font-weight: bold; width: 1%">
                                            <cc1:ucLabel ID="lblSiteCollon" runat="server">:</cc1:ucLabel>
                                        </td>
                                        <td style="font-weight: bold; width: 89%">
                                             <cc2:ucSite ID="ddlSite" runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: bold; ">
                                            <cc1:ucLabel ID="UcLineperhour_1" runat="server" Text="Line per hour "></cc1:ucLabel>
                                        </td>
                                        <td style="font-weight: bold;">
                                            <cc1:ucLabel ID="UcLabel2" runat="server">:</cc1:ucLabel>
                                        </td>
                                        <td style="font-weight: bold; ">
                                             <cc1:ucLabel ID="UcLinePerHour" runat="server"></cc1:ucLabel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: bold; ">
                                            <cc1:ucLabel ID="lblWeek" runat="server" Text="Week"></cc1:ucLabel>
                                        </td>
                                        <td style="font-weight: bold; ">
                                            <cc1:ucLabel ID="UcLabel3" runat="server">:</cc1:ucLabel>
                                        </td>
                                        <td style="font-weight: bold; ">
                                             <cc1:ucRadioButton ID="rdoCurrentWeek" runat="server" GroupName="View" Text="Current Week "
                                                                Checked="true"  />
                                                                &nbsp;
                                                                &nbsp;
                                            <cc1:ucRadioButton ID="rdoNextWeek" runat="server" GroupName="View" Text="Next Week"
                                                                 />
                                        </td>
                                    </tr>
                                </table>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        <div class="button-row">
                            <cc1:ucButton ID="UcButton1" runat="server" Text="Generate Report" CssClass="button"
                                OnClick="btnGenerateReport_Click" />
                        </div>
                    </td>
                </tr>
                <tr>
                    <td >
                        <rsweb:ReportViewer ID="CapacityPlanningOverviewReportViewer" runat="server"
                            Width="930px" DocumentMapCollapsed="True" Font-Names="Verdana" Font-Size="8pt"
                            InteractiveDeviceInfos="(Collection)" WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt"
                            Height="416px" >
                            <%--<LocalReport ReportPath="ModuleUI\Appointment\Reports\RDLC\CapacityPlanningOverview.rdlc">
                            <DataSources>
                                    <rsweb:ReportDataSource DataSourceId="SqlDataSource1" Name="dtRPTBOK_UnconfirmedUnusedSlot" />
                                </DataSources>
                            </LocalReport>--%>
                        </rsweb:ReportViewer>
                     <%--   <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:sConn %>"
                            SelectCommand="spRPTBOK_UnconfirmedUnusedSlot" SelectCommandType="StoredProcedure">
                            <SelectParameters>
                                <asp:ControlParameter ControlID="hdnCountryIDs" DefaultValue="-1" Name="SelectedCountryIDs"
                                    PropertyName="Value" Type="String" />
                                <asp:ControlParameter ControlID="hdnSiteIDs" DefaultValue="-1" Name="SelectedSiteIDs"
                                    PropertyName="Value" Type="String" />
                                <asp:ControlParameter ControlID="hdnVendorIDs" DefaultValue="-1" Name="SelectedVendorIDs"
                                    PropertyName="Value" Type="String" />
                                <asp:ControlParameter ControlID="hdnToDt" DefaultValue="" Name="DateTo" PropertyName="Value"
                                    Type="DateTime" />
                                <asp:ControlParameter ControlID="hdnFromDt" DefaultValue="" Name="DateFrom" PropertyName="Value"
                                    Type="DateTime" />
                            </SelectParameters>
                        </asp:SqlDataSource>--%>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>

﻿using System;
using System.Data;
using System.Web.UI;
using BusinessEntities.ModuleBE.Discrepancy.Report;
using BusinessLogicLayer.ModuleBAL.Discrepancy.Report;
using Utilities; using WebUtilities;
using Microsoft.Reporting.WebForms;
using System.Web.UI.WebControls;
using BusinessLogicLayer.ModuleBAL.Security;
using BusinessEntities.ModuleBE.Security;
using System.Collections.Generic;



public partial class CarrierVendorSearch : CommonPage
{
    protected void Page_Init(object sender, EventArgs e)
    {
        ucVendorTemplateSelect.CurrentPage = this;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            txtFromDate.Text = DateTime.Now.AddDays(-DateTime.Now.Day + 1).ToString("dd/MM/yyyy");
            txtToDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
            CarrierVendorContol();
            hdnJSFromDt.Value = txtFromDate.Text;
            hdnJSToDt.Value = txtToDate.Text;
        }
    }

    
    protected void rdoVendor_CheckedChanged(object sender, EventArgs e)
    {
        CarrierVendorContol();       
    }
    protected void rdoCarrier_CheckedChanged(object sender, EventArgs e)
    {
        CarrierVendorContol();
        
    }
    private void CarrierVendorContol()
    {
        bool EnableCarrierControl=false;
        bool EnableVendorControl = false;
        if (rdoCarrier.Checked)
        {
            EnableCarrierControl = rdoCarrier.Checked;
            EnableVendorControl = rdoVendor.Checked;
        }
        if (rdoVendor.Checked)
        {
            EnableCarrierControl = rdoCarrier.Checked;
            EnableVendorControl = rdoVendor.Checked;
        }
        msCarrier.Visible = EnableCarrierControl;
        lblCarrier.Visible = EnableCarrierControl;
        lblCarrierCollon.Visible = EnableCarrierControl;
        msVendor.Visible = EnableVendorControl;
        lblVendor.Visible = EnableVendorControl;
        lblVendorCollon.Visible = EnableVendorControl;  
    }
    //protected void CarrierVenSqlDataSource_Selecting(object sender, System.Web.UI.WebControls.SqlDataSourceSelectingEventArgs e)
    //{
    //    CarrierVenSqlDataSource.SelectParameters.Add("SelectedCountryIDs", msCountry.SelectedCountryIDs);
    //    CarrierVenSqlDataSource.SelectParameters.Add("SelectedSiteIDs", msSite.SelectedSiteIDs);
    //    if (msVendor.Visible == true)
    //        CarrierVenSqlDataSource.SelectParameters.Add("SelectedVendorIDs", msVendor.SelectedVendorIDs);
    //    if (msCarrier.Visible == true)
    //        CarrierVenSqlDataSource.SelectParameters.Add("SelectedCarrierIDs", msCarrier.SelectedCarrierIDs);
    //    CarrierVenSqlDataSource.SelectParameters.Add("DateTo", txtToDate.Text);
    //    CarrierVenSqlDataSource.SelectParameters.Add("DateFrom", txtFromDate.Text);
    //    if (rdoCarrier.Checked)
    //        CarrierVenSqlDataSource.SelectParameters.Add("SupplierType", "C");
    //    else if (rdoVendor.Checked)
    //        CarrierVenSqlDataSource.SelectParameters.Add("SupplierType", "V");
    //}

    /// <summary>
    /// Method to show Vendor on the basis of selected Template
    /// </summary>
    public override void TemplateSelectedIndexChanged()
    {
        base.TemplateSelectedIndexChanged();

        int VendorTemplateId = Convert.ToInt32(ucVendorTemplateSelect.innerControlddlTemplate.SelectedValue);

        ListBox lstSelectedVendor = (ListBox)msVendor.FindControl("lstRight");
        ListBox lstVendor = (ListBox)msVendor.FindControl("ucVendor").FindControl("lstLeft");

        if (VendorTemplateId > 0)
        {

            SCT_TemplateBAL oSCT_TemplateBAL = new SCT_TemplateBAL();
            SCT_TemplateBE oSCT_TemplateBE = new SCT_TemplateBE();

            oSCT_TemplateBE.VendorTemplateID = VendorTemplateId;
            oSCT_TemplateBE.Action = "GetVendorTemplateInfobyID";
            List<SCT_TemplateBE> lstVendorTemplate = oSCT_TemplateBAL.GetVendorTemplateById(oSCT_TemplateBE);

            if (lstVendorTemplate != null && lstVendorTemplate.Count > 0)
            {
                lstSelectedVendor.Items.Clear();
                msVendor.innerControlHiddenField.Value = string.Empty;
                msVendor.innerControlHiddenField2.Value = string.Empty;
                for (int iCount = 0; iCount < lstVendorTemplate.Count; iCount++)
                {
                    lstSelectedVendor.Items.Add(new ListItem(lstVendorTemplate[iCount].Vendor.Vendor_Name, lstVendorTemplate[iCount].Vendor.VendorID.ToString()));
                    msVendor.SelectedVendorIDs = lstVendorTemplate[iCount].Vendor.VendorID.ToString();
                    msVendor.SelectedVendorName = lstVendorTemplate[iCount].Vendor.Vendor_Name.ToString();
                }
            }
        }
        else
        {
            lstSelectedVendor.Items.Clear();
            msVendor.innerControlHiddenField.Value = string.Empty;
            msVendor.innerControlHiddenField2.Value = string.Empty;
        }
    }

    protected void btnGenerateReport_Click(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(msSite.SelectedSiteIDs))
            hdnSiteIDs.Value = msSite.SelectedSiteIDs;
        if (!string.IsNullOrEmpty(msCountry.SelectedCountryIDs))
            hdnCountryIDs.Value = msCountry.SelectedCountryIDs;
        if (msVendor.Visible == true && !string.IsNullOrEmpty(msVendor.SelectedVendorIDs))
            hdnVendorIDs.Value = msVendor.SelectedVendorIDs;
        if (msCarrier.Visible == true && !string.IsNullOrEmpty(msCarrier.SelectedCarrierIDs))
            hdnCarrierIDs.Value = msCarrier.SelectedCarrierIDs;
        if (rdoCarrier.Checked)
            hdnSupplierType.Value = "C";
        else if (rdoVendor.Checked)
            hdnSupplierType.Value = "V";
        if (!string.IsNullOrEmpty(txtFromDate.Text))
            hdnFromDt.Value = Utilities.Common.TextToDateFormat(hdnJSFromDt.Value).Year + "-" + Utilities.Common.TextToDateFormat(hdnJSFromDt.Value).Month + "-" + Utilities.Common.TextToDateFormat(hdnJSFromDt.Value).Day;
        if (!string.IsNullOrEmpty(txtToDate.Text))
            hdnToDt.Value = Utilities.Common.TextToDateFormat(hdnJSToDt.Value).Year + "-" + Utilities.Common.TextToDateFormat(hdnJSToDt.Value).Month + "-" + Utilities.Common.TextToDateFormat(hdnJSToDt.Value).Day;

        //if (!string.IsNullOrEmpty(msCountry.SelectedCountryIDs))
        //    CarrierVenSqlDataSource.SelectParameters.Add("SelectedCountryIDs", msCountry.SelectedCountryIDs);
        //CarrierVenSqlDataSource.SelectParameters["SelectedCountryIDs"].DefaultValue = "-1";
        //if (!string.IsNullOrEmpty(msSite.SelectedSiteIDs))
        //    CarrierVenSqlDataSource.SelectParameters.Add("SelectedSiteIDs", msSite.SelectedSiteIDs);
        //CarrierVenSqlDataSource.SelectParameters["SelectedSiteIDs"].DefaultValue = "-1";  
        
        //if (msVendor.Visible == true && !string.IsNullOrEmpty(msVendor.SelectedVendorIDs))
        //     CarrierVenSqlDataSource.SelectParameters.Add("SelectedVendorIDs", msVendor.SelectedVendorIDs);
        //if (msCarrier.Visible == true && !string.IsNullOrEmpty(msCarrier.SelectedCarrierIDs))
        //     CarrierVenSqlDataSource.SelectParameters.Add("SelectedCarrierIDs", msCarrier.SelectedCarrierIDs);

        //CarrierVenSqlDataSource.SelectParameters["SelectedCarrierIDs"].DefaultValue = "-1";
        //CarrierVenSqlDataSource.SelectParameters["SelectedVendorIDs"].DefaultValue = "-1";
        //if (!string.IsNullOrEmpty(txtFromDate.Text))
        //{
        //    hdnFromDt.Value = Utilities.Common.TextToDateFormat(hdnJSFromDt.Value).Year + "-" + Utilities.Common.TextToDateFormat(hdnJSFromDt.Value).Month + "-" + Utilities.Common.TextToDateFormat(hdnJSFromDt.Value).Day;
        //    CarrierVenSqlDataSource.SelectParameters.Add("DateFrom", hdnFromDt.Value);
        //}
        //if (!string.IsNullOrEmpty(txtToDate.Text))
        //{
        //    hdnToDt.Value = Utilities.Common.TextToDateFormat(hdnJSToDt.Value).Year + "-" + Utilities.Common.TextToDateFormat(hdnJSToDt.Value).Month + "-" + Utilities.Common.TextToDateFormat(hdnJSToDt.Value).Day;
        //    CarrierVenSqlDataSource.SelectParameters.Add("DateTo", hdnToDt.Value);
        //}
        
        //CarrierVenSqlDataSource.SelectParameters["DateTo"].DefaultValue = "";
        //CarrierVenSqlDataSource.SelectParameters["DateFrom"].DefaultValue = "";
        //if (rdoCarrier.Checked)
        //    CarrierVenSqlDataSource.SelectParameters.Add("SupplierType", "C");           
        //else if (rdoVendor.Checked)
        //    CarrierVenSqlDataSource.SelectParameters.Add("SupplierType", "V");
        //CarrierVenSqlDataSource.SelectParameters["SupplierType"].DefaultValue = "-1";
        

        

            ReportParameter[] reportParameter = new ReportParameter[6];
            reportParameter[0] = new ReportParameter("Country", msCountry.SelectedCountryName);
            reportParameter[1] = new ReportParameter("Site", msSite.SelectedSiteName);
            reportParameter[2] = new ReportParameter("Vendor", msVendor.SelectedVendorName);
            reportParameter[3] = new ReportParameter("DateFrom", hdnJSFromDt.Value);
            reportParameter[4] = new ReportParameter("DateTo", hdnJSToDt.Value);
            reportParameter[5] = new ReportParameter("Carrier", msCarrier.SelectedCarrierName);
            CarrierVendorReportViewer.LocalReport.SetParameters(reportParameter);
        
            UcCarrierVendorPanel.Visible = false;
            UcCarrierVendorReportViewPanel.Visible = true;
           
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        EncryptQueryString("CarrierReportSearch.aspx");
    }
}
﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.StockOverview;
using BusinessLogicLayer.ModuleBAL.StockOverview;
using BusinessLogicLayer.ModuleBAL.Security;
using BusinessEntities.ModuleBE.Security;
using WebUtilities;
using Utilities;
using BaseControlLibrary;
using BusinessEntities.ModuleBE.Upload;
using BusinessLogicLayer.ModuleBAL.Upload;
using System.Data;

public partial class ModuleUI_Appointment_Reports_SentPOReport : CommonPage
{
    protected void Page_InIt(object sender, EventArgs e)
    {
        ucVendorTemplateSelect.CurrentPage = this;
        msSite.isMoveAllRequired = true;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            pager1.PageSize = 200;
            pager1.GenerateGoToSection = false;
            pager1.GeneratePagerInfoSection = false;
            pager1.Visible = false;  

            txtDateFrom.Text = DateTime.Now.AddDays(-DateTime.Now.Day + 1).ToString("dd/MM/yyyy");
            txtDateTo.Text = DateTime.Now.ToString("dd/MM/yyyy");
            txtDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
            hdnJSFromDt.Value = txtDateFrom.Text;
            hdnJSToDt.Value = txtDateTo.Text;
            hdnJSDt.Value = txtDate.Text;
        }

        if (IsPostBack)
        {
            txtDateFrom.Text = hdnJSFromDt.Value;
            txtDateTo.Text = hdnJSToDt.Value;
            txtDate.Text = hdnJSDt.Value;
        }

        UIUtility.PageValidateForODUser();
       

        if (!IsPostBack)
        {
            pnlSearchScreen.Visible = true;
            pnlPotentialOutput.Visible = false;
            btnExportToExcel.Visible = false;
        }
    }

    protected void btnGenerateReport_Click(object sender, EventArgs e)
    {
        btnGenerateReport.Visible = false;
        btnBackSearch.Visible = true;

        BindGrid();

        pnlSearchScreen.Visible = false;
        pnlPotentialOutput.Visible = true;
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        pnlSearchScreen.Visible = true;
        pnlPotentialOutput.Visible = false;
        btnExportToExcel.Visible = false;

        btnGenerateReport.Visible = true;
        btnBackSearch.Visible = false;
    }

    public override void TemplateSelectedIndexChanged()
    {
        base.TemplateSelectedIndexChanged();

        int VendorTemplateId = Convert.ToInt32(ucVendorTemplateSelect.innerControlddlTemplate.SelectedValue);

        ListBox lstSelectedVendor = (ListBox)msVendor.FindControl("lstRight");
        ListBox lstVendor = (ListBox)msVendor.FindControl("ucVendor").FindControl("lstLeft");

        if (VendorTemplateId > 0)
        {

            SCT_TemplateBAL oSCT_TemplateBAL = new SCT_TemplateBAL();
            SCT_TemplateBE oSCT_TemplateBE = new SCT_TemplateBE();

            oSCT_TemplateBE.VendorTemplateID = VendorTemplateId;
            oSCT_TemplateBE.Action = "GetVendorTemplateInfobyID";
            List<SCT_TemplateBE> lstVendorTemplate = oSCT_TemplateBAL.GetVendorTemplateById(oSCT_TemplateBE);

            if (lstVendorTemplate != null && lstVendorTemplate.Count > 0)
            {
                lstSelectedVendor.Items.Clear();
                msVendor.innerControlHiddenField.Value = string.Empty;
                msVendor.innerControlHiddenField2.Value = string.Empty;
                for (int iCount = 0; iCount < lstVendorTemplate.Count; iCount++)
                {
                    lstSelectedVendor.Items.Add(new ListItem(lstVendorTemplate[iCount].Vendor.Vendor_Name, lstVendorTemplate[iCount].Vendor.VendorID.ToString()));
                    msVendor.SelectedVendorIDs = lstVendorTemplate[iCount].Vendor.VendorID.ToString();
                    msVendor.SelectedVendorName = lstVendorTemplate[iCount].Vendor.Vendor_Name.ToString();
                }
            }
        }
        else
        {
            lstSelectedVendor.Items.Clear();
            msVendor.innerControlHiddenField.Value = string.Empty;
            msVendor.innerControlHiddenField2.Value = string.Empty;
        }
    }

    protected void gvSentPoRequest_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        if (Cache["lstGridResult"] != null)
        {
            List<Up_PurchaseOrderDetailBE> lsOpenPO = Cache["lstGridResult"] as List<Up_PurchaseOrderDetailBE>;
            gvSentPoRequest.PageIndex = e.NewPageIndex;
            gvSentPoRequest.DataSource = lsOpenPO;
            gvSentPoRequest.DataBind();
        }

    }

    public override BusinessEntities.BaseBe[] SubSort(GridViewSortEventArgs e)
    {
        return Utilities.GenericListHelper<Up_PurchaseOrderDetailBE>.SortList((List<Up_PurchaseOrderDetailBE>)Cache["lstGridResult"], e.SortExpression, e.SortDirection).ToArray();
    }

    private void BindGrid(int page=1)
    {
        List<Up_PurchaseOrderDetailBE> lsOpenPO = null;
        UP_PurchaseOrderDetailBAL objPOBal = new UP_PurchaseOrderDetailBAL();
        Up_PurchaseOrderDetailBE oNewPOBE = new Up_PurchaseOrderDetailBE();

        if (!string.IsNullOrEmpty(msPO.SelectedPO))
            oNewPOBE.SelectedPONos = msPO.SelectedPO;

        if (!string.IsNullOrEmpty(msSite.SelectedSiteIDs))
            oNewPOBE.SelectedSiteIDs = msSite.SelectedSiteIDs;

        if (!string.IsNullOrEmpty(msStockPlanner.SelectedStockPlannerIDs))
            oNewPOBE.SelectedStockPlannerIDs = msStockPlanner.SelectedStockPlannerIDs;

        if (!string.IsNullOrEmpty(msVendor.SelectedVendorIDs))
            oNewPOBE.SelectedVendorIDs = msVendor.SelectedVendorIDs;

        if (rbAll.Checked)
        {
            oNewPOBE.DisplayOption = "ALL";
            oNewPOBE.SelectedDate = null;
        }
        else if (rbDue.Checked)
        {
            oNewPOBE.DisplayOption = "DUE";
            oNewPOBE.DueBy = DateTime.Now;
        }
        else if (rbDueBy.Checked)
        {
            oNewPOBE.DisplayOption = "DUEBY";
            oNewPOBE.DueBy = Utilities.Common.GetMM_DD_YYYY(hdnJSDt.Value);
        }
        else if (rbRaisedBetween.Checked)
        {
            oNewPOBE.DisplayOption = "RB";
            oNewPOBE.FromDate = Utilities.Common.GetMM_DD_YYYY(hdnJSFromDt.Value);
            oNewPOBE.ToDate = Utilities.Common.GetMM_DD_YYYY(hdnJSToDt.Value);
        }
        oNewPOBE.PageCount = page;
        oNewPOBE.Action = "SentPurchaseOrderReportData";
        gvSentPoRequest.HeaderStyle.Wrap = false;
        gvSentPoRequest.HeaderStyle.HorizontalAlign = HorizontalAlign.Justify;
        lsOpenPO = objPOBal.SentPurchaseOrderReportDataBAL(oNewPOBE);
        gvSentPoRequest.DataSource = lsOpenPO;
        gvSentPoRequest.DataBind();
        ViewState["lstUp_PurchaseOrderDetailBE"] = oNewPOBE;
        if (lsOpenPO.Count > 0)
        {
            btnExportToExcel.Visible = true;
            gvSentPoRequest.PageIndex = 0;
            pager1.ItemCount = lsOpenPO[0].RecordCount;
            pager1.Visible = true;
        }
        else
        {
            btnExportToExcel.Visible = false;
        }

        //for export to excel
        //tempGridView.DataSource = lsOpenPO;
        //tempGridView.DataBind();

       // Cache["lstGridResult"] = lsOpenPO;
        //mvDiscrepancyList.ActiveViewIndex = 1;
        btnBackSearch.Visible = true;
        btnGenerateReport.Visible = false;
        rbAll.Checked = true;
    }
    public void pager_Command(object sender, CommandEventArgs e)
    {
        int currnetPageIndx = Convert.ToInt32(e.CommandArgument);
        pager1.CurrentIndex = currnetPageIndx;
        BindGrid(currnetPageIndx);
    }
    protected void btnExport_Click(object sender, EventArgs e)
    {
        UP_PurchaseOrderDetailBAL objPOBal = new UP_PurchaseOrderDetailBAL();
        Up_PurchaseOrderDetailBE oNewPOBE = new Up_PurchaseOrderDetailBE();
        DataTable dt = new DataTable();
        oNewPOBE = (Up_PurchaseOrderDetailBE)ViewState["lstUp_PurchaseOrderDetailBE"];
        oNewPOBE.PageCount = 0;
        dt = objPOBal.SentPurchaseOrderExportReportDataBAL(oNewPOBE);
        if (dt.Rows.Count > 0)
        {
            HttpContext context = HttpContext.Current;
            //**************

            //****************
            string date = DateTime.Now.ToString("ddMMyyyy");
            string time = DateTime.Now.ToString("HH:mm");
            context.Response.ContentType = "text/vnd.ms-excel";
            context.Response.AddHeader("Content-Disposition", string.Format("attachment; filename={0}_{1}_{2}", "SentPOReports", date, time + ".xls"));


            //context.Response.ContentType = "text/csv";
            //context.Response.AddHeader("Content-Disposition", "attachment; filename=abc.csv");

            for (int i = 0; i < dt.Columns.Count; i++)
            {
                context.Response.Write(dt.Columns[i].ColumnName);
                context.Response.Write("\t");
            }


            context.Response.Write(Environment.NewLine);

            //Write data
            foreach (DataRow row in dt.Rows)
            {

                for (int i = 0; i < dt.Columns.Count; i++)
                {
                    context.Response.Write(row.ItemArray[i].ToString());
                    context.Response.Write("\t");
                }
                context.Response.Write(Environment.NewLine);
            }
            context.Response.End();
        }

    }
}
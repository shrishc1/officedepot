﻿using BaseControlLibrary;
using BusinessEntities.ModuleBE.Appointment.Booking;
using BusinessEntities.ModuleBE.Discrepancy.LogDiscrepancy;
using BusinessEntities.ModuleBE.Upload;
using BusinessLogicLayer.ModuleBAL.Upload;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Utilities;
using WebUtilities;

public partial class PurchaseOrderInquiry : CommonPage
{
    private Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE = null;
    private UP_PurchaseOrderDetailBAL oUP_PurchaseOrderDetailBAL = null;
    private APPBOK_BookingBE oAPPBOK_BookingBE = null;
    private DiscrepancyBE oDiscrepancyBE = null;

    public static string MailHTML { get; set; }

    protected void Page_InIt(object sender, EventArgs e)
    {
        ddlSite.CurrentPage = this;
        ddlSite.IsAllRequired = true;

        ddlSite.SchedulingContact = true;
        ucSeacrhVendor1.CurrentPage = this;
        ucSeacrhVendor1.IsParentRequired = true;
        ucSeacrhVendor1.IsStandAloneRequired = true;
        ucSeacrhVendor1.IsChildRequired = true;
        ucSeacrhVendor1.IsGrandParentRequired = true;
    }

    public override void SiteSelectedIndexChanged()
    {
        base.SiteSelectedIndexChanged();
        ucTextbox txtVendorNo = (ucTextbox)ucSeacrhVendor1.FindControl("txtVendorNo");
        if (ddlSite.innerControlddlSite.SelectedIndex == 0)
        {
            txtVendorNo.Enabled = false;
            ucSeacrhVendor1.SiteID = 0;
            ucSeacrhVendor1.ClearSearch();
            hdnBaseSiteId.Value = "0";
        }
        else
        {
            txtVendorNo.Enabled = true;
            ucSeacrhVendor1.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
            ucSeacrhVendor1.ClearSearch();
            hdnBaseSiteId.Value = ddlSite.innerControlddlSite.SelectedItem.Value;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        string poNo = null;

        string status = null;
        int vendorId = 0;
        int siteId = 0;
        string OrderRaisedDateDDMMYYYY = null;

        if (!IsPostBack)
        {
            Pager1.PageSize = 200;
            Pager1.GenerateGoToSection = false;
            Pager1.GeneratePagerInfoSection = false;
        }

        if (GetQueryStringValue("Page") != null)
        {
            if (GetQueryStringValue("Purchase_order") != null)
            {
                poNo = Convert.ToString(GetQueryStringValue("Purchase_order"));
            }
            if (GetQueryStringValue("VendorId") != null)
            {
                vendorId = Convert.ToInt32(GetQueryStringValue("VendorId"));
            }
            if (GetQueryStringValue("SiteID") != null)
            {
                siteId = Convert.ToInt32(GetQueryStringValue("SiteID"));
            }
            if (GetQueryStringValue("Order_raised") != null)
            {
                OrderRaisedDateDDMMYYYY = Convert.ToString(GetQueryStringValue("Order_raised"));
            }
            if (GetQueryStringValue("Status") != null)
            {
                status = Convert.ToString(GetQueryStringValue("Status"));
            }

            ShowPOInquiryDetailSections(poNo, status, vendorId, siteId, OrderRaisedDateDDMMYYYY);
        }

        txtPurchaseOrderNo.Focus();


    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (ddlStatus.Items.Count > 0)
                ddlStatus.SelectedIndex = 1;

            ddlSite.innerControlddlSite.AutoPostBack = true;

            if (GetQueryStringValue("PN") != null)
                BindBasedOnReturnedData();

            if (GetQueryStringValue("Page") == null)
            {
                pnlPurchaseOrderInqSearch.Visible = true;
                pnlPurchaseOrderInqDetail.Visible = false;
            }
        }
    }

    private void BindBasedOnReturnedData()
    {
        if (GetQueryStringValue("PN") != null && !string.IsNullOrWhiteSpace(GetQueryStringValue("PN")))
        {
            if (GetQueryStringValue("Status") != null && !string.IsNullOrWhiteSpace(GetQueryStringValue("Status")))
                ddlStatus.SelectedIndex = ddlStatus.Items.IndexOf(ddlStatus.Items.FindByText(GetQueryStringValue("Status")));

            if (GetQueryStringValue("PO") != null)
                txtPurchaseOrderNo.Text = GetQueryStringValue("PO");

            #region Logic to setting coming Site Id from Booking History and Discrepancy page

            string strSiteId = string.Empty;
            if (GetQueryStringValue("PN").Trim().ToUpper().Equals("BOOKHIST"))
            {
                if (GetQueryStringValue("ID") != null && !string.IsNullOrWhiteSpace(GetQueryStringValue("ID")))
                {
                    strSiteId = GetQueryStringValue("ID");
                    int index = strSiteId.LastIndexOf("-");
                    if (index > 0)
                    {
                        strSiteId = strSiteId.Remove(0, index + 1);
                        ddlSite.innerControlddlSite.SelectedIndex = ddlSite.innerControlddlSite.Items.IndexOf(
                                ddlSite.innerControlddlSite.Items.FindByValue(strSiteId));
                    }
                }
                else
                    if (ddlSite.innerControlddlSite.Items.Count > 0)
                    ddlSite.innerControlddlSite.SelectedIndex = 0;
            }
            else if (GetQueryStringValue("PN").Trim().ToUpper().Equals("DISLOG"))
            {
                if (GetQueryStringValue("SId") != null && !string.IsNullOrWhiteSpace(GetQueryStringValue("SId")))
                    ddlSite.innerControlddlSite.SelectedIndex = ddlSite.innerControlddlSite.Items.IndexOf(
                            ddlSite.innerControlddlSite.Items.FindByValue(GetQueryStringValue("SId")));
                else
                    if (ddlSite.innerControlddlSite.Items.Count > 0)
                    ddlSite.innerControlddlSite.SelectedIndex = 0;
            }

            #endregion Logic to setting coming Site Id from Booking History and Discrepancy page

            #region Search Inquiry

            int vendorId = 0;
            if (!string.IsNullOrWhiteSpace(ucSeacrhVendor1.VendorNo))
                vendorId = Convert.ToInt32(ucSeacrhVendor1.VendorNo);

            int siteId = 0;
            if (!string.IsNullOrWhiteSpace(ddlSite.innerControlddlSite.SelectedValue))
                siteId = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedValue);

            this.SearchPOInquiry(txtPurchaseOrderNo.Text, string.Empty, ddlStatus.SelectedItem.Text, vendorId, siteId);

            #endregion Search Inquiry
        }
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        #region Check search status ...

        var chkToSearch = false;
        if (!string.IsNullOrEmpty(txtPurchaseOrderNo.Text) && !string.IsNullOrWhiteSpace(txtPurchaseOrderNo.Text))
            chkToSearch = true;

        if (!string.IsNullOrEmpty(ucSeacrhVendor1.VendorNo) && !string.IsNullOrWhiteSpace(ucSeacrhVendor1.VendorNo))
            chkToSearch = true;

        if (!string.IsNullOrEmpty(txtSku.Text) && !string.IsNullOrWhiteSpace(txtSku.Text))
            chkToSearch = true;

        if (ddlSite.innerControlddlSite.SelectedIndex > 0)
            chkToSearch = true;

        #endregion Check search status ...

        if (chkToSearch)
        {
            int vendorId = 0;
            if (!string.IsNullOrWhiteSpace(ucSeacrhVendor1.VendorNo))
                vendorId = Convert.ToInt32(ucSeacrhVendor1.VendorNo);

            int siteId = 0;
            if (!string.IsNullOrWhiteSpace(ddlSite.innerControlddlSite.SelectedValue))
                siteId = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedValue);

            this.SearchPOInquiry(txtPurchaseOrderNo.Text, txtSku.Text, ddlStatus.SelectedItem.Text, vendorId, siteId, txtCATCode.Text, txtDateFrom.innerControltxtDate.Value != "" ? Common.GetYYYY_MM_DD(txtDateFrom.innerControltxtDate.Value) : "", txtDateTo.innerControltxtDate.Value != "" ? Common.GetYYYY_MM_DD(txtDateTo.innerControltxtDate.Value) : "", ddlReceipt.SelectedItem.Text);
        }
        else
        {
            var purchaseOrderInquirySearchMessage = WebCommon.getGlobalResourceValue("PurchaseOrderInquirySearchMessage");
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + purchaseOrderInquirySearchMessage + "');", true);
            return;
        }
    }

    private void SearchPOInquiry(string poNo, string sku, string status, int vendorId, int siteId, string viking = null, string datefrom = null, string dateto = null, string ReceiptStatus = null, int page = 1)
    {
        oUP_PurchaseOrderDetailBAL = new UP_PurchaseOrderDetailBAL();
        Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE = new Up_PurchaseOrderDetailBE();
        oUp_PurchaseOrderDetailBE.Action = "GetPOEnquiryData";
        oUp_PurchaseOrderDetailBE.Purchase_order = poNo;
        oUp_PurchaseOrderDetailBE.SKU = new UP_SKUBE();
        oUp_PurchaseOrderDetailBE.SKU.Direct_SKU = sku;

        oUp_PurchaseOrderDetailBE.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();
        if (!string.IsNullOrWhiteSpace(ucSeacrhVendor1.VendorNo))
            oUp_PurchaseOrderDetailBE.Vendor.VendorID = vendorId;

        oUp_PurchaseOrderDetailBE.Site = new BusinessEntities.ModuleBE.AdminFunctions.MAS_SiteBE();
        if (!string.IsNullOrWhiteSpace(ddlSite.innerControlddlSite.SelectedValue))
            oUp_PurchaseOrderDetailBE.Site.SiteID = siteId;

        oUp_PurchaseOrderDetailBE.Status = status;

        oUp_PurchaseOrderDetailBE.VikingSku = viking;
        oUp_PurchaseOrderDetailBE.OrderRaisedToDate = dateto;
        oUp_PurchaseOrderDetailBE.OrderRaisedFromDate = datefrom;
        oUp_PurchaseOrderDetailBE.ReceiptStatus = ReceiptStatus;
        oUp_PurchaseOrderDetailBE.Page = page;

        List<Up_PurchaseOrderDetailBE> lstUp_PurchaseOrderDetailBE = oUP_PurchaseOrderDetailBAL.GetPurchaseOrderInquiryBAL(oUp_PurchaseOrderDetailBE);
        if (lstUp_PurchaseOrderDetailBE.Count > 0)
        {
            ViewState["POGridDate"] = lstUp_PurchaseOrderDetailBE;
            gvPurchaseOrderInquiry.DataSource = lstUp_PurchaseOrderDetailBE;
            gvPurchaseOrderInquiry.DataBind();
            lblNoRecordsFound.Visible = false;

            gvPurchaseOrderInquiry.PageIndex = page;
            Pager1.PageSize = 200;
            Pager1.CurrentIndex = page;
            Pager1.ItemCount = lstUp_PurchaseOrderDetailBE.Count > 0 ? lstUp_PurchaseOrderDetailBE[0].TotalRecords : 0;
            Pager1.Visible = true;
        }
        else
        {
            gvPurchaseOrderInquiry.DataSource = null;
            gvPurchaseOrderInquiry.DataBind();
            ViewState["POGridDate"] = null;
            lblNoRecordsFound.Visible = true;
            Pager1.Visible = false;
        }
    }

    public void pager_Command(object sender, CommandEventArgs e)
    {
        int currnetPageIndx = Convert.ToInt32(e.CommandArgument);
        Pager1.CurrentIndex = currnetPageIndx;
        int vendorId = !string.IsNullOrWhiteSpace(ucSeacrhVendor1.VendorNo) ? Convert.ToInt32(ucSeacrhVendor1.VendorNo) : 0;
        int siteId = !string.IsNullOrWhiteSpace(ddlSite.innerControlddlSite.SelectedValue) ? Convert.ToInt32(ddlSite.innerControlddlSite.SelectedValue) : 0;
        this.SearchPOInquiry(txtPurchaseOrderNo.Text, txtSku.Text, ddlStatus.SelectedItem.Text, vendorId, siteId,
            txtCATCode.Text, txtDateFrom.innerControltxtDate.Value != "" ? Common.GetYYYY_MM_DD(txtDateFrom.innerControltxtDate.Value) : "",
            txtDateTo.innerControltxtDate.Value != "" ? Common.GetYYYY_MM_DD(txtDateTo.innerControltxtDate.Value) : "",
            ddlReceipt.SelectedItem.Text, currnetPageIndx);
    }

    protected void gvPurchaseOrderInquiry_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvPurchaseOrderInquiry.PageIndex = e.NewPageIndex;
        if (ViewState["POGridDate"] != null)
        {
            gvPurchaseOrderInquiry.DataSource = ViewState["POGridDate"];
            gvPurchaseOrderInquiry.DataBind();
            lblNoRecordsFound.Visible = false;
        }
    }

    protected void gvPurchaseOrderInquiry_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "SendOnPOHistory")
        {
            oUP_PurchaseOrderDetailBAL = new UP_PurchaseOrderDetailBAL();

            GridViewRow oGridViewRow = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
            int intRowIndex = oGridViewRow.RowIndex;

            string strPONo = Convert.ToString(e.CommandArgument);
            HiddenField hdnVendorId = (HiddenField)gvPurchaseOrderInquiry.Rows[intRowIndex].FindControl("hdnVendorId");
            HiddenField hdnSiteId = (HiddenField)gvPurchaseOrderInquiry.Rows[intRowIndex].FindControl("hdnSiteId");
            ucLabel lblStatusPOInq = (ucLabel)gvPurchaseOrderInquiry.Rows[intRowIndex].FindControl("lblStatusPOInq");
            ucLabel lblOrderRaisedDatePOInq = (ucLabel)gvPurchaseOrderInquiry.Rows[intRowIndex].FindControl("lblOrderRaisedDatePOInq");

            if (!string.IsNullOrWhiteSpace(strPONo) && !string.IsNullOrWhiteSpace(hdnVendorId.Value) && !string.IsNullOrWhiteSpace(hdnSiteId.Value) && !string.IsNullOrWhiteSpace(lblOrderRaisedDatePOInq.Text))
                ShowPOInquiryDetailSections(strPONo, lblStatusPOInq.Text, Convert.ToInt32(hdnVendorId.Value),
                    Convert.ToInt32(hdnSiteId.Value), lblOrderRaisedDatePOInq.Text);
        }
    }

    private void ShowPOInquiryDetailSections(string poNo, string status, int vendorId, int siteId, string OrderRaisedDateDDMMYYYY)
    {
        try
        {
            #region PO Inquiry Main Action

            oUp_PurchaseOrderDetailBE = new Up_PurchaseOrderDetailBE();
            oUP_PurchaseOrderDetailBAL = new UP_PurchaseOrderDetailBAL();
            oUp_PurchaseOrderDetailBE.Action = "GetPOEnquiryMain";

            if (!string.IsNullOrWhiteSpace(poNo))
                oUp_PurchaseOrderDetailBE.Purchase_order = poNo;

            oUp_PurchaseOrderDetailBE.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();

            if (vendorId != 0)
                oUp_PurchaseOrderDetailBE.Vendor.VendorID = vendorId;

            oUp_PurchaseOrderDetailBE.Site = new BusinessEntities.ModuleBE.AdminFunctions.MAS_SiteBE();
            if (siteId != 0)
                oUp_PurchaseOrderDetailBE.Site.SiteID = siteId;

            if (!string.IsNullOrWhiteSpace(status))
                oUp_PurchaseOrderDetailBE.Status = status;

            if (!string.IsNullOrWhiteSpace(OrderRaisedDateDDMMYYYY))
                oUp_PurchaseOrderDetailBE.Order_raised = Common.GetMM_DD_YYYY(OrderRaisedDateDDMMYYYY.Replace('-', '/'));

            #endregion PO Inquiry Main Action

            var lstPOInquiryMain = oUP_PurchaseOrderDetailBAL.GetPurchaseOrderInquiryMainBAL(oUp_PurchaseOrderDetailBE);
            ViewState["POInquiryMain"] = lstPOInquiryMain != null ? lstPOInquiryMain : null;
            ViewState["SiteId"] = siteId;
            ViewState["VendorId"] = vendorId;
            ViewState["OrderRaisedDateDDMMYYYY"] = !string.IsNullOrEmpty(OrderRaisedDateDDMMYYYY) ? OrderRaisedDateDDMMYYYY.Replace('-', '/') : string.Empty;
          
            if (lstPOInquiryMain != null && lstPOInquiryMain.Count > 0)
            {
                lblPurchaseOrderV.Text = lstPOInquiryMain[0].Purchase_order;
                lblVendorV.Text = lstPOInquiryMain[0].Vendor.VendorName;
                lblSiteV.Text = lstPOInquiryMain[0].Site.SiteName;
                lblStatusV.Text = lstPOInquiryMain[0].Status;

                if (lstPOInquiryMain[0].Original_due_date != null)
                {
                    var originalDueDate = Convert.ToDateTime(lstPOInquiryMain[0].Original_due_date);
                    lblOriginalDueDateV.Text = originalDueDate.ToString("dd/MM/yyyy");
                }
                lblBuyerV.Text = lstPOInquiryMain[0].StockPlannerName;

                var orderRaised = DateTime.Now.Date;
                if (lstPOInquiryMain[0].Order_raised != null)
                {
                    orderRaised = Convert.ToDateTime(lstPOInquiryMain[0].Order_raised);
                    lblOrderCreatedV.Text = orderRaised.ToString("dd/MM/yyyy");
                }

                btnSendMail.Attributes.Add("onclick", "javascript:return window.open('" + EncryptQuery("PurchaseOrderCommunication.aspx?POID=" + poNo + "&VendorID=" + vendorId.ToString() + "&OrderRaised=" + lblOrderCreatedV.Text) + "','new','scrollbars=1,left=100px,top=20px,width=800px,height=600px,toolbar=0,scrollbars=1,status=0');return false;");

                #region PO Inquiry Details Action

                oUp_PurchaseOrderDetailBE.Action = "GetPOEnquiryDetails";
                oUp_PurchaseOrderDetailBE.Status = lblStatusV.Text;

                var lstPOInquiryDetails = oUP_PurchaseOrderDetailBAL.GetPurchaseOrderInquiryDetailsBAL(oUp_PurchaseOrderDetailBE);
                gvPODetails.DataSource = lstPOInquiryDetails != null ? lstPOInquiryDetails : null;
                gvPODetails.DataBind();

                #endregion PO Inquiry Details Action

                #region PO Inquiry Booking Details Action

                oAPPBOK_BookingBE = new APPBOK_BookingBE();
                oAPPBOK_BookingBE.Action = "GetPOEnquiryBookingDetails";
                if (!string.IsNullOrWhiteSpace(poNo))
                    oAPPBOK_BookingBE.PurchaseOrders = poNo;

                if (vendorId != 0)
                    oAPPBOK_BookingBE.VendorID = vendorId;

                if (siteId != 0)
                    oAPPBOK_BookingBE.SiteId = siteId;

                oAPPBOK_BookingBE.PurchaseOrder = new Up_PurchaseOrderDetailBE();
                oAPPBOK_BookingBE.PurchaseOrder.Order_raised = Common.GetMM_DD_YYYY(OrderRaisedDateDDMMYYYY.Replace('-', '/'));

                var lstPOInquiryBookingDetails = oUP_PurchaseOrderDetailBAL.GetPurchaseOrderInquiryBookingDetailsBAL(oAPPBOK_BookingBE).OrderByDescending(book => book.ScheduleDate);
                gvBookingDetails.DataSource = lstPOInquiryBookingDetails != null ? lstPOInquiryBookingDetails : null;
                gvBookingDetails.DataBind();

                #endregion PO Inquiry Booking Details Action

                #region PO Inquiry Discrepancies Action

                oDiscrepancyBE = new DiscrepancyBE();
                oDiscrepancyBE.Action = "GetPOEnquiryDiscrepancies";
                oDiscrepancyBE.PurchaseOrder = new Up_PurchaseOrderDetailBE();
                if (!string.IsNullOrWhiteSpace(poNo))
                    oDiscrepancyBE.PurchaseOrder.Purchase_order = poNo;

                if (vendorId != 0)
                    oDiscrepancyBE.VendorID = vendorId;

                if (siteId != 0)
                    oDiscrepancyBE.POSiteID = siteId;

                oDiscrepancyBE.PurchaseOrder.Order_raised = Common.GetMM_DD_YYYY(OrderRaisedDateDDMMYYYY.Replace('-', '/'));

                var lstPOInquiryDiscrepancies = oUP_PurchaseOrderDetailBAL.GetPurchaseOrderInquiryDiscrepanciesBAL(oDiscrepancyBE);
                gvDiscrepancies.DataSource = lstPOInquiryDiscrepancies != null ? lstPOInquiryDiscrepancies : null;
                gvDiscrepancies.DataBind();

                #endregion PO Inquiry Discrepancies Action

                #region PO Inquiry Receipt Action

                var purchaseOrderDetailBAL = new UP_PurchaseOrderDetailBAL();
                var purchaseOrderDetail = new Up_PurchaseOrderDetailBE();
                purchaseOrderDetail.Action = "GetPOInquiryReceiptVisibilityCount";
                purchaseOrderDetail.Purchase_order = lblPurchaseOrderV.Text;
                purchaseOrderDetail.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();
                purchaseOrderDetail.Vendor.VendorID = vendorId;
                purchaseOrderDetail.Order_raised = orderRaised;
                purchaseOrderDetail.Site = new BusinessEntities.ModuleBE.AdminFunctions.MAS_SiteBE();
                purchaseOrderDetail.Site.SiteID = siteId;
                if (!string.IsNullOrWhiteSpace(OrderRaisedDateDDMMYYYY))
                    purchaseOrderDetail.Order_raised = Common.GetMM_DD_YYYY(OrderRaisedDateDDMMYYYY.Replace('-', '/'));

                var count = purchaseOrderDetailBAL.GetPOInquiryReceiptVisibilityCountBAL(purchaseOrderDetail);
                lnkClickToView.Enabled = count != null && count > 0;

                #endregion PO Inquiry Receipt Action

                //-----------Phase 18 R2 Point 7------------------

                #region PO Inquiry Scheduled PO Reconciliation

                purchaseOrderDetail = new Up_PurchaseOrderDetailBE();
                purchaseOrderDetail.Action = "GetPOReconciliationVisibilityCount";
                purchaseOrderDetail.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();
                purchaseOrderDetail.Site = new BusinessEntities.ModuleBE.AdminFunctions.MAS_SiteBE();
                purchaseOrderDetail.Site.SiteID = siteId;
                purchaseOrderDetail.Vendor.VendorID = vendorId;
                purchaseOrderDetail.Purchase_order = lblPurchaseOrderV.Text;
                count = purchaseOrderDetailBAL.GetPOInquiryReceiptVisibilityCountBAL(purchaseOrderDetail);
                if (count != null && count > 0)
                {
                    lnkClickToView_1.NavigateUrl = EncryptURL("Reports/ScheduledPOReconciliation.aspx?PreviousPage=POInquiry&Params=" + vendorId.ToString() + "~" + siteId.ToString() + "~" + lblPurchaseOrderV.Text);
                    lnkClickToView_1.Enabled = true;
                }
                else
                {
                    lnkClickToView_1.Enabled = false;
                }

                #endregion PO Inquiry Scheduled PO Reconciliation

                #region PO Inquiry PenaltyManagement

                purchaseOrderDetail.Action = "GetPenaltyManagementVisibilityCount";
                count = purchaseOrderDetailBAL.GetPOInquiryReceiptVisibilityCountBAL(purchaseOrderDetail);
                if (count != null && count > 0)
                {
                    lnkClickToView_2.NavigateUrl = EncryptURL("../StockOverview/PenaltyReview/InventoryReview1.aspx?PreviousPage=POInquiry&Params=" + vendorId.ToString() + "~" + siteId.ToString() + "~" + lblPurchaseOrderV.Text);
                    lnkClickToView_2.Enabled = true;
                }
                else
                {
                    lnkClickToView_2.Enabled = false;
                }
                #endregion PO Inquiry PenaltyManagement

                #region PO Inquiry Debits

                purchaseOrderDetail.Action = "GetDebitsVisibilityCount";
                count = purchaseOrderDetailBAL.GetPOInquiryReceiptVisibilityCountBAL(purchaseOrderDetail);

                if (count != null && count > 0)
                {
                    lnkClickToView_3.NavigateUrl = EncryptURL("../Discrepancy/AccountsPayable/SearchDebit.aspx?PreviousPage=POInquiry&Params=" + vendorId.ToString() + "~" + siteId.ToString() + "~" + lblPurchaseOrderV.Text);
                    lnkClickToView_3.Enabled = true;
                }
                else
                {
                    lnkClickToView_3.Enabled = false;
                }
                #endregion PO Inquiry Debits

                //------------------------------------------------//

                pnlPurchaseOrderInqSearch.Visible = false;
                pnlPurchaseOrderInqDetail.Visible = true;

                if (ConfigurationManager.AppSettings.AllKeys.Any(key => key == "EmailSendVendorPO"))
                {
                    var country = ConfigurationManager.AppSettings["EmailSendVendorPO"];
                    btnSendMail.Visible = country.Split(',').Contains(lstPOInquiryMain[0].CountryID.ToString(), StringComparer.InvariantCultureIgnoreCase);
                }
            }

        }
        catch (Exception ex)
        {
            LogUtility.SaveErrorLogEntry(ex);
        }
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        if (GetQueryStringValue("Page") != null)
        {
            EncryptQueryString("~/ModuleUI/StockOverview/BackOrder/SKUDetails.aspx?PreviousPage=SKUDetails");
            return;
        }

        if (pnlPOReceipts.Visible)
        {
            this.POInquiryPanelVisibility(0);
        }
        else
        {
            pnlPurchaseOrderInqSearch.Visible = true;
            pnlPurchaseOrderInqDetail.Visible = false;
        }
    }

    protected void gvDiscrepancies_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType != DataControlRowType.Header && e.Row.RowType != DataControlRowType.Footer)
        {
            var hpBookingRefPOInq = (HyperLink)e.Row.FindControl("hpBookingRefPOInq");
            var hdnDiscrepancyTypeID = (HiddenField)e.Row.FindControl("hdnDiscrepancyTypeID");
            var hdnDisLogID = (HiddenField)e.Row.FindControl("hdnDiscrepancyLogID");
            var hdnUserID = (HiddenField)e.Row.FindControl("hdnUserID");
            string VDRNo = hpBookingRefPOInq.Text;

            if (!string.IsNullOrEmpty(hdnDiscrepancyTypeID.Value))
            {
                #region Switch Case detail ...

                switch (Convert.ToInt32(hdnDiscrepancyTypeID.Value.Trim()))
                {
                    case 1:
                        hpBookingRefPOInq.NavigateUrl = EncryptQuery("~/ModuleUI/Discrepancy/LogDiscrepancy/DISLog_Overs.aspx?disLogID=" + hdnDisLogID.Value + "&VDRNo=" + VDRNo + "&UserID=" + hdnUserID.Value + "&PN=POINQ&Status=" + lblStatusV.Text + "&PO=" + lblPurchaseOrderV.Text + "&SId=" + hdnBaseSiteId.Value);
                        break;

                    case 2:
                        hpBookingRefPOInq.NavigateUrl = EncryptQuery("~/ModuleUI/Discrepancy/LogDiscrepancy/DISLog_Shortage.aspx?disLogID=" + hdnDisLogID.Value + "&VDRNo=" + VDRNo + "&UserID=" + hdnUserID.Value + "&PN=POINQ&Status=" + lblStatusV.Text + "&PO=" + lblPurchaseOrderV.Text + "&SId=" + hdnBaseSiteId.Value);
                        break;

                    case 3:
                        hpBookingRefPOInq.NavigateUrl = EncryptQuery("~/ModuleUI/Discrepancy/LogDiscrepancy/DISLog_GoodsReceivedDamaged.aspx?disLogID=" + hdnDisLogID.Value + "&VDRNo=" + VDRNo + "&UserID=" + hdnUserID.Value + "&PN=POINQ&Status=" + lblStatusV.Text + "&PO=" + lblPurchaseOrderV.Text + "&SId=" + hdnBaseSiteId.Value);
                        break;

                    case 4:
                        hpBookingRefPOInq.NavigateUrl = EncryptQuery("~/ModuleUI/Discrepancy/LogDiscrepancy/DISLog_NoPurchaseOrder.aspx?disLogID=" + hdnDisLogID.Value + "&VDRNo=" + VDRNo + "&UserID=" + hdnUserID.Value + "&PN=POINQ&Status=" + lblStatusV.Text + "&PO=" + lblPurchaseOrderV.Text + "&SId=" + hdnBaseSiteId.Value);
                        break;

                    case 5:
                        hpBookingRefPOInq.NavigateUrl = EncryptQuery("~/ModuleUI/Discrepancy/LogDiscrepancy/DISLog_NoPaperwork.aspx?disLogID=" + hdnDisLogID.Value + "&VDRNo=" + VDRNo + "&UserID=" + hdnUserID.Value + "&PN=POINQ&Status=" + lblStatusV.Text + "&PO=" + lblPurchaseOrderV.Text + "&SId=" + hdnBaseSiteId.Value);
                        break;

                    case 6:
                        hpBookingRefPOInq.NavigateUrl = EncryptQuery("~/ModuleUI/Discrepancy/LogDiscrepancy/DISLog_IncorrectProduct.aspx?disLogID=" + hdnDisLogID.Value + "&VDRNo=" + VDRNo + "&UserID=" + hdnUserID.Value + "&PN=POINQ&Status=" + lblStatusV.Text + "&PO=" + lblPurchaseOrderV.Text + "&SId=" + hdnBaseSiteId.Value);
                        break;

                    case 7:
                        hpBookingRefPOInq.NavigateUrl = EncryptQuery("~/ModuleUI/Discrepancy/LogDiscrepancy/DISLog_PresentationIssue.aspx?disLogID=" + hdnDisLogID.Value + "&VDRNo=" + VDRNo + "&UserID=" + hdnUserID.Value + "&PN=POINQ&Status=" + lblStatusV.Text + "&PO=" + lblPurchaseOrderV.Text + "&SId=" + hdnBaseSiteId.Value);
                        break;

                    case 8:
                        hpBookingRefPOInq.NavigateUrl = EncryptQuery("~/ModuleUI/Discrepancy/LogDiscrepancy/DISLog_IncorrectAddress.aspx?disLogID=" + hdnDisLogID.Value + "&VDRNo=" + VDRNo + "&UserID=" + hdnUserID.Value + "&PN=POINQ&Status=" + lblStatusV.Text + "&PO=" + lblPurchaseOrderV.Text + "&SId=" + hdnBaseSiteId.Value);
                        break;

                    case 9:
                        hpBookingRefPOInq.NavigateUrl = EncryptQuery("~/ModuleUI/Discrepancy/LogDiscrepancy/DISLog_PaperworkAmended.aspx?disLogID=" + hdnDisLogID.Value + "&VDRNo=" + VDRNo + "&UserID=" + hdnUserID.Value + "&PN=POINQ&Status=" + lblStatusV.Text + "&PO=" + lblPurchaseOrderV.Text + "&SId=" + hdnBaseSiteId.Value);
                        break;

                    case 10:
                        hpBookingRefPOInq.NavigateUrl = EncryptQuery("~/ModuleUI/Discrepancy/LogDiscrepancy/DISLog_WrongPackSize.aspx?disLogID=" + hdnDisLogID.Value + "&VDRNo=" + VDRNo + "&UserID=" + hdnUserID.Value + "&PN=POINQ&Status=" + lblStatusV.Text + "&PO=" + lblPurchaseOrderV.Text + "&SId=" + hdnBaseSiteId.Value);
                        break;

                    case 11:
                        hpBookingRefPOInq.NavigateUrl = EncryptQuery("~/ModuleUI/Discrepancy/LogDiscrepancy/DISLog_FailPalletSpecification.aspx?disLogID=" + hdnDisLogID.Value + "&VDRNo=" + VDRNo + "&UserID=" + hdnUserID.Value + "&PN=POINQ&Status=" + lblStatusV.Text + "&PO=" + lblPurchaseOrderV.Text + "&SId=" + hdnBaseSiteId.Value);
                        break;

                    case 12:
                        hpBookingRefPOInq.NavigateUrl = EncryptQuery("~/ModuleUI/Discrepancy/LogDiscrepancy/DISLog_QualityIssue.aspx?disLogID=" + hdnDisLogID.Value + "&VDRNo=" + VDRNo + "&UserID=" + hdnUserID.Value + "&PN=POINQ&Status=" + lblStatusV.Text + "&PO=" + lblPurchaseOrderV.Text + "&SId=" + hdnBaseSiteId.Value);
                        break;

                    case 13:
                        hpBookingRefPOInq.NavigateUrl = EncryptQuery("~/ModuleUI/Discrepancy/LogDiscrepancy/DISLog_PrematureInvoiceReceipt.aspx?disLogID=" + hdnDisLogID.Value + "&VDRNo=" + VDRNo + "&UserID=" + hdnUserID.Value + "&PN=POINQ&Status=" + lblStatusV.Text + "&PO=" + lblPurchaseOrderV.Text + "&SId=" + hdnBaseSiteId.Value);
                        break;

                    case 14:
                        hpBookingRefPOInq.NavigateUrl = EncryptQuery("~/ModuleUI/Discrepancy/LogDiscrepancy/DISLog_GenericDescrepancy.aspx?disLogID=" + hdnDisLogID.Value + "&VDRNo=" + VDRNo + "&UserID=" + hdnUserID.Value + "&PN=POINQ&Status=" + lblStatusV.Text + "&PO=" + lblPurchaseOrderV.Text + "&SId=" + hdnBaseSiteId.Value);
                        break;

                    case 15:
                        hpBookingRefPOInq.NavigateUrl = EncryptQuery("~/ModuleUI/Discrepancy/LogDiscrepancy/DISLog_Shuttle.aspx?disLogID=" + hdnDisLogID.Value + "&VDRNo=" + VDRNo + "&UserID=" + hdnUserID.Value + "&PN=POINQ&Status=" + lblStatusV.Text + "&PO=" + lblPurchaseOrderV.Text + "&SId=" + hdnBaseSiteId.Value);
                        break;

                    case 16:
                        hpBookingRefPOInq.NavigateUrl = EncryptQuery("~/ModuleUI/Discrepancy/LogDiscrepancy/DISLog_Reservation.aspx?disLogID=" + hdnDisLogID.Value + "&VDRNo=" + VDRNo + "&UserID=" + hdnUserID.Value + "&PN=POINQ&Status=" + lblStatusV.Text + "&PO=" + lblPurchaseOrderV.Text + "&SId=" + hdnBaseSiteId.Value);
                        break;

                    case 17:
                        hpBookingRefPOInq.NavigateUrl = EncryptQuery("~/ModuleUI/Discrepancy/LogDiscrepancy/DISLog_InvoiceDiscrepancy.aspx?disLogID=" + hdnDisLogID.Value + "&VDRNo=" + VDRNo + "&UserID=" + hdnUserID.Value + "&PN=POINQ&Status=" + lblStatusV.Text + "&PO=" + lblPurchaseOrderV.Text + "&SId=" + hdnBaseSiteId.Value);
                        break;

                    case 18:
                        hpBookingRefPOInq.NavigateUrl = EncryptQuery("~/ModuleUI/Discrepancy/LogDiscrepancy/DISLog_StockPlannerInvoiceDiscrepancy.aspx?disLogID=" + hdnDisLogID.Value + "&VDRNo=" + VDRNo + "&UserID=" + hdnUserID.Value + "&PN=POINQ&Status=" + lblStatusV.Text + "&PO=" + lblPurchaseOrderV.Text + "&SId=" + hdnBaseSiteId.Value);
                        break;

                    case 19:
                        hpBookingRefPOInq.NavigateUrl = EncryptQuery("~/ModuleUI/Discrepancy/LogDiscrepancy/DISLog_InvoiceQueryDiscrepancy.aspx?disLogID=" + hdnDisLogID.Value + "&VDRNo=" + VDRNo + "&UserID=" + hdnUserID.Value + "&PN=POINQ&Status=" + lblStatusV.Text + "&PO=" + lblPurchaseOrderV.Text + "&SId=" + hdnBaseSiteId.Value);
                        break;
                }

                #endregion Switch Case detail ...
            }
        }
    }

    protected void gvBookingDetails_OnRowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType != DataControlRowType.Header && e.Row.RowType != DataControlRowType.Footer)
        {
            int WeekDay = Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "WeekDay"));
            DateTime ScheduleDate = Convert.ToDateTime(DataBinder.Eval(e.Row.DataItem, "ScheduleDate"));

            ucLabel ltdayname = (ucLabel)e.Row.FindControl("ltdayname");

            string strWeekDay = ScheduleDate.DayOfWeek.ToString().ToUpper();
            if (strWeekDay == "SUNDAY")
            {
                if (WeekDay != 1)
                    ltdayname.Text = ScheduleDate.AddDays(-1).DayOfWeek.ToString().ToUpper();
            }
            else if (strWeekDay == "MONDAY")
            {
                if (WeekDay != 2)
                    ltdayname.Text = ScheduleDate.AddDays(-1).DayOfWeek.ToString().ToUpper();
            }
            else if (strWeekDay == "TUESDAY")
            {
                if (WeekDay != 3)
                    ltdayname.Text = ScheduleDate.AddDays(-1).DayOfWeek.ToString().ToUpper();
            }
            else if (strWeekDay == "WEDNESDAY")
            {
                if (WeekDay != 4)
                    ltdayname.Text = ScheduleDate.AddDays(-1).DayOfWeek.ToString().ToUpper();
            }
            else if (strWeekDay == "THURSDAY")
            {
                if (WeekDay != 5)
                    ltdayname.Text = ScheduleDate.AddDays(-1).DayOfWeek.ToString().ToUpper();
            }
            else if (strWeekDay == "FRIDAY")
            {
                if (WeekDay != 6)
                    ltdayname.Text = ScheduleDate.AddDays(-1).DayOfWeek.ToString().ToUpper();
            }
            else if (strWeekDay == "SATURDAY")
            {
                if (WeekDay != 7)
                    ltdayname.Text = ScheduleDate.AddDays(-1).DayOfWeek.ToString().ToUpper();
            }
        }
    }

    protected void lnkClickToView_Click(object sender, EventArgs e)
    {
        this.POInquiryPanelVisibility(1);
        var lstPurchaseOrderDetail = (List<Up_PurchaseOrderDetailBE>)ViewState["POInquiryMain"];
        if (lstPurchaseOrderDetail != null && lstPurchaseOrderDetail.Count > 0)
        {
            var siteId = 0;
            if (ViewState["SiteId"] != null)
                siteId = Convert.ToInt32(ViewState["SiteId"]);

            var vendorId = 0;
            if (ViewState["VendorId"] != null)
                vendorId = Convert.ToInt32(ViewState["VendorId"]);

            var purchaseOrderDetailBAL = new UP_PurchaseOrderDetailBAL();
            var purchaseOrderDetail = new Up_PurchaseOrderDetailBE();
            purchaseOrderDetail.Action = "GetPOInquiryReceiptData";
            purchaseOrderDetail.Purchase_order = lstPurchaseOrderDetail[0].Purchase_order;
            purchaseOrderDetail.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();
            purchaseOrderDetail.Vendor.VendorID = vendorId;
            purchaseOrderDetail.Order_raised = lstPurchaseOrderDetail[0].Order_raised;
            purchaseOrderDetail.Site = new BusinessEntities.ModuleBE.AdminFunctions.MAS_SiteBE();
            purchaseOrderDetail.Site.SiteID = siteId;
            if (ViewState["OrderRaisedDateDDMMYYYY"] != null)
                purchaseOrderDetail.Order_raised = Common.GetMM_DD_YYYY(Convert.ToString(ViewState["OrderRaisedDateDDMMYYYY"]));

            var lstReceiptData = purchaseOrderDetailBAL.GetPOInquiryReceiptDataBAL(purchaseOrderDetail);

            var distinctReceiptDateData = lstReceiptData.OrderBy(receipt => receipt.ReceiptDate)
                .GroupBy(receipt => receipt.ReceiptDate)
                .SelectMany(receipt => receipt.Take(1));
            rptPOReceipts.DataSource = distinctReceiptDateData;
            rptPOReceipts.DataBind();

            for (int index = 0; index < rptPOReceipts.Items.Count; index++)
            {
                ucGridView gvPOReceiptsDetails = (ucGridView)rptPOReceipts.Items[index].FindControl("gvPOReceiptsDetails");
                HiddenField hdnDateText = (HiddenField)rptPOReceipts.Items[index].FindControl("hdnDateText");
                if (gvPOReceiptsDetails != null)
                {
                    gvPOReceiptsDetails.DataSource = lstReceiptData.FindAll(rd => rd.ReceiptDate.Equals(Convert.ToDateTime(hdnDateText.Value)));
                    gvPOReceiptsDetails.DataBind();
                }
            }
        }
    }

    private void POInquiryPanelVisibility(int inqDetail = 0)
    {
        switch (inqDetail)
        {
            case 0:
                pnlPODetails.Visible = true;
                pnlBookingDetails.Visible = true;
                pnlDiscrepancies.Visible = true;
                pnlReceipts.Visible = true;
                pnlPOReceipts.Visible = false;
                break;

            case 1:
                pnlPODetails.Visible = false;
                pnlBookingDetails.Visible = false;
                pnlDiscrepancies.Visible = false;
                pnlReceipts.Visible = false;
                pnlPOReceipts.Visible = true;
                break;
        }
    }
}
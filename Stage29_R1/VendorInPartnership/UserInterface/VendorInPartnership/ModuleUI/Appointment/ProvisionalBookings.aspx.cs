﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.AdminFunctions;
using BusinessLogicLayer.ModuleBAL.AdminFunctions;
using System.Collections;
using Utilities;
using System.Data;
using BaseControlLibrary;
using BusinessEntities.ModuleBE.Appointment.Booking;
using BusinessLogicLayer.ModuleBAL.Appointment.Booking;
using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using BusinessLogicLayer.ModuleBAL.Appointment.SiteSettings;
using WebUtilities;
using System.Configuration;
using BusinessEntities.ModuleBE.Languages.Languages;
using System.IO;

public partial class ProvisionalBookings : CommonPage
{
    public SortDirection GridViewSortDirection {
        get {
            if (ViewState["sortDirection"] == null)
                ViewState["sortDirection"] = SortDirection.Ascending;
            return (SortDirection)ViewState["sortDirection"];
        }
        set { ViewState["sortDirection"] = value; }
    }

    public string SortDir {
        get {
            if (GridViewSortDirection == SortDirection.Ascending) {
                return " DESC";
            }
            else {                
                return " ASC";
            }
        }
    }
    public string GridViewSortExp {
        get {
            if (ViewState["GridViewSortExp"] == null)
                ViewState["GridViewSortExp"] = "DeliveryDate,OrderBY";
            return (string)ViewState["GridViewSortExp"];
        }
        set { ViewState["GridViewSortExp"] = value; }
    }

    protected void Page_Init(object sender, EventArgs e) {

        if (!Page.IsPostBack) {
            if (Session["ProvisionalBooking"] != null) {
                 Hashtable htBookingCriteria = (Hashtable)Session["ProvisionalBooking"];

                 if (htBookingCriteria.ContainsKey("site")) {
                     int country = Convert.ToInt32(htBookingCriteria["country"]);                     
                     ddlSite.PreCountryID = country;
                     ddlSite.CountryID = Convert.ToInt32(country);
                 }
            }
            else if (Session["SiteCountryID"] != null) {
                ddlSite.CountryID = Convert.ToInt32(Session["SiteCountryID"]);
            }
        }

        ddlSite.CurrentPage = this;
        ddlSite.innerControlddlSite.AutoPostBack = false;
        ddlSite.IsAllRequired = true;

        ddlCountry.CurrentPage = this;
        ddlCountry.IsAllRequired = true;
        ddlCountry.innerControlddlCountry.AutoPostBack = true;
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        ucExportToExcel1.CurrentPage = this;
        ucExportToExcel1.GridViewControl = gvExport;
        ucExportToExcel1.FileName = "ProvisionalBooking";

        if (!Page.IsPostBack) {         
           
        }
    }

    protected void Page_PreRender(object sender, EventArgs e) {

        if (!Page.IsPostBack) {
            if (Session["ProvisionalBooking"] != null) {
                GetSession();
                SearchBookings();
                if (ViewState["lstSites"] != null) {
                    DataTable lstBookings = new DataTable();
                    lstBookings = (DataTable)ViewState["lstSites"];

                    if (lstBookings != null && lstBookings.Rows.Count > 0) {
                        SortGridView(GridViewSortExp, SortDir);
                    }
                }
            }
            else {
                gvVendor.PageIndex = 0;
                BindBookings();
                SetSession();
            }
        }
    }
    public override void CountrySelectedIndexChanged()
    {
        if (ddlCountry.innerControlddlCountry.SelectedIndex > 0) {
            ddlSite.CountryID = Convert.ToInt32(ddlCountry.innerControlddlCountry.SelectedItem.Value);
            ddlSite.BindSites();
        }
        else {
            ddlSite.BindSites();
        }
    }

    protected void btnSearch_Click(object sender, EventArgs e) {
        gvVendor.PageIndex = 0;
        BindBookings();
        SetSession();
    }

    private void SetSession() {
        Session["ProvisionalBooking"] = null;
        Session.Remove("ProvisionalBooking");
        Hashtable htBookingCritreria = new Hashtable();    
        htBookingCritreria.Add("country", ddlCountry.innerControlddlCountry.SelectedItem.Value);       
        htBookingCritreria.Add("site", ddlSite.innerControlddlSite.SelectedItem.Value);
        htBookingCritreria.Add("PageIndex", "0");
        htBookingCritreria.Add("SortExpr", GridViewSortExp);            
        if (GridViewSortDirection == SortDirection.Ascending) 
            htBookingCritreria.Add("SortDir", "ASC");            
        else 
            htBookingCritreria.Add("SortDir", "DESC");
            
        Session["ProvisionalBooking"] = htBookingCritreria;
    }

    private void GetSession() {
        if (Session["ProvisionalBooking"] != null) {
            Hashtable htBookingCriteria = (Hashtable)Session["ProvisionalBooking"];                     

            if (htBookingCriteria.ContainsKey("site")) {
                int country = Convert.ToInt32(htBookingCriteria["country"]);
                ddlSite.innerControlddlSite.SelectedValue = htBookingCriteria["site"].ToString();
                ddlSite.PreCountryID = country;
                ddlSite.CountryID = Convert.ToInt32(country);               
                SiteSelectedIndexChanged();
                ddlCountry.innerControlddlCountry.SelectedValue = htBookingCriteria["country"].ToString();
            }           

            gvVendor.PageIndex = Convert.ToInt32(htBookingCriteria["PageIndex"].ToString());
            GridViewSortExp = htBookingCriteria["SortExpr"].ToString();
            GridViewSortDirection = htBookingCriteria["SortDir"].ToString().Trim().ToUpper() == "ASC" ? SortDirection.Ascending : SortDirection.Descending;
           
        }
    }

    protected void gridView_Sorting(Object sender, GridViewSortEventArgs e) {
        try {
            string sortExpression = e.SortExpression;
            GridViewSortExp = sortExpression;            

            if (GridViewSortDirection == SortDirection.Ascending) {
                GridViewSortDirection = SortDirection.Descending;
                SortGridView(sortExpression, " ASC");
            }
            else {
                GridViewSortDirection = SortDirection.Ascending;
                SortGridView(sortExpression, " DESC");
            }

            if (Session["ProvisionalBooking"] != null) {
                Hashtable htBookingCriteria = (Hashtable)Session["ProvisionalBooking"];
                htBookingCriteria.Remove("SortExpr");
                htBookingCriteria.Add("SortExpr", GridViewSortExp);
                htBookingCriteria.Remove("SortDir");
                if (GridViewSortDirection == SortDirection.Ascending) {
                    htBookingCriteria.Add("SortDir", "ASC");                    
                }
                else {
                    htBookingCriteria.Add("SortDir", "DESC");                   
                }               
                Session["ProvisionalBooking"] = htBookingCriteria;
            }          
        }
        catch (Exception ex) {
            LogUtility.SaveErrorLogEntry(ex);
        }
    }

    private void SortGridView(string sortExpression, string direction) {
        DataTable dtSortingTbl;
        DataView dtView;
        try {
            if (ViewState["lstSites"] != null) {
                direction = " " + direction;
                dtSortingTbl = (DataTable)ViewState["lstSites"];
                dtView = dtSortingTbl.DefaultView;
                //dtView =  new DataView(dtSortingTbl);
                if (sortExpression != "Weekday") {
                    int sortExpIndx = sortExpression.IndexOf(",");
                    if (sortExpIndx == -1)
                        dtView.Sort = sortExpression + direction;
                    else {
                        dtView.Sort = sortExpression.Substring(0, sortExpIndx) + direction + sortExpression.Substring(sortExpIndx);
                    }
                }
                else {
                    if (direction.Trim() == "ASC")
                        dtView.Sort = "Weekday desc,OrderBY";
                    else
                        dtView.Sort = "Weekday asc,OrderBY";
                }               

                gvVendor.DataSource = dtSortingTbl;
                gvVendor.DataBind();
                ViewState["lstSites"] = dtSortingTbl;
                CommonPage c = new CommonPage();
                c.LocalizeGridHeader(gvVendor);
            }
        }
        catch (Exception ex) {
            LogUtility.SaveErrorLogEntry(ex);
        }

    }

    public override BusinessEntities.BaseBe[] SubSort(GridViewSortEventArgs e) {
        return Utilities.GenericListHelper<APPBOK_BookingBE>.SortList((List<APPBOK_BookingBE>)ViewState["lstSites"], e.SortExpression, e.SortDirection).ToArray();
    }

    protected void gvVendor_PageIndexChanging(object sender, GridViewPageEventArgs e) {
        gvVendor.PageIndex = e.NewPageIndex;
        if (Session["ProvisionalBooking"] != null) {
            Hashtable htBookingCriteria = (Hashtable)Session["ProvisionalBooking"];

            htBookingCriteria.Remove("PageIndex");
            htBookingCriteria.Add("PageIndex", e.NewPageIndex);

            Session["ProvisionalBooking"] = htBookingCriteria;
        }
        BindBookingPageWise();
    }

    protected void gvVendor_RowDataBound(object sender, GridViewRowEventArgs e) {
        GridViewRow gvr = e.Row;
        if (e.Row.RowType == DataControlRowType.DataRow) {
            #region Code to set visibility of [Time / Day] based on [Time Window Id] ..
            ucLiteral UcLiteral27 = (ucLiteral)e.Row.FindControl("UcLiteral27");
            ucLiteral UcLiteral27_1 = (ucLiteral)e.Row.FindControl("UcLiteral27_1");
            ucLiteral UcLiteral27_2 = (ucLiteral)e.Row.FindControl("UcLiteral27_2");
            //HiddenField hdnTimeWindowID = (HiddenField)e.Row.FindControl("hdnTimeWindowID");

            if (!string.IsNullOrEmpty(UcLiteral27_1.Text) && !string.IsNullOrWhiteSpace(UcLiteral27_1.Text)) {
                UcLiteral27_2.Visible = false;
                UcLiteral27_1.Visible = true;
                UcLiteral27.Visible = false;
            }
            else if (!string.IsNullOrEmpty(UcLiteral27_2.Text) && !string.IsNullOrWhiteSpace(UcLiteral27_2.Text)) {
                UcLiteral27_2.Visible = true;
                UcLiteral27_1.Visible = false;
                UcLiteral27.Visible = false;
            }
            else {
                UcLiteral27_2.Visible = false;
                UcLiteral27_1.Visible = false;
                UcLiteral27.Visible = true;
            }
            #endregion

            DataRowView dr = (DataRowView)e.Row.DataItem;

            if (dr != null) {
                string TimeWindowFlag = dr["TimeSlotWindow"].ToString();

                ucLiteral ltHistory = (ucLiteral)e.Row.FindControl("ltHistory");
                HyperLink hypHistory = (HyperLink)e.Row.FindControl("hypHistory");

                if (ltHistory.Text == "Fixed booking") {
                    ltHistory.Visible = true;
                    hypHistory.Visible = false;
                }
                else {
                    ltHistory.Visible = false;
                    hypHistory.Visible = true;

                    string path = string.Empty;

                    if (dr["SupplierType"].ToString() == "V") {
                        if (TimeWindowFlag == "W")
                            path = "~/ModuleUI/Appointment/Booking/APPBok_WindowBookingEdit.aspx?BP=Pro&Mode=Edit&Type=" + dr["SupplierType"].ToString() + "&ID=" + dr["PKID"].ToString() + "&Scheduledate=" + Common.GetDD_MM_YYYYDatetime(dr["DeliveryDate"].ToString()).ToString("dd/MM/yyyy") + "&Logicaldate=" + Common.GetDD_MM_YYYYDatetime(dr["DeliveryDate"].ToString()).ToString("dd/MM/yyyy") + "&SiteID=" + dr["Siteid"].ToString() + "&SiteCountryID=" + dr["SiteCountryID"].ToString();
                        else
                            path = "~/ModuleUI/Appointment/Booking/APPBok_BookingEdit.aspx?BP=Pro&Mode=Edit&Type=" + dr["SupplierType"].ToString() + "&ID=" + dr["PKID"].ToString() + "&Scheduledate=" + Common.GetDD_MM_YYYYDatetime(dr["DeliveryDate"].ToString()).ToString("dd/MM/yyyy") + "&Logicaldate=" + Common.GetDD_MM_YYYYDatetime(dr["DeliveryDate"].ToString()).ToString("dd/MM/yyyy") + "&SiteID=" + dr["Siteid"].ToString() + "&SiteCountryID=" + dr["SiteCountryID"].ToString();
                    }
                    else {
                        if (TimeWindowFlag == "W")
                            path = "~/ModuleUI/Appointment/Booking/APPBok_CarrierBookingWindowEdit.aspx?BP=Pro&Mode=Edit&Type=" + dr["SupplierType"].ToString() + "&ID=" + dr["PKID"].ToString() + "&Scheduledate=" + Common.GetDD_MM_YYYYDatetime(dr["DeliveryDate"].ToString()).ToString("dd/MM/yyyy") + "&Logicaldate=" + Common.GetDD_MM_YYYYDatetime(dr["DeliveryDate"].ToString()).ToString("dd/MM/yyyy") + "&SiteID=" + dr["Siteid"].ToString() + "&SiteCountryID=" + dr["SiteCountryID"].ToString();
                        else
                            path = "~/ModuleUI/Appointment/Booking/APPBok_CarrierBookingEdit.aspx?BP=Pro&Mode=Edit&Type=" + dr["SupplierType"].ToString() + "&ID=" + dr["PKID"].ToString() + "&Scheduledate=" + Common.GetDD_MM_YYYYDatetime(dr["DeliveryDate"].ToString()).ToString("dd/MM/yyyy") + "&Logicaldate=" + Common.GetDD_MM_YYYYDatetime(dr["DeliveryDate"].ToString()).ToString("dd/MM/yyyy") + "&SiteID=" + dr["Siteid"].ToString() + "&SiteCountryID=" + dr["SiteCountryID"].ToString();

                    }
                    //NavigateUrl='<%# EncryptQuery("~/ModuleUI/Appointment/Receiving/APPRcv_BookingHistory.aspx?PN=PROBO&Scheduledate=" + Eval("DeliveryDate", "{0:dd/MM/yyyy}") + "&ID=" + Eval("PKID") + "-V-" + Eval("PKID") + "-" +  Eval("PKID")) %>'

                    hypHistory.NavigateUrl = Common.EncryptQuery(path);
                }

                //int i = e.Row.RowIndex;


                HyperLink hypReason = (HyperLink)e.Row.FindControl("hypReason");

                if (hypReason.Text == "FIXED SLOT") {
                    hypReason.NavigateUrl = EncryptURL("~/ModuleUI/Appointment/SiteSettings/APPSIT_FixedSlotSetupEdit.aspx?BP=Pro&FixedSlotID=" + dr["FixedSlotID"] + "&SiteID=" + dr["SiteID"]);

                    //----Start Phase 12 R2 Point 2------------//
                    LinkButton hypConfirm = (LinkButton)e.Row.FindControl("hypConfirm");
                    LinkButton hypReject = (LinkButton)e.Row.FindControl("hypReject");
                    hypConfirm.Visible = true;
                    hypReject.Visible = true;
                    hypConfirm.Text = WebCommon.getGlobalResourceValue("Confirm").ToUpper();
                    hypReject.Text = WebCommon.getGlobalResourceValue("Reject").ToUpper();

                    //----End Phase 12 R2 Point 2------------//
                }
                else if (hypReason.Text == "DAILY CAPACITY") {
                    MASSIT_WeekSetupBE oMASSIT_WeekSetupBE = new MASSIT_WeekSetupBE();
                    MASSIT_WeekSetupBAL oMASSIT_WeekSetupBAL = new MASSIT_WeekSetupBAL();

                    oMASSIT_WeekSetupBE.Action = "GetSpecificWeek";
                    oMASSIT_WeekSetupBE.SiteID = Convert.ToInt32(dr["SiteID"]);
                    oMASSIT_WeekSetupBE.EndWeekday = Common.GetDD_MM_YYYYDatetime(dr["DeliveryDate"].ToString()).DayOfWeek.ToString();
                    oMASSIT_WeekSetupBE.ScheduleDate = Common.GetDD_MM_YYYYDatetime(dr["DeliveryDate"].ToString());

                    List<MASSIT_WeekSetupBE> lstWeekSetup = oMASSIT_WeekSetupBAL.GetWeekSetupDetailsBAL(oMASSIT_WeekSetupBE);

                    if (lstWeekSetup == null || lstWeekSetup.Count <= 0) {
                        oMASSIT_WeekSetupBE.Action = "ShowAll";
                        oMASSIT_WeekSetupBE.SiteID = Convert.ToInt32(dr["SiteID"]);
                        oMASSIT_WeekSetupBE.EndWeekday = Common.GetDD_MM_YYYYDatetime(dr["DeliveryDate"].ToString()).DayOfWeek.ToString();
                        lstWeekSetup = oMASSIT_WeekSetupBAL.GetWeekSetupDetailsBAL(oMASSIT_WeekSetupBE);

                        hypReason.NavigateUrl = EncryptURL("~/ModuleUI/Appointment/SiteSettings/APPSIT_WeekSetupEdit.aspx?BP=Pro&SiteScheduleDiaryID=" + lstWeekSetup[0].SiteScheduleDiaryID.ToString() + "&weekday=" + lstWeekSetup[0].EndWeekday);
                    }
                    else if (lstWeekSetup != null && lstWeekSetup.Count > 0) {
                        hypReason.NavigateUrl = EncryptURL("~/ModuleUI/Appointment/SiteSettings/APPSIT_SpecificWeekSetupEdit.aspx?BP=Pro&WeekStartDate=" + dr["DeliveryDate"].ToString() + "&SiteScheduleDiaryID=" + lstWeekSetup[0].SiteScheduleDiaryID.ToString() + "&weekday=" + lstWeekSetup[0].EndWeekday + "&WeekSpecificID=" + lstWeekSetup[0].WeekSetupSpecificID.ToString());
                    }
                }

                else if (hypReason.Text == "WINDOW CAPACITY") {
                    #region Get DoorNoType
                    APPSIT_DoorOpenTimeBAL oMASSIT_DoorOpenTimeBAL = new APPSIT_DoorOpenTimeBAL();
                    MASSIT_DoorOpenTimeBE oMASSIT_DoorOpenTimeBE = new MASSIT_DoorOpenTimeBE();
                    oMASSIT_DoorOpenTimeBE.Action = "GetDoorNoAndType";
                    oMASSIT_DoorOpenTimeBE.DoorNo = new MASSIT_DoorNoSetupBE();
                    oMASSIT_DoorOpenTimeBE.DoorNo.SiteID = Convert.ToInt32(dr["SiteID"]);
                    List<MASSIT_DoorOpenTimeBE> lstDoorNoType = oMASSIT_DoorOpenTimeBAL.GetDoorNoSetupDetailsBAL(oMASSIT_DoorOpenTimeBE);

                    lstDoorNoType = lstDoorNoType.FindAll(delegate(MASSIT_DoorOpenTimeBE t) { return t.SiteDoorNumberID == Convert.ToInt32(dr["SiteDoorNumberID"]); });

                    if (lstDoorNoType != null && lstDoorNoType.Count > 0) {
                        string path = "~/ModuleUI/Appointment/SiteSettings/APPSIT_TimeWindowEdit.aspx"
                                     + "?BP=Pro&SiteId=" + dr["SiteID"]
                                     + "&SiteDoorNoId=" + lstDoorNoType[0].SiteDoorNumberID.ToString()
                                     + "&DoorNo=" + lstDoorNoType[0].DoorNo.DoorNumber.ToString()
                                     + "&DoorType=" + lstDoorNoType[0].DoorType.DoorType.ToString()
                                     + "&WeekDay=" + Common.GetDD_MM_YYYYDatetime(dr["DeliveryDate"].ToString()).DayOfWeek.ToString()
                                     + "&TimeWindowID=" + dr["TimeWindowID"];

                    #endregion

                        hypReason.NavigateUrl = Common.EncryptQuery(path);
                    }
                    else {
                        hypReason.NavigateUrl = "javascript:;";
                    }
                }
                else if (hypReason.Text == "RESERVED WINDOW CAPACITY") {
                    #region Get DoorNoType
                    //APPSIT_DoorOpenTimeBAL oMASSIT_DoorOpenTimeBAL = new APPSIT_DoorOpenTimeBAL();
                    //MASSIT_DoorOpenTimeBE oMASSIT_DoorOpenTimeBE = new MASSIT_DoorOpenTimeBE();
                    //oMASSIT_DoorOpenTimeBE.Action = "GetDoorNoAndType";
                    //oMASSIT_DoorOpenTimeBE.DoorNo = new MASSIT_DoorNoSetupBE();
                    //oMASSIT_DoorOpenTimeBE.DoorNo.SiteID = Convert.ToInt32(dr["SiteID"]);
                    //List<MASSIT_DoorOpenTimeBE> lstDoorNoType = oMASSIT_DoorOpenTimeBAL.GetDoorNoSetupDetailsBAL(oMASSIT_DoorOpenTimeBE);

                    //lstDoorNoType = lstDoorNoType.FindAll(delegate(MASSIT_DoorOpenTimeBE t) { return t.SiteDoorNumberID == Convert.ToInt32(dr["SiteDoorNumberID"]); });

                    string path = "~/ModuleUI/Appointment/SiteSettings/APPSIT_ReservedWindowSetupEdit.aspx"
                                    + "?BP=Pro&TimeWindowID=" + dr["TimeWindowID"]
                                    + "&SiteID=" + dr["SiteID"];


                    #endregion

                    hypReason.NavigateUrl = Common.EncryptQuery(path);
                }
                else if (hypReason.Text == "NO SPACE") {

                    string path = string.Empty;
                    if (dr["SupplierType"].ToString() == "V") {
                        if (TimeWindowFlag == "W")
                            path = "~/ModuleUI/Appointment/Booking/APPBok_WindowBookingEdit.aspx?PNTB=True&Mode=Edit&Type=" + dr["SupplierType"].ToString() + "&ID=" + dr["PKID"].ToString() + "&Scheduledate=" + Common.GetDD_MM_YYYYDatetime(dr["DeliveryDate"].ToString()).ToString("dd/MM/yyyy") + "&Logicaldate=" + Common.GetDD_MM_YYYYDatetime(dr["DeliveryDate"].ToString()).ToString("dd/MM/yyyy") + "&SiteID=" + dr["Siteid"].ToString() + "&SiteCountryID=" + dr["SiteCountryID"].ToString();
                        else
                            path = "~/ModuleUI/Appointment/Booking/APPBok_BookingEdit.aspx?PNTB=True&Mode=Edit&Type=" + dr["SupplierType"].ToString() + "&ID=" + dr["PKID"].ToString() + "&Scheduledate=" + Common.GetDD_MM_YYYYDatetime(dr["DeliveryDate"].ToString()).ToString("dd/MM/yyyy") + "&Logicaldate=" + Common.GetDD_MM_YYYYDatetime(dr["DeliveryDate"].ToString()).ToString("dd/MM/yyyy") + "&SiteID=" + dr["Siteid"].ToString() + "&SiteCountryID=" + dr["SiteCountryID"].ToString();
                    }
                    else {
                        if (TimeWindowFlag == "W")
                            path = "~/ModuleUI/Appointment/Booking/APPBok_CarrierBookingWindowEdit.aspx?PNTB=True&Mode=Edit&Type=" + dr["SupplierType"].ToString() + "&ID=" + dr["PKID"].ToString() + "&Scheduledate=" + Common.GetDD_MM_YYYYDatetime(dr["DeliveryDate"].ToString()).ToString("dd/MM/yyyy") + "&Logicaldate=" + Common.GetDD_MM_YYYYDatetime(dr["DeliveryDate"].ToString()).ToString("dd/MM/yyyy") + "&SiteID=" + dr["Siteid"].ToString() + "&SiteCountryID=" + dr["SiteCountryID"].ToString();
                        else
                            path = "~/ModuleUI/Appointment/Booking/APPBok_CarrierBookingEdit.aspx?PNTB=True&Mode=Edit&Type=" + dr["SupplierType"].ToString() + "&ID=" + dr["PKID"].ToString() + "&Scheduledate=" + Common.GetDD_MM_YYYYDatetime(dr["DeliveryDate"].ToString()).ToString("dd/MM/yyyy") + "&Logicaldate=" + Common.GetDD_MM_YYYYDatetime(dr["DeliveryDate"].ToString()).ToString("dd/MM/yyyy") + "&SiteID=" + dr["Siteid"].ToString() + "&SiteCountryID=" + dr["SiteCountryID"].ToString();

                    }

                    hypReason.NavigateUrl = Common.EncryptQuery(path);
                }
                else if (hypReason.Text == "EDITED") {
                    string path = string.Empty;
                    path = Common.EncryptQuery("~/ModuleUI/Appointment/Receiving/APPRcv_BookingHistory.aspx?PN=PROBO&Scheduledate="
                            + Common.GetDD_MM_YYYYDatetime(dr["DeliveryDate"].ToString()).ToString("dd/MM/yyyy") + "&ID=" + dr["PKID"].ToString() + "-"
                            + dr["SupplierType"].ToString() + "-" + dr["PKID"].ToString() + "-" + dr["Siteid"].ToString());
                    hypReason.NavigateUrl = path;
                }
            }
        }
    }

    //-----------Start Stage 12  point 2--------------//
    
    protected void btnContinue_2_Click(object sender, EventArgs e) {
        DeleteBooking(hdnBookingID.Value);
        mdlRejectProvisional.Hide();
        BindBookings();
    }

    protected void btnBack_2_Click(object sender, EventArgs e) {
        mdlRejectProvisional.Hide();
    }

    protected void gvVendor_RowCommand(object sender, GridViewCommandEventArgs e) {
        if (e.CommandName == "Reject") {
            mdlRejectProvisional.Show();            
            hdnBookingID.Value = e.CommandArgument.ToString();            
        }
        else if (e.CommandName == "Confirm") {   
            MakeBooking(e.CommandArgument.ToString());
        }
    }

    private void MakeBooking(string BookingID) {
        APPBOK_BookingBE oAPPBOK_BookingBE = new APPBOK_BookingBE();
        APPBOK_BookingBAL oAPPBOK_BookingBAL = new APPBOK_BookingBAL();

        oAPPBOK_BookingBE.Action = "ConformFixedSlotBooking";
        oAPPBOK_BookingBE.BookingID = Convert.ToInt32(BookingID);
        oAPPBOK_BookingBE.UserID = Convert.ToInt32(Session["UserID"]);
        oAPPBOK_BookingBAL.UpdateBookingStatusByIdBAL(oAPPBOK_BookingBE);
        Session["BookingCreated"] = true;
        Session["CreatedBookingID"] = BookingID.ToString();
        Session["IsBookingAmended"] = "false";
        EncryptQueryString("Booking/APPBok_Confirm.aspx?BP=PRO&MailSend=True&BookingID=" + BookingID + "&IsBookingAmended=false");
    }

    private void DeleteBooking(string BookingID) {
        APPBOK_BookingBE oAPPBOK_BookingBE = new APPBOK_BookingBE();
        APPBOK_BookingBAL oAPPBOK_BookingBAL = new APPBOK_BookingBAL();
        oAPPBOK_BookingBE.FixedSlot = new BusinessEntities.ModuleBE.Appointment.SiteSettings.MASSIT_FixedSlotBE();
        oAPPBOK_BookingBE.Action = "DeleteBooking";
        oAPPBOK_BookingBE.UserID = Convert.ToInt32(Session["UserID"]);
        oAPPBOK_BookingBE.BookingID = Convert.ToInt32(BookingID);
        oAPPBOK_BookingBAL.DeleteBookingDetailBAL(oAPPBOK_BookingBE);

        #region Logic to send emails ...
        var BookingId = Convert.ToInt32(BookingID);
        oAPPBOK_BookingBE = new APPBOK_BookingBE();
        oAPPBOK_BookingBAL = new APPBOK_BookingBAL();
        oAPPBOK_BookingBE.Action = "GetDeletedBookingDeatilsByID";
        oAPPBOK_BookingBE.BookingID = BookingId;
        var lstBooking = oAPPBOK_BookingBAL.GetDeletedBookingDetailsByIDBAL(oAPPBOK_BookingBE);
        if (lstBooking.Count > 0)
            this.SendProvisionalRefusalLetterMail(lstBooking, BookingId);

        #endregion

        mdlRejectProvisional.Hide();
    }

    private void SendProvisionalRefusalLetterMail(List<APPBOK_BookingBE> lstBooking, int BookingID) {
        try {
            var oMASSIT_VendorBE = new MASSIT_VendorBE();
            var oAPPSIT_VendorBAL = new APPSIT_VendorBAL();
            string VendorEmails = string.Empty;
            List<MASSIT_VendorBE> lstVendorDetails = null;
            string[] sentTo;
            string[] language;
            string[] VendorData = new string[2];

            if (lstBooking[0].SupplierType == "C") {
                oMASSIT_VendorBE.Action = "GetDeletedCarriersVendor";
                oMASSIT_VendorBE.BookingID = BookingID;
                lstVendorDetails = oAPPSIT_VendorBAL.GetDeletedCarriersVendorIDBAL(oMASSIT_VendorBE);
                if (lstVendorDetails != null && lstVendorDetails.Count > 0) {
                    for (int i = 0; i < lstVendorDetails.Count; i++) {
                        VendorData = CommonPage.GetVendorEmailsWithLanguage(lstVendorDetails[i].VendorID, Convert.ToInt32(lstBooking[0].SiteId));
                        if (VendorData.Length > 0 && !string.IsNullOrEmpty(VendorData[0])) {
                            sentTo = VendorData[0].Split(',');
                            language = VendorData[1].Split(',');
                            for (int j = 0; j < sentTo.Length; j++) {
                                if (sentTo[j].Trim() != "") {
                                    SendMailToVendor(sentTo[j], language[j], lstBooking, BookingID);
                                }
                            }
                        }
                        else
                            SendMailToVendor(ConfigurationManager.AppSettings["DefaultEmailAddress"], "English", lstBooking, BookingID);

                    }
                }


                //Send mail to Carrier
                VendorData = CommonPage.GetCarrierDefaultEmailWithLanguage(lstBooking[0].Carrier.CarrierID);
                if (VendorData.Length > 0 && !string.IsNullOrEmpty(VendorData[0])) {
                    sentTo = VendorData[0].Split(',');
                    language = VendorData[1].Split(',');
                    for (int j = 0; j < sentTo.Length; j++) {
                        if (sentTo[j].Trim() != "") {
                            SendMailToVendor(sentTo[j], language[j], lstBooking, BookingID);
                        }
                    }
                }
                else
                    SendMailToVendor(ConfigurationManager.AppSettings["DefaultEmailAddress"], "English", lstBooking, BookingID);

                //----------------------//

            }
            else if (lstBooking[0].SupplierType == "V") {
                VendorData = CommonPage.GetVendorEmailsWithLanguage(lstBooking[0].VendorID, Convert.ToInt32(lstBooking[0].SiteId));
                if (VendorData.Length > 0 && !string.IsNullOrEmpty(VendorData[0])) {
                    sentTo = VendorData[0].Split(',');
                    language = VendorData[1].Split(',');
                    for (int j = 0; j < sentTo.Length; j++) {
                        if (sentTo[j].Trim() != "") {
                            SendMailToVendor(sentTo[j], language[j], lstBooking, BookingID);
                        }
                    }
                }
                else
                    SendMailToVendor(ConfigurationManager.AppSettings["DefaultEmailAddress"], "English", lstBooking, BookingID);
            }

        }
        catch { }
    }

    string sPortalLink = Convert.ToString(ConfigurationManager.AppSettings["Portallink"]) + @"/ModuleUI/Security/Login.aspx";
    string path = AppDomain.CurrentDomain.BaseDirectory.ToLower();
    string templatePathName = @"/EmailTemplates/Appointment/";

    private void SendMailToVendor(string toAddress, string language, List<APPBOK_BookingBE> lstBooking, int BookingID) {
        if (!string.IsNullOrEmpty(toAddress)) {
            #region Pallet, Lines, Cartons, DeliveryDateValue and DeliveryTimeValue ...
            string DeliveryDateValue = string.Empty;

            int iTotalPalllets, iTotalLines, iTotalCartons, iTotalLifts;
            iTotalPalllets = iTotalLines = iTotalCartons = iTotalLifts = 0;

            for (int iCount = 0; iCount < lstBooking.Count; iCount++) {
                iTotalPalllets += Convert.ToInt32(lstBooking[iCount].NumberOfPallet);
                iTotalLines += Convert.ToInt32(lstBooking[iCount].NumberOfLines);
                iTotalCartons += Convert.ToInt32(lstBooking[iCount].NumberOfCartons);
                iTotalLifts += Convert.ToInt32(lstBooking[iCount].NumberOfCartons);
            }

            DeliveryDateValue = lstBooking[0].ScheduleDate.Value.ToString("dd/MM/yyyy");
            string strWeekDay = lstBooking[0].ScheduleDate.Value.DayOfWeek.ToString().ToUpper();
            if (strWeekDay == "SUNDAY") {
                if (lstBooking[0].WeekDay != 1)
                    DeliveryDateValue = lstBooking[0].ScheduleDate.Value.AddDays(-1).ToString("dd/MM/yyyy");
            }
            else if (strWeekDay == "MONDAY") {
                if (lstBooking[0].WeekDay != 2)
                    DeliveryDateValue = lstBooking[0].ScheduleDate.Value.AddDays(-1).ToString("dd/MM/yyyy");
            }
            else if (strWeekDay == "TUESDAY") {
                if (lstBooking[0].WeekDay != 3)
                    DeliveryDateValue = lstBooking[0].ScheduleDate.Value.AddDays(-1).ToString("dd/MM/yyyy");
            }
            else if (strWeekDay == "WEDNESDAY") {
                if (lstBooking[0].WeekDay != 4)
                    DeliveryDateValue = lstBooking[0].ScheduleDate.Value.AddDays(-1).ToString("dd/MM/yyyy");
            }
            else if (strWeekDay == "THURSDAY") {
                if (lstBooking[0].WeekDay != 5)
                    DeliveryDateValue = lstBooking[0].ScheduleDate.Value.AddDays(-1).ToString("dd/MM/yyyy");
            }
            else if (strWeekDay == "FRIDAY") {
                if (lstBooking[0].WeekDay != 6)
                    DeliveryDateValue = lstBooking[0].ScheduleDate.Value.AddDays(-1).ToString("dd/MM/yyyy");
            }
            else if (strWeekDay == "SATURDAY") {
                if (lstBooking[0].WeekDay != 7)
                    DeliveryDateValue = lstBooking[0].ScheduleDate.Value.AddDays(-1).ToString("dd/MM/yyyy");
            }

            #endregion

            string htmlBody = string.Empty;
            var oSendCommunicationCommon = new sendCommunicationCommon();

            //string vendorName = string.Join(",", lstBooking.Select(x => x.Vendor.VendorName).ToArray());

            string templatePath = null;
            templatePath = path + templatePathName;
            string LanguageFile = string.Empty;

            APPBOK_CommunicationBAL oAPPBOK_CommunicationBAL = new APPBOK_CommunicationBAL();
            List<MAS_LanguageBE> oLanguages = oAPPBOK_CommunicationBAL.GetLanguages();

            foreach (MAS_LanguageBE objLanguage in oLanguages) {
                bool MailSentInLanguage = false;

                if (objLanguage.Language.ToLower() == language.ToLower()) {
                    MailSentInLanguage = true;
                }

                //LanguageFile = "ProvisionalRefusalLetter." + objLanguage.Language + ".htm";
                LanguageFile = "ProvisionalRefusalLetter.english.htm";
                var singleSiteSetting = GetSingleSiteSetting(Convert.ToInt32(lstBooking[0].SiteId));

                using (StreamReader sReader = new StreamReader(templatePath + LanguageFile.ToLower())) {
                    #region mailBody
                    htmlBody = sReader.ReadToEnd();
                    //htmlBody = htmlBody.Replace("{logoInnerPath}", oSendCommunicationCommon.getAbsolutePath());

                    htmlBody = htmlBody.Replace("{DearSirMadam}", WebCommon.getGlobalResourceValue("DearSirMadam", objLanguage.Language));
                    htmlBody = htmlBody.Replace("{ProvisionalRefusalText1}", WebCommon.getGlobalResourceValue("ProvisionalRefusalText1", objLanguage.Language));

                    htmlBody = htmlBody.Replace("{GoodsInComment}", WebCommon.getGlobalResourceValue("GoodsInComment", objLanguage.Language));
                    htmlBody = htmlBody.Replace("{GINCOMMENTValue}", txtRejectProCmments.Text.Trim());

                    htmlBody = htmlBody.Replace("{BookingRef}", lstBooking[0].BookingRef);

                    htmlBody = htmlBody.Replace("{Date}", WebCommon.getGlobalResourceValue("Date", objLanguage.Language));
                    htmlBody = htmlBody.Replace("{DateValue}", DeliveryDateValue);

                    htmlBody = htmlBody.Replace("{Time}", WebCommon.getGlobalResourceValue("Time", objLanguage.Language));
                    htmlBody = htmlBody.Replace("{TimeValue}", WebCommon.getGlobalResourceValue("ProvisionalBooking", objLanguage.Language).ToUpper());

                    htmlBody = htmlBody.Replace("{Carrier}", WebCommon.getGlobalResourceValue("Carrier", objLanguage.Language));
                    htmlBody = htmlBody.Replace("{CarrierValue}", lstBooking[0].Carrier.CarrierName);

                    htmlBody = htmlBody.Replace("{VehicleTypeActual}", WebCommon.getGlobalResourceValue("VehicleTypeActual", objLanguage.Language));
                    htmlBody = htmlBody.Replace("{VehicleTypeValue}", lstBooking[0].VehicleType.VehicleType);

                    htmlBody = htmlBody.Replace("{NumberofPallets}", WebCommon.getGlobalResourceValue("NumberofPallets", objLanguage.Language));
                    htmlBody = htmlBody.Replace("{PalletsValue}", Convert.ToString(iTotalPalllets));

                    htmlBody = htmlBody.Replace("{NumberofLifts}", WebCommon.getGlobalResourceValue("NumberofLifts", objLanguage.Language));
                    htmlBody = htmlBody.Replace("{LiftsValue}", Convert.ToString(iTotalLifts));

                    htmlBody = htmlBody.Replace("{NumberofCartons}", WebCommon.getGlobalResourceValue("NumberofCartons", objLanguage.Language));
                    htmlBody = htmlBody.Replace("{CartonsValue}", Convert.ToString(iTotalCartons));

                    htmlBody = htmlBody.Replace("{NumberofPOLines}", WebCommon.getGlobalResourceValue("NumberofPOLines", objLanguage.Language));
                    htmlBody = htmlBody.Replace("{LinesValue}", Convert.ToString(iTotalLines));

                    htmlBody = htmlBody.Replace("{BookedPONo}", WebCommon.getGlobalResourceValue("BookedPONo", objLanguage.Language));
                    htmlBody = htmlBody.Replace("{BookedPOsValue}", lstBooking[0].PurchaseOrders.TrimStart(',').TrimEnd(','));

                    htmlBody = htmlBody.Replace("{dd/mm/yyyy}", lstBooking[0].BookingDate);
                    htmlBody = htmlBody.Replace("{PortalLink}", "<a href=" + sPortalLink + ">" + sPortalLink + " </a>");

                    htmlBody = htmlBody.Replace("{Yoursfaithfully}", WebCommon.getGlobalResourceValue("Yoursfaithfully", objLanguage.Language));

                    //---Stage 7 Point 29-----//
                    if (singleSiteSetting != null && singleSiteSetting.NonTimeStart != string.Empty) {
                        htmlBody = htmlBody.Replace("{tblVisible}", "");
                        htmlBody = htmlBody.Replace("{SiteNameValue}", singleSiteSetting.SiteName);
                        htmlBody = htmlBody.Replace("{NonTimedStartTimeValue}", singleSiteSetting.NonTimeStart);
                        htmlBody = htmlBody.Replace("{NonTimedEndTimeValue}", singleSiteSetting.NonTimeEnd);
                    }
                    else {
                        htmlBody = htmlBody.Replace("{tblVisible}", "none");
                    }
                    #endregion
                }

                string sFromAddress = Convert.ToString(ConfigurationManager.AppSettings["fromAddress"]);

                // Resending of Delivery mails
                oAPPBOK_CommunicationBAL = new APPBOK_CommunicationBAL();
                clsEmail oclsEmail = new clsEmail();

                if (objLanguage.Language.ToLower() == language.ToLower()) {
                    //ProvisionalRefusal                   
                    oclsEmail.sendMail(toAddress, htmlBody, WebCommon.getGlobalResourceValue("ProvisionalRefusal", objLanguage.Language), sFromAddress, true);
                }
                oAPPBOK_CommunicationBAL.AddDeletedItemBAL(BookingID, sFromAddress, toAddress, WebCommon.getGlobalResourceValue("ProvisionalRefusal", objLanguage.Language), htmlBody,
                    Convert.ToInt32(Session["UserID"]), CommunicationType.Enum.ProvisionalRefusal, objLanguage.LanguageID, MailSentInLanguage, MailSentInLanguage);
            }

        }
    }

    private MAS_SiteBE GetSingleSiteSetting(int siteId) {
        MAS_SiteBAL oMAS_SiteBAL = new MAS_SiteBAL();
        MAS_SiteBE oMAS_SiteBE = new MAS_SiteBE();
        oMAS_SiteBE.Action = "GetSingleSiteSetting";
        oMAS_SiteBE.SiteID = siteId;
        MAS_SiteBE localMAS_SiteBE = oMAS_SiteBAL.GetSingleSiteSettingBAL(oMAS_SiteBE);
        return localMAS_SiteBE;
    }
    //-----------End Stage 12 point 2--------------//

    protected void gvExport_RowDataBound(object sender, GridViewRowEventArgs e) {
        GridViewRow gvr = e.Row;
        if (e.Row.RowType == DataControlRowType.DataRow) {
            #region Code to set visibility of [Time / Day] based on [Time Window Id] ..
            ucLiteral UcLiteral27 = (ucLiteral)e.Row.FindControl("UcLiteral27");
            ucLiteral UcLiteral27_1 = (ucLiteral)e.Row.FindControl("UcLiteral27_1");
            ucLiteral UcLiteral27_2 = (ucLiteral)e.Row.FindControl("UcLiteral27_2");
            //HiddenField hdnTimeWindowID = (HiddenField)e.Row.FindControl("hdnTimeWindowID");

            if (!string.IsNullOrEmpty(UcLiteral27_1.Text) && !string.IsNullOrWhiteSpace(UcLiteral27_1.Text)) {
                UcLiteral27_2.Visible = false;
                UcLiteral27_1.Visible = true;
                UcLiteral27.Visible = false;
            }
            else if (!string.IsNullOrEmpty(UcLiteral27_2.Text) && !string.IsNullOrWhiteSpace(UcLiteral27_2.Text)) {
                UcLiteral27_2.Visible = true;
                UcLiteral27_1.Visible = false;
                UcLiteral27.Visible = false;
            }
            else {
                UcLiteral27_2.Visible = false;
                UcLiteral27_1.Visible = false;
                UcLiteral27.Visible = true;
            }
            #endregion
        }
    }

    private void BindBookingPageWise() {
        if (ViewState["lstSites"] != null) {
            DataTable lstBookings = new DataTable();
            lstBookings = (DataTable)ViewState["lstSites"];
            //lstBookings.DefaultView.Sort = "DeliveryDate,OrderBY";
            gvVendor.DataSource = lstBookings;
            gvVendor.DataBind();

            SortGridView(GridViewSortExp, SortDir);
        }
        else {
            BindBookings();
        }
    }

    protected void BindBookings() {
        SearchBookings();
        if (ViewState["lstSites"] != null) {
            DataTable lstBookings = new DataTable();
            lstBookings = (DataTable)ViewState["lstSites"];

            if (lstBookings != null && lstBookings.Rows.Count > 0) {
                GridViewSortExp = "DeliveryDate,OrderBY";
                GridViewSortDirection = SortDirection.Descending;
                SortGridView(GridViewSortExp, SortDir);
            }
        }
    }

    private void SearchBookings() {
        APPBOK_BookingBE oAPPBOK_BookingBE = new APPBOK_BookingBE();
        APPBOK_BookingBAL oAPPBOK_BookingBAL = new APPBOK_BookingBAL();


        oAPPBOK_BookingBE.FixedSlot = new BusinessEntities.ModuleBE.Appointment.SiteSettings.MASSIT_FixedSlotBE();
        oAPPBOK_BookingBE.Carrier = new BusinessEntities.ModuleBE.Appointment.CountrySetting.MASCNT_CarrierBE();
        oAPPBOK_BookingBE.Delivery = new BusinessEntities.ModuleBE.Appointment.CountrySetting.MASCNT_DeliveryTypeBE();
        oAPPBOK_BookingBE.FixedSlot.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();

        oAPPBOK_BookingBE.Action = "GetProvisionalBookingsDetails";
        if (ddlSite.innerControlddlSite.SelectedIndex > 0)
            oAPPBOK_BookingBE.FixedSlot.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
        else
            oAPPBOK_BookingBE.FixedSlot.SiteIDs = ddlSite.UserSiteIDs;

        if (ddlCountry.innerControlddlCountry.SelectedIndex > 0)
            oAPPBOK_BookingBE.Carrier.CountryID = Convert.ToInt32(ddlCountry.innerControlddlCountry.SelectedItem.Value);


        DataTable lstBookings = new DataTable();

        lstBookings = oAPPBOK_BookingBAL.GetProvisionalBookingBAL(oAPPBOK_BookingBE);
        //lstBookings.DefaultView.Sort = "DeliveryDate,OrderBY";

        if (lstBookings != null && lstBookings.Rows.Count > 0) {
            gvVendor.DataSource = lstBookings;
            gvVendor.DataBind();
            gvExport.DataSource = lstBookings;
            gvExport.DataBind();
            ViewState["lstSites"] = lstBookings;
        }
        else {
            gvVendor.DataSource = null;
            gvVendor.DataBind();
            gvExport.DataSource = null;
            gvExport.DataBind();
            ViewState["lstSites"] = null;
        }
    }
}
﻿using BusinessEntities.ModuleBE.AdminFunctions;
using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using BusinessEntities.ModuleBE.Languages.Languages;
using BusinessEntities.ModuleBE.Upload;
using BusinessLogicLayer.ModuleBAL.AdminFunctions;
using BusinessLogicLayer.ModuleBAL.Appointment.Booking;
using BusinessLogicLayer.ModuleBAL.Appointment.SiteSettings;
using BusinessLogicLayer.ModuleBAL.Upload;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using Utilities;

public partial class PurchaseOrderCommunication : CommonPage
{
    string sFromAddress = Convert.ToString(ConfigurationManager.AppSettings["fromAddress"]);
    sendCommunicationCommon oSendCommunicationCommon = new sendCommunicationCommon();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Role"] != null && Session["Role"].ToString() != "")
        {
            if (Session["Role"].ToString() == "Carrier" || Session["Role"].ToString() == "Vendor")
                trCoomunication.Visible = false;
        }

        if (!Page.IsPostBack)
        {

            InitLanguage();

            if (GetQueryStringValue("POID") != null && GetQueryStringValue("VendorID") != null)
            {
                string poID = GetQueryStringValue("POID");
                int vendorID = Convert.ToInt32(GetQueryStringValue("VendorID"));

                SendPOInformation(poID, vendorID);

            }
            else
            {
                ShowErrorMessage();
            }
        }
    }

    public string distictEmails(string Emails)
    {
        string[] arrEMails = Emails.Split(new char[] { ',' });

        List<string> lstEmails = new List<string>();
        foreach (string email in arrEMails)
        {
            if (!string.IsNullOrEmpty(email) && !string.IsNullOrWhiteSpace(email) && !lstEmails.Contains(email.Trim()))
                lstEmails.Add(email.Trim());
        }
        return string.Join(",", lstEmails.ToArray());
    }

    private string EMailContacts { get; set; }

    private string EMailSubject { get; set; }

    private string MailBody { get; set; }

    public string PointOfContactNameFinal {
        get
        {
            return ViewState["PointOfContactNameFinal"] != null ? Convert.ToString(ViewState["PointOfContactNameFinal"])
                : string.Empty;
        }
        set
        {
            ViewState["PointOfContactNameFinal"] = value;
        }
    }
    public string PointOfRoleTitle
    {
        get
        {
            return ViewState["PointOfRoleTitle"] != null ? Convert.ToString(ViewState["PointOfRoleTitle"])
                : string.Empty;
        }
        set
        {
            ViewState["PointOfRoleTitle"] = value;
        }
    }

    public string PointOfPhoneNumber
    {
        get
        {
            return ViewState["PointOfPhoneNumber"] != null ? Convert.ToString(ViewState["PointOfPhoneNumber"])
                : string.Empty;
        }
        set
        {
            ViewState["PointOfPhoneNumber"] = value;
        }
    }


    private StringBuilder EmailContent {
        get
        {
            return ViewState["EmailContent"] as StringBuilder;
        }
        set
        {
            ViewState["EmailContent"] = value;
        }
    }

    private Up_PurchaseOrderDetailBE PurchaseOrderBE { get; set; }

    private decimal POTotalCost { get; set; }

    private string Currency { get; set; }

    private void InitLanguage()
    {
        APPBOK_CommunicationBAL oAPPBOK_CommunicationBAL = new APPBOK_CommunicationBAL();
        List<MAS_LanguageBE> oLanguages = oAPPBOK_CommunicationBAL.GetLanguages();
    }

    private void ShowErrorMessage()
    {
        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "showNoMailToReSend", "alert('" + WebUtilities.WebCommon.getGlobalResourceValue("NoMailToResend") + "');", true);
    }

    protected void btnReSendCommunication_Click(object sender, EventArgs e)
    {
        #region Logic to sending email to Vendors's point of contacts and inserting records in database ...
        var communicationBAL = new APPBOK_CommunicationBAL();
        UP_PurchaseOrderDetailBAL objPurchaseOrderDetailBAL = new UP_PurchaseOrderDetailBAL();
        var lstLanguages = communicationBAL.GetLanguages();
        foreach (BusinessEntities.ModuleBE.Languages.Languages.MAS_LanguageBE objLanguage in lstLanguages)
        {
            PurchaseOrderBE = (Up_PurchaseOrderDetailBE)ViewState["PurchaseOrderObject"];
            var isMailSentInLanguage = false;
            if (objLanguage.Language.Trim().ToUpper() == PurchaseOrderBE.Vendor.Language.Trim().ToUpper())
                isMailSentInLanguage = true;

            var sentFrom = Convert.ToString(Session["LoginID"]);
            var emailSubject = UIUtility.getGlobalResourceValue("ImportNewPOLetterSubject", objLanguage.Language).Replace("{PONumber}", PurchaseOrderBE.Purchase_order);
            var htmlBody = GetEmailTemplateData(objLanguage.Language, PurchaseOrderBE, Convert.ToString(EmailContent), POTotalCost, Currency);

            var purchaseOrderDetailBE = new Up_PurchaseOrderDetailBE();
            purchaseOrderDetailBE.Action = "ManuallyInsertSentPOEmailData";
            purchaseOrderDetailBE.Purchase_order = PurchaseOrderBE.Purchase_order;
            purchaseOrderDetailBE.Site = new BusinessEntities.ModuleBE.AdminFunctions.MAS_SiteBE();
            purchaseOrderDetailBE.SiteID = PurchaseOrderBE.Site.SiteID;
            purchaseOrderDetailBE.StockPlannerID = PurchaseOrderBE.StockPlannerID;
            purchaseOrderDetailBE.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();
            purchaseOrderDetailBE.VendorId = PurchaseOrderBE.Vendor.VendorID;
            purchaseOrderDetailBE.Order_raised = PurchaseOrderBE.Order_raised;
            purchaseOrderDetailBE.Original_due_date = PurchaseOrderBE.Original_due_date;
            purchaseOrderDetailBE.EntryType = "Manual PO";
            purchaseOrderDetailBE.SentFrom = sentFrom;
            purchaseOrderDetailBE.SentTo = Convert.ToString(ViewState["pointEmailContacts"]);
            purchaseOrderDetailBE.Subject = emailSubject;
            purchaseOrderDetailBE.Body = htmlBody;
            purchaseOrderDetailBE.LanguageID = objLanguage.LanguageID;
            purchaseOrderDetailBE.IsMailSentInLanguage = isMailSentInLanguage;
            purchaseOrderDetailBE.CommunicationType = "Manually Sent PO Mail";
            purchaseOrderDetailBE.Warehouse = PurchaseOrderBE.Warehouse;
            purchaseOrderDetailBE.FirstSentToEmail = Convert.ToString(ViewState["firstPointOfConntact"]);
            var SentCommId = objPurchaseOrderDetailBAL.SaveManualSentPOEmailDataBAL(purchaseOrderDetailBE);


            divReSendCommunication.InnerHtml = htmlBody;
            EMailSubject = emailSubject;
            EMailSubject = ViewState["EMailSubject"] as string;
            MailBody = ViewState["MailBody"] as string;

            if (isMailSentInLanguage)
            {
                this.SendMail(txtEmail.Text, MailBody, EMailSubject, sentFrom, true);
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "refreshParent", "alert('Mail Sent Successfully');", true);
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "refreshParent", "window.opener.location.href = window.opener.location.href;", true);
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "close", "this.close();", true);
            }

        }
        ViewState["pointEmailContacts"] = null;
        ViewState["firstPointOfConntact"] = null;
        #endregion
    }

    protected void drpLanguageId_SelectedIndexChanged(object sender, EventArgs e)
    {

    }

    public void SendPOInformation(string purchaseOrder, int vendorID)
    {
        var dataImportSchedulerBAL = new UP_DataImportSchedulerBAL();
        var purchaseOrderDetailBE = new Up_PurchaseOrderDetailBE();

        purchaseOrderDetailBE.Action = "ImportNewPOLetterMainByPO";
        purchaseOrderDetailBE.VendorId = vendorID;
        purchaseOrderDetailBE.Purchase_order = purchaseOrder;

        purchaseOrderDetailBE.Order_raised = !string.IsNullOrEmpty(GetQueryStringValue("OrderRaised")) ? (DateTime?)Common.GetMM_DD_YYYY(GetQueryStringValue("OrderRaised")) : null;

        var lstNewPOsData = dataImportSchedulerBAL.GetImportNewPOLetterMainBAL(purchaseOrderDetailBE);

        if (lstNewPOsData != null && lstNewPOsData.Count > 0)
        {
            PurchaseOrderBE = lstNewPOsData[0] as Up_PurchaseOrderDetailBE;
        }
        else
        {
            ShowErrorMessage();
            return;
        }

        #region Logic to getting here Vendor Point of Contact for particular vendor ...

        var vendorPointsContactBAL = new MAS_MaintainVendorPointsContactBAL();
        var VendorPointsContact = new MAS_MaintainVendorPointsContactBE();
        VendorPointsContact.Action = "GetMaintainVendorPointsContact";
        VendorPointsContact.Vendor = new MAS_VendorBE();
        VendorPointsContact.Vendor.VendorID = PurchaseOrderBE.Vendor.VendorID;
        var lstVendorInvPointsContact = vendorPointsContactBAL.GetVendorPointsContactsBAL(VendorPointsContact).FindAll(x => x.InventoryPOC.Equals("Y"));

        var pointEmailContacts = string.Empty;
        var firstPointOfConntact = string.Empty;
        var PointOfContactName = string.Empty;
        var FinalPointOfContactName = string.Empty;

        foreach (MAS_MaintainVendorPointsContactBE pointContact in lstVendorInvPointsContact)
        {

            if (!string.IsNullOrEmpty(pointContact.EmailAddress) && !string.IsNullOrWhiteSpace(pointContact.EmailAddress))
            {
                if (string.IsNullOrEmpty(pointEmailContacts))
                {
                    pointEmailContacts = firstPointOfConntact = pointContact.EmailAddress;
                }
                else if (!pointEmailContacts.Contains(pointContact.EmailAddress))
                {
                    pointEmailContacts = string.Format("{0},{1}", pointEmailContacts, pointContact.EmailAddress);
                }
            }

            if (!string.IsNullOrEmpty(pointContact.FirstName) && !string.IsNullOrWhiteSpace(pointContact.SecondName))
            { 
                    PointOfContactName = string.Format("{0} {1}", PointOfContactName, pointContact.FirstName + " " + pointContact.SecondName +"<br>");                
            }

            if (!string.IsNullOrEmpty(pointContact.RoleTitle))
            { 
                    PointOfRoleTitle = string.Format("{0} {1}", PointOfRoleTitle, pointContact.RoleTitle  + "<br>");              
            }
            if (!string.IsNullOrEmpty(pointContact.PhoneNumber))
            {
                PointOfPhoneNumber = string.Format("{0} {1}", PointOfPhoneNumber, pointContact.PhoneNumber + "<br>");
            }

        }

        PointOfContactNameFinal= PointOfContactName;

        if (string.IsNullOrEmpty(pointEmailContacts))
            pointEmailContacts = firstPointOfConntact = Convert.ToString(ConfigurationManager.AppSettings["DefaultEmailAddress"]);

        EMailContacts = pointEmailContacts;
        #endregion

        #region Logic to getting here PO line details ...
        purchaseOrderDetailBE = new Up_PurchaseOrderDetailBE();
        purchaseOrderDetailBE.Action = "ImportNewPOLetterDetails";
        purchaseOrderDetailBE.Purchase_order = PurchaseOrderBE.Purchase_order;
        purchaseOrderDetailBE.Warehouse = PurchaseOrderBE.Warehouse;
        purchaseOrderDetailBE.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();
        purchaseOrderDetailBE.Vendor.VendorID = PurchaseOrderBE.Vendor.VendorID;
        purchaseOrderDetailBE.Order_raised = !string.IsNullOrEmpty(GetQueryStringValue("OrderRaised")) ? (DateTime?)Common.GetMM_DD_YYYY(GetQueryStringValue("OrderRaised")) : null;
       
        List<Up_PurchaseOrderDetailBE> lstNewPOsDetails = dataImportSchedulerBAL.GetImportInsertNewSentPOEmailDataBAL(purchaseOrderDetailBE);
        lstNewPOsDetails = lstNewPOsDetails.OrderBy(x => x.Line_No).ToList();

        decimal poTotalCost = 0;
        string currency = string.Empty;
        var poLinesDetails = new StringBuilder();
        foreach (Up_PurchaseOrderDetailBE purchaseDetailsBE in lstNewPOsDetails)
        {
            poTotalCost = Convert.ToDecimal(poTotalCost + purchaseDetailsBE.TotalCost);
            POTotalCost = poTotalCost;

            if (!string.IsNullOrEmpty(purchaseDetailsBE.Currency) && string.IsNullOrEmpty(currency))
                currency = purchaseDetailsBE.Currency;
            Currency = currency;
            #region Logic to creating dynamic PO lines data ...
            poLinesDetails.Append("<tr>");
            poLinesDetails.Append(string.Format("<td style=\"padding-top: 3px; padding-bottom: 3px; border-right: solid 2px #272727; border-left: solid 2px #272727; text-align: center; font-family: Arial, Helvetica, sans-serif; font-size: 12px;\">{0}</td>", !string.IsNullOrEmpty(Convert.ToString(purchaseDetailsBE.Line_No)) ? Convert.ToString(purchaseDetailsBE.Line_No) : "&nbsp;"));
            poLinesDetails.Append(string.Format("<td style=\"padding-top: 3px; padding-bottom: 3px; border-right: solid 2px #272727; text-align: center; font-family: Arial, Helvetica, sans-serif; font-size: 12px;\">{0}</td>", !string.IsNullOrEmpty(Convert.ToString(purchaseDetailsBE.Direct_code)) ? Convert.ToString(purchaseDetailsBE.Direct_code) : "&nbsp;"));
            poLinesDetails.Append(string.Format("<td style=\"padding-top: 3px; padding-bottom: 3px; border-right: solid 2px #272727; text-align: center; font-family: Arial, Helvetica, sans-serif; font-size: 12px;\">{0}</td>", !string.IsNullOrEmpty(Convert.ToString(purchaseDetailsBE.OD_Code)) ? Convert.ToString(purchaseDetailsBE.OD_Code) : "&nbsp;"));
            poLinesDetails.Append(string.Format("<td style=\"padding-top: 3px; padding-bottom: 3px; border-right: solid 2px #272727; text-align: center; font-family: Arial, Helvetica, sans-serif; font-size: 12px;\">{0}</td>", !string.IsNullOrEmpty(Convert.ToString(purchaseDetailsBE.Vendor_Code)) ? Convert.ToString(purchaseDetailsBE.Vendor_Code) : "&nbsp;"));
            poLinesDetails.Append(string.Format("<td style=\"padding-top: 3px; padding-bottom: 3px; border-right: solid 2px #272727; text-align: center; font-family: Arial, Helvetica, sans-serif; font-size: 12px;\">{0}</td>", !string.IsNullOrEmpty(Convert.ToString(purchaseDetailsBE.Product_description)) ? Convert.ToString(purchaseDetailsBE.Product_description) : "&nbsp;"));
            poLinesDetails.Append(string.Format("<td style=\"padding-top: 3px; padding-bottom: 3px; border-right: solid 2px #272727; text-align: center; font-family: Arial, Helvetica, sans-serif; font-size: 12px;\">{0}</td>", !string.IsNullOrEmpty(Convert.ToString(purchaseDetailsBE.Original_quantity)) ? Convert.ToString(purchaseDetailsBE.Original_quantity) : "&nbsp;"));
            poLinesDetails.Append(string.Format("<td style=\"padding-top: 3px; padding-bottom: 3px; border-right: solid 2px #272727; text-align: center; font-family: Arial, Helvetica, sans-serif; font-size: 12px;\">{0}</td>", !string.IsNullOrEmpty(Convert.ToString(purchaseDetailsBE.Outstanding_Qty)) ? Convert.ToString(purchaseDetailsBE.Outstanding_Qty) : "&nbsp;"));
            poLinesDetails.Append(string.Format("<td style=\"padding-top: 3px; padding-bottom: 3px; border-right: solid 2px #272727; text-align: center; font-family: Arial, Helvetica, sans-serif; font-size: 12px;\">{0}</td>", !string.IsNullOrEmpty(Convert.ToString(purchaseDetailsBE.UOM)) ? Convert.ToString(purchaseDetailsBE.UOM) : "&nbsp;"));
            poLinesDetails.Append(string.Format("<td style=\"padding-top: 3px; padding-bottom: 3px; border-right: solid 2px #272727; text-align: center; font-family: Arial, Helvetica, sans-serif; font-size: 12px;\">{0}</td>", !string.IsNullOrEmpty(Convert.ToString(purchaseDetailsBE.PO_cost)) ? Convert.ToString(purchaseDetailsBE.PO_cost) : "&nbsp;"));
            poLinesDetails.Append(string.Format("<td style=\"padding-top: 3px; padding-bottom: 3px; border-right: solid 2px #272727; text-align: center; font-family: Arial, Helvetica, sans-serif; font-size: 12px;\">{0}</td>", !string.IsNullOrEmpty(Convert.ToString(purchaseDetailsBE.TotalCost)) ? Convert.ToString(purchaseDetailsBE.TotalCost) : "&nbsp;"));
            DateTime deliveryDate = Convert.ToDateTime(purchaseDetailsBE.Original_due_date);
            poLinesDetails.Append(string.Format("<td style=\"padding-top: 3px; padding-bottom: 3px; text-align: center; font-family: Arial, Helvetica, sans-serif; font-size: 12px;\">{0}</td>", String.Format("{0:dd/MM/yyyy}", deliveryDate)));
            poLinesDetails.Append("</tr>");

            EmailContent = poLinesDetails;
            #endregion
        }
        #endregion

        #region Logic to sending email to Vendors's point of contacts and inserting records in database ...
        var communicationBAL = new APPBOK_CommunicationBAL();
        var lstLanguages = communicationBAL.GetLanguages();
        foreach (BusinessEntities.ModuleBE.Languages.Languages.MAS_LanguageBE objLanguage in lstLanguages)
        {
            var isMailSentInLanguage = false;
            if (objLanguage.Language.Trim().ToUpper() == PurchaseOrderBE.Vendor.Language.Trim().ToUpper())
                isMailSentInLanguage = true;
            ViewState["PurchaseOrderObject"] = PurchaseOrderBE;

            var sentFrom = Convert.ToString(ConfigurationManager.AppSettings["fromAddress"]);
            var emailSubject = UIUtility.getGlobalResourceValue("ImportNewPOLetterSubject", objLanguage.Language).Replace("{PONumber}", PurchaseOrderBE.Purchase_order);

            var htmlBody = GetEmailTemplateData(objLanguage.Language, PurchaseOrderBE, Convert.ToString(EmailContent), POTotalCost, Currency);

            string LogoPath = Convert.ToString(ConfigurationManager.AppSettings["Portallink"]);
            string absolutePath = LogoPath + "/Images/OfficeDepotLogo.jpg";
            htmlBody = htmlBody.Replace("{logoInnerPath}", absolutePath);

            divReSendCommunication.InnerHtml = htmlBody;
            txtEmail.Text = EMailContacts;
            ViewState["EMailSubject"] = emailSubject;
            ViewState["MailBody"] = htmlBody;
            ViewState["firstPointOfConntact"] = firstPointOfConntact;
            ViewState["pointEmailContacts"] = pointEmailContacts;

            if (isMailSentInLanguage)
            {
                break;
            }
        }
        #endregion

    }

    private string GetEmailTemplateData(string language, Up_PurchaseOrderDetailBE poMain, string poLineItemsDynamicTRs, decimal poTotalCost, string currency)
    {
        string path = AppDomain.CurrentDomain.BaseDirectory.ToLower();
        path = path.Replace(@"\bin\debug", "");
        string templatesPath = path + @"emailtemplates\";
        string templateFile = templatesPath + @"\Appointment\PurchaseOrderLetter.english.htm";

        string htmlBody = string.Empty;

        if (System.IO.File.Exists(templateFile.ToLower()))
        {
            using (StreamReader sReader = new StreamReader(templateFile.ToLower()))
            {
                htmlBody = sReader.ReadToEnd();
                htmlBody = htmlBody.Replace("{DearSirMadam}", UIUtility.getGlobalResourceValue("DearSirMadam", language));
                htmlBody = htmlBody.Replace("{PurchaseOrderLetter1}", UIUtility.getGlobalResourceValue("PurchaseOrderLetter1", language)).Replace("today", "");
                htmlBody = htmlBody.Replace("{PurchaseOrderLetter2}", UIUtility.getGlobalResourceValue("PurchaseOrderLetter2", language));
                htmlBody = htmlBody.Replace("{PurchaseOrderLetter3}", UIUtility.getGlobalResourceValue("PurchaseOrderLetter3", language));
                htmlBody = htmlBody.Replace("{Yoursfaithfully}", UIUtility.getGlobalResourceValue("Yoursfaithfully", language));

                if (string.IsNullOrEmpty(poMain.StockPlannerName) && string.IsNullOrEmpty(poMain.StockPlannerContactNo))
                {
                    htmlBody = htmlBody.Replace("{StockPlannerNameValue}", poMain.Buyer_no);
                }

                htmlBody = !string.IsNullOrEmpty(poMain.StockPlannerName)
                    ? htmlBody.Replace("{StockPlannerNameValue}", poMain.StockPlannerName)
                    : htmlBody.Replace("{StockPlannerNameValue}", "&nbsp;");

                htmlBody = htmlBody.Replace("{StockPlanner}", UIUtility.getGlobalResourceValue("StockPlanner", language));

                if (!string.IsNullOrEmpty(poMain.StockPlannerContactNo))
                {
                    htmlBody = htmlBody.Replace("{StockPlannerNumberValue}", poMain.StockPlannerContactNo);
                }
                else
                {
                    htmlBody = htmlBody.Replace("(StockPlannerNumberValue}", "&nbsp;");
                    htmlBody = htmlBody.Replace("({StockPlannerNumberValue})", "&nbsp;");
                }

                htmlBody = htmlBody.Replace("{PurchaseOrder}", UIUtility.getGlobalResourceValue("PurchaseOrder", language));
                htmlBody = htmlBody.Replace("{PurchaseOrderValue}", poMain.Purchase_order);

                htmlBody = htmlBody.Replace("{TOInCaps}", UIUtility.getGlobalResourceValue("TOInCaps", language));
                htmlBody = htmlBody.Replace("{DELIVERTOInCaps}", UIUtility.getGlobalResourceValue("DELIVERTOInCaps", language));
                htmlBody = htmlBody.Replace("{INVOICETOInCaps}", UIUtility.getGlobalResourceValue("INVOICETOInCaps", language));

                htmlBody = htmlBody.Replace("{DELIVERYInCaps}", UIUtility.getGlobalResourceValue("DELIVERYInCaps", language));
                htmlBody = htmlBody.Replace("{PAYMENTTERMSInCaps}", UIUtility.getGlobalResourceValue("PAYMENTTERMSInCaps", language));

                if (!string.IsNullOrEmpty(poMain.Vendor.DeliveryInstructions))
                    htmlBody = htmlBody.Replace("{DELIVERYValue}", poMain.Vendor.DeliveryInstructions);
                else
                    htmlBody = htmlBody.Replace("{DELIVERYValue}", "&nbsp;");
                if (!string.IsNullOrEmpty(poMain.Vendor.PaymentTerms))
                    htmlBody = htmlBody.Replace("{PAYMENTTERMSValue}", poMain.Vendor.PaymentTerms);
                else
                    htmlBody = htmlBody.Replace("{PAYMENTTERMSValue}", "&nbsp;");

                htmlBody = htmlBody.Replace("{FUNCTIONInCaps}", UIUtility.getGlobalResourceValue("FUNCTIONInCaps", language));
                htmlBody = htmlBody.Replace("{PHONENUMBERInCaps}", UIUtility.getGlobalResourceValue("PHONENUMBERInCaps", language));

                htmlBody = poMain.VendorPOC !=null && !string.IsNullOrEmpty(PointOfRoleTitle)
                    ? htmlBody.Replace("{FUNCTIONValue}", PointOfRoleTitle)
                    : htmlBody.Replace("{FUNCTIONValue}", "&nbsp;");

                htmlBody = poMain.VendorPOC!=null && !string.IsNullOrEmpty(PointOfPhoneNumber)
                    ? htmlBody.Replace("{PHONENUMBERValue}", PointOfPhoneNumber)
                    : htmlBody.Replace("{PHONENUMBERValue}", "&nbsp;");

                var supplierAddress = string.Empty;
                if (!string.IsNullOrEmpty(poMain.Vendor.VendorName) && !string.IsNullOrWhiteSpace(poMain.Vendor.VendorName))
                    supplierAddress = string.Format("{0}", poMain.Vendor.VendorName);

                if (!string.IsNullOrEmpty(poMain.Vendor.address1) && !string.IsNullOrWhiteSpace(poMain.Vendor.address1))
                    supplierAddress = string.Format("{0},<br />{1}", supplierAddress, poMain.Vendor.address1);

                if (!string.IsNullOrEmpty(poMain.Vendor.address2) && !string.IsNullOrWhiteSpace(poMain.Vendor.address2))
                    supplierAddress = string.Format("{0},<br />{1}", supplierAddress, poMain.Vendor.address2);

                if (!string.IsNullOrEmpty(poMain.Vendor.city) && !string.IsNullOrWhiteSpace(poMain.Vendor.city))
                    supplierAddress = string.Format("{0},<br />{1}", supplierAddress, poMain.Vendor.city);

                if (!string.IsNullOrEmpty(poMain.Vendor.county) && !string.IsNullOrWhiteSpace(poMain.Vendor.county))
                    supplierAddress = string.Format("{0},<br />{1} {2}", supplierAddress, poMain.Vendor.county, poMain.Vendor.VMPPIN);

                htmlBody = htmlBody.Replace("{SupplierAddressValue}", supplierAddress);

                var odDeliveryAddress = string.Empty;
                if (!string.IsNullOrEmpty(poMain.Site.SiteAddressLine1) && !string.IsNullOrWhiteSpace(poMain.Site.SiteAddressLine1))
                    odDeliveryAddress = string.Format("{0}", poMain.Site.SiteAddressLine1);

                if (!string.IsNullOrEmpty(poMain.Site.SiteAddressLine2) && !string.IsNullOrWhiteSpace(poMain.Site.SiteAddressLine2))
                    odDeliveryAddress = string.Format("{0},<br />{1}", odDeliveryAddress, poMain.Site.SiteAddressLine2);

                if (!string.IsNullOrEmpty(poMain.Site.SiteAddressLine3) && !string.IsNullOrWhiteSpace(poMain.Site.SiteAddressLine3))
                    odDeliveryAddress = string.Format("{0},<br />{1}", odDeliveryAddress, poMain.Site.SiteAddressLine3);

                if (!string.IsNullOrEmpty(poMain.Site.SiteAddressLine4) && !string.IsNullOrWhiteSpace(poMain.Site.SiteAddressLine4))
                    odDeliveryAddress = string.Format("{0},<br />{1}", odDeliveryAddress, poMain.Site.SiteAddressLine4);

                if (!string.IsNullOrEmpty(poMain.Site.SiteAddressLine5) && !string.IsNullOrWhiteSpace(poMain.Site.SiteAddressLine5))
                    odDeliveryAddress = string.Format("{0},<br />{1}", odDeliveryAddress, poMain.Site.SiteAddressLine5);

                if (!string.IsNullOrEmpty(poMain.Site.SiteAddressLine6) && !string.IsNullOrWhiteSpace(poMain.Site.SiteAddressLine6))
                    odDeliveryAddress = string.Format("{0},<br />{1}", odDeliveryAddress, poMain.Site.SiteAddressLine6);

                if (!string.IsNullOrEmpty(poMain.Site.SiteCountryName) && !string.IsNullOrWhiteSpace(poMain.Site.SitePincode))
                    odDeliveryAddress = string.Format("{0},<br />{1} {2}", odDeliveryAddress, poMain.Site.SiteCountryName, poMain.Site.SitePincode);
                else
                    odDeliveryAddress = string.Format("{0},<br/>{1}", odDeliveryAddress, poMain.Site.SitePincode);

                htmlBody = htmlBody.Replace("{OfficeDepotDeliveryAddressValue}", odDeliveryAddress);

                var apAddress = string.Empty;
                if (!string.IsNullOrEmpty(poMain.Site.APAddressLine1) && !string.IsNullOrWhiteSpace(poMain.Site.APAddressLine1))
                    apAddress = string.Format("{0}", poMain.Site.APAddressLine1);

                if (!string.IsNullOrEmpty(poMain.Site.APAddressLine2) && !string.IsNullOrWhiteSpace(poMain.Site.APAddressLine2))
                    apAddress = string.Format("{0},<br />{1}", apAddress, poMain.Site.APAddressLine2);

                if (!string.IsNullOrEmpty(poMain.Site.APAddressLine3) && !string.IsNullOrWhiteSpace(poMain.Site.APAddressLine3))
                    apAddress = string.Format("{0},<br />{1}", apAddress, poMain.Site.APAddressLine3);

                if (!string.IsNullOrEmpty(poMain.Site.APAddressLine4) && !string.IsNullOrWhiteSpace(poMain.Site.APAddressLine4))
                    apAddress = string.Format("{0},<br />{1}", apAddress, poMain.Site.APAddressLine4);

                if (!string.IsNullOrEmpty(poMain.Site.APAddressLine5) && !string.IsNullOrWhiteSpace(poMain.Site.APAddressLine5))
                    apAddress = string.Format("{0},<br />{1}", apAddress, poMain.Site.APAddressLine5);

                if (!string.IsNullOrEmpty(poMain.Site.APAddressLine6) && !string.IsNullOrWhiteSpace(poMain.Site.APAddressLine6))
                    apAddress = string.Format("{0},<br />{1}", apAddress, poMain.Site.APAddressLine6);

                //if (string.IsNullOrEmpty(poMain.Site.APAddressLine6) && string.IsNullOrWhiteSpace(poMain.Site.APAddressLine6))
                //    if (string.IsNullOrEmpty(poMain.Site.APCountry) && string.IsNullOrWhiteSpace(poMain.Site.APCountry))
                //        apAddress = string.Format("{0}<br />{1}", apAddress, poMain.Site.APCountry);

                if (!string.IsNullOrWhiteSpace(poMain.Site.APPincode))
                    apAddress = string.Format("{0}<br />{1} ", apAddress, poMain.Site.APPincode);
               
                htmlBody = htmlBody.Replace("{APLocationLinkedWithDeliverySiteValue}", apAddress);
                htmlBody = htmlBody.Replace("{FORATTENTIONOFInCaps}", UIUtility.getGlobalResourceValue("FORATTENTIONOFInCaps", language));
                htmlBody = htmlBody.Replace("{BUYERInCaps}", UIUtility.getGlobalResourceValue("BUYERInCaps", language));
                htmlBody = htmlBody.Replace("{PURCHASEORDERNUMBERInCaps}", UIUtility.getGlobalResourceValue("PURCHASEORDERNUMBERInCaps", language));
                htmlBody = htmlBody.Replace("{DATEORDERWASRAISEDInCaps}", UIUtility.getGlobalResourceValue("DATEORDERWASRAISEDInCaps", language));
                 
                htmlBody = htmlBody.Replace("{PurchaseOrderLetter4}",Convert.ToString(PointOfContactNameFinal));

                htmlBody = !string.IsNullOrEmpty(poMain.StockPlannerContactNo)
                    ? htmlBody.Replace("{StockPlannerNumberValue}", poMain.StockPlannerContactNo)
                    : htmlBody.Replace("{StockPlannerNumberValue}", "&nbsp;");

                htmlBody = htmlBody.Replace("{PurchaseOrderValue}", poMain.Purchase_order);

                DateTime orderRaised = !string.IsNullOrEmpty(GetQueryStringValue("OrderRaised"))
                    ? Common.GetMM_DD_YYYY(GetQueryStringValue("OrderRaised"))
                    : Convert.ToDateTime(poMain.Order_raised);
                htmlBody = htmlBody.Replace("{PODDMonthYearValue}", String.Format("{0:dd MMMMM yyyy}", orderRaised));

                htmlBody = htmlBody.Replace("{LINEInCaps}", UIUtility.getGlobalResourceValue("LINEInCaps", language));
                htmlBody = htmlBody.Replace("{VIKINGCODEInCaps}", UIUtility.getGlobalResourceValue("VIKINGCODEInCaps", language));
                htmlBody = htmlBody.Replace("{OFFICEDEPOTCODEInCaps}", UIUtility.getGlobalResourceValue("OFFICEDEPOTCODEInCaps", language));
                htmlBody = htmlBody.Replace("{VENDORCODEInCaps}", UIUtility.getGlobalResourceValue("VENDORCODEInCaps", language));
                htmlBody = htmlBody.Replace("{ITEMDESCRIPTIONInCaps}", UIUtility.getGlobalResourceValue("ITEMDESCRIPTIONInCaps", language));
                htmlBody = htmlBody.Replace("{UNITSREQUIREDInCaps}", UIUtility.getGlobalResourceValue("OriginalQty", language));
                htmlBody = htmlBody.Replace("{OutStandingQtyInCaps}", UIUtility.getGlobalResourceValue("OutstandingQty", language));
                htmlBody = htmlBody.Replace("{UNITInCaps}", UIUtility.getGlobalResourceValue("UNITInCaps", language));
                htmlBody = htmlBody.Replace("{UNITCOSTInCaps}", UIUtility.getGlobalResourceValue("UNITCOSTInCaps", language));
                htmlBody = htmlBody.Replace("{TOTALCOSTInCaps}", UIUtility.getGlobalResourceValue("TOTALCOSTInCaps", language));
                htmlBody = htmlBody.Replace("{DELIVERYDATEInCaps}", UIUtility.getGlobalResourceValue("DELIVERYDATEInCaps", language));

                htmlBody = htmlBody.Replace("{POLineItemsDynamicTRs}", poLineItemsDynamicTRs);

                htmlBody = htmlBody.Replace("{TOTALCOSTInCaps}", UIUtility.getGlobalResourceValue("TOTALCOSTInCaps", language));
                htmlBody = htmlBody.Replace("{CURRENCYInCaps}", UIUtility.getGlobalResourceValue("CURRENCYInCaps", language));
                htmlBody = htmlBody.Replace("{IMPORTANTNOTICEInCaps}", UIUtility.getGlobalResourceValue("IMPORTANTNOTICEInCaps", language));
                htmlBody = htmlBody.Replace("{PurchaseOrderLetter5}", UIUtility.getGlobalResourceValue("PurchaseOrderLetter5", language));
                htmlBody = htmlBody.Replace("{PurchaseOrderLetter6}", UIUtility.getGlobalResourceValue("PurchaseOrderLetter6", language));
                htmlBody = htmlBody.Replace("{PurchaseOrderLetter7}", UIUtility.getGlobalResourceValue("PurchaseOrderLetter7", language));
                htmlBody = htmlBody.Replace("{PurchaseOrderLetter8}", UIUtility.getGlobalResourceValue("PurchaseOrderLetter8", language));
                htmlBody = htmlBody.Replace("{TOTALInCaps}", UIUtility.getGlobalResourceValue("TOTALInCaps", language));

                htmlBody = htmlBody.Replace("{TotalValue}", Convert.ToString(poTotalCost));
                htmlBody = htmlBody.Replace("{CurrencyValue}", currency);

                htmlBody = htmlBody.Replace("{CONDITIONSOFPURCHASEInCaps}", UIUtility.getGlobalResourceValue("CONDITIONSOFPURCHASEInCaps", language));
                htmlBody = htmlBody.Replace("{PurchaseOrderLetter9}", UIUtility.getGlobalResourceValue("PurchaseOrderLetter9", language));
                htmlBody = htmlBody.Replace("{PurchaseOrderLetter10}", UIUtility.getGlobalResourceValue("PurchaseOrderLetter10", language));
                htmlBody = htmlBody.Replace("{Snooty}", Convert.ToString(poMain.Snooty));

                htmlBody = !string.IsNullOrEmpty(poMain.Snooty) ? htmlBody.Replace("{IsSnooty}", "block") : htmlBody.Replace("{IsSnooty}", "none");
            }
        }
        return htmlBody;
    }

    private string GetVendorContact(int VendorID)
    {
        MASSIT_VendorBE oMASSIT_VendorBE = new MASSIT_VendorBE();
        APPSIT_VendorBAL oAPPSIT_VendorBAL = new APPSIT_VendorBAL();
        List<MASSIT_VendorBE> lstVendorDetails = null;
        StringBuilder VendorName = new StringBuilder();
        oMASSIT_VendorBE.Action = "GetContactDetails"; 
        oMASSIT_VendorBE.SiteVendorID = VendorID;
        lstVendorDetails = oAPPSIT_VendorBAL.GetVendorContactDetailsBAL(oMASSIT_VendorBE);

        foreach (MASSIT_VendorBE item in lstVendorDetails)
        {
            if (!string.IsNullOrEmpty(item.Vendor.VendorName))
            {
                VendorName.Append(item.Vendor.VendorName.ToString() + ",");
            }
        }

        string vendor= VendorName.ToString().Length >0 ? VendorName.Remove(VendorName.Length - 1, 1).ToString() :string.Empty;
        return vendor; 
    }

    void SendMail(string toAddress, string mailBody, string mailSubject, string fromAddress, bool bIsHTMLMail)
    {
        try
        {
            clsEmail oclsEmail = new clsEmail();
            string[] toaddress = toAddress.Split(',');

            foreach (string address in toaddress)
                oclsEmail.sendMail(address, mailBody, mailSubject, fromAddress, true);

            StringBuilder sbMessage = new StringBuilder();
            sbMessage.Append("\r\n");
            sbMessage.Append("Mail successfully sent to " + toAddress);
            sbMessage.Append("\r\n");
            LogUtility.SaveTraceLogEntry(sbMessage);
        }
        catch (Exception ex)
        {
            StringBuilder sbMessage = new StringBuilder();
            sbMessage.Append("\r\n");
            sbMessage.Append("Mail sent Error ");
            sbMessage.Append("\r\n");
            LogUtility.SaveErrorLogEntry(ex, "Mail failed for: " + toAddress);
        }
    }

}
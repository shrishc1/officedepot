﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    CodeFile="APPBOK_Option.aspx.cs" Inherits="APPBOK_Option" %>
    <%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="right-shadow">
        <div class="formbox">
            <cc1:ucmultiview id="mvBookingEdit" runat="server" activeviewindex="0">
                <cc1:ucView ID="vwBookingType" runat="server">
                    <cc1:ucPanel ID="pnlSupplierType" runat="server" GroupingText="Supplier Type" CssClass="fieldset-form">
                        <table width="50%" cellspacing="5" align="center" cellpadding="0" border="0" class="top-settingsNoBorder">
                            <tr>
                                <td align="center">
                                    <cc1:ucButton ID="btnVendor" runat="server" Text="Vendor" CssClass="button" OnClick="btnVendor_Click" />
                                    &nbsp;&nbsp;
                                    <cc1:ucButton ID="btnCarrier" runat="server" Text="Carrier" CssClass="button" OnClick="btnCarrier_Click" />
                                </td>
                            </tr>
                        </table>
                    </cc1:ucPanel>
                </cc1:ucView>
                </cc1:ucmultiview>
        </div>
    </div>
</asp:Content>

﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using BaseControlLibrary;
using BusinessEntities.ModuleBE.AdminFunctions;
using BusinessEntities.ModuleBE.Appointment.Booking;
using BusinessEntities.ModuleBE.Appointment.CountrySetting;
using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using BusinessEntities.ModuleBE.Languages.Languages;
using BusinessEntities.ModuleBE.OnTimeInFull.CountrySetting;
using BusinessEntities.ModuleBE.Upload;
using BusinessLogicLayer.ModuleBAL.AdminFunctions;
using BusinessLogicLayer.ModuleBAL.Appointment.Booking;
using BusinessLogicLayer.ModuleBAL.Appointment.CountrySetting;
using BusinessLogicLayer.ModuleBAL.Appointment.SiteSettings;
using BusinessLogicLayer.ModuleBAL.OnTimeInFull.CountrySetting;
using BusinessLogicLayer.ModuleBAL.Upload;
using Utilities;
using WebUtilities;
using System.Drawing;
using System.Text;

public partial class APPBok_WindowBookingEdit : CommonPage
{
    bool isDoorTypeNotChanged = false;
    bool isDoorCheckPopup = false;
    protected string BK_ALT_CorrectVendor = WebCommon.getGlobalResourceValue("BK_ALT_CorrectVendor");
    protected string BK_ALT_Wait = WebCommon.getGlobalResourceValue("BK_ALT_Wait");
    protected string BK_ALT_04_INT = WebCommon.getGlobalResourceValue("BK_MS_04_INT");
    protected string BK_ALT_TimeNotAvailable = WebCommon.getGlobalResourceValue("BK_ALT_TimeNotAvailable");
    protected string BK_ALT_MoveBooking = WebCommon.getGlobalResourceValue("BK_ALT_MoveBooking");
    protected string BK_ALT_Selected_Time = WebCommon.getGlobalResourceValue("BK_ALT_Selected_Time");
    protected string BK_ALT_Selected_Door = WebCommon.getGlobalResourceValue("BK_ALT_Selected_Door");
    protected string BK_ALT_Insufficeint_Space_Ext = WebCommon.getGlobalResourceValue("BK_ALT_Insufficeint_Space_Ext");
    protected string BK_ALT_Insufficeint_Space_Int = WebCommon.getGlobalResourceValue("BK_ALT_Insufficeint_Space_Int");
    protected string AtDoorName = WebCommon.getGlobalResourceValue("AtDoorName");
    string IsBookingEditWithMail = "true";
    //--- Sprint 3b ---Slot time length---// 
    //public int iSlotTimeLength = 5;
    //-----------------------------------//

    string sPortalLink = Convert.ToString(ConfigurationManager.AppSettings["Portallink"]) + @"/ModuleUI/Security/Login.aspx";
    string path = AppDomain.CurrentDomain.BaseDirectory.ToLower();
    string templatePathName = @"/EmailTemplates/Appointment/";

    public string ucSeacrhVendorNo
    {
        get
        {
            return ViewState["vw_ucSeacrhVendorNo"] != null ? ViewState["vw_ucSeacrhVendorNo"].ToString() : string.Empty;
        }
        set
        {
            ViewState["vw_ucSeacrhVendorNo"] = value;
        }
    }

    public string ucSeacrhVendorName
    {
        get
        {
            return ViewState["vw_ucSeacrhVendorName"] != null ? ViewState["vw_ucSeacrhVendorName"].ToString() : string.Empty;
        }
        set
        {
            ViewState["vw_ucSeacrhVendorName"] = value;
        }
    }

    public string ucSeacrhVendorID
    {
        get
        {
            return ViewState["ucSeacrhVendorID"] != null ? ViewState["ucSeacrhVendorID"].ToString() : string.Empty;
        }
        set
        {
            ViewState["ucSeacrhVendorID"] = value;
        }
    }

    public string ucSeacrhVendorParentID
    {
        get
        {
            return ViewState["ucSeacrhVendorParentID"] != null ? ViewState["ucSeacrhVendorParentID"].ToString() : string.Empty;
        }
        set
        {
            ViewState["ucSeacrhVendorParentID"] = value;
        }
    }

    public bool? IsBookingValidationExcluded
    {
        get
        {
            return (Convert.ToBoolean(IsBookingValidationExcludedVendor) || Convert.ToBoolean(IsBookingValidationExcludedDelivery));
        }
        set
        {
            ViewState["IsBookingValidationExcluded"] = value;
        }
    }

    public bool? IsBookingValidationExcludedVendor
    {
        get
        {
            return ViewState["IsBookingValidationExcludedVendor"] != null ? Convert.ToBoolean(ViewState["IsBookingValidationExcludedVendor"]) : false;
        }
        set
        {
            ViewState["IsBookingValidationExcludedVendor"] = value;
        }
    }

    public bool? IsBookingValidationExcludedDelivery
    {
        get
        {
            return ViewState["IsBookingValidationExcludedDelivery"] != null ? Convert.ToBoolean(ViewState["IsBookingValidationExcludedDelivery"]) : false;
        }
        set
        {
            ViewState["IsBookingValidationExcludedDelivery"] = value;
        }
    }

    public bool? IsBookingValidationExcludedCarrier
    {
        get
        {
            return ViewState["IsBookingValidationExcludedCarrier"] != null ? Convert.ToBoolean(ViewState["IsBookingValidationExcludedCarrier"]) : false;
        }
        set
        {
            ViewState["IsBookingValidationExcludedCarrier"] = value;
        }
    }

    public bool? IsPreAdviseNoteRequired
    {
        get
        {
            return ViewState["IsPreAdviseNoteRequired"] != null ? Convert.ToBoolean(ViewState["IsPreAdviseNoteRequired"]) : false;
        }
        set
        {
            ViewState["IsPreAdviseNoteRequired"] = value;
        }
    }

    //--- Start Stage 9 Point 2a/2b---//
    public bool? IsProvisionalBooking
    {
        get
        {
            return ViewState["IsProvisionalBooking"] != null ? Convert.ToBoolean(ViewState["IsProvisionalBooking"]) : false;
        }
        set
        {
            ViewState["IsProvisionalBooking"] = value;
        }
    }
    //--- End Stage 9 Point 2a/2b---//

    public bool? IsNonStandardWindowSlot
    {
        get
        {
            return ViewState["IsNonTimeBooking"] != null ? Convert.ToBoolean(ViewState["IsNonTimeBooking"]) : false;
        }
        set
        {
            ViewState["IsNonTimeBooking"] = value;
        }
    }

    public bool? IsNonTimeVendor
    {
        get
        {
            return ViewState["IsNonTimeVendor"] != null ? Convert.ToBoolean(ViewState["IsNonTimeVendor"]) : false;
        }
        set
        {
            ViewState["IsNonTimeVendor"] = value;
        }
    }

    public bool? isPostBack
    {
        get
        {
            return ViewState["isPostBack"] != null ? Convert.ToBoolean(ViewState["isPostBack"]) : false;
        }
        set
        {
            ViewState["isPostBack"] = value;
        }
    }

    public string strDeliveryValidationExc
    {
        get
        {
            return ViewState["strDeliveryValidationExc"] != null ? ViewState["strDeliveryValidationExc"].ToString() : string.Empty;
        }
        set
        {
            ViewState["strDeliveryValidationExc"] = value;
        }
    }

    public string strDeliveryNonTime
    {
        get
        {
            return ViewState["strDeliveryNonTime"] != null ? ViewState["strDeliveryNonTime"].ToString() : string.Empty;
        }
        set
        {
            ViewState["strDeliveryNonTime"] = value;
        }
    }

    public string CarrierNarrativeRequired
    {
        get
        {
            return ViewState["CarrierNarrativeRequired"] != null ? ViewState["CarrierNarrativeRequired"].ToString() : string.Empty;
        }
        set
        {
            ViewState["CarrierNarrativeRequired"] = value;
        }
    }

    public string strVehicleDoorTypeMatrix
    {
        get
        {
            return ViewState["strVehicleDoorTypeMatrix"] != null ? ViewState["strVehicleDoorTypeMatrix"].ToString() : string.Empty;
        }
        set
        {
            ViewState["strVehicleDoorTypeMatrix"] = value;
        }
    }

    public string strVehicleDoorTypeMatrixSKU
    {
        get
        {
            return ViewState["strVehicleDoorTypeMatrixSKU"] != null ? ViewState["strVehicleDoorTypeMatrixSKU"].ToString() : string.Empty;
        }
        set
        {
            ViewState["strVehicleDoorTypeMatrixSKU"] = value;
        }
    }

    public string VehicleNarrativeRequired
    {
        get
        {
            return ViewState["VehicleNarrativeRequired"] != null ? ViewState["VehicleNarrativeRequired"].ToString() : string.Empty;
        }
        set
        {
            ViewState["VehicleNarrativeRequired"] = value;
        }
    }

    public int? PreSiteCountryID
    {
        get
        {
            return ViewState["PreSiteCountryID"] != null ? Convert.ToInt32(ViewState["PreSiteCountryID"].ToString()) : (int?)null;
        }
        set
        {
            ViewState["PreSiteCountryID"] = value;
        }
    }

    public NameValueCollection MsgSupressed
    {
        get
        {
            return ViewState["MsgSupressed"] != null ? (NameValueCollection)ViewState["MsgSupressed"] : null;
        }
        set
        {
            ViewState["MsgSupressed"] = value;
        }
    }

    public NameValueCollection ArrDoorTypePriority
    {
        get
        {
            return ViewState["ArrDoorTypePriority"] != null ? (NameValueCollection)ViewState["ArrDoorTypePriority"] : null;
        }
        set
        {
            ViewState["ArrDoorTypePriority"] = value;
        }
    }

    public NameValueCollection ArrDoorTypePrioritySKU
    {
        get
        {
            return ViewState["ArrDoorTypePrioritySKU"] != null ? (NameValueCollection)ViewState["ArrDoorTypePrioritySKU"] : null;
        }
        set
        {
            ViewState["ArrDoorTypePrioritySKU"] = value;
        }
    }

    public string Lines
    {
        get
        {
            return ViewState["Lines"] != null ? ViewState["Lines"].ToString() : "XX";
        }
        set
        {
            ViewState["Lines"] = value;
        }
    }

    public int? NonTimeDeliveryCartonVolume
    {
        get
        {
            return ViewState["NonTimeDeliveryCartonVolume"] != null ? Convert.ToInt32(ViewState["NonTimeDeliveryCartonVolume"].ToString()) : (int?)null;
        }
        set
        {
            ViewState["NonTimeDeliveryCartonVolume"] = value;
        }
    }

    public int? NonTimeDeliveryPalletVolume
    {
        get
        {
            return ViewState["NonTimeDeliveryPalletVolume"] != null ? Convert.ToInt32(ViewState["NonTimeDeliveryPalletVolume"].ToString()) : (int?)null;
        }
        set
        {
            ViewState["NonTimeDeliveryPalletVolume"] = value;
        }
    }

    public string BeforeSlotTimeID
    {
        get
        {
            return ViewState["BeforeSlotTimeID"] != null ? ViewState["BeforeSlotTimeID"].ToString() : null;
        }
        set
        {
            ViewState["BeforeSlotTimeID"] = value;
        }
    }

    public string AfterSlotTimeID
    {
        get
        {
            return ViewState["AfterSlotTimeID"] != null ? ViewState["AfterSlotTimeID"].ToString() : null;
        }
        set
        {
            ViewState["AfterSlotTimeID"] = value;
        }
    }

    //----Stage 5 Point 1-----//
    public string CarrierBeforeSlotTimeID
    {
        get
        {
            return ViewState["CarrierBeforeSlotTimeID"] != null ? ViewState["CarrierBeforeSlotTimeID"].ToString() : null;
        }
        set
        {
            ViewState["CarrierBeforeSlotTimeID"] = value;
        }
    }

    public string CarrierAfterSlotTimeID
    {
        get
        {
            return ViewState["CarrierAfterSlotTimeID"] != null ? ViewState["CarrierAfterSlotTimeID"].ToString() : null;
        }
        set
        {
            ViewState["CarrierAfterSlotTimeID"] = value;
        }
    }
    //--------------------------//

    public int BookedPallets
    {
        get
        {
            return ViewState["BookedPallets"] != null ? Convert.ToInt32(ViewState["BookedPallets"]) : 0;
        }
        set
        {
            ViewState["BookedPallets"] = value;
        }
    }

    public int BookedLines
    {
        get
        {
            return ViewState["BookedLines"] != null ? Convert.ToInt32(ViewState["BookedLines"]) : 0;
        }
        set
        {
            ViewState["BookedLines"] = value;
        }
    }

    public DateTime LogicalSchedulDateTime
    {
        get
        {
            return ViewState["LogicalSchedulDateTime"] != null ? Convert.ToDateTime(ViewState["LogicalSchedulDateTime"].ToString()) : new DateTime(1900, 01, 01, 0, 0, 00);
        }
        set
        {
            ViewState["LogicalSchedulDateTime"] = value;
        }
    }

    public DateTime ActualSchedulDateTime
    {
        get
        {
            return ViewState["ActualSchedulDateTime"] != null ? Convert.ToDateTime(ViewState["ActualSchedulDateTime"].ToString()) : new DateTime(1900, 01, 01, 0, 0, 00);
        }
        set
        {
            ViewState["ActualSchedulDateTime"] = value;
        }
    }

    public bool isProvisional
    {
        get
        {
            return ViewState["isProvisional"] != null ? Convert.ToBoolean(ViewState["isProvisional"]) : false;
        }
        set
        {
            ViewState["isProvisional"] = value;
        }
    }

    public string ProvisionalReason
    {
        get
        {
            return ViewState["ProvisionalReason"] != null ? Convert.ToString(ViewState["ProvisionalReason"]) : "";
        }
        set
        {
            ViewState["ProvisionalReason"] = value;
        }
    }

    //Sprint 1 - Point 7 - Start
    public string AlreadyExistsPOs
    {
        get
        {
            return ViewState["AlreadyExistsPOs"] != null ? Convert.ToString(ViewState["AlreadyExistsPOs"]) : string.Empty;
        }
        set
        {
            ViewState["AlreadyExistsPOs"] = value;
        }
    }
    //Sprint 1 - Point 7 - End

    public string SortDirection
    {
        get
        {
            return ViewState["SortDirection"] != null ? Convert.ToString(ViewState["SortDirection"]) : string.Empty;
        }
        set
        {
            ViewState["SortDirection"] = value;
        }
    }

    //----Stage 10 Point 10 L 5.1-----//
    public string IsVehicleContainer
    {
        get
        {
            return ViewState["IsVehicleContainer"] != null ? ViewState["IsVehicleContainer"].ToString() : string.Empty;
        }
        set
        {
            ViewState["IsVehicleContainer"] = value;
        }
    }

    public bool isContainerError
    {
        get
        {
            return ViewState["isContainerError"] != null ? Convert.ToBoolean(ViewState["isContainerError"]) : false;
        }
        set
        {
            ViewState["isContainerError"] = value;
        }
    }
    //---------------------------------------------//


    public enum BookingType
    {
        //FixedSlot = 1, Unscheduled = 2, NonTimedDelivery = 3, BookedSlot = 4,         
        ProvisionalNonTimedDelivery = 8, //--- Stage 9 Point 2a/2b-----//
        Provisional = 5, WindowSlot = 6, NonStandardWindowSlot = 7
    };

    IDictionary<string, VendorDetails> SlotDoorCounter = new Dictionary<string, VendorDetails>();

    public int? RemainingPallets
    {
        get
        {
            return ViewState["RemainingPallets"] != null ? Convert.ToInt32(ViewState["RemainingPallets"]) : (int?)null;
        }
        set
        {
            ViewState["RemainingPallets"] = value;
        }
    }

    public int? RemainingLines
    {
        get
        {
            return ViewState["RemainingLines"] != null ? Convert.ToInt32(ViewState["RemainingLines"]) : (int?)null;
        }
        set
        {
            ViewState["RemainingLines"] = value;
        }
    }

    protected void Page_InIt(object sender, EventArgs e)
    {
        ddlSourceCountry.CurrentPage = this;
        ddlSourceCountry.IsAllRequired = false;
        ddlSite.CurrentPage = this;
        ddlSite.SchedulingContact = true;
        ddlSite.TimeSlotWindow = "W";
        ucSeacrhVendor1.CurrentPage = this;
        txtSchedulingdate.CurrentPage = this;
        ucSeacrhVendor1.IsParentRequired = false;
        ucSeacrhVendor1.IsStandAloneRequired = true;
        ucSeacrhVendor1.IsChildRequired = true;
        ucSeacrhVendor1.IsGrandParentRequired = false;
    }

    private void SetVendorDetailsByUserID()
    {
        UP_VendorBAL oUP_VendorBAL = new UP_VendorBAL();
        UP_VendorBE oUP_VendorBE = new UP_VendorBE();

        oUP_VendorBE.Action = "GetAssignedVendorDetailsByUserID"; //get diract assigned vendor
        oUP_VendorBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
        oUP_VendorBE.User.UserID = Convert.ToInt32(Session["UserID"]);
        oUP_VendorBE.Site = new MAS_SiteBE();
        oUP_VendorBE.Site.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedValue);

        List<UP_VendorBE> lstUPVendor = new List<UP_VendorBE>();
        lstUPVendor = oUP_VendorBAL.GetVendorByUserIdBAL(oUP_VendorBE);

        if (lstUPVendor == null || lstUPVendor.Count < 1)
        {  //if no diract vendor is assigned
            oUP_VendorBE.Action = "GetVendorDetailsByUserID";
            lstUPVendor = oUP_VendorBAL.GetVendorByUserIdBAL(oUP_VendorBE);
        }

        if (lstUPVendor != null && lstUPVendor.Count > 0)
        {
            ucSeacrhVendorID = lstUPVendor[0].VendorID.ToString();
            ucSeacrhVendorName = lstUPVendor[0].VendorName;
            ucSeacrhVendorNo = lstUPVendor[0].Vendor_No;
            ucSeacrhVendorParentID = lstUPVendor[0].ParentVendorID.ToString();
        }
        spVender.Style.Add("display", "none");

        ltSearchVendorName.Visible = true;
        ltSearchVendorName.Text = ucSeacrhVendorName;
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            pnlPalletCheckforCountryDisplay.Visible = chkSiteSetting.Checked;

            hdnCurrentMode.Value = GetQueryStringValue("Mode").ToString();
            if (GetQueryStringValue("WindowSlot") == null)
                hdnWindowSlotMode.Value = "false";
            else if (GetQueryStringValue("WindowSlot") == "True")
                hdnWindowSlotMode.Value = "true";

            hdnLogicalScheduleDate.Value = GetQueryStringValue("Logicaldate").ToString();
            hdnActualScheduleDate.Value = GetQueryStringValue("Scheduledate").ToString();

            LogicalSchedulDateTime = Utilities.Common.TextToDateFormat(hdnLogicalScheduleDate.Value);
            ActualSchedulDateTime = Utilities.Common.TextToDateFormat(hdnActualScheduleDate.Value);
        }

        if (!Page.IsPostBack)
        {
            if (GetQueryStringValue("SiteCountryID") == null)
            {
                hdnActualScheduleDate.Value = string.Empty; // DateTime.Now.Day.ToString() + "/" + DateTime.Now.Month.ToString() + "/" + DateTime.Now.Year.ToString();
                ddlSite.innerControlddlSite.AutoPostBack = true;
                PreSiteCountryID = Convert.ToInt32(Session["SiteCountryID"]);
            }
            else
            {
                ddlSite.innerControlddlSite.AutoPostBack = true;
                PreSiteCountryID = Convert.ToInt32(GetQueryStringValue("SiteCountryID").ToString());
            }

            if (hdnCurrentMode.Value == "Add" && hdnWindowSlotMode.Value == "false" && hdnCurrentRole.Value == "Vendor")
            {
                // Here Vendor details will be set, when first time page will be load.
                //this.SetVendorDetailsByUserID();
                this.ActivateFistView();
            }
            else if (hdnCurrentMode.Value == "Edit" && GetQueryStringValue("Type").ToString() == "V")
            {

                btnConfirmBooking.Visible = true;


                btnConfirmAlternateSlot.Visible = false;
                btnConfirmBookingWithMail_1.Visible = true;
                btnConfirmBookingWithoutMail_1.Visible = true;

                APPBOK_BookingBE oAPPBOK_BookingBE = new APPBOK_BookingBE();
                APPBOK_BookingBAL oAPPBOK_BookingBAL = new APPBOK_BookingBAL();

                oAPPBOK_BookingBE.Action = "GetVendorBookingDeatilsByID";
                oAPPBOK_BookingBE.BookingID = Convert.ToInt32(GetQueryStringValue("ID").ToString());

                List<APPBOK_BookingBE> lstBookingDeatils = new List<APPBOK_BookingBE>();
                lstBookingDeatils = oAPPBOK_BookingBAL.GetVendorBookingDetailsBAL(oAPPBOK_BookingBE);

                if (lstBookingDeatils != null && lstBookingDeatils.Count > 0)
                {

                    //hdnSuggestedSlotTime.Value = lstBookingDeatils[0].SlotTime.SlotTime;

                    #region Country Pallet Check
                    CountryData.Visible = lstBookingDeatils[0].ISPM15CountryPalletChecking;
                    pnlPalletCheckforCountryDisplay.Visible = lstBookingDeatils[0].ISPM15CountryPalletChecking;
                    lblYesNo.Text = lstBookingDeatils[0].ISPM15CountryPalletChecking ? "Yes" : "No";
                    chkSiteSetting.Checked = lstBookingDeatils[0].ISPM15CountryPalletChecking;
                    ddlSourceCountry.innerControlddlCountry.SelectedValue = lstBookingDeatils[0].ISPM15FromCountryID.ToString();
                    lblFromCountryValue.Text = ddlSourceCountry.innerControlddlCountry.SelectedItem.Text;
                    #endregion


                    hdnBookingRef.Value = lstBookingDeatils[0].BookingRef;

                    ucSeacrhVendorID = lstBookingDeatils[0].Vendor.VendorID.ToString();
                    ucSeacrhVendorName = lstBookingDeatils[0].Vendor.VendorName;
                    ucSeacrhVendorNo = lstBookingDeatils[0].Vendor.Vendor_No;
                    ucSeacrhVendorParentID = lstBookingDeatils[0].Vendor.ParentVendorID.ToString();

                    if (lstBookingDeatils[0].BookingTypeID == 7) //Non time booking
                        IsNonStandardWindowSlot = true;
                    else if (lstBookingDeatils[0].BookingTypeID == 5) //Stage 9 Point 2a/2b
                        IsProvisionalBooking = true;

                    spVender.Style.Add("display", "none");

                    ltSearchVendorName.Visible = true;
                    ltSearchVendorName.Text = ucSeacrhVendorNo + " - " + ucSeacrhVendorName;

                    ActivateFistView();

                    ddlDeliveryType.SelectedIndex = ddlDeliveryType.Items.IndexOf(ddlDeliveryType.Items.FindByValue(lstBookingDeatils[0].Delivery.DeliveryTypeID.ToString()));
                    ddlCarrier.SelectedIndex = ddlCarrier.Items.IndexOf(ddlCarrier.Items.FindByValue(lstBookingDeatils[0].Carrier.CarrierID.ToString()));
                    if (lstBookingDeatils[0].OtherCarrier != string.Empty)
                    {
                        txtCarrierOther.Text = lstBookingDeatils[0].OtherCarrier;
                        txtCarrierOther.Visible = true;
                    }

                    ddlVehicleType.SelectedIndex = ddlVehicleType.Items.IndexOf(ddlVehicleType.Items.FindByValue(lstBookingDeatils[0].VehicleType.VehicleTypeID.ToString()));
                    hdnPreviousVehicleType.Value = lstBookingDeatils[0].VehicleType.VehicleTypeID.ToString();
                    if (lstBookingDeatils[0].OtherVehicle != string.Empty)
                    {
                        txtVehicleOther.Text = lstBookingDeatils[0].OtherVehicle;
                        txtVehicleOther.Visible = true;
                    }

                    ddlDeliveryType_SelectedIndexChanged(sender, e);
                    if (lstBookingDeatils[0].OtherCarrier != string.Empty)
                    {

                        txtCarrierOther.Style.Add("display", "");
                        lblCarrierAdvice.Style.Add("display", "");
                        rfvCarrierOtherRequired.Enabled = true;
                        txtCarrierOther.Text = lstBookingDeatils[0].OtherCarrier;
                    }

                    ddlVehicleType_SelectedIndexChanged(sender, e);
                    if (lstBookingDeatils[0].OtherVehicle != string.Empty)
                    {

                        txtVehicleOther.Style.Add("display", "");
                        lblVehicleAdvice.Style.Add("display", "");
                        rfvVehicleTypeOtherRequired.Enabled = true;
                        txtVehicleOther.Text = lstBookingDeatils[0].OtherVehicle;
                    }

                    // stage 15 R2 point no 17
                    hdnIsBookingAccepted.Value = Convert.ToString(lstBookingDeatils[0].IsBookingAccepted);
                    //
                    ShowHazordousItem();
                    if (lstBookingDeatils[0].IsEnableHazardouesItemPrompt)
                    {
                        checkBoxEnableHazardouesItemPrompt.Checked = true;
                    }
                    else
                    {
                        checkBoxEnableHazardouesItemPrompt.Checked = false;
                    }

                    if (Convert.ToString(lstBookingDeatils[0].BookingTypeID) == "5" && Convert.ToString(lstBookingDeatils[0].ProvisionalReason) == "EDITED" && lstBookingDeatils[0].OldNumberOfLines != 0
                         && Session["Role"].ToString().ToLower() != "vendor")
                    {
                        hdnBookingTypeID.Value = lstBookingDeatils[0].BookingTypeID.ToString();
                        hdnProvisionalReason.Value = "EDITED";
                        hdnLine.Value = Convert.ToString(lstBookingDeatils[0].OldNumberOfLines);
                        lblPalletsProvisional.Visible = true;
                        lblCartonsProvisional.Visible = true;
                        lblLiftsProvisional.Visible = true;
                        lblExpectedLineProvisional.Visible = true;
                        btnRejectChanges.Visible = true;
                        lblPalletsProvisional.Text = lstBookingDeatils[0].OldNumberOfPallet.ToString();
                        lblCartonsProvisional.Text = lstBookingDeatils[0].OldNumberOfCartons.ToString();
                        lblLiftsProvisional.Text = lstBookingDeatils[0].OldNumberOfLift.ToString();
                        lblExpectedLineProvisional.Text = lstBookingDeatils[0].OldNumberOfLines.ToString();
                        btnProceedBooking.Visible = false;
                        btnAccept.Visible = true;
                        btnRejectBooking.Visible = true;
                        lblVolumeDetailDescProvisional.Visible = true;
                        lblVolumeDetailDesc.Visible = false;
                        txtLifts.ForeColor = Color.Red;
                        txtPallets.ForeColor = Color.Red;
                        txtCartons.ForeColor = Color.Red;
                        txtExpectedLines.ForeColor = Color.Red;
                    }
                    else
                    {
                        hdnBookingTypeID.Value = lstBookingDeatils[0].BookingTypeID.ToString();
                        hdnProvisionalReason.Value = Convert.ToString(lstBookingDeatils[0].ProvisionalReason);
                        lblPalletsProvisional.Visible = false;
                        lblCartonsProvisional.Visible = false;
                        lblLiftsProvisional.Visible = false;
                        lblExpectedLineProvisional.Visible = false;
                        lblPalletsProvisional.Visible = false;
                        lblCartonsProvisional.Visible = false;
                        lblLiftsProvisional.Visible = false;
                        lblExpectedLineProvisional.Visible = false;
                        btnRejectChanges.Visible = false;
                        btnAccept.Visible = false;
                        btnRejectBooking.Visible = false;
                        lblVolumeDetailDescProvisional.Visible = false;
                        lblVolumeDetailDesc.Visible = true;
                        txtLifts.ForeColor = Color.Gray;
                        txtPallets.ForeColor = Color.Gray;
                        txtCartons.ForeColor = Color.Gray;
                        txtExpectedLines.ForeColor = Color.Gray;
                    }
                    txtLifts.Text = lstBookingDeatils[0].NumberOfLift.ToString();
                    txtPallets.Text = lstBookingDeatils[0].NumberOfPallet.ToString();
                    txtCartons.Text = lstBookingDeatils[0].NumberOfCartons.ToString();
                    txtExpectedLines.Text = lstBookingDeatils[0].NumberOfLines.ToString();
                    BookedLines = Convert.ToInt32(txtExpectedLines.Text);
                    BookedPallets = Convert.ToInt32(txtPallets.Text);

                    // Sprint 1 - Point 4 - Start - For Booking Comments.
                    txtBookingComment.Text = Convert.ToString(lstBookingDeatils[0].BookingComments);
                    txtEditBookingComment.Text = Convert.ToString(lstBookingDeatils[0].BookingComments);
                    // Sprint 1 - Point 4 - End - For Booking Comments.

                    //BookedLines = Convert.ToInt32(txtExpectedLines.Text);
                    //BookedPallets = Convert.ToInt32(txtPallets.Text);
                    //-----Stage 10 Point 10L 5.1-------
                    //BookedCartons = Convert.ToInt32(txtCartons.Text);
                    //----------------------------------

                    //BIND ALREADY BOOKED SLOT DETAILS//

                    string strWeekDay = LogicalSchedulDateTime.DayOfWeek.ToString().ToUpper();
                    int bookingWeekDay = 0;
                    if (strWeekDay == "SUNDAY")
                    {
                        bookingWeekDay = 1;
                    }
                    else if (strWeekDay == "MONDAY")
                    {
                        bookingWeekDay = 2;
                    }
                    else if (strWeekDay == "TUESDAY")
                    {
                        bookingWeekDay = 3;
                    }
                    else if (strWeekDay == "WEDNESDAY")
                    {
                        bookingWeekDay = 4;
                    }
                    else if (strWeekDay == "THURSDAY")
                    {
                        bookingWeekDay = 5;
                    }
                    else if (strWeekDay == "FRIDAY")
                    {
                        bookingWeekDay = 6;
                    }
                    else if (strWeekDay == "SATURDAY")
                    {
                        bookingWeekDay = 7;
                    }

                    if (Convert.ToBoolean(IsNonStandardWindowSlot))
                    {
                        hdnPreservSlot.Value = lstBookingDeatils[0].NonWindowFromTimeID + "|" + lstBookingDeatils[0].NonWindowToTimeID + "|" + lstBookingDeatils[0].DoorNoSetup.SiteDoorNumberID.ToString();
                        hdnStartTimeID.Value = Convert.ToString(lstBookingDeatils[0].NonWindowFromTimeID);
                        hdnEndTimeID.Value = Convert.ToString(lstBookingDeatils[0].NonWindowToTimeID);
                    }

                    ltBookingDate.Text = lstBookingDeatils[0].SlotTime.SlotTime + " on " + ActualSchedulDateTime.ToString("dd/MM/yyyy");
                    ltSuggestedSlotTime.Text = AtDoorName + " " + lstBookingDeatils[0].DoorNoSetup.DoorNumber.ToString();

                    if (lstBookingDeatils[0].TimeWindow.TimeWindowID != null && lstBookingDeatils[0].TimeWindow.TimeWindowID != 0)
                    {
                        MASSIT_TimeWindowBE oMASSIT_TimeWindowBE = new MASSIT_TimeWindowBE();
                        oMASSIT_TimeWindowBE.Action = "GetTimeWindowData";
                        oMASSIT_TimeWindowBE.TimeWindowID = lstBookingDeatils[0].TimeWindow.TimeWindowID;
                        MASSIT_TimeWindowBAL oMASSIT_TimeWindowBAL = new MASSIT_TimeWindowBAL();
                        List<MASSIT_TimeWindowBE> lstMASSIT_TimeWindowBE = oMASSIT_TimeWindowBAL.GetTimeWindowBAL(oMASSIT_TimeWindowBE);
                        if (lstMASSIT_TimeWindowBE.Count > 0)
                        {
                            hdnPreservSlot.Value = lstMASSIT_TimeWindowBE[0].StartSlotTimeID + "|" + lstMASSIT_TimeWindowBE[0].EndSlotTimeID + "|" + lstMASSIT_TimeWindowBE[0].SiteDoorNumberID.ToString();
                        }
                    }
                    //hdnSuggestedSlotTimeID.Value = lstBookingDeatils[0].SlotTime.SlotTimeID.ToString();
                    hdnSuggestedWindowSlotID.Value = lstBookingDeatils[0].TimeWindow.TimeWindowID.ToString();
                    hdnSuggestedSiteDoorNumberID.Value = lstBookingDeatils[0].DoorNoSetup.SiteDoorNumberID.ToString();
                    hdnSuggestedSiteDoorNumber.Value = lstBookingDeatils[0].DoorNoSetup.DoorNumber.ToString();


                    //ltTimeSlot.Text = hdnSuggestedSlotTime.Value;

                    //ltDoorNo.Text = hdnSuggestedSiteDoorNumber.Value;
                    //ltTimeSlot_1.Text = lstBookingDeatils[0].SlotTime.SlotTime;
                    //ltDoorNo_1.Text = hdnSuggestedSiteDoorNumber.Value;
                    hdnSuggestedWeekDay.Value = getWeekday(lstBookingDeatils[0].WeekDay);

                    if (hdnCurrentMode.Value == "Edit")
                    {
                        // ProceedAddPO(); //btnProceedAddPO_Click(sender, e);
                        BindAllPOGrid();
                        pnlBookingDetailsArrival.Visible = true;
                        BookingLiftsData.Text = lstBookingDeatils[0].NumberOfLift.ToString();
                        BookingPalletsData.Text = lstBookingDeatils[0].NumberOfPallet.ToString();
                        BookingCartonsData.Text = lstBookingDeatils[0].NumberOfCartons.ToString();
                        BookingLinesData.Text = lstBookingDeatils[0].NumberOfLines.ToString();
                        mvBookingEdit.ActiveViewIndex = 1;
                    }

                    //-----------Start Stage 9 point 2a/2b--------------//
                    if (GetQueryStringValue("PNTB") != null && GetQueryStringValue("PNTB").ToString() == "True")
                    {
                        Session["BookingCreated"] = false;
                        btnWindowAlternateSlot_Click(null, null);
                    }
                    if (lstBookingDeatils[0].BookingTypeID == 5 && hdnCurrentRole.Value != "Vendor")
                    {

                        if (Convert.ToString(lstBookingDeatils[0].ProvisionalReason) == "EDITED" && lstBookingDeatils[0].OldNumberOfLines != 0
                             && Session["Role"].ToString().ToLower() != "vendor")
                        {
                            btnReject.Visible = false;
                            btnReject_1.Visible = false;
                        }
                        else
                        {
                            btnReject.Visible = true;
                            btnReject_1.Visible = true;
                        }

                    }
                    //-----------End Stage 9 point 2a/2b--------------//
                }
            }
            else if (hdnWindowSlotMode.Value == "true" && GetQueryStringValue("Type").ToString() == "V")
            {
                MASSIT_TimeWindowBE oMASSIT_TimeWindowBE = new MASSIT_TimeWindowBE();
                MASSIT_TimeWindowBAL oMASSIT_TimeWindowBAL = new MASSIT_TimeWindowBAL();

                oMASSIT_TimeWindowBE.Action = "ShowAllReserved";
                oMASSIT_TimeWindowBE.TimeWindowID = Convert.ToInt32(GetQueryStringValue("ID"));
                //oMASSIT_TimeWindowBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
                //oMASSIT_TimeWindowBE.User.UserID = Convert.ToInt32(Session["UserID"]);

                List<MASSIT_TimeWindowBE> lstFixedSlot = oMASSIT_TimeWindowBAL.GetReservedWindowBAL(oMASSIT_TimeWindowBE);

                if (lstFixedSlot != null && lstFixedSlot.Count > 0)
                {

                    //hdnSuggestedSlotTime.Value = lstFixedSlot[0].SlotTime.SlotTime;

                    ucSeacrhVendorID = lstFixedSlot[0].IncludedVendorIDs;
                    ucSeacrhVendorName = lstFixedSlot[0].VendorName;
                    ucSeacrhVendorNo = lstFixedSlot[0].Vendor_No;
                    ucSeacrhVendorParentID = lstFixedSlot[0].ParentVendorID.ToString();

                    spVender.Style.Add("display", "none");

                    ltSearchVendorName.Visible = true;
                    ltSearchVendorName.Text = ucSeacrhVendorName;


                    hdnSuggestedWindowSlotID.Value = lstFixedSlot[0].TimeWindowID.ToString();

                    ActivateFistView();

                    //BIND ALREADY BOOKED SLOT DETAILS//


                    ltSuggestedSlotTime.Text = lstFixedSlot[0].StartTime + " - " + lstFixedSlot[0].EndTime + " " + AtDoorName + " " + lstFixedSlot[0].DoorNumber.ToString();
                    //hdnSuggestedSlotTimeID.Value = lstFixedSlot[0].SlotTimeID.ToString();

                    hdnSuggestedSiteDoorNumberID.Value = lstFixedSlot[0].SiteDoorNumberID.ToString();
                    hdnSuggestedSiteDoorNumber.Value = lstFixedSlot[0].DoorNumber.ToString();


                    //ltTimeSlot.Text = hdnSuggestedSlotTime.Value;


                    //ltDoorNo.Text = hdnSuggestedSiteDoorNumber.Value;
                    //ltTimeSlot_1.Text = lstFixedSlot[0].SlotTime.SlotTime;
                    //ltDoorNo_1.Text = hdnSuggestedSiteDoorNumber.Value;

                    spSCDate.Style.Add("display", "none");
                    ltSCdate.Visible = true;
                    ltSCdate.Text = string.Empty;



                    ltBookingDate.Text = ActualSchedulDateTime.ToString("dd/MM/yyyy");
                    hdnSuggestedWeekDay.Value = ActualSchedulDateTime.ToString("ddd");
                    ltSCdate.Text = ActualSchedulDateTime.ToString("dd/MM/yyyy") + " " + ltSuggestedSlotTime.Text;
                }


            }
            else if (GetQueryStringValue("Flag").ToString() == "true")
            {
                ucSeacrhVendorID = "";
                ucSeacrhVendorName = "";
                ucSeacrhVendorNo = "";
                ucSeacrhVendorParentID = "";
                spVender.Style.Add("display", "block");
                ltSearchVendorName.Visible = true;
                ltSearchVendorName.Text = ucSeacrhVendorName;
                ActivateFistView();
                hdnSuggestedWindowSlotID.Value = GetQueryStringValue("ID");
                ltSuggestedSlotTime.Text = GetQueryStringValue("Time") + " " + AtDoorName + " " + GetQueryStringValue("DoorNumber");
                spSCDate.Style.Add("display", "none");
                ltSCdate.Visible = true;
                ltSCdate.Text = string.Empty;
                //hdnSuggestedSlotTimeID.Value = lstFixedSlot[0].SlotTimeID.ToString();
                hdnSuggestedSiteDoorNumberID.Value = GetQueryStringValue("SiteDoorNumberID");
                hdnSuggestedSiteDoorNumber.Value = GetQueryStringValue("DoorNumber");
                ltBookingDate.Text = ActualSchedulDateTime.ToString("dd/MM/yyyy");
                hdnSuggestedWeekDay.Value = ActualSchedulDateTime.ToString("ddd");
                ltSCdate.Text = ActualSchedulDateTime.ToString("dd/MM/yyyy") + " " + ltSuggestedSlotTime.Text;
            }
            //else if (hdnCurrentMode.Value == "Add" && hdnWindowSlotMode.Value == "false" && hdnCurrentRole.Value != "Vendor")
            //{
            //    string TimeWindowID = GetQueryStringValue("TimeWindowID");
            //    hdnSuggestedWindowSlotID.Value = TimeWindowID;
            //}
            CheckSiteClosure();
            if (GetQueryStringValue("BP") != null && GetQueryStringValue("BP") == "OPT")
                btnVendor_Click(null, null);
        }

        //Emergency fix - temp as per Anthony's mail on 11 jan 2016
        if (!IsPostBack)
        {
            if ((Session["Role"].ToString().ToLower() == "vendor" || Session["Role"].ToString().ToLower() == "carrier"))
            {
                BindSchedulingClosedown();
            }
        }
        //if (btnErrContinue.CommandName == "BK_MS_23_EXT" && IsPostBack)
        //{
        //    btnContinue_Click(sender, btnErrContinue);
        //}
        //----------------------------------------------------------
    }

    private void BindSchedulingClosedown()
    {
        MASSIT_SchedulingClosedownBE oMASSIT_SchedulingClosedownBE = new MASSIT_SchedulingClosedownBE();
        MASSIT_SchedulingClosedownBAL oMASSIT_SchedulingClosedownBAL = new MASSIT_SchedulingClosedownBAL();

        oMASSIT_SchedulingClosedownBE.Action = "ShowAll";

        List<MASSIT_SchedulingClosedownBE> lstSchedulingClosedownBE = oMASSIT_SchedulingClosedownBAL.GetSchedulingClosedownBAL(oMASSIT_SchedulingClosedownBE);

        if (lstSchedulingClosedownBE != null && lstSchedulingClosedownBE.Count > 0)
        {
            for (int icount = 0; icount < lstSchedulingClosedownBE.Count; icount++)
            {
                if (ddlSite.innerControlddlSite.Items.IndexOf(ddlSite.innerControlddlSite.Items.FindByValue(lstSchedulingClosedownBE[icount].SiteID.ToString())) >= 0)
                    ddlSite.innerControlddlSite.Items.RemoveAt(ddlSite.innerControlddlSite.Items.IndexOf(ddlSite.innerControlddlSite.Items.FindByValue(lstSchedulingClosedownBE[icount].SiteID.ToString())));
            }
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {


        Label lblModuleText = (Label)Page.Master.FindControl("lblModuleText");
        lblModuleText.Text = "Appointment Scheduling - Booking";

        if (!Page.IsPostBack)
        {
            Session["BookingCreated"] = false;
            ViewState["IsShowPOPopup"] = false;
            hdnCurrentRole.Value = Session["Role"].ToString();
        }

        if (Session["Role"].ToString() != "Vendor")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "SetVendorNo", "<script>setVendorName();</script>", false);
        }

        //ScriptManager.RegisterStartupScript(this, this.GetType(), "scroll", "<script>setScroll();</script>", false);

        hdnBeforeSlotTimeID.Value = BeforeSlotTimeID;
        hdnAfterSlotTimeID.Value = AfterSlotTimeID;
    }

    protected void btnVendor_Click(object sender, EventArgs e)
    {
        ActivateFistView();
    }

    protected void btnCarrier_Click(object sender, EventArgs e)
    {
        MAS_SiteBAL objMasSitE = new MAS_SiteBAL();
        string TimeWindowFlag = objMasSitE.GetSiteTimeSlotWindowFlagBAL(Convert.ToString(GetQueryStringValue("SiteID")));
        string Role = Session["Role"].ToString().Trim().ToLower();


        string TimeWindowID = GetQueryStringValue("TimeWindowID");

        if (TimeWindowFlag == "W")
            EncryptQueryString("APPBok_CarrierBookingWindowEdit.aspx?Mode=" + GetQueryStringValue("Mode").ToString() + "&Scheduledate=" + ActualSchedulDateTime.ToString("dd/MM/yyyy") + "&Logicaldate=" + LogicalSchedulDateTime.ToString("dd/MM/yyyy") + "&SiteID=" + GetQueryStringValue("SiteID").ToString() + "&SiteCountryID=" + GetQueryStringValue("SiteCountryID").ToString() + "&TimeWindowID=" + GetQueryStringValue("TimeWindowID"));
        else
            EncryptQueryString("APPBok_CarrierBookingEdit.aspx?Mode=" + GetQueryStringValue("Mode").ToString() + "&Scheduledate=" + ActualSchedulDateTime.ToString("dd/MM/yyyy") + "&Logicaldate=" + LogicalSchedulDateTime.ToString("dd/MM/yyyy") + "&SiteID=" + GetQueryStringValue("SiteID").ToString() + "&SiteCountryID=" + GetQueryStringValue("SiteCountryID").ToString());
    }

    private void ActivateFistView()
    {
        string TimeWindowID = GetQueryStringValue("TimeWindowID");

        mvBookingEdit.ActiveViewIndex = 1;

        if (hdnActualScheduleDate.Value != string.Empty)
        {
            txtSchedulingdate.innerControltxtDate.Value = ActualSchedulDateTime.ToString("dd/MM/yyyy");
        }

        if (hdnCurrentMode.Value == "Add" && hdnWindowSlotMode.Value == "false" && hdnCurrentRole.Value == "Vendor")
        {
            txtSchedulingdate.innerControltxtDate.Value = "";
        }

        if (hdnCurrentMode.Value == "Edit" && hdnCurrentRole.Value == "Vendor")
        {
            //spSCDate.Style.Add("display", "none");
            //ltSCdate.Visible = true;
            //ltSCdate.Text = ActualSchedulDateTime.ToString("dd/MM/yyyy");
        }



        if (GetQueryStringValue("SiteCountryID") != null)
        {

            if (GetQueryStringValue("SiteID").ToString() != "0")
                ddlSite.innerControlddlSite.SelectedIndex = ddlSite.innerControlddlSite.Items.IndexOf(ddlSite.innerControlddlSite.Items.FindByValue(GetQueryStringValue("SiteID").ToString()));
            else
                ddlSite.innerControlddlSite.SelectedIndex = ddlSite.innerControlddlSite.Items.IndexOf(ddlSite.innerControlddlSite.Items.FindByValue(Session["SiteID"].ToString()));
            ddlSite.CountryID = Convert.ToInt32(GetQueryStringValue("SiteCountryID").ToString());
            ucSeacrhVendor1.SiteID = Convert.ToInt32(GetQueryStringValue("SiteID").ToString());
        }
        else
        {
            ddlSite.innerControlddlSite.SelectedIndex = ddlSite.innerControlddlSite.Items.IndexOf(ddlSite.innerControlddlSite.Items.FindByValue(Session["SiteID"].ToString()));
            ddlSite.CountryID = Convert.ToInt32(Session["SiteCountryID"].ToString());
            ucSeacrhVendor1.SiteID = Convert.ToInt32(Session["SiteID"].ToString());
        }

        SiteSelectedIndexChanged();
        ddlSite.SelectedIndexChanged();
        if (hdnCurrentMode.Value == "Edit")
        {
            spSite.Style.Add("display", "none");
            ltSelectedSite.Visible = true;
            ltSelectedSite.Text = ddlSite.innerControlddlSite.SelectedItem.Text;
        }
    }

    public override void SitePost_Load()
    {
        base.SitePost_Load();
        MAS_SiteBE oMAS_SiteBE = ddlSite.oSiteBE;
        if (oMAS_SiteBE != null)
        {
            ViewState["oMAS_SiteBE"] = oMAS_SiteBE;
        }
    }

    public override void SiteSelectedIndexChanged()
    {
        base.SiteSelectedIndexChanged();
        if (ddlSite.innerControlddlSite.SelectedValue != "0" && !string.IsNullOrEmpty(ddlSite.innerControlddlSite.SelectedValue))
        {
            ISPM15PalletCheckingBAL iSPM15PalletCheckingBAL = new ISPM15PalletCheckingBAL();
            ISPM15PalletCheckingBE ispm15 = new ISPM15PalletCheckingBE();
            ispm15.Action = "GetISPM15BySiteID";
            ispm15.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedValue);
            pnlPalletCheckforCountry.Visible = iSPM15PalletCheckingBAL.IsSiteCountryExistsBAL(ispm15);
        }
        IsBookingValidationExcludedDelivery = false;

        if (hdnCurrentRole.Value == "Vendor" && hdnWindowSlotMode.Value == "false" && hdnCurrentMode.Value != "Edit")
        {
            // Here Vendor details will be set, when user will change the site name from dropdownlist.
            this.SetVendorDetailsByUserID();
        }

        BindVehicleType();

        MAS_SiteBE oMAS_SiteBE = ddlSite.oSiteBE;
        if (oMAS_SiteBE == null)
        {
            BindDelivaryType();
            BindCarrier();
            oMAS_SiteBE = (MAS_SiteBE)ViewState["oMAS_SiteBE"];
        }

        if (oMAS_SiteBE != null)
        {
            int CurrentSiteCountryID = Convert.ToInt32(oMAS_SiteBE.SiteCountryID);

            //---Pre Advise Note Required show/hide---//
            if (oMAS_SiteBE.IsPreAdviseNoteRequired == true)
            {
                trpreadvise.Visible = true;
                IsPreAdviseNoteRequired = true;
            }
            else
            {
                trpreadvise.Visible = false;
                IsPreAdviseNoteRequired = false;
            }
            //-------------------------------------//

            NonTimeDeliveryCartonVolume = oMAS_SiteBE.NonTimeDeliveryCartonVolume;
            NonTimeDeliveryPalletVolume = oMAS_SiteBE.NonTimeDeliveryPalletVolume;

            if (PreSiteCountryID != CurrentSiteCountryID)
            {
                ucSeacrhVendor1.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
                ucSeacrhVendor1.ClearSearch();
                BindDelivaryType();
                BindCarrier();
            }
            else
            {
                if (!Page.IsPostBack)
                {
                    ucSeacrhVendor1.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
                    ucSeacrhVendor1.ClearSearch();
                    BindDelivaryType();
                    BindCarrier();
                }
            }

            if (Convert.ToBoolean(isPostBack))
                PreSiteCountryID = CurrentSiteCountryID;
            else
                isPostBack = true;
        }
        else
        {
            BindDelivaryType();
            BindCarrier();
        }

        VendorValidationExc();
        DeliveryValidationExc();


        if (!GetToleratedDays())
        {
            GetMaximumCapacity();

            if (txtSchedulingdate.innerControltxtDate.Value != string.Empty && ddlSite.innerControlddlSite.SelectedItem.Value != string.Empty)
            {
                bool IsWeekConstraints = GetWeekConstraints();

                if (!IsWeekConstraints)
                {
                    string errMsg = WebCommon.getGlobalResourceValue("BK_MS_28_EXT"); // "No space available on any compatible doors.Please contact Goods In office or select a different date.";

                    if (hdnCurrentRole.Value == "Vendor")
                    {
                        //Show err msg
                        ShowErrorMessage(errMsg, "BK_MS_28");
                        return;
                    }
                    else
                    {
                        //Show err msg
                        errMsg = WebCommon.getGlobalResourceValue("BK_MS_28_INT"); //"No space available on any compatible doors.";
                        ShowErrorMessage(errMsg, "BK_MS_28");
                        return;
                    }
                }
            }
        }
        ShowWarningMessages();

        //------Phase 15 R2 Point 21-----------//
        Session["CurrentSiteId"] = ddlSite.innerControlddlSite.SelectedValue;
        txtSchedulingdate.LoadDates();
        //------------------------------------//

        ShowHazordousItem();
    }

    private void BindStartEndTime()
    {
        MASSIT_TimeWindowBAL oMASSIT_TimeWindowBAL = new MASSIT_TimeWindowBAL();
        SYS_SlotTimeBAL oSYS_SlotTimeBAL = new SYS_SlotTimeBAL();
        List<SYS_SlotTimeBE> lstSlotTime = oSYS_SlotTimeBAL.GetTimeBAL();
        if (lstSlotTime.Count > 0)
        {
            FillControls.FillDropDown(ref ddlFrom, lstSlotTime, "SlotTime", "SlotTimeID");
        }
        if (lstSlotTime.Count > 0)
        {
            FillControls.FillDropDown(ref ddlTo, lstSlotTime, "SlotTime", "SlotTimeID");
        }
    }

    private void BindDoorNumber()
    {
        MASSIT_TimeWindowBAL oMASSIT_TimeWindowBAL = new MASSIT_TimeWindowBAL();
        List<MASSIT_TimeWindowBE> lstDoorNo = oMASSIT_TimeWindowBAL.GetDoorNoDetailsBAL(Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value));
        if (lstDoorNo.Count > 0)
        {
            FillControls.FillDropDown(ref ddlDoorName, lstDoorNo, "DoorNumber", "SiteDoorNumberID", "Select");
        }
    }

    private void BindVehicleType()
    {
        MASSIT_VehicleTypeBE oMASSIT_VehicleTypeBE = new MASSIT_VehicleTypeBE();
        APPSIT_VehicleTypeBAL oMASSIT_VehicleTypeBAL = new APPSIT_VehicleTypeBAL();

        oMASSIT_VehicleTypeBE.Action = "ShowAll";
        oMASSIT_VehicleTypeBE.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);

        List<MASSIT_VehicleTypeBE> lstVehicleType = oMASSIT_VehicleTypeBAL.GetVehicleTypeDetailsBAL(oMASSIT_VehicleTypeBE);

        if (lstVehicleType.Count > 0)
        {
            FillControls.FillDropDown(ref ddlVehicleType, lstVehicleType, "VehicleType", "VehicleTypeID", "--Select--");
        }

        VehicleNarrativeRequired = string.Empty;

        for (int iCount = 0; iCount < lstVehicleType.Count; iCount++)
        {
            if (lstVehicleType[iCount].IsNarrativeRequired)
            {
                if (VehicleNarrativeRequired == string.Empty)
                    VehicleNarrativeRequired = "," + lstVehicleType[iCount].VehicleTypeID.ToString() + ",";
                else
                    VehicleNarrativeRequired += lstVehicleType[iCount].VehicleTypeID.ToString() + ",";
            }

            //----Stage 10 Point 10 L 5.1-----//
            if (lstVehicleType[iCount].IsContainer)
            {
                if (IsVehicleContainer == string.Empty)
                    IsVehicleContainer = "," + lstVehicleType[iCount].VehicleTypeID.ToString() + ",";
                else
                    IsVehicleContainer += lstVehicleType[iCount].VehicleTypeID.ToString() + ",";
            }
            //---------------------------------//
        }


        MAS_SiteBE singleSiteSetting = GetSingleSiteSetting(Convert.ToInt32(ddlSite.innerControlddlSite.SelectedValue));
        if (singleSiteSetting != null)
        {
            ddlVehicleType.SelectedIndex = ddlVehicleType.Items.IndexOf(ddlVehicleType.Items.FindByValue(Convert.ToString(singleSiteSetting.VehicleTypeId)));
            ddlVehicleType_SelectedIndexChanged(null, null);
        }


    }

    private void BindDelivaryType()
    {
        MAS_SiteBE singleSiteSetting = null;
        int countrySiteId = 0;
        MASCNT_DeliveryTypeBE oMASCNT_DeliveryTypeBE = new MASCNT_DeliveryTypeBE();
        APPCNT_DeliveryTypeBAL oMASCNT_DeliveryTypeBAL = new APPCNT_DeliveryTypeBAL();

        if (!IsPostBack)
        {
            if (GetQueryStringValue("SiteID") != null)
                singleSiteSetting = GetSingleSiteSetting(Convert.ToInt32(GetQueryStringValue("SiteID")));
            else
                singleSiteSetting = GetSingleSiteSetting(Convert.ToInt32(ddlSite.innerControlddlSite.SelectedValue));
        }
        else
        {
            singleSiteSetting = GetSingleSiteSetting(Convert.ToInt32(ddlSite.innerControlddlSite.SelectedValue));
        }

        if (singleSiteSetting != null)
            countrySiteId = Convert.ToInt32(singleSiteSetting.SiteCountryID);

        oMASCNT_DeliveryTypeBE.Action = "ShowAll";
        //oMASCNT_DeliveryTypeBE.CountryID = ddlSite.CountryID;
        if (countrySiteId != 0)
            oMASCNT_DeliveryTypeBE.CountryID = countrySiteId;
        else
            oMASCNT_DeliveryTypeBE.CountryID = ddlSite.CountryID;

        oMASCNT_DeliveryTypeBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
        oMASCNT_DeliveryTypeBE.User.UserID = 0;
        List<MASCNT_DeliveryTypeBE> lstDeliveryType = oMASCNT_DeliveryTypeBAL.GetDeliveryTypeDetailsBAL(oMASCNT_DeliveryTypeBE);

        if (hdnCurrentRole.Value == "Vendor")
        {
            lstDeliveryType = lstDeliveryType.FindAll(delegate (MASCNT_DeliveryTypeBE p) { return p.IsEnabledAsVendorOption == true; });
        }

        ddlDeliveryType.SelectedIndex = -1;
        if (lstDeliveryType.Count > 0)
        {
            FillControls.FillDropDown(ref ddlDeliveryType, lstDeliveryType, "DeliveryType", "DeliveryTypeID", "--Select--");
        }
        else
        {
            ddlDeliveryType.Items.Clear();
            ddlDeliveryType.Items.Add(new ListItem("---Select---", "0"));
        }


        //MAS_SiteBE singleSiteSetting = GetSingleSiteSetting(Convert.ToInt32(ddlSite.innerControlddlSite.SelectedValue));
        if (singleSiteSetting != null)
        {
            ddlDeliveryType.SelectedIndex = ddlDeliveryType.Items.IndexOf(ddlDeliveryType.Items.FindByValue(Convert.ToString(singleSiteSetting.DeliveryTypeId)));
            ddlDeliveryType_SelectedIndexChanged(null, null);
        }

    }

    private void BindCarrier()
    {
        MAS_SiteBE singleSiteSetting = null;
        int countrySiteId = 0;
        MASCNT_CarrierBE oMASCNT_CarrierBE = new MASCNT_CarrierBE();
        APPCNT_CarrierBAL oMASCNT_CarrierBAL = new APPCNT_CarrierBAL();

        if (!IsPostBack)
        {
            if (GetQueryStringValue("SiteID") != null)
                singleSiteSetting = GetSingleSiteSetting(Convert.ToInt32(GetQueryStringValue("SiteID")));
            else
                singleSiteSetting = GetSingleSiteSetting(Convert.ToInt32(ddlSite.innerControlddlSite.SelectedValue));
        }
        else
        {
            singleSiteSetting = GetSingleSiteSetting(Convert.ToInt32(ddlSite.innerControlddlSite.SelectedValue));
        }

        if (singleSiteSetting != null)
            countrySiteId = Convert.ToInt32(singleSiteSetting.SiteCountryID);

        oMASCNT_CarrierBE.Action = "ShowAll";
        //oMASCNT_CarrierBE.CountryID = ddlSite.CountryID;
        if (countrySiteId != 0)
            oMASCNT_CarrierBE.CountryID = countrySiteId;
        else
            oMASCNT_CarrierBE.CountryID = ddlSite.CountryID;

        List<MASCNT_CarrierBE> lstCarrier = oMASCNT_CarrierBAL.GetCarrierDetailsBAL(oMASCNT_CarrierBE);

        if (lstCarrier.Count > 0)
        {
            FillControls.FillDropDown(ref ddlCarrier, lstCarrier, "CarrierName", "CarrierID", "--Select--");
        }

        CarrierNarrativeRequired = ",";
        for (int iCount = 0; iCount < lstCarrier.Count; iCount++)
        {
            if (lstCarrier[iCount].IsNarrativeRequired)
            {
                CarrierNarrativeRequired += lstCarrier[iCount].CarrierID.ToString() + ",";
            }
        }
    }

    private bool VendorValidationExc()
    {

        bool bVendorValidation = true;

        string VendorNo = ucSeacrhVendorID; // ucSeacrhVendorNo; // ((ucTextbox)ucSeacrhVendor1.FindControl("txtVendorNo")).Text;

        if (VendorNo != string.Empty)
        {
            UP_VendorBAL oUP_VendorBAL = new UP_VendorBAL();
            UP_VendorBE oUP_VendorBE = new UP_VendorBE();

            oUP_VendorBE.Action = "ValidateVendorExc";
            oUP_VendorBE.Vendor_No = VendorNo.Trim();
            oUP_VendorBE.Site = new BusinessEntities.ModuleBE.AdminFunctions.MAS_SiteBE();
            oUP_VendorBE.Site.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);

            List<UP_VendorBE> lstUPVendor = oUP_VendorBAL.ValidateVendorBAL(oUP_VendorBE);

            IsBookingValidationExcludedVendor = false;
            IsNonTimeVendor = false;

            ViewState.Add("UPVendor", lstUPVendor.ToArray());

            BeforeSlotTimeID = string.Empty;
            AfterSlotTimeID = string.Empty;
            string errMsg = string.Empty;

            if (lstUPVendor.Count > 0)
            {
                List<UP_VendorBE> lstUPVendortemp = lstUPVendor.FindAll(delegate (UP_VendorBE ven)
                {
                    return ven.VendorDetails.APP_IsBookingValidationExcluded == true;
                });

                if (lstUPVendortemp.Count > 0)
                    IsBookingValidationExcludedVendor = true;

                lstUPVendortemp = lstUPVendor.FindAll(delegate (UP_VendorBE ven)
                {
                    return ven.VendorDetails.APP_IsNonTimeDeliveryAllowed == true;
                });

                if (lstUPVendortemp.Count > 0)
                    IsNonTimeVendor = true;

                lstUPVendor = lstUPVendor.FindAll(delegate (UP_VendorBE ven)
                {
                    return ven.VendorDetails.IsConstraintsDefined == true;
                });
            }

            if (lstUPVendor.Count > 0)
            {

                bool AllowedDay = true;

                if (txtSchedulingdate.innerControltxtDate.Value != string.Empty)
                {
                    string Weekday = LogicalSchedulDateTime.DayOfWeek.ToString().ToUpper();

                    switch (Weekday)
                    {
                        case "SUNDAY":
                            if (lstUPVendor[0].VendorDetails.APP_CannotDeliveryOnSunday == true)
                                AllowedDay = false;
                            break;
                        case "MONDAY":
                            if (lstUPVendor[0].VendorDetails.APP_CannotDeliveryOnMonday == true)
                                AllowedDay = false;
                            break;
                        case "TUESDAY":
                            if (lstUPVendor[0].VendorDetails.APP_CannotDeliveryOnTuesday == true)
                                AllowedDay = false;
                            break;
                        case "WEDNESDAY":
                            if (lstUPVendor[0].VendorDetails.APP_CannotDeliveryOnWednessday == true)
                                AllowedDay = false;
                            break;
                        case "THURSDAY":
                            if (lstUPVendor[0].VendorDetails.APP_CannotDeliveryOnThrusday == true)
                                AllowedDay = false;
                            break;
                        case "FRIDAY":
                            if (lstUPVendor[0].VendorDetails.APP_CannotDeliveryOnFriday == true)
                                AllowedDay = false;
                            break;
                        case "SATURDAY":
                            if (lstUPVendor[0].VendorDetails.APP_CannotDeliveryOnSaturday == true)
                                AllowedDay = false;
                            break;
                    }

                    if (AllowedDay == false)
                    {
                        bVendorValidation = false;
                        //error               
                        if (hdnCurrentRole.Value == "Vendor")
                        {
                            errMsg = WebCommon.getGlobalResourceValue("BK_MS_04_EXT");  // "This date is not available for your booking. Please select another date or contact Office Depot to make booking on this date.";
                            ShowErrorMessage(errMsg, "BK_MS_04_EXT");
                        }
                        else
                        {
                            errMsg = WebCommon.getGlobalResourceValue("BK_MS_04_INT"); // "There is a constraint against the vendor to deliver on this date, do you want to continue?";
                            AddWarningMessages(errMsg, "BK_MS_04_INT");
                        }
                    }



                    //Check Maximum Deliveries In A Day
                    if (lstUPVendor[0].VendorDetails.APP_MaximumDeliveriesInADay != null && lstUPVendor[0].VendorDetails.APP_MaximumDeliveriesInADay > 0)
                    {

                        APPBOK_BookingBE oAPPBOK_BookingBE = new APPBOK_BookingBE();
                        APPBOK_BookingBAL oAPPBOK_BookingBAL = new APPBOK_BookingBAL();

                        oAPPBOK_BookingBE.Action = "CheckVendorBooking";
                        oAPPBOK_BookingBE.SiteId = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
                        oAPPBOK_BookingBE.ScheduleDate = LogicalSchedulDateTime;
                        oAPPBOK_BookingBE.VendorID = Convert.ToInt32(ucSeacrhVendorID);

                        int? BookingDeatils = oAPPBOK_BookingBAL.CheckVendorBookingBAL(oAPPBOK_BookingBE);

                        if (BookingDeatils >= lstUPVendor[0].VendorDetails.APP_MaximumDeliveriesInADay)
                        {
                            bVendorValidation = false;
                            if (hdnCurrentRole.Value == "Vendor")
                            {
                                errMsg = WebCommon.getGlobalResourceValue("BK_MS_27_EXT"); // +" " + BookingDeatils.ToString() + ")."; // "Your maximum number of deliveries per day has already been reached (Max - " + BookingDeatils.ToString() + ").";
                                ShowErrorMessage(errMsg, "BK_MS_27_EXT");
                            }
                            else
                            {
                                errMsg = WebCommon.getGlobalResourceValue("BK_MS_27_INT_1") + " " + BookingDeatils.ToString() + ")." + WebCommon.getGlobalResourceValue("BK_MS_27_INT_2");  //"Maximum number of deliveries per day has been reached for this vendor (Max - " + BookingDeatils.ToString() + "). Do you want to continue?";
                                AddWarningMessages(errMsg, "BK_MS_27_INT");
                            }
                        }

                    }

                }

                //Befor Time and After Time
                if (lstUPVendor[0].VendorDetails.IsConstraintsDefined)
                {
                    BeforeSlotTimeID = lstUPVendor[0].VendorDetails.BeforeOrderBYId.ToString();
                    AfterSlotTimeID = lstUPVendor[0].VendorDetails.AfterOrderBYId.ToString() != "0" ? lstUPVendor[0].VendorDetails.AfterOrderBYId.ToString() : null;
                }
            }
        }
        //--Stage 5 Point 1----//   
        //if (bVendorValidation) {
        //    if (ddlSite.IsBookingExclusions) {
        //        bVendorValidation = CarrierValidationExc();
        //    }
        //}
        return bVendorValidation;
        //-----------------//
    }

    //--Stage 5 Point 1----//
    private bool CarrierValidationExc()
    {

        bool bCarrierValidation = true;

        if (ddlSite.innerControlddlSite.SelectedIndex > -1)
        {

            //Carrier Micellaneous Constraints -- 
            CarrierBeforeSlotTimeID = "0";
            CarrierAfterSlotTimeID = "300";
            string errMsg = string.Empty;

            MASCNT_CarrierBE oMASCNT_CarrierBE = new MASCNT_CarrierBE();
            APPCNT_CarrierBAL oAPPCNT_CarrierBAL = new APPCNT_CarrierBAL();

            if (ddlCarrier.SelectedIndex > 0)
            {
                oMASCNT_CarrierBE.Action = "GetAllCarrierMiscCons";
                oMASCNT_CarrierBE.CarrierID = Convert.ToInt32(ddlCarrier.SelectedItem.Value);
                oMASCNT_CarrierBE.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
                List<MASCNT_CarrierBE> lstCarrierBE = oAPPCNT_CarrierBAL.GetAllCarrierMiscConsBAL(oMASCNT_CarrierBE);

                ViewState.Add("lstCarrier", lstCarrierBE.ToArray());

                if (lstCarrierBE != null && lstCarrierBE.Count > 0)
                {

                    bool AllowedDay = true;

                    if (txtSchedulingdate.innerControltxtDate.Value != string.Empty)
                    {
                        string Weekday = LogicalSchedulDateTime.DayOfWeek.ToString().ToUpper();

                        switch (Weekday)
                        {
                            case "SUNDAY":
                                if (lstCarrierBE[0].APP_CannotDeliveryOnSunday == true)
                                    AllowedDay = false;
                                break;
                            case "MONDAY":
                                if (lstCarrierBE[0].APP_CannotDeliveryOnMonday == true)
                                    AllowedDay = false;
                                break;
                            case "TUESDAY":
                                if (lstCarrierBE[0].APP_CannotDeliveryOnTuesday == true)
                                    AllowedDay = false;
                                break;
                            case "WEDNESDAY":
                                if (lstCarrierBE[0].APP_CannotDeliveryOnWednessday == true)
                                    AllowedDay = false;
                                break;
                            case "THURSDAY":
                                if (lstCarrierBE[0].APP_CannotDeliveryOnThrusday == true)
                                    AllowedDay = false;
                                break;
                            case "FRIDAY":
                                if (lstCarrierBE[0].APP_CannotDeliveryOnFriday == true)
                                    AllowedDay = false;
                                break;
                            case "SATURDAY":
                                if (lstCarrierBE[0].APP_CannotDeliveryOnSaturday == true)
                                    AllowedDay = false;
                                break;
                        }

                        if (AllowedDay == false)
                        {
                            bCarrierValidation = false;
                            //error               
                            if (hdnCurrentRole.Value == "Vendor")
                            {
                                errMsg = WebCommon.getGlobalResourceValue("BK_MS_04_EXT_Vendor");  // "This date is not available for your booking. Please select another date or contact Office Depot to make booking on this date.";
                                ShowErrorMessage(errMsg, "BK_MS_04_EXT");
                            }
                            else
                            {
                                errMsg = WebCommon.getGlobalResourceValue("BK_MS_04_INT_Carrier"); // "There is a constraint against the vendor to deliver on this date, do you want to continue?";
                                AddWarningMessages(errMsg, "BK_MS_04_INT_Carrier");
                            }
                        }
                    }

                    //Befor Time and After Time               
                    CarrierBeforeSlotTimeID = lstCarrierBE[0].BeforeOrderBYId.ToString();
                    CarrierAfterSlotTimeID = lstCarrierBE[0].AfterOrderBYId.ToString() != "0" ? lstCarrierBE[0].AfterOrderBYId.ToString() : "300";

                    ShowWarningMessages();
                }
                //---------------------------------------------------//
            }
        }
        return bCarrierValidation;
    }
    //--------------------//

    public int CompareTime(string t1, string t2)
    {

        TimeSpan s1 = TimeSpan.Parse(t1);

        TimeSpan s2 = TimeSpan.Parse(t2);

        return s2.CompareTo(s1);

    }

    private void DeliveryValidationExc()
    {
        DataTable dt1;

        MASSIT_BookingTypeExclusionBE oMASCNT_BookingTypeExclusionBE = new MASSIT_BookingTypeExclusionBE();
        APPSIT_BookingTypeExclusionBAL oMASCNT_BookingTypeExclusionBAL = new APPSIT_BookingTypeExclusionBAL();

        oMASCNT_BookingTypeExclusionBE.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
        oMASCNT_BookingTypeExclusionBE.Action = "GetDeliveries";
        dt1 = oMASCNT_BookingTypeExclusionBAL.GetBookingTypeExclusionDetailsBAL(oMASCNT_BookingTypeExclusionBE, "");

        strDeliveryValidationExc = string.Empty;

        for (int iCount = 0; iCount < dt1.Rows.Count; iCount++)
        {
            strDeliveryValidationExc += dt1.Rows[iCount]["SiteDeliveryID"].ToString() + ",";
        }

        //Validate Delivery Type--//
        if (ddlDeliveryType.SelectedIndex > 0 && strDeliveryValidationExc.Contains(ddlDeliveryType.SelectedItem.Value + ","))
        {
            IsBookingValidationExcludedDelivery = true;
        }
        else
        {
            IsBookingValidationExcludedDelivery = false;
        }
        //--------------------//
    }

    private bool GetToleratedDays()
    {

        bool isError = false;

        if (txtSchedulingdate.innerControltxtDate.Value != string.Empty)
        {
            MAS_SiteBE oMAS_SITEBE = new MAS_SiteBE();
            MAS_SiteBAL oMAS_SITEBAL = new MAS_SiteBAL();

            List<MAS_SiteBE> lstSites = new List<MAS_SiteBE>();

            if (hdnCurrentRole.Value == "Vendor")
                oMAS_SITEBE.Action = "ShowAllForVendor";
            else
                oMAS_SITEBE.Action = "ShowAll";
            oMAS_SITEBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
            oMAS_SITEBE.User.UserID = Convert.ToInt32(Session["UserID"]);
            oMAS_SITEBE.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);

            lstSites = oMAS_SITEBAL.GetSiteMisSettingBAL(oMAS_SITEBE);

            if (lstSites != null && lstSites.Count > 0)
            {
                int? ActNoticePeriodDays = lstSites[0].BookingNoticePeriodInDays;

                int? NoticePeriodDays = lstSites[0].BookingNoticePeriodInDays;

                //--Stage 14 Point 16----//
                int SiteCloserDays = CheckSiteClosureByRange();
                if (SiteCloserDays > 0)
                {
                    if (NoticePeriodDays != null)
                        NoticePeriodDays = NoticePeriodDays + SiteCloserDays;
                    else
                        NoticePeriodDays = SiteCloserDays;
                }
                //---------------------------//

                DateTime SchDate = LogicalSchedulDateTime;
                DateTime CurrentDate = Utilities.Common.TextToDateFormat(DateTime.Now.Day.ToString() + "/" + DateTime.Now.Month.ToString() + "/" + DateTime.Now.Year.ToString());

                //TimeSpan DateDiff = SchDate - CurrentDate;
                //int DaysDiff = DateDiff.Days;
                //int DaysDiff = Enumerable.Range(0, Convert.ToInt32(SchDate.Subtract(CurrentDate).TotalDays)).Select(i => new[] { DayOfWeek.Saturday, DayOfWeek.Sunday }.Contains(CurrentDate.AddDays(i).DayOfWeek) ? 0 : 1).Sum();
                int DaysDiff = 0;
                if (SchDate >= CurrentDate)
                {
                    DaysDiff = Enumerable.Range(0, Convert.ToInt32(SchDate.Subtract(CurrentDate).TotalDays)).Select(i => new[] { DayOfWeek.Saturday, DayOfWeek.Sunday }.Contains(CurrentDate.AddDays(i).DayOfWeek) ? 0 : 1).Sum();
                }

                //string errMsg = WebCommon.getGlobalResourceValue("BK_MS_05_EXT_1") + " " + NoticePeriodDays + " " + WebCommon.getGlobalResourceValue("BK_MS_05_EXT_2");
                // "We required " + NoticePeriodDays + " days notification for all deliveries scheduled  into our sites, therefore we cannot accept this booking on the date you have requested. please select another date.";

                //---Stage 4 Point 4---
                //All deliveries scheduled into this site must be booked before 12:00 and 1 working day(s) before delivery, therefore we cannot accept this booking on the date you have requested. Please select another date or contact the site's booking in department.
                string errMsg = string.Empty;
                if (NoticePeriodDays == null || NoticePeriodDays == 0)
                {
                    if (lstSites[0].BookingNoticeTime != null)
                    {
                        string NoticeTime = Common.FormatTime(lstSites[0].BookingNoticeTime.Value.Hour.ToString()) + ":" + Common.FormatTime(lstSites[0].BookingNoticeTime.Value.Minute.ToString());
                        errMsg = WebCommon.getGlobalResourceValue("BK_MS_05_EXT_NEW_1") + " " +
                            WebCommon.getGlobalResourceValue("BK_MS_05_EXT_NEW_2").Replace("##Time##", NoticeTime) + " " +
                            WebCommon.getGlobalResourceValue("BK_MS_05_EXT_NEW_3").Replace("##NoticePeriodInDays##", "1");
                    }

                }
                //Commeted and chnages as per anthony mail 13 Sep 2013 
                //else if (NoticePeriodDays == 1) {
                //    if (lstSites[0].BookingNoticeTime != null) {
                //        string NoticeTime = Common.FormatTime(lstSites[0].BookingNoticeTime.Value.Hour.ToString()) + ":" + Common.FormatTime(lstSites[0].BookingNoticeTime.Value.Minute.ToString());
                //        errMsg = WebCommon.getGlobalResourceValue("BK_MS_05_EXT_NEW_1") + " " +
                //            WebCommon.getGlobalResourceValue("BK_MS_05_EXT_NEW_2").Replace("##Time##", NoticeTime) + " " +
                //            WebCommon.getGlobalResourceValue("BK_MS_05_EXT_NEW_3").Replace("##NoticePeriodInDays##", "1");
                //    }
                //    else if (lstSites[0].BookingNoticeTime == null) {
                //        errMsg = WebCommon.getGlobalResourceValue("BK_MS_05_EXT_NEW_1") + " " +
                //            WebCommon.getGlobalResourceValue("BK_MS_05_EXT_NEW_3").Replace("##NoticePeriodInDays##", "2");
                //    }
                //}
                else
                {
                    if (lstSites[0].BookingNoticeTime != null)
                    {
                        string NoticeTime = Common.FormatTime(lstSites[0].BookingNoticeTime.Value.Hour.ToString()) + ":" + Common.FormatTime(lstSites[0].BookingNoticeTime.Value.Minute.ToString());
                        errMsg = WebCommon.getGlobalResourceValue("BK_MS_05_EXT_NEW_1") + " " +
                            WebCommon.getGlobalResourceValue("BK_MS_05_EXT_NEW_2").Replace("##Time##", NoticeTime) + " " +
                            WebCommon.getGlobalResourceValue("BK_MS_05_EXT_NEW_3").Replace("##NoticePeriodInDays##", (ActNoticePeriodDays).ToString());
                    }
                    else
                    {
                        errMsg = WebCommon.getGlobalResourceValue("BK_MS_05_EXT_NEW_1") + " " +
                            WebCommon.getGlobalResourceValue("BK_MS_05_EXT_NEW_3").Replace("##NoticePeriodInDays##", (ActNoticePeriodDays).ToString());
                    }
                }
                //-----------------------//

                bool isAllowed = false;

                if (NoticePeriodDays == null)
                {
                    if (DaysDiff == 0)
                    {
                        DateTime dt1, dt2;
                        dt1 = new DateTime(1900, 01, 01, DateTime.Now.Hour, DateTime.Now.Minute, 00);
                        if (lstSites[0].BookingNoticeTime != null)
                            dt2 = new DateTime(1900, 01, 01, lstSites[0].BookingNoticeTime.Value.Hour, lstSites[0].BookingNoticeTime.Value.Minute, 00);
                        else
                            dt2 = new DateTime(1900, 01, 01, DateTime.Now.Hour, DateTime.Now.Minute, 00);

                        if (dt1 <= dt2)
                        {
                            isAllowed = true;
                        }
                    }
                    else
                    {
                        isAllowed = true;
                    }
                }

                if (isAllowed == false && DaysDiff >= NoticePeriodDays + 1)
                    isAllowed = true;

                if (isAllowed == false && DaysDiff == NoticePeriodDays)
                {
                    DateTime dt1, dt2;
                    dt1 = new DateTime(1900, 01, 01, DateTime.Now.Hour, DateTime.Now.Minute, 00);
                    if (lstSites[0].BookingNoticeTime != null)
                        dt2 = new DateTime(1900, 01, 01, lstSites[0].BookingNoticeTime.Value.Hour, lstSites[0].BookingNoticeTime.Value.Minute, 00);
                    else
                        dt2 = new DateTime(1900, 01, 01, DateTime.Now.Hour, DateTime.Now.Minute, 00);

                    if (dt1 <= dt2)
                    {
                        isAllowed = true;
                    }
                }

                if (isAllowed == false && NoticePeriodDays == 0 && SchDate > CurrentDate)
                {
                    isAllowed = true;
                }

                if (!isAllowed)
                {
                    //error
                    if (hdnCurrentRole.Value == "Vendor" || hdnCurrentRole.Value == "Carrier")
                    {
                        if (hdnCurrentMode.Value == "Add")
                        {
                            ShowErrorMessage(errMsg, "BK_MS_05_EXT");
                            return true;
                        }
                    }
                    else
                    {
                        errMsg = WebCommon.getGlobalResourceValue("BK_MS_05_INT"); // "There is insufficient notice given for this booking. Do you want to continue?";
                        AddWarningMessages(errMsg, "BK_MS_05_INT");
                    }
                }


            }

            ScriptManager.RegisterStartupScript(Page, this.GetType(), "id", "$get('" + btnProceedAddPO.ClientID + "').style.display='';", true);
        }
        return isError;
    }

    //--Stage 14 Point 16----//
    private int CheckSiteClosureByRange()
    {
        int iSiteClosureDays = 0;

        if (txtSchedulingdate.innerControltxtDate.Value != string.Empty && ddlSite.innerControlddlSite.SelectedItem.Value != string.Empty)
        {

            MASSIT_HolidayBE oMASSIT_HolidayBE = new MASSIT_HolidayBE();
            MASSIT_HolidayBAL oMASSIT_HolidayBAL = new MASSIT_HolidayBAL();

            oMASSIT_HolidayBE.Action = "ShowAllByRange";
            oMASSIT_HolidayBE.Site = new BusinessEntities.ModuleBE.AdminFunctions.MAS_SiteBE();
            if (ddlSite.innerControlddlSite.SelectedIndex >= 0)
                oMASSIT_HolidayBE.Site.SiteID = !string.IsNullOrEmpty(ddlSite.innerControlddlSite.SelectedValue) ? Convert.ToInt32(ddlSite.innerControlddlSite.SelectedValue) : 0;

            oMASSIT_HolidayBE.HolidayDate = LogicalSchedulDateTime;

            List<MASSIT_HolidayBE> lstHoliday = oMASSIT_HolidayBAL.GetHolidaysBAL(oMASSIT_HolidayBE);

            if (lstHoliday != null && lstHoliday.Count > 0)
            {
                iSiteClosureDays = lstHoliday.Count;
            }
        }
        return iSiteClosureDays;
    }
    //---------------------//


    private void GetMaximumCapacity()
    {
        if (txtSchedulingdate.innerControltxtDate.Value != string.Empty)
        {

            MASSIT_WeekSetupBE oMASSIT_WeekSetupBE = new MASSIT_WeekSetupBE();
            MASSIT_WeekSetupBAL oMASSIT_WeekSetupBAL = new MASSIT_WeekSetupBAL();

            oMASSIT_WeekSetupBE.Action = "GetSpecificWeek";
            oMASSIT_WeekSetupBE.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
            oMASSIT_WeekSetupBE.EndWeekday = LogicalSchedulDateTime.DayOfWeek.ToString();
            oMASSIT_WeekSetupBE.ScheduleDate = LogicalSchedulDateTime;

            List<MASSIT_WeekSetupBE> lstWeekSetup = oMASSIT_WeekSetupBAL.GetWeekSetupDetailsBAL(oMASSIT_WeekSetupBE);

            if (lstWeekSetup == null || lstWeekSetup.Count <= 0)
            {
                oMASSIT_WeekSetupBE.Action = "ShowAll";
                oMASSIT_WeekSetupBE.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
                oMASSIT_WeekSetupBE.EndWeekday = LogicalSchedulDateTime.DayOfWeek.ToString();
                lstWeekSetup = oMASSIT_WeekSetupBAL.GetWeekSetupDetailsBAL(oMASSIT_WeekSetupBE);
            }

            string errMsg;

            if (lstWeekSetup != null && lstWeekSetup.Count > 0)
            {

                //Set Current Running Total of alternate screen
                //ltMaximumLifts.Text = lstWeekSetup[0].MaximumLift != (int?)null ? lstWeekSetup[0].MaximumLift.ToString() : "-";
                //ltMaximumPallets.Text = lstWeekSetup[0].MaximumPallet != (int?)null ? lstWeekSetup[0].MaximumPallet.ToString() : "-";
                //ltMaximumLines.Text = lstWeekSetup[0].MaximumLine != (int?)null ? lstWeekSetup[0].MaximumLine.ToString() : "-";
                //-----------------------------------//

                APPBOK_BookingBE oAPPBOK_BookingBE = new APPBOK_BookingBE();
                APPBOK_BookingBAL oAPPBOK_BookingBAL = new APPBOK_BookingBAL();

                oAPPBOK_BookingBE.Action = "GetBookedVolume";
                oAPPBOK_BookingBE.SiteId = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
                oAPPBOK_BookingBE.ScheduleDate = LogicalSchedulDateTime;
                oAPPBOK_BookingBE.BookingType = "W";

                //----Stage 10 Point 10 L 5.1-----//
                if (hdnCurrentMode.Value == "Edit")
                    oAPPBOK_BookingBE.BookingID = Convert.ToInt32(GetQueryStringValue("ID").ToString());
                //---------------------------------//

                List<APPBOK_BookingBE> lstBookedVolume = oAPPBOK_BookingBAL.GetBookedVolumeBAL(oAPPBOK_BookingBE);

                if (lstBookedVolume != null && lstBookedVolume.Count > 0)
                {

                    //Set Current Running Total of alternate screen//
                    //ltRemainingLifts.Text = lstWeekSetup[0].MaximumLift != (int?)null ? (lstWeekSetup[0].MaximumLift - lstBookedVolume[0].NumberOfLift).ToString() : "-";
                    hdnRemainingPallets.Value = lstWeekSetup[0].MaximumPallet != (int?)null && (lstWeekSetup[0].MaximumPallet - lstBookedVolume[0].NumberOfPallet) > 0 ? (lstWeekSetup[0].MaximumPallet - lstBookedVolume[0].NumberOfPallet).ToString() : "-";
                    hdnRemainingLines.Value = lstWeekSetup[0].MaximumLine != (int?)null && (lstWeekSetup[0].MaximumLine - lstBookedVolume[0].NumberOfLines) > 0 ? (lstWeekSetup[0].MaximumLine - lstBookedVolume[0].NumberOfLines).ToString() : "-";
                    //----Stage 10 Point 10 L 5.1-----//
                    //RemainingCartons = lstWeekSetup[0].MaximumContainer != (int?)null ? (lstWeekSetup[0].MaximumContainer - lstBookedVolume[0].NumberOfContainer).ToString() : "-";
                    //---------------------------------//

                    RemainingPallets = lstWeekSetup[0].MaximumPallet != (int?)null ? Convert.ToInt32(lstWeekSetup[0].MaximumPallet) - Convert.ToInt32(lstBookedVolume[0].NumberOfPallet) : (int?)null;
                    RemainingLines = lstWeekSetup[0].MaximumLine != (int?)null ? Convert.ToInt32(lstWeekSetup[0].MaximumLine) - Convert.ToInt32(lstBookedVolume[0].NumberOfLines) : (int?)null;

                    //-----------------------------------//


                    //if not the confirm fixed slot case
                    //check
                    if (hdnSuggestedWindowSlotID.Value == "-1" || hdnSuggestedWindowSlotID.Value == string.Empty)
                    {
                        if (lstBookedVolume[0].NumberOfLines >= (lstWeekSetup[0].MaximumLine > 0 ? lstWeekSetup[0].MaximumLine : 99999)
                             || lstBookedVolume[0].NumberOfPallet >= (lstWeekSetup[0].MaximumPallet > 0 ? lstWeekSetup[0].MaximumPallet : 99999)
                             || lstBookedVolume[0].NumberOfDeliveries >= (lstWeekSetup[0].MaximumDeliveries > 0 ? lstWeekSetup[0].MaximumDeliveries : 99999))
                        {

                            //error
                            if (hdnCurrentRole.Value == "Vendor" || hdnCurrentRole.Value == "Carrier")
                            {
                                if (hdnCurrentMode.Value == "Add")
                                {
                                    errMsg = WebCommon.getGlobalResourceValue("BK_MS_06_EXT"); // "There is no capacity for any further deliveries on the date selected. Please select a different date or, if the delivery is urgent, please contact the booking office.";
                                    ShowErrorMessage(errMsg, "BK_MS_06_EXT");
                                    return;
                                }
                            }
                            else
                            {
                                if (!(hdnCurrentMode.Value == "Edit" && GetQueryStringValue("BP") != null && GetQueryStringValue("BP").ToString() == "Pro"))
                                {  // If coming from Provisional page ,ignore capacity warning -- Phase 12 R2 Point 2
                                    errMsg = WebCommon.getGlobalResourceValue("BK_MS_06_INT"); // "There is no capacity for any further deliveries on the date selected. Click CONTINUE to make booking on this date.";
                                    AddWarningMessages(errMsg, "BK_MS_06_INT");
                                }
                            }
                        }

                        //-----------Point 10L-5.1---------------//                        

                        if (lstBookedVolume[0].NumberOfContainer >= (lstWeekSetup[0].MaximumContainer > 0 ? lstWeekSetup[0].MaximumContainer : 99999))
                        {
                            if (ddlVehicleType.SelectedIndex > -1)
                            {
                                if (IsVehicleContainer.Contains("," + ddlVehicleType.SelectedItem.Value + ","))
                                {
                                    if (hdnCurrentRole.Value == "Vendor" || hdnCurrentRole.Value == "Carrier")
                                    {
                                        //if (hdnCurrentMode.Value == "Add") {
                                        errMsg = WebCommon.getGlobalResourceValue("BK_MS_06_Ext_MaxContainers"); // The maximum amount of containers for this date has been reached.....
                                        ShowErrorMessage(errMsg, "BK_MS_06_Ext_MaxContainers");
                                        return;
                                        // }
                                    }
                                    else
                                    {
                                        errMsg = WebCommon.getGlobalResourceValue("BK_MS_06_INT_MaxContainers"); //The maximum amount of containers for this date has been reached. Do you want to continue?
                                        AddWarningMessages(errMsg, "BK_MS_06_INT_MaxContainers");
                                    }
                                }
                            }
                            isContainerError = true;
                        }
                        //----------------------------------------//

                    }
                }
            }
            ScriptManager.RegisterStartupScript(Page, this.GetType(), "id", "$get('" + btnProceedAddPO.ClientID + "').style.display='';", true);
        }
    }

    protected void ddlCarrier_SelectedIndexChanged(object sender, EventArgs e)
    {
        txtCarrierOther.Text = string.Empty;

        if (ddlCarrier.Items.Count > 0)
        {
            rfvCarrierOtherRequired.Enabled = false;
            if (CarrierNarrativeRequired.Contains("," + ddlCarrier.SelectedItem.Value + ","))
            {
                txtCarrierOther.Style.Add("display", "");
                txtCarrierOther.Visible = true;
                lblCarrierAdvice.Style.Add("display", "");
                rfvCarrierOtherRequired.Enabled = true;
            }
            else
            {
                txtCarrierOther.Style.Add("display", "none");
                txtCarrierOther.Visible = false;
                lblCarrierAdvice.Style.Add("display", "none");
                rfvCarrierOtherRequired.Enabled = false;
            }
        }
        else
        {
            txtCarrierOther.Style.Add("display", "none");
            lblCarrierAdvice.Style.Add("display", "none");
            rfvCarrierOtherRequired.Enabled = false;
        }

        //--Stage 5 Point 1----//      

        if (ddlSite.IsBookingExclusions)
        {
            CarrierValidationExc();
        }
        //-----------------//
    }

    protected void ddlVehicleType_SelectedIndexChanged(object sender, EventArgs e)
    {
        txtVehicleOther.Text = string.Empty;
        //-----------Point 10L-5.1------------//
        string errMsg = string.Empty;
        if (ddlVehicleType.SelectedIndex > -1 && !string.IsNullOrEmpty(txtSchedulingdate.innerControltxtDate.Value))
        {
            if (IsVehicleContainer.Contains("," + ddlVehicleType.SelectedItem.Value + ","))
            {

                if (isContainerError == true)
                {
                    if (hdnCurrentRole.Value == "Vendor" || hdnCurrentRole.Value == "Carrier")
                    {
                        //if (hdnCurrentMode.Value == "Add") {
                        errMsg = WebCommon.getGlobalResourceValue("BK_MS_06_Ext_MaxContainers"); // The maximum amount of containers for this date has been reached.....
                        ShowErrorMessage(errMsg, "BK_MS_06_Ext_MaxContainers");
                        return;
                        //}
                    }
                    else
                    {
                        errMsg = WebCommon.getGlobalResourceValue("BK_MS_06_INT_MaxContainers"); //The maximum amount of containers for this date has been reached. Do you want to continue?
                        AddWarningMessages(errMsg, "BK_MS_06_INT_MaxContainers");
                    }
                }
            }
        }

        if (!string.IsNullOrEmpty(errMsg))
            ShowWarningMessages();
        else
            VehicleTypeSelect();
    }

    private void VehicleTypeSelect()
    {
        //------------------------------------------//     

        if (ddlVehicleType.SelectedIndex > -1)
        {
            rfvVehicleTypeOtherRequired.Enabled = false;
            if (VehicleNarrativeRequired.Contains(ddlVehicleType.SelectedItem.Value + ","))
            {
                txtVehicleOther.Style.Add("display", "");
                txtVehicleOther.Visible = true;
                lblVehicleAdvice.Style.Add("display", "");
                rfvVehicleTypeOtherRequired.Enabled = true;
            }
            else
            {
                txtVehicleOther.Style.Add("display", "none");
                txtVehicleOther.Visible = false;
                lblVehicleAdvice.Style.Add("display", "none");
                rfvVehicleTypeOtherRequired.Enabled = false;
            }

            //Get Door Type 
            strVehicleDoorTypeMatrix = string.Empty;
            MASSIT_VehicleDoorMatrixBE oMASSIT_VehicleDoorMatrixBE = new MASSIT_VehicleDoorMatrixBE();

            APPSIT_VehicleDoorMatrixBAL oMASSIT_VehicleDoorMatrixBAL = new APPSIT_VehicleDoorMatrixBAL();

            oMASSIT_VehicleDoorMatrixBE.Action = "ShowAll";
            oMASSIT_VehicleDoorMatrixBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
            oMASSIT_VehicleDoorMatrixBE.User.UserID = Convert.ToInt32(Session["UserID"]);
            oMASSIT_VehicleDoorMatrixBE.SiteID = Convert.ToInt16(ddlSite.innerControlddlSite.SelectedItem.Value);

            List<MASSIT_VehicleDoorMatrixBE> lstVehicleDoorMatrix = oMASSIT_VehicleDoorMatrixBAL.GetVehicleDoorMatrixDetailsBAL(oMASSIT_VehicleDoorMatrixBE);

            //---FOR SKU DOOR MATRIX, NEED TO SKIP VEHICLE DOOR MATIX CONSTRAINTS--- SPRINT 4 POINT 7
            NameValueCollection collection = new NameValueCollection();
            for (int iCount = 0; iCount < lstVehicleDoorMatrix.Count; iCount++)
            {
                strVehicleDoorTypeMatrixSKU += lstVehicleDoorMatrix[iCount].DoorTypeID.ToString() + ",";
                collection.Add(lstVehicleDoorMatrix[iCount].DoorTypeID.ToString(), lstVehicleDoorMatrix[iCount].Priority.ToString());
            }

            ArrDoorTypePrioritySKU = new NameValueCollection();
            foreach (string key in collection.AllKeys)
            {
                string Val = collection.Get(key);
                string[] ArrVal = Val.Split(',');
                ArrDoorTypePrioritySKU.Add(key, ArrVal[0].ToString());
            }
            //---------------------------------------------------------------------------------------

            //Get Door Type as per selected Vehicle Type
            lstVehicleDoorMatrix = lstVehicleDoorMatrix.Where(Matrix => Matrix.VehicleTypeID == Convert.ToInt32(ddlVehicleType.SelectedItem.Value)).ToList();

            collection = new NameValueCollection();

            for (int iCount = 0; iCount < lstVehicleDoorMatrix.Count; iCount++)
            {
                strVehicleDoorTypeMatrix += lstVehicleDoorMatrix[iCount].DoorTypeID.ToString() + ",";
                collection.Add(lstVehicleDoorMatrix[iCount].DoorTypeID.ToString(), lstVehicleDoorMatrix[iCount].Priority.ToString());
            }

            ArrDoorTypePriority = new NameValueCollection();
            foreach (string key in collection.AllKeys)
            {
                string Val = collection.Get(key);
                string[] ArrVal = Val.Split(',');
                ArrDoorTypePriority.Add(key, ArrVal[0].ToString());
            }

        }
        //-----------------//        
    }

    protected void ddlDeliveryType_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (strDeliveryValidationExc.Contains(ddlDeliveryType.SelectedItem.Value + ","))
        {
            IsBookingValidationExcludedDelivery = true;
        }
        else
        {
            IsBookingValidationExcludedDelivery = false;
        }
    }

    public override void PostVendorNo_Change()
    {
        base.PostVendorNo_Change();


        ucSeacrhVendorID = ucSeacrhVendor1.VendorNo;
        ucSeacrhVendorName = ((HiddenField)ucSeacrhVendor1.FindControl("hdnVendorName")).Value;
        ucSeacrhVendorNo = ((ucTextbox)ucSeacrhVendor1.FindControl("txtVendorNo")).Text;
        ucSeacrhVendorParentID = ((HiddenField)ucSeacrhVendor1.FindControl("hdnParentVendorID")).Value;

        ViewState["myDataTable"] = null;
        ViewState["dtShowPO"] = null;
        DataTable myDataTable = null;
        gvPO.DataSource = myDataTable;
        gvPO.DataBind();
        grdShowPO.DataSource = myDataTable;
        grdShowPO.DataBind();
        VendorValidationExc();
        ShowWarningMessages();
    }

    //--Stage 3 Point 4----//
    private bool CheckSiteClosure()
    {
        bool bSiteClosure = false;
        if (txtSchedulingdate.innerControltxtDate.Value != string.Empty && ddlSite.innerControlddlSite.SelectedItem.Value != string.Empty)
        {

            MASSIT_HolidayBE oMASSIT_HolidayBE = new MASSIT_HolidayBE();
            MASSIT_HolidayBAL oMASSIT_HolidayBAL = new MASSIT_HolidayBAL();

            oMASSIT_HolidayBE.Action = "ShowAll";
            oMASSIT_HolidayBE.Site = new BusinessEntities.ModuleBE.AdminFunctions.MAS_SiteBE();
            if (ddlSite.innerControlddlSite.SelectedIndex >= 0)
                oMASSIT_HolidayBE.Site.SiteID = !string.IsNullOrEmpty(ddlSite.innerControlddlSite.SelectedValue) ? Convert.ToInt32(ddlSite.innerControlddlSite.SelectedValue) : 0;

            oMASSIT_HolidayBE.HolidayDate = LogicalSchedulDateTime;

            List<MASSIT_HolidayBE> lstHoliday = oMASSIT_HolidayBAL.GetHolidaysBAL(oMASSIT_HolidayBE);

            if (lstHoliday != null && lstHoliday.Count > 0)
            {
                string errMsg = WebCommon.getGlobalResourceValue("BK_MS_36"); // Site is closed on this date. Please select a different date.
                ShowErrorMessage(errMsg, "BK_MS_36");
                bSiteClosure = true;
            }
        }
        return bSiteClosure;
    }
    //---------------------//

    public override void PostDate_Change()
    {
        base.PostDate_Change();
        Date_Change(true);
    }

    private bool Date_Change(bool isDateChange)
    {
        if (ltSCdate.Visible == true)
        {
            if (LogicalSchedulDateTime < DateTime.Now.Date)
            {
                string errMsg = WebCommon.getGlobalResourceValue("BK_MS_05_EXT"); // "Booking not allowed in past date.";
                ShowErrorMessage(errMsg, "BK_MS_05_EXT");
                return false;
            }
        }
        else
        {
            if (txtSchedulingdate.GetDate.Date < DateTime.Now.Date)
            {
                string errMsg = WebCommon.getGlobalResourceValue("BK_MS_05_EXT"); // "Booking not allowed in past date.";
                ShowErrorMessage(errMsg, "BK_MS_05_EXT");
                return false;
            }
        }

        if (isDateChange)
        {
            hdnActualScheduleDate.Value = txtSchedulingdate.innerControltxtDate.Value;
            hdnLogicalScheduleDate.Value = txtSchedulingdate.innerControltxtDate.Value;

            LogicalSchedulDateTime = txtSchedulingdate.GetDate; // Utilities.Common.TextToDateFormat(txtSchedulingdate.innerControltxtDate.Value);
            ActualSchedulDateTime = txtSchedulingdate.GetDate; // Utilities.Common.TextToDateFormat(txtSchedulingdate.innerControltxtDate.Value);
        }

        if (CheckSiteClosure())
            return false;

        if (!GetToleratedDays())
        {
            GetMaximumCapacity();

            VendorValidationExc();

            //--Stage 5 Point 1----//      

            if (ddlSite.IsBookingExclusions)
            {
                CarrierValidationExc();
            }
            //-----------------//

            if (txtSchedulingdate.innerControltxtDate.Value != string.Empty && ddlSite.innerControlddlSite.SelectedItem.Value != string.Empty)
            {
                bool IsWeekConstraints = GetWeekConstraints();

                if (!IsWeekConstraints)
                {
                    string errMsg = WebCommon.getGlobalResourceValue("BK_MS_28_EXT"); // "No space available on any compatible doors.Please contact Goods In office or select a different date.";

                    if (hdnCurrentRole.Value == "Vendor")
                    {
                        //Show err msg
                        ShowErrorMessage(errMsg, "BK_MS_28");
                        return false;
                    }
                    else
                    {
                        //Show err msg
                        errMsg = WebCommon.getGlobalResourceValue("BK_MS_28_INT"); //"No space available on any compatible doors.";
                        ShowErrorMessage(errMsg, "BK_MS_28");
                        return false;
                    }
                }
            }
        }

        ShowWarningMessages();
        return true;
    }

    protected void btnProceedAddPO_Click(object sender, EventArgs e)
    {
        if (!txtSchedulingdate.isPostBackDone)
        {
            if (!Date_Change(false))
            {
                return;
            }
        }
        else
        {
            if (CheckSiteClosure())
                return;
        }

        if (ddlCarrier.SelectedIndex <= 0 || ddlDeliveryType.SelectedIndex <= 0 || ddlVehicleType.SelectedIndex <= 0)
        {
            return;
        }

        if (ltSCdate.Visible == false)
        {
            hdnActualScheduleDate.Value = txtSchedulingdate.innerControltxtDate.Value;
            hdnLogicalScheduleDate.Value = txtSchedulingdate.innerControltxtDate.Value;

            LogicalSchedulDateTime = txtSchedulingdate.GetDate; // Utilities.Common.TextToDateFormat(txtSchedulingdate.innerControltxtDate.Value);
            ActualSchedulDateTime = txtSchedulingdate.GetDate; // Utilities.Common.TextToDateFormat(txtSchedulingdate.innerControltxtDate.Value);
        }

        if (chkSiteSetting.Checked)
        {
            lblFromCountryValue.Text = ddlSourceCountry.innerControlddlCountry.SelectedItem.Text;
        }

        ProceedAddPO();
    }

    private void ProceedAddPO()
    {
        ltSiteName.Text = ddlSite.innerControlddlSite.SelectedItem.Text;
        if (hdnCurrentMode.Value == "Edit")
            ltvendorName.Text = ucSeacrhVendorNo + " - " + ucSeacrhVendorName; // ((HiddenField)ucSeacrhVendor1.FindControl("hdnVandorName")).Value;
        else if (hdnCurrentMode.Value == "Add")
            ltvendorName.Text = ucSeacrhVendorName;
        ltDeliveryType.Text = ddlDeliveryType.SelectedItem.Text;
        ltCarrier.Text = ddlCarrier.SelectedItem.Text;
        ltOtherCarrier.Text = "[" + txtCarrierOther.Text.Trim() + "]";
        if (txtCarrierOther.Text.Trim() != string.Empty)
        {
            ltOtherCarrier.Visible = true;
        }
        else
        {
            ltOtherCarrier.Visible = false;
        }

        ltVehicleType.Text = ddlVehicleType.SelectedItem.Text;
        ltOtherVehicleType.Text = "[" + txtVehicleOther.Text.Trim() + "]";
        if (txtVehicleOther.Text.Trim() != string.Empty)
        {
            ltOtherVehicleType.Visible = true;
        }
        else
        {
            ltOtherVehicleType.Visible = false;
        }

        // Sprint 1 - Point 4 - Start - For Booking Comments.
        txtEditBookingComment.Text = txtBookingComment.Text;
        // Sprint 1 - Point 4 - End - For Booking Comments.

        ltSchedulingDate.Text = ActualSchedulDateTime.ToString("dd/MM/yyyy");

        mvBookingEdit.ActiveViewIndex = 2;

        ltExpectedLines_1.Text = WebCommon.getGlobalResourceValue("ExpectedLines");

        if (hdnCurrentMode.Value == "Add")
        {
            // Sprint 1 - Point 7 - Passed purchase order number as a parameter and then set tp blank
            BindPOGrid(txtPurchaseNumber.Text);
            txtPurchaseNumber.Text = string.Empty;
        }
        else if (hdnCurrentMode.Value == "Edit")
            BindAllPOGrid();

        ShowHazordousItem();
        if (checkBoxEnableHazardouesItemPrompt.Checked == true)
        {
            chkHazardoustick.Checked = true;
            chkHazardoustick.Enabled = false;
        }
        else
        {
            chkHazardoustick.Enabled = false;
        }
    }

    protected void btnAddPO_Click(object sender, EventArgs e)
    {
        string errMsg = string.Empty;

        if (txtPurchaseNumber.Text != string.Empty)
        {
            string messageSeparator = ", ";
            DataTable myDataTable = null;

            if (ViewState["myDataTable"] != null)
            {
                bool Multiple = false;
                myDataTable = (DataTable)ViewState["myDataTable"];

                //Check Item alrady added
                DataRow[] drr = myDataTable.Select("PurchaseNumber='" + txtPurchaseNumber.Text.Trim() + "'");

                if (drr.Length > 0)
                {
                    // // Sprint 1 - Point 7 - Shown error message when adding PO using Add PO button 
                    // otherwise saved PO number in view state.
                    if (!Multiple)
                    {
                        errMsg = WebCommon.getGlobalResourceValue("PurchaseOrderExist"); //"The Purchase Order Number you have supplied already exists.";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "popup", "<script>alert('" + errMsg + "');</script>", false);
                    }
                    else
                    {
                        AlreadyExistsPOs += txtPurchaseNumber.Text.Trim() + messageSeparator;
                    }
                    return;
                }
            }

            IsPOValid();

        }
    }

    private void IsPOValid()
    {
        string errMsg = string.Empty;
        bool isWarning = false;

        if (IsBookingValidationExcluded == false)
        {

            Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE = new Up_PurchaseOrderDetailBE();
            UP_PurchaseOrderDetailBAL oUP_PurchaseOrderDetailBAL = new UP_PurchaseOrderDetailBAL();

            oUp_PurchaseOrderDetailBE.Action = "GetAllPODetails";
            oUp_PurchaseOrderDetailBE.Purchase_order = txtPurchaseNumber.Text.Trim();
            oUp_PurchaseOrderDetailBE.CountryID = Convert.ToInt32(PreSiteCountryID);

            List<Up_PurchaseOrderDetailBE> lstPO = oUP_PurchaseOrderDetailBAL.GetAllPODetailsBAL(oUp_PurchaseOrderDetailBE);
            if (lstPO.Count <= 0)
            {
                errMsg = WebCommon.getGlobalResourceValue("BK_MS_11_EXT"); // "The Purchase Order Number you have supplied is not valid, please enter correct Purchase Order Number.";
                ShowErrorMessage(errMsg, "BK_MS_11_EXT");
                return;


            }

            if (lstPO.Count > 0)
            {
                if (lstPO[0].LastUploadedFlag == "N")
                {
                    errMsg = WebCommon.getGlobalResourceValue("BK_MS_11_EXT"); //"The Purchase Order Number you have supplied is not valid, please enter correct Purchase Order Number.";
                    ShowErrorMessage(errMsg, "BK_MS_11_EXT");
                    return;
                }
            }
        }

        if (isWarning == true)
        {
            ShowWarningMessages();
        }
        else
        {
            // Sprint 1 - Point 1 - Start - Checking PO is existing or not.
            APPBOK_BookingBE appbok_BookingBE = new APPBOK_BookingBE();
            appbok_BookingBE.Action = "IsExistingPO";
            appbok_BookingBE.PurchaseOrders = txtPurchaseNumber.Text;
            appbok_BookingBE.VendorID = Convert.ToInt32(ucSeacrhVendorID);
            appbok_BookingBE.SiteId = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
            APPBOK_BookingBAL appbok_BookingBAL = new APPBOK_BookingBAL();
            List<APPBOK_BookingBE> lstAPPBOK_BookingBE = appbok_BookingBAL.IsExistingPO(appbok_BookingBE);

            // Sprint 1 - Point 1 - End

            if (lstAPPBOK_BookingBE.Count == 0)
            {
                // Sprint 1 - Point 7 - Passed purchase order number as a parameter and then set tp blank
                this.AddPO(txtPurchaseNumber.Text, false, true);
                //txtPurchaseNumber.Text = string.Empty;
            }
            else
            {
                foreach (APPBOK_BookingBE oAPPBOK_BookingBE in lstAPPBOK_BookingBE)
                {
                    #region Double booking warning message formatting.
                    errMsg = string.Format("<table><tr><td style=\"text-align:center\">{0}</td></tr>",
                        WebCommon.getGlobalResourceValue("BK_MS_29_Warning_1"));

                    errMsg = string.Format("{0}<tr><td style=\"text-align: center\">{1}</td></tr>",
                        errMsg, WebCommon.getGlobalResourceValue("BK_MS_29_Warning_2").Replace("##dd/mm/yyyy##", "<b>" + Convert.ToDateTime(oAPPBOK_BookingBE.ScheduleDate).ToString("dd/MM/yyyy") + "</b>"));

                    errMsg = string.Format("{0}<tr><td style=\"text-align: center\">{1}</td></tr>",
                        errMsg, WebCommon.getGlobalResourceValue("BK_MS_29_Warning_3").Replace("##tt:mm##", String.Format("{0}", "<b>" + oAPPBOK_BookingBE.SlotTime.SlotTime) + "</b>"));

                    errMsg = string.Format("{0}<tr><td style=\"text-align: center\">{1}</td></tr></table>",
                        errMsg, WebCommon.getGlobalResourceValue("BK_MS_29_Warning_4"));
                    #endregion
                    break; // Here being break because need only zero index value.
                }

                ltPOConfirmMsg.Text = errMsg;
                mdlPOConfirmMsg.Show();
            }
        }
    }

    // Sprint 1 - Point 7 - Passed purchase order number as a parameter.
    // Also passed additional field Multiple (Used when adding POs from popup).
    private void AddPO(string PurchaseNumber, bool Multiple = false, bool isValidPOChecked = false)
    {
        string errMsg = string.Empty;
        bool isWarning = false;
        string messageSeparator = ", ";

        if (PurchaseNumber != string.Empty)
        {
            DataTable myDataTable = null;
            if (ViewState["myDataTable"] != null)
            {
                myDataTable = (DataTable)ViewState["myDataTable"];

                //Check Item alrady added
                DataRow[] drr = myDataTable.Select("PurchaseNumber='" + PurchaseNumber.Trim() + "'");

                if (drr.Length > 0)
                {
                    // // Sprint 1 - Point 7 - Shown error message when adding PO using Add PO button 
                    // otherwise saved PO number in view state.
                    if (!Multiple)
                    {
                        errMsg = WebCommon.getGlobalResourceValue("PurchaseOrderExist"); //"The Purchase Order Number you have supplied already exists.";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "popup", "<script>alert('" + errMsg + "');</script>", false);
                    }
                    else
                    {
                        AlreadyExistsPOs += PurchaseNumber.Trim() + messageSeparator;
                    }
                    return;
                }
            }

            if (IsBookingValidationExcluded == false)
            {  //If there is no Validation Exclusion for the vendor/carrier or booking type             

                Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE = new Up_PurchaseOrderDetailBE();
                UP_PurchaseOrderDetailBAL oUP_PurchaseOrderDetailBAL = new UP_PurchaseOrderDetailBAL();

                oUp_PurchaseOrderDetailBE.Action = "GetAllPODetails";
                oUp_PurchaseOrderDetailBE.Purchase_order = PurchaseNumber.Trim();
                oUp_PurchaseOrderDetailBE.CountryID = Convert.ToInt32(PreSiteCountryID);

                List<Up_PurchaseOrderDetailBE> lstPO = oUP_PurchaseOrderDetailBAL.GetAllPODetailsBAL(oUp_PurchaseOrderDetailBE);

                if (!isValidPOChecked)
                {
                    if (lstPO.Count <= 0)
                    {
                        errMsg = WebCommon.getGlobalResourceValue("BK_MS_11_EXT"); // "The Purchase Order Number you have supplied is not valid, please enter correct Purchase Order Number.";
                        ShowErrorMessage(errMsg, "BK_MS_11_EXT");
                        return;


                    }
                }

                if (lstPO.Count > 0)
                {
                    if (!isValidPOChecked)
                    {
                        if (lstPO[0].LastUploadedFlag == "N")
                        {
                            errMsg = WebCommon.getGlobalResourceValue("BK_MS_11_EXT"); //"The Purchase Order Number you have supplied is not valid, please enter correct Purchase Order Number.";
                            ShowErrorMessage(errMsg, "BK_MS_11_EXT");
                            return;
                        }
                    }

                    if (Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value) != lstPO[0].Site.SiteID)
                    {
                        //Cross docking check

                        MASCNT_CrossDockBE oMASCNT_CrossDockBE = new MASCNT_CrossDockBE();
                        APPCNT_CrossDockBAL oMASCNT_CrossDockBAL = new APPCNT_CrossDockBAL();

                        oMASCNT_CrossDockBE.Action = "GetDestSiteById";
                        oMASCNT_CrossDockBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
                        oMASCNT_CrossDockBE.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);

                        List<MASCNT_CrossDockBE> lstCrossDock = oMASCNT_CrossDockBAL.GetCrossDockDetailsBAL(oMASCNT_CrossDockBE);

                        if (lstCrossDock != null && lstCrossDock.Count > 0)
                        {

                            lstCrossDock = lstCrossDock.FindAll(delegate (MASCNT_CrossDockBE cd)
                            {
                                return cd.DestinationSiteID == lstPO[0].Site.SiteID;
                            });
                        }

                        if (lstCrossDock == null || lstCrossDock.Count == 0)
                        {
                            if (hdnCurrentRole.Value == "Vendor" || hdnCurrentRole.Value == "Carrier")
                            {
                                errMsg = WebCommon.getGlobalResourceValue("BK_MS_14_EXT").Replace("##Ponumber##", PurchaseNumber.Trim()).Replace("##Sitename##", lstPO[0].Site.SiteName);
                                //"Please recheck the entries made and resubmit the correct information";
                                ShowErrorMessage(errMsg, "BK_MS_14_EXT");
                                return;
                            }
                            else
                            {
                                errMsg = WebCommon.getGlobalResourceValue("BK_MS_14_INT").Replace("##Ponumber##", PurchaseNumber.Trim()).Replace("##Sitename##", lstPO[0].Site.SiteName);  //"The Purchase Order you have quoted is not for the Office Depot site you have selected. Do you want to continue?";
                                AddWarningMessages(errMsg, "BK_MS_14_INT");
                                isWarning = true;
                            }
                        }

                    }

                    UP_VendorBE oUP_VendorBE = new UP_VendorBE();
                    oUP_VendorBE.Action = "GetConsolidateVendors";
                    oUP_VendorBE.VendorID = Convert.ToInt32(ucSeacrhVendorID);
                    List<UP_VendorBE> lstConsolidateVendors = oUP_PurchaseOrderDetailBAL.GetConsolidateVendorsBAL(oUP_VendorBE);

                    List<int> VenIds = new List<int>();

                    foreach (var p in lstPO)
                        VenIds.Add(p.Vendor.VendorID);

                    if (lstConsolidateVendors.FindAll(x => VenIds.Contains(x.VendorID)).ToList().Count <= 0)
                    {
                        //if (lstConsolidateVendors.FindAll(x => x.VendorID == lstPO[0].Vendor.VendorID).ToList().Count <= 0) {
                        if (hdnCurrentRole.Value == "Vendor" || hdnCurrentRole.Value == "Carrier")
                        {
                            errMsg = WebCommon.getGlobalResourceValue("BK_MS_12_EXT"); //"The Purchase Order Number you have supplied is for a different vendor. Please enter correct Purchase Order Number.";
                            ShowErrorMessage(errMsg, "BK_MS_12_EXT");
                            return;
                        }
                        else
                        {
                            errMsg = WebCommon.getGlobalResourceValue("BK_MS_12_INT"); //"The Purchase Order Number you have entered is not for the entered vendor. Do you want to continue?";
                            AddWarningMessages(errMsg, "BK_MS_12_INT");
                            isWarning = true;
                        }
                    }



                    // Sprint 1 - Point 14 - Begin - Changing from [Original_due_date] to [Expected Date]
                    /*
                       DateTime PODueDate = Utilities.Common.TextToDateFormat(GetFormatedDate(lstPO[0].Original_due_date.Value.Day.ToString() + "/" + lstPO[0].Original_due_date.Value.Month.ToString() + "/" + lstPO[0].Original_due_date.Value.Year.ToString()));
                       //DateTime CurrentDate = Utilities.Common.TextToDateFormat(DateTime.Now.Day.ToString() + "/" + DateTime.Now.Month.ToString() + "/" + DateTime.Now.Year.ToString());
                    */
                    DateTime PODueDate = DateTime.Now;
                    if (lstPO[0].Expected_date != null) // This condition is given, if [Expected Date] will come null then [Original due date] will be set like earlier.
                    {
                        PODueDate = Utilities.Common.TextToDateFormat(GetFormatedDate(lstPO[0].Expected_date.Value.Day.ToString() + "/" + lstPO[0].Expected_date.Value.Month.ToString() + "/" + lstPO[0].Expected_date.Value.Year.ToString()));
                    }
                    else
                    {
                        PODueDate = Utilities.Common.TextToDateFormat(GetFormatedDate(lstPO[0].Original_due_date.Value.Day.ToString() + "/" + lstPO[0].Original_due_date.Value.Month.ToString() + "/" + lstPO[0].Original_due_date.Value.Year.ToString()));
                    }
                    // Sprint 1 - Point 14 - End - Changing from [Original_due_date] to [Expected Date]

                    DateTime ScheduledDate = LogicalSchedulDateTime;  //SchDate; // Utilities.Common.TextToDateFormat(GetFormatedDate(lstPO[0].Original_due_date.ToString()));

                    //OTIF Future Date PO check//
                    bool isOTIFPO = false;
                    CntOTIF_FutureDateSetupBE oCntOTIF_FutureDateSetupBE = new CntOTIF_FutureDateSetupBE();
                    CntOTIF_FutureDateSetupBAL oCntOTIF_FutureDateSetupBAL = new CntOTIF_FutureDateSetupBAL();
                    oCntOTIF_FutureDateSetupBE.Action = "ShowAll";
                    oCntOTIF_FutureDateSetupBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
                    oCntOTIF_FutureDateSetupBE.User.UserID = Convert.ToInt32(Session["UserID"]);
                    oCntOTIF_FutureDateSetupBE.CountryID = Convert.ToInt32(PreSiteCountryID);
                    List<CntOTIF_FutureDateSetupBE> lstCountryFutureDatePO = oCntOTIF_FutureDateSetupBAL.GetCntOTIF_FutureDateSetupDetailsBAL(oCntOTIF_FutureDateSetupBE);
                    if (lstCountryFutureDatePO != null && lstCountryFutureDatePO.Count > 0)
                    {
                        // This condition is given, if [Expected Date] will come null then [Original due date] will be set like earlier.
                        if (lstPO[0].Expected_date != null)
                        {
                            // Sprint 1 - Point 14 - Begin - Changing from [Original_due_date] to [Expected Date]

                            if (lstPO[0].Expected_date.Value.Day == Convert.ToInt32(lstCountryFutureDatePO[0].FutureDatePODay) && lstPO[0].Expected_date.Value.Month == Convert.ToInt32(lstCountryFutureDatePO[0].FutureDatePOMonth))
                            // Sprint 1 - Point 14 - End - Changing from [Original_due_date] to [Expected Date]
                            {
                                //This is OTIF PO Date
                                isOTIFPO = true;
                            }
                        }
                        else
                        {
                            if (lstPO[0].Original_due_date.Value.Day == Convert.ToInt32(lstCountryFutureDatePO[0].FutureDatePODay) && lstPO[0].Original_due_date.Value.Month == Convert.ToInt32(lstCountryFutureDatePO[0].FutureDatePOMonth))
                            {
                                //This is OTIF PO Date
                                isOTIFPO = true;
                            }
                        }
                    }
                    //------------------------//


                    bool isError = false;

                    if (isOTIFPO == false)
                    {
                        if (lstPO[0].Site.ToleranceDueDay == 0)
                        {
                            if (ScheduledDate < PODueDate)
                            {  //if (ScheduledDate < PODueDate) {
                                isError = true;
                            }
                        }
                        else if (lstPO[0].Site.ToleranceDueDay > 0)
                        {
                            //if (PODueDate > ScheduledDate.AddDays(Convert.ToDouble(lstPO[0].Site.ToleranceDueDay)))
                            //{
                            //    isError = true;
                            //}

                            //----weekend --------------------//
                            int DaysDiff = 0;
                            if (PODueDate >= ScheduledDate)
                            {
                                DaysDiff = Enumerable.Range(0, Convert.ToInt32(PODueDate.Subtract(ScheduledDate).TotalDays)).Select(i => new[] { DayOfWeek.Saturday, DayOfWeek.Sunday }.Contains(ScheduledDate.AddDays(i).DayOfWeek) ? 1 : 0).Sum();
                            }

                            if (PODueDate > ScheduledDate.AddDays(Convert.ToDouble(lstPO[0].Site.ToleranceDueDay + DaysDiff)))
                            {
                                isError = true;
                            }
                        }

                        if (isError)
                        {           //error     
                            if (hdnCurrentRole.Value == "Vendor" || hdnCurrentRole.Value == "Carrier")
                            {
                                errMsg = WebCommon.getGlobalResourceValue("BK_MS_13_EXT").Replace("##dd/mm/yyyy##", PODueDate.ToString("dd/MM/yyyy"));  // "The  Purchase Order quoted is not due in on the date selected. Please rebook this Purchase Order for the correct date. <br> Due Date:" + PODueDate.ToString("dd/MM/yyyy"); 
                                ShowErrorMessage(errMsg, "BK_MS_13_EXT");
                                return;
                            }
                            else
                            {
                                errMsg = WebCommon.getGlobalResourceValue("BK_MS_13_INT").Replace("##dd/mm/yyyy##", PODueDate.ToString("dd/MM/yyyy")); // The Purchase Order quoted is not due in for this date.The correct due date is dd/mm/yyyy. Do you want to continue?
                                AddWarningMessages(errMsg, "BK_MS_13_INT");
                                isWarning = true;
                            }
                        }
                    }
                }

                if (isWarning == false)
                {
                    BindPOGrid(PurchaseNumber);

                }
                else
                {
                    if (!Multiple)
                    {
                        ShowWarningMessages();
                    }
                    else
                    {
                        BindPOGrid(PurchaseNumber);
                    }
                }
            }
            else if (IsBookingValidationExcluded == true)
            {  //If there is Validation Exclusion for the vendor/carrier or booking type
                BindPOGrid(PurchaseNumber);

            }
        }
        else
        {
            errMsg = WebCommon.getGlobalResourceValue("BK_MS_CorrectPO"); //"Please enter correct Purchase Order Number.";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "popup", "<script>alert('" + errMsg + "');</script>", false);
        }
    }

    private void GetOutstandingPO()
    {

        ViewState["dtOutstandingPO"] = null;

        Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE = new Up_PurchaseOrderDetailBE();
        UP_PurchaseOrderDetailBAL oUP_PurchaseOrderDetailBAL = new UP_PurchaseOrderDetailBAL();

        oUp_PurchaseOrderDetailBE.Action = "GetRemainingBookingPODetails";
        oUp_PurchaseOrderDetailBE.Vendor = new UP_VendorBE();
        oUp_PurchaseOrderDetailBE.Vendor.VendorID = Convert.ToInt32(ucSeacrhVendorID);
        oUp_PurchaseOrderDetailBE.Site = new MAS_SiteBE();
        oUp_PurchaseOrderDetailBE.Site.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
        oUp_PurchaseOrderDetailBE.SelectedProductCodes = sAddedPurchaseOrders.Substring(0, sAddedPurchaseOrders.Length - 1);

        List<Up_PurchaseOrderDetailBE> lstPO = oUP_PurchaseOrderDetailBAL.GetRemainingBookingPODetails(oUp_PurchaseOrderDetailBE);

        DateTime ScheduledDate = LogicalSchedulDateTime;

        if (lstPO != null && lstPO.Count > 0)
        {
            // 1. Filter out the seleted purchase orders AS PER THE SITE SETTINGS

            lstPO = lstPO.FindAll(delegate (Up_PurchaseOrderDetailBE PO)
            {
                return PO.Original_due_date <= ScheduledDate;
            });



            Filter(lstPO);

            DataTable dtOutstandingPO = null;

            if (ViewState["dtOutstandingPO"] != null)
            {
                dtOutstandingPO = (DataTable)ViewState["dtOutstandingPO"];
            }

            if (dtOutstandingPO != null && dtOutstandingPO.Rows.Count > 0 && Convert.ToBoolean(ViewState["IsShowPOPopup"]) == false)
            {
                //gvOutstandingPO.DataSource = dtOutstandingPO;
                //gvOutstandingPO.DataBind();
                //mdlOutstandingPO.Show();
                //---Stage 6 Point 3--//
                CalculateProposedBookingTimeSlot();
                
                mvBookingEdit.ActiveViewIndex = !isDoorCheckPopup ? 3 : 2;

                //------------//
            }
            else
            {
                CalculateProposedBookingTimeSlot();
                mvBookingEdit.ActiveViewIndex = 3;
            }
        }
        else
        {

            CalculateProposedBookingTimeSlot();
            if (isDoorCheckPopup == false)
            {
                mvBookingEdit.ActiveViewIndex = 3;
            } 
        }
    }

    private void Filter(List<Up_PurchaseOrderDetailBE> lstPOLocal)
    {
        if (lstPOLocal.Count > 0)
        {
            DateTime? Original_due_date;
            int OutStandingLines;
            string Purchase_order;

            Original_due_date = lstPOLocal[0].Original_due_date;
            Purchase_order = lstPOLocal[0].Purchase_order;

            List<Up_PurchaseOrderDetailBE> lstPOTemp = lstPOLocal.FindAll(delegate (Up_PurchaseOrderDetailBE PO)
            {
                return PO.Original_due_date == Original_due_date && PO.Purchase_order == Purchase_order;
            }
            );

            OutStandingLines = lstPOTemp.Count;

            lstPOLocal = lstPOLocal.FindAll(delegate (Up_PurchaseOrderDetailBE PO)
            {
                return PO.Purchase_order != Purchase_order;
            }
            );

            //Add to Temp Table
            DataTable dtOutstandingPO = null;

            if (ViewState["dtOutstandingPO"] != null)
            {
                dtOutstandingPO = (DataTable)ViewState["dtOutstandingPO"];
            }
            else
            {

                dtOutstandingPO = new DataTable();


                DataColumn myDataColumn = new DataColumn();
                myDataColumn.ColumnName = "PurchaseNumber";
                dtOutstandingPO.Columns.Add(myDataColumn);

                myDataColumn = new DataColumn();
                myDataColumn.ColumnName = "Original_due_date";
                dtOutstandingPO.Columns.Add(myDataColumn);


                myDataColumn = new DataColumn();
                myDataColumn.ColumnName = "OutstandingLines";
                dtOutstandingPO.Columns.Add(myDataColumn);

            }

            DataRow dataRow = dtOutstandingPO.NewRow();

            dataRow["PurchaseNumber"] = Purchase_order;
            dataRow["Original_due_date"] = Original_due_date.Value.ToString("dd/MM/yyyy");
            dataRow["OutstandingLines"] = OutStandingLines;

            dtOutstandingPO.Rows.Add(dataRow);

            if (dtOutstandingPO != null && dtOutstandingPO.Rows.Count > 0)
            {
                ViewState["dtOutstandingPO"] = dtOutstandingPO;
            }
            else
            {
                ViewState["dtOutstandingPO"] = null;
            }

            Filter(lstPOLocal);
        }
    }

    private string GetFormatedDate(string DateToFormat)
    {
        if (DateToFormat != string.Empty)
        {
            string[] arrDate = DateToFormat.Split('/');
            string DD, MM, YYYY;
            if (arrDate[0].Length < 2)
                DD = "0" + arrDate[0].ToString();
            else
                DD = arrDate[0];

            if (arrDate[1].Length < 2)
                MM = "0" + arrDate[1].ToString();
            else
                MM = arrDate[1];

            YYYY = arrDate[2].ToString().Substring(0, 4);

            return DD + "/" + MM + "/" + YYYY.Trim();
        }
        else
        {
            return DateToFormat;
        }
    }

    private string GetFormatedTime(string TimeToFormat)
    {
        if (TimeToFormat != string.Empty)
        {
            string[] arrDate = TimeToFormat.Split(':');
            string HH, MM;
            if (arrDate[0].Length < 2)
                HH = "0" + arrDate[0].ToString();
            else
                HH = arrDate[0];

            if (arrDate[1].Length < 2)
                MM = "0" + arrDate[1].ToString();
            else
                MM = arrDate[1];


            return HH + ":" + MM;
        }
        else
        {
            return TimeToFormat;
        }
    }

    protected void BindAllPOGrid()
    {
        DataTable myDataTable = null;
        myDataTable = new DataTable();

        DataColumn auto = new DataColumn("AutoID", typeof(System.Int32));
        // specify it as auto increment field
        auto.AutoIncrement = true;
        auto.AutoIncrementSeed = 1;
        auto.ReadOnly = true;
        auto.Unique = true;
        myDataTable.Columns.Add(auto);

        DataColumn myDataColumn = new DataColumn();
        myDataColumn.ColumnName = "PurchaseNumber";
        myDataTable.Columns.Add(myDataColumn);

        myDataColumn = new DataColumn();
        myDataColumn.ColumnName = "Original_due_date";
        myDataTable.Columns.Add(myDataColumn);

        myDataColumn = new DataColumn();
        myDataColumn.ColumnName = "VendorName";
        myDataTable.Columns.Add(myDataColumn);

        myDataColumn = new DataColumn();
        myDataColumn.ColumnName = "OutstandingLines";
        myDataTable.Columns.Add(myDataColumn);

        myDataColumn = new DataColumn();
        myDataColumn.ColumnName = "Qty_On_Hand";
        myDataTable.Columns.Add(myDataColumn);

        myDataColumn = new DataColumn();
        myDataColumn.ColumnName = "qty_on_backorder";
        myDataTable.Columns.Add(myDataColumn);

        myDataColumn = new DataColumn();
        myDataColumn.ColumnName = "SiteName";
        myDataTable.Columns.Add(myDataColumn);

        myDataColumn = new DataColumn();
        myDataColumn.ColumnName = "PurchaseOrderID";
        myDataTable.Columns.Add(myDataColumn);

        myDataColumn = new DataColumn();
        myDataColumn.ColumnName = "ExpectedDate";
        myDataTable.Columns.Add(myDataColumn);

        myDataColumn = new DataColumn();
        myDataColumn.ColumnName = "IsProvisionResonChangesUpdate";
        myDataTable.Columns.Add(myDataColumn);


        Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE = new Up_PurchaseOrderDetailBE();
        UP_PurchaseOrderDetailBAL oUP_PurchaseOrderDetailBAL = new UP_PurchaseOrderDetailBAL();

        oUp_PurchaseOrderDetailBE.Action = "GetAllBookingPODetails";
        oUp_PurchaseOrderDetailBE.BookingID = Convert.ToInt32(GetQueryStringValue("ID").ToString());

        List<Up_PurchaseOrderDetailBE> lstPO = oUP_PurchaseOrderDetailBAL.GetAllBookedProductOrderDetailsBAL(oUp_PurchaseOrderDetailBE);

        if (lstPO.Count > 0)
        {
            for (int iCount = 0; iCount < lstPO.Count; iCount++)
            {
                DataRow dataRow = myDataTable.NewRow();

                //oUp_PurchaseOrderDetailBE.Action = "GetStockDetails";
                //oUp_PurchaseOrderDetailBE.Purchase_order = lstPO[iCount].Purchase_order;
                //oUp_PurchaseOrderDetailBE.Site = new MAS_SiteBE();
                //oUp_PurchaseOrderDetailBE.Site.SiteID = lstPO[iCount].Site.SiteID;  //Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);

                //List<Up_PurchaseOrderDetailBE> lstPOStock = oUP_PurchaseOrderDetailBAL.GetPriorityBAL(oUp_PurchaseOrderDetailBE);

                //if (lstPOStock != null && lstPOStock.Count > 0)
                //{

                //    List<Up_PurchaseOrderDetailBE> lstPOTemp = lstPOStock.FindAll(delegate(Up_PurchaseOrderDetailBE p) { return Convert.ToDecimal(p.SKU.Qty_On_Hand) == 0 && (Convert.ToInt32(Convert.ToDecimal(p.Original_quantity)) - p.Qty_receipted > 0); });

                //    if (lstPOTemp != null && lstPOTemp.Count > 0)
                //    {
                //        dataRow["Qty_On_Hand"] = lstPOTemp.Count.ToString();
                //    }
                //    else
                //    {
                //        dataRow["Qty_On_Hand"] = "0";
                //    }


                //    lstPOTemp = lstPOStock.FindAll(delegate(Up_PurchaseOrderDetailBE p)
                //    {
                //        return (Convert.ToDecimal(p.SKU.Qty_On_Hand) - Convert.ToDecimal(p.SKU.qty_on_backorder) < 0);
                //    }
                // );
                //    if (lstPOTemp != null && lstPOTemp.Count > 0)
                //    {
                //        dataRow["qty_on_backorder"] = lstPOTemp.Count.ToString();
                //    }
                //    else
                //    {
                //        dataRow["qty_on_backorder"] = "0";
                //    }
                //}
                //else
                //{
                //    dataRow["Qty_On_Hand"] = "0";
                //    dataRow["qty_on_backorder"] = "0";
                //}


                dataRow["PurchaseNumber"] = lstPO[iCount].Purchase_order;
                dataRow["Original_due_date"] = lstPO[iCount].Original_due_date.Value.ToString("dd/MM/yyyy");
                dataRow["VendorName"] = lstPO[iCount].Vendor.VendorName;

                dataRow["Qty_On_Hand"] = lstPO[iCount].Stockouts;
                dataRow["IsProvisionResonChangesUpdate"] = lstPO[iCount].IsProvisionResonChangesUpdate;
                dataRow["qty_on_backorder"] = lstPO[iCount].Backorders;
                if (lstPO[iCount].OutstandingLines > 0)
                {
                    dataRow["OutstandingLines"] = lstPO[iCount].OutstandingLines.ToString(); //lstPOStock.Count.ToString(); //

                }
                else
                {
                    dataRow["OutstandingLines"] = 1;

                }

                dataRow["SiteName"] = lstPO[iCount].Site.SiteName;
                dataRow["PurchaseOrderID"] = lstPO[iCount].PurchaseOrderID.ToString();

                if (lstPO[iCount].Expected_date != null)
                {
                    dataRow["ExpectedDate"] = lstPO[iCount].Expected_date.Value.ToString("dd/MM/yyyy");
                }
                else
                {
                    dataRow["ExpectedDate"] = lstPO[iCount].Original_due_date.Value.ToString("dd/MM/yyyy");
                }

                myDataTable.Rows.Add(dataRow);
            }
        }

        if (myDataTable != null && myDataTable.Rows.Count > 0)
        {
            gvPO.DataSource = myDataTable;
            gvPO.DataBind();

            gvPOHistory.DataSource = myDataTable;
            gvPOHistory.DataBind();
        }

        if (myDataTable != null && myDataTable.Rows.Count > 0)
        {
            ScriptManager.RegisterStartupScript(Page, this.GetType(), "btnshow", "$get('" + btnProceedBooking.ClientID + "').style.display='';", true);
            ViewState["myDataTable"] = myDataTable;
        }
        else
        {
            ScriptManager.RegisterStartupScript(Page, this.GetType(), "btnhide", "$get('" + btnProceedBooking.ClientID + "').style.display='none';", true);
            ViewState["myDataTable"] = null;
        }

        int TotalLines = 0;
        foreach (DataRow dr in myDataTable.Rows)
        {
            if (dr["OutstandingLines"].ToString() != string.Empty)
                TotalLines += Convert.ToInt32(dr["OutstandingLines"]);
        }

        ltExpectedLines_1.Text = WebCommon.getGlobalResourceValue("ExpectedLines").Replace("XX", TotalLines.ToString()); // lblExpectedLines.Text.Replace(Lines, TotalLines.ToString());

        Lines = TotalLines.ToString();
        //txtExpectedLines.Text = TotalLines.ToString();
    }

    // Sprint 1 - Point 7 - Passed PurchaseNumber as a parameter.
    protected void BindPOGrid(string PurchaseNumber)
    {

        if (PurchaseNumber != string.Empty)
        {

            DataTable myDataTable = null;

            if (ViewState["myDataTable"] != null)
            {
                myDataTable = (DataTable)ViewState["myDataTable"];

                //Check Item alrady added
                DataRow[] drr = myDataTable.Select("PurchaseNumber='" + PurchaseNumber.Trim() + "'");

                if (drr.Length > 0)
                {
                    return;
                }
            }
            else
            {

                myDataTable = new DataTable();

                DataColumn auto = new DataColumn("AutoID", typeof(System.Int32));
                // specify it as auto increment field
                auto.AutoIncrement = true;
                auto.AutoIncrementSeed = 1;
                auto.ReadOnly = true;
                auto.Unique = true;
                myDataTable.Columns.Add(auto);

                DataColumn myDataColumn = new DataColumn();
                myDataColumn.ColumnName = "PurchaseNumber";
                myDataTable.Columns.Add(myDataColumn);

                myDataColumn = new DataColumn();
                myDataColumn.ColumnName = "Original_due_date";
                myDataTable.Columns.Add(myDataColumn);

                myDataColumn = new DataColumn();
                myDataColumn.ColumnName = "VendorName";
                myDataTable.Columns.Add(myDataColumn);

                myDataColumn = new DataColumn();
                myDataColumn.ColumnName = "OutstandingLines";
                myDataTable.Columns.Add(myDataColumn);

                myDataColumn = new DataColumn();
                myDataColumn.ColumnName = "Qty_On_Hand";
                myDataTable.Columns.Add(myDataColumn);

                myDataColumn = new DataColumn();
                myDataColumn.ColumnName = "qty_on_backorder";
                myDataTable.Columns.Add(myDataColumn);

                myDataColumn = new DataColumn();
                myDataColumn.ColumnName = "SiteName";
                myDataTable.Columns.Add(myDataColumn);

                myDataColumn = new DataColumn();
                myDataColumn.ColumnName = "PurchaseOrderID";
                myDataTable.Columns.Add(myDataColumn);

                // Sprint 1 - Point 14 - Begin - Adding [Expected Date]
                myDataColumn = new DataColumn();
                myDataColumn.ColumnName = "ExpectedDate";
                myDataTable.Columns.Add(myDataColumn);
                // Sprint 1 - Point 14 - End - Adding [Expected Date]

                myDataColumn = new DataColumn();
                myDataColumn.ColumnName = "IsProvisionResonChangesUpdate";
                myDataTable.Columns.Add(myDataColumn);
            }


            Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE = new Up_PurchaseOrderDetailBE();
            UP_PurchaseOrderDetailBAL oUP_PurchaseOrderDetailBAL = new UP_PurchaseOrderDetailBAL();
            oUp_PurchaseOrderDetailBE.Vendor = new UP_VendorBE();

            oUp_PurchaseOrderDetailBE.Action = "GetNewBookingPODetails";
            oUp_PurchaseOrderDetailBE.Purchase_order = PurchaseNumber;
            oUp_PurchaseOrderDetailBE.CountryID = Convert.ToInt32(PreSiteCountryID);
            oUp_PurchaseOrderDetailBE.Vendor.VendorID = Convert.ToInt32(ucSeacrhVendorID);

            List<Up_PurchaseOrderDetailBE> lstPO = oUP_PurchaseOrderDetailBAL.GetProductOrderDetailsBAL(oUp_PurchaseOrderDetailBE);
            DataRow dataRow = myDataTable.NewRow();
            if (lstPO.Count > 0)
            {

                //oUp_PurchaseOrderDetailBE.Action = "GetStockDetails";
                //oUp_PurchaseOrderDetailBE.Purchase_order = PurchaseNumber;
                //oUp_PurchaseOrderDetailBE.Site = new MAS_SiteBE();
                //oUp_PurchaseOrderDetailBE.Site.SiteID = lstPO[0].Site.SiteID;  // Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);

                //List<Up_PurchaseOrderDetailBE> lstPOStock = oUP_PurchaseOrderDetailBAL.GetPriorityBAL(oUp_PurchaseOrderDetailBE);

                //if (lstPOStock != null && lstPOStock.Count > 0)
                //{

                //    List<Up_PurchaseOrderDetailBE> lstPOTemp = lstPOStock.FindAll(delegate(Up_PurchaseOrderDetailBE p) { return Convert.ToDecimal(p.SKU.Qty_On_Hand) == 0 && (Convert.ToInt32(Convert.ToDecimal(p.Original_quantity)) - p.Qty_receipted > 0); });

                //    if (lstPOTemp != null && lstPOTemp.Count > 0)
                //    {
                //        dataRow["Qty_On_Hand"] = lstPOTemp.Count.ToString();
                //    }
                //    else
                //    {
                //        dataRow["Qty_On_Hand"] = "0";
                //    }


                //    lstPOTemp = lstPOStock.FindAll(delegate(Up_PurchaseOrderDetailBE p)
                //    {
                //        return (Convert.ToDecimal(p.SKU.Qty_On_Hand) - Convert.ToDecimal(p.SKU.qty_on_backorder) < 0);
                //    }
                //  );
                //    if (lstPOTemp != null && lstPOTemp.Count > 0)
                //    {
                //        dataRow["qty_on_backorder"] = lstPOTemp.Count.ToString();
                //    }
                //    else
                //    {
                //        dataRow["qty_on_backorder"] = "0";
                //    }
                //}
                //else
                //{
                //    dataRow["Qty_On_Hand"] = "0";
                //    dataRow["qty_on_backorder"] = "0";
                //}

                dataRow["Qty_On_Hand"] = lstPO[0].Stockouts.ToString();
                dataRow["IsProvisionResonChangesUpdate"] = lstPO[0].IsProvisionResonChangesUpdate;
                dataRow["qty_on_backorder"] = lstPO[0].Backorders.ToString();
                dataRow["OutstandingLines"] = lstPO[0].OutstandingLines.ToString();

                dataRow["PurchaseNumber"] = PurchaseNumber;
                dataRow["Original_due_date"] = lstPO[0].Original_due_date.Value.ToString("dd/MM/yyyy");
                // Sprint 1 - Point 14 - Begin - Adding [Expected Date]
                // This condition is given, if [Expected Date] will come null then [Original due date] will be set like earlier.
                if (lstPO[0].Expected_date != null)
                {
                    dataRow["ExpectedDate"] = lstPO[0].Expected_date.Value.ToString("dd/MM/yyyy");
                }
                else
                {
                    dataRow["ExpectedDate"] = lstPO[0].Original_due_date.Value.ToString("dd/MM/yyyy");
                }
                // Sprint 1 - Point 14 - End - Adding [Expected Date]

                dataRow["VendorName"] = lstPO[0].Vendor.VendorName;
                //dataRow["OutstandingLines"] = lstPO.Count.ToString(); //lstPO[0].OutstandingLines.ToString(); //

                dataRow["SiteName"] = lstPO[0].Site.SiteName;
                dataRow["PurchaseOrderID"] = lstPO[0].PurchaseOrderID.ToString();

                myDataTable.Rows.Add(dataRow);
            }
            else
            {
                if (IsBookingValidationExcluded == true)
                {
                    dataRow["PurchaseNumber"] = PurchaseNumber;
                    dataRow["Original_due_date"] = ActualSchedulDateTime.ToString("dd/MM/yyyy");
                    // Sprint 1 - Point 14 - Begin - Adding [Expected Date]
                    dataRow["ExpectedDate"] = ActualSchedulDateTime.ToString("dd/MM/yyyy");
                    // Sprint 1 - Point 14 - End - Adding [Expected Date]

                    dataRow["VendorName"] = ltvendorName.Text;
                    dataRow["OutstandingLines"] = "1";
                    dataRow["Qty_On_Hand"] = "0";
                    dataRow["qty_on_backorder"] = "0";
                    dataRow["SiteName"] = ddlSite.innerControlddlSite.SelectedItem.Text;
                    dataRow["PurchaseOrderID"] = 0;
                    myDataTable.Rows.Add(dataRow);
                }

                #region Adding PO for OD user.

                /*
                 * [ISSUE No - 1] by client in [VIP Outstanding Issue Overview 22-04-13 Testing.xls] file.
                 * When Office Depot are making a booking, we should be allowed to add POs for incorrect vendors. The system should warn of the potential problem but
                 * Previously, we had an issue where the PO was being added whether the user hit CONTINUE or BACK. This was fixed but now we are unable to add to
                 *
                 * As per upper side problem, I have added this code as per Vikram suggestion on Date 24/04/2013 6:20 PM .
                 * This code will allow to add the non matched PO for vendor if logged-in user is OD user. 
                 * 
                 * Also this code will add PO in case on the [Continue] button clicked by only OD user.
                 */

                string Role = Session["Role"].ToString().Trim().ToLower();
                if (Role != "vendor" && Role != "carrier" && Convert.ToString(ViewState["ByContinue"]).Trim().ToLower() == "bycontinue")
                {
                    dataRow["PurchaseNumber"] = PurchaseNumber;
                    dataRow["Original_due_date"] = ActualSchedulDateTime.ToString("dd/MM/yyyy");
                    // Sprint 1 - Point 14 - Begin - Adding [Expected Date]
                    dataRow["ExpectedDate"] = ActualSchedulDateTime.ToString("dd/MM/yyyy");
                    // Sprint 1 - Point 14 - End - Adding [Expected Date]

                    dataRow["VendorName"] = ltvendorName.Text;
                    dataRow["OutstandingLines"] = "1";
                    dataRow["Qty_On_Hand"] = "0";
                    dataRow["qty_on_backorder"] = "0";
                    dataRow["SiteName"] = ddlSite.innerControlddlSite.SelectedItem.Text;
                    dataRow["PurchaseOrderID"] = 0;
                    myDataTable.Rows.Add(dataRow);
                    ViewState["ByContinue"] = null;
                }

                #endregion
            }


            if (myDataTable != null && myDataTable.Rows.Count > 0)
            {
                gvPO.DataSource = myDataTable;
                gvPO.DataBind();
            }

            if (myDataTable != null && myDataTable.Rows.Count > 0)
            {
                ScriptManager.RegisterStartupScript(Page, this.GetType(), "btnshow", "$get('" + btnProceedBooking.ClientID + "').style.display='';", true);
                ViewState["myDataTable"] = myDataTable;
            }
            else
            {
                ScriptManager.RegisterStartupScript(Page, this.GetType(), "btnhide", "$get('" + btnProceedBooking.ClientID + "').style.display='none';", true);
                ViewState["myDataTable"] = null;
            }

            int TotalLines = 0;
            foreach (DataRow dr in myDataTable.Rows)
            {
                if (dr["OutstandingLines"].ToString() != string.Empty)
                    TotalLines += Convert.ToInt32(dr["OutstandingLines"]);
            }



            ltExpectedLines_1.Text = WebCommon.getGlobalResourceValue("ExpectedLines").Replace("XX", TotalLines.ToString()); //lblExpectedLines.Text.Replace(Lines, TotalLines.ToString());
            Lines = TotalLines.ToString();


        }

    }

    public string sAddedPurchaseOrders
    {
        get
        {
            return ViewState["vw_sAddedPurchaseOrders"] != null ? ViewState["vw_sAddedPurchaseOrders"].ToString() : string.Empty;
        }
        set
        {
            ViewState["vw_sAddedPurchaseOrders"] = value;
        }
    }

    protected void gvPO_RowDataBound(object sender, GridViewRowEventArgs e)
    {

        string DelMsg = WebCommon.getGlobalResourceValue("RemovePO");

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            // loop all data rows
            if (hdnBookingTypeID.Value == "5" && hdnProvisionalReason.Value == "EDITED" && hdnLine.Value != "0" && Session["Role"].ToString().ToLower() != "vendor")
            {
                if (gvPO.DataKeys[e.Row.RowIndex].Values[1].ToString() == "True")
                {
                    e.Row.Cells[0].ForeColor = Color.Red;
                    e.Row.Cells[1].ForeColor = Color.Red;
                    e.Row.Cells[2].ForeColor = Color.Red;
                    e.Row.Cells[3].ForeColor = Color.Red;
                    e.Row.Cells[4].ForeColor = Color.Red;
                    e.Row.Cells[5].ForeColor = Color.Red;
                    e.Row.Cells[6].ForeColor = Color.Red;
                    e.Row.Cells[7].ForeColor = Color.Red;
                }
            }

            sAddedPurchaseOrders = sAddedPurchaseOrders + e.Row.Cells[0].Text + ",";
            foreach (DataControlFieldCell cell in e.Row.Cells)
            {
                // check all cells in one row
                foreach (Control control in cell.Controls)
                {
                    LinkButton button = control as LinkButton;
                    if (button != null && button.CommandName == "Delete")
                        // Add delete confirmation
                        button.OnClientClick = "return (confirm('" + DelMsg + "')) ;";
                }
            }
        }
    }

    protected void gvPO_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {

        DataTable myDataTable = null;

        if (ViewState["myDataTable"] != null)
        {
            myDataTable = (DataTable)ViewState["myDataTable"];
        }

        if (myDataTable != null)
        {
            myDataTable.Rows.RemoveAt(e.RowIndex); // (Convert.ToInt32(e.Keys[0]) - 1);
            myDataTable.AcceptChanges();
            gvPO.DataSource = myDataTable;
            gvPO.DataBind();
        }

        if (myDataTable != null && myDataTable.Rows.Count > 0)
        {
            ScriptManager.RegisterStartupScript(Page, this.GetType(), "btnshow", "$get('" + btnProceedBooking.ClientID + "').style.display='';", true);
            ViewState["myDataTable"] = myDataTable;
        }
        else
        {
            ScriptManager.RegisterStartupScript(Page, this.GetType(), "btnhide", "$get('" + btnProceedBooking.ClientID + "').style.display='none';", true);
            ViewState["myDataTable"] = null;
        }


        int TotalLines = 0;
        foreach (DataRow dataRow in myDataTable.Rows)
        {
            TotalLines += Convert.ToInt32(dataRow["OutstandingLines"]);
        }
        ltExpectedLines_1.Text = WebCommon.getGlobalResourceValue("ExpectedLines").Replace("XX", TotalLines.ToString()); // lblExpectedLines.Text.Replace(Lines, TotalLines.ToString());

        Lines = TotalLines.ToString();
    }

    protected void gvPO_Sorting(object sender, GridViewSortEventArgs e)
    {
        if (ViewState["myDataTable"] != null)
        {
            DataTable myDataTable = (DataTable)ViewState["myDataTable"];
            ViewState["sortDirection"] = ViewState["sortDirection"] == null || Convert.ToString(ViewState["sortDirection"]) == "DESC" ? "ASC" : "DESC";
            DataView view = myDataTable.DefaultView;
            view.Sort = e.SortExpression + " " + Convert.ToString(ViewState["sortDirection"]);
            gvPO.DataSource = view;
            gvPO.DataBind();
        }
    }

    private void DeliverTypenontime()
    {
        DataTable dt1;

        MASSIT_NonTimeDeliveryBE oMASCNT_NonTimeDeliveryBE = new MASSIT_NonTimeDeliveryBE();
        APPSIT_NonTimeDeliveryBAL oMASCNT_NonTimeDeliveryBAL = new APPSIT_NonTimeDeliveryBAL();

        oMASCNT_NonTimeDeliveryBE.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
        oMASCNT_NonTimeDeliveryBE.Action = "GetDeliveries";
        dt1 = oMASCNT_NonTimeDeliveryBAL.GetNonTimeDeliveryDetailsBAL(oMASCNT_NonTimeDeliveryBE, "");

        strDeliveryNonTime = string.Empty;

        for (int iCount = 0; iCount < dt1.Rows.Count; iCount++)
        {
            strDeliveryNonTime += dt1.Rows[iCount]["SiteDeliveryID"].ToString() + ",";
        }
    }


    protected void btnRejectChanges_Click(object sender, EventArgs e)
    {
        mdlRejectProvisionalChanges.Show();
    }

    protected void btnContinueChanges_2_Click(object sender, EventArgs e)
    {
        RejectChangesBooking();
    }
    protected void btnOK_Click(object sender, EventArgs e)
    {
        mdlNoSlotFound.Hide();
    }
    private void RejectChangesBooking()
    {
        try
        {
            APPBOK_BookingBE oAPPBOK_BookingBE = new APPBOK_BookingBE();
            APPBOK_BookingBAL oAPPBOK_BookingBAL = new APPBOK_BookingBAL();
            oAPPBOK_BookingBE.FixedSlot = new BusinessEntities.ModuleBE.Appointment.SiteSettings.MASSIT_FixedSlotBE();
            oAPPBOK_BookingBE.Action = "RejectChangesBooking";
            oAPPBOK_BookingBE.BookingID = Convert.ToInt32(GetQueryStringValue("ID").ToString());
            oAPPBOK_BookingBAL.RejectChangesBookingDetailBAL(oAPPBOK_BookingBE);

            //--------------------start send reject changes letter phase 14 r2 point 20----------------------//
            sendRejectChangesLetter();
            //--------------------end send reject changes letter phase 14 r2 point 20----------------------//

            if (GetQueryStringValue("PNTB") != null && GetQueryStringValue("PNTB") == "True")
            {
                EncryptQueryString("~/ModuleUI/Appointment/ProvisionalBookings.aspx");
            }
            else if (GetQueryStringValue("BP") != null && GetQueryStringValue("BP") == "Pro")
            {
                EncryptQueryString("~/ModuleUI/Appointment/ProvisionalBookings.aspx");
            }
            else
            {
                EncryptQueryString("APPBok_BookingOverview.aspx");
            }

        }
        catch (Exception ex)
        {
            LogUtility.SaveErrorLogEntry(ex);
        }
    }

    //---Start Stage 14 Point 20-----//
    private void sendRejectChangesLetter()
    {
        APPBOK_BookingBE oAPPBOK_BookingBE = new APPBOK_BookingBE();
        APPBOK_BookingBAL oAPPBOK_BookingBAL = new APPBOK_BookingBAL();
        List<APPBOK_BookingBE> lstBooking = new List<APPBOK_BookingBE>();



        APPBOK_CommunicationBAL oAPPBOK_CommunicationBAL = new APPBOK_CommunicationBAL();
        List<MAS_LanguageBE> oLanguages = oAPPBOK_CommunicationBAL.GetLanguages();

        int BookingID = Convert.ToInt32(GetQueryStringValue("ID").ToString());

        oAPPBOK_BookingBE.Action = "GetBookingDeatilsByID";
        oAPPBOK_BookingBE.BookingID = BookingID;
        lstBooking = oAPPBOK_BookingBAL.GetBookingDetailsByIDBAL(oAPPBOK_BookingBE);

        if (lstBooking.Count > 0)
        {

            MAS_SiteBE singleSiteSetting = GetSingleSiteSetting(Convert.ToInt32(lstBooking[0].SiteId));
            try
            {
                MASSIT_VendorBE oMASSIT_VendorBE = new MASSIT_VendorBE();
                APPSIT_VendorBAL oAPPSIT_VendorBAL = new APPSIT_VendorBAL();

                string VendorEmails = string.Empty;
                List<MASSIT_VendorBE> lstVendorDetails = null;
                string[] sentTo;
                string[] language;
                string[] VendorData = new string[2];
                bool isMailSend = false;



                if (lstBooking[0].SupplierType == "C")
                {
                    oMASSIT_VendorBE.Action = "GetCarriersVendor";
                    oMASSIT_VendorBE.BookingID = BookingID;

                    lstVendorDetails = oAPPSIT_VendorBAL.GetCarriersVendorIDBAL(oMASSIT_VendorBE);
                    if (lstVendorDetails != null && lstVendorDetails.Count > 0)
                    {
                        for (int i = 0; i < lstVendorDetails.Count; i++)
                        {
                            isMailSend = false;
                            VendorData = CommonPage.GetVendorEmailsWithLanguage(lstVendorDetails[i].VendorID, Convert.ToInt32(lstBooking[0].SiteId));
                            if (VendorData.Length > 0 && !string.IsNullOrEmpty(VendorData[0]))
                            {
                                sentTo = VendorData[0].Split(',');
                                language = VendorData[1].Split(',');
                                for (int j = 0; j < sentTo.Length; j++)
                                {
                                    if (sentTo[j].Trim() != "")
                                    {
                                        SendMailToVendor(sentTo[j], language[j], lstBooking, singleSiteSetting, oLanguages);
                                        //Page.UICulture = Convert.ToString(Session["CultureInfo"]);
                                        isMailSend = true;
                                    }
                                }
                            }

                            if (!isMailSend)
                            {
                                SendMailToVendor(ConfigurationManager.AppSettings["DefaultEmailAddress"], "English", lstBooking, singleSiteSetting, oLanguages);
                                //Page.UICulture = Convert.ToString(Session["CultureInfo"]);
                            }

                        }
                    }


                    //Send mail to Carrier
                    isMailSend = false;
                    //VendorData = CommonPage.GetCarrierDefaultEmailWithLanguage(lstBooking[0].Carrier.CarrierID);
                    VendorData = CommonPage.GetCarrierEmailsWithLanguage(lstBooking[0].Carrier.CarrierID, Convert.ToInt32(lstBooking[0].SiteId));
                    if (VendorData.Length > 0 && !string.IsNullOrEmpty(VendorData[0]))
                    {
                        sentTo = VendorData[0].Split(',');
                        language = VendorData[1].Split(',');
                        for (int j = 0; j < sentTo.Length; j++)
                        {
                            if (sentTo[j].Trim() != "")
                            {
                                SendMailToVendor(sentTo[j], language[j], lstBooking, singleSiteSetting, oLanguages);
                                //Page.UICulture = Convert.ToString(Session["CultureInfo"]);
                                isMailSend = true;
                            }
                        }
                    }
                    if (!isMailSend)
                    {
                        SendMailToVendor(ConfigurationManager.AppSettings["DefaultEmailAddress"], "English", lstBooking, singleSiteSetting, oLanguages);
                        //Page.UICulture = Convert.ToString(Session["CultureInfo"]);
                    }

                    //----------------------//

                }
                else if (lstBooking[0].SupplierType == "V")
                {
                    isMailSend = false;
                    VendorData = CommonPage.GetVendorEmailsWithLanguage(lstBooking[0].VendorID, Convert.ToInt32(lstBooking[0].SiteId));
                    if (VendorData.Length > 0 && !string.IsNullOrEmpty(VendorData[0]))
                    {
                        sentTo = VendorData[0].Split(',');
                        language = VendorData[1].Split(',');
                        for (int j = 0; j < sentTo.Length; j++)
                        {
                            if (sentTo[j].Trim() != "")
                            {
                                SendMailToVendor(sentTo[j], language[j], lstBooking, singleSiteSetting, oLanguages);
                                //Page.UICulture = Convert.ToString(Session["CultureInfo"]);
                                isMailSend = true;
                            }
                        }
                    }
                    if (!isMailSend)
                    {
                        SendMailToVendor(ConfigurationManager.AppSettings["DefaultEmailAddress"], "English", lstBooking, singleSiteSetting, oLanguages);
                        //Page.UICulture = Convert.ToString(Session["CultureInfo"]);
                    }
                }

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
                SendMailToVendor(ConfigurationManager.AppSettings["DefaultEmailAddress"], "English", lstBooking, singleSiteSetting, oLanguages);
            }
        }
    }

    sendCommunication communication = new sendCommunication();
    private void SendMailToVendor(string toAddress, string language, List<APPBOK_BookingBE> lstBooking, MAS_SiteBE singleSiteSetting, List<MAS_LanguageBE> oLanguages)
    {
        if (!string.IsNullOrEmpty(toAddress))
        {
            string htmlBody = string.Empty;
            string templatePath = null;
            templatePath = path + templatePathName;
            string LanguageFile = string.Empty;
            APPBOK_CommunicationBAL oAPPBOK_CommunicationBAL = new APPBOK_CommunicationBAL();
            clsEmail oclsEmail = new clsEmail();
            int iTotalPalllets = 0;
            int iTotalLines = 0;
            int iTotalCartons = 0;
            int iTotalLifts = 0;
            if (lstBooking.Count > 0)
            {

                for (int iCount = 0; iCount < lstBooking.Count; iCount++)
                {
                    iTotalPalllets += Convert.ToInt32(lstBooking[iCount].NumberOfPallet);
                    iTotalLines += Convert.ToInt32(lstBooking[iCount].NumberOfLines);
                    iTotalCartons += Convert.ToInt32(lstBooking[iCount].NumberOfCartons);
                    iTotalLifts += Convert.ToInt32(lstBooking[iCount].NumberOfLift);
                }
            }

            foreach (MAS_LanguageBE objLanguage in oLanguages)
            {
                bool MailSentInLanguage = false;

                if (objLanguage.Language.ToLower() == language.ToLower())
                {
                    MailSentInLanguage = true;
                }

                LanguageFile = "RejectChanges.english.htm";

                using (StreamReader sReader = new StreamReader(templatePath + LanguageFile.ToLower()))
                {
                    #region mailBody
                    htmlBody = sReader.ReadToEnd();

                    htmlBody = htmlBody.Replace("{BookingReference}", WebCommon.getGlobalResourceValueByLangID("BookingReference", objLanguage.LanguageID));
                    htmlBody = htmlBody.Replace("{DearSirMadam}", WebCommon.getGlobalResourceValueByLangID("DearSirMadam", objLanguage.LanguageID));
                    htmlBody = htmlBody.Replace("{RejectChangesText1}", WebCommon.getGlobalResourceValueByLangID("RejectChangesText1", objLanguage.LanguageID));
                    htmlBody = htmlBody.Replace("{RejectChangesText2}", WebCommon.getGlobalResourceValueByLangID("RejectChangesText2", objLanguage.LanguageID));
                    htmlBody = htmlBody.Replace("{ProvisionalText7}", WebCommon.getGlobalResourceValueByLangID("ProvisionalText7", objLanguage.LanguageID));

                    /* Changed For All letters files */
                    htmlBody = htmlBody.Replace("{BookingRefValue}", lstBooking[0].BookingRef);

                    htmlBody = htmlBody.Replace("{Date}", WebCommon.getGlobalResourceValueByLangID("Date", objLanguage.LanguageID));


                    string strWeekDay = lstBooking[0].ScheduleDate.Value.DayOfWeek.ToString().ToUpper();
                    if (strWeekDay == "SUNDAY")
                    {
                        if (lstBooking[0].WeekDay != 1)
                            htmlBody = htmlBody.Replace("{DateValue}", lstBooking[0].ScheduleDate.Value.AddDays(-1).ToString("dd/MM/yyyy"));
                    }
                    else if (strWeekDay == "MONDAY")
                    {
                        if (lstBooking[0].WeekDay != 2)
                            htmlBody = htmlBody.Replace("{DateValue}", lstBooking[0].ScheduleDate.Value.AddDays(-1).ToString("dd/MM/yyyy"));
                    }
                    else if (strWeekDay == "TUESDAY")
                    {
                        if (lstBooking[0].WeekDay != 3)
                            htmlBody = htmlBody.Replace("{DateValue}", lstBooking[0].ScheduleDate.Value.AddDays(-1).ToString("dd/MM/yyyy"));
                    }
                    else if (strWeekDay == "WEDNESDAY")
                    {
                        if (lstBooking[0].WeekDay != 4)
                            htmlBody = htmlBody.Replace("{DateValue}", lstBooking[0].ScheduleDate.Value.AddDays(-1).ToString("dd/MM/yyyy"));
                    }
                    else if (strWeekDay == "THURSDAY")
                    {
                        if (lstBooking[0].WeekDay != 5)
                            htmlBody = htmlBody.Replace("{DateValue}", lstBooking[0].ScheduleDate.Value.AddDays(-1).ToString("dd/MM/yyyy"));
                    }
                    else if (strWeekDay == "FRIDAY")
                    {
                        if (lstBooking[0].WeekDay != 6)
                            htmlBody = htmlBody.Replace("{DateValue}", lstBooking[0].ScheduleDate.Value.AddDays(-1).ToString("dd/MM/yyyy"));
                    }
                    else if (strWeekDay == "SATURDAY")
                    {
                        if (lstBooking[0].WeekDay != 7)
                            htmlBody = htmlBody.Replace("{DateValue}", lstBooking[0].ScheduleDate.Value.AddDays(-1).ToString("dd/MM/yyyy"));
                    }
                    htmlBody = htmlBody.Replace("{DateValue}", lstBooking[0].ScheduleDate.Value.ToString("dd/MM/yyyy"));

                    htmlBody = htmlBody.Replace("{Time}", WebCommon.getGlobalResourceValueByLangID("Time", objLanguage.LanguageID));
                    htmlBody = htmlBody.Replace("{TimeActual}", WebCommon.getGlobalResourceValueByLangID("TimeActual", objLanguage.LanguageID));
                    htmlBody = htmlBody.Replace("{TimeValue}", lstBooking[0].SlotTime.SlotTime);

                    htmlBody = htmlBody.Replace("{Carrier}", WebCommon.getGlobalResourceValueByLangID("Carrier", objLanguage.LanguageID));
                    htmlBody = htmlBody.Replace("{CarrierValue}", lstBooking[0].Carrier.CarrierName);

                    htmlBody = htmlBody.Replace("{VehicleTypeActual}", WebCommon.getGlobalResourceValueByLangID("VehicleTypeActual", objLanguage.LanguageID));
                    htmlBody = htmlBody.Replace("{VehicleType}", WebCommon.getGlobalResourceValueByLangID("VehicleType", objLanguage.LanguageID));
                    htmlBody = htmlBody.Replace("{VehicleTypeValue}", lstBooking[0].VehicleType.VehicleType);

                    htmlBody = htmlBody.Replace("{NumberofPallets}", WebCommon.getGlobalResourceValueByLangID("NumberofPallets", objLanguage.LanguageID));
                    htmlBody = htmlBody.Replace("{PalletsValue}", Convert.ToString(iTotalPalllets));

                    htmlBody = htmlBody.Replace("{NumberofLifts}", WebCommon.getGlobalResourceValueByLangID("NumberofLifts", objLanguage.LanguageID));
                    htmlBody = htmlBody.Replace("{LiftsValue}", Convert.ToString(iTotalLifts));

                    htmlBody = htmlBody.Replace("{NumberofCartons}", WebCommon.getGlobalResourceValueByLangID("NumberofCartons", objLanguage.LanguageID));
                    htmlBody = htmlBody.Replace("{CartonsValue}", Convert.ToString(iTotalCartons));

                    htmlBody = htmlBody.Replace("{NumberofPOLines}", WebCommon.getGlobalResourceValueByLangID("NumberofPOLines", objLanguage.LanguageID));
                    htmlBody = htmlBody.Replace("{LinesValue}", Convert.ToString(iTotalLines));

                    htmlBody = htmlBody.Replace("{BookedPONo}", WebCommon.getGlobalResourceValueByLangID("BookedPONo", objLanguage.LanguageID));
                    htmlBody = htmlBody.Replace("{BookedPOsValue}", lstBooking[0].PurchaseOrders.TrimStart(',').TrimEnd(','));

                    htmlBody = htmlBody.Replace("{PortalLink}", "<a href=" + sPortalLink + ">" + sPortalLink + " </a>");

                    if (singleSiteSetting != null && singleSiteSetting.NonTimeStart != string.Empty)
                    {
                        htmlBody = htmlBody.Replace("{tblVisible}", "");
                        htmlBody = htmlBody.Replace("{SiteNameValue}", singleSiteSetting.SiteName);
                        htmlBody = htmlBody.Replace("{NonTimedStartTimeValue}", singleSiteSetting.NonTimeStart);
                        htmlBody = htmlBody.Replace("{NonTimedEndTimeValue}", singleSiteSetting.NonTimeEnd);
                    }
                    else
                    {
                        htmlBody = htmlBody.Replace("{tblVisible}", "none");
                    }

                    htmlBody = htmlBody.Replace("{Yoursfaithfully}", WebCommon.getGlobalResourceValueByLangID("Yoursfaithfully", objLanguage.LanguageID));

                    //------------------------//
                    #endregion
                }

                string sFromAddress = Convert.ToString(ConfigurationManager.AppSettings["fromAddress"]);

                oAPPBOK_CommunicationBAL.AddItemBAL(lstBooking[0].BookingID, sFromAddress, toAddress, "Reject Changes of your Booking", htmlBody, Convert.ToInt32(Session["UserID"]), CommunicationType.Enum.ProvisionalReject, objLanguage.LanguageID, MailSentInLanguage, MailSentInLanguage);
                if (objLanguage.Language.ToLower() == language.ToLower())
                {
                    oclsEmail.sendMail(toAddress, htmlBody, "Reject Changes of your Booking", sFromAddress, true);
                }
            }
        }
    }
    //---End Stage 14 Point 20-----//

    protected void btnBackChanges_2_Click(object sender, EventArgs e)
    {

        mdlRejectProvisionalChanges.Hide();
    }

    protected void btnBack_1_Click(object sender, EventArgs e)
    {
        txtPurchaseNumber.Text = string.Empty;
        mvBookingEdit.ActiveViewIndex = 1;
    }

    protected void btnProceedBooking_Click(object sender, EventArgs e)
    {
        ProceedBooking();
        hdnCurrentWeekDay.Value = ActualSchedulDateTime.ToString("ddd").ToLower();
    }

    //-----------Start Stage 9 point 2a/2b--------------//
    protected void btnReject_Click(object sender, EventArgs e)
    {
        //string errMsg = WebCommon.getGlobalResourceValue("DeleteBookingConfirmOD");
        //AddWarningMessages(errMsg, "Booking");
        //ShowWarningMessages();
        mdlRejectProvisional.Show();
    }

    protected void btnContinue_2_Click(object sender, EventArgs e)
    {
        DeleteBooking();
    }

    protected void btnBack_2_Click(object sender, EventArgs e)
    {

        mdlRejectProvisional.Hide();
    }

    private void DeleteBooking()
    {
        APPBOK_BookingBE oAPPBOK_BookingBE = new APPBOK_BookingBE();
        APPBOK_BookingBAL oAPPBOK_BookingBAL = new APPBOK_BookingBAL();
        oAPPBOK_BookingBE.FixedSlot = new BusinessEntities.ModuleBE.Appointment.SiteSettings.MASSIT_FixedSlotBE();
        oAPPBOK_BookingBE.Action = "DeleteBooking";
        oAPPBOK_BookingBE.UserID = Convert.ToInt32(Session["UserID"]);
        oAPPBOK_BookingBE.BookingID = Convert.ToInt32(GetQueryStringValue("ID").ToString());
        oAPPBOK_BookingBAL.DeleteBookingDetailBAL(oAPPBOK_BookingBE);

        #region Logic to send emails ...
        var BookingId = Convert.ToInt32(GetQueryStringValue("ID").ToString());
        oAPPBOK_BookingBE = new APPBOK_BookingBE();
        oAPPBOK_BookingBAL = new APPBOK_BookingBAL();
        oAPPBOK_BookingBE.Action = "GetDeletedBookingDeatilsByID";
        oAPPBOK_BookingBE.BookingID = BookingId;
        var lstBooking = oAPPBOK_BookingBAL.GetDeletedBookingDetailsByIDBAL(oAPPBOK_BookingBE);
        if (lstBooking.Count > 0)
            this.SendProvisionalRefusalLetterMail(lstBooking, BookingId);

        #endregion

        if (GetQueryStringValue("PNTB") != null && GetQueryStringValue("PNTB") == "True")
        {
            EncryptQueryString("~/ModuleUI/Appointment/ProvisionalBookings.aspx");
        }
        else if (GetQueryStringValue("BP") != null && GetQueryStringValue("BP") == "Pro")
        {
            EncryptQueryString("~/ModuleUI/Appointment/ProvisionalBookings.aspx");
        }
        else
        {
            EncryptQueryString("APPBok_BookingOverview.aspx");
        }
    }
    //-----------End Stage 9 point 2a/2b--------------//
    private void ProceedBooking()
    {

        if (hdnCurrentMode.Value == "Edit" && hdnPreviousVehicleType.Value != ddlVehicleType.SelectedValue)
        {
            MASSIT_VehicleDoorMatrixBE oMASSIT_VehicleDoorMatrixBE = new MASSIT_VehicleDoorMatrixBE();

            APPSIT_VehicleDoorMatrixBAL oMASSIT_VehicleDoorMatrixBAL = new APPSIT_VehicleDoorMatrixBAL();

            oMASSIT_VehicleDoorMatrixBE.Action = "VechileTypeMatched";
            oMASSIT_VehicleDoorMatrixBE.VehicleTypeID = Convert.ToInt32(ddlVehicleType.SelectedValue);
            oMASSIT_VehicleDoorMatrixBE.PreviousVehicleTypeID = Convert.ToInt32(hdnPreviousVehicleType.Value);
            isDoorTypeNotChanged = oMASSIT_VehicleDoorMatrixBAL.isVehcileTypeMatchedBAL(oMASSIT_VehicleDoorMatrixBE);
        }
        if (txtSchedulingdate.innerControltxtDate.Value != GetQueryStringValue("Scheduledate").ToString() && hdnCurrentMode.Value == "Edit")
        {
            btnNotHappy.Visible = true;
        }
        else
        {
            btnNotHappy.Visible = false;
        }
        bool isWarning = false;
        //Check Added PO//
        DataTable myDataTable = null;

        if (ViewState["myDataTable"] != null)
        {
            myDataTable = (DataTable)ViewState["myDataTable"];
        }
        else
        {
            myDataTable = new DataTable();
        }

        string errMsg = WebCommon.getGlobalResourceValue("BK_MS_17"); // "The Purchase Order/Delivery Reference field must be populated. Please enter either a Purchase Order number or a deliver reference";
        if (myDataTable.Rows.Count <= 0)
        {
            ShowErrorMessage(errMsg, "BK_MS_17");
            return;
        }
        //-------------//


        string TotalPallets = txtPallets.Text.Trim() != string.Empty ? txtPallets.Text.Trim() : "0";
        string TotalCartons = txtCartons.Text.Trim() != string.Empty ? txtCartons.Text.Trim() : "0";
        string TotalLifts = txtLifts.Text.Trim() != string.Empty ? txtLifts.Text.Trim() : "0";
        string TotalLines = txtExpectedLines.Text.Trim() != string.Empty ? txtExpectedLines.Text.Trim() : "0";

        //Stage 4 Point 13--//
        //Check Site Setting Max Volume per delivery per site
        MAS_SiteBE singleSiteSetting = GetSingleSiteSetting(Convert.ToInt32(ddlSite.innerControlddlSite.SelectedValue));
        if (
            (singleSiteSetting.MaxPalletsPerDelivery > 0
            && Convert.ToInt32(TotalPallets) > singleSiteSetting.MaxPalletsPerDelivery) ||
            (singleSiteSetting.MaxCartonsPerDelivery > 0 &&
            Convert.ToInt32(TotalCartons) > singleSiteSetting.MaxCartonsPerDelivery) ||
            (singleSiteSetting.MaxLinesPerDelivery > 0 &&
            Convert.ToInt32(TotalLines) > singleSiteSetting.MaxLinesPerDelivery))
        {

            errMsg = WebCommon.getGlobalResourceValue("BK_MS_35"); //You have broken the maximum allowed volume for this site. Please check your entry for pallets, cartons & lines.
            if (hdnCurrentRole.Value == "Vendor" || hdnCurrentRole.Value == "Carrier")
            {
                ShowErrorMessage(errMsg, "BK_MS_35");
                return;
            }
            else
            {
                AddWarningMessages(errMsg, "BK_MS_35");
                isWarning = true;
            }
        }

        //------------------//

        errMsg = WebCommon.getGlobalResourceValue("BK_MS_19"); // "There is insufficient volume information entered for this booking. The number of lines must be >0 and either pallets or cartons must be >0.";

        if (TotalPallets == "0" && TotalCartons == "0" && TotalLines == "0")
        {
            ShowErrorMessage(errMsg, "BK_MS_19");
            return;
        }
        else if ((Convert.ToInt32(TotalLines) == 0) || (Convert.ToInt32(TotalPallets) == 0 && Convert.ToInt32(TotalCartons) == 0))
        {
            ShowErrorMessage(errMsg, "BK_MS_19");
            return;
        }
        else if (Convert.ToInt32(TotalLines) > Convert.ToInt32(Lines))
        {
            if (!(hdnCurrentMode.Value == "Edit" && GetQueryStringValue("BP") != null && GetQueryStringValue("BP").ToString() == "Pro"))
            {  // If coming from Provisional page , ignore less lines check -- Phase 12 R2 Point 2
                errMsg = WebCommon.getGlobalResourceValue("BK_MS_26_Err");//Please check you entry as the number of lines entered is > than the number of outstanding lines.
                AddWarningMessages(errMsg, "BK_MS_26_Err");
                isWarning = true;
            }
        }
        else if (Convert.ToInt32(TotalLines) < Convert.ToInt32(Lines))
        {
            if (!(hdnCurrentMode.Value == "Edit" && GetQueryStringValue("BP") != null && GetQueryStringValue("BP").ToString() == "Pro"))
            {  // If coming from Provisional page , ignore less lines check -- Phase 12 R2 Point 2
                errMsg = WebCommon.getGlobalResourceValue("BK_MS_26_EXT"); // "The number of lines is less than expected. Please contact your Stock Planner.";
                AddWarningMessages(errMsg, "BK_MS_26_EXT");
                isWarning = true;
            }
        }

        //----Check Vendor Constraints----//
        UP_VendorBE[] newArray = null;
        List<UP_VendorBE> newList = null;

        if (ViewState["UPVendor"] != null)
        {
            newArray = (UP_VendorBE[])ViewState["UPVendor"];
            newList = new List<UP_VendorBE>(newArray);
        }

        if (hdnCurrentRole.Value == "Vendor" || hdnCurrentRole.Value == "Carrier")
        {
            errMsg = WebCommon.getGlobalResourceValue("BK_MS_18_EXT"); // "The volume being booked in exceeds the scheduled capacity for you.";
        }
        else
        {
            errMsg = WebCommon.getGlobalResourceValue("BK_MS_18_INT") + "[ " + ltvendorName.Text + " ]."; // "The volume being booked in exceeds the scheduled capacity for vendor [ " + ltvendorName.Text + " ]."; 
        }

        if (newList != null)
        {
            newList = newList.FindAll(delegate (UP_VendorBE ven)
            {
                return ven.VendorDetails.IsConstraintsDefined == true;
            });
        }

        if (newList != null && newList.Count > 0)
        {

            if (TotalPallets != "0")
            {
                if (Convert.ToInt32(TotalPallets) > newList[0].VendorDetails.APP_MaximumPallets)
                {
                    //Show err msg
                    if (hdnCurrentRole.Value == "Vendor" || hdnCurrentRole.Value == "Carrier")
                    {

                        ShowErrorMessage(errMsg, "BK_MS_18_EXT");
                        return;

                    }
                    else
                    {
                        if (!(hdnCurrentMode.Value == "Edit" && GetQueryStringValue("BP") != null && GetQueryStringValue("BP").ToString() == "Pro"))
                        {  // If coming from Provisional page , ignore less lines check -- Phase 12 R2 Point 2
                            AddWarningMessages(errMsg, "BK_MS_18_INT");
                            isWarning = true;
                        }
                    }
                }
            }

            if (TotalLines != "0")
            {
                if (Convert.ToInt32(TotalLines) > newList[0].VendorDetails.APP_MaximumLines)
                {
                    //Show err msg
                    if (hdnCurrentRole.Value == "Vendor" || hdnCurrentRole.Value == "Carrier")
                    {

                        ShowErrorMessage(errMsg, "BK_MS_18_EXT");
                        return;

                    }
                    else
                    {
                        if (!(hdnCurrentMode.Value == "Edit" && GetQueryStringValue("BP") != null && GetQueryStringValue("BP").ToString() == "Pro"))
                        {  // If coming from Provisional page , ignore less lines check -- Phase 12 R2 Point 2
                            AddWarningMessages(errMsg, "BK_MS_18_INT");
                            isWarning = true;
                        }
                    }
                }
            }
        }


        //----Stage 5 Point 1 Check chaeck Constraints----//
        if (ddlSite.IsBookingExclusions)
        {
            MASCNT_CarrierBE[] newArrayCarrier = null;
            List<MASCNT_CarrierBE> newListCarrier = null;


            if (ViewState["lstCarrier"] != null)
            {
                newArrayCarrier = (MASCNT_CarrierBE[])ViewState["lstCarrier"];
                newListCarrier = new List<MASCNT_CarrierBE>(newArrayCarrier);
            }

            if (hdnCurrentRole.Value == "Vendor")
            {
                errMsg = WebCommon.getGlobalResourceValue("BK_MS_18_INT_Carrier") + "[ " + ddlCarrier.SelectedItem.Text + " ]."; // ("BK_MS_18_EXT"); // "The volume being booked in exceeds the scheduled capacity for you.";
            }
            else
            {
                errMsg = WebCommon.getGlobalResourceValue("BK_MS_18_INT_Carrier") + "[ " + ddlCarrier.SelectedItem.Text + " ]."; // "The volume being booked in exceeds the scheduled capacity for vendor [ " + ltvendorName.Text + " ]."; 
            }

            errMsg = errMsg + "<br>";
            if (newListCarrier != null && newListCarrier.Count > 0 && newListCarrier[0].APP_MaximumPallets > 0)
            {
                errMsg = errMsg + WebCommon.getGlobalResourceValue("BK_MS_18_INT_MAXPallets").Replace("##MaxPallets##", newListCarrier[0].APP_MaximumPallets.ToString());
                errMsg = errMsg + "&nbsp;&nbsp;";
            }
            if (newListCarrier != null && newListCarrier.Count > 0 && newListCarrier[0].APP_MaximumLines > 0)
                errMsg = errMsg + WebCommon.getGlobalResourceValue("BK_MS_18_INT_MAXLines").Replace("##MaxLines##", newListCarrier[0].APP_MaximumLines.ToString());

            if (newArrayCarrier != null && newListCarrier.Count > 0)
            {

                if (TotalPallets != "0" && newListCarrier[0].APP_MaximumPallets != 0)
                {
                    if (Convert.ToInt32(TotalPallets) > newListCarrier[0].APP_MaximumPallets)
                    {
                        //Show err msg
                        if (hdnCurrentRole.Value == "Vendor")
                        {
                            ShowErrorMessage(errMsg, "BK_MS_18_EXT");
                            return;
                        }
                        else
                        {
                            if (!(hdnCurrentMode.Value == "Edit" && GetQueryStringValue("BP") != null && GetQueryStringValue("BP").ToString() == "Pro"))
                            {  // If coming from Provisional page , ignore fixed slot capacity -- Phase 12 R2 Point 2
                                AddWarningMessages(errMsg, "BK_MS_18_INT_Carrier");
                                isWarning = true;
                            }
                        }
                    }
                }

                if (TotalLines != "0" && newListCarrier[0].APP_MaximumLines != 0)
                {
                    if (Convert.ToInt32(TotalLines) > newListCarrier[0].APP_MaximumLines)
                    {
                        //Show err msg
                        if (hdnCurrentRole.Value == "Vendor")
                        {
                            ShowErrorMessage(errMsg, "BK_MS_18_EXT");
                            return;
                        }
                        else
                        {
                            if (!(hdnCurrentMode.Value == "Edit" && GetQueryStringValue("BP") != null && GetQueryStringValue("BP").ToString() == "Pro"))
                            {  // If coming from Provisional page , ignore fixed slot capacity -- Phase 12 R2 Point 2
                                AddWarningMessages(errMsg, "BK_MS_18_INT_Carrier");
                                isWarning = true;
                            }
                        }
                    }
                }
            }
        }
        //---Stage 5 Point 1 End--------------------//


        //Check Maximum Capacity for this day  - if not the confirm fixed slot case
        //check
        /* COMMENTED BACK AS PER REQUIREMENT OF PHASE-11-C POINT-1(17)*/

        if (hdnSuggestedWindowSlotID.Value == "-1" || hdnSuggestedWindowSlotID.Value == string.Empty)
        {
            if (hdnCurrentRole.Value == "Vendor" || hdnCurrentRole.Value == "Carrier")
            {
                errMsg = WebCommon.getGlobalResourceValue("BK_MS_18_INT_1"); //("BK_MS_18_EXT_1"); // "There is no capacity for any further deliveries on the date selected. Please select a different date or, if the delivery is urgent, please contact the booking office.";
            }
            else
            {
                errMsg = WebCommon.getGlobalResourceValue("BK_MS_18_INT_1"); //"There is no capacity for any further deliveries on the date selected. Click CONTINUE to make booking on this date.";
            }

            if (TotalPallets != "0" && hdnRemainingPallets.Value != string.Empty && hdnRemainingPallets.Value != "-")
            {
                if (Convert.ToInt32(TotalPallets) > (Convert.ToInt32(hdnRemainingPallets.Value) + BookedPallets))
                {
                    //Show err msg
                    if (hdnCurrentRole.Value == "Vendor" || hdnCurrentRole.Value == "Carrier")
                    {
                        //if (hdnCurrentMode.Value == "Add") {
                        //    ShowErrorMessage(errMsg, "BK_MS_18_EXT");
                        //    return;
                        //}
                        AddWarningMessages(errMsg, "BK_MS_18_INT_1");
                        isWarning = true;
                    }
                    else
                    {
                        AddWarningMessages(errMsg, "BK_MS_18_INT_1");
                        isWarning = true;
                    }
                }
            }
            if (TotalLines != "0" && hdnRemainingLines.Value != string.Empty && hdnRemainingLines.Value != "-")
            {
                if (Convert.ToInt32(TotalLines) > (Convert.ToInt32(hdnRemainingLines.Value) + BookedLines))
                {
                    //Show err msg
                    if (hdnCurrentRole.Value == "Vendor" || hdnCurrentRole.Value == "Carrier")
                    {
                        //if (hdnCurrentMode.Value == "Add") {
                        //    ShowErrorMessage(errMsg, "BK_MS_18_EXT");
                        //    return;
                        //}
                        AddWarningMessages(errMsg, "BK_MS_18_INT_1");
                        isWarning = true;
                    }
                    else
                    {
                        AddWarningMessages(errMsg, "BK_MS_18_INT_1");
                        isWarning = true;
                    }
                }
            }
        }
        //-------------------------------//



        if (hdnCurrentRole.Value == "Vendor")
        {
            btnWindowAlternateSlot.Visible = false;

            // Here checking Alternate Slot button allow for vendor or not.           
            if (singleSiteSetting != null)
            {
                if (singleSiteSetting.AlternateSlotOnOff.Trim().ToUpper() == "Y")
                    btnWindowAlternateSlotVendor.Visible = true;
                else
                    btnWindowAlternateSlotVendor.Visible = false;
            }
            else
                btnWindowAlternateSlotVendor.Visible = true;
        }
        else
        {
            btnWindowAlternateSlot.Visible = true;
            btnWindowAlternateSlotVendor.Visible = false;
        }

        if (isWarning == false)
        {
            GetOutstandingPO();
        }
        else
            ShowWarningMessages();


        ////Hide 'Non time Booking' and 'Alternate Slot' button in case of Confirm a fixed slot
        //if (hdnWindowSlotMode.Value == "true")
        //{
        //    btnMakeNonTimeBooking.Visible = false;
        //}

    }

    private void CalculateProposedBookingTimeSlot()
    {  //Find out unloading time for slot suggestion



        //----Start Non Time Delivary ------------//
        //check
        //if (hdnCurrentRole.Value == "Vendor")
        //{ // When vendor is loggedin

        //    DeliverTypenontime();
        //    IsNonTime = false;

        //    if (!Convert.ToBoolean(IsNonTimeVendor))
        //    {  //Check Non Time delivary for vendor

        //        if (!strDeliveryNonTime.Contains(ddlDeliveryType.SelectedItem.Value + ","))
        //        {  //Check Non Time delivary for Delivery Type

        //            bool IsPalletVolumeMatch = false;
        //            bool IsCartonVolumeMatch = false;

        //            if (txtPallets.Text.Trim() != string.Empty)
        //            {  //Check non time pallets 
        //                if (NonTimeDeliveryPalletVolume > Convert.ToInt32(txtPallets.Text.Trim()))
        //                {

        //                    IsPalletVolumeMatch = true;
        //                }
        //            }
        //            else
        //            {
        //                IsPalletVolumeMatch = true;
        //            }

        //            if (txtCartons.Text.Trim() != string.Empty)
        //            {  //Check non time cartons 
        //                if (NonTimeDeliveryCartonVolume > Convert.ToInt32(txtCartons.Text.Trim()))
        //                {

        //                    IsCartonVolumeMatch = true;
        //                }
        //            }
        //            else
        //            {
        //                IsCartonVolumeMatch = true;
        //            }

        //            if (IsPalletVolumeMatch && IsCartonVolumeMatch)
        //            {
        //                IsNonTime = true;
        //            }
        //        }
        //        else
        //        {
        //            IsNonTime = true;
        //        }
        //    }
        //    else
        //    {
        //        IsNonTime = true;
        //    }
        //}
        //else
        //{  // When OD staff is loggedin
        //    IsNonTime = true;
        //}
        //--------End of Non Time Delivary-------------------------//     

        BindWindowSlot();

        //btnMakeNonTimeBooking.Visible = Convert.ToBoolean(IsNonTime);
    }


    private bool GetWeekConstraints()
    {
        bool IsWeekConstraints = true;

        if (txtSchedulingdate.innerControltxtDate.Value != string.Empty && ddlSite.innerControlddlSite.SelectedItem.Value != string.Empty)
        {
            DateTime dtSelectedDate = LogicalSchedulDateTime;
            string WeekDay = dtSelectedDate.ToString("dddd");
            DateTime dtMon = Utilities.Common.GetFirstDayOfWeek(dtSelectedDate);

            MASSIT_WeekSetupBE oMASSIT_WeekSetupBE = new MASSIT_WeekSetupBE();
            MASSIT_WeekSetupBAL oMASSIT_WeekSetupBAL = new MASSIT_WeekSetupBAL();

            oMASSIT_WeekSetupBE.Action = "GetSpecificWeek";
            oMASSIT_WeekSetupBE.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
            oMASSIT_WeekSetupBE.EndWeekday = WeekDay;
            oMASSIT_WeekSetupBE.ScheduleDate = dtSelectedDate;

            List<MASSIT_WeekSetupBE> lstWeekSetup = oMASSIT_WeekSetupBAL.GetWeekSetupDetailsBAL(oMASSIT_WeekSetupBE);

            if (lstWeekSetup == null || lstWeekSetup.Count <= 0 || lstWeekSetup[0].StartTime == null)
            {

                oMASSIT_WeekSetupBE.Action = "ShowAll";
                lstWeekSetup = oMASSIT_WeekSetupBAL.GetWeekSetupDetailsBAL(oMASSIT_WeekSetupBE);
            }

            if (lstWeekSetup == null || lstWeekSetup.Count <= 0 || lstWeekSetup[0].StartTime == null)
            {
                IsWeekConstraints = false;
            }
        }
        return IsWeekConstraints;
    }

    //This method will check Total available time for window
    private bool CheckTotalAvailableTimeConstraint(MASSIT_TimeWindowBE objMASSIT_TimeWindowBE)
    {
        bool isTotalTimeConstraint = false;
        if (!string.IsNullOrEmpty(objMASSIT_TimeWindowBE.TotalHrsMinsAvailable))
        {
            string[] ArrayTotalTime = objMASSIT_TimeWindowBE.TotalHrsMinsAvailable.Split(':');
            int TotalTimeInMinute = (Convert.ToInt32(ArrayTotalTime[0]) * 60) + Convert.ToInt32(ArrayTotalTime[1]);

            int? TotalPallets = txtPallets.Text.Trim() != string.Empty ? Convert.ToInt32(txtPallets.Text.Trim()) : (int?)null;
            int? TotalCartons = txtCartons.Text.Trim() != string.Empty ? Convert.ToInt32(txtCartons.Text.Trim()) : (int?)null;
            int GetVolumeProcessTime = GetRequiredTimeSlotLength(TotalPallets, TotalCartons);


            APPBOK_BookingBE oAPPBOK_BookingBE = new APPBOK_BookingBE();
            APPBOK_BookingBAL oAPPBOK_BookingBAL = new APPBOK_BookingBAL();

            oAPPBOK_BookingBE.Action = "GetVolumeProcessed";
            oAPPBOK_BookingBE.ScheduleDate = LogicalSchedulDateTime;
            oAPPBOK_BookingBE.SiteId = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
            oAPPBOK_BookingBE.TimeWindow = new MASSIT_TimeWindowBE();
            oAPPBOK_BookingBE.TimeWindow.TimeWindowID = objMASSIT_TimeWindowBE.TimeWindowID;

            List<APPBOK_BookingBE> lstBookingDeatils = new List<APPBOK_BookingBE>();
            lstBookingDeatils = oAPPBOK_BookingBAL.GetGetVolumeProcessedBAL(oAPPBOK_BookingBE);

            if (lstBookingDeatils != null && lstBookingDeatils.Count > 0)
            {
                foreach (APPBOK_BookingBE Booking in lstBookingDeatils)
                {
                    GetVolumeProcessTime += GetRequiredTimeSlotLengthForOthers(Booking.NumberOfPallet, Booking.NumberOfCartons, Booking.SupplierType, Booking.VendorCarrierID, Booking.VehicleType.VehicleTypeID);
                }
            }
            if (TotalTimeInMinute >= GetVolumeProcessTime)
                isTotalTimeConstraint = false;
            else
                isTotalTimeConstraint = true;
        }
        return isTotalTimeConstraint;
    }

    //This method will check the Max time constraint for Window
    private bool CheckMaxTimeConstraint(MASSIT_TimeWindowBE objMASSIT_TimeWindowBE)
    {
        bool isMaxTimeConstraint = false;
        if (!string.IsNullOrEmpty(objMASSIT_TimeWindowBE.MaximumTime))
        {
            string[] ArrayMaxTime = objMASSIT_TimeWindowBE.MaximumTime.Split(':');
            int MaxTimeInMinute = 0;
            if (ArrayMaxTime.Length == 1)
                MaxTimeInMinute = (Convert.ToInt32(ArrayMaxTime[0]) * 60);
            else
                MaxTimeInMinute = (Convert.ToInt32(ArrayMaxTime[0]) * 60) + Convert.ToInt32(ArrayMaxTime[1]);
            int? TotalPallets = txtPallets.Text.Trim() != string.Empty ? Convert.ToInt32(txtPallets.Text.Trim()) : (int?)null;
            int? TotalCartons = txtCartons.Text.Trim() != string.Empty ? Convert.ToInt32(txtCartons.Text.Trim()) : (int?)null;
            if (MaxTimeInMinute >= GetRequiredTimeSlotLength(TotalPallets, TotalCartons))
                isMaxTimeConstraint = false;
            else
                isMaxTimeConstraint = true;
        }
        return isMaxTimeConstraint;
    }

    protected void BindWindowSlot()
    {

        string TotalPallets = txtPallets.Text.Trim() != string.Empty ? txtPallets.Text.Trim() : "0";
        string TotalCartons = txtCartons.Text.Trim() != string.Empty ? txtCartons.Text.Trim() : "0";
        string TotalLifts = txtLifts.Text.Trim() != string.Empty ? txtLifts.Text.Trim() : "0";
        string TotalLines = txtExpectedLines.Text.Trim() != string.Empty ? txtExpectedLines.Text.Trim() : "0";

        //-----------------Start Collect Basic Information --------------------------//
        DateTime dtSelectedDate = LogicalSchedulDateTime;
        string WeekDay = dtSelectedDate.ToString("dddd");


        #region Commented

        //MASSIT_WeekSetupBE oMASSIT_WeekSetupBE = new MASSIT_WeekSetupBE();
        //MASSIT_WeekSetupBAL oMASSIT_WeekSetupBAL = new MASSIT_WeekSetupBAL();

        //oMASSIT_WeekSetupBE.Action = "GetSpecificWeek";
        //oMASSIT_WeekSetupBE.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
        //oMASSIT_WeekSetupBE.EndWeekday = WeekDay;
        //oMASSIT_WeekSetupBE.ScheduleDate = dtSelectedDate;

        //List<MASSIT_WeekSetupBE> lstWeekSetup = oMASSIT_WeekSetupBAL.GetWeekSetupDetailsBAL(oMASSIT_WeekSetupBE);

        //if (lstWeekSetup == null || lstWeekSetup.Count <= 0 || lstWeekSetup[0].StartTime == null)
        //{

        //    oMASSIT_WeekSetupBE.Action = "ShowAll";
        //    lstWeekSetup = oMASSIT_WeekSetupBAL.GetWeekSetupDetailsBAL(oMASSIT_WeekSetupBE);
        //}
        ////----------------------------------------------------------------------------//
        ////test3
        //APPBOK_BookingBE oAPPBOK_BookingBE = new APPBOK_BookingBE();
        //APPBOK_BookingBAL oAPPBOK_BookingBAL = new APPBOK_BookingBAL();
        ////oAPPBOK_BookingBE.Carrier = new MASCNT_CarrierBE();

        //oAPPBOK_BookingBE.Action = "VendorBookingFixedSlotDetails_Test";
        //oAPPBOK_BookingBE.SiteId = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
        //oAPPBOK_BookingBE.VendorID = Convert.ToInt32(ucSeacrhVendorID); // Convert.ToInt32(ucSeacrhVendor1.VendorNo);
        //oAPPBOK_BookingBE.ScheduleDate = LogicalSchedulDateTime;
        //oAPPBOK_BookingBE.SelectedScheduleDate = LogicalSchedulDateTime;
        //List<APPBOK_BookingBE> lstBookings = oAPPBOK_BookingBAL.GetVendorBookingFixedSlotDetailsBAL(oAPPBOK_BookingBE);

        //Filter out fixed slot not in timeslot range
        //lstBookings = lstBookings.FindAll(delegate(APPBOK_BookingBE St)
        //{
        //    DateTime? dt = (DateTime?)null;
        //    if (St.SlotTime.SlotTime != null && St.SlotTime.SlotTime != string.Empty)
        //    {
        //        dt = new DateTime(1900, 1, 1, Convert.ToInt32(St.SlotTime.SlotTime.Split(':')[0]), Convert.ToInt32(St.SlotTime.SlotTime.Split(':')[1]), 0);
        //    }
        //    else
        //    {
        //        dt = new DateTime(1900, 1, 1, 0, 0, 0);
        //    }

        //    return dt <= lstWeekSetup[0].EndTime;
        //});


        //if (lstWeekSetup.Count > 0 && lstWeekSetup[0].StartTime != null)
        //{

        //    if (lstWeekSetup[0].StartWeekday != lstWeekSetup[0].EndWeekday)
        //    {
        //        oAPPBOK_BookingBE.SelectedScheduleDate = LogicalSchedulDateTime.AddDays(-1);  //VendorBookingFixedSlotDetails

        //        List<APPBOK_BookingBE> lstBookingsStartDay = oAPPBOK_BookingBAL.GetVendorBookingFixedSlotDetailsBAL(oAPPBOK_BookingBE);

        //        lstBookingsStartDay = lstBookingsStartDay.FindAll(delegate(APPBOK_BookingBE St)
        //        {
        //            DateTime? dt = (DateTime?)null;
        //            if (St.SlotTime.SlotTime != null && St.SlotTime.SlotTime != string.Empty)
        //            {
        //                dt = new DateTime(1900, 1, 1, Convert.ToInt32(St.SlotTime.SlotTime.Split(':')[0]), Convert.ToInt32(St.SlotTime.SlotTime.Split(':')[1]), 0);
        //            }
        //            else
        //            {
        //                dt = new DateTime(1900, 1, 1, 0, 0, 0);
        //            }



        //            return dt >= lstWeekSetup[0].StartTime;
        //        });

        //        if (lstBookingsStartDay != null && lstBookingsStartDay.Count > 0)
        //        {
        //            lstBookings = MergeListCollections(lstBookingsStartDay, lstBookings);
        //        }
        //    }
        //}

        //-----------------Get top fisrt unused fixed slot to be remonded-------------//
        //bool bUnconfirmedFixedSlotFound = false;
        //List<APPBOK_BookingBE> lstUnuseedFixedSlot = lstBookings.FindAll(delegate(APPBOK_BookingBE St) { return St.BookingRef == "Fixed booking"; });
        //if (lstUnuseedFixedSlot != null && lstUnuseedFixedSlot.Count > 0)
        //{
        //    bUnconfirmedFixedSlotFound = true;
        //}
        //----------------------------------------------------------------------------//
        #endregion
        //-------IF NO UNCONFIRMED FIXED SLOT NOT FOUND , CHECK THE SKU/DOOR MATRIX - SPRINT 4 POINT 7
        #region SKUCheck

        bool bSKUMatrixDoorFound = false;
        int iSiteDoorNumberID = 0;
        int iSKUDoorTypeID = 0;
        //if (bUnconfirmedFixedSlotFound == false)
        //{
        DataTable myDataTable = null;
        if (ViewState["myDataTable"] != null)
        {
            myDataTable = (DataTable)ViewState["myDataTable"];

            var column2Values = myDataTable.AsEnumerable().Select(x => x.Field<string>("PurchaseNumber"));
            string PurchaseNumbers = String.Join(",", column2Values.ToArray());
            if (!string.IsNullOrEmpty(PurchaseNumbers))
            {

                APPSIT_DoorOpenTimeBAL oMASSIT_DoorOpenTimeBAL = new APPSIT_DoorOpenTimeBAL();
                MASSIT_DoorOpenTimeBE oMASSIT_DoorOpenTimeBE = new MASSIT_DoorOpenTimeBE();

                oMASSIT_DoorOpenTimeBE.Action = "GetSKUMatrixDoorDetails";
                oMASSIT_DoorOpenTimeBE.DoorNo = new MASSIT_DoorNoSetupBE();
                oMASSIT_DoorOpenTimeBE.DoorNo.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
                oMASSIT_DoorOpenTimeBE.PurchaseNumbers = PurchaseNumbers;


                List<MASSIT_DoorOpenTimeBE> lstSKUMatrixDoorDetails = oMASSIT_DoorOpenTimeBAL.GetDoorNoSetupDetailsBAL(oMASSIT_DoorOpenTimeBE);

                if (lstSKUMatrixDoorDetails != null && lstSKUMatrixDoorDetails.Count > 0)
                {
                    bSKUMatrixDoorFound = true;
                    iSiteDoorNumberID = lstSKUMatrixDoorDetails[0].SiteDoorNumberID;
                    iSKUDoorTypeID = lstSKUMatrixDoorDetails[0].DoorType.DoorTypeID;
                    if (lstSKUMatrixDoorDetails.Count > 1)
                    {  //there is a conflict i.e the PO's contain SKU with different doors 
                        isProvisional = true;
                        ProvisionalReason = "WINDOW CAPACITY";
                    }
                }
            }
        }
        #endregion
        //}
        //------------------------------------------------------------------------//
        #region Commented

        //oAPPBOK_BookingBE.Action = "VendorBookingFindFixedSlotDoor";
        //oAPPBOK_BookingBE.SiteId = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
        //oAPPBOK_BookingBE.VendorID = Convert.ToInt32(ucSeacrhVendorID); // Convert.ToInt32(ucSeacrhVendor1.VendorNo);     

        //oAPPBOK_BookingBE.ScheduleDate = LogicalSchedulDateTime;
        //oAPPBOK_BookingBE.VehicleType = new MASSIT_VehicleTypeBE();
        ////if (bUnconfirmedFixedSlotFound)
        ////    oAPPBOK_BookingBE.VehicleType.VehicleTypeID = 0;
        ////else
        //    oAPPBOK_BookingBE.VehicleType.VehicleTypeID = Convert.ToInt32(ddlVehicleType.SelectedItem.Value);
        //List<APPBOK_BookingBE> lstFixedDoor = oAPPBOK_BookingBAL.GetVendorBookingFixedSlotDoorBAL(oAPPBOK_BookingBE);

        //if ((hdnSuggestedWindowSlotID.Value == "" || hdnSuggestedWindowSlotID.Value == "-1") && hdnBookingRef.Value == "")
        //{
        //    if (lstFixedDoor.Count > 0)
        //    {
        //        if (lstFixedDoor.FindAll(delegate(APPBOK_BookingBE St) { return St.VehicleType.VehicleTypeID == Convert.ToInt32(ddlVehicleType.SelectedItem.Value); }).Count <= 0)
        //        {
        //            isProvisional = true;
        //        }
        //    }
        //}

        //lstFixedDoor = lstFixedDoor.FindAll(delegate(APPBOK_BookingBE St)
        //{
        //    DateTime? dt = (DateTime?)null;
        //    if (St.SlotTime.SlotTime != null && St.SlotTime.SlotTime != string.Empty)
        //    {
        //        dt = new DateTime(1900, 1, 1, Convert.ToInt32(St.SlotTime.SlotTime.Split(':')[0]), Convert.ToInt32(St.SlotTime.SlotTime.Split(':')[1]), 0);
        //    }
        //    else
        //    {
        //        dt = new DateTime(1900, 1, 1, 0, 0, 0);
        //    }

        //    return dt <= lstWeekSetup[0].EndTime;
        //});

        //if (lstWeekSetup.Count > 0 && lstWeekSetup[0].StartTime != null)
        //{
        //    if (lstWeekSetup[0].StartWeekday != lstWeekSetup[0].EndWeekday)
        //    {
        //        oAPPBOK_BookingBE.ScheduleDate = LogicalSchedulDateTime.AddDays(-1);
        //        List<APPBOK_BookingBE> lstFixedDoorStartDay = oAPPBOK_BookingBAL.GetVendorBookingFixedSlotDoorBAL(oAPPBOK_BookingBE);

        //        lstFixedDoorStartDay = lstFixedDoorStartDay.FindAll(delegate(APPBOK_BookingBE St)
        //        {
        //            DateTime? dt = (DateTime?)null;
        //            if (St.SlotTime.SlotTime != null && St.SlotTime.SlotTime != string.Empty)
        //            {
        //                dt = new DateTime(1900, 1, 1, Convert.ToInt32(St.SlotTime.SlotTime.Split(':')[0]), Convert.ToInt32(St.SlotTime.SlotTime.Split(':')[1]), 0);
        //            }
        //            else
        //            {
        //                dt = new DateTime(1900, 1, 1, 0, 0, 0);
        //            }

        //            return dt >= lstWeekSetup[0].StartTime;
        //        });

        //        if (lstFixedDoorStartDay != null && lstFixedDoorStartDay.Count > 0)
        //        {
        //            lstFixedDoor = MergeListCollections(lstFixedDoor, lstFixedDoorStartDay);
        //        }
        //    }
        //}




        //List<APPBOK_BookingBE> lstAvailableFixedDoorSlots = lstFixedDoor;


        ////filter Fixed Slots on the basis of Fixed Slot Setup's Volume MaximumCatrons capacity 
        //lstAvailableFixedDoorSlots = lstFixedDoor.FindAll(delegate(APPBOK_BookingBE vol)
        //{
        //    if (vol.FixedSlot.MaximumCatrons > 0)
        //    {
        //        if (vol.FixedSlot.MaximumCatrons >= Convert.ToInt32(TotalCartons))
        //        {
        //            // We just found fixed slot. Capture it.
        //            return true;
        //        }
        //        else
        //        {
        //            // Not a fixed slot
        //            return false;
        //        }
        //    }
        //    else
        //    {
        //        return true;
        //    }
        //});

        ////filter Fixed Slots on the basis of Fixed Slot Setup's Volume MaximumLines capacity
        //lstAvailableFixedDoorSlots = lstAvailableFixedDoorSlots.FindAll(delegate(APPBOK_BookingBE vol)
        //{
        //    if (vol.FixedSlot.MaximumLines > 0)
        //    {
        //        if (vol.FixedSlot.MaximumLines >= Convert.ToInt32(TotalLines))
        //        {
        //            // We just found fixed slot. Capture it.
        //            return true;
        //        }
        //        else
        //        {
        //            // Not a fixed slot
        //            return false;
        //        }
        //    }
        //    else
        //    {
        //        return true;
        //    }
        //});

        ////filter Fixed Slots on the basis of Fixed Slot Setup's Volume MaximumPallets capacity
        //lstAvailableFixedDoorSlots = lstAvailableFixedDoorSlots.FindAll(delegate(APPBOK_BookingBE vol)
        //{
        //    if (vol.FixedSlot.MaximumPallets > 0)
        //    {
        //        if (vol.FixedSlot.MaximumPallets >= Convert.ToInt32(TotalPallets))
        //        {
        //            // We just found fixed slot. Capture it.
        //            return true;
        //        }
        //        else
        //        {
        //            // Not a fixed slot
        //            return false;
        //        }
        //    }
        //    else
        //    {
        //        return true;
        //    }
        //});

        ////------------------------------//



        //// Not a fixed slot        
        //if (lstFixedDoor.Count > 0 && lstAvailableFixedDoorSlots.Count <= 0)
        //{
        //    lstAvailableFixedDoorSlots = lstFixedDoor;
        //    isProvisional = true;
        //    //if (bUnconfirmedFixedSlotFound == false)
        //        hdnCapacityWarning.Value = "true";
        //}
        //else
        //{
        //    if (hdnSuggestedWindowSlotID.Value != "" && hdnSuggestedWindowSlotID.Value != "-1")
        //    {
        //        if (lstAvailableFixedDoorSlots.FindAll(delegate(APPBOK_BookingBE St) { return St.FixedSlot.FixedSlotID == Convert.ToInt32(hdnSuggestedWindowSlotID.Value); }).Count <= 0)
        //        {
        //            isProvisional = true;
        //        }
        //    }
        //}

        ////------Check for Specific Entry First--------//    

        //DateTime dtMon = Utilities.Common.GetFirstDayOfWeek(dtSelectedDate);
        //string StartWeekday = dtSelectedDate.ToString("dddd");
        //if (lstWeekSetup.Count > 0 && lstWeekSetup[0].StartTime != null)
        //{
        //    if (lstWeekSetup[0].StartWeekday != lstWeekSetup[0].EndWeekday)
        //    {
        //        StartWeekday = dtSelectedDate.AddDays(-1).ToString("dddd");
        //    }
        //}

        //string SlotIDs = string.Empty;

        //SlotIDs = GetDoorConstraints();

        //if (bUnconfirmedFixedSlotFound == false)
        //    lstBookings = lstBookings.FindAll(delegate(APPBOK_BookingBE St) { return SlotIDs.Contains(St.SlotTime.SlotTimeID + "@"); });

        //------------------------------------------------------//

        ////FILETR OUT NON TIME DELIVERY
        //lstBookings = lstBookings.FindAll(delegate(APPBOK_BookingBE St) { return St.SlotTime.SlotTime != ""; });

        //if (lstBookings.Count > 0) {
        //    gvBookingSlots.DataSource = lstBookings;
        //    gvBookingSlots.DataBind();
        //}

        //filter Booked Slots which are alrady booked
        //List<APPBOK_BookingBE> lstAvailableSlots = new List<APPBOK_BookingBE>();
        //if (hdnCurrentMode.Value == "Add")
        //{
        //    lstAvailableSlots = lstBookings.FindAll(delegate(APPBOK_BookingBE St) { return St.Status != "Booked"; });
        //}
        //else if (hdnCurrentMode.Value == "Edit")
        //{
        //    lstAvailableSlots = lstBookings;
        //}
        #endregion


        #region Find reserved window
        MASSIT_TimeWindowBE oMASSIT_TimeWindowBE = new MASSIT_TimeWindowBE();
        MASSIT_TimeWindowBAL oMASSIT_TimeWindowBAL = new MASSIT_TimeWindowBAL();

        if (hdnWindowSlotMode.Value != "true")
        {
            //Get all reserved window           
            oMASSIT_TimeWindowBE.Action = "ShowAllReserved";
            oMASSIT_TimeWindowBE.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
            oMASSIT_TimeWindowBE.ScheduleDate = LogicalSchedulDateTime;
            oMASSIT_TimeWindowBE.IncludedVendorIDs = Convert.ToString(ucSeacrhVendorID); ;
            if (hdnCurrentMode.Value == "Edit" && hdnSuggestedWindowSlotID.Value != "" && hdnSuggestedWindowSlotID.Value != "-1")
            {
                //if (hdnCurrentMode.Value == "Edit"){
                oMASSIT_TimeWindowBE.TimeWindowID = Convert.ToInt32(hdnSuggestedWindowSlotID.Value);
            }
            if (hdnCurrentMode.Value == "Add" && hdnSuggestedWindowSlotID.Value != "" && hdnSuggestedWindowSlotID.Value != "-1")
            {
                string TimeWindowID = GetQueryStringValue("TimeWindowID");
                oMASSIT_TimeWindowBE.TimeWindowID = Convert.ToInt32(TimeWindowID);
            }
            else
            {
                oMASSIT_TimeWindowBE.TimeWindowID = -1;
            }

            List<MASSIT_TimeWindowBE> lstFixedSlot = oMASSIT_TimeWindowBAL.GetReservedWindowBAL(oMASSIT_TimeWindowBE);

            if (txtSchedulingdate.innerControltxtDate.Value == GetQueryStringValue("Scheduledate").ToString() && hdnPreviousVehicleType.Value != ddlVehicleType.SelectedValue && isDoorTypeNotChanged == false && hdnCurrentMode.Value == "Edit")
            {
                hdnSuggestedWindowSlotID.Value = "";
            }
            else if (txtSchedulingdate.innerControltxtDate.Value != GetQueryStringValue("Scheduledate").ToString() && hdnCurrentMode.Value == "Edit")
            {
                hdnSuggestedWindowSlotID.Value = "";
            }

            if (lstFixedSlot != null && lstFixedSlot.Count > 0)
            {



                //Get Booked Volume
                MASSIT_TimeWindowBE objMASSIT_TimeWindowBE = new MASSIT_TimeWindowBE();

                objMASSIT_TimeWindowBE.Action = "GetBookedVolume";
                objMASSIT_TimeWindowBE.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
                objMASSIT_TimeWindowBE.ScheduleDate = LogicalSchedulDateTime;
                //Edit Case
                //if ((hdnSuggestedWindowSlotID.Value == "" || hdnSuggestedWindowSlotID.Value == "-1") && hdnBookingRef.Value != "") {
                //    objMASSIT_TimeWindowBE.BookingRef = hdnBookingRef.Value;
                //}
                if (hdnCurrentMode.Value == "Edit")
                {
                    objMASSIT_TimeWindowBE.BookingRef = hdnBookingRef.Value;
                }

                List<MASSIT_TimeWindowBE> lstBookedVolume = oMASSIT_TimeWindowBAL.GetBookedVolumeBAL(objMASSIT_TimeWindowBE);

                bool IsReservedWindowFound = false;

                if ((lstBookedVolume != null && lstBookedVolume.Count > 0) || (hdnBookingRef.Value != ""))
                {

                    foreach (MASSIT_TimeWindowBE oTimeWindowBE in lstFixedSlot)
                    {

                        List<MASSIT_TimeWindowBE> lstBookedVolumePerposed = null;

                        lstBookedVolumePerposed = lstBookedVolume.Where(vol => vol.TimeWindowID == oTimeWindowBE.TimeWindowID).ToList();

                        if (lstBookedVolumePerposed != null && lstBookedVolumePerposed.Count > 0 && hdnCurrentMode.Value == "Add")
                        {


                            //skip in edit case
                            // if (!(hdnCurrentMode.Value == "Edit" && hdnSuggestedWindowSlotID.Value != "" && hdnSuggestedWindowSlotID.Value != "-1")) {
                            lstBookedVolumePerposed = lstBookedVolumePerposed.FindAll(delegate (MASSIT_TimeWindowBE vol)
                            {
                                if (oTimeWindowBE.MaxDeliveries > 0)
                                {
                                    if (vol.MaxDeliveries > 0)
                                    {
                                        //check maximim delivery
                                        if (oTimeWindowBE.MaxDeliveries >= vol.MaxDeliveries)
                                        {
                                            // We just found fixed slot. Capture it.
                                            return true;
                                        }
                                        else
                                        {
                                            // Not a fixed slot
                                            return false;
                                        }
                                    }
                                    else
                                    {
                                        return false;
                                    }
                                }
                                else
                                {
                                    return true;
                                }

                            });
                            //}

                            lstBookedVolumePerposed = lstBookedVolumePerposed.FindAll(delegate (MASSIT_TimeWindowBE vol)
                            {
                                if (oTimeWindowBE.MaximumCartons > 0)
                                {
                                    if (vol.MaximumCartons <= 0)
                                    {
                                        return false;
                                        //if (oTimeWindowBE.MaximumCartons - vol.MaximumCartons >= 0) {
                                        //    // We just found fixed slot. Capture it.
                                        //    return true;
                                        //}
                                        //else {
                                        //    // Not a fixed slot
                                        //    return false;
                                        //}
                                    }
                                    else
                                    {
                                        return true;
                                    }
                                }
                                else
                                {
                                    return true;
                                }
                            });

                            lstBookedVolumePerposed = lstBookedVolumePerposed.FindAll(delegate (MASSIT_TimeWindowBE vol)
                            {
                                if (oTimeWindowBE.MaximumPallets > 0)
                                {
                                    if (vol.MaximumPallets <= 0)
                                    {
                                        //if (oTimeWindowBE.MaximumPallets - vol.MaximumPallets >= 0) {
                                        //    // We just found fixed slot. Capture it.
                                        //    return true;
                                        //}
                                        //else {
                                        //    // Not a fixed slot
                                        //    return false;
                                        //}
                                        return false;
                                    }
                                    else
                                    {
                                        return true;
                                    }
                                }
                                else
                                {
                                    return true;
                                }
                            });

                            lstBookedVolumePerposed = lstBookedVolumePerposed.FindAll(delegate (MASSIT_TimeWindowBE vol)
                            {
                                if (oTimeWindowBE.MaximumLines > 0)
                                {
                                    if (vol.MaximumLines <= 0)
                                    {
                                        //if (vol.MaximumLines <= 0) {
                                        //    // We just found fixed slot. Capture it.
                                        //    return true;
                                        //}
                                        //else {
                                        //    // Not a fixed slot
                                        //    return false;
                                        //}
                                        return false;
                                    }
                                    else
                                    {
                                        return true;
                                    }
                                }
                                else
                                {
                                    return true;
                                }
                            });

                            if (lstBookedVolumePerposed != null && lstBookedVolumePerposed.Count > 0)
                            {

                                IsReservedWindowFound = true;

                                if (hdnCurrentMode.Value == "Add")
                                {

                                    ltBookingDate.Text = oTimeWindowBE.StartTime + " - " + oTimeWindowBE.EndTime + " on " + LogicalSchedulDateTime.Day.ToString() + "/" + LogicalSchedulDateTime.Month.ToString() + "/" + LogicalSchedulDateTime.Year.ToString();
                                    ltSuggestedSlotTime.Text = AtDoorName + " " + oTimeWindowBE.DoorNumber.ToString();
                                    hdnPreservSlot.Value = oTimeWindowBE.StartSlotTimeID + "|" + oTimeWindowBE.EndSlotTimeID + "|" + oTimeWindowBE.SiteDoorNumberID.ToString();
                                    hdnSuggestedWindowSlotID.Value = oTimeWindowBE.TimeWindowID.ToString();
                                    hdnSuggestedSiteDoorNumberID.Value = oTimeWindowBE.SiteDoorNumberID.ToString();
                                    hdnSuggestedSiteDoorNumber.Value = oTimeWindowBE.DoorNumber.ToString();
                                    hdnSuggestedWeekDay.Value = LogicalSchedulDateTime.ToString("ddd");
                                }

                                //if (!(hdnCurrentMode.Value == "Edit" && hdnSuggestedWindowSlotID.Value != "" && hdnSuggestedWindowSlotID.Value != "-1")) {
                                lstBookedVolumePerposed = lstBookedVolumePerposed.FindAll(delegate (MASSIT_TimeWindowBE vol)
                                {
                                    if (oTimeWindowBE.MaximumCartons > 0)
                                    {
                                        if (vol.MaximumCartons >= Convert.ToInt32(TotalCartons))
                                        {
                                            // We just found fixed slot. Capture it.
                                            return true;
                                        }
                                        else
                                        {
                                            // Not a fixed slot
                                            return false;
                                        }
                                    }
                                    else
                                    {
                                        return true;
                                    }
                                });


                                //}
                                //else {
                                //    lstBookedVolumePerposed = lstBookedVolumePerposed.FindAll(delegate(MASSIT_TimeWindowBE vol) {
                                //        if (oTimeWindowBE.MaximumCartons > 0) {
                                //            if (oTimeWindowBE.MaximumCartons >= Convert.ToInt32(TotalCartons)) {
                                //                // We just found fixed slot. Capture it.
                                //                return true;
                                //            }
                                //            else {
                                //                // Not a fixed slot
                                //                return false;
                                //            }
                                //        }
                                //        else {
                                //            return true;
                                //        }
                                //    });
                                //}

                                if (lstBookedVolumePerposed.Count == 0)
                                {
                                    isProvisional = true;
                                    ProvisionalReason = "RESERVED WINDOW CAPACITY";
                                    break;
                                }


                                // if (!(hdnCurrentMode.Value == "Edit" && hdnSuggestedWindowSlotID.Value != "" && hdnSuggestedWindowSlotID.Value != "-1")) {
                                //CHECK MAXIMUM Pallets
                                lstBookedVolumePerposed = lstBookedVolumePerposed.FindAll(delegate (MASSIT_TimeWindowBE vol)
                                {
                                    if (oTimeWindowBE.MaximumPallets > 0)
                                    {
                                        if (vol.MaximumPallets >= Convert.ToInt32(TotalPallets))
                                        {
                                            // We just found fixed slot. Capture it.
                                            return true;
                                        }
                                        else
                                        {
                                            // Not a fixed slot
                                            return false;
                                        }
                                    }
                                    else
                                    {
                                        return true;
                                    }
                                });
                                //}
                                //else {
                                //    lstBookedVolumePerposed = lstBookedVolumePerposed.FindAll(delegate(MASSIT_TimeWindowBE vol) {
                                //        if (oTimeWindowBE.MaximumPallets > 0) {
                                //            if (oTimeWindowBE.MaximumPallets >= Convert.ToInt32(TotalPallets)) {
                                //                // We just found fixed slot. Capture it.
                                //                return true;
                                //            }
                                //            else {
                                //                // Not a fixed slot
                                //                return false;
                                //            }
                                //        }
                                //        else {
                                //            return true;
                                //        }
                                //    });
                                //}


                                if (lstBookedVolumePerposed.Count == 0)
                                {
                                    isProvisional = true;
                                    ProvisionalReason = "RESERVED WINDOW CAPACITY";
                                    break;
                                }

                                //if (!(hdnCurrentMode.Value == "Edit" && hdnSuggestedWindowSlotID.Value != "" && hdnSuggestedWindowSlotID.Value != "-1")) {
                                //CHECK MAXIMUM Lines
                                lstBookedVolumePerposed = lstBookedVolumePerposed.FindAll(delegate (MASSIT_TimeWindowBE vol)
                                {
                                    if (oTimeWindowBE.MaximumLines > 0)
                                    {
                                        if (vol.MaximumLines >= Convert.ToInt32(TotalLines))
                                        {
                                            // We just found fixed slot. Capture it.
                                            return true;
                                        }
                                        else
                                        {
                                            // Not a fixed slot
                                            return false;
                                        }
                                    }
                                    else
                                    {
                                        return true;
                                    }
                                });
                                //}
                                //else {
                                //    lstBookedVolumePerposed = lstBookedVolumePerposed.FindAll(delegate(MASSIT_TimeWindowBE vol) {
                                //        if (oTimeWindowBE.MaximumLines > 0) {
                                //            if (oTimeWindowBE.MaximumLines >= Convert.ToInt32(TotalLines)) {
                                //                // We just found fixed slot. Capture it.
                                //                return true;
                                //            }
                                //            else {
                                //                // Not a fixed slot
                                //                return false;
                                //            }
                                //        }
                                //        else {
                                //            return true;
                                //        }
                                //    });
                                //}

                                if (lstBookedVolumePerposed.Count == 0)
                                {
                                    isProvisional = true;
                                    ProvisionalReason = "RESERVED WINDOW CAPACITY";
                                    break;
                                }
                            }

                            if (IsReservedWindowFound)
                                break;
                        }
                        else
                        {
                            if (hdnCurrentMode.Value == "Add")
                            {

                                ltBookingDate.Text = oTimeWindowBE.StartTime + " - " + oTimeWindowBE.EndTime + " on " + LogicalSchedulDateTime.Day.ToString() + "/" + LogicalSchedulDateTime.Month.ToString() + "/" + LogicalSchedulDateTime.Year.ToString();
                                ltSuggestedSlotTime.Text = AtDoorName + " " + oTimeWindowBE.DoorNumber.ToString();
                                hdnPreservSlot.Value = oTimeWindowBE.StartSlotTimeID + "|" + oTimeWindowBE.EndSlotTimeID + "|" + oTimeWindowBE.SiteDoorNumberID.ToString();
                                hdnSuggestedWindowSlotID.Value = oTimeWindowBE.TimeWindowID.ToString();
                                hdnSuggestedSiteDoorNumberID.Value = oTimeWindowBE.SiteDoorNumberID.ToString();
                                hdnSuggestedSiteDoorNumber.Value = oTimeWindowBE.DoorNumber.ToString();
                                hdnSuggestedWeekDay.Value = LogicalSchedulDateTime.ToString("ddd");
                                break;
                            }

                            if (hdnCurrentMode.Value == "Edit")
                            {
                                if (oTimeWindowBE.MaximumCartons > 0)
                                {
                                    if (oTimeWindowBE.MaximumCartons < Convert.ToInt32(TotalCartons))
                                    {
                                        isProvisional = true;
                                        ProvisionalReason = "RESERVED WINDOW CAPACITY";
                                    }
                                }

                                if (oTimeWindowBE.MaximumPallets > 0)
                                {
                                    if (oTimeWindowBE.MaximumPallets < Convert.ToInt32(TotalPallets))
                                    {
                                        isProvisional = true;
                                        ProvisionalReason = "RESERVED WINDOW CAPACITY";
                                    }
                                }


                                if (oTimeWindowBE.MaximumLines > 0)
                                {
                                    if (oTimeWindowBE.MaximumLines < Convert.ToInt32(TotalLines))
                                    {
                                        isProvisional = true;
                                        ProvisionalReason = "RESERVED WINDOW CAPACITY";
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    if (hdnCurrentMode.Value == "Add")
                    {
                        MASSIT_TimeWindowBE oTimeWindowBE = lstFixedSlot[0];

                        ltBookingDate.Text = oTimeWindowBE.StartTime + " - " + oTimeWindowBE.EndTime + " on " + LogicalSchedulDateTime.Day.ToString() + "/" + LogicalSchedulDateTime.Month.ToString() + "/" + LogicalSchedulDateTime.Year.ToString();
                        ltSuggestedSlotTime.Text = AtDoorName + " " + oTimeWindowBE.DoorNumber.ToString();
                        hdnPreservSlot.Value = oTimeWindowBE.StartSlotTimeID + "|" + oTimeWindowBE.EndSlotTimeID + "|" + oTimeWindowBE.SiteDoorNumberID.ToString();
                        hdnSuggestedWindowSlotID.Value = oTimeWindowBE.TimeWindowID.ToString();
                        hdnSuggestedSiteDoorNumberID.Value = oTimeWindowBE.SiteDoorNumberID.ToString();
                        hdnSuggestedSiteDoorNumber.Value = oTimeWindowBE.DoorNumber.ToString();
                        hdnSuggestedWeekDay.Value = LogicalSchedulDateTime.ToString("ddd");

                        if (oTimeWindowBE.MaximumCartons > 0)
                        {
                            if (oTimeWindowBE.MaximumCartons < Convert.ToInt32(TotalCartons))
                            {
                                isProvisional = true;
                                ProvisionalReason = "RESERVED WINDOW CAPACITY";
                            }
                        }

                        if (oTimeWindowBE.MaximumPallets > 0)
                        {
                            if (oTimeWindowBE.MaximumPallets < Convert.ToInt32(TotalPallets))
                            {
                                isProvisional = true;
                                ProvisionalReason = "RESERVED WINDOW CAPACITY";
                            }
                        }


                        if (oTimeWindowBE.MaximumLines > 0)
                        {
                            if (oTimeWindowBE.MaximumLines < Convert.ToInt32(TotalLines))
                            {
                                isProvisional = true;
                                ProvisionalReason = "RESERVED WINDOW CAPACITY";
                            }
                        }
                    }
                }
            }
        }

        if (hdnWindowSlotMode.Value == "true" && hdnCurrentMode.Value == "Add" && hdnSuggestedWindowSlotID.Value != "" && hdnSuggestedWindowSlotID.Value != "-1")
        {
            //Get all reserved window           
            oMASSIT_TimeWindowBE.Action = "ShowAllReserved";
            oMASSIT_TimeWindowBE.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
            oMASSIT_TimeWindowBE.ScheduleDate = LogicalSchedulDateTime;
            oMASSIT_TimeWindowBE.IncludedVendorIDs = Convert.ToString(ucSeacrhVendorID); ;
            oMASSIT_TimeWindowBE.TimeWindowID = Convert.ToInt32(hdnSuggestedWindowSlotID.Value);


            List<MASSIT_TimeWindowBE> lstFixedSlot = oMASSIT_TimeWindowBAL.GetReservedWindowBAL(oMASSIT_TimeWindowBE);

            if (lstFixedSlot != null && lstFixedSlot.Count > 0)
            {

                List<MASSIT_TimeWindowBE> lstBookedVolumePerposed = lstFixedSlot.FindAll(delegate (MASSIT_TimeWindowBE vol)
                {
                    if (vol.MaximumCartons > 0)
                    {
                        if (vol.MaximumCartons >= Convert.ToInt32(TotalCartons))
                        {
                            // We just found fixed slot. Capture it.
                            return true;
                        }
                        else
                        {
                            // Not a fixed slot
                            return false;
                        }
                    }
                    else
                    {
                        return true;
                    }
                });

                if (lstBookedVolumePerposed.Count == 0)
                {
                    isProvisional = true;
                    ProvisionalReason = "RESERVED WINDOW CAPACITY";
                }

                lstBookedVolumePerposed = lstFixedSlot.FindAll(delegate (MASSIT_TimeWindowBE vol)
                {
                    if (vol.MaximumPallets > 0)
                    {
                        if (vol.MaximumPallets >= Convert.ToInt32(TotalPallets))
                        {
                            // We just found fixed slot. Capture it.
                            return true;
                        }
                        else
                        {
                            // Not a fixed slot
                            return false;
                        }
                    }
                    else
                    {
                        return true;
                    }
                });

                if (lstBookedVolumePerposed.Count == 0)
                {
                    isProvisional = true;
                    ProvisionalReason = "RESERVED WINDOW CAPACITY";
                }

                lstBookedVolumePerposed = lstFixedSlot.FindAll(delegate (MASSIT_TimeWindowBE vol)
                {
                    if (vol.MaximumLines > 0)
                    {
                        if (vol.MaximumLines >= Convert.ToInt32(TotalLines))
                        {
                            // We just found fixed slot. Capture it.
                            return true;
                        }
                        else
                        {
                            // Not a fixed slot
                            return false;
                        }
                    }
                    else
                    {
                        return true;
                    }
                });

                if (lstBookedVolumePerposed.Count == 0)
                {
                    isProvisional = true;
                    ProvisionalReason = "RESERVED WINDOW CAPACITY";
                }

            }
        }

        #endregion

        bool isWarning = false;
        bool isSlotFound = false;
        if (
            (hdnSuggestedWindowSlotID.Value == "" || hdnSuggestedWindowSlotID.Value == "-1")
            //(txtSchedulingdate.innerControltxtDate.Value != GetQueryStringValue("Scheduledate").ToString() && hdnCurrentMode.Value == "Edit") || 
            //(hdnPreviousVehicleType.Value != ddlVehicleType.SelectedValue &&  hdnPreviousVehicleType.Value !="")
            )
        {  //Stage 8 Point 19

            //find a specific Window for suggestion
            //List<APPBOK_BookingBE> ObjAPPBOK_BookingBE = null;
            #region Window Slot

            //Find Window for suggestion

            MASSIT_TimeWindowBE objMASSIT_TimeWindowBE = new MASSIT_TimeWindowBE();

            objMASSIT_TimeWindowBE.Action = "GetProposedWindow";
            objMASSIT_TimeWindowBE.ExcludedVendorIDs = Convert.ToString(ucSeacrhVendorID);
            objMASSIT_TimeWindowBE.ExcludedCarrierIDs = Convert.ToString(ddlCarrier.SelectedItem.Value);
            objMASSIT_TimeWindowBE.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
            objMASSIT_TimeWindowBE.ScheduleDate = LogicalSchedulDateTime;
            objMASSIT_TimeWindowBE.MaximumPallets = Convert.ToInt32(TotalPallets);
            objMASSIT_TimeWindowBE.MaximumCartons = Convert.ToInt32(TotalCartons);
            objMASSIT_TimeWindowBE.MaximumLines = Convert.ToInt32(TotalLines);

            //Edit Case
            if ((hdnSuggestedWindowSlotID.Value == "" || hdnSuggestedWindowSlotID.Value == "-1") && hdnBookingRef.Value != "")
            {
                objMASSIT_TimeWindowBE.BookingRef = hdnBookingRef.Value;
            }

            List<MASSIT_TimeWindowBE> lstAvailableSlots = oMASSIT_TimeWindowBAL.GetProposedWindowBAL(objMASSIT_TimeWindowBE);

            string str = "=======================================================================================================================";
            StringBuilder sb = new StringBuilder();
            sb.Append(str+ "\r\n");
            sb.Append("Start Window Slot Trace" +"\r\n");

            sb.Append("Vendor :" + ucSeacrhVendorID + "\r\n");
            sb.Append("Carrier :" + ddlCarrier.SelectedValue + "\r\n");
            sb.Append("UserID :" + Session["UserID"] + "\r\n");
            sb.Append("User :" + Session["UserName"] + "\r\n");


            LogUtility.WriteTrace(sb);

            LoggingWindow("GetProposedWindow", lstAvailableSlots);

            //To check the max time limit
            if (lstAvailableSlots.Count > 0)
            {
                for (int iCount = 0; iCount < lstAvailableSlots.Count; iCount++)
                {
                    if (CheckMaxTimeConstraint(lstAvailableSlots[iCount]))
                    {
                        lstAvailableSlots.Remove(lstAvailableSlots[iCount]);
                    }
                }
            }

            LoggingWindow("CheckMaxTimeConstraint", lstAvailableSlots);

            //To check the Total time limit
            if (lstAvailableSlots.Count > 0)
            {
                for (int iCount = 0; iCount < lstAvailableSlots.Count; iCount++)
                {
                    if (CheckTotalAvailableTimeConstraint(lstAvailableSlots[iCount]))
                    {
                        lstAvailableSlots.Remove(lstAvailableSlots[iCount]);
                    }
                }
            }

            LoggingWindow("CheckTotalAvailableTimeConstraint", lstAvailableSlots);


            //To check the SKU Door No and Door Type
            List<MASSIT_TimeWindowBE> lstSKUAvailableSlots = null;
           
                if (lstAvailableSlots.Count > 0)
                {
                    if (bSKUMatrixDoorFound)
                    {
                        lstSKUAvailableSlots = lstAvailableSlots.Where(WindowList => WindowList.IsCheckSKUTable == true && WindowList.SiteDoorNumberID == iSiteDoorNumberID && WindowList.SiteDoorTypeID == iSKUDoorTypeID).ToList();
                        if (lstSKUAvailableSlots.Count == 0)
                        {
                            lstSKUAvailableSlots = lstAvailableSlots.Where(WindowList => WindowList.SiteDoorNumberID == iSiteDoorNumberID && WindowList.SiteDoorTypeID == iSKUDoorTypeID).ToList();
                            LoggingWindow("IsCheckSKUTableT", lstSKUAvailableSlots);
                        }
                    }
                    else
                    {
                        lstSKUAvailableSlots = lstAvailableSlots.Where(WindowList => WindowList.IsCheckSKUTable == false).ToList();
                        LoggingWindow("IsCheckSKUTableF", lstSKUAvailableSlots);
                    }
                }
            

            //Final Window for suggestion
            MASSIT_TimeWindowBE FinalSuggestedWindow = null;
            //MASSIT_VehicleDoorMatrixBE lstWindowWithVehiclePriority = null;

            if (lstSKUAvailableSlots != null && lstSKUAvailableSlots.Count > 0)
            {
                //Vendor Time Constraint
                if (!string.IsNullOrEmpty(BeforeSlotTimeID))
                {
                    lstSKUAvailableSlots = lstSKUAvailableSlots.Where(WindowList => Convert.ToInt32(WindowList.BeforeOrderBYId) >= Convert.ToInt32(BeforeSlotTimeID)).ToList();
                    LoggingExtra("BeforeSlotTimeID", BeforeSlotTimeID);
                    LoggingWindow("BeforeSlotTimeID", lstSKUAvailableSlots);
                }
                if (!string.IsNullOrEmpty(AfterSlotTimeID))
                {
                    lstSKUAvailableSlots = lstSKUAvailableSlots.Where(WindowList => Convert.ToInt32(WindowList.BeforeOrderBYId) < Convert.ToInt32(AfterSlotTimeID)).ToList();
                    LoggingExtra("AfterSlotTimeID", AfterSlotTimeID);
                    LoggingWindow("AfterSlotTimeID", lstSKUAvailableSlots);
                }

                //----Stage 5 Point 1-----//
                //Carrier Time Constraint//
                if (ddlSite.IsBookingExclusions)
                {   //Only if Booking constraints set to yes 
                    if (!string.IsNullOrEmpty(CarrierBeforeSlotTimeID))
                    {
                        lstSKUAvailableSlots = lstSKUAvailableSlots.Where(WindowList => Convert.ToInt32(WindowList.BeforeOrderBYId) >= Convert.ToInt32(CarrierBeforeSlotTimeID)).ToList();
                        LoggingExtra("CarrierBeforeSlotTimeID", CarrierBeforeSlotTimeID);
                        LoggingWindow("CarrierBeforeSlotTimeID", lstSKUAvailableSlots);
                    }
                    if (!string.IsNullOrEmpty(CarrierAfterSlotTimeID))
                    {
                        lstSKUAvailableSlots = lstSKUAvailableSlots.Where(WindowList => Convert.ToInt32(WindowList.BeforeOrderBYId) < Convert.ToInt32(CarrierAfterSlotTimeID)).ToList();
                        LoggingExtra("CarrierAfterSlotTimeID", CarrierAfterSlotTimeID);
                        LoggingWindow("CarrierAfterSlotTimeID", lstSKUAvailableSlots);
                    }
                }
                //---------------------//

                //Constraint for Vehicle door matrix
                APPSIT_VehicleDoorMatrixBAL oMASSIT_VehicleDoorMatrixBAL = new APPSIT_VehicleDoorMatrixBAL();
                List<MASSIT_VehicleDoorMatrixBE> lstVehicleDoorMatrix = oMASSIT_VehicleDoorMatrixBAL.GetVehicleDoorMatrixBAL(Convert.ToInt32(ddlVehicleType.SelectedItem.Value));
                if (lstVehicleDoorMatrix.Count > 0)
                {
                    lstSKUAvailableSlots = lstSKUAvailableSlots.OrderBy(x => x.Priority).ToList();
                    lstVehicleDoorMatrix = lstVehicleDoorMatrix.OrderBy(x => x.Priority).ToList();

                    LoggingWindow("lstSKUAvailableSlots", lstSKUAvailableSlots);

                    LoggingVehicle("lstVehicleDoorMatrix", lstVehicleDoorMatrix);

                    for (int i = 0; i < lstSKUAvailableSlots.Count; i++)
                    {
                        for (int j = 0; j < lstVehicleDoorMatrix.Count; j++)
                        {
                            if (lstSKUAvailableSlots[i].SiteDoorTypeID == lstVehicleDoorMatrix[j].DoorTypeID)
                            {
                                FinalSuggestedWindow = lstSKUAvailableSlots[i];
                                isSlotFound = true;
                                break;
                            }
                        }
                        if (isSlotFound)
                            break;
                    }
                }
            }

            if (FinalSuggestedWindow != null)
            {
                var lstFinal = new List<MASSIT_TimeWindowBE>();
                lstFinal.Add(FinalSuggestedWindow);
                LoggingWindow("FinalSuggestedWindow", lstFinal);
            }

            sb = new StringBuilder();
            //string str = "=======================================================================================================================";
            sb.Append(str + "\r\n");
            LogUtility.WriteTrace(sb);

            bool isSameSuggestion = false;
            if (hdnCurrentMode.Value == "Edit" && FinalSuggestedWindow != null)
            {
                if (hdnSuggestedSiteDoorNumberID.Value != FinalSuggestedWindow.SiteDoorNumberID.ToString())
                {
                    isProvisional = true;
                    ProvisionalReason = "WINDOW CAPACITY";
                }
                else
                {
                    isSameSuggestion = true;
                }
            }

            if (hdnCurrentMode.Value == "Edit" && FinalSuggestedWindow == null)
            {
                if (hdnCurrentRole.Value == "Vendor")
                {
                    isProvisional = true;
                    ProvisionalReason = "WINDOW CAPACITY";
                }
            }

            if (hdnCurrentMode.Value == "Add" || isSameSuggestion == true)
            {
                if (FinalSuggestedWindow != null)
                {
                    ltBookingDate.Text = FinalSuggestedWindow.StartTime + " - " + FinalSuggestedWindow.EndTime + " on " + LogicalSchedulDateTime.Day.ToString() + "/" + LogicalSchedulDateTime.Month.ToString() + "/" + LogicalSchedulDateTime.Year.ToString();
                    ltSuggestedSlotTime.Text = AtDoorName + " " + FinalSuggestedWindow.DoorNumber.ToString();
                    hdnPreservSlot.Value = FinalSuggestedWindow.StartSlotTimeID + "|" + FinalSuggestedWindow.EndSlotTimeID + "|" + FinalSuggestedWindow.SiteDoorNumberID.ToString();
                    hdnSuggestedWindowSlotID.Value = FinalSuggestedWindow.TimeWindowID.ToString();
                    hdnSuggestedSiteDoorNumberID.Value = FinalSuggestedWindow.SiteDoorNumberID.ToString();
                    hdnSuggestedSiteDoorNumber.Value = FinalSuggestedWindow.DoorNumber.ToString();
                    hdnSuggestedWeekDay.Value = LogicalSchedulDateTime.ToString("ddd");
                }
                else
                {
                    hdnPreservSlot.Value = string.Empty;
                    btnConfirmBooking.Visible = false;
                    lblProposeWindowBookingInfo.Visible = false;
                    btnWindowAlternateSlotVendor.Visible = false;
                }
            }

            if (isSlotFound == false)
            {
                if (txtSchedulingdate.innerControltxtDate.Value == GetQueryStringValue("Scheduledate").ToString() && hdnPreviousVehicleType.Value != ddlVehicleType.SelectedValue && isDoorTypeNotChanged == false && hdnPreviousVehicleType.Value != "" && hdnCurrentMode.Value == "Edit")
                {
                    if (Session["Role"].ToString().ToLower() == "vendor" || Session["Role"].ToString().ToLower() == "carrier")
                    {
                        mdlNoSlotFound.Show();
                        isDoorCheckPopup = true;
                        return;
                    }
                    else
                    {
                        hdnPreservSlot.Value = string.Empty;
                        btnConfirmBooking.Visible = false;
                        lblProposeWindowBookingInfo.Visible = false;
                        btnWindowAlternateSlotVendor.Visible = false;
                        ltBookingDate.Visible = false;
                        ltSuggestedSlotTime.Visible = false;
                        isDoorCheckPopup = true;
                        string errMsg = WebCommon.getGlobalResourceValue("NoSlotFound");
                        AddWarningMessages(errMsg, "NoSlotFound");
                        ShowWarningMessages();
                        return;
                    }
                }
            }
            else
            {

                if ((txtSchedulingdate.innerControltxtDate.Value != GetQueryStringValue("Scheduledate").ToString() && hdnCurrentMode.Value == "Edit")
                    || (txtSchedulingdate.innerControltxtDate.Value == GetQueryStringValue("Scheduledate").ToString() && hdnPreviousVehicleType.Value != ddlVehicleType.SelectedValue && isDoorTypeNotChanged == false && hdnCurrentMode.Value == "Edit")
                    )
                {
                    ltBookingDate.Text = FinalSuggestedWindow.StartTime + " - " + FinalSuggestedWindow.EndTime + " on " + LogicalSchedulDateTime.Day.ToString() + "/" + LogicalSchedulDateTime.Month.ToString() + "/" + LogicalSchedulDateTime.Year.ToString();
                    ltSuggestedSlotTime.Text = AtDoorName + " " + FinalSuggestedWindow.DoorNumber.ToString();
                    hdnPreservSlot.Value = FinalSuggestedWindow.StartSlotTimeID + "|" + FinalSuggestedWindow.EndSlotTimeID + "|" + FinalSuggestedWindow.SiteDoorNumberID.ToString();
                    hdnSuggestedWindowSlotID.Value = FinalSuggestedWindow.TimeWindowID.ToString();
                    hdnSuggestedSiteDoorNumberID.Value = FinalSuggestedWindow.SiteDoorNumberID.ToString();
                    hdnSuggestedSiteDoorNumber.Value = FinalSuggestedWindow.DoorNumber.ToString();
                    hdnSuggestedWeekDay.Value = LogicalSchedulDateTime.ToString("ddd");
                }
            }
            #endregion

            #region Commented
            ////for (int iCount = 0; iCount < lstAvailableSlots.Count; iCount++)
            ////{
            ////    ObjAPPBOK_BookingBE = lstAvailableFixedDoorSlots.FindAll(delegate(APPBOK_BookingBE p) { return p.FixedSlot.FixedSlotID == lstAvailableSlots[iCount].FixedSlot.FixedSlotID; });
            ////    if (ObjAPPBOK_BookingBE.Count > 0)
            ////    {

            ////        //Check delivery constraints                    
            ////        bool isValidTimeSlot = CheckValidSlot(ObjAPPBOK_BookingBE[0].SlotTime.SlotTime, ObjAPPBOK_BookingBE[0].ScheduleDate.Value.ToString("ddd"), TotalPallets, TotalLines);

            ////        //if (bUnconfirmedFixedSlotFound)
            ////        //    isValidTimeSlot = true;

            ////        if (isValidTimeSlot == false)
            ////        {
            ////            continue;
            ////        }
            ////        else
            ////        {
            ////            isWindowSlotFind = true;
            ////            ltBookingDate.Text = ObjAPPBOK_BookingBE[0].ScheduleDate.Value.Day.ToString() + "/" + ObjAPPBOK_BookingBE[0].ScheduleDate.Value.Month.ToString() + "/" + ObjAPPBOK_BookingBE[0].ScheduleDate.Value.Year.ToString();
            ////            ltSuggestedSlotTime.Text = ObjAPPBOK_BookingBE[0].SlotTime.SlotTime + " " + AtDoorName + " " + ObjAPPBOK_BookingBE[0].DoorNoSetup.DoorNumber.ToString();

            ////            hdnSuggestedSlotTimeID.Value = ObjAPPBOK_BookingBE[0].SlotTime.SlotTimeID.ToString();
            ////            hdnSuggestedWindowSlotID.Value = ObjAPPBOK_BookingBE[0].FixedSlot.FixedSlotID.ToString();
            ////            hdnSuggestedSiteDoorNumberID.Value = ObjAPPBOK_BookingBE[0].DoorNoSetup.SiteDoorNumberID.ToString();
            ////            hdnSuggestedSiteDoorNumber.Value = ObjAPPBOK_BookingBE[0].DoorNoSetup.DoorNumber.ToString();
            ////            hdnSuggestedSlotTime.Value = ObjAPPBOK_BookingBE[0].SlotTime.SlotTime;
            ////            //ltTimeSlot.Text = hdnSuggestedSlotTime.Value;
            ////            //ltDoorNo.Text = hdnSuggestedSiteDoorNumber.Value;
            ////            //ltTimeSlot_1.Text = ObjAPPBOK_BookingBE[0].SlotTime.SlotTime;
            ////            //ltDoorNo_1.Text = hdnSuggestedSiteDoorNumber.Value;
            ////            hdnSuggestedWeekDay.Value = ObjAPPBOK_BookingBE[0].ScheduleDate.Value.ToString("ddd");

            ////            //Check Volume Details//
            ////            int fixedslotlength = GetRequiredTimeSlotLength(ObjAPPBOK_BookingBE[0].FixedSlot.MaximumPallets, ObjAPPBOK_BookingBE[0].FixedSlot.MaximumCatrons);
            ////            int BookingLength = GetRequiredTimeSlotLength(Convert.ToInt32(TotalPallets), Convert.ToInt32(TotalCartons));
            ////            if (fixedslotlength < BookingLength)
            ////            {
            ////                isProvisional = true;
            ////            }
            ////            else
            ////            {

            ////                if (lstAvailableFixedDoorSlots.FindAll(delegate(APPBOK_BookingBE St) { return St.FixedSlot.FixedSlotID == Convert.ToInt32(ObjAPPBOK_BookingBE[0].FixedSlot.FixedSlotID) && St.VehicleType.VehicleTypeID == Convert.ToInt32(ddlVehicleType.SelectedItem.Value); }).Count <= 0)
            ////                {
            ////                    isProvisional = true;
            ////                }

            ////                //if (ObjAPPBOK_BookingBE[0].VehicleType.VehicleTypeID != Convert.ToInt32(ddlVehicleType.SelectedItem.Value))
            ////                //    isProvisional = true;  
            ////            }
            ////            //-------------------//

            ////            break;
            ////        }
            ////    }
            ////}

            //if (ObjAPPBOK_BookingBE == null || ObjAPPBOK_BookingBE.Count == 0) {  //No Fixed Slot find for suggestion,find alternate slot
            //if (isFixedSlotFind == false)
            //{     //No Fixed Slot find for suggestion,find alternate slot
            //    btnConfirmBooking.Visible = false;
            //    lblProposeWindowBookingInfo.Visible = false;

            //    //Find Slot for this booking 
            //    GenerateAlternateSlot(true, iSiteDoorNumberID, iSKUDoorTypeID);


            //    //tableDiv_General.Controls.RemoveAt(0);
            //}
            #endregion

            //---------start Stage 9 Point 2----------------------//

            int intCapacityAvailable = 0;
            //if (hdnSuggestedWindowSlotID.Value == string.Empty || hdnSuggestedWindowSlotID.Value == "-1") {
            if (isSlotFound == false)
            {
                if (TotalPallets != "0" && hdnRemainingPallets.Value != string.Empty && hdnRemainingPallets.Value != "-")
                {
                    if (Convert.ToInt32(hdnRemainingPallets.Value) > 0)
                    {
                        intCapacityAvailable++;
                    }
                }

                if (TotalLines != "0" && hdnRemainingLines.Value != string.Empty && hdnRemainingLines.Value != "-")
                {
                    if (Convert.ToInt32(hdnRemainingLines.Value) > 0)
                    {
                        intCapacityAvailable++;
                    }
                }
            }


            if (intCapacityAvailable == 2)
            {

                if (hdnCurrentRole.Value == "Vendor")
                {
                    btnConfirmBooking.Visible = false;
                    isWarning = true;
                }
            }
            else
            {  //---------End Stage 9 Point 2----------------------//
                //No space remaining on compatable door  -- show advice
                if (hdnCurrentMode.Value == "Add")
                {
                    if (hdnSuggestedWindowSlotID.Value == string.Empty || hdnSuggestedWindowSlotID.Value == "-1")
                    {
                        string errMsg = WebCommon.getGlobalResourceValue("BK_MS_28_EXT"); // "No space available on any compatible doors.Please contact Goods In office or select a different date.";

                        if (hdnCurrentRole.Value == "Vendor")
                        {
                            //Show err msg
                            ShowErrorMessage(errMsg, "BK_MS_28");
                        }
                        else
                        {
                            //Show err msg
                            errMsg = WebCommon.getGlobalResourceValue("BK_MS_28_INT"); //"No space available on any compatible doors.";
                            ShowErrorMessage(errMsg, "BK_MS_28");
                        }
                    }
                }
            }
        }


        //---------------------To Fix the follwoing issue--------------//
        //1. OD makes booking on a date which is already over capacity for lines
        //2. Vendor then edits this booking and raises the lines significantly
        //3. System allows this, booking does not go Provisional

        if (hdnCurrentMode.Value == "Edit" && isWarning == false)
        {
            int intCapacityAvailableDaily = 0;

            if (isSlotFound == false)
            {
                if (TotalPallets != "0" && RemainingPallets != (int?)null)
                {
                    if (Convert.ToInt32(TotalPallets) > RemainingPallets + BookedPallets)
                    {
                        intCapacityAvailableDaily++;
                    }
                }

                if (TotalLines != "0" && RemainingLines != (int?)null)
                {
                    if (Convert.ToInt32(TotalLines) > RemainingLines + BookedLines)
                    {
                        intCapacityAvailableDaily++;
                    }
                }
                if (intCapacityAvailableDaily > 0)
                {
                    if (hdnCurrentRole.Value == "Vendor")
                    {
                        //btnConfirmBooking.Visible = false;
                        //isWarning = true;
                        isProvisional = true;
                        ProvisionalReason = "EDITED";
                    }
                }
            }
        }
        //-----------------------------------------------------------------------//


        //---------start Stage 9 Point 2----------------------//
        if (isWarning)
            ShowProvisionalWarning();
        //---------End Stage 9 Point 2----------------------//

        #region Commented
        //if (hdnCurrentMode.Value == "Add") {
        //    foreach (GridViewRow gvRow in gvBookingSlots.Rows) {
        //        if (gvRow.RowType == DataControlRowType.DataRow) {
        //            HiddenField hdnFixedSlotID = (HiddenField)gvRow.FindControl("hdnAvailableFixedSlotID");
        //            if (hdnFixedSlotID != null && hdnFixedSlotID.Value == hdnSuggestedWindowSlotID.Value && hdnSuggestedWindowSlotID.Value != "-1") {
        //                gvRow.Cells[6].Text = "This Booking";
        //                gvRow.BackColor = Color.GreenYellow;
        //                gvRow.ForeColor = Color.Black;
        //                break;
        //            }
        //        }
        //    }
        //}
        //else if (hdnCurrentMode.Value == "Edit") {

        //    bool isSlotFound = false;
        //    foreach (GridViewRow gvRow in gvBookingSlots.Rows) {
        //        if (gvRow.RowType == DataControlRowType.DataRow) {
        //            if (gvRow.Cells[3].Text.Trim() == hdnBookingRef.Value) {
        //                gvRow.Cells[6].Text = "This Booking";
        //                gvRow.BackColor = Color.GreenYellow;
        //                gvRow.ForeColor = Color.Black;
        //                isSlotFound = true;
        //                break;
        //            }
        //        }
        //    }

        //    if (isSlotFound == false) {
        //        foreach (GridViewRow gvRow in gvBookingSlots.Rows) {
        //            if (gvRow.RowType == DataControlRowType.DataRow) {
        //                HiddenField hdnFixedSlotID = (HiddenField)gvRow.FindControl("hdnAvailableFixedSlotID");
        //                if (hdnFixedSlotID != null && hdnFixedSlotID.Value == hdnSuggestedWindowSlotID.Value && hdnSuggestedWindowSlotID.Value != "-1") {
        //                    gvRow.Cells[6].Text = "This Booking";
        //                    gvRow.BackColor = Color.GreenYellow;
        //                    gvRow.ForeColor = Color.Black;
        //                    break;
        //                }
        //            }
        //        }
        //    }
        //}
        #endregion
    }

    //---------start Stage 9 Point 2----------------------//
    private void ShowProvisionalWarning()
    {
        mdlProConfirmMsg.Show();
        btnConfirmBooking.Visible = false;
    }

    protected void btnProContinue_Click(object sender, EventArgs e)
    {
        isProvisional = true;
        ProvisionalReason = "NO SPACE";
        MakeBooking(BookingType.ProvisionalNonTimedDelivery.ToString());
    }

    protected void btnProBack_Click(object sender, EventArgs e)
    {
        mdlProConfirmMsg.Hide();
        EncryptQueryString("APPBok_BookingOverview.aspx");
    }
    //---------End Stage 9 Point 2----------------------//


    //Check delivery constraints
    //private bool CheckValidSlot(string strStartTime, string strStartWeekDay, string strTotalPallets, string strTotalLines)
    //{   //Check delivery constraints

    //    if (string.IsNullOrEmpty(strTotalPallets))
    //        strTotalPallets = "0";
    //    if (string.IsNullOrEmpty(strTotalLines))
    //        strTotalLines = "0";

    //    APPSIT_DeliveryConstraintSpecificBAL oAPPSIT_DeliveryConstraintSpecificBAL = new APPSIT_DeliveryConstraintSpecificBAL();
    //    MASSIT_DeliveryConstraintSpecificBE oMASSIT_DeliveryConstraintSpecificBE = new MASSIT_DeliveryConstraintSpecificBE();


    //    DateTime dtSelectedDate = LogicalSchedulDateTime;
    //    string WeekDay = dtSelectedDate.ToString("ddd");

    //    oMASSIT_DeliveryConstraintSpecificBE.Action = "GetDeliveryConstraintForBooking";
    //    oMASSIT_DeliveryConstraintSpecificBE.DeliveryConstraint = new MASSIT_DeliveryConstraintBE();
    //    oMASSIT_DeliveryConstraintSpecificBE.DeliveryConstraint.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
    //    oMASSIT_DeliveryConstraintSpecificBE.DeliveryConstraint.Weekday = WeekDay;
    //    oMASSIT_DeliveryConstraintSpecificBE.DeliveryConstraint.StartTime = Convert.ToInt32(strStartTime.Split(':')[0]);
    //    oMASSIT_DeliveryConstraintSpecificBE.WeekStartDate = dtSelectedDate;
    //    oMASSIT_DeliveryConstraintSpecificBE.DeliveryConstraint.StartWeekday = strStartWeekDay;

    //    List<MASSIT_DeliveryConstraintSpecificBE> lstDeliveryConstraintSpecific = oAPPSIT_DeliveryConstraintSpecificBAL.GetDeliveryConstraintSpecificBAL(oMASSIT_DeliveryConstraintSpecificBE);

    //    if (lstDeliveryConstraintSpecific != null && lstDeliveryConstraintSpecific.Count > 0)
    //    {
    //        //Get Booked Volume
    //        APPBOK_BookingBE oAPPBOK_BookingBE = new APPBOK_BookingBE();
    //        APPBOK_BookingBAL oAPPBOK_BookingBAL = new APPBOK_BookingBAL();

    //        oAPPBOK_BookingBE.Action = "GetBookingVolumeDeatils";
    //        oAPPBOK_BookingBE.SiteId = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
    //        oAPPBOK_BookingBE.ScheduleDate = LogicalSchedulDateTime;

    //        string strWeekDay = LogicalSchedulDateTime.DayOfWeek.ToString().ToUpper();
    //        if (strWeekDay == "SUNDAY")
    //        {
    //            oAPPBOK_BookingBE.WeekDay = 1;
    //        }
    //        else if (strWeekDay == "MONDAY")
    //        {
    //            oAPPBOK_BookingBE.WeekDay = 2;
    //        }
    //        else if (strWeekDay == "TUESDAY")
    //        {
    //            oAPPBOK_BookingBE.WeekDay = 3;
    //        }
    //        else if (strWeekDay == "WEDNESDAY")
    //        {
    //            oAPPBOK_BookingBE.WeekDay = 4;
    //        }
    //        else if (strWeekDay == "THURSDAY")
    //        {
    //            oAPPBOK_BookingBE.WeekDay = 5;
    //        }
    //        else if (strWeekDay == "FRIDAY")
    //        {
    //            oAPPBOK_BookingBE.WeekDay = 6;
    //        }
    //        else if (strWeekDay == "SATURDAY")
    //        {
    //            oAPPBOK_BookingBE.WeekDay = 7;
    //        }
    //        oAPPBOK_BookingBE.StartTime = strStartTime.Split(':')[0] + ":00";

    //        List<APPBOK_BookingBE> lstBookedVolume = oAPPBOK_BookingBAL.GetBookingVolumeDeatilsBAL(oAPPBOK_BookingBE);

    //        if (lstBookedVolume != null && lstBookedVolume.Count > 0)
    //        {
    //            if (!((lstDeliveryConstraintSpecific[0].DeliveryConstraint.RestrictDelivery == 0) ||
    //                (lstBookedVolume[0].BookingCount + 1 <= lstDeliveryConstraintSpecific[0].DeliveryConstraint.RestrictDelivery)))
    //                return false;

    //            if (!((lstDeliveryConstraintSpecific[0].DeliveryConstraint.MaximumLine == 0) ||
    //                (lstBookedVolume[0].NumberOfLines + Convert.ToInt32(strTotalLines) <= lstDeliveryConstraintSpecific[0].DeliveryConstraint.MaximumLine)))
    //                return false;

    //            if (!((lstDeliveryConstraintSpecific[0].DeliveryConstraint.MaximumLift == 0) ||
    //                (lstBookedVolume[0].NumberOfPallet + Convert.ToInt32(strTotalPallets) <= lstDeliveryConstraintSpecific[0].DeliveryConstraint.MaximumLift))) //Here Lift means pallets
    //                return false;

    //            if (!((lstDeliveryConstraintSpecific[0].DeliveryConstraint.SingleDeliveryPalletRestriction == 0) ||
    //                (Convert.ToInt32(strTotalPallets) <= lstDeliveryConstraintSpecific[0].DeliveryConstraint.SingleDeliveryPalletRestriction)))
    //                return false;

    //            if (!((lstDeliveryConstraintSpecific[0].DeliveryConstraint.SingleDeliveryLineRestriction == 0) ||
    //                (Convert.ToInt32(strTotalLines) <= lstDeliveryConstraintSpecific[0].DeliveryConstraint.SingleDeliveryLineRestriction)))
    //                return false;
    //        }
    //        return true;
    //    }
    //    return true;
    //}



    //protected void gvBookingSlots_RowDataBound(object sender, GridViewRowEventArgs e)
    //{

    //    if (e.Row.RowType == DataControlRowType.DataRow)
    //    {
    //        if (e.Row.Cells[6].Text == "Booked")
    //        {
    //            e.Row.BackColor = Color.Purple;
    //            e.Row.ForeColor = Color.White;
    //        }
    //    }
    //}

    protected void btnConfirmBooking_Click(object sender, EventArgs e)
    {
        ConfirmWindowBooking();
    }

    protected void btnNotHappy_Click(object sender, EventArgs e)
    {
        EncryptQueryString("APPBok_BookingOverview.aspx");
    }

    protected void btnConfirmBookingWithMail_Click(object sender, EventArgs e)
    {
        IsBookingEditWithMail = "true";
        ConfirmAlternateSlot();
    }

    protected void btnConfirmBookingWithoutMail_Click(object sender, EventArgs e)
    {
        IsBookingEditWithMail = "false";
        ConfirmAlternateSlot();
    }

    private void ConfirmWindowBooking()
    {

        string errMsg = string.Empty;
        bool isWarning = false;

        if ((isProvisional) || (hdnCapacityWarning.Value == "true" && hdnSuggestedWindowSlotID.Value != "-1" && hdnSuggestedWindowSlotID.Value != string.Empty))
        {
            if (hdnCurrentRole.Value == "Vendor" || hdnCurrentRole.Value == "Carrier")
            {
                errMsg = WebCommon.getGlobalResourceValue("BK_MS_23_EXT");// "The volume being booked in exceeds the scheduled capacity for your slot - This delivery has been provisionally accepted. Office Depot's Goods In team will review and will contact you if we need to reschedule this booking.";
                AddWarningMessages(errMsg, "BK_MS_23_EXT");
            }
            else
            {
                errMsg = WebCommon.getGlobalResourceValue("BK_MS_23_INT");// "The volume being booked in exceeds the scheduled capacity for this fixed slot. Click Continue to confirm booking or click Back to select a different slot.";
                AddWarningMessages(errMsg, "BK_MS_23_INT");
            }
            isWarning = true;
        }

        if (isWarning == false)
        {
            if (hdnSuggestedWindowSlotID.Value != "-1" && hdnSuggestedWindowSlotID.Value != string.Empty)
            {
                MakeBooking(BookingType.WindowSlot.ToString());
            }
            else if (rdoNonStandardWindow.Checked || Convert.ToBoolean(IsNonStandardWindowSlot))
            {
                MakeBooking(BookingType.NonStandardWindowSlot.ToString());
            }
            else
            {
                //-----------Start Stage 9 point 2a/2b--------------//
                if (IsProvisionalBooking.Value == true && hdnCurrentMode.Value == "Edit")
                {
                    if (hdnSuggestedWindowSlotID.Value == "-1" || hdnSuggestedWindowSlotID.Value == string.Empty || hdnSuggestedWindowSlotID.Value == "0")
                    {
                        string errorMeesage = WebCommon.getGlobalResourceValue("confirmprovisionalbooking");
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert2", "alert('" + errorMeesage + "')", true);
                        return;
                    }
                }
                //-----------End Stage 9 point 2a/2b--------------//
            }
        }
        else
        {
            ShowWarningMessages();
        }
        //else
        //{
        //    //if (IsNonTimeBooking == true)
        //    //    MakeBooking(BookingType.NonTimedDelivery.ToString());
        //    //else
        //    MakeBooking(BookingType.BookedSlot.ToString());
        //}
        //}
        //else
        //    ShowWarningMessages();
    }


    //Write for windowslot for booking using querystring by shrish...
    private void MakeBooking(string ThisBookingType)
    {
        try
        {



            if (Session["BookingCreated"] != null && Convert.ToBoolean(Session["BookingCreated"]) == true)
            {
                EncryptQueryString("APPBok_Confirm.aspx?MailSend=False&BookingID=" + Session["CreatedBookingID"].ToString() + "&IsBookingAmended=" + Session["IsBookingAmended"].ToString());
                return;
            }

            //-----------Start Stage 9 point 2a/2b--------------//
            if (ThisBookingType != BookingType.ProvisionalNonTimedDelivery.ToString())
            {
                //-----------End Stage 9 point 2a/2b--------------//

                if (ThisBookingType != BookingType.NonStandardWindowSlot.ToString())
                {
                    if (hdnSuggestedWindowSlotID.Value == "-1" || hdnSuggestedWindowSlotID.Value == string.Empty)
                        return;
                }

                //if (ThisBookingType == BookingType.NonTimedDelivery.ToString())
                //{
                //    isProvisional = false;
                //}

                if (isProvisional)
                {
                    if (hdnCurrentRole.Value == "Vendor" || hdnCurrentRole.Value == "Carrier")
                    {
                        ThisBookingType = BookingType.Provisional.ToString();
                    }
                }
            }

            APPBOK_BookingBE oAPPBOK_BookingBE = new APPBOK_BookingBE();
            APPBOK_BookingBAL oAPPBOK_BookingBAL = new APPBOK_BookingBAL();

            ////-------Check booking is made for same time slot -----//
            //if (ThisBookingType != BookingType.NonTimedDelivery.ToString())
            //{
            //    oAPPBOK_BookingBE.SlotTime = new SYS_SlotTimeBE();
            //    oAPPBOK_BookingBE.DoorNoSetup = new MASSIT_DoorNoSetupBE();

            //    oAPPBOK_BookingBE.Action = "CheckBooking";
            //    oAPPBOK_BookingBE.SiteId = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
            //    oAPPBOK_BookingBE.ScheduleDate = LogicalSchedulDateTime; //ActualSchedulDateTime; // 
            //    //oAPPBOK_BookingBE.SlotTime.SlotTimeID = Convert.ToInt32(hdnSuggestedSlotTimeID.Value);
            //    oAPPBOK_BookingBE.TimeWindow.TimeWindowID = Convert.ToInt32(hdnSuggestedWindowSlotID.Value);
            //    oAPPBOK_BookingBE.DoorNoSetup.SiteDoorNumberID = Convert.ToInt32(hdnSuggestedSiteDoorNumberID.Value);
            //    if (hdnCurrentMode.Value == "Edit")
            //    {
            //        oAPPBOK_BookingBE.BookingID = Convert.ToInt32(GetQueryStringValue("ID").ToString());
            //    }

            //    int? iBookingCount = oAPPBOK_BookingBAL.CheckBookingBAL(oAPPBOK_BookingBE);

            //    if (Convert.ToInt32(iBookingCount) > 0)
            //    {
            //        string errMsg = WebCommon.getGlobalResourceValue("BK_MS_20"); // "This slot is no longer available,please select another slot.";
            //        ShowErrorMessage(errMsg, "BK_MS_20");
            //        return;
            //    }
            //}
            //----------------------------------------------------//


            oAPPBOK_BookingBE = new APPBOK_BookingBE();
            oAPPBOK_BookingBAL = new APPBOK_BookingBAL();

            oAPPBOK_BookingBE.VehicleType = new MASSIT_VehicleTypeBE();
            oAPPBOK_BookingBE.Delivery = new MASCNT_DeliveryTypeBE();
            oAPPBOK_BookingBE.Carrier = new MASCNT_CarrierBE();
            oAPPBOK_BookingBE.SlotTime = new SYS_SlotTimeBE();
            oAPPBOK_BookingBE.DoorNoSetup = new MASSIT_DoorNoSetupBE();
            oAPPBOK_BookingBE.FixedSlot = new MASSIT_FixedSlotBE();
            oAPPBOK_BookingBE.TimeWindow = new MASSIT_TimeWindowBE();

            if (hdnCurrentMode.Value == "Add")
            {
                oAPPBOK_BookingBE.Action = "AddVendorBooking";
                oAPPBOK_BookingBE.IsBookingAmended = false;
                if (hdnCurrentRole.Value == "Vendor")
                    oAPPBOK_BookingBE.IsOnlineBooking = true;
                else
                    oAPPBOK_BookingBE.IsOnlineBooking = false;
            }
            else if (hdnCurrentMode.Value == "Edit")
            {
                oAPPBOK_BookingBE.Action = "EditVendorBooking";
                oAPPBOK_BookingBE.BookingID = Convert.ToInt32(GetQueryStringValue("ID").ToString());
                oAPPBOK_BookingBE.BookingRef = hdnBookingRef.Value;
                oAPPBOK_BookingBE.IsBookingAmended = true;
                if (Session["Role"].ToString().ToLower() != "vendor")
                {
                    oAPPBOK_BookingBE.IsBookingAccepted = true;
                }
            }

            oAPPBOK_BookingBE.SiteId = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
            oAPPBOK_BookingBE.SupplierType = "V";
            oAPPBOK_BookingBE.Delivery.DeliveryTypeID = Convert.ToInt32(ddlDeliveryType.SelectedItem.Value);
            oAPPBOK_BookingBE.ScheduleDate = LogicalSchedulDateTime;
            oAPPBOK_BookingBE.Carrier.CarrierID = Convert.ToInt32(ddlCarrier.SelectedItem.Value);
            oAPPBOK_BookingBE.OtherCarrier = txtCarrierOther.Text.Trim();
            oAPPBOK_BookingBE.VehicleType.VehicleTypeID = Convert.ToInt32(ddlVehicleType.SelectedItem.Value);
            oAPPBOK_BookingBE.OtherVehicle = txtVehicleOther.Text.Trim();
            if (IsPreAdviseNoteRequired == true)
                oAPPBOK_BookingBE.PreAdviseNotification = rdoAdvice1.Checked ? "Y" : "N";

            if (ThisBookingType == BookingType.WindowSlot.ToString())
            {
                //oAPPBOK_BookingBE.SlotTime.SlotTimeID = Convert.ToInt32(hdnSuggestedSlotTimeID.Value);
                oAPPBOK_BookingBE.TimeWindow.TimeWindowID = Convert.ToInt32(hdnSuggestedWindowSlotID.Value);
                oAPPBOK_BookingBE.DoorNoSetup.SiteDoorNumberID = Convert.ToInt32(hdnSuggestedSiteDoorNumberID.Value);
                oAPPBOK_BookingBE.DoorNoSetup.DoorNumber = hdnSuggestedSiteDoorNumber.Value;
                oAPPBOK_BookingBE.BookingStatusID = 1;  //Confirmed Slot
                oAPPBOK_BookingBE.BookingTypeID = 6;    //Window Slot
                //oAPPBOK_BookingBE.FixedSlot.FixedSlotID = Convert.ToInt32(hdnSuggestedWindowSlotID.Value);
            }
            else if (ThisBookingType == BookingType.Provisional.ToString())
            {
                //oAPPBOK_BookingBE.SlotTime.SlotTimeID = Convert.ToInt32(hdnSuggestedSlotTimeID.Value);
                oAPPBOK_BookingBE.TimeWindow.TimeWindowID = Convert.ToInt32(hdnSuggestedWindowSlotID.Value);
                oAPPBOK_BookingBE.DoorNoSetup.SiteDoorNumberID = Convert.ToInt32(hdnSuggestedSiteDoorNumberID.Value);
                oAPPBOK_BookingBE.DoorNoSetup.DoorNumber = hdnSuggestedSiteDoorNumber.Value;
                oAPPBOK_BookingBE.BookingStatusID = 1;  //Confirmed Slot
                oAPPBOK_BookingBE.BookingTypeID = 5;    //Provisional Booking
                //oAPPBOK_BookingBE.FixedSlot.FixedSlotID = Convert.ToInt32(hdnSuggestedWindowSlotID.Value);
                //Stage 4  Point 5
                oAPPBOK_BookingBE.ProvisionalReason = ProvisionalReason;
                //-----------
            }
            else if (ThisBookingType == BookingType.NonStandardWindowSlot.ToString())
            {
                oAPPBOK_BookingBE.DoorNoSetup.SiteDoorNumberID = Convert.ToInt32(hdnSuggestedSiteDoorNumberID.Value);
                oAPPBOK_BookingBE.DoorNoSetup.DoorNumber = hdnSuggestedSiteDoorNumber.Value;
                oAPPBOK_BookingBE.NonWindowFromTimeID = Convert.ToInt32(hdnStartTimeID.Value);
                oAPPBOK_BookingBE.NonWindowToTimeID = Convert.ToInt32(hdnEndTimeID.Value);
                oAPPBOK_BookingBE.BookingStatusID = 1;   //Confirmed Slot
                oAPPBOK_BookingBE.BookingTypeID = 7;     //Non Standard Window
            }
            //----------Stage 9 Point 2--------------------//
            else if (ThisBookingType == BookingType.ProvisionalNonTimedDelivery.ToString())
            {
                oAPPBOK_BookingBE.DoorNoSetup.DoorNumber = string.Empty; //0;
                oAPPBOK_BookingBE.BookingStatusID = 1;  //Confirmed Slot
                oAPPBOK_BookingBE.BookingTypeID = 5;    //Provisional Booking
                oAPPBOK_BookingBE.ProvisionalReason = ProvisionalReason;
                hdnSuggestedWeekDay.Value = LogicalSchedulDateTime.DayOfWeek.ToString().ToLower().Substring(0, 3);//weekday
            }
            //--------------------------------------------//
            // Stage 15 R2 Point No 17

            if (hdnCurrentMode.Value == "Edit" && Session["Role"].ToString().ToLower() == "vendor")
            {
                if (hdnProvisionalReason.Value == "RESERVED WINDOW CAPACITY" && hdnBookingTypeID.Value == "5")
                {
                    if (hdnIsBookingAccepted.Value == "true")
                    {
                        oAPPBOK_BookingBE.ProvisionalReason = "EDITED";
                    }
                    else
                    {
                        oAPPBOK_BookingBE.ProvisionalReason = "RESERVED WINDOW CAPACITY";
                    }

                }
                else if (!string.IsNullOrEmpty(hdnSuggestedWindowSlotID.Value) && (ThisBookingType == BookingType.Provisional.ToString() ||
                         ThisBookingType == BookingType.ProvisionalNonTimedDelivery.ToString()))
                {
                    oAPPBOK_BookingBE.ProvisionalReason = "EDITED";
                }
            }

            //---------------------------------------------//
            oAPPBOK_BookingBE.UserID = Convert.ToInt32(Session["UserID"]);
            oAPPBOK_BookingBE.NumberOfCartons = txtCartons.Text.Trim() != string.Empty ? Convert.ToInt32(txtCartons.Text.Trim()) : (int?)null;
            oAPPBOK_BookingBE.NumberOfLift = txtLifts.Text.Trim() != string.Empty ? Convert.ToInt32(txtLifts.Text.Trim()) : (int?)null;
            oAPPBOK_BookingBE.NumberOfPallet = txtPallets.Text.Trim() != string.Empty ? Convert.ToInt32(txtPallets.Text.Trim()) : (int?)null;
            oAPPBOK_BookingBE.NumberOfLines = txtExpectedLines.Text.Trim() != string.Empty ? Convert.ToInt32(txtExpectedLines.Text.Trim()) : (int?)null;
            oAPPBOK_BookingBE.WeekDay = getWeekdayNo();

            //PO Details
            oAPPBOK_BookingBE.VendorID = Convert.ToInt32(ucSeacrhVendorID); // Convert.ToInt32(ucSeacrhVendor1.VendorNo);

            DataTable myDataTable = null;
            if (ViewState["myDataTable"] != null)
                myDataTable = (DataTable)ViewState["myDataTable"];

            string strPoIds = string.Empty;
            string strPoNos = string.Empty;

            if (myDataTable != null)
            {
                for (int iCount = 0; iCount < myDataTable.Rows.Count; iCount++)
                {
                    if (strPoIds == string.Empty)
                    {
                        strPoIds = myDataTable.Rows[iCount]["PurchaseOrderID"].ToString();
                        strPoNos = myDataTable.Rows[iCount]["PurchaseNumber"].ToString();
                    }
                    else
                    {
                        strPoIds = strPoIds + "," + myDataTable.Rows[iCount]["PurchaseOrderID"].ToString();
                        strPoNos = strPoNos + "," + myDataTable.Rows[iCount]["PurchaseNumber"].ToString();
                    }
                }
            }

            oAPPBOK_BookingBE.PurchaseOrdersIDs = strPoIds;
            oAPPBOK_BookingBE.PurchaseOrders = strPoNos;

            // Sprint 1 - Point 4 - Start - For Booking Comments.
            oAPPBOK_BookingBE.BookingComments = txtEditBookingComment.Text.Trim();
            // Sprint 1 - Point 4 - End - For Booking Comments.

            bool BookingEditFlag = false;
            if (hdnCurrentMode.Value == "Edit")
                BookingEditFlag = IsBookingEdit(oAPPBOK_BookingBE);

            oAPPBOK_BookingBE.IsEnableHazardouesItemPrompt = checkBoxEnableHazardouesItemPrompt.Checked;
            oAPPBOK_BookingBE.ISPM15CountryPalletChecking = Convert.ToBoolean(chkSiteSetting.Checked);
            oAPPBOK_BookingBE.ISPM15FromCountryID = !string.IsNullOrEmpty(ddlSourceCountry.innerControlddlCountry.SelectedValue) ? Convert.ToInt32(ddlSourceCountry.innerControlddlCountry.SelectedValue) : 0;


            int? BookingID = oAPPBOK_BookingBAL.AddVendorWindowBookingBAL(oAPPBOK_BookingBE);
            Session["BookingCreated"] = true;
            Session["CreatedBookingID"] = BookingID.ToString();
            Session["IsBookingAmended"] = BookingEditFlag.ToString();


            //----------Start Stage 9 point 2a/2b------------//
            if (ThisBookingType == BookingType.ProvisionalNonTimedDelivery.ToString())
                EncryptQueryString("APPBok_Confirm.aspx?BookingType=PNTD&MailSend=True&BookingID=" + BookingID + "&IsBookingAmended=" + BookingEditFlag.ToString() + "&IsBookingEditWithMail=" + IsBookingEditWithMail.ToString());
            else
                EncryptQueryString("APPBok_Confirm.aspx?MailSend=True&BookingID=" + BookingID + "&IsBookingAmended=" + BookingEditFlag.ToString() + "&IsBookingEditWithMail=" + IsBookingEditWithMail.ToString());
            //----------End Stage 9 point 2a/2b------------//


        }
        catch (Exception ex)
        {

        }
    }

    private bool IsBookingEdit(APPBOK_BookingBE APPBOK_BookingBE)
    {

        bool bBookingEdit = false;

        try
        {
            APPBOK_BookingBE oAPPBOK_BookingBE = new APPBOK_BookingBE();
            APPBOK_BookingBAL oAPPBOK_BookingBAL = new APPBOK_BookingBAL();
            oAPPBOK_BookingBE.Action = "GetBookingDeatilsByID";
            oAPPBOK_BookingBE.BookingID = Convert.ToInt32(GetQueryStringValue("ID").ToString());
            List<APPBOK_BookingBE> lstBooking = oAPPBOK_BookingBAL.GetBookingDetailsByIDBAL(oAPPBOK_BookingBE);

            if (lstBooking.Count > 0)
            {
                int iTotalPalllets = 0;
                int iTotalLines = 0;
                int iTotalCartons = 0;
                int iTotalLifts = 0;

                for (int iCount = 0; iCount < lstBooking.Count; iCount++)
                {
                    iTotalPalllets += Convert.ToInt32(lstBooking[iCount].NumberOfPallet);
                    iTotalLines += Convert.ToInt32(lstBooking[iCount].NumberOfLines);
                    iTotalCartons += Convert.ToInt32(lstBooking[iCount].NumberOfCartons);
                    iTotalLifts += Convert.ToInt32(lstBooking[iCount].NumberOfLift);
                }

                if (iTotalLines != APPBOK_BookingBE.NumberOfLines)
                    bBookingEdit = true;
                else if (iTotalPalllets != APPBOK_BookingBE.NumberOfPallet)
                    bBookingEdit = true;
                else if (iTotalCartons != APPBOK_BookingBE.NumberOfCartons)
                    bBookingEdit = true;
                else if (iTotalLifts != APPBOK_BookingBE.NumberOfLift)
                    bBookingEdit = true;
                else if (!IsPoAreSame(lstBooking[0].PurchaseOrders, APPBOK_BookingBE.PurchaseOrders)) //to do
                    bBookingEdit = true;
                else if (lstBooking[0].SlotTime.SlotTimeID != APPBOK_BookingBE.TimeWindow.TimeWindowID)
                    bBookingEdit = true;
                else if (lstBooking[0].FixedSlot.FixedSlotID != APPBOK_BookingBE.FixedSlot.FixedSlotID)
                    bBookingEdit = true;
                else if (lstBooking[0].DoorNoSetup.SiteDoorNumberID != APPBOK_BookingBE.DoorNoSetup.SiteDoorNumberID)
                    bBookingEdit = true;
                else if (lstBooking[0].Carrier.CarrierID != APPBOK_BookingBE.Carrier.CarrierID)
                    bBookingEdit = true;
                else if (lstBooking[0].Delivery.DeliveryTypeID != APPBOK_BookingBE.Delivery.DeliveryTypeID)
                    bBookingEdit = true;
                else if (lstBooking[0].VehicleType.VehicleTypeID != APPBOK_BookingBE.VehicleType.VehicleTypeID)
                    bBookingEdit = true;
                else if (lstBooking[0].ScheduleDate != APPBOK_BookingBE.ScheduleDate)
                    bBookingEdit = true;
                else if (lstBooking[0].VendorID != APPBOK_BookingBE.VendorID)
                    bBookingEdit = true;
                else if (lstBooking[0].BookingComments != APPBOK_BookingBE.BookingComments)
                    bBookingEdit = true;
            }
        }
        catch
        {
            bBookingEdit = true;
        }
        return bBookingEdit;
    }

    private bool IsPoAreSame(string OldPos, string NewPos)
    {

        var firstOrdered = OldPos.Split(',')
                              .Select(t => t.Trim())
                              .Where(t => t != string.Empty)
                              .OrderBy(t => t);
        var secondOrdered = NewPos.Split(',')
                                        .Select(t => t.Trim())
                                        .Where(t => t != string.Empty)
                                        .OrderBy(t => t);
        bool stringsAreEqual = firstOrdered.SequenceEqual(secondOrdered);
        return stringsAreEqual;
    }
    private int getWeekdayNo()
    {
        string Weekday = string.Empty;

        if (hdnSuggestedWeekDay.Value.Trim() != string.Empty)
            Weekday = hdnSuggestedWeekDay.Value.ToLower();
        else
            Weekday = LogicalSchedulDateTime.DayOfWeek.ToString().ToLower().Substring(0, 3);

        int iWeekNo = 0;
        switch (Weekday)
        {
            case ("sun"):
                iWeekNo = 1;
                break;
            case ("mon"):
                iWeekNo = 2;
                break;
            case ("tue"):
                iWeekNo = 3;
                break;
            case ("wed"):
                iWeekNo = 4;
                break;
            case ("thu"):
                iWeekNo = 5;
                break;
            case ("fri"):
                iWeekNo = 6;
                break;
            case ("sat"):
                iWeekNo = 7;
                break;
            default:
                iWeekNo = 0;
                break;
        }
        return iWeekNo;
    }

    private string getWeekday(int iWeekNo)
    {
        string Weekday = string.Empty;

        switch (iWeekNo)
        {
            case (1):
                Weekday = "sun";
                break;
            case (2):
                Weekday = "mon";
                break;
            case (3):
                Weekday = "tue";
                break;
            case (4):
                Weekday = "wed";
                break;
            case (5):
                Weekday = "thu";
                break;
            case (6):
                Weekday = "fri";
                break;
            case (7):
                Weekday = "sat";
                break;
            default:
                Weekday = string.Empty;
                break;
        }
        return Weekday;
    }

    //protected void btnMakeNonTimeBooking_Click(object sender, EventArgs e)
    //{
    //    string errMsg = string.Empty;

    //    if (hdnCurrentRole.Value == "Vendor" || hdnCurrentRole.Value == "Carrier")
    //    {
    //        errMsg = WebCommon.getGlobalResourceValue("BK_MS_24_EXT_1") + " " + ActualSchedulDateTime.ToString("dd/MM/yyyy") + " " + WebCommon.getGlobalResourceValue("BK_MS_24_EXT_2");  // "This booking is to be made on " + txtSchedulingdate.GetDate.ToString() + " with no fixed time. Please ensure you make the delivery within the site's receiving hours. ";
    //        ShowErrorMessage(errMsg, "BK_MS_24_EXT");
    //        return;
    //    }
    //    else
    //    {
    //        errMsg = WebCommon.getGlobalResourceValue("BK_MS_24_INT");  // "This booking is to be made without a delivery time. Click 'CONTINUE' to confirm or 'BACK' to add a time to the booking";
    //        AddWarningMessages(errMsg, "BK_MS_24_INT");
    //        ShowWarningMessages();
    //    }
    //}

    //protected void btnAlternateSlot_Click(object sender, EventArgs e)
    //{
    //    //PaintAlternateSlotScreen();
    //}

    //private void PaintAlternateSlotScreen()
    //{

    //    ltVendorNo_2.Text = ucSeacrhVendorName; // ((HiddenField)ucSeacrhVendor1.FindControl("hdnVandorName")).Value;
    //    ltCarrier_2.Text = ddlCarrier.SelectedItem.Text;
    //    ltOtherCarrier_2.Text = "[" + txtCarrierOther.Text.Trim() + "]";
    //    if (txtCarrierOther.Text.Trim() != string.Empty)
    //    {
    //        ltOtherCarrier_2.Visible = true;
    //    }
    //    else
    //    {
    //        ltOtherCarrier_2.Visible = false;
    //    }

    //    ltVehicleType_2.Text = ddlVehicleType.SelectedItem.Text;
    //    ltOtherVehicleType_2.Text = "[" + txtVehicleOther.Text.Trim() + "]";
    //    if (txtVehicleOther.Text.Trim() != string.Empty)
    //    {
    //        ltOtherVehicleType_2.Visible = true;
    //    }
    //    else
    //    {
    //        ltOtherVehicleType_2.Visible = false;
    //    }
    //    ltSchedulingDate_2.Text = ActualSchedulDateTime.ToString("dd/MM/yyyy");
    //    ltTimeSuggested_2.Text = ltSuggestedSlotTime.Text;

    //    int? TotalPallets = txtPallets.Text.Trim() != string.Empty ? Convert.ToInt32(txtPallets.Text.Trim()) : (int?)null;
    //    int? TotalCartons = txtCartons.Text.Trim() != string.Empty ? Convert.ToInt32(txtCartons.Text.Trim()) : (int?)null;

    //    if (btnConfirmBooking.Visible)
    //    {
    //        iPallets = TotalPallets != (int?)null ? Convert.ToInt32(TotalPallets) : 0;
    //        iCartons = TotalCartons != (int?)null ? Convert.ToInt32(TotalCartons) : 0;

    //        int SlotLen = GetTimeSlotLength();

    //        string[] StartTime = ltTimeSuggested_2.Text.Split(':');

    //        if (StartTime.Length == 2)
    //        {
    //            TimeSpan time1 = new TimeSpan(Convert.ToInt32(StartTime[0]), Convert.ToInt32(StartTime[1].Substring(0, 2)), 00);
    //            TimeSpan time2 = new TimeSpan(00, iSlotTimeLength, 00);
    //            //TimeSpan totalTime = new TimeSpan(00, 00, 00); ;
    //            for (int iLen = 1; iLen <= SlotLen; iLen++)
    //            {
    //                if (time1.Hours == 23 && time1.Minutes == 55)
    //                {
    //                    time1 = new TimeSpan(00, 00, 00);
    //                }
    //                else
    //                {
    //                    time1 = time1.Add(time2);
    //                }
    //            }

    //            ltlEstEnd_2.Text = time1.Hours.ToString() + ":" + time1.Minutes.ToString();
    //        }
    //    }


    //    ltNumberofLifts.Text = txtLifts.Text.Trim() != string.Empty ? txtLifts.Text.Trim() : "-";
    //    ltNumberofPallets.Text = txtPallets.Text.Trim() != string.Empty ? txtPallets.Text.Trim() : "-";
    //    ltNumberofLines.Text = txtExpectedLines.Text.Trim() != string.Empty ? txtExpectedLines.Text.Trim() : "-";
    //    ltNumberofCartons.Text = txtCartons.Text.Trim() != string.Empty ? txtCartons.Text.Trim() : "-";

    //    GenerateNonTimeBooking();
    //    GenerateAlternateSlot();
    //    mvBookingEdit.ActiveViewIndex = 4;

    //}

    protected void btnWindowAlternateSlotVendor_Click(object sender, EventArgs e)
    {
        //GenerateAlternateSlotForVendor();
        //PaintAlternateScreenForVendor();
        //mvBookingEdit.ActiveViewIndex = 5;
        isProvisional = true;
        ProvisionalReason = "WINDOW CAPACITY";
        ConfirmWindowBooking();
    }

    private void AddWarningMessages(string ErrMessage, string CommandName)
    {
        NameValueCollection ErrorMessages = new NameValueCollection();
        if (ViewState["ErrorMessages"] != null)
        {
            ErrorMessages = (NameValueCollection)ViewState["ErrorMessages"];
        }
        if (ErrorMessages.Get(CommandName) == null)
        {
            ErrorMessages.Add(CommandName, ErrMessage);
            ViewState["ErrorMessages"] = ErrorMessages;
        }
    }

    private void ShowWarningMessages()
    {
        NameValueCollection ErrorMessages = new NameValueCollection();
        if (ViewState["ErrorMessages"] != null)
        {
            ErrorMessages = (NameValueCollection)ViewState["ErrorMessages"];
        }
        if (ErrorMessages.Keys.Count > 0)
        {
            string errMsg = ErrorMessages.Get(0);
            string CommandName = ErrorMessages.GetKey(0);
            if (CommandName == "BK_MS_26_EXT")
            {
                //errMsg = @"<span style=""color:Red;font-size:14px;"" >" + errMsg + "</span>";
            }
            ltConfirmMsg.Text = errMsg;
            btnErrContinue.CommandName = CommandName;
            btnErrBack.CommandName = CommandName;
            mdlConfirmMsg.Show();
            ErrorMessages.Remove(CommandName);
            ViewState["ErrorMessages"] = ErrorMessages;
        }
    }

    private void ShowErrorMessage(string ErrMessage, string CommandName)
    {
        ltErrorMsg.Text = ErrMessage;
        btnErrorMsgOK.CommandName = CommandName;
        mdlErrorMsg.Show();
    }

    protected void btnContinue_Click(object sender, CommandEventArgs e)
    {

        NameValueCollection ErrorMessages = new NameValueCollection();
        if (ViewState["ErrorMessages"] != null)
        {
            ErrorMessages = (NameValueCollection)ViewState["ErrorMessages"];
        }

        if (ErrorMessages.Keys.Count > 0)
        {
            ShowWarningMessages();
            return;
        }

        if (ErrorMessages.Keys.Count == 0)
        {
            if (e.CommandName == "BK_MS_05_INT" || e.CommandName == "BK_MS_04_INT" || e.CommandName == "BK_MS_04_INT_Carrier" || e.CommandName == "BK_MS_04_T_INT" || e.CommandName == "BK_MS_06_INT" || e.CommandName == "BK_MS_27_INT")
            {
                mvBookingEdit.ActiveViewIndex = 1;
                ScriptManager.RegisterStartupScript(Page, this.GetType(), "btnshow", "$get('" + btnProceedAddPO.ClientID + "').style.display='';", true);
            }
            else if (e.CommandName == "BK_MS_11_INT" || e.CommandName == "BK_MS_12_INT" || e.CommandName == "BK_MS_13_INT" || e.CommandName == "BK_MS_14_INT")
            {
                ViewState["ByContinue"] = "ByContinue";
                BindPOGrid(txtPurchaseNumber.Text);
                txtPurchaseNumber.Text = string.Empty;
                ScriptManager.RegisterStartupScript(Page, this.GetType(), "btnshow", "$get('" + btnProceedBooking.ClientID + "').style.display='';", true);
            }
            //else if (e.CommandName == "BK_MS_22_INT")
            //{
            //    //PaintAlternateSlotScreen();
            //}
            //else if (e.CommandName == "BK_MS_22_EXT")
            //{
            //    //GenerateAlternateSlotForVendor();
            //    //PaintAlternateScreenForVendor();
            //    mvBookingEdit.ActiveViewIndex = 5;
            //}
            //else if (e.CommandName == "BK_MS_24_INT")
            //{
            //    MakeBooking(BookingType.NonTimedDelivery.ToString());
            //}
            else if (e.CommandName == "BK_MS_18_INT" || e.CommandName == "BK_MS_35" || e.CommandName == "BK_MS_18_INT_Carrier" || e.CommandName == "BK_MS_18_INT_1")
            {
                if (hdnCurrentRole.Value == "Vendor" || hdnCurrentRole.Value == "Carrier")
                {
                    isProvisional = true;
                    ProvisionalReason = "WINDOW CAPACITY";
                }
                GetOutstandingPO();
            }
            else if (e.CommandName == "BK_MS_26_EXT" || e.CommandName == "BK_MS_26_Err" || e.CommandName == "BK_MS_06_INT_MaxContainers_Proceed")
            {
                GetOutstandingPO();
            }
            else if (e.CommandName == "BK_MS_23_EXT")
            {
                hdnCapacityWarning.Value = "false";
                isProvisional = true;
                ProvisionalReason = "WINDOW CAPACITY";
                MakeBooking(BookingType.Provisional.ToString());   // For Vendor
            }
            else if (e.CommandName == "Booking")
            { //Stage 9 point 3
                DeleteBooking();
            }
            else if (e.CommandName == "BK_MS_06_INT_MaxContainers")
            {  //----Stage 10 Point 10 L 5.1-----//
                VehicleTypeSelect();
            }
            //else if (e.CommandName == "BK_MS_23_INT" || e.CommandName == "BK_WMS_05_INT" || e.CommandName == "BK_WMS_04_INT" || e.CommandName == "BK_WMS_03_INT" || e.CommandName == "BK_WMS_02_INT" || e.CommandName == "BK_WMS_01_INT")
            //{
            //    hdnCapacityWarning.Value = "false";
            //    MakeBooking(BookingType.WindowSlot.ToString());// For OD Staff               
            //}
            else if(e.CommandName == "BK_WMS_06_EXT")
            {
                DoContinueVolumeExceeds();
            }
            else if (e.CommandName == "BK_MS_23_INT")
            {
                hdnCapacityWarning.Value = "false";
                if (rdoNonStandardWindow.Checked || Convert.ToBoolean(IsNonStandardWindowSlot) || (hdnSuggestedSiteDoorNumberID.Value == "-1" || hdnSuggestedSiteDoorNumberID.Value == string.Empty || hdnSuggestedSiteDoorNumberID.Value == "0"))
                {
                    //if (isProvisional)
                    //    MakeBooking(BookingType.WindowSlot.ToString());// For OD Staff
                    //else
                    MakeBooking(BookingType.NonStandardWindowSlot.ToString());// For OD Staff 
                }
                else
                {
                    MakeBooking(BookingType.WindowSlot.ToString());// For OD Staff               
                }
            }
            else if (e.CommandName == "NoSlotFound")
            {
                mvBookingEdit.ActiveViewIndex = 3;
            }
        }
    }

    protected void btnBack_Click(object sender, CommandEventArgs e)
    {

        if (e.CommandName == "BK_MS_05_INT" || e.CommandName == "BK_MS_04_INT" || e.CommandName == "BK_MS_04_T_INT" || e.CommandName == "BK_MS_06_INT" || e.CommandName == "BK_MS_27_INT" || e.CommandName == "BK_MS_06_INT_MaxContainers")
        {
            txtSchedulingdate.innerControltxtDate.Value = string.Empty; //to do
            mvBookingEdit.ActiveViewIndex = 1;
            ScriptManager.RegisterStartupScript(Page, this.GetType(), "btnhide", "$get('" + btnProceedAddPO.ClientID + "').style.display='none';", true);
        }
        else if (e.CommandName == "BK_MS_11_INT" || e.CommandName == "BK_MS_12_INT" || e.CommandName == "BK_MS_13_INT" || e.CommandName == "BK_MS_14_INT")
        {
            txtPurchaseNumber.Text = string.Empty;
            DataTable myDataTable = null;

            if (ViewState["myDataTable"] != null)
                myDataTable = (DataTable)ViewState["myDataTable"];
            else
                myDataTable = new DataTable();

            if (myDataTable.Rows.Count <= 0)
                ScriptManager.RegisterStartupScript(Page, this.GetType(), "btnshow", "$get('" + btnProceedBooking.ClientID + "').style.display='none';", true);

        }
        else if (e.CommandName == "BK_MS_23_EXT" || (e.CommandName == "BK_MS_23_INT"))
        {
            BindWindowSlot();
            //if (mvBookingEdit.ActiveViewIndex == 4) // For OD Staff
            //    //GenerateAlternateSlot();
            //else if (mvBookingEdit.ActiveViewIndex == 5) // For Vendor
            //    //GenerateAlternateSlotForVendor();
        }
        else if (e.CommandName == "BK_MS_26_Err")
        {
            txtExpectedLines.Text = string.Empty;
        }
        else if (e.CommandName == "BK_WMS_05_INT" || e.CommandName == "BK_WMS_04_INT" || e.CommandName == "BK_WMS_03_INT" || e.CommandName == "BK_WMS_02_INT" || e.CommandName == "BK_WMS_01_INT")
        {
            ddlAlternateWindow.SelectedIndex = 0;
            ddlAlternateWindow_SelectedIndexChanged(this, EventArgs.Empty);
        }
        else if (e.CommandName == "BK_MS_04_INT_Carrier")
        {
            ddlCarrier.SelectedIndex = 0;
        }
        else if (e.CommandName == "NoSlotFound")
        {
        }
        ViewState["ErrorMessages"] = null;
    }

    protected void btnErrorMsgOK_Click(object sender, CommandEventArgs e)
    {

        //ScriptManager.RegisterStartupScript(this, this.GetType(), "BK_MS_0", "<script>alert('" + errMsg + "');</script>", false);

        if (e.CommandName == "BK_MS_04_EXT" || e.CommandName == "BK_MS_04_T_EXT" || e.CommandName == "BK_MS_06_EXT" || e.CommandName == "BK_MS_27_EXT" || e.CommandName == "BK_MS_36")
        {
            txtSchedulingdate.innerControltxtDate.Value = string.Empty;
        }
        else if (e.CommandName == "BK_MS_05_EXT")
        {
            txtSchedulingdate.innerControltxtDate.Value = string.Empty;
            mvBookingEdit.ActiveViewIndex = 1;
            //EncryptQueryString("APPBok_BookingOverview.aspx?ModuleName=App. Scheduling");
            return;
        }
        else if (e.CommandName == "BK_MS_11_EXT" || e.CommandName == "BK_MS_12_EXT" || e.CommandName == "BK_MS_13_EXT" || e.CommandName == "BK_MS_14_EXT")
        {
            txtPurchaseNumber.Text = string.Empty;
        }
        else if (e.CommandName == "BK_MS_17")
        {
            ScriptManager.RegisterStartupScript(Page, this.GetType(), "btnhide", "$get('" + btnProceedBooking.ClientID + "').style.display='none';", true);
        }
        else if (e.CommandName == "BK_MS_28")
        {
            ScriptManager.RegisterStartupScript(Page, this.GetType(), "btnhide", "$get('" + btnProceedAddPO.ClientID + "').style.display='none';", true);
        }
        //else if (e.CommandName == "BK_MS_19")
        //{

        //}
        else if (e.CommandName == "BK_MS_26_Err")
        {
            txtExpectedLines.Text = string.Empty;
        }
        else if (e.CommandName == "BK_MS_06_Ext_MaxContainers")
        {
            txtSchedulingdate.innerControltxtDate.Value = string.Empty;
            ScriptManager.RegisterStartupScript(Page, this.GetType(), "btnhide", "$get('" + btnProceedAddPO.ClientID + "').style.display='none';", true);
        }
        //else if (e.CommandName == "BK_MS_24_EXT")
        //{
        //    MakeBooking(BookingType.NonTimedDelivery.ToString());
        //}
        //else if (e.CommandName == "BK_MS_20")
        //{
        //    //hdnbutton_Click(null, null);
        //}
    }

    //List<APPBOK_BookingBE> lstAllFixedDoor = new List<APPBOK_BookingBE>();
    int iTimeSlotID, iSiteDoorID, iPallets, iCartons, iLifts, iLines, actSlotLength, iNextSlotLength, iNextSlotLength2, iTimeSlotOrderBYID;
    bool isClickable, isCurrentSlotFixed, isNextSlotCovered, isCurrentSlotCovered;
    string DoorIDs = ",";
    string sTimeSlot = "";
    string sDoorNo;
    string sVendorCarrierName = "";
    string NextRequiredSlot = string.Empty;
    string sVendorCarrierNameNextSlot = string.Empty;
    string sCurrentFixedSlotID = "-1";
    string sSlotStartDay = string.Empty;

    //protected void GenerateNonTimeBooking()
    //{

    //    APPBOK_BookingBE oAPPBOK_BookingBE = new APPBOK_BookingBE();
    //    APPBOK_BookingBAL oMASBOK_BookingBAL = new APPBOK_BookingBAL();

    //    oAPPBOK_BookingBE.Action = "GetNonTimeBooking";
    //    oAPPBOK_BookingBE.ScheduleDate = LogicalSchedulDateTime;
    //    oAPPBOK_BookingBE.SiteId = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);

    //    //List<APPBOK_BookingBE> lstNonTimeBooking = oMASBOK_BookingBAL.GetNonTimeBookingBAL(oAPPBOK_BookingBE);

    //    //if (lstNonTimeBooking.Count > 0)
    //    //{
    //    //    gvNonTimeBooking.DataSource = lstNonTimeBooking;
    //    //    gvNonTimeBooking.DataBind();
    //    //}

    //}

    //private void GenerateAlternateSlot()
    //{
    //    GenerateAlternateSlot(false);
    //}

    //private void GenerateAlternateSlot(bool isStandardBookingRequired, int iSKUSiteDoorID = 0, int iSKUSiteDoorTypeID = 0)
    //{

    //    //int[] ArrDoorTypeID = new int[20];
    //    int? TotalPallets = txtPallets.Text.Trim() != string.Empty ? Convert.ToInt32(txtPallets.Text.Trim()) : (int?)null;
    //    int? TotalCartons = txtCartons.Text.Trim() != string.Empty ? Convert.ToInt32(txtCartons.Text.Trim()) : (int?)null;
    //    int? TotalLines = txtExpectedLines.Text.Trim() != string.Empty ? Convert.ToInt32(txtExpectedLines.Text.Trim()) : (int?)null;

    //    actSlotLength = GetRequiredTimeSlotLength(TotalPallets, TotalCartons);

    //    #region -------Collect Basic Information-----------
    //    //-----------------Start Collect Basic Information --------------------------//
    //    DateTime dtSelectedDate = LogicalSchedulDateTime;
    //    string WeekDay = dtSelectedDate.ToString("dddd");

    //    DateTime dtMon = Utilities.Common.GetFirstDayOfWeek(dtSelectedDate);

    //    MASSIT_WeekSetupBE oMASSIT_WeekSetupBE = new MASSIT_WeekSetupBE();
    //    MASSIT_WeekSetupBAL oMASSIT_WeekSetupBAL = new MASSIT_WeekSetupBAL();

    //    oMASSIT_WeekSetupBE.Action = "GetSpecificWeek";
    //    oMASSIT_WeekSetupBE.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
    //    oMASSIT_WeekSetupBE.EndWeekday = WeekDay;
    //    oMASSIT_WeekSetupBE.ScheduleDate = LogicalSchedulDateTime;

    //    List<MASSIT_WeekSetupBE> lstWeekSetup = oMASSIT_WeekSetupBAL.GetWeekSetupDetailsBAL(oMASSIT_WeekSetupBE);

    //    if (lstWeekSetup == null || lstWeekSetup.Count <= 0 || lstWeekSetup[0].StartTime == null)
    //    {

    //        oMASSIT_WeekSetupBE.Action = "ShowAll";
    //        lstWeekSetup = oMASSIT_WeekSetupBAL.GetWeekSetupDetailsBAL(oMASSIT_WeekSetupBE);

    //        if (lstWeekSetup.Count <= 0)
    //        {
    //            string errorMeesage = WebCommon.getGlobalResourceValue("WeekSetupSettingNotDefined");
    //            ScriptManager.RegisterStartupScript(this, this.GetType(), "alert1", "alert('" + errorMeesage + "')", true);
    //            return;
    //        }
    //        else if (lstWeekSetup[0].StartTime == null)
    //        {
    //            string errorMeesage = WebCommon.getGlobalResourceValue("WeekSetupSettingNotDefined");
    //            ScriptManager.RegisterStartupScript(this, this.GetType(), "alert1", "alert('" + errorMeesage + "')", true);
    //            return;
    //        }
    //    }



    //    APPSIT_DoorOpenTimeBAL oMASSIT_DoorOpenTimeBAL = new APPSIT_DoorOpenTimeBAL();
    //    MASSIT_DoorOpenTimeBE oMASSIT_DoorOpenTimeBE = new MASSIT_DoorOpenTimeBE();

    //    //------Check for Specific Entry First--------//        


    //    DoorIDs = GetDoorConstraints();

    //    oMASSIT_DoorOpenTimeBE.Action = "GetDoorNoAndType";
    //    oMASSIT_DoorOpenTimeBE.DoorNo = new MASSIT_DoorNoSetupBE();
    //    oMASSIT_DoorOpenTimeBE.DoorNo.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);

    //    List<MASSIT_DoorOpenTimeBE> lstDoorNoType = oMASSIT_DoorOpenTimeBAL.GetDoorNoSetupDetailsBAL(oMASSIT_DoorOpenTimeBE);

    //    int iDoorCount = lstDoorNoType.Count;

    //    //decimal decCellWidth = Convert.ToDecimal(155); // (87 / (iDoorCount));
    //    //double dblCellWidth = Convert.ToDouble(Math.Ceiling(decCellWidth));

    //    int dblCellWidth = 165;
    //    int i1stColumnWidth = 145;
    //    //-------------------------End Collect Basic Information ------------------------------//
    //    #endregion


    //    #region --------------Grid Header---------------
    //    //-------------Start Grid Header--------------//
    //    Table tblSlot = new Table();
    //    tblSlot.CellSpacing = 0;
    //    tblSlot.CellPadding = 0;
    //    tblSlot.CssClass = "FixedTables";
    //    tblSlot.ID = "Open_Text_General";
    //    tblSlot.Width = Unit.Pixel((dblCellWidth * iDoorCount) + i1stColumnWidth);

    //    //tableDiv_General.Controls.Add(tblSlot);

    //    TableRow trDoorNo = new TableRow();
    //    trDoorNo.TableSection = TableRowSection.TableHeader;
    //    TableHeaderCell tcDoorNo = new TableHeaderCell();
    //    tcDoorNo.Text = "Door Name / Door Type";  // +"<input type=image src='../../../Images/arrowleft.gif' style=height:16px; width:26px; />";
    //    tcDoorNo.BorderWidth = Unit.Pixel(1);
    //    tcDoorNo.Width = Unit.Pixel(i1stColumnWidth);
    //    trDoorNo.Cells.Add(tcDoorNo);

    //    TableRow trTime = new TableRow();
    //    trTime.TableSection = TableRowSection.TableHeader;
    //    TableHeaderCell tcTime = new TableHeaderCell();
    //    tcTime.Text = "Time";
    //    tcTime.BorderWidth = Unit.Pixel(1);
    //    tcTime.Width = Unit.Pixel(i1stColumnWidth);
    //    trTime.Cells.Add(tcTime);

    //    for (int iCount = 1; iCount <= iDoorCount; iCount++)
    //    {
    //        //ArrDoorTypeID[iCount - 1] = lstDoorNoType[iCount - 1].DoorType.DoorTypeID;
    //        tcDoorNo = new TableHeaderCell();
    //        tcDoorNo.BorderWidth = Unit.Pixel(1);
    //        tcDoorNo.Text = lstDoorNoType[iCount - 1].DoorNo.DoorNumber.ToString() + " / " + lstDoorNoType[iCount - 1].DoorType.DoorType; ;
    //        tcDoorNo.Width = Unit.Pixel(dblCellWidth);
    //        trDoorNo.Cells.Add(tcDoorNo);

    //        tcTime = new TableHeaderCell();
    //        tcTime.BorderWidth = Unit.Pixel(1);
    //        tcTime.Text = "Vendor/Carrier";
    //        tcTime.Width = Unit.Pixel(dblCellWidth);
    //        trTime.Cells.Add(tcTime);
    //    }
    //    trDoorNo.BackColor = System.Drawing.Color.LightGray;
    //    tblSlot.Rows.Add(trDoorNo);

    //    trTime.BackColor = System.Drawing.Color.LightGray;
    //    tblSlot.Rows.Add(trTime);
    //    //-------------End Grid Header -------------//
    //    #endregion

    //    #region --------------Slot Ploting---------------
    //    //-------------Start Slot Ploting--------------//

    //    //--------Find all Fixed Slots, Bookings For all Vendor/Carrier-------//
    //    APPBOK_BookingBE oAPPBOK_BookingBE = new APPBOK_BookingBE();
    //    APPBOK_BookingBAL oAPPBOK_BookingBAL = new APPBOK_BookingBAL();

    //    oAPPBOK_BookingBE.Action = "AllBookingFixedSlotDetails";
    //    oAPPBOK_BookingBE.SiteId = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
    //    oAPPBOK_BookingBE.ScheduleDate = LogicalSchedulDateTime;
    //    oAPPBOK_BookingBE.SelectedScheduleDate = LogicalSchedulDateTime;
    //    lstAllFixedDoor = oAPPBOK_BookingBAL.GetAllBookingFixedSlotDetailsBAL(oAPPBOK_BookingBE);

    //    lstAllFixedDoor = lstAllFixedDoor.FindAll(delegate(APPBOK_BookingBE St)
    //    {

    //        DateTime? dt = (DateTime?)null;
    //        if (St.SlotTime.SlotTime != null && St.SlotTime.SlotTime != string.Empty)
    //        {
    //            dt = new DateTime(1900, 1, 1, Convert.ToInt32(St.SlotTime.SlotTime.Split(':')[0]), Convert.ToInt32(St.SlotTime.SlotTime.Split(':')[1]), 0);
    //        }
    //        else
    //        {
    //            dt = new DateTime(1900, 1, 1, 0, 0, 0);
    //        }

    //        return dt <= lstWeekSetup[0].EndTime;

    //    });

    //    //--------------------------------------//

    //    if (lstWeekSetup[0].StartWeekday != lstWeekSetup[0].EndWeekday)
    //    {
    //        oAPPBOK_BookingBE.Action = "AllBookingFixedSlotDetails";
    //        oAPPBOK_BookingBE.SiteId = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
    //        oAPPBOK_BookingBE.ScheduleDate = LogicalSchedulDateTime.AddDays(-1);
    //        List<APPBOK_BookingBE> lstAllFixedDoorStartWeek = oAPPBOK_BookingBAL.GetAllBookingFixedSlotDetailsBAL(oAPPBOK_BookingBE);

    //        //Filetr Non Time Delivery
    //        lstAllFixedDoorStartWeek = lstAllFixedDoorStartWeek.FindAll(delegate(APPBOK_BookingBE St)
    //        {
    //            return St.SlotTime.SlotTime != "";
    //        });

    //        if (lstAllFixedDoorStartWeek != null && lstAllFixedDoorStartWeek.Count > 0)
    //        {
    //            lstAllFixedDoorStartWeek = lstAllFixedDoorStartWeek.FindAll(delegate(APPBOK_BookingBE St)
    //            {
    //                DateTime? dt = (DateTime?)null;
    //                if (St.SlotTime.SlotTime != null && St.SlotTime.SlotTime != string.Empty)
    //                {
    //                    dt = new DateTime(1900, 1, 1, Convert.ToInt32(St.SlotTime.SlotTime.Split(':')[0]), Convert.ToInt32(St.SlotTime.SlotTime.Split(':')[1]), 0);
    //                }
    //                else
    //                {
    //                    dt = new DateTime(1900, 1, 1, 0, 0, 0);
    //                }
    //                return dt >= lstWeekSetup[0].StartTime;
    //            });

    //            if (lstAllFixedDoorStartWeek.Count > 0)
    //            {
    //                lstAllFixedDoor = MergeListCollections(lstAllFixedDoor, lstAllFixedDoorStartWeek);
    //            }
    //        }
    //    }

    //    SYS_SlotTimeBAL oSYS_SlotTimeBAL = new SYS_SlotTimeBAL();
    //    SYS_SlotTimeBE oSYS_SlotTimeBE = new SYS_SlotTimeBE();

    //    oSYS_SlotTimeBE.Action = "ShowAll";
    //    List<SYS_SlotTimeBE> lstSlotTime = oSYS_SlotTimeBAL.GetSlotTimeBAL(oSYS_SlotTimeBE);

    //    for (int jCount = 0; jCount < lstSlotTime.Count; jCount++)
    //    {
    //        string StartTime = lstWeekSetup[0].StartTime.Value.Hour.ToString() + "." + lstWeekSetup[0].StartTime.Value.Minute.ToString();
    //        if (lstSlotTime[jCount].SlotTime.Split(new char[] { ':' })[0] == "00")
    //        {
    //            StartTime = "24." + lstWeekSetup[0].StartTime.Value.Minute.ToString();
    //        }

    //        string EndTime = lstWeekSetup[0].EndTime.Value.Hour.ToString() + "." + lstWeekSetup[0].EndTime.Value.Minute.ToString();
    //        if (lstSlotTime[jCount].SlotTime.Split(new char[] { ':' })[0] == "00")
    //        {
    //            EndTime = "24." + lstWeekSetup[0].EndTime.Value.Minute.ToString();
    //        }

    //        decimal dSlotTime = Convert.ToDecimal(lstSlotTime[jCount].SlotTime.Replace(':', '.'));
    //        decimal dStartTime = Convert.ToDecimal(StartTime);
    //        decimal dEndTime = Convert.ToDecimal(EndTime);

    //        if (lstWeekSetup[0].StartWeekday != lstWeekSetup[0].EndWeekday)
    //        {
    //            dEndTime = Convert.ToDecimal("24.00");
    //        }

    //        if (dSlotTime >= dStartTime && dSlotTime < dEndTime)
    //        {

    //            TableRow tr = new TableRow();
    //            tr.TableSection = TableRowSection.TableBody;
    //            TableCell tc = new TableCell();
    //            tc.BorderWidth = Unit.Pixel(1);

    //            if (lstSlotTime.Count - 1 > jCount)
    //            {
    //                tc.Text = lstWeekSetup[0].StartWeekday + " " + GetFormatedTime(lstSlotTime[jCount].SlotTime) + " - " + GetFormatedTime(lstSlotTime[jCount + 1].SlotTime);
    //            }
    //            else
    //            {
    //                tc.Text = lstWeekSetup[0].StartWeekday + " " + GetFormatedTime(lstSlotTime[jCount].SlotTime) + " - " + GetFormatedTime(lstSlotTime[0].SlotTime);
    //            }

    //            tc.Width = Unit.Pixel(i1stColumnWidth);
    //            tr.Cells.Add(tc);

    //            for (int iCount = 1; iCount <= iDoorCount; iCount++)
    //            {
    //                tc = new TableCell();
    //                tc.BorderWidth = Unit.Pixel(1);
    //                tc.Width = Unit.Pixel(dblCellWidth);
    //                iTimeSlotID = lstSlotTime[jCount].SlotTimeID;
    //                iSiteDoorID = lstDoorNoType[iCount - 1].SiteDoorNumberID;
    //                sTimeSlot = lstSlotTime[jCount].SlotTime;
    //                sDoorNo = lstDoorNoType[iCount - 1].DoorNo.DoorNumber;
    //                sSlotStartDay = lstWeekSetup[0].StartWeekday;

    //                //----Sprint 3b------//
    //                iTimeSlotOrderBYID = lstSlotTime[jCount].OrderBYID;
    //                //------------------//

    //                Literal ddl = SetCellColor(ref tc);
    //                tc.Controls.Add(ddl);
    //                tr.Cells.Add(tc);
    //            }
    //            tblSlot.Rows.Add(tr);
    //        }
    //    }



    //    if (lstWeekSetup[0].StartWeekday != lstWeekSetup[0].EndWeekday)
    //    {
    //        for (int jCount = 0; jCount < lstSlotTime.Count; jCount++)
    //        {
    //            string EndTime = lstWeekSetup[0].EndTime.Value.Hour.ToString() + "." + lstWeekSetup[0].EndTime.Value.Minute.ToString();
    //            if (lstSlotTime[jCount].SlotTime.Split(new char[] { ':' })[0] == "00")
    //            {
    //                EndTime = "24." + lstWeekSetup[0].EndTime.Value.Minute.ToString();
    //            }
    //            if (Convert.ToDecimal(lstSlotTime[jCount].SlotTime.Replace(':', '.')) <= Convert.ToDecimal(EndTime))
    //            {

    //                TableRow tr = new TableRow();
    //                tr.TableSection = TableRowSection.TableBody;
    //                TableCell tc = new TableCell();
    //                tc.BorderWidth = Unit.Pixel(1);

    //                if (lstSlotTime.Count - 1 > jCount)
    //                {
    //                    tc.Text = lstWeekSetup[0].EndWeekday + " " + GetFormatedTime(lstSlotTime[jCount].SlotTime) + " - " + GetFormatedTime(lstSlotTime[jCount + 1].SlotTime);
    //                }
    //                else
    //                {
    //                    tc.Text = lstWeekSetup[0].EndWeekday + " " + GetFormatedTime(lstSlotTime[jCount].SlotTime) + " - " + GetFormatedTime(lstSlotTime[0].SlotTime);
    //                }

    //                tc.Width = Unit.Pixel(i1stColumnWidth);
    //                tr.Cells.Add(tc);

    //                for (int iCount = 1; iCount <= iDoorCount; iCount++)
    //                {

    //                    tc = new TableCell();
    //                    tc.Width = Unit.Pixel(dblCellWidth);
    //                    tc.BorderWidth = Unit.Pixel(1);
    //                    iTimeSlotID = lstSlotTime[jCount].SlotTimeID;
    //                    iSiteDoorID = lstDoorNoType[iCount - 1].SiteDoorNumberID;
    //                    sTimeSlot = lstSlotTime[jCount].SlotTime;
    //                    sDoorNo = lstDoorNoType[iCount - 1].DoorNo.DoorNumber;
    //                    sSlotStartDay = lstWeekSetup[0].EndWeekday;

    //                    //----Sprint 3b------//
    //                    iTimeSlotOrderBYID = lstSlotTime[jCount].OrderBYID;
    //                    //------------------//

    //                    Literal ddl = SetCellColor(ref tc);
    //                    tc.Controls.Add(ddl);
    //                    tr.Cells.Add(tc);

    //                }
    //                tblSlot.Rows.Add(tr);

    //            }
    //        }
    //    }
    //    //-------------End Slot Ploting--------------//
    //    #endregion



    //    #region Start Finding Insufficent Slot Time
    //    Color cBackColorFirstCell, cBackColor, cBorderColor, cBackColorPreviousCell, cBorderColorPreviousCell, cBackColorFirstSlot;
    //    string att, abbr;
    //    bool isNextSlot;
    //    bool isSlotInsufficent;
    //    bool isNewCell = false;

    //    cBorderColorPreviousCell = Color.Empty;

    //    for (int iCell = 1; iCell <= iDoorCount; iCell++)
    //    {
    //        cBackColorPreviousCell = Color.Empty;

    //        for (int iRow = 0; iRow < tblSlot.Rows.Count; iRow++)
    //        {



    //            isSlotInsufficent = false;
    //            cBackColor = tblSlot.Rows[iRow].Cells[iCell].BackColor;
    //            cBackColorFirstCell = tblSlot.Rows[iRow].Cells[iCell].BackColor;
    //            cBorderColor = tblSlot.Rows[iRow].Cells[iCell].BorderColor;

    //            string temp = string.Empty;
    //            if (cBackColor == Color.GreenYellow)
    //                temp = string.Empty;

    //            cBackColorFirstSlot = Color.Empty;

    //            if ((cBackColor.Name == "0" || cBackColor == Color.White || cBackColor == Color.GreenYellow || cBackColor == Color.Orange) && cBorderColor != Color.Red)
    //            {
    //                isNextSlot = true;

    //                for (int iLen = 0; iLen < actSlotLength; iLen++)
    //                {
    //                    if (iRow + iLen < tblSlot.Rows.Count)
    //                    {

    //                        if (iLen == 0)
    //                            cBackColorFirstSlot = tblSlot.Rows[iRow + iLen].Cells[iCell].BackColor;

    //                        cBackColor = tblSlot.Rows[iRow + iLen].Cells[iCell].BackColor;
    //                        cBorderColor = tblSlot.Rows[iRow + iLen].Cells[iCell].BorderColor;

    //                        att = tblSlot.Rows[iRow + iLen].Cells[iCell].Attributes["onclick"];



    //                        if (att != null && att.Contains("setAletrnateSlot"))
    //                        {
    //                            tblSlot.Rows[iRow + iLen].Cells[iCell].Attributes["onclick"].Remove(0);
    //                        }





    //                        abbr = tblSlot.Rows[iRow + iLen].Cells[iCell].Attributes["abbr"];
    //                        if (abbr != null && abbr != string.Empty && abbr.Contains("C"))
    //                        {
    //                            isNextSlot = false;
    //                            break;
    //                        }
    //                        else if (abbr != null && abbr != string.Empty && abbr.Contains("V"))
    //                        {
    //                            string[] arrabbr = abbr.Split('-');
    //                            if (arrabbr[1] != ucSeacrhVendorID)
    //                            {
    //                                isNextSlot = false;
    //                                break;
    //                            }
    //                            else if (arrabbr[1] == ucSeacrhVendorID && iLen > 0 && cBackColor != Color.White && cBackColor != Color.GreenYellow && cBorderColor != Color.Red)
    //                            {
    //                                isNextSlot = false;
    //                                break;
    //                            }
    //                        }
    //                        else if (cBackColor == Color.Black)
    //                        {
    //                            isNextSlot = false;
    //                            break;
    //                        }
    //                    }
    //                    else
    //                    {
    //                        cBackColor = tblSlot.Rows[iRow].Cells[iCell].BackColor;

    //                        att = tblSlot.Rows[iRow].Cells[iCell].Attributes["onclick"];
    //                        if (att != null && att.Contains("setAletrnateSlot"))
    //                        {
    //                            tblSlot.Rows[iRow].Cells[iCell].Attributes["onclick"].Remove(0);
    //                        }

    //                        isNextSlot = false;

    //                        isSlotInsufficent = true;
    //                        break;
    //                    }


    //                    if (hdnCurrentRole.Value != "Vendor")
    //                    {
    //                        if (cBackColorFirstSlot == Color.GreenYellow)
    //                        {
    //                            if (tblSlot.Rows[iRow + iLen].Cells[iCell].BackColor == Color.Orange)
    //                            {
    //                                tblSlot.Rows[iRow + iLen].Cells[iCell].BackColor = Color.GreenYellow;
    //                            }
    //                        }
    //                    }

    //                }
    //                if (isNextSlot == false)
    //                {
    //                    att = tblSlot.Rows[iRow].Cells[iCell].Attributes["onclick"];

    //                    if (hdnCurrentRole.Value == "Vendor")
    //                    {
    //                        tblSlot.Rows[iRow].Cells[iCell].BackColor = Color.Black;
    //                        tblSlot.Rows[iRow].Cells[iCell].Attributes.Add("onmouseover", "this.style.cursor = 'default'");
    //                        tblSlot.Rows[iRow].Cells[iCell].Attributes.Add("onmouseout", "this.style.cursor = 'default'");
    //                        tblSlot.Rows[iRow].Cells[iCell].Attributes.Add("onclick", "");
    //                    }
    //                    else
    //                    {
    //                        if (cBackColor.Name != "0" && cBackColor != Color.White && cBackColor != Color.GreenYellow && cBackColor != Color.Orange)
    //                        {
    //                            if ((att != null && att.Contains("CheckAletrnateSlot")) || isSlotInsufficent)
    //                            {
    //                                att = att.Replace("CheckAletrnateSlot(", "");
    //                                att = att.Replace(");", "");
    //                                string[] Arratt = att.Split(',');
    //                                string att0 = Arratt[0];
    //                                string att1 = Arratt[1];
    //                                string att2 = Arratt[2];
    //                                string att3 = Arratt[3];
    //                                string att4 = Arratt[5];
    //                                tblSlot.Rows[iRow].Cells[iCell].Attributes.Add("onclick", "setAletrnateSlot('true'," + att0 + "," + att1 + "," + att2 + ",'-1'," + att3 + "," + att4 + ");");
    //                            }
    //                        }
    //                        else
    //                        {
    //                            if (att != null && att.Contains("CheckAletrnateSlot"))
    //                            {
    //                                att = att.Replace("CheckAletrnateSlot(", "");
    //                                att = att.Replace(");", "");
    //                                string[] Arratt = att.Split(',');
    //                                string att0 = Arratt[0];
    //                                string att1 = Arratt[1];
    //                                string att2 = Arratt[2];
    //                                string att3 = Arratt[3];
    //                                string att4 = Arratt[5];
    //                                tblSlot.Rows[iRow].Cells[iCell].Attributes.Add("onclick", "setAletrnateSlot('true'," + att0 + "," + att1 + "," + att2 + ",'-1'," + att3 + "," + att4 + ");");
    //                            }
    //                        }

    //                        if (cBackColorFirstCell != Color.GreenYellow && cBackColorFirstCell != Color.Orange)
    //                        {
    //                            tblSlot.Rows[iRow].Cells[iCell].BackColor = Color.LightGray;
    //                        }
    //                    }
    //                }
    //            }

    //            string ReqStrVehicleDoorTypeMatrix = string.Empty;

    //            if (isStandardBookingRequired == true && iSKUSiteDoorID != 0)
    //            {
    //                ReqStrVehicleDoorTypeMatrix = strVehicleDoorTypeMatrixSKU;
    //            }
    //            else
    //            {
    //                ReqStrVehicleDoorTypeMatrix = strVehicleDoorTypeMatrix;
    //            }


    //            //--Blackout the slots which are not matched with the seleted vehicle type-------// 
    //            if (!ReqStrVehicleDoorTypeMatrix.Contains(lstDoorNoType[iCell - 1].DoorNo.SiteDoorTypeID.ToString() + ","))
    //            {
    //                if ((tblSlot.Rows[iRow].Cells[iCell].BackColor == Color.White || tblSlot.Rows[iRow].Cells[iCell].BackColor == Color.LightGray) && tblSlot.Rows[iRow].Cells[iCell].BorderColor == Color.Red)
    //                {
    //                    tblSlot.Rows[iRow].Cells[iCell].ForeColor = Color.Black;
    //                    tblSlot.Rows[iRow].Cells[iCell].BackColor = Color.LightYellow;
    //                }

    //                //If SKU door matrix find the skip to blackout that type of door  SPRINT 4 POINT 7
    //                if (!(isStandardBookingRequired == true && iSKUSiteDoorID != 0 && iSKUSiteDoorTypeID == lstDoorNoType[iCell - 1].DoorNo.SiteDoorTypeID))
    //                {

    //                    if (tblSlot.Rows[iRow].Cells[iCell].BackColor == Color.White || tblSlot.Rows[iRow].Cells[iCell].BackColor == Color.LightGray)
    //                    {
    //                        tblSlot.Rows[iRow].Cells[iCell].BackColor = Color.Black;
    //                        tblSlot.Rows[iRow].Cells[iCell].BorderColor = Color.White;
    //                        //tblSlot.Rows[iRow].Cells[iCell].BorderWidth = 0;
    //                        tblSlot.Rows[iRow].Cells[iCell].Style.Add("border-top-width", "0px");  // .BorderWidth = 0;
    //                        tblSlot.Rows[iRow].Cells[iCell].Style.Add("border-bottom-width", "0px");
    //                        tblSlot.Rows[iRow].Cells[iCell].Style.Add("border-left-width", "1px");  // .BorderWidth = 0;
    //                        tblSlot.Rows[iRow].Cells[iCell].Style.Add("border-right-width", "1px");
    //                    }

    //                    if (hdnCurrentRole.Value == "Vendor")
    //                    {
    //                        tblSlot.Rows[iRow].Cells[iCell].Attributes.Add("onmouseover", "this.style.cursor = 'default'");
    //                        tblSlot.Rows[iRow].Cells[iCell].Attributes.Add("onmouseout", "this.style.cursor = 'default'");
    //                        tblSlot.Rows[iRow].Cells[iCell].Attributes.Add("onclick", "");
    //                    }
    //                }

    //            }
    //            //----------------------------------------------------------------------------//

    //            #region Row Spaning
    //            //-----------------------Row Spaning-------------//

    //            cBackColor = tblSlot.Rows[iRow].Cells[iCell].BackColor;
    //            cBorderColor = tblSlot.Rows[iRow].Cells[iCell].BorderColor;

    //            if (cBackColor != cBackColorPreviousCell || cBorderColor != cBorderColorPreviousCell)
    //            {
    //                isNewCell = true;
    //            }



    //            bool isCellCreated = false;
    //            if (cBackColor == cBackColorPreviousCell && cBackColor != Color.White)
    //            {

    //                isCellCreated = true;
    //                tblSlot.Rows[iRow].Cells[iCell].Style.Add("border-top-width", "0px");  // .BorderWidth = 0;
    //                tblSlot.Rows[iRow].Cells[iCell].Style.Add("border-bottom-width", "0px");

    //                if (cBorderColor == Color.Red || cBackColor == Color.Purple)
    //                {
    //                    tblSlot.Rows[iRow].Cells[iCell].Style.Add("border-left-width", "2px");
    //                    tblSlot.Rows[iRow].Cells[iCell].Style.Add("border-right-width", "2px");
    //                }
    //                else
    //                {
    //                    tblSlot.Rows[iRow].Cells[iCell].Style.Add("border-left-width", "1px");
    //                    tblSlot.Rows[iRow].Cells[iCell].Style.Add("border-right-width", "1px");
    //                }

    //                if (cBackColorPreviousCell == cBackColor && iRow > 0 && isNewCell == true)
    //                {
    //                    isNewCell = false;
    //                    tblSlot.Rows[iRow - 1].Cells[iCell].Style.Add("border-bottom-width", "0px");

    //                    if (cBorderColorPreviousCell == Color.Red || cBackColorPreviousCell == Color.Purple)
    //                    {
    //                        tblSlot.Rows[iRow - 1].Cells[iCell].Style.Add("border-top-width", "2px");
    //                        tblSlot.Rows[iRow - 1].Cells[iCell].Style.Add("border-left-width", "2px");
    //                        tblSlot.Rows[iRow - 1].Cells[iCell].Style.Add("border-right-width", "2px");
    //                    }
    //                    else
    //                    {
    //                        tblSlot.Rows[iRow - 1].Cells[iCell].Style.Add("border-top-width", "1px");
    //                        tblSlot.Rows[iRow - 1].Cells[iCell].Style.Add("border-left-width", "1px");
    //                        tblSlot.Rows[iRow - 1].Cells[iCell].Style.Add("border-right-width", "1px");
    //                    }
    //                }
    //            }
    //            else if (cBackColor == cBackColorPreviousCell && cBorderColor == Color.Red)
    //            {

    //                isCellCreated = true;

    //                tblSlot.Rows[iRow].Cells[iCell].Style.Add("border-top-width", "0px");  // .BorderWidth = 0;
    //                tblSlot.Rows[iRow].Cells[iCell].Style.Add("border-bottom-width", "0px");

    //                tblSlot.Rows[iRow].Cells[iCell].Style.Add("border-left-width", "2px");
    //                tblSlot.Rows[iRow].Cells[iCell].Style.Add("border-right-width", "2px");


    //                if (cBackColorPreviousCell == cBackColor && iRow > 0 && isNewCell == true)
    //                {
    //                    isNewCell = false;

    //                    if (cBorderColorPreviousCell != Color.Red)
    //                        tblSlot.Rows[iRow].Cells[iCell].Style.Add("border-top-width", "2px");

    //                    tblSlot.Rows[iRow - 1].Cells[iCell].Style.Add("border-bottom-width", "0px");
    //                    if (cBorderColorPreviousCell == Color.Red || cBackColorPreviousCell == Color.Purple)
    //                    {
    //                        tblSlot.Rows[iRow - 1].Cells[iCell].Style.Add("border-top-width", "2px");
    //                        tblSlot.Rows[iRow - 1].Cells[iCell].Style.Add("border-left-width", "2px");
    //                        tblSlot.Rows[iRow - 1].Cells[iCell].Style.Add("border-right-width", "2px");
    //                    }
    //                    else
    //                    {
    //                        tblSlot.Rows[iRow - 1].Cells[iCell].Style.Add("border-top-width", "1px");
    //                        tblSlot.Rows[iRow - 1].Cells[iCell].Style.Add("border-left-width", "1px");
    //                        tblSlot.Rows[iRow - 1].Cells[iCell].Style.Add("border-right-width", "1px");
    //                    }
    //                }
    //            }

    //            if (isCellCreated == false && iRow > 0)
    //            {
    //                if (cBorderColorPreviousCell == Color.Red || cBackColorPreviousCell == Color.Purple)
    //                {
    //                    tblSlot.Rows[iRow - 1].Cells[iCell].Style.Add("border-bottom-width", "2px");
    //                }
    //                else
    //                    tblSlot.Rows[iRow - 1].Cells[iCell].Style.Add("border-bottom-width", "1px");
    //            }

    //            abbr = tblSlot.Rows[iRow].Cells[iCell].Attributes["abbr"];
    //            if (abbr != null && abbr != string.Empty)
    //            {
    //                if (cBorderColor == Color.Red || cBackColor == Color.Purple)
    //                {
    //                    tblSlot.Rows[iRow].Cells[iCell].Style.Add("border-top-width", "2px");
    //                }
    //                else
    //                {
    //                    tblSlot.Rows[iRow].Cells[iCell].Style.Add("border-top-width", "1px");
    //                }
    //            }


    //            cBackColorPreviousCell = cBackColor;
    //            cBorderColorPreviousCell = cBorderColor;

    //            //--------------------------------------------------//
    //            #endregion
    //        }

    //    }



    //    #endregion

    //    #region Standard Booking Required

    //    if (isStandardBookingRequired)
    //    {
    //        bool isSlotFind = false;
    //        string strCheckedHour = "-10";
    //        bool isProrityChecked = false;
    //        bool isMoveBack = false;
    //        int iCellNo = 1;
    //        string iLowestPriority = string.Empty;

    //        if (iSKUSiteDoorID == 0) //SPRINT 4 POINT 7
    //            iLowestPriority = GetLowestPriority(ArrDoorTypePriority);   // Function
    //        else
    //            iLowestPriority = GetLowestPriority(ArrDoorTypePrioritySKU);

    //        string iSuggestedPriority = "1";

    //        for (int iRow = 0; iRow < tblSlot.Rows.Count; iRow++)
    //        {
    //            isProrityChecked = false;
    //            isMoveBack = false;
    //            for (int iCell = 1; iCell <= iDoorCount; iCell++)
    //            {

    //                if (iSKUSiteDoorID == 0 || lstDoorNoType[iCell - 1].SiteDoorNumberID == iSKUSiteDoorID)
    //                {  //SPRINT 4 POINT 7
    //                    int iDoorTypeID = lstDoorNoType[iCell - 1].DoorType.DoorTypeID;

    //                    cBackColor = tblSlot.Rows[iRow].Cells[iCell].BackColor;
    //                    cBorderColor = tblSlot.Rows[iRow].Cells[iCell].BorderColor;

    //                    bool isCurrentVendorSlot = false;
    //                    abbr = tblSlot.Rows[iRow].Cells[iCell].Attributes["abbr"];
    //                    if ((abbr == null || abbr == string.Empty) && cBorderColor != Color.Red)
    //                    {
    //                        isCurrentVendorSlot = true;
    //                    }
    //                    else if (abbr != null && abbr != string.Empty && abbr.Contains("C"))
    //                    {
    //                        isCurrentVendorSlot = false;
    //                    }
    //                    else if (abbr != null && abbr != string.Empty && abbr.Contains("V"))
    //                    {
    //                        string[] arrabbr = abbr.Split('-');
    //                        if (arrabbr[1] != ucSeacrhVendorID)
    //                        {
    //                            isCurrentVendorSlot = false;
    //                        }
    //                    }


    //                    if (cBackColor == Color.White && isCurrentVendorSlot)
    //                    {
    //                        att = tblSlot.Rows[iRow].Cells[iCell].Attributes["onclick"];
    //                        if (att.Contains("CheckAletrnateSlot"))
    //                        {
    //                            att = att.Replace("CheckAletrnateSlot(", "");
    //                            att = att.Replace(");", "");
    //                            string[] Arratt = att.Split(',');
    //                            string att0 = Arratt[0].Replace("'", "");
    //                            string att1 = Arratt[1].Replace("'", "");
    //                            string att2 = Arratt[2].Replace("'", "");
    //                            string att3 = Arratt[3].Replace("'", "");
    //                            string att4 = Arratt[5].Replace("'", "");

    //                            //--------- Sprint 3b----------//
    //                            string att6 = Arratt[6].Replace("'", "");
    //                            //-----------------------------//

    //                            if (isMoveBack == false)
    //                            {
    //                                //Check Before and after time//
    //                                if (LogicalSchedulDateTime.ToString("ddd").ToLower() == att4.ToLower())
    //                                {
    //                                    if (Convert.ToInt32(att6) < Convert.ToInt32(BeforeSlotTimeID) || Convert.ToInt32(att6) > Convert.ToInt32(AfterSlotTimeID))
    //                                    {
    //                                        continue;
    //                                    }
    //                                }
    //                                else
    //                                {

    //                                    continue;
    //                                }
    //                                //--------------------------//


    //                                //Check delivery constraints//
    //                                bool isValidTimeSlot = false;
    //                                if (att3.Split(':')[0] != strCheckedHour)
    //                                    isValidTimeSlot = CheckValidSlot(att3, att4, Convert.ToString(TotalPallets), Convert.ToString(TotalLines));

    //                                if (isValidTimeSlot == false)
    //                                {
    //                                    strCheckedHour = att3.Split(':')[0];
    //                                    continue;
    //                                }
    //                                //------------------------------//

    //                                //---------Check Door Priority---------//    

    //                                string iPriority = string.Empty;

    //                                if (isStandardBookingRequired == true && iSKUSiteDoorID != 0)
    //                                {
    //                                    iPriority = ArrDoorTypePrioritySKU.Get(iDoorTypeID.ToString());
    //                                }
    //                                else
    //                                {
    //                                    iPriority = ArrDoorTypePriority.Get(iDoorTypeID.ToString());
    //                                }



    //                                if (Convert.ToInt32(iPriority) != Convert.ToInt32(iLowestPriority))
    //                                {
    //                                    if (isProrityChecked == false)
    //                                    {
    //                                        iCellNo = iCell;
    //                                        iSuggestedPriority = iPriority;
    //                                        isProrityChecked = true;
    //                                        continue;
    //                                    }
    //                                    else if (isProrityChecked == true)
    //                                    {
    //                                        if (Convert.ToInt32(iPriority) < Convert.ToInt32(iSuggestedPriority))
    //                                        {
    //                                            iCellNo = iCell;
    //                                            iSuggestedPriority = iPriority;
    //                                            isProrityChecked = true;
    //                                            continue;
    //                                        }
    //                                        else
    //                                        {
    //                                            if (iCell < iDoorCount)
    //                                            {
    //                                                continue;
    //                                            }
    //                                            if (iCell == iDoorCount)
    //                                            {  //last cell
    //                                                iCell = iCellNo - 1;
    //                                                isMoveBack = true;
    //                                                continue;
    //                                            }
    //                                        }
    //                                    }
    //                                }
    //                                //-------------------------------------//
    //                            }

    //                            DateTime dtBookingDate = LogicalSchedulDateTime;
    //                            if (LogicalSchedulDateTime.ToString("ddd").ToLower() != att4.ToLower())
    //                            {
    //                                dtBookingDate = LogicalSchedulDateTime.AddDays(-1);
    //                            }

    //                            ltBookingDate.Text = dtBookingDate.Day.ToString() + "/" + dtBookingDate.Month.ToString() + "/" + dtBookingDate.Year.ToString();
    //                            ltSuggestedSlotTime.Text = att3 + " " + AtDoorName + " " + att2;
    //                            hdnSuggestedSlotTimeID.Value = att0;
    //                            hdnSuggestedWindowSlotID.Value = "-1";
    //                            hdnSuggestedSiteDoorNumberID.Value = att1;
    //                            hdnSuggestedSiteDoorNumber.Value = att2;

    //                            hdnSuggestedSlotTime.Value = att3;
    //                            //ltTimeSlot.Text = hdnSuggestedSlotTime.Value;

    //                            //ltDoorNo.Text = hdnSuggestedSiteDoorNumber.Value;
    //                            //ltTimeSlot_1.Text = att3;
    //                            //ltDoorNo_1.Text = hdnSuggestedSiteDoorNumber.Value;
    //                            hdnSuggestedWeekDay.Value = att4;

    //                            btnConfirmBooking.Visible = true;
    //                            lblProposeWindowBookingInfo.Visible = true;
    //                            isSlotFind = true;
    //                        }
    //                    }

    //                    //Last Cell ,still No slot found but a slot alrady found with lowest priority , go for that
    //                    if (iCell == iDoorCount && isSlotFind == false && isProrityChecked == true)
    //                    {
    //                        iCell = iCellNo - 1;
    //                        isMoveBack = true;
    //                        continue;
    //                    }
    //                } //SPRINT 4 POINT 7

    //                if (isSlotFind)
    //                    break;
    //            }
    //            if (isSlotFind)
    //                break;
    //        }
    //    }
    //    #endregion

    //    #region Set Background color for Fixed slot
    //    for (int iRow = 0; iRow < tblSlot.Rows.Count; iRow++)
    //    {
    //        for (int iCell = 1; iCell <= iDoorCount; iCell++)
    //        {
    //            cBackColor = tblSlot.Rows[iRow].Cells[iCell].BackColor;
    //            cBorderColor = tblSlot.Rows[iRow].Cells[iCell].BorderColor;

    //            if (cBorderColor == Color.Red && (cBackColor == Color.White || cBackColor == Color.LightGray))
    //            {
    //                tblSlot.Rows[iRow].Cells[iCell].BackColor = Color.LightYellow;
    //            }
    //        }
    //    }
    //    #endregion

    //    hdnGraySlotClicked.Value = "false";
    //}

    private string GetLowestPriority(NameValueCollection DoorTypePriority)
    {  //SPRINT 4 POINT 7
        NameValueCollection collection = (NameValueCollection)DoorTypePriority;
        string LowestPri = "1";
        if (collection != null)
        {
            foreach (string key in collection.AllKeys)
            {
                if (Convert.ToInt32(collection.Get(key)) < Convert.ToInt32(LowestPri))
                {
                    LowestPri = collection.Get(key);
                }
            }
        }
        return LowestPri;
    }

    public List<T> MergeListCollections<T>(List<T> firstList, List<T> secondList)
    {
        List<T> mergedList = new List<T>();
        mergedList.InsertRange(0, firstList);
        mergedList.InsertRange(mergedList.Count, secondList);
        return mergedList;
    }

    //private void CreateDynamicDiv(ref TableCell tcLocal, int SlotLength, string SlotStartTime)
    //{

    //    if (hdnCurrentRole.Value == "Vendor")
    //    {
    //        if (sVendorCarrierName != ltvendorName.Text)
    //        {
    //            return;
    //        }
    //    }

    //    string[] StartTime = SlotStartTime.Split(':');
    //    TimeSpan time1 = new TimeSpan(Convert.ToInt32(StartTime[0]), Convert.ToInt32(StartTime[1].Substring(0, 2)), 00);
    //    TimeSpan time2 = new TimeSpan(00, iSlotTimeLength, 00);
    //    for (int iLen = 1; iLen <= SlotLength; iLen++)
    //    {
    //        if (time1.Hours == 23 && time1.Minutes == 55)
    //        {
    //            time1 = new TimeSpan(00, 00, 00);
    //        }
    //        else
    //        {
    //            time1 = time1.Add(time2);
    //        }
    //    }

    //    string sSlotLength1;

    //    if (iSiteDoorID == 60 && iTimeSlotID == 42)
    //        sSlotLength1 = "0";


    //    // Create a new HtmlGenericControl.
    //    HtmlGenericControl NewControl = new HtmlGenericControl("div");

    //    // Set the properties of the new HtmlGenericControl control.
    //    string ControlID = "Volume" + iTimeSlotID.ToString() + iSiteDoorID.ToString();
    //    NewControl.ID = ControlID;
    //    string InnerHTML = "<table width='100%'> " +
    //                          "<tr><td colspan='5' align='center'><strong>{VendorName}</strong></td></tr> " +
    //                          "<tr> " +
    //                            "<td><strong>Arrival {Atime} </strong></td> " +
    //                            "<td>Lifts</td> " +
    //                            "<td>Pallets</td> " +
    //                            "<td>Cartons</td> " +
    //                            "<td>Lines</td> " +
    //                          "</tr> " +
    //                          "<tr> ";
    //    if (hdnCurrentRole.Value != "Vendor")
    //    {
    //        InnerHTML = InnerHTML + "<td><strong>Est Departure {DTime} </strong></td> ";
    //        InnerHTML = InnerHTML.Replace("{DTime}", GetFormatedTime(time1.ToString()));
    //    }
    //    else
    //    {
    //        InnerHTML = InnerHTML + "<td>&nbsp;</td>";
    //    }
    //    InnerHTML = InnerHTML + "<td>{Lifts}</td> " +
    //                            "<td>{Pallets}</td> " +
    //                            "<td>{Cartons}" +
    //                            "<td>{Lines}</td> " +
    //                          "</tr> " +
    //                        "</table>";

    //    InnerHTML = InnerHTML.Replace("{VendorName}", sVendorCarrierName);
    //    InnerHTML = InnerHTML.Replace("{Atime}", GetFormatedTime(SlotStartTime));


    //    if (iLifts != 0 && iLifts != -1)
    //        InnerHTML = InnerHTML.Replace("{Lifts}", iLifts.ToString());
    //    else
    //        InnerHTML = InnerHTML.Replace("{Lifts}", "-");

    //    if (iPallets != 0)
    //        InnerHTML = InnerHTML.Replace("{Pallets}", iPallets.ToString());
    //    else
    //        InnerHTML = InnerHTML.Replace("{Pallets}", "-");

    //    if (iCartons != 0)
    //        InnerHTML = InnerHTML.Replace("{Cartons}", iCartons.ToString());
    //    else
    //        InnerHTML = InnerHTML.Replace("{Cartons}", "-");

    //    if (iLines != 0)
    //        InnerHTML = InnerHTML.Replace("{Lines}", iLines.ToString());
    //    else
    //        InnerHTML = InnerHTML.Replace("{Lines}", "-");

    //    NewControl.InnerHtml = InnerHTML;
    //    NewControl.Attributes.Add("display", "none");
    //    // Add the new HtmlGenericControl to the Controls collection of the
    //    // TableCell control. 
    //    divVolume.Controls.Add(NewControl);
    //    if (!isClickable)
    //    {
    //        tcLocal.Attributes.Add("onmouseover", "ddrivetip('ctl00_ContentPlaceHolder1_" + ControlID + "','#ededed','400');");
    //        tcLocal.Attributes.Add("onmouseout", "hideddrivetip();");
    //    }
    //    else
    //    {
    //        tcLocal.Attributes.Add("onmouseover", "defColor=this.style.color;this.style.cursor = 'hand';  this.style.color='Red';ddrivetip('ctl00_ContentPlaceHolder1_" + ControlID + "','#ededed','400');");
    //        tcLocal.Attributes.Add("onmouseout", "this.style.color=defColor;this.style.cursor = 'default';hideddrivetip();");
    //    }
    //}

    //private void MakeCellClickableForOtherVendor(ref TableCell tclocal)
    //{
    //    string fixedSlotid = "-1";
    //    tclocal.Attributes.Add("onclick", "CheckAletrnateSlot('" + iTimeSlotID.ToString() + "','" + iSiteDoorID.ToString() + "','" + sDoorNo + "','" + sTimeSlot + "','" + fixedSlotid + "','" + sSlotStartDay + "','" + iTimeSlotOrderBYID + "');");
    //}

    //private void MakeCellClickable(ref TableCell tclocal)
    //{
    //    tclocal.Attributes.Add("onmouseover", "this.style.cursor = 'hand'");
    //    tclocal.Attributes.Add("onmouseout", "this.style.cursor = 'default'");

    //    string fixedSlotid = "-1";
    //    VendorDetails vd = new VendorDetails();

    //    string[] StartTime = sTimeSlot.Split(':');
    //    TimeSpan time1 = new TimeSpan(Convert.ToInt32(StartTime[0]), Convert.ToInt32(StartTime[1]), 00);

    //    if (SlotDoorCounter.ContainsKey(time1 + iSiteDoorID.ToString()))
    //    {
    //        vd = SlotDoorCounter[time1 + iSiteDoorID.ToString()];
    //        if (vd.CurrentFixedSlotID != null)
    //        {
    //            fixedSlotid = vd.CurrentFixedSlotID.ToString();
    //        }
    //    }

    //    tclocal.Attributes.Add("onclick", "CheckAletrnateSlot('" + iTimeSlotID.ToString() + "','" + iSiteDoorID.ToString() + "','" + sDoorNo + "','" + sTimeSlot + "','" + fixedSlotid + "','" + sSlotStartDay + "','" + iTimeSlotOrderBYID + "');");
    //}

    //private void RemoveCellClickable(ref TableCellCollection tcClocal)
    //{
    //    TableCell tclocal = tcClocal[0];
    //    tclocal.Attributes.Add("onmouseover", "this.style.cursor = 'default'");
    //    tclocal.Attributes.Add("onmouseout", "this.style.cursor = 'default'");
    //    tclocal.Attributes.Add("onclick", "");
    //}

    //private Literal SetCellColor(ref TableCell tclocal)
    //{
    //    string VendorCarrierName = string.Empty;
    //    int SlotLength = 0;
    //    NextRequiredSlot = string.Empty;
    //    iNextSlotLength = 0;
    //    iNextSlotLength2 = 0;
    //    sVendorCarrierNameNextSlot = string.Empty;
    //    isCurrentSlotCovered = false;

    //    //Find Next Fixed Slot on the Same door No if seledted slot is a fixed slot
    //    List<APPBOK_BookingBE> lstNextFixedDoorLocal = lstAllFixedDoor.FindAll(delegate(APPBOK_BookingBE Bok) { return Bok.DoorNoSetup.SiteDoorNumberID == iSiteDoorID; });

    //    for (int LoopCount = 0; LoopCount < lstNextFixedDoorLocal.Count; LoopCount++)
    //    {
    //        if (lstNextFixedDoorLocal[LoopCount].SlotTime.SlotTimeID == iTimeSlotID && lstNextFixedDoorLocal[LoopCount].DoorNoSetup.SiteDoorNumberID == iSiteDoorID)
    //        {
    //            if (LoopCount < lstNextFixedDoorLocal.Count - 1)
    //            {
    //                NextRequiredSlot = lstNextFixedDoorLocal[LoopCount + 1].SlotTime.SlotTime;
    //                sVendorCarrierNameNextSlot = lstNextFixedDoorLocal[LoopCount + 1].Vendor.VendorName;
    //                iNextSlotLength = 0; // GetRequiredTimeSlotLength(lstNextFixedDoorLocal[LoopCount + 1].NumberOfPallet, lstNextFixedDoorLocal[LoopCount + 1].NumberOfCartons);


    //                if (lstNextFixedDoorLocal[LoopCount + 1].Status != "Booked")
    //                {
    //                    if (lstNextFixedDoorLocal[LoopCount + 1].SupplierType == "C")
    //                        iNextSlotLength2 = GetRequiredTimeSlotLengthForOthers(lstNextFixedDoorLocal[LoopCount + 1].NumberOfPallet, lstNextFixedDoorLocal[LoopCount + 1].NumberOfCartons, lstNextFixedDoorLocal[LoopCount + 1].SupplierType, lstNextFixedDoorLocal[LoopCount + 1].Carrier.CarrierID); // GetRequiredTimeSlotLength(lstNextFixedDoorLocal[LoopCount].NumberOfPallet, lstNextFixedDoorLocal[LoopCount].NumberOfCartons);
    //                    else if (lstNextFixedDoorLocal[LoopCount + 1].SupplierType == "V")
    //                        iNextSlotLength2 = GetRequiredTimeSlotLengthForOthers(lstNextFixedDoorLocal[LoopCount + 1].NumberOfPallet, lstNextFixedDoorLocal[LoopCount + 1].NumberOfCartons, lstNextFixedDoorLocal[LoopCount + 1].SupplierType, lstNextFixedDoorLocal[LoopCount + 1].VendorID);
    //                }
    //                else
    //                {
    //                    iNextSlotLength2 = GetRequiredTimeSlotLength(lstNextFixedDoorLocal[LoopCount + 1].NumberOfPallet, lstNextFixedDoorLocal[LoopCount + 1].NumberOfCartons);
    //                }


    //            }
    //            break;
    //        }
    //    }
    //    //---------------------------------//

    //    //Find Next Slot on the Same door No if seledted slot is not a fixed slot
    //    if (NextRequiredSlot == string.Empty)
    //    {
    //        string[] StartTime = sTimeSlot.Split(':');
    //        TimeSpan time1 = new TimeSpan(Convert.ToInt32(StartTime[0]), Convert.ToInt32(StartTime[1]), 00);
    //        TimeSpan time2 = new TimeSpan(00, iSlotTimeLength, 00);
    //        int iLen = 0;
    //        for (iLen = 1; iLen <= actSlotLength; iLen++)
    //        {
    //            if (time1.Hours == 23 && time1.Minutes == 55)
    //            {
    //                time1 = new TimeSpan(00, 00, 00);
    //            }
    //            else
    //            {
    //                time1 = time1.Add(time2);
    //            }
    //            for (int LoopCount = 0; LoopCount < lstNextFixedDoorLocal.Count; LoopCount++)
    //            {
    //                if (GetFormatedTime(lstNextFixedDoorLocal[LoopCount].SlotTime.SlotTime) == GetFormatedTime(time1.Hours.ToString() + ":" + time1.Minutes.ToString()) && lstNextFixedDoorLocal[LoopCount].DoorNoSetup.SiteDoorNumberID == iSiteDoorID)
    //                {

    //                    if (lstNextFixedDoorLocal[LoopCount].Vendor.VendorName != ltvendorName.Text.Trim())
    //                    {
    //                        if (lstNextFixedDoorLocal[LoopCount].SupplierType == "C")
    //                            iNextSlotLength = GetRequiredTimeSlotLengthForOthers(lstNextFixedDoorLocal[LoopCount].NumberOfPallet, lstNextFixedDoorLocal[LoopCount].NumberOfCartons, lstNextFixedDoorLocal[LoopCount].SupplierType, lstNextFixedDoorLocal[LoopCount].Carrier.CarrierID); // GetRequiredTimeSlotLength(lstNextFixedDoorLocal[LoopCount].NumberOfPallet, lstNextFixedDoorLocal[LoopCount].NumberOfCartons);
    //                        else if (lstNextFixedDoorLocal[LoopCount].SupplierType == "V")
    //                            iNextSlotLength = GetRequiredTimeSlotLengthForOthers(lstNextFixedDoorLocal[LoopCount].NumberOfPallet, lstNextFixedDoorLocal[LoopCount].NumberOfCartons, lstNextFixedDoorLocal[LoopCount].SupplierType, lstNextFixedDoorLocal[LoopCount].VendorID);

    //                        NextRequiredSlot = lstNextFixedDoorLocal[LoopCount].SlotTime.SlotTime;
    //                        sVendorCarrierNameNextSlot = lstNextFixedDoorLocal[LoopCount].Vendor.VendorName;
    //                        break;
    //                    }
    //                }
    //            }
    //            if (NextRequiredSlot != string.Empty)
    //            {

    //                isNextSlotCovered = true;
    //                if (actSlotLength - iLen < iNextSlotLength)
    //                {
    //                    isNextSlotCovered = false;
    //                }
    //                break;
    //            }
    //        }
    //    }
    //    //---------------------------------//



    //    //Filter slot as per SlotTimeID and SiteDoorNumberID

    //    List<APPBOK_BookingBE> lstAllFixedDoorLocal = lstAllFixedDoor.FindAll(delegate(APPBOK_BookingBE Bok) { return Bok.SlotTime.SlotTimeID == iTimeSlotID && Bok.DoorNoSetup.SiteDoorNumberID == iSiteDoorID; });
    //    if (lstAllFixedDoorLocal.Count > 0)
    //    {

    //        if (lstAllFixedDoorLocal[0].VendorID == Convert.ToInt32(ucSeacrhVendorID) && lstAllFixedDoorLocal[0].SupplierType == "V")
    //        {
    //            sVendorCarrierName = ltvendorName.Text;
    //            iCartons = lstAllFixedDoorLocal[0].NumberOfCartons != (int?)null ? Convert.ToInt32(lstAllFixedDoorLocal[0].NumberOfCartons) : 0;
    //            iLifts = lstAllFixedDoorLocal[0].NumberOfLift != (int?)null ? Convert.ToInt32(lstAllFixedDoorLocal[0].NumberOfLift) : 0;
    //            iLines = lstAllFixedDoorLocal[0].NumberOfLines != (int?)null ? Convert.ToInt32(lstAllFixedDoorLocal[0].NumberOfLines) : 0;
    //            iPallets = lstAllFixedDoorLocal[0].NumberOfPallet != (int?)null ? Convert.ToInt32(lstAllFixedDoorLocal[0].NumberOfPallet) : 0;
    //            int VahicalTypeID = lstAllFixedDoorLocal[0].VehicleType.VehicleTypeID;

    //            SetSlotLegendForVendor(ref tclocal, VahicalTypeID);
    //            tclocal.Attributes.Add("abbr", "V-" + ucSeacrhVendorID.ToString());

    //            VendorCarrierName = sVendorCarrierName;
    //        }
    //        else
    //        {

    //            isClickable = true;
    //            sVendorCarrierName = lstAllFixedDoorLocal[0].Vendor.VendorName;
    //            //commented Just for run Fixed slot
    //            iCartons = lstAllFixedDoorLocal[0].NumberOfCartons != null ? Convert.ToInt32(lstAllFixedDoorLocal[0].NumberOfCartons) : 0;

    //            iPallets = lstAllFixedDoorLocal[0].NumberOfPallet != null ? Convert.ToInt32(lstAllFixedDoorLocal[0].NumberOfPallet) : 0;

    //            iLines = lstAllFixedDoorLocal[0].NumberOfLines != (int?)null ? Convert.ToInt32(lstAllFixedDoorLocal[0].NumberOfLines) : 0;

    //            iLifts = lstAllFixedDoorLocal[0].NumberOfLift != (int?)null ? Convert.ToInt32(lstAllFixedDoorLocal[0].NumberOfLift) : 0;


    //            if (lstAllFixedDoorLocal[0].SupplierType == "C")
    //            {
    //                SlotLength = GetRequiredTimeSlotLengthForOthers(lstAllFixedDoorLocal[0].NumberOfPallet, lstAllFixedDoorLocal[0].NumberOfCartons, lstAllFixedDoorLocal[0].SupplierType, lstAllFixedDoorLocal[0].Carrier.CarrierID);
    //                tclocal.Attributes.Add("abbr", "C-" + lstAllFixedDoorLocal[0].Carrier.CarrierID.ToString());
    //            }
    //            else if (lstAllFixedDoorLocal[0].SupplierType == "V")
    //            {
    //                SlotLength = GetRequiredTimeSlotLengthForOthers(lstAllFixedDoorLocal[0].NumberOfPallet, lstAllFixedDoorLocal[0].NumberOfCartons, lstAllFixedDoorLocal[0].SupplierType, lstAllFixedDoorLocal[0].VendorID);
    //                tclocal.Attributes.Add("abbr", "V-" + lstAllFixedDoorLocal[0].VendorID.ToString());
    //            }



    //            isCurrentSlotFixed = false;
    //            string tempVendorName = "";

    //            sCurrentFixedSlotID = lstAllFixedDoorLocal[0].FixedSlot.FixedSlotID.ToString();


    //            if (hdnSuggestedSlotTime.Value == sTimeSlot && hdnSuggestedSiteDoorNumber.Value == sDoorNo.ToString())
    //            {
    //                sVendorCarrierName = ltvendorName.Text;
    //                tempVendorName = SetCallHeight(actSlotLength, ref tclocal, Color.GreenYellow, hdnSuggestedSlotTime.Value);
    //                sVendorCarrierName = "";
    //            }
    //            else
    //            {
    //                if (hdnCurrentRole.Value == "Vendor")
    //                {
    //                    tempVendorName = SetCallHeight(SlotLength, ref tclocal, Color.Black, lstAllFixedDoorLocal[0].SlotTime.SlotTime);
    //                }
    //                else
    //                {
    //                    if (lstAllFixedDoorLocal[0].SlotType == "FIXED SLOT" && lstAllFixedDoorLocal[0].Status != "Booked")
    //                    {
    //                        isCurrentSlotFixed = true;
    //                        tempVendorName = SetCallHeight(SlotLength, ref tclocal, Color.Red, lstAllFixedDoorLocal[0].SlotTime.SlotTime);

    //                        if (!tempVendorName.Contains(sVendorCarrierName))
    //                        {
    //                            if (tempVendorName != string.Empty)
    //                            {
    //                                if (sVendorCarrierName != tempVendorName)
    //                                {
    //                                    tempVendorName = sVendorCarrierName + " / " + tempVendorName;
    //                                }
    //                            }
    //                            else
    //                            {
    //                                if (sVendorCarrierName != tempVendorName)
    //                                {
    //                                    tempVendorName = sVendorCarrierName;
    //                                }
    //                            }
    //                        }

    //                    }
    //                    else if (lstAllFixedDoorLocal[0].Status == "Booked")
    //                    {
    //                        tempVendorName = SetCallHeight(SlotLength, ref tclocal, Color.Purple, lstAllFixedDoorLocal[0].SlotTime.SlotTime);

    //                        if (!tempVendorName.Contains(sVendorCarrierName))
    //                        {
    //                            if (tempVendorName != string.Empty)
    //                            {
    //                                if (sVendorCarrierName != tempVendorName)
    //                                {
    //                                    tempVendorName = sVendorCarrierName + " / " + tempVendorName;
    //                                }
    //                            }
    //                            else
    //                            {
    //                                if (sVendorCarrierName != tempVendorName)
    //                                {
    //                                    tempVendorName = sVendorCarrierName;
    //                                }
    //                            }
    //                        }
    //                    }
    //                }
    //            }

    //            if (tempVendorName != string.Empty)
    //            {
    //                VendorCarrierName = tempVendorName;
    //            }
    //            else
    //            {

    //                VendorCarrierName = lstAllFixedDoorLocal[0].Vendor.VendorName;
    //                iTimeSlotID = lstAllFixedDoorLocal[0].SlotTime.SlotTimeID;
    //                iSiteDoorID = lstAllFixedDoorLocal[0].DoorNoSetup.SiteDoorNumberID;

    //                iCartons = lstAllFixedDoorLocal[0].NumberOfCartons != (int?)null ? Convert.ToInt32(lstAllFixedDoorLocal[0].NumberOfCartons) : 0;
    //                iLifts = lstAllFixedDoorLocal[0].NumberOfLift != (int?)null ? Convert.ToInt32(lstAllFixedDoorLocal[0].NumberOfLift) : 0;
    //                iLines = lstAllFixedDoorLocal[0].NumberOfLines != (int?)null ? Convert.ToInt32(lstAllFixedDoorLocal[0].NumberOfLines) : 0;
    //                iPallets = lstAllFixedDoorLocal[0].NumberOfPallet != (int?)null ? Convert.ToInt32(lstAllFixedDoorLocal[0].NumberOfPallet) : 0;


    //            }


    //            if (hdnCurrentRole.Value == "Vendor")
    //            {
    //                VendorCarrierName = "";
    //            }

    //            if (hdnCurrentRole.Value != "Vendor")
    //            {
    //                MakeCellClickableForOtherVendor(ref tclocal);
    //            }

    //        }
    //    }
    //    else
    //    {
    //        isCurrentSlotFixed = false;

    //        bool isLoopProcessed = false;

    //        if (DoorIDs.Contains("," + iTimeSlotID + "@" + iSiteDoorID))
    //        {  //Door Available         

    //            isLoopProcessed = true;

    //            if (hdnSuggestedSiteDoorNumberID.Value == string.Empty)
    //            {
    //                hdnSuggestedSiteDoorNumberID.Value = "-1";
    //                hdnSuggestedSlotTimeID.Value = "-1";
    //            }

    //            if (iSiteDoorID == Convert.ToInt32(hdnSuggestedSiteDoorNumberID.Value) && iTimeSlotID == Convert.ToInt32(hdnSuggestedSlotTimeID.Value))
    //            {

    //                int? TotalPallets = txtPallets.Text.Trim() != string.Empty ? Convert.ToInt32(txtPallets.Text.Trim()) : (int?)null;
    //                int? TotalCartons = txtCartons.Text.Trim() != string.Empty ? Convert.ToInt32(txtCartons.Text.Trim()) : (int?)null;
    //                int? TotalLifts = txtLifts.Text.Trim() != string.Empty ? Convert.ToInt32(txtLifts.Text.Trim()) : (int?)null;
    //                int? TotalLines = txtExpectedLines.Text.Trim() != string.Empty ? Convert.ToInt32(txtExpectedLines.Text.Trim()) : (int?)null;

    //                iPallets = TotalPallets != (int?)null ? Convert.ToInt32(TotalPallets) : 0;
    //                iCartons = TotalCartons != (int?)null ? Convert.ToInt32(TotalCartons) : 0;
    //                iLines = TotalLines != (int?)null ? Convert.ToInt32(TotalLines) : 0;
    //                iLifts = TotalLifts != (int?)null ? Convert.ToInt32(TotalLifts) : 0;

    //                sVendorCarrierName = ltvendorName.Text;

    //                SetCallHeight(actSlotLength, ref tclocal, Color.GreenYellow, sTimeSlot);


    //                VendorCarrierName = ltvendorName.Text;

    //                tclocal.Attributes.Add("abbr", "V-" + ucSeacrhVendorID.ToString());
    //            }
    //            else
    //            {
    //                MakeCellClickable(ref tclocal);
    //                VendorCarrierName = SetCallHeight(1, ref tclocal, Color.White, sTimeSlot);
    //                if (hdnCurrentRole.Value == "Vendor")
    //                {
    //                    VendorCarrierName = "";
    //                }
    //            }
    //        }
    //        else if (!DoorIDs.Contains("," + iTimeSlotID + "@" + iSiteDoorID) && hdnCurrentRole.Value != "Vendor")
    //        {  //Door Not Available but User is internal user



    //            MakeCellClickable(ref tclocal);

    //            if (hdnSuggestedSiteDoorNumberID.Value == string.Empty)
    //            {
    //                hdnSuggestedSiteDoorNumberID.Value = "-1";
    //                hdnSuggestedSlotTimeID.Value = "-1";
    //            }

    //            if (iSiteDoorID == Convert.ToInt32(hdnSuggestedSiteDoorNumberID.Value) && iTimeSlotID == Convert.ToInt32(hdnSuggestedSlotTimeID.Value))
    //            {

    //                isLoopProcessed = true;
    //                int? TotalPallets = txtPallets.Text.Trim() != string.Empty ? Convert.ToInt32(txtPallets.Text.Trim()) : (int?)null;
    //                int? TotalCartons = txtCartons.Text.Trim() != string.Empty ? Convert.ToInt32(txtCartons.Text.Trim()) : (int?)null;
    //                int? TotalLifts = txtLifts.Text.Trim() != string.Empty ? Convert.ToInt32(txtLifts.Text.Trim()) : (int?)null;
    //                int? TotalLines = txtExpectedLines.Text.Trim() != string.Empty ? Convert.ToInt32(txtExpectedLines.Text.Trim()) : (int?)null;

    //                iPallets = TotalPallets != (int?)null ? Convert.ToInt32(TotalPallets) : 0;
    //                iCartons = TotalCartons != (int?)null ? Convert.ToInt32(TotalCartons) : 0;
    //                iLines = TotalLines != (int?)null ? Convert.ToInt32(TotalLines) : 0;
    //                iLifts = TotalLifts != (int?)null ? Convert.ToInt32(TotalLifts) : 0;

    //                sVendorCarrierName = ltvendorName.Text;

    //                SetCallHeight(actSlotLength, ref tclocal, Color.GreenYellow, sTimeSlot);


    //                VendorCarrierName = ltvendorName.Text;

    //                tclocal.Attributes.Add("abbr", "V-" + ucSeacrhVendorID.ToString());
    //            }

    //        }

    //        if (isLoopProcessed == false)
    //        { // Door Not Available and external user
    //            isCurrentSlotFixed = false;

    //            VendorDetails vd = new VendorDetails();

    //            string[] StartTime = sTimeSlot.Split(':');
    //            TimeSpan time1 = new TimeSpan(Convert.ToInt32(StartTime[0]), Convert.ToInt32(StartTime[1]), 00);

    //            if (SlotDoorCounter.ContainsKey(time1 + iSiteDoorID.ToString()))
    //            {
    //                vd = SlotDoorCounter[time1 + iSiteDoorID.ToString()];
    //                if (vd.CellColor == Color.GreenYellow)
    //                {
    //                    SetCellBacknForeColor(ref tclocal, Color.Orange);
    //                    VendorCarrierName = vd.VendorName;
    //                }
    //                else
    //                {
    //                    SetCellBacknForeColor(ref tclocal, Color.Black);
    //                    if (hdnCurrentRole.Value != "Vendor")
    //                        MakeCellClickable(ref tclocal);
    //                    VendorCarrierName = "";
    //                }
    //            }
    //            else
    //            {
    //                SetCellBacknForeColor(ref tclocal, Color.Black);
    //                if (hdnCurrentRole.Value != "Vendor")
    //                    MakeCellClickable(ref tclocal);
    //                VendorCarrierName = "";
    //            }
    //        }
    //    }



    //    Literal lt = new Literal();
    //    lt.Text = VendorCarrierName;
    //    return lt;
    //}

    private int GetTimeSlotLength()
    {
        return GetTimeSlotLength(-1);
    }

    private int GetTimeSlotLength(int VehicleTypeID)
    {

        //--------start Calculate Unload Time-------------------//
        decimal TotalCalculatedTime = 0;  //In Min
        bool isTimeCalculatedForPallets = false;
        bool isTimeCalculatedForCartons = false;

        //Step1 >Check Vendor Processing Window 
        if (isTimeCalculatedForPallets == false || isTimeCalculatedForCartons == false)
        {

            MASSIT_VendorProcessingWindowBE oMASSIT_VendorProcessingWindowBE = new MASSIT_VendorProcessingWindowBE();
            APPSIT_VendorProcessingWindowBAL oMASSIT_VendorProcessingWindowBAL = new APPSIT_VendorProcessingWindowBAL();

            oMASSIT_VendorProcessingWindowBE.Action = "ShowAllConsolidate";
            oMASSIT_VendorProcessingWindowBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
            oMASSIT_VendorProcessingWindowBE.User.UserID = Convert.ToInt32(Session["UserID"]);
            oMASSIT_VendorProcessingWindowBE.VendorID = Convert.ToInt32(ucSeacrhVendorID); // Convert.ToInt32(ucSeacrhVendor1.VendorNo);
            oMASSIT_VendorProcessingWindowBE.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);


            MASSIT_VendorProcessingWindowBE lstVendor = new MASSIT_VendorProcessingWindowBE();
            lstVendor = oMASSIT_VendorProcessingWindowBAL.GetVendorDetailsByIdBAL(oMASSIT_VendorProcessingWindowBE);

            if (lstVendor != null)
            {  //Vendor Processing Window Not found

                if (lstVendor.APP_PalletsUnloadedPerHour > 0 && iPallets != 0 && isTimeCalculatedForPallets == false)
                {
                    TotalCalculatedTime += Convert.ToDecimal(Convert.ToInt32(iPallets) * (60.0 / lstVendor.APP_PalletsUnloadedPerHour));
                    isTimeCalculatedForPallets = true;
                }
                if (lstVendor.APP_CartonsUnloadedPerHour > 0 && iCartons != 0 && isTimeCalculatedForCartons == false)
                {
                    TotalCalculatedTime += Convert.ToDecimal(Convert.ToInt32(iCartons) * (60.0 / lstVendor.APP_CartonsUnloadedPerHour));
                    isTimeCalculatedForCartons = true;
                }
            }
        }
        //---------------------------------// 

        //Step2 > Check Vehicle Type Setup
        if (isTimeCalculatedForPallets == false || isTimeCalculatedForCartons == false)
        {
            MASSIT_VehicleTypeBE oMASSIT_VehicleTypeBE = new MASSIT_VehicleTypeBE();
            APPSIT_VehicleTypeBAL oMASSIT_VehicleTypeBAL = new APPSIT_VehicleTypeBAL();

            oMASSIT_VehicleTypeBE.Action = "ShowById";
            oMASSIT_VehicleTypeBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
            oMASSIT_VehicleTypeBE.User.UserID = Convert.ToInt32(Session["UserID"]);

            if (VehicleTypeID <= 0)
                oMASSIT_VehicleTypeBE.VehicleTypeID = Convert.ToInt32(ddlVehicleType.SelectedItem.Value);
            else
                oMASSIT_VehicleTypeBE.VehicleTypeID = VehicleTypeID;

            MASSIT_VehicleTypeBE lstVehicleType = oMASSIT_VehicleTypeBAL.GetVehicleTypeDetailsByIdBAL(oMASSIT_VehicleTypeBE);
            if (lstVehicleType != null)
            {
                //Volume Capacity 
                if (lstVehicleType.APP_PalletsUnloadedPerHour > 0 && iPallets != 0 && isTimeCalculatedForPallets == false)
                {
                    TotalCalculatedTime += Convert.ToDecimal(Convert.ToInt32(iPallets) * (60.0 / lstVehicleType.APP_PalletsUnloadedPerHour));
                    isTimeCalculatedForPallets = true;
                }
                if (lstVehicleType.APP_CartonsUnloadedPerHour > 0 && iCartons != 0 && isTimeCalculatedForCartons == false)
                {
                    TotalCalculatedTime += Convert.ToDecimal(Convert.ToInt32(iCartons) * (60.0 / lstVehicleType.APP_CartonsUnloadedPerHour));
                    isTimeCalculatedForCartons = true;
                }

                if (isTimeCalculatedForPallets == false && isTimeCalculatedForCartons == false)
                { //Time Based 
                    if (lstVehicleType.TotalUnloadTimeInMinute > 0)
                    {
                        TotalCalculatedTime += Convert.ToDecimal(Convert.ToDecimal(lstVehicleType.TotalUnloadTimeInMinute));
                        isTimeCalculatedForPallets = true;
                        isTimeCalculatedForCartons = true;
                    }
                }

                //---------------Stage 14 Point 15-------------//
                if (lstVehicleType.TotalUnloadTimeInMinute <= 0 && TotalCalculatedTime < lstVehicleType.MinimumTimePerDeliveryInMinute)
                {
                    TotalCalculatedTime = lstVehicleType.MinimumTimePerDeliveryInMinute;
                }
                //-------------------------------------------//
            }

        }
        //---------------------------------// 




        //Calculate Slot Length
        int intResult = (int)TotalCalculatedTime;
        int intr = (int)TotalCalculatedTime;

        //Not Required in case of Window booking -- Abhinav
        //if (intResult > 0)
        //    intr++;

        if (TotalCalculatedTime > 0 && TotalCalculatedTime < 1)
            intr = 1;

        if (TotalCalculatedTime == 0)
            intr++;

        return intr;
    }

    private int GetRequiredTimeSlotLength(int? TotalPallets, int? TotalCartons)
    {
        int itempCartons, itempPallets;
        itempCartons = iCartons;
        itempPallets = iPallets;



        iPallets = TotalPallets != (int?)null ? Convert.ToInt32(TotalPallets) : 0;
        iCartons = TotalCartons != (int?)null ? Convert.ToInt32(TotalCartons) : 0;

        int reqSlotLength = GetTimeSlotLength();

        iCartons = itempCartons;
        iPallets = itempPallets;

        return reqSlotLength;
    }

    private int GetRequiredTimeSlotLengthForOthers(int? TotalPallets, int? TotalCartons, string SupplierType, int ID, int VehicleID)
    {
        //--------start Calculate Unload Time-------------------//
        decimal TotalCalculatedTime = 0;  //In Min
        bool isTimeCalculatedForPallets = false;
        bool isTimeCalculatedForCartons = false;

        TotalPallets = TotalPallets == null ? 0 : TotalPallets;
        TotalCartons = TotalCartons == null ? 0 : TotalCartons;

        if (SupplierType == "V")
        {
            //Step1 >Check Vendor Processing Window 
            if (isTimeCalculatedForPallets == false || isTimeCalculatedForCartons == false)
            {

                MASSIT_VendorProcessingWindowBE oMASSIT_VendorProcessingWindowBE = new MASSIT_VendorProcessingWindowBE();
                APPSIT_VendorProcessingWindowBAL oMASSIT_VendorProcessingWindowBAL = new APPSIT_VendorProcessingWindowBAL();

                oMASSIT_VendorProcessingWindowBE.Action = "ShowAllConsolidate";
                oMASSIT_VendorProcessingWindowBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
                oMASSIT_VendorProcessingWindowBE.User.UserID = Convert.ToInt32(Session["UserID"]);
                oMASSIT_VendorProcessingWindowBE.VendorID = ID;
                oMASSIT_VendorProcessingWindowBE.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);


                MASSIT_VendorProcessingWindowBE lstVendor = new MASSIT_VendorProcessingWindowBE();
                lstVendor = oMASSIT_VendorProcessingWindowBAL.GetVendorDetailsByIdBAL(oMASSIT_VendorProcessingWindowBE);

                if (lstVendor != null)
                {  //Vendor Processing Window Not found

                    if (lstVendor.APP_PalletsUnloadedPerHour > 0 && TotalPallets != 0 && isTimeCalculatedForPallets == false)
                    {
                        TotalCalculatedTime += Convert.ToDecimal(Convert.ToInt32(TotalPallets) * (60.0 / lstVendor.APP_PalletsUnloadedPerHour));
                        isTimeCalculatedForPallets = true;
                    }
                    if (lstVendor.APP_CartonsUnloadedPerHour > 0 && TotalCartons != 0 && isTimeCalculatedForCartons == false)
                    {
                        TotalCalculatedTime += Convert.ToDecimal(Convert.ToInt32(TotalCartons) * (60.0 / lstVendor.APP_CartonsUnloadedPerHour));
                        isTimeCalculatedForCartons = true;
                    }
                }
            }
            //---------------------------------// 
        }

        if (SupplierType == "C")
        {
            //Step1 >Check Carrier Processing Window 
            if (isTimeCalculatedForPallets == false || isTimeCalculatedForCartons == false)
            {

                MASSIT_CarrierProcessingWindowBE oMASSIT_CarrerProcessingWindowBE = new MASSIT_CarrierProcessingWindowBE();
                APPSIT_CarrierProcessingWindowBAL oMASSIT_CarrerProcessingWindowBAL = new APPSIT_CarrierProcessingWindowBAL();

                oMASSIT_CarrerProcessingWindowBE.Action = "GetConstraints";
                oMASSIT_CarrerProcessingWindowBE.CarrierID = ID;
                oMASSIT_CarrerProcessingWindowBE.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
                MASSIT_CarrierProcessingWindowBE lstCarrier = oMASSIT_CarrerProcessingWindowBAL.GetConstraintsBAL(oMASSIT_CarrerProcessingWindowBE);


                if (lstCarrier != null)
                {  //Carrier Processing Window Not found

                    if (lstCarrier.APP_PalletsUnloadedPerHour > 0 && TotalPallets != 0 && isTimeCalculatedForPallets == false)
                    {
                        TotalCalculatedTime += Convert.ToDecimal(Convert.ToInt32(TotalPallets) * (60.0 / lstCarrier.APP_PalletsUnloadedPerHour));
                        isTimeCalculatedForPallets = true;
                    }
                    if (lstCarrier.APP_CartonsUnloadedPerHour > 0 && TotalCartons != 0 && isTimeCalculatedForCartons == false)
                    {
                        TotalCalculatedTime += Convert.ToDecimal(Convert.ToInt32(TotalCartons) * (60.0 / lstCarrier.APP_CartonsUnloadedPerHour));
                        isTimeCalculatedForCartons = true;
                    }
                }
            }
            //---------------------------------// 
        }


        //Step2 > Check Vehicle Type Setup  
        if (isTimeCalculatedForPallets == false || isTimeCalculatedForCartons == false)
        {
            MASSIT_VehicleTypeBE oMASSIT_VehicleTypeBE = new MASSIT_VehicleTypeBE();
            APPSIT_VehicleTypeBAL oMASSIT_VehicleTypeBAL = new APPSIT_VehicleTypeBAL();

            oMASSIT_VehicleTypeBE.Action = "ShowById";
            oMASSIT_VehicleTypeBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
            oMASSIT_VehicleTypeBE.User.UserID = Convert.ToInt32(Session["UserID"]);
            oMASSIT_VehicleTypeBE.VehicleTypeID = VehicleID;
            MASSIT_VehicleTypeBE lstVehicleType = oMASSIT_VehicleTypeBAL.GetVehicleTypeDetailsByIdBAL(oMASSIT_VehicleTypeBE);
            if (lstVehicleType != null)
            {
                //Volume Capacity 
                if (lstVehicleType.APP_PalletsUnloadedPerHour > 0 && iPallets != 0 && isTimeCalculatedForPallets == false)
                {
                    TotalCalculatedTime += Convert.ToDecimal(Convert.ToInt32(iPallets) * (60.0 / lstVehicleType.APP_PalletsUnloadedPerHour));
                    isTimeCalculatedForPallets = true;
                }
                if (lstVehicleType.APP_CartonsUnloadedPerHour > 0 && iCartons != 0 && isTimeCalculatedForCartons == false)
                {
                    TotalCalculatedTime += Convert.ToDecimal(Convert.ToInt32(iCartons) * (60.0 / lstVehicleType.APP_CartonsUnloadedPerHour));
                    isTimeCalculatedForCartons = true;
                }

                if (isTimeCalculatedForPallets == false && isTimeCalculatedForCartons == false)
                { //Time Based 
                    if (lstVehicleType.TotalUnloadTimeInMinute > 0)
                    {
                        TotalCalculatedTime += Convert.ToDecimal(Convert.ToDecimal(lstVehicleType.TotalUnloadTimeInMinute));
                        isTimeCalculatedForPallets = true;
                        isTimeCalculatedForCartons = true;
                    }
                }

                //---------------Stage 14 Point 15-------------//
                if (lstVehicleType.TotalUnloadTimeInMinute <= 0 && TotalCalculatedTime < lstVehicleType.MinimumTimePerDeliveryInMinute)
                {
                    TotalCalculatedTime = lstVehicleType.MinimumTimePerDeliveryInMinute;
                }
                //-------------------------------------------//
            }
        }
        //---------------------------------// 


        ////Step3 >Check Site Miscellaneous Settings
        //if (isTimeCalculatedForPallets == false || isTimeCalculatedForCartons == false)
        //{

        //    MAS_SiteBE oMAS_SiteBE = new MAS_SiteBE();
        //    MAS_SiteBAL oMAS_SiteBAL = new MAS_SiteBAL();

        //    if (hdnCurrentRole.Value == "Vendor")
        //        oMAS_SiteBE.Action = "ShowAllForVendor";
        //    else
        //        oMAS_SiteBE.Action = "ShowAll";

        //    oMAS_SiteBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
        //    oMAS_SiteBE.User.UserID = Convert.ToInt32(Session["UserID"]);
        //    oMAS_SiteBE.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);

        //    List<MAS_SiteBE> lstSiteMisSettings = new List<MAS_SiteBE>();
        //    lstSiteMisSettings = oMAS_SiteBAL.GetSiteMisSettingBAL(oMAS_SiteBE);

        //    if (lstSiteMisSettings != null && lstSiteMisSettings.Count > 0)
        //    {
        //        if (lstSiteMisSettings[0].AveragePalletUnloadedPerManHour > 0 && TotalPallets != 0 && isTimeCalculatedForPallets == false)
        //        {
        //            TotalCalculatedTime += Convert.ToDecimal(Convert.ToInt32(TotalPallets) * (60.0 / lstSiteMisSettings[0].AveragePalletUnloadedPerManHour));      // Convert.ToDecimal((Convert.ToInt32(TotalPallets) / lstSiteMisSettings[0].AveragePalletUnloadedPerManHour) * 60);
        //            isTimeCalculatedForPallets = true;
        //        }
        //        if (lstSiteMisSettings[0].AverageCartonUnloadedPerManHour > 0 && TotalCartons != 0 && isTimeCalculatedForCartons == false)
        //        {
        //            TotalCalculatedTime += Convert.ToDecimal(Convert.ToInt32(TotalCartons) * (60.0 / lstSiteMisSettings[0].AverageCartonUnloadedPerManHour));      //Convert.ToDecimal((Convert.ToInt32(TotalCartons) / lstSiteMisSettings[0].AverageCartonUnloadedPerManHour) * 60);
        //            isTimeCalculatedForCartons = true;
        //        }
        //    }
        //}
        //---------------------------------//   

        //Calculate Slot Length
        int intResult = (int)TotalCalculatedTime;
        int intr = (int)TotalCalculatedTime;

        //Not Required in case of Window booking -- Abhinav
        //if (intResult > 0)
        //    intr++;

        if (TotalCalculatedTime > 0 && TotalCalculatedTime < 1)
            intr = 1;

        if (TotalCalculatedTime == 0)
            intr++;


        return intr;
    }

    //private void SetSlotLegendForVendor(ref TableCell tclocal, int VahicalTypeID)
    //{

    //    int SlotLength = 0;
    //    string CapacityWarning = "false";
    //    int? TotalPallets = txtPallets.Text.Trim() != string.Empty ? Convert.ToInt32(txtPallets.Text.Trim()) : (int?)null;
    //    int? TotalCartons = txtCartons.Text.Trim() != string.Empty ? Convert.ToInt32(txtCartons.Text.Trim()) : (int?)null;
    //    int? TotalLifts = txtLifts.Text.Trim() != string.Empty ? Convert.ToInt32(txtLifts.Text.Trim()) : (int?)null;
    //    int? TotalLines = txtExpectedLines.Text.Trim() != string.Empty ? Convert.ToInt32(txtExpectedLines.Text.Trim()) : (int?)null;
    //    string tempVendorName = string.Empty;
    //    //bool isAvailable = false;


    //    //if (iCartons < TotalCartons || iPallets < TotalPallets) {
    //    //    CapacityWarning = "true";
    //    //}



    //    //foreach (GridViewRow gvRow in gvBookingSlots.Rows) {
    //    //    if (gvRow.RowType == DataControlRowType.DataRow) {
    //    //        HiddenField hdnAvailableFixedSlotID = (HiddenField)gvRow.FindControl("hdnAvailableFixedSlotID");
    //    //        HiddenField hdnAvailableSlotTimeID = (HiddenField)gvRow.FindControl("hdnAvailableSlotTimeID");
    //    //        HiddenField hdnAvailableSiteDoorNumberID = (HiddenField)gvRow.FindControl("hdnAvailableSiteDoorNumberID");
    //    //        HiddenField hdnAvailableSiteDoorNumber = (HiddenField)gvRow.FindControl("hdnAvailableSiteDoorNumber");
    //    //        ucLiteral ltTime = (ucLiteral)gvRow.FindControl("ltTime");

    //    //        if (gvRow.Cells[5].Text == "FIXED SLOT")
    //    //            isCurrentSlotFixed = true;
    //    //        if (hdnAvailableSlotTimeID.Value == iTimeSlotID.ToString() && hdnAvailableSiteDoorNumberID.Value == iSiteDoorID.ToString()) {
    //    //            if (hdnAvailableFixedSlotID != null && hdnAvailableFixedSlotID.Value == hdnSuggestedWindowSlotID.Value && hdnSuggestedWindowSlotID.Value != "-1") { //This Booking //if (gvRow.Cells[4].Text == "This Booking") {                       

    //    //                iPallets = TotalPallets != (int?)null ? Convert.ToInt32(TotalPallets) : 0;
    //    //                iCartons = TotalCartons != (int?)null ? Convert.ToInt32(TotalCartons) : 0;
    //    //                iLines = TotalLines != (int?)null ? Convert.ToInt32(TotalLines) : 0;
    //    //                iLifts = TotalLifts != (int?)null ? Convert.ToInt32(TotalLifts) : 0;
    //    //                isClickable = false;

    //    //                if (hdnCurrentMode.Value == "Edit")
    //    //                    SlotLength = GetTimeSlotLength();
    //    //                else
    //    //                    SlotLength = GetRequiredTimeSlotLengthForOthers(iPallets, iCartons, "V", Convert.ToInt32(ucSeacrhVendorID));

    //    //                SetCallHeight(SlotLength, ref tclocal, Color.GreenYellow, ltTime.Text.Trim());

    //    //                if (actSlotLength > SlotLength) {
    //    //                    string[] StartTime = ltTime.Text.Split(':');
    //    //                    TimeSpan time1 = new TimeSpan(Convert.ToInt32(StartTime[0]), Convert.ToInt32(StartTime[1].Substring(0, 2)), 00);
    //    //                    TimeSpan time2 = new TimeSpan(00, iSlotTimeLength, 00);
    //    //                    for (int iLen = 1; iLen <= SlotLength - 1; iLen++) {
    //    //                        if (time1.Hours == 23 && time1.Minutes == 55) {
    //    //                            time1 = new TimeSpan(00, 00, 00);
    //    //                        }
    //    //                        else {
    //    //                            time1 = time1.Add(time2);
    //    //                        }
    //    //                    }
    //    //                    SetCallHeight(actSlotLength - SlotLength, Color.Red, time1.Hours.ToString() + ":" + time1.Minutes.ToString());
    //    //                }

    //    //                if (isCurrentSlotFixed && hdnCurrentRole.Value == "Vendor") {
    //    //                    tclocal.Attributes.Add("onclick", "setAletrnateSlot('" + CapacityWarning + "','" + hdnAvailableSlotTimeID.Value + "','" + hdnAvailableSiteDoorNumberID.Value + "','" + hdnAvailableSiteDoorNumber.Value + "','" + hdnAvailableFixedSlotID.Value + "','" + ltTime.Text + "','" + sSlotStartDay + "');");

    //    //                }

    //    //                //isAvailable = true;
    //    //                break;
    //    //            }
    //    //            else if (hdnCurrentMode.Value == "Edit" && hdnSuggestedSlotTimeID.Value == iTimeSlotID.ToString() && hdnSuggestedSiteDoorNumberID.Value == iSiteDoorID.ToString()) { //This Booking //if (gvRow.Cells[4].Text == "This Booking") {                       

    //    //                iPallets = TotalPallets != (int?)null ? Convert.ToInt32(TotalPallets) : 0;
    //    //                iCartons = TotalCartons != (int?)null ? Convert.ToInt32(TotalCartons) : 0;
    //    //                iLines = TotalLines != (int?)null ? Convert.ToInt32(TotalLines) : 0;
    //    //                iLifts = 0;
    //    //                isClickable = false;
    //    //                //CreateDynamicDiv(ref tclocal,false);
    //    //                SlotLength = GetTimeSlotLength();

    //    //                SetCallHeight(SlotLength, ref tclocal, Color.GreenYellow, ltTime.Text.Trim());

    //    //                if (isCurrentSlotFixed && hdnCurrentRole.Value == "Vendor") {
    //    //                    tclocal.Attributes.Add("onclick", "setAletrnateSlot('" + CapacityWarning + "','" + hdnAvailableSlotTimeID.Value + "','" + hdnAvailableSiteDoorNumberID.Value + "','" + hdnAvailableSiteDoorNumber.Value + "','" + hdnAvailableFixedSlotID.Value + "','" + ltTime.Text + "','" + sSlotStartDay + "');");

    //    //                }
    //    //                break;
    //    //            }

    //    //            else if (gvRow.Cells[6].Text == "Booked") {   //Booked Slots                       
    //    //                //SetCellBacknForeColor(ref tclocal, Color.Purple, isCurrentSlotFixed);
    //    //                isClickable = false;
    //    //                //CreateDynamicDiv(ref tclocal,false);
    //    //                SlotLength = GetTimeSlotLength(VahicalTypeID);
    //    //                //sVendorCarrierName = SetCallHeight(SlotLength, ref tclocal, Color.Purple, ltTime.Text.Trim());

    //    //                //For Display current vendor name-------
    //    //                tempVendorName = SetCallHeight(SlotLength, ref tclocal, Color.Purple, ltTime.Text.Trim());
    //    //                if (!tempVendorName.Contains(sVendorCarrierName)) {
    //    //                    if (tempVendorName != string.Empty) {
    //    //                        if (sVendorCarrierName != tempVendorName) {
    //    //                            tempVendorName = sVendorCarrierName + " / " + tempVendorName;
    //    //                        }
    //    //                    }
    //    //                    else {
    //    //                        if (sVendorCarrierName != tempVendorName) {
    //    //                            tempVendorName = sVendorCarrierName;
    //    //                        }
    //    //                    }
    //    //                }
    //    //                sVendorCarrierName = tempVendorName;
    //    //                //---------------------------------------

    //    //                if (isCurrentSlotFixed && hdnCurrentRole.Value == "Vendor") {
    //    //                    tclocal.Attributes.Add("onclick", "setAletrnateSlot('" + CapacityWarning + "','" + hdnAvailableSlotTimeID.Value + "','" + hdnAvailableSiteDoorNumberID.Value + "','" + hdnAvailableSiteDoorNumber.Value + "','" + hdnAvailableFixedSlotID.Value + "','" + ltTime.Text + "','" + sSlotStartDay + "');");

    //    //                }

    //    //                //isAvailable = true;
    //    //                break;
    //    //            }
    //    //            else if (gvRow.Cells[5].Text == "FIXED SLOT") {  // Fixed Slots        
    //    //                sCurrentFixedSlotID = "-1";
    //    //                isClickable = true;

    //    //                //SlotLength = GetTimeSlotLength();
    //    //                SlotLength = GetRequiredTimeSlotLengthForOthers(iPallets, iCartons, "V", Convert.ToInt32(ucSeacrhVendorID));

    //    //                if (actSlotLength > SlotLength) {
    //    //                    CapacityWarning = "true";
    //    //                }
    //    //                if (hdnAvailableFixedSlotID != null) {
    //    //                    sCurrentFixedSlotID = hdnAvailableFixedSlotID.Value;
    //    //                }

    //    //                if (hdnAvailableFixedSlotID != null && hdnAvailableFixedSlotID.Value != hdnCurrentFixedSlotID.Value) {

    //    //                    if (hdnCurrentRole.Value == "Vendor") {
    //    //                        tempVendorName = SetCallHeight(SlotLength, ref tclocal, Color.Red, ltTime.Text.Trim());
    //    //                    }
    //    //                    else {
    //    //                        if (actSlotLength > SlotLength) {
    //    //                            string[] StartTime = ltTime.Text.Trim().Split(':');
    //    //                            TimeSpan time1 = new TimeSpan(Convert.ToInt32(StartTime[0]), Convert.ToInt32(StartTime[1]), 00);
    //    //                            TimeSpan time2 = new TimeSpan(00, iSlotTimeLength, 00);
    //    //                            VendorDetails vd = new VendorDetails();

    //    //                            if (SlotDoorCounter.ContainsKey(time1.ToString() + iSiteDoorID.ToString())) {
    //    //                                isCurrentSlotFixed = vd.isCurrentSlotFixed;
    //    //                                tempVendorName = SetCallHeight(SlotLength, ref tclocal, vd.CellColor, ltTime.Text.Trim());
    //    //                            }
    //    //                            else {
    //    //                                tempVendorName = SetCallHeight(SlotLength, ref tclocal, Color.LightGray, ltTime.Text.Trim());
    //    //                            }

    //    //                        }
    //    //                        else {
    //    //                            tempVendorName = SetCallHeight(SlotLength, ref tclocal, Color.Red, ltTime.Text.Trim());
    //    //                        }
    //    //                    }
    //    //                }

    //    //                string WeekDay = sSlotStartDay; // dtSelectedDate.ToString("ddd");

    //    //                tclocal.Attributes.Add("onclick", "setAletrnateSlot('" + CapacityWarning + "','" + hdnAvailableSlotTimeID.Value + "','" + hdnAvailableSiteDoorNumberID.Value + "','" + hdnAvailableSiteDoorNumber.Value + "','" + hdnAvailableFixedSlotID.Value + "','" + ltTime.Text + "','" + WeekDay + "');");
    //    //                //isAvailable = true;

    //    //                //For Display current vendor name-------                        
    //    //                if (!tempVendorName.Contains(sVendorCarrierName)) {
    //    //                    if (tempVendorName != string.Empty) {
    //    //                        if (sVendorCarrierName != tempVendorName) {
    //    //                            tempVendorName = sVendorCarrierName + " / " + tempVendorName;
    //    //                        }
    //    //                    }
    //    //                    else {
    //    //                        if (sVendorCarrierName != tempVendorName) {
    //    //                            tempVendorName = sVendorCarrierName;
    //    //                        }
    //    //                    }
    //    //                }
    //    //                sVendorCarrierName = tempVendorName;
    //    //                //---------------------------------------

    //    //                break;
    //    //            }
    //    //        }


    //    //        //if (hdnSuggestedSlotTimeID.Value == iTimeSlotID.ToString()) {
    //    //        //    if (hdnSuggestedSlotTime.Value != ltTime.Text.Trim()) {
    //    //        //        isAvailable = false;
    //    //        //    }
    //    //        //}

    //    //    }
    //    //}








    //}

    //private void SetCallHeight(int SlotLength, Color CellBackColor, string SlotStartTime)
    //{
    //    if (SlotLength >= 1)
    //    {
    //        string[] StartTime = SlotStartTime.Split(':');
    //        TimeSpan time1 = new TimeSpan(Convert.ToInt32(StartTime[0]), Convert.ToInt32(StartTime[1]), 00);
    //        TimeSpan time2 = new TimeSpan(00, iSlotTimeLength, 00);
    //        int iLen = 0;

    //        for (iLen = 1; iLen <= SlotLength; iLen++)
    //        {

    //            VendorDetails vd = new VendorDetails();

    //            if (time1.Hours == 23 && time1.Minutes == 55)
    //            {
    //                time1 = new TimeSpan(00, 00, 00);
    //            }
    //            else
    //            {
    //                time1 = time1.Add(time2);
    //            }
    //            vd.VendorName = sVendorCarrierName;
    //            if (hdnCurrentRole.Value == "Vendor")
    //            {
    //                vd.CellColor = Color.Black;
    //            }
    //            else
    //            {
    //                vd.CellColor = CellBackColor;
    //            }
    //            vd.isCurrentSlotFixed = isCurrentSlotFixed;

    //            if (SlotDoorCounter.ContainsKey(time1.ToString() + iSiteDoorID.ToString()))
    //            {
    //                SlotDoorCounter.Remove(time1.ToString() + iSiteDoorID.ToString());
    //            }



    //            SlotDoorCounter.Add(time1.ToString() + iSiteDoorID.ToString(), vd);
    //        }
    //    }
    //}

    //private string SetCallHeight(int SlotLength, ref TableCell tclocal, Color CellBackColor, string SlotStartTime)
    //{

    //    string VendorName = string.Empty;
    //    string temp = string.Empty;

    //    if (iTimeSlotID == 41)
    //        temp = string.Empty;

    //    if (SlotLength >= 1)
    //    {
    //        string[] StartTime = SlotStartTime.Split(':');
    //        TimeSpan time1 = new TimeSpan(Convert.ToInt32(StartTime[0]), Convert.ToInt32(StartTime[1]), 00);
    //        TimeSpan time2 = new TimeSpan(00, iSlotTimeLength, 00);
    //        int iLen = 0;


    //        for (iLen = 1; iLen <= SlotLength; iLen++)
    //        {

    //            VendorDetails vd = new VendorDetails();
    //            if (iLen == 1)
    //            {

    //                if (SlotDoorCounter.ContainsKey(time1.ToString() + iSiteDoorID.ToString()))
    //                {
    //                    vd = SlotDoorCounter[time1.ToString() + iSiteDoorID.ToString()];
    //                    if (vd.VendorName.Trim() != ltvendorName.Text)
    //                    {

    //                        VendorName = vd.VendorName;
    //                        isCurrentSlotFixed = vd.isCurrentSlotFixed;
    //                        SetCellBacknForeColor(ref tclocal, vd.CellColor);

    //                    }
    //                    else
    //                    {
    //                        if (isCurrentSlotFixed == false)
    //                        {
    //                            VendorName = vd.VendorName;
    //                            isCurrentSlotFixed = vd.isCurrentSlotFixed;
    //                            SetCellBacknForeColor(ref tclocal, vd.CellColor);
    //                        }
    //                        else
    //                        {
    //                            if (vd.CellColor == Color.GreenYellow)
    //                            {
    //                                VendorName = ltvendorName.Text;
    //                                SetCellBacknForeColor(ref tclocal, CellBackColor);
    //                            }
    //                            else
    //                            {
    //                                VendorName = vd.VendorName;
    //                                isCurrentSlotFixed = vd.isCurrentSlotFixed;
    //                                SetCellBacknForeColor(ref tclocal, vd.CellColor);
    //                            }
    //                        }
    //                    }
    //                }
    //                else
    //                {

    //                    vd.VendorName = sVendorCarrierName;
    //                    vd.CellColor = CellBackColor;
    //                    vd.isCurrentSlotFixed = isCurrentSlotFixed;


    //                    SlotDoorCounter.Add(time1.ToString() + iSiteDoorID.ToString(), vd);
    //                    SetCellBacknForeColor(ref tclocal, CellBackColor);
    //                }

    //                if (CellBackColor != Color.White)
    //                {
    //                    CreateDynamicDiv(ref tclocal, SlotLength, SlotStartTime);
    //                }

    //            }
    //            else
    //            {
    //                if (time1.Hours == 23 && time1.Minutes == 55)
    //                {
    //                    time1 = new TimeSpan(00, 00, 00);
    //                }
    //                else
    //                {
    //                    time1 = time1.Add(time2);
    //                }


    //                if (!SlotDoorCounter.ContainsKey(time1.ToString() + iSiteDoorID.ToString()))
    //                {
    //                    vd.VendorName = string.Empty; // sVendorCarrierName;

    //                    if (hdnCurrentRole.Value == "Vendor" && CellBackColor != Color.GreenYellow)
    //                    {
    //                        vd.CellColor = Color.Black;
    //                    }
    //                    else
    //                    {
    //                        vd.CellColor = CellBackColor;
    //                    }

    //                    vd.isCurrentSlotFixed = isCurrentSlotFixed;
    //                    vd.CurrentFixedSlotID = sCurrentFixedSlotID;



    //                    SlotDoorCounter.Add(time1.ToString() + iSiteDoorID.ToString(), vd);

    //                }
    //            }
    //        }
    //    }

    //    if (SlotLength >= 1 && NextRequiredSlot != string.Empty)
    //    {
    //        string[] StartTime = SlotStartTime.Split(':');
    //        TimeSpan time1 = new TimeSpan(Convert.ToInt32(StartTime[0]), Convert.ToInt32(StartTime[1]), 00);
    //        TimeSpan time2 = new TimeSpan(00, iSlotTimeLength, 00);


    //        int iLen = 0;
    //        //VendorDetails vd = new VendorDetails();
    //        for (iLen = 1; iLen <= SlotLength; iLen++)
    //        {
    //            if (GetFormatedTime(time1.Hours.ToString() + ":" + time1.Minutes.ToString()) != GetFormatedTime(NextRequiredSlot))
    //            {
    //                if (time1.Hours == 23 && time1.Minutes == 55)
    //                {
    //                    time1 = new TimeSpan(00, 00, 00);
    //                }
    //                else
    //                {
    //                    time1 = time1.Add(time2);
    //                }
    //            }
    //            else if (GetFormatedTime(time1.Hours.ToString() + ":" + time1.Minutes.ToString()) == GetFormatedTime(NextRequiredSlot))
    //            {



    //                SetCallHeightOverSpill(SlotLength - iLen + 1, Color.Orange, time1.Subtract(time2).ToString());


    //                break;
    //            }
    //        }
    //        NextRequiredSlot = string.Empty;
    //    }

    //    sCurrentFixedSlotID = "-1";
    //    return VendorName;
    //}

    //private void SetCallHeightOverSpill()
    //{
    //    if (iNextSlotLength >= 1)
    //    {
    //        string[] StartTime = NextRequiredSlot.Split(':');
    //        TimeSpan time1 = new TimeSpan(Convert.ToInt32(StartTime[0]), Convert.ToInt32(StartTime[1]), 00);
    //        TimeSpan time2 = new TimeSpan(00, iSlotTimeLength, 00);
    //        time1 = time1.Subtract(time2);
    //        int iLen = 0;

    //        for (iLen = 1; iLen <= iNextSlotLength; iLen++)
    //        {

    //            VendorDetails vd = new VendorDetails();

    //            if (time1.Hours == 23 && time1.Minutes == 55)
    //            {
    //                time1 = new TimeSpan(00, 00, 00);
    //            }
    //            else
    //            {
    //                time1 = time1.Add(time2);
    //            }


    //            if (iLen <= iNextSlotLength2)
    //                vd.VendorName = sVendorCarrierNameNextSlot + " / " + sVendorCarrierName;
    //            else
    //                vd.VendorName = sVendorCarrierName;



    //            if (hdnCurrentRole.Value == "Vendor")
    //            {
    //                vd.CellColor = Color.Black;
    //            }
    //            else
    //            {
    //                vd.CellColor = Color.Orange;
    //            }
    //            vd.isCurrentSlotFixed = isCurrentSlotFixed;

    //            if (SlotDoorCounter.ContainsKey(time1.ToString() + iSiteDoorID.ToString()))
    //            {
    //                SlotDoorCounter.Remove(time1.ToString() + iSiteDoorID.ToString());
    //            }


    //            SlotDoorCounter.Add(time1.ToString() + iSiteDoorID.ToString(), vd);
    //        }
    //    }
    //}

    //private void SetCallHeightOverSpill(int SlotLength, Color CellBackColor, string SlotStartTime)
    //{
    //    if (SlotLength >= 1)
    //    {

    //        int LoopCounter;

    //        if (isNextSlotCovered)
    //            LoopCounter = iNextSlotLength;
    //        else
    //            LoopCounter = SlotLength;

    //        string[] StartTime = SlotStartTime.Split(':');
    //        TimeSpan time1 = new TimeSpan(Convert.ToInt32(StartTime[0]), Convert.ToInt32(StartTime[1]), 00);
    //        TimeSpan time2 = new TimeSpan(00, iSlotTimeLength, 00);
    //        int iLen = 0;

    //        for (iLen = 1; iLen <= LoopCounter; iLen++)
    //        {
    //            VendorDetails vd = new VendorDetails();

    //            if (time1.Hours == 23 && time1.Minutes == 55)
    //            {
    //                time1 = new TimeSpan(00, 00, 00);
    //            }
    //            else
    //            {
    //                time1 = time1.Add(time2);
    //            }

    //            if (iLen <= iNextSlotLength2)
    //                vd.VendorName = sVendorCarrierNameNextSlot + " / " + sVendorCarrierName;
    //            else
    //                vd.VendorName = sVendorCarrierName;



    //            if (hdnCurrentRole.Value == "Vendor")
    //            {
    //                vd.CellColor = Color.Black;
    //            }
    //            else
    //            {
    //                vd.CellColor = CellBackColor;
    //            }
    //            vd.isCurrentSlotFixed = isCurrentSlotFixed;

    //            if (SlotDoorCounter.ContainsKey(time1.ToString() + iSiteDoorID.ToString()))
    //            {
    //                SlotDoorCounter.Remove(time1.ToString() + iSiteDoorID.ToString());
    //            }



    //            SlotDoorCounter.Add(time1.ToString() + iSiteDoorID.ToString(), vd);
    //        }
    //    }
    //}

    //private void SetCellBacknForeColor(ref TableCell tclocal, Color CellBackColor)
    //{
    //    string temp = string.Empty;

    //    if (CellBackColor == Color.Red)
    //    {
    //        tclocal.BackColor = System.Drawing.Color.White;
    //        tclocal.ForeColor = System.Drawing.Color.Black;
    //        tclocal.BorderColor = System.Drawing.Color.Red;
    //        tclocal.BorderWidth = 2;
    //        return;
    //    }
    //    else if (CellBackColor == Color.Purple)
    //    {
    //        tclocal.BackColor = System.Drawing.Color.Purple;
    //        tclocal.ForeColor = System.Drawing.Color.White;
    //        if (isCurrentSlotFixed)
    //        {
    //            tclocal.BorderColor = System.Drawing.Color.Red;
    //            tclocal.BorderWidth = 2;
    //        }
    //        else
    //        {
    //            tclocal.BorderColor = System.Drawing.Color.Black;
    //            tclocal.BorderStyle = BorderStyle.Dashed;
    //            tclocal.BorderWidth = 2;
    //        }

    //        return;
    //    }
    //    else if (CellBackColor == Color.Orange)
    //    {
    //        tclocal.BackColor = System.Drawing.Color.Orange;
    //        tclocal.ForeColor = System.Drawing.Color.Black;
    //        if (isCurrentSlotFixed)
    //        {
    //            tclocal.BorderColor = System.Drawing.Color.Red;
    //            tclocal.BorderWidth = 2;
    //        }
    //        return;
    //    }
    //    else if (CellBackColor == Color.GreenYellow)
    //    {
    //        tclocal.BackColor = System.Drawing.Color.GreenYellow;
    //        tclocal.ForeColor = System.Drawing.Color.Black;
    //        if (isCurrentSlotFixed)
    //        {
    //            tclocal.BorderColor = System.Drawing.Color.Red;
    //            tclocal.BorderWidth = 2;
    //        }
    //        return;
    //    }
    //    else if (CellBackColor == Color.Black)
    //    {
    //        tclocal.BackColor = System.Drawing.Color.Black;
    //        tclocal.ForeColor = System.Drawing.Color.White;
    //        tclocal.BorderColor = System.Drawing.Color.White;
    //        return;
    //    }
    //    else if (CellBackColor == Color.White)
    //    {
    //        tclocal.BackColor = System.Drawing.Color.White;
    //        tclocal.ForeColor = System.Drawing.Color.Black;
    //        tclocal.BorderColor = System.Drawing.Color.Black;
    //        tclocal.BorderWidth = 1;
    //        return;
    //    }
    //    else if (CellBackColor == Color.LightGray)
    //    {
    //        tclocal.BackColor = System.Drawing.Color.LightGray;
    //        tclocal.ForeColor = System.Drawing.Color.Black;
    //        tclocal.BorderColor = System.Drawing.Color.Black;
    //        if (isCurrentSlotFixed)
    //        {
    //            tclocal.BorderColor = System.Drawing.Color.Red;
    //            tclocal.BorderWidth = 2;
    //        }
    //        return;
    //    }
    //}


    //private void PaintAlternateScreenForVendor()
    //{

    //    ltSite.Text = ddlSite.innerControlddlSite.SelectedItem.Text;
    //    ltVendor_3.Text = ucSeacrhVendorName; // ((HiddenField)ucSeacrhVendor1.FindControl("hdnVandorName")).Value;

    //    ltCarrier_3.Text = ddlCarrier.SelectedItem.Text;
    //    ltOtherCarrier_3.Text = "[" + txtCarrierOther.Text.Trim() + "]";

    //    if (txtCarrierOther.Text.Trim() != string.Empty)
    //    {
    //        ltOtherCarrier_3.Visible = true;
    //    }
    //    else
    //    {
    //        ltOtherCarrier_3.Visible = false;
    //    }

    //    ltVehicleType_3.Text = ddlVehicleType.SelectedItem.Text;
    //    ltOtherVehicleType_3.Text = "[" + txtVehicleOther.Text.Trim() + "]";
    //    if (txtVehicleOther.Text.Trim() != string.Empty)
    //    {
    //        ltOtherVehicleType_3.Visible = true;
    //    }
    //    else
    //    {
    //        ltOtherVehicleType_3.Visible = false;
    //    }
    //    ltSchedulingDate_3.Text = ActualSchedulDateTime.ToString("dd/MM/yyyy");
    //    ltTimeSuggested_3.Text = ltSuggestedSlotTime.Text;


    //    ltExpectedLifts.Text = txtLifts.Text.Trim() != string.Empty ? txtLifts.Text.Trim() : "-";
    //    ltExpectedPallets.Text = txtPallets.Text.Trim() != string.Empty ? txtPallets.Text.Trim() : "-";
    //    ltExpectedLines.Text = txtExpectedLines.Text.Trim() != string.Empty ? txtExpectedLines.Text.Trim() : "-";
    //    ltExpectedCartons.Text = txtCartons.Text.Trim() != string.Empty ? txtCartons.Text.Trim() : "-";
    //}

    //private void GenerateAlternateSlotForVendor()
    //{

    //    int? TotalPallets = txtPallets.Text.Trim() != string.Empty ? Convert.ToInt32(txtPallets.Text.Trim()) : (int?)null;
    //    int? TotalCartons = txtCartons.Text.Trim() != string.Empty ? Convert.ToInt32(txtCartons.Text.Trim()) : (int?)null;
    //    int? TotalLines = txtExpectedLines.Text.Trim() != string.Empty ? Convert.ToInt32(txtExpectedLines.Text.Trim()) : (int?)null;

    //    actSlotLength = GetRequiredTimeSlotLength(TotalPallets, TotalCartons);

    //    #region ------------Basic Information--------------------------
    //    //-----------------Start Collect Basic Information --------------------------//
    //    DateTime dtSelectedDate = LogicalSchedulDateTime;
    //    string WeekDay = dtSelectedDate.ToString("dddd");

    //    DateTime dtMon = Utilities.Common.GetFirstDayOfWeek(dtSelectedDate);

    //    MASSIT_WeekSetupBE oMASSIT_WeekSetupBE = new MASSIT_WeekSetupBE();
    //    MASSIT_WeekSetupBAL oMASSIT_WeekSetupBAL = new MASSIT_WeekSetupBAL();


    //    oMASSIT_WeekSetupBE.Action = "GetSpecificWeek";
    //    oMASSIT_WeekSetupBE.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
    //    oMASSIT_WeekSetupBE.EndWeekday = WeekDay;
    //    oMASSIT_WeekSetupBE.ScheduleDate = LogicalSchedulDateTime;

    //    List<MASSIT_WeekSetupBE> lstWeekSetup = oMASSIT_WeekSetupBAL.GetWeekSetupDetailsBAL(oMASSIT_WeekSetupBE);

    //    if (lstWeekSetup == null || lstWeekSetup.Count <= 0 || lstWeekSetup[0].StartTime == null)
    //    {

    //        oMASSIT_WeekSetupBE.Action = "ShowAll";
    //        lstWeekSetup = oMASSIT_WeekSetupBAL.GetWeekSetupDetailsBAL(oMASSIT_WeekSetupBE);

    //        if (lstWeekSetup.Count <= 0)
    //        {
    //            string errorMeesage = WebCommon.getGlobalResourceValue("WeekSetupSettingNotDefined");
    //            ScriptManager.RegisterStartupScript(this, this.GetType(), "alert1", "alert('" + errorMeesage + "')", true);
    //            return;
    //        }
    //        else if (lstWeekSetup[0].StartTime == null)
    //        {
    //            string errorMeesage = WebCommon.getGlobalResourceValue("WeekSetupSettingNotDefined");
    //            ScriptManager.RegisterStartupScript(this, this.GetType(), "alert1", "alert('" + errorMeesage + "')", true);
    //            return;
    //        }
    //    }


    //    APPSIT_DoorOpenTimeBAL oMASSIT_DoorOpenTimeBAL = new APPSIT_DoorOpenTimeBAL();
    //    MASSIT_DoorOpenTimeBE oMASSIT_DoorOpenTimeBE = new MASSIT_DoorOpenTimeBE();



    //    DoorIDs = GetDoorConstraints();

    //    oMASSIT_DoorOpenTimeBE.Action = "GetDoorNoAndType";
    //    oMASSIT_DoorOpenTimeBE.DoorNo = new MASSIT_DoorNoSetupBE();
    //    oMASSIT_DoorOpenTimeBE.DoorNo.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);

    //    List<MASSIT_DoorOpenTimeBE> lstDoorNoType = oMASSIT_DoorOpenTimeBAL.GetDoorNoSetupDetailsBAL(oMASSIT_DoorOpenTimeBE);

    //    int iDoorCount = lstDoorNoType.Count;

    //    //decimal decCellWidth = (90 / (iDoorCount + 1));
    //    //double dblCellWidth = Convert.ToDouble(Math.Ceiling(decCellWidth));
    //    int dblCellWidth = 165;
    //    int i1stColumnWidth = 145;

    //    //-------------------------End Collect Basic Information ------------------------------//
    //    #endregion

    //    #region --------------Grid Header---------------
    //    //-------------Start Grid Header--------------//
    //    Table tblSlot = new Table();
    //    tblSlot.CellSpacing = 0;
    //    tblSlot.CellPadding = 0;
    //    tblSlot.CssClass = "FixedTables";
    //    tblSlot.ID = "Open_Text_General";
    //    tblSlot.Width = Unit.Pixel((dblCellWidth * iDoorCount) + i1stColumnWidth);

    //    //tableDiv_GeneralVendor.Controls.Add(tblSlot);

    //    TableRow trDoorNo = new TableRow();
    //    trDoorNo.TableSection = TableRowSection.TableHeader;
    //    TableHeaderCell tcDoorNo = new TableHeaderCell();
    //    tcDoorNo.Text = "Door Name / Door Type";  // +"<input type=image src='../../../Images/arrowleft.gif' style=height:16px; width:26px; />";
    //    tcDoorNo.BorderWidth = Unit.Pixel(1);
    //    tcDoorNo.Width = Unit.Pixel(i1stColumnWidth);
    //    trDoorNo.Cells.Add(tcDoorNo);

    //    TableRow trTime = new TableRow();
    //    trTime.TableSection = TableRowSection.TableHeader;
    //    TableHeaderCell tcTime = new TableHeaderCell();
    //    tcTime.Text = "Time";
    //    tcTime.BorderWidth = Unit.Pixel(1);
    //    tcTime.Width = Unit.Pixel(i1stColumnWidth);
    //    trTime.Cells.Add(tcTime);

    //    for (int iCount = 1; iCount <= iDoorCount; iCount++)
    //    {
    //        tcDoorNo = new TableHeaderCell();
    //        tcDoorNo.BorderWidth = Unit.Pixel(1);
    //        tcDoorNo.Text = lstDoorNoType[iCount - 1].DoorNo.DoorNumber.ToString() + " / " + lstDoorNoType[iCount - 1].DoorType.DoorType; ;
    //        tcDoorNo.Width = Unit.Pixel(dblCellWidth);
    //        trDoorNo.Cells.Add(tcDoorNo);

    //        tcTime = new TableHeaderCell();
    //        tcTime.BorderWidth = Unit.Pixel(1);
    //        tcTime.Text = "Vendor/Carrier";
    //        tcTime.Width = Unit.Pixel(dblCellWidth);
    //        trTime.Cells.Add(tcTime);
    //    }
    //    trDoorNo.BackColor = System.Drawing.Color.LightGray;
    //    tblSlot.Rows.Add(trDoorNo);

    //    trTime.BackColor = System.Drawing.Color.LightGray;
    //    tblSlot.Rows.Add(trTime);
    //    //-------------End Grid Header -------------//
    //    #endregion

    //    #region --------------Slot Ploting---------------
    //    //-------------Start Slot Ploting--------------//        

    //    //--------Find all Fixed Slots, Bookings For all Vendor/Carrier-------//
    //    APPBOK_BookingBE oAPPBOK_BookingBE = new APPBOK_BookingBE();
    //    APPBOK_BookingBAL oAPPBOK_BookingBAL = new APPBOK_BookingBAL();

    //    oAPPBOK_BookingBE.Action = "AllBookingFixedSlotDetails";
    //    oAPPBOK_BookingBE.SiteId = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
    //    oAPPBOK_BookingBE.ScheduleDate = LogicalSchedulDateTime;
    //    oAPPBOK_BookingBE.SelectedScheduleDate = LogicalSchedulDateTime;
    //    oAPPBOK_BookingBE.VehicleType = new MASSIT_VehicleTypeBE();
    //    //oAPPBOK_BookingBE.VehicleType.VehicleTypeID = Convert.ToInt32(ddlVehicleType.SelectedItem.Value);
    //    lstAllFixedDoor = oAPPBOK_BookingBAL.GetAllBookingFixedSlotDetailsBAL(oAPPBOK_BookingBE);
    //    //--------------------------------------//


    //    if (lstWeekSetup[0].StartWeekday != lstWeekSetup[0].EndWeekday)
    //    {
    //        oAPPBOK_BookingBE.Action = "AllBookingFixedSlotDetails";
    //        oAPPBOK_BookingBE.SiteId = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
    //        oAPPBOK_BookingBE.ScheduleDate = LogicalSchedulDateTime.AddDays(-1);
    //        List<APPBOK_BookingBE> lstAllFixedDoorStartWeek = oAPPBOK_BookingBAL.GetAllBookingFixedSlotDetailsBAL(oAPPBOK_BookingBE);

    //        //string StartTime = lstWeekSetup[0].StartTime.Value.Hour.ToString() + ":" + lstWeekSetup[0].StartTime.Value.Minute.ToString();

    //        //Filetr Non Time Delivery
    //        lstAllFixedDoorStartWeek = lstAllFixedDoorStartWeek.FindAll(delegate(APPBOK_BookingBE St)
    //        {
    //            return St.SlotTime.SlotTime != "";
    //        });

    //        if (lstAllFixedDoorStartWeek != null && lstAllFixedDoorStartWeek.Count > 0)
    //        {
    //            lstAllFixedDoorStartWeek = lstAllFixedDoorStartWeek.FindAll(delegate(APPBOK_BookingBE St)
    //            {
    //                DateTime? dt = (DateTime?)null;
    //                if (St.SlotTime.SlotTime != null && St.SlotTime.SlotTime != string.Empty)
    //                {
    //                    dt = new DateTime(1900, 1, 1, Convert.ToInt32(St.SlotTime.SlotTime.Split(':')[0]), Convert.ToInt32(St.SlotTime.SlotTime.Split(':')[1]), 0);
    //                }
    //                else
    //                {
    //                    dt = new DateTime(1900, 1, 1, 0, 0, 0);
    //                }

    //                return dt >= lstWeekSetup[0].StartTime;
    //            });

    //            if (lstAllFixedDoorStartWeek.Count > 0)
    //            {
    //                lstAllFixedDoor = MergeListCollections(lstAllFixedDoor, lstAllFixedDoorStartWeek);

    //            }
    //        }
    //    }


    //    SYS_SlotTimeBAL oSYS_SlotTimeBAL = new SYS_SlotTimeBAL();
    //    SYS_SlotTimeBE oSYS_SlotTimeBE = new SYS_SlotTimeBE();

    //    oSYS_SlotTimeBE.Action = "ShowAll";
    //    List<SYS_SlotTimeBE> lstSlotTime = oSYS_SlotTimeBAL.GetSlotTimeBAL(oSYS_SlotTimeBE);

    //    for (int jCount = 0; jCount < lstSlotTime.Count; jCount++)
    //    {
    //        string StartTime = lstWeekSetup[0].StartTime.Value.Hour.ToString() + "." + lstWeekSetup[0].StartTime.Value.Minute.ToString();
    //        if (lstSlotTime[jCount].SlotTime.Split(new char[] { ':' })[0] == "00")
    //        {
    //            StartTime = "24." + lstWeekSetup[0].StartTime.Value.Minute.ToString();
    //        }

    //        string EndTime = lstWeekSetup[0].EndTime.Value.Hour.ToString() + "." + lstWeekSetup[0].EndTime.Value.Minute.ToString();
    //        if (lstSlotTime[jCount].SlotTime.Split(new char[] { ':' })[0] == "00")
    //        {
    //            EndTime = "24." + lstWeekSetup[0].EndTime.Value.Minute.ToString();
    //        }

    //        decimal dSlotTime = Convert.ToDecimal(lstSlotTime[jCount].SlotTime.Replace(':', '.'));
    //        decimal dStartTime = Convert.ToDecimal(StartTime);
    //        decimal dEndTime = Convert.ToDecimal(EndTime);

    //        if (lstWeekSetup[0].StartWeekday != lstWeekSetup[0].EndWeekday)
    //        {
    //            dEndTime = Convert.ToDecimal("24.00");
    //        }

    //        if (dSlotTime >= dStartTime && dSlotTime <= dEndTime)
    //        {
    //            TableRow tr = new TableRow();
    //            tr.TableSection = TableRowSection.TableBody;
    //            TableCell tc = new TableCell();
    //            tc.BorderWidth = Unit.Pixel(1);

    //            if (lstSlotTime.Count - 1 > jCount)
    //                tc.Text = lstWeekSetup[0].StartWeekday + " " + GetFormatedTime(lstSlotTime[jCount].SlotTime) + " - " + GetFormatedTime(lstSlotTime[jCount + 1].SlotTime);
    //            else
    //                tc.Text = lstWeekSetup[0].StartWeekday + " " + GetFormatedTime(lstSlotTime[jCount].SlotTime) + " - " + GetFormatedTime(lstSlotTime[0].SlotTime);

    //            tc.Width = Unit.Pixel(i1stColumnWidth);
    //            tr.Cells.Add(tc);

    //            for (int iCount = 1; iCount <= iDoorCount; iCount++)
    //            {

    //                tc = new TableCell();
    //                tc.BorderWidth = Unit.Pixel(0);
    //                tc.Width = Unit.Pixel(dblCellWidth);

    //                iTimeSlotID = lstSlotTime[jCount].SlotTimeID;
    //                iSiteDoorID = lstDoorNoType[iCount - 1].SiteDoorNumberID;
    //                sTimeSlot = lstSlotTime[jCount].SlotTime;
    //                sDoorNo = lstDoorNoType[iCount - 1].DoorNo.DoorNumber;
    //                sSlotStartDay = lstWeekSetup[0].StartWeekday;

    //                //----Sprint 3b------//
    //                iTimeSlotOrderBYID = lstSlotTime[jCount].OrderBYID;
    //                //------------------//


    //                Literal ddl = SetCellColor(ref tc);
    //                tc.Controls.Add(ddl);
    //                tr.Cells.Add(tc);
    //            }
    //            tblSlot.Rows.Add(tr);
    //        }
    //    }

    //    if (lstWeekSetup[0].StartWeekday != lstWeekSetup[0].EndWeekday)
    //    {
    //        for (int jCount = 0; jCount < lstSlotTime.Count; jCount++)
    //        {
    //            string EndTime = lstWeekSetup[0].EndTime.Value.Hour.ToString() + "." + lstWeekSetup[0].EndTime.Value.Minute.ToString();
    //            if (lstSlotTime[jCount].SlotTime.Split(new char[] { ':' })[0] == "00")
    //            {
    //                EndTime = "24." + lstWeekSetup[0].EndTime.Value.Minute.ToString();
    //            }
    //            if (Convert.ToDecimal(lstSlotTime[jCount].SlotTime.Replace(':', '.')) <= Convert.ToDecimal(EndTime))
    //            {
    //                TableRow tr = new TableRow();
    //                tr.TableSection = TableRowSection.TableBody;
    //                TableCell tc = new TableCell();
    //                tc.BorderWidth = Unit.Pixel(1);

    //                if (lstSlotTime.Count - 1 > jCount)
    //                    tc.Text = lstWeekSetup[0].EndWeekday + " " + GetFormatedTime(lstSlotTime[jCount].SlotTime) + " - " + GetFormatedTime(lstSlotTime[jCount + 1].SlotTime);
    //                else
    //                    tc.Text = lstWeekSetup[0].EndWeekday + " " + GetFormatedTime(lstSlotTime[jCount].SlotTime) + " - " + GetFormatedTime(lstSlotTime[0].SlotTime);

    //                tc.Width = Unit.Pixel(i1stColumnWidth);
    //                tr.Cells.Add(tc);

    //                for (int iCount = 1; iCount <= iDoorCount; iCount++)
    //                {


    //                    tc = new TableCell();
    //                    tc.Width = Unit.Pixel(dblCellWidth);
    //                    tc.BorderWidth = Unit.Pixel(0);

    //                    iTimeSlotID = lstSlotTime[jCount].SlotTimeID;
    //                    iSiteDoorID = lstDoorNoType[iCount - 1].SiteDoorNumberID;
    //                    sTimeSlot = lstSlotTime[jCount].SlotTime;
    //                    sDoorNo = lstDoorNoType[iCount - 1].DoorNo.DoorNumber;
    //                    sSlotStartDay = lstWeekSetup[0].EndWeekday;

    //                    //----Sprint 3b------//
    //                    iTimeSlotOrderBYID = lstSlotTime[jCount].OrderBYID;
    //                    //------------------//

    //                    Literal ddl = SetCellColor(ref tc);
    //                    tc.Controls.Add(ddl);
    //                    tr.Cells.Add(tc);

    //                }
    //                tblSlot.Rows.Add(tr);

    //            }
    //        }
    //    }

    //    //-------------End Slot Ploting--------------//
    //    #endregion

    //    #region -------------Start Finding Insufficent Slot Time---------------------
    //    Color cBackColor, cBorderColor, cBackColorPreviousCell, cBorderColorPreviousCell;
    //    bool isNextSlot;
    //    bool isNewCell = false;
    //    cBorderColorPreviousCell = Color.Empty;
    //    string strCheckedHour = "-10";
    //    bool isValidTimeSlot = false;

    //    for (int iCell = 1; iCell <= iDoorCount; iCell++)
    //    {
    //        cBackColorPreviousCell = Color.Empty;
    //        for (int iRow = 0; iRow < tblSlot.Rows.Count; iRow++)
    //        {
    //            //FindNextSufficentSlotTime(ref tblSlot, iCell, iRow);                
    //            cBackColor = tblSlot.Rows[iRow].Cells[iCell].BackColor;
    //            cBorderColor = tblSlot.Rows[iRow].Cells[iCell].BorderColor;

    //            if (iRow == 15 && iCell == 1)
    //                //if (cBackColor == Color.GreenYellow)
    //                cBackColor = tblSlot.Rows[iRow].Cells[iCell].BackColor;

    //            if (cBackColor == Color.White && cBorderColor != Color.Red)
    //            {
    //                isNextSlot = true;
    //                for (int iLen = 0; iLen < actSlotLength; iLen++)
    //                {
    //                    if (iRow + iLen < tblSlot.Rows.Count)
    //                    {
    //                        cBackColor = tblSlot.Rows[iRow + iLen].Cells[iCell].BackColor;
    //                        if (cBackColor == Color.GreenYellow)
    //                        {
    //                            isNextSlot = true;
    //                            break;
    //                        }
    //                        if (cBackColor != Color.White)
    //                        {
    //                            isNextSlot = false;
    //                            break;
    //                        }
    //                    }
    //                    else
    //                    {
    //                        isNextSlot = false;
    //                        break;
    //                    }

    //                }
    //                if (isNextSlot == false)
    //                {
    //                    //tblSlot.Rows[iRow].Cells[iCell].BackColor = Color.LightGray;

    //                    if (hdnCurrentRole.Value == "Vendor")
    //                    {
    //                        tblSlot.Rows[iRow].Cells[iCell].BackColor = Color.Black;
    //                        tblSlot.Rows[iRow].Cells[iCell].BorderWidth = 0;
    //                    }
    //                    else
    //                    {
    //                        tblSlot.Rows[iRow].Cells[iCell].BackColor = Color.LightGray;
    //                        tblSlot.Rows[iRow].Cells[iCell].BorderWidth = 0;
    //                    }


    //                }
    //            }
    //            else if (cBackColor == Color.GreenYellow)
    //            {
    //                string abbr = tblSlot.Rows[iRow].Cells[iCell].Attributes["abbr"];
    //                if (abbr == null || abbr == string.Empty)
    //                {
    //                    if (cBorderColor != Color.Red)
    //                    {
    //                        tblSlot.Rows[iRow].Cells[iCell].BackColor = Color.White;
    //                        tblSlot.Rows[iRow].Cells[iCell].BorderWidth = 1;
    //                    }
    //                    else if (cBorderColor == Color.Red)
    //                    {
    //                        tblSlot.Rows[iRow].Cells[iCell].BackColor = Color.Black;
    //                        tblSlot.Rows[iRow].Cells[iCell].BorderWidth = 0;
    //                    }
    //                }
    //            }

    //            #region -- Blackout the slots which are not fullfill the delivery constraints-------//
    //            //Check delivery constraints// 
    //            string att = tblSlot.Rows[iRow].Cells[iCell].Attributes["onclick"];

    //            if (att != null && att != string.Empty && att.Contains("CheckAletrnateSlot"))
    //            {
    //                att = att.Replace("CheckAletrnateSlot(", "");
    //                att = att.Replace(");", "");
    //                string[] Arratt = att.Split(',');


    //                string att0 = Arratt[0].Replace("'", "");
    //                string att1 = Arratt[1].Replace("'", "");
    //                string att2 = Arratt[2].Replace("'", "");
    //                string att3 = Arratt[3].Replace("'", "");
    //                string att4 = Arratt[5].Replace("'", "");
    //                string att5 = Arratt[5].Replace("'", "");

    //                //--------- Sprint 3b----------//
    //                string att6 = Arratt[6].Replace("'", "");
    //                //-----------------------------//

    //                if (att3.Split(':')[0] != strCheckedHour)
    //                {
    //                    isValidTimeSlot = CheckValidSlot(att3, att5, Convert.ToString(TotalPallets), Convert.ToString(TotalLines));
    //                    strCheckedHour = att3.Split(':')[0];
    //                }

    //                //Check Before and after time//
    //                if (LogicalSchedulDateTime.ToString("ddd").ToLower() == att4.ToLower())
    //                {
    //                    if (Convert.ToInt32(att6) < Convert.ToInt32(BeforeSlotTimeID) || Convert.ToInt32(att6) > Convert.ToInt32(AfterSlotTimeID))
    //                    {
    //                        isValidTimeSlot = false;
    //                    }
    //                }
    //                //--------------------------//

    //            }
    //            else if (att != null && att != string.Empty && att.Contains("setAletrnateSlot"))
    //            {
    //                att = att.Replace("setAletrnateSlot(", "");
    //                att = att.Replace(");", "");
    //                string[] Arratt = att.Split(',');
    //                string att5 = Arratt[5].Replace("'", "");
    //                string att6 = Arratt[6].Replace("'", "");

    //                if (att5.Split(':')[0] != strCheckedHour)
    //                {
    //                    isValidTimeSlot = CheckValidSlot(att5, att6, Convert.ToString(TotalPallets), Convert.ToString(TotalLines));
    //                    strCheckedHour = att5.Split(':')[0];
    //                }


    //            }

    //            att = tblSlot.Rows[iRow].Cells[iCell].Attributes["onclick"];
    //            if (att != null && att != string.Empty && isValidTimeSlot == false)
    //            {

    //                if (tblSlot.Rows[iRow].Cells[iCell].BackColor == Color.White && tblSlot.Rows[iRow].Cells[iCell].BorderColor == Color.Red)
    //                {
    //                    tblSlot.Rows[iRow].Cells[iCell].ForeColor = Color.White;
    //                }

    //                string att4 = string.Empty;
    //                if (att.Contains("setAletrnateSlot"))
    //                {
    //                    att = att.Replace("setAletrnateSlot(", "");
    //                    att = att.Replace(");", "");
    //                    string[] Arratt = att.Split(',');
    //                    string att0 = Arratt[0].Replace("'", "");
    //                    string att1 = Arratt[1].Replace("'", "");
    //                    string att2 = Arratt[2].Replace("'", "");
    //                    string att3 = Arratt[3].Replace("'", "");
    //                    att4 = Arratt[4].Replace("'", "");  //fixed slot id
    //                    string att5 = Arratt[5].Replace("'", "");
    //                    string att6 = Arratt[6].Replace("'", "");



    //                }



    //                if (att4 == string.Empty || att4 == "-1")
    //                {
    //                    tblSlot.Rows[iRow].Cells[iCell].BackColor = Color.Black;
    //                    tblSlot.Rows[iRow].Cells[iCell].BorderColor = Color.Black;
    //                    tblSlot.Rows[iRow].Cells[iCell].BorderWidth = 1;

    //                    tblSlot.Rows[iRow].Cells[iCell].Attributes.Add("onmouseover", "this.style.cursor = 'default'");
    //                    tblSlot.Rows[iRow].Cells[iCell].Attributes.Add("onmouseout", "this.style.cursor = 'default'");
    //                    tblSlot.Rows[iRow].Cells[iCell].Attributes.Add("onclick", "");
    //                }
    //            }
    //            #endregion--------------------------------------------------------------------------//

    //            #region--Blackout the slots which are not matched with the seleted vehicle type-------//
    //            att = tblSlot.Rows[iRow].Cells[iCell].Attributes["onclick"];

    //            if (!strVehicleDoorTypeMatrix.Contains(lstDoorNoType[iCell - 1].DoorNo.SiteDoorTypeID.ToString() + ","))
    //            {
    //                if (tblSlot.Rows[iRow].Cells[iCell].BackColor == Color.White && tblSlot.Rows[iRow].Cells[iCell].BorderColor == Color.Red)
    //                {
    //                    tblSlot.Rows[iRow].Cells[iCell].ForeColor = Color.White;
    //                }


    //                string att4 = string.Empty;
    //                if (att != null && att != string.Empty && att.Contains("setAletrnateSlot"))
    //                {
    //                    att = att.Replace("setAletrnateSlot(", "");
    //                    att = att.Replace(");", "");
    //                    string[] Arratt = att.Split(',');
    //                    string att0 = Arratt[0].Replace("'", "");
    //                    string att1 = Arratt[1].Replace("'", "");
    //                    string att2 = Arratt[2].Replace("'", "");
    //                    string att3 = Arratt[3].Replace("'", "");
    //                    att4 = Arratt[4].Replace("'", "");  //fixed slot id
    //                    string att5 = Arratt[5].Replace("'", "");
    //                    string att6 = Arratt[6].Replace("'", "");



    //                }

    //                if (att4 == string.Empty || att4 == "-1")
    //                {
    //                    tblSlot.Rows[iRow].Cells[iCell].BackColor = Color.Black;
    //                    tblSlot.Rows[iRow].Cells[iCell].BorderColor = Color.Black;
    //                    tblSlot.Rows[iRow].Cells[iCell].BorderWidth = 1;

    //                    tblSlot.Rows[iRow].Cells[iCell].Attributes.Add("onmouseover", "this.style.cursor = 'default'");
    //                    tblSlot.Rows[iRow].Cells[iCell].Attributes.Add("onmouseout", "this.style.cursor = 'default'");
    //                    tblSlot.Rows[iRow].Cells[iCell].Attributes.Add("onclick", "");
    //                }
    //            }
    //            #endregion----------------------------------------------------------------------------//

    //            #region Row Spaning
    //            //-----------------------Row Spaning-------------//

    //            cBackColor = tblSlot.Rows[iRow].Cells[iCell].BackColor;
    //            cBorderColor = tblSlot.Rows[iRow].Cells[iCell].BorderColor;

    //            if (cBackColor != cBackColorPreviousCell || cBorderColor != cBorderColorPreviousCell)
    //            {
    //                isNewCell = true;
    //            }

    //            bool isCellCreated = false;
    //            if (cBackColor == cBackColorPreviousCell && cBackColor != Color.White)
    //            {

    //                isCellCreated = true;
    //                tblSlot.Rows[iRow].Cells[iCell].Style.Add("border-top-width", "0px");  // .BorderWidth = 0;
    //                tblSlot.Rows[iRow].Cells[iCell].Style.Add("border-bottom-width", "0px");

    //                if (cBorderColor == Color.Red || cBackColor == Color.Purple)
    //                {
    //                    tblSlot.Rows[iRow].Cells[iCell].Style.Add("border-left-width", "2px");
    //                    tblSlot.Rows[iRow].Cells[iCell].Style.Add("border-right-width", "2px");
    //                }
    //                else
    //                {
    //                    tblSlot.Rows[iRow].Cells[iCell].Style.Add("border-left-width", "1px");
    //                    tblSlot.Rows[iRow].Cells[iCell].Style.Add("border-right-width", "1px");
    //                }

    //                if (cBackColorPreviousCell == cBackColor && iRow > 0 && isNewCell == true)
    //                {
    //                    isNewCell = false;
    //                    tblSlot.Rows[iRow - 1].Cells[iCell].Style.Add("border-bottom-width", "0px");

    //                    if (cBorderColorPreviousCell == Color.Red || cBackColorPreviousCell == Color.Purple)
    //                    {
    //                        tblSlot.Rows[iRow - 1].Cells[iCell].Style.Add("border-top-width", "2px");
    //                        tblSlot.Rows[iRow - 1].Cells[iCell].Style.Add("border-left-width", "2px");
    //                        tblSlot.Rows[iRow - 1].Cells[iCell].Style.Add("border-right-width", "2px");
    //                    }
    //                    else
    //                    {
    //                        tblSlot.Rows[iRow - 1].Cells[iCell].Style.Add("border-top-width", "1px");
    //                        tblSlot.Rows[iRow - 1].Cells[iCell].Style.Add("border-left-width", "1px");
    //                        tblSlot.Rows[iRow - 1].Cells[iCell].Style.Add("border-right-width", "1px");
    //                    }
    //                }
    //            }
    //            else if (cBackColor == cBackColorPreviousCell && cBorderColor == Color.Red)
    //            {

    //                isCellCreated = true;

    //                tblSlot.Rows[iRow].Cells[iCell].Style.Add("border-top-width", "0px");  // .BorderWidth = 0;
    //                tblSlot.Rows[iRow].Cells[iCell].Style.Add("border-bottom-width", "0px");

    //                tblSlot.Rows[iRow].Cells[iCell].Style.Add("border-left-width", "2px");
    //                tblSlot.Rows[iRow].Cells[iCell].Style.Add("border-right-width", "2px");


    //                if (cBackColorPreviousCell == cBackColor && iRow > 0 && isNewCell == true)
    //                {
    //                    isNewCell = false;

    //                    if (cBorderColorPreviousCell != Color.Red)
    //                        tblSlot.Rows[iRow].Cells[iCell].Style.Add("border-top-width", "2px");

    //                    tblSlot.Rows[iRow - 1].Cells[iCell].Style.Add("border-bottom-width", "0px");
    //                    if (cBorderColorPreviousCell == Color.Red || cBackColorPreviousCell == Color.Purple)
    //                    {
    //                        tblSlot.Rows[iRow - 1].Cells[iCell].Style.Add("border-top-width", "2px");
    //                        tblSlot.Rows[iRow - 1].Cells[iCell].Style.Add("border-left-width", "2px");
    //                        tblSlot.Rows[iRow - 1].Cells[iCell].Style.Add("border-right-width", "2px");
    //                    }
    //                    else
    //                    {
    //                        tblSlot.Rows[iRow - 1].Cells[iCell].Style.Add("border-top-width", "1px");
    //                        tblSlot.Rows[iRow - 1].Cells[iCell].Style.Add("border-left-width", "1px");
    //                        tblSlot.Rows[iRow - 1].Cells[iCell].Style.Add("border-right-width", "1px");
    //                    }
    //                }
    //            }

    //            if (isCellCreated == false && iRow > 0)
    //            {
    //                if (cBorderColorPreviousCell == Color.Red || cBackColorPreviousCell == Color.Purple)
    //                    tblSlot.Rows[iRow - 1].Cells[iCell].Style.Add("border-bottom-width", "2px");
    //                else
    //                    tblSlot.Rows[iRow - 1].Cells[iCell].Style.Add("border-bottom-width", "1px");
    //            }

    //            string abbr1 = tblSlot.Rows[iRow].Cells[iCell].Attributes["abbr"];
    //            if (abbr1 != null && abbr1 != string.Empty)
    //            {
    //                if (cBorderColor == Color.Red || cBackColor == Color.Purple)
    //                {
    //                    tblSlot.Rows[iRow].Cells[iCell].Style.Add("border-top-width", "2px");
    //                }
    //                else
    //                {
    //                    tblSlot.Rows[iRow].Cells[iCell].Style.Add("border-top-width", "1px");
    //                }
    //            }

    //            cBackColorPreviousCell = cBackColor;
    //            cBorderColorPreviousCell = cBorderColor;

    //            //--------------------------------------------------//
    //            #endregion

    //            cBackColor = tblSlot.Rows[iRow].Cells[iCell].BackColor;
    //            if (cBackColor == Color.Black)
    //            {
    //                tblSlot.Rows[iRow].Cells[iCell].BorderWidth = 1;
    //                tblSlot.Rows[iRow].Cells[iCell].BorderColor = Color.Black;

    //                tblSlot.Rows[iRow].Cells[iCell].Attributes.Add("onmouseover", "this.style.cursor = 'default'");
    //                tblSlot.Rows[iRow].Cells[iCell].Attributes.Add("onmouseout", "this.style.cursor = 'default'");
    //                tblSlot.Rows[iRow].Cells[iCell].Attributes.Add("onclick", "");
    //            }
    //        }
    //    }

    //    #endregion

    //    #region Set Background color for Fixed slot
    //    bool GreenCellFound = false;
    //    for (int iRow = 0; iRow < tblSlot.Rows.Count; iRow++)
    //    {

    //        for (int iCell = 1; iCell <= iDoorCount; iCell++)
    //        {
    //            cBackColor = tblSlot.Rows[iRow].Cells[iCell].BackColor;
    //            cBorderColor = tblSlot.Rows[iRow].Cells[iCell].BorderColor;



    //            if (cBorderColor == Color.Red && (cBackColor == Color.White))
    //            {
    //                tblSlot.Rows[iRow].Cells[iCell].BackColor = Color.LightYellow;
    //                tblSlot.Rows[iRow].Cells[iCell].ForeColor = Color.Black;
    //            }

    //            if (cBorderColor == Color.Red && cBackColor == Color.GreenYellow && GreenCellFound == true)
    //            {
    //                tblSlot.Rows[iRow].Cells[iCell].BackColor = Color.LightYellow;
    //                tblSlot.Rows[iRow].Cells[iCell].ForeColor = Color.Black;
    //            }

    //            if (cBackColor == Color.GreenYellow || cBackColor == Color.Purple)
    //            {
    //                //tblSlot.Rows[iRow].Cells[iCell].Attributes.Add("onclick", "");
    //            }

    //            if (cBackColor == Color.GreenYellow)
    //                GreenCellFound = true;
    //        }
    //    }
    //    #endregion
    //}

    //protected void btnConfirmAlternateSlotVendor_Click(object sender, EventArgs e)
    //{
    //    if (hdnSuggestedWindowSlotID.Value != "-1")
    //    {
    //        ConfirmWindowBooking();
    //    }
    //    else if (hdnSuggestedSiteDoorNumber.Value == string.Empty)
    //    {
    //        MakeBooking(BookingType.NonTimedDelivery.ToString());
    //    }
    //    else
    //    {
    //        MakeBooking(BookingType.BookedSlot.ToString());
    //    }
    //}

    protected void hdnbutton_Click(object sender, EventArgs e)
    {


        SlotDoorCounter.Clear();
        SlotDoorCounter = new Dictionary<string, VendorDetails>();
        isProvisional = false;
        //if (hdnCurrentRole.Value != "Vendor")
        //{
        //    ltTimeSlot.Text = hdnSuggestedSlotTime.Value;
        //    ltDoorNo.Text = hdnSuggestedSiteDoorNumber.Value;
        //}
        //if (hdnCurrentRole.Value == "Vendor")
        //{
        //    ltTimeSlot_1.Text = hdnSuggestedSlotTime.Value;
        //    ltDoorNo_1.Text = hdnSuggestedSiteDoorNumber.Value;
        //}
        //if (mvBookingEdit.ActiveViewIndex == 4) // For OD Staff
        //{

        //    tableDiv_General.Controls.RemoveAt(0);
        //    GenerateAlternateSlot();
        //}
        //else if (mvBookingEdit.ActiveViewIndex == 5) // For Vendor
        //{
        //    tableDiv_GeneralVendor.Controls.RemoveAt(0);
        //    GenerateAlternateSlotForVendor();
        //}
    }

    protected void btnOutstandingPOContinue_Click(object sender, EventArgs e)
    {
        //ProceedBooking();
        CalculateProposedBookingTimeSlot();
        mvBookingEdit.ActiveViewIndex = 3;
    }

    protected void btnOutstandingPOBack_Click(object sender, EventArgs e)
    {
        mdlOutstandingPO.Hide();
    }


    // Sprint 1 - Point 1 - Start - Checking PO is existing or not.
    protected void btnPOContinue_Click(object sender, CommandEventArgs e)
    {
        this.AddPO(txtPurchaseNumber.Text, false, true);
        //txtPurchaseNumber.Text = string.Empty;
    }

    protected void btnPOBack_Click(object sender, CommandEventArgs e)
    {
        this.mdlPOConfirmMsg.Hide();
    }
    // Sprint 1 - Point 1 - Start - Checking PO is existing or not.


    // Sprint 1 - Point 7 - Start 
    protected void btnShowPOPopup_Click(object sender, EventArgs e)
    {
        ViewState["IsShowPOPopup"] = true;
        ViewState["dtShowPO"] = null;
        this.SortDirection = "ASC";
        GetAllPOs("PurchaseNumber");
        mdlShowPO.Show();
    }

    private void GetAllPOs(string SortExpression)
    {
        Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE = new Up_PurchaseOrderDetailBE();
        UP_PurchaseOrderDetailBAL oUP_PurchaseOrderDetailBAL = new UP_PurchaseOrderDetailBAL();

        oUp_PurchaseOrderDetailBE.Action = "GetAllPOsOfSiteAndVendor";
        oUp_PurchaseOrderDetailBE.Vendor = new UP_VendorBE();
        oUp_PurchaseOrderDetailBE.Vendor.VendorID = Convert.ToInt32(ucSeacrhVendorID);
        oUp_PurchaseOrderDetailBE.Site = new MAS_SiteBE();
        oUp_PurchaseOrderDetailBE.Site.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);

        List<Up_PurchaseOrderDetailBE> lstPO = oUP_PurchaseOrderDetailBAL.GetAllPODetailsOfSiteAndVendor(oUp_PurchaseOrderDetailBE);
        if (lstPO != null && lstPO.Count > 0)
        {
            FilterAllPO(lstPO);

            DataTable dtShowPO = null;

            if (ViewState["dtShowPO"] != null)
            {
                dtShowPO = (DataTable)ViewState["dtShowPO"];
            }

            if (dtShowPO != null && dtShowPO.Rows.Count > 0)
            {
                if (string.IsNullOrWhiteSpace(SortExpression))
                {
                    SortExpression = "PurchaseNumber";
                }
                if (string.IsNullOrWhiteSpace(SortDirection))
                {
                    SortDirection = "ASC";
                }
                dtShowPO.DefaultView.Sort = SortExpression + " " + SortDirection;

                grdShowPO.DataSource = dtShowPO;
                grdShowPO.DataBind();
            }
        }
    }

    private void FilterAllPO(List<Up_PurchaseOrderDetailBE> lstPOLocal)
    {
        if (lstPOLocal.Count > 0)
        {
            DateTime? Expected_date;
            int OutStandingLines;
            string Purchase_order;
            string VendorName;
            int BookingID;
            string BookingDeliveryDate = string.Empty;
            string Status = string.Empty;
            int ToleranceDueDay = 0;
            bool Allowed = true;
            string BackOrders = string.Empty;
            bool isNoConstraintsDefined = false;
            int BackordersCount = 0;
            int StockoutsCount = 0;
            int PurchaseOrderID = 0;
            DateTime? Original_due_date = null;
            string SiteName = string.Empty;

            Expected_date = lstPOLocal[0].Expected_date;
            Purchase_order = lstPOLocal[0].Purchase_order;
            VendorName = lstPOLocal[0].Vendor.VendorName;
            if (lstPOLocal[0].Site.ToleranceDueDay != null && lstPOLocal[0].Site.ToleranceDueDay != -1)
            {
                ToleranceDueDay = lstPOLocal[0].Site.ToleranceDueDay.Value;
            }

            if (lstPOLocal[0].Site.ToleranceDueDay == -1)
            {
                isNoConstraintsDefined = true;
            }

            BookingID = lstPOLocal[0].BookingID;
            if (lstPOLocal[0].BookingDeliveryDate != null)
            {
                BookingDeliveryDate = lstPOLocal[0].BookingDeliveryDate.Value.ToString("dd/MM/yyyy");
            }

            OutStandingLines = lstPOLocal[0].OutstandingLines;
            if (lstPOLocal[0].Backorders > 0)
            {
                BackOrders = "Y";
            }

            BackordersCount = lstPOLocal[0].Backorders;
            StockoutsCount = lstPOLocal[0].Stockouts;
            PurchaseOrderID = Convert.ToInt32(lstPOLocal[0].PurchaseOrderID);
            Original_due_date = lstPOLocal[0].Original_due_date;
            SiteName = lstPOLocal[0].Site.SiteName;

            //List<Up_PurchaseOrderDetailBE> lstPOTemp = lstPOLocal.FindAll(delegate(Up_PurchaseOrderDetailBE PO) {
            //    return PO.Purchase_order == Purchase_order;
            //}
            //);

            //OutStandingLines = lstPOTemp.Count;


            //lstPOTemp = lstPOLocal.FindAll(delegate(Up_PurchaseOrderDetailBE PO) {
            //    return PO.Expected_date == Expected_date && PO.Purchase_order == Purchase_order;
            //}
            //);

            //// Calculate Back Orders
            //Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE = new Up_PurchaseOrderDetailBE();
            //UP_PurchaseOrderDetailBAL oUP_PurchaseOrderDetailBAL = new UP_PurchaseOrderDetailBAL();
            //oUp_PurchaseOrderDetailBE.Action = "GetStockDetails";
            //oUp_PurchaseOrderDetailBE.Purchase_order = Purchase_order;
            //oUp_PurchaseOrderDetailBE.Site = new MAS_SiteBE();
            //oUp_PurchaseOrderDetailBE.Site.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);

            //List<Up_PurchaseOrderDetailBE> lstPOStock = oUP_PurchaseOrderDetailBAL.GetPriorityBAL(oUp_PurchaseOrderDetailBE);
            //if (lstPOStock != null && lstPOStock.Count > 0) {
            //    // lstPOTemp = lstPOStock.FindAll(delegate(Up_PurchaseOrderDetailBE p) { return Convert.ToDecimal(p.SKU.qty_on_backorder) > 0 && (Convert.ToInt32(Convert.ToDecimal(p.Original_quantity)) - p.Qty_receipted > 0); });
            //    lstPOTemp = lstPOStock.FindAll(delegate(Up_PurchaseOrderDetailBE p) {
            //        return (Convert.ToDecimal(p.SKU.Qty_On_Hand) - Convert.ToDecimal(p.SKU.qty_on_backorder) < 0);
            //    }
            //    );
            //    if (lstPOTemp != null && lstPOTemp.Count > 0) {
            //        BackOrders = "Y";
            //    }
            //}

            lstPOLocal = lstPOLocal.FindAll(delegate (Up_PurchaseOrderDetailBE PO)
            {
                return PO.Purchase_order != Purchase_order;
            }
            );

            //Add to Temp Table
            DataTable dtShowPO = null;

            if (ViewState["dtShowPO"] != null)
            {
                dtShowPO = (DataTable)ViewState["dtShowPO"];
            }
            else
            {

                dtShowPO = new DataTable();

                DataColumn myDataColumn = new DataColumn();
                myDataColumn.ColumnName = "Allowed";
                dtShowPO.Columns.Add(myDataColumn);

                myDataColumn = new DataColumn();
                myDataColumn.ColumnName = "PurchaseNumber";
                dtShowPO.Columns.Add(myDataColumn);

                myDataColumn = new DataColumn();
                myDataColumn.ColumnName = "Status";
                dtShowPO.Columns.Add(myDataColumn);

                myDataColumn = new DataColumn();
                myDataColumn.ColumnName = "VendorName";
                dtShowPO.Columns.Add(myDataColumn);

                myDataColumn = new DataColumn();
                myDataColumn.ColumnName = "OutstandingLines";
                dtShowPO.Columns.Add(myDataColumn);

                myDataColumn = new DataColumn();
                myDataColumn.ColumnName = "BackOrders";
                dtShowPO.Columns.Add(myDataColumn);

                myDataColumn = new DataColumn();
                myDataColumn.DataType = typeof(DateTime);
                myDataColumn.ColumnName = "Expected_date";
                dtShowPO.Columns.Add(myDataColumn);

                myDataColumn = new DataColumn();
                myDataColumn.ColumnName = "BookingID";
                dtShowPO.Columns.Add(myDataColumn);

                myDataColumn = new DataColumn();
                myDataColumn.ColumnName = "BookingDeliveryDate";
                dtShowPO.Columns.Add(myDataColumn);

                myDataColumn = new DataColumn();
                myDataColumn.ColumnName = "StockoutsCount";
                dtShowPO.Columns.Add(myDataColumn);

                myDataColumn = new DataColumn();
                myDataColumn.ColumnName = "BackOrdersCount";
                dtShowPO.Columns.Add(myDataColumn);

                myDataColumn = new DataColumn();
                myDataColumn.ColumnName = "PurchaseOrderID";
                dtShowPO.Columns.Add(myDataColumn);

                myDataColumn = new DataColumn();
                myDataColumn.ColumnName = "Original_due_date";
                dtShowPO.Columns.Add(myDataColumn);

                myDataColumn = new DataColumn();
                myDataColumn.ColumnName = "Sitename";
                dtShowPO.Columns.Add(myDataColumn);
            }

            //-----------------for weekends---//
            int DaysDiff = 0;
            if (Expected_date.Value >= LogicalSchedulDateTime)
            {
                DaysDiff = Enumerable.Range(0, Convert.ToInt32(Expected_date.Value.Subtract(LogicalSchedulDateTime).TotalDays)).Select(i => new[] { DayOfWeek.Saturday, DayOfWeek.Sunday }.Contains(LogicalSchedulDateTime.AddDays(i).DayOfWeek) ? 1 : 0).Sum();
            }
            ToleranceDueDay += DaysDiff;
            //-----------------------------//

            if (OutStandingLines > 0)
            {
                DataRow dataRow = dtShowPO.NewRow();
                if (BookingID > 0)
                {
                    Status = "BOOKED";
                    //if ((hdnCurrentRole.Value == "Vendor" || hdnCurrentRole.Value == "Carrier")
                    //   && LogicalSchedulDateTime != Expected_date.Value
                    //   && Expected_date.Value.Subtract(DateTime.Now.Date.AddDays(ToleranceDueDay)).Days > 0) {                        
                    //    Allowed = false;
                    //}
                    if (hdnCurrentRole.Value == "Vendor" || hdnCurrentRole.Value == "Carrier")
                    {
                        if (Expected_date.Value.Subtract(LogicalSchedulDateTime.AddDays(ToleranceDueDay)).Days > 0)
                        {
                            Allowed = false;
                        }
                    }
                }
                else
                {
                    if (Expected_date.Value.Subtract(DateTime.Now.Date).Days < 0)
                    {
                        Status = "OVERDUE";
                    }
                    else if (Expected_date.Value.Subtract(LogicalSchedulDateTime.AddDays(ToleranceDueDay)).Days < 0)
                    {
                        Status = "ACCEPTABLE";
                    }
                    else if (Expected_date.Value.Subtract(LogicalSchedulDateTime.AddDays(ToleranceDueDay)).Days == 0)
                    {
                        Status = "DUE";
                    }
                    else
                    {
                        if (isNoConstraintsDefined == false)
                        {
                            Status = "NOT AVAILABLE";
                            if (hdnCurrentRole.Value == "Vendor" || hdnCurrentRole.Value == "Carrier")
                            {
                                Allowed = false;
                            }
                        }
                        else
                        {
                            Status = "ACCEPTABLE";
                        }
                    }
                }

                dataRow["Allowed"] = Allowed;
                dataRow["PurchaseNumber"] = Purchase_order;
                dataRow["Status"] = Status;
                dataRow["VendorName"] = VendorName;
                dataRow["OutstandingLines"] = OutStandingLines;
                dataRow["BackOrders"] = BackOrders;
                dataRow["Expected_date"] = Expected_date.Value;
                dataRow["BookingID"] = BookingID;
                dataRow["BookingDeliveryDate"] = BookingDeliveryDate;
                dataRow["StockoutsCount"] = StockoutsCount;
                dataRow["BackOrdersCount"] = BackordersCount;
                dataRow["PurchaseOrderID"] = PurchaseOrderID;
                dataRow["Original_due_date"] = Original_due_date;
                dataRow["Sitename"] = SiteName;
                dtShowPO.Rows.Add(dataRow);
            }

            if (dtShowPO != null && dtShowPO.Rows.Count > 0)
            {
                ViewState["dtShowPO"] = dtShowPO;
            }
            else
            {
                ViewState["dtShowPO"] = null;
            }

            FilterAllPO(lstPOLocal);
        }
    }

    protected void grdShowPO_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            if (!Convert.ToBoolean(((System.Data.DataRowView)(e.Row.DataItem)).Row.ItemArray[0]))
            {
                CheckBox chkPO = (CheckBox)e.Row.FindControl("chkPO");
                chkPO.Style.Add("display", "none");
            }

            Literal ltbookingStatus = (Literal)e.Row.FindControl("ltbookingStatus");
            ltbookingStatus.Text = WebCommon.getGlobalResourceValue(ltbookingStatus.Text.Replace(" ", ""));

            if (((System.Data.DataRowView)(e.Row.DataItem)).Row.ItemArray[2].Equals("NOT AVAILABLE"))
                e.Row.Style.Add("color", "gray");
        }


    }

    protected void btnAddPOToBooking_Click(object sender, EventArgs e)
    {
        AlreadyExistsPOs = string.Empty;
        foreach (GridViewRow grv in grdShowPO.Rows)
        {
            CheckBox chkPO = (CheckBox)grv.FindControl("chkPO");
            if (chkPO.Checked)
            {
                HiddenField hdnPONumber = (HiddenField)grv.FindControl("hdnPONumber");
                HiddenField hdnStockouts = (HiddenField)grv.FindControl("hdnStockouts");
                HiddenField hdnBackorders = (HiddenField)grv.FindControl("hdnBackorders");
                HiddenField hdnPurchaseOrderID = (HiddenField)grv.FindControl("hdnPurchaseOrderID");
                HiddenField hdnExpected_date = (HiddenField)grv.FindControl("hdnExpected_date");
                HiddenField hdnOriginal_due_date = (HiddenField)grv.FindControl("hdnOriginal_due_date");
                HiddenField hdnSitename = (HiddenField)grv.FindControl("hdnSitename");
                string Vendorname = grv.Cells[3].Text;
                string OutstandingLines = grv.Cells[4].Text;
                AddPOFromPopup(hdnPONumber.Value, hdnStockouts.Value, hdnBackorders.Value, Convert.ToDateTime(hdnOriginal_due_date.Value), Convert.ToDateTime(hdnExpected_date.Value), Vendorname, hdnSitename.Value, hdnPurchaseOrderID.Value, OutstandingLines);
                //this.AddPO(hdnPONumber.Value, true);
            }
        }

        AlreadyExistsPOs = AlreadyExistsPOs.TrimEnd(new char[] { ',', ' ' });
        if (AlreadyExistsPOs != string.Empty)
        {
            string errMsg = string.Empty;
            int LastCommaIndex = AlreadyExistsPOs.LastIndexOf(',');
            if (LastCommaIndex > 0)
            {
                AlreadyExistsPOs = AlreadyExistsPOs.Insert(LastCommaIndex, " and").Remove(LastCommaIndex + 4, 1);
                errMsg = String.Format("Purchase Orders {0} already exist.", AlreadyExistsPOs);
            }
            else
            {
                errMsg = String.Format("Purchase Order {0} already exists.", AlreadyExistsPOs);
            }

            ScriptManager.RegisterStartupScript(this, this.GetType(), "popup", "<script>alert('" + errMsg + "');</script>", false);
        }
    }

    protected void btnCancel_1_Click(object sender, EventArgs e)
    {
        mdlShowPO.Hide();
        return;
    }

    private void AddPOFromPopup(string PurchaseNumber, string SO, string BO, DateTime Original_due_date, DateTime Expected_date, string VendorName, string SiteName, string PurchaseOrderID, string OutstandingLines)
    {
        string errMsg = string.Empty;

        string messageSeparator = ", ";
        DataTable myDataTable = null;
        if (PurchaseNumber != string.Empty)
        {

            if (ViewState["myDataTable"] != null)
            {
                myDataTable = (DataTable)ViewState["myDataTable"];

                //Check Item alrady added
                DataRow[] drr = myDataTable.Select("PurchaseNumber='" + PurchaseNumber.Trim() + "'");

                if (drr.Length > 0)
                {
                    // otherwise saved PO number in view state.                    
                    AlreadyExistsPOs += PurchaseNumber.Trim() + messageSeparator;
                    return;
                }
            }
            else
            {

                myDataTable = new DataTable();

                DataColumn auto = new DataColumn("AutoID", typeof(System.Int32));
                // specify it as auto increment field
                auto.AutoIncrement = true;
                auto.AutoIncrementSeed = 1;
                auto.ReadOnly = true;
                auto.Unique = true;
                myDataTable.Columns.Add(auto);

                DataColumn myDataColumn = new DataColumn();
                myDataColumn.ColumnName = "PurchaseNumber";
                myDataTable.Columns.Add(myDataColumn);

                myDataColumn = new DataColumn();
                myDataColumn.ColumnName = "Original_due_date";
                myDataTable.Columns.Add(myDataColumn);

                myDataColumn = new DataColumn();
                myDataColumn.ColumnName = "VendorName";
                myDataTable.Columns.Add(myDataColumn);

                myDataColumn = new DataColumn();
                myDataColumn.ColumnName = "OutstandingLines";
                myDataTable.Columns.Add(myDataColumn);

                myDataColumn = new DataColumn();
                myDataColumn.ColumnName = "Qty_On_Hand";
                myDataTable.Columns.Add(myDataColumn);

                myDataColumn = new DataColumn();
                myDataColumn.ColumnName = "qty_on_backorder";
                myDataTable.Columns.Add(myDataColumn);

                myDataColumn = new DataColumn();
                myDataColumn.ColumnName = "SiteName";
                myDataTable.Columns.Add(myDataColumn);

                myDataColumn = new DataColumn();
                myDataColumn.ColumnName = "PurchaseOrderID";
                myDataTable.Columns.Add(myDataColumn);

                // Sprint 1 - Point 14 - Begin - Adding [Expected Date]
                myDataColumn = new DataColumn();
                myDataColumn.ColumnName = "ExpectedDate";
                myDataTable.Columns.Add(myDataColumn);
                // Sprint 1 - Point 14 - End - Adding [Expected Date]

                myDataColumn = new DataColumn();
                myDataColumn.ColumnName = "IsProvisionResonChangesUpdate";
                myDataTable.Columns.Add(myDataColumn);
            }

            DataRow dataRow = myDataTable.NewRow();
            dataRow["Qty_On_Hand"] = SO;
            dataRow["IsProvisionResonChangesUpdate"] = false;
            dataRow["qty_on_backorder"] = BO;



            dataRow["PurchaseNumber"] = PurchaseNumber;
            dataRow["Original_due_date"] = Original_due_date.ToString("dd/MM/yyyy");

            if (Expected_date != null)
            {
                dataRow["ExpectedDate"] = Expected_date.ToString("dd/MM/yyyy");
            }
            else
            {
                dataRow["ExpectedDate"] = Original_due_date.ToString("dd/MM/yyyy");
            }

            dataRow["VendorName"] = VendorName;
            dataRow["OutstandingLines"] = OutstandingLines;

            dataRow["SiteName"] = SiteName;
            dataRow["PurchaseOrderID"] = PurchaseOrderID.ToString();

            myDataTable.Rows.Add(dataRow);
        }


        if (myDataTable != null && myDataTable.Rows.Count > 0)
        {
            gvPO.DataSource = myDataTable;
            gvPO.DataBind();
        }

        if (myDataTable != null && myDataTable.Rows.Count > 0)
        {
            ScriptManager.RegisterStartupScript(Page, this.GetType(), "btnshow", "$get('" + btnProceedBooking.ClientID + "').style.display='';", true);
            ViewState["myDataTable"] = myDataTable;
        }
        else
        {
            ScriptManager.RegisterStartupScript(Page, this.GetType(), "btnhide", "$get('" + btnProceedBooking.ClientID + "').style.display='none';", true);
            ViewState["myDataTable"] = null;
        }

        int TotalLines = 0;
        foreach (DataRow dr in myDataTable.Rows)
        {
            if (dr["OutstandingLines"].ToString() != string.Empty)
                TotalLines += Convert.ToInt32(dr["OutstandingLines"]);
        }

        ltExpectedLines_1.Text = WebCommon.getGlobalResourceValue("ExpectedLines").Replace("XX", TotalLines.ToString()); //lblExpectedLines.Text.Replace(Lines, TotalLines.ToString());

        Lines = TotalLines.ToString();
    }

    protected void grdShowPO_Sorting(object sender, GridViewSortEventArgs e)
    {
        ViewState["dtShowPO"] = null;
        GetAllPOs(e.SortExpression);
        mdlShowPO.Show();
        if (SortDirection == "ASC")
        {
            SortDirection = "DESC";
        }
        else
        {
            SortDirection = "ASC";
        }
    }

    // Sprint 1 - Point 7 - End 

    private MAS_SiteBE GetSingleSiteSetting(int siteId)
    {
        MAS_SiteBAL oMAS_SiteBAL = new MAS_SiteBAL();
        MAS_SiteBE oMAS_SiteBE = new MAS_SiteBE();
        oMAS_SiteBE.Action = "GetSingleSiteSetting";
        oMAS_SiteBE.SiteID = siteId;
        MAS_SiteBE localMAS_SiteBE = oMAS_SiteBAL.GetSingleSiteSettingBAL(oMAS_SiteBE);
        return localMAS_SiteBE;
    }

    protected void btnWindowAlternateSlot_Click(object sender, EventArgs e)
    {
        BindDoorNumber();
        BindStartEndTime();
        BindAlternateWindow();
        if (Convert.ToBoolean(IsNonStandardWindowSlot))
        {
            rdoNonStandardWindow.Checked = true;
            rdoNonStandardWindow_CheckedChanged(this, EventArgs.Empty);
        }
        else
        {
            if ((hdnSuggestedWindowSlotID.Value != "" || hdnSuggestedWindowSlotID.Value != "-1") && ddlAlternateWindow.Items.Count > 0)
            {
                if (ddlAlternateWindow.Items.FindByValue(hdnSuggestedWindowSlotID.Value) != null)
                {
                    ddlAlternateWindow.SelectedValue = hdnSuggestedWindowSlotID.Value;
                    ltTimeSuggested_2.Text = ltBookingDate.Text + " " + ltSuggestedSlotTime.Text;
                }
            }
            rdoStandardWindow.Checked = true;
            rdoStandardWindow_CheckedChanged(this, EventArgs.Empty);
        }
        mvBookingEdit.ActiveViewIndex = 4;
    }


    private void BindAlternateWindow()
    {
        string TotalPallets = txtPallets.Text.Trim() != string.Empty ? txtPallets.Text.Trim() : "0";
        string TotalCartons = txtCartons.Text.Trim() != string.Empty ? txtCartons.Text.Trim() : "0";
        string TotalLifts = txtLifts.Text.Trim() != string.Empty ? txtLifts.Text.Trim() : "0";
        string TotalLines = txtExpectedLines.Text.Trim() != string.Empty ? txtExpectedLines.Text.Trim() : "0";

        MASSIT_TimeWindowBE objMASSIT_TimeWindowBE = new MASSIT_TimeWindowBE();

        objMASSIT_TimeWindowBE.Action = "GetAlternateWindow";
        objMASSIT_TimeWindowBE.ExcludedVendorIDs = Convert.ToString(ucSeacrhVendorID);
        objMASSIT_TimeWindowBE.ExcludedCarrierIDs = Convert.ToString(ddlCarrier.SelectedItem.Value);
        objMASSIT_TimeWindowBE.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
        objMASSIT_TimeWindowBE.ScheduleDate = LogicalSchedulDateTime;
        objMASSIT_TimeWindowBE.MaximumPallets = Convert.ToInt32(TotalPallets);
        objMASSIT_TimeWindowBE.MaximumCartons = Convert.ToInt32(TotalCartons);
        objMASSIT_TimeWindowBE.MaximumLines = Convert.ToInt32(TotalLines);

        MASSIT_TimeWindowBAL oMASSIT_TimeWindowBAL = new MASSIT_TimeWindowBAL();
        List<MASSIT_TimeWindowBE> lstAlternateSlots = oMASSIT_TimeWindowBAL.GetAlternateWindowBAL(objMASSIT_TimeWindowBE);
        if (lstAlternateSlots.Count > 0)
        {
            FillControls.FillDropDown(ref ddlAlternateWindow, lstAlternateSlots, "WindowName", "TimeWindowID", "--Select--");
        }
        else
        {
            ddlAlternateWindow.Items.Clear();
            ddlAlternateWindow.Items.Add(new ListItem("--Select--", "0"));
        }
    }

    protected void ddlAlternateWindow_SelectedIndexChanged(object sender, EventArgs e)
    {
        hdnSuggestedWindowSlotID.Value = string.Empty;
        hdnSuggestedSiteDoorNumberID.Value = string.Empty;
        hdnSuggestedSiteDoorNumber.Value = string.Empty;
        hdnSuggestedWeekDay.Value = string.Empty;
        ltTimeSuggested_2.Text = string.Empty;
        if (ddlAlternateWindow.SelectedValue != "0")
        {
            bool isWarning = false;
            string TotalPallets = txtPallets.Text.Trim() != string.Empty ? txtPallets.Text.Trim() : "0";
            string TotalCartons = txtCartons.Text.Trim() != string.Empty ? txtCartons.Text.Trim() : "0";
            string TotalLifts = txtLifts.Text.Trim() != string.Empty ? txtLifts.Text.Trim() : "0";
            string TotalLines = txtExpectedLines.Text.Trim() != string.Empty ? txtExpectedLines.Text.Trim() : "0";

            MASSIT_TimeWindowBE objMASSIT_TimeWindowBE = new MASSIT_TimeWindowBE();

            objMASSIT_TimeWindowBE.Action = "GetWindowDetail";
            objMASSIT_TimeWindowBE.ExcludedVendorIDs = Convert.ToString(ucSeacrhVendorID);
            objMASSIT_TimeWindowBE.ExcludedCarrierIDs = Convert.ToString(ddlCarrier.SelectedItem.Value);
            objMASSIT_TimeWindowBE.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
            objMASSIT_TimeWindowBE.ScheduleDate = LogicalSchedulDateTime;
            objMASSIT_TimeWindowBE.MaximumPallets = Convert.ToInt32(TotalPallets);
            objMASSIT_TimeWindowBE.MaximumCartons = Convert.ToInt32(TotalCartons);
            objMASSIT_TimeWindowBE.MaximumLines = Convert.ToInt32(TotalLines);
            objMASSIT_TimeWindowBE.TimeWindowID = Convert.ToInt32(ddlAlternateWindow.SelectedValue);

            MASSIT_TimeWindowBAL oMASSIT_TimeWindowBAL = new MASSIT_TimeWindowBAL();
            MASSIT_TimeWindowBE lstAlternateSlots = oMASSIT_TimeWindowBAL.GetWindowDetailBAL(objMASSIT_TimeWindowBE);
            if (lstAlternateSlots != null && lstAlternateSlots.TimeWindowID > 0)
                ltTimeSuggested_2.Text = lstAlternateSlots.StartTime + " - " + lstAlternateSlots.EndTime + " on " + LogicalSchedulDateTime.Day.ToString() + "/" + LogicalSchedulDateTime.Month.ToString() + "/" + LogicalSchedulDateTime.Year.ToString() + " " + AtDoorName + " " + lstAlternateSlots.DoorNumber.ToString();

            if (lstAlternateSlots != null && lstAlternateSlots.TimeWindowID > 0)
            {
                if (lstAlternateSlots.MaxVolumeError == "VE")
                {
                    string errMsg = WebCommon.getGlobalResourceValue("BK_WMS_04_INT"); //"The volume being booked in exceeds the scheduled capacity for this Window.
                    AddWarningMessages(errMsg, "BK_WMS_04_INT");
                    isWarning = true;
                }
                if (lstAlternateSlots.MaxDailyCapacityError == "DCE")
                {
                    string errMsg = WebCommon.getGlobalResourceValue("BK_WMS_01_INT"); //"There is no capacity for any further deliveries on the date selected.
                    AddWarningMessages(errMsg, "BK_WMS_01_INT");
                    isWarning = true;
                }
                if (lstAlternateSlots.MaxDailyDeliveryError == "DDE")
                {
                    string errMsg = WebCommon.getGlobalResourceValue("BK_WMS_02_INT"); //"Total time exceeds for any further deliveries on the date selected.
                    AddWarningMessages(errMsg, "BK_WMS_02_INT");
                    isWarning = true;
                }
                //To check the max time limit
                if (CheckMaxTimeConstraint(lstAlternateSlots))
                {
                    string errMsg = WebCommon.getGlobalResourceValue("BK_WMS_05_INT"); //"Maximum time exceeds the scheduled capacity for this Window.
                    AddWarningMessages(errMsg, "BK_WMS_05_INT");
                    isWarning = true;
                }
                //To check the Total time limit
                if (CheckTotalAvailableTimeConstraint(lstAlternateSlots))
                {
                    string errMsg = WebCommon.getGlobalResourceValue("BK_WMS_02_INT"); //"Total time exceeds for any further deliveries on the date selected.
                    AddWarningMessages(errMsg, "BK_WMS_02_INT");
                    isWarning = true;
                }
                hdnSuggestedWindowSlotID.Value = lstAlternateSlots.TimeWindowID.ToString();
                hdnSuggestedSiteDoorNumberID.Value = lstAlternateSlots.SiteDoorNumberID.ToString();
                hdnSuggestedSiteDoorNumber.Value = lstAlternateSlots.DoorNumber.ToString();
                hdnSuggestedWeekDay.Value = LogicalSchedulDateTime.ToString("ddd");
            }

            if (isWarning == true)
                ShowWarningMessages();
        }
    }

    protected void ConfirmAlternateSlot()
    {
        if (rdoStandardWindow.Checked)
        {
            //-----------Start Stage 9 point 2a/2b--------------//
            if (IsProvisionalBooking.Value == true && hdnCurrentMode.Value == "Edit")
            {
                if (hdnSuggestedWindowSlotID.Value == "-1" || hdnSuggestedWindowSlotID.Value == string.Empty || hdnSuggestedWindowSlotID.Value == "0")
                {
                    string errorMeesage = WebCommon.getGlobalResourceValue("confirmprovisionalbooking");
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "alert2", "alert('" + errorMeesage + "')", true);
                    return;
                }
            }
            //-----------End Stage 9 point 2a/2b--------------//

            ConfirmWindowBooking();
        }
        if (rdoNonStandardWindow.Checked)
        {
            string TotalPallets = txtPallets.Text.Trim() != string.Empty ? txtPallets.Text.Trim() : "0";
            string TotalCartons = txtCartons.Text.Trim() != string.Empty ? txtCartons.Text.Trim() : "0";
            string TotalLifts = txtLifts.Text.Trim() != string.Empty ? txtLifts.Text.Trim() : "0";
            string TotalLines = txtExpectedLines.Text.Trim() != string.Empty ? txtExpectedLines.Text.Trim() : "0";

            MASSIT_TimeWindowBE objMASSIT_TimeWindowBE = new MASSIT_TimeWindowBE();

            objMASSIT_TimeWindowBE.Action = "GetWeekSetup";
            objMASSIT_TimeWindowBE.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
            objMASSIT_TimeWindowBE.ScheduleDate = LogicalSchedulDateTime;
            objMASSIT_TimeWindowBE.MaximumPallets = Convert.ToInt32(TotalPallets);
            objMASSIT_TimeWindowBE.MaximumCartons = Convert.ToInt32(TotalCartons);
            objMASSIT_TimeWindowBE.MaximumLines = Convert.ToInt32(TotalLines);
            objMASSIT_TimeWindowBE.MaximumLift = Convert.ToInt32(TotalLifts);

            MASSIT_TimeWindowBAL oMASSIT_TimeWindowBAL = new MASSIT_TimeWindowBAL();          

            if (!string.IsNullOrEmpty(oMASSIT_TimeWindowBAL.GetWeekSetupBAL(objMASSIT_TimeWindowBE)) && oMASSIT_TimeWindowBAL.GetWeekSetupBAL(objMASSIT_TimeWindowBE) == "WSE")
            {
                if (hdnCurrentRole.Value != "Vendor" || hdnCurrentRole.Value != "Carrier")
                {
                    string errMsg = WebCommon.getGlobalResourceValue("BK_WMS_06_EXT"); //  "Volume exceeds the scheduled capacity for Week setup Do you want to continue
                    AddWarningMessages(errMsg, "BK_WMS_06_EXT");                   
                    ShowWarningMessages();                   
                    return;
                }
                else
                {
                    string errMsg = WebCommon.getGlobalResourceValue("BK_WMS_06_INT"); // "Volume exceeds the scheduled capacity for Week setup.
                    ShowErrorMessage(errMsg, "BK_WMS_06_INT");
                    return;
                }
            }
            else if (Convert.ToInt16(ddlFrom.SelectedValue) > Convert.ToInt16(ddlTo.SelectedValue))
            {
                string errorMeesage = WebCommon.getGlobalResourceValue("IsStartTimeGreaterThanEndTime");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "popup", "<script>alert('" + errorMeesage + "');</script>", false);
                return;
            }
            else
            {
                hdnSuggestedWindowSlotID.Value = string.Empty;
                hdnSuggestedSiteDoorNumberID.Value = ddlDoorName.SelectedValue;
                hdnSuggestedSiteDoorNumber.Value = ddlDoorName.SelectedItem.Text;
                hdnSuggestedWeekDay.Value = LogicalSchedulDateTime.ToString("ddd");
                hdnStartTimeID.Value = ddlFrom.SelectedValue;
                hdnEndTimeID.Value = ddlTo.SelectedValue;

                //-----------Start Stage 9 point 2a/2b--------------//
                if (IsProvisionalBooking.Value == true && hdnCurrentMode.Value == "Edit")
                {
                    if (hdnSuggestedSiteDoorNumberID.Value == "-1" || hdnSuggestedSiteDoorNumberID.Value == string.Empty || hdnSuggestedSiteDoorNumberID.Value == "0")
                    {
                        string errorMeesage = WebCommon.getGlobalResourceValue("confirmprovisionalbooking");
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert2", "alert('" + errorMeesage + "')", true);
                        return;
                    }
                }
                //-----------End Stage 9 point 2a/2b--------------//

                ConfirmWindowBooking();
            }


        }
    }

    protected void btnConfirmAlternateSlot_Click(object sender, EventArgs e)
    {
        ConfirmAlternateSlot();
    }


    protected void rdoNonStandardWindow_CheckedChanged(object sender, EventArgs e)
    {
        if (rdoNonStandardWindow.Checked)
        {
            rfvAlternateWindowRequired.Enabled = false;
            cmpvStartTimeCanNotBeGreaterEndTime.Enabled = true;
            rfvFromRequired.Enabled = true;
            rfvToRequired.Enabled = true;
            rfvDoorNameRequired.Enabled = true;

            hdnSuggestedWindowSlotID.Value = string.Empty;
            hdnSuggestedSiteDoorNumberID.Value = string.Empty;
            hdnSuggestedSiteDoorNumber.Value = string.Empty;
            if (!string.IsNullOrEmpty(hdnPreservSlot.Value))
            {
                string[] ArrrayPreserveValue = hdnPreservSlot.Value.Split('|');

                if (ddlFrom.Items.FindByValue(ArrrayPreserveValue[0]) != null)
                    ddlFrom.SelectedValue = ArrrayPreserveValue[0];
                if (ddlTo.Items.FindByValue(ArrrayPreserveValue[1]) != null)
                    ddlTo.SelectedValue = ArrrayPreserveValue[1];
                if (ddlDoorName.Items.FindByValue(ArrrayPreserveValue[2]) != null)
                    ddlDoorName.SelectedValue = ArrrayPreserveValue[2];
            }
            if (ddlAlternateWindow.Items.Count > 0)
                ddlAlternateWindow.SelectedIndex = 0;
            ddlAlternateWindow.Enabled = false;
            ltTimeSuggested_2.Text = string.Empty;

            ddlFrom.Enabled = true;
            ddlTo.Enabled = true;
            ddlDoorName.Enabled = true;
        }
    }

    protected void rdoStandardWindow_CheckedChanged(object sender, EventArgs e)
    {
        if (rdoStandardWindow.Checked)
        {
            rfvAlternateWindowRequired.Enabled = true;
            cmpvStartTimeCanNotBeGreaterEndTime.Enabled = false;
            rfvFromRequired.Enabled = false;
            rfvToRequired.Enabled = false;
            rfvDoorNameRequired.Enabled = false;

            ddlAlternateWindow.Enabled = true;

            ddlFrom.Enabled = false;
            ddlTo.Enabled = false;
            ddlDoorName.Enabled = false;
            ddlFrom.SelectedIndex = 0;
            ddlTo.SelectedIndex = 0;
            ddlDoorName.SelectedIndex = 0;
        }
    }

    private void SendProvisionalRefusalLetterMail(List<APPBOK_BookingBE> lstBooking, int BookingID)
    {
        try
        {
            var oMASSIT_VendorBE = new MASSIT_VendorBE();
            var oAPPSIT_VendorBAL = new APPSIT_VendorBAL();
            string VendorEmails = string.Empty;
            List<MASSIT_VendorBE> lstVendorDetails = null;
            string[] sentTo;
            string[] language;
            string[] VendorData = new string[2];

            if (lstBooking[0].SupplierType == "C")
            {
                oMASSIT_VendorBE.Action = "GetDeletedCarriersVendor";
                oMASSIT_VendorBE.BookingID = BookingID;
                lstVendorDetails = oAPPSIT_VendorBAL.GetDeletedCarriersVendorIDBAL(oMASSIT_VendorBE);
                if (lstVendorDetails != null && lstVendorDetails.Count > 0)
                {
                    for (int i = 0; i < lstVendorDetails.Count; i++)
                    {
                        VendorData = CommonPage.GetVendorEmailsWithLanguage(lstVendorDetails[i].VendorID, Convert.ToInt32(lstBooking[0].SiteId));
                        if (VendorData.Length > 0 && !string.IsNullOrEmpty(VendorData[0]))
                        {
                            sentTo = VendorData[0].Split(',');
                            language = VendorData[1].Split(',');
                            for (int j = 0; j < sentTo.Length; j++)
                            {
                                if (sentTo[j].Trim() != "")
                                {
                                    SendMailToVendor(sentTo[j], language[j], lstBooking, BookingID);
                                }
                            }
                        }
                        else
                            SendMailToVendor(ConfigurationManager.AppSettings["DefaultEmailAddress"], "English", lstBooking, BookingID);

                    }
                }


                //Send mail to Carrier
                VendorData = CommonPage.GetCarrierDefaultEmailWithLanguage(lstBooking[0].Carrier.CarrierID);
                if (VendorData.Length > 0 && !string.IsNullOrEmpty(VendorData[0]))
                {
                    sentTo = VendorData[0].Split(',');
                    language = VendorData[1].Split(',');
                    for (int j = 0; j < sentTo.Length; j++)
                    {
                        if (sentTo[j].Trim() != "")
                        {
                            SendMailToVendor(sentTo[j], language[j], lstBooking, BookingID);
                        }
                    }
                }
                else
                    SendMailToVendor(ConfigurationManager.AppSettings["DefaultEmailAddress"], "English", lstBooking, BookingID);

                //----------------------//

            }
            else if (lstBooking[0].SupplierType == "V")
            {
                VendorData = CommonPage.GetVendorEmailsWithLanguage(lstBooking[0].VendorID, Convert.ToInt32(lstBooking[0].SiteId));
                if (VendorData.Length > 0 && !string.IsNullOrEmpty(VendorData[0]))
                {
                    sentTo = VendorData[0].Split(',');
                    language = VendorData[1].Split(',');
                    for (int j = 0; j < sentTo.Length; j++)
                    {
                        if (sentTo[j].Trim() != "")
                        {
                            SendMailToVendor(sentTo[j], language[j], lstBooking, BookingID);
                        }
                    }
                }
                else
                    SendMailToVendor(ConfigurationManager.AppSettings["DefaultEmailAddress"], "English", lstBooking, BookingID);
            }

        }
        catch { }
    }

    private void SendMailToVendor(string toAddress, string language, List<APPBOK_BookingBE> lstBooking, int BookingID)
    {
        if (!string.IsNullOrEmpty(toAddress))
        {
            #region Pallet, Lines, Cartons, DeliveryDateValue and DeliveryTimeValue ...
            string DeliveryDateValue = string.Empty;

            int iTotalPalllets, iTotalLines, iTotalCartons, iTotalLifts;
            iTotalPalllets = iTotalLines = iTotalCartons = iTotalLifts = 0;

            for (int iCount = 0; iCount < lstBooking.Count; iCount++)
            {
                iTotalPalllets += Convert.ToInt32(lstBooking[iCount].NumberOfPallet);
                iTotalLines += Convert.ToInt32(lstBooking[iCount].NumberOfLines);
                iTotalCartons += Convert.ToInt32(lstBooking[iCount].NumberOfCartons);
                iTotalLifts += Convert.ToInt32(lstBooking[iCount].NumberOfCartons);
            }

            DeliveryDateValue = lstBooking[0].ScheduleDate.Value.ToString("dd/MM/yyyy");
            string strWeekDay = lstBooking[0].ScheduleDate.Value.DayOfWeek.ToString().ToUpper();
            if (strWeekDay == "SUNDAY")
            {
                if (lstBooking[0].WeekDay != 1)
                    DeliveryDateValue = lstBooking[0].ScheduleDate.Value.AddDays(-1).ToString("dd/MM/yyyy");
            }
            else if (strWeekDay == "MONDAY")
            {
                if (lstBooking[0].WeekDay != 2)
                    DeliveryDateValue = lstBooking[0].ScheduleDate.Value.AddDays(-1).ToString("dd/MM/yyyy");
            }
            else if (strWeekDay == "TUESDAY")
            {
                if (lstBooking[0].WeekDay != 3)
                    DeliveryDateValue = lstBooking[0].ScheduleDate.Value.AddDays(-1).ToString("dd/MM/yyyy");
            }
            else if (strWeekDay == "WEDNESDAY")
            {
                if (lstBooking[0].WeekDay != 4)
                    DeliveryDateValue = lstBooking[0].ScheduleDate.Value.AddDays(-1).ToString("dd/MM/yyyy");
            }
            else if (strWeekDay == "THURSDAY")
            {
                if (lstBooking[0].WeekDay != 5)
                    DeliveryDateValue = lstBooking[0].ScheduleDate.Value.AddDays(-1).ToString("dd/MM/yyyy");
            }
            else if (strWeekDay == "FRIDAY")
            {
                if (lstBooking[0].WeekDay != 6)
                    DeliveryDateValue = lstBooking[0].ScheduleDate.Value.AddDays(-1).ToString("dd/MM/yyyy");
            }
            else if (strWeekDay == "SATURDAY")
            {
                if (lstBooking[0].WeekDay != 7)
                    DeliveryDateValue = lstBooking[0].ScheduleDate.Value.AddDays(-1).ToString("dd/MM/yyyy");
            }

            #endregion

            string htmlBody = string.Empty;
            var oSendCommunicationCommon = new sendCommunicationCommon();

            //string vendorName = string.Join(",", lstBooking.Select(x => x.Vendor.VendorName).ToArray());

            string templatePath = null;
            templatePath = path + templatePathName;
            string LanguageFile = string.Empty;

            APPBOK_CommunicationBAL oAPPBOK_CommunicationBAL = new APPBOK_CommunicationBAL();
            List<MAS_LanguageBE> oLanguages = oAPPBOK_CommunicationBAL.GetLanguages();

            foreach (MAS_LanguageBE objLanguage in oLanguages)
            {
                bool MailSentInLanguage = false;

                if (objLanguage.Language.ToLower() == language.ToLower())
                {
                    MailSentInLanguage = true;
                }

                //LanguageFile = "ProvisionalRefusalLetter." + objLanguage.Language + ".htm";
                LanguageFile = "ProvisionalRefusalLetter.english.htm";
                var singleSiteSetting = GetSingleSiteSetting(Convert.ToInt32(lstBooking[0].SiteId));



                using (StreamReader sReader = new StreamReader(templatePath + LanguageFile.ToLower()))
                {
                    #region mailBody
                    htmlBody = sReader.ReadToEnd();
                    //htmlBody = htmlBody.Replace("{logoInnerPath}", oSendCommunicationCommon.getAbsolutePath());

                    htmlBody = htmlBody.Replace("{DearSirMadam}", WebCommon.getGlobalResourceValue("DearSirMadam", objLanguage.Language));
                    htmlBody = htmlBody.Replace("{ProvisionalRefusalText1}", WebCommon.getGlobalResourceValue("ProvisionalRefusalText1", objLanguage.Language));

                    htmlBody = htmlBody.Replace("{GoodsInComment}", WebCommon.getGlobalResourceValue("GoodsInComment", objLanguage.Language));
                    htmlBody = htmlBody.Replace("{GINCOMMENTValue}", txtRejectProCmments.Text.Trim());

                    htmlBody = htmlBody.Replace("{BookingRef}", lstBooking[0].BookingRef);

                    htmlBody = htmlBody.Replace("{Date}", WebCommon.getGlobalResourceValue("Date", objLanguage.Language));
                    htmlBody = htmlBody.Replace("{DateValue}", DeliveryDateValue);

                    htmlBody = htmlBody.Replace("{Time}", WebCommon.getGlobalResourceValue("Time", objLanguage.Language));
                    htmlBody = htmlBody.Replace("{TimeValue}", WebCommon.getGlobalResourceValue("ProvisionalBooking", objLanguage.Language).ToUpper());

                    htmlBody = htmlBody.Replace("{Carrier}", WebCommon.getGlobalResourceValue("Carrier", objLanguage.Language));
                    htmlBody = htmlBody.Replace("{CarrierValue}", lstBooking[0].Carrier.CarrierName);

                    htmlBody = htmlBody.Replace("{VehicleTypeActual}", WebCommon.getGlobalResourceValue("VehicleTypeActual", objLanguage.Language));
                    htmlBody = htmlBody.Replace("{VehicleTypeValue}", lstBooking[0].VehicleType.VehicleType);

                    htmlBody = htmlBody.Replace("{NumberofPallets}", WebCommon.getGlobalResourceValue("NumberofPallets", objLanguage.Language));
                    htmlBody = htmlBody.Replace("{PalletsValue}", Convert.ToString(iTotalPalllets));

                    htmlBody = htmlBody.Replace("{NumberofLifts}", WebCommon.getGlobalResourceValue("NumberofLifts", objLanguage.Language));
                    htmlBody = htmlBody.Replace("{LiftsValue}", Convert.ToString(iTotalLifts));

                    htmlBody = htmlBody.Replace("{NumberofCartons}", WebCommon.getGlobalResourceValue("NumberofCartons", objLanguage.Language));
                    htmlBody = htmlBody.Replace("{CartonsValue}", Convert.ToString(iTotalCartons));

                    htmlBody = htmlBody.Replace("{NumberofPOLines}", WebCommon.getGlobalResourceValue("NumberofPOLines", objLanguage.Language));
                    htmlBody = htmlBody.Replace("{LinesValue}", Convert.ToString(iTotalLines));

                    htmlBody = htmlBody.Replace("{BookedPONo}", WebCommon.getGlobalResourceValue("BookedPONo", objLanguage.Language));
                    htmlBody = htmlBody.Replace("{BookedPOsValue}", lstBooking[0].PurchaseOrders.TrimStart(',').TrimEnd(','));

                    htmlBody = htmlBody.Replace("{dd/mm/yyyy}", lstBooking[0].BookingDate);
                    htmlBody = htmlBody.Replace("{PortalLink}", "<a href=" + sPortalLink + ">" + sPortalLink + " </a>");

                    htmlBody = htmlBody.Replace("{Yoursfaithfully}", WebCommon.getGlobalResourceValue("Yoursfaithfully", objLanguage.Language));
                    //---Stage 7 Point 29-----//
                    if (singleSiteSetting != null && singleSiteSetting.NonTimeStart != string.Empty)
                    {
                        htmlBody = htmlBody.Replace("{tblVisible}", "");
                        htmlBody = htmlBody.Replace("{SiteNameValue}", singleSiteSetting.SiteName);
                        htmlBody = htmlBody.Replace("{NonTimedStartTimeValue}", singleSiteSetting.NonTimeStart);
                        htmlBody = htmlBody.Replace("{NonTimedEndTimeValue}", singleSiteSetting.NonTimeEnd);
                    }
                    else
                    {
                        htmlBody = htmlBody.Replace("{tblVisible}", "none");
                    }
                    #endregion
                }

                string sFromAddress = Convert.ToString(ConfigurationManager.AppSettings["fromAddress"]);

                // Resending of Delivery mails
                oAPPBOK_CommunicationBAL = new APPBOK_CommunicationBAL();
                clsEmail oclsEmail = new clsEmail();

                if (objLanguage.Language.ToLower() == language.ToLower())
                {
                    //ProvisionalRefusal

                    oclsEmail.sendMail(toAddress, htmlBody, WebCommon.getGlobalResourceValue("ProvisionalRefusal", objLanguage.Language), sFromAddress, true);
                }
                oAPPBOK_CommunicationBAL.AddDeletedItemBAL(BookingID, sFromAddress, toAddress, WebCommon.getGlobalResourceValue("ProvisionalRefusal", objLanguage.Language), htmlBody,
                    Convert.ToInt32(Session["UserID"]), CommunicationType.Enum.ProvisionalRefusal, objLanguage.LanguageID, MailSentInLanguage, MailSentInLanguage);
            }

        }
    }


    private void LoggingWindow(string ForWindows,List<MASSIT_TimeWindowBE> lstAvailableSlots)
    {
        StringBuilder stringbuilder = new StringBuilder();
        stringbuilder.Append(ForWindows);
        stringbuilder.Append("\r\n");
        lstAvailableSlots.Select(x => new { x.TimeWindowID,x.SiteDoorTypeID,x.BeforeOrderBYId,x.IsCheckSKUTable,x.MaximumTime,
            x.TotalHrsMinsAvailable,
            x.MaximumPallets,
            x.MaximumLines,
            x.MaxDeliveries,
            x.LessVolumePallets,
            x.GraterVolumePallets,
            x.LessVolumeLines
            ,x.GraterVolumeLines
        }).ToList()
                                .ForEach(x => {
                                    stringbuilder.Append(" Window :" + x.TimeWindowID +
                                                         ", Door :" + x.SiteDoorTypeID +
                                                         ", BeforeOrderBYId :" + x.BeforeOrderBYId +
                                                         ", IsCheckSKUTable :" + x.IsCheckSKUTable +
                                                         ", MaximumTime :" + x.MaximumTime +
                                                         ", TotalHrsMinsAvailable :" + x.TotalHrsMinsAvailable +
                                                         ", MaximumPallets :" + x.MaximumPallets +
                                                         ", MaximumLines :" + x.MaximumLines +
                                                         ", MaxDeliveries :" + x.MaxDeliveries +
                                                         ", LessVolumePallets :" +x.LessVolumePallets+
                                                          ", GraterVolumePallets :" + x.GraterVolumePallets +
                                                           ", LessVolumeLines :" + x.LessVolumeLines +
                                                            ", GraterVolumeLines :" + x.GraterVolumeLines +
                                "\r\n");
                                });

        LogUtility.WriteTrace(stringbuilder);
        stringbuilder.Append("\r\n");
        stringbuilder.Append("\r\n");
    }
    private void LoggingVehicle(string ForVehicle, List<MASSIT_VehicleDoorMatrixBE> lstVehicleDoorMatrix)
    {
        StringBuilder stringbuilder = new StringBuilder();
        stringbuilder.Append(ForVehicle);
        stringbuilder.Append("\r\n");
        lstVehicleDoorMatrix.Select(x => new { x.DoorTypeID }).ToList()
                                .ForEach(x => {
                                    stringbuilder.Append("DoorTypeID :" + x.DoorTypeID + "\r\n");
                                });

        LogUtility.WriteTrace(stringbuilder);
        stringbuilder.Append("\r\n");
        stringbuilder.Append("\r\n");
    }
    private void LoggingExtra(string forMsg, string extraMsg)
    {
        StringBuilder stringbuilder = new StringBuilder();
        stringbuilder.Append("\r\n");        
        stringbuilder.Append(forMsg +":  " +  extraMsg + "\r\n");
        LogUtility.WriteTrace(stringbuilder);
        stringbuilder.Append("\r\n");
    }

    public void DoContinueVolumeExceeds()
    {
        hdnSuggestedWindowSlotID.Value = string.Empty;
        hdnSuggestedSiteDoorNumberID.Value = ddlDoorName.SelectedValue;
        hdnSuggestedSiteDoorNumber.Value = ddlDoorName.SelectedItem.Text;
        hdnSuggestedWeekDay.Value = LogicalSchedulDateTime.ToString("ddd");
        hdnStartTimeID.Value = ddlFrom.SelectedValue;
        hdnEndTimeID.Value = ddlTo.SelectedValue;

        //-----------Start Stage 9 point 2a/2b--------------//
        if (IsProvisionalBooking.Value == true && hdnCurrentMode.Value == "Edit")
        {
            if (hdnSuggestedSiteDoorNumberID.Value == "-1" || hdnSuggestedSiteDoorNumberID.Value == string.Empty || hdnSuggestedSiteDoorNumberID.Value == "0")
            {
                string errorMeesage = WebCommon.getGlobalResourceValue("confirmprovisionalbooking");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert2", "alert('" + errorMeesage + "')", true);
                return;
            }
        }
        //-----------End Stage 9 point 2a/2b--------------//
        ConfirmWindowBooking();
    }

    public void ShowHazordousItem()
    {
        MAS_VendorBAL mAS_VendorBAL = new MAS_VendorBAL();
        MAS_VendorBE mAS_VendorBE = new MAS_VendorBE();
        mAS_VendorBE.Action = "GetSiteSchedulingSettingsForHazardous";
        mAS_VendorBE.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedValue);
        DataSet ds = mAS_VendorBAL.GetSiteSchedulingSettingsForHazardousBAL(mAS_VendorBE);

        if (ds != null && ds.Tables.Count > 0)
        {
            var result = Convert.ToBoolean(ds.Tables[0].Rows[0][0].ToString());
            if (result == true)
            {
                divEnableHazardouesItemPrompt.Visible = true;
                divHazrdous.Visible = true;
            }
            else
            {
                divEnableHazardouesItemPrompt.Visible = false;
                divHazrdous.Visible = false;
            }
        }
    }
    protected void chkSiteSetting_CheckedChanged1(object sender, EventArgs e)
    {
        CountryData.Visible = chkSiteSetting.Checked;

        pnlPalletCheckforCountryDisplay.Visible = chkSiteSetting.Checked;

        lblYesNo.Text = chkSiteSetting.Checked ? "Yes" : "No"; 
    }
}

//public class VendorDetails
//{
//    public string VendorName { get; set; }
//    public Color CellColor { get; set; }
//    public bool isCurrentSlotFixed { get; set; }
//    public string CurrentFixedSlotID { get; set; }
//}
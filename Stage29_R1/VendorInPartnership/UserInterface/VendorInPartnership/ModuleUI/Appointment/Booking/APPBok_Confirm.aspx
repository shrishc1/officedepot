﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="APPBok_Confirm.aspx.cs" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    Inherits="ModuleUI_Appointment_Booking_APPBok_Confirm" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
   
    <h2>
        <cc1:ucLiteral ID="ltBookingRef" runat="server"></cc1:ucLiteral>
    </h2>
    <div class="right-shadow">
        <div class="formbox">
            <asp:Panel ID="pnlShowSingleBooking" runat="server">
                <table id="tblInfo" runat="server" width="60%" cellspacing="5" cellpadding="0" border="0"
                    align="center" class="top-settings">
                    <tr>
                        <td style="width: 18%">
                            <cc1:ucLabel ID="lblDeliveryDate" runat="server" Text="Delivery Date"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold; width: 1%">
                            :
                        </td>
                        <td style="font-weight: bold; width: 31%">
                            <cc1:ucLiteral ID="ltDeliveryDateValue" runat="server" Text=""></cc1:ucLiteral>
                        </td>
                        <td style="width: 18%">
                            <cc1:ucLabel ID="lblDeliveryTime" runat="server" Text="Delivery Time"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold; width: 1%">
                            :
                        </td>
                        <td style="font-weight: bold; width: 31%">
                            <cc1:ucLiteral ID="ltDeliveryTimeValue" runat="server" Text=""></cc1:ucLiteral>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucLabel ID="lblLifts" runat="server" Text="Lifts"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            :
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucLiteral ID="ltLiftsValue" runat="server" Text="-"></cc1:ucLiteral>
                        </td>
                        <td>
                            <cc1:ucLabel ID="lblLines" runat="server" Text="Lines"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            :
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucLiteral ID="ltLinesValue" runat="server" Text="-"></cc1:ucLiteral>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucLabel ID="lblPallets" runat="server" Text="Pallets"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            :
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucLiteral ID="ltPalletsValue" runat="server" Text="-"></cc1:ucLiteral>
                        </td>
                        <td>
                            <cc1:ucLabel ID="lblCartons" runat="server" Text="Cartons"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            :
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucLiteral ID="ltCartonsValue" runat="server" Text="-"></cc1:ucLiteral>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="6" >
                            <cc1:ucLabel ID="lblInformationBookinfConfirm" runat="server" Text="An email has been sent with the booking confirmation details"></cc1:ucLabel>
                            <cc1:ucLabel ID="lblWindowVendorInformationBookinfConfirm" runat="server" Visible="false" Text="Your booking has been made provisionally and will be reviewed by a member of our Booking In team who will contact you shortly."></cc1:ucLabel>
                            <cc1:ucLabel ID="lblInfoProvisionalBooking" Visible="false" runat="server" Text="This booking has been made provisionally. Office Depot will now review this and contact you via email to either confirm a time for the booking or to let you know the booking was not possible."></cc1:ucLabel>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <asp:Panel ID="pnlShowMultipleBooking" runat="server">
                <asp:Literal ID="ltrlShowMultipleBooking" runat="server"></asp:Literal>
            </asp:Panel>
            <asp:Panel ID="pnlCommon" runat="server">
                <table id="tblError" runat="server" style="display: none;" width="60%" cellspacing="5"
                    cellpadding="0" border="0" align="center" class="top-settings">
                    <tr>
                        <td style="width: 18%">
                            <cc1:ucLabel ID="lblErrorBooking" runat="server" Text="There is some error while creating booking. Please try again after some time."></cc1:ucLabel>
                        </td>
                    </tr>
                </table>
                <table width="60%" cellspacing="5" cellpadding="0" border="0" align="center" class="form-table">
                    <tr>
                        <td align="right">
                            <cc1:ucButton ID="btnOK" runat="server" Text="OK" CssClass="button" OnClick="btnOK_Click" />
                        </td>
                    </tr>
                </table>
                <table width="60%" cellspacing="5" cellpadding="0" border="0" align="center" class="form-table">
                    <tr>
                        <td align="center" style="color:red">
                            <cc1:ucLabel ID="lblIsVendorPalletCheckingForISPM15" Visible="false" runat="server"
                                Text="Please note, this delivery could contain pallets that need to be checked for ISPM15."></cc1:ucLabel>
                             <cc1:ucLabel ID="lblIsStandardPalletChecking" Visible="false" runat="server"
                                Text="A standard pallet check is required for this vendor."></cc1:ucLabel>
                        </td>
                    </tr>
                </table>
                  <table width="60%" cellspacing="5" cellpadding="0" border="0" align="center" class="form-table">
                    <tr>
                        <td align="center" style="color:red">
                            <cc1:ucLabel ID="lblIsEnableHazardouesItem" Visible="false" runat="server" Text="Caution this delivery contains hazardous products."></cc1:ucLabel>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            
            <%--<br /><br />
            <table id="tblMultiInfo" runat="server" width="60%" cellspacing="5" cellpadding="0" border="0"
                align="center" class="top-settings">
                <tr>
                    <td style="font-weight: bold; width: 31%" colspan="6">
                        An email has been sent with the booking confirmation details, please quote all Booking References when making the delivery
                    </td>
                </tr>
                <tr>
                    <td style="height:5%;"></td>
                </tr>
                <tr>
                    <td style="width: 18%">
                        <b style="font-size:small">Delivery Date</b>                        
                    </td>
                    <td style="font-weight: bold; width: 1%">
                        <b style="font-size:small">:</b>
                    </td>
                    <td style="font-weight: bold; width: 31%">                        
                        <b style="font-size:small">22/10/12</b>
                    </td>
                    <td style="width: 18%">
                        <b style="font-size:small">Delivery Time</b>
                    </td>
                    <td style="font-weight: bold; width: 1%">
                        <b style="font-size:small">:</b>
                    </td>
                    <td style="font-weight: bold; width: 31%">
                        <b style="font-size:small">07:15</b>
                    </td>
                </tr>
                <tr>
                    <td style="height:2%;"></td>
                </tr>
                <tr>
                    <td style="font-weight: bold; width: 31%" colspan="6">
                        <b style="font-size:small">Booking Reference : ZWO-03-2210-00139</b>
                    </td>
                </tr>
                <tr>
                    <td style="height:2%;"></td>
                </tr>
                <tr>
                    <td style="font-weight: bold; width: 31%" colspan="6">
                        Vendor : 123-Everyberry
                    </td>
                </tr>
                <tr>
                    <td style="width: 18%">
                        Lifts
                    </td>
                    <td style="font-weight: bold; width: 1%">
                        :
                    </td>
                    <td style="font-weight: bold; width: 31%">
                        1
                    </td>
                    <td style="width: 18%">
                        Lines
                    </td>
                    <td style="font-weight: bold; width: 1%">
                        :
                    </td>
                    <td style="font-weight: bold; width: 31%">
                        1
                    </td>
                </tr>
                <tr>
                    <td style="width: 18%">
                        Pallets
                    </td>
                    <td style="font-weight: bold; width: 1%">
                        :
                    </td>
                    <td style="font-weight: bold; width: 31%">
                        1
                    </td>
                    <td style="width: 18%">
                        Cartons
                    </td>
                    <td style="font-weight: bold; width: 1%">
                        :
                    </td>
                    <td style="font-weight: bold; width: 31%">
                        1
                    </td>
                </tr>
            </table>--%>

        </div>
    </div>
</asp:Content>

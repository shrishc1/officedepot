﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class APPBOK_Option : CommonPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
         
    }

    protected void Page_PreRender(object sender, EventArgs e) {
        //if (!Page.IsPostBack) {
        //    DateTime LogicalSchedulDateTime = Utilities.Common.TextToDateFormat(GetQueryStringValue("Logicaldate"));
        //    DateTime ActualSchedulDateTime = Utilities.Common.TextToDateFormat(GetQueryStringValue("Scheduledate"));

        //    string FixedSlotMode = string.Empty;

        //    if (GetQueryStringValue("FixedSlot") == null)
        //        FixedSlotMode = "false";
        //    else if (GetQueryStringValue("FixedSlot") == "True")
        //        FixedSlotMode = "true";
                      

        //    if (FixedSlotMode == "true") {
        //        if (GetQueryStringValue("Mode").ToString() == "Edit" && GetQueryStringValue("Type").ToString() == "C") {
        //            EncryptQueryString("APPBok_CarrierBookingEdit.aspx?Mode=" + GetQueryStringValue("Mode").ToString() + "&ID=" + GetQueryStringValue("ID").ToString() + "&Scheduledate=" + ActualSchedulDateTime.ToString("dd/MM/yyyy") + "&Logicaldate=" + LogicalSchedulDateTime.ToString("dd/MM/yyyy") + "&SiteID=" + GetQueryStringValue("SiteID").ToString() + "&SiteCountryID=" + GetQueryStringValue("SiteCountryID").ToString());
        //        }
        //        else if (FixedSlotMode == "true" && GetQueryStringValue("Type").ToString() == "C") {
        //            EncryptQueryString("APPBok_CarrierBookingEdit.aspx?FixedSlot=True&Mode=" + GetQueryStringValue("Mode").ToString() + "&ID=" + GetQueryStringValue("ID").ToString() + "&Scheduledate=" + ActualSchedulDateTime.ToString("dd/MM/yyyy") + "&Logicaldate=" + LogicalSchedulDateTime.ToString("dd/MM/yyyy") + "&SiteID=" + GetQueryStringValue("SiteID").ToString() + "&SiteCountryID=" + GetQueryStringValue("SiteCountryID").ToString());
        //        }
        //        else if (GetQueryStringValue("Mode").ToString() == "Add" && Session["Role"].ToString() == "Carrier") {
        //            EncryptQueryString("APPBok_CarrierBookingEdit.aspx?Mode=" + GetQueryStringValue("Mode").ToString() + "&Scheduledate=" + ActualSchedulDateTime.ToString("dd/MM/yyyy") + "&Logicaldate=" + LogicalSchedulDateTime.ToString("dd/MM/yyyy") + "&SiteID=" + GetQueryStringValue("SiteID").ToString() + "&SiteCountryID=" + GetQueryStringValue("SiteCountryID").ToString());
        //        }
        //    }

        //    string WindowSlotMode = string.Empty;

        //    if (GetQueryStringValue("WindowSlot") == null)
        //        WindowSlotMode = "false";
        //    else if (GetQueryStringValue("WindowSlot") == "True")
        //        WindowSlotMode = "true";

        //    if (WindowSlotMode == "true") {
        //        if (GetQueryStringValue("Mode").ToString() == "Edit" && GetQueryStringValue("Type").ToString() == "C") {
        //            EncryptQueryString("APPBok_CarrierBookingWindowEdit.aspx?Mode=" + GetQueryStringValue("Mode").ToString() + "&Type=" +
        //                GetQueryStringValue("Type").ToString() + "&ID=" + GetQueryStringValue("ID").ToString() + "&Scheduledate="
        //                + ActualSchedulDateTime.ToString("dd/MM/yyyy") + "&Logicaldate=" + LogicalSchedulDateTime.ToString("dd/MM/yyyy") + "&SiteID="
        //                + GetQueryStringValue("SiteID").ToString() + "&SiteCountryID=" + GetQueryStringValue("SiteCountryID").ToString());
        //        }
        //        else if (WindowSlotMode == "true" && GetQueryStringValue("Type").ToString() == "C") {
        //            EncryptQueryString("APPBok_CarrierBookingWindowEdit.aspx?WindowSlot=True&Mode=" + GetQueryStringValue("Mode").ToString() + "&ID=" + GetQueryStringValue("ID").ToString() + "&Scheduledate=" + ActualSchedulDateTime.ToString("dd/MM/yyyy") + "&Logicaldate=" + LogicalSchedulDateTime.ToString("dd/MM/yyyy") + "&SiteID=" + GetQueryStringValue("SiteID").ToString() + "&SiteCountryID=" + GetQueryStringValue("SiteCountryID").ToString());
        //        }
        //        else if (GetQueryStringValue("Mode").ToString() == "Add" && Session["Role"].ToString() == "Carrier") {
        //            EncryptQueryString("APPBok_CarrierBookingWindowEdit.aspx?Mode=" + GetQueryStringValue("Mode").ToString() + "&Scheduledate=" + ActualSchedulDateTime.ToString("dd/MM/yyyy") + "&Logicaldate=" + LogicalSchedulDateTime.ToString("dd/MM/yyyy") + "&SiteID=" + GetQueryStringValue("SiteID").ToString() + "&SiteCountryID=" + GetQueryStringValue("SiteCountryID").ToString());
        //        }
        //    }
        //}
    }

    protected void btnVendor_Click(object sender, EventArgs e) {
        //ActivateFistView();        
       
        if(GetQueryStringValue("Flag")=="true")
        {
            EncryptQueryString("APPBok_WindowBookingEdit.aspx?WindowSlot=True&Mode=Add&ID=" + GetQueryStringValue("ID") + "&Scheduledate=" + GetQueryStringValue("Scheduledate") + "&Logicaldate=" + GetQueryStringValue("Logicaldate") + "&SiteID=" + GetQueryStringValue("SiteID") + "&SiteCountryID=" + GetQueryStringValue("SiteCountryID") + "&BookingWeekDay=" + GetQueryStringValue("SiteCountryID") + "&Flag=" + "true" + "&Time=" + GetQueryStringValue("Time") + "&SiteDoorNumberID=" + GetQueryStringValue("SiteDoorNumberID") + "&DoorNumber=" + GetQueryStringValue("DoorNumber") + "&Type=" + "");
        }
        else
        {
            EncryptQueryString(GetQueryStringValue("Page") + "?BP=OPT&Mode=Add&Scheduledate=" + GetQueryStringValue("Scheduledate") + "&Logicaldate=" + GetQueryStringValue("Logicaldate") + "&SiteID=" + GetQueryStringValue("SiteID") + "&SiteCountryID=" + GetQueryStringValue("SiteCountryID") + "&TimeWindowID=" + GetQueryStringValue("TimeWindowID") + "&Flag=" + "false");
        }
    }

    protected void btnCarrier_Click(object sender, EventArgs e)
    {

        DateTime LogicalSchedulDateTime = Utilities.Common.TextToDateFormat(GetQueryStringValue("Logicaldate"));
        DateTime ActualSchedulDateTime = Utilities.Common.TextToDateFormat(GetQueryStringValue("Scheduledate"));

        if (GetQueryStringValue("Page") == "APPBok_BookingEdit.aspx")
            EncryptQueryString("APPBok_CarrierBookingEdit.aspx?Mode=" + GetQueryStringValue("Mode").ToString() + "&Scheduledate=" + ActualSchedulDateTime.ToString("dd/MM/yyyy") + "&Logicaldate=" + LogicalSchedulDateTime.ToString("dd/MM/yyyy") + "&SiteID=" + GetQueryStringValue("SiteID").ToString() + "&SiteCountryID=" + GetQueryStringValue("SiteCountryID").ToString() +"&Flag=" + "true");
        else if (GetQueryStringValue("Page") == "APPBok_WindowBookingEdit.aspx")
        {
            if (GetQueryStringValue("Flag") == "true")
            {
                EncryptQueryString("APPBok_CarrierBookingWindowEdit.aspx?WindowSlot=True&Mode=" + GetQueryStringValue("Mode").ToString() + "&Scheduledate=" + ActualSchedulDateTime.ToString("dd/MM/yyyy") + "&Logicaldate=" + LogicalSchedulDateTime.ToString("dd/MM/yyyy") + "&SiteID=" + GetQueryStringValue("SiteID").ToString() + "&SiteCountryID=" + GetQueryStringValue("SiteCountryID").ToString() + "&ID=" + GetQueryStringValue("ID") + "&Flag=" + "true" + "&Time=" + GetQueryStringValue("Time") + "&SiteDoorNumberID=" + GetQueryStringValue("SiteDoorNumberID") + "&DoorNumber=" + GetQueryStringValue("DoorNumber") + "&Type=" + "");
            }
            else
            {
                EncryptQueryString("APPBok_CarrierBookingWindowEdit.aspx?Mode=" + GetQueryStringValue("Mode").ToString() + "&Scheduledate=" + ActualSchedulDateTime.ToString("dd/MM/yyyy") + "&Logicaldate=" + LogicalSchedulDateTime.ToString("dd/MM/yyyy") + "&SiteID=" + GetQueryStringValue("SiteID").ToString() + "&SiteCountryID=" + GetQueryStringValue("SiteCountryID").ToString() + "&Flag=" + "false");
            }
        }
    }
}
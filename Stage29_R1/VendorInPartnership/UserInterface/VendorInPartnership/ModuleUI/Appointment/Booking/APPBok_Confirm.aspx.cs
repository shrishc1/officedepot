﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.Appointment.Booking;
using BusinessLogicLayer.ModuleBAL.Appointment.Booking;
using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using BusinessLogicLayer.ModuleBAL.Appointment.SiteSettings;
using BusinessLogicLayer.ModuleBAL.Security;
using Utilities;
using WebUtilities;
using System.Configuration;
using System.Text;
using System.IO;
using BusinessEntities.ModuleBE.Security;
using System.Linq;
using BusinessEntities.ModuleBE.AdminFunctions;
using BusinessLogicLayer.ModuleBAL.AdminFunctions;
using BusinessEntities.ModuleBE.Languages.Languages;


public partial class ModuleUI_Appointment_Booking_APPBok_Confirm : CommonPage
{
    readonly string sPortalLink = Convert.ToString(ConfigurationManager.AppSettings["Portallink"]) + @"/ModuleUI/Security/Login.aspx";
    readonly string path = AppDomain.CurrentDomain.BaseDirectory.ToLower();
    readonly string templatePathName = @"/EmailTemplates/Appointment/";
    APPBOK_BookingBE oAPPBOK_BookingBE = null;
    APPBOK_BookingBAL oAPPBOK_BookingBAL = null;
    MASSIT_VendorBE oMASSIT_VendorBE = null;
    APPSIT_VendorBAL oAPPSIT_VendorBAL = null;
    APPBOK_CommunicationBAL oAPPBOK_CommunicationBAL = null;

    public delegate void MethodInvoker();

    List<APPBOK_BookingBE> lstBooking = new List<APPBOK_BookingBE>();
    int BookingID = 0;

    string BookingType = string.Empty;
    string IsBookingAmended = string.Empty;
    string IsBookingEditWithMail = string.Empty;
    string BookingComments = string.Empty;
    int UserID = 0;

    StringBuilder sbMessage = new StringBuilder();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            string strBookingId = GetQueryStringValue("BookingID");
            if (string.IsNullOrWhiteSpace(strBookingId))
                return;          

            #region Single Booking Display Logic ...

            pnlShowSingleBooking.Visible = true;
            pnlShowMultipleBooking.Visible = false;

            BookingID = Convert.ToInt32(strBookingId);

            sbMessage.AppendLine("----------------------------------------");
            sbMessage.AppendLine("1.1.BookingID :- " + Convert.ToString(BookingID));

            oAPPBOK_BookingBE = new APPBOK_BookingBE();
            oAPPBOK_BookingBAL = new APPBOK_BookingBAL();
            oAPPBOK_BookingBE.Action = "GetBookingDeatilsByID";
            oAPPBOK_BookingBE.BookingID = BookingID;
            lstBooking = oAPPBOK_BookingBAL.GetBookingDetailsByIDBAL(oAPPBOK_BookingBE);

            if (lstBooking.Count > 0)
            {

                sbMessage.AppendLine("1.2.BookingID :- " + Convert.ToString(BookingID));

                if (GetQueryStringValue("MailSend") != null)
                {
                    sbMessage.AppendLine("1.MailSend :- " + Convert.ToString(GetQueryStringValue("MailSend")));
                }

                BookingComments = lstBooking[0].BookingComments;

                BookingType = GetQueryStringValue("BookingType");
                IsBookingAmended = GetQueryStringValue("IsBookingAmended");
                IsBookingEditWithMail = GetQueryStringValue("IsBookingEditWithMail");
                UserID = Convert.ToInt32(Session["UserID"]);


                sbMessage.AppendLine("1.3.BookingType :- " + Convert.ToString(BookingType));
                sbMessage.AppendLine("1.4.IsBookingAmended :- " + Convert.ToString(IsBookingAmended));
                sbMessage.AppendLine("1.5.IsBookingEditWithMail :- " + Convert.ToString(IsBookingEditWithMail));


                int iTotalPalllets = 0;
                int iTotalLines = 0;
                int iTotalCartons = 0;

                for (int iCount = 0; iCount < lstBooking.Count; iCount++)
                {
                    iTotalPalllets += Convert.ToInt32(lstBooking[iCount].NumberOfPallet);
                    iTotalLines += Convert.ToInt32(lstBooking[iCount].NumberOfLines);
                    iTotalCartons += Convert.ToInt32(lstBooking[iCount].NumberOfCartons);
                }

                ltBookingRef.Text = GetQueryStringValue("BookingType") == "PNTD"
                    ? WebCommon.getGlobalResourceValue("ProvisionalBookingReference") + " " + lstBooking[0].BookingRef
                    : WebCommon.getGlobalResourceValue("BookingReference") + " " + lstBooking[0].BookingRef;

                ltDeliveryDateValue.Text = lstBooking[0].ScheduleDate.Value.ToString("dd/MM/yyyy");

                switch (lstBooking[0].ScheduleDate.Value.DayOfWeek.ToString().ToUpper())
                {
                    case "SUNDAY":
                        if (lstBooking[0].WeekDay != 1)
                            ltDeliveryDateValue.Text = lstBooking[0].ScheduleDate.Value.AddDays(-1).ToString("dd/MM/yyyy");
                        break;
                    case "MONDAY":
                        if (lstBooking[0].WeekDay != 2)
                            ltDeliveryDateValue.Text = lstBooking[0].ScheduleDate.Value.AddDays(-1).ToString("dd/MM/yyyy");
                        break;
                    case "TUESDAY":
                        if (lstBooking[0].WeekDay != 3)
                            ltDeliveryDateValue.Text = lstBooking[0].ScheduleDate.Value.AddDays(-1).ToString("dd/MM/yyyy");
                        break;
                    case "WEDNESDAY":
                        if (lstBooking[0].WeekDay != 4)
                            ltDeliveryDateValue.Text = lstBooking[0].ScheduleDate.Value.AddDays(-1).ToString("dd/MM/yyyy");
                        break;
                    case "THURSDAY":
                        if (lstBooking[0].WeekDay != 5)
                            ltDeliveryDateValue.Text = lstBooking[0].ScheduleDate.Value.AddDays(-1).ToString("dd/MM/yyyy");
                        break;
                    case "FRIDAY":
                        if (lstBooking[0].WeekDay != 6)
                            ltDeliveryDateValue.Text = lstBooking[0].ScheduleDate.Value.AddDays(-1).ToString("dd/MM/yyyy");
                        break;
                    case "SATURDAY":
                        if (lstBooking[0].WeekDay != 7)
                            ltDeliveryDateValue.Text = lstBooking[0].ScheduleDate.Value.AddDays(-1).ToString("dd/MM/yyyy");
                        break;
                }

                if (GetQueryStringValue("BookingType") == "PNTD")
                {
                    ltDeliveryTimeValue.Text = WebCommon.getGlobalResourceValue("ProvisionalBooking").ToUpper(); // "Provisional Booking";
                    lblInfoProvisionalBooking.Visible = true;
                    lblInformationBookinfConfirm.Visible = false;
                    lblWindowVendorInformationBookinfConfirm.Visible = false;
                }
                else if (lstBooking[0].BookingTypeID != 3)
                    ltDeliveryTimeValue.Text = lstBooking[0].SlotTime.SlotTime;
                else
                    ltDeliveryTimeValue.Text = WebCommon.getGlobalResourceValue("NonTimeDelivery"); // "Non Time Delivery";

                if (lstBooking[0].NumberOfCartons != (int?)null)
                    ltCartonsValue.Text = iTotalCartons.ToString();
                if (lstBooking[0].NumberOfLift != (int?)null)
                    ltLiftsValue.Text = lstBooking[0].NumberOfLift.ToString();
                if (lstBooking[0].NumberOfLines != (int?)null)
                    ltLinesValue.Text = iTotalLines.ToString();
                if (lstBooking[0].NumberOfPallet != (int?)null)
                    ltPalletsValue.Text = iTotalPalllets.ToString();

                if ((lstBooking[0].SupplierType == "V" || lstBooking[0].SupplierType == "C") && lstBooking[0].BookingTypeID == 5)
                {
                    lblInformationBookinfConfirm.Visible = false;
                    lblWindowVendorInformationBookinfConfirm.Visible = true;
                }

                sbMessage.AppendLine("1.6.IsBookingEditWithMail :- " + Convert.ToString(IsBookingEditWithMail));
                LogUtility.SaveTraceLogEntry(sbMessage);
                sbMessage.Clear();

                // SEND EMAIL
                if (GetQueryStringValue("MailSend") != null)
                {
                    if (GetQueryStringValue("MailSend") == "True")
                    {
                        sbMessage.AppendLine("2.1.BookingID :- " + Convert.ToString(BookingID));
                        LogUtility.SaveTraceLogEntry(sbMessage);
                        MethodInvoker simpleDelegate = new MethodInvoker(SendBookingConfirmationMail);
                        // Calling SendNoShowMails Async
                        simpleDelegate.BeginInvoke(null, null);
                    }
                    else
                    { //check double click case - 
                        oAPPBOK_CommunicationBAL = new APPBOK_CommunicationBAL();
                        int? iResult = oAPPBOK_CommunicationBAL.getCommunicationByIDBAL(lstBooking[0].BookingID, CommunicationType.Enum.BookingConfirmation);
                        sbMessage.AppendLine("2.1.1.iResult :- " + Convert.ToString(iResult));
                        LogUtility.SaveTraceLogEntry(sbMessage);
                        if (iResult == 0)
                        { // No Mail send - then send it
                            sbMessage.Clear();
                            sbMessage.AppendLine("2.1.2.BookingID :- " + Convert.ToString(BookingID));
                            LogUtility.SaveTraceLogEntry(sbMessage);
                            MethodInvoker simpleDelegate = new MethodInvoker(SendBookingConfirmationMail);
                            // Calling SendNoShowMails Async
                            simpleDelegate.BeginInvoke(null, null);
                        }
                    }
                }

                #region Is Vendor or Standard Pallet Checking...

                lblIsVendorPalletCheckingForISPM15.Visible = lstBooking[0].IsVenodrPalletChecking || lstBooking[0].ISPM15CountryPalletChecking;

                lblIsStandardPalletChecking.Visible = lstBooking[0].IsStandardPalletChecking;

                #endregion

                #region Is Enable Hazardoues Item Prompt...
                lblIsEnableHazardouesItem.Visible = lstBooking[0].IsEnableHazardouesItemPrompt;
                #endregion

            }
            else
            {
                sbMessage.AppendLine("2.2.Error in BookingID :- " + Convert.ToString(BookingID));
                LogUtility.SaveTraceLogEntry(sbMessage);
                ltBookingRef.Visible = false;
                tblInfo.Style.Add("display", "none");
                tblError.Style.Add("display", "");
            }
            #endregion            
        }
    }
    private void SendBookingConfirmationMail()
    {
        MAS_SiteBE singleSiteSetting = GetSingleSiteSetting(Convert.ToInt32(lstBooking[0].SiteId));
        APPBOK_CommunicationBAL oAPPBOK_CommunicationBAL = new APPBOK_CommunicationBAL();
        List<MAS_LanguageBE> oLanguages = oAPPBOK_CommunicationBAL.GetLanguages();

        try
        {
            oMASSIT_VendorBE = new MASSIT_VendorBE();
            oAPPSIT_VendorBAL = new APPSIT_VendorBAL();
            string VendorEmails = string.Empty;
            string[] sentTo;
            string[] language;
            string[] VendorData = new string[2];
            bool isMailSend = false;

            sbMessage.Clear();
            sbMessage.AppendLine("3.1.BookingID :- " + Convert.ToString(BookingID));

            if (lstBooking[0].SupplierType == "C")
            {
                oMASSIT_VendorBE.Action = "GetCarriersVendor";
                oMASSIT_VendorBE.BookingID = BookingID;

                List<MASSIT_VendorBE> lstVendorDetails = oAPPSIT_VendorBAL.GetCarriersVendorIDBAL(oMASSIT_VendorBE);
                if (lstVendorDetails != null && lstVendorDetails.Count > 0)
                {
                    sbMessage.AppendLine("3.2.BookingID :- " + Convert.ToString(BookingID));

                    for (int i = 0; i < lstVendorDetails.Count; i++)
                    {
                        isMailSend = false;
                        VendorData = CommonPage.GetVendorEmailsWithLanguage(lstVendorDetails[i].VendorID, Convert.ToInt32(lstBooking[0].SiteId));
                        if (VendorData.Length > 0 && !string.IsNullOrEmpty(VendorData[0]))
                        {
                            sentTo = VendorData[0].Split(',');
                            language = VendorData[1].Split(',');

                            for (int j = 0; j < sentTo.Length; j++)
                            {
                                if (sentTo[j].Trim() != "")
                                {
                                    if (i == 0 && j == 0)
                                    {
                                        sbMessage.AppendLine("4.1.BookingID :- " + Convert.ToString(BookingID));
                                        sbMessage.AppendLine("4.2.sentTo :- " + Convert.ToString(sentTo[j]));
                                        LogUtility.SaveTraceLogEntry(sbMessage);
                                        sbMessage.Clear();
                                    }

                                    SendMailToVendor(sentTo[j], language[j], lstBooking, singleSiteSetting, oLanguages);
                                    isMailSend = true;
                                }
                            }
                        }

                        if (!isMailSend)
                        {
                            SendMailToVendor(ConfigurationManager.AppSettings["DefaultEmailAddress"], "English", lstBooking, singleSiteSetting, oLanguages);
                        }
                    }
                }


                //Send mail to Carrier
                isMailSend = false;
                VendorData = CommonPage.GetCarrierEmailsWithLanguage(lstBooking[0].Carrier.CarrierID, Convert.ToInt32(lstBooking[0].SiteId));
                if (VendorData.Length > 0 && !string.IsNullOrEmpty(VendorData[0]))
                {
                    sentTo = VendorData[0].Split(',');
                    language = VendorData[1].Split(',');
                    for (int j = 0; j < sentTo.Length; j++)
                    {
                        if (sentTo[j].Trim() != "")
                        {
                            if (j == 0)
                            {
                                sbMessage.AppendLine("5.1.BookingID :- " + Convert.ToString(BookingID));
                                sbMessage.AppendLine("5.2.sentTo :- " + Convert.ToString(sentTo[j]));
                                LogUtility.SaveTraceLogEntry(sbMessage);
                            }

                            SendMailToVendor(sentTo[j], language[j], lstBooking, singleSiteSetting, oLanguages);
                            isMailSend = true;
                        }
                    }
                }
                if (!isMailSend)
                {
                    SendMailToVendor(ConfigurationManager.AppSettings["DefaultEmailAddress"], "English", lstBooking, singleSiteSetting, oLanguages);
                }

                //----------------------//

            }
            else if (lstBooking[0].SupplierType == "V")
            {
                isMailSend = false;
                VendorData = CommonPage.GetVendorEmailsWithLanguage(lstBooking[0].VendorID, Convert.ToInt32(lstBooking[0].SiteId));
                if (VendorData.Length > 0 && !string.IsNullOrEmpty(VendorData[0]))
                {

                    sbMessage.AppendLine("4.1.BookingID :- " + Convert.ToString(BookingID));

                    sentTo = VendorData[0].Split(',');
                    language = VendorData[1].Split(',');
                    for (int j = 0; j < sentTo.Length; j++)
                    {
                        if (sentTo[j].Trim() != "")
                        {
                            if (j == 0)
                            {
                                sbMessage.AppendLine("4.2.BookingID :- " + Convert.ToString(BookingID));
                                sbMessage.AppendLine("4.3.sentTo :- " + Convert.ToString(sentTo[j]));
                                LogUtility.SaveTraceLogEntry(sbMessage);
                            }
                            SendMailToVendor(sentTo[j], language[j], lstBooking, singleSiteSetting, oLanguages);
                            isMailSend = true;
                        }
                    }
                }
                if (!isMailSend)
                {
                    SendMailToVendor(ConfigurationManager.AppSettings["DefaultEmailAddress"], "English", lstBooking, singleSiteSetting, oLanguages);
                }
            }
        }
        catch (Exception ex)
        {
            LogUtility.SaveErrorLogEntry(ex);
            LogUtility.SaveTraceLogEntry(sbMessage);
            SendMailToVendor(ConfigurationManager.AppSettings["DefaultEmailAddress"], "English", lstBooking, singleSiteSetting, oLanguages);
        }
    }

    private void UpdateEA()
    {
        if (GetQueryStringValue("BP") == null || GetQueryStringValue("BP") != "PRO")
        {
            if (GetQueryStringValue("BookingID") != null)
            {
                string strBookingId = GetQueryStringValue("BookingID");
                BookingID = Convert.ToInt32(strBookingId);
                oAPPBOK_BookingBE = new APPBOK_BookingBE();
                oAPPBOK_BookingBAL = new APPBOK_BookingBAL();
                oAPPBOK_BookingBE.Action = "UpdateAdvisedDate";
                oAPPBOK_BookingBE.BookingID = BookingID;
                oAPPBOK_BookingBAL.UpdateBookingReviewedStatusBAL(oAPPBOK_BookingBE);
            }
        }
    }

    private void SendMailToVendor(string toAddress, string language, List<APPBOK_BookingBE> lstBooking, MAS_SiteBE singleSiteSetting, List<MAS_LanguageBE> oLanguages)
    {
        if (!string.IsNullOrEmpty(toAddress))
        {
            sendCommunication sendCommunication= new sendCommunication();

            sendCommunication.SendMailToVendor(toAddress, language, lstBooking, templatePathName, BookingType, IsBookingAmended,
                IsBookingEditWithMail, singleSiteSetting, ltDeliveryDateValue.Text, ltDeliveryTimeValue.Text, ltPalletsValue.Text, ltLiftsValue.Text,
                ltCartonsValue.Text, ltLinesValue.Text, BookingComments, oLanguages, UserID);

            //if(lstBooking[0].ISPM15CountryPalletChecking)
            //    sendCommunication.SendISPM15MailToVendor(toAddress, language,  oLanguages,templatePathName, UserID);
        }
    }

    private MAS_SiteBE GetSingleSiteSetting(int siteId)
    {
        MAS_SiteBAL oMAS_SiteBAL = new MAS_SiteBAL();
        MAS_SiteBE oMAS_SiteBE = new MAS_SiteBE();
        oMAS_SiteBE.Action = "GetSingleSiteSetting";
        oMAS_SiteBE.SiteID = siteId;
        MAS_SiteBE localMAS_SiteBE = oMAS_SiteBAL.GetSingleSiteSettingBAL(oMAS_SiteBE);
        return localMAS_SiteBE;
    }

    protected void btnOK_Click(object sender, EventArgs e)
    {
        if (GetQueryStringValue("BP") != null && GetQueryStringValue("BP") == "PRO")
            EncryptQueryString("../ProvisionalBookings.aspx");
        else
            EncryptQueryString("APPBok_BookingOverview.aspx");
    }
}
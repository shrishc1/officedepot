﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    AutoEventWireup="true" CodeFile="APPBok_BookingVolumeDetail.aspx.cs" Inherits="ModuleUI_Appointment_Booking_APPBok_BookingVolumeDetail" %>

<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="sp1" runat="server">
    </asp:ScriptManager>
    <div style="overflow: hidden;">
        <asp:UpdatePanel ID="up1" runat="server">
            <ContentTemplate>
                <div style="width: 100%; overflow: auto" id="divPrint">
                    <table width="100%" cellspacing="5" cellpadding="5">
                        <tr>
                            <td valign="bottom" width="55%" valign="top">
                                <cc1:ucPanel ID="pnlCapacityOverview" runat="server" GroupingText="Capacity Overview"
                                    CssClass="fieldset-form">
                                    <table width="100%" cellspacing="5" cellpadding="0" height="135px" class="form-table">
                                        <tr style="height: 15px">
                                            <th width="37%" align="left">
                                                <cc1:ucLabel ID="lblCapacities" runat="server" Text="Capacities"></cc1:ucLabel>
                                            </th>
                                            <th width="21%" style="font-weight: bold;">
                                                <cc1:ucLabel ID="lblPallets" runat="server" Text="Pallets"></cc1:ucLabel>
                                            </th>
                                            <th width="21%" style="font-weight: bold;">
                                                <cc1:ucLabel ID="lblLines" runat="server" Text="Lines"></cc1:ucLabel>
                                            </th>
                                            <th width="21%" style="font-weight: bold;">
                                                <cc1:ucLabel ID="lblLifts" runat="server" Text="Lifts"></cc1:ucLabel>
                                            </th>
                                        </tr>
                                        <tr>
                                            <td>
                                                <cc1:ucLabel ID="lblMaximumDailyCapacity" runat="server" Text="Maximum Daily Capacity"></cc1:ucLabel>
                                            </td>
                                            <td style="font-weight: bold;" align="center">
                                                <cc1:ucLiteral ID="ltMDCPallets" runat="server" Text="0"></cc1:ucLiteral>
                                            </td>
                                            <td style="font-weight: bold;" align="center">
                                                <cc1:ucLiteral ID="ltMDCLines" runat="server" Text="0"></cc1:ucLiteral>
                                            </td>
                                            <td style="font-weight: bold;" align="center">
                                                <cc1:ucLiteral ID="ltMDCLifts" runat="server" Text="0"></cc1:ucLiteral>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <cc1:ucLabel ID="lblCurrentRunningTotal" runat="server" Text="Current Running Total"></cc1:ucLabel>
                                            </td>
                                            <td style="font-weight: bold;" align="center">
                                                <cc1:ucLiteral ID="ltCRTPallets" runat="server" Text="0"></cc1:ucLiteral>
                                            </td>
                                            <td style="font-weight: bold;" align="center">
                                                <cc1:ucLiteral ID="ltCRTLines" runat="server" Text="0"></cc1:ucLiteral>
                                            </td>
                                            <td style="font-weight: bold;" align="center">
                                                <cc1:ucLiteral ID="ltCRTLifts" runat="server" Text="0"></cc1:ucLiteral>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <cc1:ucLabel ID="lblAvailableCapacity" runat="server" Text="Available Capacity"></cc1:ucLabel>
                                            </td>
                                            <td style="font-weight: bold;" align="center">
                                                <cc1:ucLiteral ID="ltACPallets" runat="server" Text="0"></cc1:ucLiteral>
                                            </td>
                                            <td style="font-weight: bold;" align="center">
                                                <cc1:ucLiteral ID="ltACLines" runat="server" Text="0"></cc1:ucLiteral>
                                            </td>
                                            <td style="font-weight: bold;" align="center">
                                                <cc1:ucLiteral ID="ltACLifts" runat="server" Text="0"></cc1:ucLiteral>
                                            </td>
                                        </tr>
                                    </table>
                                </cc1:ucPanel>
                            </td>
                            <td>
                                &nbsp;&nbsp;
                            </td>
                            <td valign="bottom" width="45%" valign="top">
                                <cc1:ucPanel ID="pnlHoursOverview" runat="server" CssClass="fieldset-form" GroupingText="Hours Overview">
                                    <table cellpadding="0" cellspacing="5" class="form-table" height="135px" width="100%">
                                        <tr>
                                            <th width="51%" align="left" colspan="2">
                                                &nbsp;
                                            </th>
                                            <th width="25%">
                                                <cc1:ucLabel ID="lblConfirmed" runat="server" Text="Confirmed"></cc1:ucLabel>
                                            </th>
                                            <th width="24%">
                                                <cc1:ucLabel ID="lblConfirmedExpected" runat="server" Text="Confirmed + Expected"></cc1:ucLabel>
                                            </th>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: bold;">
                                                <cc1:ucLabel ID="lblNumberofRequiredHrs" runat="server" Text="Number of Required Hrs"></cc1:ucLabel>
                                            </td>
                                            <td style="font-weight: bold; width: 1px;">
                                                :
                                            </td>
                                            <td style="font-weight: bold;" align="center">
                                                <cc1:ucLiteral ID="ltHoursRequired" runat="server" Text="0"></cc1:ucLiteral>
                                            </td>
                                            <td style="font-weight: bold;" align="center">
                                                <cc1:ucLiteral ID="ltHoursRequiredTotal" runat="server" Text="0"></cc1:ucLiteral>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: bold;">
                                                <cc1:ucLabel ID="lblNumberofUnloadingHrs" runat="server" Text="Number of Unloading Hrs"></cc1:ucLabel>
                                            </td>
                                            <td style="font-weight: bold;">
                                                :
                                            </td>
                                            <td style="font-weight: bold;" align="center">
                                                <cc1:ucLiteral ID="ltUnloadingHrs" runat="server" Text="0"></cc1:ucLiteral>
                                            </td>
                                            <td style="font-weight: bold;" align="center">
                                                <cc1:ucLiteral ID="ltUnloadingHrsTotal" runat="server" Text="0"></cc1:ucLiteral>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: bold;">
                                                <cc1:ucLabel ID="lblNumberofReceivingHrs" runat="server" Text="Number of Receiving Hrs"></cc1:ucLabel>
                                            </td>
                                            <td style="font-weight: bold;">
                                                :
                                            </td>
                                            <td style="font-weight: bold;" align="center">
                                                <cc1:ucLiteral ID="ltReceivingHrs" runat="server" Text="0"></cc1:ucLiteral>
                                            </td>
                                            <td style="font-weight: bold;" align="center">
                                                <cc1:ucLiteral ID="ltReceivingHrsTotal" runat="server" Text="0"></cc1:ucLiteral>
                                            </td>
                                        </tr>

                                         <tr>
                                            <td style="font-weight: bold;">
                                                <cc1:ucLabel ID="lblNumberofPutawayHrs" runat="server" Text="Number of Putaway Hrs"></cc1:ucLabel>
                                            </td>
                                            <td style="font-weight: bold;">
                                                :
                                            </td>
                                            <td style="font-weight: bold;" align="center">
                                                <cc1:ucLiteral ID="ltPutawayHrs" runat="server" Text="0"></cc1:ucLiteral>
                                            </td>
                                            <td style="font-weight: bold;" align="center">
                                                <cc1:ucLiteral ID="ltPutawayHrsTotal" runat="server" Text="0"></cc1:ucLiteral>
                                            </td>
                                        </tr>
                                    </table>
                                </cc1:ucPanel>
                            </td>
                        </tr>
                        <tr>
                            <td ID="trVolumes" runat="server" valign="bottom">
                                <cc1:ucPanel ID="pnlVolumes" runat="server" GroupingText="Volumes" CssClass="fieldset-form">
                                    <table width="100%" cellspacing="5" cellpadding="0" height="170px" class="form-table">
                                        <tr>
                                            <th width="37%" align="left">
                                                <cc1:ucLabel ID="lblVolumes" runat="server" Text="Volumes"></cc1:ucLabel>
                                            </th>
                                            <th width="21%">
                                                <cc1:ucLabel ID="lblConfirmed_1" runat="server" Text="Confirmed"></cc1:ucLabel>
                                            </th>
                                             <th width="15%">
                                                <cc1:ucLabel ID="lblArrived" runat="server" Text="Arrived"></cc1:ucLabel>
                                            </th>
                                             <th width="21%">
                                                <cc1:ucLabel ID="lblUnloaded" runat="server" Text="Unloaded"></cc1:ucLabel>
                                            </th>                                           
                                            <th width="15%">
                                                <cc1:ucLabel ID="lblStilltobeDelivered" runat="server" Text="Still to be Delivered"></cc1:ucLabel>
                                            </th>
                                             <th width="21%">
                                                <cc1:ucLabel ID="lblStilltobeUnLoaded" runat="server" Text="Still to be Unloaded"></cc1:ucLabel>
                                            </th>
                                        </tr>
                                        <tr align="center">
                                            <td style="font-weight: bold;" align="left">
                                                <cc1:ucLabel ID="lblNoOfDeliveries" runat="server" Text="# of Deliveries"></cc1:ucLabel>
                                            </td>
                                            <td style="font-weight: bold;">
                                                <cc1:ucLiteral ID="ltDeliveriesExpectedConfirmed" runat="server" Text="0"></cc1:ucLiteral>
                                            </td>
                                            <td style="font-weight: bold;">
                                                <cc1:ucLiteral ID="ltDeliveriesArrived" runat="server" Text="0"></cc1:ucLiteral>
                                            </td>
                                            <td style="font-weight: bold;">
                                                <cc1:ucLiteral ID="ltDeliveriesUnloaded" runat="server" Text="0"></cc1:ucLiteral>
                                            </td>
                                            <td style="font-weight: bold;">
                                                <cc1:ucLiteral ID="ltDeliveriesStillToBeDelivered" runat="server" Text="0"></cc1:ucLiteral>
                                            </td>
                                             <td style="font-weight: bold;">
                                                <cc1:ucLiteral ID="ltDeliveriesStillToBeUnloaded" runat="server" Text="0"></cc1:ucLiteral>
                                            </td>
                                        </tr>
                                        <tr align="center">
                                            <td style="font-weight: bold;" align="left">
                                                <cc1:ucLabel ID="lblNoOfLifts" runat="server" Text="# of Lifts"></cc1:ucLabel>
                                            </td>
                                            <td style="font-weight: bold;">
                                                <cc1:ucLiteral ID="ltLiftsExpectedConfirmed" runat="server" Text="0"></cc1:ucLiteral>
                                            </td>
                                             <td style="font-weight: bold;">
                                                <cc1:ucLiteral ID="ltLiftsArrived" runat="server" Text="0"></cc1:ucLiteral>
                                            </td>
                                            <td style="font-weight: bold;">
                                                <cc1:ucLiteral ID="ltLiftsUnloaded" runat="server" Text="0"></cc1:ucLiteral>
                                            </td>                                            
                                            <td style="font-weight: bold;">
                                                <cc1:ucLiteral ID="ltLiftsStillToBeDelivered" runat="server" Text="0"></cc1:ucLiteral>
                                            </td>
                                            <td style="font-weight: bold;">
                                                <cc1:ucLiteral ID="ltLiftsStillToBeUnloaded" runat="server" Text="0"></cc1:ucLiteral>
                                            </td
                                        </tr>
                                        <tr align="center">
                                            <td style="font-weight: bold;" align="left">
                                                <cc1:ucLabel ID="lblNoOfPallets" runat="server" Text="# of Pallets"></cc1:ucLabel>
                                            </td>
                                            <td style="font-weight: bold;">
                                                <cc1:ucLiteral ID="ltPalletsExtectedConfirmed" runat="server" Text="0"></cc1:ucLiteral>
                                            </td>
                                             <td style="font-weight: bold;">
                                                <cc1:ucLiteral ID="ltPalletsArrived" runat="server" Text="0"></cc1:ucLiteral>
                                            </td>
                                            <td style="font-weight: bold;">
                                                <cc1:ucLiteral ID="ltPalletsUnloaded" runat="server" Text="0"></cc1:ucLiteral>
                                            </td>   
                                            <td style="font-weight: bold;">
                                                <cc1:ucLiteral ID="ltPalletsStillToBeDelivered" runat="server" Text="0"></cc1:ucLiteral>
                                            </td>
                                            <td style="font-weight: bold;">
                                                <cc1:ucLiteral ID="ltPalletsStillToBeUnloaded" runat="server" Text="0"></cc1:ucLiteral>
                                            </td>
                                        </tr>
                                        <tr align="center">
                                            <td style="font-weight: bold;" align="left">
                                                <cc1:ucLabel ID="lblNoOfLines" runat="server" Text="# of Lines"></cc1:ucLabel>
                                            </td>
                                            <td style="font-weight: bold;">
                                                <cc1:ucLiteral ID="ltLinesExtectedConfirmed" runat="server" Text="0"></cc1:ucLiteral>
                                            </td>
                                           <td style="font-weight: bold;">
                                                <cc1:ucLiteral ID="ltLinesArrived" runat="server" Text="0"></cc1:ucLiteral>
                                            </td>
                                            <td style="font-weight: bold;">
                                                <cc1:ucLiteral ID="ltLinesUnloaded" runat="server" Text="0"></cc1:ucLiteral>
                                            </td>                                          
                                            <td style="font-weight: bold;">
                                                <cc1:ucLiteral ID="ltLinesStillToBeDelivered" runat="server" Text="0"></cc1:ucLiteral>
                                            </td>
                                              <td style="font-weight: bold;">
                                                <cc1:ucLiteral ID="ltLinesStillToBeUnloaded" runat="server" Text="0"></cc1:ucLiteral>
                                            </td>
                                        </tr>
                                        <tr align="center">
                                            <td style="font-weight: bold;" align="left">
                                                <cc1:ucLabel ID="lblNoOfCartons" runat="server" Text="# of Cartons"></cc1:ucLabel>
                                            </td>
                                            <td style="font-weight: bold;">
                                                <cc1:ucLiteral ID="ltCartonsExtectedConfirmed" runat="server" Text="0"></cc1:ucLiteral>
                                            </td>
                                            <td style="font-weight: bold;">
                                                <cc1:ucLiteral ID="ltCartonsArrived" runat="server" Text="0"></cc1:ucLiteral>
                                            </td>
                                            <td style="font-weight: bold;">
                                                <cc1:ucLiteral ID="ltCartonsUnloaded" runat="server" Text="0"></cc1:ucLiteral>
                                            </td> 
                                            <td style="font-weight: bold;">
                                                <cc1:ucLiteral ID="ltCartonsStillToBeDelivered" runat="server" Text="0"></cc1:ucLiteral>
                                            </td>
                                             <td style="font-weight: bold;">
                                                <cc1:ucLiteral ID="ltCartonsStillToBeUnloaded" runat="server" Text="0"></cc1:ucLiteral>
                                            </td>
                                        </tr>
                                    </table>
                                </cc1:ucPanel>
                            </td>
                               <td  ID="trVolumesWindow" runat="server" valign="bottom">
                                <cc1:ucPanel ID="pnlVolumesWindow" runat="server" GroupingText="Volumes" CssClass="fieldset-form">
                                    <table width="100%" cellspacing="5" cellpadding="0" height="170px" class="form-table">
                                        <tr>
                                            <th width="37%" align="left">
                                                <cc1:ucLabel ID="lblVolumes_1" runat="server" Text="Volumes"></cc1:ucLabel>
                                            </th>
                                            <th width="21%">
                                                <cc1:ucLabel ID="lblConfirmed_11" runat="server" Text="Confirmed"></cc1:ucLabel>
                                            </th>
                                            <th width="21%">
                                                <cc1:ucLabel ID="lblReceivedVolumes_1" runat="server" Text="Received Volumes"></cc1:ucLabel>
                                            </th>
                                            <th width="21%">
                                                <cc1:ucLabel ID="lblStilltobeDelivered_1" runat="server" Text="Still to be Delivered"></cc1:ucLabel>
                                            </th>
                                        </tr>
                                        <tr align="center">
                                            <td style="font-weight: bold;" align="left">
                                                <cc1:ucLabel ID="lblNoOfDeliveries_1" runat="server" Text="# of Deliveries"></cc1:ucLabel>
                                            </td>
                                            <td style="font-weight: bold;">
                                                <cc1:ucLiteral ID="ltDeliveriesExpectedConfirmed_1" runat="server" Text="0"></cc1:ucLiteral>
                                            </td>
                                            <td style="font-weight: bold;">
                                                <cc1:ucLiteral ID="ltDeliveriesReceived_1" runat="server" Text="0"></cc1:ucLiteral>
                                            </td>
                                            <td style="font-weight: bold;">
                                                <cc1:ucLiteral ID="ltDeliveriesStillToBeDelivered_1" runat="server" Text="0"></cc1:ucLiteral>
                                            </td>
                                        </tr>
                                        <tr align="center">
                                            <td style="font-weight: bold;" align="left">
                                                <cc1:ucLabel ID="lblNoOfLifts_1" runat="server" Text="# of Lifts"></cc1:ucLabel>
                                            </td>
                                            <td style="font-weight: bold;">
                                                <cc1:ucLiteral ID="ltLiftsExpectedConfirmed_1" runat="server" Text="0"></cc1:ucLiteral>
                                            </td>
                                            <td style="font-weight: bold;">
                                                <cc1:ucLiteral ID="ltLiftsReceived_1" runat="server" Text="0"></cc1:ucLiteral>
                                            </td>
                                            <td style="font-weight: bold;">
                                                <cc1:ucLiteral ID="ltLiftsStillToBeDelivered_1" runat="server" Text="0"></cc1:ucLiteral>
                                            </td>
                                        </tr>
                                        <tr align="center">
                                            <td style="font-weight: bold;" align="left">
                                                <cc1:ucLabel ID="lblNoOfPallets_1" runat="server" Text="# of Pallets"></cc1:ucLabel>
                                            </td>
                                            <td style="font-weight: bold;">
                                                <cc1:ucLiteral ID="ltPalletsExtectedConfirmed_1" runat="server" Text="0"></cc1:ucLiteral>
                                            </td>
                                            <td style="font-weight: bold;">
                                                <cc1:ucLiteral ID="ltPalletsReceived_1" runat="server" Text="0"></cc1:ucLiteral>
                                            </td>
                                            <td style="font-weight: bold;">
                                                <cc1:ucLiteral ID="ltPalletsStillToBeDelivered_1" runat="server" Text="0"></cc1:ucLiteral>
                                            </td>
                                        </tr>
                                        <tr align="center">
                                            <td style="font-weight: bold;" align="left">
                                                <cc1:ucLabel ID="lblNoOfLines_1" runat="server" Text="# of Lines"></cc1:ucLabel>
                                            </td>
                                            <td style="font-weight: bold;">
                                                <cc1:ucLiteral ID="ltLinesExtectedConfirmed_1" runat="server" Text="0"></cc1:ucLiteral>
                                            </td>
                                            <td style="font-weight: bold;">
                                                <cc1:ucLiteral ID="ltLinesReceived_1" runat="server" Text="0"></cc1:ucLiteral>
                                            </td>
                                            <td style="font-weight: bold;">
                                                <cc1:ucLiteral ID="ltLinesStillToBeDelivered_1" runat="server" Text="0"></cc1:ucLiteral>
                                            </td>
                                        </tr>
                                        <tr align="center">
                                            <td style="font-weight: bold;" align="left">
                                                <cc1:ucLabel ID="lblNoOfCartons_1" runat="server" Text="# of Cartons"></cc1:ucLabel>
                                            </td>
                                            <td style="font-weight: bold;">
                                                <cc1:ucLiteral ID="ltCartonsExtectedConfirmed_1" runat="server" Text="0"></cc1:ucLiteral>
                                            </td>
                                            <td style="font-weight: bold;">
                                                <cc1:ucLiteral ID="ltCartonsReceived_1" runat="server" Text="0"></cc1:ucLiteral>
                                            </td>
                                            <td style="font-weight: bold;">
                                                <cc1:ucLiteral ID="ltCartonsStillToBeDelivered_1" runat="server" Text="0"></cc1:ucLiteral>
                                            </td>
                                        </tr>                     
                    </table>
                     </cc1:ucPanel>
                             </td>
                            <td>
                                &nbsp;&nbsp;
                            </td>
                            <td valign="bottom">
                                <cc1:ucPanel ID="pnlFixedBookingOverview" runat="server" GroupingText="Fixed Booking Overview"
                                    CssClass="fieldset-form">
                                    <table width="100%" cellspacing="5" cellpadding="0" height="170px" class="form-table">
                                        <tr>
                                            <th width="60%" align="left">
                                                <cc1:ucLabel ID="lblFixedSlotDetails" runat="server" Text="Fixed Slot Details"></cc1:ucLabel>
                                            </th>
                                            <th width="14%" style="font-weight: bold;">
                                                <cc1:ucLabel ID="lblConfirmed_2" runat="server" Text="Confirmed"></cc1:ucLabel>
                                            </th>
                                            <th width="13%" style="font-weight: bold;">
                                                <cc1:ucLabel ID="lblOpenHard" runat="server" Text="Open (Hard)"></cc1:ucLabel>
                                            </th>
                                            <th width="13%" style="font-weight: bold;">
                                                <cc1:ucLabel ID="lblOpenSoft" runat="server" Text="Open (Soft)"></cc1:ucLabel>
                                            </th>
                                            <th width="13%" style="font-weight: bold;">
                                                <cc1:ucLabel ID="lblTotal" runat="server" Text="Total"></cc1:ucLabel>
                                            </th>
                                        </tr>
                                        <tr>
                                            <td>
                                                <cc1:ucLabel ID="lblNoOfFixedSlotDeliveries" runat="server" Text="# Fixed Slot Deliveries"></cc1:ucLabel>
                                            </td>
                                            <td style="font-weight: bold;" align="center">
                                                <cc1:ucLiteral ID="ltFSCDeliveries" runat="server" Text="0"></cc1:ucLiteral>
                                            </td>
                                            <td style="font-weight: bold;" align="center">
                                                <cc1:ucLiteral ID="ltFSODeliveries" runat="server" Text="0"></cc1:ucLiteral>
                                            </td>
                                             <td style="font-weight: bold;" align="center">
                                                <cc1:ucLiteral ID="ltFSDeliveries_SOft" runat="server" Text="0"></cc1:ucLiteral>
                                            </td>
                                            <td style="font-weight: bold;" align="center">
                                                <cc1:ucLiteral ID="ltFSTDeliveries" runat="server" Text="0"></cc1:ucLiteral>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <cc1:ucLabel ID="lblNoOfFixedSlotPallets" runat="server" Text="# Fixed Slot Pallets"></cc1:ucLabel>
                                            </td>
                                            <td style="font-weight: bold;" align="center">
                                                <cc1:ucLiteral ID="ltFSCPallets" runat="server" Text="0"></cc1:ucLiteral>
                                            </td>
                                            <td style="font-weight: bold;" align="center">
                                                <cc1:ucLiteral ID="ltFSOPallets" runat="server" Text="0"></cc1:ucLiteral>
                                            </td>
                                             <td style="font-weight: bold;" align="center">
                                                <cc1:ucLiteral ID="ltFSOPallets_SOft" runat="server" Text="0"></cc1:ucLiteral>
                                            </td>
                                            <td style="font-weight: bold;" align="center">
                                                <cc1:ucLiteral ID="ltFSTPallets" runat="server" Text="0"></cc1:ucLiteral>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <cc1:ucLabel ID="lblNoOfFixedSlotCartons" runat="server" Text="# Fixed Slot Cartons"></cc1:ucLabel>
                                            </td>
                                            <td style="font-weight: bold;" align="center">
                                                <cc1:ucLiteral ID="ltFSCCartons" runat="server" Text="0"></cc1:ucLiteral>
                                            </td>
                                            <td style="font-weight: bold;" align="center">
                                                <cc1:ucLiteral ID="ltFSOCartons" runat="server" Text="0"></cc1:ucLiteral>
                                            </td>
                                             <td style="font-weight: bold;" align="center">
                                                <cc1:ucLiteral ID="ltFSOCartons_Soft" runat="server" Text="0"></cc1:ucLiteral>
                                            </td>
                                            <td style="font-weight: bold;" align="center">
                                                <cc1:ucLiteral ID="ltFSTCartons" runat="server" Text="0"></cc1:ucLiteral>
                                            </td>
                                        </tr>

                                         <tr>
                                            <td>
                                                <cc1:ucLabel ID="lblFixedSlotLines" runat="server" Text="# Fixed Slot Lines"></cc1:ucLabel>
                                            </td>
                                            <td style="font-weight: bold;" align="center">
                                                <cc1:ucLiteral ID="ltFSCLines" runat="server" Text="0"></cc1:ucLiteral>
                                            </td>
                                            <td style="font-weight: bold;" align="center">
                                                <cc1:ucLiteral ID="ltFSOLines" runat="server" Text="0"></cc1:ucLiteral>
                                            </td>
                                             <td style="font-weight: bold;" align="center">
                                                <cc1:ucLiteral ID="ltFSOLines_Soft" runat="server" Text="0"></cc1:ucLiteral>
                                            </td>
                                            <td style="font-weight: bold;" align="center">
                                                <cc1:ucLiteral ID="ltFSTLines" runat="server" Text="0"></cc1:ucLiteral>
                                            </td>
                                        </tr>
                                    </table>
                                </cc1:ucPanel>
                            </td>
                        </tr>                       
<tr>
                            <td colspan="3" align="right">
                                <cc1:ucButton ID="btnWindowsVolume" runat="server" Text="Windows Volume" CssClass="button"
                                    OnClick="btnWindowsVolume_Click" />
                                <cc1:ucButton ID="btnBack" runat="server" Text="Back" CssClass="button" OnClick="btnBack_Click" />
                            </td>
                        </tr>
                    </table>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="btnWindowsVolume" />
                <asp:AsyncPostBackTrigger ControlID="btnBack" />
            </Triggers>
        </asp:UpdatePanel>
    </div>
    <asp:UpdatePanel ID="updpnlShowWindowVolume" runat="server">
        <ContentTemplate>
            <asp:Button ID="btnShowWindowVolume" runat="Server" Style="display: none" />
            <ajaxToolkit:ModalPopupExtender ID="mdlShowWindowVolume" runat="server" TargetControlID="btnShowWindowVolume"
                PopupControlID="pnlShowWindowVolume" BackgroundCssClass="modalBackground" BehaviorID="ShowWindowVolume"
                DropShadow="false" />
            <asp:Panel ID="pnlShowWindowVolume" runat="server" Style="display: none; width: 80%">
                <div style="overflow-y: hidden; overflow-x: hidden; background-color: #fff; padding: 5px;
                    border: 2px solid #ccc; text-align: left;">
                    <table cellspacing="5" cellpadding="0" border="0" align="center" class="top-settingsNoBorder-popup"
                        width="100%">
                        <tr>
                            <td style="width: 100%;">
                                <cc1:ucPanel ID="pnlVolumeDetails" runat="server" GroupingText="Volume Details" CssClass="fieldset-form">
                                    <table border="0" cellpadding="0" cellspacing="0" style="border-bottom: none; width: 100%;">
                                        <tr>
                                            <td align="center">
                                                <table>
                                                    <tr>
                                                        <td style="font-weight: bold;">
                                                            <asp:Label ID="lblSitePrefixName" Text="Site" runat="server"></asp:Label>
                                                        </td>
                                                        <td style="font-weight: bold;">
                                                            :
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="lblSiteT" Text="" runat="server"></asp:Label>
                                                        </td>
                                                        <td style="width: 150px;">
                                                        </td>
                                                        <td style="font-weight: bold;">
                                                            <asp:Label ID="lblActualDate" Text="Date" runat="server"></asp:Label>
                                                        </td>
                                                        <td style="font-weight: bold;">
                                                            :
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="lblDateT" Text="" runat="server"></asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    <br />
                                    <%--<table  class="grid topheadgrid" border="0" cellpadding="0" cellspacing="0" style="border-bottom:none;">                                        
                                        <tr>                                            
                                            <th style="width:402px">&nbsp;</th>                                            
                                            <th style="width:260px; border-right:solid 3px #fff; border-left:solid 3px #fff;" colspan="3">
                                                <asp:Label ID="lblMax" Text="Max" runat="server"></asp:Label>                                                   
                                            </th>   
                                            <th style="width:260px; border-right:solid 3px #fff;" colspan="3">
                                                <asp:Label ID="lblCurrent" Text="Current" runat="server"></asp:Label>
                                            </th>
                                            <th style="width:200px">&nbsp;</th>                                         
                                        </tr>
                                    </table>--%>
                                    <%--<cc1:ucGridView ID="gvVolumeDetails" runat="server" CssClass="grid" OnRowDataBound="gvVolumeDetails_RowDataBound"
                                        OnSorting="SortGrid" AllowSorting="false" Style="overflow: auto;">
                                        <Columns>
                                            <asp:TemplateField HeaderText="WindowName" SortExpression="TimeWindow.WindowName">
                                                <HeaderStyle HorizontalAlign="Left" Width="200px" />
                                                <ItemStyle HorizontalAlign="Left" />
                                                <ItemTemplate>
                                                    <asp:HiddenField ID="hdnTimeWindowID" Value='<%#Eval("TimeWindow.TimeWindowID") %>' runat="server" />                                                    
                                                    <cc1:ucLabel ID="lblWindowNameT" Text='<%#Eval("TimeWindow.WindowName") %>' runat="server"></cc1:ucLabel>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="SiteWeekSetupStart" SortExpression="TimeWindow.StartTime">
                                                <HeaderStyle HorizontalAlign="Left" Width="60px" />
                                                <ItemStyle HorizontalAlign="Left" />
                                                <ItemTemplate>
                                                    <cc1:ucLabel ID="lblStartT" Text='<%#Eval("TimeWindow.StartTime") %>' runat="server"></cc1:ucLabel>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="SiteWeekSetupEnd" SortExpression="TimeWindow.EndTime">
                                                <HeaderStyle HorizontalAlign="Left" Width="60px" />
                                                <ItemStyle HorizontalAlign="Left" />
                                                <ItemTemplate>
                                                    <cc1:ucLabel ID="lblEndT" Text='<%#Eval("TimeWindow.EndTime") %>' runat="server"></cc1:ucLabel>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Door" SortExpression="TimeWindow.DoorNumber">
                                                <HeaderStyle HorizontalAlign="Left" Width="60px" />
                                                <ItemStyle HorizontalAlign="Left" />
                                                <ItemTemplate>
                                                    <cc1:ucLabel ID="lblDoorT" Text='<%#Eval("TimeWindow.DoorNumber") %>' runat="server"></cc1:ucLabel>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Priority" SortExpression="TimeWindow.Priority">
                                                <HeaderStyle HorizontalAlign="Left" Width="60px" />
                                                <ItemStyle HorizontalAlign="Left" />
                                                <ItemTemplate>
                                                    <cc1:ucLabel ID="lblPriorityT" Text='<%#Eval("TimeWindow.Priority") %>' runat="server"></cc1:ucLabel>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="ActualPallets" SortExpression="TimeWindow.MaximumPallets">
                                                <HeaderStyle HorizontalAlign="Left" Width="60px" />
                                                <ItemStyle HorizontalAlign="Left" />
                                                <ItemTemplate>
                                                    <cc1:ucLabel ID="lblMaxPalletsT" Text='<%#Eval("TimeWindow.MaximumPallets") %>' runat="server"></cc1:ucLabel>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="ActualCartons" SortExpression="TimeWindow.MaximumCartons">
                                                <HeaderStyle HorizontalAlign="Left" Width="60px" />
                                                <ItemStyle HorizontalAlign="Left" />
                                                <ItemTemplate>
                                                    <cc1:ucLabel ID="lblMaxCortonsT" Text='<%#Eval("TimeWindow.MaximumCartons") %>' runat="server"></cc1:ucLabel>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="ActualLines" SortExpression="TimeWindow.MaximumLines">
                                                <HeaderStyle HorizontalAlign="Left" Width="60px" />
                                                <ItemStyle HorizontalAlign="Left" />
                                                <ItemTemplate>
                                                    <cc1:ucLabel ID="lblMaxLinesT" Text='<%#Eval("TimeWindow.MaximumLines") %>' runat="server"></cc1:ucLabel>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="ActualPallets" SortExpression="NumberOfPallet">
                                                <HeaderStyle HorizontalAlign="Left" Width="60px" />
                                                <ItemStyle HorizontalAlign="Left" />
                                                <ItemTemplate>
                                                    <cc1:ucLabel ID="lblCurrentPalletsT" Text='<%#Eval("NumberOfPallet") %>' runat="server"></cc1:ucLabel>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="ActualCartons" SortExpression="NumberOfCartons">
                                                <HeaderStyle HorizontalAlign="Left" Width="60px" />
                                                <ItemStyle HorizontalAlign="Left" />
                                                <ItemTemplate>
                                                    <cc1:ucLabel ID="lblCurrentCortonsT" Text='<%#Eval("NumberOfCartons") %>' runat="server"></cc1:ucLabel>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="ActualLines" SortExpression="NumberOfLines">
                                                <HeaderStyle HorizontalAlign="Left" Width="60px" />
                                                <ItemStyle HorizontalAlign="Left" />
                                                <ItemTemplate>
                                                    <cc1:ucLabel ID="lblCurrentLinesT" Text='<%#Eval("NumberOfLines") %>' runat="server"></cc1:ucLabel>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="TotalTime" SortExpression="TimeWindow.TotalHrsMinsAvailable">
                                                <HeaderStyle HorizontalAlign="Left" Width="80px" />
                                                <ItemStyle HorizontalAlign="Left" />
                                                <ItemTemplate>
                                                    <cc1:ucLabel ID="lblTotalTimeT" Text='<%#Eval("TimeWindow.TotalHrsMinsAvailable") %>' runat="server"></cc1:ucLabel>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="TimeRemaining" SortExpression="">
                                                <HeaderStyle HorizontalAlign="Left" Width="110px" />
                                                <ItemStyle HorizontalAlign="Left" />
                                                <ItemTemplate>
                                                    <cc1:ucLabel ID="lblTimeRemainingT" runat="server"></cc1:ucLabel>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </cc1:ucGridView>--%>
                                    <asp:Repeater ID="rptVolumeDetails" runat="server" OnItemDataBound="rptVolumeDetails_ItemDataBound">
                                        <HeaderTemplate>
                                            <table class="col-grid grid" border="0" cellpadding="0" cellspacing="0" width="100%">
                                                <tr>
                                                    <th colspan="5">
                                                        &nbsp;
                                                    </th>
                                                    <th colspan="3" style="text-align: center;">
                                                        <asp:Label ID="lblMax" Text="Max" runat="server"></asp:Label>
                                                    </th>
                                                    <th colspan="3" style="text-align: center;">
                                                        <asp:Label ID="lblCurrent" Text="Current" runat="server"></asp:Label>
                                                    </th>
                                                    <th colspan="2">
                                                        &nbsp;
                                                    </th>
                                                </tr>
                                                <tr>
                                                    <th>
                                                        <asp:Label ID="lblWindowName" Text="Window Name" runat="server"></asp:Label>
                                                    </th>
                                                    <th>
                                                        <asp:Label ID="lblSiteWeekSetupStart" Text="Start" runat="server"></asp:Label>
                                                    </th>
                                                    <th>
                                                        <asp:Label ID="lblSiteWeekSetupEnd" Text="End" runat="server"></asp:Label>
                                                    </th>
                                                    <th>
                                                        <asp:Label ID="lblDoor" Text="Door" runat="server"></asp:Label>
                                                    </th>
                                                    <th>
                                                        <asp:Label ID="lblPriority" Text="Priority" runat="server"></asp:Label>
                                                    </th>
                                                    <th>
                                                        <asp:Label ID="lblActualPallets" Text="Pallets" runat="server"></asp:Label>
                                                    </th>
                                                    <th>
                                                        <asp:Label ID="lblActualCartons" Text="Cartons" runat="server"></asp:Label>
                                                    </th>
                                                    <th>
                                                        <asp:Label ID="lblActualLines" Text="Lines" runat="server"></asp:Label>
                                                    </th>
                                                    <th>
                                                        <asp:Label ID="lblActualPallets_1" Text="Pallets" runat="server"></asp:Label>
                                                    </th>
                                                    <th>
                                                        <asp:Label ID="lblActualCartons_1" Text="Cartons" runat="server"></asp:Label>
                                                    </th>
                                                    <th>
                                                        <asp:Label ID="lblActualLines_1" Text="Lines" runat="server"></asp:Label>
                                                    </th>
                                                    <th>
                                                        <asp:Label ID="lblTotalTime" Text="Total Time" runat="server"></asp:Label>
                                                    </th>
                                                    <th>
                                                        <asp:Label ID="lblTimeRemaining" Text="Time Remaining" runat="server"></asp:Label>
                                                    </th>
                                                </tr>
                                        </HeaderTemplate>
                                        <AlternatingItemTemplate>
                                            <tr>
                                                <td>
                                                    <asp:HiddenField ID="hdnTimeWindowID" Value='<%# Bind("TimeWindow.TimeWindowID") %>'
                                                        runat="server" />
                                                    <cc1:ucLabel ID="lblWindowNameT" Text='<%# Bind("TimeWindow.WindowName") %>' runat="server"></cc1:ucLabel>
                                                </td>
                                                <td>
                                                    <cc1:ucLabel ID="lblStartT" Text='<%# Bind("TimeWindow.StartTime") %>' runat="server"></cc1:ucLabel>
                                                </td>
                                                <td>
                                                    <cc1:ucLabel ID="lblEndT" Text='<%# Bind("TimeWindow.EndTime") %>' runat="server"></cc1:ucLabel>
                                                </td>
                                                <td>
                                                    <cc1:ucLabel ID="lblDoorT" Text='<%# Bind("TimeWindow.DoorNumber") %>' runat="server"></cc1:ucLabel>
                                                </td>
                                                <td>
                                                    <cc1:ucLabel ID="lblPriorityT" Text='<%# Bind("TimeWindow.Priority") %>' runat="server"></cc1:ucLabel>
                                                </td>
                                                <td>
                                                    <cc1:ucLabel ID="lblMaxPalletsT" Text='<%# Bind("TimeWindow.MaximumPallets") %>'
                                                        runat="server"></cc1:ucLabel>
                                                </td>
                                                <td>
                                                    <cc1:ucLabel ID="lblMaxCortonsT" Text='<%# Bind("TimeWindow.MaximumCartons") %>'
                                                        runat="server"></cc1:ucLabel>
                                                </td>
                                                <td>
                                                    <cc1:ucLabel ID="lblMaxLinesT" Text='<%# Bind("TimeWindow.MaximumLines") %>' runat="server"></cc1:ucLabel>
                                                </td>
                                                <td>
                                                    <cc1:ucLabel ID="lblCurrentPalletsT" Text='<%# Bind("NumberOfPallet") %>' runat="server"></cc1:ucLabel>
                                                </td>
                                                <td>
                                                    <cc1:ucLabel ID="lblCurrentCortonsT" Text='<%# Bind("NumberOfCartons") %>' runat="server"></cc1:ucLabel>
                                                </td>
                                                <td>
                                                    <cc1:ucLabel ID="lblCurrentLinesT" Text='<%# Bind("NumberOfLines") %>' runat="server"></cc1:ucLabel>
                                                </td>
                                                <td>
                                                    <cc1:ucLabel ID="lblTotalTimeT" Text='<%# Bind("TimeWindow.TotalHrsMinsAvailable") %>'
                                                        runat="server"></cc1:ucLabel>
                                                </td>
                                                <td>
                                                    <cc1:ucLabel ID="lblTimeRemainingT" runat="server"></cc1:ucLabel>
                                                </td>
                                            </tr>
                                        </AlternatingItemTemplate>
                                        <ItemTemplate>
                                            <tr class="altcolor">
                                                <td>
                                                    <asp:HiddenField ID="hdnTimeWindowID" Value='<%# Bind("TimeWindow.TimeWindowID") %>'
                                                        runat="server" />
                                                    <cc1:ucLabel ID="lblWindowNameT" Text='<%# Bind("TimeWindow.WindowName") %>' runat="server"></cc1:ucLabel>
                                                </td>
                                                <td>
                                                    <cc1:ucLabel ID="lblStartT" Text='<%# Bind("TimeWindow.StartTime") %>' runat="server"></cc1:ucLabel>
                                                </td>
                                                <td>
                                                    <cc1:ucLabel ID="lblEndT" Text='<%# Bind("TimeWindow.EndTime") %>' runat="server"></cc1:ucLabel>
                                                </td>
                                                <td>
                                                    <cc1:ucLabel ID="lblDoorT" Text='<%# Bind("TimeWindow.DoorNumber") %>' runat="server"></cc1:ucLabel>
                                                </td>
                                                <td>
                                                    <cc1:ucLabel ID="lblPriorityT" Text='<%# Bind("TimeWindow.Priority") %>' runat="server"></cc1:ucLabel>
                                                </td>
                                                <td>
                                                    <cc1:ucLabel ID="lblMaxPalletsT" Text='<%# Bind("TimeWindow.MaximumPallets") %>'
                                                        runat="server"></cc1:ucLabel>
                                                </td>
                                                <td>
                                                    <cc1:ucLabel ID="lblMaxCortonsT" Text='<%# Bind("TimeWindow.MaximumCartons") %>'
                                                        runat="server"></cc1:ucLabel>
                                                </td>
                                                <td>
                                                    <cc1:ucLabel ID="lblMaxLinesT" Text='<%# Bind("TimeWindow.MaximumLines") %>' runat="server"></cc1:ucLabel>
                                                </td>
                                                <td>
                                                    <cc1:ucLabel ID="lblCurrentPalletsT" Text='<%# Bind("NumberOfPallet") %>' runat="server"></cc1:ucLabel>
                                                </td>
                                                <td>
                                                    <cc1:ucLabel ID="lblCurrentCortonsT" Text='<%# Bind("NumberOfCartons") %>' runat="server"></cc1:ucLabel>
                                                </td>
                                                <td>
                                                    <cc1:ucLabel ID="lblCurrentLinesT" Text='<%# Bind("NumberOfLines") %>' runat="server"></cc1:ucLabel>
                                                </td>
                                                <td>
                                                    <cc1:ucLabel ID="lblTotalTimeT" Text='<%# Bind("TimeWindow.TotalHrsMinsAvailable") %>'
                                                        runat="server"></cc1:ucLabel>
                                                </td>
                                                <td>
                                                    <cc1:ucLabel ID="lblTimeRemainingT" runat="server"></cc1:ucLabel>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            </table>
                                        </FooterTemplate>
                                    </asp:Repeater>
                                    <cc1:ucLabel ID="lblErrorMessage" Visible="false" Font-Bold="true" ForeColor="Red"
                                        runat="server"></cc1:ucLabel>
                                </cc1:ucPanel>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" style="width: 100%;">
                                <cc1:ucButton ID="btnBack_1" runat="server" Text="Back" CssClass="button" OnCommand="btnBack_1_Click" />
                            </td>
                        </tr>
                    </table>
                </div>
                <br />
            </asp:Panel>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnBack_1" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>

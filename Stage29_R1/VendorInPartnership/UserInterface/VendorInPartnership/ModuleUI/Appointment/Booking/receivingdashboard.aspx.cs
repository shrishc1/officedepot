﻿using BusinessEntities.ModuleBE.Appointment.Booking;
using BusinessLogicLayer.ModuleBAL.Appointment.Booking;
using System;
using System.Collections.Generic;
using System.Drawing;
using WebUtilities;

public partial class ModuleUI_Appointment_Booking_receivingdashboard : CommonPage
{
    protected int SiteID;
    protected string selectedDate = DateTime.Now.ToString("dd/MM/yyyy");
    protected string strGraphData = WebCommon.getGlobalResourceValue("GraphData");
    protected void Page_Init(object sender, EventArgs e)
    {
        ddlSite.CurrentPage = this;
        ddlSite.IsAllRequired = false;
        ddlSite.IsFromBookingOverview = false;
        ddlSite.innerControlddlSite.AutoPostBack = false;
    }
    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ddlSite.innerControlddlSite.AutoPostBack = false;
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        ddlSite.innerControlddlSite.AutoPostBack = false;
        if (!IsPostBack)
        {
            APPBOK_BookingBAL oAPPBOK_BookingBAL = new APPBOK_BookingBAL();
            DateTime selectedDateNew = DateTime.Now;
            selectedDate = selectedDateNew.ToString("dd/MM/yyyy");
            SiteID = Convert.ToInt32(Session["SiteID"]);
            txtDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
            hdnJSFromDt.Value = DateTime.Now.ToString("dd/MM/yyyy");
            string action = "AllDashboard";
            var dtBookings = oAPPBOK_BookingBAL.GetDashboardBookingsBAL(action, SiteID, selectedDateNew);
            if (dtBookings.Count > 0)
            {
                ucGridView1.DataSource = dtBookings;
                ucGridView1.DataBind();
            }
        }
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        APPBOK_BookingBAL oAPPBOK_BookingBAL = new APPBOK_BookingBAL();
        DateTime selectedDateNew = !string.IsNullOrEmpty(hdnJSFromDt.Value) ? Utilities.Common.TextToDateFormat(hdnJSFromDt.Value) : DateTime.Now;
        SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
        selectedDate = hdnJSFromDt.Value;
        string action = "AllDashboard";
        List<DashboardBooking> dtBookings = oAPPBOK_BookingBAL.GetDashboardBookingsBAL(action, SiteID, selectedDateNew);
        ucGridView1.DataSource = dtBookings.Count > 0 ? dtBookings : null;
        ucGridView1.DataBind();
        txtDate.Text = hdnJSFromDt.Value;

        //  ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "", "return btnSearchClient()", true);
    }
    protected bool IsTransType(object transType)
    {
        return Convert.ToInt32(transType) > 0;
    }
    protected System.Drawing.Color IsTransColrType(object transType)
    {
        return Convert.ToInt32(transType) > 0 ? Color.Blue : Color.FromArgb(333333);
    }
}
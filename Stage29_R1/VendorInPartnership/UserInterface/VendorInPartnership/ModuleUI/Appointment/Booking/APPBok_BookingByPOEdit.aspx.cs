﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Collections.Specialized;
using System.Drawing;
using System.Web.UI.HtmlControls;
using System.Linq;

using BusinessEntities.ModuleBE.Appointment.CountrySetting;
using BusinessLogicLayer.ModuleBAL.Appointment.CountrySetting;
using Utilities;
using WebUtilities;
using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using BusinessLogicLayer.ModuleBAL.Appointment.SiteSettings;
using BusinessEntities.ModuleBE.AdminFunctions;
using BusinessLogicLayer.ModuleBAL.AdminFunctions;
using BusinessEntities.ModuleBE.Upload;
using BusinessLogicLayer.ModuleBAL.Upload;
using BaseControlLibrary;
using BusinessEntities.ModuleBE.Appointment.Booking;
using BusinessLogicLayer.ModuleBAL.Appointment.Booking;
using BusinessEntities.ModuleBE.OnTimeInFull.CountrySetting;
using BusinessLogicLayer.ModuleBAL.OnTimeInFull.CountrySetting;


public partial class APPBok_BookingByPOEdit : CommonPage
{
    #region Local Declarations ...
    protected string BK_ALT_CorrectVendor = WebCommon.getGlobalResourceValue("BK_ALT_CorrectVendor");
    protected string BK_ALT_Wait = WebCommon.getGlobalResourceValue("BK_ALT_Wait");
    protected string BK_ALT_04_INT = WebCommon.getGlobalResourceValue("BK_MS_04_INT");
    protected string BK_ALT_TimeNotAvailable = WebCommon.getGlobalResourceValue("BK_ALT_TimeNotAvailable");
    protected string BK_ALT_MoveBooking = WebCommon.getGlobalResourceValue("BK_ALT_MoveBooking");
    protected string BK_ALT_Selected_Time = WebCommon.getGlobalResourceValue("BK_ALT_Selected_Time");
    protected string BK_ALT_Selected_Door = WebCommon.getGlobalResourceValue("BK_ALT_Selected_Door");
    protected string BK_ALT_Insufficeint_Space_Ext = WebCommon.getGlobalResourceValue("BK_ALT_Insufficeint_Space_Ext");
    protected string BK_ALT_Insufficeint_Space_Int = WebCommon.getGlobalResourceValue("BK_ALT_Insufficeint_Space_Int");
    protected string AtDoorName = WebCommon.getGlobalResourceValue("AtDoorName");

    List<APPBOK_BookingBE> lstAllFixedDoor = new List<APPBOK_BookingBE>();
    IDictionary<string, VendorDetails> SlotDoorCounter = new Dictionary<string, VendorDetails>();

    int iTimeSlotID, iSiteDoorID, iPallets, iCartons, iLifts, iLines, actSlotLength, iNextSlotLength, iNextSlotLength2, iTimeSlotOrderBYID;
    bool isClickable, isCurrentSlotFixed, isNextSlotCovered, isCurrentSlotCovered;
    string DoorIDs = ",";
    string sTimeSlot = string.Empty;
    string sDoorNo;
    string sVendorCarrierName = string.Empty;
    string NextRequiredSlot = string.Empty;
    string sVendorCarrierNameNextSlot = string.Empty;
    string sCurrentFixedSlotID = "-1";
    string sSlotStartDay = string.Empty;


    public int iSlotTimeLength = 5;
    public int intSiteId = 6;
    public string strSiteName = "ASH - Ashton";
    public int intSiteCountryId = 1;

    #endregion

    #region Page Level Properties ...
    public string ucSeacrhVendorNo
    {
        get
        {
            return ViewState["vw_ucSeacrhVendorNo"] != null ? ViewState["vw_ucSeacrhVendorNo"].ToString() : string.Empty;
        }
        set
        {
            ViewState["vw_ucSeacrhVendorNo"] = value;
        }
    }

    public string ucSeacrhVendorName
    {
        get
        {
            return ViewState["vw_ucSeacrhVendorName"] != null ? ViewState["vw_ucSeacrhVendorName"].ToString() : string.Empty;
        }
        set
        {
            ViewState["vw_ucSeacrhVendorName"] = value;
        }
    }

    public string ucSeacrhVendorID
    {
        get
        {
            return ViewState["ucSeacrhVendorID"] != null ? ViewState["ucSeacrhVendorID"].ToString() : string.Empty;
        }
        set
        {
            ViewState["ucSeacrhVendorID"] = value;
        }
    }

    public string ucSeacrhVendorParentID
    {
        get
        {
            return ViewState["ucSeacrhVendorParentID"] != null ? ViewState["ucSeacrhVendorParentID"].ToString() : string.Empty;
        }
        set
        {
            ViewState["ucSeacrhVendorParentID"] = value;
        }
    }

    public bool? IsBookingValidationExcluded
    {
        get
        {
            return (Convert.ToBoolean(IsBookingValidationExcludedVendor) || Convert.ToBoolean(IsBookingValidationExcludedDelivery));
        }
        set
        {
            ViewState["IsBookingValidationExcluded"] = value;
        }
    }

    public bool? IsBookingValidationExcludedVendor
    {
        get
        {
            return ViewState["IsBookingValidationExcludedVendor"] != null ? Convert.ToBoolean(ViewState["IsBookingValidationExcludedVendor"]) : false;
        }
        set
        {
            ViewState["IsBookingValidationExcludedVendor"] = value;
        }
    }

    public bool? IsBookingValidationExcludedDelivery
    {
        get
        {
            return ViewState["IsBookingValidationExcludedDelivery"] != null ? Convert.ToBoolean(ViewState["IsBookingValidationExcludedDelivery"]) : false;
        }
        set
        {
            ViewState["IsBookingValidationExcludedDelivery"] = value;
        }
    }

    public bool? IsBookingValidationExcludedCarrier
    {
        get
        {
            return ViewState["IsBookingValidationExcludedCarrier"] != null ? Convert.ToBoolean(ViewState["IsBookingValidationExcludedCarrier"]) : false;
        }
        set
        {
            ViewState["IsBookingValidationExcludedCarrier"] = value;
        }
    }

    public bool? IsPreAdviseNoteRequired
    {
        get
        {
            return ViewState["IsPreAdviseNoteRequired"] != null ? Convert.ToBoolean(ViewState["IsPreAdviseNoteRequired"]) : false;
        }
        set
        {
            ViewState["IsPreAdviseNoteRequired"] = value;
        }
    }

    public bool? IsNonTime
    {
        get
        {
            return ViewState["IsNonTime"] != null ? Convert.ToBoolean(ViewState["IsNonTime"]) : false;
        }
        set
        {
            ViewState["IsNonTime"] = value;
        }
    }

    public bool? IsNonTimeBooking
    {
        get
        {
            return ViewState["IsNonTimeBooking"] != null ? Convert.ToBoolean(ViewState["IsNonTimeBooking"]) : false;
        }
        set
        {
            ViewState["IsNonTimeBooking"] = value;
        }
    }

    public bool? IsNonTimeVendor
    {
        get
        {
            return ViewState["IsNonTimeVendor"] != null ? Convert.ToBoolean(ViewState["IsNonTimeVendor"]) : false;
        }
        set
        {
            ViewState["IsNonTimeVendor"] = value;
        }
    }

    public bool? isPostBack
    {
        get
        {
            return ViewState["isPostBack"] != null ? Convert.ToBoolean(ViewState["isPostBack"]) : false;
        }
        set
        {
            ViewState["isPostBack"] = value;
        }
    }

    public string strDeliveryValidationExc
    {
        get
        {
            return ViewState["strDeliveryValidationExc"] != null ? ViewState["strDeliveryValidationExc"].ToString() : string.Empty;
        }
        set
        {
            ViewState["strDeliveryValidationExc"] = value;
        }
    }

    public string strDeliveryNonTime
    {
        get
        {
            return ViewState["strDeliveryNonTime"] != null ? ViewState["strDeliveryNonTime"].ToString() : string.Empty;
        }
        set
        {
            ViewState["strDeliveryNonTime"] = value;
        }
    }

    public string strCarrierValidationExc
    {
        get
        {
            return ViewState["strCarrierValidationExc"] != null ? ViewState["strCarrierValidationExc"].ToString() : string.Empty;
        }
        set
        {
            ViewState["strCarrierValidationExc"] = value;
        }
    }

    public string CarrierNarrativeRequired
    {
        get
        {
            return ViewState["CarrierNarrativeRequired"] != null ? ViewState["CarrierNarrativeRequired"].ToString() : string.Empty;
        }
        set
        {
            ViewState["CarrierNarrativeRequired"] = value;
        }
    }

    public string strVehicleDoorTypeMatrix
    {
        get
        {
            return ViewState["strVehicleDoorTypeMatrix"] != null ? ViewState["strVehicleDoorTypeMatrix"].ToString() : string.Empty;
        }
        set
        {
            ViewState["strVehicleDoorTypeMatrix"] = value;
        }
    }

    public string strVehicleDoorTypeMatrixSKU
    {
        get
        {
            return ViewState["strVehicleDoorTypeMatrixSKU"] != null ? ViewState["strVehicleDoorTypeMatrixSKU"].ToString() : string.Empty;
        }
        set
        {
            ViewState["strVehicleDoorTypeMatrixSKU"] = value;
        }
    }

    public string VehicleNarrativeRequired
    {
        get
        {
            return ViewState["VehicleNarrativeRequired"] != null ? ViewState["VehicleNarrativeRequired"].ToString() : string.Empty;
        }
        set
        {
            ViewState["VehicleNarrativeRequired"] = value;
        }
    }

    public int? PreSiteCountryID
    {
        get
        {
            return ViewState["PreSiteCountryID"] != null ? Convert.ToInt32(ViewState["PreSiteCountryID"].ToString()) : (int?)null;
        }
        set
        {
            ViewState["PreSiteCountryID"] = value;
        }
    }

    public NameValueCollection MsgSupressed
    {
        get
        {
            return ViewState["MsgSupressed"] != null ? (NameValueCollection)ViewState["MsgSupressed"] : null;
        }
        set
        {
            ViewState["MsgSupressed"] = value;
        }
    }

    public NameValueCollection ArrDoorTypePriority
    {
        get
        {
            return ViewState["ArrDoorTypePriority"] != null ? (NameValueCollection)ViewState["ArrDoorTypePriority"] : null;
        }
        set
        {
            ViewState["ArrDoorTypePriority"] = value;
        }
    }

    public NameValueCollection ArrDoorTypePrioritySKU
    {
        get
        {
            return ViewState["ArrDoorTypePrioritySKU"] != null ? (NameValueCollection)ViewState["ArrDoorTypePrioritySKU"] : null;
        }
        set
        {
            ViewState["ArrDoorTypePrioritySKU"] = value;
        }
    }

    public string Lines
    {
        get
        {
            return ViewState["Lines"] != null ? ViewState["Lines"].ToString() : "XX";
        }
        set
        {
            ViewState["Lines"] = value;
        }
    }

    public int? NonTimeDeliveryCartonVolume
    {
        get
        {
            return ViewState["NonTimeDeliveryCartonVolume"] != null ? Convert.ToInt32(ViewState["NonTimeDeliveryCartonVolume"].ToString()) : (int?)null;
        }
        set
        {
            ViewState["NonTimeDeliveryCartonVolume"] = value;
        }
    }

    public int? NonTimeDeliveryPalletVolume
    {
        get
        {
            return ViewState["NonTimeDeliveryPalletVolume"] != null ? Convert.ToInt32(ViewState["NonTimeDeliveryPalletVolume"].ToString()) : (int?)null;
        }
        set
        {
            ViewState["NonTimeDeliveryPalletVolume"] = value;
        }
    }

    public string BeforeSlotTimeID
    {
        get
        {
            return ViewState["BeforeSlotTimeID"] != null ? ViewState["BeforeSlotTimeID"].ToString() : "0";
        }
        set
        {
            ViewState["BeforeSlotTimeID"] = value;
        }
    }

    public string AfterSlotTimeID
    {
        get
        {
            return ViewState["AfterSlotTimeID"] != null ? ViewState["AfterSlotTimeID"].ToString() : "300";
        }
        set
        {
            ViewState["AfterSlotTimeID"] = value;
        }
    }

    public int BookedPallets
    {
        get
        {
            return ViewState["BookedPallets"] != null ? Convert.ToInt32(ViewState["BookedPallets"]) : 0;
        }
        set
        {
            ViewState["BookedPallets"] = value;
        }
    }

    public int BookedLines
    {
        get
        {
            return ViewState["BookedLines"] != null ? Convert.ToInt32(ViewState["BookedLines"]) : 0;
        }
        set
        {
            ViewState["BookedLines"] = value;
        }
    }

    public DateTime LogicalSchedulDateTime
    {
        get
        {
            return ViewState["LogicalSchedulDateTime"] != null ? Convert.ToDateTime(ViewState["LogicalSchedulDateTime"].ToString()) : new DateTime(1900, 01, 01, 0, 0, 00);
        }
        set
        {
            ViewState["LogicalSchedulDateTime"] = value;
        }
    }

    public DateTime ActualSchedulDateTime
    {
        get
        {
            return ViewState["ActualSchedulDateTime"] != null ? Convert.ToDateTime(ViewState["ActualSchedulDateTime"].ToString()) : new DateTime(1900, 01, 01, 0, 0, 00);
        }
        set
        {
            ViewState["ActualSchedulDateTime"] = value;
        }
    }

    public bool isProvisional
    {
        get
        {
            return ViewState["isProvisional"] != null ? Convert.ToBoolean(ViewState["isProvisional"]) : false;
        }
        set
        {
            ViewState["isProvisional"] = value;
        }
    }

    //Sprint 1 - Point 7 - Start
    public string AlreadyExistsPOs
    {
        get
        {
            return ViewState["AlreadyExistsPOs"] != null ? Convert.ToString(ViewState["AlreadyExistsPOs"]) : string.Empty;
        }
        set
        {
            ViewState["AlreadyExistsPOs"] = value;
        }
    }
    //Sprint 1 - Point 7 - End

    public string SortDirection
    {
        get
        {
            return ViewState["SortDirection"] != null ? Convert.ToString(ViewState["SortDirection"]) : string.Empty;
        }
        set
        {
            ViewState["SortDirection"] = value;
        }
    }

    public string sAddedPurchaseOrders
    {
        get
        {
            return ViewState["vw_sAddedPurchaseOrders"] != null ? ViewState["vw_sAddedPurchaseOrders"].ToString() : string.Empty;
        }
        set
        {
            ViewState["vw_sAddedPurchaseOrders"] = value;
        }
    }

    #endregion

    #region Page Level Enums ...
    public enum BookingType
    {
        FixedSlot = 1, Unscheduled = 2, NonTimedDelivery = 3, BookedSlot = 4, Provisional = 5
    };
    #endregion

    #region Page Level Events ...

    protected void Page_InIt(object sender, EventArgs e)
    {
        //ddlSite.CurrentPage = this;
        //ddlSite.SchedulingContact = true;

        //ucSeacrhVendor1.CurrentPage = this;
        //txtSchedulingdate.CurrentPage = this;
        //ucSeacrhVendor1.IsParentRequired = true;
        //ucSeacrhVendor1.IsStandAloneRequired = true;
        //ucSeacrhVendor1.IsChildRequired = true;
        //ucSeacrhVendor1.IsGrandParentRequired = true;
    }

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            this.BindCarrier();
            this.BindVehicleType();
            this.BindDelivaryType();
        }
    }

    protected void btnVendor_Click(object sender, EventArgs e)
    {
        ActivateFistView();
    }

    protected void btnCarrier_Click(object sender, EventArgs e)
    {
        //EncryptQueryString("APPBok_CarrierBookingEdit.aspx?Mode=" + GetQueryStringValue("Mode").ToString() + "&Scheduledate=" + ActualSchedulDateTime.ToString("dd/MM/yyyy") + "&Logicaldate=" + LogicalSchedulDateTime.ToString("dd/MM/yyyy") + "&SiteID=" + GetQueryStringValue("SiteID").ToString() + "&SiteCountryID=" + GetQueryStringValue("SiteCountryID").ToString());
        EncryptQueryString("APPBok_CarrierBookingByPOEdit.aspx");
    }

    protected void ddlCarrier_SelectedIndexChanged(object sender, EventArgs e)
    {
        //txtCarrierOther.Text = string.Empty;

        //if (ddlCarrier.Items.Count > 0)
        //{
        //    if (CarrierNarrativeRequired.Contains("," + ddlCarrier.SelectedItem.Value + ","))
        //    {
        //        txtCarrierOther.Style.Add("display", "");
        //        lblCarrierAdvice.Style.Add("display", "");
        //        rfvCarrierOtherRequired.Enabled = true;
        //    }
        //    else
        //    {
        //        txtCarrierOther.Style.Add("display", "none");
        //        lblCarrierAdvice.Style.Add("display", "none");
        //        rfvCarrierOtherRequired.Enabled = false;
        //    }
        //}
        //else
        //{
        //    txtCarrierOther.Style.Add("display", "none");
        //    lblCarrierAdvice.Style.Add("display", "none");
        //    rfvCarrierOtherRequired.Enabled = false;
        //}

        ////if (strCarrierValidationExc.Contains(ddlCarrier.SelectedItem.Value + ",")) {
        ////    IsBookingValidationExcludedCarrier = true;
        ////}
    }

    protected void ddlVehicleType_SelectedIndexChanged(object sender, EventArgs e)
    {
        //txtVehicleOther.Text = string.Empty;
        //if (VehicleNarrativeRequired.Contains(ddlVehicleType.SelectedItem.Value + ","))
        //{
        //    txtVehicleOther.Style.Add("display", "");
        //    lblVehicleAdvice.Style.Add("display", "");
        //    rfvVehicleTypeOtherRequired.Enabled = true;
        //}
        //else
        //{
        //    txtVehicleOther.Style.Add("display", "none");
        //    lblVehicleAdvice.Style.Add("display", "none");
        //    rfvVehicleTypeOtherRequired.Enabled = false;
        //}

        ////Get Door Type 
        //strVehicleDoorTypeMatrix = string.Empty;
        //MASSIT_VehicleDoorMatrixBE oMASSIT_VehicleDoorMatrixBE = new MASSIT_VehicleDoorMatrixBE();

        //APPSIT_VehicleDoorMatrixBAL oMASSIT_VehicleDoorMatrixBAL = new APPSIT_VehicleDoorMatrixBAL();

        //oMASSIT_VehicleDoorMatrixBE.Action = "ShowAll";
        //oMASSIT_VehicleDoorMatrixBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
        //oMASSIT_VehicleDoorMatrixBE.User.UserID = Convert.ToInt32(Session["UserID"]);
        //oMASSIT_VehicleDoorMatrixBE.SiteID = Convert.ToInt16(ddlSite.innerControlddlSite.SelectedItem.Value);

        //List<MASSIT_VehicleDoorMatrixBE> lstVehicleDoorMatrix = oMASSIT_VehicleDoorMatrixBAL.GetVehicleDoorMatrixDetailsBAL(oMASSIT_VehicleDoorMatrixBE);

        ////---FOR SKU DOOR MATRIX, NEED TO SKIP VEHICLE DOOR MATIX CONSTRAINTS--- SPRINT 4 POINT 7
        //NameValueCollection collection = new NameValueCollection();

        //for (int iCount = 0; iCount < lstVehicleDoorMatrix.Count; iCount++)
        //{
        //    strVehicleDoorTypeMatrixSKU += lstVehicleDoorMatrix[iCount].DoorTypeID.ToString() + ",";
        //    collection.Add(lstVehicleDoorMatrix[iCount].DoorTypeID.ToString(), lstVehicleDoorMatrix[iCount].Priority.ToString());
        //}

        //ArrDoorTypePrioritySKU = new NameValueCollection();
        //foreach (string key in collection.AllKeys)
        //{
        //    string Val = collection.Get(key);
        //    string[] ArrVal = Val.Split(',');
        //    ArrDoorTypePrioritySKU.Add(key, ArrVal[0].ToString());
        //}
        ////---------------------------------------------------------------------------------------

        ////Get Door Type as per selected Vehicle Type
        //lstVehicleDoorMatrix = lstVehicleDoorMatrix.Where(Matrix => Matrix.VehicleTypeID == Convert.ToInt32(ddlVehicleType.SelectedItem.Value)).ToList();

        //collection = new NameValueCollection();

        //for (int iCount = 0; iCount < lstVehicleDoorMatrix.Count; iCount++)
        //{
        //    strVehicleDoorTypeMatrix += lstVehicleDoorMatrix[iCount].DoorTypeID.ToString() + ",";
        //    collection.Add(lstVehicleDoorMatrix[iCount].DoorTypeID.ToString(), lstVehicleDoorMatrix[iCount].Priority.ToString());
        //}

        //ArrDoorTypePriority = new NameValueCollection();
        //foreach (string key in collection.AllKeys)
        //{
        //    string Val = collection.Get(key);
        //    string[] ArrVal = Val.Split(',');
        //    ArrDoorTypePriority.Add(key, ArrVal[0].ToString());
        //}


        //-----------------//           
    }

    protected void ddlDeliveryType_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (strDeliveryValidationExc.Contains(ddlDeliveryType.SelectedItem.Value + ","))
        {
            IsBookingValidationExcludedDelivery = true;
        }
        else
        {
            IsBookingValidationExcludedDelivery = false;
        }
    }

    protected void btnAddPO_Click(object sender, EventArgs e)
    {
        string errMsg = string.Empty;

        if (txtPurchaseOrderT.Text != string.Empty)
        {
            string messageSeparator = ", ";
            DataTable myDataTable = null;

            if (ViewState["myDataTable"] != null)
            {
                bool Multiple = false;
                myDataTable = (DataTable)ViewState["myDataTable"];

                //Check Item alrady added
                DataRow[] drr = myDataTable.Select("PurchaseNumber='" + txtPurchaseOrderT.Text.Trim() + "'");

                if (drr.Length > 0)
                {
                    // // Sprint 1 - Point 7 - Shown error message when adding PO using Add PO button 
                    // otherwise saved PO number in view state.
                    if (!Multiple)
                    {
                        errMsg = "The Purchase Order Number you have supplied already exists.";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "popup", "<script>alert('" + errMsg + "');</script>", false);
                    }
                    else
                    {
                        AlreadyExistsPOs += txtPurchaseOrderT.Text.Trim() + messageSeparator;
                    }
                    return;
                }
            }
            IsPOValid();
        }
    }

    protected void btnProceedBooking_Click(object sender, EventArgs e)
    {
        //ScriptManager.RegisterStartupScript(this, this.GetType(), "popup", "<script>alert('" + errMsg + "');</script>", false);
    }

    protected void btnContinue_Click(object sender, CommandEventArgs e)
    {

        NameValueCollection ErrorMessages = new NameValueCollection();
        if (ViewState["ErrorMessages"] != null)
        {
            ErrorMessages = (NameValueCollection)ViewState["ErrorMessages"];
        }

        if (ErrorMessages.Keys.Count > 0)
        {
            ShowWarningMessages();
            return;
        }

        if (ErrorMessages.Keys.Count == 0)
        {
            if (e.CommandName == "BK_MS_05_INT" || e.CommandName == "BK_MS_04_INT" || e.CommandName == "BK_MS_04_T_INT" || e.CommandName == "BK_MS_06_INT" || e.CommandName == "BK_MS_27_INT")
            {
                //ScriptManager.RegisterStartupScript(Page, this.GetType(), "btnshow", "$get('" + btnProceedAddPO.ClientID + "').style.display='';", true);
                ScriptManager.RegisterStartupScript(Page, this.GetType(), "btnshow", "$get('" + btnProceedBooking.ClientID + "').style.display='';", true);
            }
            else if (e.CommandName == "BK_MS_11_INT" || e.CommandName == "BK_MS_12_INT" || e.CommandName == "BK_MS_13_INT" || e.CommandName == "BK_MS_14_INT")
            {
                BindPOGrid(txtPurchaseOrderT.Text);
                txtPurchaseOrderT.Text = string.Empty;
                ScriptManager.RegisterStartupScript(Page, this.GetType(), "btnshow", "$get('" + btnProceedBooking.ClientID + "').style.display='';", true);
            }
            else if (e.CommandName == "BK_MS_22_INT")
            {
                PaintAlternateSlotScreen();
            }
            else if (e.CommandName == "BK_MS_22_EXT")
            {
                GenerateAlternateSlotForVendor();
                PaintAlternateScreenForVendor();
                mvBookingEdit.ActiveViewIndex = 5;
            }
            else if (e.CommandName == "BK_MS_24_INT")
            {
                MakeBooking(BookingType.NonTimedDelivery.ToString());
            }
            else if (e.CommandName == "BK_MS_26_EXT" || e.CommandName == "BK_MS_26_Err" || e.CommandName == "BK_MS_18_INT")
            {
                GetOutstandingPO();
                //CalculateProposedBookingTimeSlot();
                //mvBookingEdit.ActiveViewIndex = 3;
            }
            else if (e.CommandName == "BK_MS_26_Err")
            {
                //txtExpectedLines.Text = string.Empty;
            }
            else if (e.CommandName == "BK_MS_23_EXT")
            {
                hdnCapacityWarning.Value = "false";
                MakeBooking(BookingType.Provisional.ToString());   // For Vendor
            }
            else if (e.CommandName == "BK_MS_23_INT")
            {
                hdnCapacityWarning.Value = "false";
                MakeBooking(BookingType.FixedSlot.ToString());// For OD Staff               
            }
        }

        //MsgSupressed.Add(e.CommandName, "true");

    }

    protected void btnConfirmBooking_Click(object sender, EventArgs e)
    {
        ConfirmFixedSlotBooking();
    }

    protected void gvBookingSlots_RowDataBound(object sender, GridViewRowEventArgs e)
    {

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            if (e.Row.Cells[6].Text == "Booked")
            {
                e.Row.BackColor = Color.Purple;
                e.Row.ForeColor = Color.White;
            }
        }
    }

    protected void hdnbutton_Click(object sender, EventArgs e)
    {
        SlotDoorCounter.Clear();
        SlotDoorCounter = new Dictionary<string, VendorDetails>();
        isProvisional = false;
        if (hdnCurrentRole.Value != "Vendor")
        {
            ltTimeSlot.Text = hdnSuggestedSlotTime.Value;
            ltDoorNo.Text = hdnSuggestedSiteDoorNumber.Value;
        }
        if (hdnCurrentRole.Value == "Vendor")
        {
            ltTimeSlot_1.Text = hdnSuggestedSlotTime.Value;
            ltDoorNo_1.Text = hdnSuggestedSiteDoorNumber.Value;
        }
        if (mvBookingEdit.ActiveViewIndex == 4) // For OD Staff
        {

            tableDiv_General.Controls.RemoveAt(0);
            GenerateAlternateSlot();
        }
        else if (mvBookingEdit.ActiveViewIndex == 5) // For Vendor
        {
            tableDiv_GeneralVendor.Controls.RemoveAt(0);
            GenerateAlternateSlotForVendor();
        }
    }

    protected void btnConfirmAlternateSlotVendor_Click(object sender, EventArgs e){ }
    protected void btnConfirmAlternateSlot_Click(object sender, EventArgs e) { }
    protected void btnBack_Click(object sender, EventArgs e) { }
    protected void btnErrorMsgOK_Click(object sender, EventArgs e) { }
    protected void btnOutstandingPOContinue_Click(object sender, EventArgs e) { }
    protected void btnOutstandingPOBack_Click(object sender, EventArgs e) { }
    protected void btnPOContinue_Click(object sender, EventArgs e) { }
    protected void btnPOBack_Click(object sender, EventArgs e) { }
    protected void btnAddPOToBooking_Click(object sender, EventArgs e) { }
    protected void grdShowPO_Sorting(object sender, GridViewSortEventArgs e) { }
    protected void grdShowPO_RowDataBound(object sender, GridViewRowEventArgs e) { }

    #endregion

    #region Page Level Methods ...

    private void ActivateFistView()
    {
        mvBookingEdit.ActiveViewIndex = 1;
        
        //if (hdnActualScheduleDate.Value != string.Empty)
        //{
        //    txtSchedulingdate.innerControltxtDate.Value = ActualSchedulDateTime.ToString("dd/MM/yyyy");
        //}

        //if (hdnCurrentMode.Value == "Add" && hdnFixedSlotMode.Value == "false" && hdnCurrentRole.Value == "Vendor")
        //{
        //    txtSchedulingdate.innerControltxtDate.Value = "";
        //}

        //if (hdnCurrentMode.Value == "Edit" && hdnCurrentRole.Value == "Vendor")
        //{
        //    spSCDate.Style.Add("display", "none");
        //    ltSCdate.Visible = true;
        //    ltSCdate.Text = ActualSchedulDateTime.ToString("dd/MM/yyyy");
        //}

        //if (GetQueryStringValue("SiteCountryID") != null)
        //{
        //    //
        //    ddlSite.innerControlddlSite.SelectedIndex = ddlSite.innerControlddlSite.Items.IndexOf(ddlSite.innerControlddlSite.Items.FindByValue(GetQueryStringValue("SiteID").ToString()));
        //    ddlSite.CountryID = Convert.ToInt32(GetQueryStringValue("SiteCountryID").ToString());
        //    ucSeacrhVendor1.SiteID = Convert.ToInt32(GetQueryStringValue("SiteID").ToString());
        //}
        //else
        //{
        //    ddlSite.innerControlddlSite.SelectedIndex = ddlSite.innerControlddlSite.Items.IndexOf(ddlSite.innerControlddlSite.Items.FindByValue(Session["SiteID"].ToString()));
        //    ddlSite.CountryID = Convert.ToInt32(Session["SiteCountryID"].ToString());
        //    ucSeacrhVendor1.SiteID = Convert.ToInt32(Session["SiteID"].ToString());
        //}

        //SiteSelectedIndexChanged();

        //if (hdnCurrentMode.Value == "Edit")
        //{
        //    spSite.Style.Add("display", "none");
        //    ltSelectedSite.Visible = true;
        //    ltSelectedSite.Text = ddlSite.innerControlddlSite.SelectedItem.Text;
        //}
    }

    private void BindVehicleType()
    {
        MASSIT_VehicleTypeBE oMASSIT_VehicleTypeBE = new MASSIT_VehicleTypeBE();
        APPSIT_VehicleTypeBAL oMASSIT_VehicleTypeBAL = new APPSIT_VehicleTypeBAL();

        oMASSIT_VehicleTypeBE.Action = "ShowAll";
        oMASSIT_VehicleTypeBE.SiteID = Convert.ToInt32(intSiteId);//ddlSite.innerControlddlSite.SelectedItem.Value);

        List<MASSIT_VehicleTypeBE> lstVehicleType = oMASSIT_VehicleTypeBAL.GetVehicleTypeDetailsBAL(oMASSIT_VehicleTypeBE);

        if (lstVehicleType.Count > 0)
        {
            FillControls.FillDropDown(ref ddlVehicleType, lstVehicleType, "VehicleType", "VehicleTypeID", "--Select--");
        }

        VehicleNarrativeRequired = string.Empty;

        for (int iCount = 0; iCount < lstVehicleType.Count; iCount++)
        {
            if (lstVehicleType[iCount].IsNarrativeRequired)
            {
                VehicleNarrativeRequired += lstVehicleType[iCount].VehicleTypeID.ToString() + ",";
            }
        }

        //if (Convert.ToString(Session["Role"]).Trim().ToLower() == "carrier") 
        //{
        MAS_SiteBE singleSiteSetting = GetSingleSiteSetting(Convert.ToInt32(intSiteId));//ddlSite.innerControlddlSite.SelectedValue));
        if (singleSiteSetting != null)
            ddlVehicleType.SelectedIndex = ddlVehicleType.Items.IndexOf(ddlVehicleType.Items.FindByValue(Convert.ToString(singleSiteSetting.VehicleTypeId)));
        //}

    }

    private void BindDelivaryType()
    {
        MASCNT_DeliveryTypeBE oMASCNT_DeliveryTypeBE = new MASCNT_DeliveryTypeBE();
        APPCNT_DeliveryTypeBAL oMASCNT_DeliveryTypeBAL = new APPCNT_DeliveryTypeBAL();

        oMASCNT_DeliveryTypeBE.Action = "ShowAll";
        oMASCNT_DeliveryTypeBE.CountryID = intSiteCountryId; //ddlSite.CountryID;
        oMASCNT_DeliveryTypeBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
        oMASCNT_DeliveryTypeBE.User.UserID = 0;
        List<MASCNT_DeliveryTypeBE> lstDeliveryType = oMASCNT_DeliveryTypeBAL.GetDeliveryTypeDetailsBAL(oMASCNT_DeliveryTypeBE);

        if (hdnCurrentRole.Value == "Vendor")
        {
            lstDeliveryType = lstDeliveryType.FindAll(delegate(MASCNT_DeliveryTypeBE p) { return p.IsEnabledAsVendorOption == true; });
        }

        if (lstDeliveryType.Count > 0)
        {
            FillControls.FillDropDown(ref ddlDeliveryType, lstDeliveryType, "DeliveryType", "DeliveryTypeID", "--Select--");
        }
        else
        {
            ddlDeliveryType.Items.Clear();
            ddlDeliveryType.Items.Add(new ListItem("---Select---", "0"));
        }

        //if (Convert.ToString(Session["Role"]).Trim().ToLower() == "carrier")
        //{
        MAS_SiteBE singleSiteSetting = GetSingleSiteSetting(Convert.ToInt32(intSiteId));//ddlSite.innerControlddlSite.SelectedItem.Value);
        if (singleSiteSetting != null)
            ddlDeliveryType.SelectedIndex = ddlDeliveryType.Items.IndexOf(ddlDeliveryType.Items.FindByValue(Convert.ToString(singleSiteSetting.DeliveryTypeId)));
        //}
    }

    private void BindCarrier()
    {
        MASCNT_CarrierBE oMASCNT_CarrierBE = new MASCNT_CarrierBE();
        APPCNT_CarrierBAL oMASCNT_CarrierBAL = new APPCNT_CarrierBAL();

        oMASCNT_CarrierBE.Action = "ShowAll";
        oMASCNT_CarrierBE.CountryID = intSiteCountryId;//ddlSite.CountryID;

        List<MASCNT_CarrierBE> lstCarrier = oMASCNT_CarrierBAL.GetCarrierDetailsBAL(oMASCNT_CarrierBE);

        if (lstCarrier.Count > 0)
        {
            FillControls.FillDropDown(ref ddlCarrier, lstCarrier, "CarrierName", "CarrierID", "--Select--");
        }

        CarrierNarrativeRequired = ",";
        for (int iCount = 0; iCount < lstCarrier.Count; iCount++)
        {
            if (lstCarrier[iCount].IsNarrativeRequired)
            {
                CarrierNarrativeRequired += lstCarrier[iCount].CarrierID.ToString() + ",";
            }
        }
    }

    private MAS_SiteBE GetSingleSiteSetting(int siteId)
    {
        MAS_SiteBAL oMAS_SiteBAL = new MAS_SiteBAL();
        MAS_SiteBE oMAS_SiteBE = new MAS_SiteBE();
        oMAS_SiteBE.Action = "GetSingleSiteSetting";
        oMAS_SiteBE.SiteID = siteId;
        MAS_SiteBE localMAS_SiteBE = oMAS_SiteBAL.GetSingleSiteSettingBAL(oMAS_SiteBE);
        return localMAS_SiteBE;
    }

    private void IsPOValid()
    {
        string errMsg = string.Empty;
        bool isWarning = false;

        if (IsBookingValidationExcluded == false)
        {

            Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE = new Up_PurchaseOrderDetailBE();
            UP_PurchaseOrderDetailBAL oUP_PurchaseOrderDetailBAL = new UP_PurchaseOrderDetailBAL();

            oUp_PurchaseOrderDetailBE.Action = "GetAllPODetails";
            oUp_PurchaseOrderDetailBE.Purchase_order = txtPurchaseOrderT.Text.Trim();
            oUp_PurchaseOrderDetailBE.CountryID = Convert.ToInt32(PreSiteCountryID);

            List<Up_PurchaseOrderDetailBE> lstPO = oUP_PurchaseOrderDetailBAL.GetAllPODetailsBAL(oUp_PurchaseOrderDetailBE);
            if (lstPO.Count <= 0)
            {
                errMsg = WebCommon.getGlobalResourceValue("BK_MS_11_EXT"); // "The Purchase Order Number you have supplied is not valid, please enter correct Purchase Order Number.";
                ShowErrorMessage(errMsg, "BK_MS_11_EXT");
                return;

                //if (hdnCurrentRole.Value == "Vendor") {
                //    errMsg = WebCommon.getGlobalResourceValue("BK_MS_11_EXT");
                //    ShowErrorMessage(errMsg, "BK_MS_11_EXT");
                //    return;
                //}
                //else {
                //    errMsg = "The Purchase Order Number you have supplied is not valid. Do you want to continue?";
                //    AddWarningMessages(errMsg, "BK_MS_11_INT");
                //    isWarning = true;
                //}
            }

            if (lstPO.Count > 0)
            {
                if (lstPO[0].LastUploadedFlag == "N")
                {
                    errMsg = WebCommon.getGlobalResourceValue("BK_MS_11_EXT"); //"The Purchase Order Number you have supplied is not valid, please enter correct Purchase Order Number.";
                    ShowErrorMessage(errMsg, "BK_MS_11_EXT");
                    return;
                }
            }
        }

        if (isWarning == true)
        {
            ShowWarningMessages();
        }
        else
        {
            // Sprint 1 - Point 1 - Start - Checking PO is existing or not.
            APPBOK_BookingBE appbok_BookingBE = new APPBOK_BookingBE();
            appbok_BookingBE.Action = "IsExistingPO";
            appbok_BookingBE.PurchaseOrders = txtPurchaseOrderT.Text;
            appbok_BookingBE.VendorID = Convert.ToInt32(ucSeacrhVendorID);
            appbok_BookingBE.SiteId = Convert.ToInt32(intSiteId); //ddlSite.innerControlddlSite.SelectedItem.Value);
            APPBOK_BookingBAL appbok_BookingBAL = new APPBOK_BookingBAL();
            List<APPBOK_BookingBE> lstAPPBOK_BookingBE = appbok_BookingBAL.IsExistingPO(appbok_BookingBE);

            // Sprint 1 - Point 1 - End

            if (lstAPPBOK_BookingBE.Count == 0)
            {
                // Sprint 1 - Point 7 - Passed purchase order number as a parameter and then set tp blank
                this.AddPO(txtPurchaseOrderT.Text, false, true);
                //txtPurchaseNumber.Text = string.Empty;
            }
            else
            {
                foreach (APPBOK_BookingBE oAPPBOK_BookingBE in lstAPPBOK_BookingBE)
                {
                    #region Double booking warning message formatting.
                    errMsg = string.Format("<table><tr><td style=\"text-align:center\">{0}</td></tr>",
                        WebCommon.getGlobalResourceValue("BK_MS_29_Warning_1"));

                    errMsg = string.Format("{0}<tr><td style=\"text-align: center\">{1}</td></tr>",
                        errMsg, WebCommon.getGlobalResourceValue("BK_MS_29_Warning_2").Replace("##dd/mm/yyyy##", "<b>" + Convert.ToDateTime(oAPPBOK_BookingBE.ScheduleDate).ToString("dd/MM/yyyy") + "</b>"));

                    errMsg = string.Format("{0}<tr><td style=\"text-align: center\">{1}</td></tr>",
                        errMsg, WebCommon.getGlobalResourceValue("BK_MS_29_Warning_3").Replace("##tt:mm##", String.Format("{0}", "<b>" + oAPPBOK_BookingBE.SlotTime.SlotTime) + "</b>"));

                    errMsg = string.Format("{0}<tr><td style=\"text-align: center\">{1}</td></tr></table>",
                        errMsg, WebCommon.getGlobalResourceValue("BK_MS_29_Warning_4"));
                    #endregion
                    break; // Here being break because need only zero index value.
                }

                ltPOConfirmMsg.Text = errMsg;
                mdlPOConfirmMsg.Show();
            }
        }
    }

    private void ShowErrorMessage(string ErrMessage, string CommandName)
    {
        ltErrorMsg.Text = ErrMessage;
        btnErrorMsgOK.CommandName = CommandName;
        mdlErrorMsg.Show();
    }

    private void ShowWarningMessages()
    {
        NameValueCollection ErrorMessages = new NameValueCollection();
        if (ViewState["ErrorMessages"] != null)
        {
            ErrorMessages = (NameValueCollection)ViewState["ErrorMessages"];
        }
        if (ErrorMessages.Keys.Count > 0)
        {
            string errMsg = ErrorMessages.Get(0);
            string CommandName = ErrorMessages.GetKey(0);
            if (CommandName == "BK_MS_26_EXT")
            {
                errMsg = @"<span style=""color:Red;font-size:14px;"" >" + errMsg + "</span>";
            }
            ltConfirmMsg.Text = errMsg;
            btnErrContinue.CommandName = CommandName;
            btnErrBack.CommandName = CommandName;
            mdlConfirmMsg.Show();
            ErrorMessages.Remove(CommandName);
            ViewState["ErrorMessages"] = ErrorMessages;
        }
    }

    private void AddWarningMessages(string ErrMessage, string CommandName)
    {
        NameValueCollection ErrorMessages = new NameValueCollection();
        if (ViewState["ErrorMessages"] != null)
        {
            ErrorMessages = (NameValueCollection)ViewState["ErrorMessages"];
        }
        if (ErrorMessages.Get(CommandName) == null)
        {
            ErrorMessages.Add(CommandName, ErrMessage);
            ViewState["ErrorMessages"] = ErrorMessages;
        }
    }

    private void AddPO(string PurchaseNumber, bool Multiple = false, bool isValidPOChecked = false)
    {
        string errMsg = string.Empty;
        bool isWarning = false;
        string messageSeparator = ", ";

        if (PurchaseNumber != string.Empty)
        {
            DataTable myDataTable = null;
            if (ViewState["myDataTable"] != null)
            {
                myDataTable = (DataTable)ViewState["myDataTable"];

                //Check Item alrady added
                DataRow[] drr = myDataTable.Select("PurchaseNumber='" + PurchaseNumber.Trim() + "'");

                if (drr.Length > 0)
                {
                    // // Sprint 1 - Point 7 - Shown error message when adding PO using Add PO button 
                    // otherwise saved PO number in view state.
                    if (!Multiple)
                    {
                        errMsg = "The Purchase Order Number you have supplied already exists.";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "popup", "<script>alert('" + errMsg + "');</script>", false);
                    }
                    else
                    {
                        AlreadyExistsPOs += PurchaseNumber.Trim() + messageSeparator;
                    }
                    return;
                }
            }

            if (IsBookingValidationExcluded == false)
            {  //If there is no Validation Exclusion for the vendor/carrier or booking type             

                Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE = new Up_PurchaseOrderDetailBE();
                UP_PurchaseOrderDetailBAL oUP_PurchaseOrderDetailBAL = new UP_PurchaseOrderDetailBAL();

                oUp_PurchaseOrderDetailBE.Action = "GetAllPODetails";
                oUp_PurchaseOrderDetailBE.Purchase_order = PurchaseNumber.Trim();
                oUp_PurchaseOrderDetailBE.CountryID = Convert.ToInt32(PreSiteCountryID);

                List<Up_PurchaseOrderDetailBE> lstPO = oUP_PurchaseOrderDetailBAL.GetAllPODetailsBAL(oUp_PurchaseOrderDetailBE);

                if (!isValidPOChecked)
                {
                    if (lstPO.Count <= 0)
                    {
                        errMsg = WebCommon.getGlobalResourceValue("BK_MS_11_EXT"); // "The Purchase Order Number you have supplied is not valid, please enter correct Purchase Order Number.";
                        ShowErrorMessage(errMsg, "BK_MS_11_EXT");
                        return;

                        //if (hdnCurrentRole.Value == "Vendor" || hdnCurrentRole.Value == "Carrier") {
                        //    errMsg = WebCommon.getGlobalResourceValue("BK_MS_11_EXT");
                        //    ShowErrorMessage(errMsg, "BK_MS_11_EXT");
                        //    return;
                        //}
                        //else {
                        //    errMsg = "The Purchase Order Number you have supplied is not valid. Do you want to continue?";
                        //    AddWarningMessages(errMsg, "BK_MS_11_INT");
                        //    isWarning = true;
                        //}
                    }
                }

                if (lstPO.Count > 0)
                {
                    if (!isValidPOChecked)
                    {
                        if (lstPO[0].LastUploadedFlag == "N")
                        {
                            errMsg = WebCommon.getGlobalResourceValue("BK_MS_11_EXT"); //"The Purchase Order Number you have supplied is not valid, please enter correct Purchase Order Number.";
                            ShowErrorMessage(errMsg, "BK_MS_11_EXT");
                            return;
                        }
                    }

                    //if (Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value) != lstPO[0].Site.SiteID)
                    if (Convert.ToInt32(intSiteId) != lstPO[0].Site.SiteID)
                    {
                        //Cross docking check

                        MASCNT_CrossDockBE oMASCNT_CrossDockBE = new MASCNT_CrossDockBE();
                        APPCNT_CrossDockBAL oMASCNT_CrossDockBAL = new APPCNT_CrossDockBAL();

                        oMASCNT_CrossDockBE.Action = "GetDestSiteById";
                        oMASCNT_CrossDockBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
                        oMASCNT_CrossDockBE.SiteID = Convert.ToInt32(intSiteId); //Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);

                        List<MASCNT_CrossDockBE> lstCrossDock = oMASCNT_CrossDockBAL.GetCrossDockDetailsBAL(oMASCNT_CrossDockBE);

                        if (lstCrossDock != null && lstCrossDock.Count > 0)
                        {

                            lstCrossDock = lstCrossDock.FindAll(delegate(MASCNT_CrossDockBE cd)
                            {
                                return cd.DestinationSiteID == lstPO[0].Site.SiteID;
                            });
                        }

                        if (lstCrossDock == null || lstCrossDock.Count == 0)
                        {
                            if (hdnCurrentRole.Value == "Vendor" || hdnCurrentRole.Value == "Carrier")
                            {
                                errMsg = WebCommon.getGlobalResourceValue("BK_MS_14_EXT").Replace("##Ponumber##", PurchaseNumber.Trim()).Replace("##Sitename##", lstPO[0].Site.SiteName); //"The Purchase Order you have quoted is not for the Office Depot site you have selected." +
                                //"Please recheck the entries made and resubmit the correct information";
                                ShowErrorMessage(errMsg, "BK_MS_14_EXT");
                                return;
                            }
                            else
                            {
                                errMsg = WebCommon.getGlobalResourceValue("BK_MS_14_INT"); //"The Purchase Order you have quoted is not for the Office Depot site you have selected. Do you want to continue?";
                                AddWarningMessages(errMsg, "BK_MS_14_INT");
                                isWarning = true;
                            }
                        }

                    }

                    UP_VendorBE oUP_VendorBE = new UP_VendorBE();
                    oUP_VendorBE.Action = "GetConsolidateVendors";
                    oUP_VendorBE.VendorID = Convert.ToInt32(ucSeacrhVendorID);
                    List<UP_VendorBE> lstConsolidateVendors = oUP_PurchaseOrderDetailBAL.GetConsolidateVendorsBAL(oUP_VendorBE);

                    List<int> VenIds = new List<int>();

                    foreach (var p in lstPO)
                        VenIds.Add(p.Vendor.VendorID);

                    if (lstConsolidateVendors.FindAll(x => VenIds.Contains(x.VendorID)).ToList().Count <= 0)
                    {
                        //if (lstConsolidateVendors.FindAll(x => x.VendorID == lstPO[0].Vendor.VendorID).ToList().Count <= 0) {
                        if (hdnCurrentRole.Value == "Vendor" || hdnCurrentRole.Value == "Carrier")
                        {
                            errMsg = WebCommon.getGlobalResourceValue("BK_MS_12_EXT"); //"The Purchase Order Number you have supplied is for a different vendor. Please enter correct Purchase Order Number.";
                            ShowErrorMessage(errMsg, "BK_MS_12_EXT");
                            return;
                        }
                        else
                        {
                            errMsg = WebCommon.getGlobalResourceValue("BK_MS_12_INT"); //"The Purchase Order Number you have entered is not for the entered vendor. Do you want to continue?";
                            AddWarningMessages(errMsg, "BK_MS_12_INT");
                            isWarning = true;
                        }
                    }



                    // Sprint 1 - Point 14 - Begin - Changing from [Original_due_date] to [Expected Date]
                    /*
                       DateTime PODueDate = Utilities.Common.TextToDateFormat(GetFormatedDate(lstPO[0].Original_due_date.Value.Day.ToString() + "/" + lstPO[0].Original_due_date.Value.Month.ToString() + "/" + lstPO[0].Original_due_date.Value.Year.ToString()));
                       //DateTime CurrentDate = Utilities.Common.TextToDateFormat(DateTime.Now.Day.ToString() + "/" + DateTime.Now.Month.ToString() + "/" + DateTime.Now.Year.ToString());
                    */
                    DateTime PODueDate = DateTime.Now;
                    if (lstPO[0].Expected_date != null) // This condition is given, if [Expected Date] will come null then [Original due date] will be set like earlier.
                    {
                        PODueDate = Utilities.Common.TextToDateFormat(GetFormatedDate(lstPO[0].Expected_date.Value.Day.ToString() + "/" + lstPO[0].Expected_date.Value.Month.ToString() + "/" + lstPO[0].Expected_date.Value.Year.ToString()));
                    }
                    else
                    {
                        PODueDate = Utilities.Common.TextToDateFormat(GetFormatedDate(lstPO[0].Original_due_date.Value.Day.ToString() + "/" + lstPO[0].Original_due_date.Value.Month.ToString() + "/" + lstPO[0].Original_due_date.Value.Year.ToString()));
                    }
                    // Sprint 1 - Point 14 - End - Changing from [Original_due_date] to [Expected Date]

                    DateTime ScheduledDate = LogicalSchedulDateTime;  //SchDate; // Utilities.Common.TextToDateFormat(GetFormatedDate(lstPO[0].Original_due_date.ToString()));

                    //OTIF Future Date PO check//
                    bool isOTIFPO = false;
                    CntOTIF_FutureDateSetupBE oCntOTIF_FutureDateSetupBE = new CntOTIF_FutureDateSetupBE();
                    CntOTIF_FutureDateSetupBAL oCntOTIF_FutureDateSetupBAL = new CntOTIF_FutureDateSetupBAL();
                    oCntOTIF_FutureDateSetupBE.Action = "ShowAll";
                    oCntOTIF_FutureDateSetupBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
                    oCntOTIF_FutureDateSetupBE.User.UserID = Convert.ToInt32(Session["UserID"]);
                    oCntOTIF_FutureDateSetupBE.CountryID = Convert.ToInt32(PreSiteCountryID);
                    List<CntOTIF_FutureDateSetupBE> lstCountryFutureDatePO = oCntOTIF_FutureDateSetupBAL.GetCntOTIF_FutureDateSetupDetailsBAL(oCntOTIF_FutureDateSetupBE);
                    if (lstCountryFutureDatePO != null && lstCountryFutureDatePO.Count > 0)
                    {
                        // This condition is given, if [Expected Date] will come null then [Original due date] will be set like earlier.
                        if (lstPO[0].Expected_date != null)
                        {
                            // Sprint 1 - Point 14 - Begin - Changing from [Original_due_date] to [Expected Date]
                            /*if (lstPO[0].Original_due_date.Value.Day == Convert.ToInt32(lstCountryFutureDatePO[0].FutureDatePODay) && lstPO[0].Original_due_date.Value.Month == Convert.ToInt32(lstCountryFutureDatePO[0].FutureDatePOMonth))*/
                            if (lstPO[0].Expected_date.Value.Day == Convert.ToInt32(lstCountryFutureDatePO[0].FutureDatePODay) && lstPO[0].Expected_date.Value.Month == Convert.ToInt32(lstCountryFutureDatePO[0].FutureDatePOMonth))
                            // Sprint 1 - Point 14 - End - Changing from [Original_due_date] to [Expected Date]
                            {
                                //This is OTIF PO Date
                                isOTIFPO = true;
                            }
                        }
                        else
                        {
                            if (lstPO[0].Original_due_date.Value.Day == Convert.ToInt32(lstCountryFutureDatePO[0].FutureDatePODay) && lstPO[0].Original_due_date.Value.Month == Convert.ToInt32(lstCountryFutureDatePO[0].FutureDatePOMonth))
                            {
                                //This is OTIF PO Date
                                isOTIFPO = true;
                            }
                        }
                    }
                    //------------------------//


                    bool isError = false;

                    if (isOTIFPO == false)
                    {
                        if (lstPO[0].Site.ToleranceDueDay == 0)
                        {
                            if (ScheduledDate < PODueDate)
                            {  //if (ScheduledDate < PODueDate) {
                                isError = true;
                            }
                        }
                        else if (lstPO[0].Site.ToleranceDueDay > 0)
                        {
                            if (PODueDate > ScheduledDate.AddDays(Convert.ToDouble(lstPO[0].Site.ToleranceDueDay)))
                            {
                                isError = true;
                            }
                        }

                        if (isError)
                        {           //error     
                            if (hdnCurrentRole.Value == "Vendor" || hdnCurrentRole.Value == "Carrier")
                            {
                                errMsg = WebCommon.getGlobalResourceValue("BK_MS_13_EXT").Replace("##dd/mm/yyyy##", PODueDate.ToString("dd/MM/yyyy"));  // "The  Purchase Order quoted is not due in on the date selected. Please rebook this Purchase Order for the correct date. <br> Due Date:" + PODueDate.ToString("dd/MM/yyyy"); 
                                ShowErrorMessage(errMsg, "BK_MS_13_EXT");
                                return;
                            }
                            else
                            {
                                errMsg = WebCommon.getGlobalResourceValue("BK_MS_13_INT").Replace("##dd/mm/yyyy##", PODueDate.ToString("dd/MM/yyyy")); // The Purchase Order quoted is not due in for this date.The correct due date is dd/mm/yyyy. Do you want to continue?
                                AddWarningMessages(errMsg, "BK_MS_13_INT");
                                isWarning = true;
                            }
                        }
                    }
                }

                if (isWarning == false)
                {
                    BindPOGrid(PurchaseNumber);
                }
                else
                {
                    if (!Multiple)
                    {
                        ShowWarningMessages();
                    }
                    else
                    {
                        BindPOGrid(PurchaseNumber);
                    }
                }
            }
            else if (IsBookingValidationExcluded == true)
            {  //If there is Validation Exclusion for the vendor/carrier or booking type
                BindPOGrid(PurchaseNumber);
            }
        }
        else
        {
            errMsg = WebCommon.getGlobalResourceValue("BK_MS_CorrectPO"); //"Please enter correct Purchase Order Number.";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "popup", "<script>alert('" + errMsg + "');</script>", false);
        }
    }

    private string GetFormatedDate(string DateToFormat)
    {
        if (DateToFormat != string.Empty)
        {
            string[] arrDate = DateToFormat.Split('/');
            string DD, MM, YYYY;
            if (arrDate[0].Length < 2)
                DD = "0" + arrDate[0].ToString();
            else
                DD = arrDate[0];

            if (arrDate[1].Length < 2)
                MM = "0" + arrDate[1].ToString();
            else
                MM = arrDate[1];

            YYYY = arrDate[2].ToString().Substring(0, 4);

            return DD + "/" + MM + "/" + YYYY.Trim();
        }
        else
        {
            return DateToFormat;
        }
    }

    private string GetFormatedTime(string TimeToFormat)
    {
        if (TimeToFormat != string.Empty)
        {
            string[] arrDate = TimeToFormat.Split(':');
            string HH, MM;
            if (arrDate[0].Length < 2)
                HH = "0" + arrDate[0].ToString();
            else
                HH = arrDate[0];

            if (arrDate[1].Length < 2)
                MM = "0" + arrDate[1].ToString();
            else
                MM = arrDate[1];


            return HH + ":" + MM;
        }
        else
        {
            return TimeToFormat;
        }
    }

    protected void BindPOGrid(string PurchaseNumber)
    {

        if (PurchaseNumber != string.Empty)
        {

            DataTable myDataTable = null;

            if (ViewState["myDataTable"] != null)
            {
                myDataTable = (DataTable)ViewState["myDataTable"];

                //Check Item alrady added
                DataRow[] drr = myDataTable.Select("PurchaseNumber='" + PurchaseNumber.Trim() + "'");

                if (drr.Length > 0)
                {
                    return;
                }
            }
            else
            {
                #region PO Data Table being Create ..
                myDataTable = new DataTable();

                DataColumn auto = new DataColumn("AutoID", typeof(System.Int32));
                // specify it as auto increment field
                auto.AutoIncrement = true;
                auto.AutoIncrementSeed = 1;
                auto.ReadOnly = true;
                auto.Unique = true;
                myDataTable.Columns.Add(auto);

                DataColumn myDataColumn = new DataColumn();
                myDataColumn.ColumnName = "PurchaseNumber";
                myDataTable.Columns.Add(myDataColumn);

                myDataColumn = new DataColumn();
                myDataColumn.ColumnName = "Original_due_date";
                myDataTable.Columns.Add(myDataColumn);

                myDataColumn = new DataColumn();
                myDataColumn.ColumnName = "VendorName";
                myDataTable.Columns.Add(myDataColumn);

                myDataColumn = new DataColumn();
                myDataColumn.ColumnName = "OutstandingLines";
                myDataTable.Columns.Add(myDataColumn);

                myDataColumn = new DataColumn();
                myDataColumn.ColumnName = "Qty_On_Hand";
                myDataTable.Columns.Add(myDataColumn);

                myDataColumn = new DataColumn();
                myDataColumn.ColumnName = "qty_on_backorder";
                myDataTable.Columns.Add(myDataColumn);

                myDataColumn = new DataColumn();
                myDataColumn.ColumnName = "SiteName";
                myDataTable.Columns.Add(myDataColumn);

                myDataColumn = new DataColumn();
                myDataColumn.ColumnName = "PurchaseOrderID";
                myDataTable.Columns.Add(myDataColumn);

                myDataColumn = new DataColumn();
                myDataColumn.ColumnName = "ExpectedDate";
                myDataTable.Columns.Add(myDataColumn);
                #endregion
            }


            Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE = new Up_PurchaseOrderDetailBE();
            UP_PurchaseOrderDetailBAL oUP_PurchaseOrderDetailBAL = new UP_PurchaseOrderDetailBAL();
            oUp_PurchaseOrderDetailBE.Vendor = new UP_VendorBE();

            oUp_PurchaseOrderDetailBE.Action = "GetNewBookingPODetails";
            oUp_PurchaseOrderDetailBE.Purchase_order = PurchaseNumber;
            oUp_PurchaseOrderDetailBE.CountryID = Convert.ToInt32(PreSiteCountryID);
            oUp_PurchaseOrderDetailBE.Vendor.VendorID = Convert.ToInt32(ucSeacrhVendorID);

            List<Up_PurchaseOrderDetailBE> lstPO = oUP_PurchaseOrderDetailBAL.GetProductOrderDetailsBAL(oUp_PurchaseOrderDetailBE);
            DataRow dataRow = myDataTable.NewRow();
            if (lstPO.Count > 0)
            {

                oUp_PurchaseOrderDetailBE.Action = "GetStockDetails";
                oUp_PurchaseOrderDetailBE.Purchase_order = PurchaseNumber;
                oUp_PurchaseOrderDetailBE.Site = new MAS_SiteBE();
                oUp_PurchaseOrderDetailBE.Site.SiteID = Convert.ToInt32(intSiteId); //Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);

                List<Up_PurchaseOrderDetailBE> lstPOStock = oUP_PurchaseOrderDetailBAL.GetPriorityBAL(oUp_PurchaseOrderDetailBE);

                if (lstPOStock != null && lstPOStock.Count > 0)
                {

                    List<Up_PurchaseOrderDetailBE> lstPOTemp = lstPOStock.FindAll(delegate(Up_PurchaseOrderDetailBE p) { return Convert.ToDecimal(p.SKU.Qty_On_Hand) == 0 && (Convert.ToInt32(Convert.ToDecimal(p.Original_quantity)) - p.Qty_receipted > 0); });

                    if (lstPOTemp != null && lstPOTemp.Count > 0)
                    {
                        dataRow["Qty_On_Hand"] = lstPOTemp.Count.ToString();
                    }
                    else
                    {
                        dataRow["Qty_On_Hand"] = "0";
                    }

                    //lstPOTemp = lstPOStock.FindAll(delegate(Up_PurchaseOrderDetailBE p) { return Convert.ToDecimal(p.SKU.qty_on_backorder) > 0 && (Convert.ToInt32(Convert.ToDecimal(p.Original_quantity)) - p.Qty_receipted > 0); });
                    lstPOTemp = lstPOStock.FindAll(delegate(Up_PurchaseOrderDetailBE p)
                    {
                        return (Convert.ToDecimal(p.SKU.Qty_On_Hand) - Convert.ToDecimal(p.SKU.qty_on_backorder) < 0);
                    }
                  );
                    if (lstPOTemp != null && lstPOTemp.Count > 0)
                    {
                        dataRow["qty_on_backorder"] = lstPOTemp.Count.ToString();
                    }
                    else
                    {
                        dataRow["qty_on_backorder"] = "0";
                    }
                }
                else
                {
                    dataRow["Qty_On_Hand"] = "0";
                    dataRow["qty_on_backorder"] = "0";
                }


                dataRow["PurchaseNumber"] = PurchaseNumber;
                dataRow["Original_due_date"] = lstPO[0].Original_due_date.Value.ToString("dd/MM/yyyy");
                // Sprint 1 - Point 14 - Begin - Adding [Expected Date]
                // This condition is given, if [Expected Date] will come null then [Original due date] will be set like earlier.
                if (lstPO[0].Expected_date != null)
                {
                    dataRow["ExpectedDate"] = lstPO[0].Expected_date.Value.ToString("dd/MM/yyyy");
                }
                else
                {
                    dataRow["ExpectedDate"] = lstPO[0].Original_due_date.Value.ToString("dd/MM/yyyy");
                }
                // Sprint 1 - Point 14 - End - Adding [Expected Date]

                dataRow["VendorName"] = lstPO[0].Vendor.VendorName;
                dataRow["OutstandingLines"] = lstPOStock.Count.ToString(); //lstPO[0].OutstandingLines.ToString(); //
                //dataRow["Qty_On_Hand"] = lstPO[0].SKU.Qty_On_Hand;
                //dataRow["qty_on_backorder"] = lstPO[0].SKU.qty_on_backorder;
                dataRow["SiteName"] = lstPO[0].Site.SiteName;
                dataRow["PurchaseOrderID"] = lstPO[0].PurchaseOrderID.ToString();

                myDataTable.Rows.Add(dataRow);
            }
            else
            {
                if (IsBookingValidationExcluded == true)
                {
                    dataRow["PurchaseNumber"] = PurchaseNumber;
                    dataRow["Original_due_date"] = ActualSchedulDateTime.ToString("dd/MM/yyyy");
                    // Sprint 1 - Point 14 - Begin - Adding [Expected Date]
                    dataRow["ExpectedDate"] = ActualSchedulDateTime.ToString("dd/MM/yyyy");
                    // Sprint 1 - Point 14 - End - Adding [Expected Date]

                    dataRow["VendorName"] = lblVendorT.Text; //ltvendorName.Text;
                    dataRow["OutstandingLines"] = "1";
                    dataRow["Qty_On_Hand"] = "0";
                    dataRow["qty_on_backorder"] = "0";
                    dataRow["SiteName"] = strSiteName; //ddlSite.innerControlddlSite.SelectedItem.Text;
                    dataRow["PurchaseOrderID"] = 0;
                    myDataTable.Rows.Add(dataRow);
                }
            }


            if (myDataTable != null && myDataTable.Rows.Count > 0)
            {
                gvPO.DataSource = myDataTable;
                gvPO.DataBind();
            }

            if (myDataTable != null && myDataTable.Rows.Count > 0)
            {
                ScriptManager.RegisterStartupScript(Page, this.GetType(), "btnshow", "$get('" + btnProceedBooking.ClientID + "').style.display='';", true);
                ViewState["myDataTable"] = myDataTable;
            }
            else
            {
                ScriptManager.RegisterStartupScript(Page, this.GetType(), "btnhide", "$get('" + btnProceedBooking.ClientID + "').style.display='none';", true);
                ViewState["myDataTable"] = null;
            }

            int TotalLines = 0;
            foreach (DataRow dr in myDataTable.Rows)
            {
                if (dr["OutstandingLines"].ToString() != string.Empty)
                    TotalLines += Convert.ToInt32(dr["OutstandingLines"]);
            }


            //for (int iCount = 0; iCount < myDataTable.Rows.Count; iCount++) {
            //    TotalLines += Convert.ToInt32(lstPO[0].OutstandingLines.ToString()); //Convert.ToInt32(dataRow["OutstandingLines"]);
            //}
            lblWeExpectXamount.Text = WebCommon.getGlobalResourceValue("WeExpectXamount").Replace("XX", TotalLines.ToString()); //lblExpectedLines.Text.Replace(Lines, TotalLines.ToString());
            Lines = TotalLines.ToString();

            //txtPurchaseNumber.Text = string.Empty;
        }

    }

    private void PaintAlternateSlotScreen()
    {

        ltVendorNo_2.Text = ucSeacrhVendorName; // ((HiddenField)ucSeacrhVendor1.FindControl("hdnVandorName")).Value;
        ltCarrier_2.Text = ddlCarrier.SelectedItem.Text;
        //ltOtherCarrier_2.Text = "[" + txtCarrierOther.Text.Trim() + "]";
        ltOtherCarrier_2.Text = "[" + ddlCarrier.SelectedItem.Text.Trim() + "]";
        if (ddlCarrier.SelectedItem.Text.Trim() != string.Empty)
        {
            ltOtherCarrier_2.Visible = true;
        }
        else
        {
            ltOtherCarrier_2.Visible = false;
        }

        ltVehicleType_2.Text = ddlVehicleType.SelectedItem.Text;
        //ltOtherVehicleType_2.Text = "[" + txtVehicleOther.Text.Trim() + "]";
        ltOtherVehicleType_2.Text = "[" + ddlVehicleType.SelectedItem.Text.Trim() + "]";
        if (ddlVehicleType.SelectedItem.Text.Trim() != string.Empty)
        {
            ltOtherVehicleType_2.Visible = true;
        }
        else
        {
            ltOtherVehicleType_2.Visible = false;
        }
        ltSchedulingDate_2.Text = ActualSchedulDateTime.ToString("dd/MM/yyyy");
        ltTimeSuggested_2.Text = ltSuggestedSlotTime.Text;

        int? TotalPallets = txtPalletsT.Text.Trim() != string.Empty ? Convert.ToInt32(txtPalletsT.Text.Trim()) : (int?)null;
        int? TotalCartons = txtCartonsT.Text.Trim() != string.Empty ? Convert.ToInt32(txtCartonsT.Text.Trim()) : (int?)null;

        if (btnConfirmBooking.Visible)
        {
            iPallets = TotalPallets != (int?)null ? Convert.ToInt32(TotalPallets) : 0;
            iCartons = TotalCartons != (int?)null ? Convert.ToInt32(TotalCartons) : 0;

            int SlotLen = GetTimeSlotLength();

            string[] StartTime = ltTimeSuggested_2.Text.Split(':');

            if (StartTime.Length == 2)
            {
                TimeSpan time1 = new TimeSpan(Convert.ToInt32(StartTime[0]), Convert.ToInt32(StartTime[1].Substring(0, 2)), 00);
                TimeSpan time2 = new TimeSpan(00, iSlotTimeLength, 00);
                //TimeSpan totalTime = new TimeSpan(00, 00, 00); ;
                for (int iLen = 1; iLen <= SlotLen; iLen++)
                {
                    if (time1.Hours == 23 && time1.Minutes == 55)
                    {
                        time1 = new TimeSpan(00, 00, 00);
                    }
                    else
                    {
                        time1 = time1.Add(time2);
                    }
                }

                ltlEstEnd_2.Text = time1.Hours.ToString() + ":" + time1.Minutes.ToString();
            }
        }


        ltNumberofLifts.Text = txtLiftsT.Text.Trim() != string.Empty ? txtLiftsT.Text.Trim() : "-";
        ltNumberofPallets.Text = txtPalletsT.Text.Trim() != string.Empty ? txtPalletsT.Text.Trim() : "-";
        //ltNumberofLines.Text = txtExpectedLines.Text.Trim() != string.Empty ? txtExpectedLines.Text.Trim() : "-";
        ltNumberofLines.Text = txtLinesT.Text.Trim() != string.Empty ? txtLinesT.Text.Trim() : "-";
        ltNumberofCartons.Text = txtCartonsT.Text.Trim() != string.Empty ? txtCartonsT.Text.Trim() : "-";

        GenerateNonTimeBooking();
        GenerateAlternateSlot();
        mvBookingEdit.ActiveViewIndex = 4;

    }

    private void GenerateAlternateSlotForVendor()
    {

        int? TotalPallets = txtPalletsT.Text.Trim() != string.Empty ? Convert.ToInt32(txtPalletsT.Text.Trim()) : (int?)null;
        int? TotalCartons = txtCartonsT.Text.Trim() != string.Empty ? Convert.ToInt32(txtCartonsT.Text.Trim()) : (int?)null;
        int? TotalLines = txtLinesT.Text.Trim() != string.Empty ? Convert.ToInt32(txtLinesT.Text.Trim()) : (int?)null;
        //int? TotalLines = txtExpectedLines.Text.Trim() != string.Empty ? Convert.ToInt32(txtExpectedLines.Text.Trim()) : (int?)null;

        actSlotLength = GetRequiredTimeSlotLength(TotalPallets, TotalCartons);

        #region ------------Basic Information--------------------------
        //-----------------Start Collect Basic Information --------------------------//
        DateTime dtSelectedDate = LogicalSchedulDateTime;
        string WeekDay = dtSelectedDate.ToString("dddd");

        DateTime dtMon = Utilities.Common.GetFirstDayOfWeek(dtSelectedDate);

        MASSIT_WeekSetupBE oMASSIT_WeekSetupBE = new MASSIT_WeekSetupBE();
        MASSIT_WeekSetupBAL oMASSIT_WeekSetupBAL = new MASSIT_WeekSetupBAL();


        oMASSIT_WeekSetupBE.Action = "GetSpecificWeek";
        oMASSIT_WeekSetupBE.SiteID = Convert.ToInt32(intSiteId); //Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
        oMASSIT_WeekSetupBE.EndWeekday = WeekDay;
        oMASSIT_WeekSetupBE.ScheduleDate = LogicalSchedulDateTime;

        List<MASSIT_WeekSetupBE> lstWeekSetup = oMASSIT_WeekSetupBAL.GetWeekSetupDetailsBAL(oMASSIT_WeekSetupBE);

        if (lstWeekSetup == null || lstWeekSetup.Count <= 0 || lstWeekSetup[0].StartTime == null)
        {

            oMASSIT_WeekSetupBE.Action = "ShowAll";
            lstWeekSetup = oMASSIT_WeekSetupBAL.GetWeekSetupDetailsBAL(oMASSIT_WeekSetupBE);

            if (lstWeekSetup.Count <= 0)
            {
                string errorMeesage = WebCommon.getGlobalResourceValue("WeekSetupSettingNotDefined");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert1", "alert('" + errorMeesage + "')", true);
                return;
            }
            else if (lstWeekSetup[0].StartTime == null)
            {
                string errorMeesage = WebCommon.getGlobalResourceValue("WeekSetupSettingNotDefined");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert1", "alert('" + errorMeesage + "')", true);
                return;
            }
        }


        APPSIT_DoorOpenTimeBAL oMASSIT_DoorOpenTimeBAL = new APPSIT_DoorOpenTimeBAL();
        MASSIT_DoorOpenTimeBE oMASSIT_DoorOpenTimeBE = new MASSIT_DoorOpenTimeBE();

        //------Check for Specific Entry First--------//
        //APPSIT_DoorOpenTimeSpecificBAL oAPPSIT_DoorOpenTimeSpecificBAL = new APPSIT_DoorOpenTimeSpecificBAL();
        //MASSIT_DoorOpenTimeSpecificBE oMASSIT_DoorOpenTimeSpecificBE = new MASSIT_DoorOpenTimeSpecificBE();
        //oMASSIT_DoorOpenTimeSpecificBE.Action = "GetDoorConstraintsSpecific";
        //oMASSIT_DoorOpenTimeSpecificBE.DoorOpenTime = new MASSIT_DoorOpenTimeBE();
        //oMASSIT_DoorOpenTimeSpecificBE.DoorOpenTime.DoorNo = new MASSIT_DoorNoSetupBE();
        //oMASSIT_DoorOpenTimeSpecificBE.DoorOpenTime.DoorType = new BusinessEntities.ModuleBE.Appointment.CountrySetting.MASCNT_DoorTypeBE();
        //oMASSIT_DoorOpenTimeSpecificBE.DoorOpenTime.DoorNo.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
        //oMASSIT_DoorOpenTimeSpecificBE.DoorOpenTime.Weekday = WeekDay;
        //oMASSIT_DoorOpenTimeSpecificBE.WeekStartDate = dtMon;
        //List<MASSIT_DoorOpenTimeSpecificBE> lstDoorConstraintsSpecific = oAPPSIT_DoorOpenTimeSpecificBAL.GetDoorConstraintsSpecificBAL(oMASSIT_DoorOpenTimeSpecificBE);
        //if (lstDoorConstraintsSpecific.Count > 0) { //Get Specific settings 
        //    for (int iCount = 0; iCount < lstDoorConstraintsSpecific.Count; iCount++) {
        //        DoorIDs += lstDoorConstraintsSpecific[iCount].DoorOpenTime.SlotTimeID + "@" + lstDoorConstraintsSpecific[iCount].DoorOpenTime.SiteDoorNumberID + ",";
        //    }
        //}
        //else {  //Get Generic settings 
        //    oMASSIT_DoorOpenTimeBE.Action = "GetDoorConstraints";
        //    oMASSIT_DoorOpenTimeBE.DoorNo = new MASSIT_DoorNoSetupBE();
        //    oMASSIT_DoorOpenTimeBE.DoorNo.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
        //    oMASSIT_DoorOpenTimeBE.Weekday = WeekDay;
        //    List<MASSIT_DoorOpenTimeBE> lstDoorConstraints = oMASSIT_DoorOpenTimeBAL.GetDoorConstraintsBAL(oMASSIT_DoorOpenTimeBE);
        //    for (int iCount = 0; iCount < lstDoorConstraints.Count; iCount++) {
        //        DoorIDs += lstDoorConstraints[iCount].SlotTimeID + "@" + lstDoorConstraints[iCount].SiteDoorNumberID + ",";

        //    }
        //}

        DoorIDs = GetDoorConstraints();

        oMASSIT_DoorOpenTimeBE.Action = "GetDoorNoAndType";
        oMASSIT_DoorOpenTimeBE.DoorNo = new MASSIT_DoorNoSetupBE();
        oMASSIT_DoorOpenTimeBE.DoorNo.SiteID = Convert.ToInt32(intSiteId); //Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);

        List<MASSIT_DoorOpenTimeBE> lstDoorNoType = oMASSIT_DoorOpenTimeBAL.GetDoorNoSetupDetailsBAL(oMASSIT_DoorOpenTimeBE);

        int iDoorCount = lstDoorNoType.Count;

        //decimal decCellWidth = (90 / (iDoorCount + 1));
        //double dblCellWidth = Convert.ToDouble(Math.Ceiling(decCellWidth));
        int dblCellWidth = 165;
        int i1stColumnWidth = 145;

        //-------------------------End Collect Basic Information ------------------------------//
        #endregion

        #region --------------Grid Header---------------
        //-------------Start Grid Header--------------//
        Table tblSlot = new Table();
        tblSlot.CellSpacing = 0;
        tblSlot.CellPadding = 0;
        tblSlot.CssClass = "FixedTables";
        tblSlot.ID = "Open_Text_General";
        tblSlot.Width = Unit.Pixel((dblCellWidth * iDoorCount) + i1stColumnWidth);

        tableDiv_GeneralVendor.Controls.Add(tblSlot);

        TableRow trDoorNo = new TableRow();
        trDoorNo.TableSection = TableRowSection.TableHeader;
        TableHeaderCell tcDoorNo = new TableHeaderCell();
        tcDoorNo.Text = "Door Name / Door Type";  // +"<input type=image src='../../../Images/arrowleft.gif' style=height:16px; width:26px; />";
        tcDoorNo.BorderWidth = Unit.Pixel(1);
        tcDoorNo.Width = Unit.Pixel(i1stColumnWidth);
        trDoorNo.Cells.Add(tcDoorNo);

        TableRow trTime = new TableRow();
        trTime.TableSection = TableRowSection.TableHeader;
        TableHeaderCell tcTime = new TableHeaderCell();
        tcTime.Text = "Time";
        tcTime.BorderWidth = Unit.Pixel(1);
        tcTime.Width = Unit.Pixel(i1stColumnWidth);
        trTime.Cells.Add(tcTime);

        for (int iCount = 1; iCount <= iDoorCount; iCount++)
        {
            tcDoorNo = new TableHeaderCell();
            tcDoorNo.BorderWidth = Unit.Pixel(1);
            tcDoorNo.Text = lstDoorNoType[iCount - 1].DoorNo.DoorNumber.ToString() + " / " + lstDoorNoType[iCount - 1].DoorType.DoorType; ;
            tcDoorNo.Width = Unit.Pixel(dblCellWidth);
            trDoorNo.Cells.Add(tcDoorNo);

            tcTime = new TableHeaderCell();
            tcTime.BorderWidth = Unit.Pixel(1);
            tcTime.Text = "Vendor/Carrier";
            tcTime.Width = Unit.Pixel(dblCellWidth);
            trTime.Cells.Add(tcTime);
        }
        trDoorNo.BackColor = System.Drawing.Color.LightGray;
        tblSlot.Rows.Add(trDoorNo);

        trTime.BackColor = System.Drawing.Color.LightGray;
        tblSlot.Rows.Add(trTime);
        //-------------End Grid Header -------------//
        #endregion

        #region --------------Slot Ploting---------------
        //-------------Start Slot Ploting--------------//        

        //--------Find all Fixed Slots, Bookings For all Vendor/Carrier-------//
        APPBOK_BookingBE oAPPBOK_BookingBE = new APPBOK_BookingBE();
        APPBOK_BookingBAL oAPPBOK_BookingBAL = new APPBOK_BookingBAL();

        oAPPBOK_BookingBE.Action = "AllBookingFixedSlotDetails";
        oAPPBOK_BookingBE.SiteId = Convert.ToInt32(intSiteId); //Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
        oAPPBOK_BookingBE.ScheduleDate = LogicalSchedulDateTime;
        oAPPBOK_BookingBE.SelectedScheduleDate = LogicalSchedulDateTime;
        oAPPBOK_BookingBE.VehicleType = new MASSIT_VehicleTypeBE();
        //oAPPBOK_BookingBE.VehicleType.VehicleTypeID = Convert.ToInt32(ddlVehicleType.SelectedItem.Value);
        lstAllFixedDoor = oAPPBOK_BookingBAL.GetAllBookingFixedSlotDetailsBAL(oAPPBOK_BookingBE);
        //--------------------------------------//


        if (lstWeekSetup[0].StartWeekday != lstWeekSetup[0].EndWeekday)
        {
            oAPPBOK_BookingBE.Action = "AllBookingFixedSlotDetails";
            oAPPBOK_BookingBE.SiteId = Convert.ToInt32(intSiteId); //Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
            oAPPBOK_BookingBE.ScheduleDate = LogicalSchedulDateTime.AddDays(-1);
            List<APPBOK_BookingBE> lstAllFixedDoorStartWeek = oAPPBOK_BookingBAL.GetAllBookingFixedSlotDetailsBAL(oAPPBOK_BookingBE);

            //string StartTime = lstWeekSetup[0].StartTime.Value.Hour.ToString() + ":" + lstWeekSetup[0].StartTime.Value.Minute.ToString();

            //Filetr Non Time Delivery
            lstAllFixedDoorStartWeek = lstAllFixedDoorStartWeek.FindAll(delegate(APPBOK_BookingBE St)
            {
                return St.SlotTime.SlotTime != "";
            });

            if (lstAllFixedDoorStartWeek != null && lstAllFixedDoorStartWeek.Count > 0)
            {
                lstAllFixedDoorStartWeek = lstAllFixedDoorStartWeek.FindAll(delegate(APPBOK_BookingBE St)
                {
                    DateTime? dt = (DateTime?)null;
                    if (St.SlotTime.SlotTime != null && St.SlotTime.SlotTime != string.Empty)
                    {
                        dt = new DateTime(1900, 1, 1, Convert.ToInt32(St.SlotTime.SlotTime.Split(':')[0]), Convert.ToInt32(St.SlotTime.SlotTime.Split(':')[1]), 0);
                    }
                    else
                    {
                        dt = new DateTime(1900, 1, 1, 0, 0, 0);
                    }
                    //DateTime dt = new DateTime(1900, 1, 1, Convert.ToInt32(St.SlotTime.SlotTime.Split(':')[0]), Convert.ToInt32(St.SlotTime.SlotTime.Split(':')[1]), 0);
                    return dt >= lstWeekSetup[0].StartTime;
                });

                if (lstAllFixedDoorStartWeek.Count > 0)
                {
                    lstAllFixedDoor = MergeListCollections(lstAllFixedDoor, lstAllFixedDoorStartWeek);
                    //List<APPBOK_BookingBE>  lstAllFixedDoorFinal = new List<APPBOK_BookingBE>(lstAllFixedDoor.Count + lstAllFixedDoorStartWeek.Count);
                }
            }
        }


        SYS_SlotTimeBAL oSYS_SlotTimeBAL = new SYS_SlotTimeBAL();
        SYS_SlotTimeBE oSYS_SlotTimeBE = new SYS_SlotTimeBE();

        oSYS_SlotTimeBE.Action = "ShowAll";
        List<SYS_SlotTimeBE> lstSlotTime = oSYS_SlotTimeBAL.GetSlotTimeBAL(oSYS_SlotTimeBE);

        for (int jCount = 0; jCount < lstSlotTime.Count; jCount++)
        {
            string StartTime = lstWeekSetup[0].StartTime.Value.Hour.ToString() + "." + lstWeekSetup[0].StartTime.Value.Minute.ToString();
            if (lstSlotTime[jCount].SlotTime.Split(new char[] { ':' })[0] == "00")
            {
                StartTime = "24." + lstWeekSetup[0].StartTime.Value.Minute.ToString();
            }

            string EndTime = lstWeekSetup[0].EndTime.Value.Hour.ToString() + "." + lstWeekSetup[0].EndTime.Value.Minute.ToString();
            if (lstSlotTime[jCount].SlotTime.Split(new char[] { ':' })[0] == "00")
            {
                EndTime = "24." + lstWeekSetup[0].EndTime.Value.Minute.ToString();
            }

            decimal dSlotTime = Convert.ToDecimal(lstSlotTime[jCount].SlotTime.Replace(':', '.'));
            decimal dStartTime = Convert.ToDecimal(StartTime);
            decimal dEndTime = Convert.ToDecimal(EndTime);

            if (lstWeekSetup[0].StartWeekday != lstWeekSetup[0].EndWeekday)
            {
                dEndTime = Convert.ToDecimal("24.00");
            }

            if (dSlotTime >= dStartTime && dSlotTime <= dEndTime)
            {
                TableRow tr = new TableRow();
                tr.TableSection = TableRowSection.TableBody;
                TableCell tc = new TableCell();
                tc.BorderWidth = Unit.Pixel(1);

                if (lstSlotTime.Count - 1 > jCount)
                    tc.Text = lstWeekSetup[0].StartWeekday + " " + GetFormatedTime(lstSlotTime[jCount].SlotTime) + " - " + GetFormatedTime(lstSlotTime[jCount + 1].SlotTime);
                else
                    tc.Text = lstWeekSetup[0].StartWeekday + " " + GetFormatedTime(lstSlotTime[jCount].SlotTime) + " - " + GetFormatedTime(lstSlotTime[0].SlotTime);

                tc.Width = Unit.Pixel(i1stColumnWidth);
                tr.Cells.Add(tc);

                for (int iCount = 1; iCount <= iDoorCount; iCount++)
                {

                    tc = new TableCell();
                    tc.BorderWidth = Unit.Pixel(0);
                    tc.Width = Unit.Pixel(dblCellWidth);

                    iTimeSlotID = lstSlotTime[jCount].SlotTimeID;
                    iSiteDoorID = lstDoorNoType[iCount - 1].SiteDoorNumberID;
                    sTimeSlot = lstSlotTime[jCount].SlotTime;
                    sDoorNo = lstDoorNoType[iCount - 1].DoorNo.DoorNumber;
                    sSlotStartDay = lstWeekSetup[0].StartWeekday;

                    //----Sprint 3b------//
                    iTimeSlotOrderBYID = lstSlotTime[jCount].OrderBYID;
                    //------------------//


                    Literal ddl = SetCellColor(ref tc);
                    tc.Controls.Add(ddl);
                    tr.Cells.Add(tc);
                }
                tblSlot.Rows.Add(tr);
            }
        }

        if (lstWeekSetup[0].StartWeekday != lstWeekSetup[0].EndWeekday)
        {
            for (int jCount = 0; jCount < lstSlotTime.Count; jCount++)
            {
                string EndTime = lstWeekSetup[0].EndTime.Value.Hour.ToString() + "." + lstWeekSetup[0].EndTime.Value.Minute.ToString();
                if (lstSlotTime[jCount].SlotTime.Split(new char[] { ':' })[0] == "00")
                {
                    EndTime = "24." + lstWeekSetup[0].EndTime.Value.Minute.ToString();
                }
                if (Convert.ToDecimal(lstSlotTime[jCount].SlotTime.Replace(':', '.')) <= Convert.ToDecimal(EndTime))
                {
                    TableRow tr = new TableRow();
                    tr.TableSection = TableRowSection.TableBody;
                    TableCell tc = new TableCell();
                    tc.BorderWidth = Unit.Pixel(1);

                    if (lstSlotTime.Count - 1 > jCount)
                        tc.Text = lstWeekSetup[0].EndWeekday + " " + GetFormatedTime(lstSlotTime[jCount].SlotTime) + " - " + GetFormatedTime(lstSlotTime[jCount + 1].SlotTime);
                    else
                        tc.Text = lstWeekSetup[0].EndWeekday + " " + GetFormatedTime(lstSlotTime[jCount].SlotTime) + " - " + GetFormatedTime(lstSlotTime[0].SlotTime);

                    tc.Width = Unit.Pixel(i1stColumnWidth);
                    tr.Cells.Add(tc);

                    for (int iCount = 1; iCount <= iDoorCount; iCount++)
                    {


                        tc = new TableCell();
                        tc.Width = Unit.Pixel(dblCellWidth);
                        tc.BorderWidth = Unit.Pixel(0);

                        iTimeSlotID = lstSlotTime[jCount].SlotTimeID;
                        iSiteDoorID = lstDoorNoType[iCount - 1].SiteDoorNumberID;
                        sTimeSlot = lstSlotTime[jCount].SlotTime;
                        sDoorNo = lstDoorNoType[iCount - 1].DoorNo.DoorNumber;
                        sSlotStartDay = lstWeekSetup[0].EndWeekday;

                        //----Sprint 3b------//
                        iTimeSlotOrderBYID = lstSlotTime[jCount].OrderBYID;
                        //------------------//

                        Literal ddl = SetCellColor(ref tc);
                        tc.Controls.Add(ddl);
                        tr.Cells.Add(tc);

                    }
                    tblSlot.Rows.Add(tr);

                }
            }
        }

        //-------------End Slot Ploting--------------//
        #endregion

        #region -------------Start Finding Insufficent Slot Time---------------------
        Color cBackColor, cBorderColor, cBackColorPreviousCell, cBorderColorPreviousCell;
        bool isNextSlot;
        bool isNewCell = false;
        cBorderColorPreviousCell = Color.Empty;
        string strCheckedHour = "-10";
        bool isValidTimeSlot = false;

        for (int iCell = 1; iCell <= iDoorCount; iCell++)
        {
            cBackColorPreviousCell = Color.Empty;
            for (int iRow = 0; iRow < tblSlot.Rows.Count; iRow++)
            {
                //FindNextSufficentSlotTime(ref tblSlot, iCell, iRow);                
                cBackColor = tblSlot.Rows[iRow].Cells[iCell].BackColor;
                cBorderColor = tblSlot.Rows[iRow].Cells[iCell].BorderColor;

                if (iRow == 15 && iCell == 1)
                    //if (cBackColor == Color.GreenYellow)
                    cBackColor = tblSlot.Rows[iRow].Cells[iCell].BackColor;

                if (cBackColor == Color.White && cBorderColor != Color.Red)
                {
                    isNextSlot = true;
                    for (int iLen = 0; iLen < actSlotLength; iLen++)
                    {
                        if (iRow + iLen < tblSlot.Rows.Count)
                        {
                            cBackColor = tblSlot.Rows[iRow + iLen].Cells[iCell].BackColor;
                            if (cBackColor == Color.GreenYellow)
                            {
                                isNextSlot = true;
                                break;
                            }
                            if (cBackColor != Color.White)
                            {
                                isNextSlot = false;
                                break;
                            }
                        }
                        else
                        {
                            isNextSlot = false;
                            break;
                        }

                    }
                    if (isNextSlot == false)
                    {
                        //tblSlot.Rows[iRow].Cells[iCell].BackColor = Color.LightGray;

                        if (hdnCurrentRole.Value == "Vendor")
                        {
                            tblSlot.Rows[iRow].Cells[iCell].BackColor = Color.Black;
                            tblSlot.Rows[iRow].Cells[iCell].BorderWidth = 0;
                        }
                        else
                        {
                            tblSlot.Rows[iRow].Cells[iCell].BackColor = Color.LightGray;
                            tblSlot.Rows[iRow].Cells[iCell].BorderWidth = 0;
                        }

                        //tblSlot.Rows[iRow].Cells[iCell].Attributes.Add("onmouseover", "this.style.cursor = 'default'");
                        //tblSlot.Rows[iRow].Cells[iCell].Attributes.Add("onmouseout", "this.style.cursor = 'default'");
                        //tblSlot.Rows[iRow].Cells[iCell].Attributes.Add("onclick", "");
                    }
                }
                else if (cBackColor == Color.GreenYellow)
                {
                    string abbr = tblSlot.Rows[iRow].Cells[iCell].Attributes["abbr"];
                    if (abbr == null || abbr == string.Empty)
                    {
                        if (cBorderColor != Color.Red)
                        {
                            tblSlot.Rows[iRow].Cells[iCell].BackColor = Color.White;
                            tblSlot.Rows[iRow].Cells[iCell].BorderWidth = 1;
                        }
                        else if (cBorderColor == Color.Red)
                        {
                            tblSlot.Rows[iRow].Cells[iCell].BackColor = Color.Black;
                            tblSlot.Rows[iRow].Cells[iCell].BorderWidth = 0;
                        }
                    }
                }

                #region -- Blackout the slots which are not fullfill the delivery constraints-------//
                //Check delivery constraints// 
                string att = tblSlot.Rows[iRow].Cells[iCell].Attributes["onclick"];

                if (att != null && att != string.Empty && att.Contains("CheckAletrnateSlot"))
                {
                    att = att.Replace("CheckAletrnateSlot(", "");
                    att = att.Replace(");", "");
                    string[] Arratt = att.Split(',');


                    string att0 = Arratt[0].Replace("'", "");
                    string att1 = Arratt[1].Replace("'", "");
                    string att2 = Arratt[2].Replace("'", "");
                    string att3 = Arratt[3].Replace("'", "");
                    string att4 = Arratt[5].Replace("'", "");
                    string att5 = Arratt[5].Replace("'", "");

                    //--------- Sprint 3b----------//
                    string att6 = Arratt[6].Replace("'", "");
                    //-----------------------------//

                    if (att3.Split(':')[0] != strCheckedHour)
                    {
                        isValidTimeSlot = CheckValidSlot(att3, att5, Convert.ToString(TotalPallets), Convert.ToString(TotalLines));
                        strCheckedHour = att3.Split(':')[0];
                    }

                    //Check Before and after time//
                    if (LogicalSchedulDateTime.ToString("ddd").ToLower() == att4.ToLower())
                    {
                        if (Convert.ToInt32(att6) < Convert.ToInt32(BeforeSlotTimeID) || Convert.ToInt32(att6) > Convert.ToInt32(AfterSlotTimeID))
                        {
                            isValidTimeSlot = false;
                        }
                    }
                    //--------------------------//

                }
                else if (att != null && att != string.Empty && att.Contains("setAletrnateSlot"))
                {
                    att = att.Replace("setAletrnateSlot(", "");
                    att = att.Replace(");", "");
                    string[] Arratt = att.Split(',');
                    string att5 = Arratt[5].Replace("'", "");
                    string att6 = Arratt[6].Replace("'", "");

                    if (att5.Split(':')[0] != strCheckedHour)
                    {
                        isValidTimeSlot = CheckValidSlot(att5, att6, Convert.ToString(TotalPallets), Convert.ToString(TotalLines));
                        strCheckedHour = att5.Split(':')[0];
                    }

                    //if (isValidTimeSlot == false) {

                    //}
                }

                att = tblSlot.Rows[iRow].Cells[iCell].Attributes["onclick"];
                if (att != null && att != string.Empty && isValidTimeSlot == false)
                {

                    if (tblSlot.Rows[iRow].Cells[iCell].BackColor == Color.White && tblSlot.Rows[iRow].Cells[iCell].BorderColor == Color.Red)
                    {
                        tblSlot.Rows[iRow].Cells[iCell].ForeColor = Color.White;
                    }

                    string att4 = string.Empty;
                    if (att.Contains("setAletrnateSlot"))
                    {
                        att = att.Replace("setAletrnateSlot(", "");
                        att = att.Replace(");", "");
                        string[] Arratt = att.Split(',');
                        string att0 = Arratt[0].Replace("'", "");
                        string att1 = Arratt[1].Replace("'", "");
                        string att2 = Arratt[2].Replace("'", "");
                        string att3 = Arratt[3].Replace("'", "");
                        att4 = Arratt[4].Replace("'", "");  //fixed slot id
                        string att5 = Arratt[5].Replace("'", "");
                        string att6 = Arratt[6].Replace("'", "");

                        //tblSlot.Rows[iRow].Cells[iCell].Attributes.Add("onclick", "setAletrnateSlotForVendor('true','" + att1 + "','" + att2 + "','" + att3 + "','" + att4 + "','" + att5 + "','" + att6 + "');");

                    }



                    if (att4 == string.Empty || att4 == "-1")
                    {
                        tblSlot.Rows[iRow].Cells[iCell].BackColor = Color.Black;
                        tblSlot.Rows[iRow].Cells[iCell].BorderColor = Color.Black;
                        tblSlot.Rows[iRow].Cells[iCell].BorderWidth = 1;

                        tblSlot.Rows[iRow].Cells[iCell].Attributes.Add("onmouseover", "this.style.cursor = 'default'");
                        tblSlot.Rows[iRow].Cells[iCell].Attributes.Add("onmouseout", "this.style.cursor = 'default'");
                        tblSlot.Rows[iRow].Cells[iCell].Attributes.Add("onclick", "");
                    }
                }
                #endregion--------------------------------------------------------------------------//

                #region--Blackout the slots which are not matched with the seleted vehicle type-------//
                att = tblSlot.Rows[iRow].Cells[iCell].Attributes["onclick"];

                if (!strVehicleDoorTypeMatrix.Contains(lstDoorNoType[iCell - 1].DoorNo.SiteDoorTypeID.ToString() + ","))
                {
                    if (tblSlot.Rows[iRow].Cells[iCell].BackColor == Color.White && tblSlot.Rows[iRow].Cells[iCell].BorderColor == Color.Red)
                    {
                        tblSlot.Rows[iRow].Cells[iCell].ForeColor = Color.White;
                    }


                    string att4 = string.Empty;
                    if (att != null && att != string.Empty && att.Contains("setAletrnateSlot"))
                    {
                        att = att.Replace("setAletrnateSlot(", "");
                        att = att.Replace(");", "");
                        string[] Arratt = att.Split(',');
                        string att0 = Arratt[0].Replace("'", "");
                        string att1 = Arratt[1].Replace("'", "");
                        string att2 = Arratt[2].Replace("'", "");
                        string att3 = Arratt[3].Replace("'", "");
                        att4 = Arratt[4].Replace("'", "");  //fixed slot id
                        string att5 = Arratt[5].Replace("'", "");
                        string att6 = Arratt[6].Replace("'", "");

                        //tblSlot.Rows[iRow].Cells[iCell].Attributes.Add("onclick", "setAletrnateSlotForVendor('true','" + att1 + "','" + att2 + "','" + att3 + "','" + att4 + "','" + att5 + "','" + att6 + "');");

                    }

                    if (att4 == string.Empty || att4 == "-1")
                    {
                        tblSlot.Rows[iRow].Cells[iCell].BackColor = Color.Black;
                        tblSlot.Rows[iRow].Cells[iCell].BorderColor = Color.Black;
                        tblSlot.Rows[iRow].Cells[iCell].BorderWidth = 1;

                        tblSlot.Rows[iRow].Cells[iCell].Attributes.Add("onmouseover", "this.style.cursor = 'default'");
                        tblSlot.Rows[iRow].Cells[iCell].Attributes.Add("onmouseout", "this.style.cursor = 'default'");
                        tblSlot.Rows[iRow].Cells[iCell].Attributes.Add("onclick", "");
                    }
                }
                #endregion----------------------------------------------------------------------------//

                #region Row Spaning
                //-----------------------Row Spaning-------------//

                cBackColor = tblSlot.Rows[iRow].Cells[iCell].BackColor;
                cBorderColor = tblSlot.Rows[iRow].Cells[iCell].BorderColor;

                if (cBackColor != cBackColorPreviousCell || cBorderColor != cBorderColorPreviousCell)
                {
                    isNewCell = true;
                }

                bool isCellCreated = false;
                if (cBackColor == cBackColorPreviousCell && cBackColor != Color.White)
                {

                    isCellCreated = true;
                    tblSlot.Rows[iRow].Cells[iCell].Style.Add("border-top-width", "0px");  // .BorderWidth = 0;
                    tblSlot.Rows[iRow].Cells[iCell].Style.Add("border-bottom-width", "0px");

                    if (cBorderColor == Color.Red || cBackColor == Color.Purple)
                    {
                        tblSlot.Rows[iRow].Cells[iCell].Style.Add("border-left-width", "2px");
                        tblSlot.Rows[iRow].Cells[iCell].Style.Add("border-right-width", "2px");
                    }
                    else
                    {
                        tblSlot.Rows[iRow].Cells[iCell].Style.Add("border-left-width", "1px");
                        tblSlot.Rows[iRow].Cells[iCell].Style.Add("border-right-width", "1px");
                    }

                    if (cBackColorPreviousCell == cBackColor && iRow > 0 && isNewCell == true)
                    {
                        isNewCell = false;
                        tblSlot.Rows[iRow - 1].Cells[iCell].Style.Add("border-bottom-width", "0px");

                        if (cBorderColorPreviousCell == Color.Red || cBackColorPreviousCell == Color.Purple)
                        {
                            tblSlot.Rows[iRow - 1].Cells[iCell].Style.Add("border-top-width", "2px");
                            tblSlot.Rows[iRow - 1].Cells[iCell].Style.Add("border-left-width", "2px");
                            tblSlot.Rows[iRow - 1].Cells[iCell].Style.Add("border-right-width", "2px");
                        }
                        else
                        {
                            tblSlot.Rows[iRow - 1].Cells[iCell].Style.Add("border-top-width", "1px");
                            tblSlot.Rows[iRow - 1].Cells[iCell].Style.Add("border-left-width", "1px");
                            tblSlot.Rows[iRow - 1].Cells[iCell].Style.Add("border-right-width", "1px");
                        }
                    }
                }
                else if (cBackColor == cBackColorPreviousCell && cBorderColor == Color.Red)
                {

                    isCellCreated = true;

                    tblSlot.Rows[iRow].Cells[iCell].Style.Add("border-top-width", "0px");  // .BorderWidth = 0;
                    tblSlot.Rows[iRow].Cells[iCell].Style.Add("border-bottom-width", "0px");

                    tblSlot.Rows[iRow].Cells[iCell].Style.Add("border-left-width", "2px");
                    tblSlot.Rows[iRow].Cells[iCell].Style.Add("border-right-width", "2px");


                    if (cBackColorPreviousCell == cBackColor && iRow > 0 && isNewCell == true)
                    {
                        isNewCell = false;

                        if (cBorderColorPreviousCell != Color.Red)
                            tblSlot.Rows[iRow].Cells[iCell].Style.Add("border-top-width", "2px");

                        tblSlot.Rows[iRow - 1].Cells[iCell].Style.Add("border-bottom-width", "0px");
                        if (cBorderColorPreviousCell == Color.Red || cBackColorPreviousCell == Color.Purple)
                        {
                            tblSlot.Rows[iRow - 1].Cells[iCell].Style.Add("border-top-width", "2px");
                            tblSlot.Rows[iRow - 1].Cells[iCell].Style.Add("border-left-width", "2px");
                            tblSlot.Rows[iRow - 1].Cells[iCell].Style.Add("border-right-width", "2px");
                        }
                        else
                        {
                            tblSlot.Rows[iRow - 1].Cells[iCell].Style.Add("border-top-width", "1px");
                            tblSlot.Rows[iRow - 1].Cells[iCell].Style.Add("border-left-width", "1px");
                            tblSlot.Rows[iRow - 1].Cells[iCell].Style.Add("border-right-width", "1px");
                        }
                    }
                }

                if (isCellCreated == false && iRow > 0)
                {
                    if (cBorderColorPreviousCell == Color.Red || cBackColorPreviousCell == Color.Purple)
                        tblSlot.Rows[iRow - 1].Cells[iCell].Style.Add("border-bottom-width", "2px");
                    else
                        tblSlot.Rows[iRow - 1].Cells[iCell].Style.Add("border-bottom-width", "1px");
                }

                string abbr1 = tblSlot.Rows[iRow].Cells[iCell].Attributes["abbr"];
                if (abbr1 != null && abbr1 != string.Empty)
                {
                    if (cBorderColor == Color.Red || cBackColor == Color.Purple)
                    {
                        tblSlot.Rows[iRow].Cells[iCell].Style.Add("border-top-width", "2px");
                    }
                    else
                    {
                        tblSlot.Rows[iRow].Cells[iCell].Style.Add("border-top-width", "1px");
                    }
                }

                cBackColorPreviousCell = cBackColor;
                cBorderColorPreviousCell = cBorderColor;

                //--------------------------------------------------//
                #endregion

                cBackColor = tblSlot.Rows[iRow].Cells[iCell].BackColor;
                if (cBackColor == Color.Black)
                {
                    tblSlot.Rows[iRow].Cells[iCell].BorderWidth = 1;
                    tblSlot.Rows[iRow].Cells[iCell].BorderColor = Color.Black;

                    tblSlot.Rows[iRow].Cells[iCell].Attributes.Add("onmouseover", "this.style.cursor = 'default'");
                    tblSlot.Rows[iRow].Cells[iCell].Attributes.Add("onmouseout", "this.style.cursor = 'default'");
                    tblSlot.Rows[iRow].Cells[iCell].Attributes.Add("onclick", "");
                }
            }
        }

        #endregion

        #region Set Background color for Fixed slot
        bool GreenCellFound = false;
        for (int iRow = 0; iRow < tblSlot.Rows.Count; iRow++)
        {

            for (int iCell = 1; iCell <= iDoorCount; iCell++)
            {
                cBackColor = tblSlot.Rows[iRow].Cells[iCell].BackColor;
                cBorderColor = tblSlot.Rows[iRow].Cells[iCell].BorderColor;


                //if (cBorderColor == Color.Red && (cBackColor == Color.White || cBackColor == Color.GreenYellow)) {
                if (cBorderColor == Color.Red && (cBackColor == Color.White))
                {
                    tblSlot.Rows[iRow].Cells[iCell].BackColor = Color.LightYellow;
                    tblSlot.Rows[iRow].Cells[iCell].ForeColor = Color.Black;
                }

                if (cBorderColor == Color.Red && cBackColor == Color.GreenYellow && GreenCellFound == true)
                {
                    tblSlot.Rows[iRow].Cells[iCell].BackColor = Color.LightYellow;
                    tblSlot.Rows[iRow].Cells[iCell].ForeColor = Color.Black;
                }

                if (cBackColor == Color.GreenYellow || cBackColor == Color.Purple)
                {
                    //tblSlot.Rows[iRow].Cells[iCell].Attributes.Add("onclick", "");
                }

                if (cBackColor == Color.GreenYellow)
                    GreenCellFound = true;
            }
        }
        #endregion
    }

    private bool CheckValidSlot(string strStartTime, string strStartWeekDay, string strTotalPallets, string strTotalLines)
    {   //Check delivery constraints

        if (string.IsNullOrEmpty(strTotalPallets))
            strTotalPallets = "0";
        if (string.IsNullOrEmpty(strTotalLines))
            strTotalLines = "0";

        APPSIT_DeliveryConstraintSpecificBAL oAPPSIT_DeliveryConstraintSpecificBAL = new APPSIT_DeliveryConstraintSpecificBAL();
        MASSIT_DeliveryConstraintSpecificBE oMASSIT_DeliveryConstraintSpecificBE = new MASSIT_DeliveryConstraintSpecificBE();


        DateTime dtSelectedDate = LogicalSchedulDateTime;
        string WeekDay = dtSelectedDate.ToString("ddd");

        oMASSIT_DeliveryConstraintSpecificBE.Action = "GetDeliveryConstraintForBooking";
        oMASSIT_DeliveryConstraintSpecificBE.DeliveryConstraint = new MASSIT_DeliveryConstraintBE();
        oMASSIT_DeliveryConstraintSpecificBE.DeliveryConstraint.SiteID = Convert.ToInt32(intSiteId); //Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
        oMASSIT_DeliveryConstraintSpecificBE.DeliveryConstraint.Weekday = WeekDay;
        oMASSIT_DeliveryConstraintSpecificBE.DeliveryConstraint.StartTime = Convert.ToInt32(strStartTime.Split(':')[0]);
        oMASSIT_DeliveryConstraintSpecificBE.WeekStartDate = dtSelectedDate;
        oMASSIT_DeliveryConstraintSpecificBE.DeliveryConstraint.StartWeekday = strStartWeekDay;

        List<MASSIT_DeliveryConstraintSpecificBE> lstDeliveryConstraintSpecific = oAPPSIT_DeliveryConstraintSpecificBAL.GetDeliveryConstraintSpecificBAL(oMASSIT_DeliveryConstraintSpecificBE);

        if (lstDeliveryConstraintSpecific != null && lstDeliveryConstraintSpecific.Count > 0)
        {
            //Get Booked Volume
            APPBOK_BookingBE oAPPBOK_BookingBE = new APPBOK_BookingBE();
            APPBOK_BookingBAL oAPPBOK_BookingBAL = new APPBOK_BookingBAL();

            oAPPBOK_BookingBE.Action = "GetBookingVolumeDeatils";
            oAPPBOK_BookingBE.SiteId = Convert.ToInt32(intSiteId); //Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
            oAPPBOK_BookingBE.ScheduleDate = LogicalSchedulDateTime;

            string strWeekDay = LogicalSchedulDateTime.DayOfWeek.ToString().ToUpper();
            if (strWeekDay == "SUNDAY")
            {
                oAPPBOK_BookingBE.WeekDay = 1;
            }
            else if (strWeekDay == "MONDAY")
            {
                oAPPBOK_BookingBE.WeekDay = 2;
            }
            else if (strWeekDay == "TUESDAY")
            {
                oAPPBOK_BookingBE.WeekDay = 3;
            }
            else if (strWeekDay == "WEDNESDAY")
            {
                oAPPBOK_BookingBE.WeekDay = 4;
            }
            else if (strWeekDay == "THURSDAY")
            {
                oAPPBOK_BookingBE.WeekDay = 5;
            }
            else if (strWeekDay == "FRIDAY")
            {
                oAPPBOK_BookingBE.WeekDay = 6;
            }
            else if (strWeekDay == "SATURDAY")
            {
                oAPPBOK_BookingBE.WeekDay = 7;
            }
            oAPPBOK_BookingBE.StartTime = strStartTime.Split(':')[0] + ":00";

            List<APPBOK_BookingBE> lstBookedVolume = oAPPBOK_BookingBAL.GetBookingVolumeDeatilsBAL(oAPPBOK_BookingBE);

            if (lstBookedVolume != null && lstBookedVolume.Count > 0)
            {
                if (!((lstDeliveryConstraintSpecific[0].DeliveryConstraint.RestrictDelivery == 0) ||
                    (lstBookedVolume[0].BookingCount + 1 <= lstDeliveryConstraintSpecific[0].DeliveryConstraint.RestrictDelivery)))
                    return false;

                if (!((lstDeliveryConstraintSpecific[0].DeliveryConstraint.MaximumLine == 0) ||
                    (lstBookedVolume[0].NumberOfLines + Convert.ToInt32(strTotalLines) <= lstDeliveryConstraintSpecific[0].DeliveryConstraint.MaximumLine)))
                    return false;

                if (!((lstDeliveryConstraintSpecific[0].DeliveryConstraint.MaximumLift == 0) ||
                    (lstBookedVolume[0].NumberOfPallet + Convert.ToInt32(strTotalPallets) <= lstDeliveryConstraintSpecific[0].DeliveryConstraint.MaximumLift))) //Here Lift means pallets
                    return false;

                if (!((lstDeliveryConstraintSpecific[0].DeliveryConstraint.SingleDeliveryPalletRestriction == 0) ||
                    (Convert.ToInt32(strTotalPallets) <= lstDeliveryConstraintSpecific[0].DeliveryConstraint.SingleDeliveryPalletRestriction)))
                    return false;

                if (!((lstDeliveryConstraintSpecific[0].DeliveryConstraint.SingleDeliveryLineRestriction == 0) ||
                    (Convert.ToInt32(strTotalLines) <= lstDeliveryConstraintSpecific[0].DeliveryConstraint.SingleDeliveryLineRestriction)))
                    return false;
            }
            return true;
        }
        return true;
    }

    private int GetRequiredTimeSlotLength(int? TotalPallets, int? TotalCartons)
    {
        int itempCartons, itempPallets;
        itempCartons = iCartons;
        itempPallets = iPallets;

        //int? TotalPallets = txtPallets.Text.Trim() != string.Empty ? Convert.ToInt32(txtPallets.Text.Trim()) : (int?)null;
        //int? TotalCartons = txtCartons.Text.Trim() != string.Empty ? Convert.ToInt32(txtCartons.Text.Trim()) : (int?)null;

        iPallets = TotalPallets != (int?)null ? Convert.ToInt32(TotalPallets) : 0;
        iCartons = TotalCartons != (int?)null ? Convert.ToInt32(TotalCartons) : 0;

        int reqSlotLength = GetTimeSlotLength();

        iCartons = itempCartons;
        iPallets = itempPallets;

        return reqSlotLength;
    }

    private int GetTimeSlotLength()
    {
        return GetTimeSlotLength(-1);
    }

    private int GetTimeSlotLength(int VehicleTypeID)
    {

        //--------start Calculate Unload Time-------------------//
        decimal TotalCalculatedTime = 0;  //In Min
        bool isTimeCalculatedForPallets = false;
        bool isTimeCalculatedForCartons = false;

        //Step1 >Check Vendor Processing Window 
        if (isTimeCalculatedForPallets == false || isTimeCalculatedForCartons == false)
        {

            MASSIT_VendorProcessingWindowBE oMASSIT_VendorProcessingWindowBE = new MASSIT_VendorProcessingWindowBE();
            APPSIT_VendorProcessingWindowBAL oMASSIT_VendorProcessingWindowBAL = new APPSIT_VendorProcessingWindowBAL();

            oMASSIT_VendorProcessingWindowBE.Action = "ShowAllConsolidate";
            oMASSIT_VendorProcessingWindowBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
            oMASSIT_VendorProcessingWindowBE.User.UserID = Convert.ToInt32(Session["UserID"]);
            oMASSIT_VendorProcessingWindowBE.VendorID = Convert.ToInt32(ucSeacrhVendorID); // Convert.ToInt32(ucSeacrhVendor1.VendorNo);
            oMASSIT_VendorProcessingWindowBE.SiteID = Convert.ToInt32(intSiteId); //Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
            
            MASSIT_VendorProcessingWindowBE lstVendor = new MASSIT_VendorProcessingWindowBE();
            lstVendor = oMASSIT_VendorProcessingWindowBAL.GetVendorDetailsByIdBAL(oMASSIT_VendorProcessingWindowBE);

            if (lstVendor != null)
            {  //Vendor Processing Window Not found

                if (lstVendor.APP_PalletsUnloadedPerHour > 0 && iPallets != 0 && isTimeCalculatedForPallets == false)
                {
                    TotalCalculatedTime += Convert.ToDecimal(Convert.ToInt32(iPallets) * (60.0 / lstVendor.APP_PalletsUnloadedPerHour));
                    isTimeCalculatedForPallets = true;
                }
                if (lstVendor.APP_CartonsUnloadedPerHour > 0 && iCartons != 0 && isTimeCalculatedForCartons == false)
                {
                    TotalCalculatedTime += Convert.ToDecimal(Convert.ToInt32(iCartons) * (60.0 / lstVendor.APP_CartonsUnloadedPerHour));
                    isTimeCalculatedForCartons = true;
                }
            }
        }
        //---------------------------------// 

        //Step2 > Check Vehicle Type Setup
        if (isTimeCalculatedForPallets == false || isTimeCalculatedForCartons == false)
        {
            MASSIT_VehicleTypeBE oMASSIT_VehicleTypeBE = new MASSIT_VehicleTypeBE();
            APPSIT_VehicleTypeBAL oMASSIT_VehicleTypeBAL = new APPSIT_VehicleTypeBAL();

            oMASSIT_VehicleTypeBE.Action = "ShowById";
            oMASSIT_VehicleTypeBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
            oMASSIT_VehicleTypeBE.User.UserID = Convert.ToInt32(Session["UserID"]);

            if (VehicleTypeID <= 0)
                oMASSIT_VehicleTypeBE.VehicleTypeID = Convert.ToInt32(ddlVehicleType.SelectedItem.Value);
            else
                oMASSIT_VehicleTypeBE.VehicleTypeID = VehicleTypeID;

            MASSIT_VehicleTypeBE lstVehicleType = oMASSIT_VehicleTypeBAL.GetVehicleTypeDetailsByIdBAL(oMASSIT_VehicleTypeBE);
            if (lstVehicleType != null)
            {
                //Volume Capacity 
                if (lstVehicleType.APP_PalletsUnloadedPerHour > 0 && iPallets != 0 && isTimeCalculatedForPallets == false)
                {
                    TotalCalculatedTime += Convert.ToDecimal(Convert.ToInt32(iPallets) * (60.0 / lstVehicleType.APP_PalletsUnloadedPerHour));
                    isTimeCalculatedForPallets = true;
                }
                if (lstVehicleType.APP_CartonsUnloadedPerHour > 0 && iCartons != 0 && isTimeCalculatedForCartons == false)
                {
                    TotalCalculatedTime += Convert.ToDecimal(Convert.ToInt32(iCartons) * (60.0 / lstVehicleType.APP_CartonsUnloadedPerHour));
                    isTimeCalculatedForCartons = true;
                }

                if (isTimeCalculatedForPallets == false && isTimeCalculatedForCartons == false)
                { //Time Based 
                    if (lstVehicleType.TotalUnloadTimeInMinute > 0)
                    {
                        TotalCalculatedTime += Convert.ToDecimal(Convert.ToDecimal(lstVehicleType.TotalUnloadTimeInMinute));
                        isTimeCalculatedForPallets = true;
                        isTimeCalculatedForCartons = true;
                    }
                }
            }

        }
        //---------------------------------// 


        //Step3 >Check Site Miscellaneous Settings  -- skip this step
        //if (isTimeCalculatedForPallets == false || isTimeCalculatedForCartons == false) {

        //    MAS_SiteBE oMAS_SiteBE = new MAS_SiteBE();
        //    MAS_SiteBAL oMAS_SiteBAL = new MAS_SiteBAL();

        //    oMAS_SiteBE.Action = "ShowAll";
        //    oMAS_SiteBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
        //    oMAS_SiteBE.User.UserID = Convert.ToInt32(Session["UserID"]);
        //    oMAS_SiteBE.SiteID =  Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);

        //    List<MAS_SiteBE> lstSiteMisSettings = new List<MAS_SiteBE>();
        //    lstSiteMisSettings = oMAS_SiteBAL.GetSiteMisSettingBAL(oMAS_SiteBE);

        //    if (lstSiteMisSettings != null) {
        //        if (iPallets != 0 && isTimeCalculatedForPallets == false) {
        //            TotalCalculatedTime += Convert.ToDecimal(Convert.ToInt32(iPallets) * (60.0 / lstSiteMisSettings[0].AveragePalletUnloadedPerManHour));      // Convert.ToDecimal((Convert.ToInt32(iPallets) / lstSiteMisSettings[0].AveragePalletUnloadedPerManHour) * 60);
        //            isTimeCalculatedForPallets = true;
        //        }
        //        if (iCartons != 0 && isTimeCalculatedForCartons == false) {
        //            TotalCalculatedTime += Convert.ToDecimal(Convert.ToInt32(iCartons) * (60.0 / lstSiteMisSettings[0].AverageCartonUnloadedPerManHour));      //Convert.ToDecimal((Convert.ToInt32(iCartons) / lstSiteMisSettings[0].AverageCartonUnloadedPerManHour) * 60);
        //            isTimeCalculatedForCartons = true;
        //        }
        //    }
        //}
        //---------------------------------//   

        //Calculate Slot Length
        int intResult = (int)TotalCalculatedTime % iSlotTimeLength;
        int intr = (int)TotalCalculatedTime / iSlotTimeLength;
        if (intResult > 0)
            intr++;

        if (TotalCalculatedTime > 0 && TotalCalculatedTime < 1)
            intr = 1;

        if (TotalCalculatedTime == 0)
            intr++;
        //if (intr == 1)
        //    intr--;

        return intr;
    }

    private Literal SetCellColor(ref TableCell tclocal)
    {
        string VendorCarrierName = string.Empty;
        int SlotLength = 0;
        NextRequiredSlot = string.Empty;
        iNextSlotLength = 0;
        iNextSlotLength2 = 0;
        sVendorCarrierNameNextSlot = string.Empty;
        isCurrentSlotCovered = false;

        //Find Next Fixed Slot on the Same door No if seledted slot is a fixed slot
        List<APPBOK_BookingBE> lstNextFixedDoorLocal = lstAllFixedDoor.FindAll(delegate(APPBOK_BookingBE Bok) { return Bok.DoorNoSetup.SiteDoorNumberID == iSiteDoorID; });

        for (int LoopCount = 0; LoopCount < lstNextFixedDoorLocal.Count; LoopCount++)
        {
            if (lstNextFixedDoorLocal[LoopCount].SlotTime.SlotTimeID == iTimeSlotID && lstNextFixedDoorLocal[LoopCount].DoorNoSetup.SiteDoorNumberID == iSiteDoorID)
            {
                if (LoopCount < lstNextFixedDoorLocal.Count - 1)
                {
                    NextRequiredSlot = lstNextFixedDoorLocal[LoopCount + 1].SlotTime.SlotTime;
                    sVendorCarrierNameNextSlot = lstNextFixedDoorLocal[LoopCount + 1].Vendor.VendorName;
                    iNextSlotLength = 0; // GetRequiredTimeSlotLength(lstNextFixedDoorLocal[LoopCount + 1].NumberOfPallet, lstNextFixedDoorLocal[LoopCount + 1].NumberOfCartons);

                    //isCurrentSlotCovered = true;
                    //if (lstNextFixedDoorLocal[LoopCount + 1].Vendor.VendorName != ltvendorName.Text.Trim()) {
                    if (lstNextFixedDoorLocal[LoopCount + 1].Status != "Booked")
                    {
                        if (lstNextFixedDoorLocal[LoopCount + 1].SupplierType == "C")
                            iNextSlotLength2 = GetRequiredTimeSlotLengthForOthers(lstNextFixedDoorLocal[LoopCount + 1].NumberOfPallet, lstNextFixedDoorLocal[LoopCount + 1].NumberOfCartons, lstNextFixedDoorLocal[LoopCount + 1].SupplierType, lstNextFixedDoorLocal[LoopCount + 1].Carrier.CarrierID); // GetRequiredTimeSlotLength(lstNextFixedDoorLocal[LoopCount].NumberOfPallet, lstNextFixedDoorLocal[LoopCount].NumberOfCartons);
                        else if (lstNextFixedDoorLocal[LoopCount + 1].SupplierType == "V")
                            iNextSlotLength2 = GetRequiredTimeSlotLengthForOthers(lstNextFixedDoorLocal[LoopCount + 1].NumberOfPallet, lstNextFixedDoorLocal[LoopCount + 1].NumberOfCartons, lstNextFixedDoorLocal[LoopCount + 1].SupplierType, lstNextFixedDoorLocal[LoopCount + 1].VendorID);
                    }
                    else
                    {
                        iNextSlotLength2 = GetRequiredTimeSlotLength(lstNextFixedDoorLocal[LoopCount + 1].NumberOfPallet, lstNextFixedDoorLocal[LoopCount + 1].NumberOfCartons);
                    }
                    //isNextSlotCovered = true;   

                }
                break;
            }
        }
        //---------------------------------//

        //Find Next Slot on the Same door No if seledted slot is not a fixed slot
        if (NextRequiredSlot == string.Empty)
        {
            string[] StartTime = sTimeSlot.Split(':');
            TimeSpan time1 = new TimeSpan(Convert.ToInt32(StartTime[0]), Convert.ToInt32(StartTime[1]), 00);
            TimeSpan time2 = new TimeSpan(00, iSlotTimeLength, 00);
            int iLen = 0;
            for (iLen = 1; iLen <= actSlotLength; iLen++)
            {
                if (time1.Hours == 23 && time1.Minutes == 55)
                {
                    time1 = new TimeSpan(00, 00, 00);
                }
                else
                {
                    time1 = time1.Add(time2);
                }
                for (int LoopCount = 0; LoopCount < lstNextFixedDoorLocal.Count; LoopCount++)
                {
                    if (GetFormatedTime(lstNextFixedDoorLocal[LoopCount].SlotTime.SlotTime) == GetFormatedTime(time1.Hours.ToString() + ":" + time1.Minutes.ToString()) && lstNextFixedDoorLocal[LoopCount].DoorNoSetup.SiteDoorNumberID == iSiteDoorID)
                    {

                        //if (lstNextFixedDoorLocal[LoopCount].Vendor.VendorName != ltvendorName.Text.Trim())
                        if (lstNextFixedDoorLocal[LoopCount].Vendor.VendorName != lblVendorT.Text.Trim())
                        {
                            if (lstNextFixedDoorLocal[LoopCount].SupplierType == "C")
                                iNextSlotLength = GetRequiredTimeSlotLengthForOthers(lstNextFixedDoorLocal[LoopCount].NumberOfPallet, lstNextFixedDoorLocal[LoopCount].NumberOfCartons, lstNextFixedDoorLocal[LoopCount].SupplierType, lstNextFixedDoorLocal[LoopCount].Carrier.CarrierID); // GetRequiredTimeSlotLength(lstNextFixedDoorLocal[LoopCount].NumberOfPallet, lstNextFixedDoorLocal[LoopCount].NumberOfCartons);
                            else if (lstNextFixedDoorLocal[LoopCount].SupplierType == "V")
                                iNextSlotLength = GetRequiredTimeSlotLengthForOthers(lstNextFixedDoorLocal[LoopCount].NumberOfPallet, lstNextFixedDoorLocal[LoopCount].NumberOfCartons, lstNextFixedDoorLocal[LoopCount].SupplierType, lstNextFixedDoorLocal[LoopCount].VendorID);

                            NextRequiredSlot = lstNextFixedDoorLocal[LoopCount].SlotTime.SlotTime;
                            sVendorCarrierNameNextSlot = lstNextFixedDoorLocal[LoopCount].Vendor.VendorName;
                            break;
                        }
                    }
                }
                if (NextRequiredSlot != string.Empty)
                {
                    //iNextSlotLength = actSlotLength - iLen;  //(iLen + iNextSlotLength) - actSlotLength;  //
                    isNextSlotCovered = true;
                    if (actSlotLength - iLen < iNextSlotLength)
                    {
                        isNextSlotCovered = false;
                    }
                    break;
                }
            }
        }
        //---------------------------------//



        //Filter slot as per SlotTimeID and SiteDoorNumberID
        //List<APPBOK_BookingBE> lstAllFixedDoorLocal = lstAllFixedDoor.Where(Bok => Bok.SlotTime.SlotTimeID  == iTimeSlotID && Bok.DoorNoSetup.SiteDoorNumberID == iSiteDoorID).ToList();
        List<APPBOK_BookingBE> lstAllFixedDoorLocal = lstAllFixedDoor.FindAll(delegate(APPBOK_BookingBE Bok) { return Bok.SlotTime.SlotTimeID == iTimeSlotID && Bok.DoorNoSetup.SiteDoorNumberID == iSiteDoorID; });
        if (lstAllFixedDoorLocal.Count > 0)
        {

            if (lstAllFixedDoorLocal[0].VendorID == Convert.ToInt32(ucSeacrhVendorID) && lstAllFixedDoorLocal[0].SupplierType == "V")
            {
                sVendorCarrierName = lblVendorT.Text; //ltvendorName.Text;
                iCartons = lstAllFixedDoorLocal[0].NumberOfCartons != (int?)null ? Convert.ToInt32(lstAllFixedDoorLocal[0].NumberOfCartons) : 0;
                iLifts = lstAllFixedDoorLocal[0].NumberOfLift != (int?)null ? Convert.ToInt32(lstAllFixedDoorLocal[0].NumberOfLift) : 0;
                iLines = lstAllFixedDoorLocal[0].NumberOfLines != (int?)null ? Convert.ToInt32(lstAllFixedDoorLocal[0].NumberOfLines) : 0;
                iPallets = lstAllFixedDoorLocal[0].NumberOfPallet != (int?)null ? Convert.ToInt32(lstAllFixedDoorLocal[0].NumberOfPallet) : 0;
                int VahicalTypeID = lstAllFixedDoorLocal[0].VehicleType.VehicleTypeID;

                SetSlotLegendForVendor(ref tclocal, VahicalTypeID);
                tclocal.Attributes.Add("abbr", "V-" + ucSeacrhVendorID.ToString());

                VendorCarrierName = sVendorCarrierName;
            }
            else
            {
                //if (hdnCurrentRole.Value == "Vendor") {
                isClickable = true;
                sVendorCarrierName = lstAllFixedDoorLocal[0].Vendor.VendorName;
                //commented Just for run Fixed slot
                iCartons = lstAllFixedDoorLocal[0].NumberOfCartons != null ? Convert.ToInt32(lstAllFixedDoorLocal[0].NumberOfCartons) : 0;

                iPallets = lstAllFixedDoorLocal[0].NumberOfPallet != null ? Convert.ToInt32(lstAllFixedDoorLocal[0].NumberOfPallet) : 0;

                iLines = lstAllFixedDoorLocal[0].NumberOfLines != (int?)null ? Convert.ToInt32(lstAllFixedDoorLocal[0].NumberOfLines) : 0;

                iLifts = lstAllFixedDoorLocal[0].NumberOfLift != (int?)null ? Convert.ToInt32(lstAllFixedDoorLocal[0].NumberOfLift) : 0;


                if (lstAllFixedDoorLocal[0].SupplierType == "C")
                {
                    SlotLength = GetRequiredTimeSlotLengthForOthers(lstAllFixedDoorLocal[0].NumberOfPallet, lstAllFixedDoorLocal[0].NumberOfCartons, lstAllFixedDoorLocal[0].SupplierType, lstAllFixedDoorLocal[0].Carrier.CarrierID);
                    tclocal.Attributes.Add("abbr", "C-" + lstAllFixedDoorLocal[0].Carrier.CarrierID.ToString());
                }
                else if (lstAllFixedDoorLocal[0].SupplierType == "V")
                {
                    SlotLength = GetRequiredTimeSlotLengthForOthers(lstAllFixedDoorLocal[0].NumberOfPallet, lstAllFixedDoorLocal[0].NumberOfCartons, lstAllFixedDoorLocal[0].SupplierType, lstAllFixedDoorLocal[0].VendorID);
                    tclocal.Attributes.Add("abbr", "V-" + lstAllFixedDoorLocal[0].VendorID.ToString());
                }

                //SlotLength = GetTimeSlotLength();

                isCurrentSlotFixed = false;
                string tempVendorName = "";

                sCurrentFixedSlotID = lstAllFixedDoorLocal[0].FixedSlot.FixedSlotID.ToString();
                //tempVendorName = SetCallHeight(SlotLength, ref tclocal, Color.Black, lstAllFixedDoorLocal[0].SlotTime.SlotTime);
                //sCurrentFixedSlotID == hdnCurrentFixedSlotID.Value &&
                if (hdnSuggestedSlotTime.Value == sTimeSlot && hdnSuggestedSiteDoorNumber.Value == sDoorNo.ToString())
                {
                    sVendorCarrierName = lblVendorT.Text; //ltvendorName.Text;
                    tempVendorName = SetCallHeight(actSlotLength, ref tclocal, Color.GreenYellow, hdnSuggestedSlotTime.Value);
                    sVendorCarrierName = "";
                }
                else
                {
                    if (hdnCurrentRole.Value == "Vendor")
                    {
                        tempVendorName = SetCallHeight(SlotLength, ref tclocal, Color.Black, lstAllFixedDoorLocal[0].SlotTime.SlotTime);
                    }
                    else
                    {
                        if (lstAllFixedDoorLocal[0].SlotType == "FIXED SLOT" && lstAllFixedDoorLocal[0].Status != "Booked")
                        {
                            isCurrentSlotFixed = true;
                            tempVendorName = SetCallHeight(SlotLength, ref tclocal, Color.Red, lstAllFixedDoorLocal[0].SlotTime.SlotTime);

                            if (!tempVendorName.Contains(sVendorCarrierName))
                            {
                                if (tempVendorName != string.Empty)
                                {
                                    if (sVendorCarrierName != tempVendorName)
                                    {
                                        tempVendorName = sVendorCarrierName + " / " + tempVendorName;
                                    }
                                }
                                else
                                {
                                    if (sVendorCarrierName != tempVendorName)
                                    {
                                        tempVendorName = sVendorCarrierName;
                                    }
                                }
                            }

                        }
                        else if (lstAllFixedDoorLocal[0].Status == "Booked")
                        {
                            tempVendorName = SetCallHeight(SlotLength, ref tclocal, Color.Purple, lstAllFixedDoorLocal[0].SlotTime.SlotTime);

                            if (!tempVendorName.Contains(sVendorCarrierName))
                            {
                                if (tempVendorName != string.Empty)
                                {
                                    if (sVendorCarrierName != tempVendorName)
                                    {
                                        tempVendorName = sVendorCarrierName + " / " + tempVendorName;
                                    }
                                }
                                else
                                {
                                    if (sVendorCarrierName != tempVendorName)
                                    {
                                        tempVendorName = sVendorCarrierName;
                                    }
                                }
                            }
                        }
                    }
                }

                if (tempVendorName != string.Empty)
                {
                    VendorCarrierName = tempVendorName;
                }
                else
                {
                    //SetCellBacknForeColor(ref tclocal, Color.Black, false);
                    VendorCarrierName = lstAllFixedDoorLocal[0].Vendor.VendorName;
                    iTimeSlotID = lstAllFixedDoorLocal[0].SlotTime.SlotTimeID;
                    iSiteDoorID = lstAllFixedDoorLocal[0].DoorNoSetup.SiteDoorNumberID;

                    iCartons = lstAllFixedDoorLocal[0].NumberOfCartons != (int?)null ? Convert.ToInt32(lstAllFixedDoorLocal[0].NumberOfCartons) : 0;
                    iLifts = lstAllFixedDoorLocal[0].NumberOfLift != (int?)null ? Convert.ToInt32(lstAllFixedDoorLocal[0].NumberOfLift) : 0;
                    iLines = lstAllFixedDoorLocal[0].NumberOfLines != (int?)null ? Convert.ToInt32(lstAllFixedDoorLocal[0].NumberOfLines) : 0;
                    iPallets = lstAllFixedDoorLocal[0].NumberOfPallet != (int?)null ? Convert.ToInt32(lstAllFixedDoorLocal[0].NumberOfPallet) : 0;

                    //CreateDynamicDiv(ref tclocal, true);
                }
                //}
                //else {
                //    isCurrentSlotFixed = false;
                //    if (DoorIDs.Contains("," + iTimeSlotID + "@" + iSiteDoorID)) {  //Door Available         
                //        //SetCellBacknForeColor(ref tclocal, Color.White, false);                
                //        MakeCellClickable(ref tclocal);
                //        VendorCarrierName = SetCallHeight(1, ref tclocal, Color.White, sTimeSlot);
                //    }
                //    else {                                                  // Door Not Available
                //        isCurrentSlotFixed = false;
                //        SetCellBacknForeColor(ref tclocal, Color.Black);
                //        VendorCarrierName = "";
                //    }
                //}

                if (hdnCurrentRole.Value == "Vendor")
                {
                    VendorCarrierName = "";
                }

                if (hdnCurrentRole.Value != "Vendor")
                {
                    MakeCellClickableForOtherVendor(ref tclocal);
                }

            }
        }
        else
        {
            isCurrentSlotFixed = false;

            bool isLoopProcessed = false;

            if (DoorIDs.Contains("," + iTimeSlotID + "@" + iSiteDoorID))
            {  //Door Available         
                //SetCellBacknForeColor(ref tclocal, Color.White, false);  
                isLoopProcessed = true;

                if (hdnSuggestedSiteDoorNumberID.Value == string.Empty)
                {
                    hdnSuggestedSiteDoorNumberID.Value = "-1";
                    hdnSuggestedSlotTimeID.Value = "-1";
                }

                if (iSiteDoorID == Convert.ToInt32(hdnSuggestedSiteDoorNumberID.Value) && iTimeSlotID == Convert.ToInt32(hdnSuggestedSlotTimeID.Value))
                {

                    int? TotalPallets = txtPalletsT.Text.Trim() != string.Empty ? Convert.ToInt32(txtPalletsT.Text.Trim()) : (int?)null;
                    int? TotalCartons = txtCartonsT.Text.Trim() != string.Empty ? Convert.ToInt32(txtCartonsT.Text.Trim()) : (int?)null;
                    int? TotalLifts = txtLiftsT.Text.Trim() != string.Empty ? Convert.ToInt32(txtLiftsT.Text.Trim()) : (int?)null;
                    int? TotalLines = txtLinesT.Text.Trim() != string.Empty ? Convert.ToInt32(txtLinesT.Text.Trim()) : (int?)null;
                    //int? TotalLines = txtExpectedLines.Text.Trim() != string.Empty ? Convert.ToInt32(txtExpectedLines.Text.Trim()) : (int?)null;

                    iPallets = TotalPallets != (int?)null ? Convert.ToInt32(TotalPallets) : 0;
                    iCartons = TotalCartons != (int?)null ? Convert.ToInt32(TotalCartons) : 0;
                    iLines = TotalLines != (int?)null ? Convert.ToInt32(TotalLines) : 0;
                    iLifts = TotalLifts != (int?)null ? Convert.ToInt32(TotalLifts) : 0;

                    sVendorCarrierName = lblVendorT.Text; //ltvendorName.Text;

                    SetCallHeight(actSlotLength, ref tclocal, Color.GreenYellow, sTimeSlot);
                    //if (hdnGraySlotClicked.Value == "true")
                    //    SetCallHeight(actSlotLength, ref tclocal, Color.Orange, sTimeSlot);
                    //else
                    //    SetCallHeight(actSlotLength, ref tclocal, Color.GreenYellow, sTimeSlot);

                    VendorCarrierName = lblVendorT.Text; //ltvendorName.Text;

                    tclocal.Attributes.Add("abbr", "V-" + ucSeacrhVendorID.ToString());
                }
                else
                {
                    MakeCellClickable(ref tclocal);
                    VendorCarrierName = SetCallHeight(1, ref tclocal, Color.White, sTimeSlot);
                    if (hdnCurrentRole.Value == "Vendor")
                    {
                        VendorCarrierName = "";
                    }
                }
            }
            else if (!DoorIDs.Contains("," + iTimeSlotID + "@" + iSiteDoorID) && hdnCurrentRole.Value != "Vendor")
            {  //Door Not Available but User is internal user



                MakeCellClickable(ref tclocal);

                if (hdnSuggestedSiteDoorNumberID.Value == string.Empty)
                {
                    hdnSuggestedSiteDoorNumberID.Value = "-1";
                    hdnSuggestedSlotTimeID.Value = "-1";
                }

                if (iSiteDoorID == Convert.ToInt32(hdnSuggestedSiteDoorNumberID.Value) && iTimeSlotID == Convert.ToInt32(hdnSuggestedSlotTimeID.Value))
                {

                    isLoopProcessed = true;
                    int? TotalPallets = txtPalletsT.Text.Trim() != string.Empty ? Convert.ToInt32(txtPalletsT.Text.Trim()) : (int?)null;
                    int? TotalCartons = txtCartonsT.Text.Trim() != string.Empty ? Convert.ToInt32(txtCartonsT.Text.Trim()) : (int?)null;
                    int? TotalLifts = txtLiftsT.Text.Trim() != string.Empty ? Convert.ToInt32(txtLiftsT.Text.Trim()) : (int?)null;
                    int? TotalLines = txtLinesT.Text.Trim() != string.Empty ? Convert.ToInt32(txtLinesT.Text.Trim()) : (int?)null;
                    //int? TotalLines = txtExpectedLines.Text.Trim() != string.Empty ? Convert.ToInt32(txtExpectedLines.Text.Trim()) : (int?)null;

                    iPallets = TotalPallets != (int?)null ? Convert.ToInt32(TotalPallets) : 0;
                    iCartons = TotalCartons != (int?)null ? Convert.ToInt32(TotalCartons) : 0;
                    iLines = TotalLines != (int?)null ? Convert.ToInt32(TotalLines) : 0;
                    iLifts = TotalLifts != (int?)null ? Convert.ToInt32(TotalLifts) : 0;

                    sVendorCarrierName = lblVendorT.Text; //ltvendorName.Text;

                    SetCallHeight(actSlotLength, ref tclocal, Color.GreenYellow, sTimeSlot);
                    //if (hdnGraySlotClicked.Value == "true")
                    //    SetCallHeight(actSlotLength, ref tclocal, Color.Orange, sTimeSlot);
                    //else
                    //    SetCallHeight(actSlotLength, ref tclocal, Color.GreenYellow, sTimeSlot);

                    VendorCarrierName = lblVendorT.Text; //ltvendorName.Text;

                    tclocal.Attributes.Add("abbr", "V-" + ucSeacrhVendorID.ToString());
                }

            }

            if (isLoopProcessed == false)
            { // Door Not Available and external user
                isCurrentSlotFixed = false;

                VendorDetails vd = new VendorDetails();

                string[] StartTime = sTimeSlot.Split(':');
                TimeSpan time1 = new TimeSpan(Convert.ToInt32(StartTime[0]), Convert.ToInt32(StartTime[1]), 00);

                if (SlotDoorCounter.ContainsKey(time1 + iSiteDoorID.ToString()))
                {
                    vd = SlotDoorCounter[time1 + iSiteDoorID.ToString()];
                    if (vd.CellColor == Color.GreenYellow)
                    {
                        SetCellBacknForeColor(ref tclocal, Color.Orange);
                        VendorCarrierName = vd.VendorName;
                    }
                    else
                    {
                        SetCellBacknForeColor(ref tclocal, Color.Black);
                        if (hdnCurrentRole.Value != "Vendor")
                            MakeCellClickable(ref tclocal);
                        VendorCarrierName = "";
                    }
                }
                else
                {
                    SetCellBacknForeColor(ref tclocal, Color.Black);
                    if (hdnCurrentRole.Value != "Vendor")
                        MakeCellClickable(ref tclocal);
                    VendorCarrierName = "";
                }
            }
        }


        //for (int LoopCount = 0; LoopCount < lstAllFixedDoor.Count; LoopCount++) {
        //    if (lstAllFixedDoor[LoopCount].SlotTime.SlotTimeID == jCount && lstAllFixedDoor[LoopCount].DoorNoSetup.SiteDoorNumberID == iCount) {

        //        if (hdnCurrentRole.Value == "Vendor" || lstAllFixedDoor[LoopCount].VendorID == Convert.ToInt32(ucSeacrhVendor1.VendorNo)) {
        //            SetSlotLegendForVendor(dCount, ref tclocal, jCount, iCount, lstAllFixedDoor[LoopCount].NumberOfCartons, lstAllFixedDoor[LoopCount].NumberOfLift, lstAllFixedDoor[LoopCount].NumberOfLines, lstAllFixedDoor[LoopCount].NumberOfPallet);
        //            VendorCarrierName = ltvendorName.Text;
        //        }
        //        else {
        //            SetCellBacknForeColor(ref tclocal, Color.Black, false);
        //            //CreateDynamicDiv(ref tclocal, lstAllFixedDoor[LoopCount].SlotTime.SlotTimeID, lstAllFixedDoor[LoopCount].DoorNoSetup.SiteDoorNumberID, lstAllFixedDoor[LoopCount].NumberOfCartons, lstAllFixedDoor[LoopCount].NumberOfLift, lstAllFixedDoor[LoopCount].NumberOfLines, lstAllFixedDoor[LoopCount].NumberOfPallet);
        //            SlotLength = GetTimeSlotLength(lstAllFixedDoor[LoopCount].NumberOfPallet, lstAllFixedDoor[LoopCount].NumberOfCartons);

        //            VendorCarrierName = lstAllFixedDoor[LoopCount].Vendor.VendorName;
        //        }
        //        break;
        //    }
        //}

        Literal lt = new Literal();
        lt.Text = VendorCarrierName;
        return lt;
    }

    private int GetRequiredTimeSlotLengthForOthers(int? TotalPallets, int? TotalCartons, string SupplierType, int ID)
    {
        //--------start Calculate Unload Time-------------------//
        decimal TotalCalculatedTime = 0;  //In Min
        bool isTimeCalculatedForPallets = false;
        bool isTimeCalculatedForCartons = false;

        TotalPallets = TotalPallets == null ? 0 : TotalPallets;
        TotalCartons = TotalCartons == null ? 0 : TotalCartons;

        if (SupplierType == "V")
        {
            //Step1 >Check Vendor Processing Window 
            if (isTimeCalculatedForPallets == false || isTimeCalculatedForCartons == false)
            {

                MASSIT_VendorProcessingWindowBE oMASSIT_VendorProcessingWindowBE = new MASSIT_VendorProcessingWindowBE();
                APPSIT_VendorProcessingWindowBAL oMASSIT_VendorProcessingWindowBAL = new APPSIT_VendorProcessingWindowBAL();

                oMASSIT_VendorProcessingWindowBE.Action = "ShowAllConsolidate";
                oMASSIT_VendorProcessingWindowBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
                oMASSIT_VendorProcessingWindowBE.User.UserID = Convert.ToInt32(Session["UserID"]);
                oMASSIT_VendorProcessingWindowBE.VendorID = ID;
                oMASSIT_VendorProcessingWindowBE.SiteID = Convert.ToInt32(intSiteId); //Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);


                MASSIT_VendorProcessingWindowBE lstVendor = new MASSIT_VendorProcessingWindowBE();
                lstVendor = oMASSIT_VendorProcessingWindowBAL.GetVendorDetailsByIdBAL(oMASSIT_VendorProcessingWindowBE);

                if (lstVendor != null)
                {  //Vendor Processing Window Not found

                    if (lstVendor.APP_PalletsUnloadedPerHour > 0 && TotalPallets != 0 && isTimeCalculatedForPallets == false)
                    {
                        TotalCalculatedTime += Convert.ToDecimal(Convert.ToInt32(TotalPallets) * (60.0 / lstVendor.APP_PalletsUnloadedPerHour));
                        isTimeCalculatedForPallets = true;
                    }
                    if (lstVendor.APP_CartonsUnloadedPerHour > 0 && TotalCartons != 0 && isTimeCalculatedForCartons == false)
                    {
                        TotalCalculatedTime += Convert.ToDecimal(Convert.ToInt32(TotalCartons) * (60.0 / lstVendor.APP_CartonsUnloadedPerHour));
                        isTimeCalculatedForCartons = true;
                    }
                }
            }
            //---------------------------------// 
        }

        if (SupplierType == "C")
        {
            //Step1 >Check Carrier Processing Window 
            if (isTimeCalculatedForPallets == false || isTimeCalculatedForCartons == false)
            {

                MASSIT_CarrierProcessingWindowBE oMASSIT_CarrerProcessingWindowBE = new MASSIT_CarrierProcessingWindowBE();
                APPSIT_CarrierProcessingWindowBAL oMASSIT_CarrerProcessingWindowBAL = new APPSIT_CarrierProcessingWindowBAL();

                oMASSIT_CarrerProcessingWindowBE.Action = "GetConstraints";
                oMASSIT_CarrerProcessingWindowBE.CarrierID = ID;
                oMASSIT_CarrerProcessingWindowBE.SiteID = Convert.ToInt32(intSiteId); //Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
                MASSIT_CarrierProcessingWindowBE lstCarrier = oMASSIT_CarrerProcessingWindowBAL.GetConstraintsBAL(oMASSIT_CarrerProcessingWindowBE);


                if (lstCarrier != null)
                {  //Carrier Processing Window Not found

                    if (lstCarrier.APP_PalletsUnloadedPerHour > 0 && TotalPallets != 0 && isTimeCalculatedForPallets == false)
                    {
                        TotalCalculatedTime += Convert.ToDecimal(Convert.ToInt32(TotalPallets) * (60.0 / lstCarrier.APP_PalletsUnloadedPerHour));
                        isTimeCalculatedForPallets = true;
                    }
                    if (lstCarrier.APP_CartonsUnloadedPerHour > 0 && TotalCartons != 0 && isTimeCalculatedForCartons == false)
                    {
                        TotalCalculatedTime += Convert.ToDecimal(Convert.ToInt32(TotalCartons) * (60.0 / lstCarrier.APP_CartonsUnloadedPerHour));
                        isTimeCalculatedForCartons = true;
                    }
                }
            }
            //---------------------------------// 
        }


        //Step2 > Check Vehicle Type Setup  -- Skip this step
        //if (isTimeCalculatedForPallets == false || isTimeCalculatedForCartons == false) {
        //    MASSIT_VehicleTypeBE oMASSIT_VehicleTypeBE = new MASSIT_VehicleTypeBE();
        //    APPSIT_VehicleTypeBAL oMASSIT_VehicleTypeBAL = new APPSIT_VehicleTypeBAL();

        //    oMASSIT_VehicleTypeBE.Action = "ShowById";
        //    oMASSIT_VehicleTypeBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
        //    oMASSIT_VehicleTypeBE.User.UserID = Convert.ToInt32(Session["UserID"]);
        //    oMASSIT_VehicleTypeBE.VehicleTypeID = Convert.ToInt32(ddlVehicleType.SelectedItem.Value);
        //    MASSIT_VehicleTypeBE lstVehicleType = oMASSIT_VehicleTypeBAL.GetVehicleTypeDetailsByIdBAL(oMASSIT_VehicleTypeBE);
        //    if (lstVehicleType != null) {
        //        if (lstVehicleType.APP_PalletsUnloadedPerHour > 0 &&
        //                lstVehicleType.APP_CartonsUnloadedPerHour > 0) { //Volume Capacity 
        //            if (TotalPallets != 0 && isTimeCalculatedForPallets == false) {
        //                TotalCalculatedTime += Convert.ToDecimal(Convert.ToInt32(TotalPallets) * (60.0 / lstVehicleType.APP_PalletsUnloadedPerHour));
        //                isTimeCalculatedForPallets = true;
        //            }
        //            if (TotalCartons != 0 && isTimeCalculatedForCartons == false) {
        //                TotalCalculatedTime += Convert.ToDecimal(Convert.ToInt32(TotalCartons) * (60.0 / lstVehicleType.APP_CartonsUnloadedPerHour));
        //                isTimeCalculatedForCartons = true;
        //            }
        //        }
        //        else { //Time Based 
        //            TotalCalculatedTime += Convert.ToDecimal(Convert.ToDecimal(lstVehicleType.TotalUnloadTimeInMinute));
        //            isTimeCalculatedForPallets = true;
        //            isTimeCalculatedForCartons = true;
        //        }
        //    }
        //}
        //---------------------------------// 


        //Step3 >Check Site Miscellaneous Settings
        if (isTimeCalculatedForPallets == false || isTimeCalculatedForCartons == false)
        {

            MAS_SiteBE oMAS_SiteBE = new MAS_SiteBE();
            MAS_SiteBAL oMAS_SiteBAL = new MAS_SiteBAL();

            if (hdnCurrentRole.Value == "Vendor")
                oMAS_SiteBE.Action = "ShowAllForVendor";
            else
                oMAS_SiteBE.Action = "ShowAll";

            oMAS_SiteBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
            oMAS_SiteBE.User.UserID = Convert.ToInt32(Session["UserID"]);
            oMAS_SiteBE.SiteID = Convert.ToInt32(intSiteId); //Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);

            List<MAS_SiteBE> lstSiteMisSettings = new List<MAS_SiteBE>();
            lstSiteMisSettings = oMAS_SiteBAL.GetSiteMisSettingBAL(oMAS_SiteBE);

            if (lstSiteMisSettings != null && lstSiteMisSettings.Count > 0)
            {
                if (lstSiteMisSettings[0].AveragePalletUnloadedPerManHour > 0 && TotalPallets != 0 && isTimeCalculatedForPallets == false)
                {
                    TotalCalculatedTime += Convert.ToDecimal(Convert.ToInt32(TotalPallets) * (60.0 / lstSiteMisSettings[0].AveragePalletUnloadedPerManHour));      // Convert.ToDecimal((Convert.ToInt32(TotalPallets) / lstSiteMisSettings[0].AveragePalletUnloadedPerManHour) * 60);
                    isTimeCalculatedForPallets = true;
                }
                if (lstSiteMisSettings[0].AverageCartonUnloadedPerManHour > 0 && TotalCartons != 0 && isTimeCalculatedForCartons == false)
                {
                    TotalCalculatedTime += Convert.ToDecimal(Convert.ToInt32(TotalCartons) * (60.0 / lstSiteMisSettings[0].AverageCartonUnloadedPerManHour));      //Convert.ToDecimal((Convert.ToInt32(TotalCartons) / lstSiteMisSettings[0].AverageCartonUnloadedPerManHour) * 60);
                    isTimeCalculatedForCartons = true;
                }
            }
        }
        //---------------------------------//   

        //Calculate Slot Length
        int intResult = (int)TotalCalculatedTime % iSlotTimeLength;
        int intr = (int)TotalCalculatedTime / iSlotTimeLength;
        if (intResult > 0)
            intr++;

        if (TotalCalculatedTime > 0 && TotalCalculatedTime < 1)
            intr = 1;

        if (TotalCalculatedTime == 0)
            intr++;

        //if (intr == 1)
        //    intr--;

        return intr;
    }

    private void MakeCellClickableForOtherVendor(ref TableCell tclocal)
    {
        string fixedSlotid = "-1";
        tclocal.Attributes.Add("onclick", "CheckAletrnateSlot('" + iTimeSlotID.ToString() + "','" + iSiteDoorID.ToString() + "','" + sDoorNo + "','" + sTimeSlot + "','" + fixedSlotid + "','" + sSlotStartDay + "','" + iTimeSlotOrderBYID + "');");
    }

    private void MakeCellClickable(ref TableCell tclocal)
    {
        tclocal.Attributes.Add("onmouseover", "this.style.cursor = 'hand'");
        tclocal.Attributes.Add("onmouseout", "this.style.cursor = 'default'");

        string fixedSlotid = "-1";
        VendorDetails vd = new VendorDetails();

        string[] StartTime = sTimeSlot.Split(':');
        TimeSpan time1 = new TimeSpan(Convert.ToInt32(StartTime[0]), Convert.ToInt32(StartTime[1]), 00);

        if (SlotDoorCounter.ContainsKey(time1 + iSiteDoorID.ToString()))
        {
            vd = SlotDoorCounter[time1 + iSiteDoorID.ToString()];
            if (vd.CurrentFixedSlotID != null)
            {
                fixedSlotid = vd.CurrentFixedSlotID.ToString();
            }
        }

        tclocal.Attributes.Add("onclick", "CheckAletrnateSlot('" + iTimeSlotID.ToString() + "','" + iSiteDoorID.ToString() + "','" + sDoorNo + "','" + sTimeSlot + "','" + fixedSlotid + "','" + sSlotStartDay + "','" + iTimeSlotOrderBYID + "');");
    }

    private void RemoveCellClickable(ref TableCellCollection tcClocal)
    {
        TableCell tclocal = tcClocal[0];
        tclocal.Attributes.Add("onmouseover", "this.style.cursor = 'default'");
        tclocal.Attributes.Add("onmouseout", "this.style.cursor = 'default'");
        tclocal.Attributes.Add("onclick", "");
    }

    private void SetCellBacknForeColor(ref TableCell tclocal, Color CellBackColor)
    {
        string temp = string.Empty;

        if (CellBackColor == Color.Red)
        {
            tclocal.BackColor = System.Drawing.Color.White;
            tclocal.ForeColor = System.Drawing.Color.Black;
            tclocal.BorderColor = System.Drawing.Color.Red;
            tclocal.BorderWidth = 2;
            return;
        }
        else if (CellBackColor == Color.Purple)
        {
            tclocal.BackColor = System.Drawing.Color.Purple;
            tclocal.ForeColor = System.Drawing.Color.White;
            if (isCurrentSlotFixed)
            {
                tclocal.BorderColor = System.Drawing.Color.Red;
                tclocal.BorderWidth = 2;
            }
            else
            {
                tclocal.BorderColor = System.Drawing.Color.Black;
                tclocal.BorderStyle = BorderStyle.Dashed;
                tclocal.BorderWidth = 2;
            }

            return;
        }
        else if (CellBackColor == Color.Orange)
        {
            tclocal.BackColor = System.Drawing.Color.Orange;
            tclocal.ForeColor = System.Drawing.Color.Black;
            if (isCurrentSlotFixed)
            {
                tclocal.BorderColor = System.Drawing.Color.Red;
                tclocal.BorderWidth = 2;
            }
            return;
        }
        else if (CellBackColor == Color.GreenYellow)
        {
            tclocal.BackColor = System.Drawing.Color.GreenYellow;
            tclocal.ForeColor = System.Drawing.Color.Black;
            if (isCurrentSlotFixed)
            {
                tclocal.BorderColor = System.Drawing.Color.Red;
                tclocal.BorderWidth = 2;
            }
            return;
        }
        else if (CellBackColor == Color.Black)
        {
            tclocal.BackColor = System.Drawing.Color.Black;
            tclocal.ForeColor = System.Drawing.Color.White;
            tclocal.BorderColor = System.Drawing.Color.White;
            return;
        }
        else if (CellBackColor == Color.White)
        {
            tclocal.BackColor = System.Drawing.Color.White;
            tclocal.ForeColor = System.Drawing.Color.Black;
            tclocal.BorderColor = System.Drawing.Color.Black;
            tclocal.BorderWidth = 1;
            return;
        }
        else if (CellBackColor == Color.LightGray)
        {
            tclocal.BackColor = System.Drawing.Color.LightGray;
            tclocal.ForeColor = System.Drawing.Color.Black;
            tclocal.BorderColor = System.Drawing.Color.Black;
            if (isCurrentSlotFixed)
            {
                tclocal.BorderColor = System.Drawing.Color.Red;
                tclocal.BorderWidth = 2;
            }
            return;
        }
    }

    private string SetCallHeight(int SlotLength, ref TableCell tclocal, Color CellBackColor, string SlotStartTime)
    {

        string VendorName = string.Empty;
        string temp = string.Empty;

        if (iTimeSlotID == 41)
            temp = string.Empty;

        if (SlotLength >= 1)
        {
            string[] StartTime = SlotStartTime.Split(':');
            TimeSpan time1 = new TimeSpan(Convert.ToInt32(StartTime[0]), Convert.ToInt32(StartTime[1]), 00);
            TimeSpan time2 = new TimeSpan(00, iSlotTimeLength, 00);
            int iLen = 0;


            for (iLen = 1; iLen <= SlotLength; iLen++)
            {
                //if (GetFormatedTime(time1.Hours.ToString() + ":" + time1.Minutes.ToString()) != NextRequiredSlot)
                //    time1 = time1.Add(time2);
                //else if (GetFormatedTime(time1.Hours.ToString() + ":" + time1.Minutes.ToString()) == NextRequiredSlot) {
                //    SetCellBacknForeColor(ref tclocal, Color.Orange, isCurrentSlotFixed);
                //    break;
                //}
                VendorDetails vd = new VendorDetails();
                if (iLen == 1)
                {

                    if (SlotDoorCounter.ContainsKey(time1.ToString() + iSiteDoorID.ToString()))
                    {
                        vd = SlotDoorCounter[time1.ToString() + iSiteDoorID.ToString()];
                        if (vd.VendorName.Trim() != lblVendorT.Text) //ltvendorName.Text)
                        {
                            //if (isCurrentSlotFixed == false) {
                            VendorName = vd.VendorName;
                            isCurrentSlotFixed = vd.isCurrentSlotFixed;
                            SetCellBacknForeColor(ref tclocal, vd.CellColor);
                            //}
                            //else {
                            //    VendorName = ltvendorName.Text;
                            //    SetCellBacknForeColor(ref tclocal, CellBackColor);
                            //}
                        }
                        else
                        {
                            if (isCurrentSlotFixed == false)
                            {
                                VendorName = vd.VendorName;
                                isCurrentSlotFixed = vd.isCurrentSlotFixed;
                                SetCellBacknForeColor(ref tclocal, vd.CellColor);
                            }
                            else
                            {
                                if (vd.CellColor == Color.GreenYellow)
                                {
                                    VendorName = lblVendorT.Text; //ltvendorName.Text;
                                    SetCellBacknForeColor(ref tclocal, CellBackColor);
                                }
                                else
                                {
                                    VendorName = vd.VendorName;
                                    isCurrentSlotFixed = vd.isCurrentSlotFixed;
                                    SetCellBacknForeColor(ref tclocal, vd.CellColor);
                                }
                            }
                        }
                    }
                    else
                    {

                        vd.VendorName = sVendorCarrierName;
                        vd.CellColor = CellBackColor;
                        vd.isCurrentSlotFixed = isCurrentSlotFixed;


                        SlotDoorCounter.Add(time1.ToString() + iSiteDoorID.ToString(), vd);
                        SetCellBacknForeColor(ref tclocal, CellBackColor);
                    }

                    if (CellBackColor != Color.White)
                    {
                        CreateDynamicDiv(ref tclocal, SlotLength, SlotStartTime);
                    }

                }
                else
                {
                    if (time1.Hours == 23 && time1.Minutes == 55)
                    {
                        time1 = new TimeSpan(00, 00, 00);
                    }
                    else
                    {
                        time1 = time1.Add(time2);
                    }
                    //if (SlotDoorCounter.ContainsKey(time1.ToString() + iSiteDoorID.ToString())) {
                    //    vd = SlotDoorCounter[time1.ToString() + iSiteDoorID.ToString()];
                    //    if (vd.VendorName.Trim() != ltvendorName.Text) {
                    //        if (isCurrentSlotFixed == false) {
                    //            VendorName = vd.VendorName;
                    //            isCurrentSlotFixed = vd.isCurrentSlotFixed;
                    //            //SetCellBacknForeColor(ref tclocal, vd.CellColor);
                    //        }
                    //        else {
                    //            VendorName = ltvendorName.Text;
                    //            //SetCellBacknForeColor(ref tclocal, CellBackColor);
                    //        }
                    //    }
                    //    else {
                    //        if (isCurrentSlotFixed == false) {
                    //            VendorName = vd.VendorName;
                    //            isCurrentSlotFixed = vd.isCurrentSlotFixed;
                    //            //SetCellBacknForeColor(ref tclocal, vd.CellColor);
                    //        }
                    //        else {
                    //            VendorName = ltvendorName.Text;
                    //            //SetCellBacknForeColor(ref tclocal, CellBackColor);
                    //        }
                    //    }
                    //}
                    //else {                           
                    //    vd.VendorName = sVendorCarrierName;
                    //    vd.CellColor = CellBackColor;
                    //    vd.isCurrentSlotFixed = isCurrentSlotFixed;
                    //    SlotDoorCounter.Add(time1.ToString() + iSiteDoorID.ToString(), vd);
                    //}

                    if (!SlotDoorCounter.ContainsKey(time1.ToString() + iSiteDoorID.ToString()))
                    {
                        vd.VendorName = string.Empty; // sVendorCarrierName;

                        if (hdnCurrentRole.Value == "Vendor" && CellBackColor != Color.GreenYellow)
                        {
                            vd.CellColor = Color.Black;
                        }
                        else
                        {
                            vd.CellColor = CellBackColor;
                        }

                        vd.isCurrentSlotFixed = isCurrentSlotFixed;
                        vd.CurrentFixedSlotID = sCurrentFixedSlotID;



                        SlotDoorCounter.Add(time1.ToString() + iSiteDoorID.ToString(), vd);

                    }
                }
            }
        }

        if (SlotLength >= 1 && NextRequiredSlot != string.Empty)
        {
            string[] StartTime = SlotStartTime.Split(':');
            TimeSpan time1 = new TimeSpan(Convert.ToInt32(StartTime[0]), Convert.ToInt32(StartTime[1]), 00);
            TimeSpan time2 = new TimeSpan(00, iSlotTimeLength, 00);


            int iLen = 0;
            //VendorDetails vd = new VendorDetails();
            for (iLen = 1; iLen <= SlotLength; iLen++)
            {
                if (GetFormatedTime(time1.Hours.ToString() + ":" + time1.Minutes.ToString()) != GetFormatedTime(NextRequiredSlot))
                {
                    if (time1.Hours == 23 && time1.Minutes == 55)
                    {
                        time1 = new TimeSpan(00, 00, 00);
                    }
                    else
                    {
                        time1 = time1.Add(time2);
                    }
                }
                else if (GetFormatedTime(time1.Hours.ToString() + ":" + time1.Minutes.ToString()) == GetFormatedTime(NextRequiredSlot))
                {
                    ////SetCellBacknForeColor(ref tclocal, Color.Orange);
                    //if (SlotDoorCounter.ContainsKey(time1.ToString() + iSiteDoorID.ToString())) {
                    //    SlotDoorCounter.Remove(time1.ToString() + iSiteDoorID.ToString());
                    //}
                    //vd.VendorName = sVendorCarrierNameNextSlot + " / " + sVendorCarrierName; // +" / " + ltvendorName.Text;
                    //vd.CellColor = Color.Orange;
                    //vd.isCurrentSlotFixed = isCurrentSlotFixed;
                    //SlotDoorCounter.Add(time1.ToString() + iSiteDoorID.ToString(), vd);


                    SetCallHeightOverSpill(SlotLength - iLen + 1, Color.Orange, time1.Subtract(time2).ToString());


                    break;
                }
            }
            NextRequiredSlot = string.Empty;
        }

        sCurrentFixedSlotID = "-1";
        return VendorName;
    }

    private void SetCallHeightOverSpill()
    {
        if (iNextSlotLength >= 1)
        {
            string[] StartTime = NextRequiredSlot.Split(':');
            TimeSpan time1 = new TimeSpan(Convert.ToInt32(StartTime[0]), Convert.ToInt32(StartTime[1]), 00);
            TimeSpan time2 = new TimeSpan(00, iSlotTimeLength, 00);
            time1 = time1.Subtract(time2);
            int iLen = 0;

            for (iLen = 1; iLen <= iNextSlotLength; iLen++)
            {

                VendorDetails vd = new VendorDetails();

                if (time1.Hours == 23 && time1.Minutes == 55)
                {
                    time1 = new TimeSpan(00, 00, 00);
                }
                else
                {
                    time1 = time1.Add(time2);
                }
                //vd.VendorName = sVendorCarrierNameNextSlot + " / " + sVendorCarrierName;

                if (iLen <= iNextSlotLength2)
                    vd.VendorName = sVendorCarrierNameNextSlot + " / " + sVendorCarrierName;
                else
                    vd.VendorName = sVendorCarrierName;



                if (hdnCurrentRole.Value == "Vendor")
                {
                    vd.CellColor = Color.Black;
                }
                else
                {
                    vd.CellColor = Color.Orange;
                }
                vd.isCurrentSlotFixed = isCurrentSlotFixed;

                if (SlotDoorCounter.ContainsKey(time1.ToString() + iSiteDoorID.ToString()))
                {
                    SlotDoorCounter.Remove(time1.ToString() + iSiteDoorID.ToString());
                }


                SlotDoorCounter.Add(time1.ToString() + iSiteDoorID.ToString(), vd);
            }
        }
    }

    private void SetCallHeightOverSpill(int SlotLength, Color CellBackColor, string SlotStartTime)
    {
        if (SlotLength >= 1)
        {

            int LoopCounter;

            if (isNextSlotCovered)
                LoopCounter = iNextSlotLength;
            else
                LoopCounter = SlotLength;

            string[] StartTime = SlotStartTime.Split(':');
            TimeSpan time1 = new TimeSpan(Convert.ToInt32(StartTime[0]), Convert.ToInt32(StartTime[1]), 00);
            TimeSpan time2 = new TimeSpan(00, iSlotTimeLength, 00);
            int iLen = 0;

            for (iLen = 1; iLen <= LoopCounter; iLen++)
            {
                VendorDetails vd = new VendorDetails();

                if (time1.Hours == 23 && time1.Minutes == 55)
                {
                    time1 = new TimeSpan(00, 00, 00);
                }
                else
                {
                    time1 = time1.Add(time2);
                }

                if (iLen <= iNextSlotLength2)
                    vd.VendorName = sVendorCarrierNameNextSlot + " / " + sVendorCarrierName;
                else
                    vd.VendorName = sVendorCarrierName;



                if (hdnCurrentRole.Value == "Vendor")
                {
                    vd.CellColor = Color.Black;
                }
                else
                {
                    vd.CellColor = CellBackColor;
                }
                vd.isCurrentSlotFixed = isCurrentSlotFixed;

                if (SlotDoorCounter.ContainsKey(time1.ToString() + iSiteDoorID.ToString()))
                {
                    SlotDoorCounter.Remove(time1.ToString() + iSiteDoorID.ToString());
                }



                SlotDoorCounter.Add(time1.ToString() + iSiteDoorID.ToString(), vd);
            }
        }
    }

    private void CreateDynamicDiv(ref TableCell tcLocal, int SlotLength, string SlotStartTime)
    {

        if (hdnCurrentRole.Value == "Vendor")
        {
            if (sVendorCarrierName != lblVendorT.Text) // ltvendorName.Text)
            {
                return;
            }
        }

        string[] StartTime = SlotStartTime.Split(':');
        TimeSpan time1 = new TimeSpan(Convert.ToInt32(StartTime[0]), Convert.ToInt32(StartTime[1].Substring(0, 2)), 00);
        TimeSpan time2 = new TimeSpan(00, iSlotTimeLength, 00);
        for (int iLen = 1; iLen <= SlotLength; iLen++)
        {
            if (time1.Hours == 23 && time1.Minutes == 55)
            {
                time1 = new TimeSpan(00, 00, 00);
            }
            else
            {
                time1 = time1.Add(time2);
            }
        }

        string sSlotLength1;
        //if ((iSiteDoorID == 58 || iSiteDoorID==60) && (iTimeSlotID == 42 || iTimeSlotID == 49))
        if (iSiteDoorID == 60 && iTimeSlotID == 42)
            sSlotLength1 = "0";


        // Create a new HtmlGenericControl.
        HtmlGenericControl NewControl = new HtmlGenericControl("div");

        // Set the properties of the new HtmlGenericControl control.
        string ControlID = "Volume" + iTimeSlotID.ToString() + iSiteDoorID.ToString();
        NewControl.ID = ControlID;
        string InnerHTML = "<table width='100%'> " +
                              "<tr><td colspan='5' align='center'><strong>{VendorName}</strong></td></tr> " +
                              "<tr> " +
                                "<td><strong>Arrival {Atime} </strong></td> " +
                                "<td>Lifts</td> " +
                                "<td>Pallets</td> " +
                                "<td>Cartons</td> " +
                                "<td>Lines</td> " +
                              "</tr> " +
                              "<tr> ";
        if (hdnCurrentRole.Value != "Vendor")
        {
            InnerHTML = InnerHTML + "<td><strong>Est Departure {DTime} </strong></td> ";
            InnerHTML = InnerHTML.Replace("{DTime}", GetFormatedTime(time1.ToString()));
        }
        else
        {
            InnerHTML = InnerHTML + "<td>&nbsp;</td>";
        }
        InnerHTML = InnerHTML + "<td>{Lifts}</td> " +
                                "<td>{Pallets}</td> " +
                                "<td>{Cartons}" +
                                "<td>{Lines}</td> " +
                              "</tr> " +
                            "</table>";

        InnerHTML = InnerHTML.Replace("{VendorName}", sVendorCarrierName);
        InnerHTML = InnerHTML.Replace("{Atime}", GetFormatedTime(SlotStartTime));


        if (iLifts != 0 && iLifts != -1)
            InnerHTML = InnerHTML.Replace("{Lifts}", iLifts.ToString());
        else
            InnerHTML = InnerHTML.Replace("{Lifts}", "-");

        if (iPallets != 0)
            InnerHTML = InnerHTML.Replace("{Pallets}", iPallets.ToString());
        else
            InnerHTML = InnerHTML.Replace("{Pallets}", "-");

        if (iCartons != 0)
            InnerHTML = InnerHTML.Replace("{Cartons}", iCartons.ToString());
        else
            InnerHTML = InnerHTML.Replace("{Cartons}", "-");

        if (iLines != 0)
            InnerHTML = InnerHTML.Replace("{Lines}", iLines.ToString());
        else
            InnerHTML = InnerHTML.Replace("{Lines}", "-");

        NewControl.InnerHtml = InnerHTML;
        NewControl.Attributes.Add("display", "none");
        // Add the new HtmlGenericControl to the Controls collection of the
        // TableCell control. 
        divVolume.Controls.Add(NewControl);
        if (!isClickable)
        {
            tcLocal.Attributes.Add("onmouseover", "ddrivetip('ctl00_ContentPlaceHolder1_" + ControlID + "','#ededed','400');");
            tcLocal.Attributes.Add("onmouseout", "hideddrivetip();");
        }
        else
        {
            tcLocal.Attributes.Add("onmouseover", "defColor=this.style.color;this.style.cursor = 'hand';  this.style.color='Red';ddrivetip('ctl00_ContentPlaceHolder1_" + ControlID + "','#ededed','400');");
            tcLocal.Attributes.Add("onmouseout", "this.style.color=defColor;this.style.cursor = 'default';hideddrivetip();");
        }
    }

    private void SetSlotLegendForVendor(ref TableCell tclocal, int VahicalTypeID)
    {

        int SlotLength = 0;
        string CapacityWarning = "false";
        int? TotalPallets = txtPalletsT.Text.Trim() != string.Empty ? Convert.ToInt32(txtPalletsT.Text.Trim()) : (int?)null;
        int? TotalCartons = txtCartonsT.Text.Trim() != string.Empty ? Convert.ToInt32(txtCartonsT.Text.Trim()) : (int?)null;
        int? TotalLifts = txtLiftsT.Text.Trim() != string.Empty ? Convert.ToInt32(txtLiftsT.Text.Trim()) : (int?)null;
        int? TotalLines = txtLinesT.Text.Trim() != string.Empty ? Convert.ToInt32(txtLinesT.Text.Trim()) : (int?)null;
        //int? TotalLines = txtExpectedLines.Text.Trim() != string.Empty ? Convert.ToInt32(txtExpectedLines.Text.Trim()) : (int?)null;
        string tempVendorName = string.Empty;
        //bool isAvailable = false;


        //if (iCartons < TotalCartons || iPallets < TotalPallets) {
        //    CapacityWarning = "true";
        //}



        foreach (GridViewRow gvRow in gvBookingSlots.Rows)
        {
            if (gvRow.RowType == DataControlRowType.DataRow)
            {
                HiddenField hdnAvailableFixedSlotID = (HiddenField)gvRow.FindControl("hdnAvailableFixedSlotID");
                HiddenField hdnAvailableSlotTimeID = (HiddenField)gvRow.FindControl("hdnAvailableSlotTimeID");
                HiddenField hdnAvailableSiteDoorNumberID = (HiddenField)gvRow.FindControl("hdnAvailableSiteDoorNumberID");
                HiddenField hdnAvailableSiteDoorNumber = (HiddenField)gvRow.FindControl("hdnAvailableSiteDoorNumber");
                ucLiteral ltTime = (ucLiteral)gvRow.FindControl("ltTime");

                if (gvRow.Cells[5].Text == "FIXED SLOT")
                    isCurrentSlotFixed = true;
                if (hdnAvailableSlotTimeID.Value == iTimeSlotID.ToString() && hdnAvailableSiteDoorNumberID.Value == iSiteDoorID.ToString())
                {
                    if (hdnAvailableFixedSlotID != null && hdnAvailableFixedSlotID.Value == hdnSuggestedFixedSlotID.Value && hdnSuggestedFixedSlotID.Value != "-1")
                    { //This Booking //if (gvRow.Cells[4].Text == "This Booking") {                       

                        iPallets = TotalPallets != (int?)null ? Convert.ToInt32(TotalPallets) : 0;
                        iCartons = TotalCartons != (int?)null ? Convert.ToInt32(TotalCartons) : 0;
                        iLines = TotalLines != (int?)null ? Convert.ToInt32(TotalLines) : 0;
                        iLifts = TotalLifts != (int?)null ? Convert.ToInt32(TotalLifts) : 0;
                        isClickable = false;

                        if (hdnCurrentMode.Value == "Edit")
                            SlotLength = GetTimeSlotLength();
                        else
                            SlotLength = GetRequiredTimeSlotLengthForOthers(iPallets, iCartons, "V", Convert.ToInt32(ucSeacrhVendorID));

                        SetCallHeight(SlotLength, ref tclocal, Color.GreenYellow, ltTime.Text.Trim());

                        if (actSlotLength > SlotLength)
                        {
                            string[] StartTime = ltTime.Text.Split(':');
                            TimeSpan time1 = new TimeSpan(Convert.ToInt32(StartTime[0]), Convert.ToInt32(StartTime[1].Substring(0, 2)), 00);
                            TimeSpan time2 = new TimeSpan(00, iSlotTimeLength, 00);
                            for (int iLen = 1; iLen <= SlotLength - 1; iLen++)
                            {
                                if (time1.Hours == 23 && time1.Minutes == 55)
                                {
                                    time1 = new TimeSpan(00, 00, 00);
                                }
                                else
                                {
                                    time1 = time1.Add(time2);
                                }
                            }
                            SetCallHeight(actSlotLength - SlotLength, Color.Red, time1.Hours.ToString() + ":" + time1.Minutes.ToString());
                        }

                        if (isCurrentSlotFixed && hdnCurrentRole.Value == "Vendor")
                        {
                            tclocal.Attributes.Add("onclick", "setAletrnateSlot('" + CapacityWarning + "','" + hdnAvailableSlotTimeID.Value + "','" + hdnAvailableSiteDoorNumberID.Value + "','" + hdnAvailableSiteDoorNumber.Value + "','" + hdnAvailableFixedSlotID.Value + "','" + ltTime.Text + "','" + sSlotStartDay + "');");

                        }

                        //isAvailable = true;
                        break;
                    }
                    else if (hdnCurrentMode.Value == "Edit" && hdnSuggestedSlotTimeID.Value == iTimeSlotID.ToString() && hdnSuggestedSiteDoorNumberID.Value == iSiteDoorID.ToString())
                    { //This Booking //if (gvRow.Cells[4].Text == "This Booking") {                       

                        iPallets = TotalPallets != (int?)null ? Convert.ToInt32(TotalPallets) : 0;
                        iCartons = TotalCartons != (int?)null ? Convert.ToInt32(TotalCartons) : 0;
                        iLines = TotalLines != (int?)null ? Convert.ToInt32(TotalLines) : 0;
                        iLifts = 0;
                        isClickable = false;
                        //CreateDynamicDiv(ref tclocal,false);
                        SlotLength = GetTimeSlotLength();

                        SetCallHeight(SlotLength, ref tclocal, Color.GreenYellow, ltTime.Text.Trim());

                        if (isCurrentSlotFixed && hdnCurrentRole.Value == "Vendor")
                        {
                            tclocal.Attributes.Add("onclick", "setAletrnateSlot('" + CapacityWarning + "','" + hdnAvailableSlotTimeID.Value + "','" + hdnAvailableSiteDoorNumberID.Value + "','" + hdnAvailableSiteDoorNumber.Value + "','" + hdnAvailableFixedSlotID.Value + "','" + ltTime.Text + "','" + sSlotStartDay + "');");

                        }
                        break;
                    }

                    else if (gvRow.Cells[6].Text == "Booked")
                    {   //Booked Slots                       
                        //SetCellBacknForeColor(ref tclocal, Color.Purple, isCurrentSlotFixed);
                        isClickable = false;
                        //CreateDynamicDiv(ref tclocal,false);
                        SlotLength = GetTimeSlotLength(VahicalTypeID);
                        //sVendorCarrierName = SetCallHeight(SlotLength, ref tclocal, Color.Purple, ltTime.Text.Trim());

                        //For Display current vendor name-------
                        tempVendorName = SetCallHeight(SlotLength, ref tclocal, Color.Purple, ltTime.Text.Trim());
                        if (!tempVendorName.Contains(sVendorCarrierName))
                        {
                            if (tempVendorName != string.Empty)
                            {
                                if (sVendorCarrierName != tempVendorName)
                                {
                                    tempVendorName = sVendorCarrierName + " / " + tempVendorName;
                                }
                            }
                            else
                            {
                                if (sVendorCarrierName != tempVendorName)
                                {
                                    tempVendorName = sVendorCarrierName;
                                }
                            }
                        }
                        sVendorCarrierName = tempVendorName;
                        //---------------------------------------

                        if (isCurrentSlotFixed && hdnCurrentRole.Value == "Vendor")
                        {
                            tclocal.Attributes.Add("onclick", "setAletrnateSlot('" + CapacityWarning + "','" + hdnAvailableSlotTimeID.Value + "','" + hdnAvailableSiteDoorNumberID.Value + "','" + hdnAvailableSiteDoorNumber.Value + "','" + hdnAvailableFixedSlotID.Value + "','" + ltTime.Text + "','" + sSlotStartDay + "');");

                        }

                        //isAvailable = true;
                        break;
                    }
                    else if (gvRow.Cells[5].Text == "FIXED SLOT")
                    {  // Fixed Slots        
                        sCurrentFixedSlotID = "-1";
                        isClickable = true;

                        //SlotLength = GetTimeSlotLength();
                        SlotLength = GetRequiredTimeSlotLengthForOthers(iPallets, iCartons, "V", Convert.ToInt32(ucSeacrhVendorID));

                        if (actSlotLength > SlotLength)
                        {
                            CapacityWarning = "true";
                        }
                        if (hdnAvailableFixedSlotID != null)
                        {
                            sCurrentFixedSlotID = hdnAvailableFixedSlotID.Value;
                        }

                        if (hdnAvailableFixedSlotID != null && hdnAvailableFixedSlotID.Value != hdnCurrentFixedSlotID.Value)
                        {

                            if (hdnCurrentRole.Value == "Vendor")
                            {
                                tempVendorName = SetCallHeight(SlotLength, ref tclocal, Color.Red, ltTime.Text.Trim());
                            }
                            else
                            {
                                if (actSlotLength > SlotLength)
                                {
                                    string[] StartTime = ltTime.Text.Trim().Split(':');
                                    TimeSpan time1 = new TimeSpan(Convert.ToInt32(StartTime[0]), Convert.ToInt32(StartTime[1]), 00);
                                    TimeSpan time2 = new TimeSpan(00, iSlotTimeLength, 00);
                                    VendorDetails vd = new VendorDetails();

                                    if (SlotDoorCounter.ContainsKey(time1.ToString() + iSiteDoorID.ToString()))
                                    {
                                        isCurrentSlotFixed = vd.isCurrentSlotFixed;
                                        tempVendorName = SetCallHeight(SlotLength, ref tclocal, vd.CellColor, ltTime.Text.Trim());
                                    }
                                    else
                                    {
                                        tempVendorName = SetCallHeight(SlotLength, ref tclocal, Color.LightGray, ltTime.Text.Trim());
                                    }

                                }
                                else
                                {
                                    tempVendorName = SetCallHeight(SlotLength, ref tclocal, Color.Red, ltTime.Text.Trim());
                                }
                            }
                        }

                        string WeekDay = sSlotStartDay; // dtSelectedDate.ToString("ddd");

                        tclocal.Attributes.Add("onclick", "setAletrnateSlot('" + CapacityWarning + "','" + hdnAvailableSlotTimeID.Value + "','" + hdnAvailableSiteDoorNumberID.Value + "','" + hdnAvailableSiteDoorNumber.Value + "','" + hdnAvailableFixedSlotID.Value + "','" + ltTime.Text + "','" + WeekDay + "');");
                        //isAvailable = true;

                        //For Display current vendor name-------                        
                        if (!tempVendorName.Contains(sVendorCarrierName))
                        {
                            if (tempVendorName != string.Empty)
                            {
                                if (sVendorCarrierName != tempVendorName)
                                {
                                    tempVendorName = sVendorCarrierName + " / " + tempVendorName;
                                }
                            }
                            else
                            {
                                if (sVendorCarrierName != tempVendorName)
                                {
                                    tempVendorName = sVendorCarrierName;
                                }
                            }
                        }
                        sVendorCarrierName = tempVendorName;
                        //---------------------------------------

                        break;
                    }
                }

                //if (hdnSuggestedSlotTimeID.Value == iTimeSlotID.ToString()) {
                //    if (hdnSuggestedSlotTime.Value != ltTime.Text.Trim()) {
                //        isAvailable = false;
                //    }
                //}
            }
        }
        //if (!isAvailable) {     //Standard Booking
        //    isAvailable = false;
        //}
    }

    private void SetCallHeight(int SlotLength, Color CellBackColor, string SlotStartTime)
    {
        if (SlotLength >= 1)
        {
            string[] StartTime = SlotStartTime.Split(':');
            TimeSpan time1 = new TimeSpan(Convert.ToInt32(StartTime[0]), Convert.ToInt32(StartTime[1]), 00);
            TimeSpan time2 = new TimeSpan(00, iSlotTimeLength, 00);
            int iLen = 0;

            for (iLen = 1; iLen <= SlotLength; iLen++)
            {

                VendorDetails vd = new VendorDetails();

                if (time1.Hours == 23 && time1.Minutes == 55)
                {
                    time1 = new TimeSpan(00, 00, 00);
                }
                else
                {
                    time1 = time1.Add(time2);
                }
                vd.VendorName = sVendorCarrierName;
                if (hdnCurrentRole.Value == "Vendor")
                {
                    vd.CellColor = Color.Black;
                }
                else
                {
                    vd.CellColor = CellBackColor;
                }
                vd.isCurrentSlotFixed = isCurrentSlotFixed;

                if (SlotDoorCounter.ContainsKey(time1.ToString() + iSiteDoorID.ToString()))
                {
                    SlotDoorCounter.Remove(time1.ToString() + iSiteDoorID.ToString());
                }
                SlotDoorCounter.Add(time1.ToString() + iSiteDoorID.ToString(), vd);
            }
        }
    }

    private void ConfirmFixedSlotBooking()
    {
        string errMsg = string.Empty;
        bool isWarning = false;

        if ((isProvisional) || (hdnCapacityWarning.Value == "true" && hdnSuggestedFixedSlotID.Value != "-1" && hdnSuggestedFixedSlotID.Value != string.Empty))
        {
            if (hdnCurrentRole.Value == "Vendor" || hdnCurrentRole.Value == "Carrier")
            {
                errMsg = WebCommon.getGlobalResourceValue("BK_MS_23_EXT");// "The volume being booked in exceeds the scheduled capacity for your slot - This delivery has been provisionally accepted. Office Depot's Goods In team will review and will contact you if we need to reschedule this booking.";
                AddWarningMessages(errMsg, "BK_MS_23_EXT");
            }
            else
            {
                errMsg = WebCommon.getGlobalResourceValue("BK_MS_23_INT");// "The volume being booked in exceeds the scheduled capacity for this fixed slot. Click Continue to confirm booking or click Back to select a different slot.";
                AddWarningMessages(errMsg, "BK_MS_23_INT");
            }
            isWarning = true;
        }

        if (isWarning == false)
        {
            if (hdnSuggestedFixedSlotID.Value != "-1" && hdnSuggestedFixedSlotID.Value != string.Empty)
            {
                MakeBooking(BookingType.FixedSlot.ToString());
            }
            else
            {
                if (IsNonTimeBooking == true)
                    MakeBooking(BookingType.NonTimedDelivery.ToString());
                else
                    MakeBooking(BookingType.BookedSlot.ToString());
            }
        }
        else
            ShowWarningMessages();
    }

    private void MakeBooking(string ThisBookingType)
    {

        if (ThisBookingType != BookingType.NonTimedDelivery.ToString())
        {
            if (hdnSuggestedSlotTimeID.Value == "-1" || hdnSuggestedSlotTimeID.Value == string.Empty)
                return;
        }

        if (ThisBookingType == BookingType.NonTimedDelivery.ToString())
        {
            isProvisional = false;
        }

        if (isProvisional)
        {
            if (hdnCurrentRole.Value == "Vendor" || hdnCurrentRole.Value == "Carrier")
            {
                ThisBookingType = BookingType.Provisional.ToString();
            }
        }

        APPBOK_BookingBE oAPPBOK_BookingBE = new APPBOK_BookingBE();
        APPBOK_BookingBAL oAPPBOK_BookingBAL = new APPBOK_BookingBAL();

        //-------Check booking is made for same time slot -----//
        if (ThisBookingType != BookingType.NonTimedDelivery.ToString())
        {
            oAPPBOK_BookingBE.SlotTime = new SYS_SlotTimeBE();
            oAPPBOK_BookingBE.DoorNoSetup = new MASSIT_DoorNoSetupBE();

            oAPPBOK_BookingBE.Action = "CheckBooking";
            oAPPBOK_BookingBE.SiteId = Convert.ToInt32(intSiteId); // Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
            oAPPBOK_BookingBE.ScheduleDate = LogicalSchedulDateTime; //ActualSchedulDateTime; // 
            oAPPBOK_BookingBE.SlotTime.SlotTimeID = Convert.ToInt32(hdnSuggestedSlotTimeID.Value);
            oAPPBOK_BookingBE.DoorNoSetup.SiteDoorNumberID = Convert.ToInt32(hdnSuggestedSiteDoorNumberID.Value);
            if (hdnCurrentMode.Value == "Edit")
            {
                oAPPBOK_BookingBE.BookingID = Convert.ToInt32(GetQueryStringValue("ID").ToString());
            }

            int? iBookingCount = oAPPBOK_BookingBAL.CheckBookingBAL(oAPPBOK_BookingBE);

            if (Convert.ToInt32(iBookingCount) > 0)
            {
                string errMsg = WebCommon.getGlobalResourceValue("BK_MS_20"); // "This slot is no longer available,please select another slot.";
                ShowErrorMessage(errMsg, "BK_MS_20");
                return;
            }
        }
        //----------------------------------------------------//


        oAPPBOK_BookingBE = new APPBOK_BookingBE();
        oAPPBOK_BookingBAL = new APPBOK_BookingBAL();

        oAPPBOK_BookingBE.VehicleType = new MASSIT_VehicleTypeBE();
        oAPPBOK_BookingBE.Delivery = new MASCNT_DeliveryTypeBE();
        oAPPBOK_BookingBE.Carrier = new MASCNT_CarrierBE();
        oAPPBOK_BookingBE.SlotTime = new SYS_SlotTimeBE();
        oAPPBOK_BookingBE.DoorNoSetup = new MASSIT_DoorNoSetupBE();
        oAPPBOK_BookingBE.FixedSlot = new MASSIT_FixedSlotBE();

        if (hdnCurrentMode.Value == "Add")
        {
            oAPPBOK_BookingBE.Action = "AddVendorBooking";
            oAPPBOK_BookingBE.IsBookingAmended = false;
            if (hdnCurrentRole.Value == "Vendor")
                oAPPBOK_BookingBE.IsOnlineBooking = true;
            else
                oAPPBOK_BookingBE.IsOnlineBooking = false;
        }
        else if (hdnCurrentMode.Value == "Edit")
        {
            oAPPBOK_BookingBE.Action = "EditVendorBooking";
            oAPPBOK_BookingBE.BookingID = Convert.ToInt32(GetQueryStringValue("ID").ToString());
            oAPPBOK_BookingBE.BookingRef = hdnBookingRef.Value;
            oAPPBOK_BookingBE.IsBookingAmended = true;
        }

        oAPPBOK_BookingBE.SiteId = Convert.ToInt32(intSiteId); //Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
        oAPPBOK_BookingBE.SupplierType = "V";
        oAPPBOK_BookingBE.Delivery.DeliveryTypeID = Convert.ToInt32(ddlDeliveryType.SelectedItem.Value);
        oAPPBOK_BookingBE.ScheduleDate = LogicalSchedulDateTime;
        oAPPBOK_BookingBE.Carrier.CarrierID = Convert.ToInt32(ddlCarrier.SelectedItem.Value);
        oAPPBOK_BookingBE.OtherCarrier = ddlCarrier.SelectedItem.Text.Trim(); //txtCarrierOther.Text.Trim();
        oAPPBOK_BookingBE.VehicleType.VehicleTypeID = Convert.ToInt32(ddlVehicleType.SelectedItem.Value);
        oAPPBOK_BookingBE.OtherVehicle = ddlVehicleType.SelectedItem.Text.Trim(); //txtVehicleOther.Text.Trim();
        //if (IsPreAdviseNoteRequired == true)
        //    oAPPBOK_BookingBE.PreAdviseNotification = rdoAdvice1.Checked ? "Y" : "N";

        if (ThisBookingType == BookingType.FixedSlot.ToString())
        {
            oAPPBOK_BookingBE.SlotTime.SlotTimeID = Convert.ToInt32(hdnSuggestedSlotTimeID.Value);
            oAPPBOK_BookingBE.DoorNoSetup.SiteDoorNumberID = Convert.ToInt32(hdnSuggestedSiteDoorNumberID.Value);
            oAPPBOK_BookingBE.DoorNoSetup.DoorNumber = hdnSuggestedSiteDoorNumber.Value;
            oAPPBOK_BookingBE.BookingStatusID = 1;  //Confirmed Slot
            oAPPBOK_BookingBE.BookingTypeID = 1;    //Fixed Slot
            oAPPBOK_BookingBE.FixedSlot.FixedSlotID = Convert.ToInt32(hdnSuggestedFixedSlotID.Value);
        }
        else if (ThisBookingType == BookingType.Provisional.ToString())
        {
            oAPPBOK_BookingBE.SlotTime.SlotTimeID = Convert.ToInt32(hdnSuggestedSlotTimeID.Value);
            oAPPBOK_BookingBE.DoorNoSetup.SiteDoorNumberID = Convert.ToInt32(hdnSuggestedSiteDoorNumberID.Value);
            oAPPBOK_BookingBE.DoorNoSetup.DoorNumber = hdnSuggestedSiteDoorNumber.Value;
            oAPPBOK_BookingBE.BookingStatusID = 1;  //Confirmed Slot
            oAPPBOK_BookingBE.BookingTypeID = 5;    //Provisional Booking
            oAPPBOK_BookingBE.FixedSlot.FixedSlotID = Convert.ToInt32(hdnSuggestedFixedSlotID.Value);
        }
        else if (ThisBookingType == BookingType.NonTimedDelivery.ToString())
        {
            oAPPBOK_BookingBE.DoorNoSetup.DoorNumber = string.Empty; //0;
            oAPPBOK_BookingBE.BookingStatusID = 1;   //Confirmed Slot
            oAPPBOK_BookingBE.BookingTypeID = 3;     //Non Timed Delivery            
        }
        else if (ThisBookingType == BookingType.BookedSlot.ToString())
        {
            oAPPBOK_BookingBE.SlotTime.SlotTimeID = Convert.ToInt32(hdnSuggestedSlotTimeID.Value);
            oAPPBOK_BookingBE.DoorNoSetup.SiteDoorNumberID = Convert.ToInt32(hdnSuggestedSiteDoorNumberID.Value);
            oAPPBOK_BookingBE.DoorNoSetup.DoorNumber = hdnSuggestedSiteDoorNumber.Value;
            oAPPBOK_BookingBE.BookingStatusID = 1;  //Confirmed Slot
            oAPPBOK_BookingBE.BookingTypeID = 4;    //Booked Slot            
        }

        oAPPBOK_BookingBE.UserID = Convert.ToInt32(Session["UserID"]);
        oAPPBOK_BookingBE.NumberOfCartons = txtCartonsT.Text.Trim() != string.Empty ? Convert.ToInt32(txtCartonsT.Text.Trim()) : (int?)null;
        oAPPBOK_BookingBE.NumberOfLift = txtLiftsT.Text.Trim() != string.Empty ? Convert.ToInt32(txtLiftsT.Text.Trim()) : (int?)null;
        oAPPBOK_BookingBE.NumberOfPallet = txtPalletsT.Text.Trim() != string.Empty ? Convert.ToInt32(txtPalletsT.Text.Trim()) : (int?)null;
        oAPPBOK_BookingBE.NumberOfLines = txtLinesT.Text.Trim() != string.Empty ? Convert.ToInt32(txtLinesT.Text.Trim()) : (int?)null;
        oAPPBOK_BookingBE.WeekDay = getWeekdayNo();

        //PO Details
        oAPPBOK_BookingBE.VendorID = Convert.ToInt32(ucSeacrhVendorID); // Convert.ToInt32(ucSeacrhVendor1.VendorNo);

        DataTable myDataTable = null;
        if (ViewState["myDataTable"] != null)
            myDataTable = (DataTable)ViewState["myDataTable"];

        string strPoIds = string.Empty;
        string strPoNos = string.Empty;

        if (myDataTable != null)
        {
            for (int iCount = 0; iCount < myDataTable.Rows.Count; iCount++)
            {
                if (strPoIds == string.Empty)
                {
                    strPoIds = myDataTable.Rows[iCount]["PurchaseOrderID"].ToString();
                    strPoNos = myDataTable.Rows[iCount]["PurchaseNumber"].ToString();
                }
                else
                {
                    strPoIds = strPoIds + "," + myDataTable.Rows[iCount]["PurchaseOrderID"].ToString();
                    strPoNos = strPoNos + "," + myDataTable.Rows[iCount]["PurchaseNumber"].ToString();
                }
            }
        }

        oAPPBOK_BookingBE.PurchaseOrdersIDs = strPoIds;
        oAPPBOK_BookingBE.PurchaseOrders = strPoNos;

        // Sprint 1 - Point 4 - Start - For Booking Comments.
        //oAPPBOK_BookingBE.BookingComments = txtEditBookingComment.Text.Trim();
        // Sprint 1 - Point 4 - End - For Booking Comments.

        int? BookingID = oAPPBOK_BookingBAL.AddVendorBookingBAL(oAPPBOK_BookingBE);

        EncryptQueryString("APPBok_Confirm.aspx?BookingID=" + BookingID);
    }

    private int getWeekdayNo()
    {
        string Weekday = string.Empty;

        if (hdnSuggestedWeekDay.Value.Trim() != string.Empty)
            Weekday = hdnSuggestedWeekDay.Value.ToLower();
        else
            Weekday = LogicalSchedulDateTime.DayOfWeek.ToString().ToLower().Substring(0, 3);

        int iWeekNo = 0;
        switch (Weekday)
        {
            case ("sun"):
                iWeekNo = 1;
                break;
            case ("mon"):
                iWeekNo = 2;
                break;
            case ("tue"):
                iWeekNo = 3;
                break;
            case ("wed"):
                iWeekNo = 4;
                break;
            case ("thu"):
                iWeekNo = 5;
                break;
            case ("fri"):
                iWeekNo = 6;
                break;
            case ("sat"):
                iWeekNo = 7;
                break;
            default:
                iWeekNo = 0;
                break;
        }
        return iWeekNo;
    }

    private string getWeekday(int iWeekNo)
    {
        string Weekday = string.Empty;

        switch (iWeekNo)
        {
            case (1):
                Weekday = "sun";
                break;
            case (2):
                Weekday = "mon";
                break;
            case (3):
                Weekday = "tue";
                break;
            case (4):
                Weekday = "wed";
                break;
            case (5):
                Weekday = "thu";
                break;
            case (6):
                Weekday = "fri";
                break;
            case (7):
                Weekday = "sat";
                break;
            default:
                Weekday = string.Empty;
                break;
        }
        return Weekday;
    }

    protected void btnMakeNonTimeBooking_Click(object sender, EventArgs e)
    {
        string errMsg = string.Empty;

        if (hdnCurrentRole.Value == "Vendor" || hdnCurrentRole.Value == "Carrier")
        {
            errMsg = WebCommon.getGlobalResourceValue("BK_MS_24_EXT_1") + " " + ActualSchedulDateTime.ToString("dd/MM/yyyy") + " " + WebCommon.getGlobalResourceValue("BK_MS_24_EXT_2");  // "This booking is to be made on " + txtSchedulingdate.GetDate.ToString() + " with no fixed time. Please ensure you make the delivery within the site's receiving hours. ";
            ShowErrorMessage(errMsg, "BK_MS_24_EXT");
            return;
        }
        else
        {
            errMsg = WebCommon.getGlobalResourceValue("BK_MS_24_INT");  // "This booking is to be made without a delivery time. Click 'CONTINUE' to confirm or 'BACK' to add a time to the booking";
            AddWarningMessages(errMsg, "BK_MS_24_INT");
            ShowWarningMessages();
        }
    }

    protected void btnAlternateSlot_Click(object sender, EventArgs e)
    {
        //string errMsg = string.Empty;

        //if (btnConfirmBooking.Visible == true) {  //(hdnSuggestedFixedSlotID.Value != string.Empty && hdnSuggestedFixedSlotID.Value != "-1") {  // 
        //    errMsg = WebCommon.getGlobalResourceValue("BK_MS_22_INT");// "There is a slot available for this delivery? Are you sure want to search for an alternative slot?";
        //    AddWarningMessages(errMsg, "BK_MS_22_INT");
        //    ShowWarningMessages();
        //}
        //else {
        //    PaintAlternateSlotScreen();
        //}


        PaintAlternateSlotScreen();

    }

    protected void btnAlternateSlotVendor_Click(object sender, EventArgs e)
    {
        //string errMsg = string.Empty;

        //if (btnConfirmBooking.Visible == true) {
        //    if (hdnSuggestedFixedSlotID.Value != string.Empty && hdnSuggestedFixedSlotID.Value != "-1") {
        //        errMsg = WebCommon.getGlobalResourceValue("BK_MS_22_EXT_1"); // "You have a fixed slot on this day  which you have chosen not to use. If this Fixed Slot is not required then please cancel this slot or it will adversely affect your vendor evaluation.";
        //    }
        //    else {
        //        errMsg = WebCommon.getGlobalResourceValue("BK_MS_22_EXT_2"); //"There is a slot available for this delivery? Are you sure want to search for an alternative slot?";
        //    }
        //    AddWarningMessages(errMsg, "BK_MS_22_EXT");
        //    ShowWarningMessages();
        //}
        //else {
        //    GenerateAlternateSlotForVendor();
        //    PaintAlternateScreenForVendor();
        //    mvBookingEdit.ActiveViewIndex = 5;
        //}

        GenerateAlternateSlotForVendor();
        PaintAlternateScreenForVendor();
        mvBookingEdit.ActiveViewIndex = 5;
    }

    private void PaintAlternateScreenForVendor()
    {

        ltSite.Text = lblVendorT.Text; //ddlSite.innerControlddlSite.SelectedItem.Text;
        ltVendor_3.Text = ucSeacrhVendorName; // ((HiddenField)ucSeacrhVendor1.FindControl("hdnVandorName")).Value;

        ltCarrier_3.Text = ddlCarrier.SelectedItem.Text;
        //ltOtherCarrier_3.Text = "[" + txtCarrierOther.Text.Trim() + "]";
        ltOtherCarrier_3.Text = "[" + ddlCarrier.SelectedItem.Text.Trim() + "]";

        //if (txtCarrierOther.Text.Trim() != string.Empty)
        if (ddlCarrier.SelectedItem.Text.Trim() != string.Empty)
        {
            ltOtherCarrier_3.Visible = true;
        }
        else
        {
            ltOtherCarrier_3.Visible = false;
        }

        ltVehicleType_3.Text = ddlVehicleType.SelectedItem.Text;
        //ltOtherVehicleType_3.Text = "[" + txtVehicleOther.Text.Trim() + "]";
        ltOtherVehicleType_3.Text = "[" + ddlVehicleType.SelectedItem.Text.Trim() + "]";

        //if (txtVehicleOther.Text.Trim() != string.Empty)
        if (ddlVehicleType.SelectedItem.Text.Trim() != string.Empty)
        {
            ltOtherVehicleType_3.Visible = true;
        }
        else
        {
            ltOtherVehicleType_3.Visible = false;
        }

        ltSchedulingDate_3.Text = ActualSchedulDateTime.ToString("dd/MM/yyyy");
        ltTimeSuggested_3.Text = ltSuggestedSlotTime.Text;


        ltExpectedLifts.Text = txtLiftsT.Text.Trim() != string.Empty ? txtLiftsT.Text.Trim() : "-";
        ltExpectedPallets.Text = txtPalletsT.Text.Trim() != string.Empty ? txtPalletsT.Text.Trim() : "-";
        //ltExpectedLines.Text = txtExpectedLines.Text.Trim() != string.Empty ? txtExpectedLines.Text.Trim() : "-";
        ltExpectedLines.Text = txtLinesT.Text.Trim() != string.Empty ? txtLinesT.Text.Trim() : "-";

        ltExpectedCartons.Text = txtCartonsT.Text.Trim() != string.Empty ? txtCartonsT.Text.Trim() : "-";
    }

    protected void GenerateNonTimeBooking()
    {

        APPBOK_BookingBE oAPPBOK_BookingBE = new APPBOK_BookingBE();
        APPBOK_BookingBAL oMASBOK_BookingBAL = new APPBOK_BookingBAL();

        oAPPBOK_BookingBE.Action = "GetNonTimeBooking";
        oAPPBOK_BookingBE.ScheduleDate = LogicalSchedulDateTime;
        oAPPBOK_BookingBE.SiteId = Convert.ToInt32(intSiteId); //Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);

        List<APPBOK_BookingBE> lstNonTimeBooking = oMASBOK_BookingBAL.GetNonTimeBookingBAL(oAPPBOK_BookingBE);

        if (lstNonTimeBooking.Count > 0)
        {
            gvNonTimeBooking.DataSource = lstNonTimeBooking;
            gvNonTimeBooking.DataBind();
        }

    }

    private void GenerateAlternateSlot()
    {
        GenerateAlternateSlot(false);
    }

    private void GenerateAlternateSlot(bool isStandardBookingRequired, int iSKUSiteDoorID = 0, int iSKUSiteDoorTypeID = 0)
    {

        //int[] ArrDoorTypeID = new int[20];
        int? TotalPallets = txtPalletsT.Text.Trim() != string.Empty ? Convert.ToInt32(txtPalletsT.Text.Trim()) : (int?)null;
        int? TotalCartons = txtCartonsT.Text.Trim() != string.Empty ? Convert.ToInt32(txtCartonsT.Text.Trim()) : (int?)null;
        //int? TotalLines = txtExpectedLines.Text.Trim() != string.Empty ? Convert.ToInt32(txtExpectedLines.Text.Trim()) : (int?)null;
        int? TotalLines = txtLinesT.Text.Trim() != string.Empty ? Convert.ToInt32(txtLinesT.Text.Trim()) : (int?)null;

        actSlotLength = GetRequiredTimeSlotLength(TotalPallets, TotalCartons);

        #region -------Collect Basic Information-----------
        //-----------------Start Collect Basic Information --------------------------//
        DateTime dtSelectedDate = LogicalSchedulDateTime;
        string WeekDay = dtSelectedDate.ToString("dddd");

        DateTime dtMon = Utilities.Common.GetFirstDayOfWeek(dtSelectedDate);

        MASSIT_WeekSetupBE oMASSIT_WeekSetupBE = new MASSIT_WeekSetupBE();
        MASSIT_WeekSetupBAL oMASSIT_WeekSetupBAL = new MASSIT_WeekSetupBAL();

        oMASSIT_WeekSetupBE.Action = "GetSpecificWeek";
        oMASSIT_WeekSetupBE.SiteID = Convert.ToInt32(intSiteId); //Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
        oMASSIT_WeekSetupBE.EndWeekday = WeekDay;
        oMASSIT_WeekSetupBE.ScheduleDate = LogicalSchedulDateTime;

        List<MASSIT_WeekSetupBE> lstWeekSetup = oMASSIT_WeekSetupBAL.GetWeekSetupDetailsBAL(oMASSIT_WeekSetupBE);

        if (lstWeekSetup == null || lstWeekSetup.Count <= 0 || lstWeekSetup[0].StartTime == null)
        {

            oMASSIT_WeekSetupBE.Action = "ShowAll";
            lstWeekSetup = oMASSIT_WeekSetupBAL.GetWeekSetupDetailsBAL(oMASSIT_WeekSetupBE);

            if (lstWeekSetup.Count <= 0)
            {
                string errorMeesage = WebCommon.getGlobalResourceValue("WeekSetupSettingNotDefined");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert1", "alert('" + errorMeesage + "')", true);
                return;
            }
            else if (lstWeekSetup[0].StartTime == null)
            {
                string errorMeesage = WebCommon.getGlobalResourceValue("WeekSetupSettingNotDefined");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert1", "alert('" + errorMeesage + "')", true);
                return;
            }
        }



        APPSIT_DoorOpenTimeBAL oMASSIT_DoorOpenTimeBAL = new APPSIT_DoorOpenTimeBAL();
        MASSIT_DoorOpenTimeBE oMASSIT_DoorOpenTimeBE = new MASSIT_DoorOpenTimeBE();

        //------Check for Specific Entry First--------//        

        //APPSIT_DoorOpenTimeSpecificBAL oAPPSIT_DoorOpenTimeSpecificBAL = new APPSIT_DoorOpenTimeSpecificBAL();
        //MASSIT_DoorOpenTimeSpecificBE oMASSIT_DoorOpenTimeSpecificBE = new MASSIT_DoorOpenTimeSpecificBE();
        //oMASSIT_DoorOpenTimeSpecificBE.Action = "GetDoorConstraintsSpecific";
        //oMASSIT_DoorOpenTimeSpecificBE.DoorOpenTime = new MASSIT_DoorOpenTimeBE();
        //oMASSIT_DoorOpenTimeSpecificBE.DoorOpenTime.DoorNo = new MASSIT_DoorNoSetupBE();
        //oMASSIT_DoorOpenTimeSpecificBE.DoorOpenTime.DoorType = new BusinessEntities.ModuleBE.Appointment.CountrySetting.MASCNT_DoorTypeBE();
        //oMASSIT_DoorOpenTimeSpecificBE.DoorOpenTime.DoorNo.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
        //oMASSIT_DoorOpenTimeSpecificBE.DoorOpenTime.Weekday = WeekDay;
        //oMASSIT_DoorOpenTimeSpecificBE.WeekStartDate = dtMon;
        //List<MASSIT_DoorOpenTimeSpecificBE> lstDoorConstraintsSpecific = oAPPSIT_DoorOpenTimeSpecificBAL.GetDoorConstraintsSpecificBAL(oMASSIT_DoorOpenTimeSpecificBE);
        //if (lstDoorConstraintsSpecific.Count > 0) { //Get Specific settings 
        //    for (int iCount = 0; iCount < lstDoorConstraintsSpecific.Count; iCount++) {
        //        DoorIDs += lstDoorConstraintsSpecific[iCount].DoorOpenTime.SlotTimeID + "@" + lstDoorConstraintsSpecific[iCount].DoorOpenTime.SiteDoorNumberID + ",";                
        //    }
        //}
        //else {  //Get Generic settings 
        //    oMASSIT_DoorOpenTimeBE.Action = "GetDoorConstraints";
        //    oMASSIT_DoorOpenTimeBE.DoorNo = new MASSIT_DoorNoSetupBE();
        //    oMASSIT_DoorOpenTimeBE.DoorNo.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
        //    oMASSIT_DoorOpenTimeBE.Weekday = WeekDay;
        //    List<MASSIT_DoorOpenTimeBE> lstDoorConstraints = oMASSIT_DoorOpenTimeBAL.GetDoorConstraintsBAL(oMASSIT_DoorOpenTimeBE);
        //    for (int iCount = 0; iCount < lstDoorConstraints.Count; iCount++) {
        //        DoorIDs += lstDoorConstraints[iCount].SlotTimeID + "@" + lstDoorConstraints[iCount].SiteDoorNumberID + ",";

        //    }
        //}
        DoorIDs = GetDoorConstraints();

        oMASSIT_DoorOpenTimeBE.Action = "GetDoorNoAndType";
        oMASSIT_DoorOpenTimeBE.DoorNo = new MASSIT_DoorNoSetupBE();
        oMASSIT_DoorOpenTimeBE.DoorNo.SiteID = Convert.ToInt32(intSiteId); //Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);

        List<MASSIT_DoorOpenTimeBE> lstDoorNoType = oMASSIT_DoorOpenTimeBAL.GetDoorNoSetupDetailsBAL(oMASSIT_DoorOpenTimeBE);

        int iDoorCount = lstDoorNoType.Count;

        //decimal decCellWidth = Convert.ToDecimal(155); // (87 / (iDoorCount));
        //double dblCellWidth = Convert.ToDouble(Math.Ceiling(decCellWidth));

        int dblCellWidth = 165;
        int i1stColumnWidth = 145;
        //-------------------------End Collect Basic Information ------------------------------//
        #endregion


        #region --------------Grid Header---------------
        //-------------Start Grid Header--------------//
        Table tblSlot = new Table();
        tblSlot.CellSpacing = 0;
        tblSlot.CellPadding = 0;
        tblSlot.CssClass = "FixedTables";
        tblSlot.ID = "Open_Text_General";
        tblSlot.Width = Unit.Pixel((dblCellWidth * iDoorCount) + i1stColumnWidth);

        tableDiv_General.Controls.Add(tblSlot);

        TableRow trDoorNo = new TableRow();
        trDoorNo.TableSection = TableRowSection.TableHeader;
        TableHeaderCell tcDoorNo = new TableHeaderCell();
        tcDoorNo.Text = "Door Name / Door Type";  // +"<input type=image src='../../../Images/arrowleft.gif' style=height:16px; width:26px; />";
        tcDoorNo.BorderWidth = Unit.Pixel(1);
        tcDoorNo.Width = Unit.Pixel(i1stColumnWidth);
        trDoorNo.Cells.Add(tcDoorNo);

        TableRow trTime = new TableRow();
        trTime.TableSection = TableRowSection.TableHeader;
        TableHeaderCell tcTime = new TableHeaderCell();
        tcTime.Text = "Time";
        tcTime.BorderWidth = Unit.Pixel(1);
        tcTime.Width = Unit.Pixel(i1stColumnWidth);
        trTime.Cells.Add(tcTime);

        for (int iCount = 1; iCount <= iDoorCount; iCount++)
        {
            //ArrDoorTypeID[iCount - 1] = lstDoorNoType[iCount - 1].DoorType.DoorTypeID;
            tcDoorNo = new TableHeaderCell();
            tcDoorNo.BorderWidth = Unit.Pixel(1);
            tcDoorNo.Text = lstDoorNoType[iCount - 1].DoorNo.DoorNumber.ToString() + " / " + lstDoorNoType[iCount - 1].DoorType.DoorType; ;
            tcDoorNo.Width = Unit.Pixel(dblCellWidth);
            trDoorNo.Cells.Add(tcDoorNo);

            tcTime = new TableHeaderCell();
            tcTime.BorderWidth = Unit.Pixel(1);
            tcTime.Text = "Vendor/Carrier";
            tcTime.Width = Unit.Pixel(dblCellWidth);
            trTime.Cells.Add(tcTime);
        }
        trDoorNo.BackColor = System.Drawing.Color.LightGray;
        tblSlot.Rows.Add(trDoorNo);

        trTime.BackColor = System.Drawing.Color.LightGray;
        tblSlot.Rows.Add(trTime);
        //-------------End Grid Header -------------//
        #endregion

        #region --------------Slot Ploting---------------
        //-------------Start Slot Ploting--------------//

        //--------Find all Fixed Slots, Bookings For all Vendor/Carrier-------//
        APPBOK_BookingBE oAPPBOK_BookingBE = new APPBOK_BookingBE();
        APPBOK_BookingBAL oAPPBOK_BookingBAL = new APPBOK_BookingBAL();

        oAPPBOK_BookingBE.Action = "AllBookingFixedSlotDetails";
        oAPPBOK_BookingBE.SiteId = Convert.ToInt32(intSiteId); //Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
        oAPPBOK_BookingBE.ScheduleDate = LogicalSchedulDateTime;
        oAPPBOK_BookingBE.SelectedScheduleDate = LogicalSchedulDateTime;
        lstAllFixedDoor = oAPPBOK_BookingBAL.GetAllBookingFixedSlotDetailsBAL(oAPPBOK_BookingBE);

        lstAllFixedDoor = lstAllFixedDoor.FindAll(delegate(APPBOK_BookingBE St)
        {

            DateTime? dt = (DateTime?)null;
            if (St.SlotTime.SlotTime != null && St.SlotTime.SlotTime != string.Empty)
            {
                dt = new DateTime(1900, 1, 1, Convert.ToInt32(St.SlotTime.SlotTime.Split(':')[0]), Convert.ToInt32(St.SlotTime.SlotTime.Split(':')[1]), 0);
            }
            else
            {
                dt = new DateTime(1900, 1, 1, 0, 0, 0);
            }

            return dt <= lstWeekSetup[0].EndTime;
            //if (St.BookingRef == "Fixed booking")
            //    return dt <= lstWeekSetup[0].EndTime;
            //else
            //    return true;
        });

        //--------------------------------------//

        if (lstWeekSetup[0].StartWeekday != lstWeekSetup[0].EndWeekday)
        {
            oAPPBOK_BookingBE.Action = "AllBookingFixedSlotDetails";
            oAPPBOK_BookingBE.SiteId = Convert.ToInt32(intSiteId); //Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
            oAPPBOK_BookingBE.ScheduleDate = LogicalSchedulDateTime.AddDays(-1);
            List<APPBOK_BookingBE> lstAllFixedDoorStartWeek = oAPPBOK_BookingBAL.GetAllBookingFixedSlotDetailsBAL(oAPPBOK_BookingBE);

            //Filetr Non Time Delivery
            lstAllFixedDoorStartWeek = lstAllFixedDoorStartWeek.FindAll(delegate(APPBOK_BookingBE St)
            {
                return St.SlotTime.SlotTime != "";
            });

            if (lstAllFixedDoorStartWeek != null && lstAllFixedDoorStartWeek.Count > 0)
            {
                lstAllFixedDoorStartWeek = lstAllFixedDoorStartWeek.FindAll(delegate(APPBOK_BookingBE St)
                {
                    DateTime? dt = (DateTime?)null;
                    if (St.SlotTime.SlotTime != null && St.SlotTime.SlotTime != string.Empty)
                    {
                        dt = new DateTime(1900, 1, 1, Convert.ToInt32(St.SlotTime.SlotTime.Split(':')[0]), Convert.ToInt32(St.SlotTime.SlotTime.Split(':')[1]), 0);
                    }
                    else
                    {
                        dt = new DateTime(1900, 1, 1, 0, 0, 0);
                    }
                    return dt >= lstWeekSetup[0].StartTime;
                });

                if (lstAllFixedDoorStartWeek.Count > 0)
                {
                    lstAllFixedDoor = MergeListCollections(lstAllFixedDoor, lstAllFixedDoorStartWeek);
                }
            }
        }

        SYS_SlotTimeBAL oSYS_SlotTimeBAL = new SYS_SlotTimeBAL();
        SYS_SlotTimeBE oSYS_SlotTimeBE = new SYS_SlotTimeBE();

        oSYS_SlotTimeBE.Action = "ShowAll";
        List<SYS_SlotTimeBE> lstSlotTime = oSYS_SlotTimeBAL.GetSlotTimeBAL(oSYS_SlotTimeBE);

        for (int jCount = 0; jCount < lstSlotTime.Count; jCount++)
        {
            string StartTime = lstWeekSetup[0].StartTime.Value.Hour.ToString() + "." + lstWeekSetup[0].StartTime.Value.Minute.ToString();
            if (lstSlotTime[jCount].SlotTime.Split(new char[] { ':' })[0] == "00")
            {
                StartTime = "24." + lstWeekSetup[0].StartTime.Value.Minute.ToString();
            }

            string EndTime = lstWeekSetup[0].EndTime.Value.Hour.ToString() + "." + lstWeekSetup[0].EndTime.Value.Minute.ToString();
            if (lstSlotTime[jCount].SlotTime.Split(new char[] { ':' })[0] == "00")
            {
                EndTime = "24." + lstWeekSetup[0].EndTime.Value.Minute.ToString();
            }

            decimal dSlotTime = Convert.ToDecimal(lstSlotTime[jCount].SlotTime.Replace(':', '.'));
            decimal dStartTime = Convert.ToDecimal(StartTime);
            decimal dEndTime = Convert.ToDecimal(EndTime);

            if (lstWeekSetup[0].StartWeekday != lstWeekSetup[0].EndWeekday)
            {
                dEndTime = Convert.ToDecimal("24.00");
            }

            if (dSlotTime >= dStartTime && dSlotTime < dEndTime)
            {

                TableRow tr = new TableRow();
                tr.TableSection = TableRowSection.TableBody;
                TableCell tc = new TableCell();
                tc.BorderWidth = Unit.Pixel(1);

                if (lstSlotTime.Count - 1 > jCount)
                {
                    tc.Text = lstWeekSetup[0].StartWeekday + " " + GetFormatedTime(lstSlotTime[jCount].SlotTime) + " - " + GetFormatedTime(lstSlotTime[jCount + 1].SlotTime);
                }
                else
                {
                    tc.Text = lstWeekSetup[0].StartWeekday + " " + GetFormatedTime(lstSlotTime[jCount].SlotTime) + " - " + GetFormatedTime(lstSlotTime[0].SlotTime);
                }

                tc.Width = Unit.Pixel(i1stColumnWidth);
                tr.Cells.Add(tc);

                for (int iCount = 1; iCount <= iDoorCount; iCount++)
                {
                    tc = new TableCell();
                    tc.BorderWidth = Unit.Pixel(1);
                    tc.Width = Unit.Pixel(dblCellWidth);
                    iTimeSlotID = lstSlotTime[jCount].SlotTimeID;
                    iSiteDoorID = lstDoorNoType[iCount - 1].SiteDoorNumberID;
                    sTimeSlot = lstSlotTime[jCount].SlotTime;
                    sDoorNo = lstDoorNoType[iCount - 1].DoorNo.DoorNumber;
                    sSlotStartDay = lstWeekSetup[0].StartWeekday;

                    //----Sprint 3b------//
                    iTimeSlotOrderBYID = lstSlotTime[jCount].OrderBYID;
                    //------------------//

                    Literal ddl = SetCellColor(ref tc);
                    tc.Controls.Add(ddl);
                    tr.Cells.Add(tc);
                }
                tblSlot.Rows.Add(tr);
            }
        }



        if (lstWeekSetup[0].StartWeekday != lstWeekSetup[0].EndWeekday)
        {
            for (int jCount = 0; jCount < lstSlotTime.Count; jCount++)
            {
                string EndTime = lstWeekSetup[0].EndTime.Value.Hour.ToString() + "." + lstWeekSetup[0].EndTime.Value.Minute.ToString();
                if (lstSlotTime[jCount].SlotTime.Split(new char[] { ':' })[0] == "00")
                {
                    EndTime = "24." + lstWeekSetup[0].EndTime.Value.Minute.ToString();
                }
                if (Convert.ToDecimal(lstSlotTime[jCount].SlotTime.Replace(':', '.')) <= Convert.ToDecimal(EndTime))
                {

                    TableRow tr = new TableRow();
                    tr.TableSection = TableRowSection.TableBody;
                    TableCell tc = new TableCell();
                    tc.BorderWidth = Unit.Pixel(1);

                    if (lstSlotTime.Count - 1 > jCount)
                    {
                        tc.Text = lstWeekSetup[0].EndWeekday + " " + GetFormatedTime(lstSlotTime[jCount].SlotTime) + " - " + GetFormatedTime(lstSlotTime[jCount + 1].SlotTime);
                    }
                    else
                    {
                        tc.Text = lstWeekSetup[0].EndWeekday + " " + GetFormatedTime(lstSlotTime[jCount].SlotTime) + " - " + GetFormatedTime(lstSlotTime[0].SlotTime);
                    }

                    tc.Width = Unit.Pixel(i1stColumnWidth);
                    tr.Cells.Add(tc);

                    for (int iCount = 1; iCount <= iDoorCount; iCount++)
                    {

                        tc = new TableCell();
                        tc.Width = Unit.Pixel(dblCellWidth);
                        tc.BorderWidth = Unit.Pixel(1);
                        iTimeSlotID = lstSlotTime[jCount].SlotTimeID;
                        iSiteDoorID = lstDoorNoType[iCount - 1].SiteDoorNumberID;
                        sTimeSlot = lstSlotTime[jCount].SlotTime;
                        sDoorNo = lstDoorNoType[iCount - 1].DoorNo.DoorNumber;
                        sSlotStartDay = lstWeekSetup[0].EndWeekday;

                        //----Sprint 3b------//
                        iTimeSlotOrderBYID = lstSlotTime[jCount].OrderBYID;
                        //------------------//

                        Literal ddl = SetCellColor(ref tc);
                        tc.Controls.Add(ddl);
                        tr.Cells.Add(tc);

                    }
                    tblSlot.Rows.Add(tr);

                }
            }
        }
        //-------------End Slot Ploting--------------//
        #endregion



        #region Start Finding Insufficent Slot Time
        Color cBackColorFirstCell, cBackColor, cBorderColor, cBackColorPreviousCell, cBorderColorPreviousCell, cBackColorFirstSlot;
        string att, abbr;
        bool isNextSlot;
        bool isSlotInsufficent;
        bool isNewCell = false;

        cBorderColorPreviousCell = Color.Empty;

        for (int iCell = 1; iCell <= iDoorCount; iCell++)
        {
            cBackColorPreviousCell = Color.Empty;

            for (int iRow = 0; iRow < tblSlot.Rows.Count; iRow++)
            {
                //FindNextSufficentSlotTime(ref tblSlot, iCell, iRow);


                isSlotInsufficent = false;
                cBackColor = tblSlot.Rows[iRow].Cells[iCell].BackColor;
                cBackColorFirstCell = tblSlot.Rows[iRow].Cells[iCell].BackColor;
                cBorderColor = tblSlot.Rows[iRow].Cells[iCell].BorderColor;

                string temp = string.Empty;
                if (cBackColor == Color.GreenYellow)
                    temp = string.Empty;

                cBackColorFirstSlot = Color.Empty;

                if ((cBackColor.Name == "0" || cBackColor == Color.White || cBackColor == Color.GreenYellow || cBackColor == Color.Orange) && cBorderColor != Color.Red)
                {
                    isNextSlot = true;

                    for (int iLen = 0; iLen < actSlotLength; iLen++)
                    {
                        if (iRow + iLen < tblSlot.Rows.Count)
                        {

                            if (iLen == 0)
                                cBackColorFirstSlot = tblSlot.Rows[iRow + iLen].Cells[iCell].BackColor;

                            cBackColor = tblSlot.Rows[iRow + iLen].Cells[iCell].BackColor;
                            cBorderColor = tblSlot.Rows[iRow + iLen].Cells[iCell].BorderColor;

                            att = tblSlot.Rows[iRow + iLen].Cells[iCell].Attributes["onclick"];

                            //if (iCell == 2 && cBackColor == Color.White && att.Contains("11:15"))
                            //    cBackColor = tblSlot.Rows[iRow + iLen].Cells[iCell].BackColor;

                            if (att != null && att.Contains("setAletrnateSlot"))
                            {
                                tblSlot.Rows[iRow + iLen].Cells[iCell].Attributes["onclick"].Remove(0);
                            }

                            //if (cBackColor.Name != "0" && cBackColor != Color.White && cBackColor != Color.GreenYellow && cBackColor != Color.Orange) {
                            //    isNextSlot = false;
                            //    break;
                            //}



                            abbr = tblSlot.Rows[iRow + iLen].Cells[iCell].Attributes["abbr"];
                            if (abbr != null && abbr != string.Empty && abbr.Contains("C"))
                            {
                                isNextSlot = false;
                                break;
                            }
                            else if (abbr != null && abbr != string.Empty && abbr.Contains("V"))
                            {
                                string[] arrabbr = abbr.Split('-');
                                if (arrabbr[1] != ucSeacrhVendorID)
                                {
                                    isNextSlot = false;
                                    break;
                                }
                                else if (arrabbr[1] == ucSeacrhVendorID && iLen > 0 && cBackColor != Color.White && cBackColor != Color.GreenYellow && cBorderColor != Color.Red)
                                {
                                    isNextSlot = false;
                                    break;
                                }
                            }
                            else if (cBackColor == Color.Black)
                            {
                                isNextSlot = false;
                                break;
                            }
                        }
                        else
                        {
                            cBackColor = tblSlot.Rows[iRow].Cells[iCell].BackColor;

                            att = tblSlot.Rows[iRow].Cells[iCell].Attributes["onclick"];
                            if (att != null && att.Contains("setAletrnateSlot"))
                            {
                                tblSlot.Rows[iRow].Cells[iCell].Attributes["onclick"].Remove(0);
                            }
                            //if (cBackColor.Name != "0" && cBackColor != Color.White && cBackColor != Color.GreenYellow && cBackColor != Color.Orange) {
                            isNextSlot = false;
                            //}
                            isSlotInsufficent = true;
                            break;
                        }

                        //if (!strVehicleDoorTypeMatrix.Contains(lstDoorNoType[iCell - 1].DoorNo.SiteDoorTypeID.ToString() + ",")) {
                        if (hdnCurrentRole.Value != "Vendor")
                        {
                            if (cBackColorFirstSlot == Color.GreenYellow)
                            {
                                if (tblSlot.Rows[iRow + iLen].Cells[iCell].BackColor == Color.Orange)
                                {
                                    tblSlot.Rows[iRow + iLen].Cells[iCell].BackColor = Color.GreenYellow;
                                }
                            }
                        }
                        //}
                    }
                    if (isNextSlot == false)
                    {
                        att = tblSlot.Rows[iRow].Cells[iCell].Attributes["onclick"];

                        if (hdnCurrentRole.Value == "Vendor")
                        {
                            tblSlot.Rows[iRow].Cells[iCell].BackColor = Color.Black;
                            tblSlot.Rows[iRow].Cells[iCell].Attributes.Add("onmouseover", "this.style.cursor = 'default'");
                            tblSlot.Rows[iRow].Cells[iCell].Attributes.Add("onmouseout", "this.style.cursor = 'default'");
                            tblSlot.Rows[iRow].Cells[iCell].Attributes.Add("onclick", "");
                        }
                        else
                        {
                            if (cBackColor.Name != "0" && cBackColor != Color.White && cBackColor != Color.GreenYellow && cBackColor != Color.Orange)
                            {
                                if ((att != null && att.Contains("CheckAletrnateSlot")) || isSlotInsufficent)
                                {
                                    att = att.Replace("CheckAletrnateSlot(", "");
                                    att = att.Replace(");", "");
                                    string[] Arratt = att.Split(',');
                                    string att0 = Arratt[0];
                                    string att1 = Arratt[1];
                                    string att2 = Arratt[2];
                                    string att3 = Arratt[3];
                                    string att4 = Arratt[5];
                                    tblSlot.Rows[iRow].Cells[iCell].Attributes.Add("onclick", "setAletrnateSlot('true'," + att0 + "," + att1 + "," + att2 + ",'-1'," + att3 + "," + att4 + ");");
                                }
                            }
                            else
                            {
                                if (att != null && att.Contains("CheckAletrnateSlot"))
                                {
                                    att = att.Replace("CheckAletrnateSlot(", "");
                                    att = att.Replace(");", "");
                                    string[] Arratt = att.Split(',');
                                    string att0 = Arratt[0];
                                    string att1 = Arratt[1];
                                    string att2 = Arratt[2];
                                    string att3 = Arratt[3];
                                    string att4 = Arratt[5];
                                    tblSlot.Rows[iRow].Cells[iCell].Attributes.Add("onclick", "setAletrnateSlot('true'," + att0 + "," + att1 + "," + att2 + ",'-1'," + att3 + "," + att4 + ");");
                                }
                            }

                            if (cBackColorFirstCell != Color.GreenYellow && cBackColorFirstCell != Color.Orange)
                            {
                                tblSlot.Rows[iRow].Cells[iCell].BackColor = Color.LightGray;
                            }
                        }
                    }
                }

                string ReqStrVehicleDoorTypeMatrix = string.Empty;

                if (isStandardBookingRequired == true && iSKUSiteDoorID != 0)
                {
                    ReqStrVehicleDoorTypeMatrix = strVehicleDoorTypeMatrixSKU;
                }
                else
                {
                    ReqStrVehicleDoorTypeMatrix = strVehicleDoorTypeMatrix;
                }


                //--Blackout the slots which are not matched with the seleted vehicle type-------// 
                if (!ReqStrVehicleDoorTypeMatrix.Contains(lstDoorNoType[iCell - 1].DoorNo.SiteDoorTypeID.ToString() + ","))
                {
                    if ((tblSlot.Rows[iRow].Cells[iCell].BackColor == Color.White || tblSlot.Rows[iRow].Cells[iCell].BackColor == Color.LightGray) && tblSlot.Rows[iRow].Cells[iCell].BorderColor == Color.Red)
                    {
                        tblSlot.Rows[iRow].Cells[iCell].ForeColor = Color.Black;
                        tblSlot.Rows[iRow].Cells[iCell].BackColor = Color.LightYellow;
                    }

                    //if (tblSlot.Rows[iRow].Cells[iCell].BackColor == Color.GreenYellow) {
                    //    tblSlot.Rows[iRow].Cells[iCell].ForeColor = Color.White;
                    //}

                    //if (tblSlot.Rows[iRow].Cells[iCell].BackColor != Color.GreenYellow) {

                    //If SKU door matrix find the skip to blackout that type of door  SPRINT 4 POINT 7
                    if (!(isStandardBookingRequired == true && iSKUSiteDoorID != 0 && iSKUSiteDoorTypeID == lstDoorNoType[iCell - 1].DoorNo.SiteDoorTypeID))
                    {

                        if (tblSlot.Rows[iRow].Cells[iCell].BackColor == Color.White || tblSlot.Rows[iRow].Cells[iCell].BackColor == Color.LightGray)
                        {
                            tblSlot.Rows[iRow].Cells[iCell].BackColor = Color.Black;
                            tblSlot.Rows[iRow].Cells[iCell].BorderColor = Color.White;
                            //tblSlot.Rows[iRow].Cells[iCell].BorderWidth = 0;
                            tblSlot.Rows[iRow].Cells[iCell].Style.Add("border-top-width", "0px");  // .BorderWidth = 0;
                            tblSlot.Rows[iRow].Cells[iCell].Style.Add("border-bottom-width", "0px");
                            tblSlot.Rows[iRow].Cells[iCell].Style.Add("border-left-width", "1px");  // .BorderWidth = 0;
                            tblSlot.Rows[iRow].Cells[iCell].Style.Add("border-right-width", "1px");
                        }

                        if (hdnCurrentRole.Value == "Vendor")
                        {
                            tblSlot.Rows[iRow].Cells[iCell].Attributes.Add("onmouseover", "this.style.cursor = 'default'");
                            tblSlot.Rows[iRow].Cells[iCell].Attributes.Add("onmouseout", "this.style.cursor = 'default'");
                            tblSlot.Rows[iRow].Cells[iCell].Attributes.Add("onclick", "");
                        }
                    }

                }
                //----------------------------------------------------------------------------//

                #region Row Spaning
                //-----------------------Row Spaning-------------//

                cBackColor = tblSlot.Rows[iRow].Cells[iCell].BackColor;
                cBorderColor = tblSlot.Rows[iRow].Cells[iCell].BorderColor;

                if (cBackColor != cBackColorPreviousCell || cBorderColor != cBorderColorPreviousCell)
                {
                    isNewCell = true;
                }

                //if (cBorderColor == Color.Red)
                //    cBorderColor = tblSlot.Rows[iRow].Cells[iCell].BorderColor;

                //if (iRow == 5)
                //    cBorderColor = tblSlot.Rows[iRow].Cells[iCell].BorderColor;

                bool isCellCreated = false;
                if (cBackColor == cBackColorPreviousCell && cBackColor != Color.White)
                {

                    isCellCreated = true;
                    tblSlot.Rows[iRow].Cells[iCell].Style.Add("border-top-width", "0px");  // .BorderWidth = 0;
                    tblSlot.Rows[iRow].Cells[iCell].Style.Add("border-bottom-width", "0px");

                    if (cBorderColor == Color.Red || cBackColor == Color.Purple)
                    {
                        tblSlot.Rows[iRow].Cells[iCell].Style.Add("border-left-width", "2px");
                        tblSlot.Rows[iRow].Cells[iCell].Style.Add("border-right-width", "2px");
                    }
                    else
                    {
                        tblSlot.Rows[iRow].Cells[iCell].Style.Add("border-left-width", "1px");
                        tblSlot.Rows[iRow].Cells[iCell].Style.Add("border-right-width", "1px");
                    }

                    if (cBackColorPreviousCell == cBackColor && iRow > 0 && isNewCell == true)
                    {
                        isNewCell = false;
                        tblSlot.Rows[iRow - 1].Cells[iCell].Style.Add("border-bottom-width", "0px");

                        if (cBorderColorPreviousCell == Color.Red || cBackColorPreviousCell == Color.Purple)
                        {
                            tblSlot.Rows[iRow - 1].Cells[iCell].Style.Add("border-top-width", "2px");
                            tblSlot.Rows[iRow - 1].Cells[iCell].Style.Add("border-left-width", "2px");
                            tblSlot.Rows[iRow - 1].Cells[iCell].Style.Add("border-right-width", "2px");
                        }
                        else
                        {
                            tblSlot.Rows[iRow - 1].Cells[iCell].Style.Add("border-top-width", "1px");
                            tblSlot.Rows[iRow - 1].Cells[iCell].Style.Add("border-left-width", "1px");
                            tblSlot.Rows[iRow - 1].Cells[iCell].Style.Add("border-right-width", "1px");
                        }
                    }
                }
                else if (cBackColor == cBackColorPreviousCell && cBorderColor == Color.Red)
                {

                    isCellCreated = true;

                    tblSlot.Rows[iRow].Cells[iCell].Style.Add("border-top-width", "0px");  // .BorderWidth = 0;
                    tblSlot.Rows[iRow].Cells[iCell].Style.Add("border-bottom-width", "0px");

                    tblSlot.Rows[iRow].Cells[iCell].Style.Add("border-left-width", "2px");
                    tblSlot.Rows[iRow].Cells[iCell].Style.Add("border-right-width", "2px");


                    if (cBackColorPreviousCell == cBackColor && iRow > 0 && isNewCell == true)
                    {
                        isNewCell = false;

                        if (cBorderColorPreviousCell != Color.Red)
                            tblSlot.Rows[iRow].Cells[iCell].Style.Add("border-top-width", "2px");

                        tblSlot.Rows[iRow - 1].Cells[iCell].Style.Add("border-bottom-width", "0px");
                        if (cBorderColorPreviousCell == Color.Red || cBackColorPreviousCell == Color.Purple)
                        {
                            tblSlot.Rows[iRow - 1].Cells[iCell].Style.Add("border-top-width", "2px");
                            tblSlot.Rows[iRow - 1].Cells[iCell].Style.Add("border-left-width", "2px");
                            tblSlot.Rows[iRow - 1].Cells[iCell].Style.Add("border-right-width", "2px");
                        }
                        else
                        {
                            tblSlot.Rows[iRow - 1].Cells[iCell].Style.Add("border-top-width", "1px");
                            tblSlot.Rows[iRow - 1].Cells[iCell].Style.Add("border-left-width", "1px");
                            tblSlot.Rows[iRow - 1].Cells[iCell].Style.Add("border-right-width", "1px");
                        }
                    }
                }

                if (isCellCreated == false && iRow > 0)
                {
                    if (cBorderColorPreviousCell == Color.Red || cBackColorPreviousCell == Color.Purple)
                    {
                        tblSlot.Rows[iRow - 1].Cells[iCell].Style.Add("border-bottom-width", "2px");
                    }
                    else
                        tblSlot.Rows[iRow - 1].Cells[iCell].Style.Add("border-bottom-width", "1px");
                }

                abbr = tblSlot.Rows[iRow].Cells[iCell].Attributes["abbr"];
                if (abbr != null && abbr != string.Empty)
                {
                    if (cBorderColor == Color.Red || cBackColor == Color.Purple)
                    {
                        tblSlot.Rows[iRow].Cells[iCell].Style.Add("border-top-width", "2px");
                    }
                    else
                    {
                        tblSlot.Rows[iRow].Cells[iCell].Style.Add("border-top-width", "1px");
                    }
                }


                cBackColorPreviousCell = cBackColor;
                cBorderColorPreviousCell = cBorderColor;

                //--------------------------------------------------//
                #endregion
            }

        }

        //foreach (TableRow row in tblSlot.Rows) {            
        //        cl = row.Cells[iCell].BackColor;
        //        if (cl == Color.White) {
        //            //Find Next While Cell

        //        }
        //    }

        #endregion

        #region Standard Booking Required

        if (isStandardBookingRequired)
        {
            bool isSlotFind = false;
            string strCheckedHour = "-10";
            bool isProrityChecked = false;
            bool isMoveBack = false;
            int iCellNo = 1;
            string iLowestPriority = string.Empty;

            if (iSKUSiteDoorID == 0) //SPRINT 4 POINT 7
                iLowestPriority = GetLowestPriority(ArrDoorTypePriority);   // Function
            else
                iLowestPriority = GetLowestPriority(ArrDoorTypePrioritySKU);

            string iSuggestedPriority = "1";

            for (int iRow = 0; iRow < tblSlot.Rows.Count; iRow++)
            {
                isProrityChecked = false;
                isMoveBack = false;
                for (int iCell = 1; iCell <= iDoorCount; iCell++)
                {

                    if (iSKUSiteDoorID == 0 || lstDoorNoType[iCell - 1].SiteDoorNumberID == iSKUSiteDoorID)
                    {  //SPRINT 4 POINT 7
                        int iDoorTypeID = lstDoorNoType[iCell - 1].DoorType.DoorTypeID;

                        cBackColor = tblSlot.Rows[iRow].Cells[iCell].BackColor;
                        cBorderColor = tblSlot.Rows[iRow].Cells[iCell].BorderColor;

                        bool isCurrentVendorSlot = false;
                        abbr = tblSlot.Rows[iRow].Cells[iCell].Attributes["abbr"];
                        if ((abbr == null || abbr == string.Empty) && cBorderColor != Color.Red)
                        {
                            isCurrentVendorSlot = true;
                        }
                        else if (abbr != null && abbr != string.Empty && abbr.Contains("C"))
                        {
                            isCurrentVendorSlot = false;
                        }
                        else if (abbr != null && abbr != string.Empty && abbr.Contains("V"))
                        {
                            string[] arrabbr = abbr.Split('-');
                            if (arrabbr[1] != ucSeacrhVendorID)
                            {
                                isCurrentVendorSlot = false;
                            }
                        }


                        if (cBackColor == Color.White && isCurrentVendorSlot)
                        {
                            att = tblSlot.Rows[iRow].Cells[iCell].Attributes["onclick"];
                            if (att.Contains("CheckAletrnateSlot"))
                            {
                                att = att.Replace("CheckAletrnateSlot(", "");
                                att = att.Replace(");", "");
                                string[] Arratt = att.Split(',');
                                string att0 = Arratt[0].Replace("'", "");
                                string att1 = Arratt[1].Replace("'", "");
                                string att2 = Arratt[2].Replace("'", "");
                                string att3 = Arratt[3].Replace("'", "");
                                string att4 = Arratt[5].Replace("'", "");

                                //--------- Sprint 3b----------//
                                string att6 = Arratt[6].Replace("'", "");
                                //-----------------------------//

                                if (isMoveBack == false)
                                {
                                    //Check Before and after time//
                                    if (LogicalSchedulDateTime.ToString("ddd").ToLower() == att4.ToLower())
                                    {
                                        if (Convert.ToInt32(att6) < Convert.ToInt32(BeforeSlotTimeID) || Convert.ToInt32(att6) > Convert.ToInt32(AfterSlotTimeID))
                                        {
                                            continue;
                                        }
                                    }
                                    else
                                    {
                                        //if (Convert.ToInt32(BeforeSlotTimeID) == 0 || Convert.ToInt32(AfterSlotTimeID) == 100)
                                        continue;
                                    }
                                    //--------------------------//


                                    //Check delivery constraints//
                                    bool isValidTimeSlot = false;
                                    if (att3.Split(':')[0] != strCheckedHour)
                                        isValidTimeSlot = CheckValidSlot(att3, att4, Convert.ToString(TotalPallets), Convert.ToString(TotalLines));

                                    if (isValidTimeSlot == false)
                                    {
                                        strCheckedHour = att3.Split(':')[0];
                                        continue;
                                    }
                                    //------------------------------//

                                    //---------Check Door Priority---------//    

                                    string iPriority = string.Empty;

                                    if (isStandardBookingRequired == true && iSKUSiteDoorID != 0)
                                    {
                                        iPriority = ArrDoorTypePrioritySKU.Get(iDoorTypeID.ToString());
                                    }
                                    else
                                    {
                                        iPriority = ArrDoorTypePriority.Get(iDoorTypeID.ToString());
                                    }

                                    //if (iCell == 1)
                                    //    iPriority = "3";
                                    //if (iCell == 2)
                                    //    iPriority = "2";
                                    //if (iCell == 3)
                                    //    iPriority = "4";

                                    if (Convert.ToInt32(iPriority) != Convert.ToInt32(iLowestPriority))
                                    {
                                        if (isProrityChecked == false)
                                        {
                                            iCellNo = iCell;
                                            iSuggestedPriority = iPriority;
                                            isProrityChecked = true;
                                            continue;
                                        }
                                        else if (isProrityChecked == true)
                                        {
                                            if (Convert.ToInt32(iPriority) < Convert.ToInt32(iSuggestedPriority))
                                            {
                                                iCellNo = iCell;
                                                iSuggestedPriority = iPriority;
                                                isProrityChecked = true;
                                                continue;
                                            }
                                            else
                                            {
                                                if (iCell < iDoorCount)
                                                {
                                                    continue;
                                                }
                                                if (iCell == iDoorCount)
                                                {  //last cell
                                                    iCell = iCellNo - 1;
                                                    isMoveBack = true;
                                                    continue;
                                                }
                                            }
                                        }
                                    }
                                    //-------------------------------------//
                                }

                                DateTime dtBookingDate = LogicalSchedulDateTime;
                                if (LogicalSchedulDateTime.ToString("ddd").ToLower() != att4.ToLower())
                                {
                                    dtBookingDate = LogicalSchedulDateTime.AddDays(-1);
                                }

                                ltBookingDate.Text = dtBookingDate.Day.ToString() + "/" + dtBookingDate.Month.ToString() + "/" + dtBookingDate.Year.ToString();
                                ltSuggestedSlotTime.Text = att3 + " " + AtDoorName + " " + att2;
                                hdnSuggestedSlotTimeID.Value = att0;
                                hdnSuggestedFixedSlotID.Value = "-1";
                                hdnSuggestedSiteDoorNumberID.Value = att1;
                                hdnSuggestedSiteDoorNumber.Value = att2;

                                hdnSuggestedSlotTime.Value = att3;
                                ltTimeSlot.Text = hdnSuggestedSlotTime.Value;

                                ltDoorNo.Text = hdnSuggestedSiteDoorNumber.Value;
                                ltTimeSlot_1.Text = att3;
                                ltDoorNo_1.Text = hdnSuggestedSiteDoorNumber.Value;
                                hdnSuggestedWeekDay.Value = att4;

                                btnConfirmBooking.Visible = true;
                                lblProposeBookingInfo.Visible = true;
                                isSlotFind = true;
                            }
                        }

                        //Last Cell ,still No slot found but a slot alrady found with lowest priority , go for that
                        if (iCell == iDoorCount && isSlotFind == false && isProrityChecked == true)
                        {
                            iCell = iCellNo - 1;
                            isMoveBack = true;
                            continue;
                        }
                    } //SPRINT 4 POINT 7

                    if (isSlotFind)
                        break;
                }
                if (isSlotFind)
                    break;
            }
        }
        #endregion

        #region Set Background color for Fixed slot
        for (int iRow = 0; iRow < tblSlot.Rows.Count; iRow++)
        {
            for (int iCell = 1; iCell <= iDoorCount; iCell++)
            {
                cBackColor = tblSlot.Rows[iRow].Cells[iCell].BackColor;
                cBorderColor = tblSlot.Rows[iRow].Cells[iCell].BorderColor;

                if (cBorderColor == Color.Red && (cBackColor == Color.White || cBackColor == Color.LightGray))
                {
                    tblSlot.Rows[iRow].Cells[iCell].BackColor = Color.LightYellow;
                }
            }
        }
        #endregion

        hdnGraySlotClicked.Value = "false";
    }

    private string GetDoorConstraints()
    {
        string OpenDoorIds = string.Empty;

        //if (txtSchedulingdate.innerControltxtDate.Value != string.Empty && ddlSite.innerControlddlSite.SelectedItem.Value != string.Empty)
        if (txtSchedulingdate.innerControltxtDate.Value != string.Empty && Convert.ToString(intSiteId) != string.Empty)
        {

            bool IsSpecificWeekDefined = false;

            DateTime dtSelectedDate = LogicalSchedulDateTime;
            string WeekDay = dtSelectedDate.ToString("dddd");
            DateTime dtMon = Utilities.Common.GetFirstDayOfWeek(dtSelectedDate);

            MASSIT_WeekSetupBE oMASSIT_WeekSetupBE = new MASSIT_WeekSetupBE();
            MASSIT_WeekSetupBAL oMASSIT_WeekSetupBAL = new MASSIT_WeekSetupBAL();

            oMASSIT_WeekSetupBE.Action = "GetSpecificWeek";
            oMASSIT_WeekSetupBE.SiteID = Convert.ToInt32(intSiteId); //Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
            oMASSIT_WeekSetupBE.EndWeekday = WeekDay;
            oMASSIT_WeekSetupBE.ScheduleDate = dtSelectedDate;

            List<MASSIT_WeekSetupBE> lstWeekSetup = oMASSIT_WeekSetupBAL.GetWeekSetupDetailsBAL(oMASSIT_WeekSetupBE);

            if (lstWeekSetup != null && lstWeekSetup.Count > 0 && lstWeekSetup[0].StartTime != null)
            {
                IsSpecificWeekDefined = true;
            }


            //------Check for Specific Entry First--------//
            APPSIT_DoorOpenTimeBAL oMASSIT_DoorOpenTimeBAL = new APPSIT_DoorOpenTimeBAL();
            MASSIT_DoorOpenTimeBE oMASSIT_DoorOpenTimeBE = new MASSIT_DoorOpenTimeBE();

            APPSIT_DoorOpenTimeSpecificBAL oAPPSIT_DoorOpenTimeSpecificBAL = new APPSIT_DoorOpenTimeSpecificBAL();
            MASSIT_DoorOpenTimeSpecificBE oMASSIT_DoorOpenTimeSpecificBE = new MASSIT_DoorOpenTimeSpecificBE();
            oMASSIT_DoorOpenTimeSpecificBE.Action = "GetDoorConstraintsSpecific";
            oMASSIT_DoorOpenTimeSpecificBE.DoorOpenTime = new MASSIT_DoorOpenTimeBE();
            oMASSIT_DoorOpenTimeSpecificBE.DoorOpenTime.DoorNo = new MASSIT_DoorNoSetupBE();
            oMASSIT_DoorOpenTimeSpecificBE.DoorOpenTime.DoorType = new BusinessEntities.ModuleBE.Appointment.CountrySetting.MASCNT_DoorTypeBE();
            oMASSIT_DoorOpenTimeSpecificBE.DoorOpenTime.DoorNo.SiteID = Convert.ToInt32(intSiteId); //Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
            oMASSIT_DoorOpenTimeSpecificBE.DoorOpenTime.Weekday = WeekDay;
            oMASSIT_DoorOpenTimeSpecificBE.WeekStartDate = dtMon;
            List<MASSIT_DoorOpenTimeSpecificBE> lstDoorConstraintsSpecific = oAPPSIT_DoorOpenTimeSpecificBAL.GetDoorConstraintsSpecificBAL(oMASSIT_DoorOpenTimeSpecificBE);
            if (lstDoorConstraintsSpecific.Count > 0 && IsSpecificWeekDefined == true)
            { //Get Specific settings 
                for (int iCount = 0; iCount < lstDoorConstraintsSpecific.Count; iCount++)
                {
                    OpenDoorIds += lstDoorConstraintsSpecific[iCount].DoorOpenTime.SlotTimeID + "@" + lstDoorConstraintsSpecific[iCount].DoorOpenTime.SiteDoorNumberID + ",";
                }
            }
            else
            {
                if (IsSpecificWeekDefined == false)
                {
                    //Get Generic settings 
                    oMASSIT_DoorOpenTimeBE.Action = "GetDoorConstraints";
                    oMASSIT_DoorOpenTimeBE.DoorNo = new MASSIT_DoorNoSetupBE();
                    oMASSIT_DoorOpenTimeBE.DoorNo.SiteID = Convert.ToInt32(intSiteId); //Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
                    oMASSIT_DoorOpenTimeBE.Weekday = WeekDay;
                    List<MASSIT_DoorOpenTimeBE> lstDoorConstraints = oMASSIT_DoorOpenTimeBAL.GetDoorConstraintsBAL(oMASSIT_DoorOpenTimeBE);
                    for (int iCount = 0; iCount < lstDoorConstraints.Count; iCount++)
                    {
                        OpenDoorIds += lstDoorConstraints[iCount].SlotTimeID + "@" + lstDoorConstraints[iCount].SiteDoorNumberID + ",";

                    }
                }
            }
        }

        return OpenDoorIds;
    }

    private void GetOutstandingPO()
    {


        ViewState["dtOutstandingPO"] = null;

        Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE = new Up_PurchaseOrderDetailBE();
        UP_PurchaseOrderDetailBAL oUP_PurchaseOrderDetailBAL = new UP_PurchaseOrderDetailBAL();

        oUp_PurchaseOrderDetailBE.Action = "GetRemainingBookingPODetails";
        oUp_PurchaseOrderDetailBE.Vendor = new UP_VendorBE();
        oUp_PurchaseOrderDetailBE.Vendor.VendorID = Convert.ToInt32(ucSeacrhVendorID);
        oUp_PurchaseOrderDetailBE.Site = new MAS_SiteBE();
        oUp_PurchaseOrderDetailBE.Site.SiteID = Convert.ToInt32(intSiteId); //Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
        oUp_PurchaseOrderDetailBE.SelectedProductCodes = sAddedPurchaseOrders.Substring(0, sAddedPurchaseOrders.Length - 1);

        List<Up_PurchaseOrderDetailBE> lstPO = oUP_PurchaseOrderDetailBAL.GetRemainingBookingPODetails(oUp_PurchaseOrderDetailBE);

        DateTime ScheduledDate = LogicalSchedulDateTime;

        if (lstPO != null && lstPO.Count > 0)
        {
            // 1. Filter out the seleted purchase orders AS PER THE SITE SETTINGS

            lstPO = lstPO.FindAll(delegate(Up_PurchaseOrderDetailBE PO)
            {
                return PO.Original_due_date <= ScheduledDate;
            });

            //if (lstPO[0].Site.ToleranceDueDay == 0) {
            //    lstPO = lstPO.FindAll(delegate(Up_PurchaseOrderDetailBE PO) {
            //        return PO.Original_due_date <= ScheduledDate;
            //    }
            //    );
            //}
            //else if (lstPO[0].Site.ToleranceDueDay > 0) {
            //    lstPO = lstPO.FindAll(delegate(Up_PurchaseOrderDetailBE PO) {
            //        return PO.Original_due_date <= ScheduledDate.AddDays(Convert.ToDouble(lstPO[0].Site.ToleranceDueDay));
            //    }
            //    );
            //}

            Filter(lstPO);

            DataTable dtOutstandingPO = null;

            if (ViewState["dtOutstandingPO"] != null)
            {
                dtOutstandingPO = (DataTable)ViewState["dtOutstandingPO"];
            }

            if (dtOutstandingPO != null && dtOutstandingPO.Rows.Count > 0 && Convert.ToBoolean(ViewState["IsShowPOPopup"]) == false)
            {
                gvOutstandingPO.DataSource = dtOutstandingPO;
                gvOutstandingPO.DataBind();
                mdlOutstandingPO.Show();
            }
            else
            {
                //ProceedBooking();
                CalculateProposedBookingTimeSlot();
                mvBookingEdit.ActiveViewIndex = 3;
            }
        }
        else
        {
            //ProceedBooking();
            CalculateProposedBookingTimeSlot();
            mvBookingEdit.ActiveViewIndex = 3;
        }
    }

    private void Filter(List<Up_PurchaseOrderDetailBE> lstPOLocal)
    {
        if (lstPOLocal.Count > 0)
        {
            DateTime? Original_due_date;
            int OutStandingLines;
            string Purchase_order;

            Original_due_date = lstPOLocal[0].Original_due_date;
            Purchase_order = lstPOLocal[0].Purchase_order;

            List<Up_PurchaseOrderDetailBE> lstPOTemp = lstPOLocal.FindAll(delegate(Up_PurchaseOrderDetailBE PO)
            {
                return PO.Original_due_date == Original_due_date && PO.Purchase_order == Purchase_order;
            }
            );

            OutStandingLines = lstPOTemp.Count;

            lstPOLocal = lstPOLocal.FindAll(delegate(Up_PurchaseOrderDetailBE PO)
            {
                return PO.Purchase_order != Purchase_order;
            }
            );

            //Add to Temp Table
            DataTable dtOutstandingPO = null;

            if (ViewState["dtOutstandingPO"] != null)
            {
                dtOutstandingPO = (DataTable)ViewState["dtOutstandingPO"];
            }
            else
            {

                dtOutstandingPO = new DataTable();


                DataColumn myDataColumn = new DataColumn();
                myDataColumn.ColumnName = "PurchaseNumber";
                dtOutstandingPO.Columns.Add(myDataColumn);

                myDataColumn = new DataColumn();
                myDataColumn.ColumnName = "Original_due_date";
                dtOutstandingPO.Columns.Add(myDataColumn);


                myDataColumn = new DataColumn();
                myDataColumn.ColumnName = "OutstandingLines";
                dtOutstandingPO.Columns.Add(myDataColumn);

            }

            DataRow dataRow = dtOutstandingPO.NewRow();

            dataRow["PurchaseNumber"] = Purchase_order;
            dataRow["Original_due_date"] = Original_due_date.Value.ToString("dd/MM/yyyy");
            dataRow["OutstandingLines"] = OutStandingLines;

            dtOutstandingPO.Rows.Add(dataRow);

            if (dtOutstandingPO != null && dtOutstandingPO.Rows.Count > 0)
            {
                ViewState["dtOutstandingPO"] = dtOutstandingPO;
            }
            else
            {
                ViewState["dtOutstandingPO"] = null;
            }

            Filter(lstPOLocal);
        }
    }

    private void CalculateProposedBookingTimeSlot()
    {  //Find out unloading time for slot suggestion
        //string TotalPallets = txtPallets.Text.Trim();
        //string TotalCartons = txtCartons.Text.Trim();
        //string TotalLifts = txtLifts.Text.Trim();
        //string TotalLines = txtExpectedLines.Text.Trim();


        //----Start Non Time Delivary ------------//

        if (hdnCurrentRole.Value == "Vendor")
        { // When vendor is loggedin

            DeliverTypenontime();
            IsNonTime = false;

            if (!Convert.ToBoolean(IsNonTimeVendor))
            {  //Check Non Time delivary for vendor

                if (!strDeliveryNonTime.Contains(ddlDeliveryType.SelectedItem.Value + ","))
                {  //Check Non Time delivary for Delivery Type

                    bool IsPalletVolumeMatch = false;
                    bool IsCartonVolumeMatch = false;

                    if (txtPalletsT.Text.Trim() != string.Empty)
                    {  //Check non time pallets 
                        if (NonTimeDeliveryPalletVolume > Convert.ToInt32(txtPalletsT.Text.Trim()))
                        {
                            //IsNonTime = true;
                            IsPalletVolumeMatch = true;
                        }
                    }
                    else
                    {
                        IsPalletVolumeMatch = true;
                    }

                    if (txtCartonsT.Text.Trim() != string.Empty)
                    {  //Check non time cartons 
                        if (NonTimeDeliveryCartonVolume > Convert.ToInt32(txtCartonsT.Text.Trim()))
                        {
                            //IsNonTime = true;
                            IsCartonVolumeMatch = true;
                        }
                    }
                    else
                    {
                        IsCartonVolumeMatch = true;
                    }

                    if (IsPalletVolumeMatch && IsCartonVolumeMatch)
                    {
                        IsNonTime = true;
                    }
                }
                else
                {
                    IsNonTime = true;
                }
            }
            else
            {
                IsNonTime = true;
            }
        }
        else
        {  // When OD staff is loggedin
            IsNonTime = true;
        }
        //--------End of Non Time Delivary-------------------------//     

        BindFixedSlot();
        btnMakeNonTimeBooking.Visible = Convert.ToBoolean(IsNonTime);
    }

    private void DeliverTypenontime()
    {
        DataTable dt1;

        MASSIT_NonTimeDeliveryBE oMASCNT_NonTimeDeliveryBE = new MASSIT_NonTimeDeliveryBE();
        APPSIT_NonTimeDeliveryBAL oMASCNT_NonTimeDeliveryBAL = new APPSIT_NonTimeDeliveryBAL();

        oMASCNT_NonTimeDeliveryBE.SiteID = Convert.ToInt32(intSiteId); //Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
        oMASCNT_NonTimeDeliveryBE.Action = "GetDeliveries";
        dt1 = oMASCNT_NonTimeDeliveryBAL.GetNonTimeDeliveryDetailsBAL(oMASCNT_NonTimeDeliveryBE, "");

        strDeliveryNonTime = string.Empty;

        for (int iCount = 0; iCount < dt1.Rows.Count; iCount++)
        {
            strDeliveryNonTime += dt1.Rows[iCount]["SiteDeliveryID"].ToString() + ",";
        }
    }

    protected void BindFixedSlot()
    {

        string TotalPallets = txtPalletsT.Text.Trim() != string.Empty ? txtPalletsT.Text.Trim() : "0";
        string TotalCartons = txtCartonsT.Text.Trim() != string.Empty ? txtCartonsT.Text.Trim() : "0";
        string TotalLifts = txtLiftsT.Text.Trim() != string.Empty ? txtLiftsT.Text.Trim() : "0";
        string TotalLines = txtLinesT.Text.Trim() != string.Empty ? txtLinesT.Text.Trim() : "0";
        //string TotalLines = txtExpectedLines.Text.Trim() != string.Empty ? txtExpectedLines.Text.Trim() : "0";

        //-----------------Start Collect Basic Information --------------------------//
        DateTime dtSelectedDate = LogicalSchedulDateTime;
        string WeekDay = dtSelectedDate.ToString("dddd");

        //DateTime dtMon = Utilities.Common.GetFirstDayOfWeek(dtSelectedDate);

        MASSIT_WeekSetupBE oMASSIT_WeekSetupBE = new MASSIT_WeekSetupBE();
        MASSIT_WeekSetupBAL oMASSIT_WeekSetupBAL = new MASSIT_WeekSetupBAL();

        oMASSIT_WeekSetupBE.Action = "GetSpecificWeek";
        oMASSIT_WeekSetupBE.SiteID = Convert.ToInt32(intSiteId); //Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
        oMASSIT_WeekSetupBE.EndWeekday = WeekDay;
        oMASSIT_WeekSetupBE.ScheduleDate = dtSelectedDate;

        List<MASSIT_WeekSetupBE> lstWeekSetup = oMASSIT_WeekSetupBAL.GetWeekSetupDetailsBAL(oMASSIT_WeekSetupBE);

        if (lstWeekSetup == null || lstWeekSetup.Count <= 0 || lstWeekSetup[0].StartTime == null)
        {

            oMASSIT_WeekSetupBE.Action = "ShowAll";
            lstWeekSetup = oMASSIT_WeekSetupBAL.GetWeekSetupDetailsBAL(oMASSIT_WeekSetupBE);
        }
        //----------------------------------------------------------------------------//

        APPBOK_BookingBE oAPPBOK_BookingBE = new APPBOK_BookingBE();
        APPBOK_BookingBAL oAPPBOK_BookingBAL = new APPBOK_BookingBAL();
        oAPPBOK_BookingBE.Carrier = new MASCNT_CarrierBE();

        oAPPBOK_BookingBE.Action = "VendorBookingFixedSlotDetails_Test";
        oAPPBOK_BookingBE.SiteId = Convert.ToInt32(intSiteId); //Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
        oAPPBOK_BookingBE.VendorID = Convert.ToInt32(ucSeacrhVendorID); // Convert.ToInt32(ucSeacrhVendor1.VendorNo);
        oAPPBOK_BookingBE.ScheduleDate = LogicalSchedulDateTime;
        oAPPBOK_BookingBE.SelectedScheduleDate = LogicalSchedulDateTime;
        List<APPBOK_BookingBE> lstBookings = oAPPBOK_BookingBAL.GetVendorBookingFixedSlotDetailsBAL(oAPPBOK_BookingBE);

        //Filter out fixed slot not in timeslot range
        lstBookings = lstBookings.FindAll(delegate(APPBOK_BookingBE St)
        {
            DateTime? dt = (DateTime?)null;
            if (St.SlotTime.SlotTime != null && St.SlotTime.SlotTime != string.Empty)
            {
                dt = new DateTime(1900, 1, 1, Convert.ToInt32(St.SlotTime.SlotTime.Split(':')[0]), Convert.ToInt32(St.SlotTime.SlotTime.Split(':')[1]), 0);
            }
            else
            {
                dt = new DateTime(1900, 1, 1, 0, 0, 0);
            }
            //if (St.BookingRef == "Fixed booking")
            //    return dt <= lstWeekSetup[0].EndTime;
            //else
            //    return true;
            return dt <= lstWeekSetup[0].EndTime;
        });


        if (lstWeekSetup.Count > 0 && lstWeekSetup[0].StartTime != null)
        {

            if (lstWeekSetup[0].StartWeekday != lstWeekSetup[0].EndWeekday)
            {
                oAPPBOK_BookingBE.SelectedScheduleDate = LogicalSchedulDateTime.AddDays(-1);  //VendorBookingFixedSlotDetails

                List<APPBOK_BookingBE> lstBookingsStartDay = oAPPBOK_BookingBAL.GetVendorBookingFixedSlotDetailsBAL(oAPPBOK_BookingBE);

                lstBookingsStartDay = lstBookingsStartDay.FindAll(delegate(APPBOK_BookingBE St)
                {
                    DateTime? dt = (DateTime?)null;
                    if (St.SlotTime.SlotTime != null && St.SlotTime.SlotTime != string.Empty)
                    {
                        dt = new DateTime(1900, 1, 1, Convert.ToInt32(St.SlotTime.SlotTime.Split(':')[0]), Convert.ToInt32(St.SlotTime.SlotTime.Split(':')[1]), 0);
                    }
                    else
                    {
                        dt = new DateTime(1900, 1, 1, 0, 0, 0);
                    }

                    //if (St.BookingRef == "Fixed booking")
                    //    return dt >= lstWeekSetup[0].StartTime;
                    //else
                    //    return false;

                    return dt >= lstWeekSetup[0].StartTime;
                });

                if (lstBookingsStartDay != null && lstBookingsStartDay.Count > 0)
                {
                    lstBookings = MergeListCollections(lstBookingsStartDay, lstBookings);
                }
            }
        }

        //-----------------Get top fisrt unused fixed slot to be remonded-------------//
        bool bUnconfirmedFixedSlotFound = false;
        List<APPBOK_BookingBE> lstUnuseedFixedSlot = lstBookings.FindAll(delegate(APPBOK_BookingBE St) { return St.BookingRef == "Fixed booking"; });
        if (lstUnuseedFixedSlot != null && lstUnuseedFixedSlot.Count > 0)
        {
            bUnconfirmedFixedSlotFound = true;
        }
        //----------------------------------------------------------------------------//

        //-------IF NO UNCONFIRMED FIXED SLOT NOT FOUND , CHECK THE SKU/DOOR MATRIX - SPRINT 4 POINT 7
        bool bSKUMatrixDoorFound = false;
        int iSiteDoorNumberID = 0;
        int iSKUDoorTypeID = 0;
        if (bUnconfirmedFixedSlotFound == false)
        {
            DataTable myDataTable = null;
            if (ViewState["myDataTable"] != null)
            {
                myDataTable = (DataTable)ViewState["myDataTable"];

                var column2Values = myDataTable.AsEnumerable().Select(x => x.Field<string>("PurchaseNumber"));
                string PurchaseNumbers = String.Join(",", column2Values.ToArray());
                if (!string.IsNullOrEmpty(PurchaseNumbers))
                {

                    APPSIT_DoorOpenTimeBAL oMASSIT_DoorOpenTimeBAL = new APPSIT_DoorOpenTimeBAL();
                    MASSIT_DoorOpenTimeBE oMASSIT_DoorOpenTimeBE = new MASSIT_DoorOpenTimeBE();

                    oMASSIT_DoorOpenTimeBE.Action = "GetSKUMatrixDoorDetails";
                    oMASSIT_DoorOpenTimeBE.DoorNo = new MASSIT_DoorNoSetupBE();
                    oMASSIT_DoorOpenTimeBE.DoorNo.SiteID = Convert.ToInt32(intSiteId); //Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
                    oMASSIT_DoorOpenTimeBE.PurchaseNumbers = PurchaseNumbers;


                    List<MASSIT_DoorOpenTimeBE> lstSKUMatrixDoorDetails = oMASSIT_DoorOpenTimeBAL.GetDoorNoSetupDetailsBAL(oMASSIT_DoorOpenTimeBE);

                    if (lstSKUMatrixDoorDetails != null && lstSKUMatrixDoorDetails.Count > 0)
                    {
                        bSKUMatrixDoorFound = true;
                        iSiteDoorNumberID = lstSKUMatrixDoorDetails[0].SiteDoorNumberID;
                        iSKUDoorTypeID = lstSKUMatrixDoorDetails[0].DoorType.DoorTypeID;
                        if (lstSKUMatrixDoorDetails.Count > 1)
                        {  //there is a conflict i.e the PO's contain SKU with different doors 
                            isProvisional = true;
                        }
                    }
                }
            }
        }
        //------------------------------------------------------------------------//

        oAPPBOK_BookingBE.Action = "VendorBookingFindFixedSlotDoor";
        oAPPBOK_BookingBE.SiteId = Convert.ToInt32(intSiteId); //Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
        oAPPBOK_BookingBE.VendorID = Convert.ToInt32(ucSeacrhVendorID); // Convert.ToInt32(ucSeacrhVendor1.VendorNo);     

        oAPPBOK_BookingBE.ScheduleDate = LogicalSchedulDateTime;
        oAPPBOK_BookingBE.VehicleType = new MASSIT_VehicleTypeBE();
        if (bUnconfirmedFixedSlotFound)
            oAPPBOK_BookingBE.VehicleType.VehicleTypeID = 0;
        else
            oAPPBOK_BookingBE.VehicleType.VehicleTypeID = Convert.ToInt32(ddlVehicleType.SelectedItem.Value);
        List<APPBOK_BookingBE> lstFixedDoor = oAPPBOK_BookingBAL.GetVendorBookingFixedSlotDoorBAL(oAPPBOK_BookingBE);

        if ((hdnSuggestedFixedSlotID.Value == "" || hdnSuggestedFixedSlotID.Value == "-1") && hdnBookingRef.Value == "")
        {
            if (lstFixedDoor.Count > 0)
            {
                if (lstFixedDoor.FindAll(delegate(APPBOK_BookingBE St) { return St.VehicleType.VehicleTypeID == Convert.ToInt32(ddlVehicleType.SelectedItem.Value); }).Count <= 0)
                {
                    isProvisional = true;
                }
            }
        }

        lstFixedDoor = lstFixedDoor.FindAll(delegate(APPBOK_BookingBE St)
        {
            DateTime? dt = (DateTime?)null;
            if (St.SlotTime.SlotTime != null && St.SlotTime.SlotTime != string.Empty)
            {
                dt = new DateTime(1900, 1, 1, Convert.ToInt32(St.SlotTime.SlotTime.Split(':')[0]), Convert.ToInt32(St.SlotTime.SlotTime.Split(':')[1]), 0);
            }
            else
            {
                dt = new DateTime(1900, 1, 1, 0, 0, 0);
            }
            //DateTime dt = new DateTime(1900, 1, 1, Convert.ToInt32(St.SlotTime.SlotTime.Split(':')[0]), Convert.ToInt32(St.SlotTime.SlotTime.Split(':')[1]), 0);
            return dt <= lstWeekSetup[0].EndTime;
        });

        if (lstWeekSetup.Count > 0 && lstWeekSetup[0].StartTime != null)
        {
            if (lstWeekSetup[0].StartWeekday != lstWeekSetup[0].EndWeekday)
            {
                oAPPBOK_BookingBE.ScheduleDate = LogicalSchedulDateTime.AddDays(-1);
                List<APPBOK_BookingBE> lstFixedDoorStartDay = oAPPBOK_BookingBAL.GetVendorBookingFixedSlotDoorBAL(oAPPBOK_BookingBE);

                lstFixedDoorStartDay = lstFixedDoorStartDay.FindAll(delegate(APPBOK_BookingBE St)
                {
                    DateTime? dt = (DateTime?)null;
                    if (St.SlotTime.SlotTime != null && St.SlotTime.SlotTime != string.Empty)
                    {
                        dt = new DateTime(1900, 1, 1, Convert.ToInt32(St.SlotTime.SlotTime.Split(':')[0]), Convert.ToInt32(St.SlotTime.SlotTime.Split(':')[1]), 0);
                    }
                    else
                    {
                        dt = new DateTime(1900, 1, 1, 0, 0, 0);
                    }
                    //DateTime dt = new DateTime(1900, 1, 1, Convert.ToInt32(St.SlotTime.SlotTime.Split(':')[0]), Convert.ToInt32(St.SlotTime.SlotTime.Split(':')[1]), 0);
                    return dt >= lstWeekSetup[0].StartTime;
                });

                if (lstFixedDoorStartDay != null && lstFixedDoorStartDay.Count > 0)
                {
                    lstFixedDoor = MergeListCollections(lstFixedDoor, lstFixedDoorStartDay);
                }
            }
        }




        List<APPBOK_BookingBE> lstAvailableFixedDoorSlots = lstFixedDoor;


        //filter Fixed Slots on the basis of Fixed Slot Setup's Volume MaximumCatrons capacity 
        lstAvailableFixedDoorSlots = lstFixedDoor.FindAll(delegate(APPBOK_BookingBE vol)
        {
            if (vol.FixedSlot.MaximumCatrons > 0)
            {
                if (vol.FixedSlot.MaximumCatrons >= Convert.ToInt32(TotalCartons))
                {
                    // We just found fixed slot. Capture it.
                    return true;
                }
                else
                {
                    // Not a fixed slot
                    return false;
                }
            }
            else
            {
                return true;
            }
        });

        //filter Fixed Slots on the basis of Fixed Slot Setup's Volume MaximumLines capacity
        lstAvailableFixedDoorSlots = lstAvailableFixedDoorSlots.FindAll(delegate(APPBOK_BookingBE vol)
        {
            if (vol.FixedSlot.MaximumLines > 0)
            {
                if (vol.FixedSlot.MaximumLines >= Convert.ToInt32(TotalLines))
                {
                    // We just found fixed slot. Capture it.
                    return true;
                }
                else
                {
                    // Not a fixed slot
                    return false;
                }
            }
            else
            {
                return true;
            }
        });

        //filter Fixed Slots on the basis of Fixed Slot Setup's Volume MaximumPallets capacity
        lstAvailableFixedDoorSlots = lstAvailableFixedDoorSlots.FindAll(delegate(APPBOK_BookingBE vol)
        {
            if (vol.FixedSlot.MaximumPallets > 0)
            {
                if (vol.FixedSlot.MaximumPallets >= Convert.ToInt32(TotalPallets))
                {
                    // We just found fixed slot. Capture it.
                    return true;
                }
                else
                {
                    // Not a fixed slot
                    return false;
                }
            }
            else
            {
                return true;
            }
        });

        //------------------------------//



        // Not a fixed slot        
        if (lstFixedDoor.Count > 0 && lstAvailableFixedDoorSlots.Count <= 0)
        {
            lstAvailableFixedDoorSlots = lstFixedDoor;
            isProvisional = true;
            if (bUnconfirmedFixedSlotFound == false)
                hdnCapacityWarning.Value = "true";
        }
        else
        {
            if (hdnSuggestedFixedSlotID.Value != "" && hdnSuggestedFixedSlotID.Value != "-1")
            {
                if (lstAvailableFixedDoorSlots.FindAll(delegate(APPBOK_BookingBE St) { return St.FixedSlot.FixedSlotID == Convert.ToInt32(hdnSuggestedFixedSlotID.Value); }).Count <= 0)
                {
                    isProvisional = true;
                }
            }
        }

        //------Check for Specific Entry First--------//    

        DateTime dtMon = Utilities.Common.GetFirstDayOfWeek(dtSelectedDate);
        string StartWeekday = dtSelectedDate.ToString("dddd");
        if (lstWeekSetup.Count > 0 && lstWeekSetup[0].StartTime != null)
        {
            if (lstWeekSetup[0].StartWeekday != lstWeekSetup[0].EndWeekday)
            {
                StartWeekday = dtSelectedDate.AddDays(-1).ToString("dddd");
            }
        }

        string SlotIDs = string.Empty;

        SlotIDs = GetDoorConstraints();

        if (bUnconfirmedFixedSlotFound == false)
            lstBookings = lstBookings.FindAll(delegate(APPBOK_BookingBE St) { return SlotIDs.Contains(St.SlotTime.SlotTimeID + "@"); });

        //------------------------------------------------------//

        //FILETR OUT NON TIME DELIVERY
        lstBookings = lstBookings.FindAll(delegate(APPBOK_BookingBE St) { return St.SlotTime.SlotTime != ""; });

        if (lstBookings.Count > 0)
        {
            gvBookingSlots.DataSource = lstBookings;
            gvBookingSlots.DataBind();
        }

        //filter Booked Slots which are alrady booked
        List<APPBOK_BookingBE> lstAvailableSlots = new List<APPBOK_BookingBE>();
        if (hdnCurrentMode.Value == "Add")
        {
            lstAvailableSlots = lstBookings.FindAll(delegate(APPBOK_BookingBE St) { return St.Status != "Booked"; });
        }
        else if (hdnCurrentMode.Value == "Edit")
        {
            lstAvailableSlots = lstBookings;
        }

        //find a specific fixed slot for suggestion
        List<APPBOK_BookingBE> ObjAPPBOK_BookingBE = null;
        bool isFixedSlotFind = false;

        if ((hdnSuggestedFixedSlotID.Value == "" || hdnSuggestedFixedSlotID.Value == "-1") && hdnBookingRef.Value == "")
        {
            for (int iCount = 0; iCount < lstAvailableSlots.Count; iCount++)
            {
                ObjAPPBOK_BookingBE = lstAvailableFixedDoorSlots.FindAll(delegate(APPBOK_BookingBE p) { return p.FixedSlot.FixedSlotID == lstAvailableSlots[iCount].FixedSlot.FixedSlotID; });
                if (ObjAPPBOK_BookingBE.Count > 0)
                {

                    //Check delivery constraints                    
                    bool isValidTimeSlot = CheckValidSlot(ObjAPPBOK_BookingBE[0].SlotTime.SlotTime, ObjAPPBOK_BookingBE[0].ScheduleDate.Value.ToString("ddd"), TotalPallets, TotalLines);

                    if (bUnconfirmedFixedSlotFound)
                        isValidTimeSlot = true;

                    if (isValidTimeSlot == false)
                    {
                        continue;
                    }
                    else
                    {
                        isFixedSlotFind = true;
                        ltBookingDate.Text = ObjAPPBOK_BookingBE[0].ScheduleDate.Value.Day.ToString() + "/" + ObjAPPBOK_BookingBE[0].ScheduleDate.Value.Month.ToString() + "/" + ObjAPPBOK_BookingBE[0].ScheduleDate.Value.Year.ToString();
                        ltSuggestedSlotTime.Text = ObjAPPBOK_BookingBE[0].SlotTime.SlotTime + " " + AtDoorName + " " + ObjAPPBOK_BookingBE[0].DoorNoSetup.DoorNumber.ToString();
                        hdnSuggestedSlotTimeID.Value = ObjAPPBOK_BookingBE[0].SlotTime.SlotTimeID.ToString();
                        hdnSuggestedFixedSlotID.Value = ObjAPPBOK_BookingBE[0].FixedSlot.FixedSlotID.ToString();
                        hdnSuggestedSiteDoorNumberID.Value = ObjAPPBOK_BookingBE[0].DoorNoSetup.SiteDoorNumberID.ToString();
                        hdnSuggestedSiteDoorNumber.Value = ObjAPPBOK_BookingBE[0].DoorNoSetup.DoorNumber.ToString();
                        hdnSuggestedSlotTime.Value = ObjAPPBOK_BookingBE[0].SlotTime.SlotTime;
                        ltTimeSlot.Text = hdnSuggestedSlotTime.Value;
                        ltDoorNo.Text = hdnSuggestedSiteDoorNumber.Value;
                        ltTimeSlot_1.Text = ObjAPPBOK_BookingBE[0].SlotTime.SlotTime;
                        ltDoorNo_1.Text = hdnSuggestedSiteDoorNumber.Value;
                        hdnSuggestedWeekDay.Value = ObjAPPBOK_BookingBE[0].ScheduleDate.Value.ToString("ddd");

                        //Check Volume Details//
                        int fixedslotlength = GetRequiredTimeSlotLength(ObjAPPBOK_BookingBE[0].FixedSlot.MaximumPallets, ObjAPPBOK_BookingBE[0].FixedSlot.MaximumCatrons);
                        int BookingLength = GetRequiredTimeSlotLength(Convert.ToInt32(TotalPallets), Convert.ToInt32(TotalCartons));
                        if (fixedslotlength < BookingLength)
                        {
                            isProvisional = true;
                        }
                        else
                        {

                            if (lstAvailableFixedDoorSlots.FindAll(delegate(APPBOK_BookingBE St) { return St.FixedSlot.FixedSlotID == Convert.ToInt32(ObjAPPBOK_BookingBE[0].FixedSlot.FixedSlotID) && St.VehicleType.VehicleTypeID == Convert.ToInt32(ddlVehicleType.SelectedItem.Value); }).Count <= 0)
                            {
                                isProvisional = true;
                            }

                            //if (ObjAPPBOK_BookingBE[0].VehicleType.VehicleTypeID != Convert.ToInt32(ddlVehicleType.SelectedItem.Value))
                            //    isProvisional = true;  
                        }
                        //-------------------//

                        break;
                    }
                }
            }

            //if (ObjAPPBOK_BookingBE == null || ObjAPPBOK_BookingBE.Count == 0) {  //No Fixed Slot find for suggestion,find alternate slot
            if (isFixedSlotFind == false)
            {     //No Fixed Slot find for suggestion,find alternate slot
                btnConfirmBooking.Visible = false;
                lblProposeBookingInfo.Visible = false;

                //Find Slot for this booking 
                GenerateAlternateSlot(true, iSiteDoorNumberID, iSKUDoorTypeID);
                //GenerateAlternateSlot(true);

                tableDiv_General.Controls.RemoveAt(0);
            }

            //No space remaining on compatable door  -- show advice
            if (hdnSuggestedSlotTimeID.Value == string.Empty || hdnSuggestedSlotTimeID.Value == "-1")
            {
                string errMsg = WebCommon.getGlobalResourceValue("BK_MS_28_EXT"); // "No space available on any compatible doors.Please contact Goods In office or select a different date.";

                if (hdnCurrentRole.Value == "Vendor")
                {
                    //Show err msg
                    ShowErrorMessage(errMsg, "BK_MS_28");
                    //return;
                }
                else
                {
                    //Show err msg
                    errMsg = WebCommon.getGlobalResourceValue("BK_MS_28_INT"); //"No space available on any compatible doors.";
                    ShowErrorMessage(errMsg, "BK_MS_28");
                    //return;
                }
            }

        }

        if (hdnCurrentMode.Value == "Add")
        {
            foreach (GridViewRow gvRow in gvBookingSlots.Rows)
            {
                if (gvRow.RowType == DataControlRowType.DataRow)
                {
                    HiddenField hdnFixedSlotID = (HiddenField)gvRow.FindControl("hdnAvailableFixedSlotID");
                    if (hdnFixedSlotID != null && hdnFixedSlotID.Value == hdnSuggestedFixedSlotID.Value && hdnSuggestedFixedSlotID.Value != "-1")
                    {
                        gvRow.Cells[6].Text = "This Booking";
                        gvRow.BackColor = Color.GreenYellow;
                        gvRow.ForeColor = Color.Black;
                        break;
                    }
                }
            }
        }
        else if (hdnCurrentMode.Value == "Edit")
        {

            bool isSlotFound = false;
            foreach (GridViewRow gvRow in gvBookingSlots.Rows)
            {
                if (gvRow.RowType == DataControlRowType.DataRow)
                {
                    if (gvRow.Cells[3].Text.Trim() == hdnBookingRef.Value)
                    {
                        gvRow.Cells[6].Text = "This Booking";
                        gvRow.BackColor = Color.GreenYellow;
                        gvRow.ForeColor = Color.Black;
                        isSlotFound = true;
                        break;
                    }
                }
            }

            if (isSlotFound == false)
            {
                foreach (GridViewRow gvRow in gvBookingSlots.Rows)
                {
                    if (gvRow.RowType == DataControlRowType.DataRow)
                    {
                        HiddenField hdnFixedSlotID = (HiddenField)gvRow.FindControl("hdnAvailableFixedSlotID");
                        if (hdnFixedSlotID != null && hdnFixedSlotID.Value == hdnSuggestedFixedSlotID.Value && hdnSuggestedFixedSlotID.Value != "-1")
                        {
                            gvRow.Cells[6].Text = "This Booking";
                            gvRow.BackColor = Color.GreenYellow;
                            gvRow.ForeColor = Color.Black;
                            break;
                        }
                    }
                }
            }
        }
    }

    public List<T> MergeListCollections<T>(List<T> firstList, List<T> secondList)
    {
        List<T> mergedList = new List<T>();
        mergedList.InsertRange(0, firstList);
        mergedList.InsertRange(mergedList.Count, secondList);
        return mergedList;
    }

    private string GetLowestPriority(NameValueCollection DoorTypePriority)
    {  //SPRINT 4 POINT 7
        NameValueCollection collection = (NameValueCollection)DoorTypePriority;
        string LowestPri = "1";
        if (collection != null)
        {
            foreach (string key in collection.AllKeys)
            {
                if (Convert.ToInt32(collection.Get(key)) < Convert.ToInt32(LowestPri))
                {
                    LowestPri = collection.Get(key);
                }
            }
        }
        return LowestPri;
    }

    #endregion
   
}


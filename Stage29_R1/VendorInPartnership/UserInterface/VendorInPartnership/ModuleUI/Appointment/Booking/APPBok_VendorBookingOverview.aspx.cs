﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.Appointment.Booking;
using BusinessEntities.ModuleBE.Security;
using BusinessLogicLayer.ModuleBAL.Appointment.Booking;
using BusinessLogicLayer.ModuleBAL.Security;
using Utilities;
using WebUtilities;
using System.Collections;
using System.Linq;

using Microsoft.Reporting.WebForms;
using BusinessEntities.ModuleBE.Upload;
using BusinessLogicLayer.ModuleBAL.Upload;
using BusinessEntities.ModuleBE.AdminFunctions;
using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using BusinessLogicLayer.ModuleBAL.Appointment.SiteSettings;
using System.IO;
using System.Configuration;
using BusinessLogicLayer.ModuleBAL.AdminFunctions;
using BaseControlLibrary;


public partial class APPBok_VendorBookingOverview : CommonPage
{
    public SortDirection GridViewSortDirection {
        get {
            if (ViewState["sortDirection"] == null)
                ViewState["sortDirection"] = SortDirection.Ascending;
            return (SortDirection)ViewState["sortDirection"];
        }
        set { ViewState["sortDirection"] = value; }
    }

    protected void Page_Init(object sender, EventArgs e) {      
        ddlSite.CurrentPage = this;
        ddlSite.IsAllRequired = true;      
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        Label lblModuleText = (Label)Page.Master.FindControl("lblModuleText");
        lblModuleText.Text = "Appointment Scheduling - Booking History Overview";
        string Role = string.Empty;

        if (Session["Role"] != null && Session["Role"].ToString() != "") {
            hdnRole.Value = Session["Role"].ToString().ToLower();
            Role = Session["Role"].ToString().Trim().ToLower();
        }

        if (!Page.IsPostBack) {
            txtDate.innerControltxtDate.Value = Convert.ToString(DateTime.Now.AddDays(-1).ToString("dd/MM/yyyy"));
            ddlSite.PreCountryID = Convert.ToInt32(Session["SiteCountryID"]);
        }
    }

    protected void Page_PreRender(object sender, EventArgs e) {

        if (Session["VendorBookingCritreria"] != null && !IsPostBack) {
            Hashtable htBookingCriteria = (Hashtable)Session["VendorBookingCritreria"];
            txtDate.innerControltxtDate.Value = htBookingCriteria.ContainsKey("SchedulingDate") ? htBookingCriteria["SchedulingDate"].ToString() : "";
            
            ddlSite.innerControlddlSite.SelectedValue = htBookingCriteria.ContainsKey("site") ? htBookingCriteria["site"].ToString() : "";
            ddlSite.PreCountryID = htBookingCriteria.ContainsKey("PreCountryID") ? Convert.ToInt32(htBookingCriteria["PreCountryID"].ToString()) : 0;

            txtBookingRef.Text = htBookingCriteria.ContainsKey("BookingRef") ? htBookingCriteria["BookingRef"].ToString() : "";
            txtPurchaseNumber.Text = htBookingCriteria.ContainsKey("PurchaseNumber") ? htBookingCriteria["PurchaseNumber"].ToString() : "";
            BindBookings();
        }
    }

    protected void btnSearch_Click(object sender, EventArgs e) {
        this.Search();
    }

    private void Search() {
        hdnCurrentDate.Value = Utilities.Common.TextToDateFormat(DateTime.Now.Day.ToString() + "/" + DateTime.Now.Month.ToString() + "/" + DateTime.Now.Year.ToString()).ToString();
        hdnSelectedDate.Value = txtDate.GetDate.ToString();

        Hashtable htBookingCritreria = new Hashtable();
        htBookingCritreria.Add("SchedulingDate", txtDate.GetDate.ToString("dd/MM/yyyy"));
        htBookingCritreria.Add("site", Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value));
        htBookingCritreria.Add("BookingRef", !string.IsNullOrEmpty(txtBookingRef.Text.Trim()) ? txtBookingRef.Text.Trim() : "");
        htBookingCritreria.Add("PurchaseNumber", !string.IsNullOrEmpty(txtPurchaseNumber.Text.Trim()) ? txtPurchaseNumber.Text.Trim() : "");
        
        BindBookings();
        htBookingCritreria.Add("PreCountryID", ddlSite.innerControlddlSite != null && !string.IsNullOrEmpty(ddlSite.innerControlddlSite.SelectedValue) ? ddlSite.PreCountryID : (int?)null);
        htBookingCritreria.Add("PageIndex", 0);
        Session["VendorBookingCritreria"] = htBookingCritreria; 
    }

    protected void BindBookings(bool SkipStep = false) {
        APPBOK_BookingBE oAPPBOK_BookingBE = new APPBOK_BookingBE();
        APPBOK_BookingBAL oAPPBOK_BookingBAL = new APPBOK_BookingBAL();

        // setting current date and selected date value.
        hdnCurrentDate.Value = Utilities.Common.TextToDateFormat(DateTime.Now.Day.ToString() + "/" + DateTime.Now.Month.ToString() + "/" + DateTime.Now.Year.ToString()).ToString();
        hdnSelectedDate.Value = txtDate.GetDate.ToString();

        oAPPBOK_BookingBE.FixedSlot = new BusinessEntities.ModuleBE.Appointment.SiteSettings.MASSIT_FixedSlotBE();
        oAPPBOK_BookingBE.Carrier = new BusinessEntities.ModuleBE.Appointment.CountrySetting.MASCNT_CarrierBE();
        oAPPBOK_BookingBE.Delivery = new BusinessEntities.ModuleBE.Appointment.CountrySetting.MASCNT_DeliveryTypeBE();
        oAPPBOK_BookingBE.FixedSlot.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();

        string Role = Session["Role"].ToString().Trim().ToLower();
     
        oAPPBOK_BookingBE.Action = "VendorBookingHistoryList";       

        if (Role == "vendor")
            oAPPBOK_BookingBE.SupplierType = "V";
        else if (Role == "carrier")
            oAPPBOK_BookingBE.SupplierType = "C";

        //oAPPBOK_BookingBE.SelectedScheduleDate = txtDate.GetDate;        
        oAPPBOK_BookingBE.UserID = Convert.ToInt32(Session["UserID"]);
        oAPPBOK_BookingBE.SelectedScheduleDate = DateTime.Today.AddDays(-1);

        if (ddlSite.innerControlddlSite.SelectedIndex > 0)
            oAPPBOK_BookingBE.FixedSlot.SiteID = !string.IsNullOrEmpty(ddlSite.innerControlddlSite.SelectedValue) ? Convert.ToInt32(ddlSite.innerControlddlSite.SelectedValue) : 0;
        else {
            oAPPBOK_BookingBE.FixedSlot.SiteIDs = ddlSite.UserSiteIDs;
        }

       // oAPPBOK_BookingBE.FixedSlot.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
       
        DataTable lstBookings = new DataTable();

        lstBookings = oAPPBOK_BookingBAL.GetAllBookingDetailBAL(oAPPBOK_BookingBE);
        
        DataColumn myDataColumn = new DataColumn();
        myDataColumn.ColumnName = "ActWeekDay";
        lstBookings.Columns.Add(myDataColumn);
              

        int iWeekDayNo = 0;
        string strWeekDaySel = txtDate.GetDate.DayOfWeek.ToString().ToUpper();
        if (strWeekDaySel == "SUNDAY") {
            iWeekDayNo = 1;
        }
        else if (strWeekDaySel == "MONDAY") {
            iWeekDayNo = 2;
        }
        else if (strWeekDaySel == "TUESDAY") {
            iWeekDayNo = 3;
        }
        else if (strWeekDaySel == "WEDNESDAY") {
            iWeekDayNo = 4;
        }
        else if (strWeekDaySel == "THURSDAY") {
            iWeekDayNo = 5;
        }
        else if (strWeekDaySel == "FRIDAY") {
            iWeekDayNo = 6;
        }
        else if (strWeekDaySel == "SATURDAY") {
            iWeekDayNo = 7;
        }

        foreach (DataRow row in lstBookings.Rows) {
            if (row["WeekDay"].ToString().Trim() == iWeekDayNo.ToString() || (row["WeekDay"].ToString().Trim() == "8"))
                row["ActWeekDay"] = iWeekDayNo.ToString();
            else
                row["ActWeekDay"] = row["WeekDay"];
        }


        List<MASSIT_WeekSetupBE> lstWeekSetup = new List<MASSIT_WeekSetupBE>();

        if (Role == "vendor" || Role == "carrier") {

            myDataColumn = new DataColumn();
            myDataColumn.ColumnName = "LogicalDeliveryDate";
            lstBookings.Columns.Add(myDataColumn);
            foreach (DataRow dr in lstBookings.Rows) {

                //-----------------Start Collect Basic Information --------------------------//
                DateTime dtSelectedDate = Common.GetDD_MM_YYYYDatetime(dr["DeliveryDate"].ToString());
                string WeekDay = dtSelectedDate.ToString("dddd");

                DateTime dtMon = Utilities.Common.GetFirstDayOfWeek(dtSelectedDate);

                MASSIT_WeekSetupBE oMASSIT_WeekSetupBE = new MASSIT_WeekSetupBE();
                MASSIT_WeekSetupBAL oMASSIT_WeekSetupBAL = new MASSIT_WeekSetupBAL();

                oMASSIT_WeekSetupBE.Action = "GetSpecificWeekShowAll";
                oMASSIT_WeekSetupBE.SiteID = Convert.ToInt32(dr["SiteId"].ToString());
                oMASSIT_WeekSetupBE.EndWeekday = WeekDay;
                oMASSIT_WeekSetupBE.ScheduleDate = dtSelectedDate;

                lstWeekSetup = oMASSIT_WeekSetupBAL.GetWeekSetupDetailsBAL(oMASSIT_WeekSetupBE);

                // To fixed - to restrict to show fixed slots on week setup not defined 
                //if (lstWeekSetup == null || lstWeekSetup.Count <= 0 || lstWeekSetup[0].StartTime == null) {
                //    DataView dv = lstBookings.DefaultView;
                //    dv.RowFilter = "WeekDay <> '8'";
                //    lstBookings = dv.ToTable();
                //}
                //---------------------------------------------------------------//


                if (lstWeekSetup.Count > 0 && lstWeekSetup[0].StartTime != null) {
                    if (lstWeekSetup[0].StartWeekday != lstWeekSetup[0].EndWeekday) {

                        if (dr["Weekday"] != DBNull.Value) {
                            string dtdate = Common.GetDD_MM_YYYYDatetime(dr["DeliveryDate"].ToString()).ToString("dddd");


                            string strWeekDay = dtdate.ToUpper();
                            if (strWeekDay == "SUNDAY") {
                                if (1 != Convert.ToInt32(dr["Weekday"]))
                                    dr["DeliveryDate"] = Common.GetDD_MM_YYYYDatetime(dr["DeliveryDate"].ToString()).AddDays(-1);
                            }
                            else if (strWeekDay == "MONDAY") {
                                if (2 != Convert.ToInt32(dr["Weekday"]))
                                    dr["DeliveryDate"] = Common.GetDD_MM_YYYYDatetime(dr["DeliveryDate"].ToString()).AddDays(-1);
                            }
                            else if (strWeekDay == "TUESDAY") {
                                if (3 != Convert.ToInt32(dr["Weekday"]))
                                    dr["DeliveryDate"] = Common.GetDD_MM_YYYYDatetime(dr["DeliveryDate"].ToString()).AddDays(-1);
                            }
                            else if (strWeekDay == "WEDNESDAY") {
                                if (4 != Convert.ToInt32(dr["Weekday"]))
                                    dr["DeliveryDate"] = Common.GetDD_MM_YYYYDatetime(dr["DeliveryDate"].ToString()).AddDays(-1);
                            }
                            else if (strWeekDay == "THURSDAY") {
                                if (5 != Convert.ToInt32(dr["Weekday"]))
                                    dr["DeliveryDate"] = Common.GetDD_MM_YYYYDatetime(dr["DeliveryDate"].ToString()).AddDays(-1);
                            }
                            else if (strWeekDay == "FRIDAY") {
                                if (6 != Convert.ToInt32(dr["Weekday"]))
                                    dr["DeliveryDate"] = Common.GetDD_MM_YYYYDatetime(dr["DeliveryDate"].ToString()).AddDays(-1);
                            }
                            else if (strWeekDay == "SATURDAY") {
                                if (7 != Convert.ToInt32(dr["Weekday"]))
                                    dr["DeliveryDate"] = Common.GetDD_MM_YYYYDatetime(dr["DeliveryDate"].ToString()).AddDays(-1);
                            }
                        }
                    }
                }
            }
        }

        //Filte out Non Timed Delivery//      
        DataTable lstBookingsBonTime = null;
      

            if (lstBookings != null && lstBookings.Rows.Count > 0) {
                DataView dv = lstBookings.DefaultView;
                dv.RowFilter = "BookingType = 'Non Timed Delivery'";
                lstBookingsBonTime = dv.ToTable();
            }

            if (lstBookings != null && lstBookings.Rows.Count > 0) {
                DataView dv = lstBookings.DefaultView;
                dv.RowFilter = "BookingType <> 'Non Timed Delivery' OR BookingType is null";
                lstBookings = dv.ToTable();
            }




        if (lstBookings == null || lstBookings.Rows.Count == 0) {
            if (lstBookingsBonTime != null && lstBookingsBonTime.Rows.Count > 0) {
                lstBookings = lstBookingsBonTime;
                lstBookingsBonTime = null;
            }
        }
        //----------------------------//

        if (lstBookings != null && lstBookings.Rows.Count > 0) {

            //---Filter By PO------//
            if (txtPurchaseNumber.Text.Trim() != string.Empty) {
                DataView dvtemp = lstBookings.DefaultView;
                dvtemp.RowFilter = "POs LIKE '%" + "," + txtPurchaseNumber.Text.Trim() + "," + "%'";
                //dvtemp.RowFilter = "PO = '" + txtPurchaseNumber.Text.Trim() + "'";
                lstBookings = dvtemp.ToTable();
            }
            //---------------------//

            if (!string.IsNullOrEmpty(txtBookingRef.Text.Trim())) {
                //DataTable lstBookingsFilter = null;
                //lstBookingsFilter = lstBookings.Copy();
                DataView dvFilter = lstBookings.DefaultView;
                dvFilter.RowFilter = "BookingRef = '" + txtBookingRef.Text.Trim() + "'";
                lstBookings = dvFilter.ToTable();
            }
            //-------------------------------------//

            DataColumn dcIndex = new DataColumn();
            dcIndex.ColumnName = "AutoID";
            dcIndex.DataType = System.Type.GetType("System.Int32");
            lstBookings.Columns.Add(dcIndex);

            int iAutoId = 0;
            //-------------------------------------//

            foreach (DataRow dr in lstBookings.Rows) {
                iAutoId++;
                dr["AutoID"] = iAutoId.ToString();
            }

            lstBookings.DefaultView.Sort = "DeliveryDate DESC,OrderBY";


            if (SkipStep != true) {
                int iPageIndex = 0;
                if (!string.IsNullOrEmpty(txtDate.GetDate.ToString())) {
                    DataTable lstBookingsFilter = null;
                    lstBookingsFilter = lstBookings.Copy();
                    DataView dvFilter = lstBookingsFilter.DefaultView;
                    dvFilter.RowFilter = "DeliveryDate = '" + txtDate.GetDate + "'";
                    lstBookingsFilter = dvFilter.ToTable();

                    if (lstBookingsFilter != null && lstBookingsFilter.Rows.Count > 0) {
                        iPageIndex = Convert.ToInt32(lstBookingsFilter.Rows[0]["AutoID"]) / 30;
                        if (Convert.ToInt32(lstBookingsFilter.Rows[0]["AutoID"]) % 30 == 0)
                            iPageIndex--;
                    }
                }

                gvVendor.PageIndex = iPageIndex;
            }

            gvVendor.DataSource = lstBookings;
            gvVendor.DataBind();
            ViewState["lstSites"] = lstBookings;            
            gvVendor.Visible = true;
                 
        }
        else {           
            gvVendor.Visible = false;
            //lblVendorNoBooking.Visible = true;
        }

        if (Session["VendorBookingCritreria"] != null) {
            //Hashtable htBookingCriteria = (Hashtable)Session["VendorBookingCritreria"];
            //ucGridView1.PageIndex =  (htBookingCriteria.ContainsKey("PageIndex") && htBookingCriteria["PageIndex"] != null) ? Convert.ToInt16(htBookingCriteria["PageIndex"]) : 0;
        }
        //GlobalResourceFields(this.Page);
    }

    protected void gridView_Sorting(Object sender, GridViewSortEventArgs e) {
        try {
            string sortExpression = e.SortExpression;
            if (GridViewSortDirection == SortDirection.Ascending) {
                GridViewSortDirection = SortDirection.Descending;
                SortGridView(sortExpression, " ASC");
            }
            else {
                GridViewSortDirection = SortDirection.Ascending;
                SortGridView(sortExpression, " DESC");
            }
        }
        catch (Exception ex) {
            LogUtility.SaveErrorLogEntry(ex);
        }
    }

    private void SortGridView(string sortExpression, string direction) {
        DataTable dtSortingTbl;
        DataView dtView;
        try {
            if (ViewState["lstSites"] != null) {
                dtSortingTbl = (DataTable)ViewState["lstSites"];
                dtView = dtSortingTbl.DefaultView;
                //dtView =  new DataView(dtSortingTbl);
                if (sortExpression != "Weekday") {
                    int sortExpIndx = sortExpression.IndexOf(",");
                    if (sortExpIndx == -1)
                        dtView.Sort = sortExpression + direction;
                    else {
                        dtView.Sort = sortExpression.Substring(0, sortExpIndx) + direction + sortExpression.Substring(sortExpIndx);
                    }
                }
                else {
                    if (direction.Trim() == "ASC")
                        dtView.Sort = "Weekday desc,OrderBY,DoorNumber";
                    else
                        dtView.Sort = "Weekday asc,OrderBY,DoorNumber";
                }
                if (sortExpression != "AutoID")
                    dtSortingTbl = GetSortByBooking(dtView.ToTable());
                else
                    dtSortingTbl = dtView.ToTable();

                string Role = Session["Role"].ToString().Trim().ToLower();
                GridView gridToParse = new GridView();
               
                gridToParse = gvVendor;
       
                gridToParse.DataSource = dtSortingTbl;
                gridToParse.DataBind();

                CommonPage c = new CommonPage();
                c.LocalizeGridHeader(gridToParse);
            }
        }
        catch (Exception ex) {
            LogUtility.SaveErrorLogEntry(ex);
        }

    }

    DataTable dtSortingTblByBooking = null;
    private DataTable GetSortByBooking(DataTable dtSortingTbl) {
        if (dtSortingTbl != null && dtSortingTbl.Rows.Count > 0) {
            //DataTable lstBookings = dtSortingTbl;
            if (dtSortingTblByBooking == null)
                dtSortingTblByBooking = dtSortingTbl.Clone();

            string bookingRef = dtSortingTbl.Rows[0]["BookingRef"].ToString();

            DataRow[] rows;
            rows = dtSortingTbl.Select("BookingRef = '" + bookingRef + "'");
            foreach (DataRow r in rows) {
                dtSortingTblByBooking.ImportRow(r);
                r.Delete();
            }
            dtSortingTblByBooking.AcceptChanges();

            //DataView dv = lstBookings.DefaultView;
            //dv.RowFilter = "BookingRef = '" + bookingRef + "'";
            //lstBookings = dv.ToTable();

            //foreach (DataRow dr in lstBookings.Rows) {
            //    dtSortingTblByBooking.ImportRow(dr);
            //}
            //dtSortingTblByBooking.AcceptChanges();

            //rows = dtSortingTbl.Select("BookingRef = '" + bookingRef + "'");

            //foreach (DataRow r in rows)
            //    r.Delete();

            GetSortByBooking(dtSortingTbl);
        }
        return dtSortingTblByBooking;
    }

    protected void gvVendor_PageIndexChanging(object sender, GridViewPageEventArgs e) {
        gvVendor.PageIndex = e.NewPageIndex;

        BindBookings(true);
    }

    protected void gvVendor_RowDataBound(object sender, GridViewRowEventArgs e) {
        GridViewRow gvr = e.Row;
        if (e.Row.RowType == DataControlRowType.DataRow) {

            
            HiddenField hdnBookingFor = (HiddenField)e.Row.FindControl("hdnBookingFor");
            HiddenField hdnPrimaryKey = (HiddenField)e.Row.FindControl("hdnPrimaryKey");
            HiddenField hdnBookingID = (HiddenField)e.Row.FindControl("hdnBookingID");
            HiddenField hdnsiteid = (HiddenField)e.Row.FindControl("hdnsiteid");



            HyperLink hyplViewHistory = (HyperLink)e.Row.FindControl("hyplViewHistory");

            //hyplViewHistory.NavigateUrl = EncryptQuery("../Receiving/APPRcv_BookingHistory.aspx?ID=" + hdnBookingID.Value);
            hyplViewHistory.NavigateUrl = EncryptQuery("~/ModuleUI/Appointment/Receiving/APPRcv_BookingHistory.aspx?Scheduledate="
                           + txtDate.innerControltxtDate.Value + "&ID=BK-"
                           + hdnBookingFor.Value.ToString() + "-" + hdnPrimaryKey.Value.Split('-')[1] + "-" + hdnsiteid.Value.ToString());


            #region Code to set visibility of [Time / Day] based on [Time Window Id] ..
            ucLiteral UcLiteral27 = (ucLiteral)e.Row.FindControl("UcLiteral27");
            ucLiteral UcLiteral27_1 = (ucLiteral)e.Row.FindControl("UcLiteral27_1");
            ucLiteral UcLiteral27_2 = (ucLiteral)e.Row.FindControl("UcLiteral27_2");
            //HiddenField hdnTimeWindowID = (HiddenField)e.Row.FindControl("hdnTimeWindowID");

            if (!string.IsNullOrEmpty(UcLiteral27_1.Text) && !string.IsNullOrWhiteSpace(UcLiteral27_1.Text)) {
                UcLiteral27_2.Visible = false;
                UcLiteral27_1.Visible = true;
                UcLiteral27.Visible = false;
            }
            else if (!string.IsNullOrEmpty(UcLiteral27_2.Text) && !string.IsNullOrWhiteSpace(UcLiteral27_2.Text)) {
                UcLiteral27_2.Visible = true;
                UcLiteral27_1.Visible = false;
                UcLiteral27.Visible = false;
            }
            else {
                UcLiteral27_2.Visible = false;
                UcLiteral27_1.Visible = false;
                UcLiteral27.Visible = true;
            }
            #endregion
        }
    }
}
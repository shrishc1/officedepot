﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    MaintainScrollPositionOnPostback="true" AutoEventWireup="true" CodeFile="APPBok_DailyVolumeReport.aspx.cs"
    Inherits="APPBok_DailyVolumeReport" %>
      
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register TagName="ucDate" TagPrefix="cc2" Src="~/CommonUI/UserControls/ucDate.ascx" %>
<%@ Register Src="~/CommonUI/UserControls/ucSite.ascx" TagName="ucSite" TagPrefix="cc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
  <style type="text/css">.Gridheader
        {
          background-color:#F6EFE7;
          color:	#934500;
          font-weight:bold;
        }</style>

    <script type="text/javascript" src="../../../Scripts/canvasjs.min.js"></script>
    <script type="text/javascript">       
        var dps1 = [];
        var dps2 = [];
        var vol = "";


        var GraphType = '';
        var Site = '';


        $(function () {
            $("body").keypress(function (e) {
                if (e.which == 13 || e.keyCode == 13) {
                    if ($('#<%= ddlGraphOverview.ClientID%>').val() == "Graph") {
                        $('#btnGo1').click();
                    }
                    else {
                        $('#<%= btnGo.ClientID %>').click();
                    }
                    return false;
                }
            });
            if ($('#<%= ddlGraphOverview.ClientID%>').val() == "Graph") {
                $('#btnGo1').show();
                $('#tdvolume1').hide();
                $('#tdvolume2').hide();
                $('#tdvolume3').hide();
                $('#divGraph').show();
                $('#<%= btnExportToExcel.ClientID %>').hide();
                $('#<%= btnGo.ClientID %>').hide();
                GenerateGraph();
            }
            else {
                $('#btnGo1').hide();
                $('#tdvolume1').show();
                $('#tdvolume2').show();
                $('#tdvolume3').show();
                $('#divGraph').hide();
                $('#tdmeasure1').hide();
                $('#tdmeasure2').hide();
                $('#tdmeasure3').hide();
                $('#<%= btnExportToExcel.ClientID %>').show();
            }
            $('#<%= btnGo.ClientID %>').click(function () {
                $('#tdmeasure1').hide();
                $('#tdmeasure2').hide();
                $('#tdmeasure3').hide();
                $('#tdvolume1').show();
                $('#tdvolume2').show();
                $('#tdvolume3').show();
                $('#divOverview').show();
                $('#divGraph').hide();
                $('#<%= btnGo.ClientID %>').show();
                $('#btnGo1').hide();
                $('#<%= btnExportToExcel.ClientID %>').show();
            });
            $('#btnGo1').click(function () {
                $('#tdvolume1').hide();
                $('#tdvolume2').hide();
                $('#tdvolume3').hide();
                $('#tdmeasure1').show();
                $('#tdmeasure2').show();
                $('#tdmeasure3').show();
                $('#<%= btnGo.ClientID %>').hide();
                $('#btnGo1').show();
                $('#divOverview').hide();
                $('#divGraph').show();
                $('#<%= btnExportToExcel.ClientID %>').hide();
            });

            $('#<%= ddlGraphOverview.ClientID%>').change(function () {
                if ($('#<%= ddlGraphOverview.ClientID%>').val() == "Graph") {
                    $('#tdvolume1').hide();
                    $('#tdvolume2').hide();
                    $('#tdvolume3').hide();
                    $('#tdmeasure1').show();
                    $('#tdmeasure2').show();
                    $('#tdmeasure3').show();
                    $('#<%= btnGo.ClientID %>').hide();
                    $('#btnGo1').show();
                    $('#divOverview').hide();
                    $('#divGraph').hide();
                    //  GenerateGraph();
                }
                else {
                    $('#tdmeasure1').hide();
                    $('#tdmeasure2').hide();
                    $('#tdmeasure3').hide();
                    $('#tdvolume1').show();
                    $('#tdvolume2').show();
                    $('#tdvolume3').show();
                    $('#divOverview').hide();
                    $('#divGraph').hide();
                    $('#<%= btnGo.ClientID %>').show();
                    $('#btnGo1').hide();
                }
            });
        });
        function GenerateGraph() {           
            GraphType =  $("#ddlGraphType").val();
            Site = $("#ctl00_ContentPlaceHolder1_ddlSite_ddlSite option:selected").text();  //"# " + $("#ddlSite option:selected").val();

            LoadChart1();

        }




        function LoadChart1() {

            $.ajax({
                type: "POST",
                url: "APPBok_DailyVolumeReport.aspx/GetAllVolumeReportData",
                data: "{GraphType: '" + $("[id*=ddlGraphType]").val() + "',VolumeType: '" + $("[id*=ddlVolume]").val() + "',SiteID: '" + $("[id*=ddlSite]").val() + "',Date: '" + $("input[name*='ctl00$ContentPlaceHolder1$txtDate$txtUCDate']").val() + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (r) {
                    var vol = r.d;

                    try {
                        dps1 = eval(vol[0]); // eval(r.d);
                        dps2 = eval(vol[1]);
                        $("#chartContainer").show();
                    }
                    catch (Error) {                       
                        $("#chartContainer").hide();
                    }
                    
                    vol = eval(vol[2]);
                    $('#<%=ltTotalPallets.ClientID%>').html(vol[0]);
                    $('#<%=ltTotalLines.ClientID%>').html(vol[1]);
                    $('#<%=ltTotalDeliveries.ClientID%>').html(vol[2]);

                    $('#<%=ltConfirmedPallets.ClientID%>').html(vol[3]);
                    $('#<%=ltConfirmedLines.ClientID%>').html(vol[4]);
                    $('#<%=ltConfirmedDeliveries.ClientID%>').html(vol[5]);

                    $('#<%=ltUnconfirmedPallets.ClientID%>').html(vol[6]);
                    $('#<%=ltUnconfirmedLines.ClientID%>').html(vol[7]);
                    $('#<%=ltUnconfirmedDeliveries.ClientID%>').html(vol[8]);

                    RenderChart();

                    //SetVolume();                 

                },
                failure: function (response) {
                    alert('There was an error.');
                }
            });
            
            
        }      


        function SetVolume() {
            $.ajax({
                type: "POST",
                url: "APPBok_DailyVolumeReport.aspx/GetSummery",
                data: "{SiteID: '" + $("[id*=ddlSite]").val() + "',Date: '" + $("input[name*='ctl00$ContentPlaceHolder1$txtDate$txtUCDate']").val() + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (r) {
                    vol = eval(r.d);
                    $('#<%=ltTotalPallets.ClientID%>').html(vol[0]);
                    $('#<%=ltTotalLines.ClientID%>').html(vol[1]);
                    $('#<%=ltTotalDeliveries.ClientID%>').html(vol[2]);

                    $('#<%=ltConfirmedPallets.ClientID%>').html(vol[3]);
                    $('#<%=ltConfirmedLines.ClientID%>').html(vol[4]);
                    $('#<%=ltConfirmedDeliveries.ClientID%>').html(vol[5]);

                    $('#<%=ltUnconfirmedPallets.ClientID%>').html(vol[6]);
                    $('#<%=ltUnconfirmedLines.ClientID%>').html(vol[7]);
                    $('#<%=ltUnconfirmedDeliveries.ClientID%>').html(vol[8]);
                },
                failure: function (response) {
                    alert('There was an error.');
                }
            });
        }

        function RenderChart() {
            var chart = new CanvasJS.Chart("chartContainer", {
                
                toolTip: {
                    reversed: true,
                    shared: true  //disable here. 
                },
                title: {
                    text: GraphType + " Per Hour (" + Site + ")"
                },
                exportFileName: GraphType + " Per Hour (" + Site + ")",
                exportEnabled: true,
                axisY: {
                    title: "# " + GraphType
                },
                axisX: {

                    interval: 1,
                    intervalType: "hour",
                    title: "Time Slot"
                },
                data: [

                            {
                                type: "stackedColumn",
                                //toolTipContent: "{label}<br/><span style='\"'color: {color};'\"'><strong>{name}</strong></span>: {y}",
                                color: "#33CCCC",
                                name: "Confirmed " + GraphType,
                                showInLegend: "true",
                                dataPoints: dps1
                            }


                            , {
                                type: "stackedColumn",
                                //toolTipContent: "{label}<br/><span style='\"'color: {color};'\"'><strong>{name}</strong></span>: {y}",
                                color: "#FFCC00",
                                name: "Unconfirmed " + GraphType,
                                showInLegend: "true",
                                dataPoints: dps2
                            }

                        ]
                          ,
                legend: {
                    cursor: "pointer",
                    fontSize: 20,
                    itemclick: function (e) {
                        if (typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
                            e.dataSeries.visible = false;
                        }
                        else {
                            e.dataSeries.visible = true;
                        }
                        chart.render();
                    }
                }
            });

            chart.render();

        }

        function LoadVolume() {
            //alert(vol);
            //$("#VolumeDetails").html(vol);
        }
    </script>
    <h2>
        <cc1:ucLabel ID="lblVolumePerHourTracker" runat="server"></cc1:ucLabel>
    </h2>
    <div class="right-shadow">
        <div class="formbox">
            <table width="100%" cellspacing="1" cellpadding="0" border="0" align="center" class="top-settings">
                <tr>
                    <td style="font-weight: bold; width: 6%">
                        <cc1:ucLabel ID="lblGraphType" runat="server"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold; width: 1%">
                        :
                    </td>
                    <td style="font-weight: bold; width: 19%">
                        <cc1:ucDropdownList ID="ddlGraphOverview" runat="server" Width="130px">
                            <asp:ListItem Text="Graph" Value="Graph"></asp:ListItem>
                            <asp:ListItem Text="Overview" Value="Overview"></asp:ListItem>
                        </cc1:ucDropdownList>
                    </td>
                    <td style="font-weight: bold; width: 4%">
                        <cc1:ucLabel ID="lblDate" runat="server" Text="Date"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold; width: 1%">
                        :
                    </td>
                    <td style="font-weight: bold; width: 25%">
                        <cc2:ucDate ClientIDMode="Static" ID="txtDate" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td style="font-weight: bold; width: 3%">
                        <cc1:ucLabel ID="lblSitePrefixName" runat="server"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold; width: 1%">
                        :
                    </td>
                    <td style="font-weight: bold; width: 26%">
                        <cc2:ucSite ID="ddlSite" runat="server" />
                    </td>
                    <td style="font-weight: bold; width: 5%; " id="tdmeasure1">
                        <cc1:ucLabel ID="lblMeasure" runat="server" Text="Measure"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold; width: 1%; " id="tdmeasure2">
                        :
                    </td>
                    <td style="font-weight: bold; width: 24%; " id="tdmeasure3">
                        <cc1:ucDropdownList ID="ddlGraphType" ClientIDMode="Static" runat="server" Width="130px">
                            <asp:ListItem Text="Pallets" Value="Pallets"></asp:ListItem>
                            <asp:ListItem Text="Lines" Value="Lines"></asp:ListItem>
                            <asp:ListItem Text="Deliveries" Value="Deliveries"></asp:ListItem>
                        </cc1:ucDropdownList>
                    </td>
                    <td style="font-weight: bold; width: 5%" id="tdvolume1">
                        <cc1:ucLabel ID="lblVolume" runat="server"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold; width: 1%" id="tdvolume2">
                        :
                    </td>
                    <td style="font-weight: bold; width: 19%" id="tdvolume3">
                        <cc1:ucDropdownList ID="ddlVolume" runat="server" Width="130px" disabled>
                            <asp:ListItem Text="--All--" Value="All"></asp:ListItem>
                        </cc1:ucDropdownList>
                    </td>
                    <td align="center" style="font-weight: bold; width: 15%">
                        <cc1:ucButton ID="btnGo" runat="server" CssClass="button" OnClick="btnGenerateReport_Click" />
                        <input id="btnGo1" type="button" name="GO" value="Go" class="button" onclick="javascript:GenerateGraph();" />
                    </td>
                </tr>
            </table>
            <br />
            <div id="divGraph">
                <table style="min-width: 240px;" border="0" cellspacing="0" align="center" cellpadding="0"
                    class="grid gvclass searchgrid-1">
                    <tr>
                        <th style="min-width: 75px; text-align: left;">
                            &nbsp;
                        </th>
                        <th style="width: 55px; text-align: center;">
                            <cc1:ucLabel ID="lblPallets" runat="server"></cc1:ucLabel>
                        </th>
                        <th style="width: 55px; text-align: center;">
                            <cc1:ucLabel ID="lblLines" runat="server"></cc1:ucLabel>
                        </th>
                        <th style="width: 55px; text-align: center;">
                            <cc1:ucLabel ID="lblDeliveries" runat="server"></cc1:ucLabel>
                        </th>
                    </tr>
                    <tr>
                        <th style="height: 25px; vertical-align: middle;">
                            <cc1:ucLabel ID="lblTotal" runat="server"></cc1:ucLabel>
                        </th>
                        <td style="color: #333333; background-color: #F7F6F3; text-align: center;">
                            <asp:Label ID="ltTotalPallets" runat="server"></asp:Label>
                        </td>
                        <td style="color: #333333; background-color: #F7F6F3; text-align: center;">
                            <asp:Label ID="ltTotalLines" runat="server"></asp:Label>
                        </td>
                        <td style="color: #333333; background-color: #F7F6F3; text-align: center;">
                            <asp:Label ID="ltTotalDeliveries" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <th style="height: 25px; vertical-align: middle;">
                            <cc1:ucLabel ID="lblConfirmed" runat="server"></cc1:ucLabel>
                        </th>
                        <td style="text-align: center;">
                            <asp:Label ID="ltConfirmedPallets" runat="server"></asp:Label>
                        </td>
                        <td style="text-align: center;">
                            <asp:Label ID="ltConfirmedLines" runat="server"></asp:Label>
                        </td>
                        <td style="text-align: center;">
                            <asp:Label ID="ltConfirmedDeliveries" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <th style="height: 25px; vertical-align: middle;">
                            <cc1:ucLabel ID="lblUnconfirmed" runat="server"></cc1:ucLabel>
                        </th>
                        <td style="color: #333333; background-color: #F7F6F3; text-align: center;">
                            <asp:Label ID="ltUnconfirmedPallets" runat="server"></asp:Label>
                        </td>
                        <td style="color: #333333; background-color: #F7F6F3; text-align: center;">
                            <asp:Label ID="ltUnconfirmedLines" runat="server"></asp:Label>
                        </td>
                        <td style="color: #333333; background-color: #F7F6F3; text-align: center;">
                            <asp:Label ID="ltUnconfirmedDeliveries" runat="server"></asp:Label>
                        </td>
                    </tr>
                </table>
                <br />
                <div id="chartContainer" style="height: 300px; width: 100%;">
                </div>
            </div>
        </div>
        <div id="divOverview">
            <div class="button-row">
                <cc1:ucButton ID="btnExportToExcel" runat="server" CssClass="exporttoexcel" Height="20px"
                    OnClick="btnExport_Click" Width="109px" />
            </div>
            <cc1:ucGridView ID="grdBind" Visible="true" ClientIDMode="Static" Width="100%" Height="80%"
                runat="server" CssClass="grid" GridLines="Both" AutoGenerateColumns="false" OnRowCreated="grdBind_RowCreated">
                <RowStyle HorizontalAlign="Center"></RowStyle>
                <Columns>
                    <asp:BoundField DataField="SlotTime" HeaderText="" />
                    <asp:BoundField DataField="Pallets" HeaderText="Pallets" />
                    <asp:BoundField DataField="Lines" HeaderText="Lines" />
                    <asp:BoundField DataField="Deliveries" HeaderText="Deliveries" />
                    <asp:BoundField DataField="Pallets1" HeaderText="Pallets" />
                    <asp:BoundField DataField="Lines1" HeaderText="Lines" />
                    <asp:BoundField DataField="Deliveries1" HeaderText="Deliveries" />
                    <asp:BoundField DataField="Pallets2" HeaderText="Pallets" />
                    <asp:BoundField DataField="Lines2" HeaderText="Lines" />
                    <asp:BoundField DataField="Deliveries2" HeaderText="Deliveries" />
                </Columns>
            </cc1:ucGridView>
             <cc1:ucLabel ID="lblNoRecordsFound" runat="server" Visible="false"></cc1:ucLabel>
        </div>
    </div>
</asp:Content>

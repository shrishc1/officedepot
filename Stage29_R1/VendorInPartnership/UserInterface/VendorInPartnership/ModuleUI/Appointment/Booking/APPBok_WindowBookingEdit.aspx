﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    CodeFile="APPBok_WindowBookingEdit.aspx.cs" Inherits="APPBok_WindowBookingEdit"
    MaintainScrollPositionOnPostback="true" %>

<%@ Register TagName="ucDate" TagPrefix="cc2" Src="~/CommonUI/UserControls/ucSchedulingDate.ascx" %>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="../../../CommonUI/UserControls/ucSite.ascx" TagName="ucSite" TagPrefix="cc2" %>
<%@ Register Src="../../../CommonUI/UserControls/ucSeacrhVendor.ascx" TagName="ucSeacrhVendor"
    TagPrefix="cc3" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<%@ Register Src="~/CommonUI/UserControls/ucCountryISPM15.ascx" TagName="ucCountry" TagPrefix="cc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <style type="text/css">
        .fixedColumn .fixedTable td
        {
            color: Black;
            background-color: Gray;
            font-size: 12px;
            font-weight: normal;
            border: 1px solid;
        }
        
        .fixedHead td, .fixedFoot td
        {
            color: Black;
            background-color: Gray;
            font-size: 12px;
            font-weight: normal;
            padding: 5px;
            border: 1px solid;
        }
        .fixedTable td
        {
            font-size: 8.5pt;
            background-color: #FFFFFF;
            padding: 5px;
            text-align: left; /*border: 0px solid #CEE7FF;*/
        }
    </style>
    <script type="text/javascript" src="Support/sh_main.min.js"></script>
    <script type="text/javascript" src="Support/sh_javascript.js"></script>
    <script type="text/javascript" src="Support/jquery.fixedtable.js"></script>
    <script type="text/javascript">

        $(document).ready(function () {

            //sh_highlightDocument();

            $(".tableDiv").each(function () {

                var Id = $(this).get(0).id;
                var maintbheight = 335;
                var maintbwidth = 940;

                $("#" + Id + " .FixedTables").fixedTable({
                    width: maintbwidth,
                    height: maintbheight,
                    fixedColumns: 1,
                    classHeader: "fixedHead",
                    classFooter: "fixedFoot",
                    classColumn: "fixedColumn",
                    fixedColumnWidth: 145,
                    outerId: Id,
                    Contentbackcolor: "#FFFFFF",
                    Contenthovercolor: "#99CCFF",
                    fixedColumnbackcolor: "#187BAF",
                    fixedColumnhovercolor: "#99CCFF"
                });
            });
        });


    </script>
    <script type="text/javascript">
        function pageLoad() {
            $('.fixedHead').find('td').each(function () {
                $(this).width(165);
            });

            $('.fixedTable').scroll(function () {
                document.getElementById('<%=scroll1.ClientID%>').value = $(this).scrollTop(); // 
                document.getElementById('<%=scroll2.ClientID%>').value = $(this).scrollLeft();

            });

            setScroll();
        }
    </script>
    <script language="javascript" type="text/javascript">

        var _oldonerror = window.onerror;
        window.onerror = function (errorMsg, url, lineNr) { return true; };

        function chkVendorID() {
            //debugger;
            if (document.getElementById('<%=hdnCurrentMode.ClientID%>').value == "Add" && document.getElementById('<%=hdnCurrentRole.ClientID%>').value != "Vendor" && document.getElementById('<%=hdnWindowSlotMode.ClientID%>').value == "false") {
                if (document.getElementById('<%=ucSeacrhVendor1.FindControl("txtVendorNo").ClientID%>') != null) {
                    if (document.getElementById('<%=ucSeacrhVendor1.FindControl("txtVendorNo").ClientID%>').value == "") {
                        alert('<%=BK_ALT_CorrectVendor%>'); //Please enter correct vendor number or use search to find vendor.
                        return false;
                    }
                    else if (document.getElementById('<%=ucSeacrhVendor1.FindControl("hdnVendorName").ClientID%>').value == "") {
                        alert('<%=BK_ALT_Wait%>');  //Please wait while system is searching for vendor.
                        return false;
                    }
                }
            }
            if (!Page_ClientValidate("DeliveryDetail")) {
                return false;
            }
        }

        function setVendorName() {
            if (document.getElementById('<%=ucSeacrhVendor1.FindControl("SelectedVendorName").ClientID%>') != null) {
                document.getElementById('<%=ucSeacrhVendor1.FindControl("SelectedVendorName").ClientID%>').innerHTML = document.getElementById('<%=ucSeacrhVendor1.FindControl("hdnVendorName").ClientID%>').value;
            }
        }

        function setToolTip() {
            if (_val_IE6)
                initBaloonTips();
        }


        function CheckAletrnateSlot(AvailableSlotTimeID, AvailableSiteDoorNumberID, DoorNumber, TimeSlot, AvailableFixedSlotID, Weekday, SlotTimeOrderByID) {


            var constwarning = true;
            var constApplied = false;



            if (document.getElementById('<%=hdnCurrentWeekDay.ClientID%>').value == Weekday) {
                if (parseInt(SlotTimeOrderByID) < parseInt(document.getElementById('<%=hdnBeforeSlotTimeID.ClientID%>').value)
                || parseInt(SlotTimeOrderByID) > parseInt(document.getElementById('<%=hdnAfterSlotTimeID.ClientID%>').value)) {
                    constApplied = true;
                }
            }


            if (constApplied == true) {
                if (document.getElementById('<%=hdnCurrentRole.ClientID%>').value != "Vendor") {
                    constwarning = confirm('<%=BK_ALT_04_INT%>'); //"There is a constraint against the vendor to deliver on this Time, do you want to continue?"
                }
                if (document.getElementById('<%=hdnCurrentRole.ClientID%>').value == "Vendor") {
                    constwarning = false;
                    alert('<%=BK_ALT_TimeNotAvailable%>'); // ("This Time is not available for your booking. Please select another Time or contact OD to make booking on this Time.");
                }
            }

            var res;

            if (constApplied == true) {
                res = constwarning;
            }
            else if (constApplied == false) {
                var confMsg = '<%=BK_ALT_MoveBooking%>' + '  \n\n' + '<%=BK_ALT_Selected_Time%>' + '  - ' + TimeSlot + '                  <%=BK_ALT_Selected_Door%>' + ' # - ' + DoorNumber;
                res = confirm(confMsg); //  ("Are you Sure you want to move current booking to the selected slot. \n\nSelected Time - " + TimeSlot + "               Selected Door # - " + DoorNumber);
            }

            if (res) {

                document.getElementById('<%=hdnSuggestedSlotTimeID.ClientID%>').value = AvailableSlotTimeID;
                document.getElementById('<%=hdnSuggestedSlotTime.ClientID%>').value = TimeSlot;
                document.getElementById('<%=hdnSuggestedSiteDoorNumberID.ClientID%>').value = AvailableSiteDoorNumberID;
                document.getElementById('<%=hdnSuggestedSiteDoorNumber.ClientID%>').value = DoorNumber;
                document.getElementById('<%=hdnSuggestedWindowSlotID.ClientID%>').value = "-1";
                document.getElementById('<%=hdnCurrentFixedSlotID.ClientID%>').value = AvailableFixedSlotID;
                document.getElementById('<%=hdnSuggestedWeekDay.ClientID%>').value = Weekday;

                if (document.getElementById('<%=hdnCurrentRole.ClientID%>').value != "Vendor") {



                }
                if (document.getElementById('<%=hdnCurrentRole.ClientID%>').value == "Vendor") {



                }
            }
        }

        function setAletrnateSlot(CapacityWarning, AvailableSlotTimeID, AvailableSiteDoorNumberID, DoorNumber, AvailableFixedSlotID, TimeSlot, Weekday) {

            var capwarning = true;

            if (CapacityWarning == "true") {
                if (document.getElementById('<%=hdnCurrentRole.ClientID%>').value != "Vendor") {
                    var confMsg = '<%=BK_ALT_Insufficeint_Space_Ext%>' + '  \n\n' + '<%=BK_ALT_Selected_Time%>' + '  - ' + TimeSlot + '                  <%=BK_ALT_Selected_Door%>' + ' # - ' + DoorNumber;
                    capwarning = confirm(confMsg); //  ("There is insufficeint space for this delivery in the slot selected. Please confirm that this slot is correct.\n\nSelected Time - " + TimeSlot + "               Selected Door # - " + DoorNumber);
                }
                if (document.getElementById('<%=hdnCurrentRole.ClientID%>').value == "Vendor") {
                    //                alert('<%=BK_ALT_Insufficeint_Space_Int%>'); // ("There is insufficeint space for this delivery in the slot selected.Please select another slot.");
                    var confMsg = '<%=BK_ALT_Insufficeint_Space_Ext%>' + '  \n\n' + '<%=BK_ALT_Selected_Time%>' + '  - ' + TimeSlot + '                  <%=BK_ALT_Selected_Door%>' + ' # - ' + DoorNumber;
                    capwarning = confirm(confMsg); //  ("There is insufficeint space for this delivery in the slot selected. Please confirm that this slot is correct.\n\nSelected Time - " + TimeSlot + "               Selected Door # - " + DoorNumber);

                }
            }



            if (capwarning == true) {
                document.getElementById('<%=hdnSuggestedSlotTimeID.ClientID%>').value = AvailableSlotTimeID;
                document.getElementById('<%=hdnSuggestedSlotTime.ClientID%>').value = TimeSlot;
                document.getElementById('<%=hdnSuggestedSiteDoorNumberID.ClientID%>').value = AvailableSiteDoorNumberID;
                document.getElementById('<%=hdnSuggestedSiteDoorNumber.ClientID%>').value = DoorNumber;
                document.getElementById('<%=hdnSuggestedWindowSlotID.ClientID%>').value = AvailableFixedSlotID;
                document.getElementById('<%=hdnCapacityWarning.ClientID%>').value = CapacityWarning;
                document.getElementById('<%=hdnGraySlotClicked.ClientID%>').value = CapacityWarning;
                document.getElementById('<%=hdnSuggestedWeekDay.ClientID%>').value = Weekday;

                var res = true;

                if (CapacityWarning != "true") {
                    var confMsg = '<%=BK_ALT_MoveBooking%>' + '  \n\n' + '<%=BK_ALT_Selected_Time%>' + '  - ' + TimeSlot + '\t\t <%=BK_ALT_Selected_Door%>' + ' # - ' + DoorNumber;
                    res = confirm(confMsg); // ("Are you Sure you want to move current booking to the selected slot.\n\nSelected Time - " + TimeSlot + "\t\t Selected Door # - " + DoorNumber);
                }
            }
        }


        function setAletrnateSlotForVendor(CapacityWarning, AvailableSlotTimeID, AvailableSiteDoorNumberID, DoorNumber, AvailableFixedSlotID, TimeSlot, Weekday) {

            var capwarning = true;

            if (CapacityWarning == "true") {
                //var confMsg = "This request has been allocated a provisional booking slot.  Due to the criteria entered, Office Depot will need to check and acknowledge the booking details. \n\nIf there is a need for this booking to be rearranged Office Depot will contact you using the details that you provided at registration.Please confirm that this slot is correct.\n\nSelected Time - " + TimeSlot + "               Selected Door # - " + DoorNumber);
                capwarning = confirm("This request has been allocated a provisional booking slot.  Due to the criteria entered, Office Depot will need to check and acknowledge the booking details. \n\nIf there is a need for this booking to be rearranged Office Depot will contact you using the details that you provided at registration.Please confirm that this slot is correct.\n\nSelected Time - " + TimeSlot + "               Selected Door # - " + DoorNumber); //  ("There is insufficeint space for this delivery in the slot selected. Please confirm that this slot is correct.\n\nSelected Time - " + TimeSlot + "               Selected Door # - " + DoorNumber);
            }

            if (capwarning == true) {
                document.getElementById('<%=hdnSuggestedSlotTimeID.ClientID%>').value = AvailableSlotTimeID;
                document.getElementById('<%=hdnSuggestedSlotTime.ClientID%>').value = TimeSlot;
                document.getElementById('<%=hdnSuggestedSiteDoorNumberID.ClientID%>').value = AvailableSiteDoorNumberID;
                document.getElementById('<%=hdnSuggestedSiteDoorNumber.ClientID%>').value = DoorNumber;
                document.getElementById('<%=hdnSuggestedWindowSlotID.ClientID%>').value = AvailableFixedSlotID;
                document.getElementById('<%=hdnCapacityWarning.ClientID%>').value = CapacityWarning;
                document.getElementById('<%=hdnGraySlotClicked.ClientID%>').value = CapacityWarning;
                document.getElementById('<%=hdnSuggestedWeekDay.ClientID%>').value = Weekday;

                var res = true;

                if (CapacityWarning != "true") {
                    var confMsg = '<%=BK_ALT_MoveBooking%>' + '  \n\n' + '<%=BK_ALT_Selected_Time%>' + '  - ' + TimeSlot + '\t\t <%=BK_ALT_Selected_Door%>' + ' # - ' + DoorNumber;
                    res = confirm(confMsg); // ("Are you Sure you want to move current booking to the selected slot.\n\nSelected Time - " + TimeSlot + "\t\t Selected Door # - " + DoorNumber);
                }
            }
        }

        function setScroll() {
            if ($('.fixedTable') != null) {
                $('.fixedTable').scrollTop(document.getElementById('<%=scroll1.ClientID%>').value);
                $('.fixedTable').scrollLeft(document.getElementById('<%=scroll2.ClientID%>').value);
            }
        }

        function getScroll1(ctrDiv) {
            document.getElementById('<%=scroll1.ClientID%>').value = ctrDiv.scrollTop; // 
            document.getElementById('<%=scroll2.ClientID%>').value = ctrDiv.scrollLeft;
        }

        function getScroll2(ctrDiv) {
            document.getElementById('<%=scroll1.ClientID%>').value = ctrDiv.scrollTop;
            document.getElementById('<%=scroll2.ClientID%>').value = ctrDiv.scrollLeft; // 
        }


        document.onmousemove = positiontip

        function disableEnterKey(e) {
            var key;
            if (window.event)
                key = window.event.keyCode; //IE
            else
                key = e.which; //firefox     

            return (key != 13);
        }

        document.onkeypress = disableEnterKey;

        function WebForm_PostBackOptions(eventTarget, eventArgument, validation, validationGroup, actionUrl, trackFocus, clientSubmit) {
            __doPostBack(eventTarget, eventArgument);
        }    
              
    </script>
    <input type="hidden" id="scroll1" runat="server" />
    <input type="hidden" id="scroll2" runat="server" />
    <div id="divVolume" runat="server" class="balloonstyle">
    </div>
    <div id="dhtmltooltip" class="balloonstyle">
    </div>
    <iframe id="iframetop" scrolling="no" frameborder="0" class="iballoonstyle" width="0"
        height="0"></iframe>
    <div class="balloonstyle" id="ssn123">
        <span class="bla8blue">
            <cc1:ucLabel ID="lblNormal" runat="server" Text="Normal"></cc1:ucLabel>
            <br />
            <cc1:ucLabel ID="lblNormalDeliveryDesc" runat="server" Text="This delivery type accounts for 99% of deliveries and relates to a requests to confirm a standard / fixed booking slot or alternatively a request for a booking slot against a Office Depot Purchase Order Number"></cc1:ucLabel>
            <br />
            <br />
            <cc1:ucLabel ID="lblInitialDistribution" runat="server" Text="Initial Distribution"></cc1:ucLabel><br />
            <cc1:ucLabel ID="lblInitialDistributionDesc" runat="server" Text="This delivery type relate to stock order for a mail out"></cc1:ucLabel>
            <br />
            <br />
            <cc1:ucLabel ID="lblReservation" runat="server" Text="Reservation"></cc1:ucLabel><br />
            <cc1:ucLabel ID="lblReservationDesc" runat="server" Text="This delivery type is when the stock is not order on a Office Depot Purchase Order. It is customer owned stock ordered directly by the customer"></cc1:ucLabel>
            <br />
            <br />
            <cc1:ucLabel ID="lblInternalSupplies" runat="server" Text="Internal Supplies"></cc1:ucLabel><br />
            <cc1:ucLabel ID="lblInternalSuppliesDesc" runat="server" Text="This delivery type relates to deliveries for Office Depot internal supplies - e.g.carton materials"></cc1:ucLabel>
        </span>
    </div>
    <div class="balloonstyle" id="ssn456">
        <span class="bla8blue">
            <cc1:ucLabel ID="lblCarrierDesc1" runat="server" Text="If the carrier who is making the delivery on your behalf is not listed, please select OTHER. You will then be asked to enter the name of the carrier which may then be added to the list for future deliveries."></cc1:ucLabel>
            <br />
            <cc1:ucLabel ID="lblCarrierDesc2" runat="server" Text="If you do not know the name of the carrier who is to make the delivery then select UNKNOWN. Please note that vehicle type is used to determine what slot and door is to be given to your booking. If the vehicle is unknown then this could lead to delays when unloading your vehicle"></cc1:ucLabel>
        </span>
    </div>
    <div class="balloonstyle" id="ssn789">
        <span class="bla8blue">
            <cc1:ucLabel ID="lblVehicleDesc1" runat="server" Text="If the vehicle type that is making the delivery is not listed, please select OTHER. You will then be asked to enter the type of vehicle. This may then be available for selection for future deliveries."></cc1:ucLabel>
            <br />
            <cc1:ucLabel ID="lblVehicleDesc2" runat="server" Text="If you do not know what type of vehicle is to be used then please select UNKNOWN."></cc1:ucLabel>
        </span>
    </div>
    <div class="balloonstyle" id="ssn101">
        <span class="bla8blue">
            <cc1:ucLabel ID="lblPalletsDesc" runat="server" Text="When pallets are stacked on top of each other and can be 'lifted' in one motion from the vehicle, this is what we define as a lift. EG if one pallet is placed on top of another and can be safely taken from the vehicle in one motion then this would be 2 pallets and 1 lift."></cc1:ucLabel>
        </span>
    </div>
    <h2>
        <cc1:ucLabel ID="lblBooking" runat="server" Text="Booking"></cc1:ucLabel>
    </h2>
    <asp:HiddenField ID="hdnPreservSlot" runat="server" />
    <asp:HiddenField ID="hdnStartTimeID" runat="server" />
    <asp:HiddenField ID="hdnEndTimeID" runat="server" />
    <asp:HiddenField ID="hdnWindowSlotMode" runat="server" />
    <asp:HiddenField ID="hdnCurrentMode" runat="server" />
    <asp:HiddenField ID="hdnBookingRef" runat="server" />
    <asp:HiddenField ID="hdnCapacityWarning" runat="server" />
    <asp:HiddenField ID="hdnCurrentRole" runat="server" />
    <asp:HiddenField ID="hdnSchedulerdate" runat="server" />
    <asp:HiddenField ID="hdnSuggestedSlotTimeID" runat="server" />
    <asp:HiddenField ID="hdnSuggestedSlotTime" runat="server" />
    <asp:HiddenField ID="hdnSuggestedWindowSlotID" runat="server" />
  
    <asp:HiddenField ID="hdnSuggestedSiteDoorNumber" runat="server" />
    <asp:HiddenField ID="hdnCurrentFixedSlotID" runat="server" />
    <asp:HiddenField ID="hdnGraySlotClicked" runat="server" />
    <asp:HiddenField ID="hdnSuggestedWeekDay" runat="server" />
    <asp:HiddenField ID="hdnBeforeSlotTimeID" runat="server" />
    <asp:HiddenField ID="hdnAfterSlotTimeID" runat="server" />
    <asp:HiddenField ID="hdnCurrentWeekDay" runat="server" />
    <asp:HiddenField ID="hdnLogicalScheduleDate" runat="server" />
    <asp:HiddenField ID="hdnActualScheduleDate" runat="server" />
    <asp:HiddenField ID="hdnDoorReference" runat="server" />
    <asp:HiddenField ID="hdnBookingTypeID" runat="server" />
　　<asp:HiddenField ID="hdnProvisionalReason" runat="server" />
   <asp:HiddenField ID="hdnLine" runat="server" />
    <asp:HiddenField ID="hdnIsBookingAccepted" runat="server" />

    <asp:HiddenField ID="hdnPreviousVehicleType" runat="server" />
    <div class="right-shadow">
        <div class="formbox">
            <cc1:ucMultiView ID="mvBookingEdit" runat="server" ActiveViewIndex="0">
                <cc1:ucView ID="vwBookingType" runat="server">
                    <cc1:ucPanel ID="pnlSupplierType" runat="server" GroupingText="Supplier Type" CssClass="fieldset-form">
                        <table width="50%" cellspacing="5" align="center" cellpadding="0" border="0" class="top-settingsNoBorder">
                            <tr>
                                <td align="center">
                                    <cc1:ucButton ID="btnVendor" runat="server" Text="Vendor" CssClass="button" OnClick="btnVendor_Click" />
                                    &nbsp;&nbsp;
                                    <cc1:ucButton ID="btnCarrier" runat="server" Text="Carrier" CssClass="button" OnClick="btnCarrier_Click" />
                                </td>
                            </tr>
                        </table>
                    </cc1:ucPanel>
                </cc1:ucView>
                <cc1:ucView ID="vwBookingSetup" runat="server">
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Always">
                        <ContentTemplate>
                            <asp:HiddenField ID="hdnRemainingPallets" runat="server" />
                            <asp:HiddenField ID="hdnRemainingLines" runat="server" />
                            <asp:ValidationSummary ID="vSummary" runat="server" ShowMessageBox="true" ShowSummary="false"
                                Style="color: Red" ValidationGroup="DeliveryDetail" />
                            <asp:RequiredFieldValidator ID="rfvDeliveryTypeRequired" runat="server" ControlToValidate="ddlDeliveryType"
                                Display="None" ValidationGroup="DeliveryDetail" SetFocusOnError="true" InitialValue="0">
                            </asp:RequiredFieldValidator>
                            <asp:RequiredFieldValidator ID="rfvCarrierRequired" runat="server" ControlToValidate="ddlCarrier"
                                Display="None" ValidationGroup="DeliveryDetail" SetFocusOnError="true" InitialValue="0">
                            </asp:RequiredFieldValidator>
                            <asp:RequiredFieldValidator ID="rfvVehicleTypeRequired" runat="server" ControlToValidate="ddlVehicleType"
                                Display="None" ValidationGroup="DeliveryDetail" SetFocusOnError="true" InitialValue="0">
                            </asp:RequiredFieldValidator>
                            <asp:RequiredFieldValidator ID="rfvSchedulingdateRequired" runat="server" ControlToValidate="txtSchedulingdate$txtUCDate"
                                Display="None" ValidationGroup="DeliveryDetail" SetFocusOnError="true">
                            </asp:RequiredFieldValidator>
                            <asp:RequiredFieldValidator ID="rfvCarrierOtherRequired" runat="server" ErrorMessage="xxx"
                                ControlToValidate="txtCarrierOther" Display="None" ValidationGroup="DeliveryDetail"
                                SetFocusOnError="true" Enabled="false">
                            </asp:RequiredFieldValidator>
                            <asp:RequiredFieldValidator ID="rfvVehicleTypeOtherRequired" runat="server" ErrorMessage="xxx"
                                ControlToValidate="txtVehicleOther" Display="None" ValidationGroup="DeliveryDetail"
                                SetFocusOnError="true" Enabled="false">
                            </asp:RequiredFieldValidator>
                            <cc1:ucPanel ID="pnlDeliveryDetail" runat="server" GroupingText="Delivery Detail"
                                CssClass="fieldset-form">
                                <table width="100%" cellspacing="5" cellpadding="0" border="0" class="top-settingsNoBorder"
                                    onmouseover="setToolTip();">
                                    <tr>
                                        <td style="font-weight: bold; width: 10%;">
                                            <cc1:ucLabel ID="lblSitePrefixName" runat="server" Text="Site" isRequired="true"></cc1:ucLabel>
                                        </td>
                                        <td style="font-weight: bold; width: 3%;">
                                            :
                                        </td>
                                        <td style="font-weight: bold; width: 33%;">
                                            <span id="spSite" runat="server">
                                                <cc2:ucSite ID="ddlSite" runat="server" />
                                            </span>
                                            <cc1:ucLiteral ID="ltSelectedSite" runat="server" Visible="false"></cc1:ucLiteral>
                                        </td>
                                        <td style="font-weight: bold;" width="10%">
                                            <cc1:ucLabel ID="lblVendorNo" runat="server" Text="Vendor No" isRequired="true"></cc1:ucLabel>
                                        </td>
                                        <td style="font-weight: bold; width: 3%">
                                            :
                                        </td>
                                        <td style="font-weight: bold; width: 41%">
                                            <span id="spVender" runat="server">
                                                <cc3:ucSeacrhVendor ID="ucSeacrhVendor1" runat="server" />
                                            </span>
                                            <cc1:ucLiteral ID="ltSearchVendorName" runat="server" Visible="false"></cc1:ucLiteral>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: bold;">
                                            <cc1:ucLabel ID="lblDeliveryType" runat="server" Text="Delivery Type" isRequired="true"></cc1:ucLabel>
                                        </td>
                                        <td style="font-weight: bold;">
                                            :
                                        </td>
                                        <td style="font-weight: bold; width: 27%">
                                            <cc1:ucDropdownList ID="ddlDeliveryType" runat="server" Width="150px" OnSelectedIndexChanged="ddlDeliveryType_SelectedIndexChanged"
                                                AutoPostBack="true" OnChange="javascript:WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(this.name, '', true, '', '', false, true))">
                                            </cc1:ucDropdownList>
                                            <a href="javascript:void(0)" rel="ssn123" tipwidth="700" onmouseover="ddrivetip('ssn123','#ededed','700');"
                                                onmouseout="hideddrivetip();">
                                                <img src="../../../Images/info_button1.gif" align="absMiddle" border="0"></a>
                                        </td>
                                        <td style="font-weight: bold;">
                                            <cc1:ucLabel ID="lblSchedulingdate" runat="server" Text="Scheduling date" isRequired="true"></cc1:ucLabel>
                                        </td>
                                        <td style="font-weight: bold;">
                                            :
                                        </td>
                                        <td style="font-weight: bold;">
                                            <span id="spSCDate" runat="server">
                                                <cc2:ucDate ID="txtSchedulingdate" runat="server" AutoPostBack="true" />
                                            </span>
                                            <cc1:ucLiteral ID="ltSCdate" runat="server" Visible="false"></cc1:ucLiteral>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: bold; width: 10%;">
                                            <cc1:ucLabel ID="lblCarrier" runat="server" Text="Carrier" isRequired="true"></cc1:ucLabel>
                                        </td>
                                        <td style="font-weight: bold; width: 3%;">
                                            :
                                        </td>
                                        <td style="font-weight: bold; width: 33%;">
                                            <cc1:ucDropdownList ID="ddlCarrier" runat="server" Width="150px" OnSelectedIndexChanged="ddlCarrier_SelectedIndexChanged"
                                                AutoPostBack="true" OnChange="javascript:WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(this.name, '', true, '', '', false, true))">
                                            </cc1:ucDropdownList>
                                            <a href="javascript:void(0)" rel="ssn456" tipwidth="700" onmouseover="ddrivetip('ssn456','#ededed','700');"
                                                onmouseout="hideddrivetip();">
                                                <img src="../../../Images/info_button1.gif" align="absMiddle" border="0"></a>
                                            &nbsp;&nbsp;
                                            <cc1:ucTextbox ID="txtCarrierOther" runat="server" Width="100px" MaxLength="50" Style="display: none;"></cc1:ucTextbox>
                                            <cc1:ucLabel ID="lblCarrierAdvice" runat="server" Text="Please advise of carrier"
                                                Style="display: none;"></cc1:ucLabel>
                                        </td>
                                        <td style="font-weight: bold;" width="15%">
                                            <cc1:ucLabel ID="lblBookingComments" runat="server" Text="Additional Info" isRequired="true"></cc1:ucLabel>
                                        </td>
                                        <td style="font-weight: bold; width: 3%">
                                            :
                                        </td>
                                        <td style="font-weight: bold; width: 36%">
                                            <cc1:ucTextbox ID="txtBookingComment" runat="server" Width="250px" MaxLength="35"></cc1:ucTextbox>
                                            <cc1:ucLabel ID="lblBookingCommentAdvise" runat="server" Text="Please advise of Additional Info"
                                                Style="display: none;"></cc1:ucLabel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: bold;">
                                            <cc1:ucLabel ID="lblVehicleType" runat="server" Text="Vehicle Type" isRequired="true"></cc1:ucLabel>
                                        </td>
                                        <td style="font-weight: bold;">
                                            :
                                        </td>
                                        <td style="font-weight: bold;">
                                            <cc1:ucDropdownList ID="ddlVehicleType" runat="server" Width="150px" OnSelectedIndexChanged="ddlVehicleType_SelectedIndexChanged"
                                                AutoPostBack="true" OnChange="javascript:WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(this.name, '', true, '', '', false, true))">
                                            </cc1:ucDropdownList>
                                            <a href="javascript:void(0)" rel="ssn789" tipwidth="700" onmouseover="ddrivetip('ssn789','#ededed','700');"
                                                onmouseout="hideddrivetip();">
                                                <img src="../../../Images/info_button1.gif" align="absMiddle" border="0"></a>
                                            &nbsp;&nbsp;
                                            <cc1:ucTextbox ID="txtVehicleOther" runat="server" Width="100px" MaxLength="50" Style="display: none;"></cc1:ucTextbox>  <cc1:ucLabel ID="lblVehicleAdvice" runat="server" Text="Please advise of vehicle type"
                                                Style="display: none;"></cc1:ucLabel>
                                        </td>
                                        <div runat="server" id="divEnableHazardouesItemPrompt" visible="false"> 
                                          <td style="font-weight: bold;">
                                           <cc1:ucLabel ID="lblEnableHazardouesItemPrompt" Text="Does the delivery contain potential hazardous products with an UN number?" isRequired="true" runat="server"></cc1:ucLabel>
                                        </td>
                                        <td style="font-weight: bold;">:
                                        </td>
                                          <td style="font-weight: bold;">
                                             <cc1:ucCheckbox ID="checkBoxEnableHazardouesItemPrompt"  runat="server" />
                                        </td>
                                            </div>
                                    </tr>
                                </table>
                            </cc1:ucPanel>

                        <cc1:ucPanel ID="pnlPalletCheckforCountry" runat="server" GroupingText="Booking Pallet Check"  CssClass="fieldset-form">
                            <asp:HiddenField ID="hdnCountryPallectChecking" runat="server" Value="0" />
                                <table cellspacing="5" cellpadding="0" border="0" >                        
                                    <tr>
                                    <td style="font-weight: bold;"> 
                                    <cc1:ucLabel ID="lblBookingCamefromOutsideUK" runat="server"  Text="Does the delivery originate from another country? (If so, please select checkbox)" 
                                    isRequired="true"></cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold;"> <asp:CheckBox ID="chkSiteSetting" runat="server" AutopostBack="true"
                                         OnCheckedChanged="chkSiteSetting_CheckedChanged1" />
                                    </td>
                                    </tr>

                                    <tr id="CountryData" runat="server" visible="false">
                                    <td style="font-weight: bold;"> 
                                    <cc1:ucLabel ID="lblFromCountry" runat="server"  Text="Country of Origin" isRequired="true"></cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold;"> <cc2:ucCountry runat="server" ID="ddlSourceCountry" /></td>
                                    </tr>
                                </table>
                            </cc1:ucPanel>



                            <cc1:ucPanel ID="pnlBookingDetailsArrival" runat="server" CssClass="fieldset-form" Visible="false">
              
                <table width="100%" cellspacing="5" cellpadding="0" border="0" class="form-table">
                    <tr>
                        <td>
                            <cc1:ucGridView ID="gvPOHistory" runat="server" AutoGenerateColumns="false" CssClass="grid"
                                CellPadding="0" Width="100%">
                                <AlternatingRowStyle BackColor="White" ForeColor="#284775"></AlternatingRowStyle>
                                <Columns>
                                    <asp:BoundField HeaderText="PO" DataField="PurchaseNumber" SortExpression="PurchaseNumber">
                                        <HeaderStyle Width="10%" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="Original Due Date" DataField="Original_due_date" SortExpression="Original_due_date">
                                        <HeaderStyle Width="10%" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="Vendor Name" DataField="VendorName" SortExpression="VendorName">
                                        <HeaderStyle Width="20%" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="# Outstanding Lines on PO" DataField="OutstandingLines"
                                        SortExpression="OutstandingLines">
                                        <HeaderStyle Width="10%" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="# Stock outs" DataField="Qty_On_Hand" SortExpression="Qty_On_Hand">
                                        <HeaderStyle Width="10%" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="# Backorders" DataField="qty_on_backorder" SortExpression="qty_on_backorder">
                                        <HeaderStyle Width="10%" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="Destination" DataField="SiteName" SortExpression="SiteName">
                                        <HeaderStyle Width="20%" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                </Columns>
                            </cc1:ucGridView>
                        </td>
                    </tr>
                </table>
                  <table width="110%" style="align:center;" cellspacing="5" cellpadding="0" class="form-table">                
                    <tr>
                        <td style="width: 10%;">
                            <cc1:ucLabel ID="lblPalletsBooking" runat="server" Text="Pallets" />
                        </td>
                        <td style="width: 1%;">
                            :
                        </td>
                        <td style="width: 10%;" class="nobold">
                            <cc1:ucLabel ID="BookingPalletsData" runat="server"  />
                        </td>
                        <td style="width: 10%;">
                            <cc1:ucLabel ID="lblCartons" runat="server" Text="Cartons" />
                        </td>
                        <td style="width: 1%;">
                            :
                        </td>
                        <td style="width: 10%;" class="nobold">
                            <cc1:ucLabel ID="BookingCartonsData" runat="server"  />
                        </td>
                        <td style="width: 10%;">
                            <cc1:ucLabel ID="lblLines" runat="server" Text="Lines" />
                        </td>
                        <td style="width: 1%;">
                            :
                        </td>
                        <td style="width: 10%;" class="nobold">
                            <cc1:ucLabel ID="BookingLinesData" runat="server"  />
                        </td>
                        <td style="width: 5%;">
                            <cc1:ucLabel ID="lblLiftsBooking" runat="server" Text="Lifts" />
                        </td>
                        <td style="width: 1%;">
                            :
                        </td>
                        <td style="width: 10%;" class="nobold">
                            <cc1:ucLabel ID="BookingLiftsData" runat="server"  />
                        </td>
                    </tr>                 
                </table>
            </cc1:ucPanel>
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td align="center">
                                        <asp:UpdateProgress runat="server" ID="PageUpdateProgress" AssociatedUpdatePanelID="UpdatePanel1">
                                            <ProgressTemplate>
                                                <div align="center" style="background-color: #000; top: 0px; left: 0px; bottom: 0px;
                                                    right: 0px; padding-top: 20%; margin: 0; width: 100%; height: 50%; overflow: hidden;
                                                    position: absolute; z-index: 1000; filter: alpha(opacity=50); opacity: 0.5;">
                                                    <asp:Image ID="imgWait" runat="server" ImageUrl="~/Images/Ajaxloading.gif" ImageAlign="Middle" />
                                                </div>
                                            </ProgressTemplate>
                                        </asp:UpdateProgress>
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="ddlCarrier" />
                            <asp:AsyncPostBackTrigger ControlID="ddlVehicleType" />
                        </Triggers>
                    </asp:UpdatePanel>
                    <div class="button-row">
                        <cc1:ucButton ID="btnProceedAddPO" runat="server" Text="Proceed" CssClass="button"
                            OnClick="btnProceedAddPO_Click" OnClientClick="return chkVendorID();" ValidationGroup="DeliveryDetail" />
                    </div>
                </cc1:ucView>
                <cc1:ucView ID="vwAddPO" runat="server">
                    <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                        <ContentTemplate>

                           

                            <cc1:ucPanel ID="pnlDeliveryDetail_1" runat="server" GroupingText="Delivery Detail"
                                CssClass="fieldset-form">
                                <table width="100%" cellspacing="5" cellpadding="0" border="0" class="top-settingsNoBorder">
                                    <tr>
                                        <td style="font-weight: bold; width: 10%;">
                                            <cc1:ucLabel ID="lblSitePrefixName_1" runat="server" Text="Site"></cc1:ucLabel>
                                        </td>
                                        <td style="font-weight: bold; width: 3%;">
                                            :
                                        </td>
                                        <td style="font-weight: bold; width: 37%;">
                                            <cc1:ucLiteral ID="ltSiteName" runat="server"></cc1:ucLiteral>
                                        </td>
                                        <td style="font-weight: bold;" width="10%">
                                            <cc1:ucLabel ID="lblVendorNo_1" runat="server" Text="Vendor No"></cc1:ucLabel>
                                        </td>
                                        <td style="font-weight: bold; width: 3%">
                                            :
                                        </td>
                                        <td style="font-weight: bold; width: 37%">
                                            <cc1:ucLiteral ID="ltvendorName" runat="server"></cc1:ucLiteral>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: bold;">
                                            <cc1:ucLabel ID="lblDeliveryType_1" runat="server" Text="Delivery Type"></cc1:ucLabel>
                                        </td>
                                        <td style="font-weight: bold;">
                                            :
                                        </td>
                                        <td style="font-weight: bold; width: 27%">
                                            <cc1:ucLiteral ID="ltDeliveryType" runat="server"></cc1:ucLiteral>
                                        </td>
                                        <td style="font-weight: bold;">
                                            <cc1:ucLabel ID="lblCarrier_1" runat="server" Text="Carrier"></cc1:ucLabel>
                                        </td>
                                        <td style="font-weight: bold;">
                                            :
                                        </td>
                                        <td style="font-weight: bold;">
                                            <cc1:ucLiteral ID="ltCarrier" runat="server"></cc1:ucLiteral>
                                            &nbsp;&nbsp;
                                            <cc1:ucLiteral ID="ltOtherCarrier" runat="server" Visible="false"></cc1:ucLiteral>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: bold; width: 10%;">
                                            <cc1:ucLabel ID="lblVehicleType_1" runat="server" Text="Vehicle Type"></cc1:ucLabel>
                                        </td>
                                        <td style="font-weight: bold; width: 3%;">
                                            :
                                        </td>
                                        <td style="font-weight: bold; width: 37%;">
                                            <cc1:ucLiteral ID="ltVehicleType" runat="server"></cc1:ucLiteral>
                                            &nbsp;&nbsp;
                                            <cc1:ucLiteral ID="ltOtherVehicleType" runat="server" Visible="false"></cc1:ucLiteral>
                                        </td>
                                        <td style="font-weight: bold; width: 15%;">
                                            <cc1:ucLabel ID="lblBookingComment" runat="server" Text="Additional Info"></cc1:ucLabel>
                                        </td>
                                        <td style="font-weight: bold; width: 3%;">
                                            :
                                        </td>
                                        <td style="font-weight: bold; width: 32%;">
                                            <cc1:ucTextbox ID="txtEditBookingComment" runat="server" Width="250px" MaxLength="35"></cc1:ucTextbox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: bold;">
                                            <cc1:ucLabel ID="lblSchedulingdate_1" runat="server" Text="Scheduling date"></cc1:ucLabel>
                                        </td>
                                        <td style="font-weight: bold;">
                                            :
                                        </td>
                                        <td style="font-weight: bold;">
                                            <cc1:ucLiteral ID="ltSchedulingDate" runat="server"></cc1:ucLiteral>
                                        </td>
                                          <div runat="server" id="divHazrdous"> 
                                        <td style="font-weight: bold;">
                                           <cc1:ucLabel ID="lblHazardous" Text="Does the delivery contain potential hazardous products with an GHS Number?" runat="server"></cc1:ucLabel>
                                        </td>
                                        <td style="font-weight: bold;">:
                                        </td>
                                          <td style="font-weight: bold;">
                                             <cc1:ucCheckbox ID="chkHazardoustick"  runat="server" />
                                        </td>
                                            </div>
                                    </tr>
                                </table>
                            </cc1:ucPanel>

                            <cc1:ucPanel ID="pnlPalletCheckforCountryDisplay" runat="server" GroupingText="Booking Pallet Check"  CssClass="fieldset-form">                            
                                <table cellspacing="5" cellpadding="0" border="0" class="top-settingsNoBorder" >                        
                                    <tr>
                                         <td style="font-weight: bold; width: 20%;">
                                            <cc1:ucLabel ID="lblBookingCamefromOutsideUK1" runat="server" 
                                                Text="Does the delivery originate from another country?"></cc1:ucLabel>
                                        </td>
                                        <td style="font-weight: bold; width: 3%;">:
                                        </td>
                                        <td style="font-weight: bold; width: 27%;">
                                               <cc1:ucLabel ID="lblYesNo" runat="server" ></cc1:ucLabel>
                                        </td>
                                        <td style="font-weight: bold;" width="10%">
                                          <cc1:ucLabel ID="lblFromCountryNew" runat="server"  Text="Country of Origin"></cc1:ucLabel>
                                        </td>
                                        <td style="font-weight: bold; width: 3%">:
                                        </td>
                                        <td style="font-weight: bold; width: 37%">
                                              <cc1:ucLabel ID="lblFromCountryValue" runat="server"  ></cc1:ucLabel>      
                                        </td> 
                                    </tr>
                                </table>
                            </cc1:ucPanel>

                            <cc1:ucPanel ID="pnlPurchaseOrderDetail" runat="server" GroupingText="Purchase Order Detail"
                                CssClass="fieldset-form">
                                <table width="100%" cellspacing="5" cellpadding="0" border="0" class="top-settingsNoBorder">
                                    <tr>
                                        <td style="font-weight: bold;" width="24%" valign="top">
                                            <cc1:ucLabel ID="lblPurchaseNumber" runat="server" Text="Purchase Number/Delivery Reference"></cc1:ucLabel>
                                        </td>
                                        <td style="font-weight: bold; width: 1%" valign="top">
                                            :
                                        </td>
                                        <td style="font-weight: bold; width: 10%" valign="top">
                                            <cc1:ucTextbox ID="txtPurchaseNumber" runat="server" Width="70px"></cc1:ucTextbox>
                                        </td>
                                        <td style="font-weight: bold;" width="8%" valign="top">
                                            <cc1:ucButton ID="btnAddPO" runat="server" Text="Add PO" CssClass="button" OnClick="btnAddPO_Click"
                                                OnClientClick="return chkVendorID();" />
                                        </td>
                                        <%--Sprint 1 - Point 7 - Start--%>
                                        <td style="font-weight: bold;" width="57%" valign="top">
                                            <cc1:ucButton ID="btnShowPOPopup" runat="server" Text="Show PO" CssClass="button"
                                                OnClick="btnShowPOPopup_Click" />
                                        </td>
                                        <%--Sprint 1 - Point 7 - End--%>
                                    </tr>
                                    <tr>
                                        <td colspan="6">
                                            <cc1:ucLabel ID="lblAllPORequired" runat="server" Text="( Please note all Purchase Orders / Delivery references are required)"></cc1:ucLabel>
                                        </td>
                                    </tr>
                                </table>
                                <table width="100%" cellspacing="5" cellpadding="0" border="0" class="form-table">
                                    <tr>
                                        <td>
                                            <cc1:ucGridView ID="gvPO" runat="server" AutoGenerateColumns="false" CssClass="grid"
                                                CellPadding="0" Width="100%" OnRowDataBound="gvPO_RowDataBound" OnRowDeleting="gvPO_RowDeleting"
                                                DataKeyNames="AutoID,IsProvisionResonChangesUpdate" OnSorting="gvPO_Sorting" AllowSorting="true">
                                                <AlternatingRowStyle BackColor="White" ForeColor="#284775"></AlternatingRowStyle>
                                                <Columns>
                                                    <asp:BoundField HeaderText="PO" DataField="PurchaseNumber" SortExpression="PurchaseNumber">
                                                        <HeaderStyle Width="10%" />
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:BoundField>
                                                    　　<%-- <asp:TemplateField>                                                  
                                                        <ItemTemplate>                                                               
                                                            <asp:HiddenField ID="hdnProvisionResonChangesUpdate" runat="server" Value='<%#Eval("IsProvisionResonChangesUpdate")%>' />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>--%><asp:BoundField HeaderText="Expected Date" DataField="ExpectedDate" SortExpression="ExpectedDate">
                                                        <HeaderStyle Width="10%" />
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:BoundField>
                                                    <asp:BoundField HeaderText="Vendor Name" DataField="VendorName" SortExpression="VendorName">
                                                        <HeaderStyle Width="20%" />
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:BoundField>
                                                    <asp:BoundField HeaderText="# Outstanding Lines on PO" DataField="OutstandingLines"
                                                        SortExpression="OutstandingLines">
                                                        <HeaderStyle Width="10%" />
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:BoundField>
                                                    <asp:BoundField HeaderText="# Stock outs" DataField="Qty_On_Hand" SortExpression="Qty_On_Hand">
                                                        <HeaderStyle Width="10%" />
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:BoundField>
                                                    <asp:BoundField HeaderText="# Backorders" DataField="qty_on_backorder" SortExpression="qty_on_backorder">
                                                        <HeaderStyle Width="10%" />
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:BoundField>
                                                    <asp:BoundField HeaderText="Destination" DataField="SiteName" SortExpression="SiteName">
                                                        <HeaderStyle Width="20%" />
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:BoundField>
                                                    <asp:CommandField HeaderText="Remove" CausesValidation="false" ButtonType="Link"
                                                        ShowDeleteButton="True" DeleteText="Remove" ItemStyle-HorizontalAlign="Center">
                                                        <HeaderStyle Width="10%" />
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:CommandField>
                                                </Columns>
                                            </cc1:ucGridView>
                                        </td>
                                    </tr>
                                </table>
                            </cc1:ucPanel>
                            <cc1:ucPanel ID="pnlVolumeDetails" runat="server" GroupingText="Volume Details" CssClass="fieldset-form">
                                <table width="100%" cellspacing="5" cellpadding="0" border="0" class="top-settingsNoBorder"
                                    onmouseover="setToolTip();">
                                    <tr>
                                        <td colspan="9">
                                            <cc1:ucLabel ID="lblVolumeDetailDesc" runat="server" isRequired="true" Font-Bold="true"></cc1:ucLabel>
                                             <cc1:ucLabel ID="lblVolumeDetailDescProvisional" runat="server" Visible="false" isRequired="true" Font-Bold="true"></cc1:ucLabel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: bold; width: 7%">
                                            <cc1:ucLabel ID="lblNoOfPallets" runat="server" Text="# of Pallets"></cc1:ucLabel>
                                        </td>
                                        <td style="font-weight: bold; width: 1%">
                                            :
                                        </td>
                                        <td style="font-weight: bold; width: 28%">
                                        <asp:Literal ID="lblPalletsProvisional" runat="server" ></asp:Literal>
                                            <cc1:ucNumericTextbox ID="txtPallets" runat="server" onkeyup="AllowNumbersOnly(this);"
                                                MaxLength="3" Width="40px"></cc1:ucNumericTextbox>
                                        </td>
                                        <td style="font-weight: bold; width: 8%">
                                            <cc1:ucLabel ID="lblNoOfCartons" runat="server" Text="# of Cartons"></cc1:ucLabel>
                                        </td>
                                        <td style="font-weight: bold; width: 1%">
                                            :
                                        </td>
                                        <td style="font-weight: bold; width: 27%">
                                         <asp:Literal ID="lblCartonsProvisional" runat="server" ></asp:Literal>
                                            <cc1:ucNumericTextbox ID="txtCartons" runat="server" onkeyup="AllowNumbersOnly(this);"
                                                MaxLength="4" Width="40px"></cc1:ucNumericTextbox>
                                        </td>
                                        <td style="font-weight: bold; width: 16%">
                                            <cc1:ucLabel ID="lblNoOfLifts" runat="server" Text="# of Lifts"></cc1:ucLabel>
                                        </td>
                                        <td style="font-weight: bold; width: 1%">
                                            :
                                        </td>
                                        <td style="font-weight: bold; width: 20%">
                                          <asp:Literal ID="lblLiftsProvisional" runat="server" ></asp:Literal>
                                            <cc1:ucNumericTextbox ID="txtLifts" runat="server" onkeyup="AllowNumbersOnly(this);"
                                                MaxLength="3" Width="40px"></cc1:ucNumericTextbox>
                                            <a href="javascript:void(0)" rel="ssn101" tipwidth="200" onmouseover="ddrivetip('ssn101','#ededed','200');"
                                                onmouseout="hideddrivetip();">
                                                <img src="../../../Images/info_button1.gif" align="absMiddle" border="0"></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: bold;" colspan="4">
                                            <cc1:ucLiteral ID="ltExpectedLines_1" runat="server" Text="We expect XX lines on this Delivery please confirm the number of Lines"></cc1:ucLiteral>
                                        </td>
                                        <td style="font-weight: bold;">
                                            :
                                        </td>
                                        <td style="font-weight: bold;" colspan="2">
                                         <asp:Literal ID="lblExpectedLineProvisional" runat="server"></asp:Literal>
                                            <cc1:ucNumericTextbox ID="txtExpectedLines" runat="server" onkeyup="AllowNumbersOnly(this);"
                                                MaxLength="3" Width="40px"></cc1:ucNumericTextbox>
                                        </td>
                                    </tr>
                                    <tr id="trpreadvise" runat="server">
                                        <td colspan="4" style="font-weight: bold;">
                                            <cc1:ucLabel ID="lblPreAdvice" runat="server" Text="Please advise as to whether any a pre-advise notification has been sent"></cc1:ucLabel>
                                        </td>
                                        <td style="font-weight: bold;">
                                            :
                                        </td>
                                        <td colspan="4">
                                            <cc1:ucRadioButton ID="rdoAdvice1" runat="server" GroupName="Advice" />
                                            <cc1:ucLabel ID="lblPreAdviceSent" runat="server" Text="YES   -  Pre-advise has been sent"></cc1:ucLabel>
                                            &nbsp;&nbsp;
                                            <cc1:ucRadioButton ID="rdoAdvice2" runat="server" GroupName="Advice" />
                                            <cc1:ucLabel ID="lblPreAdviceNotSent" runat="server" Text="NO  -   Pre-advise will NOT be sent"></cc1:ucLabel>
                                        </td>
                                    </tr>
                                </table>
                            </cc1:ucPanel>
　　　
　　　                            <%-- <cc1:ucPanel ID="pnlVolumeDetails_1" runat="server" GroupingText="Volume Details" CssClass="fieldset-form" Visible="false">
                                <table width="100%" cellspacing="5" cellpadding="0" border="0" class="top-settingsNoBorder"
                                    onmouseover="setToolTip();">
                                    <tr>
                                        <td colspan="10">
                                            <cc1:ucLabel ID="lblVolumeDetailDescProvisional" runat="server" isRequired="true" Font-Bold="true"></cc1:ucLabel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: bold; width: 7%">
                                            <cc1:ucLabel ID="lblNoOfPallets_1" runat="server" Text="# of Pallets"></cc1:ucLabel>
                                        </td>
                                        <td style="font-weight: bold; width: 1%">
                                            :
                                        </td>
                                        <td style="font-weight: bold; width: 28%">
                                           <cc1:ucLabel ID="lblPalletsProvisional" runat="server" ></cc1:ucLabel>
                                            <cc1:ucNumericTextbox ID="txtPallets_1" runat="server" onkeyup="AllowNumbersOnly(this);"
                                                MaxLength="3" Width="40px" ForeColor="Red"></cc1:ucNumericTextbox>
                                        </td>
                                        <td style="font-weight: bold; width: 8%">
                                            <cc1:ucLabel ID="lblNoOfCartons_1" runat="server" Text="# of Cartons"></cc1:ucLabel>
                                        </td>
                                        <td style="font-weight: bold; width: 1%">
                                            :
                                        </td>
                                        <td style="font-weight: bold; width: 27%">
                                        <cc1:ucLabel ID="lblCartonsProvisional" runat="server" ></cc1:ucLabel>
                                            <cc1:ucNumericTextbox ID="txtCartons_1" runat="server" onkeyup="AllowNumbersOnly(this);"
                                                MaxLength="4" Width="40px" ForeColor="Red"></cc1:ucNumericTextbox>
                                        </td>
                                        <td style="font-weight: bold; width: 16%">
                                            <cc1:ucLabel ID="lblNoOfLifts_1" runat="server" Text="# of Lifts"></cc1:ucLabel>
                                        </td>
                                        <td style="font-weight: bold; width: 1%">
                                            :
                                        </td>
                                        <td style="font-weight: bold; width: 20%">
                                         <cc1:ucLabel ID="lblLiftsProvisional" runat="server" ></cc1:ucLabel>
                                            <cc1:ucNumericTextbox ID="txtLifts_1" runat="server" onkeyup="AllowNumbersOnly(this);"
                                                MaxLength="3" Width="40px" ForeColor="Red"></cc1:ucNumericTextbox>
                                            <a href="javascript:void(0)" rel="ssn101" tipwidth="200" onmouseover="ddrivetip('ssn101','#ededed','200');"
                                                onmouseout="hideddrivetip();">
                                                <img src="../../../Images/info_button1.gif" align="absMiddle" border="0"></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: bold;" colspan="4">
                                            <cc1:ucLabel ID="lblExpectedLinesProvisional" runat="server" Text=""></cc1:ucLabel>
                                        </td>
                                        <td style="font-weight: bold;">
                                            :
                                        </td>
                                        <td style="font-weight: bold;" colspan="2">
                                         <cc1:ucLabel ID="lblExpectedLineProvisional" runat="server"></cc1:ucLabel>
                                            <cc1:ucNumericTextbox ID="txtExpectedLines_1" runat="server" onkeyup="AllowNumbersOnly(this);"
                                                MaxLength="3" Width="40px" ForeColor="Red"></cc1:ucNumericTextbox>
                                        </td>
                                    </tr>                                
                                </table>
                            </cc1:ucPanel>    --%>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="btnAddPO" EventName="Click" />
                        </Triggers>
                    </asp:UpdatePanel>
                   　<%--　 <div class="button-row" id="divProvisionalProceed" runat="server" visible="false">
                        <cc1:ucButton ID="UcBack" runat="server" Text="Back" CssClass="button" OnClick="btnBack_1_Click" />
                        <cc1:ucButton ID="UcAccept" runat="server" Text="Accept" CssClass="button"
                            OnClick="btnProceedBooking_Click" />
                        <cc1:ucButton ID="UcRejectBooking" runat="server" Text="Reject Booking" CssClass="button" OnClick="btnReject_Click"
                           />
                        <cc1:ucButton ID="UcRejectChanges" runat="server" Text="Reject Changes" CssClass="button" OnClick="btnRejectChanges_Click"
                          />
                    </div>
--%><div class="button-row" id="divProceed" runat="server">
                        <b>
                            <cc1:ucLabel ID="lblPOMessage" runat="server" Text="Please ensure that all Purchase Orders which will be delivered are entered before you proceed with your booking."></cc1:ucLabel>
                            &nbsp;&nbsp; </b>
                        <cc1:ucButton ID="btnBack_1" runat="server" Text="Back" CssClass="button" OnClick="btnBack_1_Click" />
                        <cc1:ucButton ID="btnProceedBooking" runat="server" Text="Proceed" CssClass="button"
                            OnClick="btnProceedBooking_Click" />
                         <cc1:ucButton ID="btnReject" runat="server" Text="Reject" CssClass="button" OnClick="btnReject_Click" Visible="false"/>
                          <cc1:ucButton ID="btnAccept" runat="server" Text="Accept" CssClass="button" Visible="false"
                            OnClick="btnProceedBooking_Click" />
                        <cc1:ucButton ID="btnRejectBooking" runat="server" Text="Reject Booking" Visible="false" CssClass="button" OnClick="btnReject_Click"
                           />
                        
                         <cc1:ucButton ID="btnRejectChanges" runat="server" Text="Reject Changes" Visible="false" CssClass="button" OnClick="btnRejectChanges_Click"/>
                    </div>
                </cc1:ucView>
                <cc1:ucView ID="vwBooking" runat="server">
                    <table width="100%" cellspacing="5" cellpadding="0" border="0">
                        <tr>
                            <td style="font-weight: bold; width: 100%" align="left">
                                <cc1:ucLabel ID="lblProposeWindowBookingInfo" runat="server" Text="Based on the information entered, we propose that your booking is to be between"></cc1:ucLabel>
                                &nbsp;
                                <cc1:ucLiteral ID="ltBookingDate" runat="server"></cc1:ucLiteral>&nbsp;
                                <cc1:ucLiteral ID="ltSuggestedSlotTime" runat="server" Text=""></cc1:ucLiteral>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold; width: 100%" align="left">
                                &nbsp;
                            </td>
                        </tr>
                        <%--<tr>
                            <td colspan="2">
                                <cc1:ucGridView ID="gvBookingSlots" runat="server" AutoGenerateColumns="false" CssClass="grid"
                                    Width="100%" OnRowDataBound="gvBookingSlots_RowDataBound">
                                    <AlternatingRowStyle BackColor="White" ForeColor="#284775"></AlternatingRowStyle>
                                    <Columns>
                                        <asp:BoundField HeaderText="Day" DataField="BookingDay" SortExpression="BookingDay">
                                            <HeaderStyle Width="15%" HorizontalAlign="Left" />
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:BoundField>

                                        <asp:BoundField HeaderText="Date" DataField="BookingDate" SortExpression="BookingDate">
                                            <HeaderStyle Width="15%" HorizontalAlign="Left" />
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:BoundField>

                                        <asp:TemplateField HeaderText="Time" SortExpression="SlotTime.SlotTime">
                                            <HeaderStyle Width="10%" HorizontalAlign="Left" />
                                            <ItemStyle HorizontalAlign="Left" />
                                            <ItemTemplate>
                                                <cc1:ucLiteral ID="ltTime" runat="server" Text='<%#Eval("SlotTime.SlotTime") %>'></cc1:ucLiteral>
                                                <asp:HiddenField ID="hdnAvailableFixedSlotID" runat="server" Value='<%#Eval("FixedSlot.FixedSlotID") %>' />
                                                <asp:HiddenField ID="hdnAvailableSlotTimeID" runat="server" Value='<%#Eval("SlotTime.SlotTimeID") %>' />
                                                <asp:HiddenField ID="hdnAvailableSiteDoorNumberID" runat="server" Value='<%#Eval("DoorNoSetup.SiteDoorNumberID") %>' />
                                                <asp:HiddenField ID="hdnAvailableSiteDoorNumber" runat="server" Value='<%#Eval("DoorNoSetup.DoorNumber") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField HeaderText="Booking Ref" DataField="BookingRef" SortExpression="BookingRef">
                                            <HeaderStyle Width="15%" HorizontalAlign="Left" />
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:TemplateField HeaderText="Door Name" SortExpression="DoorNoSetup.DoorNumber">
                                            <HeaderStyle Width="15%" HorizontalAlign="Left" />
                                            <ItemStyle HorizontalAlign="Left" />
                                            <ItemTemplate>
                                                <cc1:ucLiteral ID="ltDoorNumber" runat="server" Text='<%#Eval("DoorNoSetup.DoorNumber") %>'></cc1:ucLiteral>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField HeaderText="Slot Type" DataField="SlotType" SortExpression="SlotType">
                                            <HeaderStyle Width="15%" HorizontalAlign="Left" />
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:BoundField HeaderText="Status" DataField="Status" SortExpression="Status">
                                            <HeaderStyle Width="15%" HorizontalAlign="Left" />
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:BoundField>
                                    </Columns>
                                </cc1:ucGridView>   
                            </td>
                        </tr>--%>
                        <tr>
                            <td style="font-weight: bold;" colspan="2" align="right">
                                <cc1:ucButton ID="btnConfirmBooking" runat="server" Text="Confirm Booking" CssClass="button"
                                    OnClick="btnConfirmBooking_Click" />                                    
                                &nbsp;
                                  <cc1:ucButton ID="btnNotHappy" runat="server" Text="Not Happy" CssClass="button" Visible="false"
                                    OnClick="btnNotHappy_Click"  />
                                &nbsp;
                                <%--<cc1:ucButton ID="btnConfirmBookingWithMail" runat="server" Text="Confirm Booking With Mail"
                                    CssClass="button" Visible="false" OnClick="btnConfirmBookingWithMail_Click"  />
                                &nbsp;
                                <cc1:ucButton ID="btnConfirmBookingWithoutMail" runat="server" Text="Confirm Booking Without Mail"
                                    CssClass="button" Visible="false" OnClick="btnConfirmBookingWithoutMail_Click" />
                                &nbsp;--%>
                                <cc1:ucButton ID="btnWindowAlternateSlotVendor" runat="server" CssClass="button"
                                    Text="Request Alternate Time" OnClick="btnWindowAlternateSlotVendor_Click" />
                                &nbsp;
                                <cc1:ucButton ID="btnWindowAlternateSlot" runat="server" CssClass="button" Text="Enter Alternate Time"
                                    OnClick="btnWindowAlternateSlot_Click" />
                            </td>
                        </tr>
                    </table>
                </cc1:ucView>
                <cc1:ucView ID="vwAlternateSlotOD" runat="server">
                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                        <ContentTemplate>
                            <asp:ValidationSummary ID="vWSummary" runat="server" ShowMessageBox="true" ShowSummary="false"
                                Style="color: Red" ValidationGroup="AlternateWindow" />
                            <asp:CompareValidator ID="cmpvStartTimeCanNotBeGreaterEndTime" runat="server" ControlToValidate="ddlFrom"
                                ControlToCompare="ddlTo" Operator="LessThan" Type="Integer" Display="None" ValidationGroup="AlternateWindow">
                            </asp:CompareValidator>
                            <asp:RequiredFieldValidator ID="rfvAlternateWindowRequired" runat="server" ControlToValidate="ddlAlternateWindow"
                                Display="None" ValidationGroup="AlternateWindow" SetFocusOnError="true" InitialValue="0">
                            </asp:RequiredFieldValidator>
                            <asp:RequiredFieldValidator ID="rfvFromRequired" runat="server" ControlToValidate="ddlFrom"
                                Display="None" ValidationGroup="AlternateWindow" SetFocusOnError="true" InitialValue="0">
                            </asp:RequiredFieldValidator>
                            <asp:RequiredFieldValidator ID="rfvToRequired" runat="server" ControlToValidate="ddlTo"
                                Display="None" ValidationGroup="AlternateWindow" SetFocusOnError="true" InitialValue="0">
                            </asp:RequiredFieldValidator>
                            <asp:RequiredFieldValidator ID="rfvDoorNameRequired" runat="server" ControlToValidate="ddlDoorName"
                                Display="None" ValidationGroup="AlternateWindow" SetFocusOnError="true" InitialValue="0">
                            </asp:RequiredFieldValidator>
                            <cc1:ucPanel ID="pnlSystemRecommendedSlotDetails" runat="server" GroupingText="System Recommended Slot Details"
                                CssClass="fieldset-form">
                                <table width="100%" cellspacing="5" cellpadding="0" border="0" class="form-table">
                                    <tr>
                                        <td style="font-weight: bold;" colspan="3">
                                            <cc1:ucRadioButton ID="rdoStandardWindow" runat="server" Text="Standard Window" GroupName="RecomendedWindow"
                                                AutoPostBack="true" OnCheckedChanged="rdoStandardWindow_CheckedChanged"></cc1:ucRadioButton>
                                        </td>
                                        <td style="font-weight: bold;" colspan="3">
                                            <cc1:ucRadioButton ID="rdoNonStandardWindow" runat="server" Text="Non-Standard Window"
                                                GroupName="RecomendedWindow" AutoPostBack="true" OnCheckedChanged="rdoNonStandardWindow_CheckedChanged">
                                            </cc1:ucRadioButton>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="8%" style="font-weight: bold;">
                                            <cc1:ucLabel ID="lblSelectWindow" runat="server" Text="Select Window"></cc1:ucLabel>
                                        </td>
                                        <td width="1%" style="font-weight: bold;">
                                            :
                                        </td>
                                        <td width="25%" class="nobold">
                                            <cc1:ucDropdownList ID="ddlAlternateWindow" runat="server" Width="150px" OnSelectedIndexChanged="ddlAlternateWindow_SelectedIndexChanged"
                                                AutoPostBack="true">
                                            </cc1:ucDropdownList>
                                        </td>
                                        <td width="8%" style="font-weight: bold;">
                                            <cc1:ucLabel ID="lblFrom" runat="server" Text="From"></cc1:ucLabel>
                                        </td>
                                        <td width="1%" style="font-weight: bold;">
                                            :
                                        </td>
                                        <td width="20%" class="nobold">
                                            <cc1:ucDropdownList ID="ddlFrom" runat="server" Width="60px">
                                                <asp:ListItem Value="0">Select</asp:ListItem>
                                                <asp:ListItem Value="1">00:00</asp:ListItem>
                                            </cc1:ucDropdownList>
                                        </td>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <cc1:ucLabel ID="lblTimeSuggested_2" runat="server" Text="Time Suggested"></cc1:ucLabel>
                                        </td>
                                        <td>
                                            :
                                        </td>
                                        <td class="nobold">
                                            <cc1:ucLiteral ID="ltTimeSuggested_2" runat="server"></cc1:ucLiteral>
                                        </td>
                                        <td width="8%" style="font-weight: bold;">
                                            <cc1:ucLabel ID="lblTo" runat="server" Text="To"></cc1:ucLabel>
                                        </td>
                                        <td width="1%" style="font-weight: bold;">
                                            :
                                        </td>
                                        <td width="20%" class="nobold">
                                            <cc1:ucDropdownList ID="ddlTo" runat="server" Width="60px">
                                                <asp:ListItem Value="0">Select</asp:ListItem>
                                                <asp:ListItem Value="1">00:00</asp:ListItem>
                                            </cc1:ucDropdownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3">
                                        </td>
                                        <td width="8%" style="font-weight: bold;">
                                            <cc1:ucLabel ID="lblDoorName" runat="server" Text="Door Name"></cc1:ucLabel>
                                        </td>
                                        <td width="1%" style="font-weight: bold;">
                                            :
                                        </td>
                                        <td width="20%" class="nobold">
                                            <cc1:ucDropdownList ID="ddlDoorName" runat="server" Width="80px">
                                                <asp:ListItem Value="0">--Select--</asp:ListItem>
                                                <asp:ListItem Value="1">1</asp:ListItem>
                                            </cc1:ucDropdownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="22%" colspan="6" align="right">
                                            <cc1:ucButton ID="btnConfirmAlternateSlot" runat="server" Text="Confirm Booking"
                                                CssClass="button" Width="160px" ValidationGroup="AlternateWindow" OnClick="btnConfirmAlternateSlot_Click" />
                                            <cc1:ucButton ID="btnConfirmBookingWithMail_1" runat="server"  CssClass="button" OnClick="btnConfirmBookingWithMail_Click" 
                                                Visible="false" />
                                            &nbsp;
                                            <cc1:ucButton ID="btnConfirmBookingWithoutMail_1" runat="server" Text="Confirm Booking Without Mail" CssClass="button"
                                                Visible="false" OnClick="btnConfirmBookingWithoutMail_Click"  />
                                            <%--<asp:Button ID="hdnbutton" runat="server" OnClick="hdnbutton_Click" Style="display: none;" />--%>
                                             &nbsp;
                                            <cc1:ucButton ID="btnReject_1" runat="server" Text="Reject" CssClass="button" OnClick="btnReject_Click" Visible="false"/>
                                        </td>
                                    </tr>
                                </table>
                            </cc1:ucPanel>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="ddlAlternateWindow" />
                            <asp:AsyncPostBackTrigger ControlID="btnConfirmAlternateSlot" />
                            <%--<asp:AsyncPostBackTrigger ControlID="hdnbutton" />--%>
                        </Triggers>
                    </asp:UpdatePanel>
                </cc1:ucView>
                <%--<cc1:ucView ID="vwAlternateSlotOD" runat="server">
                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                        <ContentTemplate>                           
                            <table cellpadding="5" cellspacing="2" width="100%">
                                <tr>
                                    <td width="55%" valign="top">
                                        <cc1:ucPanel ID="pnlBookingInfo" runat="server" GroupingText="Current Booking Information"
                                            CssClass="fieldset-form">
                                            <table width="100%" cellspacing="5" cellpadding="0" border="0" class="form-table"
                                                height="130px">
                                                <tr>
                                                    <td width="25%">
                                                        <cc1:ucLabel ID="lblVendorNo_2" runat="server" Text="Vendor No"></cc1:ucLabel>
                                                    </td>
                                                    <td width="2%">
                                                        :
                                                    </td>
                                                    <td width="23%" class="nobold">
                                                        <cc1:ucLiteral ID="ltVendorNo_2" runat="server"></cc1:ucLiteral>
                                                    </td>
                                                    <td width="25%">
                                                        <cc1:ucLabel ID="lblCarrier_2" runat="server" Text="Carrier"></cc1:ucLabel>
                                                    </td>
                                                    <td width="2%">
                                                        :
                                                    </td>
                                                    <td width="23%" class="nobold">
                                                        <cc1:ucLiteral ID="ltCarrier_2" runat="server"></cc1:ucLiteral>
                                                        &nbsp;&nbsp;
                                                        <cc1:ucLiteral ID="ltOtherCarrier_2" runat="server" Visible="false"></cc1:ucLiteral>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <cc1:ucLabel ID="lblVehicleType_2" runat="server" Text="Vehicle Type"></cc1:ucLabel>
                                                    </td>
                                                    <td>
                                                        :
                                                    </td>
                                                    <td class="nobold">
                                                        <cc1:ucLiteral ID="ltVehicleType_2" runat="server"></cc1:ucLiteral>
                                                        &nbsp;&nbsp;
                                                        <cc1:ucLiteral ID="ltOtherVehicleType_2" runat="server" Visible="false"></cc1:ucLiteral>
                                                    </td>
                                                    <td>
                                                        <cc1:ucLabel ID="lblSchedulingdate_2" runat="server" Text="Scheduling date"></cc1:ucLabel>
                                                    </td>
                                                    <td>
                                                        :
                                                    </td>
                                                    <td class="nobold">
                                                        <cc1:ucLiteral ID="ltSchedulingDate_2" runat="server"></cc1:ucLiteral>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    
                                                    <td>
                                                        <cc1:ucLabel ID="lblEstEnd_2" runat="server" Text="Est End"></cc1:ucLabel>
                                                    </td>
                                                    <td>
                                                        :
                                                    </td>
                                                    <td class="nobold">
                                                        <cc1:ucLiteral ID="ltlEstEnd_2" runat="server"></cc1:ucLiteral>
                                                    </td>

                                                    <td colspan="3">&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <cc1:ucLabel ID="lblNumberofLifts" runat="server" Text="Number of Lifts"></cc1:ucLabel>
                                                    </td>
                                                    <td>
                                                        :
                                                    </td>
                                                    <td class="nobold">
                                                        <cc1:ucLiteral ID="ltNumberofLifts" runat="server"></cc1:ucLiteral>
                                                    </td>
                                                    <td>
                                                        <cc1:ucLabel ID="lblNumberofPallet" runat="server" Text="Number of Pallet"></cc1:ucLabel>
                                                    </td>
                                                    <td>
                                                        :
                                                    </td>
                                                    <td class="nobold">
                                                        <cc1:ucLiteral ID="ltNumberofPallets" runat="server"></cc1:ucLiteral>
                                                    </td>
                                                    <td colspan="3">
                                                        &nbsp;
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <cc1:ucLabel ID="lblNumberofLines" runat="server" Text="Number of Lines"></cc1:ucLabel>
                                                    </td>
                                                    <td>
                                                        :
                                                    </td>
                                                    <td class="nobold">
                                                        <cc1:ucLabel ID="ltNumberofLines" runat="server"></cc1:ucLabel>
                                                    </td>
                                                    <td>
                                                        <cc1:ucLabel ID="lblNumberofCartons" runat="server" Text="Number of Cartons"></cc1:ucLabel>
                                                    </td>
                                                    <td>
                                                        :
                                                    </td>
                                                    <td class="nobold">
                                                        <cc1:ucLabel ID="ltNumberofCartons" runat="server"></cc1:ucLabel>
                                                    </td>
                                                    <td colspan="3">
                                                        &nbsp;
                                                    </td>
                                                </tr>
                                            </table>
                                        </cc1:ucPanel>
                                    </td>
                                    <td width="45%" valign="top">
                                        <cc1:ucPanel ID="pnlRunningTotal" runat="server" GroupingText="Current Running Total"
                                            CssClass="fieldset-form">
                                            <table width="100%" cellspacing="5" cellpadding="0" border="0" class="form-table"
                                                height="130Px">
                                                <tr>
                                                    <th width="40%">
                                                    </th>
                                                    <th width="30%">
                                                        <cc1:ucLabel ID="lblDailyMaximums" runat="server" Text="Daily Maximums"></cc1:ucLabel>
                                                    </th>
                                                    <th width="30%">
                                                        <cc1:ucLabel ID="lblRemainingCapacity" runat="server" Text="Remaining Capacity"></cc1:ucLabel>
                                                    </th>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <cc1:ucLabel ID="lblMaximumLifts" runat="server" Text="Maximum Lifts"></cc1:ucLabel>
                                                    </td>
                                                    <td align="center">
                                                        <cc1:ucLiteral ID="ltMaximumLifts" runat="server"></cc1:ucLiteral>
                                                    </td>
                                                    <td align="center">
                                                        <cc1:ucLiteral ID="ltRemainingLifts" runat="server"></cc1:ucLiteral>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <cc1:ucLabel ID="lblMaximumPallets" runat="server" Text="Maximum Pallets"></cc1:ucLabel>
                                                    </td>
                                                    <td align="center">
                                                        <cc1:ucLiteral ID="ltMaximumPallets" runat="server"></cc1:ucLiteral>
                                                    </td>
                                                    <td align="center">
                                                        <cc1:ucLiteral ID="ltRemainingPallets" runat="server"></cc1:ucLiteral>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <cc1:ucLabel ID="lblMaximumLines" runat="server" Text="Maximum Lines"></cc1:ucLabel>
                                                    </td>
                                                    <td align="center">
                                                        <cc1:ucLiteral ID="ltMaximumLines" runat="server"></cc1:ucLiteral>
                                                    </td>
                                                    <td align="center">
                                                        <cc1:ucLiteral ID="ltRemainingLines" runat="server"></cc1:ucLiteral>
                                                    </td>
                                                </tr>
                                            </table>
                                        </cc1:ucPanel>
                                    </td>
                                </tr>
                            </table>
                            <table width="100%" cellspacing="2" cellpadding="0" border="0">
                                <tr>
                                    <td width="40%" valign="top">
                                        <cc1:ucPanel ID="pnlNonTimeBooking" runat="server" GroupingText="Non Time Booking" CssClass="fieldset-form">
                                            <cc1:ucGridView ID="gvNonTimeBooking" runat="server" AutoGenerateColumns="false"
                                                CssClass="grid" CellPadding="0" Width="100%">
                                                <AlternatingRowStyle BackColor="White" ForeColor="#284775"></AlternatingRowStyle>
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Vendor/Carrier">
                                                        <ItemStyle HorizontalAlign="Left" Width="40%" />
                                                        <HeaderStyle HorizontalAlign="Left" />
                                                        <ItemTemplate>
                                                            <asp:Label ID="vendorname" runat="server" Text='<%#Eval("Vendor.VendorName") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField HeaderText="Lifts" DataField="NumberOfLift" HeaderStyle-Width="20%"
                                                        HeaderStyle-HorizontalAlign="Left" />
                                                    <asp:BoundField HeaderText="Pallets" DataField="NumberOfPallet" HeaderStyle-Width="20%"
                                                        HeaderStyle-HorizontalAlign="Left" />
                                                    <asp:BoundField HeaderText="Cartons" DataField="NumberOfCartons" HeaderStyle-Width="20%"
                                                        HeaderStyle-HorizontalAlign="Left" />
                                                    <asp:BoundField HeaderText="Lines" DataField="NumberOfLines" HeaderStyle-Width="20%"
                                                        HeaderStyle-HorizontalAlign="Left" />
                                                </Columns>
                                            </cc1:ucGridView>
                                        </cc1:ucPanel>
                                    </td>
                                    <td width="60%" valign="top">
                                        <cc1:ucPanel ID="pnlLegend" runat="server" GroupingText="Legend" CssClass="fieldset-form">
                                            <table width="100%" cellspacing="2" cellpadding="0" border="0" class="form-table">
                                                <tr height="20Px">
                                                    <td style="width: 21%;text-align:right;"  >
                                                        <cc1:ucLabel ID="lblAvailableSlot" runat="server" Text="Available Slot"></cc1:ucLabel>
                                                    </td>
                                                    <td style="background-color: White; border: 1px solid black; width: 13%;">
                                                        &nbsp;
                                                    </td>
                                                    <td style="width: 18%;text-align:right;">
                                                        <cc1:ucLabel ID="lblBookedSlot" runat="server" Text="Booked Slot"></cc1:ucLabel>
                                                    </td>
                                                    <td style="background-color: Purple; border: 2px dashed black; width: 13%;">
                                                        &nbsp;
                                                    </td>
                                                    <td style="width: 22%;text-align:right;">
                                                        <cc1:ucLabel ID="lblInsufficentSlotTime" runat="server" Text="Insufficent Slot Time"></cc1:ucLabel>
                                                    </td>
                                                    <td style="background-color: Gray; border: 1px solid black; width: 13%;">
                                                        &nbsp;
                                                    </td>
                                                </tr>
                                                <tr height="20Px">
                                                    <td style="text-align:right;">
                                                        <cc1:ucLabel ID="lblRecommendedSlot" runat="server" Text="Recommended Slot"></cc1:ucLabel>
                                                    </td>
                                                    <td style="background-color: GreenYellow; border: 1px solid black;">
                                                        &nbsp;
                                                    </td>
                                                    <td style="text-align:right;">
                                                        <cc1:ucLabel ID="lblFixedSlot" runat="server" Text="Fixed Slot"></cc1:ucLabel>
                                                    </td>
                                                    <td style="background-color: LightYellow ; border: 2px solid red;">
                                                        &nbsp;
                                                    </td>
                                                    <td style="text-align:right;">
                                                        <cc1:ucLabel ID="lblDoorNotAvailable" runat="server" Text="Door Not Available"></cc1:ucLabel>
                                                    </td>
                                                    <td style="background-color: Black; border: 1px solid black;">
                                                        &nbsp;
                                                    </td>
                                                </tr>
                                                <tr height="20Px">
                                                    <td style="text-align:right;">
                                                        <cc1:ucLabel ID="lblSlotOverspill" runat="server" Text="Slot Overspill"></cc1:ucLabel>
                                                    </td>
                                                    <td style="background-color: Orange; border: 1px solid black;">
                                                        &nbsp;
                                                    </td>
                                                    <td colspan="4">
                                                        &nbsp;
                                                    </td>
                                                </tr>
                                            </table>
                                        </cc1:ucPanel>
                                    </td>
                                </tr>
                            </table>
                            <cc1:ucPanel ID="pnlSystemRecommendedSlotDetails" runat="server" GroupingText="System Recommended Slot Details"
                                CssClass="fieldset-form">
                                <table width="100%" cellspacing="5" cellpadding="0" border="0" class="form-table">
                                    <tr>
                                        <td width="18%" style="font-weight: bold;">
                                            <cc1:ucLabel ID="lblSelectedTimeSlot" runat="server" Text="Selected Time Slot"></cc1:ucLabel>
                                        </td>
                                        <td width="1%" style="font-weight: bold;">
                                            :
                                        </td>
                                        <td width="20%" class="nobold">
                                            <cc1:ucLabel ID="ltTimeSlot" runat="server"></cc1:ucLabel>
                                        </td>
                                        <td width="18%" style="font-weight: bold;">
                                            <cc1:ucLabel ID="lblSelectedDoorName" runat="server"></cc1:ucLabel>
                                        </td>
                                        <td width="1%" style="font-weight: bold;">
                                            :
                                        </td>
                                        <td width="20%" class="nobold">
                                            <cc1:ucLabel ID="ltDoorNo" runat="server"></cc1:ucLabel>
                                        </td>
                                        <td width="22%">
                                            <cc1:ucButton ID="btnConfirmAlternateSlot" runat="server" Text="Confirm Booking"
                                                CssClass="button" Width="160px" OnClick="btnConfirmAlternateSlot_Click" />
                                            <asp:Button ID="hdnbutton" runat="server" OnClick="hdnbutton_Click" Style="display: none;" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                                        <cc1:ucLabel ID="lblTimeSuggested_2" runat="server" Text="Time Suggested"></cc1:ucLabel>
                                                    </td>
                                                    <td>
                                                        :
                                                    </td>
                                                    <td class="nobold">
                                                        <cc1:ucLiteral ID="ltTimeSuggested_2" runat="server"></cc1:ucLiteral>
                                                    </td>
                                                    <td colspan="4">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td colspan="7" align="left">
                                            <cc1:ucLabel ID="lblChangeBookingTime" runat="server" Text="To change the time of this booking, click on the slot required in the grid below."></cc1:ucLabel>
                                        </td>
                                    </tr>
                                </table>
                            </cc1:ucPanel>
                         
                  <br />
                           <asp:Panel ID="tableDiv_General" runat="server" CssClass="tableDiv" onscroll="getScroll1(this);">

                            </asp:Panel>
                        
                        
                
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="btnConfirmAlternateSlot" />
                            <asp:AsyncPostBackTrigger ControlID="hdnbutton" />
                        </Triggers>
                    </asp:UpdatePanel>
                </cc1:ucView>--%>
                <%--<cc1:ucView ID="vwAlternateSlotVendor" runat="server">
                    <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                        <ContentTemplate>
                           
                            <table width="100%" cellspacing="5" cellpadding="0" border="0">
                                <tr>
                                    <td width="70%" valign="top">
                                        <cc1:ucPanel ID="pnlCurrentBookingInformation" runat="server" GroupingText="Current Booking Information"
                                            CssClass="fieldset-form">
                                            <table width="100%" cellspacing="5" cellpadding="0" border="0" class="form-table">
                                                <tr>
                                                    <td width="20%">
                                                        <cc1:ucLabel ID="lblSite" runat="server" Text="Site"></cc1:ucLabel>
                                                    </td>
                                                    <td width="2%">
                                                        :
                                                    </td>
                                                    <td class="nobold" width="28%">
                                                        <cc1:ucLiteral ID="ltSite" runat="server"></cc1:ucLiteral>
                                                    </td>
                                                    <td width="20%">
                                                        <cc1:ucLabel ID="lblVendor_3" runat="server" Text="Vendor"></cc1:ucLabel>
                                                    </td>
                                                    <td width="2%">
                                                        :
                                                    </td>
                                                    <td width="28%" class="nobold">
                                                        <cc1:ucLiteral ID="ltVendor_3" runat="server"></cc1:ucLiteral>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <cc1:ucLabel ID="lblCarrier_3" runat="server" Text="Carrier"></cc1:ucLabel>
                                                    </td>
                                                    <td>
                                                        :
                                                    </td>
                                                    <td class="nobold">
                                                        <cc1:ucLiteral ID="ltCarrier_3" runat="server"></cc1:ucLiteral>
                                                        &nbsp;
                                                        <cc1:ucLiteral ID="ltOtherCarrier_3" runat="server"></cc1:ucLiteral>
                                                    </td>
                                                    <td>
                                                        <cc1:ucLabel ID="lblVehicle_3" runat="server" Text="Vehicle"></cc1:ucLabel>
                                                    </td>
                                                    <td>
                                                        :
                                                    </td>
                                                    <td class="nobold">
                                                        <cc1:ucLiteral ID="ltVehicleType_3" runat="server"></cc1:ucLiteral>
                                                        &nbsp;
                                                        <cc1:ucLiteral ID="ltOtherVehicleType_3" runat="server"></cc1:ucLiteral>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <cc1:ucLabel ID="lblNumberofLifts_1" runat="server" Text="Number of Lifts"></cc1:ucLabel>
                                                    </td>
                                                    <td>
                                                        :
                                                    </td>
                                                    <td class="nobold">
                                                        <cc1:ucLiteral ID="ltExpectedLifts" runat="server"></cc1:ucLiteral>
                                                    </td>
                                                    <td>
                                                        <cc1:ucLabel ID="lblNumberofPallet_1" runat="server" Text="Number of Pallet"></cc1:ucLabel>
                                                    </td>
                                                    <td>
                                                        :
                                                    </td>
                                                    <td class="nobold">
                                                        <cc1:ucLiteral ID="ltExpectedPallets" runat="server"></cc1:ucLiteral>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <cc1:ucLabel ID="lblNumberofLines_1" runat="server" Text="Number of Lines"></cc1:ucLabel>
                                                    </td>
                                                    <td>
                                                        :
                                                    </td>
                                                    <td class="nobold">
                                                        <cc1:ucLiteral ID="ltExpectedLines" runat="server"></cc1:ucLiteral>
                                                    </td>
                                                    <td>
                                                        <cc1:ucLabel ID="lblNumberofCartons_1" runat="server" Text="Number of Cartons"></cc1:ucLabel>
                                                    </td>
                                                    <td>
                                                        :
                                                    </td>
                                                    <td class="nobold">
                                                        <cc1:ucLiteral ID="ltExpectedCartons" runat="server"></cc1:ucLiteral>
                                                    </td>
                                                </tr>
                                                <tr>
                                                   
                                                    <td>
                                                        <cc1:ucLabel ID="lblSchedulingDate_3" runat="server" Text="Scheduling Date"></cc1:ucLabel>
                                                    </td>
                                                    <td>
                                                        :
                                                    </td>
                                                    <td class="nobold">
                                                        <cc1:ucLiteral ID="ltSchedulingDate_3" runat="server"></cc1:ucLiteral>
                                                    </td>
                                                     <td colspan="3">
                                                        &nbsp;
                                                    </td>
                                                   
                                                </tr>
                                            </table>
                                        </cc1:ucPanel>
                                    </td>
                                    <td width="30%" valign="top">
                                        <cc1:ucPanel ID="pnlLegend_1" runat="server" GroupingText="Legend" CssClass="fieldset-form">
                                            <table width="100%" cellspacing="3" cellpadding="0" border="0" class="form-table">
                                                <tr>
                                                    <td style="width: 50%;text-align:right;"">
                                                        <cc1:ucLabel ID="lblAvailableSlot_1" runat="server" Text="Available Slot"></cc1:ucLabel>
                                                    </td>
                                                    <td style="background-color: White; border: 1px solid black; width: 50%;">
                                                        &nbsp;
                                                    </td>
                                                                                                   
                                                </tr>
                                                <tr>
                                                     <td style="text-align:right;">
                                                        <cc1:ucLabel ID="lblBookedSlot_1" runat="server" Text="Booked Slot"></cc1:ucLabel>
                                                    </td>
                                                    <td style="background-color: Purple; border: 2px dashed black; ">
                                                        &nbsp;
                                                    </td>   
                                                </tr>
                                                <tr>
                                                    <td style="text-align:right;">
                                                        <cc1:ucLabel ID="lblRecommendedSlot_1" runat="server" Text="Recommended Slot"></cc1:ucLabel>
                                                    </td>
                                                    <td style="background-color: GreenYellow; border: 1px solid black;">
                                                        &nbsp;
                                                    </td>
                                                                                                   
                                                    
                                                </tr>
                                                <tr>
                                                    <td style="text-align:right;">
                                                        <cc1:ucLabel ID="lblFixedSlot_1" runat="server" Text="Fixed Slot"></cc1:ucLabel>
                                                    </td>
                                                    <td style="background-color: LightYellow; border: 2px solid red;">
                                                        &nbsp;
                                                    </td>   
                                                </tr>
                                                <tr>
                                                     <td style="text-align:right;">
                                                        <cc1:ucLabel ID="lblDoorNotAvailable_1" runat="server" Text="Door Not Available"></cc1:ucLabel>
                                                    </td>
                                                    <td style="background-color: Black; border: 1px solid black;">
                                                        &nbsp;
                                                    </td>                                                    
                                                </tr>
                                               
                                            </table>
                                        </cc1:ucPanel>
                                    </td>
                                </tr>
                            </table>
                            <cc1:ucPanel ID="pnlSystemRecommendedSlotDetails_1" runat="server" GroupingText="System Recommended Slot Details"
                                CssClass="fieldset-form">
                                <table width="100%" cellspacing="5" cellpadding="0" border="0" class="form-table">
                                    <tr>
                                        <td width="18%" style="font-weight: bold;">
                                            <cc1:ucLabel ID="lblSelectedTimeSlot_1" runat="server" Text="Selected Time Slot"></cc1:ucLabel>
                                        </td>
                                        <td width="1%" style="font-weight: bold;">
                                            :
                                        </td>
                                        <td width="20%" class="nobold">
                                            <cc1:ucLabel ID="ltTimeSlot_1" runat="server"></cc1:ucLabel>
                                        </td>
                                        <td width="18%" style="font-weight: bold;">
                                            <cc1:ucLabel ID="lblSelectedDoorName_1" runat="server" Text="Selected Door No"></cc1:ucLabel>
                                        </td>
                                        <td width="1%" style="font-weight: bold;">
                                            :
                                        </td>
                                        <td width="20%" class="nobold">
                                            <cc1:ucLabel ID="ltDoorNo_1" runat="server"></cc1:ucLabel>
                                        </td>
                                        <td width="22%">
                                            <cc1:ucButton ID="btnConfirmAlternateSlotVendor" runat="server" Text="Confirm Booking"
                                                CssClass="button" Width="160px" OnClick="btnConfirmAlternateSlotVendor_Click" />
                                            <asp:Button ID="hdnbuttonVendor" runat="server" OnClick="hdnbutton_Click" Style="display: none;" />
                                        </td>
                                    </tr>
                                    <tr style="font-weight: bold;">
                                        <td>
                                        <cc1:ucLabel ID="lblTimeSuggested_3" runat="server" Text="Suggested Time"></cc1:ucLabel>
                                        </td>
                                         <td style="font-weight: bold;">
                                            :
                                        </td>
                                        <td class="nobold">
                                             <cc1:ucLiteral ID="ltTimeSuggested_3" runat="server"></cc1:ucLiteral>
                                        </td>
                                       <td colspan="4">
                                       &nbsp;
                                       </td>
                                    </tr>
                                     <tr>
                                        <td colspan="7" align="left">
                                            <cc1:ucLabel ID="lblChangeBookingTime_1" runat="server" Text="To change the time of this booking, click on the slot required in the grid below."></cc1:ucLabel>
                                        </td>
                                    </tr>
                                </table>
                            </cc1:ucPanel>
                            <br />

                            <asp:Panel ID="tableDiv_GeneralVendor" runat="server" CssClass="tableDiv" onscroll="getScroll2(this);">

                            </asp:Panel>
                        

                         
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="btnConfirmAlternateSlotVendor" />
                            <asp:AsyncPostBackTrigger ControlID="hdnbuttonVendor" />
                        </Triggers>
                    </asp:UpdatePanel>
                </cc1:ucView>--%>
            </cc1:ucMultiView>
        </div>
    </div>
   
    
   
    <%---WARNING popup start--%>
    <asp:UpdatePanel ID="updpnlWarning" runat="server">
        <ContentTemplate>
          <asp:HiddenField ID="hdnSuggestedSiteDoorNumberID" runat="server" />
            <asp:Button ID="btnConfirmMsg" runat="Server" Style="display: none" />
            <ajaxToolkit:ModalPopupExtender ID="mdlConfirmMsg" runat="server" TargetControlID="btnConfirmMsg"
                PopupControlID="pnlConfirmMsg" BackgroundCssClass="modalBackground" BehaviorID="ConfirmMsg"
                DropShadow="false" />
            <asp:Panel ID="pnlConfirmMsg" runat="server" Style="display: none;">
                <div class="popup-maincontainer" style="overflow-y: hidden; overflow-x: hidden;">
                     <h3><cc1:ucLabel ID="lblWarningInfo" runat="server"></cc1:ucLabel></h3>
                    <div class="popup-innercontainer top-setting-Popup">
                    <table cellspacing="5" cellpadding="0" border="0" align="center" class="popup-maincontainer" >
                        <tr>
                            <td>
                             <div  class="popup-innercontainer top-setting-Popup">
                              <div class="row1"><cc1:ucLiteral ID="ltConfirmMsg" runat="server" Text=""></cc1:ucLiteral></div>
                              </div>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                             <div class="row">
                            <cc1:ucButton ID="btnErrContinue" runat="server" Text="CONTINUE" CssClass="button"
                                    OnCommand="btnContinue_Click"  />
                                &nbsp;
                                <cc1:ucButton ID="btnErrBack" runat="server" Text="BACK" CssClass="button" OnCommand="btnBack_Click" />
                           </div>
                            </td>
                        </tr>
                    </table>
                    </div>
                </div>
            </asp:Panel>
        </ContentTemplate>
        <Triggers>          
            <asp:PostBackTrigger ControlID="btnErrContinue" />
            <asp:AsyncPostBackTrigger ControlID="btnErrBack" />
        </Triggers>
    </asp:UpdatePanel>
    <%---WARNING popup End--%>
    <%---ERROR popup start--%>
    <asp:UpdatePanel ID="updpnlError" runat="server">
        <ContentTemplate>
            <asp:Button ID="btnErrorMsg" runat="Server" Style="display: none" />
            <ajaxToolkit:ModalPopupExtender ID="mdlErrorMsg" runat="server" TargetControlID="btnErrorMsg"
                PopupControlID="pnlErrorMsg" BackgroundCssClass="modalBackground" BehaviorID="ErrorMsg"
                DropShadow="false" />
            <asp:Panel ID="pnlErrorMsg" runat="server" Style="display: none;">
                <div class="popup-maincontainer" style="overflow-y: hidden; overflow-x: hidden;">
                    <h3><cc1:ucLabel ID="lblERROR" runat="server"></cc1:ucLabel></h3>
                    <div class="popup-innercontainer top-setting-Popup">
                        <div class="row1"><cc1:ucLiteral ID="ltErrorMsg" runat="server" Text=""></cc1:ucLiteral></div>
   
                    </div>
                    <div class="row"><cc1:ucButton ID="btnErrorMsgOK" runat="server" Text="OK" CssClass="button" OnCommand="btnErrorMsgOK_Click" /></div>
                </div>
            </asp:Panel>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnErrorMsgOK" />
        </Triggers>
    </asp:UpdatePanel>
    <%---ERROR popup End--%>
    <%---OutstandingPO popup start--%>
    <asp:UpdatePanel ID="updOutstandingPO" runat="server">
        <ContentTemplate>
            <asp:Button ID="btnOutstandingPO" runat="Server" Style="display: none" />
            <ajaxToolkit:ModalPopupExtender ID="mdlOutstandingPO" runat="server" TargetControlID="btnOutstandingPO"
                PopupControlID="pnlOutstandingPO" BackgroundCssClass="modalBackground" BehaviorID="OutstandingPO"
                DropShadow="false" />
            <asp:Panel ID="pnlOutstandingPO" runat="server" Style="display: none;">
                <div style="overflow-y: scroll; overflow-x: hidden; background-color: #fff; padding: 5px;
                    border: 2px solid #ccc; text-align: center; max-height: 500px;">
                    <table cellspacing="5" cellpadding="0" border="0" align="center" class="top-settingsNoBorder-popup">
                        <%-- <tr>
                            <td style="font-weight: bold; color: Red; font-size: 14px; text-align: center;">
                                <cc1:ucLabel ID="lblPOWarning" runat="server" Text="WARNING"></cc1:ucLabel>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold; color: Red; font-size: 14px; text-align: center;">
                                <cc1:ucLabel ID="lblPOMsg1" runat="server" Text="The following additional Purchase Orders are expected by the date of this booking and have yet to be booked in"></cc1:ucLabel>
                            </td>
                        </tr>--%>
                        <tr>
                            <td align="center">
                                <cc1:ucGridView ID="gvOutstandingPO" runat="server" AutoGenerateColumns="false" CssClass="grid"
                                    CellPadding="0" Width="70%">
                                    <AlternatingRowStyle BackColor="White" ForeColor="#284775"></AlternatingRowStyle>
                                    <Columns>
                                        <asp:BoundField HeaderText="Purchase Order" DataField="PurchaseNumber" SortExpression="PurchaseNumber">
                                            <HeaderStyle Width="30%" />
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:BoundField>
                                        <asp:BoundField HeaderText="Line still Due" DataField="OutstandingLines" SortExpression="OutstandingLines">
                                            <HeaderStyle Width="40%" />
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:BoundField>
                                        <asp:BoundField HeaderText="Due date" DataField="Original_due_date" SortExpression="Original_due_date">
                                            <HeaderStyle Width="30%" />
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:BoundField>
                                    </Columns>
                                </cc1:ucGridView>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold; color: Red; font-size: 14px; text-align: center;">
                                <cc1:ucLabel ID="lblPOMsg2" runat="server" Text="If you are not planning to deliver these by this date then you must contact the Stock Planner urgently"></cc1:ucLabel>
                            </td>
                        </tr>
                    </table>
                </div>
                <table cellspacing="0" cellpadding="0" border="0" align="left" style="width: 810px! important;"
                    class="top-settingsNoBorder">
                    <tr>
                        <td align="center">
                            <cc1:ucButton ID="btnOutstandingPOContinue" runat="server" Text="CONTINUE" CssClass="button"
                                OnClick="btnOutstandingPOContinue_Click" />
                            &nbsp;
                            <cc1:ucButton ID="btnOutstandingPOBack" runat="server" Text="BACK" CssClass="button"
                                OnClick="btnOutstandingPOBack_Click" />
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnOutstandingPOContinue" />
            <asp:AsyncPostBackTrigger ControlID="btnOutstandingPOBack" />
        </Triggers>
    </asp:UpdatePanel>
    <%---OutstandingPO popup End--%>
    <%---PO WARNING popup start--%>
    <asp:UpdatePanel ID="updpnlPOWarning" runat="server">
        <ContentTemplate>
            <asp:Button ID="btnPOConfirmMsg" runat="Server" Style="display: none" />
            <ajaxToolkit:ModalPopupExtender ID="mdlPOConfirmMsg" runat="server" TargetControlID="btnPOConfirmMsg"
                PopupControlID="pnlPOConfirmMsg" BackgroundCssClass="modalBackground" BehaviorID="POConfirmMsg"
                DropShadow="false" />
            <asp:Panel ID="pnlPOConfirmMsg" runat="server" Style="display: none;">
                <div class="popup-maincontainer" style="overflow-y: hidden; overflow-x: hidden;">
                 <h3><cc1:ucLabel ID="lblWarningInfo_2" runat="server"></cc1:ucLabel></h3>
                    <table cellspacing="5" cellpadding="0" border="0" align="center" class="popup-maincontainer">
                        <%--<tr>
                            <td style="font-weight: bold; color: Red; font-size: 14px; text-align: center;">
                                <cc1:ucLabel ID="UcLabel1" runat="server" Text="WARNING"></cc1:ucLabel>
                            </td>
                        </tr>--%>
                        <tr>
                            <td> <div  class="popup-innercontainer top-setting-Popup">
                                <div class="row1"><cc1:ucLiteral ID="ltPOConfirmMsg" runat="server" Text=""></cc1:ucLiteral></div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                             <div class="row">
                                <cc1:ucButton ID="btnPOErrContinue" runat="server" Text="CONTINUE" CssClass="button"
                                    OnCommand="btnPOContinue_Click" />
                                &nbsp;
                                <cc1:ucButton ID="btnPOErrBack" runat="server" Text="BACK" CssClass="button" OnCommand="btnPOBack_Click" />
                          </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnPOErrContinue" />
            <asp:AsyncPostBackTrigger ControlID="btnPOErrBack" />
        </Triggers>
    </asp:UpdatePanel>
    <%---WARNING popup End--%>
    <%---Sprint 1 - Point 7 - ShowPO popup start--%>
    <asp:UpdatePanel ID="updShowPO" runat="server">
        <ContentTemplate>
            <asp:Button ID="btnShowPO" runat="Server" Style="display: none" />
            <ajaxToolkit:ModalPopupExtender ID="mdlShowPO" runat="server" TargetControlID="btnShowPO"
                PopupControlID="pnlShowPO" BackgroundCssClass="modalBackground" BehaviorID="ShowPO"
                DropShadow="false" />
            <asp:Panel ID="pnlShowPO" runat="server" Style="display: none;" Width="80%">
                <div style="overflow-y: scroll; overflow-x: hidden; background-color: #fff; padding: 5px;
                    border: 2px solid #ccc; text-align: center; max-height: 500px;">
                    <table cellspacing="5" cellpadding="0" border="0" align="center" class="top-settingsNoBorder-popup"
                        width="100%">
                        <tr>
                            <td align="center">
                                <cc1:ucGridView ID="grdShowPO" runat="server" AutoGenerateColumns="false" CssClass="grid"
                                    CellPadding="0" Width="100%" AllowSorting="true" OnRowDataBound="grdShowPO_RowDataBound"
                                    OnSorting="grdShowPO_Sorting">
                                    <AlternatingRowStyle BackColor="White" ForeColor="#284775"></AlternatingRowStyle>
                                    <Columns>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:CheckBox runat="server" ID="chkPO" Visible='<%# Convert.ToBoolean(Eval("Allowed")) %>' />
                                                <asp:HiddenField ID="hdnPONumber" runat="server" Value='<%# Eval("PurchaseNumber") %>' />
                                                 <asp:HiddenField ID="hdnStockouts" runat="server" Value='<%# Eval("StockoutsCount") %>' />
                                                <asp:HiddenField ID="hdnBackorders" runat="server" Value='<%# Eval("BackOrdersCount") %>' />
                                                <asp:HiddenField ID="hdnPurchaseOrderID" runat="server" Value='<%# Eval("PurchaseOrderID") %>' />
                                                <asp:HiddenField ID="hdnExpected_date" runat="server" Value='<%# Eval("Expected_date") %>' />
                                                <asp:HiddenField ID="hdnOriginal_due_date" runat="server" Value='<%# Eval("Original_due_date") %>' />
                                                <asp:HiddenField ID="hdnSitename" runat="server" Value='<%# Eval("Sitename") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Status" SortExpression="Status">
                                            <ItemTemplate>
                                                <asp:Literal ID="ltbookingStatus" runat="server" Text='<%# Eval("Status") %>'></asp:Literal>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <%-- <asp:BoundField HeaderText="Status" DataField="Status" SortExpression="Status">
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:BoundField>--%>
                                        <asp:BoundField HeaderText="Purchase Order" DataField="PurchaseNumber" SortExpression="PurchaseNumber">
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:BoundField>
                                        <asp:BoundField HeaderText="Vendor Name" DataField="VendorName">
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:BoundField>
                                        <asp:BoundField HeaderText="LinesStillDue" DataField="OutstandingLines">
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:BoundField>
                                        <asp:BoundField HeaderText="Back Orders" DataField="BackOrders">
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:BoundField>
                                        <asp:TemplateField HeaderText="Due date" SortExpression="Expected_date">
                                            <ItemStyle HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <cc1:ucLiteral ID="ltExpectedDate" runat="server" Text='<%# Eval("Expected_date","{0:dd-MM-yyyy}") %>'></cc1:ucLiteral>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField HeaderText="Delivery date" DataField="BookingDeliveryDate">
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:BoundField>
                                    </Columns>
                                </cc1:ucGridView>
                            </td>
                        </tr>
                    </table>
                </div>
                <table width="100%" cellspacing="0" cellpadding="0" border="0" align="center" class="top-settingsNoBorder">
                    <tr>
                        <td align="center">
                            <cc1:ucButton ID="btnAddToBooking" runat="server" Text="Add to Booking" CssClass="button"
                                OnClick="btnAddPOToBooking_Click" />
                            &nbsp;
                            <cc1:ucButton ID="btnCancel_1" runat="server" Text="Cancel" CssClass="button" OnClick="btnCancel_1_Click" />
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    <%---Sprint 1 - Point 7 - ShowPO popup End--%>

    <%---Stage 9 Point 2  Provisional WARNING popup start--%>
    <asp:UpdatePanel ID="UpdatePanel5" runat="server">
        <ContentTemplate>
            <asp:Button ID="btnProConfirmMsg" runat="Server" Style="display: none" />
            <ajaxToolkit:ModalPopupExtender ID="mdlProConfirmMsg" runat="server" TargetControlID="btnProConfirmMsg"
                PopupControlID="pnlProConfirmMsg" BackgroundCssClass="modalBackground" BehaviorID="ProConfirmMsg"
                DropShadow="false" />
            <asp:Panel ID="pnlProConfirmMsg" runat="server" Style="display: none;">
               <div class="popup-maincontainer" style="overflow-y: hidden; overflow-x: hidden;">
                     <h3><cc1:ucLabel ID="lblWarningInfo_1" runat="server"></cc1:ucLabel></h3>
                    <table cellspacing="5" cellpadding="0" border="0" align="center" class="popup-maincontainer">                       
                        <tr>
                            <td>
                            <div  class="popup-innercontainer top-setting-Popup">
                               <div class="row1">
                               <cc1:ucLabel ID="lblProConfirmMsg1" runat="server" Text=""></cc1:ucLabel>
                               <cc1:ucLabel ID="lblProConfirmMsg2" runat="server" Text=""></cc1:ucLabel>                             
                               <cc1:ucLabel ID="lblProConfirmMsg4" runat="server" Text=""></cc1:ucLabel>
                                </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                             <div class="row">
                                <cc1:ucButton ID="btnProErrContinue" runat="server" Text="Send For Review" CssClass="button"
                                    OnCommand="btnProContinue_Click"/>
                                &nbsp;
                                <cc1:ucButton ID="btnProErrBack" runat="server" Text="Cancel Booking" CssClass="button" OnCommand="btnProBack_Click" />
                           </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnProErrContinue" />
            <asp:AsyncPostBackTrigger ControlID="btnProErrBack" />
        </Triggers>
    </asp:UpdatePanel>
    <%---WARNING popup End--%>

    <%---WARNING popup Reject Provisional Booking start--%>
    <asp:UpdatePanel ID="UpdatePanel6" runat="server">
        <ContentTemplate>
            <asp:Button ID="btnRejectProvisional" runat="Server" Style="display: none" />
            <ajaxToolkit:ModalPopupExtender ID="mdlRejectProvisional" runat="server" TargetControlID="btnRejectProvisional"
                PopupControlID="pnlRejectProvisional" BackgroundCssClass="modalBackground" BehaviorID="RejectProvisional"
                DropShadow="false" />
            <asp:Panel ID="pnlRejectProvisional" runat="server" Style="display: none;">
                 <div class="popup-maincontainer" style="overflow-y: hidden; overflow-x: hidden;">
                    <h3><cc1:ucLabel ID="lblWarningInfo_3" runat="server"></cc1:ucLabel></h3>
                    <table cellspacing="5" cellpadding="0" border="0" align="center" class="popup-maincontainer">
                        <tr>
                            <td style="font-weight: bold;text-align:center;">
                             <div  class="popup-innercontainer top-setting-Popup">
                               <div class="row1"><cc1:ucLabel ID="lblRejectProvisionalMsg" runat="server" ></cc1:ucLabel></div>
                                </div>
                            </td>
                        </tr>
                         <tr>
                            <td style="font-weight: bold;">
                                <cc1:ucLabel ID="lblAddComment" runat="server" ></cc1:ucLabel><br />
                                <asp:TextBox TextMode="MultiLine" ID="txtRejectProCmments" style="height:40px;width:480px;" runat="server"></asp:TextBox>
                                
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                            <div class="row">
                                <cc1:ucButton ID="btnContinue_2" runat="server" Text="CONTINUE" CssClass="button"
                                    OnCommand="btnContinue_2_Click" />
                                &nbsp;
                                <cc1:ucButton ID="btnBack_2" runat="server" Text="BACK" CssClass="button" OnCommand="btnBack_2_Click" />
                            </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>            　　　
　　　        <ajaxToolkit:ModalPopupExtender ID="mdlRejectProvisionalChanges" runat="server" TargetControlID="btnRejectProvisional"
                PopupControlID="pnlRejectProvisionalChanges" BackgroundCssClass="modalBackground" BehaviorID="RejectProvisional1"
                DropShadow="false" />
            <asp:Panel ID="pnlRejectProvisionalChanges" runat="server" Style="display: none;">
                <div class="popup-maincontainer" style="overflow-y: hidden; overflow-x: hidden;">
                    <h3><cc1:ucLabel ID="btnWarningInfo" runat="server" Text="INFORMATION"></cc1:ucLabel></h3>
                    <table cellspacing="5" cellpadding="0" border="0" align="center" class="popup-maincontainer">
                        <tr>
                            <td>
                             <div  class="popup-innercontainer top-setting-Popup">
                               <div class="row1"> <cc1:ucLabel ID="lblRejectProvisionalChangesMsg" runat="server" ></cc1:ucLabel></div>
                               </div>
                            </td>
                        </tr>                    
                        <tr>
                            <td align="center">

                               <div class="row"> <cc1:ucButton ID="btnContinueChanges_2" runat="server" Text="CONTINUE" CssClass="button"
                                    OnCommand="btnContinueChanges_2_Click" />
                                &nbsp;
                                <cc1:ucButton ID="btnBackChanges_2" runat="server" Text="BACK" CssClass="button" OnCommand="btnBackChanges_2_Click" />
                           </div> </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnContinue_2" />
            <asp:AsyncPostBackTrigger ControlID="btnBack_2" />
            <asp:AsyncPostBackTrigger ControlID="btnContinueChanges_2" />
            <asp:AsyncPostBackTrigger ControlID="btnBackChanges_2" />
        </Triggers>
    </asp:UpdatePanel>
    <%---WARNING popup Reject Provisional Booking End--%>


      <asp:UpdatePanel ID="UpdatePanel7" runat="server">
        <ContentTemplate>
            <asp:Button ID="btnNoSlotFound" runat="Server" Style="display: none" />
            <ajaxToolkit:ModalPopupExtender ID="mdlNoSlotFound" runat="server" TargetControlID="btnNoSlotFound"
                PopupControlID="pnlbNoSlotFound" BackgroundCssClass="modalBackground" BehaviorID="btnNoSlotFound"
                DropShadow="false" />
            <asp:Panel ID="pnlbNoSlotFound" runat="server" Style="display: none;">
                <div class="popup-maincontainer" style="overflow-y: hidden; overflow-x: hidden;">
                 <h3><cc1:ucLabel ID="lblWarningInfo_5" runat="server" ></cc1:ucLabel></h3>
                    <table cellspacing="5" cellpadding="0" border="0" align="center" class="popup-maincontainer">
                        <%--<tr>
                            <td style="font-weight: bold; color: Red; font-size: 14px; text-align: center;">
                                <cc1:ucLabel ID="UcLabel1" runat="server" Text="WARNING"></cc1:ucLabel>
                            </td>
                        </tr>--%>
                        <tr>
                            <td> <div  class="popup-innercontainer top-setting-Popup">
                                <div class="row1"><cc1:ucLiteral ID="UcLiteral1" runat="server" Text="New Booking not find the best spot on that date.Please review your booking"></cc1:ucLiteral></div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                             <div class="row">
                                <cc1:ucButton ID="btnOK" runat="server" Text="OK" CssClass="button" OnCommand="btnOK_Click" />
                                
                          </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnOK" />           
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>

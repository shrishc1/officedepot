﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Utilities;
using WebUtilities;
using BusinessEntities.ModuleBE.Appointment.Booking;
using BusinessLogicLayer.ModuleBAL.Appointment.Booking;
using BusinessEntities.ModuleBE.AdminFunctions;
using BusinessLogicLayer.ModuleBAL.AdminFunctions;
using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using BusinessLogicLayer.ModuleBAL.Appointment.SiteSettings;
using BaseControlLibrary;
using System.Drawing;

public partial class ModuleUI_Appointment_Booking_APPBok_BookingVolumeDetail : CommonPage
{
    #region Declarations
    MAS_SiteBE mas_SiteBE = null;
    APPBOK_BookingBE oAPPBOK_BookingBE = null;
    APPBOK_BookingBAL oAPPBOK_BookingBAL = null;

    int iPallets, iCartons;
    #endregion

    #region Events

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            lblErrorMessage.Text = String.Empty;
            BindVolumeDetails();
        }
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        EncryptQueryString("APPBok_BookingOverview.aspx");
    }

    protected void btnWindowsVolume_Click(object sender, EventArgs e)
    {
        lblSiteT.Text = Convert.ToString(Session["VolumeDetailSiteName"]);
        lblDateT.Text = Common.GetMM_DD_YYYY(Session["VolumeDetailDate"].ToString()).Date.ToString("dd/MM/yyyy");
        lblErrorMessage.Text = String.Empty;
        BindWindowVolumeDetails();
        mdlShowWindowVolume.Show();
    }

    protected void gvVolumeDetails_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        //if (e.Row.RowType != DataControlRowType.Header && e.Row.RowType != DataControlRowType.Footer)
        //{        
        //    decimal GetVolumeProcessTime = 0;
        //    HiddenField hdnTimeWindowID = (HiddenField)e.Row.FindControl("hdnTimeWindowID");
        //    ucLabel lblTotalTimeT = (ucLabel)e.Row.FindControl("lblTotalTimeT");
        //    ucLabel lblTimeRemainingT = (ucLabel)e.Row.FindControl("lblTimeRemainingT");

        //    ucLabel lblMaxPalletsT = (ucLabel)e.Row.FindControl("lblMaxPalletsT");
        //    ucLabel lblCurrentPalletsT = (ucLabel)e.Row.FindControl("lblCurrentPalletsT");

        //    ucLabel lblMaxCortonsT = (ucLabel)e.Row.FindControl("lblMaxCortonsT");
        //    ucLabel lblCurrentCortonsT = (ucLabel)e.Row.FindControl("lblCurrentCortonsT");

        //    ucLabel lblMaxLinesT = (ucLabel)e.Row.FindControl("lblMaxLinesT");
        //    ucLabel lblCurrentLinesT = (ucLabel)e.Row.FindControl("lblCurrentLinesT");

        //    #region Logic to set Red color of maximum Pallet, Corton and lines
        //    if (lblMaxPalletsT != null && lblCurrentPalletsT != null)
        //    {
        //        if (Convert.ToInt32(lblMaxPalletsT.Text) < Convert.ToInt32(lblCurrentPalletsT.Text))
        //            lblCurrentPalletsT.ForeColor = Color.Red;

        //        if (string.IsNullOrEmpty(lblMaxPalletsT.Text) || lblMaxPalletsT.Text.Equals("0"))
        //            lblMaxPalletsT.Text = "-";

        //        if (string.IsNullOrEmpty(lblCurrentPalletsT.Text) || lblCurrentPalletsT.Text.Equals("0"))
        //            lblCurrentPalletsT.Text = "-";
        //    }

        //    if (lblMaxCortonsT != null && lblCurrentCortonsT != null)
        //    {
        //        if (Convert.ToInt32(lblMaxCortonsT.Text) < Convert.ToInt32(lblCurrentCortonsT.Text))
        //            lblCurrentCortonsT.ForeColor = Color.Red;

        //        if (string.IsNullOrEmpty(lblMaxCortonsT.Text) || lblMaxCortonsT.Text.Equals("0"))
        //            lblMaxCortonsT.Text = "-";

        //        if (string.IsNullOrEmpty(lblCurrentCortonsT.Text) || lblCurrentCortonsT.Text.Equals("0"))
        //            lblCurrentCortonsT.Text = "-";
        //    }

        //    if (lblMaxLinesT != null && lblCurrentLinesT != null)
        //    {
        //        if (Convert.ToInt32(lblMaxLinesT.Text) < Convert.ToInt32(lblCurrentLinesT.Text))
        //            lblCurrentLinesT.ForeColor = Color.Red;

        //        if (string.IsNullOrEmpty(lblMaxLinesT.Text) || lblMaxLinesT.Text.Equals("0"))
        //            lblMaxLinesT.Text = "-";

        //        if (string.IsNullOrEmpty(lblCurrentLinesT.Text) || lblCurrentLinesT.Text.Equals("0"))
        //            lblCurrentLinesT.Text = "-";
        //    }
        //    #endregion

        //    #region Logic to set [Time Remaining] column
        //    if (hdnTimeWindowID != null)
        //    {
        //        oAPPBOK_BookingBE = new APPBOK_BookingBE();
        //        oAPPBOK_BookingBAL = new APPBOK_BookingBAL();
        //        oAPPBOK_BookingBE.Action = "GetVolumeProcessed";
        //        oAPPBOK_BookingBE.ScheduleDate = Common.GetMM_DD_YYYY(Session["VolumeDetailDate"].ToString());
        //        oAPPBOK_BookingBE.SiteId = Convert.ToInt32(Session["VolumeDetailSiteID"]);
        //        oAPPBOK_BookingBE.TimeWindow = new MASSIT_TimeWindowBE();
        //        oAPPBOK_BookingBE.TimeWindow.TimeWindowID = Convert.ToInt32(hdnTimeWindowID.Value);

        //        List<APPBOK_BookingBE> lstBookingDeatils = new List<APPBOK_BookingBE>();
        //        lstBookingDeatils = oAPPBOK_BookingBAL.GetGetVolumeProcessedBAL(oAPPBOK_BookingBE);
        //        if (lstBookingDeatils != null && lstBookingDeatils.Count > 0)
        //        {
        //            foreach (APPBOK_BookingBE Booking in lstBookingDeatils)
        //            {
        //                // Here Volume Process Time in coming in minutes.
        //                GetVolumeProcessTime += GetRequiredTimeSlotLengthForOthers(Booking.NumberOfPallet, Booking.NumberOfCartons, Booking.SupplierType, Booking.VendorCarrierID, Booking.VehicleType.VehicleTypeID);
        //            }
        //        }

        //        // Here converting Total time in minutes.
        //        decimal dmlTotalTime = (GetHHMMValue(lblTotalTimeT.Text, "HH") * 60) + GetHHMMValue(lblTotalTimeT.Text, "MM");

        //        // Here minusing the Volume Process Time from Total time.
        //        decimal dmlFinalTimeRemaining = dmlTotalTime - GetVolumeProcessTime;

        //        // Here setting the Remaining Time.
        //        if (GetVolumeProcessTime <= 0)
        //        {
        //            lblTimeRemainingT.Text = "00:00";
        //        }
        //        else if (dmlFinalTimeRemaining == 60 || dmlFinalTimeRemaining > 60)
        //        {
        //            int intQuotient = Convert.ToInt32(dmlFinalTimeRemaining) / 60;
        //            int intExactHours = (intQuotient * 60);
        //            dmlFinalTimeRemaining = dmlFinalTimeRemaining - intExactHours;

        //            lblTimeRemainingT.Text = string.Format("{0}:{1}", intQuotient, dmlFinalTimeRemaining);

        //            string strHH = string.Empty;
        //            string strMM = string.Empty;
                    
        //            if (GetHHMMValue(lblTimeRemainingT.Text, "HH") <= 9)
        //                strHH = string.Format("0{0}", GetHHMMValue(lblTimeRemainingT.Text, "HH"));
        //            else
        //                strHH = string.Format("{0}", GetHHMMValue(lblTimeRemainingT.Text, "HH"));

        //            if (GetHHMMValue(lblTimeRemainingT.Text, "MM") <= 9)
        //                strMM = string.Format("0{0}", GetHHMMValue(lblTimeRemainingT.Text, "MM"));
        //            else
        //                strMM = string.Format("{0}", GetHHMMValue(lblTimeRemainingT.Text, "MM"));

        //            lblTimeRemainingT.Text = string.Format("{0}:{1}", strHH, strMM);
        //            lblTimeRemainingT.ForeColor = Color.Black;
                    
        //        }
        //        else if (dmlFinalTimeRemaining > 0)
        //        {
        //            int intFinalTimeRemaining = (int)dmlFinalTimeRemaining;
        //            if (intFinalTimeRemaining <= 9)
        //                lblTimeRemainingT.Text = string.Format("00:0{0}", intFinalTimeRemaining);
        //            else
        //                lblTimeRemainingT.Text = string.Format("00:{0}", intFinalTimeRemaining);

        //            lblTimeRemainingT.ForeColor = Color.Black;
        //        }
        //        else if (dmlFinalTimeRemaining < 0)
        //        {
        //            int intQuotient = Convert.ToInt32(dmlFinalTimeRemaining) / 60;
        //            int intExactHours = (intQuotient * 60);
        //            dmlFinalTimeRemaining = dmlFinalTimeRemaining - intExactHours;

        //            lblTimeRemainingT.Text = string.Format("{0}:{1}", intQuotient, dmlFinalTimeRemaining).Replace("-", string.Empty);

        //            string strHH = string.Empty;
        //            string strMM = string.Empty;

        //            if (GetHHMMValue(lblTimeRemainingT.Text, "HH") <= 9)
        //                strHH = string.Format("0{0}", GetHHMMValue(lblTimeRemainingT.Text, "HH"));
        //            else
        //                strHH = string.Format("{0}", GetHHMMValue(lblTimeRemainingT.Text, "HH"));

        //            if (GetHHMMValue(lblTimeRemainingT.Text, "MM") <= 9)
        //                strMM = string.Format("0{0}", GetHHMMValue(lblTimeRemainingT.Text, "MM"));
        //            else
        //                strMM = string.Format("{0}", GetHHMMValue(lblTimeRemainingT.Text, "MM"));

        //            lblTimeRemainingT.Text = string.Format("-{0}:{1}", strHH, strMM);
        //            lblTimeRemainingT.ForeColor = Color.Black;
        //        }
        //        else
        //        {
        //            lblTimeRemainingT.Text = "00:00";
        //            lblTimeRemainingT.ForeColor = Color.Black;
        //        }

        //        if (lblTimeRemainingT.Text.Equals("0"))
        //            lblTimeRemainingT.Text = "00:00";   
               
        //    }
        //    #endregion
        //}
    }

    protected void rptVolumeDetails_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            decimal GetVolumeProcessTime = 0;
            HiddenField hdnTimeWindowID = (HiddenField)e.Item.FindControl("hdnTimeWindowID");
            ucLabel lblTotalTimeT = (ucLabel)e.Item.FindControl("lblTotalTimeT");
            ucLabel lblTimeRemainingT = (ucLabel)e.Item.FindControl("lblTimeRemainingT");

            ucLabel lblMaxPalletsT = (ucLabel)e.Item.FindControl("lblMaxPalletsT");
            ucLabel lblCurrentPalletsT = (ucLabel)e.Item.FindControl("lblCurrentPalletsT");

            ucLabel lblMaxCortonsT = (ucLabel)e.Item.FindControl("lblMaxCortonsT");
            ucLabel lblCurrentCortonsT = (ucLabel)e.Item.FindControl("lblCurrentCortonsT");

            ucLabel lblMaxLinesT = (ucLabel)e.Item.FindControl("lblMaxLinesT");
            ucLabel lblCurrentLinesT = (ucLabel)e.Item.FindControl("lblCurrentLinesT");

            #region Logic to set Red color of maximum Pallet, Corton and lines
            if (lblMaxPalletsT != null && lblCurrentPalletsT != null)
            {
                if (Convert.ToInt32(lblMaxPalletsT.Text) < Convert.ToInt32(lblCurrentPalletsT.Text))
                    lblCurrentPalletsT.ForeColor = Color.Red;

                if (string.IsNullOrEmpty(lblMaxPalletsT.Text) || lblMaxPalletsT.Text.Equals("0"))
                    lblMaxPalletsT.Text = "-";

                if (string.IsNullOrEmpty(lblCurrentPalletsT.Text) || lblCurrentPalletsT.Text.Equals("0"))
                    lblCurrentPalletsT.Text = "-";
            }

            if (lblMaxCortonsT != null && lblCurrentCortonsT != null)
            {
                if (Convert.ToInt32(lblMaxCortonsT.Text) < Convert.ToInt32(lblCurrentCortonsT.Text))
                    lblCurrentCortonsT.ForeColor = Color.Red;

                if (string.IsNullOrEmpty(lblMaxCortonsT.Text) || lblMaxCortonsT.Text.Equals("0"))
                    lblMaxCortonsT.Text = "-";

                if (string.IsNullOrEmpty(lblCurrentCortonsT.Text) || lblCurrentCortonsT.Text.Equals("0"))
                    lblCurrentCortonsT.Text = "-";
            }

            if (lblMaxLinesT != null && lblCurrentLinesT != null)
            {
                if (Convert.ToInt32(lblMaxLinesT.Text) < Convert.ToInt32(lblCurrentLinesT.Text))
                    lblCurrentLinesT.ForeColor = Color.Red;

                if (string.IsNullOrEmpty(lblMaxLinesT.Text) || lblMaxLinesT.Text.Equals("0"))
                    lblMaxLinesT.Text = "-";

                if (string.IsNullOrEmpty(lblCurrentLinesT.Text) || lblCurrentLinesT.Text.Equals("0"))
                    lblCurrentLinesT.Text = "-";
            }
            #endregion

            #region Logic to set [Time Remaining] column
            if (hdnTimeWindowID != null)
            {
                oAPPBOK_BookingBE = new APPBOK_BookingBE();
                oAPPBOK_BookingBAL = new APPBOK_BookingBAL();
                oAPPBOK_BookingBE.Action = "GetVolumeProcessed";
                oAPPBOK_BookingBE.ScheduleDate = Common.GetMM_DD_YYYY(Session["VolumeDetailDate"].ToString());
                oAPPBOK_BookingBE.SiteId = Convert.ToInt32(Session["VolumeDetailSiteID"]);
                oAPPBOK_BookingBE.TimeWindow = new MASSIT_TimeWindowBE();
                oAPPBOK_BookingBE.TimeWindow.TimeWindowID = Convert.ToInt32(hdnTimeWindowID.Value);

                List<APPBOK_BookingBE> lstBookingDeatils = new List<APPBOK_BookingBE>();
                lstBookingDeatils = oAPPBOK_BookingBAL.GetGetVolumeProcessedBAL(oAPPBOK_BookingBE);
                if (lstBookingDeatils != null && lstBookingDeatils.Count > 0)
                {
                    foreach (APPBOK_BookingBE Booking in lstBookingDeatils)
                    {
                        // Here Volume Process Time in coming in minutes.
                        GetVolumeProcessTime += GetRequiredTimeSlotLengthForOthers(Booking.NumberOfPallet, Booking.NumberOfCartons, Booking.SupplierType, Booking.VendorCarrierID, Booking.VehicleType.VehicleTypeID);
                    }
                }

                // Here converting Total time in minutes.
                decimal dmlTotalTime = (GetHHMMValue(lblTotalTimeT.Text, "HH") * 60) + GetHHMMValue(lblTotalTimeT.Text, "MM");

                // Here minusing the Volume Process Time from Total time.
                decimal dmlFinalTimeRemaining = dmlTotalTime - GetVolumeProcessTime;

                // Here setting the Remaining Time.
                if (GetVolumeProcessTime <= 0)
                {
                    lblTimeRemainingT.Text = "00:00";
                }
                else if (dmlFinalTimeRemaining == 60 || dmlFinalTimeRemaining > 60)
                {
                    int intQuotient = Convert.ToInt32(dmlFinalTimeRemaining) / 60;
                    int intExactHours = (intQuotient * 60);
                    dmlFinalTimeRemaining = dmlFinalTimeRemaining - intExactHours;

                    lblTimeRemainingT.Text = string.Format("{0}:{1}", intQuotient, dmlFinalTimeRemaining);

                    string strHH = string.Empty;
                    string strMM = string.Empty;

                    if (GetHHMMValue(lblTimeRemainingT.Text, "HH") <= 9)
                        strHH = string.Format("0{0}", GetHHMMValue(lblTimeRemainingT.Text, "HH"));
                    else
                        strHH = string.Format("{0}", GetHHMMValue(lblTimeRemainingT.Text, "HH"));

                    if (GetHHMMValue(lblTimeRemainingT.Text, "MM") <= 9)
                        strMM = string.Format("0{0}", GetHHMMValue(lblTimeRemainingT.Text, "MM"));
                    else
                        strMM = string.Format("{0}", GetHHMMValue(lblTimeRemainingT.Text, "MM"));

                    lblTimeRemainingT.Text = string.Format("{0}:{1}", strHH, strMM);
                    lblTimeRemainingT.ForeColor = Color.Black;

                }
                else if (dmlFinalTimeRemaining > 0)
                {
                    int intFinalTimeRemaining = (int)dmlFinalTimeRemaining;
                    if (intFinalTimeRemaining <= 9)
                        lblTimeRemainingT.Text = string.Format("00:0{0}", intFinalTimeRemaining);
                    else
                        lblTimeRemainingT.Text = string.Format("00:{0}", intFinalTimeRemaining);

                    lblTimeRemainingT.ForeColor = Color.Black;
                }
                else if (dmlFinalTimeRemaining < 0)
                {
                    int intQuotient = Convert.ToInt32(dmlFinalTimeRemaining) / 60;
                    int intExactHours = (intQuotient * 60);
                    dmlFinalTimeRemaining = dmlFinalTimeRemaining - intExactHours;

                    lblTimeRemainingT.Text = string.Format("{0}:{1}", intQuotient, dmlFinalTimeRemaining).Replace("-", string.Empty);

                    string strHH = string.Empty;
                    string strMM = string.Empty;

                    if (GetHHMMValue(lblTimeRemainingT.Text, "HH") <= 9)
                        strHH = string.Format("0{0}", GetHHMMValue(lblTimeRemainingT.Text, "HH"));
                    else
                        strHH = string.Format("{0}", GetHHMMValue(lblTimeRemainingT.Text, "HH"));

                    if (GetHHMMValue(lblTimeRemainingT.Text, "MM") <= 9)
                        strMM = string.Format("0{0}", GetHHMMValue(lblTimeRemainingT.Text, "MM"));
                    else
                        strMM = string.Format("{0}", GetHHMMValue(lblTimeRemainingT.Text, "MM"));

                    lblTimeRemainingT.Text = string.Format("-{0}:{1}", strHH, strMM);
                    lblTimeRemainingT.ForeColor = Color.Black;
                }
                else
                {
                    lblTimeRemainingT.Text = "00:00";
                    lblTimeRemainingT.ForeColor = Color.Black;
                }

                if (lblTimeRemainingT.Text.Equals("0"))
                    lblTimeRemainingT.Text = "00:00";

            }
            #endregion
        }
    }

    protected void btnBack_1_Click(object sender, EventArgs e)
    {
        mdlShowWindowVolume.Hide();
    }

    #endregion

    #region Methods

    private int GetHHMMValue(string timeText, string timeFormat)
    {
        int formatValue = 0;
        if (!string.IsNullOrEmpty(timeText))
        {
            timeText = timeText.Replace(".", ":");
            timeText = timeText.Replace("00", "0");
            var intIndex = -1;
            intIndex = timeText.IndexOf(":");
            if (intIndex != (-1))
            {
                switch (timeFormat)
                {
                    case "HH":
                        //timeText = timeText.Substring(0, intIndex);
                        formatValue = Convert.ToInt32(timeText.Substring(0, intIndex));
                        break;
                    case "MM":
                        //timeText = timeText.Remove(0, intIndex + 1);
                        formatValue = Convert.ToInt32(timeText.Remove(0, intIndex + 1));
                        break;
                    default:
                        break;
                }
            }            
        }
        return formatValue;
    }

    protected void BindVolumeDetails()
    {
        string strWeekDay= string.Empty;
        oAPPBOK_BookingBE = new APPBOK_BookingBE();
        oAPPBOK_BookingBAL = new APPBOK_BookingBAL();

        oAPPBOK_BookingBE.Action = "VolumeDetails";
        oAPPBOK_BookingBE.FixedSlot = new BusinessEntities.ModuleBE.Appointment.SiteSettings.MASSIT_FixedSlotBE();
        oAPPBOK_BookingBE.Carrier = new BusinessEntities.ModuleBE.Appointment.CountrySetting.MASCNT_CarrierBE();
        oAPPBOK_BookingBE.Delivery = new BusinessEntities.ModuleBE.Appointment.CountrySetting.MASCNT_DeliveryTypeBE();
        oAPPBOK_BookingBE.FixedSlot.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();
        if (GetQueryStringValue("SVT") != null)
        {
            oAPPBOK_BookingBE.FixedSlot.SiteID = Convert.ToInt32(GetQueryStringValue("SiteID"));
            oAPPBOK_BookingBE.ScheduleDate = Common.GetMM_DD_YYYY(GetQueryStringValue("ReportDate"));
            strWeekDay = Convert.ToDateTime(Common.GetDD_MM_YYYY(GetQueryStringValue("ReportDate"))).DayOfWeek.ToString().ToUpper();
        }
        else
        {
            oAPPBOK_BookingBE.FixedSlot.SiteID = Convert.ToInt32(Session["VolumeDetailSiteID"]);
            if (Session["VolumeDetailDate"] != null && Session["VolumeDetailDate"] != "")
            {
                oAPPBOK_BookingBE.ScheduleDate = Common.GetMM_DD_YYYY(Session["VolumeDetailDate"].ToString());
            }
            if (Session["VolumeDetailWeekDay"] != null && Session["VolumeDetailWeekDay"] != "")
                strWeekDay = Session["VolumeDetailWeekDay"].ToString();
        }     
   
        if (strWeekDay == "SUNDAY")
        {
            oAPPBOK_BookingBE.WeekDay = 1;
        }
        else if (strWeekDay == "MONDAY")
        {
            oAPPBOK_BookingBE.WeekDay = 2;
        }
        else if (strWeekDay == "TUESDAY")
        {
            oAPPBOK_BookingBE.WeekDay = 3;
        }
        else if (strWeekDay == "WEDNESDAY")
        {
            oAPPBOK_BookingBE.WeekDay = 4;
        }
        else if (strWeekDay == "THURSDAY")
        {
            oAPPBOK_BookingBE.WeekDay = 5;
        }
        else if (strWeekDay == "FRIDAY")
        {
            oAPPBOK_BookingBE.WeekDay = 6;
        }
        else if (strWeekDay == "SATURDAY")
        {
            oAPPBOK_BookingBE.WeekDay = 7;
        }

        mas_SiteBE = new MAS_SiteBE();
        if (GetQueryStringValue("SVT") != null)
        {
            mas_SiteBE = GetSingleSiteSetting(Convert.ToInt32(GetQueryStringValue("SiteID")));
        }
        if (Session["VolumeDetailSiteID"] != null)
            mas_SiteBE = GetSingleSiteSetting(Convert.ToInt32(Session["VolumeDetailSiteID"]));

        oAPPBOK_BookingBE.SupplierType = !string.IsNullOrEmpty(mas_SiteBE.TimeSlotWindow)?mas_SiteBE.TimeSlotWindow.Trim().ToUpper():null;

        DataTable dtVolumeDetails = oAPPBOK_BookingBAL.GetVolumeDetailsDAL(oAPPBOK_BookingBE);


        if (dtVolumeDetails != null && dtVolumeDetails.Rows.Count > 0)
        {
           
            //First pannel
            ltMDCPallets.Text = dtVolumeDetails.Rows[0]["EXPECTED_CONFIRMED_PALLETS_WS"].ToString();
            ltMDCLifts.Text = dtVolumeDetails.Rows[0]["EXPECTED_CONFIRMED_LIFTS_WS"].ToString();
            ltMDCLines.Text = dtVolumeDetails.Rows[0]["EXPECTED_CONFIRMED_LINES_WS"].ToString();

            ltCRTPallets.Text = dtVolumeDetails.Rows[0]["EXPECTED_CONFIRMED_PALLETS"].ToString();
            ltCRTLifts.Text = dtVolumeDetails.Rows[0]["EXPECTED_CONFIRMED_LIFTS"].ToString();
            ltCRTLines.Text = dtVolumeDetails.Rows[0]["EXPECTED_CONFIRMED_LINES"].ToString();

            int? iLifts = 0, iLines = 0, iPallets = 0;
            if (!string.IsNullOrEmpty(ltMDCPallets.Text) && !string.IsNullOrEmpty(ltCRTPallets.Text))
                iPallets = (Convert.ToInt32(dtVolumeDetails.Rows[0]["EXPECTED_CONFIRMED_PALLETS_WS"].ToString()) - Convert.ToInt32(dtVolumeDetails.Rows[0]["EXPECTED_CONFIRMED_PALLETS"].ToString()));

            if (!string.IsNullOrEmpty(ltMDCLifts.Text) && !string.IsNullOrEmpty(ltCRTLifts.Text))
                iLifts = (Convert.ToInt32(dtVolumeDetails.Rows[0]["EXPECTED_CONFIRMED_LIFTS_WS"].ToString()) - Convert.ToInt32(dtVolumeDetails.Rows[0]["EXPECTED_CONFIRMED_LIFTS"].ToString()));

            if (!string.IsNullOrEmpty(ltMDCLines.Text) && !string.IsNullOrEmpty(ltCRTLines.Text))
                iLines = (Convert.ToInt32(dtVolumeDetails.Rows[0]["EXPECTED_CONFIRMED_LINES_WS"].ToString()) - Convert.ToInt32(dtVolumeDetails.Rows[0]["EXPECTED_CONFIRMED_LINES"].ToString()));

            if (iPallets < 0) iPallets = 0;
            if (iLifts < 0) iLifts = 0;
            if (iLines < 0) iLines = 0;

            ltACPallets.Text = iPallets.ToString();
            ltACLifts.Text = iLifts.ToString();
            ltACLines.Text = iLines.ToString();

            //End of first pannel


            //Second pannel
            if (oAPPBOK_BookingBE.SupplierType != "W")
            {
                trVolumes.Visible = true;
                trVolumesWindow.Visible = false;
                ltDeliveriesExpectedConfirmed.Text = dtVolumeDetails.Rows[0]["CONFIRMED_DELIVERIES"].ToString();
                ltDeliveriesArrived.Text = dtVolumeDetails.Rows[0]["Arrived_Delievery"].ToString();
                ltDeliveriesUnloaded.Text = dtVolumeDetails.Rows[0]["Unload_Delievery"].ToString();
                ltDeliveriesStillToBeDelivered.Text = dtVolumeDetails.Rows[0]["DELIVERED_Delievery"].ToString();
                ltDeliveriesStillToBeUnloaded.Text = dtVolumeDetails.Rows[0]["UNLOADED_Delievery"].ToString();

                ltLiftsExpectedConfirmed.Text = dtVolumeDetails.Rows[0]["CONFIRMED_LIFTS"].ToString();
                ltLiftsArrived.Text = dtVolumeDetails.Rows[0]["Arrived_Lifts"].ToString();
                ltLiftsUnloaded.Text = dtVolumeDetails.Rows[0]["Unload_Lifts"].ToString();
                ltLiftsStillToBeDelivered.Text = dtVolumeDetails.Rows[0]["DELIVERED_Lifts"].ToString();
                ltLiftsStillToBeUnloaded.Text = dtVolumeDetails.Rows[0]["UNLOADED_Lifts"].ToString();

                ltPalletsExtectedConfirmed.Text = dtVolumeDetails.Rows[0]["CONFIRMED_PALLETS"].ToString();
                ltPalletsArrived.Text = dtVolumeDetails.Rows[0]["Arrived_Pallets"].ToString();
                ltPalletsUnloaded.Text = dtVolumeDetails.Rows[0]["Unload_Pallets"].ToString();
                ltPalletsStillToBeDelivered.Text = dtVolumeDetails.Rows[0]["DELIVERED_Pallets"].ToString();
                ltPalletsStillToBeUnloaded.Text = dtVolumeDetails.Rows[0]["UNLOADED_Pallets"].ToString();

                ltLinesExtectedConfirmed.Text = dtVolumeDetails.Rows[0]["CONFIRMED_LINES"].ToString();
                ltLinesArrived.Text = dtVolumeDetails.Rows[0]["Arrived_Lines"].ToString();
                ltLinesUnloaded.Text = dtVolumeDetails.Rows[0]["Unload_Lines"].ToString();
                ltLinesStillToBeDelivered.Text = dtVolumeDetails.Rows[0]["DELIVERED_Lines"].ToString();
                ltLinesStillToBeUnloaded.Text = dtVolumeDetails.Rows[0]["UNLOADED_Lines"].ToString();

                ltCartonsExtectedConfirmed.Text = dtVolumeDetails.Rows[0]["CONFIRMED_CARTONS"].ToString();
                ltCartonsArrived.Text = dtVolumeDetails.Rows[0]["Arrived_Cartons"].ToString();
                ltCartonsUnloaded.Text = dtVolumeDetails.Rows[0]["Unload_Cartons"].ToString();
                ltCartonsStillToBeDelivered.Text = dtVolumeDetails.Rows[0]["DELIVERED_Cartons"].ToString();
                ltCartonsStillToBeUnloaded.Text = dtVolumeDetails.Rows[0]["UNLOADED_Cartons"].ToString();
            }
            else
            {
                trVolumes.Visible = false;
                trVolumesWindow.Visible = true;
                ltDeliveriesExpectedConfirmed_1.Text = dtVolumeDetails.Rows[0]["CONFIRMED_DELIVERIES"].ToString();
                ltDeliveriesReceived_1.Text = dtVolumeDetails.Rows[0]["RECEIVED_DELIVERIES"].ToString();
                ltDeliveriesStillToBeDelivered_1.Text = dtVolumeDetails.Rows[0]["STILLTOBE_DELIVERED_DELIVERIES"].ToString();

                ltLiftsExpectedConfirmed_1.Text = dtVolumeDetails.Rows[0]["CONFIRMED_LIFTS"].ToString();
                ltLiftsReceived_1.Text = dtVolumeDetails.Rows[0]["RECEIVED_LIFTS"].ToString();
                ltLiftsStillToBeDelivered_1.Text = dtVolumeDetails.Rows[0]["STILLTOBE_DELIVERED_LIFTS"].ToString();

                ltPalletsExtectedConfirmed_1.Text = dtVolumeDetails.Rows[0]["CONFIRMED_PALLETS"].ToString();
                ltPalletsReceived_1.Text = dtVolumeDetails.Rows[0]["RECEIVED_PALLETS"].ToString();
                ltPalletsStillToBeDelivered_1.Text = dtVolumeDetails.Rows[0]["STILLTOBE_DELIVERED_PALLETS"].ToString();

                ltLinesExtectedConfirmed_1.Text = dtVolumeDetails.Rows[0]["CONFIRMED_LINES"].ToString();
                ltLinesReceived_1.Text = dtVolumeDetails.Rows[0]["RECEIVED_LINES"].ToString();
                ltLinesStillToBeDelivered_1.Text = dtVolumeDetails.Rows[0]["STILLTOBE_DELIVERED_LINES"].ToString();

                ltCartonsExtectedConfirmed_1.Text = dtVolumeDetails.Rows[0]["CONFIRMED_CARTONS"].ToString();
                ltCartonsReceived_1.Text = dtVolumeDetails.Rows[0]["RECEIVED_CARTONS"].ToString();
                ltCartonsStillToBeDelivered_1.Text = dtVolumeDetails.Rows[0]["STILLTOBE_DELIVERED_CARTONS"].ToString();

            }
            //End of second pannel

            //Third pannel

            ltHoursRequired.Text = dtVolumeDetails.Rows[0]["Number_of_Hours_Required"].ToString();
            ltUnloadingHrs.Text = dtVolumeDetails.Rows[0]["Number_of_Unloading_Hours"].ToString();
            ltReceivingHrs.Text = dtVolumeDetails.Rows[0]["Number_of_Receiving_Hours"].ToString();
            ltPutawayHrs.Text = dtVolumeDetails.Rows[0]["Number_of_Putaway_Hours"].ToString();

            ltHoursRequiredTotal.Text = dtVolumeDetails.Rows[0]["Number_of_Hours_RequiredTotal"].ToString();
            ltUnloadingHrsTotal.Text = dtVolumeDetails.Rows[0]["Number_of_Unloading_HoursTotal"].ToString();
            ltReceivingHrsTotal.Text = dtVolumeDetails.Rows[0]["Number_of_Receiving_HoursTotal"].ToString();
            ltPutawayHrsTotal.Text = dtVolumeDetails.Rows[0]["Number_of_Putaway_HoursTotal"].ToString();
            //End of third pannel

            //Fourth panel

            if (!mas_SiteBE.TimeSlotWindow.Trim().ToUpper().Equals("W")) {
                btnWindowsVolume.Visible = false;
            }
            else {
                btnWindowsVolume.Visible = true;
            }

            //if (!mas_SiteBE.TimeSlotWindow.Trim().ToUpper().Equals("W"))
            //{

                ltFSCDeliveries.Text = dtVolumeDetails.Rows[0]["FS_CONFIRMED_DELIVERIES"].ToString();
                ltFSODeliveries.Text = dtVolumeDetails.Rows[0]["FS_OPEN_DELIVERIES_H"].ToString();
            ltFSDeliveries_SOft.Text = dtVolumeDetails.Rows[0]["FS_OPEN_DELIVERIES_S"].ToString()!=string.Empty?dtVolumeDetails.Rows[0]["FS_OPEN_DELIVERIES_S"].ToString():"0";
                ltFSTDeliveries.Text = dtVolumeDetails.Rows[0]["FS_TOTAL_DELIVERIES"].ToString();

                ltFSCPallets.Text = dtVolumeDetails.Rows[0]["FS_CONFIRMED_PALLETS"].ToString();
                ltFSOPallets.Text = dtVolumeDetails.Rows[0]["FS_OPEN_PALLETS_H"].ToString();
            ltFSOPallets_SOft.Text = dtVolumeDetails.Rows[0]["FS_OPEN_PALLETS_S"].ToString()!=string.Empty?dtVolumeDetails.Rows[0]["FS_OPEN_PALLETS_S"].ToString():"0";
                ltFSTPallets.Text = dtVolumeDetails.Rows[0]["FS_TOTAL_PALLETS"].ToString();

                ltFSCCartons.Text = dtVolumeDetails.Rows[0]["FS_CONFIRMED_CARTONS"].ToString();
                ltFSOCartons.Text = dtVolumeDetails.Rows[0]["FS_OPEN_CARTONS_H"].ToString();
            ltFSOCartons_Soft.Text = dtVolumeDetails.Rows[0]["FS_OPEN_CARTONS_S"].ToString()!=string.Empty?dtVolumeDetails.Rows[0]["FS_OPEN_CARTONS_S"].ToString():"0";
                ltFSTCartons.Text = dtVolumeDetails.Rows[0]["FS_TOTAL_CARTONS"].ToString();

                ltFSCLines.Text = dtVolumeDetails.Rows[0]["FS_CONFIRMED_LINES"].ToString();
                ltFSOLines.Text = dtVolumeDetails.Rows[0]["FS_HARD_LINES"].ToString();
                ltFSOLines_Soft.Text = dtVolumeDetails.Rows[0]["FS_SOFT_LINES"].ToString() != string.Empty ? dtVolumeDetails.Rows[0]["FS_SOFT_LINES"].ToString() : "0";
                ltFSTLines.Text = dtVolumeDetails.Rows[0]["FS_TOTAL_Lines"].ToString();

               
            //}
            //else
            //{
            //    ltFSCDeliveries.Text = "0";
            //    ltFSODeliveries.Text = "0";
            //    ltFSTDeliveries.Text = "0";

            //    ltFSCPallets.Text = "0";
            //    ltFSOPallets.Text = "0";
            //    ltFSTPallets.Text = "0";

            //    ltFSCCartons.Text = "0";
            //    ltFSOCartons.Text = "0";
            //    ltFSTCartons.Text = "0";

            //    btnWindowsVolume.Visible = true;
            //}

            //End of Fourth panel
        }
    }

    private MAS_SiteBE GetSingleSiteSetting(int siteId)
    {
        MAS_SiteBAL oMAS_SiteBAL = new MAS_SiteBAL();
        MAS_SiteBE oMAS_SiteBE = new MAS_SiteBE();
        oMAS_SiteBE.Action = "GetSingleSiteSetting";
        oMAS_SiteBE.SiteID = siteId;
        MAS_SiteBE localMAS_SiteBE = oMAS_SiteBAL.GetSingleSiteSettingBAL(oMAS_SiteBE);
        return localMAS_SiteBE;
    }

    private void BindWindowVolumeDetails()
    {
        oAPPBOK_BookingBE = new APPBOK_BookingBE();
        oAPPBOK_BookingBAL = new APPBOK_BookingBAL();
        oAPPBOK_BookingBE.Action = "GetWindowVolumeData";
        oAPPBOK_BookingBE.SiteId = Convert.ToInt32(Session["VolumeDetailSiteID"]);
        oAPPBOK_BookingBE.ScheduleDate = Common.GetMM_DD_YYYY(Session["VolumeDetailDate"].ToString());
        List<APPBOK_BookingBE> lstWindowVolumeDetails = oAPPBOK_BookingBAL.GetWindowVolumeDataBAL(oAPPBOK_BookingBE);
        if (lstWindowVolumeDetails.Count > 0)
        {
            lblErrorMessage.Text = String.Empty;
            rptVolumeDetails.DataSource = lstWindowVolumeDetails;
            rptVolumeDetails.DataBind();
        }
        else
        {
            lblErrorMessage.Text = WebCommon.getGlobalResourceValue("RecordNotFound");
            rptVolumeDetails.DataSource = null;
            rptVolumeDetails.DataBind();

        }
    }

    private decimal GetRequiredTimeSlotLengthForOthers(int? TotalPallets, int? TotalCartons, string SupplierType, int ID, int VehicleID)
    {
        //--------start Calculate Unload Time-------------------//
        decimal TotalCalculatedTime = 0;  //In Min
        bool isTimeCalculatedForPallets = false;
        bool isTimeCalculatedForCartons = false;

        TotalPallets = TotalPallets == null ? 0 : TotalPallets;
        TotalCartons = TotalCartons == null ? 0 : TotalCartons;

        if (SupplierType == "V")
        {
            //Step1 >Check Vendor Processing Window 
            if (isTimeCalculatedForPallets == false || isTimeCalculatedForCartons == false)
            {

                MASSIT_VendorProcessingWindowBE oMASSIT_VendorProcessingWindowBE = new MASSIT_VendorProcessingWindowBE();
                APPSIT_VendorProcessingWindowBAL oMASSIT_VendorProcessingWindowBAL = new APPSIT_VendorProcessingWindowBAL();

                oMASSIT_VendorProcessingWindowBE.Action = "ShowAllConsolidate";
                oMASSIT_VendorProcessingWindowBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
                oMASSIT_VendorProcessingWindowBE.User.UserID = Convert.ToInt32(Session["UserID"]);
                oMASSIT_VendorProcessingWindowBE.VendorID = ID;
                oMASSIT_VendorProcessingWindowBE.SiteID = Convert.ToInt32(Convert.ToInt32(Session["VolumeDetailSiteID"]));


                MASSIT_VendorProcessingWindowBE lstVendor = new MASSIT_VendorProcessingWindowBE();
                lstVendor = oMASSIT_VendorProcessingWindowBAL.GetVendorDetailsByIdBAL(oMASSIT_VendorProcessingWindowBE);

                if (lstVendor != null)
                {  //Vendor Processing Window Not found

                    if (lstVendor.APP_PalletsUnloadedPerHour > 0 && TotalPallets != 0 && isTimeCalculatedForPallets == false)
                    {
                        TotalCalculatedTime += Convert.ToDecimal(Convert.ToInt32(TotalPallets) * (60.0 / lstVendor.APP_PalletsUnloadedPerHour));
                        isTimeCalculatedForPallets = true;
                    }
                    if (lstVendor.APP_CartonsUnloadedPerHour > 0 && TotalCartons != 0 && isTimeCalculatedForCartons == false)
                    {
                        TotalCalculatedTime += Convert.ToDecimal(Convert.ToInt32(TotalCartons) * (60.0 / lstVendor.APP_CartonsUnloadedPerHour));
                        isTimeCalculatedForCartons = true;
                    }
                }
            }
            //---------------------------------// 
        }

        if (SupplierType == "C")
        {
            //Step1 >Check Carrier Processing Window 
            if (isTimeCalculatedForPallets == false || isTimeCalculatedForCartons == false)
            {

                MASSIT_CarrierProcessingWindowBE oMASSIT_CarrerProcessingWindowBE = new MASSIT_CarrierProcessingWindowBE();
                APPSIT_CarrierProcessingWindowBAL oMASSIT_CarrerProcessingWindowBAL = new APPSIT_CarrierProcessingWindowBAL();

                oMASSIT_CarrerProcessingWindowBE.Action = "GetConstraints";
                oMASSIT_CarrerProcessingWindowBE.CarrierID = ID;
                oMASSIT_CarrerProcessingWindowBE.SiteID = Convert.ToInt32(Convert.ToInt32(Session["VolumeDetailSiteID"]));
                MASSIT_CarrierProcessingWindowBE lstCarrier = oMASSIT_CarrerProcessingWindowBAL.GetConstraintsBAL(oMASSIT_CarrerProcessingWindowBE);


                if (lstCarrier != null)
                {  //Carrier Processing Window Not found

                    if (lstCarrier.APP_PalletsUnloadedPerHour > 0 && TotalPallets != 0 && isTimeCalculatedForPallets == false)
                    {
                        TotalCalculatedTime += Convert.ToDecimal(Convert.ToInt32(TotalPallets) * (60.0 / lstCarrier.APP_PalletsUnloadedPerHour));
                        isTimeCalculatedForPallets = true;
                    }
                    if (lstCarrier.APP_CartonsUnloadedPerHour > 0 && TotalCartons != 0 && isTimeCalculatedForCartons == false)
                    {
                        TotalCalculatedTime += Convert.ToDecimal(Convert.ToInt32(TotalCartons) * (60.0 / lstCarrier.APP_CartonsUnloadedPerHour));
                        isTimeCalculatedForCartons = true;
                    }
                }
            }
            //---------------------------------// 
        }


        //Step2 > Check Vehicle Type Setup  
        if (isTimeCalculatedForPallets == false || isTimeCalculatedForCartons == false)
        {
            MASSIT_VehicleTypeBE oMASSIT_VehicleTypeBE = new MASSIT_VehicleTypeBE();
            APPSIT_VehicleTypeBAL oMASSIT_VehicleTypeBAL = new APPSIT_VehicleTypeBAL();

            oMASSIT_VehicleTypeBE.Action = "ShowById";
            oMASSIT_VehicleTypeBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
            oMASSIT_VehicleTypeBE.User.UserID = Convert.ToInt32(Session["UserID"]);
            oMASSIT_VehicleTypeBE.VehicleTypeID = VehicleID;
            MASSIT_VehicleTypeBE lstVehicleType = oMASSIT_VehicleTypeBAL.GetVehicleTypeDetailsByIdBAL(oMASSIT_VehicleTypeBE);
            if (lstVehicleType != null)
            {
                //Volume Capacity 
                if (lstVehicleType.APP_PalletsUnloadedPerHour > 0 && iPallets != 0 && isTimeCalculatedForPallets == false)
                {
                    TotalCalculatedTime += Convert.ToDecimal(Convert.ToInt32(iPallets) * (60.0 / lstVehicleType.APP_PalletsUnloadedPerHour));
                    isTimeCalculatedForPallets = true;
                }
                if (lstVehicleType.APP_CartonsUnloadedPerHour > 0 && iCartons != 0 && isTimeCalculatedForCartons == false)
                {
                    TotalCalculatedTime += Convert.ToDecimal(Convert.ToInt32(iCartons) * (60.0 / lstVehicleType.APP_CartonsUnloadedPerHour));
                    isTimeCalculatedForCartons = true;
                }

                if (isTimeCalculatedForPallets == false && isTimeCalculatedForCartons == false)
                { //Time Based 
                    if (lstVehicleType.TotalUnloadTimeInMinute > 0)
                    {
                        TotalCalculatedTime += Convert.ToDecimal(Convert.ToDecimal(lstVehicleType.TotalUnloadTimeInMinute));
                        isTimeCalculatedForPallets = true;
                        isTimeCalculatedForCartons = true;
                    }
                }

                //---------------Stage 14 Point 15-------------//
                if (lstVehicleType.TotalUnloadTimeInMinute <= 0 && TotalCalculatedTime < lstVehicleType.MinimumTimePerDeliveryInMinute) {
                    TotalCalculatedTime = lstVehicleType.MinimumTimePerDeliveryInMinute;
                }
                //-------------------------------------------//
            }
        }
        //---------------------------------// 


        ////Step3 >Check Site Miscellaneous Settings
        //if (isTimeCalculatedForPallets == false || isTimeCalculatedForCartons == false)
        //{

        //    MAS_SiteBE oMAS_SiteBE = new MAS_SiteBE();
        //    MAS_SiteBAL oMAS_SiteBAL = new MAS_SiteBAL();

        //    if (hdnCurrentRole.Value == "Vendor")
        //        oMAS_SiteBE.Action = "ShowAllForVendor";
        //    else
        //        oMAS_SiteBE.Action = "ShowAll";

        //    oMAS_SiteBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
        //    oMAS_SiteBE.User.UserID = Convert.ToInt32(Session["UserID"]);
        //    oMAS_SiteBE.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);

        //    List<MAS_SiteBE> lstSiteMisSettings = new List<MAS_SiteBE>();
        //    lstSiteMisSettings = oMAS_SiteBAL.GetSiteMisSettingBAL(oMAS_SiteBE);

        //    if (lstSiteMisSettings != null && lstSiteMisSettings.Count > 0)
        //    {
        //        if (lstSiteMisSettings[0].AveragePalletUnloadedPerManHour > 0 && TotalPallets != 0 && isTimeCalculatedForPallets == false)
        //        {
        //            TotalCalculatedTime += Convert.ToDecimal(Convert.ToInt32(TotalPallets) * (60.0 / lstSiteMisSettings[0].AveragePalletUnloadedPerManHour));      // Convert.ToDecimal((Convert.ToInt32(TotalPallets) / lstSiteMisSettings[0].AveragePalletUnloadedPerManHour) * 60);
        //            isTimeCalculatedForPallets = true;
        //        }
        //        if (lstSiteMisSettings[0].AverageCartonUnloadedPerManHour > 0 && TotalCartons != 0 && isTimeCalculatedForCartons == false)
        //        {
        //            TotalCalculatedTime += Convert.ToDecimal(Convert.ToInt32(TotalCartons) * (60.0 / lstSiteMisSettings[0].AverageCartonUnloadedPerManHour));      //Convert.ToDecimal((Convert.ToInt32(TotalCartons) / lstSiteMisSettings[0].AverageCartonUnloadedPerManHour) * 60);
        //            isTimeCalculatedForCartons = true;
        //        }
        //    }
        //}
        //---------------------------------//   

        ////Calculate Slot Length
        //int intResult = (int)TotalCalculatedTime;
        //decimal dmlr = TotalCalculatedTime;

        ////Not Required in case of Window booking -- Abhinav
        ////if (intResult > 0)
        ////    intr++;

        //if (TotalCalculatedTime > 0 && TotalCalculatedTime < 1)
        //    dmlr = 1;

        //if (TotalCalculatedTime == 0)
        //    dmlr++;

        // TotalCalculatedTime = TotalCalculatedTime / Convert.ToDecimal(60.0);

        /* TotalCalculatedTime returning in minutes. */
        return TotalCalculatedTime;
    }

    #endregion
}
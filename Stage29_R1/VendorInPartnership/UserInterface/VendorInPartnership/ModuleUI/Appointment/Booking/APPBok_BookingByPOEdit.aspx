﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    AutoEventWireup="true" CodeFile="APPBok_BookingByPOEdit.aspx.cs" Inherits="APPBok_BookingByPOEdit" %>

<%@ Register TagName="ucDate" TagPrefix="cc2" Src="~/CommonUI/UserControls/ucDate.ascx" %>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="../../../CommonUI/UserControls/ucSite.ascx" TagName="ucSite" TagPrefix="cc2" %>
<%@ Register Src="../../../CommonUI/UserControls/ucSeacrhVendor.ascx" TagName="ucSeacrhVendor"
    TagPrefix="cc3" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- Style Sheet Section-->
    <style type="text/css">
        .fixedColumn .fixedTable td
        {
            color: Black;
            background-color: Gray;
            font-size: 12px;
            font-weight: normal;
            border: 1px solid;
        }
        
        .fixedHead td, .fixedFoot td
        {
            color: Black;
            background-color: Gray;
            font-size: 12px;
            font-weight: normal;
            padding: 5px;
            border: 1px solid;
        }
        .fixedTable td
        {
            font-size: 8.5pt;
            background-color: #FFFFFF;
            padding: 5px;
            text-align: left; /*border: 0px solid #CEE7FF;*/
        }
    </style>
    <!-- JS Section-->
    <script type="text/javascript" src="Support/sh_main.min.js"></script>
    <script type="text/javascript" src="Support/sh_javascript.js"></script>
    <script type="text/javascript" src="Support/jquery.fixedtable.js"></script>
    <script type="text/javascript">

        $(document).ready(function () {

            //sh_highlightDocument();

            $(".tableDiv").each(function () {
              
                var Id = $(this).get(0).id;
                var maintbheight = 335;
                var maintbwidth = 940;

                $("#" + Id + " .FixedTables").fixedTable({
                    width: maintbwidth,
                    height: maintbheight,
                    fixedColumns: 1,
                    classHeader: "fixedHead",
                    classFooter: "fixedFoot",
                    classColumn: "fixedColumn",
                    fixedColumnWidth: 145,
                    outerId: Id,
                    Contentbackcolor: "#FFFFFF",
                    Contenthovercolor: "#99CCFF",
                    fixedColumnbackcolor: "#187BAF",
                    fixedColumnhovercolor: "#99CCFF",                    
                });
            });
        });


    </script>
    <script type="text/javascript">
        function pageLoad() {
            $('.fixedHead').find('td').each(function () {
                $(this).width(165);
            });

            $('.fixedTable').scroll(function () {
                document.getElementById('<%=scroll1.ClientID%>').value = $(this).scrollTop(); // 
                document.getElementById('<%=scroll2.ClientID%>').value = $(this).scrollLeft();

            });

            //setScroll();
        }
    </script>
    
    <!-- UI Section-->
    <asp:ScriptManager ID="smBookingByPO" runat="server">
    </asp:ScriptManager>
    <input type="hidden" id="scroll1" runat="server" />
    <input type="hidden" id="scroll2" runat="server" />
    <div id="divVolume" runat="server" class="balloonstyle">
    </div>
    <div id="dhtmltooltip" class="balloonstyle">
    </div>
    <iframe id="iframetop" scrolling="no" frameborder="0" class="iballoonstyle" width="0"
        height="0"></iframe>
    <div class="balloonstyle" id="ssn123">
        <span class="bla8blue">
            <cc1:ucLabel ID="lblNormal" runat="server" Text="Normal"></cc1:ucLabel>
            <br />
            <cc1:ucLabel ID="lblNormalDeliveryDesc" runat="server" Text="This delivery type accounts for 99% of deliveries and relates to a requests to confirm a standard / fixed booking slot or alternatively a request for a booking slot against a Office Depot Purchase Order Number"></cc1:ucLabel>
            <br />
            <br />
            <cc1:ucLabel ID="lblInitialDistribution" runat="server" Text="Initial Distribution"></cc1:ucLabel><br />
            <cc1:ucLabel ID="lblInitialDistributionDesc" runat="server" Text="This delivery type relate to stock order for a mail out"></cc1:ucLabel>
            <br />
            <br />
            <cc1:ucLabel ID="lblReservation" runat="server" Text="Reservation"></cc1:ucLabel><br />
            <cc1:ucLabel ID="lblReservationDesc" runat="server" Text="This delivery type is when the stock is not order on a Office Depot Purchase Order. It is customer owned stock ordered directly by the customer"></cc1:ucLabel>
            <br />
            <br />
            <cc1:ucLabel ID="lblInternalSupplies" runat="server" Text="Internal Supplies"></cc1:ucLabel><br />
            <cc1:ucLabel ID="lblInternalSuppliesDesc" runat="server" Text="This delivery type relates to deliveries for Office Depot internal supplies - e.g.carton materials"></cc1:ucLabel>
        </span>
    </div>
    <div class="balloonstyle" id="ssn456">
        <span class="bla8blue">
            <cc1:ucLabel ID="lblCarrierDesc1" runat="server" Text="If the carrier who is making the delivery on your behalf is not listed, please select OTHER. You will then be asked to enter the name of the carrier which may then be added to the list for future deliveries."></cc1:ucLabel>
            <br />
            <cc1:ucLabel ID="lblCarrierDesc2" runat="server" Text="If you do not know the name of the carrier who is to make the delivery then select UNKNOWN. Please note that vehicle type is used to determine what slot and door is to be given to your booking. If the vehicle is unknown then this could lead to delays when unloading your vehicle"></cc1:ucLabel>
        </span>
    </div>
    <div class="balloonstyle" id="ssn789">
        <span class="bla8blue">
            <cc1:ucLabel ID="lblVehicleDesc1" runat="server" Text="If the vehicle type that is making the delivery is not listed, please select OTHER. You will then be asked to enter the type of vehicle. This may then be available for selection for future deliveries."></cc1:ucLabel>
            <br />
            <cc1:ucLabel ID="lblVehicleDesc2" runat="server" Text="If you do not know what type of vehicle is to be used then please select UNKNOWN."></cc1:ucLabel>
        </span>
    </div>
    <div class="balloonstyle" id="ssn101">
        <span class="bla8blue">
            <cc1:ucLabel ID="lblPalletsDesc" runat="server" Text="When pallets are stacked on top of each other and can be 'lifted' in one motion from the vehicle, this is what we define as a lift. EG if one pallet is placed on top of another and can be safely taken from the vehicle in one motion then this would be 2 pallets and 1 lift."></cc1:ucLabel>
        </span>
    </div>
    <h2>
        <cc1:ucLabel ID="lblBooking" runat="server" Text="Booking"></cc1:ucLabel>
    </h2>
    <asp:HiddenField ID="hdnFixedSlotMode" runat="server" />
    <asp:HiddenField ID="hdnCurrentMode" runat="server" />
    <asp:HiddenField ID="hdnBookingRef" runat="server" />
    <asp:HiddenField ID="hdnCapacityWarning" runat="server" />
    <asp:HiddenField ID="hdnCurrentRole" runat="server" />
    <asp:HiddenField ID="hdnSchedulerdate" runat="server" />
    <asp:HiddenField ID="hdnSuggestedSlotTimeID" runat="server" />
    <asp:HiddenField ID="hdnSuggestedSlotTime" runat="server" />
    <asp:HiddenField ID="hdnSuggestedFixedSlotID" runat="server" />
    <asp:HiddenField ID="hdnSuggestedSiteDoorNumberID" runat="server" />
    <asp:HiddenField ID="hdnSuggestedSiteDoorNumber" runat="server" />
    <asp:HiddenField ID="hdnCurrentFixedSlotID" runat="server" />
    <asp:HiddenField ID="hdnGraySlotClicked" runat="server" />
    <asp:HiddenField ID="hdnSuggestedWeekDay" runat="server" />
    <asp:HiddenField ID="hdnBeforeSlotTimeID" runat="server" />
    <asp:HiddenField ID="hdnAfterSlotTimeID" runat="server" />
    <asp:HiddenField ID="hdnCurrentWeekDay" runat="server" />
    <asp:HiddenField ID="hdnLogicalScheduleDate" runat="server" />
    <asp:HiddenField ID="hdnActualScheduleDate" runat="server" />
    <asp:HiddenField ID="hdnDoorReference" runat="server" />
    <div class="right-shadow">
        <div class="formbox">
            <cc1:ucMultiView ID="mvBookingEdit" runat="server" ActiveViewIndex="0">
                <cc1:ucView ID="vwBookingType" runat="server">
                    <cc1:ucPanel ID="pnlSupplierType" runat="server" GroupingText="Supplier Type" CssClass="fieldset-form">
                        <table width="50%" cellspacing="5" align="center" cellpadding="0" border="0" class="top-settingsNoBorder">
                            <tr>
                                <td align="center">
                                    <cc1:ucButton ID="btnVendor" runat="server" Text="Vendor" CssClass="button" OnClick="btnVendor_Click" />
                                    &nbsp;&nbsp;
                                    <cc1:ucButton ID="btnCarrier" runat="server" Text="Carrier" CssClass="button" OnClick="btnCarrier_Click" />
                                </td>
                            </tr>
                        </table>
                    </cc1:ucPanel>
                </cc1:ucView>
                <cc1:ucView ID="vwBookingSetup" runat="server">
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <%--<asp:ValidationSummary ID="vSummary" runat="server" ShowMessageBox="true" ShowSummary="false"
                                Style="color: Red" ValidationGroup="DeliveryDetail" />
                            <asp:RequiredFieldValidator ID="rfvDeliveryTypeRequired" runat="server" ControlToValidate="ddlDeliveryType"
                                Display="None" ValidationGroup="DeliveryDetail" SetFocusOnError="true" InitialValue="0">
                            </asp:RequiredFieldValidator>
                            <asp:RequiredFieldValidator ID="rfvCarrierRequired" runat="server" ControlToValidate="ddlCarrier"
                                Display="None" ValidationGroup="DeliveryDetail" SetFocusOnError="true" InitialValue="0">
                            </asp:RequiredFieldValidator>
                            <asp:RequiredFieldValidator ID="rfvVehicleTypeRequired" runat="server" ControlToValidate="ddlVehicleType"
                                Display="None" ValidationGroup="DeliveryDetail" SetFocusOnError="true" InitialValue="0">
                            </asp:RequiredFieldValidator>
                            <asp:RequiredFieldValidator ID="rfvSchedulingdateRequired" runat="server" ControlToValidate="txtSchedulingdate$txtUCDate"
                                Display="None" ValidationGroup="DeliveryDetail" SetFocusOnError="true">
                            </asp:RequiredFieldValidator>
                            <asp:RequiredFieldValidator ID="rfvCarrierOtherRequired" runat="server" ErrorMessage="xxx"
                                ControlToValidate="txtCarrierOther" Display="None" ValidationGroup="DeliveryDetail"
                                SetFocusOnError="true" Enabled="false">
                            </asp:RequiredFieldValidator>
                            <asp:RequiredFieldValidator ID="rfvVehicleTypeOtherRequired" runat="server" ErrorMessage="xxx"
                                ControlToValidate="txtVehicleOther" Display="None" ValidationGroup="DeliveryDetail"
                                SetFocusOnError="true" Enabled="false">
                            </asp:RequiredFieldValidator>
                            <cc1:ucPanel ID="pnlDeliveryDetail" runat="server" GroupingText="Delivery Detail"
                                CssClass="fieldset-form">
                                <table width="100%" cellspacing="5" cellpadding="0" border="0" class="top-settingsNoBorder"
                                    onmouseover="setToolTip();">
                                    <tr>
                                        <td style="font-weight: bold; width: 10%;">
                                            <cc1:ucLabel ID="lblSitePrefixName" runat="server" Text="Site" isRequired="true"></cc1:ucLabel>
                                        </td>
                                        <td style="font-weight: bold; width: 3%;">
                                            :
                                        </td>
                                        <td style="font-weight: bold; width: 33%;">
                                            <span id="spSite" runat="server">
                                                <cc2:ucSite ID="ddlSite" runat="server" />
                                            </span>
                                            <cc1:ucLiteral ID="ltSelectedSite" runat="server" Visible="false"></cc1:ucLiteral>
                                        </td>
                                        <td style="font-weight: bold;" width="10%">
                                            <cc1:ucLabel ID="lblVendorNo" runat="server" Text="Vendor No" isRequired="true"></cc1:ucLabel>
                                        </td>
                                        <td style="font-weight: bold; width: 3%">
                                            :
                                        </td>
                                        <td style="font-weight: bold; width: 41%">
                                            <span id="spVender" runat="server">
                                                <cc3:ucSeacrhVendor ID="ucSeacrhVendor1" runat="server" />
                                            </span>
                                            <cc1:ucLiteral ID="ltSearchVendorName" runat="server" Visible="false"></cc1:ucLiteral>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: bold;">
                                            <cc1:ucLabel ID="lblDeliveryType" runat="server" Text="Delivery Type" isRequired="true"></cc1:ucLabel>
                                        </td>
                                        <td style="font-weight: bold;">
                                            :
                                        </td>
                                        <td style="font-weight: bold; width: 27%">
                                            <cc1:ucDropdownList ID="ddlDeliveryType" runat="server" Width="150px" OnSelectedIndexChanged="ddlDeliveryType_SelectedIndexChanged"
                                                AutoPostBack="true">
                                            </cc1:ucDropdownList>
                                            <a href="javascript:void(0)" rel="ssn123" tipwidth="700" onmouseover="ddrivetip('ssn123','#ededed','700');"
                                                onmouseout="hideddrivetip();">
                                                <img src="../../../Images/info_button1.gif" align="absMiddle" border="0"></a>
                                        </td>
                                        <td style="font-weight: bold;">
                                            <cc1:ucLabel ID="lblSchedulingdate" runat="server" Text="Scheduling date" isRequired="true"></cc1:ucLabel>
                                        </td>
                                        <td style="font-weight: bold;">
                                            :
                                        </td>
                                        <td style="font-weight: bold;">
                                            <span id="spSCDate" runat="server">
                                                <cc2:ucDate ID="txtSchedulingdate" runat="server" AutoPostBack="true" />
                                            </span>
                                            <cc1:ucLiteral ID="ltSCdate" runat="server" Visible="false"></cc1:ucLiteral>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: bold; width: 10%;">
                                            <cc1:ucLabel ID="lblCarrier" runat="server" Text="Carrier" isRequired="true"></cc1:ucLabel>
                                        </td>
                                        <td style="font-weight: bold; width: 3%;">
                                            :
                                        </td>
                                        <td style="font-weight: bold; width: 33%;">
                                            <cc1:ucDropdownList ID="ddlCarrier" runat="server" Width="150px" OnSelectedIndexChanged="ddlCarrier_SelectedIndexChanged"
                                                AutoPostBack="true">
                                            </cc1:ucDropdownList>
                                            <a href="javascript:void(0)" rel="ssn456" tipwidth="700" onmouseover="ddrivetip('ssn456','#ededed','700');"
                                                onmouseout="hideddrivetip();">
                                                <img src="../../../Images/info_button1.gif" align="absMiddle" border="0"></a>
                                            &nbsp;&nbsp;
                                            <cc1:ucTextbox ID="txtCarrierOther" runat="server" Width="100px" MaxLength="50" Style="display: none;"></cc1:ucTextbox>
                                            <cc1:ucLabel ID="lblCarrierAdvice" runat="server" Text="Please advise of carrier"
                                                Style="display: none;"></cc1:ucLabel>
                                        </td>
                                        <td style="font-weight: bold;" width="15%">
                                            <cc1:ucLabel ID="lblBookingComments" runat="server" Text="Additional Info" isRequired="true"></cc1:ucLabel>
                                        </td>
                                        <td style="font-weight: bold; width: 3%">
                                            :
                                        </td>
                                        <td style="font-weight: bold; width: 36%">
                                            <cc1:ucTextbox ID="txtBookingComment" runat="server" Width="250px" MaxLength="35"></cc1:ucTextbox>
                                            <cc1:ucLabel ID="lblBookingCommentAdvise" runat="server" Text="Please advise of Additional Info"
                                                Style="display: none;"></cc1:ucLabel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: bold;">
                                            <cc1:ucLabel ID="lblVehicleType" runat="server" Text="Vehicle Type" isRequired="true"></cc1:ucLabel>
                                        </td>
                                        <td style="font-weight: bold;">
                                            :
                                        </td>
                                        <td style="font-weight: bold;">
                                            <cc1:ucDropdownList ID="ddlVehicleType" runat="server" Width="150px" OnSelectedIndexChanged="ddlVehicleType_SelectedIndexChanged"
                                                AutoPostBack="true">
                                            </cc1:ucDropdownList>
                                            <a href="javascript:void(0)" rel="ssn789" tipwidth="700" onmouseover="ddrivetip('ssn789','#ededed','700');"
                                                onmouseout="hideddrivetip();">
                                                <img src="../../../Images/info_button1.gif" align="absMiddle" border="0"></a>
                                            &nbsp;&nbsp;
                                            <cc1:ucTextbox ID="txtVehicleOther" runat="server" Width="100px" MaxLength="50" Style="display: none;"></cc1:ucTextbox>
                                        </td>
                                        <td colspan="3" align="left">
                                            <cc1:ucLabel ID="lblVehicleAdvice" runat="server" Text="Please advise of vehicle type"
                                                Style="display: none;"></cc1:ucLabel>
                                        </td>
                                    </tr>
                                </table>
                            </cc1:ucPanel>--%>
                            <asp:ValidationSummary ID="vSummary" runat="server" ShowMessageBox="true" ShowSummary="false"
                                Style="color: Red" ValidationGroup="AddPurchaseOrder" />
                            <asp:RequiredFieldValidator ID="rfvDeliveryTypeRequired" runat="server" ControlToValidate="ddlDeliveryType"
                                Display="None" ValidationGroup="AddPurchaseOrder" SetFocusOnError="true" InitialValue="0">
                            </asp:RequiredFieldValidator>
                            <%--<asp:RequiredFieldValidator ID="rfvCarrierRequired" runat="server" ControlToValidate="ddlCarrier"
                                Display="None" ValidationGroup="DeliveryDetail" SetFocusOnError="true" InitialValue="0">
                            </asp:RequiredFieldValidator>
                            <asp:RequiredFieldValidator ID="rfvVehicleTypeRequired" runat="server" ControlToValidate="ddlVehicleType"
                                Display="None" ValidationGroup="DeliveryDetail" SetFocusOnError="true" InitialValue="0">
                            </asp:RequiredFieldValidator>
                            <asp:RequiredFieldValidator ID="rfvSchedulingdateRequired" runat="server" ControlToValidate="txtSchedulingdate$txtUCDate"
                                Display="None" ValidationGroup="DeliveryDetail" SetFocusOnError="true">
                            </asp:RequiredFieldValidator>
                            <asp:RequiredFieldValidator ID="rfvCarrierOtherRequired" runat="server" ErrorMessage="xxx"
                                ControlToValidate="txtCarrierOther" Display="None" ValidationGroup="DeliveryDetail"
                                SetFocusOnError="true" Enabled="false">
                            </asp:RequiredFieldValidator>
                            <asp:RequiredFieldValidator ID="rfvVehicleTypeOtherRequired" runat="server" ErrorMessage="xxx"
                                ControlToValidate="txtVehicleOther" Display="None" ValidationGroup="DeliveryDetail"
                                SetFocusOnError="true" Enabled="false">
                            </asp:RequiredFieldValidator>--%>
                            <cc1:ucPanel ID="pnlAddPurchaseOrder" runat="server" GroupingText="Add Purchase Order"
                                CssClass="fieldset-form">
                                <table width="100%" cellspacing="5" cellpadding="0" border="0" class="top-settingsNoBorder"
                                    onmouseover="setToolTip();">
                                    <tr>
                                        <td style="font-weight: bold; width: 15%;">
                                            <cc1:ucLabel ID="lblDeliveryType" runat="server" isRequired="true" ValidationGroup="AddPurchaseOrder"></cc1:ucLabel>
                                        </td>
                                        <td style="font-weight: bold; width: 3%;">
                                            :
                                        </td>
                                        <td style="font-weight: bold; width: 20%;">
                                            <%--<cc1:ucLabel ID="lblDeliveryTypeT" runat="server"></cc1:ucLabel>--%>
                                            <cc1:ucDropdownList ID="ddlDeliveryType" runat="server" Width="150px" OnSelectedIndexChanged="ddlDeliveryType_SelectedIndexChanged"
                                                AutoPostBack="true">
                                            </cc1:ucDropdownList>
                                        </td>
                                        <td style="font-weight: bold;" width="10%">
                                            <cc1:ucLabel ID="lblSite" runat="server"></cc1:ucLabel>
                                        </td>
                                        <td style="font-weight: bold; width: 3%">
                                            :
                                        </td>
                                        <td style="font-weight: bold; width: 49%">
                                            <cc1:ucLabel ID="lblSiteT" runat="server"></cc1:ucLabel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: bold; width: 15%;">
                                            <cc1:ucLabel ID="lblPurchaseOrderNo" runat="server"></cc1:ucLabel>
                                        </td>
                                        <td style="font-weight: bold; width: 3%;">
                                            :
                                        </td>
                                        <td style="font-weight: bold; width: 20%;">
                                            <cc1:ucTextbox ID="txtPurchaseOrderT" runat="server" Width="120px"></cc1:ucTextbox>
                                        </td>
                                        <td style="font-weight: bold; width: 62%;" colspan="3">
                                            <table>
                                                <tr>
                                                    <td align="left">
                                                        <cc1:ucButton ID="btnAddPO" runat="server" Text="Add PO" CssClass="button" Width="100px"
                                                            OnClick="btnAddPO_Click" />
                                                        <%--OnClick="btnAddPO_Click" OnClientClick="return chkVendorID();" />--%>
                                                    </td>
                                                    <td align="left">
                                                        &nbsp;&nbsp;
                                                    </td>
                                                    <td align="left">
                                                        <cc1:ucButton ID="btnSearchforPOs" runat="server" Text="Show PO" CssClass="button" />
                                                        <%--OnClick="btnShowPOPopup_Click" />--%>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="6" style="width: 100%">
                                            <cc1:ucLabel ID="lblAllPORequired" runat="server"></cc1:ucLabel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: bold; width: 15%;">
                                            <cc1:ucLabel ID="lblVendor" runat="server"></cc1:ucLabel>
                                        </td>
                                        <td style="font-weight: bold; width: 3%;">
                                            :
                                        </td>
                                        <td style="font-weight: bold; width: 20%;">
                                            <cc1:ucLabel ID="lblVendorT" runat="server"></cc1:ucLabel>
                                        </td>
                                        <td style="font-weight: bold;" width="10%">
                                        </td>
                                        <td style="font-weight: bold; width: 3%">
                                        </td>
                                        <td style="font-weight: bold; width: 49%">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: bold; width: 15%;">
                                            <cc1:ucLabel ID="lblDeliveryDate" runat="server"></cc1:ucLabel>
                                        </td>
                                        <td style="font-weight: bold; width: 3%;">
                                            :
                                        </td>
                                        <td style="font-weight: bold; width: 20%;">
                                            <%--<cc1:ucLabel ID="lblDeliveryDateT" runat="server"></cc1:ucLabel>--%>
                                            <span id="spSCDate" runat="server">
                                            <cc2:ucDate ID="txtSchedulingdate" runat="server" AutoPostBack="true" /></span>
                                            <cc1:ucLiteral ID="ltSCdate" runat="server" Visible="false"></cc1:ucLiteral>
                                        </td>
                                        <td style="font-weight: bold;" width="10%">
                                        </td>
                                        <td style="font-weight: bold; width: 3%">
                                        </td>
                                        <td style="font-weight: bold; width: 49%">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="6" style="width: 100%">
                                            <table width="100%" cellspacing="5" cellpadding="0" border="0" class="form-table">
                                                <tr>
                                                    <td>
                                                        <cc1:ucGridView ID="gvPO" runat="server" AutoGenerateColumns="false" CssClass="grid"
                                                            CellPadding="0" Width="100%" DataKeyNames="AutoID" AllowSorting="true">
                                                            <%--OnRowDataBound="gvPO_RowDataBound" OnRowDeleting="gvPO_RowDeleting" OnSorting="gvPO_Sorting"--%>
                                                            <AlternatingRowStyle BackColor="White" ForeColor="#284775"></AlternatingRowStyle>
                                                            <Columns>
                                                                <asp:BoundField HeaderText="PO" DataField="PurchaseNumber" SortExpression="PurchaseNumber">
                                                                    <HeaderStyle Width="10%" />
                                                                    <ItemStyle HorizontalAlign="Center" />
                                                                </asp:BoundField>
                                                                <%-- <asp:TemplateField HeaderText="PO" SortExpression="PurchaseNumber">
                                                        <HeaderStyle Width="10%" />
                                                       <ItemStyle HorizontalAlign="Center" />
                                                        <ItemTemplate>   
                                                            <asp:Literal ID="ltgridPurchacse" runat="server" Text='<%#Eval("PurchaseNumber")%>'></asp:Literal>
                                                            <asp:HiddenField ID="hdnPOID" runat="server" Value='<%#Eval("PurchaseOrderID")%>' />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>--%>
                                                                <%--<asp:BoundField HeaderText="Original Due Date" DataField="Original_due_date" SortExpression="Original_due_date">
                                                        <HeaderStyle Width="10%" />
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:BoundField>--%>
                                                                <asp:BoundField HeaderText="Expected Date" DataField="ExpectedDate" SortExpression="ExpectedDate">
                                                                    <HeaderStyle Width="10%" />
                                                                    <ItemStyle HorizontalAlign="Center" />
                                                                </asp:BoundField>
                                                                <asp:BoundField HeaderText="Vendor Name" DataField="VendorName" SortExpression="VendorName">
                                                                    <HeaderStyle Width="20%" />
                                                                    <ItemStyle HorizontalAlign="Center" />
                                                                </asp:BoundField>
                                                                <asp:BoundField HeaderText="# Outstanding Lines on PO" DataField="OutstandingLines"
                                                                    SortExpression="OutstandingLines">
                                                                    <HeaderStyle Width="10%" />
                                                                    <ItemStyle HorizontalAlign="Center" />
                                                                </asp:BoundField>
                                                                <asp:BoundField HeaderText="# Stock outs" DataField="Qty_On_Hand" SortExpression="Qty_On_Hand">
                                                                    <HeaderStyle Width="10%" />
                                                                    <ItemStyle HorizontalAlign="Center" />
                                                                </asp:BoundField>
                                                                <asp:BoundField HeaderText="# Backorders" DataField="qty_on_backorder" SortExpression="qty_on_backorder">
                                                                    <HeaderStyle Width="10%" />
                                                                    <ItemStyle HorizontalAlign="Center" />
                                                                </asp:BoundField>
                                                                <asp:BoundField HeaderText="Destination" DataField="SiteName" SortExpression="SiteName">
                                                                    <HeaderStyle Width="20%" />
                                                                    <ItemStyle HorizontalAlign="Center" />
                                                                </asp:BoundField>
                                                                <asp:CommandField HeaderText="Remove" CausesValidation="false" ButtonType="Link"
                                                                    ShowDeleteButton="True" DeleteText="Remove" ItemStyle-HorizontalAlign="Center">
                                                                    <HeaderStyle Width="10%" />
                                                                    <ItemStyle HorizontalAlign="Center" />
                                                                </asp:CommandField>
                                                            </Columns>
                                                        </cc1:ucGridView>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </cc1:ucPanel>
                            <cc1:ucPanel ID="pnlDeliveryVolume" runat="server" GroupingText="Delivery/Volume"
                                CssClass="fieldset-form">
                                <table width="100%" cellspacing="5" cellpadding="0" border="0" class="top-settingsNoBorder"
                                    onmouseover="setToolTip();">
                                    <tr>
                                        <td style="font-weight: bold; width: 15%;">
                                            <cc1:ucLabel ID="lblCarrier" runat="server"></cc1:ucLabel>
                                        </td>
                                        <td style="font-weight: bold; width: 3%;">
                                            :
                                        </td>
                                        <td style="font-weight: bold; width: 20%;">
                                            <cc1:ucDropdownList ID="ddlCarrier" runat="server" Width="150px" OnSelectedIndexChanged="ddlCarrier_SelectedIndexChanged"
                                                AutoPostBack="true">
                                            </cc1:ucDropdownList>
                                        </td>
                                        <td style="font-weight: bold;" width="10%">
                                            <cc1:ucLabel ID="lblVehicleType" runat="server"></cc1:ucLabel>
                                        </td>
                                        <td style="font-weight: bold; width: 3%">
                                            :
                                        </td>
                                        <td style="font-weight: bold; width: 49%">
                                            <cc1:ucDropdownList ID="ddlVehicleType" runat="server" Width="150px" OnSelectedIndexChanged="ddlVehicleType_SelectedIndexChanged"
                                                AutoPostBack="true">
                                            </cc1:ucDropdownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: bold; width: 15%;">
                                            <cc1:ucLabel ID="lblActualPallets" runat="server"></cc1:ucLabel>
                                        </td>
                                        <td style="font-weight: bold; width: 3%;">
                                            :
                                        </td>
                                        <td style="font-weight: bold; width: 20%;">
                                            <cc1:ucTextbox ID="txtPalletsT" runat="server" Width="120px"></cc1:ucTextbox>
                                        </td>
                                        <td style="font-weight: bold;" width="10%">
                                            <cc1:ucLabel ID="lblActualCartons" runat="server"></cc1:ucLabel>
                                        </td>
                                        <td style="font-weight: bold; width: 3%">
                                            :
                                        </td>
                                        <td style="font-weight: bold; width: 49%">
                                            <cc1:ucTextbox ID="txtCartonsT" runat="server" Width="120px"></cc1:ucTextbox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: bold; width: 15%;">
                                            <cc1:ucLabel ID="lblActualLifts" runat="server"></cc1:ucLabel>
                                        </td>
                                        <td style="font-weight: bold; width: 3%;">
                                            :
                                        </td>
                                        <td style="font-weight: bold; width: 20%;">
                                            <cc1:ucTextbox ID="txtLiftsT" runat="server" Width="120px"></cc1:ucTextbox>
                                        </td>
                                        <td style="font-weight: bold;" width="10%">
                                        </td>
                                        <td style="font-weight: bold; width: 3%">
                                        </td>
                                        <td style="font-weight: bold; width: 49%">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: bold; width: 100%;" colspan="6">
                                            <cc1:ucLabel ID="lblWeExpectXamount" runat="server"></cc1:ucLabel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: bold; width: 38%;" colspan="3">
                                            <cc1:ucLabel ID="lblPleaseConfirmTheNumberOfLines" runat="server"></cc1:ucLabel>
                                        </td>
                                        <td style="font-weight: bold; width: 62%;" colspan="3">
                                            <cc1:ucTextbox ID="txtLinesT" runat="server" Width="120px"></cc1:ucTextbox>
                                        </td>
                                    </tr>
                                </table>
                            </cc1:ucPanel>
                            <table width="100%" cellspacing="5" cellpadding="0" border="0" class="top-settingsNoBorder"
                                onmouseover="setToolTip();">
                                <tr>
                                    <td style="font-weight: bold; width: 100%;" colspan="6">
                                        <cc1:ucLabel ID="lblPOMessage" runat="server"></cc1:ucLabel>
                                    </td>
                                </tr>
                            </table>
                            <cc1:ucView ID="vwBooking" runat="server">
                                <table width="100%" cellspacing="5" cellpadding="0" border="0">
                                    <tr style="display: none;">
                                        <td style="font-weight: bold; width: 100%" align="left" colspan="2">
                                            Detailed below is an overview of your booking details for this particualr day. If
                                            this booking is being added and sent on the same vehicle as one of your exsiting
                                            scheduled bookings, please add it to the vechile by clicking onto Add to Booking.Please
                                            note that each vehicle requires it's own booking.
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: bold; width: 75%" align="left">
                                            <cc1:ucLabel ID="lblProposeBookingInfo" runat="server" Text="Based on information entered, we propose a booking with an arrival time  of -"></cc1:ucLabel>
                                            &nbsp;
                                            <cc1:ucLiteral ID="ltBookingDate" runat="server"></cc1:ucLiteral>&nbsp;
                                            <cc1:ucLiteral ID="ltSuggestedSlotTime" runat="server" Text=""></cc1:ucLiteral>
                                        </td>
                                        <td style="font-weight: bold; width: 25%" align="left">
                                            <cc1:ucButton ID="btnConfirmBooking" runat="server" Text="Confirm Booking" CssClass="button"
                                                OnClick="btnConfirmBooking_Click" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" align="left">
                                            <b>
                                                <cc1:ucLabel ID="lblyourCurrentschedule" runat="server" Text="Your Current Schedule"></cc1:ucLabel></b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" align="left">
                                            <table width="100%" cellspacing="5" cellpadding="0" border="0">
                                                <tr>
                                                    <td style="background-color: GreenYellow; border: 1px solid black; width: 13%">
                                                        &nbsp;
                                                    </td>
                                                    <td style="width: 20%">
                                                        <cc1:ucLabel ID="lblCurrentBooking" runat="server" Text="Current Booking"></cc1:ucLabel>
                                                    </td>
                                                    <td style="background-color: Purple; border: 1px solid black; width: 13%">
                                                        &nbsp;
                                                    </td>
                                                    <td style="width: 20%">
                                                        <cc1:ucLabel ID="lblConfirmedBooking" runat="server" Text="Confirmed Booking"></cc1:ucLabel>
                                                    </td>
                                                    <td style="border: 1px solid black; width: 13%">
                                                        &nbsp;
                                                    </td>
                                                    <td style="width: 20%">
                                                        <cc1:ucLabel ID="lblUnconfirmedFixedSlot" runat="server" Text="Unconfirmed Fixed Slot"></cc1:ucLabel>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <cc1:ucGridView ID="gvBookingSlots" runat="server" AutoGenerateColumns="false" CssClass="grid"
                                                Width="100%" OnRowDataBound="gvBookingSlots_RowDataBound">
                                                <AlternatingRowStyle BackColor="White" ForeColor="#284775"></AlternatingRowStyle>
                                                <Columns>
                                                    <asp:BoundField HeaderText="Day" DataField="BookingDay" SortExpression="BookingDay">
                                                        <HeaderStyle Width="15%" HorizontalAlign="Left" />
                                                        <ItemStyle HorizontalAlign="Left" />
                                                    </asp:BoundField>
                                                    <asp:BoundField HeaderText="Date" DataField="BookingDate" SortExpression="BookingDate">
                                                        <HeaderStyle Width="15%" HorizontalAlign="Left" />
                                                        <ItemStyle HorizontalAlign="Left" />
                                                    </asp:BoundField>
                                                    <asp:TemplateField HeaderText="Time" SortExpression="SlotTime.SlotTime">
                                                        <HeaderStyle Width="10%" HorizontalAlign="Left" />
                                                        <ItemStyle HorizontalAlign="Left" />
                                                        <ItemTemplate>
                                                            <cc1:ucLiteral ID="ltTime" runat="server" Text='<%#Eval("SlotTime.SlotTime") %>'></cc1:ucLiteral>
                                                            <asp:HiddenField ID="hdnAvailableFixedSlotID" runat="server" Value='<%#Eval("FixedSlot.FixedSlotID") %>' />
                                                            <asp:HiddenField ID="hdnAvailableSlotTimeID" runat="server" Value='<%#Eval("SlotTime.SlotTimeID") %>' />
                                                            <asp:HiddenField ID="hdnAvailableSiteDoorNumberID" runat="server" Value='<%#Eval("DoorNoSetup.SiteDoorNumberID") %>' />
                                                            <asp:HiddenField ID="hdnAvailableSiteDoorNumber" runat="server" Value='<%#Eval("DoorNoSetup.DoorNumber") %>' />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField HeaderText="Booking Ref" DataField="BookingRef" SortExpression="BookingRef">
                                                        <HeaderStyle Width="15%" HorizontalAlign="Left" />
                                                        <ItemStyle HorizontalAlign="Left" />
                                                    </asp:BoundField>
                                                    <asp:TemplateField HeaderText="Door Name" SortExpression="DoorNoSetup.DoorNumber">
                                                        <HeaderStyle Width="15%" HorizontalAlign="Left" />
                                                        <ItemStyle HorizontalAlign="Left" />
                                                        <ItemTemplate>
                                                            <cc1:ucLiteral ID="ltDoorNumber" runat="server" Text='<%#Eval("DoorNoSetup.DoorNumber") %>'></cc1:ucLiteral>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField HeaderText="Slot Type" DataField="SlotType" SortExpression="SlotType">
                                                        <HeaderStyle Width="15%" HorizontalAlign="Left" />
                                                        <ItemStyle HorizontalAlign="Left" />
                                                    </asp:BoundField>
                                                    <asp:BoundField HeaderText="Status" DataField="Status" SortExpression="Status">
                                                        <HeaderStyle Width="15%" HorizontalAlign="Left" />
                                                        <ItemStyle HorizontalAlign="Left" />
                                                    </asp:BoundField>
                                                </Columns>
                                            </cc1:ucGridView>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: bold;" colspan="2" align="right">
                                            <cc1:ucButton ID="btnMakeNonTimeBooking" runat="server" Text="Make Booking a Non Time Booking"
                                                CssClass="button" OnClick="btnMakeNonTimeBooking_Click" />
                                            &nbsp;<cc1:ucButton ID="btnAlternateSlot" runat="server" CssClass="button" OnClick="btnAlternateSlot_Click"
                                                Text="Check for an Alternative Slot Time" />
                                            &nbsp;
                                            <cc1:ucButton ID="btnAlternateSlotVendor" runat="server" CssClass="button" OnClick="btnAlternateSlotVendor_Click"
                                                Text="Check Alternative Slot Time for Vendor" />
                                        </td>
                                    </tr>
                                </table>
                            </cc1:ucView>
                            <cc1:ucView ID="vwAlternateSlotOD" runat="server">
                                <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                    <ContentTemplate>
                                        <table cellpadding="5" cellspacing="2" width="100%">
                                            <tr>
                                                <td width="55%" valign="top">
                                                    <cc1:ucPanel ID="pnlBookingInfo" runat="server" GroupingText="Current Booking Information"
                                                        CssClass="fieldset-form">
                                                        <table width="100%" cellspacing="5" cellpadding="0" border="0" class="form-table"
                                                            height="130px">
                                                            <tr>
                                                                <td width="25%">
                                                                    <cc1:ucLabel ID="lblVendorNo_2" runat="server" Text="Vendor No"></cc1:ucLabel>
                                                                </td>
                                                                <td width="2%">
                                                                    :
                                                                </td>
                                                                <td width="23%" class="nobold">
                                                                    <cc1:ucLiteral ID="ltVendorNo_2" runat="server"></cc1:ucLiteral>
                                                                </td>
                                                                <td width="25%">
                                                                    <cc1:ucLabel ID="lblCarrier_2" runat="server" Text="Carrier"></cc1:ucLabel>
                                                                </td>
                                                                <td width="2%">
                                                                    :
                                                                </td>
                                                                <td width="23%" class="nobold">
                                                                    <cc1:ucLiteral ID="ltCarrier_2" runat="server"></cc1:ucLiteral>
                                                                    &nbsp;&nbsp;
                                                                    <cc1:ucLiteral ID="ltOtherCarrier_2" runat="server" Visible="false"></cc1:ucLiteral>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <cc1:ucLabel ID="lblVehicleType_2" runat="server" Text="Vehicle Type"></cc1:ucLabel>
                                                                </td>
                                                                <td>
                                                                    :
                                                                </td>
                                                                <td class="nobold">
                                                                    <cc1:ucLiteral ID="ltVehicleType_2" runat="server"></cc1:ucLiteral>
                                                                    &nbsp;&nbsp;
                                                                    <cc1:ucLiteral ID="ltOtherVehicleType_2" runat="server" Visible="false"></cc1:ucLiteral>
                                                                </td>
                                                                <td>
                                                                    <cc1:ucLabel ID="lblSchedulingdate_2" runat="server" Text="Scheduling date"></cc1:ucLabel>
                                                                </td>
                                                                <td>
                                                                    :
                                                                </td>
                                                                <td class="nobold">
                                                                    <cc1:ucLiteral ID="ltSchedulingDate_2" runat="server"></cc1:ucLiteral>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <cc1:ucLabel ID="lblEstEnd_2" runat="server" Text="Est End"></cc1:ucLabel>
                                                                </td>
                                                                <td>
                                                                    :
                                                                </td>
                                                                <td class="nobold">
                                                                    <cc1:ucLiteral ID="ltlEstEnd_2" runat="server"></cc1:ucLiteral>
                                                                </td>
                                                                <td colspan="3">
                                                                    &nbsp;
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <cc1:ucLabel ID="lblNumberofLifts" runat="server" Text="Number of Lifts"></cc1:ucLabel>
                                                                </td>
                                                                <td>
                                                                    :
                                                                </td>
                                                                <td class="nobold">
                                                                    <cc1:ucLiteral ID="ltNumberofLifts" runat="server"></cc1:ucLiteral>
                                                                </td>
                                                                <td>
                                                                    <cc1:ucLabel ID="lblNumberofPallet" runat="server" Text="Number of Pallet"></cc1:ucLabel>
                                                                </td>
                                                                <td>
                                                                    :
                                                                </td>
                                                                <td class="nobold">
                                                                    <cc1:ucLiteral ID="ltNumberofPallets" runat="server"></cc1:ucLiteral>
                                                                </td>
                                                                <td colspan="3">
                                                                    &nbsp;
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <cc1:ucLabel ID="lblNumberofLines" runat="server" Text="Number of Lines"></cc1:ucLabel>
                                                                </td>
                                                                <td>
                                                                    :
                                                                </td>
                                                                <td class="nobold">
                                                                    <cc1:ucLabel ID="ltNumberofLines" runat="server"></cc1:ucLabel>
                                                                </td>
                                                                <td>
                                                                    <cc1:ucLabel ID="lblNumberofCartons" runat="server" Text="Number of Cartons"></cc1:ucLabel>
                                                                </td>
                                                                <td>
                                                                    :
                                                                </td>
                                                                <td class="nobold">
                                                                    <cc1:ucLabel ID="ltNumberofCartons" runat="server"></cc1:ucLabel>
                                                                </td>
                                                                <td colspan="3">
                                                                    &nbsp;
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </cc1:ucPanel>
                                                </td>
                                                <td width="45%" valign="top">
                                                    <cc1:ucPanel ID="pnlRunningTotal" runat="server" GroupingText="Current Running Total"
                                                        CssClass="fieldset-form">
                                                        <table width="100%" cellspacing="5" cellpadding="0" border="0" class="form-table"
                                                            height="130Px">
                                                            <tr>
                                                                <th width="40%">
                                                                </th>
                                                                <th width="30%">
                                                                    <cc1:ucLabel ID="lblDailyMaximums" runat="server" Text="Daily Maximums"></cc1:ucLabel>
                                                                </th>
                                                                <th width="30%">
                                                                    <cc1:ucLabel ID="lblRemainingCapacity" runat="server" Text="Remaining Capacity"></cc1:ucLabel>
                                                                </th>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <cc1:ucLabel ID="lblMaximumLifts" runat="server" Text="Maximum Lifts"></cc1:ucLabel>
                                                                </td>
                                                                <td align="center">
                                                                    <cc1:ucLiteral ID="ltMaximumLifts" runat="server"></cc1:ucLiteral>
                                                                </td>
                                                                <td align="center">
                                                                    <cc1:ucLiteral ID="ltRemainingLifts" runat="server"></cc1:ucLiteral>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <cc1:ucLabel ID="lblMaximumPallets" runat="server" Text="Maximum Pallets"></cc1:ucLabel>
                                                                </td>
                                                                <td align="center">
                                                                    <cc1:ucLiteral ID="ltMaximumPallets" runat="server"></cc1:ucLiteral>
                                                                </td>
                                                                <td align="center">
                                                                    <cc1:ucLiteral ID="ltRemainingPallets" runat="server"></cc1:ucLiteral>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <cc1:ucLabel ID="lblMaximumLines" runat="server" Text="Maximum Lines"></cc1:ucLabel>
                                                                </td>
                                                                <td align="center">
                                                                    <cc1:ucLiteral ID="ltMaximumLines" runat="server"></cc1:ucLiteral>
                                                                </td>
                                                                <td align="center">
                                                                    <cc1:ucLiteral ID="ltRemainingLines" runat="server"></cc1:ucLiteral>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </cc1:ucPanel>
                                                </td>
                                            </tr>
                                        </table>
                                        <table width="100%" cellspacing="2" cellpadding="0" border="0">
                                            <tr>
                                                <td width="40%" valign="top">
                                                    <cc1:ucPanel ID="pnlNonTimeBooking" runat="server" GroupingText="Non Time Booking"
                                                        CssClass="fieldset-form">
                                                        <cc1:ucGridView ID="gvNonTimeBooking" runat="server" AutoGenerateColumns="false"
                                                            CssClass="grid" CellPadding="0" Width="100%">
                                                            <AlternatingRowStyle BackColor="White" ForeColor="#284775"></AlternatingRowStyle>
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="Vendor/Carrier">
                                                                    <ItemStyle HorizontalAlign="Left" Width="40%" />
                                                                    <HeaderStyle HorizontalAlign="Left" />
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="vendorname" runat="server" Text='<%#Eval("Vendor.VendorName") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:BoundField HeaderText="Lifts" DataField="NumberOfLift" HeaderStyle-Width="20%"
                                                                    HeaderStyle-HorizontalAlign="Left" />
                                                                <asp:BoundField HeaderText="Pallets" DataField="NumberOfPallet" HeaderStyle-Width="20%"
                                                                    HeaderStyle-HorizontalAlign="Left" />
                                                                <asp:BoundField HeaderText="Cartons" DataField="NumberOfCartons" HeaderStyle-Width="20%"
                                                                    HeaderStyle-HorizontalAlign="Left" />
                                                                <asp:BoundField HeaderText="Lines" DataField="NumberOfLines" HeaderStyle-Width="20%"
                                                                    HeaderStyle-HorizontalAlign="Left" />
                                                            </Columns>
                                                        </cc1:ucGridView>
                                                    </cc1:ucPanel>
                                                </td>
                                                <td width="60%" valign="top">
                                                    <cc1:ucPanel ID="pnlLegend" runat="server" GroupingText="Legend" CssClass="fieldset-form">
                                                        <table width="100%" cellspacing="2" cellpadding="0" border="0" class="form-table">
                                                            <tr height="20Px">
                                                                <td style="width: 21%; text-align: right;">
                                                                    <cc1:ucLabel ID="lblAvailableSlot" runat="server" Text="Available Slot"></cc1:ucLabel>
                                                                </td>
                                                                <td style="background-color: White; border: 1px solid black; width: 13%;">
                                                                    &nbsp;
                                                                </td>
                                                                <td style="width: 18%; text-align: right;">
                                                                    <cc1:ucLabel ID="lblBookedSlot" runat="server" Text="Booked Slot"></cc1:ucLabel>
                                                                </td>
                                                                <td style="background-color: Purple; border: 2px dashed black; width: 13%;">
                                                                    &nbsp;
                                                                </td>
                                                                <td style="width: 22%; text-align: right;">
                                                                    <cc1:ucLabel ID="lblInsufficentSlotTime" runat="server" Text="Insufficent Slot Time"></cc1:ucLabel>
                                                                </td>
                                                                <td style="background-color: Gray; border: 1px solid black; width: 13%;">
                                                                    &nbsp;
                                                                </td>
                                                            </tr>
                                                            <tr height="20Px">
                                                                <td style="text-align: right;">
                                                                    <cc1:ucLabel ID="lblRecommendedSlot" runat="server" Text="Recommended Slot"></cc1:ucLabel>
                                                                </td>
                                                                <td style="background-color: GreenYellow; border: 1px solid black;">
                                                                    &nbsp;
                                                                </td>
                                                                <td style="text-align: right;">
                                                                    <cc1:ucLabel ID="lblFixedSlot" runat="server" Text="Fixed Slot"></cc1:ucLabel>
                                                                </td>
                                                                <td style="background-color: LightYellow; border: 2px solid red;">
                                                                    &nbsp;
                                                                </td>
                                                                <td style="text-align: right;">
                                                                    <cc1:ucLabel ID="lblDoorNotAvailable" runat="server" Text="Door Not Available"></cc1:ucLabel>
                                                                </td>
                                                                <td style="background-color: Black; border: 1px solid black;">
                                                                    &nbsp;
                                                                </td>
                                                            </tr>
                                                            <tr height="20Px">
                                                                <td style="text-align: right;">
                                                                    <cc1:ucLabel ID="lblSlotOverspill" runat="server" Text="Slot Overspill"></cc1:ucLabel>
                                                                </td>
                                                                <td style="background-color: Orange; border: 1px solid black;">
                                                                    &nbsp;
                                                                </td>
                                                                <td colspan="4">
                                                                    &nbsp;
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </cc1:ucPanel>
                                                </td>
                                            </tr>
                                        </table>
                                        <cc1:ucPanel ID="pnlSystemRecommendedSlotDetails" runat="server" GroupingText="System Recommended Slot Details"
                                            CssClass="fieldset-form">
                                            <table width="100%" cellspacing="5" cellpadding="0" border="0" class="form-table">
                                                <tr>
                                                    <td width="18%" style="font-weight: bold;">
                                                        <cc1:ucLabel ID="lblSelectedTimeSlot" runat="server" Text="Selected Time Slot"></cc1:ucLabel>
                                                    </td>
                                                    <td width="1%" style="font-weight: bold;">
                                                        :
                                                    </td>
                                                    <td width="20%" class="nobold">
                                                        <cc1:ucLabel ID="ltTimeSlot" runat="server"></cc1:ucLabel>
                                                    </td>
                                                    <td width="18%" style="font-weight: bold;">
                                                        <cc1:ucLabel ID="lblSelectedDoorName" runat="server"></cc1:ucLabel>
                                                    </td>
                                                    <td width="1%" style="font-weight: bold;">
                                                        :
                                                    </td>
                                                    <td width="20%" class="nobold">
                                                        <cc1:ucLabel ID="ltDoorNo" runat="server"></cc1:ucLabel>
                                                    </td>
                                                    <td width="22%">
                                                        <cc1:ucButton ID="btnConfirmAlternateSlot" runat="server" Text="Confirm Booking"
                                                            CssClass="button" Width="160px" OnClick="btnConfirmAlternateSlot_Click" />
                                                        <asp:Button ID="hdnbutton" runat="server" OnClick="hdnbutton_Click" Style="display: none;" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <cc1:ucLabel ID="lblTimeSuggested_2" runat="server" Text="Time Suggested"></cc1:ucLabel>
                                                    </td>
                                                    <td>
                                                        :
                                                    </td>
                                                    <td class="nobold">
                                                        <cc1:ucLiteral ID="ltTimeSuggested_2" runat="server"></cc1:ucLiteral>
                                                    </td>
                                                    <td colspan="4">
                                                        &nbsp;
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="7" align="left">
                                                        <cc1:ucLabel ID="lblChangeBookingTime" runat="server" Text="To change the time of this booking, click on the slot required in the grid below."></cc1:ucLabel>
                                                    </td>
                                                </tr>
                                            </table>
                                        </cc1:ucPanel>
                                        <br />
                                        <asp:Panel ID="tableDiv_General" runat="server" CssClass="tableDiv" onscroll="getScroll1(this);">
                                        </asp:Panel>
                                        <%-- <asp:Panel ID="pnlHeaderWindow" runat="server" Style=" height: 60px; overflow:hidden;"
                                onmouseover="setToolTip();" ScrollBars="None">
                            </asp:Panel>

                            <asp:Panel ID="pnlTimeWindow" runat="server" Style="width:100%; height: 335px; overflow: auto;"
                                onmouseover="setToolTip();" onscroll="getScroll1(this);">
                            </asp:Panel>--%>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="btnConfirmAlternateSlot" />
                                        <asp:AsyncPostBackTrigger ControlID="hdnbutton" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </cc1:ucView>
                            <cc1:ucView ID="vwAlternateSlotVendor" runat="server">
                                <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                    <ContentTemplate>
                                        <table width="100%" cellspacing="5" cellpadding="0" border="0">
                                            <tr>
                                                <td width="70%" valign="top">
                                                    <cc1:ucPanel ID="pnlCurrentBookingInformation" runat="server" GroupingText="Current Booking Information"
                                                        CssClass="fieldset-form">
                                                        <table width="100%" cellspacing="5" cellpadding="0" border="0" class="form-table">
                                                            <tr>
                                                                <td width="20%">
                                                                    <cc1:ucLabel ID="UcLabel2" runat="server" Text="Site"></cc1:ucLabel>
                                                                </td>
                                                                <td width="2%">
                                                                    :
                                                                </td>
                                                                <td class="nobold" width="28%">
                                                                    <cc1:ucLiteral ID="ltSite" runat="server"></cc1:ucLiteral>
                                                                </td>
                                                                <td width="20%">
                                                                    <cc1:ucLabel ID="lblVendor_3" runat="server" Text="Vendor"></cc1:ucLabel>
                                                                </td>
                                                                <td width="2%">
                                                                    :
                                                                </td>
                                                                <td width="28%" class="nobold">
                                                                    <cc1:ucLiteral ID="ltVendor_3" runat="server"></cc1:ucLiteral>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <cc1:ucLabel ID="lblCarrier_3" runat="server" Text="Carrier"></cc1:ucLabel>
                                                                </td>
                                                                <td>
                                                                    :
                                                                </td>
                                                                <td class="nobold">
                                                                    <cc1:ucLiteral ID="ltCarrier_3" runat="server"></cc1:ucLiteral>
                                                                    &nbsp;
                                                                    <cc1:ucLiteral ID="ltOtherCarrier_3" runat="server"></cc1:ucLiteral>
                                                                </td>
                                                                <td>
                                                                    <cc1:ucLabel ID="lblVehicle_3" runat="server" Text="Vehicle"></cc1:ucLabel>
                                                                </td>
                                                                <td>
                                                                    :
                                                                </td>
                                                                <td class="nobold">
                                                                    <cc1:ucLiteral ID="ltVehicleType_3" runat="server"></cc1:ucLiteral>
                                                                    &nbsp;
                                                                    <cc1:ucLiteral ID="ltOtherVehicleType_3" runat="server"></cc1:ucLiteral>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <cc1:ucLabel ID="lblNumberofLifts_1" runat="server" Text="Number of Lifts"></cc1:ucLabel>
                                                                </td>
                                                                <td>
                                                                    :
                                                                </td>
                                                                <td class="nobold">
                                                                    <cc1:ucLiteral ID="ltExpectedLifts" runat="server"></cc1:ucLiteral>
                                                                </td>
                                                                <td>
                                                                    <cc1:ucLabel ID="lblNumberofPallet_1" runat="server" Text="Number of Pallet"></cc1:ucLabel>
                                                                </td>
                                                                <td>
                                                                    :
                                                                </td>
                                                                <td class="nobold">
                                                                    <cc1:ucLiteral ID="ltExpectedPallets" runat="server"></cc1:ucLiteral>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <cc1:ucLabel ID="lblNumberofLines_1" runat="server" Text="Number of Lines"></cc1:ucLabel>
                                                                </td>
                                                                <td>
                                                                    :
                                                                </td>
                                                                <td class="nobold">
                                                                    <cc1:ucLiteral ID="ltExpectedLines" runat="server"></cc1:ucLiteral>
                                                                </td>
                                                                <td>
                                                                    <cc1:ucLabel ID="lblNumberofCartons_1" runat="server" Text="Number of Cartons"></cc1:ucLabel>
                                                                </td>
                                                                <td>
                                                                    :
                                                                </td>
                                                                <td class="nobold">
                                                                    <cc1:ucLiteral ID="ltExpectedCartons" runat="server"></cc1:ucLiteral>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <cc1:ucLabel ID="lblSchedulingDate_3" runat="server" Text="Scheduling Date"></cc1:ucLabel>
                                                                </td>
                                                                <td>
                                                                    :
                                                                </td>
                                                                <td class="nobold">
                                                                    <cc1:ucLiteral ID="ltSchedulingDate_3" runat="server"></cc1:ucLiteral>
                                                                </td>
                                                                <td colspan="3">
                                                                    &nbsp;
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </cc1:ucPanel>
                                                </td>
                                                <td width="30%" valign="top">
                                                    <cc1:ucPanel ID="pnlLegend_1" runat="server" GroupingText="Legend" CssClass="fieldset-form">
                                                        <table width="100%" cellspacing="3" cellpadding="0" border="0" class="form-table">
                                                            <tr>
                                                                <td style="width: 50%; text-align: right;">
                                                                    <cc1:ucLabel ID="lblAvailableSlot_1" runat="server" Text="Available Slot"></cc1:ucLabel>
                                                                </td>
                                                                <td style="background-color: White; border: 1px solid black; width: 50%;">
                                                                    &nbsp;
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="text-align: right;">
                                                                    <cc1:ucLabel ID="lblBookedSlot_1" runat="server" Text="Booked Slot"></cc1:ucLabel>
                                                                </td>
                                                                <td style="background-color: Purple; border: 2px dashed black;">
                                                                    &nbsp;
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="text-align: right;">
                                                                    <cc1:ucLabel ID="lblRecommendedSlot_1" runat="server" Text="Recommended Slot"></cc1:ucLabel>
                                                                </td>
                                                                <td style="background-color: GreenYellow; border: 1px solid black;">
                                                                    &nbsp;
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="text-align: right;">
                                                                    <cc1:ucLabel ID="lblFixedSlot_1" runat="server" Text="Fixed Slot"></cc1:ucLabel>
                                                                </td>
                                                                <td style="background-color: LightYellow; border: 2px solid red;">
                                                                    &nbsp;
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="text-align: right;">
                                                                    <cc1:ucLabel ID="lblDoorNotAvailable_1" runat="server" Text="Door Not Available"></cc1:ucLabel>
                                                                </td>
                                                                <td style="background-color: Black; border: 1px solid black;">
                                                                    &nbsp;
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </cc1:ucPanel>
                                                </td>
                                            </tr>
                                        </table>
                                        <cc1:ucPanel ID="pnlSystemRecommendedSlotDetails_1" runat="server" GroupingText="System Recommended Slot Details"
                                            CssClass="fieldset-form">
                                            <table width="100%" cellspacing="5" cellpadding="0" border="0" class="form-table">
                                                <tr>
                                                    <td width="18%" style="font-weight: bold;">
                                                        <cc1:ucLabel ID="lblSelectedTimeSlot_1" runat="server" Text="Selected Time Slot"></cc1:ucLabel>
                                                    </td>
                                                    <td width="1%" style="font-weight: bold;">
                                                        :
                                                    </td>
                                                    <td width="20%" class="nobold">
                                                        <cc1:ucLabel ID="ltTimeSlot_1" runat="server"></cc1:ucLabel>
                                                    </td>
                                                    <td width="18%" style="font-weight: bold;">
                                                        <cc1:ucLabel ID="lblSelectedDoorName_1" runat="server" Text="Selected Door No"></cc1:ucLabel>
                                                    </td>
                                                    <td width="1%" style="font-weight: bold;">
                                                        :
                                                    </td>
                                                    <td width="20%" class="nobold">
                                                        <cc1:ucLabel ID="ltDoorNo_1" runat="server"></cc1:ucLabel>
                                                    </td>
                                                    <td width="22%">
                                                        <cc1:ucButton ID="btnConfirmAlternateSlotVendor" runat="server" Text="Confirm Booking"
                                                            CssClass="button" Width="160px" OnClick="btnConfirmAlternateSlotVendor_Click" />
                                                        <asp:Button ID="hdnbuttonVendor" runat="server" OnClick="hdnbutton_Click" Style="display: none;" />
                                                    </td>
                                                </tr>
                                                <tr style="font-weight: bold;">
                                                    <td>
                                                        <cc1:ucLabel ID="lblTimeSuggested_3" runat="server" Text="Suggested Time"></cc1:ucLabel>
                                                    </td>
                                                    <td style="font-weight: bold;">
                                                        :
                                                    </td>
                                                    <td class="nobold">
                                                        <cc1:ucLiteral ID="ltTimeSuggested_3" runat="server"></cc1:ucLiteral>
                                                    </td>
                                                    <td colspan="4">
                                                        &nbsp;
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="7" align="left">
                                                        <cc1:ucLabel ID="lblChangeBookingTime_1" runat="server" Text="To change the time of this booking, click on the slot required in the grid below."></cc1:ucLabel>
                                                    </td>
                                                </tr>
                                            </table>
                                        </cc1:ucPanel>
                                        <br />
                                        <asp:Panel ID="tableDiv_GeneralVendor" runat="server" CssClass="tableDiv" onscroll="getScroll2(this);">
                                        </asp:Panel>
                                        <%--<asp:Panel ID="pnlAlternateSlotHeaderVendor" runat="server" Style="width: 98%; height: 100px;
                                overflow: auto;" onmouseover="setToolTip();">
                            </asp:Panel>
                            <asp:Panel ID="pnlAlternateSlotVendor" runat="server" Style="width: 98%; height: 335px;
                                overflow: auto" onmouseover="setToolTip();" onscroll="getScroll2(this);">
                            </asp:Panel>--%>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="btnConfirmAlternateSlotVendor" />
                                        <asp:AsyncPostBackTrigger ControlID="hdnbuttonVendor" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </cc1:ucView>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="ddlCarrier" />
                            <asp:AsyncPostBackTrigger ControlID="ddlVehicleType" />
                        </Triggers>
                    </asp:UpdatePanel>
                    <div class="button-row">
                        <cc1:ucButton ID="btnBack" runat="server" CssClass="button" Width="60px" />
                        <cc1:ucButton ID="btnProceedBooking" runat="server" CssClass="button" ValidationGroup="AddPurchaseOrder"
                            OnClick="btnProceedBooking_Click" />
                        <%--OnClick="btnProceedAddPO_Click" OnClientClick="return chkVendorID();" ValidationGroup="DeliveryDetail" />--%>
                    </div>
                </cc1:ucView>
            </cc1:ucMultiView>
        </div>
    </div>
    <div class="bottom-shadow">
    </div>
    <%---WARNING popup start--%>
    <asp:UpdatePanel ID="updpnlWarning" runat="server">
        <ContentTemplate>
            <asp:Button ID="btnConfirmMsg" runat="Server" Style="display: none" />
            <ajaxToolkit:ModalPopupExtender ID="mdlConfirmMsg" runat="server" TargetControlID="btnConfirmMsg"
                PopupControlID="pnlConfirmMsg" BackgroundCssClass="modalBackground" BehaviorID="ConfirmMsg"
                DropShadow="false" />
            <asp:Panel ID="pnlConfirmMsg" runat="server" Style="display: none;">
              <div class="popup-maincontainer" style="overflow-y: hidden; overflow-x: hidden;">
                    <h3><cc1:ucLabel ID="lblWarningInfo" runat="server"></cc1:ucLabel></h3>
                    <table cellspacing="5" cellpadding="0" border="0" align="center" class="popup-maincontainer">
                        <tr>
                            <td>
                              <div class="popup-innercontainer top-setting-Popup">
                                 <div class="row1"><cc1:ucLiteral ID="ltConfirmMsg" runat="server" Text=""></cc1:ucLiteral></div>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                            <div class="row">
                                <cc1:ucButton ID="btnErrContinue" runat="server" Text="CONTINUE" CssClass="button"
                                    OnCommand="btnContinue_Click" />
                                &nbsp;
                                <cc1:ucButton ID="btnErrBack" runat="server" Text="BACK" CssClass="button" OnCommand="btnBack_Click" />
                            </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnErrContinue" />
            <asp:AsyncPostBackTrigger ControlID="btnErrBack" />
        </Triggers>
    </asp:UpdatePanel>
    <%---WARNING popup End--%>
    <%---ERROR popup start--%>
    <asp:UpdatePanel ID="updpnlError" runat="server">
        <ContentTemplate>
            <asp:Button ID="btnErrorMsg" runat="Server" Style="display: none" />
            <ajaxToolkit:ModalPopupExtender ID="mdlErrorMsg" runat="server" TargetControlID="btnErrorMsg"
                PopupControlID="pnlErrorMsg" BackgroundCssClass="modalBackground" BehaviorID="ErrorMsg"
                DropShadow="false" />
            <asp:Panel ID="pnlErrorMsg" runat="server" Style="display: none;">
               <div class="popup-maincontainer" style="overflow-y: hidden; overflow-x: hidden;">
                     <h3><cc1:ucLabel ID="lblERROR" runat="server"></cc1:ucLabel></h3>
                    <table cellspacing="5" cellpadding="0" border="0" align="center" class="popup-maincontainer">
                        <tr>
                            <td>
                            <div class="popup-innercontainer top-setting-Popup">
                                <div class="row1"> <cc1:ucLiteral ID="ltErrorMsg" runat="server" Text=""></cc1:ucLiteral></div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                             <div class="row">
                                <cc1:ucButton ID="btnErrorMsgOK" runat="server" Text="OK" CssClass="button" OnCommand="btnErrorMsgOK_Click" />
                           </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnErrorMsgOK" />
        </Triggers>
    </asp:UpdatePanel>
    <%---ERROR popup End--%>
    <%---OutstandingPO popup start--%>
    <asp:UpdatePanel ID="updOutstandingPO" runat="server">
        <ContentTemplate>
            <asp:Button ID="btnOutstandingPO" runat="Server" Style="display: none" />
            <ajaxToolkit:ModalPopupExtender ID="mdlOutstandingPO" runat="server" TargetControlID="btnOutstandingPO"
                PopupControlID="pnlOutstandingPO" BackgroundCssClass="modalBackground" BehaviorID="OutstandingPO"
                DropShadow="false" />
            <asp:Panel ID="pnlOutstandingPO" runat="server" Style="display: none;">
                <div  style="overflow-y: scroll; overflow-x: hidden; background-color: #fff; padding: 5px;
                    border: 2px solid #ccc; text-align: center; max-height: 500px;">
                    <table cellspacing="5" cellpadding="0" border="0" align="center" class="top-settingsNoBorder-popup">
                        <tr>
                            <td style="font-weight: bold; color: Red; font-size: 14px; text-align: center;">
                                <cc1:ucLabel ID="lblPOWarning" runat="server" Text="WARNING"></cc1:ucLabel>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold; color: Red; font-size: 14px; text-align: center;">
                                <cc1:ucLabel ID="lblPOMsg1" runat="server" Text="The following additional Purchase Orders are expected by the date of this booking and have yet to be booked in"></cc1:ucLabel>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <cc1:ucGridView ID="gvOutstandingPO" runat="server" AutoGenerateColumns="false" CssClass="grid"
                                    CellPadding="0" Width="70%">
                                    <AlternatingRowStyle BackColor="White" ForeColor="#284775"></AlternatingRowStyle>
                                    <Columns>
                                        <asp:BoundField HeaderText="Purchase Order" DataField="PurchaseNumber" SortExpression="PurchaseNumber">
                                            <HeaderStyle Width="30%" />
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:BoundField>
                                        <asp:BoundField HeaderText="Line still Due" DataField="OutstandingLines" SortExpression="OutstandingLines">
                                            <HeaderStyle Width="40%" />
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:BoundField>
                                        <asp:BoundField HeaderText="Due date" DataField="Original_due_date" SortExpression="Original_due_date">
                                            <HeaderStyle Width="30%" />
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:BoundField>
                                    </Columns>
                                </cc1:ucGridView>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold; color: Red; font-size: 14px; text-align: center;">
                                <cc1:ucLabel ID="lblPOMsg2" runat="server" Text="If you are not planning to deliver these by this date then you must contact the Stock Planner urgently"></cc1:ucLabel>
                            </td>
                        </tr>
                    </table>
                </div>
                <table cellspacing="0" cellpadding="0" border="0" align="left" style="width: 810px! important;"
                    class="top-settingsNoBorder">
                    <tr>
                        <td align="center">
                            <cc1:ucButton ID="btnOutstandingPOContinue" runat="server" Text="CONTINUE" CssClass="button"
                                OnClick="btnOutstandingPOContinue_Click" />
                            &nbsp;
                            <cc1:ucButton ID="btnOutstandingPOBack" runat="server" Text="BACK" CssClass="button"
                                OnClick="btnOutstandingPOBack_Click" />
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnOutstandingPOContinue" />
            <asp:AsyncPostBackTrigger ControlID="btnOutstandingPOBack" />
        </Triggers>
    </asp:UpdatePanel>
    <%---OutstandingPO popup End--%>
    <%---PO WARNING popup start--%>
    <asp:UpdatePanel ID="updpnlPOWarning" runat="server">
        <ContentTemplate>
            <asp:Button ID="btnPOConfirmMsg" runat="Server" Style="display: none" />
            <ajaxToolkit:ModalPopupExtender ID="mdlPOConfirmMsg" runat="server" TargetControlID="btnPOConfirmMsg"
                PopupControlID="pnlPOConfirmMsg" BackgroundCssClass="modalBackground" BehaviorID="POConfirmMsg"
                DropShadow="false" />
            <asp:Panel ID="pnlPOConfirmMsg" runat="server" Style="display: none;">
                <div class="popup-maincontainer" style="overflow-y: hidden; overflow-x: hidden;">
                     <h3><cc1:ucLabel ID="lblWarningInfo_2" runat="server"></cc1:ucLabel></h3>
                    <table cellspacing="5" cellpadding="0" border="0" align="center" class="popup-maincontainer">
                       <%-- <tr>
                            <td style="font-weight: bold; color: Red; font-size: 14px; text-align: center;">
                                <cc1:ucLabel ID="UcLabel1" runat="server" Text="WARNING"></cc1:ucLabel>
                            </td>
                        </tr>--%>
                        <tr>
                            <td> 
                            <div  class="popup-innercontainer top-setting-Popup">
                                <div class="row1"><cc1:ucLiteral ID="ltPOConfirmMsg" runat="server" Text=""></cc1:ucLiteral>
                           </div>
                            </td>
                            
                        </tr>
                        <tr>
                            <td align="center">
                            <div class="row">
                                <cc1:ucButton ID="btnPOErrContinue" runat="server" Text="CONTINUE" CssClass="button"
                                    OnCommand="btnPOContinue_Click" />
                                &nbsp;
                                <cc1:ucButton ID="btnPOErrBack" runat="server" Text="BACK" CssClass="button" OnCommand="btnPOBack_Click" />
                            </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnPOErrContinue" />
            <asp:AsyncPostBackTrigger ControlID="btnPOErrBack" />
        </Triggers>
    </asp:UpdatePanel>
    <%---WARNING popup End--%>
    <%---Sprint 1 - Point 7 - ShowPO popup start--%>
    <asp:UpdatePanel ID="updShowPO" runat="server">
        <ContentTemplate>
            <asp:Button ID="btnShowPO" runat="Server" Style="display: none" />
            <ajaxToolkit:ModalPopupExtender ID="mdlShowPO" runat="server" TargetControlID="btnShowPO"
                PopupControlID="pnlShowPO" BackgroundCssClass="modalBackground" BehaviorID="ShowPO"
                DropShadow="false" />
            <asp:Panel ID="pnlShowPO" runat="server" Style="display: none;" Width="80%">
                <div style="overflow-y: scroll; overflow-x: hidden; background-color: #fff; padding: 5px;
                    border: 2px solid #ccc; text-align: center; max-height: 500px;">
                    <table cellspacing="5" cellpadding="0" border="0" align="center" class="top-settingsNoBorder-popup"
                        width="100%">
                        <tr>
                            <td align="center">
                                <cc1:ucGridView ID="grdShowPO" runat="server" AutoGenerateColumns="false" CssClass="grid"
                                    CellPadding="0" Width="100%" AllowSorting="true" OnRowDataBound="grdShowPO_RowDataBound"
                                    OnSorting="grdShowPO_Sorting">
                                    <AlternatingRowStyle BackColor="White" ForeColor="#284775"></AlternatingRowStyle>
                                    <Columns>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:CheckBox runat="server" ID="chkPO" Visible='<%# Convert.ToBoolean(Eval("Allowed")) %>' />
                                                <asp:HiddenField ID="hdnPONumber" runat="server" Value='<%# Eval("PurchaseNumber") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Status" SortExpression="Status">
                                            <ItemTemplate>
                                                <asp:Literal ID="ltbookingStatus" runat="server" Text='<%# Eval("Status") %>'></asp:Literal>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <%-- <asp:BoundField HeaderText="Status" DataField="Status" SortExpression="Status">
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:BoundField>--%>
                                        <asp:BoundField HeaderText="Purchase Order" DataField="PurchaseNumber" SortExpression="PurchaseNumber">
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:BoundField>
                                        <asp:BoundField HeaderText="Vendor Name" DataField="VendorName">
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:BoundField>
                                        <asp:BoundField HeaderText="LinesStillDue" DataField="OutstandingLines">
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:BoundField>
                                        <asp:BoundField HeaderText="Back Orders" DataField="BackOrders">
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:BoundField>
                                        <asp:TemplateField HeaderText="Due date" SortExpression="Expected_date">
                                            <ItemStyle HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <cc1:ucLiteral ID="ltExpectedDate" runat="server" Text='<%# Eval("Expected_date","{0:dd-MM-yyyy}") %>'></cc1:ucLiteral>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField HeaderText="Delivery date" DataField="BookingDeliveryDate">
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:BoundField>
                                    </Columns>
                                </cc1:ucGridView>
                            </td>
                        </tr>
                    </table>
                </div>
                <table width="100%" cellspacing="0" cellpadding="0" border="0" align="center" class="top-settingsNoBorder">
                    <tr>
                        <td align="center">
                            <cc1:ucButton ID="btnAddToBooking" runat="server" Text="Add to Booking" CssClass="button"
                                OnClick="btnAddPOToBooking_Click" />
                            &nbsp;
                            <cc1:ucButton ID="btnCancel_1" runat="server" Text="Cancel" CssClass="button" />
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    <%---Sprint 1 - Point 7 - ShowPO popup End--%>
</asp:Content>

﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using BaseControlLibrary;
using BusinessEntities.ModuleBE.AdminFunctions;
using BusinessEntities.ModuleBE.Appointment.Booking;
using BusinessEntities.ModuleBE.Appointment.CountrySetting;
using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using BusinessEntities.ModuleBE.Languages.Languages;
using BusinessEntities.ModuleBE.OnTimeInFull.CountrySetting;
using BusinessEntities.ModuleBE.Upload;
using BusinessLogicLayer.ModuleBAL.AdminFunctions;
using BusinessLogicLayer.ModuleBAL.Appointment.Booking;
using BusinessLogicLayer.ModuleBAL.Appointment.CountrySetting;
using BusinessLogicLayer.ModuleBAL.Appointment.SiteSettings;
using BusinessLogicLayer.ModuleBAL.OnTimeInFull.CountrySetting;
using BusinessLogicLayer.ModuleBAL.Upload;
using Utilities;
using WebUtilities;
using System.Text;

public partial class APPBok_CarrierBookingWindowEdit : CommonPage
{
    bool isDoorTypeNotChanged = false;
    bool isDoorCheckPopup = false;
    protected string BK_ALT_CorrectVendor = WebCommon.getGlobalResourceValue("BK_ALT_CorrectVendor");
    protected string BK_ALT_Wait = WebCommon.getGlobalResourceValue("BK_ALT_Wait");
    protected string BK_ALT_04_INT = WebCommon.getGlobalResourceValue("BK_MS_04_INT_Carrier");
    protected string BK_ALT_TimeNotAvailable = WebCommon.getGlobalResourceValue("BK_ALT_TimeNotAvailable");
    protected string BK_ALT_MoveBooking = WebCommon.getGlobalResourceValue("BK_ALT_MoveBooking");
    protected string BK_ALT_Selected_Time = WebCommon.getGlobalResourceValue("BK_ALT_Selected_Time");
    protected string BK_ALT_Selected_Door = WebCommon.getGlobalResourceValue("BK_ALT_Selected_Door");
    protected string BK_ALT_Insufficeint_Space_Ext = WebCommon.getGlobalResourceValue("BK_ALT_Insufficeint_Space_Ext");
    protected string BK_ALT_Insufficeint_Space_Int = WebCommon.getGlobalResourceValue("BK_ALT_Insufficeint_Space_Int");
    protected string AtDoorName = WebCommon.getGlobalResourceValue("AtDoorName");
    string strIsVendorRequiredForSeparateBooking = WebCommon.getGlobalResourceValue("IsVendorRequiredForSeparateBooking");
    string IsBookingEditWithMail = "true";
    static int iTotalPallets = 0;
    static int iTotalCartons = 0;
    static int iTotalLines = 0;
    string []strVendorIDs;
    //APPBOK_BookingBAL APPBOK_BookingBAL

    //--- Sprint 3b ---Slot time length---//

    string sPortalLink = Convert.ToString(ConfigurationManager.AppSettings["Portallink"]) + @"/ModuleUI/Security/Login.aspx";
    string path = AppDomain.CurrentDomain.BaseDirectory.ToLower();
    string templatePathName = @"/EmailTemplates/Appointment/";

    public int iSlotTimeLength = 5;
    //-----------------------------------//

    public bool? IsBookingValidationExcluded
    {
        get
        {
            return (Convert.ToBoolean(IsBookingValidationExcludedCarrier) || Convert.ToBoolean(IsBookingValidationExcludedDelivery));
        }
        set
        {
            ViewState["IsBookingValidationExcluded"] = value;
        }
    }

    public bool? IsBookingValidationExcludedDelivery
    {
        get
        {
            return ViewState["IsBookingValidationExcludedDelivery"] != null ? Convert.ToBoolean(ViewState["IsBookingValidationExcludedDelivery"]) : false;
        }
        set
        {
            ViewState["IsBookingValidationExcludedDelivery"] = value;
        }
    }

    public bool? IsBookingValidationExcludedCarrier
    {
        get
        {
            return ViewState["IsBookingValidationExcludedCarrier"] != null ? Convert.ToBoolean(ViewState["IsBookingValidationExcludedCarrier"]) : false;
        }
        set
        {
            ViewState["IsBookingValidationExcludedCarrier"] = value;
        }
    }

    public bool? IsNonTime
    {
        get
        {
            return ViewState["IsNonTime"] != null ? Convert.ToBoolean(ViewState["IsNonTime"]) : false;
        }
        set
        {
            ViewState["IsNonTime"] = value;
        }
    }

    public bool? IsNonStandardWindowSlot
    {
        get
        {
            return ViewState["IsNonTimeBooking"] != null ? Convert.ToBoolean(ViewState["IsNonTimeBooking"]) : false;
        }
        set
        {
            ViewState["IsNonTimeBooking"] = value;
        }
    }

    public bool? IsNonTimeBooking
    {
        get
        {
            return ViewState["IsNonTimeBooking"] != null ? Convert.ToBoolean(ViewState["IsNonTimeBooking"]) : false;
        }
        set
        {
            ViewState["IsNonTimeBooking"] = value;
        }
    }

    public bool? IsNonTimeCarrier
    {
        get
        {
            return ViewState["IsNonTimeCarrier"] != null ? Convert.ToBoolean(ViewState["IsNonTimeCarrier"]) : false;
        }
        set
        {
            ViewState["IsNonTimeCarrier"] = value;
        }
    }

    public bool? isPostBack
    {
        get
        {
            return ViewState["isPostBack"] != null ? Convert.ToBoolean(ViewState["isPostBack"]) : false;
        }
        set
        {
            ViewState["isPostBack"] = value;
        }
    }

    public bool? IsPreAdviseNoteRequired
    {
        get
        {
            return ViewState["IsPreAdviseNoteRequired"] != null ? Convert.ToBoolean(ViewState["IsPreAdviseNoteRequired"]) : false;
        }
        set
        {
            ViewState["IsPreAdviseNoteRequired"] = value;
        }
    }

    public string strDeliveryValidationExc
    {
        get
        {
            return ViewState["strDeliveryValidationExc"] != null ? ViewState["strDeliveryValidationExc"].ToString() : string.Empty;
        }
        set
        {
            ViewState["strDeliveryValidationExc"] = value;
        }
    }

    public string strDeliveryNonTime
    {
        get
        {
            return ViewState["strDeliveryNonTime"] != null ? ViewState["strDeliveryNonTime"].ToString() : string.Empty;
        }
        set
        {
            ViewState["strDeliveryNonTime"] = value;
        }
    }

    public string strCarrierValidationExc
    {
        get
        {
            return ViewState["strCarrierValidationExc"] != null ? ViewState["strCarrierValidationExc"].ToString() : string.Empty;
        }
        set
        {
            ViewState["strCarrierValidationExc"] = value;
        }
    }

    public string CarrierNarrativeRequired
    {
        get
        {
            return ViewState["CarrierNarrativeRequired"] != null ? ViewState["CarrierNarrativeRequired"].ToString() : string.Empty;
        }
        set
        {
            ViewState["CarrierNarrativeRequired"] = value;
        }
    }

    public string strVehicleDoorTypeMatrix
    {
        get
        {
            return ViewState["strVehicleDoorTypeMatrix"] != null ? ViewState["strVehicleDoorTypeMatrix"].ToString() : string.Empty;
        }
        set
        {
            ViewState["strVehicleDoorTypeMatrix"] = value;
        }
    }

    public string strVehicleDoorTypeMatrixSKU
    {
        get
        {
            return ViewState["strVehicleDoorTypeMatrixSKU"] != null ? ViewState["strVehicleDoorTypeMatrixSKU"].ToString() : string.Empty;
        }
        set
        {
            ViewState["strVehicleDoorTypeMatrixSKU"] = value;
        }
    }

    public string VehicleNarrativeRequired
    {
        get
        {
            return ViewState["VehicleNarrativeRequired"] != null ? ViewState["VehicleNarrativeRequired"].ToString() : string.Empty;
        }
        set
        {
            ViewState["VehicleNarrativeRequired"] = value;
        }
    }

    public int? PreSiteCountryID
    {
        get
        {
            return ViewState["PreSiteCountryID"] != null ? Convert.ToInt32(ViewState["PreSiteCountryID"].ToString()) : (int?)null;
        }
        set
        {
            ViewState["PreSiteCountryID"] = value;
        }
    }

    public NameValueCollection ArrDoorTypePriority
    {
        get
        {
            return ViewState["ArrDoorTypePriority"] != null ? (NameValueCollection)ViewState["ArrDoorTypePriority"] : null;
        }
        set
        {
            ViewState["ArrDoorTypePriority"] = value;
        }
    }

    public NameValueCollection ArrDoorTypePrioritySKU
    {
        get
        {
            return ViewState["ArrDoorTypePrioritySKU"] != null ? (NameValueCollection)ViewState["ArrDoorTypePrioritySKU"] : null;
        }
        set
        {
            ViewState["ArrDoorTypePrioritySKU"] = value;
        }
    }

    public NameValueCollection MsgSupressed
    {
        get
        {
            return ViewState["MsgSupressed"] != null ? (NameValueCollection)ViewState["MsgSupressed"] : null;
        }
        set
        {
            ViewState["MsgSupressed"] = value;
        }
    }

    public string Lines
    {
        get
        {
            return ViewState["Lines"] != null ? ViewState["Lines"].ToString() : "XX";
        }
        set
        {
            ViewState["Lines"] = value;
        }
    }

    public string TotalLines
    {
        get
        {
            return ViewState["TotalLines"] != null ? ViewState["TotalLines"].ToString() : "XX";
        }
        set
        {
            ViewState["TotalLines"] = value;
        }
    }

    public int? NonTimeDeliveryCartonVolume
    {
        get
        {
            return ViewState["NonTimeDeliveryCartonVolume"] != null ? Convert.ToInt32(ViewState["NonTimeDeliveryCartonVolume"].ToString()) : (int?)null;
        }
        set
        {
            ViewState["NonTimeDeliveryCartonVolume"] = value;
        }
    }

    public int? NonTimeDeliveryPalletVolume
    {
        get
        {
            return ViewState["NonTimeDeliveryPalletVolume"] != null ? Convert.ToInt32(ViewState["NonTimeDeliveryPalletVolume"].ToString()) : (int?)null;
        }
        set
        {
            ViewState["NonTimeDeliveryPalletVolume"] = value;
        }
    }

    public string strSuggestedWindowSlotID
    {
        get
        {
            return ViewState["strSuggestedWindowSlotID"] != null ? ViewState["strSuggestedWindowSlotID"].ToString() : string.Empty;
        }
        set
        {
            ViewState["strSuggestedWindowSlotID"] = value;
        }
    }

    public string strSuggestedSiteDoorNumberID
    {
        get
        {
            return ViewState["strSuggestedSiteDoorNumberID"] != null ? ViewState["strSuggestedSiteDoorNumberID"].ToString() : string.Empty;
        }
        set
        {
            ViewState["strSuggestedSiteDoorNumberID"] = value;
        }
    }

    public string strSuggestedSiteDoorNumber
    {
        get
        {
            return ViewState["strSuggestedSiteDoorNumber"] != null ? ViewState["strSuggestedSiteDoorNumber"].ToString() : string.Empty;
        }
        set
        {
            ViewState["strSuggestedSiteDoorNumber"] = value;
        }
    }

    IDictionary<string, CarrierDetailsWin> SlotDoorCounter = new Dictionary<string, CarrierDetailsWin>();

    public int BookedPallets
    {
        get
        {
            return ViewState["BookedPallets"] != null ? Convert.ToInt32(ViewState["BookedPallets"]) : 0;
        }
        set
        {
            ViewState["BookedPallets"] = value;
        }
    }

    public int BookedLines
    {
        get
        {
            return ViewState["BookedLines"] != null ? Convert.ToInt32(ViewState["BookedLines"]) : 0;
        }
        set
        {
            ViewState["BookedLines"] = value;
        }
    }

    public DateTime LogicalSchedulDateTime
    {
        get
        {
            return ViewState["LogicalSchedulDateTime"] != null ? Convert.ToDateTime(ViewState["LogicalSchedulDateTime"].ToString()) : new DateTime(1900, 01, 01, 0, 0, 00);
        }
        set
        {
            ViewState["LogicalSchedulDateTime"] = value;
        }
    }

    public DateTime ActualSchedulDateTime
    {
        get
        {
            return ViewState["ActualSchedulDateTime"] != null ? Convert.ToDateTime(ViewState["ActualSchedulDateTime"].ToString()) : new DateTime(1900, 01, 01, 0, 0, 00);
        }
        set
        {
            ViewState["ActualSchedulDateTime"] = value;
        }
    }

    public bool isProvisional
    {
        get
        {
            return ViewState["isProvisional"] != null ? Convert.ToBoolean(ViewState["isProvisional"]) : false;
        }
        set
        {
            ViewState["isProvisional"] = value;
        }
    }

    public string ProvisionalReason {
        get {
            return ViewState["ProvisionalReason"] != null ? Convert.ToString(ViewState["ProvisionalReason"]) : "";
        }
        set {
            ViewState["ProvisionalReason"] = value;
        }
    }

    //--- Start Stage 9 Point 2a/2b---//
    public bool? IsProvisionalBooking {
        get {
            return ViewState["IsProvisionalBooking"] != null ? Convert.ToBoolean(ViewState["IsProvisionalBooking"]) : false;
        }
        set {
            ViewState["IsProvisionalBooking"] = value;
        }
    }
    //--- End Stage 9 Point 2a/2b---//

    public enum BookingType
    {
        //FixedSlot = 1, Unscheduled = 2, NonTimedDelivery = 3, BookedSlot = 4, 
        ProvisionalNonTimedDelivery = 8, //--- Stage 9 Point 2a/2b-----//
        Provisional = 5, WindowSlot = 6, NonStandardWindowSlot = 7
    };

    public string TotalPallets
    {
        get
        {
            return ViewState["TotalPallets"] != null ? ViewState["TotalPallets"].ToString() : "0";
        }
        set
        {
            ViewState["TotalPallets"] = value;
        }
    }

    public string TotalCartons
    {
        get
        {
            return ViewState["TotalCartons"] != null ? ViewState["TotalCartons"].ToString() : "0";
        }
        set
        {
            ViewState["TotalCartons"] = value;
        }
    }

    public string BeforeSlotTimeID
    {
        get
        {
            return ViewState["BeforeSlotTimeID"] != null ? ViewState["BeforeSlotTimeID"].ToString() : "0";
        }
        set
        {
            ViewState["BeforeSlotTimeID"] = value;
        }
    }

    public string AfterSlotTimeID
    {
        get
        {
            return ViewState["AfterSlotTimeID"] != null ? ViewState["AfterSlotTimeID"].ToString() : "300";
        }
        set
        {
            ViewState["AfterSlotTimeID"] = value;
        }
    }

    //----Stage 10 Point 10 L 5.1-----//
    public string IsVehicleContainer {
        get {
            return ViewState["IsVehicleContainer"] != null ? ViewState["IsVehicleContainer"].ToString() : string.Empty;
        }
        set {
            ViewState["IsVehicleContainer"] = value;
        }
    }

    public bool isContainerError {
        get {
            return ViewState["isContainerError"] != null ? Convert.ToBoolean(ViewState["isContainerError"]) : false;
        }
        set {
            ViewState["isContainerError"] = value;
        }
    }   
    //--------------------------//

    public int? RemainingPallets {
        get {
            return ViewState["RemainingPallets"] != null ? Convert.ToInt32(ViewState["RemainingPallets"]) : (int?)null;
        }
        set {
            ViewState["RemainingPallets"] = value;
        }
    }

    public int? RemainingLines {
        get {
            return ViewState["RemainingLines"] != null ? Convert.ToInt32(ViewState["RemainingLines"]) : (int?)null;
        }
        set {
            ViewState["RemainingLines"] = value;
        }
    }


    protected void Page_InIt(object sender, EventArgs e)
    {
        ddlSourceCountry.CurrentPage = this;
        ddlSourceCountry.IsAllRequired = false;
        ddlSite.CurrentPage = this;
        txtSchedulingdate.CurrentPage = this;
        ddlSite.TimeSlotWindow = "W";
    }    

    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            pnlPalletCheckforCountryDisplay.Visible = chkSiteSetting.Checked;

            hdnCurrentMode.Value = GetQueryStringValue("Mode").ToString();

            if (GetQueryStringValue("WindowSlot") == null)
                hdnWindowSlotMode.Value = "false";
            else if (GetQueryStringValue("WindowSlot") == "True")
                hdnWindowSlotMode.Value = "true";

            hdnLogicalScheduleDate.Value = GetQueryStringValue("Logicaldate").ToString();
            hdnActualScheduleDate.Value = GetQueryStringValue("Scheduledate").ToString();

            LogicalSchedulDateTime = Utilities.Common.TextToDateFormat(hdnLogicalScheduleDate.Value);
            ActualSchedulDateTime = Utilities.Common.TextToDateFormat(hdnActualScheduleDate.Value);
        }

        if (!Page.IsPostBack)
        {

            if (GetQueryStringValue("SiteCountryID") == null)
            {
                hdnActualScheduleDate.Value = string.Empty; // DateTime.Now.Day.ToString() + "/" + DateTime.Now.Month.ToString() + "/" + DateTime.Now.Year.ToString();
                ddlSite.innerControlddlSite.AutoPostBack = true;
                PreSiteCountryID = Convert.ToInt32(Session["SiteCountryID"]);
            }
            else
            {
                ddlSite.innerControlddlSite.AutoPostBack = true;
                PreSiteCountryID = Convert.ToInt32(GetQueryStringValue("SiteCountryID").ToString());
            }

            //if (hdnCurrentMode.Value == "Add" && hdnWindowSlotMode.Value == "false" && hdnCurrentRole.Value == "Carrier")
            //{
            //    // Here Vendor details will be set, when first time page will be load.
            //    this.SetVendorDetailsByUserID();
            //    this.ActivateFistView();
            //}
            if (hdnCurrentMode.Value == "Edit" && GetQueryStringValue("Type").ToString() == "C")
            {

                btnConfirmAlternateSlot.Visible = false;
                btnConfirmBookingWithMail_1.Visible = true;
                btnConfirmBookingWithoutMail_1.Visible = true;

                APPBOK_BookingBE oAPPBOK_BookingBE = new APPBOK_BookingBE();
                APPBOK_BookingBAL oAPPBOK_BookingBAL = new APPBOK_BookingBAL();

                oAPPBOK_BookingBE.Action = "GetCarrierBookingDeatilsByID";
                oAPPBOK_BookingBE.BookingID = Convert.ToInt32(GetQueryStringValue("ID").ToString());

                List<APPBOK_BookingBE> lstBookingDeatils = new List<APPBOK_BookingBE>();
                lstBookingDeatils = oAPPBOK_BookingBAL.GetCarrierBookingDetailsBAL(oAPPBOK_BookingBE);

                if (lstBookingDeatils != null && lstBookingDeatils.Count > 0)
                {
                    #region Country Pallet Check
                    CountryData.Visible = lstBookingDeatils[0].ISPM15CountryPalletChecking;
                    pnlPalletCheckforCountryDisplay.Visible = lstBookingDeatils[0].ISPM15CountryPalletChecking;
                    lblYesNo.Text = lstBookingDeatils[0].ISPM15CountryPalletChecking ? "Yes" : "No";
                    chkSiteSetting.Checked = lstBookingDeatils[0].ISPM15CountryPalletChecking;
                    ddlSourceCountry.innerControlddlCountry.SelectedValue = lstBookingDeatils[0].ISPM15FromCountryID.ToString();
                    lblFromCountryValue.Text = ddlSourceCountry.innerControlddlCountry.SelectedItem.Text;
                    #endregion

                    // stage 15 R2 point no 17
                    hdnIsBookingAccepted.Value = Convert.ToString(lstBookingDeatils[0].IsBookingAccepted);
                    //

                    hdnBookingRef.Value = lstBookingDeatils[0].BookingRef;

                    if (lstBookingDeatils[0].BookingTypeID == 7) //Non time booking
                        IsNonStandardWindowSlot = true;
                    else if (lstBookingDeatils[0].BookingTypeID == 5) //Stage 9 Point 2a/2b
                        IsProvisionalBooking = true;

                    spCarrier.Style.Add("display", "none");
                    hdnProvisionalReason.Value = lstBookingDeatils[0].ProvisionalReason;
                    hdnBookingTypeID.Value = Convert.ToString(lstBookingDeatils[0].BookingTypeID);
                    hdnLine.Value = Convert.ToString(lstBookingDeatils[0].OldNumberOfLines);
                    ltCarrierName.Visible = true;
                    ltCarrierName.Text = lstBookingDeatils[0].Carrier.CarrierName;

                    txtEditBookingCommentF.Text = Convert.ToString(lstBookingDeatils[0].BookingComments);
                    txtEditBookingComment.Text = Convert.ToString(lstBookingDeatils[0].BookingComments);

                    ActivateFistView();

                    ddlDeliveryType.SelectedIndex = ddlDeliveryType.Items.IndexOf(ddlDeliveryType.Items.FindByValue(lstBookingDeatils[0].Delivery.DeliveryTypeID.ToString()));
                    ddlDeliveryType_SelectedIndexChanged(sender, e);
                    //ddlCarrier_SelectedIndexChanged(sender, e);
                    //ddlVehicleType_SelectedIndexChanged(sender, e);

                    
                    ddlCarrier.SelectedIndex = ddlCarrier.Items.IndexOf(ddlCarrier.Items.FindByValue(lstBookingDeatils[0].Carrier.CarrierID.ToString()));
                    
                    ddlVehicleType.SelectedIndex = ddlVehicleType.Items.IndexOf(ddlVehicleType.Items.FindByValue(lstBookingDeatils[0].VehicleType.VehicleTypeID.ToString()));
                    hdnPreviousVehicleType.Value = lstBookingDeatils[0].VehicleType.VehicleTypeID.ToString();
                    ddlCarrier_SelectedIndexChanged(sender, e);
                    if (lstBookingDeatils[0].OtherCarrier != string.Empty) {

                        txtCarrierOther.Style.Add("display", "");
                        //lblCarrierAdvice.Style.Add("display", "");
                        rfvCarrierOtherRequired.Enabled = true;
                        txtCarrierOther.Text = lstBookingDeatils[0].OtherCarrier;
                        txtCarrierOther.Enabled = false;
                    }
                    ShowHazordousItem();
                    if (lstBookingDeatils[0].IsEnableHazardouesItemPrompt)
                    {
                        checkBoxEnableHazardouesItemPrompt.Checked = true;
                    }
                    else
                    {
                        checkBoxEnableHazardouesItemPrompt.Checked = false;
                    }
                    ddlVehicleType_SelectedIndexChanged(sender, e);
                    if (lstBookingDeatils[0].OtherVehicle != string.Empty) {

                        txtVehicleOther.Style.Add("display", "");
                        //lblVehicleAdvice.Style.Add("display", "");
                        rfvVehicleTypeOtherRequired.Enabled = true;
                        txtVehicleOther.Text = lstBookingDeatils[0].OtherVehicle;
                        txtVehicleOther.Enabled = false;
                    }

                    ltCarrier.Text = ddlCarrier.SelectedItem.Text;
                    ltOtherCarrier.Text = ddlCarrier.SelectedItem.Text;
                    ltDeliveryType.Text = ddlDeliveryType.SelectedItem.Text;

                    //BIND ALREADY BOOKED SLOT DETAILS//                   

                    if (Convert.ToBoolean(IsNonStandardWindowSlot)) {
                        hdnPreservSlot.Value = lstBookingDeatils[0].NonWindowFromTimeID + "|" + lstBookingDeatils[0].NonWindowToTimeID + "|" + lstBookingDeatils[0].DoorNoSetup.SiteDoorNumberID.ToString();
                        hdnStartTimeID.Value = Convert.ToString(lstBookingDeatils[0].NonWindowFromTimeID);
                        hdnEndTimeID.Value = Convert.ToString(lstBookingDeatils[0].NonWindowToTimeID);
                    }


                    ltBookingDate.Text = ActualSchedulDateTime.ToString("dd/MM/yyyy");
                    ltSuggestedSlotTime.Text = lstBookingDeatils[0].SlotTime.SlotTime + " " + AtDoorName + " " + lstBookingDeatils[0].DoorNoSetup.DoorNumber.ToString();

                    if (lstBookingDeatils[0].TimeWindow.TimeWindowID >0  && lstBookingDeatils[0].TimeWindow.TimeWindowID != 0)
                    {
                        MASSIT_TimeWindowBE oMASSIT_TimeWindowBE = new MASSIT_TimeWindowBE();
                        oMASSIT_TimeWindowBE.Action = "GetTimeWindowData";
                        oMASSIT_TimeWindowBE.TimeWindowID = lstBookingDeatils[0].TimeWindow.TimeWindowID;
                        MASSIT_TimeWindowBAL oMASSIT_TimeWindowBAL = new MASSIT_TimeWindowBAL();
                        List<MASSIT_TimeWindowBE> lstMASSIT_TimeWindowBE = oMASSIT_TimeWindowBAL.GetTimeWindowBAL(oMASSIT_TimeWindowBE);
                        if (lstMASSIT_TimeWindowBE.Count > 0)
                        {
                            hdnPreservSlot.Value = lstMASSIT_TimeWindowBE[0].StartSlotTimeID + "|" + lstMASSIT_TimeWindowBE[0].EndSlotTimeID + "|" + lstMASSIT_TimeWindowBE[0].SiteDoorNumberID.ToString();
                        }
                    }
                    //hdnSuggestedSlotTimeID.Value = lstBookingDeatils[0].SlotTime.SlotTimeID.ToString();
                    strSuggestedWindowSlotID = lstBookingDeatils[0].TimeWindow.TimeWindowID.ToString();
                    strSuggestedSiteDoorNumberID = lstBookingDeatils[0].DoorNoSetup.SiteDoorNumberID.ToString();
                    strSuggestedSiteDoorNumber = lstBookingDeatils[0].DoorNoSetup.DoorNumber.ToString();
                    //hdnSuggestedSlotTimeID.Value = lstBookingDeatils[0].SlotTime.SlotTimeID.ToString();
                   // strSuggestedWindowSlotID = lstBookingDeatils[0].FixedSlot.FixedSlotID.ToString();
                    //strSuggestedSiteDoorNumberID = lstBookingDeatils[0].DoorNoSetup.SiteDoorNumberID.ToString();
                   // strSuggestedSiteDoorNumber = lstBookingDeatils[0].DoorNoSetup.DoorNumber.ToString();

                    //hdnSuggestedSlotTime.Value = lstBookingDeatils[0].SlotTime.SlotTime;
                    //ltTimeSlot.Text = hdnSuggestedSlotTime.Value;
                    hdnSuggestedWeekDay.Value = getWeekday(lstBookingDeatils[0].WeekDay);

                    //ltDoorNo.Text = strSuggestedSiteDoorNumber;
                    //ltTimeSlot_1.Text = lstBookingDeatils[0].SlotTime.SlotTime;
                    //ltDoorNo_1.Text = strSuggestedSiteDoorNumber;
                    hdnSuggestedWeekDay.Value = getWeekday(lstBookingDeatils[0].WeekDay);

                    if (hdnCurrentMode.Value == "Edit")
                    {
                       // ProceedAddPO(); // btnProceedAddPO_Click(sender, e);
                        BindAllPOGrid();
                        BindVolumeDetailsGrid();
                        pnlBookingDetailsArrival.Visible = true;                      
                        mvBookingEdit.ActiveViewIndex = 0;
                    }

                    //-----------Start Stage 9 point 2a/2b--------------//
                    if (GetQueryStringValue("PNTB") != null && GetQueryStringValue("PNTB").ToString() == "True") {
                        Session["BookingCreated"] = false;
                        btnEnterAltTime_Click(null, null);
                    }
                    if (lstBookingDeatils[0].BookingTypeID == 5 && hdnCurrentRole.Value != "Carrier") {
                        if (hdnLine.Value != "0" && hdnProvisionalReason.Value == "EDITED" && Session["Role"].ToString().ToLower() != "carrier")
                        {
                            btnReject.Visible = false;
                            btnReject_1.Visible = false;
                        }
                        else
                        {
                            btnReject.Visible = true;
                            btnReject_1.Visible = true;
                        }
                    }
                    //-----------End Stage 9 point 2a/2b--------------//
                }
            }
            else if (hdnWindowSlotMode.Value == "true" && GetQueryStringValue("Type").ToString() == "C")
            {

                MASSIT_TimeWindowBE oMASSIT_TimeWindowBE = new MASSIT_TimeWindowBE();
                MASSIT_TimeWindowBAL oMASSIT_TimeWindowBAL = new MASSIT_TimeWindowBAL();

                oMASSIT_TimeWindowBE.Action = "ShowAllReserved";
                oMASSIT_TimeWindowBE.TimeWindowID = Convert.ToInt32(GetQueryStringValue("ID"));
                //oMASSIT_TimeWindowBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
                //oMASSIT_TimeWindowBE.User.UserID = Convert.ToInt32(Session["UserID"]);

                List<MASSIT_TimeWindowBE> lstFixedSlot = oMASSIT_TimeWindowBAL.GetReservedWindowBAL(oMASSIT_TimeWindowBE);

                if (lstFixedSlot != null && lstFixedSlot.Count > 0)
                {

                    spCarrier.Style.Add("display", "none");

                    ltCarrierName.Visible = true;
                    ltCarrierName.Text = lstFixedSlot[0].VendorName;

                    strSuggestedWindowSlotID = lstFixedSlot[0].TimeWindowID.ToString();
                    ActivateFistView();

                    ddlCarrier.SelectedIndex = ddlCarrier.Items.IndexOf(ddlCarrier.Items.FindByValue(lstFixedSlot[0].IncludedCarrierIDs.ToString()));

                    //BIND ALREADY BOOKED SLOT DETAILS//

                    ltSuggestedSlotTime.Text = lstFixedSlot[0].StartTime + " - " + lstFixedSlot[0].EndTime + " " + AtDoorName + " " + lstFixedSlot[0].DoorNumber.ToString();
                    hdnSuggestedSlotTimeID.Value = lstFixedSlot[0].TimeWindowID.ToString();

                    strSuggestedSiteDoorNumberID = lstFixedSlot[0].SiteDoorNumberID.ToString();
                    strSuggestedSiteDoorNumber = lstFixedSlot[0].DoorNumber.ToString();

                    //hdnSuggestedSlotTime.Value = lstFixedSlot[0].SlotTime.SlotTime;
                    //ltTimeSlot.Text = hdnSuggestedSlotTime.Value;


                    //ltDoorNo.Text = strSuggestedSiteDoorNumber;
                    //ltTimeSlot_1.Text = lstFixedSlot[0].SlotTime.SlotTime;
                    //ltDoorNo_1.Text = strSuggestedSiteDoorNumber;

                    spSCDate.Style.Add("display", "none");
                    ltSCdate.Visible = true;
                    ltSCdate.Text = string.Empty;
                    //if (GetQueryStringValue("BookingWeekDay") != null && GetQueryStringValue("BookingWeekDay") != string.Empty && GetQueryStringValue("BookingWeekDay") != "8") {
                    //    ltBookingDate.Text = txtSchedulingdate.GetDate.AddDays(-1).ToString("dd/MM/yyyy");
                    //    hdnSuggestedWeekDay.Value = txtSchedulingdate.GetDate.AddDays(-1).ToString("ddd");
                    //    ltSCdate.Text = txtSchedulingdate.GetDate.AddDays(-1).ToString("dd/MM/yyyy") + " " + ltSuggestedSlotTime.Text;
                    //}
                    //else {
                    //    ltBookingDate.Text = txtSchedulingdate.GetDate.Day.ToString() + "/" + txtSchedulingdate.GetDate.Month.ToString() + "/" + txtSchedulingdate.GetDate.Year.ToString();
                    //    hdnSuggestedWeekDay.Value = txtSchedulingdate.GetDate.ToString("ddd");
                    //    ltSCdate.Text = hdnSchedulerdate.Value + " " + ltSuggestedSlotTime.Text;
                    //}


                    ltBookingDate.Text = ActualSchedulDateTime.ToString("dd/MM/yyyy");
                    hdnSuggestedWeekDay.Value = ActualSchedulDateTime.ToString("ddd");
                    ltSCdate.Text = ActualSchedulDateTime.ToString("dd/MM/yyyy") + " " + ltSuggestedSlotTime.Text;

                }
            }

            else if (GetQueryStringValue("Flag").ToString() == "true" && hdnWindowSlotMode.Value == "true")
            {
                ActivateFistView();
                hdnSuggestedWindowSlotID.Value = GetQueryStringValue("ID");
                ltSuggestedSlotTime.Text = GetQueryStringValue("Time") + " " + AtDoorName + " " + GetQueryStringValue("DoorNumber");
                spSCDate.Style.Add("display", "none");
                ltSCdate.Visible = true;
                ltSCdate.Text = string.Empty;
                //for time 

                hdnSuggestedSlotTimeID.Value = GetQueryStringValue("ID");

                strSuggestedWindowSlotID = GetQueryStringValue("ID");

                strSuggestedSiteDoorNumberID = GetQueryStringValue("SiteDoorNumberID");//lstBookingDeatils[0].DoorNoSetup.SiteDoorNumberID.ToString();

                hdnSuggestedSiteDoorNumberID.Value = GetQueryStringValue("SiteDoorNumberID");
                hdnSuggestedSiteDoorNumber.Value = GetQueryStringValue("DoorNumber");
                ltBookingDate.Text = ActualSchedulDateTime.ToString("dd/MM/yyyy");
                hdnSuggestedWeekDay.Value = ActualSchedulDateTime.ToString("ddd");
                ltSCdate.Text = ActualSchedulDateTime.ToString("dd/MM/yyyy") + " " + ltSuggestedSlotTime.Text;
            }
           
            else if (hdnCurrentMode.Value == "Add")
            {
                ActivateFistView();
                ddlCarrier_SelectedIndexChanged(sender, e);

                //if (Session["UserCarrierID"] != null) {
                //    //ddlCarrier.SelectedIndex = ddlCarrier.Items.IndexOf(ddlCarrier.Items.FindByValue(Session["UserCarrierID"].ToString()));
                //    ddlCarrier_SelectedIndexChanged(sender, e);
                //}
            }          
            CheckSiteClosure();
        }

        if (!IsPostBack)
        {
            string strBookingId = Convert.ToString(GetQueryStringValue("ID"));
        }

        //Emergency fix - temp as per Anthony's mail on 11 jan 2016
        if (!IsPostBack) {
            if ((Session["Role"].ToString().ToLower() == "vendor" || Session["Role"].ToString().ToLower() == "carrier")) {
                BindSchedulingClosedown();
            }
        }
        //----------------------------------------------------------
    }

    private void BindSchedulingClosedown() {
        MASSIT_SchedulingClosedownBE oMASSIT_SchedulingClosedownBE = new MASSIT_SchedulingClosedownBE();
        MASSIT_SchedulingClosedownBAL oMASSIT_SchedulingClosedownBAL = new MASSIT_SchedulingClosedownBAL();

        oMASSIT_SchedulingClosedownBE.Action = "ShowAll";

        List<MASSIT_SchedulingClosedownBE> lstSchedulingClosedownBE = oMASSIT_SchedulingClosedownBAL.GetSchedulingClosedownBAL(oMASSIT_SchedulingClosedownBE);

        if (lstSchedulingClosedownBE != null && lstSchedulingClosedownBE.Count > 0) {
            for (int icount = 0; icount < lstSchedulingClosedownBE.Count; icount++) {
                if (ddlSite.innerControlddlSite.Items.IndexOf(ddlSite.innerControlddlSite.Items.FindByValue(lstSchedulingClosedownBE[icount].SiteID.ToString())) >= 0)
                    ddlSite.innerControlddlSite.Items.RemoveAt(ddlSite.innerControlddlSite.Items.IndexOf(ddlSite.innerControlddlSite.Items.FindByValue(lstSchedulingClosedownBE[icount].SiteID.ToString())));
            }
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        Label lblModuleText = (Label)Page.Master.FindControl("lblModuleText");
        lblModuleText.Text = "Appointment Scheduling - Booking";

        if (!Page.IsPostBack) {
            Session["BookingCreated"] = false;
            ViewState["IsShowPOPopup"] = false;
            hdnCurrentRole.Value = Session["Role"].ToString();
        }

        if (!Page.IsPostBack)
        {
            hdnCurrentRole.Value = Session["Role"].ToString();
        }

        //ScriptManager.RegisterStartupScript(this, this.GetType(), "scroll", "<script>setScroll();</script>", false);
        hdnBeforeSlotTimeID.Value = BeforeSlotTimeID;
        hdnAfterSlotTimeID.Value = AfterSlotTimeID;
    }

    protected void rdoNonStandardWindow_CheckedChanged(object sender, EventArgs e)
    {
        if (rdoNonStandardWindow.Checked)
        {
            rfvAlternateWindowRequired.Enabled = false;
            cmpvStartTimeCanNotBeGreaterEndTime.Enabled = true;
            rfvFromRequired.Enabled = true;
            rfvToRequired.Enabled = true;
            rfvDoorNameRequired.Enabled = true;

            strSuggestedWindowSlotID = string.Empty;
            strSuggestedSiteDoorNumberID = string.Empty;
            strSuggestedSiteDoorNumber = string.Empty;
            if (!string.IsNullOrEmpty(hdnPreservSlot.Value))
            {
                string[] ArrrayPreserveValue = hdnPreservSlot.Value.Split('|');

                if (ddlFrom.Items.FindByValue(ArrrayPreserveValue[0]) != null)
                    ddlFrom.SelectedValue = ArrrayPreserveValue[0];
                if (ddlTo.Items.FindByValue(ArrrayPreserveValue[1]) != null)
                    ddlTo.SelectedValue = ArrrayPreserveValue[1];
                if (ddlDoorName.Items.FindByValue(ArrrayPreserveValue[2]) != null)
                    ddlDoorName.SelectedValue = ArrrayPreserveValue[2];
            }
            if (ddlAlternateWindow.Items.Count > 0)
                ddlAlternateWindow.SelectedIndex = 0;
            ddlAlternateWindow.Enabled = false;
            ltTimeSuggested_2.Text = string.Empty;

            ddlFrom.Enabled = true;
            ddlTo.Enabled = true;
            ddlDoorName.Enabled = true;
        }
    }

    protected void rdoStandardWindow_CheckedChanged(object sender, EventArgs e)
    {
        if (rdoStandardWindow.Checked)
        {
            rfvAlternateWindowRequired.Enabled = true;
            cmpvStartTimeCanNotBeGreaterEndTime.Enabled = false;
            rfvFromRequired.Enabled = false;
            rfvToRequired.Enabled = false;
            rfvDoorNameRequired.Enabled = false;

            ddlAlternateWindow.Enabled = true;

            ddlFrom.Enabled = false;
            ddlTo.Enabled = false;
            ddlDoorName.Enabled = false;
            ddlFrom.SelectedIndex = 0;
            ddlTo.SelectedIndex = 0;
            ddlDoorName.SelectedIndex = 0;
        }
    }

    private void ActivateFistView()
    {

        mvBookingEdit.ActiveViewIndex = 0;


        if (hdnActualScheduleDate.Value != string.Empty)
        {
            txtSchedulingdate.innerControltxtDate.Value = ActualSchedulDateTime.ToString("dd/MM/yyyy");  // Convert.ToDateTime(Convert.ToDateTime(hdnSchedulerdate.Value).ToString("dd/MM/yyyy"));            
        }

        if (hdnCurrentMode.Value == "Add" && hdnWindowSlotMode.Value == "false" && hdnCurrentRole.Value == "Carrier")
        {
            txtSchedulingdate.innerControltxtDate.Value = "";
        }

        if (hdnCurrentMode.Value == "Edit" && hdnCurrentRole.Value == "Carrier")
        {
            //spSCDate.Style.Add("display", "none");
            //ltSCdate.Visible = true;
            //ltSCdate.Text = ActualSchedulDateTime.ToString("dd/MM/yyyy");
        }


        if (GetQueryStringValue("SiteCountryID") != null)
        {
            if (GetQueryStringValue("SiteID").ToString() != "0")
                ddlSite.innerControlddlSite.SelectedIndex = ddlSite.innerControlddlSite.Items.IndexOf(ddlSite.innerControlddlSite.Items.FindByValue(GetQueryStringValue("SiteID").ToString()));
            else
                ddlSite.innerControlddlSite.SelectedIndex = ddlSite.innerControlddlSite.Items.IndexOf(ddlSite.innerControlddlSite.Items.FindByValue(Session["SiteID"].ToString()));
            ddlSite.CountryID = Convert.ToInt32(GetQueryStringValue("SiteCountryID").ToString());
        }
        else
        {
            ddlSite.innerControlddlSite.SelectedIndex = ddlSite.innerControlddlSite.Items.IndexOf(ddlSite.innerControlddlSite.Items.FindByValue(Session["SiteID"].ToString()));
            ddlSite.CountryID = Convert.ToInt32(Session["SiteCountryID"].ToString());
        }

        SiteSelectedIndexChanged();
        ddlSite.SelectedIndexChanged();
        if (hdnCurrentMode.Value == "Edit")
        {
            spSite.Style.Add("display", "none");
            ltSelectedSite.Visible = true;
            ltSelectedSite.Text = ddlSite.innerControlddlSite.SelectedItem.Text;
        }
        if (hdnBookingTypeID.Value == "5" && hdnProvisionalReason.Value == "EDITED" && hdnLine.Value != "0" && Session["Role"].ToString().ToLower() != "carrier")
        {
            btnProceedBooking.Visible = false;           
            btnAccept.Visible = true;
            btnRejectChanges.Visible = true;
            btnRejectBooking.Visible = true;
            lblVolumeDetailDesc.Visible = false;
            lblVolumeDetailDescProvisional.Visible = true;
        }
        else
        {
            btnProceedBooking.Visible = true;
            btnAccept.Visible = false;
            btnRejectChanges.Visible = false;
            btnRejectBooking.Visible = false;
            lblVolumeDetailDesc.Visible = true;
            lblVolumeDetailDescProvisional.Visible = false;
        }

    }

    public override void SitePost_Load()
    {
        base.SitePost_Load();
        MAS_SiteBE oMAS_SiteBE = ddlSite.oSiteBE;
        if (oMAS_SiteBE != null)
        {
            ViewState["oMAS_SiteBE"] = oMAS_SiteBE;
        }
    }

    public override void SiteSelectedIndexChanged()
    {
        base.SiteSelectedIndexChanged();
        if (ddlSite.innerControlddlSite.SelectedValue != "0" && !string.IsNullOrEmpty(ddlSite.innerControlddlSite.SelectedValue))
        {
            ISPM15PalletCheckingBAL iSPM15PalletCheckingBAL = new ISPM15PalletCheckingBAL();
            ISPM15PalletCheckingBE ispm15 = new ISPM15PalletCheckingBE();
            ispm15.Action = "GetISPM15BySiteID";
            ispm15.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedValue);
            pnlPalletCheckforCountry.Visible = iSPM15PalletCheckingBAL.IsSiteCountryExistsBAL(ispm15);
        }
        IsBookingValidationExcludedDelivery = false;

        BindVehicleType();        

        MAS_SiteBE oMAS_SiteBE = ddlSite.oSiteBE;
        if (oMAS_SiteBE == null)
        {
            BindDelivaryType();
            BindCarrier();
            oMAS_SiteBE = (MAS_SiteBE)ViewState["oMAS_SiteBE"];            
        }

        if (oMAS_SiteBE != null)
        {
            int CurrentSiteCountryID = Convert.ToInt32(oMAS_SiteBE.SiteCountryID);
            //---Pre Advise Note Required show/hide---//
            if (oMAS_SiteBE.IsPreAdviseNoteRequired == true)
            {
                trpreadvise.Visible = true;
                IsPreAdviseNoteRequired = true;
            }
            else
            {
                trpreadvise.Visible = false;
                IsPreAdviseNoteRequired = false;
            }
            //-------------------------------------//

            NonTimeDeliveryCartonVolume = oMAS_SiteBE.NonTimeDeliveryCartonVolume;
            NonTimeDeliveryPalletVolume = oMAS_SiteBE.NonTimeDeliveryPalletVolume;

            if (PreSiteCountryID != CurrentSiteCountryID)
            {

                BindDelivaryType();
                BindCarrier();
            }
            else
            {
                if (!Page.IsPostBack)
                {

                    BindDelivaryType();
                    BindCarrier();
                }
            }

            if (Convert.ToBoolean(isPostBack))
                PreSiteCountryID = CurrentSiteCountryID;
            else
                isPostBack = true;
        }
        else
        {
            BindDelivaryType();
            BindCarrier();
        }

        //if (Session["UserCarrierID"] != null) {
        //    if (ddlCarrier.SelectedIndex <= 0)
        //        ddlCarrier.SelectedIndex = ddlCarrier.Items.IndexOf(ddlCarrier.Items.FindByValue(Session["UserCarrierID"].ToString()));
        //}
        //CarrierValidationExc();
        DeliveryValidationExc();
        CarrierValidationExc();
        if (!GetToleratedDays())
        {

            GetMaximumCapacity();

            if (txtSchedulingdate.innerControltxtDate.Value != string.Empty && ddlSite.innerControlddlSite.SelectedItem.Value != string.Empty) {
                bool IsWeekConstraints = GetWeekConstraints();

                if (!IsWeekConstraints) {
                    string errMsg = WebCommon.getGlobalResourceValue("BK_MS_28_EXT"); // "No space available on any compatible doors.Please contact Goods In office or select a different date.";

                    if (hdnCurrentRole.Value == "Carrier") {
                        //Show err msg
                        ShowErrorMessage(errMsg, "BK_MS_28");
                        return ;
                    }
                    else {
                        //Show err msg
                        errMsg = WebCommon.getGlobalResourceValue("BK_MS_28_INT"); //"No space available on any compatible doors.";
                        ShowErrorMessage(errMsg, "BK_MS_28");
                        return ;
                    }
                }
            }
        }
        ShowWarningMessages();

        if (strCarrierValidationExc.Contains(ddlCarrier.SelectedItem.Value + ","))
        {
            IsBookingValidationExcludedCarrier = true;
        }
        else
        {
            IsBookingValidationExcludedCarrier = false;
        }

        //------Phase 15 R2 Point 21-----------//
        Session["CurrentSiteId"] = ddlSite.innerControlddlSite.SelectedValue;
        txtSchedulingdate.LoadDates();
        //------------------------------------//
        ShowHazordousItem();
    }

    private void BindVehicleType()
    {
        MASSIT_VehicleTypeBE oMASSIT_VehicleTypeBE = new MASSIT_VehicleTypeBE();
        APPSIT_VehicleTypeBAL oMASSIT_VehicleTypeBAL = new APPSIT_VehicleTypeBAL();

        oMASSIT_VehicleTypeBE.Action = "ShowAll";
        oMASSIT_VehicleTypeBE.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);

        List<MASSIT_VehicleTypeBE> lstVehicleType = oMASSIT_VehicleTypeBAL.GetVehicleTypeDetailsBAL(oMASSIT_VehicleTypeBE);

        if (lstVehicleType.Count > 0)
        {
            FillControls.FillDropDown(ref ddlVehicleType, lstVehicleType, "VehicleType", "VehicleTypeID", "--Select--");
        }

        VehicleNarrativeRequired = string.Empty;

        for (int iCount = 0; iCount < lstVehicleType.Count; iCount++)
        {
            if (lstVehicleType[iCount].IsNarrativeRequired) {
                if (VehicleNarrativeRequired == string.Empty)
                    VehicleNarrativeRequired = "," + lstVehicleType[iCount].VehicleTypeID.ToString() + ",";
                else
                    VehicleNarrativeRequired += lstVehicleType[iCount].VehicleTypeID.ToString() + ",";
            }

            //----Stage 10 Point 10 L 5.1-----//
            if (lstVehicleType[iCount].IsContainer) {
                if (IsVehicleContainer == string.Empty)
                    IsVehicleContainer = "," + lstVehicleType[iCount].VehicleTypeID.ToString() + ",";
                else
                    IsVehicleContainer += lstVehicleType[iCount].VehicleTypeID.ToString() + ",";
            }
            //---------------------------------//
        }

        //if (Convert.ToString(Session["Role"]).Trim().ToLower() == "carrier") 
        //{
        MAS_SiteBE singleSiteSetting = GetSingleSiteSetting(Convert.ToInt32(ddlSite.innerControlddlSite.SelectedValue));
        if (singleSiteSetting != null) {
            ddlVehicleType.SelectedIndex = ddlVehicleType.Items.IndexOf(ddlVehicleType.Items.FindByValue(Convert.ToString(singleSiteSetting.VehicleTypeId)));
            ddlVehicleType_SelectedIndexChanged(null, null);
        }
        //}
    }

    private void BindDelivaryType()
    {
        MAS_SiteBE singleSiteSetting = null;
        int countrySiteId = 0;
        if (!IsPostBack)
            if (GetQueryStringValue("SiteID") != null)
                singleSiteSetting = GetSingleSiteSetting(Convert.ToInt32(GetQueryStringValue("SiteID")));
            else
                singleSiteSetting = GetSingleSiteSetting(Convert.ToInt32(ddlSite.innerControlddlSite.SelectedValue));

        if (singleSiteSetting != null)
            countrySiteId = Convert.ToInt32(singleSiteSetting.SiteCountryID);

        MASCNT_DeliveryTypeBE oMASCNT_DeliveryTypeBE = new MASCNT_DeliveryTypeBE();
        APPCNT_DeliveryTypeBAL oMASCNT_DeliveryTypeBAL = new APPCNT_DeliveryTypeBAL();

        oMASCNT_DeliveryTypeBE.Action = "ShowAll";

        if (countrySiteId != 0)
            oMASCNT_DeliveryTypeBE.CountryID = countrySiteId;
        else
            oMASCNT_DeliveryTypeBE.CountryID = ddlSite.CountryID;

        oMASCNT_DeliveryTypeBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
        oMASCNT_DeliveryTypeBE.User.UserID = 0;
        //oMASCNT_DeliveryTypeBE.EnabledAsVendorOption = "1";
        List<MASCNT_DeliveryTypeBE> lstDeliveryType = oMASCNT_DeliveryTypeBAL.GetDeliveryTypeDetailsBAL(oMASCNT_DeliveryTypeBE);

        if (Session["Role"].ToString() == "Carrier")
        {
            lstDeliveryType = lstDeliveryType.FindAll(delegate(MASCNT_DeliveryTypeBE p) { return p.IsEnabledAsVendorOption == true; });
        }

        ddlDeliveryType.SelectedIndex = -1;
        if (lstDeliveryType.Count > 0)
        {
            FillControls.FillDropDown(ref ddlDeliveryType, lstDeliveryType, "DeliveryType", "DeliveryTypeID", "--Select--");
        }
        else
        {
            ddlDeliveryType.Items.Clear();
            ddlDeliveryType.Items.Add(new ListItem("---Select---", "0"));
        }

        //if (Convert.ToString(Session["Role"]).Trim().ToLower() == "carrier")
        //{
        if (singleSiteSetting != null) {
            ddlDeliveryType.SelectedIndex = ddlDeliveryType.Items.IndexOf(ddlDeliveryType.Items.FindByValue(Convert.ToString(singleSiteSetting.DeliveryTypeId)));
            ddlDeliveryType_SelectedIndexChanged(null, null);
        }
        //}
    }

    private void BindCarrier()
    {
        MAS_SiteBE singleSiteSetting = null;
        int countrySiteId = 0;
        if (!IsPostBack)
        {
            if (GetQueryStringValue("SiteID") != null)
                singleSiteSetting = GetSingleSiteSetting(Convert.ToInt32(GetQueryStringValue("SiteID")));
            else
                singleSiteSetting = GetSingleSiteSetting(Convert.ToInt32(ddlSite.innerControlddlSite.SelectedValue));
        }
        else
        {
            singleSiteSetting = GetSingleSiteSetting(Convert.ToInt32(ddlSite.innerControlddlSite.SelectedValue));
        }

        if (singleSiteSetting != null)
            countrySiteId = Convert.ToInt32(singleSiteSetting.SiteCountryID);

        MASCNT_CarrierBE oMASCNT_CarrierBE = new MASCNT_CarrierBE();
        APPCNT_CarrierBAL oMASCNT_CarrierBAL = new APPCNT_CarrierBAL();

        if (countrySiteId != 0)
            oMASCNT_CarrierBE.CountryID = countrySiteId;
        else
            oMASCNT_CarrierBE.CountryID = ddlSite.CountryID;

        oMASCNT_CarrierBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
        oMASCNT_CarrierBE.User.UserID = Convert.ToInt32(Session["UserID"]);

        if (Convert.ToString(Session["Role"]) == "Carrier")
        {
            oMASCNT_CarrierBE.Action = "GetUserCarrier";
        }
        else
        {
            oMASCNT_CarrierBE.Action = "ShowAll";
        }
        List<MASCNT_CarrierBE> lstCarrier = oMASCNT_CarrierBAL.GetCarrierDetailsBAL(oMASCNT_CarrierBE);

        //if (Convert.ToString(Session["Role"]) == "Carrier") {
        //lstCarrier = lstCarrier.FindAll(delegate(MASCNT_CarrierBE c) { return c.CarrierID == Convert.ToInt32(Session["UserCarrierID"]); });
        // }

        if (lstCarrier.Count > 0)
        {
            FillControls.FillDropDown(ref ddlCarrier, lstCarrier, "CarrierName", "CarrierID", "--Select--");
        }
        else
        {
            ddlCarrier.Items.Clear();
            ddlCarrier.Items.Insert(0, new ListItem("----Select----", "0"));
        }

        //if (Session["UserCarrierID"] != null) {
        //    if (ddlCarrier.SelectedIndex <= 0)
        //        ddlCarrier.SelectedIndex = ddlCarrier.Items.IndexOf(ddlCarrier.Items.FindByValue(Session["UserCarrierID"].ToString()));
        //}

        CarrierNarrativeRequired = ",";
        for (int iCount = 0; iCount < lstCarrier.Count; iCount++)
        {
            if (lstCarrier[iCount].IsNarrativeRequired)
            {
                CarrierNarrativeRequired += lstCarrier[iCount].CarrierID.ToString() + ",";
            }
        }

        if (oMASCNT_CarrierBE.Action.Trim().ToUpper() == "GETUSERCARRIER" && ddlCarrier.Items.Count > 1)
            ddlCarrier.SelectedIndex = 1;
    }

    private void DeliveryValidationExc()
    {
        DataTable dt1;

        MASSIT_BookingTypeExclusionBE oMASCNT_BookingTypeExclusionBE = new MASSIT_BookingTypeExclusionBE();
        APPSIT_BookingTypeExclusionBAL oMASCNT_BookingTypeExclusionBAL = new APPSIT_BookingTypeExclusionBAL();

        oMASCNT_BookingTypeExclusionBE.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
        oMASCNT_BookingTypeExclusionBE.Action = "GetDeliveries";
        dt1 = oMASCNT_BookingTypeExclusionBAL.GetBookingTypeExclusionDetailsBAL(oMASCNT_BookingTypeExclusionBE, "");

        strDeliveryValidationExc = string.Empty;

        for (int iCount = 0; iCount < dt1.Rows.Count; iCount++)
        {
            strDeliveryValidationExc += dt1.Rows[iCount]["SiteDeliveryID"].ToString() + ",";
        }

        //Validate Delivery Type--//
        if (ddlDeliveryType.SelectedIndex > 0 && strDeliveryValidationExc.Contains(ddlDeliveryType.SelectedItem.Value + ","))
        {
            IsBookingValidationExcludedDelivery = true;
        }
        //--------------------//
    }

    private bool GetToleratedDays()
    {

        bool isError = false;

        if (txtSchedulingdate.innerControltxtDate.Value != string.Empty)
        {
            MAS_SiteBE oMAS_SITEBE = new MAS_SiteBE();
            MAS_SiteBAL oMAS_SITEBAL = new MAS_SiteBAL();

            List<MAS_SiteBE> lstSites = new List<MAS_SiteBE>();

            oMAS_SITEBE.Action = "ShowAll";
            oMAS_SITEBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
            oMAS_SITEBE.User.UserID = Convert.ToInt32(Session["UserID"]);
            oMAS_SITEBE.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);

            lstSites = oMAS_SITEBAL.GetSiteMisSettingBAL(oMAS_SITEBE);

            if (lstSites != null && lstSites.Count > 0)
            {
                int? ActNoticePeriodDays = lstSites[0].BookingNoticePeriodInDays;

                int? NoticePeriodDays = lstSites[0].BookingNoticePeriodInDays;

                //--Stage 14 Point 16----//
                int SiteCloserDays = CheckSiteClosureByRange();
                if (SiteCloserDays > 0) {
                    if (NoticePeriodDays != null)
                        NoticePeriodDays = NoticePeriodDays + SiteCloserDays;
                    else
                        NoticePeriodDays = SiteCloserDays;
                }
                //---------------------------//

                DateTime SchDate = LogicalSchedulDateTime;
                DateTime CurrentDate = Utilities.Common.TextToDateFormat(DateTime.Now.Day.ToString() + "/" + DateTime.Now.Month.ToString() + "/" + DateTime.Now.Year.ToString());

                //TimeSpan DateDiff = SchDate - CurrentDate;
                //int DaysDiff = DateDiff.Days;
                //int DaysDiff = Enumerable.Range(0, Convert.ToInt32(SchDate.Subtract(CurrentDate).TotalDays)).Select(i => new[] { DayOfWeek.Saturday, DayOfWeek.Sunday }.Contains(CurrentDate.AddDays(i).DayOfWeek) ? 0 : 1).Sum();
                int DaysDiff = 0;
                if (SchDate >= CurrentDate) {
                    DaysDiff = Enumerable.Range(0, Convert.ToInt32(SchDate.Subtract(CurrentDate).TotalDays)).Select(i => new[] { DayOfWeek.Saturday, DayOfWeek.Sunday }.Contains(CurrentDate.AddDays(i).DayOfWeek) ? 0 : 1).Sum();
                }


                //string errMsg = WebCommon.getGlobalResourceValue("BK_MS_05_EXT_1") + " " + NoticePeriodDays + " " + WebCommon.getGlobalResourceValue("BK_MS_05_EXT_2");
                // "We required " + NoticePeriodDays + " days notification for all deliveries scheduled  into our sites, therefore we cannot accept this booking on the date you have requested. please select another date.";

                //---Stage 4 Point 4---
                //All deliveries scheduled into this site must be booked before 12:00 and 1 working day(s) before delivery, therefore we cannot accept this booking on the date you have requested. Please select another date or contact the site's booking in department.
                string errMsg = string.Empty;
                if (NoticePeriodDays == null || NoticePeriodDays == 0) {
                    if (lstSites[0].BookingNoticeTime != null) {
                        string NoticeTime = Common.FormatTime(lstSites[0].BookingNoticeTime.Value.Hour.ToString()) + ":" + Common.FormatTime(lstSites[0].BookingNoticeTime.Value.Minute.ToString());
                        errMsg = WebCommon.getGlobalResourceValue("BK_MS_05_EXT_NEW_1") + " " +
                            WebCommon.getGlobalResourceValue("BK_MS_05_EXT_NEW_2").Replace("##Time##", NoticeTime) + " " +
                            WebCommon.getGlobalResourceValue("BK_MS_05_EXT_NEW_3").Replace("##NoticePeriodInDays##", "1");
                    }

                }
                //Commeted and chnages as per anthony mail 13 Sep 2013 
                //else if (NoticePeriodDays == 1) {
                //    if (lstSites[0].BookingNoticeTime != null) {
                //        string NoticeTime = Common.FormatTime(lstSites[0].BookingNoticeTime.Value.Hour.ToString()) + ":" + Common.FormatTime(lstSites[0].BookingNoticeTime.Value.Minute.ToString());
                //        errMsg = WebCommon.getGlobalResourceValue("BK_MS_05_EXT_NEW_1") + " " +
                //            WebCommon.getGlobalResourceValue("BK_MS_05_EXT_NEW_2").Replace("##Time##", NoticeTime) + " " +
                //            WebCommon.getGlobalResourceValue("BK_MS_05_EXT_NEW_3").Replace("##NoticePeriodInDays##", "1");
                //    }
                //    else if (lstSites[0].BookingNoticeTime == null) {
                //        errMsg = WebCommon.getGlobalResourceValue("BK_MS_05_EXT_NEW_1") + " " +
                //            WebCommon.getGlobalResourceValue("BK_MS_05_EXT_NEW_3").Replace("##NoticePeriodInDays##", "2");
                //    }
                //}
                else {
                    if (lstSites[0].BookingNoticeTime != null) {
                        string NoticeTime = Common.FormatTime(lstSites[0].BookingNoticeTime.Value.Hour.ToString()) + ":" + Common.FormatTime(lstSites[0].BookingNoticeTime.Value.Minute.ToString());
                        errMsg = WebCommon.getGlobalResourceValue("BK_MS_05_EXT_NEW_1") + " " +
                            WebCommon.getGlobalResourceValue("BK_MS_05_EXT_NEW_2").Replace("##Time##", NoticeTime) + " " +
                            WebCommon.getGlobalResourceValue("BK_MS_05_EXT_NEW_3").Replace("##NoticePeriodInDays##", (ActNoticePeriodDays).ToString());
                    }
                    else {
                        errMsg = WebCommon.getGlobalResourceValue("BK_MS_05_EXT_NEW_1") + " " +
                            WebCommon.getGlobalResourceValue("BK_MS_05_EXT_NEW_3").Replace("##NoticePeriodInDays##", (ActNoticePeriodDays).ToString());
                    }
                }
                //-----------------------//

                bool isAllowed = false;

                if (NoticePeriodDays == null) {
                    if (DaysDiff == 0) {
                        DateTime dt1, dt2;
                        dt1 = new DateTime(1900, 01, 01, DateTime.Now.Hour, DateTime.Now.Minute, 00);
                        if (lstSites[0].BookingNoticeTime != null)
                            dt2 = new DateTime(1900, 01, 01, lstSites[0].BookingNoticeTime.Value.Hour, lstSites[0].BookingNoticeTime.Value.Minute, 00);
                        else
                            dt2 = new DateTime(1900, 01, 01, DateTime.Now.Hour, DateTime.Now.Minute, 00);

                        if (dt1 <= dt2) {
                            isAllowed = true;
                        }
                    }
                    else {
                        isAllowed = true;
                    }
                }

                if (isAllowed == false && DaysDiff >= NoticePeriodDays + 1)
                    isAllowed = true;

                if (isAllowed == false && DaysDiff == NoticePeriodDays) {
                    DateTime dt1, dt2;
                    dt1 = new DateTime(1900, 01, 01, DateTime.Now.Hour, DateTime.Now.Minute, 00);
                    if (lstSites[0].BookingNoticeTime != null)
                        dt2 = new DateTime(1900, 01, 01, lstSites[0].BookingNoticeTime.Value.Hour, lstSites[0].BookingNoticeTime.Value.Minute, 00);
                    else
                        dt2 = new DateTime(1900, 01, 01, DateTime.Now.Hour, DateTime.Now.Minute, 00);

                    if (dt1 <= dt2) {
                        isAllowed = true;
                    }
                }

                if (isAllowed == false && NoticePeriodDays == 0 && SchDate > CurrentDate) {
                    isAllowed = true;
                }   

                if (!isAllowed)
                {
                    //error
                    if (hdnCurrentRole.Value == "Vendor" || hdnCurrentRole.Value == "Carrier")
                    {
                        if (hdnCurrentMode.Value == "Add")
                        {
                            ShowErrorMessage(errMsg, "BK_MS_05_EXT");
                            return true;
                        }
                    }
                    else
                    {
                        errMsg = WebCommon.getGlobalResourceValue("BK_MS_05_INT"); // "There is insufficient notice given for this booking. Do you want to continue?";
                        AddWarningMessages(errMsg, "BK_MS_05_INT");
                    }
                }

                //if (NoticePeriodDays > 0 && DaysDiff < NoticePeriodDays) {
                //    //error         

                //    if (Session["Role"].ToString() == "Vendor" || Session["Role"].ToString() == "Carrier") {
                //        ShowErrorMessage(errMsg, "BK_MS_05_EXT");
                //        return true;
                //    }
                //    else {
                //        errMsg = WebCommon.getGlobalResourceValue("BK_MS_05_INT"); // "There is insufficient notice given for this booking. Do you want to continue?";
                //        AddWarningMessages(errMsg, "BK_MS_05_INT");
                //    }
                //}

                //if (NoticePeriodDays >= 0 && DaysDiff == NoticePeriodDays) {
                //    DateTime dt1, dt2;
                //    dt1 = new DateTime(1900, 01, 01, DateTime.Now.Hour, DateTime.Now.Minute, 00);
                //    if (lstSites[0].BookingNoticeTime != null)
                //        dt2 = new DateTime(1900, 01, 01, lstSites[0].BookingNoticeTime.Value.Hour, lstSites[0].BookingNoticeTime.Value.Minute, 00);
                //    else
                //        dt2 = new DateTime(1900, 01, 01, DateTime.Now.Hour, DateTime.Now.Minute, 00);

                //    if (dt1 > dt2) {
                //        //error
                //        if (Session["Role"].ToString() == "Vendor" || Session["Role"].ToString() == "Carrier") {
                //            ShowErrorMessage(errMsg, "BK_MS_05_EXT");
                //            return true;
                //        }
                //        else {
                //            errMsg = WebCommon.getGlobalResourceValue("BK_MS_05_INT"); //"There is insufficient notice given for this booking. Do you want to continue?";
                //            AddWarningMessages(errMsg, "BK_MS_05_INT");
                //        }

                //    }
                //}
            }

            ScriptManager.RegisterStartupScript(Page, this.GetType(), "id", "$get('" + btnProceedAddPO.ClientID + "').style.display='';", true);
        }
        return isError;
    }

    //--Stage 14 Point 16----//
    private int CheckSiteClosureByRange() {
        int iSiteClosureDays = 0;

        if (txtSchedulingdate.innerControltxtDate.Value != string.Empty && ddlSite.innerControlddlSite.SelectedItem.Value != string.Empty) {

            MASSIT_HolidayBE oMASSIT_HolidayBE = new MASSIT_HolidayBE();
            MASSIT_HolidayBAL oMASSIT_HolidayBAL = new MASSIT_HolidayBAL();

            oMASSIT_HolidayBE.Action = "ShowAllByRange";
            oMASSIT_HolidayBE.Site = new BusinessEntities.ModuleBE.AdminFunctions.MAS_SiteBE();
            if (ddlSite.innerControlddlSite.SelectedIndex >= 0)
                oMASSIT_HolidayBE.Site.SiteID = !string.IsNullOrEmpty(ddlSite.innerControlddlSite.SelectedValue) ? Convert.ToInt32(ddlSite.innerControlddlSite.SelectedValue) : 0;

            oMASSIT_HolidayBE.HolidayDate = LogicalSchedulDateTime;

            List<MASSIT_HolidayBE> lstHoliday = oMASSIT_HolidayBAL.GetHolidaysBAL(oMASSIT_HolidayBE);

            if (lstHoliday != null && lstHoliday.Count > 0) {
                iSiteClosureDays = lstHoliday.Count;
            }
        }
        return iSiteClosureDays;
    }
    //---------------------//
    private void GetMaximumCapacity()
    {
        if (txtSchedulingdate.innerControltxtDate.Value != string.Empty)
        {
            MASSIT_WeekSetupBE oMASSIT_WeekSetupBE = new MASSIT_WeekSetupBE();
            MASSIT_WeekSetupBAL oMASSIT_WeekSetupBAL = new MASSIT_WeekSetupBAL();

            oMASSIT_WeekSetupBE.Action = "GetSpecificWeek";
            oMASSIT_WeekSetupBE.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
            oMASSIT_WeekSetupBE.EndWeekday = LogicalSchedulDateTime.DayOfWeek.ToString();
            oMASSIT_WeekSetupBE.ScheduleDate = LogicalSchedulDateTime;

            List<MASSIT_WeekSetupBE> lstWeekSetup = oMASSIT_WeekSetupBAL.GetWeekSetupDetailsBAL(oMASSIT_WeekSetupBE);

            if (lstWeekSetup == null || lstWeekSetup.Count <= 0)
            {
                oMASSIT_WeekSetupBE.Action = "ShowAll";
                oMASSIT_WeekSetupBE.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
                oMASSIT_WeekSetupBE.EndWeekday = LogicalSchedulDateTime.DayOfWeek.ToString();
                //oMASSIT_WeekSetupBE.ScheduleDate = DateTime.Now.Date;
                lstWeekSetup = oMASSIT_WeekSetupBAL.GetWeekSetupDetailsBAL(oMASSIT_WeekSetupBE);
            }

            string errMsg;

            if (lstWeekSetup != null && lstWeekSetup.Count > 0)
            {

                //Set Current Running Total of alternate screen
                //ltMaximumLifts.Text = lstWeekSetup[0].MaximumLift != (int?)null ? lstWeekSetup[0].MaximumLift.ToString() : "-";
                //ltMaximumPallets.Text = lstWeekSetup[0].MaximumPallet != (int?)null ? lstWeekSetup[0].MaximumPallet.ToString() : "-";
                //ltMaximumLines.Text = lstWeekSetup[0].MaximumLine != (int?)null ? lstWeekSetup[0].MaximumLine.ToString() : "-";
                //-----------------------------------//

                APPBOK_BookingBE oAPPBOK_BookingBE = new APPBOK_BookingBE();
                APPBOK_BookingBAL oAPPBOK_BookingBAL = new APPBOK_BookingBAL();

                oAPPBOK_BookingBE.Action = "GetBookedVolume";
                oAPPBOK_BookingBE.SiteId = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
                oAPPBOK_BookingBE.ScheduleDate = LogicalSchedulDateTime;
                oAPPBOK_BookingBE.BookingType = "W";

                //----Stage 10 Point 10 L 5.1-----//
                if (hdnCurrentMode.Value == "Edit")
                    oAPPBOK_BookingBE.BookingID = Convert.ToInt32(GetQueryStringValue("ID").ToString());
                //---------------------------------//

                List<APPBOK_BookingBE> lstBookedVolume = oAPPBOK_BookingBAL.GetBookedVolumeBAL(oAPPBOK_BookingBE);

                if (lstBookedVolume != null && lstBookedVolume.Count > 0)
                {

                    //Set Current Running Total of alternate screen//
                    //ltRemainingLifts.Text = lstWeekSetup[0].MaximumLift != (int?)null ? (lstWeekSetup[0].MaximumLift - lstBookedVolume[0].NumberOfLift).ToString() : "-";
                    //ltRemainingPallets.Text = lstWeekSetup[0].MaximumPallet != (int?)null ? (lstWeekSetup[0].MaximumPallet - lstBookedVolume[0].NumberOfPallet).ToString() : "-";
                    //ltRemainingLines.Text = lstWeekSetup[0].MaximumLine != (int?)null ? (lstWeekSetup[0].MaximumLine - lstBookedVolume[0].NumberOfLines).ToString() : "-";

                    //ltRemainingLifts.Text = lstBookedVolume[0].NumberOfLift.ToString();
                    //ltRemainingPallets.Text = lstBookedVolume[0].NumberOfPallet.ToString();
                    //ltRemainingLines.Text = lstBookedVolume[0].NumberOfLines.ToString() ;
                    hdnRemainingPallets.Value = lstWeekSetup[0].MaximumPallet != (int?)null && (lstWeekSetup[0].MaximumPallet - lstBookedVolume[0].NumberOfPallet) > 0 ? (lstWeekSetup[0].MaximumPallet - lstBookedVolume[0].NumberOfPallet).ToString() : "-";
                    hdnRemainingLines.Value = lstWeekSetup[0].MaximumLine != (int?)null && (lstWeekSetup[0].MaximumLine - lstBookedVolume[0].NumberOfLines) > 0 ? (lstWeekSetup[0].MaximumLine - lstBookedVolume[0].NumberOfLines).ToString() : "-";
                    //----Stage 10 Point 10 L 5.1-----//
                    //RemainingCartons = lstWeekSetup[0].MaximumContainer != (int?)null ? (lstWeekSetup[0].MaximumContainer - lstBookedVolume[0].NumberOfContainer).ToString() : "-";
                    //---------------------------------//

                    RemainingPallets = lstWeekSetup[0].MaximumPallet != (int?)null ? Convert.ToInt32(lstWeekSetup[0].MaximumPallet) - Convert.ToInt32(lstBookedVolume[0].NumberOfPallet) : (int?)null;
                    RemainingLines = lstWeekSetup[0].MaximumLine != (int?)null ? Convert.ToInt32(lstWeekSetup[0].MaximumLine) - Convert.ToInt32(lstBookedVolume[0].NumberOfLines) : (int?)null;
                    
                    //-----------------------------------//
                    //if not the confirm fixed slot case

                    

                    if (strSuggestedWindowSlotID == "-1" || strSuggestedWindowSlotID == string.Empty)
                    {
                        if (lstBookedVolume[0].NumberOfLines >= (lstWeekSetup[0].MaximumLine > 0 ? lstWeekSetup[0].MaximumLine : 99999)
                            || lstBookedVolume[0].NumberOfPallet >= (lstWeekSetup[0].MaximumPallet > 0 ? lstWeekSetup[0].MaximumPallet : 99999)
                            || lstBookedVolume[0].NumberOfDeliveries >= (lstWeekSetup[0].MaximumDeliveries > 0 ? lstWeekSetup[0].MaximumDeliveries : 99999))
                        {

                            //error
                            if (hdnCurrentRole.Value == "Vendor" || hdnCurrentRole.Value == "Carrier")
                            {
                                if (hdnCurrentMode.Value == "Add")
                                {
                                    errMsg = WebCommon.getGlobalResourceValue("BK_MS_06_EXT"); // "There is no capacity for any further deliveries on the date selected. Please select a different date or, if the delivery is urgent, please contact the booking office.";
                                    ShowErrorMessage(errMsg, "BK_MS_06_EXT");
                                    return;
                                }
                            }
                            else
                            {
                                if (!(hdnCurrentMode.Value == "Edit" && GetQueryStringValue("BP") != null && GetQueryStringValue("BP").ToString() == "Pro")) {  // If coming from Provisional page ,ignore capacity warning -- Phase 12 R2 Point 2
                                    errMsg = WebCommon.getGlobalResourceValue("BK_MS_06_INT"); // "There is no capacity for any further deliveries on the date selected. Click CONTINUE to make booking on this date.";
                                    AddWarningMessages(errMsg, "BK_MS_06_INT");
                                }
                            }
                        }


                        //-----------Point 10L-5.1---------------//                        

                        if (lstBookedVolume[0].NumberOfContainer >= (lstWeekSetup[0].MaximumContainer > 0 ? lstWeekSetup[0].MaximumContainer : 99999)) {
                            if (ddlVehicleType.SelectedIndex > -1) {
                                if (IsVehicleContainer.Contains("," + ddlVehicleType.SelectedItem.Value + ",")) {
                                    if (hdnCurrentRole.Value == "Vendor" || hdnCurrentRole.Value == "Carrier") {
                                        //if (hdnCurrentMode.Value == "Add") {
                                            errMsg = WebCommon.getGlobalResourceValue("BK_MS_06_Ext_MaxContainers"); // The maximum amount of containers for this date has been reached.....
                                            ShowErrorMessage(errMsg, "BK_MS_06_Ext_MaxContainers");
                                            return;
                                        //}
                                    }
                                    else {
                                        errMsg = WebCommon.getGlobalResourceValue("BK_MS_06_INT_MaxContainers"); //The maximum amount of containers for this date has been reached. Do you want to continue?
                                        AddWarningMessages(errMsg, "BK_MS_06_INT_MaxContainers");
                                    }
                                }
                            }
                            isContainerError = true;
                        }
                        //----------------------------------------//
                    }
                }
            }
            ScriptManager.RegisterStartupScript(Page, this.GetType(), "id", "$get('" + btnProceedAddPO.ClientID + "').style.display='';", true);
        }
    }

    protected void ddlCarrier_SelectedIndexChanged(object sender, EventArgs e)
    {
        txtCarrierOther.Text = string.Empty;

        //if (Session["UserCarrierID"] != null) {
        //    if (ddlCarrier.SelectedIndex <= 0)
        //        ddlCarrier.SelectedIndex = ddlCarrier.Items.IndexOf(ddlCarrier.Items.FindByValue(Session["UserCarrierID"].ToString()));
        //}

        if (ddlCarrier.Items.Count > 0)
        {
            CarrierValidationExc();
            rfvCarrierOtherRequired.Enabled = false;

            if (CarrierNarrativeRequired.Contains("," + ddlCarrier.SelectedItem.Value + ","))
            {
                txtCarrierOther.Style.Add("display", "");
                txtCarrierOther.Visible = true;
                rfvCarrierOtherRequired.Enabled = true;
            }
            else
            {
                txtCarrierOther.Style.Add("display", "none");
                txtCarrierOther.Visible = false;
                rfvCarrierOtherRequired.Enabled = false;
            }


            if (strCarrierValidationExc.Contains(ddlCarrier.SelectedItem.Value + ","))
            {
                IsBookingValidationExcludedCarrier = true;
            }
            else
            {
                IsBookingValidationExcludedCarrier = false;
            }
        }
        else
        {
            txtCarrierOther.Style.Add("display", "none");
            rfvCarrierOtherRequired.Enabled = false;
            IsBookingValidationExcludedCarrier = false;
        }
    }

    protected void ddlVehicleType_SelectedIndexChanged(object sender, EventArgs e)
    {
        txtVehicleOther.Text = string.Empty;
        //-----------Point 10L-5.1------------//
        string errMsg = string.Empty;
        if (ddlVehicleType.SelectedIndex > -1 && !string.IsNullOrEmpty(txtSchedulingdate.innerControltxtDate.Value)) {
            if (IsVehicleContainer.Contains("," + ddlVehicleType.SelectedItem.Value + ",")) {

                if (isContainerError == true) {
                    if (hdnCurrentRole.Value == "Vendor" || hdnCurrentRole.Value == "Carrier") {
                        //if (hdnCurrentMode.Value == "Add") {
                            errMsg = WebCommon.getGlobalResourceValue("BK_MS_06_Ext_MaxContainers"); // The maximum amount of containers for this date has been reached.....
                            ShowErrorMessage(errMsg, "BK_MS_06_Ext_MaxContainers");
                            return;
                        //}
                    }
                    else {
                        errMsg = WebCommon.getGlobalResourceValue("BK_MS_06_INT_MaxContainers"); //The maximum amount of containers for this date has been reached. Do you want to continue?
                        AddWarningMessages(errMsg, "BK_MS_06_INT_MaxContainers");
                    }
                }
            }
        }

        if (!string.IsNullOrEmpty(errMsg))
            ShowWarningMessages();
        else
            VehicleTypeSelect();

        //------------------------------------------//   

 
    }

    private void VehicleTypeSelect() {

        if (ddlVehicleType.SelectedIndex > -1) {
            rfvVehicleTypeOtherRequired.Enabled = false;
            if (VehicleNarrativeRequired.Contains(ddlVehicleType.SelectedItem.Value + ",")) {
                txtVehicleOther.Style.Add("display", "");
                txtVehicleOther.Visible = true;
                rfvVehicleTypeOtherRequired.Enabled = true;
            }
            else {
                txtVehicleOther.Style.Add("display", "none");
                txtVehicleOther.Visible = false;
                rfvVehicleTypeOtherRequired.Enabled = false;
            }

            //Get Door Type as per selected Vehicle Type
            strVehicleDoorTypeMatrix = string.Empty;
            MASSIT_VehicleDoorMatrixBE oMASSIT_VehicleDoorMatrixBE = new MASSIT_VehicleDoorMatrixBE();

            APPSIT_VehicleDoorMatrixBAL oMASSIT_VehicleDoorMatrixBAL = new APPSIT_VehicleDoorMatrixBAL();

            oMASSIT_VehicleDoorMatrixBE.Action = "ShowAll";
            oMASSIT_VehicleDoorMatrixBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
            oMASSIT_VehicleDoorMatrixBE.User.UserID = Convert.ToInt32(Session["UserID"]);
            oMASSIT_VehicleDoorMatrixBE.SiteID = Convert.ToInt16(ddlSite.innerControlddlSite.SelectedItem.Value);

            List<MASSIT_VehicleDoorMatrixBE> lstVehicleDoorMatrix = oMASSIT_VehicleDoorMatrixBAL.GetVehicleDoorMatrixDetailsBAL(oMASSIT_VehicleDoorMatrixBE);

            //---FOR SKU DOOR MATRIX, NEED TO SKIP VEHICLE DOOR MATIX CONSTRAINTS--- SPRINT 4 POINT 7
            NameValueCollection collection = new NameValueCollection();

            for (int iCount = 0; iCount < lstVehicleDoorMatrix.Count; iCount++) {
                strVehicleDoorTypeMatrixSKU += lstVehicleDoorMatrix[iCount].DoorTypeID.ToString() + ",";
                collection.Add(lstVehicleDoorMatrix[iCount].DoorTypeID.ToString(), lstVehicleDoorMatrix[iCount].Priority.ToString());
            }

            ArrDoorTypePrioritySKU = new NameValueCollection();
            foreach (string key in collection.AllKeys) {
                string Val = collection.Get(key);
                string[] ArrVal = Val.Split(',');
                ArrDoorTypePrioritySKU.Add(key, ArrVal[0].ToString());
            }
            //---------------------------------------------------------------------------------------

            //Get Door Type as per selected Vehicle Type 
            lstVehicleDoorMatrix = lstVehicleDoorMatrix.Where(Matrix => Matrix.VehicleTypeID == Convert.ToInt32(ddlVehicleType.SelectedItem.Value)).ToList();

            collection = new NameValueCollection();

            for (int iCount = 0; iCount < lstVehicleDoorMatrix.Count; iCount++) {
                strVehicleDoorTypeMatrix += lstVehicleDoorMatrix[iCount].DoorTypeID.ToString() + ",";
                collection.Add(lstVehicleDoorMatrix[iCount].DoorTypeID.ToString(), lstVehicleDoorMatrix[iCount].Priority.ToString());
            }

            ArrDoorTypePriority = new NameValueCollection();
            foreach (string key in collection.AllKeys) {
                string Val = collection.Get(key);
                string[] ArrVal = Val.Split(',');
                ArrDoorTypePriority.Add(key, ArrVal[0].ToString());
            }
        }
        //-----------------//  
    }

    protected void ddlDeliveryType_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (strDeliveryValidationExc.Contains(ddlDeliveryType.SelectedItem.Value + ","))
        {
            IsBookingValidationExcludedDelivery = true;
        }
        else
        {
            IsBookingValidationExcludedDelivery = false;
        }
    }

    private bool CarrierValidationExc() {

        bool bCarrierValidation = true;

        if (ddlSite.innerControlddlSite.SelectedIndex > -1)
        {
            DataTable dt1;

            MASSIT_BookingTypeExclusionBE oMASCNT_BookingTypeExclusionBE = new MASSIT_BookingTypeExclusionBE();
            APPSIT_BookingTypeExclusionBAL oMASCNT_BookingTypeExclusionBAL = new APPSIT_BookingTypeExclusionBAL();

            oMASCNT_BookingTypeExclusionBE.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
            oMASCNT_BookingTypeExclusionBE.Action = "GetCarriers";
            dt1 = oMASCNT_BookingTypeExclusionBAL.GetBookingTypeExclusionDetailsBAL(oMASCNT_BookingTypeExclusionBE, "");

            strCarrierValidationExc = string.Empty;

            for (int iCount = 0; iCount < dt1.Rows.Count; iCount++)
            {
                strCarrierValidationExc += dt1.Rows[iCount]["SiteCarrierID"].ToString() + ",";
            }

            //Carrier Micellaneous Constraints -- Sprint 3a -----//
            BeforeSlotTimeID = "0";
            AfterSlotTimeID = "300";
            string errMsg = string.Empty;

            MASCNT_CarrierBE oMASCNT_CarrierBE = new MASCNT_CarrierBE();
            APPCNT_CarrierBAL oAPPCNT_CarrierBAL = new APPCNT_CarrierBAL();

            if (ddlCarrier.SelectedIndex > 0)
            {
                oMASCNT_CarrierBE.Action = "GetAllCarrierMiscCons";
                oMASCNT_CarrierBE.CarrierID = Convert.ToInt32(ddlCarrier.SelectedItem.Value);
                oMASCNT_CarrierBE.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
                List<MASCNT_CarrierBE> lstCarrierBE = oAPPCNT_CarrierBAL.GetAllCarrierMiscConsBAL(oMASCNT_CarrierBE);

                ViewState.Add("lstCarrier", lstCarrierBE.ToArray());

                if (lstCarrierBE != null && lstCarrierBE.Count > 0)
                {

                    bool AllowedDay = true;

                    if (txtSchedulingdate.innerControltxtDate.Value != string.Empty)
                    {
                        string Weekday = LogicalSchedulDateTime.DayOfWeek.ToString().ToUpper();

                        switch (Weekday)
                        {
                            case "SUNDAY":
                                if (lstCarrierBE[0].APP_CannotDeliveryOnSunday == true)
                                    AllowedDay = false;
                                break;
                            case "MONDAY":
                                if (lstCarrierBE[0].APP_CannotDeliveryOnMonday == true)
                                    AllowedDay = false;
                                break;
                            case "TUESDAY":
                                if (lstCarrierBE[0].APP_CannotDeliveryOnTuesday == true)
                                    AllowedDay = false;
                                break;
                            case "WEDNESDAY":
                                if (lstCarrierBE[0].APP_CannotDeliveryOnWednessday == true)
                                    AllowedDay = false;
                                break;
                            case "THURSDAY":
                                if (lstCarrierBE[0].APP_CannotDeliveryOnThrusday == true)
                                    AllowedDay = false;
                                break;
                            case "FRIDAY":
                                if (lstCarrierBE[0].APP_CannotDeliveryOnFriday == true)
                                    AllowedDay = false;
                                break;
                            case "SATURDAY":
                                if (lstCarrierBE[0].APP_CannotDeliveryOnSaturday == true)
                                    AllowedDay = false;
                                break;
                        }

                        if (AllowedDay == false)
                        {
                            bCarrierValidation = false;
                            //error               
                            if (hdnCurrentRole.Value == "Carrier")
                            {
                                errMsg = WebCommon.getGlobalResourceValue("BK_MS_04_EXT");  // "This date is not available for your booking. Please select another date or contact Office Depot to make booking on this date.";
                                ShowErrorMessage(errMsg, "BK_MS_04_EXT");
                            }
                            else
                            {
                                errMsg = WebCommon.getGlobalResourceValue("BK_MS_04_INT_Carrier"); // "There is a constraint against the vendor to deliver on this date, do you want to continue?";
                                AddWarningMessages(errMsg, "BK_MS_04_INT_Carrier");
                            }
                        }


                        //Check Maximum Deliveries In A Day
                        if (lstCarrierBE[0].APP_MaximumDeliveriesInADay != null && lstCarrierBE[0].APP_MaximumDeliveriesInADay > 0)
                        {

                            APPBOK_BookingBE oAPPBOK_BookingBE = new APPBOK_BookingBE();
                            APPBOK_BookingBAL oAPPBOK_BookingBAL = new APPBOK_BookingBAL();

                            oAPPBOK_BookingBE.Action = "CheckCarrierBooking";
                            oAPPBOK_BookingBE.SiteId = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
                            oAPPBOK_BookingBE.ScheduleDate = LogicalSchedulDateTime;
                            oAPPBOK_BookingBE.Carrier = new MASCNT_CarrierBE();
                            oAPPBOK_BookingBE.Carrier.CarrierID = Convert.ToInt32(ddlCarrier.SelectedItem.Value);

                            int? BookingDeatils = oAPPBOK_BookingBAL.CheckVendorBookingBAL(oAPPBOK_BookingBE);

                            if (BookingDeatils >= lstCarrierBE[0].APP_MaximumDeliveriesInADay)
                            {
                                bCarrierValidation = false;
                                if (hdnCurrentRole.Value == "Carrier")
                                {
                                    errMsg = WebCommon.getGlobalResourceValue("BK_MS_27_EXT"); // +" " + BookingDeatils.ToString() + ")."; // "Your maximum number of deliveries per day has already been reached (Max - " + BookingDeatils.ToString() + ").";
                                    ShowErrorMessage(errMsg, "BK_MS_27_EXT");
                                }
                                else
                                {
                                    errMsg = WebCommon.getGlobalResourceValue("BK_MS_27_INT_Carrier") + " " + BookingDeatils.ToString() + ")." + WebCommon.getGlobalResourceValue("BK_MS_27_INT_2");  //"Maximum number of deliveries per day has been reached for this carrier (Max - " + BookingDeatils.ToString() + "). Do you want to continue?";
                                    AddWarningMessages(errMsg, "BK_MS_27_INT");
                                }
                            }
                        }
                    }

                    //Befor Time and After Time               
                    BeforeSlotTimeID = lstCarrierBE[0].BeforeOrderBYId.ToString();
                    AfterSlotTimeID = lstCarrierBE[0].AfterOrderBYId.ToString() != "0" ? lstCarrierBE[0].AfterOrderBYId.ToString() : "300";

                    ShowWarningMessages();
                }
                //---------------------------------------------------//
            }
        }
        return bCarrierValidation;
    }

    //--Stage 5 Point 1----//   
    private bool VendorValidationExc(string ucSeacrhVendorID) {

        bool bVendorValidation = true;

        string VendorNo = ucSeacrhVendorID; // ucSeacrhVendorNo; // ((ucTextbox)ucSeacrhVendor1.FindControl("txtVendorNo")).Text;

        if (ddlSite.IsBookingExclusions) {
            if (VendorNo != string.Empty) {
                UP_VendorBAL oUP_VendorBAL = new UP_VendorBAL();
                UP_VendorBE oUP_VendorBE = new UP_VendorBE();

                oUP_VendorBE.Action = "ValidateVendorExc";
                oUP_VendorBE.Vendor_No = VendorNo.Trim();
                oUP_VendorBE.Site = new BusinessEntities.ModuleBE.AdminFunctions.MAS_SiteBE();
                oUP_VendorBE.Site.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);

                List<UP_VendorBE> lstUPVendor = oUP_VendorBAL.ValidateVendorBAL(oUP_VendorBE);

                //BeforeSlotTimeID = string.Empty;
                //AfterSlotTimeID = string.Empty;
                string errMsg = string.Empty;

                if (lstUPVendor.Count > 0) {

                    lstUPVendor = lstUPVendor.FindAll(delegate(UP_VendorBE ven) {
                        return ven.VendorDetails.IsConstraintsDefined == true;
                    });
                }

                if (lstUPVendor.Count > 0) {

                    List<UP_VendorBE> lstUPVendors = null;
                    UP_VendorBE[] newArray = null;

                    if (ViewState["UPVendors"] != null) {
                        newArray = (UP_VendorBE[])ViewState["UPVendors"];
                        lstUPVendors = new List<UP_VendorBE>(newArray);
                        if (lstUPVendors.FindAll(delegate(UP_VendorBE v) { return v.VendorID == lstUPVendor[0].VendorID; }).Count == 0) {
                            lstUPVendors.AddRange(lstUPVendor);
                            ViewState.Add("UPVendors", lstUPVendors.ToArray());
                        }
                    }
                    else {
                        ViewState.Add("UPVendors", lstUPVendor.ToArray());
                    }

                    bool AllowedDay = true;

                    if (txtSchedulingdate.innerControltxtDate.Value != string.Empty) {
                        string Weekday = LogicalSchedulDateTime.DayOfWeek.ToString().ToUpper();

                        switch (Weekday) {
                            case "SUNDAY":
                                if (lstUPVendor[0].VendorDetails.APP_CannotDeliveryOnSunday == true)
                                    AllowedDay = false;
                                break;
                            case "MONDAY":
                                if (lstUPVendor[0].VendorDetails.APP_CannotDeliveryOnMonday == true)
                                    AllowedDay = false;
                                break;
                            case "TUESDAY":
                                if (lstUPVendor[0].VendorDetails.APP_CannotDeliveryOnTuesday == true)
                                    AllowedDay = false;
                                break;
                            case "WEDNESDAY":
                                if (lstUPVendor[0].VendorDetails.APP_CannotDeliveryOnWednessday == true)
                                    AllowedDay = false;
                                break;
                            case "THURSDAY":
                                if (lstUPVendor[0].VendorDetails.APP_CannotDeliveryOnThrusday == true)
                                    AllowedDay = false;
                                break;
                            case "FRIDAY":
                                if (lstUPVendor[0].VendorDetails.APP_CannotDeliveryOnFriday == true)
                                    AllowedDay = false;
                                break;
                            case "SATURDAY":
                                if (lstUPVendor[0].VendorDetails.APP_CannotDeliveryOnSaturday == true)
                                    AllowedDay = false;
                                break;
                        }

                        if (AllowedDay == false) {
                            bVendorValidation = false;
                            //error               
                            if (hdnCurrentRole.Value == "Carrier") {
                                errMsg = WebCommon.getGlobalResourceValue("BK_MS_04_EXT_Carrier");  // "This date is not available for your booking. Please select another date or contact Office Depot to make booking on this date.";
                                ShowErrorMessage(errMsg, "BK_MS_04_EXT_Carrier");
                            }
                            else {
                                errMsg = WebCommon.getGlobalResourceValue("BK_MS_04_INT"); // "There is a constraint against the vendor to deliver on this date, do you want to continue?";
                                AddWarningMessages(errMsg, "BK_MS_04_INT");
                            }
                        }
                    }

                    //Befor Time and After Time
                    //if (lstUPVendor[0].VendorDetails.IsConstraintsDefined) {
                    //    BeforeSlotTimeID = lstUPVendor[0].VendorDetails.BeforeOrderBYId.ToString();
                    //    AfterSlotTimeID = lstUPVendor[0].VendorDetails.AfterOrderBYId.ToString() != "0" ? lstUPVendor[0].VendorDetails.AfterOrderBYId.ToString() : null;
                    //}
                }
            }
        }
        //if (bVendorValidation) {
        //    if (ddlSite.IsBookingExclusions) {
        //        bVendorValidation = CarrierValidationExc();
        //    }
        //}
        if (!bVendorValidation)
            ShowWarningMessages();
        return bVendorValidation;
       
    }
     //-----------------//

    //--Stage 3 Point 4----//
    private bool CheckSiteClosure() {
        bool bSiteClosure = false;
        if (txtSchedulingdate.innerControltxtDate.Value != string.Empty && ddlSite.innerControlddlSite.SelectedItem.Value != string.Empty) {

            MASSIT_HolidayBE oMASSIT_HolidayBE = new MASSIT_HolidayBE();
            MASSIT_HolidayBAL oMASSIT_HolidayBAL = new MASSIT_HolidayBAL();

            oMASSIT_HolidayBE.Action = "ShowAll";
            oMASSIT_HolidayBE.Site = new BusinessEntities.ModuleBE.AdminFunctions.MAS_SiteBE();
            if (ddlSite.innerControlddlSite.SelectedIndex >= 0)
                oMASSIT_HolidayBE.Site.SiteID = !string.IsNullOrEmpty(ddlSite.innerControlddlSite.SelectedValue) ? Convert.ToInt32(ddlSite.innerControlddlSite.SelectedValue) : 0;

            oMASSIT_HolidayBE.HolidayDate = LogicalSchedulDateTime;

            List<MASSIT_HolidayBE> lstHoliday = oMASSIT_HolidayBAL.GetHolidaysBAL(oMASSIT_HolidayBE);

            if (lstHoliday != null && lstHoliday.Count > 0) {
                string errMsg = WebCommon.getGlobalResourceValue("BK_MS_36"); // Site is closed on this date. Please select a different date.
                ShowErrorMessage(errMsg, "BK_MS_36");
                bSiteClosure = true;
            }
        }
        return bSiteClosure;
    }
    //---------------------//

    public override void PostDate_Change()
    {
        base.PostDate_Change();
        Date_Change(true);       
    }

    private bool Date_Change(bool isDateChange) {
        if (ltSCdate.Visible == true) {
            if (LogicalSchedulDateTime < DateTime.Now.Date) {
                string errMsg = WebCommon.getGlobalResourceValue("BK_MS_05_EXT"); // "Booking not allowed in past date.";
                ShowErrorMessage(errMsg, "BK_MS_05_EXT");
                return false;
            }
        }
        else {
            if (txtSchedulingdate.GetDate.Date < DateTime.Now.Date) {
                string errMsg = WebCommon.getGlobalResourceValue("BK_MS_05_EXT"); // "Booking not allowed in past date.";
                ShowErrorMessage(errMsg, "BK_MS_05_EXT");
                return false;
            }
        }

        if (isDateChange) {
            hdnActualScheduleDate.Value = txtSchedulingdate.innerControltxtDate.Value;
            hdnLogicalScheduleDate.Value = txtSchedulingdate.innerControltxtDate.Value;

            LogicalSchedulDateTime = txtSchedulingdate.GetDate; // Utilities.Common.TextToDateFormat(txtSchedulingdate.innerControltxtDate.Value);
            ActualSchedulDateTime = txtSchedulingdate.GetDate; // Utilities.Common.TextToDateFormat(txtSchedulingdate.innerControltxtDate.Value);
        }

        if (CheckSiteClosure())
            return false;

        if (!GetToleratedDays()) {
            GetMaximumCapacity();

            CarrierValidationExc();

            if (txtSchedulingdate.innerControltxtDate.Value != string.Empty && ddlSite.innerControlddlSite.SelectedItem.Value != string.Empty) {
                bool IsWeekConstraints = GetWeekConstraints();

                if (!IsWeekConstraints) {
                    string errMsg = WebCommon.getGlobalResourceValue("BK_MS_28_EXT"); // "No space available on any compatible doors.Please contact Goods In office or select a different date.";

                    if (hdnCurrentRole.Value == "Carrier") {
                        //Show err msg
                        ShowErrorMessage(errMsg, "BK_MS_28");
                        return false;
                    }
                    else {
                        //Show err msg
                        errMsg = WebCommon.getGlobalResourceValue("BK_MS_28_INT"); //"No space available on any compatible doors.";
                        ShowErrorMessage(errMsg, "BK_MS_28");
                        return false;
                    }
                }
            }
        }
        ShowWarningMessages();
        return true;
    }

    protected void btnProceedAddPO_Click(object sender, EventArgs e)
    {
        ViewState["dtVolumeDetails"] = null;
        if (!txtSchedulingdate.isPostBackDone) {
            if (!Date_Change(false)) {
                return;
            }
        }
        else {
            if (CheckSiteClosure())
                return;
        }

        if (ddlCarrier.SelectedIndex <= 0 || ddlDeliveryType.SelectedIndex <= 0 || ddlVehicleType.SelectedIndex <= 0)
        {
            return;
        }

        if (ltSCdate.Visible == false)
        {
            hdnActualScheduleDate.Value = txtSchedulingdate.innerControltxtDate.Value;
            hdnLogicalScheduleDate.Value = txtSchedulingdate.innerControltxtDate.Value;

            LogicalSchedulDateTime = txtSchedulingdate.GetDate; // Utilities.Common.TextToDateFormat(txtSchedulingdate.innerControltxtDate.Value);
            ActualSchedulDateTime = txtSchedulingdate.GetDate; // Utilities.Common.TextToDateFormat(txtSchedulingdate.innerControltxtDate.Value);
        }

        if (chkSiteSetting.Checked)
        {
            lblFromCountryValue.Text = ddlSourceCountry.innerControlddlCountry.SelectedItem.Text;
        }

        ProceedAddPO();
    }

    private void ProceedAddPO()
    {
        ltSiteName.Text = ddlSite.innerControlddlSite.SelectedItem.Text;
        ltDeliveryType.Text = ddlDeliveryType.SelectedItem.Text;
        ltCarrier.Text = ddlCarrier.SelectedItem.Text;
        ltOtherCarrier.Text = "[" + txtCarrierOther.Text.Trim() + "]";
        if (txtCarrierOther.Text.Trim() != string.Empty)
        {
            ltOtherCarrier.Visible = true;
        }
        else
        {
            ltOtherCarrier.Visible = false;
        }

        ltVehicleType.Text = ddlVehicleType.SelectedItem.Text;
        ltOtherVehicleType.Text = "[" + txtVehicleOther.Text.Trim() + "]";
        if (txtVehicleOther.Text.Trim() != string.Empty)
        {
            ltOtherVehicleType.Visible = true;
        }
        else
        {
            ltOtherVehicleType.Visible = false;
        }

        ltSchedulingDate.Text = ActualSchedulDateTime.ToString("dd/MM/yyyy");

        mvBookingEdit.ActiveViewIndex = 1;
        if (hdnCurrentMode.Value == "Add")
            BindPOGrid();
        else if (hdnCurrentMode.Value == "Edit")
        {
            BindAllPOGrid();
            BindVolumeDetailsGrid();
        }

        // Sprint 4 - Point 1 - Begin
        txtEditBookingCommentF.Text = txtEditBookingComment.Text;
        // Sprint 4 - Point 1 - End
        ShowHazordousItem();
        if (checkBoxEnableHazardouesItemPrompt.Checked == true)
        {
            chkHazardoustick.Checked = true;
            chkHazardoustick.Enabled = false;
        }
        else
        {
            chkHazardoustick.Enabled = false;
        }
    }

    protected void btnAddPO_Click(object sender, EventArgs e)
    {
        // Sprint 1 - Point 1 - Begin
        string errMsg = string.Empty;

        if (txtPurchaseNumber.Text != string.Empty)
        {
            // Here getting Vendor Id from PO list.
            Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE = new Up_PurchaseOrderDetailBE();
            UP_PurchaseOrderDetailBAL oUP_PurchaseOrderDetailBAL = new UP_PurchaseOrderDetailBAL();
            oUp_PurchaseOrderDetailBE.Action = "GetNewBookingPODetails";
            oUp_PurchaseOrderDetailBE.Purchase_order = txtPurchaseNumber.Text;
            oUp_PurchaseOrderDetailBE.CountryID = Convert.ToInt32(PreSiteCountryID);
            List<Up_PurchaseOrderDetailBE> lstPO = oUP_PurchaseOrderDetailBAL.GetProductOrderDetailsBAL(oUp_PurchaseOrderDetailBE);
            int intVendorID = 0;

            foreach (Up_PurchaseOrderDetailBE po in lstPO)
            {
                intVendorID = po.Vendor.VendorID;
                if (intVendorID > 0)
                    break;
            }

            #region Commented code for point 31
            //// Here checking multiple vendor's po allow for site or not to Carrier.
            //if (gvPO.Rows.Count > 0 && Convert.ToString(Session["Role"]).Trim().ToLower() == "carrier") {
            //    if (ddlSite.innerControlddlSite.SelectedValue != null) {
            //        MAS_SiteBE singleSiteSetting =
            //            GetSingleSiteSetting(Convert.ToInt32(ddlSite.innerControlddlSite.SelectedValue));

            //        if (singleSiteSetting.PerBookingVendor.Trim().ToUpper() == "V") {
            //            bool isBreakCheck = true;
            //            for (int index = 0; index < gvPO.Rows.Count; index++) {
            //                HiddenField hdnPOVendorId = (HiddenField)gvPO.Rows[index].FindControl("hdnPOVendorId");
            //                if (hdnPOVendorId != null) {
            //                    isBreakCheck = true;
            //                    foreach (Up_PurchaseOrderDetailBE po in lstPO) {
            //                        if (Convert.ToInt32(hdnPOVendorId.Value) != po.Vendor.VendorID) {
            //                            isBreakCheck = false;
            //                            break;
            //                        }
            //                    }

            //                    if (!isBreakCheck) {
            //                        ScriptManager.RegisterStartupScript(this, this.GetType(), "popup", "<script>alert('" + strIsVendorRequiredForSeparateBooking + "');</script>", false);
            //                        return;
            //                    }
            //                }
            //            }
            //        }
            //    }
            //}
            #endregion

            IsPOValid(intVendorID);

        }
        else
        {
            errMsg = WebCommon.getGlobalResourceValue("BK_MS_CorrectPO"); //"Please enter correct Purchase Order Number.";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "popup", "<script>alert('" + errMsg + "');</script>", false);
        }
        // Sprint 1 - Point 1 - End
    }

    private void IsPOValid(int intVendorID)
    {
        string errMsg = string.Empty;
        bool isWarning = false;

        if (IsBookingValidationExcluded == false)
        {  //If there is no Validation Exclusion for the vendor/carrier or booking type

            Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE = new Up_PurchaseOrderDetailBE();
            UP_PurchaseOrderDetailBAL oUP_PurchaseOrderDetailBAL = new UP_PurchaseOrderDetailBAL();

            oUp_PurchaseOrderDetailBE.Action = "GetAllPODetails";
            oUp_PurchaseOrderDetailBE.Purchase_order = txtPurchaseNumber.Text.Trim();
            oUp_PurchaseOrderDetailBE.CountryID = Convert.ToInt32(PreSiteCountryID);

            List<Up_PurchaseOrderDetailBE> lstPO = oUP_PurchaseOrderDetailBAL.GetAllPODetailsBAL(oUp_PurchaseOrderDetailBE);

            if (lstPO.Count <= 0)
            {
                errMsg = WebCommon.getGlobalResourceValue("BK_MS_11_EXT"); // "The Purchase Order Number you have supplied is not valid, please enter correct Purchase Order Number.";
                ShowErrorMessage(errMsg, "BK_MS_11_EXT");
                return;
                //if (hdnCurrentRole.Value == "Carrier") {
                //    errMsg = WebCommon.getGlobalResourceValue("BK_MS_11_EXT"); //"The Purchase Order Number you have supplied is not valid, please enter correct Purchase Order Number.";
                //    ShowErrorMessage(errMsg, "BK_MS_11_EXT");
                //    return;
                //}
                //else {
                //    errMsg = "The Purchase Order Number you have supplied is not valid. Do you want to continue?";
                //    AddWarningMessages(errMsg, "BK_MS_11_INT");
                //    isWarning = true;
                //}
            }

            if (lstPO.Count > 0)
            {
                if (lstPO[0].LastUploadedFlag == "N")
                {
                    errMsg = WebCommon.getGlobalResourceValue("BK_MS_11_EXT"); //"The Purchase Order Number you have supplied is not valid, please enter correct Purchase Order Number.";
                    ShowErrorMessage(errMsg, "BK_MS_11_EXT");
                    return;
                }
            }
        }

        if (isWarning == true)
        {
            ShowWarningMessages();
        }
        else
        {
            // Here checking PO is existing or not.
            APPBOK_BookingBE appbok_BookingBE = new APPBOK_BookingBE();
            appbok_BookingBE.Action = "IsExistingPO";
            appbok_BookingBE.PurchaseOrders = txtPurchaseNumber.Text;
            appbok_BookingBE.VendorID = Convert.ToInt32(intVendorID);
            appbok_BookingBE.SiteId = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
            APPBOK_BookingBAL appbok_BookingBAL = new APPBOK_BookingBAL();
            List<APPBOK_BookingBE> lstAPPBOK_BookingBE = appbok_BookingBAL.IsExistingPO(appbok_BookingBE);

            if (lstAPPBOK_BookingBE.Count == 0)
            {
                if(VendorValidationExc(Convert.ToString(intVendorID)))   //Stage 5 Point 1
                    this.AddPO(true);                
            }
            else
            {
                foreach (APPBOK_BookingBE oAPPBOK_BookingBE in lstAPPBOK_BookingBE)
                {
                    #region Double booking warning message formatting.
                    errMsg = string.Format("<table><tr><td style=\"text-align:center\">{0}</td></tr>",
                        WebCommon.getGlobalResourceValue("BK_MS_29_Warning_1"));

                    errMsg = string.Format("{0}<tr><td style=\"text-align: center\">{1}</td></tr>",
                        errMsg, WebCommon.getGlobalResourceValue("BK_MS_29_Warning_2").Replace("##dd/mm/yyyy##", "<b>" + Convert.ToDateTime(oAPPBOK_BookingBE.ScheduleDate).ToString("dd/MM/yyyy") + "</b>"));

                    errMsg = string.Format("{0}<tr><td style=\"text-align: center\">{1}</td></tr>",
                        errMsg, WebCommon.getGlobalResourceValue("BK_MS_29_Warning_3").Replace("##tt:mm##", String.Format("{0}", "<b>" + oAPPBOK_BookingBE.SlotTime.SlotTime) + "</b>"));

                    errMsg = string.Format("{0}<tr><td style=\"text-align: center\">{1}</td></tr></table>",
                        errMsg, WebCommon.getGlobalResourceValue("BK_MS_29_Warning_4"));
                    #endregion
                    break; // Here being break because need only zero index value.
                }

               // lblWarningText.Visible = true;
                ViewState["WarningStatus"] = 1;
                ltWarningConfirm.Text = errMsg;
                mdlWarningConfirm.Show();
                return;
            }
        }
    }

    private void AddPO(bool isValidPOChecked = false)
    {
        string errMsg = string.Empty;

        if (txtPurchaseNumber.Text != string.Empty)
        {

            DataTable myDataTable = null;

            if (ViewState["myDataTable"] != null)
            {
                myDataTable = (DataTable)ViewState["myDataTable"];

                //Check Item alrady added
                DataRow[] drr = myDataTable.Select("PurchaseNumber='" + txtPurchaseNumber.Text.Trim() + "'");

                if (drr.Length > 0)
                {
                    errMsg = WebCommon.getGlobalResourceValue("PurchaseOrderExist"); //"The Purchase Order Number you have supplied already exists.";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "popup", "<script>alert('" + errMsg + "');</script>", false);
                    txtPurchaseNumber.Text = string.Empty;
                    return;
                }
            }

            if (IsBookingValidationExcluded == false)
            {  //If there is no Validation Exclusion for the vendor/carrier or booking type

                bool isWarning = false;

                Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE = new Up_PurchaseOrderDetailBE();
                UP_PurchaseOrderDetailBAL oUP_PurchaseOrderDetailBAL = new UP_PurchaseOrderDetailBAL();

                oUp_PurchaseOrderDetailBE.Action = "GetAllPODetails";
                oUp_PurchaseOrderDetailBE.Purchase_order = txtPurchaseNumber.Text.Trim();
                oUp_PurchaseOrderDetailBE.CountryID = Convert.ToInt32(PreSiteCountryID);
                //oUp_PurchaseOrderDetailBE.Vendor = new UP_VendorBE();
                // oUp_PurchaseOrderDetailBE.Vendor.VendorID = Convert.ToInt32(ucSeacrhVendor1.VendorNo);

                List<Up_PurchaseOrderDetailBE> lstPO = oUP_PurchaseOrderDetailBAL.GetAllPODetailsBAL(oUp_PurchaseOrderDetailBE);

                if (!isValidPOChecked)
                {
                    if (lstPO.Count <= 0)
                    {
                        errMsg = WebCommon.getGlobalResourceValue("BK_MS_11_EXT"); // "The Purchase Order Number you have supplied is not valid, please enter correct Purchase Order Number.";
                        ShowErrorMessage(errMsg, "BK_MS_11_EXT");
                        return;
                        //if (Session["Role"].ToString() == "Vendor" || Session["Role"].ToString() == "Carrier") {
                        //    errMsg = "The Purchase Order Number you have supplied is not valid, please enter correct Purchase Order Number.";
                        //    ShowErrorMessage(errMsg, "BK_MS_11_EXT");
                        //    return;
                        //}
                        //else {
                        //    errMsg = "The Purchase Order Number you have supplied is not valid. Do you want to continue?";
                        //    AddWarningMessages(errMsg, "BK_MS_11_INT");
                        //    isWarning = true;
                        //}
                    }
                }

                if (lstPO.Count > 0)
                {

                    if (!isValidPOChecked)
                    {
                        if (lstPO[0].LastUploadedFlag == "N")
                        {
                            errMsg = WebCommon.getGlobalResourceValue("BK_MS_11_EXT"); //"The Purchase Order Number you have supplied is not valid, please enter correct Purchase Order Number.";
                            ShowErrorMessage(errMsg, "BK_MS_11_EXT");
                            return;
                        }
                    }

                    if (Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value) != lstPO[0].Site.SiteID)
                    {

                        //Cross docking check

                        MASCNT_CrossDockBE oMASCNT_CrossDockBE = new MASCNT_CrossDockBE();
                        APPCNT_CrossDockBAL oMASCNT_CrossDockBAL = new APPCNT_CrossDockBAL();

                        oMASCNT_CrossDockBE.Action = "GetDestSiteById";
                        oMASCNT_CrossDockBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
                        oMASCNT_CrossDockBE.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);

                        List<MASCNT_CrossDockBE> lstCrossDock = oMASCNT_CrossDockBAL.GetCrossDockDetailsBAL(oMASCNT_CrossDockBE);

                        if (lstCrossDock != null && lstCrossDock.Count > 0)
                        {

                            lstCrossDock = lstCrossDock.FindAll(delegate(MASCNT_CrossDockBE cd)
                            {
                                return cd.DestinationSiteID == lstPO[0].Site.SiteID;
                            });
                        }

                        if (lstCrossDock == null || lstCrossDock.Count == 0)
                        {
                            if (Session["Role"].ToString() == "Vendor" || Session["Role"].ToString() == "Carrier")
                            {
                                errMsg = WebCommon.getGlobalResourceValue("BK_MS_14_EXT").Replace("##Ponumber##", txtPurchaseNumber.Text.Trim()).Replace("##Sitename##", lstPO[0].Site.SiteName); //"The Purchase Order you have quoted is not for the Office Depot site you have selected." +
                                //"Please recheck the entries made and resubmit the correct information";
                                ShowErrorMessage(errMsg, "BK_MS_14_EXT");
                                return;
                            }
                            else
                            {
                                errMsg = WebCommon.getGlobalResourceValue("BK_MS_14_INT").Replace("##Ponumber##", txtPurchaseNumber.Text.Trim()).Replace("##Sitename##", lstPO[0].Site.SiteName);//"The Purchase Order you have quoted is not for the Office Depot site you have selected. Do you want to continue?";
                                AddWarningMessages(errMsg, "BK_MS_14_INT");
                                isWarning = true;
                            }
                        }
                    }



                    int? ToleranceDueDay = lstPO[0].Site.ToleranceDueDay;

                    //DateTime PODueDate = Utilities.Common.TextToDateFormat(GetFormatedDate(lstPO[0].Original_due_date.Value.Day.ToString() + "/" + lstPO[0].Original_due_date.Value.Month.ToString() + "/" + lstPO[0].Original_due_date.Value.Year.ToString()));

                    DateTime PODueDate = DateTime.Now;
                    if (lstPO[0].Expected_date != null) // This condition is given, if [Expected Date] will come null then [Original due date] will be set like earlier.
                    {
                        PODueDate = Utilities.Common.TextToDateFormat(GetFormatedDate(lstPO[0].Expected_date.Value.Day.ToString() + "/" + lstPO[0].Expected_date.Value.Month.ToString() + "/" + lstPO[0].Expected_date.Value.Year.ToString()));
                    }
                    else
                    {
                        PODueDate = Utilities.Common.TextToDateFormat(GetFormatedDate(lstPO[0].Original_due_date.Value.Day.ToString() + "/" + lstPO[0].Original_due_date.Value.Month.ToString() + "/" + lstPO[0].Original_due_date.Value.Year.ToString()));
                    }

                    //DateTime CurrentDate = Utilities.Common.TextToDateFormat(DateTime.Now.Day.ToString() + "/" + DateTime.Now.Month.ToString() + "/" + DateTime.Now.Year.ToString());
                    DateTime ScheduledDate = LogicalSchedulDateTime;  //SchDate; // Utilities.Common.TextToDateFormat(GetFormatedDate(lstPO[0].Original_due_date.ToString()));

                    //OTIF Future Date PO check//
                    bool isOTIFPO = false;
                    CntOTIF_FutureDateSetupBE oCntOTIF_FutureDateSetupBE = new CntOTIF_FutureDateSetupBE();
                    CntOTIF_FutureDateSetupBAL oCntOTIF_FutureDateSetupBAL = new CntOTIF_FutureDateSetupBAL();
                    oCntOTIF_FutureDateSetupBE.Action = "ShowAll";
                    oCntOTIF_FutureDateSetupBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
                    oCntOTIF_FutureDateSetupBE.User.UserID = Convert.ToInt32(Session["UserID"]);
                    oCntOTIF_FutureDateSetupBE.CountryID = Convert.ToInt32(PreSiteCountryID);
                    List<CntOTIF_FutureDateSetupBE> lstCountryFutureDatePO = oCntOTIF_FutureDateSetupBAL.GetCntOTIF_FutureDateSetupDetailsBAL(oCntOTIF_FutureDateSetupBE);
                    if (lstCountryFutureDatePO != null && lstCountryFutureDatePO.Count > 0) {
                        // This condition is given, if [Expected Date] will come null then [Original due date] will be set like earlier.
                        if (lstPO[0].Expected_date != null) {
                            // Sprint 1 - Point 14 - Begin - Changing from [Original_due_date] to [Expected Date]
                            /*if (lstPO[0].Original_due_date.Value.Day == Convert.ToInt32(lstCountryFutureDatePO[0].FutureDatePODay) && lstPO[0].Original_due_date.Value.Month == Convert.ToInt32(lstCountryFutureDatePO[0].FutureDatePOMonth))*/
                            if (lstPO[0].Expected_date.Value.Day == Convert.ToInt32(lstCountryFutureDatePO[0].FutureDatePODay) && lstPO[0].Expected_date.Value.Month == Convert.ToInt32(lstCountryFutureDatePO[0].FutureDatePOMonth))
                            // Sprint 1 - Point 14 - End - Changing from [Original_due_date] to [Expected Date]
                            {
                                //This is OTIF PO Date
                                isOTIFPO = true;
                            }
                        }
                        else {
                            if (lstPO[0].Original_due_date.Value.Day == Convert.ToInt32(lstCountryFutureDatePO[0].FutureDatePODay) && lstPO[0].Original_due_date.Value.Month == Convert.ToInt32(lstCountryFutureDatePO[0].FutureDatePOMonth)) {
                                //This is OTIF PO Date
                                isOTIFPO = true;
                            }
                        }
                    }
                    //------------------------//

                    bool isError = false;

                    if (isOTIFPO == false)
                    {
                        if (lstPO[0].Site.ToleranceDueDay == 0)
                        {
                            if (ScheduledDate < PODueDate)
                            { //if (ScheduledDate < PODueDate) {
                                isError = true;
                            }
                        }
                        else if (lstPO[0].Site.ToleranceDueDay > 0)
                        {
                            //if (PODueDate > ScheduledDate.AddDays(Convert.ToDouble(lstPO[0].Site.ToleranceDueDay)))
                            //{
                            //    isError = true;  
                            //}

                            //----- for weekend --------------------//
                            int DaysDiff = 0;
                            if (PODueDate >= ScheduledDate) {
                                DaysDiff = Enumerable.Range(0, Convert.ToInt32(PODueDate.Subtract(ScheduledDate).TotalDays)).Select(i => new[] { DayOfWeek.Saturday, DayOfWeek.Sunday }.Contains(ScheduledDate.AddDays(i).DayOfWeek) ? 1 : 0).Sum();
                            }

                            if (PODueDate > ScheduledDate.AddDays(Convert.ToDouble(lstPO[0].Site.ToleranceDueDay + DaysDiff))) {
                                isError = true;
                            }     
                        }

                        if (isError)
                        {           //error     
                            if (Session["Role"].ToString() == "Vendor" || Session["Role"].ToString() == "Carrier")
                            {
                                errMsg = WebCommon.getGlobalResourceValue("BK_MS_13_EXT").Replace("##dd/mm/yyyy##", PODueDate.ToString("dd/MM/yyyy"));  // "The  Purchase Order quoted is not due in on the date selected. Please rebook this Purchase Order for the correct date. <br> Due Date:" + PODueDate.ToString("dd/MM/yyyy"); 
                                ShowErrorMessage(errMsg, "BK_MS_13_EXT");
                                return;
                            }
                            else
                            {
                                errMsg = WebCommon.getGlobalResourceValue("BK_MS_13_INT").Replace("##dd/mm/yyyy##", PODueDate.ToString("dd/MM/yyyy")); // The Purchase Order quoted is not due in for this date.The correct due date is dd/mm/yyyy. Do you want to continue?
                                AddWarningMessages(errMsg, "BK_MS_13_INT");
                                isWarning = true;
                            }
                        }
                    }
                }

                if (isWarning == false)
                    BindPOGrid();
                else
                    ShowWarningMessages();
            }
            else if (IsBookingValidationExcluded == true)
            {  //If there is Validation Exclusion for the vendor/carrier or booking type
                BindPOGrid();
            }
        }
    }

    private string GetFormatedDate(string DateToFormat)
    {
        if (DateToFormat != string.Empty)
        {
            string[] arrDate = DateToFormat.Split('/');
            string DD, MM, YYYY;
            if (arrDate[0].Length < 2)
                DD = "0" + arrDate[0].ToString();
            else
                DD = arrDate[0];

            if (arrDate[1].Length < 2)
                MM = "0" + arrDate[1].ToString();
            else
                MM = arrDate[1];

            YYYY = arrDate[2].ToString().Substring(0, 4);

            return DD + "/" + MM + "/" + YYYY.Trim();
        }
        else
        {
            return DateToFormat;
        }
    }

    private string GetFormatedTime(string TimeToFormat)
    {
        if (TimeToFormat != string.Empty)
        {
            string[] arrDate = TimeToFormat.Split(':');
            string HH, MM;
            if (arrDate[0].Length < 2)
                HH = "0" + arrDate[0].ToString();
            else
                HH = arrDate[0];

            if (arrDate[1].Length < 2)
                MM = "0" + arrDate[1].ToString();
            else
                MM = arrDate[1];


            return HH + ":" + MM;
        }
        else
        {
            return TimeToFormat;
        }
    }

    protected void btnOK_Click(object sender, EventArgs e)
    {
        mdlNoSlotFound.Hide();
    }

    protected void BindAllPOGrid()
    {
        DataTable myDataTable = null;
        myDataTable = new DataTable();

        DataColumn auto = new DataColumn("AutoID", typeof(System.Int32));
        // specify it as auto increment field
        auto.AutoIncrement = true;
        auto.AutoIncrementSeed = 1;
        auto.ReadOnly = true;
        auto.Unique = true;
        myDataTable.Columns.Add(auto);

        DataColumn myDataColumn = new DataColumn();
        myDataColumn.ColumnName = "VendorID";
        myDataTable.Columns.Add(myDataColumn);

        myDataColumn = new DataColumn();
        myDataColumn.ColumnName = "PurchaseNumber";
        myDataTable.Columns.Add(myDataColumn);

        myDataColumn = new DataColumn();
        myDataColumn.ColumnName = "Original_due_date";
        myDataTable.Columns.Add(myDataColumn);

        myDataColumn = new DataColumn();
        myDataColumn.ColumnName = "VendorName";
        myDataTable.Columns.Add(myDataColumn);

        myDataColumn = new DataColumn();
        myDataColumn.ColumnName = "OutstandingLines";
        myDataTable.Columns.Add(myDataColumn);

        myDataColumn = new DataColumn();
        myDataColumn.ColumnName = "Qty_On_Hand";
        myDataTable.Columns.Add(myDataColumn);

        myDataColumn = new DataColumn();
        myDataColumn.ColumnName = "qty_on_backorder";
        myDataTable.Columns.Add(myDataColumn);

        myDataColumn = new DataColumn();
        myDataColumn.ColumnName = "SiteName";
        myDataTable.Columns.Add(myDataColumn);

        myDataColumn = new DataColumn();
        myDataColumn.ColumnName = "PurchaseOrderID";
        myDataTable.Columns.Add(myDataColumn);

        myDataColumn = new DataColumn();
        myDataColumn.ColumnName = "ExpectedDate";
        myDataTable.Columns.Add(myDataColumn);

        myDataColumn = new DataColumn();
        myDataColumn.ColumnName = "LinesToBeDelivered";
        myDataTable.Columns.Add(myDataColumn);

        myDataColumn = new DataColumn();
        myDataColumn.ColumnName = "IsProvisionResonChangesUpdate";
        myDataTable.Columns.Add(myDataColumn);

        Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE = new Up_PurchaseOrderDetailBE();
        UP_PurchaseOrderDetailBAL oUP_PurchaseOrderDetailBAL = new UP_PurchaseOrderDetailBAL();

        oUp_PurchaseOrderDetailBE.Action = "GetAllBookingPODetails";
        oUp_PurchaseOrderDetailBE.BookingID = Convert.ToInt32(GetQueryStringValue("ID").ToString());

        List<Up_PurchaseOrderDetailBE> lstPO = oUP_PurchaseOrderDetailBAL.GetAllBookedProductOrderDetailsBAL(oUp_PurchaseOrderDetailBE);

        if (lstPO.Count > 0)
        {
            for (int iCount = 0; iCount < lstPO.Count; iCount++)
            {
                DataRow dataRow = myDataTable.NewRow();

                //oUp_PurchaseOrderDetailBE.Action = "GetStockDetails";
                //oUp_PurchaseOrderDetailBE.Purchase_order = lstPO[iCount].Purchase_order;
                //oUp_PurchaseOrderDetailBE.Site = new MAS_SiteBE();
                //oUp_PurchaseOrderDetailBE.Site.SiteID = lstPO[iCount].Site.SiteID;  //Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);

                //List<Up_PurchaseOrderDetailBE> lstPOStock = oUP_PurchaseOrderDetailBAL.GetPriorityBAL(oUp_PurchaseOrderDetailBE);

                //if (lstPOStock != null && lstPOStock.Count > 0)
                //{

                //    List<Up_PurchaseOrderDetailBE> lstPOTemp = lstPOStock.FindAll(delegate(Up_PurchaseOrderDetailBE p) { return Convert.ToDecimal(p.SKU.Qty_On_Hand) == 0 && (Convert.ToInt32(Convert.ToDecimal(p.Original_quantity)) - p.Qty_receipted > 0); });

                //    if (lstPOTemp != null && lstPOTemp.Count > 0)
                //    {
                //        dataRow["Qty_On_Hand"] = lstPOTemp.Count.ToString();
                //    }
                //    else
                //    {
                //        dataRow["Qty_On_Hand"] = "0";
                //    }

                //    //lstPOTemp = lstPOStock.FindAll(delegate(Up_PurchaseOrderDetailBE p) { return Convert.ToDecimal(p.SKU.qty_on_backorder) > 0 && (Convert.ToInt32(Convert.ToDecimal(p.Original_quantity)) - p.Qty_receipted > 0); });
                //    lstPOTemp = lstPOStock.FindAll(delegate(Up_PurchaseOrderDetailBE p)
                //    {
                //        return (Convert.ToDecimal(p.SKU.Qty_On_Hand) - Convert.ToDecimal(p.SKU.qty_on_backorder) < 0);
                //    }
                // );
                //    if (lstPOTemp != null && lstPOTemp.Count > 0)
                //    {
                //        dataRow["qty_on_backorder"] = lstPOTemp.Count.ToString();
                //    }
                //    else
                //    {
                //        dataRow["qty_on_backorder"] = "0";
                //    }
                //}
                //else
                //{
                //    dataRow["Qty_On_Hand"] = "0";
                //    dataRow["qty_on_backorder"] = "0";
                //}


                dataRow["VendorID"] = lstPO[iCount].Vendor.VendorID.ToString();
                dataRow["PurchaseNumber"] = lstPO[iCount].Purchase_order;
                dataRow["Original_due_date"] = lstPO[iCount].Original_due_date.Value.ToString("dd/MM/yyyy");
                dataRow["VendorName"] = lstPO[iCount].Vendor.VendorName;

                dataRow["Qty_On_Hand"] = lstPO[iCount].Stockouts;
                dataRow["IsProvisionResonChangesUpdate"] = lstPO[iCount].IsProvisionResonChangesUpdate;
                dataRow["qty_on_backorder"] = lstPO[iCount].Backorders;
                if (lstPO[iCount].OutstandingLines > 0) {
                    dataRow["OutstandingLines"] = lstPO[iCount].OutstandingLines.ToString(); //lstPOStock.Count.ToString(); //
                    dataRow["LinesToBeDelivered"] = lstPO[iCount].OutstandingLines.ToString(); //lstPOStock.Count.ToString(); //
                }
                else {
                    dataRow["OutstandingLines"] = 1;
                    dataRow["LinesToBeDelivered"] = 1;
                }
                
                //dataRow["Qty_On_Hand"] = lstPO[iCount].SKU.Qty_On_Hand;
                //dataRow["qty_on_backorder"] = lstPO[iCount].SKU.qty_on_backorder;
                dataRow["SiteName"] = lstPO[iCount].Site.SiteName;
                dataRow["PurchaseOrderID"] = lstPO[iCount].PurchaseOrderID.ToString();
              

                if (lstPO[iCount].Expected_date != null)
                {
                    dataRow["ExpectedDate"] = lstPO[iCount].Expected_date.Value.ToString("dd/MM/yyyy");
                }
                else
                {
                    dataRow["ExpectedDate"] = lstPO[iCount].Original_due_date.Value.ToString("dd/MM/yyyy");
                }

                myDataTable.Rows.Add(dataRow);
            }
        }

        if (myDataTable != null && myDataTable.Rows.Count > 0)
        {
            gvPO.DataSource = myDataTable;
            gvPO.DataBind();
           
            gvPOHistory.DataSource = myDataTable;
            gvPOHistory.DataBind();
        }

        if (myDataTable != null && myDataTable.Rows.Count > 0)
        {
            ScriptManager.RegisterStartupScript(Page, this.GetType(), "btnshow", "$get('" + btnProceedBooking.ClientID + "').style.display='';", true);
            ViewState["myDataTable"] = myDataTable;
        }
        else
        {
            ScriptManager.RegisterStartupScript(Page, this.GetType(), "btnhide", "$get('" + btnProceedBooking.ClientID + "').style.display='none';", true);
            ViewState["myDataTable"] = null;
        }

        int iTotalLines = 0;
        foreach (DataRow dr in myDataTable.Rows)
        {
            if (dr["OutstandingLines"].ToString() != string.Empty)
            {
                //TotalLines += Convert.ToInt32(dr["OutstandingLines"]);
                iTotalLines += Convert.ToInt32(dr["OutstandingLines"]);
            }
        }

        //lblExpectedLines.Text = lblExpectedLines.Text.Replace(Lines, TotalLines.ToString());
        Lines = iTotalLines.ToString();



        if (myDataTable != null && myDataTable.Rows.Count > 0)
        {
            ScriptManager.RegisterStartupScript(Page, this.GetType(), "btnshow", "$get('" + btnProceedBooking.ClientID + "').style.display='';", true);
            ViewState["myDataTable"] = myDataTable;


            //Build Volume Details Settings--------------//
            DataTable dtVolumeDetails = null;
            for (int iCount = 0; iCount < myDataTable.Rows.Count; iCount++)
            {


                if (ViewState["dtVolumeDetails"] != null)
                {
                    dtVolumeDetails = (DataTable)ViewState["dtVolumeDetails"];

                    DataRow[] UpdateRow = dtVolumeDetails.Select("VendorID=" + myDataTable.Rows[iCount]["VendorID"].ToString());

                    if (UpdateRow != null && UpdateRow.Length > 0)
                    {
                        UpdateRow[0]["ExpectedLines"] = Convert.ToInt32(UpdateRow[0]["ExpectedLines"]) + Convert.ToInt32(myDataTable.Rows[iCount]["OutstandingLines"].ToString());
                        UpdateRow[0]["NoOfPO"] = (Convert.ToInt32(UpdateRow[0]["NoOfPO"]) + 1).ToString();
                        //UpdateRow[0]["LinesToBeDelivered"] = Convert.ToInt32(UpdateRow[0]["LinesToBeDelivered"]) + Convert.ToInt32(myDataTable.Rows[iCount]["LinesToBeDelivered"].ToString());
                    }
                    else
                    {
                        DataRow drdataRow = dtVolumeDetails.NewRow();
                        if (lstPO.Count > 0)
                        {
                            drdataRow["VendorID"] = myDataTable.Rows[iCount]["VendorID"].ToString();
                            drdataRow["VendorName"] = myDataTable.Rows[iCount]["VendorName"].ToString();
                            drdataRow["ExpectedLines"] = myDataTable.Rows[iCount]["OutstandingLines"].ToString();

                            drdataRow["NoOfPO"] = "1";
                            //drdataRow["LinesToBeDelivered"] = myDataTable.Rows[iCount]["LinesToBeDelivered"].ToString();
                        }
                        dtVolumeDetails.Rows.Add(drdataRow);
                    }
                }
                else
                {

                    dtVolumeDetails = new DataTable();

                    auto = new DataColumn("AutoID", typeof(System.Int32));
                    // specify it as auto increment field
                    auto.AutoIncrement = true;
                    auto.AutoIncrementSeed = 1;
                    auto.ReadOnly = true;
                    auto.Unique = true;
                    dtVolumeDetails.Columns.Add(auto);

                    myDataColumn = new DataColumn();
                    myDataColumn.ColumnName = "VendorID";
                    dtVolumeDetails.Columns.Add(myDataColumn);

                    myDataColumn = new DataColumn();
                    myDataColumn.ColumnName = "VendorName";
                    dtVolumeDetails.Columns.Add(myDataColumn);

                    myDataColumn = new DataColumn();
                    myDataColumn.ColumnName = "ExpectedLines";
                    dtVolumeDetails.Columns.Add(myDataColumn);

                    myDataColumn = new DataColumn();
                    myDataColumn.ColumnName = "Pallets";
                    dtVolumeDetails.Columns.Add(myDataColumn);

                    myDataColumn = new DataColumn();
                    myDataColumn.ColumnName = "Cartons";
                    dtVolumeDetails.Columns.Add(myDataColumn);

                    myDataColumn = new DataColumn();
                    myDataColumn.ColumnName = "NoOfPO";
                    dtVolumeDetails.Columns.Add(myDataColumn);

                    myDataColumn = new DataColumn();
                    myDataColumn.ColumnName = "LinesToBeDelivered";
                    dtVolumeDetails.Columns.Add(myDataColumn);

                    myDataColumn = new DataColumn();
                    myDataColumn.ColumnName = "OldNumberOfPallet";
                    dtVolumeDetails.Columns.Add(myDataColumn);

                    myDataColumn = new DataColumn();
                    myDataColumn.ColumnName = "OldNumberOfCartons";
                    dtVolumeDetails.Columns.Add(myDataColumn);


                    myDataColumn = new DataColumn();
                    myDataColumn.ColumnName = "OldNumberOfLine";
                    dtVolumeDetails.Columns.Add(myDataColumn);


                    DataRow drdataRow = dtVolumeDetails.NewRow();
                    if (myDataTable.Rows.Count > 0)
                    {
                        drdataRow["VendorID"] = myDataTable.Rows[iCount]["VendorID"].ToString();
                        drdataRow["VendorName"] = myDataTable.Rows[iCount]["VendorName"].ToString();
                        drdataRow["ExpectedLines"] = myDataTable.Rows[iCount]["OutstandingLines"].ToString();
                        drdataRow["NoOfPO"] = "1";
                        //drdataRow["LinesToBeDelivered"] = Convert.ToString(myDataTable.Rows[iCount]["LinesToBeDelivered"]);
                    }
                    dtVolumeDetails.Rows.Add(drdataRow);
                }

                if (dtVolumeDetails != null && dtVolumeDetails.Rows.Count > 0)
                {
                    ViewState["dtVolumeDetails"] = dtVolumeDetails;
                }
                else
                {
                    ViewState["dtVolumeDetails"] = null;
                }
                //------------end volume details-------------//
            }

            if (dtVolumeDetails != null && dtVolumeDetails.Rows.Count > 0)
            {
                gvVolume.DataSource = dtVolumeDetails;
                gvVolume.DataBind();
            }           

        }
        else
        {
            ScriptManager.RegisterStartupScript(Page, this.GetType(), "btnhide", "$get('" + btnProceedBooking.ClientID + "').style.display='none';", true);
            ViewState["myDataTable"] = null;
        }




        //for (int iCount = 0; iCount < myDataTable.Rows.Count; iCount++) {
        //    TotalLines += Convert.ToInt32(lstPO[0].OutstandingLines.ToString()); //Convert.ToInt32(dataRow["OutstandingLines"]);
        //}

        TotalLines = iTotalLines.ToString();

        txtPurchaseNumber.Text = string.Empty;

    }

    private void BindVolumeDetailsGrid()
    {

        APPBOK_BookingBE oAPPBOK_BookingBE = new APPBOK_BookingBE();
        APPBOK_BookingBAL oAPPBOK_BookingBAL = new APPBOK_BookingBAL();

        oAPPBOK_BookingBE.Action = "GetCarrierVolumeDeatilsByID";
        oAPPBOK_BookingBE.BookingID = Convert.ToInt32(GetQueryStringValue("ID").ToString());

        List<APPBOK_BookingBE> lstBookingDeatils = new List<APPBOK_BookingBE>();
        lstBookingDeatils = oAPPBOK_BookingBAL.GetCarrierVolumeDeatilsBAL(oAPPBOK_BookingBE);

        List<APPBOK_BookingBE> lstVolumeDeatils = new List<APPBOK_BookingBE>();

        DataTable dtVolumeDetails = null;
        BookedLines = 0;
        BookedPallets = 0;
        if (lstBookingDeatils != null && lstBookingDeatils.Count > 0)
        {

            foreach (GridViewRow gvRow in gvVolume.Rows)
            {
                if (gvRow.RowType == DataControlRowType.DataRow)
                {
                    HiddenField hdngvVendorID = (HiddenField)gvRow.FindControl("hdngvVendorID");
                    ucNumericTextbox txtVendorPallets = (ucNumericTextbox)gvRow.FindControl("txtVendorPallets");
                    ucNumericTextbox txtVendorCartons = (ucNumericTextbox)gvRow.FindControl("txtVendorCartons");
                    ucLiteral ltVendorExpectedLines = (ucLiteral)gvRow.FindControl("ltVendorExpectedLines");
                    ucNumericTextbox txtLinesToBeDelivered = (ucNumericTextbox)gvRow.FindControl("txtLinesToBeDelivered");

                    lstVolumeDeatils = lstBookingDeatils.FindAll(delegate(APPBOK_BookingBE Bok) { return Bok.VendorID == Convert.ToInt32(hdngvVendorID.Value); });

                    if (lstVolumeDeatils != null && lstVolumeDeatils.Count > 0)
                    {
                        txtVendorPallets.Text = lstVolumeDeatils[0].NumberOfPallet.ToString();
                        txtVendorCartons.Text = lstVolumeDeatils[0].NumberOfCartons.ToString();


                        txtLinesToBeDelivered.Text = lstVolumeDeatils[0].NumberOfLines.ToString();  //ltVendorExpectedLines.Text; //
                      
                        BookedLines = BookedLines + Convert.ToInt32(txtLinesToBeDelivered.Text);
                        BookedPallets = BookedPallets + Convert.ToInt32(txtVendorPallets.Text);
                        //-----Stage 10 Point 10L 5.1-------
                        //BookedCartons = BookedCartons + Convert.ToInt32(txtVendorCartons.Text);
                        //----------------------------------


                        if (ViewState["dtVolumeDetails"] != null)
                        {
                            dtVolumeDetails = (DataTable)ViewState["dtVolumeDetails"];

                            DataRow[] UpdateRow = dtVolumeDetails.Select("VendorID='" + hdngvVendorID.Value.ToString() + "'");

                            if (UpdateRow != null && UpdateRow.Length > 0)
                            {
                                UpdateRow[0]["Pallets"] = Convert.ToInt32(UpdateRow[0]["Pallets"] == DBNull.Value ? 0 : UpdateRow[0]["Pallets"]) + Convert.ToInt32(lstVolumeDeatils[0].NumberOfPallet.ToString());
                                UpdateRow[0]["Cartons"] = (Convert.ToInt32(UpdateRow[0]["Cartons"] == DBNull.Value ? 0 : UpdateRow[0]["Cartons"]) + Convert.ToInt32(lstVolumeDeatils[0].NumberOfCartons.ToString()));
                                UpdateRow[0]["LinesToBeDelivered"] = (Convert.ToInt32(UpdateRow[0]["LinesToBeDelivered"] == DBNull.Value ? 0 : UpdateRow[0]["LinesToBeDelivered"]) + Convert.ToInt32(lstVolumeDeatils[0].NumberOfLines.ToString()));
                                UpdateRow[0]["OldNumberOfPallet"] = (Convert.ToInt32(lstVolumeDeatils[0].OldNumberOfPallet == null ? 0 : Convert.ToInt32(lstVolumeDeatils[0].OldNumberOfPallet.ToString())));
                                UpdateRow[0]["OldNumberOfCartons"] = (Convert.ToInt32(lstVolumeDeatils[0].OldNumberOfCartons == null ? 0 : Convert.ToInt32(lstVolumeDeatils[0].OldNumberOfCartons.ToString())));
                                UpdateRow[0]["OldNumberOfLine"] = (Convert.ToInt32(lstVolumeDeatils[0].OldNumberOfLines == null ? 0 : Convert.ToInt32(lstVolumeDeatils[0].OldNumberOfLines.ToString())));
                            }
                        }


                    }
                }
            }

            if (dtVolumeDetails != null && dtVolumeDetails.Rows.Count > 0) {
                gvVolume.DataSource = dtVolumeDetails;
                gvVolume.DataBind();

                gvVolumeHistory.DataSource = dtVolumeDetails;
                gvVolumeHistory.DataBind();
            }           
        }
    }

    protected void BindPOGrid()
    {

        if (txtPurchaseNumber.Text != string.Empty)
        {

            DataTable myDataTable = null;

            if (ViewState["myDataTable"] != null)
            {
                myDataTable = (DataTable)ViewState["myDataTable"];

                //Check Item alrady added
                DataRow[] drr = myDataTable.Select("PurchaseNumber='" + txtPurchaseNumber.Text.Trim() + "'");

                if (drr.Length > 0)
                {
                    return;
                }
            }
            else
            {

                myDataTable = new DataTable();
                DataColumn[] keys = new DataColumn[1];

                DataColumn auto = new DataColumn("AutoID", typeof(System.Int32));
                // specify it as auto increment field
                auto.AutoIncrement = true;
                auto.AutoIncrementSeed = 1;
                auto.ReadOnly = true;
                auto.Unique = true;
                myDataTable.Columns.Add(auto);
                keys[0] = auto;

                myDataTable.PrimaryKey = keys;

                DataColumn myDataColumn = new DataColumn();
                myDataColumn.ColumnName = "VendorID";
                myDataTable.Columns.Add(myDataColumn);

                myDataColumn = new DataColumn();
                myDataColumn.ColumnName = "PurchaseNumber";
                myDataTable.Columns.Add(myDataColumn);

                myDataColumn = new DataColumn();
                myDataColumn.ColumnName = "Original_due_date";
                myDataTable.Columns.Add(myDataColumn);

                myDataColumn = new DataColumn();
                myDataColumn.ColumnName = "VendorName";
                myDataTable.Columns.Add(myDataColumn);

                myDataColumn = new DataColumn();
                myDataColumn.ColumnName = "Pallets";
                myDataTable.Columns.Add(myDataColumn);

                myDataColumn = new DataColumn();
                myDataColumn.ColumnName = "Cartons";
                myDataTable.Columns.Add(myDataColumn);

                myDataColumn = new DataColumn();
                myDataColumn.ColumnName = "OutstandingLines";
                myDataTable.Columns.Add(myDataColumn);

                myDataColumn = new DataColumn();
                myDataColumn.ColumnName = "Qty_On_Hand";
                myDataTable.Columns.Add(myDataColumn);

                myDataColumn = new DataColumn();
                myDataColumn.ColumnName = "qty_on_backorder";
                myDataTable.Columns.Add(myDataColumn);

                myDataColumn = new DataColumn();
                myDataColumn.ColumnName = "SiteName";
                myDataTable.Columns.Add(myDataColumn);

                myDataColumn = new DataColumn();
                myDataColumn.ColumnName = "PurchaseOrderID";
                myDataTable.Columns.Add(myDataColumn);

                // Sprint 1 - Point 14 - Begin - Adding [Expected Date]
                myDataColumn = new DataColumn();
                myDataColumn.ColumnName = "ExpectedDate";
                myDataTable.Columns.Add(myDataColumn);
                // Sprint 1 - Point 14 - End - Adding [Expected Date]

                myDataColumn = new DataColumn();
                myDataColumn.ColumnName = "IsProvisionResonChangesUpdate";
                myDataTable.Columns.Add(myDataColumn);
            }


            Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE = new Up_PurchaseOrderDetailBE();
            UP_PurchaseOrderDetailBAL oUP_PurchaseOrderDetailBAL = new UP_PurchaseOrderDetailBAL();

            oUp_PurchaseOrderDetailBE.Action = "GetNewBookingPODetails";
            oUp_PurchaseOrderDetailBE.Purchase_order = txtPurchaseNumber.Text;
            oUp_PurchaseOrderDetailBE.CountryID = Convert.ToInt32(PreSiteCountryID);

            List<Up_PurchaseOrderDetailBE> lstPO = oUP_PurchaseOrderDetailBAL.GetProductOrderDetailsBAL(oUp_PurchaseOrderDetailBE);
            DataRow dataRow = myDataTable.NewRow();

            int OutstandingLines = 0;

            if (lstPO.Count > 0)
            {

                //oUp_PurchaseOrderDetailBE.Action = "GetStockDetails";
                //oUp_PurchaseOrderDetailBE.Purchase_order = txtPurchaseNumber.Text;
                //oUp_PurchaseOrderDetailBE.Site = new MAS_SiteBE();
                //oUp_PurchaseOrderDetailBE.Site.SiteID = lstPO[0].Site.SiteID;  // Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);

                //List<Up_PurchaseOrderDetailBE> lstPOStock = oUP_PurchaseOrderDetailBAL.GetPriorityBAL(oUp_PurchaseOrderDetailBE);

                //if (lstPOStock != null && lstPOStock.Count > 0)
                //{

                //    List<Up_PurchaseOrderDetailBE> lstPOTemp = lstPOStock.FindAll(delegate(Up_PurchaseOrderDetailBE p) { return Convert.ToDecimal(p.SKU.Qty_On_Hand) == 0 && (Convert.ToInt32(Convert.ToDecimal(p.Original_quantity)) - p.Qty_receipted > 0); });

                //    if (lstPOTemp != null && lstPOTemp.Count > 0)
                //    {
                //        dataRow["Qty_On_Hand"] = lstPOTemp.Count.ToString();
                //    }
                //    else
                //    {
                //        dataRow["Qty_On_Hand"] = "0";
                //    }

                //    //lstPOTemp = lstPOStock.FindAll(delegate(Up_PurchaseOrderDetailBE p) { return Convert.ToDecimal(p.SKU.qty_on_backorder) > 0 && (Convert.ToInt32(Convert.ToDecimal(p.Original_quantity)) - p.Qty_receipted > 0); });
                //    lstPOTemp = lstPOStock.FindAll(delegate(Up_PurchaseOrderDetailBE p)
                //    {
                //        return (Convert.ToDecimal(p.SKU.Qty_On_Hand) - Convert.ToDecimal(p.SKU.qty_on_backorder) < 0);
                //    }
                //  );
                //    if (lstPOTemp != null && lstPOTemp.Count > 0)
                //    {
                //        dataRow["qty_on_backorder"] = lstPOTemp.Count.ToString();
                //    }
                //    else
                //    {
                //        dataRow["qty_on_backorder"] = "0";
                //    }
                //}
                //else
                //{
                //    dataRow["Qty_On_Hand"] = "0";
                //    dataRow["qty_on_backorder"] = "0";
                //}

                dataRow["Qty_On_Hand"] = lstPO[0].Stockouts.ToString();
                dataRow["IsProvisionResonChangesUpdate"] = false;
                dataRow["qty_on_backorder"] = lstPO[0].Backorders.ToString();
                dataRow["OutstandingLines"] = lstPO[0].OutstandingLines.ToString();

                dataRow["VendorID"] = lstPO[0].Vendor.VendorID.ToString();
                dataRow["PurchaseNumber"] = txtPurchaseNumber.Text;
                dataRow["Original_due_date"] = lstPO[0].Original_due_date.Value.ToString("dd/MM/yyyy");

                // Sprint 1 - Point 14 - Begin - Adding [Expected Date]
                if (lstPO[0].Expected_date != null)
                    dataRow["ExpectedDate"] = lstPO[0].Expected_date.Value.ToString("dd/MM/yyyy");
                else
                    dataRow["ExpectedDate"] = lstPO[0].Original_due_date.Value.ToString("dd/MM/yyyy");
                // Sprint 1 - Point 14 - End - Adding [Expected Date]

                dataRow["VendorName"] = lstPO[0].Vendor.VendorName;
                //dataRow["OutstandingLines"] = lstPO.Count.ToString(); // lstPO[0].OutstandingLines.ToString(); // 
                //dataRow["Qty_On_Hand"] = lstPO[0].SKU.Qty_On_Hand;
                //dataRow["qty_on_backorder"] = lstPO[0].SKU.qty_on_backorder;
                dataRow["SiteName"] = lstPO[0].Site.SiteName;
                dataRow["PurchaseOrderID"] = lstPO[0].PurchaseOrderID.ToString();
                OutstandingLines = lstPO.Count;
                myDataTable.Rows.Add(dataRow);
            }
            else
            {
                if (IsBookingValidationExcluded == true)
                {
                    dataRow["IsProvisionResonChangesUpdate"] = false;
                    dataRow["VendorID"] = "-1";
                    dataRow["PurchaseNumber"] = txtPurchaseNumber.Text;
                    dataRow["Original_due_date"] = ActualSchedulDateTime.ToString("dd/MM/yyyy");

                    // Sprint 1 - Point 14 - Begin - Adding [Expected Date]
                    dataRow["ExpectedDate"] = ActualSchedulDateTime.ToString("dd/MM/yyyy");
                    // Sprint 1 - Point 14 - End - Adding [Expected Date]

                    dataRow["VendorName"] = "UNKNOWN";
                    dataRow["OutstandingLines"] = "1";
                    dataRow["Qty_On_Hand"] = "0";
                    dataRow["qty_on_backorder"] = "0";
                    dataRow["SiteName"] = ddlSite.innerControlddlSite.SelectedItem.Text;
                    dataRow["PurchaseOrderID"] = "0";
                    OutstandingLines = 1;
                    myDataTable.Rows.Add(dataRow);
                }

                #region Adding PO for OD user.

                /*
                 * [ISSUE No - 1] by client in [VIP Outstanding Issue Overview 22-04-13 Testing.xls] file.
                 * When Office Depot are making a booking, we should be allowed to add POs for incorrect vendors. The system should warn of the potential problem but
                 * Previously, we had an issue where the PO was being added whether the user hit CONTINUE or BACK. This was fixed but now we are unable to add to
                 *
                 * As per upper side problem, I have added this code as per Vikram suggestion on Date 24/04/2013 6:20 PM .
                 * This code will allow to add the non matched PO for vendor if logged-in user is OD user. 
                 * 
                 * Also this code will add PO in case on the [Continue] button clicked by only OD user.
                 */

                string Role = Session["Role"].ToString().Trim().ToLower();
                if (Role != "vendor" && Role != "carrier" && Convert.ToString(ViewState["ByContinue"]).Trim().ToLower() == "bycontinue")
                {
                    dataRow["IsProvisionResonChangesUpdate"] = false;
                    dataRow["VendorID"] = "-1";
                    dataRow["PurchaseNumber"] = txtPurchaseNumber.Text;
                    dataRow["Original_due_date"] = ActualSchedulDateTime.ToString("dd/MM/yyyy");
                    // Sprint 1 - Point 14 - Begin - Adding [Expected Date]
                    dataRow["ExpectedDate"] = ActualSchedulDateTime.ToString("dd/MM/yyyy");
                    // Sprint 1 - Point 14 - End - Adding [Expected Date]

                    dataRow["VendorName"] = "UNKNOWN";
                    dataRow["OutstandingLines"] = "1";
                    dataRow["Qty_On_Hand"] = "0";
                    dataRow["qty_on_backorder"] = "0";
                    dataRow["SiteName"] = ddlSite.innerControlddlSite.SelectedItem.Text;
                    dataRow["PurchaseOrderID"] = 0;
                    myDataTable.Rows.Add(dataRow);
                    ViewState["ByContinue"] = null;
                }

                #endregion
            }



            if (myDataTable != null && myDataTable.Rows.Count > 0)
            {
                gvPO.DataSource = myDataTable;
                gvPO.DataBind();
            }

            //Fill entered volume----
            DataTable dtVolumeDetails = null;
            if (ViewState["dtVolumeDetails"] != null)
            {
                dtVolumeDetails = (DataTable)ViewState["dtVolumeDetails"];

                foreach (GridViewRow Row in gvVolume.Rows)
                {
                    HiddenField hdngvVendorID = (HiddenField)Row.FindControl("hdngvVendorID");

                    DataRow[] UpdateRow = dtVolumeDetails.Select("VendorID='" + hdngvVendorID.Value.ToString() + "'");

                    if (UpdateRow != null && UpdateRow.Length > 0)
                    {

                        ucNumericTextbox txtVendorPallets = (ucNumericTextbox)Row.FindControl("txtVendorPallets");
                        ucNumericTextbox txtVendorCartons = (ucNumericTextbox)Row.FindControl("txtVendorCartons");

                        // Sprint 1 - Point 18 - Start - Carrier lines to be editable
                        ucNumericTextbox txtLinesToBeDelivered = (ucNumericTextbox)Row.FindControl("txtLinesToBeDelivered");
                        // Sprint 1 - Point 18 - End - Carrier lines to be editable

                        UpdateRow[0]["Pallets"] = txtVendorPallets.Text;
                        UpdateRow[0]["Cartons"] = txtVendorCartons.Text;

                        // Sprint 1 - Point 18 - Start - Carrier lines to be editable
                        UpdateRow[0]["LinesToBeDelivered"] = txtLinesToBeDelivered.Text;
                        // Sprint 1 - Point 18 - End - Carrier lines to be editable                   
                    }
                }
            }
            //------------------------

            int iTotalLines = 0;
            foreach (DataRow dr in myDataTable.Rows)
            {
                iTotalLines += Convert.ToInt32(dr["OutstandingLines"]);
            }

            if (myDataTable != null && myDataTable.Rows.Count > 0)
            {
                ScriptManager.RegisterStartupScript(Page, this.GetType(), "btnshow", "$get('" + btnProceedBooking.ClientID + "').style.display='';", true);
                ViewState["myDataTable"] = myDataTable;


                //Build Volume Details Settings--------------//
                dtVolumeDetails = null;
                if (ViewState["dtVolumeDetails"] != null)
                {
                    dtVolumeDetails = (DataTable)ViewState["dtVolumeDetails"];

                    if (lstPO != null && lstPO.Count > 0)
                    {
                        DataRow[] UpdateRow = dtVolumeDetails.Select("VendorID='" + lstPO[0].Vendor.VendorID.ToString() + "'");

                        if (UpdateRow != null && UpdateRow.Length > 0)
                        {
                            UpdateRow[0]["ExpectedLines"] = (Convert.ToInt32(UpdateRow[0]["ExpectedLines"]) + OutstandingLines).ToString();
                            UpdateRow[0]["NoOfPO"] = (Convert.ToInt32(UpdateRow[0]["NoOfPO"]) + 1).ToString();
                        }
                        else
                        {
                            DataRow drdataRow = dtVolumeDetails.NewRow();
                            if (lstPO.Count > 0)
                            {
                                drdataRow["VendorID"] = lstPO[0].Vendor.VendorID.ToString();
                                drdataRow["VendorName"] = lstPO[0].Vendor.VendorName;
                                drdataRow["Pallets"] = "";
                                drdataRow["Cartons"] = "";
                                drdataRow["ExpectedLines"] = OutstandingLines.ToString(); // lstPO[0].OutstandingLines.ToString();
                                drdataRow["NoOfPO"] = "1";

                                // Sprint 1 - Point 18 - Start - Carrier lines to be editable
                                drdataRow["LinesToBeDelivered"] = "";
                                // Sprint 1 - Point 18 - End - Carrier lines to be editable

                            }
                            dtVolumeDetails.Rows.Add(drdataRow);
                        }
                    }
                    else
                    {
                        DataRow[] UpdateRow = dtVolumeDetails.Select("VendorName='UNKNOWN'");
                        if (UpdateRow != null && UpdateRow.Length > 0)
                        {
                            UpdateRow[0]["ExpectedLines"] = (iTotalLines).ToString();
                            UpdateRow[0]["NoOfPO"] = (iTotalLines).ToString();
                        }
                    }
                }
                else
                {

                    dtVolumeDetails = new DataTable();

                    DataColumn auto = new DataColumn("AutoID", typeof(System.Int32));
                    // specify it as auto increment field
                    auto.AutoIncrement = true;
                    auto.AutoIncrementSeed = 1;
                    auto.ReadOnly = true;
                    auto.Unique = true;
                    dtVolumeDetails.Columns.Add(auto);

                    DataColumn myDataColumn = new DataColumn();
                    myDataColumn.ColumnName = "VendorID";
                    dtVolumeDetails.Columns.Add(myDataColumn);

                    myDataColumn = new DataColumn();
                    myDataColumn.ColumnName = "VendorName";
                    dtVolumeDetails.Columns.Add(myDataColumn);

                    myDataColumn = new DataColumn();
                    myDataColumn.ColumnName = "Pallets";
                    dtVolumeDetails.Columns.Add(myDataColumn);

                    myDataColumn = new DataColumn();
                    myDataColumn.ColumnName = "Cartons";
                    dtVolumeDetails.Columns.Add(myDataColumn);

                    myDataColumn = new DataColumn();
                    myDataColumn.ColumnName = "ExpectedLines";
                    dtVolumeDetails.Columns.Add(myDataColumn);

                    myDataColumn = new DataColumn();
                    myDataColumn.ColumnName = "NoOfPO";
                    dtVolumeDetails.Columns.Add(myDataColumn);

                    // Sprint 1 - Point 18 - Start - Carrier lines to be editable
                    myDataColumn = new DataColumn();
                    myDataColumn.ColumnName = "LinesToBeDelivered";
                    dtVolumeDetails.Columns.Add(myDataColumn);
                    // Sprint 1 - Point 18 - End - Carrier lines to be editable

                    myDataColumn = new DataColumn();
                    myDataColumn.ColumnName = "OldNumberOfPallet";
                    dtVolumeDetails.Columns.Add(myDataColumn);


                    myDataColumn = new DataColumn();
                    myDataColumn.ColumnName = "OldNumberOfCartons";
                    dtVolumeDetails.Columns.Add(myDataColumn);


                    myDataColumn = new DataColumn();
                    myDataColumn.ColumnName = "OldNumberOfLine";
                    dtVolumeDetails.Columns.Add(myDataColumn);

                    DataRow drdataRow = dtVolumeDetails.NewRow();
                    if (lstPO.Count > 0)
                    {
                        drdataRow["VendorID"] = lstPO[0].Vendor.VendorID.ToString();
                        drdataRow["VendorName"] = lstPO[0].Vendor.VendorName;
                        drdataRow["Pallets"] = "";
                        drdataRow["Cartons"] = "";
                        drdataRow["ExpectedLines"] = iTotalLines.ToString();
                        drdataRow["NoOfPO"] = "1";
                        drdataRow["LinesToBeDelivered"] = "";
                    }
                    else
                    {
                        if (IsBookingValidationExcluded == true)
                        {
                            drdataRow["VendorID"] = "-1";
                            drdataRow["VendorName"] = "UNKNOWN";
                            drdataRow["Pallets"] = "";
                            drdataRow["Cartons"] = "";
                            drdataRow["ExpectedLines"] = "1";
                            drdataRow["NoOfPO"] = "1";
                            drdataRow["LinesToBeDelivered"] = "";
                        }
                    }
                    dtVolumeDetails.Rows.Add(drdataRow);
                }
                if (dtVolumeDetails != null && dtVolumeDetails.Rows.Count > 0)
                {
                    ViewState["dtVolumeDetails"] = dtVolumeDetails;

                    gvVolume.DataSource = dtVolumeDetails;
                    gvVolume.DataBind();
                }
                            
                else
                {
                    ViewState["dtVolumeDetails"] = null;
                }
                //------------end volume details-------------//

            }
            else
            {
                ScriptManager.RegisterStartupScript(Page, this.GetType(), "btnhide", "$get('" + btnProceedBooking.ClientID + "').style.display='none';", true);
                ViewState["myDataTable"] = null;
            }
            //for (int iCount = 0; iCount < myDataTable.Rows.Count; iCount++) {
            //    TotalLines += Convert.ToInt32(lstPO[0].OutstandingLines.ToString()); //Convert.ToInt32(dataRow["OutstandingLines"]);
            //}

            TotalLines = iTotalLines.ToString();

            txtPurchaseNumber.Text = string.Empty;
        }

    }

    protected void gvPO_RowDataBound(object sender, GridViewRowEventArgs e)
    {

        string DelMsg = WebCommon.getGlobalResourceValue("RemovePO");

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            // loop all data rows
            if (hdnBookingTypeID.Value == "5" && hdnProvisionalReason.Value == "EDITED" && hdnLine.Value != "0" && Session["Role"].ToString().ToLower() != "carrier")
            {
                if (gvPO.DataKeys[e.Row.RowIndex].Values[1].ToString() == "True")
                {
                    e.Row.Cells[0].ForeColor = Color.Red;
                    e.Row.Cells[1].ForeColor = Color.Red;
                    e.Row.Cells[2].ForeColor = Color.Red;
                    e.Row.Cells[3].ForeColor = Color.Red;
                    e.Row.Cells[4].ForeColor = Color.Red;
                    e.Row.Cells[5].ForeColor = Color.Red;
                    e.Row.Cells[6].ForeColor = Color.Red;
                    e.Row.Cells[7].ForeColor = Color.Red;
                }
            }

            foreach (DataControlFieldCell cell in e.Row.Cells)
            {
                // check all cells in one row
                foreach (Control control in cell.Controls)
                {
                    LinkButton button = control as LinkButton;
                    if (button != null && button.CommandName == "Delete")
                        // Add delete confirmation
                        button.OnClientClick = "return (confirm('" + DelMsg + "')) ;";
                }
            }
        }
    }

    protected void gvPO_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {

        DataTable myDataTable = null;


        if (ViewState["myDataTable"] != null)
        {
            myDataTable = (DataTable)ViewState["myDataTable"];
        }

        string sVendorId = string.Empty;

        if (myDataTable != null)
        {

            //Update Volume Details-----------//           
            DataRow dr = null;//= myDataTable.Rows.Find(e.Keys[0]);
            foreach (DataRow dataRow in myDataTable.Rows)
            {
                if (dataRow["AutoID"].ToString() == e.Keys[0].ToString())
                {
                    dr = dataRow;
                    break;
                }
            }

            DataTable dtVolumeDetails = null;
            if (ViewState["dtVolumeDetails"] != null)
            {
                dtVolumeDetails = (DataTable)ViewState["dtVolumeDetails"];

                DataRow[] UpdateRow = dtVolumeDetails.Select("VendorID='" + dr["VendorID"].ToString() + "'");

                if (UpdateRow != null && UpdateRow.Length > 0)
                {
                    sVendorId = dr["VendorID"].ToString();
                    if (Convert.ToInt32(UpdateRow[0]["NoOfPO"]) > 1)
                    {
                        UpdateRow[0]["ExpectedLines"] = (Convert.ToInt32(UpdateRow[0]["ExpectedLines"]) - Convert.ToInt32(dr["OutstandingLines"])).ToString();
                        UpdateRow[0]["NoOfPO"] = (Convert.ToInt32(UpdateRow[0]["NoOfPO"]) - 1).ToString();
                    }
                    else
                    {
                        dtVolumeDetails.Rows.Remove(UpdateRow[0]);
                        dtVolumeDetails.AcceptChanges();
                    }
                }
                if (dtVolumeDetails != null && dtVolumeDetails.Rows.Count > 0)
                {
                    ViewState["dtVolumeDetails"] = dtVolumeDetails;
                    gvVolume.DataSource = dtVolumeDetails;
                    gvVolume.DataBind();
                }
                else
                {
                    gvVolume.DataSource = null;
                    gvVolume.DataBind();
                    ViewState["dtVolumeDetails"] = null;
                }
                
            }
            //----------end volume details update-----------------//

            //----Stage 5 Point 1------//
            DataRow[] VendorRow = myDataTable.Select("VendorID='" + sVendorId + "'");
            if (VendorRow != null && VendorRow.Length <= 1) {

                List<UP_VendorBE> lstUPVendors = null;
                UP_VendorBE[] newArray = null;

                if (ViewState["UPVendors"] != null) {
                    newArray = (UP_VendorBE[])ViewState["UPVendors"];
                    lstUPVendors = new List<UP_VendorBE>(newArray);
                    if (lstUPVendors.FindAll(delegate(UP_VendorBE v) { return v.VendorID == Convert.ToInt32(sVendorId); }).Count > 0) {
                        lstUPVendors.Remove(lstUPVendors.FindAll(delegate(UP_VendorBE v) { return v.VendorID == Convert.ToInt32(sVendorId); })[0]);
                        ViewState.Add("UPVendors", lstUPVendors.ToArray());
                    }
                }
            }
            //-------------------------//

            myDataTable.Rows.RemoveAt(e.RowIndex); // (Convert.ToInt32(e.Keys[0]) - 1);
            myDataTable.AcceptChanges();
            gvPO.DataSource = myDataTable;
            gvPO.DataBind();
        }

        if (myDataTable != null && myDataTable.Rows.Count > 0)
        {
            ScriptManager.RegisterStartupScript(Page, this.GetType(), "btnshow", "$get('" + btnProceedBooking.ClientID + "').style.display='';", true);
            ViewState["myDataTable"] = myDataTable;
            
        }
        else
        {
            ScriptManager.RegisterStartupScript(Page, this.GetType(), "btnhide", "$get('" + btnProceedBooking.ClientID + "').style.display='none';", true);
            ViewState["myDataTable"] = null;
            ViewState["UPVendors"] = null;
        }


        int iTotalLines = 0;
        foreach (DataRow dataRow in myDataTable.Rows)
        {
            iTotalLines += Convert.ToInt32(dataRow["OutstandingLines"]);
        }

        TotalLines = iTotalLines.ToString();        
    }

    protected void gvPO_Sorting(object sender, GridViewSortEventArgs e)
    {
        if (ViewState["myDataTable"] != null)
        {
            DataTable myDataTable = (DataTable)ViewState["myDataTable"];
            ViewState["sortDirection"] = ViewState["sortDirection"] == null || Convert.ToString(ViewState["sortDirection"]) == "DESC" ? "ASC" : "DESC";
            DataView view = myDataTable.DefaultView;
            view.Sort = e.SortExpression + " " + Convert.ToString(ViewState["sortDirection"]);
            gvPO.DataSource = view;
            gvPO.DataBind();
        }
    }

    private void DeliverTypenontime()
    {
        DataTable dt1;

        MASSIT_NonTimeDeliveryBE oMASCNT_NonTimeDeliveryBE = new MASSIT_NonTimeDeliveryBE();
        APPSIT_NonTimeDeliveryBAL oMASCNT_NonTimeDeliveryBAL = new APPSIT_NonTimeDeliveryBAL();

        oMASCNT_NonTimeDeliveryBE.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
        oMASCNT_NonTimeDeliveryBE.Action = "GetDeliveries";
        dt1 = oMASCNT_NonTimeDeliveryBAL.GetNonTimeDeliveryDetailsBAL(oMASCNT_NonTimeDeliveryBE, "");

        strDeliveryNonTime = string.Empty;

        for (int iCount = 0; iCount < dt1.Rows.Count; iCount++)
        {
            strDeliveryNonTime += dt1.Rows[iCount]["SiteDeliveryID"].ToString() + ",";
        }
    }

    protected void btnBack_1_Click(object sender, EventArgs e)
    {
        txtPurchaseNumber.Text = string.Empty;
        ViewState["myDataTable"] = null;
        ViewState["dtVolumeDetails"] = null;
        gvPO.DataSource = null;
        gvPO.DataBind();
        gvVolume.DataSource = null;
        gvVolume.DataBind();
        mvBookingEdit.ActiveViewIndex = 0;
        //txtPurchaseNumber.Text = string.Empty;
        //mvBookingEdit.ActiveViewIndex = 0;
    }
    protected void btnNotHappy_Click(object sender, EventArgs e)
    {
        EncryptQueryString("APPBok_BookingOverview.aspx");
    }
    protected void btnProceedBooking_Click(object sender, EventArgs e)
    {
        if (hdnCurrentMode.Value == "Edit" && hdnPreviousVehicleType.Value != ddlVehicleType.SelectedValue)
        {
            MASSIT_VehicleDoorMatrixBE oMASSIT_VehicleDoorMatrixBE = new MASSIT_VehicleDoorMatrixBE();

            APPSIT_VehicleDoorMatrixBAL oMASSIT_VehicleDoorMatrixBAL = new APPSIT_VehicleDoorMatrixBAL();

            oMASSIT_VehicleDoorMatrixBE.Action = "VechileTypeMatched";
            oMASSIT_VehicleDoorMatrixBE.VehicleTypeID = Convert.ToInt32(ddlVehicleType.SelectedValue);
            oMASSIT_VehicleDoorMatrixBE.PreviousVehicleTypeID = Convert.ToInt32(hdnPreviousVehicleType.Value);
            isDoorTypeNotChanged = oMASSIT_VehicleDoorMatrixBAL.isVehcileTypeMatchedBAL(oMASSIT_VehicleDoorMatrixBE);
        }

        if (txtSchedulingdate.innerControltxtDate.Value != GetQueryStringValue("Scheduledate").ToString() && hdnCurrentMode.Value == "Edit")
        {
            btnNotHappy.Visible = true;
        }
        else
        {
            btnNotHappy.Visible = false;
        }
        //new logic 
        // get the applicable time window from MASSIT_TimeWindow table 
        // and then recommend the time
        bool isWarning = false;
        DataTable myDataTable = null;
        iTotalPallets = 0;
        iTotalCartons = 0;
        iTotalLines = 0;
        if (ViewState["myDataTable"] != null)
        {
            myDataTable = (DataTable)ViewState["myDataTable"];
        }
        else
        {
            myDataTable = new DataTable();
        }

        string errMsg = WebCommon.getGlobalResourceValue("BK_MS_17"); // "The Purchase Order/Delivery Reference field must be populated. Please enter either a Purchase Order number or a deliver reference";
        if (myDataTable.Rows.Count <= 0)
        {
            ShowErrorMessage(errMsg, "BK_MS_17");
            return;
        }
        //-------------//               

        if (isWarning == false)
            ValidateEnteredVolume();
        else
            ShowWarningMessages();

        //-------------------------------//    
    }

    //-----------Start Stage 9 point 2a/2b--------------//
    protected void btnReject_Click(object sender, EventArgs e) {
        //string errMsg = WebCommon.getGlobalResourceValue("DeleteBookingConfirmOD");
        //AddWarningMessages(errMsg, "Booking");
        //ShowWarningMessages();
        mdlRejectProvisional.Show();
    }

    protected void btnContinue_2_Click(object sender, EventArgs e) {
        DeleteBooking();
    }

    protected void btnBack_2_Click(object sender, EventArgs e) {

        mdlRejectProvisional.Hide();
    }

    private void DeleteBooking() {
        APPBOK_BookingBE oAPPBOK_BookingBE = new APPBOK_BookingBE();
        APPBOK_BookingBAL oAPPBOK_BookingBAL = new APPBOK_BookingBAL();
        oAPPBOK_BookingBE.FixedSlot = new BusinessEntities.ModuleBE.Appointment.SiteSettings.MASSIT_FixedSlotBE();
        oAPPBOK_BookingBE.Action = "DeleteBooking";
        oAPPBOK_BookingBE.UserID = Convert.ToInt32(Session["UserID"]);
        oAPPBOK_BookingBE.BookingID = Convert.ToInt32(GetQueryStringValue("ID").ToString());
        oAPPBOK_BookingBAL.DeleteBookingDetailBAL(oAPPBOK_BookingBE);
        #region Logic to send emails ...
        var BookingId = Convert.ToInt32(GetQueryStringValue("ID").ToString());
        oAPPBOK_BookingBE = new APPBOK_BookingBE();
        oAPPBOK_BookingBAL = new APPBOK_BookingBAL();
        oAPPBOK_BookingBE.Action = "GetDeletedBookingDeatilsByID";
        oAPPBOK_BookingBE.BookingID = BookingId;
        var lstBooking = oAPPBOK_BookingBAL.GetDeletedBookingDetailsByIDBAL(oAPPBOK_BookingBE);
        if (lstBooking.Count > 0)
            this.SendProvisionalRefusalLetterMail(lstBooking, BookingId);

        #endregion

        if (GetQueryStringValue("PNTB") != null && GetQueryStringValue("PNTB") == "True")
        {
            EncryptQueryString("~/ModuleUI/Appointment/ProvisionalBookings.aspx");
        }
        else if (GetQueryStringValue("BP") != null && GetQueryStringValue("BP") == "Pro")
        {
            EncryptQueryString("~/ModuleUI/Appointment/ProvisionalBookings.aspx");
        }
        else
        {
            EncryptQueryString("APPBok_BookingOverview.aspx");
        }
    }
    //-----------End Stage 9 point 2a/2b--------------//

    private void ValidateEnteredVolume() {
        string errMsg = WebCommon.getGlobalResourceValue("BK_MS_19"); // "There is insufficient volume information entered for this booking. The number of lines must be >0 and either pallets or cartons must be >0.";


        foreach (GridViewRow gvRow in gvVolume.Rows) {
            if (gvRow.RowType == DataControlRowType.DataRow) {
                ucNumericTextbox txtVendorPallets = (ucNumericTextbox)gvRow.FindControl("txtVendorPallets");
                ucNumericTextbox txtVendorCartons = (ucNumericTextbox)gvRow.FindControl("txtVendorCartons");

                // Added new code to get the exact Expected line for particular row.
                ucLiteral ltVendorExpectedLines = (ucLiteral)gvRow.FindControl("ltVendorExpectedLines");
                if (ltVendorExpectedLines != null) {
                    TotalLines = Convert.ToString(ltVendorExpectedLines.Text);
                }

                // Sprint 1 - Point 18 - Start - Carrier lines to be editable
                ucNumericTextbox txtLinesToBeDelivered = (ucNumericTextbox)gvRow.FindControl("txtLinesToBeDelivered");
                // Sprint 1 - Point 18 - End - Carrier lines to be editable

                string sTotalPallets = txtVendorPallets.Text.Trim() != string.Empty ? txtVendorPallets.Text.Trim() : "0";
                string sTotalCartons = txtVendorCartons.Text.Trim() != string.Empty ? txtVendorCartons.Text.Trim() : "0";

                // Sprint 1 - Point 18 - Start - Carrier lines to be editable
                string sLinesToBeDelivered = txtLinesToBeDelivered.Text.Trim() != string.Empty ? txtLinesToBeDelivered.Text.Trim() : "0";
                // Sprint 1 - Point 18 - End - Carrier lines to be editable

                if (sTotalPallets == "0" && sTotalCartons == "0" && TotalLines == "0") {
                    ShowErrorMessage(errMsg, "BK_MS_19");
                    return;
                }
                else if ((Convert.ToInt32(TotalLines) == 0) || (Convert.ToInt32(sTotalPallets) == 0 && Convert.ToInt32(sTotalCartons) == 0)) {
                    ShowErrorMessage(errMsg, "BK_MS_19");
                    return;
                }
                // Sprint 1 - Point 18 - Start - Carrier lines to be editable
                else if (sLinesToBeDelivered == "0" || sLinesToBeDelivered == string.Empty) {
                    errMsg = WebCommon.getGlobalResourceValue("BK_MS_32_Message");
                    ShowErrorMessage(errMsg, "BK_MS_32_Message");
                    return;
                }

                iTotalPallets += Convert.ToInt32(sTotalPallets);
                iTotalCartons += Convert.ToInt32(sTotalCartons);
                iTotalLines += Convert.ToInt32(sLinesToBeDelivered);

                if (Convert.ToInt32(sLinesToBeDelivered) > Convert.ToInt32(TotalLines)) {
                    if (!(hdnCurrentMode.Value == "Edit" && GetQueryStringValue("BP") != null && GetQueryStringValue("BP").ToString() == "Pro")) {  // If coming from Provisional page , ignore less lines check -- Phase 12 R2 Point 2
                        ViewState["WarningStatus"] = 0;
                        errMsg = WebCommon.getGlobalResourceValue("BK_MS_31_Warning");
                        ltWarningConfirm.Text = errMsg;
                        mdlWarningConfirm.Show();
                        return;
                    }
                }
                else if (Convert.ToInt32(sLinesToBeDelivered) < Convert.ToInt32(TotalLines)) {
                    if (!(hdnCurrentMode.Value == "Edit" && GetQueryStringValue("BP") != null && GetQueryStringValue("BP").ToString() == "Pro")) {  // If coming from Provisional page , ignore less lines check -- Phase 12 R2 Point 2
                        ViewState["WarningStatus"] = 0;
                        errMsg = WebCommon.getGlobalResourceValue("BK_MS_34_Warning");
                        ltWarningConfirm.Text = errMsg;
                        mdlWarningConfirm.Show();
                        return;
                    }
                }
            }
        }

        hdnCurrentWeekDay.Value = ActualSchedulDateTime.ToString("ddd").ToLower();

        //---Stage 5 Point 1----//
        if (ddlSite.IsBookingExclusions) {
            bool isVendorVolumeNotValid = false;
            foreach (GridViewRow gvRow in gvVolume.Rows) {
                if (gvRow.RowType == DataControlRowType.DataRow) {
                    ucNumericTextbox txtVendorPallets = (ucNumericTextbox)gvRow.FindControl("txtVendorPallets");
                    ucNumericTextbox txtLinesToBeDelivered = (ucNumericTextbox)gvRow.FindControl("txtLinesToBeDelivered");

                    string sTotalPallets = txtVendorPallets.Text.Trim() != string.Empty ? txtVendorPallets.Text.Trim() : "0";
                    string sLinesToBeDelivered = txtLinesToBeDelivered.Text.Trim() != string.Empty ? txtLinesToBeDelivered.Text.Trim() : "0";

                    HiddenField hdnPOVendorId = (HiddenField)gvRow.FindControl("hdnPOVendorId");                    

                    if (isEnteredVendorVolumeNotValid(Convert.ToInt32(hdnPOVendorId.Value), Convert.ToInt32(sTotalPallets), Convert.ToInt32(sLinesToBeDelivered)))
                        isVendorVolumeNotValid = true;
                }
            }
            if (isVendorVolumeNotValid)
                ShowWarningMessages();
            else
                this.ProceedBooking(iTotalCartons, iTotalPallets, iTotalLines);
        }
        else
            this.ProceedBooking(iTotalCartons, iTotalPallets, iTotalLines);
        //----------------------------------------//
    }

    //----Stage 5 Point 1----//
    private bool isEnteredVendorVolumeNotValid(int iVendorID, int VenPallets, int VenLines) {
        //----Check Vendor Constraints----//
        UP_VendorBE[] newArray = null;
        List<UP_VendorBE> newList = null;
        string errMsg = string.Empty;
        bool isWarning = false;

        if (ViewState["UPVendors"] != null)
        {
            newArray = (UP_VendorBE[])ViewState["UPVendors"];
            newList = new List<UP_VendorBE>(newArray);
        }

        if (newList != null && newList.Count > 0) {
            newList = newList.FindAll(delegate(UP_VendorBE ven) {
                return ven.VendorDetails.IsConstraintsDefined == true && ven.VendorID == iVendorID;
            });
        }

        if (newList != null && newList.Count > 0) {

            if (hdnCurrentRole.Value == "Vendor" || hdnCurrentRole.Value == "Carrier") {
                //errMsg = WebCommon.getGlobalResourceValue("BK_MS_18_EXT"); // "The volume being booked in exceeds the scheduled capacity for you.";
                errMsg = WebCommon.getGlobalResourceValue("BK_MS_18_INT") + "[ " + newList[0].VendorName + " ]."; // "The volume being booked in exceeds the scheduled capacity for vendor [ " + ltvendorName.Text + " ]."; 
            }
            else {
                errMsg = WebCommon.getGlobalResourceValue("BK_MS_18_INT") + "[ " + newList[0].VendorName + " ]."; // "The volume being booked in exceeds the scheduled capacity for vendor [ " + ltvendorName.Text + " ]."; 
            }

            errMsg = errMsg + "<br>";
            if (newList[0].VendorDetails.APP_MaximumPallets > 0) {
                errMsg = errMsg + WebCommon.getGlobalResourceValue("BK_MS_18_INT_MAXPallets").Replace("##MaxPallets##",newList[0].VendorDetails.APP_MaximumPallets.ToString()) ;
                errMsg = errMsg + "&nbsp;&nbsp;&nbsp;&nbsp;";
            }
            if (newList[0].VendorDetails.APP_MaximumLines > 0)
                errMsg = errMsg + WebCommon.getGlobalResourceValue("BK_MS_18_INT_MAXLines").Replace("##MaxLines##", newList[0].VendorDetails.APP_MaximumLines.ToString());

            if (VenPallets != 0) {
                if (Convert.ToInt32(VenPallets) > newList[0].VendorDetails.APP_MaximumPallets) {
                    //Show err msg
                    if (hdnCurrentRole.Value == "Vendor" || hdnCurrentRole.Value == "Carrier") {

                        ShowErrorMessage(errMsg, "BK_MS_18_EXT_VEN");
                        return true;

                    }
                    else {
                        AddWarningMessages(errMsg, "BK_MS_18_INT_VEN_" + iVendorID.ToString());
                        isWarning = true;
                    }
                }
            }

            if (VenLines != 0) {
                if (Convert.ToInt32(VenLines) > newList[0].VendorDetails.APP_MaximumLines) {
                    //Show err msg
                    if (hdnCurrentRole.Value == "Vendor" || hdnCurrentRole.Value == "Carrier") {
                        ShowErrorMessage(errMsg, "BK_MS_18_EXT_VEN");
                        return true;
                    }
                    else {
                        AddWarningMessages(errMsg, "BK_MS_18_INT_VEN_" + iVendorID.ToString());
                        isWarning = true;
                    }
                }
            }
        }

        return isWarning;
    }
    //----------------------//

    //----Stage 4 Point 13----//
    private void ProceedBooking(int iTotalCartons, int iTotalPallets, int iTotalLines) {
        bool isWarning = false;
        string errMsg = String.Empty;
                
        //Check Site Setting Max Volume per delivery per site
        MAS_SiteBE singleSiteSetting = GetSingleSiteSetting(Convert.ToInt32(ddlSite.innerControlddlSite.SelectedValue));
        if (
           (singleSiteSetting.MaxPalletsPerDelivery > 0
           && Convert.ToInt32(iTotalPallets) > singleSiteSetting.MaxPalletsPerDelivery) ||
           (singleSiteSetting.MaxCartonsPerDelivery > 0 &&
           Convert.ToInt32(iTotalCartons) > singleSiteSetting.MaxCartonsPerDelivery) ||
           (singleSiteSetting.MaxLinesPerDelivery > 0 &&
           Convert.ToInt32(iTotalLines) > singleSiteSetting.MaxLinesPerDelivery)) {

            errMsg = WebCommon.getGlobalResourceValue("BK_MS_35"); //You have broken the maximum allowed volume for this site. Please check your entry for pallets, cartons & lines.
            if (hdnCurrentRole.Value == "Vendor" || hdnCurrentRole.Value == "Carrier") {
                ShowErrorMessage(errMsg, "BK_MS_35");
                return;
            }
            else {
                AddWarningMessages(errMsg, "BK_MS_35");
                isWarning = true;
            }

            if (isWarning)
                ShowWarningMessages();
        }
        else {
            SuggestBooking(iTotalCartons, iTotalPallets, iTotalLines);
        }
        //----------------------------------//
    }

    // Sprint 1 - Point 18 - Start - Carrier lines to be editable
    private void SuggestBooking(int iTotalCartons, int iTotalPallets, int iTotalLines)
    {
        //string strVendorIDs = string.Empty;
            
        TotalCartons = iTotalCartons.ToString();
        TotalPallets = iTotalPallets.ToString();
        TotalLines = iTotalLines.ToString();

        bool isWarning = false;
        string errMsg = String.Empty;

        //----Sprint 3a start Check Carrier Constraints----//
        MASCNT_CarrierBE[] newArray = null;
        List<MASCNT_CarrierBE> newList = null;

        //ViewState.Add("lstCarrier", lstCarrierBE.ToArray());
        if (ViewState["lstCarrier"] != null)
        {
            newArray = (MASCNT_CarrierBE[])ViewState["lstCarrier"];
            newList = new List<MASCNT_CarrierBE>(newArray);
        }

        if (hdnCurrentRole.Value == "Carrier")
        {
            btnEnterAltTime.Visible = false;
            errMsg = WebCommon.getGlobalResourceValue("BK_MS_18_EXT"); // "The volume being booked in exceeds the scheduled capacity for you.";
        }
        else
        {
            btnWindowAlternateSlotVendor.Visible = false;
            errMsg = WebCommon.getGlobalResourceValue("BK_MS_18_INT_Carrier") + "[ " + ddlCarrier.SelectedItem.Text + " ]."; // "The volume being booked in exceeds the scheduled capacity for vendor [ " + ltvendorName.Text + " ]."; 
        }

        if (newList != null && newList.Count > 0)
        {

            if (TotalPallets != "0" && newList[0].APP_MaximumPallets != 0)
            {
                if (Convert.ToInt32(TotalPallets) > newList[0].APP_MaximumPallets)
                {
                    //Show err msg
                    if (hdnCurrentRole.Value == "Carrier")
                    {
                        ShowErrorMessage(errMsg, "BK_MS_18_EXT");
                        return;
                    }
                    else
                    {
                        if (!(hdnCurrentMode.Value == "Edit" && GetQueryStringValue("BP") != null && GetQueryStringValue("BP").ToString() == "Pro")) {  // If coming from Provisional page , ignore fixed slot capacity -- Phase 12 R2 Point 2
                            AddWarningMessages(errMsg, "BK_MS_18_INT");
                            isWarning = true;
                        }
                    }
                }
            }

            if (TotalLines != "0" && newList[0].APP_MaximumLines != 0)
            {
                if (Convert.ToInt32(TotalLines) > newList[0].APP_MaximumLines)
                {
                    //Show err msg
                    if (hdnCurrentRole.Value == "Carrier")
                    {
                        ShowErrorMessage(errMsg, "BK_MS_18_EXT");
                        return;
                    }
                    else
                    {
                        if (!(hdnCurrentMode.Value == "Edit" && GetQueryStringValue("BP") != null && GetQueryStringValue("BP").ToString() == "Pro")) {  // If coming from Provisional page , ignore fixed slot capacity -- Phase 12 R2 Point 2
                            AddWarningMessages(errMsg, "BK_MS_18_INT");
                            isWarning = true;
                        }
                    }
                }
            }
        }
        //----Sprint 3a End--------------------//



        //Check Maximum Capacity for this day  - if not the confirm fixed slot case

        /* COMMENTED BACK AS PER REQUIREMENT OF PHASE-11-C POINT-1(17)*/
        if (strSuggestedWindowSlotID == "-1" || strSuggestedWindowSlotID == string.Empty)
        {
        if (hdnCurrentRole.Value == "Vendor" || hdnCurrentRole.Value == "Carrier") {
            errMsg = WebCommon.getGlobalResourceValue("BK_MS_18_INT_1"); //("BK_MS_18_EXT_1"); // "There is no capacity for any further deliveries on the date selected. Please select a different date or, if the delivery is urgent, please contact the booking office.";
        }
        else {
            errMsg = WebCommon.getGlobalResourceValue("BK_MS_18_INT_1"); //"There is no capacity for any further deliveries on the date selected. Click CONTINUE to make booking on this date.";
        }

        if (TotalPallets != "0" && hdnRemainingPallets.Value != string.Empty && hdnRemainingPallets.Value != "-") {
            if (Convert.ToInt32(TotalPallets) > (Convert.ToInt32(hdnRemainingPallets.Value) + BookedPallets)) {
                //Show err msg
                if (hdnCurrentRole.Value == "Vendor" || hdnCurrentRole.Value == "Carrier") {
                    //if (hdnCurrentMode.Value == "Add") {
                    //    ShowErrorMessage(errMsg, "BK_MS_18_EXT");
                    //    return;
                    //}
                    AddWarningMessages(errMsg, "BK_MS_18_INT_1");
                    isWarning = true;
                }
                else {
                    AddWarningMessages(errMsg, "BK_MS_18_INT_1");
                    isWarning = true;
                }
            }
        }

        if (TotalLines != "0" && hdnRemainingLines.Value != string.Empty && hdnRemainingLines.Value != "-") {
            if (Convert.ToInt32(TotalLines) > (Convert.ToInt32(hdnRemainingLines.Value) + BookedLines)) {
                //Show err msg
                if (hdnCurrentRole.Value == "Vendor" || hdnCurrentRole.Value == "Carrier") {
                    //if (hdnCurrentMode.Value == "Add") {
                    //    ShowErrorMessage(errMsg, "BK_MS_18_EXT");
                    //    return;
                    //}
                    AddWarningMessages(errMsg, "BK_MS_18_INT_1");
                    isWarning = true;
                }
                else {
                    AddWarningMessages(errMsg, "BK_MS_18_INT_1");
                    isWarning = true;
                }
            }
        }
        }

        if (hdnCurrentRole.Value == "Carrier")
        {
            btnWindowAlternateSlotVendor.Visible = false;

            // Here checking Alternate Slot button allow for vendor or not.
            MAS_SiteBE singleSiteSetting = GetSingleSiteSetting(Convert.ToInt32(ddlSite.innerControlddlSite.SelectedValue));
            if (singleSiteSetting != null)
            {
                if (singleSiteSetting.AlternateSlotOnOff.Trim().ToUpper() == "Y")
                    btnWindowAlternateSlotVendor.Visible = true; 
                else
                    btnWindowAlternateSlotVendor.Visible = false;
            }
            else
                btnWindowAlternateSlotVendor.Visible = true;
        }
        else
        {
            btnEnterAltTime.Visible = true;
            btnWindowAlternateSlotVendor.Visible = false;
        }

        if (isWarning == false)
        {
            BindWindowSlot();

            if (isDoorCheckPopup == false)
            {
                mvBookingEdit.ActiveViewIndex = 2;
            }
            else
            {
                mvBookingEdit.ActiveViewIndex = 1;
            }
        }
        else
            ShowWarningMessages();

        //Hide 'Non time Booking' and 'Alternate Slot' button in case of Confirm a fixed slot
        //if (hdnWindowSlotMode.Value == "true" && hdnCurrentRole.Value == "Carrier")
        //{
        //    btnMakeNonTimeBooking.Visible = false;
        //}
    }
    // Sprint 1 - Point 18 - End - Carrier lines to be editable   

    private int GetTimeSlotLength()
    {
        return GetTimeSlotLength(-1);
    }

    private int GetTimeSlotLength(int VehicleTypeID)
    {

        //--------start Calculate Unload Time-------------------//
        decimal TotalCalculatedTime = 0;  //In Min
        bool isTimeCalculatedForPallets = false;
        bool isTimeCalculatedForCartons = false;

        //Step1 >Check Carrier Processing Window 
        if (isTimeCalculatedForPallets == false || isTimeCalculatedForCartons == false)
        {

            MASSIT_CarrierProcessingWindowBE oMASSIT_CarrerProcessingWindowBE = new MASSIT_CarrierProcessingWindowBE();
            APPSIT_CarrierProcessingWindowBAL oMASSIT_CarrerProcessingWindowBAL = new APPSIT_CarrierProcessingWindowBAL();

            oMASSIT_CarrerProcessingWindowBE.Action = "GetConstraints";
            oMASSIT_CarrerProcessingWindowBE.CarrierID = Convert.ToInt32(ddlCarrier.SelectedItem.Value);
            oMASSIT_CarrerProcessingWindowBE.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
            MASSIT_CarrierProcessingWindowBE lstCarrier = oMASSIT_CarrerProcessingWindowBAL.GetConstraintsBAL(oMASSIT_CarrerProcessingWindowBE);


            if (lstCarrier != null)
            {  //Carrier Processing Window Not found
                if (lstCarrier.APP_PalletsUnloadedPerHour > 0 && lstCarrier.APP_CartonsUnloadedPerHour > 0)
                {
                    if (iPallets != 0 && isTimeCalculatedForPallets == false)
                    {
                        TotalCalculatedTime += Convert.ToDecimal(Convert.ToInt32(iPallets) * (60.0 / lstCarrier.APP_PalletsUnloadedPerHour));
                        isTimeCalculatedForPallets = true;
                    }
                    if (iCartons != 0 && isTimeCalculatedForCartons == false)
                    {
                        TotalCalculatedTime += Convert.ToDecimal(Convert.ToInt32(iCartons) * (60.0 / lstCarrier.APP_CartonsUnloadedPerHour));
                        isTimeCalculatedForCartons = true;
                    }
                }
            }
        }
        //---------------------------------// 

        //Step2 > Check Vehicle Type Setup
        if (isTimeCalculatedForPallets == false || isTimeCalculatedForCartons == false)
        {
            MASSIT_VehicleTypeBE oMASSIT_VehicleTypeBE = new MASSIT_VehicleTypeBE();
            APPSIT_VehicleTypeBAL oMASSIT_VehicleTypeBAL = new APPSIT_VehicleTypeBAL();

            oMASSIT_VehicleTypeBE.Action = "ShowById";
            oMASSIT_VehicleTypeBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
            oMASSIT_VehicleTypeBE.User.UserID = Convert.ToInt32(Session["UserID"]);
            if (VehicleTypeID <= 0)
                oMASSIT_VehicleTypeBE.VehicleTypeID = Convert.ToInt32(ddlVehicleType.SelectedItem.Value);
            else
                oMASSIT_VehicleTypeBE.VehicleTypeID = VehicleTypeID;

            MASSIT_VehicleTypeBE lstVehicleType = oMASSIT_VehicleTypeBAL.GetVehicleTypeDetailsByIdBAL(oMASSIT_VehicleTypeBE);
            if (lstVehicleType != null)
            {
                //Volume Capacity 
                if (lstVehicleType.APP_PalletsUnloadedPerHour > 0 && iPallets != 0 && isTimeCalculatedForPallets == false) {
                    TotalCalculatedTime += Convert.ToDecimal(Convert.ToInt32(iPallets) * (60.0 / lstVehicleType.APP_PalletsUnloadedPerHour));
                    isTimeCalculatedForPallets = true;
                }
                if (lstVehicleType.APP_CartonsUnloadedPerHour > 0 && iCartons != 0 && isTimeCalculatedForCartons == false) {
                    TotalCalculatedTime += Convert.ToDecimal(Convert.ToInt32(iCartons) * (60.0 / lstVehicleType.APP_CartonsUnloadedPerHour));
                    isTimeCalculatedForCartons = true;
                }

                if (isTimeCalculatedForPallets == false && isTimeCalculatedForCartons == false) { //Time Based 
                    if (lstVehicleType.TotalUnloadTimeInMinute > 0) {
                        TotalCalculatedTime += Convert.ToDecimal(Convert.ToDecimal(lstVehicleType.TotalUnloadTimeInMinute));
                        isTimeCalculatedForPallets = true;
                        isTimeCalculatedForCartons = true;
                    }
                }

                //---------------Stage 14 Point 15-------------//
                if (lstVehicleType.TotalUnloadTimeInMinute <= 0 && TotalCalculatedTime < lstVehicleType.MinimumTimePerDeliveryInMinute) {
                    TotalCalculatedTime = lstVehicleType.MinimumTimePerDeliveryInMinute;
                }
                //-------------------------------------------//
            }
        }
        //---------------------------------// 


        //Step3 >Check Site Miscellaneous Settings  - skip this step
        //if (isTimeCalculatedForPallets == false || isTimeCalculatedForCartons == false) {

        //    MAS_SiteBE oMAS_SiteBE = new MAS_SiteBE();
        //    MAS_SiteBAL oMAS_SiteBAL = new MAS_SiteBAL();

        //    oMAS_SiteBE.Action = "ShowAll";
        //    oMAS_SiteBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
        //    oMAS_SiteBE.User.UserID = Convert.ToInt32(Session["UserID"]);
        //    oMAS_SiteBE.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);

        //    List<MAS_SiteBE> lstSiteMisSettings = new List<MAS_SiteBE>();
        //    lstSiteMisSettings = oMAS_SiteBAL.GetSiteMisSettingBAL(oMAS_SiteBE);

        //    if (lstSiteMisSettings != null) {
        //        if (iPallets != 0 && isTimeCalculatedForPallets == false) {
        //            TotalCalculatedTime += Convert.ToDecimal(Convert.ToInt32(iPallets) * (60.0 / lstSiteMisSettings[0].AveragePalletUnloadedPerManHour));      // Convert.ToDecimal((Convert.ToInt32(iPallets) / lstSiteMisSettings[0].AveragePalletUnloadedPerManHour) * 60);
        //            isTimeCalculatedForPallets = true;
        //        }
        //        if (iCartons != 0 && isTimeCalculatedForCartons == false) {
        //            TotalCalculatedTime += Convert.ToDecimal(Convert.ToInt32(iCartons) * (60.0 / lstSiteMisSettings[0].AverageCartonUnloadedPerManHour));      //Convert.ToDecimal((Convert.ToInt32(iCartons) / lstSiteMisSettings[0].AverageCartonUnloadedPerManHour) * 60);
        //            isTimeCalculatedForCartons = true;
        //        }
        //    }
        //}
        //---------------------------------//   

        //Calculate Slot Length
        int intResult = (int)TotalCalculatedTime;
        int intr = (int)TotalCalculatedTime;

        //Not Required in case of Window booking -- Abhinav
        //if (intResult > 0)
        //    intr++;

        if (TotalCalculatedTime > 0 && TotalCalculatedTime < 1)
            intr = 1;

        if (TotalCalculatedTime == 0)
            intr++;

        return intr;
    }

    private string GetDoorConstraints()
    {
        string OpenDoorIds = string.Empty;

        if (txtSchedulingdate.innerControltxtDate.Value != string.Empty && ddlSite.innerControlddlSite.SelectedItem.Value != string.Empty)
        {

            bool IsSpecificWeekDefined = false;

            DateTime dtSelectedDate = LogicalSchedulDateTime;
            string WeekDay = dtSelectedDate.ToString("dddd");
            DateTime dtMon = Utilities.Common.GetFirstDayOfWeek(dtSelectedDate);

            MASSIT_WeekSetupBE oMASSIT_WeekSetupBE = new MASSIT_WeekSetupBE();
            MASSIT_WeekSetupBAL oMASSIT_WeekSetupBAL = new MASSIT_WeekSetupBAL();

            oMASSIT_WeekSetupBE.Action = "GetSpecificWeek";
            oMASSIT_WeekSetupBE.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
            oMASSIT_WeekSetupBE.EndWeekday = WeekDay;
            oMASSIT_WeekSetupBE.ScheduleDate = dtSelectedDate;

            List<MASSIT_WeekSetupBE> lstWeekSetup = oMASSIT_WeekSetupBAL.GetWeekSetupDetailsBAL(oMASSIT_WeekSetupBE);

            if (lstWeekSetup != null && lstWeekSetup.Count > 0 && lstWeekSetup[0].StartTime != null)
            {
                IsSpecificWeekDefined = true;
            }


            //------Check for Specific Entry First--------//
            APPSIT_DoorOpenTimeBAL oMASSIT_DoorOpenTimeBAL = new APPSIT_DoorOpenTimeBAL();
            MASSIT_DoorOpenTimeBE oMASSIT_DoorOpenTimeBE = new MASSIT_DoorOpenTimeBE();

            APPSIT_DoorOpenTimeSpecificBAL oAPPSIT_DoorOpenTimeSpecificBAL = new APPSIT_DoorOpenTimeSpecificBAL();
            MASSIT_DoorOpenTimeSpecificBE oMASSIT_DoorOpenTimeSpecificBE = new MASSIT_DoorOpenTimeSpecificBE();
            oMASSIT_DoorOpenTimeSpecificBE.Action = "GetDoorConstraintsSpecific";
            oMASSIT_DoorOpenTimeSpecificBE.DoorOpenTime = new MASSIT_DoorOpenTimeBE();
            oMASSIT_DoorOpenTimeSpecificBE.DoorOpenTime.DoorNo = new MASSIT_DoorNoSetupBE();
            oMASSIT_DoorOpenTimeSpecificBE.DoorOpenTime.DoorType = new BusinessEntities.ModuleBE.Appointment.CountrySetting.MASCNT_DoorTypeBE();
            oMASSIT_DoorOpenTimeSpecificBE.DoorOpenTime.DoorNo.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
            oMASSIT_DoorOpenTimeSpecificBE.DoorOpenTime.Weekday = WeekDay;
            oMASSIT_DoorOpenTimeSpecificBE.WeekStartDate = dtMon;
            List<MASSIT_DoorOpenTimeSpecificBE> lstDoorConstraintsSpecific = oAPPSIT_DoorOpenTimeSpecificBAL.GetDoorConstraintsSpecificBAL(oMASSIT_DoorOpenTimeSpecificBE);
            if (IsSpecificWeekDefined == true) {
                if (lstDoorConstraintsSpecific.Count > 0) { //Get Specific settings 
                    for (int iCount = 0; iCount < lstDoorConstraintsSpecific.Count; iCount++) {
                        OpenDoorIds += lstDoorConstraintsSpecific[iCount].DoorOpenTime.SlotTimeID + "@" + lstDoorConstraintsSpecific[iCount].DoorOpenTime.SiteDoorNumberID + ",";
                    }
                }
                else { //Get Generic settings 
                    oMASSIT_DoorOpenTimeBE.Action = "GetDoorConstraints";
                    oMASSIT_DoorOpenTimeBE.DoorNo = new MASSIT_DoorNoSetupBE();
                    oMASSIT_DoorOpenTimeBE.DoorNo.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
                    oMASSIT_DoorOpenTimeBE.Weekday = WeekDay;
                    List<MASSIT_DoorOpenTimeBE> lstDoorConstraints = oMASSIT_DoorOpenTimeBAL.GetDoorConstraintsBAL(oMASSIT_DoorOpenTimeBE);
                    for (int iCount = 0; iCount < lstDoorConstraints.Count; iCount++) {
                        OpenDoorIds += lstDoorConstraints[iCount].SlotTimeID + "@" + lstDoorConstraints[iCount].SiteDoorNumberID + ",";

                    }
                }
            }
            //if (lstDoorConstraintsSpecific.Count > 0 && IsSpecificWeekDefined == true) { //Get Specific settings 
            //    for (int iCount = 0; iCount < lstDoorConstraintsSpecific.Count; iCount++) {
            //        OpenDoorIds += lstDoorConstraintsSpecific[iCount].DoorOpenTime.SlotTimeID + "@" + lstDoorConstraintsSpecific[iCount].DoorOpenTime.SiteDoorNumberID + ",";
            //    }
            //}
            else
            {
                if (IsSpecificWeekDefined == false)
                {
                    //Get Generic settings 
                    oMASSIT_DoorOpenTimeBE.Action = "GetDoorConstraints";
                    oMASSIT_DoorOpenTimeBE.DoorNo = new MASSIT_DoorNoSetupBE();
                    oMASSIT_DoorOpenTimeBE.DoorNo.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
                    oMASSIT_DoorOpenTimeBE.Weekday = WeekDay;
                    List<MASSIT_DoorOpenTimeBE> lstDoorConstraints = oMASSIT_DoorOpenTimeBAL.GetDoorConstraintsBAL(oMASSIT_DoorOpenTimeBE);
                    for (int iCount = 0; iCount < lstDoorConstraints.Count; iCount++)
                    {
                        OpenDoorIds += lstDoorConstraints[iCount].SlotTimeID + "@" + lstDoorConstraints[iCount].SiteDoorNumberID + ",";

                    }
                }
            }
        }

        return OpenDoorIds;
    }

    private bool GetWeekConstraints() {
        bool IsWeekConstraints = true;

        if (txtSchedulingdate.innerControltxtDate.Value != string.Empty && ddlSite.innerControlddlSite.SelectedItem.Value != string.Empty) {
            DateTime dtSelectedDate = LogicalSchedulDateTime;
            string WeekDay = dtSelectedDate.ToString("dddd");
            DateTime dtMon = Utilities.Common.GetFirstDayOfWeek(dtSelectedDate);

            MASSIT_WeekSetupBE oMASSIT_WeekSetupBE = new MASSIT_WeekSetupBE();
            MASSIT_WeekSetupBAL oMASSIT_WeekSetupBAL = new MASSIT_WeekSetupBAL();

            oMASSIT_WeekSetupBE.Action = "GetSpecificWeek";
            oMASSIT_WeekSetupBE.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
            oMASSIT_WeekSetupBE.EndWeekday = WeekDay;
            oMASSIT_WeekSetupBE.ScheduleDate = dtSelectedDate;

            List<MASSIT_WeekSetupBE> lstWeekSetup = oMASSIT_WeekSetupBAL.GetWeekSetupDetailsBAL(oMASSIT_WeekSetupBE);

            if (lstWeekSetup == null || lstWeekSetup.Count <= 0 || lstWeekSetup[0].StartTime == null) {

                oMASSIT_WeekSetupBE.Action = "ShowAll";
                lstWeekSetup = oMASSIT_WeekSetupBAL.GetWeekSetupDetailsBAL(oMASSIT_WeekSetupBE);
            }

            if (lstWeekSetup == null || lstWeekSetup.Count <= 0 || lstWeekSetup[0].StartTime == null) {
                IsWeekConstraints = false;
            }
        }
        return IsWeekConstraints;
    }
    //    //This method will check Total available time for window
    //private bool CheckTotalAvailableTimeConstraint(APPBOK_BookingBE objMASSIT_TimeWindowBE)
    //{
    //    bool isTotalTimeConstraint = false;
    //    if (!string.IsNullOrEmpty(objMASSIT_TimeWindowBE.TimeWindow.TotalHrsMinsAvailable))
    //    {
    //        string[] ArrayTotalTime = objMASSIT_TimeWindowBE.TimeWindow.TotalHrsMinsAvailable.Split(':');
    //        int TotalTimeInMinute = (Convert.ToInt32(ArrayTotalTime[0]) * 60) + Convert.ToInt32(ArrayTotalTime[1]);
    //        int? TotalPallets = objMASSIT_TimeWindowBE.TimeWindow.MaximumPallets;
    //        int? TotalCartons = objMASSIT_TimeWindowBE.TimeWindow.MaximumCartons;
    //        if (TotalTimeInMinute >= GetRequiredTimeSlotLength(TotalPallets, TotalCartons))
    //            isTotalTimeConstraint = false;
    //        else
    //            isTotalTimeConstraint = true;
    //    }
    //    return isTotalTimeConstraint;
    //}

    private int GetRequiredTimeSlotLengthForOthers(int? TotalPallets, int? TotalCartons, string SupplierType, int ID, int VehicleID)
    {
        //--------start Calculate Unload Time-------------------//
        decimal TotalCalculatedTime = 0;  //In Min
        bool isTimeCalculatedForPallets = false;
        bool isTimeCalculatedForCartons = false;

        TotalPallets = iTotalPallets > 0  ? iTotalPallets : 0;
        TotalCartons = iTotalCartons >  0  ? iTotalCartons : 0 ;

        if (SupplierType == "V")
        {
            //Step1 >Check Vendor Processing Window 
            if (isTimeCalculatedForPallets == false || isTimeCalculatedForCartons == false)
            {

                MASSIT_VendorProcessingWindowBE oMASSIT_VendorProcessingWindowBE = new MASSIT_VendorProcessingWindowBE();
                APPSIT_VendorProcessingWindowBAL oMASSIT_VendorProcessingWindowBAL = new APPSIT_VendorProcessingWindowBAL();

                oMASSIT_VendorProcessingWindowBE.Action = "ShowAllConsolidate";
                oMASSIT_VendorProcessingWindowBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
                oMASSIT_VendorProcessingWindowBE.User.UserID = Convert.ToInt32(Session["UserID"]);
                oMASSIT_VendorProcessingWindowBE.VendorID = ID;
                oMASSIT_VendorProcessingWindowBE.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);


                MASSIT_VendorProcessingWindowBE lstVendor = new MASSIT_VendorProcessingWindowBE();
                lstVendor = oMASSIT_VendorProcessingWindowBAL.GetVendorDetailsByIdBAL(oMASSIT_VendorProcessingWindowBE);

                if (lstVendor != null)
                {  //Vendor Processing Window Not found

                    if (lstVendor.APP_PalletsUnloadedPerHour > 0 && TotalPallets != 0 && isTimeCalculatedForPallets == false)
                    {
                        TotalCalculatedTime += Convert.ToDecimal(Convert.ToInt32(TotalPallets) * (60.0 / lstVendor.APP_PalletsUnloadedPerHour));
                        isTimeCalculatedForPallets = true;
                    }
                    if (lstVendor.APP_CartonsUnloadedPerHour > 0 && TotalCartons != 0 && isTimeCalculatedForCartons == false)
                    {
                        TotalCalculatedTime += Convert.ToDecimal(Convert.ToInt32(TotalCartons) * (60.0 / lstVendor.APP_CartonsUnloadedPerHour));
                        isTimeCalculatedForCartons = true;
                    }
                }
            }
            //---------------------------------// 
        }

        if (SupplierType == "C")
        {
            //Step1 >Check Carrier Processing Window 
            if (isTimeCalculatedForPallets == false || isTimeCalculatedForCartons == false)
            {

                MASSIT_CarrierProcessingWindowBE oMASSIT_CarrerProcessingWindowBE = new MASSIT_CarrierProcessingWindowBE();
                APPSIT_CarrierProcessingWindowBAL oMASSIT_CarrerProcessingWindowBAL = new APPSIT_CarrierProcessingWindowBAL();

                oMASSIT_CarrerProcessingWindowBE.Action = "GetConstraints";
                oMASSIT_CarrerProcessingWindowBE.CarrierID = ID;
                oMASSIT_CarrerProcessingWindowBE.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
                MASSIT_CarrierProcessingWindowBE lstCarrier = oMASSIT_CarrerProcessingWindowBAL.GetConstraintsBAL(oMASSIT_CarrerProcessingWindowBE);


                if (lstCarrier != null)
                {  //Carrier Processing Window Not found

                    if (lstCarrier.APP_PalletsUnloadedPerHour > 0 && TotalPallets != 0 && isTimeCalculatedForPallets == false)
                    {
                        TotalCalculatedTime += Convert.ToDecimal(Convert.ToInt32(TotalPallets) * (60.0 / lstCarrier.APP_PalletsUnloadedPerHour));
                        isTimeCalculatedForPallets = true;
                    }
                    if (lstCarrier.APP_CartonsUnloadedPerHour > 0 && TotalCartons != 0 && isTimeCalculatedForCartons == false)
                    {
                        TotalCalculatedTime += Convert.ToDecimal(Convert.ToInt32(TotalCartons) * (60.0 / lstCarrier.APP_CartonsUnloadedPerHour));
                        isTimeCalculatedForCartons = true;
                    }
                }
            }
            //---------------------------------// 
        }


        //Step2 > Check Vehicle Type Setup  
        if (isTimeCalculatedForPallets == false || isTimeCalculatedForCartons == false)
        {
            MASSIT_VehicleTypeBE oMASSIT_VehicleTypeBE = new MASSIT_VehicleTypeBE();
            APPSIT_VehicleTypeBAL oMASSIT_VehicleTypeBAL = new APPSIT_VehicleTypeBAL();

            oMASSIT_VehicleTypeBE.Action = "ShowById";
            oMASSIT_VehicleTypeBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
            oMASSIT_VehicleTypeBE.User.UserID = Convert.ToInt32(Session["UserID"]);
            oMASSIT_VehicleTypeBE.VehicleTypeID = VehicleID;
            MASSIT_VehicleTypeBE lstVehicleType = oMASSIT_VehicleTypeBAL.GetVehicleTypeDetailsByIdBAL(oMASSIT_VehicleTypeBE);
            if (lstVehicleType != null)
            {
                //Volume Capacity 
                if (lstVehicleType.APP_PalletsUnloadedPerHour > 0 && iPallets != 0 && isTimeCalculatedForPallets == false) {
                    TotalCalculatedTime += Convert.ToDecimal(Convert.ToInt32(iPallets) * (60.0 / lstVehicleType.APP_PalletsUnloadedPerHour));
                    isTimeCalculatedForPallets = true;
                }
                if (lstVehicleType.APP_CartonsUnloadedPerHour > 0 && iCartons != 0 && isTimeCalculatedForCartons == false) {
                    TotalCalculatedTime += Convert.ToDecimal(Convert.ToInt32(iCartons) * (60.0 / lstVehicleType.APP_CartonsUnloadedPerHour));
                    isTimeCalculatedForCartons = true;
                }

                if (isTimeCalculatedForPallets == false && isTimeCalculatedForCartons == false) { //Time Based 
                    if (lstVehicleType.TotalUnloadTimeInMinute > 0) {
                        TotalCalculatedTime += Convert.ToDecimal(Convert.ToDecimal(lstVehicleType.TotalUnloadTimeInMinute));
                        isTimeCalculatedForPallets = true;
                        isTimeCalculatedForCartons = true;
                    }
                }

                //---------------Stage 14 Point 15-------------//
                if (lstVehicleType.TotalUnloadTimeInMinute <= 0 && TotalCalculatedTime < lstVehicleType.MinimumTimePerDeliveryInMinute) {
                    TotalCalculatedTime = lstVehicleType.MinimumTimePerDeliveryInMinute;
                }
                //-------------------------------------------//
            }
        }
        //---------------------------------// 


        ////Step3 >Check Site Miscellaneous Settings
        //if (isTimeCalculatedForPallets == false || isTimeCalculatedForCartons == false)
        //{

        //    MAS_SiteBE oMAS_SiteBE = new MAS_SiteBE();
        //    MAS_SiteBAL oMAS_SiteBAL = new MAS_SiteBAL();

        //    if (hdnCurrentRole.Value == "Vendor")
        //        oMAS_SiteBE.Action = "ShowAllForVendor";
        //    else
        //        oMAS_SiteBE.Action = "ShowAll";

        //    oMAS_SiteBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
        //    oMAS_SiteBE.User.UserID = Convert.ToInt32(Session["UserID"]);
        //    oMAS_SiteBE.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);

        //    List<MAS_SiteBE> lstSiteMisSettings = new List<MAS_SiteBE>();
        //    lstSiteMisSettings = oMAS_SiteBAL.GetSiteMisSettingBAL(oMAS_SiteBE);

        //    if (lstSiteMisSettings != null && lstSiteMisSettings.Count > 0)
        //    {
        //        if (lstSiteMisSettings[0].AveragePalletUnloadedPerManHour > 0 && TotalPallets != 0 && isTimeCalculatedForPallets == false)
        //        {
        //            TotalCalculatedTime += Convert.ToDecimal(Convert.ToInt32(TotalPallets) * (60.0 / lstSiteMisSettings[0].AveragePalletUnloadedPerManHour));      // Convert.ToDecimal((Convert.ToInt32(TotalPallets) / lstSiteMisSettings[0].AveragePalletUnloadedPerManHour) * 60);
        //            isTimeCalculatedForPallets = true;
        //        }
        //        if (lstSiteMisSettings[0].AverageCartonUnloadedPerManHour > 0 && TotalCartons != 0 && isTimeCalculatedForCartons == false)
        //        {
        //            TotalCalculatedTime += Convert.ToDecimal(Convert.ToInt32(TotalCartons) * (60.0 / lstSiteMisSettings[0].AverageCartonUnloadedPerManHour));      //Convert.ToDecimal((Convert.ToInt32(TotalCartons) / lstSiteMisSettings[0].AverageCartonUnloadedPerManHour) * 60);
        //            isTimeCalculatedForCartons = true;
        //        }
        //    }
        //}
        //---------------------------------//   

        //Calculate Slot Length
        int intResult = (int)TotalCalculatedTime;
        int intr = (int)TotalCalculatedTime;

        //Not Required in case of Window booking -- Abhinav
        //if (intResult > 0)
        //    intr++;

        if (TotalCalculatedTime > 0 && TotalCalculatedTime < 1)
            intr = 1;

        if (TotalCalculatedTime == 0)
            intr++;


        return intr;
    }

    //This method will check Total available time for window
    private bool CheckTotalAvailableTimeConstraint(MASSIT_TimeWindowBE objMASSIT_TimeWindowBE)
    {
        bool isTotalTimeConstraint = false;
        if (!string.IsNullOrEmpty(objMASSIT_TimeWindowBE.TotalHrsMinsAvailable))
        {
            string[] ArrayTotalTime = objMASSIT_TimeWindowBE.TotalHrsMinsAvailable.Split(':');
            int TotalTimeInMinute = (Convert.ToInt32(ArrayTotalTime[0]) * 60) + Convert.ToInt32(ArrayTotalTime[1]);

            int? TotalPallets = iTotalPallets;
            int? TotalCartons = iTotalCartons;
            int GetVolumeProcessTime = GetRequiredTimeSlotLength(TotalPallets, TotalCartons);


            APPBOK_BookingBE oAPPBOK_BookingBE = new APPBOK_BookingBE();
            APPBOK_BookingBAL oAPPBOK_BookingBAL = new APPBOK_BookingBAL();

            oAPPBOK_BookingBE.Action = "GetVolumeProcessed";
            oAPPBOK_BookingBE.ScheduleDate = LogicalSchedulDateTime;
            oAPPBOK_BookingBE.SiteId = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
            oAPPBOK_BookingBE.TimeWindow = new MASSIT_TimeWindowBE();
            oAPPBOK_BookingBE.TimeWindow.TimeWindowID = objMASSIT_TimeWindowBE.TimeWindowID;

            List<APPBOK_BookingBE> lstBookingDeatils = new List<APPBOK_BookingBE>();
            lstBookingDeatils = oAPPBOK_BookingBAL.GetGetVolumeProcessedBAL(oAPPBOK_BookingBE);

            if (lstBookingDeatils != null && lstBookingDeatils.Count > 0)
            {
                foreach (APPBOK_BookingBE Booking in lstBookingDeatils)
                {
                    GetVolumeProcessTime += GetRequiredTimeSlotLengthForOthers(Booking.NumberOfPallet, Booking.NumberOfCartons, Booking.SupplierType, Booking.VendorCarrierID, Booking.VehicleType.VehicleTypeID);
                }
            }
            if (TotalTimeInMinute >= GetVolumeProcessTime)
                isTotalTimeConstraint = false;
            else
                isTotalTimeConstraint = true;
        }
        return isTotalTimeConstraint;
    }

    //This method will check the Max time constraint for Window
    private bool CheckMaxTimeConstraint(MASSIT_TimeWindowBE objMASSIT_TimeWindowBE)
    {
        bool isMaxTimeConstraint = false;
        if (!string.IsNullOrEmpty(objMASSIT_TimeWindowBE.MaximumTime))
        {
            string[] ArrayMaxTime = objMASSIT_TimeWindowBE.MaximumTime.Split(':');
            int MaxTimeInMinute = 0;
            if (ArrayMaxTime.Length == 1)
               MaxTimeInMinute = (Convert.ToInt32(ArrayMaxTime[0]) * 60);
            else
              MaxTimeInMinute = (Convert.ToInt32(ArrayMaxTime[0]) * 60) + Convert.ToInt32(ArrayMaxTime[1]);
            int? TotalPallets = iTotalPallets;
            int? TotalCartons = iTotalCartons;
            if (MaxTimeInMinute >= GetRequiredTimeSlotLength(TotalPallets, TotalCartons))
                isMaxTimeConstraint = false;
            else
                isMaxTimeConstraint = true;
        }
        return isMaxTimeConstraint;
    }

    ////This method will check the Max time constraint for Window
    //private bool CheckMaxTimeConstraint(APPBOK_BookingBE objMASSIT_TimeWindowBE)
    //{
    //    bool isMaxTimeConstraint = false;
    //    if (!string.IsNullOrEmpty(objMASSIT_TimeWindowBE.TimeWindow.MaximumTime))
    //    {
    //        string[] ArrayMaxTime = objMASSIT_TimeWindowBE.TimeWindow.MaximumTime.Split(':');
    //        int MaxTimeInMinute = (Convert.ToInt32(ArrayMaxTime[0]) * 60) + Convert.ToInt32(ArrayMaxTime[1]);
    //        int? TotalPallets = iTotalPallets.ToString().Trim() != string.Empty ? Convert.ToInt32(iTotalPallets.ToString().Trim()) : (int?)null;
    //        int? TotalCartons = iTotalCartons.ToString().Trim() != string.Empty ? Convert.ToInt32(iTotalCartons.ToString().Trim()) : (int?)null;
    //        if (MaxTimeInMinute >= GetRequiredTimeSlotLength(TotalPallets, TotalCartons))
    //            isMaxTimeConstraint = false;
    //        else
    //            isMaxTimeConstraint = true;
    //    }
    //    return isMaxTimeConstraint;
    //}

    protected void BindWindowSlot()
    {        

        DateTime dtSelectedDate = LogicalSchedulDateTime;
        string WeekDay = dtSelectedDate.ToString("dddd");

        DataTable myDataTable = null;

        bool bSKUMatrixDoorFound = false;
        string totalPallets = iTotalPallets.ToString();
        string totalCartons = iTotalCartons.ToString();
        string totalLines = iTotalLines.ToString();
        //get the vendor ids for all the POs
        if (ViewState["myDataTable"] != null)
        {
            myDataTable = (DataTable)ViewState["myDataTable"];
            var vendorCol = myDataTable.AsEnumerable().Select(x => x.Field<string>("VendorId"));
            strVendorIDs = vendorCol.ToArray();
        }

        #region SKUCheck
  
        int iSiteDoorNumberID = 0;
        int iSKUDoorTypeID = 0;

        if (ViewState["myDataTable"] != null)
        {
            myDataTable = (DataTable)ViewState["myDataTable"];

            var column2Values = myDataTable.AsEnumerable().Select(x => x.Field<string>("PurchaseNumber"));
            string PurchaseNumbers = String.Join(",", column2Values.ToArray());
            if (!string.IsNullOrEmpty(PurchaseNumbers))
            {

                APPSIT_DoorOpenTimeBAL oMASSIT_DoorOpenTimeBAL = new APPSIT_DoorOpenTimeBAL();
                MASSIT_DoorOpenTimeBE oMASSIT_DoorOpenTimeBE = new MASSIT_DoorOpenTimeBE();

                oMASSIT_DoorOpenTimeBE.Action = "GetSKUMatrixDoorDetails";
                oMASSIT_DoorOpenTimeBE.DoorNo = new MASSIT_DoorNoSetupBE();
                oMASSIT_DoorOpenTimeBE.DoorNo.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
                oMASSIT_DoorOpenTimeBE.PurchaseNumbers = PurchaseNumbers;


                List<MASSIT_DoorOpenTimeBE> lstSKUMatrixDoorDetails = oMASSIT_DoorOpenTimeBAL.GetDoorNoSetupDetailsBAL(oMASSIT_DoorOpenTimeBE);

                if (lstSKUMatrixDoorDetails != null && lstSKUMatrixDoorDetails.Count > 0)
                {
                    bSKUMatrixDoorFound = true;
                    iSiteDoorNumberID = lstSKUMatrixDoorDetails[0].SiteDoorNumberID;
                    iSKUDoorTypeID = lstSKUMatrixDoorDetails[0].DoorType.DoorTypeID;
                    if (lstSKUMatrixDoorDetails.Count > 1)
                    {  //there is a conflict i.e the PO's contain SKU with different doors 
                        isProvisional = true;
                        ProvisionalReason = "WINDOW CAPACITY";
                    }
                }
            }
        }
        #endregion

        #region Find reserved window
        MASSIT_TimeWindowBE oMASSIT_TimeWindowBE = new MASSIT_TimeWindowBE();
        MASSIT_TimeWindowBAL oMASSIT_TimeWindowBAL = new MASSIT_TimeWindowBAL();

        if (hdnWindowSlotMode.Value != "true") {
            //Get all reserved window           
            oMASSIT_TimeWindowBE.Action = "ShowAllReserved";
            oMASSIT_TimeWindowBE.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
            oMASSIT_TimeWindowBE.ScheduleDate = LogicalSchedulDateTime;           
            oMASSIT_TimeWindowBE.IncludedCarrierIDs = Convert.ToString(ddlCarrier.SelectedItem.Value);

            if (hdnCurrentMode.Value == "Edit" && hdnSuggestedWindowSlotID.Value != "" && hdnSuggestedWindowSlotID.Value != "-1") {
                oMASSIT_TimeWindowBE.TimeWindowID = Convert.ToInt32(hdnSuggestedWindowSlotID.Value);
            }
            else {
                oMASSIT_TimeWindowBE.TimeWindowID = -1;
            }

            List<MASSIT_TimeWindowBE> lstFixedSlot = oMASSIT_TimeWindowBAL.GetReservedWindowBAL(oMASSIT_TimeWindowBE);

            if (txtSchedulingdate.innerControltxtDate.Value == GetQueryStringValue("Scheduledate").ToString() && hdnPreviousVehicleType.Value != ddlVehicleType.SelectedValue && isDoorTypeNotChanged == false && hdnCurrentMode.Value == "Edit")
            {               
                strSuggestedWindowSlotID = "";
            }
            else if (txtSchedulingdate.innerControltxtDate.Value != GetQueryStringValue("Scheduledate").ToString() && hdnCurrentMode.Value == "Edit")
            {
                strSuggestedWindowSlotID = "";
            }

            if (lstFixedSlot != null && lstFixedSlot.Count > 0) {       

                //Get Booked Volume
                MASSIT_TimeWindowBE objMASSIT_TimeWindowBE = new MASSIT_TimeWindowBE();

                objMASSIT_TimeWindowBE.Action = "GetBookedVolume";
                objMASSIT_TimeWindowBE.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
                objMASSIT_TimeWindowBE.ScheduleDate = LogicalSchedulDateTime;
                //Edit Case
                if (hdnCurrentMode.Value == "Edit") {
                    objMASSIT_TimeWindowBE.BookingRef = hdnBookingRef.Value;
                }

                List<MASSIT_TimeWindowBE> lstBookedVolume = oMASSIT_TimeWindowBAL.GetBookedVolumeBAL(objMASSIT_TimeWindowBE);

                bool IsReservedWindowFound = false;

                if ((lstBookedVolume != null && lstBookedVolume.Count > 0) || (hdnBookingRef.Value != "")) {

                    foreach (MASSIT_TimeWindowBE oTimeWindowBE in lstFixedSlot) {

                        List<MASSIT_TimeWindowBE> lstBookedVolumePerposed = null;

                        lstBookedVolumePerposed = lstBookedVolume.Where(vol => vol.TimeWindowID == oTimeWindowBE.TimeWindowID).ToList();

                        if (lstBookedVolumePerposed != null && lstBookedVolumePerposed.Count > 0 && hdnCurrentMode.Value == "Add") {

                            lstBookedVolumePerposed = lstBookedVolumePerposed.FindAll(delegate(MASSIT_TimeWindowBE vol) {
                                if (oTimeWindowBE.MaxDeliveries > 0) {
                                    if (vol.MaxDeliveries > 0) {
                                        //check maximim delivery
                                        if (oTimeWindowBE.MaxDeliveries >= vol.MaxDeliveries) {
                                            // We just found fixed slot. Capture it.
                                            return true;
                                        }
                                        else {
                                            // Not a fixed slot
                                            return false;
                                        }
                                    }
                                    else {
                                        return false;
                                    }
                                }
                                else {
                                    return true;
                                }

                            });

                            lstBookedVolumePerposed = lstBookedVolumePerposed.FindAll(delegate(MASSIT_TimeWindowBE vol) {
                                if (oTimeWindowBE.MaximumCartons > 0) {
                                    if (vol.MaximumCartons <= 0) {
                                        return false;
                                    }
                                    else {
                                        return true;
                                    }
                                }
                                else {
                                    return true;
                                }
                            });

                            lstBookedVolumePerposed = lstBookedVolumePerposed.FindAll(delegate(MASSIT_TimeWindowBE vol) {
                                return oTimeWindowBE.MaximumPallets > 0 ? vol.MaximumPallets > 0 : true;
                            });

                            lstBookedVolumePerposed = lstBookedVolumePerposed.FindAll(delegate(MASSIT_TimeWindowBE vol) {
                                return oTimeWindowBE.MaximumLines > 0 ? vol.MaximumLines > 0 : true;
                            });

                            if (lstBookedVolumePerposed != null && lstBookedVolumePerposed.Count > 0) {
                                IsReservedWindowFound = true;
                                if (hdnCurrentMode.Value == "Add") {

                                    ltBookingDate.Text = oTimeWindowBE.StartTime + " - " + oTimeWindowBE.EndTime + " on " + LogicalSchedulDateTime.Day.ToString() + "/" + LogicalSchedulDateTime.Month.ToString() + "/" + LogicalSchedulDateTime.Year.ToString();
                                    ltSuggestedSlotTime.Text = AtDoorName + " " + oTimeWindowBE.DoorNumber.ToString();
                                    hdnPreservSlot.Value = oTimeWindowBE.StartSlotTimeID + "|" + oTimeWindowBE.EndSlotTimeID + "|" + oTimeWindowBE.SiteDoorNumberID.ToString();
                                    hdnSuggestedWindowSlotID.Value = oTimeWindowBE.TimeWindowID.ToString();
                                    hdnSuggestedSiteDoorNumberID.Value = oTimeWindowBE.SiteDoorNumberID.ToString();
                                    hdnSuggestedSiteDoorNumber.Value = oTimeWindowBE.DoorNumber.ToString();
                                    hdnSuggestedWeekDay.Value = LogicalSchedulDateTime.ToString("ddd");
                                    strSuggestedWindowSlotID = oTimeWindowBE.TimeWindowID.ToString();
                                    strSuggestedSiteDoorNumberID = oTimeWindowBE.SiteDoorNumberID.ToString();
                                    strSuggestedSiteDoorNumber = oTimeWindowBE.DoorNumber.ToString();
                                   
                                }

                                lstBookedVolumePerposed = lstBookedVolumePerposed.FindAll(delegate(MASSIT_TimeWindowBE vol) {
                                    return oTimeWindowBE.MaximumCartons > 0 ? vol.MaximumCartons >= Convert.ToInt32(TotalCartons) : true;
                                });

                                if (lstBookedVolumePerposed.Count == 0) {
                                    isProvisional = true;
                                    ProvisionalReason = "RESERVED WINDOW CAPACITY";
                                    break;
                                }

                                //CHECK MAXIMUM Pallets
                                lstBookedVolumePerposed = lstBookedVolumePerposed.FindAll(delegate(MASSIT_TimeWindowBE vol) {
                                    return oTimeWindowBE.MaximumPallets > 0 ? vol.MaximumPallets >= Convert.ToInt32(TotalPallets) : true;
                                });

                                if (lstBookedVolumePerposed.Count == 0) {
                                    isProvisional = true;
                                    ProvisionalReason = "RESERVED WINDOW CAPACITY";
                                    break;
                                }

                                //CHECK MAXIMUM Lines
                                lstBookedVolumePerposed = lstBookedVolumePerposed.FindAll(delegate(MASSIT_TimeWindowBE vol) {
                                    // We just found fixed slot. Capture it. // Not a fixed slot
                                    return oTimeWindowBE.MaximumLines > 0 ? vol.MaximumLines >= Convert.ToInt32(TotalLines) : true;
                                });

                                if (lstBookedVolumePerposed.Count == 0) {
                                    isProvisional = true;
                                    ProvisionalReason = "RESERVED WINDOW CAPACITY";
                                    break;
                                }

                            }
                            if (IsReservedWindowFound)
                                break;
                        }
                        else {
                            if (hdnCurrentMode.Value == "Add") {

                                ltBookingDate.Text = oTimeWindowBE.StartTime + " - " + oTimeWindowBE.EndTime + " on " + LogicalSchedulDateTime.Day.ToString() + "/" + LogicalSchedulDateTime.Month.ToString() + "/" + LogicalSchedulDateTime.Year.ToString();
                                ltSuggestedSlotTime.Text = AtDoorName + " " + oTimeWindowBE.DoorNumber.ToString();
                                hdnPreservSlot.Value = oTimeWindowBE.StartSlotTimeID + "|" + oTimeWindowBE.EndSlotTimeID + "|" + oTimeWindowBE.SiteDoorNumberID.ToString();
                                hdnSuggestedWindowSlotID.Value = oTimeWindowBE.TimeWindowID.ToString();
                                hdnSuggestedSiteDoorNumberID.Value = oTimeWindowBE.SiteDoorNumberID.ToString();
                                hdnSuggestedSiteDoorNumber.Value = oTimeWindowBE.DoorNumber.ToString();
                                hdnSuggestedWeekDay.Value = LogicalSchedulDateTime.ToString("ddd");
                                strSuggestedWindowSlotID = oTimeWindowBE.TimeWindowID.ToString();
                                strSuggestedSiteDoorNumberID = oTimeWindowBE.SiteDoorNumberID.ToString();
                                strSuggestedSiteDoorNumber = oTimeWindowBE.DoorNumber.ToString();
                                break;
                            }

                            if (hdnCurrentMode.Value == "Edit") {
                                if (oTimeWindowBE.MaximumCartons > 0) {
                                    if (oTimeWindowBE.MaximumCartons < Convert.ToInt32(TotalCartons)) {
                                        isProvisional = true;
                                        ProvisionalReason = "RESERVED WINDOW CAPACITY";
                                    }
                                }

                                if (oTimeWindowBE.MaximumPallets > 0) {
                                    if (oTimeWindowBE.MaximumPallets < Convert.ToInt32(TotalPallets)) {
                                        isProvisional = true;
                                        ProvisionalReason = "RESERVED WINDOW CAPACITY";
                                    }
                                }


                                if (oTimeWindowBE.MaximumLines > 0) {
                                    if (oTimeWindowBE.MaximumLines < Convert.ToInt32(TotalLines)) {
                                        isProvisional = true;
                                        ProvisionalReason = "RESERVED WINDOW CAPACITY";
                                    }
                                }
                            }

                        }
                    }                    
                }
                else {
                    if (hdnCurrentMode.Value == "Add") {
                        MASSIT_TimeWindowBE oTimeWindowBE = lstFixedSlot[0];

                        ltBookingDate.Text = oTimeWindowBE.StartTime + " - " + oTimeWindowBE.EndTime + " on " + LogicalSchedulDateTime.Day.ToString() + "/" + LogicalSchedulDateTime.Month.ToString() + "/" + LogicalSchedulDateTime.Year.ToString();
                        ltSuggestedSlotTime.Text = AtDoorName + " " + oTimeWindowBE.DoorNumber.ToString();
                        hdnPreservSlot.Value = oTimeWindowBE.StartSlotTimeID + "|" + oTimeWindowBE.EndSlotTimeID + "|" + oTimeWindowBE.SiteDoorNumberID.ToString();
                        hdnSuggestedWindowSlotID.Value = oTimeWindowBE.TimeWindowID.ToString();
                        hdnSuggestedSiteDoorNumberID.Value = oTimeWindowBE.SiteDoorNumberID.ToString();
                        hdnSuggestedSiteDoorNumber.Value = oTimeWindowBE.DoorNumber.ToString();
                        hdnSuggestedWeekDay.Value = LogicalSchedulDateTime.ToString("ddd");
                        strSuggestedWindowSlotID = oTimeWindowBE.TimeWindowID.ToString();

                        strSuggestedSiteDoorNumberID = oTimeWindowBE.SiteDoorNumberID.ToString();
                        strSuggestedSiteDoorNumber = oTimeWindowBE.DoorNumber.ToString();


                        if (oTimeWindowBE.MaximumCartons > 0) {
                            if (oTimeWindowBE.MaximumCartons < Convert.ToInt32(TotalCartons)) {
                                isProvisional = true;
                                ProvisionalReason = "RESERVED WINDOW CAPACITY";
                            }
                        }

                        if (oTimeWindowBE.MaximumPallets > 0) {
                            if (oTimeWindowBE.MaximumPallets < Convert.ToInt32(TotalPallets)) {
                                isProvisional = true;
                                ProvisionalReason = "RESERVED WINDOW CAPACITY";
                            }
                        }


                        if (oTimeWindowBE.MaximumLines > 0) {
                            if (oTimeWindowBE.MaximumLines < Convert.ToInt32(TotalLines)) {
                                isProvisional = true;
                                ProvisionalReason = "RESERVED WINDOW CAPACITY";
                            }
                        }
                    }
                }
            }
  
        }


        if (hdnWindowSlotMode.Value == "true" && hdnCurrentMode.Value == "Add" && strSuggestedWindowSlotID != "" && strSuggestedWindowSlotID != "-1") {
            //Get all reserved window           
            oMASSIT_TimeWindowBE.Action = "ShowAllReserved";
            oMASSIT_TimeWindowBE.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
            oMASSIT_TimeWindowBE.ScheduleDate = LogicalSchedulDateTime;
            oMASSIT_TimeWindowBE.IncludedCarrierIDs = Convert.ToString(ddlCarrier.SelectedItem.Value);           
            oMASSIT_TimeWindowBE.TimeWindowID = Convert.ToInt32(strSuggestedWindowSlotID);


            List<MASSIT_TimeWindowBE> lstFixedSlot = oMASSIT_TimeWindowBAL.GetReservedWindowBAL(oMASSIT_TimeWindowBE);

            if (lstFixedSlot != null && lstFixedSlot.Count > 0) {

                List<MASSIT_TimeWindowBE> lstBookedVolumePerposed = lstFixedSlot.FindAll(delegate(MASSIT_TimeWindowBE vol) {
                    if (vol.MaximumCartons > 0) {
                        if (vol.MaximumCartons >= Convert.ToInt32(TotalCartons)) {
                            // We just found fixed slot. Capture it.
                            return true;
                        }
                        else {
                            // Not a fixed slot
                            return false;
                        }
                    }
                    else {
                        return true;
                    }
                });

                if (lstBookedVolumePerposed.Count == 0) {
                    isProvisional = true;
                    ProvisionalReason = "RESERVED WINDOW CAPACITY";
                }

                lstBookedVolumePerposed = lstFixedSlot.FindAll(delegate(MASSIT_TimeWindowBE vol) {
                    if (vol.MaximumPallets > 0) {
                        if (vol.MaximumPallets >= Convert.ToInt32(TotalPallets)) {
                            // We just found fixed slot. Capture it.
                            return true;
                        }
                        else {
                            // Not a fixed slot
                            return false;
                        }
                    }
                    else {
                        return true;
                    }
                });

                if (lstBookedVolumePerposed.Count == 0) {
                    isProvisional = true;
                    ProvisionalReason = "RESERVED WINDOW CAPACITY";
                }

                lstBookedVolumePerposed = lstFixedSlot.FindAll(delegate(MASSIT_TimeWindowBE vol) {
                    if (vol.MaximumLines > 0) {
                        if (vol.MaximumLines >= Convert.ToInt32(TotalLines)) {
                            // We just found fixed slot. Capture it.
                            return true;
                        }
                        else {
                            // Not a fixed slot
                            return false;
                        }
                    }
                    else {
                        return true;
                    }
                });

                if (lstBookedVolumePerposed.Count == 0) {
                    isProvisional = true;
                    ProvisionalReason = "RESERVED WINDOW CAPACITY";
                }

            }
        }

        #endregion
        bool isWarning = false;
        bool isSlotFound = false;
        if (
            (strSuggestedWindowSlotID == "" || strSuggestedWindowSlotID == "-1"))
        {  //Stage 8 Point 19
            #region Window Slot

            //Find Window for suggestion

            MASSIT_TimeWindowBE objMASSIT_TimeWindowBE = new MASSIT_TimeWindowBE();

            objMASSIT_TimeWindowBE.Action = "GetProposedWindow"; 
            objMASSIT_TimeWindowBE.ExcludedCarrierIDs = Convert.ToString(ddlCarrier.SelectedItem.Value);
            objMASSIT_TimeWindowBE.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
            objMASSIT_TimeWindowBE.ScheduleDate = LogicalSchedulDateTime;
            objMASSIT_TimeWindowBE.MaximumPallets = Convert.ToInt32(totalPallets);
            objMASSIT_TimeWindowBE.MaximumCartons = Convert.ToInt32(totalCartons);
            objMASSIT_TimeWindowBE.MaximumLines = Convert.ToInt32(totalLines);

            //Edit Case
            if (hdnBookingRef.Value != "") {
                objMASSIT_TimeWindowBE.BookingRef = hdnBookingRef.Value;
            }

            //handling multiple vendor ids

            List<MASSIT_TimeWindowBE> lstAvailableSlots = new List<MASSIT_TimeWindowBE>();
            List<MASSIT_TimeWindowBE> lstWindows;
            foreach (string vendorId in strVendorIDs) {
                objMASSIT_TimeWindowBE.ExcludedVendorIDs = vendorId;
                lstWindows = oMASSIT_TimeWindowBAL.GetProposedWindowBAL(objMASSIT_TimeWindowBE);
                lstAvailableSlots.AddRange(lstWindows);
            }

            string str = "=======================================================================================================================";
            StringBuilder sb = new StringBuilder();
            sb.Append(str + "\r\n");
            sb.Append("Start Carrier Window Slot Trace" + "\r\n");
            
            sb.Append("Carrier :" + ddlCarrier.SelectedValue + "\r\n");
            sb.Append("UserID :" + Session["UserID"] + "\r\n");
            sb.Append("User :" + Session["UserName"] + "\r\n");
            LogUtility.WriteTrace(sb);


            LoggingWindow("GetProposedWindow", lstAvailableSlots);

            //To check the max time limit
            if (lstAvailableSlots.Count > 0) {
                for (int iCount = 0; iCount < lstAvailableSlots.Count; iCount++) {
                    if (CheckMaxTimeConstraint(lstAvailableSlots[iCount])) {
                        lstAvailableSlots.Remove(lstAvailableSlots[iCount]);
                    }
                }
            }

            LoggingWindow("CheckMaxTimeConstraint", lstAvailableSlots);

            //To check the Total time limit
            if (lstAvailableSlots.Count > 0) {
                for (int iCount = 0; iCount < lstAvailableSlots.Count; iCount++) {
                    if (CheckTotalAvailableTimeConstraint(lstAvailableSlots[iCount])) {
                        lstAvailableSlots.Remove(lstAvailableSlots[iCount]);
                    }
                }
            }
            LoggingWindow("CheckTotalAvailableTimeConstraint", lstAvailableSlots);


            //To check the SKU Door No and Door Type

            List<MASSIT_TimeWindowBE> lstSKUAvailableSlots = null;
          
            if (lstAvailableSlots.Count > 0)
                {
                    if (bSKUMatrixDoorFound)
                    {
                        lstSKUAvailableSlots = lstAvailableSlots.Where(WindowList => WindowList.IsCheckSKUTable == true && WindowList.SiteDoorNumberID == iSiteDoorNumberID && WindowList.SiteDoorTypeID == iSKUDoorTypeID).ToList();
                        LoggingWindow("IsCheckSKUTable == true", lstSKUAvailableSlots);
                        if (lstSKUAvailableSlots.Count == 0)
                        {
                            lstSKUAvailableSlots = lstAvailableSlots.Where(WindowList => WindowList.SiteDoorNumberID == iSiteDoorNumberID && WindowList.SiteDoorTypeID == iSKUDoorTypeID).ToList();
                        }
                    }
                    else
                    {
                        lstSKUAvailableSlots = lstAvailableSlots.Where(WindowList => WindowList.IsCheckSKUTable == false).ToList();
                        LoggingWindow("IsCheckSKUTable == false", lstSKUAvailableSlots);
                    }
                }
            
            //Final Window for suggestion
            MASSIT_TimeWindowBE FinalSuggestedWindow = null;
            
            if (lstSKUAvailableSlots != null && lstSKUAvailableSlots.Count > 0) {
                //Vendor Time Constraint
                if (!string.IsNullOrEmpty(BeforeSlotTimeID)) {
                    lstSKUAvailableSlots = lstSKUAvailableSlots.Where(WindowList => Convert.ToInt32(WindowList.BeforeOrderBYId) >= Convert.ToInt32(BeforeSlotTimeID)).ToList();
                    LoggingExtra("BeforeSlotTimeID", BeforeSlotTimeID);
                    LoggingWindow("BeforeSlotTimeID", lstSKUAvailableSlots);
                }
                if (!string.IsNullOrEmpty(AfterSlotTimeID)) {
                    lstSKUAvailableSlots = lstSKUAvailableSlots.Where(WindowList => Convert.ToInt32(WindowList.BeforeOrderBYId) < Convert.ToInt32(AfterSlotTimeID)).ToList();
                    LoggingExtra("AfterSlotTimeID", AfterSlotTimeID);
                    LoggingWindow("AfterSlotTimeID", lstSKUAvailableSlots);
                }


                //----Stage 5 Point 1-----//
                //Vendors Time Constraint//
                if (ddlSite.IsBookingExclusions) {   //Only if Booking constraints set to yes 

                    int VendorBeforeSlotTimeID = 0;
                    int VendorAfterSlotTimeID = 300;

                    List<UP_VendorBE> lstUPVendors = null;
                    UP_VendorBE[] newArray = null;

                    if (ViewState["UPVendors"] != null) {
                        newArray = (UP_VendorBE[])ViewState["UPVendors"];
                        lstUPVendors = new List<UP_VendorBE>(newArray);

                        if (lstUPVendors.Count > 0) {
                            for (int iVendCount = 0; iVendCount < lstUPVendors.Count; iVendCount++) {
                                VendorBeforeSlotTimeID = lstUPVendors[0].VendorDetails.BeforeOrderBYId;
                                VendorAfterSlotTimeID = lstUPVendors[0].VendorDetails.AfterOrderBYId != 0 ? lstUPVendors[0].VendorDetails.AfterOrderBYId : 300;
                                if (lstSKUAvailableSlots.Count > 0) {
                                    lstSKUAvailableSlots = lstSKUAvailableSlots.Where(WindowList => Convert.ToInt32(WindowList.BeforeOrderBYId) >= Convert.ToInt32(VendorBeforeSlotTimeID)).ToList();
                                    LoggingExtra("VendorBeforeSlotTimeID", VendorBeforeSlotTimeID.ToString());
                                    LoggingWindow("VendorBeforeSlotTimeID", lstSKUAvailableSlots);

                                    lstSKUAvailableSlots = lstSKUAvailableSlots.Where(WindowList => Convert.ToInt32(WindowList.BeforeOrderBYId) < Convert.ToInt32(VendorAfterSlotTimeID)).ToList();

                                    LoggingExtra("CarrierAfterSlotTimeID", VendorAfterSlotTimeID.ToString());
                                    LoggingWindow("CarrierAfterSlotTimeID", lstSKUAvailableSlots);
                                }
                            }
                        }
                    }
                }
                //---------------------//

                //Constraint for Vehicle door matrix
                APPSIT_VehicleDoorMatrixBAL oMASSIT_VehicleDoorMatrixBAL = new APPSIT_VehicleDoorMatrixBAL();
               
                List<MASSIT_VehicleDoorMatrixBE> lstVehicleDoorMatrix = oMASSIT_VehicleDoorMatrixBAL.GetVehicleDoorMatrixBAL(Convert.ToInt32(ddlVehicleType.SelectedItem.Value));
                if (lstVehicleDoorMatrix.Count > 0) {                   

                    lstSKUAvailableSlots = lstSKUAvailableSlots.OrderBy(x => x.Priority).ToList();
                    lstVehicleDoorMatrix = lstVehicleDoorMatrix.OrderBy(x => x.Priority).ToList();

                    LoggingWindow("lstSKUAvailableSlots", lstSKUAvailableSlots);

                    LoggingVehicle("lstVehicleDoorMatrix", lstVehicleDoorMatrix);

                    for (int i = 0; i < lstSKUAvailableSlots.Count; i++) {
                        for (int j = 0; j < lstVehicleDoorMatrix.Count; j++) {
                            if (lstSKUAvailableSlots[i].SiteDoorTypeID == lstVehicleDoorMatrix[j].DoorTypeID) {
                                FinalSuggestedWindow = lstSKUAvailableSlots[i];
                                isSlotFound = true;
                                break;
                            }
                        }
                        if (isSlotFound)
                            break;
                    }
                }
            }

            if (FinalSuggestedWindow != null)
            {
                var lstFinal = new List<MASSIT_TimeWindowBE>();
                lstFinal.Add(FinalSuggestedWindow);
                LoggingWindow("FinalSuggestedWindow", lstFinal);
            }

            sb = new StringBuilder();
            //string str = "=======================================================================================================================";
            sb.Append(str + "\r\n");
            LogUtility.WriteTrace(sb);

            bool isSameSuggestion = false;
            if (hdnCurrentMode.Value == "Edit" && FinalSuggestedWindow != null) {
                if (strSuggestedSiteDoorNumberID != FinalSuggestedWindow.SiteDoorNumberID.ToString()) {
                    isProvisional = true;
                    ProvisionalReason = "WINDOW CAPACITY";
                }
                else {
                    isSameSuggestion = true;
                }
            }

            if (hdnCurrentMode.Value == "Edit" && FinalSuggestedWindow == null) {
                if (hdnCurrentRole.Value == "Carrier") {
                    isProvisional = true;
                    ProvisionalReason = "WINDOW CAPACITY";
                }
            }

            if (hdnCurrentMode.Value == "Add" || isSameSuggestion == true) {
                if (FinalSuggestedWindow != null) {
                    ltBookingDate.Text = FinalSuggestedWindow.StartTime + " - " + FinalSuggestedWindow.EndTime + " on " + LogicalSchedulDateTime.Day.ToString() + "/" + LogicalSchedulDateTime.Month.ToString() + "/" + LogicalSchedulDateTime.Year.ToString();
                    ltSuggestedSlotTime.Text = AtDoorName + " " + FinalSuggestedWindow.DoorNumber.ToString();
                    hdnPreservSlot.Value = FinalSuggestedWindow.StartSlotTimeID + "|" + FinalSuggestedWindow.EndSlotTimeID + "|" + FinalSuggestedWindow.SiteDoorNumberID.ToString();
                    strSuggestedWindowSlotID = FinalSuggestedWindow.TimeWindowID.ToString();
                    strSuggestedSiteDoorNumberID = FinalSuggestedWindow.SiteDoorNumberID.ToString();
                    strSuggestedSiteDoorNumber = FinalSuggestedWindow.DoorNumber.ToString();
                    hdnSuggestedWeekDay.Value = LogicalSchedulDateTime.ToString("ddd"); 
                }
                else {
                    hdnPreservSlot.Value = string.Empty;
                    btnConfirmBooking.Visible = false;
                    lblProposeWindowBookingInfo.Visible = false;
                    btnWindowAlternateSlotVendor.Visible = false;
                }
            }

            if (isSlotFound == false)
            {
                if (txtSchedulingdate.innerControltxtDate.Value == GetQueryStringValue("Scheduledate").ToString() && hdnPreviousVehicleType.Value != ddlVehicleType.SelectedValue && isDoorTypeNotChanged == false && hdnPreviousVehicleType.Value != "" && hdnCurrentMode.Value == "Edit")
                {
                    if (Session["Role"].ToString().ToLower() == "vendor" || Session["Role"].ToString().ToLower() == "carrier")
                    {
                        mdlNoSlotFound.Show();
                        isDoorCheckPopup = true;
                        return;
                    }
                    else
                    {
                        hdnPreservSlot.Value = string.Empty;
                        btnConfirmBooking.Visible = false;
                        lblProposeWindowBookingInfo.Visible = false;
                        btnWindowAlternateSlotVendor.Visible = false;
                        ltBookingDate.Visible = false;
                        ltSuggestedSlotTime.Visible = false;
                        isDoorCheckPopup = true;
                        string errMsg =   WebCommon.getGlobalResourceValue("NoSlotFound");
                        AddWarningMessages(errMsg, "NoSlotFound");                    
                        ShowWarningMessages();
                        return;
                    }
                }
            }
            else
            {
                if ((txtSchedulingdate.innerControltxtDate.Value != GetQueryStringValue("Scheduledate").ToString() && hdnCurrentMode.Value == "Edit")
                    || (txtSchedulingdate.innerControltxtDate.Value == GetQueryStringValue("Scheduledate").ToString() 
                    && hdnPreviousVehicleType.Value != ddlVehicleType.SelectedValue && isDoorTypeNotChanged == false 
                    && hdnCurrentMode.Value == "Edit"))
                {
                    ltBookingDate.Text = FinalSuggestedWindow.StartTime + " - " + FinalSuggestedWindow.EndTime + " on " + LogicalSchedulDateTime.Day.ToString() + "/" + LogicalSchedulDateTime.Month.ToString() + "/" + LogicalSchedulDateTime.Year.ToString();
                    ltSuggestedSlotTime.Text = AtDoorName + " " + FinalSuggestedWindow.DoorNumber.ToString();
                    hdnPreservSlot.Value = FinalSuggestedWindow.StartSlotTimeID + "|" + FinalSuggestedWindow.EndSlotTimeID + "|" + FinalSuggestedWindow.SiteDoorNumberID.ToString();
                    hdnSuggestedWindowSlotID.Value = FinalSuggestedWindow.TimeWindowID.ToString();
                    hdnSuggestedSiteDoorNumberID.Value = FinalSuggestedWindow.SiteDoorNumberID.ToString();
                    hdnSuggestedSiteDoorNumber.Value = FinalSuggestedWindow.DoorNumber.ToString();
                    hdnSuggestedWeekDay.Value = LogicalSchedulDateTime.ToString("ddd");
                }
            }

            //---------start Stage 9 Point 2----------------------//

            int intCapacityAvailable = 0; 
            if (isSlotFound == false)
            {
                if (TotalPallets != "0" && hdnRemainingPallets.Value != string.Empty && hdnRemainingPallets.Value != "-") {
                    if (Convert.ToInt32(hdnRemainingPallets.Value) > 0) {
                        intCapacityAvailable++;
                    }
                }

                if (TotalLines != "0" && hdnRemainingLines.Value != string.Empty && hdnRemainingLines.Value != "-") {
                    if (Convert.ToInt32(hdnRemainingLines.Value) > 0) {
                        intCapacityAvailable++;
                    }
                }
            }
            if (intCapacityAvailable == 2) {
               
                if (hdnCurrentRole.Value == "Carrier") {
                    btnConfirmBooking.Visible = false;
                    isWarning = true;
                }
            }
            else {  //---------End Stage 9 Point 2----------------------//           

                //No space remaining on compatable door  -- show advice
                if (hdnCurrentMode.Value == "Add") {
                    if (strSuggestedWindowSlotID == string.Empty || strSuggestedWindowSlotID == "-1") {
                        string errMsg = WebCommon.getGlobalResourceValue("BK_MS_28_EXT"); // "No space available on any compatible doors.Please contact Goods In office or select a different date.";

                        if (hdnCurrentRole.Value == "Carrier") {
                            //Show err msg
                            ShowErrorMessage(errMsg, "BK_MS_28");
                        }
                        else {
                            //Show err msg
                            errMsg = WebCommon.getGlobalResourceValue("BK_MS_28_INT"); //"No space available on any compatible doors.";
                            ShowErrorMessage(errMsg, "BK_MS_28");
                        }
                    }
                }

            #endregion
            }
        }




        

        //---------------------To Fix the follwoing issue--------------//
        //1. OD makes booking on a date which is already over capacity for lines
        //2. Vendor then edits this booking and raises the lines significantly
        //3. System allows this, booking does not go Provisional

        if (hdnCurrentMode.Value == "Edit" && isWarning == false) {
            int intCapacityAvailableDaily = 0;

            if (isSlotFound == false) {
                if (TotalPallets != "0" && RemainingPallets != (int?)null) {
                    if (Convert.ToInt32(TotalPallets) > RemainingPallets + BookedPallets) {
                        intCapacityAvailableDaily++;
                    }
                }

                if (TotalLines != "0" && RemainingLines != (int?)null) {
                    if (Convert.ToInt32(TotalLines) > RemainingLines + BookedLines) {
                        intCapacityAvailableDaily++;
                    }
                }
                if (intCapacityAvailableDaily > 0) {
                    if (hdnCurrentRole.Value == "Carrier") { 
                        isProvisional = true;                        
                        ProvisionalReason = "EDITED";
                    }
                }
            }
        }
        //-----------------------------------------------------------------------//

        //---------start Stage 9 Point 2----------------------//
        if (isWarning)
            ShowProvisionalWarning();
        //---------End Stage 9 Point 2----------------------//
    }

    //---------start Stage 9 Point 2----------------------//
    private void ShowProvisionalWarning() {
        mdlProConfirmMsg.Show();
        btnConfirmBooking.Visible = false;
    }

    protected void btnProContinue_Click(object sender, EventArgs e) {
        isProvisional = true;
        ProvisionalReason = "NO SPACE";
        MakeBooking(BookingType.ProvisionalNonTimedDelivery.ToString());
    }

    protected void btnProBack_Click(object sender, EventArgs e) {
        mdlProConfirmMsg.Hide();
        EncryptQueryString("APPBok_BookingOverview.aspx");
    } 
    protected void gvBookingSlots_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            if (e.Row.Cells[6].Text == "Booked")
            {
                e.Row.BackColor = Color.Purple;
                e.Row.ForeColor = Color.White;
            }
        }
    }

    protected void btnConfirmBooking_Click(object sender, EventArgs e)
    {
        ConfirmWindowBooking();
    }

    private void ConfirmWindowBooking()
    {
        string errMsg = string.Empty;
        bool isWarning = false;

        if ((isProvisional) || (hdnCapacityWarning.Value == "true" && strSuggestedWindowSlotID != "-1" && strSuggestedWindowSlotID != string.Empty))
        {
            if (hdnCurrentRole.Value == "Vendor" || hdnCurrentRole.Value == "Carrier")
            {
                errMsg = WebCommon.getGlobalResourceValue("BK_MS_23_EXT");// "The volume being booked in exceeds the scheduled capacity for your slot - This delivery has been provisionally accepted. Office Depot's Goods In team will review and will contact you if we need to reschedule this booking.";
                AddWarningMessages(errMsg, "BK_MS_23_EXT");
            }
            else
            {
                errMsg = WebCommon.getGlobalResourceValue("BK_MS_23_INT");// "The volume being booked in exceeds the scheduled capacity for this fixed slot. Click Continue to confirm booking or click Back to select a different slot.";
                AddWarningMessages(errMsg, "BK_MS_23_INT");
            }
            isWarning = true;
        }

        if (isWarning == false) {
            if (strSuggestedWindowSlotID != "-1" && strSuggestedWindowSlotID != string.Empty) {
                MakeBooking(BookingType.WindowSlot.ToString());
            }
            else if (rdoNonStandardWindow.Checked || Convert.ToBoolean(IsNonStandardWindowSlot)) {
                MakeBooking(BookingType.NonStandardWindowSlot.ToString());
            }
            else {
                //-----------Start Stage 9 point 2a/2b--------------//
                if (IsProvisionalBooking.Value == true && hdnCurrentMode.Value == "Edit") {
                    if (strSuggestedWindowSlotID == "-1" || strSuggestedWindowSlotID == string.Empty || strSuggestedWindowSlotID == "0") {
                        string errorMeesage = WebCommon.getGlobalResourceValue("confirmprovisionalbooking");
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert2", "alert('" + errorMeesage + "')", true);
                        return;
                    }
                }
                //-----------End Stage 9 point 2a/2b--------------//
            }
        }
        else {
            ShowWarningMessages();
        } 
    }

    private void BindAlternateWindow()
    {
        string TotalPallets = iTotalPallets.ToString();
        string TotalCartons = iTotalCartons.ToString();
        string TotalLifts = string.Empty;
        string TotalLines = iTotalLines.ToString();

        MASSIT_TimeWindowBE objMASSIT_TimeWindowBE = new MASSIT_TimeWindowBE();

        objMASSIT_TimeWindowBE.Action = "GetAlternateWindow";
        
        objMASSIT_TimeWindowBE.ExcludedCarrierIDs = Convert.ToString(ddlCarrier.SelectedItem.Value);
        objMASSIT_TimeWindowBE.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
        objMASSIT_TimeWindowBE.ScheduleDate = LogicalSchedulDateTime;
        objMASSIT_TimeWindowBE.MaximumPallets = Convert.ToInt32(TotalPallets);
        objMASSIT_TimeWindowBE.MaximumCartons = Convert.ToInt32(TotalCartons);
        objMASSIT_TimeWindowBE.MaximumLines = Convert.ToInt32(TotalLines);

        MASSIT_TimeWindowBAL oMASSIT_TimeWindowBAL = new MASSIT_TimeWindowBAL();
        List<MASSIT_TimeWindowBE> lstAlternateSlotsTemp ;
        List<MASSIT_TimeWindowBE> lstAlternateSlots = new List<MASSIT_TimeWindowBE>();
        DataTable myDataTable = null;
        if (strVendorIDs == null || strVendorIDs.Length < 1)
        {
            //get the vendor ids for all the POs
            if (ViewState["myDataTable"] != null)
            {
                myDataTable = (DataTable)ViewState["myDataTable"];
                var vendorCol = myDataTable.AsEnumerable().Select(x => x.Field<string>("VendorId"));
                strVendorIDs = vendorCol.ToArray();
            }
        }
        foreach (string vendorId in strVendorIDs)
        {
            objMASSIT_TimeWindowBE.ExcludedVendorIDs = vendorId;
            lstAlternateSlotsTemp = oMASSIT_TimeWindowBAL.GetAlternateWindowBAL(objMASSIT_TimeWindowBE);
            lstAlternateSlots.AddRange(lstAlternateSlotsTemp);
        }        

        if (lstAlternateSlots.Count > 0)
        {
            FillControls.FillDropDown(ref ddlAlternateWindow, lstAlternateSlots, "WindowName", "TimeWindowID", "--Select--");
        }
    }

    private void BindDoorNumber()
    {
        MASSIT_TimeWindowBAL oMASSIT_TimeWindowBAL = new MASSIT_TimeWindowBAL();
        List<MASSIT_TimeWindowBE> lstDoorNo = oMASSIT_TimeWindowBAL.GetDoorNoDetailsBAL(Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value));
        if (lstDoorNo.Count > 0)
        {
            FillControls.FillDropDown(ref ddlDoorName, lstDoorNo, "DoorNumber", "SiteDoorNumberID", "Select");
        }
    }

    private void BindStartEndTime()
    {
        MASSIT_TimeWindowBAL oMASSIT_TimeWindowBAL = new MASSIT_TimeWindowBAL();
        SYS_SlotTimeBAL oSYS_SlotTimeBAL = new SYS_SlotTimeBAL();
        List<SYS_SlotTimeBE> lstSlotTime = oSYS_SlotTimeBAL.GetTimeBAL();
        if (lstSlotTime.Count > 0)
        {
            FillControls.FillDropDown(ref ddlFrom, lstSlotTime, "SlotTime", "SlotTimeID");        
            FillControls.FillDropDown(ref ddlTo, lstSlotTime, "SlotTime", "SlotTimeID");
        }
    }

    protected void btnEnterAltTime_Click(object sender, EventArgs e)
    {
        BindDoorNumber();
        BindStartEndTime();
        BindAlternateWindow();
        if (Convert.ToBoolean(IsNonStandardWindowSlot))
        {
            rdoNonStandardWindow.Checked = true;
            rdoNonStandardWindow_CheckedChanged(this, EventArgs.Empty);
        }
        else
        {
            if ((strSuggestedWindowSlotID != "" || strSuggestedWindowSlotID != "-1") && ddlAlternateWindow.Items.Count > 0)
            {
                if (ddlAlternateWindow.Items.FindByValue(strSuggestedWindowSlotID) != null)
                {
                    ddlAlternateWindow.SelectedValue = strSuggestedWindowSlotID;
                    ltTimeSuggested_2.Text = ltBookingDate.Text + " " + ltSuggestedSlotTime.Text;
                }
            }
            rdoStandardWindow.Checked = true;
            rdoStandardWindow_CheckedChanged(this, EventArgs.Empty);
        }
        
        mvBookingEdit.ActiveViewIndex = 3;
    }

    protected void btnReqAltTime_Click(object sender, EventArgs e)
    {
        isProvisional = true;
        ProvisionalReason = "WINDOW CAPACITY";
        ConfirmWindowBooking();
    }

    private void ConfirmFixedSlotBooking()
    {
        string errMsg = string.Empty;
        bool isWarning = false; 
        if (hdnCapacityWarning.Value == "true" && hdnSuggestedFixedSlotID.Value != "-1" && hdnSuggestedFixedSlotID.Value != string.Empty) {
            if (hdnCurrentRole.Value == "Vendor" || hdnCurrentRole.Value == "Carrier") {
                errMsg = WebCommon.getGlobalResourceValue("BK_MS_23_EXT");// "The volume being booked in exceeds the scheduled capacity for your slot - This delivery has been provisionally accepted. Office Depot's Goods In team will review and will contact you if we need to reschedule this booking.";
                AddWarningMessages(errMsg, "BK_MS_23_EXT");
                isWarning = true;
            }
            else {
                if (!(hdnCurrentMode.Value == "Edit" && GetQueryStringValue("BP") != null && GetQueryStringValue("BP").ToString() == "Pro")) {  // If coming from Provisional page , ignore fixed slot capacity -- Phase 12 R2 Point 2
                    errMsg = WebCommon.getGlobalResourceValue("BK_MS_23_INT");// "The volume being booked in exceeds the scheduled capacity for this fixed slot. Click Continue to confirm booking or click Back to select a different slot.";
                    AddWarningMessages(errMsg, "BK_MS_23_INT");
                    isWarning = true;
                }
            }
        }

        if (isWarning)
            ShowWarningMessages();
    }

    private void MakeBooking(string ThisBookingType)
    {
        if (Session["BookingCreated"] != null && Convert.ToBoolean(Session["BookingCreated"]) == true) {

            EncryptQueryString("APPBok_Confirm.aspx?MailSend=False&BookingID=" + Session["CreatedBookingID"].ToString() + "&IsBookingAmended=" + Session["IsBookingAmended"].ToString());
            return;
        }

         //-----------Start Stage 9 point 2a/2b--------------//
        if (ThisBookingType != BookingType.ProvisionalNonTimedDelivery.ToString()) {
            //-----------End Stage 9 point 2a/2b--------------//
            if (ThisBookingType != BookingType.NonStandardWindowSlot.ToString()) {
                if (strSuggestedWindowSlotID == "-1" || strSuggestedWindowSlotID == string.Empty)
                    return;
            } 

            if (isProvisional) {
                if (hdnCurrentRole.Value == "Vendor" || hdnCurrentRole.Value == "Carrier") {
                    ThisBookingType = BookingType.Provisional.ToString();
                }
            }
        }

        APPBOK_BookingBE oAPPBOK_BookingBE = new APPBOK_BookingBE();
        APPBOK_BookingBAL oAPPBOK_BookingBAL = new APPBOK_BookingBAL();
                

        string strBookingID = string.Empty;

        #region Commented code for point 31
        //MAS_SiteBE singleSiteSetting = GetSingleSiteSetting(Convert.ToInt32(ddlSite.innerControlddlSite.SelectedValue));
        //if (singleSiteSetting.PerBookingVendor.Trim().ToUpper().Equals("B"))
        //{
        //    strBookingID = this.MakeMultiVendorBooking(ThisBookingType);
        //}
        //else
        //{
        // Note --> Here need to cut and paste the complete below [Make Booking Logic ...] region section for point 31.
        //}
        #endregion

        #region Make Booking Logic ...
        oAPPBOK_BookingBE = new APPBOK_BookingBE();
        oAPPBOK_BookingBAL = new APPBOK_BookingBAL();
        oAPPBOK_BookingBE.VehicleType = new MASSIT_VehicleTypeBE();
        oAPPBOK_BookingBE.Delivery = new MASCNT_DeliveryTypeBE();
        oAPPBOK_BookingBE.Carrier = new MASCNT_CarrierBE();
        oAPPBOK_BookingBE.SlotTime = new SYS_SlotTimeBE();
        oAPPBOK_BookingBE.DoorNoSetup = new MASSIT_DoorNoSetupBE();
        oAPPBOK_BookingBE.FixedSlot = new MASSIT_FixedSlotBE();
        oAPPBOK_BookingBE.TimeWindow = new MASSIT_TimeWindowBE();

        if (hdnCurrentMode.Value == "Add")
        {
            oAPPBOK_BookingBE.Action = "AddCarrierBooking";
            oAPPBOK_BookingBE.IsBookingAmended = false;
            if (hdnCurrentRole.Value == "Carrier")
                oAPPBOK_BookingBE.IsOnlineBooking = true;
            else
                oAPPBOK_BookingBE.IsOnlineBooking = false;
        }
        else if (hdnCurrentMode.Value == "Edit")
        {
            oAPPBOK_BookingBE.Action = "EditCarrierBooking";
            oAPPBOK_BookingBE.BookingID = Convert.ToInt32(GetQueryStringValue("ID").ToString());
            oAPPBOK_BookingBE.BookingRef = hdnBookingRef.Value;
            oAPPBOK_BookingBE.IsBookingAmended = true;

            if (Session["Role"].ToString().ToLower() != "carrier")
            {
                oAPPBOK_BookingBE.IsBookingAccepted = true;
            }

        }

        oAPPBOK_BookingBE.SiteId = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
        oAPPBOK_BookingBE.SupplierType = "C";
        oAPPBOK_BookingBE.Delivery.DeliveryTypeID = Convert.ToInt32(ddlDeliveryType.SelectedItem.Value);
        oAPPBOK_BookingBE.ScheduleDate = LogicalSchedulDateTime;
        oAPPBOK_BookingBE.Carrier.CarrierID = Convert.ToInt32(ddlCarrier.SelectedItem.Value);
        oAPPBOK_BookingBE.OtherCarrier = txtCarrierOther.Text.Trim();
        oAPPBOK_BookingBE.VehicleType.VehicleTypeID = Convert.ToInt32(ddlVehicleType.SelectedItem.Value);
        oAPPBOK_BookingBE.OtherVehicle = txtVehicleOther.Text.Trim();
        if (IsPreAdviseNoteRequired == true)
            oAPPBOK_BookingBE.PreAdviseNotification = rdoAdvice1.Checked ? "Y" : "N";

        #region Booking type sttings ...
        if (ThisBookingType == BookingType.WindowSlot.ToString())
        {
            //oAPPBOK_BookingBE.SlotTime.SlotTimeID = Convert.ToInt32(hdnSuggestedSlotTimeID.Value);
            oAPPBOK_BookingBE.TimeWindow.TimeWindowID = Convert.ToInt32(strSuggestedWindowSlotID);
            oAPPBOK_BookingBE.DoorNoSetup.SiteDoorNumberID = Convert.ToInt32(strSuggestedSiteDoorNumberID);
            oAPPBOK_BookingBE.DoorNoSetup.DoorNumber = strSuggestedSiteDoorNumber;
            oAPPBOK_BookingBE.BookingStatusID = 1;  //Confirmed Slot
            oAPPBOK_BookingBE.BookingTypeID = 6;    //Fixed Slot
            //oAPPBOK_BookingBE.FixedSlot.FixedSlotID = Convert.ToInt32(strSuggestedWindowSlotID);
        }
        else if (ThisBookingType == BookingType.Provisional.ToString())
        {
            //oAPPBOK_BookingBE.SlotTime.SlotTimeID = Convert.ToInt32(hdnSuggestedSlotTimeID.Value);
            oAPPBOK_BookingBE.TimeWindow.TimeWindowID = Convert.ToInt32(strSuggestedWindowSlotID);
            oAPPBOK_BookingBE.DoorNoSetup.SiteDoorNumberID = Convert.ToInt32(strSuggestedSiteDoorNumberID);
            oAPPBOK_BookingBE.DoorNoSetup.DoorNumber = strSuggestedSiteDoorNumber;
            oAPPBOK_BookingBE.BookingStatusID = 1;  //Confirmed Slot
            oAPPBOK_BookingBE.BookingTypeID = 5;    //Provisional Booking
            //oAPPBOK_BookingBE.FixedSlot.FixedSlotID = Convert.ToInt32(strSuggestedWindowSlotID);
            //Stage 4  Point 5
            oAPPBOK_BookingBE.ProvisionalReason = ProvisionalReason;
            //-----------
        }
        else if (ThisBookingType == BookingType.NonStandardWindowSlot.ToString())
        {
            oAPPBOK_BookingBE.DoorNoSetup.SiteDoorNumberID = Convert.ToInt32(strSuggestedSiteDoorNumberID);
            oAPPBOK_BookingBE.DoorNoSetup.DoorNumber = strSuggestedSiteDoorNumber;
            oAPPBOK_BookingBE.NonWindowFromTimeID = Convert.ToInt32(hdnStartTimeID.Value);
            oAPPBOK_BookingBE.NonWindowToTimeID = Convert.ToInt32(hdnEndTimeID.Value);
            oAPPBOK_BookingBE.BookingStatusID = 1;   //Confirmed Slot
            oAPPBOK_BookingBE.BookingTypeID = 7;     //Non Standard Window
        }
        //----------Stage 9 Point 2--------------------//
        else if (ThisBookingType == BookingType.ProvisionalNonTimedDelivery.ToString()) {
            oAPPBOK_BookingBE.DoorNoSetup.DoorNumber = string.Empty; //0;
            oAPPBOK_BookingBE.BookingStatusID = 1;  //Confirmed Slot
            oAPPBOK_BookingBE.BookingTypeID = 5;    //Provisional Booking
            oAPPBOK_BookingBE.ProvisionalReason = ProvisionalReason;
            hdnSuggestedWeekDay.Value = LogicalSchedulDateTime.DayOfWeek.ToString().ToLower().Substring(0, 3);//weekday
        }
        //--------------------------------------------//

        // Stage 15 R2 Point No 17

        if (hdnCurrentMode.Value == "Edit" && Session["Role"].ToString().ToLower() == "carrier")
        {
            if (hdnProvisionalReason.Value == "RESERVED WINDOW CAPACITY" && hdnBookingTypeID.Value == "5")
            {
                if (hdnIsBookingAccepted.Value == "true")
                {
                    oAPPBOK_BookingBE.ProvisionalReason = "EDITED";
                }
                else
                {
                    oAPPBOK_BookingBE.ProvisionalReason = "RESERVED WINDOW CAPACITY";
                }

            }
            else if (!string.IsNullOrEmpty(strSuggestedWindowSlotID) && (ThisBookingType == BookingType.Provisional.ToString() ||
                     ThisBookingType == BookingType.ProvisionalNonTimedDelivery.ToString()))
            {
                oAPPBOK_BookingBE.ProvisionalReason = "EDITED";
            }
        }

        //---------------------------------------------//

        #endregion

        oAPPBOK_BookingBE.BookingComments = txtEditBookingCommentF.Text.Trim();
        oAPPBOK_BookingBE.UserID = Convert.ToInt32(Session["UserID"]);
        oAPPBOK_BookingBE.WeekDay = getWeekdayNo();


        //---Stage 6 point 8---------------//
        bool BookingEditFlag = false;
        if (hdnCurrentMode.Value == "Edit")
            BookingEditFlag = IsBookingEdit(oAPPBOK_BookingBE);

        string VendorIDs = string.Empty;
        DataTable myDataTable = null;
        if (ViewState["myDataTable"] != null)
            myDataTable = (DataTable)ViewState["myDataTable"];

        string strPoIds = string.Empty;
        string strPoNos = string.Empty;

        //changes due to GET ID from request query string
        if (hdnCurrentMode.Value == "Edit" && BookingEditFlag == false)
        {
            foreach (GridViewRow gvRow in gvVolume.Rows)
            {
                if (gvRow.RowType == DataControlRowType.DataRow)
                {
                    ucNumericTextbox txtVendorPallets = (ucNumericTextbox)gvRow.FindControl("txtVendorPallets");
                    ucNumericTextbox txtVendorCartons = (ucNumericTextbox)gvRow.FindControl("txtVendorCartons");
                    HiddenField hdngvVendorID = (HiddenField)gvRow.FindControl("hdngvVendorID");
                    ucLiteral ltVendorExpectedLines = (ucLiteral)gvRow.FindControl("ltVendorExpectedLines");

                    ucNumericTextbox txtLinesToBeDelivered = (ucNumericTextbox)gvRow.FindControl("txtLinesToBeDelivered");

                    strPoIds = string.Empty;
                    strPoNos = string.Empty;

                    if (myDataTable != null)
                    {
                        DataRow[] drr = myDataTable.Select("VendorID='" + hdngvVendorID.Value.ToString() + "'");
                        for (int jCount = 0; jCount < drr.Length; jCount++)
                        {
                            if (strPoIds == string.Empty)
                            {
                                strPoIds = drr[jCount]["PurchaseOrderID"].ToString();
                                strPoNos = drr[jCount]["PurchaseNumber"].ToString();
                            }
                            else
                            {
                                strPoIds = strPoIds + "," + drr[jCount]["PurchaseOrderID"].ToString();
                                strPoNos = strPoNos + "," + drr[jCount]["PurchaseNumber"].ToString();
                            }
                        }

                        if (hdngvVendorID.Value.ToString() != "-1")
                        {
                            drr = myDataTable.Select("VendorID='-1'");
                            for (int jCount = 0; jCount < drr.Length; jCount++)
                            {
                                if (strPoIds == string.Empty)
                                {
                                    strPoIds = drr[jCount]["PurchaseOrderID"].ToString();
                                    strPoNos = drr[jCount]["PurchaseNumber"].ToString();
                                }
                                else
                                {
                                    strPoIds = strPoIds + "," + drr[jCount]["PurchaseOrderID"].ToString();
                                    strPoNos = strPoNos + "," + drr[jCount]["PurchaseNumber"].ToString();
                                }
                            }
                        }
                    }

                    oAPPBOK_BookingBE.VendorID = Convert.ToInt32(hdngvVendorID.Value);
                    oAPPBOK_BookingBE.PurchaseOrdersIDs = strPoIds;
                    oAPPBOK_BookingBE.PurchaseOrders = strPoNos;

                    oAPPBOK_BookingBE.NumberOfCartons = txtVendorCartons.Text.Trim() != string.Empty ? Convert.ToInt32(txtVendorCartons.Text.Trim()) : 0;
                    oAPPBOK_BookingBE.NumberOfLift = 0;
                    oAPPBOK_BookingBE.NumberOfPallet = txtVendorPallets.Text.Trim() != string.Empty ? Convert.ToInt32(txtVendorPallets.Text.Trim()) : 0;
                    oAPPBOK_BookingBE.NumberOfLines = txtLinesToBeDelivered.Text.Trim() != string.Empty ? Convert.ToInt32(txtLinesToBeDelivered.Text.Trim()) : 0;

                    if (BookingEditFlag == false)
                        BookingEditFlag = IsBookingEdit(oAPPBOK_BookingBE, true);
                }
            }
            //---------------------------------//
        }
        oAPPBOK_BookingBE.IsEnableHazardouesItemPrompt = checkBoxEnableHazardouesItemPrompt.Checked;
        oAPPBOK_BookingBE.ISPM15CountryPalletChecking = Convert.ToBoolean(chkSiteSetting.Checked);
        oAPPBOK_BookingBE.ISPM15FromCountryID = !string.IsNullOrEmpty(ddlSourceCountry.innerControlddlCountry.SelectedValue) ? Convert.ToInt32(ddlSourceCountry.innerControlddlCountry.SelectedValue) : 0;

        int? BookingID = oAPPBOK_BookingBAL.AddCarrierWindowBookingBAL(oAPPBOK_BookingBE);
        strBookingID = Convert.ToString(BookingID);

        oAPPBOK_BookingBE.Action = "AddCarrierBookingDetails";
        oAPPBOK_BookingBE.BookingID = Convert.ToInt32(BookingID);

        VendorIDs = string.Empty;
        myDataTable = null;
        if (ViewState["myDataTable"] != null)
            myDataTable = (DataTable)ViewState["myDataTable"];

        foreach (GridViewRow gvRow in gvVolume.Rows)
        {
            if (gvRow.RowType == DataControlRowType.DataRow)
            {
                ucNumericTextbox txtVendorPallets = (ucNumericTextbox)gvRow.FindControl("txtVendorPallets");
                ucNumericTextbox txtVendorCartons = (ucNumericTextbox)gvRow.FindControl("txtVendorCartons");
                HiddenField hdngvVendorID = (HiddenField)gvRow.FindControl("hdngvVendorID");
                ucLiteral ltVendorExpectedLines = (ucLiteral)gvRow.FindControl("ltVendorExpectedLines");

                ucNumericTextbox txtLinesToBeDelivered = (ucNumericTextbox)gvRow.FindControl("txtLinesToBeDelivered");

                strPoIds = string.Empty;
                strPoNos = string.Empty;

                if (myDataTable != null)
                {
                    DataRow[] drr = myDataTable.Select("VendorID='" + hdngvVendorID.Value.ToString() + "'");
                    for (int jCount = 0; jCount < drr.Length; jCount++)
                    {
                        if (strPoIds == string.Empty)
                        {
                            strPoIds = drr[jCount]["PurchaseOrderID"].ToString();
                            strPoNos = drr[jCount]["PurchaseNumber"].ToString();
                        }
                        else
                        {
                            strPoIds = strPoIds + "," + drr[jCount]["PurchaseOrderID"].ToString();
                            strPoNos = strPoNos + "," + drr[jCount]["PurchaseNumber"].ToString();
                        }
                    }

                    if (hdngvVendorID.Value.ToString() != "-1") {
                        drr = myDataTable.Select("VendorID='-1'");
                        for (int jCount = 0; jCount < drr.Length; jCount++) {
                            if (strPoIds == string.Empty) {
                                strPoIds = drr[jCount]["PurchaseOrderID"].ToString();
                                strPoNos = drr[jCount]["PurchaseNumber"].ToString();
                            }
                            else {
                                strPoIds = strPoIds + "," + drr[jCount]["PurchaseOrderID"].ToString();
                                strPoNos = strPoNos + "," + drr[jCount]["PurchaseNumber"].ToString();
                            }
                        }
                    }
                }

                oAPPBOK_BookingBE.VendorID = Convert.ToInt32(hdngvVendorID.Value);
                oAPPBOK_BookingBE.PurchaseOrdersIDs = strPoIds;
                oAPPBOK_BookingBE.PurchaseOrders = strPoNos;

                oAPPBOK_BookingBE.NumberOfCartons = txtVendorCartons.Text.Trim() != string.Empty ? Convert.ToInt32(txtVendorCartons.Text.Trim()) : (int?)null;
                oAPPBOK_BookingBE.NumberOfLift = (int?)null;
                oAPPBOK_BookingBE.NumberOfPallet = txtVendorPallets.Text.Trim() != string.Empty ? Convert.ToInt32(txtVendorPallets.Text.Trim()) : (int?)null;
                oAPPBOK_BookingBE.NumberOfLines = txtLinesToBeDelivered.Text.Trim() != string.Empty ? Convert.ToInt32(txtLinesToBeDelivered.Text.Trim()) : (int?)null;
                oAPPBOK_BookingBAL.AddCarrierBookingDetailsBAL(oAPPBOK_BookingBE);
            }
        }
        #endregion

        #region Commented Code ...
        //if (ViewState["dtVolumeDetails"] != null) {
        //    dtVolumeDetails = (DataTable)ViewState["dtVolumeDetails"];
        //    if (dtVolumeDetails != null) {
        //        for (int iCount = 0; iCount < dtVolumeDetails.Rows.Count; iCount++) {
        //            //if (VendorIDs == string.Empty)
        //            //    VendorIDs = dtVolumeDetails.Rows[iCount]["VendorID"].ToString();
        //            //else
        //            //    VendorIDs = VendorIDs + "," + dtVolumeDetails.Rows[iCount]["VendorID"].ToString();
        //            strPo = string.Empty;
        //            if (myDataTable != null) {
        //                DataRow[] drr = myDataTable.Select("VendorID='" + dtVolumeDetails.Rows[iCount]["VendorID"].ToString() + "'");
        //                for (int jCount = 0; jCount < drr.Length; jCount++) {
        //                    if (strPo == string.Empty)
        //                        strPo = drr[jCount]["PurchaseNumber"].ToString();
        //                    else
        //                        strPo = strPo + "," + drr[jCount]["PurchaseNumber"].ToString();
        //                }
        //            }
        //        }
        //    }
        //}
        #endregion

        // Phase 15 R2-Overdue Booking Report
        oAPPBOK_BookingBE.Action = "OverdueBookingReportDetail";
        oAPPBOK_BookingBE.BookingID =Convert.ToInt32(BookingID);
        oAPPBOK_BookingBAL.OverdueBookingReportDetailBAL(oAPPBOK_BookingBE);


        Session["BookingCreated"] = true;
        Session["CreatedBookingID"] = strBookingID;
        Session["IsBookingAmended"] = BookingEditFlag.ToString();

        //----------Start Stage 9 point 2a/2b------------//
        if (ThisBookingType == BookingType.ProvisionalNonTimedDelivery.ToString())            
            EncryptQueryString("APPBok_Confirm.aspx?BookingType=PNTD&MailSend=True&BookingID=" + strBookingID + "&IsBookingAmended=" + BookingEditFlag.ToString() + "&IsBookingEditWithMail=" + IsBookingEditWithMail.ToString());
        else
            EncryptQueryString("APPBok_Confirm.aspx?MailSend=True&BookingID=" + strBookingID + "&IsBookingAmended=" + BookingEditFlag.ToString() + "&IsBookingEditWithMail=" + IsBookingEditWithMail.ToString());
        //----------End Stage 9 point 2a/2b------------//

            
    }

    private bool IsBookingEdit(APPBOK_BookingBE APPBOK_BookingBE, bool bVolumeDetails = false) {

        bool bBookingEdit = false;

        try {
            APPBOK_BookingBE oAPPBOK_BookingBE = new APPBOK_BookingBE();
            APPBOK_BookingBAL oAPPBOK_BookingBAL = new APPBOK_BookingBAL();
            oAPPBOK_BookingBE.Action = "GetBookingDeatilsByID";
            oAPPBOK_BookingBE.BookingID = Convert.ToInt32(GetQueryStringValue("ID").ToString());
            List<APPBOK_BookingBE> lstBooking = oAPPBOK_BookingBAL.GetBookingDetailsByIDBAL(oAPPBOK_BookingBE);

            if (lstBooking.Count > 0) {

                if (bVolumeDetails) {

                    List<APPBOK_BookingBE> lstVendorBooking = lstBooking.FindAll(delegate(APPBOK_BookingBE St) {
                        return St.VendorID == APPBOK_BookingBE.VendorID;
                    });

                    if (lstVendorBooking != null && lstVendorBooking.Count > 0) {
                        int iTotalPalllets = 0;
                        int iTotalLines = 0;
                        int iTotalCartons = 0;
                        int iTotalLifts = 0;

                        for (int iCount = 0; iCount < lstVendorBooking.Count; iCount++) {
                            iTotalPalllets += Convert.ToInt32(lstVendorBooking[iCount].NumberOfPallet);
                            iTotalLines += Convert.ToInt32(lstVendorBooking[iCount].NumberOfLines);
                            iTotalCartons += Convert.ToInt32(lstVendorBooking[iCount].NumberOfCartons);
                            iTotalLifts += Convert.ToInt32(lstVendorBooking[iCount].NumberOfLift);
                        }

                        if (iTotalLines != APPBOK_BookingBE.NumberOfLines)
                            bBookingEdit = true;
                        else if (iTotalPalllets != APPBOK_BookingBE.NumberOfPallet)
                            bBookingEdit = true;
                        else if (iTotalCartons != APPBOK_BookingBE.NumberOfCartons)
                            bBookingEdit = true;
                        else if (iTotalLifts != APPBOK_BookingBE.NumberOfLift)
                            bBookingEdit = true;
                        else if (!IsPoAreSame(lstVendorBooking[0].PurchaseOrders, APPBOK_BookingBE.PurchaseOrders)) //to do
                            bBookingEdit = true;
                        else if (lstVendorBooking[0].VendorID != APPBOK_BookingBE.VendorID)
                            bBookingEdit = true;
                        else if (lstBooking[0].BookingComments != APPBOK_BookingBE.BookingComments)
                            bBookingEdit = true;
                    }
                    else {
                        bBookingEdit = true;
                    }
                }

                if (bBookingEdit == false && bVolumeDetails == false) {
                    if (lstBooking[0].SlotTime.SlotTimeID != APPBOK_BookingBE.TimeWindow.TimeWindowID)
                        bBookingEdit = true;
                    else if (lstBooking[0].FixedSlot.FixedSlotID != APPBOK_BookingBE.FixedSlot.FixedSlotID)
                        bBookingEdit = true;
                    else if (lstBooking[0].DoorNoSetup.SiteDoorNumberID != APPBOK_BookingBE.DoorNoSetup.SiteDoorNumberID)
                        bBookingEdit = true;
                    else if (lstBooking[0].Carrier.CarrierID != APPBOK_BookingBE.Carrier.CarrierID)
                        bBookingEdit = true;
                    else if (lstBooking[0].Delivery.DeliveryTypeID != APPBOK_BookingBE.Delivery.DeliveryTypeID)
                        bBookingEdit = true;
                    else if (lstBooking[0].VehicleType.VehicleTypeID != APPBOK_BookingBE.VehicleType.VehicleTypeID)
                        bBookingEdit = true;
                    else if (lstBooking[0].ScheduleDate != APPBOK_BookingBE.ScheduleDate)
                        bBookingEdit = true;
                    else if (lstBooking[0].BookingComments != APPBOK_BookingBE.BookingComments)
                        bBookingEdit = true;
                }
            }
        }
        catch {
            bBookingEdit = true;
        }
        return bBookingEdit;
    }

    private bool IsPoAreSame(string OldPos, string NewPos) {

        var firstOrdered = OldPos.Split(',')
                              .Select(t => t.Trim())
                              .Where(t => t != string.Empty)
                              .OrderBy(t => t);
        var secondOrdered = NewPos.Split(',')
                                        .Select(t => t.Trim())
                                        .Where(t => t != string.Empty)
                                        .OrderBy(t => t); 
        bool stringsAreEqual = secondOrdered.All(x => firstOrdered.Contains(x));

        return stringsAreEqual;
    }

    private string MakeMultiVendorBooking(string ThisBookingType)
    {
        string strBookingID = string.Empty;
        APPBOK_BookingBE oAPPBOK_BookingBE = new APPBOK_BookingBE();
        APPBOK_BookingBAL oAPPBOK_BookingBAL = new APPBOK_BookingBAL();

        // Here looping on [gvVolume] grid because it's a distinct for Vendors.
        string strPoIds = string.Empty;
        string strPoNos = string.Empty;
        for (int index = 0; index < gvVolume.Rows.Count; index++)
        {
            #region Initializations ...
            oAPPBOK_BookingBE = new APPBOK_BookingBE();
            oAPPBOK_BookingBAL = new APPBOK_BookingBAL();
            oAPPBOK_BookingBE.VehicleType = new MASSIT_VehicleTypeBE();
            oAPPBOK_BookingBE.Delivery = new MASCNT_DeliveryTypeBE();
            oAPPBOK_BookingBE.Carrier = new MASCNT_CarrierBE();
            oAPPBOK_BookingBE.SlotTime = new SYS_SlotTimeBE();
            oAPPBOK_BookingBE.DoorNoSetup = new MASSIT_DoorNoSetupBE();
            oAPPBOK_BookingBE.FixedSlot = new MASSIT_FixedSlotBE();
            #endregion

            oAPPBOK_BookingBE.Action = "AddCarrierBooking";
            oAPPBOK_BookingBE.IsBookingAmended = false;
            oAPPBOK_BookingBE.IsOnlineBooking = hdnCurrentRole.Value == "Carrier";

            oAPPBOK_BookingBE.SiteId = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
            oAPPBOK_BookingBE.SupplierType = "C";
            oAPPBOK_BookingBE.Delivery.DeliveryTypeID = Convert.ToInt32(ddlDeliveryType.SelectedItem.Value);
            oAPPBOK_BookingBE.ScheduleDate = LogicalSchedulDateTime;
            oAPPBOK_BookingBE.Carrier.CarrierID = Convert.ToInt32(ddlCarrier.SelectedItem.Value);
            oAPPBOK_BookingBE.OtherCarrier = txtCarrierOther.Text.Trim();
            oAPPBOK_BookingBE.VehicleType.VehicleTypeID = Convert.ToInt32(ddlVehicleType.SelectedItem.Value);
            oAPPBOK_BookingBE.OtherVehicle = txtVehicleOther.Text.Trim();
            if (IsPreAdviseNoteRequired == true)
                oAPPBOK_BookingBE.PreAdviseNotification = rdoAdvice1.Checked ? "Y" : "N";

            oAPPBOK_BookingBE.IsMultiVendCarrier = index == 0 ? "N" : "Y";
            oAPPBOK_BookingBE.BookingComments = txtEditBookingCommentF.Text.Trim();
            oAPPBOK_BookingBE.UserID = Convert.ToInt32(Session["UserID"]);
            oAPPBOK_BookingBE.WeekDay = getWeekdayNo();
            int? BookingID = oAPPBOK_BookingBAL.AddCarrierBookingBAL(oAPPBOK_BookingBE);
            if (BookingID != null)
            {
                strBookingID = index == 0 ? Convert.ToString(BookingID) : string.Format("{0},{1}", strBookingID, Convert.ToString(BookingID));
            }

            oAPPBOK_BookingBE.Action = "AddCarrierBookingDetails";
            oAPPBOK_BookingBE.BookingID = Convert.ToInt32(BookingID);

            string VendorIDs = string.Empty;
            DataTable myDataTable = null;
            if (ViewState["myDataTable"] != null)
                myDataTable = (DataTable)ViewState["myDataTable"];

            ucNumericTextbox txtVendorPallets = (ucNumericTextbox)gvVolume.Rows[index].FindControl("txtVendorPallets");
            ucNumericTextbox txtVendorCartons = (ucNumericTextbox)gvVolume.Rows[index].FindControl("txtVendorCartons");
            HiddenField hdngvVendorID = (HiddenField)gvVolume.Rows[index].FindControl("hdngvVendorID");
            ucLiteral ltVendorExpectedLines = (ucLiteral)gvVolume.Rows[index].FindControl("ltVendorExpectedLines");
            ucNumericTextbox txtLinesToBeDelivered = (ucNumericTextbox)gvVolume.Rows[index].FindControl("txtLinesToBeDelivered");
            HiddenField hdnPOVendorId = (HiddenField)gvVolume.Rows[index].FindControl("hdnPOVendorId");

            strPoIds = string.Empty;
            strPoNos = string.Empty;

            if (myDataTable != null)
            {
                DataRow[] drr = myDataTable.Select("VendorID='" + hdnPOVendorId.Value.ToString() + "'");
                for (int jCount = 0; jCount < drr.Length; jCount++)
                {
                    if (strPoIds == string.Empty)
                    {
                        strPoIds = drr[jCount]["PurchaseOrderID"].ToString();
                        strPoNos = drr[jCount]["PurchaseNumber"].ToString();
                    }
                    else
                    {
                        strPoIds = strPoIds + "," + drr[jCount]["PurchaseOrderID"].ToString();
                        strPoNos = strPoNos + "," + drr[jCount]["PurchaseNumber"].ToString();
                    }
                }
            }

            oAPPBOK_BookingBE.VendorID = Convert.ToInt32(hdnPOVendorId.Value);
            oAPPBOK_BookingBE.PurchaseOrdersIDs = strPoIds;
            oAPPBOK_BookingBE.PurchaseOrders = strPoNos;

            oAPPBOK_BookingBE.NumberOfCartons = txtVendorCartons.Text.Trim() != string.Empty ? Convert.ToInt32(txtVendorCartons.Text.Trim()) : (int?)null;
            oAPPBOK_BookingBE.NumberOfLift = (int?)null;
            oAPPBOK_BookingBE.NumberOfPallet = txtVendorPallets.Text.Trim() != string.Empty ? Convert.ToInt32(txtVendorPallets.Text.Trim()) : (int?)null;
            oAPPBOK_BookingBE.NumberOfLines = txtLinesToBeDelivered.Text.Trim() != string.Empty ? Convert.ToInt32(txtLinesToBeDelivered.Text.Trim()) : (int?)null;
            oAPPBOK_BookingBAL.AddCarrierBookingDetailsBAL(oAPPBOK_BookingBE);
        }

        return strBookingID;
    }

    private int getWeekdayNo()
    {
        string Weekday = string.Empty;

        if (hdnSuggestedWeekDay.Value.Trim() != string.Empty)
            Weekday = hdnSuggestedWeekDay.Value.ToLower();
        else
            Weekday = LogicalSchedulDateTime.DayOfWeek.ToString().ToLower().Substring(0, 3);

        int iWeekNo = 0;
        switch (Weekday)
        {
            case ("sun"):
                iWeekNo = 1;
                break;
            case ("mon"):
                iWeekNo = 2;
                break;
            case ("tue"):
                iWeekNo = 3;
                break;
            case ("wed"):
                iWeekNo = 4;
                break;
            case ("thu"):
                iWeekNo = 5;
                break;
            case ("fri"):
                iWeekNo = 6;
                break;
            case ("sat"):
                iWeekNo = 7;
                break;
            default:
                iWeekNo = 0;
                break;
        }
        return iWeekNo;
    }

    private string getWeekday(int iWeekNo)
    {
        string Weekday = string.Empty;

        switch (iWeekNo)
        {
            case (1):
                Weekday = "sun";
                break;
            case (2):
                Weekday = "mon";
                break;
            case (3):
                Weekday = "tue";
                break;
            case (4):
                Weekday = "wed";
                break;
            case (5):
                Weekday = "thu";
                break;
            case (6):
                Weekday = "fri";
                break;
            case (7):
                Weekday = "sat";
                break;
            default:
                Weekday = string.Empty;
                break;
        }
        return Weekday;
    }

    protected void btnMakeNonTimeBooking_Click(object sender, EventArgs e)
    {
        string errMsg = string.Empty;

        if (hdnCurrentRole.Value == "Vendor" || hdnCurrentRole.Value == "Carrier")
        {
            errMsg = WebCommon.getGlobalResourceValue("BK_MS_24_EXT_1") + " " + ActualSchedulDateTime.ToString("dd/MM/yyyy") + " " + WebCommon.getGlobalResourceValue("BK_MS_24_EXT_2");  // "This booking is to be made on " + txtSchedulingdate.GetDate.ToString() + " with no fixed time. Please ensure you make the delivery within the site's receiving hours. ";
            ShowErrorMessage(errMsg, "BK_MS_24_EXT");
            return;
        }
        else
        {
            errMsg = WebCommon.getGlobalResourceValue("BK_MS_24_INT");  // "This booking is to be made without a delivery time. Click 'CONTINUE' to confirm or 'BACK' to add a time to the booking";
            AddWarningMessages(errMsg, "BK_MS_24_INT");
            ShowWarningMessages();
        }
    }  

    protected void btnErrWarningConfirm_Click(object sender, EventArgs e)
    {
        // Sprint 1 - Point 18 - Start - Carrier lines to be editable
        if (ViewState["WarningStatus"] == null)
            ViewState["WarningStatus"] = -1;

        this.ErrWarningConfirmation(Convert.ToInt32(ViewState["WarningStatus"]));
        ViewState["WarningStatus"] = -1;
        //lblWarningText.Visible = false;
        // Sprint 1 - Point 18 - End - Carrier lines to be editable
    }

    // Sprint 1 - Point 18 - Start - Carrier lines to be editable
    private void ErrWarningConfirmation(int intStatus)
    {
        switch (intStatus)
        {
            case 0:
                {
                    int iTotalPallets = 0;
                    int iTotalCartons = 0;
                    int iTotalLines = 0;
                    bool isVendorVolumeNotValid = false;
                    foreach (GridViewRow gvRow in gvVolume.Rows)
                    {
                        if (gvRow.RowType == DataControlRowType.DataRow)
                        {
                            ucNumericTextbox txtVendorPallets = (ucNumericTextbox)gvRow.FindControl("txtVendorPallets");
                            ucNumericTextbox txtVendorCartons = (ucNumericTextbox)gvRow.FindControl("txtVendorCartons");
                            ucNumericTextbox txtLinesToBeDelivered = (ucNumericTextbox)gvRow.FindControl("txtLinesToBeDelivered");

                            string sTotalPallets = txtVendorPallets.Text.Trim() != string.Empty ? txtVendorPallets.Text.Trim() : "0";
                            string sTotalCartons = txtVendorCartons.Text.Trim() != string.Empty ? txtVendorCartons.Text.Trim() : "0";
                            string sTotalLines = txtLinesToBeDelivered.Text.Trim() != string.Empty ? txtLinesToBeDelivered.Text.Trim() : "0";


                            iTotalPallets += Convert.ToInt32(sTotalPallets);
                            iTotalCartons += Convert.ToInt32(sTotalCartons);
                            iTotalLines += Convert.ToInt32(sTotalLines);

                            if ((Convert.ToInt32(TotalLines) == 0) || (Convert.ToInt32(sTotalPallets) == 0 && Convert.ToInt32(sTotalCartons) == 0))
                            {                               
                                return;
                            }

                            //---Stage 5 Point 1----//
                            if (ddlSite.IsBookingExclusions) {
                                HiddenField hdnPOVendorId = (HiddenField)gvRow.FindControl("hdnPOVendorId");
                                if (isEnteredVendorVolumeNotValid(Convert.ToInt32(hdnPOVendorId.Value), Convert.ToInt32(sTotalPallets), Convert.ToInt32(sTotalLines)))
                                    isVendorVolumeNotValid = true;
                            }
                            //----------------------------------------//
                        }
                    }
                    //---Stage 5 Point 1----//                   
                    if (isVendorVolumeNotValid)
                        ShowWarningMessages();
                    else
                        this.ProceedBooking(iTotalCartons, iTotalPallets, iTotalLines);                   
                    //----------------------------------------//
                    break;
                }
            case 1:
                {
                    this.AddPO();
                    break;
                }
            default:
                {
                   
                    break;
                }
        }
    }
    // Sprint 1 - Point 18 - End - Carrier lines to be editable

    protected void btnErrWarningBack_Click(object sender, EventArgs e)
    {
        //lblWarningText.Visible = false;
        mdlWarningConfirm.Hide();
    }  

    protected void btnAlternateSlotCarrier_Click(object sender, EventArgs e)
    {
        
    }

    private void AddWarningMessages(string ErrMessage, string CommandName)
    {
        NameValueCollection ErrorMessages = new NameValueCollection();
        if (ViewState["ErrorMessages"] != null)
        {
            ErrorMessages = (NameValueCollection)ViewState["ErrorMessages"];
        }
        if (ErrorMessages.Get(CommandName) == null)
        {
            ErrorMessages.Add(CommandName, ErrMessage);
            ViewState["ErrorMessages"] = ErrorMessages;
        }
    }

    private void ShowWarningMessages()
    {
        NameValueCollection ErrorMessages = new NameValueCollection();
        if (ViewState["ErrorMessages"] != null)
        {
            ErrorMessages = (NameValueCollection)ViewState["ErrorMessages"];
        }
        if (ErrorMessages.Keys.Count > 0)
        {
            string errMsg = ErrorMessages.Get(0);
            string CommandName = ErrorMessages.GetKey(0);
            if (CommandName == "BK_MS_26_EXT")
            {
                errMsg = @"<span style=""color:Red;font-size:14px;"" >" + errMsg + "</span>";
            }
            ltConfirmMsg.Text = errMsg;
            btnErrContinue.CommandName = CommandName;
            btnErrBack.CommandName = CommandName;
            mdlConfirmMsg.Show();
            ErrorMessages.Remove(CommandName);
            ViewState["ErrorMessages"] = ErrorMessages;
        }
    }

    //---- Stage 5 point 1---//
    private void ShowOtherWarningMsgForVandorVolume() {
         NameValueCollection ErrorMessages = new NameValueCollection();
        if (ViewState["ErrorMessages"] != null)
        {
            ErrorMessages = (NameValueCollection)ViewState["ErrorMessages"];
        }
        if (ErrorMessages.Keys.Count > 0) {

            for (int iKeyCount = 0; iKeyCount < ErrorMessages.Keys.Count; iKeyCount++) {
                if (ErrorMessages.GetKey(iKeyCount).Contains("BK_MS_18_INT_VEN_")) {
                    //ShowWarningMessages();
                    return;
                }
            }                                   
        }
        this.ProceedBooking(iTotalCartons, iTotalPallets, iTotalLines);
    }
    //----------------------//

    private void ShowErrorMessage(string ErrMessage, string CommandName)
    {
        ltErrorMsg.Text = ErrMessage;
        btnErrorMsgOK.CommandName = CommandName;
        mdlErrorMsg.Show();
    }

    protected void btnContinue_Click(object sender, CommandEventArgs e)
    {
            
        //ShowWarningMessages();

        NameValueCollection ErrorMessages = new NameValueCollection();
        if (ViewState["ErrorMessages"] != null)
        {
            ErrorMessages = (NameValueCollection)ViewState["ErrorMessages"];
        }

        if (ErrorMessages.Keys.Count > 0)
        {
            ShowWarningMessages();
            return;
        }

        if (ErrorMessages.Keys.Count == 0)
        {
            if (e.CommandName == "BK_MS_05_INT" || e.CommandName == "BK_MS_04_T_INT" || e.CommandName == "BK_MS_04_INT_Carrier" || e.CommandName == "BK_MS_06_INT" || e.CommandName == "BK_MS_27_INT")
            {
                mvBookingEdit.ActiveViewIndex = 0;
                ScriptManager.RegisterStartupScript(Page, this.GetType(), "btnshow", "$get('" + btnProceedAddPO.ClientID + "').style.display='';", true);
            }
            else if (e.CommandName == "BK_MS_11_INT" || e.CommandName == "BK_MS_12_INT" || e.CommandName == "BK_MS_13_INT" || e.CommandName == "BK_MS_14_INT")
            {
                ViewState["ByContinue"] = "ByContinue";
                BindPOGrid();
                ScriptManager.RegisterStartupScript(Page, this.GetType(), "btnshow", "$get('" + btnProceedBooking.ClientID + "').style.display='';", true);
            }

            else if (e.CommandName == "BK_MS_26_EXT" || e.CommandName == "BK_MS_26_Err" || e.CommandName == "BK_MS_18_INT" || e.CommandName == "BK_MS_18_INT_1")
            {
                BindWindowSlot();
                if (hdnCurrentRole.Value == "Vendor" || hdnCurrentRole.Value == "Carrier") {
                    isProvisional = true;
                    ProvisionalReason = "WINDOW CAPACITY";
                }
                mvBookingEdit.ActiveViewIndex = 2;
            }
            else if (e.CommandName == "BK_MS_23_EXT")
            {
                hdnCapacityWarning.Value = "false";
                isProvisional = true;
                ProvisionalReason = "WINDOW CAPACITY";
                MakeBooking(BookingType.Provisional.ToString());   // For Vendor
            }
            else if (e.CommandName == "BK_MS_35") { 
                SuggestBooking(iTotalCartons, iTotalPallets, iTotalLines);
            }
            else if (e.CommandName == "BK_MS_04_INT") {
                this.AddPO(true);
            }
            else if (e.CommandName.Contains("BK_MS_18_INT_VEN_")) {
                ShowOtherWarningMsgForVandorVolume();
            }
            else if (e.CommandName == "Booking") { //Stage 9 point 3
                DeleteBooking();
            }
            //----Stage 10 Point 10 L 5.1-----//
            else if (e.CommandName == "BK_MS_06_INT_MaxContainers_Proceed") {
                mvBookingEdit.ActiveViewIndex = 2;
                BindWindowSlot(); 
            }
            else if (e.CommandName == "BK_MS_06_INT_MaxContainers") {
                VehicleTypeSelect();
            }
            else if (e.CommandName == "BK_WMS_06_EXT")
            {
                DoContinueVolumeExceeds();
            }
            else if (e.CommandName == "BK_MS_23_INT") {
                hdnCapacityWarning.Value = "false";
               if (rdoNonStandardWindow.Checked || Convert.ToBoolean(IsNonStandardWindowSlot) || (strSuggestedSiteDoorNumberID == "-1" || strSuggestedSiteDoorNumberID == string.Empty || strSuggestedSiteDoorNumberID == "0")) {
                   MakeBooking(BookingType.NonStandardWindowSlot.ToString());// For OD Staff 
                }               
               else {
                   MakeBooking(BookingType.WindowSlot.ToString());// For OD Staff               
               }           
            }
            else if (e.CommandName == "NoSlotFound")
            {
                mvBookingEdit.ActiveViewIndex = 2;
            }
            //---------------------------------//
        }
    }

    protected void btnBack_Click(object sender, CommandEventArgs e)
    {

        if (e.CommandName == "BK_MS_05_INT" || e.CommandName == "BK_MS_04_INT_Carrier" || e.CommandName == "BK_MS_06_INT" || e.CommandName == "BK_MS_27_INT"
            || e.CommandName == "BK_MS_06_INT_MaxContainers") {
            txtSchedulingdate.innerControltxtDate.Value = string.Empty;
            mvBookingEdit.ActiveViewIndex = 0;
            ScriptManager.RegisterStartupScript(Page, this.GetType(), "btnhide", "$get('" + btnProceedAddPO.ClientID + "').style.display='none';", true);
        }
        else if (e.CommandName == "BK_MS_11_INT" || e.CommandName == "BK_MS_12_INT" || e.CommandName == "BK_MS_13_INT" || e.CommandName == "BK_MS_14_INT") {
            txtPurchaseNumber.Text = string.Empty;
            DataTable myDataTable = ViewState["myDataTable"] != null ? (DataTable)ViewState["myDataTable"] : new DataTable();
            if (myDataTable.Rows.Count <= 0)
                ScriptManager.RegisterStartupScript(Page, this.GetType(), "btnshow", "$get('" + btnProceedBooking.ClientID + "').style.display='none';", true);
        }
        else if (e.CommandName == "BK_MS_23_EXT" || (e.CommandName == "BK_MS_23_INT")) {
            BindWindowSlot();
        }
        else if (e.CommandName == "BK_MS_26_Err") { 
        }
        else if (e.CommandName == "BK_WMS_05_INT" || e.CommandName == "BK_WMS_04_INT" ||
            e.CommandName == "BK_WMS_03_INT" || e.CommandName == "BK_WMS_02_INT" || e.CommandName == "BK_WMS_01_INT") {
            ddlAlternateWindow.SelectedIndex = 0;
            ddlAlternateWindow_SelectedIndexChanged(this, EventArgs.Empty);
        }
        else if (e.CommandName == "NoSlotFound") {         
        }

        ViewState["ErrorMessages"] = null; 
    }

    protected void btnErrorMsgOK_Click(object sender, CommandEventArgs e)
    {
 
        if (e.CommandName == "BK_MS_04_EXT" || e.CommandName == "BK_MS_04_T_EXT" || e.CommandName == "BK_MS_06_EXT" || e.CommandName == "BK_MS_27_EXT" || e.CommandName == "BK_MS_36")
        {
            txtSchedulingdate.innerControltxtDate.Value = string.Empty;
        }
        else if (e.CommandName == "BK_MS_05_EXT")
        {   
            txtSchedulingdate.innerControltxtDate.Value = string.Empty;
            mvBookingEdit.ActiveViewIndex = 0; 
            return;
        }
        else if (e.CommandName == "BK_MS_11_EXT" || e.CommandName == "BK_MS_12_EXT" || e.CommandName == "BK_MS_13_EXT" || e.CommandName == "BK_MS_14_EXT" || e.CommandName == "BK_MS_04_EXT_Carrier")
        {
            txtPurchaseNumber.Text = string.Empty;
        }
        else if (e.CommandName == "BK_MS_17")
        {
            ScriptManager.RegisterStartupScript(Page, this.GetType(), "btnhide", "$get('" + btnProceedBooking.ClientID + "').style.display='none';", true);
        }
        else if (e.CommandName == "BK_MS_28") {
            ScriptManager.RegisterStartupScript(Page, this.GetType(), "btnhide", "$get('" + btnProceedAddPO.ClientID + "').style.display='none';", true);
        }
        else if (e.CommandName == "BK_MS_19")
        { 
        }
        else if (e.CommandName == "BK_MS_06_Ext_MaxContainers") {
            txtSchedulingdate.innerControltxtDate.Value = string.Empty;
            ScriptManager.RegisterStartupScript(Page, this.GetType(), "btnhide", "$get('" + btnProceedAddPO.ClientID + "').style.display='none';", true);
        }
    }

    List<APPBOK_BookingBE> lstAllFixedDoor = new List<APPBOK_BookingBE>();
    int iTimeSlotID, iSiteDoorID, iPallets, iCartons, iLifts, iLines, actSlotLength, iNextSlotLength, iNextSlotLength2, iTimeSlotOrderBYID;
    bool isClickable, isCurrentSlotFixed, isNextSlotCovered;
    string DoorIDs = ",";
    string sTimeSlot = "";
    string sDoorNo;
    string sVendorCarrierName = "";
    string NextRequiredSlot = string.Empty;
    string sVendorCarrierNameNextSlot = string.Empty;
    string sCurrentFixedSlotID = "-1";
    string sSlotStartDay = string.Empty;

    protected void GenerateNonTimeBooking()
    {

        APPBOK_BookingBE oAPPBOK_BookingBE = new APPBOK_BookingBE();
        APPBOK_BookingBAL oMASBOK_BookingBAL = new APPBOK_BookingBAL();

        oAPPBOK_BookingBE.Action = "GetNonTimeBooking";
        oAPPBOK_BookingBE.ScheduleDate = LogicalSchedulDateTime;
        oAPPBOK_BookingBE.SiteId = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);

        List<APPBOK_BookingBE> lstNonTimeBooking = oMASBOK_BookingBAL.GetNonTimeBookingBAL(oAPPBOK_BookingBE);
 

    }
    

    private string GetLowestPriority(NameValueCollection DoorTypePriority)
    {  //SPRINT 4 POINT 7
        NameValueCollection collection = (NameValueCollection)DoorTypePriority;
        string LowestPri = "1";
        if (collection != null)
        {
            foreach (string key in collection.AllKeys)
            {
                if (Convert.ToInt32(collection.Get(key)) < Convert.ToInt32(LowestPri))
                {
                    LowestPri = collection.Get(key);
                }
            }
        }
        return LowestPri;
    }
  

    private int GetRequiredTimeSlotLength(int? TotalPallets, int? TotalCartons)
    {
        int itempCartons, itempPallets;
        itempCartons = iCartons;
        itempPallets = iPallets;
 
        iPallets = TotalPallets != (int?)null ? Convert.ToInt32(TotalPallets) : 0;
        iCartons = TotalCartons != (int?)null ? Convert.ToInt32(TotalCartons) : 0;

        int reqSlotLength = GetTimeSlotLength();

        iCartons = itempCartons;
        iPallets = itempPallets;

        return reqSlotLength;
    }

    private int GetRequiredTimeSlotLengthForOthers(int? TotalPallets, int? TotalCartons, string SupplierType, int ID)
    {
        //--------start Calculate Unload Time-------------------//
        decimal TotalCalculatedTime = 0;  //In Min
        bool isTimeCalculatedForPallets = false;
        bool isTimeCalculatedForCartons = false;
        TotalPallets = TotalPallets == null ? 0 : TotalPallets;
        TotalCartons = TotalCartons == null ? 0 : TotalCartons;

        if (SupplierType == "V")
        {
            //Step1 >Check Vendor Processing Window 
            if (isTimeCalculatedForPallets == false || isTimeCalculatedForCartons == false)
            {

                MASSIT_VendorProcessingWindowBE oMASSIT_VendorProcessingWindowBE = new MASSIT_VendorProcessingWindowBE();
                APPSIT_VendorProcessingWindowBAL oMASSIT_VendorProcessingWindowBAL = new APPSIT_VendorProcessingWindowBAL();

                oMASSIT_VendorProcessingWindowBE.Action = "ShowAllConsolidate";
                oMASSIT_VendorProcessingWindowBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
                oMASSIT_VendorProcessingWindowBE.User.UserID = Convert.ToInt32(Session["UserID"]);
                oMASSIT_VendorProcessingWindowBE.VendorID = ID;
                oMASSIT_VendorProcessingWindowBE.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);


                MASSIT_VendorProcessingWindowBE lstVendor = new MASSIT_VendorProcessingWindowBE();
                lstVendor = oMASSIT_VendorProcessingWindowBAL.GetVendorDetailsByIdBAL(oMASSIT_VendorProcessingWindowBE);

                if (lstVendor != null)
                {  //Vendor Processing Window Not found
                    if (lstVendor.APP_PalletsUnloadedPerHour > 0 && lstVendor.APP_CartonsUnloadedPerHour > 0)
                    {
                        if (TotalPallets != 0 && !isTimeCalculatedForPallets)
                        {
                            TotalCalculatedTime += Convert.ToDecimal(Convert.ToInt32(TotalPallets) * (60.0 / lstVendor.APP_PalletsUnloadedPerHour));
                            isTimeCalculatedForPallets = true;
                        }
                        if (TotalCartons != 0 && !isTimeCalculatedForCartons)
                        {
                            TotalCalculatedTime += Convert.ToDecimal(Convert.ToInt32(TotalCartons) * (60.0 / lstVendor.APP_CartonsUnloadedPerHour));
                            isTimeCalculatedForCartons = true;
                        }
                    }
                }
            }
            //---------------------------------// 
        }

        if (SupplierType == "C")
        {
            //Step1 >Check Carrier Processing Window 
            if (isTimeCalculatedForPallets == false || isTimeCalculatedForCartons == false)
            {

                MASSIT_CarrierProcessingWindowBE oMASSIT_CarrerProcessingWindowBE = new MASSIT_CarrierProcessingWindowBE();
                APPSIT_CarrierProcessingWindowBAL oMASSIT_CarrerProcessingWindowBAL = new APPSIT_CarrierProcessingWindowBAL();

                oMASSIT_CarrerProcessingWindowBE.Action = "GetConstraints";
                oMASSIT_CarrerProcessingWindowBE.CarrierID = ID;
                oMASSIT_CarrerProcessingWindowBE.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
                MASSIT_CarrierProcessingWindowBE lstCarrier = oMASSIT_CarrerProcessingWindowBAL.GetConstraintsBAL(oMASSIT_CarrerProcessingWindowBE);


                if (lstCarrier != null)
                {  //Carrier Processing Window Not found
                    if (lstCarrier.APP_PalletsUnloadedPerHour > 0 && lstCarrier.APP_CartonsUnloadedPerHour > 0)
                    {
                        if (TotalPallets != 0 && !isTimeCalculatedForPallets)
                        {
                            TotalCalculatedTime += Convert.ToDecimal(Convert.ToInt32(TotalPallets) * (60.0 / lstCarrier.APP_PalletsUnloadedPerHour));
                            isTimeCalculatedForPallets = true;
                        }
                        if (TotalCartons != 0 && !isTimeCalculatedForCartons)
                        {
                            TotalCalculatedTime += Convert.ToDecimal(Convert.ToInt32(TotalCartons) * (60.0 / lstCarrier.APP_CartonsUnloadedPerHour));
                            isTimeCalculatedForCartons = true;
                        }
                    }
                }
            }
            //---------------------------------// 
        }
         

        //Step3 >Check Site Miscellaneous Settings
        if (isTimeCalculatedForPallets == false || isTimeCalculatedForCartons == false)
        {

            MAS_SiteBE oMAS_SiteBE = new MAS_SiteBE();
            MAS_SiteBAL oMAS_SiteBAL = new MAS_SiteBAL();

            oMAS_SiteBE.Action = "ShowAll";
            oMAS_SiteBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
            oMAS_SiteBE.User.UserID = Convert.ToInt32(Session["UserID"]);
            oMAS_SiteBE.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);

            List<MAS_SiteBE> lstSiteMisSettings = new List<MAS_SiteBE>();
            lstSiteMisSettings = oMAS_SiteBAL.GetSiteMisSettingBAL(oMAS_SiteBE);

            if (lstSiteMisSettings != null)
            {
                if (lstSiteMisSettings[0].AveragePalletUnloadedPerManHour > 0 && TotalPallets != 0 && isTimeCalculatedForPallets == false)
                {
                    TotalCalculatedTime += Convert.ToDecimal(Convert.ToInt32(TotalPallets) * (60.0 / lstSiteMisSettings[0].AveragePalletUnloadedPerManHour));      // Convert.ToDecimal((Convert.ToInt32(TotalPallets) / lstSiteMisSettings[0].AveragePalletUnloadedPerManHour) * 60);
                    isTimeCalculatedForPallets = true;
                }
                if (lstSiteMisSettings[0].AverageCartonUnloadedPerManHour > 0 && TotalCartons != 0 && isTimeCalculatedForCartons == false)
                {
                    TotalCalculatedTime += Convert.ToDecimal(Convert.ToInt32(TotalCartons) * (60.0 / lstSiteMisSettings[0].AverageCartonUnloadedPerManHour));      //Convert.ToDecimal((Convert.ToInt32(TotalCartons) / lstSiteMisSettings[0].AverageCartonUnloadedPerManHour) * 60);
                    isTimeCalculatedForCartons = true;
                }
            }
        }
        //---------------------------------//   

        //Calculate Slot Length
        int intResult = (int)TotalCalculatedTime % iSlotTimeLength;
        int intr = (int)TotalCalculatedTime / iSlotTimeLength;

        //Not Required in case of Window booking -- Abhinav
        //if (intResult > 0)
        //    intr++;

        if (TotalCalculatedTime > 0 && TotalCalculatedTime < 1)
            intr = 1;

        if (TotalCalculatedTime == 0)
            intr++;
        //if (intr == 1)
        //    intr--;

        return intr;
    }

    protected void ConfirmAlternateSlot()
    {
        if (rdoStandardWindow.Checked)
        {          

            //-----------Start Stage 9 point 2a/2b--------------//
            if (IsProvisionalBooking.Value == true && hdnCurrentMode.Value == "Edit") {
                if (strSuggestedWindowSlotID == "-1" || strSuggestedWindowSlotID == string.Empty || strSuggestedWindowSlotID == "0") {
                    string errorMeesage = WebCommon.getGlobalResourceValue("confirmprovisionalbooking");
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "alert2", "alert('" + errorMeesage + "')", true);
                    return;
                }
            }
            //-----------End Stage 9 point 2a/2b--------------//

            ConfirmWindowBooking();
        }
        if (rdoNonStandardWindow.Checked)
        {
            string TotalPallets = iTotalPallets.ToString();
            string TotalCartons = iTotalCartons.ToString();
            string TotalLifts = "0";
            string TotalLines = iTotalLines.ToString();

            MASSIT_TimeWindowBE objMASSIT_TimeWindowBE = new MASSIT_TimeWindowBE();

            objMASSIT_TimeWindowBE.Action = "GetWeekSetup";
            objMASSIT_TimeWindowBE.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
            objMASSIT_TimeWindowBE.ScheduleDate = LogicalSchedulDateTime;
            objMASSIT_TimeWindowBE.MaximumPallets = Convert.ToInt32(TotalPallets);
            objMASSIT_TimeWindowBE.MaximumCartons = Convert.ToInt32(TotalCartons);
            objMASSIT_TimeWindowBE.MaximumLines = Convert.ToInt32(TotalLines);
            objMASSIT_TimeWindowBE.MaximumLift = Convert.ToInt32(TotalLifts);

            MASSIT_TimeWindowBAL oMASSIT_TimeWindowBAL = new MASSIT_TimeWindowBAL();
            if (!string.IsNullOrEmpty(oMASSIT_TimeWindowBAL.GetWeekSetupBAL(objMASSIT_TimeWindowBE)) && oMASSIT_TimeWindowBAL.GetWeekSetupBAL(objMASSIT_TimeWindowBE) == "WSE")
            {
                if (hdnCurrentRole.Value != "Vendor" || hdnCurrentRole.Value != "Carrier")
                {
                    string errMsg = WebCommon.getGlobalResourceValue("BK_WMS_06_EXT"); // "Volume exceeds the scheduled capacity for Week setup Do you want to continue.
                    AddWarningMessages(errMsg, "BK_WMS_06_EXT");
                    ShowWarningMessages();
                    return;                  
                }
                else
                {
                    string errMsg = WebCommon.getGlobalResourceValue("BK_WMS_06_INT"); // "Volume exceeds the scheduled capacity for Week setup.
                    ShowErrorMessage(errMsg, "BK_WMS_06_INT");
                    return;
                }
            }
            else if (Convert.ToInt16(ddlFrom.SelectedValue) > Convert.ToInt16(ddlTo.SelectedValue)) {
                string errorMeesage = WebCommon.getGlobalResourceValue("IsStartTimeGreaterThanEndTime");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "popup", "<script>alert('" + errorMeesage + "');</script>", false);
                return;
            }
            else
            {
                strSuggestedWindowSlotID = string.Empty;
                strSuggestedSiteDoorNumberID = ddlDoorName.SelectedValue;
                strSuggestedSiteDoorNumber = ddlDoorName.SelectedItem.Text;
                hdnSuggestedWeekDay.Value = LogicalSchedulDateTime.ToString("ddd");
                hdnStartTimeID.Value = ddlFrom.SelectedValue;
                hdnEndTimeID.Value = ddlTo.SelectedValue;

                //-----------Start Stage 9 point 2a/2b--------------//
                if (IsProvisionalBooking.Value == true && hdnCurrentMode.Value == "Edit") {
                    if (strSuggestedSiteDoorNumberID == "-1" || strSuggestedSiteDoorNumberID == string.Empty || strSuggestedSiteDoorNumberID == "0") {
                        string errorMeesage = WebCommon.getGlobalResourceValue("confirmprovisionalbooking");
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert2", "alert('" + errorMeesage + "')", true);
                        return;
                    }
                }
                //-----------End Stage 9 point 2a/2b--------------//

                ConfirmWindowBooking();
            }
        }
    }

    protected void btnConfirmAlternateSlot_Click(object sender, EventArgs e)
    {
        ConfirmAlternateSlot();       
    }

    protected void btnConfirmBookingWithMail_Click(object sender, EventArgs e)
    {
        IsBookingEditWithMail = "true";
        ConfirmAlternateSlot();
    }

    protected void btnConfirmBookingWithoutMail_Click(object sender, EventArgs e)
    {
        IsBookingEditWithMail = "false";
        ConfirmAlternateSlot();
    }


    protected void ddlAlternateWindow_SelectedIndexChanged(object sender, EventArgs e)
    {
        strSuggestedWindowSlotID = string.Empty;
        strSuggestedSiteDoorNumberID = string.Empty;
        strSuggestedSiteDoorNumber = string.Empty;
        hdnSuggestedWeekDay.Value = string.Empty;
        ltTimeSuggested_2.Text = string.Empty;
        if (ddlAlternateWindow.SelectedValue != "0")
        {
            bool isWarning = false;
            string TotalPallets = iTotalPallets.ToString();
            string TotalCartons = iTotalCartons.ToString();
            string TotalLifts = string.Empty;
            string TotalLines = iTotalLines.ToString();

            MASSIT_TimeWindowBE objMASSIT_TimeWindowBE = new MASSIT_TimeWindowBE();

            objMASSIT_TimeWindowBE.Action = "GetWindowDetailCarrier";
            objMASSIT_TimeWindowBE.ExcludedVendorIDs = Convert.ToString(strVendorIDs);
            objMASSIT_TimeWindowBE.ExcludedCarrierIDs = Convert.ToString(ddlCarrier.SelectedItem.Value);
            objMASSIT_TimeWindowBE.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
            objMASSIT_TimeWindowBE.ScheduleDate = LogicalSchedulDateTime;
            objMASSIT_TimeWindowBE.MaximumPallets = Convert.ToInt32(TotalPallets);
            objMASSIT_TimeWindowBE.MaximumCartons = Convert.ToInt32(TotalCartons);
            objMASSIT_TimeWindowBE.MaximumLines = Convert.ToInt32(TotalLines);
            objMASSIT_TimeWindowBE.TimeWindowID = Convert.ToInt32(ddlAlternateWindow.SelectedValue);

            MASSIT_TimeWindowBAL oMASSIT_TimeWindowBAL = new MASSIT_TimeWindowBAL();
            MASSIT_TimeWindowBE lstAlternateSlots = oMASSIT_TimeWindowBAL.GetWindowDetailBAL(objMASSIT_TimeWindowBE);
            if (lstAlternateSlots != null && lstAlternateSlots.TimeWindowID > 0)
                ltTimeSuggested_2.Text = lstAlternateSlots.StartTime + " - " + lstAlternateSlots.EndTime + " on " + LogicalSchedulDateTime.Day.ToString() + "/" + LogicalSchedulDateTime.Month.ToString() + "/" + LogicalSchedulDateTime.Year.ToString() + " " + AtDoorName + " " + lstAlternateSlots.DoorNumber.ToString();

            if (lstAlternateSlots != null && lstAlternateSlots.TimeWindowID > 0)
            {
                if (lstAlternateSlots.MaxVolumeError == "VE")
                {
                    string errMsg = WebCommon.getGlobalResourceValue("BK_WMS_04_INT"); //"The volume being booked in exceeds the scheduled capacity for this Window.
                    AddWarningMessages(errMsg, "BK_WMS_04_INT");
                    isWarning = true;
                }
                if (lstAlternateSlots.MaxDailyCapacityError == "DCE")
                {
                    string errMsg = WebCommon.getGlobalResourceValue("BK_WMS_01_INT"); //"There is no capacity for any further deliveries on the date selected.
                    AddWarningMessages(errMsg, "BK_WMS_01_INT");
                    isWarning = true;
                }
                if (lstAlternateSlots.MaxDailyDeliveryError == "DDE")
                {
                    string errMsg = WebCommon.getGlobalResourceValue("BK_WMS_02_INT"); //"Total time exceeds for any further deliveries on the date selected.
                    AddWarningMessages(errMsg, "BK_WMS_02_INT");
                    isWarning = true;
                }
                //To check the max time limit
                if (CheckMaxTimeConstraint(lstAlternateSlots))
                {
                    string errMsg = WebCommon.getGlobalResourceValue("BK_WMS_05_INT"); //"Maximum time exceeds the scheduled capacity for this Window.
                    AddWarningMessages(errMsg, "BK_WMS_05_INT");
                    isWarning = true;
                }
                //To check the Total time limit
                if (CheckTotalAvailableTimeConstraint(lstAlternateSlots))
                {
                    string errMsg = WebCommon.getGlobalResourceValue("BK_WMS_02_INT"); //"Total time exceeds for any further deliveries on the date selected.
                    AddWarningMessages(errMsg, "BK_WMS_02_INT");
                    isWarning = true;
                }
                strSuggestedWindowSlotID = lstAlternateSlots.TimeWindowID.ToString();
                strSuggestedSiteDoorNumberID = lstAlternateSlots.SiteDoorNumberID.ToString();
                strSuggestedSiteDoorNumber = lstAlternateSlots.DoorNumber.ToString();
                hdnSuggestedWeekDay.Value = LogicalSchedulDateTime.ToString("ddd");
            }

            if (isWarning)
                ShowWarningMessages();
        }
    }
   

    public List<T> MergeListCollections<T>(List<T> firstList, List<T> secondList)
    {
        List<T> mergedList = new List<T>();
        mergedList.InsertRange(0, firstList);
        mergedList.InsertRange(mergedList.Count, secondList);
        return mergedList;
    }

    protected void btnConfirmAlternateSlotVendor_Click(object sender, EventArgs e)
    {
        if (strSuggestedWindowSlotID != "-1")
        {
            ConfirmFixedSlotBooking();
        } 
    }

    protected void hdnbutton_Click(object sender, EventArgs e)
    {       
    }

    private MAS_SiteBE GetSingleSiteSetting(int siteId)
    {
        MAS_SiteBAL oMAS_SiteBAL = new MAS_SiteBAL();
        MAS_SiteBE oMAS_SiteBE = new MAS_SiteBE();
        oMAS_SiteBE.Action = "GetSingleSiteSetting";
        oMAS_SiteBE.SiteID = siteId;
        MAS_SiteBE localMAS_SiteBE = oMAS_SiteBAL.GetSingleSiteSettingBAL(oMAS_SiteBE);
        return localMAS_SiteBE;
    }

    private void SendProvisionalRefusalLetterMail(List<APPBOK_BookingBE> lstBooking, int BookingID)
    {
        try
        {
            var oMASSIT_VendorBE = new MASSIT_VendorBE();
            var oAPPSIT_VendorBAL = new APPSIT_VendorBAL();
            string VendorEmails = string.Empty;
            List<MASSIT_VendorBE> lstVendorDetails = null;
            string[] sentTo;
            string[] language;
            string[] VendorData = new string[2];

            if (lstBooking[0].SupplierType == "C")
            {
                oMASSIT_VendorBE.Action = "GetDeletedCarriersVendor";
                oMASSIT_VendorBE.BookingID = BookingID;
                lstVendorDetails = oAPPSIT_VendorBAL.GetDeletedCarriersVendorIDBAL(oMASSIT_VendorBE);
                if (lstVendorDetails != null && lstVendorDetails.Count > 0)
                {
                    for (int i = 0; i < lstVendorDetails.Count; i++)
                    {
                        VendorData = CommonPage.GetVendorEmailsWithLanguage(lstVendorDetails[i].VendorID, Convert.ToInt32(lstBooking[0].SiteId));
                        if (VendorData.Length > 0 && !string.IsNullOrEmpty(VendorData[0]))
                        {
                            sentTo = VendorData[0].Split(',');
                            language = VendorData[1].Split(',');
                            for (int j = 0; j < sentTo.Length; j++)
                            {
                                if (sentTo[j].Trim() != "")
                                {
                                    SendMailToVendor(sentTo[j], language[j], lstBooking, BookingID);
                                }
                            }
                        }
                        else
                            SendMailToVendor(ConfigurationManager.AppSettings["DefaultEmailAddress"], "English", lstBooking, BookingID);

                    }
                }


                //Send mail to Carrier
                VendorData = CommonPage.GetCarrierDefaultEmailWithLanguage(lstBooking[0].Carrier.CarrierID);
                if (VendorData.Length > 0 && !string.IsNullOrEmpty(VendorData[0]))
                {
                    sentTo = VendorData[0].Split(',');
                    language = VendorData[1].Split(',');
                    for (int j = 0; j < sentTo.Length; j++)
                    {
                        if (sentTo[j].Trim() != "")
                        {
                            SendMailToVendor(sentTo[j], language[j], lstBooking, BookingID);
                        }
                    }
                }
                else
                    SendMailToVendor(ConfigurationManager.AppSettings["DefaultEmailAddress"], "English", lstBooking, BookingID);

                //----------------------//

            }
            else if (lstBooking[0].SupplierType == "V")
            {
                VendorData = CommonPage.GetVendorEmailsWithLanguage(lstBooking[0].VendorID, Convert.ToInt32(lstBooking[0].SiteId));
                if (VendorData.Length > 0 && !string.IsNullOrEmpty(VendorData[0]))
                {
                    sentTo = VendorData[0].Split(',');
                    language = VendorData[1].Split(',');
                    for (int j = 0; j < sentTo.Length; j++)
                    {
                        if (sentTo[j].Trim() != "")
                        {
                            SendMailToVendor(sentTo[j], language[j], lstBooking, BookingID);
                        }
                    }
                }
                else
                    SendMailToVendor(ConfigurationManager.AppSettings["DefaultEmailAddress"], "English", lstBooking, BookingID);
            }

        }
        catch { }
    }

    private void SendMailToVendor(string toAddress, string language, List<APPBOK_BookingBE> lstBooking, int BookingID)
    {
        if (!string.IsNullOrEmpty(toAddress))
        {
            #region Pallet, Lines, Cartons, DeliveryDateValue and DeliveryTimeValue ...
            string DeliveryDateValue = string.Empty;

            int iTotalPalllets, iTotalLines, iTotalCartons, iTotalLifts;
            iTotalPalllets = iTotalLines = iTotalCartons = iTotalLifts = 0;

            for (int iCount = 0; iCount < lstBooking.Count; iCount++)
            {
                iTotalPalllets += Convert.ToInt32(lstBooking[iCount].NumberOfPallet);
                iTotalLines += Convert.ToInt32(lstBooking[iCount].NumberOfLines);
                iTotalCartons += Convert.ToInt32(lstBooking[iCount].NumberOfCartons);
                iTotalLifts += Convert.ToInt32(lstBooking[iCount].NumberOfCartons);
            }

            DeliveryDateValue = lstBooking[0].ScheduleDate.Value.ToString("dd/MM/yyyy");
            string strWeekDay = lstBooking[0].ScheduleDate.Value.DayOfWeek.ToString().ToUpper();
            switch (strWeekDay)
            {
                case "SUNDAY":
                    if (lstBooking[0].WeekDay != 1)
                        DeliveryDateValue = lstBooking[0].ScheduleDate.Value.AddDays(-1).ToString("dd/MM/yyyy");
                    break;
                case "MONDAY":
                    if (lstBooking[0].WeekDay != 2)
                        DeliveryDateValue = lstBooking[0].ScheduleDate.Value.AddDays(-1).ToString("dd/MM/yyyy");
                    break;
                case "TUESDAY":
                    if (lstBooking[0].WeekDay != 3)
                        DeliveryDateValue = lstBooking[0].ScheduleDate.Value.AddDays(-1).ToString("dd/MM/yyyy");
                    break;
                case "WEDNESDAY":
                    if (lstBooking[0].WeekDay != 4)
                        DeliveryDateValue = lstBooking[0].ScheduleDate.Value.AddDays(-1).ToString("dd/MM/yyyy");
                    break;
                case "THURSDAY":
                    if (lstBooking[0].WeekDay != 5)
                        DeliveryDateValue = lstBooking[0].ScheduleDate.Value.AddDays(-1).ToString("dd/MM/yyyy");
                    break;
                case "FRIDAY":
                    if (lstBooking[0].WeekDay != 6)
                        DeliveryDateValue = lstBooking[0].ScheduleDate.Value.AddDays(-1).ToString("dd/MM/yyyy");
                    break;
                case "SATURDAY":
                    if (lstBooking[0].WeekDay != 7)
                        DeliveryDateValue = lstBooking[0].ScheduleDate.Value.AddDays(-1).ToString("dd/MM/yyyy");
                    break;
            }

            #endregion

            string htmlBody = string.Empty;
            var oSendCommunicationCommon = new sendCommunicationCommon();

            //string vendorName = string.Join(",", lstBooking.Select(x => x.Vendor.VendorName).ToArray());

            string templatePath = null;
            templatePath = path + templatePathName;
            string LanguageFile = string.Empty;

            APPBOK_CommunicationBAL oAPPBOK_CommunicationBAL = new APPBOK_CommunicationBAL();
            List<MAS_LanguageBE> oLanguages = oAPPBOK_CommunicationBAL.GetLanguages();

            foreach (MAS_LanguageBE objLanguage in oLanguages)
            {
                bool MailSentInLanguage = false;

                if (objLanguage.Language.ToLower() == language.ToLower())
                {
                    MailSentInLanguage = true;
                }

                //LanguageFile = "ProvisionalRefusalLetter." + objLanguage.Language + ".htm";
                LanguageFile = "ProvisionalRefusalLetter.english.htm";
                var singleSiteSetting = GetSingleSiteSetting(Convert.ToInt32(lstBooking[0].SiteId));
               

                using (StreamReader sReader = new StreamReader(templatePath + LanguageFile.ToLower()))
                {
                    #region mailBody
                    htmlBody = sReader.ReadToEnd();
                    //htmlBody = htmlBody.Replace("{logoInnerPath}", oSendCommunicationCommon.getAbsolutePath());

                    htmlBody = htmlBody.Replace("{DearSirMadam}", WebCommon.getGlobalResourceValue("DearSirMadam", objLanguage.Language));
                    htmlBody = htmlBody.Replace("{ProvisionalRefusalText1}", WebCommon.getGlobalResourceValue("ProvisionalRefusalText1", objLanguage.Language));

                    htmlBody = htmlBody.Replace("{GoodsInComment}", WebCommon.getGlobalResourceValue("GoodsInComment", objLanguage.Language));
                    htmlBody = htmlBody.Replace("{GINCOMMENTValue}", txtRejectProCmments.Text.Trim());

                    htmlBody = htmlBody.Replace("{BookingRef}", lstBooking[0].BookingRef);

                    htmlBody = htmlBody.Replace("{Date}", WebCommon.getGlobalResourceValue("Date", objLanguage.Language));
                    htmlBody = htmlBody.Replace("{DateValue}", DeliveryDateValue);

                    htmlBody = htmlBody.Replace("{Time}", WebCommon.getGlobalResourceValue("Time", objLanguage.Language));
                    htmlBody = htmlBody.Replace("{TimeValue}", WebCommon.getGlobalResourceValue("ProvisionalBooking", objLanguage.Language).ToUpper());

                    htmlBody = htmlBody.Replace("{Carrier}", WebCommon.getGlobalResourceValue("Carrier", objLanguage.Language));
                    htmlBody = htmlBody.Replace("{CarrierValue}", lstBooking[0].Carrier.CarrierName);

                    htmlBody = htmlBody.Replace("{VehicleTypeActual}", WebCommon.getGlobalResourceValue("VehicleTypeActual", objLanguage.Language));
                    htmlBody = htmlBody.Replace("{VehicleTypeValue}", lstBooking[0].VehicleType.VehicleType);

                    htmlBody = htmlBody.Replace("{NumberofPallets}", WebCommon.getGlobalResourceValue("NumberofPallets", objLanguage.Language));
                    htmlBody = htmlBody.Replace("{PalletsValue}", Convert.ToString(iTotalPalllets));

                    htmlBody = htmlBody.Replace("{NumberofLifts}", WebCommon.getGlobalResourceValue("NumberofLifts", objLanguage.Language));
                    htmlBody = htmlBody.Replace("{LiftsValue}", Convert.ToString(iTotalLifts));

                    htmlBody = htmlBody.Replace("{NumberofCartons}", WebCommon.getGlobalResourceValue("NumberofCartons", objLanguage.Language));
                    htmlBody = htmlBody.Replace("{CartonsValue}", Convert.ToString(iTotalCartons));

                    htmlBody = htmlBody.Replace("{NumberofPOLines}", WebCommon.getGlobalResourceValue("NumberofPOLines", objLanguage.Language));
                    htmlBody = htmlBody.Replace("{LinesValue}", Convert.ToString(iTotalLines));

                    htmlBody = htmlBody.Replace("{BookedPONo}", WebCommon.getGlobalResourceValue("BookedPONo", objLanguage.Language));
                    htmlBody = htmlBody.Replace("{BookedPOsValue}", lstBooking[0].PurchaseOrders.TrimStart(',').TrimEnd(','));
                    
                    htmlBody = htmlBody.Replace("{dd/mm/yyyy}", lstBooking[0].BookingDate);
                    htmlBody = htmlBody.Replace("{PortalLink}", "<a href=" + sPortalLink + ">" + sPortalLink + " </a>");

                    htmlBody = htmlBody.Replace("{Yoursfaithfully}", WebCommon.getGlobalResourceValue("Yoursfaithfully", objLanguage.Language));
                    //---Stage 7 Point 29-----//
                    if (singleSiteSetting != null && singleSiteSetting.NonTimeStart != string.Empty)
                    {
                        htmlBody = htmlBody.Replace("{tblVisible}", "");
                        htmlBody = htmlBody.Replace("{SiteNameValue}", singleSiteSetting.SiteName);
                        htmlBody = htmlBody.Replace("{NonTimedStartTimeValue}", singleSiteSetting.NonTimeStart);
                        htmlBody = htmlBody.Replace("{NonTimedEndTimeValue}", singleSiteSetting.NonTimeEnd);
                    }
                    else
                    {
                        htmlBody = htmlBody.Replace("{tblVisible}", "none");
                    }
                    #endregion
                }

                string sFromAddress = Convert.ToString(ConfigurationManager.AppSettings["fromAddress"]);

                // Resending of Delivery mails
                oAPPBOK_CommunicationBAL = new APPBOK_CommunicationBAL();
                clsEmail oclsEmail = new clsEmail();

                if (objLanguage.Language.ToLower() == language.ToLower())
                {
                    //ProvisionalRefusal                    
                    oclsEmail.sendMail(toAddress, htmlBody, WebCommon.getGlobalResourceValue("ProvisionalRefusal", objLanguage.Language), sFromAddress, true);
                }
                oAPPBOK_CommunicationBAL.AddDeletedItemBAL(BookingID, sFromAddress, toAddress, WebCommon.getGlobalResourceValue("ProvisionalRefusal", objLanguage.Language), htmlBody,
                    Convert.ToInt32(Session["UserID"]), CommunicationType.Enum.ProvisionalRefusal, objLanguage.LanguageID, MailSentInLanguage, MailSentInLanguage);
            }            
        }
    }
    protected void btnRejectChanges_Click(object sender, EventArgs e)
    {
        mdlRejectProvisionalChanges.Show();
    }
    protected void btnContinueChanges_2_Click(object sender, EventArgs e)
    {
        RejectChangesBooking();
    }

    protected void btnBackChanges_2_Click(object sender, EventArgs e)
    {

        mdlRejectProvisionalChanges.Hide();
    }

    private void RejectChangesBooking()
    {
        try
        {

            APPBOK_BookingBE oAPPBOK_BookingBE = new APPBOK_BookingBE();
            APPBOK_BookingBAL oAPPBOK_BookingBAL = new APPBOK_BookingBAL();
            oAPPBOK_BookingBE.FixedSlot = new BusinessEntities.ModuleBE.Appointment.SiteSettings.MASSIT_FixedSlotBE();
            oAPPBOK_BookingBE.Action = "RejectChangesBooking";
            oAPPBOK_BookingBE.BookingID = Convert.ToInt32(GetQueryStringValue("ID").ToString());
            oAPPBOK_BookingBAL.RejectChangesBookingDetailBAL(oAPPBOK_BookingBE);

            //--------------------start send reject changes letter phase 14 r2 point 20----------------------//
            sendRejectChangesLetter();
            //--------------------end send reject changes letter phase 14 r2 point 20----------------------//

            if (GetQueryStringValue("PNTB") != null && GetQueryStringValue("PNTB") == "True")
            {
                EncryptQueryString("~/ModuleUI/Appointment/ProvisionalBookings.aspx");
            }
            else if (GetQueryStringValue("BP") != null && GetQueryStringValue("BP") == "Pro")
            {
                EncryptQueryString("~/ModuleUI/Appointment/ProvisionalBookings.aspx");
            }
            else
            {
                EncryptQueryString("APPBok_BookingOverview.aspx");
            }

        }
        catch (Exception ex)
        {
            LogUtility.SaveErrorLogEntry(ex);
        }
    }

    //---Start Stage 14 Point 20-----//
    private void sendRejectChangesLetter() {
        APPBOK_BookingBE oAPPBOK_BookingBE = new APPBOK_BookingBE();
        APPBOK_BookingBAL oAPPBOK_BookingBAL = new APPBOK_BookingBAL();
        List<APPBOK_BookingBE> lstBooking = new List<APPBOK_BookingBE>();



        APPBOK_CommunicationBAL oAPPBOK_CommunicationBAL = new APPBOK_CommunicationBAL();
        List<MAS_LanguageBE> oLanguages = oAPPBOK_CommunicationBAL.GetLanguages();

        int BookingID = Convert.ToInt32(GetQueryStringValue("ID").ToString());

        oAPPBOK_BookingBE.Action = "GetBookingDeatilsByID";
        oAPPBOK_BookingBE.BookingID = BookingID;
        lstBooking = oAPPBOK_BookingBAL.GetBookingDetailsByIDBAL(oAPPBOK_BookingBE);

        if (lstBooking.Count > 0) {

            MAS_SiteBE singleSiteSetting = GetSingleSiteSetting(Convert.ToInt32(lstBooking[0].SiteId));
            try {
                MASSIT_VendorBE oMASSIT_VendorBE = new MASSIT_VendorBE();
                APPSIT_VendorBAL oAPPSIT_VendorBAL = new APPSIT_VendorBAL();

                string VendorEmails = string.Empty;
                List<MASSIT_VendorBE> lstVendorDetails = null;
                string[] sentTo;
                string[] language;
                string[] VendorData = new string[2];
                bool isMailSend = false;



                if (lstBooking[0].SupplierType == "C") {
                    oMASSIT_VendorBE.Action = "GetCarriersVendor";
                    oMASSIT_VendorBE.BookingID = BookingID;

                    lstVendorDetails = oAPPSIT_VendorBAL.GetCarriersVendorIDBAL(oMASSIT_VendorBE);
                    if (lstVendorDetails != null && lstVendorDetails.Count > 0) {
                        for (int i = 0; i < lstVendorDetails.Count; i++) {
                            isMailSend = false;
                            VendorData = CommonPage.GetVendorEmailsWithLanguage(lstVendorDetails[i].VendorID, Convert.ToInt32(lstBooking[0].SiteId));
                            if (VendorData.Length > 0 && !string.IsNullOrEmpty(VendorData[0])) {
                                sentTo = VendorData[0].Split(',');
                                language = VendorData[1].Split(',');
                                for (int j = 0; j < sentTo.Length; j++) {
                                    if (sentTo[j].Trim() != "") {
                                        SendMailToVendor(sentTo[j], language[j], lstBooking, singleSiteSetting, oLanguages);
                                        //Page.UICulture = Convert.ToString(Session["CultureInfo"]);
                                        isMailSend = true;
                                    }
                                }
                            }

                            if (!isMailSend) {
                                SendMailToVendor(ConfigurationManager.AppSettings["DefaultEmailAddress"], "English", lstBooking, singleSiteSetting, oLanguages);
                                //Page.UICulture = Convert.ToString(Session["CultureInfo"]);
                            }

                        }
                    }


                    //Send mail to Carrier
                    isMailSend = false;
                    //VendorData = CommonPage.GetCarrierDefaultEmailWithLanguage(lstBooking[0].Carrier.CarrierID);
                    VendorData = CommonPage.GetCarrierEmailsWithLanguage(lstBooking[0].Carrier.CarrierID, Convert.ToInt32(lstBooking[0].SiteId));
                    if (VendorData.Length > 0 && !string.IsNullOrEmpty(VendorData[0])) {
                        sentTo = VendorData[0].Split(',');
                        language = VendorData[1].Split(',');
                        for (int j = 0; j < sentTo.Length; j++) {
                            if (sentTo[j].Trim() != "") {
                                SendMailToVendor(sentTo[j], language[j], lstBooking, singleSiteSetting, oLanguages);
                                //Page.UICulture = Convert.ToString(Session["CultureInfo"]);
                                isMailSend = true;
                            }
                        }
                    }
                    if (!isMailSend) {
                        SendMailToVendor(ConfigurationManager.AppSettings["DefaultEmailAddress"], "English", lstBooking, singleSiteSetting, oLanguages);
                        //Page.UICulture = Convert.ToString(Session["CultureInfo"]);
                    }

                    //----------------------//

                }
                else if (lstBooking[0].SupplierType == "V") {
                    isMailSend = false;
                    VendorData = CommonPage.GetVendorEmailsWithLanguage(lstBooking[0].VendorID, Convert.ToInt32(lstBooking[0].SiteId));
                    if (VendorData.Length > 0 && !string.IsNullOrEmpty(VendorData[0])) {
                        sentTo = VendorData[0].Split(',');
                        language = VendorData[1].Split(',');
                        for (int j = 0; j < sentTo.Length; j++) {
                            if (sentTo[j].Trim() != "") {
                                SendMailToVendor(sentTo[j], language[j], lstBooking, singleSiteSetting, oLanguages);
                                //Page.UICulture = Convert.ToString(Session["CultureInfo"]);
                                isMailSend = true;
                            }
                        }
                    }
                    if (!isMailSend) {
                        SendMailToVendor(ConfigurationManager.AppSettings["DefaultEmailAddress"], "English", lstBooking, singleSiteSetting, oLanguages);
                        //Page.UICulture = Convert.ToString(Session["CultureInfo"]);
                    }
                }

            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
                SendMailToVendor(ConfigurationManager.AppSettings["DefaultEmailAddress"], "English", lstBooking, singleSiteSetting, oLanguages);
            }
        }
    }

    sendCommunication communication = new sendCommunication();
    private void SendMailToVendor(string toAddress, string language, List<APPBOK_BookingBE> lstBooking, MAS_SiteBE singleSiteSetting, List<MAS_LanguageBE> oLanguages) {
        if (!string.IsNullOrEmpty(toAddress)) {
            string htmlBody = string.Empty;
            string templatePath = null;
            templatePath = path + templatePathName;
            string LanguageFile = string.Empty;
            APPBOK_CommunicationBAL oAPPBOK_CommunicationBAL = new APPBOK_CommunicationBAL();
            clsEmail oclsEmail = new clsEmail();
            int iTotalPalllets = 0;
            int iTotalLines = 0;
            int iTotalCartons = 0;
            int iTotalLifts = 0;
            if (lstBooking.Count > 0) {

                for (int iCount = 0; iCount < lstBooking.Count; iCount++) {
                    iTotalPalllets += Convert.ToInt32(lstBooking[iCount].NumberOfPallet);
                    iTotalLines += Convert.ToInt32(lstBooking[iCount].NumberOfLines);
                    iTotalCartons += Convert.ToInt32(lstBooking[iCount].NumberOfCartons);
                    iTotalLifts += Convert.ToInt32(lstBooking[iCount].NumberOfLift);
                }
            }

            foreach (MAS_LanguageBE objLanguage in oLanguages) {
                bool MailSentInLanguage = false;

                if (objLanguage.Language.ToLower() == language.ToLower()) {
                    MailSentInLanguage = true;
                }

                LanguageFile = "RejectChanges.english.htm";

                using (StreamReader sReader = new StreamReader(templatePath + LanguageFile.ToLower())) {
                    #region mailBody
                    htmlBody = sReader.ReadToEnd();

                    htmlBody = htmlBody.Replace("{BookingReference}", WebCommon.getGlobalResourceValueByLangID("BookingReference", objLanguage.LanguageID));
                    htmlBody = htmlBody.Replace("{DearSirMadam}", WebCommon.getGlobalResourceValueByLangID("DearSirMadam", objLanguage.LanguageID));
                    htmlBody = htmlBody.Replace("{RejectChangesText1}", WebCommon.getGlobalResourceValueByLangID("RejectChangesText1", objLanguage.LanguageID));
                    htmlBody = htmlBody.Replace("{RejectChangesText2}", WebCommon.getGlobalResourceValueByLangID("RejectChangesText2", objLanguage.LanguageID));
                    htmlBody = htmlBody.Replace("{ProvisionalText7}", WebCommon.getGlobalResourceValueByLangID("ProvisionalText7", objLanguage.LanguageID));

                    /* Changed For All letters files */
                    htmlBody = htmlBody.Replace("{BookingRefValue}", lstBooking[0].BookingRef);

                    htmlBody = htmlBody.Replace("{Date}", WebCommon.getGlobalResourceValueByLangID("Date", objLanguage.LanguageID));


                    string strWeekDay = lstBooking[0].ScheduleDate.Value.DayOfWeek.ToString().ToUpper();
                    if (strWeekDay == "SUNDAY") {
                        if (lstBooking[0].WeekDay != 1)
                            htmlBody = htmlBody.Replace("{DateValue}", lstBooking[0].ScheduleDate.Value.AddDays(-1).ToString("dd/MM/yyyy"));
                    }
                    else if (strWeekDay == "MONDAY") {
                        if (lstBooking[0].WeekDay != 2)
                            htmlBody = htmlBody.Replace("{DateValue}", lstBooking[0].ScheduleDate.Value.AddDays(-1).ToString("dd/MM/yyyy"));
                    }
                    else if (strWeekDay == "TUESDAY") {
                        if (lstBooking[0].WeekDay != 3)
                            htmlBody = htmlBody.Replace("{DateValue}", lstBooking[0].ScheduleDate.Value.AddDays(-1).ToString("dd/MM/yyyy"));
                    }
                    else if (strWeekDay == "WEDNESDAY") {
                        if (lstBooking[0].WeekDay != 4)
                            htmlBody = htmlBody.Replace("{DateValue}", lstBooking[0].ScheduleDate.Value.AddDays(-1).ToString("dd/MM/yyyy"));
                    }
                    else if (strWeekDay == "THURSDAY") {
                        if (lstBooking[0].WeekDay != 5)
                            htmlBody = htmlBody.Replace("{DateValue}", lstBooking[0].ScheduleDate.Value.AddDays(-1).ToString("dd/MM/yyyy"));
                    }
                    else if (strWeekDay == "FRIDAY") {
                        if (lstBooking[0].WeekDay != 6)
                            htmlBody = htmlBody.Replace("{DateValue}", lstBooking[0].ScheduleDate.Value.AddDays(-1).ToString("dd/MM/yyyy"));
                    }
                    else if (strWeekDay == "SATURDAY") {
                        if (lstBooking[0].WeekDay != 7)
                            htmlBody = htmlBody.Replace("{DateValue}", lstBooking[0].ScheduleDate.Value.AddDays(-1).ToString("dd/MM/yyyy"));
                    }
                    htmlBody = htmlBody.Replace("{DateValue}", lstBooking[0].ScheduleDate.Value.ToString("dd/MM/yyyy"));

                    htmlBody = htmlBody.Replace("{Time}", WebCommon.getGlobalResourceValueByLangID("Time", objLanguage.LanguageID));
                    htmlBody = htmlBody.Replace("{TimeActual}", WebCommon.getGlobalResourceValueByLangID("TimeActual", objLanguage.LanguageID));
                    htmlBody = htmlBody.Replace("{TimeValue}", lstBooking[0].SlotTime.SlotTime);

                    htmlBody = htmlBody.Replace("{Carrier}", WebCommon.getGlobalResourceValueByLangID("Carrier", objLanguage.LanguageID));
                    htmlBody = htmlBody.Replace("{CarrierValue}", lstBooking[0].Carrier.CarrierName);

                    htmlBody = htmlBody.Replace("{VehicleTypeActual}", WebCommon.getGlobalResourceValueByLangID("VehicleTypeActual", objLanguage.LanguageID));
                    htmlBody = htmlBody.Replace("{VehicleType}", WebCommon.getGlobalResourceValueByLangID("VehicleType", objLanguage.LanguageID));
                    htmlBody = htmlBody.Replace("{VehicleTypeValue}", lstBooking[0].VehicleType.VehicleType);

                    htmlBody = htmlBody.Replace("{NumberofPallets}", WebCommon.getGlobalResourceValueByLangID("NumberofPallets", objLanguage.LanguageID));
                    htmlBody = htmlBody.Replace("{PalletsValue}", Convert.ToString(iTotalPalllets));

                    htmlBody = htmlBody.Replace("{NumberofLifts}", WebCommon.getGlobalResourceValueByLangID("NumberofLifts", objLanguage.LanguageID));
                    htmlBody = htmlBody.Replace("{LiftsValue}", Convert.ToString(iTotalLifts));

                    htmlBody = htmlBody.Replace("{NumberofCartons}", WebCommon.getGlobalResourceValueByLangID("NumberofCartons", objLanguage.LanguageID));
                    htmlBody = htmlBody.Replace("{CartonsValue}", Convert.ToString(iTotalCartons));

                    htmlBody = htmlBody.Replace("{NumberofPOLines}", WebCommon.getGlobalResourceValueByLangID("NumberofPOLines", objLanguage.LanguageID));
                    htmlBody = htmlBody.Replace("{LinesValue}", Convert.ToString(iTotalLines));

                    htmlBody = htmlBody.Replace("{BookedPONo}", WebCommon.getGlobalResourceValueByLangID("BookedPONo", objLanguage.LanguageID));
                    htmlBody = htmlBody.Replace("{BookedPOsValue}", lstBooking[0].PurchaseOrders.TrimStart(',').TrimEnd(','));

                    htmlBody = htmlBody.Replace("{PortalLink}", "<a href=" + sPortalLink + ">" + sPortalLink + " </a>");

                    if (singleSiteSetting != null && singleSiteSetting.NonTimeStart != string.Empty) {
                        htmlBody = htmlBody.Replace("{tblVisible}", "");
                        htmlBody = htmlBody.Replace("{SiteNameValue}", singleSiteSetting.SiteName);
                        htmlBody = htmlBody.Replace("{NonTimedStartTimeValue}", singleSiteSetting.NonTimeStart);
                        htmlBody = htmlBody.Replace("{NonTimedEndTimeValue}", singleSiteSetting.NonTimeEnd);
                    }
                    else {
                        htmlBody = htmlBody.Replace("{tblVisible}", "none");
                    }

                    htmlBody = htmlBody.Replace("{Yoursfaithfully}", WebCommon.getGlobalResourceValueByLangID("Yoursfaithfully", objLanguage.LanguageID));

                    //------------------------//
                    #endregion
                }

                string sFromAddress = Convert.ToString(ConfigurationManager.AppSettings["fromAddress"]);

                oAPPBOK_CommunicationBAL.AddItemBAL(lstBooking[0].BookingID, sFromAddress, toAddress, "Reject Changes of your Booking", htmlBody, Convert.ToInt32(Session["UserID"]), CommunicationType.Enum.ProvisionalReject, objLanguage.LanguageID, MailSentInLanguage, MailSentInLanguage);
                if (objLanguage.Language.ToLower() == language.ToLower()) {
                    oclsEmail.sendMail(toAddress, htmlBody, "Reject Changes of your Booking", sFromAddress, true);
                }
            }
        }
    }
    //---End Stage 14 Point 20-----//

    protected void gvVolume_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (hdnLine.Value != "0" && hdnProvisionalReason.Value == "EDITED" && hdnBookingTypeID.Value == "5" && Session["Role"].ToString().ToLower() != "carrier")
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Literal lblPalletsProvisional = (Literal)e.Row.FindControl("lblPalletsProvisional");
                Literal lblCartonsProvisional = (Literal)e.Row.FindControl("lblCartonsProvisional");
                Literal lblExpectedLineProvisional = (Literal)e.Row.FindControl("lblExpectedLineProvisional");
                TextBox txtVendorPallets = (TextBox)e.Row.FindControl("txtVendorPallets");
                TextBox txtLinesToBeDelivered = (TextBox)e.Row.FindControl("txtLinesToBeDelivered");
                TextBox txtVendorCartons = (TextBox)e.Row.FindControl("txtVendorCartons");
                if (lblPalletsProvisional.Text != "0" && lblCartonsProvisional.Text != "0" && lblExpectedLineProvisional.Text != "0")
                {
                    lblPalletsProvisional.Visible = true;
                    lblCartonsProvisional.Visible = true;
                    lblExpectedLineProvisional.Visible = true;
                    txtVendorPallets.ForeColor = Color.Red;
                    txtLinesToBeDelivered.ForeColor = Color.Red;
                    txtVendorCartons.ForeColor = Color.Red;
                }
                else
                {
                    lblPalletsProvisional.Visible = true;
                    lblCartonsProvisional.Visible = true;
                    lblExpectedLineProvisional.Visible = true;
                    lblPalletsProvisional.Text = "&nbsp;";
                    lblCartonsProvisional.Text = "&nbsp;";
                    lblExpectedLineProvisional.Text = "&nbsp;";
                    txtVendorPallets.ForeColor = Color.Gray;
                    txtLinesToBeDelivered.ForeColor = Color.Gray;
                    txtVendorCartons.ForeColor = Color.Gray;
                }
            }

        }
        else
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Literal lblPalletsProvisional = (Literal)e.Row.FindControl("lblPalletsProvisional");
                Literal lblCartonsProvisional = (Literal)e.Row.FindControl("lblCartonsProvisional");
                Literal lblExpectedLineProvisional = (Literal)e.Row.FindControl("lblExpectedLineProvisional");
                TextBox txtVendorPallets = (TextBox)e.Row.FindControl("txtVendorPallets");
                TextBox txtLinesToBeDelivered = (TextBox)e.Row.FindControl("txtLinesToBeDelivered");
                TextBox txtVendorCartons = (TextBox)e.Row.FindControl("txtVendorCartons");
                lblPalletsProvisional.Visible = false;
                lblCartonsProvisional.Visible = false;
                lblExpectedLineProvisional.Visible = false;
                txtVendorPallets.ForeColor = Color.Gray;
                txtLinesToBeDelivered.ForeColor = Color.Gray;
                txtVendorCartons.ForeColor = Color.Gray;
            }
        }
    }



    private void LoggingWindow(string ForWindows, List<MASSIT_TimeWindowBE> lstAvailableSlots)
    {
        StringBuilder stringbuilder = new StringBuilder();
        stringbuilder.Append(ForWindows);
        stringbuilder.Append("\r\n");
        lstAvailableSlots.Select(x => new { x.TimeWindowID, x.SiteDoorTypeID, x.BeforeOrderBYId, x.IsCheckSKUTable, x.MaximumTime, x.TotalHrsMinsAvailable,x.MaximumPallets,x.MaximumLines,x.MaxDeliveries,
            x.LessVolumePallets,
            x.GraterVolumePallets,
            x.LessVolumeLines
            ,
            x.GraterVolumeLines
        }).ToList()
                               .ForEach(x => {
                                   stringbuilder.Append(" Window :" + x.TimeWindowID +
                                                        ", Door :" + x.SiteDoorTypeID +
                                                        ", BeforeOrderBYId :" + x.BeforeOrderBYId +
                                                        ", IsCheckSKUTable :" + x.IsCheckSKUTable +
                                                        ", MaximumTime :" + x.MaximumTime +
                                                        ", TotalHrsMinsAvailable :" + x.TotalHrsMinsAvailable +
                                                        ", MaximumPallets :" + x.MaximumPallets +
                                                        ", MaximumLines :" + x.MaximumLines +
                                                        ", MaxDeliveries :" +x.MaxDeliveries+
                                                           ", LessVolumePallets :" + x.LessVolumePallets +
                                                          ", GraterVolumePallets :" + x.GraterVolumePallets +
                                                           ", LessVolumeLines :" + x.LessVolumeLines +
                                                            ", GraterVolumeLines :" + x.GraterVolumeLines +
                                                        "\r\n");
                               });

        LogUtility.WriteTrace(stringbuilder);
        stringbuilder.Append("\r\n");
        stringbuilder.Append("\r\n");
    }
    private void LoggingVehicle(string ForVehicle, List<MASSIT_VehicleDoorMatrixBE> lstVehicleDoorMatrix)
    {
        StringBuilder stringbuilder = new StringBuilder();
        stringbuilder.Append(ForVehicle);
        stringbuilder.Append("\r\n");
        lstVehicleDoorMatrix.Select(x => new { x.DoorTypeID }).ToList()
                                .ForEach(x => {
                                    stringbuilder.Append("DoorTypeID :" + x.DoorTypeID + "\r\n");
                                });

        LogUtility.WriteTrace(stringbuilder);
        stringbuilder.Append("\r\n");
        stringbuilder.Append("\r\n");
    }
    private void LoggingExtra(string forMsg, string extraMsg)
    {
        StringBuilder stringbuilder = new StringBuilder();
        stringbuilder.Append("\r\n");
        stringbuilder.Append(forMsg + ":  " + extraMsg + "\r\n");
        LogUtility.WriteTrace(stringbuilder);
        stringbuilder.Append("\r\n");
    }
    public void DoContinueVolumeExceeds()
    {
        strSuggestedWindowSlotID = string.Empty;
        strSuggestedSiteDoorNumberID = ddlDoorName.SelectedValue;
        strSuggestedSiteDoorNumber = ddlDoorName.SelectedItem.Text;
        hdnSuggestedWeekDay.Value = LogicalSchedulDateTime.ToString("ddd");
        hdnStartTimeID.Value = ddlFrom.SelectedValue;
        hdnEndTimeID.Value = ddlTo.SelectedValue;
        //-----------Start Stage 9 point 2a/2b--------------//
        if (IsProvisionalBooking.Value == true && hdnCurrentMode.Value == "Edit")
        {
            if (strSuggestedSiteDoorNumberID == "-1" || strSuggestedSiteDoorNumberID == string.Empty || strSuggestedSiteDoorNumberID == "0")
            {
                string errorMeesage = WebCommon.getGlobalResourceValue("confirmprovisionalbooking");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert2", "alert('" + errorMeesage + "')", true);
                return;
            }
        }
        //-----------End Stage 9 point 2a/2b--------------//
        ConfirmWindowBooking();
    }

    public void ShowHazordousItem()
    {
        MAS_VendorBAL mAS_VendorBAL = new MAS_VendorBAL();
        MAS_VendorBE mAS_VendorBE = new MAS_VendorBE();
        mAS_VendorBE.Action = "GetSiteSchedulingSettingsForHazardous";
        mAS_VendorBE.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedValue);
        DataSet ds = mAS_VendorBAL.GetSiteSchedulingSettingsForHazardousBAL(mAS_VendorBE);

        if (ds != null && ds.Tables.Count > 0)
        {
            var result = Convert.ToBoolean(ds.Tables[0].Rows[0][0].ToString());
            if (result == true)
            {
                divEnableHazardouesItemPrompt.Visible = true;
                divHazrdous.Visible = true;
            }
            else
            {
                divEnableHazardouesItemPrompt.Visible = false;
                divHazrdous.Visible = false;
            }
        }
    }

    protected void chkSiteSetting_CheckedChanged1(object sender, EventArgs e)
    {
        CountryData.Visible = chkSiteSetting.Checked;

        pnlPalletCheckforCountryDisplay.Visible = chkSiteSetting.Checked;

        lblYesNo.Text = chkSiteSetting.Checked ? "Yes" : "No"; 
    }
}

public class CarrierDetailsWin {
    public string CarrierName { get; set; }
    public Color CellColor { get; set; }
    public bool isCurrentSlotFixed { get; set; }
    public string CurrentFixedSlotID { get; set; }
}
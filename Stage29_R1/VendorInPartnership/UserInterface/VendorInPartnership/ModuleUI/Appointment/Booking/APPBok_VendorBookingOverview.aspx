﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    CodeFile="APPBok_VendorBookingOverview.aspx.cs" Inherits="APPBok_VendorBookingOverview"
    MaintainScrollPositionOnPostback="true" %>

<%@ Register TagName="ucDate" TagPrefix="cc2" Src="~/CommonUI/UserControls/ucBackDate.ascx" %>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>

<%@ Register Src="~/CommonUI/UserControls/ucSite.ascx" TagName="ucSite" TagPrefix="cc2" %>
<%@ Register Src="~/CommonUI/UserControls/ucSeacrhVendor.ascx" TagName="ucSeacrhVendor"
    TagPrefix="cc3" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<%@ Register TagName="ucExportButton" TagPrefix="uc1" Src="~/CommonUI/UserControls/ucExportToExcel.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2>
        <cc1:ucLabel ID="lblPreviousBookings" runat="server" Text="Previous Bookings"></cc1:ucLabel>
    </h2>
    <asp:ScriptManager ID="sp1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="up1" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <div class="right-shadow">
                <div>
                <asp:HiddenField ID="hdnRole" runat="server" Value="" />
                 <asp:HiddenField ID="hdnCurrentDate" runat="server" />
                 <asp:HiddenField ID="hdnSelectedDate" runat="server" />
                   <%-- <asp:HiddenField ID="hdnBookingWeekDay" runat="server" Value="" />
                    
                    <asp:HiddenField ID="hdnBookingFor" runat="server" Value="" />
                    <asp:HiddenField ID="hdnPrimaryKey" runat="server" Value="" />
                    <asp:HiddenField ID="hdnRecordTable" runat="server" Value="" />
                    <asp:HiddenField ID="hdnsiteid" runat="server" Value="" />
                    <asp:HiddenField ID="hdnbookingdate" runat="server" />
                    <asp:HiddenField ID="hdnLogicalDate" runat="server" />
                   
                    <asp:HiddenField ID="hdnSelectedDate" runat="server" />--%>
                    <asp:ValidationSummary ID="vs1" runat="server" ValidationGroup="a" ShowMessageBox="true"
                        ShowSummary="false" />
                </div>
                <div class="formbox">
                    <div id="divSearchButtons" runat="server">
                        <table width="100%" cellspacing="1" cellpadding="0" border="0" align="center" class="top-settings">
                            <tr>
                                <td style="font-weight: bold; width: 9%">
                                    <cc1:ucLabel ID="lblSchedulingdate" runat="server" Text="Scheduling Date"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold; width: 1%">
                                    :
                                </td>
                                <td style="font-weight: bold; width: 10%">
                                    <cc2:ucDate ClientIDMode="Static" ID="txtDate" runat="server" />
                                    <asp:RequiredFieldValidator ID="rfvDateRequired" runat="server" Display="None" ControlToValidate="txtDate$txtUCDate"
                                        ValidateEmptyText="true" ValidationGroup="a" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                </td>
                                <td style="font-weight: bold; width: 5%">
                                    <cc1:ucLabel ID="lblSitePrefixName" runat="server"></cc1:ucLabel>&nbsp;:&nbsp;&nbsp;
                                </td>
                                <td style="font-weight: bold; width: 1%">
                                    :
                                </td>
                                <td style="width: 18%">
                                    <cc2:ucSite ID="ddlSite" runat="server" />
                                </td>
                                <td style="font-weight: bold; width: 4%">
                                    <cc1:ucLabel ID="lblPO" runat="server" Text="PO#"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold; width: 1%">
                                    :
                                </td>
                                <td style="font-weight: bold; width: 13%">
                                    <cc1:ucTextbox ID="txtPurchaseNumber" runat="server" Width="100px"></cc1:ucTextbox>
                                </td>
                                <td style="font-weight: bold; width: 9%">
                                    <cc1:ucLabel ID="lblBookingRef" runat="server" Text="Booking Ref#"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold; width: 1%">
                                    :
                                </td>
                                <td style="font-weight: bold; width: 20%">
                                    <cc1:ucTextbox ID="txtBookingRef" runat="server" Width="130px"></cc1:ucTextbox>
                                </td>
                                <td style="width: 8%" align="center">
                                    <cc1:ucButton ID="btnSearch" runat="server" Text="Search" CssClass="button" OnClick="btnSearch_Click"
                                        ValidationGroup="a" />
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div style="width: 100%; overflow: auto" id="divPrint" class="fixedTable">
                        <table cellspacing="1" cellpadding="0" class="form-table">
                            <tr>
                                <td>
                                    <cc1:ucGridView ID="gvVendor" runat="server" AutoGenerateColumns="false" CssClass="grid gvclass"
                                        CellPadding="0" Width="1850px" DataKeyNames="PKID" OnSorting="gridView_Sorting"
                                        OnRowDataBound="gvVendor_RowDataBound" AllowSorting="true" AllowPaging="true"
                                        PageSize="30" OnPageIndexChanging="gvVendor_PageIndexChanging">
                                        <AlternatingRowStyle BackColor="White" ForeColor="#284775"></AlternatingRowStyle>
                                        <Columns>
                                            <asp:TemplateField HeaderText="Site" SortExpression="SiteName">
                                                <HeaderStyle Width="120px" HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Center" />
                                                <ItemTemplate>                                                 
                                                    <cc1:ucLabel ID="LBLSiteName" runat="server" Text='<%# Eval("SiteName") %>'></cc1:ucLabel>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField HeaderText="Status" DataField="BookingStatus" SortExpression="BookingStatus">
                                                <HeaderStyle Width="140px" HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:BoundField>

                                            <asp:TemplateField HeaderText="Booking Ref #" SortExpression="BookingRef">
                                                <HeaderStyle Width="170px" HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Left" />
                                                <ItemTemplate>
                                                    <asp:HiddenField ID="hdnBookingFor" runat="server" Value='<%#Eval("SupplierType")%>' />
                                                    <asp:HiddenField ID="hdnPrimaryKey" runat="server" Value='<%#Eval("PKID")%>' />
                                                    <asp:HiddenField ID="hdnBookingID" runat="server" Value='<%#Eval("PKID") %>' />
                                                    <asp:HiddenField ID="hdnsiteid" runat="server" Value='<%#Eval("SiteId") %>' />

                                                   <asp:HyperLink ID="hyplViewHistory" runat="server" Text='<%#Eval("BookingRef") %>'></asp:HyperLink>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:BoundField HeaderText="Priority" DataField="Prioirty" SortExpression="Prioirty">
                                                <HeaderStyle Width="50px" HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:BoundField HeaderText="Booking Type" DataField="BookingType" SortExpression="BookingType">
                                                <HeaderStyle Width="150px" HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:BoundField>
                                            <asp:TemplateField HeaderText="Delivery Type" SortExpression="DeliveryType">
                                                <HeaderStyle Width="150px" HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Left" />
                                                <ItemTemplate>
                                                    <cc1:ucLiteral ID="UcLiteral1" runat="server" Text='<%#Eval("DeliveryType") %>'></cc1:ucLiteral>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Expected Delivery <br/> Date" SortExpression="DeliveryDate">
                                                <HeaderStyle Width="200px" HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Center" />
                                                <ItemTemplate>
                                                    <asp:HiddenField ID="hdnExpectedDeliveryDate" runat="server" Value='<%# Eval("DeliveryDate") %>' />
                                                    <cc1:ucLabel ID="lblExpectedDeliveryDate" runat="server" Text='<%# Eval("DeliveryDate", "{0:dd/MM/yyyy}") %>'></cc1:ucLabel>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Time / Day" SortExpression="Weekday">
                                                <HeaderStyle Width="150px" HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Center" />
                                                <ItemTemplate>
                                                    <asp:HiddenField ID="hdnTimeWindowID" runat="server" Value='<%# Eval("TimeWindowID") %>' />
                                                    <cc1:ucLiteral ID="UcLiteral27_2" runat="server" Text='<%#Eval("NonWindowFromToTime") %>'
                                                        Visible="false"></cc1:ucLiteral>
                                                    <cc1:ucLiteral ID="UcLiteral27_1" runat="server" Text='<%#Eval("WindowStartEndTime") %>'
                                                        Visible="false"></cc1:ucLiteral>
                                                    <cc1:ucLiteral ID="UcLiteral27" runat="server" Text='<%#Eval("ExpectedDeliveryTime") %>'></cc1:ucLiteral>&nbsp;
                                                    <cc1:ucLiteral ID="UcLiteral28" runat="server" Text='<%# Eval("DeliveryDate", "{0:dddd}")%>'></cc1:ucLiteral>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Door Name" SortExpression="DoorNumber">
                                                <HeaderStyle Width="100px" HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Center" />
                                                <ItemTemplate>
                                                    <cc1:ucLiteral ID="UcLiteral7" runat="server" Text='<%#Eval("DoorNumber") %>'></cc1:ucLiteral>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Vendor Name" SortExpression="VendorName">
                                                <HeaderStyle Width="200px" HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Center" />
                                                <ItemTemplate>
                                                    <cc1:ucLiteral ID="UcLiteral2" runat="server" Text='<%#Eval("VendorName") %>'></cc1:ucLiteral>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Carrier Name" SortExpression="CarrierName">
                                                <HeaderStyle Width="180px" HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Center" />
                                                <ItemTemplate>
                                                    <cc1:ucLiteral ID="UcLiteral3" runat="server" Text='<%#Eval("CarrierName") %>'></cc1:ucLiteral>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField HeaderText="# Lifts Scheduled" DataField="LiftsScheduled" SortExpression="LiftsScheduled">
                                                <HeaderStyle Width="100px" HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:TemplateField HeaderText="# Pallets Scheduled" SortExpression="PalletsScheduled">
                                                <HeaderStyle Width="100px" HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Center" />
                                                <ItemTemplate>
                                                    <cc1:ucLiteral ID="UcLiteral4" runat="server" Text='<%#Eval("PalletsScheduled") %>'></cc1:ucLiteral>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="# Lines Scheduled" SortExpression="LinesScheduled">
                                                <HeaderStyle Width="100px" HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Center" />
                                                <ItemTemplate>
                                                    <cc1:ucLiteral ID="UcLiteral5" runat="server" Text='<%#Eval("LinesScheduled") %>'></cc1:ucLiteral>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="# Cartons Scheduled" SortExpression="CatronsScheduled">
                                                <HeaderStyle Width="100px" HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Center" />
                                                <ItemTemplate>
                                                    <cc1:ucLiteral ID="UcLiteral6" runat="server" Text='<%#Eval("CatronsScheduled") %>'></cc1:ucLiteral>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField HeaderText="Stock Planner" DataField="InventoryManager" SortExpression="InventoryManager">
                                                <HeaderStyle Width="150px" HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:BoundField HeaderText="WindowName" DataField="WindowName" SortExpression="WindowName">
                                                <HeaderStyle Width="220px" HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:BoundField>
                                        </Columns>
                                    </cc1:ucGridView>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <div class="bottom-shadow">
            </div>
            <div class="button-row">
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    CodeFile="receivingdashboard.aspx.cs" Inherits="ModuleUI_Appointment_Booking_receivingdashboard" MaintainScrollPositionOnPostback="true" %>

<%@ Register TagName="ucDate" TagPrefix="cc2" Src="~/CommonUI/UserControls/ucSchedulingDate.ascx" %>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/ucSite.ascx" TagName="ucSite" TagPrefix="cc2" %>

<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<%@ Register TagName="ucExportButton" TagPrefix="uc1" Src="~/CommonUI/UserControls/ucExportToExcel.ascx" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <link href="../../../Css/chartstyle.css" rel="stylesheet" type="text/css" />
    <script src="../../../Scripts/loader.js"></script>

    <asp:ScriptManager ID="scrp" runat="server"></asp:ScriptManager>

    <div class="right-shadow">
        <div>
            <div id="divSearchButtons" runat="server">
                <table width="100%" cellspacing="1" cellpadding="0" border="0" align="center" class="top-settings">
                    <tr>
                        <td style="font-weight: bold; width: 18%">
                            <cc1:ucLabel ID="lblSchedulingdate" runat="server" Text="Scheduling Date"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold; width: 1%">:
                        </td>
                        <td style="font-weight: bold; width: 14%">
                            <cc1:ucTextbox ID="txtDate" ClientIDMode="Static" runat="server" onchange="setValue1(this)"
                                ReadOnly="True" Width="70px" />
                        </td>
                        <td style="font-weight: bold; width: 17%">
                            <cc1:ucLabel ID="lblSitePrefixName" runat="server"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold; width: 1%">:
                        </td>
                        <td style="width: 21%">
                            <cc2:ucSite ID="ddlSite" runat="server" />
                        </td>
                        <td style="width: 21%">
                            <cc1:ucButton ID="btnSearch" runat="server" Text="Search" CssClass="button" OnClick="btnSearch_Click" />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>

    <div style="width: 100%; height: 150px;" id="divPrint" class="fixedTable"
        runat="server">
        <table cellspacing="1" cellpadding="0" class="form-table">
            <tr>
                <td>
                    <cc1:ucGridView ID="ucGridView1" runat="server" AutoGenerateColumns="false" CssClass="grid gvclass"
                        CellPadding="0" Width="100%">
                        <EmptyDataTemplate>
                            <div align="center">No records found.</div>
                        </EmptyDataTemplate>
                        <AlternatingRowStyle BackColor="White" ForeColor="#284775"></AlternatingRowStyle>
                        <Columns>
                            <asp:TemplateField HeaderText="Date" SortExpression="Date">
                                <HeaderStyle Width="80px" HorizontalAlign="Left" />
                                <ItemStyle HorizontalAlign="Center" Width="100px" />
                                <ItemTemplate>
                                    <asp:HyperLink ID="hyDate" runat="server" Text='<%#  Eval("Date","{0:dd/MM/yyyy}") %>'
                                        NavigateUrl='<%# EncryptQuery("~/ModuleUI/Appointment/Booking/APPBok_BookingOverview.aspx?frompage=recdash&SiteID="+Eval("SiteID") +"&Scheduledate=" + Eval("Date", "{0:dd/MM/yyyy}")) %>' Target="_blank"></asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Bookings" DataField="Bookings">
                                <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                <ItemStyle HorizontalAlign="Center" Width="150px" />
                            </asp:BoundField>
                            <asp:BoundField HeaderText="Pos" DataField="Pos">
                                <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                <ItemStyle HorizontalAlign="Center" Width="150px" />
                            </asp:BoundField>

                            <asp:TemplateField HeaderText="Pallets" SortExpression="Date">
                                <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                <ItemStyle HorizontalAlign="Center" Width="100px" />
                                <ItemTemplate>
                                    <asp:Label ID="lblPallets" runat="server"
                                        Text='<%#  Eval("Pallets") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:BoundField HeaderText="Cartons" DataField="Cartons">
                                <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                <ItemStyle HorizontalAlign="Center" Width="150px" />
                            </asp:BoundField>

                            <asp:BoundField HeaderText="Lifts" DataField="Lifts">
                                <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                <ItemStyle HorizontalAlign="Center" Width="150px" />
                            </asp:BoundField>

                            <asp:TemplateField HeaderText="Lines" SortExpression="Date">
                                <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                <ItemStyle HorizontalAlign="Center" Width="100px" />
                                <ItemTemplate>
                                    <asp:Label ID="lblLines" runat="server"
                                        Text='<%#  Eval("Lines") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Bookings With Issues">
                                <HeaderStyle Width="80px" HorizontalAlign="Left" />
                                <ItemStyle HorizontalAlign="Center" Width="100px" />
                                <ItemTemplate>
                                    <asp:HyperLink ID="htBookingsWithIssues" runat="server" Text='<%#  Eval("BookingsWithIssues") %>'
                                        Enabled='<%#  IsTransType(Eval("BookingsWithIssues")) %> '
                                        ForeColor='<%#  IsTransColrType(Eval("BookingsWithIssues")) %>'
                                        NavigateUrl='<%# EncryptQuery("~/ModuleUI/Appointment/Booking/APPBok_BookingOverview.aspx?frompage=recdash&BookingsWithIssues=BookingsWithIssues&SiteID="+Eval("SiteID") +"&Scheduledate=" + Eval("Date", "{0:dd/MM/yyyy}")) %>' Target="_blank"></asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Open Discrepancies">
                                <HeaderStyle Width="80px" HorizontalAlign="Left" />
                                <ItemStyle HorizontalAlign="Center" Width="100px" />
                                <ItemTemplate>
                                    <cc1:ucLabel ID="OpenDisc" Text='<%#  Eval("OpenDiscrepancies") %>' runat="server"></cc1:ucLabel>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Discrepancies Created">
                                <HeaderStyle Width="80px" HorizontalAlign="Left" />
                                <ItemStyle HorizontalAlign="Center" Width="100px" />
                                <ItemTemplate>
                                    <asp:HyperLink ID="htDiscrepanciesCreated" runat="server" Text='<%#  Eval("TotalDiscrepancies") %>'
                                        Enabled='<%#  IsTransType(Eval("TotalDiscrepancies")) %>'
                                        ForeColor='<%#  IsTransColrType(Eval("TotalDiscrepancies")) %>'
                                        NavigateUrl='<%# EncryptQuery("~/ModuleUI/Discrepancy/DIS_SearchResult.aspx?FromPage=recdash&SiteID="+Eval("SiteID") +"&DisDate=" + Eval("Date", "{0:dd/MM/yyyy}")) %>' Target="_blank"></asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </cc1:ucGridView>
                </td>
            </tr>
        </table>
    </div>

    <br />
    <br />

    <div class="loader" id="AjaxLoader" style="display: none;">
        <div class="strip-holder">
            <div class="strip-1"></div>
            <div class="strip-2"></div>
            <div class="strip-3"></div>
        </div>
    </div>

    <div id="showButtons" runat="server">
        <table style="width: 100%; text-align: center">
            <tr>
                <td style="width: 20%; text-align: center">
                    <input type="button" id="btn1" value="Monday" />
                </td>
                <td style="width: 20%; text-align: center">
                    <input type="button" id="btn2" value="Tuesday" />
                </td>
                <td style="width: 20%; text-align: center">
                    <input type="button" id="btn3" value="Wednesday" />
                </td>
                <td style="width: 20%; text-align: center">
                    <input type="button" id="btn4" value="Thursday" />
                </td>
                <td style="width: 20%; text-align: center">
                    <input type="button" id="btn5" value="Friday" />
                </td>
            </tr>
        </table>
    </div>

    <br />
    <br />

    <cc1:ucPanel ID="pnl" runat="server" GroupingText="No graph" CssClass="fieldset-form">

        <div id="showGraph" runat="server">
            <div class="main-container">
                <div class="graph-container">
                    <div class="graph-row">
                        <div style="float: left">
                            <div id='Pallet_div' class="heading1">
                            </div>
                        </div>
                        <div style="float: right">
                            <div id='line_div' class="heading2">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="graphData" style="display: none;">
            <div class="strip-holder">
                <div style="font-weight: bold"><%= strGraphData %></div>
            </div>
        </div>

    </cc1:ucPanel>

    <asp:HiddenField ID="hdnJSFromDt" runat="server" />

    <script type='text/javascript' src="../../../Scripts/jsapi.js"></script>
    <script src="../../../Scripts/json2.js" type="text/javascript"></script>

    <script type="text/javascript">
        google.load("visualization", "1", { packages: ["corechart"] });
        google.charts.load('current', { packages: ['bar', 'corechart', 'table'] });
        google.charts.setOnLoadCallback(drawPallet);
        google.charts.setOnLoadCallback(drawLine);

        $('#txtDate').datepicker({
            showOtherMonths: true,
            selectOtherMonths: true, changeMonth: true, changeYear: true,
            minDate: new Date(2013, 0, 1),
            yearRange: '2013:+100',
            dateFormat: 'dd/mm/yy',
            onClose: function () { $('<%=hdnJSFromDt.ClientID %>').attr('value', $('#txtDate').datepicker({ dateFormat: 'dd/mm/yy' })) }
        });

        var dataValues = [];

        function DrawScheduling() {
            drawPallet();
            drawLine();
        }

        var weekday = new Array(7);
        weekday[0] = "Sunday";
        weekday[1] = "Monday";
        weekday[2] = "Tuesday";
        weekday[3] = "Wednesday";
        weekday[4] = "Thursday";
        weekday[5] = "Friday";
        weekday[6] = "Saturday";

        function drawPallet() {
            debugger;
            var obj = dataValues;
            if (obj.length > 0) {
                var scorePallet = [];
                var optionsPallets = {
                    title: 'Scheduled Pallets Per Hour',
                    width: 475,
                    height: 300,
                    bar: { groupWidth: "95%" },
                    legend: { position: "bottom" },
                    isStacked: true,
                };
                $.each(obj, function (index, element) {
                    scorePallet.push([this.ID.toString(), this.ArrivedPallets, this.NotArrivedPallets]);
                });
                var dataPallet = new google.visualization.DataTable(scorePallet);
                dataPallet.addColumn('string', 'Hour');
                dataPallet.addColumn('number', 'Arrived');
                dataPallet.addColumn('number', 'Not Arrived');

                dataPallet.addRows(JSON.parse(JSON.stringify(scorePallet)));
                var chartPallet = new google.visualization.ColumnChart(document.getElementById('Pallet_div'));
                chartPallet.draw(dataPallet, optionsPallets);
            }
        }

        function drawLine() {
            var obj = dataValues;

            if (obj.length > 0) {
                var scoreLine = [];
                var optionsLine = {
                    title: 'Scheduled Lines Per Hour',
                    width: 475,
                    height: 300,
                    bar: { groupWidth: "95%" },
                    legend: { position: "bottom" },
                    isStacked: true,
                };

                $.each(obj, function (index, element) {
                    scoreLine.push([this.ID.toString(), this.ArrivedLines, this.NotArrivedLines]);
                });

                var dataLine = new google.visualization.DataTable(scoreLine);
                dataLine.addColumn('string', 'Hour');
                dataLine.addColumn('number', 'Arrived');
                dataLine.addColumn('number', 'Not Arrived');
                dataLine.addRows(JSON.parse(JSON.stringify(scoreLine)));
                var chartLine = new google.visualization.ColumnChart(document.getElementById('line_div'));
                chartLine.draw(dataLine, optionsLine);
            }
        }

        $(function () {
            var SiteID = $('[id$=ddlSite]').val();
            var selectedDate1 = '<%= selectedDate %>';
            var selectedDate = selectedDate1.split(' ')[0].split('/');
            var obj = {
                date: selectedDate[2] + '-' + selectedDate[1] + '-' + selectedDate[0],
                SiteID: SiteID
            };
            var json = Sys.Serialization.JavaScriptSerializer.serialize(obj);
            $.ajax({
                type: "post",
                url: "../../AjaxMethodCall.aspx/BindOneDashboardData",
                data: json,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    debugger;
                    LoadData(response.d);
                },
                error: function (response) {
                    console.log(response.responseText);
                }
            });
        });

        function btnSearchClient() {
            var SiteID = $('[id$=ddlSite]').val();
            var selectedDate1 = $('#txtDate').val();
            var selectedDate = selectedDate1.split('/');
            var obj = {
                date: selectedDate[2] + '-' + selectedDate[1] + '-' + selectedDate[0],
                SiteID: SiteID
            };
            var json = Sys.Serialization.JavaScriptSerializer.serialize(obj);
            $.ajax({
                type: "post",
                url: "../../AjaxMethodCall.aspx/BindOneDashboardData",
                data: json,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    LoadData(response.d);
                },
                error: function (response) {
                    console.log(response.responseText);
                }
            });
            document.getElementById('<%=hdnJSFromDt.ClientID %>').value = selectedDate1;
            return true;
        }

        function LoadData(response) {
            debugger;
            var pallet1 = parseInt($('[id$=ctl02_hyDate]').closest("tr").find("[id*='lblPallets']").text());
            var pallet2 = parseInt($('[id$=ctl03_hyDate]').closest("tr").find("[id*='lblPallets']").text());
            var pallet3 = parseInt($('[id$=ctl04_hyDate]').closest("tr").find("[id*='lblPallets']").text());
            var pallet4 = parseInt($('[id$=ctl05_hyDate]').closest("tr").find("[id*='lblPallets']").text());
            var pallet5 = parseInt($('[id$=ctl06_hyDate]').closest("tr").find("[id*='lblPallets']").text());

            var line1 = parseInt($('[id$=ctl02_hyDate]').closest("tr").find("[id*='lblLines']").text());
            var line2 = parseInt($('[id$=ctl03_hyDate]').closest("tr").find("[id*='lblLines']").text());
            var line3 = parseInt($('[id$=ctl04_hyDate]').closest("tr").find("[id*='lblLines']").text());
            var line4 = parseInt($('[id$=ctl05_hyDate]').closest("tr").find("[id*='lblLines']").text());
            var line5 = parseInt($('[id$=ctl06_hyDate]').closest("tr").find("[id*='lblLines']").text());

            if (response.length > 0) {
                $(document).ajaxStart(function () {
                    $('#AjaxLoader').show();
                });

                dataValues = response;

                let d = response[0].DateTimes.find(x => x.IsActive == true).ID;

                if (pallet1 || line1) {
                    $('[id$=btn1]').addClass("button");
                    if ($('[id$=btn1]')[0].hasAttribute('disabled'))
                        $('[id$=btn1]').removeAttr('disabled');
                    $('[id$=btn1]').removeClass("button-readonly");
                }
                else {
                    $('[id$=btn1]').attr('disabled', 'disabled');
                    $('[id$=btn1]').addClass("button-readonly");
                }

                if (pallet2 || line2) {
                    $('[id$=btn2]').addClass("button");
                    if ($('[id$=btn2]')[0].hasAttribute('disabled'))
                        $('[id$=btn2]').removeAttr('disabled');
                    $('[id$=btn2]').removeClass("button-readonly");
                }
                else {
                    $('[id$=btn2]').attr('disabled', 'disabled');
                    $('[id$=btn2]').addClass("button-readonly");
                }
                if (pallet3 || line3) {
                    $('[id$=btn3]').addClass("button");
                    if ($('[id$=btn3]')[0].hasAttribute('disabled'))
                        $('[id$=btn3]').removeAttr('disabled');
                    $('[id$=btn3]').removeClass("button-readonly");
                }
                else {
                    $('[id$=btn3]').attr('disabled', 'disabled');
                    $('[id$=btn3]').addClass("button-readonly");
                }
                if (pallet4 || line4) {
                    $('[id$=btn4]').addClass("button");
                    if ($('[id$=btn4]')[0].hasAttribute('disabled'))
                        $('[id$=btn4]').removeAttr('disabled');
                    $('[id$=btn4]').removeClass("button-readonly");
                }
                else {
                    $('[id$=btn4]').attr('disabled', 'disabled');
                    $('[id$=btn4]').addClass("button-readonly");
                }
                if (pallet5 || line5) {
                    $('[id$=btn5]').addClass("button");
                    if ($('[id$=btn5]')[0].hasAttribute('disabled'))
                        $('[id$=btn5]').removeAttr('disabled');
                    $('[id$=btn5]').removeClass("button-readonly");
                }
                else {
                    $('[id$=btn5]').attr('disabled', 'disabled');
                    $('[id$=btn5]').addClass("button-readonly");
                }

                if ($('[id$=btn1]').val() == weekday[d]) {
                    $('[id$=btn1]').addClass("button-readonly");
                    $('[id$=btn1]').attr("disabled", "disabled");
                }

                if ($('[id$=btn2]').val() == weekday[d]) {
                    $('[id$=btn2]').addClass("button-readonly");
                    $('[id$=btn2]').attr("disabled", "disabled");
                }

                if ($('[id$=btn3]').val() == weekday[d]) {
                    $('[id$=btn3]').addClass("button-readonly");
                    $('[id$=btn3]').attr("disabled", "disabled");
                }

                if ($('[id$=btn4]').val() == weekday[d]) {
                    $('[id$=btn4]').addClass("button-readonly");
                    $('[id$=btn4]').attr("disabled", "disabled");
                }

                if ($('[id$=btn5]').val() == weekday[d]) {
                    $('[id$=btn5]').addClass("button-readonly");
                    $('[id$=btn5]').attr("disabled", "disabled");
                }


                $('[id$=pnl] fieldset legend').text("Displaying graph for " + weekday[d]);

                DrawScheduling();

                $('[id$="showGraph"]').show();

                $(document).ajaxStop(function () {
                    $('#AjaxLoader').hide();
                });

                $('[id$="graphData"]').hide();
            }
            else {
                $('[id$="showGraph"]').hide(); 
                $('[id$="graphData"]').show();

                $('[id$=pnl] fieldset legend').text("No graph ");

                $('[id$=btn1]').addClass("button-readonly");
                $('[id$=btn2]').addClass("button-readonly");
                $('[id$=btn3]').addClass("button-readonly");
                $('[id$=btn4]').addClass("button-readonly");
                $('[id$=btn5]').addClass("button-readonly");

                if (pallet1 || line1) {
                    $('[id$=btn1]').addClass("button");
                    if ($('[id$=btn1]')[0].hasAttribute('disabled'))
                        $('[id$=btn1]').removeAttr('disabled');
                    $('[id$=btn1]').removeClass("button-readonly");
                }
                else {
                    $('[id$=btn1]').attr('disabled', 'disabled');
                    $('[id$=btn1]').addClass("button-readonly");
                }

                if (pallet2 || line2) {
                    $('[id$=btn2]').addClass("button");
                    if ($('[id$=btn2]')[0].hasAttribute('disabled'))
                        $('[id$=btn2]').removeAttr('disabled');
                    $('[id$=btn2]').removeClass("button-readonly");
                }
                else {
                    $('[id$=btn2]').attr('disabled', 'disabled');
                    $('[id$=btn2]').addClass("button-readonly");
                }
                if (pallet3 || line3) {
                    $('[id$=btn3]').addClass("button");
                    if ($('[id$=btn3]')[0].hasAttribute('disabled'))
                        $('[id$=btn3]').removeAttr('disabled');
                    $('[id$=btn3]').removeClass("button-readonly");
                }
                else {
                    $('[id$=btn3]').attr('disabled', 'disabled');
                    $('[id$=btn3]').addClass("button-readonly");
                }
                if (pallet4 || line4) {
                    $('[id$=btn4]').addClass("button");
                    if ($('[id$=btn4]')[0].hasAttribute('disabled'))
                        $('[id$=btn4]').removeAttr('disabled');
                    $('[id$=btn4]').removeClass("button-readonly");
                }
                else {
                    $('[id$=btn4]').attr('disabled', 'disabled');
                    $('[id$=btn4]').addClass("button-readonly");
                }
                if (pallet5 || line5) {
                    $('[id$=btn5]').addClass("button");
                    if ($('[id$=btn5]')[0].hasAttribute('disabled'))
                        $('[id$=btn5]').removeAttr('disabled');
                    $('[id$=btn5]').removeClass("button-readonly");
                }
                else {
                    $('[id$=btn5]').attr('disabled', 'disabled');
                    $('[id$=btn5]').addClass("button-readonly");
                }
            }
        }

        $('[id$=btn1]').click(function () {
            var SiteID = $('[id$=ddlSite]').val();
            var selectedDate = $('[id$=ctl02_hyDate]').text().split('/');
            var obj = {
                date: selectedDate[2] + '-' + selectedDate[1] + '-' + selectedDate[0],
                SiteID: SiteID
            };
            var json = Sys.Serialization.JavaScriptSerializer.serialize(obj);
            $.ajax({
                type: "post",
                url: "../../AjaxMethodCall.aspx/BindOneDashboardData",
                data: json,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    LoadData(response.d);
                },
                error: function (response) {
                    console.log(response.responseText);
                }
            });

        });

        $('[id$=btn2]').click(function () {
            var SiteID = $('[id$=ddlSite]').val();
            var selectedDate = $('[id$=ctl03_hyDate]').text().split('/');
            var obj = {
                date: selectedDate[2] + '-' + selectedDate[1] + '-' + selectedDate[0],
                SiteID: SiteID
            };
            var json = Sys.Serialization.JavaScriptSerializer.serialize(obj);
            $.ajax({
                type: "post",
                url: "../../AjaxMethodCall.aspx/BindOneDashboardData",
                data: json,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    LoadData(response.d);
                },
                error: function (response) {
                    console.log(response.responseText);
                }
            });
        });

        $('[id$=btn3]').click(function () {
            var SiteID = $('[id$=ddlSite]').val();
            var selectedDate = $('[id$=ctl04_hyDate]').text().split('/');
            var obj = {
                date: selectedDate[2] + '-' + selectedDate[1] + '-' + selectedDate[0],
                SiteID: SiteID
            };
            var json = Sys.Serialization.JavaScriptSerializer.serialize(obj);
            $.ajax({
                type: "post",
                url: "../../AjaxMethodCall.aspx/BindOneDashboardData",
                data: json,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    LoadData(response.d);
                },
                error: function (response) {
                    console.log(response.responseText);
                }
            });
        });

        $('[id$=btn4]').click(function () {
            var SiteID = $('[id$=ddlSite]').val();
            var selectedDate = $('[id$=ctl05_hyDate]').text().split('/');
            var obj = {
                date: selectedDate[2] + '-' + selectedDate[1] + '-' + selectedDate[0],
                SiteID: SiteID
            };
            var json = Sys.Serialization.JavaScriptSerializer.serialize(obj);
            $.ajax({
                type: "post",
                url: "../../AjaxMethodCall.aspx/BindOneDashboardData",
                data: json,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    LoadData(response.d);
                },
                error: function (response) {
                    console.log(response.responseText);
                }
            });
        });

        $('[id$=btn5]').click(function () {
            var SiteID = $('[id$=ddlSite]').val();
            var selectedDate = $('[id$=ctl06_hyDate]').text().split('/');
            var obj = {
                date: selectedDate[2] + '-' + selectedDate[1] + '-' + selectedDate[0],
                SiteID: SiteID
            };
            var json = Sys.Serialization.JavaScriptSerializer.serialize(obj);
            $.ajax({
                type: "post",
                url: "../../AjaxMethodCall.aspx/BindOneDashboardData",
                data: json,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    LoadData(response.d);
                },
                error: function (response) {
                    console.log(response.responseText);
                }
            });
        });

        function setValue1(target) {
            document.getElementById('<%=hdnJSFromDt.ClientID %>').value = target.value;
        }

    </script>

    <style type="text/css">
        .heading1 {
            width: 100%;
            height: 300px;
            padding-top: 12px;
            text-align: center;
            font-family: Arial;
            font-size: 16px;
            color: #636363;
            font-weight: bold;
            border-style: solid;
        }

        .heading2 {
            width: 100%;
            height: 300px;
            padding-top: 12px;
            text-align: center;
            font-family: Arial;
            font-size: 16px;
            color: #636363;
            font-weight: bold;
            border-style: solid;
        }
    </style>

    <style type="text/css">
        /*!  
        // 3. Loader  
        // --------------------------------------------------*/
        .loader {
            top: 0;
            left: 0;
            position: fixed;
            opacity: 0.8;
            z-index: 10000000;
            background: Black;
            height: 100%;
            width: 100%;
            margin: auto;
        }

        .strip-holder {
            top: 50%;
            -webkit-transform: translateY(-50%);
            -ms-transform: translateY(-50%);
            transform: translateY(-50%);
            left: 50%;
            margin-left: -50px;
            position: relative;
        }

        .strip-1,
        .strip-2,
        .strip-3 {
            width: 20px;
            height: 20px;
            background: #0072bc;
            position: relative;
            -webkit-animation: stripMove 2s ease infinite alternate;
            animation: stripMove 2s ease infinite alternate;
            -moz-animation: stripMove 2s ease infinite alternate;
        }

        .strip-2 {
            -webkit-animation-duration: 2.1s;
            animation-duration: 2.1s;
            background-color: #23a8ff;
        }

        .strip-3 {
            -webkit-animation-duration: 2.2s;
            animation-duration: 2.2s;
            background-color: #89d1ff;
        }

        @@-webkit-keyframes stripMove {
            0% {
                transform: translate3d(0px, 0px, 0px);
                -webkit-transform: translate3d(0px, 0px, 0px);
                -moz-transform: translate3d(0px, 0px, 0px);
            }

            50% {
                transform: translate3d(0px, 0px, 0px);
                -webkit-transform: translate3d(0px, 0px, 0px);
                -moz-transform: translate3d(0px, 0px, 0px);
                transform: scale(4, 1);
                -webkit-transform: scale(4, 1);
                -moz-transform: scale(4, 1);
            }

            100% {
                transform: translate3d(-50px, 0px, 0px);
                -webkit-transform: translate3d(-50px, 0px, 0px);
                -moz-transform: translate3d(-50px, 0px, 0px);
            }
        }

        @@-moz-keyframes stripMove {
            0% {
                transform: translate3d(-50px, 0px, 0px);
                -webkit-transform: translate3d(-50px, 0px, 0px);
                -moz-transform: translate3d(-50px, 0px, 0px);
            }

            50% {
                transform: translate3d(0px, 0px, 0px);
                -webkit-transform: translate3d(0px, 0px, 0px);
                -moz-transform: translate3d(0px, 0px, 0px);
                transform: scale(4, 1);
                -webkit-transform: scale(4, 1);
                -moz-transform: scale(4, 1);
            }

            100% {
                transform: translate3d(50px, 0px, 0px);
                -webkit-transform: translate3d(50px, 0px, 0px);
                -moz-transform: translate3d(50px, 0px, 0px);
            }
        }

        @@keyframes stripMove {
            0% {
                transform: translate3d(-50px, 0px, 0px);
                -webkit-transform: translate3d(-50px, 0px, 0px);
                -moz-transform: translate3d(-50px, 0px, 0px);
            }

            50% {
                transform: translate3d(0px, 0px, 0px);
                -webkit-transform: translate3d(0px, 0px, 0px);
                -moz-transform: translate3d(0px, 0px, 0px);
                transform: scale(4, 1);
                -webkit-transform: scale(4, 1);
                -moz-transform: scale(4, 1);
            }

            100% {
                transform: translate3d(50px, 0px, 0px);
                -webkit-transform: translate3d(50px, 0px, 0px);
                -moz-transform: translate3d(50px, 0px, 0px);
            }
        }

        .graph-container {
            overflow: hidden;
            padding: 0px;
        }

        .graph-row {
            overflow: hidden;
            padding: 4px;
            padding-right: -1px;
        }

        .showData {
            height: 20px;
            border: 1px solid #dfc09c;
            color: #934500;
            text-shadow: 1px 1px 1px #ffffff;
            font-weight: bold;
            font-size: 15px;
            padding: 2px 7px 2px 7px;
            *padding: 5px 7px 5px 7px;
            padding-top /*\**/: 2px\9;
            font-family: Arial, Helvetica, sans-serif;
        }
    </style>

</asp:Content>

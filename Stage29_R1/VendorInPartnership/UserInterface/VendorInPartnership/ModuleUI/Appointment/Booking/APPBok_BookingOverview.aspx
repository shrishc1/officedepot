﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    CodeFile="APPBok_BookingOverview.aspx.cs" Inherits="APPBok_BookingOverview" MaintainScrollPositionOnPostback="true" %>

<%@ Register TagName="ucDate" TagPrefix="cc2" Src="~/CommonUI/UserControls/ucSchedulingDate.ascx" %>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/ucSite.ascx" TagName="ucSite" TagPrefix="cc2" %>
<%@ Register Src="~/CommonUI/UserControls/ucSeacrhVendor.ascx" TagName="ucSeacrhVendor"
    TagPrefix="cc3" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<%@ Register TagName="ucExportButton" TagPrefix="uc1" Src="~/CommonUI/UserControls/ucExportToExcel.ascx" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <style type="text/css">
        #content .gvclass th {
            text-align: left !important;
        }

        .greencell td {
            color: #339966;
            font-weight: bold;
        }

        .whiteBox, .purpleBox, .grayBox, .greenYellow, .LightYellow, .blackBox, .orangeBox, .pinkBox, .greenBox, .redBox, .blueBox {
            border: 1px solid #333;
            background: #fff;
            width: 80px;
            height: 25px;
            float: left;
        }

        .purpleBox {
            background: purple;
            border: 2px dashed black;
        }

        .grayBox {
            background: grey;
        }

        .greenYellow {
            background: GreenYellow;
        }

        .LightYellow {
            background: LightYellow;
            border: 2px solid red;
        }

        .blackBox {
            background: black;
        }

        .orangeBox {
            background: orange;
        }

        .pinkBox {
            background: #FFB6C1;
        }

        .redBox {
            background: #f00;
        }

        .greenBox {
            background: #00b050;
        }

        .blueBox {
            background: #002060;
        }

        .spanLeft span {
            float: left;
            margin-top: 6px;
            margin-right: 5px;
            width: 110px;
            text-align: right;
        }
       
    </style>

    <style type="text/css">
        .fixed_headers {
            width: 100%;
            table-layout: fixed;
            border-collapse: collapse;
        }

        .fixed_headers .tableCol3 {
            width: 245px !important;
        }

        .fixed_headers .tableCol3 {
            width: 402px !important;
        }

        .fixed_headers .tableCol3 {
            width: 559px !important;
        }

        .fixed_headers .tableCol4 {
            width: 716px !important;
        }

        .fixed_headers .tableCol5 {
        }

        .fixed_headers .timeCol {
            border: none;
            width: 50px;
            text-align: center;
            vertical-align: top;
        }

        .fixed_headers thead.test tr {
            display: block;
        }

        .fixed_headers tbody.test2 {
            display: block;
            overflow: auto;
            width: 100%;
            height: 400px;
        }

        .container {
            max-width: 100%;
            clear: both;
            float: left;
            margin-top: 30px;
        }

        .tableData {
            border-collapse: collapse;
            table-layout: fixed;
        }

        .tableData thead th {
            background: #bfbfbf;
            height: 40px;
        }

        .tableData .timeCol {
            border: none;
            width: 50px;
            text-align: center;
            vertical-align: middle;
        }

        .tableData th.timeCol {
            vertical-align: middle;
            line-height: 30px;
        }

        .tableData td {
            height: 40px;
            font-weight: bold;
        }

        .tableData tbody .timeCol {
            background: #bfbfbf;
            color: #000;
        }

        .tableData td {
            overflow: hidden;
            white-space: nowrap;
        }

        .tableData span {
            overflow: hidden;
            display: block;
            max-width: 98%;
            white-space: nowrap;
        }

        .tableData td.borderRightMain, .tableData th.borderRightMain {
            border-right: 1px solid #fff;
        }

        .greenTd {
            background: #00b050;
            cursor: pointer;
        }

        .blueTd {
            background: #002060;
        }

        .whiteTd {
            background: #fff;
        }

        .redTd {
            background: #f00;
            cursor: pointer;
        }

        .orangeTd {
            background: #FFA500;
            cursor: pointer;
            vertical-align: top;
            line-height: 17px;
        }

        .pinkTd {
            background: pink;
            cursor: pointer;
        }

        .greyTd {
            background: grey;
        }

        .greenYellowTd {
            background: #adff2f;
        }

        .shadow {
            -moz-box-shadow: inset 0 0 10px blue;
            -webkit-box-shadow: inset 0 0 10px blue;
            -webkit-box-shadow: inset 0 0 10px blue;
        }

        .shadow1 {
            -moz-box-shadow: inset 0 0 20px black;
            -webkit-box-shadow: inset 0 0 20px black;
            -webkit-box-shadow: inset 0 0 20px black;
        }

        .tableWidth {
            width: 990px
        }
    </style>

    <style type="text/css">
            .outerTable {
                position: relative;
                width: 1000px;
                overflow: hidden;
                border-collapse: collapse;
            }
            /*thead*/
            .outerTable thead {
                position: relative;
                display: block; /*seperates the header from the body allowing it to be positioned*/
                overflow: visible;
            }

            .outerTable thead th {
                min-width: 170px;
                height: 32px;
            }

            .outerTable thead th:nth-child(1) { /*first cell in the header*/
                position: relative;
                display: block; /*seperates the first cell in the header from the header*/
                min-width: 60px;
            }

            /*tbody*/
            .outerTable tbody {
                position: relative;
                display: block; /*seperates the tbody from the header*/
                height: 525px;
                overflow: scroll;
            }

            .outerTable tbody td {
                min-width: 170px;
                max-width: 170px;
            }

            .outerTable tbody tr td:nth-child(1) { /*the first cell in each tr*/
                position: relative;
                display: block; /*seperates the first column from the tbody*/
                height: 40px;
                z-index: 99;
                line-height: 17px;
                min-width: 60px;
            }

            .innerTable {
                z-index: 11;
                position: relative;
            }

            .innerTable tbody {
                position: inherit;
                display: inherit;
                overflow: inherit;
                height: 40px;
            }

            .innerTable tbody tr td:nth-child(1) {
                left: 0px !important;
            }

            .spetialtable {
            width: 170px;
            }

       .spetialtable .td1 {
        /*width: calc(100% / 1) !important;*/
        min-width: calc(100% /1) !important;
        max-width: 170px !important;
        }
       .spetialtable .td2 {
        /*width: calc(100% / 2) !important;*/
        min-width: calc(100% /2) !important;
        max-width: 170px !important;
        }
       .spetialtable .td3 {
        /*width: calc(100% / 3) !important;*/
        min-width: calc(100% /3) !important;
        max-width: 170px !important;
        }
       .spetialtable .td4 {
        /*width: calc(100% / 3) !important;*/
        min-width: calc(100% /4) !important;
        max-width: 170px !important;
        }
       .width5 {
            width: 37px ;
        }
       .width4 {
            width: 43px ;
        }
        .width3 {
            width: 57px ;
        }
        .width2 {
            width: 85px ; 
        }
        .width1  {
            width: 170px; 
        }

        .width85  {
            width: 170px; 
        }
          .width170  {
            width: 170px; 
        }

        .timeCol.innerTable1 {
            margin: 0px !important;
        }

            .innerTable1 {
                margin: 1px !important;
            }

            .outerTable1 {
                position: relative;
                overflow: hidden;
                border-collapse: collapse;
            }
            .outerTable1 thead {
                position: relative;
                display: block; /*seperates the header from the body allowing it to be positioned*/
                overflow: visible;
            }

            .outerTable1 thead th {
            }

            .outerTable1 thead th:nth-child(1) { /*first cell in the header*/
                position: relative;
                display: block;
            }

            .outerTable1 tbody {
                position: relative;
                display: block;
            }

            .outerTable1 tbody td {
            }

            .outerTable1 tbody tr td:nth-child(1) { /*the first cell in each tr*/
                position: relative;
                display: block;
                z-index: 99;
                line-height: 17px;
            }
    </style>

    <!----start of window slot--->
    <style type="text/css">
        .fixed_headers {
            width: 100%;
            table-layout: fixed;
        }

            .fixed_headers .timeCol {
                border: none;
                width: 50px;
                text-align: center;
                vertical-align: top;
            }

            .fixed_headers thead.test tr {
                display: block;
                position: relative;
            }

            .fixed_headers tbody.test2 {
                display: block;
                overflow: auto;
                width: 100%;
                height: 300px;
            }

        .tableData1 {
            width: 100%;
            table-layout: fixed;
            text-align: center;
        }

            .tableData1 thead th {
                background: #bfbfbf;
                height: 30px;
            }

            .tableData1 .timeCol {
                border: none;
                width: 50px;
                text-align: center;
                vertical-align: top;
            }

            .tableData1 th.timeCol {
                vertical-align: middle;
            }

            .tableData1 td {
                height: 100px;
                font-weight: bold;
                padding: 1px 4px 4px 4px;
            }

            .tableData1 tbody .timeCol {
                background: #bfbfbf;
                color: #000;
            }

            .tableData1 td {
                overflow: hidden;
                white-space: nowrap;
                border-radius: 19px;
            }

            .tableData1 span {
                overflow: hidden;
                display: block;
                max-width: 100%;
                white-space: nowrap;
            }

        .greenTdW {
            background: #00b050;
            cursor: pointer;
            color: white;
        }

        .redTdW {
            background: #f00;
            cursor: pointer;
            color: white;
        }

        .yellowTdW {
            background: #ffff00;
            cursor: pointer;
            color: #e63900;
        }

        .highlightW {
            text-align: right;
            width: 100%;
        }

        .borderRight {
            border-right: 1px solid #fff;
        }

        .minwindowWidth {
            width: 140px;
        }
    </style>

    <script language="javascript" type="text/javascript">


        function EnableDisableButtons(spanChk, type, bookingStatusID, supplierType, WeekDay, LogicalDate, BookingStatus, IsVenodrPalletChecking) {
            var Role = document.getElementById('<%=hdnRole.ClientID %>').value;
            var IsChecked = spanChk.checked;
            // for new alternate view
            if (spanChk.checked == undefined) {
                IsChecked = true;
            }
            var typeOfBooking = type.split('-')[0];
            var idOfBooking = type.split('-')[1];
            if (IsChecked) {
                if (typeOfBooking == 'TW') {
                    if ($('.tableData td').hasClass('shadow1')) {
                        $('.tableData td').removeClass("shadow1");
                    }
                    if ($('input[name$=BookingNo]:checked').length == 0) {
                        $(spanChk).parent().parent().parent().find('td').addClass("shadow1");
                    }
                }

                if (supplierType != '') {
                    document.getElementById('<%=hdnBookingFor.ClientID %>').value = supplierType;
                }
                else {
                    document.getElementById('<%=hdnBookingFor.ClientID %>').value = '';
                }
                document.getElementById('<%=hdnPrimaryKey.ClientID %>').value = type.split('-')[1];
                document.getElementById('<%=hdnRecordTable.ClientID %>').value = type.split('-')[0];
                document.getElementById('<%=hdnBookingWeekDay.ClientID %>').value = WeekDay;
                //

                document.getElementById('<%=btnConfirmAFixedSlot.ClientID %>').disabled = false;
                document.getElementById('<%=btnEditABooking.ClientID %>').disabled = false;
                document.getElementById('<%=btnDeleteABooking.ClientID %>').disabled = false;
                document.getElementById('<%=btnQualityChecked.ClientID %>').disabled = false;
                document.getElementById('<%=btnDelArrived.ClientID %>').disabled = false;
                document.getElementById('<%=btnDelivaryUnload.ClientID %>').disabled = false;
                document.getElementById('<%=btnBookingHistory.ClientID %>').disabled = false;
                document.getElementById('<%=btnConfirmPOArrival.ClientID %>').disabled = true;

                document.getElementById('<%=hdnLogicalDate.ClientID %>').value = LogicalDate;
                var varNoShow = document.getElementById('<%=btnNoShow.ClientID %>');

                // Logic to set disable the edit button, if back date booking trying to edit..
                var currentDate = new Date($("#<%=hdnCurrentDate.ClientID %>").val());
                var bookingDate = new Date($("#<%=hdnSelectedDate.ClientID %>").val());
                if (bookingDate < currentDate) {
                    $("#<%=btnEditABooking.ClientID %>").attr("disabled", "disabled");
                }
                else {
                    $("#<%=btnEditABooking.ClientID %>").removeAttr("disabled");
                }

                if (typeOfBooking == 'BK') {
                    document.getElementById('<%=btnBookingHistory.ClientID %>').disabled = false;
                    switch (bookingStatusID) {
                        case '1':
                            document.getElementById('<%=btnConfirmAFixedSlot.ClientID %>').disabled = true;
                            document.getElementById('<%=btnQualityChecked.ClientID %>').disabled = true;
                            if (Role == 'vendor' || Role == 'carrier') {
                                document.getElementById('<%=btnDeleteABooking.ClientID %>').disabled = true;
                                document.getElementById('<%=btnDelArrived.ClientID %>').disabled = true;
                                document.getElementById('<%=btnDelivaryUnload.ClientID %>').disabled = true;
                            }
                            if (varNoShow != null) {
                                varNoShow.disabled = false;
                            }
                            break;
                        case '2':
                            document.getElementById('<%=btnConfirmAFixedSlot.ClientID %>').disabled = true;
                            document.getElementById('<%=btnEditABooking.ClientID %>').disabled = true;
                            document.getElementById('<%=btnDeleteABooking.ClientID %>').disabled = true;
                            document.getElementById('<%=btnQualityChecked.ClientID %>').disabled = true;
                            //change for 27R1 for the undo arrived delivery.
                            document.getElementById('<%=btnDelArrived.ClientID %>').disabled = false;
                            document.getElementById('<%=btnDelArrived.ClientID %>').value = 'Undo Delivery Arrived';
                            document.getElementById('<%=hdnDliveryArrived.ClientID %>').value = 2;
                            if (Role == 'vendor' || Role == 'carrier') {
                                document.getElementById('<%=btnDelivaryUnload.ClientID %>').disabled = true;
                            }
                            if (varNoShow != null) {
                                varNoShow.disabled = true;
                            }
                            break;
                        case '3':
                            document.getElementById('<%=btnConfirmAFixedSlot.ClientID %>').disabled = true;
                            document.getElementById('<%=btnEditABooking.ClientID %>').disabled = true;
                            document.getElementById('<%=btnDeleteABooking.ClientID %>').disabled = true;
                            document.getElementById('<%=btnDelArrived.ClientID %>').disabled = true;
                            //change for undo delivery unload
                            document.getElementById('<%=btnDelivaryUnload.ClientID %>').disabled = false;
                            document.getElementById('<%=btnDelivaryUnload.ClientID %>').value = IsVenodrPalletChecking == 'True' ? 'Delivery Unload' : 'Undo Delivery Unload';
                            document.getElementById('<%=hdnDliveryUnload.ClientID %>').value = 2;
                            document.getElementById('<%=btnDelivaryUnload.ClientID %>').disabled = IsVenodrPalletChecking == 'True' ? true : false;
                            if (Role == 'vendor' || Role == 'carrier') {
                                document.getElementById('<%=btnQualityChecked.ClientID %>').disabled = true;
                            }
                            if (varNoShow != null) {
                                varNoShow.disabled = true;
                            }
                            break;
                        case '4':
                        case '5':
                        case '6':
                            document.getElementById('<%=btnConfirmAFixedSlot.ClientID %>').disabled = true;
                            document.getElementById('<%=btnEditABooking.ClientID %>').disabled = true;
                            document.getElementById('<%=btnDeleteABooking.ClientID %>').disabled = true;

                            if (bookingStatusID != 5) {
                                document.getElementById('<%=btnQualityChecked.ClientID %>').disabled = true;
                            }
                            document.getElementById('<%=btnDelArrived.ClientID %>').disabled = true;
                            document.getElementById('<%=btnDelivaryUnload.ClientID %>').disabled = true;
                            if (varNoShow != null) {
                                varNoShow.disabled = true;
                            }
                            break;
                        case '8':
                            document.getElementById('<%=btnConfirmAFixedSlot.ClientID %>').disabled = true;
                            document.getElementById('<%=btnEditABooking.ClientID %>').disabled = true;
                            document.getElementById('<%=btnDeleteABooking.ClientID %>').disabled = true;

                            if (bookingStatusID != 5) {
                                document.getElementById('<%=btnQualityChecked.ClientID %>').disabled = true;
                            }
                            document.getElementById('<%=btnDelArrived.ClientID %>').disabled = true;
                            document.getElementById('<%=btnDelivaryUnload.ClientID %>').disabled = true;
                            if (varNoShow != null) {
                                varNoShow.disabled = true;
                            }
                            break;
                    }
                }
                else if (typeOfBooking == 'FS' || typeOfBooking == 'TW') {
                    $('#<%=hdnWindowTimeID.ClientID %>').val(idOfBooking);
                    $('#<%=hdnWindowTime.ClientID %>').val($(spanChk).find('[name*="hdnTime"]').val());
                    $('#<%=hdnDoorNumber.ClientID %>').val($(spanChk).find('[name*="hdnDoorNumber"]').val());
                    $('#<%=hdnSiteDoorNumberID.ClientID %>').val($(spanChk).find('[name*="SiteDoorNumberID"]').val());

                    if (supplierType == '') {
                        document.getElementById('<%=btnConfirmAFixedSlot.ClientID %>').disabled = true;
                        document.getElementById('<%=btnDeleteABooking.ClientID %>').disabled = true;
                    }

                    document.getElementById('<%=btnEditABooking.ClientID %>').disabled = true;
                    document.getElementById('<%=btnQualityChecked.ClientID %>').disabled = true;
                    document.getElementById('<%=btnDelArrived.ClientID %>').disabled = true;
                    document.getElementById('<%=btnDelivaryUnload.ClientID %>').disabled = true;
                    document.getElementById('<%=btnBookingHistory.ClientID %>').disabled = true;
                    if (varNoShow != null) {
                        varNoShow.disabled = true;
                    }
                    //logic for disable window
                }

                if (BookingStatus == 'Delivery Arrived' || BookingStatus == 'Delivery Unloaded' || BookingStatus == 'Partially Refused' || BookingStatus == 'Quality Checked') {
                    var varBtnConfirmPOArrival = document.getElementById('<%=btnConfirmPOArrival.ClientID %>');
                    if (varBtnConfirmPOArrival != null) {
                        varBtnConfirmPOArrival.disabled = false;
                    }
                }
                if (BookingStatus == 'Deleted' || BookingStatus == 'Cancelled') {
                    var varBtnAddBooking = document.getElementById('<%=btnAddBooking.ClientID %>');
                    if (varBtnAddBooking != null) {
                        varBtnAddBooking.disabled = false;
                    }

                    var varBtnAddByPOBooking = document.getElementById('<%=btnAddByPOBooking.ClientID %>');
                    if (varBtnAddByPOBooking != null) {
                        varBtnAddByPOBooking.disabled = false;
                    }

                    var varBtnConfirmAFixedSlot = document.getElementById('<%=btnConfirmAFixedSlot.ClientID %>');
                    if (varBtnConfirmAFixedSlot != null) {
                        varBtnConfirmAFixedSlot.disabled = true;
                    }

                    var varEDBBtnEditABooking = document.getElementById('<%=btnEditABooking.ClientID %>');
                    if (varEDBBtnEditABooking != null) {
                        varEDBBtnEditABooking.disabled = true;
                    }

                    var varEDBBtnDeleteABooking = document.getElementById('<%=btnDeleteABooking.ClientID %>');
                    if (varEDBBtnDeleteABooking != null) {
                        varEDBBtnDeleteABooking.disabled = true;
                    }

                    var varEDBBtnUnexpectedDelivery = document.getElementById('<%=btnUnexpectedDelivery.ClientID %>');
                    if (varEDBBtnUnexpectedDelivery != null) {
                        varEDBBtnUnexpectedDelivery.disabled = false;
                    }

                    var varEDBBtnVolumeDetails = document.getElementById('<%=btnVolumeDetails.ClientID %>');
                    if (varEDBBtnVolumeDetails != null) {
                        varEDBBtnVolumeDetails.disabled = false;
                    }

                    var varEDBBtnDelArrived = document.getElementById('<%=btnDelArrived.ClientID %>');
                    if (varEDBBtnDelArrived != null) {
                        varEDBBtnDelArrived.disabled = true;
                    }

                    var varEDBBtnDelivaryUnload = document.getElementById('<%=btnDelivaryUnload.ClientID %>');
                    if (varEDBBtnDelivaryUnload != null) {
                        varEDBBtnDelivaryUnload.disabled = true;
                    }

                    var varEDBBtnNoShow = document.getElementById('<%=btnNoShow.ClientID %>');
                    if (varEDBBtnNoShow != null) {
                        varEDBBtnNoShow.disabled = true;
                    }

                    var varEDBBtnQualityChecked = document.getElementById('<%=btnQualityChecked.ClientID %>');
                    if (varEDBBtnQualityChecked != null) {
                        varEDBBtnQualityChecked.disabled = true;
                    }

                    var varEDBBtnBookingHistory = document.getElementById('<%=btnBookingHistory.ClientID %>');
                    if (varEDBBtnBookingHistory != null) {
                        varEDBBtnBookingHistory.disabled = false;
                    }

                }
            }
            // for new alternate view
            if (spanChk.checked != undefined) {
                var CurrentRdbID = spanChk.id;
                var Chk = spanChk;
                Parent = document.getElementById("<%=ucGridView1.ClientID%>");
                var items = Parent.getElementsByTagName('input');
                for (i = 0; i < items.length; i++) {
                    if (items[i].id != CurrentRdbID && items[i].type == "radio") {
                        if (items[i].checked) {
                            items[i].checked = false;
                        }
                    }
                }
            }

            if (spanChk.className == "whiteTd" || spanChk.className == "blueTd") {
                document.getElementById('<%=btnConfirmAFixedSlot.ClientID %>').disabled = true;
                document.getElementById('<%=btnEditABooking.ClientID %>').disabled = true;
                document.getElementById('<%=btnDeleteABooking.ClientID %>').disabled = true;
                document.getElementById('<%=btnQualityChecked.ClientID %>').disabled = true;
                document.getElementById('<%=btnDelArrived.ClientID %>').disabled = true;
                document.getElementById('<%=btnDelivaryUnload.ClientID %>').disabled = true;
                document.getElementById('<%=btnBookingHistory.ClientID %>').disabled = true;
                document.getElementById('<%=btnConfirmPOArrival.ClientID %>').disabled = true;

            }
            if (typeOfBooking != 'TW') {
                $('.tableData tbody tr td').removeClass("shadow");
                $('[name="' + type + '"]').addClass("shadow");
                var jsonText = JSON.stringify({ name: type });
                if (type != undefined || type != '') {
                    $.ajax({
                        url: '../../AjaxMethodCall.aspx/GetData',
                        type: "post",
                        data: jsonText,
                        dataType: "json",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            if (data.d != undefined || data.d != '') {
                                var obj = data.d;
                                var OtherVehichleType = obj.OtherVehichleType;
                                var Lifts = obj.Lifts;
                                var Pallets = obj.Pallets;
                                var Lines = obj.Lines;
                                var Cartons = obj.Catrons;

                                $('[id$="ltVendor_3"]').html(obj.Vendor);
                                $('[id$="ltCarrier_3"]').html(obj.CarrierName);
                                $('[id$="ltOtherCarrier_3"]').html();
                                $('[id$="ltVehicleType_3"]').html(obj.VehicleType);
                                $('[id$="ltOtherVehicleType_3"]').html(obj.VehicleTypeOther);
                                $('[id$="ltlDoor"]').html(obj.Door);
                                if (Lifts == "0") {
                                    $('[id$="ltExpectedLifts"]').html("-");
                                }
                                else {
                                    $('[id$="ltExpectedLifts"]').html(obj.Lifts);
                                }
                                if (Pallets == "0") {
                                    $('[id$="ltExpectedPallets"]').html(obj.Pallets);
                                }
                                else {
                                    $('[id$="ltExpectedPallets"]').html(obj.Pallets);
                                }
                                if (Lines == "0") {
                                    $('[id$="ltExpectedLines"]').html("-");
                                }
                                else {
                                    $('[id$="ltExpectedLines"]').html(obj.Lines);
                                }
                                if (Cartons == "0") {
                                    $('[id$="ltExpectedCartons"]').html("-");
                                }
                                else {
                                    $('[id$="ltExpectedCartons"]').html(obj.Catrons);
                                }
                                $('[id$="ltStartTime"]').html(obj.StartTime);
                                var valID = $(spanChk)["0"].attributes[1].value.split(';')[1].split('(')[1].split(',')[1].split('w')[1];
                                var cc = $('[id$="hdnEndTimeGenerate' + valID + '"]').val();
                                $('[id$="ltEndTime"]').html(cc);
                                $("[id$='lnkCurrentBookingDetails']").show();
                            }
                            else {
                                $("[id$='lnkCurrentBookingDetails']").hide();
                                $('[id$="ltVendor_3"]').html('-');
                                $('[id$="ltCarrier_3"]').html('-');
                                $('[id$="ltOtherCarrier_3"]').html('-');
                                $('[id$="ltVehicleType_3"]').html('-');
                                $('[id$="ltOtherVehicleType_3"]').html('-');
                                $('[id$="ltlDoor"]').html('-');
                                $('[id$="ltExpectedLifts"]').html('-');
                                $('[id$="ltExpectedPallets"]').html('-');
                                $('[id$="ltExpectedLines"]').html('-');
                                $('[id$="ltExpectedCartons"]').html('-');
                                $('[id$="ltStartTime"]').html('-');
                                $('[id$="ltEndTime"]').html('-');
                            }
                        }
                    });
                }
            }
            setReadonlyClass();
        }
        function EnableDisableButtonsVendor(spanChk, type, bookingStatusID, supplierType, siteid, bookingdate, LogicalDate, BookingStatus) {
            var Role = document.getElementById('<%=hdnRole.ClientID %>').value;
            var IsChecked = spanChk.checked;
            if (IsChecked) {

                document.getElementById('<%=hdnsiteid.ClientID %>').value = siteid;
                document.getElementById('<%=hdnbookingdate.ClientID %>').value = bookingdate;
                document.getElementById('<%=hdnLogicalDate.ClientID %>').value = LogicalDate;

                if (supplierType != '') {
                    document.getElementById('<%=hdnBookingFor.ClientID %>').value = supplierType;
                }
                else {
                    document.getElementById('<%=hdnBookingFor.ClientID %>').value = '';
                }
                document.getElementById('<%=hdnPrimaryKey.ClientID %>').value = type.split('-')[1];
                document.getElementById('<%=hdnRecordTable.ClientID %>').value = type.split('-')[0];

                if (type.split('-')[0] == 'BK') {

                    document.getElementById('<%=btnVendorEditBooking.ClientID %>').disabled = false;
                    document.getElementById('<%=btnDeleteBooking.ClientID %>').disabled = false;
                    document.getElementById('<%=btnConfirmFixedSlot.ClientID %>').disabled = true;
                    document.getElementById('<%=btnDeleteFixedSlot.ClientID %>').disabled = true;
                    document.getElementById('<%=btnBookingHistory_1.ClientID %>').disabled = false;
                    switch (bookingStatusID) {
                        case '2':
                        case '3':
                        case '4':
                        case '5':
                        case '6':
                            document.getElementById('<%=btnVendorEditBooking.ClientID %>').disabled = true;
                            document.getElementById('<%=btnDeleteBooking.ClientID %>').disabled = true;
                            break;
                        case '8':
                            document.getElementById('<%=btnVendorEditBooking.ClientID %>').disabled = true;
                            document.getElementById('<%=btnDeleteBooking.ClientID %>').disabled = true;
                            break;
                    }

                    if ((Role == 'vendor' && supplierType == 'C') || (Role == 'carrier' && supplierType == 'V')) {
                        document.getElementById('<%=btnVendorEditBooking.ClientID %>').disabled = true;
                        document.getElementById('<%=btnDeleteBooking.ClientID %>').disabled = true;
                    }

                }
                else if (type.split('-')[0] == 'FS' || type.split('-')[0] == 'TW') {
                    document.getElementById('<%=btnVendorEditBooking.ClientID %>').disabled = true;
                    document.getElementById('<%=btnDeleteBooking.ClientID %>').disabled = true;
                    document.getElementById('<%=btnConfirmFixedSlot.ClientID %>').disabled = false;
                    document.getElementById('<%=btnDeleteFixedSlot.ClientID %>').disabled = false;
                    document.getElementById('<%=btnBookingHistory_1.ClientID %>').disabled = true;
                }
            }

            if (BookingStatus == 'Deleted' || BookingStatus == 'Cancelled') {
                var varEDBVBtnAddVendorBooking = document.getElementById('<%=btnAddVendorBooking.ClientID %>');
                if (varEDBVBtnAddVendorBooking != null) {
                    varEDBVBtnAddVendorBooking.disabled = false;
                }

                var varEDBVBtnVendorEditBooking = document.getElementById('<%=btnVendorEditBooking.ClientID %>');
                if (varEDBVBtnVendorEditBooking != null) {
                    varEDBVBtnVendorEditBooking.disabled = true;
                }

                var varEDBVBtnDeleteBooking = document.getElementById('<%=btnDeleteBooking.ClientID %>');
                if (varEDBVBtnDeleteBooking != null) {
                    varEDBVBtnDeleteBooking.disabled = true;
                }

                var varEDBVBtnConfirmFixedSlot = document.getElementById('<%=btnConfirmFixedSlot.ClientID %>');
                if (varEDBVBtnConfirmFixedSlot != null) {
                    varEDBVBtnConfirmFixedSlot.disabled = true;
                }

                var varEDBVBtnDeleteFixedSlot = document.getElementById('<%=btnDeleteFixedSlot.ClientID %>');
                if (varEDBVBtnDeleteFixedSlot != null) {
                    varEDBVBtnDeleteFixedSlot.disabled = true;
                }
            }

            var CurrentRdbID = spanChk.id;
            var Chk = spanChk;
            Parent = document.getElementById("<%=gvVendor.ClientID%>");
            var items = Parent.getElementsByTagName('input');
            for (i = 0; i < items.length; i++) {
                if (items[i].id != CurrentRdbID && items[i].type == "radio") {
                    if (items[i].checked) {
                        items[i].checked = false;
                    }
                }
            }

            //Emergency fix - temp as per Anthony's mail on 11 jan 2016
            dispablebuttons(siteid, Role);
            //-------------------------------------------------------------------------------------------//

            setReadonlyClass();
        }

        function dispablebuttons(siteid, Role) {
            var dispbalesites = document.getElementById('<%=hdnDisableSites.ClientID %>').value;
            if (dispbalesites.indexOf("," + siteid + ",") >= 0 && (Role == 'vendor' || Role == 'carrier')) {
                document.getElementById('<%=btnAddVendorBooking.ClientID %>').disabled = true;
                document.getElementById('<%=btnVendorEditBooking.ClientID %>').disabled = true;
                document.getElementById('<%=btnDeleteBooking.ClientID %>').disabled = true;
                document.getElementById('<%=btnConfirmFixedSlot.ClientID %>').disabled = true;
                document.getElementById('<%=btnDeleteFixedSlot.ClientID %>').disabled = true;
            }
        }

        function setScroll() {
            if ($('.fixedTable') != null) {
                $('.fixedTable').scrollLeft(document.getElementById('<%=scroll2.ClientID%>').value);
            }
            setReadonlyClass();
        }

        function getScroll1(ctrDiv) {
            document.getElementById('<%=scroll2.ClientID%>').value = ctrDiv.scrollLeft;
        }

        var _oldonerror = window.onerror;
        window.onerror = function (ConfirmationMsg, url, lineNr) { return true; };

        function setToolTip() {
            if (_val_IE6)
                initBaloonTips();
        }

        function ValidateVolumeDetails() {
            if ($("select[id$='ddlSite'] option:selected").val() > 0) {
                return true;
            }
            else {
                alert('<%=siteRequired%>');
                $("select[id$='ddlSite']").focus();
                return false;
            }
        }

        function ValidateSiteCarrier() {
            if ($("select[id$='ddlSite'] option:selected").val() > 0) {
                return true;
            }
            else {
                alert('<%=siteRequired%>');
                $("select[id$='ddlSite']").focus();
                return false;
            }
        }

        document.onmousemove = positiontip

        function EnableDisableButtonsForWindowSlot(spanChk, WeekDay, LogicalDate, TimeWindowID) {
            var Role = document.getElementById('<%=hdnRole.ClientID %>').value;
            var IsChecked = spanChk.checked;
            if (spanChk.checked == undefined) {
                IsChecked = true;
            }
            if (IsChecked) {
                document.getElementById('<%=hdnBookingWeekDay.ClientID %>').value = WeekDay;
                document.getElementById('<%=btnConfirmAFixedSlot.ClientID %>').disabled = false;
                document.getElementById('<%=btnEditABooking.ClientID %>').disabled = false;
                document.getElementById('<%=btnDeleteABooking.ClientID %>').disabled = false;
                document.getElementById('<%=btnQualityChecked.ClientID %>').disabled = false;
                document.getElementById('<%=btnDelArrived.ClientID %>').disabled = false;
                document.getElementById('<%=btnDelivaryUnload.ClientID %>').disabled = false;
                document.getElementById('<%=btnBookingHistory.ClientID %>').disabled = false;
                document.getElementById('<%=btnConfirmPOArrival.ClientID %>').disabled = true;
                document.getElementById('<%=hdnLogicalDate.ClientID %>').value = LogicalDate;
                var varNoShow = document.getElementById('<%=btnNoShow.ClientID %>');
                var currentDate = new Date($("#<%=hdnCurrentDate.ClientID %>").val());
                var bookingDate = new Date($("#<%=hdnSelectedDate.ClientID %>").val());
                document.getElementById('<%=hdnWindowTimeID.ClientID %>').value = TimeWindowID;
            }
        }

        $(document).ready(function () {

            $("body").keypress(function (e) {
                if (e.which == 13 || e.keyCode == 13) {
                    $('#<%=btnSearch.ClientID %>').click();
                    return false;
                }
            });

            if ($("#<%=ddlStatus.ClientID%>").is(':visible') == false) {
                $("#tdbtnSearch").attr("align", "right");
                $("#tdbtnSearch").attr("colspan", "6");
            }
            $('.outerTable tbody').scroll(function (e) { //detect a scroll event on the tbody
                /*
                Setting the thead left value to the negative valule of tbody.scrollLeft will make it track the movement
                of the tbody element. Setting an elements left value to that of the tbody.scrollLeft left makes it maintain 			it's relative position at the left of the table.
                */
                $('.outerTable thead').css("left", -$(".outerTable tbody").scrollLeft()); //fix the thead relative to the body scrolling
                $('.outerTable thead th:nth-child(1)').css("left", $(".outerTable tbody").scrollLeft()); //fix the first cell of the header
                $('.outerTable tbody td:nth-child(1)').css("left", $(".outerTable tbody").scrollLeft()); //fix the first column of tdbody
            });

            $('.outerTable1 tbody').scroll(function (e) { //detect a scroll event on the tbody
                /*
                Setting the thead left value to the negative valule of tbody.scrollLeft will make it track the movement
                of the tbody element. Setting an elements left value to that of the tbody.scrollLeft left makes it maintain 			it's relative position at the left of the table.
                */
                $('.outerTable1 thead').css("left", -$(".outerTable1 tbody").scrollLeft()); //fix the thead relative to the body scrolling
                $('.outerTable1 thead th:nth-child(1)').css("left", $(".outerTable1 tbody").scrollLeft()); //fix the first cell of the header
                $('.outerTable1 tbody td:nth-child(1)').css("left", $(".outerTable1 tbody").scrollLeft()); //fix the first column of tdbody
            });
        });
    </script>

    <!----START of HTML page--->

    <asp:HiddenField ID="hdnWindowTime" runat="server" />
    <asp:HiddenField ID="hdnDoorNumber" runat="server" />
    <asp:HiddenField ID="hdnSiteDoorNumberID" runat="server" />
    <asp:HiddenField ID="hdnPeriod" runat="server" />
    <asp:HiddenField ID="hdnDliveryUnload" runat="server" Value="1" />
    <asp:HiddenField ID="hdnWindowTimeID" runat="server" />
    <asp:HiddenField ID="hdnDliveryArrived" runat="server" Value="1" />
    <asp:HiddenField ID="hdnAlternateViewStatus" runat="server" />
    <input type="hidden" id="scroll1" runat="server" />
    <input type="hidden" id="scroll2" runat="server" />
    <div id="div1" runat="server" class="balloonstyle">
    </div>
    <div id="divVolumeNew" runat="server" class="balloonstyle">
    </div>
    <div id="dhtmltooltip" class="balloonstyle">
    </div>
    <iframe id="iframetop" scrolling="no" frameborder="0" class="iballoonstyle" width="0" height="0"></iframe>
    <div class="balloonstyle" id="AddBooking">
        <span class="bla8blue">Click here if you would like to make a new booking and you do
            not have or want to use a Fixed Slot </span>
    </div>
    <div class="balloonstyle" id="DeleteBooking">
        <span class="bla8blue">Click here to delete an existing booking. Only bookings with
            the status Confirmed Slot can be edited </span>
    </div>
    <div class="balloonstyle" id="EditBooking">
        <span class="bla8blue">Click here to edit an existing booking. Only bookings with the
            status Confirmed Slot can be edited</span>
    </div>
    <div class="balloonstyle" id="ConfirmFixedSlot">
        <span class="bla8blue">Click here if you would like to make a new booking and you wish
            to use a Fixed Slot</span>
    </div>
    <div class="balloonstyle" id="DeleteFixedSlot">
        <span class="bla8blue">Click here to delete a Fixed Slot. This will only delete the
            slot for the date selected and will not affect this slot in the upcoming weeks.
            If you would like to cancel a slot on a permanent basis please contact the relevant
            Goods In Office. </span>
    </div>
    <h2>
        <cc1:ucLabel ID="lblBookingDelivery" runat="server" Text="Booking/Delivery"></cc1:ucLabel>
    </h2>

    <asp:UpdatePanel ID="up1" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <div class="right-shadow">
                <div>
                    <asp:HiddenField ID="hdnDisableSites" runat="server" Value="" />
                    <asp:HiddenField ID="hdnBookingWeekDay" runat="server" Value="" />
                    <asp:HiddenField ID="hdnRole" runat="server" Value="" />
                    <asp:HiddenField ID="hdnBookingFor" runat="server" Value="" />
                    <asp:HiddenField ID="hdnPrimaryKey" runat="server" Value="" />
                    <asp:HiddenField ID="hdnRecordTable" runat="server" Value="" />
                    <asp:HiddenField ID="hdnsiteid" runat="server" Value="" />
                    <asp:HiddenField ID="hdnbookingdate" runat="server" />
                    <asp:HiddenField ID="hdnLogicalDate" runat="server" />
                    <asp:HiddenField ID="hdnCurrentDate" runat="server" />
                    <asp:HiddenField ID="hdnSelectedDate" runat="server" />
                    <asp:ValidationSummary ID="vs1" runat="server" ValidationGroup="a" ShowMessageBox="true"
                        ShowSummary="false" />
                </div>
                <div class="formbox">
                    <div id="divVendorBottons" runat="server">
                        <table width="30%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                            <tr>
                                <td style="width: 10%"></td>
                                <td style="font-weight: bold; width: 30%">
                                    <cc1:ucLabel ID="lblSite" runat="server" Text="Site" isRequired="true"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold; width: 5%">:
                                </td>
                                <td style="width: 45%">
                                    <cc2:ucSite ID="ddlSiteCarrier" runat="server" />
                                </td>
                                <td style="width: 10%"></td>
                            </tr>
                        </table>
                        <table onmouseover="setToolTip();" width="95%" cellspacing="0" cellpadding="0" border="0"
                            class="form-table">
                            <tr>
                                <td colspan="5">
                                    <cc1:ucLabel runat="server" ID="lblAddNewBooking" Text="To make a new booking click add booking" />
                                </td>
                            </tr>
                            <tr>
                                <td valign="top">
                                    <cc1:ucButton ID="btnAddVendorBooking" runat="server" Text="Add Booking" CssClass="button"
                                        OnClick="btnAddBooking_Click" Width="150px" OnClientClick="return ValidateSiteCarrier();" />
                                    <a href="javascript:void(0)" rel="AddBooking" onmouseover="ddrivetip('AddBooking','#ededed','700');"
                                        onmouseout="hideddrivetip();">
                                        <img src="../../../Images/info_button1.gif" align="middle" border="0" alt=""></a>
                                </td>
                                <td colspan="4"></td>
                            </tr>
                            <tr>
                                <td colspan="5">
                                    <cc1:ucLabel runat="server" ID="lblAppropriateActionButtonBooking" Text="To confirm, edit or delete a booking, select from the list of bookings below then click the appropriate action button" />
                                    <br />
                                    <cc1:ucLabel runat="server" ID="lblbookingsmadebyacarrierforyourordersText" Text="" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <cc1:ucButton ID="btnVendorEditBooking" runat="server" Text="Edit Booking" CssClass="button"
                                        Width="150px" OnClick="btnEditABooking_Click" />
                                    <a href="javascript:void(0)" rel="EditBooking" onmouseover="ddrivetip('EditBooking','#ededed','700');"
                                        onmouseout="hideddrivetip();">
                                        <img src="../../../Images/info_button1.gif" align="middle" border="0" alt=""></a>
                                </td>
                                <td>
                                    <cc1:ucButton ID="btnDeleteBooking" runat="server" Text="Delete Booking" CssClass="button"
                                        Width="150px" OnClick="btnDeleteABooking_Click" />
                                    <a href="javascript:void(0)" rel="DeleteBooking" onmouseover="ddrivetip('DeleteBooking','#ededed','400');"
                                        onmouseout="hideddrivetip();">
                                        <img src="../../../Images/info_button1.gif" align="middle" border="0" alt=""></a>
                                </td>
                                <td>
                                    <cc1:ucButton ID="btnConfirmFixedSlot" runat="server" Text="Confirm a Fixed Slot"
                                        CssClass="button" Width="150px" OnClick="btnConfirmFixedSlot_Click" />
                                    <a href="javascript:void(0)" rel="ConfirmFixedSlot" onmouseover="ddrivetip('ConfirmFixedSlot','#ededed','400');"
                                        onmouseout="hideddrivetip();">
                                        <img src="../../../Images/info_button1.gif" align="middle" border="0" alt=""></a>
                                </td>
                                <td>
                                    <cc1:ucButton ID="btnDeleteFixedSlot" runat="server" Text="Delete a Fixed Slot" CssClass="button"
                                        Width="150px" OnClick="btnDeleteFixedSlot_Click" />
                                    <a href="javascript:void(0)" rel="DeleteFixedSlot" onmouseover="ddrivetip('DeleteFixedSlot','#ededed','700');"
                                        onmouseout="hideddrivetip();">
                                        <img src="../../../Images/info_button1.gif" align="middle" border="0" alt=""></a>
                                </td>
                                <td>
                                    <cc1:ucButton ID="btnBookingHistory_1" runat="server" Text="Booking History" CssClass="button"
                                        OnClick="btnBookingHistory_1_Click" />
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div id="divSearchButtons" runat="server">
                        <table width="100%" cellspacing="1" cellpadding="0" border="0" align="center" class="top-settings">
                            <tr>
                                <td style="font-weight: bold; width: 18%">
                                    <cc1:ucLabel ID="lblSchedulingdate" runat="server" Text="Scheduling Date"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold; width: 1%">:
                                </td>
                                <td style="font-weight: bold; width: 14%">
                                    <cc2:ucDate ClientIDMode="Static" ID="txtDate" runat="server" />
                                    <asp:RequiredFieldValidator ID="rfvDateRequired" runat="server" Display="None" ControlToValidate="txtDate$txtUCDate"
                                        ValidateEmptyText="true" ValidationGroup="a" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                </td>
                                <td style="font-weight: bold; width: 17%">
                                    <cc1:ucLabel ID="lblSitePrefixName" runat="server"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold; width: 1%">:
                                </td>
                                <td style="width: 21%">
                                    <cc2:ucSite ID="ddlSite" runat="server" />
                                </td>
                                <td style="font-weight: bold; width: 10%" id="tdStatus1" runat="server">
                                    <cc1:ucLabel ID="lblStatus" runat="server"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold; width: 1%" id="tdStatus2" runat="server">:
                                </td>
                                <td style="font-weight: bold; width: 17%" colspan="2" id="tdStatus3" runat="server">
                                    <cc1:ucDropdownList ID="ddlStatus" runat="server" Width="130px">
                                        <asp:ListItem Text="--All--" Value="0"></asp:ListItem>
                                        <asp:ListItem Text="Confirmed, not arrived" Value="1"></asp:ListItem>
                                        <asp:ListItem Text="Unconfirmed" Value="10"></asp:ListItem>
                                        <asp:ListItem Text="Delivery Arrived" Value="2"></asp:ListItem>
                                        <asp:ListItem Text="Delivery Unloaded" Value="3"></asp:ListItem>
                                        <asp:ListItem Text="Refused" Value="4"></asp:ListItem>
                                        <asp:ListItem Text="Quality Checked" Value="5"></asp:ListItem>
                                        <asp:ListItem Text="On site" Value="6"></asp:ListItem>
                                        <asp:ListItem Text="Failed" Value="7"></asp:ListItem>
                                        <asp:ListItem Text="Pending" Value="8"></asp:ListItem>
                                        <asp:ListItem Text="Deleted" Value="9"></asp:ListItem>
                                    </cc1:ucDropdownList>
                                </td>
                            </tr>
                            <tr id="trAlternateHide" runat="server" style="height: 24px;">
                                <td style="font-weight: bold;">
                                    <cc1:ucLabel ID="lblPO" runat="server" Text="PO#"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold;">:
                                </td>
                                <td style="font-weight: bold;">
                                    <cc1:ucTextbox ID="txtPurchaseNumber" runat="server" Width="120px"></cc1:ucTextbox>
                                </td>
                                <td style="font-weight: bold;">
                                    <cc1:ucLabel ID="lblStockPlanner" ClientIDMode="Static" runat="server"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold;">:
                                </td>
                                <td style="font-weight: bold;">
                                    <cc1:ucDropdownList ID="ddlStockPalnner" runat="server" Width="150px">
                                    </cc1:ucDropdownList>
                                </td>
                                <td style="font-weight: bold;">
                                    <cc1:ucLabel ID="lblDisplayBy" runat="server" Text="Display By"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold;">:
                                </td>
                                <td style="font-weight: bold;" colspan="2">
                                    <cc1:ucDropdownList ID="ddlDisplayBy" runat="server" Width="130px">
                                        <asp:ListItem Text="BY DELIVERY" Value="BYDELIVERY"></asp:ListItem>
                                        <asp:ListItem Text="BY VENDOR" Value="BYVENDOR"></asp:ListItem>
                                        <asp:ListItem Text="BY PO" Value="BYPO"></asp:ListItem>
                                    </cc1:ucDropdownList>
                                </td>
                            </tr>
                            <tr style="height: 24px;">
                                <td style="font-weight: bold;" id="tdBookingRef1" runat="server">
                                    <cc1:ucLabel ID="lblBookingRef" runat="server" Text="Booking Ref"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold;" id="tdBookingRef2" runat="server">:
                                </td>
                                <td style="font-weight: bold;" id="tdBookingRef3" runat="server">
                                    <cc1:ucTextbox ID="txtBookingRef" runat="server" Width="120px"></cc1:ucTextbox>
                                </td>
                                <td style="font-weight: bold;" id="tdVendor1" runat="server">
                                    <cc1:ucLabel ID="lblVendorNo" runat="server" Text="Vendor No"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold;" id="tdVendor2" runat="server">:
                                </td>

                                <td style="font-weight: bold;" colspan="3" id="tdVendor3" runat="server">

                                    <span id="spVender" runat="server">
                                        <cc3:ucSeacrhVendor ID="ucSeacrhVendor1" runat="server" />
                                    </span>
                                    <cc1:ucLiteral ID="ltSearchVendorName" runat="server" Visible="false"></cc1:ucLiteral>
                                </td>

                                <td align="left" id="tdbtnSearch" rowspan="2" valign="bottom">
                                    <cc1:ucButton ID="btnSearch" runat="server" Text="Search" CssClass="button" OnClick="btnSearch_Click"
                                        ValidationGroup="a" />
                                </td>
                                <td align="left" id="tdIcon" runat="server" rowspan="2" valign="bottom">
                                    <uc1:ucExportButton ID="btnExportToExcel1" runat="server" />
                                    &nbsp; &nbsp;
                                    <asp:ImageButton ID="imgReport" runat="server" ImageUrl="~/Images/Report.png" OnClick="imgReport_Click"
                                        ToolTip="Schedule PO/Line Report" />
                                    <asp:ImageButton ID="imgAlternateView" runat="server" Visible="false" ImageUrl="~/Images/alternateslot.png"
                                        OnClick="imgAlternateReport_Click" ToolTip="Alternate Slot View" />

                                    <asp:ImageButton ID="imgWindowAlternateView" runat="server" Visible="true" ImageUrl="~/Images/window-slot.png"
                                        OnClick="imgWindowAlternateView_Click" Style="width: 19px" ToolTip="Window Alternate Slot View" />
                                </td>
                            </tr>
                            <tr style="height: 24px;" id="skuCatCode" runat="server">
                                <td style="font-weight: bold;">
                                    <cc1:ucLabel ID="lblSkuWithHash" runat="server"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold;">:
                                </td>
                                <td style="font-weight: bold;">
                                    <cc1:ucTextbox ID="txtSku" Width="120px" runat="server"></cc1:ucTextbox>
                                </td>

                                <td style="font-weight: bold;">
                                    <cc1:ucLabel ID="lblCATCode" runat="server"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold;">:
                                </td>
                                <td style="font-weight: bold;">
                                    <cc1:ucTextbox ID="txtCATCode" Width="120px" runat="server"></cc1:ucTextbox>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <!------>
                    <div id="divODButtons" runat="server">
                        <div id="trManageBooking" runat="server" class="button-align">
                            <cc1:ucButton ID="btnAddBooking" runat="server" Text="Add Booking" CssClass="button"
                                OnClick="btnAddBooking_Click" OnClientClick="return ValidateVolumeDetails();"
                                Style="display: none;" />
                            <cc1:ucButton ID="btnConfirmAFixedSlot" runat="server" Text="Confirm a Fixed Slot"
                                CssClass="button" OnClick="btnConfirmAFixedSlot_Click" Style="display: none;" />
                            <cc1:ucButton ID="btnEditABooking" runat="server" Text="Edit a Booking" CssClass="button"
                                OnClick="btnEditBooking_Click" Style="display: none;" />
                            <cc1:ucButton ID="btnDeleteABooking" runat="server" Text="Delete a Booking" CssClass="button"
                                OnClick="btnDeleteABooking_Click" Style="display: none;" />
                            <cc1:ucButton ID="btnUnexpectedDelivery" runat="server" Text="Unexpected Delivery"
                                CssClass="button" OnClick="btnUnexpectedDelivery_Click" OnClientClick="return ValidateVolumeDetails();"
                                Style="display: none;" />
                            <cc1:ucButton ID="btnVolumeDetails" runat="server" Text="Volume Details" CssClass="button"
                                OnClick="btnHidden_Click" OnClientClick="return ValidateVolumeDetails();" Style="display: none;" />
                        </div>
                        <div id="trManagedDelivery" runat="server" class="button-align">
                            <cc1:ucButton ID="btnAddByPOBooking" runat="server" CssClass="button" Visible="false"
                                OnClick="btnAddByPOBooking_Click" Style="display: none;" />
                            <cc1:ucButton ID="btnDelArrived" runat="server" Text="Delivery Arrived" CssClass="button"
                                OnClick="btnDelArrived_Click" Style="display: none;" />
                            <cc1:ucButton ID="btnDelivaryUnload" runat="server" Text="Delivery Unload" CssClass="button"
                                OnClick="btnDelivaryUnload_Click" Style="display: none;" />
                            <cc1:ucButton ID="btnNoShow" runat="server" CssClass="button" OnClick="btnNoShow_Click"
                                Enabled="false" Style="display: none;" />
                            <cc1:ucButton ID="btnQualityChecked" runat="server" Text="Quality Checked" CssClass="button"
                                OnClick="btnQualityChecked_Click" Style="display: none;" />
                            <cc1:ucButton ID="btnBookingHistory" runat="server" Text="Booking History" CssClass="button"
                                OnClick="btnBookingHistory_Click" Style="display: none;" />
                            <cc1:ucButton ID="btnConfirmPOArrival" runat="server" Text="Confirm PO Arrival" CssClass="button"
                                OnClick="btnConfirmPOArrival_Click" Style="display: none;" />
                        </div>
                    </div>
                    <table cellspacing="1" cellpadding="0" class="form-table" width="75%">
                        <tr>
                            <td align="center">
                                <cc1:ucLabel ID="lblVendorNoBooking" runat="server" value="You currently have no upcoming bookings."
                                    Visible="false"></cc1:ucLabel>
                            </td>
                        </tr>
                    </table>
                    <div style="width: 100%; height: 440px; overflow: scroll" id="divPrint" class="fixedTable"
                        runat="server" onscroll="getScroll1(this);">
                        <table cellspacing="1" cellpadding="0" class="form-table">
                            <tr>
                                <td>
                                    <cc1:ucGridView ID="ucGridView1" runat="server" AutoGenerateColumns="false" CssClass="grid gvclass"
                                        CellPadding="0" Width="1600px" DataKeyNames="PKID" OnSorting="gridView_Sorting"
                                        AllowPaging="true" PageSize="45" AllowSorting="true" OnRowDataBound="ucGridView1_RowDataBound"
                                        OnPageIndexChanging="ucGridView1_PageIndexChanging">
                                        <AlternatingRowStyle BackColor="White" ForeColor="#284775"></AlternatingRowStyle>
                                        <Columns>
                                            <asp:TemplateField>
                                                <HeaderStyle Width="20px" HorizontalAlign="Left" />
                                                <ItemStyle Width="20px" HorizontalAlign="Left" />
                                                <ItemTemplate>
                                                    <asp:HiddenField ID="hdnVendorId" runat="server" Value='<%#Eval("VendorID") %>' />
                                                    <asp:HiddenField ID="hdnWeekday" runat="server" Value='<%#Eval("Weekday") %>' />
                                                    <asp:HiddenField ID="hdnBookingID" runat="server" Value='<%#Eval("PKID") %>' />
                                                    <asp:HiddenField ID="hdnBookingStatusID" runat="server" Value='<%#Eval("BookingStatusID") %>' />
                                                    <asp:HiddenField ID="hdnConfirmPOArrival" runat="server" Value='<%#Eval("ConfirmPOArrival") %>' />
                                                    <asp:RadioButton ID="rdoBookingID"
                                                        onclick='<%# "javascript:EnableDisableButtons(this,&#39;" + Eval("PKID") + "&#39;,&#39;" + Eval("BookingStatusID") + "&#39;,&#39;" + Eval("SupplierType") + "&#39;,&#39;" + Eval("Weekday") + "&#39;,&#39;" + Eval("LogicalDate", "{0:dd/MM/yyyy}") + "&#39;,&#39;" + Eval("BookingStatus")  + "&#39;,&#39;" + Eval("IsHavingIssue")  + "&#39;);" %>'
                                                        runat="server" GroupName="BookingNo" />
                                                    <asp:HiddenField ID="hdnIsVenodrPalletChecking" runat="server" Value='<%# Convert.ToBoolean(Eval("IsVenodrPalletChecking"))  || Convert.ToBoolean(Eval("IsISPM15Applied")) %>' />
                                                    <asp:HiddenField ID="hdnIsStandardPalletChecking" runat="server" Value='<%#Eval("IsStandardPalletChecking") %>' />
                                                     <asp:HiddenField ID="hdnIsEnableHazardouesItemPrompt" runat="server" Value='<%#Eval("IsEnableHazardouesItemPrompt") %>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="PO" SortExpression="PO">
                                                <HeaderStyle Width="80px" HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Left" Width="80px" />
                                                <ItemTemplate>
                                                    <cc1:ucLiteral ID="ltPO" runat="server" Text='<%#Eval("PO") %>'></cc1:ucLiteral>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField HeaderText="Status" DataField="BookingStatus" SortExpression="BookingStatus">
                                                <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Left" Width="150px" />
                                            </asp:BoundField>
                                            <asp:TemplateField HeaderText="Vendor Name" SortExpression="VendorName,BookingRef">
                                                <HeaderStyle Width="100px" HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Left" Width="100px" />
                                                <ItemTemplate>
                                                    <cc1:ucLiteral ID="UcLiteral2" runat="server" Text='<%#Eval("VendorName") %>'></cc1:ucLiteral>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Hazardous Items ?" Visible="false" SortExpression="IsEnableHazardouesItemPrompt">
                                                <HeaderStyle Width="100px" HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Left" Width="100px" />
                                                <ItemTemplate>
                                               
                                                     <cc1:ucLabel ID="lblHazardousItemCheckRequired"
                                                        Visible='<%# Convert.ToBoolean(Eval("IsEnableHazardouesItemPrompt")) %>'
                                                        ForeColor="Red" runat="server"
                                                        Text="Hazardous Item Check Required."></cc1:ucLabel>
                                                    
                                                    <%--<asp:CheckBox ID="chkIsEnableHazardouesItemPrompt" runat="server" Enabled="false" Checked='<%# Convert.ToBoolean(Eval("IsEnableHazardouesItemPrompt")) %>' />--%>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="ISPM15/Standard pallet check" SortExpression="IsVenodrPalletChecking">
                                                <HeaderStyle Width="100px" HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Left" Width="100px" />
                                                <ItemTemplate>
                                                    <cc1:ucLabel ID="lblVendorPalletCheckingRequired"
                                                        Visible='<%# Convert.ToBoolean(Eval("IsVenodrPalletChecking")) || Convert.ToBoolean(Eval("IsStandardPalletChecking")) || Convert.ToBoolean(Eval("IsISPM15Applied"))%>'
                                                        ForeColor="Red" runat="server"
                                                        Text=""
                                                        ></cc1:ucLabel>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Add Info" SortExpression="BookingComments">
                                                <HeaderStyle Width="30px" HorizontalAlign="Left" />
                                                <ItemStyle Width="30px" HorizontalAlign="Center" />
                                                <ItemTemplate>
                                                    <div>
                                                        <a href="javascript:void(0)" rel='<%# DataBinder.Eval(Container.DataItem, "BookingComments") %>'
                                                            tipwidth="350" onmouseover="ddrivetip('<%# DataBinder.Eval(Container.DataItem, "BookingComments") %>','#ededed','350');"
                                                            onmouseout="hideddrivetip();">
                                                            <img src="../../../Images/info_button1.gif" runat="server" id="imgInfo" align="absMiddle" border="0" /></a>
                                                    </div>
                                                    <div class="balloonstyle" id='<%# DataBinder.Eval(Container.DataItem, "BookingComments") %>'>
                                                        <span class="bla8blue">
                                                            <div style="text-align: center;">
                                                                <strong>
                                                                    <cc1:ucLabel ID="lblAdditionalInfo" Text="Additional Info" runat="server"></cc1:ucLabel>
                                                                </strong>
                                                                <br />
                                                                <br />
                                                                <asp:Literal ID="ltInfo" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "BookingComments") %>'></asp:Literal>
                                                            </div>
                                                        </span>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Door Name" SortExpression="DoorNumber">
                                                <HeaderStyle Width="40px" HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Center" Width="40px" />
                                                <ItemTemplate>
                                                    <cc1:ucLiteral ID="UcLiteral7" runat="server" Text='<%#Eval("DoorNumber") %>'></cc1:ucLiteral>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField HeaderText="BookingRefNo" DataField="BookingRef" SortExpression="BookingRef">
                                                <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Left" Width="100px" />
                                            </asp:BoundField>
                                            <asp:TemplateField HeaderText="TimeDay" SortExpression="AutoID">
                                                <HeaderStyle Width="120px" HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Left" Width="100px" />
                                                <ItemTemplate>
                                                    <asp:HiddenField ID="hdnExpectedDeliveryDate" runat="server" Value='<%# Eval("DeliveryDate") %>' />
                                                    <asp:HiddenField ID="hdnTimeWindowID" runat="server" Value='<%# Eval("TimeWindowID") %>' />
                                                    <cc1:ucLiteral ID="UcLiteral17_2" runat="server" Text='<%#Eval("NonWindowFromToTime") %>'
                                                        Visible="false"></cc1:ucLiteral>
                                                    <cc1:ucLiteral ID="UcLiteral17_1" runat="server" Text='<%#Eval("WindowStartEndTime") %>'
                                                        Visible="false"></cc1:ucLiteral>
                                                    <cc1:ucLiteral ID="UcLiteral17" runat="server" Text='<%#Eval("ExpectedDeliveryTime") %>'></cc1:ucLiteral>&nbsp;
                                                    <cc1:ucLiteral ID="UcLiteral18" runat="server" Text='<%# Eval("DeliveryDate", "{0:dddd}")%>'></cc1:ucLiteral>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Carrier Name" SortExpression="CarrierName">
                                                <HeaderStyle Width="80px" HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Left" />
                                                <ItemTemplate>
                                                    <cc1:ucLiteral ID="UcLiteral3" runat="server" Text='<%#Eval("CarrierName") %>'></cc1:ucLiteral>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Priority" SortExpression="Prioirty" Visible="false">
                                                <HeaderStyle Width="40px" HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Center" />
                                                <ItemTemplate>
                                                    <cc1:ucLiteral ID="ltPriority" runat="server" Text='<%#Eval("Priority") %>'></cc1:ucLiteral>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="NPlts" SortExpression="PalletsScheduled">
                                                <HeaderStyle Width="40px" HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Center" />
                                                <ItemTemplate>
                                                    <cc1:ucLiteral ID="UcLiteral4" runat="server" Text='<%#Eval("PalletsScheduled") %>'></cc1:ucLiteral>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="NCtns" SortExpression="CatronsScheduled">
                                                <HeaderStyle Width="40px" HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Center" />
                                                <ItemTemplate>
                                                    <cc1:ucLiteral ID="UcLiteral6" runat="server" Text='<%#Eval("CatronsScheduled") %>'></cc1:ucLiteral>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="NLns" SortExpression="LinesScheduled">
                                                <HeaderStyle Width="40px" HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Center" />
                                                <ItemTemplate>
                                                    <cc1:ucLiteral ID="UcLiteral5" runat="server" Text='<%#Eval("LinesScheduled") %>'></cc1:ucLiteral>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField HeaderText="NoOfLifts" DataField="LiftsScheduled" SortExpression="LiftsScheduled">
                                                <HeaderStyle Width="60px" HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:BoundField HeaderText="Booking Type" DataField="BookingType" SortExpression="BookingType">
                                                <HeaderStyle Width="80px" HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:BoundField>
                                            <asp:TemplateField HeaderText="Delivery Type" SortExpression="DeliveryType">
                                                <HeaderStyle Width="80px" HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Left" />
                                                <ItemTemplate>
                                                    <cc1:ucLiteral ID="UcLiteral1" runat="server" Text='<%#Eval("DeliveryType") %>'></cc1:ucLiteral>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField HeaderText="Stock Planner" DataField="InventoryManager" SortExpression="InventoryManager">
                                                <HeaderStyle Width="80px" HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:BoundField>
                                            <asp:BoundField HeaderText="Vehicle Type" DataField="VehicleType" SortExpression="VehicleType">
                                                <HeaderStyle Width="80px" HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:BoundField>
                                            <asp:BoundField HeaderText="Pre Advise Notification" DataField="PreAdviseNotification"
                                                SortExpression="PreAdviseNotification">
                                                <HeaderStyle Width="80px" HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:BoundField>
                                            <asp:BoundField HeaderText="Door Type" DataField="DoorType" SortExpression="DoorType">
                                                <HeaderStyle Width="80px" HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:BoundField>
                                            <%-- Sprint 3a start --%>
                                            <asp:TemplateField HeaderText="Due Date" SortExpression="dueDate">
                                                <HeaderStyle Width="80px" HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Left" />
                                                <ItemTemplate>
                                                    <cc1:ucLiteral ID="ltdueDate" runat="server" Text='<%# Eval("dueDate") != DBNull.Value ?  Convert.ToDateTime(Eval("dueDate").ToString()).ToString("dd/MM/yyyy") : ""%>'></cc1:ucLiteral>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Arrived At" SortExpression="ArrivedAt">
                                                <HeaderStyle Width="80px" HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Left" />
                                                <ItemTemplate>
                                                    <cc1:ucLiteral ID="ltArrivedAt" runat="server" Text='<%# Eval("ArrivedAt") != DBNull.Value ?  Convert.ToDateTime(Eval("ArrivedAt").ToString()).ToString("dd/MM/yyyy - HH:mm") : ""%>'></cc1:ucLiteral>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Unloaded At" SortExpression="UnloadAt">
                                                <HeaderStyle Width="80px" HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Left" />
                                                <ItemTemplate>
                                                    <cc1:ucLiteral ID="ltUnloadAt" runat="server" Text='<%#Eval("UnloadAt") != DBNull.Value ?  Convert.ToDateTime(Eval("UnloadAt").ToString()).ToString("dd/MM/yyyy - HH:mm") : "" %>'></cc1:ucLiteral>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField HeaderText="WindowName" DataField="WindowName" SortExpression="WindowName">
                                                <HeaderStyle Width="160px" HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:BoundField>
                                            <%-- Sprint 3a End --%>
                                        </Columns>
                                    </cc1:ucGridView>
                                    <cc1:ucGridView ID="ucGridViewExcel" runat="server" AutoGenerateColumns="false" CssClass="grid gvclass"
                                        Visible="false" CellPadding="0" Width="1700px" DataKeyNames="PKID" OnRowDataBound="ucGridView1_RowDataBound">
                                        <AlternatingRowStyle BackColor="White" ForeColor="#284775"></AlternatingRowStyle>
                                        <Columns>
                                            <asp:TemplateField>
                                                <HeaderStyle Width="20px" HorizontalAlign="Left" />
                                                <ItemStyle Width="20px" HorizontalAlign="Left" />
                                                <ItemTemplate>
                                                    <asp:HiddenField ID="hdnVendorId" runat="server" Value='<%#Eval("VendorID") %>' />
                                                    <asp:HiddenField ID="hdnWeekday" runat="server" Value='<%#Eval("Weekday") %>' />
                                                    <asp:HiddenField ID="hdnBookingID" runat="server" Value='<%#Eval("PKID") %>' />
                                                    <asp:HiddenField ID="hdnBookingStatusID" runat="server" Value='<%#Eval("BookingStatusID") %>' />
                                                    <asp:HiddenField ID="hdnConfirmPOArrival" runat="server" Value='<%#Eval("ConfirmPOArrival") %>' />
                                                    <asp:HiddenField ID="hdnIsVendorPalletCheckingRequired" runat="server" Value='<%#Eval("IsVenodrPalletChecking") %>' />
                                                     <asp:HiddenField ID="hdnIsEnableHazardouesItemPrompt" runat="server" Value='<%#Eval("IsEnableHazardouesItemPrompt") %>' />
                                                    <asp:HiddenField ID="hdnIsStandardPalletCheckingRequired" runat="server" Value='<%#Eval("IsStandardPalletChecking") %>' />
                                                    <asp:RadioButton ID="rdoBookingID" onclick='<%# "javascript:EnableDisableButtons(this,&#39;" + Eval("PKID") + "&#39;,&#39;" + Eval("BookingStatusID") + "&#39;,&#39;" + Eval("SupplierType") + "&#39;,&#39;" + Eval("Weekday") + "&#39;,&#39;" + Eval("BookingStatus") + "&#39;,&#39;" + Eval("IsHavingIssue") + "&#39;);" %>'
                                                        runat="server" GroupName="BookingNo" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField HeaderText="PO" DataField="PO" SortExpression="PO">
                                                <HeaderStyle Width="80px" HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:BoundField>
                                            <asp:BoundField HeaderText="Status" DataField="BookingStatus">
                                                <HeaderStyle Width="120px" HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:BoundField>
                                            <asp:TemplateField HeaderText="Vendor Name">
                                                <HeaderStyle Width="200px" HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Left" />
                                                <ItemTemplate>
                                                    <cc1:ucLiteral ID="UcLiteral2" runat="server" Text='<%#Eval("VendorName") %>'></cc1:ucLiteral>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Hazardous Items ?" Visible="false" SortExpression="IsEnableHazardouesItemPrompt">
                                                <HeaderStyle Width="100px" HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Left" Width="100px" />
                                                <ItemTemplate>
                                                    <cc1:ucLabel ID="lblEnableHazardouesItemPrompt" Text='<%# Convert.ToBoolean(Eval("IsEnableHazardouesItemPrompt")) ==true ? "Yes" :"No" %>'
                                                        runat="server"></cc1:ucLabel>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="ISPM15" SortExpression="IsVenodrPalletChecking">
                                                <HeaderStyle Width="100px" HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Left" Width="100px" />
                                                <ItemTemplate>
                                                    <cc1:ucLabel ID="lblVendorPalletCheckingRequired" Visible='<%# Convert.ToBoolean(Eval("IsVenodrPalletChecking")) ||
                                                            Convert.ToBoolean(Eval("IsStandardPalletChecking")) ||  Convert.ToBoolean(Eval("IsISPM15Applied")) %>'
                                                        ForeColor="Red" runat="server"></cc1:ucLabel>
                                                    <asp:HiddenField ID="hdnIsVenodrPalletChecking_1" runat="server" Value='<%#Eval("IsVenodrPalletChecking") %>' />
                                                    <asp:HiddenField ID="hdnIsStandardPalletChecking_1" runat="server" Value='<%#Eval("IsStandardPalletChecking") %>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Door Name">
                                                <HeaderStyle Width="40px" HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Center" />
                                                <ItemTemplate>
                                                    <cc1:ucLiteral ID="UcLiteral7" runat="server" Text='<%#Eval("DoorNumber") %>'></cc1:ucLiteral>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField HeaderText="BookingRefNo" DataField="BookingRef">
                                                <HeaderStyle Width="140px" HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:BoundField>
                                            <asp:TemplateField HeaderText="TimeDay">
                                                <HeaderStyle Width="110px" HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Left" />
                                                <ItemTemplate>
                                                    <asp:HiddenField ID="hdnExpectedDeliveryDate" runat="server" Value='<%# Eval("DeliveryDate") %>' />
                                                    <asp:HiddenField ID="hdnTimeWindowID" runat="server" Value='<%# Eval("TimeWindowID") %>' />
                                                    <cc1:ucLiteral ID="UcLiteral17_2" runat="server" Text='<%#Eval("NonWindowFromToTime") %>'
                                                        Visible="false"></cc1:ucLiteral>
                                                    <cc1:ucLiteral ID="UcLiteral17_1" runat="server" Text='<%#Eval("WindowStartEndTime") %>'
                                                        Visible="false"></cc1:ucLiteral>
                                                    <cc1:ucLiteral ID="UcLiteral17" runat="server" Text='<%#Eval("ExpectedDeliveryTime") %>'></cc1:ucLiteral>&nbsp;
                                                    <cc1:ucLiteral ID="UcLiteral18" runat="server" Text='<%# Eval("DeliveryDate", "{0:dddd}")%>'></cc1:ucLiteral>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Carrier Name">
                                                <HeaderStyle Width="120px" HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Left" />
                                                <ItemTemplate>
                                                    <cc1:ucLiteral ID="UcLiteral3" runat="server" Text='<%#Eval("CarrierName") %>'></cc1:ucLiteral>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="NoOfPallets">
                                                <HeaderStyle Width="70px" HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Center" />
                                                <ItemTemplate>
                                                    <cc1:ucLiteral ID="UcLiteral4" runat="server" Text='<%#Eval("PalletsScheduled") %>'></cc1:ucLiteral>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="NoOfCartons">
                                                <HeaderStyle Width="75px" HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Center" />
                                                <ItemTemplate>
                                                    <cc1:ucLiteral ID="UcLiteral6" runat="server" Text='<%#Eval("CatronsScheduled") %>'></cc1:ucLiteral>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="NoOfLines">
                                                <HeaderStyle Width="70px" HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Center" />
                                                <ItemTemplate>
                                                    <cc1:ucLiteral ID="UcLiteral5" runat="server" Text='<%#Eval("LinesScheduled") %>'></cc1:ucLiteral>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField HeaderText="NoOfLifts" DataField="LiftsScheduled">
                                                <HeaderStyle Width="70px" HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:BoundField HeaderText="Booking Type" DataField="BookingType">
                                                <HeaderStyle Width="150px" HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:BoundField>
                                            <asp:TemplateField HeaderText="Delivery Type">
                                                <HeaderStyle Width="120px" HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Left" />
                                                <ItemTemplate>
                                                    <cc1:ucLiteral ID="UcLiteral1" runat="server" Text='<%#Eval("DeliveryType") %>'></cc1:ucLiteral>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField HeaderText="Stock Planner" DataField="InventoryManager">
                                                <HeaderStyle Width="120px" HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:BoundField>
                                            <asp:BoundField HeaderText="Vehicle Type" DataField="VehicleType">
                                                <HeaderStyle Width="120px" HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:BoundField>
                                            <asp:BoundField HeaderText="Pre Advise Notification" DataField="PreAdviseNotification">
                                                <HeaderStyle Width="120px" HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:BoundField>
                                            <asp:BoundField HeaderText="Door Type" DataField="DoorType">
                                                <HeaderStyle Width="120px" HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:BoundField>
                                            <%-- Sprint 3a start --%>
                                            <asp:TemplateField HeaderText="Due Date" SortExpression="dueDate">
                                                <HeaderStyle Width="120px" HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Left" />
                                                <ItemTemplate>
                                                    <cc1:ucLiteral ID="ltdueDate" runat="server" Text='<%# Eval("dueDate") != DBNull.Value ?  Convert.ToDateTime(Eval("dueDate").ToString()).ToString("dd/MM/yyyy") : ""%>'></cc1:ucLiteral>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Arrived At" SortExpression="ArrivedAt">
                                                <HeaderStyle Width="120px" HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Left" />
                                                <ItemTemplate>
                                                    <cc1:ucLiteral ID="ltArrivedAt" runat="server" Text='<%# Eval("ArrivedAt") != DBNull.Value ?  Convert.ToDateTime(Eval("ArrivedAt").ToString()).ToString("dd/MM/yyyy - HH:mm") : ""%>'></cc1:ucLiteral>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Unloaded At" SortExpression="UnloadAt">
                                                <HeaderStyle Width="120px" HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Left" />
                                                <ItemTemplate>
                                                    <cc1:ucLiteral ID="ltUnloadAt" runat="server" Text='<%#Eval("UnloadAt") != DBNull.Value ?  Convert.ToDateTime(Eval("UnloadAt").ToString()).ToString("dd/MM/yyyy - HH:mm") : "" %>'></cc1:ucLiteral>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField HeaderText="Booking Comment" DataField="BookingComments" SortExpression="BookingComments">
                                                <HeaderStyle Width="220px" HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:BoundField>
                                            <asp:BoundField HeaderText="WindowName" DataField="WindowName" SortExpression="WindowName">
                                                <HeaderStyle Width="220px" HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:BoundField>
                                            <%-- Sprint 3a End --%>
                                        </Columns>
                                    </cc1:ucGridView>
                                    <cc1:ucGridView ID="gvVendor" runat="server" AutoGenerateColumns="false" CssClass="grid gvclass"
                                        CellPadding="0" Width="2100px" DataKeyNames="PKID" OnSorting="gridView_Sorting"
                                        OnRowDataBound="gvVendor_RowDataBound" AllowSorting="true" AllowPaging="false"
                                        PageSize="10" >
                                        <AlternatingRowStyle BackColor="White" ForeColor="#284775"></AlternatingRowStyle>
                                        <Columns>
                                            <asp:TemplateField>
                                                <HeaderStyle Width="20px" HorizontalAlign="Left" />
                                                <ItemStyle Width="20px" HorizontalAlign="Left" />
                                                <ItemTemplate>
                                                    <asp:HiddenField ID="hdnBookingID" runat="server" Value='<%#Eval("PKID") %>' />
                                                    <asp:HiddenField ID="hdnSupplierType" runat="server" Value='<%#Eval("SupplierType") %>' />
                                                    <asp:RadioButton ID="rdoBookingID" onclick='<%# "javascript:EnableDisableButtonsVendor(this,&#39;" + Eval("PKID") + "&#39;,&#39;" + Eval("BookingStatusID") + "&#39;,&#39;" + Eval("SupplierType") + "&#39;,&#39;" + Eval("Siteid") + "&#39;,&#39;" + Eval("DeliveryDate", "{0:dd/MM/yyyy}") + "&#39;,&#39;" + Eval("LogicalDate", "{0:dd/MM/yyyy}") + "&#39;,&#39;" + Eval("BookingStatus") + "&#39;);" %>'
                                                        runat="server" GroupName="BookingNo" />
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Site">
                                                <HeaderStyle Width="120px" HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Left" />
                                                <ItemTemplate>
                                                    <cc1:ucLabel ID="LBLSiteName" runat="server" Text='<%# Eval("SiteName") %>'></cc1:ucLabel>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Hazardous Item ?">
                                                 <HeaderStyle Width="120px" HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Left" />
                                                <ItemTemplate>
                                                     <cc1:ucLabel ID="lblHazardousItemCheckRequired"
                                                        Visible='<%# Convert.ToBoolean(Eval("IsEnableHazardouesItemPrompt")) %>'
                                                        ForeColor="Red" runat="server"
                                                        Text="Hazardous Item Check Required."></cc1:ucLabel>
                                                </ItemTemplate>

                                            </asp:TemplateField>

                                            <asp:BoundField HeaderText="Status" DataField="BookingStatus" SortExpression="BookingStatus">
                                                <HeaderStyle Width="120px" HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:BoundField>
                                            <asp:TemplateField HeaderText="ISPM15/Standard pallet check" SortExpression="IsVenodrPalletChecking">
                                                <HeaderStyle Width="100px" HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Left" Width="100px" />
                                                <ItemTemplate>
                                                    <cc1:ucLabel ID="lblVendorPalletCheckingRequired_1" Visible='<%# Convert.ToBoolean(Eval("IsVenodrPalletChecking")) 
                                                            ||Convert.ToBoolean(Eval("IsISPM15Applied"))%>'
                                                        ForeColor="Red" runat="server"></cc1:ucLabel>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="AdditionalInfo" SortExpression="BookingComments">
                                                <HeaderStyle Width="20px" HorizontalAlign="Left" />
                                                <ItemStyle Width="20px" HorizontalAlign="Center" />
                                                <ItemTemplate>
                                                    <div>
                                                        <a href="javascript:void(0)" rel='<%# DataBinder.Eval(Container.DataItem, "BookingComments") %>'
                                                            tipwidth="350" onmouseover="ddrivetip('<%# DataBinder.Eval(Container.DataItem, "BookingComments") %>','#ededed','350');"
                                                            onmouseout="hideddrivetip();">
                                                            <img src="../../../Images/info_button1.gif" runat="server" id="imgInfo" align="absMiddle"
                                                                border="0" /></a>
                                                    </div>
                                                    <div class="balloonstyle" id='<%# DataBinder.Eval(Container.DataItem, "BookingComments") %>'>
                                                        <span class="bla8blue">
                                                            <div style="text-align: center;">
                                                                <strong>
                                                                    <cc1:ucLabel ID="lblAdditionalInfo" runat="server"></cc1:ucLabel></strong>
                                                                <br />
                                                                <br />
                                                                <asp:Literal ID="ltInfo" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "BookingComments") %>'></asp:Literal>
                                                            </div>
                                                        </span>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField HeaderText="Booking Ref #" DataField="BookingRef" SortExpression="BookingRef">
                                                <HeaderStyle Width="170px" HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:BoundField>
                                            <asp:BoundField HeaderText="Priority" DataField="Priority" SortExpression="Priority"
                                                Visible="false">
                                                <HeaderStyle Width="50px" HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:BoundField>
                                            <asp:BoundField HeaderText="Booking Type" DataField="BookingType" SortExpression="BookingType">
                                                <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:BoundField>
                                            <asp:TemplateField HeaderText="Carrier Name">
                                                <HeaderStyle Width="180px" HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Left" />
                                                <ItemTemplate>
                                                    <cc1:ucLiteral ID="UcLiteral3" runat="server" Text='<%#Eval("CarrierName") %>'></cc1:ucLiteral>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Expected Delivery <br /> Date">
                                                <HeaderStyle Width="200px" HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Left" />
                                                <ItemTemplate>
                                                    <asp:HiddenField ID="hdnExpectedDeliveryDate" runat="server" Value='<%# Eval("DeliveryDate") %>' />
                                                    <cc1:ucLabel ID="lblExpectedDeliveryDate" runat="server" Text='<%# Eval("DeliveryDate", "{0:dd/MM/yyyy}") %>'></cc1:ucLabel>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Time / Day" SortExpression="Weekday">
                                                <HeaderStyle Width="150px" HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Left" />
                                                <ItemTemplate>
                                                    <asp:HiddenField ID="hdnTimeWindowID" runat="server" Value='<%# Eval("TimeWindowID") %>' />
                                                    <cc1:ucLiteral ID="UcLiteral27_2" runat="server" Text='<%#Eval("NonWindowFromToTime") %>'
                                                        Visible="false"></cc1:ucLiteral>
                                                    <cc1:ucLiteral ID="UcLiteral27_1" runat="server" Text='<%#Eval("WindowStartEndTime") %>'
                                                        Visible="false"></cc1:ucLiteral>
                                                    <cc1:ucLiteral ID="UcLiteral27" runat="server" Text='<%#Eval("ExpectedDeliveryTime") %>'></cc1:ucLiteral>&nbsp;
                                                    <cc1:ucLiteral ID="UcLiteral28" runat="server" Text='<%# Eval("DeliveryDate", "{0:dddd}")%>'></cc1:ucLiteral>
                                                    <asp:HiddenField ID="hdnTime" runat="server" Value='<%#  Eval("ExpectedDeliveryTime") %>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Door Name">
                                                <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Left" />
                                                <ItemTemplate>
                                                    <cc1:ucLiteral ID="UcLiteral7" runat="server" Text='<%#Eval("DoorNumber") %>'></cc1:ucLiteral>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Vendor Name">
                                                <HeaderStyle Width="200px" HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Left" />
                                                <ItemTemplate>
                                                    <cc1:ucLiteral ID="UcLiteral2" runat="server" Text='<%#Eval("VendorName") %>'></cc1:ucLiteral>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Delivery Type">
                                                <HeaderStyle Width="150px" HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Left" />
                                                <ItemTemplate>
                                                    <cc1:ucLiteral ID="UcLiteral1" runat="server" Text='<%#Eval("DeliveryType") %>'></cc1:ucLiteral>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField HeaderText="# Lifts Scheduled" DataField="LiftsScheduled" SortExpression="LiftsScheduled">
                                                <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:BoundField>
                                            <asp:TemplateField HeaderText="# Pallets Scheduled">
                                                <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Left" />
                                                <ItemTemplate>
                                                    <cc1:ucLiteral ID="UcLiteral4" runat="server" Text='<%#Eval("PalletsScheduled") %>'></cc1:ucLiteral>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="# Lines Scheduled">
                                                <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Left" />
                                                <ItemTemplate>
                                                    <cc1:ucLiteral ID="UcLiteral5" runat="server" Text='<%#Eval("LinesScheduled") %>'></cc1:ucLiteral>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="# Cartons Scheduled">
                                                <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Left" />
                                                <ItemTemplate>
                                                    <cc1:ucLiteral ID="UcLiteral6" runat="server" Text='<%#Eval("CatronsScheduled") %>'></cc1:ucLiteral>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField HeaderText="Stock Planner" DataField="InventoryManager" SortExpression="InventoryManager">
                                                <HeaderStyle Width="150px" HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:BoundField>
                                            <asp:BoundField HeaderText="WindowName" DataField="WindowName" SortExpression="WindowName">
                                                <HeaderStyle Width="220px" HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:BoundField>
                                        </Columns>

                                    </cc1:ucGridView>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <cc1:PagerV2_8 ID="pager1" runat="server" OnCommand="pager_Command" GenerateGoToSection="false"></cc1:PagerV2_8>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <div style="float: left; width: 84%;">
                <table cellpadding="5" cellspacing="2" width="100%">
                    <tr>
                        <td width="55%" valign="top">
                            <cc1:ucPanel ID="pnlRunningTotal_1" Visible="false" runat="server" GroupingText="Current Running Total"
                                CssClass="fieldset-form">
                                <table width="100%" cellspacing="5" cellpadding="0" border="0" class="form-table" height="130Px">
                                    <tr>
                                        <th width="40%"></th>
                                        <th width="30%">
                                            <cc1:ucLabel ID="lblDailyMaximums" runat="server" Text="Daily Maximums"></cc1:ucLabel>
                                        </th>
                                        <th width="30%">
                                            <cc1:ucLabel ID="lblRemainingCapacity" runat="server" Text="Remaining Capacity"></cc1:ucLabel>
                                        </th>
                                    </tr>
                                    <tr>
                                        <td>
                                            <cc1:ucLabel ID="lblMaximumLifts" runat="server" Text="Maximum Lifts"></cc1:ucLabel>
                                        </td>
                                        <td align="center">
                                            <cc1:ucLiteral ID="ltMaximumLifts" runat="server"></cc1:ucLiteral>
                                        </td>
                                        <td align="center">
                                            <cc1:ucLiteral ID="ltRemainingLifts" runat="server"></cc1:ucLiteral>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <cc1:ucLabel ID="lblMaximumPallets" runat="server" Text="Maximum Pallets"></cc1:ucLabel>
                                        </td>
                                        <td align="center">
                                            <cc1:ucLiteral ID="ltMaximumPallets" runat="server"></cc1:ucLiteral>
                                        </td>
                                        <td align="center">
                                            <cc1:ucLiteral ID="ltRemainingPallets" runat="server"></cc1:ucLiteral>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <cc1:ucLabel ID="lblMaximumLines" runat="server" Text="Maximum Lines"></cc1:ucLabel>
                                        </td>
                                        <td align="center">
                                            <cc1:ucLiteral ID="ltMaximumLines" runat="server"></cc1:ucLiteral>
                                        </td>
                                        <td align="center">
                                            <cc1:ucLiteral ID="ltRemainingLines" runat="server"></cc1:ucLiteral>
                                        </td>
                                    </tr>
                                </table>
                            </cc1:ucPanel>
                            <cc1:ucPanel ID="pnlVolumeDetails_1" Visible="false" runat="server" GroupingText="Volume Details"
                                CssClass="fieldset-form">
                                <table width="100%" cellspacing="5" cellpadding="0" border="0" class="form-table">
                                    <tr>
                                        <th width="10%"></th>
                                        <th width="10%">
                                            <cc1:ucLabel ID="lblMax" runat="server" Text="Max"></cc1:ucLabel>
                                        </th>
                                        <th width="10%">
                                            <cc1:ucLabel ID="lblConfirmed" runat="server" Text="Confirmed"></cc1:ucLabel>
                                        </th>
                                        <th width="10%">
                                            <cc1:ucLabel ID="lblUnConfirmed" runat="server" Text="Unconfirmed"></cc1:ucLabel>
                                        </th>
                                        <th width="25%">
                                            <cc1:ucLabel ID="lblNonTimed" runat="server" Text="Non-Timed"></cc1:ucLabel>
                                        </th>
                                        <th width="10%">
                                            <cc1:ucLabel ID="lblTotal" runat="server" Text="Total"></cc1:ucLabel>
                                        </th>
                                        <th width="10%">
                                            <cc1:ucLabel ID="lblSpace" runat="server" Text="Space"></cc1:ucLabel>
                                        </th>
                                        <th width="10%">
                                            <cc1:ucLabel ID="lblArrived" runat="server" Text="Arrived"></cc1:ucLabel>
                                        </th>
                                        <th width="10%">
                                            <cc1:ucLabel ID="lblUnloaded" runat="server" Text="Unloaded"></cc1:ucLabel>
                                        </th>
                                        <th width="10%">
                                            <cc1:ucLabel ID="lblRefusedNoShow" runat="server" Text="Refused/NoShow"></cc1:ucLabel>
                                        </th>
                                    </tr>
                                    <tr>
                                        <td width="10%">
                                            <cc1:ucLabel ID="lblDeliveries" runat="server" Text="Deliveries:"></cc1:ucLabel>
                                        </td>
                                        <td align="center" width="10%">
                                            <cc1:ucLabel ID="lblMaxDeliveriesValue" runat="server"></cc1:ucLabel>
                                        </td>
                                        <td align="center" width="10%">
                                            <cc1:ucLiteral ID="lblConfirmedDeliveriesValue" runat="server"></cc1:ucLiteral>
                                        </td>
                                        <td align="center" width="10%">
                                            <cc1:ucLiteral ID="lblUnConfirmedDeliveriesValue" runat="server"></cc1:ucLiteral>
                                        </td>
                                        <td align="center" width="10%">
                                            <cc1:ucLiteral ID="lblNonTimedDeliveriesValue" runat="server"></cc1:ucLiteral>
                                        </td>
                                        <td align="center" width="10%">
                                            <cc1:ucLabel ID="lblTotalDeliveriesValue" runat="server"></cc1:ucLabel>
                                        </td>
                                        <td align="center" width="10%">
                                            <cc1:ucLiteral ID="lblSpaceDeliveriesValue" runat="server"></cc1:ucLiteral>
                                        </td>
                                        <td align="center" width="10%">
                                            <cc1:ucLiteral ID="lblArrivedDeliveriesValue" runat="server"></cc1:ucLiteral>
                                        </td>
                                        <td align="center" width="10%">
                                            <cc1:ucLiteral ID="lblUnloadedDeliveriesValue" runat="server"></cc1:ucLiteral>
                                        </td>
                                        <td align="center" width="10%">
                                            <cc1:ucLiteral ID="lblRefusedNoShowDeliveriesValue" runat="server"></cc1:ucLiteral>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="10%">
                                            <cc1:ucLabel ID="lblPallets" runat="server" Text="Pallets:"></cc1:ucLabel>
                                        </td>
                                        <td align="center" width="10%">
                                            <cc1:ucLabel ID="lblMaxPalletsValue" runat="server"></cc1:ucLabel>
                                        </td>
                                        <td align="center" width="10%">
                                            <cc1:ucLiteral ID="lblConfirmedPalletsValue" runat="server"></cc1:ucLiteral>
                                        </td>
                                        <td align="center" width="10%">
                                            <cc1:ucLiteral ID="lblUnConfirmedPalletsValue" runat="server"></cc1:ucLiteral>
                                        </td>
                                        <td align="center" width="10%">
                                            <cc1:ucLiteral ID="lblNonTimedPalletsValue" runat="server"></cc1:ucLiteral>
                                        </td>
                                        <td align="center" width="10%">
                                            <cc1:ucLabel ID="lblTotalPalletsValue" runat="server"></cc1:ucLabel>
                                        </td>
                                        <td align="center" width="10%">
                                            <cc1:ucLiteral ID="lblSpacePalletsValue" runat="server"></cc1:ucLiteral>
                                        </td>
                                        <td align="center" width="10%">
                                            <cc1:ucLiteral ID="lblArrivedPalletsValue" runat="server"></cc1:ucLiteral>
                                        </td>
                                        <td align="center" width="10%">
                                            <cc1:ucLiteral ID="lblUnloadedPalletsValue" runat="server"></cc1:ucLiteral>
                                        </td>
                                        <td align="center" width="10%">
                                            <cc1:ucLiteral ID="lblRefusedNoShowPalletsValue" runat="server"></cc1:ucLiteral>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="10%">
                                            <cc1:ucLabel ID="lblLines_1" runat="server" Text="Lines:"></cc1:ucLabel>
                                        </td>
                                        <td align="center" width="10%">
                                            <cc1:ucLabel ID="lblMaxLinesValue" runat="server"></cc1:ucLabel>
                                        </td>
                                        <td align="center" width="10%">
                                            <cc1:ucLiteral ID="lblConfirmedLinesValue" runat="server"></cc1:ucLiteral>
                                        </td>
                                        <td align="center" width="10%">
                                            <cc1:ucLiteral ID="lblUnConfirmedLinesValue" runat="server"></cc1:ucLiteral>
                                        </td>
                                        <td align="center" width="10%">
                                            <cc1:ucLiteral ID="lblNonTimedLinesValue" runat="server"></cc1:ucLiteral>
                                        </td>
                                        <td align="center" width="10%">
                                            <cc1:ucLabel ID="lblTotalLinesValue" runat="server"></cc1:ucLabel>
                                        </td>
                                        <td align="center" width="10%">
                                            <cc1:ucLiteral ID="lblSpaceLinesValue" runat="server"></cc1:ucLiteral>
                                        </td>
                                        <td align="center" width="10%">
                                            <cc1:ucLiteral ID="lblArrivedLinesValue" runat="server"></cc1:ucLiteral>
                                        </td>
                                        <td align="center" width="10%">
                                            <cc1:ucLiteral ID="lblUnloadedLinesValue" runat="server"></cc1:ucLiteral>
                                        </td>
                                        <td align="center" width="10%">
                                            <cc1:ucLiteral ID="lblRefusedNoShowLinesValue" runat="server"></cc1:ucLiteral>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="10%">
                                            <cc1:ucLabel ID="lblCartons_1" runat="server" Text="Cartons:"></cc1:ucLabel>
                                        </td>
                                        <td align="center" width="10%">
                                            <cc1:ucLabel ID="lblMaxCartonsValue" runat="server"></cc1:ucLabel>
                                        </td>
                                        <td align="center" width="10%">
                                            <cc1:ucLiteral ID="lblConfirmedCartonsValue" runat="server"></cc1:ucLiteral>
                                        </td>
                                        <td align="center" width="10%">
                                            <cc1:ucLiteral ID="lblUnConfirmedCartonsValue" runat="server"></cc1:ucLiteral>
                                        </td>
                                        <td align="center" width="10%">
                                            <cc1:ucLiteral ID="lblNonTimedCartonsValue" runat="server"></cc1:ucLiteral>
                                        </td>
                                        <td align="center" width="10%">
                                            <cc1:ucLabel ID="lblTotalCartonsValue" runat="server"></cc1:ucLabel>
                                        </td>
                                        <td align="center" width="10%">
                                            <cc1:ucLiteral ID="lblSpaceCartonsValue" runat="server"></cc1:ucLiteral>
                                        </td>
                                        <td align="center" width="10%">
                                            <cc1:ucLiteral ID="lblArrivedCartonsValue" runat="server"></cc1:ucLiteral>
                                        </td>
                                        <td align="center" width="10%">
                                            <cc1:ucLiteral ID="lblUnloadedCartonsValue" runat="server"></cc1:ucLiteral>
                                        </td>
                                        <td align="center" width="10%">
                                            <cc1:ucLiteral ID="lblRefusedNoShowCartonsValue" runat="server"></cc1:ucLiteral>
                                        </td>
                                    </tr>
                                </table>
                            </cc1:ucPanel>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="float: right; width: 15%;">
                <cc1:ucPanel ID="pnlLinks" runat="server" GroupingText="Links" Visible="false" CssClass="fieldset-form">
                    <table border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td style="height: 130px;">

                                <ul>
                                    <li style="margin-bottom: 10px">
                                        <cc1:ucLinkButton ID="lnkNonTimeBookingDetails" runat="server" OnClientClick="return ShowNonTimeBookingModalPopup();">Non Time Booking Details</cc1:ucLinkButton>
                                    </li>
                                    <li style="margin-bottom: 10px">
                                        <cc1:ucLinkButton ID="lnkNonStandardWindowBookingDetails" runat="server" OnClientClick="return ShowNonStandardWindowBookingModalPopup();">Non Standard Window Booking Details</cc1:ucLinkButton>
                                    </li>
                                    <li style="margin-bottom: 10px">
                                        <cc1:ucLinkButton ID="lnkGoto" runat="server" OnClick="lnkGoto_Click">Go To Alternate View</cc1:ucLinkButton>
                                        <cc1:ucLinkButton ID="lnkGotoWindow" runat="server" OnClick="lnkGotoWindow_Click">Go To Alternate View</cc1:ucLinkButton>
                                    </li>
                                    <li style="margin-bottom: 10px">
                                        <cc1:ucLinkButton ID="lnkCurrentBookingDetails" runat="server" OnClientClick="return ShowCurrentBookingModalPopup();">Current Booking Details</cc1:ucLinkButton></p>
                                    </li>
                                    <li style="margin-bottom: 10px">
                                        <cc1:ucLinkButton ID="lnkLegends" runat="server" OnClientClick="return ShowLegendPopup();">Legends</cc1:ucLinkButton>
                                    </li>
                                    <li style="margin-bottom: 10px">
                                        <uc1:ucExportButton ID="btnExportNew" runat="server" />
                                        Export to Excel
                                    </li>
                                </ul>
                            </td>
                        </tr>
                    </table>
                </cc1:ucPanel>
            </div>
            <asp:UpdatePanel ID="pnlShowLegendPopup" runat="server" UpdateMode="Always">
                <ContentTemplate>
                    <table id="tblAlternate" runat="server" cellspacing="1" cellpadding="0" visible="false">
                        <tr>
                            <td>

                                <div class="container">
                                    <asp:Panel ID="tableDiv_GeneralNew" runat="server" CssClass="fixed_headers">
                                    </asp:Panel>
                                </div>
                            </td>
                        </tr>
                    </table>
                </ContentTemplate>
            </asp:UpdatePanel>

            <asp:Panel ID="pnlWindow" runat="server" CssClass="fixed_headers" ScrollBars="Both">
            </asp:Panel>
            <div class="button-row">
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnNoShow" />
            <asp:PostBackTrigger ControlID="btnEditABooking" />
            <asp:PostBackTrigger ControlID="btnExportToExcel1" />
            <asp:PostBackTrigger ControlID="btnSearch" />
            <asp:PostBackTrigger ControlID="imgAlternateView" />
            <asp:PostBackTrigger ControlID="lnkGoto" />
            <asp:PostBackTrigger ControlID="ddlSite" />
            <asp:PostBackTrigger ControlID="imgWindowAlternateView" />
        </Triggers>
    </asp:UpdatePanel>

    <asp:UpdatePanel ID="upShowLegend" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <asp:Button ID="btnShowLegend" runat="Server" Style="display: none" />
            <ajaxToolkit:ModalPopupExtender ID="mdlShowLegend" runat="server" TargetControlID="btnShowLegend"
                PopupControlID="pnlShowLegend" BackgroundCssClass="modalBackground" BehaviorID="ShowLegend"
                DropShadow="false" />
            <asp:Panel ID="pnlShowLegend" runat="server" CssClass="fieldset-form" Style="display: none">
                <div class="popup-maincontainer" style="overflow-y: hidden; overflow-x: hidden;">
                    <table width="100%" cellspacing="2" cellpadding="0" border="0" class="popup-maincontainer">
                        <tr>
                            <td width="100%" valign="top">
                                <asp:Panel ID="pnlLegend_5" runat="server" GroupingText="Legends" CssClass="fieldset-form">
                                    <table cellspacing="5" cellpadding="0" border="0" align="center">
                                        <tr>
                                            <td style="width: 33%;" class="spanLeft">
                                                <cc1:ucLabel ID="lblAvailableSlot_5" runat="server" Text="Available Slot"></cc1:ucLabel>
                                                <div class="whiteBox">
                                                </div>
                                            </td>
                                            <td style="width: 33%;" class="spanLeft">
                                                <cc1:ucLabel ID="lblBookedSlot_5" runat="server" Text="Booked Slot"></cc1:ucLabel>
                                                <div class="greenBox">
                                                </div>
                                            </td>
                                            <td style="width: 33%;" class="spanLeft">
                                                <cc1:ucLabel ID="lblFixedSlot_5" runat="server" Text="Fixed Slot"></cc1:ucLabel>
                                                <div class="redBox">
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="spanLeft">
                                                <cc1:ucLabel ID="lblSlotOverspill_5" runat="server" Text="Slot Overspill"></cc1:ucLabel>
                                                <div class="orangeBox">
                                                </div>
                                            </td>
                                            <td class="spanLeft">
                                                <cc1:ucLabel ID="lblSoftSlot_5" runat="server" Text="Soft Slot"></cc1:ucLabel>
                                                <div class="pinkBox">
                                                </div>
                                            </td>
                                            <td class="spanLeft">
                                                <cc1:ucLabel ID="lblDoorNotAvailable_5" runat="server" Text="Door Not Available"></cc1:ucLabel>
                                                <div class="blueBox">
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" colspan="3">
                                                <br />
                                                <cc1:ucButton ID="btnClose" runat="server" Text="CLOSE" CssClass="button" Style="text-transform: uppercase;"
                                                    OnClientClick="return HideLegendModalPopup();" />
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
            </td>
                </tr>
            </table>
            <div class="bottom-shadow">
            </div>
            <div class="button-row">
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnClose" />
            <asp:PostBackTrigger ControlID="btnNoShow" />
            <asp:PostBackTrigger ControlID="btnEditABooking" />
            <asp:PostBackTrigger ControlID="btnExportToExcel1" />
            <asp:PostBackTrigger ControlID="btnSearch" />
            <asp:PostBackTrigger ControlID="imgAlternateView" />
            <asp:PostBackTrigger ControlID="lnkGoto" />
            <asp:PostBackTrigger ControlID="ddlSite" />
            <asp:PostBackTrigger ControlID="lnkLegends" />
        </Triggers>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="updpnlConfirmation" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <asp:Button ID="btnConfirmationMsg" runat="Server" Style="display: none" />
            <ajaxToolkit:ModalPopupExtender ID="mdlConfirmationMsg" runat="server" TargetControlID="btnConfirmationMsg"
                PopupControlID="pnlConfirmationMsg" BackgroundCssClass="modalBackground" BehaviorID="ConfirmationMsg"
                DropShadow="false" />
            <asp:Panel ID="pnlConfirmationMsg" runat="server" Style="display: none;">
                <div style="overflow-y: hidden; overflow-x: hidden; background-color: #fff; padding: 5px; border: 2px solid #ccc; text-align: left;">
                    <table cellspacing="5" cellpadding="0" border="0" align="center" class="top-setting-Popup">
                        <tr>
                            <td style="font-weight: bold; text-align: center;">
                                <cc1:ucLiteral ID="ltConfirmationMsg" runat="server" Text=""></cc1:ucLiteral>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <cc1:ucButton ID="btnConfirmationMsgYes" runat="server" Text="Yes" CssClass="button"
                                    OnCommand="btnConfirmationMsgYes_Click" />
                                &nbsp;&nbsp;&nbsp;&nbsp;<cc1:ucButton ID="btnConfirmationMsgNo" runat="server" Text="No"
                                    CssClass="button" OnCommand="btnConfirmationMsgNo_Click" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnConfirmationMsgYes" />
            <asp:PostBackTrigger ControlID="btnDeleteABooking" />
        </Triggers>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:Button ID="btnmsgOK" runat="Server" Style="display: none" />
            <ajaxToolkit:ModalPopupExtender ID="mdlok" runat="server"
                TargetControlID="btnmsgOK"
                PopupControlID="pnlMsg"
                BackgroundCssClass="modalBackground"
                BehaviorID="Msg"
                DropShadow="false" />
            <asp:Panel ID="pnlMsg" runat="server" Style="display: none;">
                <div style="overflow-y: hidden; overflow-x: hidden; background-color: #fff; padding: 5px; border: 2px solid #ccc; text-align: left;">
                    <table cellspacing="5" cellpadding="0" border="0" align="center" class="top-setting-Popup">
                        <tr>
                            <td style="font-weight: bold;">
                                <cc1:ucLiteral ID="ltMsgOK" runat="server" Text=""></cc1:ucLiteral>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <cc1:ucButton ID="btnMessageOk" CommandName="Rediract" runat="server" Text="OK" OnCommand="btnMessageOk_Click"
                                    CssClass="button" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnDelivaryUnload" />
            <asp:PostBackTrigger ControlID="btnSearch" />
        </Triggers>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="updNoShow" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <asp:Button ID="btnNoShows" runat="Server" Style="display: none" />
            <ajaxToolkit:ModalPopupExtender ID="mdNoShow" runat="server" TargetControlID="btnNoShows"
                PopupControlID="pnlbtnNoShow" BackgroundCssClass="modalBackground" BehaviorID="MsgNoShow"
                DropShadow="false" />
            <asp:Panel ID="pnlbtnNoShow" runat="server" Style="display: none;">
                <div style="overflow-y: hidden; overflow-x: hidden; background-color: #fff; padding: 5px; border: 2px solid #ccc; text-align: left;">
                    <table cellspacing="5" cellpadding="0" border="0" align="center" class="top-setting-Popup">
                        <tr>
                            <td style="font-weight: bold;">
                                <cc1:ucLabel ID="lblShowDeliveryMessage" runat="server" Text=""></cc1:ucLabel>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <cc1:ucButton ID="btnProceed" CommandName="NoShows" runat="server" Text="OK" OnCommand="btnProceed_Click"
                                    CssClass="button" />
                                <cc1:ucButton ID="btnBack" CommandName="NoShows" runat="server" Text="OK" OnCommand="btnBack_Click"
                                    CssClass="button" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnProceed" />
        </Triggers>
    </asp:UpdatePanel>
    <!-- Phase 13 R3 Point 18 --->
    <asp:UpdatePanel ID="updInfromation" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <asp:Button ID="btnInfo" runat="Server" Style="display: none" />
            <ajaxToolkit:ModalPopupExtender ID="mdInformation" runat="server" TargetControlID="btnInfo"
                PopupControlID="pnlbtnInfo" BackgroundCssClass="modalBackground" BehaviorID="MsgbtnInfo"
                DropShadow="false" />
            <asp:Panel ID="pnlbtnInfo" runat="server" Style="display: none;">
                <div class="popup-maincontainer" style="overflow-y: hidden; overflow-x: hidden;">
                    <h3>
                        <cc1:ucLabel ID="lblWarningInfo" runat="server"></cc1:ucLabel></h3>
                    <table cellspacing="5" cellpadding="0" border="0" align="center" class="popup-maincontainer">
                        <tr>
                            <td>
                                <div class="popup-innercontainer top-setting-Popup">
                                    <div class="row1">
                                        <cc1:ucLabel ID="lblSchedulingIssueCommunication" runat="server" Text="Please select which companies will receive mail relating to this issue"></cc1:ucLabel>
                                    </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="popup-innercontainer top-setting-Popup">
                                    <div class="row1">
                                        <cc1:ucLabel ID="lblCarrier" runat="server" Text="" Font-Bold="true"></cc1:ucLabel>
                                        <br />
                                        <cc1:ucCheckbox ID="chkCarrier" runat="server" Checked="true" Text="" TextAlign="Right" />
                                        <br />
                                        <br />
                                        <br />
                                        <cc1:ucLabel ID="lblVendors" runat="server" Text="" Font-Bold="true"></cc1:ucLabel>
                                        <cc1:ucCheckboxList ID="chkListVendors" runat="server" RepeatColumns="2">
                                        </cc1:ucCheckboxList>
                                    </div>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <div class="row">
                                    <cc1:ucButton ID="btnOK" runat="server" Text="" CssClass="button" OnCommand="btnOK_Click" />
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnProceed" />
        </Triggers>
    </asp:UpdatePanel>
    <!-- //Emergency fix - temp as per Anthony's mail on 11 jan 2016 -->
    <asp:UpdatePanel ID="updAshtonClose" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <asp:Button ID="btnAshtonClose" runat="Server" Style="display: none" />
            <ajaxToolkit:ModalPopupExtender ID="mdAshtonClose" runat="server" TargetControlID="btnAshtonClose"
                PopupControlID="pnlAshtonClose" BackgroundCssClass="modalBackground" BehaviorID="MsgAshtonClose"
                DropShadow="false" />
            <asp:Panel ID="pnlAshtonClose" runat="server" Style="display: none;">
                <div class="popup-maincontainer" style="overflow-y: hidden; overflow-x: hidden;">
                    <h3>
                        <cc1:ucLabel ID="lblWarning_1" runat="server" Text="WARNING"></cc1:ucLabel></h3>
                    <table cellspacing="5" cellpadding="0" border="0" align="center" class="popup-maincontainer">
                        <tr>
                            <td>
                                <div class="popup-innercontainer top-setting-Popup">
                                    <div class="row1" style="text-align: center;">
                                        <asp:Literal ID="ltwanrningmesaage" runat="server"></asp:Literal>
                                    </div>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <div class="row">
                                    <cc1:ucButton ID="btnOK_1" runat="server" Text="" CssClass="button" OnCommand="btnOK_1_Click" />
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnOK_1" />
        </Triggers>
    </asp:UpdatePanel>
    <!---------------------->
    <asp:UpdatePanel ID="UpdatePanel9" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <asp:Button ID="btnNonTimeBooking" runat="Server" Style="display: none" />
            <ajaxToolkit:ModalPopupExtender ID="mdlNonTimeBooking" runat="server" TargetControlID="btnNonTimeBooking"
                PopupControlID="pnlNonTimeBooking_1" BackgroundCssClass="modalBackground" BehaviorID="NonTimeBooking"
                DropShadow="false" />
            <asp:Panel ID="pnlNonTimeBooking_1" runat="server" Style="display: none;">
                <div class="popup-maincontainer" style="overflow-y: hidden; overflow-x: hidden;">
                    <table cellspacing="5" cellpadding="0" border="0" align="center" class="popup-maincontainer">
                        <tr>
                            <td>
                                <cc1:ucPanel ID="pnlNonTimeBooking" runat="server" GroupingText="Non Time Booking"
                                    CssClass="fieldset-form">
                                    <cc1:ucGridView ID="gvNonTimeBooking" runat="server" AutoGenerateColumns="false"
                                        EmptyDataText="  No records Found" CssClass="grid" CellPadding="0" Width="400px">
                                        <AlternatingRowStyle BackColor="White" ForeColor="#284775"></AlternatingRowStyle>
                                        <Columns>
                                            <asp:TemplateField HeaderText="Vendor/Carrier">
                                                <ItemStyle HorizontalAlign="Left" Width="40%" />
                                                <HeaderStyle HorizontalAlign="Left" />
                                                <ItemTemplate>
                                                    <asp:Label ID="vendorname" runat="server" Text='<%#Eval("Vendor.VendorName") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField HeaderText="Lifts" DataField="NumberOfLift" HeaderStyle-Width="20%"
                                                HeaderStyle-HorizontalAlign="Left" />
                                            <asp:BoundField HeaderText="Pallets" DataField="NumberOfPallet" HeaderStyle-Width="20%"
                                                HeaderStyle-HorizontalAlign="Left" />
                                            <asp:BoundField HeaderText="Cartons" DataField="NumberOfCartons" HeaderStyle-Width="20%"
                                                HeaderStyle-HorizontalAlign="Left" />
                                            <asp:BoundField HeaderText="Lines" DataField="NumberOfLines" HeaderStyle-Width="20%"
                                                HeaderStyle-HorizontalAlign="Left" />
                                        </Columns>
                                    </cc1:ucGridView>
                                </cc1:ucPanel>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <cc1:ucButton ID="btnBackNonTimeBooking" runat="server" Text="BACK" CssClass="button"
                                    OnClientClick="return HideNonTimeBookingModalPopup();" />
                </div>
                </td> </tr> </table> </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    <!------current booking info------>
    <asp:UpdatePanel ID="upPanleCurrentInfo" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <asp:Button ID="btnCurrentBookingInfo" runat="Server" Style="display: none" />
            <ajaxToolkit:ModalPopupExtender ID="mdlCurrentBookingInfo" runat="server" TargetControlID="btnCurrentBookingInfo"
                PopupControlID="pnlCurrentBooking" BackgroundCssClass="modalBackground" BehaviorID="CurrentBooking"
                DropShadow="false" />
            <asp:Panel ID="pnlCurrentBooking" runat="server" Style="display: none;">
                <div class="popup-maincontainer" style="overflow-y: hidden; overflow-x: hidden;">
                    <table cellspacing="5" cellpadding="0" border="0" align="center" class="popup-maincontainer">
                        <tr>
                            <td>
                                <div class="popup-maincontainer" style="overflow-y: hidden; overflow-x: hidden;">
                                    <cc1:ucPanel ID="pnlCurrentBookingInformation" runat="server" GroupingText="Current Booking Information"
                                        CssClass="fieldset-form">
                                        <table width="600px" cellspacing="5" cellpadding="0" border="0" class="form-table">
                                            <tr>
                                                <td width="20%">
                                                    <cc1:ucLabel ID="lblVendor_3" runat="server" Text="Vendor"></cc1:ucLabel>
                                                </td>
                                                <td width="2%">:
                                                </td>
                                                <td width="28%" class="nobold">
                                                    <span id="ltVendor_3"></span>
                                                </td>
                                                <td width="20%">
                                                    <cc1:ucLabel ID="lblCarrier_3" runat="server" Text="Carrier"></cc1:ucLabel>
                                                </td>
                                                <td width="2%">:
                                                </td>
                                                <td class="nobold" width="28%">
                                                    <span id="ltCarrier_3"></span>
                                                    &nbsp;
                                                     <span id="ltOtherCarrier_3"></span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <cc1:ucLabel ID="lblVehicle_3" runat="server" Text="Vehicle"></cc1:ucLabel>
                                                </td>
                                                <td>:
                                                </td>
                                                <td class="nobold">
                                                    <span id="ltVehicleType_3"></span>
                                                    &nbsp;
                                                     <span id="ltOtherVehicleType_3"></span>
                                                </td>
                                                <td>
                                                    <cc1:ucLabel ID="UcLabel1" runat="server" Text="Door"></cc1:ucLabel>
                                                </td>
                                                <td>:
                                                </td>
                                                <td class="nobold">
                                                    <span id="ltlDoor"></span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <cc1:ucLabel ID="lblNumberofLifts_1" runat="server" Text="Number of Lifts"></cc1:ucLabel>
                                                </td>
                                                <td>:
                                                </td>
                                                <td class="nobold">
                                                    <span id="ltExpectedLifts"></span>
                                                </td>
                                                <td>
                                                    <cc1:ucLabel ID="lblNumberofPallet_1" runat="server" Text="Number of Pallet"></cc1:ucLabel>
                                                </td>
                                                <td>:
                                                </td>
                                                <td class="nobold">
                                                    <span id="ltExpectedPallets"></span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <cc1:ucLabel ID="lblNumberofLines_1" runat="server" Text="Number of Lines"></cc1:ucLabel>
                                                </td>
                                                <td>:
                                                </td>
                                                <td class="nobold">
                                                    <span id="ltExpectedLines"></span>
                                                </td>
                                                <td>
                                                    <cc1:ucLabel ID="lblNumberofCartons_1" runat="server" Text="Number of Cartons"></cc1:ucLabel>
                                                </td>
                                                <td>:
                                                </td>
                                                <td class="nobold">
                                                    <span id="ltExpectedCartons"></span>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td>
                                                    <cc1:ucLabel ID="UcLabel2" runat="server" Text="Start Time"></cc1:ucLabel>
                                                </td>
                                                <td>:
                                                </td>
                                                <td class="nobold">
                                                    <span id="ltStartTime"></span>
                                                </td>
                                                <td>
                                                    <cc1:ucLabel ID="UcLabel3" runat="server" Text="End Time"></cc1:ucLabel>
                                                </td>
                                                <td>:
                                                </td>
                                                <td class="nobold">
                                                    <span id="ltEndTime"></span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="6">&nbsp;
                                                </td>
                                            </tr>
                                        </table>
                                    </cc1:ucPanel>
                                    <tr>
                                        <td align="center">
                                            <cc1:ucButton ID="UcButton1" runat="server" Text="BACK" CssClass="button"
                                                OnClientClick="return HideCurrentBookingModalPopup();" />
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    <!------current booking info------>
    <asp:UpdatePanel ID="UpNonStandardWindowBooking" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <asp:Button ID="btnNonStandardWindowBooking" runat="Server" Style="display: none" />
            <ajaxToolkit:ModalPopupExtender ID="mdlNonStandardWindowBooking" runat="server" TargetControlID="btnNonStandardWindowBooking"
                PopupControlID="pnlNonStandardWindowBooking_1" BackgroundCssClass="modalBackground" BehaviorID="NonStandardWindowBooking"
                DropShadow="false" />
            <asp:Panel ID="pnlNonStandardWindowBooking_1" runat="server" Style="display: none;">
                <div class="popup-maincontainer" style="overflow-y: hidden; overflow-x: hidden;">
                    <table cellspacing="5" cellpadding="0" border="0" align="center" class="popup-maincontainer">
                        <tr>
                            <td>
                                <cc1:ucPanel ID="pnlNonStandardWindowBooking" runat="server" GroupingText="Non Standard Window Booking"
                                    CssClass="fieldset-form">
                                    <cc1:ucGridView ID="gvNonStandardWindowBooking" runat="server" AutoGenerateColumns="false"
                                        EmptyDataText="  No records Found" CssClass="grid" CellPadding="0" Width="400px">
                                        <AlternatingRowStyle BackColor="White" ForeColor="#284775"></AlternatingRowStyle>
                                        <Columns>
                                            <asp:TemplateField HeaderText="Vendor/Carrier">
                                                <ItemStyle HorizontalAlign="Left" Width="40%" />
                                                <HeaderStyle HorizontalAlign="Left" />
                                                <ItemTemplate>
                                                    <asp:Label ID="vendorname" runat="server" Text='<%#Eval("Vendor.VendorName") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField HeaderText="Lifts" DataField="NumberOfLift" HeaderStyle-Width="20%"
                                                HeaderStyle-HorizontalAlign="Left" />
                                            <asp:BoundField HeaderText="Pallets" DataField="NumberOfPallet" HeaderStyle-Width="20%"
                                                HeaderStyle-HorizontalAlign="Left" />
                                            <asp:BoundField HeaderText="Cartons" DataField="NumberOfCartons" HeaderStyle-Width="20%"
                                                HeaderStyle-HorizontalAlign="Left" />
                                            <asp:BoundField HeaderText="Lines" DataField="NumberOfLines" HeaderStyle-Width="20%"
                                                HeaderStyle-HorizontalAlign="Left" />
                                        </Columns>
                                    </cc1:ucGridView>
                                </cc1:ucPanel>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <cc1:ucButton ID="btnBackNonStandardWindowBooking" runat="server" Text="BACK" CssClass="button"
                                    OnClientClick="return HideNonStandardWindowBookingModalPopup();" />
                </div>
                </td> </tr> </table> </div>
            </asp:Panel>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="lnkNonStandardWindowBookingDetails" />
            <asp:PostBackTrigger ControlID="btnBackNonStandardWindowBooking" />
        </Triggers>
    </asp:UpdatePanel>



    <script type="text/javascript">
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequest);
        function EndRequest(sender, args) {
            if (args.get_error() == undefined) {
                ShowCurrentBookingModalPopup();
                ShowNonTimeBookingModalPopup();
                HideNonTimeBookingModalPopup();
                HideCurrentBookingModalPopup();
                ShowLegendPopup();
                HideLegendModalPopup();
                ShowNonStandardWindowBookingModalPopup();
                HideNonStandardWindowBookingModalPopup();
            }
        }

        function ShowNonTimeBookingModalPopup() {
            $find("NonTimeBooking").show();
            return false;
        }

        function ShowLegendPopup() {
            $find("ShowLegend").show();
            return false;
        }

        function HideNonTimeBookingModalPopup() {
            $find("NonTimeBooking").hide();
            return false;
        }
        function HideLegendModalPopup() {
            $find("ShowLegend").hide();
            return false;
        }
        function ShowCurrentBookingModalPopup() {
            $find("CurrentBooking").show();
            return false;
        }
        function HideCurrentBookingModalPopup() {
            $find("CurrentBooking").hide();
            return false;
        }
        function ShowNonStandardWindowBookingModalPopup() {
            $find("NonStandardWindowBooking").show();
            return false;
        }

        function HideNonStandardWindowBookingModalPopup() {
            $find("NonStandardWindowBooking").hide();
            return false;
        }

        $(window).load(function () {
            $("[id$='lnkCurrentBookingDetails']").hide();
            //for orange color only having same name attribute for fixed slot
            $('.orangeTd').each(function (i) {
                var idOrange = $('.orangeTd')[i].attributes[3];
                var valueOfName = idOrange.value;
                $("[name='" + valueOfName + "']").removeClass('redTd').addClass('orangeTd');
                $("[name='" + valueOfName + "']").css({ 'vertical-align': 'top' });
                $("[name='" + valueOfName + "']").css({ 'line-height': '17px' });

            });
        });

        $('.orangeTd').each(function (i) {
            var idOrange = $('.orangeTd')[i].attributes[3];
            var valueOfName = idOrange.value;
            $("[name='" + valueOfName + "']").removeClass('redTd').addClass('orangeTd');
            $("[name='" + valueOfName + "']").css({ 'vertical-align': 'top' });
            $("[name='" + valueOfName + "']").css({ 'line-height': '17px' });
        });
    </script>
</asp:Content>
